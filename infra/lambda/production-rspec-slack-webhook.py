'''
this script is not getting deployed automatically in anyway as of yet
this is the lambda function that is actually getting ran:
https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/tc-www-production-rspec-slack-webhook?tab=graph
'''
from __future__ import print_function

import boto3
import json
import logging
import os

from base64 import b64decode
from urllib2 import Request, urlopen, URLError, HTTPError


# The base-64 encoded, encrypted key (CiphertextBlob) stored in the kmsEncryptedHookUrl environment variable
# ENCRYPTED_HOOK_URL = os.environ['kmsEncryptedHookUrl']
# The Slack channel to send a message to stored in the slackChannel environment variable
SLACK_CHANNEL = os.environ['slackChannel']
PIPELINE      = os.environ['pipeline']

# HOOK_URL = "https://" + boto3.client('kms').decrypt(CiphertextBlob=b64decode(ENCRYPTED_HOOK_URL))['Plaintext']

HOOK_URL = "https://" + os.environ['kmsEncryptedHookUrl']

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    logger.info("Event: " + str(event))
    status = event['detail']['state']
    logger.info("Message: " + str(status))


    slack_message = {
        'channel': SLACK_CHANNEL,
        'text': "rspec run completed with a status of: %s" % (status)
    }
    color = "#36a64f" if status == "SUCCEEDED" else "#ff0000"
    title = "click here to see execution details"
    slack_message = {
        "attachments": [
            {
                "color": color,
                "title": title,
                "title_link": PIPELINE,
                "text": "completed with a status of: %s" % (status)
            }
        ]
    }

    req = Request(HOOK_URL, json.dumps(slack_message))
    try:
        response = urlopen(req)
        response.read()
        logger.info("Message posted")
    except HTTPError as e:
        logger.error("Request failed: %d %s", e.code, e.reason)
    except URLError as e:
        logger.error("Server connection failed: %s", e.reason)

