module RolloverReportGenerator
  # CF-627 PART I, III, IV, V
  class WithholdingsReport
    REPORT_HEADERS = [
      "person_id",
      "total_earnings_withheld",
      "pre_withholding_account_balance",
      "post_withholding_account_balance",
      "total_count_of_withholding_transactions",
      "total_invoice_transactions_amount",
      "total_recouped_amount",
      "amount_remitted_back_to_state",
      "active_tax_form_expiry_date",
      "person_status"
    ]

    attr_reader :report_data

    def initialize
      @report_data = {}
    end

    def run!
      generate_report_data
      write_to_disk!
    end

    private

    def generate_report_data
      [
        withheld_amount_data,
        pre_withholding_balance_data,
        post_withholding_account_balance_data,
        reported_amount_data,
        withholdings_count_data,
        grouped_current_year_transactions,
        remitted_to_state_amount,
        active_tax_form_expiry_date,
        person_status
      ].each do |queried_data|
        @report_data.merge!(queried_data) { |_person_id, dest, source| source.merge(dest) }
      end
    end

    def person_ids
      @person_ids ||= IRSTaxWithholding.joins(:withheld_from_person_transaction).pluck(:person_id).uniq
    end

    def person_transactions
      @person_transactions ||=
        PersonTransaction.where(
          created_at: DateTime.now.ago(1.year).beginning_of_year..DateTime.now,
          person_id: person_ids
        )
    end

    # { person_id { remitted_to_state_amount => 100 }}
    def remitted_to_state_amount
      @remitted_to_state_amount ||=
        BalanceAdjustment
        .where(
          person_id: person_ids,
          category: "Other",
          created_at:
          DateTime.now.ago(1.year).beginning_of_year..DateTime.now
        )
        .filter { |x| x.admin_note.match("Debit for Dormancy") }
        .group_by(&:person_id)
        .transform_values { |x| { remitted_to_state_amount: x.pluck(:debit_amount).sum } }
    end

    # { person_id => { active: true } }
    def person_status
      Person
        .where(id: person_ids)
        .reduce({}) { |acc, elem| acc.merge({ elem.id => { person_status: elem.status } }) }
    end

    # { person_id => { active_tax_form_expiry_date: "10-10-2022" } }
    def active_tax_form_expiry_date
      TaxForm.where(
        person_id: person_ids,
        tax_form_type_id: [2, 3]
      ).group_by(&:person_id).transform_values { |tax_forms|
        { active_tax_form_expiry_date: tax_forms.pluck(:expires_at).max.strftime("%m/%d/%Y") }
      }
    end

    # { person_id => { "Invoice" => [{ debit: 0, credit: 0}, { debit: 0, credit: 0}] } }
    def grouped_current_year_transactions
      @grouped_current_year_transactions ||=
        person_transactions
        .group_by(&:person_id)
        .transform_values do |transactions|
          transactions
            .group_by(&:target_type)
            .transform_values { |y| y.pluck(:debit).sum }
        end
    end

    def write_to_disk!
      raise "Report Data Not Generated!" if report_data.blank?

      File.open(report_file_meta[:filename], "w+") do |f|
        f.puts(REPORT_HEADERS.join(","))
        report_data.map do |person_id, person_data|
          f.puts [
            person_id,
            person_data[:withheld_amount],
            person_data[:pre_withholding_balance],
            person_data[:post_withholding_account_balance],
            person_data[:withholdings_count],
            person_data["Invoice"],
            person_data["EncumbranceDetail"],
            person_data[:remitted_to_state_amount],
            person_data[:active_tax_form_expiry_date],
            person_data[:person_status]
          ].join(",")
        end
      end
    end

    def report_file_meta
      {
        filename: [
          "tmp/tax_rollover_report_",
          "generated_on_#{DateTime.now.strftime('%D').tr('/', '_')}",
          "from_data_as_on_01_10_2022.csv"
        ].join("_")
      }
    end

    # {110111 => { reported_amount => 1000 } }
    def post_withholding_account_balance_data
      people_ids = IRSTaxWithholding.joins(:withheld_from_person_transaction).pluck(:person_id).uniq
      PersonBalance
        .where(person_id: people_ids)
        .pluck(:person_id, :balance)
        .group_by(&:shift)
        .transform_values { |data| { post_withholding_account_balance: data.flatten.sum } }
    end

    # {110111 => { reported_amount => 1000 } }
    def reported_amount_data
      people_ids = IRSTaxWithholding.joins(:withheld_from_person_transaction).pluck(:person_id).uniq
      PersonEarningsTaxMetadatum
        .where(person_id: people_ids, tax_year: "2021")
        .pluck("person_id", "total_taxable_distribution_earnings", "total_taxable_publishing_earnings")
        .group_by(&:shift)
        .transform_values { |data| { reported_amount: data.flatten.sum } }
    end

    # {110111 => { withholdings_count => 1000 } }
    def withholdings_count_data
      ::IRSTaxWithholding
        .joins(:withheld_from_person_transaction)
        .group("person_transactions.person_id")
        .count
        .transform_values { |data| { withholdings_count: data } }
    end

    # {110111 => { pre_withholding_balance => 1000 } }
    def pre_withholding_balance_data
      ::IRSTaxWithholding
        .joins(withheld_from_person_transaction: { person: :person_balance })
        .pluck("people.id", "person_balances.balance", "irs_tax_withholdings.withheld_amount")
        .group_by { |data| Array.new(2) { data.shift } }
        .each_with_object({}) do |data, object|
          object[data.dig(0, 0)] = { pre_withholding_balance: data.dig(1).flatten.sum + data.dig(0, 1) }
        end
    end

    # {110111 => { withheld_amount => 1000 } }
    def withheld_amount_data
      ::IRSTaxWithholding
        .joins(withheld_from_person_transaction: :person)
        .pluck("people.id", "irs_tax_withholdings.withheld_amount")
        .group_by(&:shift)
        .transform_values { |values| { withheld_amount: values.flatten.sum } }
    end
  end
end

RolloverReportGenerator::WithholdingsReport.new.run!
