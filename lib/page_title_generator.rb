module PageTitleGenerator

  def included(base)
    base.before_action :set_default_page_title
  end

  protected

  def set_default_page_title
    result = ""
    if ['new', 'edit'].include?(action_name)
      result << "#{action_name.titleize } "
      result << controller_name.singularize.titleize
    elsif ! ['index', 'update', 'create', 'destroy', 'show']
      result << controller_name.titleize
      result << " :: #{action_name.titleize}"
    else
      result << controller_name.titleize
    end
    @page_title ||=  result
  end

end
