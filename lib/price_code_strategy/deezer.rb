module PriceCodeStrategy
  module Deezer
    def self.price_code(salepoint)
      num_tracks = salepoint.salepointable.songs.size
      PriceCodeStrategy::Tunecore.price_band(6, num_tracks)
    end
  end
end
