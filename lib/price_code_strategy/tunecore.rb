module PriceCodeStrategy::Tunecore
  IMPLEMENTED_STORES = ["Deezer"]
  def self.available_strategy?(name)
    IMPLEMENTED_STORES.include?(name)
  end

  def self.price_band(threshold, num_tracks)
    if num_tracks < threshold
      "T#{num_tracks}"
    else
      "A1"
    end
  end
end
