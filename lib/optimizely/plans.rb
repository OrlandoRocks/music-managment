# frozen_string_literal: true

class Optimizely::Plans
  EMPTY     = {}
  PAGE_NAME = "Plans"

  def self.build_tag_data(_)
    EMPTY
  end
end
