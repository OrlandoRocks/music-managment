class Optimizely::Page
  attr_accessor :person, :page_type, :tag_data

  def initialize(person, options = {})
    @person = person
    @marketing_metric_opts = options[:marketing_metrics]
    # possible page types: Optimizely::Dashboard, Optimizely::Invoice, Optimizely::Album
    @page_type = options[:page_type]
    @page_opts = options.fetch(:page_opts, {})
    @albums = options[:albums]
    @options = options

    @tag_data = build_tag_data
    @tag_data
  end

  private

  # TODO: Audit this data and remove anything that is now unnecessary
  # It is currently half the page load time for heavy users.

  def build_tag_data
    @tag_data = {
      currency_code: @options[:currency],
      logged_in: "0"
    }
    merge_page_specific_data if @page_type.present?
    return @tag_data if @person.blank?

    failed_renewal = @person.optimizely_failed_renewal
    paid_for_publishing = @person.paid_for_publishing_composer? || @person.paid_for_composer?

    current_plan = @person.plan
    next_plan    = current_plan&.next_plan

    @tag_data.merge!(
      {
        dashboard_state: @person.dashboard_state(@albums),
        logged_in: "1",
        customer_id: @person.id.to_s,
        customer_email: @person.email,
        customer_country: @person.country,
        has_credits: @person.has_distribution_credits_available? ? "1" : "0",
        accessed_social: accessed_social,
        social_plan: [plan_status.plan, plan_status.plan_expires_at],
        any_release_without_YTAT: @person.releases_eligible_for_youtube_art_tracks_store? ? "1" : "0",
        failed_renewal: (failed_renewal ? "1" : "0"),
        paid_for_publishing: paid_for_publishing ? "1" : "0",
        spotify_skip_test: FeatureFlipper.show_feature?(:spotify_artists_required, @person) ? "1" : "0",
        current_plan_name: current_plan&.name,
        current_plan_id: current_plan&.id,
        next_eligible_plan_name: next_plan&.name,
        next_eligible_plan_id: next_plan&.id,
      }
    )
    return @tag_data unless failed_renewal

    @tag_data.merge!(
      {
        failed_release_name: failed_renewal.items.first.related.name,
        failed_release_renewal_date: Time.at(failed_renewal.expires_at).strftime("%b %d, %Y")
      }
    )
  rescue => err
    @tag_data.merge!(error: "#{@page_type}.build_user_data failed", error_details: err.to_s)
  end

  def plan_status
    @plan_status ||= ::Social::PlanStatus.for(person)
  end

  def accessed_social
    @accessed_social ||= OauthToken.exists?(
      user_id: person.id,
      client_application_id: ClientApplication.tc_social.id
    )
  end

  def merge_page_specific_data
    @tag_data.merge!(
      page_name: @options[:page_name],
      page_type: @page_type.to_s
    )
    @tag_data.merge!(@page_type.build_tag_data(@page_opts))
  end
end
