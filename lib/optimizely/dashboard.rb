class Optimizely::Dashboard
  def self.build_tag_data(opts={})
    @marketing_metrics = opts[:marketing_metrics]
    return {} if @marketing_metrics.blank?
    {
      no_release_no_credit: !@marketing_metrics.any_credits_available? && !@marketing_metrics.release_in_progress?,
      no_release_credit_available: @marketing_metrics.any_credits_available? && !@marketing_metrics.release_in_progress?
    }
  end
end
