class Optimizely::Invoice
  def self.build_tag_data(opts={})
    @marketing_metrics = opts[:marketing_metrics]
    return {} if @marketing_metrics.blank?
    { user_purchased_credit: @marketing_metrics.credit_in_last_invoice?  }
  end
end
