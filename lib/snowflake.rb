class Snowflake
  attr_reader :snowflake

  def self.fetch(query_str)
    new.fetch(query_str)
  end

  def initialize
    @snowflake = Sequel.odbc(drvconnect: connection_str)
  end

  def fetch(query_str)
    snowflake.fetch(query_str).all
  end

  private

  def connection_str
    [
      "Account=#{ENV["SNOWFLAKE_ACCOUNT"]};",
      "Database=#{ENV["SNOWFLAKE_DB_NAME"]};",
      "Driver=#{ENV["SNOWFLAKE_DRIVER"]};",
      "Role=TECHTEAM;",
      "Schema=DISTRIBUTIONS;",
      "Server=#{ENV["SNOWFLAKE_ACCOUNT"]}.snowflakecomputing.com;",
      "Warehouse=TC_WWW;",
      "pwd=#{ENV["SNOWFLAKE_PWD"]};",
      "uid=#{ENV["SNOWFLAKE_UID"]};",
    ].join("")
  end
end
