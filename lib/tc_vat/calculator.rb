# frozen_string_literal: true

module TcVat
  # A Service that can be used to for vat calculations
  class Calculator < TcVat::Base

    INBOUND_APPLICABLE_CACHE = 'vat_applicable_countries_inbound'
    OUTBOUND_APPLICABLE_CACHE = 'vat_applicable_countries'

    def initialize(purchase)
      @purchase = purchase
      @person = purchase.person
      @tax_info = {
        customer_location: country_name,
        vat_registration_number: @person.vat_information&.vat_registration_number
      }
      @errors = []
    end

    def perform!(cost, discount)
      @purchase.save && return unless feature_on?

      fetch_vat_rate
      @purchase.errors.add(:base, @errors.flatten.join(" "))

      return if @errors.present?

      update_purchase(cost, discount)
    end

    def self.fetch_vat_applicable_rate(transaction_type, customer_type, country)
      applicable_cache = transaction_type == TcVat::Base::INBOUND ? INBOUND_APPLICABLE_CACHE : OUTBOUND_APPLICABLE_CACHE
      applicable_countries = $redis.get(applicable_cache)
      applicable_country = JSON.parse(applicable_countries)[customer_type].find { |info| info['country'] == country }
      applicable_country.nil? ? 0.0 : applicable_country['tax_rate'].to_f
    end

    private

    def update_purchase(cost, discount)
      @purchase.vat_amount_cents = ((cost.to_f - discount.to_f) * tax_rate / 100.0).round
      @purchase.save

      return unless feature_on? && @purchase.errors.blank?

      @purchase.build_purchase_tax_information(@tax_info).save
    end

    def tax_rate
      return 0 unless feature_on?

      @tax_info.fetch(:tax_rate, 0)
    end
  end
end
