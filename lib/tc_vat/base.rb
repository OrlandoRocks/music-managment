# frozen_string_literal: true

module TcVat
  # A Service that can be used to write the common vat methods.
  class Base
    INDIVIDUAL = 'Individual'
    BUSINESS = 'Business'
    INBOUND = 'Inbound'
    OUTBOUND = 'Outbound'

    def fetch_vat_rate
      # treat all invalid tax numbers as individuals
      customer_type = validate_vat_number ? BUSINESS : INDIVIDUAL
      response = TcVat::FailSafe::Calculator.new(@person, customer_type)
      @errors << response.errors if response.errors.present?

      @tax_info.tap do |t_info|
        t_info.merge!(customer_type: customer_type,
                      tax_rate: response.tax_rate,
                      tax_type: response.tax_type,
                      place_of_supply: response.place_of_supply)

        next if response.error_code.blank?

        t_info.merge!(error_code: response.error_code)
      end
    end

    def validate_vat_number
      response = TcVat::FailSafe::Validator.new(@person)
      @errors << response.errors if response.errors.present?

      @tax_info.merge!(
        trader_name: response.trader_name,
        error_code: response.error_code,
        note: response.note
      )

      response.valid
    end

    def vat_applicable_countries
      countries = $redis.get('vat_applicable_countries')
      return JSON.parse(countries) if countries.present?

      response = api_client.send_request!(:applicable, {})
      $redis.set('vat_applicable_countries', response.data.to_json)
      response.data
    end

    def feature_on?
      FeatureFlipper.show_feature?(:vat_tax, @person)
    end

    def api_client
      @api_client ||= TcVat::ApiClient.new
    end

    def country_name
      @country_name ||= @person.country_name_untranslated
    end
  end
end
