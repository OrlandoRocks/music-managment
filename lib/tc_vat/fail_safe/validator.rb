module TcVat::FailSafe
  # A Service that can be used to for validating vat registration number
  class Validator
    include ActionView::Helpers::TranslationHelper
    include CustomTranslationHelper

    HISTORIC_CLASSES = Set[VatInformation,
                           PurchaseTaxInformation,
                           VatTaxAdjustment].freeze
    VIES_ERROR_CODE = "VIES System failure - Used the previous record".freeze
    VAT_SERVICE_ERROR_CODE = "VAT Service Down - Used the previous record".freeze

    attr_reader :trader_name, :valid, :error_code, :note, :errors

    def initialize(person, vat_params = {})
      @person = person
      country_code = vat_params[:country_code] || @person[:country]
      @country = Country.find_by(iso_code: country_code)&.name_raw
      @vat_registration_number = vat_params[:vat_registration_number] || @person.vat_information&.vat_registration_number
      @valid = false
      @errors = []
      process!
    end

    def valid?
      valid
    end

    private

    def process!
      invoke_api if @vat_registration_number.present?
    rescue TcVat::Requests::VatServiceDownError, TcVat::Requests::ViesDownError => e
      status = perform_fail_over!(e)
      @errors << "vat.errors.vat_service_down" unless status
      Tunecore::Airbrake.notify(humanized_errors, e)
    end

    def invoke_api
      @response = TcVat::ApiClient.new.send_request!(:validate, validate_params)
      @trader_name = @response.name
      @valid = @response.valid?
      @error_code = @response.error_message
    end

    def validate_params
      {
        vat_registration_number: @vat_registration_number,
        country: @country
      }
    end

    def historic_params
      [@person.id, @vat_registration_number, @country]
    end

    def perform_fail_over!(err)
      record = nil
      HISTORIC_CLASSES.each do |klass|
        record = klass.send(:historic_record, *historic_params)
        process_historic_record(record, klass, err) && break if (record.present? && record[:valid])
      end

      record.present?
    end

    def process_historic_record(record, klass, err)
      @error_code = err.is_a?(TcVat::Requests::ViesDownError) ? VIES_ERROR_CODE : VAT_SERVICE_ERROR_CODE
      @error_code += (", " + err.message) if err.message.present?

      @valid = record[:valid]
      @trader_name = record[:trader_name]
      @note = "#{klass} ID: #{record[:id]}"
    end

    def humanized_errors
      @errors.map { |e| custom_t(e) }.join(", ")
    end
  end
end
