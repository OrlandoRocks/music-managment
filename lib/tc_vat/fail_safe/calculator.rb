# frozen_string_literal: true

module TcVat::FailSafe
  # A Service that can be used to for validating vat registration number
  class Calculator
    INBOUND = 'Inbound'
    VAT_SERVICE_ERROR = 'Vat Service Down - Used cache to calculate VAT'
    DEFAULT_PLACE_OF_SUPPLY = 'Luxembourg'
    DEFAULT_TAX_TYPE = 'VAT'

    attr_reader :place_of_supply, :tax_rate, :tax_type, :error_code, :errors

    def initialize(person, customer_type, vat_params = {})
      @person = person
      country_code = vat_params[:country_code] || @person[:country]
      country = Country.find_by(iso_code: country_code)
      @corporate_entity = country.corporate_entity.name
      @country_name = country.name_raw
      @customer_type = customer_type
      @errors = []
      process!
    end

    private

    def process!
      invoke_api!
    rescue TcVat::Requests::VatServiceDownError, TcVat::Requests::ViesDownError => e
      perform_fail_over!
      Tunecore::Airbrake.notify(@errors.join(','), e)
    end

    def calculate_params
      {
        country: @country_name,
        corporate_entity: @corporate_entity,
        customer_type: @customer_type,
        transaction_type: INBOUND
      }
    end

    def invoke_api!
      @response = TcVat::ApiClient.new.send_request!(:calculate, calculate_params)
      @place_of_supply = choose_place_of_supply(@response.place_of_supply)
      @tax_rate = @response.tax_rate.to_f
      @tax_type = choose_tax_type(@response.tax_type)
      @error_code = @response.error_message
    end

    def perform_fail_over!
      cached_data = applicable_countries
      @errors << 'Unable to process VAT' if cached_data.nil?
      return if @errors.present?

      process_cache(cached_data)
    end

    def process_cache(data)
      record = data.fetch(@customer_type.downcase, []).find { |i| i['country'] == @country_name } || {}
      @place_of_supply = choose_place_of_supply(record['place_of_supply'])
      @tax_rate = record['tax_rate'].to_f
      @tax_type = choose_tax_type(record['tax_type'])
      @error_code = VAT_SERVICE_ERROR
    end

    def applicable_countries
      data = $redis.get('vat_applicable_countries_inbound')
      return if data.nil?

      JSON.parse(data)
    end

    def choose_place_of_supply(place_of_supply)
      return place_of_supply unless place_of_supply.nil? && @person.affiliated_to_bi?

      DEFAULT_PLACE_OF_SUPPLY
    end

    def choose_tax_type(tax_type)
      return tax_type unless tax_type.nil? && @person.affiliated_to_bi?

      DEFAULT_TAX_TYPE
    end
  end
end
