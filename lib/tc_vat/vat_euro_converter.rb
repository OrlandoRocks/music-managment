# frozen_string_literal: true

module TcVat
  class VatEuroConverter < TcVat::Base
    EUR = "EUR"

    def initialize(invoice:)
      @invoice = invoice
      @currency = @invoice.currency
      @balance_rate = ForeignExchangeRate.latest_by_currency(source: @currency,
                                                             target: EUR)
    end

    def convert_to_euro!
      return unless @balance_rate && feature_on?

      add_euro_vat_amount_to_purchases!
      @invoice.update(
        foreign_exchange_rate_id: @balance_rate.id,
        vat_amount_cents_in_eur: invoice_vat_amount_in_eur
      )
    end

    private

    def paid_purchases
      @invoice.purchases.paid
    end

    def add_euro_vat_amount_to_purchases!
      paid_purchases.each do |purchase|
        purchase.update(
          vat_amount_cents_in_eur: amount_in_euro(amount: purchase.vat_amount_cents)
        )
      end
    end

    def invoice_vat_amount_in_eur
      paid_purchases.sum(:vat_amount_cents_in_eur)
    end

    def amount_in_euro(amount:)
      amount * @balance_rate.exchange_rate
    end
  end
end
