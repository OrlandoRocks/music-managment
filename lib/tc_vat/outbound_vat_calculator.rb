# frozen_string_literal: true

module TcVat
  # A Service that can be used to for outbound vat calculations
  class OutboundVatCalculator < TcVat::Base
    BUSINESS = 'business'
    INDIVIDUAL = 'individual'
    VAT_TAX = "VAT"

    attr_accessor :person, :tax_info

    def initialize(person)
      @person = person
      @tax_info = {
        customer_location: person.country_name_untranslated,
        vat_registration_number: person.vat_information&.vat_registration_number
      }
      @errors = []
    end

    def fetch_vat_info
      return {} unless person.vat_applicable?

      # treat all invalid tax numbers as individuals
      customer_type = validate_vat_number ? BUSINESS : INDIVIDUAL
      begin
        countries = vat_applicable_countries
      rescue TcVat::Requests::VatServiceDownError
        tax_info[:errors] = 'vat.errors.vat_service_down'

        return tax_info
      end

      country_tax_rate = countries[customer_type].find do |c|
        c['country'] == person.country_name_untranslated && c['tax_rate'].to_f.positive?
      end

      return {} if country_tax_rate.nil?

      tax_info.tap do |t_info|
        t_info.merge!(customer_type: customer_type,
                      tax_rate: country_tax_rate['tax_rate'],
                      place_of_supply: country_tax_rate['place_of_supply'],
                      tax_type: VAT_TAX)
      end
    end
  end
end
