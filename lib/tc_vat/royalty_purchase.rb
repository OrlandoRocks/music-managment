module TcVat
  class RoyaltyPurchase
    attr_accessor :person, :invoice, :balance, :amount, :vat_tax_adjustment, :outbound_invoice, :vat_info, :balance_rate

    def initialize(person, invoice, balance)
      @person = person
      @invoice = invoice
      @balance = Tunecore::Numbers.decimal_to_cents(balance)
      @amount = [@balance, @invoice.outstanding_amount_cents].min
      @vat_info = OutboundVatCalculator.new(person).fetch_vat_info
      @balance_rate = ForeignExchangeRate.latest_by_currency(source: @person.currency,
                                                             target: CurrencyCodeType::EUR)
    end

    def process_vat!
      return if vat_info.blank?
      raise vat_info[:errors] if vat_info[:errors].present?

      amount_from_balance = [balance, invoice.outstanding_amount_cents].min
      base_amount = (amount_from_balance * 100 / (100.0 + tax_rate)).round.to_i
      vat_amount = (base_amount * tax_rate / 100.0).round.to_i

      if invoice.outstanding_amount_cents > balance
        vat_amount = (amount_from_balance * tax_rate / 100.0).round.to_i
        amount_from_balance += vat_amount
      end
      post_outbound_vat(vat_amount)
      @amount = amount_from_balance
    end

    def generate_outbound_invoice(settlement)
      return if settlement.blank? || !person.vat_applicable?

      settlement.create_outbound_invoice(
        person: person,
        vat_tax_adjustment: vat_tax_adjustment,
        invoice_date: Time.now.utc,
        currency: invoice.currency,
        user_invoice_prefix: person.outbound_invoice_prefix
      )
      update_vat_tax_related(settlement)
    end

    private

    def tax_rate
      vat_info[:tax_rate].to_f
    end

    def create_vat_tax_adjustment(vat_amount_cents)
      @vat_tax_adjustment = VatTaxAdjustment.create(
        person: person,
        amount: vat_amount_cents,
        tax_rate: tax_rate,
        related: invoice,
        vat_amount_in_eur: vat_amount_in_eur(vat_amount_cents),
        foreign_exchange_rate: balance_rate,
        vat_registration_number: vat_info[:vat_registration_number],
        tax_type: vat_info[:tax_type],
        trader_name: vat_info[:trader_name],
        place_of_supply: vat_info[:place_of_supply],
        error_message: vat_info[:error_code],
        customer_type: vat_info[:customer_type]
      )
    end

    def post_outbound_vat(vat_amount_cents)
      create_vat_tax_adjustment(vat_amount_cents)
      PersonTransaction.create!(person: person,
                                debit: 0,
                                credit: Tunecore::Numbers.cents_to_decimal(vat_amount_cents),
                                target: vat_tax_adjustment,
                                comment: 'VAT Posted')
    end

    def update_vat_tax_related(invoice_settlement)
      return if vat_tax_adjustment.nil?

      vat_tax_adjustment.update(related: invoice_settlement)
    end

    def vat_amount_in_eur(amount)
      return amount * balance_rate.exchange_rate if balance_rate.present?

      amount
    end
  end
end
