# frozen_string_literal: true

module TcVat
  # A Service that can be used to for batch vat calculations
  class BulkVatCalculator < TcVat::Base
    def initialize(person)
      @errors = []
      @person = person
      @country = Country.find_by(name: @person.country)
      @tax_info = {
        customer_location: @country.try(:name),
        vat_registration_number: @person.vat_information&.vat_registration_number
      }
    end

    def calculate!(purchase_ids)
      return if purchase_ids.blank? || !feature_on?

      fetch_vat_rate
      bulk_update_purchase(purchase_ids)
      bulk_update_purchase_tax_info(purchase_ids)
    end

    private

    def bulk_update_purchase(purchase_ids)
      Purchase.where(id: purchase_ids)
              .update_all("vat_amount_cents = ((IFNULL(cost_cents, 0) - IFNULL(discount_cents, 0)) * #{tax_rate})")
    end

    def tax_rate
      @tax_info[:tax_rate].to_f / 100
    end

    def bulk_update_purchase_tax_info(purchase_ids)
      # Destroying the existing records since this is a One to one relation
      PurchaseTaxInformation.where(purchase_id: purchase_ids).destroy_all

      PurchaseTaxInformation.create(tax_info_attributes(purchase_ids))
    end

    def tax_info_attributes(purchase_ids)
      purchase_ids.map { |purchase_id| { purchase_id: purchase_id }.merge(@tax_info) }
    end
  end
end
