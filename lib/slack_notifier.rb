# frozen_string_literal: true

require 'uri'
require 'json'
require 'net/http'
require 'net/https'

ALERTS_CHANNEL_NAME = "alerts"
TECH_DEPLOY_CHANNEL_NAME = "tech_deploy"
SIP_DEV_CHANNEL_NAME = "tech_sip_dev"

WEBHOOK_CONFIG = [
  {
     channel:ALERTS_CHANNEL_NAME,
     webhook:"SLACK_WEBHOOK"
  },
  {
    channel:TECH_DEPLOY_CHANNEL_NAME,
    webhook:"SLACK_WEBHOOK_TECH_DEPLOY_CHANNEL"
  },
  {
    channel:SIP_DEV_CHANNEL_NAME,
    webhook:"SLACK_WEBHOOK_SIP_DEV_CHANNEL"
  }
]

def notify_in_slack(msg, t_type, channel=ALERTS_CHANNEL_NAME)
  slack_webhook = fetch_webhook(channel)
  return if slack_webhook.nil? || slack_webhook.empty?

  uri = URI.parse(slack_webhook)
  environment = defined?(Rails) ? Rails.env : "from_script"
	request_body = {
	  :text => msg,
	  :username => t_type+"-"+environment,
	  :icon_emoji => ":loudspeaker:"
	}
  https = Net::HTTP.new(uri.host,uri.port)
  https.use_ssl = true
  req = Net::HTTP::Post.new(uri.request_uri)
  req.body = request_body.to_json
  req.content_type = 'application/json'
  https.request(req)
end

def fetch_webhook(channel)
  config = WEBHOOK_CONFIG.find {|config| config[:channel] == channel}[:webhook]
  ENV.fetch(config, nil)
end
