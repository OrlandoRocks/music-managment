module Logging
  # log method used by the rake task associated with credit card batch processing
  def log(category = "TuneCore", comment = "", options = {})
    # ensure that the log is written immediately, without being buffered
    prior_stdout_sync_value = STDOUT.sync
    STDOUT.sync = true

    logged = "#{category} | #{comment} |"

    if !options.empty?
      options.each_pair do |option, value|
        logged << " #{option}=#{value}"
      end
    end

    Rails.logger.info  logged

    if Rails.env.development?
      puts logged # use this for debugging
    end

    # for good measure, flush anything remaining in the logger, if possible
    Rails.logger.try(:flush)

    # restore stdout.sync
    STDOUT.sync = prior_stdout_sync_value

    return logged
  end

  # outputs a time elapsed in seconds
  def seconds_since(start_time)
    sprintf('%.4f', (Time.now - start_time).to_f)
  end

  # outputs a time elapsed for rake tasks (rounds to 1 second, so not useful for page loads or anythin requiring ms)
  def pretty_time_since(start_time)
    time_spent = Time.now - start_time
    [time_spent/3600, time_spent % 3600 / 60, time_spent % 60].map{|t| "%02d" % t.floor }.join(':')
  end
end
