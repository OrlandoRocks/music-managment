#### PayPal API
#### Created by Dan Drabik
#### Created for TuneCore
#### April, 29, 2010

#### Provides a wrapper to the Paypal Ruby SDK Express Checkout to make calls simpler.
#### all calls to the PayPalAPI return a PayPalSDKCallers::Transaction which has .success?, .response methods
#
##### AVAILABLE METHODS ########
#
# PayPalAPI.send_to_checkout(host, port, params = {})
# Send a customer with purchase information to the paypal site to enter their credentials
#
# PayPalAPI.get_checkout_details(token, payerid)
# Get purchase details back from paypal
#
# PayPalAPI.process(token, payerid, action, amount, currency)
# Process Transaction with paypal
#
require 'paypal_sdk/caller'
require 'paypal_sdk/paypal_utils'
class PayPalAPI < PayPalSDKCallers::Transaction
  extend PayPalSDKUtils
  # For systems with only one paypal_account, we could set the credetials statically, and not pass them in on each request
  #PAYPAL_CREDENTIALS = PayPalSDKProfiles::Profile.credentials

  # Send a customer with purchase information to the paypal site to enter their credentials
  # the items parameter expects an array of hashes of the form {:name=> "Name", :amt => 5, :qty => 1}
  def self.send_to_checkout(config, cancel_url, return_url, action, currency, items = [], digital = false)
    amount =  "%0.2f" % quantity_amount_sum(items) #calculates sum and forces 2 decimal places
    caller =  PayPalSDKCallers::Caller.new(false)
    req = { :METHOD                         => 'SetExpressCheckout',
            :PAYMENTREQUEST_0_PAYMENTACTION => action,
     	      :PAYMENTREQUEST_0_CURRENCYCODE  => currency,
     	      :CANCELURL                      => cancel_url,
            :RETURNURL                      => return_url,
            :NOSHIPPING                     => '1',
            :REQCONFIRMSHIPPING             => '0',
 	          :PAYMENTREQUEST_0_ITEMAMT       => amount,
 	          :PAYMENTREQUEST_0_AMT				    => amount,
 	          :MAXAMT				                  => amount,
 	          :L_BILLINGTYPE0                 => 'MerchantInitiatedBilling'
     }.merge(credentials_from_config(config)).merge(purchase_item_parameters(items, digital))

    #Only allow instance payments for sale transactions
    req[:PAYMENTREQUEST_0_ALLOWEDPAYMENTMETHOD] = "InstantPaymentOnly" if action == 'Sale'
    caller.call(req)
  end

  # Get purchase details back from paypal
  def self.get_checkout_details(config, token, payerid)
    caller =  PayPalSDKCallers::Caller.new(false)
    caller.call({
        :METHOD  => 'GetExpressCheckoutDetails',
        :TOKEN   => token,
        :PAYERID => payerid
      }.merge(credentials_from_config(config)))
  end

  # Process Transaction with paypal
  def self.process(config, token, payerid, action, amount, currency)
    caller =  PayPalSDKCallers::Caller.new(false)
    options = {
        :METHOD                                => 'DOExpressCheckoutPayment',
        :TOKEN                                 => token,
        :PAYERID                               => payerid,
        :PAYMENTREQUEST_0_AMT                  => amount.to_s,
        :PAYMENTREQUEST_0_CURRENCYCODE         => currency,
        :PAYMENTREQUEST_0_PAYMENTACTION        => action,
        :L_BILLINGTYPE0                        => 'MerchantInitiatedBilling'
      }.merge(credentials_from_config(config))

    options[:PAYMENTREQUEST_0_ALLOWEDPAYMENTMETHOD] = "InstantPaymentOnly" if action == 'Sale'
    caller.call(options)
  end

  # Process Reference Transaction with paypal
  def self.reference_transaction(config, original_transaction_id, currency, items = [])
    amount =  "%0.2f" % quantity_amount_sum(items)
    caller =  PayPalSDKCallers::Caller.new(false)
    caller.call({
        :METHOD         => 'DoReferenceTransaction',
        :REFERENCEID    => original_transaction_id,
        :AMT				    => amount,
        :CURRENCYCODE   => currency,
        :PAYMENTACTION  => 'Sale',
        :PAYMENTTYPE    => 'InstantOnly',
      }.merge(credentials_from_config(config)).merge(purchase_item_parameters(items)))
  end

  # Refund Transaction with paypal
  def self.refund(config, original_transaction_id, note = "Failed Transaction: Refunding Payment")
    caller =  PayPalSDKCallers::Caller.new(false)
    caller.call({
        :METHOD          => 'RefundTransaction',
        :TRANSACTIONID => original_transaction_id,
        :REFUNDTYPE		   => 'Full',
        :NOTE            => note
      }.merge(credentials_from_config(config)))
  end

  # Partial Refund the transaction
  def self.partial_refund(config, original_transaction_id, amount, note)
    PayPalSDKCallers::Caller
      .new(false)
      .call({
        METHOD: 'RefundTransaction',
        TRANSACTIONID: original_transaction_id,
        REFUNDTYPE: 'Partial',
        AMT: amount,
        NOTE: note
      }.merge(credentials_from_config(config)))
  end

  # Process Transaction with paypal
  def self.void(config, original_transaction_id, note = "Voiding Authorization")
    caller =  PayPalSDKCallers::Caller.new(false)
    caller.call({
        :METHOD          => 'DoVoid',
        :AUTHORIZATIONID => original_transaction_id,
        :TRXTYPE				 => 'V',
        :NOTE            => note
      }.merge(credentials_from_config(config)))
  end

  def self.credentials_from_config(config)
    return config unless config.is_a?(PayinProviderConfig)
    {
      "USER" => config.username,
      "PWD" => config.decrypted_password,
      "SIGNATURE" => config.decrypted_signature
    }
  end
end
