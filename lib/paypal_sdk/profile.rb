module PayPalSDKProfiles
  class Profile
    cattr_accessor :credentials
    cattr_accessor :checkout_return_url
    cattr_accessor :checkout_cancel_url
    cattr_accessor :stored_return_url
    cattr_accessor :stored_cancel_url
    cattr_accessor :endpoints
    cattr_accessor :client_info
    cattr_accessor :proxy_info
    cattr_accessor :PAYPAL_EC_URL
    cattr_accessor :PAYPAL_DG_URL
    cattr_accessor :DEV_CENTRAL_URL
    cattr_accessor :unipay
    cattr_accessor :pp_config

    @@config = ::PAYPAL_CONFIG

    @@PAYPAL_EC_URL=@@config['EC_URL']
    @@PAYPAL_DG_URL=@@config['DG_URL']
    #
    @@DEV_CENTRAL_URL="https://developer.paypal.com"
    ###############################################################################################################################
    #    NOTE: Production code should NEVER expose API credentials in any way! They must be managed securely in your application.
    #    To generate a Sandbox API Certificate, follow these steps: https://www.paypal.com/IntegrationCenter/ic_certificate.html
    ###############################################################################################################################
    # specify the 3-token values.

    # credentials for 3 token
    # You can use static credentials if only using one paypal account, but you need to pass in config
    #@@credentials =  {"USER" => @@config['USER'], "PWD" => @@config['PWD'], "SIGNATURE" => @@config['SIGNATURE'] }
    @@checkout_return_url = @@config['CHECKOUT_RETURN_URL']
    @@checkout_cancel_url = @@config['CHECKOUT_CANCEL_URL']
    @@stored_return_url = @@config['STORED_RETURN_URL']
    @@stored_cancel_url = @@config['STORED_CANCEL_URL']

    # endpoint of PayPal server against which call will be made. For 3 token
    @@endpoints = {"SERVER" => @@config['SERVER'], "SERVICE" => @@config['SERVICE']}

    # Information needed for tracking purposes.

    @@proxy_info = {"USE_PROXY" => @@config['USE_PROXY'], "ADDRESS" => @@config['PROXY_ADDRESS'], "PORT" => @@config['PROXY_PORT'], "USER" => @@config['PROXY_USER'], "PASSWORD" => @@config['PROXY_PASSWORD'] }
    @@client_info = { "VERSION" => @@config['VERSION'], "SOURCE" => @@config['SOURCE']}

    def initialize
      config
    end

    def config
      @config = @@config
    end

    def m_use_proxy
      @config['USE_PROXY']
    end
  end
end



