
require 'singleton'
# The module has a classes and a class method to intialize a logger and to specify formatting style using log4r library. 
module PayPalSDKUtils 
   def mask_data(data)
      data1=data.sub(/PWD=[^&]*\&/,'PWD=XXXXXX&')
      data2=data1.sub(/acct=[^&]*\&/,'acct=XXXXXX&')
      data3=data2.sub(/SIGNATURE=[^&]*\&/,'SIGNATURE=XXXXXX&')
      data4=data3.sub(/cvv2=[^&]*\&/,'cvv2=XXXXXX&')           
    end
    
  # Method to convert a hash to a string of name and values delimited by '&' as name1=value1&name2=value2...&namen=valuen.
  def hash2cgiString(h)
  #  h.map { |a| a.join('=') }.join('&') 
    h.each { |key,value| h[key] = CGI::escape(value.to_s) if (value) }   
    h.map { |a| a.join('=') }.join('&')

  end
# Class has a class method which returs the logger to be used for logging.
# All the requests sent and responses received will be logged to a file (filename passed to getLogger method) under logs directroy.
  
  ### Calculate the the sum amount for items passed in the form [{:name=> "Name", :amt => 5, :qty => 1}, {:name=> "Name 2", :amt => 25, :qty => 2}] 
  # used in the paypal_api.rb
  def quantity_amount_sum(items = [])
    return 0.0 if items.empty?
    items.inject(0) { |sum,item| sum += item[:amt].to_f*item[:qty].to_f}    
  end
  
  ### Convert items in the form [{:name=> "Name", :amt => 5, :qty => 1}, {:name=> "Name 2", :amt => 25, :qty => 2}] to valid paypal parameters

  def purchase_item_parameters(items = [], digital = false)
    return nil if items.empty?
    parameter_group = Hash.new
    items.each_with_index do |item, index|
      name  = ('L_PAYMENTREQUEST_0_NAME' << index.to_s).to_sym
      amt   = ('L_PAYMENTREQUEST_0_AMT'  << index.to_s).to_sym
      qty   = ('L_PAYMENTREQUEST_0_QTY'  << index.to_s).to_sym
      parameter_group.merge!({name => item[:name], amt => "%0.2f" % item[:amt], qty => item[:qty]})
      if digital
        category = ('L_PAYMENTREQUEST_0_ITEMCATEGORY'  << index.to_s).to_sym
        parameter_group.merge!({category => 'Digital'})
      end
    end
    return parameter_group
  end
end