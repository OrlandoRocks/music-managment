class MagicNumber
  # ripped from https://github.com/adrian-fernandez/magic_number/blob/master/lib/magic_number.rb

  SIGNATURES = {
    "mp3" => {
      'beginning_byte_matches' => [
        ["FFFA", 2],
        ["fffa", 2],
        ["FFFB", 2],
        ["fffb", 2],
        ["4944", 2],
        ["494433", 3],
      ],
    },
  }.freeze

  def self.is_real?(file_path, args = {})
    file = File.new(file_path, 'r')

    extension = args[:extension] || file.extension.downcase

    signature = MagicNumber.get_signature(extension)

    real = MagicNumber.check_begin_sign(file, signature)

    return real
  end

  def self.get_signature(ext)
    return SIGNATURES[ext]
  end

  def self.check_begin_sign(file, signature)
    if signature.has_key?("beginning_byte_matches")
      signature['beginning_byte_matches'].each do |byte_matcher_and_length|
        byte_matcher = byte_matcher_and_length[0]
        byte_length = byte_matcher_and_length[1]

        byte = MagicNumber.read_beginning_bytes(file, byte_length)

        return true if (byte == byte_matcher)
      end

      return false
    else
      return true
    end
  end

  def self.read_beginning_bytes(file, length)
    file.rewind
    return file.readpartial(length).unpack("H*").first
  end
end
