class Sift::SerializationError < StandardError
    attr_accessor :json

    def initialize(message = nil, json = nil)
        super(message)
        self.json = json
    end
end
