class RewardSystem::Eligibility::Person
  attr_accessor :record

  def initialize(person_id)
    @record = ::Person.find(person_id)
  end

  def invalid?
    @record.blank?
  end

  def current_tier_record
    @record.reload.tiers.parents_only.active.order(hierarchy: :desc).first
  end

  def next_tier
    current_hierarchy = current_tier_record&.hierarchy || 0

    next_tier_record = Tier
      .parents_only
      .active.where('hierarchy > ?', current_hierarchy)
      .order(:hierarchy)
      .first

    return if next_tier_record.blank?
    next_tier_record.handler_service
  end

  def promote_to_tier(tier)
    return if @record.tier_ids.include?(tier.id)

    @record.tiers << tier
  end
end
