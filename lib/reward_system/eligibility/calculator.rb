class RewardSystem::Eligibility::Calculator
  attr_accessor :eligibility_person

  def initialize(person_id)
    @eligibility_person = RewardSystem::Eligibility::Person.new(person_id)
  end

  def check_and_upgrade
    next_tier = @eligibility_person.next_tier

    return if next_tier.blank?
    return if !next_tier.is_person_eligible?(@eligibility_person.record)

    upgraded_tier = @eligibility_person.promote_to_tier(next_tier.record)

    return if upgraded_tier.blank?

    check_and_upgrade
  end
end
