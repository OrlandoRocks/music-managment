class RewardSystem::Events
  EARNINGS_UPDATED = 'earnings_updated'
  RELEASE_APPROVED = 'release_approved'
  NEW_USER = 'new_user'

  def self.earnings_updated(person_id)
    self.enqueue_event_worker(person_id, EARNINGS_UPDATED)
  end

  def self.release_approved(person_id)
    self.enqueue_event_worker(person_id, RELEASE_APPROVED)
  end

  def self.new_user(person_id)
    self.enqueue_event_worker(person_id, NEW_USER)
  end

  def self.enqueue_event_worker(person_id, event_name)
    RewardSystem::TierEligibilityEventWorker.perform_async(
      person_id,
      event_name
    )
  end
end
