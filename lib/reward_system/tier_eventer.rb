class RewardSystem::TierEventer
  def initialize(event_name)
    @event_name = event_name
  end

  def register_event(person_id)
    TierEventLog.create(event: @event_name, person_id: person_id)
    RewardSystem::Eligibility::Calculator.new(person_id).check_and_upgrade
  end
end
