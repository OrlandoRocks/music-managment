class RewardSystem::Tiers::Base
  attr_accessor :record

  def initialize(record)
    @record = record
  end

  def is_person_eligible?(person)
    raise "Must implement is_person_eligible?"
  end

  def threshold_criteria_met?(person)
    threshold = get_threshold(person)

    lte_meets_threshold?(threshold, person) || release_meets_threshold?(threshold, person)
  end

  def get_threshold(person)
    threshold = @record.tier_thresholds.find_by(country_id: person.country_website.country_id)

    return threshold if threshold.present?

    us_id = Country.us_only.first.id
    @record.tier_thresholds.find_by(country_id: us_id)
  end

  def lte_meets_threshold?(threshold, person)
    return false if threshold.blank?

    person.lifetime_earning_amount >= threshold.lte_min_amount
  end

  def release_meets_threshold?(threshold, person)
    return false if threshold.blank?

    person.reward_eligible_releases.count >= threshold.min_release
  end
end
