class RewardSystem::Tiers::RisingArtist < RewardSystem::Tiers::Base
  def is_person_eligible?(person)
    threshold = get_threshold(person)

    release_meets_threshold?(threshold, person)
  end
end
