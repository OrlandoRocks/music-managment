class RewardSystem::Tiers::Legend < RewardSystem::Tiers::Base
  def is_person_eligible?(person)
    threshold = get_threshold(person)

    lte_meets_threshold?(threshold, person)
  end
end
