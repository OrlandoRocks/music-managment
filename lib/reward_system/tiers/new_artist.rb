class RewardSystem::Tiers::NewArtist < RewardSystem::Tiers::Base
  def is_person_eligible?(person)
    person.present?
  end
end
