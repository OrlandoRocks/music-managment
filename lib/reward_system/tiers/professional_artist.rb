class RewardSystem::Tiers::ProfessionalArtist < RewardSystem::Tiers::Base
  def is_person_eligible?(person)
    threshold_criteria_met?(person)
  end
end
