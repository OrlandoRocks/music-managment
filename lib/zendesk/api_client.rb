module Zendesk
  class ApiClient
    BASE_URL = ENV["ZENDESK_API_HOST"]
    ZENDESK_USERNAME = ENV["ZENDESK_API_USERNAME"]
    ZENDESK_PASSWORD = ENV["ZENDESK_API_PASSWORD"]
    ENDPOINTS = {
      CREATE_OR_UPDATE: {endpoint: "api/v2/users/create_or_update_many.json", method: 'post'}
    }

    def initialize
      @http_client = Faraday.new(BASE_URL)
      set_basic_auth
    end

    def post(endpoint, call_args = {})
      http_client.post(endpoint, call_args)
    end

    def get(endpoint, call_args = {})
      http_client.get(endpoint, call_args)
    end

    def put(endpoint, call_args = {})
      http_client.put(endpoint, call_args)
    end

    private

    attr_reader :http_client

    def set_basic_auth
      http_client.basic_auth(
        ZENDESK_USERNAME,
        ZENDESK_PASSWORD
      )
    end
  end
end
