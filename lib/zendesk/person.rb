# frozen_string_literal: true

module Zendesk
  class Person
    API_ATTRIBUTES = [
      "name",
      "email",
      "phone_number",
      "status",
      "country"
    ].freeze

    def self.create_apilogger(person_id, attribute_changes = {})
      person = ::Person.find(person_id)

      options = {
        external_service_name: 'Zendesk',
        requestable_id: person.id,
        requestable_type: 'Person',
        request_body: create_client_data(person, attribute_changes),
        code_location: [__FILE__, __LINE__],
        state: 'enqueued'
      }
      ApiLogger::CreateLoggerService.new(options).log
    end

    def self.contains_zendesk_attributes?(attribute_array)
      !(attribute_array & API_ATTRIBUTES).empty?
    end

    def self.create_client_data(person, attribute_changes = {})
      data = {
        name: person.name,
        email: person.email,
        phone: person.phone_number,
        external_id: person.id,
        user_fields:
          {
            account_status: person.status,
            country: person[:country],
            plan_level: person.person_plan&.name,
            plan_expires: person.person_plan&.renewal_expires_at,
            unlimited_plan_level: person.person_plan&.name
          }
      }
      return data unless attribute_changes.keys.include?(:email)
      data.merge!(previous_email: attribute_changes[:email].first)
    end
  end
end
