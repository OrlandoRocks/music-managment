module TrendsAggregateable
  def aggregate_trend_data
    nintey_days = 90.days.ago.to_date.strftime("%Y-%m-%d")
    thirty_days = 30.days.ago.to_date.strftime("%Y-%m-%d")
    seven_days  = 7.days.ago.to_date.strftime("%Y-%m-%d")
    end_date    = Date.yesterday.strftime("%Y-%m-%d")

    TrendDataAggregationWorker.perform_async(person.id, nintey_days, end_date)
    TrendDataAggregationWorker.perform_async(person.id, thirty_days, end_date)
    TrendDataAggregationWorker.perform_async(person.id, seven_days, end_date)
  end
end
