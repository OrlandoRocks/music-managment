class CloudSearch::MumaSongSdf
  
  attr_reader :version, :add, :tc_song, :album_title, :composers,
    :isrc, :orig_release_year, :percent, :genre,
    :song_title, :upc, :label_copy, :parental_advisory, :primary_genre
  
  def initialize(song, version, add, options={})
    @tc_song              = song
    @version              = version.to_i
    @add                  = add
    @seven_day_popularity = options[:seven_day_popularity]
    @all_time_popularity  = options[:all_time_popularity]
    
    set_data if add
  end

  def valid?
    @tc_song && !@tc_song.composition_id.blank?
  end
  
  def to_hash
    return nil if !self.valid?
    
    hash = {
      :id=>"s#{@tc_song.id}",
      :type=>@add ? "add" : "delete",
      :version=>@version,
    }

    if add
      hash[:lang] = "en"

      hash[:fields] = {
        :album_title=>@album_title,
        :artist=>@artist,
        :composers=>@composers,
        :isrc=>@isrc,
        :orig_release_year=>@orig_release_year,
        :percent=>@percent,
        :primary_genre=>@primary_genre,
        :genre=>@genre,
        :song_title=>@song_title,
        :upc=>@upc,
        :label_copy=>@label_copy,
        :muma_song_code=>@tc_song.muma_code,
        :seven_day=>(@seven_day_popularity.blank? ? 0 : @seven_day_popularity),
        :all_time=>(@all_time_popularity.blank? ? 0 : @all_time_popularity),
        :parental_advisory=>@parental_advisory
      }
    else
      hash[:fields] = {
        :seven_day=>0,
        :all_time=>0
      }
    end
    
    hash
  end
  
private

  def set_data
    @artist      = []
    @album_title = ""

    @album_title       = @tc_song.album_name
    @isrc              = @tc_song.optional_isrc || @tc_song.tunecore_isrc 
    @orig_release_year = @tc_song.orig_release_year
    @percent           = @tc_song.mech_collect_share
    @parental_advisory = @tc_song.parental_advisory

    @genre             = []
    @genre << @tc_song.primary_genre.to_s   if @tc_song.primary_genre
    @genre << @tc_song.secondary_genre.to_s if @tc_song.secondary_genre

    @primary_genre     = @tc_song.primary_genre.to_s if @tc_song.primary_genre

    @song_title        = @tc_song.name
    @upc               = @tc_song.muma_upc || @tc_song.optional_upc || @tc_song.tunecore_upc
    @label_copy        = @tc_song.label_copy

    # Add all of the artists
    @tc_song.album_artists.split(",").each do |artist|
      @artist << artist 
    end if @tc_song.album_artists

    @tc_song.song_artists.split(",").each do |artist|
      @artist << artist
    end if @tc_song.song_artists
    
    @artist    = [@tc_song.muma_artist] if @artist.empty?
    @composers = @tc_song.muma_composers.split(",") if @tc_song.muma_composers

    if @composers.blank?
      @composers = @tc_song.tunecore_composers.split(",")
    end
  end

end

