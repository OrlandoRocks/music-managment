class CloudSearch::Batch
  TUNECORE_COMPANY_CODE = "TUNE".freeze
  SOCIETY_CODES = %w(BMI ASCA SESA).freeze
  BEGINNING_OF_TIME = '1970-01-01 00:00'.freeze

  attr_accessor :songs, :command, :last_add_date, :last_delete_date, :version, :registered_state
  attr_reader   :songs_to_add, :songs_to_delete, :document_endpoint, :seven_day_popularity, :all_time_popularity

  def initialize(options={})
    @pull_trend_data   = options[:pull_trend_data] || false
    @last_add_date     = options[:last_add_date]
    @last_delete_date  = options[:last_delete_date]
    @document_endpoint = options[:document_endpoint]
    @force_add         = options[:force_add]
    @force_delete      = options[:force_delete]
    @trend_end_date    = options[:trend_end_date].blank? ? Date.today - 1.day : Date.parse(options[:trend_end_date])

    if @document_endpoint.blank?
      result = ActiveRecord::Base.connection.execute("select document_endpoint from cloud_search_domain")
      row    = result.first if result
      @document_endpoint = row[0] if row
    end

    # Get the last batch date from the db if we're not overriding
    if @last_add_date.blank? || @last_delete_date.blank?
      # We use the second to the last requested_source_updated_at where the muma_import created_at is less than the last cloud_search_batch,
      # Anything before this date should be in cloudsearch already, anything after it needs to be sent or updated
      last_batch_dates = CloudSearch::Batch.last_batch_dates
      sql = "select requested_source_updated_at from muma_imports where requested_source_updated_at < ? order by requested_source_updated_at DESC LIMIT 1,1"

      if @last_add_date.blank?
        result = ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, last_batch_dates[:last_add_date]]))
        @last_add_date = result.first[0] if result && result.first
      end

      if @last_delete_date.blank?
        result = ActiveRecord::Base.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, last_batch_dates[:last_delete_date]]))
        @last_delete_date = result.first[0] if result && result.first
      end
    end

    @version          = Time.now.to_i
    @last_add_date    = Time.parse(BEGINNING_OF_TIME) if @force_add    || @last_add_date.blank?
    @last_delete_date = Time.parse(BEGINNING_OF_TIME) if @force_delete || @last_delete_date.blank?
    @registered_state = Composition.convert_to_state('Registered')

    Rails.logger.info("Cloud search batch initialized: LAST_ADD_DATE=#{@last_add_date.to_s if last_add_date} LAST_DELETE_DATE=#{@last_delete_date.to_s if @last_delete_date}")
  end

  def do_full_batch
    get_songs_to_add
    get_songs_to_delete

    do_batch(@songs_to_add, :add)
    do_batch(@songs_to_delete, :delete)

    sql = ActiveRecord::Base.send(:sanitize_sql_array, ["insert into cloud_search_batches (type, created_at, songs_in_cloudsearch, last_add_date, last_delete_date, domain ) values (?,NOW(),?,?,?,?)", "FULL", get_song_count, @last_add_date, @last_delete_date, @document_endpoint])
    ActiveRecord::Base.connection.execute(sql)
  end

  def do_add
    get_songs_to_add
    do_batch(@songs_to_add, :add)

    sql = ActiveRecord::Base.send(:sanitize_sql_array, ["insert into cloud_search_batches (type, created_at, songs_in_cloudsearch, last_add_date, last_delete_date, domain ) values (?,NOW(),?, ?, NULL, ?)", "ADD", get_song_count, @last_add_date, @document_endpoint])
    ActiveRecord::Base.connection.execute(sql)
  end

  def do_delete
    get_songs_to_delete
    do_batch(@songs_to_delete, :delete)

    sql = ActiveRecord::Base.send(:sanitize_sql_array, ["insert into cloud_search_batches (type, created_at, songs_in_cloudsearch, last_add_date, last_delete_date, domain ) values (?,NOW(),?,NULL,?, ?)", "DELETE", @force_delete ? 0 : get_song_count, @last_delete_date, @document_endpoint])
    ActiveRecord::Base.connection.execute(sql)
  end

  def delete_songs(song_ids)
    sdf_array = []
    song_ids.each do |song_id|
      song = Song.find(song_id)
      sdf = CloudSearch::MumaSongSdf.new(song, @version, false)
      sdf_array << sdf.to_hash if sdf && sdf.valid?
    end

    json_string = ActiveSupport::JSON.encode(sdf_array)
    upload(json_string)
  end

  def self.last_batch_dates
    result = ActiveRecord::Base.connection.execute("select max(created_at) as last_add_date from cloud_search_batches where type IN ('FULL', 'ADD')")
    last_add_date = result.each(as: :hash).first["last_add_date"]

    result = ActiveRecord::Base.connection.execute("select max(created_at) as last_delete_date from cloud_search_batches where type IN ('FULL', 'DELETE')")
    last_delete_date = result.each(as: :hash).first["last_delete_date"]

    { last_add_date: last_add_date, last_delete_date: last_delete_date }
  end

  def self.song_count
    result = ActiveRecord::Base.connection.execute("select songs_in_cloudsearch from cloud_search_batches where id = (select max(id) from cloud_search_batches)")
    row = result.first if result
    row ? row[0].to_i : 0
  end

  private

  def do_batch(songs, add_or_delete)
    @seven_day_popularity ||= {}
    @all_time_popularity  ||= {}
    sdf_array = []

    songs.each do |song|
      sdf = CloudSearch::MumaSongSdf.new(song, @version, add_or_delete == :add, seven_day_popularity: @seven_day_popularity[song.id], all_time_popularity: @all_time_popularity[song.id])
      sdf_array << sdf.to_hash if sdf.valid?

      # Upload file every 10k entries
      next unless !sdf_array.empty? && sdf_array.size % 10_000 == 0
      json_string = ActiveSupport::JSON.encode(sdf_array)
      # Output the batch file
      upload(json_string)

      sdf_array   = []
      json_string = ""
    end

    # Upload remaining songs
    unless sdf_array.empty?
      json_string = ActiveSupport::JSON.encode(sdf_array)
      upload(json_string)
    end
  end

  def get_song_count
    Song.select("DISTINCT songs.id")
      .joins(
        "INNER JOIN compositions on compositions.id = songs.composition_id
         INNER JOIN muma_songs   on muma_songs.composition_id = compositions.id
         INNER JOIN albums on albums.id = songs.album_id
         INNER JOIN people on albums.person_id = people.id"
      )
      .where(
        "mech_collect_share > 0
          AND muma_songs.company_code = ?
          AND compositions.state = ?
          AND is_deleted = 0
          AND albums.payment_applied = 1
          AND albums.album_type != 'Ringtone'",
          TUNECORE_COMPANY_CODE,
          registered_state
      )
      .count
  end

  def get_songs_to_add
    # Get MuMa songs
    @songs_to_add = Song.select("songs.id,
                                    songs.tunecore_isrc,
                                    songs.optional_isrc,
                                    songs.album_id,
                                    songs.composition_id,
                                    songs.name,
                                    songs.parental_advisory,
                                    albums.name as album_name,
                                    albums.orig_release_year as orig_release_year,
                                    muma_songs.mech_collect_share as mech_collect_share,
                                    primary_genres.name as primary_genre,
                                    secondary_genres.name as secondary_genre,
                                    tunecore_upcs.number as tunecore_upc,
                                    optional_upcs.number optional_upc,
                                    muma_songs.album_upc as muma_upc,
                                    muma_songs.code as muma_song_code,
                                    muma_songs.label_copy as label_copy,
                                    muma_songs.performing_artist as muma_artist,
                                    muma_songs.code as muma_code,
                                    GROUP_CONCAT(DISTINCT muma_song_ips.composer_first_name, ' ', muma_song_ips.composer_last_name) as muma_composers,
                                    GROUP_CONCAT(DISTINCT composers.first_name, ' ', composers.last_name) as tunecore_composers,
                                    GROUP_CONCAT(DISTINCT album_artists.name) as album_artists,
                                    GROUP_CONCAT(DISTINCT song_artists.name) as song_artists").joins("INNER JOIN compositions on compositions.id = songs.composition_id
                                   INNER JOIN muma_songs   on muma_songs.composition_id = compositions.id
                                   INNER JOIN albums on albums.id = songs.album_id
                                   INNER JOIN people on albums.person_id = people.id
                                   INNER JOIN composers on composers.person_id = people.id
                                   LEFT OUTER JOIN upcs as tunecore_upcs on tunecore_upcs.upcable_type = albums.album_type and tunecore_upcs.upcable_id = albums.id and tunecore_upcs.upc_type = 'tunecore' and tunecore_upcs.inactive = 0
                                   LEFT OUTER JOIN upcs as optional_upcs on optional_upcs.upcable_type = albums.album_type and optional_upcs.upcable_id = albums.id and optional_upcs.upc_type = 'optional' and optional_upcs.inactive = 0
                                   LEFT OUTER JOIN genres as primary_genres on albums.primary_genre_id = primary_genres.id
                                   LEFT OUTER JOIN genres as secondary_genres on albums.secondary_genre_id = secondary_genres.id
                                   LEFT OUTER JOIN muma_song_ips on muma_song_ips.song_code = muma_songs.code and ip_c_or_p = 'C'
                                   LEFT OUTER JOIN creatives as album_creatives on album_creatives.creativeable_id = albums.id and album_creatives.creativeable_type = albums.album_type
                                   LEFT OUTER JOIN creatives as song_creatives on song_creatives.creativeable_id = songs.id and song_creatives.creativeable_type = 'Song'
                                   LEFT OUTER JOIN artists as album_artists on album_artists.id = album_creatives.artist_id
                                   LEFT OUTER JOIN artists as song_artists on song_artists.id = song_creatives.id").group("songs.id").where(["muma_songs.mech_collect_share > 0
                                         AND muma_songs.company_code = ?
                                         AND compositions.state = ?
                                         AND is_deleted = 0
                                         AND albums.payment_applied = 1
                                         AND albums.album_type != 'Ringtone'
                                         AND (compositions.updated_at > ? || muma_songs.source_updated_at > ? || muma_songs.ip_updated_at > ? || muma_songs.company_updated_at > ? || albums.finalized_at > ? || songs.created_on >= DATE(?))",
                                       TUNECORE_COMPANY_CODE, registered_state, @last_add_date, @last_add_date, @last_add_date, @last_add_date, @last_add_date, @last_add_date])

    if @pull_trend_data
      @seven_day_popularity = get_seven_day_popularity_by_song_id(songs: @songs_to_add)
      @all_time_popularity  = all_time_popularity_by_song_id(songs: @songs_to_add)
    end
    @songs_to_add
  end

  def get_songs_to_delete
    # Songs that have been deleted since
    @songs_to_delete = Song.select("DISTINCT songs.*").joins("INNER JOIN compositions on compositions.id = songs.composition_id
                                      INNER JOIN muma_songs on muma_songs.composition_id = compositions.id
                                      INNER JOIN albums on songs.album_id = albums.id").where(["albums.album_type != 'Ringtone' and (((mech_collect_share = 0 || muma_songs.company_code != ? ) AND   muma_songs.updated_at > ? )
                                             || ( is_deleted = 1 and albums.updated_at > ? )
                                             || ( compositions.updated_at > ? and compositions.state != ? ))", TUNECORE_COMPANY_CODE, @last_delete_date, @last_delete_date, @last_delete_date, registered_state])

    if @force_delete
      @last_add_date    = Time.parse(BEGINNING_OF_TIME)
      @songs_to_add     = get_songs_to_add
      @songs_to_delete += @songs_to_add
    end

    @songs_to_delete
  end

  #
  # Returns the seven day popularity of all songs
  #
  # options[:songs]          = collection of songs
  #
  def get_seven_day_popularity_by_song_id(options={})
    get_trend_popularity(@trend_end_date - 1.week, @trend_end_date, options)
  end

  #
  # Returns the ytd popularity of all songs using
  # trend data
  #
  # options[:songs]  = collection of songs to run it on
  #
  def get_ytd_popularity_by_song_id(_options={})
    get_trend_popularity(Date.new(Date.today.year), Date.today)
  end

  #
  # Returns all time popularity for all songs using a combiniation
  # of sales data and trend data
  #
  def all_time_popularity_by_song_id(options={})
    ytd_trends     = get_ytd_popularity_by_song_id(options)
    previous_sales = previous_years_popularity_by_song_id(options)

    # merge the two
    ytd_trends.each do |song_id, qty|
      previous_sales[song_id] = 0 if previous_sales[song_id].blank?
      previous_sales[song_id] += qty
    end

    previous_sales
  end

  #
  # Returns the historical sales data for all time minus the current
  # year
  #
  def previous_years_popularity_by_song_id(_options={})
    sql = %{
      select related_id as song_id, sum(qty) as quantity
      from sales_by_year
      inner join songs on songs.id = related_id and related_type = "Song"
      inner join muma_songs on muma_songs.composition_id = songs.composition_id
      where year < ? and store_id = ?
      group by related_id
    }

    sql_array = [sql, Date.today.year, Store.where("abbrev = ?", "ww").first.id]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sql_array)

    results = ActiveSupport::HashWithIndifferentAccess.new

    ActiveRecord::Base.connection.execute(sql).each(as: :hash) do |result|
      results[result["song_id"].to_i] = result["quantity"].to_i
    end

    results
  end

  def upload(json_string)
    file = Tempfile.new("cloud_search_batch.sdf")
    file.write(json_string)
    file.flush

    # Issue the post to cloudsearch
    line = Cocaine::CommandLine.new("curl", %(-X POST --upload-file :file_path :cloud_search_endpoint --header "Content-Type:application/json" >> #{Rails.root}/log/#{Rails.env}.log))
    line.run(file_path: file.path, cloud_search_endpoint: @document_endpoint, logger: Rails.logger)

    Rails.logger.info("Cloud search batch uploaded")

    # Close and delete the batch file
    file.close!
  end

  def get_trend_popularity(start_date, end_date, options={})
    sql = %{
      select tds.song_id, sum(tds.qty) as quantity
      from trend_data_summary tds
      inner join #{TUNECORE_DB}.songs on songs.id = tds.song_id
      inner join #{TUNECORE_DB}.muma_songs on songs.composition_id = muma_songs.composition_id
      where  tds.date between ? and ?
    }

    sql += " and songs.id in (?) " if options[:songs]
    sql += " group by tds.song_id;"

    sql_array = [sql, start_date, end_date]
    sql_array << options[:songs] if options[:songs]

    sql = ActiveRecord::Base.send(:sanitize_sql_array, sql_array)

    results = ActiveSupport::HashWithIndifferentAccess.new
    TrendData.connection.execute(sql).each(as: :hash) do |result|
      results[result["song_id"].to_i] = result["quantity"].to_i
    end

    results
  end
end
