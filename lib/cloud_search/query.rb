module CloudSearch

  INTEGER_FIELDS = [:all_time, :seven_day]

  class Query

    attr_accessor :search_term, :search_field, :search_facets, :filter_facets, :search_start, :sort, :sort_descending,
      :limit, :genre_filter, :timeframe

    attr_writer   :search_version, :search_endpoint

    def initialize(options={})
      self.search_endpoint  = options[:search_endpoint]
      self.search_version   = options[:search_version]
      self.search_term      = options[:search_term]
      self.search_field     = options[:search_field]
      self.search_facets    = options[:search_facets]
      self.search_start     = options[:search_start]
      self.filter_facets    = options[:filter_facets]
      self.sort             = options[:sort] 
      self.sort_descending  = options[:sort_descending]
      self.limit            = options[:limit]
      self.genre_filter     = options[:genre_filter]
      self.timeframe        = options[:timeframe]
    end

    def search_version
      @search_version ||= CLOUD_SEARCH_SEARCH_API_VERSION
    end

    def search_endpoint
      if @search_endpoint.blank?
        @search_endpoint = ""
        result = ActiveRecord::Base.connection.execute("select search_endpoint from cloud_search_domain")
        row = result.first if result
        @search_endpoint = row[0] if row 
      end

      @search_endpoint
    end

    def search
      path = construct_search_path
      result = Net::HTTP.get(self.search_endpoint, path)
      JSON.parse(result)
    end

    def get_chart
      genre_filter = ""
      if self.genre_filter
        genre_filter = "(field primary_genre '#{self.genre_filter}')"
      end
      path = "/#{self.search_version}/search?bq=(and (filter #{self.timeframe} 1..) #{genre_filter})&return-fields=#{self.timeframe}&rank=-#{self.timeframe}&size=#{self.limit}"
      path = URI::escape(path)
      if self.genre_filter == 'R&B/Soul'
        path = path.gsub(/R&B\/Soul/, "R%26B%2FSoul") 
      end
      result = Net::HTTP.get(self.search_endpoint, path)
      JSON.parse(result)
    end

    private

    def construct_search_path
      path = "/#{self.search_version}/search?"
      params = {}

      # Add all filter facets to search query if present
      if !self.filter_facets.blank? || !self.search_field.blank?
        params[:bq] = "(and #{construct_search_term}"

        self.filter_facets.each do |facet, val|
          params[:bq] += val.map {|elem| " #{facet}:'#{elem}'"}.join(" ")
        end if self.filter_facets

        params[:bq] += ")"
      else
        params[:q] = "#{self.search_term}"
      end

      # Add start to search query to handle pagination
      if self.search_start
        params[:start] = "#{self.search_start.to_s}"
      end

      # Add a page size to the query
      if self.limit
        params[:size] = self.limit
      end

      # Add facets we are interested in getting details back on
      if !self.search_facets.blank?
        params[:facet] = "#{self.search_facets.map {|facet| facet}.join(",")}"
      end

      if self.sort
        params[:rank] = "#{self.sort_descending == true ? '-' : ''}#{self.sort}"
      end

      # Concat path and search params. to_query convert params to
      # be proper for url
      path + params.to_query
    end

    def construct_search_term
      if !search_field.blank?
        #Integer fields don't expect single quotes around the search term
        self.search_term = "'#{self.search_term.gsub(/'/, {"'" => "\\'"})}'" if !CloudSearch::INTEGER_FIELDS.include?(search_field.to_sym)

        "#{self.search_field}:#{self.search_term}"
      else
        "'#{self.search_term}'"
      end
    end

  end # end class

end # end module
