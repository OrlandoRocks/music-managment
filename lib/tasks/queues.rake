namespace :queues do
  desc "Retry all messages currently on queues"
  task :retry => :environment do

    if ENV["QUEUES"]
      queues = ENV["QUEUES"].split(",")
    else
      queues = ["petri-7digital-queue", "petri-cleanup-queue-no-vpc", "petri-itunes-queue", "petri-open-batch-queue", "petri-open-queue", "petri-open-restricted-queue", "petri-production-dist-emusic", "petri-production-queue", "petri-restricted-queue"]
    end

    all_messages = []
    queues.each do |queue_name|
      p "Deleting messages from #{queue_name}"
      queue = SQS_CLIENT.queues.named(queue_name)

      continue = true
      while continue
        messages = queue.receive_message(visibility_timeout: 1200, limit: 10, attributes: [:all])
        messages.each do |m|
          all_messages << m
          m.delete
        end
        p all_messages.length
        continue = !messages.empty?
      end
    end

    p "Deleted #{all_messages.length} messages from queues"

    distribution_ids = all_messages.map { |m| YAML.load(m.body)[:key] }
    distribution_ids = distribution_ids.uniq!

    if distribution_ids
      p "Retrying #{distribution_ids.length} distributions"
      distribution_ids.each do |id|
        distro = Distribution.find_by(id: id)
        if distro
          Delivery::DistributionWorker.perform_async(distro.class.name, distro.id)
        else
          p "Unable to find distribution with id #{id}"
        end
      end
    end
  end
end
