namespace :publishing_administration do
  desc "Update affect accounts in rights app who dosen't have PRO"
  task publishing_rights_app_accounts_without_pro: :environment do
    csv_name = "writer_IPI_not_in_RA.csv"
    rights_app_api_client = RightsApp::ApiClient.new
    request_size = 5
    RA_ACCOUNT_ID = "RA ACCOUNT #".freeze
    RakeDatafileService.csv(csv_name, headers: true).to_a.in_groups_of(request_size, false) do |group|
      group.each do |row|
        publishing_composer = PublishingComposer.joins(:account).find_by(accounts: { provider_account_id: row[RA_ACCOUNT_ID] })

        next unless publishing_composer.present? && publishing_composer.provider_identifier.present?
        
        writer_ra =  rights_app_api_client.send_request("GetWriter", writer: publishing_composer, publishing_changeover_enabled: true)
        e_tag = writer_ra.e_tag

        rights_app_api_client.send_request("UpdateWriter", writer: publishing_composer, e_tag: e_tag, publishing_changeover_enabled: true)
      end
    end
  end
end
