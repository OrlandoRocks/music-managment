require "#{Rails.root}/lib/logging"
require 'net/sftp'
require 'net/ssh/version'

namespace :petri do
  desc "Prune the PETRI worker array so that it contains no more than 1 worker per 10 messages"
  task prune_array: :environment do
    worker = Tunecore::PetriWorker.new
    queue_size = worker.queue_size
    array_size = worker.list.size
    messages_per_worker = queue_size / array_size
    puts "#{queue_size} distributions / #{array_size} workers: ~#{messages_per_worker} messages per worker (limit is 10)"
    if messages_per_worker < 10
      prune_size = ((array_size * 10) - queue_size) / 10
      puts "Pruning #{prune_size} workers"
      worker.prune_by prune_size
    else
      puts "Pruning is not needed"
    end

  end

  desc "Generate the previous day's Lala manifest and upload it to lala"
  task :lala_manifest => :environment do
    now = Time.new
    local_filename = "/tmp/manifest_#{now.year}_#{now.month}_#{(now-1.day).day}.tsv"
    remote_filename = "manifest_#{now.year}_#{now.month}_#{(now-1.day).day}.tsv"
    puts "Collecting distributions"
    distributions = Distribution.where("converter_class = 'MusicStores::Lala::Converter' and state = 'delivered' ")
    #filter for stuff that was delivered yesterday
    distributions = distributions.select{|d| d.transitions.last.new_state = 'delivered' && d.transitions.last.created_at > now-2.day  && d.transitions.last.created_at <= now-1.day  }
    manifest_file = File.new(local_filename, "w")
    puts "Writing manifest file"
    albums = distributions.collect{|d| d.petri_bundle.album }
    albums.uniq.each do |album|
      manifest_file << "#{album.upc}\t#{album.artist ? album.artist.name : 'Various Artists'}\t#{album.name}\n"
    end
    manifest_file.close

    #upload it
    puts "Uploading manifest file"
    Net::SFTP.start(LALA_SFTP_HOST, LALA_SFTP_USER, :keys => [LALA_SFTP_KEYFILE], :port => 22, :paranoid => false) do |sftp|
      sftp.upload! local_filename, remote_filename
    end
    puts "Done"
  end

  desc "Finds and retries all SSH errors"
  task :retry_ssh_errors => :environment do
    distributions = Distribution.find_errors.select{|d| d.transitions.last.message[/ssh/] rescue nil }
    puts "Retrying #{distributions.size} SSH errors"
    distributions.each{|d| d.retry(:actor => "rake task", :message => "Retrying SSH error")}
    puts "Done"
  end

  desc "Adds streaming or youtube salepoint if it doesn't exist"
  task :add_youtube_or_st, [:production_run] => [:environment] do |command, args|
    ADD_YOUTUBE = 'rake_task=petri:add_youtube_or_st'
    log(ADD_YOUTUBE, 'BEGIN', :params=>args, :album_ids => ENV['ALBUM_IDS'])
    album_ids = ENV['ALBUM_IDS'].nil? ? []:ENV['ALBUM_IDS'].split(' ')
    dryrun = (!args[:production_run].nil? && (args[:production_run] == 'true' || args[:production_run] == true)) ? false:true
    if album_ids.empty?
      log(ADD_YOUTUBE, 'Finished', :state => 'complete', :message => "nothing to process", :dryrun => dryrun)
    else
      begin
        start_time = Time.now
        albums = Album.find(album_ids)
        log(ADD_YOUTUBE, 'Start', :state => 'begin', :album_ids => albums.collect{|a| a.id}.join(', '), :dryrun=>dryrun, :start => start_time.strftime("%Y-%m-%d %H:%M:%S"))
        for album in albums
          log(ADD_YOUTUBE, 'Processing', :state => 'processing', :album_id => album.id, :dryrun=>dryrun, :album_type => album.album_type)
          begin
            if !dryrun
              album.petri_bundle.add_salepoints([])
              youtube_distro = album.petri_bundle.distributions.select{|d| d.converter_class =~/Youtube/}.first
              log(ADD_YOUTUBE, 'processed', :state => 'youtube distro added', :album_id => album.id, :dryrun=>dryrun, :distro_id => youtube_distro.id, :distro_state => youtube_distro.state )
            end
          rescue => e
            log(ADD_YOUTUBE, 'Error', :state=>'error processing album', :album_id=>album.id, :error=>e.message, :dryrun=>dryrun)
          end
        end
        log(ADD_YOUTUBE, 'Finished', :state => 'complete', :album_ids => albums.collect{|a| a.id}.join(', '), :dryrun => dryrun,
            :total_time => (Time.now-start_time), :finished_time => Time.now().strftime("%Y-%m-%d %H:%M:%S"))
      rescue => e
        log(ADD_YOUTUBE, 'Error', :state=>'error finding albums', :album_id=>album_ids.collect{|a| a.id}.join(', '), :error=>e.message, :dryrun=>dryrun)
      end
    end
  end

  desc "Recreate bundles missing distributions and/or distribution_salepoints"
  task recreate_incomplete_bundles: :environment do |t|
    ENV["LIMIT"] ||= 100
    albums = Album.find_by_sql("select distinct a.*
                                from salepoints s
                                left join distributions_salepoints d on s.id = d.salepoint_id
                                inner join albums a on s.salepointable_id = a.id
                                inner join petri_bundles p on p.album_id = a.id
                                inner join purchases pu on pu.related_id = a.id and pu.related_type = 'Album'
                                inner join stores st on s.store_id = st.id
                                where s.payment_applied = 1
                                and p.state != 'dismissed'
                                and d.salepoint_id is null
                                and DATE(pu.paid_at) < '#{Time.now.strftime('%Y-%m-%d')}'
                                and a.takedown_at is null
                                and st.in_use_flag = 1
                                limit #{ENV['LIMIT']}")

    albums.each do |album|
      begin
        PetriBundle.recreate! album
      rescue StandardError => e
        log(t, "#{e} recreating PetriBundle for album with id #{album.id}")
      end
    end
  end

  namespace :staging do
    desc "Run Petri Stub Processor"
    task process: :environment do
      Petri::StubProcessor.run!
    end
  end

  namespace :gracenote do
    desc "Retrieves iTunes metadata for gracenote"
    task :get_archived_metadata => :environment do
      distribution_ids = Distribution.find_by_sql(
        "select distinct dist.id, sp.salepointable_id
        -- dist.*,sp.salepointable_id
        from distributions dist
        inner join distributions_salepoints ds
        on dist.id = ds.distribution_id
        inner join salepoints sp
        on sp.id = ds.salepoint_id
        inner join albums
        on sp.salepointable_id = albums.id
        and sp.salepointable_type = 'Album'
        where converter_class = 'MusicStores::Itunes::Converter'
        and state = 'delivered'
        and albums.created_on > '2013-04-01'
        and albums.album_type in ('Album','Single')
        order by created_at desc
        limit 3000;"
      ).collect(&:id)

      distributions = Distribution.where(:id => distribution_ids).includes(:salepoints => :salepointable)

      dir = Rails.root + "tmp/gracenote_xmls"

      `mkdir -p #{dir}`

      puts "Getting archived metadata for #{distributions.size} distributions"
      distributions.each do |dist|
        metadata = dist.fetch_metadata
        file = File.new("#{dir}/#{dist.salepoints.first.salepointable.upc}.xml","w")
        file.write(metadata)
        file.flush
      end
    end
  end

  desc "Retrieves distribution_yamls using s3funnel"
  task :retrieve_distribution_yamls => :environment do
    raise "Need START_DATE" if ENV["START_DATE"].blank?

    FileUtils.mkdir("tmp/distribution_yamls") unless File.exists?("tmp/distribution_yamls")
    FileUtils.chdir("tmp/distribution_yamls")

    puts Rails.logger.info "Retrieving distributinos that have been updated since #{ENV["START_DATE"]}"
    distributions = Distribution.where("state = ? and updated_at >= ?", "delivered", Date.parse(ENV["START_DATE"]))

    ids = distributions.collect(&:id)
    puts Rails.logger.info "Retrieved #{ids.count}"
    puts Rails.logger.info "Outputting distribution yamls to tmp/distribution_yamls using s3_funnel"

    ids.each_slice(1000).to_a.each do |d_array|
      cmd ="s3funnel #{PETRI_MESSAGE_BUCKET} GET #{d_array.join(" ")} -a #{AWS_ACCESS_KEY} -s #{AWS_SECRET_KEY} -t 20 -v"
      puts Rails.logger.info "Running command: #{cmd}"
      `#{cmd}`
    end

  end

  desc "Deliver to YoutubeSR; Takes an optional MAX_ALBUMS=n"
  task :deliver_to_youtube_sr => :environment do
    default_options = {}
    max_albums = ENV['MAX_ALBUMS'] unless ENV['MAX_ALBUMS'].blank?
    default_options.merge!(:limit => max_albums)

    Rails.logger.info "Preparing to add and enqueue #{max_albums} distributions to YoutubeSR"
    filename = "youtubesr_delivery_#{Time.now.strftime('%Y_%m_%d_%H_%M_%S')}.txt"
    Rails.logger.info "Logging results to #{filename}"
    distributions = Composition.deliver_to_youtube_sr(default_options)

    File.open(Rails.root.join('tmp',filename),'w') do |f|
      distributions.each do |dist|
        f.write "Distribution=#{dist.inspect}\n"
      end
    end
    Rails.logger.info "enqueued #{distributions.size} distributions to YoutubeSR"
  end

  desc "Retry/Metadata Update on Distribution from a CSV list; Accepts FILENAME, optional CONVERTER_CLASS and optional DELIVERY_TYPE"
  task :retry_distributions_from_csv => :environment do |t|
    filename = ENV['FILENAME']
    delivery_type = ENV['DELIVERY_TYPE'] || 'metadata_only'
    converter_class = ENV['CONVERTER_CLASS'] || 'MusicStores::YoutubeSr::Converter'
    raise 'Requires FILENAME' unless filename

    log(t.name,"Starting #{delivery_type} to #{converter_class} from CSV: #{filename}")
    song_rows = []
    CSV.foreach(filename, headers: true) do |row|
      song_rows << {upc: row['UPC'], isrc: row['Identifier'], custom_id: row['Custom ID']}
    end

    error_filepath = "youtubesr_errored_migration_#{Time.now.strftime('%Y_%m_%d_%H_%M_%S')}.csv"

    error_file = CSV.open(Rails.root.join('tmp', error_filepath), "w")
    error_file << ['Song ID', 'ISRC', 'UPC', 'Distribution ID', 'Old Muma Song Code', 'Status', 'Error Message', 'Date Triggered']

    # group the UPC for more efficient queries
    song_rows.group_by{ |a| a[:upc] }.each do |upc, grouped_song_rows|
      songs = Song.joins(album: :upcs).where(upcs: {number: upc}).where(["tunecore_isrc in (:isrcs) or optional_isrc in(:isrcs)",:isrcs => grouped_song_rows.collect{ |s| s[:isrc] }]).includes(:album)
      songs.group_by(&:album).each do |album, grouped_songs|
        retries = 3
        begin
          distribution = album.distributions.detect {|d| d.converter_class == 'MusicStores::YoutubeSr::Converter'}

          # This runs without delay to shorten the feedback loop when it failed since we don't
          # readily monitor DJ and have no mechanism to handle failed DJ job easily anyway
          Delivery::DistributionWorker.perform_now(distribution.class.name, distribution.id, delivery_type)
          log(t.name, "Enqueued distribution=#{distribution.id} upc=#{upc} songs=#{grouped_songs.collect(&:isrc)}")
        rescue MusicStores::CompositionNotFound => e
          # grep song_id if it's invalid composition error
          bad_song_id = e.message.match(/Song id: ([\d]+)/i)[1]
          # debugger
          bad_index = grouped_songs.index {|s| s.id == bad_song_id.to_i}
          bad_song = grouped_songs.delete_at(bad_index)

          bad_song_row = grouped_song_rows.detect{|sr| sr[:isrc] == bad_song.isrc}
          custom_id = bad_song_row[:custom_id] if bad_song_row
          error_file << [bad_song.id, bad_song.isrc, upc, distribution.id, custom_id, 'Failed From TC', e, Time.now]

          log(t.name, "Retriable Errored distribution=#{distribution.id if distribution} upc=#{upc} songs=#{bad_song.isrc} error=#{e}")

          if grouped_songs.size > 0 && retries > 0
            retries -= 1
            retry
          end
        rescue StandardError => e # rescue other exceptions
          custom_ids = grouped_song_rows.collect{ |sr| sr[:custom_id] }.join('|')
          error_file << [grouped_songs.collect(&:id).join('|'), grouped_songs.collect(&:isrc).join('|'), upc, distribution.id, custom_ids, 'Failed From TC', "#{e.class};#{e}", Time.now]
          log(t.name, "Errored distribution=#{distribution.id if distribution} upc=#{upc} songs=#{grouped_songs.collect(&:isrc)} error=#{e.class};#{e}")
        end
      end
    end

    error_file.close
  end

  private

  def sent_albums
    albums = Album.joins(",petri_bundles").where("albums.id = petri_bundles.album_id")
    albums.flatten!
  end
end
