require "#{Rails.root}/lib/slack_notifier"

namespace :batch do

  #Creates batches for each country website. Then person by person, Creates Purchases for renewals due.. Attaches purchases to invoices smartly
  #Process invoice, create batch_transaction. process batch transaction
  desc "Creates purchases and invoices for renewals that are due and processes them in a batch"
  task :build_and_run, [:date, :sync] => :environment do |_, args|
    job_name = "batch:build_and_run"
    notification_type = "Renewals"

    args.with_defaults(date: Date.today.strftime('%Y-%m-%d'))

    date = args[:date]
    use_sidekiq = args[:sync].blank?
    start_time = Time.current

    past_grace_period = date.to_date <= (Date.current - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days)
    raise "Do not run renewals for greater than 42 days ago" if Rails.env.production? && past_grace_period

    notify_in_slack("rake_task=#{job_name} started - #{start_time}", notification_type)

    begin
      RenewalsBatchFailureService.resolve(date)

      Album.takedown_unsent_and_ready_to_renew(Date.parse(date))
      log("Payment Batch", "Start #{job_name} Rake Task", time: start_time)

      Batch.build_and_run(Date.parse(date), use_sidekiq)

      log("Payment Batch", "End #{job_name} Rake Task", time: pretty_time_since(start_time))

      notify_in_slack("rake_task=#{job_name} completed", notification_type)
    rescue Exception => e
      Airbrake.notify("Renewals batch rake task (batch:build_and_run) failed", e)
      notify_in_slack("rake_task=#{job_name} failed due to an error: #{e}", notification_type)
      raise
    end
  end

  desc "Re-run batch:build_and_run for each RenewalsBatchFailure"
  task rerun_failed_batches: :environment do
    RenewalsBatchFailure
      .unresolved
      .each { |rbf| Rake::Task["batch:build_and_run"].invoke(rbf.date.to_s) }
  end

  desc "Processes batch transactions that are still pending"
  task :run=>:environment do

    start_time = Time.now
    log("Payment Batch", 'Start batch:run Rake Task', :time=>start_time)
    batches = PaymentBatch.where(status: 'new')

    if !batches.empty?

      batches.each do |batch|
        transactions = BatchTransaction.where(:processed=>'pending', :payment_batch_id=>batch.id).includes(:invoice)

        #Process them
        batch.process_transactions(transactions)

        #Upload to csv and complete the batch if ready to do so
        batch.complete_batch

      end

   else
     log("Payment Batch", 'batch:run called, No Batches to process')
   end

   log("Payment Batch", 'End batch:run Rake Task', :time=>pretty_time_since(start_time))
  end

  desc 'Method to remove trend_report renewals that are expired for more than 2 weeks'
  task :remove_overdue_trend_reports =>  :environment do
     start_time = Time.now
     log("Payment Batch", 'Start batch:remove_overdue_trend_reports Rake Task', :time=>start_time)

     old_trend_report_purchases = Purchase.old_trend_reports_for_deletion

     if !old_trend_report_purchases.empty?
       old_trend_report_purchases.each do |purchase|
         log("Payment Batch", 'Old Unpaid Trend Report Purchase Deleted', :person=>purchase.person_id, :amount=>sprintf("%.2f" % ((purchase.cost_cents.to_f)/100)), :purchase=>purchase.id)
         Purchase.find(purchase.id).destroy
       end
     else
       log("Payment Batch", 'No Unpaid Trend Report Purchase Deleted')
     end

     log("Payment Batch", 'End batch:remove_overdue_trend_reports Rake Task', :time=>pretty_time_since(start_time))
   end

  desc "Roll back and update voided transactions"
  task void: :environment do |t|
    raise "Must supply 'TRANSACTION_IDS'" if ENV["TRANSACTION_IDS"].blank?

    log(t, "Beginning voiding")

    eft_batch_transactions = ENV['TRANSACTION_IDS'].split(",").map { |id| EftBatchTransaction.find_by(transaction_id: id) }
    eft_batch_transactions.each do |ebt|
      ebt.update(status: "canceled")
      EftBatchTransactionHistory.create(status: "canceled", eft_batch_transaction: ebt, comment: "Transaction voided by Braintree")
      ebt.send(:rollback_debit)
      ebt.send(:rollback_service_fee)
    end

    log(t, "Finished voiding")
  end

  desc "Backfill the unfinished batch_transactions from the removal of braintree orange code"
  task backfill_errored_renewals: :environment do |t|
    batches = PaymentBatch.where("status = 'processing' and batch_date > '2015-09-20'").includes(batch_transactions: [invoice: [:braintree_transactions]])
    batches.each do |pb|
      begin
        pb.batch_transactions.each do |batch_transaction|
          next unless batch_transaction.processed == "pending"
          bt_transaction = batch_transaction.invoice.braintree_transactions.last
          batch_transaction.update(processed: "processed", matching: bt_transaction)
          batch_transaction.invoice.update(batch_status: "waiting_for_batch") if batch_transaction.invoice.batch_status == "processing_in_batch"
        end

        pb.update(status: "complete")
      rescue
        log("Payment Batch", 'Failed to backfill correctly', payment_batch: pb.id)
      end
    end
  end
end
