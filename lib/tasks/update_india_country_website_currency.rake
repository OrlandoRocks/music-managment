namespace :country_websites do
  desc "Set India CountryWebsite Currency To USD"
  task set_india_country_website_currency: :environment do
    india_website = CountryWebsite.find_by(country: 'IN')
    india_website.update(currency: 'USD')
  end
end
