namespace :distribution do
  task :retry_batches => :environment do
    Distribution::BatchRetrier.retry_batches
  end
end
