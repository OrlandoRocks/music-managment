namespace :itunes do
  desc "Update known_live_on based on itunes status"
  task :update_status => :environment do

    start_date  = ENV["START_DATE"]  || (Date.today - 30.days).strftime("%Y-%m-%d")
    cutoff_days = ENV["CUTOFF_DAYS"] || 2

    sql = "SELECT DISTINCT albums.* FROM albums INNER JOIN salepoints ON salepoints.salepointable_id = albums.id AND salepoints.salepointable_type = 'Album' INNER JOIN distributions_salepoints ON distributions_salepoints.salepoint_id = salepoints.id INNER JOIN distributions ON distributions.id = distributions_salepoints.distribution_id WHERE (salepoints.store_id in (1,2,3,4,5,6,22,29,34,36) and distributions.state = 'delivered' and albums.finalized_at is not null and albums.known_live_on is null and albums.is_deleted = false and albums.takedown_at is null and distributions.updated_at >= '#{start_date}' and distributions.updated_at < DATE_ADD(CURDATE(), INTERVAL - #{cutoff_days} DAY))"
    albums = Album.find_by_sql(sql)
    p "Found #{albums.length} albums"
    albums.each_with_index do |a, i|

      p "#{Time.now} Processing #{i+1}/#{albums.length}" if (i+1) % 10 == 0
      begin
        url = URI.parse("http://applestatus-env-frqmgq3nki.elasticbeanstalk.com/upc/#{a.upc}")
        req = Net::HTTP::Get.new(url.path)
        res = Net::HTTP.start(url.host, url.port){|http| http.request(req)}

        begin
          json_data = JSON::load(res.body)
          if json_data['response'] && json_data['response']['status'] && json_data['response']['apple_id']
            apple_id = json_data['response']['apple_id']
            status = json_data['response']['status']
            p "Status = #{status} for Album ID #{a.id}"
            if status == 'Live'
              p " UPDATING KNOWN LIVE ON "
              a.apple_identifier = apple_id
              a.set_external_id_for("apple", apple_id)
              a.known_live_on = Time.now
              a.save!
            else
              p "NO UPDATE, NOT LIVE YET"
            end
          end
        rescue => e
          p "ERROR Updating album known_live_on or apple_id: #{e.message}"
        end
      rescue => e
        p "ERROR Getting Apple status #{e.message}"
      end
    end
  end

  task :generate_tickets_report => :environment do
    transporter_installed = `which iTMSTransporter`
    raise "You must have iTMSTransporter installed and in your path to run this task." if transporter_installed.empty? || transporter_installed.include?("no iTMSTransporter")

    # TODO: Move user and password to config file
    p "FETCH TICKETS"
    transporter_cmd = "iTMSTransporter -m queryTickets -u trends@tunecore.com -p '^t2H!+CsD' -v off 2>/dev/null"
    result = `#{transporter_cmd}`
    result_xml = REXML::Document.new(result, :compress_whitespace => :all)

    tickets = result_xml.get_elements("tickets/ticket").map { |t| { apple_id: t.get_text("contentAdamId").to_s, ticket_id: t.get_text("ticketId").to_s, upc: t.get_text("contentVendorId").to_s } }.group_by { |t| t[:upc] }

    p "CREATE FILE"
    File.open("itunes_ticket_report.csv", 'w') { |file| file.write("Apple ID, Vendor ID / UPC, Ticket IDs\n") }
    tickets.each do |upc, data|
      u = Upc.find_by(number: upc)
      # TODO: Account for iTunes salepoints being taken down as well, instead of just entire albums.
      File.open("itunes_ticket_report.csv", 'a') { |file| file.write("#{data.first[:apple_id]}, #{upc}, #{data.map { |d| d[:ticket_id] }.join("|") }\n") } if u.nil? || (album = u.upcable).nil? || (album.takedown_at.present?)
    end

    p "SEND EMAIL"
    AdminNotifier.itunes_ticket_report.deliver

    p "CLEANUP FILE"
    File.delete("itunes_ticket_report.csv")
  end

  task populate_apple_ids: :environment do
    task_uri = 'rake_task=itunes:populate_apple_ids'
    days = ENV["DAYS_AGO"] || Apple::BulkAlbumIdFetchingService::DEFAULT_DAYS_AGO

    notify_in_slack("#{task_uri} started - #{Time.now}. Cron Job to fetch Album URI from iTunes which has been delivered during the last #{days} days.", 'iTunes')
    Apple::BulkAlbumIdFetchingService.fetch_for_albums(ENV["DAYS_AGO"])
    notify_in_slack("#{task_uri} completed. Cron Job to fetch Album URI from iTunes which has been delivered during the last #{days} days.", 'iTunes')
  end

  task hidden_content_report: :environment do
    delivered_date = ENV["DATE_DELIVERED"] || (Date.today - 2.days).strftime
    File.open("/tmp/itunes_hidden_report_#{delivered_date}.csv", 'w') { |file| file.write("UPC,Album Title,Hidden Date \n") }

    albums = Album.fetch_by_store_and_distribution_date(Store::ITUNES_STORE_IDS, delivered_date)
    upc_string = albums.uniq { |a| a.id }.collect { |a| a.upc.number }.join(',')

    transporter_cmd = "iTMSTransporter -m statusAll -u trends@tunecore.com -p Uriah4eep -vendor_ids '#{upc_string}' -outputFormat xml -v off 2>/dev/null"
    result = `#{transporter_cmd}`
    result_xml = REXML::Document.new(result, :compress_whitespace => :all)
    result_xml.elements.each("itunes_transporter/upload_status/content_status_info") do |elem|
      if elem.attributes["itunes_connect_status"] == "Hidden"
        upc_number = elem.parent.attributes["vendor_identifier"]
        a = albums.find { |a| a.upc.number == upc_number }
        File.open("/tmp/itunes_hidden_report_#{delivered_date}.csv", 'a') { |file| file.write("#{upc_number},#{a.name},#{elem.next_element.attributes["created"]} \n") }
      end
    end

    AdminNotifier.itunes_hidden_content_report(delivered_date).deliver

    # remove local csv file
    File.delete("/tmp/itunes_hidden_report_#{delivered_date}.csv")
  end

  task fetch_missing_apple_ids: :environment do
    limit = JSON.parse(ENV.fetch('LIMIT', 0))
    min_index = $redis.get(ENV['JOB_KEY']).to_i || 0
    max_index = min_index + limit
    album_ids = CSV.read(ENV['FILE_PATH'])[min_index...max_index].flatten!
    raise "Album IDs were not found for #{filename}" if album_ids.empty?

    album_ids.each do |id|
      begin
        Apple::Album.get_itunes_info(id)
      rescue ActiveRecord::RecordNotFound
        Rails.logger.info "Album #{id} not found"
        next
      end
    end

    $redis.INCRBY(ENV['JOB_KEY'], limit)
  end

  task queue_new_apple_artists: :environment do
    album_ids = CSV.read(ENV['FILE_PATH']).flatten!
    raise "Album IDs were not found for #{filename}" if album_ids.empty?

    album_ids.each do |id|
      Apple::CreateAppleArtistsServiceWorker.perform_async(id)
    end
  end
end
