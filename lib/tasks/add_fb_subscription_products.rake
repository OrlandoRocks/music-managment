
namespace :product do
  task subscription_product_setup: :environment do
    Product.where(name: "Facebook Track Monetization").each do |product|
      SubscriptionProduct.find_or_create_by(
        product_name: "FBTracks",
        product_type: "annually",
        product_id: product.id,
        term_length: 0
      )
    end
  end
end
