require 'benchmark'

namespace :price_change do

  desc "Price change for December 2012"
  task :december_2012=>:environment do
    dry_run = (ENV['DRY_RUN'].blank? || ENV['DRY_RUN'] != "0")

    Product.transaction do
      # Price Changes
      #
      #     Album Initial Purchase: 29.99 (-20)
      #     2 Year Album: 75.98
      #     5 Year Album: 206.95
      #     Album Renewal: 49.99 (no change)
      #     Single Initial Purchase: 9.99 (-5)
      #     2 Year Single: 18.98
      #     5 Year Single: 44.95
      #     Single Renewal: 9.99 (no change)
      #     Ringtone Initial Purchase: 19.99 (+5)
      #     2 Year Ringtone: inactivated
      #     5 Year Ringtone: inactivated
      #     Ringtone Renewals (for New 1 yr): 19.99 (+10)
      #     Ringtone Renewal (all legacy purchases): 9.99 (no change)

      #
      # ALBUMS
      #

      #Update US initial album prices
      one_year_pir = ProductItemRule.find(90)
      two_year_pir = ProductItemRule.find(94)
      five_year_pir = ProductItemRule.find(98)

      one_year_pir.update(:price=>29.99)
      two_year_pir.update(:price=>75.98)
      five_year_pir.update(:price=>206.95)

      #Update Canadian initial album prices
      one_year_pir = ProductItemRule.find(147)
      two_year_pir = ProductItemRule.find(232)
      five_year_pir = ProductItemRule.find(236)

      one_year_pir.update(:price=>29.99)
      two_year_pir.update(:price=>75.98)
      five_year_pir.update(:price=>206.95)

      #
      # SINGLES
      #

      #Update US initial single prices
      one_year_pir = ProductItemRule.find(110)
      two_year_pir = ProductItemRule.find(114)
      five_year_pir = ProductItemRule.find(118)

      one_year_pir.update(:price=>9.99)
      two_year_pir.update(:price=>18.98)
      five_year_pir.update(:price=>44.95)

      #Update Canadian initial single prices
      one_year_pir = ProductItemRule.find(151)
      two_year_pir = ProductItemRule.find(210)
      five_year_pir = ProductItemRule.find(244)

      one_year_pir.update(:price=>9.99)
      two_year_pir.update(:price=>18.98)
      five_year_pir.update(:price=>44.95)

      #
      # RINGTONES
      #

      #Naive check to see if the script has already been run
      product = Product.where("id=?",Product::US_RINGTONE_RENEWAL_PRODUCT_ID).first

      new_us_ringtone_renewal_attributes = {
        :created_by_id=>1,
        :country_website_id=>CountryWebsite::UNITED_STATES,
        :name=>"Ringtone Renewal",
        :display_name=>"Ringtone Renewal",
        :description=>"Annual renewal fee for your ringtone.",
        :status=>"Active",
        :product_type=>"Renewal",
        :is_default=>1,
        :applies_to_product=>"Ringtone",
        :sort_order=>1,
        :renewal_level=>"None",
        :price=>19.99,
        :currency=>"USD"
      }

      new_ca_ringtone_renewal_attributes = {
        :created_by_id=>1,
        :country_website_id=>CountryWebsite::CANADA,
        :name=>"Ringtone Renewal",
        :display_name=>"Ringtone Renewal",
        :description=>"Annual renewal fee for your ringtone.",
        :status=>"Active",
        :product_type=>"Renewal",
        :is_default=>1,
        :applies_to_product=>"Ringtone",
        :sort_order=>1,
        :renewal_level=>"None",
        :price=>19.99,
        :currency=>"CAD"
      }

      new_us_ringtone_attributes = {
        :created_by_id=>1,
        :country_website_id=>CountryWebsite::UNITED_STATES,
        :name=>"1 Year Ringtone",
        :display_name=>"1 Year",
        :description=>"Your ringtone will be live for one year.",
        :status=>"Active",
        :product_type=>"Ad Hoc",
        :is_default=>1,
        :applies_to_product=>"Ringtone",
        :sort_order=>1,
        :renewal_level=>"Item",
        :price=>19.99,
        :currency=>"USD"
      }

      us_product_item_attributes = {
        :base_item_id=>3,
        :name=>"Ringtone Distribution",
        :description=>"Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
        :price=>0,
        :currency=>"USD",
        :does_not_renew=>0,
        :renewal_type=>"distribution",
        :first_renewal_duration=>1,
        :first_renewal_interval=>"year",
        :renewal_duration=>1,
        :renewal_interval=>"year"
      }

      us_product_item_rule_attributes = {
        :base_item_option_id=>67,
        :rule_type=>"inventory",
        :rule=>"price_for_each",
        :inventory_type=>"Ringtone",
        :quantity=>1,
        :unlimited=>0,
        :price=>19.99,
        :currency=>"USD"
      }

      us_song_product_item_rule_attribute = {
       :base_item_option_id=>68,
       :quantity=>1,
       :currency=>"USD",
       :price=>0.0
      }

      us_salepoint_product_item_rule_attribute = {
        :base_item_option_id => 69,
        :unlimited => 1,
        :quantity=>0,
        :currency=>"USD",
        :price=>0.99
      }

      cad_ringtone_attributes = {
        :created_by_id=>1,
        :country_website_id=>CountryWebsite::CANADA,
        :name=>"1 Year Ringtone",
        :display_name=>"1 Year",
        :description=>"Your ringtone will be live for one year.",
        :status=>"Active",
        :product_type=>"Ad Hoc",
        :is_default=>1,
        :applies_to_product=>"Ringtone",
        :sort_order=>1,
        :renewal_level=>"Item",
        :price=>19.99,
        :currency=>"CAD"
      }

      cad_product_item_attributes = {
        :base_item_id=>3,
        :name=>"Ringtone Distribution",
        :description=>"Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
        :price=>0,
        :currency=>"CAD",
        :does_not_renew=>0,
        :renewal_type=>"distribution",
        :first_renewal_duration=>1,
        :first_renewal_interval=>"year",
        :renewal_duration=>1,
        :renewal_interval=>"year"
      }

      cad_product_item_rule_attributes = {
         :base_item_option_id=>96,
         :rule_type=>"inventory",
         :rule=>"price_for_each",
         :inventory_type=>"Ringtone",
         :quantity=>1,
         :unlimited=>0,
         :price=>19.99,
         :currency=>"CAD"
      }

      cad_song_product_item_rule_attribute = {
       :base_item_option_id=>97,
       :quantity=>1,
       :currency=>"CAD",
       :price=>0.0
      }

      cad_salepoint_product_item_rule_attribute = {
        :base_item_option_id => 98,
        :unlimited => 1,
        :quantity=>0,
        :currency=>"CAD",
        :price=>0.99
      }

      if product.blank?
        #Create new US ringtone renewal at 19.99
        new_us_ringtone_renewal = Product.create!(new_us_ringtone_renewal_attributes)

        #Create new Canadian ringtone renewal at 19.99
        new_ca_ringtone_renewal = Product.create!(new_ca_ringtone_renewal_attributes)

        #Create new US 1 year ringtone product that renews with new renewal
        new_us_ringtone = Product.create!(new_us_ringtone_attributes)

        pi = ProductItem.create!(us_product_item_attributes.merge(:product=>new_us_ringtone, :renewal_product=>new_us_ringtone_renewal))

        pi.product_item_rules.create!(us_product_item_rule_attributes)
        pi.product_item_rules.create!(us_song_product_item_rule_attribute)
        pi.product_item_rules.create!(us_salepoint_product_item_rule_attribute)

        #Create new Canadian 1 year ringtone product that renews with new renewl
        new_ca_ringtone = Product.create!(cad_ringtone_attributes)

        pi = ProductItem.create!(cad_product_item_attributes.merge(:product=>new_ca_ringtone, :renewal_product=>new_ca_ringtone_renewal))

        pi.product_item_rules.create!(cad_product_item_rule_attributes)
        pi.product_item_rules.create!(cad_song_product_item_rule_attribute)
        pi.product_item_rules.create!(cad_salepoint_product_item_rule_attribute)
      else

        #Find and update new US ringtone renewal
        new_us_ringtone_renewal = Product.find(Product::US_RINGTONE_RENEWAL_PRODUCT_ID)
        new_us_ringtone_renewal.update(new_us_ringtone_renewal_attributes)

        #Find and update new CA ringtone renewal
        new_ca_ringtone_renewal = Product.find(Product::CA_RINGTONE_RENEWAL_PRODUCT_ID)
        new_ca_ringtone_renewal.update(new_ca_ringtone_renewal_attributes)

        #Find and update new us 1yr ringtone product
        new_us_ringtone = Product.find(Product::US_ONE_YEAR_RINGTONE_PRODUCT_ID)
        new_us_ringtone.update(new_us_ringtone_attributes)

        pi = new_us_ringtone.product_items.first
        pi.update(us_product_item_attributes)

        pi.product_item_rules[0].update(us_product_item_rule_attributes)
        pi.product_item_rules[1].update(us_song_product_item_rule_attribute)
        pi.product_item_rules[2].update(us_salepoint_product_item_rule_attribute)

        #Find and update new US ringtone product
        new_ca_ringtone = Product.find(Product::CA_ONE_YEAR_RINGTONE_PRODUCT_ID)
        new_ca_ringtone.update(cad_ringtone_attributes)

        pi = new_ca_ringtone.product_items.first
        pi.update(cad_product_item_attributes)

        pi.product_item_rules[0].update(cad_product_item_rule_attributes)
        pi.product_item_rules[1].update(cad_song_product_item_rule_attribute)
        pi.product_item_rules[2].update(cad_salepoint_product_item_rule_attribute)
      end

      #Inactive 2yr,5yr ringtones
      two_yr_product = Product.find(47)
      five_yr_product = Product.find(48)

      two_yr_product.update(:status=>"Inactive")
      five_yr_product.update(:status=>"Inactive")

      two_yr_product = Product.find(99)
      five_yr_product = Product.find(100)

      two_yr_product.update(:status=>"Inactive")
      five_yr_product.update(:status=>"Inactive")

      #Disable old 1 year ringtone products (46,71)
      Product.find(46).update(:status=>"Inactive")
      Product.find(71).update(:status=>"Inactive")

      #Clear all related products which at this time were only album distro credit products
      RelatedProduct.all.each { |rp| rp.destroy }

      raise Exception if dry_run
    end
  end

  desc "Update purchases for december 2012 price change"
  task :december_2012_purchase_update => :environment do
    #Update existing purchases for singles and albums
    us_product_ids = [36,37,38,41,42,43]
    ca_product_ids = [69,95,96,70,97,98]

    errored = []

    elapsed=0

    elapsed = Benchmark.realtime {
      puts "Update album/single purchases:"
      purchases = Purchase.unpaid.where(["price_adjustment_histories_count = 0 and product_id in (:product_ids)",{:product_ids=>us_product_ids+ca_product_ids}])

      purchases.each do |purchase|
        Product.transaction do
          begin
            purchase.recalculate; print ".";
          rescue StandardError => e
            errored << [purchase, e]
            print "-"
          end
        end
      end

      #Print errors
      errored.each do |error|
        puts("Purchase #{error[0].id} error: #{error[1].to_s}")
      end

      #Throw out any ringtone purchases
      us_product_ids = [46,47,48]
      ca_product_ids = [71,99,100]

      errored = []

      #Destroying ringtone purchases
      puts "\nUpdating ringtone purchases"
      purchases = Purchase.unpaid.where(["product_id in (:product_ids)",{:product_ids=>us_product_ids+ca_product_ids}])
      purchases.each do |purchase|
        Product.transaction do
          begin
            purchase.destroy
            print "."
          rescue StandardError => e
            errored << [purchase, e]
            print "-"
          end
        end
      end

      #Print errors
      errored.each do |error|
        puts("Purchase #{error[0].id} error: #{error[1].to_s}")
      end
    }

    puts  "Execution took: #{elapsed}"
  end

  desc "Update add store product item rules to look at stores.is_free? for free stores"
  task :december_2012_free_salepoints => :environment do
   dry_run = (ENV['DRY_RUN'].blank? || ENV['DRY_RUN'] != "0")

    ProductItemRule.transaction do

      #Update the original base item options for consistency
      orig_us_base_item_option = BaseItemOption.find(38)
      orig_us_base_item_option.update({:price=>0})

      orig_cad_base_item_option = BaseItemOption.find(112)
      orig_cad_base_item_option.update({:price=>0})

      #Update original PIR for add store product to be 0 priced
      pir_us = ProductItemRule.find(12)
      pir_ca = ProductItemRule.find(214)

      pir_us.price = 0
      pir_us.save!

      pir_ca.price = 0
      pir_ca.save!

      #Create new base item options
      us_base_item_option = BaseItemOption.create(
      {
        :base_item_id=>orig_us_base_item_option.base_item_id,
        :sort_order=>2,
        :parent_id=>nil,
        :name=>"Price per store",
        :product_type=>"Ad Hoc",
        :option_type=>"required",
        :rule_type=>"inventory",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.is_free?",
        :true_false=>false,
        :price=>1.98,
        :currency=>"USD"
      })

      cad_base_item_option = BaseItemOption.create(
      {
        :base_item_id=>orig_cad_base_item_option.base_item_id,
        :sort_order=>2,
        :parent_id=>nil,
        :name=>"Price per store",
        :product_type=>"Ad Hoc",
        :option_type=>"required",
        :rule_type=>"inventory",
        :rule=>"true_false",
        :inventory_type=>"",
        :model_to_check=>"Salepoint",
        :method_to_check=>"store.is_free?",
        :true_false=>false,
        :price=>1.98,
        :currency=>"CAD"
      })


      #Create new rules that conditionally add 1.98 based on return of salepoint.store.is_free?
      ProductItemRule.create!(
        :product_item_id=>4,
        :base_item_option_id=>us_base_item_option.id,
        :parent_id=>nil,
        :true_false=>0,
        :price=>1.98,
        :currency=>"USD"
      )

      ProductItemRule.create!(
        :product_item_id=>58,
        :base_item_option_id=>cad_base_item_option.id,
        :parent_id=>nil,
        :true_false=>0,
        :price=>1.98,
        :currency=>"CAD"
      )

      raise Exception if dry_run
    end
  end

end
