namespace :feature_flipper do
  desc "Deleted feature from Feature Flipper"
  task delete: :environment do
    exit unless ENV['feature_name']
    $rollout.delete(ENV['feature_name'])
  end
end
