# frozen_string_literal: true

# Example usage:
# bundle exec rake "seed:seed_agnostic_table[plans_stores]"

namespace :seed do
  task :seed_agnostic_table, [:table] => :environment do |_t, args|
    table = args[:table].to_s
    file = Rails.root.join("db/seed/csv/#{table}.csv")
    Seed.seed(file)
  end
end
