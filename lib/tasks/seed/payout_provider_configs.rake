namespace :seed do
  desc "Seed Payout Provider Configs"
  task payout_provider_configs: :environment do
    PayoutProviderConfig.connection.truncate(PayoutProviderConfig.table_name)
    CipherDatum.where(cipherable_type: 'PayoutProviderConfig').delete_all

    csv = RakeDatafileService.csv('payout_provider_configs.csv', headers: true, encoding: 'utf-8')

    csv.each do |ppc|
      PayoutProviderConfig.create!(ppc.to_h)
    end
  end
end
