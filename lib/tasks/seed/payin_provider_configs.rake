namespace :seed do
  desc "Seed Payin Provider Configs"
  task payin_provider_configs: :environment do
    PayinProviderConfig.connection.truncate(PayinProviderConfig.table_name)
    CipherDatum.where(cipherable_type: 'PayinProviderConfig').delete_all

    # we are using a different strategy here than is typical for seeding, where we updated .circle_ci/config to run
    # this task, and we grab from a csv with headers outside of the automated db/seed/csv that circle ci usually
    # picks up
    #
    # the reason for this is that we are stored 3 attributes encrypted on the 'secrets' column, but the automated
    # circle ci seeding doesn't know how to handle that and it expects the columns in the csv to match the columns
    # in the db
    csv = RakeDatafileService.csv('payin_provider_config_seeds.csv', headers: true, encoding: 'utf-8')

    csv.each do |ppc|
      PayinProviderConfig.create!(ppc.to_h)
    end
  end
end
