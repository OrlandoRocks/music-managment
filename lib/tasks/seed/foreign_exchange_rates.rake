namespace :seed do
  desc "Seed Foreign Exchange Rates"
  task foreign_exchange_rates: :environment do
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'foreign_exchange_rates.csv')) do |ppc|
      headers = ForeignExchangeRate.attribute_names
      ForeignExchangeRate.find_or_create_by(headers.zip(ppc).to_h)
    end
  end
end
