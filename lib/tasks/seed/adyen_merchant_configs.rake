namespace :seed do
  desc "Seed Adyen Merchant Configs"
  task adyen_merchant_configs: :environment do
    AdyenMerchantConfig.connection.truncate(AdyenMerchantConfig.table_name)
    CipherDatum.where(cipherable_type: "AdyenMerchantConfig").delete_all

    csv = RakeDatafileService.csv("adyen_merchant_config_seeds.csv", headers: true, encoding: "utf-8")

    csv.each do |amc|
      AdyenMerchantConfig.create!(amc.to_h)
    end
  end
end
