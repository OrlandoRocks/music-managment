namespace :seed do
  desc "Seed Country Variable Prices"
  task country_variable_prices: :environment do
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'country_variable_prices.csv')) do |values|
      headers = CountryVariablePrice.attribute_names

      attributes = headers.zip(values).to_h

      pruned_attributes = attributes.reject{ |k, _v| ["id", "created_at", "updated_at"].include?(k.to_s)  }

      sanitized_attributes = pruned_attributes.each{ |k, v| pruned_attributes[k] = nil if v == "NULL" }

      CountryVariablePrice.find_or_create_by(sanitized_attributes)
    end
  end
end
