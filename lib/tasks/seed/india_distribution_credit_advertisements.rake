namespace :seed do
  desc "Seed India Distribution Credit Advertisements"
  task india_distribution_credit_advertisements: :environment do
      # couldn't use csv here because one of the values is a string with commas - let's get old fashioned with it
    sql_1  = <<-SQL
    INSERT INTO advertisements (country_website_id, name, category, created_by_id, thumbnail_title, sidebar_title,
                                wide_title, thumbnail_copy, sidebar_copy, wide_copy, display_price,
                                display_price_modifier, thumbnail_image, sidebar_image, wide_image, image_caption,
                                status, action_link, thumbnail_link_text, sidebar_link_text, wide_link_text,
                                sort_order, is_default, updated_at, created_at)
    VALUES('8', 'Music not ready - Single', 'Single', 2740, NULL, 'Music not ready?', '', NULL,
           "Buy a Single credit, distribute when you're ready.", NULL, 9.99, 'Only', NULL, NULL, NULL, NULL, 'Active',
           '/products/423/add_to_cart?rel=1', '', 'Buy Credit', '', 0, 1, '2013-06-27 13:21:06', '2013-06-26 11:42:05');
    SQL
    sql_2 = <<-SQL
    INSERT INTO advertisements (country_website_id, name, category, created_by_id, thumbnail_title, sidebar_title,
                                wide_title, thumbnail_copy, sidebar_copy, wide_copy, display_price,
                                display_price_modifier, thumbnail_image, sidebar_image, wide_image, image_caption,
                                status, action_link, thumbnail_link_text, sidebar_link_text, wide_link_text,
                                sort_order, is_default, updated_at, created_at)
    VALUES('8', 'Album Not Ready? Buy credit', 'Album', 2740, NULL, 'Music not ready?', '', NULL,
           "Buy an album credit, distribute when you're ready.", NULL, 29.99, 'Only', NULL, NULL, NULL, NULL, 'Active',
           '/products/419/add_to_cart?rel=1', '', 'Buy Credit', '', 0, 1, '2013-06-27 13:20:51', '2012-05-09 12:10:34');
    SQL

    sql_3 = <<-SQL
    INSERT INTO advertisements (country_website_id, name, category, created_by_id, thumbnail_title, sidebar_title,
                                wide_title, thumbnail_copy, sidebar_copy, wide_copy, display_price,
                                display_price_modifier, thumbnail_image, sidebar_image, wide_image, image_caption,
                                status, action_link, thumbnail_link_text, sidebar_link_text, wide_link_text,
                                sort_order, is_default, updated_at, created_at)
    VALUES('8', 'Music not ready - Ringtone', 'Ringtone', 2740, NULL, 'Music not ready?', '', NULL,
           "Buy a Ringtone credit, distribute when you're ready.", NULL, 10.00, 'Only', NULL, NULL, NULL, NULL,
           'Active', '/products/439/add_to_cart', '', 'Buy Credit', '', 0, 1, '2014-03-07 15:36:50',
           '2013-06-26 11:42:14');
    SQL
    ApplicationRecord.connection.execute(sql_1)
    ApplicationRecord.connection.execute(sql_2)
    ApplicationRecord.connection.execute(sql_3)
  end
end
