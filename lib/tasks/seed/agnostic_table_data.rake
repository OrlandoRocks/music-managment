# frozen_string_literal: true

# Example usage:
# bundle exec rake "seed:create_agnostic_table_data[plan_features]"

namespace :seed do
  task :create_agnostic_table_data, [:table] => :environment do |_t, args|
    Rails.logger.info "Task started.........................?"
    table = args[:table].to_s
    file = Rails.root.join("db/seed/csv/#{table}.csv")
    class_object = table.classify.constantize

    created_record_ids = []

    CSV.foreach(file) do |record|
      record_hash = class_object.attribute_names.zip(record).to_h.transform_keys(&:to_sym).inject({}) do |h, (k, v)|
        new_v = v == "NULL" ? nil : v
        h[k] = new_v
        h
      end

      record = class_object.find_by(id: record_hash[:id])

      if record.blank?
        class_object.create!(record_hash)
        created_record_ids << record_hash[:id]
        Rails.logger.info "created record_id #{record_hash[:id]}"
      end
    end

    Rails.logger.info "Task ended!"
    Rails.logger.info "These record_ids created #{created_record_ids}"
  end


# Example usage:
# bundle exec rake "seed:update_agnostic_table_data[plan_features]"
  task :update_agnostic_table_data, [:table] => :environment do |_t, args|
    raise "DO NOT USE THIS" unless Rails.env.development?
    Rails.logger.info "Task started.........................?"
    table = args[:table].to_s
    file = Rails.root.join("db/seed/csv/#{table}.csv")
    class_object = table.classify.constantize

    updated_record_ids = []

    CSV.foreach(file) do |record|
      record_hash = class_object.attribute_names.zip(record).to_h.transform_keys(&:to_sym)
      record = class_object.find_by(id: record_hash[:id])

      if record.present?
        record.update!(record_hash)
        updated_record_ids << record_hash[:id]
        Rails.logger.info "updated record_id #{record_hash[:id]}"
      end
    end

    Rails.logger.info "Task ended!"
    Rails.logger.info "These record_ids updated #{updated_record_ids}"
  end
end
