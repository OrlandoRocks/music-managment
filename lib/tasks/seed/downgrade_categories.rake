namespace :seed do
  desc "Seed Plan Downgrade Categories"
  task plan_downgrade_categories: :environment do
    DowngradeCategory.delete_all
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'downgrade_categories.csv')) do |ppc|
      headers = DowngradeCategory.attribute_names
      DowngradeCategory.find_or_create_by(headers.zip(ppc).to_h)
    end
  end
end
