namespace :seed do
  desc "Seed Payout Programs"
  task payout_programs: :environment do
    PayoutProgram.delete_all
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'payout_programs.csv')) do |ppc|
      headers = PayoutProgram.attribute_names
      PayoutProgram.find_or_create_by(headers.zip(ppc).to_h)
    end
  end
end
