namespace :seed do
  desc "Seed Plan Downgrade Category Reasons"
  task plan_downgrade_category_reasons: :environment do
    DowngradeCategoryReason.delete_all
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'downgrade_category_reasons.csv')) do |ppc|
      headers = DowngradeCategoryReason.attribute_names
      DowngradeCategoryReason.find_or_create_by(headers.zip(ppc).to_h)
    end
  end
end
