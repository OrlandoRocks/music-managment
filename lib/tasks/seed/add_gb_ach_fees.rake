namespace :seed do
  desc "add gb payoneer_ach_fee"
  task add_gb_ach_fees: :environment do
      PayoneerAchFee.create(target_country_code: 'GB', transfer_fees: 1.00, minimum_threshold: 2.00,
                             target_currency: 'GBP', source_currency: 'USD')
      PayoneerAchFee.create(target_country_code: 'GB', transfer_fees: 1.00, minimum_threshold: 2.00,
                             target_currency: 'EUR', source_currency: 'USD')
  end
end
