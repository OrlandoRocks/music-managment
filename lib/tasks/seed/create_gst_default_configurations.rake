namespace :seed do
  desc "create default tax configuration"
  task load_default_tax_config: :environment do
    haryana_intra_state_tax_config = GstConfig.create(igst: nil, cgst: 9, sgst: 9, effective_from: DateTime.now)
    interstate_tax_config = GstConfig.create(igst: 18, cgst: nil, sgst: nil, effective_from: DateTime.now)
    india_country = Country.find_by(iso_code: "IN")
    india_country.country_states.update_all(gst_config_id: interstate_tax_config.id)
    india_country.country_states.find_by(iso_code: "IN-HR").update(gst_config_id: haryana_intra_state_tax_config.id)
  end
end
