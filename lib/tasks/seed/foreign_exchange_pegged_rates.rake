namespace :seed do
  desc "Seed Foreign Exchange Pegged Rates"
  task foreign_exchange_pegged_rates: :environment do
    # should only be run once, at least for now
    CSV.foreach(Rails.root.join('db', 'seed', 'csv', 'foreign_exchange_pegged_rates.csv')) do |ppc|
      headers = ForeignExchangePeggedRate.attribute_names
      ForeignExchangePeggedRate.find_or_create_by(headers.zip(ppc).to_h)
    end
  end
end
