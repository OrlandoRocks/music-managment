namespace :transient do
  task seed_dispute_statuses: :environment do
    file = Rails.root.join("db/seed/csv/dispute_statuses.csv")
    Seed.seed(file)
  end
end
