desc 'Run spec suite and combine exit codes'
task :local_tests do
  puts "Migrating Database for #{test.capitalize} Suite"
  `rake db:migrate RAILS_ENV=test`

  Rake::Task["maintenance:reset_logs"].invoke

  test_names =["spec"]
  tests = ["rake spec RAILS_ENV=test 2> /dev/null"]

  is_success = true
  tests.each do |task|
    puts "Running #{test_names.include?(task.split(" ").first.to_s) ? task.split(" ").first.to_s.capitalize : task.split(" ").second.to_s.capitalize } Suite"
    success = system(task)
    is_success = false if not success
  end
  exit(is_success ? 0 : 1)
end
