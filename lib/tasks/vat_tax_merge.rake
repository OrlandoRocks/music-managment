# frozen_string_literal: true

TASKS = %w[
  rename_vat_feature_flag
  vat:update_us_territories_corporate_entity
  add_euro_exchange_rates
  vat:updated_corporate_entity_attributes
  invoice:update_invoice_sent_at
  tc_vat:cache_vat_applicability
  vat:add_corporate_entity_invoice_prefix
  transient:add_country_audit_sources
  transient:add_address_locked_flag
  transient:create_compliance_info_fields_locked_flags
].freeze

namespace :vat_tax_merge do
  task run_tasks: :environment do
    TASKS.each do |task|
      Rake::Task[task].invoke
    end
  end
end
