namespace :backfill do
  desc 'backfill of all albums to Believe'
  task golive: :environment do
    BackfillGoliveDateService.new(verbose: true).run
  end

  desc 'estimate the golive backfill'
  task golive_estimate: :environment do
    BackfillGoliveDateService.new(verbose: true).display_count
  end
end
