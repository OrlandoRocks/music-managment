require "#{Rails.root}/lib/logging"
namespace :canada do
  desc 'Get invoices for autorenewals'
  task :batch_convert_users =>  :environment do
    subject = "One Time US to CA conversion"
    start_time = Time.now
    log(subject, "Starting",:time=>start_time )
    log(subject, "Limiting to #{ENV['LIMIT']} people") if ENV['LIMIT']
    people_to_convert = Person.find_by_sql(convertable_query(ENV['LIMIT']))
    log(subject, "Got people to convert",:people_count=>people_to_convert.size, :time=>Time.now)
    people_to_convert.each do |person|
      status = person.convert_to_canadian
      log(subject, "Converting to CA",:person_id=>person.id,:status=>status,:country_website_id=>person.reload.country_website_id, :balance_currency=>person.person_balance.currency)
    end
    log(subject, "Finshed",:time=>Time.now, :time_taken => pretty_time_since(start_time))
  end
end


def convertable_query(limit = nil)
  "SELECT p.*
  FROM people p
  LEFT JOIN purchases pu ON pu.person_id = p.id
  LEFT JOIN
  	(SELECT pu.person_id AS person_id
  	FROM purchases pu
  	INNER JOIN people p ON p.id = pu.person_id
    WHERE paid_at IS NOT NULL AND p.country = 'Canada'
  	GROUP BY person_id) with_purchases ON with_purchases.person_id = p.id
  LEFT JOIN balance_adjustments ba ON ba.person_id = p.id
  LEFT JOIN braintree_transactions bt ON bt.person_id = p.id
  LEFT JOIN paypal_transactions pt ON pt.person_id = p.id
  LEFT JOIN person_transactions pet ON pet.person_id = p.id
  LEFT JOIN stored_bank_accounts sba ON sba.person_id = p.id
  LEFT JOIN stored_paypal_accounts spa ON spa.person_id = p.id
  LEFT JOIN person_intakes pi ON pi.person_id = p.id
  LEFT JOIN inventories inv ON inv.person_id = p.id
  LEFT JOIN check_transfers ct ON ct.person_id = p.id
  LEFT JOIN paypal_transfers ppt ON ppt.person_id = p.id
  LEFT JOIN certs ON certs.person_id = p.id
  LEFT JOIN invoices i ON i.person_id = p.id
  LEFT JOIN invoice_settlements invs ON invs.invoice_id = i.id
  WHERE p.country = 'Canada'
  AND p.country_website_id = 1
  AND with_purchases.person_id IS NULL
  AND ba.id IS NULL
  AND bt.id IS NULL AND pt.id IS NULL AND pet.id IS NULL
  AND sba.id IS NULL AND spa.id IS NULL AND pi.id IS NULL
  AND inv.id IS NULL AND ct.id IS NULL AND ppt.id IS NULL
  AND certs.id IS NULL AND invs.id IS NULL
  AND p.email NOT IN ('jgluski@hotmail.com' ,'teroleague@rogers.com' ,'tedlaz@aol.com' ,'Schmange2@Rogers.com' ,'skammadix@gmail.com' ,'rodney07@gmail.com' ,'rhilton2@telus.net' ,
  'pharrison5@cogeco.ca' ,'rcgirl20@gmail.com' ,'nathancarmola@live.ca' ,'lebaronandyou@gmail.com' ,'l3magazine@hotmail.com' ,'kelly@petersackaney.com' ,
  'jasa27@yahoo.com' ,'jason.theriault@live.com' ,'jason@tunecore.com' ,'jacob.paquette@gmail.com' ,'info@jasonkechely.com' ,'israell.isaac@gmail.com' ,
  'franklinmckay@hotmail.ca' ,'g@gerryadroberts.com' ,'fandangoband@yahoo.ca' ,'djlithium@hotmail.com' ,'dlemaire@shaw.ca' ,'cproudfoot10@ubishops.ca' ,
  'caravangroupmeditations@gmail.com' ,'cgcourtney@gmail.com' ,'arose_jb@hotmail.com')
  GROUP BY p.id
  #{"LIMIT " + limit if !limit.nil?};"

end
