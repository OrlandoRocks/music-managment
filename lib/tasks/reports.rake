# 2009-08-20 -- MJL -- Removing the sessions namespace since sessions have nothing to do with reports (and the code is duplicated in the tunecore.rake task)
namespace :reports do
  desc "force the reports to update"
  task :update => :environment do
    puts "Product Sales Daily"
    Tunecore::Reports::ProductSales.find_by(name: 'Product Sales Daily').record
    puts "Annual Renewals"
    Tunecore::Reports::AnnualRenewals.find_by(name: 'Annual Renewals').record(Date.today >> 15)
  end

  desc "update the album earnings"
  task :sales_update => :environment do
    puts "Recording album sales"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(units_sold) as sold from sales_records where sale_type = 'dl' AND song_id is NULL AND usd_total_cents > 0 group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET album_sale_count = sold")

    puts "Recording Song Sales"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(units_sold) as sold from sales_records where sale_type = 'dl' AND song_id is NOT NULL AND usd_total_cents > 0 group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET song_sale_count = sold")

    puts "Recording Promo Downloads"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(units_sold) as sold from sales_records where sale_type = 'dl' AND usd_total_cents = 0 group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET promo_downloads = sold")

    puts "Recording Number of Paid Streams"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(units_sold) as sold from sales_records where sale_type = 'ss' group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET paid_streams = sold")

    puts "Recording Promo Streams"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(units_sold) as sold from sales_records where sale_type = 'st' group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET promo_streams = sold")

    puts "Updating Revenue"
    Album.connection.execute("UPDATE albums LEFT JOIN (SELECT album_id, sum(usd_total_cents) as earned from sales_records group by album_id) AS sa_rec on albums.id = sa_rec.album_id SET total_revenue_cents = earned")
  end

  desc "Create custom report for volume customers"
  task :create_custom_report => :environment do
    albums = Album.find_by_sql("select distinct albums.name, apple_identifier,
      upcs.number
      from albums
            inner join songs
            on albums.id = songs.album_id
            inner join upcs
            on upcs.upcable_type = 'Album'
            and upcs.upcable_id = albums.id
            inner join people
            on people.id = albums.person_id
            where
            people.email = 'jk@lauschmedia.de'
            and people.id = 503592
            and albums.takedown_at is null
            and albums.finalized_at is not null
            and albums.known_live_on is not null")

    output_filename = "#{Rails.root}/tmp/custom_report.xlsx"
    SimpleXlsx::Serializer.new(output_filename) do |doc|
      doc.add_sheet('Tax Info') do |sheet|
        sheet.add_row(['Album Name', 'Apple ID', 'UPC'])
        albums.each do |album|
          sheet.add_row([album.name, album.apple_identifier, album.number])
        end
      end
    end
  end

  desc "Emails a csv list of upc's that have been delivered from a start date to end date. Example: rake 'reports:send_upc_report_email[true, gracenote, 2006-01-01 00:00:00, 2006-02-01 00:00:00]' "
  task :send_upc_report_email, [:production_run, :store_short_name, :start, :finish] => [:environment] do |command, args|
    log_upc_report_message({params: args})
    start_time = Time.current
    recipients = ["productteam@tunecore.com"]

    dryrun = (args[:production_run] == 'true') ? false : true

    if args[:start].nil? || args[:finish].nil?
      start  = 7.days.ago.strftime('%Y-%m-%d 00:00:00')
      finish = Time.current.strftime('%Y-%m-%d 00:00:00')
      log_upc_report_message({state: "defaulting_to_last_week", start: start, finish: finish, dryrun: dryrun})
    else
      start = args[:start]
      finish = args[:finish]
    end

    upcs = Distribution.get_delivered_upc(args[:store_short_name], start, finish)
    upcs_str = upcs.join(', ')

    if upcs.present? && !dryrun
        log_upc_report_message({state: "processing_email", recipients: recipients, delivery_type: "email", dryrun: dryrun, upc_nums: upcs_str})
        ReportNotifier.mail_delivered_upcs(upcs, { recipients: recipients, store_short_name: args[:store_short_name], start_date: start, end_date: finish }).deliver
        log_upc_report_message({state: "success"})
    end

    end_time = Time.current
    log_upc_report_message({state: "completed", finish_time: end_time, total_upcs: upcs.size, total_time: (end_time-start_time), upc_nums: upcs_str})
  end

end

def log_upc_report_message(params)
  message = "rake_task=reports:send_upc_report_email"
  params.each {|key, value| message += " #{key}=#{value}"}
  puts message
  Rails.logger.info message
end
