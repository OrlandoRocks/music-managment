namespace :youtube do 
    task restore_deleted_channel_data:  :environment do 
        start_date = ENV.fetch("START_DATE")
        end_date = ENV.fetch("END_DATE", Time.now)

        data_to_restore = ExternalServiceIdDelete
                            .where("created_at > ? AND created_at < ?", start_date, end_date)
                            .group(
                              :identifier,
                              :service_name,
                              :artist_name,
                              :person_id)

        creatives_to_be_matched = []     
        
        Rails.logger.info "Restoring #{data_to_restore.count} IDs from external_service_id_deletes table"
        data_to_restore.each do |row|
            creative = Creative.joins(:artist)
            .find_by(
                person_id: row.person_id, 
                artists: 
                { name: row.artist_name }
            )
            ExternalServiceId.find_or_create_by(
                linkable_id: creative.id,
                linkable_type: "Creative",
                identifier: row.identifier,
                service_name: row.service_name
            )
            creatives_to_be_matched << creative.id
        end 

        Rails.logger.info "Sending #{creatives_to_be_matched.count} records to be matched"
        creatives_to_be_matched.each_slice(1000) do |batch|
          ArtistIdMatcherWorker.perform_async(creatives_to_be_matched)
        end
    end 
end 