namespace :backfill_s3_streaming_asset do
  desc "backfill s3_streaming_asset in song table"
  task run: :environment do
    CALLBACK_URL = "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_streaming_asset".freeze

    MP3_OUTPUT_FORMAT = {
      extension: "mp3"
    }
    songs_count = 0
    invalid_song_ids = []

    songs = Song.includes(:s3_orig_asset, :s3_asset)
                .where(s3_streaming_asset_id: nil)
                .where("s3_asset_id IS NOT NULL or s3_orig_asset_id IS NOT NULL")

    p "Bakckfilling for s3_streaming_asset started"

    songs.find_each do |song|
      s3_asset = song.s3_orig_asset || song.s3_asset

      # For some old songs asset is missing from db
      next if s3_asset.nil?

      p "Straming started for song id: #{song.id}"

      # For some old songs albums are missing from db
      if song.album.nil?
        p "For Song Id #{song.id} album is missing"
        next
      end

      # Check if streaming file is already generated or not
      streaming_file_path = "#{s3_asset.key}.#{MP3_OUTPUT_FORMAT[:extension]}"
      s3_object = Aws::S3::Object.new(s3_asset.bucket, streaming_file_path)

      if s3_object.exists?
        begin
          song.update!(
            {
              s3_streaming_asset: S3Asset.create(
                key: streaming_file_path,
                bucket: s3_asset.bucket
              )
            }
          )
        rescue ActiveRecord::RecordInvalid => e
          invalid_song_ids << song.id
          Rails.logger.info "#{e.record.errors} for Song Id: #{song.id}"
          Airbrake.notify("Error running backfill_s3_streaming_asset: #{e.record.errors} for Song Id: #{song.id}")
        end
      else

        # if original asset is mp3
        s3_key_extension = File.extname(s3_asset.key).delete(".")

        if s3_key_extension == MP3_OUTPUT_FORMAT[:extension]

          song.update(s3_streaming_asset_id: s3_asset.id)
        else

          # This call update streming asset api to update the assets
          AssetsBackfill::MigrateNonFlac.new(
            {
              song: song,
              asset: s3_asset,
              destination_asset_key: streaming_file_path,
              output_format: MP3_OUTPUT_FORMAT,
              callback_url: CALLBACK_URL
            }
          ).run
        end
      end
      songs_count += 1
    end
    p "Bakckfilling for s3_streaming_asset completed for #{songs_count} songs"
    p "Invalid songs for which s3_streaming not completed are #{invalid_song_ids}"
  end
end