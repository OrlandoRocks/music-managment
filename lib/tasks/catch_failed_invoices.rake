require "#{Rails.root}/lib/slack_notifier"
namespace :transactions do
  desc "find successful braintree transactions with failed invoices"
  task find_failed_invoices_paid_on_bt: :environment do
    query = <<~SQL
      SELECT bt.id FROM braintree_transactions bt 
        LEFT OUTER JOIN invoices i ON bt.invoice_id = i.id 
        WHERE bt.status = 'success' AND action = 'sale' AND DATE(bt.created_at) >= SUBDATE(CURDATE(),1) AND i.id is NULL
        ORDER BY bt.id DESC
    SQL
    failed_bt_transactions = ActiveRecord::Base.connection.execute(query).to_a.flatten
    msg = failed_bt_transactions.blank? ? "No Failed Transactions" : "Following Braintree transactions have failed invoices failed_transactions\n #{failed_bt_transactions}"
    notify_in_slack(msg, 'Braintree Transactions')
  end
  desc "find successful paypal transactions with failed invoices"
  task find_failed_invoices_paid_on_pt: :environment do
    query = <<~SQL
      SELECT pt.id FROM paypal_transactions pt 
        LEFT OUTER JOIN invoices i ON pt.invoice_id = i.id 
        WHERE pt.ack = 'Success' AND pt.action = 'sale' AND pt.status = 'Completed' AND DATE(pt.created_at) >= SUBDATE(CURDATE(),1) AND i.id is NULL
        ORDER BY pt.id DESC
    SQL
    failed_pt_transactions = ActiveRecord::Base.connection.execute(query).to_a.flatten
    msg = failed_pt_transactions.blank? ? "No Failed Transactions" : "Following Paypal transactions have failed invoices failed_transactions\n #{failed_pt_transactions}"
    notify_in_slack(msg, 'Paypal Transactions')
  end
end
