namespace :cart do
  desc "Add a store to customer's cart for each release that does not have that store"
  task :add_store => :environment do
    unless ENV['store_id']
      abort "Please set the 'store_id' environment variable: 'bundle exec rake cart:add_store store_id=<store ID> [verbose=<true>] [limit=<number of albums to run against>]"
    end

    sql = "select a.id, a.person_id from albums a left outer join salepoints sp on a.id = sp.salepointable_id and sp.salepointable_type = 'Album' and sp.store_id = #{ENV['store_id']} where a.finalized_at is not null and a.takedown_at is null and a.is_deleted = false and sp.id is null and a.album_type in ('Album','Single') order by a.person_id"

    if ENV['limit']
      sql += " limit #{ENV['limit']}"
    end

    albums = Album.find_by_sql(sql)

    @people = {}

    albums.each do | album |
      next if album.person_id.nil?

      puts album.inspect if ENV['verbose'] == "true"

      sp = Salepoint.create(store_id: ENV['store_id'], salepointable_id: album.id, status: 'new', salepointable_type: 'Album')
      person = get_person album.person_id
      Product.add_to_cart(person, sp)
    end
  end

  desc "Add store(s) to release(s) and to cart; Requires STORE_IDS and RELEASE_UPCS"
  task :add_stores_to_releases => :environment do |t|
    ENV['STORE_IDS'] || ENV['RELEASE_UPCS'] || raise("Requires STORE_IDS and RELEASE_UPCS")
    store_ids = ENV['STORE_IDS'].split(',')
    release_upcs = ENV['RELEASE_UPCS'].split(',')
    upcs = Upc.where(number: release_upcs, inactive: false).includes(upcable: :person)

    stores_to_add = Store.where(id: store_ids)
    upcs.each do |upc|
      album = upc.upcable
      salepoints_added, stores_not_added = album.add_stores(stores_to_add)

      salepoints_added.each do |salepoint|
        log(t,"Added salepoint #{salepoint.inspect} to album=#{album.id} with upc=#{upc.number}")
        purchase = Product.add_to_cart(album.person, salepoint)
        if purchase
          log(t,"Added product to cart purchase=#{purchase.inspect}")
        else
          log(t,"Failed to add product to cart salepoint=#{salepoint.inspect}")
        end
      end

      stores_not_added.each do |store|
        log(t,"Failed to add store=#{store.id} to album=#{album.id} with upc=#{upc.number}")
      end
    end
  end

  def get_person id
    person = @people.fetch(id, false)
    return person if person

    person = Person.find(id)
    @people[id] = person
    person
  end

end