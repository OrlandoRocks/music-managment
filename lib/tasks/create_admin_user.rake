namespace :create do
  desc "creates user to use it auto rejection"
  task admin_user: :environment do
    person = Person.create!(
      email: 'admin_user@tunecore.com',
      name: 'Admin User',
      password: ENV['ADMIN_USER_PASSWORD'],
      password_confirmation: ENV['ADMIN_USER_PASSWORD'],
      is_verified: true,
      country: 'US',
      accepted_terms_and_conditions_on: Time.now
    )
    person.roles << Role.find_by(name: "Content Review Manager")
  end
end
