# frozen_string_literal: true

# bundle exec rake "person_plan:upgrade_person_plan[#person_id, #purchase_id]"

desc "Upgrades person_plan"
namespace :person_plan do
  task :upgrade_person_plan, [:person_id, :purchase_id] => :environment do |_, args|
    Rails.logger.info "Starting task..."

    person = Person.find(args[:person_id])
    purchase = Purchase.find(args[:purchase_id])
    plan = purchase.product.plan

    raise "Person/Purchase mismatch!" if purchase.person_id != person.id
    raise "Purchase isn't for plan!" if plan.blank?
    raise "Not an upgrade!" if person.plan.blank? || person.plan.id >= plan.id

    PersonPlanCreationService.call(
      person_id: person.id,
      plan_id: plan.id,
      purchase_id: purchase.id
    )

    Rails.logger.info "Complete!"
  end
end
