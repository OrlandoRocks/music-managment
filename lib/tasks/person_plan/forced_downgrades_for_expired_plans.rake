# frozen_string_literal: true

# bundle exec rake person_plan:forced_downgrade_for_expired_plans

desc "Forces downgrades for plans expired past the grace period"
namespace :person_plan do
  task forced_downgrade_for_expired_plans: :environment do
    Rails.logger.info "Starting task..."

    PersonPlan::ForcedDowngradeWorkflowWorker.perform_async

    Rails.logger.info "Complete!"
  end
end
