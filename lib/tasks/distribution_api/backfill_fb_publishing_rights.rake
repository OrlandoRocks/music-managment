namespace :distribution_api do
  desc "backfill FB Publishing rights for Bytedance albums"
  task backfill_fb_publishing_rights: :environment do
    Rails.logger.info "Beginning rake task"

    songs = Song.joins(:album).where(albums: { person_id: ENV["BYTEDANCE_USER_ID"] })

    songs.each do |song|
      song_copyright_claim = SongCopyrightClaim.find_or_initialize_by(song_id: song.id)

      song_copyright_claim.update(
        {
          claimant_id: ENV["BYTEDANCE_USER_ID"],
          claimant_has_songwriter_agreement: true,
          claimant_has_pro_or_cmo: false,
          claimant_has_tc_publishing: false
        }
      )
    end

    Rails.logger.info "Rake task completed"
  end
end
