namespace :distribution_api do
  task add_bytedance_distribution_api_service: :environment do
    distribution_api_service = DistributionApiService.create(name: "Bytedance")
    distribution_api_service.update(uuid: "d848b5c5-c187-4002-b5a0-a9999df19ac5")
  end
end
