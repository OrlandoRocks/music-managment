namespace :distribution_api do
  desc "add automator for distribution api ingested albums"
  task add_automator: :environment do
    distribution_api_ingestion_log = DistributionApiIngestionLog.where(status: :ingested)
    distribution_api_ingestion_log.each do |log|
      album = DistributionApiAlbum.find_by(source_album_id: log.source_album_id)&.album
      next unless album.present?

      Album::AutomatorService.add_and_finalize_automator(album)
    end
  end
end
