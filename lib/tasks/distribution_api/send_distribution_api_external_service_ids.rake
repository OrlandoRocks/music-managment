namespace :distribution_api do
  desc "Send External Service IDs created using Distribution API Ingestion"
  task send_distribution_api_external_service_ids: :environment do
    # date Format YYYY-MM-DD
    esid_creation_date = Date.parse(ENV["ESID_CREATION_DATE"]) rescue Date.yesterday

    DistributionApi::ExternalServiceIdApi::Base.run(
      esid_creation_date: esid_creation_date,
      force_send_esid: ENV["FORCE_SEND_ESID"],
      distribution_api_service_name: ENV["DISTRIBUTION_API_SERVICE_NAME"]
    )
  end
end
