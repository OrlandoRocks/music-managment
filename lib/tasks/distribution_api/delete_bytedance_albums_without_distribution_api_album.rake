namespace :distribution_api do
  task delete_bytedance_albums_without_distribution_api_album: :environment do
    albums = Album.joins(
      "left join distribution_api_albums on albums.id = distribution_api_albums.album_id"
    ).where(
      person_id: ENV["BYTEDANCE_USER_ID"]
    ).where(
      distribution_api_albums: {
        id: nil
      }
    )

    albums.each do |album|
      next if Inventory.used?(album)
      DistributionApi::DeletionService.call(album.id)
    end
  end
end
