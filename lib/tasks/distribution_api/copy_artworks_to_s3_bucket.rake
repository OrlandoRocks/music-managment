namespace :distribution_api do
  desc "copy artwork to bytedance s3 buckets"
  task copy_artworks_to_s3_bucket: :environment do
    Rails.logger.info "Beginning rake task"
    distribution_api_service = DistributionApiService.find_by(name: "Bytedance")

    s3_folder = "artwork/#{DEPLOY_ENV}/#{distribution_api_service.uuid}"

    albums = Album.includes(:upcs, :artwork).where(person_id: ENV["BYTEDANCE_USER_ID"])

    destination_bucket = Aws::S3::Bucket.new("distributionapi.tunecore.com")

    albums.each do |album|
      next unless album.artwork.present? && album.artwork.s3_asset.present?

      original_artwork_asset = album.artwork.s3_asset
      source_bucket = Aws::S3::Bucket.new(original_artwork_asset.bucket)

      begin
        new_object = destination_bucket.object("#{s3_folder}/#{album.upc.number}.tiff")
        new_object.copy_from(source_bucket.object(original_artwork_asset.key))
      rescue Aws::S3::Errors::NoSuchKey
        Rails.logger.info "Artwork does not exist for Album #{album.id}"
      end
    end

    Rails.logger.info "Rake task completed"
  end
end
