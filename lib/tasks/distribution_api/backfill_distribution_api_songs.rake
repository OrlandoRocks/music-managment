namespace :distribution_api do
  desc "backfill distribution api songs"
  task backfill_distribution_api_songs: :environment do
    Rails.logger.info "Started Rake Task"

    distribution_api_albums = DistributionApiAlbum.joins(
      "
        left join
        distribution_api_songs
        on
        distribution_api_albums.id = distribution_api_songs.distribution_api_album_id
      "
    ).where(
      "
        distribution_api_songs.id is null
      "
    )

    distribution_api_albums.each do |distribution_api_album|
      distribution_api_ingestion_log = DistributionApiIngestionLog.find_by(
        source_album_id: distribution_api_album.source_album_id,
        status: :ingested
      )

      next if distribution_api_ingestion_log.blank?

      begin
        s3_downloader = DistributionApi::S3Downloader.new(
          asset_url: distribution_api_ingestion_log.json_path
        )

        json_parser = DistributionApi::JsonParser.new(File.read(s3_downloader.download))

        distribution_api_album.album.songs.each do |tunecore_song|
          source_song =
            json_parser.tunecore_song_attributes(distribution_api_album.album_id).find { |song|
              (
                tunecore_song.optional_isrc.present? &&
                song[:optional_isrc] == tunecore_song.optional_isrc
              ) ||
                (
                  tunecore_song.optional_isrc.blank? &&
                  song[:name].split.first.casecmp(tunecore_song.name.split.first).zero?
                )
            }

          DistributionApiSong.create(
            source_song_id: source_song[:source_song_id],
            distribution_api_album_id: distribution_api_album.id,
            song_id: tunecore_song.id
          )
        end
      ensure
        s3_downloader.cleanup
      end
    end

    Rails.logger.info "Rake Task completed"
  end
end
