namespace :distribution_api do
  desc "Backfill missing source ids"
  task backfill_missing_source_id: :environment do
    DistributionApiAlbum.find(2602).update(album_id: 4892026)
    DistributionApiSong.find(2806).update(song_id: 14923993)

    DistributionApiAlbum.find(15213).update(album_id: 5175327)
    DistributionApiSong.find(22838).update(song_id: 15459079)
    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
