namespace :distribution_api do
  desc "Add FB Salepoint for Bytedance Albums"
  task add_fb_salepoint_to_bytedance_albums: :environment do
    Rails.logger.info "Beginning rake task"

    ISRCS = %w[
      BXM842100033
      SGB502205608
      BXIGF2200001
      SGB502201762
      SGB502201763
      SGB502202117
      SGB502201281
      SGB502202211
      BCEX72100010
      BCEX72100007
      BCEX72100005
      BCEX72100011
      BCEX72100012
      BRT9E2200002
      SGB502202420
      SGB502106727
      SGB502106014
      SGB502106435
      SGB502106721
      SGB502202431
      SGB502201639
      SGB502202170
      SGB502202763
      SGB502203227
      SGB502106191
      BXVCJ2100007
      SGB502105282
      SGB502105279
      SGB502105281
      SGB502105280
      BCHBB2100202
      SGB502101620
      QZES71925757
      QM24S2107064
      QZMEV2106219
      SGB502104900
      QZNJW2105543
      SGB502106183
      SGB502106178
      SGB502106199
      SGB502106198
      SGB502246017
      SGB502246002
      QM24S2200194
      SGB502203194
      TCAFS2106558
      SGB502106830
      SGB502205348
      SGB502205347
      SGB502105741
      SGB502108548
      SGB502203980
      SGB502205483
      SGB502101397
      SGB502105018
      SGB502103180
      SGB502102136
      SGB502205719
      SGB502204284
      BXVCJ2100008
      BRQTC2200001
      BCDT12200001
      BCHBB2200001
      BCMPW2200001
      BCMPW2100007
      BCMPW2100003
      IDA832200002
      IDA832200005
      IDA832200023
      IDA832200033
      SGB502206182
      SGB502207100
      SGB502205780
      SGB502205782
      SGB502205781
      SGB502205779
      SGB502203331
    ].freeze

    stores = Store.where(id: Store::FB_REELS_STORE_ID)

    current_user = Person.find(ENV["BYTEDANCE_USER_ID"])

    album_ids = Song.where("tunecore_isrc in (?) or optional_isrc in (?)", ISRCS, ISRCS).pluck(:album_id)

    albums = Album.find(album_ids)

    albums.each do |album|
      salepoints, stores_not_added = album.add_stores(stores)

      AddPaidSalepointsService.call(current_user, album, salepoints, album.apple_music) if salepoints.present?
    end

    Rails.logger.info "Rake task completed"
  end
end
