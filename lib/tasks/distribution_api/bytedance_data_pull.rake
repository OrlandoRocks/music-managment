namespace :distribution_api do
  desc "data pull for bytedance albums"
  task :bytedance_data_pull, [:report_start_date, :rejected_releases_only] => [:environment] do |_command, args|
    Rails.logger.info "Beginning rake task"

    start_date = args[:report_start_date]
    rejected_releases_only = args[:rejected_releases_only]

    if start_date.present?
      begin
        DateTime.strptime(start_date, "%Y-%m-%d")
      rescue ArgumentError
        abort("Please provide a valid start date (e.g. 2021-03-22)")
      end
    end

    # Moving methods inside the task to prevent polluting global scope
    def artist_names(creatives)
      creatives.present? ? creatives.map { |c| c&.artist&.name }&.join(",") : ""
    end

    def album_status(album)
      return "taken down" if album.takedown_at.present?
      return "live" if album.legal_review_state == "APPROVED"

      "rejected"
    end

    headers = [
      "source_album_id",
      "album_upc",
      "album_title",
      "album_metadata_language",
      "original_release_date",
      "timed_release_type",
      "release_date",
      "primary_genre_id",
      "primary_genre_name",
      "secondary_genre_id",
      "secondary_genre_name",
      "label_name",
      "artist_names",
      "release_type",
      "territories",
      "various_artists",
      "album_store_ids",
      "status",
      "facebook_track_monetization",
      "source_song_id",
      "song_sequence",
      "song_title",
      "track_lyric_language",
      "song_artist_names",
      "song_original_release_date",
      "song_isrc",
      "clean_version",
      "instrumental",
      "lyrics",
      "sample_start_time"
    ]
    headers << "rejection_ids" if rejected_releases_only.present?

    csv_file = rejected_releases_only.present? ? "bytedance_rejected_data_pull.csv" : "bytedance_data_pull.csv"
    output_csv_path = File.join("tmp", csv_file)
    output_csv = CSV.open(output_csv_path, "w", write_headers: true, headers: headers)

    albums = Album.where(person_id: ENV["BYTEDANCE_USER_ID"])
    albums = albums.where("created_at > ?", start_date) if start_date.present?

    if rejected_releases_only.present?
      rejection_ids = [20, 21, 22, 30, 31, 32, 40, 42] # Provided by ByteDance
      albums = albums.where("legal_review_state != 'APPROVED'").select do |album|
        last_audit = album.review_audits.order(created_at: :desc).first
        # Casting to array to cover cases where there are no ReviewAudit
        (last_audit&.review_reason_ids.to_a & rejection_ids).any?
      end
    end

    songs = Song.joins(
      :distribution_api_song,
      [album: :distribution_api_album]
    ).where(album_id: albums.pluck(:id))

    total_songs = songs.count
    songs.each_with_index do |song, index|
      printf("\rProcessing: %d/%d", index, total_songs)

      album = song.album
      next if album.distribution_api_album.blank?

      row_data = [
        album.distribution_api_album&.source_album_id,
        album.upc.number,
        album.name,
        album.language_code,
        album.orig_release_date,
        album.timed_release_timing_scenario,
        album.golive_date.in_time_zone("Eastern Time (US & Canada)"),
        album.primary_genre&.id,
        album.primary_genre&.name,
        album.secondary_genre&.id,
        album.secondary_genre&.name,
        album.label_name,
        artist_names(album.creatives),
        album.album_type,
        album.country_iso_codes.join(","),
        album.is_various,
        album.salepoints.where.not(finalized_at: nil)&.pluck(:store_id)&.uniq,
        album_status(album),
        TrackMonetization.where(song_id: song.id, store_id: 83).present?,
        song.distribution_api_song&.source_song_id,
        song.track_number,
        song.name,
        song.metadata_language_code&.code,
        artist_names(song.creatives),
        song.previously_released_at,
        song.isrc,
        song.clean_version,
        song.instrumental,
        song.lyric&.content&.tr("\n", " "),
        song.song_start_times.pluck(:start_time).join(",")
      ]

      if rejected_releases_only.present?
        # Not the most efficient solution but since this is
        # a transient task letting it be
        audit = album.review_audits.order(created_at: :desc).first
        row_data << audit.review_reason_ids
      end

      output_csv << row_data
    end
    printf("\n")

    output_csv.close

    RakeDatafileService.upload(output_csv_path, overwrite: "y")

    Rails.logger.info "Rake task completed"
  end
end
