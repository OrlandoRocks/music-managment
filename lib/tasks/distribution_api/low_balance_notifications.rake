namespace :distribution_api do
  desc "send low balance notification emails"
  task send_low_balance_notifications: :environment do
    DistributionApi::LowBalanceNotificationService.run
  end
end
