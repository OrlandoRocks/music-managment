namespace :distribution_api do
  task add_youtube_salepoint_to_bytedance_albums: :environment do
    current_user = Person.find(ENV["BYTEDANCE_USER_ID"])

    ingested_album_logs = DistributionApiIngestionLog.where(status: :ingested)

    stores = Store.where(id: Store::GOOGLE_STORE_ID)

    ingested_album_logs.each do |log|
      album = DistributionApiAlbum.find_by(source_album_id: log.source_album_id)&.album
      next unless album.present?

      salepoints, stores_not_added = album.add_stores(stores)

      AddPaidSalepointsService.call(current_user, album, salepoints) if salepoints.present?
    end
  end
end
