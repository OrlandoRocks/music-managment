namespace :maintenance do
  DEV_LOGS = ["bullet","cms","development","emails","test","trend_report_cache_misses"]

  desc "Changes album_only flag on songs for a list of releases. Requires that all supplied stores accepts album only tracks."
  task :change_album_only => :environment do
    ENV['PERSON_ID']  || raise("need PERSON_ID")
    ENV['ALBUM_UPCS'] || raise("need ALBUM_UPCS")
    ENV["ALBUM_ONLY"] || raise("need ALBUM_ONLY")
    ENV["STORE_IDS"] || raise("need STORE_IDS")

    puts Rails.logger.info("Running change_album_only for PERSON_ID=#{ENV["PERSON_ID"]} ALBUM_UPCS=#{ENV["ALBUM_UPCS"]} ALBUM_ONLY=#{ENV["ALBUM_ONLY"]} STORE_IDS=#{ENV["STORE_IDS"]}")

    album_upcs = ENV["ALBUM_UPCS"].split(",")
    store_ids  = ENV["STORE_IDS"].split(",")
    stores     = Store.where("id in (?)", store_ids)

    album_only_stores = Store.stores_supporting_album_only

    unless stores.all?{ |s| album_only_stores.include?(s) }
      raise "Can only supply stores that are album only stores"
    end

    person = Person.find(ENV["PERSON_ID"])
    albums = person.albums.joins(:upcs).where("upcs.number in (?)", album_upcs)

    to_redeliver = []

    begin
      Song.transaction do

        albums.each do |album|
          redeliver_album = false

          album.songs.each do |song|
            puts Rails.logger.info("Updating album_only for song: #{song.id}")
            song.album_only = ["1", "true", "TRUE"].include?(ENV["ALBUM_ONLY"]) ? true : false
            redeliver_album = true
            song.save!
            puts Rails.logger.info("Saved song #{song.id} as album_only: #{song.album_only? ? "true" : "false" }")
          end

          if redeliver_album
            salepoints = album.salepoints.where("salepoints.store_id in (?)", store_ids)

            salepoints.each do |salepoint|
              puts Rails.logger.info("Marking salepoint for #{salepoint.store.name} (#{salepoint.id}) for redelivery")
              to_redeliver << salepoint
            end
          end

        end
      end

      #Send metadata update
      to_redeliver.each { |salepoint|
        puts Rails.logger.info("Sending meta data update for salepoint #{salepoint.id}")
        salepoint.send_metadata_update
      }

    rescue StandardError => e
      puts Rails.logger.info("Exception: #{e}")
    end

    puts Rails.logger.info("Finished change_album_only")
  end

  desc "Changes variable pricing on a list of releases for a collection of stores"
  task :change_variable_pricing => :environment do
    ENV['PERSON_ID']  || raise("need PERSON_ID")
    ENV['ALBUM_UPCS'] || raise("need ALBUM_UPCS")
    ENV["TRACK_VARIABLE_PRICE_ID"] || ENV["ALBUM_VARIABLE_PRICE_ID"] || raise("need TRACK_VARIABLE_PRICE_ID or ALBUM_VARIABLE_PRICE_ID")
    ENV["STORE_IDS"] || raise("need STORE_IDS")

    puts Rails.logger.info("Running change_variable_pricing for PERSON_ID=#{ENV["PERSON_ID"]} ALBUM_UPCS=#{ENV["ALBUM_UPCS"]} TRACK_VARIABLE_PRICE_ID=#{ENV["TRACK_VARIABLE_PRICE_ID"]} STORE_IDS=#{ENV["STORE_IDS"]}")

    album_upcs = ENV["ALBUM_UPCS"].split(",")
    store_ids  = ENV["STORE_IDS"].split(",")

    person = Person.find(ENV["PERSON_ID"])
    albums = person.albums.joins(:upcs).where("upcs.number in (?)", album_upcs)

    to_redeliver = []

    begin
      Salepoint.transaction do

        albums.each do |album|
          puts Rails.logger.info("Updating pricing for album: #{album.id}")

          salepoints = album.salepoints.where("salepoints.store_id in (?)", store_ids)

          salepoints.each do |salepoint|

            if ENV["TRACK_VARIABLE_PRICE_ID"]
              puts Rails.logger.info("Updating salepoint for #{salepoint.store.name} to track variable price #{ENV["TRACK_VARIABLE_PRICE_ID"]}")
              salepoint.track_variable_price_id = ENV["TRACK_VARIABLE_PRICE_ID"]

              puts Rails.logger.info("Saving salepoint for #{salepoint.store.name} (#{salepoint.id})")
              salepoint.save!
            elsif ENV["ALBUM_VARIABLE_PRICE_ID"]
              puts Rails.logger.info("Updating salepoint for #{salepoint.store.name} to variable price #{ENV["ALBUM_VARIABLE_PRICE_ID"]}")
              salepoint.variable_price_id = ENV["ALBUM_VARIABLE_PRICE_ID"]

              puts Rails.logger.info("Saving salepoint for #{salepoint.store.name} (#{salepoint.id})")
              salepoint.save!
            end

            to_redeliver << salepoint

          end

        end
      end

      #Send metadata update
      to_redeliver.each { |salepoint|
        puts Rails.logger.info("Sending meta data update for salepoint #{salepoint.id}")
        salepoint.send_metadata_update
      }


    rescue StandardError => e
      puts Rails.logger.info("Exception: #{e}")
    end

    puts Rails.logger.info("Finished change_variable_pricing")
  end

  desc "Add Stores to a persons Albums"
  task :add_stores_to_releases => :environment do
    ENV['PERSON_ID'] || ENV['PERSON_IDS'] || raise("Need PERSON_ID or PERSON_IDS")
    ENV['STORE_ID']                       || raise("Need Store ID")

    person_ids = []
    if ENV['PERSON_IDS']
      person_ids = ENV['PERSON_IDS'].split(",")
    else
      person_ids << ENV['PERSON_ID']
    end

    person_ids.each do |person_id|

      person  = Person.find(person_id)
      product = nil

      #Find add store product
      #9  = Add store US
      #85 = Add store Canada
      #This will throw an exception if these products are inactived
      if person.country_website_id    == CountryWebsite::UNITED_STATES
        product = Product.active.find(9)
      elsif person.country_website_id == CountryWebsite::CANADA
        product = Product.active.find(85)
      end

      store  = Store.find(ENV['STORE_ID'])
      variable_price = nil
      albums = []

      if store.has_variable_pricing?
        raise("NEED VARIABLE PRICE ID") if ENV['VARIABLE_PRICE_ID'].blank?
        variable_price = VariablePrice.find(ENV['VARIABLE_PRICE_ID'])
        raise("VARIABLE PRICE ID INVALID") if !variable_price || !store.variable_prices.include?(variable_price)
      end

      if ENV['ALBUM_ID_ARRAY']
        ENV['ALBUM_ID_ARRAY'].split(',').each do |id|
          albums << Album.find(id)
        end
      else
        #Get all finalized paid for albums
        albums = person.albums.select("distinct albums.*").joins("inner join inventory_usages on related_type='Album' and related_id=albums.id").where("takedown_at is NULL")
      end

      albums.each do |album|

        #Don't add stores twice
        if album.salepoints.any?{ |salepoint| salepoint.store_id == store.id }
          puts "Album #{album.id} already has store #{store.id}"
        else
          salepoint_attrs = {:store=>store}
          salepoint_attrs[:has_rights_assignment] = 1                        if store.needs_rights_assignment?
          salepoint_attrs[:variable_price_id]     = ENV['VARIABLE_PRICE_ID'] if store.has_variable_pricing?

          salepoint = album.salepoints.create(salepoint_attrs)
          Product.add_to_cart(person, salepoint, product)

          puts("Added store #{store.id} to album #{album.id}")
        end
      end

    end

  end

  desc 'Deletes and recreates the development logs to speed up dev environment'
  task :reset_logs => :environment do
    DEV_LOGS.each do |log|
      `rm #{Rails.root}/log/#{log}.log`
      `touch #{Rails.root}/log/#{log}.log`
      puts "#{log}.log has been reset"
    end
  end

  desc 'Does a mass lock on all of the accounts passed in'
  task :mass_lock => :environment do
    ENV['ADMIN'] || raise("Must provide an admin email")
    ENV['PERSON_ID_ARRAY'] || raise("Must provide PERSON_ID_ARRAY='1,2,3,4'")
    person_id_array = ENV['PERSON_ID_ARRAY'].split(',')
    puts person_id_array
    admin = Person.find_by(email: ENV['ADMIN'])
    person_id_array.each do |person_id|
      puts person_id
      person = Person.find(person_id)
      puts person
      puts person.status
      if person.status != "Locked"
              note = Note.create(:note=>"Locked by mass lock maintenance task", :subject=>"Locked", :related => person, :note_created_by_id => admin.id)
      person.lock!("Store End", true) if note.valid?
      end
    end
  end

  desc 'Converts album to single'
  task :album_to_single => :environment do
    ENV['ADMIN'] || raise("Must provide an admin email")
    ENV['ALBUM_ID_LIST'] || raise("Need ALBUM_ID_LIST='1,2,3,4'")
    album_id_array = ENV['ALBUM_ID_LIST'].split(',')
    admin = Person.find_by(email: ENV['ADMIN'])
    album_id_array.each do |album_id|
      begin
        a = Album.find(album_id)
        p = Person.find(a.person_id)
        r = Renewal.renewal_for(a)
        if a.songs.count == 1 && r.item_to_renew_id != 30 && p.country_website_id == 1
          a.update_attribute :album_type, 'Single'
          Renewal.update(r.id, :item_to_renew_id => 30)
          Note.create(:note=>"Converted album #{a.id} to single", :subject=>"Maintenance", :related => p, :note_created_by_id => admin.id)
	        puts "Converted album #{album_id} to a single"
        else
          puts "Album #{album_id} is not a US 1-song album"
        end
      rescue
        puts "Problem converting album #{album_id}"
      end
    end
  end

  desc 'Adjust renewal dates for a list of album ids'
  task :adjust_renewal_dates => :environment do
    ENV['ADMIN'] || raise("Must provide an admin email")
    ENV['ALBUM_ID_LIST'] || raise("Need ALBUM_ID_LIST='1,2,3,4'")
    ENV['TARGET_DATE'] || raise("Need TARGET_DATE='YYYY-MM-DD'")
    album_id_array = ENV['ALBUM_ID_LIST'].split(',')
    admin = Person.find_by(email: ENV['ADMIN'])
    target_date = ENV['TARGET_DATE']
    album_id_array.each do |album_id|
      begin
        a = Album.find(album_id)
        old_expires = a.renewal.expires_at
        Renewal.adjust_expiration_date_for(a, target_date)
        puts "#{a.id} renewal date adjusted from #{old_expires.strftime('%Y-%m-%d')} to #{a.renewal.expires_at.strftime('%Y-%m-%d')}"
        Note.create(:related => a, :subject => "Renewal Updated", :note => "Release #{a.id} renewal date adjusted from #{old_expires.strftime('%Y-%m-%d')} to #{a.renewal.expires_at.strftime('%Y-%m-%d')}", :note_created_by_id => 543681)
      rescue => e
        puts "#{a.id} error: #{e.message}"
      end
    end
  end

  desc 'Take down a list of albums without notifying customers'
  task :album_list_takedown => :environment do
    ENV['ADMIN'] || raise("Must provide an admin email")
    ENV['ALBUM_ID_LIST'] || raise("Need ALBUM_ID_LIST='1,2,3,4'")
    album_id_array = ENV['ALBUM_ID_LIST'].split(',')
    admin = Person.find_by(email: ENV['ADMIN'])
    album_id_array.each do |album_id|
      begin
        a = Album.find(album_id)
        if !a.takedown?
          if a.takedown!
            puts "#{a.id} taken down"
            Note.create(:related => a, :subject => "Automated Takedown", :note => "Release #{a.id} was taken down from console", :note_created_by_id => 543681)
          else
            puts "#{a.id} could not be taken down because an exception was thrown in takedown!"
          end
        else
          puts "#{a.id} already taken down"
        end
      rescue => e
        puts "#{a.id} error: #{e.message}"
      end
    end
  end

  desc 'Resets login attempts older than 1 week'
  task :reset_login_attempts => :environment do
    LoginAttempt.connection.execute("DELETE FROM login_attempts where updated_at < NOW() - INTERVAL 1 WEEK")
  end

  desc "Error distributions from a csv of distribution IDs"
  task :error_distributions_from_csv => :environment do
    filename = ENV["DISTRIBUTION_CSV"] || raise("Missing variable DISTRIBUTION_CSV")
    message = ENV["MESSAGE"] || raise("Need MESSAGE")
    Rails.logger.info "Erroring distributions in file #{filename}"
    puts "Erroring distributions in file #{filename}"

    CSV.foreach(filename) do |line|
      begin
        distribution_id = line[0].to_i
        d = Distribution.find(distribution_id)
        if d.present?
          d.errored(:actor => "Rake maintenance:error_distributions_from_csv", :message => "#{message}")
        end
      rescue StandardError => e
        Rails.logger.info e
        puts e
      end
    end
  end

  desc "Destroy upload records from a csv of upload IDs"
  task :destroy_uploads_from_csv => :environment do
    filename = ENV["UPLOAD_CSV"] || raise("Missing variable UPLOAD_CSV")
    Rails.logger.info "Destroying uploads in file #{filename}"
    puts "Destroying uploads in file #{filename}"

    CSV.foreach(filename) do |line|
      begin
        upload_id = line[0].to_i
        u = nil
        u = Upload.find(upload_id)
        if u.present?
          s = Upload.where("song_id = ?", u.song_id)
          if s.size == 1
            Rails.logger.info "No duplicates for song #{s.first.song_id} found for upload id #{upload_id}"
            puts "No duplicates for song #{s.first.song_id} found for upload id #{upload_id}"
          else
            u.destroy
            Rails.logger.info "Upload id #{upload_id} destroyed"
            puts "Upload id #{upload_id} destroyed"
          end
        end
      rescue StandardError => e
        Rails.logger.info e
        puts e
      end
    end
  end

  desc "Replace uploads.uploaded_filename with s3_asset.key filename from a csv of song IDs"
  task :reconcile_upload_to_s3_from_csv => :environment do
    filename = ENV["SONG_CSV"] || raise("Missing variable SONG_CSV")
    Rails.logger.info "Renaming uploaded_filename in uploads in file #{filename}"
    puts "Renaming uploaded_filename in uploads in file #{filename}"

    CSV.foreach(filename) do |line|
      begin
        song_id = line[0].to_i
        s = nil
        s = Song.find(song_id)
        if s.present?
          u = s.upload
          s3 = s.s3_asset
          if u.present? && s3.present?
            u.uploaded_filename = s3.key.split('/').last
            u.save
            Rails.logger.info "Renamed uploads.uploaded_filename to #{u.uploaded_filename}"
            puts "Renamed uploads.uploaded_filename to #{u.uploaded_filename}" end
        end
      rescue StandardError => e
        Rails.logger.info e
        puts e
      end
    end
  end

end
