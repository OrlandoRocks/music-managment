desc "Fetch Southeast Asian Currency Exchange Rates"
task fetch_southeast_asian_currency_exchange_rates: :environment do
  worker_id = CurrencyLayer::SoutheastAsianForeignExchangeRateFetchWorker.perform_async
  Rails.logger.info("Spawned worker with id=#{worker_id} to fetch exchange rates")
end
