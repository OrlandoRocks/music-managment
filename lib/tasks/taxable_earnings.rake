namespace :taxable_earnings do
  # sample usage: rake taxable_earnings:backfill[taxbackfill_2019.csv]
  task :backfill, [:filename] => :environment do |task, args|
    puts "Starting TaxableEarnings YTD import"

    filepath = "/home/deploy/tmp/#{args[:filename]}"
    unless File.file?(filepath)
      raise "File not found #{filepath}"
    end

    rows = CSV.read(filepath)
    headers = ["revenue_source", "person_id", "created_at", "currency", "credit"]
    puts("Records to be imported - #{rows.length}")

    TaxableEarning.import(headers, rows, batch_size: 10000)
    puts "Finished importing"
  end
end
