# frozen_string_literal: true
#
# e.g. bundle exec rake "renewal_ad_hoc:set_visible_to_customer[set_visible_to_customer.csv]"

namespace :renewal_ad_hoc do
  desc "Set invoices to visible_to_customer from s3 csv"
  task :set_visible_to_customer, [:s3_file_name] => :environment do |_, args|
    data_file_key = args[:s3_file_name]
    invoices = RakeDatafileService.csv(data_file_key)
    invoices.each do |invoice|
      i = Invoice.find(invoice["invoice_id"])
      i.update(batch_status: "visible_to_customer")
    end
  end
end
