namespace :recalculate_lte do
  task update_hold_amount_for_claims: :environment do
    CopyrightClaim.active.where.not(hold_amount: nil).each do |claim|
      begin
        claim.calculate_hold_amount!
        Rails.logger.info "Updated hold amount for claim #{claim.id}"
      rescue
        Rails.logger.info "Cannot update hold amount #{claim.id} - #{claim.errors}"
      end
    end
  end
end
