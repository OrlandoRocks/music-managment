desc 'Rename the vat feature flag to project standard'
task rename_vat_feature_flag: :environment do
  FeatureFlipper.remove_feature('vat-tax')
  puts 'Removed vat-tax feature flipper'

  FeatureFlipper.add_feature('vat_tax')
  puts 'Added vat_tax feature flipper'
end
