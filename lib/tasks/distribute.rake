require 'aws-sdk'
require "S3"
require 'active_support'


namespace :distribute do
  desc "Catching up old distributions"
  task :catch_up => :environment do

    converter_class = "MusicStores::#{ENV["CONVERTER"]}::Converter"
    catch_up_date   = ENV["DATE"] || Date.today - 3.days
    limit           = ENV["LIMIT"] || 2000
    queue           = ENV["QUEUE"] || "petri-cleanup-queue-vpc"
    exclude_list    = ['delivered', 'error', 'new', 'pending_approval', 'blocked']

    distros = Distribution.joins(:petri_bundle).where("petri_bundles.state <> 'dismissed' and distributions.converter_class = ? and distributions.state not in (?) and date(distributions.updated_at) < ?", converter_class, exclude_list, catch_up_date).limit(5000).order("distributions.id desc").readonly(false)
    distros.each do |d|
      Delivery::DistributionWorker.perform_async(d.class.name, d.id, d.last_delivery_type || "full_delivery", queue)
    end
  end

  desc "Distribute to a store based on a csv file of upcs"
  task :by_converter_class_and_upcs => :environment do
    raise "Set a CONVERTER_CLASS env var" unless ENV["CONVERTER_CLASS"]
    raise "Set a FILE env var" unless ENV['FILE']
    converter_class = ENV["CONVERTER_CLASS"]
    path_to_file    = ENV['FILE']
    queue           = ENV["QUEUE"] || "petri-cleanup-queue-no-vpc"
    throttle_limit  = ENV['THROTTLE_LIMIT'] || 2000
    completed       = $redis.get("by_converter_class_and_upc-#{converter_class}").blank? ? 0 : $redis.get("by_converter_class_and_upc-#{converter_class}").to_i
    todays_upcs     = File.readlines("#{path_to_file}")[completed.to_i, throttle_limit.to_i].map(&:chomp)
    distros         = DistributionRetryQueryBuilder.new
                        .not_in_dismissed_petri_bundle
                        .by_converter_class(converter_class)
                        .by_upcs_list(todays_upcs)
                        .query
    distros.each do |distro|
      Delivery::DistributionWorker.perform_async(distro.class.name, distro.id, distro.last_delivery_type, queue)
      #this sets the upc for the starting point of next run
      $redis.set("by_converter_class_and_upc-#{converter_class}", completed += 1)
    end
  end

  task get_archived_metadata: :environment do
    album_id = ENV["ALBUM_ID"]
    upc = ENV["UPC"]
    p "You must provide either an ALBUM_ID or a UPC via an environment variable. If you provide both, only the ALBUM_ID will be used." and return if (album_id.nil? && upc.nil?)

    album = album_id ? Album.find(album_id) : Album.find(upc)
    p "Album not found." and return unless album

    p "Fetching archived metadata from metadata.tunecore.com"

    stores = S3_CLIENT.buckets["metadata.tunecore.com"].objects.with_prefix(album.upc).map(&:key).map { |k| k.split("/")[1] }.uniq

    stores.each do |store|
      latest = S3_CLIENT.buckets["metadata.tunecore.com"].objects.with_prefix("#{album.upc}/#{store}").sort_by(&:last_modified).last
        File.open("tmp/#{album.id}_#{latest.key.split("/")[1]}.xml", "wb") do |file|
          latest.read do |chunk|
            file.write(chunk)
          end
        end
    end
  end
end
