namespace :transient do 
  task update_store_delivery_configs_ddex: :environment do
    store_ids = [7, 32, 39, 47, 51, 52, 53, 55, 56, 58, 
                60, 67, 68, 69, 70, 71, 74, 76, 77, 78, 79]

    StoreDeliveryConfig
      .where(store_id: store_ids, ddex_version: nil)
      .update_all(ddex_version: "341")
  end 
end 