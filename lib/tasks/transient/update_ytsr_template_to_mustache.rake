namespace :transient do
  task update_ytsr_template_to_mustache: :environment do
    replace_erb_with_mustache = -> (text) {
      text
        .gsub("<%= @", "{{ ")
        .gsub("%>", "}}")
    }

    YoutubeSrMailTemplate.all.each do |template|
      template.update(
        header: replace_erb_with_mustache.call(template.header),
        subject: replace_erb_with_mustache.call(template.subject),
        body: new_body(template.template_name)
      )

      puts "Updated template:#{template.template_name} - #{template.id}"
    end
  end

  def new_body(template_name)
    new_values = {
      "removed_monetization" => removed_monetization_text,
      "instrumental_rights" => instrumental_rights_text,
      "disputing_a_claim" => disputing_a_claim_text,
      "ownership_conflict" => ownership_conflict_text,
      "authorization" => authorization_text,
      "disputed_claim" => disputed_claim_text,
      "creative_commons_content" => creative_commons_content_text,
      "art_track" => art_track_text,
    }

    new_values[template_name]
  end

  def removed_monetization_text
    <<~TEMPLATE
      {{#data}}
        <p>We have been contacted by <strong>{{ custom_fields.first.third_party }}</strong> for: </p>
        <ul>
          <li>ISRC: {{ song.isrc }}</li>
          <li>Title: {{ song.name }}</li>
          <li>Artist: {{ song.artist_name }}</li>
        </ul>
      {{/data}}
    TEMPLATE
  end

  def instrumental_rights_text
    <<~TEMPLATE
      {{#data}}
        <p>We have been contacted by <strong>{{ custom_fields.first.third_party }}</strong> for your sound recording <strong>{{ song.name }}</strong> (ISRC <strong>{{ song.isrc }}</strong>).
      {{/data}}
    TEMPLATE
  end

  def disputing_a_claim_text
    <<~TEMPLATE
      {{#data}}
        <a href="{{ custom_fields.first.link_to_video }}">{{ custom_fields.first.link_to_video }}</a>
      {{/data}}
    TEMPLATE
  end

  def ownership_conflict_text
    <<~TEMPLATE
      {{#data}}
        <p>We have been contacted by <strong>{{ custom_fields.first.third_party }}</strong> for: </p>
        <ul>
          <li>ISRC: {{ song.isrc }}</li>
          <li>Title: {{ song.name }}</li>
          <li>Artist: {{ song.artist_name }}</li>
        </ul>
        <p><strong>{{ custom_fields.first.third_party }}</strong> is asserting rights in the following territories: <strong>{{ custom_fields.first.territories }}</strong></p>
      {{/data}}
    TEMPLATE
  end

  def authorization_text
    <<~TEMPLATE
      {{#data}}
        <p>Song Title: <strong>{{ song.name }}</strong> with ISRC: <strong>{{ song.isrc }}</strong></p>

        {{#custom_fields}}
          <ul>
            <li>Video Uploader: <strong>{{ video_uploader }}</strong></li>
            <li>Video Link: <a href="{{ link_to_video }}">{{ link_to_video }}</a></li>
          </ul>
        {{/custom_fields}}
      {{/data}}
    TEMPLATE
  end

  def disputed_claim_text
    <<~TEMPLATE
      {{#data}}
        <p>For sound recording <strong>{{ song.name }}</strong> by me with the ISRC <strong>{{ song.isrc }}</strong>, <strong>{{ custom_fields.first.plaintiff }}</strong>
          appealed TuneCore's claim of your recording that is used in the following video: <a href="{{ custom_fields.first.link_to_video }}">{{ custom_fields.first.link_to_video }}</a>.</p>
          <p>The reason invoked is: <strong>{{ custom_fields.first.plaintiff_message }}</strong>.
      {{/data}}
    TEMPLATE
  end

  def creative_commons_content_text
    <<~TEMPLATE
      {{#data}}
        <p>We have been contacted by <strong>{{ custom_fields.first.third_party }}</strong> for the sound recording: </p>
        <ul>
          <li>ISRC: {{ song.isrc }}</li>
          <li>Title: {{ song.name }}</li>
          <li>Artist: {{ song.artist_name }}</li>
        </ul>
      {{/data}}
    TEMPLATE
  end

  def art_track_text
    <<~TEMPLATE
      {{#data}}
        <ul>
          <li>Claimant: <strong>{{ custom_fields.first.third_party_email }}</strong></li>
          <li>Recordings: <strong>{{ custom_fields.first.recording_name }}</strong></li>
          <li>Territories: <strong>Worldwide</strong></li>
        </ul>
      {{/data}}
    TEMPLATE
  end

end
