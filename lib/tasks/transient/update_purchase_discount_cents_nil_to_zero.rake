namespace :transient do
  task update_purchase_discount_cents_nil_to_zero: :environment do
    nil_discount_cents_purchases = Purchase.where(discount_cents: nil)

    Rails.logger.info "Updating #{nil_discount_cents_purchases.length} purchases"

    nil_discount_cents_purchases.update_all(discount_cents: 0)

    Rails.logger.info "#{nil_discount_cents_purchases.length} purchases updated"
  end
end
