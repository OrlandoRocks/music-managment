namespace :transient do
  task activate_music_island: :environment do

    description = <<-HTML
Music Island delivers your music to six popular digital stores and streaming platforms throughout South Korea, including Melon, the country's largest subscription service boasting over 4 million subscribers.<p>Your music will be delivered to the following Korean music platforms:</p><ul style="padding-left: 4em;"><li>Melon (Kakao-M)</li><li>Genie Music (KT)</li><li>Bugs (NHN Bugs)</li><li>VIBE (Naver Music)</li><li>Soribada</li><li>FLO (Dreamus Co.)</li></ul><strong>Strength:</strong> Reach. Your tracks reach up to six digital music services in a rapidly growing music market.
    HTML

    Store.find_by(short_name: "M_Island").update(
      is_active: true,
      in_use_flag: true,
      position: 479,
      description: description
    )

    KnowledgebaseLink.create(article_id: 360028254611, article_slug: "music-island-description")

    Store.find_by(short_name: "Akazoo").update(
      position: 485
    )
  end
end