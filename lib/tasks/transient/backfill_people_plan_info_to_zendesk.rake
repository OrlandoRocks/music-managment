namespace :transient do
  desc "backfill users plan data to zendesk"
  task people_plan_info_to_zendesk: :environment do
    Rails.logger.info "Initiating sidekiq queue for zendesk person plan backfill"
    person_ids = PersonPlan.pluck(:person_id)
    person_ids.each_slice(700) do |sliced_person_ids|
      sliced_person_ids.each do |sliced_person_id|
        Rails.logger.info "Initiated sidekiq queue for zendesk person plan backfill for #{sliced_person_id}"
        Zendesk::Person.create_apilogger(sliced_person_id)
      end
      sleep(2.minutes)
    end
    Rails.logger.info "Initialized sidekiq queue for zendesk person plan backfill"
  end

  desc "backfill random users plan data to zendesk"
  task people_plan_info_random_to_zendesk: :environment do
    Rails.logger.info "Initiating sidekiq queue for zendesk random person plan backfill"
    person_ids = PersonPlan.pluck(:person_id).first(5)
    person_ids.each do |person|
      Rails.logger.info "Initiated sidekiq queue for zendesk random person plan backfill for #{person}"
      Zendesk::Person.create_apilogger(person)
    end
    Rails.logger.info "Initialized sidekiq queue for zendesk random person plan backfill"
  end
end
