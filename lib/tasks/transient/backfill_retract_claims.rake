namespace :transient do
  task backfill_retracted_copyright_claim: :environment do
    file_name = "Holds_Retracted_Backfill.xlsx"
    path_name = Rails.root.join("tmp", ENV["RAILS_ENV"], file_name)
    file = RakeDatafileService.download(
      file_name,
      temporary: true,
      environment_folder: ENV["RAILS_ENV"],
      binary_mode: true
    ).local_file
    workbook = Roo::Spreadsheet.open(file)
    claims = []
    workbook.each_with_pagename do |name, sheet|
      if ["YTD 21", "YTD 22"].exclude?(name)
        header_row = sheet.row(1)
        track_index = header_row.index(header_row.grep(/track number/i)&.first)
        store_index = header_row.index(header_row.grep(/Store Name\/Claimant/i)&.first)
        claimant_index = header_row.index(header_row.grep(/claimant email/i)&.first)
        claimant_retraction1 = header_row.index(header_row.grep(/Claim Resolution/i)&.first)
        claimant_retraction2 = header_row.index(header_row.grep(/Claim Retracted\/Resolved?/i)&.first)
        (2..sheet.last_row).each do |row|
          record = sheet.row(row)
          if claimant_retraction1 || claimant_retraction2
            claimant_retraction_status1 = record[claimant_retraction1] if claimant_retraction1
            claimant_retraction_status2 = record[claimant_retraction2] if claimant_retraction2
            claimant_retraction_status = claimant_retraction_status1.present? ? claimant_retraction_status1 : claimant_retraction_status2
            if claimant_retraction_status.present?
            
              next unless ['retracted', 'retraction', 'yes'].each { |val| claimant_retraction_status.downcase.include?(val) }

              internal_tracking_number = record[4].to_s.strip
              upcs = record[5].to_s.scan(/\d+/)
              track_numbers = track_index ? record[track_index].to_s.split(';') : []
              store_name = record[store_index]
              claimants = record[claimant_index]
              albums = Hash[upcs&.zip(track_numbers)] if upcs

              next unless internal_tracking_number
              next if upcs.blank?

              albums.each do |key, value|
                album = Album.find_by_upc(key)
                puts "Album found with upc #{key} with id #{album&.id} in sheet #{name}"
                next  unless album

                store =  Store.where(name: store_name).first unless store_name&.downcase == 'claimant'
                next if !store

                puts "getting claims"
                if (!value || value == "all")
                  claim = CopyrightClaim.where(asset_type: "Album",asset_id: album.id, store_id: store ? store.id : nil, internal_claim_id: internal_tracking_number, person_id: album.person_id).last
                  claims << claim if claim
                  puts claim
                else
                  value.scan(/\d+/).each do |track_id|
                    puts "song claims"
                    song = album.songs.where(track_num: track_id).first
                    next unless song
                    claim = CopyrightClaim.where(asset_type: "Song",asset_id: song.id, store_id: store ? store.id : nil, internal_claim_id: internal_tracking_number, person_id: album.person_id).last
                    claims << claim if claim
                    puts claim
                  end
                end
              end
            end
          end
        end
      end
    end
    puts claims.map(&:id)
    puts claims.count
    puts "-------------"
    admin = Person.where(email: "james.nance@tunecore.com").first
    claims.each do |claim|
      claim.deactivate!
      claim.notes.create(subject: "Copyright Claim Update", note:"Claim retracted", note_created_by_id: admin.id)
    end
  end
end

