namespace :transient do
  task change_album_renewal_product_price_india: :environment do
    NEW_ALBUM_RENEWAL_PRICE = 39.6255
    india_album_renewal_product = Product.find_by(id: 454)
    if india_album_renewal_product.present?
      india_album_renewal_product.update(price: NEW_ALBUM_RENEWAL_PRICE)
      if india_album_renewal_product.errors.blank?
        Rails.logger.info "Success! India Album Renewal Price Updated to #{NEW_ALBUM_RENEWAL_PRICE}."
      else
        Rails.logger.error "Failed! Unable to update India Album Renewal Price."
      end
    else
      Rails.logger.error "Failed! Unable to Find product with ID 454."
    end
  end
end
