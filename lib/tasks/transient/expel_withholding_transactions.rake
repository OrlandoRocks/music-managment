# These 4 excluded users will be handled in CF-883 since they have encumbrances around the withholding transactions
PERSON_IDS_TO_EXCLUDE = [273_830, 1_953_646, 1_949_393, 1_818_377]

namespace :transient do
  task expel_withholding_transactions: :environment do
    Rails.logger.info("Withholding transactions expulsion initiated")

    person_ids =
      PersonTransaction
      .where(target_type: "TransactionErrorAdjustment")
      .where.not(person_id: PERSON_IDS_TO_EXCLUDE)
      .pluck(:person_id)

    person_ids.each do |person_id|
      all_transactions = Person.find(person_id).person_transactions
      withholding_transaction_ids =
        all_transactions
        .where(target_type: "TransactionErrorAdjustment")
        .or(
          all_transactions.where(
            target_type: "BalanceAdjustment",
            comment: "IRS Tax Withholding Reversal"
          )
        )
        .ids
      TransactionExpulsionService.new(withholding_transaction_ids).expel!
    end

    Rails.logger.info("Withholding transactions expulsion concluded")
  end
end
