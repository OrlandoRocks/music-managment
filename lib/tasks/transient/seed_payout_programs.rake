namespace :transient do
  task seed_payout_programs: :environment do
    file = Rails.root.join("db/seed/csv/payout_programs.csv")
    Seed.seed(file)
  end
end
