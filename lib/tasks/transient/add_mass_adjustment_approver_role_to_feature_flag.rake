namespace :transient do
  task add_mass_adjustment_approver_role_to_feature_flag: :environment do
    admin_ids = Person
                .joins(:roles)
                .where(roles: { long_name: "Mass Balances Adjustment Approver" })
                .pluck(:id)
                .uniq

    FeatureFlipper.add_by_user_ids(:mass_balance_adjustment_email, admin_ids)

    Rails.logger.info("Succesfully added the following admin_ids to the feature flag: #{admin_ids}")
  end
end
