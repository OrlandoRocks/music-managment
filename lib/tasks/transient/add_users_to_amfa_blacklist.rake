namespace :transient do
  task add_users_to_amfa_blacklist: :environment do
    list_of_users = [14003, 10281, 15135, 17193, 35163]

    list_of_users.each do |user|
      FeatureBlacklist.create(person_id: user, feature: "apple_music_for_artists")
    end
  end
end
