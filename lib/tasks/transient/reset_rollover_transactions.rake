namespace :transient do
  desc "Resets All Rolled Over Earnings to Zero (Debit & Credit)"
  task reset_rollover_transactions: :environment do
    Rails.logger.info "Initialized Transaction Correction Service"
    rollover_transactions = PersonTransaction.where(target_type: "TaxableEarningsRollover")
    grouped_person_transactions =
      rollover_transactions
      .where(PersonTransaction.arel_table[:debit].gt(0))
      .or(rollover_transactions.where(PersonTransaction.arel_table[:credit].gt(0)))
      .group_by(&:person_id)
      .values

    grouped_person_transactions.map do |person_rollover_transactions|
      corrections =
        person_rollover_transactions.map do |rollover_transaction|
          { id: rollover_transaction.id, debit: 0, credit: 0 }
        end
      TransactionCorrectionService.new(corrections).exec!
    end
    Rails.logger.info "Concluded Transaction Correction Service"
  end

  desc "Reset Rollover & Withholding Transactions"
  task reset_rollover_and_withholding: :environment do
    rollover_transactions = PersonTransaction.where(target_type: "TaxableEarningsRollover")
    errored_reset_users =
      rollover_transactions
      .where(PersonTransaction.arel_table[:debit].gt(0))
      .or(rollover_transactions.where(PersonTransaction.arel_table[:credit].gt(0)))
      .distinct(:person_id)
      .pluck(:person_id)

    errored_reset_users.map do |person_id|
      user_transactions =
        PersonTransaction
        .where(person_id: person_id)
        .where(created_at: DateTime.now.beginning_of_year.ago(1.day)..DateTime.now)

      adjustable_user_transactions =
        user_transactions
        .where(target_type: ["TaxableEarningsRollover", "TransactionErrorAdjustment"])
        .or(user_transactions.where(target_type: "BalanceAdjustment", comment: "IRS Tax Withholding Reversal"))

      corrections =
        adjustable_user_transactions.map do |correctable_transaction|
          { id: correctable_transaction.id, debit: 0, credit: 0 }
        end

      TransactionCorrectionService.new(corrections).exec!
    end
  end

  desc "Reset Withholding Transactions"
  task reset_withholding_transactions: :environment do
    person_ids =
      PersonTransaction
      .where(target_type: "TransactionErrorAdjustment")
      .where(PersonTransaction.arel_table[:debit].gt(0))
      .distinct(:person_id)
      .pluck(:person_id)

    person_ids.map do |person_id|
      user_transactions =
        PersonTransaction
        .where(person_id: person_id)
        .where(created_at: DateTime.now.beginning_of_year.ago(1.day)..DateTime.now)

      adjustable_user_transactions =
        user_transactions
        .where(target_type: "TransactionErrorAdjustment")
        .or(user_transactions.where(target_type: "BalanceAdjustment", comment: "IRS Tax Withholding Reversal"))

      corrections =
        adjustable_user_transactions.map do |correctable_transaction|
          { id: correctable_transaction.id, debit: 0, credit: 0 }
        end
      TransactionCorrectionService.new(corrections).exec!
    end
  end
end
