namespace :transient do
  task :update_renewal_product_item_duration => :environment do
    product_items_to_fix = ProductItem.where(first_renewal_duration: 5)
    product_items_to_fix.each do |product_item|
      product_item.update!(renewal_duration: 5)
    end
  end
end
