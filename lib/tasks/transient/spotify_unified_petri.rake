namespace :transient do
  task spotify_unified_petri: :environment do
    store = Store.find(26)
    store.update(delivery_service: "tunecore")
    config = StoreDeliveryConfig.readonly(false).for("Spotify")
    config.update(
      bit_depth: 32,
      remote_dir_timestamped: false,
      remote_dir: "tunecore",
      party_full_name: "Spotify",
      party_id: "PADPIDA2011072101T",
      skip_batching: false,
      ddex_version: "382",
      image_size: 1600
    )
  end
end
