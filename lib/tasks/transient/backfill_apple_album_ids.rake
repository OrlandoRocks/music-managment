namespace :transient do
  task backfill_apple_ids: :environment do
    apple_map = Album.joins("left outer join external_service_ids esi on esi.linkable_id = albums.id and esi.linkable_type='Album' and esi.service_name = 'apple'").
      where("albums.apple_identifier is not null").
      where("esi.id is null").
      select("albums.id, albums.apple_identifier")

    if apple_map.present?
      insert = "INSERT INTO external_service_ids (linkable_id, identifier, service_name, linkable_type) VALUES "
      apple_map.each do |obj|
        insert << "(#{obj.id}, #{obj.apple_identifier}, 'apple', 'Album'), "
      end

      ActiveRecord::Base.connection.execute(insert.chomp(", "))
    else
      puts "Nothing to backfill"
    end
  end
end
