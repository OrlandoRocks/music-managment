namespace :transient do
  task add_person_id_to_payout_transfers: :environment do
    PayoutTransfer.joins(:payout_provider).each do |pt|
      pt.update(person_id: pt.payout_provider.person_id)
    end
  end
end
