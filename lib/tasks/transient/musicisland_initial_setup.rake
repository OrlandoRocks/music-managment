namespace :transient do
  task musicisland_initial_setup: :environment do
    misland = StoreCreator.create(
      abbrev: "island",
      delivery_service: "tunecore",
      description: "",
      in_use_flag: false,
      is_active: false,
      launched_at: Time.now,
      name: "Music Island",
      on_sale: false,
      position: 0,
      reviewable: false,
      short_name: "M_Island"
    )

    StoreDeliveryConfig.create(
      ddex_version: "341",
      party_id: "PADPIDA2017030806L",
      delivery_queue: "delivery-default",
      file_type: "mp3",
      username: "tunecore-ftp",
      hostname: "218.39.93.217",
      image_size: 1500,
      party_full_name: "Music Island",
      password: "UUmxfez8*2!@",
      remote_dir: "incoming",
      remote_dir_timestamped: false,
      skip_batching: false,
      store_id: misland.id,
      transfer_type: "sftp",
      use_resource_dir: true
    )
  end
end
