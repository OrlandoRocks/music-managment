namespace :transient do
  task backfill_blacklisted_artist_to_studio_watchlist: :environment do
    BlacklistedArtist.all.each.with_index(1) do |ba, i|
      artist = ba.artist
      artist_orig_name = artist.name.strip.downcase
      orig_watchlist_obj = StudioWatchlist.create(text_to_compare: artist_orig_name, is_active: ba.active, group_id: i)
      StudioWatchlist.create(text_to_compare: artist.scrubbed_name, is_active: ba.active, group_id: i, parent_id: orig_watchlist_obj.id) if artist.scrubbed_name && artist_orig_name != artist.scrubbed_name
    end
  end
end
