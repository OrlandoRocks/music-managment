namespace :transient do
  task add_format_release_for_stores_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "115006503247", article_slug: "format_release_for_stores", route_segment: nil).first_or_create
  end
end
