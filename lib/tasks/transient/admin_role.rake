namespace :transient do
  desc "fills admin role with admin role flag"
  task admin_role: :environment do
    Role.find_by(name: "Admin").update_attribute(:admin_role, true)
  end
end
