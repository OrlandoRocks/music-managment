namespace :transient do
    task add_scheduled_release_date_feature_to_rising_artist_plan: :environment do
      scheduled_release_plan_feature = PlanFeature.find(10)
      rising_artist_plan = Plan.find(2)
      rising_artist_plan.plan_features << scheduled_release_plan_feature if rising_artist_plan.present? && scheduled_release_plan_feature.present?
    end
  end