namespace :transient do
  task add_kosovo_as_country: :environment do
    Country.find_or_create_by(
      name: "Kosovo",
      iso_code: "XK",
      region: "Europe",
      sub_region: "Eastern Europe",
      tc_region: "EMEA",
      iso_code_3: "XKX",
      is_sanctioned: 0,
      corporate_entity_id: 2
    )
  end
end
