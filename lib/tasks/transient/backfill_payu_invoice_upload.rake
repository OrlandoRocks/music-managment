namespace :transient do
  task backfill_payu_invoice_upload: :environment do
    Rails.logger.info "Starting task"
    PayuInvoiceUploadResponse.where(related_id: nil).find_each do |resp|
      resp.update!(related: resp.payments_os_transaction)
      Rails.logger.info "completed backfill for response_id: #{resp.id}"
    end
    Rails.logger.info "Hooray! Complete mf!"
  end
end
