namespace :transient do
  task add_suspicious_flag_and_update_flag_reasons: :environment do
    suspicious_flag = Flag.find_or_create_by(Flag::SUSPICIOUS)

    blocked_from_distribution_flag = Flag.find_by(Flag::BLOCKED_FROM_DISTRIBUTION)

    FlagReason
      .where(flag: blocked_from_distribution_flag)
      .update_all(flag_id: suspicious_flag.id)
  end
end
