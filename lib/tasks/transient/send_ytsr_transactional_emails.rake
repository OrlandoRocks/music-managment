namespace :transient do
  task send_ytsr_transactional_emails: :environment do
    CSV.foreach("/tmp/ytsr_email_users.csv", headers: true).each do |row|
      break if row.blank?

      person = Person.find_by(id: row["person_id"])

      if person.present?
        MailerWorker.perform_async("YtsrMailer", :ytsr_transactional_email, person.id)
      end
    end
  end
end
