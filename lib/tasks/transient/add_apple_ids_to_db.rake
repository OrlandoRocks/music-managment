namespace :transient do
  task add_apple_ids_to_db: :environment do
    start_index = ENV["START_INDEX"].to_i == 0 ? nil : ENV["START_INDEX"].to_i
    end_index   = ENV["END_INDEX"].to_i   == 0 ? nil : ENV["END_INDEX"].to_i

    Apple::ArtistIdCsvDbLoader.upload("/var/tmp/epf_Tunecore_catalog.csv", start_index, end_index)
  end
end
