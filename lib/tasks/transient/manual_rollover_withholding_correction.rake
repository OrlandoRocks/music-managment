namespace :transient do
  desc "Manually resets rollover and withholding transactions by correcting errored previous balances"
  task manual_rollover_withholding_correction: :environment do
    Rails.logger.info "Initialized Manual Transaction Correction Service"
    resolvable_account_ids = [
      1_027_033,
      1_561_442,
      1_978_153,
      3_706_361,
      3_173_533,
      2_795_066,
      2_352_334,
      2_961_239,
      2_660_444,
      2_390_563,
      3_363_877,
      2_839_833,
      2_668_195,
      3_366_617,
      2_975_077,
      2_950_940,
      2_914_123,
      2_478_623,
      3_205_347
    ]

    resolvable_account_ids.each do |account_id|
      TaxWithholdings::Transient::ManualRolloverWithholdingResetService.new(account_id).reset!
    end
    Rails.logger.info "Concluded Manual Transaction Correction Service"
  end
end
