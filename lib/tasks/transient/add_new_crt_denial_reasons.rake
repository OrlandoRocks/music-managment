namespace :transient do
  desc "add new crt denial reasons"
  task add_new_crt_denial_reasons: :environment do
    roles = Role.where(name: ['Content Review Specialist', 'Content Review Manager'])
    new_crt_denial_reasons = [
      { reason: "Metadata: Confirm Compound Artist", email_template_path: "confirm_compound_artist" },
      { reason: "Metadata: Explicit Tracks (no parental logo on artwork)", email_template_path: "explicit_no_parental_logo" },
      { reason: "Metadata: Add/Fix Remixer Formatting", email_template_path: "add_remixer_fix_remix_formatting" },
      { reason: "Metadata: Confirm Audio Language", email_template_path: "confirm_audio_language" }
    ]

    new_crt_denial_reasons.each do |denial_reason|
      review_reason = ReviewReason.find_or_create_by(
        reason_type: "REJECTED",
        reason: denial_reason[:reason],
        review_type: "LEGAL",
        email_template_path: denial_reason[:email_template_path],
        should_unfinalize: 1,
        primary: 0,
        template_type: "changes_requested"
      )

      review_reason.roles << roles
    end
  end
end
