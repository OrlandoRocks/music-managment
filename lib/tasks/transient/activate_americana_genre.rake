namespace :transient do
  task activate_americana_genre: :environment do
    genre = Genre.where(name: "Americana").first
    genre.update(is_active: true)
  end
end