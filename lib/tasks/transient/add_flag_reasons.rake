namespace :transient do
  task add_flag_reasons: :environment do

    flag = Flag.find_by(**Flag::BLOCKED_FROM_DISTRIBUTION)
    flag_reasons = [ "GDPR ACCOUNT DELETE", "No Rights", "Store End", "PayPal Fraud", "Video Fraud", "Credit Card Fraud", "Hacked/Ownership Dispute", "Fraudulent Sales", "Other", "Suspicious Country IP" ]

    flag_reasons.each do |flag_reason|
      FlagReason.create(reason: flag_reason, flag_id: flag.id)
    end
  end
end

