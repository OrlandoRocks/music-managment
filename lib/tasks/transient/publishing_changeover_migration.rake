namespace :transient do
  task publishing_changeover_migration: :environment do
    Transient::PublishingChangeoverMigrationWorker.perform_async
  end
end
