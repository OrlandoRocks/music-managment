namespace :transient do
  task instagram_store_setup: :environment do
    store = Store.create!(
      name: "Instagram Audio Library",
      abbrev: "ig",
      short_name: "Instagram",
      is_active: false,
      in_use_flag: false,
      delivery_service: "tunecore",
      position: 780,
    )

    StoreDeliveryConfig.create!(
      hostname: "sftp.fb.com",
      username: "2998261238",
      password: "Tc3VD3T1R@!rRGz",
      remote_dir: "Tunecore",
      remote_dir_timestamped: false,
      transfer_type: "sftp",
      party_id: "PADPIDA2018010804X",
      party_full_name: "Facebook",
      ddex_version: "382",
      use_resource_dir: true,
      delivery_queue: "delivery-default",
      store_id: store.id,
      test_delivery: true 
    )
  end
end
