namespace :transient do
  task extract_code_and_description_from_transfer_response: :environment do
    PayoutTransfer.where.not(raw_response: nil).find_each do |transfer|
      response = JSON.parse(transfer.raw_response).with_indifferent_access
      transfer.update(description: response[:description], code: response[:code])
    end
  end
end
