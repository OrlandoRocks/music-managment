namespace :transient do
  desc "backfill Creatives person id"
  task backfill_creatives_person_id: :environment do
    Creative.where(creativeable_type: "Album").includes(:creativeable).find_each do |creative|
      creative.update_attribute(:person_id, creative.album.try(:person_id))  
    end

    Creative.where(creativeable_type: "Song").includes(creativeable: :album).find_each do |creative|
      creative.update_attribute(:person_id, creative.album.try(:person_id))
    end
  end
end
