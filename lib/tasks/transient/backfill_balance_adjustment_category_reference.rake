namespace :transient do
  desc "Backfill balance adjustments for new category 'Tidal DAP'"
  task backfill_balance_adjustment_category_references: :environment do
    category = BalanceAdjustmentCategory.find_by(name: "Tidal DAP")
    BalanceAdjustment.where("admin_note like ?", "%Tidal DAP%").update(category: category.name)
    MassAdjustmentBatch.where("admin_message like ?", "%Tidal DAP%").update(balance_adjustment_category_id: category.id)
  end
end
