namespace :transient do
  task seed_adyen_payment_method_infos: :environment do
    file = Rails.root.join("db/seed/csv/adyen_payment_method_infos.csv")
    Seed.seed(file)
  end
end
