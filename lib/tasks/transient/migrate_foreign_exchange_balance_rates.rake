namespace :transient do
  desc "Copy foreign_exchange_balance_rates to new foreign_exchange_rates table"
  task migrate_foreign_exchange_balance_rates: :environment do
    # Migrate records in foreign_exchange_balance_rates to foreign_exchange_rates table
    
    # Purge all the existing record in this new table (for idempotent run)
    ForeignExchangeRate.destroy_all
    
    balance_rates_query = "select * from foreign_exchange_balance_rates"
    foreign_exchange_balance_rates = ActiveRecord::Base.connection.select_all(balance_rates_query).to_hash
    foreign_exchange_balance_rates.each do |forex_rate|
      ForeignExchangeRate.create(
        exchange_rate: forex_rate['exchange_rate'],
        source_currency: CountryWebsite.find(forex_rate['country_website_id']).currency,
        target_currency: forex_rate['currency'],
        valid_from: forex_rate['created_at'],
        valid_till: forex_rate['updated_at'],
        created_at: forex_rate['created_at'],
        updated_at: forex_rate['updated_at']
      )
    end
  end
end
