namespace :transient do
  desc "Add missing FBTracks subscriptions to users with FB track monetizations"
  task missing_fb_tracks_subs: :environment do
    Person
      .joins(:track_monetizations)
      .where(
        'NOT EXISTS (:subscription)',
        subscription: PersonSubscriptionStatus
          .select('1')
          .where('people.id = person_subscription_statuses.person_id')
          .where("person_subscription_statuses.subscription_type = 'FBTracks'")
      )
      .where(track_monetizations: { store_id: 83 })
      .distinct
      .find_each do |person|
        PersonSubscriptionStatus.create(
          effective_date: Time.zone.now,
          grace_period_length: 5,
          person_id: person.id,
          subscription_type: 'FBTracks'
        )
        puts 'Missing subscription saved.'
      end

    puts 'All missing subscriptions were created.'
  end
end
