namespace :transient do
  task youtube_sr_ineligibility_rules: :environment do

    ytsr = Store.find_by(abbrev: "ytsr")
    ytsr_ineligible_genres = Genre.where(you_tube_eligible: false)
    ytsr_ineligible_keywords = YouTubeIneligibleKeyword.all

    ytsr_ineligible_genres.each do |genre|
      ytsr.ineligibility_rules.create!(
        property: "primary_genre_name",
        operator: "equal",
        value:    genre.name
      )

      ytsr.ineligibility_rules.create!(
        property: "secondary_genre_name",
        operator: "equal",
        value:    genre.name
      )
    end

    ytsr_ineligible_keywords.each do |keyword|
      ytsr.ineligibility_rules.create!(
        property: "album_name",
        operator: "contains",
        value:    keyword.keyword
      )

      ytsr.ineligibility_rules.create!(
        property: "song_name",
        operator: "contains",
        value:    keyword.keyword
      )
    end

  end
end
