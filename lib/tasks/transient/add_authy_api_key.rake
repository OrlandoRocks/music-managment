namespace :transient do
  task create_authy_api_key: :environment do
    params = { partner_name: "Authy", key: ENV["AUTHY_INTERNAL_KEY"] }
    ApiKey.create(params) unless ApiKey.where(params).any?
  end
end
