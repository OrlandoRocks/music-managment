namespace :transient do
  task ytsr_to_track_monetization_migration: :environment do
    limit = ENV.fetch("LIMIT", 100).to_i

    max_limit = Transient::YtsrTrackMonetizationMigrator::MAX_LIMIT

    puts "Initiating migration..."

    batch_limit(limit, max_limit) { |limit| migrate_ytsr_to_track_monetizations(limit) }

    puts "Migration complete!"
  end

  def migrate_ytsr_to_track_monetizations(limit)
    migrated = Transient::YtsrTrackMonetizationMigrator.migrate(limit: limit)

    unless migrated
      abort "No more track_monetizations to create! Congrats!"
    end
  end

  task ytsr_to_track_monetization_delivery_config_change: :environment do
    rollback = ENV.fetch("ROLLBACK", nil)

    distributable_class = rollback ? "DistributionSong" : "TrackMonetization"

    store_delivery_config = StoreDeliveryConfig.find_by(party_full_name: "YouTube_ContentID")

    unless store_delivery_config
      abort "StoreDeliveryConfig could not be found!"
    end

    if store_delivery_config.update(distributable_class: distributable_class)
      puts "Ytsr StoreDeliveryConfig distributable_class successfully changed to '#{distributable_class}'!"
    else
      puts "Failed to update Ytsr StoreDeliveryConfig: #{store_delivery_config.errors.full_messages}"
    end
  end

  task ytsr_to_track_monetization_migration_redeliveries: :environment do
    limit = ENV.fetch("LIMIT", 100).to_i

    max_limit = 1000

    puts "Initiating redeliveries..."

    batch_limit(limit, max_limit) { |limit| redeliver_new_youtube_track_monetizations(limit) }

    puts "Redeliveries completed!"
  end

  def redeliver_new_youtube_track_monetizations(limit)
    track_monetizations = TrackMonetization.includes(:transitions)
      .where(
        store_id: Store::YTSR_STORE_ID,
        state: [:enqueued, :pending_approval, :gathering_assets, :packaged],
        transitions: { id: nil }
      )
      .limit(limit)
      .uniq
      .where(takedown_at: nil)

    unless track_monetizations.present?
      abort "No more track_monetizations to redeliver! Congrats!"
    end

    track_monetizations.each do |track_monetization|
      track_monetization.retry({
        actor:   "Ytsr to Track Monetization Rake Task",
        message: "Retrying delivery for TrackMonetization ##{track_monetization.id} post-ytsr-migration"
      })
    end
  end

  def batch_limit(limit, max_limit)
    if limit > max_limit
      times = (limit.to_f / max_limit.to_f).ceil
      puts "Limit batched, will occur #{times} times..."
      times.times do |time|
        puts "Running #{time + 1}/#{times}..."
        yield max_limit
      end
    else
      yield limit
    end
  end
end
