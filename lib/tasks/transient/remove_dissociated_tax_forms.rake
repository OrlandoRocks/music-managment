namespace :transient do
  task remove_dissociated_tax_forms: :environment do
    # delete all outdated tax forms from accounts with multiple payout providers
    sql = "SELECT DISTINCT person_id FROM payout_providers GROUP BY person_id HAVING COUNT(person_id) > 1"
    ids = ActiveRecord::Base.connection.exec_query(sql).rows.flatten

    # delete all tax forms that are not the most recent tax form
    ids.each do |person_id|
      tax_forms = TaxForm.where(person_id: person_id).order('created_at DESC')
      tax_forms[1..-1].each(&:delete) if tax_forms.size > 1
    end

    # delete tax forms from users who have a inactive payoneer account and have not signed up again
    sql = "SELECT person_id FROM payout_providers WHERE active_provider = 0 GROUP BY person_id HAVING COUNT(person_id) = 1"
    ids = ActiveRecord::Base.connection.exec_query(sql).rows.flatten
    TaxForm.where(person_id: ids).delete_all
  end
end
