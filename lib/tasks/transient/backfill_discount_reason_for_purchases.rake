namespace :transient do 
    task backfill_discount_reason_for_purchases: :environment do
      Rails.logger.info("⏲ Beginning rake task... ⏲")
      
      DiscountReasonBackfill::WorkflowWorker.perform_async
  
      Rails.logger.info("✅ Rake task completed! ✅")
    end
end
  