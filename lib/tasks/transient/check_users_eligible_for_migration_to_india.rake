namespace :transient do
  desc "Check which users are eligible to be migrated to TuneCore India"
  task check_users_eligible_for_migration_to_india: :environment do
    # Users in country_website_id = 1 and country = “IN” with no transactions
    sql_1  = <<-SQL
      SELECT COUNT(*)
      FROM people
      LEFT JOIN person_transactions ON person_transactions.person_id = people.id
      WHERE country_website_id = 1
        AND country = "IN"
        AND person_transactions.person_id is null;
    SQL
    # Users from the previous query with Active TC Social Subscriptions
    sql_2 = <<-SQL
      SELECT COUNT(DISTINCT people.id)
      FROM people
      JOIN person_subscription_statuses ON people.id = person_subscription_statuses.person_id
        AND subscription_type = 'Social' 
        AND person_subscription_statuses.effective_date IS NOT NULL 
        AND (person_subscription_statuses.termination_date IS NULL 
          OR person_subscription_statuses.termination_date > CURRENT_DATE)
      LEFT JOIN person_transactions on person_transactions.person_id = people.id
      WHERE country_website_id = 1
        AND country = "IN"
        AND person_transactions.person_id is null;
    SQL

    puts "Users in country_website_id = 1 and country = “IN” with no transactions"
    puts ApplicationRecord.connection.execute(sql_1).first
    puts "Users from the previous query with Active TC Social Subscriptions"
    puts ApplicationRecord.connection.execute(sql_2).first
  end
end
