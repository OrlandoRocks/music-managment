namespace :transient do
  task add_country_website_language_id_to_cms_sets: :environment do
    CmsSet.joins(:country_website).each do |cs|
      country_website_language = CountryWebsiteLanguage.find_by(redirect_country_website_id: cs.country_website.id) || cs.country_website.country_website_languages.first
      cs.update(country_website_language_id: country_website_language.id)
    end
  end
end
