namespace :transient do
  task remove_html_artist_names: :environment do
    def migrate_artist(old_artist, correct_artist)
      Rails.logger.info("Cleaning up artist: #{old_artist.id}")

      ActiveRecord::Base.transaction do
        #Reassign creatives
        old_artist.creatives.each do |creative|
          creative.update!(artist_id: correct_artist.id)
        end

        #Reassign person's artist id
        if old_artist.person
          old_artist.person.update!(artist_id: correct_artist.id)
        end

        #Reassign blacklisted artist
        if old_artist.blacklisted_artist
          old_artist.blacklisted_artist.update!(artist_id: correct_artist.id)
        end

        #Reassign artist store whitelist
        if old_artist.artist_store_whitelists.any?
          old_artist.artist_store_whitelists.each do |aswl|
            aswl.update!(artist_id: correct_artist.id)
          end
        end
      end

      Rails.logger.info("Done artist #{old_artist.id}")
    end

    #Find all artists with the &amp issue.
    Artist.where("name LIKE '%&amp;%'").find_in_batches(batch_size: 100) do |artists|
      artists.each do |artist|
        corrected_name = artist.name.gsub('&amp;', '&')

        correct_artist = Artist.find_or_create_by!(name: corrected_name)

        migrate_artist(artist, correct_artist)
      end
    end
  end
end
