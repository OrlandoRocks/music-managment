namespace :transient do 
  task update_itunes_delivery_configs_delivery_queue: :environment do
    store_id = 36
    StoreDeliveryConfig
      .where(store_id: store_id)
      .update_all(delivery_queue: 'delivery-apple')
  end
end
