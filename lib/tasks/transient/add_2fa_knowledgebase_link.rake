namespace :transient do
  task add_two_factor_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "360000650226", article_slug: "two-factor-auth-faq", route_segment: nil).first_or_create
  end
end
