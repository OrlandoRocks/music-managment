namespace :transient do
  task create_compliance_info_fields_locked_flags: :environment do
    Flag.find_or_create_by!(Flag::COMPANY_NAME_LOCKED)
    Flag.find_or_create_by!(Flag::FIRST_NAME_LOCKED)
    Flag.find_or_create_by!(Flag::LAST_NAME_LOCKED)
    Flag.find_or_create_by!(Flag::CUSTOMER_TYPE_LOCKED)
    Flag.find_or_create_by!(Flag::ADDRESS_LOCKED)
  end
end
