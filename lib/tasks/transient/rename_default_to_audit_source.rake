namespace :transient do
  desc "Rename 'Default to Country Website' audit source to 'Default to US'"
  task rename_default_to_audit_source: :environment do
    default_to_audit_source = CountryAuditSource.find_by(name: 'Default to Country Website')
    default_to_audit_source.update(name: CountryAuditSource::DEFAULT_TO_US_NAME)
  end
end
