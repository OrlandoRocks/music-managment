namespace :transient do
  task remove_duplicated_renewal_history: :environment do
    RenewalHistory.where(starts_at: '2019-09-27', expires_at: '2019-10-08').delete_all
  end
end
