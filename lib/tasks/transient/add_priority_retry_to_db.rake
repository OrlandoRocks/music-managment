namespace :transient do
  task add_priority_retry_to_db: :environment do
    new_role = Role.where(
      name: "Priority Retry",
      long_name: "Priority Retry",
      description: "Gives admin the ability to redeliver an album immediately, ahead of any other enqueued distributions.",
      is_administrative: 1
    ).first_or_create!

    priority_retry_users = $rollout.get(:priority_retry).users

    priority_retry_users.each do |person_id|
      Person.find(person_id).roles << new_role
    end
  end
end