namespace :transient do
  desc "Updates batches to completed if completed"
  task process_mass_balance_adjustments_as_completed: :environment do
    MassAdjustmentBatch.process_batches_as_completed
  end
end
