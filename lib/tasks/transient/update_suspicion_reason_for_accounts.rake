namespace :transient do
  namespace :update_suspicion_reason_for_accounts do
    desc "update suspicion reason for accounts specified in gs-8008"
    task run: :environment do
      accounts_data_file = ENV['UPDATE_SUSPICION_ACCOUNTS_CSV']
      suspicious_accounts_data = CSV.open(accounts_data_file, headers: true)
      # Account of Paul Zachopoulos - Controller (Finances Dept.)
      task_admin_user = Person.find_by(id: ENV.fetch('ADMIN_ACCOUNT_ID', '2190048'))

      suspicious_accounts_data.each do |account_info|
        find_user_and_update_lock_reason(account_info[0], task_admin_user)
      end
    end

    def create_lock_reason_resolved_note(person, note_created_by)
      Note.create(
        related: person,
        subject: 'Suspicion Reason Updated',
        note: ENV.fetch('SUSPICION_NOTE_TEXT', 'No longer holding funds for GS-7640'),
        note_created_by: note_created_by
      )
    end

    def find_user_and_update_lock_reason(person_id, task_admin)
      person = Person.find_by(id: person_id)
      if person.present?
        note = create_lock_reason_resolved_note(person, task_admin)
        if note.persisted?
          print_task_log(:info, "successfully added note to person.", [person_id])
        else
          print_task_log(:error, "UNABLE TO CREATE NOTE.\n\tERROR: %s" % note.errors.full_messages.join(',') , [person_id])
        end
      else
        print_task_log(:error, 'UNABLE TO FIND ACCOUNT.', [person_id])
      end
    end

    def print_task_log(log_type, log_message, person_details=[])
      person_log_tag = "[account_id: %d]: " % person_details if person_details.present?
      Rails.logger.send(log_type, "[UPDATE SUSPICION REASON] #{person_log_tag}#{log_message}")
    end
  end
end
