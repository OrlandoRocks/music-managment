namespace :transient do
  task add_country_website_languages_from_seed: :environment do
    seed_file_path = ENV.fetch("SEED_FILE_PATH", "db/seed/csv/country_website_languages.csv")
    new_record_values = CSV.read(seed_file_path)

    klass = CountryWebsiteLanguage
    column_names = klass.column_names.map(&:to_sym)
    strictly_relevant_keys = column_names - [:id, :created_at, :updated_at]

    record_hashes = new_record_values.map do |value_set|
      initial_record_hash = column_names.zip(value_set).to_h.symbolize_keys

      initial_record_hash.slice(*strictly_relevant_keys)
    end

    record_hashes.each do |record_hash|
      klass.find_or_create_by(record_hash)
    end
  end
end
