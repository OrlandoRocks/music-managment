namespace :transient do
  task add_video_links_to_ytsr_appeal_email_templates: :environment do
    templates = YoutubeSrMailTemplate.where(template_name: "disputed_claim")

    templates.each do |template|
      old_body = template.body
      new_body = old_body.gsub(
        "<a href=\"\#{message[:custom_fields][\"1\"][\"link_to_video\"]}\"></a>",
        "<a href=\"\#{message[:custom_fields][\"1\"][\"link_to_video\"]}\">\#{message[:custom_fields][\"1\"][\"link_to_video\"]}</a>"
      )

      template.update(body: new_body)
    end
  end
end
