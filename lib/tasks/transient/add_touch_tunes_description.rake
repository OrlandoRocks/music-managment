namespace :transient do
  task :add_touch_tunes_description => :environment do
    store = Store.find_by(abbrev: "ttunes")
    store.update(description: "PlayNetwork provides restaurants, retail and hospitality establishments with non-interactive branded music curation and strategic marketing around in-store music.<br/><br/><strong>Strength:</strong> Reach. PlayNetwork delivers music for 400+ brands in 150,000+ locations worldwide.<br/><br/>TouchTunes provides curated, in-venue interactive music and entertainment within bars and restaurants across North America and Europe.<br/><br/><strong>Strength:</strong> Reach. TouchTunes’ digital jukebox platform can be found in over 75,000 establishments.<br/><br/><span style='color: red;'>Note:</span> Because TouchTunes and PlayNetwork curates any and all content available on their service, we cannot guarantee that they will accept your submission(s).<br/><br/><strong>Store Types:</strong> Discovery, Streaming")
  end
end
