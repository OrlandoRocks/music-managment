namespace :transient do
  task correct_country_website_language_selector_order: :environment do
    country_website_language_ids = [11, 12, 13]

    country_website_language_ids.each do |id|
      country_website_language = CountryWebsiteLanguage.find_by(id: id)

      country_website_language&.update!(selector_order: id)
    end

    Rails.logger.info("CountryWebsiteLanguage selector_orders corrected!")
  end
end
