namespace :transient do
  desc "Backfill is_matched column in scrapi_jobs"
  task update_is_matched_in_scrapi_jobs: :environment do
    sql = "UPDATE scrapi_jobs inner join scrapi_job_details ON scrapi_jobs.id = scrapi_job_details.scrapi_job_id SET is_matched = true"
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
