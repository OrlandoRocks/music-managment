namespace :transient do
  task aod_store_delivery_config: :environment do
    aod = StoreDeliveryConfig.readonly(false).for("AmazonOD")
    aod.update(
      password: "Lz5XkabLa7",
      transfer_type: "sftp")
  end
end