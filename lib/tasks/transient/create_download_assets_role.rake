namespace :transient do
  task create_download_assets_role: :environment do
    role = Role.find_or_create_by(name: 'Download Assets')
    role.update(
      long_name: "Download Assets",
      description: "Gives admin the capability to the individual album download feature.",
      is_administrative: true
    )
  end

  task create_mass_download_assets_role: :environment do
    Role.create(
      name: "Mass Download Assets",
      long_name: "Mass Download Assets",
      description: "Gives admin the capability to the Mass Download assets Tool",
      is_administrative: true
    )
  end

end
