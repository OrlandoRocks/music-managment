namespace :transient do
  desc "Flac Encode Re-Uploaded assets"
  task :flac_encode_reuploaded_assets, [:read_only] => [:environment] do |command, args|

    read_only = ActiveModel::Type::Boolean.new.cast(args.read_only)
    reuploaded_assets = []

    Song.includes(:s3_flac_asset, :s3_asset, :s3_orig_asset).in_batches do |songs|
      album_ids = []

      songs.each do |song|
        re_encode = false

        next unless song.s3_flac_asset.present? && !song.s3_flac_asset.key.include?("flacv1")

        if song.s3_orig_asset.present?
          re_encode = true if song.s3_flac_asset.updated_at < song.s3_orig_asset.updated_at
        elsif song.s3_asset.present?
          re_encode = true if song.s3_flac_asset.updated_at < song.s3_asset.updated_at
        end

        next unless re_encode

        reuploaded_assets.push({
          song_id: song.id, album_id: song.album_id
        })

        # Do not clear the s3_flac_asset if read only is true
        next if read_only

        flac_asset = song.s3_flac_asset
        ActiveRecord::Base.transaction do
          flac_asset.destroy
          song.update(duration_in_seconds: nil, s3_flac_asset_id: nil)
        end

        album_ids.push(song.album_id)
      end

      AssetsBackfill::Base.new(album_ids: album_ids.uniq).run if album_ids.present?
    end

    if reuploaded_assets.present?
      filepath = File.join(Rails.root, "tmp/", "#{Time.now.to_i}_reuploaded_assets.csv")
      CSV.open(filepath, "w", write_headers: true, headers: ["album_id", "song_id"]) do |file|
        reuploaded_assets.each { |asset| file << [asset[:album_id], asset[:song_id]] }
      end
      RakeDatafileService.upload(filepath)
    else
      p "No asset needs re-encoding!"
    end
  end
end
