# frozen_string_literal: true

namespace :transient do
  namespace :remove_suspicion_from_accounts do
    desc "extract accounts from ENV['REMOVE_SUSPICION_ACCOUNTS_CSV'] and remove suspicion."
    task run: :environment do
      # csv format: ensure that the first column is person_id
      if ENV['REMOVE_SUSPICION_ACCOUNTS_CSV'].present?
        accounts_data_file = ENV['REMOVE_SUSPICION_ACCOUNTS_CSV']
        suspicious_accounts_data = CSV.open(accounts_data_file, headers: true)

        # Account of Paul Zachopoulos - Controller (Finances Dept.)
        task_admin_user = Person.find_by(id: ENV.fetch('ADMIN_ACCOUNT_ID', '2190048'))

        suspicious_accounts_data.each do |account_info|
          find_account_and_remove_suspicion(account_info[0], task_admin_user)
        end
      else
        suspicion_print_task_log(
          :error,
          'PLEASE SPECIFY A CSV FILE WITH THE DETAILS OF ACCOUNTS FOR REMOVING SUSPICION.'
        )
      end
    end

    def create_suspicion_removal_note(person, note_created_by)
      Note.create(
        related: person,
        subject: 'Suspicion Removed',
        note: ENV.fetch('REMOVE_SUSPICION_NOTE', 'No longer holding funds for GS-7640'),
        note_created_by: note_created_by
      )
    end

    def find_account_and_remove_suspicion(person_id, task_admin)
      person = Person.find_by(id: person_id)
      if person.present?
        person_note = create_suspicion_removal_note(person, task_admin)
        if person_note.persisted?
          if person.remove_suspicion!
            suspicion_print_task_log(:info, "Successfully Removed Suspicion for account_id: #{person.id}")
          else
            suspicion_print_task_log(
              :error,
              "COULD NOT REMOVE SUSPICION FROM ACCOUNT.\n\tERROR: %s" % person.errors.full_messages.join(', '),
              [person_id]
            )
          end
        else
          suspicion_print_task_log(
            :error,
            "UNABLE TO CREATE NOTE.\n\tERROR: %s" % person_note.errors.full_messages.join(', '),
            [person_id]
          )
        end
      else
        suspicion_print_task_log(:error, 'UNABLE TO FIND ACCOUNT.', [person_id])
      end
    end

    def suspicion_print_task_log(log_type, log_message, person_details=[])
      person_log_tag = format("[account_id: %<account_id>d]: ", account_id: person_details) if person_details.present?
      Rails.logger.send(log_type, "[REMOVE SUSPICION FROM ACCOUNTS] #{person_log_tag}#{log_message}")
    end
  end
end
