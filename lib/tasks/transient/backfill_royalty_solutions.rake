namespace :transient do
  task backfill_royalty_solutions_three: :environment do
    include RoyaltySolutionsTempImporter

    csv_path = ENV['ROYALITY_CSV_PATH']
    original_csv_path = ENV['ORIGINAL_CSV_PATH']
    raise "NO CSV" unless csv_path
    raise "ORIGINAL CSV PATH" unless original_csv_path

    def find_event_date(errored_row)
      original_csv_path = ENV['ORIGINAL_CSV_PATH']

      CSV.foreach(original_csv_path, headers: true) do |orig_row|
        if errored_row['isrc'] == orig_row['ISRC']
          return orig_row['Request Date']
        end
      end
    end

    class RoyaltyCSVRow
      attr_accessor :email, :isrc, :upc, :success, :failure

      def initialize(email, isrc, upc, success, failure)
        self.email     = email
        self.isrc      = isrc
        self.upc       = upc
        self.success   = success
        self.failure   = failure
      end

      def to_string
        [email, isrc, upc, success, failure].join(',') + "\n"
      end
    end

    #write headers
    File.write('third_errors.csv', "email,isrc,upc,success,failure\n", mode: 'a')

    CSV.foreach(csv_path, headers: true) do |row|
      event_date = find_event_date(row)

      body = {
        event_date: find_event_date(row),
        isrcs: row['isrc'],
        upcs: row['upc'] || '',
      }

      unless event_date
        Rails.logger.error("Failed for row #{row}. CCan't find event date")
        next
      end

      resp = RoyaltySolutionsTempImporter.register(body)

      if resp[:status] == "success"
        csv_row = RoyaltyCSVRow.new(row['email'], row['isrc'], row['upc'], 'success', nil).to_string
      else
        csv_row = RoyaltyCSVRow.new(row['email'], row['isrc'], row['upc'], nil, resp).to_string
      end

      File.write('third_errors.csv', csv_row, mode: 'a')
    end
  end

  task backfill_royality_solutions_two: :environment do
    csv_path = ENV['ROYALTIES_CSV_PATH']
    royalty_api_key = ENV['ROYALTY_SOLUTIONS_API_KEY_INPUT']
    raise "No CSV" unless csv_path
    raise "Need API Key!" unless royalty_api_key

    def find_song_upc(isrc)
      song = Song.where("tunecore_isrc = '#{isrc}' OR optional_isrc = '#{isrc}'").first

      if !song
        Rails.logger.error("Can't find song with isrc #{isrc}") and return
      end

      upc = song.album.upcs.first&.number

      if !upc
        Rails.logger.error("No UPC associated to song #{song.id}") and return
      end

      upc
    end

    CSV.foreach(csv_path) do |row|
      upc  = row[3]
      isrc = row[2]

      body = {
        api_key: royalty_api_key,
        email: row[0],
        event_date: row[1],
        isrcs: isrc,
        upcs: upc,
      }

      if upc.blank?
        upc = find_song_upc(isrc)
        next unless upc

        body[:upcs] = upc
      end

      resp = Faraday.post(ENV['APP_HOST'] + "/api/royalty_solutions/register", body)

      if resp.success?
        Rails.logger.info("Success for #{row[0]}, isrc: #{row[2]}")
      else
        Rails.logger.error("Failed request for #{row[0]}, #{row[2]}")
        Rails.logger.error("Response status: #{resp.status}")
        Rails.logger.error("Response body: #{resp.body}")
      end
    end
  end


  task backfill_royalty_soutions_event_dates: :environment do
    csv_path        = ENV['ROYALTY_CSV_PATH']
    royalty_api_key = ENV['ROYALTY_SOLUTIONS_API_KEY_INPUT']

    raise "No CSV PATH!" unless csv_path.present?
    raise "Need API Key!" unless royalty_api_key

    CSV.foreach(csv_path, headers: true) do |row|
      body = {
        api_key: royalty_api_key,
        email: row["email"],
        event_date: row["request_date"],
        isrcs: row["isrc"],
        upcs: row["upc"],
      }

      resp = Faraday.post(ENV['APP_HOST'] + "/api/royalty_solutions/register", body)

      if resp.success?
        print(".")
      else
        Rails.logger.error("Failed request for #{row['email']}, #{row['isrc']}")
      end
    end

    Rails.logger.info("Done processing CSV")
  end
end
