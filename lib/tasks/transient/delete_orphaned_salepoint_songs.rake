namespace :transient do
  task :delete_orphaned_salepoint_songs => :environment do
    SalepointSong.includes(:salepoint).where(salepoints: {id: nil}).destroy_all
  end
end