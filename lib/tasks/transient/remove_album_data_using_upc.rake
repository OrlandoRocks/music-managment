namespace :transient do
  #Usage UPCS=810046030271,810046030356,810046030639 rake transient:remove_album
  task remove_album: :environment do
    if Rails.env.production?
      puts "Please don't run this on production"
      exit
    end

    ENV['UPCS'].split(',').each do |upc|
      upc = Upc.find_by(number: upc)

      next if upc.nil?

      resource = upc.upcable
      resource.salepoints.delete_all
      resource.songs.destroy_all
      resource.notes.destroy_all
      resource.destroy
    end
  end
end