namespace :transient do
  task backfill_ntc_album_account_id: :environment do
    NonTunecoreAlbum.joins(:account).update_all('non_tunecore_albums.account_id = accounts.id')
    Rails.logger.info "Backfilled NTC Album records"
  end
end
