namespace :transient do
    task backfill_default_label_names_for_ringtones: :environment do
      Rails.logger.info("⏲ Beginning rake task... ⏲")
  
      unfinalized_ringtones_without_labels = Ringtone.where(label: nil, finalized_at: nil)

      unfinalized_ringtones_without_labels.find_each do |ringtone|
        Rails.logger.info("Setting Default Label Name for ringtone id:#{ringtone.id}")
        begin
          ringtone.set_default_label_name
          if !ringtone.save
              Rails.logger.error(ringtone.errors.full_messages.join(". "))
          end
        rescue => e
          Rails.logger.error(e)
        end
      end
  
      Rails.logger.info("✅ Rake task completed! ✅")
    end
  end
  