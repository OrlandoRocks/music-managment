desc "Load additional indian languages from a CSV"
namespace :load do
  task extra_indian_languages: :environment do
    file_path = ENV["FILE_PATH"]

    unless file_path
      puts "FILE_PATH env variable is needed to run this rake task"
      next
    end

    CSV.foreach(file_path, headers: true) do |row|
      language = LanguageCode.find_or_initialize_by(description: row["description"])

      if language.persisted?
        puts "Language #{row['description']} already exists. Going to next."
        next
      end

      language.assign_attributes(row.to_hash)

      if language.save
        puts "Created - Language: #{row['description']}"
      else
        puts "Error - Language: #{row['description']} - Error: #{language.errors.full_messages.flatten.join(', ')}"
      end
    end

    puts "Finished creating languages. Good Day!"
  end
end
