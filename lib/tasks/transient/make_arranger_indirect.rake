namespace :transient do
  task make_arranger_indirect: :environment do
    ddex = DDEXRole.where(role_type: "arranger").first
    ddex.update(resource_contributor_role: false, indirect_contributor_role: true)

    srole = SongRole.where(role_type: "arranger").first
    srole.update(resource_contributor_role: false, indirect_contributor_role: true, user_defined: true)
  end
end
