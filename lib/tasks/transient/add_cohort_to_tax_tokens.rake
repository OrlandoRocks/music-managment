namespace :transient do
  task :seed_tax_tokens_cohort do
    TaxTokensCohort.create(start_date: "2017-11-20", end_date: "2017-11-28")
  end
  task :add_cohort_to_tax_tokens => :environment do
    ENV["tax_token_people_ids"].split(",").each do |person_id|
      tax_token = TaxToken.where(person_id: person_id).first
      tax_token.update(tax_tokens_cohort_id: ENV["tax_tokens_cohort_id"])
    end
  end
end
