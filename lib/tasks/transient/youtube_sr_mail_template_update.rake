namespace :transient do
  task youtube_sr_mail_template_update: :environment do
    template = YoutubeSrMailTemplate.find_by(template_name: 'authorization')

    new_body = <<~HEREDOC
    @message_data.map do |message|
      custom_fields = message[:custom_fields].map do |_, custom_field|
        <<-HTML
        <ul>
          <li>Video Uploader: <strong>\#{custom_field["video_uploader"]}</strong></li>
          <li>Video Link: <a href="\#{custom_field["link_to_video"]}">\#{custom_field["link_to_video"]}</a></li>
        </ul>
        HTML
      end.join("")
    
      <<-HTML
      <p>Song Title: <strong>\#{message[:song].name}</strong> with ISRC: <strong>\#{message[:song].isrc}</strong></p>
      \#{custom_fields}
      HTML
    end.join("")
    HEREDOC

    template.update(body: new_body)
  end
end
