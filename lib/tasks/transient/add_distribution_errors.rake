namespace :transient do
  task add_distribution_errors: :environment do
    [
      "Itunes package verify failed",
      "No such file or directory",
      "Error determining duration",
      "undefined local variable or method `server' for #",
      "can't convert nil into String",
      "Connection timed out",
      "452 Transfer aborted. No space left on device",
      "550 The filename, directory name, or volume label syntax is incorrect",
      "SFTP session failed"
    ].each do |message|
      DistributionError.create(message: message)
    end
  end
end
