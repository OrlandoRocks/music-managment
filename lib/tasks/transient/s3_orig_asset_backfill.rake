namespace :transient do
  desc 'Try to populate song.s3_orig_asset_id,'
  task backfill_missing_s3_orig_asset: :environment do
    overwrite = ENV['OVERWRITE'] == 'true'
    AssetsBackfill::S3OrigAsset.new.backfill_missing(overwrite)
  end

  desc 'Correct s3_orig_asset_id when possible'
  task fix_s3_orig_asset: :environment do
    overwrite = ENV['OVERWRITE'] == 'true'
    AssetsBackfill::S3OrigAsset.new.fix_originals(overwrite)
  end
end
