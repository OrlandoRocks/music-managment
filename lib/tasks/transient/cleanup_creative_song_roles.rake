desc "Cleanup Stale Records for Creative Song Roles"
namespace :transient do
  task cleanup_creative_song_roles: :environment do

    creative_song_roles = CreativeSongRole
      .joins('LEFT JOIN creatives ON creatives.id = creative_song_roles.creative_id')
      .joins('LEFT JOIN songs ON songs.id = creative_song_roles.song_id')
      .where('creatives.id IS NULL OR songs.id IS NULL')
      .where.not(creative_song_roles: {creative_id: nil, song_id: nil})

    puts "#{creative_song_roles.size} creative song roles to be deleted."

    creative_song_roles.destroy_all

    puts "Cleaned up the stale records for creative song roles"
  end
end
