namespace :transient do
  desc "Create Feature Flag for renewal payment splits"
  task create_payment_splits_renewal_flag: :environment do
    FeatureFlipper.add_feature('payment_splits_renewal')
  end
end
