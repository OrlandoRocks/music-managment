namespace :transient do
  task musicisland_enable_backfill: :environment do
    misland = Store.find_by(short_name: "M_Island")

    misland.update(in_use_flag: true)

    misland.salepointable_stores.create!(salepointable_type: "Album")
    misland.salepointable_stores.create!(salepointable_type: "Single")

    sdc = StoreDeliveryConfig.readonly(false).for("M_Island")
    sdc.update(delivery_queue: "delivery-low")
  end
end