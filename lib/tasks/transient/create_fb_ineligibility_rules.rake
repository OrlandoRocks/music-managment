namespace :transient do
  task create_fb_ineligibility_rules: :environment do
    Store.find(Store::YTSR_STORE_ID).ineligibility_rules.each do |rule|
      return if IneligibilityRule.exists?(
        property: rule.property,
        operator: rule.operator,
        value: rule.value,
        store_id: Store::FBTRACKS_STORE_ID
      )

      IneligibilityRule.create(
        property: rule.property,
        operator: rule.operator,
        value: rule.value,
        store_id: Store::FBTRACKS_STORE_ID
      )
    end
  end
end
