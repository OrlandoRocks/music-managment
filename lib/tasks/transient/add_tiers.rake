namespace :transient do
  task add_tiers: :environment do
    s3_file_name = ENV['INITIAL_TIERS']

    COLUMN = {
      id: 0,
      name: 1,
      hierarchy: 2,
      is_active: 3,
      is_sub_tier: 4,
      parent_id: 5
    }

    tiers = RakeDatafileService.csv(s3_file_name)

    tiers.each do |tier|
      current_tier = Tier.find_or_create_by(
        name: tier[COLUMN[:name]],
        hierarchy: tier[COLUMN[:hierarchy]],
        is_active: tier[COLUMN[:is_active]],
        is_sub_tier: tier[COLUMN[:is_sub_tier]],
        parent_id: tier[COLUMN[:parent_id]]
      )
    end

    puts "Tiers created"
  end
end
