namespace :transient do
  task update_juke: :environment do
    kb_link = KnowledgebaseLink.find_by(article_slug: 'juke-description')
    kb_link.update!(article_id: 360039364311)
  end
end
