namespace :transient do
  task rename_touchtunes_store: :environment do
    store = Store.find_by(abbrev: "ttunes")
    store.update(name: "TouchTunes/PlayNetwork")
  end
end
