namespace :transient do
  namespace :update_taxform_expiry do
    task run: :environment do
      TaxForm.select(:person_id).distinct(:person_id).pluck(:person_id).map do |person_id|
        FetchTaxFormsWorker.perform_async(person_id)
      end
    end
  end
end
