namespace :transient do
  task add_es_locale: :environment do
    CountryWebsiteLanguage.create({
      country_website_id: CountryWebsite::UNITED_STATES,
      yml_languages: 'es-us',
      selector_language: 'Español',
      selector_country: 'Latin America',
      selector_order: 10,
      redirect_country_website_id: nil
    })

    Rails.logger.info("Added ES country website language")
  end

  task add_indonesian_locale: :environment do
    CountryWebsiteLanguage.create({
      country_website_id: CountryWebsite::UNITED_STATES,
      yml_languages: 'id-us',
      selector_language: 'Bahasa Indonesia',
      selector_country: 'Indonesia',
      selector_order: 11,
      redirect_country_website_id: nil
    })

    Rails.logger.info("Added Indonesia country website language")
  end

  task add_romanian_locale: :environment do
    CountryWebsiteLanguage.create({
      country_website_id: CountryWebsite::UNITED_STATES,
      yml_languages: 'ro-us',
      selector_language: 'Română',
      selector_country: 'Romania',
      selector_order: 12,
      redirect_country_website_id: nil
    })

    Rails.logger.info("Added Romania country website language")
  end

  task rename_en_locale: :environment do
    CountryWebsiteLanguage.find_by(yml_languages: 'en').update(yml_languages: 'en-us')

    Rails.logger.info("Renamed EN locale country website language")
  end
end
