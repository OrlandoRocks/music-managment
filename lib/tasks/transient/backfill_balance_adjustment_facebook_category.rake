namespace :transient do
  desc "Backfill balance adjustments Facebook category"
  task backfill_balance_adjustment_facebook_category: :environment do
    Rails.logger.info("Initialized Facebook category backfill")

    batch_size = ENV["BATCH_SIZE"] || "100000"
    category = BalanceAdjustmentCategory.find_by(name: "Facebook")

    # following query returns ~2M records at present
    BalanceAdjustment
      .where("admin_note like ?", "%Facebook Payment%")
      .where.not(category: category.name)
      .in_batches(of: Integer(batch_size, 10))
      .each_with_index do |batch, batch_no|
        batch.update_all(category: category.name)
        Rails.logger.info("Dispatched batch #{batch_no}")
      end

    # following query returns 24 records at present
    MassAdjustmentBatch
      .where("admin_message like ?", "%Facebook Payment%")
      .update_all(balance_adjustment_category_id: category.id)

    Rails.logger.info("Concluded Facebook category backfill")
  end
end
