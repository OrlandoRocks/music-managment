namespace :transient do
  task youtube_sr_subscription_products: :environment do 

    ytsr_product_ids = Product.where(display_name: "ytsr").pluck(:id)

    ytsr_product_ids.each do |id|
      SubscriptionProduct.create!(
        product_name: "YTTracks",
        product_type: "annually",
        product_id: id,
        term_length: 12
      )
    end 

  end 
end 