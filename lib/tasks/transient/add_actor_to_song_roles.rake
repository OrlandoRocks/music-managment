namespace :transient do
  task add_actor_to_song_roles: :environment do
    song_role = SongRole.create(role_type: "actor", resource_contributor_role: 1, indirect_contributor_role: 0, user_defined: 0, role_group: "other roles")
    ddex_role = DDEXRole.create(role_type: "actor", resource_contributor_role: 1, indirect_contributor_role: 0)
    DDEXSongRole.create(song_role_id: song_role.id, ddex_role_id: ddex_role.id)
  end
end
