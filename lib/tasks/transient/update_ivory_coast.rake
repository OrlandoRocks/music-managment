namespace :transient do
  task update_ivory_coast: :environment do
    ivory_coast = Country.sanctioned.find_by(iso_code: 'CI')
    ivory_coast.update(is_sanctioned: 0) if ivory_coast
  end
end