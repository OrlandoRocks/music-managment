namespace :transient do
  desc "Update old iHeart distributions with current converter class"
  task iheart_converter_backfill: :environment do
    Distribution
      .where(converter_class: 'MusicStores::Thumbplay::Converter').in_batches do |batch|
        batch.update_all(converter_class: 'MusicStores::DDEX::Converter')
      end
  end
end
