namespace :transient do
  task backfill_stored_adyen_payment_method_info: :environment do
    transaction_ids = AdyenTransaction
      .where(
        adyen_stored_payment_method_id: nil,
        related_transaction_id: nil,
        result_code: AdyenTransaction::AUTHORISED
      ).where.not(psp_reference: nil).pluck(:id)

    Rails.logger.info("Updating stored_payment_method_info for adyen_transactions: #{transaction_ids}")
    transaction_ids.each { |id| Adyen::RecurringDetailsUpdateWorker.perform_async(id) }
  end
end
