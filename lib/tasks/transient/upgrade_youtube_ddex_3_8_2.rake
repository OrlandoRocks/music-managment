namespace :transient do
  desc "Upgrade Youtube Music DDEX to 3.8.2"
  task upgrade_youtube_ddex_3_8_2: :environment do
    Rails.logger.info("Beginning rake task...")

    if StoreDeliveryConfig.find_by(store_id: 28).update(ddex_version: 382)
      Rails.logger.info("YouTube Music / YouTube Shorts upgraded to 3.8.2")
    else
      Rails.logger.info("Something went wrong")
    end
  end
end
