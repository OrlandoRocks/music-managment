namespace :transient do
  task add_iso_639_1_code_indian_languages: :environment do
    {
      bho: 'bh',
      brx: 'bn',
      doi: 'hi',
      grt: 'bn',
      kha: 'bn',
      trp: 'bn',
      kok: 'kn',
      ccl: 'sd',
      mai: 'bh',
      mni: 'bn',
      lus: 'bn',
      sat: 'bh',
      tcy: 'kn'
    }.each do |code, iso_639_1_code|
      LanguageCode.find_by(code: code).update(ISO_639_1_code: iso_639_1_code)
    end
  end
end
