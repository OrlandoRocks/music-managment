namespace :transient do
  desc "Adds missing adyen_payment_method_info to adyen_transaction"
  task :add_missing_adyen_payment_method_info, [:adyen_transaction_id, :payment_method_info_id] => :environment do |_t, args|
    Rails.logger.info "Add missing payment method info"

    adyen_transaction = AdyenTransaction.find args.adyen_transaction_id
    adyen_transaction.update_column(:adyen_payment_method_info_id, args.payment_method_info_id)
    adyen_stored_payment_method = adyen_transaction.adyen_stored_payment_method
    adyen_stored_payment_method.update_column(:adyen_payment_method_info_id, args.payment_method_info_id)
  end
end
