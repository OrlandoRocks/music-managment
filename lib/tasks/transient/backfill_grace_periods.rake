namespace :transient do
  task :backfill_grace_periods => :environment do
    PersonSubscriptionStatus.where(subscription_type: "Social").update_all(grace_period_length: 5)
  end
end
