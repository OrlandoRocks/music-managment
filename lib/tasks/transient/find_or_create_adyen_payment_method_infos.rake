namespace :transient do
  task find_or_create_adyen_payment_method_infos: :environment do
    file = Rails.root.join("db/seed/csv/adyen_payment_method_infos.csv")
    csv = CSV.read(file, liberal_parsing: true)
    csv.each do |row|
      AdyenPaymentMethodInfo.find_or_create_by(payment_method_name: row[1])
    end
  end
end
