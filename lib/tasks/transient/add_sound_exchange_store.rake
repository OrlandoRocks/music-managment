namespace :transient do
  task :add_sound_exchange_store => :environment do
    store_info = { id: 80, name: "Sound Exchange", abbrev: "sndxch", short_name: "SndXchg", position: 760 }
    StoreCreator.create(store_info)
  end
end
