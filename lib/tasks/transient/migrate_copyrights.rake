namespace :transient do
  desc "Migrate copyright records to new format"
  task migrate_copyrights: :environment do
    # Consolidate record pairs to single record
    # If composition is nil, the record is in the old format, and so is part of a pair.
    # We will later archive all records, then delete the un-updated pair member.
    Copyright
      .where(composition: nil)
      .group(:copyrightable_id)
      .update(composition: true, recording: true)
  end
end
