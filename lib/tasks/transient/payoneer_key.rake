namespace :transient do
  desc "adds payoneer api key"
  task add_payoneer_api_key: :environment do
    payoneer_key_exists = ApiKey.exists?(partner_name: "Payoneer")
    ApiKey.create(partner_name: "Payoneer", key: SecureRandom.hex) unless payoneer_key_exists
  end
end
