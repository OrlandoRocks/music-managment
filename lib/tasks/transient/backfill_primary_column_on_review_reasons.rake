namespace :transient do
  task backfill_primary_column: :environment do
    primary_review_reasons = [
      "Artwork/Metadata: Cover art/album info mismatch (unfinalize release)",
      "Copyright: Need More Info, Possibly using original masters",
      "Copyright: TuneCore Will Not Distribute (Viet, Nazi, etc)",
      "Duplicate Audio: Inconsistent Metadata",
      "Metadata: Generic and Duplicate Titles",
      "Metadata: Correct featured artists (unfinalize release)"
    ]

    ReviewReason.where(reason: primary_review_reasons).update_all(primary: true)
    ReviewReason.where.not(reason: primary_review_reasons).update_all(primary: false)
  end
end
