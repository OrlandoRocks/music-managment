namespace :transient do
  desc 'Removes gst_config from transactions that are not payments_os settled'
  task remove_gst_from_non_india_invoices: :environment do
    non_payments_os_invoices = Invoice
      .joins(:invoice_settlements)
      .where.not(
        invoice_settlements:
        { source_type: 'PaymentsOSTransaction' }
      )

    non_payments_os_invoices.update_all(gst_config_id: nil)
    puts "Invoices updated successfully!"
  end
end
