namespace :transient do
  namespace :fix_store_mismap do
    desc "Fix mapping store name mismatch between sip stores & tc-www stores"
    task name_mismap: :environment do
      data = RakeDatafileService.csv("cf-1075-name-with-leading-space.csv", headers: true)
      data.each do |store_meta|
        store      = SipStore.find_by(id: store_meta["tcw_store_id"])
        store.name = store.name.strip
        if store.save
          Rails.logger.info "Removing Spaces from store: #{store_meta["tcw_store_name"]}"
        else
          Rails.logger.error "Error Removing Spaces from store: #{store.name}"
        end
      end
    end

    desc "Fix ID mismatch between tc-www & sip stores"
    task id_mismap: :environment do
      data           = RakeDatafileService.csv("cf-1075-store-id-mismatch.csv")
      display_groups = SipStore
        .all
        .each_with_object({}) { |store, acc| acc[store.name] = store.display_group_id }

      data.each do |store_meta|
        tc_store = SipStore.find_by(id: store_meta["sip_store_id"])
        if tc_store.present?
          tc_store.name             = store_meta["sip_store_name"]
          tc_store.display_group_id = display_groups[store_meta["sip_store_name"]]

          unless tc_store.save
            Rails.logger.error "Unable to Update Store: #{store_meta}"
          end
        else
          Rails.logger.error "Store not found #{store_meta}"
        end
      end
    end
  end
end
