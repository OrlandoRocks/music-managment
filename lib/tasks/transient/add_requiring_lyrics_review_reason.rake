namespace :transient do
  task add_requiring_release_review_reason: :environment do
    review_reason = ReviewReason.find_or_create_by(
      reason_type: "REJECTED",
      reason: "Metadata: Requiring Lyrics",
      review_type: "LEGAL",
      email_template_path: "denied_requiring_lyrics",
      should_unfinalize: 1,
      primary: 0,
      template_type: "changes_requested"
    )
    roles = Role.where(name: ['Content Review Specialist', 'Content Review Manager'])
    review_reason.roles << roles
  end
end
