desc "Load genres into genres table from a CSV"
namespace :load do
  task genres: :environment do
    file_path = ENV["GENRE_FILE_PATH"]

    unless file_path
      puts "GENRE_FILE_PATH env variable is needed to run this rake task"
      next
    end

    CSV.foreach(file_path, headers: true) do |row|
      genre = Genre.find_or_initialize_by(name: row["name"])

      if genre.id.present?
        puts "#{genre.name} already exists. Skipping."
        next
      end

      genre.assign_attributes(row.to_h)

      if genre.save
        puts "Created Genre: #{genre.name}"
      else
        puts "Error - Genre: #{genre.name} - Error: #{genre.errors.full_messages.flatten.join(', ')}"
      end
    end

    puts "Finished creating genres. Good Day!"
  end
end
