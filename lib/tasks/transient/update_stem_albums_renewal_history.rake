namespace :transient do
  #USAGE: STEM_ALBUM_IDS_CSV_PATH="/home/deploy/stem_rh_update_album_ids.csv" rake transient:update_stem_albums_renewal_history

  task update_stem_albums_renewal_history: :environment do
    logger = Logger.new(Rails.root.join("log/stem-rh-update.log"))
    album_ids = CSV.read(ENV['STEM_ALBUM_IDS_CSV_PATH']).flatten
    new_expires_at = Date.strptime('2020-02-15', '%Y-%m-%d')

    Album.where(id: album_ids).each do |album|
      if album.renewal_histories.length == 1
        rh = album.renewal_histories.first

        if rh.expires_at.to_date == Date.strptime('2019-10-27', '%Y-%m-%d') && rh.update(expires_at: new_expires_at)
          logger.info("Successfully updated expires_at to #{new_expires_at} AlbumId:#{album.id} RenewalHistoryId:#{rh.id}")
        else
          logger.info("Unable to update expires_at for AlbumId: #{album.id} RenewalHistoryId:#{rh.id}")
        end
      else
        logger.info("Skipping AlbumId: #{album.id}")
      end
    end
  end
end
