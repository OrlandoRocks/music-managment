namespace :transient do
  task create_auto_renewal_email_sent_flag: :environment do
    Flag.find_or_create_by!(Flag::AUTO_RENEWAL_EMAIL_SENT)
  end
end
