namespace :transient do
  desc "fix false track monetization delivery errors"
  task fix_false_track_monetization_errors: :environment do
    track_monetization_join = "
      INNER JOIN track_monetizations tm on tm.id = transitions.state_machine_id
      and transitions.state_machine_type = 'TrackMonetization'
    "
    transitions = Transition
                  .joins(track_monetization_join)
                  .where(created_at: '2020-10-15'..)
                  .where("message like ?", "%post_delivery_update")
                  .where("tm.delivery_type = 'full_delivery' and tm.state = 'error'")

    TrackMonetization
      .where(id: transitions.pluck(:state_machine_id))
      .in_batches { |batch| batch.update_all(state: 'delivered') }

    transitions.delete_all
    puts 'Task completed successfully.'
  end

  desc "unblock taken-down track monetizations"
  task unblock_tm_takedowns: :environment do
    TrackMonetization
      .where.not(takedown_at: nil)
      .where(state: 'blocked', delivery_type: 'metadata_only')
      .in_batches { |batch| batch.update_all(state: 'delivered') }
  puts 'Task completed successfully.'
  end
end
