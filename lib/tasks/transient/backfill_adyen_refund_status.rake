namespace :transient do
  desc "Backfill adyen refund transaction status"
  task backfill_adyen_refund_status: [:environment] do
    AdyenTransaction.where.not(related_transaction_id: nil).find_each do |record|
      next if record.result_code == "Received"

      status = record.result_code == "Authorised" ? "success" : "failure"
      record.update!(status: status)
    end
  end
end
