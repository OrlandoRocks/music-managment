namespace :transient do
  desc "add mass adjustments approver role"
  task add_mass_adjustments_approver_role_to_db: :environment do
    Role.create(
        name: "Mass Adjustments Approver",
        long_name: "Mass Balances Adjustment Approver",
        description: "Gives admin ability to approve or reject an uploaded mass balance adjustments request",
        is_administrative: true
    )
  end
end