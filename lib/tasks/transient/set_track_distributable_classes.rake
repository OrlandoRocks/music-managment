namespace :transient do
  task set_sdc_distributable_classes: :environment do
    StoreDeliveryConfig.readonly(false).for("YoutubeSR")
      .update(distributable_class: "DistributionSong")

    StoreDeliveryConfig.readonly(false).for("FBTracks")
      .update(distributable_class: "TrackMonetization")
  end
end