namespace :transient do
  task send_stem_reminder_email_to_users_without_payment: :environment do
    # this rake task corresponds to the reminder emails set in the 9/25/2019 sprint
    stem_users = Person.joins(
      "LEFT JOIN person_preferences ON people.id = person_preferences.person_id"
    ).where(
      referral: "stem", person_preferences: { preferred_payment_type: nil }
    )

    stem_users.each do |person|
      MailerWorker.perform_async("StemMailer", :stem_onboarding_reminder, person.id)
    end
  end
end
