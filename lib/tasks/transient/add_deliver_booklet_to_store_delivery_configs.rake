namespace :transient do
  task add_deliver_booklet_to_store_delivery_configs: :environment do
    StoreDeliveryConfig.all.each do |config|
      config.update(deliver_booklet: false)
    end
  end
end
