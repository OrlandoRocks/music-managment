namespace :transient do
  desc "TODO"
  task :migrate_tuncore_upcs_to_upc_type => :environment do
    Upc.where(tunecore_upc: 1).update_all(upc_type: 'tunecore')
    Upc.where(tunecore_upc: 0).update_all(upc_type: 'optional')
  end
end
