namespace :transient do
  task update_tfa_auth_event_actions: :environment do
    TwoFactorAuthEvent.where(action: "set_preference").update_all(action: "preferences")
  end
end
