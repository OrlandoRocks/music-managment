namespace :transient do
  task block_specified_publishing_accounts: :environment do

    # the below account identifiers are for Fireheart publishing accounts
    account_identifiers = %w(
      TUN0000003738A
      TUN0000033729A
      TUN0000024481A
      TUN0000035117A
      TUN0000043481A
      TUN0000053364A
      TUN0000043479A
      TUN0000043489A
      TUN0000043480A
      TUN0000043490A
      TUN0000043483A
      TUN0000045087A
      TUN0000043874A
      TUN0000043873A
    )

    accounts = Account.where(provider_account_id: account_identifiers, blocked: false)

    puts "Found #{accounts.count} publishing accounts to block."
    puts "Blocking these publishing accounts..."

    accounts.update_all(blocked: true)

    puts "Accounts blocked! Thank you!"
  end
end
