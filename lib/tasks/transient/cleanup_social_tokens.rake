desc "Cleanup TC Social tokens for non allowed regions"
namespace :cleanup do
  task social_tokens: :environment do
    client_application_id = ClientApplication.find_by(name: 'tc_social').id

    allowed_countries = CountryWebsite
      .where(country: ClientApplication::SOCIAL_ALLOWED_REGIONS)
      .pluck(:id)

    tokens = OauthToken
      .joins(:user)
      .where(client_application_id: client_application_id)
      .where('token is not null')
      .where('people.country_website_id NOT IN (?)', allowed_countries)

    puts "#{tokens.size} to be deleted."

    tokens.destroy_all

    puts "Cleaned up all the tokens. Ciao."
  end
end
