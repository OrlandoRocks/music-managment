namespace :transient do
  task convert_batch_manager: :environment do
    DistributionBatch.all.each do |dist_batch|
      del_batch = DeliveryBatch.create(dist_batch.attributes)
      dist_batch.distribution_batch_items.each do |dist_batch_item|
        if dist_batch.store_id == 83 # FBTracks
          DeliveryBatchItem.create(delivery_batch_id: del_batch.id,
                                   deliverable_type: "TrackMonetization",
                                   deliverable_id: dist_batch_item.distribution_id,
                                   status: dist_batch_item.status)
        else
          DeliveryBatchItem.create(delivery_batch_id: del_batch.id,
                                   deliverable_type: "Distribution",
                                   deliverable_id: dist_batch_item.distribution_id,
                                   status: dist_batch_item.status)
        end
      end
    end
  end

  task close_active_batches: :environment do
    active_batches = DistributionBatch.where(active: true)
    active_ids = active_batches.map(&:id).to_csv

    write_path = File.join("tmp", "old_distro_batches.csv")
    File.write(write_path, active_ids)

    active_batches.update_all(active: false)

    active_batches.each do |batch|
      Rails.log("Closing batch #{batch.id}.")
      if batch.store_id == 83
        @distributable = TrackMonetization.find(batch.distribution_batch_items.last.distribution_id)
      else
        @distributable = batch.distributions.last
      end
      @distribution_system = DistributionSystem::Deliverer.new(DELIVERY_CONFIG)
      @converted_album = @distributable.convert("metadata_only")
      @distributor = DistributionSystem::DistributorFactory.distributor_for(@converted_album, @distribution_system)

      @distributor.remote_server.connect do |server|
        @distributor.send_complete_file(server)
        Rails.log("Batch complete for #{batch.id} sent")
      end
    end
  end
end
