FAILED_HOLDS = []
namespace :transient do
  task backfill_copyright_claim: :environment do
    file_name = "Holds_Backfill.xlsx"
    path_name = Rails.root.join("tmp", ENV["RAILS_ENV"], file_name)
    file = RakeDatafileService.download(
      file_name,
      temporary: true,
      environment_folder: ENV["RAILS_ENV"],
      binary_mode: true
    ).local_file
    workbook = Roo::Spreadsheet.open(file)
    error_file_path = Rails.root + "tmp/failed_holds.xls"
    csv_file_path = Rails.root + "tmp/parsed_holds.csv"
    failed_holds = []
    CSV.open(csv_file_path, "w") do |csv|
      csv << ["internal_claim_id", "asset_type", "asset_id", "person_id", "store_id", "claimant_emails", "Worksheet Name - Row ID"]
      workbook.each_with_pagename do |name, sheet|
        unless name == 'YTD 21'
          header_row = sheet.row(1)
          track_index = header_row.index(header_row.grep(/track number/i)&.first)
          store_index = header_row.index(header_row.grep(/Store Name\/Claimant/i)&.first)
          claimant_index = header_row.index(header_row.grep(/claimant email/i)&.first)
          (2..sheet.last_row).each do |row|
            record = sheet.row(row)
            internal_tracking_number = record[4].to_s.strip
            upcs = record[5].to_s.scan(/\d+/)
            track_numbers = track_index ? record[track_index].to_s.split(';') : []
            store_name = record[store_index]
            claimants = record[claimant_index]
            albums = Hash[upcs&.zip(track_numbers)] if upcs
            next log_error(record, name, "internal tracking_number is blank") unless internal_tracking_number
            next log_error(record, name, "upc is blank") if upcs.blank?
            albums.each do |key, value|
              album = Album.find_by_upc(key)
              puts "Album found with upc #{key} with id #{album&.id} in sheet #{name}"
              next log_error(record, name, "Upc not found #{key}") unless album
              unless store_name&.downcase == 'claimant'
                store =  Store.where(name: store_name).first
                next log_error(record, name, "Store not found #{store_name}") if !store
              end
              if (!value || value == "all")
                puts value
                csv << [internal_tracking_number, "Album", album.id, album.person_id, store ? store.id : nil, claimants, "Sheet #{name} -- Row #{row}"]
              else
                value.scan(/\d+/).each do |track_id|
                  song = album.songs.where(track_num: track_id).first
                  next log_error(record, name, "Song with track no #{track_id} not found for album  #{key}") unless song
                  csv << [internal_tracking_number, "Song", song.id, album.person_id, store ? store.id : nil, claimants, "Sheet #{name} -- Row #{row}"]
                end
              end
            end
          end
        end
      end
    end
    RakeDatafileService.upload(csv_file_path, overwrite: 'y')
    puts FAILED_HOLDS.count
    create_claims_from_csv
    SimpleXlsx::Serializer.new(error_file_path) do |doc|
      doc.add_sheet('Failed Claims') do |sheet|
        FAILED_HOLDS.each { |row| sheet.add_row(row)}
      end
    end
    RakeDatafileService.upload(error_file_path, overwrite: 'y')
  end
end

def log_error(record, sheet_name, error_msg)
  FAILED_HOLDS << (record << [error_msg, sheet_name])
  puts error_msg + sheet_name
end

def create_claims_from_csv
  admin_id = Person.where(email: 'dylan@tunecore.com')&.first&.id
  csv = CSV.read(Rails.root + "tmp/parsed_holds.csv", headers: true)
  csv.each.with_index do |row, i|
    create_claimants( row["asset_type"],row["asset_id"],row["internal_claim_id"],row["person_id"],row["store_id"], row["claimant_emails"], admin_id)
  end
end

def create_claimants(asset_type, asset_id, internal_tracking_number, person_id, store_id, claimant_emails, admin_id)
  claim = CopyrightClaim.new(asset_type: asset_type,asset_id: asset_id, store_id: store_id, internal_claim_id: internal_tracking_number, person_id: person_id, fraud_type: 'Copyright Claim', admin_id: admin_id , compute_hold_amount: true)
  if claim.save 
    create_claimant_emails(claim, claimant_emails) if store_id.blank?
  else
    log_error([asset_type, asset_id], "Create Error", "Claim not created - #{claim.errors.full_messages}")
  end
end

def create_claimant_emails(claim, claimants)
  return if claimants.blank?
  claimant_emails = claimants&.downcase&.split(/,|;/)&.map(&:strip)
  claimant_emails.each do |email|
    claim.copyright_claimants.create(email: email) unless email.blank?
  end
end
