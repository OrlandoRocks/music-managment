namespace :transient do
  task use_resource_dir_update: :environment do

    StoreDeliveryConfig.find([18, 47]).each do |sdc|
      sdc.update_attribute(:use_resource_dir, true)
    end

    StoreDeliveryConfig.where(use_resource_dir: nil).map{|sdc| sdc.update_attribute(:use_resource_dir, true)}
  end
end