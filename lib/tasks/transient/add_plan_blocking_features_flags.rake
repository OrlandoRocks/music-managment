# frozen_string_literal: true

namespace :transient do
  # bundle exec rake transient:add_blocked_from_plans_flag
  task add_blocked_from_plans_flag: :environment do
    Flag.find_or_create_by(Flag::BLOCKED_FROM_PLANS)
    puts "Great success, created Blocked from Plans Flag"
  end

  # bundle exec rake transient:block_from_plans_by_email\[email_goes_here\]
  task :block_from_plans_by_email, [:email] => :environment do |_, args|
    person = Person.find_by(email: args[:email])
    flag = Flag.find_by(Flag::BLOCKED_FROM_PLANS)
    puts "cant find person from email" unless person
    return unless person

    PeopleFlag.find_or_create_by(person: person, flag: flag)
    puts "Great success, created people flag for person"
  end
end
