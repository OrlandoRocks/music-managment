namespace :transient do
  task create_2fa_role: :environment do
    Role.create(
      name: "tfa_admin",
      long_name: "Two-Factor Auth Admin",
      description: "Gives admins the ability to adjust two-factor auth settings for users",
      is_administrative: true
      )
  end
end