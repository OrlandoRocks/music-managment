namespace :transient do
  task seed_performing_rights_organizations: :environment do
    tablename = "performing_rights_organizations"
    filename  = Rails.root.join("db/seed/csv/performing_rights_organizations.csv")
    klass     = tablename.singularize.classify.constantize
    headers   = klass.attribute_names
    body      = CSV.read(filename)
    klass.delete_all
    klass.import(headers, body, validate: false)
  end
end
