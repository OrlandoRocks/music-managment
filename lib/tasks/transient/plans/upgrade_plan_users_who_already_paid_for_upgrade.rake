# frozen_string_literal: true
namespace :transient do
    namespace :plans do
        task upgrade_plan_users_who_already_paid_for_upgrade: :environment do
            Rails.logger.info("⏲ Beginning rake task... ⏲")
            ORDERED_PLANS = %w[
                new_artist
                rising_artist
                breakout_artist
                professional
            ]

            log = Tempfile.new

            begin
                (2..4).reverse_each do |plan_num|
                    success_counter = 0
                
                    plan_name = ORDERED_PLANS[plan_num - 1]

                    unfulfilled_purchases = Purchase
                                        .joins(
                                            :product,
                                            person: :person_plan
                                        )
                                        .where(
                                            status: 'processed',
                                            products:{display_name: plan_name },
                                            person: {person_plans: {plan_id: 1...plan_num} }
                                        )
                                        .group(:person_id)
        

        
                    log.write("Upgrading users with the following ids to #{plan_name} Plan:\n") if unfulfilled_purchases.length > 0
                    unfulfilled_purchases.each do |purchase|
                        PersonPlanCreationService.call(
                            person_id: purchase.person_id,
                            plan_id: plan_num,
                            purchase_id: purchase.id
                        )
                        log.write("#{purchase.person_id}\n")
                        success_counter += 1
                    end

                    log.write("Successfully changed plan_id for #{success_counter} out of #{unfulfilled_purchases.length} person_plan records to #{plan_name} Plan\n\n")
                end

            rescue => e
                Rails.logger.error(e)
                log.write("#{e}\n")
                log.write("#{e.backtrace}\n")
            ensure
                log.close
                S3LogService.upload_only(
                    {
                        bucket_name: "rake-data",
                        bucket_path: ENV.fetch("RAILS_ENV").to_s,
                        file_name: "upgrade_plan_users_who_already_paid_for_upgrade_#{Time.now.iso8601}.log",
                        data: log
                    }
                )
                log.unlink
            end

            Rails.logger.info("✅ Rake task completed! ✅")
        end
    end
end
