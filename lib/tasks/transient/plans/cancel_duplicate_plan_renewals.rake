namespace :transient do

    namespace :plans do
        task cancel_duplicate_plan_renewals: :environment do 
            Rails.logger.info("⏲ Beginning rake task to cancel duplicated plan renewals... ⏲")

            Renewal.duplicate_active_plan_renewals.find_in_batches do |renewal_batch|
                person_ids_with_multiple_active_plan_renewals = renewal_batch.pluck(:person_id)

                duplicate_plan_renewal_ids = Renewal
                    .plans
                    .canceled(false)
                    .where(person_id: person_ids_with_multiple_active_plan_renewals)
                    .order(id: :asc)
                    .pluck(:person_id, :id)
                
                renewal_ids_to_cancel = []
                renewal_id_hash = {} # current most recent renewal_id hashed by person_id

                duplicate_plan_renewal_ids.each do |(person_id, renewal_id)|
                    renewal_ids_to_cancel << renewal_id_hash[person_id] if renewal_id_hash[person_id]
                    renewal_id_hash[person_id] = renewal_id
                end

                renewals_to_cancel = Renewal.where(id:renewal_ids_to_cancel)

                Rails.logger.info("Canceling #{renewals_to_cancel.length} out of #{duplicate_plan_renewal_ids.length} renewal records")
                
                begin
                    renewals_to_cancel.update_all(canceled_at:Time.now)
                rescue => e
                    Rails.logger.error(e)
                end

            end
        
            Rails.logger.info("✅ Rake task completed! ✅")
        end
    end
end
    