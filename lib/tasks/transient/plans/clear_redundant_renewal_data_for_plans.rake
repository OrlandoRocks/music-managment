namespace :transient do
    namespace :plans do
      task clear_rendundant_renewal_data_for_plans: :environment do 
  
        Rails.logger.info("⏲ Beginning rake task to clear renewal data with duplicate purchase ids... ⏲")
  
        Plans::ClearRedundantRenewalDataService.call
  
        Rails.logger.info("✅ Rake task completed! ✅")
      end
    end
end
