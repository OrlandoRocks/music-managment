namespace :transient do
  task add_low_risk_accounts: :environment do
    ids = File.readlines(File.join(Rails.root, "config", "low_risk_accounts.txt")).map(&:chomp)
    admin = Person.find_by(email: ENV["ADMIN_EMAIL"] || "corey@tunecore.com")
    low_risk_accounts = []
    ids.each do |id|
      low_risk_accounts << LowRiskAccount.new(person_id: id, added_by_admin_id: admin.id)
    end

    LowRiskAccount.import low_risk_accounts
  end
end
