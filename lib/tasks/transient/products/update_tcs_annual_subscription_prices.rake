namespace :transient do
  namespace :products do

    desc 'update prices for annual TCS subscriptions (before TCS 2.0 launch)'
    task :update_tcs_annual_subscription_prices => :environment do
      begin
        annual_US = Product.find(403)
        annual_CA = Product.find(405)
        annual_UK = Product.find(407)
        annual_AU = Product.find(409)

        annual_US.update_attribute('price', 83.99)
        annual_CA.update_attribute('price', 119.99)
        annual_UK.update_attribute('price', 71.99)
        annual_AU.update_attribute('price', 107.99)

        puts "TCS Annual Subscription product prices updated succesfully"
      rescue => e
        puts "Error: #{e}"
      end
    end
  end
end
