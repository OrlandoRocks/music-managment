namespace :transient do
  namespace :products do
    desc 'creates products for facebook products'
    task :create_facebook_freemium_products => :environment do
      CountryWebsite.all.each do |cw|
        # Album product logic

        ActiveRecord::Base.transaction do
          product = Product.find_or_create_by!(
            name: "Facebook Freemium - Album",
            display_name: "facebook_freemium_album",
            status: "Active",
            price: 0.00,
            currency: cw.currency,
            product_family: "Distribution",
            country_website_id: cw.id,
            created_by: Person.find_by(email: 'eric.whitebloom@tunecore.com') || Person.first
          )

          monthly_album_renewal_product = Product.find_by(name: "Monthly Album Renewal")

          item = ProductItem.find_or_create_by!(
                                    product_id: product.id,
                                    base_item_id: 1,
                                    name: "Facebook Freemium - Album",
                                    description: "Distribution of a Facebook Freemium Album",
                                    price: 0.0,
                                    currency: cw.currency,
                                    does_not_renew: 0,
                                    renewal_type: 'distribution',
                                    first_renewal_duration: 127,
                                    first_renewal_interval: 'month',
                                    renewal_duration: 1,
                                    renewal_interval: 'year',
                                    renewal_product_id: monthly_album_renewal_product.id,
          )

          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 102,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Album',
                                 quantity: 1,
                                 unlimited: 0,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
                                 )

          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 102,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Song',
                                 method_to_check: 'songs.count',
                                 model_to_check: 'Album',
                                 quantity: 0,
                                 unlimited: 1,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
          )

          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 102,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Salepoint',
                                 quantity: 1,
                                 unlimited: 0,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
          )

          cw.products << product
        end

        # Single product logic.

        monthly_single_renewal_product = Product.find_by(name: "Monthly Single Renewal")

        ActiveRecord::Base.transaction do
          product = Product.find_or_create_by!(
            name: "Facebook Freemium - Single",
            display_name: "facebook_freemium_single",
            status: "Active",
            price: 0.00,
            currency: cw.currency,
            product_family: "Distribution",
            country_website_id: cw.id,
            created_by: Person.find_by(email: 'eric.whitebloom@tunecore.com') || Person.first
          )

          item = ProductItem.find_or_create_by!(
                                    product_id: product.id,
                                    base_item_id: 1,
                                    name: "Facebook Freemium - Single",
                                    description: "Distribution of a Facebook Freemium Single",
                                    price: 0.0,
                                    currency: cw.currency,
                                    does_not_renew: 0,
                                    renewal_type: 'distribution',
                                    first_renewal_duration: 127,
                                    first_renewal_interval: 'month',
                                    renewal_duration: 1,
                                    renewal_interval: 'year',
                                    renewal_product_id: monthly_single_renewal_product.id,
          )

          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 122,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Single',
                                 quantity: 1,
                                 unlimited: 0,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
          )


          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 122,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Song',
                                 quantity: 1,
                                 unlimited: 0,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
          )

          ProductItemRule.find_or_create_by!(
                                 product_item_id: item.id,
                                 parent_id: 122,
                                 rule_type: "inventory",
                                 rule: "price_for_each",
                                 inventory_type: 'Salepoint',
                                 quantity: 1,
                                 unlimited: 0,
                                 price: 0,
                                 currency: cw.currency,
                                 is_active: true
          )

          cw.products << product
        end
      end

      Rails.logger.info("Finished adding products.")
    end
  end
end
