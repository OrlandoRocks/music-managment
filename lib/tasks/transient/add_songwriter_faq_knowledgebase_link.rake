namespace :transient do
  task add_songwriter_faq_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "360001315143", article_slug: "songwriter-song-roles-info", route_segment: "/articles/").first_or_create
  end
end
