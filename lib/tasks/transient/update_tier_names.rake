# frozen_string_literal: true

namespace :transient do
  desc "Update Tier Names to match Plans"
  task update_tier_names: :environment do
    Rails.logger.info "Starting Task"
    old_breakout = Tier.find(4)
    old_rising = Tier.find(3)
    old_emerging = Tier.find(2)

    old_breakout.update!(name: "PROFESSIONAL ARTIST", slug: "professional_artist")
    old_rising.update!(name: "BREAKOUT ARTIST", slug: "breakout_artist")
    old_emerging.update!(name: "RISING ARTIST", slug: "rising_artist")
    Rails.logger.info "Complete"
  end
end
