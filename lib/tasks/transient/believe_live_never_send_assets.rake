namespace :transient do
  task believe_live_never_send_assets: :environment do
    believe_live = StoreDeliveryConfig.where(store_id: 75).first

    believe_live.update(
      image_size: 0,
      skip_media: true,
      deliver_booklet: false,
      use_resource_dir: false
    )
  end
end