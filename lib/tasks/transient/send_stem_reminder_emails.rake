namespace :transient do
  task send_stem_reminder_emails: :environment do

    new_tc_users = []
    existing_tc_users = []

    CSV.foreach("./stem_csv.csv", headers: true) do |row|
      break if row.blank?

      person = Person.find(row["person_id"])

      if person.referral_campaign == "stem"
        new_tc_users << row["person_id"]
      elsif person.referral_campaign == "tc-stem"
        existing_tc_users << row["person_id"]
      end
    end



    new_tc_users.each do |person|
      MailerWorker.perform_async("StemMailer", :reminder_email_for_new_tc_users, person.id)
    end


    existing_tc_users.each do |person|
      MailerWorker.perform_async("StemMailer", :reminder_email_for_tc_stem_users, person.id)
    end
  end
end
