namespace :transient do
  desc "Update wrong artist roles - from with to featuring"
  task backfill_wrong_artist_role_with_to_featuring: :environment do
    Creative.where(role: 'with').update_all(role: 'featuring')
  end
end
