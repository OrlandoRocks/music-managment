namespace :transient do
  task :verify_spotify_album_uris => :environment do
    spot = SpotifyService.new
    existing = Album.where(id: ExternalServiceId.by_type("Album").spotify.pluck(:linkable_id))
    spot.search_by_upc(existing)
  end
end