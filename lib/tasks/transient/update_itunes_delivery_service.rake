namespace :transient do
  task update_itunes_delivery_service: :environment do
    Store.where(id: Store::ITUNES_STORE_IDS).each do |store|
      store.update(delivery_service: "tunecore")
    end
  end
end
