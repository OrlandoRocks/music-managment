namespace :transient do
  task tax_blocking_summarized_earnings_backfill: :environment do |_t, args|
    TaxBlocking::BatchPersonEarningsSummarizationWorker.perform_async(
      years: (DateTime.current.ago(3.years).year..DateTime.current.year).to_a
    )
  end
end
