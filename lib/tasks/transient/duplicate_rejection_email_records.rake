namespace :transient do
  task duplicate_rejection_email_records: :environment do
    original_rejection_emails = RejectionEmail.where(country_website_id: CountryWebsite::UNITED_STATES)

    original_rejection_emails.each do |original_rejection_email|
      attributes = original_rejection_email.attributes
        .except("id", "country_website_id")
        .merge(country_website_id: CountryWebsite::INDIA)

      RejectionEmail.find_or_create_by(attributes)
    end
  end
end
