namespace :transient do
  task instagram_final_setup: :environment do
    ig = Store.find_by(short_name: "Instagram")

    ig.update(in_use_flag: true)

    ig.salepointable_stores.create!(salepointable_type: 'Album')
    ig.salepointable_stores.create!(salepointable_type: 'Single')

    sdc = StoreDeliveryConfig.readonly(false).for("Instagram")

    sdc.update(
      remote_dir: "Tunecore/audio_library",
      image_size: 1600)
  end
end
