namespace :transient do
  task :update_payment_channel => :environment do
    SubscriptionPurchase.update_all(payment_channel: "Tunecore")
  end
end
