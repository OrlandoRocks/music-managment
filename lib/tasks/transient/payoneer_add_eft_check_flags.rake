namespace :transient do
  desc "Adds payoneer_show_eft_and_check flag"
  task payoneer_add_eft_check_flags: :environment do
    us_country_website = CountryWebsite.find_by(id: CountryWebsite::UNITED_STATES)
    FeatureFlipper.add_feature('payoneer_show_eft_and_check')
    FeatureFlipper.add_feature('payoneer_show_eft_and_check', us_country_website)
    FeatureFlipper.update_feature(:payoneer_show_eft_and_check, 100, [], us_country_website)
  end
end
