namespace :transient do
  namespace :process_blocked_invoices do

    desc "update_gst_config"
    task update_gst_config: :environment do
      last_invoice_id          = ENV.fetch('LAST_SUCCESSFUL_INVOICE_ID', '8798868').to_i
      blocked_indian_invoices  = Invoice.joins(:person).where(
        id: last_invoice_id.next..,
        people: {
          country_website_id: CountryWebsite::INDIA
        }
      )
      cgst_config                = GstConfig.find_by(cgst: 9.0, sgst: 9.0)
      igst_config                = GstConfig.find_by(igst: 18.0)
      blocked_invoices_with_cgst = blocked_indian_invoices.where(
        gst_config_id: cgst_config.id
      )
      processed_invoice_count = blocked_invoices_with_cgst.count
      blocked_invoices_with_cgst.update_all(gst_config_id: igst_config.id)
      Rails.logger.info("UPDATE GST CONFIG: GST CONFIGS UPDATED SUCCESSFULLY! #{processed_invoice_count} INVOICES AFFECTED")

      Rails.logger.info("DISABLING INVOICE BLOCKING")
      india_country_website = CountryWebsite.find_by(id: CountryWebsite::INDIA)
      FeatureFlipper.update_feature('block_indian_invoices', 0, nil, india_country_website)
      Rails.logger.info("INVOICE BLOCKING DISABLED")
    end

    desc "trigger_emails"
    task trigger_blocked_email: :environment do
      last_invoice_id          = ENV.fetch('LAST_SUCCESSFUL_INVOICE_ID', '8798868').to_i
      blocked_indian_invoices  = Invoice.joins(:person).where(
        id: last_invoice_id.next..,
        people: {
          country_website_id: CountryWebsite::INDIA
        }
      )

      Rails.logger.info("TRIGGER EMAILS: TRIGGERING EMAILS FOR #{blocked_indian_invoices.count} BLOCKED INVOICES")
      blocked_indian_invoices.map do |blocked_invoice|
        PersonNotifier.payment_thank_you(blocked_invoice.person, blocked_invoice).deliver
      end

      Rails.logger.info("EMAILS SENT FOR BLOCKED INVOICES!")
    end

    desc "upload invoices to payu"
    task upload_invoices_to_payu: :environment do
      last_invoice_id          = ENV.fetch('LAST_SUCCESSFUL_INVOICE_ID', '8798868').to_i
      blocked_indian_invoices  = Invoice.joins(:person, :invoice_settlements).where(
        id: last_invoice_id.next..,
        people: {
          country_website_id: CountryWebsite::INDIA
        },
        invoice_settlements: { source_type: 'PaymentsOSTransaction' }
      )
      Rails.logger.info("UPLOADING #{blocked_indian_invoices.count} BLOCKED INVOICES TO PAYU")
      blocked_indian_invoices.map do |blocked_invoice|
        response = Payu::InvoiceUploadService.new(blocked_invoice).upload
        Rails.logger.info("PAYU INVOICE UPLOAD (invoice_id: #{blocked_invoice.id}): #{response.body}")
      end
    end
  end
end
