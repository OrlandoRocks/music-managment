namespace :transient do
  task deezer_unified_petri_upgrade: :environment do
    store = Store.find_by(abbrev: "dz")
    store.update(delivery_service: "tunecore")

    store_delivery_config = StoreDeliveryConfig.where(store_id: store.id).first
    store_delivery_config.update(
      use_resource_dir: true
    )
  end
end
