namespace :transient do
  desc "Backfill the adyen merchant config to adyen transaction"
  task backfill_adyen_merchant_config: [:environment] do
    AdyenTransaction.find_each do |record|
      country = record.country
      corporate_entity = country.corporate_entity
      adyen_merchant_config = AdyenMerchantConfig.find_by(corporate_entity: corporate_entity)
      record.update(adyen_merchant_config: adyen_merchant_config)
    end
  end
end
