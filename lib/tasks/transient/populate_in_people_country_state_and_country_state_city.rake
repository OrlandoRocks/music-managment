namespace :transient do
  task populate_in_people_country_state_and_country_state_city: :environment do
    ActiveRecord::Base.transaction do
      query_params = {
        country_website_id: CountryWebsite::INDIA,
        country_state_id: nil,
        country: Country::INDIA_ISO
      }
      in_users = Person.where(query_params).in_batches do |batch|
        batch.each do |person|
          next if person.state.nil?

          country_state = CountryState.find_by_name(person.state)
          next if country_state.nil?

          country_state_city = CountryStateCity.find_by(name: person.city, state_id: country_state.id)

          person.update(
            country_state: country_state,
            country_state_city: country_state_city,
            )

        end
      end
    end
  end
end
