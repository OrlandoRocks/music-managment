namespace :transient do
  task add_active_provider_to_payout_providers: :environment do
    active_statuses = ["approved", "onboarding", "pending"].map(&:freeze).freeze

    inactive_providers = PayoutProvider.where(provider_status: "declined")
    inactive_providers.each { |provider| provider.update(active_provider: false) }

    active_providers = PayoutProvider.where(provider_status: active_statuses)
    active_providers.each { |provider| provider.update(active_provider: true) }
  end
end
