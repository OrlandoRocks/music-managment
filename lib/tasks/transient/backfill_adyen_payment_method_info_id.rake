namespace :transient do
  desc "Backfill data from old payment_method_info_id  to new adyen_payment_method_info_id"
  task backfill_adyen_payment_method_info_id: [:environment] do
    AdyenTransaction.where(adyen_payment_method_info_id: nil).find_each do |record|
      record.update!(adyen_payment_method_info_id: record.payment_method_info_id)
    end
  end
end
