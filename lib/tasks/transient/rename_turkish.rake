namespace :transient do
  task rename_turkish: :environment do
    turkish_language = CountryWebsiteLanguage.find_by(yml_languages: "tr-us")
    turkish_language.update(selector_language: "Türkçe")
  end
end
