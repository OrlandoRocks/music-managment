namespace :transient do
  task joox_myanmar_backfill: :environment do
    raise "Set a FILE_PATH env var" unless ENV['FILE_PATH']

    file_path     = ENV['FILE_PATH']
    limit         = 7500
    key           = "joox_myanmar_backfill"
    completed     = $redis.get(key).blank? ? 0 : $redis.get(key).to_i
    distributions = File.readlines(file_path.to_s)[completed.to_i, limit.to_i].map(&:chomp)

    Distribution.where(id: distributions).find_each do |d|
      Delivery::DistributionWorker
        .set(queue: "delivery-critical")
        .perform_async(d.class.name, d.id, 'metadata_only')
    end
    $redis.set(key, completed + distributions.size)
  end
end
