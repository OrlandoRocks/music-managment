namespace :transient do
  namespace :lyric do
    task add_missing_advances: :environment do
      person_id = ENV.fetch('PERSON_ID', 1164855)
      person = Person.find_by(id: person_id)
      return if person.blank?

      back_date = '2018-01-02'

      # Following data provided by finance, since this is a special one-off
      # case, hard coding the data feels appropriate
      advances = [{
        reference_id: "fU7ig3zmM - #{SecureRandom.uuid}",
        reference_source: 'LYRIC',
        fee_amount: 193.0,
        total_amount: 3193.0,
        created_at: back_date,
        updated_at: back_date
      }, {
        reference_id: "yKcUBibMH - #{SecureRandom.uuid}",
        reference_source: 'LYRIC',
        fee_amount: 109.0,
        total_amount: 1409.0,
        created_at: back_date,
        updated_at: back_date
      }, {
        reference_id: "v1M0a99lG - #{SecureRandom.uuid}",
        reference_source: 'LYRIC',
        fee_amount: 34.0,
        total_amount: 334.0,
        created_at: back_date,
        updated_at: back_date
      }, {
        reference_id: "OxSddEHXG - #{SecureRandom.uuid}",
        reference_source: 'LYRIC',
        fee_amount: 55.0,
        total_amount: 563.0,
        created_at: back_date,
        updated_at: back_date
      }]
      advances.each do |edata|
        encumbrance_summary = EncumbranceSummary.new(edata)
        encumbrance_summary.outstanding_amount = edata[:total_amount]
        person.encumbrance_summaries << encumbrance_summary
        person.save
      end

      # NOTE: outstanding_amount cannot go negative, hence deducting the
      # recoupment amount of 2551.99 from the first encumbrance_summary with
      # reference id `fU7ig3zmM`, having outstanding_amount of 3193.0
      recoup_amount = -2551.99
      es = person.encumbrance_summaries.where('reference_id like ?', '%fU7ig3zmM%').first
      EncumbranceDetail.create!(
        encumbrance_summary: es,
        transaction_date: back_date,
        transaction_amount: recoup_amount
      )
      es.update_column(:outstanding_amount, es.outstanding_amount += recoup_amount)
    end
  end
end
