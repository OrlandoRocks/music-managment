namespace :transient do 
    task spotify_time_release_fix_gs_11286: :environment do 
        albums = Album.where("
            finalized_at > '2021-03-24'
            AND HOUR(golive_date) <> 23
            AND MINUTE(golive_date) = 0
            AND golive_date > ?
            ", DateTime.now)
        Rails.logger.info "Found #{albums.count} albums to update golive date"

        albums.each do |album|
            album.update(golive_date: album.golive_date.change(hour: 23, min: 0))
        end 
    end 
end 