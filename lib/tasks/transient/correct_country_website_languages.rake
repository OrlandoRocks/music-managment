namespace :transient do
  task correct_english_canadian: :environment do
    country_website_language = CountryWebsiteLanguage.find_by(
      selector_language: "English",
      selector_country: "Canada",
    )

    country_website_language.update!(
      country_website_id: 2,
      yml_languages: "en-ca"
    )
  end
end
