namespace :transient do
  desc "template to dynamically create a flag based on parameters passed in"
  task :add_flag_template_with_category_name, [:category, :name] => [:environment] do |_, args|
    raise "**************************** \n Category or Name parameter missing! \n ****************************" unless args[:category] && args[:name]
    Flag.create!(category: args[:category], name: args[:name])
    puts "Flag created with category='#{args[:category]}' and name='#{args[:name]}'"
  end
end

