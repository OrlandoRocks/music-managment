namespace :transient do
  desc "Replace wide image, name and description to include Resso, keep all other references TikTok only."
  task add_resso_to_tiktok: :environment do
    FileUtils.mv("#{Rails.root}/public/images/store_logos/resso.png",
                 "#{Rails.root}/public/images/store_logos/byda_wide_color.png")
    store = Store.find_by(abbrev: 'byda')
    store.name = 'TikTok/Resso'
    store.description = "TikTok is the leading destination for short-form mobile video. Our mission is to inspire and enrich people’s lives by offering a home for creative expression and an experience that is genuine, joyful, and positive. TikTok has global offices including Los Angeles, New York, London, Paris, Berlin, Dubai, Mumbai, Singapore, Jakarta, Seoul, and Tokyo.<br/><br/>Resso is ByteDance’s social music streaming platform, allowing users to stream, save and add artists' songs to playlists or share across social media channels. Artists can build profiles, create social media posts using their song lyrics, promote songs, and interact with users directly.<br/><br/><strong>Strength:</strong> Reach - users can discover new artists and interact with them from all over the world."
    store.save
  end
end
