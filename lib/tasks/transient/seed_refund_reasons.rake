namespace :transient do
  task seed_refund_reasons: :environment do
    file = Rails.root.join("db/seed/csv/refund_reasons.csv")
    Seed.seed(file)
  end
end
