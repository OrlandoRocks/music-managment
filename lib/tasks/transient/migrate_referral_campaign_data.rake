namespace :transient do
  task migrate_referral_campaign_data: :environment do
    sql_query = "SELECT p.id, p.referral, p.referral_type, p.referral_campaign
                FROM people AS p
                  LEFT OUTER JOIN referral_data_tmp AS rdt ON p.id = rdt.person_id
                WHERE rdt.id IS NULL
                  AND (IFNULL(p.referral, '') != ''
                      OR IFNULL(p.referral_type, '') != ''
                      OR IFNULL(p.referral_campaign, '') != '')"

    limit = ENV["ROW_COUNT"] ? "LIMIT #{ENV["ROW_COUNT"]}" : ""

    people = ActiveRecord::Base.connection.exec_query(sql_query.concat(limit))

    people.each do |p|
      if tmp_table_insert(p)
        person = Person.find(p["id"])
        person.update(referral: nil, referral_type: nil, referral_campaign: nil)
      end
    end

  end
end

def tmp_table_insert(person)
  referral          = person["referral"].presence
  referral_type     = person["referral_type"].presence
  referral_campaign = person["referral_campaign"].presence
  dt                = DateTime.now

  begin
    # INSERT INTO referral_data_tmp(person_id, referral, referral_type, referral_campaign, created_at, updated_at)
    # VALUES(person["id"], referral, referral_type, referral_campaign, dt, dt)

    insert_statement = "INSERT INTO referral_data_tmp(person_id"
    insert_statement.concat(", referral") if referral
    insert_statement.concat(", referral_type") if referral_type
    insert_statement.concat(", referral_campaign") if referral_campaign
    insert_statement.concat(", created_at, updated_at) VALUES (#{person["id"]}")
    insert_statement.concat(", '#{referral}'") if referral
    insert_statement.concat(", '#{referral_type}'") if referral_type
    insert_statement.concat(", '#{referral_campaign}'") if referral_campaign
    insert_statement.concat(", '#{dt}', '#{dt}')")

    ActiveRecord::Base.connection.execute(insert_statement)
    true
  rescue
    puts "Insert into referral_data_tmp for person_id #{person["id"]} failed"
  end
end
