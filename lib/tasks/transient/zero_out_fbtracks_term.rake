namespace :transient do
  task zero_out_fbtracks_term: :environment do
    SubscriptionProduct.where(product_name: "FBTracks").update_all(term_length: 0)
  end
end
