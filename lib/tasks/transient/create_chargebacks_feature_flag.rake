namespace :transient do
  desc "Create Feature Flag for Chargebacks"
  task create_chargebacks_feature_flag: :environment do
    FeatureFlipper.add_feature("chargebacks")
    puts "Created 'chargebacks' Feature Flag"
  end
end

