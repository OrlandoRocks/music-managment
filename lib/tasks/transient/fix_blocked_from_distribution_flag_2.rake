namespace :transient do
  desc "add underscore to blocked_from_distribution flag name"
  task fix_blocked_from_distribution_flag_2: :environment do
    flag = Flag.find_by(name: "blocked from distribution", category: "distribution")
    flag.name = "blocked_from_distribution"
    flag.save!
    puts "blocked_from_distribution flag updated"
  end
end
