namespace :transient do
  desc "Update hostname for tidal"
  task update_tidal_host: :environment do
    config = StoreDeliveryConfig.for("Wimp")
    config.update(hostname: "sftp.tidal.com")
  end
end
