namespace :transient do
  task migrate_tax_tokens_to_tax_forms: :environment do
    TaxToken.where("tax_form_type is not null").each do |token|
      form = token.tax_form
      next if form.nil?
      TaxForm.create!(person_id: token.person_id,
                      form_type: token.tax_form,
                      provider: "payoneer",
                      submitted_at: token.updated_at,
                      expires_at: token.updated_at + 3.years)
    end
  end
end