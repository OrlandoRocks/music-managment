desc "Fix language codes into language codes table from a CSV"
namespace :fix do
  task language_codes: :environment do
    languages = [
      'Gujarati',
      'Maori',
      'Marathi'
    ]

    LanguageCode.where(description: languages).each do |language|
      description = language.description

      language.update(description: description.downcase)

      puts "Downcased #{description} => #{description.downcase}"
    end

    puts "Finished downcasing codes. You rock!"
  end
end
