namespace :transient do
  task :update_ytsr_mail_templates => :environment do
    { removed_monetization: {
        subject: "<%= @isrc %>: Removed from Monetization",
        header: "<p>Hello <%= @person_name %>,</p><p>We have been contacted by third parties stating that they have the rights to your content:</p>",
        body: '@message_data.map do |message|
  <<-HTML
  <p>We have been contacted by <strong>#{message[:custom_fields]["1"]["third_party"]}</strong> for: </p>
  <ul>
    <li>ISRC: #{message[:song].isrc}</li>
    <li>Title: #{message[:song].name}</li>
    <li>Artist: #{message[:song].artist_name}</li>
  </ul>
  HTML
end.join("")',
        custom_fields: ["third_party"],
        footer: "<p>For the assets listed above, the third parties assert that your version is using their copyrighted material. As such, we have removed the assets from YouTube monetization.</p><p>As a reminder, by agreeing to the TuneCore Terms and Conditions, you have:</p><ul><li>(a) legally represented to TuneCore that you control or have obtained all rights required to exploit the Recordings (including the art/images you associate with such Recordings);</li><li>(b) agreed that we may, in our sole discretion, disable access to any master recordings or other materials with respect to which we receive a complaint; and</li><li>(c) agreed that we shall have the right to deduct from your account or charge your credit card a minimum of $300 to offset the costs of associated legal fees, if necessary, and also deduct any and all revenues from your account which are received in connection with Recordings if we believe, in our good faith discretion, such Recordings violate the TuneCore Terms and Conditions.</li></ul>"
    }, instrumental_rights: {
        subject: "<%= @isrc %>: Rights to Instrumental",
        header: "<p>Hello <%= @person_name %>,</p><p>We have been contacted by third parties stating that they own the rights to the original instrumentals for your content.</p>",
        body: '@message_data.map do |message|
  <<-HTML
  <p>We have been contacted by <strong>#{message[:custom_fields]["1"]["third_party"]}</strong> for your sound recording <strong>#{message[:song].name}</strong> (ISRC <strong>message[:song].isrc</strong>).
  HTML
end.join("")',
        custom_fields: ["third_party"],
        footer: "<p>Have you purchased an exclusive license to use these instrumentals? If so please send them in as an attachment. If you do not have the exclusive rights to these instrumentals then we cannot monetize this track on YouTube.</p><p>Please respond within 3 business days or we reserve the right to remove these tracks from monetization.</p><p>To see what is eligible and ineligible for monetization in the YouTube Sound Recording Revenue Collection service, please go <a href='https://support.tunecore.com/hc/en-us/articles/115006693068' target='_blank'>here</a>.</p>"
    }, disputing_a_claim: {
        subject: "Disputing a Claim on Your YouTube Channel",
        header: "<p>Hello <%= @person_name %>,</p><p>Following disputes in YouTube, we have reinstated our claim on the following videos:</p>",
        body: '@message_data.map do |message|
  <<-HTML
  <a href="#{message[:custom_fields]["1"]["link_to_video"]}"></a>
  HTML
end.join("")',
        custom_fields: ["link_to_video"],
        footer: "<p>You may get a notification on YouTube mentioning that TuneCore has <strong>reinstated</strong> a claim on your video. This notification is normal and is meant to tell you that TuneCore is collecting money on your behalf for this video. Please do not dispute that notification (in YouTube) again. If you dispute it, we will no longer be able to collect revenue for your sound recording in that particular video.</p><h3>About the service</h3><p>The YouTube Sound Recording Revenue Collection service in TuneCore is meant to collect revenues for your sound recordings on YouTube on your channel or anyone else's channel. This means that when you or someone else uploads a video that contains your sound recording, YouTube will issue a notification to the uploader that states that TuneCore claims that video and collects revenues for you on that video. Please do not dispute that notification (in YouTube) or it will halt the monetization of the video.</p><p>Let us know if you have any questions!</p>"
    }, ownership_conflict: {
        subject: "<%= @isrc %>: Youtube Ownership Conflict",
        header: "<p>Hello <%= @person_name %>,</p><p>We have been contacted by third parties stating that they have rights for your content.</p>",
        body: '@message_data.map do |message|
  <<-HTML
  <p>We have been contacted by <strong>#{message[:custom_fields]["1"]["third_party"]}</strong> for:</p>
  <ul>
    <li>ISRC: #{message[:song].isrc}</li>
    <li>Artist: #{message[:song].artist_name}</li>
    <li>Track: #{message[:song].name}</li>
  </ul>
  <p><strong>#{message[:custom_fields]["1"]["third_party"]}</strong> is asserting rights in the following territories: <strong>#{message[:custom_fields]["1"]["territories"]}</strong></p>
  HTML
end.join("")',
        custom_fields: ["third_party", "territories"],
        footer: "<p>Do you have the exclusive rights for these tracks? If you are asserting rights, please confirm that TuneCore should be monetizing these tracks on your behalf. When there is a conflict in YouTube, monetization stops until the issue is resolved, so it is very important that you only select tracks for YouTube monetization in TuneCore, that are not being administered by anyone else.</p><p>Please respond within 3 business days or we reserve the right to remove your tracks from monetization.</p><p>To see what is eligible and ineligible for monetization in the YouTube Sound Recording Revenue Collection service, please go <a href='https://support.tunecore.com/hc/en-us/articles/115006693068' target='_blank'>here</a>.</p>"
    }, authorization: {
        subject: "<%= @isrc %>: Authorization for Use in Video",
        header: "<p>Hello <%= @person_name %>,</p><p>We have been notified by video uploaders that you may have authorized the use of your recording in their video.</p>",
        body: '@message_data.map do |message|
  custom_fields = eval(message[:custom_fields]).map do |_, custom_field|
    <<-HTML
      <ul>
        <li>Video Uploader: <strong>#{custom_field["video_uploader"]}</strong></li>
        <li>Video Link: <a href="#{custom_field["link_to_video"]}">#{custom_field["link_to_video"]}</a></li>
      </ul>
    HTML
  end.join("")

  <<-HTML
  <p>Song Title: <strong>#{message[:song].name}</strong> with ISRC: <strong>#{message[:song].isrc}</strong></p>
  #{custom_fields}
  HTML
end.join("")',
        custom_fields: ["video_uploader", "link_to_video"],
        footer: "<p>If you have authorized these usages, please confirm and we will release our claims. Please note that if we release our claims, this means that you will no longer earn revenue for your sound recording in these particular videos. Any generated revenue would then go to the video uploader.</p><p><a href='http://www.tunecore.com/index/youtube_help' target='_blank'></a></p><p>Please respond within 3 business days so we can resolve this issue.</p>"
    }, disputed_claim: {
        subject: "<%= @isrc %>: Appealed Claim",
        header: "<p>Hello <%= @person_name %>,</p><p>We encountered a problem with the monetization on YouTube of your sound recordings:</p>",
        body: '@message_data.map do |message|
  <<-HTML
    <p>For sound recording <strong>#{message[:song].name}</strong> by me with the ISRC <strong>#{message[:song].isrc}</strong>, <strong>#{message[:custom_fields]["1"]["plaintiff"]}</strong>
appealed TuneCore\'s claim of your recording that is used in the following video: <a href="#{message[:custom_fields]["1"]["link_to_video"]}"></a>.</p>
    <p>The reason invoked is: <strong>#{message[:custom_fields]["1"]["plaintiff_message"]}</strong>.
  HTML
end.join("")',
        custom_fields: ["plaintiff", "link_to_video", "plaintiff_message"],
        footer: "<p>As a result your sound recordings are no longer monetized on these videos.</p><p>If you do not agree with these uses of your recordings and wish to block them, you can file a DMCA complaint for each video. The process is described <a href='https://support.google.com/youtube/answer/2807622'>here</a>.</p><p>Please let us know if you need anything else.</p>"
    }, creative_commons_content: {
        subject: "<%= @isrc %>: Creative Commons",
        header: "<p>Hello <%= @person_name %>,</p><p>We have been contacted by third parties through YouTube stating that they accessed your music through Creative Commons.</p>",
        body: '@message_data.map do |message|
  <<-HTML
    <p>We have been contacted by <strong>#{message[:custom_fields]["1"]["third_party"]}</strong> for the sound recording:</p>
    <ul>
      <li>ISRC: #{message[:song].isrc}</li>
      <li>Artist: #{message[:song].artist_name}</li>
      <li>Track: #{message[:song].name}</li>
    </ul>
  HTML
end.join("")',
        custom_fields: ["third_party"],
        footer: "<p>Did you license these tracks (or any others) for Creative Commons? If so, please specify if you have granted others to monetize your sound recordings in their videos.</p><p>Please respond within 3 business days so we can resolve this issue.</p>"
    }, art_track: {
        subject: "<%= @isrc %>: Art Track",
        header: "<p>If you are asserting that you control or have obtained all rights required to exploit the Recordings as YouTube Art Tracks in the Territories, please reply in this email within ten (10) business days. Failure to meet this dealine will result in TuneCore permanently releasing any ownership in the corresponding YouTube Art Tracks.</p><p>If you have made this takedown request yourself via YouTube you may disregard this message. Please note that the Recordings were delivered to the YouTube Music Key Store (https://support.tunecore.com/hc/en-us/categories/115001079227), because (i) you have store automator enabled for this release, (ii) you selected this store when you distributed this release, or (iii) you paid to add this store to an exisiting release. The Art Tracks created from your sound recording for YouTube Music Key will be/have been removed. If you made this takedown request in error and would like these Art Tracks reinstated please let us know.</p>",
        body: '@message_data.map do |message|
  <<-HTML
  <ul>
    <li>Claimant: <strong>#{message[:custom_fields]["1"]["third_party_email"]}</strong></li>
    <li>Recordings: <strong>#{message[:custom_fields]["1"]["recording_name"]}</strong></li>
    <li>Territories: <strong>Worldwide</strong></li>
  </ul>
  HTML
end.join("")',
        custom_fields: ["third_party", "recording_name"],
        footer: "<p>Greeting</p><p>YouTube has notified us that they received a notice from a third party that the YouTube Art Tracks created from the following sound recordings (the 'Recordings') infringe upon the Claimant's rights in the following territories (the 'Territories'):</p>"
    } }.each do |template_name, attributes|
      ytsr_mail_template = YoutubeSrMailTemplate.find_by(template_name: template_name)
      ytsr_mail_template.update(attributes.merge(country_website_id: CountryWebsite::UNITED_STATES))
    end
  end
end
