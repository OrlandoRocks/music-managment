namespace :backfill_refunds do
  desc "backfill old refunds"
  task braintree: :environment do
    RefundBackfill::BatchWorker.perform_async("BraintreeTransaction")
    puts "Scheduled worker to backfill braintree transactions"
  end

  task payments_os: :environment do
    RefundBackfill::BatchWorker.perform_async("PaymentsOSTransaction")
    puts "Scheduled worker to backfill payments_os transactions"
  end
end
