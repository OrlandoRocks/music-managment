namespace :transient do
  task snake_case_review_reason_email_templates: :environment do
    Rails.logger.info "Updating Review Reasons"

    ReviewReason
      .find_by(reason: "Audio: Snippets, Defective or Duplicate Audio")
      .update(email_template_path: "denied_defective_audio")

    ReviewReason
      .find_by(reason: "Metadata: EP Error")
      .update(email_template_path: "denied_metadata_ep_error")

    ReviewReason
      .find_by(reason: "Metadata: Remove Remixer from Primary Artist Field")
      .update(email_template_path: "denied_remove_remixer")

    ReviewReason
      .find_by(reason: "Duplicate Audio: Inconsistent Metadata")
      .update(email_template_path: "denied_duplicate_audio")

    Rails.logger.info "🕺 Successfully updated Review Reasons 🕺"
  end

  task snake_case_rejection_email_name: :environment do
    Rails.logger.info "Updating Rejection Emails"

    RejectionEmail
      .find_by(name: "Audio: Snippets, Defective or Duplicate Audio")
      .update(name: "denied_defective_audio")

    RejectionEmail
      .find_by(name: "Metadata: EP Error")
      .update(name: "denied_metadata_ep_error")

    RejectionEmail
      .find_by(name: "Metadata: Remove Remixer from Primary Artist Field")
      .update(name: "denied_remove_remixer")

    RejectionEmail
      .find_by(name: "Duplicate Audio: Inconsistent Metadata")
      .update(name: "denied_duplicate_audio")

    Rails.logger.info "🕺 Successfully updated Rejection Emails 🕺"
  end
end
