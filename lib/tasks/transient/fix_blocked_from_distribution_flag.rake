namespace :transient do
  desc "update blocked from distribution flag to swap previously incorrect name and category"
  task fix_blocked_from_distribution_flag: :environment do
    flag = Flag.find_by(category: "blocked from distribution", name: "distribution")
    flag.name = "blocked from distribution"
    flag.category = "distribution"
    flag.save!
    puts "blocked from distribution flag updated"
  end
end


