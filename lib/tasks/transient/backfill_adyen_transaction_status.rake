namespace :transient do
  desc "Backfill the Adyen transaction status"
  task backfill_adyen_transaction_status: :environment do
    require "csv"
    csv = RakeDatafileService.csv("AdyenPaymentList.csv", headers: true, encoding: "utf-8")
    possible_failure_status = ["Error", "Refused"]
    csv.each do |row|
      transaction = AdyenTransaction.find_by! psp_reference: row["PSP Reference"]
      next unless transaction.status.nil?

      status = possible_failure_status.include?(row["Status"]) ? "failure" : "success"
      transaction.update!(status: status)
    rescue ActiveRecord::RecordNotFound
      Rails.logger.info "Couldn't find Adyen Transaction for PSP Reference = #{row['PSP Reference']}"
    end
  end
end
