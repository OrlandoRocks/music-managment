namespace :transient do
  task add_position_to_ttunes_and_pandora: :environment do
    Store.update(82, position: 155, is_active: 1) # touch tunes
    Store.update(78, position: 45) # pandora
    Store.update(64, position: 790) # SimfyAfrica
  end
end
