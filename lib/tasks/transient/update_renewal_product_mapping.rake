namespace :transient do
  task :update_renewal_product_mapping => :environment do
    renewal_product_mapping = {
      Product::FR_TWO_YEAR_ALBUM_PRODUCT_ID => 353, # 2 Year Album renewal
      Product::FR_FIVE_YEAR_ALBUM_PRODUCT_ID => 355, # 5 Year Album renewal
      Product::FR_TWO_YEAR_SINGLE_PRODUCT_ID => 354, # 2 Year Single renewal
      Product::FR_FIVE_YEAR_SINGLE_PRODUCT_ID => 356, # 5 Year Single renewal
      Product::IT_TWO_YEAR_ALBUM_PRODUCT_ID => 392, # 2 Year Album renewal
      Product::IT_FIVE_YEAR_ALBUM_PRODUCT_ID => 394, # 5 Year Album renewal
      Product::IT_TWO_YEAR_SINGLE_PRODUCT_ID => 393, # 2 Year Single renewal
      Product::IT_FIVE_YEAR_SINGLE_PRODUCT_ID => 395, # 5 Year Single renewal
      Product::US_FIVE_YEAR_ALBUM_PRODUCT_ID => 125, # 5 Year Album renewal
      Product::US_FIVE_YEAR_SINGLE_PRODUCT_ID => 127, # 5 Year Single renewal
      Product::UK_FIVE_YEAR_ALBUM_PRODUCT_ID => 186, # 5 Year Album renewal
      Product::UK_FIVE_YEAR_SINGLE_PRODUCT_ID => 187, # 5 Year Single renewal
      Product::CA_FIVE_YEAR_ALBUM_PRODUCT_ID => 126, # 5 Year Album renewal
      Product::CA_FIVE_YEAR_SINGLE_PRODUCT_ID => 128, # 5 Year Single renewal
      Product::AU_FIVE_YEAR_ALBUM_PRODUCT_ID => 228, # 5 Year Album renewal
      Product::AU_FIVE_YEAR_SINGLE_PRODUCT_ID => 229, # 5 Year Single renewal
      Product::DE_FIVE_YEAR_ALBUM_PRODUCT_ID => 315, # 5 Year Album renewal
      Product::DE_FIVE_YEAR_SINGLE_PRODUCT_ID => 316 # 5 Year Single renewal
    }

    renewal_product_mapping.each do |product_id, renewal_product_id|
      product = Product.find(product_id)
      product.product_items.first.update!(renewal_product_id: renewal_product_id)
    end
  end
end
