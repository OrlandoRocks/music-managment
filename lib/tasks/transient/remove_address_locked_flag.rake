namespace :transient do
  task remove_address_locked_flag: :environment do
    Flag.find_by(:name => "address_locked", :category => "person_location").destroy!
  end
end

