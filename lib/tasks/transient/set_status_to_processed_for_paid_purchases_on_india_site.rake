namespace :transient do
  task set_status_to_processed_for_paid_purchases_on_india_site: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    indian_paid_processing_purchases = Purchase
                                       .paid
                                       .joins(:person)
                                       .where(
                                         people: { country_website_id: CountryWebsite::INDIA },
                                         status: "processing"
                                       )

    purchase_status_change_log = Tempfile.new

    status_change_count = 0

    begin
      purchase_status_change_log.write("Changing Purchase Statuses for purchases with the following ids:\n")
      indian_paid_processing_purchases.in_batches do |purchases|   
        purchase_ids = purchases.pluck(:id)
        purchases.update_all(status: "processed")
        status_change_count += purchase_ids.count
        purchase_ids.each { |pid| purchase_status_change_log.write("#{pid}\n") }
      end
    rescue => e
      Rails.logger.error(e)
      purchase_status_change_log.write("#{e}\n")
      purchase_status_change_log.write("#{e.backtrace}\n")
    ensure
      purchase_status_change_log.write("Changed Status for #{status_change_count} purchased records from 'processing' to 'processed'")
      purchase_status_change_log.close
      S3LogService.upload_only(
        {
          bucket_name: "rake-data",
          bucket_path: ENV.fetch("RAILS_ENV").to_s,
          file_name: "set_status_to_processed_for_paid_purchases_on_india_site_#{Time.now.iso8601}.log",
          data: purchase_status_change_log
        }
      )
      purchase_status_change_log.unlink
    end

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
