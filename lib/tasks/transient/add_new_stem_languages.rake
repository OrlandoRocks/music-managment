namespace :transient do
  task add_new_stem_languages: :environment do
    languages = [
      { code: "gu", desc: "Gujarati" },
      { code: "mi", desc: "Maori" },
      { code: "mr", desc: "Marathi" }
    ]

    languages.each { |lang| LanguageCode.create(code: lang[:code], description: lang[:desc]) }
  end
end
