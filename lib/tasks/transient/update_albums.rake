namespace :transient do
  task update_is_dj_release_for_albums: :environment do
    Album.unscoped.where(is_dj_release: nil).in_batches do |collection|
      collection.update_all(is_dj_release: false)
    end

    Rails.logger.info("Finished setting defaults for dj release flag.")
  end
end
