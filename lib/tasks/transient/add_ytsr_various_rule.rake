namespace :transient do
  desc "adds various artists rule to YTSR ineligibility rules"
  task add_various_rule_to_ytsr: :environment do
    ytsr_id = Store.find_by(short_name: 'YoutubeSR').id
    IneligibilityRule.find_or_create_by!(
      operator: 'equal',
      property: 'is_various?',
      store_id: ytsr_id,
      value: 'true'
    )
    puts 'Successfully added new ineligibility rule.'
  end
end
