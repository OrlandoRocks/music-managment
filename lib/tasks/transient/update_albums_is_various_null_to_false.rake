namespace :transient do
  desc 'update albums is_various from nil to false'
  task :update_albums_is_various_null_to_false => :environment do
    Album.where(is_various: nil).update_all(is_various: false)
  end
end
