namespace :lod do
  task backfill_document_status: :environment do
    # Documents sent and have been signed are "Completed"
    LegalDocument.where(status: nil)
      .where('signed_at IS NOT NULL')
      .update_all(status: :completed)

    Rails.logger.info("Updated 'Completed' Envelopes")

    # Documents sent 120 days ago and haven't sigend are "Voided"
    # 120 days is the envelope expiration @Docusign.
    LegalDocument.where(status: nil)
      .where(signed_at: nil)
      .where('created_at < ?', 120.days.ago)
      .update_all(status: :voided)

    Rails.logger.info("Updated 'Voided' Envelopes")

    # Everything else is "Sent"
    LegalDocument.where(status: nil)
      .update_all(status: :sent)

    Rails.logger.info("Updated 'Sent' Envelopes")
  end
end
