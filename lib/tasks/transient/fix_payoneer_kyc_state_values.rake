namespace :transient do
  desc "All 00 Values in the state column of users (provided by payoneer) will be replaced with NULL in our DB"
  task fix_payoneer_kyc_state_values: :environment do
    Rails.logger.info "Updating all 00 state values for NON US users to NULL"
    people_with_invalid_state = Person.where(state: '00').where.not(country: 'US')
    people_with_invalid_state.update_all(state: nil)
  end
end
