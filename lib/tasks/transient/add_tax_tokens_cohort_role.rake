namespace :transient do
  task add_tax_tokens_cohort_role: :environment do
    #TODO put your task logic here
    Role.where(name: "Tax Tokens Cohort",
      long_name: "Tax Tokens Cohort Admin",
      description: "Gives admins the ability to handle tax token cohorts and upload users to them.",
      is_administrative: true).first_or_create
  end
end
