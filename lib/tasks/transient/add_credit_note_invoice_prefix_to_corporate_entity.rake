namespace :transient do
  desc "Add credit note invoice prefix values to corporate entity"
  task add_credit_note_invoice_prefix_values: :environment do
    CorporateEntity
      .find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
      .update!(credit_note_inbound_prefix: CorporateEntity::BI_CREDIT_NOTE_INVOICE_PREFIX)

    CorporateEntity
      .find_by(name: CorporateEntity::TUNECORE_US_NAME)
      .update!(credit_note_inbound_prefix: CorporateEntity::TC_CREDIT_NOTE_INVOICE_PREFIX)
  end
end
