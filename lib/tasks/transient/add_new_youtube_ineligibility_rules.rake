namespace :transient do
  task add_new_youtube_ineligibility_rules: :environment do
    store_id = Store::YTSR_STORE_ID

    new_rules = [
      {
        property: "primary_genre_name",
        operator: "contains",
        value: "Audiobooks",
        store_id: store_id
      },{
        property: "primary_genre_name",
        operator: "contains",
        value: "Comedy",
        store_id: store_id
      },{
        property: "primary_genre_name",
        operator: "contains",
        value: "Spoken",
        store_id: store_id
      },{
        property: "primary_genre_name",
        operator: "contains",
        value: "Fitness & Workout",
        store_id: store_id
      },{
        property: "primary_genre_name",
        operator: "contains",
        value: "Karaoke",
        store_id: store_id
      },{
        property: "secondary_genre_name",
        operator: "contains",
        value: "Audiobooks",
        store_id: store_id
      },{
        property: "secondary_genre_name",
        operator: "contains",
        value: "Comedy",
        store_id: store_id
      },{
        property: "secondary_genre_name",
        operator: "contains",
        value: "Spoken",
        store_id: store_id
      },{
        property: "secondary_genre_name",
        operator: "contains",
        value: "Fitness & Workout",
        store_id: store_id
      },{
        property: "secondary_genre_name",
        operator: "contains",
        value: "Karaoke",
        store_id: store_id
      },{
        property: "album_type",
        operator: "equal",
        value: "Ringtone",
        store_id: store_id
      }
    ]

    new_rules.each do |ruleset|
      IneligibilityRule.find_or_create_by(ruleset)
    end
  end

  task add_new_youtube_ineligibility_rules_from_seed_file: :environment do
    store_id = Store::YTSR_STORE_ID

    seed_file_path = ENV.fetch("SEED_FILE_PATH", "db/seed/csv/ineligibility_rules.csv")

    new_rule_values = CSV.read(seed_file_path)

    column_names = IneligibilityRule.column_names
    strictly_relevant_keys = [:property, :operator, :value, :store_id]

    rulesets = new_rule_values.map do |value_set|
      initial_rule_hash = column_names.zip(value_set).to_h.symbolize_keys

      initial_rule_hash.slice(*strictly_relevant_keys)
    end

    rulesets.each do |ruleset|
      IneligibilityRule.find_or_create_by(ruleset)
    end
  end

  task add_new_youtube_ineligibility_rules_two: :environment do
    rules_path = ENV.fetch("INELIGIBILITY_RULES_PATH")

    new_rule_values = CSV.read(rules_path)

    column_names = IneligibilityRule.column_names
    strictly_relevant_keys = [:property, :operator, :value, :store_id]

    rulesets = new_rule_values.map do |value_set|
      initial_rule_hash = column_names.zip(value_set).to_h.symbolize_keys

      initial_rule_hash.slice(*strictly_relevant_keys)
    end

    rulesets.each do |ruleset|
      IneligibilityRule.find_or_create_by(ruleset)
    end
  end

  task fix_bad_youtube_ineligibility_rules: :environment do
    rules = IneligibilityRule.where(property: 'sub_genre')

    rules.each do |rule|
      rule.update!(property: 'primary_genre_name')

      new_rule_secondary_genre = rule.dup
      new_rule_secondary_genre.property = 'secondary_genre_name'
      new_rule_secondary_genre.save!
    end
  end

  task backfill_albums_affected_by_bad_yt_rules: :environment do
    store = Store.find_by(id: Store::YTSR_STORE_ID)

    audits = ReviewAudit.joins(album: {person: :youtube_monetization})
                        .where("review_audits.created_at > '2020-09-21'")
                        .where(review_audits: {event: 'APPROVED'})
                        .select('distinct(review_audits.album_id)')

    audits.each do |audit_result|
      TrackMonetization::PostApprovalWorker.perform_async(audit_result.album_id, store.id)
    end
  end
end
