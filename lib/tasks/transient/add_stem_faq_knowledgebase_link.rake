namespace :transient do
  task add_stem_faq_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "360030991552", article_slug: "Stem-FAQ", route_segment: "/articles/").first_or_create
  end
end
