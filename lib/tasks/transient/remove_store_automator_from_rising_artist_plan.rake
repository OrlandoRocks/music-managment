namespace :transient do
    task remove_store_automator_from_rising_artist_plan: :environment do
      plans_plan_feature = PlansPlanFeature.find_by({:plan_id => 2, :plan_feature_id => 20})
      plans_plan_feature.destroy if !plans_plan_feature.nil?
    end
  end