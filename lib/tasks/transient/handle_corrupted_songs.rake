namespace :transient do
  desc "Handle corrputed songs due to failing foreign key constraint"
  task handle_not_reuploaded_corrupted_songs: :environment do
    Song.where('s3_asset_id = s3_flac_asset_id').
      where('s3_asset_id != s3_orig_asset_id').
      where.not(s3_asset_id: nil).
      where.not(s3_flac_asset_id: nil).
      where.not(s3_orig_asset_id: nil).each do |song|
        song.update!(s3_asset: song.s3_orig_asset) if song.s3_orig_asset.present? && song.s3_asset.present? && song.s3_asset.key.ends_with?(".flac")
      end
  end

  desc "Handle reuploaded corrputed songs where the s3_asset and s3_orig_asset got deleted"
  task handle_reuploaded_corrupted_songs: :environment do

    album_ids_to_reencode = []

    Song.joins(:album).
      where(s3_flac_asset_id: nil).
      where.not(s3_asset_id: nil).
      where.not(albums: {finalized_at: nil}).each do |song|
        next unless song.s3_asset.nil?

        s3_asset = S3Asset.create(
          key: "#{song.person.id}/#{song.upload.uploaded_filename}",
          bucket: 'bigbox.tunecore.com'
        )

        song.update!(
          {
            s3_asset: s3_asset,
            s3_orig_asset: s3_asset
          }
        )

        album_ids_to_reencode << song.album_id
    end

    # Re-encode the assets
    AssetsBackfill::Base.new(album_ids: album_ids_to_reencode.uniq).run

    p "List of Albums which need a Backfill"
    p album_ids_to_reencode
  end
end
