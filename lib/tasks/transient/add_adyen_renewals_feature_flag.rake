# frozen_string_literal: true

namespace :transient do
  desc "Create Feature Flag For Adyen integration"
  task add_adyen_renewals_feature_flag: :environment do
    FeatureFlipper.add_feature("adyen_renewals")
    Rails.logger.info("Added adyen_renewals feature flag")
  end
end
