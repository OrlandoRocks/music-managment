namespace :transient do
  task create_address_locked_flag: :environment do
    Flag.create!({:name => "address_locked", :category => "person_location"})
  end
end
