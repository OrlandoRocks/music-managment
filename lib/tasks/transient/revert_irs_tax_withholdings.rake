namespace :transient do
  namespace :revert_irs_tax_withholdings do
    desc "Create a compound entry for all wrongly charged IRSTaxWithholdings CF-619"
    task run: :environment do
      checksum = BalanceAdjustment.where(posted_by_id: 331961, created_at: DateTime.now.beginning_of_year..DateTime.now).count
      if checksum == 67_089
        PersonTransaction.where(target_type: "IRSTaxWithholding").pluck(:person_id).uniq.map do |person_id|
          TaxWithholdings::Transient::TaxWithholdingReversalService.new(person_id).revert!
        end
      else
        Rails.logger.info("EXPECTED CHECKSUM: #{67_089} BUT RECEIVED #{checksum}")
      end
    end
  end
end
