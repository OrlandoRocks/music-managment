namespace :transient do
  desc "kickoff sidekiq worker to backfill country_audits AND update person.country for Payoneer users & invalid country users"
  task person_country_backfill_all: :environment do
    PersonCountryBackfillWorker.perform_async(:backfill_all)
  end

  task person_country_backfill_audit: :environment do
    PersonCountryBackfillWorker.perform_async(:backfill_country_audit)
  end

  desc "Update people's payout provider and compliance info to their correct values"
  task :person_country_backfill_payoneer_kyc, [:s3_file_name] => :environment do |_t, args|
    person_ids = []

    if args[:s3_file_name].present?
      person_ids = CSV.read(RakeDatafileService.download(args[:s3_file_name]).local_file).flatten
    end

    PayoneerKycBackfill::WorkflowWorker.perform_async(person_ids)
  end

  task person_country_backfill_invalid: :environment do
    PersonCountryBackfillWorker.perform_async(:backfill_invalid)
  end
end
