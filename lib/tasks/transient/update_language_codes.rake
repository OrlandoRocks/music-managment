namespace :transient do
  task update_language_codes: :environment do
    CURRENT_ISO_639_2_codes = %w(bho doi kha kok mai mni lus sat)

    # If it's 3 letters, it's certainly in 639_3,
    # but only some are also in 639_2.
    LanguageCode.all.each do |lc|
      lc.ISO_639_1_code = lc.code if lc.code.length == 2
      lc.ISO_639_2_code = lc.code if CURRENT_ISO_639_2_codes.include?(lc.code)
      lc.ISO_639_3_code = lc.code if lc.code.length == 3
      lc.save
    end

    # special handling for IANA code mapping
    yue = LanguageCode.find_by(code: 'yue-Hant')
    yue.update(ISO_639_1_code: 'zh', ISO_639_2_code: 'zho', ISO_639_3_code: 'yue')

    hans = LanguageCode.find_by(code: 'cmn-Hans')
    hans.update(ISO_639_1_code: 'zh', ISO_639_2_code: 'zho', ISO_639_3_code: 'cmn')

    hant = LanguageCode.find_by(code: 'cmn-Hant')
    hant.update(ISO_639_1_code: 'zh', ISO_639_2_code: 'zho', ISO_639_3_code: 'cmn')
  end

  task update_albums_with_zh_iso_code: :environment do
    albums = Album.where(language_code: 'zh')
    albums.update(language_code: 'cmn-Hant')
  end
end
