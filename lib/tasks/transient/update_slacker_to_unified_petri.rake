namespace :transient do
  task update_slacker_to_unified_petri: :environment do
    store = Store.find_by(abbrev: "sk")
    store.update(delivery_service: "tunecore")

    store_delivery_config = StoreDeliveryConfig.where(store_id: store.id).first
    store_delivery_config.update(
      use_resource_dir: true
    )
  end
end
