namespace :transient do
  desc "Backfill Apple ESIDs to nil for non-primary creatives, and deliver"
  task nullify_apple_esids: :environment do
    creatives = Creative
                .joins(:external_service_ids)
                .where(external_service_ids: { service_name: 'apple', identifier: 'unavailable' })
                .where.not(role: 'primary_artist')
    esids = ExternalServiceId.by_creatives(creatives.map(&:id))

    puts "ESIDs: #{esids}"
    puts "Found #{esids.size} ESIDs to update."
    esids.update_all(identifier: nil, state: nil)

    puts "Updated ESIDs."

    album_ids = creatives.map(&:creativeable_id).uniq

    puts "Albums: #{album_ids}"
    puts "Found #{album_ids.size} associated albums."
    Distribution
      .joins(:salepoints)
      .where(distributions: { converter_class: 'MusicStores::Itunes::Converter' },
             salepoints: { salepointable_id: album_ids, salepointable_type: 'Album' })
      .each do |d|
        Delivery::DistributionWorker
          .set(queue: "delivery-critical")
          .perform_async(d.class.name, d.id, d.delivery_type)
      end

    puts "Enqueued associated distributions for Apple."
  end
end
