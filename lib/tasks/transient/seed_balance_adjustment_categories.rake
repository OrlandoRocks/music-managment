namespace :transient do
  task seed_balance_adjustment_categories: :environment do
    [
      "Refund - Renewal",
      "Refund - Other",
      "Service Adjustment",
      "Songwriter Royalty",
      "Other",
      "YouTube MCN Royalty",
      "Facebook",
      "Tidal DAP"
    ].each do |category_name|
      BalanceAdjustmentCategory.find_or_create_by(name: category_name)
    end
  end
end
