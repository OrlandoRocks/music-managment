namespace :transient do
  task :remove_included_from_album_countries => :environment do
    countries       = Country::SANCTIONED_COUNTRIES
    album_countries = AlbumCountry.select(AlbumCountry.arel_table[Arel.star]).joins(:country).where(Country.arel_table[:iso_code].in(countries))
    album_countries.destroy_all
  end
end
