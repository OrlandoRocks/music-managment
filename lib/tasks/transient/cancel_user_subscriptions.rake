namespace :transient do
  namespace :cancel_user_subscriptions do
    desc "cancels user subscriptions listed in gs-8031"
    task run: :environment do
      admin_account = Person.find_by(id: 381655) # sean@tunecore.com
      log_data = {}
      logfile = File.new("tmp/8039_subscription_cancelations.json", 'w+')
      filename = ENV.fetch('SUBSCRIPTION_USER_LIST', 'subscription_cancelation_accounts.csv')

      users_list = RakeDatafileService.csv(filename, headers: true, encoding: 'utf-8')
      users_list.each do |user_data|
        person = Person.find_by(
          id: user_data['person_id'],
          email: user_data['email']
        )
        if person.blank?
          log_data[user_data['email']] = { error: "Person Not Found" }
          next
        end
        active_subscriptions = person&.person_subscription_statuses.to_a.select(&:is_active?)
        active_subscriptions.each do |active_person_subscription|
          social_cancellation_form = SubscriptionCancellationForm.new(
            subscription: active_person_subscription, admin: admin_account
          )
          if (cancelation_status = social_cancellation_form.save)
            log_data[person.id] = log_data[person.id].to_h.merge(
              active_person_subscription.subscription_type => cancelation_status
            )
          else
            log_data[person.id] = log_data[person.id].to_h.merge(
              active_person_subscription.subscription_type => cancelation_status,
              "errors" => active_person_subscription.errors.full_messages.join(' & ')
            )
          end
        end

        next if person&.youtube_monetization.blank?

        note_params = {
          note_created_by: admin_account,
          note: "YTM Disabled for User. GS-8039"
        }
        ytm_terminated = person.terminate_ytm(note_params, "Tunecore Admin: #{admin_account.name}")
        log_data[person.id] = log_data[person.id].to_h.merge(
          "YTM" => ytm_terminated
        )
      end
      logfile.puts(JSON.pretty_generate(log: log_data))
      logfile.close
      RakeDatafileService.upload("tmp/8039_subscription_cancelations.json", overwrite: 'y')
    end
  end
end
