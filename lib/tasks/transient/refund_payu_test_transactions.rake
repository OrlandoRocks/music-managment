namespace :transient do
  task refund_payu_test_transactions: :environment do
    refundable_txn_data = [
      {
        payment_id: "64366dd3-2827-4a07-8aaa-5a9293f45d2f",
        charge_id: "928d322a-2471-45d5-915c-c01632055628",
        reconciliation_id: "j3HqxrohWRLxnrDqukrS4jEWZ",
        amount: {inr: 98570, usd: 13.47}
      },
      {
        payment_id: "ee446218-bce8-46b4-bf3f-8047280a95c0",
        charge_id: "977e65d8-d153-4425-ba1b-80761c43bef0",
        reconciliation_id: "kbWoOIp5bbUM5DXfsRIHCWUa6",
        amount: {inr: 147900, usd: 20.21}
      },
      {
        payment_id: "940b01e5-e42e-4012-9bdd-f92b1265e8a7",
        charge_id: "27a165f0-3f18-406b-83f8-12dfeeb293be",
        reconciliation_id: "87ttNITGbVGBK1xEikMvVWp07",
        amount: {inr: 246295, usd: 33.66}
      }
    ]

    refundable_txn_data.each do |refund_txn_info|
      execute_refund(refund_txn_info)
    end
  end

  def execute_refund(txn_data)
    payment = PaymentsOSTransaction.find_by(txn_data.slice(:payment_id, :charge_id, :reconciliation_id))
    if payment.present?
      refund_transaction = PaymentsOS::RefundService.new(payment).refund(
        txn_data.dig(:amount, :usd),
        "Other",
        "Production Test Transaction"
      )
      if refund_transaction.success?
        Rails.logger.info "Payment #{txn_data[:payment_id]} has been refunded successfully"
      else
        Rails.logger.fatal "Refunding #{txn_data[:payment_id]} Failed! \n REFUND RESPONSE: #{refund_transaction.raw_response}"
      end
    else
      Rails.logger.fatal "Transaction to refund not found: #{txn_data[:payment_id]}"
    end
  end
end
