namespace :transient do
  task create_publishing_roles: :environment do
    titles = ["manager", "publisher", "lawyer", "other", "self"]

    titles.each do |title|
      PublishingRole.create!(title: title)
    end
  end
end
