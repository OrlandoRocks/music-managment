# frozen_string_literal: true

namespace :transient do
  task remove_completed_auto_renewal_form_flag: :environment do
    flag = Flag.find_by(
      category: "vat_compliance",
      name: "completed_auto_renewal_form"
    )
    PeopleFlag.where(flag: flag).destroy_all
    flag.destroy
  end
end
