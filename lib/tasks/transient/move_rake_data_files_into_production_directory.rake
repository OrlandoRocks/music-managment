namespace :transient do
  desc "moves rake data files under rake-data bucket into production directory"
  task move_rake_data_files_into_production_directory: :environment do
    bucket = AWS::S3.new.buckets["rake-data"]
    bucket.objects.map(&:key).each do |key|
      bucket.objects[key].move_to("production/#{key}")
    end
  end
end
