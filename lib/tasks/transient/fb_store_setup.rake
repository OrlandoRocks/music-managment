namespace :transient do
  task fb_store_setup: :environment do
    Store.create!(
      name: "Facebook Track Monetizations",
      abbrev: "fbt",
      short_name: "FBTracks",
      is_active: false,
      in_use_flag: true,
      delivery_service: "tunecore",
      position: 780,
    )

    CountryWebsite.all.each do |cw|
      product = Product.create!(
                  name: "Facebook Track Monetization",
                  display_name: "fb_track_monetization",
                  status: "Active",
                  price: 0.00,
                  currency: cw.currency,
                  product_family: "Artist Services (post-distribution)",
                  country_website_id: cw.id,
                  created_by: Person.first
                )

      cw.products << product

      SubscriptionProduct.create!(
        product_name: "FBTracks",
        product_type: "annually",
        product_id: product.id,
        term_length: 12
      )
    end

    KnowledgebaseLink.create(article_slug: "facebook-tracks-description")
  end
end
