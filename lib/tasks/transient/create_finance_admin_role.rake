namespace :transient do
  task create_finance_admin_role: :environment do
    pegged_rate_create_rights = Right.find_or_create_by(name: "create_pegged_rate", controller: "admin/foreign_exchange_pegged_rates", action: "create")
    pegged_rate_create_form_rights = Right.find_or_create_by(name: "new_pegged_rate", controller: "admin/foreign_exchange_pegged_rates", action: "new")
    pegged_rate_price_preview_rights = Right.find_or_create_by(name: "preview_pegged_prices", controller: "admin/foreign_exchange_pegged_rates", action: "preview_price")
    pegged_rates_history_view_rights = Right.find_or_create_by(name: "view_pegged_rates", controller: "admin/foreign_exchange_pegged_rates", action: "index")

    finance_admin_rights = [pegged_rate_create_rights, pegged_rates_history_view_rights, pegged_rate_create_form_rights, pegged_rate_price_preview_rights]
    finance_admin_role = Role.find_or_create_by(name: "Finance Admin", long_name: "Finance Admin", is_administrative: 1)
    finance_admin_role.update(rights: finance_admin_rights)
    finance_admin_records = (finance_admin_rights + [finance_admin_role])
    if finance_admin_records.all?(&:persisted?)
      Rails.logger.info "Successfully Created Finance Admin Roles & Rights"
    else
      failed_operations = finance_admin_records.select{|finance_admin_record| !finance_admin_record.persisted? }
      Rails.logger.fatal "The Following Operations Failed: "
      failed_operations.each do |failed_operation|
        Rails.logger.fatal "#{failed_operation.class}::#{failed_operation.name}: #{failed_operation.errors.full_messages.join(', ')}"
      end
    end
  end
end
