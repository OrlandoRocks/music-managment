# frozen_string_literal: true

JAN2022_POSTING_IDS = [28_056, 28_104, 28_105, 28_106, 28_107, 28_108, 28_109, 28_110]

namespace :transient do
  task fix_youtube_sales_report_for_jan2022: :environment do
    Rails.logger.info "Starting Task"
    batch_size = ENV["BATCH_SIZE"] || "100000"
    YouTubeRoyaltyRecord
      .where(posting_id: JAN2022_POSTING_IDS)
      .in_batches(of: Integer(batch_size, 10))
      .each_with_index do |batch, batch_no|
        batch.update_all(sales_period_start: Date.parse("2022-01-01"))
        Rails.logger.info("Dispatched batch #{batch_no}")
      end
    Rails.logger.info "Complete"
  end
end
