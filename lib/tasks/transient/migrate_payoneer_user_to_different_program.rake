# frozen_string_literal: true

namespace :transient do
  desc "migrate payoneer users to a different program"
  task :migrate_payoneer_user_to_different_program, [:person_id, :program_id] => :environment do |_t, args|
    new_program_config = PayoutProviderConfig.by_env.find_by(program_id: args.program_id)

    if new_program_config.present?
      payout_provider = Person.find_by(id: args.person_id)&.payout_provider

      if payout_provider.present?
        payout_provider.update(payout_provider_config: new_program_config)
        Rails.logger.info("[Payout Program Migration] Person Migrated To New Program Successfully")
      else
        Rails.logger.error("[Payout Program Migration] Invalid Person ID")
      end
    else
      Rails.logger.error("[Payout Program Migration] Invalid Program ID ")
    end
  end
end
