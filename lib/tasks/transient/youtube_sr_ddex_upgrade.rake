namespace :transient do
  task youtube_sr_ddex_upgrade: :environment do
    store = Store.find_by(abbrev: "ytsr")
    store.update(delivery_service: "tunecore")

    store_delivery_config = StoreDeliveryConfig.where(store_id: store.id).first
    store_delivery_config.update(
      ddex_version: "382",
      party_id: "PADPIDA2015120100H",
      party_full_name: "YouTube_ContentID",
      image_size: 0,
      file_type: "flac",
      skip_batching: true,
      use_resource_dir: true,
      remote_dir_timestamped: true,
      remote_dir: "tunecore_contentID"
    )
  end
end
