namespace :transient do
  task update_india_renewal_pricing: :environment do
    updated_products = {
      "434" => 20.4893, # Ringtone Renewal
      "444" => 53.9776, # 2 Year Album Renewal
      "445" => 18.4390, # 2 Year Single Renewal
      "446" => 135.3062, # 5 Year Album Renewal
      "447" => 46.4598, # 5 Year Single Renewal
      "417" => 9.5544, # Single Renewal
      "454" => 27.3237, # Yearly Album Renewal
    }
    updated_products.each do |id, updated_price|
      Product.find(id).update(price: updated_price)
    end
  end
end
