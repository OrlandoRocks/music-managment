namespace :transient do
  #USAGE: RENEWAL_PERSON_IDS_CSV_PATH="/tmp/person_ids_to_renew_releases.csv" rake transient:add_renewal_feature_flipper_to_users_in_csv
  task add_renewal_feature_flipper_to_users_in_csv: :environment do
    FeatureFlipper.add_feature('renew_expired_release_modal') unless FeatureFlipper.all_features.map(&:name).include?(:renew_expired_release_modal)
    user_ids = CSV.read(ENV['RENEWAL_PERSON_IDS_CSV_PATH']).flatten
    FeatureFlipper.update_feature('renew_expired_release_modal', 0, user_ids)
  end
end
