namespace :transient do
  task cleanup_apple_ids: :environment do
    creatives = Creative.joins(:external_service_id)
                        .joins("join albums on albums.id = creatives.creativeable_id and creatives.creativeable_type = 'Album'")
                        .joins("join salepoints on salepoints.salepointable_id = albums.id and salepoints.salepointable_type = 'Album'")
                        .joins("join artists on artists.id = creatives.artist_id")
                        .where(creatives: {role: 'primary_artist'})
                        .where(external_service_ids: {linkable_type: "Creative", identifier: "unavailable", service_name: "apple"})
                        .where(salepoints: {store_id: 36})
                        .where.not(salepoints: {finalized_at: nil})

    creatives.each do |creative|
      creative.external_service_id.apple_service.destroy!

      dc = DistributionCreator.create(album, "iTunesWW")
      Delivery::Distribution.deliver("Distribution", dc.distribution.id, dc.distribution.delivery_type || “full_delivery”)
    end
  end

  task cleanup_all_ids: :environment do
    ExternalServiceId.where(identifier: 'unavailable').in_batches do |batch|
      batch.delete_all
      sleep(2)
    end
  end
end
