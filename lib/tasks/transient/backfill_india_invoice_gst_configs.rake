# frozen_string_literal: true

namespace :transient do
  task backfill_india_invoice_gst_configs: :environment do
    active_indian_user_ids = StoredCreditCard
                               .payments_os_cards
                               .joins(:person)
                               .where(people: {country_website_id: CountryWebsite::INDIA})
                               .distinct
                               .map(&:person_id)
    gst_required = Invoice
                     .joins(:person)
                     .where(people: {id: active_indian_user_ids})
                     .where(gst_config: nil)
    csv_headers = ['invoice_id', 'gst_config_id', 'person_id']
    csv_data = []

    num_of_invoices = gst_required.length
    Rails.logger.info "starting task for #{num_of_invoices} invoices"

    gst_required.each_with_index do |invoice, idx|
      Rails.logger.info "starting update for invoice: #{invoice.id}, #{idx + 1} of #{num_of_invoices}"
      person = invoice.person

      gst_config = person.person_gst_config

      csv_data << [invoice.id, gst_config.id, person.id]

      invoice.update(gst_config: gst_config)

      msg = "completed update for invoice: #{invoice.id}, person: #{person.id}, gst_config: #{gst_config.id}"
      Rails.logger.info msg
    end

    s3_streaming_options = {
      file_name:   "gst_backfill_invoices_#{Rails.env}.csv",
      headers:     csv_headers,
      data:        csv_data,
      bucket_name: "indlocal",
      bucket_path: "backfill_invoices"
    }
    Rails.logger.info "uploading to s3, options: #{s3_streaming_options}"

    S3StreamingService.write_and_upload(s3_streaming_options)
  end
end
