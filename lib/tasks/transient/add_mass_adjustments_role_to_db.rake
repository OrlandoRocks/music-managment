namespace :transient do
  desc "add mass adjustments creator role"
  task add_mass_adjustments_role_to_db: :environment do
    Role.create(
        name: "Mass Adjustments",
        long_name: "Mass Adjustments Admin",
        description: "Gives admins the ability to process mass adjustments for customers",
        is_administrative: true
    )
  end
end