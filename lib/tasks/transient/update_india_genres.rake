namespace :transient do
  task update_india_genres: :environment do
    genres = Genre.where("name LIKE 'Indian - %'")
    india_parent_genre = Genre.find_by(name: 'Indian')

    genres.each do |genre|
      genre.update(name: genre.name.remove('Indian - '), parent_id: india_parent_genre.id)
    end
  end

  task fix_india_genres: :environment do
    genres = Genre.where(parent_id: 40)

    genres.each do |genre|
      new_name = genre.name.remove("translation missing: ")
      new_name = new_name.gsub("translation_missing_en_genre_name_indian_", "india_genres.").gsub("en.", "")

      genre.update(name: I18n.t(new_name))
    end
  end

  task rename_dup_india_genres: :environment do
    folk = Genre.find_by(name: 'Folk', parent_id: 40)
    folk.update(name: 'Indian Folk')

    classical = Genre.find_by(name: 'Classical', parent_id: 40)
    classical.update(name: 'Indian Classical')
  end

  task activate_indian_genres: :environment do
    parent_genre = Genre.india_parent_genre
    exit if parent_genre.blank?

    parent_genre.update(is_active: true, you_tube_eligible: 1)
    Rails.logger.info("Activated parent genre - #{parent_genre.id}")

    parent_genre.subgenres.update_all(you_tube_eligible: 1)
    Rails.logger.info("Activated YouTube for all subgenres")
  end
end
