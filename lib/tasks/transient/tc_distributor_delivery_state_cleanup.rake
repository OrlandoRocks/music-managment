namespace :transient do
  task tc_distributor_distro_clean_up: :environment do
    CUTOVER_STORES = {
      'juke' => '2020-10-29',
      '7digital' => '2020-11-11',
      'Zed' => '2020-11-11',
      'Tim' => '2020-11-11',
      'UMA' => '2020-12-02',
      'NetE' => '2020-12-02',
      'Medianet' => '2020-12-02'
    }

    CUTOVER_STORES.each do |name, date|
      store = Store.find_by(short_name: name)
      next unless store
      distros = Distribution.joins(:salepoints)
                  .where("salepoints.store_id = ?
                    AND distributions.state = 'error'
                    AND distributions.updated_at > ?", store.id, date)

      distros.update(state: 'delivered_via_tc_distributor')
    end
  end
end
