namespace :transient do
  desc 'update google store config'
  task update_google_store_config: :environment do
    config = StoreDeliveryConfig.readonly(false).for('Google')
    config.update(
      ddex_version: "37",
      party_full_name: "Google",
      party_id: "PADPIDA2010120902Y",
      image_size: 1400,
      file_type: "flac"
    )
  end
end