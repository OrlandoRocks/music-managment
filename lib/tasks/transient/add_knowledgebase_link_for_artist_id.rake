namespace :transient do 
  task add_knowledgebase_link_for_artist_id: :environment do 
    KnowledgebaseLink.create(article_id: "360001132963", 
          article_slug: "how-to-find-artist-id", 
          route_segment: "/articles/")
  end
end
