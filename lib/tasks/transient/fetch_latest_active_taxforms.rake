namespace :transient do
  task fetch_latest_active_taxforms: :environment do
    Rails.logger.info("Taxform fetch task initiated")
    Person.select(:id).for_united_states_and_territories.in_batches(of: 1000) do |people_batch|
      people_batch.each do |person|
        Rails.logger.info("Fetching Taxform for: #{person.id}")
        FetchTaxFormsWorker.perform_async(person.id)
      end
    end
    Rails.logger.info("Taxform fetch task concluded")
  end
end
