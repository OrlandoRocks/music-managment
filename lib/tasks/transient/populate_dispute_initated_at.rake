namespace :transient do
  desc "Update initated_at column of old dispute records with created_at"
  task populate_dispute_initiated_at: :environment do
    raise "You cannot run this in production" if Rails.env.production?

    Dispute.where(initiated_at: nil).update_all("initiated_at = created_at")
    Rails.logger.info "Updated initated_at column of old disputes with created_at"
  end
end
