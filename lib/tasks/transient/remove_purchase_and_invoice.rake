# Usage PURCHASE_IDS_CSV_PATH='purchase_ids.csv' rake transient:remove_purchase_and_invoice

namespace :transient do
  task remove_purchase_and_invoice: :environment do
    date = Date.today
    logger = ActiveSupport::Logger.new Rails.root.join("log", "remove_purchase_and_invoice_#{date.strftime("%m_%d_%Y")}.log")
    ActiveRecord::Base.logger = logger

    collector = Hash.new { |hash, key| hash[key] = [] }

    CSV.foreach(ENV['PURCHASE_IDS_CSV_PATH'], headers: true) do |row|
      collector[:purchase_ids] << row['purchase_id']
    end

    purchases = Purchase.where(id: collector[:purchase_ids])

    purchases.each do |p|
      invoice = p.invoice

      if invoice.present?
        invoice.delete
        if invoice.destroyed?
          collector[:deleted_invoices] << invoice.id
        else
          collector[:undeleted_invoices] << invoice.id
        end
      end

      p.delete
      if p.destroyed?
        collector[:deleted_purchases] << p.id
      else
        collector[:undeleted_purchases] << p.id
      end
    end

    logger.info("Results - #{collector}")
    logger.info("Rake task has completed!")
  end
end
