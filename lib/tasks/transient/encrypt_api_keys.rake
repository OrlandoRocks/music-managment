namespace :transient do
  task :encrypt_api_keys => :environment do
    api_v1_secret_key = SECRETS['api_v1_secret_key']
    raise "secret key not set in the SECRETS constant" unless api_v1_secret_key
    ApiKey.all.each do |api_key|
      encrypted_key = Digest::SHA2.hexdigest(api_v1_secret_key + api_key.key)
      api_key.key = encrypted_key
      api_key.save!
    end
  end
end
