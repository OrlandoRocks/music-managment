namespace :transient do
  namespace :publishing_administration do
    task publishing_recording_backfill: :environment do
      log = Logger.new("log/recording_backfill_#{Date.today.strftime("%d-%b")}.log")
      cutoff_date = ENV['cutoff_date'] || '2019-04-23'

      publishing_compositions_query = %{
        SELECT DISTINCT(co.id) AS publishing_composition_id, co.provider_identifier AS work_code, c.id AS publishing_composer_id FROM publishing_compositions co
          INNER JOIN publishing_composition_splits ps ON ps.publishing_composition_id = co.id
          INNER JOIN publishing_composers c ON c.id = ps.publishing_composer_id
          LEFT OUTER JOIN recordings r ON r.publishing_composition_id = c.id
        WHERE co.provider_identifier IS NOT NULL
          AND r.recording_code IS NULL
          AND co.state IN('split_submitted')
          AND co.is_unallocated_sum = 0
          AND DATE(co.created_at) >= '2018-12-21'
          AND DATE(co.created_at) <= '#{cutoff_date}'
      }

      publishing_compositions_to_be_sent = ActiveRecord::Base.connection.execute(publishing_compositions_query)

      publishing_compositions_to_be_sent.each do |row|
        begin
          publishing_composition = PublishingComposition.find(row[0])
          publishing_composer = PublishingComposer.find(row[2])
          params = { publishing_composer: publishing_composer, publishing_composition: publishing_composition }

          PublishingAdministration::ApiClientServices::PublishingRecordingService.post_recording(params)
          log.info("Sent publishing_composition #{row[0]} of the publishing_composer #{row[2]}")
        rescue => e
          log.error("Unable to send publishing_composition #{row[0]} of the publishing_composer #{row[2]}")
          log.error(e.message)
          next
        end
      end
    end
  end
end
