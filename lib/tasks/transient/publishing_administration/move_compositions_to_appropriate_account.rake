namespace :transient do
  namespace :publishing_administration do
    task move_composer_132331_compositions_to_appropriate_account: :environment do
      appropriate_composer = Composer.find_by(
        id: 104156,
        first_name: "Octavio",
        last_name: "Vizcarra recio"
      )

      composer = Composer.find_by(
        id: 132331,
        first_name: "Octavio",
        last_name: "Vizcarra recio"
      )

      publishing_splits = composer.publishing_splits

      compositions = Composition.where(id: publishing_splits.pluck(:composition_id))

      non_tunecore_songs = NonTunecoreSong.where(composition_id: compositions.pluck(:id))

      non_tunecore_albums = NonTunecoreAlbum.where(id: non_tunecore_songs.pluck(:non_tunecore_album_id))

      publishing_splits.update_all(composer_id: appropriate_composer.id)
      non_tunecore_albums.update_all(composer_id: appropriate_composer.id, account_id: appropriate_composer.account_id)
    end
  end
end
