namespace :transient do
  namespace :publishing_administration do
    task update_docusign_document_templates: :environment do
      DocuSign::TemplatesApi.list_templates.envelope_templates.each do |docusign_template|
        template      = DocuSign::TemplatesApi.get_template(docusign_template.template_id)
        language_code = template.recipients.signers.first.tabs.text_tabs.find do |tab|
          tab.tab_label == "language_code"
        end.value

        document_template = DocumentTemplate.where(
          document_title: docusign_template.name,
          language_code:  language_code,
          document_type:  "publishing",
          role:           docusign_template.name == "Terms of Service" ? "composer_without_publisher" : "composer_with_publisher"
        ).first

        if docusign_template.present? && document_template.template_id != docusign_template.template_id
          document_template.update(template_id: docusign_template.template_id)
        end
      end
    end
  end
end
