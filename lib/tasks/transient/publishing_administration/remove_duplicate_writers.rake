namespace :transient do
  namespace :publishing_administration do
    task remove_duplicate_writers: :environment do
      def write_log_headers
        File.open("optimizely_5_logs_2.txt", 'a+') { |f| f.write("composer_id,account_id,person_id,message\n") }
      end

      def append_to_log(composer, message)
        File.open("optimizely_5_logs_2.txt", 'a+') { |f| f.write("#{composer.id}, #{composer.account_id}, #{composer.person_id}, #{message}\n") }
      end

      def handle_all_composers_not_active(duplicate_candidates: , statuses:)
        return false unless statuses.all? { |status| [:pending, :terminated].include?(status) }

        duplicate_candidates.each do |comp|
          if comp.terminated_composer
            append_to_log(comp, "#{comp.name}'s terminated composer deleted.")

            comp.terminated_composer.destroy!
          end
        end

        paid_composers = duplicate_candidates.select{ |dup| dup.paid_at != nil }

        unpaid_composers = duplicate_candidates.select{ |dup| dup.paid_at == nil }

        if paid_composers.any?
          to_delete = unpaid_composers.first || paid_composers.first

          append_to_log(to_delete, "#{to_delete.name} was deleted.")

          paid_composers.first.destroy!
        else
          to_delete = duplicate_candidates.first

          append_to_log(to_delete, "#{to_delete.name} was deleted.")

          to_delete.destroy!
        end

        true
      end

      def handle_all_composers_active(duplicate_candidates:, statuses:)
        return false unless statuses.all? { |status| status == :active }

        most_compositions_composer = duplicate_candidates.joins(:publishing_compositions)
                                                         .group("publishing_composers.id")
                                                         .order("COUNT(publishing_compositions.id) DESC")
                                                         .first

        duplicate_candidates.each do |comp|
          if comp.terminated_composer
            append_to_log(comp, "#{comp.name}'s terminated composer deleted.")

            comp.terminated_composer.destroy!
          end

          unless comp.id == most_compositions_composer.id
            append_to_log(comp, "#{comp.name} was deleted.")

            comp.destroy!
          end
        end

        true
      end

      write_log_headers

      composers = PublishingComposer.where(is_primary_composer: true).group(:person_id, :account_id, :first_name, :last_name).having("COUNT(*) > 1")

      composers.each do |composer|
        person = composer.person || composer&.account&.person

        append_to_log(composer,  "No associated person for composer: #{composer.id}") and next unless person

        duplicate_candidates = person.account.publishing_composers.where(first_name: composer.first_name,
                                                                         last_name: composer.last_name,
                                                                         is_primary_composer: true,
                                                                         account_id: composer.account_id)

        append_to_log(composer,  "No duplicate candidates found for composer: #{composer.id}") and next unless duplicate_candidates.any?

        statuses = duplicate_candidates.map { |comp|  PublishingAdministration::ComposerStatusService.new(comp).state }

        next if handle_all_composers_not_active(duplicate_candidates: duplicate_candidates, statuses: statuses)

        next if handle_all_composers_active(duplicate_candidates: duplicate_candidates, statuses: statuses)

        duplicate_candidates.each do |composer|
          status = PublishingAdministration::ComposerStatusService.new(composer).state

          if [:pending, :terminated].include?(status)
            append_to_log(composer, "#{composer.name} was deleted.")

            composer.destroy!
          else
            if composer.terminated_composer
              composer.terminated_composer.destroy!
              append_to_log(composer, "Deleted terminated composer for #{composer.id}")
            end

            append_to_log(composer, "#{composer.name} kept.")
          end
        end
      end
    end
  end
end
