namespace :transient do
  namespace :publishing_administration do
    task add_knowledgebase_links: :environment do
      KnowledgebaseLink.create(
        article_id:     "115001079227",
        article_slug:   "publishing-admin-index",
        route_segment:  "/categories/"
      )
    end
  end
end
