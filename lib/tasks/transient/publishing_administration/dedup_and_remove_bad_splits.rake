namespace :transient do
  namespace :publishing_administration do
    task dedup_and_remove_back_splits: :environment do
      original_splits = PublishingCompositionSplit.joins(:publishing_composition)
                                                  .where.not(publishing_compositions: {state: 'verified'})
                                                  .group(:publishing_composition_id, :publishing_composer_id, :percent)
                                                  .having("COUNT(*) > 1")

      Rails.logger.info("Original splits found.")

      original_splits.each do |original_split|
        duplicates = PublishingCompositionSplit.where(publishing_composer_id: original_split.publishing_composer_id,
                                                      publishing_composition_id: original_split.publishing_composition_id,
                                                      percent: original_split.percent
                                               ).where.not(id: original_split.id)

        duplicates.destroy_all

        Rails.logger.info(".")
      end

      zero_splits = PublishingCompositionSplit.where(percent: 0)

      Rails.logger.info("#{zero_splits.count} zero splits found.")

      zero_splits.destroy_all

      Rails.logger.info("Zero splits deleted.")
    end
  end
end
