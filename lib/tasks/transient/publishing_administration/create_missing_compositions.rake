namespace :transient do
  namespace :publishing_administration do
    task create_missing_compositions: :environment do
      songs = Song.joins(:album).where(
        composition_id: nil,
        albums: {
          legal_review_state: 'APPROVED',
          takedown_at: nil
        }).joins(person: :composers)
      songs.each do |song|
        if song.person&.composers&.any? && song.person.composers.all?(&:is_paid) && !song.person.composers.first.try(:has_fully_terminated_composers?)
          song.composition = Composition.create(name: song.name)
          song.save
        end
      end
    end
  end
end
