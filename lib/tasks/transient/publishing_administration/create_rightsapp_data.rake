namespace :transient do
  namespace :publishing_administration do
    task create_rightsapp_ids_for_paid_composers: :environment do
      composers = Composer.select(:id).is_paid.with_no_provider_identifier

      composers.each do |composer|
        PublishingAdministration::ArtistAccountWriterWorker.perform_in(10.seconds, composer.id)
      end
    end
  end
end
