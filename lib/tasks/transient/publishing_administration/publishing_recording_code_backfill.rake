namespace :transient do
  namespace :publishing_administration do
    task publishing_recording_code_backfill: :environment do
      publishing_compositions = PublishingComposition
        .joins("LEFT OUTER JOIN recordings ON publishing_compositions.id = recordings.publishing_composition_id")
        .where.not(provider_recording_id: [nil, ""])
        .where(publishing_compositions: { is_unallocated_sum: 0 } )
        .where(recordings: { recording_code: nil })

      publishing_compositions.each do |publishing_composition|
        publishing_composition.recordings.create!(
          recording_code: publishing_composition.provider_recording_id,
          recordable: publishing_composition.song
        )
      rescue e
        puts e.message
        next
      end
    end
  end
end
