namespace :transient do
  task resubmit_unregistered_composers: :environment do
    unregistered_composer_ids = Composer
      .left_joins(:related_purchases)
      .where("composers.provider_identifier is null AND purchases.id is not null AND purchases.paid_at is not null")
      .distinct
      .pluck(:id)

    unregistered_publishing_composers = PublishingComposer.where(legacy_composer_id: unregistered_composer_ids)

    unregistered_publishing_composers.each{ |publishing_composer| publishing_composer.send(:send_to_publishing_administration) }
  end
end
