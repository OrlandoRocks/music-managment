desc "Creates or Updates the LOD Document Template by fetching data from DocuSign"
namespace :transient do
  namespace :publishing_administration do
    task load_lod_template_data: :environment do
      lod_present_in_docusign = false
      DocuSign::TemplatesApi.list_templates.envelope_templates.each do |docusign_template|
        if docusign_template.name == DocumentTemplate::LOD
          lod_present_in_docusign = true
          template = DocumentTemplate.find_by(document_title: DocumentTemplate::LOD)
          if template
            # Update the template_id
            template.update(template_id: docusign_template.template_id)
          else
            # Create a new document template
            DocumentTemplate.create(
              template_id: docusign_template.template_id,
              document_title: DocumentTemplate::LOD,
              document_type: "publishing",
              language_code: "en"
            )
          end
        end
      end
      puts lod_present_in_docusign ?
          "Updated LOD template details" :
          "Letter of Direction Template not found in DocuSign"
    end
  end
end
