namespace :transient do
  namespace :publishing_administration do
    task recording_backfill: :environment do
      log = Logger.new("log/recording_backfill_#{Date.today.strftime("%d-%b")}.log")
      cutoff_date = ENV['cutoff_date'] || '2019-04-23'

      compositions_query = %{
SELECT DISTINCT(co.id) AS composition_id, co.provider_identifier AS work_code, c.id AS composer_id FROM compositions co
  INNER JOIN publishing_splits ps ON ps.composition_id = co.id
  INNER JOIN composers c ON c.id = ps.composer_id
  LEFT OUTER JOIN recordings r ON r.composition_id = c.id
WHERE co.provider_identifier IS NOT NULL
  AND r.recording_code IS NULL
  AND co.state IN('split_submitted')
  AND co.is_unallocated_sum = 0
  AND DATE(co.created_at) >= '2018-12-21'
  AND DATE(co.created_at) <= '#{cutoff_date}'
}
      compositions_to_be_sent = ActiveRecord::Base.connection.execute(compositions_query)

      compositions_to_be_sent.each do |row|
        begin
          composition = Composition.find(row[0])
          composer = Composer.find(row[2])
          params = { composer: composer, composition: composition }

          PublishingAdministration::ApiClientServices::RecordingService.post_recording(params)
          log.info("Sent composition #{row[0]} of the composer #{row[2]}")
        rescue => e
          log.error("Unable to send composition #{row[0]} of the composer #{row[2]}")
          log.error(e.message)
          next
        end
      end

    end
  end
end
