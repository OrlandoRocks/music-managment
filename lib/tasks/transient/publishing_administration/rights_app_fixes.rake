namespace :transient do
  namespace :publishing_administration do
    task remove_writer_from_00001KZ9W: :environment do
      composer = Composer.find_by(provider_identifier: '00001KZ9W')

      next if composer.blank?

      composer.publishing_splits.destroy_all
      composer.destroy

      puts "Removed Composer - #{composer.id}"
    end
  end
end
