namespace :transient do
  namespace :publishing_administration do
    task create_rightsapp_ids_for_paid_publishing_composers: :environment do
      publishing_composers = PublishingComposer.select(:id).is_paid.with_no_provider_identifier

      publishing_composers.each do |publishing_composer|
        PublishingAdministration::PublishingArtistAccountWriterWorker.perform_in(10.seconds, publishing_composer.id)
      end
    end
  end
end
