namespace :transient do
  namespace :publishing_administration do
    task recording_code_backfill: :environment do
      compositions = Composition
        .joins("LEFT OUTER JOIN recordings ON compositions.id = recordings.composition_id")
        .where.not(provider_recording_id: [nil, ""])
        .where(compositions: { is_unallocated_sum: 0 } )
        .where(recordings: { recording_code: nil })

      compositions.each do |composition|
        composition.recordings.create!(
          recording_code: composition.provider_recording_id,
          recordable: composition.song
        )
      rescue e
        puts e.message
        next
      end
    end
  end
end
