namespace :transient do
  namespace :publishing_administration do
    task create_missing_publishing_compositions: :environment do
      songs = Song.joins(:album).where(
        publishing_composition_id: nil,
        albums: {
          legal_review_state: 'APPROVED',
          takedown_at: nil
        }).joins(person: :publishing_composers)

      songs.each do |song|
        paid_for_publishing = song.person&.publishing_composers&.any? &&
          song.person.publishing_composers.all?(&:is_paid) &&
          !song.person.publishing_composers.first.try(:has_fully_terminated_composers?)

        if paid_for_publishing
          song.publishing_composition = PublishingComposition.create(name: song.name, account: song.person.account)
          song.save
        end
      end
    end
  end
end
