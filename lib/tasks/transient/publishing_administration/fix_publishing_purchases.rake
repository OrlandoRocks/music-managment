namespace :transient do
  namespace :publishing_administration do
    task delete_unpaid_optimizely_purchases: :environment do
      def log_to_output_file(purchase_id, message, location = nil, composer = nil)
        File.open("optimizely_logs_for_5.txt", 'a+') {|f| f.write("#{purchase_id}, #{message}, #{location}, #{composer}\n")}
      end

      def create_new_composer_for_person(person)
        person.publishing_composers.create!(
          first_name: "",
          last_name: "",
          account_id: person.account.id,
          is_primary_composer: true,
          )
      end

      def delete_unpaid_optimizely_purchases
        purchases = Purchase.where(product_id: 65,
                                   related_id: 65,
                                   related_type: "Product",
                                   paid_at: nil
                                   )
        Rails.logger.info("Deleting purchase ID's: #{purchases.pluck(:id)}")

        deleted_purchases = purchases.destroy_all

        deleted_purchases.each do |purchase|
          log_to_output_file(purchase.id, "Deleted")
        end
      end

      def log_refund_purchases(person, good_purchase_id)
        to_refund = person.purchases.where(product_id: 65).where.not(id: good_purchase_id).where.not(paid_at: nil)

        to_refund.each do |purchase|
          log_to_output_file(purchase.id, "Needs refund")
        end
      end

      def get_composer_for_person(person)
        person.publishing_composers.primary.where.not(first_name: "", last_name: "").first ||
        person.publishing_composers.primary.where(first_name: "", last_name: "").first ||
        create_new_composer_for_person(person)
      end

      def handle_paid_but_unassociated_purchase(person, pub_purchases, tries = 0)
        paid_purchase = pub_purchases.where.not(paid_at: nil).first
        composer = get_composer_for_person(person)

        begin
          paid_purchase.update!(related: composer)
        rescue => e
          if tries == 0 && e.message.include?("Related has already been taken")
            # delete other associated purchase if it's unpaid for, associate to this purchase (which is paid for) by retrying the method.
            unpaid_associated = pub_purchases.find_by(paid_at: nil, related_id: composer.id, related_type: "PublishingComposer")

            raise e unless unpaid_associated

            Rails.logger.info("Deleting unpaid but associated purchase #{unpaid_associated.id}")

            unpaid_associated.destroy!

            handle_paid_but_unassociated_purchase(person, pub_purchases, 1)
          end
        end

        log_to_output_file(paid_purchase.id, "Associated purchase to composer: #{composer.id}")
        log_refund_purchases(person, paid_purchase.id)
      end

      def process_bad_purchase(purchase)
        person = purchase.person

        pub_purchases = person.purchases.where(product_id: 65)

        already_associated_paid_purchase = pub_purchases.joins("JOIN publishing_composers on publishing_composers.id = purchases.related_id and purchases.related_type = 'PublishingComposer'")
                                                        .where.not(purchases: { paid_at: nil } )
                                                        .where(publishing_composers: {
                                                           is_primary_composer: true
                                                        })
                                                        .order("publishing_composers.first_name DESC") #we want to take the purchase associated to a non-blank name composer first

        if already_associated_paid_purchase.any?
          Rails.logger.info("Already associated paid purchase for purchase: #{purchase.id}, person: #{person.id}")

          log_refund_purchases(purchase.person, already_associated_paid_purchase.first.id)
        elsif pub_purchases.where.not(paid_at: nil).any?
          Rails.logger.info("Handling paid but unassociated purchase: #{purchase.id}, person: #{person.id}")

          handle_paid_but_unassociated_purchase(person, pub_purchases)
        else
          log_to_output_file(purchase.id, "Has no paid purchases")
        end
      end

      # Main program execution

      delete_unpaid_optimizely_purchases

      unassociated_purchases = Purchase.where(related_id: 65, related_type: "Product")

      Rails.logger.info("#{unassociated_purchases.count} unassociated purchases found.")

      unassociated_purchases.each do |purchase|
        Rails.logger.info("Processing purchase #{purchase.id}")

        begin
          ActiveRecord::Base.transaction do
            process_bad_purchase(purchase)
          end
        rescue => e
          log_to_output_file(purchase.id, e.message)
        end
      end
    end
  end
end
