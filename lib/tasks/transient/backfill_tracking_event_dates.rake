namespace :transient do
  task backfill_tracking_event_dates: :environment do
    affected_rows = TunecoreTracking.where(event_date: nil).update_all('event_date = created_at')
    Rails.info.logger("Done Updating #{affected_rows} rows")
  end
end
