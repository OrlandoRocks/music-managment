namespace :transient do
  desc "Reset Withholding Exection Started At Dates to the beginning of the year"
  task reset_withholding_exemptions_started_at_date: :environment do
    WithholdingExemption.update_all(started_at: DateTime.current.beginning_of_year)
    Rails.logger.info "Finished Running Rake task ✓"
  end
end
