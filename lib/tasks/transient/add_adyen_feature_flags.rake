namespace :transient do
  namespace :add_adyen_feature_flags do
    desc "Create Feature Flag For Adyen integration"
    task create_adyen_integraion_feature_flag: :environment do
      FeatureFlipper.add_feature("adyen_integration")
      Rails.logger.info("Added adyen_integraion feature flag")
    end

    desc "Create Feature Flag For DANA and GoPay integration"
    task create_indonesian_wallets_feature_flag: :environment do
      FeatureFlipper.add_feature("indonesian_wallets")
      Rails.logger.info("Added indonesian_wallets feature flag")
    end

    desc "Create Feature Flags for Adyen Integration & Indonesian Wallet integration"
    task all: %i[
      create_adyen_integraion_feature_flag
      create_indonesian_wallets_feature_flag
    ]
  end
end

