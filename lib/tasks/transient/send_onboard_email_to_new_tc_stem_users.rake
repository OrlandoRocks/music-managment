namespace :transient do
  task send_onboard_email_to_new_tc_stem_users: :environment do
    stem_users = Person.where(referral_campaign: "stem")
    stem_users.each do |person|
      MailerWorker.perform_async("StemMailer", :onboarding_for_new_tc_user, person.id)
    end
  end
end
