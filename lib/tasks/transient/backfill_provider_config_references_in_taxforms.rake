# frozen_string_literal: true

namespace :backfill_provider_config_references_in_taxforms do
  task run: :environment do
    Rails.logger.info("PAYONEER TAX FORM UPDATE STARTED")
    TaxForm.where.not(program_id: nil).each do |tax_form|
      tax_form.update(payout_provider_config_id: PayoutProviderConfig.by_env.find_by(program_id: tax_form.program_id).id)
    end
    Rails.logger.info("PAYONEER TAX FORM UPDATE COMPLETE")
  end
end
