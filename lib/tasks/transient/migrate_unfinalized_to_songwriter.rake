namespace :transient do
  task migrate_unfinalized_to_songwriter: :environment do
    albums = Album.where.not(legal_review_state: 'APPROVED')
                  .where(finalized_at: nil)
                  .where("created_with != 'songwriter' OR created_with IS NULL")
                  .where("album_type IN ('Album', 'Single')")

    albums.find_in_batches.with_index do |group, batch|
      Rails.logger.info("Processing group #{batch}")

      Album.where(id: group).update_all(created_with: 'songwriter')
    end
  end
end
