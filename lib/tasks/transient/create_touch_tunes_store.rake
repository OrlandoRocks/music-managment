namespace :transient do
  task create_touch_tunes_store: :environment do
    unless Store.exists?(abbrev: "ttunes")
      StoreCreator.create(
        abbrev: "ttunes",
        base_price_policy: 3,
        delivery_service: "tunecore",
        description: "",
        in_use_flag: false,
        is_active: false,
        launched_at: Time.now,
        name: "TouchTunes",
        on_sale: false,
        position: 0,
        reviewable: false,
        short_name: "TouchTunes"
      )
    end
  end

  task create_touch_tunes_store_delivery_config: :environment do
    unless StoreDeliveryConfig.exists?(store_id: Store.find_by(abbrev: "ttunes").id)
      StoreDeliveryConfig.create(
        ddex_version: "37",
        delivery_queue: "delivery-default",
        file_type: "mp3",
        hostname: "filedrop.touchtunes.com",
        image_size: 1500,
        party_full_name: "TouchTunes Interactive Network",
        party_id: "PADPIDA2011051704A",
        password: "Theg2eph",
        port: 22022,
        remote_dir: "incoming",
        remote_dir_timestamped: false,
        skip_batching: false,
        store: Store.find_by(abbrev: "ttunes"),
        transfer_type: "sftp",
        use_resource_dir: true,
        username: "tunecore"
      )
    end
  end
end
