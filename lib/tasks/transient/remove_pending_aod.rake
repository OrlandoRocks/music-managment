namespace :transient do
  task remove_pending_aod: :environment do
    Salepoint.where(store_id: 19, payment_applied: false, finalized_at: nil)
             .find_in_batches(batch_size: 500).with_index do |salepoints, num|
               Salepoint.where(id: salepoints).delete_all

               Rails.logger.info("Batch #{num} salepoints deleted.")
             end
  end
end
