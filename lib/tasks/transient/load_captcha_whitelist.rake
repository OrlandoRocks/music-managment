desc "Load data into captcha whitelist table from a CSV"
namespace :load do
  task captcha_whitelist: :environment do
    file_path = ENV["WHITELIST_FILE_PATH"]

    unless file_path
      puts "WHITELIST_FILE_PATH env variable is needed to run this rake task"
      next
    end

    CSV.foreach(file_path) do |row|
      captcha = CaptchaWhitelist.new(ip: row.first, description: row.second)

      if captcha.save
        puts "Created - IP: #{row.first} Description: #{row.second}"
      else
        puts "Error - IP: #{row.first} Description: #{row.second} - Error: #{captcha.errors.full_messages.flatten.join(', ')}"
      end
    end

    puts "Finished creating captcha whitelists. Good Day!"
  end
end
