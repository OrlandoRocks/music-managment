REJECTION_EMAIL_TEMPLATE_MAP = {
  denied_streaming_fraud: :will_not_distribute,
  denied_ringtone_submitted_textone: :will_not_distribute,
  denied_album_unsupported_language: :will_not_distribute,
  denied_album_classical_album: :will_not_distribute,
  album_will_not_be_distributed: :will_not_distribute,
  album_blocked_from_sales: :will_not_distribute,
  denied_album_aka_or_alternate_artist_name: :changes_requested,
  denied_album_artist_name_incorrect_features: :changes_requested,
  denied_album_artist_name_misleading_lyrics_original: :changes_requested,
  denied_album_artist_name_too_generic: :changes_requested,
  denied_album_artwork_extra_info: :changes_requested,
  denied_album_artwork_cover_metadata_mismatch: :changes_requested,
  denied_album_artwork_quality: :changes_requested,
  denied_album_artwork_reference_physical_packaging: :changes_requested,
  denied_album_extra_info_associated_bands_instruments_members: :changes_requested,
  denied_album_remove_iTunes_iPhone_references: :changes_requested,
  denied_album_titles_producer_credit: :changes_requested,
  denied_album_unauthorized_artwork: :changes_requested,
  denied_album_reference_in_title: :changes_requested,
  denied_album_trademark_in_artist_field: :changes_requested,
  denied_unnecessary_title: :changes_requested,
  denied_update_artist_name: :changes_requested,
  denied_live_formatting: :changes_requested,
  denied_soundtrack_formatting: :changes_requested,
  denied_duplicate_titles: :changes_requested,
  denied_translations: :changes_requested,
  denied_incorrect_parental_logo: :changes_requested,
  denied_defective_audio: :changes_requested,
  denied_metadata_ep_error: :changes_requested,
  denied_remove_remixer: :changes_requested,
  multiple_songwriters_in_one_field: :changes_requested,
  denied_duplicate_audio: :changes_requested,
  album_blocked_from_sales_cover_song_formatting: :changes_requested,
  album_not_sure_maybe_block_from_sales: :rights_verification,
  denied_album_high_profile_features: :rights_verification,
  denied_album_possibly_using_masters: :rights_verification,
  denied_album_unauthorized_remix: :rights_verification,
  denied_album_unauthorized_samples_replace_not_OK: :rights_verification
}

namespace :transient do
  task backfill_template_type: :environment do
    REJECTION_EMAIL_TEMPLATE_MAP.each do |email_template_path, template_type|
      Rails.logger.info("*** Backfilling template_type ***")

      review_reason = ReviewReason.find_by(email_template_path: email_template_path)

      if review_reason.nil?
        Rails.logger.info("\nUnable to find a Review Reason with an email_template_path of #{email_template_path}")
        next
      end

      review_reason.update(template_type: template_type.to_s)
    end

    Rails.logger.info("\n*** Finished backfilling template_type ***")
  end
end
