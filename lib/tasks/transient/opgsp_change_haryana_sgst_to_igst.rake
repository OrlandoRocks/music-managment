namespace :transient do
  namespace :opgsp_change_haryana_sgst_to_igst do
    desc "Update All India Website Taxes To Use only 18% IGST config "
    task run: :environment do
      igst_config    = GstConfig.find_by(igst: 18.0)
      indian_states  = CountryState.joins(:country).where(countries: { iso_code: 'IN' })
      indian_states.update_all(gst_config_id: igst_config.id)
      Rails.logger.info "GST Configs for States As Per OPGSP requirements have been updated."
    end

    desc "Revert OPGSP .IN Website GST changes"
    task revert: :environment do
      sgst_config   = GstConfig.find_by(cgst: 9.0, sgst: 9.0)
      haryana_state = CountryState
        .joins(:country)
        .where(countries: { iso_code: 'IN' })
        .find_by(iso_code: 'IN-HR')
      haryana_state.update!(gst_config_id: sgst_config.id)
      Rails.logger.info "GST Configs for States As Per OPGSP requirements reverted."
    end
  end
end
