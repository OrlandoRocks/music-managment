namespace :transient do
  task extend_bytedance_renewal: :environment do
    Rails.logger.info "Grabbing all Bytedance Renewal Info"

    expiring_renewals = Person.find(3820050)
                              .renewals
                              .joins(:renewal_history)
                              .where("takedown_at IS NULL")
                              .where("expires_at < ? and expires_at >= ?", "2022-12-01", "2022-10-01")

    expiring_renewals.each do |expiring_renewal|
      expiring_renewal.renewal_history.update(expires_at: Date.new(2022, 12, 2))
    end
  end
end
