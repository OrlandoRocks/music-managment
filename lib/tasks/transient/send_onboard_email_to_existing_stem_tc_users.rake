namespace :transient do
  task send_onboard_email_to_existing_stem_tc_users: :environment do
    stem_users = Person.where(referral_campaign: "tc-stem")
    stem_users.each do |person|
      MailerWorker.perform_async("StemMailer", :onboarding_for_existing_tc_user, person.id)
    end
  end
end
