namespace :transient do 
    task extend_tiktok_renewal: :environment do
        #dont need to join targeted offers, can filter by targeted products instead 
        Rails.logger.info "Grabbing all Tiktok Singles Renewal Info"
        expiring_renewals = RenewalHistory
                            .joins(purchase: :targeted_product)
                            .joins("INNER JOIN albums 
                                ON purchases.related_id = albums.id 
                                AND purchases.related_type = 'Album'")
                            .joins("INNER JOIN salepoints  
                                ON albums.id = salepoints.salepointable_id 
                                AND salepoints.salepointable_type = 'Album'")
                            .where(
                                targeted_products: { display_name: ["Tiktok 1 Year Free", "TikTok 1 Jahr frei"] },
                                salepoints: { store_id: Store::TIK_TOK_STORE_ID}
                            )
        
        Rails.logger.info "Updating #{expiring_renewals.count} records"
        expiring_renewals.update(expires_at: DateTime.now + 1.years)
    end 

    task extend_paid_tiktok_renewal: :environment do 
        Rails.logger.info "Fetching All Releases with Only a Tiktok Salepoint" 
        sql =   "INNER JOIN albums a
                    ON purchases.related_id = a.id
                    AND purchases.related_type = 'Album'
                INNER JOIN salepoints s
                    ON a.id = s.salepointable_id
                    AND s.salepointable_type = 'Album'
                    AND s.store_id = 87
                LEFT JOIN salepoints s2 
                    ON a.id = s2.salepointable_id 
                    AND s2.salepointable_type = 'Album'
                    AND s2.store_id NOT IN (87,48,75,73)
                WHERE s2.id IS NULL
                    AND a.finalized_at IS NOT NULL 
                    AND s.finalized_at IS NOT NULL
                    AND a.takedown_at IS NOT NULL
                    AND a.id NOT IN (
                        SELECT DISTINCT a.id
                        FROM renewal_history rh
                        INNER JOIN purchases p
                            ON rh.purchase_id =  p.id
                        INNER JOIN targeted_products tp
                            ON p.targeted_product_id = tp.id
                            AND (tp.display_name = 'Tiktok 1 Year Free' OR tp.display_name = 'TikTok 1 Jahr frei')
                        INNER JOIN targeted_offers tof
                            ON tof.id = tp.targeted_offer_id
                            AND tof.id in (1766, 1764, 1767, 1768, 1770, 1769, 1765,1754)
                        INNER JOIN albums a
                            ON p.related_id = a.id
                            AND p.related_type = 'Album')"
        
        renewals_to_extend = RenewalHistory
                                .joins(:purchase)
                                .joins(sql.squish)
                                .update(expires_at: DateTime.now + 1.years)
                                                
    end 
end 

