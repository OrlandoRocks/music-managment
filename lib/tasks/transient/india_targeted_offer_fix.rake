namespace :transient do
  desc "Apply missing discount for India Penetration Offer-applicable purchases currently in cart"
  task india_cart_fix: :environment do
    INDIA_PENETRATION_OFFER_ID = 1687
    targeted_products = TargetedProduct.where(targeted_offer_id: INDIA_PENETRATION_OFFER_ID)
    product_ids = targeted_products.map(&:product_id)

    Purchase
      .joins(:person)
      .where(people: { country_website_id: 8 })
      .where(
        created_at: '2020-09-21 00:00:00'...,
        discount_cents: [nil, 0],
        paid_at: nil,
        product_id: product_ids
      )
      .each do |purchase|
        targeted_product = targeted_products.detect { |tp| tp.product_id == purchase.product_id }
        discount_cents = targeted_product.discount_amount_cents(purchase.cost_cents)

        purchase.update(
          discount_cents: discount_cents,
          targeted_product_id: targeted_product.id
        )
      end
  end
end
