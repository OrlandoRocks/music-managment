namespace :transient do
  task update_india_pricing: :environment do
    product_ids_with_us_map = {
      "454" => 1, # Yearly Album Renewal
      "417" => 3, # Single Renewal
      "418" => 9, # Added Store
      "419" => 18, # Album Distribution Credit
      "420" => 19, # Album Distribution Credit - 5 Pack
      "421" => 20, # Album Distribution Credit - 10 Pack
      "422" => 21, # Album Distribution Credit - 20 Pack
      "423" => 22, # Single Distribution Credit
      "424" => 23, # Single Distribution Credit - 5 Pack
      "425" => 24, # Single Distribution Credit - 10 Pack
      "426" => 25, # Single Distribution Credit - 20 Pack
      "427" => 36, # 1 Year Album
      "428" => 37, # 2 Year Album
      "429" => 38, # 5 Year Album
      "430" => 41, # 1 Year Single
      "431" => 42, # 2 Year Single
      "432" => 43, # 5 Year Single
      "433" => 65, # Songwriter Service
      "434" => 101, # Ringtone Renewal
      "435" => 103, # 1 Year Ringtone
      "436" => 105, # Store Automator
      "437" => 107, # Credit Usage
      "438" => 109, # Booklet
      "439" => 111, # Ringtone Distribution Credit
      "440" => 112, # Ringtone Distribution Credit - 3 Pack
      "441" => 113, # Ringtone Distribution Credit - 5 Pack
      "442" => 114, # Ringtone Distribution Credit - 10 Pack
      "443" => 115, # Ringtone Distribution Credit - 20 Pack
      "444" => 121, # 2 Year Album Renewal
      "445" => 123, # 2 Year Single Renewal
      "446" => 125, # 5 Year Album Renewal
      "447" => 127, # 5 Year Single Renewal
      "448" => 133, # TuneCore Fan Reviews Starter
      "449" => 134, # TuneCore Fan Reviews Enhanced
      "450" => 135, # TuneCore Fan Reviews Premium
      "451" => 145, # Collect Your YouTube Sound Recording Revenue
      "452" => 147, # Preorder
      "453" => 149, # LANDR Instant Mastering
      "455" => 410  # Facebook Track Monetization
    }

    india_products = Product.where(id: product_ids_with_us_map.keys)
    us_products = Product.where(id: product_ids_with_us_map.values).to_a

    india_products.each do |in_product|
      us_product = us_products.find {|pr| pr.id == product_ids_with_us_map[in_product.id.to_s]}

      in_product.update(price: calc_price(us_product.price), currency: 'USD')

      puts "Updated Product pricing for #{in_product.name} | #{calc_price(us_product.price)}"

      in_product.product_items.each do |in_product_item|
        in_product_item.update(price: calc_price(in_product_item.price), currency: 'USD')

        puts "Updated ProductItem pricing  #{in_product.name} | #{in_product_item.name} | #{calc_price(in_product_item.price)}"

        ProductItemRule.where(product_item_id: in_product_item.id).each do |rule|
          new_price = calc_price(rule.price)

          rule.update(price: new_price, currency: 'USD')

          puts "Updated ProductItemRule pricing  #{in_product.name} | #{in_product_item.name} | #{rule.id} | #{new_price}"
        end
      end
    end
  end

  # Following formula and the constant pegg rate are provided by finance & product
  # for this one-time price update exercise.
  def calc_price(price)
    return price if price <= 0

    pegged_rate = 73.16
    buffer = 0.28

    converted_price = (price * pegged_rate)
    price_with_buffer = converted_price + (converted_price * buffer) # add 28% for VAT + GST
    rounded_up_price = price_with_buffer.ceil_to(100) - 1 # -1 is to make the price end with 99

    (rounded_up_price / pegged_rate).round(4)
  end
end
