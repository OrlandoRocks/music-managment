namespace :transient do
  namespace :stem do
    task mark_as_suspicious_for_crt: :environment do
      stem_users = Person.where("referral_campaign = 'stem' or referral_campaign = 'tc-stem'")
      stem_users.each do |user|
        Note.create!(related: user, note_created_by: user, subject: "Marked as Suspicious", note: "STEM CUSTOMER: Please do not DENY any releases until further notice")
        user.update(status: "Suspicious", status_updated_at: Time.now, lock_reason: "stem")
        p "Marked user id #{user.id} as suspicious"
      end
    end
  end
end
