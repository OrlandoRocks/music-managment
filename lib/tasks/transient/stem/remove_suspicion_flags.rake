namespace :transient do
  namespace :stem do
    task remove_suspicion_flags: :environment do
      stem_user_id_query = <<-SQL
        SELECT DISTINCT `people`.id FROM `people`
        LEFT OUTER JOIN `notes` ON `notes`.`related_id` = `people`.`id` AND `notes`.`related_type` = "Person"
        WHERE
          `people`.lock_reason = "stem"
        AND
          `people`.`referral` = "stem"
        AND
          `people`.`status` = "Suspicious"
        AND
          `notes`.`note` = "STEM CUSTOMER: Please do not DENY any releases until further notice"
        AND
          NOT EXISTS (
            SELECT * from notes
            WHERE
              `notes`.`related_id` = `people`.`id` AND `notes`.`related_type` = "Person"
            AND
              `notes`.`note` != "STEM CUSTOMER: Please do not DENY any releases until further notice"
            AND
              `notes`.`subject` = "Marked as Suspicious"
          )
      SQL

      stem_user_ids = ActiveRecord::Base.connection.execute(stem_user_id_query).to_a.flatten

      stem_users = Person.where(id: stem_user_ids)

      unless stem_users.present?
        abort "No more users found to remove suspicion flags from. Congrats!"
      end

      stem_users.each do |user|
        updated_successfully = user.update(
          status: "Active",
          status_updated_at: Time.now,
          lock_reason: nil
        )

        if updated_successfully
          Note.create!(
            related: user,
            note_created_by: user,
            subject: "Suspicion Removed",
            note: "STEM CUSTOMER: Removed suspicion flag"
          )

          puts "Marked user id #{user.id} as active"
        end
      end

      puts "Task completed!"
    end
  end
end
