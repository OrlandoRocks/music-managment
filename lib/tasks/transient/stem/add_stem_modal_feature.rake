namespace :transient do
  namespace :stem do
    task add_disabled_modal_feature: :environment do
      feature_name = :disable_stem_modal

      if Feature.find_by(name: feature_name)
        p "Feature already created. Exiting"
      else
        Feature.create!(name: feature_name, restrict_by_user: true)
        p "Created Feature - #{feature_name}"
      end
    end
  end
end
