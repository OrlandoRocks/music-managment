# frozen_string_literal: true

namespace :transient do
    desc "Rename 'Default to Country Website' audit source to 'Default to US'"
    task rename_sdm_flags_to_tc_accelerator: :environment do

        OLD_FLAGS = {
            opt_out: {
                category: "distribution",
                name: "spotify_discovery_mode_opt_out"
            }.freeze,
            do_not_notify: {
                category: "distribution",
                name: "spotify_discovery_mode_do_not_notify"
            }.freeze
        }.freeze

        NEW_FLAGS = {
            opt_out: {
                category: "distribution",
                name: "tc_accelerator_opt_out"
            }.freeze,
            do_not_notify: {
                category: "distribution",
                name: "tc_accelerator_do_not_notify"
            }.freeze
        }.freeze

        update_count = 0

        OLD_FLAGS.each do |(i, flag)|
            update_count += Flag.where(flag).update(name: NEW_FLAGS[i][:name]).length
        end

        Rails.logger.info "Successfully updated #{update_count} flag records from 'spotify_discovery_mode' to 'tc_accelerator'"
    end
  end
  