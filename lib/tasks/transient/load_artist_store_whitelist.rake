desc "Load data into artist store whitelist table from a CSV"
namespace :load do
  task artist_store_whitelist: :environment do
    file_path = ENV["FILE_PATH"]

    unless file_path
      puts "FILE_PATH env variable is needed to run this rake task"
      next
    end

    CSV.foreach(file_path, headers: true) do |row|
      artist = Artist.find(row['artist_id'])
      store = Store.find(row['store_id'])

      record = ArtistStoreWhitelist.find_or_initialize_by(artist: artist, store: store)

      if record.persisted?
        log_artist_store(artist, store, "Record already exists. Skipping.")
        next
      end

      if record.save
        log_artist_store(artist, store, "Successfully created")
      else
        log_artist_store(
          artist,
          store,
          "Error creating record. #{record.errors.full_messages.flatten.join(', ')}"
        )
      end
    end

    Rails.logger.info("Finished. Thanks for the opportunity to serve you. You are awesome!")
  end

  def log_artist_store(artist, store, msg)
    Rails.logger.info("ArtistID: #{artist.id} | StoreID: #{store.id}: #{msg}")
  end
end
