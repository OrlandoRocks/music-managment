namespace :transient do
  namespace :taxable_earnings_rollover_correction do
    desc "Taxable Earnings Rollover Correction for 2021-2022"
    task rollover: :environment do
      rake_datafile_service = RakeDatafileService.download(
        "tax_rollover_details_unfiltered.xlsx",
        environment_folder: Rails.env,
        binary_mode: true
      )

      workbook = Roo::Spreadsheet.open(rake_datafile_service.local_file.path)
      workbook.sheet(workbook.sheets[0]).parse(headers: true).each do |entry|
        if entry["Backup Withhold Yes/No"]&.strip.downcase.eql?("yes")
          TaxWithholdings::Transient::RolloverWithholdingService.new(
            entry["person_id"],
            entry["2021 1099 Amount (Combined Dist and Pub)"].to_f
          ).rollover!
        end
      end
    end
    
    desc "Run Withholding for Current Year distribution Earnings"
    task distribution_withhold: :environment do
      TaxWithholdings::SipBatchWorker.perform_async
      Rails.logger.info <<-MESSAGE
        JOB STARTED FOR WITHHOLDING FROM DISTRIBUTION TRANSACTIONS FOR THE CURRENT YEAR"
      MESSAGE
    end

    desc "Run Withholding for Current Year Publishing Earnings"
    task publishing_withholding: :environment do
      TaxWithholdings::MassAdjustmentsBatchWorker.perform_async
      Rails.logger.info <<-MESSAGE
        JOB STARTED FOR WITHHOLDING FROM PUBLISHING TRANSACTIONS FOR THE CURRENT YEAR"
      MESSAGE
    end
  end
end
