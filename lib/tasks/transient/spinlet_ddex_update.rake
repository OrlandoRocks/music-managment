namespace :transient do
  task spinlet_ddex_update: :environment do
    sdc = StoreDeliveryConfig.find(12)

    sdc.update_attribute(:ddex_version, "37")
  end
end