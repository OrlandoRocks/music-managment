namespace :transient do
  task add_country_audit_sources: :environment do
    CountryAuditSource::ALL_SOURCE_NAMES.each { |name| CountryAuditSource.find_or_create_by(name: name) }
  end
end
