namespace :transient do
  task add_chargeback_refund_reason: :environment do
    RefundReason.find_or_create_by(reason: "Chargebacks", visible: false)
  end
end
