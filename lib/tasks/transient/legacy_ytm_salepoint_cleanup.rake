namespace :transient do 
    task add_ytm_salepoint_to_legacy_data: :environment do 
        Rails.logger.info "Starting Rake Task to Backfill YTM legacy Salepoints"
        Rails.logger.info "Querying for affected Albums........."
        
        albums_missing_salepoints = Album.joins(:salepoints)
            .joins("LEFT JOIN salepoints s1
                ON albums.id = s1.salepointable_id 
                AND s1.salepointable_type = 'Album' 
                AND s1.store_id = 28")
        .where(salepoints: { store_id: 45 })
        .where.not(finalized_at: nil, salepoints: { finalized_at: nil})
        .where("s1.id IS NULL")
       
       Rails.logger.info "Enqueuing #{albums_missing_salepoints.count} jobs to Create Youtube Music Salepoints"
        albums_missing_salepoints.each do |album|
            Distribution::DistributionCreationWorker.perform_async(album.id, "Google")
       end 
    end 
end 