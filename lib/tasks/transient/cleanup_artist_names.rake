namespace :transient do
  task cleanup_artist_names: :environment do
    NAME_HTML_REGEX = /<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">(.+)<\/font><\/font>/

    def migrate_artist(html_artist, correct_artist)
      Rails.logger.info("Cleaning up artist: #{html_artist.id}")

      ActiveRecord::Base.transaction do
        #Reassign creatives
        html_artist.creatives.each do |creative|
          creative.update!(artist_id: correct_artist.id)
        end

        #Reassign person's artist id
        if html_artist.person
          html_artist.person.update!(artist_id: correct_artist.id)
        end

        #Reassign blacklisted artist
        if html_artist.blacklisted_artist
          html_artist.blacklisted_artist.update!(artist_id: correct_artist.id)
        end

        #Reassign artist store whitelist
        if html_artist.artist_store_whitelists.any?
          html_artist.artist_store_whitelists.each do |aswl|
            aswl.update!(artist_id: correct_artist.id)
          end
        end
      end

      Rails.logger.info("Done artist #{html_artist.id}")
    end

    #Process all artists with incorrect formatting.
    artists = Artist.where("name LIKE '<font style=\"vertical-align: inherit;\"><font style=\"vertical-align: inherit;\">%</font></font>'")

    artists.each do |html_artist|
      matched = html_artist.name.match(NAME_HTML_REGEX)

      raise "There's been a problem matching #{html_artist.name}" unless matched

      extracted_name = matched[1]

      #See if there is an existing artist with the extracted name
      correct_artist = Artist.find_by(name: extracted_name)

      #If no existing, create one.
      correct_artist = Artist.create!(name: extracted_name) unless correct_artist

      migrate_artist(html_artist, correct_artist)
    end

    # These two artists had a different format of their html tags, than the rest.

    blader = Artist.find_by(name: "<font style=\"vertical-align: inherit;\">Blader04</font></font>")
    correct_artist = Artist.find_by(name: "Blader04")
    migrate_artist(blader, correct_artist)

    jbeat = Artist.find_by(name: "<font style=\"vertical-alin: inherit;\"><font style=\"vertical-align: inherit;\">Jbeat</font></font>")
    correct_artist = Artist.find_by(name: "Jbeat")
    migrate_artist(jbeat, correct_artist)
  end
end
