namespace :transient do
  task add_payout_admin_role: :environment do
  	Role.create(name: "Payout Admin",
  				long_name: "Payout Administrator/Manager",
  				description: "Gives admins the ability to switch user between legacy balance history and new balance history views",
  				is_administrative: 1)
  end
end