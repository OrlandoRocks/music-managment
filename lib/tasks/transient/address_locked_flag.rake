#frozen_string_literal: true

namespace :transient do
  task add_address_locked_flag: :environment do
    Flag.find_or_create_by(category: Flag::ADDRESS_LOCKED[:category], name: Flag::ADDRESS_LOCKED[:name])
  end

  task update_country_locked_flag_name: :environment do
    flag = Flag.find_by(category: Flag::ADDRESS_LOCKED[:category], name: 'country_locked')
    flag.update(name: Flag::ADDRESS_LOCKED[:name])
  end
end
