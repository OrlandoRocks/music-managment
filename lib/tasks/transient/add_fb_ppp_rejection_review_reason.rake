namespace :transient do
  task add_fb_ppp_review_reason: :environment do
    review_reason = ReviewReason.find_or_create_by(
      reason_type: "REJECTED",
      reason: "Cannot Distribute: Ineligible Facebook-only release",
      review_type: "LEGAL",
      email_template_path: "fb_ppp_rejection",
      should_unfinalize: 1,
      primary: 0,
      template_type: "will_not_distribute"
    )
    roles = Role.where(name: ['Content Review Specialist', 'Content Review Manager'])
    review_reason.roles << roles
  end
end
