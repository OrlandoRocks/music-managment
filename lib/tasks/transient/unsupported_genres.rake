namespace :transient do
  desc 'Change genres for around 150 releases and then destroy the unsupported genre records.'
  task map_and_destroy_unsupported_genres: :environment do
    unsupported_genres = [42, 44, 47, 49, 50, 52, 53, 54, 62, 64, 66, 67, 68, 69, 72, 75, 78, 80, 84, 87, 89, 91, 95, 97, 100]
    regional_indian_genre_id = 88
    alb = Album.arel_table

    ActiveRecord::Base.transaction do
      Album
        .where(alb[:primary_genre_id].in(unsupported_genres)
        .or(alb[:secondary_genre_id].in(unsupported_genres)))
        .each do |a|
          a.primary_genre_id = regional_indian_genre_id if unsupported_genres.include?(a.primary_genre_id)
          a.secondary_genre_id = regional_indian_genre_id if unsupported_genres.include?(a.secondary_genre_id)
          a.save
        end

      Genre.where(id: unsupported_genres).each(&:destroy)
    end
  end
end
