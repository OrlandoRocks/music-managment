namespace :transient do
  task set_zero_dates_to_null: :environment do

    date = '1900-01-01'

    TABLES_TO_BE_UPDATED = [{ "apple_identifiers" => [:created_at]},
      {"cloud_search_batches" => [:last_add_date, :last_delete_date]},
      {"delivery_batch_items" => [:created_at, :updated_at]},
      {"delivery_batches" => [:created_at, :updated_at]},
      {"distributions" => [:created_at]},
      {"muma_song_societies" => [:registered_at]},
      {"petri_bundles" => [:created_at]},
      {"redistribution_packages" => [:created_at, :updated_at]}
    ]

    TABLES_TO_BE_UPDATED.each do |table_hash|
      table_hash.each do |table_name, rows|
        rows.each do |row|
          Rails.logger.info "Updating #{table_name}.#{row} where date is zero to #{date}"
          sql = "update #{table_name} SET #{row} = '#{date}' WHERE #{row} LIKE '%0000%'"
          ActiveRecord::Base.connection.execute(sql)
        end
      end
    end
  end
end
