namespace :transient do
  task :update_dead_stores => :environment do
    simfy = Store.find(31)
    simfy.update_attribute(:is_active, false)

    simfyaf = Store.find(64)
    simfyaf.update_attribute(:is_active, true)
  end
end
