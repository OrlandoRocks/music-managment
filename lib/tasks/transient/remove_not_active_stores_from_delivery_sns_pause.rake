namespace :transient do
  task remove_not_active_stores_from_delivery_sns_pause: :environment do
    PAUSED_SNS_QUEUE = "delivery-sns-pause".freeze

    Sidekiq::Queue.new(PAUSED_SNS_QUEUE).each do |job|
      if Store.not_active.is_not_used.pluck(:id).include?(job.args[1].to_i)
        job.delete
      end
    end
  end
end
