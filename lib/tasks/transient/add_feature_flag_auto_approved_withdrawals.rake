namespace :transient do
  desc "Adds auto_approved_withdrawals feature flag"
  task add_auto_approved_withdrawals_feature_flag: :environment do
    FeatureFlipper.add_feature("auto_approved_withdrawals")
  end
end
