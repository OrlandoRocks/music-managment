namespace :transient do
  task address_lock_flag_reason_migration: [
    :address_lock_flag_reason_backfill,
    :remove_self_identified_address_lock_flag_from_user,
    :remove_self_identified_address_lock_flag
  ]

  desc "Backfill flag_reason for address locked flags due to payoneer kyc"
  task address_lock_flag_reason_backfill: :environment do
    address_lock_flag = Flag.find_by(Flag::ADDRESS_LOCKED)
    payoneer_kyc_reason_id = FlagReason.payoneer_kyc_completed_address_lock.id

    records_to_update =
      PeopleFlag
      .joins(<<-SQL.strip_heredoc)
        INNER JOIN flag_transitions
        ON
        people_flags.flag_id = flag_transitions.flag_id
          AND
          people_flags.person_id = flag_transitions.person_id
      SQL
      .where(
        flag: address_lock_flag,
        flag_transitions: {
          add_remove: "add"
        }
      ).where(<<-SQL.strip_heredoc)
        flag_transitions.comment = 'Verified by Payoneer KYC'
        OR
        flag_transitions.comment = 'Payoneer KYC'
      SQL

    updated_rows_count = records_to_update.update_all([
      "people_flags.flag_reason_id = ?, flag_transitions.flag_reason_id = ?",
      payoneer_kyc_reason_id,
      payoneer_kyc_reason_id
    ])

    puts "Updated flag_reason for #{updated_rows_count} records"
  end

  desc "Remove self_identified_address_lock flag from user"
  task :remove_self_identified_address_lock_flag_from_user do
    self_identified_address_lock_flag = Flag.find_by(
      category: "person_location",
      name: "self_identified_address_locked"
    )
    self_identified_address_lock_reason =
      FlagReason
        .address_lock_flag_reasons
        .find_by(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)

    address_lock_flag = Flag.find_by(Flag::ADDRESS_LOCKED)

    person = Person.joins(:people_flags).find_by(
      people_flags: {
        flag: self_identified_address_lock_flag
      }
    )

    if person.present?
      # remove old flag
      person.people_flags.find_by!(flag: self_identified_address_lock_flag).destroy!
      person.flag_transitions.find_by!(flag: self_identified_address_lock_flag).destroy!
      puts "Removed self_identified_address_lock flag from user"

      # udpate address_lock flag reason
      person.people_flags
        .find_by(flag: address_lock_flag, flag_reason: nil)
        &.update!(flag_reason: self_identified_address_lock_reason)
      puts "Updated address_lock flag_reason to self_identified_address_lock"
    end
  end

  desc "Remove redundant self_identified_address_lock flag"
  task remove_self_identified_address_lock_flag: :environment do
    Flag.find_by!(
      category: "person_location",
      name: "self_identified_address_locked"
    ).destroy!
    puts "Destroyed self_identified_address_locked flag"
  end
end
