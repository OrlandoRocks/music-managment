namespace :transient do
    task change_target_music_to_use_keyfile: :environment do
        target_music = StoreDeliveryConfig.find(2)
        target_music.update(keyfile: "target_music_rsa", password: nil,transfer_type: "sftp")
    end
  end