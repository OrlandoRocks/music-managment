namespace :transient do
  task migrate_gain_employee_id: :environment do
    ActiveRecord::Base.transaction do
      Person
        .where("gain_employee_id IS NOT NULL AND gain_employee_id <> ''")
        .select(:gain_employee_id, :id, :created_on)
        .find_each do |person|
          ActiveRecord::Base.connection.execute(
            <<-SQL
              INSERT INTO archive_gain (gain_employee_id, person_id, created_at, updated_at)
              VALUES('#{person.gain_employee_id}',#{person.id},'#{person.created_on.to_s(:db)}',NOW())
            SQL
          )
      end
    end
  end
end
