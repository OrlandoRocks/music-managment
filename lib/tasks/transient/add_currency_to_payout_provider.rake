namespace :transient do
  desc 'Backfil person currency to payout provider'
  task add_currency_to_payout_provider: :environment do
    PayoutProvider
      .includes(:person)
      .where(active_provider: true)
      .where.not(person_id: nil)
      .find_each do |provider|
      provider.update(currency: provider.person.currency)
    end
  end
end
