namespace :transient do
  task youtube_music_ddex_upgrade: :environment do
    store = Store.find_by(abbrev: "ytm")
    store.update(delivery_service: "tunecore")

    store_delivery_config = StoreDeliveryConfig.where(store_id: store.id).first
    store_delivery_config.update(
      ddex_version: "37",
      party_id: "PADPIDA2013020802I",
      party_full_name: "YouTube",
      image_size: 1400,
      file_type: "flac",
      skip_batching: true,
      remote_dir_timestamped: true
    )

    StoreDeliveryConfig.update_all(test_delivery: false)
  end
end
