namespace :transient do
  task add_knowledgebase_links: :environment do
    KnowledgebaseLink.where( article_id: "661208", article_slug: "new_distribution_request", route_segment: "/requests/new?ticket_form_id=").first_or_create!
    KnowledgebaseLink.where( article_id: %w(177106 213803)).update_all(route_segment: "/requests/new?ticket_form_id=")
    KnowledgebaseLink.where(article_id: "", article_slug: "new_request", route_segment: "/requests/new").first_or_create
    KnowledgebaseLink.where(article_id: "", article_slug: "help-homepage", route_segment: "").first_or_create
  end

  task add_how_to_convert_audio_files_link: :environment do
    KnowledgebaseLink.create(
        article_id: "115006685288",
        article_slug: "how-to-convert-your-audio-files",
        route_segment: "/articles/"
    )
  end

  task add_how_format_albums_link: :environment do
    KnowledgebaseLink.create(
        article_id: "115006503247",
        article_slug: "how-do-i-format-my-album",
        route_segment: "/articles/"
    )
  end

  task update_help_homepage_link: :environment do
    link = KnowledgebaseLink.find_by(article_slug: "help-homepage")
    link.update(article_id: "360032390172", route_segment: "/articles/") if link.present?
  end

  task add_spotify_id_help_link: :environment do
    KnowledgebaseLink.create(
      article_id: "360040325651",
      article_slug: "how-to-find-artist-id-spotify",
      route_segment: "/articles/"
    )
  end

  task add_ringtone_description_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: "115006508747",
      article_slug: "ringtone-description",
      route_segment: "/articles/"
    )
  end

  task add_ytsr_help_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: "360040524812",
      article_slug: "youtube-music-and-sound-recording-revenue-services",
      route_segment: "/articles/"
    )
  end

  task add_spotify_artist_helper_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: "360041772131",
      article_slug: "spotify-for-artists",
      route_segment: "/articles/"
    )
  end

  task fix_help_homepage_link: :environment do
    link = KnowledgebaseLink.find_by(article_slug: "help-homepage")
    link.update(article_id: "", route_segment: "") if link.present?
  end

  task add_soundout_kb_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: "115006692948",
      article_slug: "about-soundout",
      route_segment: "/articles/"
    )
  end

  task add_wesing_kb_link: :environment do
    # Article slug needs to start with store.abbrev
    kb_link = KnowledgebaseLink.find_by(article_slug: "wesing-description") ||
      KnowledgebaseLink.new(article_slug: "wsng-description")

    kb_link.assign_attributes(
      article_slug: "wsng-description",
      article_id: "4405460084884",
      route_segment: "/articles/",
    )

    kb_link.save!
  end

  task add_luna_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_slug: "luna-description",
      article_id: "8322678188308",
      route_segment: "/articles/"
    )
  end

  task add_peloton_kb_link: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    KnowledgebaseLink.find_or_create_by(
      article_slug: "pel-description",
      article_id: "4406119493908",
      route_segment: "/articles/",
    )

    Rails.logger.info("✅ Rake task completed! ✅")
  end


  task add_douyin_kb_link: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    KnowledgebaseLink.find_or_create_by(
      article_id: "4418053746068",
      article_slug: "dou-description",
      route_segment: "/articles/",
    )

    Rails.logger.info("✅ Rake task completed! ✅")
  end

  task add_pricing_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: "115006503487",
      article_slug: "plans-release-pricing",
      route_segment: "/articles/"
    )
  end

  task add_tc_accelerator_link: :environment do
    KnowledgebaseLink.create(
      article_id: "13259365372820",
      article_slug: "tc-accelerator",
      route_segment: "/articles/"
    )
  end
end
