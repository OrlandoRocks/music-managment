namespace :transient do
  desc "Manual Tax Withholds Accounts with Encumbrances"
  task manual_rollover_accounts_with_encumbrances: :environment do
    Rails.logger.info "Initialized Manual Correction of Rollovers and Withholdings for Accounts with Encumbrances"

    manual_rollover_correction_accounts = [273_830]
    manual_erroneous_withholdings_deletion_accounts = [
      273_830,
      1_953_646,
      1_949_393,
      1_818_377
    ]

    manual_rollover_correction_accounts.each do |account_id|
      TaxWithholdings::Transient::ManualRolloverWithholdingCorrectionService.new(account_id).update_rollovers!
    end

    manual_erroneous_withholdings_deletion_accounts.each do |person_id|
      TaxWithholdings::Transient::ManualRolloverWithholdingCorrectionService
        .new(person_id)
        .expunge_withholding_transactions!
    end

    Rails.logger.info "Concluded Manual Correction of Rollovers and Withholdings for Accounts with Encumbrances"
  end
end
