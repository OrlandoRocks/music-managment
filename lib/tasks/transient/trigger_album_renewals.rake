# Usage RENEWAL_IDS_CSV_PATH='renewal_ids.csv' BATCH_DATE='10/27/2019' rake transient:trigger_album_renewals

namespace :transient do
  task trigger_album_renewals: :environment do
    batch_date = Date.strptime(ENV['BATCH_DATE'], "%m/%d/%Y")
    logger = ActiveSupport::Logger.new Rails.root.join("log", "trigger_album_renewals_batch_#{batch_date.strftime("%m_%d_%Y")}.log")
    Rails.logger = logger

    logger.info("Creating a batches for the date - #{batch_date}")

    renewal_ids = []
    CSV.foreach(ENV['RENEWAL_IDS_CSV_PATH'], headers: true) do |row|
      renewal_ids << row['renewal_id']
    end

    grouped_renewals = Renewal.where(id: renewal_ids).group_by(&:person)
    return if grouped_renewals.blank?

    renewal_products = Product.renewals.to_a.group_by { |p| p.id }
    batches = Batch.create_batches(batch_date)

    grouped_renewals.each do |person, renewals|
      purchases = []
      renewals.each do |renewal|
        product = renewal_products[renewal.item_to_renew.renewal_product_id].last
        related = renewal.renewal_items.first.related

        if related.is_a?(Album) && !related.has_ever_been_approved? && !related.has_been_distributed?
          logger.info "Skipping renewal purchase for renewal #{renewal.id} because album #{renewal.renewal_items.first.related.id} has never been approved or distributed or album.source is not STEM."
        else
          purchase = Product.add_to_cart(person, renewal, product)

          if purchase.invoice_id.nil?
            purchases << purchase
          else
            logger.info "Payment Batch | comment = Purchase skipped because it is in use person_id=#{person.id} renewal_id=#{renewal.id}"
          end
        end
      end

      logger.info("Payment Batch | Processing person #{{:person_id=>person.id, :country_website=>person.country_website_id}}")

      #Get the right batch
      batch = batches[person.country_website_id]

      #Create a group of invoices
      invoices     = Invoice.add_renewal_purchases_to_invoices(person, purchases)

      #Create batch transactions for all invoices
      transactions = batch.create_transactions_for_invoices(invoices)

      #Process the transactions
      batch.process_transactions(transactions)

      logger.info("Payment Batch | Completed processing person #{{ :person_id=>person.id}}")
    end

    batches.each do |country_website_id, batch|
      logger.info("completing batch - #{batch.inspect}")
      batch.complete_batch
    end
  end
end
