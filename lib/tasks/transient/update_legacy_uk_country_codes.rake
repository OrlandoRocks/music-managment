namespace :transient do
  desc "Update the legacy person country code from UK to GB"
  task update_legacy_uk_country_codes: :environment do
    start_time = Time.now

    puts "Started"
    Person.where(country: 'UK').find_each do |person|
      person.country_audit_source = CountryAuditSource::LEGACY_UK_NAME
      person.save!
    end

    puts "Completed. Took #{Time.now - start_time} seconds"
  end
end
