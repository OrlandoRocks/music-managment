namespace :transient do
  task seed_song_roles_and_ddex_song_roles: :environment do
    tablename = "song_roles"
    filename  = Rails.root.join("db/seed/csv/song_roles.csv")
    klass     = tablename.singularize.classify.constantize
    headers   = klass.attribute_names
    body      = CSV.read(filename)
    klass.delete_all
    klass.import(headers, body, validate: false)

    tablename = "ddex_roles"
    filename  = Rails.root.join("db/seed/csv/ddex_roles.csv")
    klass     = tablename.singularize.classify.constantize
    headers   = klass.attribute_names
    body      = CSV.read(filename)
    klass.delete_all
    klass.import(headers, body, validate: false)

    tablename = "ddex_song_roles"
    filename  = Rails.root.join("db/seed/csv/ddex_song_roles.csv")
    klass     = tablename.singularize.classify.constantize
    headers   = klass.attribute_names
    body      = CSV.read(filename)
    klass.delete_all
    klass.import(headers, body, validate: false)
  end
end
