namespace :transient do
  task remove_india_songwriter_product: :environment do
    in_songwriter_service_id = 433
    product = Product.find(in_songwriter_service_id)

    product.product_item_rules.map(&:destroy)
    product.product_items.destroy_all
    product.destroy

    puts "Destroyed Product: #{in_songwriter_service_id}"
  end
end
