namespace :transient do
  desc "Adds missing country_audit record for accounts missing them"
  task add_missing_country_audits: :environment do
    Rails.logger.info "Adding missing country audits"

    accounts_missing_country_audits = [1_179_268, 1_230_248]
    accounts_missing_country_audits.each do |person_id|
      person = Person.find_by(id: person_id)
      next unless person

      person.country_audits.create(
        country: Country.search_by_iso_code(person[:country]),
        country_audit_source: CountryAuditSource.find_by(name: CountryAuditSource::SELF_IDENTIFIED_NAME),
        created_at: DateTime.new(2021).beginning_of_year
      )
    end

    Rails.logger.info "Added missing country audits"
  end
end
