namespace :transient do
  task send_update_tax_forms_email: :environment do
    s3_file_name = ENV['UPDATE_TAX_FORMS_FILE']
    COLUMN = {
      id: 0,
      email: 1,
      name: 2,
      tin: 3,
    }
    tax_form_details = RakeDatafileService.csv(s3_file_name)
    tax_form_details.each do |row|
      MailerWorker.perform_async('PersonNotifier', :update_tax_forms, row[COLUMN[:id]], row[COLUMN[:name]], row[COLUMN[:tin]])
    end
  end
end
