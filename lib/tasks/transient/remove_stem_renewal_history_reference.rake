# Usage RENEWAL_IDS_CSV_PATH='renewal_history_ids.csv' rake transient:remove_stem_renewal_history_reference

namespace :transient do
  task remove_stem_renewal_history_reference: :environment do
    logger = ActiveSupport::Logger.new Rails.root.join("log", "remove_stem_renewal_history_reference.log")
    ActiveRecord::Base.logger = logger
    Rails.logger = logger

    renewal_history_ids = []
    CSV.foreach(ENV['RENEWAL_HISTORY_IDS_CSV_PATH'], headers: true) do |row|
      renewal_history_ids << row['renewal_history_id']
    end

    logger.info("Parsed renewal_history_ids - #{renewal_history_ids.length}")
    logger.info(renewal_history_ids)

    deleted_count = RenewalHistory.where(id: renewal_history_ids).delete_all

    logger.info("Deleted Count - #{deleted_count}")
  end
end
