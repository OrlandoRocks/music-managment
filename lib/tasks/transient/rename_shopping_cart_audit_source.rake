namespace :transient do
  desc "Rename 'Shopping Cart' audit source to 'Checkout'"
  task rename_shopping_cart_audit_source: :environment do
    default_to_audit_source = CountryAuditSource.find_by(name: 'Shopping Cart')
    default_to_audit_source.update(name: CountryAuditSource::CHECKOUT_NAME)
  end
end
