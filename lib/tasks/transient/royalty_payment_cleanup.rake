namespace :transient do
  task delete_royalty_payments_without_amounts: :environment do
    RoyaltyPayment
      .where.not(pdf_summary_file_name: nil)
      .where.not(pdf_summary_file_size: nil)
      .where(period_interval: 1, period_year: 2020, amount: nil)
      .delete_all
  end

  task update_royalty_payment_code: :environment do
    RoyaltyPayment
      .where(code: 0, pdf_summary_file_name: nil, pdf_summary_file_size: nil, period_interval: 1, period_year: 2020)
      .includes(person: [:account])
      .find_each do |rp|
        rp.update(code: rp.person.account.provider_account_id)
      end
  end
end
