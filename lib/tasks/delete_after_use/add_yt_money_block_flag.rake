namespace :delete_after_use do
  task add_yt_money_block_flag: :environment do
    p "Adding flag..."

    Flag.find_or_create_by(
      category: "distribution",
      name: "blocked_from_yt_money"
    )

    p "Flag added"
  end
end
