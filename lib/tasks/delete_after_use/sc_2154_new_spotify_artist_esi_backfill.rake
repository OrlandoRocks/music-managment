namespace :delete_after_use do
  task populate_new_spotify_artist_ids: :environment do
    albums_with_new_artists = Album
                              .distinct
                              .joins(:creatives)
                              .joins(:artists)
                              .joins(salepoints: :distributions)
                              .joins(:spotify_external_service_id)
                              .joins("INNER JOIN external_service_ids artist_esi
																	ON artist_esi.linkable_id = creatives.id
																	AND artist_esi.linkable_type = 'Creative'
																	AND artist_esi.service_name = 'spotify'")
                              .where(takedown_at: nil)
                              .where(salepoints: { store_id: 26, takedown_at: nil })
                              .where(distributions: { state: "delivered" })
                              .where.not(finalized_at: nil, salepoints: { finalized_at: nil })
                              .where("artist_esi.state = 'new_artist' AND artist_esi.identifier is null")

    spotify_service = SpotifyService.new

    spotify_service.update_artist_ids(albums_with_new_artists)
  end
end
