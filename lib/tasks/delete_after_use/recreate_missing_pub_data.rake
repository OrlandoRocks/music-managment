namespace :delete_after_use do
  task recreate_missing_pub_data: :environment do
    Rails.logger.info "⏲ Beginning rake task... ⏲"
    s3_file_name = "GS-11132-rights_app-data-6.csv"

    primary_publishing_composer = PublishingComposer.find_by(provider_identifier: "0000154CW", is_primary_composer: true)
    account = primary_publishing_composer.account

    unknown_publishing_composer = account.publishing_composers.where(is_unknown: true).first

    if unknown_publishing_composer
      unnecessary_unknown_splits = unknown_publishing_composer.publishing_composition_splits
      unnecessary_unknown_splits.delete_all
    end

    RakeDatafileService.csv(s3_file_name, headers: true).each_with_index do |row, index|
      publishing_composition_name = row["Song Title"].to_s
      publishing_composition_iswc = row["ISWC"]
      publishing_composition_provider_identifier = row["Client Song Id"]
      artist_name = row["Performing Artist 1"].to_s.strip
      non_tunecore_song_isrc = row["ISRC 1"]

      publishing_composition = account.publishing_compositions.find_or_initialize_by(provider_identifier: publishing_composition_provider_identifier)

      if publishing_composition.id.nil?
        publishing_composition.name = publishing_composition_name
        publishing_composition.account = account
      end

      publishing_composition.iswc ||= publishing_composition_iswc

      publishing_composition.save

      if publishing_composition.song.nil?
        ntc_album = NonTunecoreAlbum.create(
          name: publishing_composition.name,
          publishing_composer_id: primary_publishing_composer.id,
          orig_release_year: Date.today,
          account_id: primary_publishing_composer.account_id
        )

        artist = Artist.where(name: artist_name).first_or_create if artist_name.present?

        NonTunecoreSong.create(
          name: publishing_composition.name,
          non_tunecore_album_id: ntc_album.id,
          publishing_composition_id: publishing_composition.id,
          isrc: non_tunecore_song_isrc,
          artist_id: artist.try(:id)
        )
      end

      song = publishing_composition.reload.song

      [*1..2].each do |int|
        recording_name = row["Recording Title #{int}"]

        next unless recording_name.present?
        next if song.recordings[int - 1].present?

        song.recordings.create(publishing_composition: publishing_composition)
      end

      [*1..5].each do |int|
        performing_rights_organization_name = row["Writer Membership Name #{int}"]

        publishing_composition_split_percent = row["Writer Share #{int}"]
        publishing_composition_split_right_to_collect = row["Right To Collect? #{int}"] == "Y"

        publishing_composer_first_name = row["Writer First Name #{int}"]
        publishing_composer_middle_name = row["Writer Middle Name(S) #{int}"]
        publishing_composer_last_name = row["Writer Last Name #{int}"]
        publishing_composer_cae = row["Writer CAE/IPI #{int}"]
        publishing_composer_is_unknown = row["Unknown Writer? #{int}"] == "Y"
        publishing_composer_is_primary_composer = publishing_composition_split_right_to_collect
        publishing_composer_provider_identifier = row["Writers Originator Code #{int}"]

        publisher_name = row["Publisher Name #{int}"]
        publisher_cae = row["Publisher CAE/IPI #{int}"]

        if publishing_composer_provider_identifier
          publishing_composer = account.publishing_composers.where(
            provider_identifier: publishing_composer_provider_identifier
          ).first_or_initialize

          if publishing_composer.id.nil?
            publishing_composer.first_name = publishing_composer_is_unknown ? "" : publishing_composer_first_name
            publishing_composer.last_name = publishing_composer_is_unknown ? "" : publishing_composer_last_name
            publishing_composer.middle_name = publishing_composer_middle_name unless publishing_composer_is_unknown
            publishing_composer.cae = publishing_composer_cae unless publishing_composer_is_unknown
            publishing_composer.is_unknown = publishing_composer_is_unknown
            publishing_composer.is_primary_composer = publishing_composer_is_primary_composer

            publishing_composer.save
          end

          publishing_composition_split = publishing_composer.publishing_composition_splits.where(
            publishing_composer: publishing_composer,
            publishing_composition_id: publishing_composition
          ).first_or_initialize

          publishing_composition_split.percent = publishing_composition_split_percent
          publishing_composition_split.right_to_collect = publishing_composition_split_right_to_collect
          publishing_composition_split.save

          performing_rights_organization = PerformingRightsOrganization.find_by(name: performing_rights_organization_name)

          if performing_rights_organization
            publishing_composer.performing_rights_organization = performing_rights_organization
            publishing_composer.save
          end

          publisher = Publisher.where(
            name: publisher_name,
            cae: publisher_cae,
            performing_rights_organization: performing_rights_organization,
          ).first_or_create

          publishing_composer.publisher = publisher
          publishing_composer.save
        end
      end
    end

    Rails.logger.info "✅ Rake task completed! ✅"
  end
end
