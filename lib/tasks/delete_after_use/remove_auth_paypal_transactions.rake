# frozen_string_literal: true

namespace :delete_after_use do
  task remove_auth_paypal_transactions: :environment do
    PaypalTransaction.select(:id).where(action: ["Authorization", "Void"]).find_in_batches do |ids|
      Rails.logger.info "sleep for 2 secs"
      sleep(2.seconds)
      PaypalTransaction.where(id: ids).delete_all
      Rails.logger.info "deleted paypal_transactions, ids: #{ids}"
    end
    Rails.logger.info "completed!!!!"
  end
end
