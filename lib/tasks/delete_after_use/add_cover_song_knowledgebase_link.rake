namespace :delete_after_use do
  task add_cover_song_knowledgebase_link: :environment do
    KnowledgebaseLink.where(
      article_id: "115006695208",
      article_slug: "Distributing-Cover-Songs-Remixes-Mixtapes-or-MashUps",
      route_segment: "/articles/"
    ).first_or_create
  end
end
