# frozen_string_literal: true

namespace :delete_after_use do
  desc "update inactive plan_downgrade_requests to canceled"
  task update_inactive_downgrade_requests: :environment do
    inactive_downgrades = PlanDowngradeRequest.where(status: "Inactive")
    inactive_downgrades.update_all(status: PlanDowngradeRequest::CANCELED_STATUS)
  end
end
