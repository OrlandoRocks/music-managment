namespace :delete_after_use do
  task ytsr_proxy_track_monetization_backfill: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    query_statement = <<-SQL.strip_heredoc
      select
        DISTINCT(albums.id)
      from
        albums
      inner join
        salepoints
        on
        salepoints.salepointable_type = 'Album'
        and
        salepoints.salepointable_id = albums.id
        and
        salepoints.store_id = 105
      left outer join
        songs
        on
        songs.album_id = albums.id
      left outer join
        track_monetizations
        on
        track_monetizations.song_id = songs.id
        and
        track_monetizations.store_id = 48
      where
        albums.legal_review_state = 'APPROVED'
        and
        albums.takedown_at is null
        and
        albums.is_deleted = false
        and
        albums.finalized_at is not null
        and
        track_monetizations.id is null
        and
        salepoints.finalized_at is not null
        and
        salepoints.payment_applied = true
        and
        salepoints.takedown_at is null
        and
        salepoints.finalized_at < '2022-03-01 00:00:00'
    SQL

    initial_results = ActiveRecord::Base.connection.execute(query_statement).to_a

    album_ids = initial_results.present? ? initial_results.flatten.uniq : []

    salepoints = Salepoint.where(
      salepointable_type: "Album",
      salepointable_id: album_ids,
      store_id: 105
    )

    salepoints.each do |salepoint|
      salepoint.monetize_yt_tracks if salepoint.post_purchase_ytsr_proxy_salepoint?
    end

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
