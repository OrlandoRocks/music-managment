namespace :delete_after_use do
  task rename_snapchat_store: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    store = Store.find_by(id: 43)

    if store
      store.update(name: "Snapchat/Triller/7digital")
      Rails.logger.info("✅ Rake task completed! ✅")
    else
      Rails.logger.info("❌ Store not found ❌")
    end
  end
end
