namespace :delete_after_use do
  task shut_down_spinlet_store: :environment do
    pp "⏲ Beginning rake task ⏲"

    store = Store.find_by(name: "Spinlet")

    store.update(is_active: false, in_use_flag: false)

    pp "✅ Rake task completed ✅"
  end

  task pause_spinlet_store_devlivery_config: :environment do
    pp "⏲ Beginning rake task ⏲"

    store = Store.find_by(name: "Spinlet")

    store_devlivery_config = StoreDeliveryConfig.find_by(store_id: store.id)

    store_devlivery_config.update(paused: true)

    pp "✅ Rake task completed ✅"
  end

  task remove_spinlet_salepoints: :environment do
    pp "⏲ Beginning rake task ⏲"

    store = Store.find_by(name: "Spinlet")

    salepoint_ids = Salepoint
      .select(:id)
      .joins("inner join albums on albums.id = salepoints.salepointable_id")
      .where(
        store_id: store.id
      )
      .where(
        "
          (
            albums.finalized_at IS NULL
          )
          OR
          (
            albums.finalized_at IS NOT NULL
            AND
            salepoints.finalized_at IS NULL
          )

        "
      )

    salepoint_ids.in_groups_of(5000, false) do |batch_of_ids|
      Salepoint.where(id: batch_of_ids).delete_all
    end

    pp "✅ Rake task completed ✅"
  end
end
