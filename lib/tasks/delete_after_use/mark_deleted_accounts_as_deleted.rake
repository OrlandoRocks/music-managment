namespace :delete_after_use do
  task mark_deleted_accounts_as_deleted: :environment do
    deleted_ids = DeletedAccount.all.pluck(:person_id)

    unless deleted_ids.present?
      Rails.logger.info "💔 Did not find any deleted ids 💔"
      next
    end

    deleted_people = unmarked_deleted_people(deleted_ids)
    deleted_amount = deleted_people.size

    deleted_people.update_all(deleted: 1)

    deleted_amount_after = unmarked_deleted_people(deleted_ids).size
    total = deleted_amount - deleted_amount_after

    Rails.logger.info "✨ Successfully updated #{total} people ✨"
  end

  def unmarked_deleted_people(deleted_ids)
    Person.where(id: deleted_ids, deleted: 0)
  end
end
