namespace :delete_after_use do
    task add_apra_to_pro_table: :environment do
        PerformingRightsOrganization.where(name: "APRA", cicaccode: 8, provider_identifier: "39e6a3a9-52db-0ed2-9097-0e32d4564665").first_or_create
        Rails.logger.info("Generated APRA pro")
    end

    task fix_identifier_for_apra: :environment do
      pro = PerformingRightsOrganization.where(name: "APRA").first
      pro.update!(provider_identifier: "39ea3ed9-5b98-685c-1f4b-14be55242518")
      
      Rails.logger.info("Updated APRA identifier")
    end
end
