namespace :delete_after_use do
  desc "add new languages"
  task add_new_languages: :environment do
    lang1 = LanguageCode.new(
      {
        description: "fijian",
        code: "fj",
        needs_translated_song_name: 0,
        ISO_639_1_code: "fj",
        ISO_639_2_code: "fij",
        ISO_639_3_code: "fij"
      }
    )
    lang2 = LanguageCode.new(
      {
        description: "igbo",
        code: "ig",
        needs_translated_song_name: 0,
        ISO_639_1_code: "ig",
        ISO_639_2_code: "ibo",
        ISO_639_3_code: "ibo"
      }
    )
    lang3 = LanguageCode.new(
      {
        description: "kurdish",
        code: "ku",
        needs_translated_song_name: 0,
        ISO_639_1_code: "ku",
        ISO_639_2_code: "kur",
        ISO_639_3_code: "kur"
      }
    )
    lang4 = LanguageCode.new(
      {
        description: "kyrgyz",
        code: "ky",
        needs_translated_song_name: 0,
        ISO_639_1_code: "ky",
        ISO_639_2_code: "kir",
        ISO_639_3_code: "kir"
      }
    )
    lang5 = LanguageCode.new(
      {
        description: "lingala",
        code: "ln",
        needs_translated_song_name: 0,
        ISO_639_1_code: "ln",
        ISO_639_2_code: "lin",
        ISO_639_3_code: "lin"
      }
    )
    lang6 = LanguageCode.new(
      {
        description: "somali",
        code: "so",
        needs_translated_song_name: 0,
        ISO_639_1_code: "so",
        ISO_639_2_code: "som",
        ISO_639_3_code: "som"
      }
    )
    lang7 = LanguageCode.new(
      {
        description: "swahili",
        code: "sw",
        needs_translated_song_name: 0,
        ISO_639_1_code: "sw",
        ISO_639_2_code: "swa",
        ISO_639_3_code: "swa"
      }
    )
    lang8 = LanguageCode.new(
      {
        description: "tsonga",
        code: "ts",
        needs_translated_song_name: 0,
        ISO_639_1_code: "ts",
        ISO_639_2_code: "tso",
        ISO_639_3_code: "tso"
      }
    )
    lang9 = LanguageCode.new(
      {
        description: "wolof",
        code: "wo",
        needs_translated_song_name: 0,
        ISO_639_1_code: "wo",
        ISO_639_2_code: "wol",
        ISO_639_3_code: "wol"
      }
    )
    lang10 = LanguageCode.new(
      {
        description: "yoruba",
        code: "yo",
        needs_translated_song_name: 0,
        ISO_639_1_code: "yo",
        ISO_639_2_code: "yor",
        ISO_639_3_code: "yor"
      }
    )
    lang1.id = 79
    lang2.id = 80
    lang3.id = 81
    lang4.id = 82
    lang5.id = 83
    lang6.id = 84
    lang7.id = 85
    lang8.id = 86
    lang9.id = 87
    lang10.id = 88
    lang1.save
    lang2.save
    lang3.save
    lang4.save
    lang5.save
    lang6.save
    lang7.save
    lang8.save
    lang9.save
    lang10.save
  end
end
