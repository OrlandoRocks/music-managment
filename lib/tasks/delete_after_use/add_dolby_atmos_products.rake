# frozen_string_literal: true

namespace :delete_after_use do
  desc "add dolby atmos products"
  task add_dolby_atmos_products: :environment do
    ApplicationRecord.transaction do
      dolby_atmos_products = []
      CountryWebsite.all.each do |country_website|
        dolby_atmos_products << Product.find_or_create_by(
          country_website: country_website,
          name: "Dolby Atmos Track",
          display_name: "dolby_atmos_track",
          description: "Dolby Atmos Track",
          status: Product::ACTIVE,
          product_type: Product::AD_HOC,
          is_default: true,
          applies_to_product: SpatialAudio.to_s,
          sort_order: 1,
          renewal_level: Product::NONE,
          price: 16.99,
          currency: country_website.currency,
          product_family: Product::DISTRIBUTION_ADD_ONS,
          created_by_id: 1
        )
      end
      dolby_atmos_products.each do |product|
        ProductItem.find_or_create_by(
                     product: product,
                     name: product.name,
                     description: "Dolby Atmos Track",
                     price: product.price,
                     currency: product.currency,
                     does_not_renew: true
        )
      end
    end
  end
end
