namespace :delete_after_use do
  task backfill_tax_info: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    Transient::PublishingChangeover::TaxInfoBackfiller.backfill

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
