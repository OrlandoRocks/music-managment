namespace :delete_after_use do
  task update_dolby_product_prices: :environment do
    product_prices = { CA: 22.99, UK: 15.99, AU: 25.99, IN: 19.1224 }.with_indifferent_access
    locales_to_be_updated = product_prices.keys.map(&:to_s)
    products = Product.joins(:country_website)
                      .where(country_websites: { country: locales_to_be_updated })
                      .where(display_name: "dolby_atmos_audio")

    products.each do |product|
      country = product.country
      new_price = product_prices[country]

      Rails.logger.info "Updating #{country} plan from #{product.price} to #{new_price}"
      product.update!(price: new_price)
      product.product_items.first.update!(price: new_price)
    end

    Rails.logger.info "🎺 Completed updating Dolby Atmos Product Prices 🎻"
  end
end
