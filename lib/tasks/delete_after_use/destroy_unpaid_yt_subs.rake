namespace :delete_after_use do
  desc "destroy unpaid yt subs for discovery platforms launch"
  task destroy_unpaid_yt_subs: :environment do
    ytsr_product_ids = Product.where(display_name: 'ytsr').pluck(:id)

    p 'Deleting unpaid YTSR subscriptions'

    Purchase
      .joins(:product)
      .where(products: { id: ytsr_product_ids })
      .where(paid_at: nil)
      .in_batches { |batch| batch.delete_all }

    p 'Deleted all unpaid YTSR subscriptions'
  end
end
