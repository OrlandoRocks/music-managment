namespace :delete_after_use do
  task opt_out_terminated_composers: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    terminated_composers = PublishingComposer.where(sync_opted_in: true).where.not(terminated_at: nil)

    terminated_composers.update_all(sync_opted_in: false, sync_opted_updated_at: Time.current)

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
