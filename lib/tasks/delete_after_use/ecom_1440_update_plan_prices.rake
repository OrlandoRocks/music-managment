namespace :delete_after_use do
  task update_plan_product_prices: :environment do
    product_display_names = ["rising_artist", "breakout_artist", "professional", "additional_artist"]
    euro_county_codes = ["FR", "IT", "DE"]
    locales_to_be_updated = ["UK", "IN"] + euro_county_codes

    product_prices = {
      uk: {
        rising_artist: 12.99,
        breakout_artist: 24.99,
        professional: 39.99,
        additional_artist: 12.99
      },
      in: {
        rising_artist: 13.655,
        breakout_artist: 27.3237,
        professional: 54.6610,
        additional_artist: 13.655
      },
      eu: {
        rising_artist: 14.99,
        breakout_artist: 29.99,
        professional: 49.99,
        additional_artist: 14.99
      }
    }.with_indifferent_access

    products = Product.joins(:country_website)
                      .where(country_websites: { country: locales_to_be_updated })
                      .where(display_name: product_display_names)

    products.each do |product|
      country = (product.country.in? euro_county_codes) ? "eu" : product.country.downcase
      plan = product.display_name_raw
      new_price = product_prices[country][plan]

      Rails.logger.info "updating #{country} plan for #{new_price} from #{product.price} to #{new_price}"
      product.update(price: new_price)
    end

    Rails.logger.info "Completed Updating of Plan Product Prices"
  end
end
