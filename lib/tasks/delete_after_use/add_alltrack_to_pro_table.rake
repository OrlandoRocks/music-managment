namespace :delete_after_use do
  desc "Adds alltrack PRO into the Performing Rights Organization table"
  task add_alltrack_to_pro_table: :environment do
    PerformingRightsOrganization.where(name: "ALLTRACK", cicaccode: 999, provider_identifier: "39fd0d66-380a-cc3e-1b62-e87be26a9cc0").first_or_create
    Rails.logger.info("Generated ALLTRACK pro")
  end

  task fix_identifier_for_alltrack: :environment do
    pro = PerformingRightsOrganization.where(name: "ALLTRACK").first
    pro.update!(provider_identifier: "39fd0d66-380a-cc3e-1b62-e87be26a9cc0")

    Rails.logger.info("Updated ALLTRACK identifier")
  end
end
