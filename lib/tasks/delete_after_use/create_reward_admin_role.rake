namespace :delete_after_use do
  task create_reward_admin_role: :environment do
    role = Role.find_or_initialize_by(name: "Reward Admin")
    role.assign_attributes(
      long_name: "Reward System Admin",
      description: "Gives admins the ability to manage the reward system.",
      is_administrative: true
    )

    role.save!
  end
end
