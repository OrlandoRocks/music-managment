namespace :delete_after_use do
  desc "Takedown fb track monetizations that are missing copyrights"
  task takedown_fb_tracks_without_copyrights: :environment do
    Song.joins(:song_copyright_claim)
        .joins(:track_monetizations)
        .where(song_copyright_claims: { person_id: nil })
        .where(track_monetizations: { store_id: 83 })
        .find_each do |song|
      TrackMonetizationBlocker.find_or_create_by!(
        song_id: song.id,
        store_id: 83
      )
    end
  end
end
