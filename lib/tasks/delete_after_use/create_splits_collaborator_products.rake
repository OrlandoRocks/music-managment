# frozen_string_literal: true

namespace :delete_after_use do
  desc "create splits collaborator products"
  task create_splits_collaborator_products: :environment do
    products = []
    CountryWebsite.all.each do |cw|
      products << Product.find_or_create_by(
        name: "Splits Collaborator",
        display_name: "splits_collaborator",
        description: "Splits Collaborator",
        status: "Active",
        product_type: "Renewal",
        renewal_level: "Item",
        price: 7.99,
        currency: cw.currency,
        product_family: "Renewal",
        country_website_id: cw.id,
        created_by: Person.first
      )
    end
    products.each do |product|
      ProductItem.find_or_create_by(
        product: product,
        name: product.name,
        description: "Splits Collaborator",
        price: product.price,
        currency: product.currency
      )
    end
  end

  task update_splits_collaborator_products: :environment do
    prices = {
      "US" => { price: 7.99, currency: "USD" },
      "CA" => { price: 9.99, currency: "CAD" },
      "UK" => { price: 6.99, currency: "GBP" },
      "AU" => { price: 10.99, currency: "AUD" },
      "DE" => { price: 7.99, currency: "EUR" },
      "FR" => { price: 7.99, currency: "EUR" },
      "IT" => { price: 7.99, currency: "EUR" },
      "IN" => { price: 8.19, currency: "USD" },
    }

    Product.where(display_name: "splits_collaborator").each do |product|
      product_item = product.product_items.first
      country = product.country_website.country
      Product.transaction do
        product.update(prices[country])
        product_item.update(prices[country])
      end
    end
  end
end
