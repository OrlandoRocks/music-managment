namespace :delete_after_use do
  task add_isrcs_to_recordings: :environment do
    class RecordingIsrcBackfiller < Transient::PublishingChangeover::BackfillerBase
      def query_statement
        <<-SQL.strip_heredoc
          SELECT
            `recordings`.`id`
          FROM
            (
              SELECT
                `recordings`.`recordable_id`,
                Min(`recordings`.`id`) AS `first_recording_id`
              FROM
                `recordings`
              WHERE
                `recordings`.`recordable_type` = 'NonTunecoreSong'
              GROUP BY
                `recordings`.`recordable_id`
            ) AS `recordings_map`
          INNER JOIN
            `recordings` ON `recordings`.`id` = `recordings_map`.`first_recording_id`
          INNER JOIN
            `non_tunecore_songs`
            ON
            `non_tunecore_songs`.`id` = `recordings`.`recordable_id`
            AND
            `recordings`.`recordable_type` = 'NonTunecoreSong'
          WHERE
            `recordings`.`isrc` IS NULL
            AND
            `non_tunecore_songs`.isrc IS NOT NULL
            AND
            `non_tunecore_songs`.isrc != ''
        SQL
      end

      def update_statement
        <<-SQL.strip_heredoc
          UPDATE
            `recordings`
          INNER JOIN
            `non_tunecore_songs`
            ON
            `recordings`.`recordable_id` = `non_tunecore_songs`.`id`
            AND
            `recordings`.`recordable_type` = 'NonTunecoreSong'
          SET
            `recordings`.`isrc` = `non_tunecore_songs`.`isrc`
          WHERE
            `recordings`.`id` IN #{backfillable_record_ids}
        SQL
      end
    end

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    RecordingIsrcBackfiller.backfill

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
