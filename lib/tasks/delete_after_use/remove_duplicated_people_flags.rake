namespace :delete_after_use do
  task remove_duplicated_people_flags: :environment do
    PeopleFlag.find(3339).destroy!
    PeopleFlag.find(3377).destroy!
  end
end
