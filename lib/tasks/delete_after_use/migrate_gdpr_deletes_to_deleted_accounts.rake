namespace :delete_after_use do
  task migrate_gdpr_deletes_to_deleted_accounts: :environment do
    # because no model exists for this table, we must use raw sql
    sql = "SELECT person_id, created_at, updated_at from archive_gdpr_deletes"
    gdpr_deleted_records = ActiveRecord::Base.connection.execute(sql)
    @counter = 0

    unless gdpr_deleted_records
      Rails.logger.info "😿 Did not find anything on the gdpr_deletes table. Ending rake. 😿"
      next
    end

    if gdpr_deleted_records
      migrate_gdpr_records(gdpr_deleted_records)
      Rails.logger.info "✨ Migrated #{@counter} records from gdpr_deletes to deleted_accounts ✨"
    end
  end

  def migrate_gdpr_records(gdpr_deleted_records)
    gdpr_deleted_records.each do |record|
      DeletedAccount.find_or_create_by(
        person_id: record[0],
        delete_type: "GDPR",
        created_at: record[1],
        updated_at: record[2]
      ) do |_|
        @counter += 1 # this block only runs when a record is created
      end
    end
  end
end
