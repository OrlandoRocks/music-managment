namespace :delete_after_use do
  task remove_douyin_salepoints_from_china_excluding_releases: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    salepoints = Salepoint.joins(
        "
          left outer join
          albums
          on
          salepoints.salepointable_id = albums.id
          and
          salepoints.salepointable_type = 'Album'
        "
      )
      .joins(
        "
          left outer join
          salepoint_subscriptions
          on
          salepoint_subscriptions.album_id = albums.id
        "
      )
      .joins(
        "
          left outer join
          album_countries china_exclusions
          on
          china_exclusions.album_id = albums.id
          and
          china_exclusions.country_id = 45
          and
          china_exclusions.relation_type = 'exclude'
        "
      )
      .where(
        "
          salepoints.store_id = 103
          and
          albums.id is not null
          and
          albums.takedown_at IS NULL
          and
          albums.finalized_at IS NOT NULL
          and
          albums.payment_applied = 1
          and
          albums.legal_review_state = 'APPROVED'
          and
          salepoint_subscriptions.id is not null
          and
          salepoint_subscriptions.is_active = 1
          and
          china_exclusions.id is not null
        "
      )

    inventory_usages = InventoryUsage.where(related: salepoints)

    inventory_usages.destroy_all
    salepoints.update_all(payment_applied: false)
    salepoints.destroy_all

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
