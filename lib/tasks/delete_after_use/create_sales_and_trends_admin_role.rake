namespace :delete_after_use do
  task create_sales_and_trends_admin_role: :environment do
    Rails.logger.info "Createing Role for Sales and Trends Admin Bypass"

    role = Role.find_or_create_by!(
      name: "Sales And Trends",
      long_name: "Sales and Trends Feature Gating Bypass For Admin",
      description: "Allows admin to view Trends and Sales page regardless of plan status and feature gating eligibility",
      is_administrative: true
    )

    Rails.logger.info "Succesfully Created new role with following attributes: #{role.attributes}"
  end
end
