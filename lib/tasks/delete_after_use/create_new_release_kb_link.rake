namespace :delete_after_use do
  desc 'Create New Release KnowledgebaseLink'
  task create_new_release_kb_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: 115006689988,
      article_slug: 'create-new-release',
      route_segment: '/articles/'
    )
    Rails.logger.info('Task completed successfully')
  end
end
