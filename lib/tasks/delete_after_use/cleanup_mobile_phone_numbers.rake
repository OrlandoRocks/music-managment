namespace :delete_after_use do
  task cleanup_mobile_phone_numbers: :environment do
    s3_file_name = "bad_mobile_numbers.csv"
    BATCH_SIZE = 5000

    Rails.logger.info("Rake task begin")

    total = 0
    count = 0
    ids = []
    RakeDatafileService.csv(s3_file_name, headers: true).each do |row|
      count = count + 1
      ids << row["id"]

      if count >= BATCH_SIZE
        Person.where(id: ids).update_all(mobile_number: nil)
        ids = []
        total += count
        count = 0
      end
    end

    Person.where(id: ids).update_all(mobile_number: nil) if count > 0
    total += count

    Rails.logger.info("Rake task completed. Removed #{total} bad mobile_numbers")
  end
end
