namespace :delete_after_use do
  task correct_lod: :environment do
    person = Person.find(3325458)
    legacy_composer = person.composers.first
    publishing_composer = person.publishing_composers.primary.find_by(legacy_composer_id: legacy_composer.id)

    legacy_lod = person.legal_documents.find_by(subject: legacy_composer)
    new_lod = person.legal_documents.find_by(subject: publishing_composer)

    new_lod.signed_at = legacy_lod.signed_at
    new_lod.asset_file_name = legacy_lod.asset_file_name
    new_lod.asset_content_type = legacy_lod.asset_content_type
    new_lod.asset_file_size = legacy_lod.asset_file_size
    new_lod.asset_updated_at = legacy_lod.asset_updated_at
    new_lod.status = legacy_lod.status

    new_lod.save!
  end
end
