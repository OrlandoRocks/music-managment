# frozen_string_literal: true

namespace :delete_after_use do
  desc "rename dolby atmos products"
  task rename_dolby_atmos_products: :environment do
    Rails.logger.info "Starting..."
    ApplicationRecord.transaction do
      dolby_atmos_products = Product.where(display_name: "dolby_atmos_track")
      dolby_atmos_products.each do |product|
        product_item = product.product_items.first
        product.update(
          name: "Dolby Atmos Audio",
          display_name: "dolby_atmos_audio",
          description: "Dolby Atmos Audio"
        )
        product_item.update(
          name: "Dolby Atmos Audio",
          description: "Dolby Atmos Audio"
        )
      end
    end
    Rails.logger.info "and.....done."
  end
end
