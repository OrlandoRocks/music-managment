namespace :delete_after_use do
    task backfill_royalty_payments: :environment do
        @royalty_payments = RoyaltyPayment.select(
                                                "*",
                                                "royalty_payments.id as 'id'",
                                                "royalty_payments.composer_id",
                                                "publishing_composers.id as 'publishing_composer_id_real'")
                                          .joins("INNER JOIN publishing_composers on publishing_composers.legacy_composer_id = royalty_payments.composer_id")
                                          .where("royalty_payments.publishing_composer_id is NULL AND royalty_payments.composer_id IS NOT NULL")
        
        Rails.logger.info("Starting backfill operation")
        @royalty_payments.each do |royalty_payment|
            royalty_payment.publishing_composer_id = royalty_payment.publishing_composer_id_real;
            royalty_payment.save
            if royalty_payment.errors.present?
                Rails.logger.error(royalty_payment.errors)
            end
        end
        Rails.logger.info("Finished backfill operation")
    end
end