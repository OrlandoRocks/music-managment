namespace :delete_after_use do
  task add_retroactive_royalty_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "115006689048", article_slug: "Does-TuneCore-Publishing-Administration-collect-retroactively", route_segment: "/articles/").first_or_create
  end
end
