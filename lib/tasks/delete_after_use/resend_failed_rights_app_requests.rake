namespace :delete_after_use do
  task resend_failed_rights_app_requests: :environment do
    s3_file_name = "GS-10535-rights_app_errors-trimmed.csv"

    RakeDatafileService.csv(s3_file_name, headers: true).each_with_index do |row, index|
      requestable_type = row["requestable_type"]
      requestable_id = row["requestable_id"]

      if requestable_type == "Account"
        publishing_composer = PublishingComposer.find_by(account_id: requestable_id)

        PublishingAdministration::PublishingArtistAccountWriterWorker
          .perform_in(
            (index * 5).seconds,
            publishing_composer.id
          ) if publishing_composer

      elsif ["PublishingComposition", "Composition"].include?(requestable_type)
        publishing_composition = if requestable_type == "Composition"
          PublishingComposition.find_by(legacy_composition_id: requestable_id)
        else
          PublishingComposition.find_by(id: requestable_id)
        end

        publishing_composer = publishing_composition&.account && publishing_composition.account.publishing_composers.primary.first

        PublishingAdministration::PublishingWorkRecordingCreationWorker
          .perform_in(
            (index * 5).seconds,
            publishing_composer.id,
            publishing_composition.id
          ) if publishing_composer && publishing_composition

      end
    end
  end
end
