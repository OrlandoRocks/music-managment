namespace :delete_after_use do
  desc "update plan name to snake_case"
  task from_space_to_snake: :environment do
    Plan.all.each do |plan|
      plan.update(name: plan.name.parameterize.underscore)
    end
  end
end
