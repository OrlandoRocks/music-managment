namespace :delete_after_use do
  task rename_yt_store: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    store = Store.find_by(id: 28)

    if store && store.update(name: "YouTube Music")
      Rails.logger.info("✅ Rake task completed! ✅")
    else
      Rails.logger.info("❌ Rake task failed ❌")
    end
  end

  task rename_ytsr_store: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    store = Store.find_by(id: 105)

    if store && store.update(name: "YouTube Content ID / YouTube Shorts")
      Rails.logger.info("✅ Rake task completed! ✅")
    else
      Rails.logger.info("❌ Rake task failed ❌")
    end
  end
end
