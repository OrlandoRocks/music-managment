namespace :delete_after_use do
  task update_optional_isrc: :environment do
    pp "⏲ Beginning rake task ⏲"

    data_sets = [
      {
        song_id: 13844503,
        optional_isrc: "FR2X41620235",
      },
      {
        song_id: 13793591,
        optional_isrc: "SE3F70200201",
      },
      {
        song_id: 13793602,
        optional_isrc: "SE3F70200202",
      },
      {
        song_id: 13793608,
        optional_isrc: "SE3F70200203",
      },
      {
        song_id: 13793619,
        optional_isrc: "SE3F70200204",
      },
    ]

    data_sets.each do |data_set|
      song_id = data_set[:song_id]
      optional_isrc = data_set[:optional_isrc]

      song = Song.find_by(id: song_id)

      song&.update(optional_isrc: optional_isrc)
    end

    pp "✅ Rake task completed ✅"
  end
end
