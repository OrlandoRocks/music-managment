namespace :delete_after_use do
  task fix_braintree_transaction_country_website_ids: :environment do
    Rails.logger.info "Braintree Transaction country_website_ids started updating"
    BraintreeTransaction.joins(:person).where("people.country_website_id != braintree_transactions.country_website_id").in_batches do |batch|
      batch.each do |txn|
        txn.update(country_website_id: txn.person.country_website_id)
        Rails.logger.info "Transaction #{txn.id} updated"
      end
    end
    Rails.logger.info "Braintree Transaction country_website_ids finished updating"
  end
end
