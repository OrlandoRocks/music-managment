namespace :delete_after_use do
  task :fix_stored_credit_cards_with_invalid_last_four, [:dry_run, :start_id, :limit] => :environment do |_, args|
    dry_run = ["false"].exclude?(args[:dry_run])
    if dry_run
      required_changes = []

      start_id = args[:start_id] ? args[:start_id].to_i : 1
      limit = args[:start_id] ? args[:start_id].to_i : 1000
      sandbox = !Rails.env.production?
      payin_provider_config_ids = PayinProviderConfig.where(sandbox: sandbox).pluck(:id)

      StoredCreditCard.where("id >= ?", start_id).where(deleted_at: nil).where("length(last_four) < 4").where(payin_provider_config_id: payin_provider_config_ids).limit(limit).find_in_batches do |cards|
        cards.each do |card|
          next if card.bt_token.blank? || card.payin_provider_config.blank?
          config_gateway_service = Braintree::ConfigGatewayService.new(card.payin_provider_config)
          payment_method = config_gateway_service.find_payment_method(card.bt_token)
          required_changes << "#{card.id},#{card.last_four},#{payment_method.last_4}"
        end
      end

      required_changes_str = "#{required_changes.join(",\n")}"
      Rails.logger.info "stored_credit_card_id,previous_last_four,new_last_four\n#{required_changes_str}"
      File.open("./tmp/fix_stored_credit_cards_with_invalid_last_four.txt", 'w') do |file|
        file.write(required_changes_str)
      end
    else
      ActiveRecord::Base.transaction do
        File.open("./tmp/fix_stored_credit_cards_with_invalid_last_four.txt", 'r').each_line do |line|
          line_arr = line.split(",")
          card = StoredCreditCard.find(line_arr[0])
          card.update(last_four: line_arr[2])
        end
      rescue StandardError => e
        Rails.logger.error "An error occurred while updating cards: #{e}"
      end
    end
  end
end