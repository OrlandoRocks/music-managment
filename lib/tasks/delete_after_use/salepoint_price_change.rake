namespace :delete_after_use do
  desc "update salepoint prices"
  task update_salepoint_prices: :environment do
    country_price_map = {
      1 => 0.25, # US
      2 => 0.25, # CA
      3 => 0.20, # UK
      4 => 0.40, # AU
      5 => 0.25, # DE
      6 => 0.25, # FR
      7 => 0.25, # IT
      8 => 0.10  # IN
    }

    p 'Updating salepoint prices'

    ProductItemRule
      .includes(product_item: [:product])
      .where(method_to_check: 'store.is_free?')
      .each { |pir| pir.update(price: country_price_map[pir.product_item.product.country_website_id]) }

    p 'Salepoint prices updated'
  end
end
