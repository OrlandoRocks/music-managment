namespace :delete_after_use do
    # RUN AND ANALYZE OUTPUT FROM A DRY RUN FIRST BEFORE DISABLING SAFETY FEATURES
    # SAFETY ENABLED:  rake delete_after_use:remove_duplicate_publishing_compositions
    # SAFETY DISABLED: rake delete_after_use:remove_duplicate_publishing_compositions\[false,false,false\] 
    task :remove_duplicate_publishing_compositions, [:dry_run, :nil_associations_only, :pristine_only, :start_id, :end_id] => :environment do |_, args|
        # dry_run will not modify database records
        dry_run = ["false"].exclude?(args[:dry_run])

        # nil_associations_only will only delete compositions with no associated songs
        nil_associations_only = ["false"].exclude?(args[:nil_associations_only])

        # pristine_only will only delete compositions which have not been modified beyond new or new hidden
        pristine_only = ["false"].exclude?(args[:pristine_only])

        start_id = args[:start_id] ? args[:start_id].to_i : 2562733
        end_id = args[:end_id] ? args[:end_id].to_i : 2617466

        # Select all accounts which have duplicate compositions after id start_id (Last composition before pub-351 was merged) and id = end_id (Last one before Pub-404 Hotfix)
        publishing_compositions = PublishingComposition.where("id > ?", start_id).where("id < ?", end_id)
        account_ids = publishing_compositions.group(:name, :account_id).having('COUNT(*) > 1').select(&:account_id).pluck(:account_id).uniq
        account_publishing_compositions = publishing_compositions.where(account_id: account_ids).each_with_object({}) { |publishing_composition, result| result[publishing_composition.account_id] ||= []; result[publishing_composition.account_id] << publishing_composition }

        # arrays to store compositions slated for destruction and log them to console for inspection
        @deleted_compositions_log = []
        @to_be_deleted_publishing_compositions = []
        @to_be_disassociated_songs = []

        account_ids.each do |account_id|
            # Select all compositions associated with account_id after id start_id
            publishing_compositions = account_publishing_compositions[account_id]
            next unless publishing_compositions.present?

            # Grab song ids first before querying them to speed up the task.
            # Compositions will rarely be associated so this is the best solution
            songs_query = Song.where(publishing_composition_id: publishing_compositions.pluck(:id))
            songs = songs_query.each_with_object({}) { |song, result| next unless song.publishing_composition_id.present?; result[song.publishing_composition_id] = song }
            ntc_songs = NonTunecoreSong.where(publishing_composition_id: publishing_compositions.pluck(:id)).each_with_object({}) { |song, result| next unless song.publishing_composition_id.present?; result[song.publishing_composition_id] = song }
            albums = Album.where(id: songs_query.group(:album_id).pluck(:album_id)).each_with_object({}) { |album, result| result[album.id] = album }

            publishing_compositions.each do |publishing_composition|
                # Check composition associations
                publishing_composition_associated_TC = songs[publishing_composition.id].present?
                publishing_composition_associated_NTC = ntc_songs[publishing_composition.id].present?

                # Check if the composition has been modified by a user
                if pristine_only && !(publishing_composition.state == "hidden" && publishing_composition.prev_state == "new" || 
                                    publishing_composition.state == "new" && publishing_composition.prev_state.blank?)
                    next
                end

                if publishing_composition_associated_NTC || publishing_composition.is_unallocated_sum
                    next
                elsif publishing_composition_associated_TC
                    # Get associated song and album to check if the composition should have been created in the first place
                    song = songs[publishing_composition.id]
                    album = albums[song.album_id]

                    # If the composition is associated to a song and
                    if !nil_associations_only && (["APPROVED"].exclude?(album.legal_review_state) || album.takedown_at.present?)
                        @to_be_disassociated_songs.push(song)
                        Rails.logger.info "DELETING Composition: #{publishing_composition.to_json}"
                        @deleted_compositions_log.push("#{publishing_composition.to_json}\n")
                        @to_be_deleted_publishing_compositions.push(publishing_composition)
                    end
                else
                    Rails.logger.info "DELETING Composition: #{publishing_composition.to_json}"
                    @deleted_compositions_log.push("#{publishing_composition.to_json}\n")
                    @to_be_deleted_publishing_compositions.push(publishing_composition)
                end
            end
        end

        ActiveRecord::Base.transaction do
            if !dry_run
                @to_be_disassociated_songs.each do |song|
                    song.publishing_composition_id = nil
                    song.save!
                end

                @to_be_deleted_publishing_compositions.each do |publishing_composition|
                    publishing_composition.destroy!
                end
            end
        end

        Rails.logger.info @deleted_compositions_log
        Rails.logger.info "Total Deletions #{@deleted_compositions_log.length}"
        Rails.logger.info "COMPLETED remove_duplicate_publishing_compositions"
        Rails.logger.info "dry_run = #{dry_run}, nil_associations_only = #{nil_associations_only}, pristine_only = #{pristine_only}"
    end
end
