namespace :delete_after_use do
  task recalculate_steps_required_for_albums: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")
    start_time = Time.now

    albums = Person.find(3820050).albums.where(
      is_deleted: false,
      takedown_at: nil,
      known_live_on: nil,
      finalized_at: nil,
      steps_required: 0,
      legal_review_state: "DO NOT REVIEW"
    )

    errored_album_ids = []

    albums.map do |album|
      begin
        album.try(:calculate_steps_required)
      rescue => e
        errored_album_ids << album.id
      end
    end

    Rails.logger.info("The following albums produced errors: #{errored_album_ids.join(", ")}")

    end_time = Time.now
    Rails.logger.info("✅ Rake task completed! ✅")
    Rails.logger.info("start_time: #{start_time}")
    Rails.logger.info("end_time: #{end_time}")
  end
end
