namespace :delete_after_use do
  task resubmit_unregistered_composers: :environment do
    unregistered_composer_ids = Composer
      .left_joins(:related_purchases)
      .where("composers.provider_identifier is null AND purchases.id is not null AND purchases.paid_at is not null")
      .distinct
      .pluck(:id)

    unregistered_publishing_composers = PublishingComposer.where(legacy_composer_id: unregistered_composer_ids)

    unregistered_publishing_composers.each{ |publishing_composer| publishing_composer.send(:send_to_publishing_administration) }
  end

  task clean_up_and_resubmit_unregistered_composers: :environment do
    legacy_composer_ids = [
      101035, 115768, 118179,
      41069, 90892, 92108, 101101, 125506
    ]

    publishing_composers = PublishingComposer.where(legacy_composer_id: legacy_composer_ids)

    no_punctuation_regex = /[^A-Za-z0-9\s]/i

    publishing_composers.each do |publishing_composer|
      publishing_composer.first_name = publishing_composer.first_name&.gsub(no_punctuation_regex, "")
      publishing_composer.last_name = publishing_composer.last_name&.gsub(no_punctuation_regex, "")
      publishing_composer.middle_name = publishing_composer.middle_name&.gsub(no_punctuation_regex, "")

      publishing_composer.save!

      publisher = publishing_composer.publisher

      if publisher
        publisher.name = publisher.name&.gsub(no_punctuation_regex, "")

        publisher.save!
      end

      publishing_composer.reload

      publishing_composer.send(:send_to_publishing_administration)
    end
  end
end
