namespace :delete_after_use do
  task recreate_missing_pub_composer: :environment do
    account = Account.find(26652)

    agreed_to_terms_at = Time.now

    shared_attributes = {
      person_id: 1182755,
      account_id: 26652,
      publishing_role_id: 5,
      provider_identifier: "0000154CW",
      first_name: "Jason",
      last_name: "Gallo Gaffner",
      performing_rights_organization_id: 36,
      cae: "701677551"
    }

    composer = Composer.find_or_create_by(**shared_attributes, id: 67770)
    composer.agreed_to_terms_at ||= agreed_to_terms_at
    composer.save

    publishing_composer = PublishingComposer.find_or_create_by(**shared_attributes, legacy_composer_id: 67770, is_primary_composer: true, is_unknown: false)
    publishing_composer.agreed_to_terms_at ||= agreed_to_terms_at
    publishing_composer.save

    publishing_compositions = account.publishing_compositions

    publishing_compositions_splits = PublishingCompositionSplit.where(
      publishing_composition_id: publishing_compositions.pluck(:id),
      right_to_collect: true,
      publishing_composer_id: nil
    )

    publishing_compositions_splits.update_all(publishing_composer_id: publishing_composer.id)

    Rails.logger.info("Rake task completed!")
  end
end
