namespace :delete_after_use do
  task rename_china_to_mainland_china: :environment do
    china = Country.find_by(name: "China")

    unless china
      Rails.logger.info "😿 Did not find a country named China. Ending without renaming. 😿"
      next
    end

    if china.update!(name: "Mainland China")
      Rails.logger.info "✨ Successfully renamed China to Mainland China ✨"
    end
  end
end
