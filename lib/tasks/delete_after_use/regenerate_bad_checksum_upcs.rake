namespace :delete_after_use do
  desc "Regenerate bad upcs which failed checksum validation"
  task regenerate_bad_checksum_upcs: :environment do
    skip_delivered_albums = ActiveModel::Type::Boolean.new.cast(ENV.fetch("SKIP_DELIVERED_ALBUMS"))

    upcs = Upc.where(id: 5377700..5999999).where(upc_type: "tunecore").where("CHAR_LENGTH(number)=13")
    p "Re-generating bad UPCs"

    upcs.each do |upc|
      album = upc.upcable

      next if skip_delivered_albums && album.ready_for_distribution?

      Upc.where(id: album.tunecore_upcs.pluck(:id)).delete_all
      album.make_tunecore_upc
    end

    p "Re-generated bad UPCs"
  end
end
