namespace :delete_after_use do
  desc 'Replace mtn-description article in the KnowledgebaseLink'
  task update_knowledgebase_info: :environment do
    abort unless (link = KnowledgebaseLink.find_by(article_slug: 'mtn-description'))

    link.update(article_id: "115006689488")
    Rails.logger.info('Task completed successfully')
  end
end
