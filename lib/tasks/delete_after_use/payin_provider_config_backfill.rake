# please delete the worker as well when deleting this file

namespace :delete_after_use do
  task payin_provider_config_backfill: :environment do
    PayinProviderConfigBackfill::WorkflowWorker.perform_async
  end
end
