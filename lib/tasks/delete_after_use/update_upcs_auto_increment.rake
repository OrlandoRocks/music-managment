namespace :delete_after_use do
  task update_upcs_auto_increment: :environment do
    ActiveRecord::Base.connection.execute("ALTER TABLE upcs AUTO_INCREMENT = 6000000")
  end
end
