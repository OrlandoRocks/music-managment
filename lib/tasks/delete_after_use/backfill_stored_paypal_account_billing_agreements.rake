namespace :delete_after_use do
  task backfill_stored_paypal_account_billing_agreements: :environment do
    StoredPaypalAccount.where(billing_agreement_id: nil).find_each do |acct|
      acct.update(billing_agreement_id: acct.billing_agreement_id, email: acct.email)
    end
  end
end
