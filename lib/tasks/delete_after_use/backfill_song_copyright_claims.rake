namespace :delete_after_use do
  task backfill_song_copyright_claims: :environment do
    class SongCopyrightClaimMigrator < Transient::PublishingChangeover::MigratorBase
      def query_statement
        <<-SQL.strip_heredoc
          SELECT
            DISTINCT(`copyrights`.`copyrightable_id`) AS song_id,
            `copyrights`.`person_id`,
            `copyrights`.`claimant_has_songwriter_agreement`,
            `copyrights`.`claimant_has_pro_or_cmo`,
            `copyrights`.`claimant_has_tc_publishing`
          FROM
            `copyrights`
          WHERE
            `copyrights`.`id` in #{joined_ids_for_query_statement}
        SQL
      end

      def id_query_statement
        <<-SQL.strip_heredoc
          SELECT
            DISTINCT(`copyrights`.`id`)
          FROM
            `copyrights`
          LEFT OUTER JOIN
            `songs` ON `songs`.`id` = `copyrights`.`copyrightable_id` AND `copyrights`.`copyrightable_type` = "Song"
          LEFT OUTER JOIN
            `song_copyright_claims` ON `song_copyright_claims`.`song_id` = `songs`.`id`
          LEFT OUTER JOIN
            `albums` ON `albums`.`id` = `songs`.`album_id`
          LEFT OUTER JOIN
            `salepoints` ON `salepoints`.`salepointable_id` = `albums`.`id` AND `salepoints`.`salepointable_type` = "Album"
          WHERE
            `song_copyright_claims`.`id` IS NULL AND `salepoints`.`store_id` = 100
        SQL
      end

      def map_insertion_row(row)
        song_id,
          person_id,
          claimant_has_songwriter_agreement,
          claimant_has_pro_or_cmo,
          claimant_has_tc_publishing = row

        [
          song_id,
          person_id,
          claimant_has_songwriter_agreement,
          claimant_has_pro_or_cmo,
          claimant_has_tc_publishing
        ]
      end

      def insertion_statement
        <<-SQL.strip_heredoc
          INSERT INTO song_copyright_claims (
            song_id,
            person_id,
            claimant_has_songwriter_agreement,
            claimant_has_pro_or_cmo,
            claimant_has_tc_publishing
          ) VALUES #{insertion_values}
        SQL
      end
    end

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    SongCopyrightClaimMigrator.migrate

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
