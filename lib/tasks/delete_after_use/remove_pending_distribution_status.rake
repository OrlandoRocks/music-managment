namespace :delete_after_use do
  task remove_pending_distribution_status: :environment do
    PublishingComposition.where(state: 'pending_distribution').update_all(state: 'split_submitted')
  end
end
