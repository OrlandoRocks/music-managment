namespace :delete_after_use do
  desc "add latin genres"
  task add_african_genres: :environment do
    african_genre_names = [
      "Afrobeats",
      "Afropop",
      "Afro-fusion",
      "Afro-soul",
      "Afro house",
      "Amapiano",
      "Bongo Flava",
      "Highlife",
      "Maskandi"
    ].freeze

    parent_african_genre = Genre.find_or_create_by(name: "African", parent_id: 0, is_active: 0, you_tube_eligible: 1)

    african_genre_names.each do |african_genre_name|
      Genre.find_or_create_by(
        name: african_genre_name,
        parent_id: parent_african_genre.id,
        is_active: 0,
        you_tube_eligible: 1
      )
    end
  end
end
