namespace :delete_after_use do
  task update_um_corporate_entity: :environment do
    us_minor_outlying_islands = Country.find_by(iso_code: "UM")

    unless us_minor_outlying_islands
      Rails.logger.info "Did not find a country named US Minor Outlying Islands. No changes made."
      next
    end

    if us_minor_outlying_islands.update!(corporate_entity: CorporateEntity.find_by_name(CorporateEntity::TUNECORE_US_NAME))
      Rails.logger.info "🏝 Successfully changed US Minor Outlying Islands corporate entity to #{us_minor_outlying_islands.corporate_entity.name} 🏝"
    end
  end
end
