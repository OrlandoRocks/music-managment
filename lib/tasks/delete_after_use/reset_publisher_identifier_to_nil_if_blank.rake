namespace :delete_after_use do
  task reset_publisher_identifier_to_nil_if_blank: :environment do
    Rails.logger.info("Beginning rake task...")


    publishing_compositions = PublishingComposition.where(provider_identifier: "")
    initialCount = publishing_compositions.count

    publishing_compositions.each do |publishing_composition|
      publishing_composition.update(provider_identifier: nil);
      publishing_composition.save();
    end

    publishing_compositions = PublishingComposition.where(provider_identifier: "")
    finalCount = publishing_compositions.count;

    Rails.logger.info("\n\n\n");
    Rails.logger.info("BEGINNING COUNT: #{initialCount}");
    Rails.logger.info("ENDING COUNT: #{finalCount}\n\n\n");

    Rails.logger.info("Finished running rake task!")
  end
end
