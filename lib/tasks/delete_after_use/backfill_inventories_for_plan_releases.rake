namespace :delete_after_use do
  task backfill_inventories_for_plan_releases: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    query_statement = <<-SQL.strip_heredoc
      select distinct
        credit_usages.related_id
      from
        person_plan_histories 
      join
        purchases 
        on
        purchases.id = person_plan_histories.purchase_id 
      join
        invoices 
        on
        purchases.invoice_id = invoices.id
      join
        purchases album_purchases
        on
        album_purchases.invoice_id = invoices.id
        and
        album_purchases.related_type = 'CreditUsage'
      join
        credit_usages
        on
        album_purchases.related_id = credit_usages.id
        and
        credit_usages.related_type = 'Album'
      where
        credit_usages.plan_credit = true 
        and
        person_plan_histories.change_type = 'initial_purchase'
    SQL

    album_ids = ActiveRecord::Base.connection.execute(query_statement).to_a.flatten

    albums = Album.where(id: album_ids)

    albums.each do |album|
      purchase = album.credit_usage.purchase

      Plans::InventoryShimService.call(album.person, purchase)

      Inventory.use!(album.person, { items_to_use: [purchase.item_to_use]})

      next unless album.approved?

      album.salepoints.each { |sp| Delivery::Salepoint.deliver(sp.id) }
    end

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
