namespace :delete_after_use do
  task backfill_sync_opt_in: :environment do
    class SyncOptInBackfiller < Transient::PublishingChangeover::BackfillerBase
      def query_statement
        <<-SQL.strip_heredoc
          SELECT
            DISTINCT(`publishing_composers`.`id`)
          FROM
            `publishing_composers`
          INNER JOIN
            `accounts` ON `accounts`.`id` = `publishing_composers`.`account_id`
          INNER JOIN (
            SELECT
              DISTINCT(`people`.`id`)
            FROM
              `people`
            INNER JOIN (
              SELECT
                DISTINCT(`albums`.`person_id`)
              FROM
                `albums`
              INNER JOIN
                `review_audits` ON `review_audits`.`album_id` = `albums`.`id`
              WHERE
                `review_audits`.`event` = 'APPROVED'
                AND
                `albums`.`takedown_at` IS NULL
                AND
                `albums`.`finalized_at` IS NOT NULL 
                AND
                `albums`.`legal_review_state` = 'APPROVED'
            ) AS `albums` ON `albums`.`person_id` = `people`.`id`
          ) AS `people` ON `people`.`id` = `accounts`.`person_id`
          WHERE
            `publishing_composers`.`is_primary_composer` = 1
            AND
            `publishing_composers`.`sync_opted_in` IS NULL
        SQL
      end

      def update_statement
        <<-SQL.strip_heredoc
          UPDATE
            `publishing_composers`
          SET
            `publishing_composers`.`sync_opted_in` = 1,
            `publishing_composers`.`sync_opted_updated_at` = NOW()
          WHERE
            `publishing_composers`.`id` IN #{backfillable_record_ids}
        SQL
      end
    end

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    SyncOptInBackfiller.backfill

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
