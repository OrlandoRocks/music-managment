namespace :delete_after_use do
  task clone_album_products: :environment do
    Rails.logger.info "⏲ Beginning rake task... ⏲"

    products = Product.where(display_name: "1_yr", applies_to_product: ["Album", "Single"])

    products.each do |product|
      cloned_product = product.dup

      product_name =
        case product.name
        when "1 Year Single"
          "1 Year Free Single"
        when "1 Year Album"
          "1 Year Free Album"
        when "1-Jahres-Album"
          "1-Jahres-Kostenloses-Album"
        when "1-Jahres-Single"
          "1-Jahres-Kostenloses-Single"
        when "Album - 1 an"
          "Album gratuit - 1 an"
        when "Single - 1 an"
          "Single gratuit - 1 an"
        when "Album 1 anno"
          "Gratis Album 1 anno"
        when "Singolo 1 anno"
          "Gratis Singolo 1 anno"
        else
          "1 Year Free Release"
        end

      cloned_product.assign_attributes(
        original_product_id: product.id,
        status: "Inactive",
        name: product_name,
      )
      cloned_product.save!

      product_items = product.product_items

      product_items.each do |product_item|
        cloned_product_item = product_item.dup

        cloned_product_item.assign_attributes(
          product_id: cloned_product.id,
          does_not_renew: false,
          renewal_type: nil,
          first_renewal_duration: nil,
          first_renewal_interval: nil,
          renewal_duration: nil,
          renewal_interval: nil,
          renewal_product_id: nil,
        )

        cloned_product_item.save!

        product_item_rules = product_item.product_item_rules

        product_item_rules.each do |product_item_rule|
          cloned_product_item_rule = product_item_rule.dup

          cloned_product_item_rule_attributes =
            if ["Album", "Single"].include?(cloned_product_item_rule.inventory_type)
              { price: 0 }
            else
              {}
            end

          cloned_product_item_rule.assign_attributes(
            **cloned_product_item_rule_attributes,
            product_item_id: cloned_product_item.id,
          )

          cloned_product_item_rule.save!
        end
      end
    end

    Rails.logger.info "✅ Rake task completed! ✅"
  end
end
