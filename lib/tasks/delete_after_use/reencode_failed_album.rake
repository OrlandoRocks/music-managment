namespace :delete_after_use do
  task reencode_failed_album: :environment do
    Rails.logger.info "⏲ Beginning rake task... ⏲"

    album_id = 4_209_810

    album = Album.find(album_id)

    album.songs.each { |song| song.update(duration_in_seconds: nil, s3_flac_asset_id: nil) }

    AssetsBackfill::Base.new(album_ids: [album_id]).run

    Rails.logger.info "✅ Rake task completed! ✅"
  end
end
