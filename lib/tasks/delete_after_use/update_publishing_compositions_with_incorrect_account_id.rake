namespace :delete_after_use do
  task update_publishing_compositions_with_incorrect_account_id: :environment do
    s3_file_name = "publishing_composition_account_id_backfill.csv"
    RakeDatafileService.csv(s3_file_name, headers: true).each do |row|
      begin
        publishing_composer = PublishingComposer.find_by(provider_identifier: row["writer_code"])
        publishing_composition = PublishingComposition.find_by(provider_identifier: row["work_code"])
        publishing_composition.update(account_id: publishing_composer.account.id)
      rescue => e
        Rails.logger.error "#{row} Failed to update"
      end
    end
  end
end
