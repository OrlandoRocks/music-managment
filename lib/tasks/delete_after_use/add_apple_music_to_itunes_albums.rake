namespace :delete_after_use do
  task add_apple_music_to_itunes_albums: :environment do
    pp "⏲ Beginning rake task ⏲"

    albums = Album.joins(:salepoints).where(
      "
        salepoints.store_id = 36
        and
        salepoints.updated_at between '2021-12-23 00:00:00' and '2022-1-13 23:59:59'
        and
        albums.apple_music = 0
        and
        albums.payment_applied = 1
      "
    )

    albums.update_all(apple_music: true)

    pp "✅ Rake task completed ✅"
  end
end
