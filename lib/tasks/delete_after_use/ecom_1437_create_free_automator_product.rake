namespace :delete_after_use do 
  task create_free_automator_product: :environment do
    paid_automator_products = Product.where(display_name: "automator").where.not(price: 0)

    paid_automator_products.each do |product|
      product_attrs = product.attributes.except("id", "price", "updated_at", "created_at")
      product_tiem_attrs = product_attrs.merge!(price: 0)
      Rails.logger.info "Creating Free Product for Automator for Country website id #{product_attrs["country_website_id"]}"
      product = Product.find_or_create_by!(product_attrs)

      Rails.logger.info "Creating Free Product Item for Automator for Country website id #{product_attrs["country_website_id"]}"
      ProductItem.find_or_create_by!(product: product, currency: product.currency, name: product.name, description: product.description, price: product.price)
    end
  end
end