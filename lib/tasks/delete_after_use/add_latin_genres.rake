namespace :delete_after_use do
  desc "add latin genres"
  task add_latin_genres: :environment do
    latin_genres = ["Baladas", "Boleros", "Caribbean", "Cuban", "Latin Hip-Hop", "Latin Trap", "Latin Urban", "Ranchera", "Regional Mexicano", "Salsa/Merengue", "Tango", "Tropical"].freeze
    brazilian_genres = ["Axé", "Baile Funk", "Bossa Nova", "Chorinho", "Forró", "Frevo", "MPB", "Pagode", "Samba", "Sertanejo"].freeze

    genre = Genre.new({ name: "Brazilian", parent_id: 0, is_active: 0, you_tube_eligible: 1 })
    genre.id = 102
    genre.save

    new_id = 103

    latin_genres.each do |new_genre|
      genre = Genre.new({ name: new_genre, parent_id: 17, is_active: 0, you_tube_eligible: 1 })
      genre.id = new_id
      genre.save
      new_id += 1
    end
    brazilian_genres.each do |new_genre|
      genre = Genre.new({ name: new_genre, parent_id: 102, is_active: 0, you_tube_eligible: 1 })
      genre.id = new_id
      genre.save
      new_id += 1
    end
  end
end
