namespace :delete_after_use do
  desc "add ambient genre"
  task add_ambient_genre: :environment do
    genre = Genre.new({ name: "Ambient", parent_id: 0, is_active: 0, you_tube_eligible: 0 })
    genre.id = 101
    genre.save

    IneligibilityRule.create!({ property: "primary_genre_name", operator: "equal", value: "Ambient", store_id: 48 })
    IneligibilityRule.create!({ property: "primary_genre_name", operator: "equal", value: "Ambient", store_id: 83 })
  end
end
