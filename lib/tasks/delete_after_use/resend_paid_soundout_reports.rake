namespace :delete_after_use do
  task resend_paid_soundout_reports: :environment do
    reports = SoundoutReport.where(status: "paid", track_type: "Song", canceled_at: nil)
    Rails.logger.info("Attempting to resend SoundoutReports")
    Rails.logger.info("IDs: #{reports.pluck(:id)}")
    reports.each do |report|
      Rails.logger.info("Sending Report: #{report.id}")
      soundout_id = Soundout::Transcoder.new(soundout_report: report)
    end
    Rails.logger.info("Done resending reports.")
  end
end
