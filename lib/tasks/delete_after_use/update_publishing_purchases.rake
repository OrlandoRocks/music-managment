namespace :delete_after_use do
  task update_publishing_purchases: :environment do
    class PublishingPurchasesUpdater < Transient::PublishingChangeover::UnpaidPurchasesBackfiller
      def query_statement
        <<-SQL.strip_heredoc
          SELECT
            `purchases`.`id`
          FROM
            `purchases`
          WHERE
            `purchases`.`related_type` = 'Composer'
        SQL
      end
    end

    PublishingPurchasesUpdater.backfill

    Rails.logger.info("Rake task completed!")
  end

  task revert_update_publishing_purchases: :environment do
    s3_file_name = "GS-10032-purchases-query_result-trimmed.csv"

    RakeDatafileService.csv(s3_file_name, headers: true).each_with_index do |row, index|
      purchase_id = row["id"]
      related_id = row["related_id"]
      related_type = "Composer"

      Purchase.find_by(id: purchase_id)&.update_columns(related_id: related_id, related_type: related_type)
    end

    Rails.logger.info("Rake task completed!")
  end
end
