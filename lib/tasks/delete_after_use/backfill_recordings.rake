namespace :delete_after_use do
  task backfill_recordings: :environment do
    Rails.logger.info("Beginning rake task...")

    publishing_compositions = PublishingComposition
                              .joins(
                                "
                                  left outer join recordings
                                  on recordings.recording_code = publishing_compositions.provider_recording_id
                                  and
                                  recordings.publishing_composition_id = publishing_compositions.id
                                "
                              )
                              .where
                              .not(provider_recording_id: [nil, "", "00000000-0000-0000-0000-000000000000"])
                              .where(recordings: { id: nil })

    publishing_compositions.each do |publishing_composition|
      publishing_composition.recordings.find_or_create_by(
        recording_code: publishing_composition.provider_recording_id,
        recordable: publishing_composition.song
      )
    end

    Rails.logger.info("Finished running rake task!")
  end
end
