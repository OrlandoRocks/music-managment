namespace :delete_after_use do 
  task youtube_renewal_extension: :environment do
    class RenewalExtender < Transient::PublishingChangeover::BackfillerBase
      attr_accessor :expires_at, :parsed_expires_at

      def initialize(params = {})
        @expires_at = params[:expires_at] || Time.now + 1.years
        @parsed_expires_at = @expires_at.strftime('%Y-%m-%d %H:%M:%S').to_s
      end

      def update_statement
        <<-SQL.strip_heredoc
          UPDATE
            renewal_history
          SET
            renewal_history.expires_at = '#{parsed_expires_at}'
          WHERE
            renewal_history.id IN #{backfillable_record_ids}
        SQL
      end
    end

    class YouTubeOnlyRenewalExtender < RenewalExtender
      def query_statement
        <<-SQL.strip_heredoc
          SELECT 
            renewal_history.id
          FROM renewal_history
            INNER JOIN purchases
              ON
              purchases.id = renewal_history.purchase_id
            INNER JOIN albums
              ON
              purchases.related_id = albums.id
              AND
              purchases.related_type = 'Album'
            INNER JOIN salepoints 
              ON
              albums.id = salepoints.salepointable_id 
              AND
              salepoints.salepointable_type = 'Album'
            LEFT JOIN salepoints miscellaneous_salepoints
              ON
              miscellaneous_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              miscellaneous_salepoints.salepointable_type = 'Album' 
              AND
              miscellaneous_salepoints.store_id not in (28, 75,48,73)
              AND
              miscellaneous_salepoints.finalized_at IS NOT NULL
          WHERE
            salepoints.store_id = 28
            AND
            renewal_history.expires_at < '2022-06-01'
            AND
            albums.finalized_at IS NOT NULL
            AND
            salepoints.finalized_at IS NOT NULL
            AND
            miscellaneous_salepoints.id IS NULL
        SQL
      end
    end

    class YouTubeAndFacebookOnlyRenewalExtender < RenewalExtender
      def query_statement
        <<-SQL.strip_heredoc
          SELECT 
            renewal_history.id
          FROM renewal_history
            INNER JOIN purchases
              ON
              purchases.id = renewal_history.purchase_id
            INNER JOIN albums
              ON
              purchases.related_id = albums.id
              AND
              purchases.related_type = 'Album'
            INNER JOIN salepoints 
              ON
              albums.id = salepoints.salepointable_id 
              AND
              salepoints.salepointable_type = 'Album'
            INNER JOIN salepoints facebook_salepoints
              ON
              facebook_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              facebook_salepoints.salepointable_type = 'Album' 
              AND
              facebook_salepoints.store_id = 100
            LEFT JOIN salepoints miscellaneous_salepoints
              ON
              miscellaneous_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              miscellaneous_salepoints.salepointable_type = 'Album' 
              AND
              miscellaneous_salepoints.store_id not in (28, 100, 75,48,73)
              AND
              miscellaneous_salepoints.finalized_at IS NOT NULL
          WHERE
            salepoints.store_id = 28
            AND
            renewal_history.expires_at < '2022-06-01'
            AND
            facebook_salepoints.id IS NOT NULL
            AND
            albums.finalized_at IS NOT NULL
            AND
            salepoints.finalized_at IS NOT NULL
            AND
            facebook_salepoints.finalized_at IS NOT NULL
            AND
            miscellaneous_salepoints.id IS NULL
        SQL
      end
    end

    class YouTubeAndTikTokOnlyRenewalExtender < RenewalExtender
      def query_statement
        <<-SQL.strip_heredoc
          SELECT 
            renewal_history.id
          FROM renewal_history
            INNER JOIN purchases
              ON
              purchases.id = renewal_history.purchase_id
            INNER JOIN albums
              ON
              purchases.related_id = albums.id
              AND
              purchases.related_type = 'Album'
            INNER JOIN salepoints 
              ON
              albums.id = salepoints.salepointable_id 
              AND
              salepoints.salepointable_type = 'Album'
            INNER JOIN salepoints tiktok_salepoints
              ON
              tiktok_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              tiktok_salepoints.salepointable_type = 'Album' 
              AND
              tiktok_salepoints.store_id = 87
            LEFT JOIN salepoints miscellaneous_salepoints
              ON
              miscellaneous_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              miscellaneous_salepoints.salepointable_type = 'Album' 
              AND
              miscellaneous_salepoints.store_id not in (28, 87, 75,48,73)
              AND
              miscellaneous_salepoints.finalized_at IS NOT NULL
          WHERE
            salepoints.store_id = 28
            AND
            renewal_history.expires_at < '2022-06-01'
            AND
            tiktok_salepoints.id IS NOT NULL
            AND
            albums.finalized_at IS NOT NULL
            AND
            salepoints.finalized_at IS NOT NULL
            AND
            tiktok_salepoints.finalized_at IS NOT NULL
            AND
            miscellaneous_salepoints.id IS NULL
        SQL
      end
    end

    class YouTubeAndFacebookAndTikTokOnlyRenewalExtender < RenewalExtender
      def query_statement
        <<-SQL.strip_heredoc
          SELECT 
            renewal_history.id
          FROM renewal_history
            INNER JOIN purchases
              ON
              purchases.id = renewal_history.purchase_id
            INNER JOIN albums
              ON
              purchases.related_id = albums.id
              AND
              purchases.related_type = 'Album'
            INNER JOIN salepoints 
              ON
              albums.id = salepoints.salepointable_id 
              AND
              salepoints.salepointable_type = 'Album'
            INNER JOIN salepoints facebook_salepoints
              ON
              facebook_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              facebook_salepoints.salepointable_type = 'Album' 
              AND
              facebook_salepoints.store_id = 100
            INNER JOIN salepoints tiktok_salepoints
              ON
              tiktok_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              tiktok_salepoints.salepointable_type = 'Album' 
              AND
              tiktok_salepoints.store_id = 87
            LEFT JOIN salepoints miscellaneous_salepoints
              ON
              miscellaneous_salepoints.salepointable_id = salepoints.salepointable_id 
              AND
              miscellaneous_salepoints.salepointable_type = 'Album' 
              AND
              miscellaneous_salepoints.store_id not in (28, 87, 100, 75,48,73)
              AND
              miscellaneous_salepoints.finalized_at IS NOT NULL
          WHERE
            salepoints.store_id = 28
            AND
            renewal_history.expires_at < '2022-06-01'
            AND
            facebook_salepoints.id IS NOT NULL
            AND
            tiktok_salepoints.id IS NOT NULL
            AND
            albums.finalized_at IS NOT NULL
            AND
            salepoints.finalized_at IS NOT NULL
            AND
            facebook_salepoints.finalized_at IS NOT NULL
            AND
            tiktok_salepoints.finalized_at IS NOT NULL
            AND
            miscellaneous_salepoints.id IS NULL
        SQL
      end
    end

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    start_time = Time.now

    expires_at = DateTime.now + 1.years

    YouTubeOnlyRenewalExtender.backfill(expires_at: expires_at)

    YouTubeAndFacebookOnlyRenewalExtender.backfill(expires_at: expires_at)

    YouTubeAndTikTokOnlyRenewalExtender.backfill(expires_at: expires_at)

    YouTubeAndFacebookAndTikTokOnlyRenewalExtender.backfill(expires_at: expires_at)

    end_time = Time.now

    Rails.logger.info("✅ Rake task completed! ✅")

    Rails.logger.info("start_time: #{start_time}")
    Rails.logger.info("end_time: #{end_time}")
  end
end 
