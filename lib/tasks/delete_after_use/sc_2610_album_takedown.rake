namespace :delete_after_use do
  task expired_renewal_takedown: :environment do
    sql = "select distinct ri.related_id as album_id
    from renewals r
    join renewal_items ri on ri.renewal_id = r.id
    join (select renewal_id, max(expires_at) MaxDate from renewal_history
    where expires_at>='2022-03-01' group by 1) rh on rh.renewal_id = r.id
    left join person_plans pp on r.person_id = pp.person_id
    where ri.related_type = 'Album' and MaxDate < DATE_ADD(now(), interval -42 day)
    and (r.takedown_at is null and r.canceled_at is null)
    and pp.person_id is null"

    album_ids = ActiveRecord::Base.connection.exec_query(sql)

    takedowns_per_batch = ENV.fetch("TAKEDOWNS_PER_BATCH", 500)

    hours_from_now = 0
    
    opts = { 
      subject: "Automated Takedown", 
      note: "Release was taken down via rake task stemming from ticket SC-2610", 
      user_id: 0 
    }

    album_ids.each_slice(takedowns_per_batch) do |album_ids_slice|
      Takedown::BatchTakedownWorker.perform_in(hours_from_now.hours, album_ids_slice, true, opts)
      hours_from_now += 1
    end

    Rails.logger.info "Added #{hours_from_now} BatchTakedownWorker jobs to Sidekiq"
  end

  task remove_scheduled_batch_takedown_jobs: :environment do
    Rails.logger.info "Purging All Scheduled BatchTakedownWorker jobs from Sidekiq....."
    queue = Sidekiq::ScheduledSet.new
    job_count = 0
    queue.each do |job|
      if job.klass == "Takedown::BatchTakedownWorker"
        job_count += 1
        job.delete
      end
    end

    Rails.logger.info "#{job_count} BatchTakedownWorker jobs removed from Sidekiq"
  end
end
