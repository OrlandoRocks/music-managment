namespace :delete_after_use do
  task add_dolby_atmos_knowledgebase_link: :environment do
    KnowledgebaseLink.where(article_id: "10511913953812", article_slug: "dolby-atmos", route_segment: "/articles/").first_or_create
  end
end
