namespace :delete_after_use do 
  task update_wimp_short_name_to_tidal: :environment do 
    store = Store.find_by(short_name: "Wimp")
    raise "Cant find store!" unless store
    Rails.logger.info "updating store_id: #{store.id} short_name from 'Wimp' to 'Tidal'"
    store.update(short_name: "Tidal")
    Rails.logger.info "Store Updated to Tidal"
  end 

	#just in case something goes terribly wrong in tc-distributor
  task revert_wimp_short_name_to_tidal: :environment do 
    store = Store.find_by(short_name: "Tidal")
    raise "Cant find store!" unless store
    Rails.logger.info "Reverting store_id: #{store.id} short_name from 'Tidal' to 'Wimp'"
    store.update(short_name: "Wimp")
    Rails.logger.info "Store Updated to back to 'Wimp'"
  end 
end 
