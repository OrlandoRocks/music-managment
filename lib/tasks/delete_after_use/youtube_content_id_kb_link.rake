namespace :delete_after_use do
  desc 'Create YouTube Content ID KnowledgebaseLink'
  task youtube_content_id_kb_link: :environment do
    KnowledgebaseLink.find_or_create_by(
      article_id: 115006507207,
      article_slug: 'youtube-content-id',
      route_segment: '/articles/'
    )
    Rails.logger.info('Task completed successfully')
  end
end
