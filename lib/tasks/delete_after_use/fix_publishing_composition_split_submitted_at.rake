module DeleteAfterUse
  class PublishingCompositionSplitUpdatedAtBackfiller
    MAX_LIMIT = 5000

    attr_reader :migration_data, :joined_ids_for_query_statement, :backfillable_record_ids

    def self.backfill
      new.backfill
    end

    def initialize(params={}); end

    def backfill
      fetched_backfillable_record_ids = fetch_backfillable_record_ids.to_a.flatten

      return unless fetched_backfillable_record_ids.present?

      fetched_backfillable_record_ids.in_groups_of(max_limit, false) do |batch|
        @backfillable_record_ids = "(#{batch.join(',')})"

        execute_update
      end
    end

    private

    def query_statement
      <<-SQL.strip_heredoc
        SELECT
          `publishing_composition_splits`.`id`
        FROM
          `publishing_composition_splits`
        WHERE
          `publishing_composition_splits`.`updated_at` = `publishing_composition_splits`.`created_at`
        AND
          `publishing_composition_splits`.`legacy_publishing_split_id` IS NOT NULL
        AND
          `publishing_composition_splits`.`updated_at` BETWEEN "2021-01-14" AND "2021-01-15"
      SQL
    end

    def update_statement
      <<-SQL.strip_heredoc
        UPDATE
          `publishing_composition_splits`
        INNER JOIN
          `publishing_splits` ON `publishing_splits`.`id` = `publishing_composition_splits`.`legacy_publishing_split_id`
        SET
          `publishing_composition_splits`.`updated_at` = `publishing_splits`.`updated_at`
        WHERE
          `publishing_composition_splits`.`id` IN #{backfillable_record_ids}
      SQL
    end

    def max_limit
      MAX_LIMIT
    end

    def fetch_backfillable_record_ids
      ActiveRecord::Base.connection.execute(query_statement)
    end

    def execute_update
      ActiveRecord::Base.connection.execute(update_statement)
    end
  end
end

namespace :delete_after_use do
  task fix_publishing_composition_split_submitted_at: :environment do
    DeleteAfterUse::PublishingCompositionSplitUpdatedAtBackfiller.backfill
  end
end
