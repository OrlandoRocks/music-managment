namespace :delete_after_use do
    desc "Updates missing_splits_notifications url to the composers index"
    task :missing_splits_notifications_update_url => :environment do
        puts Rails.logger.info "Running notifications:missing_splits_notifications_update_url"
        MissingSplitsNotification.update_all(url: "/publishing_administration/composers")
        puts Rails.logger.info "Completed notifications:missing_splits_notifications_update_url"
    end
end