namespace :delete_after_use do
  task backfill_old_spotify_album_uris: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    spotify_service = SpotifyService.new

    albums = spotify_service
      .find_unlinked_spotify_distributions("Mon, 01 Mar 2021 22:17:36 UTC +00:00")
      .where(person_id: 3069267)

    spotify_service.search_by_upc(albums)

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
