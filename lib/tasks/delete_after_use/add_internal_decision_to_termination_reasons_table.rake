namespace :delete_after_use do
    task add_internal_decision_to_termination_reasons_table: :environment do
        PublishingTerminationReason.where(reason: "Internal Decision").first_or_create
        Rails.logger.info("Generated 'Internal Decision' termination reason.")
    end
end
