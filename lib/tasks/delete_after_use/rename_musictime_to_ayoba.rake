namespace :delete_after_use do
  task rename_musictime_to_ayoba: :environment do
    Rails.logger.info "⏲ Beginning rake task ⏲"

    store = Store.find(64)

    store.name = "Ayoba"

    if store.save
      Rails.logger.info "✅ Rake task completed ✅"
    else
      Rails.logger.info store.errors.messages
      Rails.logger.info "❌ Rake task failed ❌"
    end
  end
end
