namespace :delete_after_use do
  task clean_up_unncessary_publishing_compositions: :environment do
    account = Account.find_by(provider_account_id: "TUN0000015208A")
    unnecessary_publishing_composers = PublishingComposer.where(legacy_composer_id: [44120, 135939], account: account)

    unnecessary_publishing_composers.each do |publishing_composer|
      publishing_composer.publishing_compositions.where(is_unallocated_sum: false).each do |publishing_composition|
        publishing_composition.publishing_composition_splits.destroy_all

        publishing_composition.destroy
      end

      publishing_composer.destroy
    end
  end
end
