namespace :delete_after_use do
  task reencode_corrupted_flac_assets: :environment do
    Rails.logger.info "Beginning rake task..."
    album_ids = [1824345, 2715943, 2715950, 3758708, 3759689, 3763703, 3802244, 3822335, 3845918, 3857504, 3880769, 3883484, 3959886, 4008360, 4017559, 4028609, 4056137, 4061227, 4101149, 4103285, 4114367, 4134371, 4137023, 4142860, 4170835, 4187279]

    albums = Album.find(album_ids)

    albums.each do |album|
      album.songs.each do |song|
        next if song.s3_flac_asset.key.ends_with? ".flac"
        song.update(duration_in_seconds: nil, s3_flac_asset_id: nil)
      end
    end

    AssetsBackfill::Base.new(album_ids: album_ids).run

    Rails.logger.info "Rake task completed!"
  end
end
