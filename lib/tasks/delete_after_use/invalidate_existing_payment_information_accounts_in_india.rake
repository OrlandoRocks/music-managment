# frozen_string_literal: true

namespace :delete_after_use do
  task invalidate_existing_payment_information_accounts_in_india: :environment do
    Rails.logger.info "⏲ Beginning rake task... ⏲"

    logfile = File.new('tmp/ecom_582_invalidated_india_users', 'w+')
    StoredCreditCard.where.not(payments_os_token: nil).where(deleted_at: nil).in_batches do |batch|
      batch.each do |scc|
        person = scc.person
        ip_address = "3.220.67.230"
        logfile.puts(scc.id)
        scc.destroy(ip_address, person, { skip_validation: true })
      end
    end

    logfile.close
    Rails.logger.info "✅ Rake task completed! ✅"
  end
end

