namespace :delete_after_use do
  namespace :lyric do
    task add_missing_advances: :environment do
      # One time run, so data is hard coded
      advances = [{
        reference_id: "cznL4TCKx - b5db21bc-7e0b-4dce-9320-ca1b8b392f2d",
        person_id: 28420,
        reference_source: 'LYRIC',
        fee_amount: 35.0,
        total_amount: 315.0,
        created_at: '2020-11-12',
        updated_at: '2020-11-12'
      }, {
        reference_id: "B3CNxJxv9 - 96d6935d-29bd-4631-86f8-42a127cd3090",
        person_id: 1506733,
        reference_source: 'LYRIC',
        fee_amount: 56.0,
        total_amount: 456.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "p4vNWCQVY - 96d6935d-29bd-4631-86f8-42a127cd3090",
        person_id: 1506733,
        reference_source: 'LYRIC',
        fee_amount: 56.0,
        total_amount: 456.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "MJmarhhmx - 96d6935d-29bd-4631-86f8-42a127cd3090",
        person_id: 1506733,
        reference_source: 'LYRIC',
        fee_amount: 56.0,
        total_amount: 456.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "RKROJoCew - cae92360-9ed8-4ceb-af23-375257d29ffb",
        person_id: 1524662,
        reference_source: 'LYRIC',
        fee_amount: 53.0,
        total_amount: 426.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "ESQ3IkMau - 9aaefcd0-1d62-41f2-b089-d764b8b55ead",
        person_id: 1356705,
        reference_source: 'LYRIC',
        fee_amount: 62.0,
        total_amount: 602.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "wMeOQiQTX - 134b7074-2cba-4b0c-a561-23e090e51191",
        person_id: 1342456,
        reference_source: 'LYRIC',
        fee_amount: 342.0,
        total_amount: 3336.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "OycO0mzB1 - 0f6c98ad-1a12-4425-9c57-62786514a6d2",
        person_id: 33824,
        reference_source: 'LYRIC',
        fee_amount: 40.0,
        total_amount: 390.0,
        created_at: '2021-2-3',
        updated_at: '2021-2-3'
      }, {
        reference_id: "YUkyHUWTb - 038addb9-c881-44a9-b51c-3135557bddf4",
        person_id: 100137,
        reference_source: 'LYRIC',
        fee_amount: 112.0,
        total_amount: 912.0,
        created_at: '2021-2-4',
        updated_at: '2021-2-4'
      }]

      advances.each do |edata|
        encumbrance_summary = EncumbranceSummary.new(edata)
        encumbrance_summary.outstanding_amount = edata[:total_amount]
        encumbrance_summary.save
      end
    end
  end
end
