namespace :delete_after_use do
  task update_youtube_store_names: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")
    start_time = Time.now

    stores_data_array = [
      { # Youtube Music
        id: 28,
        name: "YouTube Music / YouTube Shorts",
      },
      { # YTSR
        id: 48,
        name: "YouTube Content ID Monetization",
      }
    ]

    stores_data_array.each do |data_hash|
      id = data_hash[:id]
      name = data_hash[:name]

      store = Store.find(id)
      store.update(name: name)
    end

    end_time = Time.now
    Rails.logger.info("✅ Rake task completed! ✅")
    Rails.logger.info("start_time: #{start_time}")
    Rails.logger.info("end_time: #{end_time}")
  end
end
