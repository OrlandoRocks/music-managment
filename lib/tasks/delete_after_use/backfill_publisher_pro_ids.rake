namespace :delete_after_use do
  task backfill_publisher_pro_ids: :environment do
    s3_file_name = "GS-10994-publisher-backfill-slimmed.csv"

    RakeDatafileService.csv(s3_file_name, headers: true).each_with_index do |row, index|
      id = row["id"]
      performing_rights_organization_id = row["performing_rights_organization_id"]

      publisher = Publisher.find_by(id: id)

      if publisher && publisher.performing_rights_organization_id.nil?
        publisher.update_columns(performing_rights_organization_id: performing_rights_organization_id)
      end
    end

    Rails.logger.info("Rake task completed")
  end
end
