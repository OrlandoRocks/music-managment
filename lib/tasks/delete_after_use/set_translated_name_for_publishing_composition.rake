namespace :delete_after_use do
    # RUN AND ANALYZE OUTPUT FROM A DRY RUN FIRST BEFORE DISABLING SAFETY FEATURES
    # rake delete_after_use:set_translated_name_for_publishing_composition\[<dry_run_enabled>,<publishing_composition_id>,<new_translated_name>\] 
    task :set_translated_name_for_publishing_composition, [:dry_run, :publishing_composition_id, :translated_name] => :environment do |_, args|
        # dry_run will not modify database records
        dry_run = ["false"].exclude?(args[:dry_run])
        publishing_composition_id = args[:publishing_composition_id].to_i
        translated_name = args[:translated_name]

        if publishing_composition_id.blank? || publishing_composition_id == 0 || translated_name.blank?
            Rails.logger.info "Error: You must provide a composition_id and translated name"
            Rails.logger.info "Re-run and add inputs <>: delete_after_use:set_translated_name_for_publishing_composition\[<dry_run_enabled>,<publishing_composition_id>,<new_translated_name>\]"
            return
        end

        publishing_composition = PublishingComposition.find_by(id: publishing_composition_id)
        publishing_composition.translated_name = translated_name

        ActiveRecord::Base.transaction do
            if !dry_run
                publishing_composition.save!
                publishing_composition.send_to_rights_app unless publishing_composition.provider_identifier.blank?
            end
        end

        Rails.logger.info "Result: #{publishing_composition.to_json}"
        Rails.logger.info "COMPLETED set_translated_name_for_publishing_composition"
        Rails.logger.info "dry_run = #{dry_run}, publishing_composition_id = #{publishing_composition_id}, translated_name = #{translated_name}"
    end
end
