namespace :db do
  desc "Returns the current schema version from schema_info. Usage: to get it, type 'rake db:version', to set it type 'rake db:version set=100'"
  task :version => :environment do
    current_vers = ActiveRecord::Migrator.current_version.to_s

    begin
      puts 'The current version is ' + ActiveRecord::Migrator.current_version.to_s
      if ENV['set']
        raise "This rake task no longer works, use Tunecore::DoctorTunecore.fill_migrations(#you want) instead"
        begin
          ActiveRecord::Base.connection.execute("UPDATE schema_info SET version=#{ENV['set']} WHERE version=#{current_vers}")      
          puts 'Changed the schema_info db migration version from ' + current_vers + ' to ' + ENV['set']
        rescue
          puts 'Failed changing the schema_info db migration version from ' + current_vers + ' to ' + ENV['set']
        end
      end
    rescue
      puts 'The database has not been migrated yet: ' + $!.to_s
      if ENV['set']
        begin
          ActiveRecord::Base.connection.execute("INSERT INTO schema_info (version) VALUES(#{ENV['set']})")      
          puts 'Set the schema_info db migration version to ' + ENV['set']
        rescue
          puts 'Failed setting the schema_info db migration version to ' + ENV['set']
        end
      end
    end
  end
end