namespace :automator do
  desc "Add salepoint to all the automator albums for a store"
  task add_salepoints: :environment do
    new_store_id  = ENV['STORE_ID']
    limit         = ENV['MAX_ALBUMS']   || nil
    csv_filepath  = ENV['CSV_FILEPATH'] || nil

    store = Store.find(new_store_id)
    unless store
      puts Rails.logger.warning "Store id of #{new_store_id} does not return a valid store"
      exit(0)
    end

    album_ids = []
    if csv_filepath
      album_ids = CSV.read(csv_filepath)
      album_ids.flatten!
    end

    salepoint_subscriptions = []
    if album_ids.blank?
      salepoint_subscriptions = SalepointSubscription.active_subscriptions_for_new_store(store, limit)
    else
      salepoint_subscriptions = SalepointSubscription.active_subscriptions_for_new_store(store, limit).where("albums.id IN (?)", album_ids)
    end

    puts Rails.logger.info "Start processing of #{salepoint_subscriptions.size} automator releases"

    salepoint_subscriptions.each do |ss|
      inventory_usages = ss.add_to_subscription([store], false)

      inventory_usages.each do |iu|
        puts "Inventory used=#{iu.inspect}"
      end
    end
  end

  desc "Distribute all the salepoints which were created through automator:add_salepoints"
  task distribute: :environment do
    limit = ENV['MAX_DISTRIBUTIONS'] || 5000
    store = Store.find(ENV['STORE_ID'])
    exit(0) unless store

    puts "Started creating distributions for salepoints #{store.short_name}"
    distributions = []
    Salepoint.includes(:distributions).where(store_id: store.id).find_each do |salepoint|
      next if salepoint.distributions.present? || salepoint.inventory_usages.blank? || salepoint.finalized_at.blank?
      items = InventoryItem.new({item: salepoint, inventory: salepoint.inventory_usages.last&.inventory})
      InventoryItem.distribute!([items])
      distributions << salepoint.id
      if limit.present? && distributions.count == limit.to_i
        puts "Initiated #{limit} distributions"
        exit(0)
      end
    end
    puts "Initiated #{distributions.count} distributions"

    puts "Finished creating distributions for store #{store.short_name}"
  end

  desc "Distribute all the salepoints via TC-Distributor which were created through automator:add_salepoints"
  task distribute_via_tc_distributor: :environment do
    limit = ENV['MAX_DISTRIBUTIONS'] || 5000
    store = Store.find(ENV['STORE_ID'])
    raise "Store Delivered on Legacy Tc-www, use automator:distribute instead" unless store && store.delivery_service == "tc_distributor"

    puts "Started creating SNS Messages for salepoints #{store.short_name} to deliver on TC-Distributor"

    distributions = []

    # this query should not include salepoints with distributions, without inventory_usages, and withour a finalized_at date
    # this query should also be set at the MAX_DISTRIBUTIONS limit
    salepoint_query = Salepoint
      .left_joins(:distributions)
      .left_joins(:inventory_usages)
      .where(
        store_id: store.id,
        distributions: { id: nil },
        distributions_salepoints: { distribution_id: nil }
      )
      .where.not(
        finalized_at: nil,
        inventory_usages: { id: nil }
      )
      .distinct

    limited_salepoint_query = salepoint_query.limit(limit)

    limited_salepoint_query.find_each do |salepoint|
      item = InventoryItem.new({item: salepoint, inventory: salepoint.inventory_usages.last&.inventory})
      InventoryItem.distribute!([item])

      Sns::Notifier.perform(
        topic_arn: ENV.fetch("SNS_RELEASE_NEW_TOPIC"),
        album_ids_or_upcs: [salepoint.salepointable_id],
        store_ids: [salepoint.store_id],
        delivery_type: "full_delivery"
      ) if salepoint.distributions.present?

      distributions << salepoint.id
    end

    puts "Initiated #{distributions.count} Deliveries"

    puts "Finished creating SNS Messages for store #{store.short_name}"

    remaining_salepoint_count = salepoint_query.count - distributions.count

    airbrake_message = "Finished creating SNS Messages for store #{store.short_name}! Initiated #{distributions.count} Deliveries. #{remaining_salepoint_count} distributions are remaining."

    Airbrake.notify(
      "Automator Status Update",
      {
        message: airbrake_message
      }
    )
  end
end
