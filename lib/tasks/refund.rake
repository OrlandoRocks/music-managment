namespace :refund do
  desc "Refunds all credit card or balance transactions where a user was charged twice"
  task :refund_double_charges => :environment do
    if ENV["FOR_REAL"]
      Refunder.refund_double_charges({start_date: ENV["START_DATE"], admin_id: ENV["ADMIN_ID"], for_real: ENV["FOR_REAL"]})
      ENV.delete("FOR_REAL")
    else
      puts "Starting dry run; set ENV['FOR_REAL'] = true and try again for the real thing."
      Refunder.refund_double_charges_dry_run({start_date: ENV["START_DATE"]})
    end
  end

  task :refund_double_charges_dry_run => :environment do
    Refunder.refund_double_charges_dry_run({start_date: ENV["START_DATE"]})
  end
end
