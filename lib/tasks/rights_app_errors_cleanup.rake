namespace :publishing_administration do
  task rights_app_errors_cleanup: :environment do
    old_errors = RightsAppError.over_sixty_days_old
    old_errors.destroy_all
  end
end
