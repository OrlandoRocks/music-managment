namespace :legacy_assets do
  desc "S3 assets for songs are pointing to legacy buckets like tunecore.com and
    do not have the required flac assets as per Believe standards. This task does the below functionality -
    1. If an asset is a flac asset
      a. Check if the asset is as per Tunecore standard using mediainfo system command.
      Believe support the following standards for flac assets.

      The Believe supports the following flac standards:
      RAW - FLAC - 16bits - 44100Hz
      RAW - FLAC - 24bits - 44100Hz
      RAW - FLAC - 24bits - 96000Hz
      RAW - FLAC - 24bits - 88200Hz
      RAW - FLAC - 24bits - 48000Hz
      RAW - FLAC - 24bits - 192000Hz
      b. If the asset is as per Tunecore standard, migrate the asset to bigbox.tunecore.com
      if not already on that bucket.
      c. Create a new entry to S3Asset table for flac asset on bigbox.tunecore.com if not already present.
      Link already existing or newly created entry in S3Asset table to songs table via s3_flac_asset_id in songs.
      An Existing flac asset might be linked via s3_asset_id or s3_orig_asset_id.
      d. Update the duration field in the songs table returned by mediainfo system command.
      e. If the asset is not as per Believe standard, consider it to be a non flac asset and process according to Step 2.
    2. If an asset is non flac asset or flac asset not following Tunecore standards
      a. Use tc-transcoder to process the asset. (tc-transcoder is responsible for converting the asset to flac
      asset as per standards and upload the asset to the destination bucket which in this case will be bigbox.tunecore.com)
      b. Create a S3Asset based on the key and bucket returned by tc-transcoder.
      c. Link this S3Asset to songs table via s3_flac_asset_id.
      d. Update the song duration returned by tc-transcoder in the songs duration field."
  task :update_legacy_assets, [:album_ids, :force] => [:environment] do |_t, args|
    args.with_defaults(:force => false)
    Rails.application.eager_load!
    AssetsBackfill::Base.new(
      album_ids: args.album_ids&.split(' '),
      force: args.force
    ).run
  end
end