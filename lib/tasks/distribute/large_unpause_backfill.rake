namespace :distribute do
  task large_unpause_backfill: :environment do
    limit = (ENV['MAX_ALBUMS'] || 5000).to_i
    store = Store.find_by(short_name: ENV['STORE_SHORT_NAME'])
    paused = 0

    Sidekiq::Queue.new("delivery-pause").each do |job|
      break if paused >= limit
      distro_id = job.args[1]
      if Distribution.joins(:salepoints).where(id: distro_id, salepoints: {store_id: store.id}).present?
        paused += 1
        job.delete
        Delivery::DistributionWorker.set(queue: "delivery-low").perform_async("Distribution", distro_id)
      end
    end
  end
end