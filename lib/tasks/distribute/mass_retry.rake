namespace :distribute do
  task mass_retry: :environment do
    MassRetryService.new(
      person_id: ENV["PERSON_ID"],
      job_name: ENV["JOB_NAME"],
      retry_limit: ENV["RETRY_LIMIT"],
      file_name: ENV["FILE_NAME"],
      priority: 0
    ).run
  end

  task priority_mass_retry: :environment do
    MassRetryService.new(
      person_id: ENV["PERSON_ID"],
      job_name: ENV["JOB_NAME"],
      retry_limit: ENV["RETRY_LIMIT"],
      file_name: ENV["FILE_NAME"],
      priority: 1
    ).run
  end

  task remove_batches_from_redis: :environment do
    raise "This task cannot be ran in production :(" if Rails.env.production?

    job_name = ENV.fetch("JOB_NAME")

    incomplete_batches = $redis.keys("*#{job_name}*")
    incomplete_batches.each do |batch_key|
      batch = $redis.hgetall(batch_key).with_indifferent_access
      uuid = batch[:uuid]
      $redis.del([batch_key, "distribution:batch-retry-albums:#{uuid}"])
      Rails.logger.info "Deleted batch for job name: #{batch[:job_name]}"
    end
  end
end
