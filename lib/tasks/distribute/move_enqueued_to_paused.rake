namespace :distribute do
  task move_enqueued_to_paused: :environment do
    Sidekiq::Queue.new("delivery-pause").each_slice(1000) do |slice|
      groups = slice.group_by { |job| job.args[0] }
      distribution_jobs = groups["Distribution"]
      distribution_song_jobs = groups["DistributionSong"]
      track_monetization_jobs = groups["TrackMonetization"]

      distributions = distribution_jobs.present? ? Distribution.where(id: distribution_jobs&.map { |job| job.args[1] }).where("state <> 'delivered'") : []
      distribution_songs = distribution_jobs.present? ? DistributionSong.where(id: distribution_song_jobs&.map { |job| job.args[1] }).where("state <> 'delivered'") : []
      track_monetizations = distribution_jobs.present? ? TrackMonetization.where(id: track_monetization_jobs&.map { |job| job.args[1] }).where("state <> 'delivered'") : []

      pause_function = ->(distribution) do
        distribution.pause(
          actor: "distribute:move_enqueued_to_pause",
          message: "clearing out sidekiq delivery-pause queue"
        )
      end

      distributions.each(&pause_function)
      distribution_songs.each(&pause_function)
      track_monetizations.each(&pause_function)
    end
  end

  task clear_sidekiq_paused_queue: :environment do
    Sidekiq::Queue.new("delivery-pause").clear
  end
end
