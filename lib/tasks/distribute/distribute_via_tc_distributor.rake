namespace :distribute do 
    task tc_distributor_retry: :environment do 
        csv_name = ENV.fetch('CSV_FILE_NAME')
        limit = ENV.fetch('LIMIT', 2500).to_i
        data = CSV.read(RakeDatafileService.download(csv_name).local_file)
        retry_data = data.slice!(1..limit)       

        #expecting format of CSV data:
        # ["album_id", "store_ids"]
        # ["2273785", "8,25,43,44,52,53,58,67,68,78,79,85,88,89,93,95"],
        # ["2273785", "8,25,43,44,52,53,58,67,68,78,79,85,88,89,93,95"]

        # data aquired via query in tc-distributor db:
        # SELECT r.album_id, group_concat(rs.store_id) 
        # FROM releases r
        # JOIN releases_stores rs 
        # ON r.id = rs.release_id
        # GROUP BY r.id

        retry_data.each do |row|
            Sns::Notifier.perform(
                topic_arn: ENV['SNS_RELEASE_RETRY_TOPIC'],
                album_ids_or_upcs: [row[0]],
                store_ids: row.last.split(","),
                delivery_type: ENV["delivery_type"] || "full_delivery"
            )
        end

        update_csv(data, csv_name)
    end

    def update_csv(data, csv_name)
       filepath = File.join(Rails.root, "tmp/", csv_name)
        CSV.open(filepath, "w") do |file|
            data.each { |row| file << row }
        end
        RakeDatafileService.upload(filepath, overwrite: 'y')
    end
end
