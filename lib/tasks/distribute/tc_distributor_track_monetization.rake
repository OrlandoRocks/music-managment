namespace :tc_distributor_track_monetization do
  task :deliver => :environment do |task|
    puts "Deliver Track Monetization to TC-Distributor"
    TrackMonetization::Deliverer.build.call
  end

  task :update_state => :environment do |task|
    puts "Update Track Monetization state from TC-Distributor"
    # NOTE: To avoid "414 Request-URI Too Large" errors we
    # are updating TrackMonetization state in batches of 100
    TrackMonetization.enqueued_tc_distributor_delivery.in_batches(of: 100) do |tracks|
      TrackMonetization::StateUpdater.new(track_monetizations: tracks).call
    end
  end
end
