namespace :distribute do
  task :retry_stalled  => :environment do
    Distribution::StalledRetryService.retry
  end
end
