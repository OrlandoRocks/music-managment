namespace :distribute do
  task uma_error_backfill: :environment do
    limit = ENV["LIMIT"]
    store_id = ENV["STORE_ID"]
    distributions = Distribution.joins(:salepoints)
                      .joins(petri_bundle: :album)
                      .where.not(albums: { finalized_at: nil})
                      .where(salepoints: { store_id: store_id })
                      .where(distributions: { state: 'error' })
                      .limit(limit)

    raise "UMA/BOOM Error Complete!" unless distributions.any?

    distributions.each do |distribution|
      distribution.retry(
        actor: "CRONTAB INTIATED JOB",
        message: "UMA/BOOM ERROR BACKFILL JOB GS-9885"
      )
    end
  end
end
