namespace :distribute do
  task store_music_island_backlog_in_redis: :environment do
    config = StoreDeliveryConfig.for("M_Island")
    config.update(delivery_queue: "delivery-low") unless config.delivery_queue == "delivery-low"
    store_id = config.store_id
    sidekiq = Sidekiq::Queue.new("delivery-default")

    Rails.logger.info "Fetching Enqueued Distribtuions for Music Island"
    enqueued_music_island_hash = Distribution.joins(:salepoints)
                                             .where(salepoints: { store_id: store_id})
                                             .where(state: 'enqueued')
                                             .group_by(&:id)

    Rails.logger.info "Storing #{enqueued_music_island_hash.count} Music Island Distributions in redis"

    new_job_hash = {}
    sidekiq.each do |job|
      id = job.args[1]
      if job.args.first == "Distribution" && enqueued_music_island_hash[id].present?
        new_job_hash[job.args[1]] = job.args
        job.delete
      end
    end

    new_job_hash.each do |key, value|
      $redis.rpush('music_island_backfill', [value])
    end
  end

  task :enqueue_music_island_distributions do
    limit = ENV.fetch('limit', '2500').to_i

    limit.times do
      item = $redis.lpop('music_island_backfill')
      raise "MUSIC ISLAND BACKFILL COMPLETE!" if item.nil?
      args = JSON.parse(item)
      Delivery::DistributionWorker.set(queue: "delivery-low").perform_async(args)
    end
  end

  task music_island_tc_distributor_backfill: :environment do 
    limit = ENV.fetch('limit', '2500').to_i
    store_id = Store.find_by(short_name: 'M_Island').id
    albums = Album.joins(salepoints: :distributions)
                  .where(salepoints: { store_id: store_id })
                  .where(distributions: { state: ["error", "enqueued"]})
                  .where("distributions.updated_at < '2021-01-30'")
                  .limit(limit)
    
    raise "MUSIC ISLAND BACKFILL COMPLETE!" if albums.empty?

    metadata_deliveries = albums.where(distributions: { delivery_type: "metadata_only" })
    metadata_params = {
      album_ids_or_upcs: metadata_deliveries.pluck(:id).uniq,
      store_ids: [store_id],
      delivery_type: "metadata_only"
    }

    Rails.logger.info("Sending #{metadata_deliveries.count} Metadata Updates to Tc-Distributor")
    send_sns_notification(metadata_params) if metadata_deliveries.any?

    full_deliveries = albums - metadata_deliveries
    full_delivery_params = {
      album_ids_or_upcs: full_deliveries.pluck(:id).uniq,
      store_ids: [store_id],
      delivery_type: "full_delivery"
    }

    Rails.logger.info("Sending #{full_deliveries.count} Full Deliveries to Tc-Distributor")
    send_sns_notification(full_delivery_params) if full_deliveries.any?

    distro_ids = albums.pluck("distributions.id")
    Distribution.where(id: distro_ids).update(state: "delivered_via_tc_distributor")
  end 

  def send_sns_notification(params)
    Sns::Notifier.perform(
      topic_arn: ENV['SNS_RELEASE_RETRY_TOPIC'],
      album_ids_or_upcs: params[:album_ids_or_upcs],
      store_ids: params[:store_ids],
      delivery_type: params[:delivery_type]
    )
  end
end
