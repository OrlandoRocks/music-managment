namespace :distribute do
  task mass_takedown: :environment do
    csv_key = ENV.fetch("CSV_KEY")
    store_ids = ENV.fetch("STORE_IDS", "").split(" ").map(&:to_i)
    album_takedown = ENV["ALBUM_TAKEDOWN"].to_s.downcase == "true"
    album_ids = CSV.read(RakeDatafileService.download(csv_key).local_file).flatten.map(&:to_i)
    msg = ENV.fetch("MSG", "")
    opts = { subject: "MASS TAKEDOWN RAKE", message: msg, priority: 1 }
    per_batch = ENV.fetch("PER_BATCH", 1000).to_i
    batch_name = ENV.fetch("BATCH_NAME")
    days_from_now = 0

    if album_takedown && store_ids.any?
      raise "CANNOT PROVIDE STORE_IDS WHEN TRIGGERING ALBUM LEVEL TAKEDOWNS"
    elsif !album_takedown && store_ids.empty?
      raise "STORE_IDS were not provided, ALBUM_TAKEDOWN was not specified \n
      If attempting album level takedown, set ALBUM_TAKEDOWN='true'"
    end

    album_ids.each_slice(per_batch) do |album_ids_slice|
      Takedown::MassTakedownWorker.perform_in(
        days_from_now.days,
        album_ids_slice,
        store_ids,
        batch_name,
        opts
      )
      days_from_now += 1
    end

    output_message = album_takedown ? "." : " for stores: #{store_ids}."
    Rails.logger.info "Enqueued Batch #{batch_name} with #{album_ids.count} total albums#{output_message} \n
    #{per_batch} albums will be process per day, the task will be completed on #{Time.now + days_from_now.days}"
  end

  task remove_mass_takedown_jobs: :environment do
    batch_name = ENV.fetch("BATCH_NAME")
    scheduled_set = Sidekiq::ScheduledSet.new
    deleted_count = 0

    scheduled_set.each do |job|
      # job.args[-2] is the arg for batch_name in MassTakedownWorker
      if job.klass == "Takedown::MassTakedownWorker" && job.args[-2] == batch_name
        job.delete
        deleted_count += 1
      end
    end
    Rails.logger.info "Deleted #{deleted_count} scheduled Takedown::MassTakedownWorker tasks for Batching Job #{batch_name}"
  end
end
