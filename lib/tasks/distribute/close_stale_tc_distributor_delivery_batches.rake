namespace :distribute do
  task close_stale_tc_distributor_delivery_batches: :environment do
    DistributorAPI::DeliveryBatch.close_stale_batches
  end
end
