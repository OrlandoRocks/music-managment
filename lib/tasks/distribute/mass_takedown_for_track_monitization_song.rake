# This rake task is for track monitized store
namespace :distribute do
  task :mass_takedown_for_track_monitization_song, [:store_ids, :monitized_store_id, :takedown_at] => :environment do |_, args|
    def validate_envs_and_args!(args)
      @store_ids = args.store_ids&.split(" ")
      raise "Please provide store_id seperated by space" if @store_ids.blank?

      @monitized_store_id = args.monitized_store_id&.split(" ")
      raise "Please provide monitized_store_id" if @monitized_store_id.blank?

      @takedown_at = args.takedown_at
      raise "Please provide takedown_at date" if @takedown_at.blank?

      person_id = ENV["PERSON_ID"]
      raise "Please set ENV['PERSON_ID']" if person_id.nil?

      @person = Person.find(person_id)
      raise "Person with ENV['PERSON_ID']: #{person_id} not found" if @person.nil?
    end

    validate_envs_and_args!(args)

    params = {
      takedown: true,
      delivery_type: "metadata_only",
      admin: @person
    }

    songs = TrackMonetization.get_song_data_of_takedown_album(@store_ids, @monitized_store_id, @takedown_at)
    process_song_count = 0

    p "Mass takedown of songs started for store: #{@monitized_store_id}"
    songs.find_each do |song|

      raise "Song.store_id is missing for song #{song.id}}" if song.store_id.nil?

      retry_params = params.merge({ song_ids_and_isrcs: song.id.to_s, store_ids: Array(song.tm_store_id) })
      mass_retry_songs_form = Admin::MassRetrySongsForm.new(retry_params)

      if mass_retry_songs_form.save
        p "Mass Takedown done for song: song/isrcs: #{song.id}"
        process_song_count += 1
      else
        error_message = mass_retry_songs_form&.errors&.full_messages&.join(',')
        p "Mass Takedown failed for song: song/isrcs: #{song.id}. Error: #{error_message}"
      end
    end
    p "Mass takedown rake task ended: Processed #{process_song_count}"
  end
end
