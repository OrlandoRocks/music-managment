namespace :audio_fingerprinting do
  desc "Backfill missing scrapi jobs by album finalized at date"
  task backfill_by_album_finalized_at_date: :environment do
    finalized_at_date = ENV["FINALIZED_AT_DATE"] || Date.yesterday

    puts "Starting backfill of Scrapi Jobs"

    Song
      .joins(:album)
      .left_joins(:scrapi_job)
      .where("albums.finalized_at >= '#{finalized_at_date}'")
      .where(scrapi_jobs: { id: nil })
      .distinct
      .each do |song|
      Scrapi::CreateJobWorker.perform_async(song.id)
    end

    puts "Completed backfill of Scrapi Jobs"
  end
end
