namespace :soundout do
desc "Fetch reports from SoundOut"
  task :retrieve_reports => :environment do
    Soundout.retrieve_reports
  end
end
