require "#{Rails.root}/lib/eft"

namespace :eft do
  include Logging
  include Eft
  @eft = "EFT Batch"

  desc 'Create an eft payment batch and send to Braintree'
  task create_batch: :environment do
    start_time = Time.now
    log(@eft, 'Start eft:create_batch Rake Task', time: start_time)
    #get transactions that are waiting for batch
    transactions = EftBatchTransaction.waiting
    #only process if there are transactions to process
    if transactions.present?
      batch = create_eft_batch(transactions)
      create_csv_from_batch(batch)
      upload_eft_csv_to_braintree(batch.id)
    else
      log(@eft, 'No Transactions to Process')
    end
    log(@eft, 'End eft:create_batch Rake Task', time: pretty_time_since(start_time))
  end

  desc 'Get invoices for autorenewals'
  task process_response: :environment do
    start_time = Time.now
    log(@eft, 'Start eft:process_response Rake Task', time: start_time)

    EftBatch
      .uploaded
      .unprocessed
      .sent_last_30_days
      .each do |batch|
      internal_path = download_eft_csv_from_braintree(batch.id)

      if File.exist?(internal_path)
        batch.update(response_processed_at: Time.now)
        process_eft_download_from_braintree(batch.id)
        EftNotifier.withdrawal_batch_summary(batch.id).deliver
      else
        log(@eft, 'EFT Response File did not download from braintree', batch: batch.id, file_received: batch.response_file_name)
      end
    end

    log(@eft, 'End eft:process_response Rake Task', time: pretty_time_since(start_time))
  end

  desc 'Query Braintree for successful responses'
  task query_braintree: :environment do
    dates_to_query = ENV['DATE'] ? [ENV['DATE']] : EftBatch.upload_confirmed_batch_dates
    dates_to_query.each { |date| query_eft_transactions(date) }
    mark_batches_as_complete
  end

  desc 'Query Braintree for successful responses'
  task update_batch_statuses: :environment do
    mark_batches_as_complete
  end
end
