namespace :tunecore do
  task :sentric_articles => :environment do
    KnowledgebaseLink.where(article_id: "115006503067").first_or_create(article_slug: "Why-do-I-need-to-be-affiliated-as-a-writer-with-a-Performing-Rights-Organization-PRO-Performing-Rights-Society-", route_segment: "/articles/")
    KnowledgebaseLink.where(article_id: "115006692828").first_or_create(article_slug: "What-are-splits-and-how-do-I-figure-out-my-split-", route_segment: "/articles/")
    KnowledgebaseLink.where(article_id: "115006692908").first_or_create(article_slug: "Can-I-submit-a-work-that-contains-a-sample-or-interpolation-of-a-copyrighted-composition-", route_segment: "/articles/")
  end
end
