namespace :delivery_service do
  desc "Update store delivery service"
  task :update, [:store_id, :delivery_service] => [:environment] do |command, args|
    if args.store_id.present? && args.delivery_service.present?
      store = Store.find_by(id: args.store_id)
      store.update!({
        delivery_service: args.delivery_service
      })
    else
      Rails.logger.error "Please provide store_id and delivery_service - Sample command `bundle exec rake delivery_service:update[1,believe]`"
    end
  end
end