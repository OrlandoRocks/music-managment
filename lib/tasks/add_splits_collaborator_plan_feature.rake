# frozen_string_literal: true

# bundle exec rake "plans:add_splits_collaborator"
namespace :plans do
  task :add_splits_collaborator => :environment do

    2.upto(5) do |plan_id|
      plan = Plan.find(plan_id)

      plan.plan_features << PlanFeature.find_or_create_by!(feature: "splits_collaborator")
    end
  end
end
