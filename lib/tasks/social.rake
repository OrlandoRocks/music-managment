desc "Process Social Renewals: cron 30 0 * * *"
namespace :social do
  task :renew_tunecore_subscriptions => :environment do
    Subscription::SocialRenewalWorker.perform_async("Tunecore")
  end

  task :renew_apple_subscriptions => :environment do
    Subscription::SocialRenewalWorker.perform_async("Apple")
  end

  task :renew_android_subscriptions => :environment do
    Subscription::SocialRenewalWorker.perform_async("Android")
  end
end
