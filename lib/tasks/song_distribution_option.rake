namespace :song_distribution_option do
  task update_option_value: :environment do
    name      = ENV['OPTION']
    value     = ENV['VALUE'].to_s
    upc_codes = File.readlines(ENV['FILE_PATH']).map(&:to_i)

    updater = SongDistributionOptionUpdater.update(
      option_name:  name, 
      option_value: value, 
      upc_codes:    upc_codes
    )

    if updater.errors.messages.present?
      puts 'ENV variable error:'
      updater.errors.full_messages.each { |msg| puts msg }
    else
      if updater.rejected_upc_codes.any?
        puts "UPC codes that were not updated:\n#{updater.rejected_upc_codes}\n"
      end

      puts 'Success'
    end
  end
end

