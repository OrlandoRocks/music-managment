namespace :youtube do
  task flac_asset_backfill: :environment do
    batch = ENV.fetch("BATCH").to_i
    csv_key = ENV.fetch("CSV_KEY")
    csv_data = CSV.read(RakeDatafileService.download(csv_key).local_file)

    raise "GS-10598 YOUTUBE FLAC ASSET BACKFILL COMPLETE!" if csv_data.empty?

    albums_songs = csv_data.shift(batch)
    album_ids = albums_songs.map(&:first)
    song_ids = albums_songs.map(&:last)

    yt_track_monetizations = TrackMonetization.where(song_id: song_ids, store_id: Store::YTSR_STORE_ID)
    Rails.logger.info "Enqueuing #{yt_track_monetizations.count} Tracks for YTSR"
    yt_track_monetizations.each do |track|
      Delivery::DistributionWorker.perform_async(track.class.name, track.id, "full_delivery")
    end

    yt_albums = Distribution.joins(:salepoints).where(salepoints: { store_id: Store::GOOGLE_STORE_ID, salepointable_id: album_ids, salepointable_type: "Album" })
    Rails.logger.info "Enqueuing #{yt_albums.count} Albums for Youtube Music"
    yt_albums.each do |album|
      Delivery::DistributionWorker.perform_async(album.class.name, album.id, "full_delivery")
    end

    filepath = File.join(Rails.root, "tmp/", csv_key)
    CSV.open(filepath, "w") do |file|
      csv_data.each { |row| file << row }
    end

    RakeDatafileService.upload(filepath, overwrite: 'y')
  end
end
