# CALL: rake country_sub_region_data:populate_states IN
namespace :country_sub_region_data do
  desc "populates state data in the country_states table"
  task populate_states: :environment do
    states_data_file = fetch_data_file
    if states_ready_to_upload?(states_data_file)
      puts "States Are Being Uploaded"
      states_upload_log = upload_states_data_to_database(states_data_file)
      populate_log(states_upload_log, 'states')
    end
  end

  desc "populates state_city data in the country_state_cities table"
  task populate_cities: :environment do
    cities_data_file = fetch_data_file
    if cities_ready_to_upload?(cities_data_file)
      puts "Cities Are Being Uploaded"
      cities_upload_log = upload_cities_data_to_database(cities_data_file)
      populate_log(cities_upload_log, 'cities')
      remove_azad_kashmir
    end
  end

  desc "removes duplicate city-state entries while preserving alt names"
  task remove_duplicate_cities: :environment do
    if cities_data_already_exists?
      remove_duplicate_cities
      remove_azad_kashmir
    end
  end

  desc "remove Azad Kashmir state and associated cities from db"
  task remove_azad_kashmir: :environment do
    if states_data_already_exists? && cities_data_already_exists?
      remove_azad_kashmir
    end
  end


  def populate_log(log_data, data_type)
    File.open("tmp/country_#{data_type}_upload_log_#{DateTime.now.strftime('%m_%d_%Y')}_#{SecureRandom.hex(5)}", 'w+') do |log_file_ref|
      log_data.map do |log_data_row|
        log_file_ref.puts(log_data_row)
      end
    end
  end

  def fetch_data_file
    CSV.read(ARGV[1], headers: true) if ARGV[1].present? && File.exist?(ARGV[1])
  end

  def target_country
    Country.find_by(iso_code: ARGV.fetch(2, "IN"))
  end

  def normalize_state_type(state_type)
    state_type.to_s.downcase.gsub(/\s+/, '_')
  end

  def upload_states_data_to_database(states_data)
    states_data.map do |state_datum|
      created_state = target_country.country_states.create(
        name: I18n.transliterate(state_datum['name']),
        iso_code: state_datum['iso'],
        entity_type: normalize_state_type(state_datum['type'])
      )
      upload_status = created_state.errors.blank? ? "SUCCESS" : "FAILED"
      "#{upload_status}: #{state_datum['name']} - #{state_datum['iso']}"
    end
  end

  def upload_cities_data_to_database(cities_data)
    cities_data.map do |city_datum|
      state = target_country.country_states.find_by(iso_code: city_datum['state_iso'])
      next if state.blank?

      created_city = state.country_state_cities.create(
        name: I18n.transliterate(city_datum['city']),
        alt_name: city_datum['city_alt']
      )
      upload_status = created_city.errors.blank? ? "SUCCESS" : "FAILED"
      "#{upload_status}: #{city_datum['city']} - #{city_datum['state']}"
    end
  end

  def remove_duplicate_cities
    sql1 = <<-SQL
      DELETE t1
      FROM country_state_cities t1
      INNER JOIN (
        SELECT name, state_id, max(id) as id
        FROM country_state_cities
        WHERE alt_name is not null
        GROUP BY state_id, name
        ) t2
        ON t1.name = t2.name AND
          t1.state_id = t2.state_id
      WHERE
        t1.id != t2.id;
    SQL

    sql2 = <<-SQL
      DELETE t1
      FROM country_state_cities t1
      INNER JOIN (
        SELECT name, state_id, max(id) as id 
        FROM country_state_cities 
        GROUP BY state_id, name
        ) t2 
        ON t1.name = t2.name AND
          t1.state_id = t2.state_id
      WHERE
          t1.id < t2.id;
    SQL

    puts "Deleting all duplicate country_state_cities"
    puts "# of entries in country_state_cities #{CountryStateCity.count}"
    CountryStateCity.connection.execute(sql1)
    CountryStateCity.connection.execute(sql2)
    puts "Duplicate country_state_cities deleted"
    puts "# of entries in country_state_cities #{CountryStateCity.count}"
  end

  def remove_azad_kashmir
    azad_kashmir = target_country.country_states.find_by(iso_code: "PK-JK")
    
    if azad_kashmir
      move_azad_kashmir_cities_to_jammu_and_kashmir(azad_kashmir)
      azad_kashmir.reload
      azad_kashmir.destroy
    end
  end

  def move_azad_kashmir_cities_to_jammu_and_kashmir(azad_kashmir)
    jammu_and_kashmir = target_country.country_states.find_by(iso_code: "IN-JK")

    azad_kashmir.country_state_cities.each do |city| 
      city.country_state = jammu_and_kashmir
      city.save
    end
  end

  def states_ready_to_upload?(states_data_file)
    states_data_file.present? && valid_states_data?(states_data_file) && !states_data_already_exists?
  end

  def cities_ready_to_upload?(cities_data_file)
    cities_data_file.present? && valid_cities_data?(cities_data_file) && !cities_data_already_exists?
  end

  def states_data_file_headers
    %w[name iso type].sort
  end

  def cities_data_file_headers
    %w[city city_alt state state_iso state_type].sort
  end

  def valid_states_data?(states_data_file)
    states_data_file.headers.sort == states_data_file_headers
  end

  def valid_cities_data?(cities_data_file)
    cities_data_file.headers.sort == cities_data_file_headers
  end

  def states_data_already_exists?
    Country.find_by(iso_code: "IN").country_states.present?
  end

  def cities_data_already_exists?
    Country.find_by(iso_code: "IN").country_state_cities.present?
  end
end
