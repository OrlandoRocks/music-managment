namespace :tc_distributor_cutover_stores do
  desc "Add store to TC_DISTRIBUTOR_CUTOVER_STORES to do delivery via tc_distributor based on
    TC_DISTRIBUTOR_ALBUM_CUTOVER_USERS/TC_DISTRIBUTOR_TRACK_CUTOVER_USERS"
  task :add, [:store_id] => [:environment] do |command, args|
    $redis.sadd(:TC_DISTRIBUTOR_CUTOVER_STORES, args.store_id) if args.store_id.present?
  end

  desc "Remove store from TC_DISTRIBUTOR_CUTOVER_STORES"
  task :remove, [:store_id] => [:environment] do |command, args|
    $redis.srem(:TC_DISTRIBUTOR_CUTOVER_STORES, args.store_id) if args.store_id.present?
  end

  desc "Display all TC_DISTRIBUTOR_CUTOVER_STORES"
  task all: :environment do
    puts $redis.smembers(:TC_DISTRIBUTOR_CUTOVER_STORES)
  end
end
