module Publishing
  class DataUpdate
    def self.backfill_lod(composer, rs_document)
      return if composer.lod #prevent accidentally overwriting existing lod

      composer = Composer.find(composer.id)
      composer.create_lod(:last_status => 'imported',
        :document_guid => rs_document['guid'], :template_guid => Lod::TEMPLATE_ID,
        :raw_response => rs_document)
      composer.save(:validate=>false) #to get pass the stupid validation
      composer.lod.mark_as_sent_to_customer
      composer.lod.update_attribute(:last_status_at, rs_document['created_at'])
      if rs_document['state'] == 'signed'
        composer.lod.mark_as_signed_by_customer
        composer.lod.update_attribute(:last_status_at, rs_document['completed_at'])
      end

    end
  end
end

namespace :publishing do
  desc "Set publishing split values for a csv of composition IDs to a target percent"
  task :update_splits_from_csv => :environment do
    filename = ENV["COMPOSITION_CSV"] || raise("Missing variable COMPOSITION_CSV")
    target_percent = ENV["TARGET_PERCENT"].to_i || raise("Missing variable TARGET_PERCENT")
    Rails.logger.info "Setting publishing splits for compositions in #{filename} to #{target_percent} percent"
    puts "Setting publishing splits for compositions in #{filename} to #{target_percent} percent"

    CSV.foreach(filename) do |line|
      begin
        composition_id = line[0].to_i
        psa = PublishingSplit.where(:composition_id => composition_id)
        if psa.nil?
          Rails.logger.info "No publishing split found for composition id #{composition_id}"
          puts "No publishing split found for composition id #{composition_id}"
        else
          psa.each do |ps|
            ps.percent = target_percent
            ps.save
            Rails.logger.info "Publishing split #{ps.id} set to #{target_percent}"
            puts "Publishing split #{ps.id} set to #{target_percent}"
          end
        end
      rescue StandardError => e
        Rails.logger.info e
      end
    end
  end

  desc "Resync NTC compositions that were wrongly put in 'pending_distribution' state"
  task resync_pending_distro_compositions: :environment do
    def log_sync_error(composition, error)
      Rails.logger.error("*****\nProblem processing composition #{composition.id}\n*****\n")
      Rails.logger.error(error)
    end

    compositions = Composition.joins(:non_tunecore_songs).where(compositions: {state: 'pending_distribution'})

    Rails.logger.info("Processing #{compositions.count} compositions")

    result_count = 0

    Parallel.each(compositions, in_threads: 10) do |composition|
      ActiveRecord::Base.connection_pool.with_connection do
        work_exists = true

        if composition.provider_identifier
          #Update Works that already exist.
          begin
            #If the next line doesn't find produce a work, it throws an exception, so it won't make it to WorkListService.list line,
            # and will proceed to create the work.
            response = PublishingAdministration::ApiClientServices::GetWorkService.get_work(composition.provider_identifier)
            if response && response["Error"]
              log_sync_error(composition, response["Error"])
              next
            else
              PublishingAdministration::WorkListService.list(composition.account, true)
            end
          rescue PublishingAdministration::ErrorService::ApiError => e
            if e.message.include?("No work found")
              work_exists = false
            else
              log_sync_error(composition, e.message)
              next
            end
          rescue => e
            log_sync_error(composition, e.message)
            next
          end

          next if work_exists
        end

        #Create works that don't exist.
        begin
          PublishingAdministration::ApiClientServices::WorkCreationService.post_work({composition: composition, composer: composition.composer})
        rescue => e
          log_sync_error(composition, e.message)
        end
      end

      if (result_count += 1) % 500 == 0
        Rails.logger.info("*****\nProcessed #{result_count} compositions.\n*****\n")
      end
    end

    Rails.logger.info("Done syncing #{result_count} compositions.")
  end

  desc  "Upload Royalty Payment PDFS and CSVS"
  task :upload_royalty_payment_statements => :environment do

    #Check if the directory exists
    if !File.directory? ROYALTY_PDF_DIRECTORY
      Dir.mkdir(ROYALTY_PDF_DIRECTORY)
    end

    # get date for path for csv's
    time = Time.current
    year = time.year
    month = time.month
    quarter = (month.to_f / 3).ceil - 1

    if quarter == 0
      year -= 1
      quarter = 4
    end

    # process pdfs
    if system("sh #{Rails.root}/script/import/quarterly_publishing_statement_retriever#{Rails.env.production? ? '' : '_qa'}.sh #{quarter} #{year}")
      Rails.logger.info "PDFs and CSVs successfully pulled from S3 and dropped into #{Rails.root}/#{ROYALTY_PDF_DIRECTORY}#{year}/#{quarter}"
    else
      Rails.logger.info "Error. Could not retrieve/extract PDFs or CSVs"
    end

    Dir.chdir(ROYALTY_PDF_DIRECTORY)
    pdf_files = File.join("**", "*.pdf")
    pdf_files = Dir.glob(pdf_files)
    csv_files = File.join("**", "*.csv")
    csv_files = Dir.glob(csv_files)
    pdf_hash = create_file_hash(pdf_files)
    csv_hash = create_file_hash(csv_files)

    Rails.logger.info "Created File Hashes"

    #Check that all pdfs have a matching csv
    unmatched_pdfs, unmatched_csvs = find_unmatched_files(pdf_hash, csv_hash)

    Rails.logger.info "Checked File Hashes for missing files"

    if unmatched_pdfs.length > 0 || unmatched_csvs.length > 0
      Rails.logger.info "Error: Must have matching PDF and CSV files for all users. Aborting task."
      puts "Error: Must have matching PDF and CSV files for all users. Aborting task."
      next
    end

    #Loop through pdf files
    pdf_hash.each do |person_id, values|
      csv_values = csv_hash[person_id]

      #Make sure the person exists
      person = Person.find_by(id: person_id)

      if person
        attributes = {:period_type => "quarterly", :period_interval => quarter,
          :period_year => year, :person_id => person.id}
        attributes.merge!({:code => values[:code]}) if values[:code]

        composer = person.composers.first
        if composer
          attributes.merge!({:composer_id => composer.id})
        else
          puts "Warning: Composer ID not found for person #{person.id}"
          Rails.logger.info "Composer ID not found for person #{person.id}"
        end

        #Check to see if history of the royalty_payment exists
        royalty_payment = RoyaltyPayment.where(attributes).first

        if royalty_payment
          Rails.logger.info "Updating royalty payment for #{attributes.inspect}"
          puts "Updating royalty payment for #{attributes.inspect}"
        else
          #Create a new royalty payment with a blank amount

          royalty_payment = RoyaltyPayment.create(attributes)
          Rails.logger.info "Creating royalty payment for #{attributes.inspect}"
          puts "Creating royalty payment for #{attributes.inspect}"
        end

        royalty_payment.pdf_summary = File.open(values[:file_path])
        royalty_payment.csv_summary = File.open(csv_values[:file_path])
        royalty_payment.save
      else
        Rails.logger.info "Could not find person_id #{person_id}"
        puts "Could not find person_id #{person_id}"
      end
    end
  end

  def create_file_hash(file_array)
    Rails.logger.info "Creating file hashes"
    hash = {}
    file_array.each_with_index do |path, i|
      parts = path.split("/")
      if parts.size == 4 &&
         parts[0].match("^[1-9][0-9][0-9][0-9]$") != nil &&
         parts[1].match("^Q[1-4]$") != nil

        #Pull out parts
        year      = parts[0]
        quarter   = parts[1][-1,1]
        file_name = parts[3]

        #Pull the person id from the file name
        person_id_format = file_name.match("([0-9]*)(?:[_]([A-Z0-9]*))?")

        if person_id_format
          person_id = person_id_format[1].to_s
          code = person_id_format[2]
          hash[person_id] = {
            file_path: path,
            code: code
          }
        else
          Rails.logger.info "File name format is invalid for #{file_name}"
          puts "File name format is invalid for #{file_name}"
          next
        end
      else
        #Log that the file structure is incorrect
        Rails.logger.info "Directory structure is incorrect.  File must exist in folder YYYY/Q(1-4)/pdfs/person_id.pdf or YYYY/Q(1-4)/csvs/person_id.csv"
        puts "Directory structure is incorrect.  File must exist in folder YYYY/Q(1-4)/pdfs/person_id.pdf or YYYY/Q(1-4)/csvs/person_id.csv"
      end
    end

    return hash
  end

  def find_unmatched_files(pdf_hash, csv_hash)
    unmatched_pdfs = []
    pdf_hash.each do |person_id, values|
      unless csv_hash[person_id]
        unmatched_pdfs << person_id
        Rails.logger.info "CSV file missing for user #{person_id}"
        puts "CSV file missing for user #{person_id}"
      end
    end

    unmatched_csvs = []
    csv_hash.each do |person_id, values|
      unless pdf_hash[person_id]
        unmatched_csvs << person_id
        Rails.logger.info "PDF file missing for user #{person_id}"
        puts "PDF file missing for user #{person_id}"
      end
    end

    return unmatched_pdfs, unmatched_csvs
  end

  namespace :reports do
    desc "Generates composer summary in excel"
    task :generate_composer_summary => :environment do
      log("publishing:generate_composer_summary","Start generating summary")
      filename = Composer.generate_summary_xls
      log("publishing:generate_composer_summary", "Completed summary filename=#{filename}")
    end

  end

  namespace :data_updates do
    desc "Backfill 'unallocated sums' compositions"
    task :backfill_unallocated_sum_compositions => :environment do
      composers = Composer.with_no_unallocated_sum_composition
      puts "Creating unallocated sum composition for #{composers.count} composers"
      composers.each do |composer|
        puts "Creating for #{composer.email}"
        composer.send :create_unallocated_sum_composition
      end

      puts "Succesfully updated all composers" if Composer.with_no_unallocated_sum_composition.count == 0
    end

    desc "Rename 'unallocated sums' compositions to the new format"
    task :rename_unallocated_sum_compositions => :environment do
      # rename previously created unallocated sums composition based on the new requirement
      update_compositions = Composition.joins(:publishing_splits => :composer).where(:is_unallocated_sum => true).includes(:publishing_splits => :composer)

      puts "Updating #{update_compositions.count} unallocated sum composition's name"
      update_compositions.each do |uc|
        new_name = uc.publishing_splits.first.composer.send(:unallocated_sums_composition_name)
        puts "Updating composition from #{uc.name} to #{new_name}"
        Composition.update(uc.id, :name => new_name)
      end
    end

    desc "Backfill Composition#cwr_name"
    task :backfill_compositions_cwr_name => :environment do
      Composition.record_timestamps = false
      compositions = Composition.where("cwr_name is null or cwr_name != name") #, :conditions => "cwr_name is NULL")
      puts "Updating #{compositions.count} compositions"
      compositions.each do |c|
        c.send :save_cwr_name
        if c.save
          puts "Updated composition's cwr_name to #{c.cwr_name}"
        end
      end
      Composition.record_timestamps = true
      puts "Successfully updated #{compositions.count} compositions"
    end

    desc "Generate cwr name report"
    task :generate_cwr_name_report => :environment do
      compositions_with_name_changed = Composition.where("cwr_name != name")
      compositions_without_name_changed = Composition.where("cwr_name = name")
      output_filename = Rails.root + "tmp/cwr_name_report.xlsx"

      title = compositions_with_name_changed.first.attributes.keys
      SimpleXlsx::Serializer.new(output_filename) do |doc|
        doc.add_sheet('CWR Name Changed') do |sheet|
          sheet.add_row(title)
          compositions_with_name_changed.each do |composition|
            sheet.add_row(composition.attributes.values)
          end
        end

        doc.add_sheet('CWR Name Unchanged') do |sheet|
          sheet.add_row(title)
          compositions_without_name_changed.each do |composition|
            sheet.add_row(composition.attributes.values)
          end
        end
      end

    end

    desc "Update royalty hold payment type"
    task :update_hold_payment_type => :environment do
      rp = RoyaltyPayment.joins("inner join balance_adjustments on posting_type = 'BalanceAdjustment' and posting_id = balance_adjustments.id").where("balance_adjustments.credit_amount = 0").readonly(false)

      rp.each do |r|
        r.update(:hold_payment => true, :hold_type => 'missing_tax')
      end

      updated_rp_count = RoyaltyPayment.where(hold_payment: true, hold_type: 'missing_tax').count

      puts "Successfully updated #{updated_rp_count} records" if rp.size == updated_rp_count
    end

  end
end
