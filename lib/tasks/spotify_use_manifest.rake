namespace :transient do
  task spotify_manifest_flag: :environment do
    sdc = StoreDeliveryConfig.readonly(false).for("Spotify")
    sdc.update(use_manifest: true)
  end
end