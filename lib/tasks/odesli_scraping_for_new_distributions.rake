namespace :odesli_scraping_for_new_distributions do
  # rubocop:disable Lint/NumberConversion
  distribution_check_days = ENV.fetch("DISTRIBUTION_CHECK_DAYS", 2).to_i
  # rubocop:enable Lint/NumberConversion

  desc "fetch other stores' release identifiers using existing spotify and apple song urls - for latest distributions"
  task fetch_new_stores_with_odesli: :environment do
    odesli_scraping_logger
    last_update_check_date = (Date.current - distribution_check_days.days)
    @logger.info "Distribution check date: #{last_update_check_date}"
    # Conditions for scraping: Find newly distributed
    # Albums of TCS pro users, albums having
    # takedown_at value as nil
    newly_distributed_albums = Album.distinct.joins(:distributions)
                                    .joins(person: :person_subscription_statuses)
                                    .where(person_subscription_statuses: { subscription_type: "Social" })
                                    .where.not("person_subscription_statuses.effective_date" => nil)
                                    .where("person_subscription_statuses.termination_date IS NULL
                                            OR person_subscription_statuses.termination_date > NOW()")
                                    .where("Date(distributions.created_at) > ?", last_update_check_date)
                                    .where(albums: { takedown_at: nil })
    @logger.info "TCS pro users newly distributed albums count: #{newly_distributed_albums.count} date: #{Date.current}"
    @scrape_count = 0
    batch_size = 1000
    newly_distributed_albums.each_slice(batch_size) do |new_distr_album_batch|
      break if odesli_limit_reached?

      new_distr_album_batch.each do |album|
        break if odesli_limit_reached?

        @logger.info "Next Album scraping: #{album.id}"
        song_ids = album.songs.ids
        @logger.info "Album: #{album.id} songs count: #{song_ids.count}" if song_ids.count.positive?
        song_ids.each do |song_id|
          break if odesli_limit_reached?

          @logger.info "Next song scraping: #{song_id}"
          song = Song.find(song_id)
          source = ENV["NIGHTLY_RAKE_NEW_DISTROS"]
          @scrape_count = Api::Linkshare::FetchStoresService.new(song, source, @scrape_count).fetch_odesli_url
        end
      end
    end
  end

  def odesli_limit_reached?
    # Odesli API call limit of 5000/day
    @scrape_count >= 5000
  end

  def odesli_scraping_logger
    @logger = Logger.new("log/odesli_nightly_scraping_logger.log")
    @logger.level = Logger::INFO
  end
end
