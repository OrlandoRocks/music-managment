namespace :youtubesr do
  desc "Backfill nil effective_dates for valid YTSR"
  task :backfill_effective_date => :environment do |t|
    log(t,'Begin backfilling YoutubeMonetization effective_date')
    YoutubeMonetization.
      joins(:purchase).
      where.not(purchases: {invoice_id: nil}).
      where(youtube_monetizations: {effective_date: nil, termination_date: nil}).
      update_all('effective_date = agreed_to_terms_at')
    log(t,'End backfilling YoutubeMonetization effective_date')
  end

  desc "Backfill salepointable_id for YouTubeSR salepoints"
  task :backfill_salepointable_id => :environment do |t|
    log(t,'Begin backfilling salepointable_id')

    #backfill missing salepointable type/id for salepoints with store_id = 48
    Salepoint.joins("inner join distributions_salepoints on salepoints.id = distributions_salepoints.salepoint_id
      inner join distributions on distributions_salepoints.distribution_id = distributions.id
      inner join petri_bundles on distributions.petri_bundle_id = petri_bundles.id and petri_bundles.state != 'dismissed'
      inner join albums on petri_bundles.album_id = albums.id")
    .where("salepoints.store_id = 48 and salepoints.salepointable_id IS NULL")
    .update_all("salepointable_id = albums.id, salepointable_type = 'Album'")
    log(t, 'End backfilling salepointable_id')
  end


  desc "Backfill the SalepointSongs with tracks that have already been reviewed by Publishing"
  task :backfill_salepoint_and_distro_songs => :environment do |t|
  end

  desc "Backfill distribution_songs based on distributions table"
  task :backfill_from_distribution => :environment do |t|
  end

  desc "Update DistributionSong with correct delivery_state"
  task :update_distro_song_state => :environment do |t|
  end

  desc "Backfill missing salepoints"
  task :backfill_salepoints => :environment do |t|
    unless ENV['PERSON_ID']
      puts "please pass a PERSON_ID or 'all'"
      exit(0)
    end
    log(t, 'Begin backfilling')
    realtime = Benchmark.realtime do
      conditions = "((albums.takedown_at IS NULL AND albums.is_deleted = 0)
        OR salepoint_songs.id IS NOT NULL) AND albums.payment_applied = 1 AND albums.album_type != 'Ringtone'
        AND albums.primary_genre_id != 39 AND
        (albums.secondary_genre_id != 39 OR albums.secondary_genre_id IS NULL) AND salepoints.id IS NULL AND ytm.effective_date IS NOT NULL
        AND ytm.termination_date IS NULL"
      conditions += " AND albums.person_id = #{ENV['PERSON_ID']}" unless ENV['PERSON_ID'] == 'all'

      albums = Album.joins("INNER JOIN youtube_monetizations ytm ON ytm.person_id = albums.person_id inner join songs on songs.album_id = albums.id
        left outer join salepoint_songs on songs.id = salepoint_songs.song_id LEFT OUTER JOIN salepoints ON albums.id = salepoints.salepointable_id
        AND salepoints.salepointable_type = 'Album' AND salepoints.store_id = 48").where(conditions).uniq

      failpoints = []

      albums.each do |album|
        ytm_purchase = album.person.youtube_monetization.purchase
        unless Salepoint.add_youtubesr_store(album, ytm_purchase)
          failpoints << album
        end
      end

      File.open(File.join(Rails.root,'tmp/salepoint_backfill_error.log'),'w') do |f|
        failpoints.each do |album|
          f.write("#{album.id}: #{album.errors.full_messages}\n")
        end
      end
    end

    log(t, "End backfill of salepoints; realtime=#{realtime}")
  end

  desc "Fix SalepointSongs"
  task :fix_salepoint_songs => :environment do |t|
  end

  desc "Create/associate missing/unassociated distribution_songs"
  task :clean_up_distribution_songs => :environment do |t|
  end

  desc "Retry DistributionSongs stuck in \"enqueued\" state"
  task :retry_enqueued_distribution_songs => :environment do |t|
  end

  desc "Update state of and send DistributionSongs stuck in \"pending_approval\""
  task :unpend_approval => :environment do |t|
  end

  desc "Backfill rejected salepoint songs for automated vetting"
  task create_automated_rejections: :environment do
  end

  desc "Get YTSRAV albums with release date greater than 1 year in future"
  task get_ytsrav_releases_too_far_in_future: :environment do
    path = Rails.root.join(ENV.fetch('YTSRAV_FUTURE_RELEASES_PATH', nil))
    csv_read = CSV.readlines(path, headers: true)
    csv_write_path = "/tmp/yt_srav_1_year_in_future.csv"

    year_from_now = DateTime.current + 1.year
    ytsr_store = Store.find_by(abbrev: 'ytsr')

    CSV.open(csv_write_path, "wb") do |write_csv|
      write_csv << ["song isrc", "orig_release_year", "golive_date", "album id", "track monetization id"]

      csv_read.each do |release|
        song = Song.find_by(tunecore_isrc: release['isrc'])

        if !song
          Rails.logger.debug("⚠️ Couldn't find song ID: #{release['isrc']} ⚠️")
          next
        end

        if (song.album.orig_release_year > year_from_now) || (song.album.golive_date > year_from_now)
          Rails.logger.info("Song ID: #{song.id} too far in future 🍦🍦")

          monetization = song.track_monetizations.where(store: ytsr_store).first
          write_csv << [release['isrc'], song.album.orig_release_year, song.album.golive_date, song.album.id, monetization&.id]
        end
      end
    end
  end
end
