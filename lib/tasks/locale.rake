

namespace :locale do
  desc "Create CSV files from local YML files"
  task create_csv: :environment do
    logger = ActiveSupport::Logger.new("log/locale_translations.log", Logger::DEBUG)

    root_scope = ENV["COUNTRY"] || "en"
    out_file = CSV.open("#{ENV["out_file_path"] || "tmp"}/#{root_scope}_#{Time.now.strftime("%Y%m%d_%H%M")}_translations.csv", "a", { force_quotes: true })
    out_file <<["Source File", "Token", "English (US) Text", "#{root_scope.upcase} translation"]

    files = Dir.glob("#{File.join("config", "locales")}/**/en.yml")
    files.each do |file|
      file_contents = YAML.load_file(file)
      unless file_contents
        logger.error "Empty file #{file}"
        next
      end

      tokens = file_contents["en"]
      unless tokens && tokens.is_a?(Hash) && !tokens.empty?
        logger.error "Improper file structure in #{file}"
        next
      else
        yml_hash_to_csv tokens, root_scope, root_scope, out_file, file, logger
      end
    end
    if ENV["PRINT_FILENAME_TO_STDOUT"]
      puts out_file.path
    end
  end

  def yml_hash_to_csv tokens, root_scope, scope, out_file, source_file, logger
    tokens.keys.each do |key|
      if tokens[key].is_a?(Hash)
        yml_hash_to_csv(tokens[key], root_scope, "#{scope}.#{key}", out_file, source_file, logger)
      else
        if tokens[key].blank? && tokens[key].class != FalseClass
          logger.error "Missing value for key #{key} in #{source_file}"
        else
          begin
            if tokens[key].class == Integer
              value = tokens[key]
            elsif tokens[key].class == String
              value = tokens[key].gsub("&#58;", ":")
            else
              logger.error "Skipping key #{key} in #{source_file} because it is a #{tokens[key].class}"
              return
            end
            out_file << [source_file.gsub("/en.", "/#{root_scope}."), "#{scope}.#{key}", value]
          rescue => e
            logger.error "Error processing key #{key} in #{source_file}: #{e.message}"
            logger.error e.backtrace.join("\n\t")
          end
        end
      end
    end
  end

  desc "Create YML files based on translations in TRANSLATION_FILE CSV"
  task create_ymls: :environment do
    begin
      logger = ActiveSupport::Logger.new("log/locale_translations.log", Logger::DEBUG)

      file = ENV["TRANSLATIONS_FILE"]
      raise "You must specify a path to a TRANSLATIONS_FILE" unless file

      done_file = CSV.open("tmp/#{file.split("/").last}.done", "w")
      error_file = CSV.open("tmp/#{file.split("/").last}.error", "w")

      file_columns = {
        target_yml_file: 0,
        key_path: 1,
        english_version: 2,
        translated_version: 3
      }

      file_hash = {}
      rownum = 0
      CSV.foreach(file, force_quotes: true, encoding: 'iso-8859-1:utf-8') do |row|
        begin
          rownum += 1
          next if rownum == 1

          if row[file_columns[:translated_version]].blank?
            logger.error "Skipping row #{rownum} because of missing translation."
            error_file << row
            next
          end

          key_path = row[file_columns[:key_path]].split(".").reverse
          row_hash = { key_path.shift => row[file_columns[:translated_version]].gsub(":", "&#58;") }

          key_path.each { |key| row_hash = { key => row_hash } }
          row_hash = { row[file_columns[:target_yml_file]] => row_hash }
          file_hash.deep_merge!(row_hash)
          done_file << row
        rescue => e
          error_file << row
          logger.error "Unable to parse row #{rownum}: e.message"
          logger.error e.backtrace.join("\n")
        ensure
          next
        end
      end

      file_hash.keys.each { |file_name|
        FileUtils.mkdir_p File.dirname(file_name)
        File.open(file_name, "a") { |f|
          f.write file_hash[file_name].to_yaml.gsub("---\n", "")
        }
      }
    ensure
      done_file.try(:close) if done_file
      error_file.try(:close) if error_file
    end
  end
end
