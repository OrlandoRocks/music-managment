
namespace :cart_refactor do

  desc "Removes partially paid invoices"
  task :remove_partially_paid_invoices => :environment do
    puts Rails.logger.info "Removing partially paid invoices"
    handle_invoices
  end

  def handle_invoices
    invoices = Invoice.includes(:purchases, :invoice_settlements).where('invoices.settled_at is null')

    invoices.each do |invoice|
      begin
        puts Rails.logger.info "Beginning to remove invoice #{invoice.id}"
        if invoice.invoice_settlements && invoice.invoice_settlements.any? {|s| s.source_type != 'PersonTransaction'}
          Rails.logger.info "!! Unable to remove invoice #{invoice.id} for person #{invoice.person_id}"
        else
          if invoice.can_destroy?
            invoice.credit_and_remove_settlements!
            invoice.reload
            invoice.destroy
            puts Rails.logger.info "Successfully removed invoice #{invoice.id}"
          else
            puts Rails.logger.info  "!! Unable to remove invoice #{invoice.id} for person #{invoice.person_id}"
          end
        end
      rescue StandardError => e
        puts Rails.logger.info "ERROR invoice #{invoice.id}: #{e}"
      end
    end
  end

end
