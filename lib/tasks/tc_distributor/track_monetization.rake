namespace :tc_distributor do
  namespace :track_monetization do

    desc "Send pending track monetization to TC-Distributor"
    task :deliver => :environment do |task|
      puts "Deliver pending Track Monetization to TC-Distributor"
      TrackMonetization::Deliverer.build.call
    end

    desc "Pull and update delivery state from TC-Distributor"
    task :update_state => :environment do
      puts "Pull and update Track Monetization states from TC-Distributor"
      TrackMonetization::StateUpdateWorker.perform_async
    end
  end
end