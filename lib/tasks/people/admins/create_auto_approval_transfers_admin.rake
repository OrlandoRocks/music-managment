desc "Adds an auto_approve_withdrawls_admin if one does not exist"
namespace :people do
  task create_auto_approval_transfers_admin: :environment do

    admin_email = "auto_approve_withdrawls_admin@tunecore.com"
    person      = Person.find_by(email: admin_email)

    next if person.present?

    random_password = Faker::Internet.password(
      min_length: 8,
      max_length: 16,
      mix_case: true,
      special_characters: false
    )

    random_password += Faker::Number.digit.to_s + ["@", "$", "!", "%", "*", "?", "&"].sample(1).join

    admin = Person.new({
      name: "System Auto Approval Withdrawls Admin",
      email: admin_email,
      password: random_password,
      password_confirmation: random_password,
      accepted_terms_and_conditions: 1,
      phone_number: Faker::PhoneNumber.unique.phone_number,
      country: "US"
    })

    admin.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
    admin.save
    admin.roles = Role.all
  end
end
