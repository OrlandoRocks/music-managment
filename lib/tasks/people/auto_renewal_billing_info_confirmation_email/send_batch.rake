namespace :people do
  namespace :auto_renewal_billing_info_confirmation_email do
    desc "Send auto_renewal emails to people who have upcoming auto-renewals"
    task :send_batch, [:limit, :offset] => :environment do |_t, args|
      limit = args[:limit].to_i if args[:limit]
      offset = args[:offset].to_i if args[:offset]
      Person::AutoRenewalPeopleAndMailerWorker.perform_async(limit: limit, offset: offset)
    end
  end
end
