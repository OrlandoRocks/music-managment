namespace :people do
  namespace :tc_social_pro_billing_info_confirmation do
    desc "Send renewal emails to people who have upcoming TC Social Pro renewals"
    task :send_batch, [:limit, :offset] => :environment do |_t, args|
      limit = args[:limit].to_i if args[:limit]
      offset = args[:offset].to_i if args[:offset]
      Person::TcSocialProMailerBatchWorker.perform_async(limit: limit, offset: offset)
    end
  end
end
