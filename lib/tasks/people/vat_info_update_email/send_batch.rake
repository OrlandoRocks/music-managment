namespace :people do
  namespace :vat_info_update_email do
    desc "Send VAT info update e-mail to people in the EU"
    task :send_batch, [:limit, :offset] => :environment do |_t, args|
      limit = args[:limit].to_i if args[:limit]
      offset = args[:offset].to_i if args[:offset]

      Person::VatInfoUpdateBatchMailerWorker.perform_async(
        limit: limit,
        offset: offset
      )
    end
  end
end
