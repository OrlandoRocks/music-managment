namespace :people do
  namespace :sanctions do
    desc "Sanction all users in sanctioned countries"
    task :sanction_users => :environment do |_t, args|
      # dry_run = true unless rake task is executed with `DRY_RUN=0 && rake people:sanctions:sanction_users` 
      dry_run = (ENV['DRY_RUN'].blank? || ENV['DRY_RUN'] != "0")
      csv_file_name = "tmp/sanctioned_users.csv".freeze

      Rails.logger.info "DRY_RUN = #{dry_run}"

      if (dry_run)
        Rails.logger.info "Begin collecting sanctioned users"
        sanctioned_people = Person.where(country: Country::CURRENT_SANCTIONED_COUNTRIES_ISO_CODES)
        sanctioned_people = sanctioned_people.where.not(lock_reason: "Sanctioned").or(sanctioned_people.where(lock_reason: nil))
        
        if (sanctioned_people.blank?) 
          Rails.logger.error "There are no users to lock."
          return
        end

        begin
          CSV.open(csv_file_name, "w") do |file|
            sanctioned_people.pluck(:id).each_slice(2500) do |people|
              file << people
            end
          end
        rescue => e
          Rails.logger.error "Failed to create sanctioned people csv file: #{e}"
        end

        Rails.logger.info "Done collecting users to be locked."
        Rails.logger.info "Run task again with DRY_RUN disabled."
        Rails.logger.info "Execute 'DRY_RUN=0 && rake people:sanctions:sanction_users'"
      else
        Rails.logger.info "Begin locking users"
        count = 0

        begin
          CSV.open(csv_file_name, "r").each_slice(2500) do |csv| 
            csv.each do |people_ids|
              sanctioned_people = Person.where(id: people_ids)
              next if sanctioned_people.blank?

              sanctioned_people.each do |person|
                note = "Locked due to sanctions."
                if (person.active?) 
                  note = "#{note} Prior lock_reason = #{person.lock_reason}"
                end
                
                Note.create(:note=>note, :subject=>"Locked", :related => person, :note_created_by_id => 0)
                person.lock!("Sanctioned", true)
                count = count + 1
              end
            end
          end
        rescue => e
          Rails.logger.error "There was an error locking a user: #{e}"
        end

        Rails.logger.info "Done locking users. Total Users Locked: #{count}"
      end
    end
  end
end
