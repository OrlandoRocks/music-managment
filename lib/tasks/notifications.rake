namespace :notifications do

  desc "Adds a notification for users who have never distributed and have an unreleased album"
  task :unreleased_release_notification => :environment do
    run_date = ENV["RUN_DATE"]

    puts Rails.logger.info "Running notifications:unreleased_release_notification with RUN_DATE=#{run_date.blank? ? "" : run_date.to_s}"
    UnreleasedNotification.create_notifications(run_date: run_date)
    puts Rails.logger.info "Completed notifications:unreleased_release_notification"
  end

  desc "Adds notifications for albums that are newly available in itunes"
  task :itunes_link_notification => :environment do
    run_date = ENV["RUN_DATE"]

    puts Rails.logger.info "Running notifications:itunes_link_notification with RUN_DATE=#{run_date.blank? ? "" : run_date.to_s}"
    ItunesLinkNotification.create_notifications(run_date: run_date)
    puts Rails.logger.info "Completed notifications:unreleased_release_notification"
  end

  desc "Adds notifications to composers with missing splits information"
  task :missing_splits_notification => :environment do
    run_date = ENV["RUN_DATE"]

    puts Rails.logger.info "Running notifcaionts:missing_splits_notification with RUN_DATE=#{run_date.blank? ? "" : run_date.to_s}"
    MissingSplitsNotification.create_notifications(run_date: run_date)
    puts Rails.logger.info "Completed notifications:missing_splits_notification"
  end

  desc "Adds notifications to publishing_composers with missing splits information"
  task :missing_publishing_composition_splits_notification => :environment do
    run_date = ENV["RUN_DATE"]

    puts Rails.logger.info "Running notifcaionts:missing_publishing_composition_splits_notification with RUN_DATE=#{run_date.blank? ? "" : run_date.to_s}"
    MissingSplitsNotification.create_notifications_for_publishing_composers(run_date: run_date)
    puts Rails.logger.info "Completed notifications:missing_publishing_composition_splits_notification"
  end

  desc "Adds notifications to ytm customers who have not submitted songs for approval"
  task :missing_ytm_notification => :environment do
    puts Rails.logger.info "Running notifications:missing_ytm_notification"
    MissingYtmTracksNotification.create_notifications
    puts Rails.logger.info "Completed notifications:missing_ytm_notification"
  end

  desc "Adds notifications to users who had royalties posted to their account"
  task :youtube_royalties_notification => :environment do
    puts Rails.logger.info "Running notifications:missing_splits_notification"
    YoutubeRoyaltiesNotification.create_notifications
    puts Rails.logger.info "Completed notifications:missing_splits_notification"
  end
end
