namespace :widget do
  desc "Run this task to deactivate widgets"
  task :deactivate => :environment do
    array_of_widgets = Widget.joins("inner join renewal_items ri on ri.related_id=widgets.id and ri.related_type='Widget'").where("(select max(expires_at) from renewal_history where renewal_id=ri.renewal_id group by renewal_id) < NOW()")
    puts "About to expire a bunch of widgets. They will no longer be publicly active."
    Widget.expire!(array_of_widgets)
  end

  desc "Run this task to activate all widgets"
  task :activate => :environment do
    process_start_time = Time.now

    Rails.logger.info "starting rake task to activate all widgets start_time=#{process_start_time}"
    puts "starting rake task to activate all widgets start_time=#{process_start_time}"


    begin
      configured_widgets_to_activate = Widget.where("created_as_free = 'N' and deactivated_at is not null and config_data is not null")
      Rails.logger.info "About to activate the following configured widgets=[#{configured_widgets_to_activate.collect{|w| w.id}.join(", ")}]"
      puts "About to activate the following configured widgets=[#{configured_widgets_to_activate.collect{|w| w.id}.join(", ")}]"

      configured_widgets_to_activate.each do |w|
        w.deactivated_at = nil
        w.save
      end

      Rails.logger.info "Finished activating configured widgets finished_time=#{Time.now} total_time = #{Time.now-process_start_time}"
      puts "Finished activating configured widgets finished_time=#{Time.now} total_time = #{Time.now-process_start_time}"

    rescue => e
      Rails.logger.info "issue activating widget error=#{e.message}"
    end

    begin
      process_start_time = Time.now

     Rails.logger.info "starting process to set deactivated_at column to null for uncofigured widgets start_time=#{process_start_time}"
     puts "starting process to set deactivated_at column to null for uncofigured widgets start_time=#{process_start_time}"

      unconfigured_widgets_to_activate = Widget.where("created_as_free = 'N' and deactivated_at is not null and config_data is null")
      Rails.logger.info "About to set deactivated_at column to null for the following unconfigured widgets [#{unconfigured_widgets_to_activate.collect{|w| w.id}.join(", ")}]"
      puts "About to set deactivated_at column to null for the following unconfigured widgets=[#{unconfigured_widgets_to_activate.collect{|w| w.id}.join(", ")}]"

      Widget.where("created_as_free = 'N' and deactivated_at is not null and config_data is null").update_all("deactivated_at = null")

      Rails.logger.info "Finished setting deactivated_at column to null for unconfigured widgets finished_time=#{Time.now} total_time = #{Time.now-process_start_time}"
      puts "Finished setting deactivated_at column to null for unconfigured widgets finished_time=#{Time.now} total_time = #{Time.now-process_start_time}"

    rescue => e
      Rails.logger.info "issue setting deactivated_at column to null error=#{e.message}"
    end

  end
end
