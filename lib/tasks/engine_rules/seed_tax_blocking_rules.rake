
namespace :engine_rules do
  task seed_tax_blocking_rules: :environment do
    rules = [
      {
        "category" => "tax_blocking",
        "name" => "current_year_earnings_threshold",
        "config" => { "current_year_threshold" => 5.0 },
        "enabled" => true
      },
      {
        "category" => "tax_blocking",
        "name" => "past_years_earnings_threshold",
        "config" => {
          "previous_evaluation_years" => {
            2018 => 5.0,
            2019 => 5.0,
            2020 => 5.0,
          },
        },
        "enabled" => true
      }
    ]
    rules.each(&EngineRule.method(:find_or_create_by))
  end
end