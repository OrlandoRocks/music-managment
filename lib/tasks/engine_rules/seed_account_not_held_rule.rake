namespace :engine_rules do
  desc "Seed account_not_held auto-approval rule"
  task seed_account_not_held_rule: :environment do
    EngineRule.create_or_find_by(
      "category" => "auto_approve_transfers",
      "name" => "account_not_held",
      "config" => {},
      "enabled" => true
    )
  end
end
