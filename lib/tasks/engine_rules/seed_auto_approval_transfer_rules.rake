
namespace :engine_rules do
  task seed_auto_approve_transfer_rules: :environment do
    rules = [
      {
        "category" => "auto_approve_transfers",
        "name" => "currency_limit",
        "config" => { "max_amount" => 100 },
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "request_approved_within",
        "config" => { "window_in_days" => 7 },
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "withdrawals_using_same_payout_method",
        "config" => { "successful_withdrawals" => 3 },
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "not_currently_flagged",
        "config" => { "flag" => "suspicious" },
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "not_multiple_occupancy_payoneer_unless_vip",
        "config" => {},
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "enabled_2fa_unless_vip",
        "config" => {},
        "enabled" => true
      },
      {
        "category" => "auto_approve_transfers",
        "name" => "transfer_login_match_previous",
        "config" => { "number_of_previous_logins" => 5 },
        "enabled" => true
      }
    ]
    rules.each(&EngineRule.method(:find_or_create_by))
  end
end