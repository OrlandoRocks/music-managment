namespace :friend_referral do
  desc "Batch proces unposted commissions; Accepts optional PERSON_ID"
  task :batch_process_commissions => :environment do
    log("friend_referral:batch_process_commissions", 'Starting process')

    if person_id = ENV['PERSON_ID']
      processed_referrals = FriendReferral.batch_process_commissions(nil, person_id)
    else
      processed_referrals = FriendReferral.batch_process_commissions
    end

    log("friend_referral:batch_process_commission", "Processed #{processed_referrals.size} commissions")
  end
end
