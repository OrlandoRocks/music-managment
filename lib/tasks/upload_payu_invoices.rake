namespace :upload_payu_invoices do
  # Example usage:
  # bundle exec rake "upload_payu_invoices:run[2021-01-01..2021-01-15]"

  task :run, [:date_range] => :environment do |_t, args|
    job_id = Payu::WorkflowWorker.perform_async
    Rails.logger.info("PAYU_INV_UPLOAD_WORKER SCHEDULED FOR: JOB ID: #{job_id}")
  end
end
