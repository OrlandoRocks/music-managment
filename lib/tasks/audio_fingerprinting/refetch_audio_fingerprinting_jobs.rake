namespace :audio_fingerprinting do
  desc "refetch jobs waiting for more than 24 hours"
  task :refetch_waiting_songs => :environment do
    Rails.logger.info "Started Audio Fingerprinting - Refetch waiting songs"
    return unless ScrapiJob.exists?(['status= "done" and created_at > ?', 24.hours.ago])
    jobs_to_refetch = ScrapiJob.where(created_at: Time.now-24.hours..Time.now-2.hours, status: 'waiting_file')
    Rails.logger.info "Refetching - #{jobs_to_refetch.length} jobs"
    jobs_to_refetch.each do |scrapi_job|
      Scrapi::GetJobWorker.perform_async(scrapi_job.job_id)
    end
  end
end
