namespace :audio_fingerprinting do
  desc "remove all redis keys"
  task :remove_redis_keys => :environment do
    Rails.logger.info("Removing redis keys for audio fingerprinting")
    keys = $redis.keys "scrapi*"
    $redis.del(*keys) unless keys.empty?
    Rails.logger.info("Removed redis keys for audio fingerprinting.")
  end

  desc "deletes song id is 0 and requeue waiting jobs"
  task :requeue => :environment do
    ScrapiJob.where(song_id: [0, nil]).destroy_all
    exit unless ENV['FROM_DATE']
    ScrapiJob.where(created_at: Date.parse(ENV['FROM_DATE'])..Time.now-2.hours, status: 'waiting_file').each do |scrapi_job|
      Scrapi::GetJobWorker.perform_async(scrapi_job.job_id)
    end
  end
end
