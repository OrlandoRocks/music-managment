namespace :audio_fingerprinting do
  desc "backfill missed scrapi jobs"
  task create_scrapi_jobs: :environment do
    Rails.logger.info "Initiating sidekiq queue for creating scrapi jobs"
    albums = Album.find_by_sql("SELECT DISTINCT(album_id) FROM songs
                                  LEFT OUTER JOIN scrapi_jobs sj ON sj.song_id = songs.id
                                  WHERE
                                    album_id IN (SELECT distinct(albums.id) as id
                                      FROM albums
                                      WHERE (albums.takedown_at IS NULL
                                        AND albums.payment_applied = 1
                                        AND finalized_at IS NOT NULL
                                        AND albums.updated_at > '2020-07-13'
                                      )
                                      GROUP BY albums.person_id
                                      ORDER BY person_id ASC)
                                    AND sj.id IS NULL")

    Song.where(album_id: albums.map(&:album_id)).find_each do |song|
      next if ScrapiJob.exists?(song_id: song.id)
      Sidekiq.logger.info "Sidekiq Job initiated: #{song.id}"
      Scrapi::CreateJobWorker.perform_async(song.id)
    end
    Rails.logger.info "Completed sidekiq queue for creating scrapi jobs"
  end
end
