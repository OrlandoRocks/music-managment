namespace :backfill_distribution do
  desc "Backfill distribution for existing salepoint"
  task :run, [:source_store_id, :destination_store_id] => :environment do |_, args|
    source_store_id = args.source_store_id
    destination_store_id = args.destination_store_id

    BackfillDistribution.new(source_store_id, destination_store_id).run
  end
end
