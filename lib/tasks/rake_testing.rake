# there's not a great way to do a full integration test for rake tasks, so i've created
# rake tasks to test rake tasks (basically mimicing the tests I would run manually)

namespace :rake_testing do

  desc 'renewal testing'
  task :autorenewals =>  [:environment] do
    # create the test data
    # get the expiration date
    ENV['DATE'] = RenewalHistory.maximum(:expires_at).to_date.to_s
    Rake::Task["batch:build_invoices"].invoke
    Rake::Task["batch:run"].invoke
    count = 0
    begin
      count += 1
      puts "#{count} times through the process_response loop"
      sleep 60
      Rake::Task["batch:process_response"].invoke
      Rake::Task["batch:process_response"].reenable
    end while !PaymentBatch.find_by(status: 'processing').nil? && count < 30
  end

  desc 'renewal testing'
  task :eft_sending =>  [:environment] do
    # create the test data
    ENV['COUNT']="6"
    # approve some of the eft_withdrawals
    efts = EftBatchTransaction.all
    admin_user = Person.find_by(email: 'admin@tunecore.com')
    efts.each_with_index do |eft, i|
      if i % 3 != 0 # approve 2/3 of the eft withdrawals
        eft.approve(admin_user)
        puts "Admin approved eft id: #{eft.id} amount: #{eft.amount} #{eft.currency}"
      end
    end
    # kick off eft batch
    Rake::Task["eft:create_batch"].invoke
    count = 0
    begin
      count += 1
      puts "#{count} times through the process_response loop"
      sleep 60
      Rake::Task["eft:process_response"].invoke
      Rake::Task["eft:process_response"].reenable
    end while !EftBatch.find_by(status: 'uploaded').nil? && count < 30
  end
end
