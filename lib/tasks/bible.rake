#Rake tasks for importing the bible albums
namespace :bible do

  desc "Imports bible artwork in specific folder format"
  task :import_artwork => :environment do

    #Requires artwork to be in a directories..  Each one containing a jpg and the meta datacsv file for that
    #album

    raise("Need BIBLE_ARTWORK_DIRECTORY") unless ENV["BIBLE_ARTWORK_DIRECTORY"]
    raise("Need PERSON_ID")               unless ENV["PERSON_ID"]

    person = Person.find(ENV["PERSON_ID"])

    #directory where artwork and meta data exist
    bible_artwork_directory = ENV["BIBLE_ARTWORK_DIRECTORY"]

    Dir.chdir(bible_artwork_directory)

    #loop through folders each folder has artwork and the meta data for the album
    Dir.glob("**").each do |folder|

      #Sanity check
      if folder.size == 10
        puts(folder)

        Dir.chdir(folder)

        csv_fname     = Dir.glob("*.csv").first
        artwork_fname = Dir.glob("*.jpg").first

        i = 0
        CSV.foreach(csv_fname) do |line|
          i = i + 1
          next if i == 1

          begin
            album_name = line[0]
            album = person.albums.find_by(name: album_name)
            puts(album_name)
            puts(album.name)

            begin
              @artwork = album.artwork || Artwork.new(:album => album)

              file = File.open("#{artwork_fname}")
              @artwork.assign_artwork file
              @artwork.save
            rescue StandardError => e
              puts(e)
              @artwork.update(:last_errors=>"Please try again or contact customer support if the problem persists.", :uploaded => false, :height => nil, :width => nil)
            end

          rescue StandardError => e
            puts(e)
          end

          break if i == 2
        end

      end

      Dir.chdir(bible_artwork_directory)
    end

  end

end
