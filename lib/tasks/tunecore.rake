# 2009-08-20 -- MJL -- Removing the reports namespace from the Tunecore rake task since there is a reports rake task that does the same thing
namespace :tunecore do
  namespace :staging do
    namespace :incoming do
      desc "Cleanup old files in staging"
      task :cleanup => :environment do
        Staging.cleanup
      end

      desc "Dry run to cleanup old files in staging"
      task :pretend_cleanup => :environment do
        Staging.cleanup(false)
      end
    end
  end

  namespace :promo_codes do
    desc "Create an empty promo codes table for use on db dumps without this table"
    task :create => :environment do
      raise "Task unavailable to this database (No migration support!)" unless ActiveRecord::Base.connection.supports_migrations?

    ActiveRecord::Base.connection.create_table :promo_codes do |t|
          t.column :promo, :string, :limit => 12, :null => false
          t.column :code, :string, :limit => 18, :null => false
          t.column :used, :boolean, :null => false, :default => false
          t.column :created_on, :date
          t.column :used_on, :date
          t.column :lock_version, :integer, :null => false, :default => 0
        end
      end
    end

  namespace :sessions do
    desc "Immediately creates a sessions table for use with CGI::Session::ActiveRecordStore"
    task :create => :environment do
      raise "Task unavailable to this database (no migration support)" unless ActiveRecord::Base.connection.supports_migrations?

      ActiveRecord::Base.connection.create_table :sessions do |t|
        t.column :session_id, :string
        t.column :data, :text
        t.column :updated_at, :datetime
      end

      ActiveRecord::Base.connection.add_index :sessions, :session_id
    end

    desc "Drop the sessions table"
    task :drop => :environment do
      raise "Task unavailable to this database (no migration support)" unless ActiveRecord::Base.connection.supports_migrations?

      ActiveRecord::Base.connection.drop_table :sessions
    end

    desc "Drop and recreate the session table (much faster than 'DELETE * FROM sessions')"
    task :purge => [ :drop, :create ] do
    end
  end

  desc "run report for distribution errors"
  task :itunes_distribution_errors => :environment do
    Distribution.where("converter_class = 'MusicStores::Itunes::Converter' and state = 'error'").select{|d| d.petri_bundle.state != 'dismissed' &&  ! d.petri_bundle.album.takedown? && d.transitions.detect{|t| next if t.message == nil; t.message[/Invalid Package/]} && !PetriBundle.where(album_id: d.petri_bundle.album_id).detect{|b| b.distributions.detect{|d| d.state == 'delivered' && d.converter_class == 'MusicStores::Itunes::Converter'}}}.each{|d| puts d.petri_bundle.album_id};nil
  end

  desc "Add publishing product for COUNTRY_WEBSITE_ID. Optional params ADMIN_ID, PRICE"
  task :add_publishing_product => :environment do
    @product_name = SONGWRITER_PRODUCT_NAME
    @display_name = "Songwriter Service"
    @description =  "Collecting, licensing and policing your songwriter copyrights"
    @admin = ENV['ADMIN'] || 'arne@tunecore.com'
    @country_website_id = ENV['COUNTRY_WEBSITE_ID'] || 1
    @price = ENV['PRICE'] || 75
    admin = Person.find_by(email: ENV['ADMIN'])
    country_website = CountryWebsite.find_by(id: @country_website_id)
    ActiveRecord::Base.transaction do
      if product = Product.where('name = ? and country_website_id = ?', @product_name, @country_website_id).first
        puts "#{@product_name} exists for country_website_id #{@country_website_id} - exiting"
      else
        product = Product.new(:name => @product_name,
                                 :display_name => @display_name,
                                 :description => @description,
                                 :status => 'Active',
                                 :product_type => 'Ad Hoc',
                                 :is_default => true,
                                 :sort_order => 1,
                                 :renewal_level => 'None',
                                 :price => @price,
                                 :currency => country_website.currency,
                                 :created_by_id => @admin,
                                 :applies_to_product => 'Composer',
                                 :country_website_id => @country_website_id,
                                 :original_product_id => 0)

        product.save!
        product.id = Product.where('name = ? and country_website_id = ?', @product_name, @country_website_id).first.id
        # You will need to hard-code the resulting product id in the product.rb model as well
        puts "Created product #{@product_name} id #{product.id} -- remember to hard-code this as a variable in the product.rb model"
        base_item = BaseItem.find_or_create_by(:name => @product_name,
                                                     :description => @description,
                                                     :price => @price,
                                                     :currency => country_website.currency)
        base_item_option = BaseItemOption.find_or_create_by(:name=>@product_name,
                                                            :base_item => base_item,
                                                            :rule_type => 'inventory',
                                                            :rule => 'price_for_each',
                                                            :inventory_type => 'Composer',
                                                            :price => @price,
                                                            :currency => country_website.currency)
      product_item = ProductItem.find_or_create_by(:name => @product_name,
                                                      :product => product,
                                                      :base_item => base_item,
                                                      :currency => country_website.currency)
      product_item_rule = ProductItemRule.find_or_create_by(:inventory_type => 'Composer',
                                                           :product_item => product_item,
                                                           :base_item_option => base_item_option,
                                                           :rule_type => 'price_for_each',
                                                           :rule => 'price_for_each',
                                                           :quantity => 1,
                                                           :price => @price,
                                                           :currency => country_website.currency)
      puts "Created base_item, base_item_option, product_item, and product_item_rule for #{@product_name} in country_website_id #{@country_website_id}"
      end
    end
  end


end
