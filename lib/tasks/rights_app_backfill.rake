namespace :publishing_administration do
  task rights_app_backfill: :environment do
    start_date = ENV["START_DATE"]
    end_date   = ENV["END_DATE"]

    PublishingAdministration::BackfillWorker.perform_async(start_date: start_date, end_date: end_date)
  end
end
