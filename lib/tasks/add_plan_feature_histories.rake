# frozen_string_literal: true

# bundle exec rake plans:backfill_plan_feature_histories
namespace :plans do
  task :backfill_plan_feature_histories => :environment do
    raise "❌ THIS TABLE ALREADY HAS ENTRIES. THERE IS NO NEED TO BACKFILL. ❌" if PlansPlanFeatureHistory.any?

    PlansPlanFeature.find_each do |plans_plan_feature|
      PlansPlanFeatureHistory.create!(
        plan: plans_plan_feature.plan,
        plan_feature: plans_plan_feature.plan_feature,
        change_type: :add,
        created_at: plans_plan_feature.created_at,
        updated_at: plans_plan_feature.updated_at
      )
    end

    Rails.logger.info "🎉 Task ended! #{PlansPlanFeatureHistory.count} PlansPlanFeatureHistories backfilled 🎉"
  end
end
