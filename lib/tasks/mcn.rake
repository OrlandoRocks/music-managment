namespace :mcn do
  desc  "Upload MCN Payment Files"
  task :upload_mcn_payment_files => :environment do

    get_files.each do | file_path |
      file_info = file_path.match(/^(?<year>\d*)\/(?<period>\d*)\/(?<person_id>.*)\.pdf$/)

      if valid_filename? file_info
        person = Person.find file_info[:person_id]

        if person
          attributes = get_attributes person, file_info
          royalty_payment = RoyaltyPayment.where(attributes).first
          associate_report_with_payment royalty_payment, attributes, file_path
        else
          emit_warning "Could not find person_id #{person_id}"
        end

      else
        emit_warning "Directory structure is incorrect.  File must exist in folder YYYY/(1-12)/<person_id>.pdf"
      end
    end
  end

  def get_attributes person, file_info
    {:period_type     => "monthly",
     :period_interval => file_info[:period],
     :period_year     => file_info[:year],
     :person_id       => person.id
    }
  end

  def associate_report_with_payment payment, attributes, file
    if payment
      emit_warning "Updating royalty payment for #{attributes.inspect}"
      payment.pdf_summary = File.open(file)
      payment.save
    else
      emit_warning "Unable to find royalty payment for #{attributes.inspect}"
    end
  end

  def emit_warning message
    Rails.logger.info message
    puts message
  end

  def valid_filename? file_elements
    file_elements.size == 4                   &&
    file_elements[:year].match(/^\d{4}$/)     &&
    file_elements[:period].match(/^\d{1,2}$/)
  end

  def get_files
    if !File.directory? ROYALTY_PDF_DIRECTORY
      Dir.mkdir(ROYALTY_PDF_DIRECTORY)
    end

    Dir.chdir(ROYALTY_PDF_DIRECTORY)
    files = File.join("**", "*.pdf")
    Dir.glob(files)
  end
end
