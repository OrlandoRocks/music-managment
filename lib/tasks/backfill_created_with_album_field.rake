desc "backfill the album created_with field"
task backfill_created_with_field: :environment do
  Album.where(created_with_songwriter: true).update_all(created_with: "songwriter")
end
