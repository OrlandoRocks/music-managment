# frozen_string_literal: true

namespace :flag_reasons do
  desc "Task to populate flag_reaons"
  task create: :environment do
    # Modify this array to add more flag reasons in the future
    flag_reaons = [
      {
        flag: Flag::WITHDRAWAL,
        reason: FlagReason::INCORRECT_TAX_FORM
      },
      {
        flag: Flag::ADDRESS_LOCKED,
        reason: FlagReason::PAYONEER_KYC_COMPLETED
      },
      {
        flag: Flag::ADDRESS_LOCKED,
        reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED
      },
      {
        flag: Flag::SUSPICIOUS,
        reason: "Sift Identified"
      }
    ].freeze

    flag_reaons.map do |flag_reason|
      flag = Flag.find_by(flag_reason[:flag])
      next if flag.blank?

      flag.flag_reasons.find_or_create_by(
        reason: flag_reason[:reason]
      )
    end
  end
end
