namespace :distribution_system do
  desc "daily summary of delivery errors sent to zendesk"
  task :delivery_error_summary_email => :environment do
    DeliveryErrorNotifierService.notify(ENV["FROM_DATE"], ENV["TO_DATE"], ENV["EXCLUDED_STORE_IDS"])
  end
end
