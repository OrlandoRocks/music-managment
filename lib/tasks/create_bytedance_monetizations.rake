namespace :create_bytedance_monetizations do
  desc "create Facebook monetizations for Byte Dance albums"
  task :fb_reels, [:album_ids] => [:environment] do |_t, args|
    if args.album_ids.blank?
      raise "Album IDs not provided. Please provide space-separated album IDs argument.\n" \
        "Example: rake create_bytedance_monetizations:fb_reels['123 456']"
    end
    album_ids = args.album_ids&.split(" ")

    puts "Creating FB Reels Track Monetization for albums: #{album_ids}"
    albums = Album.where(person_id: ENV["BYTEDANCE_USER_ID"], id: album_ids)
    stores = Store.where(id: Store::FB_REELS_STORE_ID)
    albums.each do |album|
      if album.fb_reels_salepoint?
        salepoints = album.salepoints.facebook_reels
      else
        salepoints, _stores_not_added = album.add_stores(stores)
      end

      if salepoints.present?
        salepoints.each(&:paid_post_proc)
        TrackMonetization::PostApprovalWorker.perform_async(album.id, Store::FB_REELS_STORE_ID)
      end
    end
    puts "Done!"
  end
end
