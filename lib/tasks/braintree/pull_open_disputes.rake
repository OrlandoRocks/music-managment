namespace :braintree do
  desc "Pull open disputes from braintree"
  task pull_open_disputes: :environment do
    # Fetch disputes from Braintree
    config = PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)
    gateway = Braintree::ConfigGatewayService.new(config).gateway
    collection =
      gateway.dispute.search do |search|
        search.status.is Braintree::Dispute::Status::Open
      end
    fetched_disputes = collection.disputes

    # Remove already existing disputes on disputes_tables
    fetched_dispute_ids = fetched_disputes.map(&:id)
    existing_dispute_ids = Dispute
                           .where(dispute_identifier: fetched_dispute_ids)
                           .map(&:dispute_identifier)
    disputes_to_create =
      fetched_disputes.reject do |dispute|
        existing_dispute_ids.include? dispute.id
      end

    failed_disputes = []
    dispute_status = DisputeStatus.find_by!(
      source_type: DisputeStatus::BRAINTREE,
      status: Braintree::Dispute::Status::Open
    )

    # Create dispute entry in disputes_table
    disputes_to_create.each do |bt_dispute_obj|
      dispute_id = bt_dispute_obj.id
      transaction_id = bt_dispute_obj.transaction.id
      source = BraintreeTransaction.find_by!(
        transaction_id: transaction_id
      )
      amount_cents = (Float(bt_dispute_obj.amount) * 100).round

      Dispute.create!(
        dispute_identifier: dispute_id,
        reason: bt_dispute_obj.reason,
        currency: bt_dispute_obj.currency_iso_code,
        initiated_at: bt_dispute_obj.created_at,
        transaction_identifier: transaction_id,
        source: source,
        amount_cents: amount_cents,
        dispute_status: dispute_status
      )
    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotFound => e
      error_info = {
        dispute_id: dispute_id,
        originial_transaction_id: transaction_id,
        error: e.message
      }
      failed_disputes << error_info
      Tunecore::Airbrake.notify("Failed to create dispute", error_info)
    end

    # Logging
    total_disputes = disputes_to_create.count
    failure_count = failed_disputes.count

    puts "================================================"
    puts "Total disputes: #{total_disputes}\n" \
      "Successfully created: #{total_disputes - failure_count}\n"\
      "Failed: #{failure_count}"
    puts "================================================"
    puts "Failure Details => \n #{failed_disputes}" if failure_count.positive?
  end
end
