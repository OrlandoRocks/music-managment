namespace :braintree do
  namespace :refund_transactions do
    task run: :environment do
      # NOTE: The CSV used to run this task is required to have two columns with the following headers on the first row:  
      #       transaction_id, refund_amount

      task_log_tag = "[BRAINTREE TRANSACTIONS REFUND PROCESS]: ".freeze
      transactions_s3_key = ENV['BRAINTREE_REFUNDS_FILE']

      if transactions_s3_key.present?
        transactions_csv = RakeDatafileService.csv(transactions_s3_key)
        refund_transaction_records = transactions_csv.entries
        stats = { successful_refunds: 0, errors: 0 }

        print(task_log_tag, "beginning refunds for #{refund_transaction_records.length} transactions\n")
        refund_transaction_records.each do |transaction_record|
          result = Braintree::Transaction.refund(transaction_record['transaction_id'], transaction_record['refund_amount'])

          if result.success?
            print(task_log_tag, "successfully refunded transaction_id: #{transaction_record['transaction_id']}, refund_amount: #{transaction_record['refund_amount']}\n")
            stats[:successful_refunds] += 1
          else
            error = result.errors.to_a.map(&:message).join(', ')
            print(
              task_log_tag,
              "Error refunding transaction_id: #{transaction_record['transaction_id']}, refund_amount: #{transaction_record['refund_amount']} error: #{error}\n"
            )
            stats[:errors] += 1
          end
        rescue Braintree::NotFoundError
          print(
            task_log_tag,
            "Error refunding transaction_id: #{transaction_record['transaction_id']}, refund_amount: #{transaction_record['refund_amount']} error: Transaction Not Found\n"
          )
        end

        print(task_log_tag, "Completed refunds for #{stats[:successful_refunds]} transactions\n")
        print(task_log_tag, "#{stats[:errors]} errors\n")
      else
        print(task_log_tag, "ERROR: BRAINTREE_REFUNDS_FILE ENV VARIABLE UNSET\n")
      end
    end
  end
end

