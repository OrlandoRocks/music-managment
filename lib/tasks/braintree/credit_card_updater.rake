
# Usage: rake braintree:update_credit_card_info csv_file=braintree_sample_au_report.csv

desc "Update last 4 digits and expiry date of credit cards stored in Braintree"
namespace :braintree do
  task :update_credit_card_info => :environment do
    TOKEN_COLUMN = "Payment Method Token"
    UPDATE_TYPE_COLUMN = "Account Updater Event Description"
    CC_NUMBER_COLUMN = "Credit Card Last 4"
    EXPIRY_DATE_COLUMN = "Expiration Date"

    input_csv_file = ENV['csv_file']
    unless input_csv_file
      abort "Error: Braintree 'csv_file' needs to be passed to run this rake task"
    end

    account_updater = Braintree::AccountUpdaterService.new
    CSV.foreach(input_csv_file, headers: true) do |row|
      account_updater.process(row[TOKEN_COLUMN], row[UPDATE_TYPE_COLUMN],
                              row[CC_NUMBER_COLUMN], row[EXPIRY_DATE_COLUMN])
    end
  end
end
