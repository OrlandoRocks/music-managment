namespace :artist_mapping do
  desc "update redistribution packages"
  task update_redistribution_packages: :environment do
    RedistributionPackageChecker.check_redistribution_statuses
  end

  desc "reinitialize old zendesk ticket external service ids"
  task reset_zendesk_tickets_for_apple: :environment do
    ExternalServiceId
        .created_zendesk_tickets
        .by_service("apple")
        .where("updated_at < ?", 2.weeks.ago).find_each do |external_service_id|
      external_service_id.update(state: nil)
    end
  end
end
