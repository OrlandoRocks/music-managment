namespace :zendesk do
  task check_job_statuses: :environment do
    Zendesk::JobStatusWorker.perform_async
  end
end
