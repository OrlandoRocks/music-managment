namespace :royalties do
  desc "process currency conversions based of info provided in CSV"
  task :process_conversion_rates, [:s3_file_name] => :environment do |_, args|
    rates = RakeDatafileService.csv(args[:s3_file_name])
    rates_hash = []
    rates.each do |rate|
      rates_hash << {
        "base_currency" => rate["base_currency"],
        "currency" => rate["currency"],
        "fx_rate" => rate["fx_rate"],
        "coverage_rate" => rate["coverage_rate"]
      }
    end

    service = Royalties::PostFinanceApprovalService.new(rates_hash)

    if service.valid?
      service.process
    else
      puts "Error!! Check if posting is in the right state and if conversion rates are provided for all currencies."
    end
  end
end
