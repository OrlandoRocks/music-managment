namespace :royalties do
  desc "Maps substores to sip stores for symphony stores"
  task :add_royalty_sub_store_to_sip_stores => :environment do
    SipStore.where(id: 575).update(royalty_sub_store_id: 1)
    SipStore.where(id: 576).update(royalty_sub_store_id: 2)
    SipStore.where(id: 577).update(royalty_sub_store_id: 3)
  end
end
