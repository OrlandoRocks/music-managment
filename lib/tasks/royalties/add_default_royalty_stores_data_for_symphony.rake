namespace :royalties do
  desc "Creates default stores information for symphony"
  task :add_default_royalty_stores_data_for_symphony => :environment do
    RoyaltyStore.create(id:1, name: 'Spotify')
    RoyaltyStore.create(id: 2, name: 'Tiktok')

    RoyaltySubStore.create(id:1, name: "Spotify", royalty_store_id: 1)
    RoyaltySubStore.create(id:2, name: "Spotify Discovery", royalty_store_id: 1)
    RoyaltySubStore.create(
        id: 3,
        name: "TikTok",
        royalty_store_id: 2,
        royalty_rate: 0.8
    )

    UserStoreRoyaltyRate.create(person_id: 2721, royalty_sub_store_id: 3, royalty_rate: 0.9)
    UserStoreRoyaltyRate.create(person_id: 986682, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 1322783, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 1219613, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 621695, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 615859, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 3436437, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 227182, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 1090860, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 882085, royalty_sub_store_id: 3, royalty_rate: 1)
    UserStoreRoyaltyRate.create(person_id: 3262717, royalty_sub_store_id: 3, royalty_rate: 1)
  end
end

