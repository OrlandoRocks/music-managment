namespace :youtube_oac do
  task flag_expired_channels: :environment do
    batching_numerator = ENV.fetch('NUMBER_OF_BATCHES', 10)

    query = "SELECT
              a.name AS artist_name, p.id person_id, e1.identifier AS channel, e2.identifier AS token
            FROM
              creatives c
            JOIN
              artists a ON a.id = c.artist_id
            JOIN
              people p ON c.person_id = p.id
            JOIN
              external_service_ids e1 ON c.id = e1.linkable_id
                AND e1.linkable_type = 'Creative'
                AND e1.service_name = 'youtube_channel_id'
                AND (e1.verified_at < CURDATE() - INTERVAL 30 DAY OR e1.verified_at is NULL)
            JOIN
              external_service_ids e2 ON c.id = e2.linkable_id
                AND e2.linkable_type = 'Creative'
                AND e2.service_name = 'youtube_authorization'
            GROUP BY
              e1.identifier"
  
    results = ActiveRecord::Base.connection.exec_query(query)
    number_of_rows = results.count
    slice = (results.count > batching_numerator) ? (number_of_rows / batching_numerator) : 1

    results.each_slice(slice) do |batch|
      YoutubeMusic::YoutubeChannelVerificationWorker.perform_async(batch)
    end
  end
end
