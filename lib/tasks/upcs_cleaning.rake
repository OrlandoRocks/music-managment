namespace :upcs do
  desc 'clean upcs with special characters'
  task cleaning: :environment do
    upcs = Upc.where(id: [2082455, 2082424, 2082503, 2081673, 2081830, 2082104, 2082090, 2082675, 2082821, 2082964, 2083010, 2083029, 2082993, 2081926, 2081982, 2082098, 2082188, 2082265, 2082502, 2082695, 2082769, 2082884])

    upcs.each do |upc|
      upc.delete if upc.tunecore_upc == false
    end

    upcs = Upc.where('number LIKE ?', '%+%')

    upcs.each do |upc|
      begin
        wanted_number = upc.number.gsub(/[^0-9A-Za-z]/, '')
        number_of_leading_zeros = 12 - wanted_number.size
        wanted_number = '0' * number_of_leading_zeros + wanted_number
        upc.number = wanted_number
        upc.save!
      rescue => exception
        if upc.tunecore_upc == false
          upc.delete
          Rails.logger.info "[upc deleted] Problem with the optional upc id=#{upc.id} with number #{wanted_number} : " + exception.message
        else
          Rails.logger.info "Problem with the tunecore upc id=#{upc.id} with number #{wanted_number} : " + exception.message
        end
      end
    end

    upcs = Upc.where('number LIKE ?', '%-%')

    upcs.each do |upc|
      begin
        wanted_number = upc.number.gsub(/[^0-9A-Za-z]/, '')
        number_of_leading_zeros = 12 - wanted_number.size
        wanted_number = '0' * number_of_leading_zeros + wanted_number
        upc.number = wanted_number
        upc.save!
      rescue => exception
        if upc.tunecore_upc == false
          upc.delete
          Rails.logger.info "[upc deleted] Problem with the optional upc id=#{upc.id} with number #{wanted_number} : " + exception.message
        else
          Rails.logger.info "Problem with the tunecore upc id=#{upc.id} with number #{wanted_number} : " + exception.message
        end
      end
    end
  end
end
