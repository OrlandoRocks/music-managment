namespace :scrapi_jobs do
  desc "monitor scrapi jobs and alert if no jobs are created or processed"
  task :monitor_scrapi_jobs => :environment do
    Rails.logger.info "Started monitor scrapi jobs task"
    MonitorScrapiJobsService.notify
  end
end
