desc "Add Curated Artist Admin Override Role"
task add_curated_artist_admin_role: :environment do
  Role
    .find_or_create_by(
      name: "Curated Artist",
      long_name: "Curated Artist Admin Override",
      description: "Gives admins the ability to override curated artists",
      is_administrative: true
    )
end

desc "Add Curated Artist Admin Override Role to Admins"
task add_curated_artist_role_to_admins: :environment do
  Rails.logger.info "Adding Curated Artist role to admins"

  admin_ids = ENV.fetch("ADMIN_IDS_FOR_CURATED_ARTIST_ROLE") {
    raise "\nRake task failed\n"\
      "You must pass in ADMIN_IDS_FOR_CURATED_ARTIST_ROLE as a comma separated string\n"\
      "Example: rake add_curated_artist_role_to_admins ADMIN_IDS_FOR_CURATED_ARTIST_ROLE='1,2,3'"
  }.split(",")

  curated_artist_role = Role.find_by(name: "Curated Artist")

  Person.where(id: admin_ids).each do |admin|
    admin.roles << curated_artist_role
  end

  Rails.logger.info "Successfully added Curated Artist role to admins"
end
