desc "TC Social 3 month Promo"
namespace :social_promo do
  task set_price_and_term_length: :environment do
    social_products = Product
      .joins(:subscription_product)
      .where(subscription_products: { product_name: "social", product_type: "monthly" })

    social_products.each do |product|
      product.update(price: 0.to_d)

      product.subscription_product.update(term_length: 3)
    end
  end

  task reset_price_and_term_length: :environment do
    us_social_product = social_product_by_country(CountryWebsite::UNITED_STATES)
    us_social_product.update(price: 7.99)
    us_social_product.subscription_product.update(term_length: 1)

    ca_social_product = social_product_by_country(CountryWebsite::CANADA)
    ca_social_product.update(price: 10.99)
    ca_social_product.subscription_product.update(term_length: 1)

    uk_social_product = social_product_by_country(CountryWebsite::UK)
    uk_social_product.update(price: 6.99)
    uk_social_product.subscription_product.update(term_length: 1)

    au_social_product = social_product_by_country(CountryWebsite::AUSTRALIA)
    au_social_product.update(price: 9.99)
    au_social_product.subscription_product.update(term_length: 1)
  end
end

def social_product_by_country(country_website_id)
  Product.find_by(display_name: "tc_social_monthly", country_website_id: country_website_id)
end
