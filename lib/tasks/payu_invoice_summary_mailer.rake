namespace :payu do
  task :send_invoice_summary_mailer => :environment do
    PayuInvoiceSummaryMailer.invoice_summary.deliver
  end
end
