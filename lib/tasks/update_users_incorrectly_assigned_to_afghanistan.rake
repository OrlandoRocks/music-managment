namespace :transient do
  desc "move users to the correct country"
  task update_users_countries_and_country_websites: :environment do
    start_time = Time.current

    lost_users = Person.where(country: "AF").where({created_on: Date.new(2020,6,7).. })

    countries_changed = Hash.new{ |h,country| h[country] = 0 } 
    country_iso_mapping = {}

    header_names = ['User Id', 'User Country Website Id', 'User Country', 'Action Taken']
    updated_users_file = CSV.open(Rails.root.join("tmp","updated_users_#{Time.current.strftime("%Y_%m_%d_%H%M%S")}.csv"), "wb", headers: true)
    updated_users_file << header_names

    lost_users.each do |user|
      country_from_ip = LoginEvent.find_by(person_id: user.id)&.country
      country_from_ip_iso = find_country_iso_code(country_from_ip, country_iso_mapping)

      if country_from_ip_iso && has_not_converted(user)
        # if the country is India, change country to India and keep country_website as 1
        if ["IN", "US"].include?(country_from_ip_iso)
          user.update(country: country_from_ip_iso)
          updated_users_file << [user.id, user.country_website_id, country_from_ip, 'Changed country based on IP']
        else
          user.update(country: country_from_ip_iso, country_website_id: CountryWebsite.country_id_for(country_from_ip_iso))
          CountryWebsiteUpdater::Update.update(person: user)
          updated_users_file << [user.id, user.country_website_id, country_from_ip, 'Changed country AND country_website_id based on IP']
        end
        countries_changed[country_from_ip] += 1
      else 
        if !country_from_ip_iso
          user.update(country: "US")
          updated_users_file << [user.id, user.country_website_id, 'United States', 'Changed country to US']
        else 
          user.update(country: country_from_ip_iso)
          updated_users_file << [user.id, user.country_website_id, country_from_ip, 'Changed country based on IP']
        end
      end
    end

    puts "Task complete. Task took #{(Time.current - start_time)/60.to_i} minutes"
    p country_iso_mapping
    p countries_changed
  end
end

def has_not_converted(person)
  has_no_purchases(person) && has_no_invoices(person) && has_no_balance(person)
end

def has_no_purchases(person)
  person.purchases.length == 0
end

def has_no_invoices(person)
  person.invoices.length == 0
end

def has_no_balance(person)
  person.balance == 0
end

def find_country_iso_code(country_from_ip, country_iso_mapping)         
  if country_iso_mapping[country_from_ip]
    country_iso_mapping[country_from_ip]
  else
    country_from_ip_iso = Country.find_by(name: country_from_ip)&.iso_code
    country_iso_mapping[country_from_ip] = country_from_ip_iso
    country_from_ip_iso
  end
end