namespace :targeted_offer do
  task :remove_expired_offers_from_purchases_in_cart => :environment do |t|
    unless ENV['NUM_DAYS']
      puts "please pass a NUM_DAYS"
      exit(0)
    end

    log(t, "Begin recalculating purchases with expired offers")

    realtime = Benchmark.realtime do
      TargetedOffer.where("expiration_date < CURRENT_TIMESTAMP and expiration_date > '#{Time.at(ENV['NUM_DAYS'].to_i.days.ago).to_s(:db)}'").each { |to| to.purchases.unpaid.each(&:recalculate) }
    end

    log(t, "Finish recalculating purchases; realtime: #{realtime}")
  end
end