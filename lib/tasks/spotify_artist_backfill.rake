namespace :spotify do
  task ingest_spotify_artist_ids: :environment do
    roles = {
      "main artist" => :primary_artist,
      "featured artist" => :contributor,
      "remixer" => :remixer
    }

    number_of_records = ENV['RECORDS'] || 0
    file_path = ENV['CSV_FILEPATH']
    record_index_start = $redis.get("tunecore_spotify_artist_id_20180101_20191126") || 0
    record_index_end = record_index_start.to_i + number_of_records.to_i
    $redis.set("tunecore_spotify_artist_id_20180101_20191126", record_index_end)
    spotify_artists = CSV.open(file_path, headers: :true, header_converters: :symbol)
                         .map(&:to_h)[record_index_start.to_i...record_index_end]

    unloaded_artists = []
    spotify_artists.each do |artist|
      artist_name = artist[:artist_name].downcase if artist[:artist_name]
      role = roles[artist[:artist_role]]

      if role == :remixer
        creative = Upc.where(number: artist[:upc]).first
                      .upcable.creatives.joins(:song_roles)
                      .where(song_roles: { role_type: :remixer })
                      .joins(:artist)
                      .where('LOWER(artists.name) LIKE (?)', "%#{artist_name}%")
                      .first
      else
        creative = Upc.where(number: artist[:upc]).first
                      .upcable.creatives.where(role: role)
                      .joins(:artist)
                      .where('LOWER(artists.name) LIKE (?)', "%#{artist_name}%")
                      .first
      end

      spotify_artist_id = artist[:artist_uri].scan(/^spotify:artist:(.+)$/).flatten.first
      if creative.present?
        creative.set_external_id_for("spotify", spotify_artist_id)
      else
        unloaded_artists << artist
      end
    end
    puts "The Following Artists Failed to write to the data base: \n: #{unloaded_artists}"
  end
end
