namespace :external_service_ids do
  task match_missing_artist_external_ids: :environment do
    service_name_var = service_name

    #creatives that do not have a external id
    creatives_missing_ids = Creative.joins(:external_service_ids)
                                            .where(external_service_ids: {
                                                    identifier: ["unavailable", nil],
                                                    service_name: service_name_var
                                                  })
    Rails.logger.info("Found #{creatives_missing_ids.count} creatives without a #{service_name_var} id")

    #artists with creatives missing external ids
    artist_missing_ids = creatives_missing_ids.pluck(:artist_id).uniq

    #artist which have other creatives with at least one external id for same service
    artist_creatives_with_existing_ids = Creative.joins(:external_service_ids)
                                        .where(artist_id: artist_missing_ids)
                                        .where(external_service_ids: { service_name: service_name_var })
                                        .where.not(external_service_ids: { identifier: ['unavailable', nil] })
                                        .pluck(:artist_id).uniq
    Rails.logger.info("Found #{artist_creatives_with_existing_ids.count} associated artists with ids for #{service_name_var}")

    #creatives missing external ids which should be matched through artist record
    creatives_to_be_matched = creatives_missing_ids.where(artist_id: artist_creatives_with_existing_ids)
    Rails.logger.info("Starting matching process for #{creatives_to_be_matched.count} creatives")

    ArtistIds::CreationService.match(creatives_to_be_matched)
  end

  def service_name
     services = [ExternalServiceId::APPLE_SERVICE, ExternalServiceId::SPOTIFY_SERVICE]
     services.detect { |s| s == ENV['service'] }
  end


  task apple_ids_not_created: :environment do

    albums_missing_apple_ids = Album.joins(:creatives)
                                    .joins(:salepoints)
                                    .joins("JOIN external_service_ids esi
                                            ON  esi.linkable_id = creatives.id
                                            AND esi.linkable_type = 'Creative'
                                            AND esi.state = 'new_artist'
                                            AND esi.identifier = 'unavailable'
                                            AND esi.service_name = 'apple'")
                                    .where(albums: {
                                              takedown_at: nil
                                            },
                                            salepoints: {
                                                store_id: 36
                                            })
                                    .where
                                    .not(albums: {
                                            finalized_at: nil
                                         },
                                         salepoints: {
                                          finalized_at: nil
                                    })
    albums_missing_apple_ids.each do |album|
      Apple::CreateAppleArtistsServiceWorker.perform_async(album.id)
    end
  end
end
