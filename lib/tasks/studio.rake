namespace :studio do
desc "Sync your local database with a remote one USER=you REMOTE=name_of_database LOCAL=your_local_db"
  task :sync do   
     puts
     puts "--" * 31
     puts "--            SETTING UP SSH TUNNEL FOR DB SYNC             --"
     puts "--" * 31
     puts
     
     open_tunnel 
    
     puts 
     puts "--" * 31
     puts "--  PREPARING TO SYNC REMOTE DATABASE TO YOUR LOCAL MACHINE --"
     puts "--                 THIS WILL TAKE A WHILE!                  --"
     puts "--" * 31
     puts 
#    `time mysqldump -h127.0.0.1 -P 3307 --ignore-table=tunecore_production.us_zip_codes --ignore-table=tunecore_production.us_zip_code_prefixes --ignore-table=tunecore_production.sessions -u dumpdb2  #{ENV['REMOTE']} |  mysql -u root #{ENV['LOCAL'] || "tunecore_development"}`

       `time mysqldump -h127.0.0.1 -P 3307   -u dumpdb2  tc_daily |  mysql -u root #{ENV['LOCAL'] || "tunecore_development"}`


     kill_tunnel
    
  end
end

def open_tunnel
  os = `uname -s`
  if os != "Linux"
    system("ssh -i /Users/#{ENV['USER']}/.ssh/db_rsa -p 22022 -fNg -L 3307:127.0.0.1:3306 dumpdb2@d2.tunecore.net")
  else
    system("ssh -i /home/#{ENV['USER']}/.ssh/db_rsa -p 22022 -fNg -L 3307:127.0.0.1:3306 dumpdb2@d2.tunecore.net")
  end
end

def kill_tunnel
  os = `uname -s`
  if os != "Linux"
    system("kill -9 `ps -ax | grep -e ssh.*3307:127.0.0.1:3306.*dumpdb2@ | egrep -v grep | awk '{print $1}'`")
  else
   system("kill -9 `ps -ef | grep -e ssh.*3307:127.0.0.1:3306.*dumpdb2@ | egrep -v grep | awk '{print $2}'`")
  end
end
