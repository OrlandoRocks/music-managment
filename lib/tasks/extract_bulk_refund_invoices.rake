namespace :extract_refund_invoices_in_bulk do
  desc "upload refund invoices to s3"
  task :inbound, [:start_date, :end_date] => :environment do |task, args|
    RefundExport::InboundBatchWorker.perform_async(args.start_date, args.end_date)
    puts "Scheduled worker to upload inbound refund invoices"
  end

  task :outbound, [:start_date, :end_date] => :environment do |task, args|
    RefundExport::OutboundBatchWorker.perform_async(args.start_date, args.end_date)
    puts "Scheduled worker to upload outbound refund invoices"
  end
end
