namespace :countries do
  desc "Adds sanctioned countries into the countries table"
  task :populate_sanctioned_countries => :environment do
    info = CSV.open("db/seed/csv/data/sanctioned_countries.csv")
    info.read.each do |country|
      exists = Country.unscoped.find_by(iso_code: country[1])
      if exists
        exists.update(is_sanctioned: true)
      else
        Country.create!(
          name: country[0],
          iso_code: country[1],
          region: country[2],
          sub_region: country[3],
          tc_region: country[4],
          iso_code_3: country[5],
          is_sanctioned: true
        )
      end
    end
  end
end
