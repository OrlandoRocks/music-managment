
namespace :ringtone_credits_update do

  PROD_CONV = {
    13 => 111,
    14 => 112,
    15 => 113,
    16 => 114,
    17 => 115,
    72 => 116,
    73 => 117,
    74 => 118,
    75 => 119,
    76 => 120
  }

  PROD_ITEM_CONV = {
    8  => 78,
    9  => 79,
    10 => 80,
    11 => 81,
    12 => 82,
    45 => 83,
    46 => 84,
    47 => 85,
    48 => 86,
    49 => 87
  }

  desc "Add New Ringtone Credit Products"
  task :sept_2013_new_products => :environment do
    Product.transaction do

      ## Initial attributes for new ringtone credit products
      new_ringtone_attributes = {
        :created_by_id => 1,
        :country_website_id=>CountryWebsite::UNITED_STATES,
        :currency => "USD",
        :renewal_level => "Item",
        :status => "Active",
        :product_type => "Package",
        :is_default => 1,
        :applies_to_product => "None"
      }

      ## Initial product item attributes for ringtone credit products
      product_item_attributes = {
        :base_item_id => 3,
        :name => "Ringtone Distribution",
        :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
        :price => "0.00",
        :currency => "USD",
        :does_not_renew => 0,
        :renewal_type => "distribution",
        :first_renewal_duration => 1,
        :first_renewal_interval => "year",
        :renewal_duration => 1,
        :renewal_interval => "year",
        :renewal_product_id => 101
      }

      ## Initial attributes for product item rules
      ringtone_prod_item_rule_attributes = {
        :base_item_option_id => 70,
        :rule_type => "inventory",
        :rule => "price_for_each",
        :inventory_type => "Ringtone",
        :quantity => 1,
        :price => 0.00,
        :currency => "USD",
        :true_false => 0,
        :minimum => 0,
        :maximum => 0
      }

      song_prod_item_rule_attributes = {
        :base_item_option_id => 71,
        :rule_type => "inventory",
        :rule => "total_included",
        :inventory_type => "Song",
        :model_to_check => "Ringtone",
        :method_to_check => "songs.count",
        :quantity => 1,
        :price => 0.00,
        :currency => "USD",
        :true_false => 0,
        :minimum => 0,
        :maximum => 0
      }

      salepoint_prod_item_rule_attributes = {
        :base_item_option_id => 74,
        :rule_type => "inventory",
        :rule => "total_included",
        :inventory_type => "Salepoint",
        :model_to_check => "Ringtone",
        :method_to_check => "salepoints.count",
        :unlimited => 1,
        :quantity => 0,
        :price => 0.00,
        :currency => "USD",
        :true_false => 0,
        :minimum => 0,
        :maximum => 0
      }

      salepoints_above_prod_item_rule_attributes = {
        :base_item_option_id => 75,
        :rule_type => "price_only",
        :rule => "price_for_each_above_included",
        :inventory_type => "Salepoint",
        :model_to_check => "Ringtone",
        :method_to_check => "salepoints.count",
        :unlimited => 1,
        :quantity => 0,
        :price => 0.99,
        :currency => "USD",
        :true_false => 0,
        :minimum => 0,
        :maximum => 0
      }

      [13, 14, 15, 16, 17].each do |prod_id|
        credit = Product.find(prod_id)
        credit.update(:status=>"Inactive")
        quantity = credit.product_item_rules.where("parent_id is NULL OR parent_id = 0").first.quantity

        new_ringtone_attributes[:name] = credit.name
        new_ringtone_attributes[:flag_text] = credit.flag_text
        new_ringtone_attributes[:display_name] = credit.display_name
        new_ringtone_attributes[:description] = credit.description
        new_ringtone_attributes[:sort_order] = credit.sort_order
        new_ringtone_attributes[:price] = credit.price

        prod = Product.create!(new_ringtone_attributes)
        pi = ProductItem.create!(product_item_attributes.merge(:product_id => prod.id))

        pi.product_item_rules.create!(ringtone_prod_item_rule_attributes.merge(:quantity => quantity))
        pi.product_item_rules.create!(song_prod_item_rule_attributes)
        pi.product_item_rules.create!(salepoint_prod_item_rule_attributes)
        pi.product_item_rules.create!(salepoints_above_prod_item_rule_attributes)
      end

      ####
      # Update for CAD
      ####
      new_ringtone_attributes[:country_website_id] = CountryWebsite::CANADA
      new_ringtone_attributes[:currency] = "CAD"

      product_item_attributes[:base_item_id] = 13
      product_item_attributes[:currency] = "CAD"
      product_item_attributes[:renewal_product_id] = 102

      ringtone_prod_item_rule_attributes[:base_item_option_id] = 99
      ringtone_prod_item_rule_attributes[:currency] = "CAD"

      song_prod_item_rule_attributes[:base_item_option_id] = 100
      song_prod_item_rule_attributes[:currency] = "CAD"

      salepoint_prod_item_rule_attributes[:base_item_option_id] = 101
      salepoint_prod_item_rule_attributes[:currency] = "CAD"

      salepoints_above_prod_item_rule_attributes[:base_item_option_id] = 102
      salepoints_above_prod_item_rule_attributes[:currency] = "CAD"
      # =======================================================================

      [72, 73, 74, 75, 76].each do |prod_id|
        credit = Product.find(prod_id)
        credit.update(:status=>"Inactive")
        quantity = credit.product_item_rules.where("parent_id is NULL OR parent_id = 0").first.quantity

        new_ringtone_attributes[:name] = credit.name
        new_ringtone_attributes[:flag_text] = credit.flag_text
        new_ringtone_attributes[:display_name] = credit.display_name
        new_ringtone_attributes[:description] = credit.description
        new_ringtone_attributes[:sort_order] = credit.sort_order
        new_ringtone_attributes[:price] = credit.price

        prod = Product.create!(new_ringtone_attributes)
        pi = ProductItem.create!(product_item_attributes.merge(:product_id => prod.id))

        pi.product_item_rules.create!(ringtone_prod_item_rule_attributes.merge(:quantity => quantity))
        pi.product_item_rules.create!(song_prod_item_rule_attributes)
        pi.product_item_rules.create!(salepoint_prod_item_rule_attributes)
        pi.product_item_rules.create!(salepoints_above_prod_item_rule_attributes)
      end

    end
  end

  desc "Update Previous Ringtone Credit Purchase Records"
  task :sept_2013_update_previously_purchased => :environment do
    filename = ENV["PURCHASES_CSV"] || raise("Missing variable PURCHASES_CSV")
    Rails.logger.info "Updating purchases for purchases in #{filename}"
    puts "Updating purchases for purchases in #{filename}"

    CSV.foreach(filename) do |line|
      begin
        purchase_id = line[0].to_i
        purchase = Purchase.find(purchase_id)

        old_id = purchase.product_id
        new_id = PROD_CONV[old_id]
        if purchase.update(:product_id => new_id, :related_id => new_id)
          Rails.logger.info "Updated purchase #{purchase_id} from #{old_id} to #{new_id}"
        else
          Rails.logger.info "Unable to update purchase #{purchase_id}"
          puts "Unable to update purchase #{purchase_id}"
        end

      rescue StandardError => e
        Rails.logger.info e
        puts e
      end
    end
  end

  desc "Update Previous Ringtone Credit Inventory Records"
  task :sept_2013_update_credit_inventories => :environment do
    filename = ENV["INVENTORIES_CSV"] || raise("Missing variable INVENTORIES_CSV")
    Rails.logger.info "Updating inventories from #{filename}"
    puts "Updating inventories from #{filename}"

    CSV.foreach(filename) do |line|
      begin
        inventory_id = line[0].to_i
        i = Inventory.find(inventory_id)

        old_id = i.product_item_id
        new_id = PROD_ITEM_CONV[old_id]

        if i.update(:product_item_id => new_id)
          Rails.logger.info "Updated inventory #{inventory_id} from #{old_id} to #{new_id}"
        else
          Rails.logger.info "Unable to update #{inventory_id}"
          puts "Unable to update #{inventory_id}"
        end

      rescue
        Rails.logger.info e
        puts e
      end

    end
  end

  desc "Update Previous Ringtone Credit Renewal Records"
  task :sept_2013_update_credit_renewals => :environment do
    filename = ENV["RENEWALS_CSV"] || raise("Missing variable RENEWALS_CSV")
    Rails.logger.info "Updating renewals from #{filename}"
    puts "Updating renewals from #{filename}"

    CSV.foreach(filename) do |line|
      begin
        renewal_id = line[0].to_i
        r = Renewal.find(renewal_id)

        old_id = r.item_to_renew_id
        new_id = PROD_ITEM_CONV[old_id]

        if r.update(:item_to_renew_id => new_id)
          Rails.logger.info "Updated renewal #{renewal_id} from #{old_id} to #{new_id}"
        else
          Rails.logger.info "Unable to update #{renewal_id}"
          puts "Unable to update #{renewal_id}"
        end

      rescue
        Rails.logger.info e
        puts e
      end
    end
  end

end
