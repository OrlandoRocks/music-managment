# frozen_string_literal: true

# bundle exec "rake cart:clear_persons_cart[#person_id]"
namespace :cart do
  desc "Clear the cart for a specified person"
  task :clear_persons_cart, [:person_id] => :environment do |_task, args|
    raise ArgumentError
      .new("Please specify a person_id") if args[:person_id].blank?
    person = Person.find(args[:person_id])
    unless person
      Rails.logger.info "Cannot find a user with the id #{args[:person_id]} 😢"
    end

    person.clear_cart!
    Rails.logger.info "We cleared the cart for #{person.name}, id: #{person.id}  🛒"
  end
end
