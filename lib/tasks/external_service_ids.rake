namespace :esids do
  desc "Run ArtistIdMatcherWorker for Creative IDs"
  task match: :environment do
    raise "Must set an CREATIVE_ID_FILE_PATH" unless ENV['CREATIVE_ID_FILE_PATH']

    file_path = ENV['CREATIVE_ID_FILE_PATH']

    puts 'RUNNING TASK...'

    File
      .readlines(file_path.to_s)
      .map(&:chomp)
      .each_slice(100) do |ids|
        ArtistIdMatcherWorker.perform_async(ids)
      end

    puts 'TASK COMPLETE'
  end
end
