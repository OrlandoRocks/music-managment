namespace :self_identified_address_lock do
  desc "Enable self_identified_address_lock flag for given email"
  task :enable_for_user, [:email] => :environment do |_task, args|
    raise ArgumentError
      .new("required user's email as argument") if args.email.blank?

    email = args.email
    person = Person.find_by!(email: email)
    flag_reason = FlagReason
                  .address_lock_flag_reasons
                  .find_by!(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)

    if person.lock_address!(reason: flag_reason)
      Rails.logger.info "Locked address for #{email}"
    end
  end

  desc "Disable self_identified_address_lock flag for given email"
  task :disable_for_user, [:email] => :environment do |_task, args|
    raise ArgumentError
      .new("required user's email as argument") if args.email.blank?

    email = args.email
    person = Person.find_by!(email: email)
    flag_reason = FlagReason
                  .address_lock_flag_reasons
                  .find_by!(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)

    if person.unlock_address!(flag_Reason: flag_reason)
      Rails.logger.info "Unlocked address for #{email}"
    end
  end
end
