namespace :payout_taxes do
  namespace :add_withholding_exemptions do
    desc "Add People to tax withholding exemption"
    task run: :environment do
      people_ids = RakeDatafileService
        .csv("withholding_exemption_list.csv")
        .entries
        .pluck("person_id")

      Person.where(id: people_ids).map do |person|
        person.exempt_withholding!(:contractual_or_business_decision)
        person.block_withdrawal! if person.reload.exempt_from_withholding?

        puts "PERSON ID: #{person.id}, WITHHOLDING_EXEMPTION: #{person.exempt_from_withholding?}, WITHDRAWAL_BLOCKING: #{person.blocked_withdrawal?}" 
      end
    end
  end

  namespace :hard_reset_withholding_exemptions do
    task run: :environment do
      bytedance = {
        id:                              1,
        person_id:                       3820050,
        started_at:                      Date.new(2022).beginning_of_year,
        ended_at:                        nil,
        withholding_exemption_reason_id: 1,
        revoked_at:                      nil
      }
      
      task = ->(bytedance) do
        return "Done all ready!" if WithholdingExemption.where(bytedance).exists?

        WithholdingExemption.connection.execute(
          "truncate table #{WithholdingExemption.table_name}"
        )
        WithholdingExemption.connection.execute(
          "ALTER TABLE #{WithholdingExemption.table_name} AUTO_INCREMENT = 1"
        )
        WithholdingExemption.create(bytedance)

        "#{WithholdingExemption.table_name} TRUNACTED! -> Bytedance inserted!"
      end

      puts task.(bytedance)
    end
  end
end
