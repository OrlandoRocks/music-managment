namespace :payout_taxes do
  desc "rake task to be run after deployment for epic/payout-taxes"
  task post_deploy: :environment do

    # Order matters
    tasks = [
      "transient:reset_rollover_transactions",
      "transient:reset_rollover_and_withholding",
      "transient:reset_withholding_transactions",
      "transient:manual_rollover_withholding_correction",
      "transient:backfill_balance_adjustment_facebook_category",
      "transient:manual_rollover_accounts_with_encumbrances",
      "transient:taxable_earnings_rollover_correction:rollover",
      "revenue_streams:create_base_configs"
    ]

    # Invokes each rak task in order.
    tasks.each do |task|
      Rake::Task[task].invoke
    end

    # After rake tasks are run, we invoke each sidekiq in serial.
    TaxWithholdings::SipBatchWorker.new.perform
    TaxWithholdings::MassAdjustmentsBatchWorker.new.perform

    Rake::Task["transient:expel_withholding_transactions"].invoke
  end
end
