namespace :direct_advance do
  desc "checks TCDA eligibility, creates & uploads sales reports, and
        enables/disables DirectAdvance feature for eligible users if
        optional second argument is true"

  task :create_reports_and_set_feature_flags => :environment do
    Rails.application.eager_load!
    test_run = ENV["TEST_RUN"] == 'true'
    end_date = ENV["END_DATE"] || Date.today

    DirectAdvance::ManagerWorker.new.perform(end_date, test_run)
  end
end
