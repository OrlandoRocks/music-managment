namespace :transient do
  task send_terms_and_conditions_update: :environment do
    path = Rails.root.join(ENV.fetch('T_AND_C_UPDATE_LEADS', nil))
    csv = CSV.readlines(path, headers: true)

    csv.each do |person|
      MailerWorker.perform_async("PersonNotifier", :terms_and_conditions_update, person["person_id"])
    end
  end
end
