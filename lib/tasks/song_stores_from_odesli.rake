namespace :song_stores_from_odesli do
  API_LIMIT_STATUS_CODE = 429
  # Matching ESI Albums' batch size for each traversal
  ESI_ALBUM_LIST_BATCH_SIZE = 1000
  # Song count to avoid Odesli API call limit of 60/min
  SONG_COUNT_MAX_LIMIT = 5
  LAST_ESI_ID = "Last_esi_id".freeze
  # Last esi id is saved in redis key. Next time when task is run, only esi albums having id
  # greater than the saved value is considered for scraping

  desc "fetch other stores' release identifiers with odesli API, using existing spotify and apple song urls"
  task fetch_songs_with_odesli: :environment do
    odesli_scraping_logger
    @logger.info "scraping start time: #{Time.current}"
    # Conditions for scraping: Find ExternalServiceId records of albums having linkable id as album ids,
    # linkable type as 'Album', only albums of tc-social users, albums having
    # takedown_at value as nil, ExternalServiceIds' service name being 'spotify' or 'apple'
    store_array = [ExternalServiceId::SPOTIFY_SERVICE, ExternalServiceId::APPLE_SERVICE]
    last_esi_id = $redis.get(LAST_ESI_ID).nil? ? 0 : $redis.get(LAST_ESI_ID)
    esi_albums = ExternalServiceId.joins("left join albums ON external_service_ids.linkable_id = albums.id AND
                                          external_service_ids.linkable_type = 'Album'
                                          inner join people on people.id = albums.person_id
                                          inner join oauth_tokens on oauth_tokens.user_id = people.id
                                          inner join client_applications on
                                          client_applications.id = oauth_tokens.client_application_id and
                                          client_applications.name = 'tc_social'")
    .where("albums.takedown_at IS NULL")
    .where('service_name IN (?)', store_array)
    .order(id: :asc)
    .where('external_service_ids.id > ?', last_esi_id)
    .uniq
    @logger.info "ESI albums(criteria matching) count #{esi_albums.count}"

    # Traverse through song_ids, keep a count of valid song_ids,
    # after each batch of 50, put a delay for 1 min and reset count
    # so that odesli API limit of 60/min is not attained
    scrapable_song_count = 0
    last_traversed_esi_album_id = 0
    esi_albums.each_slice(ESI_ALBUM_LIST_BATCH_SIZE) do |esi_album_batch|
      esi_album_batch.each do |esi_album|
        last_traversed_esi_album_id = esi_album.id - 1
        album = esi_album.linkable
        next if album.nil?

        song_ids = album.songs.ids
        @logger.info "Album id: #{album.id} Song ids count: #{song_ids.count}"
        song_ids.each do |song_id|
          esi_song = ExternalServiceId.find_by(linkable_id: song_id,
                                               linkable_type: "Song",
                                               service_name: esi_album.service_name)
          next if esi_song.nil?

          @logger.info "Fetching esi song for #{song_id}"
          service_name = esi_song.service_name
          song_url = nil
          song_url = "#{ENV['SPOTIFY_STORE_BASE_URL']}/track/#{esi_song.identifier}" if service_name == 'spotify'
          song_url = "#{ENV['APPLE_STORE_BASE_URL']}/album/#{esi_album.identifier}?i=#{esi_song.identifier}" if service_name == 'apple'
          next if song_url.nil?

          # Update song_count, if valid song_url present
          scrapable_song_count = scrapable_song_count + 1

          @logger.info "Created song url #{song_url}"
          response_body = fetch_song_details_with_odesli(song_url)
          next if response_body.nil?

          stores_response_hash = response_body['entitiesByUniqueId']
          next if stores_response_hash.nil?

          stores_names_ids_combined_list = stores_response_hash.keys
          store_names_ids_hash = []
          stores_names_ids_combined_list.each do |name_id_combined|
            name_id_array = name_id_combined.split('::')
            store_names_ids_hash << { name: name_id_array.first.split("_").first.downcase, id: name_id_array.last }
          end

          # Fetches stores to where the album has been distributed through TC
          tc_distributed_albums = album.distributions.where(state: "delivered")
          album_stores_combined = tc_distributed_albums.map {
            |tc_distr_album| tc_distr_album.stores.map(&:name).map(&:downcase)
          }.flatten.uniq
          album_stores = album_stores_combined.map { |store| store.split('/') }.flatten.uniq
          tc_album_stores = album_stores.map { |store| store.split.first }

          @logger.info "album_id: #{album.id}, stores distributed: #{tc_album_stores}"
          # Fetches stores from where scraping has to be done
          stores_to_be_fetched = eval(ENV['STORE_ORDER']).keys.map(&:to_s)
          # Select only needed stores
          stores_needed = tc_album_stores.select { |store| stores_to_be_fetched.include? store }
          stores_obtained_from_odesli = store_names_ids_hash.map{ |odesli_hash| odesli_hash[:name] }
          @logger.info "album_id: #{album.id}, odesli stores: #{stores_obtained_from_odesli}"
          available_stores = stores_needed.select { |store| stores_obtained_from_odesli.include? store }
          @logger.info "album_id: #{album.id}, needed stores: #{available_stores}"
          next if available_stores.blank?

          new_esi_entries = []
          available_stores.each do |available_store|
            odesli_hash = store_names_ids_hash.detect { |store| store[:name] == available_store }
            esi = ExternalServiceId.where(external_services_id_params(song_id, odesli_hash))
            # Create record if its not existing already
            @logger.info "New esi with song id: #{song_id},
                        album id: #{album.id}
                        name: #{odesli_hash[:name]}
                        identifier : #{odesli_hash[:id]}" if esi.blank?
            new_esi_entries << external_services_id_params(song_id, odesli_hash) if esi.blank?
          end
          ExternalServiceId.import new_esi_entries, on_duplicate_key_update: :all
          if scrapable_song_count >= SONG_COUNT_MAX_LIMIT
            sleep(1.minutes)
            scrapable_song_count = 0
          end
        end
      end
    rescue StandardError => e
      $redis.set(LAST_ESI_ID, last_traversed_esi_album_id) unless $redis.nil?
      Rails.logger.info e.message
    end
    $redis.set(LAST_ESI_ID, ExternalServiceId.last.id) unless $redis.nil?
    @logger.info "scraping end time: #{Time.current}"
  end

  # Odesli API limits 60 requests per min.
  # Delay request for 1 min if api limit error received
  def fetch_song_details_with_odesli(song_url)
    client = Faraday.new
    odesli_url = "#{ENV['ODESLI_BASE_URL']}?url=#{song_url}&key=#{ENV['ODESLI_API_KEY']}"
    @logger.info "Calling odesli API for #{odesli_url}"
    response = client.get odesli_url
    response_body = JSON.parse(response.body)
    if response_body['entitiesByUniqueId'].nil?
      if response_body['statusCode'].present?
        @logger.info "Received error in odesli API for #{odesli_url}, statusCode: #{response_body['statusCode']},
                      code: #{response_body['code']}"
        if response_body['statusCode'] == API_LIMIT_STATUS_CODE
          sleep(1.minutes)
          fetch_song_details_with_odesli(song_url)
        else
          return nil
        end
      end
    end
    @logger.info "Recieved response in odesli API for #{odesli_url}"
    response_body
  end

  def external_services_id_params(song_id, odesli_hash)
    {
      linkable_id: song_id,
      linkable_type: "Song",
      service_name: odesli_hash[:name],
      identifier: odesli_hash[:id]
    }
  end

  def odesli_scraping_logger
    @logger = Logger.new('log/odesli_scraping_logger.log')
    @logger.level = Logger::INFO
  end
end
