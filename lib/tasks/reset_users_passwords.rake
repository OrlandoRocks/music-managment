namespace :reset_users_passwords do
  desc "Resets users password and emails them"
  task :run, [:emails_csv] => [:environment] do |_, args|
    users_list   = args[:emails_csv]
    # csv format: email
    password_change_users = RakeDatafileService.csv(users_list).entries.map do |user_email_record|
      user_email_record[0]
    end

    Person.where(email: password_change_users).each do |person|
      new_password = Faker::Internet.password(min_length: 10) + "1$"
      password_saved = person.update(password: new_password, password_confirmation: new_password)
      if password_saved
        puts "SUCC [PASSWORD UPDATE TASK] #{person.email}: PASSWORD UPDATED SUCCESSFULLY"
        PasswordResetService.new(person.email).reset(:system)
      else
        puts "FAIL [PASSWORD UPDATE TASK] #{person.email}: UNABLE TO UPDATE PASSWORD"
      end
    end
  end
end
