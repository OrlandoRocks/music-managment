namespace :tax_reports do
  desc "Annual 1099 Report Generation For Use with 1099 Pro by Finance Team"
  task :generate_1099_report, [:report_year, :w9_report_filename] => :environment do |_t, args|
    # NOTE: LOCAL TESTING FILENAME = "test_w9_report_2022.csv"
    raise ArgumentError if args[:report_year].blank? || args[:w9_report_filename].blank?

    worker = TaxReports::AnnualTaxReportGenerationWorker
      .perform_async(args[:report_year], args[:w9_report_filename])

    Rails.logger.info(<<-LOG_ENTRY)
      [1099 GENERATION TASK] Report Generation Process Initialized 
        YEAR: #{args[:report_year]},
        W9_REPORT_S3_KEY: #{args[:w9_report_filename]}
        LOG_WORKER_ID: #{worker}
    LOG_ENTRY
  end

  desc "Fetch an IRS Payables Report, and saves it in the /tmp directory"
  task :fetch_irs_payables_report, [:irs_transaction_id] => :environment do |_t, args|
    raise ArgumentError if args[:irs_transaction_id].blank?
    
    puts "Fetching report..."

    irs_transaction = IRSTransaction.find(args[:irs_transaction_id])
    file = TaxWithholdings::IRSPayablesReport.new(irs_transaction: irs_transaction).fetch

    puts "Report saved in #{file.to_path}"
  end

  desc "Generate an IRS Payables Report"
  task :generate_irs_payables_report, [:irs_transaction_id] => :environment do |_t, args|
    puts "Generating report..."
    
    irs_transaction = IRSTransaction.find_by(id: args[:irs_transaction_id]).presence
    TaxWithholdings::IRSPayablesReport.new(irs_transaction: irs_transaction).generate(async: false)

    puts "Report generation completed"
  end
end
