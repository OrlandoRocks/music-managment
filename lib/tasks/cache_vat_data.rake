namespace :tc_vat do
  desc "Cache the Vat Applicability API so that it can be used on VAT Service failure. CRON:  0 0 * * *"
  task cache_vat_applicability: :environment do
    update_cache('inbound', 'vat_applicable_countries_inbound')
    update_cache('outbound', 'vat_applicable_countries')
    puts "Updated the redis cache for vat applicable countries"
  end
end

def update_cache(type, key)
  response = TcVat::ApiClient.new.send_request!(:applicable, transaction_type: type)
  $redis.set(key, response.data.to_json)
end
