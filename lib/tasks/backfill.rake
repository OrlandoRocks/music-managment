namespace :backfill do
  desc "backfill the paid_at field on purchases"
  task purchases_paid_at: :environment do
    Purchase.where(paid_at: '0000-00-00 00:00:00').update_all(paid_at: nil)
  end

  desc "ytsr salepoints https://github.com/tunecore/tc-www/wiki/Queries-to-backfill-Ytsr-Salepoints"
  task ytsr_salepoints: :environment do
  end

  desc "undelivered releases https://github.com/tunecore/tc-www/wiki/Retrieve-releases-that-weren't-delivered-to-all-stores"
  task undelivered_releases: :environment do
  end

  desc "backfill the tunecore_isrc field on song"
  task update_tunecore_isrc: :environment do
    isrc_conditions = [
      "optional_isrc is null and tunecore_isrc is null",
      "tunecore_isrc in (select distinct(optional_isrc) from songs where optional_isrc like 'TC%' and tunecore_isrc is null)",
      "optional_isrc like 'TC%' and tunecore_isrc is null",
      "optional_isrc not like 'TC%' and tunecore_isrc is null"
    ]
    isrc_conditions.each do |where_clause|
      Rails.logger.info("#{where_clause} started")
      Song.where("#{where_clause}").find_each do |song|
        song.send(:update_tunecore_isrc)
      end
      Rails.logger.info("#{where_clause} completed")
    end
  end

  desc "backfill RoyaltyPayment.code"
  task copy_counterpoint_code_to_code: :environment do
    ActiveRecord::Base.connection.execute "UPDATE royalty_payments SET code = counterpoint_code"
  end

  desc "backfill Joox for language of performance node"
  task backfill_joox_language_node: :environment do
      JOOX_STORE_ID = 92
      limit = ENV['LIMIT']&.to_i
      id_filter = $redis.get("joox_backfill_id")&.to_i || 0
      distros = Distribution.joins(:salepoints, :delivery_batch_items)
                            .where("distributions.id > ?", id_filter)
                            .where("salepoints.store_id = ?", JOOX_STORE_ID)
                            .where("delivery_batch_items.status = 'sent'")
                            .where("delivery_batch_items.updated_at < '2020-07-29'")
                            .order(:id).limit(limit)
      distros.each do |d|
        Delivery::DistributionWorker.set(queue: "delivery-default").perform_async(d.class.name, d.id, 'metadata_only')
      end
      id_index = distros.last&.id

    if id_index.empty?
      Tunecore::Airbrake.notify("backfill:backfill_joox_language_node rake task complete, remove recurring rake task")
      raise "Recurring Joox Backfill Language Job Complete!"
    else
       $redis.set("joox_backfill_id", id_index)
    end
  end
end
