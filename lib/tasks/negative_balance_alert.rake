task negative_balance_alert: :environment do
  Rails.logger.info("Starting Negative Balance Alerting Service for #{Date.yesterday}")
  NegativeBalanceAlertingService.alert
end
