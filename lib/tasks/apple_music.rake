namespace :apple_music do
  task send_opt_outs: :environment do
    logger = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)

    limit = ENV["LIMIT"] || 1000
    logger.info "Limit set to #{limit}"

    last_id_done = $redis.get("apple_music:last_album_id") || "1500000"
    sql = "select distinct a.* from albums a inner join salepoints sp on sp.salepointable_id = a.id inner join stores s on s.id = sp.store_id where s.short_name like 'iTunes%' and a.takedown_at is null and sp.takedown_at is null and a.payment_applied = 1 and a.is_deleted = 0 and a.legal_review_state in ('APPROVED', 'DO NOT REVIEW') and a.style_review_state in ('APPROVED', 'DO NOT REVIEW') and a.id < #{last_id_done} order by a.id DESC limit #{limit}"

    logger.info "Retrieving albums..."
    albums = Album.find_by_sql(sql)
    albums.each do |album|
      logger.info "Processing album #{album.id}: #{album.name}"
      next if album.person.nil?
      next if album.allow_itunes_streaming

      distros = album.salepoints.joins(:store).select { |sp| sp.store.short_name[/iTunes/] }.map { |sp| sp.distributions.joins(:petri_bundle).where("petri_bundles.state <> 'dismissed'") }.flatten.uniq

      logger.info "Found #{distros.length} distributions to enqueue"
      distros.each do |d|
        logger.info "Processing distribution #{d.id}: State = #{d.state}, Updated At = #{d.updated_at.strftime("%Y-%m-%d %H:%M")}"
        next if d.state == "delivered" && d.updated_at > Date.strptime("2015-06-23 15:00", "%Y-%m-%d %H:%M")

        if album.finalized_at.nil?
          d.update_attribute(:state, "new") if d.state == "error"
          next
        end

        delivery_type = album.finalized_at > Date.strptime("2015-06-22 00:00", "%Y-%m-%d %H:%M") ? "full_delivery" : "metadata_only"

        logger.info("Enqueuing distribution #{d.id}")
        Delivery::DistributionWorker.perform_async(d.class.name, d.id, delivery_type, "petri-apple-music-queue")
      end

      logger.info "Setting last album processed to #{album.id}"
      $redis.set("apple_music:last_album_id", album.id)
    end
  end

  task redistribute_opted_out_albums_with_error: :environment do
    logger  = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)

    raise "You need to provide an ERROR_MESSAGE env variable." unless error_message = ENV["ERROR_MESSAGE"]
    enqueue_type  = ENV["ENQUEUE_TYPE"]  || 'matadata_only'
    petri_queue   = ENV["PETRI_QUEUE"]   || 'petri-apple-music-queue'

    distributions = Distribution.select("distinct distributions.*")
                .joins("inner join petri_bundles on distributions.petri_bundle_id = petri_bundles.id
                          and petri_bundles.state = 'error'
                        inner join albums on albums.id = petri_bundles.album_id
                          and albums.apple_music = 0
                          and albums.takedown_at IS NULL
                          and albums.finalized_at IS NOT NULL
                          and albums.is_deleted = 0
                          and albums.legal_review_state IN('DO NOT REVIEW', 'APPROVED')")
                .where(state: "error", converter_class: "MusicStores::Itunes::Converter")

    distributions.each do |distribution|
      unless distribution.transitions.where(new_state: "delivered").empty?
        latest_transition = distribution.transitions.last
        unless latest_transition.new_state = 'error' && latest_transition.message =~ /#{error_message}/
          Delivery::DistributionWorker.perform_async(distribution.class.name, distribution.id, enqueue_type, petri_queue)
          logger.info "Distribution with id: #{distribution.id} enqueued."
        else
          logger.info "Distribution with id: #{distribution.id} did not match error '#{error_message}'. not enqueued."
        end
      else
        logger.info "Distribution with id: #{distribution.id} never successfully delivered."
      end
    end
  end

  task send_apple_music_info: :environment do
    client = ZendeskAPI::Client.new do |config|
      config.url = "https://musicconnect.zendesk.com/api/v2"
      config.username = ENV["ZENDESK_USER"]
      config.token = ENV["ZENDESK_TOKEN"]
    end

    distribution_start_date = ENV["START"] || Date.today - 2.days

    distros = Distribution.where(["state = 'delivered' and updated_at > ? and converter_class = 'MusicStores::Itunes::Converter'", distribution_start_date]).preload(petri_bundle: {album: [:person, :optional_upcs, :tunecore_upcs]})

    distros.each do |d|
      a = d.petri_bundle.album
      p = a.person
      next unless p.apple_role.present? && p.apple_id.present?


      apple_artist_id = $apple_transporter_client.transport(:lookup_item, "release", a.upc.number)[:apple_id]

      next if AppleArtistId.where(["person_id = ? and apple_artist_id = ?", p.id, apple_artist_id]).present?

      p.apple_artist_ids.create(apple_artist_id: apple_artist_id)

      if Rails.env.production?
        ZendeskAPI::Ticket.create(client, {"requester" => {"name" => "#{p.name}", "email" => "no-reply@apple.com"},
         "ticket_form_id" => "70707",
         "tags" => ["API","tunecore"],
         "type" => "task",
         "subject" => "Tunecore request for #{p.name} #{p.email}",
         "comment" => { "body" => "tunecore request" },
         "custom_fields" =>
          [
          {"id" => "26106607", "value" => "#{a.artist_name}"},
          {"id" => "26189028", "value" => "#{p.apple_id}"},
          {"id" => "26086358", "value" => "#{p.apple_role}"},
          {"id" => "26244277", "value" => "nocontent"},
          {"id" => "26574807", "value" => "#{apple_artist_id}"},
          {"id" => "26574817", "value" => "#{a.apple_identifier}"}]
          })
      end
    end
  end

  task daily_opt_ins: :environment do
    logger      = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)
    start_date  = ENV["START_DATE"] ? Date.parse(ENV["START_DATE"], "%Y-%m-%d") : Date.today
    end_date    = ENV["END_DATE"]   ? Date.parse(ENV["END_DATE"], "%Y-%m-%d")   : Date.today

    distros     = Distribution.find_by_sql(["select distinct d.* from distributions d inner join distributions_salepoints ds on d.id = ds.distribution_id inner join salepoints sp on sp.id = ds.salepoint_id inner join stores s on sp.store_id = s.id and s.short_name like 'iTunes%' inner join albums a on a.id = sp.salepointable_id inner join person_service_opt_ins p on p.person_id = a.person_id inner join petri_bundles b on d.petri_bundle_id = b.id and b.state <> 'dismissed' where a.finalized_at is not null and a.takedown_at is null and sp.takedown_at is null and a.is_deleted = 0 and a.payment_applied = 1 and d.state = 'delivered' and date(p.opt_in_at) between ? and ?", start_date, end_date])

    logger.info "Apple Music Opt-in job found #{distros.length} distributions to enqueue for #{start_date.strftime("%Y-%m-%d")} to #{end_date.strftime("%Y-%m-%d")}"
    distros.each do |d|
      logger.info "Enqueuing distribution #{d.id} @ #{Time.now.strftime("%Y-%m-%d %H:%M")}"
      Delivery::DistributionWorker.perform_async(d.class.name, d.id, "metadata_only", "petri-apple-music-queue")
    end
    logger.info "Apple Music Opt-in job completed for #{start_date.strftime("%Y-%m-%d")} to #{end_date.strftime("%Y-%m-%d")}"
  end

  task reset_blocked_distros: :environment do
    logger  = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)
    limit   = ENV["LIMIT"] || 1000
    logger.info "Limit set to #{limit}"

    distros = Distribution.limit(limit).where("updated_at > '2015-06-19' and updated_at < '2015-06-30' and converter_class = 'MusicStores::Itunes::Converter' and state <> 'blocked'")

    logger.info "Found #{distros.length} distributions "
    distros.each do |d|

      logger.info "Checking for previous block on distribution #{d.id}"
      transitions = d.transitions.order("created_at DESC")
      blocked = transitions.find{ |t| t.new_state == "blocked" }
      delivered = transitions.find{ |t| t.new_state == "delivered" }

      next if blocked.nil?

      logger.info "Found previoud blocked state for distribution #{d.id}"
      if !delivered.nil? && delivered.created_at > blocked.created_at
        logger.info "Sending takedown for distribution #{d.id}"
        d.salepoints.uniq.each { |sp| sp.update_attribute(:takedown_at, Time.now) }
        Delivery::DistributionWorker.perform_async(d.class.name, d.id, "metadata_only", "petri-apple-music-queue")
      end

      logger.info "Updated distribution #{d.id} to blocked"
      d.update(state: "blocked", updated_at: Time.now)
    end
  end

  task fix_territory_restrictions: :environment do
    logger  = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)
    sql = "select distinct d.* from distributions d inner join distributions_salepoints ds on ds.distribution_id = d.id inner join salepoints sp on ds.salepoint_id = sp.id inner join albums a on a.id = sp.salepointable_id inner join album_countries ac on a.id = ac.album_id inner join petri_bundles p on p.id = d.petri_bundle_id where d.state != 'blocked' and a.finalized_at is not null and p.state <> 'dismissed' and a.takedown_at is null and sp.takedown_at is null and sp.store_id = 36 and d.state <> 'new'"
    distros = Distribution.find_by_sql(sql)
    logger.info "Found #{distros.length} distributions"
    distros.each do |d|
      logger.info "Redelivering distribution #{d.id}"
      Delivery::DistributionWorker.perform_async(d.class.name, d.id, "metadata_only", "petri-apple-music-queue")
    end
  end

  task update_opted_in_account_albums: :environment do
    logger  = ActiveSupport::Logger.new("log/apple_music.log", Rails.logger.level)

    start_date = ENV["START"] || Date.today - 1.year
    exclude_albums = ENV["APPLE_MUSIC_EXCLUDES"].nil? ? [-1] : CSV.read(ENV["APPLE_MUSIC_EXCLUDES"]).flatten
    stores = Store.where("short_name like 'iTunes%'").map{|s| s.id}
    albums = Album.select("distinct albums.*").joins("inner join salepoints on albums.id = salepoints.salepointable_id and salepoints.salepointable_type = 'Album' inner join people on albums.person_id = people.id inner join person_service_opt_ins psoi on people.id = psoi.person_id").where(["salepoints.store_id in (?) and albums.id not in (?) and albums.apple_music = 0  and albums.takedown_at is null and salepoints.takedown_at is null and opt_in_at > ?", stores, exclude_albums, start_date])
    logger.info "Updating #{albums.length} Albums"
    albums.each do |a|
      a.update(apple_music: true)
      logger.info "Updating album: #{a.id}"
    end
  end
end
