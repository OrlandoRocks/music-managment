=begin
  USAGE:
  rake content_fingerprint:create_fingerprints
=end
namespace :content_fingerprint do
  task :create_fingerprints => :environment do
    Song.missing_fingerprint.each(&:create_and_store_content_fingerprint!)
  end
end
