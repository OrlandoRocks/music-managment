namespace :ytsr do
  task retry_failures: :environment do
    failures = TrackMonetization.where(
                 store_id: Store::YTSR_STORE_ID,
                 state: 'error',
                 updated_at: 1.days.ago..Time.now,
                 retry_count: 0..3)

    Rails.logger.info "Starting Deliveries for #{failures.count} YTSR TrackMonetizations"

    failures.each do |track|
      retry_count = track.increment!(:retry_count)
      Delivery::DistributionWorker.set(queue: "delivery-critical").perform_async(track.class.name, track.id, track.delivery_type)
    end
  end
end
