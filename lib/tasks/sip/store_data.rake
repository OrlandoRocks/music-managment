namespace :transient do
  namespace :sip do
    desc "It adds a new store in which SIP can attribute payments from"
    task :add_store, [:id, :name, :store_id, :store_id_old, :display_on_status_page, :display_group_id, :display_group_id_old] => :environment do |_t, params|
      store   = SipStore.find_by(id: params.id)
      message = "Found" unless store.nil?
      store ||= SipStore.create!(
        id: params.id,
        name: params.name,
        store_id: params.store_id,
        store_id_old: params.store_id_old,
        display_on_status_page: params.display_on_status_page,
        display_group_id: params.display_group_id,
        display_group_id_old: params.display_group_id_old
      )
      message ||= "Created"
      puts "#{message} SipStore: #{store.attributes}"
    end

    desc "It adds new SIP Stores displayed"
    task :add_stores_display_group, [:id, :name, :sort_order, :group_on_status] => :environment do |_t, params|
      display_group = SipStoresDisplayGroup.find_by(id: params.id)
      message       = "Found" unless display_group.nil?
      display_group ||= SipStoresDisplayGroup.create!(
        id: params.id,
        name: params.name,
        sort_order: params.sort_order,
        group_on_status: params.group_on_status
      )
      message ||= "Created"
      puts "#{message} SipStoresDisplayGroup: #{display_group.attributes}"
    end
  end
end
