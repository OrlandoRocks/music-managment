namespace :fbreels_salepoint do
  desc "backfill instagram salepoint and distribution to fbreels"
  task backfill: :environment do
    Rails.logger.info "Starting backfill for instagram salepoints"
    salepoints = Salepoint.includes(:distributions).where(
      store_id: Store::IG_STORE_ID, salepointable_type: "Album"
    ).where.not(
      salepointable_id:
        Salepoint.where(
          store_id: Store::FB_REELS_STORE_ID, salepointable_type: "Album"
        ).pluck(:salepointable_id)
    )

    salepoints_processed = 0
    total_salepoints = salepoints.count

    store = Store.find(Store::FB_REELS_STORE_ID)

    salepoints.find_in_batches do |salepoint_batches|
      salepoints_processed += salepoint_batches.count

      salepoint_batches.each do |salepoint|
        # Create new salepoint for store id 100
        new_salepoint = salepoint.dup
        new_salepoint.store_id = Store::FB_REELS_STORE_ID
        new_salepoint.save

        # Do not create distributions for 100 if no distribution exists for 84
        next if salepoint.distributions.blank?

        distro = Distribution.create!(
          state: "new",
          converter_class: store.converter_class,
          delivery_type: "full_delivery",
          petri_bundle_id: salepoint.distributions.first.petri_bundle_id
        )

        distro.salepoints << new_salepoint
      end

      remaining_salepoints = total_salepoints - salepoints_processed
      Rails.logger.info "Salepoints Processed: #{salepoints_processed} Salepoints remaining: #{remaining_salepoints}"
    end

    Rails.logger.info "Rake task completed"
  end
end
