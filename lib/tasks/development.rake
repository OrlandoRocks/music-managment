if Rails.env.development? || Rails.env.test?
  require "factory_bot_rails"

  namespace :dev do
    desc "Sample data for local development environment"
    task setup: [:environment, "db:setup"] do
      FactoryBot.factories.clear
      FactoryBot.definition_file_paths = %W(spec/support/factories)
      FactoryBot.reload
      include FactoryBot::Syntax::Methods

      password = "Bradysux666!"
      admin    = create(:person, :admin, :with_balance, amount: 50_000, email: "developer@tunecore.com", password: password, password_confirmation: password)
      system_admin    = create(:person, :admin, email: "studio_automation@tunecore.com", password: password, password_confirmation: password)

      Role.all.each do |role|
        admin.roles << role
        system_admin.roles << role
      end

      sip_store = create(:streaming_sip_store)

      people = CountryWebsite.all.reduce([]) do |peeps, country_website|
        person = create(:person, :with_stored_paypal_account, password: password, password_confirmation: password, country: country_website.country, country_website_id: country_website.id)
        srm  = create(:sales_record_master, sip_store: sip_store, status: "new", period_year: "2018", period_sort: "2018-01-01", country: country_website.country, revenue_currency: country_website.currency, amount_currency: country_website.currency)
        5.times do |x|
          album = create(:album, :with_artwork, :with_salepoints, :with_uploaded_song, person: person)
          30.times do |x|
            create(:sales_record, person: person, sales_record_master: srm, related: album.songs.last, quantity: Random.rand(20), revenue_per_unit: Random.rand, revenue_total: Random.rand, amount: Random.rand)
            create(:streaming_sales_record, person: person, sales_record_master: srm, related: album.songs.last, quantity: Random.rand(20), revenue_per_unit: Random.rand, revenue_total: Random.rand, amount: Random.rand)
          end
        end
        peeps << person
      end

      StoredProcedureLoader.load
      ActiveRecord::Base.connection.execute("CALL create_person_intake();")
      ActiveRecord::Base.remove_connection
      ActiveRecord::Base.establish_connection(ActiveRecord::Base.configurations[Rails.env])
      ActiveRecord::Base.connection.execute("CALL populate_sales_record_summaries();")

      puts "|admin email    | #{admin.email} |"
      puts "|admin password | #{password} |"
      people.each do |person|
        puts "|#{person.country_domain} user email    | #{person.email} |"
        puts "|#{person.country_domain} user password | #{password} |"
      end
    end
  end
end
