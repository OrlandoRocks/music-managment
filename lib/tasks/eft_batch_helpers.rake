# ======================================================
# = Helper Rake Tasks for processing the EFT Payouts in Batch =
# ======================================================

require "#{Rails.root}/lib/eft"

namespace :eft do
  include Logging
  include Eft
  @eft = "EFT Batch"

  desc 'Upload EFT Batch to Braintree'
  task :upload_batch =>  :environment do
    ENV['BATCH'] || raise('must provide batch number')

    start_time = Time.now
    log(@eft, 'Start eft:upload_batch Rake Task', :time=>start_time)

    upload_eft_csv_to_braintree(ENV['BATCH'].to_i)

    log(@eft, 'End eft:upload_batch Rake Task', :time=>pretty_time_since(start_time))
  end

  desc 'Process EFT Batch from Braintree'
  task :process_response_for_batch =>  :environment do
    ENV['BATCH'] || raise('must provide batch number')

    start_time = Time.now
    log(@eft, 'Start eft:process_response_for_batch Rake Task', :time=>start_time)

    process_eft_download_from_braintree(ENV['BATCH'].to_i)

    log(@eft, 'End eft:process_response_for_batch Rake Task', :time=>pretty_time_since(start_time))
  end

  desc 'Test email Send'
  task :test_email =>  :environment do
    EftNotifier.admin_error_notice('Test Error message').deliver
  end

  desc 'Test query braintree response by passing in xml file'
  task :test_query_braintree =>  :environment do
    ENV['FILE'] || raise("must provide full file path to xml file, such as FILE='/Users/dandrabik/batch/response/response.xml'")
    test_query_braintree_from_xml_file(ENV['FILE'])
  end

  desc "Recreate a csv file for an eft batch that has already been created"
  task :recreate_eft_batch_csv => :environment do
    raise "Must supply 'EFT_BATCH_ID'"  if ENV["EFT_BATCH_ID"].blank?

    batch_id = ENV["EFT_BATCH_ID"]
    batch = EftBatch.find(batch_id.to_s)

    if batch.status == "complete"
     raise "Batch is already complete!"
    end

    create_csv_from_batch(batch)
  end

end
