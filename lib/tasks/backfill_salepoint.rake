namespace :backfill_salepoint do
  desc "Backfill salepoint without distributions"
  task :run, [:source_store_id, :destination_source_id] => :environment do |_, args|
    source_store_id = args.source_store_id
    destination_source_id = args.destination_source_id

    BackfillSalepoint.new(source_store_id, destination_source_id, false).run
  end

  desc "Backfill salepoint without distributions"
  task :with_distribution, [:source_store_id, :destination_source_id] => :environment do |_, args|
    source_store_id = args.source_store_id
    destination_source_id = args.destination_source_id

    BackfillSalepoint.new(source_store_id, destination_source_id, true).run
  end
end
