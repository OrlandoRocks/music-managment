namespace :alerts do
  task dj_workers: :environment do
    host = Socket.gethostname
    response = `ps -ef | grep delayed | grep -v grep`
    dj_threads = response.scan("delayed_job").length
    HTTParty.post("https://hooks.slack.com/services/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", body: { text: "DELAYED JOB ALERTS:\nThere are only #{dj_threads} delayed job workers running on #{host}." }.to_json) if dj_threads < 5
  end
end
