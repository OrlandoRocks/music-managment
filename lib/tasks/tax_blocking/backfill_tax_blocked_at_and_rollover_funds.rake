namespace :tax_blocking do
  # bundle exec rake tax_blocking:backfill_tax_blocked_at_and_rollover_funds[2021]

  desc "Schedules a sidekiq job for updating tax_blocked_at, total_taxable_distribution_earnings, total_taxable_publishing_earnings, & rollover_earnings in PersonEarningsTaxMetadatum"
  task :backfill_tax_blocked_at_and_rollover_funds, [:year] => :environment do |_t, args|


    people_ids = TaxableEarningsRollover
                  .where(
                    tax_blocked_at: nil,
                    tax_blocked: true,
                    tax_year: args.year )
                  .pluck(:person_id)

    people_ids.each do |person_id|
      TaxBlocking::TaxableEarningsRolloverWorker.perform_async(person_id, args.year)
    end

    Rails.logger.info "The Worker for updating tax_blocked_at for the year: #{args.year}, in PersonEarningsTaxMetadatum + funds have been scheduled to be rolled over."
  end
end