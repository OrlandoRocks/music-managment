namespace :tax_blocking do
  # rake "tax_blocking:backfill_taxform_info[2018..2021]"
  desc "Schedules a sidekiq batch job for updating tax form info in PersonEarningsTaxMetadatum"
  task :backfill_taxform_info, [:years] => :environment do |_t, args|
    TaxBlocking::BatchTaxformsAggregationWorker.perform_async(args.years)
    Rails.logger.info "The Batch for TaxInfoUpdate has been scheduled."
  end
end
