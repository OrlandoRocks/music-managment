namespace :roles do
    desc "Create VAT Admin Role"
    task :create_vat_admin => :environment do
        #
        #  Creates the VAT Admin role
        #

        Role.find_or_create_by(name: 'VAT Admin').tap do |role|
            puts "Creating the VAT Admin role"
            role.long_name = "VAT Admin"
            role.description = "Gives admin the ability to manage(create/read/update/list) VAT rules"
            role.is_administrative = true
            role.save!
        end
    end
end
