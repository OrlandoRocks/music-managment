#frozen_string_literal: true
namespace :mark_accounts_suspicious do
  desc "extract accounts from ENV['SUSPICIOUS_ACCOUNTS_CSV'] and mark accounts as suspicious"
  task run: :environment do
    next unless validate_args

    # csv format: email
    suspicious_accounts_data_file = ENV['SUSPICIOUS_ACCOUNTS_CSV']
    suspicious_accounts_data = CSV.open(suspicious_accounts_data_file, headers: true)
    suspicious_accounts_data.each do |account_info|
      find_and_mark_account_suspicious(account_info['email'])
    end
  end

  def validate_args
    required_flags = ['SUSPICIOUS_ACCOUNTS_CSV', 'SUSPICION_NOTE', 'SUSPICION_REASON']

    required_flags.each do |flag|
      if ENV[flag].blank?
        suspicious_accounts_log(:error, "REQUIRED PARAMETER MISSING - #{flag}")
        return false
      end
    end

    if !Person::REASONS_FOR_DEACTIVATION.include?(ENV['SUSPICION_REASON'])
      suspicious_accounts_log(
        :error,
        "INVALID SUSPICION REASON. VALID REASONS - #{Person::REASONS_FOR_DEACTIVATION}"
      )
      return false
    end

    return true
  end

  def find_and_mark_account_suspicious(person_email)
    person = Person.find_by(email: person_email)
    if person.present?
      mark_account_suspicious(person)
    else
      suspicious_accounts_log(:error, 'UNABLE TO FIND ACCOUNT.', person_email)
    end
  end

  def mark_account_suspicious(account_to_mark)
    # Account of Paul Zachopoulos - Controller (Finances Dept.)
    note_created_by_acct = Person.find_by(id: ENV.fetch('ADMIN_ACCOUNT_ID', '2190048'))
    note_to_attach = Note.create(
      related: account_to_mark,
      subject: "Marked as Suspicious",
      note: ENV['SUSPICION_NOTE'],
      note_created_by: note_created_by_acct
    )

    if note_to_attach.persisted?
      account_to_mark.mark_as_suspicious!(note_to_attach, ENV["SUSPICION_REASON"])
      if account_to_mark.persisted?
        suspicious_accounts_log(:info, "Marked Suspicious.", account_to_mark.email)
      else
        suspicious_accounts_log(
          :error,
          "COULD NOT MARK ACCOUNT SUSPICIOUS.\n\tERROR: %s" % account_to_mark.errors.full_messages.join(', '),
          account_to_mark.email
        )
      end
    else
      suspicious_accounts_log(
        :error,
        "UNABLE TO CREATE NOTE.\n\tERROR: %s" % note_to_attach.errors.full_messages.join(', '),
        account_to_mark.email
      )
    end
  end

  def suspicious_accounts_log(log_type, log_message, person_email=nil)
    person_log_tag = "[%s]: " % person_email if person_email.present?
    Rails.logger.send(
      log_type,
      "#{ENV.fetch('TASK_LOG_TAG', "[MARK ACCOUNTS SUSPICIOUS] ")}#{person_log_tag}#{log_message}"
    )
  end
end
