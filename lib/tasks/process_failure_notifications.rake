require "#{Rails.root}/lib/logging"

class FailureNotificationEmail < ActionMailer::Base

  def failure_email(task, message="")
    mail(:from => "TuneCore Process Failure <process_failure@tunecore.com>", :to => "techteam@tunecore.com", :subject=>"Failure in TuneCore Process: #{task}" ) do |format|
      format.text { render plain: message }
    end
  end

end

namespace :process_failure_notifications do

  desc "Reports if there are any unsummarized sales_record masters and sends an email"
  task :check_sales_record_summary => :environment do |t|

    log("Check Sales Record Summary", "checking for unsummarized sales record masters")
    if SalesRecordMaster.where("summarized = 0").exists?
      log("Check Sales Record Summary", "ERROR - unsumarized sales records exist")

      message = %Q{
There are unsummarized sales_record_masters!

Note that in this state its possible that:
  1) customers did not have sales posted
  2) customers had sales posted and their balances adjusted but they cannot see details

Please investigate cause asap.

We need to check:
  1) If a posting or 'populate_sales_record_summaries' is still running
      1.1) If yes then this should resolve itself
  2) If not then: need to check error logs of the posting
      2.1) If errors posting then need to investigate why/where posting failed and potentially reset posting or pick up where posting failed
  3) Do the unsummarized sales_record_masters have any sales_records ( this is not a normal case )
      3.1) This scenario points to an issue posting sales_records and requires the same investigation as 2.1

The stored_procedure that can resummarize sales_record_masters is "populate_sales_record_summaries()" but most scenarios of this failure point to an issue in sip's rake sip:post
      }
      FailureNotificationEmail.failure_email(t, message).deliver
    end

  end

  desc "Reports if there are any you_tube_royalty_records without a person_intake and sends an email"
  task :check_you_tube_records => :environment do |t|

    log("Check You Tube Records", "checking for you tube records that have not been posted to balance")
    if YouTubeRoyaltyRecord.where("net_revenue > 0 and you_tube_royalty_intake_id IS NULL").exists?
      log("Check You Tube Records", "ERROR - unposted you tube royalty records exist")

      message = %Q{
There are you tube royalty records that do not have a person intake!

This state means that either a posting is very late or the update_balances_from_you_tube_records stored procedure has failed
      }
      FailureNotificationEmail.failure_email(t, message).deliver
    end

  end

end
