namespace :two_factor_auth do
  task import_phone_numbers_from_authy: :environment do
    tfa_ids = TwoFactorAuth.where.not(authy_id: nil).where(e164: nil).pluck(:id)
    tfa_ids.each do |tfa_id|
      TwoFactorAuth::PhoneNumberImportWorker.perform_async(tfa_id)
    end
  end
end
