namespace :batch do

  desc 'Method to manually upload the CSV to Braintree in the case of an upload error'
  task :daily_log =>  :environment do
    date = Date.parse(ENV["DATE"]) if !ENV["DATE"].nil?
    BatchNotifier.daily_log(:email=>ENV["EMAIL"], :date=>date).deliver
  end

  #### BELOW THIS LINE ARE RAKE TASKS FOR INDIVIDUAL PARTS OF THE PROCESS
  #### THESE SHOULD NEVER BE USED UNLESS SOMETHING HAS GONE WRONG IN THE PROCESSING

  desc 'Method to manually upload the CSV to Braintree in the case of an upload error'
  task :upload_csv =>  :environment do
    batch_id = ENV["BATCH"]  || raise("must provide payment batch id with BATCH=batch_id (e.g. BATCH=12 for payment batch with an id of 12)")
    batch = PaymentBatch.find(batch_id)
    batch.send(:upload_csv_to_braintree)
  end

  desc 'Method to upload the CSV manually to Braintree just in case something happens'
  task :process_csv =>  :environment do
    file = ENV["BATCH"]  || raise("must provide payment batch id with BATCH=batch_id (e.g. BATCH=12 for payment batch with an id of 12)")
    process_download_from_braintree(ENV["BATCH"])
  end

  desc 'Reset batch for to reprocess'
  task :reset_batch_for_response =>  :environment do
    file = ENV["BATCH"]  || raise("must provide payment batch id with BATCH=batch_id (e.g. BATCH=12 for payment batch with an id of 12)")
    batch = PaymentBatch.find(ENV["BATCH"])
    batch.update(:response_received=>false)
  end

  desc 'Create response file with fake responses'
  task :create_response_on_sandbox do
    sent_name = ENV['NAME']

    if !sent_name.nil?
      response_name = 'response-' + sent_name
      upload_path = '/home/dandrabik/upload/' + sent_name
      reponse_path = '/home/dandrabik/response/' + response_name
      csv_rows = []
      count = 1
      require 'csv'
      CSV.foreach(upload_path) do |row|
        if row[0] !='customer_vault_id' # skip header row (there is no header row, but leaving this check in for now)
                                              #row number / field
          csv_rows << [
            count,                           # 0     Transaction Id
            [1,2,3].rand,                    # 1     Response (1=success/2=declined/3=error)
            "text response",                 # 2     Processor Response (text response)
            ["X","Y"].rand,                  # 3     AVS Response
            ["M","N"].rand,                  # 4     CVV Response
            123456,                          # 5     Auth Code
            row[2],                          # 6     Amount
            "",                              # 7     BLANK
            row[3],                          # 8     Order Id (internally, invoice_id)
            "",                              # 9     Order Description
            "",                              # 10    PO Number
            "",                              # 11    Shipping
            "",                              # 12    Tax
            "",                              # 13    Billing First Name
            "",                              # 14    Billing Last Name
            "",                              # 15    Billing Company
            "",                              # 16    Billing Email
            "",                              # 17    Billing Phone
            "",                              # 18    Billing Fax
            "",                              # 19    Billing Website Address
            "",                              # 20    Billing Address1
            "",                              # 21    Billing Address2
            "",                              # 22    Billing City
            "",                              # 23    Billing State
            "",                              # 24    Billing Zip Code
            "",                              # 25    Billing Country
            "",                              # 26    Shipping First Name
            "",                              # 27    Shipping Last Name
            "",                              # 28    Shipping Company
            "",                              # 29    Shipping Email
            "",                              # 30    Shipping Address1
            "",                              # 31    Shipping Address2
            "",                              # 32    Shipping City
            "",                              # 33    Shipping State
            "",                              # 34    Shipping Zip Code
            "",                              # 35    Shipping Country
            row[1],                          # 36    Type
            [100,100,200,300].rand,          # 37    Response Code
            "",                              # 38    Processor ID
            "",                              # 39    Payment Type
            row[6],                          # 40    Merchant Defined Field 1 (payment_batch_id)
            row[5],                          # 41    Merchant Defined Field 2 (batch_transaction_id)
            row[4],                          # 42    Merchant Defined Field 3 (person_id)
            row[7],                          # 43    Merchant Defined Field 4 (filename)
            "",                              # 44    Merchant Defined Field 5
            "",                              # 45    Merchant Defined Field 6
            "",                              # 46    Merchant Defined Field 7
            "",                              # 47    Merchant Defined Field 8
            "",                              # 48    Merchant Defined Field 9
            "",                              # 49    Merchant Defined Field 10
            "",                              # 50    Merchant Defined Field 11
            "",                              # 51    Merchant Defined Field 12
            "",                              # 52    Merchant Defined Field 13
            "",                              # 53    Merchant Defined Field 14
            "",                              # 54    Merchant Defined Field 15
            "",                              # 55    Merchant Defined Field 16
            "",                              # 56    Merchant Defined Field 17
            "",                              # 57    Merchant Defined Field 18
            "",                              # 58    Merchant Defined Field 19
            "",                              # 59    Merchant Defined Field 20
            ""                               # 60    username
            ]
        end
        count += 1
      end
      CSV.open(reponse_path, "w", :force_quotes=>true) do |csv|
        # csv << ['Transaction Id', 'Response (1=success/2=declined/3=error)', 'Processor Response', 'AVS Response',
        #   'CVV Response', 'Auth Code','Amount','BLANK','customer_vault_id', 'type', 'amount', 'invoice_id','person_id','batch_transaction_id',
        #   'payment_batch_id', 'payment_batch_file_name']
        csv_rows.each do |row|
          csv << row
        end
      end
      puts "Response file created in 'response' folder named #{response_name}"
    else
      puts 'There is no file with that name'
    end
  end
end

namespace :preferences do

  desc 'Get Count for renewal amail send'
  task :test_reminder_emails =>  :environment do
    puts 'Start preferences:test_reminder_emails'
    start_date = ENV['DATE'] || nil
    end_date = ENV['END_DATE'] || nil
    if ENV['LIMIT'].nil?
      people_to_notify = Person.preference_reminders(start_date, end_date)
    else
      people_to_notify = Person.preference_reminders(start_date, end_date).limit(ENV['LIMIT']).offset(ENV['OFFSET'] || 0)
    end
    puts "Send Reminder emails numbers for contact - count: #{people_to_notify.group_by(&:id).size}"
    puts 'End preferences:test_reminder_emails'
  end
end
