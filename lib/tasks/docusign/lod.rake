# This should be removed after Dec-2020. If seen after, please remove it if its in the crontab
# and this file here. The reason is in this ticket - https://tunecore.atlassian.net/browse/GS-7105

namespace :lod do
  task expire_lods: :environment do
    # Documents sent 120 days ago and haven't sigend are "Voided"
    # 120 days is the envelope expiration @Docusign.
    LegalDocument.where(status: nil)
      .where(signed_at: nil)
      .where('created_at < ?', 120.days.ago)
      .update_all(status: :voided)

    Rails.logger.info("Updated 'Voided' Envelopes")
  end

  task mark_envelopes_completed: :environment do
    envelope_ids = ENV["ENVELOPE_IDS"].to_s
    envelope_ids = envelope_ids.split(',').select(&:present?).map(&:strip)

    if envelope_ids.blank?
      puts "Please provide comma separated ENVELOPE_IDS to proceed"
      next
    end

    envelope_ids.each do |envelope_id|
      lod = LegalDocument.find_by(provider_identifier: envelope_id)

      if lod.blank?
        puts "Error: Invalid Envelope ID #{envelope_id}"
      else
        lod.update(signed_at: Time.current, status: :completed)
        puts "Success: Updated to completed #{envelope_id}"
      end
    end
  end
end
