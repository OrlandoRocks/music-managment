namespace :transient do
  # Starting on __ December 2020, we will be temporarily blocking Indian invoices as part of the OPGSP project.
  # Run this task on that date/time to create the flag and block the invoices!
  desc "GS-9715 create a feature flag block_indian_invoices"
  task create_feature_flag_block_indian_invoices: :environment do
    india_country_website = CountryWebsite.find_by(id: CountryWebsite::INDIA)
    FeatureFlipper.add_feature("block_indian_invoices", india_country_website)
  end

  task destroy_feature_flag_block_indian_invoices: :environment do
    $country_rollouts["IN"].delete(:block_indian_invoices)
  end
end
