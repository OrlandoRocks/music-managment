namespace :stores do
  task update_zvuk: :environment do
      zvooq_store = Store.find_by(short_name: 'Zvooq')
      return unless zvooq_store
      zvooq_store.update(description: 'Zvuk is an independent Russian streaming service available for iOS, Android and by web at Zvuk.com  The service helps millions of users to find the right music for any moment.  Thousands of playlists curated by music experts and local celebrities, and geo-based smart recommendations.')
  end
end