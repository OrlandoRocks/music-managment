namespace :stores do
  task setup_luna: :environment do
    luna_store = Store.find_or_create_by(short_name: "Luna")

    luna_store.update(
      name: 'Qishui Yinyue',
      abbrev: 'luna',
      short_name: 'Luna',
      delivery_service: 'tc_distributor',
      discovery_platform: 0,
      in_use_flag: 0,
      is_active: 0
    )
  end

  task setup_luna_salepointable_stores: :environment do
    luna_store = Store.find_by(short_name: "Luna")

    SalepointableStore.find_or_create_by!(
      store_id: luna_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: luna_store.id,
      salepointable_type: "Single"
    )
  end

  task setup_luna_config: :environment do
    luna_store = Store.find_by(short_name: "Luna")

    store_delivery_config = StoreDeliveryConfig.for('Luna') || StoreDeliveryConfig.new(store_id: luna_store.id)

    store_delivery_config.assign_attributes(
      store_id: luna_store.id,
      party_id: 'PADPIDA20211214123',
      party_full_name: 'Luna',
      image_size: 3000,
      file_type: 'flac',
      hostname: 'music-ingestion.bytedance.com',
      username: 'tunecoreluna',
      password: 'lNFrnnUW+SYMkIaO',
      port: 8900,
      ddex_version: '382',
      remote_dir: 'upload',
      display_track_language: 1,
      use_resource_dir: 1,
      batch_size: 100,
      bit_depth: 16,
      transfer_type: 'sftp',
      delivery_queue: 'delivery-default',
      paused: 1,
      use_manifest: 0
    )

    store_delivery_config.save
  end

  task setup_luna_plans_stores: :environment do
    Rails.logger.info("⏲ Beginning rake task... ⏲")

    plan_ids = [
      Plan::RISING_ARTIST,
      Plan::BREAKOUT_ARTIST,
      Plan::PROFESSIONAL,
      Plan::LEGEND,
    ]

    store = Store.find_or_create_by(short_name: "Luna")

    plan_ids.each do |plan_id|
      plans_store = PlansStore.find_or_initialize_by(plan_id: plan_id, store_id: store.id)

      plans_store.save!
    end

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
