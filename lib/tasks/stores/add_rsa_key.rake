namespace :stores do
  task add_rsa_key: :environment do
    store_short_name = ENV['STORE']
    key_name = ENV['KEY']
    store_config = StoreDeliveryConfig.for(store_short_name)
    return unless store_config && key_name
    store_config.update(keyfile: key_name)
  end
end
