namespace :stores do
  task setup_tc_distributor: :environment do
    tc_dist_store = Store.find_or_create_by_short_name("TC-Distro")
    tc_dist_store.update(
      name: 'TC-Distributor',
      abbrev: 'TCD',
      short_name: 'TC-Distro',
      delivery_service: 'tunecore',
      is_active: 0,
      in_use_flag: 0,
      position: 691
    )

    SalepointableStore.find_or_create_by!(
      store_id: tc_dist_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: tc_dist_store.id,
      salepointable_type: "Single"
    )

    SalepointableStore.find_or_create_by!(
      store_id: tc_dist_store.id,
      salepointable_type: "Ringtone"
    )

    store_delivery_config = StoreDeliveryConfig.for('TC-Distro')
    store_delivery_config ||= StoreDeliveryConfig.create!(
      store_id: tc_dist_store.id,
      party_id: 'xxxxxxxxxxxxxx',
      party_full_name: 'tc-distributor',
      image_size: 1600,
      file_type: 'flac',
      hostname: 'sftp.tunecore.com',
      username: 'tc-distributor',
      keyfile: 'tc-distributor-ftp',
      ddex_version: '382',
      remote_dir: 'test',
      transfer_type: 'sftp',
      delivery_queue: 'delivery-default',
      include_behalf: 1,
      display_track_language: 1,
      display_track_id: 1,
      skip_media: 1,
      skip_batching: 0,
      use_resource_dir: 0
    )
  end
end
