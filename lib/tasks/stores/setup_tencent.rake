namespace :stores do
  task setup_tencent: :environment do
    if Store.find_by(short_name: "tencent")
      puts "tencent store already exists"
      next
    end

    tencent_store = Store.create!(
      name: "Tencent",
      abbrev: "tnct",
      short_name: "tencent",
      is_active: false,
      delivery_service: "believe"
    )

    StoreDeliveryConfig.create!(
      store_id: tencent_store.id,
      party_id: "PADPIDA20140404063",
      image_size: 1600,
      file_type: "flac",
      bit_depth: 16,
      ddex_version: "382",
      paused: true
    )

    SalepointableStore.create!(
      store_id: tencent_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.create!(
        store_id: tencent_store.id,
        salepointable_type: "Single"
    )
  end

  task update_tencent: :environment do
    tencent_store = Store.find_by(short_name: "tencent")
    exit unless tencent_store
    tencent_store.update(description: "Tencent delivers your music to three popular digital streaming platforms throughout China, including KuGuo Music, the world's largest subscription service boasting hundreds of millions of monthly active users.<p>Your music will be delivered to the following Chinese music platforms:</p><ul style='padding-left: 2em;'><li>KuGou Music</li><li>QQ Music</li><li>Kuwo Music</li></ul><strong>Strength:</strong> Get discovered, streamed and downloaded in the growing Chinese music market across three popular platforms.", short_name: 'Tencent', delivery_service: "tunecore", abbrev: 'tnct')
    tencent_store.update(is_active: ENV['TENCENT_STORE_ACTIVE']) if ENV['TENCENT_STORE_ACTIVE'].present?

    store_delivery_config = StoreDeliveryConfig.for('tencent')
    store_delivery_config.update(
      party_full_name: "Tencent Holdings Limited",
      delivery_queue: "delivery-default",
      transfer_type: "ftp",
      remote_dir: nil,
      batch_size: 10
    )

    store_delivery_config.update(
      hostname: ENV['HOSTNAME'],
      username: ENV['USERNAME'],
      port:     "8080",
      password: ENV['PASSWORD'],
    ) if ENV['HOSTNAME'].present? && ENV['USERNAME'].present? && ENV['PASSWORD'].present?

    store_delivery_config.update(paused: !ENV['TENCENT_ACTIVE']) if ENV['TENCENT_ACTIVE']

    knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "tencent-description")
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end
end
