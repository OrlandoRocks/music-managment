namespace :stores do
  task setup_qobuz: :environment do
    qobuz_store = Store.find_or_create_by(short_name:"Qobuz")
    qobuz_store.update(
      name: 'Qobuz',
      abbrev: 'Qob',
      short_name: 'Qobuz',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: qobuz_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: qobuz_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Qobuz') || StoreDeliveryConfig.create!(
      store_id: qobuz_store.id,
      party_id: 'PADPIDA2011021510N',
      party_full_name: 'Qobuz.com',
      image_size: 600,
      file_type: 'flac',
      hostname: 'sftp://sftp-ext-tunecore.qobuz.com',
      username: 'ext-tunecore',
      password: 's4dnDDbh9U8NmGRJ6yUx2qY7',
      port: '22',
      ddex_version: '382',
      batch_size: 100,
      transfer_type: 'sftp',
      bit_depth: '24',
      delivery_queue: 'delivery-default',
      paused: 1,
      use_manifest: 1
    )
  end

  task update_qobuz_knowledge_links: :environment do
    kbl = KnowledgebaseLink.find_or_create_by(article_slug: 'qboz-description')

    kbl.update(article_id: ENV.fetch('ARTICLE_ID'), route_segment: '/articles/')
  end

  task update_qoboz_description: :environment do
    qobuz_store = Store.find_by(short_name:"Qobuz")

    description_string = "Qobuz is the leading provider of high-resolution lossless audio to music fans. Users of Qobuz can stream in the best available quality and download tracks in different audio file formats. Subscribers also enjoy a robust selection of editorial content covering new and old albums, genre commentary, and artist spotlights. <br><strong>Strength:</strong> Quality - provide audiophiles with hi-res releases"

    qobuz_store.update(description: description_string)
  end
end
