namespace :stores do 
    task toggle_discovery_platform: :environment do
        store_id = ENV.fetch("STORE_ID")
        discovery = ENV.fetch("IS_DISCOVERY")
        is_discovery = ActiveModel::Type::Boolean.new.cast(discovery)
        store = Store.find(store_id)
        
        Rails.logger.info "Updating #{store.short_name} ID: #{store.id} discovery_platform to #{is_discovery}"
        store.update(discovery_platform: is_discovery)
    end 
end 