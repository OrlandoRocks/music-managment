namespace :stores do
  task setup_douyin: :environment do
    douyin_store = Store.find_or_create_by(short_name: "Douyin")
    douyin_store.update(
      name: 'Douyin',
      abbrev: 'Dou',
      short_name: 'Douyin',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: douyin_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: douyin_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Douyin') || StoreDeliveryConfig.create!(
      store_id: douyin_store.id,
      party_id: 'PADPIDA20200512052',
      party_full_name: 'Douyin',
      image_size: 1600,
      file_type: 'flac',
      hostname: 'music-ingestion.bytedance.com',
      username: 'tunecore',
      password: ENV.fetch('PASSWORD'),
      port: '8900',
      ddex_version: '382',
      remote_dir: 'upload',
      display_track_language: 1,
      use_resource_dir: 1,
      batch_size: 100,
      bit_depth: 16,
      transfer_type: 'sftp',
      delivery_queue: 'delivery-default',
      paused: 1,
      use_manifest: 0
    )
  end

  task update_douyin_knowledge_links: :environment do
    kbl = KnowledgebaseLink.find_or_create_by(article_slug: 'douyin-description')

    kbl.update(article_id: ENV.fetch('ARTICLE_ID'), route_segment: '/articles/')
  end

  task correct_douyin_abbrev: :environment do
    # store.abbrev should be lowercase

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    peloton_store = Store.find_by(short_name:"Douyin")

    peloton_store.update!(abbrev: 'dou')

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
