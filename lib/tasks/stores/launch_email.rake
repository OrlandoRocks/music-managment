namespace :store do
  desc "Send a store launch mail to people who has automated releases"
  task :launch => :environment do

    store = Store.find(ENV['STORE_ID'])
    exit(0) unless store

    persons_with_automated_releases = Album.find_by_sql("SELECT DISTINCT(albums.person_id) FROM albums INNER JOIN salepoint_subscriptions ON salepoint_subscriptions.album_id = albums.id WHERE salepoint_subscriptions.finalized_at IS NOT NULL AND albums.finalized_at IS NOT NULL AND albums.takedown_at IS NULL AND albums.is_deleted = 0 AND salepoint_subscriptions.is_active = TRUE")
    
    persons_with_automated_releases.each do |album|
      MailerWorker.perform_async("StoreLaunchMailer", :launch, store.id, album.person_id)
    end
  end
end
