namespace :stores do
  task setup_audiomack: :environment do
    if Store.find_by(short_name: "audiomack")
      puts "audiomack store already exists"
      next
    end

    audiomack_store = Store.find_or_create_by(short_name: "audiomack") do |store|
      store.name = "Audiomack"
      store.abbrev = "admk"
      store.short_name = "audiomack"
      store.is_active = false
      store.delivery_service = "tunecore"
    end

    StoreDeliveryConfig.find_or_create_by(store_id: audiomack_store.id) do |store_config|
      store_config.store_id = audiomack_store.id
      store_config.party_id = "PA-DPIDA-2017103008-S"
      store_config.image_size = 3000
      store_config.file_type = "flac"
      store_config.bit_depth = 16
      store_config.ddex_version = "382"
      store_config.paused = true
    end

    SalepointableStore.find_or_create_by(store_id: audiomack_store.id, salepointable_type: "Album")
    SalepointableStore.find_or_create_by(store_id: audiomack_store.id, salepointable_type: "Single")
  end

  task update_audiomack: :environment do
    audiomack_store = Store.find_by(short_name: "audiomack")
    exit unless audiomack_store
    audiomack_store.update_attributes(short_name: "audiomack", delivery_service: "tunecore", abbrev: "admk")
    audiomack_store.update_attributes(is_active: ENV['AUDIOMACK_STORE_ACTIVE']) if ENV['AUDIOMACK_STORE_ACTIVE'].present?

    store_delivery_config = StoreDeliveryConfig.for("audiomack")
    store_delivery_config.update_attributes(
      party_full_name: "Audiomack LLC",
      delivery_queue: "delivery-default",
      transfer_type: "s3",
      remote_dir: nil,
      skip_batching: 1,
      use_manifest: 0,
      use_resource_dir: 0
    )

    store_delivery_config.update_attributes(
      hostname: ENV['AUDIOMACK_HOSTNAME'],
      username: ENV['AUDIOMACK_USERNAME'],
      port:     "8080",
      password: ENV['AUDIOMACK_PASSWORD'],
    ) if ENV['AUDIOMACK_HOSTNAME'].present? && ENV['AUDIOMACK_USERNAME'].present? && ENV['AUDIOMACK_PASSWORD'].present?

    store_delivery_config.update_attributes(paused: !ENV['AUDIOMACK_ACTIVE']) if ENV['AUDIOMACK_ACTIVE']

    knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "audiomack-description")
    knowledgebase_link.update_attributes(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end
end
