namespace :stores do
  task setup_acr_cloud: :environment do
    acr_store = Store.find_or_create_by(short_name:"acrcloud")
    acr_store.update(
      name: 'ACR Cloud',
      abbrev: 'acr',
      short_name: 'acrcloud',
      position: 691,
      base_price_policy_id: 3,
      in_use_flag: true,
      delivery_service: 'believe',
      is_active: false
    )

    SalepointableStore.find_or_create_by!(
      store_id: acr_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: acr_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('acrcloud') || StoreDeliveryConfig.create!(
      store_id: acr_store.id,
      party_id: '',
      party_full_name: 'ACR Cloud',
      image_size: 1600,
      file_type: 'flac',
      skip_batching: true,
      skip_media: true,
      bit_depth: '24',
      paused: 1,
    )
  end
end
