namespace :stores do
  task cache_number_of_paused_distributions: :environment do
    Store.is_used.each do |store|
      Rails.cache.write(store.number_of_paused_distributions_key, store.send(store.distributable_class.tableize).where(state: "paused", created_at: Time.parse("2018-05-24")..Time.now).uniq.count)
    end
  end
end