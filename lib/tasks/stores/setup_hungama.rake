namespace :stores do
  task setup_hungama: :environment do
    hungama_store = Store.find_or_create_by(short_name: "Hungama")
    hungama_store.update(
      name: "Hungama",
      abbrev: "hung",
      delivery_service: "tunecore",
      is_active: 0
      )

    SalepointableStore.find_or_create_by!(
      store_id: hungama_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: hungama_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Hungama') || StoreDeliveryConfig.create!(
      store_id: hungama_store.id,
      party_id: 'PADPIDA20140114048',
      party_full_name: 'Hungama',
      image_size: 1400,
      file_type: 'flac',
      hostname: '18.204.104.115',
      username: 'tunecore',
      password: 'tunecore@789123',
      remote_dir: 'upload',
      port: '22',
      ddex_version: '382',
      batch_size: '50' ,
      transfer_type: 'sftp',
      bit_depth: 16,
      delivery_queue: 'delivery-default',
      paused: 1
    )
  end

  task update_hungama: :environment do
    hungama_store = Store.find_or_create_by(short_name: "Hungama")
    hungama_store.update(
      name: "Hungama Music",
      description: "Hungama is a popular music and video streaming service developed and launched by one of the leading digital entertainment sites in India. With over 18 million users, Hungama offers video streaming capabilities, a loyalty program that allows fans to redeem points within the app, and a mood discovery feature. <p> <strong> Strength: </strong> Reach - make your music available across India.")
    hungama_store.update(is_active: is_active?) if ENV['STORE_ACTIVE'].present?

    knowledgebase_link = KnowledgebaseLink.where(article_slug: 'hung-description').first_or_create
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end


  task add_wynk_to_hungama: :environment do
    hungama_store = Store.find_or_create_by(short_name: "Hungama")
    hungama_store.update(
      name: "Wynk/Hungama",
      description: "Wynk is one of India's most-downloaded music apps, with over 100 million installs and over 1.5 billion monthly streams. The service is integrated with one of the leading telecom brands in the region, Airtel, and offers users features like personalized radio, playlists, voice search, and more recently, \"Wynk Direct\", which gives them the opportunity to share music with their friends on the app regardless of connectivity. <p> <strong> Strength: </strong> Reach - get your music in front of listeners across India, Sri Lanka, and fifteen countries within Africa.",
      abbrev: "wkhg"
      )

    knowledgebase_link = KnowledgebaseLink.where(article_slug: 'hung-description')
    knowledgebase_link.update(article_slug: 'wkhg-description')
  end

  def is_active?
    if ENV['STORE_ACTIVE'] == "true"
      true
    elsif ENV['STORE_ACTIVE'] == "false"
      false
    end
  end
end
