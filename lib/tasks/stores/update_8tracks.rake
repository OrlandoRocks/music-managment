namespace :stores do
  task update_8tracks: :environment do
    active = ENV['ACTIVE'] == 'true' ? true : false
    eight_tracks_store = Store.find_by(short_name: '8tracks')
    return unless eight_tracks_store
    eight_tracks_store.update(is_active: active)

    eight_tracks_store = StoreDeliveryConfig.find_by(store_id: eight_tracks_store.id)
    eight_tracks_store.update(
      paused: !active
    ) if eight_tracks_store
  end
end
