namespace :stores do
  task update_triller: :environment do
    triller_store = Store.find_by(short_name: '7digital')
    description = "Triller is a hugely popular social app designed to
    let users create and share video clips that feature artists' songs.
    The app boasts over 250 million downloads and includes tons of options
    for artists to edit clips and utilize features. Users can promote
    TuneCore Artists' songs and views will count toward Apple Music streams.
    <p> <strong>Strength</strong>: Reach. Triller is widely used by music fans
    and high profile celebrities alike who upload videos and check the app to
    see what's trending."

    triller_store.update({
      name: "Triller/7Digital",
      description: description
    })
  end
end
