namespace :stores do
  task setup_tim: :environment do
    tim_store = Store.find_or_create_by(short_name: "Tim")
    tim_store.update(
      name: "TIM",
      abbrev: "tim",
      delivery_service: "tunecore",
      is_active: 0
      )

    SalepointableStore.find_or_create_by!(
      store_id: tim_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: tim_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Tim') || StoreDeliveryConfig.create!(
      store_id: tim_store.id,
      party_id: 'PADPIDA2011021002Q',
      party_full_name: 'Mondia Media Group GmbH (Standard Audio)',
      image_size: 600,
      file_type: 'flac',
      hostname: 'transfer.mondiamedia.com',
      username: 'su64',
      password: 'fiZiequ1ai',
      port: '22',
      ddex_version: '382',
      batch_size: '100' ,
      transfer_type: 'sftp',
      bit_depth: 16,
      delivery_queue: 'delivery-default',
      paused: 1
    )
  end


  task update_tim: :environment do
    tim_store = Store.find_or_create_by(short_name: "Tim")
    tim_store.update(
      name: "TIM",
      description: "TIMMUSIC is a digital music streaming platform that is made instantly available to users of the first telecommunications provider in Italy, Telecom Italia S.p.A on web or mobile app. When you distribute your releases to TIMMUSIC, you can get discovered by music fans across the country. The app boasts user generated and curated playlists, weekly discovery charts, and almost 50,000 active monthly users. <p> <strong> Reach </strong> - make your music available across Italy."
      )
    tim_store.update(is_active: is_active?) if ENV['STORE_ACTIVE'].present?

    knowledgebase_link = KnowledgebaseLink.where(article_slug: 'tin-description').first_or_create
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end

  def is_active?
    if ENV['STORE_ACTIVE'] == "true"
      true
    elsif ENV['STORE_ACTIVE'] == "false"
      false
    end
  end
end
