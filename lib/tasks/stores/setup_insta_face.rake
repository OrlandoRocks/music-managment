namespace :stores do 
    task setup_reels: :environment do 
        reels_store = Store.find_or_create_by(short_name: "FBReels")
        reels_store.update(
            name: "Facebook / Instagram / Reels",
            abbrev: "reels",
            delivery_service: "tunecore",
            is_active: 0
            )
      
          SalepointableStore.find_or_create_by!(
            store_id: reels_store.id,
            salepointable_type: "Album"
          )
      
          SalepointableStore.find_or_create_by!(
            store_id: reels_store.id,
            salepointable_type: "Single"
          ) 

          StoreDeliveryConfig.for('FBReels') || StoreDeliveryConfig.create!(
            store_id: reels_store.id,
            party_id: 'PADPIDA2018010804X',
            party_full_name: 'Facebook',
            image_size: 1600,
            hostname: 'sftp.fb.com',
            username: '2998261238',
            password: 'Tc3VD3T1R@!rRGz',
            ddex_version: '382',
            batch_size: '25' ,
            transfer_type: 'sftp',
            delivery_queue: 'delivery-default',
            distributable_class: "Distribution",
            paused: 1
          )
    end 

    task update_reels: :environment do 
      store = Store.find_by(short_name: "FBReels")
      store.update(position: 1200)

      knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: 'reels-description')
      knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end 
end 