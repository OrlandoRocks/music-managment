namespace :stores do
  task :setup_tiktok, [:store_name] => [:environment] do |command,args|
    bytedance_store = Store.find_or_create_by(short_name: "Bytedance")
    bytedance_store.update(
      name: args[:store_name] || "Tiktok",
      abbrev: "byda",
      short_name: "bytedance",
      delivery_service: "tunecore",
      description: "TikTok is the leading destination for short-form mobile video. Our mission is to inspire and enrich people’s lives by offering a home for creative expression and an experience that is genuine, joyful, and positive. TikTok has global offices including Los Angeles, New York, London, Paris, Berlin, Dubai, Mumbai, Singapore, Jakarta, Seoul, and Tokyo."
    )
    bytedance_store.update(is_active: ENV['STORE_ACTIVE']) if ENV['STORE_ACTIVE']

    SalepointableStore.find_or_create_by!(
      store_id: bytedance_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: bytedance_store.id,
      salepointable_type: "Single"
    )

    store_delivery_config = StoreDeliveryConfig.for('bytedance')
    store_delivery_config = StoreDeliveryConfig.create!(
      store_id: bytedance_store.id,
      party_id: "PADPIDA2018082301A,PADPIDA2018111407I",
      party_full_name: "​TikTok / Bytedance​,Project M / Bytedance​",
      image_size: 1600,
      file_type: "flac",
      bit_depth: 16,
      transfer_type: "sftp",
      ddex_version: "382",
      remote_dir: "upload",
      display_track_language: true
    ) unless store_delivery_config

    store_delivery_config.update(
      hostname: ENV['HOSTNAME'],
      username: ENV['USERNAME'],
      password: ENV['PASSWORD'],
      port: '8900'
    ) if ENV['HOSTNAME'].present? && ENV['USERNAME'].present? && ENV['PASSWORD'].present?

    store_delivery_config.update(paused: ENV['DELIVERY_CONFIG_ACTIVE']) if ENV['DELIVERY_CONFIG_ACTIVE']
  end
end
