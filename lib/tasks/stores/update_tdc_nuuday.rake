namespace :stores do
  task update_tdc_nuuday: :environment do
    if ENV['DISPLAY_NAME'].present?
      tdc_store = Store.find_by(short_name: 'Juke')
      return unless tdc_store

      tdc_store.update(name: ENV['DISPLAY_NAME'])
    end

    configs = {}
    configs['password'] = ENV['PASSWORD'] if ENV['PASSWORD'].present?
    configs['party_id'] = ENV['PARTY_ID'] if ENV['PARTY_ID'].present?
    configs['party_full_name'] = ENV['PARTY_FULL_NAME'] if ENV['PARTY_FULL_NAME'].present?

    if configs.present?
      tdc_config = StoreDeliveryConfig.for('Juke')
      return unless tdc_config

      tdc_config.update(configs)
    end
  end

  task update_tdc_nuuday_config: :environment do
    tdc_config = StoreDeliveryConfig.for('Juke')
    tdc_config.update(ddex_version: 382, file_type: 'flac', image_size: 1600) if tdc_config
  end
end
