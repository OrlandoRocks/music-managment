namespace :stores do
  task setup_joox: :environment do
    joox_store = Store.find_or_create_by(short_name:"Joox")
    joox_store.update(
      name: 'JOOX',
      abbrev: 'joox',
      short_name: 'Joox',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: joox_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: joox_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Joox') || StoreDeliveryConfig.create!(
      store_id: joox_store.id,
      image_size: 1600,
      file_type: 'flac',
      hostname: '119.28.4.159',
      username: 'TuneCore',
      password: 'TuneCore@02a67319',
      port: '53000',
      ddex_version: '382',
      batch_size: 50,
      transfer_type: 'sftp',
      keyfile: 'joox_rsa',
      bit_depth: 16,
      delivery_queue: 'delivery-default',
      paused: 1
    )
  end

   task update_joox: :environment do


    joox_store = Store.where(short_name: "Joox").first_or_create
    joox_store.update(
      name: "Joox",
      description: "Joox is one of the fastest growing streaming platforms in South Eastern Asia, with an increasing market share in South Africa. With millions of active users, the app has a community of music lovers and provides artists opportunities to engage with fans.<p> <strong> Strengths:​ </strong> Reach. Increase your potential to be discovered by music fans in Hong Kong, Macao, Indonesia, Malaysia, Thailand, Myanmar and South Africa."
      )
    joox_store.update(is_active: is_active?) if ENV['STORE_ACTIVE'].present?

    joox_config = StoreDeliveryConfig.for('Joox')
    if joox_config
      joox_config.update!(
        port: '65100',
        password: nil,
        username: 'TuneCore_Sftp',
        paused: 0
      )
    end

      knowledgebase_link = KnowledgebaseLink.where(article_slug: 'joox-description').first_or_create
      knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end

  def is_active?
    if ENV['STORE_ACTIVE'] == "true"
      true
    elsif ENV['STORE_ACTIVE'] == "false"
      false
    end
  end
end
