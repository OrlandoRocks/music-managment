namespace :stores do
  task setup_zed: :environment do
    zed_store = Store.find_or_create_by(short_name: "zed")
    zed_store.update(
      abbrev: 'zed',
      delivery_service: 'tunecore',
      is_active: 0,
      name: 'Zed',
      short_name: 'Zed'
    )

    SalepointableStore.find_or_create_by!(
      store_id: zed_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: zed_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('zed') || StoreDeliveryConfig.create!(
      batch_size: 100,
      bit_depth: 24,
      ddex_version: '382',
      delivery_queue: 'delivery-default',
      file_type: 'flac',
      hostname: '95.163.248.234',
      image_size: 1500,
      keyfile: 'zedplus_rsa',
      party_full_name: 'Tematika, ZAO',
      party_id: 'PADPIDA20151027010',
      paused: 1,
      port: '22',
      remote_dir: '/releases',
      store_id: zed_store.id,
      transfer_type: 'sftp',
      username: 'tunecore'
    )
  end

   task update_zed: :environment do
    zed_store = Store.where(short_name: "Zed").first_or_create
    zed_store.update(
        description: "Zed+ is a Russian music streaming service that can be used via exclusive partnerships with major telecommunications providers throughout the region. With Zed+, your music is instantly discoverable by music fans in less traditional digital markets. The app boasts curated playlists and a built-in social network for users to connect and share with friends. <strong> Strengths: </strong> Reach - make your music available to new fans in Russia."
        )
    zed_store.update(is_active: is_active?) if ENV['STORE_ACTIVE'].present?
    zed_config = StoreDeliveryConfig.for('Zed')

    knowledgebase_link = KnowledgebaseLink.where(article_slug: 'zed-description').first_or_create
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end

  def is_active?
    if ENV['STORE_ACTIVE'] == "true"
      true
    elsif ENV['STORE_ACTIVE'] == "false"
      false
    end
  end
end
