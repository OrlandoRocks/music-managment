namespace :stores do
  task setup_peloton: :environment do
    peloton_store = Store.find_or_create_by(short_name:"Peloton")
    peloton_store.update(
      name: 'Peloton',
      abbrev: 'Pel',
      short_name: 'Peloton',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: peloton_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: peloton_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Peloton') || StoreDeliveryConfig.create!(
      store_id: peloton_store.id,
      party_id: 'PADPIDA2020081303D',
      party_full_name: 'Peloton',
      image_size: 3000,
      file_type: 'mp3',
      hostname: 'sftp-label.pelomusic.io',
      username: 'ftp_tunecore',
      keyfile: 'neurotic_media_rsa',
      port: '22',
      ddex_version: '37',
      batch_size: 100,
      transfer_type: 'sftp',
      delivery_queue: 'delivery-default',
      paused: 1,
      use_manifest: 0
    )
  end

  task correct_peloton_abbrev: :environment do
    # store.abbrev should be lowercase

    Rails.logger.info("⏲ Beginning rake task... ⏲")

    peloton_store = Store.find_by(short_name:"Peloton")

    peloton_store.update!(abbrev: 'pel')

    Rails.logger.info("✅ Rake task completed! ✅")
  end
end
