namespace :stores do
  task setup_sound_cloud: :environment do
    sound_cloud_store = Store.find_or_create_by(short_name:"SCloud")
    sound_cloud_store.update!(
      name: 'Sound Cloud',
      abbrev: 'scld',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: sound_cloud_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: sound_cloud_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('SCloud') || StoreDeliveryConfig.create!(
      store_id: sound_cloud_store.id,
      party_id: 'PADPIDA20121010037',
      party_full_name: 'SoundCloud',
      image_size: 1600,
      file_type: 'flac',
      hostname: 'tunecore.ingest.soundcloud.com',
      username: 'contpart',
      keyfile: 'soundcloud_rsa',
      port: '22',
      remote_dir: '/home/contpart/data/',
      ddex_version: '382',
      batch_size: 100,
      transfer_type: 'sftp',
      bit_depth: '16',
      delivery_queue: 'delivery-default',
      use_manifest: 1
    )
  end

  task update_sound_cloud: :environment do
    sound_cloud_store = Store.find_by(short_name:"SCloud")
    sound_cloud_store.update(
      description: "SoundCloud Go is the same SoundCloud you’ve known for years taken to the next level, featuring an even larger catalog of songs brought onto the platform. Fans can still listen to tens of millions of tracks completely free on the SoundCloud Go platform, including releases from TuneCore artists, remixes, viral hits, and more! Plus, with SoundCloud Go’s algorithm-fueled discovery features, your releases could be queued up after a chart-topping hit. <br />

      <strong>Strengths:</strong> Reach - make your music even more available to new fans from all over the world."
    )
    
    knowledgebase_link = KnowledgebaseLink.where(article_slug: "#{sound_cloud_store.abbrev}-description").first_or_create
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end 

  task setup_sound_cloud_block_feed: :environment do
    sound_cloud_store = Store.find_or_create_by(short_name:"SCblock")
    sound_cloud_store.update!(
      name: 'Sound Cloud Block Feed',
      abbrev: 'scbk',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: sound_cloud_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: sound_cloud_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('SCblock') || StoreDeliveryConfig.create!(
      store_id: sound_cloud_store.id,
      party_id: 'PADPIDA20121010037',
      party_full_name: 'SoundCloud',
      image_size: 1600,
      file_type: 'flac',
      hostname: 'tunecore_fp.ingest.soundcloud.com',
      username: 'contpart',
      keyfile: 'soundcloud_rsa',
      port: '22',
      remote_dir: '/home/contpart/data/',
      ddex_version: '382',
      batch_size: 100,
      transfer_type: 'sftp',
      bit_depth: '16',
      delivery_queue: 'delivery-default',
      use_manifest: 1
    )
  end
end
