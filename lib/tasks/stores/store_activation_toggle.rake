namespace :stores do 
    task activate_store: :environment do 
        store = Store.find_by(short_name: ENV["STORE_SHORT_NAME"])
        raise "Store #{ENV["STORE_SHORT_NAME"]} Not Found" unless store

        store.update(is_active: true, in_use_flag: true)
        Rails.logger.info "Store #{store.short_name} (store_id: #{store.id}) has been succesfully activated!"
    end 


    task deactivate_store: :environment do 
        store = Store.find_by(short_name: ENV["STORE_SHORT_NAME"])
        raise "Store #{ENV["STORE_SHORT_NAME"]} Not Found!" unless store

        store.update(is_active: false, in_use_flag: false)
        Rails.logger.info "Store #{store.short_name} (store_id: #{store.id}) has been succesfully deactivated!"
    end 
end 