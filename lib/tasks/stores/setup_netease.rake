namespace :stores do
  task setup_netease: :environment do
    netease_store = Store.find_or_create_by(short_name: "NetE")
    netease_store.update(
      name: "NetEase",
      abbrev: "net",
      short_name: "NetE",
      delivery_service: "tunecore",
      is_active: 0
    )

    netease_store.update(is_active: ENV['STORE_ACTIVE']) if ENV['STORE_ACTIVE']

    SalepointableStore.find_or_create_by!(
      store_id: netease_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: netease_store.id,
      salepointable_type: "Single"
    )

    store_delivery_config = StoreDeliveryConfig.for("NetE")
    store_delivery_config ||= StoreDeliveryConfig.create!(
      store_id: netease_store.id,
      party_id: "PADPIDA2015122102I",
      party_full_name: "NetEase(Hangzhou) Network Co.,Ltd.",
      image_size: 1500,
      username: "tunecore",
      password: "cV5*jK4",
      use_manifest: 1,
      file_type: "flac",
      hostname: "103.126.92.132",
      port: 21112,
      ddex_version: '382',
      transfer_type: "sftp",
      bit_depth: 24,
      delivery_queue: "delivery-default",
      paused: 1
    )

    store_delivery_config.update(paused: ENV['DELIVERY_CONFIG_PAUSED']) if ENV['DELIVERY_CONFIG_PAUSED']
  end

  task update_netease: :environment do
    netease_store = Store.find_by(short_name: "NetE")
    return unless netease_store

    netease_store.update(
      description: 'NetEase Music is one of the largest streaming platforms in China. With 800 million registered users, NetEase Music boasts a catalog of 50% international releases and social features that encourage fan engagement and discovery.<p><strong>Strength:</strong> Reach. Get discovered within a growing music market and across popular social media networks.',
      is_active: ENV['STORE_ACTIVE'] ? ENV['STORE_ACTIVE'] : 0
    )

    knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "netease-description")
    knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end
end
