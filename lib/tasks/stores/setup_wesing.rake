namespace :stores do
  task setup_wesing: :environment do
    if Store.find_by(short_name: "wesing")
      puts "wesing store already exists"
      next
    end

    wesing_store = Store.find_or_create_by(short_name: "wesing") do |store|
      store.abbrev = "wsng"
      store.short_name = "wesing"
      store.is_active = false
      store.delivery_service = "believe"
    end

    StoreDeliveryConfig.find_or_create_by(store_id: wesing_store.id) do |store_config|
      store_config.party_id = "PADPIDA20140404063"
      store_config.image_size = 1440
      store_config.file_type = "flac"
      store_config.bit_depth = 16
      store_config.ddex_version = "382"
      store_config.paused = true
    end

    SalepointableStore.find_or_create_by(store_id: wesing_store.id, salepointable_type: "Album")

    SalepointableStore.find_or_create_by(store_id: wesing_store.id, salepointable_type: "Single")
  end

  task update_wesing: :environment do
    wesing_store = Store.find_by(short_name: "wesing")
    exit unless wesing_store
    wesing_store.update_attributes(
      short_name: "WeSing",
      delivery_service: "tunecore",
      abbrev: "wsng"
    )
    wesing_store.update_attributes(is_active: ENV['WESING_STORE_ACTIVE']) if ENV['WESING_STORE_ACTIVE'].present?

    store_delivery_config = StoreDeliveryConfig.for("wesing")
    store_delivery_config.update_attributes(
      party_full_name: "Tencent",
      delivery_queue: "delivery-default",
      transfer_type: "ftp",
      remote_dir: nil,
      batch_size: 10,
      use_resource_dir: 1,
      use_manifest: 0
    )

    store_delivery_config.update_attributes(
      hostname: ENV['HOSTNAME'],
      username: ENV['USERNAME'],
      port:     "8080",
      password: ENV['PASSWORD'],
    ) if ENV['HOSTNAME'].present? && ENV['USERNAME'].present? && ENV['PASSWORD'].present?

    store_delivery_config.update_attributes(paused: !ENV['WESING_ACTIVE']) if ENV['WESING_ACTIVE']

    knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "wsng-description")
    knowledgebase_link.update_attributes(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
  end
end
