namespace :stores do
    task setup_uma: :environment do
      uma_store = Store.find_or_create_by_short_name("uma")
      uma_store.update(
        name: 'UMA',
        abbrev: 'uma',
        short_name: 'UMA',
        delivery_service: 'tunecore',
        is_active: 0
      )

      SalepointableStore.find_or_create_by!(
        store_id: uma_store.id,
        salepointable_type: "Album"
      )

      SalepointableStore.find_or_create_by!(
        store_id: uma_store.id,
        salepointable_type: "Single"
      )

      store_delivery_config = StoreDeliveryConfig.for('uma')
      store_delivery_config ||= StoreDeliveryConfig.create!(
        store_id: uma_store.id,
        party_id: 'PADPIDA20150803045',
        party_full_name: 'United Media Agency',
        image_size: 1500,
        file_type: 'flac',
        hostname: 'ftp.um-agency.com',
        keyfile: 'uma_rsa',
        port: '2324',
        ddex_version: '382',
        remote_dir: 'tunecore',
        transfer_type: 'sftp',
        bit_depth: 16,
        delivery_queue: 'delivery-default',
        paused: 1
      )
    end

    task update_uma: :environment do
      uma_store = Store.find_or_create_by_short_name("uma")
      uma_store.update(
        name: 'BOOM',
        description: 'BOOM is one of the most popular streaming platforms in Russia, boasting a catalog of over 35 million tracks. BOOM users are able to stream, discover, and create/follow playlists, and the app is integrated within two of Russia’s largest social networks, <a class="sc-eXNvrr fPRyGw" href="http://VK.com" title="http://VK.com">VK.com</a> and <a class="sc-eXNvrr fPRyGw" href="http://OK.ru" title="http://OK.ru">OK.ru</a>.</p><p><strong>Strength:</strong> Reach. Get discovered within a growing music market and across popular social media networks.',
        is_active: ENV['STORE_ACTIVE'] ? ENV['STORE_ACTIVE'] : 0
      )
      store_delivery_config = StoreDeliveryConfig.for('uma')
      store_delivery_config.update(paused: ENV['DELIVERY_CONFIG_PAUSED']) if ENV['DELIVERY_CONFIG_PAUSED']

      knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "uma-description")
      knowledgebase_link.update(article_id: ENV['ARTICLE_ID'], route_segment: "/articles/") if knowledgebase_link.present? && ENV['ARTICLE_ID'].present?
    end
  end
