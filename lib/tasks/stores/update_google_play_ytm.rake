namespace :stores do
  task update_google_play_ytm: :environment do
    google_store = Store.find_by(short_name: "Google")
    google_store.update(
      name: ENV['GOOGLE_NAME']
    ) if ENV['GOOGLE_NAME'].present?

    if ENV['DEACTIVATE_YTMUSIC'].present?
      activate = ENV['DEACTIVATE_YTMUSIC'].to_i == 1 ? 0 : 1
      ytm_store = Store.find_by(short_name: "YTMusic")
      return unless ytm_store

      ytm_store.update(
        is_active: activate,
        in_use_flag: activate
      )

      ytm_store_config = StoreDeliveryConfig.find_by(store_id: ytm_store.id)
      ytm_store_config.update(
        paused: !activate
      ) if ytm_store_config
    end
  end
end