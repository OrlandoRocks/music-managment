namespace :update_store_delivery_config do
  desc 'Update store delivery config for a store'
  task :run, [:store_id, :key, :value] => [:environment] do |command, args|
    if args.store_id.present? && args.key.present? && args.value.present?
      StoreDeliveryConfig.find_by(store_id: args.store_id).update!({"#{args.key}": args.value})
    else
      puts "Invalid command! Sample: bundle exec rake update_store_delivery_config:run[store_id,image_size,1417]"
    end
  end
end