namespace :stores do
  task setup_gaana: :environment do
    gaana_store = Store.find_or_create_by(short_name: "Gaana")
    gaana_store.update(
      name: 'Gaana',
      abbrev: 'gaana',
      short_name: 'Gaana',
      delivery_service: 'tunecore',
      is_active: 0
    )

    SalepointableStore.find_or_create_by!(
      store_id: gaana_store.id,
      salepointable_type: "Album"
    )

    SalepointableStore.find_or_create_by!(
      store_id: gaana_store.id,
      salepointable_type: "Single"
    )

    StoreDeliveryConfig.for('Gaana') || StoreDeliveryConfig.create!(
      store_id: gaana_store.id,
      party_id: 'PADPIDA2012081610S',
      party_full_name: 'Gaana',
      image_size: 1600,
      file_type: 'wav',
      hostname: '223.165.25.236',
      username: 'TuneCore',
      password: 'TuneCore@123',
      port: '22',
      ddex_version: '37',
      remote_dir: '/upload',
      transfer_type: 'sftp',
      bit_depth: 16,
      delivery_queue: 'delivery-default',
      paused: 1
    )
  end

  task update_gaana: :environment do
    knowledgebase_link = KnowledgebaseLink.find_or_create_by(article_slug: "gaana-description")
    knowledgebase_link.update(article_id: ENV['GAANA_ARTICLE_ID'],
                                         route_segment: "/articles/") if knowledgebase_link.present? && ENV['GAANA_ARTICLE_ID'].present?
  end
end
