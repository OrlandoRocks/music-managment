namespace :stores do
    task setup_ytsr_proxy: :environment do 
        ytsr_proxy = Store.find_or_create_by(short_name: "YouTubeTracksProxy")
        ytsr_proxy.update(
          name: "YouTube Track Monetization",
          abbrev: "yttm",
          short_name: "YTSRProxy",
          delivery_service: "tunecore",
          is_active: 0
        )
        
        ytsr_proxy.update(is_active: ENV['STORE_ACTIVE']) if ENV['STORE_ACTIVE']
    
        SalepointableStore.find_or_create_by!(
          store_id: ytsr_proxy.id,
          salepointable_type: "Album"
        )
    
        SalepointableStore.find_or_create_by!(
          store_id: ytsr_proxy.id,
          salepointable_type: "Single"
        )
    end 
end