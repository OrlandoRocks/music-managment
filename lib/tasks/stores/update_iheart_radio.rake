namespace :stores do
  task update_iheart_radio: :environment do
    iheart_config = StoreDeliveryConfig.for('Thumbplay')
    iheart_config.update(ddex_version: 382, party_id: 'PADPIDA2010121501F', party_full_name: 'iheartradio', image_size: 1400) if iheart_config
  end
end
