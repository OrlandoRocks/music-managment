namespace :stores do
  desc "Update Simfy Africa to MusicTime"
  task update_simfy_africa_to_musictime: :environment do
    simfy_store = Store.find_by(short_name: 'SimfyAF')

    simfy_store.update!(name: 'MusicTime',
                       abbrev: 'mtn',
                       short_name: 'M_Time',
                       description: "MusicTime is a 'pay-as-you-go' streaming service available to music fans all across Africa. " \
                                    "MTN Users can purchase either two hours or five hours of MusicTime that includes bundled data to stream songs. " \
                                    "Fans who download the app will have access to artist-specific and genre-focused editorial playlists. <br>" \
                                    "<strong>Strength:</strong> Focus on emerging artists throughout Africa and access to leading telecom provider MTN's marketing inventory."
    )

    simfy_kbl = KnowledgebaseLink.find_by(article_slug: 'simfy-africa-description')

    musctime_kbl = simfy_kbl.dup
    musctime_kbl.article_slug = 'mtn-description'
    musctime_kbl.save!
  end
end
