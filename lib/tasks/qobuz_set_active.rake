namespace :qobuz_setup do
  task :set_active => :environment do
    qobuz_store = Store.find_by(short_name: 'Qobuz')
    qobuz_store.update!(is_active: true, delivery_service: 'believe')
  end
end
