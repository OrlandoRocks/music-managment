namespace :feature do
  desc "Specific Raketasks that need to be run for Payout Tax PH2: Backup Withholding -- (https://tunecore.atlassian.net/browse/CF-453)"
  task us_tax_withholding: :environment do

    # Insert rake task names in below
    Rake::Task["tax_blocking:backfill_tax_blocked_at_and_rollover_funds"].invoke(2021)
  end
end
