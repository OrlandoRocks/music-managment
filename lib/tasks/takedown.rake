require "#{Rails.root}/lib/logging"
require "#{Rails.root}/lib/slack_notifier"
namespace :takedown do
  # take downs albums where the yearly or monthly renewal has expired past the applicable grace period
  task expired_albums: :environment do 
    album_ids = Album.fetch_albums_ids_for_renewal_takedown
    opts = { 
      subject: "Automated Takedown", 
      note: "Release was taken down via takedown:expired_albums task", 
      user_id: 0 
    }
    Takedown::BatchTakedownWorker.perform_async(album_ids, true, opts)
  end

  desc "Emails a csv list of albums for takedown. Example: rake 'takedown:send_takedown_email[false, 2006-01-01 00:00:00, 2006-02-01 00:00:00]' "
  task :send_takedown_email, [:production_run, :start, :finish] => [:environment] do |command, args|
    log_message  "rake_task=takedown:send_takedown_email params=#{args}"
    start_time = Time.now
    case Rails.env
    when "production"
       recipients = ["dod-am-music@amazon.com"]
    else
      recipients = ["ops@tunecore.com"]
    end

    if args[:production_run].nil?
      dryrun = true
    else
      dryrun = (args[:production_run] == 'true') ? false:true
    end

    if args[:start].nil? || args[:finish].nil?
      start  = 7.days.ago.strftime('%Y-%m-%d 00:00:00') #we're running the task every Wed but we're sending a takedown for the previous week
      finish = Time.now.strftime('%Y-%m-%d 00:00:00')
      log_message  "rake_task=takedown:send_takedown_email state=defaulting_to_last_week start=#{start} finish=#{finish} dryrun=#{dryrun}"
    else
      start = args[:start]
      finish = args[:finish]
    end

    albums = Album.for_amazon_takedown(start, finish)

    if !albums.empty? && !dryrun
        log_message "rake_task=takedown:send_takedown_email recipients=#{recipients} state=processing_email album_ids=#{albums.collect{|a| a.upc.number}.join(', ')} delivery_type=email AOD_STORE_ID=#{Store::AOD_STORE_ID} dryrun=#{dryrun}"
        TakedownNotifier.albums_for_takedown(albums, { recipients: recipients, store_id: Store::AOD_STORE_ID }).deliver
        log_message "rake_task=takedown:send_takedown_email state=success"
    end

    end_time = Time.now
    log_message "rake_task=takedown:expired_albums state=completed finish_time=#{end_time} total_releases=#{albums.size} total_time=#{(end_time-start_time)} album_ids=#{albums.collect{|a| a.upc.number}.join(', ')}"
  end

  desc "Takedown track monetizations for one store from a CSV of TC or Optional ISRCs"
  task takedown_by_isrc: :environment do
    raise "Set a STORE_ID env var" unless ENV["STORE_ID"]
    raise "Set a FILE_PATH env var" unless ENV['FILE_PATH']

    store_id  = ENV['STORE_ID'].to_i
    file_path = ENV['FILE_PATH']
    limit     = ENV['LIMIT'] || 25_000

    key       = "takedown_by_isrc-#{file_path}"
    completed = $redis.get(key).blank? ? 0 : $redis.get(key).to_i
    isrcs     = File.readlines(file_path.to_s)[completed.to_i, limit.to_i].map(&:chomp)

    TrackMonetization.joins(:song)
                     .where(store_id: store_id)
                     .where(Song.arel_table[:tunecore_isrc].in(isrcs)
                       .or(Song.arel_table[:optional_isrc].in(isrcs)))
                     .find_each do |tm|
                       TrackMonetizationBlocker.find_or_create_by!(
                         song_id: tm.song_id,
                         store_id: store_id
                       )
                     end
    $redis.set(key, completed + isrcs.size)
  end
end

def log_message(message="")
  puts message
  Rails.logger.info message
end
