namespace :ytsr do
  desc "Updates youtube monetization for missed users"
  task update_monetization: :environment do
    Rails.logger.info 'Started updating youtube_monetizations'
    YoutubeMonetization.find_by_sql("select * from youtube_monetizations where id in (select related_id from purchases where paid_at is not null and related_type = 'YoutubeMonetization') and effective_date is NULL").map(&:after_successful_purchase)
    Rails.logger.info 'Completed updating youtube_monetizations'
  end

  desc "Backfill all genres for youtube monetization"
  task create_track_monetizations: :environment do
    Rails.logger.info 'Started creating track_monetizations for excluded genres'
    query = "SELECT distinct(ytm.id), ytm.person_id
              FROM albums al
              INNER JOIN songs s ON al.id = s.album_id
              INNER JOIN people p ON p.id = al.person_id
              INNER JOIN youtube_monetizations ytm ON ytm.person_id = p.id
              LEFT OUTER JOIN track_monetizations ON s.id = track_monetizations.song_id AND track_monetizations.store_id = 48
              WHERE al.payment_applied = TRUE
                AND al.legal_review_state = 'APPROVED'
                AND (al.finalized_at IS NOT NULL)
                AND al.is_deleted = FALSE
                AND al.takedown_at IS NULL
                AND (album_type != 'Ringtone')
                AND track_monetizations.id IS NULL"
    YoutubeMonetization.find_by_sql(query).map(&:after_successful_purchase)
    Rails.logger.info 'Completed creating track_monetizations for excluded genres'
  end
end
