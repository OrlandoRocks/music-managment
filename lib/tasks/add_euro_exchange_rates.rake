desc "Add Euro Foreign Exchange Rates"
task add_euro_exchange_rates: :environment do
  csv_text = File.read(Rails.root.join('db/seed/csv/data/euro_exchange_rates.csv'))
  csv      = CSV.parse(csv_text, headers: true)
  csv.each do |row|
    ForeignExchangeRate.find_or_create_by!(
      exchange_rate: row['exchange_rate'],
      source_currency: row['source_currency'],
      target_currency: row['target_currency'],
      valid_from: row['valid_from'],
      valid_till: row['valid_till'],
      created_at: row['created_at'],
      updated_at: row['updated_at']
    )
  end
end
