namespace :plans do
  desc "generate plans_stores records"
  task generate_plans_stores: :environment do
    if PlansStore.exists?
      p "Rake Task Failed:-------------------------------"
      raise "Task requires plans_stores table to be empty"
    end

    plan_ids         = Plan.all.order(:id).pluck(:id)
    paid_plan_ids    = plan_ids.drop(1)
    social_store_ids = Store.where(discovery_platform: true).pluck(:id)
    paid_store_ids   = Store
                       .where(discovery_platform: false)
                       .pluck(:id)

    social_plans_stores_hashes = create_plan_stores_hashes(plan_ids, social_store_ids)
    paid_plans_stores_hashes   = create_plan_stores_hashes(paid_plan_ids, paid_store_ids)

    PlansStore.insert_all(social_plans_stores_hashes + paid_plans_stores_hashes)
  end

  def create_plan_stores_hashes(plan_ids, stores)
    plan_ids.flat_map do |plan_id|
      stores.map do |store_id|
        {
          plan_id: plan_id,
          store_id: store_id,
          created_at: Time.now,
          updated_at: Time.now
        }
      end
    end
  end
end
