# frozen_string_literal: true

# bundle exec rake plans:backfill_plans_plan_feature_history_with_id_74
namespace :plans do
  desc 'Update PlansPlanFeatureHistory, with ID 74, to use 6/7/2022 for its created_at and updated_at values.'
  task backfill_plans_plan_feature_history_with_id_74: :environment do
    history = PlansPlanFeatureHistory.find_by(id: 74)
    raise '❌ Could not find PlansPlanFeatureHistory #74. Exiting.' if history.nil?
    
    date = Date.new(2022, 6, 7)
    history.update(
      created_at: date,
      updated_at: date
    )

    Rails.logger.info('🎉 Updated PlansPlanFeatureHistory #74.')
  end
end
