require 'net/ftp'

namespace :believe do
  task send_them_all: :environment do
    last_id = ($redis.get("believe:send_them_all:last_id") || "10000000").to_i
    limit = ENV["LIMIT"] || 10000

    believe_exp_store = Store.find_by(short_name: "BelieveExp")

    albums = Album.includes(salepoints: [:distributions, :store]).where(Album.arel_table[:id].lt(last_id)).order("id DESC").limit(limit)

    albums.each do |album|
      begin
        unless album.salepoints.select { |sp| sp.store.is_active && !sp.distributions.select { |d| d.state == "delivered" }.empty? }.empty?
          believe_salepoints = album.salepoints.select { |sp| sp.store.short_name =~ /Believe/ }
          if believe_salepoints.empty?

            new_salepoints, failed_salepoints = album.add_stores([believe_exp_store])

            if new_salepoints.length == 1 && failed_salepoints.empty?
              believe_distribution = Distribution.create!({
                converter_class: "MusicStores::Believe::Converter",
                salepoints: new_salepoints,
                state: "new",
                retry_count: 0,
                petri_bundle: album.petri_bundle
              })
              Delivery::DistributionWorker.perform_async(believe_distribution.class.name, believe_distribution.id, "full_delivery")
              Rails.logger.info "#{album.id}: Added believe salepoint and delivered distribution"
            else
              Rails.logger.info "#{album.id}: Failed to add believe salepoint"
            end
          else
            delivered_distros = believe_salepoints.map { |sp| sp.distributions.select { |d| d.state == "delivered" } }.flatten.uniq
            if delivered_distros.empty?
              distro = believe_salepoints.first.distributions.first
              if distro
                Delivery::DistributionWorker.perform_async(distro.class.name, distro.id, "full_delivery")
              end
              Rails.logger.info "#{album.id}: Delivered existing believe distribution"
            else
              Rails.logger.info "#{album.id}: Already delivered"
            end
          end
        else
          Rails.logger.info "#{album.id}: Skipping album because never distributed"
        end
      rescue => e
        p "Error with album #{album.id}: #{e.message}"
      ensure
        $redis.set("believe:send_them_all:last_id", album.id)
      end
    end
  end

  task export_data: :environment do
    limit = ENV["LIMIT"] || 10000
    store_short_name = ENV["STORE_SHORT_NAME"] || "Spotify"

    last_id = ($redis.get("believe:takendown_#{store_short_name.downcase}:album_id") || "0").to_i

    takendown_albums = Album.joins(salepoints: :store)
      .where(Store.arel_table[:short_name].eq(store_short_name))
      .where(Album.arel_table[:takedown_at].not_eq(nil))
      .where(Album.arel_table[:id].gt(last_id))
      .limit(limit)
      .order("albums.id asc")

    believe_exp_store = Store.find_by(short_name: "BelieveExp")
    believe_liv_store = Store.find_by(short_name: "BelieveLiv")

    takendown_albums.each do |album|
      existing_exp_sp = Salepoint.where(salepointable_id: album.id, store_id: believe_exp_store.id)
      existing_liv_sp = Salepoint.where(salepointable_id: album.id, store_id: believe_liv_store.id)

      next if existing_liv_sp.present? || existing_exp_sp.present?

      new_salepoints, failed_salepoints = album.add_stores([believe_exp_store])

      begin
        if new_salepoints.length == 1 && failed_salepoints.empty?
          believe_distribution = Distribution.create!({
            converter_class: "MusicStores::Believe::Converter",
            salepoints: new_salepoints,
            state: "new",
            retry_count: 0,
            petri_bundle: album.petri_bundle
          })
          Delivery::DistributionWorker.perform_async(believe_distribution.class.name, believe_distribution.id, "full_delivery")

          $redis.set("believe:takendown_#{store_short_name.downcase}:album_id", album.id)
            Rails.logger.info("Added Believe salepoint to album #{album.id}")
        else
          raise "Salepoint could not be created"
        end
      rescue => e
        $redis.lpush("believe:failed_albums", album.id)
        Rails.logger.error "Unable to add Believe salepoint to album #{album.id}"
      end
    end
  end

  task export_people: :environment do
    limit         = ENV["LIMIT"] || 10000
    last_id       = ($redis.get("believe:last_exported:person_id") || "0").to_i
    DEFER_BELIEVE = false
    people_table  = Person.arel_table

    p "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} - Querying person data"

    people = Person.limit(limit).order("people.id ASC").
              where(people_table[:is_verified].eq(true).
                and(people_table[:created_on].lt(Date.new(2016,4,18))).
                and(people_table[:id].gt(last_id)))

    if people.empty?
      p "Exiting because no Person records found."
      return
    end

    p "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} - Writing JSON data for #{people.length} people"

    file_name = "tmp/tc_person_#{Time.now.strftime("%Y%m%d_%H%M%S")}.json#{ Rails.env.production? ? "" : ".test"}"
    File.open(file_name, "w") do |file|
      people.each_with_index do |person, cnt|
        record_num = cnt + 1
        file.write(Believe::Person.create_client_data(person).to_json)
        file.write("\n")
        $redis.set("believe:last_exported:person_id", person.id)
        p "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} - Completed writing #{record_num} of #{people.length}" if record_num % 1000 == 0 || record_num == people.length
      end
    end

    p "#{Time.now.strftime("%Y-%m-%d %H:%M:%S")} - Uploading #{file_name} to Believe server"
    ftp = Net::FTP.new(BELIEVE_CONFIG["FTP_HOST"], BELIEVE_CONFIG["FTP_USER"], BELIEVE_CONFIG["FTP_PASS"])
    ftp.passive = true
    ftp.put(file_name)
    ftp.close
  end
end
