namespace :refunds do
  desc "create refunds based of info provided in CSV"
  task :bulk_refund, [:s3_file_name] => :environment do |_, args|
    def create_refund(invoice_id, refund_amount, refund_reason_id)
      refund_service = AutoRefundService.new(
        system_admin,
        invoice_id: invoice_id,
        refund_level: Refund::INVOICE,
        request_amount: Float(refund_amount),
        refund_type: Refund::FIXED_AMOUNT,
        refund_reason_id: refund_reason_id,
        tax_inclusive: true,
        refunded_by_id: system_admin.id
      )

      refund_service.process

      if refund_service.refund.nil? || refund_service.refund.error?
        puts "Failed to create refund for invoice #{invoice_id}"
      else
        puts "Successfully created refund for invoice #{invoice_id}"
      end
    end

    def system_admin
      @system_admin ||= Person.find_by(email: ENV["SYSTEM_ADMIN"])
    end

    refunds = RakeDatafileService.csv(args[:s3_file_name])

    refunds.each do |refund|
      create_refund(
        refund["invoice_id"],
        Float(refund["amount"]),
        refund["refund_reason_id"]
      )
    end

    puts "Refunds created"
  end
end
