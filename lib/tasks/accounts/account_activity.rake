namespace :account_activity do
  desc "mark accounts inactive and sends email to reactivate"
  task mark_dormant_and_send_email: :environment do
    excluded_users_file_key = ENV.fetch('EXCLUDED_USERS_FILE_KEY', nil)
    excluded_users = RakeDatafileService
                       .csv(excluded_users_file_key)
                       .map{|row| row['person_id']} if excluded_users_file_key.present?

    dormant_users = Person
                      .needs_marked_dormant
                      .for_united_states_and_territories
                      .for_not_locked_accounts
                      .where.not(id: excluded_users)

    Rails.logger.info("Sending mail to #{dormant_users.length} users")

    dormant_users.find_each do |person|
      Rails.logger.info("Sending mail to Account ID: #{person.id}")

      balance_history_url = 'https://web.tunecore.com/my_account/list_transactions' #TODO fix this to not be hardcoded
      MailerWorker.perform_async("AccountActivityMailer", :inactivity_email, person.id, balance_history_url)
    end

    Rails.logger.info("Mail queued for #{dormant_users.length} users")

    dormant_users.update_all(dormant: true)
    Rails.logger.info("#{dormant_users.length} users marked dormant")
  end

  #TODO dry up these rake tasks
  #this is not dry, but it's high priority and I felt more comfortable keeping it separate
  task mark_dormant_and_send_email_to_targeted_users: :environment do
    excluded_users_file_key = ENV.fetch('EXCLUDED_USERS_FILE_KEY', nil)
    excluded_user_ids = RakeDatafileService
                          .csv(excluded_users_file_key)
                          .map{|row| row['person_id']} if excluded_users_file_key.present?
    targeted_users_file_key = ENV.fetch('TARGETED_USERS_FILE_KEY', nil)
    dormant_user_ids = RakeDatafileService
                         .csv(targeted_users_file_key)
                         .map{|row| row['person_id']} if dormant_user_ids.present?
    dormant_users = Person
                      .where(id: dormant_user_ids)
                      .where.not(id: excluded_user_ids)
                      .for_not_locked_accounts
    Rails.logger.info("Sending mail to #{dormant_users.length} users")

    dormant_users.find_each do |person|
      Rails.logger.info("Sending mail to Account ID: #{person.id}")

      balance_history_url = 'https://web.tunecore.com/my_account/list_transactions' #TODO fix this to not be hardcoded
      MailerWorker
        .perform_async("AccountActivityMailer", :inactivity_email_for_targeted_users, person.id, balance_history_url)
    end

    Rails.logger.info("Mail queued for #{dormant_users.length} users")
  end
end
