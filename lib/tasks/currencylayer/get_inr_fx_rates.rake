# frozen_string_literal: true

desc "get inr fx rates from payoneer api"
namespace :currencylayer do
  desc "Get USD to INR fx rates for India website"
  task get_inr_fx_rates: :environment do
    exchange_service = Currencylayer::ExchangeService.new
    exchange_service.store_exchange_rates("USD", "INR");
  end
end
