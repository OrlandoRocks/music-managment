namespace :albums do
  task populate_metadata_language_code_id_column: :environment do
    language_ids_hash = {}
    LanguageCode.pluck(:id,:code).each { |l| language_ids_hash[l.last] = l.first }
    albums = Album.where(metadata_language_code_id: nil)

    language_ids_hash.each do |code, id|
      albums.where(language_code: code).in_batches do |batch|
        begin
          Rails.logger.info "Updating #{batch.count} albums with lang code #{code}"
          batch.update_all(metadata_language_code_id: id)
        rescue ActiveRecord::RecordInvalid => exception
          Rails.logger.info "Album #{album.id} failed while updating metadata_language_code_id: #{exception.message}"
        end
      end
    end
  end
end
