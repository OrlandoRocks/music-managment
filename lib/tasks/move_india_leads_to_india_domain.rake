namespace :transient do
  desc "Check which users are eligible to be migrated to TuneCore India"
  task move_india_leads_to_india_domain: :environment do
    # since this rake task should only be run once, and because writing the person_subscription_statuses portion in
    # ActiveRecord is inelegant, we are going to hardcode in the single person that currently meets
    # the criteria of having a TC Social account, as TC Social is not available in India
    #
    #
    #
    # In order to find the list of people for the variable below: people_to_exclude
    # (people with tc social that need to be migrated to .in)
    # in the future, run in a local sql client and replace the current variable with those person_ids:
    #------------------------------------------------
    # select distinct people.id
    # from people
    # left join purchases on people.id = purchases.person_id
    # left join invoices on people.id = invoices.person_id
    # join person_subscription_statuses on person_subscription_statuses.person_id = people.id
    # 	and subscription_type = 'Social'
    #     AND effective_date IS NOT NULL
    #     AND (termination_date IS NULL
    #       OR termination_date > CURRENT_DATE)
    # where purchases.id is null and invoices.id is null and people.country_website_id != 8 and people.country = 'IN';
    #------------------------------------------------


    #to run this rake task with different people_to_exclude, run the query above,
    # and with the output (e.g. person_ids 1,2,3) run
    # bundle exec rake transient:move_india_leads_to_india_domain PEOPLE_TO_EXCLUDE=1,2,3
    people_to_exclude = ENV.fetch('PEOPLE_TO_EXCLUDE', "2777678").split(",").map(&:to_i)
    all_leads = Person
                  .left_joins(:purchases)
                  .left_joins(:invoices)
                  .left_joins(:person_transactions)
                  .where(purchases: {id:nil})
                  .where(invoices: {id:nil})
                  .where(person_transactions: {id:nil})
                  .where(country: Country::INDIA_ISO)
                  .where.not(country_website_id: CountryWebsite::INDIA)
                  .where.not(id: people_to_exclude)
    people_without_ccs = all_leads.left_joins(:stored_credit_cards).where(stored_credit_cards: {id:nil})
    people_with_ccs = all_leads.joins(:stored_credit_cards).distinct
    #we want to do credit card people last because we don't want them to be on .in with bt cards for long
    people_to_update = people_without_ccs + people_with_ccs


    Rails.logger.info("Starting Migration for #{people_to_update.length} users")
    Rails.logger.info("--------------------------")

    people_to_update.each do |p|
      Rails.logger.info("Starting Migration for person_id #{p.id}")

      #we have to skip validations because many users have incorrect phone numbers.
      # On checkout we get the correct phone numbers if their current number is invalid
      p.update_attribute(:country_website_id, CountryWebsite::INDIA)
      CountryWebsiteUpdater::Update.update(person: p)

      Rails.logger.info("Finished Migration for person_id #{p.id}")
      Rails.logger.info("--------------------------")
    end

    Rails.logger.info("Successfully Migrated #{people_to_update.length} users to .in")

    Braintree::PersonCardsDeleterService.process(people_with_ccs)
  end
end

