require "#{Rails.root}/lib/logging"

namespace :publishing_update do

  desc 'Send Publishing Welcome Emails 24 Hours after user signed up'
  task :send_welcome_emails => :environment do |t, args|
    start_time = Time.now
    date = ENV['DATE'] || Date.today
    log("Publishing Batch","Start publishing_update:send_welcome_emails between #{date - 48.hours} and #{date - 24.hours}", :time => start_time)
    composers_to_notify = Composer.where("created_at >= ? AND created_at <= ?", date - 48.hours, date - 24.hours)
    count = 0
    composers_to_notify.each do |composer|
      BatchNotifier.publishing_welcome(composer.person).deliver
      log("Publishing Batch", 'Publishing update email sent to customer', :email => composer.email, :person => composer.person.id)
      count += 1
    end
    log("Publishing Batch", 'End publishing_update:send_welcome_emails', :count => count, :time => pretty_time_since(start_time))
  end
end
