namespace :revenue_streams do
  desc "creates all config records for tying revenue streams to person transaction types"
  task create_base_configs: :environment do
    revenue_streams                      = CSV.open("db/seed/csv/revenue_streams.csv").to_a
    revenue_streams_transaction_mappings = CSV.open("db/seed/csv/revenue_streams_transaction_mappings.csv").to_a

    revenue_streams.each do |rs|
      RevenueStream.find_or_create_by(
        id: rs[0],
        code: rs[1],
        code_info: rs[2]
      )
    end
    revenue_streams_transaction_mappings.each do |rstm|
      RevenueStreamsTransactionMapping.find_or_create_by(
        id: rstm[0],
        revenue_stream_id: rstm[1],
        person_transaction_type: rstm[2],
        balance_adjustment_category: rstm[3]
      )
    end
  end
end