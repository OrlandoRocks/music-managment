namespace :shrine_song_library_upload_migration do
  desc "One-time task to map the existing SongLibraryUpload#asset_key to the" \
       " data format used by Shrine::UploadedFile."

  task :migrate => :environment do
    puts "beginning data migration..."
    process_count = 0

    SongLibraryUpload
      .where.not(asset_key: nil)
      .in_batches(of: 100)
      .each_record do |song_library_upload|
        Shrine::Attacher.from_model(
          song_library_upload,
          :song_library_asset
        ).set(
          Shrine.uploaded_file(
            storage: :store,
            id: song_library_upload.asset_key,
            metadata: {'migrated_from_s3_direct_upload': 'true'}
          )
        )

      song_library_upload.save!
      process_count += 1

      puts "#{process_count} processed" if process_count % 100 == 0
    end

    puts "finished data migration. processed #{process_count} records."
  end
end
