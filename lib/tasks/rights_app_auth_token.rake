namespace :publishing_administration do
  task rights_app_auth_token: :environment do
    RightsApp::ApiClient.new.login
  end
end
