# frozen_string_literal: true

namespace :payoneer do
  task :batch_update_tax_forms, [:currency] => :environment do |_t, args|
    program_ids =
      PayoutProviderConfig.by_env.then do |relation|
        args[:currency].present? ? relation.where(currency: args[:currency]) : relation
      end.pluck(:program_id)

    Rails.logger.info("Starting updating of tax forms for #{program_ids.count} payout programs...")

    program_ids.each do |program_id|
      Payoneer::TaxFormUpdaterService.new(program_id: program_id).call
    end
  end
end
