namespace :payoneer do
  desc "Disassociate & disable all payout providers associated with deleted (or specified) accounts"

  # This task can be called with or without args. If you do not specify person_ids,
  # this worker runs for users with deleted TC accounts.

  # If you want to specify the users to disassociate, provide them as space-separated args, like so
  # bundle exec rake "payoneer:disassociate_payoneer_with_accounts[26 20]"

  task :disassociate_payoneer_with_accounts, [:person_ids] => [:environment] do |_, args|
    person_ids = args[:person_ids]&.split
    DisassociatePayoneerBackfill::WorkflowWorker.perform_async(person_ids)

    Rails.logger.info("🏈 Kicked off DisassociatePayoneerBackfill::WorkflowWorker ⚽️")
    Rails.logger.info("Check sidekiq to verify jobs are running")
  end
end
