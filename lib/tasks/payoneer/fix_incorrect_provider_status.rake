desc "Task to fix the payout status that was overwritten by the bug GS-2098"
namespace :payoneer do
  task fix_incorrect_provider_status: :environment do
    file_path = ENV["FILE"]
    unless file_path
      puts "FILE env variable is needed to run this rake task"
      next
    end

    incorrect_client_payee_ids = File.readlines(file_path).map(&:strip)
    payout_providers = PayoutProvider.where(client_payee_id: incorrect_client_payee_ids)
    payout_providers.update_all(provider_status: PayoutProvider::APPROVED)

    updated_client_payee_ids = payout_providers.pluck("client_payee_id")
    puts "Corrected #{payout_providers.size} payout provider statuses"
    puts updated_client_payee_ids.join(", ")
  end
end
