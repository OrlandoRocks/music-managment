namespace :payout_provider_configs do
  LOG_FORMATTERS = {
    data_file_not_found:   "File not Found %s",
    config_already_exists: "Config already exists for country_iso: %s with payout_provider_config_id: %s",
    country_not_found: "Country with iso code %s not found."
  }


  def data_file_present?(data_file_key)
    return true if data_file_key.present?
    print_logs(:error, LOG_FORMATTERS[:data_file_not_found], [data_file_key])
    false
  end

  def print_logs(log_type, log_formatter, formatter_data=[])
    log_string = formatter_data.present? ? (log_formatter % formatter_data) : log_formatter
    Rails.logger.send(log_type, log_string)
  end

  namespace :payoneer do
    desc "add payout provider configs from CSV. ENV['PAYONEER_CONFIGS_FILE']"
    task create_configs: :environment do
      # file format: currency, program_id, username, password, sandbox
      if data_file_present?(ENV["PAYONEER_CONFIGS_FILE"])
        payout_provider_configs = RakeDatafileService.csv(
          ENV["PAYONEER_CONFIGS_FILE"],
          { headers: :first_row, encoding: "UTF-8" }
        )

        payout_provider_configs.each do |provider_config|
          options = {
            name: "Payoneer",
            currency: provider_config["currency"],
            program_id: provider_config["program_id"],
            username: provider_config["username"],
            password: provider_config["password"],
            sandbox: Integer(provider_config["sandbox"], 10),
            corporate_entity_id: provider_config["corporate_entity_id"]
          }

          ppc = PayoutProviderConfig.find_by(options.except(:password))
          PayoutProviderConfig.create(options) unless ppc
        end
      end
    end

    task map_configs_to_corporate_entities: :environment do
      data_file_key = ENV.fetch('CONFIG_TO_CORPORATE_ENTITIES_MAPPING', nil)
      config_entity_mappings = RakeDatafileService.csv(data_file_key, {headers: :first_row, encoding: 'UTF-8'})

      config_entity_mappings.each do |config_entitiy_map|
        payout_provider_config = PayoutProviderConfig.find(config_entitiy_map['payout_provider_config_id'])
        corporate_entity = CorporateEntity.find(config_entitiy_map['corporate_entity_id'])

        payout_provider_config.update(corporate_entity: corporate_entity)
        Rails.logger.info "payout_provider_config_id: #{payout_provider_config.id}, corporate_entity_id updated to: #{corporate_entity.id}"
      end

      Rails.logger.info "Payout Provider Config to Corporate Entity mapping complete"
      PayoutProviderConfig.all.each do |config|
        corporate_entity_message = config.corporate_entity.present? ? config.corporate_entity.id : "NULL"
        Rails.logger.info "Payout Provider Config: #{config.id} - Corporate Entity: #{corporate_entity_message}"
      end
    end
  end
  namespace :paypal do
    desc "add payout provider configs from CSV. ENV['PAYPAL_CONFIGS_FILE']"
    task create_configs: :environment do
      # file format: currency, program_id, username, password, sandbox
      if data_file_present?(ENV['PAYPAL_CONFIGS_FILE'])
        payout_provider_configs = RakeDatafileService.csv(ENV['PAYPAL_CONFIGS_FILE'], {headers: :first_row, encoding: 'UTF-8'})

        payout_provider_configs.each do |provider_config|
          PayoutProviderConfig.find_or_create_by(
            name:       provider_config['name'],
            currency:   provider_config['currency'],
            program_id: provider_config['program_id'],
            username:   provider_config['username'],
            password:   provider_config['password'],
            sandbox:    provider_config['sandbox'].to_i,
            corporate_entity_id: provider_config['corporate_entity_id'],
            cert_file:  provider_config['cert_file'],
            key_file:   provider_config['key_file'],
          )
        end
      end
    end
  end

  task map_configs_to_corporate_entities: :environment do
    data_file_key = ENV.fetch('CONFIG_TO_CORPORATE_ENTITIES_MAPPING', nil)
    config_entity_mappings = RakeDatafileService.csv(data_file_key, {headers: :first_row, encoding: 'UTF-8'})

    config_entity_mappings.each do |config_entitiy_map|
      payout_provider_config = PayoutProviderConfig.find(config_entitiy_map['payout_provider_config_id'])
      corporate_entity = CorporateEntity.find(config_entitiy_map['corporate_entity_id'])

      payout_provider_config.update(corporate_entity: corporate_entity)
      Rails.logger.info "payout_provider_config_id: #{payout_provider_config.id}, corporate_entity_id updated to: #{corporate_entity.id}"
    end

    Rails.logger.info "Payout Provider Config to Corporate Entity mapping complete"
    PayoutProviderConfig.all.each do |config|
      corporate_entity_message = config.corporate_entity.present? ? config.corporate_entity.id : "NULL"
      Rails.logger.info "Payout Provider Config: #{config.id} - Corporate Entity: #{corporate_entity_message}"
    end
  end
end
