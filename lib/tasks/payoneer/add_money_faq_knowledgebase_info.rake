desc "Add knowledge base link for payoneer money transfer FAQ"
namespace :payoneer do
  task add_my_money_knowledgebase_link: :environment do
    unless KnowledgebaseLink.exists?(article_id: "115001899688")
      KnowledgebaseLink.create!(article_id: "115001899688", article_slug: "my-money", route_segment: "/sections/")
    end
  end
end