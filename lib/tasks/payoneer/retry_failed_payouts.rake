desc "Retry failed payouts due to Payoneer downtime"
namespace :payoneer do
  task retry_failed_payouts: :environment do
    payouts_to_retry = fetch_data_file
    unless payouts_to_retry
      puts "CSV argument is needed to run this rake task"
      next
    end

    print_retry_task_log(:info, "Beginning payout retry process")
    payouts_to_retry.each do |_date_time, _payee_id, _partner_id, _program_name, _api_function, _api_request, client_reference_id, _req_payee_id, _fxquote_id, _description, _group_id, _amount, _api_response|
        transfer = PayoutTransfer.find_by(client_reference_id: client_reference_id)
        person = transfer.person
        print_retry_task_log(:info, "Retrying payout for Person ID##{person.id} & Person Name #{person.name}")
        retry_payout(person, transfer)
        print_retry_task_log(:info, "Finished retrying payout for Person ID##{person.id} & Person Name #{person.name}")
    end

    print_retry_task_log(:info, "Retry process completed")
  end

  def retry_payout(person, transfer)
    payoneer_payout_client = Payoneer::PayoutApiClientShim.fetch_for(person).new(person: person)
    payoneer_payout_client.send_request("submit_payout", {person: person, transfer: transfer})
  end

  def fetch_data_file
    CSV.read(ARGV[1], headers: true) if ARGV[1].present? && File.exist?(ARGV[1])
  end

  def print_retry_task_log(log_type, log_message)
    puts "[RETRY FAILED PAYMENTS]  [#{log_type.upcase}] #{log_message}"
    Rails.logger.send(log_type, "[RETRY FAILED PAYMENTS] #{log_message}")
  end
end
