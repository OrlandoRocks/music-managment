desc "Load payoneer ACH fees information"
namespace :payoneer do

  task load_ach_fees: :environment do
    #will need to track down ach-fee-info.csv and put the path in manually on running this rake task
    # specs are set up to create their own ach fees, and makes rake db:load try to seed based on the file name
    # another ticket will be created to investigate this and include the csv and amend the specs if/where necessary
    file_path = ENV["CSV"]
    unless file_path
      puts "CSV env variable is needed to run this rake task"
      next
    end

    CSV.foreach(file_path) do |country_code, _country, target_currency_str, fee_str, threshold_str, source_currency_str|
      next if country_code == "Country Code" # For Header line

      currencies = target_currency_str.split("/")
      currencies.each_with_index do |currency, i|
         fee = get_value(fee_str, i).to_i
         threshold =  get_value(threshold_str, i).to_i
         source_currency = source_currency_str
         country_fee = PayoneerAchFee.find_or_create_by(target_country_code: country_code, target_currency: currency,
                                                         source_currency: source_currency)
         country_fee.update(transfer_fees: fee, minimum_threshold: threshold)
       end
    end

  end

  def get_value(values, index)
    values_array = values.split("/")
    values_array[index] || values_array.last
  end
end
