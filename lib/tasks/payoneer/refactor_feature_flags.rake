namespace :payoneer do
  desc "refactor feature flags to make use of the new country domain specific feature flags system & discard existing ones"
  task refactor_feature_flags: :environment do
    # task variables
    india_country_website     = CountryWebsite.find_by(id: CountryWebsite::INDIA)
    us_country_website        = CountryWebsite.find_by(id: CountryWebsite::UNITED_STATES)
    australia_country_website = CountryWebsite.find_by(id: CountryWebsite::AUSTRALIA)
    canada_country_website    = CountryWebsite.find_by(id: CountryWebsite::CANADA)

    #NOTE: payoneer mandatory
    $rollout.delete(:payoneer_mandatory)

    #NOTE: payoneer_login_redirect [currently at 0% rollout]
    # No changes required.

    #NOTE: payoneer_payout_usa [currently at 100% rollout]
    # Removing the feature as it's not used anywhere in the codebase
    $rollout.delete(:payoneer_payout_usa)
    FeatureFlipper.add_feature('payoneer_payout', us_country_website)
    FeatureFlipper.add_feature('payoneer_payout', india_country_website)

    FeatureFlipper.update_feature('payoneer_payout', 100, nil, india_country_website)
    FeatureFlipper.update_feature('payoneer_payout', 100, nil, us_country_website)

    #NOTE: payoneer_payout_australia_canada [currently at 0% rollout]
    # These values are hardcoded in Payoneer::FeatureService

    $rollout.delete(:payoneer_payout_australia_canada)

    FeatureFlipper.add_feature('payoneer_payout', australia_country_website)
    FeatureFlipper.add_feature('payoneer_payout', canada_country_website)

    #NOTE: payoneer_payout_other_countries [currently at 0%]
    # The payoneer payout other countries will be represented by payoneer_payout in the global features namespace

    $rollout.delete(:payoneer_payout_other_countries)
    FeatureFlipper.add_feature('payoneer_payout')

    #NOTE: payoneer_exclude_tax_check [currently at 0% rollout]
    # No changes

    #NOTE: payoneer_opt_out [currently at 0% rollout]
    # No changes

    #NOTE: payoneer_redesigned_onboarding [currently at 100% rollout]
    # Since australia & canada need to old flow, changing adding the features to australia & canada country domains.
    FeatureFlipper.add_feature('payoneer_redesigned_onboarding', australia_country_website)
    FeatureFlipper.add_feature('payoneer_redesigned_onboarding', canada_country_website)

    #NOTE: payoneer [currently at 0% rollout]
    # currently used nowhere in the code. hence removing it.
    $rollout.delete(:payoneer)

    # $rollout.delete(:payoneer_payout_usa_withdrawal_currency_dropdown)

  end
end
