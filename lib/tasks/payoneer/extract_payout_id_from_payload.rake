desc "Extract the payout_id from the raw_response and store it in a column"
namespace :payoneer do
  task extract_payout_id: :environment do
    payout_transfers = PayoutTransfer.where('raw_response is not null')
    payout_transfers.each do |payout_transfer|
      payout_id = JSON.parse(payout_transfer.raw_response)["payout_id"]
      payout_transfer.update(payout_id: payout_id)
    end
  end
end
