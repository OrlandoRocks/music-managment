desc "Remove user sensitive information from the tax details"
namespace :payoneer do
  task remove_sensitive_tax_info: :environment do
    TaxForm.where("payload is not null").each do |tax_form|
      payload = YAML.load(tax_form.payload)
      if payload["GetSubmittedTaxFormsForPayee"] && payload["GetSubmittedTaxFormsForPayee"]["TaxForms"]
        desensitized_payload = desensitize(payload, "GetSubmittedTaxFormsForPayee")
        tax_form.update(payload: desensitized_payload)
      end
    end
  end
end

def desensitize(payload, root_key)
  if payload[root_key] && payload[root_key]["TaxForms"]
    tax_forms = payload[root_key]["TaxForms"]["Form"]
    if tax_forms.is_a?(Array)
      tax_forms.each { |tax_form| clear_sensitive_info(tax_form) }
    else
      clear_sensitive_info(tax_forms)
    end
  end
  payload
end

def clear_sensitive_info(tax_form)
  tax_form["TaxPayerIdType"] = ""
  tax_form["TaxPayerIdNumber"] = ""
end

