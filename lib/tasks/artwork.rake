def convert_artwork(artwork)
  artwork_file = open(artwork.original.url)
  image_converter = Utilities::ImageConverter.new(artwork_file)
  artwork.assign_artwork image_converter.change_colorspace("RGB")
end

namespace :artwork do
  desc "Convert Artworks to RGB colorspace"
  task convert: :environment do
    from_date = ENV['CONVERT_FROM_DATE'] || Time.current.beginning_of_day.strftime('%Y-%m-%d')
    Transition
      .joins("left outer join distributions on transitions.state_machine_type = 'Distribution' and transitions.state_machine_id = distributions.id")
      .joins('left outer join petri_bundles on distributions.petri_bundle_id = petri_bundles.id')
      .joins('left outer join albums on petri_bundles.album_id = albums.id')
      .joins('left outer join artworks on artworks.album_id = albums.id')
      .where(old_state: 'packaged', new_state: 'error')
      .where('message like ?', '%CMYK%')
      .where('transitions.created_at > ?', from_date)
      .find_each do |transition|
        convert_artwork(transition.state_machine.album.artwork)
      end
  end
end
