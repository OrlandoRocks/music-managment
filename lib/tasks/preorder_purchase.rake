namespace :preorder_purchase do
  desc "Backfill purchases on preorders"
  task :backfill_purchases_related => :environment do |t|
    Rails.logger.info("Started preorder purchases backfill task")

    purchases = Purchase.includes(:person, related: :preorder_purchase).where(related_type: "SalepointPreorderData").where(paid_at: nil)
    products = Product.where("name = 'Preorder' AND status = 'Active'")
    purchases.each do |p|
      preorder_purchase = p.related.preorder_purchase
      product = products.detect{ |product| product.country_website_id == p.person.country_website_id}
      p.update(related_type: 'PreorderPurchase', related_id: preorder_purchase.id, product: product)
      p.recalculate
    end
  end

  desc "Remove existing, unfinalized Google PreOrder Purchases"
  task :remove_unfinalized_google_preorders => :environment do |t|
    #If it's a Google and iTunes preorder purchase, update it to just be iTunes and adjust the product and price.
    google_and_itunes_preorders = PreorderPurchase.joins(:album)
                                       .where(preorder_purchases: {
                                                    google_enabled: true,
                                                    itunes_enabled: true,
                                                    paid_at: nil
                                       },
                                              albums: {payment_applied: false}
                                       )

    google_and_itunes_preorders.each do |preorder|
      ActiveRecord::Base.transaction do
        preorder.update_enabled_states('itunes')
        preorder&.purchase&.recalculate
      end
    end

    # If it's a Google-only preorder, then just destroy the whole thing and purchase object.
    google_preorders = PreorderPurchase.joins(:album)
                                    .where(preorder_purchases: {
                                      google_enabled: true,
                                      itunes_enabled: false,
                                      paid_at: nil
                                    },
                                           albums: {payment_applied: false}
                                    )

    google_preorders.each do |preorder|
      ActiveRecord::Base.transaction do
        preorder&.purchase&.destroy!
        preorder.destroy!
      end
    end
  end
end
