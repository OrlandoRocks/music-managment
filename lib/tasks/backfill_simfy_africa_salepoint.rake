namespace :simfy_africa_salepoint do
  desc "backfill simfy africa salepoint and distribution to Ayoba"
  task backfill: :environment do
    Rails.logger.info "Starting backfill for simfy africa salepoints"

    records = ApplicationRecord.connection.exec_query(
      Salepoint.sanitize_sql_for_assignment(
        [
          Salepoint.get_missing_salepoint_query,
          {
            source_store_id: Store::SIMFY_AFRICA_STORE_ID,
            destination_store_id: Store::AYOBA_STORE_ID
          }
        ]
      )
    )

    salepoints_processed = 0
    salepointable_ids = records.rows&.flatten
    salepoints = Salepoint.where(id: salepointable_ids)
    store = Store.find(Store::AYOBA_STORE_ID)

    salepoints.find_each do |salepoint|
      salepoints_processed += 1
      # Create new salepoint for store id 64
      new_salepoint = salepoint.dup
      new_salepoint.store_id = Store::AYOBA_STORE_ID

      # Do not process if salepoint already exist
      next if new_salepoint.invalid? &&
              Salepoint.exists?(
                salepointable_id: new_salepoint.salepointable_id,
                store_id: Store::AYOBA_STORE_ID
              )

      new_salepoint.save
      p "New Salepoint added #{new_salepoint.id}"

      # Do not create distributions for 64 if no distribution exists for 31
      next if salepoint.distributions.blank?

      distro = Distribution.create!(
        state: "new",
        converter_class: store.converter_class,
        delivery_type: "full_delivery",
        petri_bundle_id: salepoint.distributions.first.petri_bundle_id
      )
      distro.salepoints << new_salepoint
    end

    total_salepoints = salepoints.count
    remaining_salepoints = total_salepoints - salepoints_processed
    Rails.logger.info "Salepoints Processed: #{salepoints_processed} Salepoints remaining: #{remaining_salepoints}"
    Rails.logger.info "Rake task completed"
  end
end
