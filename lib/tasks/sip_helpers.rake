namespace :sip do
  
  # sample usage: rake sip:files_for_year[2010] FILES=manifest.tsv,videos.txt,video_statements/ LOCAL=/Users/dandrabik/sales_intakes/video_recon
  desc 'pulls down SIP files from beats for the entire year to a local directory, broken down by year/month'
  task :files_for_year, :year do |t, args|
   
    files_to_pull = ENV['FILES'] ? ENV['FILES'].split(',')  : raise("Please specify the files to pull from server, e.g. rake sip:files_for_year[2010] FILES=manifest.tsv,videos.txt,video_statements/")
    local = ENV['LOCAL'] || raise("Please specify local directory to put files, e.g. rake sip:files_for_year[2010] LOCAL=/Users/dandrabik/sales_intakes/video_recon")
    local = local.slice(0..-2) if local[-1..-1] == "/" #removes trailing slash if present in directory input
    year = args[:year] || raise("need year to run")
    months= (1..12).to_a.map{|m| "%02d" % m }
    `mkdir #{local}/`
    `mkdir #{local}/#{year}/`
    months.each do |month|
      `mkdir #{local}/#{year}/#{month}/`
      files_to_pull.each do |file|
        `scp -r -P22022 tunecore@beats.tunecore.com:~/sales_intakes/#{year}/#{month}/#{file} #{local}/#{year}/#{month}/`
      end
    end
  end

  desc "Adds a comment to person transactions for a given id range - uses id range to be explicit"
  task :add_comment_to_person_intake_transactions => :environment do |t|
    raise "Must supply id_start"if ENV["INTAKE_ID_START"].blank?
    raise "Must supply id_end"  if ENV["INTAKE_ID_END"].blank?
    raise "Must supply comment" if ENV["COMMENT"].blank?

    dry_run = true
    dry_run = false if !ENV["DRY_RUN"].blank? && ENV["DRY_RUN"] == "0"

    id_start = ENV["INTAKE_ID_START"]
    id_end   = ENV["INTAKE_ID_END"]
    comment  = ENV["COMMENT"]

    log(t, "ID_START=#{id_start} and ID_END=#{id_end} and target_type=#{comment} DRY_RUN=#{dry_run}")
    transactions = PersonTransaction.where("target_id >= ? and target_id <= ? and target_type = 'PersonIntake'", id_start.to_i, id_end.to_i).all

    ids = transactions.collect(&:id)
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [%Q{UPDATE person_transactions SET comment = :comment WHERE ID in (:ids)}, {comment: comment, ids: ids}])

    if dry_run
      log(t, "#{transactions.size} PersonTransactions")
      log(t, "SQL that will be run:\n #{sql}")
    else 
      log(t, "#{transactions.size} PersonTransactions")
      log(t, "Running SQL:\n #{sql}")
      ActiveRecord::Base.connection.execute(sql)
    end

    log(t, "Complete")
  end
end

