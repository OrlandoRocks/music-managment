namespace :recharge_mistakenly_refunded_invoices do
  desc "On Aug 23 & Aug 24, 2021 many invoices were incorrectly refunded. Let's recharge them."

  task :run_paypal, [:id] => [:environment] do |_, args|
    extend PayPalSDKUtils

    Rails.logger.info "🧾 Running Paypal recharge rake task 🧾 "
    csv_filename = "mistakenly_refunded_paypal_transactions.csv"
    errored_txn_ids = []
    recharged_txn_ids = []

    refunded_txn_ids = find_txn_ids(args[:id], csv_filename)

    refunded_txn_ids.each do |txn_id|
      txn = PaypalTransaction.find(txn_id)
      next if txn.refund? || txn.invoice.paypal_transactions.where( action: "Sale", status: "Completed").length > 2

      new_txn = process_paypal_payment(txn)

      if new_txn.success?
        Rails.logger.info "🧾 Recharged txn id: #{txn_id} 🧾"
        recharged_txn_ids << txn_id
      else
        Rails.logger.info "❌ ERROR for txn id #{txn_id} ❌"
        errored_txn_ids << txn_id
      end
      # TESTING
      Rails.logger.info "ENSURE EQUALITY: #{txn.amount}, #{new_txn.amount}"
    end

    finale_print("Paypal", "transactions", recharged_txn_ids, errored_txn_ids)
  end

  task :run_braintree, [:id] => [:environment] do |_, args|
    Rails.logger.info "💳 Running Braintree recharge rake task 💳 "
    csv_filename = "mistakenly_refunded_braintree_invoices.csv"
    recharged_invoice_ids = []
    errored_invoice_ids = []

    refunded_invoice_ids = find_invoice_ids(args[:id], csv_filename)

    refunded_invoice_ids.each do |invoice_id|
      invoice = Invoice.find(invoice_id)

      # maintain idempotency & don't recharge users manually refunded by customer care
      next if invoice.braintree_transactions.length != 1

      braintree_txn = invoice.braintree_transactions.first
      stored_cc = braintree_txn.stored_credit_card

      amount = braintree_txn.amount * 100  # we only want to re-charge Braintree txn amount, not full invoice amount
      ip_address = braintree_txn.ip_address

      new_txn = BraintreeTransaction.process_purchase_with_stored_card(
        invoice,
        stored_cc,
        {
          ip_address: ip_address,
          payment_amount: amount
        }
      )

      if successful_txn?(new_txn)
        Rails.logger.info "💳 Recharged invoice id: #{invoice_id} 💳"
        recharged_invoice_ids << invoice_id
      else
        Rails.logger.info "❌  Errored invoice id: #{invoice_id} ❌"
        errored_invoice_ids << invoice_id
      end
    end

    finale_print("Braintree", "invoices", recharged_invoice_ids, errored_invoice_ids)
  end

  def finale_print(payment_type, obj_type, recharged_invoice_ids, errored_invoice_ids)
    Rails.logger.info "💳 Finished #{payment_type} recharge rake task 💳 \n
                       💳 Recharged #{obj_type} count: #{recharged_invoice_ids.length} 💳 \n
                       💳 Errored #{obj_type} count: #{errored_invoice_ids.length} 💳"
    Rails.logger.info "Recharged #{payment_type} #{obj_type}: #{recharged_invoice_ids}"
    Rails.logger.info "Failed #{payment_type} #{obj_type}: #{errored_invoice_ids}"
  end

  def items_for_paypal(invoice)
    invoice.purchases.select(&:cents_greater_than_zero?).map(&:item_for_paypal)
  end

  def process_paypal_payment(txn)
    invoice = txn.invoice
    stored_paypal_account = txn.referenced_paypal_account
    country_website = stored_paypal_account.country_website
    payin_config = stored_paypal_account.payin_provider_config

    transaction = PayPalAPI.reference_transaction(payin_config, stored_paypal_account.billing_agreement_id, invoice.currency, items_for_paypal(invoice))

    paypal_transaction = PaypalTransaction.create!(
      invoice: invoice,
      country_website: country_website,
      person_id: invoice.person_id,
      email: stored_paypal_account.email,
      action: "Sale",
      referenced_paypal_account: stored_paypal_account,
      ack: transaction.response["ACK"].to_s,
      status: transaction.response["PAYMENTSTATUS"].to_s,
      amount: transaction.response["AMT"].to_s,
      billing_agreement_id: transaction.response["BILLINGAGREEMENTID"].to_s,
      transaction_id: transaction.response["TRANSACTIONID"].to_s,
      transaction_type: transaction.response["TRANSACTIONTYPE"].to_s,
      fee: transaction.response["FEEAMT"].to_s,
      pending_reason: transaction.response["PENDINGREASON"].to_s,
      error_code: transaction.response["REASONCODE"].to_s,
      currency: transaction.response["CURRENCYCODE"].to_s,
      raw_response: hash2cgiString(transaction.response),
      payin_provider_config: payin_config
    )
  end

  def find_txn_ids(rake_ids, csv_filename)
    rake_ids.present? ? [rake_ids]
                      : RakeDatafileService.csv(csv_filename)
                                          .entries
                                          .map{|row| row.fetch("id", "")}
  end


  def find_invoice_ids(rake_ids, csv_filename)
    rake_ids.present? ? [rake_ids]
                      : RakeDatafileService.csv(csv_filename)
                                          .entries
                                          .map{|row| row.fetch("invoice_id", "")}
  end

  def successful_txn?(txn)
    txn.response_code == "1000" && !txn.error?
  end
end
