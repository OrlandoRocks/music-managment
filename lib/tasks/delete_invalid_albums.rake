namespace :albums do
  desc "destroy invalid albums"
  task delete_invalid: :environment do
    albums = Album.where("person_id = -1 or person_id IS NULL")
    songs = Song.where(album_id: albums.map(&:id))
    Song.where(album_id: albums.map(&:id)).delete_all
    Salepoint.where(salepointable_id: albums.map(&:id)).delete_all
    petri_bundles = PetriBundle.where(album_id: albums.map(&:id))
    Distribution.where(id: petri_bundles.map(&:id)).delete_all
    ReviewAudit.where(album_id: albums.map(&:id)).delete_all
    petri_bundles.delete_all
    albums.destroy_all
  end
end
