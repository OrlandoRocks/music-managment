namespace :reports do
  desc "populate rollup tables for reporting dashboard"

  task generate_report_data: :environment do
    if ENV["YESTERDAY"] == "Y"
      start_date  = Date.yesterday
      end_date    = Date.yesterday
    else
      start_date  = ENV.key?("START_DATE") ? Date.parse(ENV["START_DATE"], "%Y-%m-%d") : Date.today
      end_date    = ENV.key?("END_DATE")   ? Date.parse(ENV["END_DATE"],   "%Y-%m-%d") : Date.today
    end

    if ENV["ANALYTICS"] == "Y"
      invoices = Invoice.analytics_by_date(start_date, end_date)
      invoices.each { |invoice| PersonAnalytic.update_analytics_for_invoice(invoice.id) }
    end

    (start_date..end_date).each do |for_date|
      ReportingDashboardRollup.calculate_daily_values(for_date)
      ReportingDashboardProduct.calculate_daily_values(for_date)
    end
  end
end
