require "#{Rails.root}/lib/trends_database"
require "#{Rails.root}/db/seed/seed"

namespace :db do
  namespace :structure do
    task :dump do
      path = Rails.root.join('db', 'structure.sql')
      # Remove autoincrement to remove diff noise
      File.write path, File.read(path).gsub(/ AUTO_INCREMENT=\d*/, '')
    end
  end

  task :reset do
    Rake::Task["db:drop"].invoke
    Rake::Task["db:create"].invoke
    Rake::Task["db:load"].invoke
  end

  task load: :environment do
    exit(true) if Rails.env.production? || Rails.env.staging?

    if Rails.env.development?
      Rake::Task["dev:setup"].invoke
    else
      Rake::Task["db:structure:load"].invoke
    end

    Rake::Task["db:migrate"].invoke

    if Rails.env.test?
      ENV["FIXTURES_PATH"] = "spec/fixtures"
      Rake::Task["db:fixtures:load"].invoke
    end

    Rake::Task["db:seed"].invoke
    Rake::Task["db:load_trends"].invoke
    Rake::Task["seed:payout_provider_configs"].invoke
    Rake::Task["transient:add_country_audit_sources"].invoke
  end

  task :load_trends => :environment do
    exit(true) if Rails.env.production? || Rails.env.staging?

    TrendsDatabase.create_trends_tables
  end

  task :dump_seed => :environment do
    Seed.dump_all
  end

  GUARDED_STAGING_TASKS = [
    "db:drop",
    "db:create",
    "db:migrate",
    "db:rollback",
    "db:load",
    "db:structure:load"
  ].freeze

  task :guard_staging_db do
    host = ActiveRecord::Base.configurations[Rails.env]["host"]
    if Rails.env.development? && !%w(db localhost).include?(host)
      unless ENV["FORCE"] == "true"
        puts "You are connected to the Staging DB. Are you sure you want to run this command?"
        puts "Run command with FORCE=true to bypass this warning."
        exit
      end
    end
  end

  task :guard do
    name = ActiveRecord::Base.configurations[Rails.env]["database"]
    if %w(tunecore_production tunecore_staging).include?(name)
      raise "Cannot run this task on a production/staging db"
    end
  end

  %w(db:drop db:create db:structure:load).each do |task|
    Rake::Task[task].enhance ["db:guard"]
  end

  GUARDED_STAGING_TASKS.each do |task|
    Rake::Task[task].enhance ["db:guard_staging_db"]
  end
end
