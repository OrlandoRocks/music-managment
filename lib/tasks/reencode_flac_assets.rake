namespace :reencode_flac_assets do
  task :run, [:album_id] => [:environment] do |command, args|
    Rails.logger.info "Beginning rake task..."  
    raise "Please provide album id" if args[:album_id].nil?

    album_id = args[:album_id]
    album = Album.find(album_id)  
    
    album.songs.each do |song|
      next if (song&.s3_flac_asset.nil?) || (!song.s3_flac_asset.key.ends_with?(".flac"))
      s3_flac_asset = song.s3_flac_asset
      song.update(duration_in_seconds: nil, s3_flac_asset_id: nil)
      s3_flac_asset.destroy
    end
    
    AssetsBackfill::Base.new(album_ids: [album_id]).run
    Rails.logger.info "Rake task completed!"
  end
end