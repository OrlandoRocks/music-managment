require "open-uri"

module Publishing
  class DataUpdate
    def self.backfill_lod(publishing_composer, rs_document)
      return if publishing_composer.lod #prevent accidentally overwriting existing lod

      publishing_composer = PublishingComposer.find(publishing_composer.id)
      publishing_composer.create_lod(:last_status => 'imported',
        :document_guid => rs_document['guid'], :template_guid => Lod::TEMPLATE_ID,
        :raw_response => rs_document)
      publishing_composer.save(:validate=>false) #to get pass the stupid validation
      publishing_composer.lod.mark_as_sent_to_customer
      publishing_composer.lod.update_attribute(:last_status_at, rs_document['created_at'])
      if rs_document['state'] == 'signed'
        publishing_composer.lod.mark_as_signed_by_customer
        publishing_composer.lod.update_attribute(:last_status_at, rs_document['completed_at'])
      end

    end
  end
end

namespace :new_publishing do
  desc "Set publishing split values for a csv of composition IDs to a target percent"
  task :update_splits_from_csv => :environment do
    filename = ENV["COMPOSITION_CSV"] || raise("Missing variable COMPOSITION_CSV")
    target_percent = ENV["TARGET_PERCENT"].to_i || raise("Missing variable TARGET_PERCENT")
    Rails.logger.info "Setting publishing splits for compositions in #{filename} to #{target_percent} percent"
    puts "Setting publishing splits for compositions in #{filename} to #{target_percent} percent"

    CSV.foreach(filename) do |line|
      begin
        publishing_composition_id = line[0].to_i
        psa = PublishingCompositionSplit.where(:publishing_publishing_composition_id => publishing_composition_id)
        if psa.nil?
          Rails.logger.info "No publishing split found for composition id #{publishing_composition_id}"
          puts "No publishing split found for composition id #{publishing_composition_id}"
        else
          psa.each do |ps|
            ps.percent = target_percent
            ps.save
            Rails.logger.info "Publishing split #{ps.id} set to #{target_percent}"
            puts "Publishing split #{ps.id} set to #{target_percent}"
          end
        end
      rescue StandardError => e
        Rails.logger.info e
      end
    end
  end

  desc "Resync NTC compositions that were wrongly put in 'pending_distribution' state"
  task resync_pending_distro_compositions: :environment do
    def log_sync_error(publishing_composition, error)
      Rails.logger.error("*****\nProblem processing publishing_composition #{publishing_composition.id}\n*****\n")
      Rails.logger.error(error)
    end

    publishing_compositions = PublishingComposition.joins(:non_tunecore_songs).where(publishing_compositions: {state: 'pending_distribution'})

    Rails.logger.info("Processing #{publishing_compositions.count} publishing_compositions")

    result_count = 0

    Parallel.each(publishing_compositions, in_threads: 10) do |composition|
      ActiveRecord::Base.connection_pool.with_connection do
        work_exists = true

        if publishing_composition.provider_identifier
          #Update Works that already exist.
          begin
            #If the next line doesn't find produce a work, it throws an exception, so it won't make it to WorkListService.list line,
            # and will proceed to create the work.
            response = PublishingAdministration::ApiClientServices::GetPublishingWorkService.get_work(publishing_composition.provider_identifier)
            if response && response["Error"]
              log_sync_error(publishing_composition, response["Error"])
              next
            else
              PublishingAdministration::WorkListService.list(publishing_composition.account, true)
            end
          rescue PublishingAdministration::ErrorService::ApiError => e
            if e.message.include?("No work found")
              work_exists = false
            else
              log_sync_error(publishing_composition, e.message)
              next
            end
          rescue => e
            log_sync_error(publishing_composition, e.message)
            next
          end

          next if work_exists
        end

        #Create works that don't exist.
        begin
          PublishingAdministration::ApiClientServices::PublishingWorkCreationService.post_work({publishing_composition: publishing_composition, publishing_composer: publishing_composition.publishing_composer})
        rescue => e
          log_sync_error(publishing_composition, e.message)
        end
      end

      if (result_count += 1) % 500 == 0
        Rails.logger.info("*****\nProcessed #{result_count} publishing_compositions.\n*****\n")
      end
    end

    Rails.logger.info("Done syncing #{result_count} publishing_compositions.")
  end

  task :extract_royalty_statement_files, [:year, :quarter] => :environment do |t, args|

    #Check if the directory exists
    if !File.directory? ROYALTY_PDF_DIRECTORY
      Dir.mkdir(ROYALTY_PDF_DIRECTORY)
    end

    # get date for path for csv's
    time = Time.current
    year = args[:year] ? args[:year] : time.year
    month = time.month
    quarter = args[:quarter] ? args[:quarter] : (month.to_f / 3).ceil - 1

    if quarter == 0
      year -= 1
      quarter = 4
    end

    if !File.directory? "#{ROYALTY_PDF_DIRECTORY}#{year}"
      Dir.mkdir("#{ROYALTY_PDF_DIRECTORY}#{year}")
    end

    if !File.directory? "#{ROYALTY_PDF_DIRECTORY}#{year}/Q#{quarter}"
      Dir.mkdir("#{ROYALTY_PDF_DIRECTORY}#{year}/Q#{quarter}")
    end

    Rails.logger.info "Downloading zip file"
    client = Aws::S3::Client.new
    s3 = Aws::S3::Resource.new
    folder = 'path/to/the/folder'
    objects = s3.bucket("ftp.tunecore.com").objects({prefix: "tcxfr/royalty_temp_#{quarter}Q#{year}"})
    objects.batch_delete!
    resp = client.get_object({ bucket:"ftp.tunecore.com", key:"tcxfr/#{quarter}Q#{year} Royalties/Statements.zip" }, target: "tmp/Statements.zip")

    Rails.logger.info "Unzipping zip file"
    Zip::File.open("tmp/Statements.zip") do |zip_file|
      zip_file.each do |f|
        f_path = File.join("#{ROYALTY_PDF_DIRECTORY}#{year}/Q#{quarter}", f.name)
        FileUtils.mkdir_p(File.dirname(f_path))
        zip_file.extract(f, f_path) unless File.exist?(f_path)
      end
    end

    Dir.chdir(ROYALTY_PDF_DIRECTORY)
    pdf_files = File.join("**", "*.pdf")
    pdf_files = Dir.glob(pdf_files)
    csv_files = File.join("**", "*.csv")
    csv_files = Dir.glob(csv_files)
    pdf_hash = build_file_hash(pdf_files)
    csv_hash = build_file_hash(csv_files)

    Rails.logger.info "Created File Hashes"

    #Check that all pdfs have a matching csv
    unmatched_pdfs, unmatched_csvs = check_for_unmatched_files(pdf_hash, csv_hash)

    Rails.logger.info "Checked File Hashes for missing files"
    Rails.logger.info "Uploading unzipped files to temporary S3 folder"
    Rails.logger.info "Uploading PDFs"
    upload_unzipped_files(pdf_hash, client, 'application/pdf', quarter, year)
    Rails.logger.info "Uploading CSVs"
    upload_unzipped_files(csv_hash, client, 'text/csv', quarter, year)
  end

  task :attach_royalty_statement_files, [:year, :quarter] => :environment do |t, args|
    time = Time.current
    year = args[:year] ? args[:year] : time.year
    month = time.month
    quarter = args[:quarter] ? args[:quarter] : (month.to_f / 3).ceil - 1

    if quarter == 0
      year -= 1
      quarter = 4
    end

    attributes = {:period_type => "quarterly", :period_interval => quarter, :period_year => year}
    royalty_payments = RoyaltyPayment.where(attributes).where(pdf_summary_file_name: nil).or(RoyaltyPayment.where(attributes).where(csv_summary_file_name: nil))

    royalty_payments.each do |royalty_payment|
      PublishingAdministration::RoyaltyStatementUploadWorker.perform_async(royalty_payment.id, quarter, year)
    end
  end

  def build_file_hash(file_array)
    Rails.logger.info "Creating file hashes"
    hash = {}
    file_array.each_with_index do |path, i|
      parts = path.split("/")
      if parts.size == 3 &&
         parts[0].match("^[1-9][0-9][0-9][0-9]$") != nil &&
         parts[1].match("^Q[1-4]$") != nil

        #Pull out parts
        year      = parts[0]
        quarter   = parts[1][-1,1]
        file_name = parts[2]

        #Pull the person id from the file name
        person_id_format = file_name.match("([0-9]*)(?:[_]([A-Z0-9]*))?")

        if person_id_format
          person_id = person_id_format[1].to_s
          code = person_id_format[2]
          hash[person_id] = {
            file_path: path,
            file_name: file_name,
            code: code
          }
        else
          Rails.logger.info "File name format is invalid for #{file_name}"
          puts "File name format is invalid for #{file_name}"
          next
        end
      else
        #Log that the file structure is incorrect
        Rails.logger.info "Directory structure is incorrect.  File must exist in folder YYYY/Q(1-4)/person_id.pdf or YYYY/Q(1-4)/person_id.csv"
        puts "Directory structure is incorrect.  File must exist in folder YYYY/Q(1-4)/person_id.pdf or YYYY/Q(1-4) /person_id.csv"
      end
    end

    return hash
  end

  def upload_unzipped_files(hash, client, content_type, quarter, year)
    hash.each_with_index do |(person_id, values), index|
      print "--- Uploading file [#{index}/#{hash.size}] --- \r"
      resp = client.put_object({
        body: File.open(values[:file_path]),
        bucket: "ftp.tunecore.com",
        key: "tcxfr/royalty_temp_#{quarter}Q#{year}/#{values[:file_name]}",
        metadata: {
          "content-type" => content_type
        },
      })
    end
  end

  def check_for_unmatched_files(pdf_hash, csv_hash)
    unmatched_pdfs = []
    pdf_hash.each do |person_id, values|
      unless csv_hash[person_id]
        unmatched_pdfs << person_id
        Rails.logger.info "CSV file missing for user #{person_id}"
        puts "CSV file missing for user #{person_id}"
      end
    end

    unmatched_csvs = []
    csv_hash.each do |person_id, values|
      unless pdf_hash[person_id]
        unmatched_csvs << person_id
        Rails.logger.info "PDF file missing for user #{person_id}"
        puts "PDF file missing for user #{person_id}"
      end
    end

    return unmatched_pdfs, unmatched_csvs
  end

  namespace :reports do
    desc "Generates composer summary in excel"
    task :generate_composer_summary => :environment do
      log("publishing:generate_composer_summary","Start generating summary")
      filename = PublishingComposer.generate_summary_xls
      log("publishing:generate_composer_summary", "Completed summary filename=#{filename}")
    end

  end

  namespace :data_updates do
    desc "Backfill 'unallocated sums' compositions"
    task :backfill_unallocated_sum_publishing_compositions => :environment do
      publishing_composers = PublishingComposer.with_no_unallocated_sum_composition
      puts "Creating unallocated sum composition for #{publishing_composers.count} publishing_composers"
      publishing_composers.each do |publishing_composer|
        puts "Creating for #{publishing_composer.email}"
        publishing_composer.send :create_unallocated_sum_publishing_composition
      end

      puts "Succesfully updated all publishing_composers" if PublishingComposer.with_no_unallocated_sum_composition.count == 0
    end

    desc "Rename 'unallocated sums' publishing_compositions to the new format"
    task :rename_unallocated_sum_publishing_compositions => :environment do
      # rename previously created unallocated sums composition based on the new requirement
      update_publishing_compositions = PublishingComposition.joins(:publishing_composition_splits => :publishing_composer).where(:is_unallocated_sum => true).includes(:publishing_composition_splits => :publishing_composer)

      puts "Updating #{update_publishing_compositions.count} unallocated sum composition's name"
      update_publishing_compositions.each do |uc|
        new_name = uc.publishing_composition_splits.first.publishing_composer.send(:unallocated_sums_composition_name)
        puts "Updating publishing_composition from #{uc.name} to #{new_name}"
        PublishingComposition.update(uc.id, :name => new_name)
      end
    end

    desc "Update royalty hold payment type"
    task :update_hold_payment_type => :environment do
      rp = RoyaltyPayment.joins("inner join balance_adjustments on posting_type = 'BalanceAdjustment' and posting_id = balance_adjustments.id").where("balance_adjustments.credit_amount = 0").readonly(false)

      rp.each do |r|
        r.update(:hold_payment => true, :hold_type => 'missing_tax')
      end

      updated_rp_count = RoyaltyPayment.where(hold_payment: true, hold_type: 'missing_tax').count

      puts "Successfully updated #{updated_rp_count} records" if rp.size == updated_rp_count
    end

  end
end
