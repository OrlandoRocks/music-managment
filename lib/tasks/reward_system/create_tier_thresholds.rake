namespace :reward_system do
  task create_tier_thresholds: :environment do
    csv = RakeDatafileService.csv('thresholds.csv')

    csv.entries.each do |threshold|
      next if TierThreshold.exists?(tier_id: threshold['tier_id'], country_id: threshold['country_id'])

      TierThreshold.create!({
        tier_id: threshold['tier_id'],
        lte_min_amount: threshold['lte_min_amount'],
        min_release: threshold['min_release'],
        country_id: threshold['country_id']
      })

      puts "Created threshold for Tier: #{threshold['tier_id']} Country: #{threshold['country_id']}"
    end
  end
end
