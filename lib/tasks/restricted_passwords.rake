namespace :restricted_passwords do
  desc "Load restricted passwords into Redis for password validation.
        List is of 1 million most commonly used passwords."
  task :load => :environment do
    require "csv" unless defined?(CSV)
    restricted_passwords_csv = File.join(Rails.root, "lib", "restricted_passwords.csv")

    CSV.foreach(restricted_passwords_csv) do |row|
      row.each do |pwd|
        $redis.sadd("restricted_passwords", pwd)
      end
    end
  end
end
