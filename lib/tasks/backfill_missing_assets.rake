namespace :backfill_missing_assets do
  desc "backfill missing assets for album whose song doesn't have s3_asset"

  task :run, [:song_ids] => [:environment] do |_, args|
    BUCKET = "bigbox.tunecore.com".freeze

    song_ids = args.song_ids&.split(" ")
    puts "Rake task started for backfilling songs: #{song_ids}"

    if song_ids.blank?
      puts "Please specify the song ids"
      return
    end

    song_ids.each do |song_id|
      song = Song.find(song_id)
      person_id = song.album.person_id
      upload_name = song.upload.uploaded_filename
      path = "#{person_id}/#{upload_name}"
      obj = Aws::S3::Object.new(BUCKET, path)

      next unless obj.exists?

      s3_asset = S3Asset.where(key: path, bucket: BUCKET).first_or_create
      song.s3_orig_asset_id = s3_asset.id
      song.s3_asset_id = s3_asset.id
      song.save
    end

    # backfilling album
    album_ids = Song.where(id: song_ids).pluck("album_id")
    AssetsBackfill::Base.new(album_ids: album_ids).run
  end
end
