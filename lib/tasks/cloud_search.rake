namespace :cloud_search do

  desc "Does an incremental cloud_search batch upload"
  task :batch_upload => :environment do
    last_add_date        = nil
    last_delete_date     = nil
    document_endpoint    = nil
    trend_end_date       = nil

    last_add_date        = ENV["LAST_ADD_DATE"]
    last_delete_date     = ENV["LAST_DELETE_DATE"]
    document_endpoint    = ENV["DOCUMENT_ENDPOINT"]
    trend_end_date       = ENV["TREND_END_DATE"]

    last_add_date    = DateTime.parse(last_add_date)    if last_add_date
    last_delete_date = DateTime.parse(last_delete_date) if last_delete_date

    puts Rails.logger.info("Cloud Search batch_upload: LAST_ADD_DATE=#{last_add_date.to_s if last_add_date} LAST_DELETE_DATE=#{last_delete_date.to_s if last_delete_date}")

    batch = CloudSearch::Batch.new(:last_add_date=>last_add_date, :last_delete_date=>last_delete_date, :document_endpoint=>document_endpoint, :trend_end_date=>trend_end_date)
    batch.do_full_batch
  end

  desc "Does a full cloudsearch upload - used to force update popularity on all songs"
  task :full_upload => :environment do
    document_endpoint    = nil
    trend_end_date       = nil

    document_endpoint    = ENV["DOCUMENT_ENDPOINT"]
    trend_end_date       = ENV["TREND_END_DATE"]

    puts Rails.logger.info("Cloud Search full_upload")

    batch = CloudSearch::Batch.new(:force_add=>true, :document_endpoint=>document_endpoint, :trend_end_date=>trend_end_date)
    batch.do_full_batch
  end

  desc "Does a force cloud_search batch delete - force deletes all songs"
  task :force_batch_delete => :environment do 
    document_endpoint = nil
    document_endpoint = ENV["DOCUMENT_ENDPOINT"]

    puts Rails.logger.info("Cloud search force_batch_delete")
    batch = CloudSearch::Batch.new(:force_delete=>true, :document_endpoint=>document_endpoint)

    batch.do_delete
  end

  desc "Delete entered song_ids from cloudsearch"
  task :delete_songs => :environment do
    raise "Need to supply SONG_IDS" if ENV["SONG_IDS"].blank?

    document_endpoint = nil
    song_ids          = []
    document_endpoint = ENV["DOCUMENT_ENDPOINT"]

    song_ids          = ENV["SONG_IDS"]
    song_ids          = song_ids.split(",")

    batch = CloudSearch::Batch.new(:document_endpoint=>document_endpoint)
    batch.delete_songs(song_ids)
  end

end
