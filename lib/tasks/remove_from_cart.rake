namespace :cart do
  desc "Remove all instances of an unpaid store salepoint from customer carts"
  task remove_store: :environment do | t |
    unless ENV['store_id']
      abort "Please set the 'store_id' environment variable: 'bundle exec rake cart:remove_store store_id=<store ID> [limit=<number of records to remove>]"
    end

    query = "select p.* from purchases p inner join salepoints s on s.id = p.related_id where p.paid_at is null and s.store_id = #{ENV['store_id']} and p.related_type = 'Salepoint'"

    if ENV['limit']
      query += " limit #{ENV['limit']}"
    end

    unpurchased_items = Purchase.find_by_sql query

    unpurchased_items.each do | item |
      item.destroy

      if ENV['limit']
        puts "Purchase ID: #{item.id}: Salepoint ID: #{item.related_id} deleted"
      end
    end
  end
end
