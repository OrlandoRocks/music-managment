#
# Defines tasks for sending reminder emails.
#
namespace :renewals do

  desc "send out reminder emails"
  task :send_reminder_emails => :environment do |task, args|

    #Hash in the form :email_id => {: renewal_interval=>xxx,:sent_count=>xxx,:expiration_date, "subject"=>{counts}}
    email_status = {}

    #28 days prior reminders for yearly
    email_status.merge! send_reminder_emails(
      Date.today + 28.days,
      renewal_interval: :yearly,
      subject: "models.batch_notifier.renewal_alert"
      )

    #2 Day prior reminders for yearly
    expiration = Date.today + 2.days
    email_status.merge! send_reminder_emails(
      expiration,
      renewal_interval: :yearly,
      subject: "models.batch_notifier.renewal_reminder",
      exp_date:  " #{(expiration).strftime("%b %-d, %Y")}",
      only_cannot_renew: true
      )

    #7 Days after reminders for yearly
    expiration = Date.today - 7.days
    email_status.merge! send_reminder_emails(
      expiration,
      renewal_interval: :yearly,
      subject:  "models.batch_notifier.music_taken_down",
      exp_date: " #{(expiration + Renewal::GRACE_DAYS_BEFORE_TAKEDOWN.days).strftime('%b %-d, %Y')}"
      )

    #35 days after renewal reminders for yearly
    expiration = Date.today - 35.days
    email_status.merge! send_reminder_emails(
      expiration,
      renewal_interval: :yearly,
      subject: "models.batch_notifier.final_notice",
      exp_date: " #{(expiration + Renewal::GRACE_DAYS_BEFORE_TAKEDOWN.days).strftime('%b %-d, %Y')}"
      )

    BatchSummaryNotifier.reminders_summary( email_status ).deliver

  end

end

#
# reminder email
#
# Returns: {email_id=>{:sent_count=>total_count, :expiration_date=>expiration_date, :renewal_interval=>options[:renewal_interval], "subject"=>{counts}})}
#
def send_reminder_emails( expiration_date, options={} )
  options[:renewal_interval]  ||= :yearly
  options[:only_cannot_renew] ||= false
  ret_val                      = {}
  total_count                  = 0

  #assign a random string so you can pull only from this send in the logs
  email_id   = rand(36**10).to_s(36)

  start_time = Time.now

  log("Payment Batch","Start send_reminder_emails", :time=>start_time, :email_id=>email_id)

  #Get the people that we need to notify. People that have renewals that expire ON the expiration date
  #Get all the renewals expiring ON the expiration date

  if options[:renewal_interval] == :both
    people_to_notify = Person.with_renewals_in_period_old(expiration_date.to_s, expiration_date.to_s)
    renewals_for_all = Renewal.expires_within_dates(expiration_date, expiration_date).group_by(&:person_id)
  else
    people_to_notify = Person.with_renewals_for_day(options[:renewal_interval]==:monthly, expiration_date )
    renewals_for_all = Renewal.expires_on_date(options[:renewal_interval] == :monthly, expiration_date).group_by(&:person_id)
  end

  people_to_notify.each do |person|
    #Skip this person if we're looking for only people that can't auto renew and this person can auto renew by some means other than tc balance
    next if options[:only_cannot_renew] == true and (person.autorenew? && ( person.preferred_credit_card? || person.preferred_paypal_account? ))

    #Get the renewals for this person
    renewals = renewals_for_all.values_at(person.id).flatten
    renewals.select! { |renewal| renewal.plan_related? } if person.has_plan?
    next if person.has_plan? && renewals.blank?

    if renewals.first.present?

      locale            = COUNTRY_LOCALE_MAP[person.country_domain].to_sym
      expiration_string = options.fetch(:exp_date, "")

      if options[:subject]
        subject = I18n.t(options[:subject], locale: locale) + expiration_string
      end

      #Add person conditional subjects here
      if person.preferred_credit_card_expired?
        subject = I18n.t("models.batch_notifier.renewal_due", locale: locale)
      end

      #Catch new cases where subject is not defined
      raise "Subject not defined - define subject above or pass in as an option if it is not conditional based on the person" if subject.blank?

      #Deliver the reminder
      BatchNotifier.renewal_reminder( person, subject, renewals, expiration_date ).deliver

      #Initialize counts if this is the first time we're using this subject
      ret_val[subject] = { :sent_count                  => 0,
                          :no_preferences_count        => 0,
                          :do_not_auto_renew_count     => 0,
                          :no_paypal_count             => 0,
                          :no_credit_card_count        => 0,
                          :expired_card_count          => 0,
                          :no_preferred_card_or_paypal => 0 } if ret_val[subject].blank?

      # get reason why the person is getting a notice for logging purposes
      if person.person_preference
        if !person.autorenew?
          reason_text = "Set Do Not AutoRenew"
          ret_val[subject][:do_not_auto_renew_count] = ret_val[subject][:do_not_auto_renew_count] + 1
        elsif person.person_preference.preferred_payment_type == 'PayPal' && person.person_preference.preferred_paypal_account_id.nil?
          reason_text = "No Paypal Account"
          ret_val[subject][:no_paypal_count] = ret_val[subject][:no_paypal_count] + 1
        elsif person.person_preference.preferred_payment_type == 'CreditCard' && person.person_preference.preferred_credit_card_id.nil?
          reason_text = "No Preferred Card"
          ret_val[subject][:no_credit_card_count] = ret_val[subject][:no_credit_card_count] + 1
        elsif person.preferred_credit_card_expired?
          reason_text = "Expired Card"
          ret_val[subject][:expired_card_count] = ret_val[subject][:expired_card_count] + 1
        else
          reason_text = "No Preferred Card or PayPal"
          ret_val[subject][:no_preferred_card_or_paypal] = ret_val[subject][:no_preferred_card_or_paypal] + 1
        end
      else
        reason_text = "No Preferences"
        ret_val[subject][:no_preferences_count] = ret_val[subject][:no_preferences_count] + 1
      end

      log("Payment Batch","Renewal Notice sent to customer - #{reason_text}",:email_id=>email_id, :email=>person.email, :person=>person.id )

      ret_val[subject][:sent_count] += 1
      total_count = total_count + 1

    else
      log("Payment Batch",'Error - person pulled by preference_reminders, but has no renewals in time period',:email_id=>email_id, :person=> person.id)
    end
  end if people_to_notify

  log("Payment Batch","End send_reminder_emails",:email_id=>email_id, :count=> total_count, :time=>pretty_time_since(start_time) )

  #Return email_id and count
  {email_id=>ret_val.merge!({:sent_count=>total_count, :expiration_date=>expiration_date, :renewal_interval=>options[:renewal_interval]})}
end
