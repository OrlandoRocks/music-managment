namespace :tunecore do
  namespace :video do


        desc "Adds video product for Canada"
        task :add_canada_video_product => :environment do
          @product_name = "Music Video Distribution"
          @display_name = "Music Video Distribution"
          @description =  "Distribution of your music video to iTunes."
          @admin = ENV['ADMIN'] || 'arne@tunecore.com'
          @country_website_id = 2
          @price = "0.00"
          admin = Person.find_by(email: ENV['ADMIN'])
          country_website = CountryWebsite.find_by(id: @country_website_id)

          ActiveRecord::Base.transaction do
              if product = Product.where('name = ? and country_website_id = ?', @product_name, @country_website_id).first
                puts "#{@product_name} exists for country_website_id #{@country_website_id} - exiting"
              else
                product = Product.new(:name => @product_name,
                                       :display_name => @display_name,
                                       :description => @description,
                                       :status => 'Active',
                                       :product_type => 'Ad Hoc',
                                       :is_default => true,
                                       :sort_order => 1,
                                       :renewal_level => 'None',
                                       :price => @price,
                                       :currency => country_website.currency,
                                       :created_by_id => @admin,
                                       :country_website_id => country_website.id)

                product.save!
                product.id = Product.where('name = ? and country_website_id = ?', @product_name, @country_website_id).first.id
              # You will need to hard-code the resulting product id in the product.rb model as well
                puts "Created product #{@product_name} id #{product.id} (#{country_website.currency})"

                base_item = BaseItem.find_or_create_by(:name => @product_name,
                                                                   :description => @description,
                                                                   :price => "0.00",
                                                                   :currency => country_website.currency)
                base_item_option = BaseItemOption.find_or_create_by(:name=>@product_name,
                                                                          :base_item => base_item,
                                                                          :rule_type => 'inventory',
                                                                          :rule => 'price_for_each',
                                                                          :inventory_type => 'MusicVideo',
                                                                          :price => "0.00",
                                                                          :currency => country_website.currency)
                base_item_option_1 = BaseItemOption.find_or_create_by(:name=>"Cost for 0 - 5 minutes",
                                                                           :base_item => base_item,
                                                                           :rule_type => 'inventory',
                                                                           :parent_id => base_item_option.id,
                                                                           :rule => 'range',
                                                                           :minimum => "0",
                                                                           :maximum => "5",
                                                                           :inventory_type => 'video_length',
                                                                           :model_to_check => "MusicVideo",
                                                                           :method_to_check => "video_length_in_minutes",
                                                                           :price => "299.00",
                                                                           :currency => country_website.currency)
                base_item_option_2 = BaseItemOption.find_or_create_by(:name=>"Cost for 6 - 11 minutes",
                                                                           :base_item => base_item,
                                                                           :rule_type => 'inventory',
                                                                           :parent_id => base_item_option.id,
                                                                           :rule => 'range',
                                                                           :minimum => "6",
                                                                           :maximum => "11",
                                                                           :inventory_type => 'video_length',
                                                                           :model_to_check => "MusicVideo",
                                                                           :method_to_check => "video_length_in_minutes",
                                                                           :price => "399.00",
                                                                           :currency => country_website.currency)
                base_item_option_3 = BaseItemOption.find_or_create_by(:name=>"Cost for 12 - 20 minutes",
                                                                           :base_item => base_item,
                                                                           :rule_type => 'inventory',
                                                                           :parent_id => base_item_option.id,
                                                                           :rule => 'range',
                                                                           :minimum => "12",
                                                                           :maximum => "20",
                                                                           :inventory_type => 'video_length',
                                                                           :model_to_check => "MusicVideo",
                                                                           :method_to_check => "video_length_in_minutes",
                                                                           :price => "499.00",
                                                                           :currency => country_website.currency)
                base_item_option_4 = BaseItemOption.find_or_create_by(:name=>"Cost for 21 - 30 minutes",
                                                                           :base_item => base_item,
                                                                           :rule_type => 'inventory',
                                                                           :parent_id => base_item_option.id,
                                                                           :rule => 'range',
                                                                           :minimum => "21",
                                                                           :maximum => "30",
                                                                           :inventory_type => 'video_length',
                                                                           :model_to_check => "MusicVideo",
                                                                           :method_to_check => "video_length_in_minutes",
                                                                           :price => "649.00",
                                                                           :currency => country_website.currency)
                product_item = ProductItem.find_or_create_by(:name => @product_name,
                                                                      :description => "Music Video Distribution to iTunes",
                                                                      :product => product,
                                                                      :base_item => base_item,
                                                                      :price => "0.00",
                                                                      :currency => country_website.currency)
                product_item_rule = ProductItemRule.find_or_create_by(:inventory_type => 'MusicVideo',
                                                                           :product_item => product_item,
                                                                           :base_item_option => base_item_option,
                                                                           :rule_type => 'price_for_each',
                                                                           :rule => 'price_for_each',
                                                                           :quantity => 1,
                                                                           :price => "0.00",
                                                                           :currency => country_website.currency)
                product_item_rule_1 = ProductItemRule.find_or_create_by(:inventory_type => 'video_length',
                                                                              :product_item => product_item,
                                                                              :parent_id => product_item_rule.id,
                                                                              :base_item_option => base_item_option_1,
                                                                              :rule_type => 'price_only',
                                                                              :rule => 'range',
                                                                              :minimum => '0',
                                                                              :maximum => '5',
                                                                              :model_to_check => "MusicVideo",
                                                                              :method_to_check => "video_length_in_minutes",
                                                                              :price => "299.00",
                                                                              :currency => country_website.currency)
                product_item_rule_2 = ProductItemRule.find_or_create_by(:inventory_type => 'video_length',
                                                                              :product_item => product_item,
                                                                              :parent_id => product_item_rule.id,
                                                                              :base_item_option => base_item_option_2,
                                                                              :rule_type => 'price_only',
                                                                              :rule => 'range',
                                                                              :minimum => '6',
                                                                              :maximum => '11',
                                                                              :model_to_check => "MusicVideo",
                                                                              :method_to_check => "video_length_in_minutes",
                                                                              :price => "399.00",
                                                                              :currency => country_website.currency)


                product_item_rule_3 = ProductItemRule.find_or_create_by(:inventory_type => 'video_length',
                                                                              :product_item => product_item,
                                                                              :parent_id => product_item_rule.id,
                                                                              :base_item_option => base_item_option_3,
                                                                              :rule_type => 'price_only',
                                                                              :rule => 'range',
                                                                              :minimum => '11',
                                                                              :maximum => '20',
                                                                              :model_to_check => "MusicVideo",
                                                                              :method_to_check => "video_length_in_minutes",
                                                                              :price => "499.00",
                                                                              :currency => country_website.currency)
                product_item_rule_4 = ProductItemRule.find_or_create_by(:inventory_type => 'video_length',
                                                                              :product_item => product_item,
                                                                              :parent_id => product_item_rule.id,
                                                                              :base_item_option => base_item_option_4,
                                                                              :rule_type => 'price_only',
                                                                              :rule => 'range',
                                                                              :minimum => '21',
                                                                              :maximum => '30',
                                                                              :model_to_check => "MusicVideo",
                                                                              :method_to_check => "video_length_in_minutes",
                                                                              :price => "649.00",
                                                                              :currency => country_website.currency)

               puts "Created base_item, base_item_option #{base_item_option.id}, product_item, and product_item_rules for #{@product_name} in #{country_website.currency}"
             end
           end
    end

    desc "Adds feature film products for USD and CAD SD and HD resolution"
    task :add_feature_film_product => :environment do
      @product_name = "Feature Film Distribution"
      @display_name = "Feature Film Distribution"
      @description =  "Distribution of your feature film to iTunes."
      @admin = ENV['ADMIN'] || 'arne@tunecore.com'
      @country_website_ids = [1,2]
      @price = "0.00"
      admin = Person.find_by(email: ENV['ADMIN'])

      @country_website_ids.each do |country_website_id|
        country_website = CountryWebsite.find_by(id: country_website_id)

        ActiveRecord::Base.transaction do
          if product = Product.where('name = ? and country_website_id = ?', @product_name, country_website_id).first
            puts "#{@product_name} exists for country_website_id #{@country_website_id} - exiting"
          else
            product = Product.new(:name => @product_name,
                                   :display_name => @display_name,
                                   :description => @description,
                                   :status => 'Active',
                                   :product_type => 'Ad Hoc',
                                   :is_default => true,
                                   :sort_order => 1,
                                   :renewal_level => 'None',
                                   :price => @price,
                                   :currency => country_website.currency,
                                   :created_by_id => @admin,
                                   :country_website_id => country_website.id)

            product.save!
            product.id = Product.where('name = ? and country_website_id = ?', @product_name, country_website_id).first.id
          # You will need to hard-code the resulting product id in the product.rb model as well
            puts "Created product #{@product_name} id #{product.id} (#{country_website.currency}) -- remember to hard-code this as a variable in the product.rb model"

            base_item = BaseItem.find_or_create_by(:name => @product_name,
                                                           :description => @description,
                                                           :price => "0.00",
                                                           :currency => country_website.currency)
            base_item_option = BaseItemOption.find_or_create_by(:name=>@product_name,
                                                                  :base_item => base_item,
                                                                  :rule_type => 'inventory',
                                                                  :rule => 'price_for_each',
                                                                  :inventory_type => 'FeatureFilm',
                                                                  :price => "0.00",
                                                                  :currency => country_website.currency)
            base_item_option_hd = BaseItemOption.find_or_create_by(:name=>"HD Feature Film",
                                                                    :base_item => base_item,
                                                                    :rule_type => 'inventory',
                                                                    :parent_id => base_item_option.id,
                                                                    :rule => 'true_false',
                                                                    :true_false => "1",
                                                                    :inventory_type => 'feature_definition',
                                                                    :model_to_check => "FeatureFilm",
                                                                    :method_to_check => "is_hd_feature?",
                                                                    :price => "1299.00",
                                                                    :currency => country_website.currency)
            base_item_option_sd = BaseItemOption.find_or_create_by(:name=>"SD Feature Film",
                                                                    :base_item => base_item,
                                                                    :parent_id => base_item_option.id,
                                                                    :rule_type => 'inventory',
                                                                    :rule => 'true_false',
                                                                    :inventory_type => 'feature_definition',
                                                                    :model_to_check => "FeatureFilm",
                                                                    :method_to_check => "is_hd_feature?",
                                                                    :price => "999.00",
                                                                    :currency => country_website.currency)
            product_item = ProductItem.find_or_create_by(:name => @product_name,
                                                              :description => "Feature Film Distribution to iTunes",
                                                              :product => product,
                                                              :base_item => base_item,
                                                              :price => "0.00",
                                                              :currency => country_website.currency)
            product_item_rule = ProductItemRule.find_or_create_by(:inventory_type => 'FeatureFilm',
                                                                   :product_item => product_item,
                                                                   :base_item_option => base_item_option,
                                                                   :rule_type => 'price_for_each',
                                                                   :rule => 'price_for_each',
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => country_website.currency)
            product_item_rule_hd = ProductItemRule.find_or_create_by(:inventory_type => 'feature_definition',
                                                                       :product_item => product_item,
                                                                       :parent_id => product_item_rule.id,
                                                                       :base_item_option => base_item_option_hd,
                                                                       :rule_type => 'price_only',
                                                                       :rule => 'true_false',
                                                                       :model_to_check => "FeatureFilm",
                                                                       :method_to_check => "is_hd_feature?",
                                                                       :true_false => "1",
                                                                       :price => "1299.00",
                                                                       :currency => country_website.currency)
            product_item_rule_sd = ProductItemRule.find_or_create_by(:inventory_type => 'feature_definition',
                                                                     :product_item => product_item,
                                                                     :base_item_option => base_item_option_sd,
                                                                     :parent_id => product_item_rule.id,
                                                                     :rule_type => 'price_only',
                                                                     :rule => 'true_false',
                                                                     :model_to_check => 'FeatureFilm',
                                                                     :method_to_check => 'is_hd_feature?',
                                                                     :true_false => "0",
                                                                     :price => "999.00",
                                                                     :currency => country_website.currency)
            puts "Created base_item, base_item_option #{base_item_option.id}, product_item, and product_item_rules for #{@product_name} in country_website_id #{country_website.currency}"
         end
       end
     end
   end

   desc "updates feature film prices"
   task :update_feature_prices => :environment do
     @country_website_ids = [1,2]
     @country_website_ids.each do |country_website_id|
       country = CountryWebsite.find(country_website_id)

       ActiveRecord::Base.transaction do
         if rule = ProductItemRule.find_by(price: "999.00", currency: country.currency)
         rule.price = "849.00"
         if rule.save
           puts "Updated ProductItemRule #{rule.id} to 849.00"
         end
        end
         if rule2 = ProductItemRule.find_by(price: "1299.00", currency: country.currency)
         rule2.price = "1099.00"
         if rule2.save
           puts "Updated ProductItemRule #{rule2.id} to 1099.00"
         end
        end
      end
     end
   end

 end
end
