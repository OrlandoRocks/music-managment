namespace :extract_invoices_in_bulk do
  desc "upload invoices to s3"
  task :inbound, [:start_date, :end_date] => :environment do |task, args|
    InvoiceExport::InboundBatchWorker.perform_async(args.start_date, args.end_date)
    puts "Scheduled worker to upload inbound invoices"
  end

  task :outbound, [:start_date, :end_date] => :environment do |task, args|
    InvoiceExport::OutboundBatchWorker.perform_async(args.start_date, args.end_date)
    puts "Scheduled worker to upload outbound invoices"
  end
end
