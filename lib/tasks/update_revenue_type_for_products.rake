namespace :products do
  desc "Add revenue type for existing products"
  task update_revenue_type: :environment do
    puts "Adding revenue type for products"
    product_revenue_types = {
      'Artist Services': %w[fan_reviews_1
                            fan_reviews_2
                            fan_reviews_3
                            tc_social_annually
                            tc_social_monthly],
      'Distribution': %w[1_yr
                         10_album_creds
                         10_single_creds
                         10_ringtone_creds
                         2_more_yrs
                         2_yrs
                         2_yr_album_renewal
                         2_yr_single_renewal
                         20_single_creds
                         20_album_creds
                         20_ringtone_creds
                         3_ringtone_creds
                         4_more_yrs
                         5_album_creds
                         5_ringtone_creds
                         5_yr_album_renewal
                         5_yr_single_renewal
                         5_single_creds
                         5_yrs
                         automator
                         booklet
                         credit
                         credit_usage
                         lifetime
                         monthly_renewal
                         monthly_album
                         monthly
                         preorder
                         ringtone_renewal
                         ringtone_credit
                         single_credit
                         single_renewal
                         store
                         yearly_renewal],
      'Publishing': %w[pub_admin],
      'Video': %w[fb_track_monetization
                  ytsr]
    }
    # Exclude the products having ids listed below as those are obsolete
    # as per the spreadsheet
    obsolete_ids = [4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 26,
                    27, 28, 29, 30, 31, 32, 46, 47, 48, 63, 64, 71, 72,
                    73, 74, 75, 76, 89, 90, 91, 92, 99, 100, 139, 140,
                    141, 142, 143, 144, 149, 150, 151, 152, 191, 192, 193,
                    196, 197, 233, 234, 235, 238, 320, 321, 322, 325, 362, 401, 453]
    product_revenue_types.each do |revenue_type, display_names|
      Product.where(display_name: display_names)
             .where.not(id: obsolete_ids)
             .update_all(revenue_type: revenue_type)
    end
    puts "Completed adding revenue type for products"
  end
end
