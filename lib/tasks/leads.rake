require "#{Rails.root}/lib/logging"

namespace :leads do
  desc 'Migrates Leads to appropriate country code'
  task migration: :environment do
    raise "Must set COUNTRY_WEBSITE_ID environment variable to run leads migration task" if ENV["COUNTRY_WEBSITE_ID"].nil?

    target_country_website = CountryWebsite.find(ENV["COUNTRY_WEBSITE_ID"])

    name_to_id_map = CountryWebsite::COUNTRY_WEBSITE_ID_MAP.each_with_object({}) do |(country_website_name, country_website_id), object|
      (object[country_website_id] ||= []) << country_website_name
    end
    countries_for_target =  name_to_id_map[target_country_website.id]

    subject = "Leads Migration Task"

    people = ENV["TRANSFER_EMAILS"] ? Person.where(email: ENV["TRANSFER_EMAILS"].split(",")) : Person.leads_for(countries_for_target)

    people.each do |person|
      begin
        person.country_website_id = target_country_website.id
        person.person_balance.currency = target_country_website.currency

        person.accepted_terms_and_conditions_on = nil
        if !person.purchases.empty?
          person.purchases.each do |purchase|
            purchase.destroy
            log(subject, "Destroyed Purchase_id #{purchase.id} for person_id: #{person.id}.")
          end
        end

        person.person_balance.save!
        person.save!
        PersonNotifier.lead_migrated(person).deliver
        log(subject, "Migration notification sent to person_id: #{person.id}.")
        property = {'tccountrywebsite' => target_country_website.country}
        log(subject, "HubSpot 'tccountrywebsite' property was updated for person_id: #{person.id}.")
      rescue => e
        log(subject, "Failed to Migrate Lead person id #{person.id} from #{person.country} due to: #{e.message}")
      end
    end
  end
end
