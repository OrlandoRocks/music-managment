namespace :tc_distributor do
  desc "last two days summary of delivery errors from tc-distributor"
  task :delivery_error_from_tc_distributor_email => :environment do
    DeliveryTcDistributorErrorNotifierService.notify
  end
end
