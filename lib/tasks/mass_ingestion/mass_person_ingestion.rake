namespace :mass_ingestions do
  task people: :environment do
    # Usage: Ingest people from CSV file
    # FILEPATH=tmp/people.csv SOURCE=stem be rake mass_ingestions:people

    filepath  = ENV["FILEPATH"].presence
    source    = ENV["SOURCE"].presence

    if filepath && source
      MassIngestion::PersonService.ingest(filepath, source)
    else
      puts "FILEPATH and SOURCE ENV variables must be provided"
    end
  end
end
