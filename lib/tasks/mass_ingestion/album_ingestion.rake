namespace :mass_ingestions do
  # Usage 1: Ingest all the albums present inside a folder in the s3 bucket
  # VENDOR_BUCKET="stem-transfer-staging.tunecore.com" VENDOR_BUCKET_PREFIX="batch-1" rake mass_ingestions:albums

  # Usage 2: Ingest specific upcs inside a folder in the s3 bucket from a csv
  # VENDOR_BUCKET="stem-transfer-staging.tunecore.com" VENDOR_BUCKET_PREFIX="batch-1" VENDOR_UPCS_FILE_PATH="upcs_file_path" rake mass_ingestions:albums
  task albums: :environment do
    s3 = AWS::S3::Client.new(region: 'us-east-1',
                     access_key_id: ENV['VENDOR_AWS_ACCESS_KEY'],
                     secret_access_key: ENV['VENDOR_AWS_SECRET_KEY'])

    vendor_upcs_file_path = ENV['VENDOR_UPCS_FILE_PATH'] if ENV['VENDOR_UPCS_FILE_PATH'] && File.exists?(ENV['VENDOR_UPCS_FILE_PATH'])
    run_at = Time.now.strftime('%Y-%b-%d_%H-%M')
    bucket_album_dirs = []
    if vendor_upcs_file_path.present?
      CSV.foreach(vendor_upcs_file_path, headers: true) do |vendor_upc_row|
        vendor_upc =  vendor_upc_row.to_h['UPC']
        common_prefix = s3.list_objects(bucket_name:ENV['VENDOR_BUCKET'], prefix: "#{ENV['VENDOR_BUCKET_PREFIX']}/#{vendor_upc}",  delimiter:'/').common_prefixes.last if vendor_upc
        bucket_album_dirs << common_prefix[:prefix] if common_prefix
      end
    else
      buckets = s3.list_objects(bucket_name:ENV['VENDOR_BUCKET'], prefix: "#{ENV['VENDOR_BUCKET_PREFIX']}/",  delimiter:'/')
      while buckets.truncated do
        bucket_album_dirs << buckets.common_prefixes.map(&:values).flatten
        marker = buckets.try(:next_marker) || buckets.common_prefixes.last[:prefix]
        buckets = s3.list_objects(bucket_name:ENV['VENDOR_BUCKET'], prefix: "#{ENV['VENDOR_BUCKET_PREFIX']}/",  delimiter:'/', marker: marker)
      end
      bucket_album_dirs << buckets.common_prefixes.map(&:values).flatten unless buckets.truncated #pushing the upcs directly from the last page of the paginated object
    end

    bucket_album_dirs.flatten!

    bucket_album_dirs.each do |bucket_album_dir|
      Rails.logger.info("enqueueing-ingestion-for #{bucket_album_dir}")
      MassIngestion::AlbumServiceWorker.perform_async(ENV['VENDOR_BUCKET'], bucket_album_dir)
    end
  end
end