namespace :streaming do
  desc "find streaming distributions for albums that need review and start them"
  task :start => :environment do
    Distribution.unreviewed_albums
    .find_each do |d|
      d.start(actor: "streaming:start cron job", message: "Distribution created for new markets")
    end
  end
end
