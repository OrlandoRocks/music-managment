namespace :new_publishing do
  namespace :reports do
    desc "Generate W-8BEN history listing in excel"
    task generate_w8_ben_history_listing: :environment do
      date_at = '2012-12-31 23:59:59'

      file_name = Rails.env.production? ? 'w8ben_listing.csv' : 'test_w8ben_listing.csv'

      file_path = File.join(Rails.root, 'tmp', file_name)

      tax_infos = TaxInfo.all

      CSV.open(file_path,'w', :force_quotes => true) do |csv|
        csv << ['Acct No.', 'Name', 'Country of Corp', 'Owner Type', 'Tax ID', 'EIN Checked?', 'Address 1', 'Address 2', 'City', 'State', 'Zip', 'Country',
          'Mailing Address 1', 'Mailing Address 2', 'Mailing City', 'Mailing State', 'Mailing Zip', 'Mailing Country', 'Claim of Treaty Benefits',
          'Agreed to W-8BEN At'
          ]

        tax_infos.each do |tax_info|
          v = tax_info.paper_trail.version_at(date_at)
          if v and v.completed_w8ben?
            name = v.name || v.entity_name
            csv << [tax_info.publishing_composer&.person_id, name, Country.country_name(v.country_of_corp), v.w8ben_owner_type_text, v.tax_id, v.is_entity, v.address_1, v.address_2, v.city, v.state, v.zip, Country.country_name(v.country),
              v.mailing_address_1, v.mailing_address_2, v.mailing_city, v.mailing_state, v.mailing_zip, Country.country_name(v.mailing_country), v.w8ben_claim_of_treaty_benefits_text,
              v.agreed_to_w8ben_at.to_s(:long)
            ]
          end
        end
      end

      RakeDatafileService.upload(file_path, overwrite: 'y')
    end
  end
end
