namespace :invoice do
  desc 'Sent VAT invoices With PDF attachment on End Of Day: cron 0 6 * * *'
  task send_vat_invoices: :environment do
    Invoice.not_sent.where_settled.with_invoice_sequence.find_each do |invoice|
      puts "enqueueing #{invoice.id}"
      InvoiceNotificationWorker.perform_async(invoice.id)
    end
  end

  desc 'Update invoice_sent_at column for all historic invoices'
  task update_invoice_sent_at: :environment do
    Invoice.update_all(invoice_sent_at: Time.zone.now)
  end
end

