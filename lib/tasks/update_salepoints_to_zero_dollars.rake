# frozen_string_literal: true


# e.g. bundle exec rake update_salepoints_to_zero_dollars\[4\]
desc "Update Salepoints to Zero Dollars"
task :update_salepoints_to_zero_dollars, [:country_website_id] => :environment do |_, args|
  Rails.logger.info "Starting Task"
  product_item_rules = Product
                         .includes(:product_item_rules)
                         .find_by(
                           country_website_id: args[:country_website_id],
                           display_name: Product::STORE_DISPLAY_NAME,
                           status: Product::ACTIVE,
                           product_item_rules: {price: 0.1..Float::INFINITY}
                         ).product_item_rules
  product_item_rules.update_all(price: 0)
  Rails.logger.info "Updated salepoint to zero dollars for country_website_id: #{args[:country_website_id]} and " \
  "product_item_rules_ids: #{product_item_rules.map(&:id)}"

  Rails.logger.info "There it is."
end
