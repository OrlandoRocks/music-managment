namespace :transient do
  desc "Create Feature Flag for New-Refunds Process"
  task create_refunds_feature_flag: :environment do
    FeatureFlipper.add_feature('new_refunds')
  end
end
