desc "backfill missing track monetizations for approved albums with facebook monetization"
task facebook_track_monetization: :environment do
  query = %(
    SELECT DISTINCT COUNT(`track_monetizations`.`id`) AS `track_mon_count`,
      COUNT(`songs`.`id`) AS `song_count`,
      `albums`.`id`,
      `albums`.`person_id`
    FROM `albums`
    JOIN `people` ON `albums`.`person_id` = `people`.`id`
    JOIN `person_subscription_statuses` ON `person_subscription_statuses`.`person_id` = `people`.`id`
    JOIN `review_audits` ON `review_audits`.`album_id` = `albums`.`id`
    JOIN `songs` ON `songs`.`album_id` = `albums`.`id`
    LEFT OUTER JOIN (
      SELECT `track_monetizations`.* FROM `track_monetizations`
      WHERE `track_monetizations`.`store_id` = 83
    ) AS `track_monetizations` ON `track_monetizations`.`song_id` = `songs`.`id`
    WHERE
      `people`.`is_verified` = 1
    AND
      `people`.`deleted` = 0
    AND
      `people`.`status` IN ('Active', 'Suspicious')
    AND
      `person_subscription_statuses`.`subscription_type` = 'FBTracks'
    AND
      `review_audits`.`event` = "APPROVED"
    AND
      `albums`.`payment_applied` = 1
    AND
      `albums`.`finalized_at` IS NOT NULL
    AND
      `albums`.`takedown_at` IS NULL
    GROUP BY `albums`.`id`
    HAVING `track_mon_count` = 0 AND `song_count` > 0
    LIMIT :limit 
  )

  limit = ENV.fetch("LIMIT", 100).to_i

  albums = Album.find_by_sql([query, { limit: limit }])
  album_ids = albums.map(&:id)
  store_id = Store.find_by(short_name: "FBTracks")&.id

  album_ids.each do |album_id|
    TrackMonetization::PostApprovalWorker.perform_async(album_id, store_id)
  end
end
