namespace :backfill do
  desc "backfill users data to zendesk"
  task people_info_to_zendesk: :environment do
    Rails.logger.info "Initiating sidekiq queue for zendesk backfill"
    persons = Person.find_by_sql("SELECT distinct(albums.person_id) as id,
                                    (SELECT max(expires_at)
                                      FROM renewal_history rh
                                      INNER JOIN renewals r ON r.id = rh.renewal_id
                                      INNER JOIN renewal_items ri ON ri.renewal_id = r.id
                                      WHERE ri.related_type = 'Album'
                                      AND ri.related_id = albums.id
                                      AND r.takedown_at IS null
                                      AND rh.expires_at > current_timestamp
                                    )
                                    renewal_date
                                    FROM albums
                                    WHERE (albums.takedown_at IS NULL
                                      AND albums.payment_applied = 1)
                                    GROUP BY albums.person_id
                                    ORDER BY person_id ASC")
    persons.each do |person|
      Rails.logger.info "Initiated sidekiq queue for zendesk backfill for #{person.id}"
      Zendesk::Person.create_apilogger(person.id)
    end
    Rails.logger.info "Completed Initiating sidekiq queue for zendesk backfill"
  end

  desc "Rake task to backfill data every minute 800 requests"
  task backfill_zendesk_failures: :environment do
    ApiLogger.where(state: 'error', external_service_name: "Zendesk").find_each do |api_logger|
      Rails.logger.info "Initiating queue for api_logger: #{api_logger.id}"
      ApiLoggerWorker.perform_async(api_logger.id)
    end
  end
end
