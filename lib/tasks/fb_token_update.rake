FB_TOKEN_EXPIRY_LIMIT = 58
SOCIAL_PROFILE_FB = 'facebook'
def extend_fb_token(access_token)
  client_id = ENV['FB_CLIENT_ID']
  client_secret = ENV['FB_SECRET_KEY']
  facebook_oauth = Koala::Facebook::OAuth.new(client_id, client_secret)
  response = facebook_oauth.exchange_access_token_info(access_token)
  response['access_token']
rescue Koala::Facebook::APIError => error
  nil
end

def update_fb_tokens(fb_services_list)
  fb_services_list.each do |fb_service|
    extended_token = extend_fb_token(fb_service.access_token)
    next if extended_token.nil?
    fb_service.social_login = true
    fb_service.update(token_update_attributes(extended_token))
  end
end

def token_update_attributes(extended_token)
  {
    access_token: extended_token,
    access_token_expiry: Date.today + FB_TOKEN_EXPIRY_LIMIT.days
  }
end

namespace :fb_token_update do
  desc "updates facebook tokens expiring tomorrow"
  task facebook_token_updater: :environment do
    fb_service_id = ExternalService.find_by(name: SOCIAL_PROFILE_FB).id
    fb_services_list = ExternalServicesPerson
    .where(external_service_id: fb_service_id,
      access_token_expiry:
      Date.tomorrow.beginning_of_day..Date.tomorrow.end_of_day)
    update_fb_tokens(fb_services_list)
  end
end
