desc "removing the invoice_id from a failed purchase if the invoice does not exist"
task remove_absent_invoice_from_failed_purchases: :environment do
  purchase_ids = ENV.fetch("PURCHASE_IDS", "").split(",")
  purchases = Purchase.where(id: purchase_ids, status: "failed")

  if purchases.present?
    purchases.each do |purchase|
      if Invoice.find_by(id: purchase.invoice_id).present?
        puts "Can not remove invoice_id from Purchase #{purchase.id}: that invoice is present"
      else
        puts "Removing invoice_id from Purchase #{purchase.id}"

        purchase.invoice_id = nil

        if purchase.save
          puts "Successfully removed invoice_id from Purchase #{purchase.id}"
        else
          puts "Failed to removed invoice_id from Purchase #{purchase.id}"
        end
      end
    end
  else
    puts "Please set PURCHASE_IDS=(a string of comma-separated ids for present, failed purchases)"
  end
end
