namespace(:gremlins) do
  namespace(:controllers) do
    desc "Detect Actions that are overly long"
    task :bloat => :environment do
      require 'ruby2ruby'
      require 'ruport'
      controllers = Dir["#{Rails.root}/app/controllers/**/*.rb"]
    
      method_length_report = Ruport::Data::Table.new(:column_names => %w(method line_count respond_to))
      controllers.each do |c|
        filename = c.gsub("#{Rails.root}/app/controllers/", '').gsub(".rb", '')
        klassname = filename.camelize
        klass = klassname.constantize
        klass.instance_methods(false).each do |m|
          method = Ruby2Ruby.translate(klass, m.to_sym)
          method_length_report << ["#{klassname}##{m}", method.split("\n").length - 2, !!method.match("respond_to")]
        end
      end
      method_length_report.reduce {|r| r.line_count > 7 }
      method_length_report.sort_rows_by!("line_count", :order => :descending)
      puts method_length_report
    end
    
    desc "Detect Actions that do not have REST names "
    task :unrest => :environment do
      require 'ruby2ruby'
      require 'ruport'
      controllers = Dir["#{Rails.root}/app/controllers/**/*.rb"]
      allowed_methods = %w(index new show edit create update destroy)
      non_resty_report = Ruport::Data::Table.new(:column_names => %w(method line_count respond_to))
      controllers.each do |c|
        filename = c.gsub("#{Rails.root}/app/controllers/", '').gsub(".rb", '')
        klassname = filename.camelize
        klass = klassname.constantize
        klass.public_instance_methods(false).each do |m|
          method = Ruby2Ruby.translate(klass, m.to_sym)
          non_resty_report << ["#{klassname}##{m}", method.split("\n").length - 2, !!method.match("respond_to")] unless allowed_methods.include?(m)
        end
      end
      puts "#{non_resty_report.length} Non Resty Actions found in your App!"
      puts non_resty_report
    end
  end
  
  namespace(:models) do
    desc "Detect Methods on Models that are overly long"
    task :bloat => :environment do
      require 'ruby2ruby'
      require 'ruport'
      models = Dir["#{Rails.root}/app/models/**/*.rb"]
    
      method_length_report = Ruport::Data::Table.new(:column_names => %w(method line_count))
      models.each do |model|
        filename = model.gsub("#{Rails.root}/app/models/", '').gsub(".rb", '')
        klassname = filename.camelize
        klass = klassname.constantize
        klass.instance_methods(false).each do |m|
          puts "looking for #{klassname} #{m}"
          method = Ruby2Ruby.translate(klass, m.to_sym) rescue "ab"
          method_length_report << ["#{klassname}##{m}", method.split("\n").length - 2]
        end
      end
      method_length_report.reduce {|r| r.line_count > 8 }
      method_length_report.sort_rows_by!("line_count", :order => :descending)
      puts method_length_report
    end
  end
end