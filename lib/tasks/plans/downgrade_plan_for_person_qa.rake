# frozen_string_literal: true

# bundle exec rake "plans:downgrade_plan_for_person_qa['<person_email>', <plan_id>]"
# This task can be removed from repo after ECOM-1600 is deployed

desc "Handles required setup and calls Plans::DowngradeService, for QA purposes only!"
namespace :plans do
  task :downgrade_plan_for_person_qa, [:person_email, :plan_id] => :environment do |_, args|
    raise "Don't run in production" if Rails.env == "production"
    Rails.logger.info "Starting task..."
    begin
        person = Person.find_by(email: args[:person_email])
        
        plan = Plan.find(args[:plan_id])
        plan_product = plan.products.find_by(country_website_id:person.country_website_id)
        
        purchase = Purchase.new(person:person,product:plan_product,cost_cents:0,discount_cents:0,currency:"USD", related:plan_product)
        purchase.save!(validate: false)

        Plans::DowngradeService.call(person, plan, purchase:purchase)

        Rails.logger.info "Complete!"
    rescue => e
        Rails.logger.error(e)
        Rails.logger.error(JSON.pretty_generate(JSON.parse(e.backtrace)))
    end
  end
end
