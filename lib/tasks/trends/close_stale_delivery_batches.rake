namespace :stale_delivery_batches do
  desc "Close batches older than cutoff time (minutes), within age_cap (hours)"
  task :close_fb_spotify_batches => :environment do
    stores = [
      {
        short_name: "Spotify",
        cutoff_time: 180
      }, {
        short_name: "FBTracks",
        cutoff_time: 1440,
        age_cap: 49
      }, {
        short_name: "FBReels",
        cutoff_time: 1440,
        age_cap: 49
      }, {
        short_name: "Instagram",
        cutoff_time: 1440,
        age_cap: 49
      }
    ]

    close_batches(stores)
  end

  task :close_stale_batches => :environment do
    stores = [
       {
        short_name: "Joox",
        cutoff_time: 180
      },{
        short_name: "ClaroMusic",
        cutoff_time: 180
      }
    ]
    close_batches(stores)
  end

  def close_batches(stores)
    stores.each do |si|
      store = Store.find_by(short_name: si[:short_name])

      DistributionSystem::BatchManager.close_stale_batches(
        store.id,
        si[:cutoff_time],
        si[:age_cap]
      )
    end
  end 
end
