require 'active_record'
namespace :canada do
  task :create_ca_monthly_album_renewals => :environment do

    puts "CREATING RENEWAL PRODUCTS"
    puts "  ---  Creating Album Renewal for Canada"
    monthly_album_renewal_product = Product.create(:name => "Monthly Album Renewal",
                                            :created_by_id => 1,
                                            :country_website_id => 2,
                                            :display_name => "Monthly Album Renewal",
                                            :description => "Monthly renewal fee for your album.",
                                            :status => "Active",
                                            :product_type => "Renewal",
                                            :is_default => true,
                                            :applies_to_product => 'Album',
                                            :sort_order => 1,
                                            :renewal_level => 'None',
                                            :renewal_product_id => 0,
                                            :price => "4.99",
                                            :currency => "CAD")
    puts "CREATING AD HOC PRODUCTS"
    puts "  ---  Creating adhoc album product"
    base_item = BaseItem.find_by(name: "Album Distribution")
    base_item_option1 = BaseItemOption.where(:name=>"Album Distribution", :parent_id=>0, :base_item_id=>base_item.id).first
    base_item_option2 = BaseItemOption.where(:inventory_type=>"Song", :parent_id=>base_item_option1.id, :base_item_id=>base_item.id, :rule=>'price_for_each',:unlimited=>1).first
    base_item_option3 = BaseItemOption.where(:inventory_type=>"Salepoint", :parent_id=>base_item_option1.id, :base_item_id=>base_item.id, :rule=>'price_for_each',:unlimited=>1).first
    base_item_option4 = BaseItemOption.where(:method_to_check=>"has_booklet?").first

    adhoc_album_product = Product.create(:name => "Monthly Album", :created_by_id => 1,  :country_website_id => 2, :display_name => "Album - Monthly Plan",
                                          :description => "Your album will be live for one month. Renews monthly for $4.99.", :status => "Active", :product_type => "Ad Hoc",
                                          :is_default => false, :applies_to_product => 'Album', :sort_order => 2, :renewal_level => 'Item',
                                          :renewal_product_id => 0, :price => "0.00", :currency => "CAD")

    adhoc_album_product_item = ProductItem.create(:product_id => adhoc_album_product.id, :base_item_id => base_item.id, :name => "Album Distribution",
                                                   :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                   :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                   :first_renewal_duration => 1, :first_renewal_interval => 'month', :renewal_duration => 1,
                                                   :renewal_interval => 'month', :renewal_product_id => monthly_album_renewal_product.id)

    adhoc_album_product_item_rule_1 = ProductItemRule.create(:product_item_id => adhoc_album_product_item.id,
                                                              :base_item_option_id => base_item_option1.id,
                                                              :parent_id => 0,
                                                              :rule_type => "inventory",
                                                              :rule => "price_for_each",
                                                              :inventory_type => 'Album',
                                                              :quantity => 1,
                                                              :unlimited => 0,
                                                              :price => "4.99",
                                                              :currency => "CAD")

    adhoc_album_product_item_rule_2 = ProductItemRule.create(:product_item_id => adhoc_album_product_item.id,
                                                              :base_item_option_id => base_item_option2.id,
                                                              :parent_id => adhoc_album_product_item_rule_1.id,
                                                              :rule_type => "inventory",
                                                              :rule => "price_for_each",
                                                              :inventory_type => 'Song',
                                                              :model_to_check => "Album",
                                                              :method_to_check => "songs.count",
                                                              :unlimited => 1,
                                                              :quantity => 0,
                                                              :price => "0.00",
                                                              :currency => "CAD")

    adhoc_album_product_item_rule_3 = ProductItemRule.create(:product_item_id => adhoc_album_product_item.id,
                                                              :base_item_option_id => base_item_option3.id,
                                                              :parent_id => adhoc_album_product_item_rule_1.id,
                                                              :rule_type => "inventory",
                                                              :rule => "price_for_each",
                                                              :inventory_type => 'Salepoint',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => 'salepoints.count',
                                                              :unlimited => 1,
                                                              :quantity => 0,
                                                              :price => "0.00",
                                                              :currency => "CAD")

    adhoc_album_product_item_rule_4 = ProductItemRule.create(:product_item_id => adhoc_album_product_item.id,
                                                              :base_item_option_id => base_item_option4.id,
                                                              :parent_id => adhoc_album_product_item_rule_1.id,
                                                              :rule_type => "inventory",
                                                              :rule => "true_false",
                                                              :inventory_type => 'Booklet',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => "has_booklet?",
                                                              :unlimited => 0,
                                                              :quantity => 1,
                                                              :true_false => 1,
                                                              :price => "20.00",
                                                              :currency => "CAD")
    #something is strange with the creation, it seems to take defaults from the base_item, overwriting them in an update
    adhoc_album_product_item_rule_4.update(:inventory_type=>'Booklet',:rule => "true_false",:model_to_check => 'Album',:method_to_check => "has_booklet?" )
  end


  task :create_ca_add_store => :environment do
    ActiveRecord::Base.transaction do

      if Product.exists?(:currency => "CAD", :name => "Added Store")
        puts "Canadian Added store already created - skipping migration"
        return
      else
        puts "CREATING BASE ITEMS"
        puts "  ---  Creating Added Store Base Items"
        ca_store_base_item = BaseItem.create(:name => "Added Store", :description => "Addition of store distribution after an album has already been distributed.",
                                           :first_renewal_duration => 4, :first_renewal_interval => "week", :renewal_duration => 4, :renewal_interval => 'week',
                                           :price => "0.00", :currency => "CAD")

         puts "CREATING BASE ITEM OPTIONS"
         puts "  ---  Creating Added Store Base Item Options"
         ca_store_base_item_option = BaseItemOption.create(:base_item_id => ca_store_base_item.id,
                                                          :sort_order => 1,
                                                          :name => 'Price per store',
                                                          :product_type => 'Ad Hoc',
                                                          :option_type => 'required',
                                                          :rule_type => 'inventory',
                                                          :rule => 'price_for_each',
                                                          :inventory_type => 'Salepoint',
                                                          :quantity => 0,
                                                          :unlimited => 1,
                                                          :price => "1.98",
                                                          :currency => "CAD")
          puts "CREATING AD HOC PRODUCTS"
          puts "  ---  Creating Added Store product"
          ca_store_product = Product.create(:name => "Added Store", :created_by_id => 1,  :country_website_id => 2, :display_name => "Added Store",
                                                :description => "Fee for adding a store to a previously distributed release.", :status => "Active", :product_type => "Ad Hoc",
                                                :is_default => true, :applies_to_product => 'None', :sort_order => 1, :renewal_level => 'Item',
                                                :renewal_product_id => 0, :price => "0.00", :currency => "CAD")

          ca_store_product_item = ProductItem.create(:product_id => ca_store_product.id, :base_item_id => ca_store_base_item.id, :name => "Added Store",
                                                         :description => "Addition of store distribution after an album has already been distributed.",
                                                         :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => nil,
                                                         :first_renewal_duration => nil, :first_renewal_interval => nil, :renewal_duration => nil,
                                                         :renewal_interval => nil, :renewal_product_id => nil)

          ca_store_product_item_rule = ProductItemRule.create(:product_item_id => ca_store_product_item.id,
                                                                    :base_item_option_id => ca_store_base_item_option.id,
                                                                    :parent_id => nil,
                                                                    :rule_type => "inventory",
                                                                    :rule => "price_for_each",
                                                                    :inventory_type => 'Salepoint',
                                                                    :quantity => 1,
                                                                    :unlimited => 0,
                                                                    :price => "1.98",
                                                                    :currency => "CAD")

      end
    end
  end
end #end canada namespace

desc 'Creates all records for CA products'
task :create_ca_products => :environment do
  ActiveRecord::Base.transaction do

    if Product.exists?(:currency => "CAD", :name => "Album Renewal")
      puts "Canadian product found - skipping migration"
      return
    else

      puts "CREATING BASE ITEMS"
      puts "  ---  Creating Album Distribution Base Items"
      @album_base_item = BaseItem.create(:name => "Album Distribution", :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                         :first_renewal_duration => 4, :first_renewal_interval => "week", :renewal_duration => 4, :renewal_interval => 'week',
                                         :price => "0.00", :currency => "CAD")

      puts "  ---  Creating Single Distribution Base Items"
      @single_base_item = BaseItem.create(:name => "Single Distribution", :description => "Distribution of a single (1 song) to customer selected stores.",
                                          :first_renewal_duration => 4, :first_renewal_interval => "week", :renewal_duration => 4, :renewal_interval => 'week',
                                          :price => "0.00", :currency => "CAD")

      puts "  ---  Creating Ringtone Distribution Base Items"
      @ringtone_base_item = BaseItem.create(:name => "Ringtone Distribution", :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                            :first_renewal_duration => 4, :first_renewal_interval => "week", :renewal_duration => 4, :renewal_interval => 'week',
                                            :price => "0.00", :currency => "CAD")

      puts "CREATING BASE ITEM OPTIONS"
      puts "  ---  Creating Album Distribution Base Item Options"
      @album_base_item_option1 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                       :sort_order => 1,
                                                       :name => 'Album Distribution',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'required',
                                                       :rule_type => 'inventory',
                                                       :rule => 'price_for_each',
                                                       :inventory_type => 'Album',
                                                       :quantity => 1,
                                                       :price => "19.98",
                                                       :currency => "CAD")

      @album_base_item_option2 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                       :sort_order => 2,
                                                       :parent_id => @album_base_item_option1.id,
                                                       :name => 'Price per Song',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'select_one',
                                                       :rule_type => 'inventory',
                                                       :rule => 'price_for_each',
                                                       :inventory_type => 'Song',
                                                       :model_to_check => 'Album',
                                                       :method_to_check => 'songs.count',
                                                       :unlimited => 1,
                                                       :price => "1.98",
                                                       :currency => "CAD")

      @album_base_item_option3 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                       :sort_order => 3,
                                                       :parent_id => @album_base_item_option1.id,
                                                       :name => 'Price per Salepoint',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'select_one',
                                                       :rule_type => 'inventory',
                                                       :rule => 'price_for_each',
                                                       :inventory_type => 'Salepoint',
                                                       :model_to_check => 'Album',
                                                       :method_to_check => 'salepoints.count',
                                                       :unlimited => 1,
                                                       :price => "1.98",
                                                       :currency => "CAD")

      @album_base_item_option4 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                       :sort_order => 3,
                                                       :parent_id => @album_base_item_option1.id,
                                                       :name => 'has_booklet?',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'required',
                                                       :rule_type => 'inventory',
                                                       :rule => 'true_false',
                                                       :inventory_type => 'has_booklet?',
                                                       :model_to_check => 'Album',
                                                       :method_to_check => 'has_booklet?',
                                                       :quantity => 1,
                                                       :true_false => 1,
                                                       :price => "20.00",
                                                       :currency => "CAD")

      puts "  ---  Creating Single Distribution Base Item Options"
      @single_base_item_option1 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                        :sort_order => 1,
                                                        :name => 'Single Distribution',
                                                        :product_type => 'Ad Hoc',
                                                        :option_type => 'required',
                                                        :rule_type => 'inventory',
                                                        :rule => 'price_for_each',
                                                        :inventory_type => 'Single',
                                                        :quantity => 1,
                                                        :price => "9.99",
                                                        :currency => "CAD")

      @single_base_item_option2 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                       :sort_order => 2,
                                                       :parent_id => @single_base_item_option1.id,
                                                       :name => 'Included Song',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'required',
                                                       :rule_type => 'inventory',
                                                       :rule => 'price_for_each',
                                                       :inventory_type => 'Song',
                                                       :model_to_check => 'Single',
                                                       :method_to_check => 'songs.count',
                                                       :quantity => 1,
                                                       :price => "0.00",
                                                       :currency => "CAD")

      @single_base_item_option3 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                       :sort_order => 3,
                                                       :parent_id => @single_base_item_option1.id,
                                                       :name => 'Price per Salepoint',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'select_one',
                                                       :rule_type => 'inventory',
                                                       :rule => 'price_for_each',
                                                       :inventory_type => 'Salepoint',
                                                       :model_to_check => 'Single',
                                                       :method_to_check => 'salepoints.count',
                                                       :unlimited => 1,
                                                       :price => "0.99",
                                                       :currency => "CAD")

      @single_base_item_option4 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                       :sort_order => 4,
                                                       :parent_id => @single_base_item_option1.id,
                                                       :name => 'has_booklet?',
                                                       :product_type => 'Ad Hoc',
                                                       :option_type => 'required',
                                                       :rule_type => 'inventory',
                                                       :rule => 'true_false',
                                                       :inventory_type => 'has_booklet?',
                                                       :model_to_check => 'Single',
                                                       :method_to_check => 'has_booklet?',
                                                       :true_false => 1,
                                                       :price => "20.00",
                                                       :currency => "CAD")

      puts "  ---  Creating Ringtone Distribution Base Item Options"
      @ringtone_base_item_option1 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                          :sort_order => 1,
                                                          :name => 'Ringtone Distribution',
                                                          :product_type => 'Ad Hoc',
                                                          :option_type => 'required',
                                                          :rule_type => 'inventory',
                                                          :rule => 'price_for_each',
                                                          :inventory_type => 'Ringtone',
                                                          :quantity => 1,
                                                          :price => "9.99",
                                                          :currency => "CAD")

      @ringtone_base_item_option2 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                          :sort_order => 2,
                                                          :parent_id => @ringtone_base_item_option1.id,
                                                          :name => 'Included Song',
                                                          :product_type => 'Ad Hoc',
                                                          :option_type => 'required',
                                                          :rule_type => 'inventory',
                                                          :rule => 'total_included',
                                                          :inventory_type => 'Song',
                                                          :model_to_check => 'Ringtone',
                                                          :method_to_check => 'songs.count',
                                                          :quantity => 1,
                                                          :price => "0.00",
                                                          :currency => "CAD")

      @ringtone_base_item_option3 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                          :sort_order => 3,
                                                          :parent_id => @ringtone_base_item_option1.id,
                                                          :name => 'Price per Salepoint',
                                                          :product_type => 'Ad Hoc',
                                                          :option_type => 'select_one',
                                                          :rule_type => 'inventory',
                                                          :rule => 'price_for_each',
                                                          :inventory_type => 'Salepoint',
                                                          :model_to_check => 'Ringtone',
                                                          :method_to_check => 'salepoints.count',
                                                          :unlimited => 1,
                                                          :quantity => 0,
                                                          :price => "0.99",
                                                          :currency => "CAD")

      puts "  ---  Creating Ringtone Distribution Credit Base Item Options"
      @ringtone_credit_base_item_option1 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                                 :sort_order => 1,
                                                                 :name => 'Ringtone Distribution',
                                                                 :product_type => 'Package',
                                                                 :option_type => 'required',
                                                                 :rule_type => 'inventory',
                                                                 :rule => 'price_for_each',
                                                                 :inventory_type => 'Ringtone',
                                                                 :quantity => 5,
                                                                 :price => "0.00",
                                                                 :currency => "CAD")

      @ringtone_credit_base_item_option2 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                                 :parent_id =>  @ringtone_credit_base_item_option1.id,
                                                                 :sort_order => 2,
                                                                 :name => 'Songs Included in Package',
                                                                 :product_type => 'Package',
                                                                 :option_type => 'required',
                                                                 :rule_type => 'inventory',
                                                                 :rule => 'total_included',
                                                                 :inventory_type => 'Song',
                                                                 :model_to_check => 'Ringtone',
                                                                 :method_to_check => 'songs.count',
                                                                 :quantity => 1,
                                                                 :unlimited => 0,
                                                                 :price => "0.00",
                                                                 :currency => "CAD")

      @ringtone_credit_base_item_option3 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                                 :parent_id =>  @ringtone_credit_base_item_option1.id,
                                                                 :sort_order => 5,
                                                                 :name => 'Stores included in Package',
                                                                 :product_type => 'Package',
                                                                 :option_type => 'required',
                                                                 :rule_type => 'inventory',
                                                                 :rule => 'total_included',
                                                                 :inventory_type => 'Salepoint',
                                                                 :model_to_check => 'Ringtone',
                                                                 :method_to_check => 'salepoints.count',
                                                                 :quantity => 0,
                                                                 :unlimited => 1,
                                                                 :price => "0.00",
                                                                 :currency => "CAD")

      @ringtone_credit_base_item_option4 = BaseItemOption.create(:base_item_id => @ringtone_base_item.id,
                                                                 :parent_id =>  @ringtone_credit_base_item_option3.id,
                                                                 :sort_order => 6,
                                                                 :name => 'Price per Store above included',
                                                                 :product_type => 'Package',
                                                                 :option_type => 'select_one',
                                                                 :rule_type => 'price_only',
                                                                 :rule => 'price_for_each',
                                                                 :inventory_type => 'Salepoint',
                                                                 :model_to_check => 'Ringtone',
                                                                 :method_to_check => 'salepoints.count',
                                                                 :quantity => 0,
                                                                 :unlimited => 1,
                                                                 :price => "0.99",
                                                                 :currency => "CAD")

      puts "  ---  Creating Album Distribution Credit Base Item Options"
      @album_credit_base_item_option1 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                              :sort_order => 1,
                                                              :name => 'Album Distribution',
                                                              :product_type => 'Package',
                                                              :option_type => 'required',
                                                              :rule_type => 'inventory',
                                                              :rule => 'price_for_each',
                                                              :inventory_type => 'Album',
                                                              :quantity => 1,
                                                              :unlimited => 0,
                                                              :price => "0.00",
                                                              :currency => "CAD")

      @album_credit_base_item_option2 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                              :parent_id =>  @album_credit_base_item_option1.id,
                                                              :sort_order => 2,
                                                              :name => 'Songs Included in Package',
                                                              :product_type => 'Package',
                                                              :option_type => 'required',
                                                              :rule_type => 'inventory',
                                                              :rule => 'total_included',
                                                              :inventory_type => 'Song',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => 'songs.count',
                                                              :quantity => 0,
                                                              :unlimited => 1,
                                                              :price => "0.00",
                                                              :currency => "CAD")

      @album_credit_base_item_option3 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                              :parent_id =>  @album_credit_base_item_option2.id,
                                                              :sort_order => 3,
                                                              :name => 'Price per Song above included',
                                                              :product_type => 'Package',
                                                              :option_type => 'select_one',
                                                              :rule_type => 'price_only',
                                                              :rule => 'price_for_each',
                                                              :inventory_type => 'Song',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => 'songs.count',
                                                              :quantity => 0,
                                                              :unlimited => 1,
                                                              :price => "0.99",
                                                              :currency => "CAD")

      @album_credit_base_item_option4 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                              :parent_id =>  @album_credit_base_item_option1.id,
                                                              :sort_order => 5,
                                                              :name => 'Stores included in Package',
                                                              :product_type => 'Package',
                                                              :option_type => 'required',
                                                              :rule_type => 'inventory',
                                                              :rule => 'total_included',
                                                              :inventory_type => 'Salepoint',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => 'salepoints.count',
                                                              :quantity => 0,
                                                              :unlimited => 1,
                                                              :price => "0.00",
                                                              :currency => "CAD")

      @album_credit_base_item_option5 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                              :parent_id =>  @ringtone_credit_base_item_option4.id,
                                                              :sort_order => 6,
                                                              :name => 'Price per Store above included',
                                                              :product_type => 'Package',
                                                              :option_type => 'select_one',
                                                              :rule_type => 'price_only',
                                                              :rule => 'price_for_each',
                                                              :inventory_type => 'Salepoint',
                                                              :model_to_check => 'Album',
                                                              :method_to_check => 'salepoints.count',
                                                              :quantity => 0,
                                                              :unlimited => 1,
                                                              :price => "0.99",
                                                              :currency => "CAD")

      puts "  ---  Creating Single Distribution Credit Base Item Options"
      @single_credit_base_item_option1 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                               :sort_order => 1,
                                                               :name => 'Single Distribution',
                                                               :product_type => 'Ad Hoc',
                                                               :option_type => 'required',
                                                               :rule_type => 'inventory',
                                                               :rule => 'price_for_each',
                                                               :inventory_type => 'Single',
                                                               :quantity => 1,
                                                               :price => "9.99",
                                                               :currency => "CAD")

      @single_credit_base_item_option2 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                               :parent_id =>  @single_credit_base_item_option1.id,
                                                               :sort_order => 2,
                                                               :name => 'Included Song',
                                                               :product_type => 'Ad Hoc',
                                                               :option_type => 'required',
                                                               :rule_type => 'inventory',
                                                               :rule => 'price_for_each',
                                                               :inventory_type => 'Song',
                                                               :model_to_check => 'Single',
                                                               :method_to_check => 'songs.count',
                                                               :quantity => 1,
                                                               :unlimited => 0,
                                                               :price => "0.00",
                                                               :currency => "CAD")

      @single_credit_base_item_option3 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                               :parent_id =>  @single_credit_base_item_option1.id,
                                                               :sort_order => 3,
                                                               :name => 'Price per Salepoint',
                                                               :product_type => 'Ad Hoc',
                                                               :option_type => 'select_one',
                                                               :rule_type => 'inventory',
                                                               :rule => 'price_for_each',
                                                               :inventory_type => 'Salepoint',
                                                               :model_to_check => 'Single',
                                                               :method_to_check => 'salepoints.count',
                                                               :quantity => 0,
                                                               :unlimited => 1,
                                                               :price => "0.99",
                                                               :currency => "CAD")

      @single_credit_base_item_option4 = BaseItemOption.create(:base_item_id => @single_base_item.id,
                                                               :parent_id =>  @single_credit_base_item_option3.id,
                                                               :sort_order => 4,
                                                               :name => 'has_booklet?',
                                                               :product_type => 'Ad Hoc',
                                                               :option_type => 'required',
                                                               :rule_type => 'inventory',
                                                               :rule => 'true_false',
                                                               :inventory_type => 'has_booklet?',
                                                               :model_to_check => 'Single',
                                                               :method_to_check => 'has_booklet?',
                                                               :quantity => 0,
                                                               :unlimited => 0,
                                                               :true_false => 1,
                                                               :price => "20.00",
                                                               :currency => "CAD")

      puts "CREATING RENEWAL PRODUCTS"
      puts "  ---  Creating Album Renewal for Canada"
      @album_renewal_product = Product.create(:name => "Album Renewal",
                                              :created_by_id => 1,
                                              :country_website_id => 2,
                                              :display_name => "Album Renewal",
                                              :description => "Annual renewal fee for your album.",
                                              :status => "Active",
                                              :product_type => "Renewal",
                                              :is_default => true,
                                              :applies_to_product => 'None',
                                              :sort_order => 1,
                                              :renewal_level => 'None',
                                              :renewal_product_id => 0,
                                              :price => "49.99",
                                              :currency => "CAD")

      puts "  ---  Creating Ringtone Renewal for Canada"
      @ringtone_renewal_product = Product.create(:name => "Ringtone Renewal",
                                                 :created_by_id => 1,
                                                 :country_website_id => 2,
                                                 :display_name => "Ringtone Renewal",
                                                 :description => "Annual renewal fee for your ringtone.",
                                                 :status => "Active",
                                                 :product_type => "Renewal",
                                                 :is_default => true,
                                                 :applies_to_product => 'None',
                                                 :sort_order => 1,
                                                 :renewal_level => 'None',
                                                 :renewal_product_id => 0,
                                                 :price => "9.99",
                                                 :currency => "CAD")

      puts "  ---  Creating Single Renewal for Canada"
      @single_renewal_product = Product.create(:name => "Single Renewal",
                                               :created_by_id => 1,
                                               :country_website_id => 2,
                                               :display_name => "Single Renewal",
                                               :description => "Annual renewal fee for your single.",
                                               :status => "Active",
                                               :product_type => "Renewal",
                                               :is_default => true,
                                               :applies_to_product => 'None',
                                               :sort_order => 1,
                                               :renewal_level => 'None',
                                               :renewal_product_id => 0,
                                               :price => "9.99",
                                               :currency => "CAD")

      puts "CREATING AD HOC PRODUCTS"
      puts "  ---  Creating adhoc album product"
      @adhoc_album_product = Product.create(:name => "1 Year Album", :created_by_id => 1,  :country_website_id => 2, :display_name => "Album Distribution",
                                            :description => "Your album will be live for one year. Renews annually for $49.99.", :status => "Active", :product_type => "Ad Hoc",
                                            :is_default => true, :applies_to_product => 'Album', :sort_order => 1, :renewal_level => 'Item',
                                            :renewal_product_id => 0, :price => "0.00", :currency => "CAD")

      @adhoc_album_product_item = ProductItem.create(:product_id => @adhoc_album_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                     :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                     :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                     :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                     :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

      @adhoc_album_product_item_rule_1 = ProductItemRule.create(:product_item_id => @adhoc_album_product_item.id,
                                                                :base_item_option_id => @album_base_item_option1.id,
                                                                :parent_id => 0,
                                                                :rule_type => "inventory",
                                                                :rule => "price_for_each",
                                                                :inventory_type => 'Album',
                                                                :quantity => 1,
                                                                :unlimited => 0,
                                                                :price => "49.99",
                                                                :currency => "CAD")

      @adhoc_album_product_item_rule_2 = ProductItemRule.create(:product_item_id => @adhoc_album_product_item.id,
                                                                :base_item_option_id => @album_base_item_option2.id,
                                                                :parent_id => @adhoc_album_product_item_rule_1.id,
                                                                :rule_type => "inventory",
                                                                :rule => "price_for_each",
                                                                :inventory_type => 'Song',
                                                                :model_to_check => "Album",
                                                                :method_to_check => "songs.count",
                                                                :unlimited => 1,
                                                                :quantity => 0,
                                                                :price => "0.00",
                                                                :currency => "CAD")

      @adhoc_album_product_item_rule_3 = ProductItemRule.create(:product_item_id => @adhoc_album_product_item.id,
                                                                :base_item_option_id => @album_base_item_option3.id,
                                                                :parent_id => @adhoc_album_product_item_rule_1.id,
                                                                :rule_type => "inventory",
                                                                :rule => "price_for_each",
                                                                :inventory_type => 'Salepoint',
                                                                :model_to_check => 'Album',
                                                                :method_to_check => 'salepoints.count',
                                                                :unlimited => 1,
                                                                :quantity => 0,
                                                                :price => "0.00",
                                                                :currency => "CAD")

      @adhoc_album_product_item_rule_4 = ProductItemRule.create(:product_item_id => @adhoc_album_product_item.id,
                                                                :base_item_option_id => @album_base_item_option4.id,
                                                                :parent_id => @adhoc_album_product_item_rule_1.id,
                                                                :rule_type => "inventory",
                                                                :rule => "true_false",
                                                                :inventory_type => 'Booklet',
                                                                :model_to_check => 'Album',
                                                                :method_to_check => "has_booklet?",
                                                                :unlimited => 0,
                                                                :quantity => 1,
                                                                :true_false => 1,
                                                                :price => "20.00",
                                                                :currency => "CAD")

      puts "  ---  Creating adhoc single product"
      @adhoc_single_product = Product.create(:name => "1 Year Single", :created_by_id => 1,  :country_website_id => 2, :display_name => "Single Distribution",
                                             :description => "Your single will be live for one year. Renews annually for $9.99. ", :status => "Active", :product_type => "Ad Hoc",
                                             :is_default => true, :applies_to_product => 'Single', :sort_order => 1, :renewal_level => 'Item',
                                             :renewal_product_id => 0, :price => "0.00", :currency => "CAD")

      @adhoc_single_product_item = ProductItem.create(:product_id => @adhoc_single_product.id, :base_item_id => @single_base_item.id, :name => "Single Distribution",
                                                      :description => "Distribution of a single (1 song) to customer selected stores.",
                                                      :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                      :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                      :renewal_interval => 'year', :renewal_product_id => @single_renewal_product.id)

      @adhoc_single_product_item_rule_1 = ProductItemRule.create(:product_item_id => @adhoc_single_product_item.id,
                                                                 :base_item_option_id => @single_base_item_option1.id,
                                                                 :parent_id => 0,
                                                                 :rule_type => "inventory",
                                                                 :rule => "price_for_each",
                                                                 :inventory_type => 'Single',
                                                                 :quantity => 1,
                                                                 :unlimited => 0,
                                                                 :price => "9.99",
                                                                 :currency => "CAD")

      @adhoc_single_product_item_rule_2 = ProductItemRule.create(:product_item_id => @adhoc_single_product_item.id,
                                                                 :base_item_option_id => @single_base_item_option2.id,
                                                                 :parent_id => @adhoc_single_product_item_rule_1.id,
                                                                 :rule_type => "inventory",
                                                                 :rule => "price_for_each",
                                                                 :inventory_type => 'Song',
                                                                 :model_to_check => "Single",
                                                                 :method_to_check => "songs.count",
                                                                 :quantity => 1,
                                                                 :unlimited => 0,
                                                                 :price => "0.00",
                                                                 :currency => "CAD")

      @adhoc_single_product_item_rule_3 = ProductItemRule.create(:product_item_id => @adhoc_single_product_item.id,
                                                                 :base_item_option_id => @single_base_item_option3.id,
                                                                 :parent_id => @adhoc_single_product_item_rule_1.id,
                                                                 :rule_type => "inventory",
                                                                 :rule => "price_for_each",
                                                                 :inventory_type => 'Salepoint',
                                                                 :model_to_check => 'Single',
                                                                 :method_to_check => 'salepoints.count',
                                                                 :unlimited => 1,
                                                                 :quantity => 0,
                                                                 :price => "0.00",
                                                                 :currency => "CAD")

      @adhoc_single_product_item_rule_4 = ProductItemRule.create(:product_item_id => @adhoc_single_product_item.id,
                                                                 :base_item_option_id => @single_base_item_option4.id,
                                                                 :parent_id => @adhoc_single_product_item_rule_1.id,
                                                                 :rule_type => "inventory",
                                                                 :rule => "true_false",
                                                                 :inventory_type => 'Booklet',
                                                                 :model_to_check => 'Single',
                                                                 :method_to_check => "has_booklet?",
                                                                 :unlimited => 0,
                                                                 :quantity => 0,
                                                                 :true_false => 1,
                                                                 :price => "20.00",
                                                                 :currency => "CAD")

      puts "  ---  Creating adhoc ringtone product"
      @adhoc_ringtone_product = Product.create(:name => "1 Year Ringtone", :created_by_id => 1,  :country_website_id => 2, :display_name => "Ringtone Distribution",
                                               :description => "Your ringtone will be live for one year. Renews annually for $9.99.", :status => "Active", :product_type => "Ad Hoc",
                                               :is_default => true, :applies_to_product => 'Ringtone', :sort_order => 1, :renewal_level => 'Item',
                                               :renewal_product_id => 0, :price => "9.99", :currency => "CAD")

      @adhoc_ringtone_product_item = ProductItem.create(:product_id => @adhoc_ringtone_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                        :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                        :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                        :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                        :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @adhoc_ringtone_product_item_rule_1 = ProductItemRule.create(:product_item_id => @adhoc_ringtone_product_item.id,
                                                                   :base_item_option_id => @ringtone_base_item_option1.id,
                                                                   :parent_id => 0,
                                                                   :rule_type => "inventory",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Ringtone',
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "9.99",
                                                                   :currency => "CAD")

      @adhoc_ringtone_product_item_rule_2 = ProductItemRule.create(:product_item_id => @adhoc_ringtone_product_item.id,
                                                                   :base_item_option_id => @ringtone_base_item_option2.id,
                                                                   :parent_id => @adhoc_ringtone_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Song',
                                                                   :model_to_check => "Ringtone",
                                                                   :method_to_check => "songs.count",
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @adhoc_ringtone_product_item_rule_3 = ProductItemRule.create(:product_item_id => @adhoc_ringtone_product_item.id,
                                                                   :base_item_option_id => @ringtone_base_item_option3.id,
                                                                   :parent_id => @adhoc_ringtone_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.99",
                                                                   :currency => "CAD")

      puts "CREATING DISTRIBUTION PRODUCTS"
      puts "  ---  Creating ringtone distribution credit product"
      @ringtone_credit_product = Product.create(:name => "Ringtone Distribution Credit", :created_by_id => 1,  :country_website_id => 2, :display_name => "Ringtone Distribution Credit",
                                               :description => "Your ringtone in the iPhone store. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                               :is_default => true, :applies_to_product => 'None', :sort_order => 1, :renewal_level => 'Item',
                                               :renewal_product_id => 0, :price => "9.99", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @ringtone_credit_product_item = ProductItem.create(:product_id => @ringtone_credit_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                        :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                        :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                        :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                        :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @ringtone_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option1.id,
                                                                   :parent_id => 0,
                                                                   :rule_type => "inventory",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Ringtone',
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @ringtone_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option2.id,
                                                                   :parent_id => @ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Song',
                                                                   :model_to_check => "Ringtone",
                                                                   :method_to_check => "songs.count",
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @ringtone_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option3.id,
                                                                   :parent_id => @ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @ringtone_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option4.id,
                                                                   :parent_id => @ringtone_credit_product_item_rule_3.id,
                                                                   :rule_type => "price_only",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.99",
                                                                   :currency => "CAD")

      puts "  ---  Creating 3 ringtone distribution credit product"
      @three_ringtone_credit_product = Product.create(:name => "Ringtone Distribution Credit - 3 Pack", :flag_text => 'Save 7%', :created_by_id => 1,  :country_website_id => 2, :display_name => "3 Ringtone Distribution Credits",
                                               :description => "Your ringtone in the iPhone store. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                               :is_default => true, :applies_to_product => 'None', :sort_order => 2, :renewal_level => 'Item',
                                               :renewal_product_id => 0, :price => "27.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @three_ringtone_credit_product_item = ProductItem.create(:product_id => @three_ringtone_credit_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                        :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                        :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                        :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                        :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @three_ringtone_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @three_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option1.id,
                                                                   :parent_id => 0,
                                                                   :rule_type => "inventory",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Ringtone',
                                                                   :unlimited => 0,
                                                                   :quantity => 3,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @three_ringtone_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @three_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option2.id,
                                                                   :parent_id => @three_ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Song',
                                                                   :model_to_check => "Ringtone",
                                                                   :method_to_check => "songs.count",
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @three_ringtone_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @three_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option3.id,
                                                                   :parent_id => @three_ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @three_ringtone_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @three_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option4.id,
                                                                   :parent_id => @three_ringtone_credit_product_item_rule_3.id,
                                                                   :rule_type => "price_only",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.99",
                                                                   :currency => "CAD")

      puts "  ---  Creating 5 ringtone distribution credit product"
      @five_ringtone_credit_product = Product.create(:name => "Ringtone Distribution Credit - 5 Pack", :flag_text => 'Save 10%', :created_by_id => 1,  :country_website_id => 2, :display_name => "5 Ringtone Distribution Credits",
                                               :description => "Your ringtone in the iPhone store. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                               :is_default => true, :applies_to_product => 'None', :sort_order => 3, :renewal_level => 'Item',
                                               :renewal_product_id => 0, :price => "44.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @five_ringtone_credit_product_item = ProductItem.create(:product_id => @five_ringtone_credit_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                        :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                        :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                        :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                        :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @five_ringtone_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @five_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option1.id,
                                                                   :parent_id => 0,
                                                                   :rule_type => "inventory",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Ringtone',
                                                                   :unlimited => 0,
                                                                   :quantity => 5,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @five_ringtone_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @five_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option2.id,
                                                                   :parent_id => @five_ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Song',
                                                                   :model_to_check => "Ringtone",
                                                                   :method_to_check => "songs.count",
                                                                   :unlimited => 0,
                                                                   :quantity => 1,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @five_ringtone_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @five_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option3.id,
                                                                   :parent_id => @five_ringtone_credit_product_item_rule_1.id,
                                                                   :rule_type => "inventory",
                                                                   :rule => "total_included",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.00",
                                                                   :currency => "CAD")

      @five_ringtone_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @five_ringtone_credit_product_item.id,
                                                                   :base_item_option_id => @ringtone_credit_base_item_option4.id,
                                                                   :parent_id => @five_ringtone_credit_product_item_rule_3.id,
                                                                   :rule_type => "price_only",
                                                                   :rule => "price_for_each",
                                                                   :inventory_type => 'Salepoint',
                                                                   :model_to_check => 'Ringtone',
                                                                   :method_to_check => 'salepoints.count',
                                                                   :unlimited => 1,
                                                                   :quantity => 0,
                                                                   :price => "0.99",
                                                                   :currency => "CAD")
      puts "  ---  Creating 10 ringtone distribution credit product"
      @ten_ringtone_credit_product = Product.create(:name => "Ringtone Distribution Credit - 10 Pack", :flag_text => 'Save 12%', :created_by_id => 1,  :country_website_id => 2, :display_name => "10 Ringtone Distribution Credits",
                                                    :description => "Your ringtone in the iPhone store. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 4, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "87.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @ten_ringtone_credit_product_item = ProductItem.create(:product_id => @ten_ringtone_credit_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                             :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @ten_ringtone_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @ten_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Ringtone',
                                                                        :unlimited => 0,
                                                                        :quantity => 10,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_ringtone_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @ten_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option2.id,
                                                                        :parent_id => @ten_ringtone_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Ringtone",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 0,
                                                                        :quantity => 1,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_ringtone_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @ten_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option3.id,
                                                                        :parent_id => @ten_ringtone_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Ringtone',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_ringtone_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @ten_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option4.id,
                                                                        :parent_id => @ten_ringtone_credit_product_item_rule_3.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Ringtone',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating 20 ringtone distribution credit product"
      @twenty_ringtone_credit_product = Product.create(:name => "Ringtone Distribution Credit - 20 Pack", :flag_text => 'Save 14%', :created_by_id => 1,  :country_website_id => 2, :display_name => "20 Ringtone Distribution Credits",
                                                    :description => "Your ringtone in the iPhone store. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 5, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "171.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @twenty_ringtone_credit_product_item = ProductItem.create(:product_id => @twenty_ringtone_credit_product.id, :base_item_id => @ringtone_base_item.id, :name => "Ringtone Distribution",
                                                             :description => "Distribution of a Ringtone (1 5 - 30 second sound clip) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @ringtone_renewal_product.id)

      @twenty_ringtone_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @twenty_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Ringtone',
                                                                        :unlimited => 0,
                                                                        :quantity => 20,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_ringtone_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @twenty_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option2.id,
                                                                        :parent_id => @twenty_ringtone_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Ringtone",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 0,
                                                                        :quantity => 1,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_ringtone_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @twenty_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option3.id,
                                                                        :parent_id => @twenty_ringtone_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Ringtone',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_ringtone_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @twenty_ringtone_credit_product_item.id,
                                                                        :base_item_option_id => @ringtone_credit_base_item_option4.id,
                                                                        :parent_id => @twenty_ringtone_credit_product_item_rule_3.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Ringtone',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating album distribution credit product"
      @album_credit_product = Product.create(:name => "Album Distribution Credit", :created_by_id => 1,  :country_website_id => 2, :display_name => "Album Distribution Credit",
                                                    :description => "Unlimited songs. all stores. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 1, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "49.99", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @album_credit_product_item = ProductItem.create(:product_id => @album_credit_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                             :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

      @album_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => "Album",
                                                                        :unlimited => 0,
                                                                        :quantity => 1,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @album_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option2.id,
                                                                        :parent_id => @album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Album",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @album_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option3.id,
                                                                        :parent_id => @album_credit_product_item_rule_2.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'songs.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")

      @album_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @album_credit_product_item_rule_5 = ProductItemRule.create(:product_item_id => @album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @album_credit_product_item_rule_4.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating 5 album distribution credit product"
      @five_album_credit_product = Product.create(:name => "Album Distribution Credit - 5 Pack", :flag_text => 'Save 10%', :created_by_id => 1,  :country_website_id => 2, :display_name => "5 Album Distribution Credits",
                                                    :description => "Unlimited songs. all stores. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 2, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "210.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @five_album_credit_product_item = ProductItem.create(:product_id => @five_album_credit_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                             :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

      @five_album_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @five_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => "Album",
                                                                        :unlimited => 0,
                                                                        :quantity => 5,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @five_album_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @five_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option2.id,
                                                                        :parent_id => @five_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Album",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @five_album_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @five_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option3.id,
                                                                        :parent_id => @five_album_credit_product_item_rule_2.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'songs.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")

      @five_album_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @five_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @five_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @five_album_credit_product_item_rule_5 = ProductItemRule.create(:product_item_id => @five_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @five_album_credit_product_item_rule_4.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating 10 album distribution credit product"
      @ten_album_credit_product = Product.create(:name => "Album Distribution Credit - 10 Pack", :flag_text => 'Save 12%', :created_by_id => 1,  :country_website_id => 2, :display_name => "10 Album Distribution Credits",
                                                    :description => "Unlimited songs. all stores. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 3, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "412.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @ten_album_credit_product_item = ProductItem.create(:product_id => @ten_album_credit_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                             :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

      @ten_album_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @ten_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => "Album",
                                                                        :unlimited => 0,
                                                                        :quantity => 10,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_album_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @ten_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option2.id,
                                                                        :parent_id => @ten_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Album",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_album_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @ten_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option3.id,
                                                                        :parent_id => @ten_album_credit_product_item_rule_2.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'songs.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")

      @ten_album_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @ten_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @ten_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @ten_album_credit_product_item_rule_5 = ProductItemRule.create(:product_item_id => @ten_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @ten_album_credit_product_item_rule_4.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating 20 album distribution credit product"
      @twenty_album_credit_product = Product.create(:name => "Album Distribution Credit - 20 Pack", :flag_text => 'Save 14%', :created_by_id => 1,  :country_website_id => 2, :display_name => "20 Album Distribution Credits",
                                                    :description => "Unlimited songs. all stores. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 4, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "807.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @twenty_album_credit_product_item = ProductItem.create(:product_id => @twenty_album_credit_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                             :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

      @twenty_album_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @twenty_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option1.id,
                                                                        :parent_id => 0,
                                                                        :rule_type => "inventory",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => "Album",
                                                                        :unlimited => 0,
                                                                        :quantity => 20,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_album_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @twenty_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option2.id,
                                                                        :parent_id => @twenty_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => "Album",
                                                                        :method_to_check => "songs.count",
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_album_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @twenty_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option3.id,
                                                                        :parent_id => @twenty_album_credit_product_item_rule_2.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Song',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'songs.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")

      @twenty_album_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @twenty_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @twenty_album_credit_product_item_rule_1.id,
                                                                        :rule_type => "inventory",
                                                                        :rule => "total_included",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.00",
                                                                        :currency => "CAD")

      @twenty_album_credit_product_item_rule_5 = ProductItemRule.create(:product_item_id => @twenty_album_credit_product_item.id,
                                                                        :base_item_option_id => @album_credit_base_item_option4.id,
                                                                        :parent_id => @twenty_album_credit_product_item_rule_4.id,
                                                                        :rule_type => "price_only",
                                                                        :rule => "price_for_each",
                                                                        :inventory_type => 'Salepoint',
                                                                        :model_to_check => 'Album',
                                                                        :method_to_check => 'salepoints.count',
                                                                        :unlimited => 1,
                                                                        :quantity => 0,
                                                                        :price => "0.99",
                                                                        :currency => "CAD")
      puts "  ---  Creating single distribution credit product"
      @single_credit_product = Product.create(:name => "Single Distribution Credit", :created_by_id => 1,  :country_website_id => 2, :display_name => "Single Distribution Credit",
                                                    :description => "All stores included. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 1, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "9.99", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @single_credit_product_item = ProductItem.create(:product_id => @single_credit_product.id, :base_item_id => @single_base_item.id, :name => "Single Distribution",
                                                             :description => "Distribution of a single (1 song) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @single_renewal_product.id)

      @single_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option1.id,
                                                                  :parent_id => 0,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => "Single",
                                                                  :unlimited => 0,
                                                                  :quantity => 1,
                                                                  :price => "9.99",
                                                                  :currency => "CAD")

      @single_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option2.id,
                                                                  :parent_id => @single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Song',
                                                                  :model_to_check => "Single",
                                                                  :method_to_check => "songs.count",
                                                                  :unlimited => 0,
                                                                  :quantity => 1,
                                                                  :price => "0.00",
                                                                  :currency => "CAD")

      @single_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option3.id,
                                                                  :parent_id => @single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Salepoint',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'salepoints.count',
                                                                  :unlimited => 1,
                                                                  :quantity => 0,
                                                                  :price => "0.99",
                                                                  :currency => "CAD")

      @single_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option4.id,
                                                                  :parent_id => @single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "true_false",
                                                                  :inventory_type => 'Booklet',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'has_booklet?',
                                                                  :unlimited => 0,
                                                                  :quantity => 0,
                                                                  :true_false => 1,
                                                                  :price => "20.00",
                                                                  :currency => "CAD")
      puts "  ---  Creating 5 single distribution credit product"
      @five_single_credit_product = Product.create(:name => "Single Distribution Credit - 5 Pack", :flag_text => 'Save 10%', :created_by_id => 1,  :country_website_id => 2, :display_name => "5 Single Distribution Credits",
                                                    :description => "All stores included. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 2, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "44.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @five_single_credit_product_item = ProductItem.create(:product_id => @five_single_credit_product.id, :base_item_id => @single_base_item.id, :name => "Single Distribution",
                                                             :description => "Distribution of a single (1 song) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @single_renewal_product.id)

      @five_single_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @five_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option1.id,
                                                                  :parent_id => 0,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => "Single",
                                                                  :unlimited => 0,
                                                                  :quantity => 5,
                                                                  :price => "9.99",
                                                                  :currency => "CAD")

      @five_single_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @five_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option2.id,
                                                                  :parent_id => @five_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Song',
                                                                  :model_to_check => "Single",
                                                                  :method_to_check => "songs.count",
                                                                  :unlimited => 0,
                                                                  :quantity => 1,
                                                                  :price => "0.00",
                                                                  :currency => "CAD")

      @five_single_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @five_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option3.id,
                                                                  :parent_id => @five_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Salepoint',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'salepoints.count',
                                                                  :unlimited => 1,
                                                                  :quantity => 0,
                                                                  :price => "0.99",
                                                                  :currency => "CAD")

      @five_single_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @five_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option4.id,
                                                                  :parent_id => @five_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "true_false",
                                                                  :inventory_type => 'Booklet',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'has_booklet?',
                                                                  :unlimited => 0,
                                                                  :quantity => 0,
                                                                  :true_false => 1,
                                                                  :price => "20.00",
                                                                  :currency => "CAD")

      puts "  ---  Creating 10 single distribution credit product"
      @ten_single_credit_product = Product.create(:name => "Single Distribution Credit - 10 Pack", :flag_text => 'Save 12%', :created_by_id => 1,  :country_website_id => 2, :display_name => "10 Single Distribution Credits",
                                                    :description => "All stores included. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 3, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "87.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @ten_single_credit_product_item = ProductItem.create(:product_id => @ten_single_credit_product.id, :base_item_id => @single_base_item.id, :name => "Single Distribution",
                                                             :description => "Distribution of a single (1 song) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @single_renewal_product.id)

      @ten_single_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @ten_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option1.id,
                                                                  :parent_id => 0,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => "Single",
                                                                  :unlimited => 0,
                                                                  :quantity => 10,
                                                                  :price => "9.99",
                                                                  :currency => "CAD")

      @ten_single_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @ten_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option2.id,
                                                                  :parent_id => @ten_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Song',
                                                                  :model_to_check => "Single",
                                                                  :method_to_check => "songs.count",
                                                                  :unlimited => 0,
                                                                  :quantity => 1,
                                                                  :price => "0.00",
                                                                  :currency => "CAD")

      @ten_single_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @ten_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option3.id,
                                                                  :parent_id => @ten_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Salepoint',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'salepoints.count',
                                                                  :unlimited => 1,
                                                                  :quantity => 0,
                                                                  :price => "0.99",
                                                                  :currency => "CAD")

      @ten_single_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @ten_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option4.id,
                                                                  :parent_id => @ten_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "true_false",
                                                                  :inventory_type => 'Booklet',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'has_booklet?',
                                                                  :unlimited => 0,
                                                                  :quantity => 0,
                                                                  :true_false => 1,
                                                                  :price => "20.00",
                                                                  :currency => "CAD")

      puts "  ---  Creating 20 single distribution credit product"
      @twenty_single_credit_product = Product.create(:name => "Single Distribution Credit - 20 Pack", :flag_text => 'Save 14%', :created_by_id => 1,  :country_website_id => 2, :display_name => "20 Single Distribution Credits",
                                                    :description => "All stores included. Distribute whenever you want.", :status => "Active", :product_type => "Package",
                                                    :is_default => true, :applies_to_product => 'None', :sort_order => 4, :renewal_level => 'Item',
                                                    :renewal_product_id => 0, :price => "171.95", :currency => "CAD", :first_renewal_duration => 0, :renewal_duration => 0)

      @twenty_single_credit_product_item = ProductItem.create(:product_id => @twenty_single_credit_product.id, :base_item_id => @single_base_item.id, :name => "Single Distribution",
                                                             :description => "Distribution of a single (1 song) to customer selected stores.",
                                                             :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                             :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                             :renewal_interval => 'year', :renewal_product_id => @single_renewal_product.id)

      @twenty_single_credit_product_item_rule_1 = ProductItemRule.create(:product_item_id => @twenty_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option1.id,
                                                                  :parent_id => 0,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => "Single",
                                                                  :unlimited => 0,
                                                                  :quantity => 20,
                                                                  :price => "9.99",
                                                                  :currency => "CAD")

      @twenty_single_credit_product_item_rule_2 = ProductItemRule.create(:product_item_id => @twenty_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option2.id,
                                                                  :parent_id => @twenty_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Song',
                                                                  :model_to_check => "Single",
                                                                  :method_to_check => "songs.count",
                                                                  :unlimited => 0,
                                                                  :quantity => 1,
                                                                  :price => "0.00",
                                                                  :currency => "CAD")

      @twenty_single_credit_product_item_rule_3 = ProductItemRule.create(:product_item_id => @twenty_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option3.id,
                                                                  :parent_id => @twenty_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Salepoint',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'salepoints.count',
                                                                  :unlimited => 1,
                                                                  :quantity => 0,
                                                                  :price => "0.99",
                                                                  :currency => "CAD")

      @twenty_single_credit_product_item_rule_4 = ProductItemRule.create(:product_item_id => @ten_single_credit_product_item.id,
                                                                  :base_item_option_id => @single_credit_base_item_option4.id,
                                                                  :parent_id => @twenty_single_credit_product_item_rule_1.id,
                                                                  :rule_type => "inventory",
                                                                  :rule => "true_false",
                                                                  :inventory_type => 'Booklet',
                                                                  :model_to_check => 'Single',
                                                                  :method_to_check => 'has_booklet?',
                                                                  :unlimited => 0,
                                                                  :quantity => 0,
                                                                  :true_false => 1,
                                                                  :price => "20.00",
                                                                  :currency => "CAD")
    end
  end
end

task :create_ca_add_store => :environment do
  ActiveRecord::Base.transaction do

    if Product.exists?(:currency => "CAD", :name => "Added Store")
      puts "Canadian Added store already created - skipping migration"
      return
    else
      puts "CREATING BASE ITEMS"
      puts "  ---  Creating Album Distribution Base Items"
      @album_base_item = BaseItem.create(:name => "Album Distribution", :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                         :first_renewal_duration => 4, :first_renewal_interval => "week", :renewal_duration => 4, :renewal_interval => 'week',
                                         :price => "0.00", :currency => "CAD")

       puts "CREATING BASE ITEM OPTIONS"
       puts "  ---  Creating Album Distribution Base Item Options"
       @album_base_item_option1 = BaseItemOption.create(:base_item_id => @album_base_item.id,
                                                        :sort_order => 1,
                                                        :name => 'Album Distribution',
                                                        :product_type => 'Ad Hoc',
                                                        :option_type => 'required',
                                                        :rule_type => 'inventory',
                                                        :rule => 'price_for_each',
                                                        :inventory_type => 'Album',
                                                        :quantity => 1,
                                                        :price => "19.98",
                                                        :currency => "CAD")
        puts "CREATING AD HOC PRODUCTS"
        puts "  ---  Creating adhoc album product"
        @adhoc_album_product = Product.create(:name => "1 Year Album", :created_by_id => 1,  :country_website_id => 2, :display_name => "Album Distribution",
                                              :description => "Your album will be live for one year. Renews annually for $49.99.", :status => "Active", :product_type => "Ad Hoc",
                                              :is_default => true, :applies_to_product => 'Album', :sort_order => 1, :renewal_level => 'Item',
                                              :renewal_product_id => 0, :price => "0.00", :currency => "CAD")

        @adhoc_album_product_item = ProductItem.create(:product_id => @adhoc_album_product.id, :base_item_id => @album_base_item.id, :name => "Album Distribution",
                                                       :description => "Distribution of a full album (2 or more songs) to customer selected stores.",
                                                       :price => "0.00", :currency => "CAD", :does_not_renew => 0, :renewal_type => 'distribution',
                                                       :first_renewal_duration => 1, :first_renewal_interval => 'year', :renewal_duration => 1,
                                                       :renewal_interval => 'year', :renewal_product_id => @album_renewal_product.id)

        @adhoc_album_product_item_rule_1 = ProductItemRule.create(:product_item_id => @adhoc_album_product_item.id,
                                                                  :base_item_option_id => @album_base_item_option1.id,
                                                                  :parent_id => 0,
                                                                  :rule_type => "inventory",
                                                                  :rule => "price_for_each",
                                                                  :inventory_type => 'Album',
                                                                  :quantity => 1,
                                                                  :unlimited => 0,
                                                                  :price => "49.99",
                                                                  :currency => "CAD")

    end
  end
end
