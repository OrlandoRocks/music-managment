namespace :backfill_ringtone do
  task set_golive_date_and_release_datetime_utc: :environment do

    Ringtone.where(golive_date: nil).or(
      Ringtone.where(release_datetime_utc: nil)
    ).where.not(sale_date: nil)
    .find_in_batches do |ringtones|

      ringtones.each do |ringtone|
        sale_datetime = 
          ringtone.sale_date
            &.to_datetime
            &.in_time_zone('Eastern Time (US & Canada)')
            &.beginning_of_day

        ringtone.golive_date =
          sale_datetime&.strftime("%Y-%m-%d %H:%M:%S")
        
        ringtone.release_datetime_utc =
          sale_datetime&.in_time_zone('UTC')
            &.strftime("%Y-%m-%d %H:%M:%S")
        
        ringtone.save
      end
  
    end

  end
end
