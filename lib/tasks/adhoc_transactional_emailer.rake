namespace :adhoc_transactional_emailer do
  desc "allows for delivery of adhoc emails using homegrown templates not in the codebase."
  task deliver_from_csv: :environment do
    subject = ENV["SUBJECT"] || "Notification From TuneCore, Inc."
    AdhocTransactionalMailer.deliver_multiple_from_csv(CSV.read(ENV["CSV_FILE_PATH"]), File.read(ENV["TEMPLATE_FILE_PATH"]), { subject: subject })
  end
end
