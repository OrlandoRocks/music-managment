namespace :store_creditcard_address_in_braintree_for_existing_tcs_pro_users do
  task store_cc_address_to_bt: :environment do
    store_cc_address_logger
    tcs_pro_users = Person.joins(:person_subscription_statuses)
                          .where.not(person_subscription_statuses: { effective_date: nil })
                          .where('person_subscription_statuses.termination_date > ?', Time.now)
                          .where(person_subscription_statuses: { subscription_type: 'Social' })
                          .or(Person.joins(:person_subscription_statuses)
                                    .where.not(person_subscription_statuses: { effective_date: nil })
                                    .where(person_subscription_statuses: { termination_date: nil, subscription_type: 'Social' }))
                          .uniq
    @cc_address_logger.info "TCS pro users count: #{tcs_pro_users.count}"
    tcs_pro_users.each do |person|
      @cc_address_logger.info "User: id- #{person.id} email- #{person.email}"
      stored_credit_cards = person.stored_credit_cards
      stored_credit_cards.each do |card|
        next if card.bt_token.nil?

        payment_method = nil
        payment_method = Braintree::PaymentMethod.find(card.bt_token)
        next if payment_method.nil?

        payment_method_billing_info = payment_method.billing_address
        next if payment_method_billing_info.present?

        Braintree::PaymentMethod.update(
          card.bt_token,
          billing_address: {
            street_address: card.address1,
            extended_address: card.address2,
            locality: card.city,
            region: card.state,
            postal_code: card.zip,
            country_code_alpha2: card.country,
            first_name: card.first_name,
            last_name: card.last_name,
            options: {
              update_existing: true
            }
          }
        )
      end
    rescue StandardError => e
      @cc_address_logger.info "error: #{e.message}"
    end
  end

  def store_cc_address_logger
    @cc_address_logger = Logger.new('log/store_cc_address_logger.log')
    @cc_address_logger.level = Logger::INFO
  end
end
