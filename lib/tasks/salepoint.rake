

namespace :salepoint do
  desc "Backfill missing Simfy Africa salepoints"
  task :backfill_missing_simfy_africa_salepoints => :environment do |t|
    log(t, "Beginning Backfill")
    store = Store.find_by(short_name: "SimfyAF")
    albums = Album.find_by_sql(["select distinct albums.* from distributions
                               inner join petri_bundles on distributions.petri_bundle_id = petri_bundles.id
                               inner join albums on albums.id = petri_bundles.album_id
                               left outer join salepoints on albums.id = salepoints.salepointable_id and salepoints.salepointable_type = 'Album' and salepoints.store_id = ?
                               where converter_class = 'MusicStores::SimfyAfrica::Converter' and salepoints.id is null and petri_bundles.state <> 'dismissed'", store.id])

    log(t, "Backfilling #{albums.count} album's Simfy Africa Salepoint")

    albums.each do |album|
      salepoint = album.salepoints.create(store: store)
      salepoint.update_post_purchase
      distro = album.petri_bundle.distributions.where(converter_class: "MusicStores::SimfyAfrica::Converter").first
      distro.salepoints << salepoint
    end

    log(t, "Task Complete")
  end

  desc "Fix paid salepoints marked as unpaid"
  task fix_unpaid: :environment do |t|
    log(t, "Beginning fix")

    limit = ENV["LIMIT"] || 1000
    fixed = 0

    salepoints = Salepoint.where("finalized_at is not null and payment_applied = 0 and salepointable_type = 'Album'").limit(limit)
    CSV.open("salepoint_marked_unpaid_failures #{Time.now}", "wb") do |csv|
      salepoints.each do |salepoint|
        if salepoint.update(payment_applied: true)
          fixed += 1
        else
          csv << salepoint.errors.full_messages.unshift(salepoint.id.to_s)
        end

        purchase = Purchase.where("paid_at is null and related_type = 'salepoint' and related_id = #{salepoint.id}").first
        # delete not destroy so it won't attempt to destroy related salepoint
        purchase.delete if purchase
      end
    end

    log(t, "The fix is in. #{fixed} salepoints fixed.")
  end

  desc "Fix unfinalized Facebook salepoints that should be finalized"
  task fix_unfinalized_facebook: :environment do |t|
    log(t, "Beginning fix")

    salepoints = Salepoint.find_by_sql("select s.* from salepoints s inner join albums a on s.salepointable_id = a.id and s.salepointable_type = 'Album' inner join facebook_recognition_service_purchases f on f.person_id = a.person_id where s.store_id = #{Store.find_by(short_name: 'Facebook').id} and s.finalized_at is null and a.finalized_at is not null")

    salepoints.each do |salepoint|
      salepoint.update(finalized_at: Time.now, payment_applied: true)
    end

    log(t, "The fix is in")
  end

  desc "Fix unpaid, unfinalized salepoints that were actually purchased and rebundle related album"
  task fix_purchased: :environment do |t|
    log(t, "Beginning fix")

    salepoints = Salepoint.find_by_sql("select s.*, p.paid_at from salepoints s inner join purchases p on p.related_id = s.id and p.related_type = 'Salepoint' inner join albums a on s.salepointable_id = a.id and s.salepointable_type = 'Album' where p.paid_at is not null and s.payment_applied = 0 and s.finalized_at is null and a.is_deleted = 0 and a.takedown_at is null and a.finalized_at is not null;")
    salepoints.each do |salepoint|
      salepoint.update(finalized_at: salepoint.paid_at, payment_applied: true)
    end

    log(t, "The fix is in")
  end
end
