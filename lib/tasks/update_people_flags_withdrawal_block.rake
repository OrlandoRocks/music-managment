namespace :update do
  desc "Removes duplicate entry from flags table"
  task remove_duplicate_flags_entries: :environment do
    Rails.logger.info 'Started fixing people_flags data issue'
    PeopleFlag.joins("left outer join flags on flags.id = people_flags.flag_id where flags.id IS null").delete_all
    Flag.where(category: Flag::WITHDRAWAL[:category], name: Flag::WITHDRAWAL[:name]).where.not(id: 1).delete_all
    PeopleFlag.where.not(flag_id: 1).update_all(flag_id: 1)
    FlagTransition.where.not(flag_id: 1).update_all(flag_id: 1)
    Rails.logger.info 'Completed fixing people_flags data issue'
  end
end
