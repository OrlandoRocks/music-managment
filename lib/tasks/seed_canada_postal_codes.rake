require 'active_record'

namespace(:db) do

  task :seed_canada_postal_codes => [:environment] do
    #  Load all seed files 
    require 'lib/seed/seed.rb'
    Dir['lib/seed/*.rb'].each { |file| require file }

    CanadaPostalCodeSeed.fill

  end

end
