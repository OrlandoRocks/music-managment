#frozen_string_literal: true

namespace :vat do
  desc "Add corporate entity to existing users"
  task add_corporate_entity: :environment do
    puts "Started"
    CorporateEntity.create([{ name: "TuneCore US" }, { name: "BI Luxembourg" }]) unless CorporateEntity.any?
    entity_list = CorporateEntity.pluck(:name, :id).to_h
    sql = "UPDATE people SET corporate_entity_id = CASE WHEN country IN ('US', 'United States') THEN #{entity_list["TuneCore US"]} ELSE #{entity_list["BI Luxembourg"]} END"
    ActiveRecord::Base.connection.execute(sql)
    puts "Completed"
  end

  task add_corporate_entity_attributes: :environment do
    puts "Started"
    if CorporateEntity.any?
      CorporateEntity.where(name: "BI Luxembourg")
                     .update(address1: "Believe International, an affiliated company of TuneCore",
                             address2: "24 rue Beaumont",
                             city: "Grand-Duché du Luxembourg",
                             currency: "EUR",
                             postal_code: "L-1219",
                             country: "LUXEMBOURG",
                             crn: "B230194",
                             vat_registration_number: "LU30940137")
      CorporateEntity.where(name: "TuneCore US")
                     .update(address1: "TuneCore, Inc",
                             address2: "63 Pearl Street, Box #256",
                             city: "Brooklyn",
                             currency: "USD",
                             postal_code: "NY 11201")
    end
    puts "Completed"
  end

  desc "add corporate_entities to countries"
  task map_countries_to_corporate_entities: :environment do
    CorporateEntityMappingService.new(ENV.fetch('COUNTRY_ENTITY_MAPPING', nil)).map_to_countries
  end

  task update_us_territories_corporate_entity: :environment do
    puts "Started"
    # This is to ensure corporate entity is added
    CorporateEntity.create([{ name: "TuneCore US" }, { name: "BI Luxembourg" }]) if CorporateEntity.none?
    entity_list = CorporateEntity.pluck(:name, :id).to_h
    query = <<-SQL
              UPDATE people SET corporate_entity_id =
              CASE
              WHEN country IN (#{TUNECORE_US_COUNTRIES.map(&:inspect).join(', ')})
              THEN #{entity_list["TuneCore US"]}
              ELSE #{entity_list["BI Luxembourg"]}
              END
    SQL
    ActiveRecord::Base.connection.execute(query)
  end

  task updated_corporate_entity_attributes: :environment do
    puts "Started"
    return unless CorporateEntity.any?

    CorporateEntity.where(name: "BI Luxembourg")
                   .update(address1: "Believe International",
                           address2: "5 Place de la Gare, Bureau 601",
                           city: "",
                           currency: "EUR",
                           postal_code: "L-1616",
                           country: "Luxembourg ",
                           crn: "B230194",
                           vat_registration_number: "LU30940137",
                           paying_bank: "SOCIETE GENERALE BANK & TRUST",
                           rib: "0613832222600EUR",
                           bic: "SGABLULL",
                           iban: "LU53 0613 8322 2260 0EUR")
    CorporateEntity.where(name: "TuneCore US")
                   .update(address1: "TuneCore, Inc",
                           address2: "63 Pearl Street, Box #256",
                           city: "Brooklyn",
                           currency: "USD",
                           postal_code: "NY 11201")
    puts "Completed"
  end

  task add_corporate_entity_invoice_prefix: :environment do
    puts "Started"
    if CorporateEntity.any?
      CorporateEntity.where(name: "BI Luxembourg")
                     .update(inbound_invoice_prefix: "BI-INV")
      CorporateEntity.where(name: "TuneCore US")
                     .update(inbound_invoice_prefix: "TC-INV")
    end
    puts "Completed"
  end
end
