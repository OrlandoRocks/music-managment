namespace :ops_support do
  task all_the_things: [
    :clear_dead_distros,
    :clear_dead_pub_admin_jobs,
    :ytsr_person_backfill,
    :ytsr_salepoint_backfill,
    :find_duplicate_invoices,
    :fix_shoulda_been_paid_salepoints
  ]

  task clear_dead_distros: :environment do
    dead_set = Sidekiq::DeadSet.new

    dead_set.each do |job|
      next unless ['Distribution', 'DistributionSong', 'TrackMonetization'].include?(job.args[0])
      klass = job.args[0]
      d_id = job.args[1]
      distro = klass.constantize.where(id: d_id).first
      next if distro.nil?
      if ['delivered', 'new', 'blocked', 'pending_approval', 'dismissed'].include?(distro.state)
        job.delete
      end
    end
  end

  task clear_dead_pub_admin_jobs: :environment do
    PublishingAdministration::CleanupSidekiqQueuesService.cleanup
  end

  task ytsr_person_backfill: :environment do
  end

  task ytsr_salepoint_backfill: :environment do
  end

  task find_duplicate_invoices: :environment do
    tmp_path = File.join(Rails.root, "tmp")

    credit_cards = ActiveRecord::Base.connection.execute("
      SELECT i2.*
      FROM (
      SELECT i.person_id, DATE_FORMAT(i.settled_at, '%Y-%m-%d') AS settled_at, count(1) AS count FROM invoices i
      INNER JOIN purchases p ON p.invoice_id = i.id
      INNER JOIN braintree_transactions bt ON bt.invoice_id = i.id
      WHERE i.final_settlement_amount_cents > 0
      AND i.settled_at IS NOT NULL
      AND bt.action != 'refund'
      AND bt.refunded_amount IS NULL
      AND p.product_id = 402
      GROUP BY 1, 2 HAVING count > 1
      ) AS i1
      INNER JOIN invoices i2 ON i1.person_id = i2.person_id
      INNER JOIN braintree_transactions bt ON bt.invoice_id = i2.id
      INNER JOIN purchases p ON p.invoice_id = i2.id
      WHERE i2.final_settlement_amount_cents > 0
      AND i2.settled_at IS NOT NULL
      AND bt.action != 'refund'
      AND bt.refunded_amount IS NULL
      AND bt.status != 'declined'
      AND p.product_id = 402
      AND i1.settled_at = DATE_FORMAT(i2.settled_at, '%Y-%m-%d')
      AND i2.settled_at > '#{2.days.ago}'
      GROUP BY p.invoice_id
      HAVING count(p.invoice_id) = 1
      ORDER BY i2.created_at DESC;"
    )

    person_balances = ActiveRecord::Base.connection.execute("
      SELECT DISTINCT i2.*
      FROM (
      SELECT i.person_id, DATE_FORMAT(i.settled_at, '%Y-%m-%d') AS settled_at, count(1) AS count FROM invoices i
      INNER JOIN purchases p ON p.invoice_id = i.id
      INNER JOIN person_transactions pt ON pt.target_id = i.id
      WHERE i.final_settlement_amount_cents > 0
      AND i.settled_at IS NOT NULL
      AND pt.target_type = 'Invoice'
      AND p.product_id = 402
      GROUP BY 1, 2 HAVING count > 1
      ) AS i1
      INNER JOIN invoices i2 ON i1.person_id = i2.person_id
      INNER JOIN person_transactions pt ON pt.target_id = i2.id
      INNER JOIN purchases p ON p.invoice_id = i2.id
      WHERE i2.final_settlement_amount_cents > 0
      AND pt.target_type = 'Invoice'
      AND i2.settled_at IS NOT NULL
      AND i2.settled_at > '2018-03-01'
      AND p.product_id = 402
      AND i1.settled_at = DATE_FORMAT(i2.settled_at, '%Y-%m-%d')
      AND i2.settled_at > '#{2.days.ago}'
      GROUP BY p.invoice_id
      HAVING count(p.invoice_id) = 1
      ORDER BY i2.person_id;"
    )

    paypal_invoices = ActiveRecord::Base.connection.execute("
      SELECT DISTINCT i2.*
      FROM (
      SELECT i.person_id, DATE_FORMAT(i.settled_at, '%Y-%m-%d') AS settled_at, count(1) AS count FROM invoices i
      INNER JOIN purchases p ON p.invoice_id = i.id
      INNER JOIN paypal_transactions pt ON pt.invoice_id = i.id
      WHERE i.final_settlement_amount_cents > 0
      AND i.settled_at IS NOT NULL
      AND p.product_id = 402
      GROUP BY 1, 2 HAVING count > 1
      ) AS i1
      INNER JOIN invoices i2 ON i1.person_id = i2.person_id
      INNER JOIN paypal_transactions pt ON pt.invoice_id = i2.id
      INNER JOIN purchases p ON p.invoice_id = i2.id
      WHERE i2.final_settlement_amount_cents > 0
      AND i2.settled_at IS NOT NULL
      AND p.product_id = 402
      AND i1.settled_at = DATE_FORMAT(i2.settled_at, '%Y-%m-%d')
      AND i2.settled_at > '#{2.days.ago}'
      GROUP BY p.invoice_id
      HAVING count(p.invoice_id) = 1
      ORDER BY i2.person_id;"
    )

    [credit_cards, person_balances, paypal_invoices].each do |invoices|
      if invoices.count > 0
        CSV.open("#{tmp_path}/duplicate_balance_invoices_#{Date.today.strftime('%m_%e_%Y')}.csv", "w") do |csv|
          csv << invoices.fields
          invoices.each do |i|
            csv << i
          end

        end
      end
    end
  end

  task fix_shoulda_been_paid_salepoints: :environment do
    query = <<-SQL.strip_heredoc
      SELECT DISTINCT salepoints.id
      FROM purchases
        INNER JOIN inventories ON inventories.purchase_id = purchases.id
        INNER JOIN inventory_usages ON inventory_usages.inventory_id = inventories.id
        INNER JOIN salepoints ON salepoints.id = inventory_usages.related_id AND inventory_usages.related_type = 'Salepoint'
        LEFT OUTER JOIN distributions_salepoints ON distributions_salepoints.salepoint_id = salepoints.id
      WHERE purchases.related_type = 'Album'
        AND inventories.inventory_type = 'Salepoint'
        AND salepoints.payment_applied = 0
        AND purchases.paid_at > '2019-01-01'
        AND salepoints.store_id NOT IN (18,44,75)
        AND distributions_salepoints.distribution_id IS NULL
      ORDER BY purchases.paid_at;
    SQL

    ActiveRecord::Base.connection.execute(query).to_a.flatten.each do |salepoint_id|
      salepoint = Salepoint.find(salepoint_id)
      salepoint.paid_post_proc
      next if salepoint.ytsr_proxy_salepoint?
      DistributionCreator.create(salepoint.salepointable, salepoint.store.short_name)
    end
  end

  task catch_and_fix_renewals_susceptible_to_multiple_payments: :environment do
    query = <<-SQL
      SELECT DISTINCT ri1.related_id AS album_id FROM renewal_items ri1
      INNER JOIN renewal_items ri2 ON ri1.related_id = ri2.related_id AND ri1.related_type = 'Album' AND ri2.related_type = 'Album' AND ri1.id != ri2.id
      INNER JOIN albums ON ri1.related_id = albums.id
      WHERE ri2.created_at < DATE_ADD(ri1.created_at, INTERVAL 5 MINUTE)
      AND albums.takedown_at IS NULL
      GROUP BY ri1.related_id ORDER BY albums.created_at;
    SQL

    album_ids = ActiveRecord::Base.connection.execute(query).to_a.flatten
    not_yet_paid_renewal_albums = []

    album_ids.each do |album_id|
      histories = RenewalHistory.joins(renewal: :renewal_items).includes(:purchase).where(renewal_items: {
      related_id: album_id, related_type: "Album"
      })
      unless histories.map(&:purchase).any? { |purchase| purchase.related_type == 'Renewal' }
        not_yet_paid_renewal_albums << album_id
      end
    end
    not_yet_paid_renewal_albums.each do |album_id|
      renewal_items = RenewalItem.where(related_id: album_id, related_type: "Album")
      return if renewal_items.count <= 1
      good_renewal_item = renewal_items.last
      bad_renewal_items = renewal_items.where("id != ?", good_renewal_item.id)
      bad_renewals = bad_renewal_items.map(&:renewal)
      bad_renewal_histories = bad_renewals.flat_map(&:renewal_history)

      ActiveRecord::Base.transaction do
        bad_renewal_histories.each(&:delete)
        bad_renewal_items.each(&:delete)
        bad_renewals.each(&:delete)
      end
    end
  end

  task remove_duplicated_distributions_salepoints: :environment do
    OpsSupport::RemoveDuplicatedDistributionsSalepointsService.new.remove!
  end
end
