namespace :tunecore do
  namespace :checkpayment do

    # TODO: check if this is still being used, need to look for rake task in cronjobs
    # =>    if not, then gut this and the mailer
    desc "export pending check payments"
    task :email => :environment do
      export = CheckPayment::Export.new
      export.run
      unless export.empty?
        CheckTransferExportMailer.check_export(export, CheckPayment::Export::EMAILS).deliver
        puts "exported #{export.size} check(s) for a total of #{export.total_string} to #{export.filename}"
      else
        puts "no checks to export"
      end
    end
  end
end
