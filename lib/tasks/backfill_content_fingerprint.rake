namespace :fingerprint do
  desc "back fill content fingerprints for finalized albums"
  task backfill_content_fingerprints: :environment do
    start_date = ENV["START_DATE"] || Time.zone.today - 10
    batch_size = ENV["BATCH_SIZE"] || 500

    # Some songs have a hash of an empty file: 'd41d8cd98f00b204e9800998ecf8427e'
    Song.joins(album: :distributions)
        .where(albums: { finalized_at: start_date.. })
        .where(distributions: {state: 'delivered'})
        .where("songs.content_fingerprint IS NULL OR songs.content_fingerprint = 'd41d8cd98f00b204e9800998ecf8427e'")
        .select('songs.id')
        .distinct
        .find_in_batches(batch_size: batch_size.to_i).with_index do |batch, batch_no|
          ContentFingerprintingWorker.perform_async(batch.pluck(:id))

          Rails.logger.info("Dispatched batch #{batch_no}")
        end
  end

  task songs_missing_content_fingerprints: :environment do
    year = ENV.fetch('YEAR')
    album_ids = Album.where.not(finalized_at: nil)
                .where("YEAR(finalized_at) > ?", year)
                .joins(:songs)
                .where(songs: { content_fingerprint: nil }).pluck(:id)
    eneuque_flag_worker(album_ids)
  end

  task flag_albums_with_existing_md5: :environment do
    limit = ENV.fetch("LIMIT")
    md5_csv_key = ENV.fetch("MD5_KEY")
    exlcuded_albums_key = ENV.fetch("MD5_ALBUM_KEY")
    md5_hashes = CSV.read(RakeDatafileService.download(md5_csv_key).local_file).flatten!
    exlcuded_album_ids = CSV.read(RakeDatafileService.download(exlcuded_albums_key).local_file).flatten.map(&:to_i)
    songs_not_flagged = Song.where(content_fingerprint: md5_hashes).limit(limit)
    albums_ids_to_flag = songs_not_flagged.pluck(:album_id) - exlcuded_album_ids
    remaining_unflagged_md5s = md5_hashes - songs_not_flagged.pluck(:content_fingerprint)

    eneuque_flag_worker(albums_ids_to_flag)
    filepath = File.join(Rails.root, "tmp/", md5_csv_key)
    CSV.open(filepath, "w") do |file|
      remaining_unflagged_md5s.each { |md5| file << [md5] }
    end
    RakeDatafileService.upload(filepath, overwrite: 'y')
  end

  def eneuque_flag_worker(album_ids)
    album_ids.each do |id|
      FlagDuplicateSongsWorker.set(queue: 'default').perform_async(id)
    end
  end
end
