
TIMES = 100

namespace :benchmark do

  task :upload_select => :environment do

    query = "SELECT albums.`id` AS t0_r0, albums.`name` AS t0_r1, albums.`title_version` AS t0_r2, albums.`p_copyright_year` AS t0_r3, albums.`c_copyright_year` AS t0_r4, albums.`p_copyright` AS t0_r5, albums.`c_copyright` AS t0_r6, albums.`price_id` AS t0_r7, albums.`sale_date` AS t0_r8, albums.`orig_release_year` AS t0_r9, albums.`recording_location` AS t0_r10, albums.`liner_notes` AS t0_r11, albums.`created_on` AS t0_r12, albums.`album_state` AS t0_r13, albums.`is_various` AS t0_r14, albums.`artist_id` AS t0_r15, albums.`label_id` AS t0_r16, albums.`person_id` AS t0_r17, albums.`approval_date` AS t0_r18, albums.`is_deleted` AS t0_r19, albums.`deleted_date` AS t0_r20, albums.`golive_date` AS t0_r21, albums.`is_not_uploading` AS t0_r22, albums.`paid_idea` AS t0_r23, albums.`updated_at` AS t0_r24, albums.`legacy_annual_fee` AS t0_r25, albums.`temp_person` AS t0_r26, albums.`price_policy_id` AS t0_r27, albums.`payment_applied` AS t0_r28, albums.`finalized_at` AS t0_r29, albums.`invoice_id` AS t0_r30, albums.`locked_by_id` AS t0_r31, albums.`locked_by_type` AS t0_r32, albums.`allow_different_format` AS t0_r33, albums.`purchase_id` AS t0_r34, albums.`display_on_page` AS t0_r35, albums.`primary_genre_id` AS t0_r36, albums.`secondary_genre_id` AS t0_r37, albums.`takedown_at` AS t0_r38, albums.`known_live_on` AS t0_r39, albums.`apple_identifier` AS t0_r40, albums.`price_override` AS t0_r41, albums.`renewal_due_on` AS t0_r42, albums.`steps_required` AS t0_r43, annual_renewals.`id` AS t1_r0, annual_renewals.`album_id` AS t1_r1, annual_renewals.`purchase_id` AS t1_r2, annual_renewals.`price_policy_id` AS t1_r3, annual_renewals.`created_on` AS t1_r4, annual_renewals.`number_of_months` AS t1_r5, annual_renewals.`promotion_identifier` AS t1_r6, annual_renewals.`finalized_at` AS t1_r7, artists.`id` AS t2_r0, artists.`name` AS t2_r1, upcs.`id` AS t3_r0, upcs.`number` AS t3_r1, upcs.`upcable_id` AS t3_r2, upcs.`upcable_type` AS t3_r3, upcs.`tunecore_upc` AS t3_r4, upcs.`inactive` AS t3_r5, artworks.`id` AS t4_r0, artworks.`uploaded` AS t4_r1, artworks.`album_id` AS t4_r2, artworks.`s3_asset_id` AS t4_r3, artworks.`last_errors` AS t4_r4, artworks.`auto_generated` AS t4_r5 FROM albums LEFT OUTER JOIN annual_renewals ON annual_renewals.album_id = albums.id LEFT OUTER JOIN artists ON artists.id = albums.artist_id LEFT OUTER JOIN upcs ON upcs.upcable_id = albums.id AND upcs.upcable_type = 'Album' LEFT OUTER JOIN artworks ON artworks.album_id = albums.id WHERE (albums.person_id = 11033 and is_deleted = false) ORDER BY renewal_due_on, albums.created_on DESC;"
    
    Benchmark.bmbm do |x|
      x.report "select" do
        TIMES.times { ActiveRecord::Base.connection.execute(query) }
      end
    end    
    
  end
end
