namespace :advertisements do
  desc "Remove all instances of an unpaid store salepoint from customer carts"
  task remove_price_from_india_advertisements: :environment do
    Advertisement.where(country_website_id: 8).update_all(display_price: nil, display_price_modifier: nil)
  end

  task add_display_modifier_to_india_advertisements: :environment do
    Advertisement.where(country_website_id: 8).update_all(display_price_modifier: 'only')
  end
end
