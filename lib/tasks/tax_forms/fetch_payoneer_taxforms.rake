namespace :tax_forms do
  desc "Fetch Taxforms from Payoneer & Load data into DB"
  task fetch: :environment do
    Person.select('people.id').for_united_states_and_territories.with_no_valid_taxforms_or_associations.in_batches(of: 1000) do |people_batch|
      people_batch.each do |person|
        Rails.logger.info("Fetching Taxform for: #{person.id}")
        FetchTaxFormsWorker.perform_async(person.id)
      end
    end
  end

  desc "Fetch Taxforms from Payoneer across all payee ids & Load data into DB"
  task fetch_across_all_payee_ids: :environment do
    Rails.logger.info("Taxform fetch across payee ids task initiated")
    Person.select(:id).for_united_states_and_territories.in_batches(of: 1000) do |people_batch|
      people_batch.each do |person|
        Rails.logger.info("Fetching Taxform for: #{person.id}")
        FetchTaxFormsWorker.perform_async(person.id, true)
      end
    end
    Rails.logger.info("Taxform fetch across payee ids task concluded")
  end
end
