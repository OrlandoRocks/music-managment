namespace :tax_forms do
  task seed_tax_form_types: :environment do
    [
      "W8BEN",
      "W9 - Individual",
      "W9 - Entity",
      "W8BEN-E",
      "W8ECI - Individual",
      "W8ECI - Entity",
      "W8233"
    ].each do |form_type|
      TaxFormType.find_or_create_by(kind: form_type)
    end
  end
end
