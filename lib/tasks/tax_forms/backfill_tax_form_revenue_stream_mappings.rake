namespace :tax_forms do
  task backfill_tax_form_revenue_stream_mappings: :environment do
    mapped = -> (tax_form) do
      TaxReports::TaxFormMappingService.call!(tax_form.person_id)
    end

    TaxForm
      .where(payout_provider_config_id: PayoutProviderConfig.default_us_tax_program_configs.ids)
      .current
      .w9
      .in_batches
      .each_record(&mapped)
  end
end
