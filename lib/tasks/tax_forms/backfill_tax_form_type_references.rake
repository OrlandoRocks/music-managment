namespace :tax_forms do
  task backfill_tax_form_type_references: :environment do
    # TaxForm#form_type holds both string and numeric values. Eg: "1", "W8BEN", "2", "W9 - Individual", etc
    # Hence, we are fetching TaxForm records by both TaxFormType#id and TaxFormType#kind
    TaxFormType.select(:id, :kind).pluck(:id, :kind).each do |form_types|
      TaxForm.where(form_type: form_types).update_all(tax_form_type_id: form_types[0])
    end
  end
end
