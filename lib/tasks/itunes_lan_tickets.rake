namespace :itunes_lan_tickets do
  desc "creates ticket emails from itunes tickets"
  task :generate_ticket_emails => :environment do
    ItunesLanTicketsProcessor.generate_ticket_emails(ENV["NUM_DAYS_AGO"])
  end
end
