namespace :albums do
  desc "cleanup legacy language codes in albums table with updated codes"
  task cleanup_album_language_codes: :environment do

    LEGACY_LANGUAGE_CODE_MAP = {
      "en-US" => "en",
      "nl-NL" => "nl",
      "fi-FI" => "fi",
      "it-IT" => "it",
      "ja-JP" => "ja",
      "no-NO" => "no"
    }.freeze
    limit = ENV["LIMIT"]
    albums = Album.where(language_code: LEGACY_LANGUAGE_CODE_MAP.keys).limit(limit)
    LEGACY_LANGUAGE_CODE_MAP.each do |k,v|
      albums.where(language_code: k).update(language_code: v)
    end
  end
end
