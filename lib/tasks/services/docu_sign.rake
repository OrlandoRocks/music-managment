namespace :docu_sign do
  task seed_document_templates: :environment do
    tablename = "document_templates"
    filename  = Rails.root.join("db/seed/csv/document_templates.csv")
    klass     = tablename.singularize.classify.constantize
    headers   = klass.attribute_names
    body      = CSV.read(filename)
    klass.delete_all
    klass.import(headers, body, validate: false)
  end
end
