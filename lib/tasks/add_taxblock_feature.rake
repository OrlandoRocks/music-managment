namespace :add_taxblock_feature do
  desc "Adds the `advanced_tax_blocking_feature flag`"
  task run: :environment do
    feature_name = :advanced_tax_blocking
    feature_exists = FeatureFlipper.all_features.any? { |feature_flag| feature_flag.name == feature_name }

    unless feature_exists
      FeatureFlipper.add_feature(feature_name.to_s)
      puts "Feature Flag: `advanced_tax_blocking` has been added successfully!"
    end
  end
end
