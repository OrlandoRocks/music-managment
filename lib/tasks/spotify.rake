namespace :spotify do
  task :get_recent_album_uris => :environment do
    task_uri = 'rake_task=spotify:get_recent_album_uris'
    notify_in_slack("#{task_uri} started - #{Time.now}. Daily Cron Job to fetch Album URI from Spotify which has been delivered during the last 10 days.", 'Spotify')
    SpotifyUriWorker.new.perform(:batch_uri_retrieval)
    notify_in_slack("#{task_uri} completed. Daily Cron Job to fetch Album URI from Spotify which has been delivered during the last 10 days.", 'Spotify')
  end

  task backfill_album_uris: :environment do
    task_uri = 'rake_task=spotify:backfill_album_uris'
    days = Date.today.mjd - DateTime.parse(1.year.ago.to_s).mjd

    notify_in_slack("#{task_uri} started - #{Time.now}. Cron Job to fetch Album URI from Spotify which has been delivered during the last #{days} days.", 'Spotify')
    SpotifyUriWorker.perform_async(:batch_uri_retrieval, ENV["SINCE_DATE"])
    notify_in_slack("#{task_uri} completed. Cron Job to fetch Album URI from Spotify which has been delivered during the last #{days} days.", 'Spotify')
  end

  task backfill_artist_ids: :environment do
    SpotifyUriWorker.perform_async(:backfill_artist_ids, ENV["LIMIT"] || 1000)
  end

  task backfll_album_uris_by_album_id: :environment do
    limit = JSON.parse(ENV.fetch('LIMIT', 0))
    min_index = $redis.get(ENV['JOB_KEY']).to_i || 0
    max_index = min_index + limit
    album_ids = CSV.read(ENV['FILE_PATH'])[min_index...max_index].flatten!
    raise "Album IDs were not found for #{filename}" if album_ids.empty?

    albums = Album.where(id: album_ids)
    SpotifyUriWorker.perform_async(:get_uri_by_albums, albums)
    $redis.INCRBY(ENV['JOB_KEY'], limit)
  end
end
