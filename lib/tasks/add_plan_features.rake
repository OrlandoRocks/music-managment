# frozen_string_literal: true


# bundle exec rake "plans:add_plan_features"
# DO NOT USE THIS RAKE
# keeping it just in case we need to create new features and want a framework, but we don't want to use this -
# use create create_agnostic_table_data or update_agnostic_table_data on prod and other envs - this is only for dev
namespace :plans do
  ALL_PLANNY_FEATURES = [
    :spotify_verified,
    :apple_verified,
    :youtube_oac,
    :revenue_splits,
    :rewards_classes,
    :sales_report,
    :premium_sales_report,
    :trend_reports,
    :cover_art,
    :scheduled_release,
    :partnerships,
    :promotional_opportunities,
    :pro_panels,
    :custom_label,
    :upc,
    :territory_restrictions,
    :recording_location,
    :buy_additional_artists,
    :isrc,
    :store_automator,
    :store_expander,
    :ringtones
  ]

  PLANNY_FEATS = {
    1 => [ # NEW ARTIST
      :youtube_oac,
      :rewards_classes,
      :sales_report,
      :isrc
    ],
    2 => [ # RISING ARTIST
      :spotify_verified,
      :apple_verified,
      :youtube_oac,
      :revenue_splits,
      :rewards_classes,
      :sales_report,
      :isrc,
      :store_automator,
      :store_expander,
      :ringtones
    ],
    3 => [ # BREAKOUT ARTIST
      :spotify_verified,
      :apple_verified,
      :youtube_oac,
      :revenue_splits,
      :rewards_classes,
      :sales_report,
      :premium_sales_report,
      :trend_reports,
      :cover_art,
      :scheduled_release,
      :isrc,
      :store_automator,
      :store_expander,
      :ringtones
    ],
    4 => [ # PROFESSIONAL ARTIST
      :spotify_verified,
      :apple_verified,
      :youtube_oac,
      :revenue_splits,
      :rewards_classes,
      :sales_report,
      :premium_sales_report,
      :trend_reports,
      :cover_art,
      :scheduled_release,
      :partnerships,
      :promotional_opportunities,
      :pro_panels,
      :custom_label,
      :upc,
      :territory_restrictions,
      :recording_location,
      :buy_additional_artists,
      :isrc,
      :store_automator,
      :store_expander,
      :ringtones
    ],
    5 => [ # LEGEND
      :spotify_verified,
      :apple_verified,
      :youtube_oac,
      :revenue_splits,
      :rewards_classes,
      :sales_report,
      :premium_sales_report,
      :trend_reports,
      :cover_art,
      :scheduled_release,
      :partnerships,
      :promotional_opportunities,
      :pro_panels,
      :custom_label,
      :upc,
      :territory_restrictions,
      :recording_location,
      :buy_additional_artists,
      :isrc,
      :store_automator,
      :store_expander,
      :ringtones
    ]
  }
  task :add_plan_features => :environment do
    raise "DO NOT USE THIS" unless Rails.env.development?

    5.downto(1) do |plan_id|
      features = PLANNY_FEATS[plan_id]
      plan = Plan.find(plan_id)
      features.each do |feature|
        plan.plan_features << PlanFeature.find_or_create_by!(feature: feature)
      end
    end
  end
end
