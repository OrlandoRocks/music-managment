namespace :roles do

  desc "Create the CMS role"
  task :create_cms => :environment do
    #
    #  Creates the CMS role
    #

    puts "Creating the CMS role"
    cms_role = Role.find_or_create_by(name: 'CMS')
    cms_role.long_name = "CMS Admin"
    cms_role.description = "Users of this role have the ablility to access, edit, preview and update cms components of the tunecore whitelabel sites"
    cms_role.is_administrative = true
    cms_role.save!

    puts "Adding the CMS rights"
    view_cms = Right.create(:controller => "admin/content", :action => "index", :name => "view_cms")
    publish_cms = Right.create(:controller => "admin/content", :action => "publish", :name => "publish_cms")
    edit_cms = Right.create(:controller => "admin/content", :action => "edit", :name => "edit_cms")

    puts "Associating Rights"
    cms_role.rights << [view_cms, publish_cms, edit_cms]
  end


  desc "Create Digital Store Manager role"
  task :create_store_manager => :environment do
    #
    #  Creates the store manager role
    #

    puts "Creating the store manager role"
    store_role = Role.find_or_create_by(name: 'Store Manager')
    store_role.long_name = "Digital Store Manager Admin"
    store_role.description = "Gives the admin the ability to turn stores on and off for the tunecore and the white label sites"
    store_role.is_administrative = true
    store_role.save!

    puts "Adding the Store manager rights"
    view_store = Right.create(:controller => "admin/manager", :action => "index", :name => "view_stores")
    update_store = Right.create(:controller => "admin/manager", :action => "update", :name => "update_stores")

    puts "Associating Rights"
    store_role.rights << [view_store, update_store]
  end


end