namespace :youtube do
  desc "Export new youtube preferences"
  task :export_preferences => :environment do
    notify_in_slack("youtube:export_preferences started - #{Time.now}.", 'Youtube')
    YoutubePreferencesExportService.export(ENV["FROM_TIME"])
    notify_in_slack("youtube:export_preferences completed - #{Time.now}.", 'Youtube')
  end
end
