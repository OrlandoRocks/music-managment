namespace :product do
  desc 'creates products for facebook products'
  task :add_pub_admin_reinstatement_products => :environment do
    CountryWebsite.where.not(id: 8).each do |cw|
      ActiveRecord::Base.transaction do
        pub_product =  Product.find_by(display_name: 'pub_admin', country_website_id: cw.id)

        product = Product.find_or_create_by!(
          name: "Songwriter Service Reinstatement",
          display_name: "pub_admin_unterminate",
          description: "Re-enabling the collecting, licensing and policing of your songwriter copyrights",
          status: "Active",
          product_type: "Ad Hoc",
          renewal_level: "None",
          price: pub_product.price,
          currency: cw.currency,
          product_family: "Publishing",
          country_website_id: cw.id,
          created_by: Person.find_by(email: 'daniel.risdon@tunecore.com') || Person.first
        )

        item = ProductItem.find_or_create_by!(
          product_id: product.id,
          base_item_id: nil,
          name: "Pub Admin Reinstatement",
          description: "Re-enabling the collecting, licensing and policing of your songwriter copyrights",
          price: 0.0,
          currency: cw.currency,
          does_not_renew: 0,
          renewal_type: nil,
          first_renewal_duration: nil,
          first_renewal_interval: nil,
          renewal_duration: nil,
          renewal_interval: nil,
          renewal_product_id: nil,
        )

        ProductItemRule.find_or_create_by!(
          product_item_id: item.id,
          parent_id: nil,
          rule_type: "inventory",
          rule: "price_for_each",
          inventory_type: 'Composer',
          quantity: 1,
          unlimited: 0,
          price: product.price,
          currency: cw.currency,
          is_active: true
        )

        cw.products << product
      end
    end
    Rails.logger.info("Finished adding products.")
  end
end
