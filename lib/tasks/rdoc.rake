require "rdoc/task"

begin
  desc "Generate TuneCore Docs"
  Rake::RDocTask.new("docs") do |rdoc|
    rdoc.rdoc_dir = 'doc/app'
    rdoc.title    = "TuneCore Application Docs"
    rdoc.main = "doc/README.rdoc"
    rdoc.rdoc_files.include('doc/*.rdoc')
    rdoc.rdoc_files.include('app/models/**/*.rb')
    rdoc.rdoc_files.include('app/helpers/*.rb')
    rdoc.rdoc_files.include('lib/tunecore/**/*.rb')
    rdoc.rdoc_files.include('lib/utilities/**/*.rb')

    rdoc.options << '--line-numbers'
    rdoc.options << '--inline-source'
  end
rescue LoadError
  puts 'Load error in rdoc.rake'
end
