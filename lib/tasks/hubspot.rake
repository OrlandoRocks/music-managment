namespace :hubspot do
  desc "backfill album contact property"
  task backfill_album_contact_property: :environment do
    Album.where("payment_applied = 1 and takedown_at is null").each { |a| a.send_hubspot_update_if_last_purchased }
  end
end
