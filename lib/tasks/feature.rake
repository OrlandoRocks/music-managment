require "#{Rails.root}/lib/logging"

#Rake tasks to manage features
namespace :feature do

  desc "Adds people IDs from a CSV file to the features_people table, adding them to the group with the feature enabled"
  task :add_people_to_feature => :environment do

    filename = ENV["PERSON_CSV"] || raise("Missing variable PERSON_CSV")
    feature_id = ENV["FEATURE_ID"].to_i || raise("Missing variable  FEATURE_ID")
    f = Feature.find(feature_id)
    log("Features", "Adding person ids in #{filename} to feature #{feature_id} (#{f.name})")

    CSV.foreach(filename) do |line|
      begin
        person_id = line[0].to_i
        p = Person.find(person_id)
        fp = FeaturePerson.where(:feature_id => f.id, :person_id => p.id).first
        if fp.nil?
          FeaturePerson.create(:feature_id => feature_id, :person_id => person_id)
          log("Features", "Person #{person_id} (#{p.email}) added to feature #{feature_id} (#{f.name})")
        else
          log("Features", "Feature mapping exists for person #{person_id} (#{p.email}) and feature #{feature_id} (#{f.name})")
        end
      rescue StandardError => e
        log("Features", e)
      end
    end
  end

  desc "Populates local redis with feature flags"
  task :add_features_to_local_redis => :environment do
    if Rails.env.development?
      $redis.flushdb
      one_hundred_percent_features = ["Landr",
        "Yt Posting",
        "Artwork Requirements",
        "Product Rightside",
        "Ytsr In Cart",
        "New Reporting Dashboard",
        "Home Sell Worldwide",
        "Home Manage Career",
        "Home Collect Royalties",
        "Braintree Blue",
        "Uk Country Signup",
        "Tracksmarts Uploader",
        "Au Country Signup",
        "Nz Country Signup",
        "Force Password Reset",
        "Au Uk Credit Cards"]

      zero_percent_features = ["Bio James",
        "Bio Kedar",
        "Bio Jamie",
        "Account Profile Survey",
        "Work with Brands",
        "Priority Retry"]

      one_hundred_percent_features.each do |feature|
        FeatureFlipper.add_feature(feature)
        FeatureFlipper.update_feature feature.gsub(/\s+/, "_").downcase.to_sym, 100, ""
      end

      zero_percent_features.collect{|feature| FeatureFlipper.add_feature(feature)}
    end
  end

  desc "Enable or disable any one feature flag"
  task update: :environment do
    feature = ENV["FEATURE"]
    puts "Must identify a feature flag, try again and enter FEATURE=<feature-name>" and return unless feature.present?
    puts "Default is to turn feature ON. To turn feature off, enter ON=false"
    FeatureFlipper.add_feature(feature)
    unless ENV["ON"] == "false"
      puts "Turning on feature: #{feature}"
      FeatureFlipper.update_feature(feature, 100, "")
    else
      puts "Turning off feature: #{feature}"
      FeatureFlipper.update_feature(feature, 0, "")
    end
    puts "Feature flippin' finished."
  end

  desc "Get a CSV of user ids that has a feature enabled."
  task export_eligible_users: :environment do
    start_time = Time.current
    feature_name = ENV["FEATURE"]

    if feature_name.blank?
      puts "Must identify a feature flag, try again with FEATURE=<feature-name>"
      exit
    end

    feature = FeatureFlipper.all_features.find { |f| f.name.to_s == feature_name }

    if feature.blank?
      puts "Invalid feature. Please ensure the name is correct. Entered Feature: '#{feature_name}'"
      exit
    end

    if feature.percentage == 100
      puts "Feature '#{feature_name}' is rolled out to 100% of the users. Cannot export."
      exit
    end

    formatted_date = Date.current.strftime("%Y%m%d")
    csv_file_path = "/tmp/#{feature_name}_#{formatted_date}.csv"

    CSV.open(csv_file_path, "wb") do |csv|
      Person.select(:id).find_in_batches(batch_size: 100_000).with_index do |batch, batch_no|
        batch.each do |person|
          csv << [person.id] if rollout_user_in_percentage(person.id, feature.percentage)
        end

        puts "Processed batch: #{batch_no}"
      end
    end

    puts "Finished exporting. It's now in #{csv_file_path}. Time elapsed: #{Time.current - start_time}s"
  end

  # This is an internal implementation of Rollout gem to determin if a user falls in
  # a percentage bracket. We're using it here to avoid redis fetch for all the user ids.
  def rollout_user_in_percentage(user_id, percentage)
    Zlib.crc32(user_id.to_s) < Rollout::RAND_BASE * percentage
  end

end
