namespace :tax_block_withdrawals do
  desc "Adds People Flags, Flag Transitions, Flag Reason Applicable for Blocking Withdrawals"
  task :run, [:tax_block_userlist_file] => :environment do |_t, args|
    # Fetching Flag Data
    withdraw_block_flag  = Flag.find_by(Flag::WITHDRAWAL)
    taxblock_flag_reason = withdraw_block_flag.flag_reasons.find_by(
      reason: FlagReason::INCORRECT_TAX_FORM
    )

    # Fetching User Data
    # People must not already have the same flag with the same flag reason
    taxblock_user_details = RakeDatafileService.csv(args[:tax_block_userlist_file])
    taxblockable_user_ids = taxblock_user_details.map { |user| user["id"] }.uniq
    taxblockable_users =
      Person
      .select("people.id, people.name")
      .distinct("person.id")
      .where(
        country_website_id: CountryWebsite::UNITED_STATES,
        id: taxblockable_user_ids
      )
      .where(<<~SQL, withdraw_block_flag.id, taxblock_flag_reason.id)
        NOT EXISTS (
          SELECT * FROM people_flags AS pf
          WHERE pf.person_id = people.id
            AND pf.flag_id = ?
            AND pf.flag_reason_id = ?
        )
      SQL

    next if taxblockable_users.none?

    users_flag_transitions =
      taxblockable_users.map do |taxblock_user|
        {
          add_remove: "add",
          comment: "Withdrawals for #{taxblock_user.name} has been blocked by system.",
          flag_id: withdraw_block_flag.id,
          person_id: taxblock_user.id,
          flag_reason_id: taxblock_flag_reason.id,
          updated_by: 0,
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    users_people_flags =
      taxblockable_users.map do |taxblock_user|
        {
          person_id: taxblock_user.id,
          flag_id: withdraw_block_flag.id,
          flag_reason_id: taxblock_flag_reason.id,
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    users_notes =
      taxblockable_users.map do |taxblock_user|
        {
          subject: "Blocked Withdrawal",
          note: "Withdrawal has been blocked by system",
          related_id: taxblock_user.id,
          related_type: "Person",
          note_created_by_id: 0,
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    # rubocop:disable Rails/SkipsModelValidations
    FlagTransition.insert_all(users_flag_transitions)
    PeopleFlag.insert_all(users_people_flags)
    Note.insert_all(users_notes)
    # rubocop:enable Rails/SkipsModelValidations
  end
end
