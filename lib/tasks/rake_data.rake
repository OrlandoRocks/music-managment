namespace :rake_data do
  [:production, :staging, :development].each do |environment_name|
    namespace environment_name do
      desc "upload csv data file to rake-data s3 bucket under #{environment_name} folder"
      task upload: :environment do
        upload_file_path = ENV["UPLOAD_FILE_PATH"]
        upload_object = RakeDatafileService.upload(upload_file_path, { environment_folder: environment_name.to_s })
        puts "[#{upload_object.action_status ? 'SUCCESS' : 'FAILURE'}] RAKE FILE UPLOAD: #{upload_file_path}"
      end

      desc "fetch csv data file from rake-data s3 bucket under #{environment_name} folder"
      task download: :environment do
        s3_file_key = ENV["S3_FILE_KEY"]
        RakeDatafileService.download(s3_file_key, { temporary: false, environment_folder: environment_name.to_s })
      end
    end
  end
end
