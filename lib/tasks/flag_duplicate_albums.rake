desc "Flag duplicate albums."
task flag_duplicate_content: [:environment] do
  start_date = ENV['START_DATE'] || "2019-01-01"
  query = <<-EOF
    select distinct aid1 album_id1, aid2 album_id2 from (
      select s1.id sid1, afc1.id afcid1, s1.album_id aid1, s1.created_on scon1,
             s2.id sid2, afc2.id afcid2, s2.album_id aid2, s2.created_on scon2
      from songs s1
      join albums a1 on s1.album_id=a1.id and a1.finalized_at is not null
      join songs s2 on s2.content_fingerprint=s1.content_fingerprint and
                 s2.content_fingerprint is not null and s1.id != s2.id
      join albums a2 on s2.album_id=a2.id and a2.finalized_at is not null
      left join album_flagged_content afc1 on afc1.album_id=s1.album_id
      left join album_flagged_content afc2 on afc2.album_id=s2.album_id
      where s1.created_on > '#{start_date}' and s2.created_on > '#{start_date}'
      and (afc1.id is null or afc2.id is null)
      ) dupalbums;
  EOF
  res = ActiveRecord::Base.connection.execute query
  res.to_a.each do |rec|
    Album.find(rec[1]).flag_duplicate_content
  end
end
