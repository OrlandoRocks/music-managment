namespace :geolocation_db_updater do
  desc "Updates the geolocation ip address lookup database"
  task update: :environment do
    response = Faraday.new(ENV["GEOLOCATION_ENDPOINT"] + "/refresh_db").get

    if JSON.parse(response.body)["status"] == "not_updated"
      ActionMailer::Base.mail(
        from: "no-reply@tunecore.com",
        to: "techteam@tunecore.com",
        subject: "Update failure on MaxMind DB",
        body: "There was a failure updating the MaxMind DB, the new DB was not built after the old DB"
      )
    end
  end
end
