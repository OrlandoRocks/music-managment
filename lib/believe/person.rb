module Believe
  class Person
    API_ATTRIBUTES = [
      "id",
      "country_website_id",
      "name",
      "email",
      "artist_id",
      "is_verified",
      "deleted",
      "is_opted_in",
      "label_id",
      "phone_number",
      "referral",
      "created_on",
      "referral_campaign",
      "referral_type",
      "address1",
      "address2",
      "city",
      "state",
      "zip",
      "country",
      "us_zip_code_id",
      "referral_token",
      "vip",
      "foreign_postal_code",
      "accepted_terms_and_conditions_on",
      "status",
      "status_updated_at",
      "lock_reason",
      "mobile_number",
      "last_logged_in_ip",
      "apple_id",
      "apple_role"
    ]

    class << self
      def add_or_update(person_id)
        person = ::Person.find(person_id)
        client_data = create_client_data(person)

        $believe_api_client.post(
          "client/addedit",
          {
            idClientRemote: person.id,
            clientData: client_data.to_json
          }
        )
      end

      def contains_believe_attributes?(attribute_array)
        !(attribute_array & API_ATTRIBUTES).empty?
      end

      def purchased_attributes(person)
        {
          "has_ytsr" => person.has_active_ytm?,
          "has_publishing" => person.paid_for_composer?
        }
      end

      def create_client_data(person)
        client_data = person.attributes.extract!(*API_ATTRIBUTES)
        client_data.merge!(purchased_attributes(person))
        client_data.update(client_data){|k, v| v.to_s}
        client_data
      end
    end
  end
end
