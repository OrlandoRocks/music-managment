module Believe
  class ApiClient
    def initialize(believe_config)
      @believe_config = believe_config
      @http_client = Faraday.new(url: @believe_config["HOST"], ssl: {verify: false})
    end

    def post(endpoint, call_args = {})
      call_args.merge!(get_secret(endpoint))
      call_args.merge!(apiDebug: 1) if @believe_config["API_DEBUG"]
      response = @http_client.post(endpoint, call_args)
      response_body =  JSON.parse(response.body)
      create_believe_error(endpoint, response_body["errors"], call_args) unless response_body["status"]
      response_body
    end

    private

    def create_believe_error(endpoint, errors, call_args={})
      BelieveError.create(response_errors: errors, endpoint: endpoint, request_args: call_args, album_id: call_args[:idAlbumRemote])
    end

    def get_secret(endpoint)
      nonce = Time.now.utc.to_i.to_s
      { nonce: nonce, apiPwd: Digest::SHA256.hexdigest(@believe_config["API_PW"] + @believe_config["HOST"] + "/" + endpoint + nonce), apiKey: @believe_config["API_KEY"] }
    end
  end
end
