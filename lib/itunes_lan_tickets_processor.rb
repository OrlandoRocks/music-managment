class ItunesLanTicketsProcessor

  def self.generate_ticket_emails(days_ago=1)
    new(days_ago).tap(&:generate_ticket_emails)
  end

  def initialize(days_ago)
    @days_ago    = days_ago.to_i
    @target_date = set_target_date
  end

  def generate_ticket_emails
    tickets_for_date = get_tickets
    tickets_for_date.each do |ticket|
      people = people_query(ticket)
      ItunesLanTicketMailer.lan_ticket_creation(ticket, people).deliver
    end
    Rails.logger.info("#{tickets_for_date.count} tickets updated on #{@target_date}")
  end

  def set_target_date
    if @days_ago
      target_time = (Time.now - (@days_ago * 86400)).to_date
      target_time.strftime("%Y-%m-%d")
    else
      Time.now.strftime("%Y-%m-%d")
    end
  end

  def get_tickets
    tickets = self.class.apple_transporter_client.query_tickets
    tickets.select {|ticket| ticket[:last_modified].strftime("%Y-%m-%d") >= @target_date}
  end

  def people_query(ticket)
    Person
      .select("people.email,people.name,people.vip,albums.takedown_at,albums.finalized_at")
      .joins({albums: :upcs}).where({upcs: {number: ticket[:content_vendor_id]}})
  end

  def self.apple_transporter_client
    @@apple_transporter_client ||= Apple::Transporter.new({
      user: ENV["TRENDS_TRANSPORTER_USER"],
      pass: ENV["TRENDS_TRANSPORTER_PASSWORD"],
      path: ENV["ITUNES_TRANSPORTER_PATH"]
    })
  end
end
