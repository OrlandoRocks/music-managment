module Soundout
  class SoundoutAirbrakeException < StandardError; end

  SOUNDOUT_URL = "#{SOUNDOUT_CONFIG['BASE_URL']}?accessKey=#{SOUNDOUT_CONFIG['ACCESS_KEY']}"

  def self.order_report ( report )

    return true unless SOUNDOUT_CONFIG['BASE_URL']
    return false unless report
    is_song_library = report.track.is_a? SongLibraryUpload

    # Note that the trackStream is double url encoded because soundout at the time of writing this is
    # improperly double decoding its value.
    post_body = {
      "authId"        => SOUNDOUT_CONFIG['AUTH_ID'],
      "authKey"       => SOUNDOUT_CONFIG['ACCESS_KEY'],
      "prid"          => "tc.#{report.id}",
      "userEmail"     => SOUNDOUT_CONFIG['USER_EMAIL'],
      "userId"        => SOUNDOUT_CONFIG['USER_ID'],
      "trackTitle"    => report.track.name,
      "trackArtist"   => report.track.artist_name,
      "trackGenre"    => is_song_library ? report.track.genre.name : report.track.album.primary_genre.name,
      "trackStream"   => is_song_library ? CGI.escape(report.track.soundout_url) : CGI.escape(report.track.streaming_url),
      "reviewTotal"   => report.soundout_product.number_of_reviews.to_s,
      "reviewSpeed"   => "1"
    }

    post_body["trackDuration"] = is_song_library ? (report.track.duration * 1000).to_s : report.track.duration.to_s

    Rails.logger.debug("Requesting report with URL: #{SOUNDOUT_URL}")

    query_string = post_body.map{|k,v| "#{CGI.escape(k)}=#{CGI.escape(v)}"}.join("&")

    Rails.logger.debug("Report Parameters: #{query_string}")
    begin
      response = HTTParty.post( "#{SOUNDOUT_URL}", body: query_string, headers: { "Content-Length" => query_string.length.to_s, "Content-Type" => "application/x-www-form-urlencoded" } )
      Rails.logger.info("Soundout Response: #{response}")

      if response['SUCCESS']
        report_id = response[/\d+/]
        Rails.logger.info("Successfully requested report for #{report.track_id}: #{report_id}")
        report_id
      else
        Rails.logger.error("Error requesting report for #{report.track_id}: #{response}")
        false
      end
    rescue StandardError => e
      Rails.logger.error("Error requesting report for #{report.track_id}: #{e}")
      false
    end
  end

  def self.fetch_report report_id

    return true unless SOUNDOUT_CONFIG['BASE_URL']

    post_body = {
      "authKey"   => SOUNDOUT_CONFIG['ACCESS_KEY'],
      "authId"    => SOUNDOUT_CONFIG['AUTH_ID'],
      "reportId"  => report_id.to_s
    }

    Rails.logger.debug("Fetching report with URL: #{SOUNDOUT_URL}")
    Rails.logger.debug("Report Parameters: #{post_body}")
    query_string = post_body.map{|k,v| "#{CGI.escape(k)}=#{CGI.escape(v)}"}.join("&")

    begin
      raw_response = HTTParty.post( "#{SOUNDOUT_URL}", body: query_string, headers: { "Content-Length" => query_string.length.to_s, "Content-Type" => "application/x-www-form-urlencoded" } )
      Rails.logger.debug("Response: #{raw_response}")
    rescue StandardError => e
      Rails.logger.error("Could not request report from soundout. SoundoutId=#{report_id}, Exception=#{e}")
      Airbrake.notify(
        SoundoutAirbrakeException.new('Soundout.fetch_report - httparty'),
        {
          report_id: report_id,
          raw_response: raw_response,
          query_string: query_string,
          error: e.message
        }
      )

      return false, nil
    end

    json = nil
    begin
      json = JSON.parse(raw_response)
    rescue JSON::ParserError
      json = nil
      Rails.logger.error("Could not parse json from soundout. SoundoutId=#{report_id}, JSON=#{raw_response}")

      Airbrake.notify(
        SoundoutAirbrakeException.new('Soundout.fetch_report -- json parsing'),
        {
          report_id: report_id,
          raw_response: raw_response,
          query_string: query_string
        }
      )
      return false, raw_response
    end

    if json["status"] == true || json["status"]["reason"] == "OK"
      Rails.logger.info("Successfully retrieved soundout report #{report_id}")
      Rails.logger.info(raw_response)
      return true,  raw_response
    else
      Rails.logger.error("Error retrieving soundout report #{report_id}: #{json['status']['reason']}")
      Airbrake.notify(
        SoundoutAirbrakeException.new('Soundout.fetch_report -- status checks'),
        {
          report_id: report_id,
          raw_response: raw_response,
          query_string: query_string
        }
      )
      return false, raw_response
    end
  end
end
