module Sidekiq
  module Middleware
    module Server
      class Profiler
        # Number of jobs to process before reporting
        JOBS = 100

        class << self
          mattr_accessor :counter
          self.counter = 0

          def synchronize(&block)
            @lock ||= Mutex.new
            @lock.synchronize(&block)
          end
        end

        def call(worker_instance, item, queue)
          begin
            yield
          ensure
            self.class.synchronize do
              self.class.counter += 1

              if self.class.counter % JOBS == 0
                Sidekiq.logger.info "reporting allocations after #{self.class.counter} jobs"
                GC.start
                f = File.open('heap.json','w')
                  ObjectSpace.dump_all(output: f)
                f.close
                Sidekiq.logger.info "heap saved to heap.json"
              end
            end
          end
        end
      end
    end
  end
end
