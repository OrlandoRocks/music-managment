#
# Encapsulates logic to provide a daily report on batches.
# Consider saving the aggregated data in the database
#
class Batch::BatchSummary
  # Release counts
  attr_reader :renewals_due
  attr_reader :renewed_total,         :renewed_in_batch,      :paid_in_advance
  attr_reader :not_renewed_total,     :not_renewed_canceled,  :not_renewed_takendown, :not_renewed_in_grace, :outstanding_revenue
  attr_reader :totals_by_release_type

  # Revenue totals
  attr_reader :albums_revenue_in_batch,    :albums_revenue_in_advance,    :total_albums_revenue
  attr_reader :singles_revenue_in_batch,   :singles_revenue_in_advance,   :total_singles_revenue
  attr_reader :ringtones_revenue_in_batch, :ringtones_revenue_in_advance, :total_ringtones_revenue
  attr_reader :total_revenue_in_batch,     :total_revenue_in_advance,     :total_revenue

  # By Payment Type
  # Autorenewed
  attr_reader :auto_renewed_by_balance,     :auto_renewed_by_balance_revenue,     :auto_renewed_by_balance_failures
  attr_reader :auto_renewed_by_credit_card, :auto_renewed_by_credit_card_revenue, :auto_renewed_by_credit_card_failures
  attr_reader :auto_renewed_by_paypal,      :auto_renewed_by_paypal_revenue,      :auto_renewed_by_paypal_failures

  # Paid in advance
  attr_reader :in_advance_balance_only,            :in_advance_balance_only_revenue
  attr_reader :in_advance_balance_and_credit_card, :in_advance_balance_and_credit_card_revenue
  attr_reader :in_advance_balance_and_paypal,      :in_advance_balance_and_paypal_revenue
  attr_reader :in_advance_credit_card_only,        :in_advance_credit_card_only_revenue
  attr_reader :in_advance_paypal_only,             :in_advance_paypal_only_revenue

  # People Counts
  attr_reader :people_with_renewals_due
  attr_reader :people_renewed_in_advance, :people_renewed_in_batch, :people_renewed_total
  attr_reader :people_not_renewed_canceled, :people_not_renewed_takendown, :people_not_renewed_in_grace, :people_not_renewed_total
  attr_reader :people_totals_by_release_type

  # Invoice Split
  attr_reader :num_of_people_with_one_invoice,  :total_invoice_amount_one_invoice
  attr_reader :num_of_people_with_two_invoices, :total_invoice_amount_two_invoices

  # Failures
  attr_reader :albums_total_failure, :singles_total_failure, :ringtones_total_failure, :total_failure
  attr_reader :albums_failure_rate, :singles_failure_rate, :ringtones_failure_rate, :total_failure_rate
  attr_reader :albums_total, :singles_total, :ringtones_total, :total_purchases

  def initialize(batch)
    @batch = batch
    @time  = batch.batch_date

    @renewal_base_scope = Renewal.for_country_website(batch.country_website_id)
    @person_base_scope  = Person.for_country_website(batch.country_website_id)
    @invoice_base_scope = Invoice.for_country_website(batch.country_website_id)

    @totals_by_release_type = {}
    @people_totals_by_release_type = {}

    calculate_total_counts
    calculate_counts_by_release_type
    calculate_total_revenues
    calculate_by_payment_type
    calculate_invoice_split
    calculate_failures

    # calculate_person_total_counts
    # calculate_person_counts_by_release_type
  end

  def balance_percent(value)
    @auto_renewed_by_balance > 0 ? value.to_f / @auto_renewed_by_balance * 100 : 0
  end

  def credit_card_percent(value)
    @auto_renewed_by_credit_card > 0 ? value.to_f / @auto_renewed_by_credit_card * 100 : 0
  end

  def paypal_percent(value)
    @auto_renewed_by_paypal > 0 ? value.to_f / @auto_renewed_by_paypal * 100 : 0
  end

  def person_percent(value)
    @people_with_renewals_due > 0 ? value.to_f / @people_with_renewals_due * 100 : 0
  end

  def total_percent(value)
    @renewals_due > 0 ? value.to_f / @renewals_due * 100 : 0
  end

  def renewed_release_percent(value, release_type)
    @totals_by_release_type[release_type][:total_count] > 0 ? value.to_f / @totals_by_release_type[release_type][:total_count] * 100 : 0
  end

  private

  def calculate_failures
    @albums_total_failure = @batch.filter_sum('Album', 'cannot_process')
    @singles_total_failure = @batch.filter_sum('Single', 'cannot_process')
    @ringtones_total_failure = @batch.filter_sum('Ringtone', 'cannot_process')
    @total_failure = @batch.filter_sum(nil, 'cannot_process')

    @albums_total = @batch.filter_sum('Album', nil)
    @singles_total = @batch.filter_sum('Single', nil)
    @ringtones_total = @batch.filter_sum('Ringtone', nil)
    @total_purchases = @batch.filter_sum(nil, nil)

    @albums_failure_rate = normalize_rate(@albums_total_failure.to_f / @albums_total.to_f)
    @singles_failure_rate = normalize_rate(@singles_total_failure.to_f / @singles_total.to_f)
    @ringtones_failure_rate = normalize_rate(@ringtones_total_failure.to_f / @ringtones_total.to_f)
    @total_failure_rate = normalize_rate(@total_failure.to_f / @total_purchases.to_f)
  end

  def normalize_rate(rate)
    rate.nan? ? 0.0 : rate
  end

  def calculate_total_counts
    # Total
    @renewals_due      = @renewal_base_scope.due_on(@time.to_date).count

    # Renewed
    @renewed_in_batch  = @renewal_base_scope.renewed_in(@batch).count
    @paid_in_advance   = @renewal_base_scope.renewed_in_advance(@batch).count
    @renewed_total     = @paid_in_advance + @renewed_in_batch

    # Not Renewed
    @not_renewed_canceled  = @renewal_base_scope.not_renewed_canceled(@batch).count
    @not_renewed_takendown = @renewal_base_scope.not_renewed_takendown(@batch).count
    @not_renewed_in_grace  = @renewal_base_scope.not_renewed_but_in_grace(@batch).count
    @not_renewed_total     = @not_renewed_canceled + @not_renewed_takendown + @not_renewed_in_grace
    @outstanding_revenue   = @renewal_base_scope.not_renewed_in(@batch).sum(:cost_cents).to_i
  end

  def calculate_person_total_counts
    # Total
    @people_with_renewals_due = @person_base_scope.with_renewals_expiring_between(@time.to_date, @time.to_date).count(select: "distinct people.id")

    # Renewed
    @people_renewed_in_advance = @person_base_scope.that_extended_renewals_due_on_paid_before(@time.to_date, @time).count(select: "distinct people.id")
    @people_renewed_in_batch   = @person_base_scope.that_extended_renewals_due_on_paid_after(@time.to_date, @time).count(select: "distinct people.id")
    @people_renewed_total      = @people_renewed_in_advance + @people_renewed_in_batch

    # Not Renewed
    @people_not_renewed_canceled  = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_canceled_renewals(true).count(select: "distinct people.id")
    @people_not_renewed_takendown = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_takendown_renewals(true).with_canceled_renewals(false).count(select: "distinct people.id")
    @people_not_renewed_in_grace  = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_takendown_renewals(false).with_canceled_renewals(false).count(select: "distinct people.id")
    @people_not_renewed_total     = @people_not_renewed_canceled + @people_not_renewed_takendown + @people_not_renewed_in_grace
  end

  def calculate_counts_by_release_type
    %w(Album Single Ringtone).each do |related_type|
      data = {}

      # renewed
      data[:in_batch_count]      = @renewal_base_scope.renewed_in(@batch).for_album_type(related_type).count
      data[:in_advance_count]    = @renewal_base_scope.renewed_in_advance(@batch).for_album_type(related_type).count
      data[:renewed_total_count] = data[:in_batch_count] + data[:in_advance_count]

      # not renewed
      data[:not_renewed_canceled_count]  = @renewal_base_scope.not_renewed_canceled(@batch).for_album_type(related_type).count
      data[:not_renewed_takendown_count] = @renewal_base_scope.not_renewed_takendown(@batch).for_album_type(related_type).count
      data[:not_renewed_in_grace_count]  = @renewal_base_scope.not_renewed_but_in_grace(@batch).for_album_type(related_type).count
      data[:not_renewed_total]           = data[:not_renewed_canceled_count] + data[:not_renewed_takendown_count] + data[:not_renewed_in_grace_count]

      data[:total_count]                 = data[:renewed_total_count] + data[:not_renewed_total]

      @totals_by_release_type[related_type.to_s.to_sym] = data
    end
  end

  def calculate_person_counts_by_release_type
    %w(Album Single Ringtone).each do |related_type|
      data = {}

      # renewed
      data[:in_batch_count]      = @person_base_scope.that_extended_renewals_due_on_paid_after(@time.to_date, @time).with_renewals_for_album_type(related_type).count(select: "distinct people.id")
      data[:in_advance_count]    = @person_base_scope.that_extended_renewals_due_on_paid_before(@time.to_date, @time).with_renewals_for_album_type(related_type).count(select: "distinct people.id")
      data[:renewed_total_count] = data[:in_batch_count] + data[:in_advance_count]

      # not renewed
      data[:not_renewed_canceled_count]  = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_canceled_renewals(true).with_renewals_for_album_type(related_type).count(select: "distinct people.id")
      data[:not_renewed_takendown_count] = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_takendown_renewals(true).with_canceled_renewals(false).with_renewals_for_album_type(related_type).count(select: "distinct people.id")
      data[:not_renewed_in_grace_count]  = @person_base_scope.with_renewals_due_on_but_not_renewed(@time.to_date).with_takendown_renewals(false).with_canceled_renewals(false).with_renewals_for_album_type(related_type).count(select: "distinct people.id")
      data[:not_renewed_total]           = data[:not_renewed_canceled_count] + data[:not_renewed_takendown_count] + data[:not_renewed_in_grace_count]

      data[:total_count]                 = data[:not_renewed_total] + data[:renewed_total_count]

      @people_totals_by_release_type[related_type.to_s.to_sym] = data
    end
  end

  def calculate_total_revenues
    # Revenue in batch
    @albums_revenue_in_batch    = @renewal_base_scope.renewed_in(@batch).for_album_type("Album").sum(:cost_cents).to_i
    @singles_revenue_in_batch   = @renewal_base_scope.renewed_in(@batch).for_album_type("Single").sum(:cost_cents).to_i
    @ringtones_revenue_in_batch = @renewal_base_scope.renewed_in(@batch).for_album_type("Ringtone").sum(:cost_cents).to_i
    @total_revenue_in_batch     = @albums_revenue_in_batch + @singles_revenue_in_batch + @ringtones_revenue_in_batch

    # Revenue in advance
    @albums_revenue_in_advance    = @renewal_base_scope.renewed_in_advance(@batch).for_album_type("Album").sum(:cost_cents).to_i
    @singles_revenue_in_advance   = @renewal_base_scope.renewed_in_advance(@batch).for_album_type("Single").sum(:cost_cents).to_i
    @ringtones_revenue_in_advance = @renewal_base_scope.renewed_in_advance(@batch).for_album_type("Ringtone").sum(:cost_cents).to_i
    @total_revenue_in_advance     = @albums_revenue_in_advance + @singles_revenue_in_advance + @ringtones_revenue_in_advance

    # Total revenue from releases due on batch date
    @total_albums_revenue    = @albums_revenue_in_batch    + @albums_revenue_in_advance
    @total_singles_revenue   = @singles_revenue_in_batch   + @singles_revenue_in_advance
    @total_ringtones_revenue = @ringtones_revenue_in_batch + @ringtones_revenue_in_advance

    # Totals
    @total_revenue = @total_albums_revenue + @total_singles_revenue + @total_ringtones_revenue
  end

  def calculate_by_payment_type
    # AUTO RENEWED

    # by balance
    @auto_renewed_by_balance = @batch.balance_batch_transactions.count
    @auto_renewed_by_balance_revenue  = @batch.balance_batch_transactions.sum(:amount).to_f
    @auto_renewed_by_balance_failures = 0

    # by credit card
    @auto_renewed_by_credit_card = @batch.credit_card_batch_transactions.count
    successful_braintree_transactions = @batch.batch_transactions.successful_braintree_transactions
    successful_payments_os_transactions = @batch.batch_transactions.successful_payments_os_transactions
    @auto_renewed_by_credit_card_revenue  = @batch.country_website_id == CountryWebsite::INDIA ?
                                                successful_payments_os_transactions.sum(:amount) :
                                                successful_braintree_transactions.sum(:amount)
    @auto_renewed_by_credit_card_failures = @auto_renewed_by_credit_card -
        (@batch.country_website_id == CountryWebsite::INDIA ? successful_payments_os_transactions.count : successful_braintree_transactions.count)

    # by paypal
    @auto_renewed_by_paypal = @batch.paypal_batch_transactions.count
    @auto_renewed_by_paypal_revenue  = @batch.batch_transactions.successful_paypal_transactions.sum(:amount).to_f
    @auto_renewed_by_paypal_failures = @auto_renewed_by_paypal - @batch.batch_transactions.successful_paypal_transactions.count

    invoices = @invoice_base_scope.extending_renewals_due_on(@time.to_date).settled_at_before(@time)

    # PAID IN ADVANCE
    # Balance
    @in_advance_balance_only = invoices
      .settled_only_with(["PersonTransaction"])
      .select("distinct invoices.id")
      .count
    @in_advance_balance_only_revenue = invoices
      .settled_only_with(["PersonTransaction"])
      .sum("purchases.cost_cents")
      .to_i

    # by balance + credit card
    @in_advance_balance_and_credit_card = invoices
      .settled_only_with(%w(PersonTransaction BraintreeTransaction PaymentsOSTransaction))
      .select("distinct invoices.id")
      .count
    @in_advance_balance_and_credit_card_revenue = invoices
      .settled_only_with(%w(PersonTransaction BraintreeTransaction PaymentsOSTransaction))
      .sum("purchases.cost_cents")
      .to_i

    # Balance + Paypal
    @in_advance_balance_and_paypal = invoices
      .settled_only_with(%w(PersonTransaction PaypalTransaction))
      .select("distinct invoices.id")
      .count
    @in_advance_balance_and_paypal_revenue = invoices
      .settled_only_with(%w(PersonTransaction PaypalTransaction))
      .sum("purchases.cost_cents")
      .to_i

    # Credit card
    @in_advance_credit_card_only = invoices
      .settled_only_with(%w(BraintreeTransaction PaymentsOSTransaction))
      .select("distinct invoices.id")
      .count
    @in_advance_credit_card_only_revenue = invoices
      .settled_only_with(%w(BraintreeTransaction PaymentsOSTransaction))
      .sum("purchases.cost_cents")
      .to_i

    # Paypal only
    @in_advance_paypal_only = invoices
      .settled_only_with(["PaypalTransaction"])
      .select("distinct invoices.id")
      .count
    @in_advance_paypal_only_revenue = invoices
      .settled_only_with(["PaypalTransaction"])
      .sum("purchases.cost_cents")
      .to_i
  end

  def calculate_invoice_split
    # Sum number of people in batch that have one invoice and sum total
    split_information                 = @batch.invoice_split_information(1)
    @num_of_people_with_one_invoice   = split_information[0].to_i
    @total_invoice_amount_one_invoice = split_information[1].to_i / 100

    # Sum number of people in batch that have two invoices and sum total
    split_information                  = @batch.invoice_split_information(2)
    @num_of_people_with_two_invoices   = split_information[0].to_i
    @total_invoice_amount_two_invoices = split_information[1].to_i / 100
  end
end
