class Apple::ApiClient
   def initialize(params)
      @http_client = Faraday.new(url: params[:host])
      @password    = params[:password]
    end

    def post(url, params, source = nil)
      @source = source
      response = make_post(url, params.merge({password: @password}))
      parse_response(response)
    end

    private
    attr_reader :password

    def make_post(url, params)
      @http_client.post do |req|
        setup_request(req, url)
        req.body = params.to_json
      end
    end

     def setup_request(req, url)
      req.url url
      req.headers['Content-Type']  = 'application/json'
    end

    def parse_response(response)
      result = JSON.parse(response.body)
      return set_failure_response(result) unless result["status"] == 0
      return set_rf_success_response(result) if @source.present?

      set_success_response(result)
    end

    def set_rf_success_response(result)
      details_list = result["receipt"]["in_app"].select { |r| r['expires_date'] == nil}
      details = details_list.sort_by { |r| Date.parse(r['purchase_date']) }.last
      data = {
        purchase_date: details["purchase_date"],
        receipt_data:  result["latest_receipt"]
      }
      Response.new(status: "success", data: data)
    end

    def set_success_response(result)
      details_list = result["receipt"]["in_app"].select { |r| r['expires_date'] != nil}
      details = details_list.sort_by { |r| Date.parse(r['expires_date']) }.last
      data = {
        purchase_date: details["purchase_date"],
        expires_date:  details["expires_date"],
        receipt_data:  result["latest_receipt"]
      }
      Response.new(status: "success", data: data)
    end

    def set_failure_response(result)
      Response.new(status: "failure", code: result["status"])
    end

    class Response
      attr_reader :status, :data, :code
      def initialize(attributes)
        @status = attributes[:status]
        @data   = attributes[:data]
        @code   = attributes[:code]
      end

      def is_successful
        status == "success"
      end
    end
end
