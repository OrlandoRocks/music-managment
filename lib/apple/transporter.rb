class Apple::Transporter
  extend ActiveModel::Callbacks

  class TransporterCreateArtistError < StandardError; end
  class TransporterVerifyPackageError < StandardError; end

  attr_reader :transporter_installed

  define_model_callbacks :transport, only: [:before]
  before_transport :validate_presence_of_transporter

  def initialize(config)
    @username = config[:user]
    @password = config[:pass]
    @path     = config[:path]
    @provider = config[:provider]
  end

  def transport(method, *args)
    run_callbacks :transport do
      send(method, *args)
    end
  end

  def release_status(identifier)
    result = release_status_xml(identifier)
    format_release_status_result(result)
  end

  def lookup_item(item_type, identifier)
    result = send("#{item_type}_xml", identifier)
    send("format_#{item_type}_result", result)
  end

  def validate_package(package)
    output = package_validation_exec(package)
    format_validation_result(output)
  end

  def curated_artists(package, artist_names)
    output = curated_artist_exec(package)
    format_curated_artists(output, artist_names)
  end

  def send_package(package)
    result = package_send_cmd(package)
    format_send_package_result(result)
  end

  def query_tickets
    result = query_tickets_cmd
    format_query_tickets_xml(result)
  end

  def validate_presence_of_transporter
    raise "You must have iTMSTransporter installed and in your path to run this job." unless transporter_present
  end

  def create_artist(artist_name, artist_name_locale, force_create_token = nil)
    result = format_create_artist_result(
      create_artist_cmd(artist_name, artist_name_locale, force_create_token),
      artist_name
    )

    return create_artist(artist_name, artist_name_locale, result[:force_create_token]) if result[:success] && result[:force_create_token]

    result
  end

  private

  def transporter_present
    @transporter_installed ||= `which #{@path}iTMSTransporter`
    return false if transporter_installed.empty?
    return false if transporter_installed.include?("no iTMSTransporter")
    true
  end

  def package_validation_cmd(package)
    "#{@path}iTMSTransporter -m verify -f #{Rails.root.join package.dir.path} -u #{@username} -p #{@password} -itc_provider #{@provider} -v critical -outputFormat xml"
  end

  def package_validation_exec(package)
    `#{package_validation_cmd(package)}`
  end

  def curated_artist_exec(package)
    pid, stdin, stdout, stderr = Open4::popen4 "#{package_validation_cmd(package)}"
    stderr.read
  end

  def package_send_cmd(package)
    `#{@path}iTMSTransporter -m upload -f #{Rails.root.join package.dir.path} -u #{@username} -p #{@password} -itc_provider #{@provider} -v critical -k 1500 -t Signiant`
  end

  def query_tickets_cmd
    `#{@path}iTMSTransporter -m queryTickets -u #{@username} -p #{@password} -v off`
  end

  def valid_xml?(output)
    !output.try(:include?, "error")
  end

  def successful_upload?(output)
    output.try(:include?, "1 packages were uploaded successfully") ||  output.try(:include?, "1 package was uploaded successfully")
  end

  def release_status_xml(identifier)
    `#{@path}iTMSTransporter -m status -u #{@username} -p #{@password} -vendor_id #{identifier} -itc_provider #{@provider} -outputFormat xml`
  end

  def release_xml(album_upc)
    `#{@path}iTMSTransporter -m lookupMetadata -u #{@username} -p #{@password} -vendor_id #{album_upc} -itc_provider #{@provider} -outputFormat xml -destination ./tmp/itunes_metadata`
    File.read("tmp/itunes_metadata/#{album_upc}.itmsp/metadata.xml")
  end

  def artist_xml(artist_id)
    `#{@path}iTMSTransporter -m lookupArtist -u #{@username} -p #{@password} -apple_id #{artist_id} -outputFormat xml`
  end

  def format_release_status_result(result)
    doc = parse_xml(result)
    {
      document: doc,
      apple_id: doc.at_xpath("//upload_status").try(:attributes).try(:[], "apple_identifier").try(:value)
    }
  end

  def format_release_result(result)
    doc = parse_xml(result)
    {
      document: doc,
      apple_id: doc.at_xpath("//apple_id").try(:content).try(:to_i)
    }
  end

  def format_artist_result(result)
    doc = parse_xml(result)
    {
      document: doc,
      name: doc.at_xpath("//name").try(:text).try(:strip),
      apple_id: doc.at_xpath("//appleId").try(:text).try(:strip)
    }
  end

  def format_query_tickets_xml(result)
    doc     = parse_xml(result)
    doc.xpath("tickets//ticket").map do |ticket|
      if ticket.xpath("contentTicketState").text == "Your Action Needed"
        notes = ticket.xpath("notes//note").map do |note|
          {note: note.text, date: note.xpath("date").text}
        end
        {
          created: Time.parse(ticket.xpath("created").text),
          last_modified: Time.parse(ticket.xpath("lastModified").text),
          ticket_id: ticket.xpath("ticketId").text,
          content_ticket_type: ticket.xpath("contentTicketType").text,
          content_vendor_id: ticket.xpath("contentVendorId").text,
          name: ticket.xpath("name").text,
          notes: notes
        }
      end
    end.compact
  end

  def format_validation_result(result)
    {
      valid: valid_xml?(result),
      output: result
    }
  end

  def format_send_package_result(result)
    {
      success: successful_upload?(result),
      output: result
    }
  end

  def parse_xml(xml)
    Nokogiri::XML.parse(xml) {|config| config.noblanks}
  end

  def create_artist_cmd(artist_name, artist_name_locale, force_create_token = nil)
    `#{@path}iTMSTransporter -m createArtist -u #{@username} -p #{@password} -itc_provider #{@provider} -n "#{artist_name}" -nativeLocale "#{artist_name_locale}" #{force_create_token ? "-forceCreateToken " + force_create_token : ''}`
  end

  def format_create_artist_result(result, artist_name)
    if artist_force_creation_valid?(result)
      result[/-forceCreateToken[ ]+"(\S+)"/]
      {
        success: true,
        output: result,
        force_create_token: $1,
        apple_id: nil
      }
    else
      result[/#{artist_name}[ ]+(\d+)/]
      {
        success: artist_creation_valid?(result),
        output: result,
        force_create_token: nil,
        apple_id: $1
      }
    end
  end

  def artist_creation_valid?(output)
    output.try(:include?, "Artist created")
  end

  def artist_force_creation_valid?(output)
    output.try(:include?, "Artist not created, to force the artist to be created use the option -forceCreateToken")
  end

  def format_curated_artists(output, artist_names)
    names = artist_names.select do |name|
      output.try(:include?, "Curated Artist: The artist '#{name}' has been curated")
    end
    names
  end
end
