module CheckPayment
  require 'check_payment/export'

  @columns = []

  class Column
    attr_reader :name
    def initialize(name)
      @name = name
    end

  end

  class ConstantColumn < Column
    def initialize(name, value)
      super(name)
      @value = value
    end

    def data_for(record)
      @value
    end
  end

  class RecordFieldColumn < Column
    def initialize(name, field, &block)
      super(name)
      @field = field
      @block = block
    end

    def data_for(record)
      if @block
        @block.call(record.send(@field))
      else
        record.send(@field)
      end
    end
  end

  class << self
    def columns(&block)
      self.instance_eval(&block)
    end

    def constant(name, value)
      @columns << ConstantColumn.new(name, value)
    end

    def column(name, field, &block)
      @columns << RecordFieldColumn.new(name, field, &block)
    end
  end

  # maps fields of the check_transfer active record into an excel spreadsheet column
  # constant field are indicated with "constant"
  columns do
    constant "Client Name", "TuneCore"
    column "Payee Name", :payee
    column "Address 1", :address1
    column "Address 2", :address2
    column "City", :city
    column "State/Province", :state
    column "Zip", :postal_code
    column "Country", :country
    column("Amount", :payment_cents) {|p| sprintf("%.2f", p.to_f / 100)}
    column "Reference", :id
    constant "Stub Comment", "Congratulations on your sales!  These proceeds were requested from your account.  Thank you for using TuneCore."
  end
end
