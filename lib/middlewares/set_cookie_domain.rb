module Middlewares
  class SetCookieDomain
    def initialize(app, options = {})
      @app = app
    end

    def call(env)
      domain = (env['HTTP_HOST']||env['HTTPS_HOST']).gsub(/:\d+$/, '').gsub(/^[^.]*/, '')
      env["rack.session.options"][:domain] = domain
      @app.call(env)
    end
  end
end
