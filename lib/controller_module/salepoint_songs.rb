module ControllerModule
  module SalepointSongs
    include YoutubeHelper

    def salepoint_song_takedown
      song = Song.find(params[:id])
      if SalepointSong.takedown(song)
        create_note(song, "removed")
        create_ytsr_track_takedown_note(song)
        flash[:success] = "Successfully sent metadata update for takedown"
      else
        flash[:error] = "Failed to successfully send metadata update"
      end

      redirect_to admin_album_path(song.album)
    end

    def salepoint_song_remove_takedown
      song = Song.find(params[:id])
      if SalepointSong.remove_takedown(song)
        create_note(song, "reclaimed")
        flash[:success] = "Successfully sent metadata update for takedown"
      else
        flash[:error] = "Failed to successfully send metadata update"
      end

      redirect_to admin_album_path(song.album)
    end

    private

    def create_note(song, state)
      Note.create(
        related: song.album,
        note_created_by: current_user,
        ip_address: request.remote_ip,
        subject: "Rights #{state.capitalize}",
        note: "Sound Recording Rights for Song ##{song.id} were #{state}"
      )
    end
  end
end
