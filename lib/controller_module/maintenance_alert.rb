module ControllerModule
  module MaintenanceAlert
    private

    def maintenance_alert
      flash.now[:persistent_alert] = "<span class=\"release-alert\" ><strong class=\"alert-title\">#{custom_t(:maintenance_title)}</strong><br>#{custom_t(:maintenance_message)}<span>" if FeatureFlipper.show_feature?(:maintenance_alert, current_user)
    end
  end
end
