module SystemStatus
  module BraintreeTransactions
    def self.do_check
      { score: calculate_score }
    end

    def self.calculate_score
      counts = status_counts
      return 0.0 if counts[:total] == 0
      counts[:success].to_f / counts[:total].to_f
    end

    def self.status_counts
      statuses.each_with_object(success: 0, total: 0) do |status_row, counts|
        counts[:total] += status_row.status_count
        counts[:success] = status_row.status_count if status_row.status == "success"
        counts
      end
    end

    def self.statuses
      BraintreeTransaction
        .select("status, count(*) as 'status_count'")
        .where(created_at: 1.hour.ago..Time.current)
        .group(:status)
    end
  end
end
