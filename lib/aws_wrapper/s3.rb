module AwsWrapper
  class S3
    # For valid keys for options hash see
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#write-instance_method
    def self.write(bucket:, key:, data:, options: {})
      call_wrapper(bucket, key, nil) do
        s3_object(bucket, key).write(data, options)
      end
    end

    # For valid keys for options hash see
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#read-instance_method
    def self.read(bucket:, key:, options: {})
      call_wrapper(bucket, key, nil) do
        s3_object(bucket, key).read(options)
      end
    end

    def self.metadata(bucket:, key:)
      call_wrapper(bucket, key, {}) do
        object = s3_object(bucket, key)

        {
          last_modified: object.last_modified
        }
      end
    end

    # For valid keys for options hash see
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#url_for-instance_method
    def self.read_url(bucket:, key:, options: {})
      call_wrapper(bucket, key, nil) do
        s3_object(bucket, key).url_for(:read, options).to_s
      end
    end

    # For valid keys for options hash see
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#copy_from-instance_method
    def self.update_options(bucket:, key:, options:)
      call_wrapper(bucket, key, nil) do
        s3_object(bucket, key).copy_from(key, options)
      end
    end

    # For valid keys for options hash see
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/S3/S3Object.html#delete-instance_method
    def self.delete(bucket:, key:, options: {})
      call_wrapper(bucket, key, nil) do
        s3_object(bucket, key).delete(options)
      end
    end

    # --------------------- #
    # Private class methods #
    # --------------------- #

    def self.s3_object(bucket, key)
      client.buckets[bucket].objects[key]
    end

    def self.call_wrapper(bucket, object_key, default_return, &block)
      begin
        Rails.logger.info("Accessing object=#{object_key} in bucket=#{bucket}")
        block.call
      rescue => e
        Rails.logger.error("#{self.class} calling s3 sdk raised error #{e.message}")
        Airbrake.notify(e, bucket: bucket, key: object_key)

        default_return
      end
    end

    def self.client
      @client ||= AWS::S3.new
    end

    private_class_method :client, :call_wrapper, :s3_object
  end
end
