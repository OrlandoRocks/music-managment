module AwsWrapper
  class Sqs
    # See https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/SQS/Queue.html#approzimate_number_of_messages-instance_method
    # for sdk library documentation around valid options and usage.
    def self.size(queue_name:)
      with_queue_object(queue_name, &:approximate_number_of_messages)
    end

    # See https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/SQS/Queue.html#batch_send-instance_method
    # for sdk library documentation around valid options and usage.
    def self.batch_send(queue_name:, messages:)
      with_queue_object(queue_name) do |queue|
        queue.batch_send(messages)
      end
    end

    # See https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/SQS/Queue.html#poll-instance_method
    # for sdk library documentation around valid options and usage.
    #
    # Yields a AWS::SQS::ReceivedMessage to the caller
    #
    # TODO: Long term the object yielded should be a custom object once the
    # attributes required are more well defined across the application.
    def self.poll(queue_name:, options: {}, &block)
      raise(ArgumentError.new, "Calling with a block is required") unless block_given?

      with_queue_object(queue_name) do |queue|
        queue.poll(options) do |message|
          block.call(message)
        end
      end
    end

    # --------------------- #
    # Private class methods #
    # --------------------- #

    def self.with_queue_object(name)
      begin
        Rails.logger.info("Accessing queue=#{name}")
        yield(client.queues.named(name))
      rescue => e
        Rails.logger.error("#{self.class} calling sqs sdk raised error #{e.message}")
        Airbrake.notify(e, queue: name)

        nil
      end
    end

    def self.client
      @client ||= AWS::SQS.new
    end

    private_class_method :client, :with_queue_object
  end
end
