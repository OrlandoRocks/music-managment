module AwsWrapper
  class SimpleDb
    # See documentation for #attributes for the hash that will be returned
    # https://docs.aws.amazon.com/AWSRubySDK/latest/AWS/SimpleDB/Item.html#attributes-instance_method
    def self.get_attributes(domain:, item:)
      call_wrapper(domain, item, {}) do
        client.domains[domain].items[item].attributes.to_h
      end
    end

    # --------------------- #
    # Private class methods #
    # --------------------- #

    def self.call_wrapper(domain, item, default_return, &block)
      begin
        Rails.logger.info("Retreiving SDB attributes from SDB domain=#{domain}")
        Rails.logger.info("Running query 'item'='#{item}'")
        block.call
      rescue => e
        Rails.logger.error("#{self.class} calling sdb sdk raised error #{e.message}")
        Airbrake.notify(e, domain: domain, item: item)

        default_return
      end
    end

    def self.client
      @client ||= AWS::SimpleDB.new(AwsWrapper::Sdb::CONFIG)
    end

    private_class_method :client, :call_wrapper
  end
end
