#
#  SalepointSystem:
#
#  Intended to extract out controller logic for
#  manipulating incoming parameter hash data
#  into Salepoint objects
#
module SalepointSystem

  #
  #  Take the incoming collection of salepoint hashes and
  #  transform them into Salepoint objects
  #
  def transform_salepoints(salepoint_hashes = [])
    return [] if ! salepoint_hashes
    
    salepoint_attributes = extract_salepoints(salepoint_hashes)
    salepoint_attributes.map do |attributes|
      SalepointTransformer.transform(attributes)
    end
  end

  #
  #  Grouped by store id, so we have to separate the store_id from the salepoint hash
  #
  #  Example salepoint:
  #  ["13", {"salepoint"=>{"variable_price_id"=>"4", "store_id"=>"13", "has_rights_assignment"=>"true"}}]
  #
  def extract_salepoints(salepoint_hashes = [])
    return [] if ! salepoint_hashes

    result = salepoint_hashes.map do |salepoint|
      salepoint.last[:salepoint]
    end
    result.select { |attributes| !!attributes[:store_id] }
  end

  class SalepointTransformer

    def initialize(salepoint_hash)
      @salepoint_hash = ActionController::Parameters.new(salepoint_hash)
    end

    def self.transform(salepoint_hash)
      new(salepoint_hash).salepoint
    end

    def salepoint
      if does_not_exist?
        Salepoint.new(@salepoint_hash.permit(:variable_price_id, :store_id, :has_rights_assignment))
      else
        updated
      end
    end

    protected

    def does_not_exist?
      @salepoint_hash[:id].blank?
    end

    def existing
      Salepoint.find(@salepoint_hash[:id])
    end

    def updated
      result = existing
      result.attributes = @salepoint_hash.permit(:variable_price_id, :store_id, :has_rights_assignment)
      result
    end
  end
end
