module TcDistributor
  class Base
    attr_accessor :album_id, :distributions, :release_id, :releases_store_id, :releases_store_ids

    def initialize(album_id: nil, distributions: nil, release_id: nil, releases_store_id: nil, releases_store_ids: nil, monetization_song_ids: nil)
      @album_id = album_id
      @distributions = distributions
      @release_id = release_id
      @releases_store_id = releases_store_id
      @releases_store_ids = releases_store_ids
      @monetization_song_ids = monetization_song_ids
    end

    def tc_distributor_metadata
      {
        latest_release: latest_release,
        latest_releases_stores: latest_releases_stores,
        latest_releases_stores_tracks: latest_releases_stores_tracks
      }
    end

    def xml_metadata
      resp = DistributorAPI::ReleasesStore.list(id: releases_store_id)
      resp[0].http_body.dig("releases_stores", -1, "metadata_xml_path")
    end

    def json_metadata
      resp = DistributorAPI::Release.list(id: release_id)
      resp[0].http_body.dig("releases", -1, "release_json_path")
    end

    def releases_stores
      resp = DistributorAPI::ReleasesStore.list(id: releases_store_ids)
      resp[0].http_body.dig("releases_stores")
    end

    def latest_releases_stores
      api_response = DistributorAPI::Album.latest_releases_stores(
        album_id,
        {
          store_id: store_ids_from_distributions
        }
      )

      releases_stores = response_body_for(api_response: api_response)&.releases_stores

      release_stores_for_distribution(releases_stores: releases_stores)
    end

    def latest_releases_stores_tracks
      api_response = DistributorAPI::Album.latest_releases_stores_tracks(
        album_id,
        {
          track_ids: @monetization_song_ids
        }
      )

      releases_stores_tracks = response_body_for(api_response: api_response)&.releases_stores
    end

    private

    def latest_release
      api_response = DistributorAPI::Album.latest_release(album_id)

      response_body_for(api_response: api_response)&.release
    end

    def response_body_for(api_response:)
      JSON.parse(api_response[0].http_body.to_json, object_class: OpenStruct)
    end

    def store_ids_from_distributions
      store_to_distribution_ids_hash.keys.uniq
    end

    def store_to_distribution_ids_hash
      @store_to_distribution_ids_hash ||=
        Array(distributions).each_with_object({}) do |distro, h|
          store_id = distro.salepoints&.first&.store&.id
          h[store_id] = distro.id unless store_id.nil?
        end
    end

    def release_stores_for_distribution(releases_stores:)
      store_metadata = releases_stores.index_by(&:store_id)
      store_metadata.transform_keys { |store_id| store_to_distribution_ids_hash[store_id] }
    end
  end
end
