class QuirkyJson
  def self.load(arg)
    JSON.parse(parse_arg(arg), quirks_mode: true) || {}
  end

  def self.dump(arg)
    JSON.dump(arg)
  end

  private

  def self.parse_arg(arg)
    arg.blank? ? {}.to_json : arg
  end
end
