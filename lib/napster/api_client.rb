# frozen_string_literal: true

class Napster::ApiClient
  class NapsterApiUrlEnvNotSetError < StandardError; end
  class NapsterApiKeyEnvNotSetError < StandardError; end

  attr_accessor :http_client, :isrc

  def initialize(isrc)
    @isrc        = isrc
    @http_client = Faraday.new(url: request_url,
                               headers: {
                                 'apikey' => napster_api_key
                               })
  end

  def fetch_data
    resp      = http_client.get
    resp_body = JSON.parse(resp.body)

    return if resp_body['tracks'].nil?

    tracks = resp_body['tracks']&.first
    return if tracks.blank?

    tracks['id']
  end

  private

  def request_url
    "#{napster_api_url}#{isrc}"
  end

  def napster_api_key
    ENV.fetch('NAPSTER_API_KEY') do
      raise NapsterApiKeyEnvNotSetError, "NAPSTER_API_KEY Env is not Set!"
    end
  end

  def napster_api_url
    ENV.fetch('NAPSTER_API_URL') do
      raise NapsterApiUrlEnvNotSetError, "NAPSTER_API_URL Env is not Set!"
    end
  end
end
