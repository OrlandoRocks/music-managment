class GoogleRecaptcha
  BASE_URL   = "https://www.google.com/".freeze
  VERIFY_URL = "recaptcha/api/siteverify".freeze

  def initialize(use_v3=false)
    @client = Faraday.new(BASE_URL)
    @use_v3 = use_v3
  end

  def verify_recaptcha(params)
    response = perform_verify_request(params)
    success?(response)
  end

  def success?(response)
    parsed_res = JSON.parse(response.body)

    if @use_v3
      parsed_res['score'] > ENV['RECAPTCHA_THRESHOLD'].to_f
    else
      parsed_res['success']
    end
  end

  private

  attr_reader :client

  def perform_verify_request(params)
    client.post(VERIFY_URL) do |req|
      if @use_v3
        req.params = params.merge({secret: ENV["RECAPTCHA_V3_SECRET_KEY"]})
      else
        req.params = params.merge({secret: ENV["RECAPTCHA_SECRET_KEY"]})
      end
    end
  end
end
