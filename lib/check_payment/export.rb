

module CheckPayment
  class Export

    EMAILS = []
    def EMAILS.set(*emails)
      clear()
      concat emails
    end

    attr_reader :time
    attr_accessor :size
    attr_accessor :total_cents

    def initialize(dir = File.expand_path(File.join(Rails.root, "tmp", "checktransfers")))
      @time = Time.new
      @dir = dir
      @total_cents = 0
    end

    def empty?
      !(@size && @size > 0)
    end

    def total_string
      sprintf("$%.2f", @total_cents.to_f / 100)
    end

    def run
      book = Spreadsheet::Workbook.new
      checks = book.create_worksheet :name => 'Checks'
      export = self
      CheckPayment.instance_eval do
        #add headers
        checks.row(0).concat @columns.map {|c| c.name}
        outstanding = CheckTransfer.where('export = TRUE AND export_date IS NULL').order("country DESC")
        export.size = outstanding.length
        return if (export.empty?)

        outstanding.each_with_index do |t, i|
          export.total_cents += t.payment_cents
          checks.row(i + 1).concat @columns.map {|c| c.data_for(t)}
          t.export_date = export.time
          t.save!
        end



      end

      FileUtils.mkdir_p @dir
      book.write self.filename
    end

    def filename
      File.join(@dir, sprintf("%4d-%02d-%02d.xls", @time.year, @time.month, @time.day))
    end
  end
end
