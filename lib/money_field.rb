module MoneyField
  def money_reader(*args)
    args.each do |money_name|
      module_eval(<<-EOS, "(__MONEY_READER__)", 1)
        def #{money_name}
          Money.new(#{money_name}_cents.to_i, self.currency)
        end
      EOS
    end
  end

  def money_writer(*args)
    args.each do |money_name|
      module_eval(<<-EOS, "(__MONEY_WRITER__)", 1)
        def #{money_name}=(money_value)
          self.#{money_name}_cents = money_value.cents
        end
      EOS
    end
  end
  
  def money_accessor(*args)
    money_reader(*args)
    money_writer(*args)
  end

  protected :money_reader, :money_writer, :money_accessor
end