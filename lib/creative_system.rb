module CreativeSystem

  module Controller

    protected

    #
    #  Setup options for a select box with the given roles
    #
    def creative_roles_for_select(roles)
      @creative_roles = roles.map { |role| [role.titleize, role.underscore] }
    end

    #
    #  Send users over the correct type's controller
    #
    def redirect_to_polymorphic_type(object)
      if ! is_polymorphic_type?(object)
        redirect_to polymorphic_path(object, request.query_parameters)
      end
    end

    #
    #  Use as a filter in the controller
    #  to make sure that singles do not
    #  get shown in the album view
    #
    def is_polymorphic_type?(record)
      return true if ! record

      class_name = record.class.name.downcase
      !!(request.fullpath =~ /#{class_name}/)
    end

  end

end
