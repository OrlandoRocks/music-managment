module Translatable
  def self.included(base)
    base.extend ClassMethods
  end

  def formatted_model_name
    self.class.name.underscore
  end

  def formatted_attribute_value(attr)
    read_attribute(attr.to_sym).parameterize.underscore
  end

  def attribute_value_exists?(attr)
    !read_attribute(attr.to_sym).blank?
  end


  module ClassMethods
    def attr_translate(attr)
      define_method(attr) do
        return unless attribute_value_exists?(attr)

        if self.instance_of?(Product) && Product::PLAN_RELATED_PRODUCT_IDS.include?(self.id) && attr.to_sym == :description
          I18n.t("#{formatted_model_name}.description.#{formatted_attribute_value :name}", raise: false)
        else
          I18n.t("#{formatted_model_name}.#{attr}.#{formatted_attribute_value attr}", raise: false)
        end
      end

      define_method("#{attr}_raw") do
        return unless attribute_value_exists?(attr)
        read_attribute(attr.to_sym)
      end

      define_method("#{attr}_safely_translated") do
        translated_value = public_send(attr)

        return translated_value unless translated_value.to_s.include?("translation missing:")

        public_send("#{attr}_raw")
      end
    end
  end
end
