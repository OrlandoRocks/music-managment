# frozen_string_literal: true

# Error raised when a function is not found
class RulesEngine::RuleMissing < RulesEngine::RulesEngineError
  def initialize(rule_name)
    message = "Unknown rule `#{rule_name}`"
    super(message)
  end
end
