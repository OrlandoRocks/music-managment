# frozen_string_literal: true

# Base class for RulesEngineError
class RulesEngine::RulesEngineError < StandardError
  attr_reader :lisp_stack_trace

  def initialize(*args)
    super

    @lisp_stack_trace = []
  end

  def to_s
    ret = super

    lisp_stack_trace.each do |s_expression|
      ret += "\n  in s-expression"
      ret += " \e[36m#{s_expression}\e[0m"
    end

    ret
  end
end
