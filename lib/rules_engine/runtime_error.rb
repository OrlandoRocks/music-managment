# frozen_string_literal: true

# Error raised when evaluation of a Conflisp expression fails
class RulesEngine::RuntimeError < RulesEngine::RulesEngineError
  attr_reader :original_error

  def initialize(error, s_expression)
    message = "#{error.class.name}: #{error.message}"
    message += "\n  while evaluating \e[31m#{s_expression}\e[0m"
    super(message)

    @original_error = error
  end
end