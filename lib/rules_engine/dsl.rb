# frozen_string_literal: true

class RulesEngine::DSL
  attr_reader :registry

  def self.define(&block)
    registry = {}
    dsl = new(registry: registry)
    dsl.instance_exec(&block)
    registry
  end

  def initialize(registry:)
    @registry = registry
  end

  def rule(name, implementation)
    registry[name.to_sym] = implementation
  end
end
