# frozen_string_literal: true

# A collection of rules that can evaluate JSON encoded s-expressions
class RulesEngine::RuleRegistry
  attr_accessor :registry

  def self.define_rules(&block)
    method_registry = RulesEngine::DSL.define(&block)
    new(registry: method_registry)
  end

  def initialize(registry:)
    @registry = registry
  end

  def evaluate(s_expression, globals: {})
    evaluator = RulesEngine::Evaluator.new(registry: registry, globals: globals)
    evaluator.resolve(s_expression)
  end

  def extend(&block)
    new_registry = RulesEngine::DSL.define(&block)
    self.class.new(registry: registry.merge(new_registry))
  end
end