# frozen_string_literal: true

# Evaluator takes s-expressions and resolves them into Ruby values
class RulesEngine::Evaluator
  attr_reader :registry, :globals

  def initialize(registry:, globals:)
    @registry = registry
    @globals = globals
  end

  # rubocop:disable Metrics/MethodLength
  def resolve(s_expression)
    case s_expression
    when Array
      # In Lisp, Arrays are function calls
      rule_name, *raw_args = s_expression
      rule_name = rule_name.to_sym
      args = raw_args.map { |arg| resolve(arg) }
      if rule_name == :global
        globals.dig(*args)
      else
        apply(rule_name, *args)
      end
    when Hash
      s_expression.transform_values do |value|
        resolve(value)
      end
    else
      s_expression
    end
  rescue RulesEngine::RulesEngineError => error
    error.lisp_stack_trace << s_expression
    raise error
  end
  # rubocop:enable Metrics/MethodLength

  def apply(rule_name, *args)
    method = registry[rule_name.to_sym]
    raise RulesEngine::RuleMissing, rule_name unless method

    begin
      instance_exec(*args, &method)
    rescue StandardError => e
      raise RulesEngine::RuntimeError.new(e, [rule_name.to_sym, *args])
    end
  end
end
