class TransactionalEmailFaker
  def fake_hubspot_email_client
    email_path = "http://fake.hubspot.api/whut?hapikey=12345"
    Faraday.new("http://fake.hubspot.api/whut") do |builder|
      builder.params[:hapikey] = 12345
      builder.headers["Content-Type"] = "application/json"
      builder.adapter :test do |stubs|
        stubs.post(email_path) do |env|
          correct_config?(env) ? success_response : error_response
        end
      end
    end
  end

  def success_response
    [200, { "success" => true }, { message: "success", sendResult: "valid API request"}.to_json ]
  end

  def error_response
    [400, {}, {message: "error", sendResult: "required request params missing"}.to_json ]
  end

  def correct_config?(env)
    body = JSON.parse(env[:body])
    body["emailId"].present? && body["message"].fetch("to", nil).present? && body["customProperties"].present?
  end
end
