class TrendsDatabase
  def self.run(username, password, database, file)
    puts "\nTrends DB task about to load this file: #{file}\n"
    command = "mysql"
    command << " -h #{TRENDS_CONNECTION_INFO[:host]}"
    command << " -u#{username}" if username
    command << " -p'#{password}'" if password
    command << " #{database} < #{file}"
    puts `#{command}`
  end

  def self.create_trends_tables
    return true if !Rails.env.development? && !Rails.env.test?

    # Create federated database seperate from main database
    ActiveRecord::Base.connection.execute("DROP DATABASE IF EXISTS `#{TRENDS_DB}`")
    ActiveRecord::Base.connection.execute("CREATE DATABASE `#{TRENDS_DB}`")
    ActiveRecord::Base.establish_connection(TRENDS_CONNECTION_INFO)

    config    = TRENDS_CONNECTION_INFO
    database  = config[:database]
    username  = config[:username]
    password  = config[:password]

    run(username, password, database, "#{Rails.root}/db/trends/trends_schema.sql")
    run(username, password, database, "#{Rails.root}/db/trends/#{Zipcode.table_name}.sql")
    run(username, password, database, "#{Rails.root}/db/trends/#{Provider.table_name}.sql")
    run(username, password, database, "#{Rails.root}/db/trends/#{TrendsCountry.table_name}.sql")
    run(username, password, database, "#{Rails.root}/db/trends/#{TransType.table_name}.sql")

    puts "All Trends Tables Created"
    ActiveRecord::Base.remove_connection
    ActiveRecord::Base.establish_connection(ActiveRecord::Base.configurations[Rails.env])
  end
end
