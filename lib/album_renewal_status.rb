class AlbumRenewalStatus

  def self.status? (album)
    new(album).status?
  end

  def initialize (album)
    @person                       = album.person
    @album                        = album
    @renewal                      = album.renewal
    @expiration_date              = @renewal.expires_at
    @renewal_canceled             = !@renewal.canceled_at.nil?
    @renewal_expired              = @expiration_date <= Time.now
    @has_credit_card              = !!@person.stored_credit_cards.active[0]
    @card_expires_before_renewal  = @has_credit_card && @person.stored_credit_cards.active[0].expires_before_date?(@expiration_date)
    @renewal_coming_up            = Time.now >= @expiration_date - 4.weeks
  end

  def status?
    status = statuses.detect { |k,v| v }
    status[0].to_s if status
  end

  private

  def statuses
    {
      taken_down:                       @renewal.taken_down?    && !@renewal_canceled,
      take_down_requested:              @renewal_canceled       && @renewal_expired,
      take_down_requested_not_expired:  @renewal_canceled       && !@renewal_expired,
      renewal_due_grace_period:         @person.autorenew?      && @renewal_expired && @person.legacy_user?,
      renewal_due_no_cc:                @person.autorenew?      && !@has_credit_card  && @renewal_coming_up && @person.legacy_user?,
      expired_card:                     @person.autorenew?      && @has_credit_card   && @card_expires_before_renewal && @renewal_coming_up && @person.legacy_user?,
      renewal_due:                      @person.autorenew?      && @has_credit_card   && @renewal_coming_up && @person.legacy_user?,
      renewal_due_autorenew_disabled:   !@person.autorenew?     && @renewal_coming_up && @person.legacy_user?,
      live_on_stores:                   @album.state == "live"  || @album.live_itunes_preorder,
      sent_to_stores:                   @album.state == 'sent'
    }
  end
end
