# frozen_string_literal: true

# Apple MusicKit API client
class AppleMusic::ApiClient
  class ApiError < StandardError; end

  ALBUMS_ENDPOINT    = 'https://api.music.apple.com/v1/catalog/us/albums'
  SEARCH_ENDPOINT    = 'https://api.music.apple.com/v1/catalog/us/search'
  ARTIST_ID_ENDPOINT = 'https://api.music.apple.com/v1/catalog/us/artists/'

  DUMMY_ALBUM = { 'attributes' => { 'artwork' => { 'url' => '' }.freeze, 'name' => '' }.freeze }.freeze

  SEARCH_ERROR_RESPONSE = [].freeze
  API_ERROR_RESPONSE    = { error: { status: 500 }.freeze }.freeze
  ID_ERROR_RESPONSE     = { error: { status: 404 }.freeze }.freeze

  attr_reader :client

  def initialize
    @client = Faraday.new(headers: { 'Authorization': "Bearer #{APPLE_MUSIC_TOKEN}" })
  end

  # example: https://api.music.apple.com/v1/catalog/us/search?term=vektroid&types=artists&limit=10
  def find_by_artist_name(term)
    response = client.get(SEARCH_ENDPOINT) do |req|
      req.params.merge!('limit': 10,
                        'term': term,
                        'types': 'artists')
    end

    artists = parse_search_results(response, term)
    return SEARCH_ERROR_RESPONSE if artists.blank?

    artists_with_top_albums(artists)
  rescue ApiError => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    SEARCH_ERROR_RESPONSE
  end

  # example: https://api.music.apple.com/v1/catalog/us/artists/178834
  def get_artist_by_id(id, name)
    response = client.get("#{ARTIST_ID_ENDPOINT}#{id}")
    return ID_ERROR_RESPONSE if id_error?(response)

    artists = parse_get_artist_by_id(response, name)
    return ID_ERROR_RESPONSE if artists.empty?

    artists_with_top_albums(artists)[0]
  rescue ApiError => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace)
    API_ERROR_RESPONSE
  end

  private

  def formatted_error(response)
    response.inspect
  end

  def api_error?(response)
    response.status != 200
  end

  def id_error?(response)
    response.status == 404
  end

  def filter_invalid_results(artist, name)
    Apple::ArtistIdValidator.valid?({
      apple_name: artist['attributes']['name'],
      check_id: false,
      name: name
    }.with_indifferent_access)
  end

  def parse_search_results(response, name)
    raise ApiError, "AppleMusic API error #{formatted_error(response)}" if api_error?(response)

    JSON.parse(response.body)
        .dig('results', 'artists', 'data')
        &.select { |a| filter_invalid_results(a, name) }
  end

  def parse_get_artist_by_id(response, name)
    raise ApiError, "AppleMusic API error #{formatted_error(response)}" if api_error?(response)

    JSON.parse(response.body)['data']
        .select { |a| filter_invalid_results(a, name) }
  end

  # example: https://api.music.apple.com/v1/catalog/us/albums?ids=310730204,19075891
  def fetch_albums(ids)
    response = client.get(ALBUMS_ENDPOINT) { |req| req.params.merge!('ids': ids.join(',')) }
    raise ApiError, "AppleMusic API error #{formatted_error(response)}" if api_error?(response)

    JSON.parse(response.body)['data']
  end

  def thin_artist_hash(artist)
    artist['relationships'].delete('albums')

    if artist['relationships']['topAlbum']['relationships']
      artist['relationships']['topAlbum'].delete('relationships')
    end

    artist
  end

  def artists_with_top_albums(artists)
    return SEARCH_ERROR_RESPONSE if artists.blank?

    top_album_ids = artists.map { |a| parse_top_album(a) }
    albums = fetch_albums(top_album_ids.compact)

    artists.map do |artist|
      set_top_album(artist, albums)
      thin_artist_hash(artist)
    end
  end

  def parse_top_album(artist)
    artist.dig('relationships', 'albums', 'data', 0, 'id')
  end

  def set_top_album(artist, albums)
    top_album_id = parse_top_album(artist)
    matching_fetched_album = match_fetched_album(albums, top_album_id)

    artist['relationships']['topAlbum'] = matching_fetched_album || DUMMY_ALBUM
  end

  # API response de-dupes album requests, so we can't zip/index artists to albums. Dupes are common.
  def match_fetched_album(albums, top_album_id)
    albums.find { |a| a['id'] == top_album_id }
  end
end
