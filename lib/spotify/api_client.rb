class Spotify::ApiClient
  attr_accessor :token, :client

  API_ERROR        = { error: { status: 500 } }.freeze
  ID_ERROR         = { error: { status: 403 } }.freeze
  ErrorResponse    = Struct.new(:body, :success?, :status)
  APIErrorResponse = ErrorResponse.new(API_ERROR.to_json, false, API_ERROR[:error][:status])
  IDErrorResponse  = ErrorResponse.new(ID_ERROR.to_json, false, ID_ERROR[:error][:status])

  def initialize
    @client = Faraday.new
    @client.token_auth(get_token)
    @client.url_prefix = SPOTIFY_API[:base]
  end

  def execute_request(&block)
    response = block.call

    if !response.success? && (token_expired?(response) || limit_exceeded?(response))
      refresh_token
      execute_request(&block)
    elsif !response.success?
      handle_failure(response)
    end

    response
  end

  def find_by_upc(upc)
    response = execute_request do
      @client.get("search") do |req|
        req.params = { q: "upc:#{upc}", type: "album" }
      end
    end

    response_json = JSON.parse(response.body)
    album_jsons = response_json["albums"].fetch("items", []) if response_json["albums"].present?

    return if album_jsons.blank?
    return album_jsons.first if album_jsons.one?

    album = Album.joins(:upcs).find_by(upcs: { number: upc })
    album_json = album_jsons.find{ |json_item| json_item["name"] == album&.name }

    album_json.presence || album_jsons.first
  end

  def find_by_artist_name(name)
    response = execute_request do
      @client.get("search") do |req|
        req.params = { q: "#{name}", type: "artist", limit: 10 }
      end
    end

    response_json = JSON.parse(response.body)

    return response_json unless response_json["artists"].present?

    artists = response_json["artists"].fetch("items", [])
    artists.select do |a|
      Spotify::ArtistIdValidator.valid?(name: name, id: nil, spotify_name: a['name'])
    end
  rescue Spotify::ArtistIdValidator::ArtistIdValidatorError => _e
    APIErrorResponse
  end

  def get_artist_by_id(id)
    @client.get("artists/#{id}")
  end

  def get_valid_artist_by_id(id, name)
    response = get_artist_by_id(id)
    return response unless response.success?

    spotify_name = JSON.parse(response.body)['name']
    valid = Spotify::ArtistIdValidator.valid?(name: name, id: nil, spotify_name: spotify_name)

    return response if valid

    IDErrorResponse
  rescue Spotify::ArtistIdValidator::ArtistIdValidatorError => _e
    APIErrorResponse
  end

  def spotify_recent_albums(artist_id)
    response = execute_request do
      @client.get("artists/#{artist_id}/albums") do |req|
        req.params = { country: 'US', limit: 1 }
      end
    end

    response_json = JSON.parse(response.body)
    response_json["items"] if response_json["items"].present?
  end

  def find_by_isrc(isrc)
    response = execute_request do
      client.get("search") do |req|
        req.params = { q: "isrc:#{isrc}", type: "track" }
      end
    end

    response_json = JSON.parse(response.body)
    response_json.dig("tracks", "items", 0, "uri")
  end

  def verify_album(spotify_album_array, tunecore_upc)
    uris         = spotify_album_array.map{ |alb| alb["id"] }
    album_by_uri = get_full_albums(uris)
    return nil unless album_by_uri.present?
    album_by_uri.select{ |alb| alb["external_ids"]["upc"] == tunecore_upc }.first
  end

  def get_full_albums(album_uris=[])
    param_string = album_uris.join(",")

    response = execute_request do
      @client.get("albums") do |req|
        req.params = {
          ids: param_string
        }
      end
    end

    response_json = JSON.parse(response.body)
    response_json["albums"].fetch("items", [])
  end

  def get_album_tracks(album_uri)
    response = execute_request do
      @client.get("albums/#{album_uri}")
    end
    track_response = JSON.parse(response.body)
    return nil if track_response["tracks"].blank? || track_response["tracks"]["items"].blank?

    track_uris = track_response["tracks"]["items"].map{|i| i["id"]}
    response_json = get_multiple_tracks(track_uris)
    response_json["tracks"]
  end

  def get_multiple_tracks(track_uris=[])
    param_string = track_uris.join(",")

    response = execute_request do
      @client.get("tracks") do |req|
        req.params = {
          ids: param_string
        }
      end
    end
    response_json = JSON.parse(response.body)
    response_json
  end

  def get_token
    @client.basic_auth(SPOTIFY_API[:client_id], SPOTIFY_API[:client_secret])
    token_request = @client.post do |req|
      req.url SPOTIFY_API[:token_request]
      req.body = { :grant_type => "client_credentials" }
    end

    body = JSON.parse(token_request.body)
    token_request.status == 200 ? "#{body["token_type"]} #{body["access_token"]}" : log_error(body)
  end

  def limit_exceeded?(response)
    response_json = JSON.parse(response.body)

    response_expired = (response.status == 429 && response_json["error"]["message"].include?("exceeded"))
    retry_after = response["retry-after"].to_i || 5
    sleep(retry_after) if response_expired
    response_expired
  end

  def token_expired?(response)
    response_json = JSON.parse(response.body)
    response.status == 401 && response_json["error"]["message"].include?("expired")
  end

  def handle_failure(response)
    Tunecore::Airbrake.notify("Spotify External Ids Error", response: response)
  end

  def refresh_token
    @client.token_auth(get_token)
  end

  def get_all_album_tracks(album_uri)
    response = execute_request do
      @client.get("albums/#{album_uri}/tracks")
    end

    JSON.parse(response.body)
  end

  def find_artists_by_album_uri(album_uri)
    response = get_all_album_tracks(album_uri)
    return unless response["items"].present?
    all_artists = {}
    response["items"].each do |track|
      if track["artists"].present?
        track["artists"].each do |artist|
          next unless artist["name"].present? && artist["id"].present?
          all_artists[artist["name"].downcase] = artist["id"]
        end
      end
    end

    all_artists
  end

  def log_error(msg)
    Rails.logger.debug(msg)
  end
end
