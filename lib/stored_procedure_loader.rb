class StoredProcedureLoader
  DB_CONFIG = ActiveRecord::Base.connection_config

  def self.load
    `#{connect_string} < #{File.join(Rails.root, "db", "stored_procedures", "withhold_encumbrances.sql")}`
    `#{connect_string} < #{File.join(Rails.root, "db", "stored_procedures", "create_person_intake.sql")}`
    `#{connect_string} < #{File.join(Rails.root, "db", "stored_procedures", "populate_sales_record_summaries.sql")}`
  end

  def self.connect_string
    connect_string = "mysql #{DB_CONFIG[:database]} -h #{DB_CONFIG[:host]} --user=#{DB_CONFIG[:username]} "
    connect_string += "--password=#{DB_CONFIG[:password]}" if DB_CONFIG[:password]
  end
end
