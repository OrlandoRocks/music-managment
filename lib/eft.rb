module Eft
  include Logging
  EFT = "EFT Batch".freeze

  def upload_eft_csv_to_braintree(batch_id)
    batch = EftBatch.find(batch_id)
    filename = batch.batch_file_name
    filepath = BRAINTREE_BATCH[:local_uploads] + filename
    number_of_lines = `grep #{filename} #{filepath} | wc -l`
    log(EFT, 'Count of lines in upload csv', count: number_of_lines)
    if number_of_lines.to_i > 0
      curl_text = `curl #{BRAINTREE_UPLOAD} #{filepath}`
      log(EFT,'Curl Output', batch: batch.id, file_sent: filename, curl: curl_text)
      batch.update(batch_sent_at: Time.now, status: 'uploaded')
      log(EFT, 'File uploaded to Braintree', batch: batch.id,file_sent: filename, amount: batch.total_amount)
    else
      log(EFT, 'File not uploaded to Braintree since there were no transactions', batch: batch.id, file_sent: filename, amount: batch.total_amount)
    end
  end

  def create_eft_batch(transactions)
    batch = EftBatch.create
    filename = 'tunecore_eft_batch_'+ Time.now.strftime("%Y_%m_%d_#{batch.id}_%H%M%S") + '.csv'

    #Create a new EFT Batch
    batch.update(batch_file_name: filename, response_file_name: 'response-'+ filename)

    transactions.each do |txn|
      #update to processing_in_batch and associate with batch
      txn.update(status: 'processing_in_batch', eft_batch_id: batch.id) unless txn.status != 'waiting_for_batch' # protect against a customer clicking cancel at the same time we process
      if txn.status == 'processing_in_batch'
        EftBatchTransactionHistory.create(eft_batch_transaction_id: txn.id, status: 'processing', comment: "EFT withdrawal picked up for batch processing")
      else
        log(EFT,'Customer clicked "cancel" during the processing', eft_batch_transaction: txn.id, status: txn.status)
      end
    end

    batch
  end

  def create_csv_from_batch(batch)
    return if batch.batch_file_name.blank?
    filepath = BRAINTREE_BATCH[:local_uploads] + batch.batch_file_name

    #iniitate totals for logging purposes
    running_total, count = 0.0, 0

    CSV.open(filepath, "w") do |csv|
      batch.eft_batch_transactions.each do |txn|
        if txn.status == 'processing_in_batch'
          # we specify the order of the fields in the csv we send to braintree
           csv << [
                   txn.stored_bank_account.customer_vault_id, # 0 cc_vault_id
                   'credit',                                   # 1 type
                   txn.amount,                               # 2 amount
                   txn.order_id,                             # 3 order_id
                   txn.stored_bank_account.person_id,        # 4 person_id
                   '',                                       # 5 batch_transaction_id
                   '',                                       # 6 batch_id
                   '',                                       # 7 filename
                   'check',                                  # 8 payment_method (this defaults to 'creditcard' if not passed in)
                   txn.eft_batch_id,                         # 9 eft_batch_id
                   txn.id,                                   # 10 eft_batch_transaction_id
                   batch.batch_file_name                     # 11 eft_batch_filename
                 ]

            count += 1
            running_total +=txn.amount

            log(EFT, 'EFT Batch Transaction Added to csv for upload', eft_batch: txn.eft_batch_id, file_sent: batch.batch_file_name, eft_batch_transaction: txn.id,
                person: txn.stored_bank_account.person_id, order: txn.order_id, amount: txn.amount)
        end
      end
    end

    batch.update(total_amount: running_total)
    log(EFT,'EFT csv created', eft_batch: batch.id, filename: batch.batch_file_name, count: count, amount: running_total)
    return batch
  end

  def download_eft_csv_from_braintree(batch_id)
    batch = EftBatch.find(batch_id)
    filename = batch.response_file_name
    internal_path = BRAINTREE_BATCH[:local_downloads] + filename
    braintree_amount_successful = 0
    # download file from braintree
    log(EFT, 'Attempt to download file from braintree', batch: batch.id, file_received: batch.response_file_name)
    curl_text = `curl #{BRAINTREE_DOWNLOAD}/#{filename} -o #{internal_path}`
    log(EFT, 'Curl Output', batch: batch.id,file_sent: filename, curl: curl_text)
    return internal_path
  end

  def process_eft_download_from_braintree(batch_id)
    batch = EftBatch.find(batch_id)
    # initialize amounts/counts for logging purposes
    @errored_amount, @sent_to_bank_amount, @total_count, @errored_count, @sent_to_bank_count = 0.0,0.0,0,0,0

    internal_path = BRAINTREE_BATCH[:local_downloads] + batch.response_file_name
    log(EFT, 'Successfully download response from Braintree', batch: batch.id, file_received: batch.response_file_name, amount: batch.total_amount)
    #go through each response row and create an update the eft_batch_transaction record
    CSV.foreach(internal_path) do |row|
      eft_batch_transaction = EftBatchTransaction.where(id: row[46]).first
      if !eft_batch_transaction.nil?
        transaction = process_eft_batch_response_row(batch, eft_batch_transaction, row)
        @total_count += 1
      else
        message = log(EFT, 'Error - response row does not match eft transaction in our system', batch: batch.id, file_received: batch.response_file_name, eft_batch_transaction: row[41])
        EftNotifier.admin_error_notice(message).deliver
      end
    end
    #mark batch as upload_confirmed
    batch.update(status: 'upload_confirmed', sent_to_bank_amount: @sent_to_bank_amount, errored_amount: @errored_amount)
    log(EFT, 'Braintree EFT Totals', batch: batch.id, file_received: batch.response_file_name,count_from_db: batch.eft_batch_transactions.size ,count: @total_count, amount: batch.total_amount)
    log(EFT, 'Braintree EFT Breakdown', sent_to_bank_count: @sent_to_bank_count, sent_to_bank_amount: @sent_to_bank_amount, errored_count: @errored_count, errored_amount: @errored_amount)
  end

  def process_eft_batch_response_row(batch, eft_batch_transaction, row)
    transaction_id = row[0]
    response_code = row[37]
    response_text = row[2]
    amount = row[6].to_f

    if !eft_batch_transaction.nil?
      if response_code == '100' # success code
        eft_batch_transaction.sent_to_bank(transaction_id)
        @sent_to_bank_amount += amount
        @sent_to_bank_count += 1
      else
        # treat all non 100 code as error but log unrecognizeable response code
        eft_batch_transaction.error(transaction_id, response_code, response_text)
        @errored_amount += amount
        @errored_count += 1
        unless EftBatchTransaction::KNOWN_ERROR_RESPONSE_CODE.include?(response_code)
          message = log(EFT, 'Error - unrecognizable error response code', eft_batch_transaction_id: eft_batch_transaction.id, filename: batch.response_file_name, response_code: response_code )
          EftNotifier.admin_error_notice(message, eft_batch_transaction.id).deliver
        end
      end
    else
      message = log(EFT, 'Error - could not find associated Eft Batch Transaction', transaction_id: transaction_id, filename: batch.response_file_name, response_code: response_code )
      EftNotifier.admin_error_notice(message).deliver
    end
  end

  def get_formatted_date_range(end_day)
    case end_day
      when String then end_day = Date.parse(end_day)
      when Date,Time then end_day
      else raise "Must pass a string or a date to the fuction"
    end

    # run transactions for the day before, and on monday, run Friday,Saturday, Sunday
    start_day = (end_day.wday == 0) ? (end_day - 2.day) : end_day

    @start_date = "#{start_day.strftime('%Y%m%d')}000000"
    @end_date   = "#{end_day.strftime('%Y%m%d')}235959"
  end

  def query_eft_transactions(day)
    get_formatted_date_range(day)
    transaction_type = 'ck'
    condition = 'complete,failed' # use 'pendingsettlement' for testing with the braitnree test account

    query = Tunecore::Braintree::Query.new
    raw_response = query.execute(transaction_type: transaction_type, condition: condition, start_date: @start_date, end_date: @end_date)
    raw_query = "#{BRAINTREE[:query_api_url]}?transaction_type=#{transaction_type}&condition=#{condition}&start_date=#{@start_date}&end_date=#{@end_date}"
    eft_query = EftQuery.create(raw_query: raw_query)
    response_hash = raw_response['transaction']
    process_response_hash(response_hash, eft_query)
  end

  def process_response_hash(response_hash, eft_query)
    if response_hash.present?
      response_hash = [response_hash] if response_hash.is_a?(Hash)
      response_hash.each do |transaction|
        eft_batch_transaction = EftBatchTransaction.where(transaction_id: transaction['transaction_id']).first
        if eft_batch_transaction.present?
          if eft_batch_transaction.can_be_updated_by_bank?
            if transaction['condition'] ==  'complete' # use for testing: 'pendingsettlement' && transaction['transaction_id'].to_i % 30 != 0
              eft_batch_transaction.success(transaction['action']['success'], transaction['action']['response_text'], eft_query)
            elsif transaction['condition'] == 'failed' && transaction['action']['success'] == "1"
              eft_batch_transaction.failure(transaction['action']['success'], transaction['action']['response_text'], eft_query)
            elsif transaction['condition'] == 'failed' && transaction['action']['success'] == "0"
              # do nothing, since these are transactions that errored out during the initial braintree check
              # currently braintree does not support eliminating these from the query
            else
              # Transaction had a status other than 'complete' or 'failed'
              message = log(EFT, 'Error - transaction condition does not match "complete" or "failure"',
                            transaction_id: transaction['transaction_id'], order_id: transaction['order_id'], condition: transaction['condition'])
              EftNotifier.admin_error_notice(message).deliver
            end
          else
            unless eft_batch_transaction.failed?
              # Transaction has a status other than processing in batch (could be a duplicate transaction processing)
              message = log(EFT, 'Error - Eft Batch Transaction has a status other than sent_to_bank or success or one of the failed statuses (could be a duplicate transaction processing)', order_id: transaction['order_id'],
                          eft_batch_transaction_id: eft_batch_transaction.id, status: eft_batch_transaction.status)
              EftNotifier.admin_error_notice(message).deliver
            end
          end
        else
          # Transaction not in our database
          message = log(EFT, 'Error - Eft Batch Transaction returned by Braintree was not found in our database',
                        transaction_id: transaction['transaction_id'], order_id: transaction['order_id'])

          EftNotifier.admin_error_notice(message).deliver
        end
      end

      finalize_eft_query(eft_query)
    else
      eft_query.destroy
      log(EFT, 'No transactions returned by Query, DB query record destroyed')
    end
  end

  def finalize_eft_query(eft_query)
    if eft_query.eft_batch_transactions.present?
      EftNotifier.daily_query_summary(eft_query.id).deliver
    else
      eft_query.destroy
    end
  end

  def mark_batches_as_complete
    incomplete_batches = EftBatch.where(status: "upload_confirmed")

    if incomplete_batches.present?
      incomplete_batches.each do |batch|
        if batch.completed?
          batch.update(status: 'complete')
          log(EFT, 'Eft Batch is complete', batch_id: batch.id)
        else
          log(EFT, 'Eft Batch was not complete', batch_id: batch.id)
        end
      end
    else
      log(EFT, 'No Batches are incomplete')
    end
  end

  def test_query_braintree_from_xml_file(file_path)
    file = File.open(file_path, 'r')
    hash = Hash.from_xml(file)['nm_response']
    response_hash = hash['transaction']
    eft_query = EftQuery.create
    process_response_hash(response_hash, eft_query)
  end
end
