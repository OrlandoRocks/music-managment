module SalepointsWithInventoryChecks
  def salepoints=(values)
    delete_salepoints = salepoints - values
    used_salepoints = delete_salepoints.select { |salepoint| Inventory.used?(salepoint) || salepoint.payment_applied }
    values = values + used_salepoints #add used salepoints back to the array since it cannot be deleted anyway
    super(values)
  end
end
