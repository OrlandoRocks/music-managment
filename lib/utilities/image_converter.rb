module Utilities
  class ImageConverter
    def initialize(io_object)
      @file = io_object
      FileUtils.mkdir_p('tmp/image_converter/original')
      FileUtils.mkdir_p('tmp/image_converter/converted')
    end

    def change_colorspace(colorspace)
      @file.rewind
      uuid = SecureRandom.uuid
      filename = "#{uuid}#{File.extname(@file.path)}"
      tmpfile = File.join(
        Rails.root,
        'tmp',
        'image_converter',
        'original',
        filename
      )
      File.open(tmpfile, 'wb') do |file|
        file.write(@file.read)
      end
      convertedfile = File.join(
        Rails.root,
        'tmp',
        'image_converter',
        'converted',
        filename
      )
      MiniMagick::Tool::Convert.new do |convert|
        convert << tmpfile
        convert.type "truecolor"
        convert.colorspace colorspace
        convert << convertedfile
      end
      File.open(convertedfile, 'rb')
    end
  end
end
