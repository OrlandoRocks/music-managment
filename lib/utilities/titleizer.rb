#!/bin/env ruby
# encoding: utf-8

#
#  A class with helper methods
#  to perform titleization
#
module Utilities
  class Titleizer

    def self.titleize(arg)
      # only capitalize first word if cyrillic chars
      return arg.capitalize if !!(arg =~ /\p{Cyrillic}/)

      # split(/\b/) will return an array with punctuation, whitespace, and alphanumeric blocks as separate elements
      string_array = (arg || "").split(/\b/)
      len = string_array.length
      titleized_nuggets = string_array.each_with_index.map {|part, i|
        # There are rules for capitalizing that depend on the word's context within the title, apply those first
        # Lowercase after an apostrophe in possessives and contractions

        if (i > 1) && string_array[i-1] =~ /’|'/ && string_array[i-2] =~ /\w/
          nugget = part.downcase
        # First and last words are always capitalized
        elsif i == 0 || i == len - 1
          nugget = TitleizerNugget.new(part).capitalized_result
        # Always capitalize after slash (/), colon (:), an opening parenthesis/bracket/brace, or a hyphen surrounded by spaces ( - )
        elsif string_array[i-1] =~ /([:\/\(\[\{])|( - )/
          nugget = TitleizerNugget.new(part).capitalized_result
        # Always capitalize before slash (/), colon (:),a closing parenthesis/bracket/brace, or a hyphen surrounded by spaces ( - )
        elsif string_array[i+1] =~ /([:\/\)\]\}]+)|( - )/
          nugget = TitleizerNugget.new(part).capitalized_result
        # Otherwise follow rules that don't depend on context within the title
        else
          nugget = TitleizerNugget.new(part).result
        end
      }.join.strip
    end

    #  Titleize with featured artists:
    #  takes a creativeable object (album, single, song, etc..) that has featured artists and generates a title that has capitalization rules applied

    def self.titleize_with_featured_artists(item)
      titleizer = Titleizer.new(item.name, item.featured_artists)
      titleizer.result(item)
    end

    def self.titleize_name(item)
      titleizer = Titleizer.new(item.name)
      titleizer.titleized_result(item)
    end

    attr_accessor :featured_artists, :name

    def initialize(initial_name="", featured_artists=[])
      self.name = (initial_name || "").strip
      self.featured_artists = featured_artists
    end

    def clean_name
      name.split(/\s(?:\[|\()f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i)[0] || ""
    end

    def titleize
      title = Titleizer.titleize(clean_name)
      title << featured_text(clean_name) if featured_artists.present?
      title
    end

    #  Concat the creative's name with the list of featured artists
    def result(current_item)
      return "" if clean_name.blank?
      if current_item.class == Single
        title = clean_name.split(' - Single')[0] || "" # We no longer add '- Single', this is for legacy purposes
      else
        title = clean_name
      end

      if !current_item.titleize?
        title + featured_text(title)
      else
        Titleizer.titleize(title) + featured_text(title)
      end
    end

    def titleized_result(current_item)
      return "" if @name.blank?
      
      if current_item.class == Single
        @name = @name.split(' - Single')[0] || "" # We no longer add '- Single', this is for legacy purposes
      end
       Titleizer.titleize(@name)
    end

    #  Generate the list of featured artists to be used in the title
    def featured_text(title)
      if title.end_with?(")")
        encl_open = "["
        encl_close = "]"
      else
        encl_open = "("
        encl_close = ")"
      end

      case featured_artists.size
      when 0
         ""
      when 1
        " " + encl_open + singular_featured_artists + encl_close
      when 2
        " " + encl_open + just_two_featured_artists + encl_close
      else
        " " + encl_open + multiple_featured_artists + encl_close
      end
    end

    def singular_featured_artists
       "feat. #{featured_artists.first.name}"
    end

    def just_two_featured_artists
       "feat. #{featured_artists.collect{|x| x.name}.join(' & ')}"
    end

    def multiple_featured_artists
      list = all_featured_artists_but_the_last.collect{ |x| x.name }
      full = list.join(', ')
      "feat. #{full} & #{last_featured_artist.name}"
    end

    #  A collection of all of the featured artists BUT the last one of the collection
    def all_featured_artists_but_the_last
      featured_artists.first(featured_artists.size - 1)
    end

    #  The last artist in the featured artist collection
    def last_featured_artist
      featured_artists.last
    end
  end
end
