module Utilities
  module Databases
    def self.index_exist?(table_name, index_name)
      rs = ActiveRecord::Base.connection.execute("select count(1) cnt
        FROM INFORMATION_SCHEMA.STATISTICS
        WHERE table_name = '#{table_name}'
        and index_name = '#{index_name}'")
      result = rs.first.first
      if result == '0'
        return false
      else
        return true
      end
    end
  end
end
