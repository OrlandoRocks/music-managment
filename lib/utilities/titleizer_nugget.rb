#
#  A class to encapsulate titlization logic
# 
module Utilities
  class TitleizerNugget

    attr_accessor :nugget

    def initialize(nugget)
      self.nugget = nugget.gsub(/\s{2,}/, " ")
    end

    def result
      if make_all_uppercase?
        n = nugget.upcase
      elsif do_not_titleize?
        n = nugget
      elsif exception_word?
        n = nugget.downcase
      else
        n = nugget.capitalize
      end
      return n
    end

    def capitalized_result
      if make_all_uppercase?
        n = nugget.upcase
      elsif do_not_titleize?
        n = nugget
      else
        n = nugget.capitalize
      end
      return n
    end

    def do_not_titleize?
      featured_case = ["feat", "with"]
      is_interCapped? || featured_case.member?(nugget.downcase)
    end

    def make_all_uppercase?
      is_roman_numeral? || on_common_caps_list?
    end

    def exception_word?
      article_list = ["a", "an", "da", "n", "the", "tha"]
      conjunction_list = ["and", "but", "nor", "or"]
      other_list= ["as", "so", "vs", "yet"]
      preposition_list = ["at", "by", "for", "fo", "from", "in", "into", "of", "off", "on", "onto", "out", "over", "to", "up", "with"]
      exception_list = article_list + conjunction_list + other_list + preposition_list
      exception_list.member?(nugget.downcase)
    end

    def is_roman_numeral?
      #romans = ["I", "V", "X", "L", "C", "M", "-"]
      #nugget.split(//).size == nugget.split(//).select{|x| romans.member?(x) == true}.size
      # taken from OZAWA Sakuro"s Roman.pm
      nugget =~ /^(?:M{0,3})(?:D?C{0,3}|C[DM])(?:L?X{0,3}|X[LC])(?:V?I{0,3}|I[VX])$/ ? true : false
    end

    def is_interCapped?
      nugget =~ (/^[A-Z]*[a-z]+[A-Z]/)
    end

    def on_common_caps_list?
      caps_list = ["TV", "MC", "DJ", "EP", "CIA", "FBI", "IRS", "OK", "WWE", "BPM", "AJ", "JJ", "BJ", "MVP", "VP", "IOU", "OMG", "NSFW", "LOL"]
      caps_list.member?(nugget.upcase)
    end

  end
end
