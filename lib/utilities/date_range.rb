module Utilities
  # = DESCRIPTION
  # The DateRange class is used to generate date ranges that can be used in creating reports, annual renewals
  # or any other place that you want to generate a time frame.  The date range types DateRange currently
  # handles are :day, :week, :month, :quarter, :year.  When created with a type and date the class will give you 
  # ranges as follows:
  #
  # * day -- starts: midnight of the day entered | stops: midnight the following day
  # * week -- starts: midnight of the previous Sunday | stops: midnight the following Sunday
  # * month -- starts: midnight on the first of the month | stops: midnight on the following month
  # * quarter -- starts: midnight on the first day of the quarter | stops: 3 months after the first day of the quarter
  # * year -- starts: midnight on the first day of the year | stops: on the first day of the following year
  #
  #
  # = USAGE
  # 
  #     date_range = Utilities::DateRange.new(:month, '2009-12-25')
  #     date_range.start =>   Date: '2009-12-01'
  #     date_range.stop  =>   Date: '2010-01-01'
  # 
  #
  # = CHANGE LOG
  # [2009-12-04 -- MJL]
  # Refactoring the date_range methods from the DataReport class into the DateRange utility
  #
  class DateRange
    attr_reader :type, :date

    def initialize(type, entered_date = Date.today)
      # Raise an error if the DateRange type is not a symbol
      raise("DateRange type must be a symbol") unless type.instance_of?(Symbol)

      # Rase an error if the entered_date is not the type of a String, Date or Time object
      raise("You must supply a date as a String, Date or Time object") unless entered_date.instance_of?(String) || entered_date.instance_of?(Date) || entered_date.instance_of?(Time)

      # Set the date based on type      
      if entered_date.instance_of?(String)
        begin
          @date = entered_date.to_date
        rescue ArgumentError
          raise ArgumentError, "#{entered_date} is an invalid date.  Please enter a valid date in format YYY-MM-DD."
        end
      elsif entered_date.instance_of?(Date)
        @date = entered_date
      elsif entered_date.instance_of?(Time)
        @date = entered_date
      else
        raise "Could not parse a date from your input type of #{entered_date.class}.  Date is #{@date}"
      end

      @type = type
    end

    # Returns a date object for the start date
    def start
      @start_date ||= case type
      when :day
        date
      when :week
        date - (date.cwday - 1) #commercial weeks work monday (1) - sunday (7), so we minus an extra day to get us to Sunday to Sunday
      when :month
        Date.new(date.year, date.month, 1)
      when :quarter
        start_date_for_quarter
      when :year
        Date.new(date.year, 1, 1)
      end
      return @start_date
    end

    # Returns a date object for the stop date
    def stop
      @stop_date ||= case type
      when :day
        start + 1
      when :week
        start + 7
      when :month
        start >> 1
      when :quarter
        stop_date_for_quarter
      when :year
        Date.new(start.year + 1, 1, 1)
      end
      return @stop_date
    end

    # Returns a symbol representation of the date range
    def identifier
      identifier_string = case type
      when :live
        "live"
      when :day
        "D#{start.yday.to_s}_#{start.year.to_s}"
      when :week
        "W#{start.cweek.to_s}_#{start.cwyear}"
      when :month
        "M#{start.month}_#{start.year}"
      when :quarter
        identifier_for_quarter
      when :year
        "Y#{date.year}"
      end
      return identifier_string.intern
    end

    private

    def start_date_for_quarter
      case quarter
      when :Q1
         Date.new(date.year, 1, 1)
      when :Q2
        Date.new(date.year, 4, 1)
      when :Q3
        Date.new(date.year, 7, 1)
      when :Q4
        Date.new(date.year, 10, 1)
      end
    end

    def stop_date_for_quarter
      case quarter
      when :Q1
        Date.new(date.year, 4, 1)
      when :Q2
        Date.new(date.year, 7, 1)
      when :Q3
        Date.new(date.year, 10, 1)
      when :Q4
        Date.new(date.year + 1, 1, 1)
      end
    end

    def identifier_for_quarter
      case quarter
      when :Q1
        "Q1_#{date.year.to_s}"
      when :Q2
        "Q2_#{date.year.to_s}"
      when :Q3
        "Q3_#{date.year.to_s}"
      when :Q4
        "Q4_#{date.year.to_s}"
      end
    end

    def quarter
      @quarter ||= case
      when is_first_quarter?
        :Q1
      when is_second_quarter?
        :Q2
      when is_third_quarter?
        :Q3
      when is_fourth_quarter?
        :Q4
      else
        raise "There is an issue calculating the quarters"
      end
    end

    #
    #  Conditionals for deciding quarter
    #
    def is_first_quarter?
      (date >= Date.new(date.year, 1, 1)) && (date <= Date.new(date.year, 3, 31))
    end

    def is_second_quarter?
      (date >= Date.new(date.year, 4, 1)) && (date <= Date.new(date.year, 6, 30))
    end

    def is_third_quarter?
      (date >= Date.new(date.year, 7, 1)) && (date <= Date.new(date.year, 9, 30))
    end

    def is_fourth_quarter?
      (date >= Date.new(date.year, 10, 1)) && (date <= Date.new(date.year, 12, 31))
    end

  end

end
