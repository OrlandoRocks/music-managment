module Utilities
  module StringToHashConverter

    def self.convert_to_hash(str)
      Hash[str.tr('{  }', '').split(",").collect{|x| x.strip.split(":")}]
    end
  end
end
