  # = DESCRIPTION
  # The FormDateRange module is used to extrapolate a range of dates given a start date and end date.
  # It returns a nested array used to populate dropdowns in a view.
  #
  # = USAGE
  #   @my_start_date = Date.parse("05/2010")
  #   @my_end_date = Date.parse("10/2010")
  #   create_range(@my_start_date, @my_end_date)
  #
  #  This returns the nested_array: 
  #  [["May 2010", "2010-05-01"], ["Jun 2010", "2010-06-01"], ["Jul 2010", "2010-07-01"], 
  #   ["Aug 2010", "2010-08-01"], ["Sep 2010", "2010-09-01"], ["Oct 2010", "2010-10-01"]]
  # 
  #
  #
module Utilities
  module FormDateRange
    def self.create_range(start_date, end_date)
      dates_array = []
      (start_date.year..end_date.year).each do |year|
         month_start = (start_date.year == year) ? start_date.month : 1
         month_end = (end_date.year == year) ? end_date.month : 12
         (month_start..month_end).each_with_index do |month, index|  
           date = "#{Date::ABBR_MONTHNAMES[month]} #{year}"
           dates_array << [date, Date.parse(date).strftime("%Y-%m-%d")]
         end
      end
      dates_array
    end
  end
end
