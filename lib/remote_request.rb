# 2009-07-28 -- MJL -- Introduction of RemoteRequest library class

# RemoteRequest is a simple wrapper around net/http that allows the user to specify 
# the url that they wish to be called, and optionally the cookie session from the rails application
# (to allow cross-subdomain authentication--if applicable), and then return a 
# response body from the remote call. 
# 
# USAGE: 
# EXAMPLE 1:
#           url = artwork_suggestion_url(@album, params[:id], :host => 'localhost', :port => 9191)
#           cover_id = RemoteRequest.get(url, cookies)
# 
# (this example form the artwork controller passes in the cookies hash so that the tunecore_id 
#  can be passed along to the remote server.  This allows us to share sessions between www and artwork servers)

class RemoteRequest
  require 'net/http'

  def self.get(your_url, cookies=nil)
    url = URI.parse(your_url)
    req = Net::HTTP::Get.new(url.path)
    req.add_field('cookie', "tunecore_id=#{cookies[:tunecore_id]}") if cookies

    res = Net::HTTP.start(url.host, url.port) {|http|
      http.read_timeout = 960
      #puts "Request Read Timeout set to = #{http.read_timeout}"
      http.request(req)
    }
    

    return(res.body)
  end
end