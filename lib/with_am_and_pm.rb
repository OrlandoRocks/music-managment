module WithAmAndPm
  def instantiate_time_object(name, values)
    if values.last < 0
      ampm = values.pop
      if ampm == ActionView::Helpers::DateTimeSelector::AM and values[3] == 12
        values[3] = 0
      elsif ampm == ActionView::Helpers::DateTimeSelector::PM and values[3] != 12
        values[3] += 12
      end
    end

    super(name, values)
  end

  def select_hour
    return super unless @options[:twelve_hour]

    if @options[:use_hidden] || @options[:discard_hour]
      build_hidden(:hour, hour12)
    else
      build_options_and_select(:hour, hour12, :start => 1, :end => 12)
    end
  end

  private

  def build_selects_from_types(order)
    order += [:ampm] if @options[:twelve_hour] and !order.include?(:ampm)
    super(order)
  end

  def hour12
    h12 = hour % 12
    h12 = 12 if h12 == 0
    return h12
  end
end
