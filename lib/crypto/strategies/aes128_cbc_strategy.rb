module Crypto::Strategies
  class Aes128CbcStrategy
    KEY_SIZE = 16
    ALGORITHM = "AES-128-CBC".freeze

    EncryptionArtifact = Struct.new(:secret, :salt, :encrypted_string, keyword_init: true)

    def initialize(secret:, base64_encode:, salt: nil)
      @secret = secret
      @salt = salt
      @base64_encode = base64_encode
    end

    def encrypt(data)
      @salt ||= random_salt

      EncryptionArtifact.new(
        secret: secret,
        salt: salt,
        encrypted_string: encrypted_string(data)
      )
    end

    def decrypt(data)
      use_cipher(:decrypt, base64_encode ? Base64.decode64(data) : data)
    end

    private

    attr_reader :secret, :salt, :base64_encode

    def random_salt
      Base64.encode64(sha1_digest(OpenSSL::Random.random_bytes(KEY_SIZE))).chop
    end

    def encrypted_string(data)
      binary_escaped_string = use_cipher(:encrypt, data)

      base64_encode ? Base64.encode64(binary_escaped_string) : binary_escaped_string
    end

    def sha1_digest(data)
      Digest::SHA1.digest(data)[0..(KEY_SIZE - 1)]
    end

    def use_cipher(mode, data)
      cipher = OpenSSL::Cipher.new(ALGORITHM)
      cipher.send(mode)
      cipher.key = sha1_digest(salt + secret)
      cipher.padding = 1

      cipher.update(data) + cipher.final
    end
  end
end
