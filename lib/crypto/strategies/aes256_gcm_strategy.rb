module Crypto::Strategies
  class Aes256GcmStrategy
    ALGORITHM = "aes-256-gcm".freeze

    EncryptionArtifact = Struct.new(:key, :iv, :auth_tag, :encrypted_string, keyword_init: true)

    def initialize(key: nil, iv: nil, auth_data: "", auth_tag: nil)
      @key = key
      @iv = iv
      @auth_data = auth_data
      @auth_tag = auth_tag
    end

    def encrypt(data)
      cipher = OpenSSL::Cipher.new(ALGORITHM).encrypt
      use_key_or_random(cipher)
      use_iv_or_random(cipher)
      cipher.auth_data = auth_data

      encrypted_value = cipher.update(data) + cipher.final

      @auth_tag = cipher.auth_tag

      EncryptionArtifact.new(
        key: key,
        iv: iv,
        auth_tag: auth_tag,
        encrypted_string: hex_encode(encrypted_value)
      )
    end

    def decrypt(data)
      cipher = OpenSSL::Cipher.new(ALGORITHM).decrypt
      cipher.key = key
      cipher.iv = iv
      cipher.auth_tag = auth_tag
      cipher.auth_data = auth_data
      cipher.update(hex_decode(data)) + cipher.final
    end

    private

    def use_key_or_random(cipher)
      @key ||= cipher.random_key
      cipher.key = key
    end

    def use_iv_or_random(cipher)
      @iv ||= cipher.random_iv
      cipher.iv = iv
    end

    def hex_encode(data)
      data.unpack1('H*').upcase
    end

    def hex_decode(data)
      [data].pack('H*').unpack('C*').pack('c*')
    end

    attr_accessor :key, :iv, :auth_data, :auth_tag
  end
end
