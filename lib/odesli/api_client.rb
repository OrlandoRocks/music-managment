class Odesli::ApiClient
  def initialize(song_url, api_key)
    @http_client = Faraday.new
    @odesli_url = "#{ENV['ODESLI_BASE_URL']}?url=#{song_url}&key=#{api_key}"
  end

  def get
    response = @http_client.get(@odesli_url)
    response_body = JSON.parse(response.body)
    setup_response(response_body)
  end

  def setup_response(response_body)
    response_body['entitiesByUniqueId'].nil? ? response_body = nil : response_body
  end
end
