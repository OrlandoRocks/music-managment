require "logging.rb"

module Batch
  extend Logging

  def self.build_and_run(time=Time.now, use_sidekiq=false)

    # Create batches for all country websites
    batches = create_batches(time)

    sidekiq_batch = Sidekiq::Batch.new
    sidekiq_batch.description = "Batch for #{time}"

    #Create purchases for renewals due
    Renewal.create_renewal_purchases(time.to_date) do |person, purchases|

      log("Payment Batch", "Processing person", :person_id=>person.id, :country_website=>person.country_website_id)

      #Get the right batch
      batch = batches[person.country_website_id]

      #Smartly create a group of invoices
      invoices     = Invoice.add_renewal_purchases_to_invoices(person, purchases)

      #Create batch transactions for all invoices
      transactions = batch.create_transactions_for_invoices(invoices)

      #Process the transactions
      if use_sidekiq
        sidekiq_batch.jobs { transactions.each { |transaction| BatchTransactionProcessWorker.perform_async(batch.id, [transaction.id]) } }
      else
        batch.process_transactions(transactions)
      end

      log("Payment Batch", "Completed processing person", :person_id=>person.id)
    end

    # We're resetting valid Invoices from 21 < x < 42 days ago from visible_to_customer to waiting_for_batch, so they can go through the regular retry flow.
    Invoice.unsettled_visible_to_customer.batch_transaction_eligible(time.to_date).each do |invoice|
      invoice.update(batch_status: "waiting_for_batch") if invoice.batch_attempts.to_i < BRAINTREE_BATCH[:run_limit].to_i 
    end

    #Upload and complete the batches
    batches.each do |_, batch|
      #Process failed invoices from previous days
      previously_failed_invoices = Invoice.waiting_for_batch(batch.country_website)
      if previously_failed_invoices.count > 0
        log("Payment Batch", "Processing failed invoices from previous batches", :previous_failed_invoice_count=>previously_failed_invoices.count)

        transactions   = batch.create_transactions_for_invoices(previously_failed_invoices)

        if use_sidekiq
          sidekiq_batch.jobs { transactions.each { |transaction| BatchTransactionProcessWorker.perform_async(batch.id, [transaction.id]) } }
        else
          batch.process_transactions(transactions)
        end

        log("Payment Batch", "Processing failed invoices from previous batches complete")
      end
    end


    loop do
      batches.each do |_, batch|
        log('Payment Batch', "##{batch.id} #{batch.country_website.name}: finished #{batch.finished_batch_transactions.count} of #{batch.batch_transactions.count}")
      end
      break if batches.all?{|_, batch| batch.pending_batch_transactions.empty?}
      sleep(2)
    end


    log("Payment Batch", "Running completions")
    batches.each do |_, batch|
      batch.complete_batch
    end
  end

  # Creates and returns batch to be processed
  def self.create_batches(time)
    batches = {}

    CountryWebsite.all.each do |country_website|
      log("Payment Batch")
      log("Payment Batch", "Creating a new batch", :country_website_id=>country_website.id)
      batch = PaymentBatch.create(:country_website_id=>country_website.id, :currency=>country_website.currency)
      batch.update(:status=>'processing',:batch_date => time )

      batches[country_website.id] = batch
    end

    return batches
  end

  def pretty_time_since(start_time)
    time_spent = Time.now - start_time
    [time_spent/3600, time_spent % 3600 / 60, time_spent % 60].map{|t| "%02d" % t.floor }.join(':')
  end

end
