module Colorized

  attr_reader :str

  CODE_LOOKUP = {
    red: 31,
    green: 32,
    yellow: 33,
    blue: 34,
    pink: 35,
    light_blue: 36
  }

  def self.string_io(color_code, str)
    "\e[#{CODE_LOOKUP[color_code]}m#{str}\e[0m"
  end
end