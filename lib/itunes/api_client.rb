# frozen_string_literal: true

class Itunes::ApiClient
  class ItunesApiUrlEnvNotSetError < StandardError; end

  attr_accessor :client, :upc, :song_name

  def initialize(upc, song_name)
    @upc = upc
    @song_name = song_name
    @client = Faraday.new(url: request_url)
  end

  def fetch_song_id
    response = client.get
    return if response.nil?

    json_response = JSON.parse(response.body)
    results = json_response['results']
    song_id = nil
    results.each do |result|
      next if result['wrapperType'] == 'collection'

      song_id = result['trackId'] if result['trackName'].include? song_name
    end
    song_id
  end

  private

  def request_url
    "#{itunes_api_url}lookup?upc=#{upc}&entity=song"
  end

  def itunes_api_url
    ENV.fetch('ITUNES_API_URL') do
      raise ItunesApiUrlEnvNotSetError, "ITUNES_API_URL Env is not Set!"
    end
  end
end
