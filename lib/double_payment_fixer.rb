class DoublePaymentFixer

  def self.log_bad_withdrawal_requests
    results = {}
    people_without_requests = []
    people_with_negative_balances.each do |person|
      person_intake_created_at  = person.person_intakes.where("created_at > '2018-05-18'").last.created_at
      paypal =  paypal_transfer_requests(person.id, person_intake_created_at)
      eft    =  eft_batch_transaction_requests(person.id, person_intake_created_at)
      check  =  check_transfer_requests(person.id, person_intake_created_at)
      if !paypal.any? && !eft.any? && !check.any?
        people_without_requests << person.id
      else
        results[person.id] = []
        results[person.id] << paypal_transfer_requests(person.id, person_intake_created_at) if paypal.any?
        results[person.id] << eft_batch_transaction_requests(person.id, person_intake_created_at) if eft.any?
        results[person.id] << check_transfer_requests(person.id, person_intake_created_at) if check.any?
      end
    end

    my_logger = Logger.new("#{Rails.root}/log/bad_withdrawal_requests.log")
    results.each do |person_id, requests|
      my_logger.info("PERSON ID: #{person_id}")
      requests.flatten.each do |req|
        my_logger.info("#{req.inspect}")
      end
      my_logger.info("\n")
    end

    my_logger.info("PEOPLE WITHOUT REQUESTS")
    people_without_requests.each do |person_id|
      my_logger.info(person_id)
    end
  end

  def self.cancel_payment_request(admin_email, people_to_fix=people_with_negative_balances)
    admin = Person.find_by(email: admin_email)
    my_logger = Logger.new("#{Rails.root}/log/bad_withdrawal_requests.log")
    people_to_fix.each do |person|
      begin
        PaypalTransfer.transaction do
          person_intake_created_at  = person.person_intakes.where("created_at > '2018-05-18'").last.created_at
          paypal_transfer_requests(person.id, person_intake_created_at).each do |paypal_transfer|

            paypal_transfer.cancel
          end
          check_transfer_requests(person.id, person_intake_created_at).each do |check_transfer|
            check_transfer.cancel(false)
          end

          eft_batch_transaction_requests(person.id, person_intake_created_at).each do |eft_batch_transaction|
            eft_batch_transaction.reject(admin,nil,nil,false)
          end
        end
      rescue => e
        my_logger.info("ERROR: #{e.message}")
        my_logger.info("ERROR: #{e.backtrace}")
      end
    end
  end

  attr_reader :my_logger

  def initialize(log_file="double_sip_posting_fix")
    @my_logger = Logger.new(Rails.root.join("#{Rails.root}/log/", "#{log_file}.log"))
  end

  def fix_double_sip_posting_credits(people_to_fix=people_we_double_paid)
    begin
      people_to_fix.map do |person|
        person_balance = person.person_balance
        person_intake = PersonIntake.where(self.class.person_intakes[:created_at].gteq("2018-05-18").and(self.class.person_intakes[:person_id].eq(person.id)).and(self.class.person_intakes[:amount].gt(0))).last
        if person_intake
          my_logger.info("[#{Time.now}] [person_id: #{person.id}] STARTING PERSON INTAKE ID: #{person_intake.id}")
          fix_double_sip_posting_credit(person_intake, person_balance)
        else
          my_logger.info("[#{Time.now}] [person_id: #{person.id}] COULD NOT FIND PERSON INTAKE WITH AMOUNT > 0")
        end
      end
    rescue  => e
      my_logger.info("ERROR: #{e.message}")
      my_logger.info("ERROR: #{e.backtrace}")
    end
  end

  def fix_double_encumbrance_witholding(people_to_fix=people_we_double_paid)
    begin
      people_to_fix.each do |person|
        person_balance = person.person_balance
        EncumbranceSummary.where(person_id: person.id).each do |encumbrance_summary|
        # create person transaction for any details created for this person
          details = enc_details(encumbrance_summary.id)
          if details.count == 2
            if PersonTransaction.exists?(person_id: person.id, comment: "Credit for encumbrance withholding from 2018-05-18")
              my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] SKIPPING)")
            else
              detail = details.last
              amount_withheld = detail.transaction_amount.abs
              EncumbranceDetail.transaction do
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Creating encumbrance detail")
                credit_detail = EncumbranceDetail.create!(encumbrance_summary_id: encumbrance_summary.id, transaction_date: Time.now, transaction_amount: amount_withheld)
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Creating person_transaction")
                PersonTransaction.create!(
                  person_id: encumbrance_summary.person_id,
                  credit: amount_withheld,
                  target_type: "EncumbranceDetail",
                  target_id: credit_detail.id,
                  currency: person_balance.currency,
                  comment: "Credit for encumbrance withholding from 2018-05-18",
                  previous_balance: person_balance.balance)
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Current encumbrance_summary oustanding amount: #{encumbrance_summary.outstanding_amount}")
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Updating encumbrance summary ID #{encumbrance_summary.id}")
                # add withheld amount BACK to summary outstanding amount
                encumbrance_summary.outstanding_amount = encumbrance_summary.outstanding_amount + amount_withheld
                encumbrance_summary.save!
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Updated encumbrance_summary oustanding amount: #{encumbrance_summary.outstanding_amount}")
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] updating person balance")
                # update person balance
                person_balance.balance = person_balance.balance + amount_withheld
                person_balance.save!
                my_logger.info("[#{Time.now}] [person_id: #{person.id}] [balance: #{person_balance.balance}] [transaction_amount: #{amount_withheld}] Updated balance")
              end
            end
          end
        end
      end
    rescue  => e
      my_logger.info("ERROR: #{e.message}")
      my_logger.info("ERROR: #{e.backtrace}")
    end
  end

  private

  def fix_double_sip_posting_credit(person_intake, person_balance)
    if completed_fix?(person_intake)
      my_logger.info("[#{Time.now}] [person_id: #{person_intake.person_id}] SKIPPING PERSON INTAKE ID: #{person_intake.id}")
    else
      PersonTransaction.transaction do
        create_person_transaction(person_intake, person_balance)
        update_person_balance(person_intake, person_balance)
      end
      my_logger.info("[#{Time.now}] [person_id: #{person_intake.person_id}] completed PERSON INTAKE ID: #{person_intake.id}")
    end
  end


  def completed_fix?(person_intake)
    PersonTransaction.exists?(person_id: person_intake.person_id, comment: "Reversal of 2018-05-18 Duplicate Sales Posting")
  end

  def create_person_transaction(person_intake, person_balance)
    my_logger.info("[#{Time.now}] [person_id: #{person_intake.person_id}] [balance: #{person_balance.balance}] [sip_posting_amount: #{person_intake.amount}] Creating person transaction for PERSON INTAKE ID: #{person_intake.id}")
    pt = PersonTransaction.create!(
          person_id: person_intake.person_id,
          debit: person_intake.amount,
          target_type: "PersonIntake",
          target_id: person_intake.id,
          comment: "Reversal of 2018-05-18 Duplicate Sales Posting",
          previous_balance: person_balance.balance)
    pt.currency = person_intake.currency
    pt.save(validate: false)
  end

  def update_person_balance(person_intake, person_balance)
    my_logger.info("[#{Time.now}] [person_id: #{person_intake.person_id}] [balance: #{person_balance.balance}] [sip_posting_amount: #{person_intake.amount}] Updating person balance for PERSON INTAKE ID: #{person_intake.id}")
    person_balance.balance = person_balance.balance - person_intake.amount
    person_balance.save!
    my_logger.info("[#{Time.now}] [person_id: #{person_intake.person_id}] [balance: #{person_balance.balance}] [sip_posting_amount: #{person_intake.amount}] Updated balance")
  end

  def enc_details(encumbrance_summary_id)
    EncumbranceDetail.where(encumbrance_details[:encumbrance_summary_id].eq(encumbrance_summary_id).and(encumbrance_details[:transaction_date].gteq("2018-05-18")).and(encumbrance_details[:transaction_amount].lt(0)))
  end

  # def fix_double_sip_posting_debit
  # end

  def self.sum_withdrawal_requests_by_person
    people_who_made_withdrawal_requests.each_with_object({}) do |person, obj|
      person_intake_created_at = PersonIntake.where(person_intakes[:person_id].eq(person.id).and(person_intakes[:created_at].gteq("2018-05-18"))).last.created_at
      obj[person.id] = paypal_transfer_amount(person.id, person_intake_created_at) + eft_batch_transactions_amount(person.id, person_intake_created_at) + check_transfer_amount(person.id, person_intake_created_at)
    end
  end

  def self.people_with_bad_requests
    sum_withdrawal_requests_by_person.select do |person_id, amount_requested|
      person_balance = PersonBalance.where(person_id: person_id).last
      (amount_requested > person_balance.balance) && person_balance.balance < 0
    end
  end

  def self.correct_posting_amount(person_id)
    @correct_posing_amount ||= SalesRecord.select(sales_records[:amount].sum.as("amount")).joins(:sales_record_master).where(sales_record_masters[:created_at].gteq("2018-05-18").and(sales_records[:person_id].eq(person_id))).first.amount
  end

  def self.people_with_negative_balances
    Person.select(people[:id]).eager_load(:person_intakes).joins(:person_intakes).where(person_intakes[:created_at].gteq('2018-05-18')).group("people.id HAVING count(people.id) = 2").joins(:person_balance).where(person_balances[:balance].lt(0))
  end

  def people_we_double_paid
    @people_we_double_paid ||= Person.select(self.class.people[:id]).eager_load(:person_balance).joins(:person_intakes).where(self.class.person_intakes[:created_at].gteq('2018-05-18')).group("people.id HAVING count(people.id) = 2")
  end


  def self.people_who_made_withdrawal_requests
    Person.find_by_sql("SELECT pi.person_id as id FROM person_intakes pi
      INNER JOIN paypal_transfers pt ON pi.person_id = pt.person_id
      WHERE pt.transfer_status = 'pending'
      AND pi.created_at > '2018-05-18' GROUP BY 1 HAVING count(DISTINCT pi.id) = 2
          UNION
      SELECT pi.person_id as id FROM person_intakes pi
      INNER JOIN check_transfers ct ON pi.person_id = ct.person_id
      WHERE ct.transfer_status = 'pending'
      AND pi.created_at > '2018-05-18' GROUP BY 1 HAVING count(DISTINCT pi.id) = 2
          UNION
      SELECT pi.person_id as id FROM person_intakes pi
      INNER JOIN stored_bank_accounts ba ON pi.person_id = ba.person_id
      INNER JOIN eft_batch_transactions eft ON ba.id = eft.stored_bank_account_id
      WHERE eft.status = 'pending_approval'
      AND pi.created_at > '2018-05-18' GROUP BY 1 HAVING count(DISTINCT pi.id) = 2;")
  end

  def self.paypal_transfer_amount(person_id, person_intake_created_at)
    amount = PaypalTransfer
              .select(paypal_transfers[:payment_cents].sum.as('total_payment_cents'))
              .where(paypal_transfers[:person_id].eq(person_id)
                .and(paypal_transfers[:transfer_status].eq('pending'))
                .and(paypal_transfers[:created_at].gt(person_intake_created_at)))
                .first.total_payment_cents || 0
     amount * 0.01
  end

  def self.eft_batch_transactions_amount(person_id, person_intake_created_at)
    amount = EftBatchTransaction
              .select("SUM(amount + 2.75) as total_amount")
              .joins(stored_bank_account: :person)
              .where(people[:id].eq(person_id)
                .and(efts[:status].eq('pending_approval'))
                .and(efts[:created_at].gt(person_intake_created_at)))
              .first.total_amount || 0
  end

  def self.check_transfer_amount(person_id, person_intake_created_at)
    amount = CheckTransfer
              .select("SUM(payment_cents + admin_charge_cents) as total_charge_cents")
              .where(checks[:transfer_status].eq('pending')
                .and(checks[:person_id].eq(person_id))
                .and(checks[:created_at].gt(person_intake_created_at)))
              .first.total_charge_cents || 0
    amount * 0.01
  end

  def self.paypal_transfer_requests(person_id, person_intake_created_at)
    PaypalTransfer.where(person_id: person_id, transfer_status: :pending).where(paypal_transfers[:created_at].gt(person_intake_created_at)).readonly(false)
  end

  def self.eft_batch_transaction_requests(person_id, person_intake_created_at)
    EftBatchTransaction.joins(stored_bank_account: :person).where(people[:id].eq(person_id).and(efts[:status].eq('pending_approval')).and(efts[:created_at].gt(person_intake_created_at))).readonly(false)
  end

  def self.check_transfer_requests(person_id, person_intake_created_at)
     CheckTransfer.where(checks[:transfer_status].eq('pending').and(checks[:person_id].eq(person_id)).and(checks[:created_at].gt(person_intake_created_at))).readonly(false)
  end

  private

  def self.people
    Person.arel_table
  end

  def self.person_intakes
    PersonIntake.arel_table
  end

  def self.paypal_transfers
    PaypalTransfer.arel_table
  end

  def self.efts
    EftBatchTransaction.arel_table
  end

  def self.stored_bank_accounts
    StoredBankAccount.arel_table
  end

  def self.checks
    CheckTransfer.arel_table
  end

  def self.sales_records
    SalesRecord.arel_table
  end

  def self.sales_record_masters
    SalesRecordMaster.arel_table
  end

  def encumbrance_details
    EncumbranceDetail.arel_table
  end

  def self.person_balances
    PersonBalance.arel_table
  end
end
