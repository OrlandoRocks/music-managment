module DateFunctions
  def parse_str_for_date(date)
    date.class== Date ? date : Date.parse(date)
  end  
end