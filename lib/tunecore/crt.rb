module Tunecore
  class Crt
    include ERB::Util
    extend Tunecore::Cacheable

    class << self
      def fetch_album_from_cache(tc_album)
        if FeatureFlipper.show_feature?(:crt_cache, nil)
          fetch("crt:#{tc_album.id}") { create_album_hash(tc_album) }
        else
          create_album_hash(tc_album)
        end
      end

      def invalidate_album_in_cache(tc_album)
        if FeatureFlipper.show_feature?(:crt_cache, nil)
          invalidate("crt:#{tc_album.id}")
        end
      end

      def create_album_hash(tc_album)
        {
          label_name: tc_album.label_name,
          country_website: tc_album.person.country_domain,
          account_country: tc_album.person.country,
          distribution_type: tc_album.album_type,
          created: tc_album.created_on,
          tracks: tc_album.songs.size,
          tunecore_upc: tc_album.tunecore_upc.number,
          title: tc_album.name,
          artwork: tc_album.artwork.url(:medium),
          songs: collect_crt_songs(tc_album),
          artists: collect_artist_names(tc_album),
          is_various: tc_album.is_various?,
          album_id: tc_album.id,
          person_id: tc_album.person_id,
          auto_format_on: tc_album.allow_different_format,
          blacklists: [],
          blacklists_disabled: true,
          stores: tc_album.salepoints.map{|s| s.store.short_name},
          vip_status: tc_album.person.vip? ? 'True' : 'False',
          total_releases: tc_album.person.albums.size,
          account_status: tc_album.person.status,
          account_created: tc_album.person.created_on,
          account_owner: tc_album.person.name,
          audits: collect_crt_audits(tc_album),
          legal_review_state: tc_album.legal_review_state.capitalize,
          finalized: I18n.localize(tc_album.finalized_at, format: :slash_with_time),
          account_email: tc_album.person.email,
          genres: tc_album.genres.map{|g| g.name},
          release_language: tc_album.language.description,
          countries: tc_album.countries.map{|c| [c.name,c.iso_code]},
          total_earnings: 0,
          dup_titles: get_song_ids_of_duplicate_titles(tc_album),
          flagged_content: tc_album.flagged_content_data
        }
      end


      def collect_crt_songs(tc_album)
        tc_album.songs.map do |s|
          {
            track_number: s.track_num,
            title: s.name,
            translated_title: s.translated_name,
            parental_advisory: s.parental_advisory,
            artists: SongArtist.for_crt(s),
            uuid: s.s3_asset.nil? ? nil : s.s3_asset.uuid,
            id: s.id,
            song_id: s.id,
            lyrics: s.lyric.try(:content),
            language: s.metadata_language_code.try(:description),
            streaming_url: s.s3_url(1.hour)
          }
        end
      end

      def collect_crt_audits(tc_album)
        tc_album.review_audits.map do |ra|
        list_of_reasons = ra.review_reasons.map {|rr| rr.reason}
          {
            auditor: ra.person.name,
            event: ra.event,
            note: ra.note,
            created: I18n.localize(ra.created_at, format: :slash_with_time),
            reasons: list_of_reasons
          }
        end
      end

      def collect_artist_names(tc_album)
        if tc_album.is_various?
          [{name: "Various Artists", role: "Various"}]
        else
          creatives = tc_album.creatives.includes(:artist)
          creatives.map do |c|
            {
              name: c.name,
              role: c.role.titleize,
              blacklisted: Artist.active_blacklist.exists?(["lower(name) = ?", c.name.downcase])
            }
          end
        end
      end

      def get_song_ids_of_duplicate_titles(tc_album)
        titles = tc_album.songs.map(&:name)
        tc_album.songs.select { |song| titles.count(song.name) > 1 }.map(&:id)
      end
    end
  end
end
