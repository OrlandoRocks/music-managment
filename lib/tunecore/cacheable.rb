module Tunecore
  module Cacheable
    def fetch(key, &block)
      Rails.cache.fetch(key) do
        block.call
      end
    end

    def write(key, value)
      Rails.cache.write(key, value)
    end

    def invalidate(key)
      Rails.cache.delete(key)
    end
  end
end
