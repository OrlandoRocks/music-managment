module Tunecore
  class AmbassadorApi
    class RequestError < Exception; end
      
    class << self
      # attr_accessor :user_name, :api_key, :sandbox
      def user_name
        GA_USER_NAME
      end
      
      def api_key
        GA_API_KEY
      end
      
      def sandbox
        GA_SANDBOX
      end
    end
    
    def self.site_uri
      "https://getambassador.com/api/v2/#{user_name}/#{api_key}/json/"
    end
    
    def self.default_params
      {:sandbox => (sandbox ? 1:0)}
    end
    
    def self.get_ambassador(params={})
      api_url = "#{site_uri}ambassador/get"
      call(api_url, params)
    end
    
    def self.record_event(params={})
      api_url = "#{site_uri}event/record"
      call(api_url, params)
    end
    
    def self.social_addthis(params={})
      api_url = "#{site_uri}social/addthis"
      call(api_url, params)
    end
    
    def self.ambassador_stats(params={})
      api_url = "#{site_uri}ambassador/stats"
      call(api_url, params)
    end
    
    def self.add_balance(params={})
      api_url = "#{site_uri}balance/add"
      call(api_url, params)
    end
    
    def self.all_commissions(params={})
      api_url = "#{site_uri}commission/all"
      call(api_url, params)
    end
    
    def self.add_commission(params={})
      api_url = "#{site_uri}commission/add"
      call(api_url, params)
    end
    
  private
    def self.call(api_url, params)
       clnt = HTTPClient.new
       #FIXME: Remove test response once testing is done!!
       # response = {"code"=>"401", "errors"=>{"error"=>["Invalid API credentials."]}, "message"=>"UNAUTHORIZED: See error message for more details."}
       response = JSON.parse(clnt.get(api_url, params.merge(default_params)).content)['response']
       raise RequestError, "#{response.inspect}" if response['code'] != '200'
       return response
    end
    
  end
end
