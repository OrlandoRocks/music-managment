#
#  A module to extend Creative types
#  with a genre
#
module Tunecore

  module Genreable

    #
    #  Add associations to the included class
    #
    #  - primary_genre
    #  - secondary_genre
    #
    def self.included(base)
      base.belongs_to :primary_genre, :class_name => 'Genre'
      base.belongs_to :secondary_genre, :class_name => 'Genre'
    end

    def genres
      [primary_genre, secondary_genre].compact
    end

  end

end
