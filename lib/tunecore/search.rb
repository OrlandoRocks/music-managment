class Tunecore::Search
  extend ActiveModel::Naming

  attr_accessor :search_params, :relation

  def initialize(params, active_record_relation)
    self.search_params = params
    self.relation      = active_record_relation
  end

  def all(options)
   relation.all(options)
  end

  def paginate(options)
    result = relation
    result = result.select(options[:select]) if options[:select].present?
    result = result.joins(options[:joins]) if options[:joins].present?
    result = result.includes(options[:includes]) if options[:includes].present?
    result = result.where(options[:conditions]) if options[:conditions].present?
    result = result.order(options[:order]) if options[:order].present?

    result.paginate(page: options[:page], per_page: options[:per_page])
  end

  def self.model_name
   ActiveModel::Name.new(Tunecore::Search, Tunecore, "search")
  end

  def method_missing(name)
    return search_params[name.to_sym]
  end

end
