module Tunecore
  class AutoRenewalDateCalculator
    # ==========================================================================
    # = start_date_for_renewal contains the logic necessary to set the start
    # = and end dates of any renewal. Renewals are run every Thursday Night
    # = so all renewals start at Midnight Sunday and end at Midnight Thursday Night.
    # = In order to be egalitarian about the subscriptions, we push
    # = any late in the week subscriptions (Friday, Saturday, Sunday) to the next Thursday Night
    # ==========================================================================
    def self.expiration_date(starts_at, renewal_interval, renewal_duration)
      ret = (starts_at + renewal_duration.to_i.send(renewal_interval.to_sym)) if !renewal_interval.nil?
      if starts_at and ret and starts_at.localtime.dst? and !ret.localtime.dst?
        ret += 1.hour
      end
      ret
    end
  end
end
