class Tunecore::MusicSearch
  def self.album_search(person, options={})
    search_term = "'%#{options[:search]}%'"
    extra_condition = options[:release_type] ? "albums.album_type = '#{options[:release_type]}'" : "albums.album_type != 'Ringtone'"
    options[:order] ||= "golive_date desc"

    if options[:search]
      Album.paginate_by_sql(
        "select searched_albums.*, min(song_match) as song_matched from
         ((SELECT albums.*, false as song_match from albums
          left join creatives c1 on c1.creativeable_id = albums.id and c1.creativeable_type = 'Album'
          left join artists a1 on c1.artist_id = a1.id
          WHERE albums.person_id = #{person.id} AND #{extra_condition} AND albums.takedown_at IS NULL AND
                albums.finalized_at IS NOT NULL AND (UPPER(albums.name) LIKE UPPER(#{search_term}) OR UPPER(a1.name) LIKE UPPER(#{search_term}))
         )

         UNION

         (SELECT albums.*, true as song_match from albums
          inner join songs on albums.id = songs.album_id
          left  join creatives c2 on c2.creativeable_id = songs.id and c2.creativeable_type = 'Song'
          left join artists a2 on c2.artist_id = a2.id
          WHERE albums.person_id = #{person.id} AND #{extra_condition} AND albums.takedown_at IS NULL AND
                albums.finalized_at IS NOT NULL AND (UPPER(songs.name) LIKE UPPER(#{search_term}) OR UPPER(a2.name) LIKE UPPER(#{search_term}))
         )) as searched_albums
         group by searched_albums.id
         order by #{options[:order]}", :page => options[:page], :per_page => options[:per_page]
      )
    else
      person.albums
        .where(extra_condition)
        .finalized
        .not_taken_down
        .order(options[:order])
        .paginate(page: options[:page], per_page: options[:per_page])
    end
  end

  def self.song_search(person, options={})
    search_term = "'%#{options[:search]}%'"
    extra_condition = options[:release_type] ? "albums.album_type = '#{options[:release_type]}'" : "albums.album_type != 'Ringtone'"
    options[:order] ||= "golive_date desc"
    options[:order] = "songs.#{options[:order]} " if options[:order].include?("name")

    if options[:soundout_album_id]
      album_id_or_ids = options[:soundout_album_id].split(',')
      extra_condition += " AND albums.id IN ('#{album_id_or_ids.join("','")}')"
      options[:order] = "track_num"
      options[:per_page] = 1000000
    end

    options[:order] = "albums.#{options[:order]}" if options[:order].include?('golive_date')

    if options[:search]
      Song.paginate_by_sql(
        "select distinct songs.* from songs
         inner join albums on songs.album_id = albums.id
         left join creatives c1 on c1.creativeable_id = albums.id and c1.creativeable_type = 'Album'
         left join artists a1 on c1.artist_id = a1.id
         left join creatives c2 on c2.creativeable_id = songs.id and c2.creativeable_type = 'Song'
         left join artists a2 on c2.artist_id = a2.id
         WHERE albums.person_id = #{person.id} AND #{extra_condition} AND albums.takedown_at IS NULL AND
               albums.finalized_at IS NOT NULL AND (UPPER(songs.name) LIKE UPPER(#{search_term}) OR
               UPPER(a1.name) LIKE UPPER(#{search_term}) OR UPPER(a2.name) LIKE UPPER(#{search_term}))
         order by #{options[:order]}", :page => options[:page], :per_page => options[:per_page]
      )
    else
      person.songs
        .joins(album: :distributions)
        .where(extra_condition)
        .where('albums.takedown_at IS NULL AND albums.finalized_at IS NOT NULL')
        .order(options[:order])
        .paginate(page: options[:page], per_page: options[:per_page])
    end
  end

  def self.soundout_report_search(person, options={})
    if options[:order].present?
      sort_order = "asc"
    else
      options[:order] = "report_requested_tmsp"
      sort_order = "desc"
    end

    extra_condition = "purchases.paid_at IS NOT NULL"

    if options[:search]
      if options[:order].include?("name")
        library_order = "song_library_uploads.#{options[:order]}"
        options[:order] = "songs.#{options[:order]} COLLATE utf8_unicode_ci"
      else
        library_order = options[:order]
      end


      search_term = "'%#{options[:search]}%'"
      SoundoutReport.paginate_by_sql(
        "(select distinct soundout_reports.*, #{options[:order]} as sort_condition from soundout_reports
         inner join purchases on soundout_reports.id = purchases.related_id AND purchases.related_type = 'SoundoutReport'
         left join soundout_report_data on soundout_reports.id = soundout_report_data.soundout_report_id
         inner join soundout_products on soundout_reports.soundout_product_id = soundout_products.id
         inner join songs on soundout_reports.track_id = songs.id and soundout_reports.track_type = 'Song'
         inner join albums on songs.album_id = albums.id
         left join creatives c1 on c1.creativeable_id = albums.id and c1.creativeable_type = 'Album'
         left join artists a1 on c1.artist_id = a1.id
         left join creatives c2 on c2.creativeable_id = songs.id and c2.creativeable_type = 'Song'
         left join artists a2 on c2.artist_id = a2.id
         WHERE #{extra_condition} AND albums.person_id = #{person.id} AND (UPPER(songs.name) LIKE UPPER(#{search_term})
         OR UPPER(a2.name) LIKE UPPER(#{search_term}) OR UPPER(a1.name) LIKE UPPER(#{search_term})))

         UNION

         (select distinct soundout_reports.*, #{library_order} as sort_condition from soundout_reports
         inner join purchases on soundout_reports.id = purchases.related_id AND purchases.related_type = 'SoundoutReport'
         left join soundout_report_data on soundout_reports.id = soundout_report_data.soundout_report_id
         inner join soundout_products on soundout_reports.soundout_product_id = soundout_products.id
         inner join song_library_uploads on soundout_reports.track_id = song_library_uploads.id and soundout_reports.track_type = 'SongLibraryUpload'
         WHERE #{extra_condition} AND soundout_reports.person_id = #{person.id} AND UPPER(song_library_uploads.name) LIKE UPPER(#{search_term}))
         order by sort_condition #{sort_order}", page: options[:page], per_page: options[:per_page]
      )
    else
      join_terms = "inner join purchases on soundout_reports.id = purchases.related_id AND purchases.related_type = 'SoundoutReport'"
      if options[:order].include?("name")
        join_terms += " left join songs on soundout_reports.track_id = songs.id and soundout_reports.track_type = 'Song'
          left join song_library_uploads on soundout_reports.track_id = song_library_uploads.id and soundout_reports.track_type = 'SongLibraryUpload'"
        result = SoundoutReport.select("soundout_reports.*, COALESCE(songs.name, song_library_uploads.name) as sort_condition")
                               .joins(join_terms)
                               .where("#{extra_condition} AND soundout_reports.person_id = #{person.id}")
                               .order("sort_condition")
      else
        join_terms += " left join soundout_report_data on soundout_reports.id = soundout_report_data.soundout_report_id" if options[:order].include?("track_rating") || options[:order].include?("market_potential")
        result = person.soundout_reports.joins(join_terms).where(extra_condition).order("#{options[:order]} #{sort_order}")
      end
      result = result.paginate(page: options[:page], per_page: options[:per_page])
      result
    end
  end
end
