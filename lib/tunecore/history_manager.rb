module Tunecore
  class HistoryManager

    CREDIT_USAGE = "Credit Usage".freeze

    RELATED_FILTERS = {
      :salepoints => 'Salepoint',
      :renewals   => 'Renewal',
      :add_ons    => ['SalepointSubscription', 'Booklet'],
      :albums     => 'Album',
      :credits    => 'CreditUsage'
    }

    attr_accessor :manager
    attr_accessor :person
    attr_accessor :invoices

    def initialize(person)
      self.person = person
      self.invoices = nil
      self.manager = {}
    end

    def generate_data
      self.invoices = self.person.invoices.where("settled_at IS NOT NULL").order("settled_at DESC")
      if person.grandfathered_pubadmin_user?
        ytm_product_id = Product.find_products_for_country(person.country_domain, :ytm)
        self.invoices = self.invoices.joins(:purchases).where(["product_id not in (?)", ytm_product_id]).distinct
      end

      # Need to do seperately to properly handle includes through polymorphic associations
      self.manager[:salepoints] = self.person.purchases.related_type_filter(RELATED_FILTERS[:salepoints]).includes([:product, { :related => :salepointable } ]).group_by(&:invoice_id)
      self.manager[:renewals]   = self.person.purchases.related_type_filter(RELATED_FILTERS[:renewals]).includes([:product,   { :related => { :renewal_items => :related} } ]).group_by(&:invoice_id)
      self.manager[:add_ons]    = self.person.purchases.related_type_filter(RELATED_FILTERS[:add_ons]).includes([:product, { :related => :album } ]).group_by(&:invoice_id)
      self.manager[:albums]     = self.person.purchases.related_type_filter(RELATED_FILTERS[:albums]).includes([:product, :related]).group_by(&:invoice_id)
      self.manager[:credits]    = self.person.purchases.related_type_filter(RELATED_FILTERS[:credits]).includes([:product, { :related => :related } ]).group_by(&:invoice_id)
      self.manager[:others]     = self.person.purchases.related_type_exlude(RELATED_FILTERS.values.flatten).group_by(&:invoice_id)
    end

    def line_items(invoice)
      (RELATED_FILTERS.keys).each do |key|
        items = line_items_for_group(self.manager[key][invoice.id], true)
        yield items if items.present?
      end

      items = line_items_for_group(self.manager[:others][invoice.id], false)
      yield items if items.present?
    end

    private

    def line_items_for_group(group, album_filter)
      line_items = []
      albums = []

      groups = group.try(:group_by) { |p| p.product_id }
      if groups
        groups.each do |k, purchases|
          if album_filter
            purchases.each do |purchase|
              next if purchase.is_plan?

              name = album_name(purchase)
              albums << name if name
            end
          end

          item = { :purchases => { :size => purchases.size, :name => display_name_or_plan(purchases) } }
          item[:albums] = albums if album_filter
          line_items << item
        end
      end

      line_items
    end

    def album_name(purchase)
      if purchase.related
        case purchase.related_type
        when 'Salepoint'
          purchase.related.salepointable.name
        when 'Album'
          purchase.related.name
        when 'Renewal'
          purchase.related.items.first.related.name
        when 'CreditUsage'
          purchase.related.related.name
        when 'Booklet', 'SalepointSubscription'
          purchase.related.album.name
        else
          nil
        end
      else
        nil
      end
    end

    def display_name_or_plan(purchases)
      if purchases.first.product.name == CREDIT_USAGE && purchases.first.related&.plan_credit
        return I18n.t(".album.select_price_has_credit.distribute_with_plan")
      end

      purchases.first.product.display_name
    end
  end
end
