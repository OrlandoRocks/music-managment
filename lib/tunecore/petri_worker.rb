module Tunecore
  class PetriWorker
    PRODUCTION_PETRI_QUEUE = "production-petri-distribution".freeze

    def initialize
      # TODO: IF more parts of the application need ec2 api access, move this
      # into `AwsWrapper`
      @ec2_client = AWS::EC2.new
    end

    # Get the PETRI instances that are currently running
    def list
      all_instances.select { |instance| running_petri_instance?(instance) }
    end

    def queue_size
      AwsWrapper::Sqs.size(queue_name: PRODUCTION_PETRI_QUEUE)
    end

    # Kills a given number of workers in the array
    def prune_by(amount)
      destroy!(list[0..(amount - 1)].map(&:id))

      nil
    end

    private

    def running_petri_instance?(instance)
      instance.status == :running && instance.image_id == PETRI_WORKER_IMAGE_ID
    end

    # Get all the instances that are currently running.
    # Will return non-PETRI instances if there are some running (there probably will be).
    def all_instances
      @all_instances || @ec2_client.instances
    end

    # kill specific workers
    def destroy!(instance_ids)
      @ec2_client.terminate_instances(instance_ids: instance_ids)
    end
  end
end
