class Tunecore::Flagging::TransferFlagger
  #
  # assumptions:
  # the set of transfers implements a transfer_status column in which
  # completed is the final 'good' state
  # the attribute in question is a string
  #

  def initialize(flagged_object, transfers, attribute)
    @flagged_object = flagged_object
    @attribute      = attribute
    @flags          = {}
    @transfers      = transfers
  end

  attr_reader :flagged_object, :flags, :attribute

  def set_flags
    flags[attribute] = set_flag_for_attribute
    flags
  end

  private

  def set_flag_for_attribute
    any_completed_tranfers? && no_matching_attributes?
  end

  def completed_transfers
    @completed_transfers ||= @transfers.where(transfer_status: 'completed').map { |result| result.send(attribute).downcase }
  end

  def any_completed_tranfers?
    completed_transfers.any?
  end

  def no_matching_attributes?
    !completed_transfers.include?(flagged_object.send(attribute).downcase)
  end
end
