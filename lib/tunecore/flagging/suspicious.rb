module Tunecore::Flagging::Suspicious
  extend ActiveSupport::Concern

  # in order to use this module in a model, you need
  # to add a suspicious_flags column to the table
  # and implement a set_suspicious_flags method in the
  # model.

  included do
    serialize   :suspicious_flags, QuirkyJson
    before_save :set_suspicious_flags
  end

  def set_suspicious_flags
    raise NotImplementedError.new
  end

  def suspicious?(flag)
    suspicious_flags.has_key?(flag) && suspicious_flags[flag]
  end
end
