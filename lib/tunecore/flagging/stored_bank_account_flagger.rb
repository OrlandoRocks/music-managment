class Tunecore::Flagging::StoredBankAccountFlagger
  #
  # assumtions:
  # flagged 'belongs_to' stored_bank_accounts
  # flagged implements a 'status' column with a happy path eq to 'success'
  #
  def initialize(flagged)
    @flagged = flagged
    @flags   = {}
    @bank_account = StoredBankAccount.find(flagged.stored_bank_account_id)
  end

  attr_reader :flagged, :flags, :bank_account

  def set_flags
    flags[:stored_bank_account] = set_stored_bank_account_flag
    flags
  end

  private

  def set_stored_bank_account_flag
    any_successful? && acount_updated_after?
  end

  def all_successful
    @as ||= bank_account.send("#{flagged.class.to_s.underscore}s").where(status: 'success')
  end

  def any_successful?
    all_successful.any?
  end

  def acount_updated_after?
    bank_account.updated_at > all_successful.last.updated_at
  end
end
