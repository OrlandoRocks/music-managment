module Tunecore
  class ProductUrl
    @@product_urls = YAML.load_file(File.join(Rails.root, "config", "product_urls.yml")).with_indifferent_access

    def self.generate(brand, type, country_website)
      new.link(brand, type, country_website)
    end

    def link(brand, type, country_website)
      @@product_urls[brand][type].try(:[], country_website) || @@product_urls[brand][type][:us]
    end
  end
end
