# frozen_string_literal: true

module Tunecore
  module People
    module VatCompliance
      extend ActiveSupport::Concern

      included do
        scope :without_completed_compliance_info, -> do
          joins("LEFT JOIN compliance_info_fields cif1 ON cif1.person_id = people.id AND cif1.field_name = 'first_name'")
          .joins("LEFT JOIN compliance_info_fields cif2 ON cif2.person_id = people.id AND cif2.field_name = 'last_name'")
          .joins("LEFT JOIN compliance_info_fields cif3 ON cif3.person_id = people.id AND cif3.field_name = 'company_name'")
          .where(<<~SQL
            (
              customer_type = 'individual' AND NOT (cif1.id IS NOT NULL AND cif2.id IS NOT NULL)
              OR customer_type = 'company' AND cif3.id IS NULL
            )
          SQL
          )
        end
      end
    end
  end
end
