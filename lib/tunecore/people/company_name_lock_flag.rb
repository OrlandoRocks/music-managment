# frozen_string_literal: true

module Tunecore
  module People
    module CompanyNameLockFlag
      extend ActiveSupport::Concern

      def company_name_locked?
        flags.where(Flag::COMPANY_NAME_LOCKED).exists?
      end

      # Locks company_name if unlocked, and does nothing if already locked
      def lock_company_name(admin_person_id: 0, reason: nil)
        return if company_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flags << company_name_locked_flag
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'add',
            flag_id: company_name_locked_flag.id,
            comment: reason
          )
        end
      end

      # Unlocks company_name if locked, and does nothing if already unlocked
      def unlock_company_name(admin_person_id: 0, reason: nil)
        return unless company_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'remove',
            flag_id: company_name_locked_flag.id,
            comment: reason
          )
          flags.destroy(company_name_locked_flag)
        end
      end

      private

      def company_name_locked_flag
        Flag.find_by(
          category: Flag::COMPANY_NAME_LOCKED[:category],
          name: Flag::COMPANY_NAME_LOCKED[:name]
        )
      end
    end
  end
end
