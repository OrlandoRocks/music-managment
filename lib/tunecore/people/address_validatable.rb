module Tunecore
  module People
    module AddressValidatable
      extend ActiveSupport::Concern

      class AddressLockedError < StandardError; end

      ADDRESS_REGEX             = "(^[\\p{L}0-9_ .()@#',*/-]*$)".freeze
      AREA_REGEX                = "(^[\\p{L} .'-]*$)".freeze
      POSTAL_CODE_REGEX         = "(^[\\p{L}0-9_ -]*$)".freeze
      ADDRESS_FIELDS_REGEX_FLAG = "u".freeze

      included do
        validate :no_address_changes, on: [:update], if: :address_locked?

        validates_presence_of :address1, :city, :postal_code, :foreign_postal_code,
                              on: [:update],
                              if: :validate_address_for_non_us?
        validates_presence_of :country, on: [:update], if: :validate_address?
        validates_format_of :address1, :address2,
                            on: [:update],
                            with: Regexp.new(ADDRESS_REGEX, ADDRESS_FIELDS_REGEX_FLAG),
                            allow_blank: true,
                            if: :validate_address?
        validates_format_of :city, :state,
                            on: [:update],
                            with: Regexp.new(AREA_REGEX, ADDRESS_FIELDS_REGEX_FLAG),
                            allow_blank: true,
                            if: :validate_address?
        validates_format_of :postal_code,
                            on: [:update],
                            with: Regexp.new(POSTAL_CODE_REGEX, ADDRESS_FIELDS_REGEX_FLAG),
                            allow_blank: true,
                            if: :validate_address?
        validate :valid_state, on: [:update], if: :validate_address_for_us?
        validate :valid_country, on: [:update], if: :validate_address?

        before_save :format_state, if: :validate_address_for_us?

        attr_accessor :require_address, :override_address_lock
      end

      def require_address?
        return false if require_address.blank?

        require_address
      end

      def validate_address?
        require_address? && FeatureFlipper.show_feature?(:bi_transfer, self)
      end

      def validate_address_for_us?
        validate_address? && from_united_states_and_territories?
      end

      def validate_address_for_non_us?
        validate_address? && !from_united_states_and_territories?
      end

      def format_state
        return unless state.present?

        us_state = UsState.by_name_or_abbr(state).first
        self[:state] = us_state.abbreviation
      end

      def valid_state
        return unless state.present? && from_united_states_and_territories? && UsState.by_name_or_abbr(state).blank?

        errors.add(:state, I18n.with_locale(locale) {I18n.t("errors.messages.invalid")})
      end

      def valid_country
        return unless Country.find_by_name_or_iso_code(self[:country]).blank?

        errors.add(:country, I18n.with_locale(locale) {I18n.t("errors.messages.invalid")})
      end

      def no_address_changes
        return unless FeatureFlipper.show_feature?(:bi_transfer, self)
        return unless address_fields_changed? && !override_address_lock

        raise AddressLockedError, "Sorry, address fields are locked,
          attributes: #{attributes},attempted changes: #{changes}".freeze
      end

      def address_fields_changed?
        [
          address1_changed?,
          address2_changed?,
          city_changed?,
          state_changed?,
          foreign_postal_code_changed?,
          country_changed?
        ].any?
      end
    end
  end
end
