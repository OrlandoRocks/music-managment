# frozen_string_literal: true

module Tunecore
  module People
    module LastNameLockFlag
      extend ActiveSupport::Concern

      def last_name_locked?
        flags.where(Flag::LAST_NAME_LOCKED).exists?
      end

      # Locks last_name if unlocked, and does nothing if already locked
      def lock_last_name(admin_person_id: 0, reason: nil)
        return if last_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flags << last_name_locked_flag
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'add',
            flag_id: last_name_locked_flag.id,
            comment: reason
          )
        end
      end

      # Unlocks last_name if locked, and does nothing if already unlocked
      def unlock_last_name(admin_person_id: 0, reason: nil)
        return unless last_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'remove',
            flag_id: last_name_locked_flag.id,
            comment: reason
          )
          flags.destroy(last_name_locked_flag)
        end
      end

      private

      def person_flags_relation
        flags.where(
          category: Flag::LAST_NAME_LOCKED[:category],
          name: Flag::LAST_NAME_LOCKED[:name]
        )
      end

      def last_name_locked_flag
        Flag.find_by(
          category: Flag::LAST_NAME_LOCKED[:category],
          name: Flag::LAST_NAME_LOCKED[:name]
        )
      end
    end
  end
end
