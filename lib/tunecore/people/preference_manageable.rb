module Tunecore
  module People
    module PreferenceManageable
      extend ActiveSupport::Concern
      def set_payment_preference(card, pay_with_balance)
        if person_preference.nil?
          PersonPreference.create(person_id: id, pay_with_balance: pay_with_balance,
                                  preferred_credit_card: card, preferred_payment_type: "CreditCard")
        else
          person_preference.update(pay_with_balance: pay_with_balance,
                                              preferred_credit_card: card,
                                              preferred_payment_type: "CreditCard")
        end
      end
    end
  end
end
