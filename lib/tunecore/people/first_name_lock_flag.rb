# frozen_string_literal: true

module Tunecore
  module People
    module FirstNameLockFlag
      extend ActiveSupport::Concern

      def first_name_locked?
        flags.where(Flag::FIRST_NAME_LOCKED).exists?
      end

      # Locks first_name if unlocked, and does nothing if already locked
      def lock_first_name(admin_person_id: 0, reason: nil)
        return if first_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flags << first_name_locked_flag
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'add',
            flag_id: first_name_locked_flag.id,
            comment: reason
          )
        end
      end

      # Unlocks first_name if locked, and does nothing if already unlocked
      def unlock_first_name(admin_person_id: 0, reason: nil)
        return unless first_name_locked?

        ActiveRecord::Base.transaction(requires_new: true) do
          flag_transitions.create(
            updated_by: admin_person_id,
            add_remove: 'remove',
            flag_id: first_name_locked_flag.id,
            comment: reason
          )
          flags.destroy(first_name_locked_flag)
        end
      end

      private

      def first_name_locked_flag
        Flag.find_by(
          category: Flag::FIRST_NAME_LOCKED[:category],
          name: Flag::FIRST_NAME_LOCKED[:name]
        )
      end
    end
  end
end
