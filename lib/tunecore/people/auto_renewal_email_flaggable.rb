# frozen_string_literal: true

module Tunecore
  module People
    module AutoRenewalEmailFlaggable
      extend ActiveSupport::Concern

      included do
        scope :with_sent_auto_renewal_email, -> do
          with_flag(Flag::AUTO_RENEWAL_EMAIL_SENT)
        end

        scope :without_sent_auto_renewal_email, ->(table_alias: "f_sent") do
          without_flag(Flag.find_by(Flag::AUTO_RENEWAL_EMAIL_SENT))
        end
      end

      def auto_renewal_email_sent?
        Person::FlagService.person_flag_exists?(
          person: self,
          flag_attributes: Flag::AUTO_RENEWAL_EMAIL_SENT
        )
      end

      def mark_auto_renewal_email_sent!(admin_person_id: 0)
        return if auto_renewal_email_sent?

        Person::FlagService.flag!(
          person: self,
          flag_attributes: Flag::AUTO_RENEWAL_EMAIL_SENT,
          admin_person_id: admin_person_id
        )
      end

      def mark_auto_renewal_email_unsent!(admin_person_id: 0)
        return unless auto_renewal_email_sent?

        Person::FlagService.unflag!(
          person: self,
          flag_attributes: Flag::AUTO_RENEWAL_EMAIL_SENT,
          admin_person_id: admin_person_id
        )
      end
    end
  end
end
