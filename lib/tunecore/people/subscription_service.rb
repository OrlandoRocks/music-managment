module Tunecore
  module People
    module SubscriptionService
      extend ActiveSupport::Concern

      included do
        has_many :subscription_purchases

        scope :with_active_social_subscription, -> do
          joins(:person_subscription_statuses)
            .merge(PersonSubscriptionStatus.active.social)
        end

        scope :with_active_social_subscription_with_termination_date, -> do
          joins(:person_subscription_statuses)
            .merge(PersonSubscriptionStatus.active.social.with_termination_date)
        end
      end

      def active_subscription_expires_at(subscription_type:)
        person_subscription_statuses
          .where(subscription_type: subscription_type)
          .active.last&.get_termination_date
      end

      def subscription_payment_channel_for(subscription_type)
        se_table = SubscriptionEvent.arel_table
        subscription_purchases
          .joins(:subscription_event)
          .where(se_table[:subscription_type].eq(subscription_type))
          .order("subscription_purchases.termination_date DESC")
          .limit(1).last.try(:payment_channel)
      end

      def has_active_subscription_product_for?(subscription_type)
        !!subscription_status_for(subscription_type).try(:is_active?)
      end

      def has_subscription_product_in_cart_for?(subscription_type)
        subscription_purchases = self.purchases.where(paid_at: nil, related_type: "SubscriptionPurchase")
        return false if subscription_purchases.empty?
        subscription_purchases.any? do |p|
          p.related.subscription_product.product_name == subscription_type
        end
      end

      def cleanup_unpaid_subscriptions(subscription_type)
        unpaid_purchases = self.purchases.where(paid_at: nil, related_type: "SubscriptionPurchase")
        unpaid_purchases.each do |p|
          if p.related
            p.related.destroy
          end
        end
      end

      def remove_unpaid_subscriptions_for_social(subscription_type)
        unpaid_purchases = self.purchases.where(paid_at: nil, related_type: "SubscriptionPurchase")
        unpaid_purchases.each do |p|
          if p.related.subscription_product.product_name == subscription_type
            p.related.destroy
          end
        end
      end

      def cancel_subscription_for(subscription_type)
        subscription_status = subscription_status_for(subscription_type)
        if subscription_status && subscription_status.canceled_at.nil?
          event = SubscriptionEvent.new(
            event_type: "Cancellation",
            subscription_type: subscription_type,
            person_subscription_status: subscription_status)

          event.person_subscription_status.update(canceled_at: Time.now) if event.save
        end
      end

      def subscription_status_for(subscription_type)
        person_subscription_statuses.where(subscription_type: subscription_type).last
      end

      def subscription_event_history_for(subscription_type)
        self.subscription_status_for(subscription_type).subscription_events.order(id: :desc).as_json(root: false, only: [:created_at, :event_type, :price])
      end

      def subscription_purchase_for(status)
        event = preloaded_subscription_event_for(status)
        event.subscription_purchase if event
      end

      def preloaded_subscription_event_for(status)
        status.subscription_events
          .order("created_at DESC")
          .limit(1)
          .first if status
      end
    end
  end
end
