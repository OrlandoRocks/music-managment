module Tunecore
  module People
    module TaxWithholdable
      extend ActiveSupport::Concern

      def exempt_withholding!(withholding_exemption_reason = :other)
        return :active_exemption_exists if exempt_from_withholding?

        withholding_exemptions.create(
          withholding_exemption_reason: WithholdingExemptionReason.find_by(name: withholding_exemption_reason),
          started_at: DateTime.current,
        )
      end

      def exempt_from_withholding?
        withholding_exemptions.active.present?
      end

      def remove_withholding_exemption!
        withholding_exemptions.active.update_all(ended_at: DateTime.current)
      end
    end
  end
end
