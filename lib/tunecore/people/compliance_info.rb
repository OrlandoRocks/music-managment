module Tunecore
  module People
    module ComplianceInfo
      extend ActiveSupport::Concern

      def compliance_first_name
        return unless individual?

        @compliance_first_name ||= field_value(for_field: :first_name)
      end

      def compliance_first_name?
        compliance_first_name.present?
      end

      def compliance_last_name
        return unless individual?

        @compliance_last_name ||= field_value(for_field: :last_name)
      end

      def compliance_last_name?
        compliance_last_name.present?
      end

      def compliance_company_name
        return unless business?

        @compliance_company_name ||= field_value(for_field: :company_name)
      end

      def compliance_company_name?
        compliance_comapny_name.present?
      end

      private

      def field_value(for_field:)
        compliance_info_fields.where(field_name: for_field)
                              .pluck(:field_value)
                              .first
      end
    end
  end
end
