module Tunecore
  module People
    module RewardSystem
      extend ActiveSupport::Concern

      def rewards
        Reward
          .joins(tiers: :tier_people)
          .is_active
          .where(tier_people: { person_id: self.id })
      end

      def points_balance
        person_points_balance || create_person_points_balance(balance: 0)
      end

      def available_points
        points_balance.balance
      end
    end
  end
end
