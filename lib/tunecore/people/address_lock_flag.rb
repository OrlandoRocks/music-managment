# frozen_string_literal: true

module Tunecore
  module People
    module AddressLockFlag
      extend ActiveSupport::Concern

      def address_locked?(reason: nil)
        Person::FlagService.person_flag_exists?(
          person: self,
          flag_attributes: Flag::ADDRESS_LOCKED,
          flag_reason: reason
        )
      end

      # Locks address if unlocked, and does nothing if already locked
      def lock_address!(admin_person_id: 0, reason:)
        return if address_locked?(reason: reason)

        Person::FlagService.flag!(
          person: self,
          flag_attributes: Flag::ADDRESS_LOCKED,
          admin_person_id: admin_person_id,
          flag_reason: reason
        )
      end

      # Unlocks address if locked, and does nothing if already unlocked
      def unlock_address!(admin_person_id: 0, reason:)
        return unless address_locked?(reason: reason)

        Person::FlagService.unflag!(
          person: self,
          flag_attributes: Flag::ADDRESS_LOCKED,
          admin_person_id: admin_person_id,
          flag_reason: reason
        )
      end

      def payoneer_kyc_address_locked?
        address_locked?(reason: FlagReason.payoneer_kyc_completed_address_lock)
      end

      def self_identified_address_locked?
        reason = FlagReason
          .address_lock_flag_reasons
          .find_by(reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED)

        address_locked?(reason: reason)
      end
    end
  end
end
