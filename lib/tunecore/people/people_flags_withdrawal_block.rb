module Tunecore
  module People
    module PeopleFlagsWithdrawalBlock
      extend ActiveSupport::Concern

      def blocked_withdrawal?
        withdrawal_people_flag.present?
      end

      def block_withdrawal!(admin=nil, remote_ip=nil)
        return false if blocked_withdrawal?

        flag = withdrawal_flag
        admin_id = admin&.id || 0
        people_flag = people_flags.create(flag: flag)

        Person.transaction(requires_new: true) do
          create_flag_note(admin_id, remote_ip, 'Blocked')

          FlagTransition.create_trans(
            updated_by: admin_id,
            comment: "Withdrawals for #{name} is blocked by #{admin&.name || 'system'}",
            add_remove: 'add',
            flag_id: flag.id,
            person_id: self.id
          )
        end
      end

      def unblock_withdrawal!(admin=nil, remote_ip=nil)
        return false unless blocked_withdrawal?

        flag = withdrawal_flag
        admin_id = admin&.id || 0

        Person.transaction(requires_new: true) do
          create_flag_note(admin_id, remote_ip, 'Unblocked')

          FlagTransition.create_trans(
            updated_by: admin_id,
            comment: "Withdrawals for #{name} is unblocked by #{admin&.name || 'system'}",
            add_remove: 'remove',
            flag_id: flag.id,
            person_id: self.id
          )
          withdrawal_people_flag.destroy
        end
      end

      def blocked_for_incorrect_taxform?
        withdrawal_people_flag&.flag_reason&.reason == FlagReason::INCORRECT_TAX_FORM
      end

      private

      def create_flag_note(admin_id=0, remote_ip, block_state)
        Note.create(
          {
            related: self,
            subject: "#{block_state} Withdrawal",
            note: "Withdrawal has been #{block_state} by ",
            note_created_by_id: admin_id,
            ip_address: remote_ip
          }
        )
      end

      def withdrawal_category
        Flag::WITHDRAWAL[:category]
      end

      def withdrawal_name
        Flag::WITHDRAWAL[:name]
      end

      def withdrawal_flag
        Flag.find_by(category: withdrawal_category, name: withdrawal_name)
      end

      def withdrawal_people_flag
        people_flags.find_by(flag: withdrawal_flag)
      end
    end
  end
end
