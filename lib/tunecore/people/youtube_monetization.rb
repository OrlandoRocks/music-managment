module Tunecore
  module People
    module YoutubeMonetization
      extend ActiveSupport::Concern

      included do
        has_many :youtube_track_monetizations, -> {
            where(store_id: Store::YTSR_STORE_ID)
          }, class_name: "TrackMonetization"

        has_many :monetized_youtube_track_monetizations, -> {
            where(store_id: Store::YTSR_STORE_ID)
              .where(state: TrackMonetization::STATES.select{ |state| state.to_s == "delivered" }, takedown_at: nil)
              .where.not(state: nil)
          }, class_name: "TrackMonetization"

        has_many :ineligible_youtube_track_monetizations, -> {
            where(store_id: Store::YTSR_STORE_ID, eligibility_status: TrackMonetization::INELIGIBLE)
          }, class_name: "TrackMonetization"

        has_many :unsent_youtube_track_monetizations, -> {
            where(store_id: Store::YTSR_STORE_ID, state: TrackMonetization::STATES[0])
          }, class_name: "TrackMonetization"
      end

      def has_active_ytm?
        youtube_monetization.present? &&
        youtube_monetization.effective_date.present? &&
        (youtube_monetization.effective_date <= Time.now) &&
        (youtube_monetization.termination_date.nil? || youtube_monetization.termination_date > Time.now)
      end

      def has_ytsr_proxy?
        albums.finalized.joins(:salepoints).where(salepoints: { store_id: Store::YTSR_PROXY_ID, takedown_at: nil}).any?
      end

      def ytm_blocked?
        (youtube_monetization&.block_date.present?) ||
          Person::FlagService.person_flag_exists?( {
            person: self,
            flag_attributes: Flag::BLOCKED_FROM_YT_MONEY
          })
      end

      def grandfathered?
        youtube_monetization.pub_opted_in
      end

      def has_purchased_ytm?
        youtube_monetization.present? &&
        youtube_monetization.effective_date.present? &&
        (youtube_monetization.effective_date <= Time.now)
      end

      def ytm_in_cart?
        self.purchases.unpaid.find_by(related_type: "YoutubeMonetization").present?
      end

      def has_sent_tracks?
        monetized_youtube_track_monetizations.present?
      end

      def terminate_ytm(note={}, takedown_source="Tunecore Admin")
        begin
          Person.transaction do
            youtube_monetization.update(termination_date: Time.now)
            takedown_ytsr_tracks(note, subject = 'Youtube Money Purchase Terminated', takedown_source)

            Note.create({related: self, subject: "Terminated Youtube Money", note: "YTM purchase has been terminated"}.merge(note))
          end
        rescue
          return false
        end

        return true
      end

      def handle_ytm_block
        youtube_monetization.update(block_date: Time.now) if youtube_monetization.present?

        Person::FlagService.flag!(
          {
            person: self,
            flag_attributes: Flag::BLOCKED_FROM_YT_MONEY
          }
        )
      end

      def block_ytm(note={}, admin_email="Tunecore Admin")
        begin
          Person.transaction do
            handle_ytm_block
            takedown_ytsr_tracks(note, subject = 'Youtube Money Blocked', admin_email)

            Note.create({
              related: self,
              subject: "YTSR Service Blocked",
              note: "YTM has been Blocked by #{admin_email}"
            }.merge(note))
          end
        rescue
          return false
        end

        true
      end

      def handle_ytm_unblock
        youtube_monetization.update(block_date: nil) if youtube_monetization.present?

        Person::FlagService.unflag!(
          {
            person: self,
            flag_attributes: Flag::BLOCKED_FROM_YT_MONEY
          }
        )
      end

      def unblock_ytm(note={}, admin_email="Tunecore Admin")
        begin
          Person.transaction do
            handle_ytm_unblock
            store = Store.find_by(abbrev: "ytsr")
            self.track_monetizations.where(store_id: store.id).update(state: 'new', takedown_at: nil)

            Note.create({related: self, subject: "YTSR Service Unblocked", note: "YTM has been Unblocked by #{admin_email}"}.merge(note))
          end
        rescue
          return false
        end

        true
      end

      def youtube_monetization
        @youtube_monetization ||= youtube_monetizations.last
      end

      def has_active_youtube_monetization?
        youtube_monetization.present? && !youtube_monetization.termination_date?
      end

      private

      def create_take_down_note(related:, note:, store:, subject:)
        Note.create(
          related: related,
          note_created_by: note[:note_create_by],
          ip_address: note[:ip_address],
          subject: subject,
          note: "#{related.class.name} was taken down from #{store.name}"
        )
      end

      def takedown_ytsr_tracks(note, subject, takedown_source)
        store = Store.find_by(abbrev: "ytsr")

        self.track_monetizations.where(store_id: store.id, takedown_at: nil).each do |track_monetization|
          TrackMonetization::TakedownService.takedown(
            track_monetizations: [track_monetization],
            takedown_source: takedown_source
          )

          @note = create_take_down_note(related: track_monetization.song, note: note, store: store, subject: subject)
        end
      end
    end
  end
end
