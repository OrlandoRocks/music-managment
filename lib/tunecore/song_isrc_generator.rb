#
#  ISRC Number must be in format:
#
#  CC-XXX-YY-NNNNN
#
#  c = country
#  x = company identifier
#  y = 2 digit year
#  n = song number identifier
#
#
module Tunecore
  class SongIsrcGenerator

    ID_LIMIT = 99999 # only 99999 will fit in NNNNN

    def initialize(song_id)
      @song_id = song_id
    end

    #  example:
    #  US-TCA-09-10000
    def isrc
      company_identifier = ordered_company_identifier
      tc_isrc = ISRC_PREFIX + company_identifier + isrc_year + last_five_digits
      find_duplicate = Song.exists?(tunecore_isrc: tc_isrc)

      # This change is support fetching next company identifier as we are currently having more than 99k per identifier.
      # So to create ISRC we are incrementing one company identifier
      find_duplicate && loop do
        tc_isrc = ISRC_PREFIX + company_identifier.next! + isrc_year + last_five_digits
        break tc_isrc unless Song.exists?(tunecore_isrc: tc_isrc)
      end

      tc_isrc
    end

    protected
    # this year w/o century
    def isrc_year
      Date.today.strftime("%y")
    end

    def last_five_digits
      if too_large?
        @song_id.to_s[-5,5]
      else
        sprintf("%05d",@song_id)
      end
    end

    # A - Z Generator
    def random_letter
      @random = (65 + rand(26)).chr
    end

    def ordered_company_identifier
      grouping = ""
      base_25 = (@song_id / 100000).to_s(25)
      if base_25.length > 3
        base_25 = base_25[-3..-1]
      end
      length = base_25.length
      if length == 1
        grouping += "AA"
      elsif length == 2
        grouping += "A"
      end

      (0..length - 1).each do |i|
        current = base_25[i,1].to_i(25)
        # This is to skip past the letter V
        current += 1 if current > 20

        # 2012-12-04 AK Don't use JP, that's for TuneCore Japan TCJP........
        if grouping == 'J' && current > 14 # O is the 14th letter of the alphabet, next is P
          current += 1
        end

        grouping += (current + 65).chr
      end

      # ensure that we never return a company identifier starting with JP
      if grouping[/^JP/]
        raise "Invalid ISRC: company identifiers starting with JP are reserved for TuneCore Japan"
      end

      return grouping
    end

    # 2012-12-04 AK This doesn't appear to be used any more
    def random_company_identifier
      grouping = ""

      (1..3).each do |i|
        f = random_letter

        # We totally randomize the last two letters of the group
        # But we cannot use V as the first letter of the group; used by Video ISRC
        if i == 1 && f == "V"
            f = random_letter until f != "V"
        end
        grouping += f
      end

      return grouping
    end

    #
    #  The song identifier is larger than the amount of spaces available
    #  the ISRC only allows for a song identifier of 5 characters
    #
    def too_large?
      @song_id > ID_LIMIT
    end
  end
end
