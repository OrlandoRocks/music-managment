module Tunecore
  class CurrencyInputNormalizer
    def self.normalize_currency_input(params, main_currency_method, fractional_currency_method)
      "#{(params[main_currency_method] || '0').tr('.,', '')}.#{params[fractional_currency_method] || '00'}"
    end
  end
end
