# [2010-03-08 -- CH]
# Added chargeable_salepoints method to satisfy SpawningFreeStores cert engine.

module Tunecore
  module PriceCalculator
    class ProductBase < PriceCalculator::Base

      def purchase_cost_cents
        if purchase.related_type != 'Product'
          item_to_price = purchase.related
        else
          item_to_price = purchase.product
        end

        final_total = Product.price(purchase.person, item_to_price, purchase.product)

        return final_total.cents.to_s
      end

      def purchase_discount_cents
        @discount_amount_cents ||= case
        when targeted_product && cert.nil? && purchase.price_adjustment_histories_count == 0
          targeted_product.discount_amount_cents(purchase_cost_cents)
        when cert
          cert.discount_amount_cents(self)
        else
          0
        end
      end

      def purchase_discount_cents_and_reason
        return [@discount_amount_cents, @discount_reason] if !@discount_amount_cents.nil?
        
        @discount_amount_cents, @discount_reason =
        if apply_targeted_product_discount?
          [targeted_product.discount_amount_cents(purchase_cost_cents), Purchase::VALID_DISCOUNT_REASONS[:non_plan_discount]]
        elsif cert
          [cert.discount_amount_cents(self), Purchase::VALID_DISCOUNT_REASONS[:non_plan_discount]]
        else
          [0, Purchase::VALID_DISCOUNT_REASONS[:no_discount]]
        end
      end

      def net_cost
        (purchase_cost_cents.to_f - purchase_discount_cents.to_f) / 100.00
      end

      def purchase_discount_details
        describer = cert
        return nil unless describer
        describer.details
      end

      def allow_new_cert?
        not has_discounts?
      end

      def has_discounts?
        !!cert
      end

      def chargeable_salepoints
        if purchase.related_type == 'Album'
          purchase.purchased_item.salepoints
        else
          0
        end
      end

      def chargeable_salepoints_count
        chargeable_salepoints.size
      end


      protected

      def cert
        # hoop jumping to avoid hitting the DB all the time
        # looking for a cert that's not there...
        @cert ||= purchase.cert
      end

      def targeted_product
        TargetedProduct.targeted_product_for_active_offer(purchase.person, purchase.product, purchase.related)
      end

      def apply_targeted_product_discount?
        targeted_product && cert.nil? && purchase.price_adjustment_histories_count == 0
      end
    end
  end
end
