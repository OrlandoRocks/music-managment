module Tunecore
  class PriceCalculator::Base
    extend MoneyField
    attr_reader :purchaseable, :purchase

    # don't override base_price_cents - it should always be the value
    # as returned by the price_policy
    delegate :base_price_cents, :to => :price_policy

    # Add accessors to return money objects 
    money_reader :base_price, :base_cost, :total_cost, :purchase_cost, :purchase_discount, :purchase_total

    def initialize(purchase, purchaseable)
      self.purchase = purchase
      self.purchaseable = purchaseable
    end

    def price_policy
      @price_policy ||= PricePolicy[purchaseable]
    end

    # The base_cost of the current Purchaseable.  In general it is
    # just base_price_cents() but you can override this if you
    # need to manipulate the base_price in a way other than discounts.
    def base_cost_cents
      base_price_cents
    end

    # This method gives you a place to override if you are aggregating
    # costs in a way that's opaque to the associated Purchase.  This way
    # you can add your own base_cost_cents to the total_cost_cents for 
    # each of the opaque aggregates.
    # Note that when your calculator only aggregates Purchaseables enlisted
    # in a Purchase you should override purchase_cost_cents instead.
    def total_cost_cents
      base_cost_cents
    end
  
    # This is the non-discounted amount of pennies that will be charged to
    # the user, based on the Priceables aggregated by the current Purchase.
    # When we present a dollar amount the user sees for the current Purchaseable
    # we base it on this value.
    #
    # Note that calculators used by PricePolicies for any Purchaseable 
    # referenced by a Purchase as the related() *must* override this method.
    # Don't override this for any other calculators though - it's another safeguard
    # against setting a related() value for a Purchaseable that doesn't know how
    # to be standalone.
    def purchase_cost_cents
      raise "Unimplemented by subclass"
    end
  
    # This is the amount of pennies that will be discounted from the current
    # purchase.
    # When we present a dollar amount the user sees for the current Purchaseable
    # we base it on this value.
    #
    # Note that calculators used by PricePolicies for any Purchaseable 
    # referenced by a Purchase as the related() *must* override this method.
    # Don't override this for any other calculators though - it's another safeguard
    # against setting a related() value for a Purchaseable that doesn't know how
    # to be standalone.
    def purchase_discount_cents
      raise "Unimplemented by subclass"
    end

     # This is the amount of pennies that will be discounted from the current
    # purchase and the reason for the discount.
    # When we present a dollar amount the user sees for the current Purchaseable
    # we base it on this value.
    #
    # Note: discount reason is only currently utilized for unlimited plan related logic
    # but we can no longer allow this value to be null
    #
    # Note that calculators used by PricePolicies for any Purchaseable 
    # referenced by a Purchase as the related() *must* override this method.
    # Don't override this for any other calculators though - it's another safeguard
    # against setting a related() value for a Purchaseable that doesn't know how
    # to be standalone.

    def purchase_discount_cents_and_reason
      raise "Unimplemented by subclass"
    end
  
    # Don't override this - the math is pretty simple :-)
    def purchase_total_cents
      [purchase_cost_cents.to_i - purchase_discount_cents.to_i, 0].max
    end
  
    def purchase_discount_details
      nil
    end
  
    protected
  
    attr_writer :purchaseable, :purchase
  
    # def include_in_cost?
    #   not priced.finalized?
    # end
  
  end
end
