module Tunecore
  module Reports

    class ReleaseSalesReport < Tunecore::Reports::SalesReport
      include Tunecore::Reports::SalesReportFilter

      def initialize(person, type, options={})
        @person = person
        @type   = type

        @sip_store_lookup = {}
        @tc_store_lookup  = {}
        @release_lookup   = {}
        @song_lookup      = {}

        case type
        when :for_release
          raise "Must initialize with release" if options[:release].nil?
          @release = options[:release]
          @report_method = :for_release
        when :for_song
          raise "Must initialize with song_id and release_id" if options[:song_id].nil? || options[:release_id].nil?
          @song_id    = options[:song_id]
          @release_id = options[:release_id]
          @report_method = :for_song
        end
      end

      private

      #
      # Returns all sales of a release
      #
      def for_release(options = {})
        distribution_types = ["Download", "Streaming"] if options[:distribution_type].blank?
        distribution_types = options[:distribution_type].collect{ |dt| dt.capitalize } if options[:distribution_type]

        options[:query_modifier_conditions] = "sr.related_id = :qm_related_id and sr.related_type = :qm_related_type and sr.distribution_type in (:distribution_types)"
        options[:query_modifier_values]     = {qm_related_id: options[:related_id], qm_related_type: options[:related_type], distribution_types: distribution_types}

        #Get recent album sales and archived album sales
        album_sales = album_sales_query("sales_records", options)
        archived_album_sales = album_sales_query("sales_record_archives", options)
        album_sales += archived_album_sales
        
        downloads, streaming = true, true
        if !options[:distribution_type].blank?
           downloads = options[:distribution_type].include?("Download")
           streaming = options[:distribution_type].include?("Streaming")
        end
        options[:query_modifier_conditions] = "sr.related_type='Song' and sr.release_type =:qm_release_type and sr.release_id = :qm_release_id"
        options[:query_modifier_values]     = {qm_release_type: @release.class_name, qm_release_id: @release.id}

        #Generate having clause
        having = []
        if downloads
          having << "songs_sold > 0"
        end
        if streaming
          having << "streams_sold > 0"
        end

        #Get song sales for the release
        song_sales = ReadOnlyReplica::SalesRecord.select("songs.track_num as track_number,
                                                                songs.name as song_name,
                                                                songs.id as song_id,
                                                                SUM(IF( #{downloads}, sr.downloads_sold, 0)) AS songs_sold,
                                                                SUM(IF( #{streaming}, sr.streams_sold, 0)) AS streams_sold,
                                                                SUM(IF( #{downloads}, sr.download_amount, 0 )) as song_amount,
                                                                SUM(IF( #{streaming}, sr.stream_amount, 0 )) as stream_amount,
                                                                TRUNCATE(SUM(if( #{streaming}, sr.stream_amount,0) + if(#{downloads}, sr.download_amount, 0)),2) as formatted_amount,
                                                                srm.amount_currency as amount_currency,
                                                                srm.revenue_currency as revenue_currency").from("sales_record_summaries sr").joins("left join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                                               left join songs ON songs.id = sr.related_id
                                                               left join sip_stores s_stores ON srm.sip_store_id=s_stores.id
                                                               left join albums a ON a.id = songs.album_id").where(build_conditions_array(@person, options)).order("track_number asc").group("song_id").having(having.join(" or "))

        {album_sales: album_sales, song_sales: song_sales}
      end

      #
      # Returns all the sales records for a given song
      #
      def for_song(options = {})
        distribution_types = ["Download", "Streaming"] if options[:distribution_type].blank?
        distribution_types = options[:distribution_type].collect{ |dt| dt.capitalize } if options[:distribution_type]

        options[:query_modifier_conditions] = "(a.id = :qm_album_id and songs.id = :qm_songs_id) and sr.distribution_type in (:distribution_types)"
        options[:query_modifier_values] = {qm_album_id: @release_id, qm_songs_id: @song_id, distribution_types: distribution_types}

        return ReadOnlyReplica::SalesRecord.select("songs.track_num as track_number,
                                                          songs.name as song_name,
                                                          DATE_FORMAT(srm.period_sort, '%b %Y') as period_sort,
                                                          stores.name as store_name,
                                                          srm.country as country,
                                                          srm.revenue_currency as currency,
                                                          srm.amount_currency as amount_currency,
                                                          TRUNCATE(srm.exchange_rate,4) as exchange_rate,
                                                          songs.id as song_id,
                                                          sr.quantity as quantity,
                                                          sr.revenue_per_unit as formatted_revenue_per_unit,
                                                          sr.revenue_total as formatted_revenue_total,
                                                          sr.amount as formatted_amount").from("sales_records sr").joins("left join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                                         left join songs ON songs.id = sr.related_id
                                                         left join sip_stores s_stores ON srm.sip_store_id=s_stores.id
                                                         inner join stores ON stores.id = s_stores.store_id
                                                         left join albums a ON a.id = songs.album_id").where(build_conditions_array(@person, options)).order("track_number asc")
      end

      #
      # Aggregate summaries, results expected to be
      #
      def summarize(results, options={})
        summary = {}
        summary[:releases_sold]  = 0
        summary[:release_amount] = 0
        summary[:songs_sold]     = 0
        summary[:song_amount]    = 0
        summary[:streams_sold]   = 0
        summary[:stream_amount]  = 0
        summary[:total_earned]   = 0

        #Summarize album sales
        results[:album_sales].each do |result|

          if result.distribution_type == "Download"
            summary[:releases_sold]  += result.quantity
            summary[:release_amount] += result.formatted_amount
          elsif result.distribution_type == "Streaming"
            summary[:releases_sold]  += result.quantity
            summary[:streams_sold] += result.formatted_amount
          end

        end

        #Summarize song sales
        results[:song_sales].each do |result|
          #Downloads only
          if !options[:distribution_type].blank? && options[:distribution_type].include?("Download") and options[:distribution_type].size == 1

            summary[:songs_sold]     += result.songs_sold
            summary[:song_amount]    += result.song_amount

          #Streaming Only
          elsif !options[:distribution_type].blank? && options[:distribution_type].include?("Streaming") and options[:distribution_type].size == 1

            summary[:streams_sold]  += result.streams_sold
            summary[:stream_amount] += result.stream_amount

          #Everything
          else
            summary[:songs_sold]     += result.songs_sold
            summary[:song_amount]    += result.song_amount
            summary[:streams_sold]  += result.streams_sold
            summary[:stream_amount] += result.stream_amount

          end
        end

        summary[:total_earned] = summary[:song_amount] + summary[:release_amount] + summary[:stream_amount]
        summary[:currency] = results[:album_sales].first.try(:amount_currency) || results[:song_sales].first.try(:amount_currency) if results.present?
        summary
      end

      def album_sales_query(table_name, options)
        ReadOnlyReplica::SalesRecord.select("DATE_FORMAT(srm.period_sort, '%b %Y') as period_sort,
                                                             stores.name as store_name,
                                                             srm.country as country,
                                                             srm.revenue_currency as currency,
                                                             srm.amount_currency as amount_currency,
                                                             TRUNCATE(srm.exchange_rate,4) as exchange_rate,
                                                             sr.quantity as quantity,
                                                             sr.revenue_per_unit as formatted_revenue_per_unit,
                                                             sr.revenue_total as formatted_revenue_total,
                                                             sr.amount as formatted_amount,
                                                             sr.distribution_type").from("#{table_name} sr").joins("inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                                            left join sales_record_archives sra on sra.id=sr.sales_record_master_id
                                                            left join sip_stores s_stores ON srm.sip_store_id=s_stores.id
                                                            left join stores ON stores.id = s_stores.store_id").where(build_conditions_array(@person,options)).order("srm.period_sort desc")
      end

    end
  end
end
