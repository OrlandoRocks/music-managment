module Tunecore
  module Reports
    class SignupsByCampaign < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date type campaign signups conversions conversion_rate revenue per_person_revenue }
        super
      end

      def self.doc
        <<-EOSTRING
          Shows new account signups. The campaign is the last webpage that the user was on before beginning the signup process. The type of campaign can either be "internet" for any external website, "local" for another page on the tunecore site, or "google" for a link anywhere that was tagged with google analytics. Conversions are the number of new accounts who have paid an invoice to Tunecore. The conversion rate is the percentage of new accounts that paid an invoices to Tunecore. Per person revenue divides total revenue from the new accounts by the number of new accounts.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "people.created_on")
        group_conditions = [group_exp, "people.referral_campaign"].compact.join(", ")

        result_rows = Person.select("#{group_exp || "'ALL'"} as date, referral_type as type, referral_campaign as campaign, count(*) as signups, (sum(payment.total) / 100) as revenue, count(distinct(payment.person_id)) as conversions").joins("LEFT JOIN (SELECT person_id, sum(final_settlement_amount_cents) as total FROM invoices WHERE settled_at is not null GROUP BY person_id) payment ON payment.person_id = people.id").group(group_conditions).where(date_conditions("people.created_on")).order('date DESC, signups DESC').map {|row|
          row[:conversion_rate] = row[:conversions].to_i.percent_of_s(row[:signups].to_i)
          row[:per_person_revenue] = ( row[:revenue].to_f / row[:conversions].to_f ).as_dollars
          row[:revenue] = row[:revenue].to_f.as_dollars
          row
        }


        result_rows.group_by {|row|
          row[:date]
        }.sort {|a,b| b <=> a }.each {|date,rows|
          self.rows << {"date" => "#{date}"}
          rows.each {|r| r["date"] = nil;  self.rows << r}
        }

      end

    end
  end
end
