module Tunecore
  module Reports
    class Base
      extend MoneyField

      #     Add new report types to the following array
      #     Each of these has a matching class derived from this class (ie Reports::SomeReport < Reports::Base)
      #
      def self.available_reports
        %w{ signups albums_added
            invoice_settlements incomplete_albums new_album_titles example_report
            customers balance_statements}
      end

      #  The parameters method is overridden in derived report classes which take different input parameters
      #
      def self.parameters
        [:interval, :start_date, :end_date]
      end

      #  By default each column will be rendered as plain text, however an optional row partial can be created
      #    to customize the display of report rows (add links, bold text in certain columns, etc.)
      #  The row partials match the name of the report class. They are in app/views/admin/reports/row_partials
      def self.row_partial
        @row_partial ||= self.check_row_partial
        return @row_partial != "" ? @row_partial : nil
      end

      def self.check_row_partial
        partial = "admin/reports/row_partials/_#{self.name.demodulize.underscore}.rhtml"
        filename = File.join(Rails.root,"app/views/",partial)
        File.exists?(filename) ?  "admin/reports/row_partials/#{self.name.demodulize.underscore}" : ""
      end


      #     Add new report parameter types to the following array.
      #     Each of these has a matching partial called _<parameter-name>_selections.rhtml in app/views/admin/reports
      #
      REPORT_PARAMS = [
                       :interval, # time period by which results will be grouped (day, week, month, year, all)
                       :start_date,
                       :end_date,
                       :genre
                      ].freeze unless defined? REPORT_PARAMS

      # These params help out with little implementation details
      MINOR_REPORT_PARAMS = [
                             :use_end_date,
                             :genre_id
                            ].freeze unless defined? MINOR_REPORT_PARAMS
      ALL_REPORT_PARAMS = REPORT_PARAMS + MINOR_REPORT_PARAMS unless defined? ALL_REPORT_PARAMS

      protected
      attr_writer :name, :header, :rows
      attr_accessor :result_sets
      ALL_REPORT_PARAMS.each {|x| attr_writer(x) }

      public
      attr_reader :name, :header, :rows
      ALL_REPORT_PARAMS.each {|x| attr_reader(x) }

      def self.report_class(name)
        Reports.const_get(name.to_s.camelize.to_sym)
      end

      def self.available_report_classes
        self.available_reports.map {|x| self.report_class(x)}
      end

      def self.all_available_parameters
        REPORT_PARAMS
      end

      #currently all available parameters are included on the page and the inapplicable ones are hidden via javascript
      def self.hidden_parameters
        self.all_available_parameters - self.parameters
      end

      def self.generate(opts={})
        raise "Missing report type name" unless opts["name"]
        report = self.report_class(opts["name"]).new(opts)
        report.calculate!
        report
      end

      def initialize(opts={})
        self.interval = opts["interval"]
        if opts["start_date(1i)"]
          self.start_date = Time.local(opts["start_date(1i)"], opts["start_date(2i)"], opts["start_date(3i)"])
        elsif opts["start_date"]
          self.start_date = opts["start_date"]
        end
        if opts["end_date(1i)"]
          self.end_date = Time.local(opts["end_date(1i)"], opts["end_date(2i)"], opts["end_date(3i)"])
        elsif opts["end_date"]
          self.end_date = opts["end_date"]
        end
        if opts["use_end_date"] and '1' == opts["use_end_date"]
          self.use_end_date = true
        else
          self.use_end_date = false
        end
        self.rows = []
      end

      def date_conditions(fieldname)
        if self.start_date and self.end_date and self.use_end_date
          ["#{fieldname} between ? and ?", self.start_date.to_date, self.end_date.to_date + 1 ]
        elsif self.start_date
          ["#{fieldname} >= ?", self.start_date.to_date]
        else
          nil
        end
      end

      def date_conditions_string(fieldname, concat="")
        result = if self.start_date and self.end_date and self.use_end_date
                   "#{fieldname} between '#{self.start_date.to_date.to_s(:db)}' and '#{(self.end_date.to_date + 1).to_s(:db)}'"
                 elsif self.start_date
                   "#{fieldname} >= '#{self.start_date.to_date.to_s(:db)}'"
                 else
                   nil
                 end
        return '' unless result
        result + ' ' + concat
      end

      def self.group_expression(val, fieldname)
        case val
        when 'day'
          "DATE(#{fieldname})"
        when 'week'
          "DATE(DATE_SUB(#{fieldname}, INTERVAL (DAYOFWEEK(#{fieldname}) - 1) DAY))"
          ### I think this would be quicker but it is not readable for display: "DATE_FORMAT(#{fieldname}, '%Y-%u')"
        when 'month'
          "DATE_FORMAT(#{fieldname}, '%Y-%m')"
        when 'year'
          "YEAR(#{fieldname})"
        when 'all'
          nil
        end
      end

      def self.group_expression_string(val, fieldname, concat="")
        return '' unless result = self.group_expression(val, fieldname)
        result + ' ' + concat
      end

      def self.row_is_section_break?(row)
        if row.is_a?(Hash)
          row.has_key?("section_break")
        else
          !row["section_break"].nil?
        end
      end

      def name
        self.class.name.demodulize.underscore
      end

      def title
        if 'all' == self.interval
          "All " + self.class.name.demodulize.titleize + " starting " + I18n.l(start_date, format: :slash_short_year)
        else
          self.class.name.demodulize.titleize + " by " + self.interval + " starting " + I18n.l(start_date, format: :slash_short_year)
        end
      end

      def self.display_name
        self.name.demodulize.titleize
      end

      def self.doc
        "TODO: Write documentation for #{self.display_name}"
      end

      def calculated?
        rows.any?
      end

      def to_csv
        csv = self.header.map {|header| header.titleize }.join(",") + "\n"
        csv << self.rows.map {|row| self.header.collect {|header| row[header] ? row[header].to_s.gsub(',','') : '' }.join(",") }.join("\n")
        csv
      end

    end
  end
end
