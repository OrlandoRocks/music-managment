module Tunecore
  module Reports
    module PurchaseReportDataFields   
      def total_purchased
        attributes['total_purchased'].to_i
      end
      
      def total_cost
        attributes['total_cost'].to_f
      end
      
      def total_discounts
        attributes['total_discounts'].to_i
      end
      
      def average_cost
        attributes['average_cost'].to_f
      end
      
      def average_discount
        attributes['average_discount'].to_f
      end
      
      def average_paid
        attributes['average_paid'].to_f
      end
      
      def average_transaction
        attributes['average_transaction'].to_f
      end
      
      def resolution
        attributes['resolution'] || "none"
      end
      
      def cost_after_discount
        attributes['cost_after_discount'].to_f
      end
      
      def new_customers
        attributes['new_customers'].to_i     
      end
      
      def returning_customers
        attributes['returning_customers'].to_i
      end
      
      def total_customers
        attributes['total_customers'].to_i
      end
    end
  end
end