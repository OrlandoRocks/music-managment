module Tunecore
  module Reports
    module SalesReportFilter

      # Placeholder conditions are used in order to allow filtering to be more flexible.
      # - condition_keys is the array that has the fields we want to filter by
      # - condition_values is the hash that holds the values for those fields
      # We have an options hash which is used to pass in the filtering parameters.

      def build_conditions_array(person, options = {})
        start_date = options[:start_date]
        end_date   = options[:end_date]
        releases   = options[:releases]
        release_type = options[:release_types]
        stores     = options[:stores]
        countries  = options[:countries]

        query_modifier_conditions = options[:query_modifier_conditions]
        query_modifier_values     = options[:query_modifier_values]

        @condition_keys = ["sr.person_id = :person_id"]
        @condition_values = {:person_id => person.id}

        assign_start_and_end_date(start_date, end_date)

        # Filter by release
        assign_releases(releases)

        # Filter by release type (Album/Single/Ringtone/Video)
        assign_release_types(release_type)

        # Filter by store
        assign_stores(stores)

        # Filter by country
        assign_countries(countries)

        assign_query_modifier_conditions(query_modifier_conditions)
        assign_query_modifier_values(query_modifier_values)

        [@condition_keys.join(" and "), @condition_values]
      end

      def assign_start_and_end_date(start_date, end_date)
        if start_date && end_date
          @condition_keys << ["srm.period_sort BETWEEN :start_date AND LAST_DAY(:end_date)"]
          @condition_values.update(:start_date => start_date, :end_date => end_date)
        end
      end

      def assign_stores(stores)
        if stores
          @condition_keys << "s_stores.display_group_id IN (:store_ids)"
          @condition_values.update(:store_ids => stores)
        end
      end

      def assign_countries(countries)
        if countries
          @condition_keys << "srm.country IN (:countries)"
          @condition_values.update(:countries => countries)
        end
      end

      def assign_release_types(release_type)
        if release_type
          release_type_conditions= []

          if release_type.include?("album")
            release_type_conditions << "sr.release_type = 'Album'"
          end

          if release_type.include?("single")
            release_type_conditions << "sr.release_type = 'Single'"
          end

          if release_type.include?("ringtone")
            release_type_conditions << "sr.release_type = 'Ringtone'"
          end

          if release_type.include?("video")
            release_type_conditions << "sr.release_type = 'Video'"
          end

          @condition_keys << "(#{release_type_conditions.join(" OR ")})"
        end
      end

      def assign_releases(releases)
        if releases
          release_conditions = []

          if releases[:albums]
            release_conditions << ["(sr.release_id IN (:album_releases) AND sr.release_type='Album')"]
            @condition_values.update(:album_releases => releases[:albums])
          end

          if releases[:singles]
            release_conditions << ["sr.release_id IN (:single_releases) AND release_type='Single'"]
            @condition_values.update(:single_releases => releases[:singles])
          end

          if releases[:ringtones]
            release_conditions << ["sr.release_id IN (:ringtone_releases) AND release_type='Ringtone'"]
            @condition_values.update(:ringtone_releases => releases[:ringtones])
          end

          if releases[:videos]
            release_conditions << ["sr.related_id IN (:video_releases) AND sr.related_type='Video'"]
            @condition_values.update(:video_releases => releases[:videos])
          end

          @condition_keys << "(#{release_conditions.join(" OR ")})"
        end
      end

      def assign_query_modifier_conditions(query_modifier_conditions)
        if query_modifier_conditions
          @condition_keys << query_modifier_conditions
        end
      end

      def assign_query_modifier_values(query_modifier_values)
        if query_modifier_values
          @condition_values.update(query_modifier_values)
        end
      end

    end
  end
end
