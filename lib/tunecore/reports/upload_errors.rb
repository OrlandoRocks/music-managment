module Tunecore
  module Reports
    class UploadErrors < DataReport

      def data(back = 6)
        results = []
        0.upto(back) do |x|
          results << data_records.where(resolution_identifier: resolution_identifier(:week, (Date.today - (7 * x))).to_s)
        end

        results.compact
      end



      def record_week(date)
        data = []

        #select the distribution of the file upload erros uploaded for the given week.
        u = Song.where('created_on >= ? AND created_on <= ? AND last_errors IS NOT null AND last_errors NOT LIKE "% it should be%"', date_range_week(date)[0], date_range_week(date)[1]).select('last_errors, count(last_errors) as number').group('last_errors')

        u.each do |datum|
          data << [datum.number, "#{datum.last_errors}"]
        end

        # run this query again to get the bit rate erros
        u = Song.where('created_on >= ? AND created_on <= ? AND last_errors IS NOT null AND last_errors LIKE "% it should be%"', date_range_week(date)[0], date_range_week(date)[1]).select('last_errors, count(last_errors) as number').group('last_errors')

        data << [u.size, "low bitrate errors"]

        create_record(:week, date, data)
      end

    end
  end
end