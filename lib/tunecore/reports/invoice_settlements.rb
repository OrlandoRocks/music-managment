module Tunecore
  module Reports
    class InvoiceSettlements < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date invoices settlements payment_rate revenue average_sale std_dev largest distinct_prices avg_album_count max_album_count }
        super
      end

      def self.doc
        <<-EOSTRING
          Invoices created over a given date range. Invoices show the total number of invoices created, Settlements show the number of these that were paid (not all are). Payment rate gives the percentage of new invoices that have been paid. Revenue shows the total revenue from all paid invoices. Average sale, standard deviation, and largest amount give statistics on the paid invoices. Distinct prices tells how many distinct invoice amounts there were. Avg album count tells how many albums each invoices had on average. Max album count holds the largest number of albums on a single invoice.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "invoices.created_at")
        group_conditions = group_exp

        select_query = <<-EOSTRING
                      #{group_exp || "'ALL'"} as date,
                      count(*) as invoices,
                      count(settled_at) as settlements,
                      sum(final_settlement_amount_cents) / 100 as revenue,
                      avg(final_settlement_amount_cents) / 100 as average_sale,
                      stddev(final_settlement_amount_cents) / 100 as std_dev,
                      max(final_settlement_amount_cents) / 100 as largest,
                      count(distinct(final_settlement_amount_cents)) as distinct_prices,
                      avg(item_count) as avg_album_count,
                      max(item_count) as max_album_count
        EOSTRING


        self.rows = Invoice.select(select_query).joins("INNER JOIN (SELECT invoice_id, count(*) as item_count FROM purchases GROUP BY invoice_id) item ON item.invoice_id = invoices.id").group(group_conditions).where(date_conditions("invoices.created_at")).order('date DESC'
        ).map {|row|
          report = {}
          report['payment_rate'] = row[:settlements].to_i.percent_of_s(row[:invoices].to_i)
          report['revenue'] = row[:revenue].to_f.as_dollars
          report['average_sale'] = row[:average_sale].to_f.as_dollars
          report['std_dev'] = row[:std_dev].to_f.as_dollars
          report['largest'] = row[:largest].to_f.as_dollars
          report['date'] = row[:date]
          report['invoices'] = row[:invoices]
          report['settlements'] = row[:settlements]
          report['distinct_prices'] = row[:distinct_prices]
          report['avg_album_count'] = row[:avg_album_count]
          report['max_album_count'] = row[:max_album_count]
          report
        }

      end

    end
  end
end
