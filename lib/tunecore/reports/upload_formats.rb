module Tunecore
  module Reports
    class UploadFormats < DataReport

      def data(back = 6)
        results = []
        0.upto(back) do |x|
          results << data_records.where(resolution_identifier: resolution_identifier(:week, (Date.today - (7 * x))).to_s)
        end

        results.compact
      end



      def record_week(date)
        #select the distribution of the file types uploaded for the given week.
        u = Upload.where('created_at >= ? AND created_at <= ?', date_range_week(date)[0], date_range_week(date)[1]).select('filetype, count(filetype) as number').group('filetype')

        data = []
        u.each do |datum|
          data << [datum.number, "#{datum.filetype}"]
        end

        create_record(:week, date, data)
      end

    end
  end
end