module Tunecore
  module Reports
    module YouTubeRoyalties
      class DetailReport
        include YouTubeRoyaltyReport

        FIELDS = [
          :sales_period_start,
          :total_views,
          :you_tube_policy_type,
          :gross_revenue,
          :exchange_rate,
          :tunecore_commision,
          :net_revenue,
          :net_revenue_currency,
          :you_tube_video_id,
          :song_name,
          :album_name,
          :label_name,
          :artist_name
        ].freeze

        HEADERS = [
          "Sales Period",
          "Total Views",
          "Policy",
          "Gross Revenue (USD)",
          "Exchange Rate",
          "Tunecore Commision",
          "Net Revenue",
          "Net Revenue Currency",
          "YouTube Video Link",
          "Song Name",
          "Album Name",
          "Label Name",
          "Artist Name"
        ].freeze

        # TODO: Update this to use <<~ once we're on Ruby >=2.3
        QUERY = <<-SQL.strip_heredoc.freeze
          SELECT sales_period_start,
                 total_views,
                 you_tube_policy_type,
                 gross_revenue,
                 exchange_rate,
                 tunecore_commision,
                 net_revenue,
                 net_revenue_currency,
                 you_tube_video_id,
                 song_name,
                 albums.name as album_name,
                 label_name,
                 artist_name
          FROM you_tube_royalty_records
          INNER JOIN songs ON songs.id = you_tube_royalty_records.song_id
          INNER JOIN albums ON albums.id = songs.album_id
          WHERE you_tube_royalty_records.person_id = :person_id
            AND you_tube_royalty_records.sales_period_start = :sales_period
          ORDER BY you_tube_royalty_records.id asc
        SQL

        #
        # Returns csv row enumerated backed by a streaming db connection
        #
        def streamable_csv
          Enumerator.new do |y|
            y << CSV::Row.new(FIELDS, HEADERS, true).to_s

            # Get the mysql2 client
            client = SalesRecord.connection.raw_connection

            sql = ActiveRecord::Base.send(
              :sanitize_sql_array,
              [QUERY, person_id: @person.id, sales_period: @start_date]
            )

            # Query with the stream option - 1 query but not pulled all into ruby/ram at once
            result = client.query(sql, stream: true, cache_rows: false)

            result.each(as: :hash) do |record|
              data = {}
              FIELDS.each do |field|
                data[field] = if field == :you_tube_video_id
                                "http://www.youtube.com/watch?v=#{record[field.to_s]}"
                              else
                                record[field.to_s]
                              end
              end
              y << CSV::Row.new(data.keys, data.values).to_s
            end
          end
        end
      end
    end
  end
end
