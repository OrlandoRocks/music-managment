module Tunecore
  module Reports
    module YouTubeRoyalties
      class ArtistReport
        include YouTubeRoyaltyReport

        SELECT = %{artist_name,
                  count(distinct you_tube_royalty_records.you_tube_video_id) as total_videos,
                  count(distinct if(you_tube_royalty_records.net_revenue > 0, you_tube_royalty_records.you_tube_video_id, NULL )) as monetizable_videos,
                  sum(you_tube_royalty_records.total_views) as total_views,
                  sum(if(you_tube_royalty_records.net_revenue > 0, you_tube_royalty_records.total_views, 0 )) as monetizable_views,
                  sum(you_tube_royalty_records.net_revenue) as net_revenue,
                  you_tube_royalty_records.net_revenue_currency as currency}.freeze
        JOINS = %{
          INNER JOIN songs on songs.id   = you_tube_royalty_records.song_id
          INNER JOIN albums on albums.id = songs.album_id
        }.freeze
        GROUP = "artist_name".freeze

        #
        # Returns royalties aggreged by album id
        #
        def report
          to_report(SELECT, JOINS, GROUP)
        end

        def to_csv
          csv = CSV.new(response = "", :row_sep => "\r\n")
          set_csv_titles(csv)
          report.first.each do |r|
            csv << [r.artist_name,
                    r.total_videos,
                    r.monetizable_videos,
                    r.total_views,
                    r.monetizable_views,
                    r.net_revenue]
          end
          response
        end

        def set_csv_titles(csv)
          csv << [I18n.t(:artist_name), I18n.t("you_tube_royalties.total_videos"), I18n.t("you_tube_royalties.monetized_videos"), I18n.t("you_tube_royalties.individual_release_report.total_views"), I18n.t("you_tube_royalties.monetized_views"), I18n.t("you_tube_royalties.net_revenue")]
        end
      end
    end
  end
end
