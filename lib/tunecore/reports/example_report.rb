module Tunecore
  module Reports
    class ExampleReport < Tunecore::Reports::Base

      # This class is an example of how to make a report using the Reports::Base framework

      # First, you need to specify what parameters the report will take.
      # Each of these parameters will appear in a web form when the user runs the report, our report will take a start date and an end date
      def self.parameters
        [:start_date, :end_date]
      end

      # We set the names of the columns that will be displayed in the initialize method
      def initialize(opts={})
        self.header = %w{ created_on name artist_name }
        super
      end

      # The doc method explains what the report does. It is a class method.
      def self.doc
        "This report lists all the albums created in a certain time period."
      end

      # We could override the title method to give a custom title, but by default it will use the class name and that is good enough.
      #def title
      # "my custom title"
      #end

      # The calculate! method actually does the work.
      # Results are returned by adding rows to the self.rows array, each row is a hash with keys that match the column names.
      def calculate!

        #end date is optional
        effective_end_date = self.use_end_date ? self.end_date : Time.now

        albums = Album.includes(creatives: :artist).where(created_on: self.start_date..effective_end_date).not_deleted.order(:name)

        #format results into rows
        self.rows = albums.map do |album|
          {
            created_on: album.created_on,
            name: album.name,
            artist_name: album.artist_name
          }.with_indifferent_access
        end
      end

      #
      #
      # The rows contents will be displayed automatically as plain text, however you can specify a row partial to customize the display
      # Open app/views/admin/reports/row_partials/_example_report.rhtml to see this report's row partial
      #
      # Also, there is one more thing that you have to do, add this report class to the available reports list in Reports::Base !!!
      #
      #

    end
  end
end
