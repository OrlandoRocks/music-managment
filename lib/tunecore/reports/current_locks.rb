module Tunecore
  module Reports
    class CurrentLocks
      def self.by(what)
        results = case what
        when Person
          locks_by_person(what)
        when :people
          locks_by_people
        when :invoices
          locks_by_invoices
        when :bundles
          locks_by_bundles
        else
          raise ArgumentError, "unknown argument to by: #{what.inspect}"
        end

        results
      end

      def self.locks_by_person(person)
        Album.includes(:locked_by).where(locked_by_type: 'Person', locked_by_id: person.id)
      end

      def self.locks_by_people
        Album.includes(:locked_by).where(locked_by_type: 'Person')
      end

      def self.locks_by_invoices
        Album.includes(:locked_by).where(locked_by_type: 'Invoice')
      end

      def self.locks_by_bundles
        Album.includes(:locked_by).where(locked_by_type: 'PetriBundle')
      end

      private_class_method :locks_by_person,
        :locks_by_people,
        :locks_by_invoices,
        :locks_by_bundles
    end
  end
end
