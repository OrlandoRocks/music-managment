module Tunecore
  module Reports
    class Customers < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date customers new percent_new returning percent_returning revenue new_customer_revenue percent_new_customer_revenue returning_customer_revenue percent_returning_customer_revenue returning_customer_previous_revenue }
        super
      end

      def self.doc
        <<-EOSTRING
            Shows new vs. returning customers. Takes all customers who paid invoices over the period and determines which were new and which had previously paid an invoice. The revenue from each of these groups is totalled. "Returning Customer Previous Revenue" gives the total amount that the returning customers spent previously at Tunecore.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "invoices.created_at")
        group_conditions = group_exp
        conditions = date_conditions("invoices.created_at")
        if conditions.is_a?(Array)
          conditions[0] = conditions[0] + " AND invoices.settled_at is not null"
        else
          conditions << " AND invoices.settled_at is not null"
        end

        select_query = <<-EOSTRING
                      invoices.person_id,
                      #{group_exp || "'ALL'"} as date,
                      count(*) as customers,
                      sum(final_settlement_amount_cents) / 100 as revenue,
                      sum(if(previous.returning_customer,final_settlement_amount_cents,0) / 100) as returning_customer_revenue,
                      sum(previous.revenue) as returning_customer_previous_revenue,
                      sum(previous.returning_customer) as returning
        EOSTRING

        self.rows = Invoice.select(select_query).group(group_conditions).joins("LEFT JOIN (SELECT people.id, invoices.person_id, sum(final_settlement_amount_cents) / 100 as revenue, count(*) as previous_invoice_count, 1 as returning_customer FROM invoices, people where people.id = invoices.person_id AND settled_at is not null AND invoices.created_at < '#{self.start_date.to_s :db}' GROUP BY person_id) previous on previous.person_id = invoices.person_id").where(conditions).order('date DESC').map {|row|
          row[:new] = row[:customers].to_i - row[:returning].to_i
          row[:percent_new] = row[:new].to_i.percent_of_s(row[:customers].to_i)
          row[:percent_returning] = row[:returning].to_i.percent_of_s(row[:customers].to_i)

          revenue = row[:revenue].to_f
          returning_customer_revenue = row[:returning_customer_revenue].to_f
          new_customer_revenue = revenue - returning_customer_revenue

          row[:revenue] = revenue.as_dollars
          row[:returning_customer_revenue] = returning_customer_revenue.as_dollars
          row[:new_customer_revenue] =  new_customer_revenue.as_dollars

          row[:percent_new_customer_revenue] = new_customer_revenue.percent_of_s(revenue)
          row[:percent_returning_customer_revenue] = returning_customer_revenue.percent_of_s(revenue)

          returning_customer_previous_revenue = row[:returning_customer_previous_revenue].to_f
          row[:returning_customer_previous_revenue] = returning_customer_previous_revenue.as_dollars

          row
        }

      end

    end
  end
end
