# 2009-11-30 -- MJL -- Report that gives us the number of isrcs created for the year.  Currently we can only handle about 3.5
#                      million songs being created before the system that generates tunecore_isrcs will wrap around and give us
#                      duplicate isrcs...  This report will help warn us of impending disasters.

module Tunecore
  module Reports
    module Operations
      class IsrcCount < DataReport
      
        
        # TODOMJL -- Move this initialize into the data report base.
        def initialize(options={})
          super
          self.name = self.class.name.split("::").last
          self.resolution = [:year]
          self.start_on = Date.today << 1 #Go back one month
        end


        def build_records(resolution, start_date, stop_date)
          number_of_songs = Song.where('created_on BETWEEN ? and ?', start_date, stop_date).count
          data = [[number_of_songs, 'song count']]
          create_record(resolution, start_date, data)
        end
        
        def data
          super(:year, 1)
        end
        
      end
    end
  end
end
