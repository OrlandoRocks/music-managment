module Tunecore
  module Reports
    class AnnualRenewals < DataReport
      def data(back = 20)
        results = []
        0.upto(back) do |x|
          record_set = data_records.where(resolution_identifier: Utilities::DateRange.new(:month, (Date.today >> 15) << x).identifier.to_s)
          results << [record_set.detect{|x| x.description == 'total albums due'}, record_set.detect{|x| x.description == 'Annual Renewals Paid'}, record_set.detect{|x| x.description == 'total albums due'}, record_set.detect{|x| x.description == 'total taken down'}, record_set.detect{|x| x.description == 'renewal rate'}, record_set.detect{|x| x.description == 'Total Revenue'}, record_set.detect{|x| x.description == 'total projected revenues'}] if record_set.first != nil
        end

        results.compact
      end




      def build_records(resolution, start_date, stop_date)
        annual_renewals_unpaid = Album.where("annual_renewals.created_on >= ? AND annual_renewals.created_on <= ? AND annual_renewals.promotion_identifier = 'Annual Renewal' AND annual_renewals.finalized_at is null", start_date, stop_date).joins('left join annual_renewals on albums.id = annual_renewals.album_id').select('albums.id, albums.price_policy_id, albums.person_id').includes('person')

        annual_renewals_paid = Album.where("annual_renewals.created_on >= ? AND annual_renewals.created_on <= ? AND annual_renewals.promotion_identifier = 'Annual Renewal' AND annual_renewals.finalized_at is not null", start_date, stop_date).joins('left join annual_renewals on albums.id = annual_renewals.album_id').select('albums.id, albums.price_policy_id, albums.purchase_id')

        albums_taken_down = Album.where("renewal_due_on >= ? AND renewal_due_on <= ? AND takedown_at is not null", start_date, stop_date)

        early_renewals_created_and_paid = AnnualRenewal.where("created_on >= ? AND created_on < ? AND promotion_identifier = 'Early Annual Renewal 1' AND finalized_at is not null", start_date, stop_date)



        total_albums_paid = annual_renewals_paid.size

        revenue_798 = 0
        revenue_998 = 0
        revenue_999 = 0
        revenue_1998 = 0
        revenue_misc = 0
        total_from_paypal = 0
        total_from_other = 0
        annual_renewals_paid.each do |album|
          case renewal_cost_cents(album)
          when 798    then revenue_798 += 1
          when 998    then revenue_998 += 1
          when 999    then revenue_999 += 1
          when 1998   then revenue_1998 += 1
          else
            raise  "I don't think we should be here: costs == #{renewal_cost_cents(album)}"
            revenue_misc += renewal_cost_cents(album)
          end

          #add money to ipn or other
          begin
            if album.annual_renewals.first.purchase.invoice.invoice_settlements.detect{|x| x.source_type == 'PaypalIpn'}
              total_from_paypal += renewal_cost_cents(album)
            else
              total_from_other += renewal_cost_cents(album)
            end
          rescue
            total_from_other += renewal_cost_cents(album)
            puts "This albums (#{album.id}) annual renewal (ar##{album.annual_renewals.first.id}) doesn't seem to have an invoice or purchase"
          end
        end
        total_revenue = revenue_798 * 798 + revenue_998 * 998 + revenue_999 * 999 + revenue_1998 * 1998



        taken_down = albums_taken_down.size

        lost_revenue_798 = 0
        lost_revenue_998 = 0
        lost_revenue_999 = 0
        lost_revenue_1998 = 0
        lost_revenue_misc = 0
        albums_taken_down.each do |album|
          case renewal_cost_cents(album)
          when 798    then lost_revenue_798 += 1
          when 998    then lost_revenue_998 += 1
          when 999    then lost_revenue_999 += 1
          when 1998   then lost_revenue_1998 += 1
          else
            raise  "I don't think we should be here: costs == #{renewal_cost_cents(album)}"
            lost_revenue_misc += renewal_cost_cents(album)
          end
        end
        lost_revenue = lost_revenue_798 * 798 + lost_revenue_998 * 998 + lost_revenue_999 * 999 + lost_revenue_1998 * 1998




        still_unpaid = annual_renewals_unpaid.size
        open_amount_798 = 0
        open_amount_998 = 0
        open_amount_999 = 0
        open_amount_1998 = 0
        open_amount_misc = 0
        open_covered_by_balances = 0
        annual_renewals_unpaid.each do |album|
          case renewal_cost_cents(album)
          when 798    then open_amount_798 += 1
          when 998    then open_amount_998 += 1
          when 999    then open_amount_999 += 1
          when 1998   then open_amount_1998 += 1
          else
            raise  "I don't think we should be here: costs == #{renewal_cost_cents(album)}"
            open_amount_misc += renewal_cost_cents(album)
          end
          #begin
            if album.person.person_balance && album.person.person_balance.balance > Tunecore::Numbers.cents_to_decimal(renewal_cost_cents(album))
              open_covered_by_balances += renewal_cost_cents(album)
            end
          #rescue
          #  puts "weird album without a person...."
          #end
        end
        open_amount_total = open_amount_798 * 798 + open_amount_998 * 998 + open_amount_999 * 999 + open_amount_1998 * 1998


        # projected_due (are all of the albums with renewal_due on in the time frame, but without an annual renewal object.)
        projected_albums_due = Album.where("albums.renewal_due_on >= ? AND albums.renewal_due_on <= ? AND albums.takedown_at is NULL AND albums.renewal_due_on > ?", start_date, stop_date, stop_date).select('albums.id, albums.price_policy_id')


        projected_total_count = projected_albums_due.size
        projected_amount_798 = 0
        projected_amount_998 = 0
        projected_amount_999 = 0
        projected_amount_1998 = 0
        projected_amount_misc = 0
        projected_albums_due.each do |album|
          case renewal_cost_cents(album)
          when 798    then projected_amount_798 += 1
          when 998    then projected_amount_998 += 1
          when 999    then projected_amount_999 += 1
          when 1998   then projected_amount_1998 += 1
          else
            raise  "I don't think we should be here: costs == #{renewal_cost_cents(album)}"
            projected_amount_misc += renewal_cost_cents(album)
          end
        end
        projected_amount_total = projected_amount_798 * 798 + projected_amount_998 * 998 + projected_amount_999 * 999 + projected_amount_1998 * 1998 + projected_amount_misc

        total_albums_due = total_albums_paid + still_unpaid + taken_down + projected_total_count# the number of renewals paid, # of renewals not paid, # of takedowns with a renewal_due_on date of this month, # of albums with a renewal due on in this period
        total_amount_due = 0 #this is a different number Projected total

        if total_albums_due > 0
          takedown_rate = (taken_down.to_f / total_albums_due.to_f) * 100
          unpaid_rate = (still_unpaid.to_f / total_albums_due.to_f) * 100
          renewal_rate = (total_albums_paid.to_f / total_albums_due.to_f) * 100
          retention_rate = 0 #taken down/total
        else
          takedown_rate = 0
          unpaid_rate = 0
          renewal_rate = 0
          retention_rate = 0
        end


        create_record(resolution, start_date, [
                                [total_revenue.to_f / 100.to_f, 'Total Revenue'],
                                [total_albums_paid, 'Annual Renewals Paid'],
                                [renewal_rate, 'renewal rate'],
                                [revenue_798, '$7.98 Revenue'],
                                [revenue_998, '$9.98 Revenue'],
                                [revenue_999, '$9.99 Single Revenue'],
                                [revenue_1998, '$19.98 Revenue'],
                                [total_from_paypal.to_f / 100.to_f, 'paypal revenue'],
                                [total_from_other.to_f / 100.to_f, 'tunecore revenue'],

                                [taken_down, 'total taken down'],
                                [takedown_rate, 'take down rate'],
                                [lost_revenue.to_f / 100.to_f, 'total lost rev'],
                                [lost_revenue_798, 'lost rev from 7.98'],
                                [lost_revenue_998, 'lost rev from 9.98'],
                                [lost_revenue_999, 'lost rev from 9.99'],
                                [lost_revenue_1998, 'lost rev from 19.98'],

                                [still_unpaid, 'renewals outstanding'],
                                [unpaid_rate, 'unpaid rate'],
                                [open_amount_total.to_f / 100.to_f, 'total revenue due'],
                                [open_covered_by_balances.to_f / 100.to_f, 'outstanding covered by TC balances'],
                                [open_amount_798, '7.98 revenue due'],
                                [open_amount_998, '9.98 revenue due'],
                                [open_amount_999, '9.99 revenue due'],
                                [open_amount_1998, '19.98 revenue due'],


                                [projected_total_count, 'albums that will be due'],
                                [projected_amount_total.to_f / 100.to_f, 'total projected revenues'],
                                [projected_amount_798, '7.98 futures'],
                                [projected_amount_998, '9.98 futures'],
                                [projected_amount_999, '9.99 futures'],
                                [projected_amount_1998, '19.98 futures'],
                                [total_albums_due, 'total albums due']
                                ])



      end

      private

      def renewal_cost_cents(album)
        #this will unnecessarily hit the db, we can create an array of all the album price policies

        album.price_policy.base_price_cents

      end



    end
  end
end
