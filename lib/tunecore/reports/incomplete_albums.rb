module Tunecore
  module Reports
    class IncompleteAlbums < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date albums incomplete incomplete_rate bad_songs bad_songs_rate }
        super
      end

      def self.doc
        <<-EOSTRING
           Shows information about albums created in a certain date range which have not been paid for. These include abandoned albums and those where the user had trouble on their end trying to upload them. The one error that we have access to is whether they tried to upload an incorrect file as a song.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "albums.created_on")
        group_conditions = [group_exp].compact.join(", ")

        self.rows = Album.select("#{group_exp || "'ALL'"} as date, count(*) as albums, sum(if(payment_applied,0,1)) as incomplete, sum(has_bad_songs) as bad_songs").joins("LEFT JOIN (SELECT album_id, if(count(*)>0,1,0) as has_bad_songs FROM songs WHERE last_errors is not null GROUP BY songs.album_id) songs ON songs.album_id = albums.id").group(group_conditions).where(date_conditions("albums.created_on")
        ).map {|row|
          report = {}
          report['incomplete_rate'] = row[:incomplete].to_i.percent_of_s(row[:albums].to_i)
          report['bad_songs_rate'] = row[:bad_songs].to_i.percent_of_s(row[:albums].to_i)
          report['date'] = row.date
          report['albums'] = row.albums
          report['incomplete'] = row.incomplete
          report['bad_songs'] = row.bad_songs
          report
        }

      end

    end
  end
end
