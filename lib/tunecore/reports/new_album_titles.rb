module Tunecore
  module Reports
    class NewAlbumTitles < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date account artist album paid }
        super
      end

      def self.doc
        <<-EOSTRING
           Shows new albums entered into the system. This report was intended to aid in detecting fraudulently uploaded material which does not belong to the uploader, such as uploading Michael Jackson's "Thriller" to Tunecore.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "albums.finalized_at")
        group_conditions = [group_exp].compact.join(", ")

        result_rows = Album.select("#{group_exp || "'ALL'"} as date, people.id as person_id, people.name as account, if(payment_applied=1,'$','!') as paid, album_state, artists.name as artist, albums.name as album, albums.id as album_id").joins("LEFT JOIN (artists, people, creatives) ON (artists.id = creatives.artist_id AND people.id = albums.person_id AND creatives.creativeable_id = albums.id AND creatives.creativeable_type = 'Album' )").where(date_conditions("albums.finalized_at"))

        result_rows.group_by {|row|
          row[:date]
        }.sort {|a,b| b <=> a }.each {|date,rows|
          self.rows << {"date" => "#{date}"}
          rows.each {|r| r["date"] = nil;  self.rows << r}
        }


      end

    end
  end
end
