module Tunecore
  module Reports
    class RenewalsDueByPerson < Tunecore::Reports::Base

      def self.parameters
        [:start_date, :end_date]
      end

      def initialize(opts={})
        self.header = %w{id name num_of_albums_due num_of_albums_paid total_due total_received conversion_rate }
        super
      end

      def self.doc
        <<-EOSTRING
          People that have albums overdue
        EOSTRING
      end

      def title
        "Renewals Due By Person between #{@start_date.strftime("%Y-%m-%d")} and #{@end_date.strftime("%Y-%m-%d")}"
      end

      def calculate!
        @start_date = self.start_date
        @end_date = self.end_date
        #find all people that have an album renewal, paid or unpaid
        puts "Finding all people with album renewal purchases"
        people_with_purchases = Person.includes(:purchases).where("purchases.sti_type = 'AnnualRenewalPurchase' and purchases.created_at between ? and ?", @start_date, @end_date)

        all_albums_to_renew = Album.includes(:annual_renewal_events).where("annual_renewal_events.event_type = 'BeginSubscription' and annual_renewal_events.occurred_on between ? and ?", @start_date - 12.months, @end_date-12.months)

        #all_albums_to_renew = AnnualRenewalEventFactory.live_albums.select do |album|
        #  (album.annual_renewal_beginning + 12.months) > @start_date && (album.annual_renewal_beginning + 12.months) < @end_date
        #end

        people_to_renew = all_albums_to_renew.collect{|a| a.person}
        people_to_renew.uniq!

        people = people_with_purchases + people_to_renew
        puts "************************************* Finding uniq people ***************************************************"
        people.uniq!

        #build the row
        sum_albums_due = 0
        sum_albums_paid = 0
        sum_cents_due = 0
        sum_cents_paid = 0
        puts "******************************************* Building rows... ***********************************************"
        people.each do |person|
          # find all purchases for this person between the given dates.  We don't have to look at
          # people that aren't already in marked as having a purchase that matches our criteria.
          purchases = []
          if people_with_purchases.include?(person)
            puts "***************************** Loading purchases for #{person.id} *****************************************"
            purchases = Purchase.where("person_id = ? and sti_type = 'AnnualRenewalPurchase' and created_at between ? and ?" , person.id, @start_date, @end_date )
          end

          #find albums that will renew during the time period
          this_person_albums_to_renew = all_albums_to_renew.select{|a| a.person_id == person.id}

          #find purchases that HAVEN'T been paid
          purchases_due = purchases.select{|p| p.paid_at.nil? }
          #find purchases that HAVE been paid
          purchases_paid = purchases.select{|p| ! p.paid_at.nil? }

          #find albums that HAVEN'T been paid
          albums_due = purchases_due.collect{|p| Album.find( AnnualRenewal.find(p.related_id).album_id ) }
          #find albums that HAVE been paid
          albums_paid = purchases_paid.collect{|p| Album.find( AnnualRenewal.find(p.related_id).album_id ) }

          puts "**************************************** Calcing total cents due *********************************************"
          total_cents_due = this_person_albums_to_renew.collect{|a| a.price_policy.base_price_cents}.sum
          total_cents_paid = purchases_paid.collect{|p| p.cost_cents}.sum || 0.0

          person['id'] = person.id
          person['name'] = person.name
          person['num_of_albums_due'] = this_person_albums_to_renew.size
          person['num_of_albums_paid'] = albums_paid.size
          person['total_due'] = total_cents_due / 100.00
          person['total_recieved'] = total_cents_paid / 100.00
          #person['conversion_rate'] = (albums_paid.size / albums_due.size) * 100.00
          self.rows << person

          sum_albums_due = sum_albums_due + albums_due.size
          sum_albums_paid = sum_albums_paid + albums_paid.size
          sum_cents_due = sum_cents_due + total_cents_due
          sum_cents_paid = sum_cents_paid + sum_cents_paid

        end

        totals_row = Person.new
        totals_row['name'] = "<strong>Totals</strong>"
        totals_row['num_of_albums_due'] = "<strong>#{sum_albums_due}</strong>"
        totals_row['num_of_albums_paid'] = "<strong>#{sum_albums_paid}</strong>"
        totals_row['total_due'] = "<strong>#{(sum_cents_due / 100.00)}</strong>"
        totals_row['total_paid'] = "<strong>#{(sum_cents_paid / 100.00)}</strong>"
        #totals_row['conversion_rate'] = "<strong>#{(sum_albums_paid.size / sum_albums_due.size) * 100.00}</strong>"
        self.rows.insert(0, totals_row)

      end

    end
  end
end
