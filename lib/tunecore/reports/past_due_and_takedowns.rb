module Tunecore
  module Reports
    # = Description
    #
    #
    # = Usage
    #
    #
    # = Change Log
    # [2009-07-16 -- ED]
    # Added Amazon On Demand column for past due and taken down reporting
    # [2009-08-14 -- ED]
    # Added Nokia column for past due and taken down reporting. Ticket - 99
    # [2010-01-15 -- MJL]
    # Fixing a call to album.artist which had been deprecated with multiple creatives
    # and singles.  Have modified it to call album.artist_name (which concatenates) the
    # list of all primary_artist creatives.
    class PastDueAndTakedowns < Tunecore::Reports::Base

      def self.parameters
        []
      end

      def initialize(opts={})
        self.header = %w{ renewal_date upc album_id album_name artist_name itunes rh mn np em sc gt am ll sh as lw aod_us nokia my zn takedown_date }
        super
      end

      def self.doc
        <<-EOSTRING
          Albums that are 30+ days past due for renewal fees, or marked as takedown.
        EOSTRING
      end

      def calculate!
        #find all the purchases and albums that are overdue
        purchases = Purchase.where("related_type = 'Renewal' and paid_at is NULL and created_at < ?", Date.today - 30)
        albums = purchases.collect do |purchase|
          album_renewal = AlbumRenewal.find( purchase.related_id )
          Album.find(album_renewal.album_id)
        end

        # Add takedown albums to the list
        takedowns = Album.where("takedown_at is not NULL")
        takedowns.each {|td| albums << td}

        # Build the rows
        albums.each do |album|
          album['renewal_date'] = album.renewal_due_on? ? album.renewal_due_on : "None"
          album['upc'] = album.upc
          album['album_id'] = album.id
          album['album_name'] = album.name
          album['artist_name'] = album.creatives.blank? ? "?" : album.artist_name
          album['itunes']  = ! album.salepoints.detect {|s| s.store_id <= 6 }.nil?
          album['rh'] = ! album.salepoints.detect {|s| s.store_id == 7}.nil?
          album['mn'] = ! album.salepoints.detect {|s| s.store_id == 8}.nil?
          album['np'] = ! album.salepoints.detect {|s| s.store_id == 9}.nil?
          album['em'] = ! album.salepoints.detect {|s| s.store_id == 10}.nil?
          album['sc'] = ! album.salepoints.detect {|s| s.store_id == 11}.nil?
          album['gt'] = ! album.salepoints.detect {|s| s.store_id == 12}.nil?
          album['am'] = ! album.salepoints.detect {|s| s.store_id == 13}.nil?
          album['ll'] = ! album.salepoints.detect {|s| s.store_id == 15}.nil?
          album['sh'] = ! album.salepoints.detect {|s| s.store_id == 16}.nil?
          album['as'] = ! album.salepoints.detect {|s| s.store_id == 17}.nil?
          album['lw'] = ! album.salepoints.detect {|s| s.store_id == 20}.nil?
          album['aod_us'] = ! album.salepoints.detect {|s| s.store_id == 19}.nil?
          album['nokia'] = ! album.salepoints.detect {|s| s.store_id == 21}.nil?
					album['my'] = ! album.salepoints.detect {|s| s.store_id == 23}.nil?
          album['zn'] = ! album.salepoints.detect {|s| s.store_id == 24}.nil?
          album['takedown_date'] = album.takedown_at.strftime("%Y-%m-%d")

          self.rows << album
        end

      end

    end
  end
end
