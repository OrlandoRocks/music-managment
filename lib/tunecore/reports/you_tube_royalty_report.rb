module Tunecore
  module Reports
    module YouTubeRoyaltyReport
      #
      # Initializes a royalty report
      #
      def initialize(person, start_date, end_date, options = {})
        @person             = person
        @start_date         = start_date
        @end_date           = end_date
        @release_types      = options[:release_types]
        @release_ids        = options[:release_ids]
        @release            = options[:release]
        @song               = options[:song]
        @only_monetized     = options[:only_monetized].nil? ? true : options[:only_monetized]
        @max_statement_time = options[:max_statement_time]
        @total_revenue = 0
      end

      def filtering(scope)
        if @start_date.present?
          scope = scope.where("sales_period_start >= ?", @start_date)
        end

        if @end_date.present?
          scope = scope.where("sales_period_start <= ?", @end_date)
        end

        if @release_types.present?
          scope = scope.where("albums.album_type in (?)", @release_types)
        end

        if @release_ids.present?
          scope = scope.where("albums.id in (?)", @release_ids)
        end

        if @release.present?
          scope = scope.where("albums.id = ?", @release.id)
        end

        if @song.present?
          scope = scope.where("songs.id = ?", @song.id)
        end

        if @only_monetized
          scope = scope.where("net_revenue > 0")
        end

        scope
      end

      def summarize(records)
        return nil if records.blank?
        @total_revenue = records.map(&:net_revenue).reduce(:+)
      end

      def to_report(select, joins, group)
        relation = @person.you_tube_royalty_records.joins(joins)
        relation = filtering(relation)
        relation = relation.group(group).select(select)
        records =
          if @max_statement_time.present?
            relation.with_max_statement_time(@max_statement_time)
          else
            relation.all
          end
        [records, summarize(records)]
      end
    end
  end
end
