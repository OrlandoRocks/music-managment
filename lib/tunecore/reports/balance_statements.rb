module Tunecore
  module Reports
    class BalanceStatements < Base

      def self.parameters
        [:start_date, :end_date] 
      end

      def initialize(opts={})
        self.header = %w{ desc amount }
        super
      end

      def self.doc
        doc = <<-EOSTRING 
          Shows total transactions in Tunecore balances over a given period.
        EOSTRING
        doc
      end

      def title
        "Tunecore Balance Statement: #{self.start_date.to_date} through #{self.end_date.to_date}"
      end

      def calculate!
        self.end_date ||= self.start_date + 30
        statement = BalanceStatement.generate(self.start_date.to_date, self.end_date.to_date)
        BalanceStatement.money_fields.each do |field|
          hash = {"desc" => field.to_s.titleize, "amount" => statement.send(field).as_dollars}
          self.rows << hash
        end
      end

    end
  end
end