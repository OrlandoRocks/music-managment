module Tunecore
  module Reports
    class Signups < Tunecore::Reports::Base

      def initialize(opts={})
        self.header= %w{ date signups conversions conversion_rate revenue per_person_revenue }
        super
      end

      def self.doc
        doc = <<-EOSTRING
          Shows new account signups. Conversions are the number of new accounts who have paid an invoice to Tunecore. The conversion rate is the percentage of new accounts that paid an invoices to Tunecore. Per person revenue divides total revenue from the new accounts by the number of new accounts.
        EOSTRING
        doc
      end

      def calculate!
        group_conditions = Reports::Base.group_expression(interval, "people.created_on")

        self.rows = Person.select("#{group_conditions || "'ALL'"} as date, count(*) as signups, (sum(payment.total) / 100) as revenue, count(distinct(payment.person_id)) as conversions").joins("LEFT JOIN (SELECT person_id, sum(final_settlement_amount_cents) as total FROM invoices WHERE settled_at is not null GROUP BY person_id) payment ON payment.person_id = people.id").group(group_conditions).where(date_conditions("people.created_on")).order('date DESC').map {|row|
          row.conversion_rate = row[:conversions].to_i.percent_of_s(row[:signups].to_i)
          row.per_person_revenue = ( row[:revenue].to_f / row[:conversions].to_f ).as_dollars
          row[:revenue] = row[:revenue].to_f.as_dollars
          row
        }

      end

    end
  end
end
