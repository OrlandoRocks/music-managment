module Tunecore
  module Reports
    class AlbumsAdded < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date album_state albums }
        super
      end

      def self.doc
        <<-EOSTRING
          Albums added to the Tunecore system broken down by album state.
        EOSTRING
      end

      def calculate!
        group_exp = Reports::Base.group_expression(interval, "albums.created_on")
        group_conditions = [group_exp, "albums.album_state"].compact.join(", ")

        result_rows = Album.select("#{group_exp || "'ALL'"} as date, album_state, count(*) as albums").group(group_conditions + " WITH ROLLUP").where(date_conditions("albums.created_on"))

        result_rows.group_by {|row|
          row[:date]
        }.reject{ |k, v|
          k.nil?
        }.sort {|a,b| b <=> a }.each {|date,rows|
          self.rows << {"date" => "#{date}"}
          rows.each {|r|
            r["album_state"] = "total" if r["album_state"].nil?
            r["album_state"] = "grand total" if 1 == rows.size and r["date"].nil?
            r["date"] = nil
            self.rows << r
          }
        }

      end

    end
  end
end
