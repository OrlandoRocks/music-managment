# 2009-11-24 -- MJL -- Fixing how new and returning customers are calculated.  We were marking too many people as returning customers.
# 2009-11-24 -- MJL -- Adding the method todays_row to allow easier access to the data records for testing and soon on the UI
module Tunecore
  module Reports
    class ProductSales < DataReport

      def data(back = 6)
        results = []
        0.upto(back) do |x|
          record_set = data_records.where(resolution_identifier: Utilities::DateRange.new(:day, Date.today - x).identifier.to_s)
          results << [record_set.detect{|x| ['total revenue'].include?(x.description)}, record_set.detect{|x| ['invoices revenue'].include?(x.description)}, record_set.detect{|x| ['albums'].include?(x.description)}, record_set.detect{|x| ['singles'].include?(x.description)}, record_set.detect{|x| ['annual renewals'].include?(x.description)}, record_set.detect{|x| ['early annual renewals'].include?(x.description)}, record_set.detect{|x| ['additional stores'].include?(x.description)}, record_set.detect{|x| ['videos'].include?(x.description)}, record_set.detect{|x| ['new customers'].include?(x.description)}, record_set.detect{|x| ['returning customers'].include?(x.description)}, record_set.detect{|x| ['avg revenue per cust'].include?(x.description)}]
        end

        results.compact
      end

      #currently used in testing, but will soon be accessed via the ui
      #creates a data_row that allows for access via the key descriptions
      def todays_row
        return DataRow.new(data(1).first)
      end
      #
      #
      #def record_live
      #  #build_records(:live, Date.today, Date.today + 1)
      #  build_records(:live, date_range_month(Date.today)[0], date_range_month(Date.today)[1])
      #
      #end
      #
      #def record_day(date)
      #  build_records(:day, date_range_day(date)[0], date_range_day(date)[1])
      #end
      #
      #def record_month(date)
      #  build_records(:month, date_range_month(date)[0], date_range_month(date)[1] + 1)
      #end

      def build_records(resolution, start, stop)
        invoices = Invoice.where('settled_at between ? and ?', start, stop)

        @album_purchases = [0, 0]
        @single_purchases = [0, 0]
        @annual_renewal_purchases = [0, 0]
        @early_annual_renewal_purchases = [0, 0]
        @additional_store_purchases = [0, 0]
        @video_purchases = [0, 0]
        @invoice_total = 0
        @total_customers= []
        @new_customers= 0
        @returning_customers= 0



        invoices.each do |invoice|
          @total_customers<< invoice.person_id
          @invoice_total += invoice.final_settlement_amount_cents
          invoice.purchases.each do |purchase|
            case purchase.sti_type
              when 'HistoricAlbumPurchase' then historic_album_purchase(purchase)
              when 'AlbumPurchase' then album_purchase(purchase)
              when 'VideoPurchase' then video_purchase(purchase)
              else
                raise RuntimeError, "Purchase type is not being handled by the ProductSales Report"
            end
          end
        end

        @total_customers.uniq!      # eliminate duplicate person_id's in this list
        @total_customers.each do |person_id|
            person = Person.find(person_id)
            if person.invoices.where("settled_at < ?", Date.today).size > 0
                @returning_customers += 1
            else
                @new_customers += 1
            end
        end

        total_revenue = (@album_purchases[1] + @single_purchases[1] + @annual_renewal_purchases[1] + @early_annual_renewal_purchases[1] + @additional_store_purchases[1] + @video_purchases[1]).to_f / 100.to_f

        @avg_rev= 0.0
        tc= @total_customers.size.to_f
        if tc > 0
            @avg_rev= total_revenue / tc
        end

        data = [[@album_purchases[0], 'albums'],
                [@album_purchases[1].to_f / 100.to_f, 'album sales revenue'],
                [@single_purchases[0], 'singles'],
                [@single_purchases[1].to_f / 100.to_f, 'single sales revenue'],
                [@annual_renewal_purchases[0], 'annual renewals'],
                [@annual_renewal_purchases[1].to_f / 100.to_f, 'annual renewals revenue'],
                [@early_annual_renewal_purchases[0], 'early annual renewals'],
                [@early_annual_renewal_purchases[1].to_f / 100.to_f, 'early annual renewals revenue'],
                [@additional_store_purchases[0], 'additional stores'],
                [@additional_store_purchases[1].to_f / 100.to_f, 'additional stores revenue'],
                [@video_purchases[0], 'videos'],
                [@video_purchases[1].to_f / 100.to_f, 'video revenue'],
                [total_revenue, 'total revenue'],
                [@invoice_total.to_f / 100.to_f, 'invoices revenue'],
                [@total_customers.size, 'total unique customers'],
                [@new_customers, 'new customers'],
                [@returning_customers, 'returning customers'],
                [@avg_rev, 'avg revenue per cust']
               ]

        create_record(resolution, start, data)
      end


      def album_purchase(purchase)
        #if it is an first time album purchase we want all of its "junk" (stores, tracks, etc) all summed up.  Else we want the separate items broken out
        if purchase.purchase_items.detect{|x| x.purchaseable_type == 'Album'} && purchase.related.instance_of?(Single)
          @single_purchases[0] += 1
          @single_purchases[1] += purchase.purchase_total_cents
        elsif purchase.purchase_items.detect{|x| x.purchaseable_type == 'Album'}
          #create_record(resolution, start, [[purchase.cost_cents, 'revenue_cents', 'album purchase']])
          @album_purchases[0] += 1
          @album_purchases[1] += purchase.purchase_total_cents

        else
            # count the annual renewals
            if purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'} && purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'}.purchaseable.promotion_identifier == 'Annual Renewal'
              @annual_renewal_purchases[0] += 1
              @annual_renewal_purchases[1] += PriceCalculator.of(purchase, purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'}.purchaseable).total_cost_cents
            elsif purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'} && purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'}.purchaseable.promotion_identifier == 'Early Annual Renewal 1'
              @early_annual_renewal_purchases[0] += 1
              @early_annual_renewal_purchases[1] += PriceCalculator.of(purchase, purchase.purchase_items.detect{|x| x.purchaseable_type == 'AnnualRenewal'}.purchaseable).total_cost_cents
            end

            #get the number of sale points and add them all up
            if purchase.purchase_items.detect{|x| x.purchaseable_type == 'Salepoint'}
              number_of_new_stores = purchase.purchase_items.select{|x| x.purchaseable_type == 'Salepoint'}.size

              @additional_store_purchases[0] += number_of_new_stores

              purchase.purchase_items.select{|x| x.purchaseable_type == 'Salepoint'}.each do |store_item|
                @additional_store_purchases[1] += PriceCalculator.of(purchase, store_item.purchaseable).total_cost_cents
              end
            end

            # If an individual song was added.... Yes, we have one...
            if purchase.purchase_items.detect{|x| x.purchaseable_type == 'Song'}
              number_of_new_songs = purchase.purchase_items.select{|x| x.purchaseable_type == 'Song'}.size #yes, in fact we do have an album with a single song added....  Ahhhh, admins.
              @album_purchases[0] += 1
              @album_purchases[1] += (number_of_new_songs * PriceCalculator.of(purchase, purchase.purchase_items.detect{|x| x.purchaseable_type == 'Song'}.purchaseable).total_cost_cents)
            end
        end

        rescue
          puts "there was an error recording an album purchase on album purchase# #{purchase.id}"

      end

      def historic_album_purchase(purchase)
        @album_purchases[0] += 1
        @album_purchases[1] += purchase.purchase_total_cents # I don't think we care to separate historic vs. other album purchases
      end

      def video_purchase(purchase)
        @video_purchases[0] += 1
        @video_purchases[1] += purchase.purchase_total_cents
      end

    end
  end
end
