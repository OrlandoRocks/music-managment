module Tunecore
  module Reports
    class Customers < Tunecore::Reports::Base

      def initialize(opts={})
        self.header = %w{ date customers new percent_new returning_customers percent_returning revenue new_customer_revenue percent_new_customer_revenue returning_customer_revenue percent_returning_customer_revenue }
        super
      end

      def self.doc
        <<-EOSTRING
            Shows new vs. returning customers. Takes all customers who paid invoices over the period and determines which were new and which had previously paid an invoice. The revenue from each of these groups is totalled.
        EOSTRING
      end

      def calculate!
        conditions = date_conditions_string("I1.created_at", " AND I1.settled_at is not null")
        date_part = self.class.group_expression(interval, "I1.created_at")
        date_part = "'ALL'" if date_part.empty?

    query =  <<-EOSTRING
    SELECT date, count(distinct person_id) as customers,
                 sum(if(previous_count > 0,1,0)) as returning_customers,
                 sum(person_revenue) as revenue,
                 sum(if(previous_count > 0,person_revenue,0)) as returning_customer_revenue
    FROM
         (
           SELECT
           #{date_part} as date,
           I1.person_id,
           count(I1.id) as person_invoice_count,
           sum(I1.final_settlement_amount_cents) / 100 AS person_revenue,
           (SELECT count(*) FROM invoices I2 WHERE I2.person_id = I1.person_id
                AND I2.settled_at IS NOT NULL AND I2.settled_at < I1.settled_at) AS previous_count
          FROM invoices AS I1
          WHERE #{conditions}
          GROUP BY #{self.class.group_expression_string(interval, "I1.settled_at", ",")} person_id
          ORDER BY date DESC, person_invoice_count
         ) AS time_periods
    GROUP BY date
    ORDER BY date DESC
                EOSTRING


        self.rows = Invoice.find_by_sql(query).map {|row|
          report = {}
          report['new'] = row[:customers].to_i - row[:returning_customers].to_i
          report['date'] = row[:date]
          report['customers'] = row[:customers]
          report['returning_customers'] = row[:returning_customers]
          report['percent_new'] = report['new'].to_i.percent_of_s(row[:customers].to_i)
          report['percent_returning'] = row[:returning_customers].to_i.percent_of_s(row[:customers].to_i)

          revenue = row[:revenue].to_f
          returning_customer_revenue = row[:returning_customer_revenue].to_f
          new_customer_revenue = revenue - returning_customer_revenue

          report['revenue'] = revenue.as_dollars
          report['returning_customer_revenue'] = returning_customer_revenue.as_dollars
          report['new_customer_revenue'] =  new_customer_revenue.as_dollars
          report['percent_new_customer_revenue'] = new_customer_revenue.percent_of_s(revenue)
          report['percent_returning_customer_revenue'] = returning_customer_revenue.percent_of_s(revenue)

          report
        }

      end

    end
  end
end
