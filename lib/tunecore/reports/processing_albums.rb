module Tunecore
  module Reports
    class ProcessingAlbums < DataReport


      def data(back = 6)
        #self.record

        results = []
        0.upto(back) do |x|
          results << data_records.where(resolution_identifier: resolution_identifier(:day, (Date.today - x)).to_s)
        end

        results.compact
      end



      def record_day(date)
        data = []
        data << [self.processing_albums.size, "processing longer than 48 hours"]

        create_record(:day, date, data)
      end

      def processing_albums
        Album.where("locked_by_type = 'PetriBundle' AND takedown_at IS null AND deleted_date IS null").includes('bundles').select{|x| x.locked_by.updated_at < (Time.now - (60 * 60 * 48))}.collect{|x| x.id}
      end


    end
  end
end