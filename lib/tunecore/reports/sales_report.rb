module Tunecore
  module Reports

    #
    # FILTERING
    # Date is srm.period_sort
    # RELEASE_TYPE is if sr.related_type = 'Album' then albums.album_type  if sr.related_type = 'Video' then 'Video'
    # Stores is sip_stores.display_group_id
    # Countries is srm.country
    # Distribution_type affects what we're summing
    # Releases can be albums/singles/ringtones or videos...
    #
    #  Returns Summary
    #    { :releases_sold, :release_amount, :songs_sold, :song_amount, :streams_sold, :stream_amount, :total_earned }
    #  Returns sales_records instantiated with
    #    ( release_id, release_type, releases_sold, release_amount, songs_sold, song_amount, streams_sold, stream_amount )
    #
    class SalesReport
      include Tunecore::Reports::SalesReportFilter

      def initialize(person, type, options={})
        @person = person
        @type   = type

        @sip_store_lookup = {}
        @tc_store_lookup  = {}
        @release_lookup   = {}
        @song_lookup      = {}

        #Map report data method to report type
        case type
        when :by_release, :release_report
          @report_method = :by_release
          populate_release_lookup
        when :by_song, :song_report
          @report_method = :by_song
          populate_song_lookup
          populate_release_lookup
        when :by_date, :date_report
          @report_method = :by_month
        when :by_store, :store_report
          @report_method = :by_store
        when :by_country, :country_report
          @report_method = :by_country
        when :sales_period
          raise "Must initialize with month" if options[:month].nil?
          @month = options[:month]
          @report_method = :full_export_for_sales_period
        when :reporting_period
          raise "Must initialize with month" if options[:month].nil?
          @month = options[:month]
          @report_method = :full_export_for_reporting_period
        else
          raise "Unsupported type"
        end
      end

      #
      # Returns release name in O(1) using release lookup hash
      #
      def release_name(release_type, release_id)
        @release_lookup[release_type][release_id].try(:name) || eval(release_type).find_by(id: release_id).try(:name)
      end

      #
      # Returns report data and its summary
      #
      def report_and_summary(options)
        data = self.send(@report_method, options)
        [data, summarize(data, options)]
      end

      #
      # Returns just the report data
      #
      def report(options)
        self.send(@report_method, options)
      end

      #
      # Returns song name in O(1) using song lookup hash
      #
      def song_name( song_id )
        @song_lookup[song_id].try(:name) || Song.find_by(id: song_id).try(:name)
      end

      #
      # Returns a Tunecore Store given the report sip store id
      #
      def tunecore_store( sip_store_id )
        ReadOnlyReplica::Store.joins(:sip_stores).where(sip_stores: {id: sip_store_id}).first
      end

      #
      # Returns a Sip Store name in O(1)
      #
      def sip_store_name( sip_store_id )
        SipStoresDisplayGroup
          .select(sip_stores_display_groups[:name].as('name'))
          .joins(sip_stores_join)
          .where(sip_stores[:id].eq(sip_store_id))
          .first.name
      end

      #
      # Export report to csv
      #
      def to_csv(options)
        csv = CSV.new(response = "", :row_sep => "\r\n")

        rows_for_report = self.send(@report_method, options)

        case @report_method
        when :by_release
          csv << ["Title","Type","Releases Sold","Songs Sold","Streams","Total Earned"]
          rows_for_report.each do |sales_record|
            csv << [release_name(sales_record.release_type, sales_record.release_id),
                    sales_record.release_type,
                    sales_record.releases_sold,
                    sales_record.songs_sold,
                    sales_record.streams_sold,
                    sales_record.total_earned]
          end
        when :by_song
          csv << ["Song Name","Release Name","Songs Sold","Streams","Total Earned"]
          rows_for_report.each do |sales_record|
            csv << [song_name(sales_record.song_id),
                    release_name(sales_record.release_type, sales_record.release_id),
                    sales_record.songs_sold,
                    sales_record.streams_sold,
                    sales_record.total_earned]
          end
        when :by_month
          csv << ["Date","Releases Sold","Songs Sold","Streams","Total Earned"]
          rows_for_report.each do |sales_record|
            csv << [sales_record.resolution,
                    sales_record.releases_sold,
                    sales_record.songs_sold,
                    sales_record.streams_sold,
                    sales_record.total_earned]
          end
        when :by_store
          csv << ["Store","Releases Sold","Songs Sold","Streams","Total Earned"]
          rows_for_report.each do |sales_record|
            csv << [sip_store_name(sales_record.store_id),
                    sales_record.releases_sold,
                    sales_record.songs_sold,
                    sales_record.streams_sold,
                    sales_record.total_earned]
          end
        when :by_country
          csv << ["Country","Releases Sold","Songs Sold","Streams","Total Earned"]
          rows_for_report.each do |sales_record|
            csv << [sales_record.country,
                    sales_record.releases_sold,
                    sales_record.songs_sold,
                    sales_record.streams_sold,
                    sales_record.total_earned]
          end
        end

        return response
      end

      #
      # Export as a streamable csv, returns an enumerator of csv rows
      # Rows are streamed in and out of memory
      #
      def to_streamable_csv(options)
        case @report_method
        when :full_export_for_sales_period, :full_export_for_reporting_period
          Enumerator.new do |y|
            y <<  CSV::Row.new(
              [:sales_period, :posted_date, :store_name, :country, :artist, :release_type, :release_title, :song_title, :label,
               :upc, :optional_upc, :tc_song_id, :optional_isrc, :sales_type, :units, :price_per_unit, :net_sales, :net_sales_currency,
               :exchange_rate, :total_earned, :currency ],
              ["Sales Period","Posted Date","Store Name","Country Of Sale", "Artist","Release Type","Release Title", "Song Title","Label",
               "UPC", "Optional UPC","TC Song ID","Optional ISRC","Sales Type","# Units Sold", "Per Unit Price","Net Sales","Net Sales Currency",
               "Exchange Rate","Total Earned","Currency"], true).to_csv(encoding: "utf-8")

            self.send(@report_method, options) do |sr|
              data = {sales_period: sr["period_sort"], posted_date: sr["posted_date"], store_name: sr["store_name"], country: sr["country"], artist: sr["artist_name"],
                                release_type: sr["release_type"], release_title: sr["release_title"], song_title: sr["song_title"], label: sr["label_name"], upc: sr["upc"],
                                optional_upc: sr["optional_upc"], tc_song_id: sr["isrc"], optional_isrc: sr["optional_isrc"], sales_type: sr["distribution_type"],
                                units: sr["quantity"], price_per_unit: sr["revenue_per_unit"], net_sales: sr["revenue_total"], net_sales_currency: sr["revenue_currency"],
                                exchange_rate: sr["exchange_rate"], total_earned: sr["amount"], currency: sr["amount_currency"]}
              y << CSV::Row.new(data.keys, data.values).to_csv(encoding: 'utf-8')
            end
          end
        end
      end

      private


      #
      # Returns sales grouped by the sales record master country
      #
      def by_country(options = {})
        downloads, streaming = true, true
        if !options[:distribution_type].blank?
           downloads = options[:distribution_type].include?("Download")
           streaming = options[:distribution_type].include?("Streaming")
        end

        ReadOnlyReplica::SalesRecord.select("s_stores.id as store_id,
                                            srm.country,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.downloads_sold,  0)) as releases_sold,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.download_amount, 0)) as release_amount,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.downloads_sold,  0)) as songs_sold,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.download_amount, 0)) as song_amount,
                                            sum(if( #{streaming}, sr.streams_sold, 0)) as streams_sold,
                                            sum(if( #{streaming}, sr.stream_amount,0)) as stream_amount,
                                            sum(if( #{streaming}, sr.stream_amount,0) + if( #{downloads}, sr.download_amount, 0)) as total_earned,
                                            srm.amount_currency as amount_currency").joins("inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                            inner join sip_stores s_stores on srm.sip_store_id=s_stores.id").from("sales_record_summaries sr").where(build_conditions_array(@person, options)).group("srm.country").order("total_earned desc")
      end

      #
      # Returns sales grouped by release ( album/single/ringtone/video )
      #
      def by_release(options = {})
        downloads, streaming = true, true
        if !options[:distribution_type].blank?
           downloads = options[:distribution_type].include?("Download")
           streaming = options[:distribution_type].include?("Streaming")
        end

        ReadOnlyReplica::SalesRecord.select("sr.release_id,
                                            sr.release_type,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.downloads_sold,  0)) as releases_sold,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.download_amount, 0)) as release_amount,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.downloads_sold,  0)) as songs_sold,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.download_amount, 0)) as song_amount,
                                            sum(if( #{streaming}, sr.streams_sold, 0)) as streams_sold,
                                            sum(if( #{streaming}, sr.stream_amount,0)) as stream_amount,
                                            sum(if( #{streaming}, sr.stream_amount,0) + if( #{downloads}, sr.download_amount, 0)) as total_earned,
                                            srm.amount_currency as amount_currency,
                                            srm.revenue_currency as revenue_currency").joins("INNER JOIN sales_record_masters srm on srm.id=sr.sales_record_master_id
                                           INNER JOIN sip_stores s_stores ON srm.sip_store_id=s_stores.id").from("sales_record_summaries sr").where(build_conditions_array(@person, options)).group("release_id, release_type").order("total_earned desc")
      end

      #
      # Returns sales grouped by month
      #
      def by_month(options = {})
        downloads, streaming = true, true
        if !options[:distribution_type].blank?
          downloads = options[:distribution_type].include?("Download")
          streaming = options[:distribution_type].include?("Streaming")
        end

        ReadOnlyReplica::SalesRecord.select("date_format(str_to_date(srm.period_sort, '%Y-%m-%d'), '%b %Y') as resolution,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.downloads_sold,  0)) as releases_sold,
                                            sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.download_amount, 0)) as release_amount,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.downloads_sold,  0)) as songs_sold,
                                            sum(if( #{downloads} and sr.related_type='Song', sr.download_amount, 0)) as song_amount,
                                            sum(if( #{streaming}, sr.streams_sold, 0)) as streams_sold,
                                            sum(if( #{streaming}, sr.stream_amount,0)) as stream_amount,
                                            sum(if( #{streaming}, sr.stream_amount,0) + if( #{downloads}, sr.download_amount, 0)) as total_earned,
                                            srm.amount_currency as amount_currency").joins("inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                           inner join sip_stores s_stores on srm.sip_store_id=s_stores.id").from("sales_record_summaries sr").where(build_conditions_array(@person, options)).group("srm.period_sort desc")
      end

      #
      # Returns sales grouped by song
      #
      def by_song(options={})
        downloads, streaming = true, true
        if !options[:distribution_type].blank?
          downloads = options[:distribution_type].include?("Download")
          streaming = options[:distribution_type].include?("Streaming")
        end

        options[:query_modifier_conditions] = "sr.related_type='Song' and sr.release_type !='Ringtone'"
        ReadOnlyReplica::SalesRecord.select("sr.related_id as song_id,
                                              sr.release_id, sr.release_type,
                                              sum(0) as releases_sold,
                                              sum(0) as release_amount,
                                              sum(if( #{downloads}, sr.downloads_sold,  0)) as songs_sold,
                                              sum(if( #{downloads}, sr.download_amount, 0)) as song_amount,
                                              sum(if( #{streaming}, sr.streams_sold, 0)) as streams_sold,
                                              sum(if( #{streaming}, sr.stream_amount,0)) as stream_amount,
                                              sum(if( #{streaming}, sr.stream_amount,0) + if( #{downloads}, sr.download_amount, 0)) as total_earned,
                                              srm.amount_currency as amount_currency").joins("inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                             inner join sip_stores s_stores on srm.sip_store_id=s_stores.id").from("sales_record_summaries sr").where(build_conditions_array(@person, options)).group("sr.related_id").order("total_earned desc")
      end

      #
      # Returns sales grouped by sales record master store display group
      #
      def by_store(options = {})
          downloads, streaming = true, true
          if !options[:distribution_type].blank?
             downloads = options[:distribution_type].include?("Download")
             streaming = options[:distribution_type].include?("Streaming")
          end

          ReadOnlyReplica::SalesRecord.select("s_stores.id as store_id,
                                              sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.downloads_sold,  0)) as releases_sold,
                                              sum(if( #{downloads} and (sr.related_type = 'Album' or sr.related_type='Video'), sr.download_amount, 0)) as release_amount,
                                              sum(if( #{downloads} and sr.related_type='Song', sr.downloads_sold,  0)) as songs_sold,
                                              sum(if( #{downloads} and sr.related_type='Song', sr.download_amount, 0)) as song_amount,
                                              sum(if( #{streaming}, sr.streams_sold, 0)) as streams_sold,
                                              sum(if( #{streaming}, sr.stream_amount,0)) as stream_amount,
                                              sum(if( #{streaming}, sr.stream_amount,0) + if( #{downloads}, sr.download_amount, 0)) as total_earned,
                                              srm.amount_currency as amount_currency").joins("inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                              inner join sip_stores s_stores ON srm.sip_store_id=s_stores.id").from("sales_record_summaries sr").where(build_conditions_array(@person, options)).group("s_stores.display_group_id").order("total_earned desc")
      end

      #
      # Exports all sales records for a sale period in a stream
      #
      def full_export_for_sales_period(options={}, &block)
        results = sales_record_export_query("DATE(srm.period_sort) = DATE('#{Date.parse(@month)}')")

        results.each(as: :hash) do |sr|
          yield sr
        end
      end

      #
      # Exports all sales records for a reporting period in a stream
      #
      def full_export_for_reporting_period(options={}, &block)
        parsed_month = Date.parse(@month)

        condition = [
          "MONTH(srm.created_at) = MONTH('#{parsed_month}')",
          "YEAR(srm.created_at) = YEAR('#{parsed_month}')",
        ].join(" AND ")

        results = sales_record_export_query(condition)

        results.each(as: :hash) do |sr|
          yield sr
        end
      end

      # Generic helper method to construct query for export methods
      def sales_record_export_query(condition)
        client = ReadOnlyReplica::SalesRecord.connection.raw_connection

        sql = %Q{
          SELECT *
          FROM (
            SELECT #{ReadOnlyReplica::SalesRecord.export_information_select('sales_records')}
            FROM sales_records
            #{ReadOnlyReplica::SalesRecord.export_information_join('sales_records')}
            WHERE sales_records.person_id = #{@person.id} and #{condition}
            GROUP by sales_records.id

            UNION ALL

            SELECT #{ReadOnlyReplica::SalesRecord.export_information_select('sales_record_archives')}
            FROM sales_record_archives
            #{ReadOnlyReplica::SalesRecord.export_information_join('sales_record_archives')}
            WHERE sales_record_archives.person_id = #{@person.id} and #{condition}
            GROUP by sales_record_archives.id
          ) result
        }

        client.query(sql, stream: true)
      end

      #
      # Aggregate summaries in O(n)
      #
      def summarize(results, options)
        summary = {}
        summary[:releases_sold]  = 0
        summary[:release_amount] = 0
        summary[:songs_sold]     = 0
        summary[:song_amount]    = 0
        summary[:streams_sold]   = 0
        summary[:stream_amount]  = 0
        summary[:total_earned]   = 0

        results.each do |result|
          #Downloads only
          if !options[:distribution_type].blank? && options[:distribution_type].include?("Download") and options[:distribution_type].size == 1

            summary[:releases_sold]  += result.releases_sold
            summary[:release_amount] += result.release_amount
            summary[:songs_sold]     += result.songs_sold
            summary[:song_amount]    += result.song_amount

          #Streaming Only
          elsif !options[:distribution_type].blank? && options[:distribution_type].include?("Streaming") and options[:distribution_type].size == 1

            summary[:streams_sold]  += result.streams_sold
            summary[:stream_amount] += result.stream_amount

          #Everything
          else

            summary[:releases_sold]  += result.releases_sold
            summary[:release_amount] += result.release_amount
            summary[:songs_sold]     += result.songs_sold
            summary[:song_amount]    += result.song_amount
            summary[:streams_sold]   += result.streams_sold
            summary[:stream_amount]  += result.stream_amount

          end
        end

        summary[:total_earned] = summary[:song_amount] + summary[:release_amount] + summary[:stream_amount]
        summary[:currency] = results.first.try(:amount_currency) if results.present?
        summary
      end

      def populate_release_lookup
        return if @release_lookup.present? 

        albums    = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Album'",@person.id).order("name ASC")
        singles   = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Single'",@person.id).order("name ASC")
        ringtones = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Ringtone'",@person.id).order("name ASC")
        videos    = ReadOnlyReplica::Video.select("id, name").where("person_id = ?",@person.id).order("name ASC")

        @release_lookup["Album"]    = {}
        @release_lookup["Single"]   = {}
        @release_lookup["Ringtone"] = {}
        @release_lookup["Video"]    = {}

        albums.each    { |a| @release_lookup["Album"][a.id]    = a }
        singles.each   { |s| @release_lookup["Single"][s.id]   = s }
        ringtones.each { |r| @release_lookup["Ringtone"][r.id] = r }
        videos.each    { |v| @release_lookup["Video"][v.id]    = v }
      end

      def populate_song_lookup
        return unless @song_lookup.empty?

        ReadOnlyReplica::Song.select(:id, :name).joins(:album).where(albums: { person_id: @person }).order(track_num: :asc)
                             .each { |s| @song_lookup[s.id] = s }
      end

      def sip_stores
        SipStore.arel_table
      end

      def sip_stores_display_groups
        SipStoresDisplayGroup.arel_table
      end

      def sip_stores_join
        sip_stores_display_groups
          .join(sip_stores)
          .on(sip_stores_display_groups[:id].eq(sip_stores[:display_group_id]))
          .join_sources
      end
    end
  end
end
