module Tunecore
  module Reports
    class UploaderComparison < DataReport

      def data(back = 0)
        results = []
        0.upto(back) do |x|
          results << data_records.where(resolution_identifier: resolution_identifier(:live, (Date.today - (7 * x))).to_s)
        end

        results.compact
      end



      def record_live
        data = []

        nu_account_count = 0
        ou_account_count = 0
        nu_albums_created = 0
        ou_albums_created = 0
        nu_albums_finalized = 0
        ou_albums_finalized = 0
        pc_wnu = 0
        pf_wnu = 0

        pc_wou = 0
        pf_wou = 0


        #get the number of people who have been created using the alternate uploaders
        nu_account_count = Person.where("created_on >= '2008-10-12' AND alternate_upload is true").size
        ou_account_count = Person.where("created_on >= '2008-10-12' AND alternate_upload is false").size


        #get the number of people who have made albums using the new uploader
        nu_albums_created = Person.where("created_on >= '2008-10-12' AND alternate_upload is true").select{|x| x.albums.size > 0}.size
        ou_albums_created = Person.where("created_on >= '2008-10-12' AND alternate_upload is false").select{|x| x.albums.size > 0}.size



        #get the number of albums purchase
        nu_albums_finalized = Person.where("created_on >= '2008-10-12' AND alternate_upload is true").select{|x| x.albums.any?{|y| y.finalized?}}.size
        ou_albums_finalized = Person.where("created_on >= '2008-10-12' AND alternate_upload is false").select{|x| x.albums.any?{|y| y.finalized?}}.size


        pc_wnu =  (nu_albums_created.to_f / nu_account_count.to_f) * 100.0 if nu_account_count > 0
        pf_wnu =  (nu_albums_finalized.to_f / nu_account_count.to_f) * 100.0 if nu_account_count > 0

        pc_wou =  (ou_albums_created.to_f / ou_account_count.to_f) * 100.0 if ou_account_count > 0
        pf_wou =  (ou_albums_finalized.to_f / ou_account_count.to_f) * 100.0 if ou_account_count > 0

          #raise "fucker: #{pc_wnu} -- #{pc_wnu.class}"
        data = [[nu_account_count, '# using new uploader'],
                [nu_albums_created, 'albums created with nu'],
                [nu_albums_finalized, 'albums finalized with nu'],
                [pc_wnu, 'rate created w nu' ],
                [pf_wnu, 'rate finalized w nu' ],

                [ou_account_count, '# using old uploader'],
                [ou_albums_created, 'albums created with ou'],
                [ou_albums_finalized, 'albums finalized with ou'],
                [pc_wou, 'rate created w ou' ],
                [pf_wou, 'rate finalized w ou' ],

               ]


        create_record(:live, '2008-10-12'.to_date, data)
      end

    end
  end
end