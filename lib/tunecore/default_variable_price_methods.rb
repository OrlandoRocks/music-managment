module Tunecore
  module DefaultVariablePriceMethods


    #
    #  Abstracting away the DefaultVariablePrice class and giving
    #  an interface for VariablePrice
    #
    def default_variable_price
      @default_variable_price ||= DefaultVariablePrice.find_by(store_id: self.id)
      @default_variable_price ? @default_variable_price.variable_price : implicit_default_variable_price
    end

    #
    #  Abstracting away the DefaultVariablePrice class and takes
    #  an interface for VariablePrice
    #
    def default_variable_price=(variable_price)
      transaction do
        delete_default_variable_price
        return if ! variable_price  # get out, we're done
        set_new_default_variable_price(variable_price)
      end
    end

    protected

    def delete_default_variable_price
      DefaultVariablePrice.where("store_id = #{self.id}").delete_all
    end

    def set_new_default_variable_price(val)
      variable_prices_store = variable_prices_stores.find_by(variable_price_id: val.id)

      if ! variable_prices_store
        raise ActiveRecord::Rollback
      else
        @default_variable_price = DefaultVariablePrice.create(
          variable_price_store_id: variable_prices_store.id,
          store_id: self.id
        )
      end
    end

    private

    def implicit_default_variable_price
      ret = (self.variable_prices.length == 1) ? self.variable_prices.first : nil
    end

  end
end
