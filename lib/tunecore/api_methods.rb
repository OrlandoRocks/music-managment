module Tunecore
  module ApiMethods

    GENRE_MAP = {'Soul/R&B' => 'R&B/Soul'}

    # Gathers all the necessary data to be returned by the API call.
    # release_or_releases -- An Album object or list of Album objects.
    # Returns a Hash which can be sent to the client in JSON format.
    def build_response(release_or_releases)
      releases = [release_or_releases].flatten # force in to an array
      #releases = releases[0..MAX_RESPONSE_LENGTH-1] # enforce the max number of releases returned
      data = releases.collect do |release|
        artwork = {:small => artwork_url(release,:small),
                   :medium => artwork_url(release,:medium),
                   :large => nil,
                   :original => nil}
        creatives = release.creatives.collect{|c| {:role => c.role, :name => c.artist.name} }
        renewal = {}
        songs = release.songs.collect{|s| {:id => s.id,
                                           :track_number => s.track_number,
                                           :tunecore_isrc => s.tunecore_isrc,
                                           :optional_isrc => s.optional_isrc,
                                           :explicit => s.parental_advisory,
                                           :artists => get_creatives(s),
                                           :title => s.name,
                                           :asset_metadata => s.big_box_meta_data == nil ? nil : s.big_box_meta_data.collect{|k,v| {k => Array(v)[0]} }[0],
                                           :bigbox_uuid => s.bigbox_uuid,
                                           :composer_id => s.composer !=nil ? s.composer.id : nil
                                           }
                                      }
        {:id => release.id,
        :type => release.class.name,
        :title => release.name,
        :finalized_at => release.finalized_at,
        :valid => release.valid?,
        :status => release.status,
        :created_on => release.created_on.strftime("%m/%d/%Y"),
        :modified_on => release.updated_at.strftime("%m/%d/%Y"),
        :sale_date => release.sale_date.strftime("%m/%d/%Y"),
        :alerts => release.alerts,
        :stores => release.store_groups,
        :tunecore_upc => release.tunecore_upc ? release.tunecore_upc.number : nil,
        :optional_upc => release.optional_upc ? release.optional_upc.number : nil,
        :person_id => release.person_id,
        :has_various_artists => release.is_various,
        :previously_released => release.previously_released,
        :label => release.label_name,
        :genre => release.genres[0].name,
        :sub_genre => release.genres[1] ? release.genres[1].name : '',
        :recording_location => release.recording_location,
        :language => release.language_code,
        :artists => creatives,
        :artwork => artwork,
        :renewal => renewal,
        :salepoints => release_salepoints(release),
        :songs => songs}
      end
      if release_or_releases.class == Array
        return data
      else
        return data[0]
      end
    end

    # size must be :small, :medium
    def artwork_url(release,size)
      artwork = release.artwork
      return nil if !artwork || !artwork.last_errors.blank?
      release.artwork.url(size)
    end

    # item -- song or album
    def get_creatives(item)
      item.creatives.collect{|c| {:role => c.role, :name => c.artist.name} }
    end

    # returns a hash of the salepoint information for the given release
    def release_salepoints(release)
      release.salepoints.collect{|sp| store_group = sp.store.group == nil ? nil : sp.store.group
                                      {:id => sp.id,
                                       :store_name => sp.store.short_name,
                                       :store_group => store_group,
                                       :payment_applied => sp.payment_applied}
                                }
    end


    # Takes a release and applies changes to it.
    # release -- the release that you want to update
    # new_release_data -- Hash of the data that will update, should be in the format returned by build_response
    def update_release(release,new_release_data)

      # map genres if necessary
      if GENRE_MAP[new_release_data['genre']]
        api_logger.info "Mapping primary genre: #{new_release_data['genre']} => #{GENRE_MAP[new_release_data['genre']]} "
        new_release_data['genre'] = GENRE_MAP[new_release_data['genre']]
      end

      if GENRE_MAP[new_release_data['sub_genre']]
        api_logger.info "Mapping sub-genre: #{new_release_data['sub_genre']} => #{GENRE_MAP[new_release_data['sub_genre']]} "
        new_release_data['sub_genre'] = GENRE_MAP[new_release_data['sub_genre']]
      end

      release.name = new_release_data['title']
      release.label_name = new_release_data['label']
      release.language_code = new_release_data['language']
      release.album_type = new_release_data['type'] if new_release_data['type']
      release.is_various = new_release_data['has_various_artists']
      release.sale_date = new_release_data['sale_date']
      release.primary_genre = Genre.find_by(name: new_release_data['genre'])
      release.secondary_genre = Genre.find_by(name: new_release_data['sub_genre'])
      release.previously_released = new_release_data['previously_released']
      release.recording_location = new_release_data['recording_location']
      release.optional_upc_number = new_release_data['optional_upc'] if new_release_data['optional_upc']

      # # Singles creatives are handled differently than Album creatives
      if (release.album_type == 'Single' || release.album_type == 'Ringtone') && new_release_data['artists'] && new_release_data['artists'].size > 0
        update_song_creatives(release.songs.first, new_release_data['artists'])
      end

      if release.album_type == 'Album' && new_release_data['artists'] && new_release_data['artists'].size > 0
        # set the artist name for the album
        release.artist_name = new_release_data['artists'][0]['name']

        # remove any creatives that exist on the album but were not passed in via the api
        creatives_to_keep = release.creatives.select do |creative|
           new_release_data['artists'].detect{|a| a['role'] == creative.role && a['name'] == creative.name}
        end

        creatives_to_destroy = release.creatives - creatives_to_keep

        api_logger.debug("creatives_to_keep: #{creatives_to_keep.collect{|c| c.name}.join(',')}")
        api_logger.debug("creatives_to_destroy: #{creatives_to_destroy.collect{|c| c.name}.join(',')}")

        creatives_to_destroy.each do |creative|
          api_logger.info("Removing creative '#{creative.name}' from release '#{release.name}'")
          Creative.find(creative.id).destroy
        end

        # add new creatives if there are multiple roles
        if new_release_data['artists'].uniq.size >= 1
          new_release_data['artists'].uniq.each do |new_artist|
            api_logger.debug("Adding new creative: #{new_artist['name']}")
            release.send("add_#{new_artist['role'].underscore}".to_sym, new_artist['name']) # copied from admin/albums_controller#add_role
          end
        end

      end

      release.save!

      # 1) Find tracks by song.track_num.  If there is already a song for that track_num, update the attributes for that track.
      # 2) If there is no song for that track_num, create it.
      # 3) Remove existing songs that are not included in 'new_tracks'
      new_tracks = new_release_data['tracks']
      if new_tracks
        new_tracks.each do |new_track|
          track = release.songs.detect{|song| song.track_num == new_track['number']}

          if ! track
            api_logger.info("creating new track for album #{release.id}.  Metadata: #{new_track.inspect}")
            track = Song.new(:album_id => release.id)
          end
          api_logger.debug("track=#{track.inspect}")
          track.name = new_track['title']
          track.optional_isrc = new_track['optional_isrc'] if new_track['optional_isrc']
          track.parental_advisory = new_track['explicit'] if new_track['explicit']
          track.creatives = new_track['artists'] if new_track['artists']
          track.track_num = new_track['number']
          #TODO song creatives
          api_logger.debug("Saving track: #{track.inspect} creatives: #{track.creatives}")
          track.save!
        end

        # remove existing songs that are not included in 'new_tracks'
        release.songs.each do |existing_song|
          new_track_numbers = new_tracks.collect{|new_track| new_track['number']}
          if ! new_track_numbers.include?( existing_song.track_num)
            existing_song.delete
            api_logger.info("Deleted song #{existing_song.id}")
          end
        end
      end

      return release
    end


    # 2012-2-10 AK
    # Deletes existing creatives, replaces with new_creatives
    # media -- single
    # creatives -- hash of creatives
    #
    # Raises an exception if the operation cannot complete - the exception should be handled
    # so that an appropriate message can be returned to the client.
    def update_song_creatives(media, new_creatives)
      api_logger.debug("update_song_creatives(#{media},#{new_creatives.inspect}). media class: '#{media.class.name}'")
      Creative.transaction do
        media.creatives.destroy_all # TODO don't destroy the creative if it's the same as what the client passed in
        if media.class.name == 'Song' && (media.album.class.name == 'Single' || media.album.class.name == 'Ringtone')
          api_logger.debug("Updating creatives for #{media.class}, will remove Album creatives first")
          media.album.creatives.destroy_all
        end
        new_creatives.each do |creative|
          begin
            a = Artist.find_or_create_by(name: creative['name'])
            c = Creative.new(:role => creative['role'],
                             :creativeable_id => media.id,
                             :creativeable_type => media.class.name,
                             :artist_id => a.id)
            api_logger.debug("Saving Creative for #{media.class.name} #{media.id}: #{c.inspect}")
            c.save!
            api_logger.debug("Saved creative: #{c.inspect}")
          rescue => e
            api_logger.warn("Error in update_creatives: #{e.message}")
            raise "Couldn't update creatives: #{e.message}"
          end
        end
      end
    end

    def api_logger
      @api_logger ||= ActiveSupport::Logger.new("#{Rails.root}/log/api.log")
    end

  end
end