module Tunecore
  class StoreManager

    attr_reader :album_hash
    attr_reader :automator_hash
    attr_reader :albums_missing_count
    attr_reader :stores
    attr_reader :stores_subtotal
    attr_reader :missing_total_count
    attr_reader :apple_music_eligible_albums
    attr_reader :currency

    def self.qualified_albums(person, store = nil)
      qualified = person
        .albums
        .distributed
        .not_taken_down
        .not_rejected
        .not_ringtones
        .includes(:salepoints, :tunecore_upcs, :optional_upcs, :salepoint_subscription)

      qualified = qualified.genre_filter(Genre.spoken_word_genre.id)  if (store && store.name == "Shazam")
      qualified = qualified.genre_filter(Genre.classical_genre.id)    if (store && store.short_name =~ /iTunes/)
      qualified
    end

    def self.qualified_albums_for_automator(person, store = nil)
      person
        .albums
        .distributed
        .not_taken_down
        .not_rejected
        .not_ringtones
        .includes(:salepoint_subscription)
        .reject{|a| a.has_salepoint_subscription? or !a.supports_subscriptions}
    end

    def self.store_manager_stores(person, store_ids = nil)
      stores = show_active_stores(person) ? active_stores(person) : Store
      stores = stores.includes(:variable_prices)

      if store_ids
        stores.find(store_ids)
      else
        stores.where(
          "(short_name NOT LIKE ? AND short_name NOT LIKE ?) OR short_name = ?",
          "iTunes%",
          "AmazonOD",
          "iTunesWW"
        )
        .order("on_sale desc, launched_at desc")
      end
    end

    def self.create_store_purchases(person, stores_hash)
      return if stores_hash.blank?
      stores = store_manager_stores(person, stores_hash.keys)
      stores.each do |store|
        if !stores_hash["#{store.id}"].blank?
          albums = qualified_albums(person, store).salepoints_not_bought(store).where("albums.id IN (?)", stores_hash["#{store.id}"])
          album_ids = Salepoint.create_batch_salepoints(store, albums)

          if !album_ids.blank?
            cart_up = person.salepoints.where("store_id = ? AND salepointable_type = ? AND salepointable_id in (?)", store.id, "Album", album_ids)
            Purchase.create_batch_purchases(person, cart_up)
          end
        end
      end
    end

    def self.create_automator_purchases(person, data)
      return if data.blank?

      albums = qualified_albums(person)
        .includes(:salepoint_subscription)
        .where("id in (?)", data)

      album_ids = SalepointSubscription.create_batch_subscriptions(albums)

      if !album_ids.blank?
        cart_up = person.salepoint_subscriptions.where("album_id in (?)", album_ids)
        Purchase.create_batch_purchases(person, cart_up)
      end
    end

    def initialize(person)
      @person = person
      @stores = []
      @stores_subtotal = 0
      @album_hash = {}
      @automator_hash = nil
      @missing_total_count = 0
      @apple_music_eligible_albums = []
      generate_data
    end

    private

    def generate_data
      stores = StoreManager.store_manager_stores(@person)
      albums = StoreManager.qualified_albums(@person)
      automator = Product.automator_product(@person)
      Product.set_targeted_product_and_price(@person, automator)
      automator_money = automator.adjusted_price.to_money(automator.currency)
      @automator_hash = {
        missing_releases: [],
        price: automator_money.amount,
        original_price: automator.original_price,
        currency_symbol: automator_money.currency.symbol,
        product: automator
      }

      albums.each do |album|
        @album_hash[album.id] = { name: album.name, upc: album.upc.number, release_type: album.album_type}
        @automator_hash[:missing_releases] << album.id if (album.salepoint_subscription.blank? && album.supports_subscriptions)
        @apple_music_eligible_albums << album if !album.apple_music && album.has_itunes_ww_salepoint?
      end

      missing_set = Set.new

      stores.each_with_index do |s, i|
        compute_store(s, missing_set)
      end

      @currency = @person.currency
      @missing_total_count = missing_set.size
    end

    def compute_store(s, set)
      store           = { missing_releases: [] }

      missing_albums = StoreManager.qualified_albums(@person, s)
        .salepoints_not_bought(s)
        .to_a
        .select { |album| !(album.automated_store?(s) && album.has_been_delivered?(s)) }

      missing_albums.each do |album|
        store[:missing_releases] << album.id if @album_hash[album.id]
        set << album.id
      end

      if !store[:missing_releases].blank?
        store[:id]                  = s.id
        store[:name]                = s.name
        store[:abbrev]              = s.abbrev
        store[:description]         = s.description
        store[:knowledgebase_slug]  = Store::STORE_INFO_LINKS.fetch(s.abbrev, "")
        store[:store_types]         = Store::STORE_DISTRO_TYPES.fetch(s.abbrev, [])
        store[:on_sale]             = s.on_sale
        store[:on_flash_sale]       = s.on_flash_sale
        store[:position]            = s.position

        sp_attr                     = {store_id: s.id}
        sp_attr[:variable_price_id] = s.default_variable_price.id if s.variable_pricing_required?
        sp                          = Salepoint.new(sp_attr)
        product                     = Product.find_ad_hoc_product(sp, @person)
        product.set_current_targeted_product(TargetedProduct.targeted_product_for_active_offer(@person, product))

        store_price                 = Product.price(@person, sp, product)

        store[:upgrade_cost]        = store_price.amount
        store[:original_cost]       = store_price.amount
        store[:currency_symbol]     = store_price.currency.symbol

        targeted                    = product.current_targeted_product

        if targeted
          store[:upgrade_cost]      = product.set_adjusted_price(store[:upgrade_cost])
          store[:original_cost]     = product.original_price
          store[:target_flag]       = targeted.flag_text if !targeted.flag_text.blank?
          store[:target_desc]       = targeted.price_adjustment_description if !targeted.price_adjustment_description.blank?
          store[:target_exp]        = targeted.targeted_offer.expiration_date_for_person(@person) if targeted.show_expiration_date
        end

        @stores_subtotal += store[:upgrade_cost] * store[:missing_releases].size
        @stores << store
      end
    end

    def self.show_active_stores(person)
      person.nil? || !FeatureFlipper.show_feature?(:show_inactive_stores, person.id)
    end

    def self.active_stores(person)
      return Store.is_active if FeatureFlipper.show_feature?(:freemium_flow, person)
      Store.is_active.exclude_discovery(person)
    end
  end
end
