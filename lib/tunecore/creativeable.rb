module Tunecore
  module Creativeable

    NON_AUTO_FORMATTED_LANGUAGE_CODES = ["fr", "it", "sv"]

    #the code below goes through the array of creative role types defined in the method Creatives.roles
    #taking each one in turn and creating 2 methods (one to add and one to access the list) of
    #each creative type.
    # This allows for methods such as album.primary_artists, song.composer_artists
    creative_roles = Creative.roles

    creative_roles.each do |role|
      define_method("add_#{role.underscore}".to_sym) do |passed_in_name|
        Creative.create_role(self, passed_in_name, role.underscore)
      end
    end

    creative_roles.each do |role|
      define_method("#{role.underscore.pluralize}".to_sym) do
        creatives.select{|x| x.role == role.underscore}
      end
    end

    # featurings is a particularly ugly method name so I'm overriding it here
    # feature_artist had been the previous name desired by iTunes so featured_artsist is littered through the site
    def featured_artists
      return featurings
    end

    def add_featured_artist(name)
      add_featuring(name)
    end

    #legacy methods.  Eventually we don't want to call
    def artist
      x = creatives.detect{|x| x.role == 'primary_artist'}
      if x
        return x.artist.nil? ? nil : x.artist
      else
        nil
      end
    end

    def artist=(art)
      if art
        set_new_primary_artist(art)
      else
        remove_all_primary_artists
      end
    end

    def artist_name
      return nil if self.class == Song && self.album.nil?
      artist.name rescue ''
    end

    def artist_name=(arg)
      @artist_name = arg
    end

    def has_featured_artists?
      featured_artists.size > 0
    end

    #
    #  Perform titleization if
    #  we should not allow different formats
    #
    #  The "allow_different_formats?" method kept
    #  spinning my (ABB) head in circles and
    #  titleize is exactly opposite of it.
    #
    def titleize?
      !(allow_different_format? || has_non_formattable_language_code?)
    end

    def has_non_formattable_language_code?
      if is_a?(Album) || is_a?(ReadOnlyReplica::Album)
        NON_AUTO_FORMATTED_LANGUAGE_CODES.include?(language_code.downcase)
      elsif album
        NON_AUTO_FORMATTED_LANGUAGE_CODES.include?(album.language_code.downcase)
      else
        false
      end
    end

    def set_default_auto_format
      self.allow_different_format = has_non_formattable_language_code?
      true
    end

    protected

    #
    #  Pass through all creatives and perform
    #  titleization rules
    #
    def titleize_creatives
      return if ! titleize?

      creatives.each do |creative|
        creative.titleize_artist
      end
    end

    def set_new_primary_artist(art)
      temp = creatives.detect{|x| x.role == 'primary_artist'}
      if temp.nil?
        creatives << Creative.new(:role => 'primary_artist', :artist => art)
      else
        temp.artist = art
        temp.save
      end
    end

    def remove_all_primary_artists
      creatives.select{|x| x.role == 'primary_artist'}.each{|x| x.destroy}
      creatives.reload
    end

  end
end
