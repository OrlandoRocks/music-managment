module Tunecore
  module Trending
    module Cache

      class AlbumCache

        def initialize
          self.logger = Logger.new(Rails.root + "/log/trend_report_cache_misses.log")
        end

        def apply_to(statement)
          apply_zips_to(statement)
          apply_albums_to(statement)
          apply_songs_to(statement)
        end

        def apply_zips_to(statement)
          ziprows = statement.rows.select {|d| not d[:zip].nil?}
          #build_cache_from_zips(ziprows.collect {|d| d[:zip]}.compact.uniq)
          build_cache_from_zips(ziprows.collect {|d| d[:zip][0,5]}.compact.uniq)

          ziprows.each do |row|
            row.us_zip_code = zips[row[:zip][0,5]]
          end
        end

        def apply_albums_to(statement)
          albumrows = statement.rows.select {|d| not d.upc.nil?}
          build_cache_from_albums(albumrows.collect {|d| d.upc }.compact.uniq)
          albumrows.each do |row|
            row.album = albums[row.upc]
    	if row.album
    	  row.person_id = row.album.person_id
            else
              #puts "ROW MISSING ALBUM #{row.row_number}"
            end
          end
        end

        def apply_songs_to(statement)
          songrows = statement.rows.select {|d| not d[:isrc].nil?}
          build_cache_from_songs(songrows.collect {|d| d[:isrc]}.compact.uniq)
          songrows.each do |row|
            # row.song = (songs[row[:isrc]] || songs_by_tunecore_isrc[row[:isrc]]) or next
            song_candidates = Array(songs[row[:isrc]])
            case song_candidates.size
            when 0
              #puts %Q|impending failure: could not locate song for isrc #{row[:isrc]} in #{row.report.statement_file}, row #{row.row_number}.|
              next # nothing to store, this will cause a failure later
            when 1
              row.song = song_candidates[0]
            else # more than 1, we disambiguate
              if failed_songs[row[:isrc]] == true
                #puts "********************SKIPPING SONG WE ALREADY KNOW IS BAD"
              else
                if apple_identifier = AppleIdentifier.includes(:duplicated_isrc => {:song => :album}).find_by(row[:apple_customer_id])
                  #puts "$$$$$$FOUND AN AMBIGUOUS SONG USING APPLE IDENTIFIER"
                  row.song = apple_identifier.duplicated_isrc.song
                  row.album = row.song.album
                else
                  failed_songs[row[:isrc]] = true
                  #puts %Q| FAILED TO RESOLVE AMBIGUOUS ISRC: could not resolve isrc #{row[:isrc]} in #{row.report.statement_file}, row #{row.row_number}.|
                end
              end
            end
            if row.song and album_ids.has_key?(row.song.album_id)
              row.album = album_ids[row.song.album_id]
              #puts "ROW #{row.row_number} has person id #{row.album.person_id}"
              if row.album
                row.person_id = row.album.person_id
              else
                 #puts "ROW MISSING ALBUM FOR SONG #{row.row_number}"
              end
            end
          end

          fatal_failures = 0

          statement.rows.each do |row|
            if row.album.nil?
              logger.warn %Q|Missing album in #{row.inspect}|
              fatal_failures += 1
            end
          end

          songrows.each do |row|
            if row.song.nil?
              logger.warn %Q|Missing song in #{row.inspect}|
              fatal_failures += 1
            end
          end

          statement.rows.select {|d| not d[:zip].nil?}.each do |row|
            if row.us_zip_code.nil? and "USD" == row[:customer_currency]
              logger.warn %Q|Missing zip in #{row.inspect}|
            end
          end

          total_rows = statement.rows.compact.size
          identified_rows = statement.rows.compact.select {|d| !d.target.nil? }.size
          puts "CACHE RESULTS: #{total_rows} total rows, #{identified_rows} successfully identified, #{identified_rows.percent_of_s(total_rows)} INGESTION SUCCESS RATE" unless "test" == Rails.env
        end

        protected

        attr_accessor :logger

        def build_cache_from_zips(code_list)
          code_list.in_groups_of(1000) do |group|
            group = sanitize_group(:zips, group.compact)
            next unless group.length > 0
            records = UsZipCode.where('us_zip_codes.code in (?)',group)
            records.each do |us_zip_code|
              if zips[us_zip_code.code]
                raise "fatal: zip code already in cache: #{us_zip_code.code}.  Either loader logic is flawed or database is not consistent."
              end
              zips[us_zip_code.code] = us_zip_code
            end
          end
        end

        def build_cache_from_albums(id_list)
          id_list.in_groups_of(1000) do |group|
            group = sanitize_group(:albums, group.compact)
            next unless group.length > 0
            records = Album.includes(:songs,:upcs).where('upcs.number in (?)',group)
            cache_records(records)
          end
        end

        def build_cache_from_songs(id_list)
          id_list.in_groups_of(1000) do |group|
            group = sanitize_group(:songs, group.compact)
            next unless group.length > 0

            fetched = Song.select('distinct album_id').where(['songs.tunecore_isrc in (?) or songs.optional_isrc in (?)',
                                                                      group, group]).collect(&:album_id)

            records = Album.includes(:songs, :upcs).find(sanitize_group(:album_ids, fetched))

            cache_records(records)
          end
        end

        def cache_records(records)
          records.each do |album|
            if albums[album.upc.number]
              raise %Q|fatal: album already in cache: #{album.upc}.  Either loader logic is flawed or database is not consistent. It's not safe to continue.|
            end
            albums[album.upc.number] = album
            album_ids[album.id] = album
            album.songs.each do |song|
              if songs[song.isrc].any? {|x| x.id == song.id}
                raise %Q|fatal: song already in cache: #{song.isrc}.  Either loader logic is flawed or database is not consistent. It's not safe to continue.|
              end
              songs[song.isrc] << song
              (songs_by_tunecore_isrc[song.tunecore_isrc] ||= []) << song
            end
          end
        end

        def sanitize_group(aors, group)
          case aors
          when :albums
            group.reject {|d| not albums[d].nil?}
          when :songs
            group.reject {|d| not songs[d].empty?}
          when :album_ids
            group.reject {|d| not album_ids[d].nil?}
          when :zips
            group.reject {|d| not zips[d].nil? }
          else
            raise ArgumentError, "sanitize_group - unknown first argument #{aors.inspect}"
          end
        end

        def albums
          @albums ||= {}
        end

        def songs
          @songs ||= Hash.new {|hsh, key| hsh[key] = [] }
          # @songs ||= {}
        end

        def zips
          @zips ||= {}
        end

        def failed_songs
          @failed_songs ||= {}
        end

        def songs_by_tunecore_isrc
          @songs_by_tunecore_isrc ||= {}
        end

        def album_ids
          @album_ids ||= {}
        end
      end
    end
  end
end
