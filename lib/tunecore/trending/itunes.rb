module Tunecore
  module Trending
    module Itunes

      class ItunesReport

        attr_accessor :rows, :statement_file

        def initialize(statement_file)
          self.statement_file = statement_file
          self.rows = []
        end

        def self.load(file_name)
          report = new(file_name)
          File.open(file_name, 'r') do |file_fh|
            file_fh.each_line do |line|
              row_number = file_fh.lineno
              next if 1 == row_number #apple trending reports have a header row
              if /^\s+/ =~ line
                puts "#{file_name}, row #{row_number}: starts with a space, skipping input line."
                next
              end
              fields = line.chomp.split(/\t/)
              begin
                report.rows << ItunesRow.new(report, fields, row_number)
              rescue
                puts "Error encountered while loading fields from #{file_name}, row #{row_number}"
                raise
              end
            end
          end
          return report
        end

        def rows_for(currency)
          self.rows.select {|d| d[:customer_currency] == currency}
        end

        def save_to_db!(trend_report)
          TrendReportSale.where("trend_report_id = ?", trend_report.id).delete_all
          trend_report.update_attribute :imported_at, Time.now
          rows.in_groups_of(1000) do |group|
            group = group.compact.delete_if {|row| row.target.nil? }
            next if group.empty?
            #hard coded batch saves
            query_values = group.map {|row| "(#{trend_report.id}, #{row.target.id}, '#{row.target.class.name}', #{row.units_sold}, #{row.usd_cents}, #{row.is_promo ? 1 : 0}, #{row.us_zip_code ? row.us_zip_code.id : "NULL" }, #{row.apple_customer_id})" }.join(", ")
            insert_query = "INSERT INTO trend_report_sales (trend_report_id, target_id, target_type, units_sold, usd_cents, is_promo, us_zip_code_id, apple_customer_id)  VALUES #{query_values}"


            TrendReportSale.connection.execute(insert_query)
          end
        end

      end


      class ItunesRow

        attr_reader :report, :row_number, :units_sold, :usd_cents, :is_promo, :upc, :isrc, :zip, :target_type, :apple_customer_id
        attr_accessor :album, :song, :us_zip_code, :person_id

        def initialize(report, raw_data, row_number)
          @report = report
          @row_number = row_number
          @raw_data = raw_data
          @raw_data.each_index do |idx|
            @raw_data[idx] = nil if @raw_data[idx].blank?
          end

          #determine target without using unpredictable "product type identifier"
          if self[:isrc]
            @target_type = :song
            @isrc = self[:isrc]
          elsif self[:upc]
            @target_type = :album
            @upc = self[:upc].to_i.to_s.rjust(12,'0')
          else
            @target_type = :unknown
          end

          #demographic data
          @zip = self[:zip]
          @apple_customer_id = self[:apple_customer_id]

          #unit count
          raise "invalid units sold" if 0 == @units_sold = self[:units_sold].to_i

          #royalty payment
          @is_promo = (0 == (self[:royalty_price].to_f * 100).to_i)
          @usd_cents = if "USD" == self[:customer_currency]
                         (@units_sold * self[:royalty_price].to_f * 100).to_i
                       else
                         0
                       end

        end

        def target
          case self.target_type
          when :album
            self.album
          when :song
            self.song
          else
            nil
          end
        end

        def create_trend_report_sale(trend_report)
          unless self.target.nil?
            TrendReportSale.create( {:trend_report => trend_report,
                                      :target => self.target,
                                      :units_sold => self.units_sold,
                                      :usd_cents => self.usd_cents,
                                      :us_zip_code => self.us_zip_code,
                                      :apple_customer_id => self.apple_customer_id} )
          end
        end

        def [](arg)
          raw_data[self.class.field_index(arg)]
        end

        def inspect
          "Row ##{row_number} in #{self.report.statement_file}: #{@raw_data.join(", ")}"
        end

        protected

        def parsefix_date(str)
          x = Date.parse(str)
          return x if x.year > 2000
          Date.civil(x.year + 2000, x.month, x.day)
        end

        attr_reader :raw_data

        class << self
          INPUT_FIELDS =
            [:service_provider_code,
             :service_provider_country_code,
             :vendor_identifier,
             :upc,
             :isrc,
             :artist_name,
             :target_name,
             :label_name,
             :product_type_identifier,
             :units_sold,
             :royalty_price,
             :sold_on,
             :order_number,
             :zip,
             :apple_customer_id,
             :reporting_date,
             :credit_flag,
             :customer_currency,
             :iso_country_name,
             :royalty_currency,
             :preorder_flag,
             :isan,
             :apple_identifier,
             :fill1,
             :fill2
          ] unless const_defined? :INPUT_FIELDS

          def field_index(arg)
            field_indices[arg]
          end

          def field_indices
            @field_indices ||= INPUT_FIELDS.inject({}) {|memo, val| memo[val] = memo.length; memo }
          end

        end

      end

    end
  end
end