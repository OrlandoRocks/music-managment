class Tunecore::Trending::DateRange

  MAX_DAY_RANGE = 90
  DATE_FORMAT   = "%B %-d, %Y"

  def initialize(last_trend_date, start_date, end_date)
    @last_trend_date = last_trend_date
    @end_date        = !end_date.blank?   ? translate(:end, end_date) : default_date(:end)
    @start_date      = !start_date.blank? ? translate(:start, start_date) : default_date(:start)
    validate_dates
  end

  attr_reader :date_format, :start_date, :end_date

  def formatted_start_date
    @start_date.strftime(date_format)
  end

  def formatted_end_date
    @end_date.strftime(date_format)
  end

  def date_format
    DATE_FORMAT
  end

  private

  def translate(boundary, date)
    unless parsed_date = Date.parse(date)
      parsed_date = Date.parse(Tunecore::DateTranslator.translate(date,"long_date", :en))
    end

    parsed_date
  rescue ArgumentError
    default_date(boundary)
  end

  def default_date(boundary)
    if boundary == :start
      default_start_date
    elsif boundary == :end
      default_end_date
    end
  end

  def default_start_date
    @end_date - 1.week + 1.day
  end

  def default_end_date
     @last_trend_date ? @last_trend_date.to_date : Date.today
  end

  def validate_dates
    check_end_date
    check_start_date
    check_date_range
  end

  def check_end_date
    if @end_date > Date.today
      @end_date = Date.today
    end
  end

  def check_start_date
    if @end_date < @start_date
      @start_date = default_start_date
    end
  end

  def check_date_range
    if ((@end_date - @start_date) > MAX_DAY_RANGE)
      @start_date = @end_date - MAX_DAY_RANGE.days
    end
  end
end
