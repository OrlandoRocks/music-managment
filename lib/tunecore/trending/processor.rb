module Tunecore
  module Trending
    module Processor

      def ingest_trend_report!(filename, report_date)
        readout = "test" != Rails.env

        raise "cant't read file #{filename}" unless File.readable?(filename)
        import_file = filename
        raise "missing report date" unless report_date && report_date.is_a?(Date)
        raise "date too old" unless (Date.today - report_date) < 31

        start_time = Time.now
        puts "importing #{import_file} as trend report for #{report_date}" if readout

        puts "reading report file from disk" if readout
        report = Tunecore::Trending::Itunes::ItunesReport.load(import_file)

        puts "cacheing albums, songs and zip codes for report" if readout
        cache = Tunecore::Trending::Cache::AlbumCache.new
        cache.apply_to(report)

        puts "storing trend report sales records to db" if readout
        trend_report = TrendReport.find_or_create_by(sold_on: report_date)
        report.save_to_db!(trend_report)

        elapsed_time = sprintf("%.2f",(Time.now - start_time) / 60)
        puts "finished at #{Time.now}. Elapsed Time #{elapsed_time} minutes" if readout
      end

    end
  end
end
