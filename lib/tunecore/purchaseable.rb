# This module allows an ActiveRecord::Base object to be
# purchased and priced.
#
# The model must include a price_policy_id integer column and
# there must be a default price_policy for all new instances of
# the model.  Additionally there must be a purchase_id integer column.
#
# If you don't explicitly call:
# default_price_policy :something (see salepoint.rb) then the default
# price_policy will be the underscore() name of your ActiveRecord class.

module Tunecore
  module Purchaseable

    class << self
      def included(base)

        # should not perform db introspection if table does not exist
        return if ! base.table_exists?

        unless base.is_a? Purchaseable::ClassMethods # only setup associations once, use the extend of ClassMethods as gate
          return if ! base.table_exists?

          # freak out if the associations have been defined outside our control
          if base.reflect_on_association(:price_policy)
            base.logger.error "Do not define :price_policy association in #{base.name}" rescue nil
          end
          # freak out if the price_policy_id column is not there
          unless base.columns_hash['price_policy_id']
            base.logger.error "price_policy_id column not present in #{base.table_name.inspect} table" rescue nil
          end

          # ditto freak out for purchase_id
          unless base.columns_hash['purchase_id']
            base.logger.error "purchase_id column not present in #{base.table_name.inspect} table" rescue nil
          end

          base.before_create :ensure_price_policy_id_before_create
          base.belongs_to :purchase unless base.reflect_on_association(:purchase)
          base.extend ClassMethods
        end
      end
    end

    module ClassMethods
      # returns the 'default' price_policy for new instances of your class.
      def price_policy
        PricePolicy[default_price_policy_name()]
      end

      # set the default price_policy, overriding inferencing based on your class name.  eg:
      # Salepoint.default_price_policy_name :store
      def default_price_policy_name(price_policy_name = nil)
        if price_policy_name.nil?
          @default_price_policy_name ||= self.name.underscore.to_sym
        else
          @default_price_policy_name = price_policy_name.to_sym
        end
      end

      # Return the class to use for the purchase you will enlist to.
      # Override this in the Purchaseable if it turns out that either
      # cnoard() guesses wrong or you have some other naming scheme
      # (remember that it's a class method tho...)
      def purchase_class
        my_safe_class_name = self.base_class.name
        "#{my_safe_class_name}Purchase".constantize
      end
    end

    # returns the price_policy for this instance.  If this is a new record and the
    # price_policy_id has not been set, it will fetch the 'default' price_policy from self.class
    # and set this instance's price_policy_id accordingly.
    def price_policy
      self.price_policy_id = self.class.price_policy.id if price_policy_id.blank?
      PricePolicy[self]
    end

    # In case you create a Purchaseable object but don't ask for the price_policy(), we
    # want to make sure the default price_policy_id is set.
    def ensure_price_policy_id_before_create
      price_policy # auto-sets the price_policy_id
    end


    protected :ensure_price_policy_id_before_create

  end
end
