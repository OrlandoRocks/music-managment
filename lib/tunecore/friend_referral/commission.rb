module Tunecore
  module FriendReferral
    class Commission < Tunecore::FriendReferral::Base
      STATUS = { 0 => 'pending', 1 => 'approved', 2 => 'denied' }
  
      def commissions
        data['commissions'].blank? ? []:data['commissions'] 
      end
  
      def commission(transaction_id)
        commissions.detect{ |c| c['transaction_id'] == transaction_id.to_s }
      end
  
      def get_status(transaction_id)
        if c = commission(transaction_id)
          STATUS[c['is_approved'].to_i]
        end
      end
  
      def get_amount(transaction_id)
        if c = commission(transaction_id)
          c['commission_amount'].to_f
        end
      end
    end
    
  end
end
