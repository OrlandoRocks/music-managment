module Tunecore
  module FriendReferral
    class AmbassadorStat < Tunecore::FriendReferral::Base
      def ambassador
        data['ambassador']
      end
  
      def campaign_stats(campaign_uid)
        if campaign = ambassador['campaign_links'].detect { |c| c['campaign_uid'] == campaign_uid.to_s }
          campaign['stats']
        end
      end
  
      def balance
        ambassador['balance_money']
      end
  
      def unique_referrals
        ambassador['unique_referrals']
      end
  
      def stats_summary
        ambassador['stats_summary']
      end
    end
    
  end
end
