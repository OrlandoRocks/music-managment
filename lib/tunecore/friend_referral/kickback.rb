module Tunecore
 module FriendReferral
    class Kickback < Tunecore::FriendReferral::Base

      def campaign_url(campaign_uid)
        if campaign = data['ambassador']['campaign_links'].detect{ |c| c['campaign_uid'] == campaign_uid.to_s }
          campaign['url']
        end
      end

      def campaign_code(campaign_uid)
        if campaign = data['ambassador']['campaign_links'].detect{ |c| c['campaign_uid'] == campaign_uid.to_s }
          campaign['url'].to_s.split('/')[-1]
        end
      end

      def campaigns
        ambassador['campaign_links']
      end

      def referring_ambassador
        data['referring_ambassador']
      end

      def ambassador
        data['ambassador']
      end

      def referrer_commission
        referring_ambassador['commission']
      end

      def referrer_balance
        referring_ambassador['balance_money']
      end

      def referrer_email
        referring_ambassador['email']
      end

      def referrer_id
        referring_ambassador['uid']
      end

      def addthis_snippet(campaign_uid)
        if addthis = ambassador['addthis'].detect{ |a| a['campaign_uid'] == campaign_uid.to_s }
          addthis['snippet']
        end
      end

      def referrer_unique_referrals
        referring_ambassador['unique_referrals'].to_i
      end

      def balance
        data['balance']
      end

      # Params:
      # email your customer's email address
      # uid your customer's internal unique ID
      # first_name  your customer's first name
      # last_name your customer's last name
      # custom1 custom field
      # custom2 custom field
      # email_new_ambassador  email login credentials to new ambassadors  1 - yes
      # 0 - no  1
      # sandbox new ambassadors created as sandbox  1 - yes
      # 0 - no  0
      # deactivate_new_ambassador new ambassadors created are deactivated 1 - yes
      # 0 - no  0
      # auto_create if the customer is not an ambassador, create them 1 - yes
      # 0 - no  1
      # Response:
      # {
      #   "response": {
      #     "code": "200",
      #     "message": "OK: The request was successful. See response body for additional data.",
      #     "data": {
      #       "referring_ambassador": {
      #         "first_name": "Jane",
      #         "last_name": "Doe",
      #         "email": "jane@example.com",
      #         "uid": null,
      #         "commission": null,
      #         "balance_money": "2.40",
      #         "balance_points": "0",
      #         "memorable_url": "http://mbsy.co/tunecore/djane",
      #         "unique_referrals": "1",
      #         "sandbox": "0",
      #         "custom1": null,
      #         "custom2": null
      #       },
      #       "ambassador": {
      #         "first_name": "John",
      #         "last_name": "Doe",
      #         "email": "john@example.com",
      #         "uid": null,
      #         "balance_money": "0.00",
      #         "balance_points": "0",
      #         "memorable_url": "http://mbsy.co/tunecore/djohn",
      #         "unique_referrals": "0",
      #         "sandbox": "0",
      #         "custom1": null,
      #         "custom2": null,
      #         "campaign_links": [
      #           {
      #             "campaign_uid": "25",
      #             "campaign_name": "Refer a Company",
      #             "sandbox": "0",
      #             "private": "0",
      #             "facebook_enabled": "0",
      #             "campaign_description": "Refer a new company to Ambassador and get rewarded 50% of their first payment and 10% of each recurring payment!",
      #             "url": "http:\/\/mbsy.co\/8l",
      #             "total_money_earned": "0",
      #             "total_points_earned": "0"
      #           }
      #         ]
      #       }
      #     }
      #   }
      # }
      def self.signup(person)
        params = {:email => person.email,
          :uid => person.id,
          :first_name => person.first_name,
          :last_name => person.last_name,
          :email_new_ambassador => 0,
          :auto_create => 1
        }

        response = handle_api_exception { api.get_ambassador(params) }

        log_event({ :type => 'referrer_signup', :person_id => person.id, :raw_request => params, :raw_response => response })
        self.new(response)
      end

      # Params:
      # campaign_uid  your campaign's ID
      # email your customer's email address
      # short_code  referrer's "mbsy" shortcode
      # revenue revenue amount of the event   0
      # transaction_uid unique transaction ID for the event
      # ip_address  customer's IP address
      # uid your customer's internal unique ID
      # first_name  your customer's first name
      # last_name your customer's last name
      # custom1 custom field
      # custom2 custom field
      # email_new_ambassador  email login credentials to new ambassadors  1 - yes
      # 0 - no  1
      # deactivate_new_ambassador new ambassadors created are deactivated 1 - yes
      # 0 - no  0
      # auto_create if the customer is not an ambassador, create them 1 - yes
      # 0 - no  1
      # Response:
      # {
      #   "response": {
      #     "code": "200",
      #     "message": "OK: The request was successful. See response body for additional data.",
      #     "data": {
      #       "referring_ambassador": {
      #         "first_name": "Jane",
      #         "last_name": "Doe",
      #         "email": "jane@example.com",
      #         "uid": null,
      #         "commission": 2,
      #         "balance_money": 2,
      #         "balance_points": "0",
      #         "memorable_url": "http://mbsy.co/tunecore/djane",
      #         "unique_referrals": "1",
      #         "sandbox": "0"
      #         "custom1": null,
      #         "custom2": null
      #       },
      #       "ambassador": {
      #         "first_name": "John",
      #         "last_name": "Doe",
      #         "email": "john@example.com",
      #         "uid": null,
      #         "balance_money": 0,
      #         "balance_points": 0,
      #         "memorable_url": "http://mbsy.co/tunecore/djohn",
      #         "unique_referrals": 0,
      #         "sandbox": "0",
      #         "custom1": null,
      #         "custom2": null,
      #         "campaign_links": [
      #           {
      #             "campaign_uid": "25",
      #             "campaign_name": "Refer a Company",
      #             "sandbox": "0",
      #             "private": "0",
      #             "facebook_enabled": "0",
      #             "campaign_description": "Refer a new company to Ambassador and get rewarded 50% of their first payment and 10% of each recurring payment!",
      #             "url": "http:\/\/mbsy.co\/8l",
      #             "total_money_earned": "0",
      #             "total_points_earned": "0"
      #           }
      #         ]
      #       }
      #     }
      #   }
      # }
      def self.record_event(invoice, person, campaign_hash, ip_address)

        params = {
          :campaign_uid => campaign_hash[:campaign_id], :email => person.email, :short_code => campaign_hash[:campaign_code],
          :revenue => invoice.final_settlement_amount_cents/100.0, :transaction_uid => invoice.id, :uid => person.id,
          :first_name => person.first_name, :last_name => person.last_name, :email_new_ambassador => 0,
          :deactivate_new_ambassador => 1, :auto_create => 0, :ip_address => ip_address
        }
        response = handle_api_exception{ api.record_event(params) }

        log_event({ :type => 'referral', :person_id => person.id, :raw_request => params, :raw_response => response })
        self.new(response)
      end

      # Call to social/addthis
      # Params:
      # email your customer's email address
      # uid your customer's internal unique ID
      # first_name  your customer's first name
      # last_name your customer's last name
      # custom1 custom field
      # custom2 custom field
      # custom3 custom field
      # message default share message   "I'm an ambassador of..."
      # email_new_ambassador  email login credentials to new ambassadors  1 - yes
      # 0 - no  1
      # sandbox new ambassadors created as sandbox  1 - yes
      # 0 - no  0
      # deactivate_new_ambassador new ambassadors created are deactivated 1 - yes
      # 0 - no  0
      # auto_create if the customer is not an ambassador, create them 1 - yes
      # 0 - no  1
      #
      # Response:
      # <ambassador>
      #   <addthis>
      #     <link>
      #       <campaign_uid>25</campaign_uid>
      #       <campaign_name>Refer a Company</campaign_name>
      #       <sandbox>0</sandbox>
      #       <private>0</private>
      #       <facebook_enabled>0</facebook_enabled>
      #       <campaign_description>Refer a new company to Ambassador and get rewarded 50% of their first payment and 10% of each recurring payment!</campaign_description>
      #       <url>https://mbsy.co/8m</url>
      #       <total_money_earned>0</total_money_earned>
      #       <total_points_earned>0</total_points_earned>
      #       <snippet><![CDATA[<center><div style="text-align:center;width:350px;" class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="https://mbsy.co/8m" addthis:title="I'm an Ambassador of Example"><a class="addthis_button_facebook"></a><a class="addthis_button_twitter" tw:via="GetAmbassador"></a><a class="addthis_button_stumbleupon"></a><a class="addthis_button_linkedin"></a><a class="addthis_button_tumblr"></a><a class="addthis_button_email"></a><a class="addthis_button_wordpress"></a><a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a></div></center>]]></snippet>
      #     </link>
      #   </addthis>
      # </ambassador>
      def self.get_social_snippet(person, campaign_id, share_message=nil)
        params = {
          :uid => person.id, :email => person.email, :email_new_ambassador => 0, :auto_create => 0, :deactivate_new_ambassador => 1
        }
        params.merge!(:message => share_message) if share_message
        response = handle_api_exception{ api.social_addthis(params) }
        kickback = self.new(response)
        return kickback.addthis_snippet(campaign_id)
      end

      def self.get_ambassador_stats(person, start_date=nil, end_date=nil)
        params = {
          :uid => person.id, :email => person.email, :email_new_ambassador => 0, :auto_create => 0, :deactivate_new_ambassador => 1
        }
        params.merge!(:start_date => start_date) if start_date
        params.merge!(:end_date => end_date) if end_date
        response = handle_api_exception { api.ambassador_stats(params) }

        return AmbassadorStat.new(response)
      end

      # Params:
      # email your customer's email address
      # amount  amount to add   0
      # uid your customer's internal unique ID
      # custom1 custom field
      # custom2 custom field
      # custom3 custom field
      # type  balance type to add to  "money" or "points" money
      # email_new_ambassador  email login credentials to new ambassadors  1 - yes
      # 0 - no  1
      # sandbox new ambassadors created as sandbox  1 - yes
      # 0 - no  0
      # deactivate_new_ambassador new ambassadors created are deactivated 1 - yes
      # 0 - no  0
      # auto_create if the customer is not an ambassador, create them 1 - yes
      # 0 - no  1
      # Response:
      # {
      #       "response": {
      #         "code": "200",
      #         "message": "OK: The request was successful. See response body for additional data.",
      #         "data": {
      #           "balance": {
      #             "old": "993",
      #             "new": "994",
      #             "type": "points"
      #           },
      #           "ambassador": {
      #             "first_name": "Jane",
      #             "last_name": "Doe",
      #             "email": "jane@example.com",
      #             "uid": null,
      #             "balance_money": "0.00",
      #             "balance_points": "0",
      #             "memorable_url": "http://mbsy.co/tunecore/djane",
      #             "unique_referrals": "0",
      #             "sandbox": "0",
      #             "custom1": null,
      #             "custom2": null,
      #             "custom3": null,
      #             "campaign_links": [
      #               {
      #                 "campaign_uid": "25",
      #                 "campaign_name": "Refer a Company",
      #                 "sandbox": "0",
      #                 "private": "0",
      #                 "facebook_enabled": "0",
      #                 "campaign_description": "Refer a new company to Ambassador and get rewarded 50% of their first payment and 10% of each recurring payment!",
      #                 "url": "http:\/\/mbsy.co\/8l",
      #                 "total_money_earned": "0",
      #                 "total_points_earned": "0"
      #               }
      #             ]
      #           }
      #         }
      #       }
      #     }
      def self.add_balance(person, amount)
        params = {
          :uid => person.id,
          :email => person.email,
          :amount => amount,
          :deactivate_new_ambassador => 1,
          :email_new_ambassador => 0
        }

        response = handle_api_exception{ api.add_balance(params) }

        log_event({ :type => 'add_balance', :person_id => person.id, :raw_request => params, :raw_response => response })
        return self.new(response)
      end

      # Returns up to 100 results only. We would need to make multiple calls in cases
      # where we could not find the status after the first call.
      # Params:
      # status [pending, approved, denied]
      def self.get_commissions(person, page=1, status=nil)
        params = {
          :email => person.email,
          :page => page
        }

        if status_code = Commission::STATUS.detect {|k,v| v == status}
          params.merge!(:is_approved => status_code)
        end

        response = handle_api_exception{ api.all_commissions(params) }

        log_event({ :type => 'all_commissions', :person_id => person.id, :raw_request => params, :raw_response => response })
        return Commission.new(response)
      end

      # 2013-05-09 GC - This call does not behave the way I anticipated
      # def self.add_commission(amount, referral)
      #       transaction_uid = referral.invoice_id
      #       params = {
      #         :email => referral.referrer.email,
      #         :uid => referral.referrer_id,
      #         :campaign_uid => referral.campaign_id,
      #         :amount => amount,
      #         :is_approved => 1,
      #         :event_data1 => "First time referral commission for invoice #{transaction_uid}"
      #       }
      #
      #       response = api.add_commission(params)
      #       log_event({ :type => 'add_commission', :person_id => referral.referrer_id, :raw_request => params, :raw_response => response })
      #
      #       return response
      #     end

      private

      def self.log_event(params)
        FriendReferralEvent.create(:event_type => params[:type], :person_id => params[:person_id], :raw_request => params[:raw_request], :raw_response => params[:raw_response])
      end
    end
  end
end
