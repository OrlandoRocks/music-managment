module Tunecore
  module FriendReferral
    class Base
      attr_reader :data, :errors
      
      class << self
        def api
          @api ||= Tunecore::AmbassadorApi
        end
        
        def logger
          @logger ||= Rails.logger
        end
      end
      
      def initialize(params)
        @data = params['data']
        @errors = params['errors'] || []
      end
      
      def self.handle_api_exception(&block)
        begin
          response = block.call
        rescue Tunecore::AmbassadorApi::RequestError => e
          logger.error("AmbassadorApi::RequestError; Message=#{e.message}")
          return {'errors' => e.message}
        end
          
        return response
      end
      
    end
  end
end