require 'net/http'
require 'uri'

module Tunecore
  module Braintree
    class Query

      def execute(query_params)
        params = query_params.merge( {'username' => BRAINTREE[:query_username], 'password' => BRAINTREE[:query_password]} )

        url = URI.parse(BRAINTREE[:query_api_url])
        server = Net::HTTP.new(url.host, 443)
        server.use_ssl = true

        resp = server.start do |http|
          req = Net::HTTP::Post.new(url.path)
          req.set_form_data(params)
          http.request(req)
        end

        case resp
        when Net::HTTPSuccess, Net::HTTPRedirection
          return Hash.from_xml(resp.body)["nm_response"]
        else
          resp.error!
        end
      end
    end
  end
end


