module Tunecore
  module Albums
    module MultipleCreatives
      def self.included(base)
        base.validate :check_for_various_artist_rules
        base.validate :check_for_primary_artist
        base.after_save :save_creatives
      end

      def artist_name
        name = case primary_artists.size
        when 0
           ""
        when 1
          singular_primary_artist_name
        when 2
          two_primary_artists_name
        else
          multiple_primary_artists_name
        end
        return name
      end

      def singular_primary_artist_name
         primary_artists.first.name
      end

      def two_primary_artists_name
         result = primary_artists.collect{ |artist| artist.name }
         result.join(' & ')
      end

      def multiple_primary_artists_name
        result = primary_artists.first(primary_artists.size - 1)
        result.collect!{ |artist| artist.name }
        comma_list = result.join(', ')
        "#{comma_list} & #{primary_artists.last.name}"
      end

      protected

      def artist_error_messages
        # collect all of the messages
        all_messages = artists.collect { |artist|
          artist.valid?
          artist.errors.full_messages
        }
        all_messages.reject! { |error_message| error_message.blank? }
        all_messages
      end

      def check_for_various_artist_rules
        if is_various? && !creatives.empty?
          errors.add(:creatives, "You cannot have an album artist on the album level if the #{self.class.name} is various artist")
        end
      end

      def validation_set_artist
        # override superclass functionality
        # to do nothing.
        # logger.warn "validation set artist is overridden to do nothing.  will be refactored later."
      end

      #
      #  A method to go through each creative,
      #  associate w/ object and save
      #
      def save_creatives
        creatives.each do |creative|
          if ! id.blank?
            creative.creativeable_id = id
            creative.save
          end
        end
      end

      # Replaced original with this method which relies on
      # @creatives_missing which is added in by the creative
      # setter method
      def check_for_primary_artist
        errors.add(:creatives, "#{I18n.t('models.album.requires_main_artist')}") if @creatives_missing
      end

    end
  end
end
