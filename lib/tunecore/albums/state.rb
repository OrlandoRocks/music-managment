module Tunecore
  module Albums
    module State
      def mark_as_deleted
        raise 'You cannot delete this album without a takedown request' unless is_deletable?
        begin
          Purchase.destroy_by_purchase_item(person, self)
          update_attribute :is_deleted, true
          update_attribute :deleted_date, Time.now
          remove_optional_upcs
          remove_physical_upc
          renewal_without_info.update_attribute :canceled_at, Time.now if renewal
        rescue StandardError => e
          logger.error "Error in album mark_as_deleted; album_id=#{self.id}; message=#{e.message}"
          raise 'There is an issue with deleting this album'
        end
      end

      def finalized?
        !finalized_at.blank?
      end

      def unfinalized?
        !finalized? && (payment_applied? || has_ever_been_approved?)
      end

      def has_possibly_been_finalized?
        finalized? || payment_applied?
      end

      def has_ever_been_approved?
        review_audits.where(event: "APPROVED").exists?
      end

      def approved?
        legal_review_state == "APPROVED"
      end

      def needs_review? 
        legal_review_state == "NEEDS REVIEW"
      end

      def is_editable?
        return false if approved?
        !unfinalized? && !has_possibly_been_finalized?
      end

      def is_live?
        ! known_live_on.blank?
      end

      def eligible_for_tier_check?
        finalized? && !is_deleted? && !takedown?
      end

      # This should be moved into the views.
      def state
        case
          when is_deleted?                                then "deleted"
          when locked? && locked_by_type == "Person"      then "admin lock"
          when locked? && locked_by_type == "Invoice"     then "in invoice"
          when takedown?                                  then 'taken down'
          when is_live? && steps_required == 1            then "ready to go"
          when finalized? && has_artwork_but_no_s3_asset? then "legacy"
          when is_live?                                   then "live"
          when finalized? && (steps_required == 0 )       then "sent"
          when steps_required == 1                        then "ready to go"
        else
          steps = ( steps_required || Tunecore::DistributableChecklist::CHECKLIST_STEPS.count ) - 1
          steps == 1 ? "1 step to go"  : "#{steps} steps to go"
        end
      end

      # database wrapper that checks if the steps required is nil.
      # If so, calculated the number of steps.
      def steps_required
        if super.blank?
          calculate_steps_required
          super
        else
          super
        end
      end

      def calculate_steps_required
        if persisted?
          update_attribute(:steps_required, number_of_steps_required)
        end
      end

      #remove the following code after the album_state column has been dropped.
      def album_state
        #raise "ALBUM STATE GETTER"
      end

      def album_state=(args)
        #raise "ALBUM STATE SETTER"
      end

      def is_deletable?
        !has_possibly_been_finalized?
      end

      def rejected?
        if status == "rejected"
          true
        elsif review_audits.empty?
          false
        else
          review_audits.last.event == "REJECTED" && status != "down"
        end
      end

      protected

      def has_artwork_but_no_s3_asset?
        artwork && !artwork.uploaded
      end

    end
  end
end
