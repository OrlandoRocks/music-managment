module Tunecore
  module Albums
    module Takedown
      def takedown?
        takedown_at.present?
      end

      # Mark album as taken down from all stores
      def takedown!(options = {})
        ret_val = false

        #Make sure that the renewal is not processing in batch
        if (self.renewal.blank? || self.renewal.purchases.joins(:invoice).where('invoices.batch_status = ?', :processing_in_batch).count == 0)

          begin
            self.known_live_on    = DateTime.now if known_live_on.blank?
            self.renewal_due_on   = DateTime.now if renewal_due_on.blank?
            self.takedown_at      = DateTime.now
            self.can_stream_cache = false

            # Update the genre if needed before issuing the takedown
            self.ensure_takedown_genre if self.respond_to?(:ensure_takedown_genre)

            # Delete any unpaid renewals
            ::Renewal.renewals_for(self).each{ |r| r.mark_as_takedown! } if ::Renewal.renewals_for(self).present?

            #set album to takedown before enqueuing a distribution
            save(validate: false)

            create_note(options)

            # Set all salepoints to takedown
            if approved_for_distribution?
              salepoints.includes(:store).each { |sp| sp.takedown!(true) unless Store::INDIVIDUAL_TAKEDOWN_STORES.include?(sp.store.short_name) }
            end
            ret_val = true

            bundle = PetriBundle.for(self)

            if bundle && ret_val == true
              reason = {actor: "system", reason: "issuing takedown distribution for iTunes salepoints"}
              bundle.add_salepoints(self.itunes_salepoints, reason)
            else
              raise "Transaction for takedown of rolled back."
            end

            send_sns_notification_takedown(options)
          rescue StandardError => e
            message = "Takedown not processed for #{self.class.name}-#{self.id}: #{e}"
            Tunecore::Airbrake.notify(message)
            logger.info message
            ret_val = false
          end
        end

        return ret_val
      end

      def remove_takedown(options = {})
        ret_val = false

        begin
          self.transaction do
            self.update_attribute(:takedown_at, nil)

            # Remove takedown from salepoints
            salepoints.each { |sp| sp.update_attribute(:takedown_at, nil) }

            # Generate the annual renewal if there is one due
            if ::Renewal.renewal_for(self)
              ::Renewal.renewal_for(self).remove_takedown!
            end

            ret_val = true
          end

          if bundle = PetriBundle.for(self)
            reason = { actor: "system", reason: "issuing reactivation distribution for iTunes salepoints" }
            bundle.add_salepoints(self.itunes_salepoints, reason)
          end

          salepoints.each(&:remove_takedown)

          send_sns_notification_untakedown(options)
        rescue StandardError => e
          logger.info "Remove takedown not processed for #{self.class.name}-#{self.id}: #{e}"
        end

        return ret_val
      end

      def send_sns_notification_takedown(options)
        user_id = options[:user_id].to_i.zero? ? self.person_id : options[:user_id]
        sns_queue = options[:priority].to_i == 1 ? 'SNS_RELEASE_TAKEDOWN_URGENT_TOPIC' : 'SNS_RELEASE_TAKEDOWN_TOPIC'

        return unless FeatureFlipper.show_feature?(:use_sns, user_id) &&
                      ENV.fetch(sns_queue, nil).present?

        Sns::Notifier.perform(
          topic_arn: ENV.fetch(sns_queue),
          album_ids_or_upcs: [self.id],
          store_ids: self.salepoints.pluck(:store_id),
          delivery_type: 'takedown',
          person_id: options[:user_id]
        )
      end

      def send_sns_notification_untakedown(options)
        user_id = options[:user_id].to_i.zero? ? self.person_id : options[:user_id]
        sns_queue = options[:priority].to_i == 1 ? 'SNS_RELEASE_UNTAKEDOWN_URGENT_TOPIC' : 'SNS_RELEASE_UNTAKEDOWN_TOPIC'

        return unless FeatureFlipper.show_feature?(:use_sns, user_id) &&
                      ENV.fetch(sns_queue, nil).present?

        Sns::Notifier.perform(
          topic_arn: ENV.fetch(sns_queue),
          album_ids_or_upcs: [self.id],
          store_ids: self.salepoints.pluck(:store_id),
          delivery_type: 'untakedown',
          person_id: options[:user_id]
        )
      end
    end
  end
end
