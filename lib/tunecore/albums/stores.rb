module Tunecore
  module Albums
    module Stores
      def apple_salepoints
        @apple_salepoints ||= self.salepoints.select {|sp| sp.store.short_name =~ /^iTunes/}
      end

      def live_in_itunes?
        # TODO: remove first condition when all Apple album ids have been moved from Album to ESI table
        apple_identifier.present? || external_id_for("apple").present?
      end

      def has_proxy_ytsr_store?
        has_store?("YTSRProxy")
      end 

      def has_store?(store_short_name)
        stores_by_short_name.include?(store_short_name)
      end

      def has_salepoints?(from_store_names:)
        salepoints.joins(:store).where(stores: { name: from_store_names }).exists?
      end

      def stores_by_short_name
        stores.map(&:short_name)
      end

      def can_not_deselect_store(store_id)
        salepoint = Salepoint.find_by(album_id: id, store_id: store_id)
        if salepoint == nil or salepoint.status == 'new'
          return false
        end
        true
      end

      def unselected_stores
        self.class.stores.is_active - self.stores.is_active
      end

      # SKU used by music stores for reporting purposes.
      def sku
        return "A" + id.to_s.rjust(10, '0')
      end

      # code used by music stores for reporting purposes.
      def code
        "X"+sprintf("%06d",id)
      end

      def has_new_stores?
        !new_stores.empty?
      end

      def new_stores
        salepoints.where("status = 'new'").select("DISTINCT store_id").collect do |sp|
          Store.find(sp.store_id)
        end
      end

      # Conveniently access just the iTunes stores belonging to this album.
      def itunes_stores
        stores.find_all {|store| /^iTunes/ =~ store.name}
      end

      # Convenient accessor for just the non-iTunes stores belonging to this album.
      def other_stores
        stores.reject do |store|
          /^iTunes/ =~ store.name
        end
      end

      # Is the given store within the selected stores or within the
      # list of available stores for this type of album
      def available_store?(store)
        Array(self.class.stores).include?(store)  
      end

      # Returns a list of stores that should be available for the user to select
      def available_stores(person_id = nil)
        return @available_stores if @available_stores
        @available_stores = self.class.stores
        @available_stores = @available_stores.is_active if show_active_stores(person_id)
        @available_stores = @available_stores.exclude_discovery(person_id) unless FeatureFlipper.show_feature?(:freemium_flow, person_id)
        @available_stores = @available_stores.includes(:variable_prices)
        if primary_genre_id == Genre.spoken_word_genre.id
          @available_stores = @available_stores.where.not(name: 'Shazam')
        elsif primary_genre_id == Genre.classical_genre.id
          @available_stores = @available_stores.where.not('name LIKE ?', '%iTunes%')
        end
        @available_stores
      end

      # stores that already have a salepoint reference to a store
      def selected_stores(person_id = nil)
        @selected_stores ||= stores.exclude_discovery(person_id)
        @selected_stores = @selected_stores.is_active if show_active_stores(person_id)
        @selected_stores.includes(:variable_prices).order(:position).all
      end

      def paid_stores
        return @paid_stores if @paid_stores.present?

        @paid_stores = store_platforms(person_id)
        @paid_stores = @paid_stores.is_active if show_active_stores(person_id)
        @paid_stores.where("salepoints.payment_applied = 1").order(:position)
      end

      def listed_stores(person_id = nil)
        if ['incomplete', 'ready'].include?(status)
          selected_stores(person_id)
        else
          paid_stores
        end
      end

      def listed_freemium_platforms(person_id = nil)
        @selected_freemium ||= freemium_platforms(person_id)
        @selected_freemium = @selected_freemium.is_active if show_active_stores(person_id)
        @selected_freemium.includes(:variable_prices).order(:position).all
      end 

      def store_platforms(person_id)
        stores.exclude_discovery(person_id)
      end 

      def freemium_platforms(person_id)
        stores.discovery_platforms(person_id)
      end 
    
      def has_aod
        listed_stores.include?(Store.find_by(short_name: "AmazonOD"))
      end

      def display_added_aod
        aod = Store.find_by(short_name: "AmazonOD")
        salepoints.find_by(store: aod)&.template_id
      end

      # Add stores to this release
      # Expects an array of stores
      # Returns salepoints, store_not_added
      def add_stores(stores)
        salepoints = []
        store_not_added = []
        stores.each do |store|
          if self.salepoints.any?{ |salepoint| salepoint.store_id == store.id }
            store_not_added << store
          else
            salepoint_attrs = {:store=>store}
            salepoint_attrs[:has_rights_assignment] = 1 if store.needs_rights_assignment?
            #FIXME: What would variable price_id be?
            salepoint_attrs[:variable_price_id]     = store.default_variable_price.id if store.variable_pricing_required?

            # To override salepoint attrs options
            salepoint_attrs = yield(salepoint_attrs) if block_given?

            salepoint = self.salepoints.create(salepoint_attrs)

            if salepoint.errors.any?
              store_not_added << salepoint.store
            else
              salepoints << salepoint
            end

          end
        end

        return salepoints, store_not_added
      end

      def salepoints_by_store(store)
        self.salepoints.where(store_id: store.id)
      end

      def has_salepoints_by_store?(store)
        self.salepoints.where(store_id: store.id).present?
      end

      private

      def show_active_stores(person_id)
        person_id.nil? || !FeatureFlipper.show_feature?(:show_inactive_stores, person_id)
      end
    end
  end
end
