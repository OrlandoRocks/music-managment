module Tunecore
  module Albums
    module Discography
      def discography_search(person, options={})
        if options[:keyword].present?
          join_terms = {creatives: :artist}
          search_condition = ["albums.name LIKE ? or artists.name LIKE ?", "%#{options[:keyword]}%", "%#{options[:keyword]}%"]
        end

        release_type_condition = {album_type: options[:release_type]} unless options[:release_type] == "all"

        order_by = options[:sort_by] == "status" ? "steps_required desc, (takedown_at = null) desc, takedown_at" : options[:sort_by]

        status_condition = case options[:status]
        when "down"
          "takedown_at IS NOT null"
        when "live"
          "takedown_at IS null and known_live_on IS NOT null"
        when "incomplete"
          "takedown_at IS null and known_live_on IS null and steps_required > 1"
        when "rejected"
          {legal_review_state: 'REJECTED'}
        when "sent"
          "finalized_at IS NOT null and known_live_on IS null"
        when "ready"
          "steps_required = 1 and known_live_on IS null"
        end

        albums = person.albums.not_deleted.includes(join_terms).where(search_condition)
                       .where(release_type_condition).where(status_condition).references(creatives: :artist).group("albums.id")
                       .order(order_by).page(options[:page]).per_page(options[:per_page])

        [albums, albums.count.count]
      end
    end
  end
end
