module Tunecore
  module Albums
    module Streaming

      def can_stream?
        can_stream_cache
      end

      def has_streaming_distro?
        !distributions.where(converter_class: "MusicStores::Streaming::Converter", state: "delivered").empty?
      end
    end
  end
end
