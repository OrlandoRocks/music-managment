module Tunecore
  module Albums
    module Songs

      def esis_for_songs_by_service(service_name)
        ExternalServiceId.poly_inner_join("songs").where(songs: {album_id: id})
      end

      #
      #  Maximum number of songs per album
      #
      def number_of_tracks_allowed
        100
      end

      #
      #  seed for the next track number
      #
      def next_available_track_number
        songs.size + 1
      end

      #
      #  Save songs in a new order
      #  given list of ids:
      #
      #  e.g.  [12,41,52,23] => save 12 as track number 1, 41 as track number 2, etc.
      #
      def song_order=(song_ids)
        transaction do
          song_ids.each_with_index do |song_id,i|
            song = songs.find(song_id)
            song.track_num = i+1
            song.save
          end
        end
      end

      #
      #  Are any of the songs marked as Parental Advisory
      #
      def parental_advisory?
        songs.any?{|song| song.parental_advisory == true}
      end

      #
      #  leave this in there PETRI uses it instead of partental_advistory?
      #  There is no test to make sure that this exists...
      #
      def parental_advisory
        return parental_advisory?
      end

      def clear_songs_previously_released_at
        previously_released_songs = songs.where(Song.arel_table[:previously_released_at].not_eq(nil))
        previously_released_songs.update_all(previously_released_at: nil)
      end
    end
  end
end
