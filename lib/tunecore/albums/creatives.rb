
module Tunecore
  module Albums
    module Creatives

      def song_creatives
        songs.map { |song| song.creatives }.flatten.uniq
      end

      def resource_contributors
        exact_duplicate_creatives_at_song_level.uniq do |creative|
          creative.artist_id && creative.creative_song_roles.map(&:song_role_id)
        end
      end

      protected

      def exact_duplicate_creatives_at_song_level
        song_creatives.select do |creative|
          songs.all? do |s|
            s.creatives.detect do |c|
              c.role == creative.role && c.artist_id == creative.artist_id
            end
          end
        end
      end

      def titleize_songs
        return true if allow_different_format? || !has_featured_artists?
        songs.each do |song|
          song.titleize_name
        end
        true
      end

      def move_creatives_to_album
        return if is_various?

        exact_duplicate_creatives_at_song_level.each do |song_creative|
          if artist_already_exists_on_album?(song_creative)
            song = song_creative.creativeable
            song.creatives = song.creatives.select { |creative| creative.id != song_creative.id }
          else
            song_creative.creativeable = self
            song_creative.save
            creatives << song_creative
          end
        end
      end

      def artist_already_exists_on_album?(song_creative)
        album_artist_ids = self.creatives.map(&:artist_id)
        album_artist_ids.include?(song_creative[:artist_id])
      end

      def titleize_name
        self.name = Utilities::Titleizer.titleize_name(self) unless allow_different_format || has_non_formattable_language_code?
        true
      end
    end
  end
end
