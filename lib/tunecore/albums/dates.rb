module Tunecore
  module Albums
    module Dates
      attr_accessor :not_previously_released

      def previously_released?
        !not_previously_released
      end

      def previously_released=(value)
        self.not_previously_released = true if value == "false"
        self.not_previously_released = false if value == "true"
      end

      def previously_released
        # guard against nil comparisons
        return false if !sale_date
        return false if !orig_release_year
        return false if !orig_release_year_was
        return false if orig_release_year == sale_date
        sale_date > orig_release_year_was && sale_date >= orig_release_year
      end

      def orig_release_date
        return sale_date if not_previously_released
        orig_release_year == sale_date ? sale_date : orig_release_year
      end

      # display methods should not be in the model
      def golive_date_for_display
        return nil unless golive_date
        return golive_date unless sale_date
        sale_date.to_time > golive_date ? sale_date.to_time : golive_date
      end

      #
      #  Handle the string date transformations
      #
      #  September 29, 2009 => #<Date 2009-09-29>
      #
      def sale_date_to_s
        Tunecore::StringDateParser.to_s(sale_date)
      end

      def sale_date=(date)
        super(date)
      rescue
        self.sale_date = Tunecore::StringDateParser.to_date(date)
      end

      def orig_release_year_to_s
        Tunecore::StringDateParser.to_s(orig_release_year)
      end

      def orig_release_year=(date)
        super(date)
      rescue
        self.orig_release_year = Tunecore::StringDateParser.to_date(date)
      end

      def is_datetime_type(obj)
        [Date, Time, DateTime].any? {|klass| obj.is_a?(klass) }
      end

      protected

      # "silently" set orig_release_year when nil or greater than sale_date
      def set_default_orig_release_year
        self.orig_release_year = self.sale_date if !self.orig_release_year.present? || self.orig_release_year > self.sale_date
        true
      end

      def valid_orig_release_year?
        return true if not_previously_released.nil? || orig_release_year.nil?
        new_release_date =  if sale_date
                              sale_date
                            else
                              Album::DateTransformerService.translate_release_date_time(self).to_date
                            end
        self.errors.add(:orig_release_year, "can't be after Digital Release Date") if orig_release_year > new_release_date
      end

    end
  end
end
