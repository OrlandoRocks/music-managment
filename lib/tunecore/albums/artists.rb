module Tunecore
  module Albums
    module Artists
      def artist_name
        if is_various?
          return 'Various Artists'
        elsif !artist.nil?
          name_list = primary_artists.collect{ |c| c.name }
          return name_list.to_sentence(:last_word_connector => " & ", :two_words_connector => " & ")
        elsif !person.artist.nil?
          return ""
        else
          return ""
        end
      end
    end
  end
end
