module Tunecore
  module Albums
    module OneSong

      #
      #  Includes validations necessary for One Song
      #  rules.  We want to make sure that only one
      #  song is allowed
      #
      def self.included(base)
        base.validates_presence_of :song

        base.validates_size_of :songs,
            :maximum => 1,
            :message => "can only have one song."

        base.validates_size_of :songs,
            :minimum => 1,
            :message => "must have a song"

        base.validate :check_for_song, :validate_optional_isrc
        base.after_save :set_song_name

        base.delegate :optional_isrc, :optional_isrc=,
          :parental_advisory, :parental_advisory=, :clean_version, :clean_version=, :isrc, :to => :song
      end

      #
      #  wrapping the relationship w/ songs
      #  from albums to have an interface for
      #  only one song from an album
      #
      def song
        @song ||= songs.first
      end

      #
      #  using the songs association to create
      #  an interface for setting one and only
      #  one song
      #
      def song=(song)
        @song = song
        songs.clear if @song
        @song.name = name
        songs << @song
      end

      # set a new song_id on the song_ids collection
      def song_id=(id)
        song_ids = [id]
      end

      #  Get the song id
      def song_id
        song ? song.id : nil
      end

      #  Data method to only allow one song
      def number_of_tracks_allowed
        1
      end

      protected

      #
      #  Setting the song to have the same name as the
      #  Album
      #
      def set_song_name
        clean_name = name.split(' - Single')[0] || ""
        song.update_attribute(:name, clean_name)
        true
      end

      #
      #  A validation method to make sure that
      #  the album has a song associated
      #
      def check_for_song
        if song == nil
          errors.add(:song, "must have one song.")
        end
      end

      #
      # A validation method to make sure that
      # the Single's optional_isrc is valid
      #
      def validate_optional_isrc
        if !song.valid? && !song.errors[:optional_isrc].blank?
          errors.add(:optional_isrc, song.errors[:optional_isrc])
        end
      end
    end
  end
end
