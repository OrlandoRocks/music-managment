module Tunecore
  module Albums
    module Petri
      # = Instance Methods =
      def error_message
        DistributionErrorDecoder.decode_bundle( PetriBundle.for( self ) )
      end

      def copyright_name
        begin label_name         rescue nil end ||
        begin person.label_name  rescue nil end ||
        begin artist_name        rescue nil end ||
        begin person.artist_name rescue nil end ||
        person.name
      end
    end
  end
end
