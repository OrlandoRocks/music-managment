# = Description
# The AccountOverview class encompasses a distribution history, available inventory, and renewals due logic
#
# = Usage
# Tunecore::AccountOverview.new(person)
#
# = Change Log
# [2010-02-09 -- CH]
# Created Class
module Tunecore
  class AccountOverview
    attr_accessor :available_inventory, :ringtone_count, :album_count, :single_count, :video_count, :number_of_primary_artists
    attr_accessor :waiting_ringtone_count, :waiting_album_count, :waiting_single_count, :waiting_video_count

    def initialize(person, load_distribution_counts = true)
      Rails.logger.info "GETTING ACCOUNT OVERVIEW"
      self.person = person
      get_available_inventory
      get_distribution_counts if load_distribution_counts == true
    end

    private
    attr_accessor :person

    def get_available_inventory
      self.available_inventory  = CreditUsage.all_available(person)
    end

    def get_distribution_counts
      self.ringtone_count = person.albums.ringtones.distributed.count
      self.album_count    = person.albums.full_albums.distributed.count
      self.single_count   = person.albums.singles.distributed.count
      self.video_count    = person.videos.where("videos.finalized_at IS NOT NULL AND is_deleted = 0").count
      self.number_of_primary_artists = person.active_artists.count
    end

    def get_waiting_counts
      self.waiting_ringtone_count = person.albums.ringtones.undistributed.count
      self.waiting_album_count    = person.albums.full_albums.undistributed.count
      self.waiting_single_count   = person.albums.singles.undistributed.count
      self.waiting_video_count    = person.videos.count(["videos.finalized_at IS NULL"])
    end
  end
end
