#
#  Salepointable:
#
#  = Description
#
#  A class of objects is Salepointable if it can be
#  distributed to stores.  The customer can select
#  which stores they would like their Salepointable
#  objects to be delivered and the reference of
#  Object-to-Store is stored in the *Salepoints* table.
#
#  Each Salepointable type needs to be configured in
#  *salepointable_stores* with a row for every store
#  the type should allow.
#
#
module Tunecore
  module Salepointable

    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods

      #
      #  Find all available stores for the
      #  Salepointable (base class) type
      #
      def stores
        Store.salepointable_type(self.name)
      end

    end

  end
end
