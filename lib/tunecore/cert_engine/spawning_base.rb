module Tunecore
  module CertEngine
    class SpawningBase
      include MultiyearProductIds

      attr_reader :cert
      def initialize(cert)
        @cert = cert
      end

      def spawning?
        true
      end

      # Override the applicable_for_discount in subclasses for special item certs like singles
      def applicable_for_discount?(purchase)
        !multi_year_product_ids.include? purchase.product_id
      end

      def verify(person_id, purchase)
        raise ArgumentError, "expected a Purchase object" unless purchase.is_a?(Purchase)

        if cert.expiry_date < Time.now
          return "Certificate expired at: #{cert.expiry_date.strftime('%d-%b-%Y %l:%M %p')}"
        end

        Cert.transaction do
          if detected = detect_previously_spawned(person_id, purchase)
            return detected
          end

          # create a spawned cert.  We mangle the cert name with a dash
          # because that character will have special significance in the verify()
          # routine for cert-issuers (and no cert will ever be able to have it
          # as a valid character)
          Cert.create!(
            :cert => '-' + cert.cert,
            :person_id => person_id,
            :purchase => purchase,
            :date_used => Time.now,
            :parent_id => cert.id,
            :expiry_date => cert.expiry_date,
            :percent_off => cert.percent_off,
            :cert_engine => cert.cert_engine,
            :engine_params => cert.engine_params,
            :cert_batch_id => cert.cert_batch_id,
            :admin_only => cert.admin_only
          )
        end
      rescue
        return "There was a probem using the cert"
      end

      def detect_previously_spawned(person_id, purchase)
        already_spawned = Cert.where('person_id = ? and parent_id = ?', person_id, cert.id).first

        return nil unless already_spawned

        # show the name of the item they used this cert for
        if cert_purchase = Purchase.find_by(id: already_spawned.purchase_id)
          usedby = " by: #{cert_purchase.related_type.inspect} #{cert_purchase.related_id.inspect}"
        end
        "Certificate already used #{usedby}"
      end
    end
  end
end
