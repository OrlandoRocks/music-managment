# [2010-03-08 -- CH]
# modified applicable_for_discount to get the purchased_item for single test

module Tunecore
  module CertEngine
    class DefaultSinglePercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for a one-year single only.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.purchased_item.instance_of?(Single) && one_year_single_ids.include?(purchase.product_id)
      end

      private

      def one_year_single_ids
        Product::PRODUCT_COUNTRY_MAP.map { |k,v| v[:one_year_single] }
      end
    end
  end
end
