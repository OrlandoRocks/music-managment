module Tunecore
  module CertEngine
    class SpawningPercentRisingArtist < SpawningBase
      def self.instructions
        %Q!  A spawning cert giving a fixed percentage discount from Rising Artist plan. Discount % is set in the engine params !
      end
  
      def applicable_for_discount?(purchase)
        person_plan = PersonPlan.find_by(person_id: purchase.person_id)
        rising_artist_plan_ids = Product.where(display_name: "rising_artist").pluck(:id)
        rising_artist_plan_ids.include?(purchase.purchased_item.id) && (person_plan.nil? || person_plan.plan.id == Plan::NEW_ARTIST)
      end
  
      def discount_amount_cents(price_calculator)
        (price_calculator.purchase_cost_cents.to_f * (cert.engine_params.to_f / 100)).to_i
      end
    end
  end
end
