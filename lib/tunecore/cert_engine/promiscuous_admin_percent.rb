module Tunecore
  module CertEngine
    class PromiscuousAdminPercent < SpawningBase
  
      # this is a promiscuous cert engine - so there are *no* checks if it's been previously spawned
      def detect_previously_spawned(person_id, purchase)
        nil
      end
  
      def discount_amount_cents(price_calculator)
        (price_calculator.purchase_cost_cents.to_f * (cert.engine_params.to_f / 100)).to_i
      end

      # this is the message that will be displayed to explain the discount
      def details
        "Customer Service - #{cert.engine_params.to_i}% off."
      end

      def self.instructions
        %Q!  A PROMISCUOUS spawning cert giving a fixed percentage discount from the invoice total.  Discount % is set in the engine params.  FOR ADMIN USE ONLY. ! 
      end

    end
  end
end