# Engine for Guitar Center Distribution credit
# acceptable params is album, single, ringtone
module Tunecore
  module CertEngine
    class DefaultRedemption < OneTimeBase
      VALID_PARAMS = {:album => 'album', :single => 'single', :ringtone => 'ringtone'}
      REDEEMABLE_PRODUCTS = {
        :album => Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID,
        :single => Product::US_ONE_SINGLE_CREDIT_PRODUCT_ID,
        :ringtone => Product::US_ONE_RINGTONE_CREDIT_PRODUCT_ID
      }
      DISCOUNTABLE_PRODUCTS = {
        :album => [Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID, Product::US_ONE_YEAR_ALBUM_PRODUCT_ID],
        :single => [Product::US_ONE_SINGLE_CREDIT_PRODUCT_ID, Product::US_ONE_YEAR_SINGLE_PRODUCT_ID],
        :ringtone => [Product::US_ONE_RINGTONE_CREDIT_PRODUCT_ID, Product::US_ONE_YEAR_RINGTONE_PRODUCT_ID]
      }
      
      def allow_product_redemption?
        true
      end

      def discount_amount_cents(price_calculator)
        price_calculator.purchase_cost_cents
      end

      # this is the message that will be displayed to the customer to explain the discount
      def details
        "Redemption Certificate - 1 x #{sanitized_engine_params}."
      end

      def self.instructions
        "A onetime cert giving distribution credit to Album, Single or Ringtone. The type of distribution credit is set in the engine params: #{valid_params.join(', ')}"
      end
      
      def self.valid_params
        return VALID_PARAMS.collect { |k,v| v }
      end
      
      # Given the cert, return the product that it is valid for based on the engine_params
      def redeemable_product
        product_id = case sanitized_engine_params
          when VALID_PARAMS[:album]
             REDEEMABLE_PRODUCTS[:album]
          when VALID_PARAMS[:single]
             REDEEMABLE_PRODUCTS[:single]
          when VALID_PARAMS[:ringtone]
            REDEEMABLE_PRODUCTS[:ringtone]
        end
        return Product.find(product_id)
      end
      
      def applicable_for_discount?(purchase)
        case sanitized_engine_params
          when VALID_PARAMS[:album]
            DISCOUNTABLE_PRODUCTS[:album].include?(purchase.product_id)
          when VALID_PARAMS[:single]
            DISCOUNTABLE_PRODUCTS[:single].include?(purchase.product_id)
          when VALID_PARAMS[:ringtone]
            DISCOUNTABLE_PRODUCTS[:ringtone].include?(purchase.product_id)
          else
            false
        end
      end
      
      # validations that gets run
      def validations
        validate_valid_params
      end
      
    private
      def sanitized_engine_params
        cert.engine_params.downcase
      end
    
      def validate_valid_params
        unless self.class.valid_params.include?(sanitized_engine_params)
          cert.errors.add(:engine_params, "must be one of the followings: #{self.class.valid_params.join(', ')}")
        end
      end

    end
  end
end