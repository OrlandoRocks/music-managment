module Tunecore
  module CertEngine
    class SpawningPercentSingle < SpawningBase

      def detect_previously_spawned(_person_id, _purchase)
        nil
      end

      def discount_amount_cents(price_calculator)
        (price_calculator.purchase_cost_cents.to_f * (cert.engine_params.to_f / 100)).to_i
      end

      def applicable_for_discount?(purchase)
        product_ids = []
        product_countries = Product::PRODUCT_COUNTRY_MAP.keys

        product_countries.each do |country|
          product_ids << Product::PRODUCT_COUNTRY_MAP[country][:one_year_single]
        end

        Product.find(product_ids).include?(purchase.product)
      end

      def details
        "#{cert.engine_params.to_i}% off a Single distribution."
      end

      def self.instructions
        %Q!  A spawning cert giving a fixed percentage discount from one Single distribution.  Discount % is set in the engine params !
      end
    end
  end
end
