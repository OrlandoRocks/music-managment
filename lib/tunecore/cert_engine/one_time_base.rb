# Base class for all one-time (non-spawning) certs, handles the one-time-usage
# aspects.  Actual discount calculation should be handled in subclasses
module Tunecore
  module CertEngine
    class OneTimeBase
      attr_reader :cert
      def initialize(cert)
        @cert = cert
      end

      def spawning?
        false
      end

      # Override the applicable_for_discount in subclasses for special item certs like singles
      def applicable_for_discount?(purchase)
        true
      end



      def verify(person_id, purchase)
        raise ArgumentError, "expected a Purchase object" unless purchase.is_a?(Purchase)

        if cert.expiry_date < Time.now
          return "Certificate expired at: #{cert.expiry_date.strftime('%d-%b-%Y %l:%M %p')}"
        end

        unless cert.date_used.blank?
          if cert.person_id == person_id && (cert_purchase = Purchase.find_by(id: cert.purchase_id))
            usedby = " by: #{cert_purchase.related.name}"
          end
          return "Certificate already used#{usedby}"
        end

        cert.purchase = purchase
        cert.person_id = person_id
        cert.date_used = Time.now
        if cert.save
          cert
        else
          "There was a problem using the cert"
        end
      end
    end
  end
end