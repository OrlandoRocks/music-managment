module Tunecore
  module CertEngine
    class SongwriterPercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for songwriter service.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.purchased_item.instance_of?(Composer) || purchase.purchased_item.instance_of?(PublishingComposer)
      end
    end
  end
end
