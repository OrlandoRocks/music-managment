# Engine for Alfred Publishing distribution redemption
# acceptable params is album, single, ringtone
module Tunecore
  module CertEngine
    class OurstageDistro < DefaultRedemption
      
      # this is the message that will be displayed to the customer to explain the discount
      def details
        "OurStage Distribution Redemption Certificate - 1 x #{sanitized_engine_params}."
      end

      def self.instructions
        "A onetime OurStage cert giving distribution credit to Album, Single or Ringtone. The type of distribution credit is set in the engine params: album, single or ringtone"
      end
    end
  end
end