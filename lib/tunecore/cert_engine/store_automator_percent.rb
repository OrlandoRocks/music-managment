module Tunecore
  module CertEngine
    class StoreAutomatorPercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for Store Automator.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.product.name == 'Store Automator'
      end
    end
  end
end
