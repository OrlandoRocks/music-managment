module Tunecore
  module CertEngine
    class YoutubeMonetizationPercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for youtube monetization service.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.purchased_item.instance_of?(YoutubeMonetization)
      end
    end
  end
end
