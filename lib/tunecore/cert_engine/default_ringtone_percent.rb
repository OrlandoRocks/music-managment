module Tunecore
  module CertEngine
    class DefaultRingtonePercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for a one-year ringtone only.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.purchased_item.instance_of?(Ringtone) && one_year_ringtone_ids.include?(purchase.product_id)
      end

      private

      def one_year_ringtone_ids
        Product::PRODUCT_COUNTRY_MAP.map { |k,v| v[:one_year_ringtone] }
      end
    end
  end
end
