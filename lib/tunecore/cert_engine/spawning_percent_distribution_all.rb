module Tunecore
  module CertEngine
    class SpawningPercentDistributionAll < SpawningBase

      def detect_previously_spawned(person_id, purchase); end

      def discount_amount_cents(price_calculator)
        (price_calculator.purchase_cost_cents.to_f * (cert.engine_params.to_f / 100)).to_i
      end

      def applicable_for_discount?(purchase)
        product_countries = Product::PRODUCT_COUNTRY_MAP
        release_types     = ["album", "single", "ringtone"].freeze

        product_ids = product_countries.keys.map do |country|
          release_types.map do |release_type|
            [
              product_countries[country]["one_#{release_type}_credit".to_sym],
              product_countries[country]["one_year_#{release_type}".to_sym],
              product_countries[country][:ringtone_distribution]
            ]
          end
        end.flatten.compact
        Product.where(id: product_ids).include?(purchase.product)
      end

      def self.instructions
        %Q!  A spawning cert giving a fixed percentage discount from one Album, Single, Ringtone or Distribution Credit. Discount % is set in the engine params. This discount does not apply to packs of Distribution Credits. !
      end
    end
  end
end
