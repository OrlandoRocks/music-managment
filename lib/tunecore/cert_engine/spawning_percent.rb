module Tunecore
  module CertEngine
    class SpawningPercent < SpawningBase
  
      def discount_amount_cents(price_calculator)
        (price_calculator.purchase_cost_cents.to_f * (cert.engine_params.to_f / 100)).to_i
      end

      # this is the message that will be displayed to explain the discount
      def details
        "Certificate - #{cert.engine_params.to_i}% off."
      end

      def self.instructions
        %Q!  A spawning cert giving a fixed percentage discount from the invoice total.  Discount % is set in the engine params !
      end

    end
  end
end