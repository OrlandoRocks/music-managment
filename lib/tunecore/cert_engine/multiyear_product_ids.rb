module Tunecore
  module CertEngine
    module MultiyearProductIds
      def multi_year_product_ids
        ids = []

        ["five", "ten", "twenty"].each do |duration|
          ["album", "single", "ringtone"].each do |type|
            ids += Product::PRODUCT_COUNTRY_MAP.map { |_k, v| v["#{duration}_#{type}_credits".to_sym] }
          end
        end

        ids += Product::PRODUCT_COUNTRY_MAP.map { |_k, v| v[:three_ringtone_credits] }

        CountryWebsite.all.each do |country_website|
          ["TWO", "FIVE"].each do |duration|
            ["ALBUM", "SINGLE"].each do |type|
              ids << "Product::#{country_website.country}_#{duration}_YEAR_#{type}_PRODUCT_ID".constantize
            end
          end
        end

        ids
      end
    end
  end
end
