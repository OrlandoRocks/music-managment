# Engine for Alfred Publishing distribution redemption
# acceptable params is album, single, ringtone
module Tunecore
  module CertEngine
    class AlfredDistro < DefaultRedemption
      
      # this is the message that will be displayed to the customer to explain the discount
      def details
        "Alfred Publishing Distribution Redemption Certificate - 1 x #{sanitized_engine_params}."
      end

      def self.instructions
        "A onetime Alfred Publishing cert giving distribution credit to Album or Single. The type of distribution credit is set in the engine params: album or single"
      end
    end
  end
end