module Tunecore
  module CertEngine
    class DefaultRenewalPercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for any renewal product.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.product.product_type == 'Renewal'
      end
    end
  end
end
