# This is the calculation logic for the gc_brochure engine'd certs.
#
# They provide the following discount characteristics:
# 5 free songs, 
# 1 free store, 
# album's base fee (initial startup) also free

module Tunecore
  module CertEngine
    class GcBrochure < OneTimeBase

      def discount_amount_cents(price_calculator)
        max_free_stores = [price_calculator.chargeable_salepoints_count, 1].min
        max_free_songs = [price_calculator.chargeable_songs_count, 5].min
    
        price_calculator.base_cost_cents +
        (max_free_stores * PricePolicy[:store].base_price_cents).to_i +
        (max_free_songs * PricePolicy[:song].base_price_cents).to_i
      end

      # this is the message that will be displayed to explain the discount
      def details
        "Up to 5 Free Songs, 1 Free Store, and no initial Album Storage Fee."
      end

      def self.instructions
        "Up to 5 Free Songs, 1 Free Store, and no initial Album Storage Fee."
      end


    end
  end
end
