module Tunecore
  module CertEngine
    class DefaultAlbumPercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for a one-year album only.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        purchase.purchased_item.instance_of?(Album) && one_year_album_ids.include?(purchase.product_id)
      end

      private

      def one_year_album_ids
        Product::PRODUCT_COUNTRY_MAP.map { |k,v| v[:one_year_album] }
      end
    end
  end
end
