module Tunecore
  module CertEngine
    class FreeStores < OneTimeBase

      def discount_amount_cents(price_calculator)
        ([price_calculator.chargeable_salepoints_count, cert.engine_params.to_i].min * PricePolicy[:store].base_price_cents).to_i
      end

      # this is the message that will be displayed to explain the discount
      def details
        numstores = cert.engine_params.to_i
        if numstores == 1
          "1 Free Store."
        else
          "Up to #{numstores} Free Stores."
        end
      end

      def self.instructions
        %Q!  Onetime cert giving a certain number of stores for free. Number of free stores is set in the engine params !
      end

    end
  end
end