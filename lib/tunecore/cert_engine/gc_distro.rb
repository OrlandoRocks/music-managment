# Engine for Guitar Center Distribution credit
# acceptable params is album, single, ringtone
module Tunecore
  module CertEngine
    class GcDistro < DefaultRedemption
      # this is the message that will be displayed to the customer to explain the discount
      def details
        "Guitar Center Distribution Credit Certificate - 1 x #{sanitized_engine_params}."
      end

      def self.instructions
        "A onetime cert giving distribution credit to Album, Single or Ringtone. The type of distribution credit is set in the engine params: #{valid_params.join(', ')}"
      end
      
    end
  end
end