module Tunecore
  module CertEngine
    class DefaultAlbumSinglePercent < DefaultPercent
      def self.instructions
        %Q!  A onetime cert giving a fixed percentage discount for a one-year album or single only.  Discount % is set in the engine params !
      end

      def applicable_for_discount?(purchase)
        eligible_product_type(purchase) &&
        eligible_product_ids.include?(purchase.product_id)
      end

      private

      def eligible_product_type(purchase)
        purchase.purchased_item.instance_of?(Album) ||
        purchase.purchased_item.instance_of?(Single)
      end

      def eligible_product_ids
        [:one_year_album, :one_year_single].each_with_object([]) do | product_type, product_ids |
          product_ids << Product::PRODUCT_COUNTRY_MAP.map{ |_, v| v[product_type] }
        end.flatten
      end
    end
  end
end
