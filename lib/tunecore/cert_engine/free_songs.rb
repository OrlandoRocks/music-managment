module Tunecore
  module CertEngine
    class FreeSongs < SpawningBase

      def discount_amount_cents(price_calculator)
        ([price_calculator.chargeable_songs_count, cert.engine_params.to_i].min * PricePolicy[:song].base_price_cents).to_i
      end

      # this is the message that will be displayed to explain the discount
      def details
        "Guitar Center - up to #{cert.engine_params.to_i} free songs."
      end

      def self.instructions
        %Q!  A spawning cert giving a certain number of free songs. Number of free songs is set in the engine params !
      end

    end
  end
end