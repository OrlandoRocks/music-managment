module Tunecore
  module CrtReviewable
    def pop_album_for_review
      Album.select([a_table[Arel.star], album_count_for_person.as("total_albums")])
           .joins(skipped_or_recently_started_reviews_by_others)
           .where(album_not_taken_down)
           .where(album_needs_review)
           .where(ra_table[:id].eq(nil))
           .where(album_finalized)
           .group(a_table[:id])
           .order("finalized_at asc")
           .includes(:person, :tunecore_upcs, :artwork, {
                       songs: [
                         :s3_asset,
                         {
                           creative_song_roles: [:song_role, { creative: :artist }],
                           creatives: :artist
                         }
                       ]
                     }, creatives: :artist, review_audits: [:review_reasons, :person], salepoints: :store)
           .first
    end

    def albums_left_to_review
      Album.select(a_table[:id])
           .joins(skipped_or_recently_started_reviews)
           .where(album_not_taken_down)
           .where(album_needs_review.or(album_skipped_review))
           .where(a_table[:person_id].eq(id))
           .where(ra_table[:id].eq(nil))
           .where(album_finalized)
           .group(a_table[:id])
           .length
    end

    def total_albums_left_to_review_for_agent
      Album.select(a_table[:id])
           .joins(skipped_or_recently_started_reviews_by_others)
           .where(album_not_taken_down)
           .where(album_needs_review.or(album_skipped_review))
           .where(ra_table[:id].eq(nil))
           .where(album_finalized)
           .group(a_table[:id])
           .length
    end

    private

    def a_table
      Album.arel_table
    end

    def ra_table
      ReviewAudit.arel_table
    end

    def review_started
      ra_table[:event].eq("STARTED REVIEW")
    end

    def not_created_by_person
      ra_table[:person_id].not_eq(id)
    end

    def review_started_over_30_mins_ago
      ra_table[:created_at].gt(30.minutes.ago.localtime.strftime("%Y-%m-%d %H:%M:%S"))
    end

    def review_started_over_2_hours_ago
      ra_table[:created_at].gt(2.hours.ago.localtime.strftime("%Y-%m-%d %H:%M:%S"))
    end

    def skipped_review
      ra_table[:event].eq("SKIPPED")
    end

    def started_review
      ra_table[:event].eq("STARTED REVIEW")
    end

    def album_needs_review
      a_table[:legal_review_state].eq("NEEDS REVIEW")
    end

    def album_skipped_review
      a_table[:legal_review_state].eq("SKIPPED")
    end

    def album_finalized
      a_table[:finalized_at].not_eq(nil)
    end

    def album_not_taken_down
      a_table[:takedown_at].eq(nil)
    end

    def skipped_or_recently_started_reviews_by_others
      on_condition = ra_table.create_on(
        ra_table[:album_id].eq(a_table[:id])
        .and(
          review_started.and(review_started_over_2_hours_ago.and(not_created_by_person))
          .or(skipped_review)
        )
      )
      ra_table.create_join(
        ra_table,
        on_condition,
        Arel::Nodes::OuterJoin
      )
    end

    def skipped_or_recently_started_reviews
      on_condition = ra_table.create_on(
        ra_table[:album_id].eq(a_table[:id])
        .and(
          started_review.and(review_started_over_2_hours_ago)
          .or(skipped_review)
        )
      )
      ra_table.create_join(
        ra_table,
        on_condition,
        Arel::Nodes::OuterJoin
      )
    end

    def album_count_for_person
      a_table.project(a_table[:id].count).where(a_table[:person_id].eq(id))
    end
  end
end
