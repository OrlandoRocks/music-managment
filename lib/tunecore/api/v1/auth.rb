module Tunecore
  module Api
    module V1
      class Auth
        def self.valid_key?(key)
          ApiKey.where(key: key).any?
        end
      end
    end
  end
end
