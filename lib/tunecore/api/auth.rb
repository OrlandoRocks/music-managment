module Tunecore
  module Api
    module Auth
      def self.valid_key?(params)
        service = params[:controller].split('/').last
        service = 'sip_vat' if %w[vat_posting generate_invoice].include? params[:action]
        config_key = !API_CONFIG[service].nil? ? API_CONFIG[service]["API_KEY"] : API_CONFIG["API_KEY"]
        !params[:api_key].nil? && (params[:api_key] == config_key)
      end

      def self.valid_account?(email, status = "Active")
        !Person.where(email: email, status: status, deleted: false).empty?
      end
    end
  end
end
