module Tunecore
  module DistributableChecklist

    CHECKLIST_STEPS = [
      "has_artwork?",
      "has_tracks?",
      "has_stores?",
      "has_artwork_template_info?",
      "already_finalized?"
    ].freeze

    def can_distribute?
      has_artwork? && has_tracks? && has_stores? && has_artwork_template_info?
    end

    def ready_to_purchase?
      !!(can_distribute? && !finalized?)
    end

    # Returns the number of steps that are still required to be finished before the user
    # can deliver their album
    def number_of_steps_required
      CHECKLIST_STEPS.reject { |x| self.send(x) }.size
    end

    def permit_song_uploads?
      @permit_song_uploads ||= !(finalized?)
    end

    def permit_metadata_changes?
      !finalized?
    end

    def permit_song_changes?
      @permit_song_changes ||= !(finalized? || payment_applied?)
    end

    def permit_salepoint_changes?
      @permit_salepoint_changes ||= (finalized? || ! payment_applied?)
    end

    def has_required_elements?
      has_artwork? && has_tracks? && has_stores? && has_artwork_template_info? && has_primary_artist?
    end

    def already_paid?
      (finalized? || payment_applied?)
    end

    def has_artwork?
      @has_artwork ||= !!(artwork && artwork.last_errors.blank? && artwork.uploaded)
    end

    def has_primary_artist?
      primary_artists.any? || is_various? && songs.joins(:creatives).exists?(creatives: { role: "primary_artist" })
    end

    def has_tracks?
      has_songs? && has_uploads? && songs_pass_validation?
    end

    def has_songs?
      @has_songs ||= songs.any?
    end

    # 2011-04-07 AK -- removed the check for errors on Song, this is now done by bigbox
    def has_uploads?
      @has_uploads ||= songs.all? do |song|
        !!(song.upload)
      end
    end

    def songs_pass_validation?
      return false if songwriter? && !songs_have_songwriters?

      if is_various?
        songs.all? do |song|
          !song.primary_artists.blank?
        end
      else
        return true
      end
    end

    def songs_have_songwriters?
      CreativeSongRole
        .joins(:song_role, :song)
        .where(
          songs: { album_id: id },
          song_roles: { role_type: "songwriter" }
        ).group(:song_id).length == song_ids.length
    end

    def has_stores?
      @has_stores ||= (salepoints.any? && stores_with_variable_pricing.all? {|s| s.has_chosen_price_code?} && has_stores_other_than_believe_and_streaming?)
    end

    # move these to Salepoint model
    # into Salepoint.needs_variable_pricing?(Album)
    def stores_with_variable_pricing
      salepoints.includes(store: :variable_prices).select{|sp| sp.store.variable_pricing_required?}
    end

    def has_stores_with_variable_pricing?
      stores_with_variable_pricing.any?
    end

    # move these to Salepoint model
    # into Salepoint.needs_artowrk_template?(Album)
    def stores_with_artwork_templates
      salepoints.select{|sp| sp.requires_template?}
    end

    def has_artwork_template_info?
      stores_with_artwork_templates.all? {|s| s.has_chosen_template?}
    end

    # move into Inventory Usage Thing +++++++++++++
    def should_finalize?
      !already_finalized? && has_required_elements? && valid?
    end

    def already_finalized?
      @already_finalized ||= finalized?
    end

    def has_stores_other_than_believe_and_streaming?
      salepoints.select{|sp| !['BelieveLiv', 'Streaming'].include?(sp.store.short_name)}.present?
    end
  end
end
