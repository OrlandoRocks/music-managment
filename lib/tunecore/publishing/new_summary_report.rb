module Tunecore
  module Publishing
    module NewSummaryReport
      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods
        def generate_summary_xls
          filename = summary_temp_file_path

          time = Benchmark.realtime do
            publishing_composers = composer_summary
            albums_finalized_count = PublishingComposer.albums_finalized_count
            albums_with_splits_count = PublishingComposer.albums_with_splits_count
            title = ['PID', 'Acct. Status', 'First Name', 'Last Name', 'SW ID', 'Email', 'Phone', 'Agreed', 'Paid', 'SSN?', '# Splits', '# Rel', 'Last Splits']

            SimpleXlsx::Serializer.new(filename) do |doc|
              doc.add_sheet('Composer Summary') do |sheet|
                sheet.add_row(title)

                publishing_composers.each do |publishing_composer|
                  sheet.add_row(
                    [
                      publishing_composer.person_id,
                      publishing_composer.person_status,
                      publishing_composer.first_name,
                      publishing_composer.last_name,
                      publishing_composer.id,
                      publishing_composer.email ? publishing_composer.email : "(#{publishing_composer.person_email})",
                      publishing_composer.phone ? publishing_composer.phone : "(#{publishing_composer.person_phone_number})",
                      I18n.l(publishing_composer.agreed_to_terms_at, format: :slash_short_year),
                      publishing_composer.purchase_paid_at ? I18n.l(publishing_composer.purchase_paid_at, format: :slash_short_year) : nil,
                      publishing_composer.submitted_tax_id.blank? ? 'No' : 'Yes',
                      (result = albums_with_splits_count.detect { |a| a.id == publishing_composer.id }) ? result.albums_count : 0,
                      (result = albums_finalized_count.detect { |a| a.id == publishing_composer.id }) ? result.albums_count : 0,
                      publishing_composer.splits_updated_at.blank? ? '' : I18n.l(publishing_composer.splits_updated_at, format: :slash_short_year)
                    ]
                  )
                end
              end
            end
          end

          write_file_to_s3(PUBLISHING_REPORTS_BUCKET_NAME, summary_filename, File.read(filename))

          logger.info "generate_summary_xls completed in #{time} seconds"

          remove_temp_summaries

          filename
        end

        def remove_temp_summaries
          file_pattern = "#{Rails.root}/tmp/composer_summary*"
          old_files = `find #{file_pattern}`.split("\n")
          old_files.each { |f| FileUtils.rm_rf(f) }
        end

        def summary_filename
          "composer_summary.xls"
        end

        def summary_temp_file_path
          Rails.root + "tmp/#{summary_filename}"
        end

        def summary_last_modified
          last_modified = file_last_modified(PUBLISHING_REPORTS_BUCKET_NAME, summary_filename)
          return I18n.l(last_modified.localtime, format: :slash_short_year_with_time) if last_modified.present?
        end

        def summary_exists?
          file_last_modified(PUBLISHING_REPORTS_BUCKET_NAME, summary_filename).present?
        end

        def summary_download_path
          s3_object = AWS::S3.new(:access_key_id => AWS_ACCESS_KEY, :secret_access_key => AWS_SECRET_KEY)
          bucket = s3_object.buckets[PUBLISHING_REPORTS_BUCKET_NAME]
          bucket.objects[summary_filename].url_for(:read, :expires => 30)
        end

        def composer_summary
          PublishingComposer.primary.find_by_sql "(SELECT DISTINCT publishing_composers.*, publishing_composition_splits.updated_at as splits_updated_at, people.status as person_status, people.id as person_id,
            people.email as person_email, people.phone_number as person_phone_number,
            purchases.paid_at as purchase_paid_at, tax_info.encrypted_tax_id as submitted_tax_id
            FROM `publishing_composers` inner join people on publishing_composers.person_id = people.id
            left outer join tax_info on publishing_composers.id = tax_info.publishing_composer_id
            left outer join purchases on publishing_composers.id = purchases.related_id and purchases.related_type = 'PublishingComposer'
            inner join
            (
              select publishing_composition_splits.publishing_composer_id, max(publishing_composition_splits.updated_at) as updated_at
              from publishing_composition_splits
              inner join publishing_compositions
              on publishing_composition_splits.`publishing_composition_id` = publishing_compositions.id
              where publishing_compositions.is_unallocated_sum = 0
              group by publishing_composition_splits.publishing_composer_id
            )
            publishing_composition_splits on publishing_composers.id = publishing_composition_splits.publishing_composer_id)
            UNION
            (SELECT DISTINCT publishing_composers.*, NULL as splits_updated_at, people.status as person_status, people.id as person_id,
            people.email as person_email, people.phone_number as person_phone_number,
            purchases.paid_at as purchase_paid_at, tax_info.encrypted_tax_id as submitted_tax_id
            FROM `publishing_composers` inner join people on publishing_composers.person_id = people.id
            left outer join tax_info on publishing_composers.id = tax_info.publishing_composer_id
            left outer join purchases on publishing_composers.id = purchases.related_id and purchases.related_type = 'PublishingComposer'
            WHERE NOT EXISTS
            (
              select publishing_composition_splits.publishing_composer_id, max(publishing_composition_splits.updated_at) as splits_updated_at
              from publishing_composition_splits
              inner join publishing_compositions
              on publishing_composition_splits.`publishing_composition_id` = publishing_compositions.id
              where publishing_composers.id = publishing_composition_splits.publishing_composer_id
              AND publishing_compositions.is_unallocated_sum = 0
              group by publishing_composition_splits.publishing_composer_id
            )) order by splits_updated_at desc"
        end

        def write_file_to_s3(bucket_name, filename, data)
          AwsWrapper::S3.write(
            bucket: bucket_name,
            key: filename,
            data: data,
            options: {
              cache_control: "no-cache",
              acl: :authenticated_read,
              content_type: "application/vnd.ms-excel"
            }
          )
        end

        def file_last_modified(bucket_name, filename)
          AwsWrapper::S3.metadata(
            bucket: bucket_name,
            key: filename
          )[:last_modified]
        end
      end
    end
  end
end
