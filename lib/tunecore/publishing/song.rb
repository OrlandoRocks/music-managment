module Tunecore
  module Publishing
    module Song
      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods
        # find songs with composition that we administer 100%
        def get_administered_muma_compositions
          MumaSong.where("muma_songs.mech_collect_share = 100 AND muma_song_ips.capacity_code = 'CA'").includes(:muma_song_ips)
        end
      end

      ## INSTANCE METHODS ##
      def muma_composition
        compositions = muma_songs.includes(:muma_song_ips)
        # there should only be one muma_song associated with a song
        # if there are more, we would need to filter out the bad records
        # and only return the one true composition associated to the song
        if compositions.size > 1
          composition = compositions.detect { |c| c.isrc == isrc }
        else
          composition = compositions.first
        end

        return composition
      end

    end
  end
end