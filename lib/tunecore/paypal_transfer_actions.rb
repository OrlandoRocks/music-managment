#
# = Description
#
#   Provide actions for processing Paypal Transfers
#   and setting up control and redirect logic
#   for edge cases
#
#
# = History
#
#   Module extracted from MyAccount Controller
#   to bring down the controller complexity
#
#
#
module Tunecore
  module PaypalTransferActions
    include PayoutHelper

    def withdraw_paypal
      reject_withdrawal_by_admin if current_user_is_taken_over?
      redirect_to(action: 'withdraw', controller: 'my_account') unless show_legacy_options?
      @compliance_info_form ||= ComplianceInfoForm.new(@person || current_user)

      if request.post?
        create_paypal_transfer

        if @withdrawal.new_record?
          render :withdraw_paypal
        else
          redirect_to(:action => 'list_transactions', :controller => 'my_account')
        end
      else
        @withdrawal = PaypalTransfer.new
      end
    end

    protected

    def create_paypal_transfer
      flash.discard
      withdrawal_params = params[:withdrawal]
      withdrawal_params[:payment_amount] ||= Tunecore::CurrencyInputNormalizer.normalize_currency_input(
        withdrawal_params,
        :payment_amount_main_currency,
        :payment_amount_fractional_currency
      )
      @withdrawal = PaypalTransfer.generate(current_user, withdrawal_params)
      if @withdrawal.errors.empty?
        save_compliance_info
        payment_amount = params[:withdrawal][:payment_amount]
        paypal_address = params[:withdrawal][:paypal_address]
        flash[:notice] = "Confirming a PayPal transfer of #{MONEY_CONFIG[current_user.person_balance.currency][:unit]}#{payment_amount} within 3-4 business days to #{paypal_address}"
      end
    end

    def reject_withdrawal_by_admin
      flash[:error] = "Admin personnel cannot withdraw money."
      redirect_to(:controller => 'my_account', :action => 'list_transactions')
    end

  end
end
