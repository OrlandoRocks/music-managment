#
#  Tunecore::CsvExporter
#
#  A module to add the correct headers to send csv files
#  based on the browser.
#
#  Intended to be included in an ActionController
#
module Tunecore
  module CsvExporter
    def set_csv_headers(filename)
      if is_microsoft_user_agent?
        microsoft_csv_headers(filename)
      else
        non_microsoft_csv_headers(filename)
      end
    end

    def microsoft_csv_headers(filename)
      headers['Pragma'] = 'public'
      headers["Content-type"] = "text/plain"
      # headers['Cache-Control'] = 'no-cache, must-revalidate, post-check=0, pre-check=0'
      headers['Cache-Control'] = 'max-age=0'
      headers['Content-Disposition'] = "attachment; filename=\"#{filename}\""
      headers['Expires'] = "0"
    end

    def non_microsoft_csv_headers(filename)
      headers["Content-Type"] ||= 'text/csv'
      headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
    end

    def is_microsoft_user_agent?
      request.env['HTTP_USER_AGENT'] =~ /msie/i
    end

    def set_streaming_headers
      headers['X-Accel-Buffering'] = 'no'

      headers["Cache-Control"] ||= "no-cache"
      headers.delete("Content-Length")
    end

  end
end
