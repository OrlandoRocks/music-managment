module Tunecore
  module AdminReports
    module DistributionReportDataFields
      include Tunecore::AdminReports::AdminReportDataFieldsBase

      def total_distributions
        attributes['total_distributions'].to_i
      end
    end
  end
end
