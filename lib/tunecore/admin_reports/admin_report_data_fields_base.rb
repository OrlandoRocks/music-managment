module Tunecore
  module AdminReports
    module AdminReportDataFieldsBase
      def resolution
        attributes['resolution'] || "none"
      end
    end
  end
end