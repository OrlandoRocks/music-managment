# To handle the behavior of redeeming cert for a product without having to go through the checkout process
module Tunecore
  module Certs
    class UnableToCheckoutException < Exception
      def message
        "There is an issue with the redemption. The product and certificate has already been added and applied to your cart. Proceed with checkout."
      end
    end

    module ProductRedemption
      def redeem!
        begin
          invoice = Invoice.factory(person, purchase)
          raise "invoice not created!" if invoice.blank?
          invoice.settled!
        rescue StandardError => e
          # FIXME: Get text for error message when invoice.settled! failed for whatever reason
          logger.debug e.message
          raise Tunecore::Certs::UnableToCheckoutException
        end
      end

      # flag on whether customer can redeem the cert for a product
      def allow_product_redemption?
        return engine.allow_product_redemption? rescue false
      end
    end
  end
end
