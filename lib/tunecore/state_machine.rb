module Tunecore
  module StateMachine
    def audit(params)
      old_state = self.state
      yield
      return unless @state_changed == true
      now = Time.now
      params = {:actor => "Mystery Actor"}.merge(params)
      if backtrace = params[:backtrace]
        backtrace = backtrace.kind_of?(Array) ? backtrace.join(";") : backtrace
      end
      self.updated_at = now
      self.transitions.create({
        :actor => params[:actor],
        :message => params[:message],
        :backtrace => backtrace,
        :old_state => old_state,
        :new_state => self.state,
        :created_at => now
      })
    end

  end
end
