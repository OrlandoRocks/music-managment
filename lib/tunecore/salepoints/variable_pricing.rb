module Tunecore::Salepoints
  module VariablePricing

    ADMIN_ELIGIBLE_PRICING_STORE_IDS = [13, 36, 28] # amazon music, itunes, google play

    def set_implicit_default_variable_price
      implicit_price = Store.get_implicit_default_variable_price(self.store, self.salepointable.class.name)
      self.variable_price = implicit_price unless implicit_price.nil?
    end

    def set_implicit_track_variable_price
      implicit_price = Store.get_implicit_track_variable_price(self.store)
      self.track_variable_price = implicit_price unless implicit_price.nil?
    end

    def variable_price_options
      self.store.variable_prices.by_salepointable_type(salepointable.class.name)
    end

    def track_variable_price_options
      self.store.variable_prices.by_salepointable_type('Song')
    end

    def valid_variable_price
      if store_has_variable_pricing?
        unless [*store.variable_prices].include?(variable_price) || (variable_price.nil? && !finalized?)
          errors.add('variable_price', "#{I18n.t('model.salepoint.variable_price_error', store_name: store.name)}")
        end
      end
    end

    def store_has_variable_pricing?
      self.store.variable_pricing_required?
    end

    #return the price_code/text of the variable_price or nil if there is no variable pricing selected
    def price_code
      if ::PriceCodeStrategy::Tunecore.available_strategy?(self.store.short_name.capitalize)
        "PriceCodeStrategy::#{self.store.short_name.capitalize}".constantize.price_code(self)
      else
        variable_price ? variable_price.price_code : nil
      end
    end

    #return the price_code/text of the track level variable_price or nil if there is no variable pricing selected
    def track_price_code
      track_variable_price ? track_variable_price.price_code : nil
    end

    def price_code_id
      self.variable_price ? self.variable_price.price_code.id : nil
    end

    def has_chosen_price_code?
      return true unless store_has_variable_pricing?
      price_code ? true : false
    end

    def self.salepoints_eligible_for_admin_update(album)
      album.salepoints.select { |salepoint| ADMIN_ELIGIBLE_PRICING_STORE_IDS.include?(salepoint.store.id) }
    end

    def has_variable_price_selection?
      variable_price_id.present?
    end

    def validate_variable_price_selection
      return if !store.has_variable_pricing?

      if store.variable_pricing_required? && !has_variable_price_selection?
        self.errors.add :distribution, "#{I18n.t('model.salepoint.variable_price_selection_error', store_name: store.name)}"
      end
    end
  end
end
