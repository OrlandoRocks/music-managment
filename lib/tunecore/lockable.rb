module Tunecore
  module Lockable
    class << self
      def included(base)
        return if ! base.table_exists?

        unless base.columns_hash['locked_by_id'] && base.columns_hash['locked_by_type']
          base.logger.error "locked_by_id or locked_by_type column not present in #{base.table_name.inspect} table"
        end

        unless base.columns_hash['person_id'] # must belong to a person...
          base.logger.error "person_id column not present in #{base.table_name.inspect} table"
        end
        base.belongs_to :locked_by, :polymorphic => true
      end
    end

    def obtain_lock(entity)
      raise RuntimeError, "Unable to lock with instance of #{entity.class.name}" unless entity.is_a?(LockableLock)
      return false if locked? && locked_by_type != "PetriBundle"

      update_attribute( :locked_by, entity )
    end

    def release_lock
      if locked_by && !locked_by.okay_to_release?(self)
        return false
      end

      calculate_steps_required if (respond_to?(:calculate_steps_required) && locked_by == 'Bundle')
      update_attribute( :locked_by, nil )
    end

    def editable_by?(person_entity)
      case locked_by
      when Person
        person_entity == locked_by
      when nil
        person_entity == person
      else
        false
      end
    end

    def locked?
      locked_by.present?
    end
  end
end
