
module Tunecore
  class StringDateParser
    DATE_FORMAT = "%B %d, %Y"

    def self.to_s(date)
      date.strftime(DATE_FORMAT) if date
    end

    def self.to_date(date_string)
      result = nil
      begin
        result = Date.strptime(date_string, DATE_FORMAT)
      rescue
        # do nothing
      end
      result
    end

    def self.error_message
      "Enter date in [Month] [Day], [Year] format.  example: #{Time.now.strftime(DATE_FORMAT)}"
    end

    private

    def initialize
    end
  end
end
