class Tunecore::DateTranslator
  def self.translate(date, format, to_locale, from_locale = I18n.locale)
    new(date, format, to_locale, from_locale).translate_date
  end

  def self.parse_date(date, format, to_locale, from_locale = I18n.locale)
    new(date, format, to_locale, from_locale).parse_date
  end

  def initialize(date, format, to_locale, from_locale = I18n.locale)
    @untranslated_date     = date
    @untranslated_month    = @untranslated_date[/[^\s\d,\.]+/i]
    @format                = format
    @localized_date_format = I18n.t("date.formats")[@format.to_sym]
    @english_date_format   = I18n.t("date.formats.#{@format}", locale: :en)
    @to_locale             = to_locale
    @from_locale           = from_locale
    find_month_match
  end

  def translate_to_english
    return @untranslated_date if english_date?

    if date_format_for(@from_locale).index(@untranslated_month)
      month_index = date_format_for(@from_locale).index(@untranslated_month)
    else
      month_index = date_format_for(@to_locale).index(@untranslated_month)
    end

    translated_month = date_format_for(@to_locale)[month_index]
    translated_date  = @untranslated_date.gsub(@untranslated_month, translated_month)
  end

  def translate_date
    begin
      I18n.l(parse_date, format: I18n.t("date.formats.#{@format}", locale: @to_locale), locale: @to_locale).squish
    rescue => e
      Rails.logger.info("Translation of date string failed due to: #{e.message}")
      @untranslated_date
    end
  end

  def parse_date
    if found_match? && english_date?
      date_string = @untranslated_date
      date_format = @english_date_format
    elsif found_match?
      date_string =  translate_to_english
      date_format = @localized_date_format
    else
      date_string = @untranslated_date
      date_format = @localized_date_format
    end

    Date.strptime(date_string, date_format)
  end

  private

  def found_match?
    !!@month_match
  end

  def english_date?
    @from_locale == :en
  end

  def date_format_for(locale)
    I18n.t(match_map[@month_match], locale: locale, scope: [:date])
  end

  def find_month_match
    @month_match = match_map.keys.detect { |regex| @localized_date_format =~ regex }
  end

  def match_map
    {
      /%B/ => "month_names",
      /%b/ => "abbr_month_names",
    }
  end
end
