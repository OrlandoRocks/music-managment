#
#  Tunecore::Numbers
#
#  A module to convert numbers due to the variation of 
#  how we store amounts (cents and decimals)
#
module Tunecore
  module Numbers
    class << self
      def cents_to_decimal(amount_in_cents)
        BigDecimal("#{amount_in_cents / 100.00}")
      end

      def decimal_to_cents(amount_in_decimal)
        (amount_in_decimal * 100).to_i
      end
    end
  end
end
