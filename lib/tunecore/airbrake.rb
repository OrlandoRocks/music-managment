module Tunecore
  module Airbrake
    def self.notify(exception, options = {})
      airbrake_exception = exception.is_a?(String) ? Exception.exception(exception) : exception
      ::Airbrake.notify(airbrake_exception, options)
    end
  end
end
