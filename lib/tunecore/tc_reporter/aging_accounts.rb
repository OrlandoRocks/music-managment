module Tunecore::TcReporter
  module  AgingAccounts
    def self.s3_links
      links = {}
      S3_CLIENT.buckets[s3_bucket].objects.each do |o|
        links[o.url_for(:read, :expires => 3600).to_s] = o.key
      end
      links
    end

    def self.create_report(params, user)
     ::TcReporter::ApiClient.post(
        "/api/aging_accounts/",
        {
          user_ids: params[:user_ids].strip,
          period: "#{params[:date][:month]}-#{params[:date][:year]}"
        },
        user
      )
    end

    def self.s3_bucket
      "tc.reporter.aging-accounts"
    end
  end
end
