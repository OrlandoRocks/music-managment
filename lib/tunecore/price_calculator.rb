module Tunecore
  module PriceCalculator
    class << self
      def of(purchase, purchaseable)
        klass = purchase.product.price_policy.calculator_class
        klass.new(purchase, purchaseable)
      end
    
      def class_of(calculator_string)
        PriceCalculator.const_get(calculator_string.classify.to_sym)
      end
    end
  end
end
