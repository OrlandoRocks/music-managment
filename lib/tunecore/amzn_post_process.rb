# Post processes Amazon album XML files before they are summarized in Manifest.xml
require "rexml/document"
include REXML
class AmznPostProcess

  attr_accessor :xml_filename

  # Creates a processor instance
  def initialize( xml_filename )
    @xml_filename = xml_filename
    load_xml_file
  end

   #Loads the file that the object was initialized with
  def load_xml_file
    raise "No XML file specified" if @xml_filename.nil?
    file = File.new( @xml_filename, "r" ) #open the file for reading only
    @doc = Document.new file
    file.close
  rescue => e
    raise "Failed to load XML file #{@xml_filename}: " + e.message
  end

  #Nubmer of tracks for this album
  def track_count
    return XPath.match( @doc, "//ALBUM/TRACK").size
  end

  # Sets the albums delivery type.  Default is but sometimes we need to do a UPDATE_ONLY
  def delivery_type( d_type )
    element = XPath.first( @doc, "//ALBUM/ALBUM_INFO/DELIVERY_TYPE"  )
    element.text = d_type
  end

  #Marks a single track as "album only"
  def album_only( track_num )
    #find the appropriate track element
    track = XPath.match( @doc, "//ALBUM/TRACK")[ track_num - 1 ]
    album_only_node = Element.new("ALBUM_ONLY")
    #Create the ALBUM_ONLY node if it doesn't exist
    if track.elements["TRACK_TERRITORY"].elements["ALBUM_ONLY"] == nil
      # insert it in the right place
      composition_clearance_node = track.elements["TRACK_TERRITORY"].elements["COMPOSITION_CLEARANCE"]
      track.elements["TRACK_TERRITORY"].insert_after( composition_clearance_node, album_only_node )
    end

    #Set the ALBUM_ONLY node to true
    track.elements["TRACK_TERRITORY"].elements["ALBUM_ONLY"].text = "true"
  end

  # Writes the new XML file
  def write( new_filename=@xml_filename )
    file = File.new( new_filename, "w")
    @doc.write file
    file.close
    return true
  end

end
