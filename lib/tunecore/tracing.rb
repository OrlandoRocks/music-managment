module Tunecore
  module Tracing
    # Only enable tracing when the `TRACING` env var is configured
    def self.enabled?
      connection_string.present?
    end

    def self.service_name
      # Distinguish traces originating from console
      return "#{ENV['DEPLOY_ENV']}-console" if defined?(Rails::Console)

      ENV['DEPLOY_ENV']
    end

    def self.connection_string
      ENV['TRACING']
    end
  end
end
