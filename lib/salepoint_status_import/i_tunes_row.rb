module SalepointStatusImport
  class ITunesRow
    attr_accessor :upc, :reported_status, :row_number, :album
    protected :upc=, :reported_status=, :row_number=

    def initialize(row_number, upc, reported_status)
      self.row_number = row_number
      self.upc = upc
      self.reported_status = reported_status
    end

    def status
      case self.reported_status
      when 'Live'
        'live'
      when 'Hidden'
        'hidden'
      when 'In Review', 'Imported', 'Importing', 'Uploading [DAV]', 'Ready To Import'
        'pending'
      when 'Import Error'
        'error'
      else
        'garbled'
      end
    end

    def garbled?
      'garbled' == self.status
    end

  end
end
