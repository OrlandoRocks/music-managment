module AdminResource
  def check_admin_login
    return access_forbidden() unless current_user.is_administrator?
  end

  def load_person
    return access_forbidden() unless current_user.is_administrator?
    @person = Person.find(params[:person_id])
  end
  protected :load_person

end
