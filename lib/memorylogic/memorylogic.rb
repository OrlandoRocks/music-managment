module Memorylogic
  def self.included(klass)
    klass.class_eval do
      after_action :log_memory_usage
    end
  end

  private
    def log_memory_usage
      if logger and !Rails.env.test?
        memory_usage = `ps -o rss= -p #{$$}`.to_i
        logger.info("Memory usage: #{memory_usage} | PID: #{$$}")
      end
    end
end
