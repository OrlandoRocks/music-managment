class PaginationWithSelectLinkRenderer < WillPaginate::ActionView::LinkRenderer

  #
  # Outputs pagination links as a select box
  #
  def to_html
    elements = []
    elements << "<span>Page</span>"

    if @options[:page_links]
      # Add page links as a select box
      elements << @template.content_tag(:select, nil, :class => "select_page") do

        # Add each page number to select box
        (1..total_pages).collect do |page|
          options = {}
          options[:selected]="selected" if page == current_page
          @template.content_tag(:option, page, options)
        end.join.html_safe

      end
    end

    elements << "<span>of #{total_pages}</span>"

    # previous/next buttons
    elements << previous_or_next_page(@collection.previous_page, @options.delete(:previous_label), 'prev_page')
    elements << previous_or_next_page(@collection.next_page, @options.delete(:next_label), 'next_page')

    html = elements.join(@options[:link_separator])
    html = html.html_safe if html.respond_to? :html_safe

    @options[:container] ? html_container(html) :html
  end

end
