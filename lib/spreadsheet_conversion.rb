module SpreadsheetConversion

  def self.check_for_converter
    raise "can't find conversion program xls2csv, is catdoc installed?" unless `xls2csv -V` =~ /Catdoc Version/i
    true
  end
  
  def self.convert_to_csv(filename)
    self.check_for_converter
    raise "can't find file to convert" unless File.readable? filename    
    raise "filename doesn't have .xls extension" unless filename =~ /\.xls$/
    targetname = filename.sub(/.xls$/,".csv")
    `xls2csv -b '' #{filename} 2> /dev/null > #{targetname}`
    raise "failed to create csv file" unless File.readable?(targetname)
    targetname
  end

end

