# frozen_string_literal: true

module VatAssesmentAddressBuilder
  FIELDS = Set[
    :address1,
    :address2,
    :city,
    :corporate_entity_id,
    :country,
    :customer_type,
    :postal_code,
    :state
  ]

  def complete_address_for(user)
    addr_hash = address_hash_for(user)
                .merge(compliance_info: compliance_info_hash_for(user))

    format(addr_hash)
  end

  def format(address)
    slice_compliance_info(address)
      .deep_transform_values { |value| value.to_s.downcase }
      .with_indifferent_access
  end

  private

  def address_hash_for(user)
    FIELDS
      .index_with { |key| user[key] }
      .tap { |addr| addr[:postal_code] = user.postal_code }
  end

  def compliance_info_hash_for(user)
    ComplianceInfoFields::MetaDataService
      .display(user)
      .transform_values { |value| value[:field_value] }
  end

  def compliance_field_set(customer_type)
    ComplianceInfoForm::
      FIELD_DEPENDENCY_MAP[customer_type.titlecase]
      &.map(&:to_sym)
  end

  def slice_compliance_info(address)
    customer_type = address[:customer_type]
    fields = compliance_field_set(customer_type)

    address
      .except(:compliance_info)
      .tap do |addr|
        addr[:compliance_info] = address[:compliance_info].slice(*fields)
      end
  end

  def address_values_updated?(addr_hash)
    preset_keys = %w[corporate_entity_id country customer_type]
    hash = addr_hash.except(*preset_keys)

    !blank_hash? hash
  end

  def blank_hash?(hash)
    hash.each_value do |v|
      case v
      when Hash
        return false unless blank_hash? v
      else
        return false if v.present?
      end
    end

    true
  end
end
