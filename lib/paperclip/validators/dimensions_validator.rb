module Paperclip
  module Validators
    class DimensionsValidator < ActiveModel::EachValidator
      def initialize(options)
        super
      end

      def self.helper_method_name
        :validates_attachment_dimensions
      end

      def validate_each(record, attribute, value)
        return unless value.queued_for_write[:original]

        begin
          dimensions = Paperclip::Geometry.from_file(value.queued_for_write[:original].path)
          validate_min_dimensions(record, attribute, dimensions)
          validate_dimensions_shape(record, attribute, dimensions)
        rescue Paperclip::Errors::NotIdentifiedByImageMagickError
          Paperclip.log("cannot validate dimensions on #{attribute}")
        end
      end

      def validate_min_dimensions(record, attribute, dimensions)
        [:height, :width].each do |dimension|
          if options[dimension] && dimensions.send(dimension) < options[:min][dimension].to_f
            record.errors.add(attribute.to_sym, "image must be at least #{options[:min][:width]}x#{options[:min][:height]}")
          end
        end
      end

      def validate_dimensions_shape(record, attribute, dimensions)
        if options[:shape] == "square"
          record.errors.add(attribute.to_sym, "shape must be a square") if dimensions.send(:height) != dimensions.send(:width)
        end
      end
    end

    module HelperMethods
      def validates_attachment_dimensions(*attr_names)
        options = _merge_attributes(attr_names)
        validates_with(DimensionsValidator, options.dup)
        validate_before_processing(DimensionsValidator, options.dup)
      end
    end
  end
end
