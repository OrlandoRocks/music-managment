#
#  A module to contain additional logging methods
#
module ActionLogger

  def self.included(base)
    base.prepend_before_action :log_session_id
    base.prepend_before_action :log_person_id
  end

  protected

  #
  #  Log the session id out into the Rails log, to be monitored by 
  #  scripting metrics
  #
  def log_session_id
    logger.info "session_id: #{cookies[:tunecore_id]}" if cookies[:tunecore_id]
  end

  def log_person_id
    logger.info "person_id: #{session[:person]}" if session[:person]
  end

end
