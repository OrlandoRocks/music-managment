class ItunesLinkinator

  #unencoded itunes link: http://itunes.apple.com/album/id#{album.apple_identifier}
  def self.linkshare_link_for(apple_id)
    %Q!http://click.linksynergy.com/fs-bin/stat?id=eH8NOY8fBFE&offerid=78941&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fphobos.apple.com%252FWebObjects%252FMZStore.woa%252Fwa%252FviewAlbum%253Fid%253D#{apple_id}%2526s%253D143441%2526partnerId%253D30!.html_safe
  end

  def self.itunes_url(album)
    return "" unless album.apple_identifier
    %Q!http://itunes.apple.com/album/id#{album.apple_identifier}?ls=1&app=itunes!.html_safe
  end

  def self.itunes_raw_link(album)
    return "" unless album.apple_identifier
    %Q!<a href="http://itunes.apple.com/album/id#{album.apple_identifier}?ls=1&app=itunes"><img height="15" width="61" alt="#{album.name}" src="http://www.tunecore.com/images/buttons/badgeitunes61x15dark.gif" /></a>!.html_safe
  end

  def self.link_for(apple_id, alt_text = "")
    return "" unless apple_id
    %Q!<a href="#{self.linkshare_link_for(apple_id)}"><img height="15" width="61" alt="#{alt_text}" src="http://www.tunecore.com/images/buttons/badgeitunes61x15dark.gif" /></a>!.html_safe
  end

  def self.link_for_album_with_text(album, text="View your music on iTunes")
    return "" unless album.apple_identifier
    %Q!<a href="#{self.linkshare_link_for(album.apple_identifier)}" target="_blank">#{text}</a>!.html_safe
  end

  def self.itunes_streaming_url(album)
    return "" unless album.apple_identifier
    %Q!http://itunes.apple.com/album/id#{album.apple_identifier}!.html_safe
  end
end
