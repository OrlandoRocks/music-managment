class ActiveRecord::AttributeAssignment::MultiparameterAttribute
  prepend WithAmAndPm

  def instantiate_time_object(name, values)
    if self.class.send(:create_time_zone_conversion_attribute?, name, column_for_attribute(name))
      Time.zone.local(*values)
    else
      Time.time_with_datetime_fallback(self.class.default_timezone, *values)
    end
  end
end

module ActionView::Helpers
  class DateTimeSelector
    prepend WithAmAndPm

    POSITION = remove_const(:POSITION).merge(:ampm => 7)
    # XXX would like to do this, but it's frozen
    # POSITION[:ampm] = 7

    # We give them negative values so can differentiate between normal
    # date/time values. The way the multi param stuff works, from what I
    # can see, results in a variable number of fields (if you tell it to
    # include seconds, for example). So we expect the AM/PM field, if
    # present, to be last and have a negative value.
    AM = -1
    PM = -2

    def select_ampm
      selected = hour < 12 ? AM : PM

      # XXX i18n?
      label = { AM => 'AM', PM => 'PM' }
      ampm_options = []
      [AM, PM].each do |meridiem|
        option = { :value => meridiem }
        option[:selected] = "selected" if selected == meridiem
        ampm_options << content_tag(:option, label[meridiem], option) + "\n"
      end
      build_select(:ampm, ampm_options.join)
    end
  end
end
