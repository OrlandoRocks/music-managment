# frozen_string_literal: true

module AccountSystem
  CONFIG = {
    email_from: 'TuneCore <noreplies@tunecore.com>',
    email_from_for_copyright_issues: 'TuneCore <copyright@tunecore.com>',
    email_from_for_legal_issues: 'TuneCore <contentreview@tunecore.com>',
    email_from_for_streaming_issues: 'TuneCore <streaming@tunecore.com>',
    email_from_for_takedown: 'TuneCore <content@tunecore.com>',
    admin_email:                  'ops@tunecore.com',
    admin_publishing_email:       ['troy@tunecore.com','julie@tunecore.com'],
    admin_youtube_export_email:   ["youtube_notifications@tunecore.com"],
    app_url:                      'http://www.tunecore.com/',
    app_name:                     'TuneCore',
    mail_charset:                 'utf-8',
    security_token_life_hours:    24,
    two_column_input:             false,
    changeable_fields:            ['name'],
    delayed_delete:               false,
    delayed_delete_days:          7,
    server_env:                   "#{Rails.env}"
  }.freeze

  def self.config
    CONFIG
  end

  # accesses the current user from the session.
  # overwrite this to set how the current user is retrieved from the session.
  # To store just the whole user model in the session:
  #
  #   def current_user
  #     session[:user]
  #   end
  #
  def current_user
    return @current_user if defined?(@current_user)

    @current_user = session[:person] ? Person.find_by(id: session[:person]) : nil
    @current_user.takeover_admin_id = session[:admin] if @current_user.present? && session[:admin]

    clear_login_flash if @current_user.present?

    cleanup_path and return if completed_shared_login?
  end

  protected

  def logout
    self.current_user = nil
    redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
  end

  def logged_in?
    current_user.present?
  end

  def not_logged_in?
    current_user.nil?
  end

  def under_admin_control?
    current_admin.present?
  end

  def current_admin
    @current_admin ||= session[:admin] ? Person.find_by(id: session[:admin]) : nil
  end

  def current_admin=(user)
    if user.is_administrator?
      session[:admin] = user.id
    end
  end

  # store the given user in the session.  overwrite this to set how
  # users are stored in the session.  To store the whole user model, do:
  #
  #   def current_user=(new_user)
  #     session[:user] = new_user
  #   end
  #
  def current_user=(new_user)
    session[:person] = new_user.nil? ? nil : new_user.id
    @current_user = new_user
  end

  # overwrite this if you want to restrict access to only a few actions
  # or if you want to check if the person has the correct rights
  # example:
  #sy
  #  # only allow nonbobs
  #  def authorized?
  #    person.login != "bob"
  #  end
  def authorized?
    return true if under_admin_control?
    if current_user.account_locked?
      self.current_user = nil
      return false
    else
      return true
    end
  end

  # overwrite this method if you only want to protect certain actions of the controller
  # example:
  #
  #  # don't protect the login and the about method
  #  def protect?(action)
  #    if ['action', 'about'].include?(action)
  #       return false
  #    else
  #       return true
  #    end
  #  end
  def protect?(action)
    true
  end

  # login_required filter. add
  #
  #   before_action :login_required
  #
  # if the controller should be under any rights management.
  # for finer access control you can overwrite
  #
  #   def authorize?(person)
  #
  def login_required
    # store current location so that we can
    # come back after the user logged in
    store_location

    # skip login check if action is not protected
    return true unless protect?(action_name)

    # check if user is logged in and authorized

    if !logged_in?
      flash[:error] = custom_t("must_login") unless api_auth_login?
      access_denied
      return false
    elsif !authorized?
      flash[:error] = custom_t("account_locked")
      access_denied
      return false
    else
      return true
    end
  end

  def login_disallowed
    redirect_to dashboard_path if logged_in?
  end

  def api_auth_login?
    request.path == oauth_authorize_path
  end

  # overwrite if you want to have special behavior in case the person is not authorized
  # to access the current operation.
  # the default action is to redirect to the login screen
  # example use :
  # a popup window might just close itself for instance
  def access_denied
    redirect_to(login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1))
  end

  def access_forbidden
    render :file => "#{Rails.root}/public/403.html", :layout => false, :status => :forbidden
    false
  end

  # we're already at the login prompt, remove the prompt param so that we're not re-prompted
  def remove_prompt(fullpath)
    fullpath.gsub('&prompt=true', '')
  end

  # store current uri in the session.
  # we can return to this location by calling return_location
  def store_location
    return_to = remove_prompt(request.fullpath)
    session[:return_to] = return_to
  end

  # move to the last store_location call or to the passed default one
  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def current_user_is_taken_over?
    current_user && session[:admin] && (session[:admin] != current_user.id)
  end

  def completed_shared_login?
    @current_user.present? && request.params[:nonce].present?
  end

  def clear_login_flash
    return unless session.to_h.dig(:flash, "flashes").present?

    session[:flash]["flashes"].reject! { |_k, v| v == "You must be logged in to access that page." }
  end

  # Clear the nonce and user from the URL
  def cleanup_path
    redirect_to request.path
  end
end
