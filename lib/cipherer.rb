# Utilizing OpenSSL Cipher for a symmetric key encryption for storing sensitive
# information in the database.
#
# REF: https://gist.github.com/wteuber/5318013
# REF: https://crypto.stackexchange.com/questions/42621/best-choice-out-of-these-six-tls-cipher-suites
# REF: https://ruby-doc.org/stdlib-2.4.0/libdoc/openssl/rdoc/OpenSSL/Cipher.html#class-OpenSSL::Cipher-label-Authenticated+Encryption+and+Associated+Data+-28AEAD-29
#
# Examples:
#
# cipherer = Cipherer.new(SomeStrategy.new(key: key, iv: iv))
#
# encryption_artifact = cipherer.encrypt("Some string")
# cipherer.decrypt(encryption_artifact.encrypted_string)
# => "Some string"
#
# -- OR you may pass in values from the returned encryption artifact into a new strategy/cipherer instance--
# Cipherer.new(SomeStrategy.new(key: encryption_artifact.key, iv: encryption_artifact.iv)).decrypt(encrypted)
# => "Some string"
#
class Cipherer
  class Migrator
    def initialize(old_strategy:, new_strategy:)
      @old_strategy = old_strategy
      @new_strategy = new_strategy
    end

    def migrate(encrypted_value)
      new_strategy.encrypt(old_strategy.decrypt(encrypted_value))
    end

    private

    attr_reader :old_strategy, :new_strategy
  end

  AUTH_DATA = "cipherer"

  def initialize(strategy:)
    @strategy = strategy
  end

  def encrypt(string)
    strategy.encrypt(string)
  end

  def decrypt(string)
    strategy.decrypt(string)
  end

  private

  attr_reader :strategy
end
