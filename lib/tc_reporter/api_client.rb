module TcReporter
  class ApiClient

    def self.post(endpoint, call_args = {}, person)
      response = $tc_reporter_client.post(endpoint, call_args)
      TcReporterAudit.create(
          api_response: {status: response.status, body: response.body},
          api_params: call_args,
          report: endpoint,
          person: person
      )
      response.status == 200
    rescue  Faraday::Error::ConnectionFailed => e
      Rails.logger.error("TC REPORTER IS NOT RESPONDING: #{e.message}")
      false
    end

  end
end
