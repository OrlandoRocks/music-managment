# frozen_string_literal: true

require "net/http"

module Api::Iterable
  def self.api_post(endpoint, body)
    uri = URI.parse("https://api.iterable.com/api/" + String(endpoint))

    Net::HTTP.start(uri.host, use_ssl: true) do |https|
      request = Net::HTTP::Post.new(uri)
      request["Api-Key"] = ENV["ITERABLE_API_KEY"]
      request["Content-Type"] = "application/json"
      request.body = Oj.dump(Hash(body))

      https.request(request)
    end
  end

  module Email
    def self.send(campaign_name, recipient_email, data_fields, async = true)
      campaign_id = IterableCampaign.find_by_name(campaign_name)&.campaign_id
      raise "Could not find an IterableCampaign by the name of '#{campaign_name}'." if campaign_id.nil?

      return IterableEmailWorker.perform_async(campaign_id, recipient_email, Oj.dump(data_fields)) if async

      Api::Iterable.api_post(
        "email/target",
        {
          campaignId: Integer(campaign_id),
          recipientEmail: String(recipient_email),
          dataFields: Hash(data_fields)
        }
      )
    end
  end
end
