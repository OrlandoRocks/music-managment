class AdminConstraint
  def matches?(request)
    session_hash = request.cookie_jar["tc_active_session"]
    if session_hash
      session_json = JSON.parse(SessionEncryptionEngine.decrypt(session_hash))
      if session_json
        user = Person.find(session_json["customer_id"])
        user.is_administrator?
      else
        return false
      end
    else
      false
    end
  rescue
    false
  end
end
