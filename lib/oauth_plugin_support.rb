module OauthPluginSupport
  # Rails 6.0 upgrade:
  # GS-1407: remove this module once this ticket addresses the issue
  def self.included(klass)
    klass.extend(ClassMethods)
  end

  module ClassMethods
    # Rails 6.0 upgrade:
    # aliasing of before_filter needed in order to allow the oauth-plugin gem to work
    def before_filter(*filters, &block)
      before_action(*filters, &block)
    end
  end

  def invalid_oauth_response(code = 401, message = "Invalid OAuth Request")
    begin
      super
    rescue
      render json: { text: message }, status: code
      false
    end
  end
end
