module VerifyWithProductRedemption
  module ClassMethods
    DOES_NOT_ACCEPT_PROMOTION_CODE = "The code you entered was not recognized as a distribution redemption code purchased from a TuneCore partner.<br/>If you tried to use a promotion code,  please enter that at check out and it will be applied to your order."
    MANY_FAILED_ATTEMPTS = "Redemption entry locked after too many failed attempts. Either contact Artist Support at tunecore.com/contact or try again in 4 hours."
    PROMOTION_NOT_VALID_FOR_COUNTRY = "This promotion is not available for your country."

    # check the type of cert
    def verify(options)
      product_redemption = options[:product_redemption]

      if product_redemption
        #
        #  FIXME: For product redemption, cert's find is getting run twice since
        #  it will be lookup again when the original method gets run
        #
        cert = validate_cert(options[:entered_code])
        current_user = options[:current_user]

        if cert.blank?
          message = mark_invalid_redemption_attempt(current_user)
          return message if message
          return DOES_NOT_ACCEPT_PROMOTION_CODE
        end

        if cert.allow_product_redemption?
          raise ArgumentError, "expected a User" unless current_user
          return MANY_FAILED_ATTEMPTS if current_user.redemption_locked?

          purchase = add_redemption_product(cert, current_user)
          if purchase.errors.empty?
            options.merge!(:purchase => purchase)
            verify_result = super(options)
            # rollback the purchase when cert verification failed
            #FIXME: If there are same product in the cart, the product will not be added to the cart
            # but will be removed since that purchase id(when there is open purchase) is returned
            rollback_purchase(purchase) unless verify_result.is_a?(Cert)
          else
            verify_result = handle_purchase_error(purchase)
          end
        else
          return DOES_NOT_ACCEPT_PROMOTION_CODE
        end
      else
        verify_result = super(options)
      end

      return verify_result
    end

    private

    def add_redemption_product(cert, current_user)
      # check the product allowed, put the product in the cart
      product = cert.engine.redeemable_product
      #FIXME: If there is purchase in the cart with the same product, add to cart will not add the product
      purchase = Product.add_to_cart(current_user, product)
    end

    def rollback_purchase(purchase)
      purchase.destroy
    end

    def validate_cert(entered_code)
      code, issuer = code_and_issuer(entered_code)
      find_by(cert: code)
    end

    def mark_invalid_redemption_attempt(current_user)
      current_user.increment_invalid_redemption_count
      return MANY_FAILED_ATTEMPTS if current_user.redemption_locked?
    end

    def handle_purchase_error(purchase)
      if purchase.errors.full_messages.join(' ') =~ /not available for your country/
        return PROMOTION_NOT_VALID_FOR_COUNTRY
      else
        return purchase.errors.full_messages.join(' ')
      end
    end
  end

  def self.prepended(base)
    class << base
      prepend ClassMethods
    end
  end
end
