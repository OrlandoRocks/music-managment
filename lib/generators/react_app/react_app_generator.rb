class ReactAppGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  argument :reducer_names, type: :array, default: []

  def create_react_app
    webpack_file = "app/javascript_apps/webpack.config.js"
    inject_into_file webpack_file, after: "entry: {\n" do
      <<-JS
    #{name}: './#{name}/app.js',
      JS
    end

    empty_directory "app/javascript_apps/#{folder_name}"

    template "app.erb", "app/javascript_apps/#{folder_name}/app.js"
  end

  def create_base_component
    empty_directory "app/javascript_apps/#{folder_name}/components"

    template "container.erb", "app/javascript_apps/#{folder_name}/components/#{class_name}Container.js"
  end

  def create_base_redux_store
    empty_directory "app/javascript_apps/#{folder_name}/store"

    template "configureStore.erb", "app/javascript_apps/#{folder_name}/store/configureStore.js"
    template "initialState.erb", "app/javascript_apps/#{folder_name}/store/initialState.js"
    template "schema.erb", "app/javascript_apps/#{folder_name}/store/schema.js"

    empty_directory "app/javascript_apps/#{folder_name}/store/initialStates"

    reducer_names.each do |reducer_name|
      @reducer_name = reducer_name
      @reducer_variable = reducer_name.singularize.camelize(:lower)
      template "initialStateExample.erb", "app/javascript_apps/#{folder_name}/store/initialStates/#{reducer_name.pluralize.camelize(:lower)}.js"
    end
  end

  def create_reducers
    empty_directory "app/javascript_apps/#{folder_name}/reducers"

    template "reducersIndex.erb", "app/javascript_apps/#{folder_name}/reducers/index.js"

    reducer_names.each do |reducer_name|
      @reducer_name = reducer_name
      @reducer_variable = reducer_name.singularize.camelize(:lower)
      template "reducersExample.erb","app/javascript_apps/#{folder_name}/reducers/#{reducer_name.pluralize.camelize(:lower)}Reducer.js"
    end
  end

  def create_actions
    empty_directory "app/javascript_apps/#{folder_name}/actions"

    template "actionTypes.erb", "app/javascript_apps/#{folder_name}/actions/actionTypes.js"

    reducer_names.each do |reducer_name|
      @reducer_name = reducer_name
      @reducer_variable = reducer_name.singularize.camelize(:lower)
      template "actionExample.erb", "app/javascript_apps/#{folder_name}/actions/#{reducer_name.pluralize.camelize(:lower)}Actions.js"
    end
  end

  def create_specs
    empty_directory "app/javascript_apps/spec/#{folder_name}/integration"

    template "specFile.erb", "app/javascript_apps/spec/#{folder_name}/integration/doesAThing.spec.js"
  end

  def folder_name
    name.underscore
  end

  def class_name
    name.classify
  end
end
