class AdminPageGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  argument :routes, type: :array, default: [:index, :new, :create]
  class_option :service_method, type: :string

  def create_backend_files
    template "admin_page_form.erb", "app/forms/#{file_name}_form.rb"
    template "admin_page_service.erb", "app/services/#{file_name}_service.rb"
    template "admin_page_controller.erb", "app/controllers/admin/#{plural_name}_controller.rb"
  end

  def create_frontend_files
    empty_directory "app/views/admin/#{plural_name}"

    routes.each do |route|
      template "#{route}.erb", "app/views/admin/#{plural_name}/#{route}.html.erb"
    end
    template "_form.erb", "app/views/admin/#{plural_name}/_form.html.erb"
  end

  def create_assets
    template "stylesheet.css.erb", "app/assets/stylesheets/admin_foundation/#{file_name}.css.scss"
    append_to_file "app/assets/stylesheets/admin_foundation.css.scss", "@import \"admin_foundation/#{file_name}\";\n"
    template "javascript.js.erb", "app/assets/javascripts/admin/#{file_name}.js"
    append_to_file "app/assets/javascripts/admin/foundation.js", "//= require ./#{file_name}\n"
  end

  def create_route
    inject_into_file 'config/routes.rb', after: "namespace :admin do\n" do
      <<-RUBY
    resources :#{plural_name}, only: [:#{routes.map(&:to_sym).join(", :")}]
      RUBY
    end
  end

  private

  def controller_method_for(route)
    send("#{route}_method_contents")
  end

  def index_method_contents
    <<-METHOD_CONTENT
  def index
  end
    METHOD_CONTENT
  end

  def show_method_contents
    <<-METHOD_CONTENT
  def show
  end
    METHOD_CONTENT
  end

  def destroy_method_contents
    <<-METHOD_CONTENT
  def destroy
  end
    METHOD_CONTENT
  end

  def new_method_contents
    <<-METHOD_CONTENT
  def new
  end
    METHOD_CONTENT
  end

  def edit_method_contents
    <<-METHOD_CONTENT
  def edit
  end
    METHOD_CONTENT
  end

  def create_method_contents
    <<-METHOD_CONTENT
  def create
    if @#{file_name}_form.save
      redirect_to :#{file_name}_path
    else
      render :new
    end
  end
    METHOD_CONTENT
  end

  def update_method_contents
    <<-METHOD_CONTENT
  def update
    if @#{file_name}_form.save
      redirect_to :#{file_name}_path
    else
      render :new
    end
  end
    METHOD_CONTENT
  end

  def service_method
    options['service_method'] || file_name.split("_").last.singularize
  end

  def hyphened_file_name
    file_name.gsub("_", "-")
  end
end
