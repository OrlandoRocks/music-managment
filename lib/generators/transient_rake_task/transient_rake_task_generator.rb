class TransientRakeTaskGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  argument :routes, type: :array, default: [:index, :new, :create]
  class_option :service_method, type: :string

  def create_task
    template "task.erb", "lib/tasks/transient/#{file_name}.rake"
  end
end
