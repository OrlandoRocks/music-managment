class Gtm::Dashboard
  def self.build_user_data(person, options)
    user_data = {}
    if options[:pb] == "fl"
      user_data[:page_name] = options[:page_name]
      user_data[:page_type] = "dashboard/first login"
    else
      user_data[:page_name] = options[:page_name]
      user_data[:page_type] = :dashboard
      user_data[:publishing_customer]   = person.paid_for_composer? ? "1" : "0"
      user_data[:distribution_customer] = person.has_finalized_release? ? "1" : "0"
      user_data[:facebook_sign_up]      = "1" if options[:facebook_sign_up].present?
    end
    user_data
  end
end
