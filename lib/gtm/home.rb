class Gtm::Home
  def self.build_user_data(person, options)
    user_data = {}
    user_data[:page_name] = options[:page_name]
    user_data[:page_type] = :home
    user_data
  end
end
