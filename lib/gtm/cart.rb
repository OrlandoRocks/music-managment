class Gtm::Cart
  def self.build_user_data(person, options)
    user_data = {}
    user_data[:page_name] = options[:page_name]
    user_data[:page_type] = :cart
    user_data[:order_grand_total] = 0
    options[:purchases].each do |purchase|
      user_data[:order_grand_total] += purchase.cost_cents
    end
    user_data[:order_grand_total] = (user_data[:order_grand_total].to_f/100).to_s
    user_data
  end
end
