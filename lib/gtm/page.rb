class Gtm::Page
  def initialize(person, options = {})
    @user_data = {}
    @person    = person
    @options   = options
    @page_type = options[:page_type] if options

    build_user_data
  end

  attr_accessor :person, :page_type, :user_data, :options

  private

  def build_user_data
    begin
      user_data[:currency_code] = options[:currency]

      if person
        current_plan = person.plan
        next_plan    = current_plan&.next_plan

        user_data.merge!({
          dashboard_state:          person.dashboard_state,
          logged_in:                "1",
          customer_id:              person.id.to_s,
          customer_email:           person.email,
          customer_country:         person.country.blank? ? "unknown" : person.country,
          has_credits:              person.has_distribution_credits_available? ? "1" : "0",
          accessed_social:          accessed_social,
          social_plan:              [plan_status.plan, plan_status.plan_expires_at],
          any_release_without_YTAT: person.releases_eligible_for_youtube_art_tracks_store? ? "1" : "0",
          current_plan_name:        current_plan&.name,
          current_plan_id:          current_plan&.id,
          next_eligible_plan_name:  next_plan&.name,
          next_eligible_plan_id:    next_plan&.id,
        })
      else
        user_data[:logged_in] = "0"
      end

      if page_type
        user_data.merge!(page_type.build_user_data(person, options))
      end
    rescue => e
      user_data.merge!(error: "#{page_type}.build_user_data failed", error_details: e.to_s)
    end

    user_data
  end

  def plan_status
    @plan_status ||= ::Social::PlanStatus.for(person)
  end

  def accessed_social
    @accessed_social ||= OauthToken.exists?(
      user_id: person.id,
      client_application_id: ClientApplication.tc_social.id
    )
  end
end
