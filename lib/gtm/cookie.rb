class Gtm::Cookie
  def self.build_user_data(person, options = {})
    user_data = {}
    user_data[:country_website_id]      = person.country_website_id
    user_data[:publishing_customer]     = person.paid_for_composer? ? "1" : "0"
    user_data[:distribution_customer]   = person.has_finalized_release? ? "1" : "0"
    user_data[:join_date]               = person.created_on.strftime("%Y-%m-%d")
    user_data
  end
end
