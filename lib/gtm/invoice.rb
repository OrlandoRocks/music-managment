class Gtm::Invoice
  # This method still needs significant refactoring, as its
  # pretty much a straght copy from the original procedural
  # method.  A further refinement of the abstraction might
  # be needed where you can plug in certain data points (Purchase, Invoice)
  # into any give Gtm 'page'
  def self.build_user_data(person, options)
    user_data                                 = {}
    user_data[:page_name]                     = options[:page_name]
    user_data[:page_type]                     = :invoice
    user_data[:contains_distribution_product] = options[:invoice].contains_distribution_product? ? "1" : "0"
    user_data[:first_time_distribution]       = options[:invoice].check_and_set_first_time_distribution_or_credit ? "1" : "0"
    user_data[:publishing_customer]           = person.paid_for_composer? ? "1" : "0"
    user_data[:distribution_customer]         = person.has_finalized_release? ? "1" : "0"
    user_data[:publishing_only_customer]      = person.paid_for_composer? && !options[:invoice].contains_distribution_product? && person.has_never_distributed? ? "1" : "0"
    user_data[:customer_state]                = person.state.blank? ? "unknown" : person.state
    user_data[:customer_city]                 = person.city.blank? ? "unknown" : person.city
    user_data[:customer_phone]                = !person.phone_number.blank? ? person.phone_number : (!person.mobile_number.blank? ? person.mobile_number : "unknown")
    user_data[:customer_postal_code]          = person.postal_code.blank? ? "unknown" : person.postal_code
    discount_cents                            = options[:invoice].purchases.sum(:discount_cents)
    user_data[:order_discount_amount]         = (discount_cents.to_f / 100).to_s
    user_data[:order_grand_total]             = (options[:invoice].final_settlement_amount_cents.to_f / 100).to_s
    user_data[:order_id]                      = options[:invoice].id.to_s

    #Set the payment types
    settlement_types = options[:invoice].invoice_settlements.pluck(:source_type)
    user_data[:order_payment_type] = settlement_types.collect do  |type|
      case type
      when PaypalTransaction.name
        "paypal"
      when PersonTransaction.name
        "balance"
      when BraintreeTransaction.name
        "credit_card"
      end
    end

    #Set the product variables
    user_data[:product_brand]                                  = []
    user_data[:product_category]                               = []
    user_data[:product_id]                                     = []
    user_data[:product_name]                                   = []
    user_data[:product_price]                                  = []
    user_data[:product_quantity]                               = []
    user_data[:product_discount]                               = []
    user_data[:ecommerce]                                      = {}
    user_data[:ecommerce][:purchase]                           = {}
    user_data[:ecommerce][:purchase]["actionField"]            = {}
    user_data[:ecommerce][:purchase]["actionField"][:id]       = user_data[:order_id]
    user_data[:ecommerce][:purchase]["actionField"][:revenue]  = user_data[:order_grand_total]
    user_data[:ecommerce][:purchase][:products] = []

    options[:invoice].purchases.group_by{ |p| p.product_id }.each do |product_id, purchases|
      purchase = purchases.first
      #NEW ecommerce tracking
      product_hash = {}
      product_hash[:name]     = purchase.product.name
      product_hash[:id]       = product_id.to_s
      product_hash[:price]    = (purchase.cost_cents.to_f / 100).to_s
      product_hash[:brand]    = "TuneCore"
      product_hash[:category] = purchase.product.name
      product_hash[:quantity] = purchases.count.to_s
      user_data[:ecommerce][:purchase][:products] << product_hash

      #OLD Tealium tracking
      user_data[:product_brand]    << product_hash[:brand]
      user_data[:product_id]       << product_hash[:id]
      user_data[:product_name]     << purchase.product.display_name_raw
      user_data[:product_price]    << product_hash[:price]
      user_data[:product_quantity] << product_hash[:quantity]
      user_data[:product_discount] << (purchase.discount_cents.to_f / 100).to_s
      user_data[:product_category] << product_hash[:category]
    end

    user_data
  end
end
