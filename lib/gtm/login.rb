class Gtm::Login
  def self.build_user_data(_person, options)
    user_data = {}
    user_data[:page_name] = options[:page_name]
    user_data[:page_type] = "Login Page"
    user_data
  end
end
