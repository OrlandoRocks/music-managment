module ParamsWithStandardization
  def query_params
    normalize_first_level_arrays(super)
  end

  def request_params
    normalize_first_level_arrays(super)
  end

  def normalize_first_level_arrays(params)
    normalized_params = params.dup

    params.each do |k,values|
      if values.is_a?(Array)
        values.each do |v|
          normalized_params["#{k}[]"]||=[]
          normalized_params["#{k}[]"] << v
        end
        normalized_params.delete(k)
      end
    end

    normalized_params
  end
end
