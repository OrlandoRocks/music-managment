class ReferAFriend::ApiClient
  def initialize config
    @http_connection = Faraday.new(url: config["HOST"])
    @http_connection.basic_auth(config["API_USERNAME"], config["API_PASSWORD"])
    @prefix = config["PREFIX"]
  end

  def post(data, post_url)
    response = @http_connection.post do |req|
      req.url "#{@prefix}#{post_url}"
      req.headers["Content-Type"] = "application/json"
      req.body                    = data.to_json
    end

    response.status == 201
  rescue => e
    Tunecore::Airbrake.notify(e)
    false
  end
end