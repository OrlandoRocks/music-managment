module ReferAFriend
  class PurchaseHandler
    attr_reader :invoice, :person, :new_customer

    def initialize(invoice, new_customer)
      @invoice      = invoice
      @person       = invoice.person
      @new_customer = new_customer
    end

    def self.prepare_data(invoice, new_customer)
      new(invoice, new_customer).prepare_data
    end

    def self.referral_code_key
      @referral_code_key ||= "fbuy_ref_code"
    end

    def prepare_data
      {
        order: {
          id:           "#{@invoice.id}",
          amount:       @invoice.settlement_amount_cents / 100.0,
          email:        @person.email,
          new_customer: @new_customer
        },
        referral_code:  @person.friend_referral_code,
        customer: {
          id:         "#{@person.id}",
          account_id: "#{@person.id}",
          email:      @person.email,
          first_name: @person.first_name,
          last_name:  @person.last_name
        },
        products:       prepare_products
      }
    end

    private

    def prepare_products
      @invoice.purchases.map do |purchase|
        {
          sku:      purchase.product.display_name_raw,
          price:    purchase.cost_cents / 100.0,
          quantity: 1
        }
      end
    end
  end
end
