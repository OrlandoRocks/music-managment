module RoyaltySolutions
  def self.validate_assets(params)
    @albums, @songs = get_assets(params)
    @event_date     = parse_event_date(params[:event_date])
    missing_upcs, missing_isrcs = missing_assets(params, @albums, @songs)
    create_response(missing_upcs, missing_isrcs, params[:event_date], @event_date)
  end

  def self.register(params)
    resp_data = validate_assets(params)
    return resp_data if resp_data[:status] == "failure"
    insert_new_tracking(@albums, @songs, params[:email], @event_date)
    resp_data
  end

  private

  def self.get_assets(params)
    matching_albums = Album.joins(:person, :upcs).where("people.email = ? AND upcs.number in (?) AND albums.takedown_at IS NULL", params[:email], params[:upcs].split(','))
    matching_songs = Song.joins(:album, "INNER JOIN people ON people.id = albums.person_id").where("people.email = ? AND (songs.optional_isrc in (?) OR songs.tunecore_isrc in (?))",params[:email], params[:isrcs].split(','), params[:isrcs].split(','))
    [matching_albums, matching_songs]
  end

  def self.missing_assets(params, matching_albums, matching_songs)
    missing_upcs = params[:upcs].split(',') - matching_albums.collect { |a| a.upc.number }
    missing_isrcs = params[:isrcs].split(',') - matching_songs.collect { |s| s.isrc }
    [missing_upcs, missing_isrcs]
  end

  # ignore failed duplicates
  def self.insert_new_tracking(albums, songs, email, event_date)
    person = Person.find_by(email: email)
    album_inserts = albums.collect { |album| ({person: person, model: album.class.name, model_id: album.id, service: "royalty_solutions", event_date: event_date})}
    TunecoreTracking.create(album_inserts)
    song_inserts = songs.collect { |song| ({person: person, model: song.class.name, model_id: song.id, service: "royalty_solutions", event_date: event_date}) }
    TunecoreTracking.create(song_inserts)
  end

  def self.create_response(missing_upcs, missing_isrcs, event_date_input, parsed_event_date)
    response = {}
    unless missing_upcs.empty? && missing_isrcs.empty?
      message = missing_upcs.collect { |u|  ({:"UPC#{u}"=> "not found"}) }
      message << missing_isrcs.collect { |u|  ({:"ISRC#{u}"=> "not found"}) }
      response = { status: "failure", message: message.flatten, timestamp: Time.now.getutc }
    end

    if event_date_input && !valid_event_date?(parsed_event_date)
      new_msg = "Invalid event_date: #{event_date_input}"

      if response.present?
        response[:message] << new_msg
      else
        response = { status: "failure", message: new_msg, timestamp: Time.now.getutc }
      end
    end

    response.present? ? response : { status: "success", message: "OK", timestamp: Time.now.getutc }
  end

  def self.valid_event_date?(event_date)
    event_date.present? && (event_date < DateTime.current)
  end

  def self.parse_event_date(event_date)
    begin
      event_date ? DateTime.parse(event_date) : DateTime.current
    rescue
      nil
    end
  end
end
