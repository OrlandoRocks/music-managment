module ZipFolder
  def compress_folder(folder, file_name)
    Zip::File.open(file_name, Zip::File::CREATE) do |zipfile|
      Dir["#{folder}/**/**"].each do |file|
        zipfile.add(file.sub(folder + '/', ''), file)
      end
    end
  end
end
