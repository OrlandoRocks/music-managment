#
#
#  Verification System:
#
#  Abstraction of verification logic from the Application Controller
#
module VerificationSystem

  #
  #  Users can log in, but if they are not verified, they are not allowed
  #  to do anything on the site
  #
  def check_for_verification
    return if !current_user

    if !current_user.is_verified?
      flash.keep(:redeem_thank_you)
      redirect_to unverified_person_path(current_user)
    end
  end

  #  Predicate method to determine if
  #  the current user is verified
  def is_verified?
    return false if ! current_user

    current_user.is_verified?
  end

  #
  #  Make sure that exising users accept terms and conditions before
  #  going anywhere else in the site
  #
  def check_for_terms_and_conditions
    return if ! current_user

    if ! current_user.accepted_terms_and_conditions
      flash[:error] = "Please accept the Tunecore Terms and Conditions."
      redirect_to edit_person_path(current_user, tab: "terms")
    end
  end

end
