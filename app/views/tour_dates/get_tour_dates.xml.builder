# 2009-08-11 -- ED -- initial creation for Widget 2 release
# 2009-08-27 -- ED -- modified xml to load tour dates from widget
# 2009-09--1 -- ED -- returns abbreviated us states
# 2009-09-09 -- ED -- returns error messages
xml.instruct!

if @widget and !@widget.person.tour_dates.empty?
  xml.status(:result => "success") do
    xml.tour_dates do
      @widget.person.tour_dates.upcomming.each do |tour_date|
        xml.tour_date(:city => tour_date.city, :state => tour_date.us_state.abbreviation,
                      :country => tour_date.country.name.titleize, :venue => tour_date.venue,
                      :date => tour_date.event_date_at.nil? ? "":tour_date.event_date_at.strftime("%Y-%m-%d"),
                      :time => tour_date.event_time_at.nil? ? "":tour_date.event_time_at.strftime("%H:%M")) do
          xml.info tour_date.information
          xml.artist(:id => tour_date.artist.id, :name => tour_date.artist.name)
        end
      end
    end
  end
else
  xml.status(:result => "error") do
    xml.errors do
      xml.error(:label => "", :message => custom_t(".no_available"))
    end
  end
end
