class PersonEarningsSummarizationWorker
  include Sidekiq::Worker

  # Params:
  # - person_id: a person ID
  # - year(optional): defaults to current year, accepts a single or a list of years. Eg: "2021" or ["2022", "2023"]
  def perform(person_id, year = nil)
    TaxBlocking::PersonEarningsSummarizationService.new(person_id, year).summarize
  end
end
