class TrackMonetization::SubscriptionWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 3, backtrace: 45

  def perform(params)
    logger.info "Sidekiq Job initiated TrackMonetization::SubscriptionWorker: #{params}"
    TrackMonetization::SubscriptionService.subscribe(params)
  end
end
