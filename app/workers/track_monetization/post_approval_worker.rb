class TrackMonetization::PostApprovalWorker
  include Sidekiq::Worker

  sidekiq_options queue: "default", retry: 3, backtrace: 45

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      Rails.logger.error(msg["error_message"])
      Rails.logger.error(msg["error_backtrace"])
    end
  end

  def perform(album_id, store_id)
    logger.info "Sidekiq Job initiated TrackMonetization::PostApprovalWorker: #{album_id} -> #{store_id}"
    songs = Song.where(album_id: album_id)
    monetize_and_check_eligibility(songs, store_id)
  end

  private

  def monetize_and_check_eligibility(songs, store_id)
    songs.each do |song|
      track_mon = TrackMonetization.where(
        song_id: song.id,
        store_id: store_id,
        person_id: song.person.id
      ).first_or_create

      track_mon.check_eligibility
    end
  end
end
