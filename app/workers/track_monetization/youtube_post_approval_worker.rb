class TrackMonetization::YoutubePostApprovalWorker
  include Sidekiq::Worker

  sidekiq_options queue: "default", retry: 3, backtrace: 45

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      Rails.logger.error(msg["error_message"])
      Rails.logger.error(msg["error_backtrace"])
    end
  end

  def perform(album_id, store_id)
    logger.info "Sidekiq Job initiated TrackMonetization::YoutubePostApprovalWorker: #{album_id} -> #{store_id}"
    TrackMonetization::CreationService.monetize_album(album_id: album_id, store_id: store_id)
  end
end
