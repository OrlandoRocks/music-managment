# frozen_string_literal: true

class GenerateOutboundInvoiceSequenceWorker
  include Sidekiq::Worker
  sidekiq_options queue: "critical"

  LAST_SEQ_METHOD = {
    OutboundInvoice: :last_generated_outbound_sequence,
    OutboundRefund: :last_generated_cr_outbound_sequence
  }

  def perform(invoice_id, klass = "OutboundInvoice")
    invoice = klass.constantize.find invoice_id
    new_sequence = invoice.person.send(LAST_SEQ_METHOD[klass.to_sym]) + 1
    invoice.update!(invoice_sequence: new_sequence)
  end
end
