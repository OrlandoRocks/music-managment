class ClientImportReportWorker
  include Sidekiq::Worker

  def perform(options)
    options = options.with_indifferent_access
    ClientImportReportService.new(options).process
  end
end
