class SocialNotifierWorker
  include Sidekiq::Worker
  def perform(method, args)
    SocialNotifier.send(method, *args).deliver
  end
end
