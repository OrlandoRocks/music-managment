class BatchBalanceAdjustment::PostAdjustmentWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 0, backtrace: 45

  def perform(batch_id, person_id, options = {})
    opts   = options.with_indifferent_access
    person = Person.find(person_id)
    batch  = BatchBalanceAdjustment.find(batch_id)

    raise "This batch balance adjustment has already been posted" if batch.posted?
    raise "Posting periods required" unless opts[:period_year] or opts[:period_interval] or opts[:period_type]
    unless opts[:category] or opts[:admin_note] or opts[:customer_note]
      raise "Category, Admin Note and Customer Note is required"
    end

    errors                             = []
    matched_details, unmatched_details = match_details(batch.details)
    main_note                          = opts[:customer_note]
    exchange_rate_note                 = "Exchange Rate: #{opts[:exchange_rate]}" if opts[:exchange_rate].present?
    person_ids                         = matched_details.collect(&:person_id)

    canadian_accounts = Person.for_country_website(CountryWebsite::CANADA).where({ id: person_ids })

    matched_details.each do |detail|
      next if detail.posted?

      ActiveRecord::Base.transaction do
        customer_note = main_note.to_s
        customer_note << " #{exchange_rate_note}" if canadian_accounts.detect { |a| a.id == detail.person_id }

        balance_adjustment = BalanceAdjustment.new(
          category: opts[:category],
          credit_amount: detail.current_balance,
          person_id: detail.person_id,
          posted_by_id: person.id,
          posted_by_name: person.name,
          admin_note: opts[:admin_note],
          customer_note: customer_note
        )

        save_successful =
          if balance_adjustment.valid?
            balance_adjustment.save
          elsif zero_amount_adjustment_error?(balance_adjustment)
            balance_adjustment.save(validate: false)
          end

        if save_successful
          post_to_category(balance_adjustment, detail, opts)
          detail.update(status: "posted", balance_adjustment: balance_adjustment)
        else
          errors << BatchBalanceAdjustmentError.create(
            batch_balance_adjustment_id: detail.batch_balance_adjustment_id,
            batch_balance_adjustment_detail_id: detail.id,
            raw_transaction: detail.attributes,
            notes: "Unable to post due to #{balance_adjustment.errors.full_messages}"
          )
        end
      end
    end

    unmatched_details.each do |detail|
      errors << BatchBalanceAdjustmentError.create(
        batch_balance_adjustment_id: detail.batch_balance_adjustment_id,
        batch_balance_adjustment_detail_id: detail.id,
        raw_transaction: detail.attributes,
        notes: "Unable to post due to an unmatched account"
      )
    end

    if errors.empty?
      batch.update_attribute(:status, "posted")
      AdminNotifier.batch_balance_adjustment(batch_id, person.email, "posted").deliver
    else
      batch.update_attribute(:status, "incomplete")
      AdminNotifier.batch_balance_adjustment(batch_id, person.email, "incomplete").deliver
    end
  rescue => e
    AdminNotifier.batch_balance_adjustment(batch_id, person.email, "errored: #{e.message}").deliver
  end

  def match_details(adjustment_details)
    account_ids = adjustment_details.collect(&:person_id)
    matched_accounts = Person.where({ id: account_ids }).all

    matched_details = []
    matched_accounts.collect(&:id).each do |person_id|
      matched_details << adjustment_details.select { |detail| detail.person_id == person_id }
    end
    matched_details.flatten!
    unmatched_details = adjustment_details - matched_details

    return matched_details, unmatched_details
  end

  def post_to_category(balance_adjustment, batch_detail, opts)
    allowed_categories = ["Songwriter Royalty", "YouTube MCN Royalty"]
    unless allowed_categories.include?(opts[:category])
      raise "Only Songwriter and MCN Royalty posting is allowed at this time"
    end

    RoyaltyPayment.update_transaction(balance_adjustment, batch_detail, opts)
  end

  def zero_amount_adjustment_error?(balance_adjustment)
    balance_adjustment.errors.one? && balance_adjustment.errors.of_kind?(:credit_amount, :greater_than_or_equal_to)
  end
end
