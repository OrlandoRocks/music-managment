class BatchTransactionProcessWorker
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def perform(payment_batch_id, batch_transaction_ids)
    PaymentBatch.find(payment_batch_id).process_transactions(
      batch_transaction_ids.map { |bt_id|
        BatchTransaction.find(bt_id)
      }
    )
  end
end
