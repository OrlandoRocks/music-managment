class PublishingAdministration::RecordingReconciliationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(account_id)
    $rights_app_limiter.within_limit do
      account = Account.find(account_id)
      PublishingAdministration::RecordingListService.list(account)
    end
  end
end
