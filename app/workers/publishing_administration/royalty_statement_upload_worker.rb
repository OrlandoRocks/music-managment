class PublishingAdministration::RoyaltyStatementUploadWorker
  include Sidekiq::Worker

  sidekiq_options queue: :publishing_royalties, retry: 2

  def perform(royalty_payment_id, quarter, year)
    logger.info "Sidekiq Job initiated PublishingAdministration::RoyaltyStatementUploadWorker: #{royalty_payment_id}"

    attachment_success = PublishingAdministration::RoyaltyStatementUploadService.attach(
      royalty_payment_id,
      quarter,
      year
    )

    return if attachment_success

    Tunecore::Airbrake.notify(
      "Unable to upload royalty statement files",
      {
        royalty_payment_id: royalty_payment_id
      }
    )
  end
end
