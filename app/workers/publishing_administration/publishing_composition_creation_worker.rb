class PublishingAdministration::PublishingCompositionCreationWorker
  include Sidekiq::Worker

  sidekiq_options unique_for: 30.minutes, queue: :critical, retry: 3

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      Rails.logger.error(msg["error_message"])
      Rails.logger.error(msg["error_backtrace"])
    end
  end

  def perform(publishing_composer_id)
    return unless PublishingComposer.find_by(id: publishing_composer_id)

    logger.info "Sidekiq Job initiated PublishingAdministration::PublishingCompositionCreationWorker: #{publishing_composer_id}"
    PublishingAdministration::PublishingCompositionCreationService.create(publishing_composer_id)
  end
end
