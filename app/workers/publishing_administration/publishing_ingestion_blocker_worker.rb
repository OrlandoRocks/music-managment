class PublishingAdministration::PublishingIngestionBlockerWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical

  def perform(account_id)
    $rights_app_limiter.within_limit do
      @account = Account.find(account_id)
      if import_data_from_rightsapp_enabled? || !account_has_multiple_writers_with_right_to_collect?
        @account.import_from_rights_app
      end
    end
  end

  def account_has_multiple_writers_with_right_to_collect?
    PublishingAdministration::PublishingIngestionBlocker.multiple_writers_with_right_to_collect?(@account.provider_account_id)
  end

  def import_data_from_rightsapp_enabled?
    FeatureFlipper.show_feature?(:import_data_from_rightsapp, @account.person)
  end
end
