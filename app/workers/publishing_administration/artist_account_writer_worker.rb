class PublishingAdministration::ArtistAccountWriterWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id)
    $rights_app_limiter.within_limit do
      @composer = Composer.find(composer_id)

      find_or_create_artist_account
      create_writer
    end
  end

  private

  attr_reader :composer

  def find_or_create_artist_account
    return unless composer.account.provider_account_id.nil?

    begin
      PublishingAdministration::ApiClientServices::ArtistAccountService.find_or_create_artist(account: composer.account)
    rescue => e
      Tunecore::Airbrake.notify("API RIGHTSAPP Client Call Failed: #{composer.id} #{composer.account.id} #{composer.name} #{e}")
    end
  end

  def create_writer
    PublishingAdministration::ApiClientServices::WriterService.create_or_update(composer)
  end
end
