class PublishingAdministration::PublishingWorkReconciliationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :low, retry: 0, unique_for: 60.minutes

  def perform(account_id, free_composer_purchases = false)
    $rights_app_limiter.within_limit do
      account = Account.find(account_id)

      return if FeatureFlipper.show_feature?(:publishing_reconciliation_enabled, account.person)

      free_purchase = free_composer_purchases

      PublishingAdministration::PublishingWorkListService.list(account, free_purchase)
    end
  end
end
