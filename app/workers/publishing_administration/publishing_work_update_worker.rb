class PublishingAdministration::PublishingWorkUpdateWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composition_id)
    @publishing_composition = PublishingComposition.find(composition_id)

    $rights_app_limiter.within_limit do
      PublishingAdministration::ApiClientServices::PublishingWorkCreationService
        .update_work(
          publishing_composition: @publishing_composition
        )
    end
  end
end
