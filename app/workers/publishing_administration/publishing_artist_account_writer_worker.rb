class PublishingAdministration::PublishingArtistAccountWriterWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(publishing_composer_id)
    $rights_app_limiter.within_limit do
      @publishing_composer = PublishingComposer.find(publishing_composer_id)

      find_or_create_artist_account
      create_writer
    end
  end

  private

  attr_reader :publishing_composer

  def find_or_create_artist_account
    return unless publishing_composer.account.provider_account_id.nil?

    begin
      PublishingAdministration::ApiClientServices::ArtistAccountService.find_or_create_artist(account: publishing_composer.account)
    rescue => e
      Tunecore::Airbrake.notify(
        "API RIGHTSAPP Client Call Failed: #{publishing_composer.id} #{publishing_composer.account.id} #{publishing_composer.name} #{e}"
      )
    end
  end

  def create_writer
    PublishingAdministration::ApiClientServices::PublishingWriterService.create_or_update(publishing_composer)
  end
end
