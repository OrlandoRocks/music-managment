class PublishingAdministration::PublishingWriterImporterWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(publishing_composer_id)
    $rights_app_limiter.within_limit do
      publishing_composer = PublishingComposer.find_by(id: publishing_composer_id)
      next if publishing_composer.blank?

      next unless publishing_composer.is_active?

      PublishingAdministration::ApiClientServices::WriterImporterService.import(publishing_composer)
    end
  end
end
