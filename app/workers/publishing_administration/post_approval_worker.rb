class PublishingAdministration::PostApprovalWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(album_id)
    PublishingAdministration::PostApprovalService.create(album_id)
  end
end
