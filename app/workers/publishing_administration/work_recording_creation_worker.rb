class PublishingAdministration::WorkRecordingCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id, composition_id)
    @composer     = Composer.find(composer_id)
    @composition  = Composition.find(composition_id)

    find_existing_match_before_sending_work

    $rights_app_limiter.within_limit do
      send_work
    end unless work_exists?

    $rights_app_limiter.within_limit do
      send_recording
    end
  end

  private

  attr_reader :composer, :composition

  def find_existing_match_before_sending_work
    return if work_exists?

    PublishingAdministration::CompositionMatcherService.match(params)
  end

  def params
    { composer: composer, composition: composition }
  end

  def send_recording
    return unless work_exists? && !composition.recordings.exists?(recording_code: nil)

    PublishingAdministration::ApiClientServices::RecordingService.post_recording(params)
  end

  def send_work
    PublishingAdministration::ApiClientServices::WorkCreationService.post_work(params)
  end

  def work_exists?
    composition.provider_identifier.present?
  end
end
