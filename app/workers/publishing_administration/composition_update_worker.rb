class PublishingAdministration::CompositionUpdateWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id)
    PublishingAdministration::CompositionUpdateService.update(composer_id)
  end
end
