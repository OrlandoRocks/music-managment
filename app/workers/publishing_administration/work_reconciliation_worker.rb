class PublishingAdministration::WorkReconciliationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(account_id, free_composer_purchases = false)
    $rights_app_limiter.within_limit do
      account = Account.find(account_id)
      free_purchase = free_composer_purchases

      PublishingAdministration::WorkListService.list(account, free_purchase)
    end
  end
end
