class PublishingAdministration::TerminatedAccountWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(terminated_composer_id)
    $rights_app_limiter.within_limit do
      @terminated_composer = TerminatedComposer.find(terminated_composer_id)
      send_terminated_account
    end
  end

  private

  attr_reader :terminated_composer

  def send_terminated_account
    PublishingAdministration::ApiClientServices::TerminatedComposerAccountService
      .post_terminated_composer(terminated_composer: terminated_composer)
  end
end
