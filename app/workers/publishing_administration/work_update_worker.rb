class PublishingAdministration::WorkUpdateWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composition_id)
    @composition = Composition.find(composition_id)

    $rights_app_limiter.within_limit do
      PublishingAdministration::ApiClientServices::WorkCreationService.update_work(composition: @composition)
    end
  end
end
