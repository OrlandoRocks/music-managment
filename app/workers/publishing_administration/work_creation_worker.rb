class PublishingAdministration::WorkCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id, composition_id)
    @composer     = Composer.find(composer_id)
    @composition  = Composition.find(composition_id)

    find_existing_match_before_sending

    $rights_app_limiter.within_limit do
      send_work
    end unless match_exists?
  end

  private

  attr_reader :composer, :composition

  def find_existing_match_before_sending
    PublishingAdministration::CompositionMatcherService.match(params)
  end

  def match_exists?
    composition.provider_identifier.present?
  end

  def params
    { composer: composer, composition: composition }
  end

  def send_work
    PublishingAdministration::ApiClientServices::WorkCreationService.post_work(params)
  end
end
