class PublishingAdministration::PublishingWorkRecordingCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(publishing_composer_id, publishing_composition_id)
    @publishing_composer     = PublishingComposer.find(publishing_composer_id)
    @publishing_composition  = PublishingComposition.find(publishing_composition_id)

    find_existing_match_before_sending_work

    $rights_app_limiter.within_limit do
      send_work
    end unless work_exists?

    $rights_app_limiter.within_limit do
      send_recording
    end
  end

  private

  attr_reader :publishing_composer, :publishing_composition

  def find_existing_match_before_sending_work
    return if work_exists?

    PublishingAdministration::PublishingCompositionMatcherService.match(params)
  end

  def params
    { publishing_composer: publishing_composer, publishing_composition: publishing_composition }
  end

  def send_recording
    return unless work_exists? && !publishing_composition.recordings.exists?(recording_code: nil)

    PublishingAdministration::ApiClientServices::PublishingRecordingService.post_recording(params)
  end

  def send_work
    PublishingAdministration::ApiClientServices::PublishingWorkCreationService.post_work(params)
  end

  def work_exists?
    publishing_composition.provider_identifier.present?
  end
end
