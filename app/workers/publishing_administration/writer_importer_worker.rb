class PublishingAdministration::WriterImporterWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id)
    $rights_app_limiter.within_limit do
      composer = Composer.find_by(id: composer_id)
      PublishingAdministration::ApiClientServices::WriterImporterService.import(composer) if composer.present?
    end
  end
end
