class PublishingAdministration::PublishingBackfillWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(params = {})
    $rights_app_limiter.within_limit do
      PublishingAdministration::PublishingBackfillService.backfill(params)
    end
  end
end
