class PublishingAdministration::CompositionCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 3

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      Rails.logger.error(msg["error_message"])
      Rails.logger.error(msg["error_backtrace"])
    end
  end

  def perform(composer_id)
    logger.info "Sidekiq Job initiated PublishingAdministration::CompositionCreationWorker: #{composer_id}"
    PublishingAdministration::CompositionCreationService.create(composer_id)
  end
end
