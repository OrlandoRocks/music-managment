class PublishingAdministration::PublishingWriterCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(publishing_composer_id)
    $rights_app_limiter.within_limit do
      publishing_composer = PublishingComposer.find(publishing_composer_id)
      PublishingAdministration::ApiClientServices::PublishingWriterService.create_or_update(publishing_composer)
    end
  end
end
