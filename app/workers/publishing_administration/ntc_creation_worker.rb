class PublishingAdministration::NtcCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical

  def perform(params)
    PublishingAdministration::NonTunecorePublishingCompositionForm.new(params).save
  end
end
