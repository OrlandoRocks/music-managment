class PublishingAdministration::PublishingCompositionUpdateWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id)
    PublishingAdministration::PublishingCompositionUpdateService.update(composer_id)
  end
end
