class PublishingAdministration::ArtistAccountWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(account_id)
    $rights_app_limiter.within_limit do
      artist_account(account_id)
    end
  end

  private

  def artist_account(account_id)
    account = Account.find(account_id)
    PublishingAdministration::ApiClientServices::ArtistAccountService.find_or_create_artist(account: account)
  end
end
