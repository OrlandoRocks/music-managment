class PublishingAdministration::WriterCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 2

  def perform(composer_id)
    $rights_app_limiter.within_limit do
      composer = Composer.find(composer_id)
      PublishingAdministration::ApiClientServices::WriterService.create_or_update(composer)
    end
  end
end
