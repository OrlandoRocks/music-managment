class Webhooks::WebhookWorker
  include Sidekiq::Worker

  sidekiq_options queue: "critical", retry: true

  def perform(type, options)
    case type
    when "sift"
      Sift::WebhookService.new(options).process
    else
      Tunecore::Airbrake.notify("Webhooks::WebhookWorker#perform - Unimplemented case for event_type: #{type}")
    end
  end
end
