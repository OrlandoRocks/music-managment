# frozen_string_literal: true

class AutoApproveTransfers::WorkflowWorker
  class AutoApproveWithdrawlsAdminMissingError < StandardError; end

  class TransferMetadatumTackableTypeError < StandardError; end

  include Sidekiq::Worker
  sidekiq_options queue: :critical, unique_for: 10.minutes, retry: 3

  def perform(async_transfer_metadatum_id)
    @async_transfer_metadatum_id = async_transfer_metadatum_id

    return if transfer_metadatum.blank?
    return if transfer_metadatum.auto_approved_at?

    handle_approval_service!
  end

  private

  # Handles Transfers::ApproveService updates in a transaction
  def handle_approval_service!
    ApplicationRecord.transaction do
      if automation_on? && transfer_metadatum.trackable.auto_approveable? && rules_still_pass?
        Transfers::ApproveService.call(approve_service_params)
      else
        transfer_metadatum.roll_back_approval!
      end
      log_auto_approved_payout_transfers if transfer_metadatum.complete_auto_approval!
    end
  end

  # Preloads the transfer and the metadata record
  def transfer_metadatum
    @transfer_metadatum ||=
      TransferMetadatum.where(id: @async_transfer_metadatum_id)
                       .preload(:trackable)
                       .take
  end

  # Checks if the feature is enabled at the time of job execution
  def automation_on?
    FeatureFlipper.show_feature?(:auto_approved_withdrawals)
  end

  # Checks that the rules still pass
  def rules_still_pass?
    AutoApproveTransfers::RulesService.new(transfer_metadatum.trackable).all_rules_pass?
  end

  # Creates the approve params for the Transfers::ApproveService
  def approve_service_params
    @approve_service_params ||= {
      provider: trackable_type,
      action: "approve",
      transfer_ids: [
        transfer_metadatum.trackable.id
      ],
      person: auto_approve_admin
    }
  end

  # Figures out the provider for the approve_service_params
  def trackable_type
    @trackable_type ||=
      case transfer_metadatum.trackable_type
      when "PaypalTransfer" then "paypal"
      when "PayoutTransfer" then "payoneer"
      else raise TransferMetadatumTackableTypeError, "#{transfer_metadatum.class}: #{transfer_metadatum.attributes}"
      end
  end

  # The automated admin person record
  def auto_approve_admin
    email = ENV.fetch("AUTO_APPROVE_WITHDRAWLS_ADMIN_EMAIL", "auto_approve_withdrawls_admin@tunecore.com")

    Person.find_by(email: email).tap do |person|
      next if person.present?

      raise AutoApproveWithdrawlsAdminMissingError,
            "Person with #{email} missing. Run rake task, people:create_auto_approval_transfers_admin, to create admin"
    end
  end

  def log_auto_approved_payout_transfers
    Rails.logger.info(
      "Topic: PayoutTransfers, AutoApproval RulesEngine
      Approved the following payout_transfer,
      #{transfer_metadatum.trackable_type}: #{transfer_metadatum.trackable_id}"
    )
  end
end
