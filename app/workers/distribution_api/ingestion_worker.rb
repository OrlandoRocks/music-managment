class DistributionApi::IngestionWorker
  include Sidekiq::Worker
  sidekiq_options queue: "distribution-api-ingestion", retry: 3

  def perform(params)
    params = params.with_indifferent_access
    DistributionApi::IngestionService.call(params[:Key], params[:Bucket])
  end
end
