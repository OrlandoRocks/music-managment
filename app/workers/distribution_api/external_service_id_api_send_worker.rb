class DistributionApi::ExternalServiceIdApiSendWorker
  include Sidekiq::Worker
  sidekiq_options queue: "distribution-api-outbound", retry: 3

  def perform(params)
    params = params.with_indifferent_access
    DistributionApi::ExternalServiceIdApi::SendService.call(params)
  end
end
