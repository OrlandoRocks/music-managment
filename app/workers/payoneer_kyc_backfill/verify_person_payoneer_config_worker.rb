# frozen_string_literal: true

class PayoneerKycBackfill::VerifyPersonPayoneerConfigWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: 5, backtrace: true

  def perform(async_person_id)
    puts "Job started"
    person = Person.find_by(id: async_person_id)
    return if person.blank?

    Payoneer::PersonKycBackfillService.new(person).call!
  end
end
