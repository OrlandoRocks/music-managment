# frozen_string_literal: true

class PayoneerKycBackfill::WorkflowWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: true, backtrace: true

  def perform(person_ids = nil)
    over_all = Sidekiq::Batch.new

    over_all.on(:success, self.class)
    over_all.description = "Sync all eligible person's compliance info with KYC Payoneer"
    over_all.jobs do
      Person.uncached do
        relation = Person
        relation = relation.where(id: person_ids) if person_ids.present?

        people = relation
                 .select(:id)
                 .for_country_website([1, 8])
                 .joins(:payout_providers)
                 .where(payout_providers: { name: "payoneer", provider_status: "approved" })

        people.find_in_batches do |people|
          Sidekiq::Client.push_bulk(
            "class" => PayoneerKycBackfill::VerifyPersonPayoneerConfigWorker,
            "args" => people.map { |x| [x.id] }
          )
        end
      end
    end
  end

  def on_success(_status, _options)
    puts "Ok!"
  end
end
