class Delivery::DistributionWorker
  include Sidekiq::Worker

  # setting this to a very large lock_timeout due to some deliveries
  # needing that long to upload the audio assets.
  # https://github.com/mperham/sidekiq/wiki/Ent-Rate-Limiting

  DEFAULT_WAIT_TIMEOUT = 1200
  RATE_LIMIT_LOCK_TIMEOUT = 20_000

  CONCURRENT_PROCESSES = {
    "7Digital" => 8,
    "Akazoo" => 12,
    "Amazon" => 24,
    "AmazonCld" => 12,
    "AmazonOD" => 12,
    "AmazonUL" => 12,
    "Amiestreet" => 12,
    "Anghami" => 12,
    "AppleMusic" => 12,
    "BelieveExp" => 12,
    "BelieveLiv" => 128,
    "Bloom" => 12,
    "Boom" => 12,
    "ClaroMusic" => 8,
    "Connect" => 12,
    "Cur" => 12,
    "DMusic" => 12,
    "Deezer" => 12,
    "Default" => 50,
    "FBTracks" => 12,
    "FNAC" => 12,
    "Facebook" => 12,
    "Google" => 32,
    "Gracenote" => 12,
    "GroupieTun" => 12,
    "Guvera" => 12,
    "Guvera7Dig" => 12,
    "InProdicon" => 12,
    "Instagram" => 12,
    "Jbhifi" => 12,
    "Juke" => 12,
    "KKBox" => 12,
    "Lala" => 12,
    "Limewire" => 12,
    "M_Island" => 2,
    "Medianet" => 12,
    "MusicOnlin" => 12,
    "Musicload" => 12,
    "Muve" => 12,
    "Myspace" => 12,
    "NMusic" => 12,
    "Napster" => 12,
    "Neurotic" => 12,
    "Nokia" => 12,
    "Pandora" => 12,
    "PlayMe" => 12,
    "Qsic" => 8,
    "Rdio" => 12,
    "Revibe" => 12,
    "RhapsodyRH" => 12,
    "SNOCAP" => 12,
    "Saavn" => 12,
    "Shazam" => 12,
    "Shockhound" => 12,
    "Simfy" => 12,
    "SimfyAF" => 12,
    "Slacker" => 12,
    "SndXchg" => 12,
    "SonyMU" => 12,
    "Spinlet" => 12,
    "Spotify" => 20,
    "Streaming" => 12,
    "Target" => 12,
    "Tencent" => 12,
    "Thumbplay" => 12,
    "TouchTunes" => 12,
    "UMA" => 12,
    "Vevo" => 12,
    "Wimp" => 12,
    "YTMusic" => 12,
    "Yandex" => 12,
    "Youtube" => 12,
    "YoutubeSR" => 12,
    "Zune" => 12,
    "Zvooq" => 12,
    "bytedance" => 25,
    "eMusic" => 12,
    "iTunes" => 10,
    "Gaana" => 12,
    "Joox" => 12,
    "Zed" => 10,
    "Tim" => 12,
    "Hungama" => 12
  }.freeze

  WAIT_LIMITS = {
    "FBTracks" => 3600,
    "Instagram" => 3600,
    "M_Island" => 6000,
    "Default" => 3600,
    "iTunes" => 3600,
    "Qsic" => 6000,
    "UMA" => 6000,
    "YoutubeSR" => 3600
  }

  LOCK_LIMITS = {
    "FBTracks" => 31_600
  }.freeze

  RATE_LIMITERS =
    CONCURRENT_PROCESSES.each_with_object({}) do |(short_name, limit), acc|
      acc[short_name] =
        if short_name == "iTunes"
          Sidekiq::Limiter.window(
            "delivery-rate-limit-#{short_name}",
            limit,
            :minute,
            wait_timeout: WAIT_LIMITS.fetch(short_name, DEFAULT_WAIT_TIMEOUT),
            lock_timeout: LOCK_LIMITS.fetch(short_name, RATE_LIMIT_LOCK_TIMEOUT)
          )
        else
          Sidekiq::Limiter.concurrent(
            "delivery-rate-limit-#{short_name}",
            limit,
            wait_timeout: WAIT_LIMITS.fetch(short_name, DEFAULT_WAIT_TIMEOUT),
            lock_timeout: LOCK_LIMITS.fetch(short_name, RATE_LIMIT_LOCK_TIMEOUT)
          )
        end
    end

  sidekiq_options queue: "delivery-default", retry: 3, backtrace: 45, unique_for: 1.hour

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def perform(delivery_class, distribution_id, delivery_type = nil, queue = nil)
    store = delivery_class.classify.constantize.find(distribution_id).store
    if store
      limiter(store).within_limit do
        Delivery::Distribution.deliver(delivery_class, distribution_id, delivery_type, queue, jid)
      end
    else
      Delivery::Distribution.deliver(delivery_class, distribution_id, delivery_type, queue, jid)
    end
  end

  def limiter(store)
    RATE_LIMITERS[store.short_name] || RATE_LIMITERS[store.short_name[/iTunes/, 0]] || RATE_LIMITERS["Default"]
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      delivery_class    = msg["args"][0]
      distribution_id   = msg["args"][1]
      job_id            = msg["jid"]
      distribution      = Delivery::Distribution.new(delivery_class, distribution_id, nil, nil, job_id).distributable
      params            = {
        state: "error",
        message: msg["error_message"],
        backtrace: msg["error_backtrace"].join("\n\t"),
        actor: "Delivery::DistributionWorker"
      }
      Distribution::StateUpdateService.update(distribution, params)
    end
  end
end
