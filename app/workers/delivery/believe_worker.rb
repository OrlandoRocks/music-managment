class Delivery::BelieveWorker
  include Sidekiq::Worker
  sidekiq_options queue: :believe, retry: 3

  def perform(distribution_id, album_id, person_id)
    return unless ActiveModel::Type::Boolean.new.cast(ENV.fetch("NOTIFY_BELIEVE", false))

    # There are a lot of failures from the believe
    # side. So we are temporarily disabling this
    # https://tunecore.atlassian.net/browse/TEC-438
    Delivery::Believe.deliver(distribution_id, album_id, person_id)
  end
end
