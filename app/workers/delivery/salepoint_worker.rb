class Delivery::SalepointWorker
  include Sidekiq::Worker

  sidekiq_options queue: :delivery_post_purchase, retry: 3, backtrace: 45

  def perform(salepoint_id)
    Delivery::Salepoint.deliver(salepoint_id)
  end
end
