class Delivery::BatchCompleteWorker
  include Sidekiq::Worker

  sidekiq_options queue: "delivery-default", retry: 3, backtrace: 45

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def perform(batch_id)
    DistributionSystem::BatchManager.close_batch(batch_id)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      batch_id = msg["args"][0]
      DeliveryBatch.find(batch_id).update(currently_closing: false)
    end
  end
end
