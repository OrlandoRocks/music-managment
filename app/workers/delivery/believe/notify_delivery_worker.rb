class Delivery::Believe::NotifyDeliveryWorker
  include Sidekiq::Worker
  sidekiq_options queue: :believe, retry: false

  def perform(album_id, store_ids, delivery_type = "INSERT")
    return unless ActiveModel::Type::Boolean.new.cast(ENV.fetch("NOTIFY_BELIEVE", false))

    # There are a lot of failures from the believe
    # side. So we are temporarily disabling this
    # https://tunecore.atlassian.net/browse/TEC-438
    Delivery::Believe::NotifyDelivery.notify_delivery(album_id, store_ids, delivery_type)
  end
end
