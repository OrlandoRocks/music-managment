class Delivery::Believe::ChangeStoreRestrictionWorker
  include Sidekiq::Worker
  sidekiq_options queue: :believe, retry: false

  def perform(album_id, delivery_type, finalized_store_ids = nil)
    return unless ActiveModel::Type::Boolean.new.cast(ENV.fetch("NOTIFY_BELIEVE", false))

    Delivery::Believe::ChangeStoreRestriction.change_store_restriction(
      album_id,
      delivery_type,
      finalized_store_ids
    )
  end
end
