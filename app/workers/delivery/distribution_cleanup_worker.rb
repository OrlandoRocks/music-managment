class Delivery::DistributionCleanupWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: 3, backtrace: 45

  def perform(delivery_class, delivery_id)
    Delivery::DistributionCleanupService.cleanup(delivery_class, delivery_id)
  end
end
