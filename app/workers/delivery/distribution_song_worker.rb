class Delivery::DistributionSongWorker
  include Sidekiq::Worker

  sidekiq_options queue: "delivery-default", retry: 3, backtrace: 45

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def perform(salepoint_song_id)
    Delivery::DistributionSong.deliver(salepoint_song_id)
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      salepoint_song_id = msg["args"][0]
      distribution      = Delivery::DistributionSong.new(salepoint_song_id).find_or_create_distribution
      params            = {
        state: "errored",
        message: msg["error_message"],
        backtrace: msg["error_backtrace"].join("\n\t")
      }
      Distribution::StateUpdateService.update(distribution, params)
    end
  end
end
