class Delivery::AlbumWorker
  include Sidekiq::Worker

  sidekiq_options queue: :delivery_post_purchase, retry: 3

  def perform(album_id)
    Delivery::Album.deliver(album_id)
  end
end
