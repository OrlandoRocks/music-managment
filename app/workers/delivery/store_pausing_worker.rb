class Delivery::StorePausingWorker
  include Sidekiq::Worker
  sidekiq_options queue: "default", retry: 3, backtrace: 45

  def perform(store_id)
    Delivery::StorePausingService.pause!(store_id)
  end
end
