class Delivery::SnsNotifierPausedWorker
  include Sidekiq::Worker

  sidekiq_options queue: "delivery-sns-pause"

  def perform(album_ids, store)
  end
end
