class Delivery::StoreUnpausingWorker
  include Sidekiq::Worker
  sidekiq_options queue: "default", retry: 3, backtrace: 45

  def perform(store_id, person_id, remote_ip)
    Delivery::StoreUnpausingService.schedule_backfill!(store_id, person_id, remote_ip)
  end
end
