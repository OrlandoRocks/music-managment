class IndlocalNotifications::InvoiceStatementsWorker
  include Sidekiq::Worker
  # we want it unique for 2 minutes in case the worker is called twice
  # two emails could potentially go out, but they should be identical
  # the logs in s3 will be overwritten, so there isn't a concern there
  sidekiq_options queue: :default, unique_for: 2.minutes

  def perform(date = Date.current)
    Indlocal::InvoiceStatementsService.send_invoice_statements_email(date)
  end
end
