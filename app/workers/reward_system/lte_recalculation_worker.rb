class RewardSystem::LteRecalculationWorker
  include Sidekiq::Worker

  def perform(person_id)
    @person = Person.find_by(id: person_id)
    return if @person.blank?

    @person.recalculate_lifetime_earning
    RewardSystem::Events.earnings_updated(person_id)
  end
end
