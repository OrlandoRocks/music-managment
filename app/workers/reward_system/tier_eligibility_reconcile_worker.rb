class RewardSystem::TierEligibilityReconcileWorker
  include Sidekiq::Worker

  def perform(person_id)
    person = Person.find_by(id: person_id)

    return if person.blank?
    return if person.tiers.any?

    RewardSystem::Eligibility::Calculator.new(person.id).check_and_upgrade
  end
end
