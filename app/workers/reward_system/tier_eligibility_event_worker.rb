class RewardSystem::TierEligibilityEventWorker
  include Sidekiq::Worker

  def perform(person_id, event_name)
    RewardSystem::TierEventer.new(event_name).register_event(person_id)
  end
end
