class TaxReports::AnnualTaxReportCallbacks
  def handle_user_without_w9(status, options)
    overall = Sidekiq::Batch.new(status.parent_bid)
    overall.description = "1099 Report: Non W9 Users (Withheld)"
    overall.jobs do
      moving_records_workflow = Sidekiq::Batch.new
      moving_records_workflow.on(:success, "TaxReports::AnnualTaxReportCallbacks#handle_moving_csv_to_s3")
      moving_records_workflow.jobs do
        year = DateTime.strptime(String(options["year"]), "%Y").all_year
        withholdings_for_year(year).each do |person_transaction_record|
          TaxReports::AnnualTaxReportNonW9RowWorker
            .perform_async(person_transaction_record.person_id, options["year"])
        end
      end
    end
  end

  def handle_moving_csv_to_s3(_status, _options)
    TaxReports::TaxInfoEnhancementService.new.finalize_csv
  end

  private

  def withholdings_for_year(year)
    PersonTransaction
      .select(:person_id)
      .where(target_type: "IRSTaxWithholding")
      .where(created_at: year)
      .distinct
  end
end
