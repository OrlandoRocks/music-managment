class TaxReports::AnnualTaxReportGenerationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: true, backtrace: 90, unique_for: 3.hours

  ACCEPTED_FORM_TYPES = ["W9 - Individual", "W9 - Entity"].freeze

  attr_accessor :w9_report_filename, :year

  def perform(year, w9_report_filename)
    @w9_report_filename = w9_report_filename
    @year = year

    over_all = Sidekiq::Batch.new
    over_all.description = "1099 Report: W9 Submitted Users"
    over_all.on(
      :success,
      "TaxReports::AnnualTaxReportCallbacks#handle_user_without_w9",
      year: year
    )

    over_all.jobs { process_users_with_w9 }
  end

  private

  def process_users_with_w9
    filtered_data.each do |record|
      TaxReports::AnnualTaxReportWithW9RowWorker.perform_async(
        Base64.urlsafe_encode64(Oj.dump(record.to_h)),
        year
      )
    end
  end

  def taxform_submissions
    @taxform_submissions ||=
      RakeDatafileService
      .download(w9_report_filename)
      .local_file
      .then { |file| File.readlines(file).slice(3..).join }
      .then { |lines| CSV.parse(lines, headers: true, liberal_parsing: true) }
  end

  def filtered_data
    @filtered_data ||=
      taxform_submissions
      .group_by { |record| provider_lookup_table[record["Payee ID"]] }
      .values
      .map do |records|
        records.max_by do |record|
          DateTime.strptime(record["Date Of Signature"], "%m/%d/%Y")
        end
      end
  end

  def provider_lookup_table
    @provider_lookup_table ||=
      taxform_submissions
      .pluck("Payee ID")
      .then { |payee_ids| PayoutProvider.where(client_payee_id: payee_ids) }
      .then { |payout_providers| payout_providers.pluck(:client_payee_id, :person_id).to_h }
  end
end
