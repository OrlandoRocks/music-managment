class TaxReports::AnnualTaxReportNonW9RowWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: true, backtrace: 45, unique_for: 3.hours

  attr_accessor :person_id

  def perform(person_id, year)
    @person_id = person_id
    return if record_already_processed?

    TaxReports::AnnualTaxReportRowService.new(
      kind: :person_id,
      data: person_id,
      year: year
    ).generate_user_csv_row
  end

  def record_already_processed?
    Redis.new(url: ENV["REDIS_URL"], timeout: 20)
         .sismember(TaxReports::AnnualTaxReportRowService::REDIS_RECORDS_KEY, person_id)
  end
end
