class TaxReports::AnnualTaxReportWithW9RowWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: true, backtrace: 45, unique_for: 3.hours

  def perform(encoded_csv_row, year)
    TaxReports::AnnualTaxReportRowService.new(
      kind: :decoded_csv_row,
      data: decode_csv_record(encoded_csv_row),
      year: year
    ).generate_user_csv_row
  end

  def decode_csv_record(data)
    Base64.urlsafe_decode64(data)
          .then { |encoded_json| Oj.load(encoded_json) }
  end
end
