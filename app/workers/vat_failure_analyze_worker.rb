# frozen_string_literal: true

class VatFailureAnalyzeWorker
  include Sidekiq::Worker

  def perform(start_date = nil, end_date = nil)
    return unless vat_failure_report_enabled?

    start_date = start_date ? Time.zone.parse(start_date) : Time.zone.yesterday
    end_date = end_date ? Time.zone.parse(end_date) : Time.zone.yesterday

    analyzer = VatFailureAnalyzeService.new(start_date, end_date)
    VatFailureReportMailer.report(analyzer).deliver
  end

  def vat_failure_report_enabled?
    ActiveModel::Type::Boolean.new.cast(
      ENV.fetch("ENABLE_VAT_FAILURE_REPORT", "false").to_s
    )
  end
end
