class InvoiceNotificationWorker
  include Sidekiq::Worker

  def perform(invoice_id)
    InvoiceGenerator::EmailNotification.new(Invoice.find_by(id: invoice_id)).perform!
  end
end
