class StoreBackfillStartWorker
  include Sidekiq::Worker

  def perform(store_id, backfill_amount = 1000)
    Stores::BackfillStartService.start(store_id, backfill_amount)
  end
end
