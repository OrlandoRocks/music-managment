module Scrapi
  class GetJobWorker
    include Sidekiq::Worker

    def perform(job_id)
      logger.info "Sidekiq Job initiated GetJobWorker : #{job_id}"
      api_client = Scrapi::ApiClient.new(ENV["SCRAPI_USERNAME"], ENV["SCRAPI_PASSWORD"])
      api_client.get_access_token

      job = api_client.get_job(job_id)
      job.to_model
    end
  end
end
