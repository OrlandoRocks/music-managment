module Scrapi
  class CreateJobWorker
    include Sidekiq::Worker

    def perform(song_id)
      song = Song.find(song_id)
      logger.info "Sidekiq Job initiated CreateJobWorker : #{song_id}"

      api_client = Scrapi::ApiClient.new(ENV["SCRAPI_USERNAME"], ENV["SCRAPI_PASSWORD"])
      api_client.get_access_token

      job = api_client.create_job(song)
      job.to_model
    end
  end
end
