class BraintreeTransactionLogWorker
  include Concurrent::Async

  def write_to_bucket(invoice_id, response)
    bucket_name = ENV["PAYMENT_GATEWAY_TX_LOG_BUCKET_NAME"]
    bucket_path = "#{ENV['PAYMENT_GATEWAY_TX_LOG_BUCKET_ENV']}/braintree"
    file_name = "#{Time.now.iso8601}_braintree_#{invoice_id}.log"
    data = response.to_yaml

    s3_options = {
      bucket_name: bucket_name,
      bucket_path: bucket_path,
      file_name: file_name,
      data: data
    }

    begin
      S3LogService.write_and_upload(s3_options)
    rescue
      message = "Braintree transaction log raw response writing to S3 bucket failed"
      Airbrake.notify(message, { invoice_id: invoice_id })
    end
  end
end
