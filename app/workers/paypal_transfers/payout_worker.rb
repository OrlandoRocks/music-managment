# frozen_string_literal: true

class PaypalTransfers::PayoutWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: true, backtrace: true, unique_for: 7.days

  attr_reader :async_admin_id, :async_paypal_transfer_id

  def perform(async_admin_id, async_paypal_transfer_id)
    @async_admin_id           = async_admin_id
    @async_paypal_transfer_id = async_paypal_transfer_id

    return if paypal_transfer.blank?
    return if admin.blank?

    Admin::PaypalPayoutService.call(admin, paypal_transfer)
  end

  def paypal_transfer
    @paypal_transfer ||=
      PaypalTransfer.find_by(id: async_paypal_transfer_id)
  end

  def admin
    @admin ||=
      Person.find_by(id: async_admin_id)
  end
end
