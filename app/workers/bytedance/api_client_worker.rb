class Bytedance::ApiClientWorker
  include Sidekiq::Worker

  sidekiq_options queue: "distribution-api-outbound", retry: 3

  def perform(request_name, review_audit_id, album_id)
    Bytedance::ApiClient.new(request_name, review_audit_id, album_id).post
  end
end
