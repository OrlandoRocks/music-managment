class SuspiciousLoginWorker
  include Sidekiq::Worker

  def perform(login_event_id)
    SuspiciousLoginService.new(login_event_id).investigate
  end
end
