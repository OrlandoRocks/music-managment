class PublishingClientImportReportWorker
  include Sidekiq::Worker

  def perform(options)
    options = options.with_indifferent_access
    PublishingClientImportReportService.new(options).process
  end
end
