# frozen_string_literal: true

class Person::VatInfoUpdateBatchMailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: "mailers", backtrace: true

  def perform(params = {})
    params.transform_keys!(&:to_sym)
    query_params = params.slice(:limit, :offset)
    limit = params[:limit]
    offset = params[:offset] || 0

    batch = Sidekiq::Batch.new
    batch.description = "E-mailing people in the EU to remind them to update their VAT number"
    batch.on(:success, self.class)

    batch.jobs do
      Person.uncached do
        people = Person.select(:id).in_eu.limit(limit).offset(offset)
        people.find_in_batches do |persons|
          Sidekiq::Client.push_bulk(
            "class" => Person::VatInfoUpdateMailerWorker,
            "args" => persons.map { |x| [x.id] }
          )
        end
      end
    end
  end

  def on_success(_status, _options)
    puts "Successfully e-mailed VAT number reminder to all EU people"
  end
end
