# frozen_string_literal: true

class Person::AutoRenewalPeopleAndMailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: "mailers", retry: 0

  def perform(params = {})
    params.transform_keys!(&:to_sym)
    query_params = params.slice(
      :last_logged_in_before,
      :last_logged_in_after,
      :corporate_entity_id,
      :limit,
      :offset
    )

    batch = Sidekiq::Batch.new
    batch.description = "Sending auto-renewal update e-mail to people who have upcoming auto-renewals"
    batch.on(:success, self.class)
    batch.on(:complete, self.class)

    batch.jobs do
      Person.uncached do
        people = Person::AutoRenewalPeopleService.new(**query_params).call
        people.find_in_batches do |people_batch|
          Sidekiq::Client.push_bulk(
            "class" => Person::AutoRenewalBillingInfoConfirmationMailerWorker,
            "args" => people_batch.map { |x| [x.id] }
          )
        end
      end
    end
  end

  def on_success(_status, _options)
    puts "Successfully sent auto-renewal e-mail to all people in queue"
  end

  def on_complete(status, _options)
    puts "Auto-renewal e-mail have been sent out with some failures" if status.failures.any?
    puts "#{status.failures} jobs out of #{status.total} have failed."
  end
end
