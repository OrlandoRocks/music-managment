# frozen_string_literal: true

class Person::VatInfoUpdateMailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: "mailers", retry: true

  def perform(person_id)
    person = Person.find(person_id)
    PersonNotifier.vat_info_update_reminder(person).deliver_now
  end
end
