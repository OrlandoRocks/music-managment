# frozen_string_literal: true

class Person::AutoRenewalBillingInfoConfirmationMailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: "mailers", retry: 3, backtrace: true

  def perform(person_id, tc_social = false)
    person = Person.find(person_id)
    return if person.auto_renewal_email_sent?

    PersonNotifier.auto_renewal_billing_info_confirmation(person, tc_social).deliver_now
  end
end
