class TrendDataAggregationWorker
  include Sidekiq::Worker

  def perform(person_id, start_date = nil, end_date = nil)
    TrendDataAggregator.execute(person_id, start_date, end_date)
  end
end
