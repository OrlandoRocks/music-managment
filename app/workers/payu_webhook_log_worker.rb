# frozen_string_literal: true

class PayuWebhookLogWorker
  include Sidekiq::Worker
  sidekiq_options queue: "default", retry: 3, backtrace: 45

  def perform(txnid, params)
    bucket_name = ENV.fetch("PAYMENT_GATEWAY_TX_LOG_BUCKET_NAME")
    bucket_path = "#{ENV.fetch('PAYMENT_GATEWAY_TX_LOG_BUCKET_ENV')}/payu/webhooks"
    file_name = "#{txnid}_#{Time.now.iso8601}.log"
    data = params.to_yaml

    s3_options = {
      bucket_name: bucket_name,
      bucket_path: bucket_path,
      file_name: file_name,
      data: data
    }

    begin
      S3LogService.write_and_upload(s3_options)
    rescue StandardError => e
      Airbrake.notify(self.class.name, { error: e, txnid: txnid })
    end
  end
end
