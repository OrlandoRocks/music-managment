class TaxTokensCohortsMailWorker
  include Sidekiq::Worker

  def perform(cohort_id)
    TaxTokensCohortsMailService.send_mail_for_cohort(cohort_id)
  end
end
