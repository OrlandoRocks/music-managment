class EmailChangeWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(person_id, new_email)
    person = Person.find(person_id)
    token = EmailChangeRequest.generate_token

    EmailChangeRequest.create(
      person_id: person_id,
      token: token,
      old_email: person.email,
      new_email: new_email
    )

    PersonNotifier.verify_email(person, new_email, token).deliver
  end
end
