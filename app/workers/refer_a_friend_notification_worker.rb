class ReferAFriendNotificationWorker
  include Sidekiq::Worker

  def perform(invoice_id, new_customer)
    ReferAFriendNotification.send_notification(invoice_id, new_customer)
  end
end
