class BulkStoreLaunchEmailWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  attr_accessor :new_store

  def perform(store_id)
    BulkStoreLaunchEmail.send_store_launch_emails(store_id)
  rescue => e
    Rails.logger.info e.message
  end
end
