class Sift::EventWorker
  include Sidekiq::Worker

  sidekiq_options queue: "critical", retry: 3

  def perform(event_type, serialized_bodies)
    indifferent_access =
      if (event_type == Sift::EventService::TRANSACTION)
        serialized_bodies.map do |body|
          ActiveSupport::HashWithIndifferentAccess.new(body)
        end
      else
        ActiveSupport::HashWithIndifferentAccess.new(serialized_bodies)
      end

    Sift::ApiClient.send_request(event_type, indifferent_access)
  end
end
