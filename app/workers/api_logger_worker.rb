class ApiLoggerWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0

  def perform(api_logger_id)
    ApiLogger::InitLoggerService.new(api_logger_id).call
  end
end
