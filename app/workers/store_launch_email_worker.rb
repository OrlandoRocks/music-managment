class StoreLaunchEmailWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  attr_accessor :new_store

  def perform(store_id, person_id, releases)
    StoreLaunchEmail.send_store_launch_email(store_id, person_id, releases)
  end
end
