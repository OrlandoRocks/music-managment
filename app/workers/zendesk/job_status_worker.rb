class Zendesk::JobStatusWorker
  include Sidekiq::Worker

  sidekiq_options retry: 3

  def perform
    ApiLogger::JobStatusService.new.check_status
  end
end
