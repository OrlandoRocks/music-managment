class MailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailers

  def perform(klass, method, *args)
    klass.constantize.send(method, *args).deliver
  end
end
