class PersonAnalyticWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(invoice_id)
    PersonAnalytic.update_analytics_for_invoice(invoice_id)
  end
end
