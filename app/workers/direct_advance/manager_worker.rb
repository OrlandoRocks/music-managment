class DirectAdvance::ManagerWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 0

  def perform(end_date, test_run)
    processed_end_date = end_date.to_date
    processed_test_run = test_run.to_s == "true"

    DirectAdvance::Manager.manage(processed_end_date, test_run: processed_test_run)
  end
end
