# frozen_string_literal: true

class TaxChanges::EarningsPersonEmailWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailers, retry: 0, unique_for: 30.minutes

  def perform(person_id)
    return unless FeatureFlipper.show_feature?(:tax_change_notifications_has_earnings)

    @person_id = person_id
    return unless person?

    TaxChangeNotificationsMailer.notify_user_has_earnings(
      target_user: person.email
    ).deliver
  end

  private

  def person
    @person ||= Person.select(:id, :email).find_by(id: @person_id)
  end

  def person?
    person.present?
  end
end
