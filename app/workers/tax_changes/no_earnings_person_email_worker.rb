# frozen_string_literal: true

class TaxChanges::NoEarningsPersonEmailWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailers, retry: 0, unique_for: 30.minutes

  def perform(async_person_id)
    return unless FeatureFlipper.show_feature?(:tax_change_notifications_no_earnings)

    @async_person_id = async_person_id

    return unless person?

    TaxChangeNotificationsMailer.notify_users_no_inc_no_taxform(
      {
        target_user: person.email,
      }
    ).deliver
  end

  private

  def person
    @person ||= Person.select(:id, :email).find_by(id: @async_person_id)
  end

  def person?
    person.present?
  end
end
