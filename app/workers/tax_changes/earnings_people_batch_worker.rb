# frozen_string_literal: true

class TaxChanges::EarningsPeopleBatchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailers, retry: 0, backtrace: 45, unique_for: 10.minutes

  def perform
    return unless FeatureFlipper.show_feature?(:tax_change_notifications_has_earnings)

    batch_of_people.each do |person_id|
      TaxChanges::EarningsPersonEmailWorker.perform_async(person_id)
    end
  end

  def batch_of_people
    return @batch_of_people unless @batch_of_people.nil?

    people_without_payout_providers =
      Person
      .for_united_states_and_territories
      .with_lifetime_earnings
      .without_active_payout_providers
      .pluck(:id)

    people_with_payout_providers =
      Person
      .for_united_states_and_territories
      .with_lifetime_earnings
      .with_active_payout_providers
      .with_no_taxform_associations
      .pluck(:id)

    @batch_of_people = people_without_payout_providers.union(people_with_payout_providers)
  end
end
