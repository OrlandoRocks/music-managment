# frozen_string_literal: true

class TaxChanges::NoEarningsPeopleBatchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailers, retry: 0, backtrace: 45, unique_for: 10.minutes

  def perform
    return unless FeatureFlipper.show_feature?(:tax_change_notifications_no_earnings)

    batch_of_people.each do |person_id|
      TaxChanges::NoEarningsPersonEmailWorker.perform_async(person_id)
    end
  end

  private

  def batch_of_people
    return @batch_of_people unless @batch_of_people.nil?

    united_states_users = Person.for_united_states_and_territories
    target_user_queries = [
      :with_no_taxform_associations,
      :with_no_valid_taxforms,
      :with_no_lifetime_earnings_association,
      :with_no_lifetime_earning
    ]

    @batch_of_people =
      target_user_queries.each_with_object(Set.new) do |query, enum|
        enum.merge(
          united_states_users
          .public_send(query)
          .pluck("people.id")
        )
      end

    @batch_of_people
  end
end
