# frozen_string_literal: true

class PayinProviderConfigBackfill::AccountPayinProviderConfigWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: 5, backtrace: true

  def perform(async_ids, association_type)
    associations =
      case association_type
      when StoredPaypalAccount.to_s
        StoredPaypalAccount.where(id: async_ids)
      when PaypalTransaction.to_s
        PaypalTransaction.where(id: async_ids)
      when StoredCreditCard.to_s
        StoredCreditCard.where(id: async_ids)
      when BraintreeTransaction.to_s
        BraintreeTransaction.where(id: async_ids)
      end
    puts "Job started"
    return if associations.blank?

    associations.each do |association|
      PayinProviderConfigBackfillService.new(association).call!
    end
  end
end
