# frozen_string_literal: true

class PayinProviderConfigBackfill::WorkflowWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: true, backtrace: true

  attr_reader :child_worker

  def perform
    over_all = Sidekiq::Batch.new

    over_all.on(:success, self.class)
    over_all.description = "Backfill all payin provider configs"
    over_all.jobs do
      @child_worker = PayinProviderConfigBackfill::AccountPayinProviderConfigWorker

      handle_braintree
      handle_stored_credit_card
      handle_paypal_transaction
      handle_stored_paypal_account
    end
  end

  def handle_braintree
    BraintreeTransaction.uncached do
      transactions = BraintreeTransaction.select(:id).where(payin_provider_config: nil)
      transactions.find_in_batches do |txns|
        child_worker.perform_async(txns.map(&:id), BraintreeTransaction.to_s)
      end
    end
  end

  def handle_stored_credit_card
    StoredCreditCard.uncached do
      accounts = StoredCreditCard.select(:id).where(payin_provider_config: nil)
      accounts.find_in_batches do |accts|
        child_worker.perform_async(accts.map(&:id), StoredCreditCard.to_s)
      end
    end
  end

  def handle_paypal_transaction
    PaypalTransaction.uncached do
      transactions = PaypalTransaction.select(:id).where(payin_provider_config: nil)
      transactions.find_in_batches do |txns|
        child_worker.perform_async(txns.map(&:id), PaypalTransaction.to_s)
      end
    end
  end

  def handle_stored_paypal_account
    StoredPaypalAccount.uncached do
      accounts = StoredPaypalAccount.select(:id).where(payin_provider_config: nil)
      accounts.find_in_batches do |accts|
        child_worker.perform_async(accts.map(&:id), StoredPaypalAccount.to_s)
      end
    end
  end

  def on_success(_status, _options)
    puts "Ok!"
  end
end
