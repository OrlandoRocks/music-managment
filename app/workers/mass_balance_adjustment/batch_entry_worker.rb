class MassBalanceAdjustment::BatchEntryWorker
  include Sidekiq::Worker

  sidekiq_options queue: "low"

  def perform(mass_adjustment_entry_id)
    begin
      MassAdjustmentEntry.transaction do
        @entry = MassAdjustmentEntry.find(mass_adjustment_entry_id)
        logger.info("Processing mass adjustment entry #{mass_adjustment_entry_id}")
        process_entry
      end
    rescue StandardError => e
      message = "Unable to process mass adjustment entry. #{e.message}"
      mark_as_error(message) if @entry
      logger.error message
      logger.error e.backtrace.join("\n")
    end
  end

  private

  def process_entry
    mark_as_error("Person not found") unless valid?
    return if @entry.status.present?

    if @entry.account_goes_negative? && !@entry.mass_adjustment_batch.allow_negative_balance
      mark_as_cancelled("Balance can't go negative") and return
    end

    create_balance_adjustment
  end

  def valid?
    @entry && @entry.person
  end

  def create_balance_adjustment
    person = @entry.person
    batch = @entry.mass_adjustment_batch
    adjustment_amount = @entry.amount

    ba = BalanceAdjustment.new(
      person_id: person.id,
      posted_by_id: batch.created_by_id,
      posted_by_name: Person.find(batch.created_by_id).name,
      credit_amount: adjustment_amount.positive? ? adjustment_amount : 0,
      debit_amount: adjustment_amount.negative? ? -adjustment_amount : 0,
      currency: person.currency,
      category: batch.balance_adjustment_category&.name,
      customer_note: batch.customer_message,
      admin_note: batch.admin_message
    )

    validate = !@entry.account_goes_negative?
    if ba.save(validate: validate)
      mark_as_success
    else
      mark_as_error(ba.errors)
    end
  end

  def mark_as_error(info)
    @entry.update(status: MassAdjustmentEntry::ERROR, status_info: info)
  end

  def mark_as_cancelled(info)
    @entry.update(status: MassAdjustmentEntry::CANCELLED, status_info: info)
  end

  def mark_as_success
    @entry.update(status: MassAdjustmentEntry::SUCCESS)

    return unless @entry.mass_adjustment_batch.processing_completed?

    MassAdjustmentBatch.process_batches_as_completed
    send_mail
  end

  def send_mail
    AdminNotifier
      .mass_balance_adjustment(
        @entry.mass_adjustment_batch,
        mass_adjustment_approvers_emails
      ).deliver
  end

  def mass_adjustment_approvers_emails
    admin_ids = FeatureFlipper.get_users(:mass_balance_adjustment_email)
    Person.where(id: admin_ids).pluck(:email)
  end
end
