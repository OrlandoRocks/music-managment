class MassBalanceAdjustment::BatchCsvWorker
  include Sidekiq::Worker

  def perform(mass_adjust_batch_id)
    @batch_id = mass_adjust_batch_id
    fetch_from_s3_and_process
  end

  private

  def fetch_from_s3_and_process
    file_name = MassAdjustmentBatch.find(@batch_id).s3_file_name
    object = S3_MASS_BALANCE_ADJUSTMENT_CLIENT
             .buckets[ENV["MASS_BALANCE_ADJUSTMENT_BUCKET"]]
             .objects["#{Rails.env}/#{file_name}"]
    process_csv(object)
  end

  def process_csv(object)
    csv_data = object.read
    csv = CSV.parse(csv_data, headers: true)
    entries =
      csv.map do |row|
        person_id = row[MassAdjustmentsForm::PERSON_ID].to_i
        adjustment_amount = row[MassAdjustmentsForm::CORRECTION_AMOUNT].to_d
        person_balance = PersonBalance.find_by(person_id: person_id)&.balance || 0
        balance_turns_negative = (person_balance + adjustment_amount).negative?
        status_info = validate_entry(row)

        MassAdjustmentEntry.new(
          person_id: person_id,
          mass_adjustment_batch_id: @batch_id,
          balance_turns_negative: balance_turns_negative,
          amount: adjustment_amount,
          status_info: status_info.join(",")
        )
      end
    MassAdjustmentEntry.import(entries)
    MassAdjustmentBatch.find(@batch_id).update(status: MassAdjustmentBatch::INGESTED)
  end

  def validate_entry(row)
    errors = []
    person_id = row[MassAdjustmentsForm::PERSON_ID]
    errors << "Invalid person id" unless person_id && person_id.match(/^(\d)+$/)
    errors << "Unknown person id" unless Person.exists?(person_id)

    amount = row[MassAdjustmentsForm::CORRECTION_AMOUNT]
    errors << "Invalid correction amount" unless amount && amount.match(/^-?\d+.?\d+$/)
    errors
  end
end
