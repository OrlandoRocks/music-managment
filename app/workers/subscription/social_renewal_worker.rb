class Subscription::SocialRenewalWorker < Subscription::RenewalWorker
  sidekiq_options retry: 0

  def perform(payment_channel)
    # Android needs to consider all subscriptions for renewal.
    # Assigning filter based on env variable ANDROID_BACKFILL
    # to not affect social renewal worker for apple.
    filter = ENV["BACKFILL_ALL"] ? :all : :today_and_grace_period
    filter = ENV["ANDROID_BACKFILL"] ? :all : :today_and_grace_period if payment_channel == "Android"
    processor = process(filter, "Social", payment_channel)
    push_renewals_to_tunecore_social(processor.renewed) if processor.renewed.any?
    notify_failed_renewals(processor.declined, payment_channel) if processor.declined.any?
  end

  private

  def push_renewals_to_tunecore_social(renewed)
    begin
      Social::PlanStatusUpdater.update(renewed)
    rescue Social::PlanStatusUpdater::PlanStatusError => e
      Tunecore::Airbrake.notify(e)
    end
  end

  def notify_failed_renewals(declined, payment_channel)
    declined.each do |person|
      if notifier_method = notifier_method_for(person.subscription_status_for("Social"), payment_channel)
        SocialNotifierWorker.perform_async(notifier_method, [person.id])
      end
    end
  end

  def notifier_method_for(subscription, payment_channel)
    if subscription.grace_period_ends_in_5_days?
      "#{payment_channel.downcase}_initial_grace_period".to_sym
    elsif subscription.grace_period_ends_in_3_days?
      "#{payment_channel.downcase}_second_grace_period".to_sym
    elsif subscription.grace_period_ends_today?
      "#{payment_channel.downcase}_final_grace_period".to_sym
    end
  end
end
