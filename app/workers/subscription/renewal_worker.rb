class Subscription::RenewalWorker
  include Sidekiq::Worker
  def process(filter, subscription_type, payment_channel)
    query = Subscription::Renewal::QueryBuilder.build(filter, subscription_type, payment_channel)
    Subscription::Renewal::BulkProcessor.renew(query.subscriptions, payment_channel)
  end
end
