# frozen_string_literal: true

class FetchTaxFormsWorker
  include Sidekiq::Worker

  sidekiq_options retry: 5, queue: "low"

  def perform(async_person_id, include_inactive_payee_ids = false)
    return unless Person.exists?(id: async_person_id)
    return TaxFormCheckService.check_api?(async_person_id) unless include_inactive_payee_ids

    Person.find(async_person_id).payout_providers.each do |payout_provider|
      fetch_tax_form_for_payee(payout_provider.client_payee_id, payout_provider.payout_provider_config.program_id)
      fetch_tax_form_for_payee(payout_provider.client_payee_id, PayoutProviderConfig::SECONDARY_TAX_FORM_PROGRAM_ID)
    end
  rescue => e
    Rails.logger.error("FetchTaxFormsWorker failure: person_id: #{async_person_id}, error: #{e}")
    Tunecore::Airbrake.notify("FetchTaxFormsWorker failure: #{e}", person_id: async_person_id)
  end

  def fetch_tax_form_for_payee(payee_id, program_id)
    Payoneer::TaxApiService.new(PartnerPayeeID: payee_id, program_id: program_id).save_tax_form
  end
end
