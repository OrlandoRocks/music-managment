class Apple::AlbumItunesInfoWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, backtrace: 45, retry: 0

  RATE_LIMIT_WAIT_TIMEOUT = 300
  RATE_LIMIT_LOCK_TIMEOUT = 10_000

  TRANSPORTER_RATE_LIMITER = Sidekiq::Limiter.concurrent("transporter-rate-limit", 50, wait_timeout: RATE_LIMIT_WAIT_TIMEOUT, lock_timeout: RATE_LIMIT_LOCK_TIMEOUT)

  sidekiq_retries_exhausted do |msg|
    handle_failure(msg)
  end

  def perform(album_id)
    TRANSPORTER_RATE_LIMITER.within_limit do
      Apple::Album.get_itunes_info(album_id)
    end
  end

  def self.handle_failure(msg)
    ActiveRecord::Base.connection_pool.with_connection do
      album_id = msg["args"][0]
      Rails.logger.error(msg["error_message"])
      Rails.logger.error(msg["error_backtrace"])
      Apple::Album.create_or_update_itunes_status_failure(album_id)
    end
  end
end
