class Royalties::NotifySipWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 1.minute

  def perform
    Royalties::NotifySipService.new.run
  end
end
