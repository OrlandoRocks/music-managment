class Royalties::ComputePersonRoyaltiesWorker
  include Sidekiq::Worker

  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 20.minutes

  def perform(batch_id)
    Royalties::ComputePersonRoyaltiesService.run(batch_id)
  end
end
