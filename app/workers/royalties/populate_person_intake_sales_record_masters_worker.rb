class Royalties::PopulatePersonIntakeSalesRecordMastersWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 20.minutes

  def perform
    Royalties::PopulatePersonIntakeSalesRecordMastersService.new.run
  end
end
