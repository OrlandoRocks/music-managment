class Royalties::CleanUpWorker
  include Sidekiq::Worker

  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 5.minutes

  def perform
    Royalties::CleanUpService.run
  end
end
