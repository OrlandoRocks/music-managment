class Royalties::DataExportNotificationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 10.minutes

  def perform(posting_id)
    Royalties::DataExportNotificationService.run(posting_id)
  end
end
