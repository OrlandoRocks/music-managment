class Royalties::CopySalesRecordsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 2.hours

  def perform
    Royalties::CopySalesRecordsService.new.run
  end
end
