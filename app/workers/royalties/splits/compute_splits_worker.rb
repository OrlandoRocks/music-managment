class Royalties::Splits::ComputeSplitsWorker
  include Sidekiq::Worker

  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 5.minutes

  def perform(batch_id)
    Royalties::Splits::SplitsService.run(batch_id)
  end
end
