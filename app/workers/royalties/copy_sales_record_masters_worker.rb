class Royalties::CopySalesRecordMastersWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 10.minutes

  def perform
    Royalties::CopySalesRecordMastersService.new.run
  end
end
