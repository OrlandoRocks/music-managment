class Royalties::BatchCreationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 20.minutes

  def perform
    Royalties::BatchCreationService.new.run
  end
end
