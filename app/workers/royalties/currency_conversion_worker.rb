class Royalties::CurrencyConversionWorker
  include Sidekiq::Worker
  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 10.minutes

  def perform(batch_id)
    Royalties::CurrencyConversionService.run(batch_id)
  end
end
