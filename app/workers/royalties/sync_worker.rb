class Royalties::SyncWorker
  include Sidekiq::Worker

  sidekiq_options queue: :royalty, retry: 3, backtrace: 45, unique_for: 5.minutes

  def perform(batch_state, old_posting_state, new_posting_state, success_worker)
    Royalties::SyncService.run(batch_state, old_posting_state, new_posting_state, success_worker)
  end
end
