class ArtistMappingWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(options)
    options = options.with_indifferent_access
    gatekeeper = ArtistMapping::Gatekeeper.new(options)
    return unless gatekeeper.can_proceed?

    if gatekeeper.updating?
      ArtistMapping::ArtistIdUpdater.update_artist_id(options)
    else
      ArtistMapping::TicketCreator.create_ticket(options)
      # ArtistMapping::ArtistIdCreator.create_artist_id(options)
    end
  end
end
