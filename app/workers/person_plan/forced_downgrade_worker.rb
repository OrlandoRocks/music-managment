# frozen_string_literal: true

class PersonPlan::ForcedDowngradeWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: 3, backtrace: true

  attr_accessor :person, :target_plan

  def perform(renewal_id)
    renewal = Renewal.find(renewal_id)
    person = renewal.person
    Plans::ForcedDowngradeService.call(person)
  end
end
