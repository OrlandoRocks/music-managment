# frozen_string_literal: true

class PersonPlan::ForcedDowngradeWorkflowWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 3, backtrace: true

  # date format = YYYY-MM-DD
  def perform
    over_all = Sidekiq::Batch.new
    over_all.on(:success, self.class)

    past_grace_period_date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days
    renewals = Renewal.expires_past_date(past_grace_period_date).plans

    over_all.jobs do
      renewals.each do |renewal|
        PersonPlan::ForcedDowngradeWorker.perform_async(renewal.id)
      end
    end
  end

  def on_success(_status, _options)
    puts "Ok!"
  end
end
