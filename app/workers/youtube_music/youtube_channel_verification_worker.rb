class YoutubeMusic::YoutubeChannelVerificationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: 1

  def perform(data_batch)
    YoutubeMusic::YoutubeChannelVerificationService.verify_channels(data_batch)
  end
end
