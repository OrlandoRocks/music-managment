class Takedown::BatchTakedownWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 0, backtrace: true

  def perform(album_ids, send_email, opts)
    Takedown::BatchService.new(
      album_ids: album_ids,
      send_email: send_email, opts: opts
    ).run
  end
end
