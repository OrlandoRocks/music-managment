class Takedown::MassTakedownWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: 0, backtrace: true

  def perform(album_ids, store_ids = [], batch_name = nil, opts = {})
    album_ids.each do |album_id|
      Takedown::ReleaseTakedownWorker.perform_async(
        album_id,
        store_ids,
        batch_name,
        opts
      )
    end
  end
end
