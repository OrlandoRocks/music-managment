class Takedown::ReleaseTakedownWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: 0, backtrace: true

  def perform(album_id, store_ids = [], batch_name, opts)
    Takedown::ReleaseTakedownService.new(
      album_id: album_id,
      store_ids: store_ids,
      batch_name: batch_name,
      opts: opts
    ).run
  end
end
