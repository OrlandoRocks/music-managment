class Takedown::ExpiredRenewalWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 0, backtrace: true

  def perform(album_id, send_email, opts)
    Takedown::ExpiredRenewalService.new(album_id, send_email, opts).run
  end
end
