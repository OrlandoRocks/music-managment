class SipOutboundInvoiceWorker
  include Sidekiq::Worker
  sidekiq_options queue: "critical_invoice"

  def perform(posting_id)
    invoice_processor = SipInvoiceProcessor.new(posting_id)
    invoice_processor.process!
  end
end
