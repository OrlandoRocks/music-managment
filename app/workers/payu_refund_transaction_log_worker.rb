class PayuRefundTransactionLogWorker
  include Concurrent::Async

  def write_to_bucket(refund_id, response)
    bucket_name = ENV.fetch("PAYMENT_GATEWAY_TX_LOG_BUCKET_NAME")
    bucket_path = "#{ENV.fetch('PAYMENT_GATEWAY_TX_LOG_BUCKET_ENV')}/payu"
    file_name = "#{Time.now.iso8601}_payu_refund_#{refund_id}.log"
    data = response.to_yaml

    s3_options = {
      bucket_name: bucket_name,
      bucket_path: bucket_path,
      file_name: file_name,
      data: data
    }

    begin
      S3LogService.write_and_upload(s3_options)
    rescue => e
      message = "PayU transaction log raw response writing to S3 bucket failed"
      Airbrake.notify(message, { error: e, refund_id: refund_id })
    end
  end
end
