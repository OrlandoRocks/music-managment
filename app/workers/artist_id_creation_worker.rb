class ArtistIdCreationWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(options = {})
    options = options.with_indifferent_access

    album = Album.find_by(id: options["album_id"])
    return unless album

    person = Person.find_by(id: options["person_id"])
    ArtistIds::CreationService.create(album, options["artist_urls"].map(&:with_indifferent_access), person)
  end
end
