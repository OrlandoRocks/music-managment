class GetMetadataPriorityWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 3

  def perform(album_id)
    return if album_id.blank?

    Song.includes(:s3_asset)
        .left_joins(:s3_detail)
        .where(album_id: album_id, s3_details: { id: nil })
        .pluck("s3_assets.id")
        .compact
        .each do |s3_asset_id|
      s3_asset = S3Asset.find(s3_asset_id)
      s3_asset.set_asset_metadata!
    end
  end
end
