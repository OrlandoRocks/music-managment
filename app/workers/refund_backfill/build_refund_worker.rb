class RefundBackfill::BuildRefundWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(transaction_id, transaction_type)
    AutoRefunds::RefundBackfill.new(
      transaction_id, transaction_type
    ).process
  end
end
