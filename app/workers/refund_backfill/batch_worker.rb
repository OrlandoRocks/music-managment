class RefundBackfill::BatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0, unique_for: 30.minutes

  def perform(transaction_type)
    transactions = fetch_transactions(transaction_type)
    schedule_backfill_workers(transaction_type, transactions)
  end

  private

  def schedule_backfill_workers(transaction_type, transactions)
    batch = Sidekiq::Batch.new
    batch.description = "Backfill old refunds"
    batch.on(:success, self.class)
    batch.on(:complete, self.class)
    batch.jobs do
      transactions.each do |transaction|
        next if RefundSettlement.find_by(source: transaction)

        RefundBackfill::BuildRefundWorker.perform_async(transaction.id, transaction_type)
      end
    end
  end

  def on_success(_status, _options)
    puts "Successfully backfilled all refunds"
  end

  def on_complete(status, _options)
    puts "#{status.failures} jobs out of #{status.total} have failed"
  end

  def fetch_transactions(transaction_type)
    case transaction_type
    when BraintreeTransaction.name
      fetch_braintree_transactions
    when PaymentsOSTransaction.name
      fetch_payments_os_transactions
    else
      []
    end
  end

  def fetch_braintree_transactions
    BraintreeTransaction
      .joins(:invoice)
      .where(
        action: BraintreeTransaction::REFUND,
        status: BraintreeTransaction::SUCCESS
      )
  end

  def fetch_payments_os_transactions
    PaymentsOSTransaction
      .joins(:invoice)
      .where(
        action: PaymentsOSTransaction::REFUND,
        refund_status: PaymentsOSTransaction::SUCCEEDED
      )
  end
end
