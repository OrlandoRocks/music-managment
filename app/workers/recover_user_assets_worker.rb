class RecoverUserAssetsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    RecoverUserAssets.new(id).recover
  end
end
