class LoginEventGeocodeWorker
  include Sidekiq::Worker

  def perform(login_event_id)
    LoginEventGeocoder.geocode(login_event_id)
    SuspiciousLoginWorker.perform_async(login_event_id)
  end
end
