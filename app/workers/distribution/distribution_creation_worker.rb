class Distribution::DistributionCreationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: 0, backtrace: true

  def perform(album_id, store_short_name, params = {})
    album = Album.find_by(id: album_id)
    return if album.blank?

    DistributionCreator.create(album, store_short_name, params)
  end
end
