class Distribution::MassPriorityRetryWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 0, backtrace: true

  def perform(params)
    Distribution::MassPriorityRetrier.mass_retry(params.with_indifferent_access)
  end
end
