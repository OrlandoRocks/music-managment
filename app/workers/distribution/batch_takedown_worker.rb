class Distribution::BatchTakedownWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 0, backtrace: true

  def perform(album_ids_for_takedown, send_email = false)
    albums_for_takedown = Album.where(id: album_ids_for_takedown)
    Tunecore::Albums::BatchTakedown.process_batch_takedown(
      albums_for_takedown: albums_for_takedown,
      send_email: send_email
    )
  end
end
