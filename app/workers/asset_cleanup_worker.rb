class AssetCleanupWorker
  include Sidekiq::Worker

  def perform(key, bucket)
    AssetCleanupService.new(key: key, bucket: bucket).call
  end
end
