class TargetedOffer::ChangeActivationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 0, backtrace: 45

  def perform(targeted_offer_id, activate, issuing_user_id)
    TargetedOffer::ChangeActivation.change(targeted_offer_id, activate, issuing_user_id)
  end
end
