class TargetedOffer::RemoveTargetedPeopleWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 0, backtrace: 45

  def perform(targeted_offer_id, issuing_user_id)
    TargetedOffer::RemoveTargetedPeople.remove(targeted_offer_id, issuing_user_id)
  end
end
