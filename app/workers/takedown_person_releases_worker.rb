class TakedownPersonReleasesWorker
  include Sidekiq::Worker

  def perform(person_id, admin_id, ip_address)
    TakedownPersonReleasesService.takedown_releases(person_id, admin_id, ip_address)
  end
end
