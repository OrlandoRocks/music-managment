module Social
  class RenderforestRenditionWorker
    include Sidekiq::Worker
    sidekiq_options retry: 5

    def perform(payload)
      project_id = JSON.parse(payload).dig("id")
      return if project_id.blank?

      logger.info "Sidekiq Job initiated RenderforestRenditionWorker, project: #{project_id}"
      url = ENV["SOCIAL_API_BASE_URL"] + "/renderforest/project_status"
      response =
        Faraday.post(url) do |req|
          req.headers["api-key"] = ENV["SOCIAL_API_KEY_BACKEND"]
          req.headers["Content-Type"] = "application/json"
          req.body = payload
        end
      response_body = JSON.parse(response.body)
      raise response_body.dig("errors").join(" ") if response_body.dig("errors")
    end
  end
end
