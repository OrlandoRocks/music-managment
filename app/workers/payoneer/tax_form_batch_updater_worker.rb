# frozen_string_literal: true

module Payoneer
  class TaxFormBatchUpdaterWorker
    include Sidekiq::Worker
    sidekiq_options queue: :critical, retry: true, backtrace: true

    # Kicks off a Sidekiq batch process that calls the Payoneer Tax Form API for every person
    # with a tax form in the given Payoneer program.
    # @param program_id [String] the Payoneer program ID of the tax forms to update.
    def perform(program_id)
      over_all = Sidekiq::Batch.new

      over_all.on(:success, self.class)
      over_all.description = "Updating all tax forms in Payoneer program ID ##{program_id}"
      over_all.jobs do
        Person.uncached do
          people =
            Person
            .select(:id)
            .joins(:tax_forms)
            .where(
              tax_forms: { payout_provider_config: PayoutProviderConfig.by_env.where(program_id: program_id) }
            )

          people.find_in_batches do |people_batch|
            Sidekiq::Client.push_bulk(
              "class" => Payoneer::TaxFormUpdaterWorker,
              "args" => people_batch.map { |x| [x.id] }
            )
          end
        end
      end
    end

    def on_success(_status, _options)
      puts "Success!"
    end
  end
end
