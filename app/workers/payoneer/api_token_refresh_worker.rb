# frozen_string_literal: true

class Payoneer::ApiTokenRefreshWorker
  include Sidekiq::Worker
  sidekiq_options queue: :critical, retry: true, backtrace: true

  # Will hit Rails.cache. if the cache has been busted, will hit
  # an external API and cache the result.
  # (We don't want to create a bunch of useless keys on the lower environments)

  def perform
    return unless Rails.env.production?
    return unless FeatureFlipper.show_feature?(:use_payoneer_v4_api)

    Payoneer::V4::TokenGenerationService.fetch_token
  end
end
