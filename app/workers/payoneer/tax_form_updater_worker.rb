# frozen_string_literal: true

module Payoneer
  class TaxFormUpdaterWorker
    include Sidekiq::Worker
    sidekiq_options queue: :critical, retry: true, backtrace: true

    def perform(person_id)
      person = Person.find(person_id)

      Rails.logger.info("Updating tax forms for: #{person.name} (#{person.id})")
      Payoneer::TaxApiService
        .new(PartnerPayeeID: person.client_payee_id)
        .save_tax_form
    end
  end
end
