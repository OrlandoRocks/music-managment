# frozen_string_literal: true

class DisassociatePayoneerBackfill::DisassociatePayoneerWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: 5, backtrace: true

  def perform(async_person_id)
    puts "Job started"
    person = Person.find_by(id: async_person_id)
    return if person.blank?

    Payoneer::DisassociatePayoneerService.new(person)
  end
end
