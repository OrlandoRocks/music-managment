# frozen_string_literal: true

class DisassociatePayoneerBackfill::WorkflowWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: true, backtrace: true

  # This worker can be called with or without args.
  # If you do not specify person_ids, this worker runs for users with deleted TC accounts.
  def perform(person_ids = nil)
    over_all = Sidekiq::Batch.new

    over_all.on(:success, self.class)
    over_all.description = "Disassociate Payoneer payout provider with deleted users' accounts"
    over_all.jobs do
      Person.uncached do
        person_ids ||= deleted_payoneer_person_ids

        person_ids.in_groups_of(1000, false) do |batch|
          Sidekiq::Client.push_bulk(
            "class" => DisassociatePayoneerBackfill::DisassociatePayoneerWorker,
            "args" => batch.map { |x| [x] }
          )
        end
      end
    end
  end

  def deleted_payoneer_person_ids
    deleted_people = Person.where(deleted: 1)
                           .or(Person.where(id: DeletedAccount.all.pluck(:person_id)))

    deleted_people.joins(:payout_providers)
                  .where(payout_providers:
                    {
                      name: "payoneer",
                      provider_status: ["approved", "onboarding", "pending"]
                    })
                  .pluck(:id)
  end

  def on_success(_status, _options)
    puts "Completed!"
  end
end
