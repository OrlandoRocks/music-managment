require "./lib/slack_notifier"

class Braintree::AccountUpdaterWorker
  include Sidekiq::Worker

  TOKEN_COLUMN = "payment_method_token"
  UPDATE_TYPE_COLUMN = "update_type"
  CC_NUMBER_COLUMN = "new_last_4"
  EXPIRY_DATE_COLUMN = "new_expiration"

  def perform(csv_file_url)
    logger.info("Started sidekiq job to process Braintree Account Updater csv #{csv_file_url}")
    csv_file = open(csv_file_url).read
    account_updater = Braintree::AccountUpdaterService.new
    CSV.parse(csv_file, headers: true) do |row|
      account_updater.process(
        row[TOKEN_COLUMN],
        row[UPDATE_TYPE_COLUMN],
        row[CC_NUMBER_COLUMN],
        row[EXPIRY_DATE_COLUMN]
      )
    end
    logger.info("Completed processing Braintree Account Updater csv #{csv_file_url}")
    notify_in_slack("Account Updater: successfully - #{Time.now.getutc}", "Renewals-production")
  end
end
