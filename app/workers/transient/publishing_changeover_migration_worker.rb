class Transient::PublishingChangeoverMigrationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 3

  def perform
    logger.info "Sidekiq Job Transient::PublishingChangeoverMigrationWorker"

    Transient::PublishingChangeover::PublishingChangeoverMigrator.migrate
  end
end
