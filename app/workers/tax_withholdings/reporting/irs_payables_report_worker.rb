class TaxWithholdings::Reporting::IRSPayablesReportWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: true, backtrace: 45, unique_for: 30.minutes

  attr_accessor :irs_transaction, :report_csv_filepath, :report_stats

  def perform(irs_transaction_id = nil)
    make_log_entry("Report generation process initialized")
    return unless ENV.fetch("ADMIN_TAX_WITHHOLDINGS_REPORT_RECIPIENTS")
    return unless FeatureFlipper.show_feature?(:us_tax_withholding)

    @irs_transaction =
      if irs_transaction_id.present?
        make_log_entry("Using given IRSTransaction ID")
        IRSTransaction.find_by(id: irs_transaction_id)
      else
        make_log_entry("Creating new IRSTransaction using newly summarized withholdings")
        TaxWithholdings::Reporting::WithholdingsSummarizationService.new.summarize!
      end
    return unless irs_transaction.is_a?(IRSTransaction)

    generate_csv
    upload_csv
    mail_report
  end

  private

  def generate_csv
    make_log_entry("Generating CSV file")
    filename =
      TaxWithholdings::IRSPayablesReport.new(irs_transaction: irs_transaction).filename

    self.report_csv_filepath, self.report_stats =
      TaxWithholdings::Reporting::IRSPayablesReportCsvService
      .new
      .generate_csv!(irs_transaction, filename)
  end

  def upload_csv
    make_log_entry("Uploading CSV to AWS")
    RakeDatafileService.upload(report_csv_filepath, overwrite: "y")
  end

  def mail_report
    make_log_entry("Mailing report")

    # TODO: Consider creating a Sidekiq worker for handling this mailer
    AdminTaxReportsMailer
      .weekly_tax_withholdings_summary_report(
        report_csv_filepath,
        report_stats,
        ENV.fetch("ADMIN_TAX_WITHHOLDINGS_REPORT_RECIPIENTS")
      ).deliver!
  end

  def make_log_entry(message)
    log_str = "[IRS Payables Report] #{message}"
    log_str += "\n  IRS TRANSACTION ID: #{irs_transaction.id}" if irs_transaction.present?
    log_str += "\n  CSV FILENAME: #{report_csv_filepath}" if report_csv_filepath.present?
    Rails.logger.info(log_str)
  end
end
