class TaxWithholdings::MassAdjustmentsBatchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: true, backtrace: 45, unique_for: 30.minutes

  def perform(year = DateTime.current.year)
    return unless FeatureFlipper.show_feature?(:us_tax_withholding)

    transactions_filter_year        = DateTime.strptime(String(year), "%Y")
    @transactions_filter_date_range = [transactions_filter_year.beginning_of_year, transactions_filter_year.end_of_year]

    return if person_transaction_ids.blank?

    over_all = Sidekiq::Batch.new
    over_all.on(:success, self.class)
    over_all.description = "Tax Withholding for Mass Balance Adjustment Transactions"
    over_all.jobs do
      person_transaction_ids.each do |person_transaction_id|
        TaxWithholdings::IRSWithholdingsServiceWorker.perform_async(person_transaction_id)
      end
    end
  end

  def person_transaction_ids
    @person_transaction_ids ||=
      PersonTransaction
      .between_dates(*@transactions_filter_date_range)
      .for_us_taxable_balance_adjustment_target
      .with_no_irs_tax_withholdings
      .joins(:person)
      .merge(Person.for_united_states_and_territories.with_no_valid_taxforms_or_associations)
      .distinct
      .order(:created_at, :id)
      .pluck("person_transactions.id")
  end

  def on_success(_status, _options)
    Rails.logger.info(
      "Tax Withholding Done for Mass Balance Adjustment: #{DateTime.now.strftime('%m/%d/%Y %I:%M:%S %P')}"
    )
  end
end
