class TaxWithholdings::IRSWithholdingsServiceWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: true, backtrace: 45, unique_for: 30.minutes

  def perform(person_transaction_id)
    return unless FeatureFlipper.show_feature?(:us_tax_withholding)

    person_transaction = PersonTransaction.find_by(id: person_transaction_id)
    return if person_transaction.blank?

    TaxWithholdings::IRSWithholdingsService.new(person_transaction).call
  end
end
