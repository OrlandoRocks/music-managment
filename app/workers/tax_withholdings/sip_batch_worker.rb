class TaxWithholdings::SipBatchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: true, backtrace: 45, unique_for: 30.minutes

  attr_accessor :year

  def perform(year = Time.current.year)
    return unless FeatureFlipper.show_feature?(:us_tax_withholding)

    @year = year
    return if latest_taxable_transaction_for_person_ids.blank?

    over_all = Sidekiq::Batch.new
    over_all.description = <<-DESCRIPTION
      Processes tax withholdings for all distribution
      transactions without IRS tax withholdings
    DESCRIPTION

    over_all.on(:success, self.class)
    over_all.jobs do
      latest_taxable_transaction_for_person_ids.each do |person_transaction_id|
        TaxWithholdings::IRSWithholdingsServiceWorker.perform_async(person_transaction_id)
      end
    end
  end

  # TODO: change this once tests pass.
  def person_transaction_ids
    latest_taxable_transaction_for_person_ids
  end

  def on_success(_status, _options)
    Rails.logger.info "Successfully processed tax withholdings"
  end

  private

  def latest_taxable_transaction_for_person_ids
    @latest_taxable_transaction_for_person_ids ||=
      person_transaction_ids_filter_relation
      .select(:id, :person_id, :created_at)
      .group_by(&:person_id)
      .transform_values { |transactions| transactions.max_by(&:created_at)&.id }
      .values
  end

  def person_transaction_ids_filter_relation
    date = Time.new(year)

    PersonTransaction
      .with_no_irs_tax_withholdings
      .joins(:person)
      .merge(Person.for_united_states_and_territories.with_no_valid_taxforms_or_associations)
      .for_us_taxable_distribution_targets
      .where(
        created_at: date.beginning_of_year..date.end_of_year,
        credit: 0..
      )
      .order(:created_at, :id)
  end
end
