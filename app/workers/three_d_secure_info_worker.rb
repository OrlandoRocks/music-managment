class ThreeDSecureInfoWorker
  include Sidekiq::Worker

  def perform(braintree_transaction_id)
    Braintree::ThreeDSecureInfoUpdateService.new.process(braintree_transaction_id)
  end
end
