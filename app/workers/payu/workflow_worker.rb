# frozen_string_literal: true

class Payu::WorkflowWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: true, backtrace: true

  # date format = YYYY-MM-DD
  def perform
    over_all = Sidekiq::Batch.new
    over_all.on(:success, self.class)

    payu_invoices_issued_on_date =
      Invoice
      .joins(:invoice_settlements)
      .where(invoice_settlements: { source_type: PayuTransaction.to_s })
      .where(
        no_response_sql, PayuInvoiceUploadResponse::SUCCESS_RESPONSE_CODE
      )

    over_all.jobs do
      payu_invoices_issued_on_date.each do |invoice|
        Payu::InvoiceUploadWorker.perform_async(invoice.id)
      end
    end
  end

  def no_response_sql
    <<-SQL
    invoices.id not in (
      select distinct#{' '}
      payu_transactions.invoice_id#{' '}
      from payu_invoice_upload_responses
      join payu_transactions on payu_transactions.id = payu_invoice_upload_responses.related_id and payu_invoice_upload_responses.related_type = "PayuTransaction"#{' '}
      where payu_invoice_upload_responses.api_response_code = ?
    )
    SQL
  end

  def on_success(_status, _options)
    puts "Ok!"
  end
end
