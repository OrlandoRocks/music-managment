# frozen_string_literal: true

class Payu::InvoiceUploadWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default, retry: 3, backtrace: true

  def perform(invoice_id)
    puts "Job started"
    invoice = Invoice.find(invoice_id)
    Payu::InvoiceUploadService.new(invoice).upload
  end
end
