module Adyen
  class RecurringDetailsUpdateWorker
    include Sidekiq::Worker

    sidekiq_options queue: :default

    def perform(adyen_transaction_id)
      adyen_transaction = AdyenTransaction.find(adyen_transaction_id)

      Adyen::FetchRecurringDetailsService.call!(adyen_transaction)
    end
  end
end
