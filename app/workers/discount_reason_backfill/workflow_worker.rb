# frozen_string_literal: true

class DiscountReasonBackfill::WorkflowWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 3, backtrace: true

  def perform
    over_all = Sidekiq::Batch.new
    over_all.on(:success, self.class)

    purchases_with_null_discount_reason = Purchase.where(discount_reason: nil)

    over_all.jobs do
      purchases_with_null_discount_reason.in_batches do |purchases|
        DiscountReasonBackfill::PurchaseDiscountReasonWorker.perform_async(purchases.pluck(:id))
      end
    end
  end

  def on_success(_status, _options)
    puts "Ok!"
  end
end
