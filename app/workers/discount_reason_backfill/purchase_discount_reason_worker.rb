# frozen_string_literal: true

class DiscountReasonBackfill::PurchaseDiscountReasonWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: 3, backtrace: true

  def perform(purchase_ids)
    puts "Job started"

    purchase_ids.each do |purchase_id|
      purchase = Purchase.find(purchase_id)

      reason =
        if purchase.discount_cents.nil? || purchase.discount_cents.zero?
          Purchase::NO_DISCOUNT
        else
          Purchase::NON_PLAN_DISCOUNT
        end

      Rails.logger.info("Setting Default discount_reason to '#{reason}' for purchase id:#{purchase.id}")
      # Using update_attribute here to allow this process to work with bad historical data
      # rubocop:disable SkipsModelValidations
      purchase.update_attribute :discount_reason, reason
      # rubocop:enable SkipsModelValidations
    end
  end
end
