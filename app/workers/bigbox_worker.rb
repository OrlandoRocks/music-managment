class BigboxWorker
  include Sidekiq::Worker
  def perform(queue_name, job_type, locker_name, urls, aws_access_key_id, aws_secret_access_key, options = {})
    BigboxCommunicator.enqueue_for_bigbox(
      queue_name,
      job_type,
      locker_name,
      urls,
      aws_access_key_id,
      aws_secret_access_key,
      options
    )
  end
end
