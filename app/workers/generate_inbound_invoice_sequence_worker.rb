class GenerateInboundInvoiceSequenceWorker
  include Sidekiq::Worker
  sidekiq_options queue: "critical"

  def perform(invoice_id)
    invoice = Invoice.find invoice_id
    new_number = Invoice.last_generated_invoice_number(invoice.corporate_entity).to_i + 1
    invoice.update!(invoice_sequence: new_number)
  end
end
