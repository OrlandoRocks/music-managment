class ArtistIdMatcherWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3, queue: :low

  def perform(creative_ids)
    creatives = Creative.includes(:external_service_id).where(id: creative_ids)
    ArtistIds::CreationService.match(creatives)
  end
end
