# frozen_string_literal: true

class PayuRedirectReceiptWorker
  include Sidekiq::Worker

  def perform(invoice_id)
    bucket_name = ENV.fetch("PAYMENT_GATEWAY_TX_LOG_BUCKET_NAME")
    bucket_path = "#{ENV.fetch('PAYMENT_GATEWAY_TX_LOG_BUCKET_ENV')}/payu/redirects"
    file_name   = "#{invoice_id}_#{Time.now.iso8601}.log"

    s3_options = {
      bucket_name: bucket_name,
      bucket_path: bucket_path,
      file_name: file_name,
      data: invoice_id.to_s
    }

    begin
      S3LogService.write_and_upload(s3_options)
    rescue StandardError => e
      Airbrake.notify(self.class.name, { error: e, invoice_id: invoice_id })
    end
  end
end
