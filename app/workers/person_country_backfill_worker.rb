# frozen_string_literal: true

class PersonCountryBackfillWorker
  include Sidekiq::Worker
  sidekiq_options unique_for: 30.minutes, queue: "default"

  def perform(method_name)
    case method_name
    when "backfill_all"
      Person::CountryBackfillService.perform_backfill("all")
    when "backfill_country_audit"
      Person::CountryBackfillService.perform_backfill("country_audit")
    when "backfill_payoneer_kyc"
      Person::CountryBackfillService.perform_backfill("payoneer_kyc")
    when "backfill_invalid"
      Person::CountryBackfillService.perform_backfill("invalid")
    end
  end
end
