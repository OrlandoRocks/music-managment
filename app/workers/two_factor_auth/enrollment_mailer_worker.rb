class TwoFactorAuth::EnrollmentMailerWorker
  include Sidekiq::Worker

  def perform(person_id)
    person = Person.find(person_id)

    TwoFactorAuth::EnrollmentMailer.send_enrollment_mailer(person).deliver
  end
end
