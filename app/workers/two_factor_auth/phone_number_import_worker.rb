class TwoFactorAuth::PhoneNumberImportWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 1

  LIMIT = Sidekiq::Limiter.concurrent(
    "twilio-import-rate-limit",
    15,
    wait_timeout: 15,
    lock_timeout: 60
  )

  def perform(tfa_id)
    LIMIT.within_limit do
      TwoFactorAuth::PhoneNumberImportService.import(tfa_id)
    end
  end
end
