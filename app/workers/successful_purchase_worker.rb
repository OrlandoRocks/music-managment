class SuccessfulPurchaseWorker
  include Sidekiq::Worker

  sidekiq_options queue: :delivery_post_purchase, retry: 2

  def perform(invoice_id)
    logger.info "Sidekiq Job initiated SuccessfulPurchaseWorker: #{invoice_id}"
    invoice = Invoice.find(invoice_id)
    SuccessfulPurchaseService.process(invoice)
  end
end
