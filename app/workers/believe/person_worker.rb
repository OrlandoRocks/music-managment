class Believe::PersonWorker
  include Sidekiq::Worker
  sidekiq_options queue: :believe

  def perform(person_id)
    return unless ActiveModel::Type::Boolean.new.cast(ENV.fetch("NOTIFY_BELIEVE", false))

    # There are a lot of failures from the believe
    # side. So we are temporarily disabling this
    # https://tunecore.atlassian.net/browse/TEC-438
    Believe::Person.add_or_update(person_id)
  end
end
