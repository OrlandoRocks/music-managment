class Soundout::FetchWorker
  include Sidekiq::Worker

  def perform(soundout_report_id)
    report = SoundoutReport.find(soundout_report_id)
    status = report.soundout_product.fetch_report(report).first
    raise "Error downloading report from soundout" unless status
  end
end
