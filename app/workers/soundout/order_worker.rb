class Soundout::OrderWorker
  include Sidekiq::Worker

  def perform(soundout_report_id)
    report = SoundoutReport.find(soundout_report_id)
    soundout_id = report.soundout_product.order_report(report)
    raise "Error requesting report from soundout" unless soundout_id
  end
end
