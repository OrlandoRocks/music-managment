class CurrencyLayer::GetForeignExchangeRatesWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: false

  def perform
    # Get today's exchange rates of all currencies that needs VAT calculation
    # Believe headquarters is in France and VAT will be paid there
    service = Currencylayer::ExchangeService.new
    CountryWebsite::VAT_CURRENCIES.each do |source_currency|
      service.store_exchange_rates(source_currency, CountryWebsite.find(CountryWebsite::FRANCE).currency)
    end

    # Get forex rates for USD
    CountryWebsite.select(:currency).distinct.pluck(:currency).excluding("USD").each do |source_currency|
      service.store_exchange_rates(source_currency, CountryWebsite.find(CountryWebsite::UNITED_STATES).currency)
    end
  rescue => e
    Rails.logger.error("Get latest forex rates rake task failed #{e}")
    Tunecore::Airbrake.notify("Unable to get latest forex rates", e)
  end
end
