class CurrencyLayer::SoutheastAsianForeignExchangeRateFetchWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: false

  def perform
    south_east_asian_currencies =
      Country::SOUTH_EAST_ASIAN_COUNTRY_CURRENCY_MAP.values

    service = Currencylayer::ExchangeService.new
    CountryWebsite.distinct.pluck(:currency).each do |source_currency|
      service.store_exchange_rates(
        source_currency,
        south_east_asian_currencies.join(",")
      )
    end
  rescue => e
    Rails.logger.error("Get latest forex rates rake task failed #{e}")
    Tunecore::Airbrake.notify("Unable to get latest forex rates", e)
  end
end
