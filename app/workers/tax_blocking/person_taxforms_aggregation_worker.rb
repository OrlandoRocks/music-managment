class TaxBlocking::PersonTaxformsAggregationWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 3

  def perform(person_id, *years)
    TaxBlocking::TaxformInfoUpdateService.new(person_id, *years).summarize
  end
end
