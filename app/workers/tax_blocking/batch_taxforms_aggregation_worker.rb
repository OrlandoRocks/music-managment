class TaxBlocking::BatchTaxformsAggregationWorker
  include Sidekiq::Worker

  def perform(years)
    @summarizable_years = parse_years(years)
    over_all = Sidekiq::Batch.new
    over_all.description = "Summarizes tax forms users for years: #{years}"
    over_all.jobs do
      summarizable_person_ids.each do |person_id|
        TaxBlocking::PersonTaxformsAggregationWorker.perform_async(
          person_id,
          Range.new(
            *(@summarizable_years.map { |i| Integer(i, 10) })
          ).to_a
        )
      end
    end
  end

  private

  def summarizable_person_ids
    query_years = @summarizable_years.map { |year| DateTime.strptime(year, "%Y") }
    Person
      .joins(:tax_forms)
      .where(
        people: { country: Country::UNITED_STATES_AND_TERRITORIES },
        tax_forms: {
          submitted_at: query_years.min.beginning_of_year..query_years.max.end_of_year
        }
      )
      .distinct("people.id")
      .pluck("people.id")
  end

  def parse_years(years)
    return [String(DateTime.current.year)] if years.blank?

    String(years)
      .split("..")
      .map(&:strip)
      .compact
      .uniq
  end
end
