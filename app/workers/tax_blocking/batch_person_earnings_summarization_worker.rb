class TaxBlocking::BatchPersonEarningsSummarizationWorker
  include Sidekiq::Worker

  attr_accessor :limit, :offset, :country, :years, :posting_ids, :batch_balance_adjustment_ids

  # Params:
  # - limit - the amount of records to query
  # - offset - 0-based offset of records returned by the query
  # - country - 2 character representation of a country to find people from
  # - years - the year or array of years of earnings to return
  # - posting_ids - limit results to people found for specific posting_ids
  # - batch_balance_adjustment_ids - limit results to people found for specific batch_balance_adjustment_ids
  def perform(params = {})
    set_attributes(params)

    over_all = Sidekiq::Batch.new
    over_all.description = "summarizes annual earnings of users for the years: #{years}"
    over_all.on(:success, self.class)
    over_all.jobs do
      person_ids.each do |person_id|
        PersonEarningsSummarizationWorker.perform_async(
          person_id,
          years.map { |year_int| String(year_int) }
        )
      end
    end
  end

  def person_ids
    return @person_ids if @person_ids.present?

    @person_ids =
      Person
      .select(:id)
      .distinct
      .joins(:person_transactions)
      .where(country: country)
      .where(
        "EXTRACT(YEAR FROM person_transactions.created_at) BETWEEN ? AND ?",
        years.min, years.max
      )
      .limit(limit).offset(offset)
      .pluck(:id)

    return @person_ids if limit || offset.positive?

    filter_by_posting_ids
    filter_by_batch_balance_adjustments

    @person_ids
  end

  def on_success(_status, _options)
    puts "Successfully summarized person earnings for all the people in the queue"
  end

  private

  def filter_by_posting_ids
    return if posting_ids.none?

    posting_person_ids = []
    posting_person_ids +=
      SalesRecord
      .select(:person_id)
      .where(posting_id: posting_ids)
      .distinct(:person_id)
      .pluck(:person_id)
    posting_person_ids +=
      YouTubeRoyaltyRecord
      .select(:person_id)
      .where(posting_id: posting_ids)
      .distinct(:person_id)
      .pluck(:person_id)

    @person_ids &= posting_person_ids
  end

  def filter_by_batch_balance_adjustments
    return if batch_balance_adjustment_ids.blank?

    bba_person_ids =
      BatchBalanceAdjustmentDetail
      .select(:person_id)
      .where(batch_balance_adjustment_id: batch_balance_adjustment_ids)
      .distinct(:person_id)
      .pluck(:person_id)

    @person_ids &= bba_person_ids
  end

  def set_attributes(params)
    params.transform_keys!(&:to_sym).slice!(
      :limit, :offset, :country, :years, :posting_ids, :batch_balance_adjustment_ids
    )
    self.limit = params[:limit]
    self.offset = params.fetch(:offset, 0)
    self.country = params.fetch(:country, Country::UNITED_STATES_AND_TERRITORIES)
    self.years = params.fetch(
      :years, [DateTime.current.year]
    )
    self.posting_ids = params.fetch(:posting_ids, [])
    self.batch_balance_adjustment_ids = params.fetch(:batch_balance_adjustment_ids, [])
  end
end
