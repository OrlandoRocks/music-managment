class TaxBlocking::TaxableEarningsRolloverWorker
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: true

  def perform(person_id, year = 2021)
    return unless Person.exists?(id: person_id)

    TaxBlocking::TaxableEarningsRolloverService.new(person_id, year).call
  end
end
