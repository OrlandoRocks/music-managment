class ClearFailedLoginAttemptsWorker
  include Sidekiq::Worker

  def perform(person_id)
    ClearFailedLoginAttemptsService.clear(person_id)
  end
end
