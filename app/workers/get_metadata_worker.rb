class GetMetadataWorker
  include Sidekiq::Worker

  sidekiq_options queue: :critical, retry: 3

  def perform(s3_asset_id)
    return if s3_asset_id.blank?

    begin
      s3_asset = S3Asset.find(s3_asset_id)
      s3_asset.set_asset_metadata!
    rescue ActiveRecord::RecordNotFound
      nil
    end
  end
end
