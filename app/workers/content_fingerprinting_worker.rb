class ContentFingerprintingWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: false

  def perform(song_ids)
    Song.where(id: song_ids).each do |song|
      begin
        song.create_and_store_content_fingerprint!
      rescue => e
        Airbrake.notify("ContentFingerprintingWorker: Fingerprinting error", e) if Rails.env.production?
      end
    end
  end
end
