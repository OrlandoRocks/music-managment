class CrtCacheWorker
  include Sidekiq::Worker

  def perform(album_id)
    CrtCacheWriter.write_album_to_cache(album_id)
  end
end
