module Disputes
  class CurrencyConversionWorker
    include Sidekiq::Worker
    sidekiq_options queue: :critical, retry: 3

    def perform(dispute_id)
      AmountUpdateService.call!(dispute_id)
    end
  end
end
