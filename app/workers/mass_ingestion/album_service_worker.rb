class MassIngestion::AlbumServiceWorker
  include Sidekiq::Worker

  sidekiq_options retry: 3, queue: :critical

  def perform(bucket_name, bucket_album_dir)
    puts("picking up ingestion processing for - #{bucket_name}, #{bucket_album_dir}")
    album_service = MassIngestion::AlbumService.new(bucket_name, bucket_album_dir)
    album_service.process
  end
end
