class WithdrawalMismatchReportWorker
  include Sidekiq::Worker
  sidekiq_options queue: "critical", retry: true

  def perform
    report_csv = WithdrawalMismatchReportService.new.generate_report
    return false if report_csv.eql?(:no_mismatched_transactions_found)

    AdminWithdrawalMismatchReportMailer
      .daily_withdrawal_amount_mismatch_report(report_csv, ENV["WITHDRAWAL_MISMATCH_REPORT_RECIPIENTS"])
      .deliver!
  end
end
