require "./lib/slack_notifier.rb"
class RoyaltySolutionsStatusWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0

  def perform
    return unless invoice_export_enabled?

    last_record = TunecoreTracking.last
    return if last_record.present? && last_record.created_at > 2.days_ago

    notify_in_slack(
      "ALERT: no new TunecoreTracking records have been created in the past 48 hours",
      "Royalty Solutions"
    )
  end

  def invoice_export_enabled?
    ActiveModel::Type::Boolean.new.cast(
      ENV.fetch("ENABLE_INVOICE_EXPORT", "false").to_s
    )
  end
end
