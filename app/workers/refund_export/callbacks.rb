# frozen_string_literal: true

class RefundExport::Callbacks
  def handle_refund_export_notification(status, options)
    RefundExportMailer.invoice_export_complete(options["invoice_type"], status).deliver
  end
end
