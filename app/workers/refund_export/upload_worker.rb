# frozen_string_literal: true

class RefundExport::UploadWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(invoice_id, invoice_type)
    case invoice_type.to_sym
    when :outbound
      outbound_refund = OutboundRefund.find_by(id: invoice_id)
      CreditNoteInvoiceGenerator::UploadOutboundInvoicePdf.new(outbound_refund).process
    when :inbound
      refund = Refund.find_by(id: invoice_id)
      CreditNoteInvoiceGenerator::UploadInboundInvoicePdf.new(refund).process
    end
  end
end
