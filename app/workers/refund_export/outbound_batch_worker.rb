# frozen_string_literal: true

class RefundExport::OutboundBatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0, unique_for: 12.hours

  def perform(start_date = nil, end_date = nil)
    return unless invoice_export_enabled?

    start_date = start_date ? Time.zone.parse(start_date) : Time.zone.today
    end_date = end_date ? Time.zone.parse(end_date) : Time.zone.today

    outbound_refund_ids = OutboundRefund.where(fetch_query(start_date, end_date))
                                        .where.not(invoice_sequence: nil)
                                        .pluck(:id)
    schedule_upload_workers(outbound_refund_ids)
  end

  def schedule_upload_workers(outbound_refund_ids)
    batch = Sidekiq::Batch.new
    batch.description = "Export Refund Outbound invoices to S3"
    call_back_name = "RefundExport::Callbacks#handle_refund_export_notification"
    batch.on(:complete, call_back_name, invoice_type: :outbound)

    batch.jobs do
      outbound_refund_ids.each do |outbound_refund_id|
        RefundExport::UploadWorker.perform_async(outbound_refund_id, :outbound)
      end
    end
  end

  def fetch_query(start_date, end_date)
    query = []
    query << "created_at >= '#{start_date.beginning_of_day}'"
    query << "created_at <= '#{end_date.end_of_day}'"
    query&.join(" AND ")
  end

  def invoice_export_enabled?
    ActiveModel::Type::Boolean.new.cast(
      ENV.fetch("ENABLE_INVOICE_EXPORT", "false").to_s
    )
  end
end
