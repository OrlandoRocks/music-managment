# frozen_string_literal: true

class RefundExport::InboundBatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0, unique_for: 12.hours

  def perform(start_date = nil, end_date = nil)
    return unless invoice_export_enabled?

    start_date = start_date ? Time.zone.parse(start_date) : Time.zone.yesterday
    end_date = end_date ? Time.zone.parse(end_date) : Time.zone.yesterday

    refund_ids = Refund.joins(invoice: :invoice_static_corporate_entity)
                       .where(fetch_query(start_date, end_date))
                       .where.not(invoice_sequence: nil)
                       .pluck(:id)
    schedule_upload_workers(refund_ids)
  end

  def schedule_upload_workers(refund_ids)
    batch = Sidekiq::Batch.new
    batch.description = "Export Refund Inbound invoices to S3"
    call_back_name = "RefundExport::Callbacks#handle_refund_export_notification"
    batch.on(:complete, call_back_name, invoice_type: :inbound)

    batch.jobs do
      refund_ids.each do |invoice_id|
        RefundExport::UploadWorker.perform_async(invoice_id, :inbound)
      end
    end
  end

  def fetch_query(start_date, end_date)
    query = []
    query << "refunds.created_at >= '#{start_date.beginning_of_day}'"
    query << "refunds.created_at <= '#{end_date.end_of_day}'"
    query&.join(" AND ")
  end

  def invoice_export_enabled?
    ActiveModel::Type::Boolean.new.cast(
      ENV.fetch("ENABLE_INVOICE_EXPORT", "false").to_s
    )
  end
end
