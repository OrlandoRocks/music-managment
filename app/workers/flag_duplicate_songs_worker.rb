class FlagDuplicateSongsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low

  def perform(album_id)
    begin
      album_object = Album.find(album_id)
      create_and_store_hash(album_object)
      album_object.flag_duplicate_content
    rescue Exception => e
      Tunecore::Airbrake.notify(e, { album_id: album_id })
      Rails.logger.error(e.message)
    end
  end

  private

  def create_and_store_hash(album)
    album.songs.each(&:create_and_store_content_fingerprint!)
  end
end
