class SpotifyUriWorker
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def self.perform_now(method, *args)
    new.perform(method, *args)
  end

  def perform(method, *args)
    SpotifyService.send(method, *args)
  end
end
