class GenerateCreditNoteInvoiceSequenceWorker
  include Sidekiq::Worker
  sidekiq_options queue: "critical"

  def perform(refund_id)
    refund = Refund.find(refund_id)
    last_sequence = Refund.last_generated_invoice_sequence(refund.corporate_entity)

    refund.update!(invoice_sequence: last_sequence + 1)
  end
end
