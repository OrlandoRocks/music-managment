class RoyaltySplitsMailerWorker
  include Sidekiq::Worker
  sidekiq_options queue: :mailers, retry: 3, backtrace: true

  def perform(method, args)
    RoyaltySplitsMailer.send(method, **args.symbolize_keys).deliver
  end
end
