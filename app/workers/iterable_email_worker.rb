# frozen_string_literal: true

class IterableEmailWorker
  include Sidekiq::Worker
  include Api::Iterable
  sidekiq_options queue: :mailers, retry: 3, backtrace: true

  def perform(campaign_id, recipient_email, data_fields)
    data_fields = Oj.load(data_fields)

    Api::Iterable.api_post(
      "email/target",
      {
        campaignId: Integer(campaign_id),
        recipientEmail: String(recipient_email),
        dataFields: Hash(data_fields)
      }
    )
  end
end
