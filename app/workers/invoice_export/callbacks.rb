class InvoiceExport::Callbacks
  def handle_inbound_export_notification(status, options)
    invoice_ids = options.fetch("invoice_ids")
    InvoiceExportMailer.invoice_export_complete(:inbound, status).deliver
  end

  def handle_outbound_export_notification(status, options)
    invoice_ids = options.fetch("invoice_ids")
    InvoiceExportMailer.invoice_export_complete(:outbound, status).deliver
  end
end
