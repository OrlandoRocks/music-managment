class InvoiceExport::UploadWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(invoice_id, invoice_type)
    case invoice_type
    when "outbound"
      invoice = OutboundInvoice.find_by(id: invoice_id)
      InvoiceGenerator::UploadOutboundInvoicePdf.new(invoice).process
    when "inbound"
      invoice = Invoice.find_by(id: invoice_id)
      InvoiceGenerator::UploadInboundInvoicePdf.new(invoice).process
    end
  end
end
