class InvoiceExport::OutboundBatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0, unique_for: 12.hours

  def perform(start_date = nil, end_date = nil)
    return unless invoice_export_enabled?

    start_date = start_date ? Time.zone.parse(start_date) : Time.zone.yesterday
    end_date = end_date ? Time.zone.parse(end_date) : Time.zone.yesterday

    invoice_ids = OutboundInvoice.where(fetch_query(start_date, end_date)).pluck(:id)
    schedule_upload_workers(invoice_ids)
  end

  def schedule_upload_workers(invoice_ids)
    batch = Sidekiq::Batch.new
    batch.description = "Export Outbound invoices to S3"
    call_back_name = "InvoiceExport::Callbacks#handle_outbound_export_notification"
    batch.on(:complete, call_back_name, invoice_ids: invoice_ids)

    batch.jobs do
      invoice_ids.each do |invoice_id|
        InvoiceExport::UploadWorker.perform_async(invoice_id, :outbound)
      end
    end
  end

  def fetch_query(start_date, end_date)
    query = []
    query << "invoice_sequence is not null"
    query << "invoice_date >= '#{start_date.beginning_of_day}'"
    query << "invoice_date <= '#{end_date.end_of_day}'"
    query&.join(" AND ")
  end

  def invoice_export_enabled?
    ActiveModel::Type::Boolean.new.cast(
      ENV.fetch("ENABLE_INVOICE_EXPORT", "false").to_s
    )
  end
end
