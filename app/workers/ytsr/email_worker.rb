class Ytsr::EmailWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(person_id, template_name, message_data)
    PersonNotifier.youtube_sound_recording_update(
      person_id, template_name, message_data.map!(&:with_indifferent_access)
    ).deliver
  end
end
