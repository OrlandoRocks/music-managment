class Ytsr::TrackWorker
  include Sidekiq::Worker

  sidekiq_options retry: 3

  def perform(album_id)
    Ytsr::Track.create(album_id)
  end
end
