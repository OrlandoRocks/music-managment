class Ytsr::SalepointWorker
  include Sidekiq::Worker

  sidekiq_options retry: 3

  def perform(person_id)
    Ytsr::Salepoint.create(person_id)
  end
end
