class YoutubeMonetizationsController < ApplicationController
  include YoutubeHelper
  include YtmRedirector

  before_action :load_person, :load_ytm
  before_action :require_ytm, except: [:tos, :agree_to_terms, :setup_ytm]
  before_action :blocked_ytm

  layout "application"

  def show
    @page_title = custom_t("controllers.youtube_monetizations.page_title")
    @youtube_preference = current_user.youtube_preference

    load_youtube_tracks

    @has_royalty = yt_royalty?
    gon.push(
      {
        youtube_api_key: ENV["YOUTUBE_API_KEY"],
        youtube_client_id: ENV["YOUTUBE_CLIENT_ID"]
      }
    )
  end

  def mark_as_eligible
    redirect_to ytm_tracks_path
  end

  def mark_as_ineligible
    redirect_to ytm_tracks_path
  end

  def monetize
    redirect_to ytm_tracks_path
  end

  def monetize_or_mark
    redirect_to ytm_tracks_path
  end

  def tos
    render layout: false
  end

  def agree_to_terms
    if current_user.youtube_monetization.blank?
      @page_title = custom_t("controllers.youtube_monetizations.terms_and_conditions")
      @ytm_product = YoutubeMonetization.find_youtube_product(current_user)

      @youtube_price = calc_youtube_price

      respond_to do |format|
        format.html {
          render layout: "application"
        }
      end
    elsif current_user.youtube_monetization.effective_date.nil?
      redirect_to cart_url
    else
      redirect_to "index"
    end
  end

  def setup_ytm
    if params[:agree_to_terms]
      unless current_user.has_active_youtube_monetization?
        YoutubeMonetization.create!(person: current_user, agreed_to_terms_at: Time.now)
      end

      redirect_to add_to_cart_product_url(id: params[:ytm_product_id], product_type: params[:product_type])
    else
      flash[:error] = custom_t("controllers.youtube_monetizations.setup_ytm_error_msg")
      redirect_to agree_to_terms_youtube_monetizations_path
    end
  end

  private

  def load_ytm
    @youtube_monetization = YoutubeMonetization.find_by(person_id: current_user.id)
  end

  def yt_royalty?
    royalty_dates = YouTubeRoyaltyRecord.date_range_of_royalties(current_user).drop_while(&:nil?)
    royalty_dates.any?
  end

  def load_youtube_tracks
    @total_monetizable_songs = current_user.youtube_track_monetizations
    @monetized_tracks = current_user.monetized_youtube_track_monetizations
    @ineligible_tracks = current_user.ineligible_youtube_track_monetizations
    @unsent_songs = current_user.youtube_track_monetizations.where(state: TrackMonetization::STATES[0])
  end
end
