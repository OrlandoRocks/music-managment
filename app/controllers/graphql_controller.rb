# frozen_string_literal: true

class GraphqlController < ActionController::Base
  include V2::JwtHelper

  EMPTY_USER = {}.freeze

  # If accessing from outside this domain, nullify the session
  # This allows for outside API access while preventing CSRF attacks,
  # but you'll have to authenticate your user separately
  protect_from_forgery with: :null_session

  def execute
    variables      = prepare_variables(params[:variables])
    query          = params[:query]
    operation_name = params[:operationName]
    context        = {
      current_user: current_user,
      request: request,
      cookies: cookies
    }

    render json: TcWwwSchema.execute(
      query,
      variables: variables,
      context: context,
      operation_name: operation_name
    )
  rescue StandardError => e
    raise e unless Rails.env.development?

    handle_error_in_development(e)
  end

  private

  # Handle variables in form data, JSON body, or a blank value
  def prepare_variables(variables_param)
    case variables_param
    when String
      if variables_param.present?
        JSON.parse(variables_param) || {}
      else
        {}
      end
    when Hash
      variables_param
    when ActionController::Parameters
      variables_param.to_unsafe_hash # GraphQL-Ruby will validate name and type of incoming variables.
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{variables_param}"
    end
  end

  def handle_error_in_development(err)
    logger.error err.message
    logger.error err.backtrace.join("\n")

    render json: { errors: [{ message: err.message, backtrace: err.backtrace }], data: {} },
           status: :internal_server_error
  end

  # The current user is an empty hash unless:
  #  - there is a valid header token
  #  - the header token is already saved
  def current_user
    @current_user ||= saved_token&.with_indifferent_access || EMPTY_USER
  end
end
