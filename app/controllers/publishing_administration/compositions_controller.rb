class PublishingAdministration::CompositionsController < ApplicationController
  include PublishingAdministrationHelper
  include Compositionable

  layout "tc-foundation"

  before_action :redirect_to_dashboard, unless: :publishing_available_in_region?
  before_action :redirect_to_legacy_composers_index_page, unless: :rights_app_enabled?
  before_action :load_composer
  before_action :redirect_to_publishing_page, unless: :composer_is_active?

  def show
    @serialized_composer            = serialized_composer
    @serialized_cowriters           = serialized_cowriters
    @serialized_compositions        = serialized_compositions
    @pros                           = pro_list.to_json(root: false)
    @publishing_splits_translations = composition_translations
  end

  private

  def load_composer
    @composer = current_user.account.publishing_composers.primary.includes(:person, :publishing_compositions).find(params[:id])
  end

  def composer_is_active?
    PublishingAdministration::ComposerStatusService.new(@composer).state != :pending
  end

  def serialized_composer
    Api::Backstage::ComposerSerializer.new(@composer).to_json(root: false)
  end

  def serialized_cowriters
    serialize_cowriters(cowriters_collection)
  end

  def serialized_compositions
    serialize_compositions(compositions_collection)
  end

  def compositions_collection
    PublishingAdministration::PublishingCompositionsBuilder.build({ publishing_composer_id: params[:id] })
  end

  def cowriters_collection
    PublishingAdministration::NonCollectableSplitsBuilder.build(params[:id])
  end
end
