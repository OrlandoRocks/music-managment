class PublishingAdministration::ComposersController < ApplicationController
  include PublishingAdministrationHelper
  include Compositionable

  layout "tc-foundation"

  before_action :redirect_to_dashboard, unless: :publishing_available_in_region?
  before_action :redirect_to_legacy_composers_index_page, unless: :rights_app_enabled?
  before_action :load_composers, only: [:index, :resend_lod]
  before_action :redirect_to_terms_and_conditions_page, only: [:index]
  before_action :redirect_to_new_publishing_administration_enrollment, only: [:index]

  def index
    @account_composer = @composers.first.account_composer
    @user_can_edit    = user_can_edit?
    @lod = @account_composer.letter_of_direction if @account_composer

    load_split_shares_modal_variables if composer_index_split_shares_modal_enabled?
  end

  def update
    @enrollment_form = PublishingAdministration::PublishingEnrollmentForm.new(publishing_composer_params)
    @acceptable_pro_cae_state = (@enrollment_form.composer_pro_id.present? && @enrollment_form.composer_cae.present?) ||
                                (@enrollment_form.composer_pro_id.blank? && @enrollment_form.composer_cae.blank?)
    render :edit unless @enrollment_form.save
  end

  def resend_lod
    @composer = PublishingComposer.find(params[:id])

    DocuSign::LodApi.resend_lod_mail(@composer.person, @composer)
    flash[:success] = custom_t("publishing.lod_mail_resent")
    redirect_to publishing_administration_composers_path
  end

  def sync_opt_in
    @publishing_composer = PublishingComposer.find(params[:id])
    PublishingAdministration::PublishingComposerSyncOptInService.opt_in(publishing_composer: @publishing_composer)

    flash[:success] = custom_t("publishing.sync_opt_in.successfully_opted_in")
    redirect_to publishing_administration_composers_path
  end

  def sync_opt_out
    @publishing_composer = PublishingComposer.find(params[:id])
    PublishingAdministration::PublishingComposerSyncOptInService.opt_out(publishing_composer: @publishing_composer)

    flash[:success] = custom_t("publishing.sync_opt_out.successfully_opted_out")
    redirect_to publishing_administration_composers_path
  end

  private

  def serialized_cowriters
    collection = PublishingAdministration::AllNonCollectableSplitsBuilder.build(current_user.account.publishing_composers.pluck(:id))

    serialize_cowriters(collection)
  end

  def serialized_compositions
    collection = PublishingAdministration::AllPublishingCompositionsBuilder.build(
      {
        publishing_composer_ids: current_user.account.publishing_composers.pluck(:id)
      }
    )

    serialize_compositions(collection)
  end

  def load_composers
    load_publishing_composers
  end

  def load_publishing_composers
    if managing_composer?
      account_id = current_user.account.id
    elsif PublishingComposer.exists?(person_id: current_user.id)
      person_id  = current_user.id
      account_id = PublishingComposer.find_by(person_id: person_id).account_id
    end

    @composers = PublishingAdministration::PublishingComposersBuilder
                 .build(account_id, person_id)
                 .paginate(page: params[:page], per_page: (params[:per_page] || 50).to_i)
  end

  def load_legacy_composers
    return @composers unless @composer.nil?

    if managing_composer?
      account_id = current_user.account.id
    elsif Composer.exists?(person_id: current_user.id)
      person_id  = current_user.id
      account_id = Composer.find_by(person_id: person_id).account_id
    end

    @composers = PublishingAdministration::ComposersBuilder.build(account_id, person_id)
  end

  def publishing_composer_params
    PublishingAdministration::PublishingComposerParamsService.compose(params[:id]).merge!(enrollment_form_params)
  end

  def composer_params
    PublishingAdministration::ParamsComposerService.compose(params[:id]).merge!(enrollment_form_params)
  end

  def enrollment_form_params
    PublishingAdministration::PublishingComposerParamsService.format_dob_params(params)

    params.require(enrollment_form_params_key)
          .permit(
            :first_name,
            :middle_name,
            :last_name,
            :dob,
            :email,
            :composer_cae,
            :id,
            :publishing_role_id,
            :composer_pro_id,
            :person_id,
            :publisher_name,
            :publisher_pro_id,
            :name_prefix,
            :name_suffix,
            :publisher_cae,
            :dob_y,
            :dob_m,
            :dob_d
          ).transform_values { |v| v&.strip }
  end

  def enrollment_form_params_key
    :publishing_administration_publishing_enrollment_form
  end

  def redirect_to_new_publishing_administration_enrollment
    redirect_to new_publishing_administration_enrollment_path if @composers.empty?
  end

  def redirect_to_terms_and_conditions_page
    return unless current_user.account

    composers = current_user.account.publishing_composers.where(agreed_to_terms_at: nil, is_primary_composer: true)

    redirect_to publishing_administration_terms_and_condition_path(composers.first) if composers.present?
  end

  def user_can_edit?
    return false if @composers.blank?

    current_user.account.id == @composers.first.account_id
  end

  def load_split_shares_modal_variables
    @serialized_composer = Api::Backstage::ComposerSerializer.new(@account_composer).to_json(root: false)

    @serialized_composers = ActiveModel::ArraySerializer.new(
      @composers,
      each_serializer: Api::Backstage::ComposerSerializer
    ).to_json(root: false)

    @serialized_cowriters = serialized_cowriters
    @serialized_compositions = serialized_compositions
    @pros = pro_list.to_json(root: false)
    @publishing_splits_translations = composition_translations
  end
end
