class PublishingAdministration::EnrollmentsController < ApplicationController
  include PublishingAdministrationHelper

  layout "tc-foundation"

  rescue_from ActionController::MethodNotAllowed, with: :forbidden

  before_action :redirect_to_dashboard, unless: :publishing_available_in_region?
  before_action :redirect_to_legacy_composers_new_page, unless: :rights_app_enabled?
  before_action :set_eligible_publishing_pro_ids, except: [:show]

  def show
    composer = current_user.publishing_composers.includes(:publisher).find(params[:id])

    redirect_to edit_publishing_administration_enrollment_path(composer)
  end

  def new
    @enrollment_form = enrollment_form_class.new(
      {
        publishing_role_id: PublishingRole::ON_BEHALF_OF_SELF_ID
      }
    )
  end

  def create
    @enrollment_form = enrollment_form_class.new(creation_params)

    if @enrollment_form.save
      redirect_to path_after_creation
    else
      render :new
    end
  end

  def edit
    @enrollment_form = enrollment_form_class.new(edit_composer_params)
  end

  def update
    @enrollment_form = enrollment_form_class.new(update_params)

    if @enrollment_form.save
      redirect_to path_after_update
    else
      render :new
    end
  end

  private

  def path_after_creation
    cart_path
  end

  def path_after_update
    cart_path
  end

  def creation_params
    enrollment_form_params
  end

  def update_params
    enrollment_form_params.merge(action: :update)
  end

  def enrollment_form_class
    PublishingAdministration::PublishingEnrollmentForm
  end

  def enrollment_form_params
    params
      .require(required_enrollment_form_param_key)
      .transform_values(&:strip)
      .permit(
        :first_name,
        :last_name,
        :middle_name,
        :name_prefix,
        :name_suffix,
        :publishing_role_id,
        :tos_agreed,
      ).merge(person_id: current_user.id, email: current_user.email, id: params[:id])
  end

  def required_enrollment_form_param_key
    :publishing_administration_publishing_enrollment_form
  end

  def edit_composer_params
    composer = current_user.publishing_composers.includes(:publisher).find(params[:id])

    PublishingAdministration::PublishingParamsComposerService.compose(composer.id)
  end

  def set_eligible_publishing_pro_ids
    gon.eligble_publishing_pro_ids = PerformingRightsOrganization.eligible_publishing_pro_ids
  end
end
