class PublishingAdministration::TermsAndConditionsController < ApplicationController
  include PublishingAdministrationHelper

  layout "tc-foundation"

  before_action :redirect_to_dashboard, unless: :publishing_available_in_region?
  before_action :redirect_to_legacy_composers_new_page, unless: :rights_app_enabled?
  before_action :load_composer
  before_action :load_legal_document, only: [:index]
  before_action :redirect_to_cart_page

  COMPOSER_WITHOUT_PUBLISHER = "composer_without_publisher".freeze
  COMPOSER_WITH_PUBLISHER    = "composer_with_publisher".freeze

  def update
    if signing_complete?
      @composer.update_attribute(:agreed_to_terms_at, DateTime.now)
      add_publishing_administration_product_to_cart
      redirect_to cart_path
    else
      redirect_to publishing_administration_terms_and_condition_path(@composer)
    end
  end

  private

  def load_composer
    @composer = PublishingComposer.find(params[:id])
  end

  def load_legal_document
    @legal_document = LegalDocument.where(id: params[:legal_document_id]).first
  end

  def signing_complete?
    params[:event] == "signing_complete"
  end

  def add_publishing_administration_product_to_cart
    Product.add_to_cart(current_user, @composer)
  end

  def document_template_role
    @composer.publisher.present? ? COMPOSER_WITH_PUBLISHER : COMPOSER_WITHOUT_PUBLISHER
  end

  def redirect_to_cart_page
    redirect_to cart_path if @composer.agreed_to_terms_at.present?
  end
end
