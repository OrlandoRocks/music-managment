class Ytm::SalepointsController < ApplicationController
  def update
    Ytsr::MonetizationService.new(
      person: current_user,
      params: update_params
    ).call

    Ytsr::AlbumSalepointService.new(
      person: current_user,
      params: update_params
    ).call

    render head: :ok, nothing: true
  end

  def update_params
    params
      .require(:youtube_music_and_monetization_form)
      .permit(
        :monetization,
        :salepoints,
        :album_id
      )
  end
end
