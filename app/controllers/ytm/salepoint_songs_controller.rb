class Ytm::SalepointSongsController < ApplicationController
  layout "admin"

  before_action :redirect_to_ytm_judgement_index
  before_action :requires_ytm_approver_role

  def index
    @page_title = custom_t("controllers.ytm.salepoint_songs.ytsr_revenue_songs")
    @query = params[:query]
    @search = params[:search]
    @per_page = (params[:per_page] == "All") ? 99_999_999_999 : params[:per_page] || 25
    @order = params[:order]
    @valid_state = params[:valid_state] || "new"
    @page = params[:field_change].present? ? 1 : params[:page] || 1

    @query = SalepointSong.admin_query(query: @query, search: @search, order: @order, valid_state: @valid_state)
                          .paginate(page: @page, per_page: @per_page)

    @searched_items = @query.all
  end

  def update
    salepoint_songs = params[:salepoint_songs]
    if salepoint_songs.select { |_id, data| data["state"].present? }.empty?
      flash[:error] = custom_t("controllers.ytm.salepoint_songs.no_updates_submitted")
    else
      error = SalepointSong.update_states(salepoint_songs, current_user)
      flash[:error] = custom_t("controllers.ytm.salepoint_songs.failed_to_update") if error
      flash[:mass_update_success] =
        custom_t("controllers.ytm.salepoint_songs.updated_successfully") unless flash[:error]
    end
    redirect_back fallback_location: dashboard_path
  end

  def play_song
    @salepoint_song_id = params[:salepoint_song_id]
    salepoint_song = SalepointSong.find(@salepoint_song_id)
    @song_url = salepoint_song.song.s3_url(300) || salepoint_song.song.streaming_url
    render json: { song_url: @song_url, cell_id: "##{@salepoint_song_id}" }
  end

  private

  def requires_ytm_approver_role
    return if current_user.has_role?("YTM Approver", false)

    flash[:error] = custom_t("controllers.ytm.salepoint_songs.access_denied")
    if current_user.is_administrator
      redirect_to admin_home_path
    else
      redirect_to dashboard_path
    end
  end

  def redirect_to_ytm_judgement_index
    redirect_to admin_youtube_track_monetization_judgements_path
  end
end
