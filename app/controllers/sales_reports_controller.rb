class SalesReportsController < ReportsController
  def index
    @sales_report = current_user.sales_report(default_options)
    @report_months = @sales_report.months
    render_report
  end

  protected

  #  Overrides ReportsController#setup_year
  def setup_years
    @years = available_years_with_this_year.map { |year| [year.to_s, year.to_s] }
  end

  #
  #  Front end should display the current year
  #  whether the user has sales in the current year or not
  #
  def available_years_with_this_year
    ([Time.now.year] | available_years).uniq
  end

  #  Overrides ReportsController#set_year
  def set_year
    @year = params[:year].blank? ? Time.now.year : params[:year].to_i
  end
end
