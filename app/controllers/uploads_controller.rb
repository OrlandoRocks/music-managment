class UploadsController < ApplicationController
  before_action :load_person
  before_action :load_song, only: [:validate_asset, :save_streaming_asset]

  layout "application_old"

  def register
    form = UploadsRegisterForm.new(register_params)

    if form.save
      render json: { status: 0 }
    else
      render json: { errors: form.errors }, status: :unprocessable_entity
    end
  end

  def get_put_presigned_url
    response = S3Asset.get_put_presigned_url(
      BIGBOX_BUCKET_NAME,
      params[:filename],
      current_user.id
    )

    render json: response.to_json
  end

  def validate_asset
    response = AssetsUploader::Transcoder.new(
      song: @song,
      orig_filename: File.basename(params[:key_name]),
      source_key: params[:key_name],
      source_bucket: params[:bucket_name],
      file_type_validation_enabled: stereo_file_type_validation_enabled?
    ).validate_asset

    if response[:errors]
      render json: { errors: response[:errors] }, status: :unprocessable_entity
    else
      render json: response.to_json
    end
  end

  def save_streaming_asset
    AssetsUploader::Transcoder.new(
      song: @song,
      orig_filename: File.basename(params[:key_name]),
      source_key: params[:key_name],
      source_bucket: params[:bucket_name]
    ).save_streaming_asset

    render json: { status: "success" }
  end

  private

  def register_params
    upload_params
      .merge(s3_asset_params)
      .merge(person: current_user)
  end

  def upload_params
    params.require(:upload).permit(:song_id, :orig_filename, :bit_rate, :duration)
  end

  def s3_asset_params
    params.require(:s3_asset).permit(:key, :uuid, :bucket)
  end

  def load_song
    @song = Song.find(params[:song_id])
  end

  def stereo_file_type_validation_enabled?
    FeatureFlipper.show_feature?(:stereo_file_type_validation, current_user)
  end
end
