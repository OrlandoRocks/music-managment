# frozen_string_literal: true

class OutboundRoyaltyInvoicesController < ApplicationController
  before_action :load_person
  before_action :retrieve_outbound_refund

  def show
    @refund_presenter = CreditNoteInvoices::OutboundPresenter.new(@outbound_refund, request.format.html?)
    respond_to do |format|
      format.html { render_html }
      format.pdf do
        send_data render_pdf,
                  filename: "#{@refund_presenter.refund_details[:credit_note_invoice_number]}.pdf",
                  type: "application/pdf"
      end
    end
  end

  private

  def retrieve_outbound_refund
    @outbound_refund = @person.outbound_refunds.find_by(id: params[:id])
    return if @outbound_refund.present?

    flash[:error] = custom_t("invalid_request")
    redirect_back(fallback_location: dashboard_path)
  end

  def render_pdf
    render_to_string pdf: @refund_presenter.refund_details[:credit_note_invoice_number],
                     template: "credit_note_invoices/show.html.erb",
                     formats: [:html],
                     layout: "layouts/pdf"
  end

  def render_html
    render "credit_note_invoices/preview.html.erb",
           layout: "layouts/application"
  end
end
