# FIXME: Consider renaming this to ProductRedemptionsController??
class CertRedemptionsController < ApplicationController
  before_action :save_redemption_code, only: [:create]
  before_action :load_person, except: [:index, :new]
  skip_before_action :check_for_verification
  layout nil

  # new redemption does not require authentication
  def index
    @cert = Cert.new
    render action: :new, layout: false
  end

  def create
    opts = {
      product_redemption: true,
      entered_code: session[:cert_code],
      current_user: @person
    }

    # Cert.verify verifies, apply the cert code, and update the purchase
    @verify_result = Cert.verify(opts)

    case @verify_result
    when Cert
      # The verification succeeded
      @cert = @verify_result
      # redeem would create and settle the invoice
      begin
        @cert.redeem!
      rescue Tunecore::Certs::UnableToCheckoutException => e
        # The cert and purchase is still intact, but just in case the checkout failed for whatever reasons
        flash[:error] = e.message
        redirect_to dashboard_path and return
      end

      # FIXME: Refactor the handling of errors
      if @cert.errors.empty?
        Rails.logger.info "Setting redeem_thank_you"
        Rails.logger.info request.env["HTTP_REFERER"]
        flash[:redeem_thank_you] = @cert.cert_engine
        redirect_to dashboard_path
      else
        flash.now[:error] = @cert.errors.full_messages.join(", ")
        @cert = Cert.new
        render action: :new
      end
    when String
      # an error message was returned
      flash.now[:error] = @verify_result
      @cert = Cert.new
      render action: :new
    else
      flash.now[:error] = "Unexpected result from cert verification."
      @cert = Cert.new
      render action: :new
    end
  end

  protected

  def save_redemption_code
    session[:cert_code] = params[:cert] ? params[:cert][:cert] : session[:cert_code]
    login_required
  end

  def store_location
    session[:return_to] = "/cert_redemptions/create"
  end
end
