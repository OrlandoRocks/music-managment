class StoredCreditCardsController < ApplicationController
  include ApplicationHelper
  before_action :load_person
  before_action :generate_person_braintree_token, only: [:new]
  before_action :generate_card_braintree_token, only: [:edit]
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:new]
  skip_before_action :redirect_stem_users, only: [:new, :create]

  def create
    status, @vault_transaction = build_vault_transaction
    case status
    when :braintree_error then handle_braintree_error
    when :gst_error then handle_gst_error
    when :card_not_created, :invalid_card, :no_cc_params, :payos_customer_error, :invalid_vendor then redirect_error_response
    when :ok
      flash[:notice] = custom_t("controllers.stored_credit_cards.cc_added")
      Sift::EventService.update_account_payment_methods(@person, cookies, request)
      redirect_back_after_processing
    end
  end

  def update
    stored_credit_card = @person.stored_credit_cards.find(params[:id])
    merge_params = {
      ip_address: request.remote_ip,
      payin_provider_config: stored_credit_card.payin_provider_config
    }
    update_params = params.merge(merge_params)
    is_success, @vault_transaction = stored_credit_card.update_with_braintree(update_params)

    if is_success
      flash[:notice] = custom_t("controllers.stored_credit_cards.cc_updated")
      Sift::EventService.update_account_payment_methods(@person, cookies, request)
      redirect_back_after_processing
    else
      redirect_error_response
    end
  end

  def index
    redirect_to person_preferences_path
    @page_title = custom_t("controllers.stored_credit_cards.my_credit_cards")
    @stored_credit_cards = @person.stored_credit_cards.active
  end

  def destroy
    begin
      stored_credit_card = @person.stored_credit_cards.find(params[:id])
      preferred_credit_card_deleted = @person.person_preference && !@person.renew_with_balance? && @person.preferred_credit_card_id == stored_credit_card.id
      card_deleted = stored_credit_card.destroy(request.remote_ip, current_user)

      if card_deleted
        if preferred_credit_card_deleted
          note = "Preferred Credit Card ************#{stored_credit_card.last_four} Deleted"
          flash[:notice] = custom_t("controllers.stored_credit_cards.preferred_cc_removed")
        else
          note = "Credit Card ************#{stored_credit_card.last_four} Deleted"
          flash[:notice] = custom_t("controllers.stored_credit_cards.cc_removed")
        end

        Note.create(
          related: @person,
          note_created_by: current_user,
          ip_address: request.remote_ip,
          subject: "Payment Preferences Changed",
          note: note
        )
      else
        flash[:error] = custom_t("controllers.stored_credit_cards.must_have_preferred_method")
      end
    rescue ActiveRecord::RecordNotFound
      flash[:error] = custom_t("controllers.stored_credit_cards.cc_not_found")
    end
    if !params[:invoice_id]
      redirect_to person_preferences_path
    else
      redirect_to credit_card_invoice_new_payment_path(Invoice.find(params[:invoice_id]))
    end
  end

  def edit
    @stored_credit_card = @person.stored_credit_cards.find(params[:id])
    @page_title = custom_t("controllers.stored_credit_cards.edit_cc")
    @payment_method = @stored_credit_card.config_gateway_service.find_payment_method(@stored_credit_card.bt_token)
    @payment_method_billing_info = @payment_method.billing_address
    @edit_page = true
    set_redirect
    load_states_and_countries
    initialize_translations

    render layout: "application"
  end

  def new
    @stored_credit_card = StoredCreditCard.new(country: current_user.country_domain)
    @page_title = custom_t("controllers.stored_credit_cards.add_cc")
    @vendor_form = get_vendor_form
    set_redirect
    load_states_and_countries
    @tcs_return_msg = params[:tcs_return_msg]
    initialize_translations

    render layout: "application"
  end

  def country_cities
    @country_state_cities = CountryStateCity.for_state(params[:state_id])
                                            .auto_complete_query_filter(params[:query])
                                            .limit(10)
    cities = @country_state_cities.blank? ? [custom_t(".no_cities_found")] : @country_state_cities.pluck(:name)
    render json: { city_names: cities }
  end

  private

  def initialize_translations
    gon.push(translations: {})
    gon_custom_t("translations.tokenize_error", "stored_credit_cards.tokenize_error")
    gon_custom_t("translations.try_again", "stored_credit_cards.try_again")
    gon_custom_t("translations.tds_init_error", "stored_credit_cards.tds_init_error")
    gon_custom_t("translations.verification_failed", "stored_credit_cards.verification_failed")
    gon_custom_t("translations.unsupported_card", "stored_credit_cards.unsupported_card")
    gon_custom_t("translations.billing_phone_number_invalid", "stored_credit_cards.billing_phone_number_invalid")
    gon_custom_t("translations.billing_postal_code_invalid", "stored_credit_cards.billing_postal_code_invalid")
    gon_custom_t("translations.billing_given_name_invalid", "stored_credit_cards.billing_given_name_invalid")
    gon_custom_t("translations.billing_surname_invalid", "stored_credit_cards.billing_surname_invalid")
    gon_custom_t("translations.billing_line1_invalid", "stored_credit_cards.billing_line1_invalid")
    gon_custom_t("translations.billing_line2_invalid", "stored_credit_cards.billing_line2_invalid")
    gon_custom_t("translations.billing_line3_invalid", "stored_credit_cards.billing_line3_invalid")
    gon_custom_t("translations.billing_city_invalid", "stored_credit_cards.billing_city_invalid")
    gon_custom_t("translations.billing_state_invalid", "stored_credit_cards.billing_state_invalid")
    gon_custom_t("translations.billing_country_code_invalid", "stored_credit_cards.billing_country_code_invalid")
  end

  def build_vault_transaction
    case params.fetch(:payment_vendor, BRAINTREE_VENDOR)
    when PAYMENTS_OS_VENDOR
      cc_params = stored_credit_card_params
      StoredCreditCard.create_with_payments_os(@person, cc_params).tap do |stored_cc|
        update_person_address if (stored_cc.first == :ok) && FeatureFlipper.show_feature?(:enable_gst, @person)
      end
    when BRAINTREE_VENDOR
      StoredCreditCard.create_with_braintree(@person, stored_credit_card_params)
    else
      [:invalid_vendor, "Card Invalid"]
    end
  end

  def load_states_and_countries
    @states = UsState::STATES_AND_TERRITORIES
    @countries = country_dropdown_for_website(current_user.country_website_id)
  end

  def redirect_error_response
    case payment_vendor
    when BRAINTREE_VENDOR
      handle_braintree_error
    when PAYMENTS_OS_VENDOR
      handle_payments_os_error
    end
  end

  def handle_payments_os_error
    flash[:error] = custom_t("controllers.stored_credit_cards.error_adding_cc")
    redirect_for_edit_or_new
  end

  def handle_braintree_error
    # check to see if the response was an error in the credit card or expiration date, return the user to the form
    case @vault_transaction.raw_response
    when /Invalid.*Credit.*Card.*/, /Credit card number is invalid/
      flash[:error] = custom_t("controllers.stored_credit_cards.invalid_cc")
      redirect_for_edit_or_new
    when /(Expired)/
      flash[:error] = custom_t("controllers.stored_credit_cards.expired_cc")
      redirect_for_edit_or_new
    else
      msg = (params[:action] == "receive_new") ? custom_t("controllers.stored_credit_cards.error_adding_cc") : custom_t("controllers.stored_credit_cards.error_updating_cc")
      flash[:error] = msg
      redirect_back_after_processing
    end
  end

  def handle_gst_error
    flash[:error] = custom_t("controllers.stored_credit_cards.gstin_error")
    redirect_for_edit_or_new
  end

  def redirect_for_edit_or_new
    if params[:action] == "receive_update"
      redirect_to action: :edit, id: params[:id]
    else
      redirect_to action: :new, payment_vendor: payment_vendor
    end
  end

  def redirect_back_after_processing
    if session[:stored_card_redirect] && !session[:stored_card_redirect].nil?
      url = session[:stored_card_redirect]
      session.delete("stored_card_redirect")
      redirect_to url
    elsif params[:tcs_return_msg]
      redirect_to person_preferences_path(tcs_return_msg: params[:tcs_return_msg])
    else
      redirect_to person_preferences_path
    end
  end

  def set_redirect
    session[:stored_card_redirect] = nil
    if params[:p] == "payment"
      url = credit_card_new_invoice_payment_path(@person.invoices.find(params[:i]))
      session[:stored_card_redirect] = url
      @cancel_redirect = url
    elsif params[:redirect]
      session[:stored_card_redirect] = params[:redirect]
      @cancel_redirect = params[:redirect]
    else
      @cancel_redirect = person_preferences_path
    end
  end

  def set_subscription_session
    return unless params[:subscription_id]

    subscription = @person.entitlements.find(params[:subscription_id])
    session[:stored_card_subscription_id] = subscription.id if subscription
  end

  def create_with_vendor
    {
      PAYMENTS_OS_VENDOR => "create_with_payments_os",
      BRAINTREE_VENDOR => "create_with_braintree"
    }[payment_vendor]
  end

  def get_vendor_form
    {
      PAYMENTS_OS_VENDOR => "payu_form",
      BRAINTREE_VENDOR => "form"
    }[payment_vendor]
  end

  def payment_vendor
    params.fetch(:payment_vendor, BRAINTREE_VENDOR)
  end

  def stored_credit_card_params
    if payment_vendor == BRAINTREE_VENDOR
      return params.merge(ip_address: request.remote_ip, payin_provider_config: @person.braintree_config_by_corporate_entity)
    end

    params.permit(
      :firstname,
      :company,
      :lastname,
      :phone_number,
      :payments_os_token,
      :payment_vendor,
      :address1,
      :address2,
      :city,
      :zip,
      :country,
      :state_id,
      :state_city_name,
      :gstin
    )
  end

  def generate_person_braintree_token
    gon.braintreeToken = @person.config_gateway_service.generate_client_token
  end

  def generate_card_braintree_token
    stored_credit_card = @person.stored_credit_cards.find(params[:id])
    gon.braintreeToken = stored_credit_card.config_gateway_service.generate_client_token
  end

  def update_person_address
    params = stored_credit_card_params
    @person.update(
      address1: params[:address1],
      address2: params[:address2],
      city: params[:state_city_name],
      state: CountryState.find(params[:state_id]).name,
      foreign_postal_code: params[:zip]
    )
  end
end
