class CoversController < ApplicationController
  before_action :retrieve_or_create_cover

  def show
    respond_to do |wants|
      wants.html {}
      wants.json {
        render json: ("[" + @cover.to_json(methods: [:background_url, :type, :guid]) + "]")
      }
    end
  end

  def update
    @cover.update params[:cover]

    respond_to do |format|
      format.html { raise "html is not supported" }
      format.json { render json: @cover.to_json(methods: :background_url) }
    end
  end
end
