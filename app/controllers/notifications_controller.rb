class NotificationsController < ApplicationController
  layout "application"

  before_action :load_person

  def index
    params[:page]     ||= 1
    params[:per_page] ||= 10
    if @person.notifications.present?
      @start_date = @person.notifications.last.effective_created_at
      @end_date = @person.notifications.first.effective_created_at
      @notification_dates = Utilities::FormDateRange.create_range(@start_date, @end_date)
    end

    @notifications = @person.notifications
                            .read(params[:status_select])
                            .range(params[:date_from_select], params[:date_to_select])
                            .paginate(page: params[:page], per_page: params[:per_page])

    respond_to do |format|
      format.html
      format.js do
        render partial: "notifications_archive", locals: { notifications: @notifications }, content_type: "text/html"
      end
    end
  end

  # Dropdown
  # this is where the notifications come from
  def dropdown
    @notifications = Notification.unarchived_notifications(@person)
    Notification.mark_seen(@person, @notifications)

    respond_to do |format|
      format.html { render layout: false }
      format.js { head :ok }
    end
  end

  #
  # Marks a notification as actioned
  #
  def action
    @notification = @person.notifications.find(params[:id])

    Notification.transaction do
      @notification.mark_actioned
      Notification.mark_seen(@person, [@notification])
    end

    respond_to do |format|
      format.html { head :ok }
      format.js { head :ok }
    end
  end

  #
  # Marks a notification as archived
  #
  def archive
    @notification = @person.notifications.find(params[:id])
    @notification.mark_archived

    respond_to do |format|
      format.html { head :ok }
      format.js { head :ok }
    end
  end
end
