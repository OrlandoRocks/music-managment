# frozen_string_literal: true

class DiscographyController < ApplicationController
  include DiscographyHelper

  before_action :load_person
  skip_before_action :alert_rejections

  layout "application"

  def index
    @page_title = custom_t("controllers.discography.my_discography")
    @grace_days = Renewal::GRACE_DAYS_BEFORE_TAKEDOWN
    recalculate_params
    set_view_type

    @releases, @release_count = Album.discography_search(
      current_user,
      keyword: @keyword,
      release_type: @release_type,
      sort_by: @sort_by,
      status: @status,
      page: @page,
      per_page: @per_page
    )
  end

  def stop_cancellation
    album = Album.find(params[:id])
    begin
      Renewal.transaction do
        Renewal.renewal_for(album).keep!
        @note = Note.create(related: album, note_created_by: current_user, ip_address: request.remote_ip, subject: "Discography: Stop Cancellation", note: "Album will now renew")
      end
    rescue StandardError => e
      logger.error("DiscographyController.stop_cancellation: #{e}")
    end
    redirect_to :discography
  end

  private

  def recalculate_params
    @keyword = params[:keyword]
    @release_type = params[:release_type] || "all"
    @sort_by = params[:sort_by] || "created_on desc"
    @status = params[:status] || "all"
    @page = params[:page].to_i || 1
    @page = 1 if @page < 1
    set_view_type
    set_per_page
  end

  def set_per_page
    @per_page = params[:per_page].to_i || cookies[:per_page].to_i || 25
    @per_page = [@per_page, 100].min
    @per_page = [@per_page, 25].max
    cookies[:per_page] = @per_page
  end
end
