class TaxInfosController < ApplicationController
  before_action :load_person
  before_action :restrict_admin_access
  before_action :require_publishing_access

  layout "application"

  def new
    load_composer
    @tax_form = params.require(:tax_form)
    setup_submit_text
    @tax_info = TaxInfo.new
  end

  def create
    load_composer
    @tax_form = params.require(:tax_form)
    setup_params_for_composer

    if @composer.tax_info
      @tax_info = @composer.tax_info
      tax_info_id = @tax_info.id

      # we need to reset the columns incase user switch from W8 to W9 or vice versa
      @tax_info.attributes = TaxInfo.reset_attributes
    else
      @tax_info = @composer.build_tax_info(tax_info_params)
    end
    @tax_info.agreed_to_w9_at = tax_info_params[:agreed_to_w9] ? Time.now : nil
    @tax_info.agreed_to_w8ben_at = tax_info_params[:agreed_to_w8ben] ? Time.now : nil
    success =
      if @tax_info.new_record?
        @tax_info.save
      else
        @tax_info.update(tax_info_params.merge(id: tax_info_id))
      end
    if success
      respond_to do |format|
        format.html { redirect_to composer_path(@composer) }
      end
    else
      @tax_info.agreed_to_w9_at = nil
      @tax_info.agreed_to_w8ben_at = nil
      setup_submit_text

      flash.now[:error] = collect_understandable_error
      respond_to do |format|
        format.html { render action: "new" }
      end
    end
  end

  private

  def tax_info_params
    params.require(:tax_info).permit(
      :composer_id,
      :name,
      :address_1,
      :address_2,
      :city,
      :state,
      :zip,
      :country,
      :nosocial,
      :agreed_to_w9,
      :salt,
      :encrypted_tax_id,
      :is_entity,
      :entity_name,
      :dob,
      :classification,
      :llc_classification,
      :tax_id,
      :country_of_corp,
      :owner_type,
      :mailing_address_1,
      :mailing_address_2,
      :mailing_city,
      :mailing_state,
      :mailing_zip,
      :mailing_country,
      :agreed_to_w8ben,
      :created_at,
      :updated_at,
      :exempt_payee,
      :claim_of_treaty_benefits
    )
  end

  def load_composer
    @composer = current_user.publishing_composers.find_by(id: params[:composer_id])
  end

  def setup_params_for_composer
    # setting default values for composer
    dob = tax_info_params[:dob] ? Date.parse(tax_info_params[:dob]).strftime("%m/%d/%Y") : nil
    tax_info_params[:dob] = dob
    collect_claim_of_treaty_benefits if @tax_form == "w8ben"
  end

  def setup_submit_text
    @submit_text =
      case @tax_form
      when "w9"
        custom_t("controllers.tax_infos.submit_w9")
      when "w8ben"
        custom_t("controllers.tax_infos.submit_w8ben")
      else
        custom_t("controllers.tax_infos.submit")
      end
  end

  def collect_understandable_error
    error_msg = ""
    @tax_info.understandable_error_messages.each do |msg|
      error_msg << "<li>#{msg}</li>"
    end
    "<ul>#{error_msg}</ul>"
  end

  def restrict_admin_access
    return unless under_admin_control?

    flash[:error] = custom_t("controllers.composer.admin_are_not_permitted_to_update_tax_info")
    redirect_back fallback_location: dashboard_path
  end

  def collect_claim_of_treaty_benefits
    tax_info_params.merge!(
      claim_of_treaty_benefits: {
        q9: {
          a: { checked: checked?(params[:q9][:a][:checked]), resident: params[:q9][:a][:resident] },
          b: { checked: checked?(params[:q9][:b]) },
          c: { checked: checked?(params[:q9][:c]) },
          d: { checked: checked?(params[:q9][:d]) },
          e: { checked: checked?(params[:q9][:e]) }
        }
        # :q10 => {
        #   :claim_provisions_of_article => params[:q10][:claim_provisions_of_article],
        #   :claim_percentage => params[:q10][:claim_percentage],
        #   :withholding_on => params[:q10][:withholding_on],
        #   :reasons => params[:q10][:reasons]
        #   }
      }
    )
  end

  def checked?(field)
    field ? true : false
  end
end
