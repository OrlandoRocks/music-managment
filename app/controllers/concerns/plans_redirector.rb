module PlansRedirector
  def dashboard_or_plans_redirect
    if show_plans? && plans_redirect?
      deactivate_plans_redirect
      redirect_to plans_path
    else
      redirect_to dashboard_path unless request.path == dashboard_path
    end
  end

  def reset_plans_redirect(person)
    return unless reset_plans_redirect?(person)

    cookies[:plans_redirect] = { value: true }
  end

  private

  def plans_redirect_cookie
    @plans_redirect_cookie ||= cookies[:plans_redirect]
  end

  def show_plans?
    current_user.legacy_user? ||
      Plan.enabled?(current_user, person_flags[:blocked_from_plans])
  end

  def plans_redirect?
    ActiveModel::Type::Boolean.new.cast(plans_redirect_cookie) == true
  end

  def deactivate_plans_redirect
    return unless plans_redirect?

    cookies[:plans_redirect] = { value: false, expires: 7.days.from_now }
  end

  def reset_plans_redirect?(person)
    person.has_plan? == false && plans_redirect_cookie.nil?
  end
end
