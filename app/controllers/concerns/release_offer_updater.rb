# frozen_string_literal: true

# Release offers can change depending on salepoint addition/removal.
module ReleaseOfferUpdater
  extend ActiveSupport::Concern

  # Ensure carted releases have applicable offers.
  # Can be a targeted_product_store_offer or a targeted_offer.
  # targeted_product_store_offer supercedes a regular targeted_offer.
  # In case there are multiple of either, the first matching product from either is used.
  # We don't yet have logic to otherwise choose among multiple offers.
  def update_release_offers
    current_user
      .purchases
      .unpaid
      .not_in_invoice
      .select { |item| item.related_type == "Album" }
      .each do |item|
        tp = tpso_product(item) || targeted_product(item)
        apply_offer(item, tp)
      end
  end

  private

  def apply_offer(item, tp)
    if tp.blank?
      item.update(
        discount_cents: 0, discount_reason: Purchase::VALID_DISCOUNT_REASONS[:no_discount],
        targeted_product_id: nil
      )
    else
      discount = tp.discount_amount_cents(item.cost_cents) || 0
      discount_reason_sym = discount.positive? ? :non_plan_discount : :no_discount
      item.update(
        discount_cents: discount, discount_reason: Purchase::VALID_DISCOUNT_REASONS[discount_reason_sym],
        targeted_product: tp
      )
    end
  end

  # targeted_product_store_offer product
  def tpso_product(item)
    TargetedProduct
      .targeted_product_store_offer(tpso_params(item))
      &.targeted_products
      &.find { |p| p.product_id == item.product_id }
  end

  def tpso_params(item)
    {
      item_to_price: Album.find(item.related_id),
      person: current_user,
      product_id: item.product_id
    }
  end

  def targeted_product(item)
    product = Product.find(item.product_id)
    item_to_price = Album.find(item.related_id)
    TargetedProduct.targeted_product_for_active_offer(current_user, product, item_to_price)
  end
end
