# frozen_string_literal: true

require "active_support/concern"

# Shared Automator-state behavior
module AutomatorManager
  extend ActiveSupport::Concern

  def manage_automator_state(params = {})
    session[:automator_checked] = deliver_automator?(params)
  end

  # Create/destroy Automator via Salepoints page action only if album is in cart
  def automator_salepoints_update(album, params)
    sync_automator_with_cart(album, params) if album_in_cart?(album)
  end

  def automator_products_update(album, params)
    update_automator(album, params)
    remove_from_cart(album) unless deliver_automator?(params)
  end

  private

  def deliver_automator?(params)
    result = params[:deliver_automator] || params[:add_automator]
    result == "1"
  end

  def sync_automator_with_cart(album, params)
    deliver_automator = deliver_automator?(params)

    update_automator(album, params)

    if deliver_automator
      Product.add_to_cart(current_user, album.salepoint_subscription)
    else
      remove_from_cart(album)
    end
  end

  def album_in_cart?(album)
    Purchase.albums_in_cart(current_user).map(&:id).include?(album.id)
  end

  def update_automator(album, params)
    Album::AutomatorService.update_automator(album, deliver_automator?(params))
  end

  def remove_from_cart(album)
    return unless album.salepoint_subscription&.purchase

    album.salepoint_subscription.purchase.destroy
  end
end
