module Api::Frontstage::Authenticatable
  extend ActiveSupport::Concern

  included do
    before_action :authenticate
  end

  def authenticate
    redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1) unless request_verified?
  end

  def request_verified?
    !!validate_token && person_is_active_user?
  end

  private

  def validate_token
    @email = decode_jwt.first.try(:[], "email") if auth_present?
  end

  def person_is_active_user?
    @person = Person.find_by(email: @email)
    return false unless @person && !Person.account_locked?(@person.email)

    true
  end

  def auth_present?
    !!token
  end

  def decode_jwt
    ::TwoFactorAuth::TokenService.decode(token)
  end

  def token
    @token ||= params[:token]
  end
end
