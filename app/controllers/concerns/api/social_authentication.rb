module Api::SocialAuthentication
  attr_reader :user

  def authenticate
    authenticate_tc_social_token || render_unauthorized
  end

  private

  def authenticate_tc_social_token
    authenticate_with_http_token do |token, _options|
      decrypted_token = SessionEncryptionEngine.decrypt64(token + "=\n")
      @user = OauthToken.find_by(token: decrypted_token).try(:user)
      user && user.has_tc_social?
    end
  rescue => e
    Tunecore::Airbrake.notify(e)
    false
  end

  def render_unauthorized
    render json: { status: "failure", code: 2000 }
  end
end
