module SignInCallback
  extend ActiveSupport::Concern

  include ReferralData::Parsable
  include PlansRedirector
  include PayoneerHelper

  def sign_in_success
    V2::Authentication::SharedSignInSuccessCallback.call(@person, request, cookies, params)
    self.current_user = @person

    evaluate_plan_blocking
    create_active_cable_token # This generates token that can be used with ActionCable
    reroute_logged_in_user
  end

  def sign_in_failure
    client_ip = request.headers["True-Client-IP"] || request.remote_ip
    LoginAttempt.record_failed_attempt(client_ip)
    @person = Person.new

    flash[:error] = custom_t("controllers.sessions.incorrect_email_or_pw")
    redirect_to new_session_path
  end

  def reroute_logged_in_user
    flash[:just_logged_in] = true
    callout_name = CmsSet.active_set("interstitial", country_website, country_website_language).try(:callout_name).try(:to_sym)

    # Reset the captcha count
    LoginAttempt.reset_since_last_login_count(request.remote_ip)

    if params[:return_to].present?
      redirect_to params[:return_to]
    elsif current_user.password_reset_tmsp.nil? && FeatureFlipper.show_feature?(:force_password_reset, current_user)
      redirect_to new_reset_password_path(force: true)
    elsif has_preset_redirect?
      redirect_to_preset_url
    elsif current_user.first_login && !redirect_to_tc_social?
      session[:verification] = true
      if PersonProfileSurveyInfo.get_survey_info(current_user).show_interstitial? && cookies[:profile_survey] == "true"
        redirect_to account_profile_survey_announcement_path(first_login: true)
      else
        redirect_to(controller: "dashboard", action: "create_account")
      end
    elsif fill_payoneer_info?(current_user)
      redirect_to edit_person_path(current_user, tab: "tax_info")
    elsif redirect_to_tc_social?
      redirect_to tc_social_oauth_url
    elsif !session[:return_to].nil? && session[:return_to].include?("oauth")
      logger.info "requesting oauth access token, redirecting user to oauth #{t(:authoriz)}e page"
      redirect_back_or_default(dashboard_path)
    elsif current_user.is_administrator? && session[:return_to].blank?
      redirect_to(controller: "admin/dashboard")
    elsif feature_enabled?(:sync_licensing) && session[:return_to].blank?
      redirect_to(week_sync_chart_path)
    elsif PersonProfileSurveyInfo.get_survey_info(current_user).show_interstitial? && cookies[:profile_survey] == "true"
      redirect_to account_profile_survey_announcement_path
    elsif feature_enabled?(:promo_callouts) && callout_name && callout_name != :None && cookies[callout_name].blank? && current_user.albums.distributed.not_taken_down.present?
      cookies[callout_name] = true
      redirect_to announcements_path
    elsif plan_eligible?
      session[:previous_path] = request.path
      dashboard_or_plans_redirect
    elsif current_user.dashboard_state == "virgin" && is_mobile_agent?
      redirect_to mobile_announcement_path
    else
      options = {}
      options[:pb] = "n" unless current_user.purchased_before?
      ensure_correct_country_domain(session[:return_to] || dashboard_path(options)) # in application controller
    end
  end

  def plan_eligible?
    if Person::FlagService.person_flag_exists?({ person: current_user, flag_attributes: Flag::BLOCKED_FROM_PLANS })
      return
    end

    session[:dont_want_a_plan].nil? && FeatureFlipper.show_feature?(:plans_pricing, current_user) && current_user.legacy_user?
  end

  def preset_redirect_url(redirect_url)
    session[:redirect_url] = redirect_url if redirect_url.present?
  end

  def has_preset_redirect?
    session[:redirect_url].present?
  end

  def redirect_to_preset_url
    url = session.delete(:redirect_url)

    return if url.blank?

    redirect_to url
  end

  private

  def create_active_cable_token
    CableAuthService.refresh_token(current_user)
  end

  def evaluate_plan_blocking
    flag_exists = Person::FlagService.person_flag_exists?({ person: current_user, flag_attributes: Flag::BLOCKED_FROM_PLANS })
    session[:blocked_from_plans] = flag_exists if flag_exists
  end
end
