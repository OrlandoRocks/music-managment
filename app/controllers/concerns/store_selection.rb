module StoreSelection
  extend ActiveSupport::Concern

  def add_salepoints
    @album ||= @single if @single.present?
    @salepoints ||= @album.salepoints.includes(
      :store,
      :variable_price,
      :inventory_usages
    )
    album_hash = {}
    digital_stores.each do |ds|
      ds.stores.each do |store|
        album_hash[store.id] ||= {
          salepoint: {}
        }
        store_view = SalepointHelper::StoreView.new(
          store: store,
          salepoints: @salepoints,
          album: @album,
          store_group: ds.store_group
        )

        album_hash[store.id][:salepoint][:variable_price_id] = store_view.variable_price.id if store_view.variable_price
        album_hash[store.id][:salepoint][:has_rights_assignment] = true if store.needs_rights_assignment?

        # Include all stores
        album_hash[store.id][:salepoint][:store_id] = store.id
      end
    end
    SalepointService.new(
      current_user,
      @album,
      album_hash,
      true
    ).call
  end
end
