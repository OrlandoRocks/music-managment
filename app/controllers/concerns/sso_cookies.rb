require "openssl"

module SsoCookies
  # Creates the SSO cookie that's used by the coredata app to authenticate the
  # user. Tampering is prevented by adding an HMAC. User can be assigned to
  # faciliate account 'take over'
  # * for_user_id is the user being taken over
  # * by_user_id is the user taking over (should be current_user)
  def add_sso_cookie(for_user_id:, by_user_id: nil)
    by_user_id ||= for_user_id
    return unless for_user_id.present? && by_user_id.present?

    hmac = generate_hmac_sha1(by_user_id.to_s)
    cookie_value = "#{for_user_id}-#{hmac}"

    cookies[:tc_pid] = { value: cookie_value, domain: COOKIE_DOMAIN }
  end

  def generate_hmac_sha1(data_str)
    raise "Invalid data_str" if data_str.nil? || !data_str.is_a?(String)

    digest = OpenSSL::Digest.new("SHA1")
    OpenSSL::HMAC.hexdigest(digest, SSO_SECRET, data_str)
  end
end
