# frozen_string_literal: true

require "active_support/concern"

module YtmRedirector
  include KnowledgebaseLinkHelper

  def require_ytm
    redirect_to content_id_help and return unless current_user.has_active_ytm? || current_user.has_ytsr_proxy?
  end

  def blocked_ytm
    redirect_to artist_services_path and return if current_user.ytm_blocked?
  end

  private

  def content_id_help
    knowledgebase_link_for("youtube-content-id", current_user.country_domain)
  end
end
