module PaymentFinalizable
  extend ActiveSupport::Concern
  include OptimizelyService

  def successful_purchase_after_effects(invoice, after_hosted_checkout = false)
    logger.info "Sidekiq Job initiated SuccessfulPurchaseWorker after_cart_finalize: #{invoice.id}"
    SuccessfulPurchaseWorker.perform_async(invoice.id)
    PersonPlansPurchaseService.process(invoice)
    begin
      optimizely_events(invoice)
    rescue => e
      Airbrake.notify(
        e,
        details: "Unable to send optimizely events after successful purchase for invoice",
        invoice: invoice
      )
    ensure
      invoice.purchases.each(&:processed!) if after_hosted_checkout
    end
  end

  def failed_purchase_after_effects_for_hosted_checkout(invoice)
    invoice.purchases.each(&:failed!)
  end

  def optimizely_events(invoice)
    @optimizely_end_user_id = cookies[:optimizelyEndUserId]
    @optimizely_buckets     = cookies[:optimizelyBuckets]
    return unless @optimizely_end_user_id && @optimizely_buckets

    if invoice.publishing_purchase?
      record_optimizely_event(
        "purchase_publishing",
        @optimizely_end_user_id,
        @optimizely_buckets,
        v: invoice.final_settlement_amount_cents
      )
    end

    is_first_time_dist_customer = invoice.check_and_set_first_time_distribution_or_credit

    if is_first_time_dist_customer
      record_optimizely_event(
        "purchase_first_distribution",
        @optimizely_end_user_id,
        @optimizely_buckets,
        v: invoice.final_settlement_amount_cents
      )
    end

    return unless is_first_time_dist_customer

    record_optimizely_event(
      "purchase",
      @optimizely_end_user_id,
      @optimizely_buckets,
      v: invoice.final_settlement_amount_cents
    )
  end
end
