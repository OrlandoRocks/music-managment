module TargetedProductStoreOffer
  extend ActiveSupport::Concern

  def update_targeted_product_stores(targeted_product)
    tp_store_ids = params[:targeted_product][:targeted_product_store_ids] &&
                   params[:targeted_product][:targeted_product_store_ids].reject(&:blank?)

    return if valid_targeted_product_stores?(targeted_product, tp_store_ids)

    TargetedProduct.transaction(requires_new: true) do
      targeted_product.targeted_product_stores.destroy_all
      tp_store_ids.each do |store_id|
        targeted_product.targeted_product_stores.create(store_id: store_id)
      end
    end
  end

  def valid_targeted_product_stores?(targeted_product, tp_store_ids)
    tp_store_ids.blank? ||
      !single_year_products.pluck(:id).include?(targeted_product.product_id)
  end
end
