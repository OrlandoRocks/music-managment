# frozen_string_literal: true

require "active_support/concern"

module OneTimePaymentRedirectable
  extend ActiveSupport::Concern
  include PaymentHelper

  def redirect_one_time_payment_users
    redirect_to account_settings_path
  end

  def one_time_payment_user?
    one_time_payment_only?
  end

  def one_time_payment_user_payment_tab?
    one_time_payment_user? && params[:tab] == "payment"
  end
end
