module FeatureFlag
  def feature_enabled?(value, country_website_code: nil)
    if Feature.restrict_by_user?(value)
      if logged_in?
        current_user.feature_enabled?(value)
      else
        false
      end
    else
      # check if feature is enabled for the country website
      country_website_code ||= country_website # calling the country_website helper
      CountryWebsite.find_by(country: country_website_code.to_s).feature_enabled?(value)
    end
  end
end
