# frozen_string_literal: true

require "active_support/concern"

module YoutubeOAC
  extend ActiveSupport::Concern

  MINIMUM_QUALIFYING_SONG_COUNT = 3

  def oac_eligible_artists
    not_onboarded_artists
  end

  def show_yt_oac?
    return false unless yt_oac_enabled? && has_minimum_track_count?

    oac_eligible_artists.any? || onboarded_all_potential_artists?
  end

  def yt_oac_enabled?
    FeatureFlipper.show_feature?(:yt_oac_self_serve, current_user)
  end

  def onboarded_all_potential_artists?
    list_size = uniq_artists(oac_releases).size
    list_size.nonzero? && list_size == onboarded_artists.size
  end

  private

  def oac_releases
    return @oac_releases if @oac_releases

    @oac_releases = current_user
                    .albums
                    .approved
                    .not_taken_down
                    .includes(:artists)
                    .joins(salepoints: [:store, :distributions])
                    .joins(creatives: :artist)
                    .joins("
                      LEFT JOIN creative_song_roles ON creative_song_roles.creative_id = creatives.id
                      LEFT JOIN song_roles ON song_roles.id = creative_song_roles.song_role_id
                    ")
                    .joins("
                      LEFT JOIN external_service_ids esi ON esi.linkable_type = 'Creative'
                        AND esi.linkable_id = creatives.id
                        AND esi.service_name = 'youtube_channel_id'
                    ")
                    .joins("LEFT JOIN artist_store_whitelists wl ON wl.artist_id = artists.id AND " \
                            "wl.store_id = #{Store::GOOGLE_STORE_ID}")
                    .where("song_roles.role_type = 'remixer' OR creatives.role IN " \
                            "('primary_artist', 'featuring')")
                    .where(salepoints: { takedown_at: nil, stores: { id: Store::GOOGLE_STORE_ID } })
                    .where(distributions: { state: "delivered" })
                    .where(creatives: { role: "primary_artist" })
  end

  def has_minimum_track_count?
    return false if oac_releases.empty?

    size = oac_releases.joins(:songs).limit(MINIMUM_QUALIFYING_SONG_COUNT).length
    size == MINIMUM_QUALIFYING_SONG_COUNT
  end

  def uniq_artists(query)
    query.flat_map { |r| r.artists.map(&:name) }.uniq
  end

  def onboarded_artists
    return oac_releases if oac_releases.empty?

    uniq_artists(oac_releases.where("esi.identifier IS NOT NULL"))
  end

  def not_onboarded_artists
    return oac_releases if oac_releases.empty?

    uniq_artists(oac_releases.where("esi.identifier IS NULL"))
  end

  def has_google_store?
    current_user.salepoints
                .where(store_id: Store::GOOGLE_STORE_ID)
                .where.not(finalized_at: nil)
                .exists?
  end
end
