require "active_support/concern"

module DistributionProgressable
  extend ActiveSupport::Concern
  include AlbumHelper

  private

  def distribution_progress_level
    @distribution_progress_level ||= set_distribution_progress_level(@album)
  end

  def songs_complete
    @songs_complete ||= set_songs_complete(@album)
  end

  def set_distribution_progress_level(album)
    @distribution_progress_level = get_distribution_progress_level(album)
  end

  def set_songs_complete(album)
    @songs_complete = album_songs_status(album)
  end
end
