module InternationalGenre
  extend ActiveSupport::Concern

  SUBGENRE_FEATURE_FLAGS = {
    Genre::LATIN_GENRE_ID => :latin_genres,
    Genre::BRAZILIAN_GENRE_ID => :brazilian_genres,
    Genre::AFRICAN_GENRE_ID => :african_genres,
  }

  ENABLED_SUBGENRE_PARENT_IDS = [
    Genre::INDIAN_GENRE_ID
  ]

  def handle_parent_genre_params(release_params)
    set_genre(release_params, "primary") if release_params[:sub_genre_id_primary].present?

    return if release_params[:sub_genre_id_secondary].blank?

    set_genre(release_params, "secondary")
  end

  def check_subgenre_feature_flags(parent_id)
    return true if ENABLED_SUBGENRE_PARENT_IDS.include?(parent_id)

    SUBGENRE_FEATURE_FLAGS.any? do |parent_genres_id, flag|
      parent_id == parent_genres_id && FeatureFlipper.show_feature?(flag, current_user)
    end
  end

  def set_genre(release_params, precedence)
    @sub_genre = Genre.find(release_params["sub_genre_id_#{precedence}".to_sym])
    @sub_genre_parent_id = @sub_genre.parent_id

    return unless release_params["#{precedence}_genre_id".to_sym] == @sub_genre_parent_id.to_s &&
                  check_subgenre_feature_flags(@sub_genre_parent_id)

    release_params["#{precedence}_genre_id".to_sym] = release_params["sub_genre_id_#{precedence}".to_sym]
  end
end
