require "active_support/concern"

module RecaptchaVerifiable
  extend ActiveSupport::Concern

  include RecaptchaHelper

  def recaptcha
    return if current_user
    return if RecaptchaService.verify(params["g-recaptcha-response"], request.remote_ip)

    client_ip = request.headers["True-Client-IP"] || request.remote_ip
    LoginAttempt.record_failed_attempt(client_ip) if logging_in?
    reroute_failed_recaptcha
  end

  def reroute_failed_recaptcha
    @person = Person.new
    flash.now[:error] = custom_t("controllers.sessions.i_am_not_a_robot")
    render action: "new"
  end

  def handle_v3_recaptcha
    RecaptchaService.verify(params["g-recaptcha-response"], request.remote_ip, GoogleRecaptcha, true)
  end

  def captcha_on_login(is_verified)
    return unless !is_verified && !two_factor_auth_active?(@person)

    @person.suspicious_email_sent!
    ::SessionsMailer.suspicious_login_mailer(
      @person,
      request,
      Time.find_zone("Eastern Time (US & Canada)").now
    ).deliver_now
  end

  def captcha_on_pw_reset(is_verified)
    redirect_failed_password_reset unless is_verified
  end

  def logging_in?
    request.path_parameters[:controller] == "sessions"
  end

  def hide_captcha?
    CaptchaWhitelist.find_by(ip: request.remote_ip).present?
  end
end
