require "active_support/concern"

module ArtistUrls
  extend ActiveSupport::Concern

  included do
    before_action :assign_artist_urls, only: [:create, :update]
    before_action :gon_artist_urls, only: [:create, :edit, :update]
  end

  def assign_artist_urls
    @artist_urls = map_artist_name_to_artist_url
  end

  def gon_artist_urls
    artist_urls = params[:id] ? build_artist_urls : @artist_urls
    gon.push(artist_urls: artist_urls)
  end

  def build_artist_urls
    album = Album.find(params[:id])
    ArtistUrlsPresenter.build_artist_urls(album)
  end

  def release_creatives
    send("#{controller_name.singularize}_params")[:creatives]
  end

  def map_artist_name_to_artist_url
    return [] unless release_creatives.present? && artist_url_params.present?

    release_creatives
      .select { |c| c[:role] == "primary_artist" }
      .map.with_index do |creative, index|
      artist_urls = artist_url_params["artist_urls"][index]&.to_hash
      if artist_urls && artist_urls.values.any?(&:present?)
        artist_urls["artist_name"] = creative[:name]
        artist_urls
      end
    end.compact
  end

  def create_artist_ids
    record = instance_variable_get(:"@#{controller_name.singularize}")
    ArtistIdCreationWorker.perform_async(
      album_id: record.id,
      artist_urls: @artist_urls,
      person_id: current_user.id
    )
  end

  def artist_url_params
    permitted_artist_params = [:apple]

    if FeatureFlipper.show_feature?(:spotify_artist_ids, current_user)
      permitted_artist_params.concat([:spotify, :spotify_new_artist])
    end

    if FeatureFlipper.show_feature?(:new_apple_artist_id_creation, current_user)
      permitted_artist_params << :apple_new_artist
    end

    params.permit(artist_urls: permitted_artist_params)
  end
end
