module PayoneerAchFeeInfo
  extend ActiveSupport::Concern

  def load_payoneer_ach_fee_info
    domain = CountryWebsite.find_by(country: country_website)
    all_fees = PayoneerAchFee.where(source_currency: domain.site_currency).order(:target_country_code)
    @ach_fees = Hash.new { |fees, country_code| fees[country_code] = [] }
    all_fees.each { |fee| @ach_fees[fee.target_country_code] << fee }
    country_codes = all_fees.pluck(:target_country_code).uniq
    @countries_map =
      Country.where(iso_code: country_codes).each_with_object({}) do |country, mapping|
        mapping[country.name] = country.iso_code
      end
  end

  def load_default_ach_fee_info
    @default_fees = PayoneerAchFee.find_by(target_country_code: country_website)
  end
end
