module SpecializedReleasesHandleable
  extend ActiveSupport::Concern

  included do
    before_action :redirect_to_specialized_release

    include SpecializedReleasesHelper
  end

  private

  def redirect_to_specialized_release
    return unless specialized_releases_enabled?

    return unless ["albums", "singles", "ringtones"].include?(params[:controller])

    album = @album || @ringtone || @single || Album.find_by(id: params[:id])
    redirect_to request.original_url.gsub(params[:controller], redirect_path) if album.try(:is_tiktok_release?)
  end

  def redirect_path
    FeatureFlipper.show_feature?(:tiktok_freemium, current_user) ? "tiktok_releases" : "tiktok_singles"
  end
end
