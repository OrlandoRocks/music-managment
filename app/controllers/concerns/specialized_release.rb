# frozen_string_literal: true

require "active_support/concern"

# This concern supports the new freemium flow implemented in React as of 4/19/2021
module SpecializedRelease
  extend ActiveSupport::Concern
  extend AlbumApp

  FB_START = "fb_start"
  FB_END   = "fb_end"
  FB_PROMO = "fb_promo"
  FB_TERMS = "fb_terms"

  RELEASE_TYPES = [:facebook, :discovery_platforms]

  RELEASE_TYPE_CONFIGS = {
    facebook: {
      album_preorder_disabled: true,
      store_automator_disabled: true,
      amazon_on_demand_disabled: true,
      artist_url_disabled: true,
      timed_release_disabled: true,
      specialized_release_type: :facebook
    },
    discovery_platforms: {
      album_preorder_disabled: true,
      store_automator_disabled: true,
      amazon_on_demand_disabled: true,
      artist_url_disabled: true,
      timed_release_disabled: true,
      specialized_release_type: :discovery_platforms
    }
  }.with_indifferent_access

  def set_discovery_popup(previous_store_ids)
    return if !FeatureFlipper.show_feature?(
      :freemium_flow,
      current_user
    ) && !FeatureFlipper.show_feature?(
      :discovery_platforms,
      current_user
    )

    return fb_upsell unless has_more_than_fb?
    return FB_END if discovery_end?(previous_store_ids)
  end

  # Appended to a redirect and therefore is always seen if triggered
  def fb_upsell
    if session[:fb_promo].nil?
      session[:fb_promo] = true
      FB_PROMO
    else
      session[:fb_promo] = false
      FB_START
    end
  end

  def remove_automator_from_freemium
    return unless discovery_platform?

    Album::AutomatorService.update_automator(@album, false)
  end

  def valid_facebook_release?(album)
    return true unless album.salepoints.exists?(store_id: Store::FB_REELS_STORE_ID)

    album.songs_missing_facebook_copyrights.empty?
  end

  def remove_unsupported_facebook_salepoint(album)
    return if valid_facebook_release?(album)
    return if album.strictly_facebook_release? # cached JavaScript guard, should not reach otherwise

    album.salepoints.where(store_id: Store::FB_REELS_STORE_ID).destroy_all
  end

  def add_active_stores_to_album
    # creates non freemium salepoints for albums
    album = Album.find(album_to_active_stores_params[:album_id])
    stores = Store.freemium_upsell_stores(current_user)
    active_store_params = store_salepoints_params(stores)
    salepoint_params = active_store_params.merge(existing_salepoints(album, :discovery))
    salepoint_service = SalepointService.new(
      current_user,
      album,
      salepoint_params,
      true # apple_music_opt_in
    )
    save_salepoints = salepoint_service.call

    # re-add to cart in order to apply any available credit usages
    Product.add_to_cart(current_user, album)

    flash[:notice] =
      save_salepoints ? custom_t("salepoints.freemium_upsell_success") : custom_t("salepoints.freemium_upsell_failure")

    redirect_to cart_path
  end

  def discovery_platforms
    @salepoints = @album.salepoints.includes(:store, :variable_price, :inventory_usages)
    @songs = @album.songs.preload(:ytm_ineligible_song, salepoint_songs: :salepoint)
    setup_discovery_platform_views
    initialize_index_variables
    render :index
  end

  private

  def discovery_platform?
    discovery_enabled? && @album.strictly_discovery_release?(current_user)
  end

  def discovery_enabled?
    FeatureFlipper.show_feature?(
      :freemium_flow,
      current_user
    ) || FeatureFlipper.show_feature?(:discovery_path_platform, current_user)
  end

  def discovery_start?
    discovery_platform?
  end

  def had_only_discovery?(previous_store_ids)
    return if previous_store_ids.empty?

    (previous_store_ids - Store.discovery_platforms(current_user).pluck(:id)).empty?
  end

  def has_more_than_fb?
    (@album.salepoints.map(&:store_id) - Store.discovery_platforms(current_user).pluck(:id)).any?
  end

  def discovery_end?(previous_store_ids)
    had_only_discovery?(previous_store_ids) &&
      has_more_than_fb?
  end

  def specialized_release_type
    return @specialized_release_type if @specialized_release_type

    release_type = request.path.split("/")[1].to_sym
    @specialized_release_type = release_type if RELEASE_TYPES.include? release_type
  end

  def release_type_vars
    {
      artistUrlDisabled: release_type_config_vars[:artist_url_disabled],
      explicitFieldsEnabled: explicit_fields_enabled?,
      specializedReleaseType: release_type_config_vars[:specialized_release_type],
      timedReleaseDisabled: release_type_config_vars[:timed_release_disabled],
      variousFieldEnabled: various_field_enabled?,
    }.to_json(root: false)
  end

  def release_type_config_vars
    return @release_type_config_vars if @release_type_config_vars

    if specialized_release_type # At this time, all freemium releases display the same fields.
      RELEASE_TYPE_CONFIGS[specialized_release_type]
    else
      {}
    end
  end

  def handle_discovery_platform_path
    # redirect to dash if path is not enabled
    redirect_to dashboard_path if redirect_discovery_platform?
    redirect_to dashboard_path if redirect_facebook?
  end

  def discovery_platforms_enabled?
    FeatureFlipper.show_feature?(:discovery_platforms, current_user)
  end

  def facebook_freemium_enabled?
    FeatureFlipper.show_feature?(:freemium_flow, current_user)
  end

  def discovery_plaftorm_path?
    specialized_release_type == RELEASE_TYPE_CONFIGS[:discovery_platforms][:specialized_release_type]
  end

  def facebook_freemium_path?
    specialized_release_type == RELEASE_TYPE_CONFIGS[:facebook][:specialized_release_type]
  end

  def redirect_discovery_platform?
    (!discovery_platforms_enabled? && discovery_plaftorm_path?)
  end

  def redirect_facebook?
    facebook_freemium_path? && (!facebook_freemium_enabled? || discovery_platforms_enabled?)
  end
end
