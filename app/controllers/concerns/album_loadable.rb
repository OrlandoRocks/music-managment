require "active_support/concern"

module AlbumLoadable
  def preloaded_untaken_down_albums
    @preloaded_untaken_down_albums ||= current_user
                                       .albums
                                       .not_taken_down
                                       .includes(:review_audits, :petri_bundle, :artwork)
  end
end
