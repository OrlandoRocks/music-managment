require "active_support/concern"

module CountryPhoneCodeable
  extend ActiveSupport::Concern
  def sanitize_phone_number_params
    @country_phone_code ||= CountryPhoneCode.find_for_iso_code(country_website)
    sanitized_phone_number = sanitized_number(params.dig(:person, :prefix), params.dig(:person, :phone_number))
    sanitized_mobile_number = sanitized_number(params.dig(:person, :prefix), params.dig(:person, :mobile_number))
    { phone_number: sanitized_phone_number, mobile_number: sanitized_mobile_number }
  end

  def sanitized_number(prefix, number)
    return "#{prefix}#{number}" if prefix_number?(prefix, number)
    return "+#{@country_phone_code.code}#{number}" if needs_country_code?(number)

    number
  end

  def prefix_number?(prefix, number)
    prefix.present? && number.present?
  end

  def needs_country_code?(number)
    number.present? && number.first != "+"
  end
end
