require "active_support/concern"

module DistributionProgressBar
  extend ActiveSupport::Concern
  include AlbumHelper

  def set_distribution_progress(distro)
    @songs_complete              ||= distro && album_songs_status(distro)
    @distribution_progress_level ||= get_distribution_progress_level(distro)
  end

  def load_progress_bar_translations
    @progress_bar_translations ||= TranslationFetcherService
                                   .translations_for(
                                     "javascripts/progress_bar",
                                     COUNTRY_LOCALE_MAP[locale.upcase.to_s]
                                   )
  end
end
