require "active_support/concern"

module PayoutProvidable
  extend ActiveSupport::Concern

  def reject_withdrawal_by_admin
    flash[:error] = "Admin personnel cannot withdraw money."
    redirect_to_dashboard!
  end

  def redirect_to_withdraw!
    if TaxFormCheckService.needs_tax_form?(current_user.id)
      redirect_to edit_person_path(current_user, tab: "tax_info") and return
    end

    redirect_to withdraw_path
  end

  def redirect_to_payout_provider_withdrawal!
    redirect_to new_payout_transfer_path
  end

  def redirect_to_legacy_transactions_list_page!
    redirect_to transactions_for_admin_person_path(current_user)
  end

  def redirect_to_dashboard!
    redirect_to dashboard_path
  end

  def redirect_to_status!
    redirect_to payoneer_status_index_path
  end

  def has_no_balance?
    current_user.person_balance.balance.zero?
  end

  def restrict_access
    redirect_to withdraw_path
  end

  def needs_payout_provider?
    payoneer_payout_enabled? && (payout_status_is_nil? || payout_user_is_onboarding?)
  end

  def legacy_payout_system_access_required?
    return true unless payoneer_payout_enabled?

    payout_disabled_on_tunecore? || payout_provider_declined_user? || payout_user_is_pending?
  end

  def change_payout_method_link(redirect_url)
    api_client = Payoneer::PayoutApiClientShim.fetch_for(current_user)

    PayoutProvider::ApiService.new(person: current_user, api_client: api_client).register_new_user(
      person: current_user,
      redirect_url: redirect_url
    )
  end

  private

  def providable_payout_provider
    @memoized_payout_providable_payout_provider ||= current_user.payout_provider
  end

  def payout_user_is_approved?
    providable_payout_provider.try(:approved?)
  end

  def payout_disabled_on_tunecore?
    providable_payout_provider.try(:disabled?)
  end

  def payout_provider_declined_user?
    providable_payout_provider.try(:declined?)
  end

  def payout_user_is_onboarding?
    providable_payout_provider.try(:onboarding?)
  end

  def payout_user_is_pending?
    providable_payout_provider.try(:pending?)
  end

  def payout_status_is_nil?
    !providable_payout_provider.try(:tunecore_status) || !providable_payout_provider.try(:provider_status)
  end
end
