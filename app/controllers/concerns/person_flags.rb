require "active_support/concern"

module PersonFlags
  def person_flags
    @person_flags ||= {
      blocked_from_plans: session[:blocked_from_plans]
    }
  end
end
