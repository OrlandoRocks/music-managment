# frozen_string_literal: true

require "active_support/concern"

module CartPlanable
  extend ActiveSupport::Concern

  delegate :plan_in_cart?, to: :current_user
  delegate :owns_or_carted_plan?, to: :current_user

  def no_active_plan_deleting_plan?(purchase)
    current_user.legacy_user? && purchase.is_plan?
  end

  def clear_cart
    current_user.clear_cart!
  end

  def reapply_credit_usage_to_releases
    # find first eligible unpaid item for the credit_usage which was previously applied to destroyed purchase
    # re-add item to cart if item exists
    # this will apply the credit to the item in the cart

    unpaid_purchases = current_user.purchases.unpaid.where(related_type: "Album")
    item_to_apply_credit =
      unpaid_purchases.find do |purchase|
        CreditUsage.credit_available_for?(current_user, purchase.related.album_type)
      end
    return unless item_to_apply_credit

    Product.add_to_cart(current_user, item_to_apply_credit.related)
  end

  def validate_plan_is_eligible
    return unless Plan::PLAN_UPGRADE_AVAILABILITY.key?(Integer(params[:plan_id], 10))

    plan = Plan.find(Integer(params[:plan_id], 10))

    if current_user.plan && !PersonPlan.plan_eligible_for_user?(current_user, plan)
      flash[:error] = custom_t("plans.errors.added_ineligible_plan")
      redirect_to dashboard_path
    elsif current_user.number_of_plans_in_cart > 1
      flash[:error] = custom_t("plans.errors.too_many_plans")
      redirect_to dashboard_path
    end
  end
end
