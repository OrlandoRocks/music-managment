require "active_support/concern"

module WorldwideReleasable
  extend ActiveSupport::Concern

  included do
    before_action :load_countries, only: [:edit, :new]
  end

  def load_selected_countries(release = nil)
    @selected_country_codes =
      if release
        release&.country_iso_codes || []
      else
        Country.unsanctioned.map(&:iso_code)
      end
  end

  def load_countries
    @countries = Country.unsanctioned.map { |c| [c.name, c.iso_code] }
  end

  def territory_picker_state
    @state = nil

    return unless @countries.length != @selected_country_codes.length

    @state = ((@countries.length / 2) > @selected_country_codes.length) ? "include" : "exclude"
  end

  def set_release_countries(release, iso_codes)
    return if iso_codes.blank?

    release.countries = Country.where(iso_code: iso_codes.split(","))
  end

  def rebuild_territory_picker(iso_codes)
    load_countries
    @selected_country_codes = iso_codes&.split(",") || []
    territory_picker_state
  end
end
