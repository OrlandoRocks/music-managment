require "active_support/concern"

module TimedReleasable
  extend ActiveSupport::Concern

  def translate_release_date_time(record)
    begin
      record.golive_date = Album::DateTransformerService.translate_release_date_time(record)
      record.sale_date = record.golive_date.in_time_zone("Eastern Time (US & Canada)").to_date
    rescue ArgumentError => e
      record.errors.add(:golive_date, e.message)
    rescue
      record.errors.add(:golive_date, I18n.t("album_timed_release_fields.error_message"))
    end
  end

  def convert_golive_to_est(golive_date)
    if golive_date.present? && golive_date.is_a?(Time)
      golive_date.in_time_zone("Eastern Time (US & Canada)")
    else
      golive_date
    end
  end

  def convert_date_hash_to_golive_date(golive_date_hash)
    if golive_date_hash.present? && golive_date_hash.respond_to?(:keys)
      Album::DateTimeHashTransformerService.datetime_hash_to_iso(golive_date_hash)
    else
      golive_date_hash
    end
  end

  def convert_date_hash_to_sale_date(golive_date_hash)
    if golive_date_hash.present? && golive_date_hash.respond_to?(:keys)
      Album::DateTimeHashTransformerService.datetime_hash_to_sale_date(golive_date_hash)
    else
      golive_date_hash
    end
  end
end
