# frozen_string_literal: true

require "active_support/concern"

# Provide the album_app's data dependencies
module AlbumApp
  extend ActiveSupport::Concern
  include DistributionProgressable

  attr_reader :album

  included do
    before_action :redirect_to_release_type, only: :edit
  end

  DAYJS_FALLBACK_LOCALE = "en"
  DAYJS_LOCALE_MAP = {
    "cs-us": "cs_us",
    'de-DE': "de",
    "de-de": "de",
    'en-AU': "en_au",
    "en-au": "en_au",
    'en-CA': "en_ca",
    "en-ca": "en_ca",
    'en-GB': "en_gb",
    "en-gb": "en_gb",
    'en-IN': "en_in",
    "en-in": "en_in",
    "en-us": "en_us",
    'es-US': "es_us",
    "es-us": "es_us",
    'fr-CA': "fr_ca",
    "fr-ca": "fr_ca",
    'fr-FR': "fr",
    "fr-fr": "fr",
    "hu-us": "hu_us",
    "id-us": "id_us",
    'it-IT': "it",
    "it-it": "it",
    "nl-us": "nl_us",
    "pl-us": "pl_us",
    "pt-us": "pt_us",
    "ro-us": "ro_us",
    "ru-us": "ru_us",
    "th-us": "th_us",
    "tr-us": "tr_us",
    de: "de",
    fr: "fr",
    it: "it",
  }.with_indifferent_access.freeze

  def init_album_app_vars(release, release_class)
    return unless album_app_enabled?

    @album                  = release || release_class.new
    @album_app_translations = album_app_translations
    @dayjs_locale           = dayjs_locale
    @album_app_vars         = {
      id: "album_app",
      'data-album': serialized_album,
      'data-active-artists': active_artists,
      'data-copyrights-form-enabled': copyrights_form_enabled?,
      'data-countries': countries,
      'data-country-website': country_website,
      'data-dayjs-locale': @dayjs_locale,
      'data-features': features,
      'data-genres': genres,
      'data-language-codes': language_codes,
      'data-locale': I18n.locale,
      'data-person': person,
      'data-plans': plans,
      'data-possible-artists': serialized_artists,
      'data-specialized-release-type': specialized_release_type,
      'data-release-type-vars': release_type_vars,
      'data-spotify-artists-required': spotify_artists_required?,
    }

    set_page_title
  end

  private

  def active_artists
    current_user.active_artists.to_json(root: false)
  end

  def album_app_translations
    TranslationFetcherService.translations_for(
      "javascripts/album_app",
      COUNTRY_LOCALE_MAP[I18n.locale.upcase.to_s]
    )
  end

  def serialized_album
    Api::Backstage::AlbumApp::AlbumSerializer.new(album).to_json(root: false)
  end

  def copyrights_form_enabled?
    available_in_region?(country_website, "song_copyrights")
  end

  def countries
    @countries.to_json
  end

  def dayjs_locale
    result = DAYJS_LOCALE_MAP[I18n.locale]
    return result if result.present?

    Tunecore::Airbrake.notify("Missing Day.js locale mapping for #{I18n.locale}")
    DAYJS_FALLBACK_LOCALE
  end

  def features
    FeatureFlipper.get_features(FeatureFlipper::ALBUM_APP_FEATURES, current_user.id)
                  .to_json(root: false)
  end

  def genres
    @genres ||= Genre.display_genres(current_user).select(%i[id name parent_id]).to_json(root: false)
  end

  def language_codes
    @unordered_languages = LanguageCode.order("id ASC").select(%i[code description id])
    @languages = @unordered_languages[0..10] + @unordered_languages[11..].sort_by { |el| el[:description].downcase }

    check_feature_flag_languages
    @languages.to_json(root: false)
  end

  def check_feature_flag_languages
    unallowed_languages =
      LanguageCode::FEATURE_FLAG_LANGUAGES.reject do |lang|
        FeatureFlipper.show_feature?("#{lang}_language", current_user)
      end
    @languages = @languages.reject { |el| unallowed_languages.include? el[:description] }
  end

  def person
    {
      person_plan: current_user.person_plan&.plan_id,
      plan_features: plan_features
    }.to_json(root: false)
  end

  def plan_features
    current_user.plan&.plan_features_h || {}
  end

  def plans
    Plan.all.each_with_object({}) do |plan, hash|
      hash[plan.id] = plan.name
    end.to_json(root: false)
  end

  def serialized_artists
    current_user.artists.distinct.pluck(:name).to_json(root: false)
  end

  def explicit_fields_enabled?
    @album.album_type == "Single"
  end

  def various_field_enabled?
    @album.album_type == "Album"
  end

  def set_page_title
    @page_title = "#{@album.album_type.titleize}: #{display_name(@album)}" if album.persisted?
  end

  def album_app_enabled?
    FeatureFlipper.show_feature?(:album_app, current_user)
  end

  def specialized_release_type
    nil
  end

  # Values that vary by release type (album/single, or specialized_release)
  def release_type_vars
    {
      artistUrlDisabled: @artist_url_disabled,
      explicitFieldsEnabled: explicit_fields_enabled?,
      specializedReleaseType: @specialized_release_type,
      timedReleaseDisabled: @timed_release_disabled,
      variousFieldEnabled: various_field_enabled?
    }.to_json(root: false)
  end

  def get_release
    @release ||= @album || @ringtone || @single || Album.find_by(id: params[:id])
  end

  def redirect_to_release_type
    release = get_release
    return unless release

    redirect_path = get_polymorphic_edit_path(release)
    redirect_to redirect_path unless request.path == redirect_path
  end

  def verified_specialized_single(album)
    album&.is_tiktok_release? || album&.strictly_facebook_release?
  end

  def spotify_artists_required?
    FeatureFlipper.show_feature?(:spotify_artists_required, current_user)
  end
end
