require "active_support/concern"

module TwoFactorAuthable
  extend ActiveSupport::Concern

  ENROLLMENTS = "enrollments".freeze
  PREFERENCES = "preferences".freeze
  AUTHENTICATION = "authentication".freeze

  def two_factor_auth_required?(person = current_user)
    return false if under_admin_control?
    return false unless two_factor_feature_enabled?

    two_factor_auth_active?(person) && two_factor_auth_expired?(person)
  end

  def two_factor_feature_enabled?
    ENV["TWO_FACTOR_AUTHENTICATION_ENABLED"].to_s == "true"
  end

  def two_factor_auth_active?(person)
    !!two_factor_auth(person).try(:active?)
  end

  def two_factor_auth_expired?(person)
    !two_factor_auth(person).within_authentication_window?
  end

  def build_two_factor_auth_form(person)
    auth_params = {
      tfa_action: auth_sign_in_action,
      page: map_controller_action_to_page,
      person: person
    }.merge(person_params)

    TwoFactorAuth::AuthenticationForm.new(auth_params)
  end

  def person_params
    {
      email: params[:person][:email],
      password: params[:person][:password]
    }
  end

  def two_factor_auth_session_for(person)
    @auth_form = build_two_factor_auth_form(person)
    if @auth_form.valid? && @auth_form.perform
      token = TwoFactorAuth::TokenService.issue({ email: person.email })
      redirect_to new_two_factor_auth_sessions_path(
        token: token,
        two_factor_auth_authentication_form: {
          tfa_action: auth_authentication_action,
          page: map_controller_action_to_page
        }
      )
    else
      sign_in_failure
    end
  end

  def two_factor_authenticate!
    return false unless two_factor_auth_expired?(current_user)

    redirect_to two_factor_auth_authentications_path(
      two_factor_auth_authentication_form: {
        tfa_action: auth_sign_in_action,
        page: map_controller_action_to_page
      }
    )
  end

  def force_two_factor_authenticate!(success_callbacks = [])
    redirect_to two_factor_auth_authentications_path(
      two_factor_auth_authentication_form: {
        tfa_action: auth_sign_in_action,
        page: map_controller_action_to_page,
        success_callbacks: success_callbacks
      }
    )
  end

  def auth_authentication_action
    TwoFactorAuth::AuthenticationForm::ACTIONS[1]
  end

  def auth_sign_in_action
    TwoFactorAuth::AuthenticationForm::ACTIONS[0]
  end

  def map_controller_action_to_page
    {
      "my_account#withdraw_paypal" => "paypal_withdrawal",
      "stored_bank_accounts#new" => "new_stored_bank_account",
      "eft_batch_transactions#new" => "eft_withdrawal",
      "my_account#withdraw_check" => "check_withdrawal",
      "people#edit" => "account_settings",
      "sessions#create" => "create_session",
      "preferences#create" => "change_preferences",
      "preferences#show" => "change_preferences",
      "enrollments#destroy" => "disable_2fa",
      "cash_advances#request_advance" => "request_advance"
    }[controller_action]
  end

  def page_the_user_wanted_to_access(page, person = current_user)
    {
      "paypal_withdrawal" => withdraw_paypal_path,
      "check_withdrawal" => withdraw_check_path,
      "eft_withdrawal" => new_eft_batch_transaction_path,
      "account_settings" => edit_person_path(id: person.id),
      "new_stored_bank_account" => new_stored_bank_account_path(withdrawal_path: "1"),
      "create_session" => dashboard_path,
      "change_preferences" => two_factor_auth_preferences_path,
      "disable_2fa" => edit_person_path(current_user, tab: "account"),
      "request_advance" => request_advance_cash_advances_path
    }[page]
  end

  def two_factor_auth(person)
    @two_factor_auth ||= person.two_factor_auth
  end

  def set_toll_fraud_validation_variables
    @toll_fraud_validation_enabled = toll_fraud_validation_enabled?
    @toll_fraud_phone_codes = CountryPhoneCode.toll_fraud_phone_codes.pluck(:code).map(&:to_s)
  end

  def exceeds_max_daily_attempts?(person, type)
    persons_daily_attempts(person, type) >= allowed_daily_attempts(type)
  end

  def exceeds_signups_or_resends?
    preferences_and_exceeding_signups? || authentication_and_exceeding_resends?
  end

  def toll_fraud_daily_limit_error
    I18n.t("tfa_form.preferences_step.toll_fraud_daily_limit_message")
  end

  private

  def allowed_daily_attempts(type)
    case type
    when :signup
      ENV.fetch("MAX_2FA_SIGNUP_ATTEMPTS", "3").to_i
    when :resend
      ENV.fetch("MAX_2FA_RESEND_ATTEMPTS", "3").to_i
    end
  end

  def authentication_and_exceeding_resends?
    exceeds_max_daily_attempts?(current_user, :resend) && @step == AUTHENTICATION
  end

  def preferences_and_exceeding_signups?
    exceeds_max_daily_attempts?(current_user, :signup) && @step == PREFERENCES
  end

  def on_the_page?(params, type)
    case type
    when :authentication
      params.fetch(:page, "") == AUTHENTICATION
    when :enrollments
      params.fetch(:page, "") == ENROLLMENTS
    end
  end

  def persons_daily_attempts(person, type)
    case type
    when :signup
      return @persons_daily_signups if @persons_daily_signups

      @persons_daily_signups = person.two_factor_auth_events
                                     .signup_attempts_in_last_day
                                     .count
    when :resend
      return @persons_daily_resends if @persons_daily_resends

      @persons_daily_resends = person.two_factor_auth_events
                                     .resend_attempts_in_last_day
                                     .count
    end
  end
end
