# frozen_string_literal: true

# ExtendAccessToken
module ExtendAccessToken
  include ActiveSupport::Concern
  def extend_fb_token(access_token)
    client_id = ENV["FB_CLIENT_ID"]
    client_secret = ENV["FB_SECRET_KEY"]
    facebook_oauth = Koala::Facebook::OAuth.new(client_id, client_secret)
    response = facebook_oauth.exchange_access_token_info(access_token)
    response["access_token"]
  rescue Koala::Facebook::APIError
    nil
  end
end
