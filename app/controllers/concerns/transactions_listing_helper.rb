require "active_support/concern"

module TransactionsListingHelper
  extend ActiveSupport::Concern

  def fetch_transactions(person)
    PersonTransaction
      .by_target_type(params[:transaction_values].try(:values))
      .between_dates_by_month(@transaction_dates.start_date, @transaction_dates.end_date)
      .where(person_id: person.id)
      .then { |r| filter_rollover_transactions(r) }
      .order("person_transactions.created_at DESC, id DESC")
      .paginate(page: params[:page], per_page: 15)
  end

  def filter_rollover_transactions(relation)
    return relation if FeatureFlipper.show_feature?(:show_rollover_transactions, current_user)

    relation.non_rollover_transactions
  end
end
