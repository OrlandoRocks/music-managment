module FetchSingleYearProducts
  extend ActiveSupport::Concern

  def single_year_products
    @products ||= Product.one_yr_ad_hoc
  end

  def single_year_products_with_country(country_website_id)
    single_year_products.by_country(country_website_id)
  end
end
