require "active_support/concern"

module Compositionable
  extend ActiveSupport::Concern

  private

  def serialize_compositions(collection)
    ActiveModel::ArraySerializer.new(
      collection,
      each_serializer: Api::Backstage::CompositionsSerializer
    ).to_json(root: false)
  end

  def serialize_cowriters(collection)
    ActiveModel::ArraySerializer.new(
      collection,
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    ).to_json(root: false)
  end

  def pro_list
    PerformingRightsOrganization.select([:id, :name])
  end

  def composition_translations
    TranslationFetcherService
      .translations_for("javascripts/compositions", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
      .with_support_links(
        "submit-tax-info-howto" => "submit-tax-info-howto",
        "what-is-the-cae-ipi-number" => "what-is-the-cae-ipi-number",
        "What-are-splits-and-how-do-I-figure-out-my-split-" => "What-are-splits-and-how-do-I-figure-out-my-split-",
        "Can-I-submit-a-work-that-contains-a-sample-or-interpolation-of-a-copyrighted-composition-" => "Can-I-submit-a-work-that-contains-a-sample-or-interpolation-of-a-copyrighted-composition-",
        "Does-TuneCore-Publishing-Administration-collect-retroactively" => "Does-TuneCore-Publishing-Administration-collect-retroactively"
      )
  end
end
