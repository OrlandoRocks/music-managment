class BalanceHistories::FiltersController < ApplicationController
  include PayoutProvidable

  def show
    @filtered_transactions = BalanceHistory::TransactionsQueryBuilder.build(
      params[:filter_transactions].merge(person: current_user)
    )
  end
end
