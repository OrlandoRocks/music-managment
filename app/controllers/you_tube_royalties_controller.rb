class YouTubeRoyaltiesController < ApplicationController
  include Tunecore::CsvExporter
  include Utilities::FormDateRange

  layout "application"

  MAX_STATEMENT_TIME = 60

  before_action :parse_csv_filters, only: [:release_report, :song_report, :artist_report, :sales_period_report], if: -> { params[:format] == "csv" }
  before_action :load_page_title, only: [:release_report, :song_report, :artist_report, :sales_period_report]
  before_action :load_person
  before_action :load_release, only: [:individual_release_report, :individual_song_report]

  before_action :check_if_user_has_royalties, except: [:no_sales]
  before_action :load_albums_and_singles
  before_action :load_reporting_dates
  before_action :check_if_inital_page_load
  before_action :load_filter_parameters
  before_action :store_filter_parameters
  before_action :load_date_range_for_full_reports, only: [:release_report, :song_report, :artist_report, :sales_period_report]
  before_action :load_date_range_for_individual_reports, only: [:individual_release_report, :individual_song_report]
  before_action :set_currency_variables, only: [:release_report, :song_report, :artist_report, :sales_period_report]

  def release_report
    @report = create_full_report("release_report")

    respond_to do |format|
      format.html do
        @release_report, @net_revenue = @report.report
      rescue ActiveRecord::StatementInvalid => e
        handle_max_statement_time_exceeded_exception(e)
      end

      format.csv do
        filename = "youtube_release_report_#{Date.today}.csv"
        set_csv_headers(filename)
        render plain: @report.to_csv
      end
    end
  end

  def song_report
    @report = create_full_report("song_report")

    respond_to do |format|
      format.html do
        @song_report, @net_revenue = @report.report
      rescue ActiveRecord::StatementInvalid => e
        handle_max_statement_time_exceeded_exception(e)
      end

      format.csv do
        filename = "youtube_song_report_#{Date.today}.csv"
        set_csv_headers(filename)
        render plain: @report.to_csv
      end
    end
  end

  def artist_report
    @report = create_full_report("artist_report")

    respond_to do |format|
      format.html do
        @artist_report, @net_revenue = @report.report
      rescue ActiveRecord::StatementInvalid => e
        handle_max_statement_time_exceeded_exception(e)
      end

      format.csv do
        filename = "youtube_artist_report_#{Date.today}.csv"
        set_csv_headers(filename)
        render plain: @report.to_csv
      end
    end
  end

  def sales_period_report
    @report = create_full_report("sales_period_report")

    respond_to do |format|
      format.html do
        @sales_period_report, @net_revenue = @report.report
      rescue ActiveRecord::StatementInvalid => e
        handle_max_statement_time_exceeded_exception(e)
      end

      format.csv do
        filename = "youtube_sales_period_report_#{Date.today}.csv"
        set_csv_headers(filename)
        render plain: @report.to_csv
      end
    end
  end

  # Do not rescue max_statement_time exceeded error raised by the report.
  # There is nothing the user can do to fix this issue.
  # We need to optimize the queries or run in the background.
  def individual_release_report
    load_release

    @report = create_individual_report("sales_period_report", release: @release)
    @release_report, @net_revenue = @report.report

    @report = create_individual_report("song_report", release: @release)
    @song_report, @net_revenue_from_songs = @report.report
  end

  def individual_song_report
    @song = @release.songs.find(params[:song_id])

    @report = create_individual_report("sales_period_report", song: @song)
    @song_report, @net_revenue = @report.report

    if request.xhr?
      render layout: false
    else
      render
    end
  end

  protected

  # Handles ActiveRecord::StatementInvalid exception raised when max_statement_time
  # is exceeded in the database.
  #
  # Displays a flash error message, asking the user to change their date filters.
  #
  # This implementation is specific to MariaDB.
  def handle_max_statement_time_exceeded_exception(exception)
    # Re-raise the exception if not related to max statement time (specific to MariaDB).
    raise unless exception.message.include?("max_statement_time exceeded")

    flash.now[:error] = custom_t("controllers.you_tube_royalties.max_statement_time_exceeded")
  end

  private

  def parse_csv_filters
    [:release_types, :release_ids].each do |param|
      params[param] = params[param].split(" ") if params[param].present?
    end
  end

  def load_page_title
    @page_title = custom_t("controllers.you_tube_royalties.#{action_name}")
  end

  def load_release
    @release = @person.albums.find(params[:release_id])
  end

  def check_if_user_has_royalties
    @ytm_tracks = YtmTracks.new(current_user)
    render action: "no_sales" unless @person.you_tube_royalty_records.exists?
  end

  def full_date_range_of_royalties
    YouTubeRoyaltyRecord.date_range_of_royalties(current_user)
  end

  def create_full_report(report_type)
    Tunecore::Reports::YouTubeRoyalties.const_get(report_type.classify).new(
      @person,
      @start_date,
      @end_date,
      {
        release_types: params[:release_types],
        release_ids: params[:release_ids],
        max_statement_time: (request.format.html? ? MAX_STATEMENT_TIME : nil)
      }
    )
  end

  def create_individual_report(report_type, release: nil, song: nil)
    Tunecore::Reports::YouTubeRoyalties.const_get(report_type.classify).new(
      @person,
      @start_date,
      @end_date,
      {
        release: release,
        song: song,
        only_monetized: false,
        max_statement_time: (request.format.html? ? MAX_STATEMENT_TIME : nil)
      }
    )
  end

  def load_date_range_for_full_reports
    last_month = full_date_range_of_royalties.last.beginning_of_month

    @start_date = params.dig(:date, :start_date)&.to_date || last_month
    @end_date = params.dig(:date, :end_date)&.to_date || last_month
  end

  def load_date_range_for_individual_reports
    @start_date = params.dig(:date, :start_date)&.to_date || full_date_range_of_royalties.first
    @end_date = params.dig(:date, :end_date)&.to_date || full_date_range_of_royalties.last
  end

  def load_albums_and_singles
    @albums ||= Album.select("id, name").where("person_id = ? and album_type = 'Album' and payment_applied = 1", @person.id).order("name ASC")
    @singles ||= Album.select("id, name").where("person_id = ? and album_type = 'Single' and payment_applied = 1", @person.id).order("name ASC")
  end

  def load_reporting_dates
    full_date_range = full_date_range_of_royalties
    @reporting_dates = Utilities::FormDateRange.create_range((full_date_range.first || Date.today - 1.month).beginning_of_month, (full_date_range.last || Date.today).beginning_of_month)
  end

  def check_if_inital_page_load
    # Check if there are any parameters set because on the initial page load because we want all checkboxes to be selected by default.
    # We check if size is greater than 2 because there are 2 default params (action and controller).
    @initial_page_load = params.keys.size == 2
  end

  def load_filter_parameters
    # persist filters across different reports or if we are exporting the current filtered report
    if @initial_page_load && (session[:filter_params] || params[:format] == "csv")
      params.merge(session[:filter_params].permit!).permit!
    end

    return if params[:action] == "export"
  end

  def store_filter_parameters
    session[:filter_params] = params.dup
    session[:filter_params].delete_if { |k| ["action", "controller", "commit"].include?(k) }
  end

  def set_currency_variables
    service = CurrencyDisplayService.new(current_user)
    @multiple_currency_domain = service.multiple_currency_domain?
    @display_currency         = service.display_currency
    @transaction_currency     = service.transaction_currency
    @selected_currency        = params.fetch(:selected_currency, @transaction_currency)
  end
end
