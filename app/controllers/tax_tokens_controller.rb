class TaxTokensController < ApplicationController
  before_action :load_person, :load_token

  def update
    tax_token = TaxTokenApiService.update(@person.id, @tax_token)

    render json: { token: tax_token.token }
  end

  private

  def load_token
    @tax_token = TaxToken.find(params[:id])
  end
end
