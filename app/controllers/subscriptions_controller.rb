class SubscriptionsController < ApplicationController
  before_action :load_person
  after_action :zero_out_discovery_renewals, only: [:buy]

  def index
    redirect_to "/people/#{current_user.id}/edit/?tab=subscriptions"
  end

  def show
    redirect_to "/people/#{current_user.id}/edit/?tab=subscriptions"
  end

  def cancel
    redirect_to "/people/#{current_user.id}/edit/?tab=subscriptions"
  end

  def keep
    redirect_to "/people/#{current_user.id}/edit/?tab=subscriptions"
  end

  def buy
    renewal = current_user.renewals.with_renewal_information.find(params[:id])
    product_id = params[:product_id]
    product =
      if product_id && product_id != "n"
        Product.find(product_id)
      else
        Product.find(renewal.renewal_product_id)
      end

    handle_prior_failed_purchase(renewal)

    Product.add_to_cart(current_user, renewal, product)

    redirect_to cart_path
  end

  private

  def handle_prior_failed_purchase(renewal)
    purchase = Purchase
               .includes(:invoice)
               .includes(invoice: :invoice_settlements)
               .unpaid
               .find_by(person: current_user,
                        related_id: renewal.id,
                        related_type: Renewal.name)
    return if purchase.nil?

    Subscription::NullifyInvoiceIDService.nullify!(purchase)
  end

  def zero_out_discovery_renewals
    current_user
      .purchases
      .unpaid
      .not_in_invoice
      .select { |item| item.related_type == "Renewal" && item.related.discovery_platform_renewal? }
      .each do |item|
      item.update(discount_cents: item.cost_cents)
    end
  end
end
