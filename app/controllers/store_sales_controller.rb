class StoreSalesController < ApplicationController
  before_action :load_person

  def index
    @report = StoreReport.new({ person_id: current_user.id, date: params[:date] })
  end
end
