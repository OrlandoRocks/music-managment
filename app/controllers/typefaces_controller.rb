class TypefacesController < ApplicationController
  def list
    @typefaces = Typeface.all

    respond_to do |format|
      format.html { raise "html is not supported." }
      format.json { render json: @typefaces.to_json }
    end
  end
end
