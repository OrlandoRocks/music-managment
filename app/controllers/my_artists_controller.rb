class MyArtistsController < ApplicationController
  before_action :load_person
  layout "tc-foundation"

  def index
    @my_artists = MyArtist.title_artists_for(current_user.id)
  end

  def show
    @translations = TranslationFetcherService.translations_for(
      "javascripts/my_artists",
      COUNTRY_LOCALE_MAP[current_user.country_website.country]
    )
                                             .with_support_links(contact_customer_care_link: :new_distribution_request)
    @artist       = current_user.artists.find(params[:id])
    @my_artist    = MyArtist.new(current_user.id, params[:id])
  end
end
