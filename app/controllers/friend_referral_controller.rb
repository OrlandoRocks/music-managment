class FriendReferralController < ApplicationController
  include RegionHelper

  before_action :load_person
  before_action :redirect_unavailable_regions

  def signup
    campaign = FriendReferralCampaign.signup(current_user)

    if campaign.errors.empty?
      @kickback_url = campaign.campaign_url
      @campaign_id = campaign.campaign_id
    end
    if request.post?
      if campaign.errors.empty?
        redirect_to "/people/#{current_user.id}/edit/?tab=refer"
      else
        flash[:error] = custom_t("controllers.friend_referral.unavailable")
        redirect_to "/people/#{current_user.id}/edit/?tab=refer"
      end
    else
      respond_to do |format|
        if campaign.errors.empty?
          format.json { render json: { "campaign_id" => @campaign_id, "url" => @kickback_url } }
        else
          format.json { render json: { "refer-a-friend-status" => "unavailable" } }
        end
      end
    end
  end

  def index
    cookies[:ref_campaign] = { value: params[:campaignid], expires: 45.days.from_now }
    cookies[:ref_type]     = { value: "raf", expires: 45.days.from_now }
    cookies[:ref_token]    = { value: params[:mbsy], expires: 45.days.from_now }

    if params[:campaignid] && params[:mbsy]
      cookies[:raf_campaign_id]   = { value: params[:campaignid], expires: 45.days.from_now }
      cookies[:raf_campaign_code] = { value: params[:mbsy], expires: 45.days.from_now }
    end
    redirect_to home_path
  end

  def show
    redirect_to "/people/#{current_user.id}/edit/?tab=refer"
  end

  def referee_list
    referees = FriendReferral.referees(current_user)
    referees_hash =
      referees.collect do |r|
        {
          name: r.referee.name,
          date: r.created_at
        }
      end

    respond_to do |format|
      format.json { render json: { referees: referees_hash }.to_json }
    end
  end

  def payouts
    referees = FriendReferral.select("commission, created_at").where(referrer_id: current_user.id, status: "posted")
    referees_hash =
      referees.collect do |r|
        {
          amount: r.commission,
          date: r.created_at
        }
      end

    respond_to do |format|
      format.json { render json: { payouts: referees_hash }.to_json }
    end
  end

  private

  def redirect_unavailable_regions
    redirect_to dashboard_path unless available_in_region?(current_user.country_domain, "refer_a_friend")
  end
end
