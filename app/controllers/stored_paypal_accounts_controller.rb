require "cgi"
require "paypal_sdk/paypal_api"
class StoredPaypalAccountsController < ApplicationController
  before_action :load_person
  skip_before_action :redirect_stem_users, only: [:ec_step1, :ec_step3]

  def destroy
    begin
      stored_paypal_account = @person.stored_paypal_accounts.find(params[:id])

      preferred_account_deleted = @person.person_preference && !@person.renew_with_balance? && @person.preferred_paypal_account == stored_paypal_account.id

      account_deleted = stored_paypal_account.destroy(current_user)

      if account_deleted
        if preferred_account_deleted
          flash[:notice] = custom_t("controllers.stored_paypal_accounts.removed_prf_account")
          note = "User removed preferred PayPal account id:#{params[:id]}"
        else
          flash[:notice] = custom_t("controllers.stored_paypal_accounts.removed_account")
          note = "User removed PayPal account id:#{params[:id]}"
        end

        Note.create(
          related: @person,
          note_created_by: current_user,
          ip_address: request.remote_ip,
          subject: "Payment Preferences Changed",
          note: note
        )
      else
        flash[:error] = custom_t("controllers.stored_paypal_accounts.must_have_prf_account")
      end
    rescue ActiveRecord::RecordNotFound
      flash[:error] = custom_t("controllers.stored_paypal_accounts.account_not_found")
    end

    redirect_to person_preferences_path
  end

  # SetExpressCheckout API call
  def ec_step1
    reset_paypal_session
    paypal_payin_config = current_user.paypal_payin_provider_config

    set_urls(request, params[:redirect])

    session[:pay_with_balance] = params[:pay_with_balance].nil? ? 0 : 1
    session[:ccode] = params[:currency]
    session[:remote_ip] = request.remote_ip
    session[:auth_amt] = 0.01
    session[:paypal_payin_config_id] = paypal_payin_config.id
    transaction = PayPalAPI.send_to_checkout(paypal_payin_config, @cancel_url, @return_url, "Authorization", current_user.currency, [{ name: "Authorization", amt: session[:auth_amt], qty: 1 }])
    session[:payment_action] = "Authorization"

    if transaction.success?
      token = transaction.response["TOKEN"].to_s
      session[:token] = token
      redirect_to(@paypal_redirect_url + token)
    else
      redirect_to action: "error"
    end
  rescue Errno::ENOENT => e
    redirect_to action: "error"
  end

  def ec_step3
    session[:payerid] = params[:PayerID].to_s
    payin_config = PayinProviderConfig.find(session[:paypal_payin_config_id])

    get_details = PayPalAPI.get_checkout_details(payin_config, session[:token].to_s, session[:payerid].to_s)

    redirect_to action: "error" and return unless get_details.success?

    session[:ecdetails] = get_details.response
    session[:ccode] = session[:ecdetails]["CURRENCYCODE"]
    session[:user_email] = session[:ecdetails]["EMAIL"].to_s
    response, account = StoredPaypalAccount.process_authorization(auth_options(payin_config))
    # session[:step3]=transaction.response
    # redirect_to :action => 'thanks' #successfully added paypal account

    redirect_to action: "error" and return if response == "Failure" || account.blank?

    flash[:notice] = custom_t("controllers.stored_paypal_accounts.added_account")
    Sift::EventService.update_account_payment_methods(@person, cookies, request)

    create_auth_note(account)

    if params[:redirect]
      redirect_to params[:redirect]
    else
      redirect_to controller: "person_preferences", action: "index"
    end
  end

  def auth_options(payin_config)
    token, payerid, amt, ccode, ip, email, pay_with_balance = session[:token], session[:payerid], session[:auth_amt], session[:ccode], session[:remote_ip], session[:user_email], session[:pay_with_balance]
    reset_paypal_session
    {
      person: current_user,
      email: email,
      _ip: ip,
      token: token,
      payerid: payerid,
      amount: amt,
      currency: ccode,
      pay_with_balance: pay_with_balance,
      payin_config: payin_config
    }
  end

  def create_auth_note(account)
    Note.create(
      related: @person,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Payment Preferences Changed",
      note: "Added new paypal account id:#{account.id} and updated perferred payment type to this account"
    )
  end

  def error
    reset_paypal_session
    render action: "error"
  end

  private

  def set_urls(request, redirect)
    @paypal_redirect_url = PaypalTransaction::EXPRESS_CHECKOUT_URL
    return_url = construct_url(PaypalTransaction::STORED_RETURN_URL, redirect)
    cancel_url = construct_url(PaypalTransaction::STORED_CANCEL_URL, redirect)
    host = request.host.to_s
    port = request.port.to_s
    protocol = "https"
    @cancel_url = !port.nil? ? "#{protocol}://#{host}:#{port}#{cancel_url}" : "#{protocol}://#{host}#{cancel_url}"
    @return_url = !port.nil? ? "#{protocol}://#{host}:#{port}#{return_url}" : "#{protocol}://#{host}#{return_url}"
  end

  def construct_url(url, redirect)
    return unless url
    return url unless redirect

    url + "?redirect=" + redirect
  end
end
