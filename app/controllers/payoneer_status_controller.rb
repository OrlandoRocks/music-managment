class PayoneerStatusController < ApplicationController
  include PayoutProvidable
  include PayoneerAchFeeInfo

  layout "tc-foundation"

  STEP_TRANSLATION_MAP = {
    "1": nil,
    "2": "payoneer_status.index.application_submitted",
    "3": "payoneer_status.index.application_accepted",
    "4": "payoneer_status.index.complete_tax_form",
  }.with_indifferent_access.freeze

  SIGN_UP_STEPS = [1, 2, 3, 4].freeze

  def index
    @page_title = "Withdraw Funds"
    @payout_provider = PayoutProvider.find_by_person_id(current_user.id)
    @payoneer_response = change_payout_method_link(
      payout_provider_registration_landings_url(host: current_user.country_website.url)
    ) if @payout_provider
    load_payoneer_ach_fee_info
  end
end
