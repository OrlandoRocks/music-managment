class AlbumSalesReportsController < ReportsController
  def index
    @album_report = current_user.sales_report(default_options)
    @albums = @album_report.albums
    render_report
  end
end
