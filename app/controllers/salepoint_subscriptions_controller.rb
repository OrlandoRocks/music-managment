class SalepointSubscriptionsController < ApplicationController
  before_action :load_person
  before_action :load_album
  before_action :require_store_automator_feature

  def create
    @album.build_salepoint_subscription(is_active: true)
    sp_sub = @album.salepoint_subscription

    if sp_sub.save
      Product.add_to_cart(@person, sp_sub) if sp_sub.can_distribute?

      respond_to do |format|
        format.html { redirect_to album_path(@album) }
      end
    else
      respond_to do |format|
        format.html { redirect_to album_path(@album) }
      end
    end
  end

  def destroy
    sp_sub = @album.salepoint_subscription

    if sp_sub && !sp_sub.finalized? && sp_sub.destroy
      respond_to do |format|
        format.html { redirect_to album_path(@album) }
      end
    else
      respond_to do |format|
        format.html { redirect_to album_path(@album) }
      end
    end
  end

  def toggle_active
    salepoint_subscription = @album.salepoint_subscription

    head :no_content and return unless salepoint_subscription.toggle_active!

    respond_to do |format|
      format.html { redirect_to album_path(@album) }
    end
  end

  private

  def load_album
    @album = Album.find(params[:album_id])
  end

  def require_store_automator_feature
    if feature_enabled?(:store_automator)
      true
    elsif request.xhr?
      render_js_error
    else
      redirect_to album_path(@album)
    end
  end

  def render_js_error
    head :internal_server_error
  end
end
