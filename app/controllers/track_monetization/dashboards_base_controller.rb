class TrackMonetization::DashboardsBaseController < ApplicationController
  layout "tc-foundation"

  skip_before_action :alert_rejections, unless: :is_html_request?
  skip_before_action :maintenance_alert, unless: :is_html_request?
  skip_before_action :set_gon_variables, unless: :is_html_request?
  skip_before_action :set_gon_two_factor_auth_prompt, unless: :is_html_request?
  skip_before_action :force_pw_reset, unless: :is_html_request?
  skip_before_action :redirect_sync_users, unless: :is_html_request?
  skip_before_action :redirect_ytm_approvers, unless: :is_html_request?

  def show
    respond_to do |format|
      format.html
      format.json do
        render json: serialized_tracks, root: false
      end
    end
  end

  protected

  def is_html_request?
    request.format.html?
  end
end
