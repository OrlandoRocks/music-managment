class MyAccountController < ApplicationController
  include Tunecore::CsvExporter
  include Tunecore::PaypalTransferActions
  include Utilities::FormDateRange
  include RegionHelper
  include TwoFactorAuthable
  include PayoutProvidable
  include TransactionsListingHelper

  before_action :blocked_withdrawal?, only: [:withdraw, :withdraw_check, :withdraw_paypal]
  before_action :reject_withdrawal_by_admin, only: [:withdraw, :withdraw_check], if: :current_user_is_taken_over?
  before_action :evaluate_and_tax_block_user, only: [:withdraw_check, :withdraw_paypal], if: :evaluate_tax_blocking?
  before_action -> { redirect_to account_settings_path(tab: "taxpayer_id") },
                only: [:withdraw_check, :withdraw_paypal],
                if: :tax_blocked_user_attempting_withdrawal?
  before_action :redirect_to_dashboard!, only: [:withdraw, :withdraw_check, :withdraw_paypal], if: :has_no_balance?
  before_action :redirect_to_payout_provider_withdrawal!,
                only: [:withdraw, :withdraw_check, :withdraw_paypal],
                if: :payout_provider_enabled_and_has_withdrawal_flow?
  before_action :load_person
  before_action :load_states, on: [:withdraw, :withdraw_check]
  before_action :load_form_variables, on: [:withdraw, :withdraw_check, :withdraw_help]
  before_action :load_transaction_dates, on: [:export_transactions, :list_transactions]
  before_action :check_minimum_amount, only: [:withdraw_check]
  before_action :two_factor_authenticate!, only: [:withdraw_check, :withdraw_paypal], if: :two_factor_auth_required?
  before_action :set_paper_trail_whodunnit, only: [:withdraw_paypal]

  layout "application_old"

  def index
    redirect_to controller: "sales", action: "date_report"
  end

  def exports
    if FeatureFlipper.show_feature?(:disable_sales, current_user)
      redirect_to controller: "sales", action: "date_report" && return
    end

    @page_title = custom_t("controllers.my_account.download_monthly_sales_reports")
    @sale_months = ReadOnlyReplica::SalesRecord.months_with_sales(current_user).map(&:sales_period)

    return redirect_to action: "index" if @sale_months.empty?
  end

  def you_tube_exports
    @page_title = custom_t("controllers.my_account.download_monthly_youtube_reports")
    @royalty_months = YouTubeRoyaltyRecord.months_with_royalties(current_user)

    return redirect_to controller: "you_tube_royalties", action: "release_report" if @royalty_months.empty?
  end

  def export_reporting_period
    @page_title = custom_t("controllers.my_account.download_reporting_period_sales_reports")
    @reporting_months = ReadOnlyReplica::SalesRecord.reporting_months_with_sales(current_user).map(&:reporting_period)

    return redirect_to action: "index" if @reporting_months.empty?
  end

  def sales_record_export
    @report = Tunecore::Reports::SalesReport.new(current_user, params[:type].to_sym, month: params[:month])

    filename = "tunecore-musicsales-#{params[:type]}-#{Date.parse(params[:month]).strftime('%m-%Y')}.csv"
    set_csv_headers(filename)
    set_streaming_headers

    # Stream the csv by setting the response to an enumerator. Rails iterates
    self.response_body = @report.to_streamable_csv(month: params[:month], person: current_user)
  end

  def you_tube_export
    @report =  Tunecore::Reports::YouTubeRoyalties::DetailReport.new(current_user, Date.parse(params[:month]), Date.parse(params[:month]))
    filename = "tunecore-youtube-royalties-sales-period-#{Date.parse(params[:month]).strftime('%m-%Y')}.csv"
    set_csv_headers(filename)
    set_streaming_headers

    # Stream the csv by setting the response to an enumerator. Rails iterates
    self.response_body = @report.streamable_csv
  end

  def withdraw
    if show_payoneer_redesigned_onboarding?
      redirect_to payout_application_processing? ? payoneer_status_index_path : payoneer_fees_path
    end
    @page_title = "Withdraw Funds"
    @eft_max_amount = EftBatchTransaction::MAX_WITHDRAWAL
    @eft_expected_days_lower = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_LOWER
    @eft_expected_days_upper = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_UPPER
    @payout_provider = PayoutProvider.find_by(person_id: current_user.id)
    @payoneer_response = change_payout_method_link(
      payout_provider_registration_landings_url(host: current_user.country_website.url)
    ) if @payout_provider

    case (params["payment_method"] rescue nil)
    when PayoutTransfer::PAYOUT_METHODS[:ACCOUNT]
      redirect_to controller: "payout_transfers", action: "new"
    when "paypal"
      @withdrawal = PaypalTransfer.new
      redirect_to controller: "my_account", action: "withdraw_paypal"
    when "check"
      @withdrawal = ManualCheckTransfer.new
      redirect_to controller: "my_account", action: "withdraw_check"
    when "eft"
      if @person.has_stored_bank_account?
        redirect_to new_eft_batch_transaction_path
      else
        redirect_to controller: "stored_bank_accounts", action: "new", withdrawal_path: 1
      end
    else
      @withdrawal = PaypalTransfer.new
      flash.now[:error] = custom_t("controllers.my_account.select_payment") if request.post?
    end
  end

  def withdraw_check
    @check_fee = PayoutServiceFee.current("check_service").amount

    if country_website != "US"
      @withdrawal = PaypalTransfer.new
      redirect_to controller: "my_account", action: "withdraw_paypal"
    end

    if request.post?
      payment_amount = withdrawal_params[:payment_amount].presence || Tunecore::CurrencyInputNormalizer.normalize_currency_input(
        withdrawal_params,
        :payment_amount_main_currency,
        :payment_amount_fractional_currency
      )

      flash.discard

      @withdrawal = ManualCheckTransfer.generate(current_user, withdrawal_params.merge(payment_amount: payment_amount))

      if @withdrawal.new_record?
        render controller: "my_account", action: "withdraw_check"
      else
        flash[:notice] = custom_t("controllers.my_account.confirming_withdrawal_via_check", amount: view_context.number_to_currency(payment_amount))
        redirect_to controller: "my_account", action: "list_transactions"
      end
    else
      @withdrawal = ManualCheckTransfer.new
    end
  end

  def withdraw_help
    render layout: "application_lightbox"
  end

  # Renders template
  def trend_reports
    redirect_to controller: "trend_reports", action: "index"
  end

  def list_transactions
    @page_title = custom_t("controllers.my_account.transfers")

    @lyric_button_translations = TranslationFetcherService
                                 .translations_for("javascripts/lyric_button", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
                                 .translations["lyric_button"]

    # we want to display 2 different types of transactions based on 1 selection
    if params[:transaction_values] && params[:transaction_values].include?("Invoice")
      params[:transaction_values]["BatchTransaction"] = "BatchTransaction"
    end
    @transactions = fetch_transactions(current_user)

    service = CurrencyDisplayService.new(current_user)
    @multiple_currency_domain = service.multiple_currency_domain?
    @display_currency         = service.display_currency
    @transaction_currency     = service.transaction_currency
    @selected_currency        = params.fetch(:selected_currency, @display_currency)

    @show_popup                 = open_lyric_popup?
    @transactions_sales_records = get_transactions_sales_records(@transactions)
    @transactions_split_records = PersonTransaction.with_splits_information(current_user.id, @transactions).group_by(&:transaction_id)
    @advance_display            = CashAdvanceRegistrationService.new(current_user).advance_display
    @initial_load               = params.keys.size == 2
    @has_never_distributed      = current_user.has_never_distributed?
    @compliance_info_form ||= ComplianceInfoForm.new(current_user)
    @composer = current_user.publishing_composers.primary.first
  end

  def export_transactions
    if params[:transaction_values] && params[:transaction_values].include?("Invoice")
      params[:transaction_values]["BatchTransaction"] = "BatchTransaction"
    end

    @transactions = PersonTransaction.by_target_type(params[:transaction_values].try(:keys))
                                     .between_dates_by_month(@transaction_dates.start_date, @transaction_dates.end_date)
                                     .where(person_id: current_user.id)
                                     .order("person_transactions.created_at DESC, id DESC")

    @transactions_sales_records = get_transactions_sales_records(@transactions)
    @transactions_split_records = PersonTransaction.with_splits_information(current_user.id, @transactions).group_by(&:transaction_id)
    csv = CSV.new(response = "", row_sep: "\r\n")

    header_row = %w[Date Description Sales Debit Credit Balance Currency]
    header_row.insert(5, "Withholding") if FeatureFlipper.show_feature?(:us_tax_withholding, current_user)
    csv << header_row

    @transactions.each do |t|
      # if its a PersonIntake (meaning a sales posting) we just want to display that its a sales post
      if t.target_type == "PersonIntake"
        row = [l(t.created_at, format: :dash), "Sales Posted", nil, t.debit, t.credit, t.previous_balance + t.credit - t.debit, t.currency]
        if FeatureFlipper.show_feature?(:us_tax_withholding, current_user)
          row.insert(5, helpers.balance_history_withholding(t))
        end
        csv << row

        if @transactions_sales_records[t.id].present?
          @transactions_sales_records[t.id].each do |tsr|
            csv << [nil, "#{tsr.store_name} #{tsr.country} #{tsr.period_sort}", tsr.store_amount]
          end
        end
      else
        row = [l(t.created_at, format: :dash), t.comment, nil, t.debit, t.credit, t.previous_balance + t.credit - t.debit, t.currency]
        if FeatureFlipper.show_feature?(:us_tax_withholding, current_user)
          row.insert(5, helpers.balance_history_withholding(t))
        end
        csv << row
      end
    end

    filename = "tunecore-transactions_#{l(Time.now, format: :underscore)}.csv"
    set_csv_headers(filename)
    render plain: response
  end

  def export_intake_transactions
    transaction_id = params[:transaction_id].to_i
    transactions   = PersonTransaction.where(id: transaction_id)

    @transactions_sales_records = get_transactions_sales_records(transactions)

    csv = CSV.new(response = "", row_sep: "\r\n")
    # csv << ["Store", "Sales Period","Country","Debit USD", "Credit USD","Revenue Total" , "Transaction Comment"]
    csv << ["Posting Date", "Store", "Sales Period", "Amount"]

    @transactions_sales_records[transaction_id].each do |transaction|
      csv << [
        transaction.created_at,
        transaction.store_name,
        transaction.period_sort,
        transaction.store_amount
      ]
    end

    filename = "transactions-intakes-#{Date.today}.csv"
    set_csv_headers(filename)
    render plain: response
  end

  def view_royalty_invoice
    @outbound_invoice = OutboundInvoice.find(params[:invoice_id])
    outbound_data_service = InvoiceGenerator::GenerateOutboundData.new(@outbound_invoice)
    @invoice_data = outbound_data_service.invoice_data
    @static_corporate_entity = outbound_data_service.static_corporate_entity
    @sub_total = outbound_data_service.sub_total

    respond_to do |format|
      format.html { render_invoice_to_html }
      format.pdf do
        send_data render_invoice_to_pdf,
                  filename: "#{@invoice_data[:invoice_number]}.pdf",
                  type: "application/pdf"
      end
    end
  end

  private

  def withdrawal_params
    params.require(:withdrawal).permit(
      :transfer_status,
      :person_id,
      :payment_cents,
      :admin_charge_cents,
      :currency,
      :payee,
      :customer_type,
      :first_name,
      :last_name,
      :company_name,
      :address1,
      :address2,
      :city,
      :state,
      :postal_code,
      :country,
      :phone_number,
      :debit_transaction_id,
      :refund_transaction_id,
      :export,
      :export_date,
      :approved_by_id,
      :suspicious_flags,
      :payment_amount,
      :payment_amount_main_currency,
      :payment_amount_fractional_currency,
      :other_state,
      :confirm_password,
      :password_entered
    )
  end

  def get_transactions_sales_records(transaction_set)
    return [] if FeatureFlipper.show_feature?(:disable_sales, current_user)

    PersonTransaction.with_sales_record_information(
      current_user.id, transaction_set
    ).group_by(&:transaction_id)
  end

  def load_states
    @countries = country_dropdown_for_website(current_user.country_website_id)
    @states = UsState::STATES | CanadianProvince::PROVINCES_ARRAY
  end

  def load_form_variables
    @payout_eft_fee = PayoutServiceFee.current("eft_service").amount
    @payout_check_fee = PayoutServiceFee.current("check_service").amount
    @payout_check_min_amount = CheckTransfer.min_amount
  end

  def load_transaction_dates
    @transaction_dates = Person::TransactionDatesFinder.new(current_user, params[:date])
  end

  def check_minimum_amount
    return unless current_user.balance < CheckTransfer.min_amount

    flash[:error] = custom_t("controllers.my_account.min_check_amount", amount: view_context.number_to_currency(CheckTransfer.min_amount))
    redirect_to controller: "my_account", action: "withdraw"
  end

  def open_lyric_popup?
    !params[:open_lyric_popup].nil?
  end

  def blocked_withdrawal?
    render "my_account/blocked_withdrawal" if current_user.blocked_withdrawal?
  end

  def render_invoice_to_html
    case @static_corporate_entity.name
    when CorporateEntity::TUNECORE_US_NAME
      render_tc_invoice
    when CorporateEntity::BI_LUXEMBOURG_NAME
      render_bi_invoice
    else
      render_bi_invoice
    end
  end

  def render_tc_invoice
    render "invoices/outbound/tc_invoice.html.erb",
           locals: {
             inbound: false,
             invoice_data: @invoice_data,
             sub_total: @sub_total,
             discounts: 0,
             pdf: false,
             static_corporate_entity: @static_corporate_entity
           },
           layout: "layouts/application"
  end

  def render_bi_invoice
    render "invoices/outbound/bi_invoice.html.erb",
           locals: {
             inbound: false,
             invoice_data: @invoice_data,
             pdf: false,
             static_corporate_entity: @static_corporate_entity
           },
           layout: "layouts/application"
  end

  def render_invoice_to_pdf
    render_to_string pdf: @invoice_data[:invoice_number],
                     template: "invoice_pdf/believe_invoice_pdf.html.erb",
                     layout: "layouts/pdf",
                     locals: {
                       inbound: false,
                       pdf: true,
                       invoice_data: @invoice_data,
                       static_corporate_entity: @static_corporate_entity
                     },
                     footer: {
                       html: outbound_pdf_marginals(:footer)
                     },
                     header: {
                       html: outbound_pdf_marginals(:header)
                     },
                     page_size: "A4",
                     margin: { top: 50, bottom: 25 }
  end

  def outbound_pdf_marginals(type)
    template = type == :header ? "outbound_invoice_pdf_header" : "outbound_footer"

    {
      template: "invoice_pdf/#{template}",
      layout: "layouts/pdf",
      locals: {
        invoice_data: @invoice_data,
        static_corporate_entity: @static_corporate_entity
      }
    }
  end

  def save_compliance_info
    return true unless FeatureFlipper.show_feature?(:bi_transfer, current_user)

    @compliance_info_form ||= ComplianceInfoForm.new(current_user)
    @compliance_info_form.save(compliance_info_params)

    @compliance_info_form.errors.any? do
      translated_error_field_names = @compliance_info_form.errors.full_messages.map do |field_name|
        custom_t("people.renewal_billing_info.#{field_name}")
      end.join(", ")
      flash[:error_dialog] = custom_t("people.renewal_billing_info.error_saving_info", error: translated_error_field_names)
      return false
    end

    true
  end

  def compliance_info_params
    params.fetch(:withdrawal).permit(:customer_type).merge(
      params.permit(:first_name, :last_name, :company_name)
    )
  end
end
