class ProductReportsController < ApplicationController
  before_action :set_report_options
  before_action :requires_reporting_role

  layout "admin"

  def index
    @page_title = "Dashboard Reports"
    create_snapshot_report
  end

  def detail_reports
    @page_title = "Detail Reports"

    case @report_type
    when "product"
      create_product_report
    when "snapshot"
      create_snapshot_report
    when "renewal"
      create_renewal_report
    end
  end

  def preorder_reports
    @page_title = "Preorder Reports"
    create_preorder_report
  end

  private

  def requires_reporting_role
    return if current_user.has_role?("Report Viewer", false)

    flash[:error] = "You don't have permission to view that page."
    redirect_to admin_home_path and return
  end

  def set_report_options
    @new_css = true
    @report_type = params[:report] || "product"
    @resolution = params[:resolution] || "day"
    set_product_id
    set_range
    set_custom_dates
    set_country_website
  end

  def set_range
    case @resolution
    when "day"
      @range = params[:day_range] || "last_7_days"
    when "month"
      @range = params[:month_range] || "last_6_months"
    when "week"
      @range = params[:week_range] || "last_6_weeks"
    end
  end

  def set_product_id
    @product_id =
      if !params[:product_id] || params[:product_id] == "all"
        nil
      else
        params[:product_id]
      end
  end

  def set_custom_dates
    unless params[:custom_date_range] &&
           (params[:week_range] == "custom" || params[:day_range] == "custom" || params[:month_range] == "custom")

      return
    end

    @custom_dates = {}
    @custom_dates[:start_date] = Date.parse("#{params[:custom_date_range][:start_date][:day].to_i}/#{params[:custom_date_range][:start_date][:month].to_i},#{params[:custom_date_range][:start_date][:year].to_i}") rescue nil
    @custom_dates[:end_date] = Date.parse("#{params[:custom_date_range][:end_date][:day].to_i}/#{params[:custom_date_range][:end_date][:month].to_i},#{params[:custom_date_range][:end_date][:year].to_i}") rescue nil
  end

  def set_country_website
    params[:country] ||= "US" # if params are not set, default to 'US'
    @country_website = CountryWebsite.find_by(country: params[:country].to_s)
  end

  def create_snapshot_report
    @sales_overview_report = ProductSalesOverviewReport.new(
      {
        country_website_id: @country_website.id,
        report_type: ProductReport::UNGROUPED,
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates,
      }
    )
    @customer_report = CustomerOverviewReport.new(
      {
        country_website_id: @country_website.id,
        report_type: ProductReport::UNGROUPED,
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates,
        headers: @sales_overview_report.headers
      }
    )
    @product_report = ProductReport.new(
      {
        country_website_id: @country_website.id,
        report_type: ProductReport::GROUPED,
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates,
        grouped_by: ProductReport::GROUPED_BY_PRODUCT,
        headers: @sales_overview_report.headers
      }
    )
    @preorder_report = PreorderProductReport.new(
      {
        country_website_id: @country_website.id,
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates,
        headers: @sales_overview_report.headers
      }
    )
    @headers = @sales_overview_report.data_table.headers
  end

  def create_product_report
    @product_report = ProductReport.new(
      {
        country_website_id: @country_website.id,
        report_type: ProductReport::UNGROUPED,
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates,
        product_id: @product_id
      }
    )
  end

  def create_preorder_report
    @preorder_report = PreorderProductReport.new(
      {
        country_website_id: @country_website.id,
        preorder_type: params[:preorder_type],
        resolution: @resolution,
        range: @range,
        custom_dates: @custom_dates
      }
    )
  end

  def create_renewal_report
  end
end
