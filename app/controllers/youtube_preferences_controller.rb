class YoutubePreferencesController < ApplicationController
  include YtmRedirector

  before_action :load_person
  before_action :require_ytm

  layout "application"

  def show
    @page_title = "Connect To YouTube"
    @preferences = @person.youtube_preference || YoutubePreference.new
    gon.push(
      {
        youtube_api_key: ENV["YOUTUBE_API_KEY"],
        youtube_client_id: ENV["YOUTUBE_CLIENT_ID"]
      }
    )
  end

  def create
    @preferences = @person.build_youtube_preference(create_params)

    respond_to do |format|
      if @preferences.save
        send_email_confirmation
        flash[:notice] = custom_t("controllers.youtube_preferences.successful_save")

        format.html { redirect_to youtube_monetizations_path }
      else
        format.html { render action: "show" }
      end
    end
  end

  def update
    @preferences = @person.youtube_preference

    respond_to do |format|
      if @preferences.update(update_params)
        send_email_confirmation
        flash[:notice] = custom_t("controllers.youtube_preferences.successful_update")

        format.html { redirect_to youtube_monetizations_path }
      else
        flash[:error] = @preferences.errors.full_messages
        format.html { render action: "show" }
      end
    end
  end

  def log_api_error
    Airbrake.notify("Youtube Preferences API Error:", response: error_params, user: @current_user)
  end

  private

  def create_params
    update_params
  end

  def update_params
    params
      .require(:youtube)
      .permit(
        :person_id,
        :channel_id,
        :whitelist,
        :mcn,
        :mcn_agreement
      )
  end

  def error_params
    params.require(:response)
  end

  def send_email_confirmation
    PersonNotifier.youtube_publishing_confirmation(@person).deliver
  end
end
