class PreviewArtistsController < ApplicationController
  def show
    render plain: ArtistNameTitleizerService.artist_names(primary_artists)
  end

  private

  def primary_artists
    primary_artists = params[:song].present? ? Album.find(params[:id]).primary_artists.map(&:name) : []
    primary_artists + creative_params
                      .select { |artist| artist["role"] == "primary_artist" }
                      .map { |a| a["name"] }
  end

  def creative_params
    klass = [:song, :single, :ringtone].find { |klass| params[klass].present? }
    params[klass].fetch(:creatives, {})
  end
end
