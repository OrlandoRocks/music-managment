class MyBandController < ApplicationController
  before_action :redirect_to_tc_social

  layout "application_old"
  skip_before_action :login_required, only: [:index]

  def index
  end

  def redirect_to_tc_social
    redirect_to "https://social.tunecore.com/artist-pages"
  end
end
