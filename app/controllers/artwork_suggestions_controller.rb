class ArtworkSuggestionsController < ApplicationController
  before_action :load_person
  before_action :load_album
  before_action :load_covers, only: [:new]

  skip_before_action :set_gon_two_factor_auth_prompt, only: [:index, :new, :preview]

  def index
    if FeatureFlipper.show_feature?(:disabled_artwork, current_user)
      render "artwork_unavailable"
      return true
    end

    @album.covers.clear
    gon.new_artwork_suggestion_path = new_album_artwork_suggestion_path(@album)
    gon.cover_id_array              = suggestion_indices
  end

  def new
    render :new, formats: :js
  end

  def preview
    @cover = Cover.find(params[:id])
    @cover.render(600)
    @image_path  = @cover.s3_cover_path
    @action_path = album_artwork_suggestion_path(album_id: @album, id: @cover)

    render :preview, formats: :js
  end

  def update
    @cover = Cover.find(params[:id])
    file_location = @cover.generate_artwork
    @cover.delete_images([300, 600])

    dest_path = File.join(Rails.root, "public", file_location)

    @artwork = Artwork.create_or_find_by(album: @album)

    @artwork.assign_artwork(File.open(dest_path), true)
    @artwork.save

    @album.covers.each { |cover| cover.destroy unless cover == @cover }

    redirect_to(album_path(@album))
  end

  protected

  def load_album
    @album ||=
      if @person.is_administrator
        Album.eager_load(:covers).find(params[:album_id])
      else
        @person.albums.eager_load(:covers).find(params[:album_id])
      end
  end

  def suggestion_indices
    @suggestion_indices ||= (1..6).to_a
  end

  def load_covers
    @covers = suggestion_indices.map do |_id|
      begin
        cover = ArtworkSuggestion::CoverSuggestionService.suggest_for_genre(
          Genre.find(@album.primary_genre_id),
          @album
        )
        cover.render(300)
        cover
      rescue => e
        Rails.logger.info "Failure calling ArtworkSuggestion::CoverSuggestionService.suggest_for_genre for album_id [#{@album.id}]: #{e.message}"
        Airbrake.notify(
          e,
          {
            person: @person,
            params: params,
            album: @album
          }
        )
        next
      end
    end.compact
  end
end
