class SalepointsController < ApplicationController
  include DistributionProgressBar
  include SalepointSystem
  include SalepointHelper
  include SpecializedReleasesHelper
  include AutomatorManager
  include ReleaseOfferUpdater
  include SpecializedRelease

  layout "application"

  before_action :load_person
  before_action :load_album, except: [:popup, :storelogotest]
  before_action :load_progress_bar_translations, only: [:index]
  before_action -> { set_distribution_progress(@album) }, only: [:index]
  after_action :update_release_offers, only: [:create, :add_active_stores_to_album]

  skip_before_action :alert_rejections
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:index]

  TIKTOK_CANCELLED = "tiktok_cancelled".freeze
  TIKTOK_TRIGGERED = "tiktok_triggered".freeze

  #
  #  Grab Salepoints and display them in the correct order.
  #
  def index
    if @album.status == "down"
      flash[:notice] = custom_t("controllers.salepoints.cannot_edit_down_store")
      redirect_to "/albums/#{@album.id}" and return
    end
    @salepoints = @album.salepoints.includes(:store, :variable_price, :inventory_usages)
    @songs = @album.songs.preload(:ytm_ineligible_song, salepoint_songs: :salepoint)
    @show_automator = current_user.can_do?(:store_automator)
    setup_store_group_views
    initialize_index_variables
  end

  def show
    redirect_to select_album_distribution_path(@album)
  end

  def create
    manage_automator_state(params)
    automator_salepoints_update(@album, params)

    previous_store_ids = @album.salepoints.map(&:store_id)

    respond_to do |format|
      format.json { return render json: { status: 200 } }
      format.html {}
    end

    @show_automator = current_user.can_do?(:store_automator)

    if params.fetch(:album, {}).fetch(:salepoints, nil)
      salepoint_service = SalepointService.new(
        current_user,
        @album,
        album_salepoints_params,
        apple_music_opt_in
      )
      save_salepoints = salepoint_service.call

      if save_salepoints
        if params[:upgrade]
          cookies[:in_upgrade] = true
          redirect_to "/albums/#{@album.id}/salepoints"
        else
          flash[:notice] = custom_t("controllers.salepoints.successful_save")
          remove_automator_from_freemium
          save_salepoints_redirect(salepoint_service, previous_store_ids)
        end
      else
        setup_variables_for_index_view
        render :index
      end
    else
      setup_variables_for_index_view
      render :index
    end
  end

  def activate_itunes_ww
    salepoint_service = SalepointService.new(
      current_user,
      @album,
      itunes_ww_params,
      "on"
    )
    salepoint_service.call

    DistributionCreator.create(@album, "iTunesWW")
    respond_to do |format|
      format.js
    end
  end

  def discovery_platforms
    @salepoints = @album.salepoints.includes(:store, :variable_price, :inventory_usages)
    @songs = @album.songs.preload(:ytm_ineligible_song, salepoint_songs: :salepoint)

    @show_automator = false
    setup_discovery_platform_views
    initialize_index_variables
    render :index
  end

  protected

  def setup_variables_for_index_view
    @album.valid?
    @salepoints = fix_unsaved_salepoints
    setup_store_group_views
    initialize_index_variables
  end

  def load_album
    @album = @person.is_administrator? ? Album.find(params[:album_id]) : @person.albums.find(params[:album_id])
  end

  def fix_unsaved_salepoints
    all_salepoints = @album.salepoints
    existing_salepoints = @album.reload.salepoints
    new_salepoints = all_salepoints - existing_salepoints
    new_salepoints.each { |s| s.id = nil }

    existing_salepoints + new_salepoints
  end

  private

  def initialize_index_variables
    @showItunesWW = !(@album.payment_applied? && @album.has_itunes_salepoints? && !@album.has_itunes_ww_salepoint?)
    @person = current_user
  end

  def salepoints_success_path(aod_salepoint, aod_added, other_params = {})
    opts = { id: @album.id }

    return edit_album_salepoint_artwork_template_path(
      album_id: @album.id,
      salepoint_id: aod_salepoint
    ) if FeatureFlipper.show_feature?(
      :salepoints_redesign, current_user
    ) && aod_added

    album_path(opts.merge(other_params))
  end

  def album_salepoints_params
    new_salepoints = params_include_freemium_stores? ? freemium_salepoint_params : digital_stores_salepoint_params

    eligible_salepoint_params.merge(new_salepoints)
  end

  def apple_music_opt_in
    return "on" if params_include_freemium_stores? && @album.apple_music

    params[:apple_music_opt_in]
  end

  def freemium_salepoint_params
    existing_salepoints(@album, :exclude_discovery)
  end

  def digital_stores_salepoint_params
    existing_salepoints(@album, :discovery)
  end

  def eligible_salepoint_params
    return salepoint_params.select! { |sp|
             Integer(sp, 10).in? Store::DISCOVERY_PLATFORMS
           } if block_paid_salepoint_changes?

    salepoint_params
  end

  def salepoint_params
    Array.wrap(params.dig(:album, :salepoints)&.keys).compact.each_with_object({}) do |store_id, hash|
      permitted_salepoint_params_hash = params.dig(:album, :salepoints, store_id, :salepoint)
        &.permit(:id, :store_id, :variable_price_id, :has_rights_assignment)
        &.to_hash
        &.symbolize_keys

      next if permitted_salepoint_params_hash.blank?

      hash[store_id] = { salepoint: permitted_salepoint_params_hash }
    end
  end

  def params_include_freemium_stores?
    salepoint_params.keys.any? { |store_id|
      Store.discovery_platforms(current_user).pluck(:id).include? Integer(store_id, 10)
    }
  end

  def save_salepoints_redirect(salepoint_service, previous_store_ids)
    redirect_to salepoints_success_path(
      salepoint_service.aod_salepoint,
      salepoint_service.aod_just_added,
      popup: set_discovery_popup(previous_store_ids)
    )
  end

  def itunes_ww_params
    @store = Store.find(Store::ITUNES_WW_ID)
    variable_price_id = @store.default_variable_price.id
    { @store.id => { salepoint: { store_id: @store.id, variable_price_id: variable_price_id } } }
  end

  def album_to_active_stores_params
    params.permit(:album_id)
  end

  def block_paid_salepoint_changes?
    @album.finalized? && current_user.can_use_store_expander? == false
  end
end
