module FeatureSpecDbProxy
  class RemoteActiveRecordBaseProxiesController < ActionController::Base
    def destroy
      model_klass = params["model"].camelize.constantize
      model_klass.delete_all

      head :ok
    end
  end
end
