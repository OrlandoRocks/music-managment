module FeatureSpecDbProxy
  class RemoteFactoryProxiesController < ActionController::Base
    def create
      object = FactoryBot.create(*params["_json"])

      render json: object.as_json(root: false)
    end

    def update
      object = params["model"].constantize.find_by(id: params["id"])

      if object
        object.update(params["attributes"])

        render json: object.as_json(root: false)
      else
        head 404
      end
    end

    private

    # Overrides strong params for this controller.
    # This is safe because it's only used in testing.
    def params
      request.params
    end
  end
end
