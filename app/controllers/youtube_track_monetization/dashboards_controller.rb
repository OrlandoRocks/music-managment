class YoutubeTrackMonetization::DashboardsController < TrackMonetization::DashboardsBaseController
  include KnowledgebaseLinkHelper

  before_action :load_translations, on: :show, if: :is_html_request?
  before_action :youtube_dashboard_accessible?
  before_action :redirect_to_main_dashboard, on: :show, unless: :subscribed?

  def show
    @has_royalty        = yt_royalty?
    @youtube_preference = current_user.youtube_preference
    @ytsr_help_link     = knowledgebase_link_for("youtube-music-and-sound-recording-revenue-services", country_website)
    super
  end

  private

  def load_translations
    @translations ||= TranslationFetcherService.translations_for(
      "javascripts/youtube_tracks_dashboard",
      COUNTRY_LOCALE_MAP[country_website]
    )
  end

  def redirect_to_main_dashboard
    redirect_to dashboard_path
  end

  def serialized_tracks
    ytsr  = Store.find_by(abbrev: "ytsr")
    query = TrackMonetization::DashboardQueryBuilder.build(current_user, ytsr.id)

    ActiveModel::ArraySerializer.new(
      query,
      each_serializer: TrackMonetization::DashboardSerializer,
      context: { current_user: current_user }
    ).to_json
  end

  def legacy_subscription?
    already_subscribed?("YTTracks") || current_user.has_active_ytm?
  end

  def subscribed?
    legacy_subscription? || (current_user.has_ytsr_proxy? && !current_user.ytm_blocked?)
  end
end
