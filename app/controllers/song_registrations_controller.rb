class SongRegistrationsController < ApplicationController
  include PublishingAdministrationHelper
  helper_method :sort_column, :sort_direction

  before_action :redirect_to_new_publishing_administration_enrollment, if: :rights_app_enabled?
  before_action :redirect_to_pub_admin_compositions_page, if: :rights_app_enabled?
  before_action :load_person, :requires_publishing_purchase
  before_action :load_my_compositions, only: [:index, :paginate]
  before_action :load_disputes, only: [:resolve_disputes]

  layout "application"

  def index
    @albums = @my_compositions.albums
    @singles = @my_compositions.singles
    @ringtones = @my_compositions.ringtones
    @statuses = @my_compositions.statuses
    @stats = @my_compositions.stats
    @splits = @my_compositions.splits
    @composer = current_user.composers.first

    respond_to do |format|
      format.html
      format.js
    end
  end

  def paginate
    respond_to do |format|
      format.js {
        render partial: "compositions", content_type: "text/html", locals: { compositions_paginated: @compositions }
      }
    end
  end

  def resolve_disputes
    respond_to do |format|
      format.html
    end
  end

  def send_disputes
    if params[:disputes][:compositions]
      PersonNotifier.resolve_disputes(params[:disputes]).deliver

      flash[:notice] = custom_t("controllers.song_registrations.investigate_dispute")
    else
      flash[:notice] = custom_t("controllers.song_registrations.currently_dont_have_disputes")
    end
    redirect_to action: "index"
  end

  private

  def requires_publishing_purchase
    redirect_to home_path unless current_user.paid_for_composer? || current_user.paid_for_publishing_composer?
  end

  def sort_column # compositions.name, publishing_splits.percent, status
    %w[composition_name splits_percentage status].include?(params[:sort]) ? params[:sort] : "status"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def actual_sort
    case sort_column
    when "composition_name"
      ["compositions.name #{sort_direction}", "publishing_splits.percent asc", "status asc"]
    when "splits_percentage"
      ["publishing_splits.percent #{sort_direction}", "compositions.name asc", "status asc"]
    when "status"
      ["status #{sort_direction}", "compositions.name asc", "publishing_splits.percent asc"]
    else
      ["status asc", "compositions.name asc", "publishing_splits.percent asc"]
    end
  end

  def load_disputes
    @my_compositions = MyCompositions.new(current_user)
    @disputes = @my_compositions.compositions_in_dispute
  end

  def load_my_compositions
    @my_compositions = MyCompositions.new(current_user)
    params[:page] ||= 1
    params[:per_page] ||= 25
    order = actual_sort
    @compositions = @my_compositions.search(
      {
        page: params[:page],
        per_page: params[:per_page],
        order: order,
        keyword: params[:search],
        filters: { splits: params[:split], album_ids: params[:release], statuses: params[:status] }
      }
    )
  end

  def redirect_to_new_publishing_administration_enrollment
    return unless current_user.account.nil? || current_user.account.publishing_composers.empty?

    redirect_to new_publishing_administration_enrollment_path
  end

  def redirect_to_pub_admin_compositions_page
    redirect_to publishing_administration_composition_path(current_user.account.publishing_composers.first)
  end
end
