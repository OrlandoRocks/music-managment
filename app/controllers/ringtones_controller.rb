class RingtonesController < ApplicationController
  include CreativeSystem::Controller
  include AlbumHelper
  include ArtistUrls
  include WorldwideReleasable
  include SpecializedReleasesHandleable

  helper_method :creative_roles_for_select
  before_action :load_editable_ringtone, only: [:edit, :update, :destroy]
  before_action :load_ringtone, only: [:edit, :update, :show]
  before_action :gon_main_artist_text, only: [:new, :show, :edit]
  before_action :gon_datepicker_setup, only: [:new, :edit]
  before_action :gon_itunes_delays, only: [:new, :edit, :create, :update]
  before_action :translate_dates, except: [:new, :edit]
  before_action :scrub_artist_names, only: [:update, :create]
  before_action :set_view_variables, only: [:new, :edit]

  skip_before_action :set_gon_two_factor_auth_prompt, only: [:new, :show]

  layout "application"

  ADVANCED_RINGTONE_FEATURES = %i[custom_label territory_restrictions upc].freeze

  def show
    @uploadURL   = BIGBOX_UPLOAD_URL
    @registerURL = BIGBOX_REGISTER_URL
    @assetsURL   = BIGBOX_ASSETS_URL

    @page_title = @ringtone.name
    @ringtone.calculate_steps_required
    @song = @ringtone.song
    @creatives = @ringtone.creatives
    @artwork = @ringtone.artwork
    @unpurchased_stores = @ringtone.unselected_stores
    @sso_cookie_value = generate_sso_cookie_value(@song.album.person_id)
    @album = @song.album
    @show_add_stores = show_add_stores?(@ringtone)

    gon_custom_t("confirm_deletion", "songs.delete.are_you_sure")
    gon_custom_t("could_not_upload", "songs.upload.could_not_upload")
    gon_custom_t("try_again", "songs.upload.try_again")
  end

  def new
    @page_title = custom_t("controllers.ringtones.create_a_new_ringtone")
    domain_service = MultipleCurrencyDomainService.new(current_user)
    @ringtone = Ringtone.new(
      sale_date: Time.now,
      language_code: domain_service.distribution_language_code_select(default_language_code)
    )
    @ringtone.person = current_user
    @creatives = @ringtone.creatives
    load_selected_countries
    set_sale_date_vars
  end

  def create
    @ringtone = Ringtone.new(
      ringtone_params.merge(
        person: current_user,
        metadata_language_code_id: metadata_language_code_id(ringtone_params)
      ).except(:iso_codes)
    )

    set_release_countries(@ringtone, ringtone_params[:iso_codes])
    if @ringtone.errors.empty? &&
       is_a_cyrillic_language?(ringtone_params[:language_code_legacy_support]) &&
       @ringtone.save

      render :cyrillic_release and return false
    end

    set_default_label_name

    if @ringtone.errors.empty? && @ringtone.save
      Note.create(related: @ringtone, note_created_by: current_user, ip_address: request.remote_ip, subject: "Ringtone created", note: "Ringtone was created.")
      create_artist_ids
      flash[:success] = custom_t("controllers.ringtones.ringtone_created")

      redirect_to ringtone_path(@ringtone)
    else
      load_selected_countries
      load_countries
      @creatives = @ringtone.creatives
      flash[:error] = custom_t("controllers.ringtones.error_creating_ringtone")

      set_all_variables_for_view

      render :new
    end
  end

  def edit
    @creatives = @ringtone.creatives
    load_upc(@ringtone)
    load_selected_countries(@ringtone)
    territory_picker_state
    set_sale_date_vars
  end

  def update
    set_release_countries(@ringtone, ringtone_params[:iso_codes])
    @ringtone.assign_attributes(update_params)

    set_default_label_name

    if @ringtone.errors.empty? && @ringtone.save
      flash[:success] = custom_t("controllers.ringtones.ringtone_saved")
      create_artist_ids

      redirect_to ringtone_path(@ringtone)
    else
      rebuild_territory_picker(ringtone_params[:iso_codes])
      load_upc(@ringtone)
      @creatives = @ringtone.creatives
      flash[:error] = custom_t("controllers.ringtones.error_updating_ringtone")

      set_all_variables_for_view

      render :edit
    end
  end

  protected

  def load_editable_ringtone
    @ringtone = current_user.albums.find(params[:id])
    return true unless @ringtone.finalized?

    flash.now[:error] = custom_t("controllers.ringtones.cannot_release")
  end

  def load_ringtone
    @ringtone =
      if current_user.is_administrator?
        Album.find(params[:id])
      else
        current_user.albums.find(params[:id])
      end

    redirect_to_polymorphic_type(@ringtone)
  rescue => e
    flash[:error] = custom_t("controllers.ringtones.unable_to_load_page")
    redirect_to dashboard_path
  end

  def update_params
    result = ringtone_params.except(:iso_codes)
    result = reject_optional_upc(result) if reject_optional_upc_number?(@ringtone, result)
    result
  end

  def initial_ringtone_params
    permitted_params = [
      :name,
      :clean_version,
      :iso_codes,
      :label_name,
      :language_code_legacy_support,
      :optional_isrc,
      :optional_upc_number,
      :orig_release_year,
      :parental_advisory,
      :primary_genre_id,
      :release_datetime_utc,
      :sale_date,
      :secondary_genre_id,
      :song_id,
      { creatives: [:id, :role, :name] },
    ]

    params
      .require(:ringtone)
      .permit(permitted_params)
  end

  def ringtone_params
    if (sale_date = initial_ringtone_params[:sale_date].presence)
      sale_datetime =
        sale_date
          &.to_datetime
          &.in_time_zone("Eastern Time (US & Canada)")
          &.beginning_of_day
      new_golive_date =
        sale_datetime
          &.strftime("%Y-%m-%d %H:%M:%S")
      new_release_datetime_utc =
        sale_datetime
          &.in_time_zone("UTC")
          &.strftime("%Y-%m-%d %H:%M:%S")

      initial_ringtone_params.merge(
        golive_date: new_golive_date,
        release_datetime_utc: new_release_datetime_utc
      )
    else
      initial_ringtone_params
    end
  end

  def advanced_features?
    return true unless FeatureFlipper.show_feature?(:plans_pricing, current_user)

    ADVANCED_RINGTONE_FEATURES.all? { |f| current_user.can_do?(f) }
  end

  def can_schedule_release?
    return true unless FeatureFlipper.show_feature?(:plans_pricing, current_user)

    current_user.can_do?(:scheduled_release)
  end

  def set_view_variables
    @cannot_distribute_to_itunes = current_user.can_do?(:ringtones) == false
    @advanced_features           = advanced_features?
  end

  def set_sale_date_vars
    gon.push(
      {
        canScheduleRelease: can_schedule_release?,
        saleDate: @ringtone.sale_date
      }
    )
  end

  private

  def set_all_variables_for_view
    set_view_variables
    gon_datepicker_setup
    set_sale_date_vars
  end

  def set_default_label_name
    @ringtone.set_default_label_name if @ringtone.label.blank?
  end
end
