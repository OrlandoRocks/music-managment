class StoreManagerController < ApplicationController
  # The Store Expander will not auto-select all stores (default behavior), if the resulting
  # price is gte this value
  MAXIMUM_AUTO_SELECTED_TOTAL_COST = 300

  layout "application"

  before_action :load_person
  before_action :set_gon_all_stores

  #
  # Get list of all stores and the user releases
  # missing from each
  #
  def show
    init_manager_vars

    @expander_banner_num          = [@missing_total_count, @apple_music_eligible_albums.count].max
    @have_distributed             = ["selling", "no_longer_live"].include?(@person.dashboard_state)
    @unpaid_expanders_in_cart     = @person.purchases.unpaid.not_in_invoice.where(related_type: "Salepoint").count
    @unpaid_automators_in_cart    = @person.purchases.unpaid.not_in_invoice.where(related_type: "SalepointSubscription").count
    @initial_total                = initial_total
    @free_translated              = I18n.t(:free)
    @uncheck_all                  = uncheck_all?
    @uncheck_stores               = uncheck_stores?

    respond_to do |format|
      format.html
    end
  end

  def create
    redirect_to dashboard_path and return if params[:data].blank? || disable_manager?

    data = JSON.parse(params.fetch(:data))

    handle_expander(data["stores"])

    handle_automator(data["future"])

    respond_to do |format|
      format.html { redirect_to cart_path(sm: 1) }
    end
  end

  def add_apple_music
    if params[:album_ids]
      params[:album_ids].each do |album_id|
        album = Album.find album_id
        album.update(apple_music: true) if album.person_id == current_user.id
      end
    end
    album_count     = params[:album_ids].count
    release_text    = custom_t("controllers.store_manager.release", count: album_count)
    pronoun_text    = custom_t("controllers.store_manager.pronoun", count: album_count)
    flash[:success] = custom_t("controllers.store_manager.available_in_weeks", album_count: album_count, release_text: release_text, pronoun_text: pronoun_text)
    redirect_to store_manager_path
  end

  def set_gon_all_stores
    gon.push(all_stores: custom_t("controllers.store_manager.all_stores"))
  end

  private

  def handle_expander(stores)
    return unless stores.present? && disable_expander? == false

    Tunecore::StoreManager.create_store_purchases(current_user, stores)
  end

  def handle_automator(automator_param)
    return unless automator_param.present? && disable_automator? == false

    Tunecore::StoreManager.create_automator_purchases(current_user, automator_param)
  end

  def init_manager_vars
    manager                       = Tunecore::StoreManager.new(current_user)
    @album_hash                   = manager.album_hash
    @stores_subtotal              = manager.stores_subtotal
    @stores                       = manager.stores.sort_by { |s| s[:position] }
    @missing_total_count          = manager.missing_total_count
    @apple_music_eligible_albums  = manager.apple_music_eligible_albums
    @currency                     = manager.currency
    @automator_disabled           = disable_automator?
    @disable_expander             = disable_expander?
    @disable_checkout             = @automator_disabled && @disable_expander
    @hide_checkout                = @missing_total_count.zero? ||
                                    current_user.has_never_distributed?
    return unless feature_enabled?(:store_automator)

    @salepoint_subscriptions = manager.automator_hash

    nil
  end

  def initial_total
    result = 0
    result += @stores_subtotal unless disable_expander?
    result += (@salepoint_subscriptions[:price] *
              @salepoint_subscriptions[:missing_releases].length) if include_automator_in_total?

    result
  end

  def disable_expander?
    result = current_user.can_use_store_expander? == false
    gon.push(disable_expander: result)

    result
  end

  def disable_automator?
    result = feature_enabled?(:store_automator) == false ||
             current_user.can_do?(:store_automator) == false ||
             current_user.has_never_distributed?
    gon.push(disable_automator: result)

    result
  end

  def disable_manager?
    disable_expander? && disable_automator?
  end

  def uncheck_all?
    @uncheck_all ||=
      begin
        result = MAXIMUM_AUTO_SELECTED_TOTAL_COST <= @initial_total
        gon.push(uncheck_all: result)

        result
      end
  end

  def uncheck_stores?
    disable_expander? || uncheck_all?
  end

  def include_automator_in_total?
    @salepoint_subscriptions.present? && !@automator_disabled
  end
end
