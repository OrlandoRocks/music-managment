class ItunesLinksController < ApplicationController
  def show
    @page_title = custom_t("controllers.itunes_links.apple_phobos_link")
    @album = current_user.albums.find(params[:id])

    render layout: false
  end
end
