class ApplicationController < ActionController::Base
  include OauthPluginSupport
  protect_from_forgery with: :exception, prepend: true

  include ActionView::Helpers::TextHelper
  include AccountSystem
  include Memorylogic
  include PageTitleGenerator
  include ActionFilters
  include ActionLogger
  include VerificationSystem
  include ReferrerService
  include OptimizelyService
  include CustomTranslationHelper
  include ControllerModule::MaintenanceAlert
  include TwoFactorAuthHelper
  include CountryDomainHelper
  include PayoutHelper
  include TrackMonetizationsHelper
  include SfaHelper
  include SsoCookies
  include AlbumLoadable
  include PersonFlags
  include SignInCallback
  include FeatureFlag

  rescue_from ActionController::InvalidAuthenticityToken, with: :handle_invalid_csrf_token

  helper :all
  helper_method :logged_in?
  helper_method :feature_enabled?
  helper_method :current_user
  helper_method :under_admin_control?
  helper_method :current_admin
  helper_method :is_verified?
  helper_method :header_partial, :footer_partial
  helper_method :clean_up_tr_subscription_invoice
  helper_method :enable_bigbox_uploader?
  helper_method :country_website
  helper_method :country_specific_domain
  helper_method :cms_enabled?
  helper_method :current_user_is_taken_over?
  helper_method :reskin?

  layout "application_old"

  before_action :set_locale
  before_action :compute_user_language, if: :cookie_language_setter_enabled?
  before_action :handle_v2_login, except: [:login, :logout]
  before_action :login_required

  append_before_action :redirect_sync_users
  append_before_action :redirect_ytm_approvers

  before_action :alert_rejections
  before_action :maintenance_alert
  before_action :set_gon_variables
  before_action :set_gon_two_factor_auth_prompt, if: :should_show_tfa_prompt?
  before_action :force_pw_reset
  before_action :redirect_stem_users
  before_action :set_paper_trail_whodunnit
  before_action :fetch_language_selector_options, if: proc { current_user&.can_use_language_selector? }
  before_action :current_locale_language, if: proc { current_user&.can_use_language_selector? }
  before_action :country_website_language
  before_action :set_dark_mode

  around_action :handle_in_context_editor

  def alert_rejections
    return unless logged_in?

    @rejected_albums ||= preloaded_untaken_down_albums
                         .includes(:locked_by)
                         .select(&:rejected?)
                         .sort_by { |album| -album[:id] }
                         .first(10)

    release = custom_t("controllers.application.release").pluralize(@rejected_albums.size)
    sentence = @rejected_albums.map { |album| "&ldquo;" + album.name + "&rdquo;" }.to_sentence

    return if @rejected_albums.blank?

    flash.now[:persistent_alert] = custom_t("controllers.application.problem_with_release_msg", release: release, release_lower: release.downcase, sentence: sentence)
  end

  def redirect_sync_users
    return unless (feature_enabled?(:sync_licensing) && !current_user.is_administrator) && !(self.class.name =~ /^Sync/)

    redirect_to sync_root_path
  end

  def redirect_ytm_approvers
    unless current_user && !current_user.is_administrator &&
           current_user.has_role?("YTM Approver", false) &&
           (self.class.name != "Ytm::SalepointSongsController")

      return
    end

    redirect_to ytm_salepoint_songs_path
  end

  #
  # Instance Methods
  #
  def check_role
    logger.error "needs_role: " + @needs_role.to_s
    redirect_to "/" and return false if @needs_role.nil?

    role = Role.find_by(name: @needs_role)
    if current_user.roles.include?(role)
      nil
    else
      flash[:error] = "You are not #{t(:authoriz)}ed for this action"
      redirect_to "/" and return false
    end
  end

  def check_if_2fa_enabled
    return if (two_factor_auth(current_user).try(:active?) || exempt_current_user_from_2fa?)

    flash[:error] = "As a TuneCore Admin user, you must have 2FA enabled on your account"
    redirect_to two_factor_auth_enrollments_path
  end

  def force_admin_2fa?
    ENV["FORCE_ADMIN_2FA"] == "true"
  end

  def reskin?
    return @_reskin if defined?(@_reskin)

    @_reskin = FeatureFlipper.show_feature?(:reskin, current_user)
  end

  protected

  def set_dark_mode
    @dark_mode = FeatureFlipper.show_feature?(:dark_mode, current_user)
  end

  def controller_action
    "#{controller_name}##{action_name}"
  end

  # default load_person
  def load_person
    return false unless logged_in?

    @person ||= current_user
  end

  def load_album
    @album = @person.albums.find(params[:album_id])
  end

  # redirect if person's country is not from the US or its territories
  def restrict_based_on_united_states_and_territories
    return if current_user.from_united_states_and_territories?

    flash[:notice] = custom_t("controllers.application.na_outside_us_territories_msg")
    redirect_to dashboard_path
  end

  def remove_sso_cookie
    return unless current_user

    cookies[:tc_pid] = nil
    logger.info "Removed sso cookie for user #{current_user.id}"
  end

  def generate_sso_cookie_value(person_id)
    return if Person.find(person_id).blank?

    hmac = generate_hmac_sha1(person_id.to_s)
    "#{person_id}-#{hmac}"
  end

  def add_takeover_cookie
    cookies[:tc_active_takeover] = { value: "true", domain: COOKIE_DOMAIN }
    logger.info "Set takeover cookie: person_id=#{current_user.id}"
  end

  def remove_takeover_cookie
    return unless current_user

    cookies.delete(:tc_active_takeover, domain: COOKIE_DOMAIN)
    logger.info "Removed takeover cookie for user #{current_user.id}"
  end

  def redirect_to_tc_social?
    session["client_application"] == "tc_social"
  end

  def redirect_if_not_permitted
    return if current_user.is_permitted_to?(params[:controller], params[:action])

    redirect_to({ controller: "admin/rights", action: :not_permitted })
  end

  def redirect_if_not_permitted_lightbox
    return if current_user.is_permitted_to?(params[:controller], params[:action])

    redirect_to({ controller: "admin/rights", action: :not_permitted, layout: "application_lightbox" })
  end

  def redirect_if_has_pending_payout
    redirect_to(controller: "my_account", action: "withdraw") if current_user.has_pending_payout?
  end

  def store_session_based_locale(locale_code = "en")
    session[:locale] = locale_code
  end

  def enable_bigbox_uploader?
    return unless logged_in?

    logger.debug("User #{current_user.email} referral='#{current_user.referral}'")
    logger.debug("returning false")
    true
  end

  def reset_paypal_session
    [:token, :payerid, :payment_action, :AMT, :ccode, :step3, :paypal_error, :remote_ip, :pp_invoice_id, :paypal_payin_config_id].each do |session_var|
      session[session_var] = nil
    end
  end

  def set_locale
    # FIXME: Remove temporary fixing of session's locale
    if params[:country_locale]
      session[:locale] = I18n.locale = COUNTRY_LOCALE_MAP[params[:country_locale].upcase]
    elsif params[:locale] # Uses new name for locale, so it doesn't affect existing code that reads this to find a country website.
      session[:locale] = I18n.locale = params[:locale]
    elsif session[:locale]
      I18n.locale = session[:locale]
    else
      I18n.locale = COUNTRY_LOCALE_MAP[country_website]
    end

    logger.info("Using locale '#{I18n.locale}' for user #{current_user ? current_user.email : 'not logged in'} on host #{request.host}")
    set_language_cookie(session[:locale]) if session[:locale] && cookie_language_setter_enabled?
  end

  def update_dictionaries
    if feature_enabled?(:update_dictionaries_on_staging) && (Rails.env.development? || Rails.env.staging? == "staging")
      stime = Time.now
      logger.info("Updating dictionaries from WTI")
      `wti pull` # requires the wti gem. will fail gracefully if the gem is not installed.
      if $?.exitstatus&.zero?
        logger.info("Updated dictionaries in #{Time.now - stime} secs")
      else
        logger.info("Error updating dictionaries (is wti installed?)")
      end
    else
      logger.info("Will not attempt to update dictionaries")
    end
  end

  # For use in a before_action
  def require_publishing_access
    if feature_enabled?(:publishing)
      true
    else
      redirect_to "/"
    end
  end

  # For use in a before_action
  def requires_publishing_manager_role
    if current_user.has_role?("Publishing Manager", false)
      true
    else
      redirect_to "/"
    end
  end

  # For use in a before_action
  def requires_publishing_viewer_role
    if current_user.has_role?("Publishing Manager", false) || current_user.has_role?("Publishing Viewer", false)
      true
    else
      redirect_to "/"
    end
  end

  def info_for_paper_trail
    { ip: request.remote_ip }
  end

  def is_mobile_agent?
    browser = Browser.new(request.env["HTTP_USER_AGENT"], accept_language: "en-us")
    browser.device.mobile? ? true : false
  end

  def set_gon_variables
    set_gon_locale
    set_gon_current_user
    set_gon_git_branch
  end

  def set_gon_locale
    gon.push(
      locale: I18n.locale.to_s[0..1],
      datepicker_locale: I18n.t(:datepicker_locale),
      currency_delimiter: I18n.t("number.format.delimiter"),
      currency_separator: I18n.t("number.format.separator"),
      currency: (current_user.currency if current_user),
      country: (current_user.country_domain if current_user),
      long_date_picker_format: I18n.t("date.formats.long_date_picker")
    )
  end

  def set_gon_current_user
    person_keys_blocklist = %w[password salt password_reset_tmsp]
    return unless current_user

    gon.push(
      current_user: {
        person: current_user.as_json["person"].reject { |k, _| person_keys_blocklist.include?(k) },
        cable_auth_token: cable_auth_token&.token,
        tc_accelerator: {
          opt: current_user.tc_accelerator_opted_in?,
          something_went_wrong: custom_t("something_went_wrong")
        }
      }
    )
  end

  def set_gon_git_branch
    return if Rails.env.production?

    gon.push(
      git_info: {
        branch: GIT_BRANCH,
        commit: GIT_COMMIT,
        commit_url: GIT_COMMIT_URL
      }
    )
  end

  def cms_enabled?
    TcWww::Application.config.cms_enabled
  end

  def cable_auth_token
    CableAuthService.refresh_token(current_user) if current_user.cable_auth_token.blank?

    current_user.cable_auth_token
  end

  def force_pw_reset
    return if session[:admin].present?
    return unless (current_user && request.path != "/reset_password/new")

    unless current_user.password_reset_tmsp.nil? && FeatureFlipper.show_feature?(:force_password_reset, current_user)
      return
    end

    redirect_to new_reset_password_path(force: true)
  end

  def forbidden
    render file: File.join(Rails.root, "public/403.html"), status: :unauthorized
  end

  def redirect_stem_users
    redirect_to dashboard_path if current_user && current_user.blocked_for_stem?
  end

  def handle_in_context_editor
    phrase_app_enabled = Rails.env.staging? && FeatureFlipper.show_feature?(:phrase_app)

    PhraseApp::InContextEditor.with_config(enabled: phrase_app_enabled) do
      yield
    end
  end

  def handle_invalid_csrf_token
    return if request.format.js? || request.format.json?

    flash[:error] = custom_t("invalid_request")
    redirect_back(fallback_location: dashboard_path)
  end

  def exempt_current_user_from_2fa?
    FeatureFlipper.show_feature?(:exempt_from_2fa, current_user)
  end

  def handle_v2_login
    return if @current_user.present?

    service = V2::Authentication::SharedLoginService.new(request)
    authenticated_person_id = service.person_id

    if authenticated_person_id.present?
      @current_user = Person.find(authenticated_person_id)
      @person = @current_user
      reroute_logged_in_user
    elsif service.redirect_to_v2?
      # Guards session fixation attack https://guides.rubyonrails.org/security.html#session-fixation
      request.reset_session

      redirect_to service.v2_url
    end
  end
end
