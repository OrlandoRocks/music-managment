class PersonPlansController < ApplicationController
  class PersonPlanControllerIndexPathError < StandardError; end

  include CartPlanable
  before_action :validate_plan_is_eligible, only: [:create]
  before_action :prepare_cart, only: [:create]

  def create
    redirect_to dashboard_path and return unless FeatureFlipper.show_feature?(:plans_pricing, current_user)

    PlanService.adjust_cart_plan!(current_user, params[:plan_id])
    redirect_to cart_path
  end

  def index
    # users is somehow navigating to this action
    # unable to currently identify how this path is being reached
    # log and redirect them back to cart
    Airbrake.notify(PersonPlanControllerIndexPathError, params.merge({ referrer: request.referrer }))
    redirect_to cart_path
  end

  private

  def prepare_cart
    clear_cart unless current_user.has_plan?
  end

  def delete_improper_plans
    existing_plan = PersonPlan.find_by(person: current_user)
    return if existing_plan.blank?

    # redundant if we're already putting other cart validations
    cart_item = Purchase.unpaid.find_by(related: existing_plan)
    return if cart_item.blank?

    cart_item.delete
    existing_plan.delete
  end
end
