# frozen_string_literal: true

class TcAcceleratorsController < ApplicationController
  def opt
    person = Person.find(tc_accelerator_params[:person_id])
    opt = tc_accelerator_params[:opt].to_sym
    TcAcceleratorService.opt!(person, opt)

    render json: { person_id: person.id, opted_in: person.tc_accelerator_opted_in?, success: person.errors.blank? }
  end

  def do_not_notify
    person = Person.find(tc_accelerator_params[:person_id])
    TcAcceleratorService.do_not_notify!(person, nil)

    render json: {
      person_id: person.id,
      do_not_notify: person.tc_accelerator_do_not_notify?,
      success: person.errors.blank?
    }
  end

  private

  def tc_accelerator_params
    params.require(:tc_accelerator).permit(:person_id, :opt)
  end
end
