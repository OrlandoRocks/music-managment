# frozen_string_literal: true

class PurchasesController < ApplicationController
  include AutomatorManager
  include CartPlanable

  before_action :load_person
  before_action :load_purchase,
                except: [
                  :remove_all_for_store,
                  :remove_all_salepoint_subscriptions,
                  :remove_all_for_expander
                ]
  before_action :abort, if: :plan_renewal_purchase?, only: [:destroy]

  def destroy
    @purchase.destroy
    manage_automator_state if @purchase.store_automator?
    clear_cart if no_active_plan_deleting_plan?(@purchase)
    reapply_credit_usage_to_releases if @purchase.credited?
    ImmersiveAudio::RemovalService.new(immersive_audio_id: @purchase.related_id).call if @purchase.immersive_audio?

    respond_to do |format|
      format.html { redirect_to cart_path }
      format.js { head :ok }
    end
  end

  def remove_code
    @purchase.cert.disassociate
    @purchase.reload
    @purchase.product.set_targeted_product(
      TargetedProduct.targeted_product_for_active_offer(@purchase.person, @purchase.product)
    )
    @purchase.recalculate
    redirect_to cart_path
  end

  def remove_all_for_store
    if params[:store_id]
      store = Album.stores.is_active.find(params[:store_id])
      purchases = @person.purchases
                         .unpaid
                         .not_in_invoice
                         .joins(
                           "INNER JOIN salepoints ON purchases.related_id = salepoints.id " \
                           "AND purchases.related_type = 'Salepoint'"
                         )
                         .where(salepoints: { store_id: store.id })

      Purchase.where(id: purchases.pluck(:id)).destroy_all
    end

    respond_to do |format|
      format.html { redirect_to cart_path }
      format.js { head :ok }
    end
  end

  def remove_all_salepoint_subscriptions
    purchases = @person.purchases
                       .unpaid
                       .not_in_invoice
                       .joins(
                         "INNER JOIN salepoint_subscriptions sp ON purchases.related_id = sp.id " \
                         "AND purchases.related_type = 'SalepointSubscription'"
                       )

    ids = purchases.reject do |purchase|
      Purchase.currently_in_cart?(@person, purchase.related.album)
    end.map(&:id)

    Purchase.where(id: ids).destroy_all

    respond_to do |format|
      format.html { redirect_to cart_path }
      format.js { head :ok }
    end
  end

  def remove_all_for_expander
    salepoints = @person.purchases
                        .unpaid
                        .not_in_invoice
                        .joins(
                          "INNER JOIN salepoints sp ON purchases.related_id = sp.id AND " \
                          "purchases.related_type = 'Salepoint'"
                        )

    salepoints.destroy_all

    respond_to do |format|
      format.html { redirect_to cart_path }
      format.js { head :ok }
    end
  end

  def abort
    flash[:error_dialog] = custom_t(:something_went_wrong)
    redirect_to cart_path and return
  end

  private

  def load_purchase
    @purchase = current_user.purchases.find(params[:id])
  end

  def plan_renewal_purchase?
    @purchase.present? && (@purchase.plan_renewal? || failed_additional_artist_purchase?)
  end

  def failed_additional_artist_purchase?
    @purchase.additional_artist? && @purchase.failed?
  end
end
