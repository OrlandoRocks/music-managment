class Api::TcSocialController < Api::BaseController
  before_action :verify_token
  before_action :load_person
  before_action :load_plan_status
  skip_before_action :verify_api_key

  def index
    @data = {
      "tc_user_id" => @verifier.user_id,
      "tc_first_name" => @person.first_name,
      "tc_last_name" => @person.last_name,
      "tc_email" => @person.email,
      "tc_plan" => @plan_status.plan,
      "tc_plan_expires_at" => @plan_status.plan_expires_at.to_s,
      "tc_grace_period_ends_on" => @plan_status.grace_period_ends_on.to_s,
      "tc_payment_channel" => @person.subscription_payment_channel_for("Social"),
      "tc_country_website" => @person.country_domain
    }

    render json: @data
  end

  private

  def verify_token
    @verifier = Oauth2Token.where(token: params[:token]).first
    head 401 unless @verifier
  end

  def load_person
    @person = Person.find(@verifier.user_id)
  rescue ActiveRecord::RecordNotFound => e
    render json: { is_error: 1, message: "User #{@verifier.user_id} not found in TC" }
  end

  def load_plan_status
    @plan_status = ::Social::PlanStatus.for(@person)
  end
end
