class Api::SipController < Api::BaseController
  before_action :validate_vat_flipper, only: [:generate_invoice]

  def mapped
    sip_store_ids = []
    sip_stores    = []

    if params[:store_ids].present?
      sip_store_ids = params[:store_ids].split(",")
      sip_stores = SipStore.where("id in (?)", sip_store_ids)
    end

    render json: { mapped: sip_stores.count == sip_store_ids.count }
  end

  def vat_posting
    vat_posting = SipVatProcessor.new(
      params[:person_id],
      params[:sales_record_master_ids],
      params[:you_tube_royalty_record_ids]
    )
    vat_posting.process!
    render json: {
      status: vat_posting.status,
      person_id: params[:person_id],
      message: vat_posting.message
    },
           status: vat_posting.status
  end

  def generate_invoice
    job_id = SipOutboundInvoiceWorker.perform_async(
      params[:posting_id]
    )
    render json: {
      status: :ok,
      job_id: job_id,
      message: "Successfully initiated invoice generation"
    },
           status: :ok
  end

  def posting_complete
    # NOTE: Disabling this as part of CF-684
    # Because summarization is done at point of withdrawal at a person level
    # This summarization can be considered redundant

    # if FeatureFlipper.show_feature?(:advanced_tax_blocking)
    #   TaxBlocking::BatchPersonEarningsSummarizationWorker.perform_async(posting_ids: posting_ids)
    # end
    TaxWithholdings::SipBatchWorker.perform_async if FeatureFlipper.show_feature?(:us_tax_withholding)

    render json: { status: :ok }
  end

  def posting_finalize
    if Royalties::Posting.get_id.present?
      render json: { message: "Posting already in progress." }, status: :unprocessable_entity
    else
      posting_id = SecureRandom.uuid

      Royalties::Posting.add(posting_id, params[:posting_ids])
      Royalties::BatchCreationWorker.perform_async
      render json: { status: :ok }
    end
  end

  private

  def validate_vat_flipper
    return if FeatureFlipper.show_feature?(:vat_tax)

    Tunecore::Airbrake.notify(
      "SIP Invoice Posting failure - "\
            "VAT Tax feature is currently disabled - Posting ID - #{params[:posting_id]}"
    )
    render json: {
      status: :ok,
      job_id: nil,
      message: "VAT Tax feature is currently disabled"
    },
           status: :ok
  end
end
