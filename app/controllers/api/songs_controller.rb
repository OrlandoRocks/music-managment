class Api::SongsController < Api::BaseController
  def create
    album_id = song_params[:album_id]
    successful_tracks = []
    song_params[:tracks].each do |song_param|
      asset = song_param.delete(:asset)

      @song = Song.new(song_param.merge!(album_id: album_id))
      @creatives = @song.creatives

      next unless @song.save

      @song.update_attribute(:track_num, song_param[:track_num]) if song_param[:track_num].present?

      if asset.present?
        asset_bucket = asset[:bucket].presence || "tunecore.mass-ingestion"
        @song.create_s3_orig_asset(bucket: asset_bucket, key: asset[:key])
      end

      Note.create(
        related_id: album_id,
        related_type: "Album",
        note_created_by_id: 0,
        ip_address: request.remote_ip,
        subject: "Song created",
        note: "Song '#{@song.name}' (ID: #{@song.id}) created on Album by Batch Ingestion"
      )

      @song.send_to_bigbox(@song.album.person_id) if @song.s3_orig_asset
      successful_tracks << @song
    end

    if song_params[:tracks].count == successful_tracks.count
      render json: successful_tracks, status: :created, serializer: nil
    else
      render json: { message: "Failed to create the tracks due to #{@song.errors.full_messages}" },
             status: :unprocessable_entity
    end
  end

  private

  def song_params
    params.permit(
      :album_id,
      :song,
      :api_key,
      tracks: [
        :name,
        :optional_isrc,
        :parental_advisory,
        :clean_version,
        :track_num,
        {
          asset: [
            :key,
            :bucket
          ],
          creatives: [
            :role,
            :name
          ]
        }
      ]
    )
  end

  def verify_params
    head 400 unless params.has_key?("isrc")
  end
end
