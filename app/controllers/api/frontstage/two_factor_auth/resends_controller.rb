class Api::Frontstage::TwoFactorAuth::ResendsController < ActionController::Base
  include Api::Frontstage::Authenticatable

  before_action :fetch_attempts_and_successes, only: [:create]

  def create
    alternate_method = params[:alternate_method].present?
    request_method = @person.tfa_method(alternate_method)

    if TwoFactorAuth::FraudPrevention.request_banned?(@person.id, request_method)
      render json: { banned: true }
      return
    end

    if below_limit_or_recently_verified? && minute_since_last_resend?
      if FeatureFlipper.show_feature?(:verify_2fa, @person)
        TwoFactorAuth::ApiClient.new(@person).request_authorization(alternate_method)
      else
        TwoFactorAuth::AuthyApiClient.new(@person).request_authorization(alternate_method)
      end

      TwoFactorAuthEvent.create(
        type: "authentication",
        page: "account_settings",
        action: "frontstage_code_resend",
        successful: true,
        two_factor_auth_id: @person.two_factor_auth.id
      )
    else
      render json: { exceeds_resends_per_code: true }
      return
    end

    respond_to do |format|
      format.html { head :ok }
      format.js { head :ok }
    end
  end

  def fetch_attempts_and_successes
    @recent_resend_attempts = TwoFactorAuthEvent.joins(:two_factor_auth).where(action: "frontstage_code_resend", created_at: 10.minutes.ago..Time.now, two_factor_auths: { person_id: @person.id })
    @recent_auth_successes = TwoFactorAuthEvent.joins(:two_factor_auth).where(action: "authentication", successful: true, created_at: 10.minutes.ago..Time.now, two_factor_auths: { person_id: @person.id })
  end

  def below_limit_or_recently_verified?
    return true if @recent_resend_attempts.length < 4

    if @recent_auth_successes.present?
      return @recent_auth_successes.last.created_at > @recent_resend_attempts[-4].created_at
    end

    false
  end

  def minute_since_last_resend?
    return @recent_resend_attempts.last.created_at < 1.minute.ago if @recent_resend_attempts.present?

    true
  end
end
