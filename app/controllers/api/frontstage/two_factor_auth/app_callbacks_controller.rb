class Api::Frontstage::TwoFactorAuth::AppCallbacksController < ActionController::Base
  include Api::Frontstage::Authenticatable

  before_action :load_tfa, only: [:show]

  def show
    if @tfa && @tfa.within_authentication_window?
      render json: { status: "successful" }
    else
      render json: { status: "unsuccessful" }
    end
  end

  private

  def load_tfa
    @tfa ||= TwoFactorAuth.find_by(person_id: @person.id)
  end
end
