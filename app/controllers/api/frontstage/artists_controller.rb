class Api::Frontstage::ArtistsController < ActionController::Base
  include OauthPluginSupport
  include ArelTableMethods
  include AccountSystem
  protect_from_forgery prepend: true
  oauthenticate strategies: :token, interactive: false

  def show
    render json: { status: "success", artists: serialized_artists }
  rescue
    render json: { status: "failure" }
  end

  private

  def serialized_artists
    ActiveModel::ArraySerializer.new(
      Album::CreativeQueryBuilder.build(current_user),
      each_serializer: Api::Frontstage::CreativeSerializer
    )
  end
end
