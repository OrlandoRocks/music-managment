class Api::Frontstage::AccountsController < ActionController::Base
  include OauthPluginSupport
  include AccountSystem
  protect_from_forgery prepend: true
  oauthenticate strategies: :token, interactive: false

  def show
    render json: { status: "success", email: current_user.try(:email), name: current_user.try(:name) }
  end
end
