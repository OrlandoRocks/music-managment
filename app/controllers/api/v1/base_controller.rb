class Api::V1::BaseController < ActionController::Base
  protect_from_forgery prepend: true
  before_action :authenticate

  protected

  def authenticate
    render json: { message: "Unauthorized Api Key" }, status: :unprocessable_entity unless valid_request
  end

  def current_api_key
    return unless params[:key]

    digested_key = Digest::SHA2.hexdigest(SECRETS["api_v1_secret_key"] + params[:key])
    @current_api_key ||= ApiKey.find_by(key: digested_key)
  end

  private

  def valid_request
    current_api_key.present?
  end
end
