class Api::V1::AffiliateReportsController < Api::V1::BaseController
  before_action :validate_content_type

  def create
    affiliate_report = AffiliateReport.new(
      affiliate_report_params.merge(api_key_id: current_api_key.id)
    )

    if affiliate_report.save
      head :created
    else
      render json: { errors: affiliate_report.errors }, status: :unprocessable_entity
    end
  end

  private

  # RAILS_5_UPGRADE: when at 5.1.2+, replace the tap below with:
  #  params.require(:affiliate_report).permit(..., :files => {})
  def affiliate_report_params
    params.require(:affiliate_report).permit(
      :date_paid,
      :person_email,
      :product_name,
      :sale_amount,
      :sale_currency,
      :units_sold,
      :person_id,
      :api_key_id
    ).tap do |whitelisted|
      whitelisted[:data] = params[:affiliate_report][:data].permit!
    end
  end

  def validate_content_type
    head 422 unless request.headers["CONTENT_TYPE"] == "application/json"
  end
end
