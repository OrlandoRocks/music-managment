class Api::V1::BraintreeController < ActionController::Base
  skip_before_action :verify_authenticity_token

  # TODO remove this action in tandem with updating the braintree account portal in production you old so and so
  # the update should point the webhook at tc_account_updater_report instead, which is a duplicate of this action
  def account_updater_report
    corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
    config = PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: corporate_entity
    )
    Braintree::MultiGatewayAccountUpdaterService.account_updater_report(config, request)

    head :ok
  end

  def tc_account_updater_report
    corporate_entity = CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
    config = PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: corporate_entity
    )
    Braintree::MultiGatewayAccountUpdaterService.account_updater_report(config, request)

    head :ok
  end

  def bi_account_updater_report
    corporate_entity = CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
    config = PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: corporate_entity
    )
    Braintree::MultiGatewayAccountUpdaterService.account_updater_report(config, request)

    head :ok
  end
end
