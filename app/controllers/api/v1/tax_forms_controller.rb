# frozen_string_literal: true

class Api::V1::TaxFormsController < Api::V1::BaseController
  before_action :load_person, only: [:create]

  def create
    render json: { status: :not_found, message: "not found" } and return if @person.blank?

    # TaxFormType is returned as a numeric value. Expected values: 1, 2, 3, 4, 5, 6, 7
    tax_form_type_id = Integer(create_params[:TaxFormType], 10, exception: false)
    if TaxFormType.ids.exclude?(tax_form_type_id)
      error_message = "Unknown TaxFormType(#{create_params[:TaxFormType]}) received from TaxformSubmitted webhook"
      Airbrake.notify(error_message, { person_id: @person.id, TaxFormType: create_params[:TaxFormType] })
      render json: { status: :unprocessable_entity, message: error_message } and return
    end

    FetchTaxFormsWorker.perform_async(@person.id)
    render json: { status: :created, message: "Tax form created" }
  end

  private

  def load_person
    @person ||= PayoutProvider.find_by(
      client_payee_id: create_params.fetch(:payeeid, create_params[:apuid])
    )&.person
  end

  def create_params
    params.permit(:payeeid, :TaxFormType, :apuid)
  end
end
