class Api::V1::PayoutProvidersController < Api::V1::BaseController
  before_action :load_payout_provider

  RESPONSES = {
    approved: { status: :success, message: PayoutProvider::APPROVED }.freeze,
    not_approved: { status: :failure, message: "Not " + PayoutProvider::APPROVED }.freeze,
    declined: { status: :success, message: PayoutProvider::DECLINED }.freeze,
    not_declined: { status: :failure, message: "Not " + PayoutProvider::DECLINED }.freeze,
    not_found: { status: :failure, message: "not found".freeze }.freeze
  }.freeze

  def approved
    if @payout_provider.update(payout_provider_approval_params)
      verify_address_for(
        person: @payout_provider.person,
        country_audit_source: :payoneer_kyc
      )

      render json: RESPONSES[:approved]
    else
      render json: RESPONSES[:not_approved]
    end
  end

  def declined
    if @payout_provider.update(payout_provider_declined_params)
      render json: RESPONSES[:declined]
    else
      render json: RESPONSES[:not_declined]
    end
  end

  def released
    if @payout_provider.update(payout_provider_pending_params)
      render json: { status: :ok, message: "ok" }
    else
      render json: { status: :unprocessable_entity, message: "error updating status" }
    end
  end

  def payee_details_changed
    verify_address_for(
      person: @payout_provider.person,
      country_audit_source: :payoneer_post_kyc
    )

    render json: { status: :ok, message: "ok" }
  end

  private

  def verify_address_for(person:, country_audit_source:)
    return unless FeatureFlipper.show_feature?(:bi_transfer, @payout_provider.person)

    PayoutProvider::VerifyPayeeComplianceInfo
      .new(person, country_audit_source: country_audit_source)
      .call
  end

  def payout_provider_approval_params
    {
      provider_processed_at: Time.now,
      provider_status: PayoutProvider::APPROVED,
      provider_identifier: params[:provider_identifier],
      active_provider: true
    }
  end

  def payout_provider_declined_params
    {
      provider_processed_at: Time.now,
      provider_status: PayoutProvider::DECLINED,
      provider_identifier: params[:provider_identifier],
      active_provider: false
    }
  end

  def payout_provider_pending_params
    {
      provider_processed_at: Time.now,
      provider_status: PayoutProvider::PENDING
    }
  end

  def load_payout_provider
    @payout_provider ||=
      PayoutProvider.find_by_client_payee_id!(params[:client_payee_id])
  rescue ActiveRecord::RecordNotFound
    render json: RESPONSES[:not_found], root: false
  end
end
