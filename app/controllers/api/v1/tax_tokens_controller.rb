class Api::V1::TaxTokensController < Api::V1::BaseController
  def update
    person = Person.find(params[:apuid])
    person.tax_tokens.last.update(tax_form_type: params[:TaxFormType])
    head :ok
  end
end
