class Api::V1::DocusignController < ActionController::Base
  skip_before_action :verify_authenticity_token, raise: false

  def update_document_status
    begin
      legal_document.update(signed_at: lod_signed_at, status: envelope_status)
    rescue => e
      Airbrake.notify(e, { legal_document: legal_document })
    ensure
      head :ok
    end
  end

  private

  def legal_document
    LegalDocument.find_by(provider_identifier: envelope_id)
  end

  def envelope_id
    payload["DocuSignEnvelopeInformation"]["EnvelopeStatus"]["EnvelopeID"]
  end

  def lod_signed_at
    payload["DocuSignEnvelopeInformation"]["EnvelopeStatus"]["RecipientStatuses"]["RecipientStatus"]["Signed"]
  end

  def envelope_status
    payload["DocuSignEnvelopeInformation"]["EnvelopeStatus"]["Status"].downcase
  end

  def payload
    @payload ||= Hash.from_xml(request.raw_post)
  end
end
