class Api::V1::TwoFactorAuthsController < Api::V1::BaseController
  before_action :load_two_factor_auth

  def create
    head :ok and return if @tfa.blank?

    (params[:status] == "approved") ? @tfa.auth_success! : @tfa.auth_failure!
    head :ok
  end

  def load_two_factor_auth
    @tfa = TwoFactorAuth.find_by(authy_id: params[:authy_id])
  end
end
