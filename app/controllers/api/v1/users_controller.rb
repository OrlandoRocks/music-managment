class Api::V1::UsersController < Api::V1::BaseController
  def index
    verifier = EmailVerifier.new(params[:email])
    if verifier.verify!
      render json: verifier
    else
      head 404
    end
  end
end
