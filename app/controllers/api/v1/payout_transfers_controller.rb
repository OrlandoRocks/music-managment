class Api::V1::PayoutTransfersController < Api::V1::BaseController
  skip_forgery_protection
  before_action :load_payout_transfer

  def pending
    update_provider_status_and_render(PayoutTransfer::PENDING)
  end

  def completed
    if v4_enabled? && params.key?(:amount)
      Payoneer::PayoutTransferAmountVerificationService.verify(
        payout_transfer: @payout_transfer,
        amount: params[:amount],
        referrer_url: request.url
      )
    end

    update_provider_status_and_render(PayoutTransfer::COMPLETED)
  end

  def cancelled
    if [PayoutTransfer::CANCELLED, PayoutTransfer::DECLINED].include?(@payout_transfer&.provider_status)
      msg = "Payoneer IPCN call failed: payment #{params[:payout_id]} already marked as cancelled/declined"
      render status: :bad_request, json: { message: msg }
    else
      update_provider_status_and_render(PayoutTransfer::CANCELLED)
      cancel_transfer
    end
  end

  private

  def cancel_transfer
    return if @payout_transfer.blank?

    PayoutTransfer::Refund.refund(payout_transfer: @payout_transfer)

    @payout_transfer.update(
      code: params.require("ReasonCode"),
      description: params.require("ReasonDescription")
    )
  end

  def update_provider_status_and_render(status)
    response_key = @payout_transfer.mark_provider_as(status) ? status : :not_modified
    render status: :ok, json: { message: "payout transfer marked #{response_key}" }
  end

  def load_payout_transfer
    payout_id = params.require(:payout_id)
    @payout_transfer = PayoutTransfer.find_by!(client_reference_id: payout_id)
  rescue ActiveRecord::RecordNotFound
    msg = "Payoneer IPCN call failed: Not able to find the payout transfer by identifier #{params[:payout_id]}"
    Tunecore::Airbrake.notify(msg)
    render status: :unprocessable_entity, json: { message: msg }
  end

  def v4_enabled?
    FeatureFlipper.show_feature?(:use_payoneer_v4_api, @payout_transfer.person)
  end
end
