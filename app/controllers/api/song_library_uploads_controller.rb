class Api::SongLibraryUploadsController < ApplicationController
  def index
    params[:per_page] ||= 10
    songs = current_user.song_library_uploads.paginate(page: params[:page], per_page: params[:per_page])
    render partial: "/soundout_reports/song_library_uploads", locals: { soundout_songs: songs }
  end
end
