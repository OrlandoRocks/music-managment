class Api::DistributionsController < Api::BaseController
  before_action :find_distribution, only: [:show_state, :update_state]
  before_action :check_distribution_state, only: [:update_state]

  def ping
    render json: { result: "success", message: "pong" }
  end

  def show_state
    render json: success_json
  end

  def update_state
    if @distribution
      Distribution::StateUpdateService.update(@distribution, params)
      render json: success_json
    else
      render json: fail_json
    end
  end

  private

  def find_distribution
    @distribution =
      if params[:album_id] && params[:store_id]
        Distribution.find_by_store_and_album(params[:store_id], params[:album_id])
      elsif params[:store_name] && params[:upc]
        Distribution.find_by_store_name_and_upc(params[:store_name], params[:upc])
      else
        Distribution.find(params[:id])
      end
  rescue ActiveRecord::RecordNotFound => e
    render json: fail_json
  end

  def check_distribution_state
    return if Distribution.valid_state?(params[:state])

    render json: { status: "error", reason: "Please provide a valid distribution state" }
  end

  def success_json
    { status: "success", id: @distribution.id, state: @distribution.state }
  end

  def fail_json
    { status: "error", reason: "Please provide a valid distribution id or store and album ids or upc and store name" }
  end
end
