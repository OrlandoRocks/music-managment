# frozen_string_literal: true

class Api::TranslationServiceController < ActionController::API
  def phrase_webhook
    hermes_base_url = ENV.fetch("HERMES_BASE_URL")
    Faraday.post do |req|
      req.url "#{hermes_base_url}/translations/phrase/event"
      req.headers["Content-Type"] = "application/json"
      req.body = request.raw_post
    end
    head :ok
  rescue Faraday::Error => e
    Airbrake.notify("Error connecting to Translation service: #{e.message}")
    head :ok
  end
end
