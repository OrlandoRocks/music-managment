class Api::CertController < Api::OauthBaseController
  before_action :load_person
  before_action :load_purchase

  def create
    if cert_verifies?
      render json: { status: "success", message: ["Applied"], cert: @person.certs.last }
    else
      @cert = Cert.new
      render json: { status: "failure", message: ["Cert not Applied"] }
    end
  end

  def remove
    if @purchase.cert.nil?
      render json: { status: "failure", message: ["Cert not available"] }
    else
      cert_removed_purchase = Api::CertService.new(@purchase).remove_cert
      if cert_removed_purchase.cert.present?
        render json: { status: "failure", message: ["Cert could not be removed"] }
      else
        render json: { status: "success", message: ["Cert Removed"], data: cert_removed_purchase }
      end
    end
  end

  protected

  def load_person
    @person = user
    render json: { status: "failure", message: ["User #{params[:email]} not found in TC"] } and return if @person.nil?
  end

  def load_purchase
    @purchase =
      if @person.is_administrator
        Purchase.find_by(id: params[:purchase_id])
      else
        @person.purchases.find_by(id: params[:purchase_id])
      end
    render json: { status: "failure", message: ["Could not Enter Code"] } unless @purchase or @purchase.allow_new_cert?
  end

  def cert_verifies?
    opts = {
      entered_code: params[:cert][:cert],
      purchase: @purchase
    }
    @verify_result = Cert.verify(opts)
    if @verify_result.is_a? Cert
      @cert = @verify_result
      true
    else
      false
    end
  end
end
