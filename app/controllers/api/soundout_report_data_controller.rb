class Api::SoundoutReportDataController < ApplicationController
  #
  # Returns all data except reviews in json format
  #
  # URL:
  #  /api/soundout_report_data/:id
  #
  # Required Params
  #  params[:id]  = id of soundout_report/soundout_report_data
  #
  def show
    @data = SoundoutReportData.joins(:soundout_report).where("person_id = ?", current_user.id).find(params[:id])

    begin
      @json = JSON.parse(@data.report_data)

      respond_to do |format|
        format.json { render json: { data: @json["data"] }.to_json }
      end
    rescue JSON::ParserError
      respond_to do |format|
        format.json { head :internal_server_error }
      end
    end
  end

  #
  # Searches/filters and pages reviews
  #
  # URL:
  #  /api/soundout_report_data/:id/reviews
  #
  # Required Params
  #  params[:id]         = id of soundout_report/soundout_report_data
  #
  # Optional Params
  #  params[:page]       = page to show
  #  params[:pagesize]   = pagesize to show
  #  params[:rating_gte] = rating greater than or equal to
  #  params[:rating_lte] = rating less than or equal to
  #  params[:age_lte]    = age less than or equal to
  #  params[:age_gte]    = age greater than or equal to
  #  params[:gender]     = gender ( 1 = Male, 2 = Female, All | blank = everything )
  #  params[:search]     = string search
  #  params[:sort]       = 'Rating ASC', 'Rating DESC', 'Age ASC', 'Gender ASC', 'Gender DESC'
  #
  def reviews
    params[:page]     ||= 1
    params[:per_page] ||= 50

    @data = SoundoutReportData.joins(:soundout_report).where("person_id = ?", current_user.id).find(params[:id])

    begin
      @reviews, total_count = @data.search_and_filter_reviews(params)

      respond_to do |format|
        format.json {
          render json: {
            reviews: @reviews,
            per_page: params[:per_page],
            rating_gte: params[:rating_gte],
            rating_lte: params[:rating_lte],
            age_lte: params[:age_lte],
            age_gte: params[:age_gte],
            gender: params[:gender],
            search: params[:search],
            order: params[:order],
            total_count: total_count,
            current_page: params[:page],
            total_pages: (
              total_count / params[:per_page].to_i + ((total_count % params[:per_page].to_i).positive? ? 1 : 0)
            )
          }.to_json
        }
      end
    rescue JSON::ParserError
      respond_to do |format|
        format.json { head :internal_server_error }
      end
    end
  end
end
