class Api::CartsController < Api::OauthBaseController
  before_action :load_person
  before_action :load_cart_finalize, :load_rf_purchase

  RF_VIDEO_DISPLAY_NAME = "rf_video_download".freeze

  def finalize
    if @finalize.save
      SuccessfulPurchaseWorker.perform_async(@finalize.invoice.id)
      handle_rf_purchase if @rf_purchase
      render json: { status: "success", data: @finalize.invoice }
    else
      api_message = fetch_message_key
      render json: { status: "failure", message: api_message }
    end
  end

  protected

  def fetch_message_key
    case @finalize.errors.full_messages
    when "Your credit card was denied. Please check that you've entered your card information correctly."
      ["api.tc.message.braintree_failed_transaction"]
    when "There was an issue placing your order."
      ["api.tc.message.issue_placing_your_order"]
    else
      @finalize.errors.full_messages
    end
  end

  def load_cart_finalize
    @finalize ||= CartFinalize.new(cart_params)
  end

  def cart_finalize_params
    params[:cart_finalize] || {}
  end

  def cart_params
    {
      person: @person,
      use_balance: cart_finalize_params[:use_balance] || params[:balance],
      payment_id: cart_finalize_params[:payment_id],
      ip_address: request.remote_ip || "unknown",
      purchases: purchase_items,
      three_d_secure_nonce: cart_finalize_params[:three_d_secure_nonce]
    }
  end

  def purchase_items
    tcs_prod_ids = Product.where(display_name: ["tc_social_annually", "tc_social_monthly", "rf_video_download"]).ids
    @person.purchases.unpaid.not_in_invoice.where(product_id: tcs_prod_ids)
  end

  def load_person
    @person = user
    render json: { status: "failure", message: ["User #{params[:email]} not found in TC"] } and return unless @person
  end

  def first_time_purchase
    @person.person_subscription_statuses.where(subscription_type: "social").count == 1
  end

  def load_rf_purchase
    @purchase = purchase_items.last
    return false unless @purchase
    return false unless @purchase.related

    rf_product = Product.where(display_name: RF_VIDEO_DISPLAY_NAME)
    @rf_purchase = rf_product.include?(@purchase.related)
  end

  def handle_rf_purchase
    AddOnPurchase.create!(add_on_params(@purchase.related))
  end

  def add_on_params(product)
    {
      product_id: product.id,
      payment_channel: "TCS " + request.headers["platform"],
      person: @person,
      payment_date: Date.today,
      receipt_data: @finalize.invoice.id
    }
  end
end
