class Api::StoredCreditCardsController < Api::OauthBaseController
  before_action :load_person
  before_action :load_card, only: [:update, :payment_method_nonce]
  MERCHANT_DEFINED_FIELD_5 = 1
  MERCHANT_DEFINED_FIELD_10 = 1

  def create
    params.merge!(
      merchant_defined_field_5: MERCHANT_DEFINED_FIELD_5,
      merchant_defined_field_10: MERCHANT_DEFINED_FIELD_10
    )
    is_success, @vault_transaction = StoredCreditCard.create_with_braintree(
      @person,
      card_create_params
    )
    related_id = @vault_transaction&.related_id
    credit_card = @person.stored_credit_cards.find_by(id: related_id) if related_id
    if is_success && credit_card
      render json: {
        data: @vault_transaction.as_json(except: [:raw_response]),
        stored_credit_card: credit_card&.billing_info.as_json["stored_credit_card"],
        status: "success",
        message: ["Successfully created creditcard."]
      }
    else
      render json: { status: "failure", message: ["Creditcard create failure."] }
    end
  end

  def index
    @stored_credit_cards = @person.stored_credit_cards.active
    render json: {
      status: "success",
      client_token: client_token,
      stored_credit_cards: @stored_credit_cards.map(&:billing_info)
    }
  end

  def update
    is_success, @vault_transaction = @credit_card.update_social_card_with_braintree(card_update_params)
    related_id = @vault_transaction&.related_id
    card = @person.stored_credit_cards.find_by(id: related_id) if related_id
    if is_success && card
      last_4 = card.last_four
      render json: {
        data: @vault_transaction.as_json(except: [:raw_response]),
        last_4: last_4,
        status: "success",
        message: ["Successfully Updated creditcard."]
      }
    else
      render json: { status: "failure", message: ["Creditcard updation failure."] }
    end
  end

  def braintree_token
    gateway_token = @person.config_gateway_service.generate_client_token
    render json: { status: "success", data: gateway_token }
  end

  def payment_method_nonce
    payment_method_nonce = @credit_card.create_payment_method_nonce

    render json: {
      status: :success,
      data: {
        client_token: client_token,
        payment_method_nonce: payment_method_nonce.nonce,
        bin: payment_method_nonce.details[:bin],
        billing_address: @credit_card.billing_address
      }
    }
  end

  private

  def load_person
    @person = user
    render json: { status: "failure", message: "User Token not found in TC" } if @person.nil?
  end

  def load_card
    @credit_card = @person.stored_credit_cards.find_by(id: params[:id])
    render json: { status: "failure", message: "Card not found" } if @credit_card.nil?
  end

  def card_update_params
    params.merge(
      ip_address: request.remote_ip,
      payin_provider_config: @credit_card.payin_provider_config
    )
  end

  def card_create_params
    params.merge(
      ip_address: request.remote_ip,
      payin_provider_config: @person.braintree_config_by_corporate_entity
    )
  end

  def country_code
    params[:country_code] || @person.country_iso_code
  end

  def client_token
    @person
      .config_gateway_service_by_country(country_code)
      .generate_client_token
  end
end
