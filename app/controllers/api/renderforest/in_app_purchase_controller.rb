class Api::Renderforest::InAppPurchaseController < Api::OauthBaseController
  rescue_from ::Renderforest::Apple::PurchaseVerifier::VerificationError, with: :render_error_response

  before_action :load_product

  def create
    case params[:platform]
    when IOS
      apple_purchase
    when ANDROID
      android_purchase
    else
      render json: { status: "failure", messages: ["Invalid Platfrom error"] }
    end
  end

  private

  def verify_apple_request
    @purchase_details ||= Renderforest::Apple::PurchaseVerifier.execute(params["purchase"]["receipt_data"])
  end

  def verify_android_request
    @purchase_details ||= Renderforest::Android::PurchaseVerifier.new.purchase_verifier(
      params[:purchase_token],
      params[:product_id]
    )
  end

  def build_purchase_params
    @purchase_params ||= @purchase_details.merge(
      {
        person: user,
        product: @product
      }
    )
  end

  def apple_purchase
    verify_apple_request
    build_purchase_params
    handler = Renderforest::Apple::PurchaseRequest.new(@purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Renderforest::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  def android_purchase
    verify_android_request
    build_purchase_params
    handler = Renderforest::Android::PurchaseRequest.new(@purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Renderforest::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  def load_product
    @product = Product.where(
      country_website_id: user.country_website_id,
      display_name: params[:name],
      status: "Active"
    ).first
    render json: { status: "failure", message: ["Invalid product name"] } and return if @product.nil?
  end

  def render_error_response(exception)
    render json: { status: "failure", messages: [exception.message] }
  end
end
