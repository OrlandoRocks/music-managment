class Api::Renderforest::ProjectsController < Api::BaseController
  skip_before_action :verify_api_key
  before_action :validate_auth_key

  def project_status
    ::Social::RenderforestRenditionWorker.perform_async(status_params)
    render json: { status: "success" }
  end

  private

  def status_params
    parameters = params.permit(:message, :quality, :status)
    parameters[:id] = params[:project_id]
    parameters[:thumbnail] = params[:thumbnailUrl]
    parameters[:video_url] = params[:videoUrl]
    parameters.to_json
  end

  def validate_auth_key
    hash_input_string = "#{params[:project_id]}:#{params[:queue_id]}:#{params[:status]}:#{params[:timestamp]}:#{ENV['SOCIAL_RENDERFOREST_API_KEY']}"
    valid = Digest::MD5.hexdigest(hash_input_string) == request.headers["Authorization"]
    render json: { status: "failure", message: "api.tc.message.authorization_failure" } unless valid
  end
end
