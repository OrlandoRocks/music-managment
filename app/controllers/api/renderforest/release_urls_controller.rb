class Api::Renderforest::ReleaseUrlsController < Api::OauthBaseController
  before_action :fetch_song, :validate_s3_key

  def index
    url = @song.s3_asset.original_url(1.day)
    render json: { data: url, status: "success" }
  end

  private

  def fetch_song
    @song = Song.find_by(id: params[:song_id])
    render json: { status: "failure", message: ["api.tc.message.not_users_song"] } if @song.album.person != user
  end

  def validate_s3_key
    s3_asset_key = @song.s3_asset.key
    render json: { status: "failure", message: ["api.tc.message.invalid_key"] } if s3_asset_key != params[:s3_key]
  end
end
