class Api::Renderforest::SongsController < Api::OauthBaseController
  before_action :load_albums

  def index
    songs = retrieve_songs(params).paginate(
      page: params[:page],
      per_page: params[:per_page]
    )
    render  json: {
      data: serialized_songs(songs),
      status: "success",
      total_pages: songs&.total_pages,
      next_page: songs&.next_page,
      previous_page: songs&.previous_page
    }
  end

  private

  def retrieve_songs(params)
    song_order = Api::SongsService.new(params[:sort], nil).song_sort
    search_key = params[:search].present? ? "%#{params[:search]}%" : "%"
    Song.with_s3_asset(albums: @albums)
        .where("songs.name LIKE ?", search_key)
        .order(song_order)
  end

  def load_albums
    @albums = user.albums.distributed_albums
    render json: { status: "failure", message: ["api.tc.message.user_no_albums"] } if @albums.blank?
  end

  def serialized_songs(songs)
    ActiveModel::ArraySerializer.new(
      songs,
      each_serializer: Api::Renderforest::SongSerializer
    )
  end
end
