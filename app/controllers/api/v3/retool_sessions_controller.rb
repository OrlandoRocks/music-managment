class Api::V3::RetoolSessionsController < Api::V3::BaseController
  def create
    person = Person.where(email: params[:email]).first
    if person.present?
      render json: { token: generate_jwt(person) }
    else
      render json: { errors: [{ fieldName: :login, messages: ["invalid email id"] }] },
             status: :unauthorized
    end
  end
end
