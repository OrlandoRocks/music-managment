class Api::V3::PostingCurrencyExchangeController < Api::V3::BaseController
  before_action :authenticate_user!
  before_action :current_posting_request?

  def get_current_eligible_posting_royalties
    service = Royalties::PostFinanceApprovalService.new(params[:exchange_rates])
    posting_royalties, currencies = service.current_posting_royalties.as_json(root: false)
    render json: { posting_royalties: posting_royalties, currencies: currencies }.to_json(root: false)
  rescue
    render json: { error: "An error occurred" }, status: :not_found
  end

  def apply_currency_exchange
    service = Royalties::PostFinanceApprovalService.new(params[:exchange_rates])
    if service.valid?
      service.apply_fx_rates(current_user)
      render json: { info: "Currency Exchange applied sucessfully" }, status: :ok
    else
      render json: { error: "An Error Occurred" },
             status: :not_found
    end
  end

  def approve_currency_exchange
    posting_royalties = PostingRoyalty.where(posting_id: Royalties::Posting.get_id)
    approve_postings(posting_royalties)
    render json: { info: "Currency Exchange applied sucessfully" }, status: :ok
  rescue => e
    render json: { error: "An error occurred #{e}" }, status: :not_found
  end

  def make_currency_exchange_editable
    posting_royalties = PostingRoyalty.where(posting_id: Royalties::Posting.get_id)
    update_to_pending(posting_royalties)
    render json: { info: "Currency Exchange is now editable" }, status: :ok
  rescue => e
    render json: { error: "An error occurred #{e}" }, status: :not_found
  end

  def export_posting_data
    Royalties::DataExportNotificationWorker.perform_async(Royalties::Posting.get_id)
    render json: { info: "Report download link will be available in slack once it's generated" }, status: :ok
  rescue => e
    render json: { error: "An error occured while generating the posting report #{e}" }
  end

  private

  def current_posting_request?
    return if Royalties::Posting.get_state == Royalties::Posting::FINANCE_APPROVAL_REQUESTED

    render json: { error: "No actionable posting exists at this momemt" }, status: :not_found
  end

  def approve_postings(posting_royalties)
    posting_royalties.update_all(approved_by: current_user.name, approved_on: Time.current, state: "approved")  # rubocop:disable Rails/SkipsModelValidations
    Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVED)
    Royalties::CopySalesRecordMastersWorker.perform_async
  end

  def update_to_pending(posting_royalties)
    posting_royalties.update_all(fx_rate_submitted_by: nil, fx_rate_submitted_at: nil, state: PostingRoyalty::PENDING)  # rubocop:disable Rails/SkipsModelValidations
  end
end
