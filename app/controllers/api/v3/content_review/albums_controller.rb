class Api::V3::ContentReview::AlbumsController < Api::V2::ContentReview::AlbumsController
  def album_for_review
    album = params[:id].present? ? Album.finalized.find_by(id: params[:id]) : current_user.pop_album_for_review
    if album
      album.start_review(person_id: current_user.id) unless album.nil?
      render json: Api::V3::ContentReview::AlbumSerializer.new(album)
    else
      render json: { error: "This album is not finalized and not available for CRT review" }, status: :not_found
    end
  end

  def songs_for_album
    album = Album.finalized.find_by(id: params[:id])
    if album
      render json: {
        songs: JSON.parse(
          ActiveModel::ArraySerializer.new(
            album.songs.paginate(page: params[:page] || 1, per_page: params[:limit]),
            each_serializer: Api::V2::ContentReview::SongSerializer
          ).to_json(root: false)
        )
      }
    else
      render json: { error: "This album is not finalized and not available for CRT review" }, status: :not_found
    end
  end
end
