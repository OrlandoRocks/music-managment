class Api::BaseController < ApplicationController
  before_action :verify_api_key
  after_action :set_access_control_headers

  skip_before_action :verify_authenticity_token, raise: false
  skip_before_action :login_required
  skip_before_action :redirect_sync_users
  skip_before_action :redirect_ytm_approvers
  skip_before_action :alert_rejections

  attr_reader :user

  def validate_account
    valid = account_is_valid
    render json: {
      message: valid ? "OK" : "Not Found",
      status: valid ? "success" : "failure",
      timestamp: Time.now.getutc
    }
  end

  protected

  def account_is_valid
    Tunecore::Api::Auth.valid_account?(params[:email], ["Active", "Suspicious"])
  end

  def set_access_control_headers
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Request-Method"] = %w[GET POST OPTIONS].join(",")
  end

  def verify_api_key
    head :unauthorized unless Tunecore::Api::Auth.valid_key?(params)
  end
end
