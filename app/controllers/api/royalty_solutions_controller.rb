class Api::RoyaltySolutionsController < Api::BaseController
  before_action :verify_params
  before_action :verify_account

  def validate_account
    valid = account_is_valid

    if valid
      render json: { message: "OK", status: "success", timestamp: Time.now.getutc }, status: :ok
    else
      send_airbrake("Unable to validate account for RoyaltySolutions")

      render json: { message: "Not Found", status: "failure", timestamp: Time.now.getutc },
             status: :unprocessable_entity
    end
  end

  def validate_assets
    response_json = RoyaltySolutions.validate_assets(params)

    if response_json.with_indifferent_access[:status] == "success"
      render json: response_json, status: :ok
    else
      send_airbrake("Unable to validate assets for RoyaltySolutions")

      render json: response_json, status: :unprocessable_entity
    end
  end

  def register
    response_json = RoyaltySolutions.register(params)

    if response_json.with_indifferent_access[:status] == "success"
      render json: response_json, status: :ok
    else
      send_airbrake("Unable to register for RoyaltySolutions")

      render json: response_json, status: :unprocessable_entity
    end
  end

  private

  def verify_params
    missing = ""
    missing << "email" unless params.has_key?("email")
    unless action_name == "validate_account"
      missing << " upcs" unless params.has_key?("upcs")
      missing << " isrcs" unless params.has_key?("isrcs")
    end

    return if missing.blank?

    missing_params_message = "Required Params:#{missing}"

    send_airbrake("Required params missing for RoyaltySolutions request. #{missing_params_message}")

    render json: { message: missing_params_message, status: "failure", timestamp: Time.now },
           status: :unprocessable_entity
  end

  def verify_account
    return if Tunecore::Api::Auth.valid_account?(params[:email], ["Active", "Suspicious"])

    send_airbrake("Account not found for RoyaltySolutions request")

    render json: { message: "Not Found", status: "failure", timestamp: Time.now }, status: :unprocessable_entity
  end

  def send_airbrake(message)
    Airbrake.notify(message, { params: params })
  end
end
