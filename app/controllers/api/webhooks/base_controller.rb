class Api::Webhooks::BaseController < ApplicationController
  skip_before_action :verify_authenticity_token, raise: false
  skip_before_action :login_required
end
