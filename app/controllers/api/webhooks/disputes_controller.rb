# frozen_string_literal: true

class Api::Webhooks::DisputesController < Api::Webhooks::BaseController
  def braintree
    Disputes::BraintreeDispute
      .new(braintree_params[:bt_signature], braintree_params[:bt_payload])
      .update_or_create_dispute!
  rescue Disputes::BraintreeDispute::InvalidDisputeError
    head :bad_request
  else
    head :ok
  end

  private

  def braintree_params
    params.permit(:bt_signature, :bt_payload)
  end
end
