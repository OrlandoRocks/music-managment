# frozen_string_literal: true

class Api::Webhooks::AdyenController < Api::Webhooks::BaseController
  before_action :trace_request, :authenticate_webhook!

  def authorize_payment
    response =
      if payment_authorisation?
        write_transaction_log_to_s3(webhook_params)
        Adyen::UpdateAdyenTransactionInfoService.new(webhook_params).update_transaction
      elsif payment_recurring_contract?
        Adyen::UpdateAdyenRecurringContractService.new(webhook_params).update_transaction
      elsif refund_authorisation?
        Adyen::UpdateAdyenRefundTransactionService.new(webhook_params).update_transaction
      end
    render json: { notificationResponse: "[accepted]", authorisation: response }
  end

  private

  def authenticate_webhook!
    return if Adyen::AuthorizationService.new(webhook_params).authorize!

    fetch_adyen_transaction&.broadcast_unauthorization if payment_authorisation?
    render json: { notificationResponse: "[accepted]", authorisation: "unauthorized" }
  end

  def write_transaction_log_to_s3(adyen_response)
    return unless (adyen_transaction = fetch_adyen_transaction)

    Adyen::TransactionLogWorker.new.async.write_to_bucket(
      adyen_transaction.invoice_id,
      adyen_response
    )
  end

  def fetch_adyen_transaction
    AdyenTransaction.find_by(session_id: checkout_session_id)
  end

  def checkout_session_id
    webhook_params.dig("notificationItems", 0, "NotificationRequestItem", :additionalData, :checkoutSessionId)
  end

  def payment_authorisation?
    event_code == AdyenTransaction::AUTHORISATION
  end

  def payment_recurring_contract?
    event_code == AdyenTransaction::RECURRING_CONTRACT
  end

  def refund_authorisation?
    event_code == AdyenTransaction::REFUND
  end

  def event_code
    params["notificationItems"][0]["NotificationRequestItem"]["eventCode"]
  end

  def webhook_params
    params.permit(
      notificationItems: {
        NotificationRequestItem: [
          :paymentMethod,
          {
            additionalData: [
              :checkoutSessionId,
              :hmacSignature,
              :"recurring.recurringDetailReference"
            ]
          },
          :pspReference,
          :reason,
          :originalReference,
          :merchantAccountCode,
          :merchantReference,
          :eventCode,
          :success,
          {
            amount: [
              :currency,
              :value
            ]
          }
        ]
      }
    )
  end

  def trace_request
    Rails.logger.info "Adyen webhook parameters: #{webhook_params}"
  end
end
