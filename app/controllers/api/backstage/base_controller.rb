class Api::Backstage::BaseController < ApplicationController
  before_action :load_person
  after_action :set_access_control_headers

  [
    :alert_rejections,
    :maintenance_alert,
    :set_gon_variables,
    :set_gon_two_factor_auth_prompt,
    :force_pw_reset,
    :redirect_sync_users,
    :redirect_ytm_approvers
  ].each do |filter|
    skip_before_action filter
  end

  protected

  def set_access_control_headers
    headers["Access-Control-Allow-Origin"]   = "*"
    headers["Access-Control-Request-Method"] = "GET, PATCH, PUT, POST, DELETE, OPTIONS"
    headers["Access-Control-Allow-Headers"]  = "Origin, Content-Type, X-Auth-Token"
  end
end
