class Api::Backstage::BalanceHistory::ExportsController < Api::Backstage::BaseController
  def create
    transactions = ::BalanceHistory::TransactionsQueryBuilder.build(export_params)
    export_form = PersonTransaction::ExportForm.new(
      transactions: transactions,
      person: current_user
    )
    render csv: export_form.csv.to_s, filename: export_form.filename if export_form.save
  end

  private

  def export_params
    (params[:export_form] || {}).merge(person: current_user)
  end
end
