class Api::Backstage::SongDataController < Api::Backstage::BaseController
  include AlbumHelper
  before_action :set_paper_trail_whodunnit

  def create
    song_form = SongDataForm.new(creation_params)
    if song_form.save
      render json: song_form, serializer: Api::Backstage::SongDataFormSerializer, root: false
    else
      # TODO: use correct codes
      render json: song_form, serializer: Api::Backstage::SongDataFormSerializer, root: false, status: :bad_request
    end
  end

  # The V2 Distribution flow song-related data endpoint
  # See app/presenters/song_data_presenter and app/services/song_data
  def index
    album = Album.find_by(id: params[:album_id])
    tc_pid = generate_sso_cookie_value(album.person_id)
    data = SongDataPresenter.new(album, params[:person_id], tc_pid)

    render json: Api::Backstage::AlbumApp::SongsDataSerializer.new(data), root: false
  end

  def update
    song_form = SongDataForm.new(update_params)
    if song_form.save
      render json: song_form, serializer: Api::Backstage::SongDataFormSerializer, root: false
    else
      render json: song_form, serializer: Api::Backstage::SongDataFormSerializer, root: false, status: :bad_request
    end
  end

  def destroy
    song = current_user.songs.find(params[:id])
    return head 404 unless song.album.is_editable?

    ActiveRecord::Base.transaction do
      handle_immersive_audio_removal(song) if song.immersive_audio
      song.destroy
      create_note(song)
    end

    head :no_content
  end

  def destroy_immersive_audio
    song = current_user.songs.find(params[:id])
    return head 404 unless song.album.is_editable?

    handle_immersive_audio_removal(song) if song.immersive_audio
    head :no_content
  end

  private

  def album
    @album ||= Album.find_by(id: params.dig(:song, :album_id))
  end

  def creation_params
    song_data_form_params
  end

  def update_params
    song_data_form_params
  end

  def song_data_form_params
    {
      song_params: song_params,
      person_id: current_user.id,
      should_build_copyrights: song_copyrights_enabled_in_region?,
      should_build_start_times: song_start_time_form_enabled?,
      should_build_cover_song_metadata: cover_song_metadata_enabled?,
    }
  end

  def song_params
    params.require(:song).permit(
      :id,
      :name,
      :language_code_id,
      :translated_name,
      :version,
      :cover_song,
      :made_popular_by,
      :explicit,
      :clean_version,
      :optional_isrc,
      :lyrics,
      :asset_url,
      :album_id,
      :previously_released_at,
      :asset_filename,
      :instrumental,
      :track_number,
      :language_code_id,
      artists: [
        :artist_name,
        :credit,
        :creative_id,
        :associated_to,
        { role_ids: [] },
      ],
      copyrights: [
        :id,
        :composition,
        :recording,
      ],
      song_start_times: [
        :id,
        :store_id,
        :start_time,
      ],
      cover_song_metadata: [
        :id,
        :cover_song,
        :licensed,
        :will_get_license,
      ],
    )
  end

  def handle_immersive_audio_removal(song)
    song.immersive_audio.purchase && song.immersive_audio.purchase.destroy
    ImmersiveAudio::RemovalService.new(immersive_audio_id: song.immersive_audio.id).call
  end

  def create_note(song)
    Note.create!(
      related: song.album,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Song deleted",
      note: "Song '#{song.name}' (ID: #{song.id}) deleted on Album"
    )
  end
end
