class Api::Backstage::AlbumApp::SalepointsController < Api::Backstage::BaseController
  def create
    if creation_form.save
      render json: creation_response_json, status: :ok
    else
      render json: { errors: creation_form.errors }, status: :unprocessable_entity
    end
  end

  private

  def creation_form
    @creation_form ||= AlbumSalepointsForm.new(creation_params)
  end

  def creation_params
    params.permit(:album_id, :deliver_automator, :apple_music, { store_ids: [] })
  end

  def creation_response_json
    {
      stores: serialized_digital_stores,
      freemium_stores: serialized_freemium_stores,
      deliver_automator: creation_form.deliver_automator,
      apple_music: creation_form.apple_music,
    }
  end

  def serialized_digital_stores
    salepoint_store_compiler_service.serialized_digital_stores
  end

  def serialized_freemium_stores
    salepoint_store_compiler_service.serialized_freemium_stores
  end

  def salepoint_store_compiler_service
    @salepoint_store_compiler_service ||= SalepointStoreCompilerService.new(person: current_user, album: album)
  end

  def album
    @album ||= Album.find(creation_params[:album_id])
  end
end
