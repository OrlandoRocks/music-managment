# frozen_string_literal: true

class Api::Backstage::AlbumApp::ExternalServiceIdsController < Api::Backstage::BaseController
  before_action :load_apple_client,
                only: %i[
                  apple_get_artist
                  apple_search_artists
                ]
  before_action :load_spotify_client,
                only: %i[
                  spotify_get_artist
                  spotify_recent_albums
                  spotify_search_artists
                ]

  attr_accessor :apple, :spotify

  def spotify_search_artists
    render json: { message: "Param 'name' is required." }, status: :unprocessable_entity and return unless params[:name]

    results = spotify.find_by_artist_name(params[:name])

    render json: results.to_json
  end

  def spotify_recent_albums
    unless params[:artist_id]
      render json: { message: "Param 'artist_id' is required." }, status: :unprocessable_entity and return
    end

    results = spotify.spotify_recent_albums(params[:artist_id])
    render json: results.to_json
  end

  def spotify_get_artist
    unless params[:id] && params[:name]
      render json: { message: "Params 'id' and 'name' are required." }, status: :unprocessable_entity and return
    end

    results = spotify.get_valid_artist_by_id(params[:id], params[:name])

    if results.success?
      render json: results.body
    else
      render json: results.body, status: results.status
    end
  end

  def apple_search_artists
    render json: { message: "Param 'name' is required." }, status: :unprocessable_entity and return unless params[:name]

    results = apple.find_by_artist_name(params[:name])
    render json: results.to_json
  end

  def apple_get_artist
    unless params[:id] && params[:name]
      render json: { message: "Params 'id' and 'name' are required." }, status: :unprocessable_entity and return
    end

    results = apple.get_artist_by_id(params[:id], params[:name])

    error = results.dig(:error, :status)
    if error
      render json: results, status: status
    else
      render json: results
    end
  end

  private

  def load_apple_client
    @apple = AppleMusic::ApiClient.new
  end

  def load_spotify_client
    @spotify = Spotify::ApiClient.new
  end
end
