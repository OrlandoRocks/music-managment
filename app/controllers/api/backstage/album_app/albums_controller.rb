# frozen_string_literal: true

class Api::Backstage::AlbumApp::AlbumsController < Api::Backstage::BaseController
  include AlbumHelper
  include TimedReleasable
  include InternationalGenre
  include WorldwideReleasable
  include SpecializedRelease

  before_action :load_album_type, only: [:update, :create]
  before_action :load_album, only: [:update, :create]
  before_action :scrub_artist_names, only: [:update, :create]
  before_action -> { handle_parent_genre_params(params[:album]) }, only: [:update, :create]
  before_action -> { set_original_release_date(params[:album]) }, only: [:update]

  attr_accessor :album

  def create
    @album_form = album_form_class.new(creation_params)
    if @album_form.save
      set_release_countries(@album, release_countries_params)

      creatives_param = creation_params[:album_params]["creatives"]
      create_esids(@album, creatives_param)

      album_response
    else
      render json: { errors: @album_form.errors }, root: false, status: :unprocessable_entity
    end
  end

  def update
    return if @album.finalized? # form is disabled, should not reach

    @album_form = album_form_class.new(update_params)

    if @album_form.save
      set_release_countries(@album, release_countries_params)

      creatives_param = update_params[:album_params]["creatives"]
      create_esids(@album, creatives_param)

      album_response
    else
      render json: { errors: @album_form.errors }, root: false, status: :unprocessable_entity
    end
  end

  private

  def album_response
    if FeatureFlipper.show_feature?(:album_app_v2, current_user)
      render json: { album: Api::Backstage::AlbumApp::AlbumSerializer.new(@album) }, root: false
    else
      render json: { album_show_url: album_show_url(@album_form.album) }, root: false
    end
  end

  def load_album_type
    @album_type = (params.dig(:album, :album_type) == "Album") ? "Album" : "Single"
  end

  def load_album
    @album =
      if @album_type == "Single"
        current_user.singles.find_by(id: params[:id]) || current_user.singles.build
      else
        current_user.albums.find_by(id: params[:id]) || current_user.albums.build
      end
  end

  def update_params
    params_copy = SongData::ParamsScrubber.scrub(album_params, @album)
                                          .merge(created_with: @album.created_with)

    form_params.merge(album_params: params_copy)
  end

  def creation_params
    params_copy = album_params.merge(created_with: Album.created_withs[:songwriter])

    form_params.merge(album_params: params_copy)
  end

  def form_params
    {
      album: @album,
      album_id: params[:id],
      album_type: @album_type,
      person: current_user,
      spotify_artists_required: spotify_artists_required?
    }
  end

  def album_params
    result = initial_album_params
             .merge(metadata_language_code_id: metadata_language_code_id(initial_album_params))

    handle_golive_date(result)
  end

  def initial_album_params
    params
      .require(:album)
      .permit(
        :album_type,
        :id,
        :is_various,
        :label_name,
        :language_code,
        :name,
        :orig_release_year,
        :primary_genre_id,
        :recording_location,
        :sale_date,
        :secondary_genre_id,
        :optional_isrc,
        :optional_upc_number,
        :timed_release_timing_scenario,
        :parental_advisory,
        :clean_version,
        :specialized_release_type,
        golive_date: [:year, :month, :day, :hour, :min, :meridian],
        creatives: [
          :id,
          :name,
          :role,
          {
            apple: %i[identifier create_page state],
            spotify: %i[identifier create_page state]
          }
        ]
      )
  end

  def specialized_release_type
    initial_album_params[:specialized_release_type]
  end

  def release_countries_params
    params[:album][:selected_countries]
  end

  def album_form_class
    specialized_release_type.present? ? SpecializedReleaseForm : AlbumForm
  end

  def album_show_url(album)
    album_url(
      id: album.id,
      host: album.person.country_website.url.to_s,
      protocol: ENV["DOCUSIGN_PROTOCOL"],
    )
  end

  def handle_golive_date(result)
    return result if result[:golive_date].blank?

    new_golive_date = convert_date_hash_to_golive_date(initial_album_params[:golive_date])
    new_sale_date = convert_date_hash_to_sale_date(initial_album_params[:golive_date])

    result.merge(
      golive_date: new_golive_date,
      sale_date: new_sale_date
    )
  end

  def spotify_artists_required?
    FeatureFlipper.show_feature?(:spotify_artists_required, current_user)
  end
end
