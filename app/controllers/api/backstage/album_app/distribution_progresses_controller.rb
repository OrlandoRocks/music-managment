class Api::Backstage::AlbumApp::DistributionProgressesController < Api::Backstage::BaseController
  include DistributionProgressable

  def show
    if album
      render json: response_json, status: :ok
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private

  def album
    @album ||= Album.find_by(id: params[:album_id])
  end

  def response_json
    {
      distribution_progress_level: set_distribution_progress_level(album),
      songs_complete: set_songs_complete(album),
    }
  end
end
