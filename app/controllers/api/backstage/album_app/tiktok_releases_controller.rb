class Api::Backstage::AlbumApp::TiktokReleasesController < Api::Backstage::AlbumApp::AlbumsController
  private

  def album_form_class
    TiktokReleaseForm
  end

  def album_show_url(album)
    tiktok_release_url(
      id: album.id,
      host: album.person.country_website.url.to_s,
      protocol: ENV["DOCUSIGN_PROTOCOL"]
    )
  end
end
