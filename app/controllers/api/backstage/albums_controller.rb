class Api::Backstage::AlbumsController < Api::Backstage::BaseController
  def show
    album = Album.find(params[:id])
    render json: album, serializer: Api::Backstage::AlbumSerializer
  end

  def update
    album = Album.find(params[:id])

    if album.update(album_params)
      render json: { song_order: album.songs.pluck(:id) }, status: :ok
    else
      render json: { errors: album.errors }, status: :unprocessable_entity
    end
  end

  def fetch_song_ids
    album = Album.find(params[:album_id])
    song_ids = album.songs.map(&:id)
    render json: { song_ids: song_ids }, status: :ok
  end

  private

  def album_params
    album_parameters = {}

    album_parameters[:song_order] = params[:album][:song_order] if params[:album][:song_order]

    album_parameters
  end
end
