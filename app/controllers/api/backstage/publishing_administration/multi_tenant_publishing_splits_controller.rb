class Api::Backstage::PublishingAdministration::MultiTenantPublishingSplitsController < Api::Backstage::BaseController
  def create
    composition = PublishingComposition.find(publishing_splits_params[:composition_id])
    form_type = ::PublishingAdministration::MultiTenantPublishingCompositionSplitsForm

    form_params = {
      person: current_user,
      composition: composition,
      composer_params: composer_params,
      cowriter_params: cowriter_params,
      translated_name: publishing_splits_params[:translated_name]
    }

    publishing_splits_form = form_type.new(form_params)

    if publishing_splits_form.save
      render json: {
        composition_id: params[:composition_id],
        composer_percent: publishing_splits_form.composer_percent,
        cowriter_percent: publishing_splits_form.cowriter_percent,
        cowriters: publishing_splits_form.cowriters,
        composers: publishing_splits_form.composers,
        translated_name: publishing_splits_form.translated_name,
        submitted_at: composition.share_submitted_date,
        unknown_split_percent: composition.publishing_composition_splits.sum(:percent),
      }
    else
      render json: { errors: publishing_splits_form.errors }, status: :bad_request
    end
  end

  private

  def publishing_splits_params
    params.permit(
      :composer_id,
      :composition_id,
      :translated_name,
      composer_params: [
        :id,
        :composer_share
      ],
      cowriter_params: [
        :first_name,
        :last_name,
        :cowriter_share
      ]
    )
  end

  def composer_params
    return [] if publishing_splits_params[:composer_params].nil?

    publishing_splits_params[:composer_params].map do |composer_hash|
      composer_hash.each_value { |value| value.to_s.strip! }
    end
  end

  def cowriter_params
    return [] if publishing_splits_params[:cowriter_params].nil?

    publishing_splits_params[:cowriter_params].map do |cowriter_hash|
      cowriter_hash.each_value { |value| value.to_s.strip! }
    end
  end
end
