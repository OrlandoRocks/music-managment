class Api::Backstage::PublishingAdministration::ComposersController < Api::Backstage::BaseController
  def update
    # enrollment_form = ::PublishingAdministration::EnrollmentForm.new(composer_params)
    #
    # if enrollment_form.save
    #  render json: serialized_composer(enrollment_form.composer)
    # else
    #  render json: { errors: enrollment_form.errors }
    # end
    head :access_denied
  end

  private

  def composer_params
    PublishingAdministration::ParamsComposerService
      .compose(params[:id])
      .merge!(sanitized_composer_params)
  end

  def serialized_composer(composer)
    Api::Backstage::ComposerSerializer.new(composer).to_json(root: false)
  end

  def sanitized_composer_params
    params[:composer].each_value { |value| value.to_s.strip! }.with_indifferent_access
  end
end
