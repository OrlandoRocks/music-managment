class Api::Backstage::PublishingAdministration::CompositionsHiddenController < Api::Backstage::BaseController
  def update
    composition = PublishingComposition.find(params[:id])

    if composition.hide!.present?
      render json: composition, root: false
    else
      render json: { errors: { hidden: false } }
    end
  end
end
