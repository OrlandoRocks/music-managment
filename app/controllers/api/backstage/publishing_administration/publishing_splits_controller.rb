class Api::Backstage::PublishingAdministration::PublishingSplitsController < Api::Backstage::BaseController
  def create
    composition = PublishingComposition.find(publishing_splits_params[:composition_id])
    form_type = ::PublishingAdministration::PublishingCompositionSplitsForm

    publishing_splits_form = form_type.new(publishing_splits_form_params(composition))

    if publishing_splits_form.save
      render json: {
        composition_id: params[:composition_id],
        composer_percent: publishing_splits_form.composer_percent,
        cowriter_percent: publishing_splits_form.cowriter_percent,
        cowriters: publishing_splits_form.cowriters,
        public_domain: publishing_splits_form.public_domain,
        translated_name: publishing_splits_form.translated_name,
        submitted_at: composition.share_submitted_date,
      }
    else
      render json: {
        composition_id: publishing_splits_form.composition.id,
        errors: publishing_splits_form.errors
      },
             status: :unprocessable_entity
    end
  end

  def update
    update_splits_form = update_form_class.new(update_splits_params)

    if update_splits_form.update
      render json: update_splits_form.response_body
    else
      render json: {
        composition_id: update_splits_params[:composition_id],
        errors: update_splits_form.errors
      },
             status: :bad_request
    end
  end

  private

  def update_form_class
    ::PublishingAdministration::UpdatePublishingCompositionSplitsForm
  end

  def publishing_splits_params
    params.permit(
      :composer_id,
      :composition_id,
      :translated_name,
      :composer_share,
      :public_domain,
      cowriter_params: [
        :first_name,
        :last_name,
        :cowriter_share
      ]
    )
  end

  def cowriter_params
    return [] if publishing_splits_params[:cowriter_params].nil?

    publishing_splits_params[:cowriter_params].map do |cowriter_hash|
      cowriter_hash.each_value { |value| value.to_s.strip! }
    end
  end

  def update_splits_params
    permitted = params
                .require(:composition_params)
                .permit(
                  :composition_title,
                  :translated_name,
                  :appears_on,
                  :record_label,
                  :composer_share,
                  :cowriter_share,
                  :status,
                  :isrc,
                  :performing_artist,
                  :release_date,
                  :is_ntc_song,
                  :unknown_split_percent,
                  :public_domain
                )

    permitted = permitted.merge(
      cowriter_params: update_cowriter_params || [],
      composition_id: params.require(:composition_id)
    )

    permitted[:is_ntc_song] = \
      ActiveModel::Type::Boolean.new.cast(permitted[:is_ntc_song])

    permitted
  end

  def update_cowriter_params
    params
      .permit(
        cowriter_params: [
          :id,
          :first_name,
          :last_name,
          :cowriter_share,
          :is_unknown,
          :uuid,
        ]
      )["cowriter_params"]
  end

  def publishing_splits_form_params(composition)
    composer = PublishingComposer.find(publishing_splits_params[:composer_id])
    {
      person: current_user,
      composer: composer,
      composition: composition,
      public_domain: publishing_splits_params[:public_domain],
      composer_share: publishing_splits_params[:composer_share].try(:to_d) || 0.0,
      cowriter_params: cowriter_params,
      translated_name: publishing_splits_params[:translated_name]
    }
  end
end
