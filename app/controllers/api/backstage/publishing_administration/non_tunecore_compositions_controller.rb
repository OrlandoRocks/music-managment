class Api::Backstage::PublishingAdministration::NonTunecoreCompositionsController < Api::Backstage::BaseController
  include Tunecore::CsvExporter
  before_action :validate_bulk_feature_enabled, only: [:get_bulk_upload_sample, :bulk_upload]

  def create
    ntc_composition_form = creation_form_class.new(ntc_composition_params)

    if ntc_composition_form.save
      render json: ntc_composition_form.response_body
    else
      render json: { errors: ntc_composition_form.errors }, status: :unprocessable_entity
    end
  end

  def get_bulk_upload_sample
    file = ::PublishingAdministration::NtcBulkUploadService.sample_upload_file

    filename = "bulk-upload-compositions-sample.csv"
    set_csv_headers(filename)
    send_data file, filename: filename, type: "text/csv"
  end

  def bulk_upload
    file =  params[:compositions_upload].tempfile
    composer_id = params[:composer_id]

    service = ::PublishingAdministration::NtcBulkUploadService
              .new(current_user.id, composer_id, file)

    service.process

    if service.errors.present?
      render json: { errors: service.errors }, status: :unprocessable_entity
    else
      render json: { status: "ok" }, status: :ok
    end
  end

  private

  def creation_form_class
    ::PublishingAdministration::NonTunecorePublishingCompositionForm
  end

  def ntc_composition_params
    params
      .require(:non_tunecore_composition)
      .permit(:title,
              :artist_name,
              :album_name,
              :isrc_number,
              :release_date,
              :percent,
              :record_label,
              :public_domain,
              cowriter_params: [:first_name, :last_name, :cowriter_share, :errors])
      .merge(person_id: current_user.id.to_s, composer_id: params[:composer_id])
      .each_value { |value| value.strip! if value.respond_to?(:strip!) }
  end

  def validate_bulk_feature_enabled
    redirect_to compositions_path unless FeatureFlipper.show_feature?(:bulk_upload_compositions, current_user)
  end
end
