class Api::Backstage::TrackMonetization::DashboardController < Api::Backstage::BaseController
  include TrackMonetizationsHelper

  before_action :load_distribution_form, only: :create

  def create
    render json: { data: @distribution_form.response_data } if @distribution_form.save
  end

  private

  def load_distribution_form
    @distribution_form = TrackMonetization::DistributionForm.new(distribution_form_params)
  end

  def distribution_form_params
    params.require(:track_monetization).merge({ current_user: current_user })
  end
end
