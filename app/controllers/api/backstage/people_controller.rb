class Api::Backstage::PeopleController < Api::Backstage::BaseController
  skip_before_action :verify_authenticity_token, :check_for_verification

  def update_preference
    update_language_preference

    render json: { status: "ok" }
  end

  private

  def update_language_preference
    language = CountryWebsiteLanguage.find_by(yml_languages: language_params[:locale])
    current_user.update(country_website_language_id: language.id) if language.present?
  end

  def language_params
    params.require(:language).permit(:locale)
  end
end
