class Api::Backstage::TwoFactorAuth::ResendsController < Api::Backstage::BaseController
  extend TwoFactorAuthable

  before_action :fetch_attempts, only: [:create]

  def create
    alternate_method = params[:alternate_method].present?
    request_method = current_user.tfa_method(alternate_method)

    if TwoFactorAuth::FraudPrevention.request_banned?(current_user.id, request_method)
      render json: { banned: true }
      return
    end

    if on_the_page?(params, :authentication) && exceeds_max_daily_attempts?(current_user, :resend)
      render json: { exceeds_daily_resends: true }
      return
    end

    if @recent_auth_events.blank?
      if FeatureFlipper.show_feature?(:verify_2fa, current_user)
        TwoFactorAuth::ApiClient.new(current_user).request_authorization(alternate_method)
      else
        TwoFactorAuth::AuthyApiClient.new(current_user).request_authorization(alternate_method)
      end

      TwoFactorAuthEvent.create(
        type: "authentication",
        page: "account_settings",
        action: "back_stage_code_resend",
        successful: true,
        two_factor_auth_id: @person.two_factor_auth.id
      )
    end

    render json: { status: "ok" }
  end

  def fetch_attempts
    @recent_auth_events = TwoFactorAuthEvent.joins(:two_factor_auth).where(action: "backstage_code_resend", created_at: 1.minute.ago..Time.current, two_factor_auths: { person_id: @person.id })
  end
end
