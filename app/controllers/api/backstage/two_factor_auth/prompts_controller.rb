class Api::Backstage::TwoFactorAuth::PromptsController < Api::Backstage::BaseController
  def update
    tfa_prompt = current_user.two_factor_auth_prompt
    tfa_prompt&.dismiss

    head :ok
  end
end
