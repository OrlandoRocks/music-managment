class Api::Backstage::TwoFactorAuth::AppCallbacksController < Api::Backstage::BaseController
  before_action :load_tfa, only: [:show]

  def show
    if @tfa && @tfa.within_authentication_window?
      render json: { status: "successful" }
    else
      render json: { status: "unsuccessful" }
    end
  end

  private

  def load_tfa
    @tfa ||= current_user.two_factor_auth
  end
end
