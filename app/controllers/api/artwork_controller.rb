class Api::ArtworkController < Api::BaseController
  def create
    @album = Album.find(params[:album_id])
    @artwork = Artwork.new(album: @album)

    @artwork.copy_image_file_from_s3 params[:s3_artwork_keys]
    @artwork.save
    head 201
  rescue StandardError => e
    Rails.logger.error("\n#{e.class} (#{e.message}):\n #{Rails.backtrace_cleaner.clean(e.backtrace).join("\n ")}")
    head 422
  end
end
