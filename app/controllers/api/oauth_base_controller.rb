class Api::OauthBaseController < ActionController::Base
  include Api::SocialAuthentication

  protect_from_forgery prepend: true
  before_action :authenticate
end
