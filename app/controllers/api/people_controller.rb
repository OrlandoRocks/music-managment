class Api::PeopleController < Api::BaseController
  include PeopleHelper
  include CartHelper
  include ReferralData::Parsable
  include Api::SocialAuthentication

  before_action :build_person, only: [:registration]
  before_action(only: [:registration]) { |c| c.build_referral_data_from_cookies(:tcs_signup, true) }
  before_action(only: [:registration]) { |c| c.build_referral_data_from_url(:tcs_signup, true) }
  skip_before_action :verify_api_key, only: [:complete_verify]
  before_action :authenticate,
                only: [
                  :vat_information,
                  :update_vat_information,
                  :non_self_identified_countries,
                  :personal_info,
                  :personal_info_setting
                ]

  def update_account_profile
    survey_info = PersonProfileSurveyInfo.find_by(survey_token: params[:sguid])
    survey_info.set_segment_response(params[:customer_segment]) if survey_info
    render json: { "message" => "OK" }
  end

  def registration
    @person.password_reset_tmsp = Time.zone.now
    if @person.save
      @person.create_logged_in_event(request.headers["Client-Remote-IP"], request.user_agent)
      notify_person
      record_optimizely_event(
        "successfully_signed_up",
        cookies[:optimizelyEndUserId],
        cookies[:optimizelyBuckets]
      ) if cookies[:optimizelyEndUserId] && cookies[:optimizelyBuckets]
      @person.record_login(request.remote_ip)
      clear_referral_cookies
      render json: { status: "success", message: ["api.tc.message.person_created"] }
    else
      render json: { status: "failure", message: @person.errors.full_messages }
    end
  end

  def update_password
    @person = Person.find_by(id: password_params[:id].to_i, invite_code: password_params[:invite_code])
    render json: { status: "failure", message: ["api.tc.message.person_unknown"] } and return if @person.nil?

    update_params = password_params.merge(from_password_reset_form: true, password_reset_tmsp: Time.now).except(
      "invite_code", "id"
    )
    if @person.update(update_params)
      @person.post_update_password(password_params[:password], request.remote_ip, request.user_agent)
      render json: { status: "success", message: ["api.tc.message.password_updated"] }
    else
      render json: { status: "failure", message: @person.errors.full_messages }
    end
  end

  def resend_confirmation_email
    @person = Person.find_by_id(params[:id].to_i)
    if @person and deliver_verification_email(@person)
      render json: { status: "success", message: ["Confirmation email sent."] }
    else
      render json: { status: "failure", message: ["Invalid email"] }
    end
  end

  def complete_verify
    @person = Person.find_by_id(params[:id].to_i)
    if @person.is_verified?
      message = "already_verified"
    elsif @person.confirm_invite(params[:key])
      handle_successful_verification
      deliver_cyrillic_formatting(@person) if load_cyrillic_countries.include? @person.country
      session[:verification] = true if current_user.present?
      message = "verified"
      RewardSystem::Events.new_user(@person.id)
    else
      message = handle_failed_verification
    end
    redirect_to ENV["TC_SOCIAL_REGISTRATION_URL"] + "?message=#{message}&id=#{@person.id}"
  end

  def send_auth_code
    person = Person.find_by(email: params[:email])
    render json: { status: "failure", message: ["api.tc.message.email_exists"] } and return if person.present?

    response = PhoneNumberVerifier.register_authy_user(params)
    if response.ok?
      sms_response = PhoneNumberVerifier.request_sms(response.id)
      timeout = ENV["OTP_RESEND_TIMEOUT_IN_SEC"].to_i
      data = { authy_id: response.id, timeout_in_sec: timeout }
      if sms_response.ok?
        render json: { data: data, status: "success", message: "api.tc.message.auth_code.generated" }
      else
        render json: { data: data, status: "failure", message: "api.tc.message.auth_code.generate_otp_error" }
      end
    else
      render json: { status: "failure", message: "api.tc.message.auth_code.invalid_authy_params" }
    end
  end

  def vat_information
    vat_info = user.vat_information
    data = {
      id: vat_info&.id,
      vat_registration_number: vat_info&.vat_registration_number,
      company_name: vat_info&.company_name,
      customer_type: user.customer_type,
      corporate_entity: user.corporate_entity&.name
    }
    render json: { status: "success", data: data }
  end

  def update_vat_information
    user.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
    @compliance_info_form = ComplianceInfoForm.new(user)
    @compliance_info_form.save(compliance_info_params)

    if @compliance_info_form.errors.empty? && user.update(vat_params)
      render json: { status: "success" }
    else
      render json: { status: "failure", message: [user.errors.values, compliance_info_errors].flatten.join(", ") }
    end
  end

  def show_feature
    @person = Person.find_by(id: params[:id])
    render json: { status: :success, data: { show: FeatureFlipper.show_feature?(params[:feature], @person) } }
  end

  def non_self_identified_countries
    country_data = user.countries_from_non_self_sources&.map(&:attributes)
    render json: { status: :success, data: country_data }
  end

  def personal_info
    address_data = user.attributes
                       .slice("address1", "address2", "city", "state")
                       .merge(postal_code: user.foreign_postal_code,
                              country_code: user[:country],
                              country_name: user.country_name_untranslated)

    render json: { status: :success, data: { address: address_data, compliance_info: compliance_info } }
  end

  def personal_info_setting
    @person = user
    customer_type_locked = Person::FlagService.person_flag_exists?(
      {
        person: @person,
        flag_attributes: Flag::CUSTOMER_TYPE_LOCKED
      }
    )
    render json: {
      status: :success,
      data: {
        enable_address_form: show_country_selector?,
        customer_type_locked: customer_type_locked,
        vat_info_sharing: user.sharing_vat_number?
      }
    }
  end

  private

  def compliance_info_params
    return {} unless params[:compliance_info_fields_attributes]

    info_params = params[:compliance_info_fields_attributes].map do |info|
      [info[:field_name], info[:field_value]]
    end.to_h
    info_params.merge!(params.permit(:customer_type, :country)).with_indifferent_access
  end

  def compliance_info_errors
    return [] unless @compliance_info_form.errors.any?

    translated_error_field_names = @compliance_info_form.errors.full_messages.map do |field_name|
      custom_t("people.renewal_billing_info.#{field_name}")
    end.join(", ")
    custom_t("people.renewal_billing_info.error_saving_info", error: translated_error_field_names)
  end

  def vat_params
    params.permit(
      :customer_type,
      :address1,
      :address2,
      :city,
      :state,
      :postal_code,
      :country,
      vat_information_attributes: %i[id company_name vat_registration_number]
    )
  end

  def country_params
    country = Country.find(params[:country].to_i)&.iso_code
    params.merge!(country: country)
  end

  def build_person
    user = Person.find_by(email: params[:email])
    render json: { status: "failure", message: ["api.tc.message.email_exists"] } and return if user.present?

    is_name_size_valid = params[:name].length >= 5 && params[:name].length <= 80
    render json: {
      status: "failure",
      message: ["api.tc.message.name_size_invalid"]
    } and return unless is_name_size_valid

    render json: {
      status: "failure",
      message: ["api.tc.message.accept_tandc"]
    } and return unless params[:accepted_terms_and_conditions] == "true"

    country = Country.find(params[:country].to_i)&.iso_code
    country_website_id = CountryWebsite.country_id_for(country)
    if country_website_id == CountryWebsite::INDIA
      response = PhoneNumberVerifier.verify_auth_code(params[:authy_id], params[:auth_code])
      render json: {
        status: "failure",
        message: "api.tc.message.auth_code.invalid_code"
      } and return unless response.ok?

      phone_number = "+#{params[:phone_code]}#{params[:phone_number]}"
    end
    params.merge!(country: country, country_website_id: country_website_id, phone_number: phone_number)
    @person = Person.new(person_params)
    @person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
  end

  def person_params
    params.permit(
      :accepted_terms_and_conditions,
      :email,
      :name,
      :password,
      :password_confirmation,
      :country,
      :country_website_id,
      :phone_number
    )
  end

  def notify_person
    offer_expiration =
      if @person.targeted_offers.blank?
        ""
      else
        @person.targeted_offers
               .first.expiration_date_for_person(@person)
               .try(:to_date)
               .try(:to_s, :long) || ""
      end
    deliver_verification_email(@person)
  end

  def password_params
    params.permit(:id, :invite_code, :password, :password_confirmation)
  end

  def handle_failed_verification
    if params[:key] == @person.invite_code && !@person.invite_active?
      deliver_verification_email(@person)
      message = "expired"
    else
      message = "not_recognized"
    end
  end

  def deliver_verification_email(person)
    PersonNotifier.verify(person, { host: person.domain, is_from_social: true }).deliver
  end

  def handle_successful_verification
    @person.verified_at = Time.now
    @person.save
  end

  def compliance_info
    ComplianceInfoForm.new(user).fields.each_with_object([]) do |(key, value), compliance_info|
      compliance_info << {
        field: key,
        info: value
      }
    end
  end
end
