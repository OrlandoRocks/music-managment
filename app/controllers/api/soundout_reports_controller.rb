class Api::SoundoutReportsController < ApplicationController
  def index
    @reports = Tunecore::MusicSearch.soundout_report_search(current_user, params)

    render json: {
      total_count: @reports.total_entries,
      per_page: @reports.per_page,
      total_pages: @reports.total_pages,
      current_page: @reports.current_page,
      soundout_reports: @reports.as_json(root: false)
    }
  end
end
