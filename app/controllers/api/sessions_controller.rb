class Api::SessionsController < Api::BaseController
  before_action :authenticate, raise: false, except: [:fetch_user]
  attr_reader :user

  def create
    session[:person] = user.id
    render json: { status: "success", data: { session_id: session.id } }
  end

  def clear_session
    session[:person] = nil
    reset_session
    session_key = fetch_session_key
    $redis.del(session_key)
    render json: { status: "success", message: ["Session destroyed"] }
  end

  def fetch_user
    session_key = fetch_session_key
    session_data = Marshal.load($redis.get(session_key))
    logged_in_user = Person.find_by(id: session_data["person"]) if session_data["person"]
    render_unauthorized if logged_in_user.nil?

    set_token(logged_in_user)
    render json: logged_in_user, serializer: Api::Social::PersonDetailsSerializer, root: false
  rescue => e
    render_unauthorized
  end

  private

  def authenticate
    authenticate_tc_social_token || render_unauthorized
  end

  def authenticate_tc_social_token
    authenticate_with_http_token do |token, _options|
      decrypted_token = SessionEncryptionEngine.decrypt64(token + "=\n")
      @user = OauthToken.find_by_token(decrypted_token).try(:user)
      user && user.has_tc_social?
    end
  rescue => e
    Tunecore::Airbrake.notify(e)
    false
  end

  def render_unauthorized
    render json: { status: "failure", code: 2000 }
  end

  # [TCS-1627] workaround for fetching session_key
  # session_key is stored differently in production redis server
  def fetch_session_key
    Rails.env.production? ? "0:#{params[:tunecore_id]}" : "session:#{params[:tunecore_id]}"
  end

  def set_token(logged_in_user)
    unless logged_in_user.tc_social_token
      client_application = ClientApplication.find_by(name: "tc_social")
      Oauth2Token.create!(user: logged_in_user, client_application: client_application, scope: nil)
    end
    headers["Authorization"] = "Token token=#{encrypt(logged_in_user.tc_social_token)}"
  end

  def encrypt(token)
    SessionEncryptionEngine.encrypt64(token) if token
  end
end
