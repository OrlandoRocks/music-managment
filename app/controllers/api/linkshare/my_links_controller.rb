class Api::Linkshare::MyLinksController < Api::OauthBaseController
  before_action :load_songs, :load_albums, :fetch_valid_songs

  def create
    render json: { data: serialized_songs, status: "success" }
  end

  private

  def fetch_valid_songs
    @valid_songs = @songs.where(album_id: @albums.ids)
    render json: { status: "failure", message: "No Valid Song ids given for the user" } if @valid_songs.blank?
  end

  def load_songs
    @songs = Song.where(id: params[:songs_ids])
    render json: { status: "failure", message: "No Songs found" } if @songs.blank?
  end

  def load_albums
    @albums = user.albums
    render json: { status: "failure", message: "User has no Albums" } if @albums.blank?
  end

  def serialized_songs
    ActiveModel::ArraySerializer.new(
      @valid_songs,
      each_serializer: Api::Linkshare::SongSerializer
    )
  end
end
