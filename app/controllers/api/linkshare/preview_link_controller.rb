class Api::Linkshare::PreviewLinkController < Api::BaseController
  skip_before_action :verify_api_key
  before_action :load_song, :retrieve, :validate_store

  def index
    urls = Api::Linkshare::StoresService.new(@album_external_service_id, @song_external_service_ids, @song).fetch_urls
    render json: { status: "success", data: { stores: urls, song_data: song_data } }
  end

  private

  def load_song
    @song = Song.find_by(id: params[:song_id])
    @album = @song.album unless @song.nil?
    render json: { status: "failure", message: "No Song found" } if @song.nil? || @album.nil?
  end

  def retrieve
    @song_external_service_ids = @song.external_service_ids.where(
      "id IN (?)",
      params[:store_ids].tr("[]", "").split(",")
    )
    @album_external_service_id = @album.external_service_ids
    render json: {
      status: "failure",
      message: "No Stores found"
    } unless @song_external_service_ids.present? && @album_external_service_id.present?
  end

  def validate_store
    valid_count = @song_external_service_ids.count { |store| store.linkable_id == @song.id }
    render json: { status: "failure", message: "Invalid stores" } if valid_count != @song_external_service_ids.count
  end

  def song_data
    {
      id: @song.id,
      title: @song.name,
      image: (@album.artwork.url(:medium) if @album.artwork && @album.artwork.s3_asset),
      artist_name: @album.artist.name
    }
  end
end
