class Api::Linkshare::SongsController < Api::OauthBaseController
  before_action :load_albums

  def index
    songs = Api::Linkshare::SongsService.new(params, @albums)
                                        .retrieve_songs
                                        .paginate(page: params[:page],
                                                  per_page: params[:per_page])
    render  json: {
      data: serialized_songs(songs),
      status: "success",
      total_pages: songs&.total_pages,
      next_page: songs&.next_page,
      previous_page: songs&.previous_page
    }
  end

  private

  def load_albums
    @albums = user.albums.distributed_albums
    render json: { status: "failure", message: ["api.tc.message.user_no_albums"] } if @albums.blank?
  end

  def serialized_songs(songs)
    ActiveModel::ArraySerializer.new(songs, each_serializer: Api::Linkshare::SongSerializer)
  end
end
