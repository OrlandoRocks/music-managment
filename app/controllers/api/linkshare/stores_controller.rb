class Api::Linkshare::StoresController < Api::OauthBaseController
  before_action :load_song, :validate_song, :fetch_stores, :retrieve

  def index
    urls = Api::Linkshare::StoresService.new(@album, @stores, @song).fetch_urls
    render json: { status: "success", data: urls }
  end

  private

  def retrieve
    @stores = ExternalServiceId.where(linkable: @song)
    @album = ExternalServiceId.where(linkable: @song.album)
    render json: { status: "failure", message: "api.tc.message.no_stores" } if @stores.blank?
  end

  def validate_song
    album = Album.where(id: @song.album_id, person_id: user.id)
    render json: { status: "failure", message: "api.tc.message.not_user_song" } if album.blank?
  end

  def load_song
    @song = Song.find_by(id: params[:song_id])
    render json: { status: "failure", message: "api.tc.message.song_not_found" } if @song.nil?
  end

  def fetch_stores
    Api::Linkshare::FetchStoresService.new(@song, ENV["LINKSHARE_STORES_API"]).fetch_odesli_url
  rescue StandardError => e
    Airbrake.notify("Store Fetching Failure in Stores API. Error: #{e}")
  end
end
