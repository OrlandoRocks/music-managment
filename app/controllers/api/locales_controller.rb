class Api::LocalesController < Api::BaseController
  def available_locales
    ph_mandatory_countries = ENV["PHONE_MANDATORY_COUNTRIES"].tr("[]", "").split(",")
    country_data =
      Country.all.map do |country|
        phnum_mandatory = ph_mandatory_countries.include? country.name
        phone_code = CountryPhoneCode.find_for_iso_code(country.iso_code)&.code
        {
          id: country.id,
          name: country.name,
          iso_code: country.iso_code,
          phone_code: phone_code,
          is_phone_mandatory: phnum_mandatory
        }
      end
    render json: { status: "success", data: country_data }
  end
end
