class Api::SalepointsController < Api::BaseController
  def create
    if params[:iso_codes]
      included_countries = Country.where(iso_code: params[:iso_codes].split(","))
      @album.countries   = included_countries
    end

    if params[:deliver_automator].present?
      deliver_automator = params[:deliver_automator].to_i == 1
      Album::AutomatorService.update_automator(@album, deliver_automator)
    end

    if params[:album][:salepoints]
      params[:album][:salepoints] = transform_salepoints(params[:album][:salepoints])

      Salepoint.transaction do
        # Check if itunes us exists already with a special variable price.  If it does
        # we want to preserve this variable price if upgrading to itunes WW
        itunes_ww = Store.find_by(short_name: "iTunesWW")
        itunes_us = Store.find_by(short_name: "iTunesUS")

        # Check if we're upgrading to itunes ww
        itunes_ww_salepoints = params[:album][:salepoints].select { |salepoint| salepoint.store_id == itunes_ww.id }
        itunes_us_salepoints = @album.salepoints.select { |salepoint| salepoint.store_id == itunes_us.id }
        upgrading = (!itunes_ww_salepoints.empty? && itunes_ww_salepoints.first.new_record? && !itunes_us_salepoints.empty?)

        salepoints = params[:album].delete(:salepoints)
        @album.attributes = params[:album]
        # Existing salepoints minus whats being deleted
        @album.salepoints = salepoints.reject(&:new_record?)

        # Add the new salepoints this way so they are not saved on assignment
        salepoints.each { |salepoint| @album.salepoints << salepoint if salepoint.new_record? }

        # Save all the salepoints
        salepoints.each do |salepoint|
          salepoint.salepointable = @album

          # If we're upgrading to itunes ww we want to preserve the itunes us variable pricing
          if salepoint.store_id == itunes_ww.id && upgrading
            salepoint.variable_price       = itunes_us_salepoints.first.variable_price
            salepoint.track_variable_price = itunes_us_salepoints.first.track_variable_price
          end

          salepoint.save!
        end

        @album.apple_music = params[:apple_music_opt_in].present?
        @album.save!

        if params[:upgrade]
          # send them back to salepoints page so they get the checkout button
          cookies[:in_upgrade] = true
          redirect_to "/albums/#{@album.id}/salepoints"
        else
          flash[:notice] = custom_t("controllers.salepoints.successful_save")
          redirect_to "/albums/#{@album.id}"
        end
      end
    end
  rescue StandardError => e
    @album.valid?

    # need to use existing salepoints, don't query again
    # 04/17 GC - If we query again, whatever the user selected will be lost. However, without reloading
    # the rolled-backed records still contains the id and thus the subsequent update will think that
    # the record that got rolled-back exist
    @salepoints = fix_unsaved_salepoints
    setup_store_group_views
    initialize_index_variables
    render action: :index
  end
end
