class Api::IndexController < Api::BaseController
  include ReferralData::Parsable

  skip_before_action :verify_api_key

  def index
    create_referral_cookies
    render json: { status: "success", data: cookies.permanent }
  end
end
