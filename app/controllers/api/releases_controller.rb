class Api::ReleasesController < Api::BaseController
  before_action :load_release, only: [:show]

  def show
    render json: @release, root: "release"
  end

  private

  def load_release
    unless params[:id]
      render json: { status: "error", message: "id parameter required" }, status: :not_found
      return
    end

    return if @release = Album.find_by(id: params[:id])

    render json: { status: "error", message: "Release with id #{params[:id]} not found" }, status: :not_found
  end
end
