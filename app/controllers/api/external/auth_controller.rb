module Api::External
  class AuthController < Api::V2::BaseController
    before_action :set_session_key
    before_action :set_person_from_session_key

    def get_token
      render json: {
        person_id: @person.id,
        country_website: @person.country_website.country,
        token: generate_jwt(@person)
      }
    end

    private

    def set_session_key
      stored_session = $redis.get(fetch_session_key)

      if stored_session.nil?
        render_invalid_session
        return
      end

      @person_session = Marshal.load(stored_session).with_indifferent_access
    rescue
      render_invalid_session
    end

    def set_person_from_session_key
      @person = Person.find_by(id: @person_session["person"])

      render_invalid_session if @person.blank?
    end

    def fetch_session_key
      Rails.env.production? ? "0:#{params[:tunecore_id]}" : "session:#{params[:tunecore_id]}"
    end

    def render_invalid_session
      render json: { status: "invalid session" }, status: :unauthorized
    end
  end
end
