module Api::External
  class ScrapiController < Api::BaseController
    before_action :ensure_job_id

    def callback
      Scrapi::GetJobWorker.perform_async(callback_params[:jobId])
      render json: { status: :ok }
    end

    private

    def callback_params
      params.permit(:jobId, :analysisId)
    end

    def ensure_job_id
      head :not_found if callback_params[:jobId].blank?
    end
  end
end
