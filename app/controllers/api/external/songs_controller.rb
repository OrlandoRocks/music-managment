class Api::External::SongsController < Api::V2::BaseController
  before_action :authenticate_user!

  def ad_songs
    person = Person.find_by(id: params[:person_id])

    if person
      albums = person.albums.spotify_or_apple.approved.includes(
        :tunecore_upcs,
        :artwork,
        { creatives: :artist, songs: :s3_asset }
      )

      render json: albums, each_serializer: Api::External::AlbumSerializer, root: "albums"
    else
      render json: {}, status: :not_found
    end
  end
end
