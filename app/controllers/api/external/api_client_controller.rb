module Api::External
  class ApiClientController < Api::BaseController
    before_action :set_album

    def get_album_status
      review_audit = @album.review_audits.last
      if review_audit
        Bytedance::ApiClientWorker.perform_async(Bytedance::Requests::ContentReview, review_audit.id, @album.id)
        render json: { status: :ok }
      else
        render json: { error: "Review not started", status: :unprocessable_entity }
      end
    rescue
      render json: { status: 500 }
    end

    private

    def api_params
      params.permit(:source_album_id, :tc_album_id)
    end

    def set_album
      @album = Album.find_by(id: api_params[:tc_album_id])
      render json: { status: :not_found } unless @album
    end
  end
end
