class Api::SocialLoginController < Api::BaseController
  include ExtendAccessToken
  before_action :validate_params, :extend_access_token, :fetch_user

  SOCIAL_PROFILE_FB = "facebook"
  SOCIAL_PROFILE_APPLE = "apple"
  FB_TOKEN_EXPIRY_LIMIT = 58

  def create
    if @person.nil?
      create_person
      if @person.save
        @person.verified_at = Time.now
        @person.clear_invite
        update_external_services_person
        perform_success_actions(is_create = true)
      else
        render json: { status: "failure", message: @person.errors.full_messages }
      end
    else
      update_person
      if @person.save
        perform_success_actions(is_create = false)
      else
        render json: { status: "failure", message: @person.errors.full_messages }
      end
    end
  end

  private

  # email will not be received for apple login(not signup), so check db with
  # the id token(param: access_token) received, find the corresponding email
  # and merge it with param, to pass model validation for email.
  def fetch_user
    case params[:social_profile]
    when SOCIAL_PROFILE_FB
      render json: { status: "failure", message: ["api.tc.message.invalid_email"] } and return if params[:email].nil?

      @person = Person.find_by(email: params[:email])
      @name = fetch_fb_name(params[:access_token])
    when SOCIAL_PROFILE_APPLE
      if params[:email].present?
        @person = Person.find_by(email: params[:email])
        @name = params[:name]
      else
        apple_service_id = ExternalService.find_by(name: SOCIAL_PROFILE_APPLE).id
        external_services_person = ExternalServicesPerson.find_by(
          access_token: params[:access_token],
          external_service_id: apple_service_id
        )
        render json: {
          status: "failure",
          message: ["api.tc.message.invalid_email"]
        } and return if external_services_person.nil?

        @person = Person.find_by(id: external_services_person.person_id)
        params[:email] = @person.email
        @name = @person.name
      end
    end
  end

  def validate_params
    if params[:accepted_terms_and_conditions] != "true"
      render json: { status: "failure", message: ["api.tc.message.accept_tandc"] }
    end
    return if ExternalService.all.map(&:name).include?(params[:social_profile])

    render json: { status: "failure", message: ["api.tc.message.invalid_social_profile"] }
  end

  def extend_access_token
    return unless params[:social_profile] == SOCIAL_PROFILE_FB

    @extended_token = extend_fb_token(params[:access_token])
    render json: { status: "failure", message: ["api.tc.message.invalid_access_token"] } if @extended_token.nil?
  end

  def fetch_fb_name(access_token)
    graph = Koala::Facebook::API.new(access_token)
    graph.get_object("me") { |data| data["name"] }
  rescue Koala::Facebook::APIError
    nil
  end

  def person_params
    @random_password = Faker::Internet.password(
      min_length: 8,
      max_length: 16,
      mix_case: true,
      special_characters: false
    )

    @random_password += Faker::Number.digit.to_s + ["@", "$", "!", "%", "*", "?", "&"].sample(1).join
    {
      email: params[:email],
      password: @random_password,
      password_confirmation: @random_password,
      name: @name,
      accepted_terms_and_conditions: params[:accepted_terms_and_conditions],
      phone_number: params[:phone_number]
    }
  end

  def external_services_person_params
    {
      person_id: @person.id,
      external_service_id: ExternalService.find_by(name: params[:social_profile]).id,
      access_token: @access_token,
      access_token_expiry: @access_token_expiry
    }
  end

  def person_response
    plan_status = ::Social::PlanStatus.for(@person)
    country_id = Country.find_by(name: @person.country)&.id
    {
      person_id: @person.id,
      email: @person.email,
      name: @person.name,
      plan: plan_status.plan,
      balance: @person.balance,
      country_website: COUNTRY_URLS[@person.country_domain],
      plan_expires_at: plan_status.plan_expires_at.to_s,
      grace_period_ends_on: plan_status.grace_period_ends_on.to_s,
      payment_channel: @person.subscription_payment_channel_for("Social"),
      country: country_id
    }
  end

  def create_person
    @person = Person.new(person_params)
    country = Country.find_by(id: params[:country].to_i)&.iso_code
    country_website_id = CountryWebsite.country_id_for(country)
    @person.country = country
    @person.country_website_id = country_website_id
    @person.is_verified = true
    @person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
  end

  def update_person
    @person.update(person_params.except(:password, :password_confirmation))
    update_external_services_person
  end

  def update_external_services_person
    if params[:social_profile] == SOCIAL_PROFILE_FB
      @access_token = @extended_token
      @access_token_expiry = Date.today + FB_TOKEN_EXPIRY_LIMIT.days
    else
      @access_token = params[:access_token]
      @access_token_expiry = nil
    end
    external_service_id = ExternalService.find_by(name: params[:social_profile]).id
    external_services_person = @person.external_services_people.where(external_service_id: external_service_id).last
    if external_services_person.present?
      external_services_person.social_login = true
      external_services_person.update(external_services_person_params)
    else
      esp = ExternalServicesPerson.new(external_services_person_params)
      esp.social_login = true
      esp.save
    end
  end

  def set_token
    unless @person.tc_social_token
      client_application = ClientApplication.find_by(name: "tc_social")
      Oauth2Token.create!(user: @person, client_application: client_application, scope: nil)
    end
    headers["Authorization"] = "Token token=#{encrypt(@person.tc_social_token)}"
  end

  def encrypt(token)
    SessionEncryptionEngine.encrypt64(token) if token
  end

  def perform_success_actions(is_create = true)
    Person.authenticate(email: @person.email, password: @random_password)
    tfa ||= TwoFactorAuth.find_by(person_id: @person.id)
    message = is_create == true ? "api.tc.message.person_created" : "api.tc.message.person_updated"
    @person.create_logged_in_event(request.headers["Client-Remote-IP"], request.user_agent)
    if @person.two_factor_auth.try(:active?)
      # scope "recent_auth_events" is added to TwoFactorAuthEvents model
      recent_auth_events = tfa.two_factor_auth_events.recent_auth_events
      show_person_response(message) and return if recent_auth_events.present?

      data = { is_tfa_enabled: true, person_id: @person.id }
      message = "api.tc.message.tfa_enabled"
      render json: { data: data, status: "success", message: [message] }
    else
      show_person_response(message)
    end
  end

  def show_person_response(message)
    set_token
    render json: { data: person_response, status: "success", message: [message] }
  end
end
