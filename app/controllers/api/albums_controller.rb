class Api::AlbumsController < Api::BaseController
  def create
    @album = Person.find(params[:person_id]).albums.build(album_params)

    if @album.save
      Note.create(
        related: @album,
        note_created_by_id: 0,
        ip_address: request.remote_ip,
        subject: "Album created",
        note: "Album was created by Batch Ingestion."
      )
      render json: @album
    else
      render json: { status: "error", message: error_message }, status: :unprocessable_entity
    end
  end

  private

  def album_params
    params
      .require(:album)
      .permit(
        :golive_date,
        :is_various,
        :label_name,
        :language_code_legacy_support,
        :name,
        :optional_upc_number,
        :orig_release_year,
        :previously_released,
        :primary_genre_id,
        :recording_location,
        :sale_date,
        :secondary_genre_id,
        :timed_release_timing_scenario,
        creatives: [:role, :name]
      )
  end

  def error_message
    "Failed to create the album due to #{@album.errors.full_messages}"
  end
end
