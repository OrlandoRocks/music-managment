class Api::SystemstatusController < Api::BaseController
  def transaction_status
    status = SystemStatus::BraintreeTransactions.do_check
    render json: status.to_json
  end
end
