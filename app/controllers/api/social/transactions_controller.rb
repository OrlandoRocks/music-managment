class Api::Social::TransactionsController < Api::Social::BaseController
  def index
    transaction_fetcher = ::Social::PersonTransactionFetcher.execute(user.id, transaction_params)
    render json: transaction_fetcher, serializer: Api::Social::PersonTransactionFetcherSerializer, root: false
  end

  private

  def transaction_params
    {
      start_date: params[:start_date],
      end_date: params[:end_date],
      page_size: params[:page_size],
      page_num: params[:page_num]
    }
  end
end
