class Api::Social::SessionsController < Api::BaseController
  include ReferralData::Parsable

  skip_before_action :verify_api_key
  before_action :load_person, :load_tfa, :trigger_login_event
  before_action(only: [:create]) { |c| c.build_referral_data_from_cookies(:tcs_signin, true) }
  before_action(only: [:create]) { |c| c.build_referral_data_from_url(:tcs_signin, true) }

  def create
    @auth_request = Api::Social::AuthenticationRequest.new(params)
    render_unauthorized and return unless @auth_request.granted?

    if @person.two_factor_auth.try(:active?)
      # scope "recent_auth_events" is added to TwoFactorAuthEvents model
      recent_auth_events = @tfa.two_factor_auth_events.recent_auth_events
      show_person_response and return if recent_auth_events.present?

      data = { is_tfa_enabled: true, person_id: @person.id }
      message = "Two Factor Authentication enabled"
      render json: { data: data, status: "success", message: [message] }
    elsif params[:captcha_failed]
      time = Time.find_zone("Eastern Time (US & Canada)").now
      ::SocialSessionsMailer.suspicious_login_mailer(@person, params, time).deliver_now
      show_person_response
    else
      show_person_response
    end
  end

  private

  def load_person
    @person ||= Person.find_by(email: params[:email])
  end

  def load_tfa
    @tfa ||= TwoFactorAuth.find_by(person_id: @person.id) if @person.present?
  end

  def show_person_response
    @person.create_logged_in_event(request.headers["Client-Remote-IP"], request.user_agent)
    @person.update(recent_login: Time.zone.now, dormant: 0, last_logged_in_ip: request.remote_ip)
    clear_referral_cookies
    headers["Authorization"] = "Token token=#{encrypt(@auth_request.token)}"
    render json: @auth_request, serializer: Api::Social::AuthenticationRequestSerializer, root: false
  end

  def render_unauthorized
    render json: { status: "failure", code: 2000 }
  end

  def encrypt(token)
    SessionEncryptionEngine.encrypt64(token) if token
  end

  def trigger_login_event
    user = Person.authenticate(params)
    @trigger = user.tc_social_token if user
  end
end
