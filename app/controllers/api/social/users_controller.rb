class Api::Social::UsersController < Api::Social::BaseController
  def show
    plan_status = ::Social::PlanStatus.for(user)
    render json: plan_status, serializer: Api::Social::PlanStatusSerializer, root: false
  end
end
