class Api::Social::PasswordResetController < Api::Social::BaseController
  skip_before_action :authenticate

  def create
    if PasswordResetService.reset(params[:email], :social)
      render json: { status: "success", data: "api.tc.message.pw_reset_success" }
    else
      render json: { status: "failure", code: 2100, messages: ["api.tc.message.pw_reset_failed"] }
    end
  end
end
