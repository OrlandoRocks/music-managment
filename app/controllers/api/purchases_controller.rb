class Api::PurchasesController < Api::OauthBaseController
  before_action :retrieve_purchase

  def show
    render json: { status: :success, data: purchase_data }
  end

  private

  def retrieve_purchase
    @purchase = Purchase.find_by(id: params[:id])
    render json: { status: "failure", message: ["Invalid Purchase"] } if @purchase.nil?
  end

  def purchase_data
    @purchase.attributes.slice(
      "id", "cost_cents", "discount_cents", "discount_reason", "vat_amount_cents", "status"
    ).merge(purchase_tax_info)
  end

  def purchase_tax_info
    @purchase.purchase_tax_information.attributes.slice(
      "customer_location", "vat_registration_number", "tax_type", "tax_rate", "place_of_supply", "error_code"
    )
  end
end
