module Api::Transcoder
  class SoundoutReportsController < Api::V2::BaseController
    def complete_order
      if params[:state] == "COMPLETED"
        Soundout::OrderWorker.perform_async(params[:id])

        render json: { status: :successful }
      else
        render json: { status: :unsuccessful }, status: :unprocessable_entity
      end
    end
  end
end
