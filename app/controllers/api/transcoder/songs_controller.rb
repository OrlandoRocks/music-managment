module Api::Transcoder
  class SongsController < Api::V2::BaseController
    def set_asset_data
      ::AssetsBackfill::Base.new.parse_transcoder_response(params)
      render json: { status: :successful }
    end

    def set_streaming_asset
      if params[:state] == "COMPLETED"
        song = Song.find(params[:songId])

        song.update!(
          {
            s3_streaming_asset: S3Asset.create(
              key: params[:outputs][0][:key],
              bucket: ENV["ASSETS_BACKFILL_DESTINATION_BUCKET"]
            )
          }
        )

        render json: { status: :successful }
      else
        render json: { status: :unsuccessful }, status: :unprocessable_entity
      end
    end
  end
end
