class Api::SubscriptionsController < Api::OauthBaseController
  def index
    subscription_request = ::Subscription::SubscriptionRequest.get(user, params[:subscription_type])
    render json: subscription_request, serializer: Api::Subscription::SubscriptionRequestSerializer, root: false
  end

  def destroy
    subscription_request = ::Subscription::SubscriptionRequest.cancel(
      user,
      params[:subscription_type],
      params[:user_note]
    )
    if subscription_request.success
      render json: subscription_request, serializer: Api::Subscription::SubscriptionRequestSerializer, root: false
    else
      render json: { status: "failure", messages: subscription_request.errors.messages }
    end
  end
end
