class Api::EncumbranceSummariesController < Api::BaseController
  def create
    summary = EncumbranceSummary.create(summary_params)

    if summary.id
      render json: { status: "success", encumbrance_summary_id: summary.id }
    else
      render json: { status: "failure", messages: summary.errors.messages }
    end
  rescue => e
    render json: { status: "error", message: e.message }
  end

  private

  def summary_params
    {
      person_id: params[:person_id],
      reference_id: params[:reference_id],
      reference_source: "LYRIC",
      total_amount: params[:total_amount],
      fee_amount: params[:fee_amount],
      outstanding_amount: params[:total_amount]
    }
  end
end
