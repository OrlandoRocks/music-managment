require "base64"

class Api::Subscription::InAppPurchasesController < Api::Subscription::BaseController
  skip_before_action :authenticate, only: [:update_subscription_status]
  rescue_from ::Subscription::Apple::PurchaseVerifier::VerificationError, with: :render_error_response

  def create
    case params[:platform]
    when IOS
      apple_purchase
    when ANDROID
      android_purchase
    else
      render json: { status: "failure", messages: ["Invalid Platfrom error"] }
    end
  end

  def purchase_token_update
    return unless params[:platform] == ANDROID

    payment_channel_receipt = SubscriptionPurchase.where(person_id: @user.id, payment_channel: "Android")
                                                  .last.payment_channel_receipt
    receipt_data = "#{params[:purchase_token]} #{params[:product_id]}"
    if payment_channel_receipt
      payment_channel_receipt.update(receipt_data: receipt_data)
      render json: { status: "success" }
    else
      render json: { status: "failure", messages: ["No purchase found"] }
    end
  end

  def update_subscription_status
    errors = []
    status = "success"
    begin
      Subscription::Android::HandleAccountStatus.new(decode_params["subscriptionNotification"]).process
    rescue Exception => e
      status = "failure"
      errors = ["Invalid Data"]
      Airbrake.notify(
        e,
        {
          params: params,
          message: "In-app Purchase Subscription Callback API Error"
        }
      )
    end
    render json: { status: status, messages: errors }
  end

  private

  def verify_apple_request
    @purchase_details ||= Subscription::Apple::PurchaseVerifier.execute(params["purchase"]["receipt_data"])
  end

  def build_apple_purchase_params
    @purchase_params ||= Subscription::Apple::ParamsBuilder.build(@purchase_details, user.id)
  end

  def render_error_response(exception)
    render json: { status: "failure", messages: [exception.message] }
  end

  def verify_android_request
    @purchase_details ||= Subscription::Android::PurchaseVerifier.new.purchase_verifier(
      params[:purchase_token],
      params[:product_id]
    )
  end

  def build_android_purchase_params
    @purchase_params ||= Subscription::Android::ParamsBuilder.build(@purchase_details, user.id)
  end

  def apple_purchase
    verify_apple_request
    build_apple_purchase_params
    handler = Subscription::Apple::PurchaseRequest.new(@purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Subscription::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  def android_purchase
    verify_android_request
    build_android_purchase_params
    handler = Subscription::Android::PurchaseRequest.new(@purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Subscription::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  def first_time_purchase
    user.person_subscription_statuses.where(subscription_type: "social").count == 1
  end

  def decode_params
    JSON.parse(Base64.decode64(params[:message][:data]))
  end
end
