class Api::Subscription::PlansController < Api::Subscription::BaseController
  before_action :fetch_rf_plans

  def index
    plan_fetcher = ::Subscription::PlansFetcher.new(user)
    plan_fetcher.fetch_plans
    plan_fetcher.plans = plan_fetcher.plans + @rf_product
    render json: plan_fetcher, serializer: Api::Subscription::PlansSerializer, root: false
  end

  def fetch_rf_plans
    @rf_product = Product.where(
      display_name: ENV.fetch("RF_PRODUCT_NAME", "rf_video_download"),
      country_website_id: user.country_website_id
    )
  end
end
