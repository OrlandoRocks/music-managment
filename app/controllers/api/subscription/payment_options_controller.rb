class Api::Subscription::PaymentOptionsController < Api::Subscription::BaseController
  def index
    payments_fetcher = ::Subscription::PaymentOptionsFetcher.new(user)
    payments_fetcher.fetch_payment_options
    render json: payments_fetcher, serializer: Api::Subscription::PaymentOptionsSerializer, root: false
  end
end
