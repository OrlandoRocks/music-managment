class Api::Subscription::TunecorePurchasesController < Api::Subscription::BaseController
  def create
    handler = ::Subscription::Tunecore::PurchaseRequest.new(purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Subscription::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  private

  def purchase_params
    params[:purchase].merge({ request_ip: request.remote_ip, person_id: user.id })
  end
end
