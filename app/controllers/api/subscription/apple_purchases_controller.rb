class Api::Subscription::ApplePurchasesController < Api::Subscription::BaseController
  rescue_from ::Subscription::Apple::PurchaseVerifier::VerificationError, with: :render_error_response

  def create
    verify_request
    build_purchase_params
    handler = ::Subscription::Apple::PurchaseRequest.new(purchase_params)
    if handler.finalize
      render json: handler, serializer: Api::Subscription::PurchaseHandlerSerializer, root: false
    else
      render json: { status: "failure", messages: handler.errors }
    end
  end

  private

  attr_reader :purchase_details, :purchase_params

  def verify_request
    @purchase_details = ::Subscription::Apple::PurchaseVerifier.execute(params["purchase"]["receipt_data"])
  end

  def build_purchase_params
    @purchase_params = Subscription::Apple::ParamsBuilder.build(purchase_details, user.id)
  end

  def render_error_response(exception)
    render json: { status: "failure", messages: exception.message }
  end
end
