# API for 2FA for TCS
class Api::SocialTwoFactorAuthController < Api::BaseController
  before_action :load_person, :load_tfa
  before_action :validate_source, only: [:generate_auth_code]

  def generate_auth_code
    timeout = ENV["TFA_RESEND_TIMEOUT_IN_SEC"].to_i
    recent_auth_events = @tfa.two_factor_auth_events
                             .where(created_at: timeout.seconds.ago..Time.current,
                                    action: ["sign_in", "front_stage_code_resend"])
    if recent_auth_events.blank?
      if FeatureFlipper.show_feature?(:verify_2fa, @person)
        TwoFactorAuth::ApiClient.new(@person).request_authorization(params[:alternate_method].present?)
      else
        TwoFactorAuth::AuthyApiClient.new(@person).request_authorization(params[:alternate_method].present?)
      end

      data = format_response_data(params, timeout)
      render json: { data: data, status: "success", message: "api.tc.message.auth_code.generated" }
    else
      render json: { status: "failure", message: "api.tc.message.auth_code.one_min_error" }
    end
  end

  def validate_auth_code
    tfa_event = @tfa.two_factor_auth_events
                    .where(id: params[:tfa_event_id])
                    .where.not(action: "complete").last
    render json: {
      status: "failure",
      message: "api.tc.message.auth_code.invalid_event_error"
    } and return if tfa_event.blank?

    tfa_event.update(action: "authentication")
    validation =
      if FeatureFlipper.show_feature?(:verify_2fa, @person)
        TwoFactorAuth::ApiClient.new(@person).submit_auth_code(params[:auth_code])
      else
        TwoFactorAuth::AuthyApiClient.new(@person).submit_auth_code(params[:auth_code])
      end

    if validation.successful?
      tfa_event.update(action: "complete")
      set_token
      serialized_person = Api::Social::PersonDetailsSerializer.new(@person, root: false)
      render json: serialized_person.as_json.merge({ message: "api.tc.message.auth_code.valid_code" })
    else
      render json: { status: "failure", message: "api.tc.message.auth_code.invalid_code" }
    end
  end

  private

  def load_person
    @person ||= Person.find(params[:person_id])
    render json: {
      status: "failure",
      message: "api.tc.message.auth_code.invalid_user"
    } and return if @person.blank?
  end

  def load_tfa
    @tfa ||= TwoFactorAuth.find_by(person_id: @person.id) if @person.present?
    message = "api.tc.message.auth_code.tfa_not_enabled"
    render json: { status: "failure", message: message } and return unless @tfa.try(:active?)
  end

  def validate_source
    is_valid_source = false
    valid_sources = ["tc_social_web", "tc_social_ios", "tc_social_android"]
    is_valid_source = valid_sources.include? params[:source] if params[:source].present?
    render json: {
      status: "failure",
      message: "api.tc.message.auth_code.invalid_source"
    } and return unless is_valid_source
  end

  def set_token
    unless @person.tc_social_token
      client_application = ClientApplication.find_by(name: "tc_social")
      Oauth2Token.create!(user: @person, client_application: client_application, scope: nil)
    end
    headers["Authorization"] = "Token token=#{encrypt(@person.tc_social_token)}"
  end

  def encrypt(token)
    SessionEncryptionEngine.encrypt64(token) if token
  end

  def format_response_data(params, timeout)
    action = (params[:resend] == true) ? "front_stage_code_resend" : "sign_in"
    tfa_event = TwoFactorAuthEvent.create(
      type: "authentication",
      page: params[:source],
      action: action,
      successful: true,
      two_factor_auth_id: @person.two_factor_auth.id
    )

    { tfa_event_id: tfa_event.id, notification_method: @tfa.notification_method, timeout_in_sec: timeout }
  end
end
