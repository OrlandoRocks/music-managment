class Api::V2::RewardSystem::BaseController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_user!

  def authorize_user!
    render json: { status: "unauthorized" }, status: :unauthorized unless current_user.is_reward_admin?
  end
end
