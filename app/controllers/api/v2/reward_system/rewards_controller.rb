class Api::V2::RewardSystem::RewardsController < Api::V2::RewardSystem::BaseController
  before_action :set_reward, only: [:show, :update, :redeem]
  before_action :set_person, only: :redeem
  skip_before_action :authorize_user!, only: [:person_info, :redeem]

  def person_info
    render json: current_user, serializer: Api::V2::RewardSystem::RewardPersonSerializer
  end

  def index
    rewards = Reward.all.order(:id)
    render json: rewards,
           each_serializer: Api::V2::RewardSystem::RewardSerializer,
           scope: { include_associations: true }
  end

  def create
    reward = Reward.new(reward_params)

    if reward.save
      render json: reward
    else
      render json: reward.errors.details, status: :unprocessable_entity
    end
  end

  def show
    render json: @reward, scope: { include_associations: true }
  end

  def update
    if @reward.update(reward_update_params)
      render json: @reward
    else
      render json: @reward.errors.details, status: :unprocessable_entity
    end
  end

  def redeem
    begin
      @reward.redeem!(@person)
    rescue => e
      render json: { error: e.message }, status: :unprocessable_entity
    else
      render json: @person, serializer: Api::V2::RewardSystem::PersonPointsSerializer
    end
  end

  private

  def reward_params
    required_keys = [
      :name,
      :content_type,
      :category,
      :points,
      :is_active
    ]

    permitted_keys = [
      *required_keys,
      :description,
      :link
    ]

    params.require(:reward).permit(*permitted_keys).tap do |obj_params|
      obj_params.require(required_keys)
    end
  end

  def set_reward
    @reward = Reward.find_by(id: params[:id])

    render json: { error: "Invalid Reward" }, status: :not_found if @reward.blank?
  end

  def reward_update_params
    params.require(:reward).permit(
      :name,
      :description,
      :link,
      :points,
      :content_type,
      :category,
      :is_active
    )
  end

  def set_person
    person_id = params.require(:person_id).to_i

    if person_id != current_user.id && !current_user.is_reward_admin?
      render json: { error: "unauthorized" }, status: :unauthorized
      return
    end

    @person = Person.find_by(id: person_id)

    render json: { error: "Invalid Person" }, status: :not_found if @person.blank?
  end
end
