class Api::V2::RewardSystem::CertificationsController < Api::V2::RewardSystem::BaseController
  def index
    certifications = Certification.all.order(:id)
    render json: certifications, each_serializer: Api::V2::RewardSystem::CertificationSerializer
  end

  def show
    certification = Certification.find(params[:id])
    render json: certification
  end

  def create
    certification = Certification.new(certification_params)

    if certification.save
      render json: certification, except: [:tiers]
    else
      render json: certification.errors.details, status: :unprocessable_entity
    end
  end

  def update
    certification = Certification.find(params[:id])

    if certification.update(certification_params)
      render json: certification, except: [:tiers]
    else
      render json: certification.errors.details, status: :unprocessable_entity
    end
  end

  private

  def certification_params
    params.require(:certification).permit(:name, :description, :link, :badge_url, :points, :category, :is_active)
  end
end
