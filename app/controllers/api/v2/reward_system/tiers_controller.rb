class Api::V2::RewardSystem::TiersController < Api::V2::RewardSystem::BaseController
  before_action :set_tier, except: [:index, :create]
  before_action :set_achievement, only: [:add_achievement, :remove_achievement]
  before_action :set_certification, only: [:add_certification, :remove_certification]
  before_action :set_reward, only: [:add_reward, :remove_reward]

  def index
    tiers = Tier.all.order(:hierarchy)
    render json: tiers,
           each_serializer: Api::V2::RewardSystem::TierSerializer,
           scope: { tier_associations: true }
  end

  def show
    render_tier_with_associations
  end

  def create
    tier = Tier.new(tier_params)

    if tier.save
      render json: tier
    else
      render_object_error(tier)
    end
  end

  def update
    if @tier.update(tier_update_params)
      render json: @tier
    else
      render_object_error(@tier)
    end
  end

  def add_achievement
    tier_achievement = @tier
                       .tier_achievements
                       .find_or_initialize_by(
                         achievement_id: @achievement.id
                       )

    if tier_achievement.save
      render_tier_with_associations
    else
      render_object_error(tier_achievement)
    end
  end

  def remove_achievement
    tier_achievement = @tier
                       .tier_achievements
                       .find_by(achievement_id: @achievement.id)

    if tier_achievement.blank? || tier_achievement.destroy
      render_tier_with_associations
    else
      render_object_error(tier_achievement)
    end
  end

  def add_certification
    tier_certification = @tier
                         .tier_certifications
                         .find_or_initialize_by(
                           certification_id: @certification.id
                         )

    if tier_certification.save
      render_tier_with_associations
    else
      render_object_error(tier_certification)
    end
  end

  def remove_certification
    tier_certification = @tier
                         .tier_certifications
                         .find_by(certification_id: @certification.id)

    if tier_certification.blank? || tier_certification.destroy
      render_tier_with_associations
    else
      render_object_error(tier_certification)
    end
  end

  def add_reward
    tier_reward = @tier.tier_rewards.find_or_initialize_by(reward_id: @reward.id)

    if tier_reward.save
      render_tier_with_associations
    else
      render_object_error(tier_reward)
    end
  end

  def remove_reward
    tier_reward = @tier.tier_rewards.find_by(reward_id: @reward.id)

    if tier_reward.blank? || tier_reward.destroy
      render_tier_with_associations
    else
      render_object_error(tier_reward)
    end
  end

  private

  def set_tier
    @tier = Tier.find_by(id: params[:id])

    render json: { error: "Invalid Tier" }, status: :not_found if @tier.blank?
  end

  def set_reward
    @reward = Reward.find_by(id: params.require(:reward_id))

    render json: { error: "Invalid Reward" }, status: :not_found if @reward.blank?
  end

  def tier_params
    required_keys = [
      :name,
      :is_active,
      :is_sub_tier
    ]

    permitted_keys = [
      *required_keys,
      :description,
      :badge_url,
      :parent_id
    ]

    params.require(:tier).permit(*permitted_keys).tap do |obj_params|
      obj_params.require(required_keys)
    end
  end

  def tier_update_params
    params.require(:tier).permit(
      :name,
      :is_active,
      :description,
      :badge_url
    )
  end

  def set_achievement
    @achievement = Achievement.find_by(id: params.require(:achievement_id))

    render json: { error: "Invalid Achievement" }, status: :not_found if @achievement.blank?
  end

  def set_certification
    @certification = Certification.find_by(id: params.require(:certification_id))

    render json: { error: "Invalid Certification" }, status: :not_found if @certification.blank?
  end

  def render_tier_with_associations
    render json: @tier, scope: { tier_associations: true }
  end

  def render_object_error(object)
    render json: object.errors.details, status: :unprocessable_entity
  end
end
