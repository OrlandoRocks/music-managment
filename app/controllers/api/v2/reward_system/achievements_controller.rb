class Api::V2::RewardSystem::AchievementsController < Api::V2::RewardSystem::BaseController
  def index
    achievements = Achievement.all.order(:id)
    render json: achievements
  end

  def show
    render json: Achievement.find(params[:id])
  end

  def create
    achievement = Achievement.new(achievement_params)
    if achievement.save
      render json: achievement, except: [:tiers]
    else
      render json: achievement.errors.details, status: :unprocessable_entity
    end
  end

  def update
    achievement = Achievement.find(params[:id])
    if achievement.update(achievement_params)
      render json: achievement, except: [:tiers]
    else
      render json: achievement.errors.details, status: :unprocessable_entity
    end
  end

  private

  def achievement_params
    params.require(:achievement).permit(:name, :description, :link, :points, :category, :is_active)
  end
end
