class Api::V2::RetoolSessionsController < Api::V2::BaseController
  def create
    person = Person.where(email: params[:email]).first
    if person.present?
      render json: { token: generate_jwt(person) }
    else
      render json: { errors: [{ fieldName: :login, messages: ["invalid email id"] }] },
             status: :unauthorized
    end
  end
end
