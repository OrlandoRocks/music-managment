class Api::V2::BaseController < ActionController::Base
  include OauthPluginSupport
  include TwoFactorAuthable
  rescue_from ActionController::ParameterMissing, with: :handle_parameter_missing

  attr_accessor :current_user, :decoded_payload

  after_action :set_access_control_headers

  skip_before_action :verify_authenticity_token

  STUDIO_JWT_EXPIRY_DURATION_IN_SECONDS = ENV.fetch("STUDIO_JWT_EXPIRY_DURATION_IN_SECONDS", 86_400).to_i

  def cors_preflight
    head(:ok) if request.request_method == "OPTIONS"
  end

  private

  def authenticate_user!
    @decoded_payload = decode_jwt_payload
    person =
      if decoded_payload["id"].present? && decoded_payload["email"].present?
        Person.find_by(id: decoded_payload["id"], email: decoded_payload["email"])
      end

    if person.present?
      @current_user = person
    else
      render json: { status: "unauthorized" }, status: :unauthorized
    end
  end

  def generate_jwt(person, expiry = STUDIO_JWT_EXPIRY_DURATION_IN_SECONDS)
    active_2fa = two_factor_auth(person).try(:active?) || exempt_current_user_from_2fa(person)
    payload = {
      id: person.id,
      email: person.email,
      name: person.name,
      exp: Time.current.to_i + expiry,
      roles: person.roles.pluck(:name),
      force_2fa: force_admin_2fa? ? !active_2fa : false
    }
    private_key = OpenSSL::PKey::RSA.new(File.read(ENV["TC_STUDIO_JWT_SIGNING_PRIVATE_KEY_FILE"]))

    JWT.encode(payload, private_key, "RS256")
  end

  def parse_http_bearer_token
    (request.env["HTTP_AUTHORIZATION"] || "")
      .scan(/Bearer (.*)$/)
      .flatten
      .last
  end

  def decode_jwt_payload
    payload = {}
    token = parse_http_bearer_token
    public_key = OpenSSL::PKey::RSA.new(File.read(ENV["TC_STUDIO_JWT_SIGNING_PUBLIC_KEY_FILE"]))

    payload, _headers = JWT.decode token, public_key, true, { algorithm: "RS256" } if token.present?

    payload
  rescue JWT::ExpiredSignature, JWT::DecodeError
    payload
  end

  def set_access_control_headers
    headers["Access-Control-Allow-Origin"]   = ENV["CRT_ACCESS_CONTROL_ALLOW_ORIGIN"]
    headers["Access-Control-Request-Method"] = "GET, PATCH, PUT, POST, DELETE, OPTIONS"
    headers["Access-Control-Allow-Headers"]  = "Origin, Content-Type, X-Auth-Token, Authorization"
  end

  def authorize_content_review_users!
    content_review_roles = ClientApplication.includes(:roles).find_by(name: "content_review").roles.map(&:name)
    return if decoded_payload.fetch("roles", []).any? { |r| content_review_roles.include?(r) }

    render json: { status: "unauthorized" }, status: :unauthorized
  end

  def authorize_refund_users!
    return if decoded_payload.fetch("roles", []).any? { |role| Refund::ADMIN_ROLES.include?(role) }

    render json: { status: "unauthorized" }, status: :unauthorized
  end

  def authorize_admin_user!
    return if decoded_payload.fetch("roles", []).any? { |role| role == "Admin" }

    render json: { status: "unauthorized" }, status: :unauthorized
  end

  def reformat_errors(errors, error_attributes)
    formatted_errors = Hash.new([])

    errors.each do |attr, message|
      if error_attributes.include?(attr)
        formatted_errors[attr] = message
      else
        formatted_errors[:base] += message
      end
    end

    formatted_errors.map do |key, value|
      formatted_errors[key] = value.join(", ")
    end

    formatted_errors
  end

  def handle_parameter_missing(exception)
    render json: {
      error: exception.message,
      error_key: "params_missing"
    },
           status: :bad_request
  end

  def force_admin_2fa?
    !!ActiveModel::Type::Boolean.new.cast(ENV.fetch("FORCE_ADMIN_2FA", "false"))
  end

  def exempt_current_user_from_2fa(person)
    FeatureFlipper.show_feature?(:exempt_from_2fa, person)
  end
end
