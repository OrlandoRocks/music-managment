class Api::V2::PersonPlanController < Api::V2::BaseController
  before_action :authenticate_user!

  def show
    person = Person.find(params.require(:id))
    plan = person.person_plan
    if plan
      render json: Api::V2::PersonPlanSerializer.new(plan), root: false
    else
      render json: { error: "No plan found" }, status: :not_found
    end
  end
end
