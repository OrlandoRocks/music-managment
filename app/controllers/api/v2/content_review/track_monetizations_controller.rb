class Api::V2::ContentReview::TrackMonetizationsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  def block
    @songs = Song.where(id: params.require(:song_ids), album_id: params[:album_id])
    @songs.each do |song|
      unless TrackMonetizationBlocker.exists?(song_id: song.id, store_id: store.id)
        TrackMonetizationBlocker.create(song_id: song.id, store_id: store.id)
        create_blocker_note(song, store)
      end
    end

    render json: serialized_songs, status: :ok
  rescue => e
    render json: { base: "An error occured when trying to block tracks: #{e}" }, status: :bad_request
  end

  private

  def store
    @store ||= Store.find(params.require(:store_id))
  end

  def serialized_songs
    ActiveModel::ArraySerializer.new(
      @songs.reload,
      each_serializer: Api::V2::ContentReview::SongSerializer
    ).to_json(root: false)
  end

  def create_blocker_note(song, store)
    Note.create(
      related: song.album,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Blocked Track Monetization",
      note: "Song isrc: #{song.isrc} for store, #{store.name}, blocked"
    )
  end
end
