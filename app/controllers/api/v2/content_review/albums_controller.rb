class Api::V2::ContentReview::AlbumsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  ERRORED_UPDATE_ATTRIBUTES = [
    :name,
    :label,
    :primary_genre,
    :secondary_genre,
    :language_code,
    :golive_date,
    :timed_release_timing_scenario
  ]

  def index
    albums = Album
             .send_chain(crt_list_view_queries)
             .paginate(page: page, per_page: limit, total_entries: total_entries)

    render json: {
      albums: serialized_albums(albums),
      total_entries: albums.total_entries
    }.to_json(root: false)
  end

  def show
    @album = Album.first
    render json: @album
  end

  def album_for_review
    album = params[:id].present? ? Album.finalized.find_by(id: params[:id]) : current_user.pop_album_for_review
    if album
      album.start_review(person_id: current_user.id) unless album.nil?
      render json: Api::V2::ContentReview::AlbumSerializer.new(album)
    else
      render json: { error: "This album is not finalized and not available for CRT review" }, status: :not_found
    end
  end

  def needed_review_count
    count = Album.needs_review.count
    render json: { count: count }
  end

  def update
    album = Album.find_by(id: params[:id])
    language_code = LanguageCode.find_by(id: album_params[:metadata_language_code_id])&.code
    album.assign_attributes(updated_album_params(album, album_params))
    album.golive_date = album_params["golive_date"].to_time.in_time_zone(SERVER_TIMEZONE) if album_params["golive_date"]
    album.language_code = language_code if language_code
    note = "#{current_user.name} updated #{album.changed.join(', ')}"
    album.release_date_changes(current_user, "golive_date", "Customer Release Date")

    if album.save
      crt_update_note(album, note)
      album.add_note_for_release_date_change(current_user, "golive_date")
      render json: Api::V2::ContentReview::AlbumSerializer.new(album)
    else
      render json: reformat_errors(album.errors.messages, ERRORED_UPDATE_ATTRIBUTES), status: :unprocessable_entity
    end
  end

  def crt_update_note(album, note)
    album.notes.create!(
      note_created_by_id: current_user.id,
      subject: "CRT album update".freeze,
      note: note
    )
  end

  private

  def serialized_albums(albums)
    ActiveModel::ArraySerializer.new(
      albums,
      each_serializer: Api::V2::ContentReview::AlbumListSerializer
    )
  end

  def album_params
    params
      .require(:album)
      .permit(:name,
              :label_name,
              :metadata_language_code_id,
              :primary_genre_id,
              :secondary_genre_id,
              :golive_date,
              :timed_release_timing_scenario,
              :allow_different_format,
              :is_various,
              creatives: [:name, :role])
  end

  def search_params
    return {} if params[:search_params].blank?

    params
      .require(:search_params)
      .permit(
        :search_by,
        :search_term,
        :legal_review_state,
        :language_code,
        :country_website_id,
        :range_start_date,
        :range_end_date,
        :date_range_field
      )
      .delete_if { |_, v| v.blank? }
      .to_h
      .symbolize_keys
  end

  def page
    params.permit(:page).fetch(:page, 1).to_i
  end

  def limit
    params.permit(:limit).fetch(:limit, 500)
  end

  def total_entries
    params.permit(:total_entries).fetch(:total_entries, nil)
  end

  def search_by_field
    search_params.fetch(:search_by, nil)
  end

  def search_term
    search_params.fetch(:search_term, nil)
  end

  def legal_review_state
    search_params.fetch(:legal_review_state, nil)
  end

  def language_code
    search_params.fetch(:language_code, nil)
  end

  def country_website_id
    search_params.fetch(:country_website_id, nil)
  end

  def date_range_field
    search_params.fetch(:date_range_field, nil)
  end

  def date_ranges
    return {} if search_params.slice(:range_start_date, :range_end_date).empty?

    { start_date: search_params.fetch(:range_start_date), end_date: search_params.fetch(:range_end_date) }
  end

  def sort_by
    params.permit(:sort_by).fetch(:sort_by, "albums.finalized_at")
  end

  def sort_order
    params.permit(:sort_order).fetch(:sort_order, "asc")
  end

  def updated_album_params(album, album_params)
    if album.album_type == "Single"
      album_params.reject! { |k, _v| k == "creatives" }
    else
      album_params
    end
  end

  # We're dynamically calling the search_by query that gets
  # the search_by field from a dropdown menu on the crt list view frontend.
  # The list of search_by methods that could get invoked are:
  #
  #   search_by_album_id, search_by_album_name, search_by_artist_name,
  #   search_by_person_id, search_by_person_name

  def crt_list_view_queries
    [].tap do |arr|
      arr << [:crt_list_view_query, sort_by, sort_order, legal_review_state]
      arr << [:search_by_legal_review_state, legal_review_state] if legal_review_state.present?
      arr << [:search_by_language_code, language_code] if language_code.present?
      arr << [:search_by_country_website_id, country_website_id] if country_website_id.present?
      arr << ["search_by_#{search_by_field}".to_sym, search_term] if search_term.present? && search_by_field.present?
      arr << ["search_by_#{date_range_field}".to_sym, date_ranges] if date_range_field.present? && date_ranges.present?
    end
  end
end
