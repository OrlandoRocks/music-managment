class Api::V2::ContentReview::ReviewAuditsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  def create
    review_audit = ReviewAudit.new(review_audit_params)
    if review_audit_params[:album_id].present? && review_audit.save
      render json: Api::V2::ContentReview::ReviewAuditSerializer.new(review_audit.reload, root: false), status: :created
    else
      render json: review_audit.errors, status: :unprocessable_entity
    end
  end

  private

  def review_audit_params
    params.permit(:album_id, :event, :note, review_reason_ids: []).merge(person: current_user)
  end
end
