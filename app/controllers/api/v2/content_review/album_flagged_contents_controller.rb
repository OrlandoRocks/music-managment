class Api::V2::ContentReview::AlbumFlaggedContentsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  def expand
    @album_flagged_content = AlbumFlaggedContent.find_by(id: params[:album_flagged_content_id])
    @scrapi_jobs_details = ScrapiJobDetail.where(scrapi_job_id: params[:scrapi_job_ids].split(","))
    render json: flagged_content_data
  rescue ActiveRecord::RecordNotFound
    render json: { message: "Not Found" }, status: :not_found
  end

  private

  def flagged_content_data
    {
      expanded_scrapi_jobs_details: ActiveModel::ArraySerializer.new(@scrapi_jobs_details, each_serializer: Api::V2::ContentReview::ScrapiJobDetailSerializer),
      expanded_flagged_content: @album_flagged_content ? Api::V2::ContentReview::ExpandedAlbumFlaggedContentSerializer.new(@album_flagged_content).flagged_content : []
    }.to_json(root: false)
  end
end
