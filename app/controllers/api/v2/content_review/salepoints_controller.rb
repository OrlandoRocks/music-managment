class Api::V2::ContentReview::SalepointsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!
  before_action :load_salepoint

  def block_salepoint
    if @salepoint.block!(actor: "TuneCore Admin: #{current_user.id}", message: "blocking_distribution")
      create_note("blocked")

      render json: Api::V2::ContentReview::SalepointSerializer.new(@salepoint.reload, root: false)
    else
      render json: { base: "Unable to block #{@salepoint.store.name}" }, status: :unprocessable_entity
    end
  end

  def unblock_salepoint
    if @salepoint.unblock!(actor: "TuneCore Admin: #{current_user.id}", message: "unblocking_distribution")
      create_note("unblocked")

      render json: Api::V2::ContentReview::SalepointSerializer.new(@salepoint.reload, root: false)
    else
      render json: { base: "Unable to unblock #{@salepoint.store.name}" }, status: :unprocessable_entity
    end
  end

  private

  def load_salepoint
    @salepoint = Album.find(params[:album_id]).salepoints.joins(:store).find(params[:salepoint_id])
  end

  def create_note(action)
    Note.create(
      related: @salepoint.salepointable,
      note_created_by: current_user,
      subject: "#{action.capitalize} Salepoint",
      note: "Salepoint id: #{@salepoint.id} for store, #{@salepoint.store.name}, #{action}",
      ip_address: request.remote_ip
    )
  end
end
