class Api::V2::ContentReview::SongsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  def update
    song = Song.find(params[:id])
    song_data_form = SongDataForm.new(
      song_params: params[:song_attributes].merge(album_id: song.album_id),
      person_id: song.album.person_id
    )

    if song_data_form.save && song.reload
      song.album.save if song.album.single? || song.album.ringtone? # Moves creatives to album level
      render json: song, serializer: Api::V2::ContentReview::SongSerializer, root: false
    else
      render json: song_data_form.errors.messages, status: :unprocessable_entity
    end
  end

  def retry_audio_fingerprint
    song = Song.find(params[:song_id])
    Sidekiq.logger.info "Sidekiq Job initiated CreateJobWorker from CRT: #{song.id}"
    Scrapi::CreateJobWorker.perform_async(song.id)
    render json: song, status: :ok
  rescue ActiveRecord::RecordNotFound
    render json: { message: "Not Found" }, status: :not_found
  end
end
