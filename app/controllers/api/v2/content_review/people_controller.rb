class Api::V2::ContentReview::PeopleController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_admin_user!

  def plan_downgrade_eligibility
    person = Person.find_by(id: params[:person_id])
    if person && person.plan
      render json: Plans::PlanEligibilityService.lower_plans_by_downgrade_status(person), status: :ok
    elsif person.blank?
      render json: { error: "Person #{params[:person_id]} not found" }, status: :not_found
    elsif person.plan.blank?
      render json: { error: "PersonPlan for user_id #{params[:person_id]} not found" }, status: :not_found
    end
  end
end
