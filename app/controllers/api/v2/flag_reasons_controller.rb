class Api::V2::FlagReasonsController < Api::V2::BaseController
  def index
    flag_reasons = FlagReason
                   .suspicious_flag_reasons
                   .order(:reason)
                   .as_json(only: [:id, :reason], root: false)

    render json: flag_reasons, root: false
  end
end
