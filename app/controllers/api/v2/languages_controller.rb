class Api::V2::LanguagesController < Api::V2::BaseController
  def index
    languages = LanguageCode
                .available_album_languages
                .order(:description)
                .as_json(only: [:id, :code, :description], root: false)
    render json: languages, root: false
  end
end
