# frozen_string_literal: true

class Api::V2::RefundsController < Api::V2::BaseController
  before_action :authenticate_user!, :authorize_refund_users!, :validate_new_refunds_feature
  before_action :set_invoice, only: [:new]
  around_action :set_admin_locale, only: [:new]

  def list_transactions
    invoices = AutoRefunds::TransactionsService.new(params).invoices_for_refund
    if invoices.any?
      render json: {
        status: "success",
        transactions: build_transactions(invoices),
        total_pages: invoices.total_pages,
        total_entries: invoices.total_entries
      }
    else
      render json: {
        status: "failure",
        message: "No invoices found"
      }
    end
  end

  def new
    if @invoice
      render json: {
        status: "success",
        invoice: invoice_refund_info,
        line_items: line_items_refund_info,
        has_other_plans: other_plan_purchases?
      }
    else
      render json: {
        status: "failure",
        message: "invoice not found"
      }
    end
  end

  def create
    refund_service = AutoRefundService.new(
      current_user,
      refund_params.to_h.deep_symbolize_keys
    )
    refund_service.process
    if refund_service.success?
      render json: {
        refund_id: refund_service.refund.id,
        refund_status: refund_service.refund.status,
        status: "success",
        message: "refund created"
      }
    else
      render json: {
        status: "failure",
        message: refund_service.error
      }
    end
  end

  def refund_reasons
    render json: {
      reasons: RefundReason.visible.map { |reason| reason.slice(:id, :reason) }
    }
  end

  private

  def other_plan_purchases?
    @invoice.other_plan_purchases?
  end

  def validate_new_refunds_feature
    return if current_user.new_refunds_feature_on?

    render json: { status: "Refunds feature not enabled" }, status: :unauthorized
  end

  def set_invoice
    @invoice = Invoice.find_by(id: params[:id])
  end

  def build_transactions(invoices)
    transactions = []
    invoices.each do |invoice|
      transaction = invoice.slice(:id, :settled_at, :final_settlement_amount_cents, :currency)
      transaction.tap do |txn|
        txn[:country_website_id] = invoice.website_id
        txn[:name] = invoice.name
        txn[:refunded_amount_cents] = invoice.refunded_amount_cents
        txn[:refund_disable_info] = refund_disable_info(invoice)
      end
      transactions << transaction
    end
    transactions
  end

  def invoice_refund_info
    {
      invoice_amount: @invoice.final_settlement_amount_cents,
      refunded_amount: @invoice.refunded_amount_cents,
      tax_rate: @invoice.tax_rate,
      country_website: @invoice.country_website.name,
      exchange_rate: @invoice.pegged_rate
    }
  end

  def line_items_refund_info
    line_items = []
    @invoice.purchases.each do |purchase|
      item = {}
      product = purchase.product
      item[:id] = purchase.id
      item[:product_name] = "#{product.name} (#{product.display_name})"
      item[:total_amount] = purchase.purchase_total_cents
      item[:refunded_amount] = purchase.refunded_amount_cents
      item[:tax_rate] = purchase.purchase_tax_information&.tax_rate
      line_items << item
    end
    line_items
  end

  def refund_params
    params.permit(
      :invoice_id,
      :refund_level,
      :refund_type,
      :request_amount,
      :tax_inclusive,
      :refund_reason_id,
      :admin_note,
      refund_items: [
        :purchase_id,
        :refund_type,
        :request_amount,
        :tax_inclusive,
      ]
    ).tap do |params|
      params[:refunded_by_id] = current_user.id
    end
  end

  def set_admin_locale
    I18n.with_locale(:en) { yield }
  end

  def refund_disable_info(invoice)
    return {} unless invoice.refunds.pending.any?

    {
      disabled: true,
      reason: "Pending"
    }
  end
end
