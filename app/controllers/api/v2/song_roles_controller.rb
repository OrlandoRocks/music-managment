class Api::V2::SongRolesController < Api::V2::BaseController
  def index
    render json: SongRole.all.as_json(root: false), root: false
  end
end
