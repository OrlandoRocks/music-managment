class Api::V2::PaymentsOsWebhooksController < Api::V2::BaseController
  include PaymentFinalizable
  before_action :verify_request

  def payments
    transaction = PaymentsOSTransaction.find_by(
      charge_id: transaction_id,
      payment_id: params[:payment_id]
    )

    if transaction.blank? || transaction.payment_completed?
      render(json: { error: "Transaction has either been processed or not found" }, status: :not_found)
    else
      if transaction_status == "succeed"
        transaction.succeeded
        successful_purchase_after_effects(transaction.invoice, true)
      else
        transaction.failed
        failed_purchase_after_effects_for_hosted_checkout(transaction.invoice) if transaction.invoice.present?
      end
      render json: { info: "Transaction Updated Successfully" }, status: :ok
    end
  end

  def refunds
    transaction = PaymentsOSTransaction.find_by(refund_id: transaction_id)
    if transaction.present? && transaction.refund_modifiable? &&
       transaction.update(refund_status: transaction_status)

      render json: { info: "Transaction Updated Successfully" }, status: :ok
    else
      render json: { error: "Transaction Not Found Or Has Already Been Processed" }, status: :not_found
    end
  end

  private

  def transaction_id
    params.dig(:data, :id)
  end

  def transaction_status
    params.dig(:data, :result, :status).to_s.downcase
  end

  def verify_request
    received_auth_signature = request.headers["signature"]
    verified = ActiveSupport::SecurityUtils.secure_compare(received_auth_signature.to_s, "sig1=#{generate_signature}")
    render json: { error: "Data Authenticity Verification Failed" }, status: :forbidden unless verified
  end

  def generate_signature
    encryption_key = ENV["PAYMENTS_OS_PAYU_PRIVATE_KEY"]
    digest = OpenSSL::Digest.new("SHA256")
    hmac = OpenSSL::HMAC.new(encryption_key, digest)
    hmac << signature_data
    hmac.hexdigest
  end

  def signature_data
    [
      request.headers["event-type"],
      params[:id],
      params[:account_id],
      params[:payment_id],
      params[:created],
      ENV["PAYMENTS_OS_PAYU_APP_ID"],
      params.dig(:data, :id),
      params.dig(:data, :result, :status),
      params.dig(:data, :result, :category),
      params.dig(:data, :result, :sub_category),
      params.dig(:data, :provider_data, :response_code),
      params.dig(:data, :reconciliation_id),
      params.dig(:data, :amount),
      params.dig(:data, :currency)
    ].join ","
  end
end
