class Api::V2::PeopleController < Api::V2::BaseController
  before_action :authenticate_user!

  def show
    person_id = params.require(:id)
    person = Person.find(person_id)

    if person
      render json: Api::V2::PersonSerializer.new(person), root: false
    else
      render json: { error: "Invalid Person" }, status: :not_found
    end
  end

  def current_user_from_token
    render json: Api::V2::PersonSerializer.new(current_user)
  end

  def fetch_feature_flags_status
    render json: {
      status: :success,
      feature_flags: build_feature_flags_status
    }
  end

  private

  def build_feature_flags_status
    params[:feature_flags].split(",").map do |flag|
      {
        flag: flag,
        enabled: FeatureFlipper.show_feature?(flag, current_user)
      }
    end
  end
end
