class Api::V2::CountryWebsitesController < Api::V2::BaseController
  before_action :authenticate_user!

  def index
    render json: CountryWebsite.all.as_json(only: [:id, :country], root: false), root: false
  end
end
