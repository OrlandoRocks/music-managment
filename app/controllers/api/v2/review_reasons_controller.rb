class Api::V2::ReviewReasonsController < Api::V2::BaseController
  def index
    render json: ReviewReason.all,
           each_serializer: Api::V2::ContentReview::ReviewReasonSerializer,
           root: false
  end
end
