class Api::V2::PlansController < Api::V2::BaseController
  before_action :authenticate_user!

  def index
    plans = Plan.order({ id: :desc }).all.as_json(only: [:id, :name], root: false)
    render json: plans, root: false
  end
end
