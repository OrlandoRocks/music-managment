# frozen_string_literal: true

# tc-web-react REST resources
class Api::V2::Web::RefreshTokensController < ActionController::Base
  include V2::JwtHelper

  protect_from_forgery with: :null_session

  def create
    return head :unauthorized if saved_token.blank?

    result = V2::Authentication::RefreshTokenService.refresh(request)

    render json: { nonce: result[:nonce], token: result[:jwt] }
  end
end
