class Api::V2::NavDataController < Api::V2::BaseController
  include AccountSystem

  include ApplicationHelper
  include CountryDomainHelper
  include PublishingAdministrationHelper

  before_action :authenticate_user!

  def index
    set_locale

    render json: v2_nav_data
  end

  private

  def set_locale
    I18n.locale =
      if valid_locale?
        locale
      else
        fallback_locale
      end
  end

  def valid_locale?
    I18n.available_locales.include?(locale)
  end

  def fallback_locale
    stored_preference = current_user.country_website_language&.yml_languages

    stored_preference || TLD_DEFAULT_LOCALE_MAP.fetch(tld, USA_LOCALE)
  end

  #
  # params
  #

  def locale
    nav_params[:tc_lang]&.to_sym
  end

  def tld
    nav_params[:tld]
  end

  def nav_params
    params.permit(:tc_lang, :tld)
  end
end
