class Api::V2::FraudPrevention::CopyrightClaimsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!
  before_action :set_asset, only: [:create, :update]

  def index
    search_value, search_term = search_params if params[:search_value].present?
    @claims =
      if search_value
        CopyrightClaim.public_send("search_by_#{search_term}".to_sym, search_value).paginate(
          page: params[:page],
          per_page: params[:limit]
        )
      else
        CopyrightClaim.active.paginate(page: params[:page], per_page: params[:limit])
      end
    render json: serialized_claims, status: :ok
  end

  # To be cleaned up [STU-492] - Copyright claims with sorting for retool
  def claims_with_sorting
    search_value, search_term = search_params if params[:search_value].present?
    @claims =
      if search_value
        CopyrightClaim.left_outer_joins(:store).public_send("search_by_#{search_term}".to_sym, search_value)
                      .order(sort_order_params)
      else
        CopyrightClaim.left_outer_joins(:store).active
                      .order(sort_order_params)
      end
    @total_claim_count = @claims.count
    @claims = @claims.paginate(page: params[:page], per_page: params[:limit])
    render json: { copyrightClaims: list_serialized_claims, count: @total_claim_count }, status: :ok
  end

  def total_claim_count
    count = CopyrightClaim.active.count
    render json: { count: count }
  end

  def create
    copyright_claim = CopyrightClaim.new(copyright_claim_params)
    copyright_claim.asset = @asset
    copyright_claim.person = @asset.person
    copyright_claim.admin_id = current_user.id
    if copyright_claim.save
      create_claimant_email(copyright_claim, params[:copyright_claim][:claimant_email]) unless copyright_claim.store_id
      create_claim_note(copyright_claim, params[:copyright_claim][:note], "Created")
      render json: Api::V2::FraudPrevention::CopyrightClaimSerializer.new(copyright_claim.reload, root: false),
             status: :created
    else
      render json: copyright_claim.errors, status: :unprocessable_entity
    end
  end

  def update
    copyright_claim = CopyrightClaim.find_by(id: params[:id])

    copyright_claim.update(copyright_claim_params)
    create_claim_note(copyright_claim, params[:copyright_claim][:note], "Updated")
    render json: Api::V2::FraudPrevention::CopyrightClaimSerializer.new(copyright_claim.reload, root: false),
           status: :ok
  rescue
    render json: copyright_claim.errors, status: :unprocessable_entity
  end

  def deactivate
    copyright_claim = CopyrightClaim.find_by(id: params[:id])

    copyright_claim.deactivate!
    create_claim_note(copyright_claim, params[:copyright_claim][:note], "Deactivated")
    render json: Api::V2::FraudPrevention::CopyrightClaimSerializer.new(copyright_claim.reload, root: false),
           status: :ok
  rescue
    render json: copyright_claim.errors, status: :unprocessable_entity
  end

  private

  def sort_order_params
    sort_by = params[:sort_by] || "copyright_claims.created_at"
    sort_type = params[:sort_type] || "desc"
    "#{sort_by} #{sort_type}"
  end

  def list_serialized_claims
    ActiveModel::ArraySerializer.new(
      @claims,
      each_serializer: Api::V2::FraudPrevention::CopyrightClaimSerializer
    )
  end

  def serialized_claims
    ActiveModel::ArraySerializer.new(
      @claims,
      each_serializer: Api::V2::FraudPrevention::CopyrightClaimSerializer
    ).to_json(root: false)
  end

  def copyright_claim_params
    params.require(:copyright_claim).permit(
      :internal_claim_id,
      :fraud_type,
      :hold_amount,
      :store_id,
      :compute_hold_amount,
    )
  end

  def set_asset
    asset = params[:copyright_claim][:asset]
    @asset =
      if params[:copyright_claim][:asset_type] == "ISRC"
        Song.find_isrc(asset)
      else
        Album.find_by_upc(asset)
      end
    render json: { error: "Asset is not found" }, status: :not_found if @asset.blank?
  end

  def create_claim_note(copyright_claim, note, type)
    Note.create(
      related: copyright_claim,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Copyright Claim #{type}",
      note: note
    )
  end

  def search_params
    if params[:search_field].casecmp("asset_upc_isrc").zero?
      search_term = "asset"
      search_value = Album.find_by_upc(params[:search_value])
      search_value ||= Song.find_isrc(params[:search_value])
      search_value ||= "asset-not-found"
    else
      search_term = params[:search_field]
      search_value = params[:search_value]
    end
    return search_value, search_term
  end

  def create_claimant_email(copyright_claim, claimant_emails)
    claimant_emails.split(",").each do |email|
      copyright_claim.copyright_claimants.create(email: email)
    end
  end
end
