class Api::V2::FraudPrevention::CopyrightHoldsSummaryController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_content_review_users!

  def show
    person_id = params.require("id")
    claims = CopyrightClaim.where(person_id: person_id)

    render json: claims, each_serializer: Api::V2::FraudPrevention::CopyrightClaimSerializer, root: false
  end
end
