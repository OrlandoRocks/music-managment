class Api::V2::DowngradeRequestsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :authorize_admin_user!

  def show
    person = Person.find_by(id: params.require(:id))
    if person
      render json: Api::V2::PlanDowngradeRequestSerializer.new(person), root: false
    else
      render json: { status: "error", message: "Person not found" }, status: :not_found
    end
  end

  def create
    plan_downgrade_request = Plans::DowngradeRequestCreationService.call(downgrade_request_params)

    if plan_downgrade_request.errors.empty?
      render json: { status: "success" }, status: :ok
    else
      render json: plan_downgrade_request.errors.messages, status: :unprocessable_entity
    end
  end

  def cancel
    plan_downgrade_request = PlanDowngradeRequest.find_by(id: params[:id])

    render json: {
      status: "error",
      message: "Downgrade Request not found"
    }, status: :not_found and return if plan_downgrade_request.nil?

    render json: {
      status: "error",
      message: "Downgrade Request does not match person"
    }, status: :bad_request and return if plan_downgrade_request.person_id != downgrade_request_params[:person_id]

    plan_downgrade_request.cancel
    if plan_downgrade_request.errors.empty?
      render json: { status: "success" }, status: :ok
    else
      render json: plan_downgrade_request.errors.messages, status: :unprocessable_entity
    end
  end

  private

  def downgrade_request_params
    params.permit(:person_id, :requested_plan_id, :reason_id, :downgrade_other_reason)
  end
end
