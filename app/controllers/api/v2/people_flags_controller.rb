class Api::V2::PeopleFlagsController < Api::V2::BaseController
  before_action :authenticate_user!
  before_action :get_asset_person_id, only: [:create]

  def index
    people = Api::PeopleFlags::SearchService.new(
      person_ids: search_params[:person_ids],
      flag_name: search_params[:flag_name],
      limit: search_params[:limit] || 25,
      offset: search_params[:offset] || 0
    ).search

    render json: people, each_serializer: Api::V2::PersonSerializer, root: false
  end

  def create
    flag_accounts = Api::PeopleFlags::MassFlagForm.new(create_people_flags_params)
    flag_accounts.save

    if flag_accounts.is_valid?
      render json: flag_accounts.valid_people, status: :ok
    else
      render json: flag_accounts.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    flag_accounts = Api::PeopleFlags::MassUnflagForm.new(destroy_people_flags_params)
    flag_accounts.save

    if flag_accounts.has_invalid_people?
      render json: flag_accounts.errors.messages, status: :unprocessable_entity
    else
      render json: flag_accounts.valid_people, status: :ok
    end
  end

  def suspicious_people_count
    total = Person.where(status: "suspicious").count
    render json: { count: total }, status: :ok
  end

  private

  def search_params
    params.permit(:person_ids, :flag_name, :limit, :offset)
  end

  def create_people_flags_params
    params
      .permit(
        :flag_reason,
        :flag_name,
        :takedown,
        :note,
        :block_from_distribution
      ).merge(
        admin_id: current_user.id,
        ip_address: request.remote_ip,
        person_ids: @person_id.presence || params[:person_ids],
        from_claims: @person_id.present?
      )
  end

  def destroy_people_flags_params
    params
      .permit(
        :person_ids,
        :flag_name,
        :untakedown,
        :note,
        :unblock_from_distribution
      ).merge(
        admin_id: current_user.id,
        ip_address: request.remote_ip,
      )
  end

  def get_asset_person_id
    return unless params[:asset]

    asset_id = params[:asset]
    asset =
      if params[:asset_type] == "ISRC"
        Song.find_isrc(asset_id)
      else
        Album.find_by_upc(asset_id) # rubocop:disable Rails/DynamicFindBy
      end
    @person_id = asset.person&.id.to_s if asset
  end
end
