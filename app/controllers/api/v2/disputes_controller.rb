# frozen_string_literal: true

class Api::V2::DisputesController < Api::V2::BaseController
  before_action :authenticate_user!,
                :authorize_refund_users!,
                :validate_chargebacks_feature_flag!
  before_action :set_dispute, only: [:accept_dispute, :finalise_dispute, :submit_evidence]

  INVALID_SOURCE_TYPE = "Invalid source type"

  def index
    disputes_service = Disputes::TransactionListingService
                       .new(formatted_list_params)
    render json: {
      status: :success,
      data: {
        disputes: disputes_service.formatted_disputes.as_json(root: false),
        total_pages: disputes_service.disputes.total_pages,
        total_entries: disputes_service.disputes.total_entries
      }
    }
  end

  def transaction_types
    render json: {
      status: :success,
      data: Disputes::TransactionListingService.transaction_types
    }
  end

  def accept_dispute
    data =
      case @dispute.source_type
      when BraintreeTransaction.name
        Braintree::DisputeService.new(@dispute).accept
      else
        handle_invalid_source_type
      end
    update_and_render(data, :accepted)
  end

  def submit_evidence
    data =
      case @dispute.source_type
      when BraintreeTransaction.name
        submit_evidence_braintree
      else
        handle_invalid_source_type
      end
    update_and_render(data, :dispute_initiated)
  end

  def finalise_dispute
    data =
      case @dispute.source_type
      when BraintreeTransaction.name
        Braintree::DisputeService.new(@dispute).finalize
      else
        handle_invalid_source_type
      end
    update_and_render(data, :dispute_completed)
  end

  private

  def list_params
    params.permit(
      :page,
      :per_page,
      :sort_column,
      :sort_order,
      :source_type,
      :status,
      :transaction_identifier,
      :dispute_identifier,
      :start_date,
      :end_date,
      :minimum_amount,
      :maximum_amount
    )
  end

  def formatted_list_params
    list_params
      .to_h
      .reject { |_, v| v.blank? }
      .deep_symbolize_keys
  end

  def validate_chargebacks_feature_flag!
    return if current_user.chargebacks_feature_on?

    render json: { status: :unauthorized }, status: :unauthorized
  end

  def set_dispute
    @dispute = Dispute.find_by(id: params[:id])

    render json: { status: :not_found, errors: ["Invalid Dispute"] }, status: :not_found if @dispute.blank?
  end

  def submit_evidence_braintree
    text_evidence_result = {}
    file_evidence_result = {}
    service_obj = Braintree::DisputeService.new(@dispute)
    text_evidence_result = service_obj.attach_text_evidence(
      params[:text_evidence],
    ) if params[:text_evidence]

    if params[:file_evidence]
      file_upload_result = service_obj.upload_document(params[:file_evidence])
      file_evidence_result = get_file_evidence_result(service_obj, file_upload_result)
    end
    upload_evidence_to_s3 if file_evidence_result[:success]
    create_dispute_evidences(text_evidence_result, file_evidence_result)
    combined_response(text_evidence_result, file_evidence_result)
  end

  def render_json(data)
    render json: {
      status: :success,
      data: data
    }
  end

  def combined_response(text_evidence_result, file_evidence_result)
    {
      success: text_evidence_result[:success] || file_evidence_result[:success],
      text_evidence: text_evidence_result,
      file_evidence: file_evidence_result
    }
  end

  def create_dispute_evidences(text_evidence_result, file_evidence_result)
    if text_evidence_result[:success] && file_evidence_result[:success]
      @dispute.dispute_evidences.create(
        text_evidence: params[:text_evidence],
        file_evidence: params[:file_evidence],
        submitted_by_id: current_user.id
      )
    elsif file_evidence_result[:success]
      @dispute.dispute_evidences.create(file_evidence: params[:file_evidence], submitted_by_id: current_user.id)
    elsif text_evidence_result[:success]
      @dispute.dispute_evidences.create(text_evidence: params[:text_evidence], submitted_by_id: current_user.id)
    end
  end

  def upload_evidence_to_s3
    UploadEvidence.new(
      {
        evidence: params[:file_evidence],
        dispute: @dispute
      }
    ).process
  end

  def update_dispute(action_type)
    @dispute.update(
      processed_by: current_user,
      action_type: action_type
    )
  end

  def dispute_hash
    {
      id: @dispute.id,
      last_action: @dispute.action_type,
      updated_at: @dispute.updated_at.to_s
    }
  end

  def update_and_render(data, action_type)
    update_dispute(action_type) if data[:success]
    data[:dispute] = dispute_hash
    render_json(data)
  end

  def get_file_evidence_result(service_obj, file_upload_result)
    if file_upload_result[:document_id]
      service_obj.attach_file_evidence(
        file_upload_result[:document_id]
      )
    else
      file_upload_result
    end
  end

  def handle_invalid_source_type
    Tunecore::Airbrake.notify(
      INVALID_SOURCE_TYPE,
      {
        dispute: @dispute,
        person: @dispute.person
      }
    )
    { success: false, errors: [INVALID_SOURCE_TYPE] }
  end
end
