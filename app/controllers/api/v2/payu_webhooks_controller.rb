# frozen_string_literal: true

class Api::V2::PayuWebhooksController < Api::V2::BaseController
  include PaymentFinalizable

  skip_before_action :verify_authenticity_token, raise: false
  before_action :verify_request

  SUCCESS = "success"

  def payments
    Payu::WebhookProcessorService.call(params)
    if success?
      handle_success
    else
      handle_failure
    end

    head :ok
  end

  private

  def verify_request
    head :forbidden and return if Payu::WebhookValidatorService.invalid?(params)
  end

  def handle_success
    successful_purchase_after_effects(transaction.invoice, true)
  end

  def handle_failure
    failed_purchase_after_effects_for_hosted_checkout(transaction.invoice) if invoice?
  end

  def invoice?
    transaction.present? && transaction.invoice.present?
  end

  def success?
    params[:status] == SUCCESS && transaction.present?
  end

  def transaction
    @transaction ||= PayuTransaction.find_by(invoice_id: params[:txnid])
  end
end
