class Api::V2::GenresController < Api::V2::BaseController
  def index
    genres = Genre
             .global
             .active.or(Genre.where(name: "Indian"))
             .order("name")
             .as_json(only: [:id, :name], root: false)
    render json: genres, root: false
  end
end
