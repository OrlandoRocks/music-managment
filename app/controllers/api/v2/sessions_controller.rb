class Api::V2::SessionsController < Api::V2::BaseController
  def create
    person = Person.where(email: params[:email]).first
    if person.present? && person.is_password_correct?(params[:password])
      render json: { token: generate_jwt(person) }
    else
      render json: { errors: [{ fieldName: :login, messages: ["invalid email id or password"] }] },
             status: :unauthorized
    end
  end
end
