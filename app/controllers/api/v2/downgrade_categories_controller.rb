class Api::V2::DowngradeCategoriesController < Api::V2::BaseController
  before_action :authenticate_user!

  def index
    plan_downgrade_categories = DowngradeCategory.all.as_json(
      only: [:id, :name],
      include: { downgrade_category_reasons: { only: [:id, :name] } }, root: false
    )

    render json: plan_downgrade_categories, root: false
  end
end
