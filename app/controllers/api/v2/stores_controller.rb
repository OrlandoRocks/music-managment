class Api::V2::StoresController < Api::V2::BaseController
  before_action :authenticate_user!

  def index
    stores = Store.order("name")
                  .as_json(only: [:id, :name], root: false)
    render json: stores, root: false
  end

  def monetized_stores
    monetized_stores = Store.where(id: Store::TRACK_MONETIZATION_STORES)
                            .as_json(only: [:id, :name], root: false)

    render json: monetized_stores, root: false
  end
end
