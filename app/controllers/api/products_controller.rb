# frozen_string_literal: true

class Api::ProductsController < Api::OauthBaseController
  include PublishingAdministrationHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  before_action :load_person, :load_product
  SOCIAL = "social"
  RF_VIDEO_DOWNLOAD = "rf_video_download"
  GENERIC_ERROR_MESSAGE = "api.tc.message.purchase_invalid"

  def social_add_to_cart
    case params[:product_type]
    when SOCIAL
      @purchase = SubscriptionProduct.create_subscription_purchase_for_social(
        @person,
        @product,
        params[:product_type].capitalize
      )
    when RF_VIDEO_DOWNLOAD
      @purchase = Product.add_to_cart(@person, @product)
    end

    if @purchase && @purchase.errors.empty?
      render json: { status: "success", data: @purchase }
    else
      purchase = @person.purchases.where(product: @product).where.not(status: "processed").last
      render json: { status: "failure", message: error_message, data: purchase }
    end
  end

  private

  def load_product
    @product = Product.where(
      country_website_id: @person.country_website_id,
      display_name: params[:name],
      status: "Active"
    ).first
    render json: { status: "failure", message: ["Invalid product name"] } and return if @product.nil?
  end

  def load_person
    @person = user
    render json: { status: "failure", message: ["User #{params[:email]} not found in TC"] } and return if @person.nil?
  end

  def error_message
    message = GENERIC_ERROR_MESSAGE

    if @purchase.errors.present?
      message =
        @purchase.errors
                 .map { |_, value| value if translation_available?(value) }
                 .compact
    end

    message
  end

  # Return errors with valid translations only
  def translation_available?(key)
    custom_t(key, raise: true)
    true
  rescue I18n::MissingTranslationData
    false
  end
end
