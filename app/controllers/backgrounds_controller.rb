class BackgroundsController < ApplicationController
  def index
    @backgrounds = Background.all

    respond_to do |format|
      format.html { raise "html is not supported." }
      format.json { render json: @backgrounds.to_json }
    end
  end
end
