class SoundoutAlbumsController < ApplicationController
  def index
    params.merge({ per_page: 5 })
    @albums = Tunecore::MusicSearch.album_search(current_user, params)
    render json: {
      total_count: @albums.total_entries,
      per_page: @albums.per_page,
      total_pages: @albums.total_pages,
      current_page: @albums.current_page,
      albums: @albums.as_json(root: false, only: [:id, :name, :artist_name, :album_type, :artwork_url, :song_matched])
    }
  end
end
