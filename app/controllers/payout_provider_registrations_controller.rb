class PayoutProviderRegistrationsController < ApplicationController
  include PayoutProvidable
  before_action :restrict_access, unless: :payoneer_payout_enabled?

  def create
    registration_form = PayoutProvider::RegistrationForm.new(
      person: current_user,
      currency: params[:currency],
      program_id: params.dig(:program_id),
      payout_methods_list: params[:payout_methods_list],
    )

    if registration_form.save
      # When we make an AJAX call from currency_selector_popup.js we need to return a URL to manually redirect to
      render json: { location: registration_form.link, status: 200 } and return if params.dig(:js_redirect)

      redirect_to registration_form.link
    else
      flash[:errors] = custom_t(:something_went_wrong)
      redirect_back fallback_location: dashboard_path
    end
  end
end
