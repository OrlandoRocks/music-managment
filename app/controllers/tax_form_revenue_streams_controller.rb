# frozen_string_literal: true

class TaxFormRevenueStreamsController < ApplicationController
  def create
    form = PayoutProvider::TaxFormRevenueStreamMapperForm.new(
      person: current_user,
      mappings: tax_form_mapping_params
    )

    if form.save
      flash[:success] = custom_t("people.edit.tax_form_mapping.success_message")
    else
      flash[:error] = custom_t("people.edit.tax_form_mapping.error_message")
      Airbrake.notify(
        "Failed mapping Tax Forms to Revenue Sources: #{form.errors.full_messages.join(', ')}",
        tax_form_mapping_params.merge(person_id: current_user.id)
      )
    end

    redirect_to account_settings_path(tab: "payoneer_payout")
  end

  def tax_form_mapping_params
    params.require(:tax_form_mappings)
  end
end
