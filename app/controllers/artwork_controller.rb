require "digest/md5"
require "net/scp"

class ArtworkController < ApplicationController
  include DistributionProgressBar

  before_action :nonsession_access_with_key, only: [:create]
  before_action :load_person, :load_album, :ensure_metadata_changes_permitted
  before_action :load_progress_bar_translations, only: [:show]
  before_action -> { set_distribution_progress(@album) }, only: [:show]
  skip_before_action :alert_rejections
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:create, :show]

  def show
    @artwork = @album.artwork || Artwork.new

    render layout: "application"
  end

  def create
    begin
      @artwork = Artwork.create_or_find_by(album: @album)
      @artwork.update(last_errors: nil) if @artwork.last_errors?
      @artwork.assign_artwork params[:artwork] || params[:Filedata]
      @artwork.save
    rescue StandardError => e
      Rails.logger.error("\n#{e.class} (#{e.message}):\n #{Rails.backtrace_cleaner.clean(e.backtrace).join("\n ")}")
      @artwork.update_columns(
        last_errors: custom_t("controllers.artwork.try_again"),
        uploaded: false,
        height: nil,
        width: nil
      )
    end

    render action: "show", layout: "application"
  end

  protected

  def nonsession_access_with_key
    return true if params[:key].blank?

    @album = Album.find(params[:album_id])
    current_user = @person = Person.find(@album.person_id)

    # don't check if user is admin
    return unless @person.is_administrator && (params[:key] != upload_key)

    raise ActiveRecord::RecordNotFound, "Unable to find artwork record for key: #{params[:key].inspect}"
  end

  def load_album
    @album ||=
      if @person.is_administrator
        Album.find(params[:album_id])
      else
        @person.albums.find(params[:album_id])
      end
  end

  def ensure_metadata_changes_permitted
    unless @album.permit_metadata_changes?
      return access_forbidden if params[:key].present? # status code for flash

      flash[:error] = custom_t("controllers.artwork.cannot_be_edited")
      redirect_to(album_url(@album)) && (return false)
    end
    true
  end

  def upload_key(person = @person, album = @album)
    Base64.encode64(Digest::MD5.digest("UrtWerk_#{album.id}_t00nK0re58_#{person.id}_#{person.salt}")).strip
  end
  helper_method :upload_key
end
