class PeopleController < ApplicationController
  include ReferralData::Parsable
  include MetaTags::Taggable
  include TwoFactorAuthable
  include RecaptchaVerifiable
  include PayoneerAchFeeInfo
  include CountryPhoneCodeable
  include OneTimePaymentRedirectable

  skip_before_action :login_required,
                     only: [
                       :new,
                       :create,
                       :profile,
                       :complete_verify,
                       :set_default_phone_prefix
                     ]
  before_action :load_person,
                except: [
                  :new,
                  :create,
                  :profile,
                  :complete_verify,
                  :set_default_phone_prefix
                ]

  skip_before_action :check_for_verification, :check_for_terms_and_conditions

  before_action :build_person, only: [:new, :create]
  before_action(only: [:new]) { |c| c.build_referral_data_from_cookies(:signup) }
  before_action(only: [:new]) { |c| c.build_referral_data_from_url(:signup) }
  before_action :login_disallowed, only: [:new, :create]
  before_action :oauth_required, only: [:profile]
  before_action :check_and_load_cert_redemption_values
  before_action :set_page_title, only: [:new]
  before_action :set_meta_tags, only: [:new]
  before_action :set_canonical_link, only: [:new]
  before_action :redirect_one_time_payment_users, only: [:edit], if: :one_time_payment_user_payment_tab?
  before_action :two_factor_authenticate!, only: [:edit], if: :two_factor_auth_required?
  # TODO: change these two 'load' before_actions to only: instead of except:
  before_action :load_payoneer_ach_fee_info, except: [:renewal_billing_info, :update_renewal_billing_info]
  before_action :load_default_ach_fee_info, except: [:renewal_billing_info, :update_renewal_billing_info]
  before_action :recaptcha, only: [:create], if: :use_recaptcha?
  after_action :flash_to_headers
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:edit, :unverified]
  skip_before_action :redirect_stem_users, only: [:edit, :update]
  # renders 'Payout Preferences' tab when requested 'Taxpayer ID' tab is not available
  # Note: 'Taxpayer ID' tab ceases to display as soon as valid tax forms are submitted.
  #       This redirection caters to situations where stale notifications are clicked.
  before_action -> { redirect_to account_settings_path(tab: "payoneer_payout") },
                only: :edit,
                if: -> { params[:tab].eql?("taxpayer_id") && !helpers.show_taxpayer_id_tab? }

  layout "application_old"

  def new
    if request.path != "/signup"
      redirect_to signup_url, status: :moved_permanently
      return
    end

    @person.is_opted_in = true
    @person.promo_ref = params[:ref] unless params[:ref] == "signup"
    location = IpAddressGeocoder.geocode(request.remote_ip, request: { timeout: 3 })
    @ip_country_code = location["country_code"]
    show_and_set_domain_modal

    find_targeted_offer_by_token(@person.join_token) if @person.has_join_token?
    @country_phone_code ||= CountryPhoneCode.find_for_iso_code(country_website)
    initialize_translations

    if reskin?
      render "new_reskin", layout: "registration_reskin"
    else
      render "new", layout: "registration"
    end
  end

  def create
    @page_title = custom_t(:sign_up)
    @person.password_reset_tmsp = Time.zone.now

    if params[:external_services_person].present?
      external = ExternalServicesPerson.new(create_external_services_person_params)
    end

    if create_person_processing(external)
      session[:verification] = true if @person.is_verified
      session[:cert_code].present? ? ensure_correct_country_domain("/cert_redemptions/create") : ensure_correct_country_domain("/dashboard/create_account?fb=true")
      Sift::EventService.create_account(@person, cookies, request)
      clear_referral_cookies
    elsif external && !@person.email_uniqueness_validated?
      flash[:error] = custom_t("controllers.people.already_account", person_email: @person.email)
      redirect_to login_path(email: @person.email, ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    else
      initialize_translations

      if reskin?
        render action: "new_reskin", layout: "registration_reskin"
      else
        render action: "new", layout: "registration"
      end
    end
  end

  def edit
    @days_until_expiration = current_user.check_credit_card_expiration_date
    @page_title = custom_t(:my_account_display)
    @person.valid? # display any validation error messages
    @account_overview = Tunecore::AccountOverview.new(@person)
    @compliance_info_form = ComplianceInfoForm.new(current_user)
    @company_information_form = CompanyInformationForm.new(current_user)
    set_tab
    set_country_change_flag
    edit_profile_friends
    edit_profile_history
    edit_profile_payment
    edit_profile_payout
    edit_profile_subscriptions
    edit_profile_account_profile_survey
    edit_tax_form_mapping
    validate_priority_artist
    render layout: "application"
  end

  def set_tab
    @tab =
      if params[:tab] && (params[:tab] != "refer") || (params[:tab] == "refer" && @friend_referral_enabled)
        params[:tab]
      else
        "account"
      end
  end

  def edit_profile_friends
    @ambassador_available = true
    @friend_referral_enabled = feature_enabled?(:friend_referral)

    @friend_referral_share_title = custom_t("controllers.people.recommend")

    return unless @person.is_ambassador?

    campaign = FriendReferralCampaign.get_current_campaign_details(@person)
    ambassador_stats = FriendReferralCampaign.get_stats(@person)

    if campaign.errors.empty? && ambassador_stats.errors.empty?
      @ambassador_available = true
      @friend_referral_share_url = campaign.campaign_url
      @friend_referral_referred_count = ambassador_stats.unique_referrals
      @friend_referral_dollars_earned = ambassador_stats.balance
    else
      @ambassador_available = false
    end
  end

  def edit_profile_history
    @manager = Tunecore::HistoryManager.new(@person)
    @manager.generate_data
    @invoices = @manager.invoices
  end

  def edit_profile_payment
    if @person.autorenew?
      @person_preference = PersonPreference.find_by(person_id: @person.id)
      @person_preference = PersonPreference.new if @person_preference.nil?
      @stored_paypal_account = StoredPaypalAccount.currently_active_for_person(@person)
      @stored_credit_cards = @person.stored_credit_cards.active
      @payments_os_credit_cards = @stored_credit_cards.payments_os_cards
      @braintree_credit_cards = @stored_credit_cards.braintree_cards
      @adyen_stored_payment_methods = @person.latest_in_each_adyen_payment_methods
      @stored_and_not_expired_credit_cards = @person.stored_credit_cards.active.not_expired
      if !@person_preference.nil? && (!@stored_and_not_expired_credit_cards.empty? && @person_preference.preferred_credit_card_id.nil?)
        @person_preference.preferred_credit_card_id = @stored_and_not_expired_credit_cards.first.id
      end
    else
      @person_preference = PersonPreference.new
    end
    @tcs_return_msg = params[:tcs_return_msg]
  end

  def edit_profile_payout
    @stored_bank_account = @person.stored_bank_accounts.current
    @balance = @person.person_balance.balance
    @withdraw_page = true
  end

  def edit_profile_subscriptions
    @active_subscriptions = Renewal.active_subscriptions_by_person(current_user)
    @expired_subscriptions = Renewal.expired_subscriptions_by_person(current_user)
    @active_renewals = active_renewals
    @plan_renewal_service = Plans::PlanRenewalCardService.new(current_user)
    @person_subscriptions = @person
                            .person_subscription_statuses
                            .joins(subscription_products: :product)
                            .where(subscription_type: "Social")
                            .distinct
  end

  def edit_profile_account_profile_survey
    @survey_info          = PersonProfileSurveyInfo.get_survey_info(@person)
    @compliance_info_form = ComplianceInfoForm.new(@person)
  end

  def edit_email_phone
    @page_title = custom_t("controllers.people.edit_email_phone")
    @person.valid? # display any validation error messages

    render layout: false
  end

  def update
    @page_title = custom_t("controllers.people.edit_account")
    old_email = @person.email
    if @person.update(person_params)
      handle_changed_password if updating_password?
      handle_changed_email(old_email) if updating_email?(old_email)
      flash[:notice] = custom_t("controllers.people.updated_profile")
      redirect_to controller: :dashboard
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
      render action: :edit
    end
  end

  def update_email_phone
    old_email = @person.email
    if @person.update(person_params)
      handle_changed_email(old_email) if updating_email?(old_email)
      flash.now[:notice] = custom_t("controllers.people.updated_profile")
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
    end

    render action: "edit_email_phone", layout: false
  end

  def update_account_settings
    old_email                   = @person.email
    partial                     = params[:person].delete(:account_settings_partial)
    @shared_login_form_section  = params[:person].delete(:shared_login_form_section)

    if person_params[:password] == person_params[:original_password]
      @person.errors.add(:original_password, custom_t("controllers.people.old_cannot_be_new_password_error"))
    end

    @person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
    @vat_update_result = UpdateVatInfoService.new(
      person: @person,
      country_code: person_params[:country]
    ).process

    @country_change = person_params[:country] != @person[:country]

    notify_password_change if errors_by_partial?(partial) && updating_password?

    email_change_modal = Person::EmailChangeService.call(@person, person_params)
    partial = email_change_modal if email_change_modal.present?

    save_compliance_info unless updating_password? || updating_email?(old_email)

    Sift::EventService.update_account_password(@person, cookies, request) if updating_password?

    @compliance_info_form = ComplianceInfoForm.new(current_user)

    respond_to do |format|
      format.js { render partial: "people/account_settings/#{partial}", layout: false }
    end
  end

  def update_priority_artist
    artist = Artist.find(priority_artist_params[:priority_artist_id])
    if @person.priority_artist.present?
      @person.priority_artist.update(artist: artist)
    else
      PriorityArtist.create(person: @person, artist: artist)
    end

    respond_to do |format|
      format.js { render partial: "people/account_settings/priority_artist", layout: false }
    end
  end

  def errors_by_partial?(partial)
    # when updating account info we want to run validatons on phone number (specifically for indian users),
    # otherwise we want to run regular update validations
    # ---update_phone_number runs validations on phone_number for indian users
    update_params = person_params.dup.except(:email)
    phone_number  = update_params.delete(:phone_number)
    mobile_number = update_params.delete(:mobile_number)
    old_email = @person.email

    return false unless @person.errors.none?
    return false unless password_verified?(person_params[:original_password])

    case partial.to_sym
    when :contact_info
      @person.update_phone_number(phone_number: phone_number, mobile_number: mobile_number) &&
        @person.update(update_params)
    when :address_info
      @person.require_address = true
      @person.update(update_params)
    else
      @person.update(update_params)
    end
  end

  def unverified
    # page w/ links to verification actions
    # Redirect to dashboard if the person is_verified? (some users get directed to back to the unverified screen because
    # when logging back in because of how we redirect users to the page they were trying to access before logging in.) 2009-10-05 MJL
    @page_title = custom_t("controllers.people.verify_account")
    @is_created_today = @person.created_on.to_date == Date.today

    if @person.is_verified?
      redirect_to controller: :dashboard
    elsif reskin?
      render "unverified_reskin", layout: "registration_reskin"
    else
      render "unverified", layout: "application"
    end
  end

  def send_verification_email
    deliver_verification_email(@person)
    flash[:notice] = custom_t("controllers.people.verification_email_sent")
    redirect_to unverified_person_path(@person)
  end

  def complete_verify
    @person = Person.find(params[:id].to_i)

    if @person.is_verified?
      handle_existing_verification
      redirect_to(dashboard_path)
    elsif @person.confirm_invite(params[:key])
      handle_successful_verification
      deliver_cyrillic_formatting(@person) if %w[Belarus Kazakhstan Russia Ukraine].include? @person.country
      RewardSystem::Events.new_user(@person.id)

      # If not logged in
      if current_user.blank?
        flash[:notice] = custom_t("controllers.people.person_verified", person_email: @person.email)
        redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      # If logged in as another user
      elsif @person != current_user
        flash[:notice] = custom_t("controllers.people.access_account", person_email: @person.email, logout_path: logout_path(redirect: login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)))
        redirect_to dashboard_path
      # Otherwise logged in as the user
      else
        session[:verification] = true
        redirect_to(controller: "dashboard", action: "create_account")
      end
    else
      handle_failed_verification
      redirect_to(unverified_person_path(@person))
    end
  end

  # for oauth'd api calls
  def profile
    data = {}
    data[:id]     = current_user.id
    data[:roles]  = current_user.roles.map(&:name)
    render plain: data.to_json
  end

  def flash_to_headers
    return unless request.xhr?

    response.headers["X-Message"] = flash[:error] if flash[:error].present?
    response.headers["X-Message"] = flash[:notice] if flash[:notice].present?

    flash.discard # don't want the flash to appear when you reload page
  end

  def deliver_verification_email(person)
    PersonNotifier.verify(person, host: person.domain).deliver
  end

  def deliver_cyrillic_formatting(person)
    PersonNotifier.cyrillic_formatting(person).deliver
  end

  def restart_survey
    @person.person_profile_survey_info.restart_survey
    redirect_to(params[:source] == "interstitial") ? account_profile_survey_announcement_path : "/people/#{@person.id}/edit/?tab=profile"
  end

  def set_default_phone_prefix
    selected_country = params.fetch(:selected_country, Country::INDIA_ISO)
    phone_code = CountryPhoneCode.find_for_iso_code(selected_country)
    render json: { phone_code: phone_code.code }
  end

  def update_vat_information
    if @person.update(vat_params)
      flash[:notice] = custom_t("vat.added_vat_info")
    else
      err_msgs =
        @person.errors.values.flatten.map do |err|
          custom_t(err, cust_care_link: TC_CUSTOMER_CARE_LINK)
        end
      flash[:error] = err_msgs.join(", ").html_safe
    end
    respond_to do |format|
      format.js { flash }
    end
  end

  def update_company_information
    redirect_to :account_settings_path unless @person.vat_feature_on?

    @company_information_form = CompanyInformationForm.new(@person)
    @company_information_form.submit(company_info_params)

    respond_to do |format|
      format.js
    end
  end

  def update_paypal_kyc_info
    @compliance_info_form  = ComplianceInfoForm.new(@person)
    @compliance_info_saved =
      ActiveRecord::Base.transaction do
        unless save_compliance_info && save_customer_type
          raise ActiveRecord::Rollback, "Compliance Info Persistence Failed"
        end

        true
      end

    @person.update(self_billing_params)
    @person.update_address_with_country_audit(CountryAuditSource::PAYPAL_KYC_NAME, address_params)

    respond_to do |format|
      format.js { render partial: "my_account/paypal_self_billing_and_kyc_info", layout: false }
      format.json do
        if @person.errors.any? || @compliance_info_form.errors.any?
          render json: {
            success: false,
            errors: [
              *@person.errors.full_messages,
              *@compliance_info_form.errors.full_messages
            ]
          },
                 status: :bad_request
        else
          render json: { success: true }
        end
      end
    end
  end

  def update_self_billing_status
    partial = params[:person].delete(:account_settings_partial)
    @person.update(self_billing_params)

    respond_to do |format|
      format.js { render partial: "people/account_settings/#{partial}", layout: false }
      format.json do
        if @person.errors.any?
          render json: { success: false, errors: @person.errors.full_messages }, status: :bad_request
        else
          render json: { success: true }
        end
      end
    end
  end

  def renewal_billing_info
    unless FeatureFlipper.show_feature?(:vat_tax, @person) && FeatureFlipper.show_feature?(:bi_transfer, @person)
      return redirect_to dashboard_path
    end

    @compliance_info_form ||= ComplianceInfoForm.new(@person)
    render layout: "application"
  end

  def update_renewal_billing_info
    renewal_params = person_params.merge(self_billing_params).merge(vat_params)
    @person.require_address = true
    @person.country_audit_source = CountryAuditSource::AUTO_RENEWAL_FORM_NAME
    @compliance_info_form ||= ComplianceInfoForm.new(@person)

    vat_options = { person: @person, country_code: person_params[:country] }
    @vat_update_result = UpdateVatInfoService.new(vat_options).process

    ActiveRecord::Base.transaction do
      @person.update(renewal_params)
      save_compliance_info
    end

    partial = @person.errors.any? ? "people/edit_renewal_billing_info.js" : "people/renewal_form_success_modal.js"
    respond_to do |format|
      format.js { render partial: partial, layout: false }
    end
  end

  def confirm_vat_info
    errors = []
    vat_info = params[:vat_info].each_value(&:strip!)
    country_code = vat_info[:country_code].presence || @person[:country]
    country_name = Country.find_by(iso_code: country_code)&.name_raw

    unless vat_info[:vat_registration_number].match(VAT_FORMAT[country_name])
      errors << custom_t("vat.errors.invalid_format")
    end

    vat_assesment = VatAssesmentService.new(
      @person,
      vat_info[:vat_registration_number],
      complete_address_params
    )
    errors << custom_t(
      "vat.errors.unavailable",
      cust_care_link: TC_CUSTOMER_CARE_LINK
    ) unless vat_assesment.acceptable?

    validator = TcVat::FailSafe::Validator.new(@person, vat_info) if errors.empty?
    customer_type =
      if errors.empty? && validator.valid?
        VatInformation::BUSINESS
      else
        errors << vat_error_message(validator) if validator
        VatInformation::INDIVIDUAL
      end

    inbound_rate = TcVat::Calculator.fetch_vat_applicable_rate(TcVat::Base::INBOUND, customer_type, country_name)
    outbound_rate = TcVat::Calculator.fetch_vat_applicable_rate(TcVat::Base::OUTBOUND, customer_type, country_name)
    render json: {
      valid_vat: customer_type == VatInformation::BUSINESS,
      outbound_rate: outbound_rate,
      inbound_rate: inbound_rate,
      errors: errors.flatten.compact
    }
  end

  def confirm_current_tax_form
    TaxReports::TaxFormMappingService.call!(@person.id, kind: :user)

    respond_to do |format|
      format.js
    end
  end

  def request_secondary_tax_form
    api_client = Payoneer::PayoutApiClientShim.fetch_for(@person)

    @response =
      PayoutProvider::ApiService
      .new(
        person: @person,
        program_id: PayoutProviderConfig::SECONDARY_TAX_FORM_PROGRAM_ID,
        api_client: api_client
      )
      .adopt_payee(person: @person)

    respond_to do |format|
      format.js
    end
  end

  protected

  def password_verified?(password)
    @person.is_password_correct?(password) ? true : (@person.errors.add(:original_password, "Password is incorrect.") && false)
  end

  def check_and_load_cert_redemption_values
    return unless flash[:redeem_thank_you]

    @brand_code = flash[:redeem_thank_you].downcase
    @redemption = true
    @account_overview = Tunecore::AccountOverview.new(@person, false)
  end

  def handle_successful_verification
    @person.verified_at = Time.now
    @person.save
  end

  def handle_existing_verification
    flash[:notice] = custom_t("controllers.people.email_already_verified")
  end

  def handle_failed_verification
    # If its the right key but its expired generate a new one and tell the customer its expired.
    if params[:key] == @person.invite_code && !@person.invite_active?
      deliver_verification_email(@person)
      flash[:error] = custom_t("controllers.people.verif_key_exp_sent_new_one", person_email: @person.email)
    else
      flash[:error] = custom_t("controllers.people.verif_key_not_recognized", send_verif_person: send_verification_email_person_url(@person))
    end
  end

  def handle_changed_email(old_email)
    PersonNotifier.change_email(@person, @person.email, specified_email: old_email).deliver
  end

  def notify_password_change
    PersonNotifier.change_password(@person, params[:person][:password]).deliver
  end

  def email_param_exists?
    person_params[:email].present?
  end

  def updating_email?(old_email)
    return false if old_email.blank?

    new_email = params.dig(:person, :email)
    return false if new_email.blank?

    new_email != old_email
  end

  def updating_password?
    # check to see if both are set in params
    person = params[:person]
    person[:password].present? && person[:password_confirmation].present?
  end

  def load_person
    raise ActiveRecord::RecordNotFound unless current_user.id

    @person = current_user
  end

  def find_targeted_offer_by_token(join_token)
    data = TargetedOffer.offer_data_by_join_token(join_token)

    if data[:error_message]
      flash.now[:error] = data[:message]
    else
      @targeted_offer = data[:targeted_offer]
    end
  end

  def payout_details_mobile_view
    render :payout_details_mobile_view
  end

  private

  def initialize_translations
    gon.push(translations: {})
    gon_custom_t("translations.name", "name")
    gon_custom_t("translations.country_display", "country_display")
    gon_custom_t("translations.password", "password")
    gon_custom_t("translations.new_password", "new_password")
    gon_custom_t("translations.email", "email")
    gon_custom_t("translations.confirm_password", "people.people_fields.confirm_password")
    gon_custom_t("translations.should_match_confirmation", "models.person.should_match_confirmation")
    gon_custom_t("translations.passwords_do_not_match", "models.person.passwords_do_not_match")
    gon_custom_t("translations.emails_do_not_match", "models.person.emails_do_not_match")
    gon_custom_t("translations.password_requirements", "models.person.password_requirements")
    gon_custom_t("translations.tc_not_accepted", "models.person.accepted")
    gon_custom_t("translations.blank", "errors.messages.blank")
    gon_custom_t("translations.invalid", "errors.messages.invalid")
  end

  def vat_params
    params.require(:person).permit(
      :customer_type,
      vat_information_attributes: %i[id company_name vat_registration_number]
    )
  end

  def show_and_set_domain_modal
    non_dot_com_domains = CountryWebsite.where.not(id: CountryWebsite::UNITED_STATES).pluck(:country)

    if @ip_country_code.present? && non_dot_com_domains.include?(@ip_country_code) && country_website != @ip_country_code
      @show_modal = FeatureFlipper.show_feature?(:domain_redirect_modal, current_user)
      @redirect_country = @ip_country_code
    elsif country_website != "US"
      @show_modal = FeatureFlipper.show_feature?(:domain_redirect_modal, current_user)
      @redirect_country = "US"
    end
  end

  def create_external_services_person_params
    params
      .require(:external_services_person)
      .permit(
        :account_id,
        :external_service_id,
        :person_id
      )
  end

  def person_params
    selected_country = params.dig(:person, :country)

    if (country_website == Country::INDIA_ISO || selected_country == Country::INDIA_ISO) && params.dig(:person, :mobile_number).present?
      unless params[:action] == "update_account_settings"
        params[:person][:prefixless_mobile_number] = params.dig(:person, :mobile_number)
      end
      params[:person].merge!(sanitize_phone_number_params)
    end

    params[:person][:state] = CountryState.find(params[:state_id]).name if params[:state_id].present?
    if params[:state_city_name].present?
      params[:person][:city] = CountryStateCity.find_by_name(params[:state_city_name])&.name
    end

    permit_params = [
      :accepted_terms_and_conditions,
      :is_opted_in,
      :email,
      :email_confirmation,
      :apple_id,
      :apple_role,
      :name,
      :original_password,
      :password,
      :password_confirmation,
      :phone_number,
      :mobile_number,
      :prefixless_mobile_number,
      :customer_type,
      { referral_data_attributes: [:key, :value, :page, :source, :timestamp] }
    ]

    unless @person&.address_locked?
      permit_params
        .push(:country, :address1, :address2, :city, :state, :zip, :postal_code)
    end

    params.require(:person).permit(permit_params)
  end

  def priority_artist_params
    params.require(:person).permit(:priority_artist_id)
  end

  def self_billing_params
    params.require(:person).permit(self_billing_status_attributes: %i[id status version ip_address])
  end

  def address_params
    params.require(:person).permit(:address1, :address2, :city, :state, :postal_code, :country)
  end

  def create_person_processing(external = nil)
    if @person.save
      self.current_user = @person
      @person.create_logged_in_event(request.remote_ip, request.user_agent)
      @person.external_services_people << external if external
      @person.create_two_factor_auth_prompt

      offer_expiration = @person.targeted_offers.blank? ? "" : (@person.targeted_offers.first.expiration_date_for_person(@person).try(:to_date).try(:to_s, :long) || "")

      if external && external.valid?
        @person.is_verified = true
        @person.clear_invite
        @person.save

        session[:facebook_signup] = true if external.external_service.name == "facebook"
        handle_successful_verification
      else
        deliver_verification_email(@person)
      end

      if cookies[:optimizelyEndUserId] && cookies[:optimizelyBuckets]
        record_optimizely_event("successfully_signed_up", cookies[:optimizelyEndUserId], cookies[:optimizelyBuckets])
      end

      @person.record_login(request.remote_ip)

      true
    else
      find_targeted_offer_by_token(@person.join_token) if @person.has_join_token?
      flash.now[:error] = custom_t("controllers.people.errors_account_fix")
      false
    end
  end

  def build_person
    begin
      @person = current_user || Person.new(person_params)
    rescue ActionController::ParameterMissing
      @person = Person.new
    end

    @person.country_website_id = CountryWebsite.find_by(country: LOCALE_TO_COUNTRY_MAP[locale.to_s]).id
    @person.country_audit_source = CountryAuditSource::SELF_IDENTIFIED_NAME
    verify_person if ENV["AUTO_VERIFY_PERSON"]
  end

  def verify_person
    @person.is_verified = 1
    @person.verified_at = Time.now
  end

  def save_compliance_info
    return true unless FeatureFlipper.show_feature?(:bi_transfer, @person)

    @compliance_info_form ||= ComplianceInfoForm.new(@person)
    @compliance_info_form.save(compliance_info_params)

    @compliance_info_form.errors.any? do
      translated_error_field_names = @compliance_info_form.errors.full_messages.map do |field_name|
        custom_t("people.renewal_billing_info.#{field_name}")
      end.join(", ")
      flash[:error_dialog] = custom_t("people.renewal_billing_info.error_saving_info", error: translated_error_field_names)
      return false
    end

    Sift::EventService.update_account_contact_info(@person, person_params, cookies, request)

    true
  end

  def save_customer_type
    return true unless FeatureFlipper.show_feature?(:bi_transfer, @person)

    return true if @person.update(kyc_customer_type_params)

    flash[:error_dialog] = custom_t("people.renewal_billing_info.error_saving_info", error: "Customer Type")
    false
  end

  def compliance_info_params
    params.fetch(:person).permit(:customer_type).merge(
      params.permit(:first_name, :last_name, :company_name)
    )
  end

  def kyc_customer_type_params
    params.fetch(:person).permit(:customer_type)
  end

  def complete_address_params
    params.require(:address_values)
          .permit(
            :address1,
            :address2,
            :city,
            :corporate_entity_id,
            :country,
            :customer_type,
            :state,
            :postal_code,
            compliance_info: [:first_name, :last_name, :company_name]
          )
  end

  def company_info_params
    params
      .require(:company_information_form)
      .permit(
        :company_registry_data,
        :current_password,
        :enterprise_number,
        :place_of_legal_seat,
        :registered_share_capital
      )
  end

  def set_country_change_flag
    @country_change =
      case params[:country_change]
      when "true"
        true
      else
        false
      end
  end

  def vat_error_message(validator)
    return validator.errors.map { |e| custom_t(e) }.join(" ") if validator.errors.present?

    custom_t("vat.errors.invalid")
  end

  def active_renewals
    return [] if current_user.has_plan?

    Renewal.renewals_within_date_range_by_person(current_user, Time.now, Time.now + 6.months)
  end

  def validate_priority_artist
    @person.valid_priority_artist
  end

  def reroute_failed_recaptcha
    @person = Person.new
    flash.now[:error] = custom_t("controllers.sessions.i_am_not_a_robot")

    if reskin?
      render "new_reskin", layout: "registration_reskin"
    else
      render "new"
    end
  end

  def edit_tax_form_mapping
    @primary_tax_form = @person.primary_tax_form
    @secondary_tax_form = @person.secondary_tax_form
    @user_mapped_distribution_tax_form = @person.user_mapped_distribution_tax_form
    @user_mapped_publishing_tax_form = @person.user_mapped_publishing_tax_form
  end
end
