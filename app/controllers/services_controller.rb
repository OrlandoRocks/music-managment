class ServicesController < ApplicationController
  include YoutubeHelper

  before_action :load_person

  def index
    @page_title                 = custom_t("controllers.artist_services.artist_services")
    @youtube_monetization       = current_user.youtube_monetization
    @ytm_tracks                 = YtmTracks.new(current_user)
    @ytsr_product_id            = Product.find_products_for_country(current_user.country_domain, :ytm)
    @ytsr_product               = Product.find(@ytsr_product_id)
    @soundout_report            = current_user.soundout_reports.where("canceled_at IS NULL")
    albums_in_cart              = Purchase.albums_in_cart(current_user)
    @is_album_in_cart           = albums_in_cart.present?
    @albums_in_cart_count       = albums_in_cart.count
    @albums_count               = current_user.albums.distributed.not_taken_down.not_ringtones.count

    @youtube_price = calc_youtube_price

    respond_to do |format|
      format.html {
        render layout: "application"
      }
    end
  end

  def apple_music
    redirect_to home_url
  end
end
