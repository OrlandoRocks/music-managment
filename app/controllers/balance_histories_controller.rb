class BalanceHistoriesController < ApplicationController
  layout "tc-foundation"
  include PayoutProvidable

  def show
    # Redirect until balance history story work begins
    # @page_title                = custom_t("controllers.my_account.transfers")
    # @balance_history_presenter = BalanceHistoryPresenter.new(balance_history_params)
    redirect_to list_transactions_path
  end

  private

  def balance_history_params
    params.merge(person: current_user, page: page)
  end

  def page
    params[:person_transaction_page_form].try(:[], :page) || 1
  end
end
