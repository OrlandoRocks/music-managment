class LegalDocumentsController < ApplicationController
  def show
    legal_document = LegalDocument.find(params[:id])

    if legal_document.person_id == current_user.id
      redirect_to legal_document.document_url
    else
      head 404
    end
  end
end
