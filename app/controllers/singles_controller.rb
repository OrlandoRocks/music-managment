class SinglesController < ApplicationController
  include AlbumHelper
  include ArtistUrls
  include CreativeSystem::Controller
  include DistributionProgressBar
  include InternationalGenre
  include PhysicalStoresHelper
  include TimedReleasable
  include WorldwideReleasable
  include StoreSelection
  include SpecializedReleasesHandleable
  include AlbumApp
  include S3MetadataFetchable

  helper_method :creative_roles_for_select

  before_action :load_editable_single, only: [:edit, :update, :destroy, :distribution_panel]
  before_action :load_single, only: [:show, :edit, :update, :monetize, :confirm_monetize]
  before_action :load_yt_store, only: [:monetize]
  before_action :load_songs, only: [:show, :confirm_monetize]
  before_action :gon_main_artist_text, only: [:new, :show, :edit]
  before_action :gon_datepicker_setup, only: [:new, :edit]
  before_action :gon_itunes_delays, only: [:new, :update, :edit, :create]
  before_action :translate_dates, only: [:create, :update]
  before_action :gonify_all_main_artists, only: [:edit, :new]
  before_action :load_progress_bar_translations, only: [:edit, :new, :show]
  before_action :scrub_artist_names, only: [:update, :create]
  before_action :set_golive_date_est, only: [:edit]
  before_action -> { set_original_release_date(params[:single]) }, only: [:update]
  before_action -> { handle_parent_genre_params(params[:single]) }, only: [:update, :create]
  before_action -> { set_distribution_progress(@single) }, only: [:new, :edit, :show]
  before_action -> { init_album_app_vars(@single, Single) }, only: %i[new edit]

  after_action -> { backfill_album_metadata(params[:id]) }, only: [:show], if: proc { !@single.finalized? }

  layout "application"

  skip_before_action :alert_rejections
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:new, :show]

  def new
    @page_title ||= custom_t("controllers.singles.create_a_new_single")
    domain_service = MultipleCurrencyDomainService.new(current_user)
    @single = Single.new(
      sale_date: Time.current,
      language_code: domain_service.distribution_language_code_select(default_language_code),
      parental_advisory: nil,
    )
    @single.person = current_user
    @creatives = @single.creatives
    @single.golive_date = convert_golive_to_est(@single.golive_date)
    @single_clean_version_selector_enabled = single_clean_version_selector_enabled?
    load_selected_countries
    load_upc_link

    @popup_modal_translations = prepare_popup_translations(current_user.country_website)

    return unless album_app_enabled?

    render layout: "tc-foundation", template: "singles/album_app" and return false
  end

  def show
    @uploadURL    =   BIGBOX_UPLOAD_URL
    @registerURL  =   BIGBOX_REGISTER_URL
    @assetsURL    =   BIGBOX_ASSETS_URL

    @page_title = @single.name
    @single.calculate_steps_required
    @artwork = @single.artwork
    @song = @songs.first
    @song_duration = @song.duration.divmod(1.minute) if @song.duration

    @creatives = @single.creatives
    @single.golive_date = convert_golive_to_est(@single.golive_date)
    load_physical_store_views

    @preorder = @single.salepoint_preorder_data.by_store("iTunesWW").first || @single.salepoint_preorder_data.by_store("Google").first
    @enabled = (@preorder and @preorder.preorder_purchase.enabled?)
    @paid = (@preorder and @preorder.preorder_purchase.paid_at?)
    @grat_songs = @preorder ? @preorder.preorder_instant_grat_songs : []

    @unpurchased_stores = @single.unselected_stores
    @artwork = @single.artwork

    @sso_cookie_value = generate_sso_cookie_value(@single.person_id)

    @show_add_stores = show_add_stores?(@single)
    @show_buy_plan   = show_buy_plan?(@single)

    gon_custom_t("remove_facebook_salepoint_confirmation", "album.distribute_modal_popup.remove_facebook_salepoint_confirmation")
    gon_custom_t("remove_facebook_salepoint_confirmation_edit", "album.distribute_modal_popup.remove_facebook_salepoint_confirmation_edit")
    gon_custom_t("remove_qobuz_salepoint_confirmation", "album.distribute_modal_popup.remove_qobuz_salepoint_confirmation")

    if @single.created_with_songwriter?
      @song_data_presenter = SongDataPresenter.new(@single, current_user.id)
      @spatial_audio_enabled = spatial_audio_enabled_and_allowed?
      @dolby_atmos_article_link = knowledgebase_link_for_article_id(KnowledgebaseLink::DOLBY_ATMOS_ARTICLE_ID, country_website)
      @cover_song_metadata_enabled = cover_song_metadata_enabled?
      @cover_song_article_link = knowledgebase_link_for_article_id(KnowledgebaseLink::COVER_SONG_ARTICLE_ID, country_website)
      @plan_upgrade_link = "/plans"
      @songs_app_translations = TranslationFetcherService
                                .translations_for("javascripts/songs_app", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
                                .with_support_links(
                                  format_release_for_stores: :format_release_for_stores,
                                  how_to_convert_your_audio_files: "how-to-convert-your-audio-files",
                                  how_do_i_format_my_album: "how-do-i-format-my-album",
                                  songwriter_song_roles_info: "songwriter-song-roles-info"
                                )
      @popup_modal_translations = TranslationFetcherService
                                  .translations_for("javascripts/popup_modal", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
                                  .translations["popup_modal"]

    end
    check_for_expired_preorder unless @single.payment_applied
  end

  def create
    @single = Single.new(
      single_params.except(:iso_codes).merge(
        {
          person: current_user,
          created_with: Album.created_withs[:songwriter],
          metadata_language_code_id: metadata_language_code_id(single_params)
        }
      )
    )

    @creatives = @single.creatives
    is_cyrillic = is_a_cyrillic_language?(params[:single][:language_code_legacy_support])

    set_release_countries(@single, single_params[:iso_codes])

    render :cyrillic_release and return false if is_cyrillic && @single.errors.empty? && @single.save

    if @single.errors.empty? && @single.save
      if FeatureFlipper.show_feature?(:salepoints_redesign, current_user)
        add_salepoints
        Album::AutomatorService.update_automator(@single, true)
      end
      Note.create(related: @single, note_created_by: current_user, ip_address: request.remote_ip, subject: "Single created", note: "Single was created.")
      create_artist_ids

      flash[:success] = custom_t("controllers.singles.single_created_upload_file")
      single_redirect
    else
      flash[:error] = custom_t("controllers.singles.error_creating_single", { count: @single.errors.size })
      @single.golive_date = convert_golive_to_est(@single.golive_date)
      @single_clean_version_selector_enabled = single_clean_version_selector_enabled?
      rebuild_territory_picker(single_params[:iso_codes])
      load_upc_link
      render :edit
    end
  end

  def cyrillic_release
    render layout: "application"
  end

  def edit
    @single_clean_version_selector_enabled = single_clean_version_selector_enabled?
    @creatives = @single.creatives
    load_upc(@single)
    load_selected_countries(@single)
    territory_picker_state

    return unless album_app_enabled?

    render layout: "tc-foundation", template: "singles/album_app" and return false
  end

  def update
    set_release_countries(@single, single_params[:iso_codes])
    @single.assign_attributes(
      single_params.merge(
        {
          metadata_language_code_id: metadata_language_code_id(single_params)
        }
      )
            .except(:iso_codes)
    )

    if @single.errors.empty? && @single.save
      check_for_expired_preorder unless @single.payment_applied
      create_artist_ids
      flash[:success] = custom_t("controllers.singles.single_created_upload_file") unless flash[:album_notice]
      redirect_to single_path(@single)
    else
      @creatives = @single.creatives
      @single.golive_date = convert_golive_to_est(@single.golive_date)
      load_upc(@single)
      flash[:error] = custom_t("controllers.singles.error_updating_single", { count: @single.errors.size })
      rebuild_territory_picker(single_params[:iso_codes])
      render action: :edit
    end
  end

  def monetize
    @page_title = custom_t("controllers.singles.monetize_tracks_on_yt")
    @songs = YtmTracks.songs_for_album(@single)
  end

  def confirm_monetize
    @page_title = custom_t("controllers.singles.confirm_tracks_for_monetization")
    @songs_to_monetize =
      params[:songs].reject do |s|
        s["send_or_block_#{s[:id]}"].blank?
      end

    return unless @songs_to_monetize.empty?

    flash[:error] = custom_t("controllers.singles.no_songs_selected")
    redirect_back fallback_location: dashboard_path
  end

  protected

  def set_golive_date_est
    @single.golive_date = convert_golive_to_est(@single.golive_date)
  end

  def load_editable_single
    @single = current_user.albums.find(params[:id])
    return true unless @single.finalized?

    # if current_user.is_administrator? # single is not editable
    #   flash.now[:error] = 'This single has already been pushed to stores - edit with caution'
    # else
    flash.now[:error] = custom_t("controllers.singles.your_release_cannot_be_edited")
    # end
  end

  def load_single
    @single =
      if current_user.is_administrator?
        Album.find(params[:id])
      else
        current_user.albums.find(params[:id])
      end
    session[:current_album_id] = @single.id
    redirect_to_polymorphic_type(@single)
  rescue => e
    flash[:error] = custom_t("controllers.singles.unable_to_load_that_page")
    redirect_to dashboard_path
  end

  def load_yt_store
    @yt_store = Store.find_by(short_name: "YoutubeSR")
  end

  def load_songs
    @songs = @single.songs.preload(:ytm_ineligible_song, salepoint_songs: :salepoint)
  end

  def check_for_expired_preorder
    return unless @single.preorder_date_expired?

    if @single.sale_date_supports_preorder?
      @single.salepoint_preorder_data.each { |spd| spd.update start_date: (@single.sale_date - 1.day) }
      flash[:album_notice] = custom_t("controllers.singles.single_was_saved_distribute")
    else
      @single.salepoint_preorder_data.each { |spd| spd.update enabled: false, start_date: nil }
      flash[:album_notice] = custom_t("controllers.singles.single_was_saved_preorder")
    end
  end

  def single_redirect
    if session[:tiktok_upsell]
      session[:tiktok_upsell] = false
      redirect_to single_path(@single, popup: "tiktok_promo")
    else
      redirect_to single_path(@single)
    end
  end

  def single_params
    result = initial_single_params

    if initial_single_params[:golive_date].present?
      new_golive_date = convert_date_hash_to_golive_date(initial_single_params[:golive_date])
      new_sale_date   = convert_date_hash_to_sale_date(initial_single_params[:golive_date])
      result = result.merge(
        golive_date: new_golive_date,
        sale_date: new_sale_date
      )
    end

    result = reject_optional_upc(result) if reject_optional_upc_number?(@single, result)
    result
  end

  def initial_single_params
    params
      .require(:single)
      .permit(
        :name,
        :parental_advisory,
        :clean_version,
        :iso_codes,
        :timed_release_timing_scenario,
        :language_code_legacy_support,
        :primary_genre_id,
        :secondary_genre_id,
        :sale_date,
        :song_id,
        :previously_released,
        :orig_release_year,
        :label_name,
        :optional_upc_number,
        :optional_isrc,
        :recording_location,
        creatives: [:id, :role, :name],
        golive_date: [
          :month,
          :day,
          :year,
          :hour,
          :min,
          :meridian
        ]
      )
  end
end
