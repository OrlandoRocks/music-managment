class BraintreeTransactionsController < ApplicationController
  before_action :check_admin_login, only: :index

  # =================================================================================
  # = stub allows for full integration testing against the braintree transparent redirect API
  # = It acts just like Braintree's transact.php url by redirecting with the correct parameters to
  # = the payment_controller.receive_transaction
  # =================================================================================
  def stub
    raise "There was an error processing your request." unless Rails.env.test?

    response =
      if params[:response_code] == "100"
        "1"
      else
        "3"
      end

    response_code = params[:response_code]
    response_text = params[:response_text].nil? ? "SUCCESS" : params[:response_text]

    response = {
      response: response,
      responsetext: response_text,
      authcode: "123456",
      transactionid: "1073209378",
      avsresponse: "N",
      cvvresponse: "N",
      orderid: params[:orderid],
      type: "sale",
      response_code: response_code,
      username: BRAINTREE[:key_id],
      time: "20090825202153",
      amount: params[:amount],
      currency: params[:currency],
      processor_id: params[:processor_id]
    }

    response = response.merge(customer_vault_id: "931471295") if params[:customer_vault]

    if params[:response] == "valid"
      hash_string = [
        response[:orderid],
        response[:amount],
        response[:response],
        response[:transactionid],
        response[:avsresponse],
        response[:cvvresponse],
        response[:customer_vault_id],
        "20090825202153",
        BRAINTREE[:key]
      ].compact.join("|")
      hash = Digest::MD5.hexdigest(hash_string)
    else
      hash = "2333c5d10c67893711da0e6b63sfdgfgdaa3f8eb"
    end

    response = response.merge(hash: hash)
    redirect_to controller: "payment", action: "receive_transaction", params: response
  end
end
