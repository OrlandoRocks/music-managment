class TourDatesController < ApplicationController
  before_action :load_person
  before_action :load_editable_tour_date, except: [:index, :new, :create]
  before_action :load_countries_and_states, only: [:new, :edit]
  before_action :translate_dates, only: [:create, :update]

  layout "application"

  def index
    respond_to do |format|
      format.html
    end
  end

  def new
    @tour_date = TourDate.new
  end

  def create
    @tour_date = TourDate.new((tour_date_params || {}).merge(person_id: @person.id))
    respond_to do |format|
      if @tour_date.save
        flash[:notice] = custom_t("controllers.tour_dates.success_creating")
        format.html { redirect_to tour_dates_path }
      else
        load_countries_and_states
        format.html { render action: "new" }
      end
    end
  end

  def edit
  end

  def update
    if @tour_date.update(tour_date_params)
      flash[:notice] = custom_t("controllers.tour_dates.success_updating")

      redirect_to tour_dates_path
    else
      load_countries_and_states
      respond_to do |format|
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    if @tour_date.destroy
      flash[:notice] = custom_t("controllers.tour_dates.success_deleting")
    else
      flash[:error] = custom_t("controllers.tour_dates.error_deleting")
    end
    redirect_to tour_dates_path
  end

  private

  def tour_date_params
    params.require(:tour_date).permit(
      :person_id,
      :artist_id,
      :country_id,
      :us_state_id,
      :city,
      :venue,
      :information,
      :event_time_at,
      :event_date_at
    )
  end

  def load_editable_tour_date
    @tour_date = @person.tour_dates.find(params[:id])
  end

  def load_countries_and_states
    @countries = Country.all
    @us_states = UsState.all
  end
end
