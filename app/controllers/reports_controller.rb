class ReportsController < ApplicationController
  include Tunecore::CsvExporter
  before_action :parse_options, :setup_options

  helper_method :build_report_row
  helper_method :build_total_row
  helper_method :build_adjustment_row

  protected

  def build_report_row(csv)
    csv << yield
  end

  def build_adjustment_row(csv, spaces = 3)
    row = []
    spaces.times do |_space|
      row << ""
    end
    row << "Currency Conversion Adjustment"
    row << yield
    csv << row
  end

  def build_total_row(csv)
    csv << yield
  end

  def default_options
    {
      year: @year,
      month: @month
    }
  end

  def render_report
    respond_to do |format|
      format.html
      format.js { render layout: false }
      format.csv {
        set_csv_headers(csv_filename)
        render layout: false
      }
    end
  end

  def csv_filename
    result = ""
    result += controller_name
    result += "-#{@year}"  if @year
    result += "-#{@month}" if @month
    result += ".csv"
  end

  def is_csv_request
    request.format == :csv
  end

  #
  #  Filters for Reports:
  #
  def parse_options
    set_year
    set_month
    set_page
    set_order
    set_per_page
  end

  def set_order
    @order = params[:order] || :name
  end

  def set_per_page
    @per_page = params[:per_page].blank? ? 10 : params[:per_page].to_i
  end

  def set_year
    @year = params[:year].to_i if params[:year].present?
  end

  def set_month
    @month = params[:month].to_i if params[:month].present?
  end

  def set_page
    @page = (params[:page] || 1).to_i
  end

  #
  #  Options for the filter controls
  #
  def setup_options
    setup_months
    setup_years
    setup_per_page_options
  end

  def setup_months
    i = -1
    @months =
      Date::MONTHNAMES.map { |name|
        [name, i += 1]
      }
    @months[0] = ["All", nil]
  end

  def setup_years
    empty = ["All", nil]
    @years = [empty] | available_years.map { |year| [year.to_s, year.to_s] }
  end

  def available_years
    SalesReport.all_years(current_user.id).reverse
  end

  def setup_per_page_options
    @per_page_options = [5, 10, 25, 50]
  end
end
