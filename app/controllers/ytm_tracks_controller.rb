class YtmTracksController < ApplicationController
  include YtmRedirector

  before_action :redirect_to_youtube_track_monetization_dashboard
  helper_method :sort_column, :sort_direction
  before_action :require_ytm
  before_action :blocked_ytm
  before_action :load_ytm_tracks, only: [:show, :submit_all_confirm]

  layout "application"

  def show
    @page_title = custom_t("controllers.ytm_tracks.tracks_monetization")
    @songs, any_missing_salepoints = @ytm_tracks.search(
      status: params[:status],
      releases: params[:release],
      artists: params[:artist],
      search: params[:search],
      page: params[:page],
      per_page: params[:per_page],
      sort: sort_column,
      direction: sort_direction
    )

    @unsent_songs   = @ytm_tracks.unsent_songs
    vetting_status  = YoutubeMonetization.vetting_status(current_user)
    pending_reviews = current_user.albums_left_to_review

    if pending_reviews.positive? && flash[:notice].nil?
      flash[:notice] = custom_t("controllers.ytm_tracks.tracks_notyet_approved").html_safe
    end

    if any_missing_salepoints || (pending_reviews.zero? && vetting_status != "all") && flash[:notice].nil?
      flash[:notice] = custom_t("controllers.ytm_tracks.setting_up_account")
    end

    if !any_missing_salepoints && pending_reviews.zero? && vetting_status == "all"
      @all_tracks_shown = custom_t("controllers.ytm_tracks.tracks_avail_yt_monetization")
    end

    respond_to do |format|
      format.js {
        render partial: "monetizations", content_type: "text/html", locals: { songs: @songs }
      }
      format.html
    end
  end

  def submit_all_confirm
    @page_title = custom_t("controllers.ytm_tracks.confirm_tracks")
    @songs, any_missing_salepoints = @ytm_tracks.unsent_songs_search

    redirect_to ytm_tracks_path and return if @songs.empty?

    flash[:notice] = custom_t("controllers.ytm_tracks.see_tracks") if any_missing_salepoints

    render :confirm
  end

  def confirm
    if params[:songs].nil?
      flash[:error] = custom_t("controllers.ytm_tracks.no_songs_selected")
      redirect_to ytm_tracks_path and return
    end

    @page_title = custom_t("controllers.ytm_tracks.confirm_tracks")
    @songs =
      params[:songs].reject do |s|
        s["id"].blank?
      end
  end

  private

  def sort_column
    if %w[
      songs.name
      track_isrc
      release_upc
      prim_artist_name
      artist_names
      album_name
      status
    ].include?(params[:sort])

      params[:sort]
    else
      "status"
    end
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def load_ytm_tracks
    @ytm_tracks = YtmTracks.new(current_user)
  end

  def redirect_to_youtube_track_monetization_dashboard
    redirect_to youtube_track_monetization_dashboard_path
  end
end
