class SongsController < ApplicationController
  include CreativeSystem::Controller
  before_action :load_person
  before_action :load_album
  before_action :ensure_song_changes_permitted, only: [:create, :destroy]
  before_action :set_song_partial

  helper_method :creative_roles_for_select

  layout "application_old"

  #
  #  Display list of songs
  #
  def index
    @songs = @album.songs
    @song = Song.new

    respond_to do |format|
      format.html
      format.js {
        if enable_bigbox_uploader?
          render partial: @song_partial, collection: @songs
        else
          render partial: "song", collection: @songs
        end
      }
    end
  end

  #
  #  Markup for new song record
  #
  def new
    load_new_song
    @creatives = @song.creatives

    respond_to do |format|
      if !request.xhr?
        format.html
      else
        format.html { render layout: false }
      end
    end
  end

  #
  #  Create new song record
  #
  def create
    song_params = update_params.merge(album_id: @album.id)
    @song = Song.new(song_params)
    @creatives = @song.creatives

    if @song.save
      Note.create(
        related: @album,
        note_created_by: current_user,
        ip_address: request.remote_ip,
        subject: "Song created",
        note: "Song '#{@song.name}' (ID: #{@song.id}) created on Album"
      )

      respond_to do |format|
        if request.xhr?
          format.html {
            if enable_bigbox_uploader?
              render partial: @song_partial, object: @song, locals: { album: @album }, status: :ok
            else
              render partial: "song", object: @song, status: :ok
            end
          }
        else
          format.html {
            redirect_to controller: "album", action: "show", id: @album.id
          }
        end
      end

    else
      respond_to do |format|
        if request.xhr?
          format.js {
            render action: "new", layout: false
          }
        end
      end
    end
  end

  #
  #  Edit HTML for Song
  #
  def edit
    @song = @album.songs.find(params[:id])
    @creatives = @song.creatives

    respond_to do |format|
      if request.xhr?
        format.js {
          if enable_bigbox_uploader?
            render partial: "edit_bigbox", layout: false
          else
            render layout: false
          end
        }
      else
        format.html {
          render partial: "edit_bigbox" if enable_bigbox_uploader?
        }
      end
    end
  end

  #
  #  Update song record
  #
  def update
    @song = @album.songs.find(params[:id])

    if update_params.blank?
      respond_to do |format|
        if request.xhr?
          format.js {
            render partial: "edit_bigbox", layout: false and return
          }
        end
      end
    end

    update_params[:creatives] = [] unless update_params[:creatives]

    @song.update(update_params)
    @creatives = @song.creatives
    if @song.save
      Note.create(
        related: @album,
        note_created_by: current_user,
        ip_address: request.remote_ip,
        subject: "Song updated",
        note: "Song '#{@song.name}' (ID: #{@song.id}) was updated on Album"
      )
      respond_to do |format|
        if request.xhr?
          format.js {
            if enable_bigbox_uploader?
              render partial: @song_partial, object: @song, locals: { album: @album }, layout: false
            else
              render partial: "song", object: @song
            end
          }
        else
          format.html { flash[:notice] = custom_t("controllers.songs.song_updated") }
        end
      end
    else
      respond_to do |format|
        if request.xhr?
          format.js {
            if enable_bigbox_uploader?
              render partial: "edit_bigbox", layout: false
            else
              render action: "new", layout: false
            end
          }
        end
      end
    end
  end

  #
  #  Destroy Song Record and uploaded files
  #
  def destroy
    @song = @album.songs.find(params[:id])
    @song.destroy
    Note.create(
      related: @album,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Song deleted",
      note: "Song '#{@song.name}' (ID: #{@song.id}) deleted on Album"
    )

    respond_to do |format|
      format.html {
        redirect_to polymorphic_path(@album)
      }
      format.js {
        if enable_bigbox_uploader?
          render partial: @song_partial, collection: @album.songs, locals: { album: @album }
        else
          render partial: "song", collection: @album.songs
        end
      }
    end
  end

  #
  #  Show Song Record
  #
  def show
    @song = @album.songs.find(params[:id])
    gon_custom_t("confirm_deletion", "songs.delete.are_you_sure")
    gon_custom_t("could_not_upload", "songs.upload.could_not_upload")
    gon_custom_t("try_again", "songs.upload.try_again")

    respond_to do |format|
      if enable_bigbox_uploader?
        format.js { render partial: @song_partial, object: @song, locals: { album: @album } }
      else
        format.js { render partial: "song", object: @song }
      end
    end
  end

  #
  #  HTML form for song file upload
  #
  def new_upload
    @song = @album.songs.find(params[:id])
    respond_to do |format|
      format.html
      format.js { render layout: false }
    end
  end

  protected

  def set_song_partial
    @song_partial = "song_bigbox"
  end

  def load_new_song
    @song = Song.new
    @song.track_num = @album.next_available_track_number
    @song.album = @album
  end

  def load_album
    # added to allow admin to edit songs
    @album ||=
      if @person.is_administrator
        Album.find(params[:album_id])
      else
        @person.albums.find(params[:album_id])
      end
  end

  def ensure_song_changes_permitted
    if !@album.permit_song_changes?
      flash[:error] = custom_t("controllers.songs.cannot_add_or_remove")
      redirect_to(album_path(@album))
      false
    else
      true
    end
  end

  def create_params
    update_params
  end

  def update_params
    params
      .require(:song)
      .permit(:name, :track_num, :parental_advisory, :clean_version, creatives: [:name, :role])
  end
end
