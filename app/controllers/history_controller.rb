class HistoryController < ApplicationController
  before_action :load_person

  layout "application_old"

  def index
    redirect_to "/people/#{current_user.id}/edit/?tab=history"
  end
end
