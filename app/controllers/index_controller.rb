class IndexController < ApplicationController
  include ReferralData::Parsable
  include MetaTags::Taggable

  skip_before_action :login_required
  before_action :create_referral_cookies
  before_action :redirect_to_cms, except: [:contact_bizdev, :bizdev]
  before_action :gotodash, only: [:index]
  before_action :set_page_title
  before_action :set_meta_tags
  before_action :set_canonical_link

  # There are hard-coded '/index/...' paths in our app (see e.g. footer),
  # which in production never hit nginx/puma,
  # and instead are pre-emptively redirected from the app subdomain ('web.'),
  # to the CMS, AKA 'marketing site' ('www.'),
  # via Cloudflare.
  #
  # Not to be confused with the #index action here, which redirects only
  # the base path '/' to the CMS,
  # yet is irrelevant in production because of a pre-emptive redirect via Cloudflare.

  def index
    (redirect_to country_specific_domain and return) if cms_enabled?

    # Not redirected, therefore in a lower environment.

    @reskin = false

    render layout: "application"
  end

  def gotodash
    return if current_user.nil?

    if feature_enabled?(:sync_licensing)
      redirect_to week_sync_chart_path
    else
      redirect_to dashboard_path(go_to_dash_params)
    end
  end

  def contact_bizdev
    PersonNotifier.contact_bizdev(params[:bizdev]).deliver
    PersonNotifier.bizdev_thankyou(params[:bizdev]).deliver

    flash[:notice] = "Thank you for your inquiry."
    redirect_to action: "bizdev"
  end

  def bizdev; end

  private

  def redirect_to_cms
    redirect_to "#{country_specific_domain}#{request.path}" if cms_enabled?
  end

  def go_to_dash_params
    params.permit(:pb)
  end
end
