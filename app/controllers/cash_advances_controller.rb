class CashAdvancesController < ApplicationController
  include TwoFactorAuthable
  before_action :two_factor_authenticate!, only: [:request_advance], if: :two_factor_auth_required?

  def create
    render CashAdvanceRegistrationService.new(current_user).token
  end

  def request_advance
    redirect_path = params[:redirect_path] || list_transactions_path
    redirect_to "#{redirect_path}?open_lyric_popup=true"
  end
end
