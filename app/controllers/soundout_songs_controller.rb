class SoundoutSongsController < ApplicationController
  def index
    @songs = Tunecore::MusicSearch.song_search(current_user, params)
    data = {
      songs: @songs.as_json(
        root: false,
        only: [:id, :album_id, :name, :artist_name, :track_num, :longer_than_90s]
      )
    }

    if params[:page] || params[:per_page]
      data[:total_count] = @songs.total_entries
      data[:per_page] = @songs.per_page
      data[:total_pages] = @songs.total_pages
      data[:current_page] = @songs.current_page
    end

    render json: data
  end
end
