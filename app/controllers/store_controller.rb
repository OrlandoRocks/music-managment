class StoreController < ApplicationController
  before_action :if_not_us

  layout "application_old"

  def if_not_us
    if current_user
      redirect_to controller: "index" if current_user && current_user.country_domain != "US"
    elsif country_website == "CA"
      redirect_to controller: "index"
    end
  end

  def index
    @page_title = custom_t("controllers.store.store")
    @new_css = true
    @products = PromotionalProduct.where("is_deleted = false AND online = true").order("position")
  end

  def product
    if params[:product_id].to_i == 62 || params[:product_id].to_i == 40
      redirect_to knowledgebase_link_for("help-homepage", country_website) and return
    end

    @product    = PromotionalProduct.find_by(url_slug: params[:product_id], is_deleted: false, online: true) || PromotionalProduct.find_by(id: params[:product_id], is_deleted: false, online: true)
    @page_title = @product.short_title
    @new_css    = true
    tag         = Struct.new(:name, :content)
    desc_tag    = tag.new("description", @product.meta_description)
    keyw_tag    = tag.new("keywords", @product.meta_keywords)
    @metatags   = []
    @metatags   << desc_tag
    @metatags   << keyw_tag
  rescue
    redirect_to controller: "store"
  end
end
