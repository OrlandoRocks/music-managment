class SpecializedSinglesController < SinglesController
  before_action :load_specialized_release_variables
  before_action :redirect_to_standard_release

  def new
    super
  end

  def show
    super
  end

  private

  def load_specialized_release_variables
    @specialized_release_type = nil
  end

  def redirect_to_polymorphic_type(_album)
    nil
  end

  def redirect_to_standard_release
    return if params[:action] == "new" && specialized_releases_enabled?

    album = @album || @ringtone || @single || Album.find_by(id: params[:id])

    return unless !specialized_releases_enabled? || !verified_specialized_single(album)

    if album
      redirect_to polymorphic_path(album)
    else
      redirect_to request.original_url.gsub(params[:controller], "singles")
    end
  end

  def verified_specialized_single(album)
    album.present?
  end
end
