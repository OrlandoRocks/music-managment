class StoreSalesReportsController < ReportsController
  def index
    @store_report = current_user.sales_report(default_options)
    @stores = @store_report.stores
    render_report
  end
end
