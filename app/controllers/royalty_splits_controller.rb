# frozen_string_literal: true

class RoyaltySplitsController < ApplicationController
  before_action :check_feature_flag
  before_action :only_if_user_can_create_splits,
                except: [:invited_splits_onboarding, :invited_splits, :accept_all_splits, :accept_split]
  before_action :only_if_user_can_accept_splits, only: [:accept_all_splits, :accept_split]

  before_action :check_suspicious_account, except: [:invited_splits, :invited_splits_onboarding]

  before_action :set_royalty_split, only: [:show, :edit, :update, :destroy]
  before_action :set_owned_royalty_splits, only: [:albums, :add_songs]
  before_action :set_albums, only: [:albums, :add_songs]

  before_action :set_selected_royalty_split_for_albums_pages, only: [:albums, :add_songs]
  before_action :set_songs, only: [:add_songs]

  before_action :set_royalty_splits_js_i18n, only: [:index, :edit, :update, :new, :create, :albums, :add_songs]

  rescue_from ActionController::ParameterMissing, with: :bad_request

  layout "application"

  def index
    @royalty_splits = current_user.owned_royalty_splits
                                  .includes({ recipients: :person }, { albums: :artwork })
                                  .left_outer_joins(:royalty_split_songs)
                                  .group(:"royalty_splits.id")
                                  .order("count(royalty_split_songs.id) desc, title, id")
  end

  def show
    redirect_to edit_royalty_split_path(@royalty_split)
  end

  def update
    if @royalty_split.update(royalty_split_params)
      redirect_to edit_royalty_split_path(@royalty_split), notice: custom_t("royalty_splits.success")
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def new
    @royalty_split = RoyaltySplit.new_with_owner(current_user)
  end

  def create
    @royalty_split = royalty_split_from_params_with_owner

    if @royalty_split.save
      redirect_to royalty_splits_path, notice: custom_t("royalty_splits.success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    render json: custom_t("royalty_splits.split_config_delete_not_found"),
           status: :not_found and return if @royalty_split.royalty_split_songs.exists?

    render json: @royalty_split.destroy!
  rescue ActiveRecord::RecordNotDestroyed
    render json: custom_t("royalty_splits.split_config_not_destroyed"), status: :unprocessable_entity
  end

  def add_songs
    is_new_split = @selected_royalty_split.nil?
    if is_new_split
      # Create new split config
      new_royalty_split = royalty_split_from_params_with_owner
      if new_royalty_split.valid?
        # Created new split, can assign songs to it.
        @selected_royalty_split = new_royalty_split
      else
        @edit_form_royalty_split = new_royalty_split
        render :albums, status: :unprocessable_entity and return
      end
    end

    # Empty "create split" form.
    @edit_form_royalty_split = RoyaltySplit.new_with_owner(current_user)

    RoyaltySplit.transaction do
      @selected_royalty_split.save! if is_new_split
      @selected_royalty_split.associate_songs!(@songs.includes(:person), new_split: is_new_split)
    rescue ActiveRecord::RecordInvalid
      flash.now[:alert] = custom_t("royalty_splits.error_applying_split_to_songs")
      render :albums, status: :unprocessable_entity and return
    end

    redirect_to helpers.get_albums_royalty_splits_path(@albums), notice: custom_t("royalty_splits.success")
  end

  def remove_song
    @royalty_split_song = RoyaltySplitSong.find(params[:royalty_split_song_id])
    render json: I18n.t("royalty_splits.forbidden"),
           status: :forbidden and return unless @royalty_split_song.owner == current_user

    render json: @royalty_split_song.destroy!
  rescue ActiveRecord::RecordNotFound
    render json: I18n.t("royalty_splits.songs_split_config_not_found"), status: :not_found
  rescue ActiveRecord::RecordNotDestroyed
    render json: custom_t("royalty_splits.split_config_not_removed_from_song"), status: :unprocessable_entity
  end

  def albums
    # If @selected_royalty_split is (still) nil, then it'll show the create preset form
    @selected_royalty_split ||= RoyaltySplit.with_the_most_songs(album_ids: @albums.map(&:id))
    # Used to populate blank create form, overwrites old value of @royalty_split
    @edit_form_royalty_split = RoyaltySplit.new_with_owner(current_user)
  end

  def invited_splits
    @invited_royalty_splits = RoyaltySplit.invited(current_user)
                                          .includes(:recipients, { albums: :artwork })
  end

  def invited_splits_onboarding
    if current_user.blank?
      redirect_to signup_path
    elsif current_user.can_accept_splits?
      redirect_to(invited_royalty_splits_path, status: :see_other)
    else
      redirect_to plans_path
    end
  end

  def accept_split
    @recipient = current_user.royalty_split_recipients.where(accepted_at: nil, royalty_split_id: params[:id]).first
    @recipient.update!(accepted_at: Time.current) if @recipient.present?
    redirect_to invited_royalty_splits_path
  end

  def accept_all_splits
    accepted_at = Time.current
    current_user.royalty_split_recipients.pending_invites.map { |recipient|
      recipient.update(accepted_at: accepted_at)
    }
    redirect_to invited_royalty_splits_path
  end

  private

  def royalty_split_from_params_with_owner
    # TODO: Can create a RoyaltySplit without an owner (via DOM manipulation)
    RoyaltySplit.new(royalty_split_params) do |royalty_split|
      royalty_split.owner = current_user
    end
  end

  def set_owned_royalty_splits
    @owned_royalty_splits = current_user.owned_royalty_splits.includes(
      { albums: :artwork }, :songs,
      { recipients: :person }
    )
  end

  def set_royalty_split
    @royalty_split = current_user.owned_royalty_splits.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    respond_to do |format|
      format.html {
        redirect_to royalty_splits_path,
                    alert: custom_t("royalty_splits.split_config_not_found_for_user")
      }
      format.json { head :not_found }
    end
  end

  def set_selected_royalty_split_for_albums_pages
    @selected_royalty_split = @owned_royalty_splits.find(chosen_royalty_split_id) if chosen_royalty_split_id.present?
  rescue ActiveRecord::RecordNotFound
    redirect_to helpers.get_albums_royalty_splits_path(@albums),
                alert: custom_t("royalty_splits.split_config_not_found_for_user")
  end

  def set_albums
    @albums = current_user.albums
                          .where(id: album_ids_params)
                          .includes(:artwork, songs: { royalty_split: { recipients: :person } })
    redirect_to discography_path,
                notice: custom_t("royalty_splits.albums_not_found_for_current_user") and return if @albums.blank?
  rescue ActionController::ParameterMissing
    redirect_to discography_path, notice: custom_t("royalty_splits.album_missing_from_url")
  end

  def set_songs
    @songs = current_user.songs.where(id: song_ids_params)
    return if @songs.present?

    flash.now[:alert] = custom_t("royalty_splits.no_valid_songs_selected")
    @edit_form_royalty_split =
      if chosen_royalty_split_id.blank?
        royalty_split_from_params_with_owner
      else
        RoyaltySplit.new_with_owner(current_user)
      end
    render :albums, status: :not_found
  end

  def chosen_royalty_split_id
    params[:chosen_royalty_split_id]
  end

  def song_ids_params
    params.permit(song_ids: [])[:song_ids]
  end

  def album_ids_params
    params.permit(album_ids: []).require(:album_ids)
  end

  def royalty_split_params
    params.require(:royalty_split).permit(:title, recipients_attributes: [:id, :email, :person_id, :percent, :_destroy])
  end

  def check_feature_flag
    redirect_to discography_path unless current_user.show_invited_splits_based_on_feature_flag?
  end

  def only_if_user_can_create_splits
    head :forbidden unless current_user.can_create_splits?
  end

  def only_if_user_can_accept_splits
    head :forbidden unless current_user.can_accept_splits?
  end

  def set_royalty_splits_js_i18n
    gon.push(email_already_exists_in_form: custom_t("royalty_splits.email_already_exists_in_form"))
    gon.push(remove_split_config_confirmation: custom_t("royalty_splits.remove_split_config_confirmation"))
    gon.push(deleted_split_config: custom_t("royalty_splits.deleted_split_config"))
    gon.push(you_must_select_a_song: custom_t("royalty_splits.you_must_select_a_song"))
    gon.push(will_affect_X_songs_Y_releases: custom_t("royalty_splits.will_affect_X_songs_Y_releases"))
  end

  def check_suspicious_account
    head :forbidden if current_user.flagged_suspicious?
  end

  def bad_request
    head :bad_request
  end
end
