class TrendReportsController < ApplicationController
  include Tunecore::CsvExporter
  include YoutubeOAC

  before_action :plan_feature_gating
  before_action :load_person
  before_action :set_download_provider_ids
  before_action :set_stream_provider_ids
  before_action :set_provider_names
  before_action :set_provider_ids
  before_action :load_duration_array, only: [:index, :song_view, :release_view]

  layout "application"

  def index
    if FeatureFlipper.show_feature?(:disable_trends, current_user)
      render "trends_disabled"
    else
      @duration_array = [["1 #{custom_t(:day)}", "1d"], ["2 #{custom_t(:day)}", "2d"], ["3 #{custom_t(:days)}", "3d"], ["4 #{custom_t(:days)}", "4d"], ["5 #{custom_t(:days)}", "5d"], ["1 #{custom_t(:week)}", "1w"], ["2 #{custom_t(:weeks)}", "2w"], ["1 #{custom_t(:month)}", "1M"], ["2 #{custom_t(:months)}", "2M"], ["3 #{custom_t(:months)}", "3M"]]
      @page_title = custom_t("controllers.trend_reports.trend_data")
      set_system_status
      set_start_end_dates

      selected_providers      = @download_provider_ids + @streaming_provider_ids

      @ringtones_sold         = TrendDataSummary.ringtones_sold?(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_download_releases  = TrendDataSummary.top_downloaded_releases(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_stream_releases    = TrendDataSummary.top_streamed_releases(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_download_songs     = TrendDataSummary.top_downloaded_songs(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_stream_songs       = TrendDataSummary.top_streamed_songs(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_ringtones          = TrendDataSummary.top_ringtones(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @counts_by_day          = TrendDataSummary.trend_data_counts_by_day(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @downloads, @streams    = @counts_by_day.partition { |hash| @download_provider_ids.include? hash[:provider_id].to_i }
      @album_downloads        = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.album_download.id }
      @song_downloads         = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.song_download.id }
      @ringtone_downloads     = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.ringtone.id }
      @top_download_countries = TrendDataSummary.top_downloaded_countries(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_stream_countries   = TrendDataSummary.top_streamed_countries(current_user, selected_providers, @date_range.start_date, @date_range.end_date)
      @top_us_markets         = TrendDataSummary.top_us_markets(current_user, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
      @has_never_distributed  = current_user.has_never_distributed?

      set_promotional_ticker

      respond_to do |format|
        format.html {
          @trend_count = TrendDataSummary.all_trend_data_count(current_user, @provider_ids, @date_range.start_date, @date_range.end_date)
        }

        format.csv {
          @trend_data, @trend_count = TrendDataSummary.all_trend_data(current_user, @provider_ids, @date_range.start_date, @date_range.end_date)
          stream_trend_data(@trend_data, " trend_data_#{@date_range.start_date.strftime('%Y-%m-%d')}_to_#{@date_range.end_date.strftime('%Y-%m-%d')}.csv")
        }
      end
    end
  end

  def release_view
    @release = current_user.albums.find(params[:album_id])
    @page_title = custom_t("controllers.trend_reports.release_name", release_name: @release.name)
    set_system_status
    set_start_end_dates
    @ringtones_sold         = TrendDataSummary.ringtones_sold?(current_user, @provider_ids, @date_range.start_date, @date_range.end_date)
    @top_download_songs     = TrendDataSummary.top_downloaded_songs(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
    @top_stream_songs       = TrendDataSummary.top_streamed_songs(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
    @counts_by_day          = TrendDataSummary.trend_data_counts_by_day(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
    @downloads, @streams    = @counts_by_day.partition { |hash| @download_provider_ids.include? hash[:provider_id].to_i }
    @album_downloads        = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.album_download.id }
    @song_downloads         = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.song_download.id }
    @top_download_countries = TrendDataSummary.top_downloaded_countries(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
    @top_stream_countries   = TrendDataSummary.top_streamed_countries(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
    @top_us_markets         = TrendDataSummary.top_us_markets(current_user, @date_range.start_date, @date_range.end_date, album: @release)

    respond_to do |format|
      format.html {
        @trend_count = TrendDataSummary.all_trend_data_count(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
        render "index"
      }

      format.csv {
        @trend_data, @trend_count = TrendDataSummary.all_trend_data(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release)
        stream_trend_data(@trend_data, "#{@release.name} trend_data_#{@date_range.start_date.strftime('%Y-%m-%d')}_to_#{@date_range.end_date.strftime('%Y-%m-%d')}.csv")
      }
    end
  end

  def song_view
    @release = current_user.albums.find(params[:album_id])
    @song    = @release.songs.find(params[:song_id])
    @page_title = custom_t("controllers.trend_reports.release_name_song_name", release_name: @release.name, song_name: @song.name)
    set_system_status
    set_start_end_dates
    @ringtones_sold         = TrendDataSummary.ringtones_sold?(current_user, @provider_ids, @date_range.start_date, @date_range.end_date)
    @counts_by_day          = TrendDataSummary.trend_data_counts_by_day(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
    @downloads, @streams    = @counts_by_day.partition { |hash| @download_provider_ids.include? hash[:provider_id].to_i }
    @album_downloads        = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.album_download.id }
    @song_downloads         = @downloads.select { |hash| hash[:trans_type_id].to_i == TransType.song_download.id }
    @top_download_countries = TrendDataSummary.top_downloaded_countries(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
    @top_stream_countries   = TrendDataSummary.top_streamed_countries(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
    @top_us_markets         = TrendDataSummary.top_us_markets(current_user, @date_range.start_date, @date_range.end_date, album: @release, song: @song)

    respond_to do |format|
      format.html {
        @trend_count = TrendDataSummary.all_trend_data_count(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
        render "index"
      }

      format.csv {
        @trend_data, @trend_count = TrendDataSummary.all_trend_data(current_user, @provider_ids, @date_range.start_date, @date_range.end_date, album: @release, song: @song)
        stream_trend_data(@trend_data, "#{@release.name} - #{@song.name} trend_data_#{@date_range.start_date.strftime('%Y-%m-%d')}_to_#{@date_range.end_date.strftime('%Y-%m-%d')}.csv")
      }
    end
  end

  private

  def set_system_status
    import_dates = TrendDataSummary.last_us_import

    @last_trend_imports           = {}
    @last_trend_imports[:amazon]  = import_dates.detect { |t| t["provider_id"] == Provider.amazon.last.try(:id)  }.try(:[], "trend_date")
    @last_trend_imports[:itunes]  = import_dates.detect { |t| t["provider_id"] == Provider.itunes.last.try(:id)  }.try(:[], "trend_date")
    @last_trend_imports[:spotify] = import_dates.detect { |t| t["provider_id"] == Provider.spotify.last.try(:id) }.try(:[], "trend_date")
  end

  #
  # If start_date and end date are blank we default the period to the
  # time from the end of the last sale report date to the date of the last trend
  #
  # We don't allow a period greater than MAX_DAY_RANGE days
  #
  def set_start_end_dates
    @last_trend_date = TrendDataSummary.last_trend_date(current_user, @provider_ids).map { |d| d["last_trend_date"] }.max
    @date_range = Tunecore::Trending::DateRange.new(
      @last_trend_date,
      params[:sd],
      params[:ed]
    )
    @locale_start_date = Tunecore::DateTranslator.translate(
      @date_range.formatted_start_date,
      "long_date",
      I18n.locale,
      :en
    )
    @locale_end_date = Tunecore::DateTranslator.translate(
      @date_range.formatted_end_date,
      "long_date",
      I18n.locale,
      :en
    )
  end

  def stream_trend_data(trend_data, filename)
    @response = ""
    set_csv_headers(filename)
    csv = CSV.new(@response, row_sep: "\r\n")
    csv << ["Date", "City", "State", "Country Code", "Zip", "Units", "Album", "Song", "Artist", "Song ISRC", "Album UPC", "Release Type", "Transaction Type", "Provider"]
    trend_data.each do |trend|
      csv << [
        trend.sale_date,
        trend.city,
        trend.state,
        trend.country_name,
        trend.zip,
        trend.quantity,
        trend.album_name,
        trend.song_name,
        trend.artist_name,
        isrc_for(trend),
        "'#{trend.album_upc}'",
        trend.album_type,
        trend.trans_type_name,
        Provider.find(trend.provider_id).try(:name)
      ]
    end
    render plain: @response
  end

  def isrc_for(trend)
    trend.optional_isrc.presence || trend.tunecore_isrc
  end

  def load_duration_array
    @duration_array = [
      ["1 #{custom_t(:day)}", "1d"],
      ["2 #{custom_t(:day)}", "2d"],
      ["3 #{custom_t(:days)}", "3d"],
      ["4 #{custom_t(:days)}", "4d"],
      ["5 #{custom_t(:days)}", "5d"],
      ["1 #{custom_t(:week)}", "1w"],
      ["2 #{custom_t(:weeks)}", "2w"],
      ["1 #{custom_t(:month)}", "1M"],
      ["2 #{custom_t(:months)}", "2M"],
      ["3 #{custom_t(:months)}", "3M"]
    ]
  end

  def set_provider_names
    @provider_names = {}

    Provider.all.each do |provider|
      @provider_names[provider.name] = provider.id
    end
  end

  def set_download_provider_ids
    @download_provider_ids ||=
      if params[:download_provider_ids]
        params[:download_provider_ids].split(",").map(&:to_i)
      else
        Provider.download.pluck(:id)
      end
    @form_download_provider_ids = @download_provider_ids.join(",")
  end

  def set_stream_provider_ids
    @streaming_provider_ids ||= Provider.streaming.pluck(:id)
  end

  def set_provider_ids
    @provider_ids ||= Provider.all.pluck(:id)
  end

  def plan_feature_gating
    return if current_user.feature_gating_can_do?(:trend_reports)

    flash[:error_dialog] = custom_t("plans.no_plan_access")
    flash[:error_dialog_title] = custom_t("plans.upgrade_modal_header")
    redirect_back fallback_location: dashboard_path
  end
end
