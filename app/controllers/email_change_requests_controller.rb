# frozen_string_literal: true

class EmailChangeRequestsController < ApplicationController
  include AfterCommitEverywhere
  before_action :load_person
  before_action :load_change_request
  before_action :redirect_to_dashboard_failure!, if: :invalid_request?

  def complete_verification
    process_verification!
  end

  private

  def load_change_request
    @change_request = EmailChangeRequest.find_by(token: params[:token], person: @person, finalized: false)
  end

  def invalid_request?
    !(change_request_present? && email_changed? && active_request?)
  end

  def change_request_present?
    @change_request.present?
  end

  def email_changed?
    @person.email != @change_request.new_email
  end

  def active_request?
    @change_request.created_at >= (Time.current - 24.hours)
  end

  def redirect_to_dashboard_failure!
    flash[:error] = custom_t("email_change_title") + ": " + custom_t("email_changed.fail")
    redirect_to dashboard_path
  end

  def redirect_to_dashboard_success!
    PersonNotifier.change_email(@person, @person.email, specified_email: @change_request.old_email).deliver
    flash[:notice] = custom_t("email_change_title") + ": " + custom_t("email_changed.success")
    redirect_to dashboard_path
  end

  def process_verification!
    ApplicationRecord.transaction do
      after_commit do
        Sift::EventService.update_account_email(@person, cookies, request)
        redirect_to_dashboard_success!
      end
      after_rollback { redirect_to_dashboard_failure! }

      @person.update!(email: @change_request.new_email)
      @change_request.update!(finalized: true)
    end
  end
end
