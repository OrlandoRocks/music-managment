class SoundoutReportsController < ApplicationController
  layout "application"

  skip_before_action :login_required, only: [:available]
  skip_before_action :verify_authenticity_token, only: [:available], raise: false
  before_action :load_report, except: [:index, :purchase, :available, :purchase_reports]
  http_basic_authenticate_with name: SOUNDOUT_CONFIG["TC_API_AUTH_ID"],
                               password: SOUNDOUT_CONFIG["TC_API_PASSWORD"],
                               only: :available

  def index
    @reports = Tunecore::MusicSearch.soundout_report_search(current_user, params).includes(:track, soundout_product: [])
    @total_pages = @reports.total_pages
    @total_count = @reports.total_entries

    @page_title = custom_t("controllers.soundout_reports.fan_reviews_dashboard_view_my_reports")
  end

  def show
    @report_hash = @report.report_data_hash

    if @report_hash
      @reviews     = @report_hash["reviews"]
      @report_data = @report_hash["data"]
    end

    respond_to do |format|
      format.pdf {
        render  pdf: "soundout_report",
                layout: "pdf.html",
                show_as_html: params[:debug].present?
      }
    end
  end

  def purchase_reports
    purchases = JSON.parse(params[:purchase])
    assets = JSON.parse(params[:assets])
    redirect_back fallback_location: dashboard_path and return if purchases.blank?

    purchase_error = SoundoutProduct.purchase(current_user, purchases, assets)
    case purchase_error[:error_type]
    when :invalid_report_purchases
      error_msg = purchase_error[:soundout_reports].map { |soundout_report|
        soundout_report.errors.full_messages
      }.join(". ")
      flash[:error] = error_msg
      redirect_to :purchase_soundout_reports
    when :invalid_purchases
      flash[:error] = custom_t("controllers.soundout_reports.system_error")
      redirect_to :purchase_soundout_reports
    else
      redirect_to cart_path
    end
  end

  def available
    soundout_report = SoundoutReport.joins(:soundout_product).where(
      "soundout_id = ?",
      params[:id]
    ).readonly(false).first

    if soundout_report.nil?
      Airbrake.notify(
        Soundout::SoundoutAirbrakeException.new("soundout_reports_controller#available"),
        {
          params: params
        }
      )
      render json: { message: "Unknown ReportId." }, status: :unprocessable_entity
    else
      # Call the fetch report processing for this soundout product and report
      soundout_report.mark_as_available
      Soundout::FetchWorker.perform_async(soundout_report.id)
      render json: { message: "OK" }, status: :ok
    end
  end

  def purchase
    @albums = Tunecore::MusicSearch.album_search(current_user, page: 1, per_page: 500).includes(songs: [:creatives])
    @total_pages = @albums.total_pages
    @total_albums = @albums.total_entries
    @total_songs = Tunecore::MusicSearch.song_search(current_user).count
    @soundout_songs = current_user.song_library_uploads.order("created_at desc").paginate(page: 1, per_page: 100)

    @page_title = custom_t("controllers.soundout_reports.fan_reviews_dashboard_purchase_reports")
  end

  def market_potential
    @page_title = custom_t("controllers.soundout_reports.fan_reviews_report_market_potential")
  end

  def sample_group
    @page_title = custom_t("controllers.soundout_reports.fan_reviews_report_sample_group")
  end

  def overall_ratings
    @page_title = custom_t("controllers.soundout_reports.fan_reviews_report_overall_ratings")
  end

  def review_analysis
    @page_title = custom_t("controllers.soundout_reports.fan_reviews_report_review_analysis")
  end

  def reviews
    params[:order] ||= "Rating DESC"
    @reviews, @total_count = @report.search_and_filter_reviews(params)
    @total_pages = @total_count / 30 + ((@total_count % 30).positive? ? 1 : 0)

    @page_title = custom_t("controllers.soundout_reports.fan_reviews_report_reviews")
  end

  def ingrooves_fontana
    @page_title = custom_t("controllers.soundout_reports.ingrooves_submission")

    redirect_to action: :index if @report.ingrooves_fontana_submitted_at.present?
  end

  def thanks_ingroove_fontana
    @page_title = custom_t("controllers.soundout_reports.thanks_ingrooves_submission")
  end

  def submit_ingrooves_fontana
    @report.update_attribute(:ingrooves_fontana_submitted_at, Time.now)
    PersonNotifier.submit_ingrooves_fontana(params[:fontana], @report).deliver
    AdminNotifier.ingrooves_fontana_requested(params[:fontana], @report).deliver
    redirect_to action: "thanks_ingroove_fontana"
  end

  private

  def load_report
    # Allow admins to see any soundout report
    begin
      @report =
        if current_user.is_administrator?
          SoundoutReport.find(params[:id])
        else
          current_user.soundout_reports.where("canceled_at IS NULL and status = 'received'").find(params[:id])
        end

      @report_data = JSON.parse(@report.report_data)["data"] if @report.report_data
    rescue ActiveRecord::RecordNotFound
      redirect_to soundout_reports_path
    end
  end
end
