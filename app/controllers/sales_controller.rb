class SalesController < ApplicationController
  include Tunecore::CsvExporter
  include Utilities::FormDateRange

  before_action :load_person
  before_action :disable_sales_reports
  before_action :check_if_user_has_sales, only:   [:release_report, :store_report, :country_report, :song_report, :date_report, :individual_release]
  before_action :load_form_variables,     except: [:stores_status]
  before_action :page_title
  before_action :ensure_correct_country_domain
  after_action :store_filter_parameters, only: [:release_report, :store_report, :country_report, :song_report, :date_report]

  layout "application_old"

  def release_report
    @report = Tunecore::Reports::SalesReport.new(current_user, :by_release)
    @release_sales_records, @report_totals = @report.report_and_summary(@filtered_hash)
  end

  def date_report
    @report = Tunecore::Reports::SalesReport.new(current_user, :by_date)
    @monthly_sales_records, @report_totals = @report.report_and_summary(@filtered_hash)
  end

  def store_report
    @report = Tunecore::Reports::SalesReport.new(current_user, :by_store)
    @store_sales_records, @report_totals = @report.report_and_summary(@filtered_hash)
  end

  def country_report
    @report = Tunecore::Reports::SalesReport.new(current_user, :by_country)
    @country_sales_records, @report_totals = @report.report_and_summary(@filtered_hash)
  end

  def song_report
    @report = Tunecore::Reports::SalesReport.new(current_user, :by_song)
    @song_sales_records, @report_totals = @report.report_and_summary(@filtered_hash)
  end

  def individual_release
    release = "ReadOnlyReplica::#{params[:related_type]}".constantize.find(params[:related_id])

    @release_details = ReadOnlyReplica::SalesRecord.get_album_release_details(current_user, release, params)
    @filtered_hash[:related_type] = release.base_class_name
    @report = Tunecore::Reports::ReleaseSalesReport.new(current_user, :for_release, release: release)
    data, @report_totals = @report.report_and_summary(@filtered_hash)
    @release_sales_records = data[:album_sales]
    if params[:related_type] != "Video"
      @album = release
      @grouped_songs_sales_records = data[:song_sales]
    end

    # Check if there are any parameters set because on the initial page load because we want all checkboxes to be selected by default.
    # We check if size is equal to 4 because there 2 for the action and controller and 2 that are the related_id and related_type.
    @initial_page_load = (params.keys.size == 5) ? true : false
  end

  def individual_song_release
    @report = Tunecore::Reports::ReleaseSalesReport.new(current_user, :for_song, song_id: params[:song_id], release_id: params[:release_id])
    @song_sales_records = @report.report(@filtered_hash)

    respond_to do |format|
      format.html
      format.js { render layout: false }
    end
  end

  def export
    @report = Tunecore::Reports::SalesReport.new(current_user, params[:report_type].to_sym)
    response = @report.to_csv(@filtered_hash)

    filename = "#{params[:report_type]}-#{Date.today}.csv"

    set_csv_headers(filename)
    render plain: response
  end

  def stores_status
    @months = [Date.today]
    9.times do
      @months << @months.last - 1.month
    end

    pp(@months)
    @sip_stores = SipStore.where("display_on_status_page=1").group("display_group_id").order("name") # optional: sort_order
    @page_title = custom_t("controllers.sales.store_reporting_status")
    @special_warnings = []

    # check for blank columns

    @sip_stores.each do |sip_store|
      @months.first(3).each do |month|
        @special_warnings << month
        @special_warnings.pop if sip_store.sales_for_month_in_group?(month, sip_store.display_group_id)
      end
    end
    @special_warnings.uniq!
  end

  private

  def check_if_user_has_sales
    return if ReadOnlyReplica::SalesRecordSummary.exists?(person_id: current_user.id)

    render action: "no_sales", locals: { never_distributed: current_user.has_never_distributed? }
  end

  def build_filter_hash
    filter = {}
    filter[:start_date]         = @start_date
    filter[:end_date]           = @end_date
    filter[:releases]           = params[:release]
    filter[:stores]             = params[:stores]             if params[:stores]
    filter[:countries]          = params[:countries]          if params[:countries]
    filter[:release_type]       = params[:release_type]       if params[:release_type]
    filter[:distribution_type]  = params[:distribution_type]  if params[:distribution_type]
    filter[:related_id]         = params[:related_id]
    filter[:related_type]       = params[:related_type]
    filter[:song_id]            = params[:song_id]
    filter
  end

  def load_form_variables
    # persist filters across different reports or if we are exporting the current filtered report

    if session[:filter_params].present? && (params.keys.size == 2 || params[:action] == "export")
      params.merge!(session_params).permit!
    end

    if params[:date] && !params[:date].value?("")
      @start_date = Date.parse(params[:date]["start_date"])
      @end_date   = Date.parse(params[:date]["end_date"])
    end

    sr_info = ReadOnlyReplica::SalesRecordMaster.earliest_and_latest_sales_record_master(current_user).first

    earliest_sales_record = sr_info.earliest  || Date.today - 1.month
    latest_sales_record   = sr_info.latest    || Date.today

    @start_date = [Date.today - 6.months, earliest_sales_record].max  if @start_date.blank?
    @end_date   = [Date.today, latest_sales_record].min               if @end_date.blank?

    @filtered_hash = build_filter_hash

    return if params[:action] == "export"

    @reporting_dates = Utilities::FormDateRange.create_range(earliest_sales_record, latest_sales_record)
    @user_stores    = ReadOnlyReplica::SipStore.find_sip_stores_for_person_grouped_by_display(current_user.id)
    @user_countries = ReadOnlyReplica::Country.all

    @albums    = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Album' and payment_applied = 1",    @person.id).order("name ASC")
    @singles   = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Single' and payment_applied = 1",   @person.id).order("name ASC")
    @ringtones = ReadOnlyReplica::Album.select("id, name").where("person_id = ? and album_type = 'Ringtone' and payment_applied = 1", @person.id).order("name ASC")
    @videos    = ReadOnlyReplica::Video.select("id, name").where("person_id = ? and is_deleted = 0",                                  @person.id).order("name ASC")

    # Check if there are any parameters set because on the initial page load because we want all checkboxes to be selected by default.
    # We check if size is greater than 2 because there are 2 default params (action and controller).
    @initial_page_load = (params.keys.size == 2 || params[:external_page]) ? true : false
  end

  def session_params
    session[:filter_params].permit(
      date: {},
      release_type: [],
      stores: [],
      countries: [],
      distribution_type: [],
      albums: [],
      singles: [],
      release: {}
    )
  end

  def page_title
    @page_title = custom_t("controllers.sales.sales_reports")
  end

  def store_filter_parameters
    params.delete_if { |k, _v| ["action", "controller", "commit"].include?(k) }
    session[:filter_params] = params
  end

  def disable_sales_reports
    render "disable_sales" if FeatureFlipper.show_feature?(:disable_sales, current_user)
  end
end
