# frozen_string_literal: true

class PlansController < ApplicationController
  layout "plans"

  def index
    redirect_to dashboard_path and return if person_flags[:blocked_from_plans]

    @plans = Plans::PlanEligibilityService.plans_by_upgrade_status(current_user)
    @viewable_plan_cards = @plans.reject { |plan_obj| PersonPlan.downgrade?(current_user, plan_obj[:plan]) }

    redirect_to dashboard_path unless FeatureFlipper.show_feature?(:plans_pricing, current_user)
  end

  def dont_want_a_plan
    if FeatureFlipper.show_feature?(:plans_pricing, current_user)
      session[:dont_want_a_plan] = { value: true }
      redirect_to session[:previous_path]
    else
      redirect_to dashboard_path
    end
  end

  def verify_cart_already_has_a_plan
    popup_template = render_to_string(
      "plans/plan_replacement_modal",
      layout: false,
      locals: { clicked_plan_id: params[:clicked_plan_id] }
    ) if pricier_plan_in_cart?

    render json: { pricier_plan_in_cart: pricier_plan_in_cart?, popup_template: popup_template }
  end

  def plan_renewal_due
    popup_template = render_to_string(
      "plans/plan_renewal_due_modal",
      layout: false
    ) if current_user.person_plan&.expired?

    render json: { popup_template: popup_template, plan_renewal_due: current_user.person_plan&.expired? }
  end

  def replace_plan_in_cart
    case PlanService.adjust_cart_plan!(current_user, params[:plan_id])
    when :success
      redirect_to cart_path
    else
      flash[:error] = custom_t(:something_went_wrong)
      redirect_to dashboard_path
    end
  end

  private

  def pricier_plan_in_cart?
    @pricier_plan_in_cart ||=
      if current_user.plan_in_cart?
        cart_price = Integer(Float(current_user.cart_plan_purchase.cost_cents) / 100)
        Integer(params[:clicked_plan_price]) < cart_price
      else
        false
      end
  end
end
