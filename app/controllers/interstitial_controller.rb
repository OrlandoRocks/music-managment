# encoding: utf-8

class InterstitialController < ApplicationController
  before_action :load_person
  before_action :redirect_to_dashboard, except: [:index, :account_profile_survey, :mobile]
  layout "interstitial"

  def index
    @cms_set = CmsSet.active_set("interstitial", country_website, country_website_language).as_json(root: false)
    @page_title = @cms_set["callout_name"]

    redirect_to dashboard_path if @cms_set["callout_name"] == "None"
  end

  def account_profile_survey
    @page_title = custom_t("controllers.interstitial.more_about_you")
    @survey_info = PersonProfileSurveyInfo.get_survey_info(current_user)
    @survey_info.set_viewed_status(true)
    @redirect_url = params[:first_login].present? ? "/dashboard/create_account" : "/dashboard"
  end

  def mobile
    @album_credit_id = Product.find_products_for_country(
      current_user.country_domain,
      :one_album_credit
    )

    @single_credit_id = Product.find_products_for_country(
      current_user.country_domain,
      :one_single_credit
    )
  end

  def store_manager
  end

  def juke_store
  end

  def jb_hifi_store
  end

  def slacker_store
  end

  def bloom_store
  end

  def beats_store
  end

  def guvera_store
  end

  def akazoo_store
  end

  def march_madness
  end

  def anghami_store
  end

  def kkbox_store
  end

  def preorder
  end

  def neurotic_media_store
  end

  def july_store_sale
  end

  def track_smarts
  end

  def credit_sale
  end

  def target_music
  end

  def ringtone_sale
  end

  def yandex_store
  end

  def youtube_money
  end

  def claro_store
  end

  private

  def redirect_to_dashboard
    redirect_to dashboard_path
  end
end
