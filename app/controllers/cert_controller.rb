class CertController < ApplicationController
  layout "application_old"
  before_action :load_person
  before_action :load_purchase

  def show
    redirect_to new_cert_url
  end

  def new
    @page_title = custom_t("controllers.cert.enter_your_code")
    @cert = Cert.new
  end

  def create
    @page_title = custom_t("controllers.cert.enter_your_code")

    if cert_verifies?
      # cert_verifies? updates purchase with new
      # amount ... discounted by cert code
      cert_applied
    else
      @cert = Cert.new
      cert_not_applied
    end
  end

  protected

  #
  # Method to load the given purchase
  #
  def load_purchase
    @purchase =
      if current_user.is_administrator
        Purchase.find(params[:purchase_id])
      else
        @person.purchases.find(params[:purchase_id])
      end

    invalid_purchase unless @purchase
    invalid_purchase unless @purchase.allow_new_cert?
  end

  #
  #  Response if a cert code is not allowed to be used
  #  on a certain purchase
  #
  def invalid_purchase
    respond_to do |format|
      format.html {
        flash.now[:error] = custom_t("controllers.cert.cant_enter_code")
        redirect_to cart_path
      }
    end
  end

  #
  #  Predicate method to determine if given
  #  cert code is valid.
  #
  def cert_verifies?
    # Cert.verify updates the purchase
    # with discount if verification is successful
    opts = {
      entered_code: params[:cert][:cert],
      purchase: @purchase
    }

    @verify_result = Cert.verify(opts)

    case @verify_result
    when Cert
      # The verification succeeded
      @cert = @verify_result
      true
    when String
      # an error message was returned
      false
    else
      raise "Unexpected result from cert verification."
    end
  end

  #
  #  Response if verification is successful
  #
  def cert_applied
    respond_to do |format|
      # HTML

      format.html {
        flash[:notice] = custom_t("controllers.cert.certificate_applied")
        # raise @purchase.inspect
        # redirect_to thanks_invoice_path(@purchase.invoice)
        logger.info("redirect to cart")
        redirect_to cart_url
      }
    end
  end

  #
  #  Response if verification failed
  #
  def cert_not_applied
    respond_to do |format|
      format.html {
        flash[:error] = @verify_result
        redirect_to cart_url
      }
    end
  end
end
