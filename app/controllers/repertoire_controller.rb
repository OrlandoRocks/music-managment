class RepertoireController < ApplicationController
  before_action :load_person

  layout "application"

  def index
  end

  def search
  end
end
