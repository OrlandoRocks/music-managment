class DashboardController < ApplicationController
  include PlansRedirector
  include YoutubeHelper
  include YoutubeOAC

  before_action :ensure_correct_country_domain, except: :close_splits_pending_banner
  before_action :load_person
  before_action :check_and_load_cert_redemption_values
  before_action :load_page_title, only: [:index, :create_account]

  after_action :clean_up_redemption_session

  skip_before_action :redirect_stem_users, only: :index

  layout "application_old"

  def index
    reskin and return if reskin?

    load_legacy_dash_data
    check_credit_card_expiration_date
    renewal_expires_cache
    set_promotional_ticker

    @automator_count = Tunecore::StoreManager.qualified_albums_for_automator(@person).count
    @cms_set = CmsSet.active_set("dashboard", country_website, country_website_language).as_json(root: false)
    @marketing_metrics = PersonMarketingMetrics.new(@person, @albums)

    dashboard_state = @person.dashboard_state(all_albums)
    @main_body =
      case dashboard_state
      when "virgin" then "state_1_virgin"
      when "unfinalized" then "state_2_unfinalized"
      when "should_finalize" then "state_3_should_finalize"
      # when "published" then "state_4_published" # this state is unused
      when "selling", "no_longer_live" then "state_5_selling"
      else
        logger.warn "Missing check in case statement.  value for @person.dashboard_state is #{@person.dashboard_state}"
      end

    return unless dashboard_state == "virgin"

    render layout: "application"
  end

  def create_an_album
    @band_photo = @person.band_photos.first if @person

    render layout: "application"
  end

  def create_account
    dashboard_or_plans_redirect and return if account_created?

    set_account_creation_cookie

    dashboard_or_plans_redirect and return unless account_just_verified?

    update_recent_login

    prepare_create_account_view
  end

  def close_splits_pending_banner
    session[:splits_pending_banner_closed] = true
    render json: { success: true }
  end

  private

  def load_legacy_dash_data
    load_albums
    load_singles
    load_ringtones
  end

  def recent_releases_by_type(type)
    current_user
      .albums
      .includes(:artwork)
      .public_send(type)
      .not_taken_down
      .not_deleted
      .by_id_desc
      .limit(5)
  end

  def recent_releases
    [:full_albums, :singles, :ringtones].index_with do |type|
      recent_releases_by_type(type).to_a
    end
  end

  def show_null_dashboard?
    current_user.albums.not_deleted.empty? && current_user.has_plan? == false
  end

  def reskin_ivars
    @artist_accounts       = set_promotional_ticker(reskin: true)
    @automator_count       = Tunecore::StoreManager
                             .qualified_albums_for_automator(current_user)
                             .size
    @days_until_expiration = current_user.check_credit_card_expiration_date
    @marketing_metrics     = PersonMarketingMetrics.new(@person, [])
    @show_null_dashboard   = show_null_dashboard?
    @recent_releases       = recent_releases
    @plan                  = current_user.plan
    @has_unaccepted_split_invites = current_user.has_unaccepted_split_invites?
  end

  def dashboard_modal_options
    {
      containerCss: {
        background: "#282828",
        'border-radius': "1rem",
        padding: "2rem",
      },
      maxWidth: "600",
      minHeight: "33%",
      minWidth: "50%",
      overlayClose: true,
      overlayCss: {
        background: "#282828",
      },
    }
  end

  def reskin_gon_vars
    gon.push(dashboardModalOptions: dashboard_modal_options)
  end

  def reskin
    reskin_gon_vars
    reskin_ivars

    render "index_reskin", layout: "application_reskin"
  end

  def update_recent_login
    @person.update(recent_login: Time.zone.now)
  end

  def prepare_create_account_view
    set_create_account_redirect_url
  end

  def set_create_account_redirect_url
    @redirect_url =
      if Plan.enabled?(current_user, false)
        plans_path
      elsif current_user.india_user?
        payoneer_fees_path
      elsif is_mobile_agent?
        mobile_announcement_path
      else
        dashboard_path(pb: "fl")
      end
  end

  def account_created?
    cookies["account_created_#{@person.id}"].present?
  end

  def set_account_creation_cookie
    cookies["account_created_#{@person.id}"] = Time.now.strftime("%Y-%m-%d %H:%M")
  end

  def account_just_verified?
    if session.delete(:verification)
      session[:verification_complete] = true

      true
    else
      false
    end
  end

  def check_credit_card_expiration_date
    @days_until_expiration = current_user.check_credit_card_expiration_date
  end

  def renewal_expires_cache
    @renewal_expires_cache ||= Renewal.make_expires_at_cache_for_albums(load_albums, load_singles, load_ringtones)
  end

  def load_albums
    @albums ||= preloaded_untaken_down_albums
                .full_albums
                .not_deleted
                .by_finalized_asc
                .limit(3)
                .includes(:creatives, :renewals)
                .all
  end

  def load_singles
    @singles ||= preloaded_untaken_down_albums
                 .singles
                 .not_deleted
                 .by_finalized_asc
                 .limit(3)
                 .includes(:creatives, :renewals)
                 .all
  end

  def load_ringtones
    @ringtones ||= preloaded_untaken_down_albums
                   .ringtones
                   .not_deleted
                   .by_finalized_asc
                   .limit(3)
                   .includes(:creatives, :renewals)
                   .all
  end

  def all_albums
    return @all_albums if @all_albums

    load_albums
    load_singles
    load_ringtones

    @all_albums = Album.where(id: [*@albums.pluck(:id), *@singles.pluck(:id), *@ringtones.pluck(:id)])
  end

  def load_page_title
    @page_title = custom_t("controllers.dashboard.#{action_name}")
  end

  def load_store
    @newest_store ||= Store.order("launched_at DESC").first
  end

  def load_social_product
    @social_product = Product.where(display_name: "tc_social_monthly", country_website_id: current_user.country_website_id).first
  end

  def check_and_load_cert_redemption_values
    return unless flash[:redeem_thank_you]

    @brand_code = flash[:redeem_thank_you].downcase
    @redemption = true
    @account_overview = Tunecore::AccountOverview.new(@person, false)
  end

  def clean_up_redemption_session
    session.delete(:cert_code)
  end
end
