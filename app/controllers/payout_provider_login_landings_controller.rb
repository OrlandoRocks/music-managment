class PayoutProviderLoginLandingsController < ApplicationController
  before_action :user_is_onboarding?

  def show
    current_user.payout_provider.update(provider_status: PayoutProvider::PENDING)
    redirect_to withdraw_path
  end

  private

  def user_is_onboarding?
    redirect_to dashboard_path unless current_user.payout_provider.onboarding?
  end
end
