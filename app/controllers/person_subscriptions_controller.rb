class PersonSubscriptionsController < ApplicationController
  def update
    @subscription_request = ::Subscription::SubscriptionRequest.cancel(current_user, subscription_type)
  end

  private

  def subscription_type
    params.require(:subscription_type)
  end
end
