class BandPhotosController < ApplicationController
  before_action :redirect_to_tc_social

  before_action :load_person
  before_action :load_editable_band_photo, except: [:index, :new, :create]
  before_action :setup_progress_monitor, only: [:new, :edit, :create, :update]

  layout "application_old"

  def index
    respond_to do |format|
      format.html
    end
  end

  def show
  end

  def new
    @band_photo = BandPhoto.new
  end

  def edit
  end

  def update
    if @band_photo.update(band_photo_params)
      flash[:notice] = custom_t("controllers.band_photos.successfully_updated")
      redirect_to band_photos_path
    else
      render action: "edit"
    end
  end

  def create
    @band_photo = BandPhoto.new(band_photo_params.merge!(person_id: @person.id))
    if @band_photo.save
      flash[:notice] = custom_t("controllers.successfully_uploaded")
      redirect_to band_photos_path
    else
      render action: "new"
    end
  end

  def destroy
    @band_photo.destroy

    redirect_to band_photos_path
  end

  protected

  def load_editable_band_photo
    @band_photo = @person.band_photos.find(params[:id])
  end

  #
  # give random number to use as a tracker during upload
  # - using an apache module to query upload progress
  #
  def setup_progress_monitor
    # avoiding collision
    @progress_id = rand(10_000)
  end

  private

  def band_photo_params
    params
      .require(:band_photo)
      .permit(
        :description,
        :width,
        :height,
        :bit_depth,
        :size,
        :local_asset_id,
        :s3_asset_id,
        :type,
        :mime_type,
        :file
      )
  end

  def redirect_to_tc_social
    redirect_to "https://social.tunecore.com/artist-pages"
  end
end
