class PreordersController < ApplicationController
  before_action :load_person
  before_action :load_album

  def set_data
    itunes_preorder_data = params[:itunes]
    all_preorder_data = params[:all]

    if itunes_preorder_data.blank? || all_preorder_data.blank?
      preorder_data_error = true
    else
      itunes_preorder_data[:gratification_tracks] = [] unless itunes_preorder_data[:gratification_tracks]

      preorder_purchase = @album.preorder_purchase || PreorderPurchase.create(album: @album)
      preorder_purchase.update_enabled_states(all_preorder_data[:enabled])

      preorder_purchase.update_preorder_data(itunes_preorder_data, all_preorder_data)
    end

    respond_to do |format|
      format.json { render json: { errored: preorder_data_error || preorder_purchase.errors.messages.present? || preorder_purchase.salepoint_preorder_data.any? { |spd| spd.errors.messages.present? } } }
      format.html { redirect_to controller: "album", action: "show", id: @album.id }
    end
  end

  protected

  def load_album
    # added to allow admin to edit songs
    @album ||=
      if @person.is_administrator
        Album.find(params[:album_id])
      else
        @person.albums.find(params[:album_id])
      end
  end
end
