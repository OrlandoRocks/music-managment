# TODO: this appears to be deprecated, create ticket to investigate and remove from codebase

class PayoutProviderLoginsController < ApplicationController
  include PayoutProvidable

  before_action :restrict_access, unless: :payoneer_payout_enabled?

  def create
    login_form = PayoutProvider::LoginForm.new(payout_provider_params)

    if login_form.save
      redirect_to login_form.link
    else
      flash[:errors] = custom_t(:something_went_wrong)
      redirect_back fallback_location: home_path
    end
  end

  private

  def payout_provider_params
    {
      person: current_user,
      provider_name: params[:payout_provider_name].try(:downcase)
    }
  end
end
