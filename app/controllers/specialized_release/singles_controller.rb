# Supports general and specialized Freemium flow for singles with new React Album App - 4/19/2021
class SpecializedRelease::SinglesController < SinglesController
  include SpecializedRelease
  before_action :handle_discovery_platform_path

  def new
    super
  end

  def edit
    super
  end
end
