class NewReportsController < ApplicationController
  before_action :requires_reporting_role

  layout "admin_new"

  def index
    @page_title         = custom_t("controllers.new_reports.reporting_dashboard")
    @countries          = CountryWebsite.all

    granularity         = params[:granularity].try(:downcase) || "daily"

    end_date            = params[:end_date].nil? ? Date.today : Date.parse(params[:end_date], "%Y-%m-%d")
    start_date          = params[:start_date]

    country_id          = params[:country].to_i.positive? ? params[:country] : 1

    @currency           = CountryWebsite.find(country_id).currency

    @rollup, @products  = send("get_#{granularity}_values", start_date, end_date, country_id)

    @product_families   = [
      "Distribution",
      "Publishing",
      "Renewal",
      "Distribution Add-Ons",
      "Credits",
      "Artist Services (post-distribution)",
      "Artist Services (non-distribution)",
      "Miscellaneous"
    ]
  end

  def refresh
    ReportingDashboardRollup.delay(queue: "reporting-queue").calculate_daily_values(Date.today)
    ReportingDashboardProduct.delay(queue: "reporting-queue").calculate_daily_values(Date.today)
    flash[:notice] = custom_t("controllers.new_reports.refresh_enqueued")
    redirect_to new_reports_path
  end

  private

  def requires_reporting_role
    return if current_user.has_role?("Report Viewer", false)

    flash[:error] = custom_t("controllers.new_reports.no_permissions_for_page")
    redirect_to admin_home_path and return
  end

  def get_daily_values(start_date, end_date, country)
    start_date = start_date.nil? ? end_date - 14.days : Date.parse(start_date, "%Y-%m-%d")
    return ReportingDashboardRollup.get_daily_values(
      start_date,
      end_date,
      country
    ), ReportingDashboardProduct.get_daily_values(
      start_date,
      end_date,
      country
    ).group_by(&:product_family)
  end

  def get_weekly_values(start_date, end_date, country)
    start_date = start_date.nil? ? (end_date - 13.weeks).beginning_of_week(:sunday) : Date.parse(start_date, "%Y-%m-%d")
    end_date = end_date.end_of_week(:sunday)
    return ReportingDashboardRollup.get_weekly_values(
      start_date,
      end_date,
      country
    ), ReportingDashboardProduct.get_weekly_values(
      start_date, end_date, country
    ).group_by(&:product_family)
  end

  def get_monthly_values(start_date, end_date, country)
    start_date = start_date.nil? ? (end_date - 11.months).beginning_of_month : Date.parse(start_date, "%Y-%m-%d")
    end_date = end_date.end_of_month
    return ReportingDashboardRollup.get_monthly_values(
      start_date,
      end_date,
      country
    ), ReportingDashboardProduct.get_monthly_values(
      start_date, end_date, country
    ).group_by(&:product_family)
  end
end
