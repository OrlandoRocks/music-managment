class StoredBankAccountsController < ApplicationController
  include TwoFactorAuthable
  include PayoutProvidable

  before_action :load_person, :page_title
  before_action :load_states, only: [:new, :edit]
  before_action :redirect_if_has_pending_eft_batch_transaction, only: [:new, :edit, :destroy]
  before_action :restrict_based_on_united_states_and_territories, only: [:new, :edit, :destroy]
  before_action :page_title
  before_action :if_not_us
  before_action :two_factor_authenticate!, only: [:new, :edit], if: :two_factor_auth_required?
  before_action :generate_braintree_token, only: [:new, :edit]

  layout "application_old"

  def index
    redirect_to_payout
  end

  def if_not_us
    redirect_to_payout if current_user.country_domain != "US"
  end

  def new
    @withdrawal_path = true if (params[:withdrawal_path] == "1")
    @stored_bank_account = StoredBankAccount.new
  end

  def create
    # For update too, we just create a new bank account as per braintree
    # documentation
    #
    # Ref: https://developers.braintreepayments.com/guides/ach/server-side/ruby
    #
    # Quoting from that page :
    #
    # > NOTE: You can’t change the details of a vaulted US Bank Account payment
    # > method using PaymentMethod.update(). If you need to retry verification
    # > with new bank account information, create a new payment method.
    #
    # NOTE : Also note, that StoredBankAccount model ensures existing / older
    # bank accounts are soft deleted in an after_create callback to ensure this
    # is the one that is active and in use.

    is_success, @vault_transaction = StoredBankAccount.create_with_braintree(
      @person, params.merge(ip_address: request.remote_ip)
    )
    if is_success
      flash[:notice] = custom_t("controllers.stored_bank_accounts.bank_account_added")
      PersonNotifier.change_stored_bank_account(@person).deliver
    else
      flash[:error] = custom_t("controllers.stored_bank_accounts.error_updating_account")
    end
    redirect_to_payout
  end

  def edit
    @stored_bank_account = @person.stored_bank_accounts.current
    @stored_bank_account&.last_four_routing = nil
    @stored_bank_account&.last_four_account = nil
    @edit = true
  end

  def destroy
    @stored_bank_account = @person.stored_bank_accounts.find(params[:id])
    @stored_bank_account.destroy(request.remote_ip)

    if !@stored_bank_account.deleted_at.nil?
      flash[:notice] = custom_t("controllers.stored_bank_accounts.bank_account_deleted")
    else
      flash[:error] = custom_t("controllers.stored_bank_accounts.error_deleting_bank_account")
    end
    redirect_to_payout
  end

  def check_image
    render :check_image, layout: "application_lightbox"
  end

  private

  def load_states
    @states = UsState::STATES_AND_TERRITORIES
  end

  def page_title
    @page_title = "Stored Bank Accounts"
  end

  def redirect_to_payout
    redirect_to edit_person_path(current_user, tab: "payout")
  end

  def redirect_if_has_pending_eft_batch_transaction
    redirect_to_payout if @person.has_pending_eft_batch_transaction?
  end

  def generate_braintree_token
    gon.braintreeToken = Braintree::ClientToken.generate
  end
end
