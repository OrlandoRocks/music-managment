class PreviewTitlesController < ApplicationController
  def show
    render plain: SongTitleizerService.titleize(title, featuring_artists)
  end

  private

  def featuring_artists
    creative_params
      .select { |creative_data| creative_data[:role] == "featuring" && creative_data[:name].present? }
      .map { |creative_data| Artist.new(name: creative_data[:name]) }
  end

  def title
    song_params[:name] || album_params[:name] || Album.find(params[:id]).name
  end

  def song_params
    params.fetch(:song, {})
          .permit(:name, creatives: [:name, :role])
  end

  def album_params
    params.fetch(klass, {})
          .permit(:name, creatives: [:name, :role])
  end

  def klass
    [:song, :single, :ringtone].find { |klass| params[klass].present? }
  end

  def creative_params
    album_params.fetch(:creatives, {})
  end
end
