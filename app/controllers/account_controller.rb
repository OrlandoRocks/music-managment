class AccountController < ApplicationController
  skip_before_action :login_required, only: :login
  skip_before_action :check_for_verification, only: :release_account
  skip_before_action :redirect_stem_users, only: :release_account
  skip_before_action :check_for_terms_and_conditions

  def release_account
    if session[:admin] && session[:person]
      session[:person] = session[:admin]
      session[:admin] = nil
    end
    add_sso_cookie(for_user_id: session[:person], by_user_id: current_user.id)
    remove_takeover_cookie
    if album_id_params.present? && Album.exists?(id: album_id_params["album"])
      redirect_to admin_album_path(album_id_params["album"])
    else
      redirect_to admin_person_path(current_user)
    end
  end

  def login
    redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
  end

  private

  def album_id_params
    params.permit(:album)
  end
end
