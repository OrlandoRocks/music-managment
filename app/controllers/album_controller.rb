# frozen_string_literal: true

class AlbumController < ApplicationController
  include AlbumHelper
  include ArtistUrls
  include CreativeSystem::Controller
  include DistributionProgressBar
  include InternationalGenre
  include PhysicalStoresHelper
  include PreorderHelper
  include SalepointSystem
  include SalepointHelper
  include TimedReleasable
  include WorldwideReleasable
  include StoreSelection
  include SpecializedReleasesHandleable
  include AlbumApp
  include S3MetadataFetchable
  include YtmRedirector

  layout "application"

  before_action :load_person
  before_action :load_editable_album, only: [:edit, :edit_linernotes, :update, :destroy, :distribution_panel, :distribution_songs_status, :album_meta_data]
  before_action :load_album, only: [:show, :itunes_link, :monetize, :confirm_monetize, :update_renewal, :territories]
  before_action :load_yt_store, only: [:monetize]
  before_action :gonify_all_main_artists, only: [:edit, :new]
  before_action :require_ytm, only: [:monetize, :confirm_monetize]
  before_action :load_songs, only: [:show, :confirm_monetize]
  before_action :gon_main_artist_text, only: [:new, :show, :edit]
  before_action :gon_datepicker_setup, only: [:new, :create, :edit]
  before_action :gon_itunes_delays, only: [:new, :create, :edit, :update]
  before_action :translate_dates, only: [:create, :update]
  before_action :get_spotify_tracks, only: [:show]
  before_action :scrub_artist_names, only: [:update, :create]
  before_action :load_progress_bar_translations, only: [:edit, :new, :show]
  before_action :set_golive_date_est, only: [:edit]
  before_action -> { set_original_release_date(params[:album]) }, only: [:update]
  before_action -> { handle_parent_genre_params(params[:album]) }, only: [:update, :create]
  before_action -> { set_distribution_progress(@album) }, only: [:new, :edit, :show]
  before_action -> { init_album_app_vars(@album, Album) }, only: %i[new edit]

  after_action -> { backfill_album_metadata(params[:id]) }, only: [:show], if: proc { !@album.finalized? }

  helper_method :creative_roles_for_select

  skip_before_action :alert_rejections
  skip_before_action :set_gon_two_factor_auth_prompt, only: [:new, :show]

  def index
    # we are going to show something useful here
    redirect_to discography_path
  end

  def show
    load_physical_store_views
    @uploadURL    =   BIGBOX_UPLOAD_URL
    @registerURL  =   BIGBOX_REGISTER_URL
    @assetsURL    =   BIGBOX_ASSETS_URL

    @page_title = @album.name
    @album.calculate_steps_required
    @artwork = @album.artwork if @album
    @unpurchased_stores = @album.unselected_stores
    @song = Song.new
    set_golive_date_est

    @preorder = @album.salepoint_preorder_data.by_store("iTunesWW").first || @album.salepoint_preorder_data.by_store("Google").first
    @enabled = (@preorder and @preorder.preorder_purchase.enabled?)
    @paid = (@preorder and @preorder.preorder_purchase.paid_at?)
    @grat_songs = @preorder ? @preorder.preorder_instant_grat_songs : []

    @sso_cookie_value = generate_sso_cookie_value(@album.person_id)

    @show_add_stores = show_add_stores?(@album)
    @show_buy_plan   = show_buy_plan?(@album)

    gon_custom_t("confirm_deletion", "songs.delete.are_you_sure")
    gon_custom_t("could_not_upload", "songs.upload.could_not_upload")
    gon_custom_t("try_again", "songs.upload.try_again")
    gon_custom_t("add_song", "songs.index.add_song")
    gon_custom_t("remove_facebook_salepoint_confirmation", "album.distribute_modal_popup.remove_facebook_salepoint_confirmation")
    gon_custom_t("remove_facebook_salepoint_confirmation_edit", "album.distribute_modal_popup.remove_facebook_salepoint_confirmation_edit")
    gon_custom_t("remove_qobuz_salepoint_confirmation", "album.distribute_modal_popup.remove_qobuz_salepoint_confirmation")

    if @album.created_with_songwriter?
      @song_data_presenter = SongDataPresenter.new(@album, current_user.id)
      @spatial_audio_enabled = spatial_audio_enabled_and_allowed?
      @dolby_atmos_article_link = knowledgebase_link_for_article_id(KnowledgebaseLink::DOLBY_ATMOS_ARTICLE_ID, country_website)
      @cover_song_metadata_enabled = cover_song_metadata_enabled?
      @cover_song_article_link = knowledgebase_link_for_article_id(KnowledgebaseLink::COVER_SONG_ARTICLE_ID, country_website)
      @plan_upgrade_link = "/plans"
      @songs_app_translations = TranslationFetcherService
                                .translations_for("javascripts/songs_app", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
                                .with_support_links(
                                  format_release_for_stores: :format_release_for_stores,
                                  how_to_convert_your_audio_files: "how-to-convert-your-audio-files",
                                  how_do_i_format_my_album: "how-do-i-format-my-album",
                                  songwriter_song_roles_info: "songwriter-song-roles-info"
                                )
      @popup_modal_translations = prepare_popup_translations(current_user.country_website)
    end

    if enable_bigbox_uploader?
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
    end

    check_for_expired_preorder unless @album.payment_applied
  end

  def new
    @page_title = custom_t("controllers.album.create_a_new_album")
    domain_service = MultipleCurrencyDomainService.new(current_user)
    @album = @person.albums.build(
      previously_released: false,
      sale_date: Time.now,
      full_stream_widget: true,
      full_stream_iphone: true,
      language_code: domain_service.distribution_language_code_select(default_language_code)
    )
    set_golive_date_est
    @state = nil
    load_selected_countries
    load_upc_link

    @popup_modal_translations = prepare_popup_translations(current_user.country_website)

    return unless album_app_enabled?

    render layout: "tc-foundation", template: "album/album_app" and return false
  end

  def create
    is_cyrillic = is_a_cyrillic_language?(params[:album][:language_code_legacy_support])

    if album_params && album_params[:salepoints]
      album_params[:salepoints] = transform_salepoints(album_params[:salepoints])
    end

    params_to_save = album_params.except(:iso_codes).merge(
      {
        created_with: Album.created_withs[:songwriter],
        metadata_language_code_id: metadata_language_code_id(album_params)
      }
    )

    @album = @person.albums.build(params_to_save)
    if @album.errors.empty? && @album.save
      set_release_countries(@album, album_params[:iso_codes])

      render :cyrillic_release and return false if is_cyrillic

      if FeatureFlipper.show_feature?(:salepoints_redesign, current_user)
        add_salepoints
        Album::AutomatorService.update_automator(@album, true)
      end
      Note.create(related: @album, note_created_by: current_user, ip_address: request.remote_ip, subject: "Album created", note: "Album was created.")
      create_artist_ids

      redirect_to album_path(@album)
    else
      set_golive_date_est
      rebuild_territory_picker(album_params[:iso_codes])
      render :new and return false
    end
  end

  def cyrillic_release
    render layout: "application"
  end

  def distribution_panel
    render partial: "checklist_and_distribution_ui", locals: { album: @album }
  end

  def distribution_songs_status
    render json: { status: album_songs_status(@album) }
  end

  def album_meta_data
    @metadata = AlbumMetadataService.new(@album)

    respond_to do |format|
      format.json { render partial: "album_meta_data.json" }
    end
  end

  def edit_linernotes
    # album already fetched
  end

  def edit
    load_upc(@album)
    load_selected_countries(@album)
    territory_picker_state

    return unless album_app_enabled?

    render layout: "tc-foundation", template: "album/album_app" and return false
  end

  def update
    set_release_countries(@album, album_params[:iso_codes])
    params_copy = SongData::ParamsScrubber.scrub(album_params, @album)

    params_copy[:salepoints] = transform_salepoints(params_copy[:salepoints]) if params_copy && params_copy[:salepoints]
    @album.assign_attributes(
      params_copy.merge(
        {
          created_with: @album.created_with,
          metadata_language_code_id: metadata_language_code_id(album_params)
        }
      )
            .except(:iso_codes)
    )

    if @album.errors.empty? && @album.save
      check_for_expired_preorder unless @album.payment_applied
      handle_updated_album
      create_artist_ids
    else
      set_golive_date_est
      load_upc(@album)
      rebuild_territory_picker(album_params[:iso_codes])
      render :edit
    end
  end

  def monetize
    @page_title = custom_t("controllers.album.monetize_tracks_on_yt")

    @songs = YtmTracks.songs_for_album(@album)
  end

  def confirm_monetize
    @page_title = custom_t("controllers.album.confirm_tracks_for_monetization")
    @songs_to_monetize =
      params[:songs].reject do |s|
        s["send_or_block_#{s[:id]}"].blank?
      end

    return unless @songs_to_monetize.empty?

    flash[:error] = custom_t("controllers.album.no_songs_selected")
    redirect_back fallback_location: dashboard_path
  end

  def update_renewal
    # if the change was a success
    if @album.change_renewal_interval(renewal.opposite_renewal_interval)
      # upon success, redirect to cart if the renewal is expired, or the person does not have a renewal payment method and it's due within 28 days)
      if @album.renewal.expired_live_and_not_canceled? || (@album.person.preferred_payment_account.nil? && Time.now > @album.renewal.expires_at - Renewal::DAYS_IN_RENEWAL_PERIOD.days)
        Product.add_to_cart(current_user, @album.renewal, Product.find(@album.renewal.renewal_product_id))
        @redirect = cart_path
      end
      flash[:notice] = custom_t("controllers.album.renewal_period", renewal_interval: @album.renewal.renewal_interval)
    else
      flash[:notice] = custom_t("controllers.album.renewal_not_edited")
    end
    redirect_back fallback_location: dashboard_path
  end

  def destroy
    if request.delete?
      begin
        @album.mark_as_deleted
        flash[:notice] = custom_t("controllers.album.album_deleted", type: @album.class.name.downcase, name: @album.name)
      rescue StandardError => e
        flash[:error] = e.message
      end
    end

    redirect_to controller: "discography", action: :index
  end

  def stop_cancellation
    album = Album.find(params[:id])
    begin
      Renewal.transaction do
        Renewal.renewal_for(album).keep!
        @note = Note.create(related: album, note_created_by: current_user, ip_address: request.remote_ip, subject: "Album: Stop Cancellation", note: "Album will now renew")
      end
    rescue StandardError => e
      logger.error("AlbumController.stop_cancellation: #{e}")
    end
    redirect_to action: "show"
  end

  def get_preorder_data
    preorder_prices = preorder_price_options
    @album = Album.includes(:creatives, songs: :creatives).find(params[:id])
    preorder_purchase = @album.preorder_purchase
    preorder = @album.salepoint_preorder_data.by_store("iTunesWW").first || @album.salepoint_preorder_data.by_store("Google").first
    enabled = (preorder and preorder.preorder_purchase.enabled?)
    paid = (preorder and preorder.preorder_purchase.paid_at?)
    songs = @album.songs
    grat_songs = preorder ? preorder.preorder_instant_grat_songs : []
    currency = @album.person.user_currency
    currency_symbol = MONEY_CONFIG[currency][:unit]

    song_data =
      songs.map do |s|
        { id: s.id, name: s.name, artist: s.artist_name }
      end
    data = {
      max_date: l((@album.sale_date - 1.day), format: :country_date),
      min_date: l((Date.today + 10.days), format: :country_date),
      songs: song_data,
      grat_songs: grat_songs.map(&:song_id),
      itunes_enabled: preorder_purchase && preorder_purchase.itunes_enabled,
      google_enabled: preorder_purchase && preorder_purchase.google_enabled,
      paid: paid,
      preorder_price: (preorder ? preorder.preorder_price : ""),
      track_price: (preorder ? preorder.track_price : ""),
      track_preview: (preorder ? preorder.preview_songs : false),
      date_format: t(:datepicker_date_format),
      preorder_prices: preorder_prices,
      currency: currency,
      currency_symbol: currency_symbol,
      show_iso_code: show_currency_iso_code?(currency)
    }

    render json: data
  end

  protected

  def set_golive_date_est
    @album.golive_date = convert_golive_to_est(@album.golive_date)
  end

  def handle_updated_album
    Note.create(related: @album, note_created_by: current_user, ip_address: request.remote_ip, subject: "Album updated", note: "Album was updated.")
    respond_to do |format|
      if request.xhr?
        format.html {
          render partial: "songs/song_bigbox", collection: @album.songs, locals: { album: @album }, status: :ok
        }
      else
        format.html {
          flash[:success] = "Your Album Was Saved" unless flash[:album_notice]
          redirect_to album_path(@album)
        }
      end
    end
  end

  def load_editable_album
    @album = @person.albums.find(params[:id])
    return true unless @album.finalized?

    flash.now[:error] = custom_t("controllers.album.cannot_edit_release")
  end

  def load_album
    @album =
      if current_user.is_administrator?
        Album.find(params[:id])
      else
        @person.albums.find(params[:id])
      end
    session[:current_album_id] = @album.id
    redirect_to_polymorphic_type(@album)
  rescue => e
    flash[:error] = custom_t("controllers.album.unable_to_load_page")
    redirect_to dashboard_path
  end

  def load_yt_store
    @yt_store = Store.find_by(short_name: "YoutubeSR")
  end

  def load_songs
    @songs = @album.songs.preload(:ytm_ineligible_song, salepoint_songs: :salepoint)
  end

  def check_for_expired_preorder
    return unless @album.preorder_date_expired?

    if @album.sale_date_supports_preorder?
      @album.salepoint_preorder_data.each { |spd| spd.update start_date: (@album.sale_date - 1.day) }
      flash[:album_notice] = custom_t("controllers.album.updated_preorder_date")
    else
      @album.salepoint_preorder_data.each { |spd| spd.update start_date: nil }
      @album.preorder_purchase.update(google_enabled: false, itunes_enabled: false)
      flash[:album_notice] = custom_t("controllers.album.invalid_preorder")
    end
  end

  def get_spotify_tracks
    @album_spotify = @album.external_id_for("spotify")
    gon.album_spotify = @album_spotify
    return unless @album_spotify

    @album_spotify_tracks = @album.get_tracks_for("spotify")
    gon.push(magnific: false)
    gon.watch.album_spotify_tracks = @album_spotify_tracks
  end

  def initial_album_params
    permitted_params = [
      :created_on,
      :is_various,
      :iso_codes,
      :label_name,
      :language_code_legacy_support,
      :name,
      :optional_upc_number,
      :orig_release_year,
      :previously_released,
      :primary_genre_id,
      :recording_location,
      :sale_date,
      :secondary_genre_id,
      :timed_release_timing_scenario,
      { creatives: [:id, :role, :name] },
      { golive_date: [:year, :month, :day, :hour, :min, :meridian] },
      { salepoints: [] }
    ]

    permitted_params << :is_dj_release if FeatureFlipper.show_feature?(:dj_mix_type, current_user)

    params
      .require(:album)
      .permit(permitted_params)
  end

  def album_params
    if initial_album_params[:golive_date].blank?
      initial_album_params
    else
      new_golive_date =
        convert_date_hash_to_golive_date(initial_album_params[:golive_date])
      new_sale_date =
        convert_date_hash_to_sale_date(initial_album_params[:golive_date])
      release_datetime_utc =
        new_sale_date
          &.in_time_zone("UTC")
          &.strftime("%Y-%m-%d %H:%M:%S")

      initial_album_params.merge(
        golive_date: new_golive_date,
        sale_date: new_sale_date,
        release_datetime_utc: release_datetime_utc
      )
    end
  end

  private

  def album_app_features
    FeatureFlipper.get_features(FeatureFlipper::ALBUM_APP_FEATURES, @person.id).to_json
  end
end
