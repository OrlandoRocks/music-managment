require "base64"
require "openssl"
require "digest/sha1"
class UploadController < ApplicationController
  skip_before_action :login_required,
                     only: [
                       :create,
                       :upload_status,
                       :success_callback,
                       :failure_callback,
                       :upload_server_mock
                     ]
  before_action :authorize, only: [:index, :address]
  layout "application_old"

  def refresh_upload
    @song = Song.find(params[:id])
    @album = @song.album
    render partial: "songs/song", locals: { song: @song, show_sort_handle: (@song.album.songs.size > 1) }
  end

  # Called by upload server upon a successful upload.
  # Should pass back the values included in :notify_url_ok URL
  # @song.s3_orig_key_prefix is :s3_path for upload
  def success_callback
    @song = Song.find(params[:id])
    # These are passed back to us upon a successful upload.
    # uploaded_filename plus a deduced extension will be used by worker instances.
    # Suggested URL
    # /upload/success_callback/:id?fileext=%{FILEEXT}&original_filename=%{FILENAME%}&bitrate=???
    @upload = Upload.new(
      {
        song_id: params[:id],
        filetype: params[:fileext],
        uploaded_filename: "#{UUIDTools::UUID.random_create}.#{params[:fileext]}",
        original_filename: params[:original_filename],
        bitrate: params[:bitrate]
      }
    )
    # We know these from the Song model and app environment constants.
    @s3_asset = S3Asset.new(
      {
        key: @song.s3_orig_key_prefix + "." + params[:fileext],
        bucket: ORIG_BUCKET_NAME
      }
    )

    if @s3_asset.save && @upload.valid?
      # has_one
      @song.upload = @upload # the orphaned upload is deleted automatically when the song_id is removed

      # belongs_to

      # the s3 asset, if it previously existed, is deleted (the file on s3 has already been replaced by the new file by the upload
      # server and we only need to remove the S3Asset model)
      @song.s3_orig_asset.blank? || @song.s3_orig_asset.destroy
      @song.s3_orig_asset_id = @s3_asset.id
      # Save
      @song.save
      @upload.save

      # Clear any errors.
      @song.update_attribute("last_errors", nil)
      head :ok
      return
    end
    render plain: "", status: :internal_server_error
    # Failure; What does upload server want back?
  end

  def failure_callback
    @song = Song.find(params[:id])
    # Could be nil, but shouldn't be if there's a failure.
    # If it's nil, use a generic error message?

    # if the upload has failed delete a previous upload
    @song.remove_associated_files if @song.upload
    @song.update_attribute("last_errors", params[:error])
    @song.update_attribute("upload_error_log", params[:log])
    head :ok
  end

  def upload_server_mock
    params[:id] = params[:upload_id]
    # The upload server would normally parse :info and pass these
    # values to our success_callback.
    params[:info][:notify_url_ok].gsub(/^[^\?]+\?/, "").split(/&/).each { |arg|
      a = arg.split(/=/)
      params[a.first.to_sym] = a.last
    }
    if params[:fail].blank?
      params[:error] = "Your song file must be a 5,000,000 kbsp file with some hyperdrive..."
      success_callback
    else
      # Already calls render, so we can't call it again here.
      # Should be fine, since it will return 200 OK anyway.
      failure_callback
    end
  end

  # => #<Upload:0xb797b53c @attributes={"filetype"=>"mp3", "converted_filename"=>nil, "is_probably_lossless"=>"0", "id"=>"13873", "bitrate"=>"256", "song_id"=>"25553", "created_at"=>nil, "uploaded_filename"=>"f1c6afe2-5e64-4c01-894e-3fe463e649d5.mp3", "original_filename"=>"03_Travelers.mp3"}>

  #=> #<S3Asset:0xb6fd61a8 @attributes={"updated_at"=>"2007-02-03 22:41:49", "bucket"=>"tunecore.com", "id"=>"36614", "key"=>"origv1/3907/25553.mp3", "created_at"=>"2007-02-03 22:41:49"}>

  def feed
    uploads = Song.where("last_errors is not null").order("id DESC").limit(100).all

    content =
      RSS::Maker.make("2.0") do |m|
        m.channel.title = "Track Upload Events"
        m.channel.description = "Displays the upload errors."
        m.channel.link = request.env["REQUEST_URI"]
        m.items.do_sort = true

        uploads.each do |song|
          i = m.items.new_item

          i.title = "Track Upload Failure: Track ID #{song.id}"
          i.description = <<-DESC
            <div><b>Track Name</b>: #{song.name}</div>
            <div><b>Album</b>: #{song.album.name} by #{song.album.artist_name}</div>
            <div><b>Uploaded File</b>: #{song.upload ? song.upload.original_filename : 'No Upload'}</div>
            <div><b>Upload File Type:</b>: #{song.upload ? song.upload.filetype : 'No Upload'}</div>
            <div><b>Upload File Bitrate:</b>: #{song.upload ? song.upload.bitrate : 'No Upload'}</div>
            <div><b>Display Error</b>:#{song.last_errors || 'None'}</div>
            <div><b>Log Error</b>:#{song.upload_error_log || 'None'}</div>
          DESC

          # <div><b>Uploaded File</b>: #{song.upload ? song.upload.original_filename : 'No Upload'}</div>
          # <div><b>Upload File Type:</b>: #{song.upload ? song.upload.filetype: 'No Upload'}</div>
          # <div><b>Upload File Bitrate:</b>: #{song.upload ? song.upload.bitrate: 'No Upload'}</div>
        end
      end
    headers["Content-Type"] = "text/xml"
    render plain: content.to_s
  end

  def test_redirect
    # raise "stop #{params.size}"
    # ADD current_user.albums.collect{|x| x.songs}.compact.find() --Make sure the song belongs to the current user.
    @song = Song.find(params[:id])

    # if there is no original filename we need to prompt the user to re-upload
    format = params[:original_filename].split(".").last.downcase if params[:original_filename]

    # These are passed back to us upon a successful upload.
    # uploaded_filename plus a deduced extension will be used by worker instances.
    # Suggested URL
    # /upload/success_callback/:id?fileext=%{FILEEXT}&original_filename=%{FILENAME%}&bitrate=???
    if format == "wav"
      @upload = Upload.new(
        {
          song_id: params[:id],
          uploaded_filename: "#{UUIDTools::UUID.random_create}.#{format}",
          original_filename: CGI.unescape(params[:original_filename]),
          filetype: format,
          is_probably_lossless: true,
          bitrate: 1378

        }
      )
      # We know these from the Song model and app environment constants.
      @s3_asset = S3Asset.new(
        {
          key: @song.s3_orig_key_prefix + "." + format,
          bucket: ORIG_BUCKET_NAME
        }
      )

      # raise "stop--in the name of love"
      if @s3_asset.save && @upload.valid?

        # has_one
        @song.upload = @upload # the orphaned upload is deleted automatically when the song_id is removed

        # belongs_to

        # the s3 asset, if it previously existed, is deleted (the file on s3 has already been replaced by the new file by the upload
        # server and we only need to remove the S3Asset model)
        @song.s3_orig_asset.blank? || @song.s3_orig_asset.destroy
        @song.s3_orig_asset_id = @s3_asset.id
        # Save
        @song.save
        @upload.save!

        # Clear any errors.
        @song.update_attribute("last_errors", nil)
        # render nothing: true
        # return
      end

      # render(text: 'success')

    elsif params.keys.size != 7 # if there is an ampersand in the filename we will be getting more than seven params.
      @song.update_attribute("last_errors", "Please remove all spaces and special characters like (*, &, #, $, etc).")
    else
      @song.update_attribute("last_errors", "You must upload a wav file")
      @song.upload.destroy if @song.upload

      # render(text: 'error')
      # raise "you have to give us a wav file or you suck"
    end
    redirect_to(action: "s3_test_form", id: @song.album, song_id: @song)
  end

  protected

  def authorize
    if params[:id] != nil
      @album = Album.find(params[:id])
    elsif current_user.id != nil
      @album = Album.where("album_state in ('paid','exported') and person_id = #{current_user.id}").first
    end
    if @album.person_id != current_user.id
      flash[:error] = custom_t("controllers.upload.unauthorized_edit", authoriz: t(:authoriz))
      redirect_to discography_path
    elsif @album.nil?
      flash[:error] = custom_t("controllers.upload.no_albums_for_upload")
      redirect_to discography_path
    end
  end
end
