module ReferralData::Parsable
  # add this module to any controller that you
  # want to have parsing of cookie and urls
  # for insertion into the referral_data table.
  # you can use build_referral_data_from_*
  # as before_actions.
  #
  # NOTE: ensure that you have @person set before
  # the before_actions are run

  def build_referral_data_from_cookies(page, tcs = false)
    cookies.each do |name, cookie|
      next unless cookie_matches?(name)

      build_referral_data_for_form(
        ReferralDatum::Parser.new(cookie),
        page,
        :cookie,
        tcs ? :tc_social : nil
      )
    end
  end

  def build_referral_data_from_url(page, tcs = false)
    build_referral_data_for_form(
      ReferralDatum::Parser.new(params),
      page,
      :url,
      tcs ? :tc_social : nil
    )
  end

  def create_referral_data(_person, referral_attributes)
    return unless referral_attributes

    referral_attributes.each do |_k, attrs|
      @person.referral_data.create(attrs)
    end
  end

  def create_referral_cookies
    last_cookie_number   = last_cookie ? trailing_digits(last_cookie[0]) : 0
    parser               = ReferralDatum::Parser.new(params)
    return if parser.parsed.empty?

    cookie_data = parser.parsed.merge(tc_timestamp: parser.timestamp.to_i).to_json
    cookies.permanent["tunecore_wp_#{last_cookie_number + 1}"] = cookie_data
  end

  def clear_referral_cookies
    # cookie_domain = get_cookie_domain
    cookies.each do |k, _v|
      cookies.delete(k, domain: COOKIE_DOMAIN) if cookie_matches?(k)
    end
  end

  def get_cookie_domain
    host_array = request.host.split(".")
    host_array.shift
    host_array.join(".")
  end

  def referral_params
    params
      .require(:person)
      .permit(
        referral_data_attributes:
          [:key, :value, :page, :source, :timestamp]
      )
  rescue
    {}
  end

  def referral_attributes
    referral_params[:referral_data_attributes]
  end

  private

  def last_cookie
    parsed_cookies = cookies.select { |k, _v| cookie_matches?(k) }
    parsed_cookies.max_by { |w| trailing_digits(w[0]) }
  end

  def ends_with_digit?(string)
    string =~ /\d+$/
  end

  def trailing_digits(string)
    match = string.match(/\d+$/)
    match[0].to_i if match
  end

  def cookie_matches?(cookie)
    cookie.include?("tunecore_wp")
  end

  def build_referral_data_for_form(parser, page, source, tc_social)
    parser.parsed.each do |k, v|
      referral_datum = @person.referral_data
                              .build(referral_datum_params(k, v, parser, page, source, tc_social))
      referral_datum.save if page == :tcs_signin
      referral_datum
    end
  end

  def referral_datum_params(k, v, parser, page, source, tc_social)
    {
      key: k.to_s[0..100],
      value: v.to_s[0..100],
      page: page,
      source: source,
      page_source: tc_social,
      timestamp: parser.timestamp
    }
  end
end
