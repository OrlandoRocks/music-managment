class PayoutTransfersController < ApplicationController
  include PayoutProvidable
  include PayoneerAchFeeInfo

  before_action :reject_withdrawal_by_admin, if: :current_user_is_taken_over?
  before_action :redirect_to_withdraw!,
                if: proc {
                      current_user.blocked_withdrawal? || !useable_payout_provider_approved?
                    }
  before_action :evaluate_and_tax_block_user, only: :new, if: :evaluate_tax_blocking?
  before_action -> { redirect_to account_settings_path(tab: "taxpayer_id") },
                only: :confirm,
                if: :tax_blocked_user_attempting_withdrawal?

  layout "tc-foundation"

  def show
    transfer = current_user.payout_transfers.find_by_client_reference_id!(params[:id])
    @payout_transfer = PayoutTransferDecorator.new(transfer)
  end

  def new
    @payoneer_response = change_payout_method_link(new_payout_transfer_url(host: current_user.country_website.url))
    @create_form = PayoutTransfer::CreateForm.new(person: current_user)
  end

  def confirm
    @create_form = PayoutTransfer::CreateForm.new(withdrawal_params)
    load_payoneer_ach_fee_info if show_payoneer_redesigned_onboarding?
  end

  def create
    @create_form = PayoutTransfer::CreateForm.new(withdrawal_params)
    if @create_form.save
      redirect_to @create_form.payout_transfer
    else
      render :confirm
    end
  end

  private

  def withdrawal_params
    params
      .require(:payout_transfer_create_form)
      .permit(:amount_cents_raw, :amount)
      .merge(person: current_user)
  end
end
