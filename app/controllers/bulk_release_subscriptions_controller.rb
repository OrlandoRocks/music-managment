class BulkReleaseSubscriptionsController < ApplicationController
  before_action :load_person
  before_action :check_for_encumbrances

  layout "application"

  def index
    @releases = current_user.live_releases
    @releases = @releases.paginate(page: params[:page] || 1, per_page: 30)
  end

  def create
    note_options = {
      user_id: current_user.id,
      ip_address: request.remote_ip,
      subject: "Takedown",
      note: "Album was taken down"
    }
    bulk_takedown = BulkReleaseSubscriptionForm.new(params["bulk_release_subscription"].merge(note_options: note_options)).process
    flash[:error] = custom_t("controllers.bulk_release_subscriptions.release_errors") unless bulk_takedown
    redirect_to bulk_release_subscriptions_path
  end

  private

  def check_for_encumbrances
    return unless current_user.has_encumbrance?

    flash[:notice] = "Due to your active encumbrance you cannot take down your content from stores at this time."
    redirect_to dashboard_path
  end
end
