class ResetPasswordsController < ApplicationController
  include KnowledgebaseLinkHelper
  include RecaptchaVerifiable

  skip_before_action :login_required
  before_action :requires_person_and_key, only: [:show, :update]
  skip_before_action :check_for_verification, if: proc { (params[:action] == "new" && !!params[:force]) || params[:action] == "create" }
  skip_before_action :force_pw_reset

  layout "registration"

  def new
    @page_title = custom_t("controllers.reset_passwords.forgot_password")
    @person     = Person.new

    if !!params[:force]
      flash[:error] = custom_t("controllers.reset_passwords.we_are_updating_security")
    elsif logged_in? && !params[:force_from_tfa_form]
      flash[:notice] = custom_t("controllers.reset_passwords.you_are_logged_in_change_password_now")
      redirect_to edit_person_url(current_user)
      return
    end

    if reskin?
      render "new_reskin", layout: "registration_reskin"
    else
      render "new"
    end
  end

  def create
    @page_title = custom_t("controllers.reset_passwords.forgot_password_activation")
    email = current_user.try(:email) || person_params[:email]

    redirect_failed_password_reset and return if email.blank?

    return if use_recaptcha? && captcha_on_pw_reset(handle_v3_recaptcha)

    result = handle_reset(email)
    redirect_failed_password_reset and return unless result

    handle_reset_request_success(email)
  end

  def show
    @page_title = custom_t("controllers.reset_passwords.forgot_password_activation")

    @person = Person.find(params[:person_id])
    @activation_key = params[:key]

    unless @person.valid_invite?(@activation_key)
      flash[:error] = custom_t("controllers.reset_passwords.forgotten_password_key_not_valid")
      redirect_to(new_reset_password_url)
      return
    end

    @person.unlock_account

    if reskin?
      render "show_reskin", layout: "registration_reskin"
    else
      render "show"
    end
  end

  def update
    @person = Person.find(params[:person_id])
    @activation_key = params[:key]

    password_change_failed and return unless @person.valid_invite?(@activation_key)

    if @person.update(person_params.merge(from_password_reset_form: true, password_reset_tmsp: Time.now))
      password_change_success
    else
      password_change_failed
    end
  end

  private

  def handle_reset_request_success(email)
    flash.now[:notice] = custom_t("controllers.reset_passwords.reset_password_instructions", email: email)

    redirect_to success_redirect_url
  end

  def handle_reset(email)
    person = Person.find_by(email: email)
    return true if person.blank?

    person.unlock_account if person.account_locked?

    begin
      PasswordResetService.reset(person.email, :user)
    rescue StandardError => e
      handle_reset_error(e)

      false
    end
  end

  def redirect_to_edit_account
    flash[:notice] = custom_t("controllers.reset_passwords.reset_your_password_below")
    self.current_user = @person
    redirect_to edit_person_path(@person)
  end

  def password_change_success
    PersonNotifier.change_password(@person, person_params[:password]).deliver
    @person.create_logged_in_event(request.remote_ip, request.user_agent)
    self.current_user = @person

    flash[:notice] = custom_t("controllers.reset_passwords.your_password_has_been_updated")
    @person.clear_invite
    redirect_to dashboard_path
  end

  def password_change_failed
    Rails.logger.error("Password change failed for Person ID #{@person.id}: #{@person.errors.full_messages.join(',')}")

    if reskin?
      render "show_reskin", layout: "registration_reskin"
    else
      render "show"
    end
  end

  def requires_person_and_key
    access_denied if params[:person_id].blank? || params[:key].blank?
  end

  def redirect_failed_password_reset(error = nil)
    handle_reset_error(error)
    set_request_reset_error_flash

    @person = Person.new

    if reskin?
      render "new_reskin", layout: "registration_reskin"
    else
      render :new
    end
  end

  def person_params
    params.require(:person).permit(:email, :force, :password, :password_confirmation, :new_password)
  end

  def success_redirect_url
    return logout_path(force_reset: true) if person_params[:force] == "true"

    login_url(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
  end

  def handle_reset_error(error)
    logger.error("Invalid request: #{error}") if error
  end

  def set_request_reset_error_flash
    flash.now[:error] =
      if person_params[:email].blank?
        custom_t("errors.messages.blank")
      else
        custom_t(
          "controllers.reset_passwords.sorry_we_were_not_able_to_send_password_reset",
          email: person_params[:email],
          link: knowledgebase_link_for("help-homepage", country_website)
        )
      end

    nil
  end
end
