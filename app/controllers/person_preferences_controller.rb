class PersonPreferencesController < ApplicationController
  include OneTimePaymentRedirectable
  before_action :load_person
  before_action :redirect_one_time_payment_users, only: :index, if: :one_time_payment_user?
  skip_before_action :redirect_stem_users, only: :index

  def index
    flash.keep(:notice)
    flash.keep(:error)
    if params[:redirect]
      redirect_to params[:redirect]
    elsif params[:tcs_return_msg]
      redirect_to "/people/#{current_user.id}/edit/?tab=payment&tcs_return_msg=#{params[:tcs_return_msg]}"
    else
      redirect_to "/people/#{current_user.id}/edit/?tab=payment"
    end
  end

  def create
    update_auto_renewal and return unless @person.autorenew?

    # additional check to prevent duplicate person_prefences for people who click submit twice quickly
    @person_preference = @person.person_preference

    if @person_preference.nil?
      @person_preference = @person.create_person_preference(create_params)

      if create_params[:preferred_credit_card_id]
        @person.person_preference.preferred_credit_card =
          @person.stored_credit_cards.find(create_params[:preferred_credit_card_id])
      end

      if create_params[:preferred_paypal_account_id]
        @person.person_preference.preferred_paypal_account =
          @person.stored_paypal_accounts.find(create_params[:preferred_paypal_account_id])
      end

      if @person.person_preference.save
        flash[:notice] = "Preferences saved."
        redirect_to person_preferences_path
      else
        flash[:error] = @person.person_preference.errors.full_messages.join(" ")
        redirect_to "/people/#{current_user.id}/edit/?tab=payment"
      end
    else
      update
    end
  end

  def create_params
    params
      .require(:person_preference)
      .permit(
        :preferred_credit_card_id,
        :pay_with_balance,
        :preferred_paypal_account_id,
        :preferred_payment_type
      )
  end

  def update
    # have to check it manually because it is a checkbox because their is an issue with not passing in the attribute if the checkbox is not selected.
    params[:person_preference][:pay_with_balance] =
      if params[:pay_with_balance]
        false
      else
        true
      end

    @person_preference = @person.person_preference
    if params[:person_preference][:preferred_credit_card_id]
      @person.person_preference.preferred_credit_card    = @person.stored_credit_cards.find(params[:person_preference][:preferred_credit_card_id])
    end
    if params[:person_preference][:preferred_paypal_account_id]
      @person.person_preference.preferred_paypal_account = @person.stored_paypal_accounts.find(params[:person_preference][:preferred_paypal_account_id])
    end
    if params[:person_preference][:preferred_adyen_payment_method_id]
      @person.person_preference.preferred_adyen_payment_method = @person.adyen_stored_payment_methods.find(params[:person_preference][:preferred_adyen_payment_method_id])
    end

    @person_preference.attributes = update_params
    changed = @person_preference.changes

    if @person_preference.save
      changed.each do |field, change|
        note ||= ""
        note += "#{field.humanize} was set to "

        note +=
          case field
          when "pay_with_balance"
            (change[1])&.zero? ? "false. " : "true. "
          when "preferred_credit_card_id"
            "************#{StoredCreditCard.find(change[1]).last_four}. "
          else
            "#{change[1]}. "
          end

        Note.create(
          related: @person,
          note_created_by: current_user,
          ip_address: request.remote_ip,
          subject: "Payment Preferences Changed",
          note: note
        )
      end

      return if request.xhr?&.zero?

      flash[:notice] = custom_t("controllers.person_preferences.saved")
      redirect_to person_preferences_path
    else
      flash[:error] = custom_t("controllers.person_preferences.error_saving")
      redirect_to "/people/#{current_user.id}/edit/?tab=payment"
    end
  end

  def update_params
    params
      .require(:person_preference)
      .permit(
        :preferred_credit_card_id,
        :pay_with_balance,
        :preferred_payment_type,
        :preferred_paypal_account_id,
        :preferred_adyen_payment_method_id
      )
  end

  private

  def load_form_variables
    @stored_paypal_account = StoredPaypalAccount.currently_active_for_person(@person)
    @stored_credit_cards = @person.stored_credit_cards.active
    @stored_and_not_expired_credit_cards = @person.stored_credit_cards.active.not_expired
    unless !@person_preference.nil? &&
           (!@stored_and_not_expired_credit_cards.empty? && @person_preference.preferred_credit_card_id.nil?)

      return
    end

    @person_preference.preferred_credit_card_id = @stored_and_not_expired_credit_cards.first.id
  end

  def update_auto_renewal
    if params[:enable_auto_renewal]
      note = { note_created_by: current_user, note: "Enable auto renewal", ip_address: request.remote_ip }
      @person.enable_auto_renewal(note)
    end

    if @person.errors.empty?
      flash[:notice] = custom_t("controllers.person_preferences.auto_renewals_on")
      redirect_to person_preferences_path
    else
      @person_preference = PersonPreference.new
      @page_title = custom_t("controllers.person_preferences.my_payment_preferences")
      render :edit_auto_renewal
    end
  end
end
