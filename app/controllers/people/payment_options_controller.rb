class People::PaymentOptionsController < ApplicationController
  def index
    redirect_to edit_person_path(current_user, tab: "payment", tcs_return_msg: params["tcs_return_msg"])
  end
end
