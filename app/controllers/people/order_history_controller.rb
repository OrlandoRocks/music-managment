class People::OrderHistoryController < ApplicationController
  def index
    redirect_to edit_person_path(current_user, tab: "history")
  end
end
