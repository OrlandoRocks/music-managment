class PayoutProviderRegistrationLandingsController < ApplicationController
  def show
    payout_provider = current_user.payout_provider
    payout_provider.update(provider_status: PayoutProvider::PENDING) if payout_provider.onboarding?

    redirect_to check_feature_redirect_path
  end

  def check_feature_redirect_path
    Payoneer::FeatureService.payoneer_redesigned_onboarding?(current_user) ? payoneer_status_index_path : withdraw_path
  end
end
