class SpatialAudioUploadsController < UploadsController
  include AlbumHelper

  before_action :verify_feature_enabled
  before_action :load_song, only: [:validate_asset]

  layout "application_old"

  def get_put_presigned_url
    response = S3Asset.get_put_presigned_url(
      BIGBOX_BUCKET_NAME,
      spatial_audio_filename,
      current_user.id
    )

    render json: response.to_json
  end

  def register
    form = SpatialAudioS3AssetsRegisterForm.new(register_params)

    if form.save
      render json: {
        status: 0,
        spatial_audio_asset_url: form.song.spatial_audio_url(36_000)
      }
    else
      render json: { errors: form.errors }, status: :unprocessable_entity
    end
  end

  def validate_asset
    response = ImmersiveAudio::ValidationService.new(
      @song.s3_orig_asset,
      params[:key_name],
      params[:bucket_name],
      SpatialAudio::AUDIO_TYPE
    ).call

    if response[:errors].any?
      ImmersiveAudio::RemovalService.new(key: params[:key_name], bucket: params[:bucket_name]).call
      render json: { errors: response[:errors] }, status: :unprocessable_entity
    else
      render json: {
        key: params[:key_name],
        bucket: params[:bucket_name],
        orig_filename: File.basename(params[:key_name])
      }.to_json
    end
  end

  def checklist_partial
    partial = render_to_string(
      "album/_checklist_and_distribution_ui",
      layout: false,
      locals: { album: current_user.albums.find(params[:album_id]) }
    )
    render json: { partial: partial }
  end

  private

  def verify_feature_enabled
    render status: :unauthorized unless spatial_audio_enabled_and_allowed?
  end

  def spatial_audio_filename
    @spatial_audio_filename ||= FileNameModifier.spatial_audio_filename(filename: params[:filename])
  end
end
