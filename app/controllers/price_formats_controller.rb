class PriceFormatsController < ApplicationController
  skip_before_action :set_locale
  skip_before_action :alert_rejections
  skip_before_action :redirect_sync_users
  skip_before_action :redirect_ytm_approvers

  def format_price
    @div = params[:div]
    @price = params[:price]
    @currency = params[:currency]

    render layout: false
  end
end
