# frozen_string_literal: true

class SurveyResponsesController < ApplicationController
  def create
    survey_response = SurveyResponse.create(
      person_id: survey_response_params[:person_id],
      survey_id: survey_response_params[:survey_id],
      json: survey_response_params[:json].to_json
    )

    render json: { survey_response_id: survey_response.id, success: survey_response.errors.blank? }
  end

  def update
    survey_response = SurveyResponse.find(params[:id])
    success = survey_response.update(json: survey_response_params[:json].to_json)

    render json: { survey_response_id: survey_response.id, success: success }
  end

  private

  def survey_response_params
    params.require(:survey_response).permit(:person_id, :survey_id, json: [:happy, :text_response])
  end
end
