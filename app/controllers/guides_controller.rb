class GuidesController < ApplicationController
  skip_before_action :check_for_verification, only: [:copyright]
  layout "guide"

  def index
    @page_title = "Music Industry Survival Guide: Tips for Musicians"
  end

  def landingpage
  end

  def copyright
    redirect_to action: "index", status: :moved_permanently
    # require 'rss'
    # @rss = RSS::Parser.parse(open('http://feeds.feedburner.com/Tunecorner?format=xml').read, false)
    # render :layout => "application"
  end

  def thirteen_ways_to_make_money
    @page_title = "13 Different Ways To Make Money From Your Songs"
  end

  def sixrights
    @page_title = "Music Industry Survival Guide: How Not To Get Screwed"
  end

  def promote_introduction
    @page_title = "Distribution Of Music - New Music Distribution"
  end

  def basics_to_know
    @page_title = "Music Publishing - How To Guide To Music Publishing"
  end

  def basics_of_mastering
    @page_title = "Mixing And Mastering Music - Music Recording Mastering"
  end

  def basics_of_vinyl
    @page_title = "Record To Vinyl - Recording To Vinyl - Vinyl Record Recording"
  end

  def basics_of_mixing
    @page_title = "Mixing Music - Music Editing Mixing - Mixing Your Music"
  end

  def basics_of_copyright
    @page_title = "Copyrighting Music - Copyright Basics - How To Copyright Music"
  end

  def sellmusiconline
    @page_title = "Music Industry Survival Guide: Make Money - Sell More Music Online"
  end

  def healthcare
    @page_title = "Musician Healthcare - Music Healthcare"
  end

  def promote_blogpromotion
    @page_title = "Promote Music On Blogs - Music Blog Promotion"
  end

  def promote_gettingstarted
    @page_title = "Music Affiliate Program - Get Music Found - Music Store Affiliate"
  end

  def promote_indieradio
    @page_title = "How To Get Music On Radio - How To Get Music Played On The Radio"
  end

  def promote_itunes
    @page_title = "Itunes Promotion - Promote On Itunes - Imix Promotion"
  end

  def promote_marketing
    @page_title = "Music Marketing - Internet Music Marketing"
  end

  def promote_merchandising
    @page_title = "Music Merchandising - Music Business Plan - How To Merchandise Clothing"
  end

  def promote_pressmedia
    @page_title = "Music Media Promoton - Getting Music Reviewed - Getting Music To The Media"
  end

  def promote_resources
    @page_title = "Free Music Resources - Online Music Resource - Music Business Resource"
  end

  def promote_streetmarketing
    @page_title = "Street Marketing - Street Marketing Ideas - Street Team Marketing"
  end

  def promote_marketing
    @page_title = "Music Marketing - Internet Music Marketing"
  end
end
