class YoutubeOacController < ApplicationController
  layout "tc-foundation"

  def new
    @names = Creative.person_unique_artist_names_no_oac(current_user)
  end
end
