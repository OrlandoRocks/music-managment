class FacebookTrackMonetization::DashboardsController < TrackMonetization::DashboardsBaseController
  before_action :facebook_dashboard_accessible?
  before_action :subscribed?, only: :show
  before_action :load_translations, on: :show, if: :is_html_request?

  private

  def load_translations
    @dashboard_translations ||= TranslationFetcherService.translations_for(
      "javascripts/facebook_tracks_dashboard",
      COUNTRY_LOCALE_MAP[country_website]
    )
  end

  def serialized_tracks
    fb    = Store.find_by(short_name: "FBTracks")
    query = TrackMonetization::DashboardQueryBuilder.build(current_user, fb.id)

    ActiveModel::ArraySerializer.new(
      query,
      each_serializer: TrackMonetization::DashboardSerializer,
      context: { current_user: current_user }
    ).to_json
  end

  def subscribed?
    return true if already_subscribed?("FBTracks")

    redirect_to dashboard_path
  end
end
