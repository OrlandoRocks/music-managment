class FacebookTrackMonetization::SubscriptionsController < ApplicationController
  layout "tc-foundation"

  before_action :load_purchase, only: :create
  before_action :redirect_to_fbtracks_dashboard, only: :new, if: :subscribed?

  def create
    if @purchase.finalize
      mailer_method = "mail_for_sign_up_and_select#{'_all' if params[:send_all]}"
      queue_email(mailer_method)

      flash[:alert] = "all_tracks_were_sent" if params[:send_all]
      redirect_to facebook_track_monetization_dashboard_path
    else
      render :new
    end
  end

  private

  def load_purchase
    @purchase = ::Subscription::Tunecore::PurchaseRequest.new(subscription_params)
  end

  def queue_email(mailer_method)
    MailerWorker.perform_async(
      "FacebookTrackMonetizationMailer",
      mailer_method.to_sym,
      current_user.id
    )
  end

  def redirect_to_fbtracks_dashboard
    redirect_to facebook_track_monetization_dashboard_path
  end

  def subscribed?
    already_subscribed?("FBTracks")
  end

  def subscription_params
    {
      person_id: current_user.id,
      product_name: "FbTracks",
      product_type: "annually",
      payment_method: "balance",
      send_all: params[:send_all]
    }
  end
end
