class TwoFactorAuth::PreferencesController < ApplicationController
  include TwoFactorAuthable

  before_action  :load_two_factor_auth
  before_action  :load_preference, only: [:show, :create, :destroy]
  before_action  :two_factor_authenticate!, only: [:show, :create], if: :two_factor_auth_required?
  before_action  :save_previous_state, only: :show
  before_action  :reset_state, only: :destroy
  before_action  :ensure_tfa_settings_access
  before_action  :set_toll_fraud_validation_variables, only: :show

  def show
    set_tfa_gon_variables(
      current_user,
      "authentication",
      two_factor_auth_preferences_path(redirect_params(@preference.next_action)),
      api_backstage_two_factor_auth_app_callbacks_path(format: :json)
    ) if @preference.on_authentication_action?

    redirect_to edit_person_path(id: current_user.id) if @preference.is_complete?
  end

  def create
    unless @preference.valid?
      check_for_errors
      redirect_to two_factor_auth_preferences_path(redirect_params(@preference.tfa_action))
      return
    end

    @preference.perform
    check_for_errors

    redirect_to two_factor_auth_preferences_path(redirect_params(@preference.next_action))
  end

  def destroy
    @preference.terminate
    redirect_to edit_person_path(id: current_user.id)
  end

  def save_previous_state
    @preference.save_previous_state
  end

  def reset_state
    @preference.restore_previous_state
  end

  private

  def load_preference
    @preference ||= TwoFactorAuth::PreferenceForm.new(preference_params)
  end

  def load_two_factor_auth
    @two_factor_auth = current_user.two_factor_auth
  end

  def check_for_errors
    flash[:errors] = @preference.errors.full_messages if @preference.errors.any?
  end

  def preference_params
    @preference_params ||= (params[:two_factor_auth_preference_form] || {}).merge(person_params)
  end

  def person_params
    { person: current_user }
  end

  def redirect_params(tfa_action)
    { two_factor_auth_preference_form: { tfa_action: tfa_action } }
  end

  def ensure_tfa_settings_access
    redirect_to dashboard_path unless has_tfa_access?
  end

  def has_tfa_access?
    return false unless current_user.two_factor_auth

    is_not_admin_session? && two_factor_feature_enabled?
  end

  def is_not_admin_session?
    session[:admin].nil?
  end
end
