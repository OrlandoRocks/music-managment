class TwoFactorAuth::AuthenticationsController < ApplicationController
  include TwoFactorAuthable
  before_action :load_authentication, only: [:show, :create]

  def show
    set_tfa_gon_variables(
      current_user,
      "authentication",
      two_factor_auth_authentications_path(redirect_params(@authentication.next_action)),
      api_backstage_two_factor_auth_app_callbacks_path(format: :json)
    ) if @authentication.on_authentication_action?

    redirect_to page_the_user_wanted_to_access(@authentication.page, current_user) if @authentication.is_complete?
  end

  def create
    unless @authentication.valid?
      check_for_errors
      redirect_to two_factor_auth_authentications_path(redirect_params("sign_in"))
      return
    end
    @authentication.perform
    check_for_errors

    redirect_to two_factor_auth_authentications_path(redirect_params(@authentication.next_action))
  end

  def load_authentication
    @authentication ||= TwoFactorAuth::AuthenticationForm.new(auth_params)
  end

  def check_for_errors
    flash[:errors] = @authentication.errors.full_messages if @authentication.errors.any?
  end

  def auth_params
    @auth_params ||= params[:two_factor_auth_authentication_form].merge(person: current_user)
  end

  def redirect_params(tfa_action)
    {
      two_factor_auth_authentication_form: {
        page: @authentication.page,
        tfa_action: tfa_action,
        success_callbacks: @authentication.success_callbacks
      }
    }
  end
end
