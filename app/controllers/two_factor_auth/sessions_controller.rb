class TwoFactorAuth::SessionsController < ApplicationController
  include Api::Frontstage::Authenticatable
  include SignInCallback
  include TwoFactorAuthable

  layout "registration"

  skip_before_action :login_required
  before_action :check_person

  def new
    return sign_in_success if session["person"].present?

    @auth_form = TwoFactorAuth::AuthenticationForm.new(auth_params.merge(person: @person))
    check_auth_form

    set_tfa_gon_variables(
      @person,
      "session",
      new_two_factor_auth_sessions_path(redirect_params),
      api_frontstage_two_factor_auth_app_callbacks_path(callback_params)
    ) if @auth_form.on_authentication_action?

    if @auth_form.is_complete?
      sign_in_success
      return
    end

    if FeatureFlipper.show_feature?(:reskin)
      render "new_reskin", layout: "registration_reskin"
    else
      render "new"
    end
  end

  def create
    @auth_form = TwoFactorAuth::AuthenticationForm.new(auth_params.merge(person: @person))

    if !@auth_form.is_complete? && !@auth_form.valid?
      redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      return
    end

    @auth_form.perform
    check_for_errors
    redirect_to new_two_factor_auth_sessions_path(redirect_params)
  end

  private

  def check_person
    redirect_to login_path(
      ENV.fetch(
        "LOGIN_DDOS_MITIGATION_PARAM",
        "check"
      ).to_sym => 1
    ) if @person.two_factor_auth.blank?
  end

  def check_auth_form
    return true if @auth_form.is_complete?

    redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1) unless @auth_form.valid?
  end

  def check_for_errors
    flash[:errors] = @auth_form.errors.full_messages if @auth_form.errors.any?
  end

  def redirect_params
    {
      token: @token,
      two_factor_auth_authentication_form: { page: @auth_form.page, tfa_action: @auth_form.next_action }
    }
  end

  def callback_params
    { token: @token, format: :json }
  end

  def auth_params
    params
      .require(
        :two_factor_auth_authentication_form
      )
      .permit(
        :tfa_action,
        :page,
        :auth_code
      )
  end
end
