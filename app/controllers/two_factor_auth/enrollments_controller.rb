class TwoFactorAuth::EnrollmentsController < ApplicationController
  include TwoFactorAuthable

  before_action  :ensure_tfa_settings_access
  before_action  :load_enrollment
  before_action  :load_two_factor_auth, only: :show
  before_action  :set_toll_fraud_validation_variables, only: :show

  skip_before_action :set_gon_two_factor_auth_prompt

  def show
    set_tfa_gon_variables(
      current_user,
      "enrollment",
      two_factor_auth_enrollments_path,
      api_backstage_two_factor_auth_app_callbacks_path(format: :json)
    ) if @enrollment.on_authentication_action?

    if @enrollment.is_complete?
      TwoFactorAuth::EnrollmentMailerWorker.perform_async(current_user.id)
      @enrollment.reset_steps
      redirect_to edit_person_path(current_user, tab: "account")
    else
      get_step
    end
  end

  def update
    respond_to do |format|
      format.html do
        @enrollment.submit_step(params[:tfa_form])
        redirect_to two_factor_auth_enrollments_path
      end
      format.json do
        @enrollment.step_success
        render json: { status: "successful" }
      end
    end
  end

  def destroy
    if params[:cancel]
      current_user.two_factor_auth.try(:disable!)
      redirect_to edit_person_path(current_user, tab: "account")
    else
      success_callbacks = [:disable_two_factor_auth]
      force_two_factor_authenticate!(success_callbacks)
    end
  end

  private

  def load_enrollment
    @enrollment = TwoFactorAuth::EnrollmentForm.new(current_user)
  end

  def load_two_factor_auth
    @two_factor_auth = current_user.two_factor_auth || current_user.build_two_factor_auth
  end

  def get_step
    step_back = params[:back_to_previous_step] == "true"
    @step = step_back ? @enrollment.step_back : @enrollment.current_step
  end

  def ensure_tfa_settings_access
    redirect_to dashboard_path unless has_tfa_access
  end

  def has_tfa_access
    is_not_admin_session && two_factor_feature_enabled?
  end

  def is_not_admin_session
    session[:admin].nil?
  end
end
