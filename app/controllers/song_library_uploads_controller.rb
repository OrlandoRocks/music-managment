class SongLibraryUploadsController < ApplicationController
  def create
    soundout_song = SongLibraryUpload.new(song_library_upload_params)

    begin
      soundout_song.save

      if soundout_song.errors[:song_library_asset].present?
        set_flash_invalid_mp3_error
      elsif soundout_song.errors.present?
        set_flash_upload_error
      end
    rescue => e
      Airbrake.notify("SongLibraryUpload error", e) if Rails.env.production?

      set_flash_upload_error
    end

    redirect_to purchase_soundout_reports_url
  end

  private

  def set_flash_upload_error
    flash[:error] = custom_t("controllers.song_library_uploads.error_saving_song")
  end

  def set_flash_invalid_mp3_error
    flash[:error] = custom_t("controllers.song_library_uploads.invalid_mp3_error")
  end

  def song_library_upload_params
    params
      .permit(:name, :artist_name, :genre_id, :song_library_asset)
      .merge(person: current_user)
  end
end
