class PublishingSplitsController < ApplicationController
  helper_method :sort_column, :sort_direction

  before_action :load_person, :requires_publishing_purchase
  before_action :prevent_non_admin_editing, only: [:create]
  before_action :load_my_compositions, only: [:index, :paginate, :confirm]
  before_action :load_previous_page_params, only: [:confirm_splits]

  layout "application"

  def index
    @albums = @my_compositions.albums
    @singles = @my_compositions.singles
    @ringtones = @my_compositions.ringtones

    respond_to do |format|
      format.html
    end
  end

  def paginate
    respond_to do |format|
      format.js {
        render partial: "missing_splits",
               content_type: "text/html",
               locals: { missing_splits_paginated: @missing_splits }
      }
    end
  end

  # POST /publishing_splits
  def create
    songs_array = []
    if params[:splits]
      splits = ActiveSupport::JSON.decode(params[:splits])
      songs_array = splits.map { |s| { song_id: s["song_id"], percent: s["split_pct"].to_f } }
      Composition.batch_submit_splits(songs_array)

      render layout: false, json: { status: 0 }
    else
      songs = params[:songs]
      selected_splits = songs.select { |s| !s["split"].empty? }

      songs_array =
        selected_splits.map do |s|
          if s["split"] == "other"
            { song_id: s["song_id"], percent: s["custom_split"].to_f }
          else
            { song_id: s["song_id"], percent: s["split"].to_f }
          end
        end
      Composition.batch_submit_splits(songs_array)

      redirect_to controller: "song_registrations", action: "index"
    end
  end

  def confirm_splits
    @songs =
      params[:songs].select do |s|
        (s["split"].present? && s["split"] != "other") ||
          (s["custom_split"] && s["custom_split"].match(/^\d{1,3}(\.\d{0,2})?$/))
      end

    if @songs.empty?
      flash[:error] = custom_t("controllers.publishing_split.no_split_percentages_set")
      redirect_to action: "index"
    else
      respond_to do |format|
        format.html
      end
    end
  end

  private

  def requires_publishing_purchase
    redirect_to home_path unless current_user.paid_for_composer?
  end

  def sort_column
    %w[songs.name publishing_splits.percent].include?(params[:sort]) ? params[:sort] : "songs.name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def load_my_compositions
    @my_compositions = MyCompositions.new(current_user)
    params[:page] ||= 1
    params[:per_page] ||= 25
    order = "#{sort_column} #{sort_direction}"
    @missing_splits = @my_compositions.search(
      page: params[:page],
      per_page: params[:per_page],
      order: order,
      keyword: params[:search],
      filters: { album_ids: params[:release] },
      missing_splits_only: true
    )
    @songs_selected = params[:songs] || []
  end

  def load_previous_page_params
    songs =
      params[:songs].select { |s| !s["split"].try(:empty?) }.map do |s|
        { song_id: s["song_id"], split: s["split"] }
      end
    @url_params = {
      page: params[:page],
      per_page: params[:per_page],
      sort: sort_column,
      direction: sort_direction,
      search: params[:search],
      release: params[:release],
      songs: songs
    }.to_query
  end

  def prevent_non_admin_editing
    return unless under_admin_control? && !current_admin.has_role?("Publishing Manager", false)

    flash[:errors] = custom_t("controllers.publishing_split.only_admin_with_publishing_manager_role_is_allowed")
    redirect_to controller: "song_registrations", action: "index"
  end

  # Set paper trail's user to admin if account is taken over
  def user_for_paper_trail
    under_admin_control? ? current_admin : current_user
  end
end
