require "./lib/oauth/controllers/provider_controller"

class OauthController < ApplicationController
  include OAuth::Controllers::ProviderController
  before_action :logout_if_prompt_is_set, only: [:authorize]

  layout "tc-foundation"

  def authorize
    if params[:oauth_token]
      super
    else
      @client_application = ClientApplication.find_by_key! params[:client_id]
      if @client_application
        if request.post? || @client_application.skip_auth_form?
          @authorizer = OAuth::Provider::Authorizer.new current_user, true, params
          redirect_to @authorizer.redirect_uri
        else
          render action: "oauth2_authorize"
        end
      else
        oauth2_error("Missing or invalid parameter")
      end
    end
  end

  def oauth2_token_authorization_code
    @verification_code = Oauth2Verifier.find_by(token: params[:code])
    unless @verification_code
      oauth2_error
      return
    end
    @token = @verification_code.exchange!
    render json: @token
  end

  private

  def logout_if_prompt_is_set
    redirect_to logout_path(redirect: oauth_authorize_url(permitted_oauth_params)) if params.delete(:prompt)
  end

  def permitted_oauth_params
    params.permit(:action, :client_id, :controller, :redirect_uri, :response_type, :state)
  end
end

# override code in authorizer, since current TC implementation requires a secret
module OAuth
  module Provider
    class Authorizer
      def code
        @code ||= ::Oauth2Verifier.create!(
          {
            client_application: app,
            user: @user,
            scope: @params[:scope],
            callback_url: @params[:redirect_uri],
            secret: app.secret
          }
        )
      end
    end
  end
end
