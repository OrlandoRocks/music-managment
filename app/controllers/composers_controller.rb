class ComposersController < ApplicationController
  include PublishingAdministrationHelper

  before_action :redirect_to_publishing_page, only: [:index, :new, :edit], if: :rights_app_enabled?
  before_action :load_person
  before_action :restrict_admin_access, only: [:edit_tax_info, :update_tax_info]
  before_action :require_publishing_access
  before_action :load_composer, only: [:edit, :update]
  before_action :redirect_to_composers_dashboard, only: :create

  layout "application_old"

  def index
    @page_title = custom_t(:publishing_dashboard)

    load_composers

    if @composers.empty? # the user hasn't entered composer info yet
      redirect_to action: "new"
    else # the user has already entered songwriter info, may or may not have paid yet tho
      @composer = @composers.first
      load_index

      render layout: "application"
    end
  end

  # GET /composers/new
  # GET /composers/new.xml
  def new
    @page_title = custom_t("controllers.composer.signup_for_publishing_administration")

    load_composers
    if @composers.size >= 1 # don't let people create more than 1 composer for now.
      redirect_to "/composers"
    else
      @composer = Composer.new
      @publisher = @composer.build_publisher
      respond_to do |format|
        format.html { render "registration", layout: "application" }
        format.xml  { render xml: @composer }
      end
    end
  end

  def show
    redirect_to "/composers"
  end

  def edit
    @composer.agreed_to_terms_at = nil
    render "registration", layout: "application"
  end

  def update
    setup_params_for_composer
    @composer.agreed_to_terms_at = Time.now
    setup_publisher

    if @composer.update(composer_update_params)
      flash[:error] = nil

      redirect_to products_add_composer_registration_to_cart_path(composer_id: @composer.id)
    else
      flash.now[:error] = custom_t("controllers.composer.your_registration_couldnt_be_saved")
      @composer.agreed_to_terms_at = nil
      render "registration", layout: "application"
    end
  end

  def update_cae_number
    load_composer
    if @composer.update(composer_update_params)
      flash[:error] = nil
      redirect_to "/composers"
    else
      load_index
      flash[:error] = custom_t("controllers.people.errors_updating_account")
      render "index", layout: "application"
    end
  end

  def destroy
    redirect_to "/composers"
  end

  # POST /composers
  # POST /composers.xml
  def create
    params[:composer][:person] = current_user
    setup_params_for_composer
    @composer = Composer.new(composer_create_params)
    @composer.agreed_to_terms_at = Time.now
    # This is not necessary and resulted in invalid data being saved into publishers table which was not a problem in Rails 2.3.2 but
    # a problem in Rails 2.3.14 due to fixing a bug with not running validation on autosave
    # https://rails.lighthouseapp.com/projects/8994/tickets/2249-autosaving-associations-skips-validations-and-persists-invalid-data-in-the-db
    # @publisher = @composer.build_publisher
    @publisher = Publisher.new
    respond_to do |format|
      if @composer.save
        flash[:error] = nil
        format.html { redirect_to products_add_composer_registration_to_cart_path(composer_id: @composer.id) }
      else
        flash.now[:error] = custom_t("controllers.composer.your_registration_couldnt_be_saved")
        @composer.agreed_to_terms_at = nil
        format.html { render action: "registration", layout: "application" }
      end
    end
  end

  def tos
    @page_title = custom_t("controllers.composer.adminstration_amendment_to_tunecores_terms_and_conditions")
    render layout: "application_lightbox"
  end

  def download
    head(:not_found) and return if (royalty_payment = RoyaltyPayment.find(params[:id])).nil?
    head(:forbidden) and return unless royalty_payment.downloadable?(current_user)

    path = (params[:format] == "pdf") ? royalty_payment.pdf_summary_file_name : royalty_payment.csv_summary_file_name
    head(:bad_request) and return unless params[:format].to_s == File.extname(path).gsub(/^\.+/, "")

    redirect_to(royalty_payment.authenticated_url(params[:format]))
  end

  private

  def composer_create_params
    composer_update_params
  end

  def composer_update_params
    params
      .require(:composer)
      .permit(
        { dob: [:month, :day, :year] },
        :alternate_email,
        :agreed_to_terms,
        :country,
        :cae,
        :name_suffix,
        :last_name,
        :alternate_phone,
        :agreed_to_terms_at,
        :middle_name,
        :first_name
      )
  end

  def publisher_params
    params
      .require(:publisher)
      .permit(:name, :cae, :performing_rights_organization_id)
  end

  def load_composer
    @composer = current_user.composers.find(params[:id])
  end

  def load_composers
    @composers = current_user.composers
  end

  def load_index
    @yt = current_user.youtube_preference
    @youtube_monetization = current_user.youtube_monetization
    @my_compositions = MyCompositions.new(current_user)
    @disputes = @my_compositions.compositions_in_dispute
    @albums = @my_compositions.albums
    @singles = @my_compositions.singles
    @ringtones = @my_compositions.ringtones
    @statuses = @my_compositions.statuses
    @stats = @my_compositions.stats
    @splits = @my_compositions.splits
    @open_composer_purchase = Purchase.open_composer_purchase(current_user)

    @add_to_cart_url = "/products/add_composer_registration_to_cart?composer_id=#{@composer.id}"
  end

  def setup_params_for_composer
    date_of_birth =
      if composer_update_params[:dob].present?
        dob = composer_update_params[:dob]
        [dob[:month], dob[:day], dob[:year]].join("/")
      end

    params[:composer].merge!(dob: date_of_birth)
  end

  def setup_publisher
    @publisher = Publisher.find_or_initialize_by(
      name: publisher_params[:name],
      cae: publisher_params[:cae],
      performing_rights_organization_id: publisher_params[:performing_rights_organization_id]
    )

    @composer.publisher = @publisher
  end

  def setup_submit_text
    @submit_text =
      if @composer.completed_w9?
        custom_t("controllers.composer.update_my_tax_info")
      else
        custom_t("controllers.composer.submit_my_registration")
      end
  end

  def collect_understandable_error
    error_msg = ""
    @composer.understandable_error_messages.each do |msg|
      error_msg << "<li>#{msg}</li>"
    end
    "<ul>#{error_msg}</ul>"
  end

  def restrict_admin_access
    return unless under_admin_control?

    flash[:error] = custom_t("controllers.composer.admin_are_not_permitted_to_update_tax_info")
    redirect_back fallback_location: dashboard_path
  end

  def redirect_to_composers_dashboard
    redirect_to publishing_composers_path if current_user.publishing_composers.exists?
  end
end
