module MetaTags::Taggable
  DEFAULT_COUNTRY_ID = 1

  def self.included(base)
    base.before_action :set_country_website
  end

  def set_country_website
    @country_website = LOCALE_TO_COUNTRY_MAP[locale.to_s] ? CountryWebsite.find_by(country: LOCALE_TO_COUNTRY_MAP[locale.to_s]) : CountryWebsite.first
  end

  def set_page_title
    @page_title = get_meta_tags(:title, @country_website.id).first.try(:content)

    @page_title = get_meta_tags(:title, DEFAULT_COUNTRY_ID).first.try(:content) if @page_title.blank?
  end

  def set_meta_tags
    @metatags = get_meta_tags(MetaTag.meta_tags, @country_website.id)

    @metatags = get_meta_tags(MetaTag.meta_tags, DEFAULT_COUNTRY_ID) if @metatags.empty?

    return unless @metatags.empty?

    @metatags = MetaTag.where(
      page_name: :default,
      country_website_id: DEFAULT_COUNTRY_ID,
      name: MetaTag.meta_tags
    )
  end

  def set_canonical_link
    @canonical_link = get_meta_tags(:canonical, @country_website.id).first.try(:content)

    @canonical_link = request.url if @canonical_link.blank?
  end

  private

  def get_meta_tags(name, country_id)
    MetaTag.where(
      page_name: "#{controller_name}##{action_name}",
      country_website_id: country_id,
      name: name
    )
  end
end
