class ProductsController < ApplicationController
  include AutomatorManager
  include CartPlanable
  include PublishingAdministrationHelper
  include SpecializedRelease

  before_action :load_product, only: [:buy, :add_to_cart]
  before_action :ban_credits_if_plan, only: [:add_to_cart]
  before_action :validate_splits_collaborator, only: [:add_splits_collaborator_to_cart]

  HTTP_VIOLATION = "HTTP Violation".freeze

  layout "application_old"

  def buy
    @purchase = Product.add_to_cart(current_user, @product)
    verify_purchase_and_redirect
  end

  def add_to_cart
    log_get_add_to_cart

    redirect_to dashboard_path and return if plan_manually_carted? # This is merely the minimal criteria for ECOM-1235

    if params[:album_id]
      album = current_user.albums.find(params[:album_id])
      manage_automator_state(params)
      automator_products_update(album, params)
      remove_unsupported_salepoints(album)
    end

    @purchase =
      if params[:product_type] == "youtube-money"
        current_user.youtube_monetization.create_youtube_purchase(@product)
      elsif params[:product_type] == "social"
        SubscriptionProduct.create_subscription_purchase(current_user, @product, params[:product_type].capitalize)
      elsif !album
        Product.add_to_cart(current_user, @product)
      else
        Product.add_to_cart(current_user, album, @product)
      end

    verify_purchase_and_redirect
  end

  def add_stores_to_cart
    album = current_user.albums.find(params[:album_id])
    salepoints = Salepoint.salepoints_to_add_to_cart(album)
    Product.add_to_cart(current_user, salepoints)
    remove_unsupported_salepoints(album)

    album.add_automator if params[:add_automator]
    if (album.salepoint_subscription && album.salepoint_subscription.can_distribute?)
      Product.add_to_cart(current_user, album.salepoint_subscription)
    end

    redirect_to cart_path
  end

  def add_to_cart_with_credit
    album = current_user.albums.find(params[:album_id])
    manage_automator_state(params)
    automator_products_update(album, params)
    remove_unsupported_salepoints(album)

    if @purchase = album.create_purchase_with_credit
      verify_purchase_and_redirect
    else
      flash[:error] = custom_t("controllers.products.credit_can_not_be_applied")
      Rails.logger.info("Unable to apply credit: #{album.class} #{album.id} for person #{current_user.id}")
      redirect_to dashboard_path
    end
  end

  def add_composer_registration_to_cart
    composer = current_user.account.publishing_composers.unscoped.find(params[:composer_id])
    @purchase = Product.add_to_cart(current_user, composer)
    logger.info("Added composer #{composer.id} to cart")

    verify_purchase_and_redirect
  end

  def distribution_credits
    redirect_to dashboard_path and return if owns_or_carted_plan?

    @page_title = custom_t("controllers.products.distribution_credits")
    @album_products = Product.list_packages_by_ids(current_user, :album)
    @single_products = Product.list_packages_by_ids(current_user, :single)
    @ringtone_products = Product.list_packages_by_ids(current_user, :ringtone)

    respond_to do |format|
      format.html {
        render layout: "application"
      }
    end
  end

  def add_renewals_to_cart
    renewals = current_user.renewals.with_renewal_information.find(params[:ids])
    renewals.each do |renewal|
      product = Product.find(renewal.renewal_product_id)
      Product.add_to_cart(current_user, renewal, product)
    end

    redirect_to cart_path
  end

  def add_splits_collaborator_to_cart
    current_user.add_splits_collaborator_to_cart!

    redirect_to cart_path
  end

  private

  # Log all insecure GET requests for followup refactoring to POSTs
  # TODO: Remove when this stops getting triggered
  def log_get_add_to_cart
    return unless request.get?

    Airbrake.notify(
      "#{self.class} HTTP_VIOLATION",
      {
        host: request.host,
        path: request.path,
        referer: request.referrer
      }
    )
  end

  def plan_manually_carted?
    request.referrer.nil? && request.path_parameters[:id]&.to_i.in?(Product::PLAN_PRODUCT_IDS)
  end

  def load_product
    product_id = params[:product_id] || params[:id]
    @product = Product.where("id = ? AND status = 'Active'", product_id.to_i).first

    return unless @product.nil?

    flash[:error] = custom_t("controllers.products.product_no_longer_available")
    redirect_to controller: "dashboard"
  end

  def verify_purchase_and_redirect
    if @purchase && !@purchase.errors.empty?
      # don't display field for Cost cents.
      flash[:error] = @purchase.errors
                               .map { |_key, value| custom_t(value) }
                               .join(" ")
      redirect_to controller: "dashboard" and return
    end

    redirect_to cart_path
  end

  def remove_unsupported_salepoints(album)
    SalepointRemovalService.remove_unsupported_salepoints(album)
  end

  def ban_credits_if_plan
    return unless (owns_or_carted_plan? && @product&.is_a_credit_product?)

    flash[:error] = custom_t("plans.errors.cannot_add_credit")
    redirect_to dashboard_path
  end

  def validate_splits_collaborator
    if current_user.active_splits_collaborator?
      flash[:error] = custom_t("controllers.products.user_has_active_collaborator")
      redirect_to dashboard_path and return
    end

    return if current_user.user_or_cart_plan?(Plan::NEW_ARTIST)

    flash[:error] = custom_t("controllers.products.not_new_artist_plan")
    redirect_to dashboard_path and return
  end
end
