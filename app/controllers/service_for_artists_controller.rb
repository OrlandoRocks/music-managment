class ServiceForArtistsController < ApplicationController
  include YoutubeOAC

  layout "tc-foundation"

  def apple
    redirect_to_dashboard unless amfa_enabled?
    load_artists(ExternalServiceId::APPLE_SERVICE)
    set_selected_artist
  end

  def spotify
    redirect_to_dashboard unless spotify_enabled?
    load_artists(ExternalServiceId::SPOTIFY_SERVICE)
  end

  def youtube_oac
    redirect_to_dashboard unless show_yt_oac?

    @onboarded_all_potential_artists = onboarded_all_potential_artists?
    load_artists(ExternalServiceId::YOUTUBE_CHANNEL)
  end

  def spotify_access
    store_artist_name(ExternalServiceId::SPOTIFY_SERVICE)
    spotify_authentication_path = Spotify::SpotifyForArtistsService.authenticate_spotify_user
    redirect_to spotify_authentication_path
  end

  def spotify_redirect_access
    load_artist_name(ExternalServiceId::SPOTIFY_SERVICE)
    if error_params.present?
      redirect_to ENV["SPOTIFY_FOR_ARTISTS_USER_URL"]
    elsif @artist_name.nil?
      redirect_to spotify_for_artists_path
    else
      spotify_onboard_path = Spotify::SpotifyForArtistsService.request_spotify_for_artists_access(spotify_auth_params)
      redirect_to spotify_onboard_path
    end
  end

  def authenticate_youtube_user
    store_artist_name(ExternalServiceId::YOUTUBE_SERVICE)
    redirect_to YoutubeMusic::OfficialArtistChannelService.authorization_redirect_url
  end

  def youtube_auth_callback
    return if youtube_response_params.blank?

    load_artist_name(ExternalServiceId::YOUTUBE_SERVICE)
    youtube_service = YoutubeMusic::OfficialArtistChannelService.new(youtube_response_params)
    youtube_service.fetch_and_store_channel
    youtube_service.enqueue_distribution!
    redirect_to dashboard_path popup: "oac_success"
  end

  private

  def safe_artist_name(name)
    names = Creative.person_unique_artist_names_no_oac(current_user)
    names.find { |n| n == name }
  end

  def artist_names
    params[:artists] || safe_artist_name(params[:artist_name])
  end

  def load_artists(service)
    if service == ExternalServiceId::YOUTUBE_CHANNEL
      @artists = oac_eligible_artists
    else
      builder = ServiceForArtists::QueryBuilder.new(current_user.id, service)
      @artists = builder.artists_with_album_ids.map { |artist| [artist.artist_name, artist.identifier] }
    end
  end

  def load_artist_name(service)
    @artist_name = $redis.get("#{service}artist:user_id:#{current_user.id}")
    $redis.del("#{service}artist:user_id:#{current_user.id}")
  end

  def store_artist_name(service)
    $redis.set("#{service}artist:user_id:#{current_user.id}", artist_names.to_s)
  end

  def set_selected_artist
    @selected_artist = @artists.first[1] if (@artists.present? && @artists.length == 1)
  end

  def spotify_auth_params
    params.permit(:state, :code).merge(current_user: current_user, artist_uri: @artist_name)
  end

  def error_params
    params.permit(:error)
  end

  def youtube_response_params
    yt_params = params.permit(:code, :scope)
    yt_params.merge(person: current_user, artist_name: @artist_name) unless yt_params.empty?
  end
end
