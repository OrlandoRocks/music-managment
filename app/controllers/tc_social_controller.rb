class TcSocialController < ApplicationController
  before_action :load_plan_status

  include TcSocialHelper

  def index
    redirect_to tc_social_dashboard_url
  end

  def create
    cookies[:tc_social_message] = { domain: COOKIE_DOMAIN, value: encode_url(params[:message]) }
    redirect_to tc_social_dashboard_url
  end

  private

  def encode_url(url)
    CGI.escape(url)
  end

  def load_plan_status
    @plan_status ||= ::Social::PlanStatus.for(current_user)
  end
end
