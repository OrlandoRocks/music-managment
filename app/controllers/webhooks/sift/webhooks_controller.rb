class Webhooks::Sift::WebhooksController < ApplicationController
  skip_before_action :login_required, only: [:update]
  skip_before_action :verify_authenticity_token, only: [:update]

  before_action :authenticate_signature, only: [:update]

  def update
    begin
      options = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
    rescue ArgumentError => e
      render json: { message: e.message }, status: :bad_request and return
    end

    Webhooks::WebhookWorker.perform_async("sift", options)

    render json: { message: "OK" }, status: :ok
  end

  private

  def authenticate_signature
    postback_signature = request.env["HTTP_X_SIFT_SCIENCE_SIGNATURE"]
    body = request.body.present? ? JSON.parse(request.body).to_json : ""
    digest = OpenSSL::Digest.new("sha256")
    calculated_hmac = OpenSSL::HMAC.hexdigest(digest, sift_webhook_key, body)
    verification_signature = "sha256=#{calculated_hmac}"

    render json: { message: "Unauthorized" }, status: :forbidden if verification_signature != postback_signature
  end

  def sift_webhook_key
    ENV.fetch("X_SIFT_SCIENCE_SIGNATURE_KEY") do
      Tunecore::Airbrake.notify("ENV variable X_SIFT_SCIENCE_SIGNATURE_KEY is missing")
      raise "ENV variable X_SIFT_SCIENCE_SIGNATURE_KEY is missing"
    end
  end
end
