class EftBatchTransactionsController < ApplicationController
  include TwoFactorAuthable
  include PayoutProvidable

  before_action :redirect_to_payout_provider_withdrawal!, if: :payoneer_enabled_and_approved?
  before_action :load_person
  before_action :restrict_based_on_united_states_and_territories, only: [:new, :create, :edit, :destroy]
  before_action :page_title
  before_action :two_factor_authenticate!, only: [:new], if: :two_factor_auth_required?
  before_action :redirect_to_withdraw!, only: [:new, :create, :confirm], if: :payoneer_mandatory?

  def new
    load_form_variables
    @eft_batch_transaction = EftBatchTransaction.new
  end

  def create
    @stored_bank_account = @person.stored_bank_accounts.current
    @eft_batch_transaction = EftBatchTransaction.new(
      permitted_params.merge(stored_bank_account: @stored_bank_account)
    )

    if @eft_batch_transaction.save
      redirect_to eft_batch_transaction_path(@eft_batch_transaction)
    else
      load_form_variables
      render action: :new
    end
  end

  def confirm
    if permitted_params.present?
      eft_batch_transaction_params = permitted_params.merge(
        amount_from_form: Tunecore::CurrencyInputNormalizer.normalize_currency_input(
          permitted_params,
          :amount_from_form_main_currency,
          :amount_from_form_fractional_currency
        )
      )

      load_form_variables

      @stored_bank_account = @person.stored_bank_accounts.current

      @eft_batch_transaction = EftBatchTransaction.new(eft_batch_transaction_params.merge(stored_bank_account: @stored_bank_account))

      render action: :new unless @eft_batch_transaction.valid?
    else
      redirect_to new_eft_batch_transaction_path
    end
  end

  def show
    @eft_batch_transaction = EftBatchTransaction.find_by(id: params[:id], stored_bank_account_id: @person.stored_bank_accounts.collect(&:id))
    @eft_expected_days_lower = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_LOWER
    @eft_expected_days_upper = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_UPPER
  end

  private

  def permitted_params
    params
      .require(:eft_batch_transaction)
      .permit(
        :amount_from_form,
        :amount_from_form_main_currency,
        :amount_from_form_fractional_currency,
        :customer_eft_page,
        :total,
        :confirm_page,
        :password_entered,
        :payout_service_fee_id,
        :stored_bank_account,
        :from_form
      )
  end

  def load_form_variables
    @balance = @person.person_balance.balance
    @stored_bank_account = @person.stored_bank_accounts.current
    @eft_service_fee = PayoutServiceFee.current("eft_service")
    @eft_failure_fee = PayoutServiceFee.current("eft_failure")
    @eft_max_amount = EftBatchTransaction::MAX_WITHDRAWAL
  end

  def page_title
    @page_title = custom_t("controllers.eft_branch_transactions.electronic_funds_transfer")
  end
end
