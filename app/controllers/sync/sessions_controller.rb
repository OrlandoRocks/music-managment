class Sync::SessionsController < SessionsController
  layout "sync"

  def create
    # FIXME: Could not find where session is created even when login failed
    if !cookies[:tunecore_id]
      reroute_no_cookies
    else
      @page_title = "Login"

      authenticated_user = Person.authenticate(params[:person])

      if authenticated_user
        self.current_user = authenticated_user
        add_sso_cookie(for_user_id: authenticated_user.id)

        reroute_logged_in_user
      elsif Person.account_locked?(params[:person][:email])
        person = Person.find_by(email: params[:person][:email])
        reroute_locked_account(person)
      else

        reroute_failed_login
      end
    end
  end

  def destroy
    remove_sso_cookie
    remove_takeover_cookie
    remove_oauth_tokens
    reset_session
    redirect_to(sync_root_path)
  end

  protected

  def login_disallowed
    if !feature_enabled?(:sync_licensing)
      super()
    elsif current_user.is_administrator?
      redirect_to(sync_admin_root_path)
    else
      redirect_to(week_sync_chart_path)
    end
  end

  def reroute_logged_in_user
    if !feature_enabled?(:sync_licensing)
      super()
    elsif current_user.is_administrator?
      redirect_to(sync_admin_root_path)
    else
      ensure_correct_country_domain(session[:return_to] || "/synch/chart/week")
    end
  end
end
