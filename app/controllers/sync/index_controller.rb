class Sync::IndexController < ApplicationController
  skip_before_action :login_required
  layout "sync"

  def index
    if logged_in?
      redirect_to week_sync_chart_path
    else
      redirect_to sync_home_path
    end
  end

  def home
    @playlists =
      ActiveSupport::JSON.decode(
        open("http://s3.amazonaws.com/tunecore.site.json/spotify_playlists.json").read
      )
  end

  def playlists
    @playlists =
      ActiveSupport::JSON.decode(
        open("http://s3.amazonaws.com/tunecore.site.json/spotify_playlists.json").read
      )
  end
end
