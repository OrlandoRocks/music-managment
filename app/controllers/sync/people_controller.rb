class Sync::PeopleController < Sync::SyncController
  layout "sync"

  before_action :login_required,       except: [:new, :create]
  before_action :require_sync_feature, except: [:new, :create]
  before_action :login_disallowed,     only: [:new, :create]
  before_action :load_person,          only: [:show, :edit_email, :edit_password, :edit_contact, :edit_address, :update_email, :update_password, :update_contact, :update_address]
  before_action :get_request_code,     only: [:new, :create]

  skip_before_action :check_for_verification

  def new
    @person       = Person.new

    respond_to do |format|
      format.html
    end
  end

  def create
    @person = @sync_request.complete_registration(person_params)

    if @sync_request.valid? && @person.valid?
      # log the user in
      self.current_user = @person

      respond_to do |format|
        format.html {
          redirect_to week_sync_chart_path
        }
      end
    else
      respond_to do |format|
        format.html {
          render action: :new
        }
      end
    end
  end

  def edit_email
    respond_to do |format|
      format.html { render partial: "edit_email" }
      format.js { render partial: "edit_email", layout: false, content_type: "text/html" }
    end
  end

  def edit_password
    respond_to do |format|
      format.html { render partial: "edit_password" }
      format.js { render partial: "edit_password", layout: false, content_type: "text/html" }
    end
  end

  def edit_contact
    respond_to do |format|
      format.html { render partial: "edit_contact" }
      format.js { render partial: "edit_contact", layout: false, content_type: "text/html" }
    end
  end

  def edit_address
    respond_to do |format|
      format.html { render partial: "edit_address" }
      format.js { render partial: "edit_address", layout: false, content_type: "text/html" }
    end
  end

  def email_info
    respond_to do |format|
      format.html { render partial: "email_info" }
      format.js { render partial: "email_info", layout: false }
    end
  end

  def password_info
    respond_to do |format|
      format.html { render partial: "password_info" }
      format.js { render partial: "password_info", layout: false }
    end
  end

  def contact_info
    respond_to do |format|
      format.html { render partial: "contact_info" }
      format.js { render partial: "contact_info", layout: false }
    end
  end

  def address_info
    respond_to do |format|
      format.html { render partial: "address_info" }
      format.js { render partial: "address_info", layout: false }
    end
  end

  def update_email
    old_email = @person.email
    if @person.update(person_params)
      handle_changed_email(old_email)
      flash[:notice] = custom_t("controllers.people.updated_profile")
      respond_to do |format|
        format.html { redirect_to action: "edit" }
        format.js { render partial: "email_info", layout: false }
      end
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
      respond_to do |format|
        format.html { render partial: "edit_email" }
        format.js { render partial: "edit_email", layout: false }
      end
    end
  end

  def update_password
    if @person.update(person_params)
      notify_password_change
      flash[:notice] = custom_t("controllers.people.updated_profile")
      respond_to do |format|
        format.html { redirect_to action: "edit" }
        format.js { render partial: "password_info", layout: false }
      end
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
      respond_to do |format|
        format.html { render partial: "edit_password" }
        format.js { render partial: "edit_password", layout: false }
      end
    end
  end

  def update_contact
    if @person.update(person_params)
      flash[:notice] = custom_t("controllers.people.updated_profile")
      respond_to do |format|
        format.html { redirect_to "edit" }
        format.js { render partial: "contact_info", layout: false }
      end
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
      respond_to do |format|
        format.html { render partial: "edit_contact" }
        format.js { render partial: "edit_contact", layout: false }
      end
    end
  end

  def update_address
    if @person.update(person_params)
      flash[:notice] = custom_t("controllers.people.updated_profile")
      respond_to do |format|
        format.html { redirect_to "edit" }
        format.js { render partial: "address_info", layout: false }
      end
    else
      flash.now[:error] = custom_t("controllers.people.errors_updating_account")
      respond_to do |format|
        format.html { render partial: "edit_address" }
        format.js { render partial: "edit_address", layout: false }
      end
    end
  end

  def show
    @page_title = "My Account"
    @tab = "account"
    @person.valid? # display any validation error messages
  end

  def load_person
    raise ActiveRecord::RecordNotFound unless current_user

    @person = current_user
  end

  protected

  def handle_changed_email(_old_email)
    PersonNotifier.change_email(@person, @person.email).deliver
  end

  def notify_password_change
    PersonNotifier.change_password(@person, params[:person][:password]).deliver
  end

  private

  def get_request_code
    @sync_request =
      if params[:request_code]
        SyncMembershipRequest
          .where(request_code: params[:request_code]).first
      else
        nil
      end

    # No Request
    if @sync_request.blank?
      respond_to do |format|
        format.html {
          flash[:error] = custom_t("controllers.people.cant_find_membership_request")
          redirect_to sync_root_path
        }
      end
    # Request already registered
    elsif @sync_request.person_id.present?
      respond_to do |format|
        format.html {
          flash[:message] = custom_t("controllers.people.member_already_registered")
          redirect_to sync_login_path
        }
      end
    end
  end

  def person_params
    params
      .require(:person)
      .permit(
        :accepted_terms_and_conditions,
        :is_opted_in,
        :address1,
        :address2,
        :city,
        :state,
        :zip,
        :country,
        :email,
        :apple_id,
        :apple_role,
        :name,
        :original_password,
        :password,
        :password_confirmation,
        :postal_code,
        :phone_number,
        :mobile_number
      )
  end
end
