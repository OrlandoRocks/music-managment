class Sync::SyncController < ApplicationController
  include Sync::SearchHelper

  layout "sync"

  before_action :require_sync_feature

  def require_sync_feature
    if feature_enabled?(:sync_licensing)
      true
    else
      redirect_to dashboard_path
    end
  end

  def require_admin
    if current_user && current_user.is_administrator?
      true
    else
      redirect_to sync_root_path
    end
  end

  def access_denied
    redirect_to(sync_login_path)
  end

  def login_disallowed
    if feature_enabled?(:sync_licensing)
      if current_user.is_administrator?
        redirect_to(sync_admin_root_path)
      else
        redirect_to(week_sync_chart_path)
      end
    else
      super()
    end
  end

  def sync_licensing_feature
    Feature.where("name=?", "sync_licensing").first
  end
end
