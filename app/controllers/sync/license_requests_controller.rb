class Sync::LicenseRequestsController < Sync::SyncController
  before_action :load_person
  before_action :require_sync_feature
  before_action :find_song, only: [:create, :new, :confirmation]
  before_action :find_downloadable_request, only: [:download]

  def index
    @license_requests = current_user
                        .sync_license_requests.where("status!=?", SyncLicenseRequest::STATUSES[:licensed])
                        .order("created_at DESC")

    @license_requests.each do |license_request|
      license_request[:composer_names] = []

      # Use composers from the muma song first
      if license_request.song.muma_songs.present? && license_request.song.muma_songs.first.composers.present?
        license_request[:composer_names] =
          license_request
          .song
          .muma_songs
          .first
          .composers
          .collect { |composer| "#{composer.composer_first_name} #{composer.composer_last_name}" }
      end

      # Fall back on composer from TC composition
      next unless license_request[:composer_names].blank? &&
                  license_request.song.composition &&
                  license_request.song.composition.publishing_splits.blank?

      license_request[:composer_names] =
        license_request
        .song
        .composition
        .publishing_splits
        .map { |split| "#{split.composer.first_name} #{split.composer.last_name}" }
    end

    @licensed_requests = current_user
                         .sync_license_requests
                         .where("status=?", SyncLicenseRequest::STATUSES[:licensed])
                         .order("created_at DESC")

    @licensed_requests.each do |licensed_request|
      # Use composers from the muma song first
      if licensed_request.song.muma_songs.present? &&
         licensed_request.song.muma_songs.first.composers.present?

        licensed_request[:composer_names] =
          licensed_request
          .song
          .muma_songs
          .first
          .composers
          .collect { |composer| "#{composer.composer_first_name} #{composer.composer_last_name}" }
      end

      # Fall back on composer from TC composition
      next unless licensed_request[:composer_names].blank? &&
                  licensed_request.song.composition &&
                  song.composition.publishing_splits.present?

      licensed_request[:composer_names] =
        licensed_request
        .song
        .composition
        .publishing_splits
        .map { |split| "#{split.composer.first_name} #{split.composer.last_name}" }
    end

    respond_to do |format|
      format.html
    end
  end

  def show
    @sync_request = current_user.sync_license_requests.find(params[:id])
  end

  def new
    # return list of productions to allow user to use a previously
    # entered production
    @productions = @person.sync_license_productions
    @request_license = SyncLicenseRequest.new
    @request_license.build_sync_license_production
    @request_license.sync_license_options.build
  end

  def confirmation
    build_request

    if @request_license.valid?
      respond_to do |format|
        format.html
      end
    else
      @request_license.build_sync_license_production unless @request_license.sync_license_production
      @request_license.sync_license_options.build if @request_license.sync_license_options.blank?
      @productions = @person.sync_license_productions
      render action: :new
    end
  end

  def create
    build_request

    if @request_license.save
      notify_request_created(@person, @request_license, @song, @muma_song)

      respond_to do |format|
        format.html do
          flash[:message] = "Your request has been submitted. Thank you."
          redirect_to sync_license_request_path(@request_license)
        end

        format.js do
          render json: {
            status: 0,
            upload_url: sync_license_request_path(@request_license)
          }
        end
      end
    else
      respond_to do |format|
        # rebuild the assocations to be rendered out in the
        # new action form
        @request_license.build_sync_license_production unless @request_license.sync_license_production
        @request_license.sync_license_options.build if @request_license.sync_license_options.blank?
        @productions = @person.sync_license_productions

        format.html { render action: :new }
        format.js   { render json: { status: 500 } }
      end
    end
  end

  # currently only coded to handle updates for re-uploading request file
  def update
    if params[:request_upload].blank?
      respond_to do |format|
        format.js { render json: { status: 500 } }
      end
    else
      sync_request = @person.sync_license_requests.find(params[:id])

      if sync_request.update(request_document: params[:request_upload])
        respond_to do |format|
          format.js { render json: { status: 0 } }
        end
      else
        respond_to do |format|
          format.js { render json: { status: 500 } }
        end
      end
    end
  end

  def download
    redirect_to @request_license.authenticated_url(10)
  end

  private

  def sync_license_options_attributes
    params[:sync_license_request] &&
      params[:sync_license_request][:sync_license_options_attributes]
  end

  def sync_license_options_params
    @sync_license_options_params ||=
      sync_license_options_attributes&.keys&.each_with_object({}) do |id, whitelist|
        values = sync_license_options_attributes[id]
        whitelist[id] = { offer: values[:offer], number: values[:number] }
      end
  end

  def sync_license_request_params
    # NOTE: After Rails 5.1.2 this can just be
    #
    # .permit(
    #   sync_license_request: {
    #     sync_license_production_attributes: [:title],
    #     sync_license_options_attributes: []
    #   }
    # )[:sync_license_request]
    #
    # And the nested options hash won't be munged.
    params
      .permit(
        sync_license_request: { sync_license_production_attributes: [:title] }
      )
      .tap do |whitelisted|
        if sync_license_options_params
          whitelisted[:sync_license_request][:sync_license_options_attributes] = sync_license_options_params
        end
      end[:sync_license_request]
  end

  def build_request
    if params[:request_upload].blank?

      # if user wants to use a previously entered production, make sure it's associated
      # to the current person and delete any form data which might contain new
      # sync license production information
      production = nil
      if params[:sync_license_request] and params[:sync_license_request][:sync_license_production_id].present?
        production = @person.sync_license_productions.find(params[:sync_license_request][:sync_license_production_id])
        params[:sync_license_request].delete(:sync_license_production_attributes)
      end

      @request_license = @person.sync_license_requests.build(sync_license_request_params)
    else
      @request_license = @person.sync_license_requests.build(request_document: params[:request_upload])
    end

    @request_license.song = @song
    @request_license.sync_license_options.each { |option| option.sync_license_request = @request_license }
    @request_license.sync_license_production.person = @person if @request_license.sync_license_production
    @request_license.sync_license_production = production if production
  end

  #
  # Find the song that the user is attempting to place
  # a sync license request in for
  #
  def find_song
    if params[:song_id]
      begin
        @song = Song.find(params[:song_id])
        @muma_song = @song.muma_songs.first

        if @song and @muma_song
          true
        else
          song_not_avail
        end
      rescue
        song_not_avail
      end

    else
      flash[:message] = "Please select song to request a license for"
      redirect_to sync_search_path
    end
  end

  def song_not_avail
    flash[:message] = "Requested song is not available"
    redirect_to sync_search_path
  end

  def notify_request_created(person, license_request, song, muma_song)
    AdminNotifier.sync_license_requested(person, license_request, song, muma_song, request.host).deliver
  end

  def find_downloadable_request
    @request_license = @person.sync_license_requests.find(params[:id])

    return if @request_license.downloadable?

    flash[:message] = "Unable to download specified license request"
    redirect_to sync_license_requests_path
  end
end
