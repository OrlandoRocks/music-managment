class Sync::MembershipsController < Sync::SyncController
  layout "sync"

  before_action :login_required, except: [:new, :create]
  before_action :login_disallowed, only: [:new, :create]
  skip_before_action :require_sync_feature

  def new
    @sync_membership_request = SyncMembershipRequest.new
  end

  def create
    @sync_membership_request = SyncMembershipRequest.new(sync_membership_request_params)
    @current_sync_membership_request = SyncMembershipRequest.where(email: @sync_membership_request.email).first
    @person = Person.where(email: @sync_membership_request.email).first

    existing_request  = (@current_sync_membership_request.present? || @person.present?)
    new_request_saved = false
    new_request_saved = @sync_membership_request.save unless existing_request

    AdminNotifier.sync_membership_requested(@sync_membership_request, request.host).deliver if new_request_saved

    if existing_request || new_request_saved
      flash[:notice] =
        "Your request has been sent. We'll get back to you as fast as we can, usually within 2-3 business days."
      redirect_to sync_login_path
    else
      render :new
    end
  end

  private

  def sync_membership_request_params
    params
      .require(:sync_membership_request)
      .permit(
        :email,
        :company,
        :country,
        :name,
        :phone_number,
        :website
      )
  end
end
