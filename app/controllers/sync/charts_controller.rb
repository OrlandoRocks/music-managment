class Sync::ChartsController < Sync::SyncController
  layout "sync"

  before_action :require_sync_feature

  def all_time
    chart_by_timeframe({ genre_filter: params[:genre], timeframe: "all_time", limit: 200 })
  end

  def week
    chart_by_timeframe({ genre_filter: params[:genre], timeframe: "seven_day", limit: 100 })
  end

  def chart_by_timeframe(options = {})
    timeframe = options[:timeframe]
    limit = options[:limit]

    # ids of songs returned from cloud search
    @chart_genres = ["Rock", "Alternative", "Hip Hop/Rap", "Pop", "Electronic", "Instrumental", "Singer/Songwriter", "Dance", "R&B/Soul", "Jazz", "Country", "Blues", "World", "Folk"]
    ids, sales = []

    if @chart_genres.include? params[:genre]
      cloudsearch = CloudSearch::Query.new(genre_filter: params[:genre], timeframe: timeframe.to_s, limit: limit.to_s)
      @chart_genre = params[:genre]
    else
      cloudsearch = CloudSearch::Query.new(timeframe: timeframe.to_s, limit: limit.to_s)
      @chart_genre = "All Genres"
    end
    parsed_json = cloudsearch.get_chart

    # Remove 's' from cloudsearch id's
    ids = parsed_json["hits"]["hit"].collect { |hash| hash["id"].slice(1..-1).to_i }
    sales = parsed_json["hits"]["hit"].collect { |hash| hash["data"][timeframe.to_s][0].to_i }

    @songs_hash = {}
    current_user.sync_favorite_songs.each { |song| @songs_hash[song.song_id] = true }

    @songs = Song.select("songs.*, muma_songs.mech_collect_share as mech_collect_share").where("songs.id IN (?)", ids).order("FIELD(songs.id,?)", ids.join(",")).includes({ creatives: :artist }, { muma_songs: [{ composition: :publishing_splits }, :composers] }, { album: [:artwork, { creatives: :artist }, :primary_genre] }).joins("INNER JOIN muma_songs ON muma_songs.composition_id = songs.composition_id")

    index = 0
    @songs.each do |song|
      song[:is_favorite]      = @songs_hash[song.id] ? "1" : "0"
      song[:sales]            = sales[index];
      index += 1;
    end

    respond_to do |format|
      format.html { render template: "sync/charts/chart" }
    end
  end
end
