class Sync::Admin::LicenseRequestsController < Sync::Admin::SyncController
  def index
    params[:page]     ||= 1
    params[:per_page] ||= 10

    @sync_requests = SyncLicenseRequest.order("created_at DESC")
                                       .paginate(page: params[:page], per_page: params[:per_page])

    @sync_requests.each do |sync_request|
      sync_request[:composer_names] = []

      # Use composers from the muma song first
      if sync_request.song.muma_songs.present? && sync_request.song.muma_songs.first.composers.present?
        sync_request[:composer_names] = sync_request.song.muma_songs.first.composers.collect { |composer| "#{composer.composer_first_name} #{composer.composer_last_name}" }
      end

      # Fall back on composer from TC composition
      if sync_request[:composer_names].blank? and sync_request.song.composition && sync_request.song.composition.publishing_splits.present?
        sync_request[:composer_names] = sync_request.song.composition.publishing_splits.map { |split| "#{split.composer.first_name} #{split.composer.last_name}" }
      end
    end

    respond_to do |format|
      format.html
      format.js {
        render partial: "licenses", content_type: "text/html", object: @sync_requests
      }
    end
  end

  def show
    @sync_license = SyncLicenseRequest.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def change_status
    @sync_license = SyncLicenseRequest.find(params[:id])
    @sync_license.status = params[:sync_license_request][:status]

    if @sync_license.save
      SyncNotifier.license(@sync_license).deliver if @sync_license.status == SyncLicenseRequest::STATUSES[:licensed]

      respond_to do |format|
        if !request.xhr?
          format.html {
            redirect_to sync_admin_license_request_path(@sync_license)
          }
        else
          format.js {
            render partial: "shared/message", locals: { text: "Successfully updated the license request" }, status: :ok, content_type: "text/html"
          }
        end
      end
    else
      respond_to do |format|
        if !request.xhr?
          format.html {
            render action: :show
          }
        else
          format.js {
            render partial: "shared/message", locals: { text: "Could not update the license request" }, status: :internal_server_error, content_type: "text/html"
          }
        end
      end
    end
  end
end
