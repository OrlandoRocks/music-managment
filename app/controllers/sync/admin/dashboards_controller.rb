class Sync::Admin::DashboardsController < Sync::Admin::SyncController
  def show
    @number_sync_users = Person.joins(:feature_people).where(
      "features_people.feature_id = ?",
      sync_licensing_feature.id
    ).count

    @new_membership_requests  = SyncMembershipRequest.where(status: "pending").count
    @approved_requests        = SyncMembershipRequest.where(status: "approved").count
    @declined_requests        = SyncMembershipRequest.where(status: "declined").count
    @total_songs              = CloudSearch::Batch.song_count

    last_batch_dates          = CloudSearch::Batch.last_batch_dates
    @last_cloud_search_date   = last_batch_dates[:last_add_date] if last_batch_dates

    @new_sync_requests        = SyncLicenseRequest.where(status: "new").count
    @quote_sent_sync_requests = SyncLicenseRequest.where(status: "quote sent").count
    @revised_sync_requests    = SyncLicenseRequest.where(status: "revised").count
    @licensed_sync_requests   = SyncLicenseRequest.where(status: "licensed").count
    @expired_sync_requests    = SyncLicenseRequest.where(status: "expired").count

    respond_to do |format|
      format.html
    end
  end
end
