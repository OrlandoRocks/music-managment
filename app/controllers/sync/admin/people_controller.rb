class Sync::Admin::PeopleController < Sync::Admin::SyncController
  def index
    @music_supervisors = Person.where("pr.role_id is null")
                               .joins("inner join features_people fp on fp.person_id = people.id and fp.feature_id = #{sync_licensing_feature.id}
                                       left outer join people_roles pr on pr.person_id = people.id and pr.role_id = #{Role.where(name: 'Admin').first.id}")
                               .paginate(page: params[:page], per_page: params[:per_page])

    respond_to do |format|
      format.html
      format.js {
        render partial: "people", object: @music_supervisors, content_type: "text/html"
      }
    end
  end
end
