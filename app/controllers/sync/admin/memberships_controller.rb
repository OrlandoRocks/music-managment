class Sync::Admin::MembershipsController < Sync::Admin::SyncController
  # Shows pending memberships to approve/disapprove
  def index
    params[:page]     ||= 1
    params[:per_page] ||= 10

    @memberships = SyncMembershipRequest.order("created_at DESC")
                                        .paginate(page: params[:page], per_page: params[:per_page])

    respond_to do |format|
      format.html
      format.js {
        render partial: "memberships", object: @memberships, content_type: "text/html"
      }
    end
  end

  def approve
    @sync_request = SyncMembershipRequest.find(params[:id])

    if @sync_request && @sync_request.approve
      PersonNotifier.sync_approval(@sync_request, COUNTRY_URLS["US"]).deliver

      respond_to do |format|
        format.html {
          flash[:message] = "#{@sync_request.email} has been approved"
          redirect_to sync_admin_memberships_path
        }
      end
    else
      respond_to do |format|
        format.html {
          redirect_to sync_admin_memberships_path
        }
      end
    end
  end

  def decline
    @sync_request = SyncMembershipRequest.find(params[:id])

    if @sync_request && @sync_request.decline

      respond_to do |format|
        format.html {
          flash[:message] = "#{@sync_request.email} has been declined"
          redirect_to sync_admin_memberships_path
        }
      end
    else
      respond_to do |format|
        format.html {
          redirect_to sync_admin_memberships_path
        }
      end
    end
  end

  def resend
    @sync_request = SyncMembershipRequest.find(params[:id])

    if @sync_request.approved?
      PersonNotifier.sync_register_reminder(@sync_request, COUNTRY_URLS["US"]).deliver

      respond_to do |format|
        format.html {
          flash[:message] = "#{@sync_request.email} has been sent another approval email"
          redirect_to sync_admin_memberships_path
        }
      end
    else
      respond_to do |format|
        format.html {
          redirect_to sync_admin_memberships_path
        }
      end
    end
  end
end
