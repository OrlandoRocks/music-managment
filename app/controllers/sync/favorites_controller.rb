class Sync::FavoritesController < Sync::SyncController
  def index
    params[:page] ||= 1
    params[:per_page] ||= 10
    @favorite_songs = current_user.sync_favorite_songs.includes(song: { album: :primary_genre })
                                  .paginate(page: params[:page], per_page: params[:per_page])

    # get bigbox information for each song

    @favorite_songs.each do |favorite_song|
      attributes = favorite_song.song.big_box_meta_data
      favorite_song[:duration]    = (attributes && attributes["duration"]) ? attributes["duration"].first.to_i : nil
      favorite_song[:stream_url]  = (favorite_song.song.s3_asset || favorite_song.song.s3_orig_asset) ? favorite_song.song.streaming_url : nil
      favorite_song[:artwork_url] = (favorite_song.song.album.artwork && favorite_song.song.album.artwork.uploaded?) ? sync_artwork_url(favorite_song.song.album.artwork.url(:small)) : ""

      favorite_song[:composer_names] = []

      # Use composers from the muma song first
      if favorite_song.song.muma_songs.present? && favorite_song.song.muma_songs.first.composers.present?
        favorite_song[:composer_names] = favorite_song.song.muma_songs.first.composers.collect { |composer| "#{composer.composer_first_name} #{composer.composer_last_name}" }
      end

      # Fall back on composer from TC composition
      if favorite_song[:composer_names].blank? and favorite_song.song.composition && favorite_song.song.composition.publishing_splits.present?
        favorite_song[:composer_names] = favorite_song.song.composition.publishing_splits.map { |split| "#{split.composer.first_name} #{split.composer.last_name}" }
      end

      favorite_song[:label_copy] = favorite_song.song.muma_songs.blank? ? "" : favorite_song.song.muma_songs.first.label_copy
    end

    respond_to do |format|
      format.html
      format.js {
        render partial: "favorites", content_type: "text/html", object: @favorite_songs
      }
    end
  end

  def toggle_favorite
    @favorite_song = current_user.sync_favorite_songs.where(song_id: params[:song_id]).first
    success        = false
    favorite       = false

    if @favorite_song.blank?
      @favorite_song = current_user.sync_favorite_songs.build(song_id: params[:song_id])
      success  = @favorite_song.save
      favorite = true
    else
      success = @favorite_song.destroy
    end

    if success
      respond_to do |format|
        format.js { render json: { "favorite" => favorite }.to_json, status: :ok }
      end
    else
      respond_to do |format|
        format.js { head :internal_server_error }
      end
    end
  end
end
