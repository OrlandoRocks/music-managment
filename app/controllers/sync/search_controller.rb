class Sync::SearchController < Sync::SyncController
  layout "sync"

  before_action :require_sync_feature

  # list of facets we are interested in for search results
  FACETS = %w[genre percent artist].freeze

  def show
    @search_term  = params[:q]
    @search_field = params[:q_field]
    @total_songs  = CloudSearch::Batch.song_count
    @genres = Genre.all
  end

  def create
  end

  # searches the cloud search server
  def search
    if params[:q].blank?
      ret_data = {}
    else
      params[:pg] ||= "1"
      params[:ps] ||= "10"

      facets = params[:facets] ? JSON.parse(params[:facets]) : {}

      # determine which page we are using for pagination
      start = (params[:pg].to_i - 1) * params[:ps].to_i

      cloudsearch = CloudSearch::Query.new(
        search_start: start,
        search_term: params[:q],
        search_field: params[:q_field],
        search_facets: FACETS,
        filter_facets: facets
      )
      parsed_json = cloudsearch.search

      # Remove 's' from cloudsearch id's
      ids = parsed_json["hits"]["hit"].collect { |hash| hash["id"].slice(1..-1).to_i }

      @songs_hash = {}
      current_user.sync_favorite_songs.each { |song| @songs_hash[song.song_id] = true }

      if facets["percent"].blank?
        conditions = ["songs.id IN (?)", ids]
        joins = "INNER JOIN muma_songs ON muma_songs.composition_id = songs.composition_id"
      else
        conditions = ["songs.id IN (?) AND albums.takedown_at IS NULL", ids]
        joins = "INNER JOIN muma_songs ON muma_songs.composition_id = songs.composition_id " \
                "INNER JOIN albums ON albums.id = songs.album_id"
      end

      @songs = Song
               .select("songs.*, muma_songs.mech_collect_share as mech_collect_share")
               .joins(joins)
               .includes(album: :primary_genre)
               .where(conditions)

      serialized_songs =
        @songs.map do |song|
          Sync::Search::SongSerializer.new(song, scope: self)
        end

      ret_data = { songs: serialized_songs, query: parsed_json, page: params[:pg] }
    end

    render json: ret_data
  end
end
