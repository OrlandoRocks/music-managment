class PayoneerFeesController < ApplicationController
  include PayoutProvidable
  include PayoneerAchFeeInfo
  include PayoutHelper

  before_action :redirect_to_status!, if: proc { current_user.payout_provider.present? && !current_user.payout_provider.needs_resubmission? }

  layout "tc-foundation"

  def index
    @payout_provider = current_user.payout_provider
    load_payoneer_ach_fee_info
  end
end
