class Admin::ContentReviewController < Admin::AdminController
  include ApplicationHelper
  require "oj"

  layout "admin"

  skip_before_action :verify_authenticity_token, raise: false
  before_action :authorization_by_roles_required
  before_action :content_review_manager_required, only: :list
  after_action  :invalidate_album, only: [:update, :update_album]

  def list
    params[:limit]            = 10                  if params[:limit].blank?
    params[:review_state]     = "ALL"               if params[:review_state].blank?
    params[:start_time]       = 1.day.ago.localtime if params[:start_time].blank?
    params[:end_time]         = Time.now            if params[:end_time].blank?
    params[:date_filter_type] = "paid"              if params[:date_filter_type].blank?
    params[:language_code]    = "ALL"               if params[:language_code].blank?

    album_models = Album.content_review_list_query(
      parameters_for_album_select,
      parameters_for_eager_loading,
      limit: 100,
      review_state: params[:review_state],
      start_time: params[:start_time],
      end_time: params[:end_time],
      date_filter_type: params[:date_filter_type],
      language_code: params[:language_code]
    )

    album_list = generate_album_list(album_models)
    render json: Oj.dump(
      {
        code: 0,
        response: { albums: album_list, languages: language_list }
      },
      mode: :compat
    )
  end

  def show
    if params[:ids]
      albums = Album.select(parameters_for_album_select).includes(parameters_for_eager_loading).find(params[:ids])
      albums.each { |album| album.start_review(person_id: current_user.id) }
    else
      album = current_user.pop_album_for_review
      album.start_review(person_id: current_user.id) unless album.nil?
      albums = album.nil? ? [] : [album]
    end

    album_list = generate_album_list(albums)
    render json: { code: 0, response: { albums: album_list, total_releases: { needs_review: current_user.total_albums_left_to_review_for_agent }, genres: genre_list, languages: language_list } }
  end

  def song_metadata
    song = Song.find(params[:id])
    song_metadata = song.metadata_for_review
    render json: { code: 0, response: { metadata: song_metadata } }
  end

  def reasons
    @reasons = current_user.permissable_content_review_reasons
    render json: { code: 0, response: { reasons: @reasons.collect { |r| { "id" => r.id, "reason" => r.reason, "type" => r.reason_type } } } }
  end

  def update
    if params[:review_audit]
      @review_audit = ReviewAudit.new(update_params.merge(person: current_user))

      if @review_audit.save
        render json: { code: 0, response: { messages: ["Audit record was successfully created."] } }
      else
        render json: { code: 1, response: { messages: @review_audit.errors.full_messages.each { |msg| msg } } }
      end
    else
      render json: { code: 1, response: { messages: ["no review_audit parameter"] } }
    end
  end

  def update_album
    album_params = params["album_update"]
    album = Album.find(album_params[:id])
    has_updated = album.update(
      primary_genre_id: album_params[:primary_genre_id],
      language_code: album_params[:language_code],
      name: album_params[:name],
      label_name: album_params[:label_name],
      secondary_genre_id: album_params[:secondary_genre_id]
    )
    album_params.each do |k, v|
      next unless k.include?("song")

      song_id, song_attribute = k.split("-")[1..2]
      song = album.songs.find(song_id)
      if song_attribute == "lyrics"
        song.lyric.update(content: v)
      else
        has_updated = album.songs.find(song_id).update({ song_attribute.to_s => v })
      end
    end

    if has_updated
      render json: { code: 0, response: { messages: ["update succeeded"] } }
    else
      render json: { code: 1, response: { messages: ["update failed"] } }
    end
  end

  def change_album_format_flag
    album = Album.find(params[:album_id])

    if album.flip_format_flag
      if album.has_non_formattable_language_code?
        message = "update not allowed because of language code: #{album.language_code}"
        code = 1
      else
        message = "update succeeded"
        code = 0
      end
      render json: { code: code, response: { messages: [message], auto_format_on: album.allow_different_format } }
    else
      render json: { code: 1, response: { messages: ["update failed"] } }
    end
  end

  private

  def update_params
    params
      .require(:review_audit)
      .permit(:album_id, :email_note, :event, :note, :person_id, review_reason_ids: [])
  end

  def generate_album_list(albums)
    album_list = []
    albums.each do |album|
      begin
        album_list << Tunecore::Crt.fetch_album_from_cache(album)
      rescue => e
        Rails.logger.error("state=error_generating_album_list album_id=#{album.id} error_message=#{e.message}")
        Airbrake.notify("CRT: Unable to load album - album_id=#{album.id} error_message=#{e.message}")
      end
    end
    album_list
  end

  def content_review_manager_required
    return if current_user.has_role?("Content Review Manager")

    render json: { code: 1, response: { messages: ["permission denied"] } }
  end

  def authorization_by_roles_required
    client_application = ClientApplication.where("name = ?", "content_review").includes(:roles).first
    return if current_user.roles.any? { |r| client_application.roles.include?(r) }

    render json: { code: 1, response: { messages: ["permission denied"] } }
  end

  def parameters_for_eager_loading
    [
      :person,
      :label,
      { songs: [{ creatives: :artist }, :s3_asset] },
      { creatives: :artist },
      { review_audits: [:review_reasons, :person] },
      :tunecore_upcs,
      :artwork,
      { salepoints: :store },
      :primary_genre,
      :secondary_genre,
      :album_countries
    ]
  end

  def parameters_for_album_select
    "albums.*, (SELECT count(*) FROM `albums` as a WHERE (`a`.person_id = albums.person_id)) as total_albums"
  end

  def genre_list
    Genre.all.map { |g| { g.id => g.name } }
  end

  def invalidate_album
    album_id = params[:album_update] ? params[:album_update][:id] : update_params[:album_id]
    return unless album_id && FeatureFlipper.show_feature?(:crt_cache, nil)

    album = Album.find(album_id)
    Tunecore::Crt.invalidate_album_in_cache(album)
  end
end
