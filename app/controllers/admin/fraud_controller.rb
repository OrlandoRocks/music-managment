class Admin::FraudController < Admin::AdminController
  include PopCrit

  def index
    @page_title = "Suspicious or Locked Accounts"
    if params[:d]
      process_conditions
    else
      @conditions = ["status_updated_at >= ? AND status IN ('Locked', 'Suspicious')", Date.today]
    end

    @people          = fetch_people.paginate(page: params[:page], per_page: 25)
    @user_can_unlock = current_user.is_permitted_to?("admin/people", "unlock_account")
  end

  def statistics
    @suspicious_accounts_total = Person.where("status = 'Suspicious'").count
    @active_accounts_total = Person.where("status = 'Active'").count
    @total_accounts = Person.count
    @locked_accounts_total = Person.where("status = 'Locked' or status IS NULL").count
    @suspicious_this_month = Person.where(
      "status = 'Suspicious' and (status_updated_at >= ? and status_updated_at <= ?)", Date.today.at_beginning_of_month, Date.today.at_end_of_month
    ).count
    @locked_this_month = Person.where(
      "status = 'Locked' and (status_updated_at >= ? and status_updated_at <= ?)", Date.today.at_beginning_of_month, Date.today.at_end_of_month
    ).count
    render layout: "admin_application_lightbox"
  end

  private

  def fetch_people
    return people_query.to_a if params[:pub_admin] == "All"

    if params[:pub_admin] == "Yes"
      people_query.to_a.select { |p| p.paid_for_composer? || p.paid_for_publishing_composer? }
    else
      people_query.to_a.select { |p| !(p.paid_for_composer? || p.paid_for_publishing_composer?) }
    end
  end

  def people_query
    @people_query ||=
      Person.select("id, name, email, status, status_updated_at, country_website_id, lock_reason").where(@conditions).order("status_updated_at DESC")
  end

  def process_conditions
    process_common_fields
    override_options = {
      from_date: { field_name: "status_updated_at", operator: ">=", value: @from_date_as_date },
      to_date: { field_name: "status_updated_at", operator: "<=", value: @to_date_as_date }
    }.merge(@p_override)
    override_options = override_options.merge(@status_override) if @status_override
    @conditions = process_criteria_params(params[:d], { skip_value: "All", override_fields: override_options })
  end

  def process_common_fields
    process_dates
    # set the person search field based on @ symbol matching
    @p_override =
      if params[:d][:person_search] != nil && params[:d][:person_search].match(/.*(@).*/)
        { person_search: { field_name: "email" } }
      else
        { person_search: { field_name: "name", operator: "like", value_ends_with: "%" } }
      end

    return unless params[:d][:status] != nil && params[:d][:status] == "Both Locked and Suspicious"

    @status_override = { status: { value: ["Locked", "Suspicious"] } }
  end

  def process_dates
    @from_date_as_date = params[:d][:from_date].blank? ? Date.today : DateParser.parse(params[:d][:from_date])
    @to_date_as_date = params[:d][:to_date].blank? ? Date.today : DateParser.parse(params[:d][:to_date])
  end
end
