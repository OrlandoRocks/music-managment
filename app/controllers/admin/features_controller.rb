require "./lib/slack_notifier"

class Admin::FeaturesController < Admin::AdminController
  before_action :feature_role_required

  SLACK_NOTIFICATION_TYPE = "FeatureFlipper"
  def index
    @features = Feature.unlock_features
    @rollouts = FeatureFlipper.all_features
    @country_website_rollouts =
      CountryWebsite.all.map do |cw_object|
        {
          country_code: cw_object.country,
          country: cw_object.name,
          features: FeatureFlipper.all_features(cw_object)
        }
      end
  end

  def create
    country_website = CountryWebsite.find_by(country: params[:country_iso_code]) if params[:country_iso_code].present?
    FeatureFlipper.add_feature(params["new_feature"], country_website)
    notify_in_slack(creation_slack_message, SLACK_NOTIFICATION_TYPE, TECH_DEPLOY_CHANNEL_NAME)
    redirect_back fallback_location: home_path
  end

  def update
    country_website = CountryWebsite.find_by(country: params[:country_iso_code]) if params[:country_iso_code].present?
    FeatureFlipper.update_feature(
      params["feature_name"].to_sym,
      params["percentage"],
      params["users"],
      country_website
    )
    if Integer(params[:percentage], 10).zero?
      airbrake_response = {
        admin: current_user.name,
        admin_id: current_user.id,
        feature_name: params[:feature_name]
      }
      Airbrake.notify("Feature Flag set to 0 : #{airbrake_response}")
    end
    notify_in_slack(updation_slack_message, SLACK_NOTIFICATION_TYPE, TECH_DEPLOY_CHANNEL_NAME)
    redirect_back fallback_location: home_path
  end

  def remove
    country_website = CountryWebsite.find_by(country: params[:country_iso_code]) if params[:country_iso_code].present?
    if valid_ticket?(params[:ticket_link])
      FeatureFlipper.remove_feature(
        params[:feature_name],
        country_website
      )
      airbrake_response = {
        admin: current_user.name,
        admin_id: current_user.id,
        feature_name: params[:feature_name],
        ticket_link: params[:ticket_link]
      }
      Airbrake.notify("Feature Flag removed : #{airbrake_response}")
      notify_in_slack(deletion_slack_message, SLACK_NOTIFICATION_TYPE, TECH_DEPLOY_CHANNEL_NAME)
    else
      flash[:error] =
        "Please enter Jira ticket created for code deletion or 'mistake' if feature flag created by mistake"
    end
    redirect_back fallback_location: home_path
  end

  private

  def creation_slack_message
    "#{current_user.name} created a FeatureFlipper '#{params['new_feature']}' under "\
    "#{params[:country_iso_code].presence || 'Global'} Application features"
  end

  def updation_slack_message
    msg = "#{current_user.name} updated the FeatureFlipper '#{params['feature_name']}'"\
    " under #{params[:country_iso_code].presence || 'Global'} Application features"\
    " with Percentage - #{params['percentage']}"
    msg += " and User(s) - #{params['users']}" if params["users"].present?
    msg
  end

  def deletion_slack_message
    "#{current_user.name} removed the FeatureFlipper '#{params['feature_name']}'"\
    " under #{params[:country_iso_code].presence || 'Global'} Application features"
  end

  def feature_role_required
    role = current_user.roles.where("name =?", "Feature Manager").first
    raise ActiveRecord::RecordNotFound if role.nil?
  end

  def valid_ticket?(ticket_link)
    valid_ticket = (/\Ahttps?:\/\/tunecore.atlassian.net\/browse\/\S+\z/).match?(ticket_link)
    valid_ticket || ticket_link == "mistake"
  end
end
