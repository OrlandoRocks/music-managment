class Admin::EftQueriesController < Admin::AdminController
  before_action :load_person

  def index
    @per_page = 10
    @eft_queries = EftQuery.includes(:eft_batch_transactions).order("created_at DESC")
                           .paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
    @eft_summary = EftBatchTransaction.summary_grouped_by_status
  end

  def show
    @has_cancelable_transaction = false
    @person_page = false
    @per_page = 20
    @eft_query = EftQuery.includes(:eft_batch_transactions).find(params[:id])
    @eft_batch_transactions = @eft_query.eft_batch_transactions
                                        .includes(:payout_service_fee, stored_bank_account: :person)
                                        .paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
  end
end
