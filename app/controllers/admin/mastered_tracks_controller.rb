class Admin::MasteredTracksController < Admin::AdminController
  def index
    @page_title = "Mastered Tracks"
    @person = Person.find(params[:person_id])
    @order = params[:order]
    @mastered_tracks = @person.mastered_tracks.paid.select("*, case when refunded_at is not null then 2 when download_link is not null then 1 else 0 end status").order(@order)
  end

  def mark_as_refunded
    @mastered_track = MasteredTrack.find(params[:id])

    if @mastered_track.update(refunded_at: Time.now)
      flash[:success] = "Updated Successfully"
    else
      flash[:error] = "Update Failed"
    end

    redirect_to admin_person_mastered_tracks_path(params[:person_id])
  end

  def unmark_refund
    @mastered_track = MasteredTrack.find(params[:id])

    if @mastered_track.update(refunded_at: nil)
      flash[:success] = "Updated Successfully"
    else
      flash[:error] = "Update Failed"
    end

    redirect_to admin_person_mastered_tracks_path(params[:person_id])
  end
end
