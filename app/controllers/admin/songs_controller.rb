class Admin::SongsController < Admin::AdminController
  layout "tc_foundation_admin"

  def play_song
    @song = Song.find_by(id: params[:song_id])
    @song_url = @song&.s3_url(300) || @song&.streaming_url
    render json: { song_url: @song_url }
  end
end
