class Admin::VideosController < Admin::AdminController
  before_action :load_person
  before_action :load_editable_video, except: :search

  def show
  end

  def search
    @page_title = "Video Search Results"
    # raise "controller stop"
    if params[:term] =~ /^\s*(id|bundle|state|album_state|upc|isrc|name|label|artist|director|all):\s*(.*)$/
      @search_field = $1
      @search_text = $2
    else
      @search_text = params[:term]
      @search_field =
        if @search_text.match?(/^\s*\d+\s*$/)
          "id"
        else
          "all"
        end
    end

    # artist_hack = 'creatives.id IS NOT NULL AND '
    add_include = []
    case @search_field

    when "id"
      conditions = ["videos.id = ?", @search_text]
      order = "videos.updated_at ASC"
    when "upc"
      term = "%#{@search_text}%"
      conditions = ["upcs.number like ?", term]
      order = "upcs.number"
    when "isrc"
      term = "%#{@search_text}%"
      conditions = ["videos.tunecore_isrc like ? or videos.optional_isrc like ?", term, term]
      order = "videos.tunecore_isrc ASC, videos.optional_isrc ASC"
    when "name"
      conditions = ["videos.name like ?", "%#{@search_text}%"]
      order = "videos.name ASC"
    when "artist"
      conditions = ["creatives.id IS NOT NULL AND artists.name like _utf8 ? COLLATE utf8_general_ci", "%#{@search_text}%"]
      order = "artists.name ASC"
      add_include = [:creatives, :artists]
    when "director"
      conditions = ["videos.director like _utf8 ? COLLATE utf8_general_ci", "%#{@search_text}%"]
      order = "videos.name ASC"
    # when 'genre'
    #  conditions = ['videos.primname like ?', "%#{@search_text}%"]

    when "all"
      term = "%#{@search_text}%"
      conditions = [
        "albums.name like ? or labels.name like ? or artists.name like ? or upcs.number like ? or songs.tunecore_isrc like ? or songs.optional_isrc like ?",
        term,
        term,
        term,
        term,
        term,
        term
      ] # ack!
      order = "albums.id ASC"
    end
    logger.error "searching for bundle #{@search_text}"
    @search_items = Video.includes(add_include + [:person, :upcs])
                         .where(conditions)
                         .order(order)
                         .paginate(page: params[:page], per_page: (params[:per_page] || 10).to_i)
  end

  def metadata
    @page_title = "Video Metadata"
    @new_css = true
    @video = Video.find(params[:id])
    return unless params[:metadata_sent]

    @video.metadata_picked_up = true
    @video.save!
  end

  private

  def load_editable_video
    @video = Video.find(params[:id])
  end
end
