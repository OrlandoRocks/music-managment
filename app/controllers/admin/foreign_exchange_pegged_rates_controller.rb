class Admin::ForeignExchangePeggedRatesController < Admin::AdminController
  before_action :load_current_pegged_rate, only: [:index, :new, :preview_price]
  before_action :load_rights, only: [:index, :new, :create, :preview_price]

  def index
    @pegged_currencies = ForeignExchangePeggedRate.pegged_currencies
  end

  def new
    @new_pegged_rate = ForeignExchangePeggedRate.new
    render layout: "admin_application_lightbox"
  end

  def create
    @new_pegged_rate = ForeignExchangePeggedRate.create(foreign_exchange_pegged_rate_params.merge(updated_admin: @current_user))
    if @new_pegged_rate.errors.present?
      flash[:alert] = "Error: #{@new_pegged_rate.errors.full_messages.join(', ')}"
    else
      flash[:info] = " Pegged Rate Updated Successfully! "
    end
    redirect_to new_admin_foreign_exchange_pegged_rate_path(params: { currency: @new_pegged_rate.currency })
  end

  private

  def load_rights
    has_page_rights = @current_user.roles.joins(:rights).where(rights: { controller: params[:controller], action: params[:action] }).present?
    return if has_page_rights

    flash[:error] = "You do not have Finance Admin rights to view the page"
    redirect_to admin_dashboard_index_path
  end

  def foreign_exchange_pegged_rate_params
    params.require(:foreign_exchange_pegged_rate).permit(:currency, :pegged_rate, :note, :country_id, :country_website_id)
  end

  def load_current_pegged_rate
    return if params[:currency].blank?

    @selected_currency_pegged_rates = ForeignExchangePeggedRate.for_currency(params[:currency])
    @selected_currency_current_pegged_rate = @selected_currency_pegged_rates.first
  end
end
