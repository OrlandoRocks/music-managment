class Admin::AlbumPurchasesController < Admin::AdminController
  before_action :load_person
  before_action :load_album
  before_action :load_purchase, except: [:index]

  def index
    @purchases = Purchase.where("(related_type = 'Album' AND related_id = ?) OR (related_type = 'Salepoint' AND related_id IN (?))", @album.id, @album.salepoint_ids).all
  end

  def show
  end

  def new_cert
  end

  def destroy
    if (@purchase.destroy rescue false)
      flash[:notice] = "Destroyed purchase #{@purchase.id}"
      redirect_to admin_person_album_purchases_path(@person, @album)
    else
      flash[:error] = "Unable to destroy purchase #{@purchase.id}"
      redirect_to admin_person_album_purchase_path(@person, @album, @purchase)
    end
  end

  def create_cert
    @cert_code = params[:cert_code]
    opts = {
      entered_code: @cert_code,
      purchase: @purchase,
      admin: nil
    }
    opts[:admin] = current_user if current_user.is_administrator
    verify_result = Cert.verify(opts)
    if verify_result.is_a?(Cert)
      flash[:notice] = "Certificate applied to purchase of: #{@album.name}"
      redirect_to admin_person_album_purchase_path(@person, @album, @purchase) # , @purchase.person)
    else
      flash[:error] = verify_result
      render action: "new_cert"
    end
  end

  protected

  def load_person
    @person = Person.find(params[:person_id])
  end

  def load_purchase
    @purchase = Purchase.find(params[:id])
  end
end
