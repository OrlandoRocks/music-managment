class Admin::LegalDocumentsController < Admin::AdminController
  def show
    legal_document = LegalDocument.find(params[:id])

    if current_user.is_publishing_manager?
      redirect_to legal_document.document_url
    else
      head 404, "content_type" => "text/plain"
    end
  end
end
