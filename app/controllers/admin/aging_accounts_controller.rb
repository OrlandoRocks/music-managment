class Admin::AgingAccountsController < Admin::AdminController
  def index
    @page_title = "Aging Accounts"
    @reports = Tunecore::TcReporter::AgingAccounts.s3_links
  end

  def create
    if Tunecore::TcReporter::AgingAccounts.create_report(params, current_user)
      flash[:notice] = "Reporting kicked off for #{params[:date][:month]}-#{params[:date][:year]}"
    else
      flash[:error] = "TC REPORTER is currently down."
    end
    redirect_to admin_aging_accounts_path
  end
end
