class Admin::SalepointSubscriptionsController < Admin::AdminController
  before_action :load_album

  def toggle_active
    success = false
    sp_sub  = @album.salepoint_subscription

    # only makes sense to toggle automator subscription
    # status if the subscription has been paid for
    if sp_sub && sp_sub.finalized?
      sp_sub.is_active = !sp_sub.is_active
      success = sp_sub.save
    end

    if success
      # Add admin note marking the transition
      Note.create(
        related: @album,
        note_created_by: current_user,
        ip_address: request.remote_ip,
        subject: "Automator status toggled",
        note: "Automator active status was marked #{sp_sub.is_active}"
      )

      respond_to do |format|
        format.html { redirect_to admin_album_path(@album) }
        format.js   { render json: { active: sp_sub.active? }.to_json, status: :ok }
      end
    else
      respond_to do |format|
        format.html { redirect_to admin_album_path(@album) }
        format.js   { head :internal_server_error }
      end
    end
  end

  private

  def load_album
    @album = Album.find(params[:album_id])
  end
end
