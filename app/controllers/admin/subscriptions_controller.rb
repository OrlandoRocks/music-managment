class Admin::SubscriptionsController < Admin::AdminController
  def index
    @person        = Person.find(params[:person_id])
    @subscriptions = @person.person_subscription_statuses.eager_load(subscription_events: :subscription_purchase)
  end

  def update
    subscription_status = PersonSubscriptionStatus.find(params[:id])
    result = PersonSubscriptionStatusService.disable_access(subscription_status, current_user)
    flash[:error] = result.errors.full_messages.join(" & ") unless result.errors.empty?
    redirect_to admin_person_subscriptions_path(subscription_status.person)
  end
end
