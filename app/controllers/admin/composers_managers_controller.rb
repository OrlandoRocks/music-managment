class Admin::ComposersManagersController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :load_association_form, only: :update
  before_action :load_destruction_form, only: :destroy

  def show
    @page_title = "Manage Composers".freeze
    @account    = Account.find(params[:id])
  end

  def update
    if @association_form.save
      flash[:notice] = "Composer has been associated to TC User ID #{@association_form.person_id}"
    else
      flash[:error] = @association_form.errors[:person].first
    end

    redirect_to admin_composers_manager_path(params[:id])
  end

  def destroy
    if @destruction_form.save
      flash[:notice] = "Composer with ID of #{@destruction_form.composer.id} has been successfully deleted!"
    else
      flash[:error] =
        "Unable to destroy Composer due to the following errors: #{@destruction_form.errors.full_messages}."
    end

    redirect_to admin_composers_manager_path(params[:id])
  end

  private

  def load_association_form
    @association_form = PublishingAdministration::ComposerPersonAssociationForm.new(form_params)
  end

  def form_params
    {
      composer: Composer.find(params[:composer][:id]),
      person_id: params[:composer][:person_id]
    }
  end

  def load_destruction_form
    @destruction_form = ::PublishingAdministration::ComposerDestructionForm.new(destruction_params)
  end

  def destruction_params
    {
      composer: Composer.find_by(id: params[:composer][:id]),
      admin: current_user,
      ip_address: request.remote_ip
    }
  end
end
