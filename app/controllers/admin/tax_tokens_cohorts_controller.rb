class Admin::TaxTokensCohortsController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :set_tax_tokens_cohort_form_create, only: [:new, :create]
  before_action :set_tax_tokens_cohort_form_update, only: [:edit, :update]

  def index
    @tax_tokens_cohorts = TaxTokensCohort.active
  end

  def update
    if @tax_tokens_cohort_form.update(tax_tokens_cohort_params)
      redirect_to admin_tax_tokens_cohorts_path
    else
      render :edit,
             tax_tokens_cohort_form: @tax_tokens_cohort_form,
             notice: @tax_tokens_cohort_form.errors.full_messages
    end
  end

  def create
    if @tax_tokens_cohort_form.save
      redirect_to admin_tax_tokens_cohorts_path
    else
      render :new
    end
  end

  def remove_cohort
    cohort = TaxTokensCohort.find(params[:cohort_id])
    if cohort.remove_all_tax_tokens
      redirect_to admin_tax_tokens_cohorts_path, notice: "Successfully removed #{cohort.name}"
    else
      render :index, notice: "Something went wrong."
    end
  end

  private

  def set_tax_tokens_cohort_form_create
    @tax_tokens_cohort_form = TaxTokensCohortForm.new(tax_tokens_cohort_params)
  end

  def set_tax_tokens_cohort_form_update
    @tax_tokens_cohort_form = TaxTokensCohortForm.new(tax_tokens_cohort_params.merge(id: params[:id]))
  end

  def tax_tokens_cohort_params
    params[:tax_tokens_cohort_form].presence || params[:tax_tokens_cohort] || {}
  end
end
