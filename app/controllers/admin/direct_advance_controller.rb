class Admin::DirectAdvanceController < Admin::AdminController
  def index
    @report = DirectAdvance::ReportCollectionSearch.search(query) if query.present?
  end

  def update
    person = Person.find(params[:id])
    if person.update(override_advance_ineligibility: !person.override_advance_ineligibility)
      redirect_to admin_direct_advance_index_path(tcda_reports_search: { id_or_email: person.id })
    else
      redirect_to admin_direct_advance_index_path
    end
    # we have to redirect to index and query a new search because of how person is nested in @report
  end

  private

  def query
    params[:tcda_reports_search].try(:[], :id_or_email)
  end
end
