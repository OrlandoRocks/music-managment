class Admin::PreordersController < Admin::AdminController
  before_action :redirect_if_preorder_unpaid

  def edit
    @page_title = "Edit Preorder"
    @album = @preorder_purchase.album
    @itunes_preorder_data = @preorder_purchase.salepoint_preorder_data.by_store("iTunesWW").first
    @google_preorder_data = @preorder_purchase.salepoint_preorder_data.by_store("Google").first
    @preorder_data = @itunes_preorder_data || @google_preorder_data
    @grat_songs = @itunes_preorder_data ? @itunes_preorder_data.preorder_instant_grat_songs : []
    @data = {
      grat_songs_count: @grat_songs.count,
      songs_count: @album.songs.count,
      max_date: l((@album.sale_date - 1.day), format: :country_date),
      min_date: l((Date.today + 1.day), format: :country_date)
    }
  end

  def update
    begin
      @preorder_purchase.admin_update(all_params_hash, itunes_params_hash || {})
      preorder_data_to_update = @preorder_purchase.salepoint_preorder_data.select(&:is_enabled?)
      salepoints_to_update = preorder_data_to_update.collect(&:salepoint)
      PetriBundle.for(@preorder_purchase.album).add_salepoints(salepoints_to_update, actor: current_user.name, message: "Metadata update for admin preorder data update")
      Note.create(related: @album, note_created_by: current_user, ip_address: request.remote_ip, subject: "Preorder data change", note: "Pre-order data was updated by admin user.")
    rescue
      flash[:error] = "The update and metadata update failed"
    end

    flash[:success] = "Preorder was Updated and Sent Successfully" unless flash[:error]
    redirect_to edit_admin_preorder_url(@preorder_purchase)
  end

  private

  def redirect_if_preorder_unpaid
    @preorder_purchase = PreorderPurchase.find(params[:id])
    return if @preorder_purchase.paid_at?

    flash[:error] = "Preorder is not paid for and cannot be updated"
    redirect_to admin_home_url
  end

  def all_params_hash
    params[:all].try(:permit, [:start_date, :preview_songs])
  end

  def itunes_params_hash
    params[:itunes].try(
      :permit,
      [
        { instant_grat_songs: [] },
        :variable_price_id,
        :preorder_price_id,
        :track_variable_price_id
      ]
    )
  end
end
