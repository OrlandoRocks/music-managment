# 2009-11-30 -- ED -- added release login lock method
class Admin::PeopleController < Admin::AdminController
  include ApplicationHelper
  include IndiaPaymentsHelper
  include TransactionsListingHelper

  helper "admin/albums"
  helper "my_account"

  before_action :authorize_download_assets, only: [:download_assets, :create_recover_assets]
  before_action :load_person, only: [:transactions_for, :update_tax_tokens, :toggle_feature, :download_assets, :create_recover_assets, :block_withdrawals, :unblock_withdrawals]
  before_action :load_transaction_dates, only: [:transactions_for]

  def edit_profile
    @person = Person.find(params[:id])
    render layout: "admin_application_lightbox"
  end

  def update_profile
    return if params[:person].blank?

    @person = Person.find(params[:id])
    return if @person.blank?

    email_param = params.dig(:person, :email)
    changed_email = @person.email != email_param
    @person.email = email_param if email_param.present?

    @person.postal_code = params[:person][:postal_code]

    if @person.save
      handle_changed_email if changed_email
      render layout: "admin_application_lightbox"
    else
      render action: :edit_profile, layout: "admin_application_lightbox"
    end
  end

  def reset_pw_mail
    @person = Person.find(params[:id])

    if @person
      begin
        Person.transaction do
          PasswordResetService.reset(@person.email, :admin)
          flash[:notice] = "Instructions on resetting password have been emailed to #{@person.email}"
          Note.create(
            related: @person,
            note_created_by: current_user,
            ip_address: request.remote_ip,
            subject: "Password Reset Mail Sent",
            note: "A password reset mail was sent to #{@person.email}"
          )
          redirect_to(admin_person_path(@person)) && return
        end
      rescue
        flash[:error] = "Password could not be emailed to #{@person.email}"
        redirect_to(admin_person_path(@person)) && return
      end
    else
      flash[:error] = "Invalid person."
      redirect_to admin_person_path(@person)
    end
  end

  def reset_pw_show
    @person = Person.find(params[:id])

    if @person
      @password_reset_url = get_reset_password_link(@person)
      render template: "admin/people/reset_pw_show", layout: false
    else
      flash[:error] = "Invalid person."
      redirect_to admin_person_path(@person)
    end
  end

  def ambassador
    @person = Person.find(params[:id])
    campaign = FriendReferralCampaign.get_current_campaign_details(@person)
    ambassador_stats = FriendReferralCampaign.get_stats(@person)
    @friend_referral_share_url = campaign.campaign_url
    @friend_referral_referred_count = ambassador_stats.unique_referrals
    @friend_referral_dollars_earned = ambassador_stats.balance
  end

  def control_account
    if can_take_over_account?
      @person = Person.find(params[:id])
      add_sso_cookie(for_user_id: @person.id, by_user_id: current_user.id)

      self.current_admin = current_user
      self.current_user  = @person
      Note.create(related: @person, note_created_by: current_admin, ip_address: request.remote_ip, subject: "Taken Over", note: "User was taken over")
      add_takeover_cookie
    end

    if params[:album] && Album.find(params[:album])
      redirect_to controller: "/album", action: "show", id: params[:album]
    else
      redirect_to controller: "/dashboard"
    end
  end

  def show
    @person           = Person.find(params[:id])
    @products         = @person
                        .albums
                        .includes(:upcs, :artwork, :locked_by, creatives: :artist)
                        .group_by(&:album_type)
    @videos           = @person.videos
    @notes            = @person.all_notes
    @total_note_count = @notes.count
  end

  def set_song_name
    song = Song.find(params[:id])
    old_name = song.name
    song.name = params[:value]
    if song.save
      render(text: song.name)
    else
      render(text: old_name)
    end
  end

  def set_album_artist_name
    album = Album.find(params[:id])
    album.selected_artist_name = params[:value]

    if album.save
      render(text: album.artist.name)
    else
      album.reload
      render(text: album.artist_name)
    end
  end

  def set_album_label_name
    album = Album.find(params[:id])

    if params[:value].present?
      label = Label.find_or_create_by(name: params[:value])
      album.label_id = label.id if label.name.present?
      album.save
    end

    render(text: album.label_name)
  end

  def set_album_name
    album = Album.find(params[:id])
    old_name = album.name
    album.name = params[:value]
    if album.save
      render(text: album.name)
    else
      render(text: old_name)
    end
  end

  def transactions_for
    # we want to diplay 2 different types of transactions based on 1 selection
    if params[:transaction_values] && params[:transaction_values].include?("Invoice")
      params[:transaction_values][:BatchTransaction] = "BatchTransaction"
    end
    # Don't like this either, but leaving for now.
    @initial_load = (params.keys.size == 3) ? true : false
    @transactions = fetch_transactions(@person)

    if FeatureFlipper.show_feature?(:disable_sales, @person)
      @transactions_sales_records = []
    else
      @transactions_sales_records = PersonTransaction.with_sales_record_information(@person.id, @transactions).group_by(&:transaction_id)
    end
    @composer = @person.composers.first
    @has_never_distributed = @person.has_never_distributed?
    @transactions_split_records = PersonTransaction.with_splits_information(@person.id, @transactions).group_by(&:transaction_id)
  end

  def credit_card_transactions
    @user_can_refund = current_user.has_role?("Refunds") || current_user.has_role?("Issues Refunds Only")
    @person = Person.find(params[:id])
    @transactions = fetch_user_cc_transactions

    @payu_transactions = user_payu_transactions if one_time_payment_only?(@person)
  end

  def search
    if params[:term] =~ /^\s*(id|email|name|label|artist|rights_app_id|client_payee_id):\s*(.*)$/
      @search_field = $1
      @search_text = $2
    else
      @search_text = params[:term]
      @search_field = @search_text.match?(/^\s*\d+\s*$/) ? "id" : "all"
    end

    join_terms = ""
    select_terms = ""
    case @search_field
    when "id"
      conditions = ["people.id = ?", @search_text]
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      order = "people.id ASC"
    when "email"
      conditions = ["people.email like ?", @search_text.delete("%").sub("*", "%").to_s]
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      order = "people.email ASC"
    when "name"
      conditions = ["people.name like ?", @search_text.delete("%").sub("*", "%").to_s]
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      order = "people.name ASC"
    when "label"
      conditions = ["labels.name like ?", @search_text.delete("%").sub("*", "%").to_s]
      order = "labels.name, people.name ASC"
      join_terms = "INNER JOIN labels ON labels.id=people.label_id"
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, labels.name AS label_name"
    when "artist"
      conditions = ["artists.name like _utf8 ? COLLATE utf8_general_ci", @search_text.delete("%").sub("*", "%").to_s]
      join_terms = "INNER JOIN artists ON artists.id=people.artist_id"
      select_terms = "artists.name AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      order = "artists.name ASC"
    when "rights_app_id"
      conditions = ["accounts.provider_account_id = ?", @search_text]
      join_terms = "INNER JOIN accounts ON accounts.person_id = people.id"
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      order = "people.id ASC"
    when "client_payee_id"
      select_terms = "(SELECT NAME FROM artists WHERE id=people.artist_id) AS artist_name, (SELECT NAME FROM labels WHERE id=people.label_id) AS label_name"
      conditions = ["payout_providers.client_payee_id like ?", @search_text.delete("%").sub("*", "%").to_s]
      join_terms = "INNER JOIN payout_providers ON payout_providers.person_id = people.id"
    end

    @people = Person
              .select("people.*, (SELECT balance FROM person_balances WHERE person_id=people.id) AS balance_cents, #{select_terms}")
              .joins(join_terms)
              .where(conditions)
              .order(order)
              .paginate(page: params[:page])
  end

  # 2009-09-02 -- CH -- added method
  def change_status
    @person = Person.find(params[:id])
    @note = Note.new
    @is_suspicion = !params[:ns].nil?
    @is_deletion = !params[:nd].nil?
    render layout: "admin_application_lightbox"
  end

  # 2009-09-02 -- CH -- added method
  # 2009-10-6 -- CH -- changed method name from deactivate to lock_account
  def lock_account
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Locked"))
    if @note.valid?
      @person.lock!(params[:person][:lock_reason], !params[:takedown].nil?)
      flash[:notice] = "#{@person.name} was locked."
      render_changed_status
    else
      render_change_status
    end
  end

  # 2009-09-02 -- CH -- added method
  # 2009-10-6 -- CH -- changed method name from activate to unlock_account
  def unlock_account
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Unlocked"))
    if @note.valid?
      @person.activate!
      flash[:notice] = "#{@person.name} was unlocked."
      render_changed_status
    else
      render_change_status
    end
  end

  # 2009-09-03 -- CH -- added method
  def mark_as_suspicious
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Marked as Suspicious"))
    if @note.valid?
      @person.mark_as_suspicious!(@note, params[:person][:lock_reason])
      flash[:notice] = "#{@person.name} was marked as suspicious."
      render_changed_status
    else
      @is_suspicion = true
      render_change_status
    end
  end

  # 2009-09-03 -- CH -- added method
  def remove_suspicion
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Suspicion Removed"))
    if @note.valid?
      @person.remove_suspicion!
      flash[:notice] = "#{@person.name} is no longer suspected of fraud."
      render_changed_status
    else
      @is_suspicion = true
      render_change_status
    end
  end

  def mark_as_deleted
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Marked as Deleted"))
    if @note.valid?
      ActiveRecord::Base.transaction do
        @person.mark_as_deleted(@note, params[:person][:lock_reason])
        @person.payout_providers.each(&:disassociate_account) if @person.payout_providers.present?
      end
      if @person.errors.any?
        flash[:error] = @person.errors.full_messages.join(", ")
      else
        flash[:notice] = "#{@person.name} was marked as deleted."
      end
      render_changed_status
    else
      @is_deletion = true
      render_change_status
    end
  end

  def remove_deletion
    @person = Person.find(params[:id])
    @note = Note.create(note_params.merge(related: @person, ip_address: request.remote_ip, subject: "Deletion Removed"))
    if @note.valid?
      @person.remove_deletion!
      flash[:notice] = "#{@person.name} is no longer deleted."
      render_changed_status
    else
      @is_deletion = true
      render_change_status
    end
  end

  # 2009-11-30 -- ED -- see Ed if this doesn't make sense
  def release_login_lock
    @person = Person.find(params[:id])
    if @person.unlock_account
      flash[:notice] = "#{@person.name}'s account has been unlocked"
    else
      flash[:error] = "Couldn't unlock #{@person.name}'s account"
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  # 2010-06-07 -- RT -- added method
  def complete_verify
    @person = Person.find(params[:id].to_i)
    if @person.is_verified?
      flash[:error] = "#{@person.name}'s account already verified"
    else
      @person.is_verified = 1
      if @person.update_attribute(:is_verified, true)
        Note.create(related: @person, note_created_by: current_user, ip_address: request.remote_ip, subject: "Verified", note: "User was manually verified")
        flash[:notice] = "#{@person.name}'s account has been verified"
      else
        flash[:notice] = "#{@person.name}'s account does not have all of the required information (such as zip codes, phone numbers, etc. and cannot be manually verified)"
      end
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  # 2011-08-03 -- RT -- added method
  def lock_redemption
    @person = Person.find(params[:id].to_i)
    if @person.redemption_locked?
      flash[:error] = "#{@person.name}'s redemption ability already locked"
    else
      @person.lock_redemption
      @person.save!
      Note.create(related: @person, note_created_by: current_user, ip_address: request.remote_ip, subject: "Redemption Locked", note: "User's redemption ability was manually locked")
      flash[:notice] = "#{@person.name}'s redemption ability has been locked"
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  # 2011-08-03 -- RT -- added method
  def unlock_redemption
    @person = Person.find(params[:id].to_i)
    if !@person.redemption_locked?
      flash[:error] = "#{@person.name}'s redemption ability already unlocked"
    else
      @person.unlock_redemption
      @person.save!
      Note.create(related: @person, note_created_by: current_user, ip_address: request.remote_ip, subject: "Redemption Unlocked", note: "User's redemption ability was manually unlocked")
      flash[:notice] = "#{@person.name}'s redemption ability has been unlocked"
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  # 2010-06-17 -- RT -- added method
  def make_vip
    @person = Person.find(params[:id].to_i)
    if @person.vip?
      flash[:error] = "#{@person.name}'s account already has VIP status"
    else
      @person.vip = 1
      if @person.update_attribute(:vip, true)
        Note.create(related: @person, note_created_by: current_user, ip_address: request.remote_ip, subject: "VIP", note: "User was given VIP status")
        flash[:notice] = "#{@person.name}'s account has been given VIP status"
      else
        flash[:notice] = "#{@person.name} account does not have all of the required information (such as zip codes, phone numbers, etc. and cannot be marked as VIP)"
      end
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  # 2010-06-21 -- RT -- added method
  def undo_vip
    @person = Person.find(params[:id].to_i)
    if @person.vip?
      @person.vip = 0
      @person.update_attribute(:vip, false)
      Note.create(related: @person, note_created_by: current_user, ip_address: request.remote_ip, subject: "VIP", note: "User VIP status removed")
      flash[:notice] = "#{@person.name}'s account no longer has VIP status"
    else
      flash[:error] = "#{@person.name}'s account does not have VIP status"
    end
    redirect_to controller: "admin/people", action: "show", id: @person
  end

  def migrate_to_bigbox
    @person = Person.find(params[:id])
    @migrated_songs = @person.migrate_to_bigbox
    flash[:success] = "Sent #{@migrated_songs} to be processed by bigbox. When the job is complete this page will state 'Migrated to Bigbox'"
    redirect_to admin_person_path(@person)
  end

  def features
    @person = Person.find(params[:id])
    @features = Feature.unlock_and_restrict_by_user_features
    render action: :features, layout: "admin_application_lightbox"
  end

  def toggle_features
    @person = Person.find(params[:id])

    features =
      if !params[:person].nil? && !params[:person][:features].nil?
        params[:person][:features]
      else
        []
      end

    @person.features = Feature.find(features)
    if @person.features.empty?
      note = "#{@person.name} does not have any features enabled"
    else
      note = "#{@person.name} has the following feature(s) enabled #{@person.features.collect { |f| f.name.titleize }.join(', ')}"
    end
    @note = Note.create(
      related: @person,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Enable/Disable Features",
      note: note
    )
    flash[:notice] = note
    render action: :toggle_features_changed, layout: "admin_application_lightbox"
  end

  def update_tax_tokens
    @tax_token_service = TaxTokenService.new(params[:id]).remove_user_from_all_cohorts
    redirect_to admin_person_path(@person)
  end

  def toggle_feature
    feature = Feature.find_by(name: params[:feature])

    feature_enabler = @person.feature_people.find_or_initialize_by(feature_id: feature.id)
    feature_enabler.new_record? ? feature_enabler.save : feature_enabler.destroy

    redirect_to admin_person_path(@person)
  end

  def remove_dormancy
    person = Person.find(params[:id])
    service = AdminDormancyService.remove_person_dormancy(current_user, person, request)
    flash[:error] = service.error if service.errors?
    redirect_to admin_person_path(person)
  end

  def download_assets
    @recover_user_assets_form = Admin::RecoverUserAssetsForm.new
    @recovery_user_assets = @person.recover_assets.includes(:s3_asset)
  end

  def create_recover_assets
    @recover_user_assets_form = Admin::RecoverUserAssetsForm.new(recover_user_assets_params)
    if @recover_user_assets_form.save
      flash[:success] = "Request has been submitted successfuly. Please find the status in the items listed below."
      redirect_to download_assets_admin_person_path(@person)
    else
      flash[:error] = "Something went wrong, please check the errors and submit again."
      @recovery_user_assets = @person.recover_assets.includes(:s3_asset)
      render action: "download_assets"
    end
  end

  def block_withdrawals
    if @person.block_withdrawal!(current_user, request.remote_ip)
      flash[:success] = "Successfuly blocked withdrawals for #{@person.name}"
    else
      flash[:error] = "Not able to block the withdrawals, contact support team."
    end
    redirect_to admin_person_path(@person)
  end

  def unblock_withdrawals
    if @person.unblock_withdrawal!(current_user, request.remote_ip)
      flash[:success] = "Successfuly unblocked withdrawals for #{@person.name}"
    else
      flash[:error] = "Not able to unblock the withdrawals, contact support team."
    end
    redirect_to admin_person_path(@person)
  end

  protected

  def can_take_over_account?
    return false if params[:id].blank?

    account_to_take_over = Person.find(params[:id])

    # don't allow takeover of administrator's account
    !account_to_take_over.is_administrator?
  end

  private

  def fetch_user_cc_transactions
    if can_use_payments_os?(@person)
      @payment_vendor = "payments_os".freeze
      @decorator = PaymentsOSTransactionDecorator
      PaymentsOSTransaction.user_transactions(@person).order(created_at: :desc).paginate(
        page: params.fetch(:page, 1), per_page: params.fetch(:per_page, 10)
      )
    else
      @payment_vendor = "braintree".freeze
      BraintreeTransaction.search_transactions(
        ["person_id = ?", @person.id], page: params[:page], per_page: params[:per_page]
      )
    end
  end

  def user_payu_transactions
    @payu_payment_vendor = "payu".freeze
    @payu_decorator = PayuTransactionDecorator
    PayuTransaction.user_transactions(@person).order(created_at: :desc).paginate(
      page: params.fetch(:page, 1), per_page: params.fetch(:per_page, 10)
    )
  end

  def get_reset_password_link(person)
    key = (person.invite_expiry.blank? || person.invite_expiry < Time.now + 2.days) ? person.generate_invite_code : person.invite_code
    raise "Unable to generate invite code" unless key

    host = WEB_ONLY_URLS[person.country_domain]
    "#{host}/reset_password?person_id=#{person.id}&key=#{key}"
  end

  def handle_changed_email
    PersonNotifier.change_email(@person, @person.email).deliver
  end

  def render_change_status
    render action: :change_status, layout: "admin_application_lightbox"
  end

  def render_changed_status
    render action: :status_changed, layout: "admin_application_lightbox"
  end

  def load_person
    @person = Person.find(params[:id])
  end

  def load_transaction_dates
    @transaction_dates = Person::TransactionDatesFinder.new(@person, params[:date])
  end

  def note_params
    params[:note].permit(
      :related_id,
      :ip_address,
      :subject,
      :note
    ).merge(note_created_by: current_user)
  end

  def recover_user_assets_params
    params
      .require(
        :admin_recover_user_assets_form
      )
      .permit(
        :album_ids_and_isrcs,
        include_assets: []
      ).merge(
        admin: current_user,
        person: @person
      )
  end

  def authorize_download_assets
    return if current_user.is_mass_download_assets_admin?

    flash[:alert] = "You are not authorized to do this."
    redirect_back fallback_location: home_path
  end
end
