class Admin::ComposersManagersImportsController < Admin::AdminController
  layout "tc_foundation_admin"

  def create
    PublishingAdministration::IngestionBlockerWorker.perform_async(params[:composers_manager_id])

    @flash_notice = "Importing Data from RightsApp for this account. The account should be updated within a few minutes."
  end
end
