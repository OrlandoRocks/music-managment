class Admin::Petri::MassTakedownsController < Admin::AdminController
  before_action :load_mass_takedown_variables, only: [:new, :create]
  before_action :load_mass_takedown_form, only: [:new, :create]

  def create
    @notify_list = @mass_takedown_form.notify_list if @mass_takedown_form.save

    flash.now[:error] = @mass_takedown_form.errors.full_messages.join(", ") if @mass_takedown_form.errors.any?

    render :new
  end

  def edit
    @mass_takedown_stores = Store.mass_takedown_stores
    @ytsr_store_id = Store.find_by(abbrev: "ytsr").id.to_s
    @fbt_store_id = Store.find_by(abbrev: "fbt").id.to_s
  end

  def destroy
    untakedown      = MassTakedown.mass_untakedown(params[:album_ids], params[:selected_stores], current_user)
    flash[:error]   = untakedown.errors.join(", ") if untakedown.errors.any?
    flash[:notice] =
      "Succesfully undid takedowns for albums: #{untakedown.success.join(', ')}" if untakedown.success.any?
    redirect_to admin_petri_mass_untakedown_path
  end

  def encumbrance_info
    albums = BulkAlbumFinder.new(params[:album_ids]).execute
    render json: find_albums_with_encumbrance(albums), status: :ok
  end

  private

  def load_mass_takedown_variables
    @new_css              = true
    @page_title           = "PETRI Mass Takedown Tool"
    @mass_takedown_stores = Store.mass_takedown_stores
  end

  def load_mass_takedown_form
    @mass_takedown_form = MassTakedownForm.new(mass_takedown_form_params)
  end

  def mass_takedown_form_params
    @form_params = params.fetch(:mass_takedown_form, { album_ids: params[:album_id] })
                         .merge(
                           admin: current_user,
                           ip_address: request.remote_ip
                         )
    ensure_youtube_parity if includes_yt_album_takedown?
    @form_params
  end

  def includes_yt_album_takedown?
    !@form_params[:album_level_stores].nil? && @form_params[:album_level_stores].include?("28")
  end

  def ensure_youtube_parity
    @form_params[:album_level_stores].push("105") unless @form_params[:album_level_stores].include?("105")
    if @form_params[:track_level_stores]
      @form_params[:track_level_stores].push("48") unless @form_params[:track_level_stores].include?("48")
    else
      @form_params[:track_level_stores] = ["48"]
    end
  end

  def send_sns_notif_mass_untakedown
    Sns::Notifier.perform(
      topic_arn: ENV["SNS_RELEASE_UNTAKEDOWN_TOPIC"],
      album_ids_or_upcs: params[:album_ids],
      store_ids: params_to_store_ids(params[:selected_stores]),
      delivery_type: "untakedown",
      person_id: current_user.id
    )
  end

  def params_to_store_ids(stores)
    store_ids = stores
    store_ids += Store::ITUNES_STORE_IDS if stores.include?("iTunes")
    store_ids.map(&:to_i).reject(&:zero?)
  end

  def find_albums_with_encumbrance(albums)
    {
      album_ids: params[:album_ids].split(", ")
                                   .map { |album_id|
                   albums.map { |album|
                     album_id if person_has_encumbrance?(album, album_id)
                   }
                 }.flatten.compact.uniq
    }
  end

  def person_has_encumbrance?(album, album_id)
    album.person.has_encumbrance? &&
      (album.id.to_s == album_id || album.upc.number.to_s == album_id)
  end
end
