class Admin::SettlementSummariesController < Admin::AdminController
  before_action :load_country

  def index
    @month_summaries = SettlementReport::MonthSummary.report(@country_website)

    respond_to do |format|
      format.html # show.rhtml
      format.csv do
        response = ""
        filename = "month_summaries_#{@country_website.country}.csv"
        csv = CSV.new(response, row_sep: "\r\n")
        if request.env["HTTP_USER_AGENT"].match?(/msie/i)
          headers["Pragma"] = "public"
          headers["Content-type"] = "text/plain"
          # headers['Cache-Control'] = 'no-cache, must-revalidate, post-check=0, pre-check=0'
          headers["Cache-Control"] = "max-age=0"
          headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
          headers["Expires"] = "0"
        else
          headers["Content-Type"] ||= "text/csv"
          headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
        end
        csv << SettlementReport::MonthSummary.csv_headers
        @month_summaries.each { |summary| csv << summary.to_csv_a }
        render plain: response
      end
    end
  end

  def show
    @day = params[:id]
    @daily_summaries = SettlementReport::DaySummary.report(@country_website, @day)

    respond_to do |format|
      format.html # show.rhtml
      format.csv do
        response = ""
        filename = "daily_summaries_#{@day.underscore}.csv"
        csv = CSV.new(response, row_sep: "\r\n")
        if request.env["HTTP_USER_AGENT"].match?(/msie/i)
          headers["Pragma"] = "public"
          headers["Content-type"] = "text/plain"
          # headers['Cache-Control'] = 'no-cache, must-revalidate, post-check=0, pre-check=0'
          headers["Cache-Control"] = "max-age=0"
          headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
          headers["Expires"] = "0"
        else
          headers["Content-Type"] ||= "text/csv"
          headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
        end
        csv << SettlementReport::DaySummary.csv_headers
        @daily_summaries.each { |summary| csv << summary.to_csv_a }
        render plain: response
      end
    end
  end

  private

  def load_country
    params[:country] ||= "US" # if params are not set, default to 'US'
    @country_website = CountryWebsite.find_by(country: params[:country].to_s)
  end
end
