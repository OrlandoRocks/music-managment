class Admin::RightsController < Admin::AdminController
  layout "admin_application_lightbox"

  def edit
    @admin_roles = Role.administrative
    @person = Person.find(params[:id])
    @selected_roles = @person.roles.administrative.group_by(&:name)
  end

  def update
    @person = Person.find(params[:id])
    PeopleRole.where(
      person_id: @person.id,
      role_id: @person.roles.administrative.pluck(:id)
    ).delete_all
    @person.roles << Role.find(params[:roles]) if params[:roles]
  end

  def not_permitted
    render(layout: "admin") unless params[:layout]
  end
end
