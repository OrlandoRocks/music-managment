class Admin::PersonPreferencesController < Admin::AdminController
  def edit_auto_renewal
    @person = Person.find(params[:person_id])

    load_payment_auto_renew_form_variables
    render layout: "admin_application_lightbox"
  end

  def update_auto_renewal
    @person = Person.find(params[:person_id])
    note = { note_created_by: current_user, ip_address: request.remote_ip, note: params[:note][:note] }
    case params[:person][:action_type]
    when "disable"
      @person.disable_auto_renewal(note)
    when "enable"
      @person.enable_auto_renewal(note)
    end

    if @person.errors.empty?
      @action_title = @person.autorenew? ? "Auto-renew enabled" : "Auto-renew disabled"
      @action_title += " for #{h @person.name}"
    else
      load_payment_auto_renew_form_variables
      render action: "edit_auto_renewal"
    end
  end

  private

  def load_payment_auto_renew_form_variables
    if @person.autorenew?
      @action_title = "Disable Auto-Renew"
      @action_type = "disable"
    else
      @action_title = "Enable Auto-Renew"
      @action_type = "enable"
    end
  end
end
