class Admin::QaTools::AlbumApprovalsController < Admin::ContentReviewController
  before_action :redirect_unless_qa_env

  def index
    @page_title = "Album Approval"
  end

  def create
    update
  end

  private

  def redirect_unless_qa_env
    redirect_to admin_dashboard_index_path unless qa_or_dev_environment?
  end

  def update_params
    super.merge(event: "APPROVED")
  end
end
