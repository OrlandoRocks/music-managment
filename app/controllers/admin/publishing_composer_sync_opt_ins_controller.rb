class Admin::PublishingComposerSyncOptInsController < Admin::AdminController
  def destroy
    ::PublishingAdministration::PublishingComposerSyncOptInForm.destroy(destruction_params)

    redirect_to destruction_redirection_path(params[:id])
  end

  private

  def destruction_params
    params.require(:publishing_composer).permit(:id, :sync_opted_updated_at)
  end

  def destruction_redirection_path(account_id)
    admin_publishing_composers_manager_path(account_id)
  end
end
