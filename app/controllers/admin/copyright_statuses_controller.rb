class Admin::CopyrightStatusesController < Admin::AdminController
  def update
    @copyright_status = CopyrightStatus.find(params[:id])
    @status_updater = Admin::CopyrightStatusUpdaterService
                      .update(@copyright_status, current_user, copyright_status_params)
    @album = Album.find(@copyright_status.related_id)
  end

  private

  def copyright_status_params
    params
      .require(:copyright_status)
      .permit(:status, note: [:note])
  end
end
