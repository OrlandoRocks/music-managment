class Admin::DistributionRetryController < Admin::AdminController
  before_action :load_album, only: [:create]
  before_action :build_retriable_distributions, only: [:create]
  before_action :build_distribution_retry_form, only: [:create]

  def create
    respond_to do |format|
      if @distribution_retry_form.save
        format.js { render partial: "admin/albums/petri_bundle", locals: { album: @album, distro: @distribution } }
      else
        format.js { render json: @distribution_retry_form.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_curated_artists
    distribution = ::Distribution.find(params[:distribution_id])
    curated_artist_service = Apple::CuratedArtistService.new(distribution.album.id)
    curated_artist_service.remove_curated_creatives_flags
    album = distribution.album
    Note.create(
      related: album,
      note_created_by_id: current_user.id,
      subject: "Curated Artists Admin Override",
      note: "Admin override to send release to Apple despite curated artist block",
      ip_address: request.remote_ip
    )
    respond_to do |format|
      format.js { render partial: "admin/albums/petri_bundle", locals: { album: album, distro: distribution } }
    end
  end

  private

  def build_retriable_distributions
    @distribution = ::Distribution.find(params[:distribution_id])

    @retriable_distributions = []
    @retriable_distributions << Delivery::Retry.new(
      {
        delivery: @distribution,
        priority: params[:priority],
        person: current_user,
        delivery_type: params[:delivery_type],
      }
    )
  end

  def build_distribution_retry_form
    @distribution_retry_form = DistributionRetryForm.new(
      retriable_distributions: @retriable_distributions,
      ip_address: request.remote_ip,
      album: @album
    )
  end

  def load_album
    @album = Album.find(params[:album_id])
  end
end
