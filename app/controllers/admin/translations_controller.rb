class Admin::TranslationsController < Admin::AdminController
  def index
    @language_code = (params[:language_code].presence || "de")
    diff_data(@language_code)
  end

  private

  def diff_data(language_code)
    @en_files = build_locale_file_hash(file_paths_for("en"), language_code)
    @translation_files = build_locale_file_hash(file_paths_for(language_code), language_code)
  end

  def build_locale_file_hash(files_array, language_code)
    files_array.each_with_object({}) do |f_name, result|
      result[f_name] = []
      File.readlines(f_name).each do |line|
        result[f_name] << extract_key(line) if is_valid_line?(line, language_code)
      end
    end
  end

  def file_paths_for(language_code)
    Dir["#{Rails.root}/config/locales/*#{language_code}.yml", "#{Rails.root}/config/locales/*/*#{language_code}.yml", "#{Rails.root}/config/locales/*/*/*#{language_code}.yml"]
  end

  def extract_key(line)
    char_num = line.index(":")
    line[0..char_num].strip
  end

  def is_valid_line?(line, language_code)
    left_string = nil
    left_string = extract_key(line) if line.to_s.include?(":")
    !(left_string.to_s.blank? || left_string.include?("en:") || left_string.include?("#{language_code}:") || is_not_underscored?(left_string) || left_string[0] == "<")
  end

  def is_not_underscored?(string)
    !(string == string.underscore)
  end
end
