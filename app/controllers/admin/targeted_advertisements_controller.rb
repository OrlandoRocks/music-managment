class Admin::TargetedAdvertisementsController < Admin::AdminController
  before_action :redirect_if_not_permitted, only: :destroy
  before_action :redirect_if_not_permitted_lightbox, only: [:new, :create]
  before_action :load_targeted_advertisement, except: [:new, :create]
  before_action :load_offer, only: [:new, :create]

  def new
    current_ads = @targeted_offer.targeted_advertisements.collect { |ta| [ta.advertisement_id] }.flatten.uniq
    conditions =
      if !current_ads.empty?
        ["status = 'Active' and id NOT IN (?)", current_ads]
      else
        ["status = 'Active'"]
      end
    @targeted_advertisement = TargetedAdvertisement.new
    @ad_list =
      Advertisement.where(conditions).order(:category, :name).collect { |a|
        ["#{a.category} | #{a.name}", a.id]
      }
    render layout: "admin_application_lightbox"
  end

  def create
    @targeted_offer.targeted_advertisements.create(params[:targeted_advertisement])
    render layout: "admin_application_lightbox"
  end

  def destroy
    targeted_ad = TargetedAdvertisement.find(params[:id])
    targeted_ad.destroy
    redirect_to admin_targeted_offer_path(targeted_ad.targeted_offer)
  end

  private

  def load_targeted_advertisement
    @targeted_advertisement = TargetedAdvertisement.find(params[:id])
  end

  def load_offer
    @targeted_offer = TargetedOffer.find(params[:targeted_offer_id])
  end
end
