class Admin::PublishingComposersManagersImportsController < Admin::AdminController
  layout "tc_foundation_admin"

  def create
    ::PublishingAdministration::PublishingIngestionBlockerWorker.perform_async(params[:publishing_composers_manager_id])

    @flash_notice = "Importing Data from RightsApp for this account. The account should be updated within a few minutes."
  end
end
