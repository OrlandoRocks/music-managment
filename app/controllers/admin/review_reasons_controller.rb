class Admin::ReviewReasonsController < Admin::AdminController
  before_action :get_review_reason, only: [:show, :edit, :update, :destroy]
  before_action :content_manager_role_required

  def index
    @review_reasons = ReviewReason.all
  end

  def new
    @review_reason = ReviewReason.new
  end

  def create
    review_reason_params[:role_ids] ||= []
    @review_reason = ReviewReason.new(review_reason_params)
    if @review_reason.save
      flash[:notice] = "Registered the information successfully"
      redirect_to action: "show", id: @review_reason.id
    else
      render action: "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    review_reason_params[:role_ids] ||= []
    if @review_reason.update(review_reason_params)
      flash[:notice] = "Updated the review reason information successfully"
      redirect_to action: "show", id: @review_reason.id
    else
      render action: "edit"
    end
  end

  def destroy
    @review_reason.destroy
    flash[:notice] = "Destroyed the review reason"
    redirect_to action: "index"
  end

  private

  def get_review_reason
    return if @review_reason = ReviewReason.find(params[:id])

    flash.now[:error] = "Wrong review reason ID"
    raise ActiveRecord::RecordNotFound
  end

  def content_manager_role_required
    render plain: "Unauthorized", status: :forbidden unless current_user.has_role?("Content Review Manager")
  end

  def review_reason_params
    params.require(:review_reason).permit(
      { role_ids: [] },
      :reason_type,
      :reason,
      :email_template_path,
      :should_unfinalize
    )
  end
end
