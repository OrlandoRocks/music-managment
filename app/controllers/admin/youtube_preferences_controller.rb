class Admin::YoutubePreferencesController < Admin::AdminController
  def index
    return unless params[:term_type] and params[:term]

    @people = Person.where(params[:term_type] => params[:term]) if ["id", "email"].include? params[:term_type]
    @people = Person.where("name like ?", "%#{params[:term]}%") if params[:term_type] == "name"
    @people = @people.paginate(page: (params[:page] || 1).to_i, per_page: (params[:per_page] || 30).to_i)
  end

  def update
    youtube_preference = YoutubePreference.find(params[:id])

    flash_message =
      if youtube_preference.update(update_params_hash)
        { success: "Youtube Channel ##{params[:id]} Updated" }
      else
        { error: "Unable To Update Youtube Channel ##{params[:id]}" }
      end

    redirect_to request.referrer, flash: flash_message
  end

  def destroy
    pref = YoutubePreference.find params[:id]
    pref.reset_preferences
    redirect_to request.referrer, flash: { success: "Youtube Channel Removed" }
  end

  private

  def update_params_hash
    params.permit(:channel_id).merge(whitelist: 1)
  end
end
