class Admin::PublishingCompositionSplitsController < Admin::AdminController
  before_action :requires_publishing_manager_role

  def index
    @page_title = "Publishing Splits Search"
  end

  def search
    @page_title = "Publishing Splits Search"
    @query = params[:query]
    @search = params[:search]
    @per_page = params[:per_page]
    @order = params[:order]
    @searched_items = Admin::PublishingAdministration::PublishingCompositionSplitAdminQueryBuilder
                      .build(
                        query: @query,
                        search: @search,
                        per_page: @per_page,
                        order: @order,
                        page: params[:page] || 1
                      )
  end

  def update
    split_hash = params[:split]

    if split_hash
      splits = PublishingCompositionSplit.includes(:publishing_composition).where(id: split_hash.keys).all

      splits.each do |split|
        split.submit_split(split_hash[split.id.to_s])
        next unless split.errors.messages.empty?

        flash[:error] =
          "#{flash[:error]} Composition #{split.composition.id} (#{split.errors.full_messages.join(', ')})."
      end

      flash[:success] = "Splits Updated Successfully" unless flash[:error]
    else
      flash[:error] = "You did not submit any split changes"
    end

    redirect_back fallback_location: home_path
  end

  private

  def requires_publishing_manager_role
    return if current_user.has_role?("Publishing Manager", false)

    flash[:error] = "You do not have sufficient Role to access this page"
    redirect_to admin_home_path and return
  end
end
