class Admin::DashboardController < Admin::AdminController
  before_action :check_if_2fa_enabled, if: :force_admin_2fa?

  def reports
    redirect_to controller: "product_reports", action: :index
  end

  def processing_albums
    @albums = Tunecore::Reports::ProcessingAlbums.find_by(name: "Processing Albums").processing_albums
  end

  def search
    redirect_to controller: "/admin/albums" and return unless params[:query]

    # id|state|album_state|upc|isrc|name|label|artist

    search_type = (params[:query]["type"]).split("|")
    search_term = "#{search_type[1]}: #{params[:query]['term']}"
    cookies[:search_type] = params[:query]["type"]

    logger.debug("=========================\nparams: #{params.inspect}\n===========================")

    if search_type[1] == "bundle_state"
      search_term = "#{params[:query]['state']} #{params[:query]['term']}"
      redirect_to controller: "/admin/albums",
                  action: "search_bundles",
                  term: search_term,
                  search: params[:query]["type"]
    elsif search_type[0] == "album"
      redirect_to controller: "/admin/albums", action: "search", term: search_term, search: params[:query]["type"]
    elsif search_type[0] == "person"
      redirect_to controller: "/admin/people", action: "search", term: search_term, search: params[:query]["type"]
    elsif search_type[0] == "video"
      redirect_to controller: "/admin/videos", action: "search", term: search_term, search: params[:query]["type"]
    elsif search_type[0] == "invoice"
      redirect_to controller: "/admin/invoices", action: "search", term: search_term, search: params[:query]["type"]
    else
      head :ok
    end
  end
end
