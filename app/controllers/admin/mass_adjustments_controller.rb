class Admin::MassAdjustmentsController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :restrict_access, unless: :has_correct_admin_role?
  before_action :process_batches_as_completed, only: [:index]
  before_action :redirect_if_batch_in_flight, only: [:new, :create]

  def index
    @mass_adjustments = MassAdjustmentBatch
                        .joins(:created_by)
                        .order(created_at: :desc)
                        .all
                        .paginate(page: params[:page] || 1, per_page: 25)
  end

  def new
    @mass_adjustments_tool_form = MassAdjustmentsForm.new({ admin_message: "", customer_message: "" })
  end

  def create
    @mass_adjustments_tool_form = MassAdjustmentsForm.new(form_params)
    @mass_adjustments_tool_form.save
    @errors = @mass_adjustments_tool_form.errors.full_messages

    @errors.empty? ? redirect_to(action: :show, id: @mass_adjustments_tool_form.batch.id) : (render :new)
  end

  def update
    batch = MassAdjustmentBatch.find(batch_params[:id])
    batch.update(batch_params)
    MassAdjustmentSchedulerService.schedule(batch) if batch.status == MassAdjustmentBatch::APPROVED
    redirect_to action: :show
  end

  def show
    @batch = MassAdjustmentBatch.find(params[:id])
    @batch_entry_stats = MassAdjustmentEntry
                         .where(mass_adjustment_batch_id: @batch.id)
                         .group(:status)
                         .count
    @negative_balance_entries_count = MassAdjustmentEntry
                                      .where(mass_adjustment_batch_id: @batch.id, balance_turns_negative: true)
                                      .count
    @mass_adjustment_entries = MassAdjustmentEntry
                               .where(mass_adjustment_batch_id: @batch.id)
                               .paginate(page: params[:page] || 1, per_page: 50)
  end

  private

  def process_batches_as_completed
    MassAdjustmentBatch.process_batches_as_completed
  end

  def form_params
    form_params = params[:mass_adjustments_form] || {}
    form_params[:current_user] = current_user
    form_params
  end

  def restrict_access
    flash[:error] = "unauthorized"
    redirect_to admin_home_path
  end

  def has_correct_admin_role?
    current_user.has_role?("Mass Adjustments")
  end

  def batch_params
    params.require(:mass_adjustment_batch)
          .permit(:id, :allow_negative_balance, :status)
  end

  def redirect_if_batch_in_flight
    return unless MassAdjustmentBatch.batch_in_flight?

    latest_batch = MassAdjustmentBatch.latest
    flash[:notice] = "There is already a batch ( ID: #{latest_batch.id} ) in flight. Mass balance adjustments are designed to process batches sequentially"
    redirect_to admin_mass_adjustments_path
  end
end
