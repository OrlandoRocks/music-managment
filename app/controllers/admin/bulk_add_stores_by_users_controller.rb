class Admin::BulkAddStoresByUsersController < Admin::AdminController
  layout "admin_new"
  include AdminResource
  before_action :check_admin_login
  before_action :set_variables_for_new, only: [:new]

  def new
    @stores = Store.is_active.is_used
  end

  def create
    bulk_adder = BulkAddStoresByUsers.new(bulk_adder_attrs)
    bulk_adder.save
    flash[:errors] = bulk_adder.errors[:service_message]
    redirect_to new_admin_bulk_add_stores_by_user_path
  end

  protected

  def bulk_adder_params
    params[:bulk_add_stores_by_users]
  end

  def set_variables_for_new
    @bulk_adder = BulkAddStoresByUsers.new
  end

  def bulk_adder_attrs
    { store_id: bulk_adder_params[:store_id], user_ids: bulk_adder_params[:user_ids] }
  end
end
