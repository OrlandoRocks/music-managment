require "will_paginate/array"

class Admin::Distributor::ErrorsController < Admin::AdminController
  respond_to :js

  def index
    @page_title = "Distributor Delivery Error"
    @distributor_errors_form = ::Distributor::ErrorForm.new(distributor_error_params).save
    @distributor_errors_ungrouped = (@distributor_errors_form.distributor_errors || [])
                                    .paginate(page: params["page"] || 1, per_page: 20_000)
    @distributor_errors =
      @distributor_errors_ungrouped
      .group_by { |f| f[@distributor_errors_form.filter_type] }
  end

  def create
    @releases_stores = TcDistributor::Base.new(releases_store_ids: params[:releases_store_ids]).releases_stores

    @releases_stores.each do |releases_store|
      send_sns_notification(
        delivery_type: releases_store["delivery_type"],
        album_id: releases_store["album_id"],
        store_id: releases_store["store_id"]
      )
    end

    respond_with releases_store_ids
  end

  private

  def distributor_error_params
    {
      start_date: params[:start_date],
      end_date: params[:end_date],
      filter_type: params[:filter_type],
      error_type: params[:error_type],
      store_id: params[:store_id]
    }
  end

  def releases_store_ids
    @releases_store_ids ||= @releases_stores.pluck("id")
  end

  def send_sns_notification(delivery_type:, album_id:, store_id:)
    Sns::Notifier.perform(
      topic_arn: ENV["SNS_RELEASE_RETRY_TOPIC"],
      album_ids_or_upcs: [album_id],
      store_ids: [store_id],
      delivery_type: delivery_type,
      person_id: current_user.id
    )
  end
end
