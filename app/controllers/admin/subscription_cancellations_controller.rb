class Admin::SubscriptionCancellationsController < Admin::AdminController
  def create
    subscription = PersonSubscriptionStatus.find(params[:subscription_id])
    social_cancellation_form = SubscriptionCancellationForm.new(
      subscription: subscription, admin: current_user
    )
    flash[:error] = social_cancellation_form.errors.full_messages.join(" & ") unless social_cancellation_form.save
    redirect_to admin_person_subscriptions_path(subscription.person)
  end
end
