class Admin::AddSalepointsController < Admin::AdminController
  def edit
    @album = fetch_album
  end

  def update
    @album = Album.find_by(id: params[:album_id])
    stores = Store.where(id: params[:store_ids])
    @apple_music = apple_music?
    @salepoints, @stores_not_added = @album.add_stores(stores)

    AddPaidSalepointsService.call(current_user, @album, @salepoints, @apple_music)
  end

  private

  def album_id_or_upc
    params.require(:album_id_or_upc)
  end

  def fetch_album
    return [] if params[:album_id_or_upc].blank?

    Album
      .joins(:upcs)
      .where(id: album_id_or_upc)
      .or(
        Album.joins(:upcs).where(upcs: { number: album_id_or_upc })
      ).first
  end

  def apple_music?
    params[:apple_music] == "on"
  end
end
