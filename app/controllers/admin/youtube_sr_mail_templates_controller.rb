class Admin::YoutubeSrMailTemplatesController < Admin::AdminController
  layout "admin_new"

  def index
    @youtube_sr_mail_templates = YoutubeSrMailTemplate.includes(:country_website).all
  end

  def new
    @youtube_sr_mail_template = YoutubeSrMailTemplate.new
  end

  def create
    @youtube_sr_mail_template = YoutubeSrMailTemplate.new(ytsr_mail_template_params)
    if @youtube_sr_mail_template.save
      redirect_to admin_youtube_sr_mail_templates_path
    else
      render :new
    end
  end

  def edit
    @youtube_sr_mail_template = YoutubeSrMailTemplate.find(params[:id])
  end

  def update
    @youtube_sr_mail_template = YoutubeSrMailTemplate.find(params[:id])
    if @youtube_sr_mail_template.update(ytsr_mail_template_params)
      redirect_to admin_youtube_sr_mail_templates_path
    else
      render :edit
    end
  end

  def destroy
    YoutubeSrMailTemplate.find(params[:id]).destroy
    redirect_to admin_youtube_sr_mail_templates_path
  end

  private

  def ytsr_mail_template_params
    fields = [
      :select_label,
      :country_website_id,
      :subject,
      :header,
      :body,
      :footer
    ]

    fields << :custom_fields if current_user.has_role?("Developer")

    params.require(:youtube_sr_mail_template).permit(*fields).tap do |ytsr_params|
      ytsr_params[:custom_fields] = ytsr_params[:custom_fields].split(" ") if current_user.has_role?("Developer")
      ytsr_params[:template_name] = ytsr_params[:select_label].parameterize.underscore if params[:action] == "create"
    end
  end
end
