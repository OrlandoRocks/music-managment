# 2011-08-29 AK -- creating admin/composers controller as per Joe's instructions
class Admin::ComposersController < Admin::AdminController
  before_action :requires_publishing_role, only: [:list]
  before_action :requires_publishing_manager_role, except: [:list]
  before_action :load_composer, only: [:show, :edit, :destroy]
  before_action :rights_app_enabled?, only: [:edit]

  def list
    setup_fields
    page = params[:page] || 1
    per_page = params[:per_page] || 50
    options = { page: page, per_page: per_page, order_by: params[:order_by] }
    options[:all] = true if params[:all]
    load_composers(all = true, options)
    @albums_finalized_count = Composer.albums_finalized_count
                                      .where("composers.id IN (?)", @composers.collect(&:id))
    @albums_with_splits_count = Composer.albums_with_splits_count
                                        .where("composers.id IN (?)", @composers.collect(&:id))

    respond_to do |format|
      format.html
      format.xml  { render xml: @composers }
    end
  end

  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render xml: @composer }
    end
  end

  def edit
    @publisher = @composer.publisher

    render :edit
  end

  def update
    @composer = Composer.find(params[:id])
    setup_params_for_composer

    setup_publisher if params[:publisher].present?
    respond_to do |format|
      if @composer.update(composer_params)
        @composer.notes.new(subject: "Updated", note: "Saved changes", note_created_by: current_user).save
        flash[:notice] = "Composer was successfully updated."
        format.html { render :show }
        format.xml  { head :ok }
      else
        flash[:error] = custom_t("controllers.composer.your_registration_couldnt_be_saved")
        format.html { render :edit }
      end
    end
  end

  def destroy
    if @composer.related_purchases.empty?
      @composer.destroy
    else
      flash[:error] = "Cannot delete record because it's in a cart, or has been paid for"
    end
    respond_to do |format|
      format.html { redirect_to(action: "list") }
      format.xml  { head :ok }
    end
  end

  # 2011-8-7 AK Streams all songs for existing composers as CSV
  def download_songs
    filename = "#{Date.today.year}-#{Date.today.month}-#{Date.today.day}-songwriter-songs.csv"
    headers["Content-Type"] ||= "text/csv"
    headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
    col_names = [
      "songwriter id",
      "songwriter name",
      "songwriter email",
      "artist name",
      "album id",
      "album name",
      "track #",
      "song id",
      "song name",
      "tunecore identifier",
      "ISRC",
      "genre",
      "release date",
      "distributed",
      "taken down",
      "account id",
      "account name",
      "account email"
    ]
    person_ids = Composer.all.collect(&:person_id).join(",")
    sql = %Q(select
    	sw.id as 'songwriter id',
    	concat(sw.first_name,' ', sw.last_name) as 'songwriter name',
    	sw.email as 'songwriter email',
    	at.name as 'artist name',
    	a.id as 'album id',
    	a.name as 'album name',
    	s.track_num as 'track #',
    	s.id as 'song id',
    	s.name as 'song name',
    	s.tunecore_isrc as 'tunecore identifier',
    	s.optional_isrc as 'ISRC',
    	g.name as 'genre',
    	a.orig_release_year as 'release date',
    	if((a.finalized_at is null), 0, 1) as 'distributed',
    	if((a.takedown_at is null), 0, 1) as 'taken down',
    	p.id as 'account id',
    	p.name as 'account name',
    	p.email as 'account email'
    from composers sw
    	inner join people p on sw.person_id = p.id
    	inner join albums a on a.person_id = p.id
    	left join songs s on s.album_id = a.id
    	left join genres g on a.primary_genre_id = g.id
    	left join creatives cr on cr.creativeable_id = a.id and cr.creativeable_type = 'Album'
    	left join artists at on at.id = cr.artist_id
    where p.id in (#{person_ids})
    order by sw.id, sw.created_at, a.id, s.track_num ASC)
    conn = ActiveRecord::Base.connection
    logger.info conn.inspect
    res = conn.execute(sql, "Songwriter Songs")
    logger.debug("resultset: #{res}")
    render plain: proc do |_response, output|
      output.write(col_names.to_csv)
      res.each do |line|
        output.write(line.to_csv)
      end
    end
  end

  def download_client_import
    # FIXME: Track the person that downloaded the report
    options = { start_date: params[:d][:from_date] }
    options[:end_date] = params[:d][:to_date] if params[:d][:to_date].present?
    if date_range_is_over_a_year?(options)
      ClientImportReportWorker.perform_async(options.merge(person_id: current_user.id))
      flash[:notice] = "You'll receive a report in your mail shortly, kindly check your mail."
      redirect_to list_admin_composers_path
    else
      filename = PublishingReport.client_import(options)
      if filename
        current_user.notes.new(subject: "Publishing Client Import Download", note: "Downloaded #{filename}", note_created_by: current_user, ip_address: request.remote_ip).save
      end
      send_file(filename, type: "application/xls")
    end
  rescue => e
    flash[:error] = "There is an error in generating the client import file: #{e.message}"
    logger.error "Error in generating the client import file #{e.message}"
    redirect_to list_admin_composers_path
  end

  def download_summary
    redirect_to Composer.summary_download_path.to_s
  end

  private

  def composer_params
    params
      .require(:composer)
      .permit(
        :agreed_to_terms,
        :cae,
        :dob,
        :first_name,
        :last_name,
        :middle_name,
        :name_prefix,
        :name_suffix,
        :performing_rights_organization_id
      )
  end

  def publisher_params
    pro = PerformingRightsOrganization.find_by(id: params[:publisher][:performing_rights_organization_id])
    params
      .require(:publisher)
      .permit(:performing_rights_organization_id, :name, :cae)
      .merge(performing_rights_organization_id: pro&.id)
  end

  def load_composer
    @composer =
      if current_user.has_role?("Publishing Manager", false)
        Composer.find(params[:id])
      else
        current_user.composers.find(params[:id])
      end
  end

  def load_composers(all = false, options = {})
    if options[:order_by]
      case options[:order_by]
        # TODO: Did not implement order_by = paid_at yet, since it would require join to related_purchases
        # when 'paid_at'
        #          order = "composers.paid_at desc"
      when "splits_updated_at"
        order = "splits_updated_at desc"
      when "agreed_at"
        order = "agreed_to_terms_at desc"
      when "account_status"
        order = "people_status desc"
      end
    else
      order = "splits_updated_at desc"
    end

    unless (
            current_user.has_role?("Publishing Manager", false) ||
            current_user.has_role?("Publishing Viewer", false)
          ) &&
           all == true

      return
    end

    if options[:all]
      per_page = Composer.count
    elsif options[:page] && options[:per_page]
      per_page = options[:per_page]
    end

    # GC - I have to perform the search this way because for some reason, this query below is slow in MYSQL
    # SELECT distinct composers.*, publishing_splits.updated_at
    # FROM `composers` inner join people on composers.person_id = people.id
    # left join
    # (
    # select publishing_splits.composer_id, max(publishing_splits.updated_at) as updated_at
    # from publishing_splits
    # inner join compositions
    # on publishing_splits.`composition_id` = compositions.id
    # where compositions.is_unallocated_sum = 0
    # group by publishing_splits.composer_id
    # ) publishing_splits
    # on publishing_splits.composer_id = composers.id
    # order by publishing_splits.updated_at desc
    @composers = Composer.paginate_by_sql(
      "(SELECT DISTINCT composers.*, publishing_splits.updated_at as splits_updated_at, people.status as people_status
      FROM `composers` inner join people on composers.person_id = people.id
      inner join
      (
        select publishing_splits.composer_id, max(publishing_splits.updated_at) as updated_at
        from publishing_splits
        inner join compositions
        on publishing_splits.`composition_id` = compositions.id
        where compositions.is_unallocated_sum = 0
        group by publishing_splits.composer_id
      )
      publishing_splits on composers.id = publishing_splits.composer_id)
      UNION
      (SELECT DISTINCT composers.*, NULL as splits_updated_at, people.status as people_status
      FROM `composers` inner join people on composers.person_id = people.id
      WHERE NOT EXISTS
      (
        select publishing_splits.composer_id, max(publishing_splits.updated_at) as splits_updated_at
        from publishing_splits
        inner join compositions
        on publishing_splits.`composition_id` = compositions.id
        where composers.id = publishing_splits.composer_id
        AND compositions.is_unallocated_sum = 0
        group by publishing_splits.composer_id
      ))
      order by #{order}",
      page: options[:page],
      per_page: per_page,
      include: :person
    )
  end

  def setup_params_for_composer
    dob = params[:composer][:dob] ? params[:composer][:dob][:month] + "/" + params[:composer][:dob][:day] + "/" + params[:composer][:dob][:year] : nil
    params[:composer].merge!(dob: dob)
  end

  def setup_publisher
    @publisher = Publisher.find_or_initialize_by(
      name: publisher_params[:name],
      cae: publisher_params[:cae],
      performing_rights_organization_id: publisher_params[:performing_rights_organization_id]
    )

    @composer.publisher = @publisher
  end

  def publisher_pro_affiliation_name
    pro_id = params[:publisher][:performing_rights_organization_id]
    return PerformingRightsOrganization.find(pro_id).name if pro_id.present?
  end

  def requires_publishing_manager_role
    return if current_user.has_role?("Publishing Manager", false)

    flash[:error] = "You do not have sufficient Role to access this page"
    redirect_to admin_home_path and return
  end

  def requires_publishing_role
    unless !current_user.has_role?("Publishing Viewer", false) && !current_user.has_role?("Publishing Manager", false)
      return
    end

    flash[:error] = "You do not have sufficient Role to access this page"
    redirect_to admin_home_path and return
  end

  def setup_fields
    @from_date = 1.day.ago.to_date.strftime("%m/%d/%Y")
    @order_by_options = [["Last Splits", "splits_updated_at"], ["Agreed", "agreed_at"], ["Account Status", "account_status"]]
  end

  def rights_app_enabled?
    redirect_to edit_admin_accounts_composer_path(@composer.account, @composer)
  end

  def date_range_is_over_a_year?(options)
    return false if options[:end_date].blank? || options[:start_date].blank?

    start_date = Date.strptime(options[:start_date], "%m/%d/%Y")
    end_date = Date.strptime(options[:end_date], "%m/%d/%Y")
    (end_date - start_date).to_i > Time.days_in_year
  end
end
