class Admin::TakedownsController < Admin::AdminController
  before_action :load_album

  def create
    opts = {
      user_id: current_user.id,
      ip_address: request.remote_ip,
      subject: "Takedown",
      note: "Album was taken down",
      priority: 1
    }
    if @album.takedown!(opts)
      flash[:success] = "Succesfully marked album for takedown"
      TakedownNotifier.takedown_notification(@album.person, [{ album: @album, stores: :all }]).deliver
    else
      flash[:error] = "Could not mark album for takedown"
    end

    redirect_to admin_album_path(@album)
  end

  def destroy
    opts = {
      user_id: current_user.id,
      priority: 1
    }

    if @album.remove_takedown(opts)
      @note = Note.create(
        related: @album,
        note_created_by: current_user,
        ip_address: request.remote_ip,
        subject: "Takedown",
        note: "Album takedown was cleared"
      )
      flash[:success] = "Successfully removed the takedown"
    else
      flash[:error] = "Takedown removal failed"
    end

    redirect_to admin_album_path(@album)
  end

  private

  def load_album
    @album = Album.find(params[:album_id])
  end
end
