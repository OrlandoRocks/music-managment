class Admin::YoutubeTrackMonetizationJudgementsController < Admin::TrackMonetizationJudgementsController
  def index
    @page_title = "Youtube Money :: Approve/Reject Tracks"
  end

  private

  def load_store
    @store = Store.find_by(id: Store::YTSR_STORE_ID)
  end
end
