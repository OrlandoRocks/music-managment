class Admin::LoginEventsController < Admin::AdminController
  def index
    @person = Person.find(params[:person_id])
    @login_events = @person.login_events.order("created_at DESC")
                           .page(params[:page]).per_page(25)
  end
end
