class Admin::StoredPaypalAccountsController < Admin::AdminController
  def history
    @person_shown = Person.find(params[:person_id])
    @active_stored_paypal_account = StoredPaypalAccount.currently_active_for_person(@person_shown)
    @previous_stored_paypal_accounts = StoredPaypalAccount.where(
      "person_id = ? and archived_at is not null",
      @person_shown.id
    )
    unless @active_stored_paypal_account.nil?
      @success_total = @active_stored_paypal_account.successful_transaction_total
      @failed_total = @active_stored_paypal_account.failure_transaction_total
    end
    @paypal_transactions = @person_shown.paypal_transactions.reverse
  end
end
