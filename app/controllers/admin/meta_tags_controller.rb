class Admin::MetaTagsController < Admin::AdminController
  layout "admin_new"

  def index
    load_meta_tags
  end

  def new
    build_meta_tag
  end

  def show
    load_meta_tag
  end

  def create
    build_meta_tag
    save_meta_tag or render "new"
  end

  def edit
    load_meta_tag
    build_meta_tag
  end

  def update
    load_meta_tag
    build_meta_tag
    save_meta_tag or render "edit"
  end

  def destroy
    load_meta_tag
    @meta_tag.destroy
    redirect_to admin_meta_tags_path
  end

  private

  def load_meta_tags
    @meta_tags ||=
      if params[:country_website_id]
        meta_tag_scope.where(country_website_id: params[:country_website_id]).order("page_name ASC").to_a
      else
        meta_tag_scope.order("page_name ASC").to_a
      end
  end

  def load_meta_tag
    @meta_tag ||= MetaTag.find(params[:id])
  end

  def build_meta_tag
    @meta_tag ||= meta_tag_scope.build
    @meta_tag.update(meta_tag_params)
  end

  def save_meta_tag
    redirect_to admin_meta_tags_path if @meta_tag.save
  rescue ActiveRecord::RecordNotUnique
    @meta_tag.errors.add(:page_name, "An entry for that page already exists")
    render "new"
  end

  def meta_tag_scope
    MetaTag.includes(:country_website).where(nil)
  end

  def meta_tag_params
    params.permit(:meta_tag)
  end
end
