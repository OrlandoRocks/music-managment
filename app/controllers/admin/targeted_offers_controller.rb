class Admin::TargetedOffersController < Admin::AdminController
  include Tunecore::CsvExporter

  before_action :load_targeted_offer, except: [:index, :new, :create]
  before_action :redirect_if_not_permitted_lightbox, only: [:edit_population_settings]
  before_action :redirect_if_not_permitted, only: [:show, :new, :create, :update, :edit, :activate, :deactivate, :add_person, :remove_person, :add_people, :remove_people, :clone, :destroy]

  def index
    if !params[:person_id]
      redirect_if_not_permitted
      @page_title = "Targeted Offers"
      @targeted_offers = TargetedOffer.includes(:created_by).order("created_at")
    else
      @person = Person.find(params[:person_id])
      @page_title = "Targeted Offers for #{@person.name}"
      @targeted_people = TargetedPerson.includes(:targeted_offer).where(person_id: @person.id)
    end
  end

  def show
    load_show_variables
  end

  def clone
    @new_targeted_offer = @targeted_offer.dup
    @new_targeted_offer.save!

    redirect_to admin_targeted_offer_path(@new_targeted_offer)
  end

  def new
    @page_title = "New Targeted Offer"
    @targeted_offer = TargetedOffer.new(offer_type: "existing", created_by_id: current_user.id, start_date: Time.current.at_beginning_of_month, expiration_date: Time.current.at_end_of_month)
  end

  def create
    @tp = TargetedOffer.new(targeted_offer_params)
    if @tp.save
      redirect_to admin_targeted_offer_path(@tp)
    else
      @targeted_offer = @tp
      render action: :new
    end
  end

  def destroy
    if @targeted_offer.destroy
      redirect_to admin_targeted_offers_path
    else
      respond_to do |format|
        format.html {
          flash.now[:error] = "Cannot delete targeted offers that are active and/or used"
          load_show_variables
          render action: :show, status: :internal_server_error
        }
      end
    end
  end

  def update
    if @targeted_offer.update(targeted_offer_params)
      respond_to do |format|
        format.html {
          flash[:success] = "Succesfully updated the targeted offer"
          redirect_to action: :show, id: @targeted_offer.id
        }
      end
    else
      respond_to do |format|
        format.html { render action: "edit", status: :internal_server_error }
      end
    end
  end

  def edit
    @page_title = "Editing #{@targeted_offer.name}"
  end

  def price_list
    @targeted_offer = TargetedOffer.find(params[:id])
    @products = Product
                .with_targeted_products(targeted_offer_id: @targeted_offer.id)
                .order(:applies_to_product, :product_type)
    @products.each do |p|
      p.set_targeted_product(p.targeted_products.detect { |tp| tp.product_id == p.id && tp.targeted_offer_id == @targeted_offer.id })
      p.set_adjusted_price(p.price)
    end
    @page_title = "Product List for #{@targeted_offer.name}"
    render layout: "admin_application_lightbox"
  end

  def edit_population_settings
    load_lists
  end

  def update_population_settings
    targeted_offer_params[:exclude_targeted_offer_ids] ||= []
    @targeted_offer.update(targeted_offer_params)
    @targeted_offer.set_population_count_from_criteria
    redirect_to action: :show, id: @targeted_offer.id
  end

  def activate
    if @targeted_offer.scheduled_or_processing?
      respond_to do |format|
        format.html {
          flash.now[:error] = "Delayed job already running for this targeted offer"
          load_show_variables
          render action: :show, status: :internal_server_error
        }
      end
    else
      @targeted_offer.schedule_change_activation_job(true, current_user)

      respond_to do |format|
        format.html {
          flash[:success] = "Succesfully scheduled a job to activate the targeted offer.  You will receive an email when this job is done"
          redirect_to admin_targeted_offer_path(@targeted_offer)
        }
      end
    end
  end

  def deactivate
    if @targeted_offer.scheduled_or_processing?
      respond_to do |format|
        format.html {
          flash.now[:error] = "Delayed job already running for this targeted offer"
          load_show_variables
          render action: :show, status: :internal_server_error
        }
      end
    else
      @targeted_offer.schedule_change_activation_job(false, current_user)

      respond_to do |format|
        format.html {
          flash[:success] = "Succesfully scheduled a job to deactivate the targeted offer.  You will receive an email when this job is done"
          redirect_to admin_targeted_offer_path(@targeted_offer)
        }
      end
    end
  end

  def build_population
    @targeted_offer.customers_to_add = params[:customers_to_add]
    @targeted_offer.add_people_from_criteria(params[:rebuild])
    redirect_to admin_targeted_offer_path(@targeted_offer)
  end

  def add_person
    begin
      @person = Person.find(params[:add_person_id])

      # Skip validation that targeted_offer.add_person provides
      @new_targeted_person = @targeted_offer.targeted_people.create(person_id: @person.id)
      if @new_targeted_person.valid?
        flash[:success] = "Succesfully added person id: #{@new_targeted_person.person_id} to the targeted offer"
        redirect_to admin_targeted_offer_path(@targeted_offer)
      else
        load_show_variables
        render action: :show, status: :internal_server_error
      end
    rescue ActiveRecord::RecordNotFound
      flash.now[:error] = "Could not find person id: #{params[:add_person_id]}"
      load_show_variables
      render action: :show, status: :not_found
    end
  end

  def remove_person
    begin
      @targeted_person_to_destroy = @targeted_offer.targeted_people.find_by_person_id!(params[:remove_person_id].to_i)

      if @targeted_person_to_destroy.destroy
        if !request.xhr?
          respond_to do |format|
            format.html {
              flash[:success] = "Succesfully removed person id: #{params[:remove_person_id]}"
              redirect_to admin_targeted_offer_path(@targeted_offer)
            }
          end
        else
          respond_to do |format|
            format.js { head :ok }
          end
        end
      else
        flash.now[:error] = "Cannot remove this person because they have already used the offer"
        load_show_variables
        render action: :show, status: :internal_server_error
      end
    rescue ActiveRecord::RecordNotFound
      flash.now[:error] = "Could not find person id: #{params[:remove_person_id]}"
      load_show_variables
      render action: :show, status: :not_found
    end
  end

  def add_people
    if @targeted_offer.scheduled_or_processing?
      respond_to do |format|
        format.html {
          flash.now[:error] = "Delayed job already running for this targeted offer"
          load_show_variables
          render action: :show, status: :internal_server_error
        }
      end
    else

      @targeted_offer.upload_csv(params[:add_people_csv], @targeted_offer.add_csv_filename)
      @targeted_offer.schedule_add_people_job(current_user)

      respond_to do |format|
        format.html {
          flash[:success] = "Succesfully scheduled a job to add people to the targeted offer.  You will receive an email when this job is done"
          redirect_to admin_targeted_offer_path(@targeted_offer)
        }
      end
    end
  end

  def remove_people
    if @targeted_offer.scheduled_or_processing?
      respond_to do |format|
        format.html {
          flash.now[:error] = "Delayed job already running for this targeted offer"
          load_show_variables
          render action: :show, status: :internal_server_error
        }
      end
    else
      @targeted_offer.upload_csv(params[:remove_people_csv], @targeted_offer.remove_csv_filename)
      @targeted_offer.schedule_remove_people_job(current_user)

      respond_to do |format|
        format.html {
          flash[:success] = "Succesfully scheduled a job to remove people from the targeted offer.  You will recieve an email when the process is finished."
          redirect_to admin_targeted_offer_path(@targeted_offer)
        }
      end
    end
  end

  def targeted_people
    @targeted_people = @targeted_offer.targeted_people.includes(:person)
    # @targeted_people = @targeted_offer.targeted_people.all
    respond_to do |format|
      format.csv {
        csv = ::CSV.new(response = "", row_sep: "\r\n")
        @targeted_people.each { |tp| tp.to_csv(csv) }
        send_data(response)
        filename = "#{@targeted_offer.name.tr(' ', '_').downcase}_targeted_people.csv"
        set_csv_headers(filename)
      }
    end
  end

  private

  def targeted_offer_params
    toffer_params = params.require(:targeted_offer).permit(
      :name,
      :country_website_id,
      :offer_type,
      :join_token,
      :population_cap,
      :product_show_type,
      :date_constraint,
      :join_plus_duration,
      :start_date,
      :expiration_date,
      :usage_limit,
      :include_sidebar_cms,
      :created_by_id,
      :status,
      :country,
      :zip,
      :created_on_start,
      :created_on_end,
      :referral,
      :purchase_criteria_type,
      :last_purchase_made_start,
      :last_purchase_made_end,
      :exclude_targeted_offer_ids
    )

    toffer_params[:start_date] = DateParser.parse(toffer_params[:start_date]) if toffer_params[:start_date].present?
    if toffer_params[:expiration_date].present?
      toffer_params[:expiration_date] = DateParser.parse(toffer_params[:expiration_date])
    end

    toffer_params
  end

  def load_targeted_offer
    @targeted_offer = TargetedOffer.find(params[:id])
  end

  def load_lists
    @referral_list = [["All", "all"]] + Person.select("DISTINCT referral").order("referral").collect { |r| [r.referral] }
  end

  def load_show_variables
    @targeted_products = @targeted_offer.targeted_products
                                        .joins(:product)
                                        .order("applies_to_product, product_type, targeted_products.sort_order, products.sort_order")
    @targeted_products.each do |tp|
      tp.product.set_targeted_product(tp)
      tp.product.set_adjusted_price(tp.product.base_price)
    end
    @targeted_advertisements = @targeted_offer.targeted_advertisements.includes(:advertisement).order("advertisements.category, sort_order")
    @page_title = "Targeted Offer - #{@targeted_offer.name}"
  end

  def deploy_independent_shared_path
    Rails.root.to_s.gsub(/releases\/[0-9]+/, "current")
  end
end
