class Admin::PayoutServiceFeesController < Admin::AdminController
  before_action :load_person
  before_action :redirect_restrict_access

  def index
    @types = PayoutServiceFee::TYPES
    @payout_service_fees = []
    @types.each do |type|
      @payout_service_fees << PayoutServiceFee.current(type)
    end
  end

  def edit
    @payout_service_fee = PayoutServiceFee.find(params[:id])
  end

  def update
    @payout_service_fee = PayoutServiceFee.find(params[:id])

    if @payout_service_fee.update(payout_fees_params)
      flash[:notice] = "#{@payout_service_fee.fee_type.humanize.titleize} Fee has been updated"
      redirect_to admin_payout_service_fees_path
    else
      render action: :edit
    end
  end

  def show
    @payout_service_fee = PayoutServiceFee.find(params[:id])
    @payout_service_fee_history = PayoutServiceFee.fee_type(@payout_service_fee.fee_type).order("created_at DESC, archived_at ASC")
  end

  protected

  def redirect_restrict_access
    return if current_user.is_permitted_to?(params[:controller], params[:action])

    flash[:notice] = "You don't have permission to access that page"
    redirect_to admin_home_path
  end

  def payout_fees_params
    params.require(:payout_service_fee).permit(:amount)
  end
end
