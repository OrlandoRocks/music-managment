class Admin::ContentController < Admin::AdminController
  def images
    path = CMS_FULL_IMAGES_PATH
    @files = []
    if File.exist?(path) && File.directory?(path)
      Dir.glob(path + "*.*").map do |x|
        @files << [x.split("/").pop, File.new(x).mtime]
      end
    else
      flash[:error] = "CMS images directory #{CMS_FULL_IMAGES_PATH} cannot be accessed, please contact tech."
    end
  end

  def upload
    upload = params[:upload][:image] rescue nil

    if ["image/jpeg", "image/png", "image/gif"].include?(upload.content_type)
      begin
        File.open(
          CMS_FULL_IMAGES_PATH +
                          upload.original_filename,
          "wb"
        ) { |f| f.write(upload.read) }
      rescue
        flash[:error] = "Problem writing uploaded file #{upload.original_filename}"
      end
    else
      flash[:error] = "Wrong filetype! Must be a gif, jpeg or png."
    end

    redirect_to action: "images"
  end

  def image_delete
    image_filename = params[:image] rescue nil
    begin
      File.delete(CMS_FULL_IMAGES_PATH + image_filename)
    rescue
      flash[:error] = "Unable to delete file #{image_filename}"
    end

    redirect_to action: "images"
  end
end
