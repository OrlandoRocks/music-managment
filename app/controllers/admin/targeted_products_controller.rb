# [2010-06-17 -- CH]
# Created Controller

class Admin::TargetedProductsController < Admin::AdminController
  include ProductHelper
  include FetchSingleYearProducts
  include TargetedProductStoreOffer
  layout "admin_application_lightbox"

  def new
    @targeted_offer = TargetedOffer.find(params[:targeted_offer_id])
    @targeted_product = TargetedProduct.new(targeted_offer: @targeted_offer)
    load_targeted_products
  end

  def create
    if targeted_product_params[:product_id] == "all_products"
      TargetedProduct.add_all_products(targeted_product_params.except(:product_id))
    else
      targeted_product = TargetedProduct.create(targeted_product_params)
      update_targeted_product_stores(targeted_product)
    end
  end

  def edit
    @targeted_offer = TargetedOffer.find(params[:targeted_offer_id])
    @targeted_product = TargetedProduct.find(params[:id])
    load_product_list(@targeted_offer.country_website)
  end

  def update
    TargetedProduct.find(params[:id]).tap do |targeted_product|
      ActiveRecord::Base.transaction do
        targeted_product.update(targeted_product_params)
        update_targeted_product_stores(targeted_product)
      end
    end
  end

  def destroy
    targeted_product = TargetedProduct.find(params[:id])
    targeted_product.destroy
    redirect_to admin_targeted_offer_path(targeted_product.targeted_offer)
  end

  private

  def targeted_product_params
    params.require(:targeted_product).permit(
      :product_id,
      :display_name,
      :flag_text,
      :price_adjustment_type,
      :price_adjustment,
      :price_adjustment_description,
      :show_expiration_date,
      :sort_order,
      :targeted_offer_id,
      :renewals_past_due,
      :renewals_due,
      :renewals_upcoming
    )
  end

  def load_product_list(country_website, skip = true)
    skip_products = []
    @targeted_offer.targeted_products.each { |tp| skip_products << tp.product_id } if skip
    @product_list = product_list_for_select(country_website, skip_products)
  end

  def load_targeted_products
    load_product_list(@targeted_offer.country_website)

    all_products = [
      "ALL PRODUCTS",
      :all_products
    ]

    @product_list.unshift(all_products) if @targeted_offer.targeted_products.blank?
  end
end
