class Admin::Accounts::ComposersController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :set_eligible_publishing_pro_ids

  def edit
    @account = Account.find_by(id: params[:account_id])
    @composer = @account.composers.find_by(id: params[:id])
    @composer.build_publisher if @composer.publisher.nil?
  end

  def update
    @account  = Account.find_by(id: params[:account_id])
    @composer = @account.composers.find_by(id: params[:id])
    @composer.create_or_update_publisher(publisher_params[:publisher_attributes])

    if @composer.update(composer_params)
      ::PublishingAdministration::WriterCreationWorker.perform_async(@composer.id)
      flash[:success] = "#{@composer.full_name} was successfully updated"

      redirect_to edit_admin_accounts_composer_path(@account, @composer)
    else
      flash[:error] = "#{@composer.full_name} was not able to be updated"
      render :edit
    end
  end

  def terms_of_service
    @account = Account.find_by(id: params[:account_id])
    @composer = @account.composers.find_by(id: params[:composer_id])

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Terms of Service - Account ##{@account.id} - Composer ##{@composer.id}",
               page_size: "A4",
               template: "admin/accounts/composers/_terms_of_service.html.erb",
               layout: "pdf.html",
               lowquality: true,
               zoom: 1,
               dpi: 75
      end
    end
  end

  private

  def composer_params
    params
      .require(:composer)
      .permit(
        :cae,
        :dob,
        :first_name,
        :last_name,
        :middle_name,
        :name_prefix,
        :name_suffix,
        :performing_rights_organization_id,
        :provider_identifier,
      )
  end

  def publisher_params
    params.require(:composer).permit(publisher_attributes: [:name, :cae, :performing_rights_organization_id])
  end

  def set_eligible_publishing_pro_ids
    gon.eligble_publishing_pro_ids = PerformingRightsOrganization.eligible_publishing_pro_ids
  end
end
