class Admin::Accounts::PublishingComposersController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :set_eligible_publishing_pro_ids

  def edit
    @account = Account.find_by(id: params[:account_id])
    @publishing_composer = @account.publishing_composers.find_by(id: params[:id])
    @publishing_composer.build_publisher if @publishing_composer.publisher.nil?
  end

  def update
    @account = Account.find_by(id: params[:account_id])
    @publishing_composer = @account.publishing_composers.find_by(id: params[:id])
    @error = Admin::PublishingComposerUpdaterService.new(
      @publishing_composer,
      publishing_composer_params,
      publisher_params[:publisher_attributes]
    ).update

    if @error.nil?
      flash[:success] = "PublishingComposer #{@publishing_composer.id} was successfully updated"
      redirect_to edit_admin_accounts_publishing_composer_path(@account, @publishing_composer)
    else
      flash[:success] = nil
      flash[:error] = @error[:message]
      render :edit
    end
  end

  def terms_of_service
    @account = Account.find_by(id: params[:account_id])
    @publishing_composer = @account.publishing_composers.find_by(id: params[:publishing_composer_id])

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Terms of Service - Account ##{@account.id} - PublishingComposer ##{@publishing_composer.id}",
               page_size: "A4",
               template: "admin/accounts/publishing_composers/_terms_of_service.html.erb",
               layout: "pdf.html",
               lowquality: true,
               zoom: 1,
               dpi: 75
      end
    end
  end

  def update_lod
    account = Account.find_by(id: params[:account_id])
    publishing_composer = account.publishing_composers.find_by(id: params[:publishing_composer_id])
    DocuSign::LodApi.update_legal_document(account.person, publishing_composer, envelope_params[:envelope_id])
    redirect_to edit_admin_accounts_publishing_composer_path(account, publishing_composer)
  end

  private

  def publishing_composer_params
    params
      .require(:publishing_composer)
      .permit(
        :cae,
        :dob,
        :first_name,
        :last_name,
        :middle_name,
        :name_prefix,
        :name_suffix,
        :performing_rights_organization_id,
        :provider_identifier,
      )
  end

  def publisher_params
    params.require(:publishing_composer).permit(publisher_attributes: [:name, :cae, :performing_rights_organization_id])
  end

  def envelope_params
    params.permit(:envelope_id)
  end

  def set_eligible_publishing_pro_ids
    gon.eligble_publishing_pro_ids = PerformingRightsOrganization.eligible_publishing_pro_ids
  end
end
