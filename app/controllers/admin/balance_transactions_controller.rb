class Admin::BalanceTransactionsController < Admin::AdminController
  layout "tc_foundation_admin"
  include PayoutProvidable

  before_action :redirect_to_legacy_transactions_list_page!, unless: :payoneer_payout_enabled?

  def index
    @page_title                = "Transactions".freeze
    @person                    = Person.find(params[:id])
    @balance_history_presenter = BalanceHistoryPresenter.new(balance_transaction_params)
  end

  private

  def balance_transaction_params
    params.merge(person: @person, page: PersonTransaction::ADMIN_PAGINATION_LIMIT)
  end
end
