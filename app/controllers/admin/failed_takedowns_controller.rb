class Admin::FailedTakedownsController < Admin::AdminController
  def index
    report                        = FailedTakedowns.new
    @grace_period_cutoff_date     = report.grace_period_cutoff_date
    @failed_takedowns             = report.failed_takedowns.paginate(page: params[:page] || 1, per_page: 50)
    gon.failed_takedown_album_ids = report.album_ids_strings
  end
end
