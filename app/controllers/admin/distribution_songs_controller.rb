class Admin::DistributionSongsController < Admin::AdminController
  def retry_distribution_song
    @distro_song = DistributionSong.find(params[:id])
    @album = Album.find(params[:album_id])
    @distro_song.delivery_type = params[:delivery_type] if params[:delivery_type]

    @distro_song.retry actor: "Tunecore admin: #{current_user.name}", message: "Retrying distribution_song #{@distro_song.id} (#{@distro_song.delivery_type})"

    @note = Note.create(related: @album, note_created_by: current_user, ip_address: request.remote_ip, subject: "DistributionSong", note: "Retrying distribution #{@distro_song.id} (#{@distro_song.delivery_type})")
    @distro_song.reload
    @distro = @distro_song.distribution

    respond_to do |format|
      format.html {
        render partial: "admin/albums/distribution_song", locals: { ds: @distro_song, distro: @distro, album: @album }
      }
    end
  end
end
