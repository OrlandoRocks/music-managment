class Admin::DistributionReportsController < Admin::AdminController
  before_action :requires_reporting_role

  def index
    @page_title = "US Distribution Report - Last 30 Days"
    @report = DistributionReport.new(
      {
        resolution: DistributionReport::DAILY,
        range: "last_30_days",
        data_field_name: "album_type",
        headers: ["Album", "Single", "Ringtone"],
        country: "US"
      }
    )
    @headers = @report.data_table.headers
  end

  def canada
    @page_title = "CA Distribution Report - Last 30 Days"
    @report = DistributionReport.new(
      {
        resolution: DistributionReport::DAILY,
        range: "last_30_days",
        data_field_name: "album_type",
        headers: ["Album", "Single", "Ringtone"],
        country: "CA"
      }
    )
    @headers = @report.data_table.headers

    render "index"
  end

  private

  def requires_reporting_role
    redirect_to admin_home_path and return unless current_user.has_role?("Report Viewer", false)
  end
end
