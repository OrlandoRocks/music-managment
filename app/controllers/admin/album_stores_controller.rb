class Admin::AlbumStoresController < Admin::AdminController
  include AdminResource

  before_action :check_admin_login
  before_action :load_person

  def edit
    set_variables_for_edit_action
  end

  def update
    @store = Store.find(params[:store_id])
    @albums = @person.albums.where("id in (?)", params[:album_ids])

    BulkAddAlbumsToStores.add(@store, @albums)

    set_variables_for_edit_action
    render action: "edit"
  end

  protected

  def set_variables_for_edit_action
    @release_class = params[:release_type].blank? ? Album : params[:release_type].constantize
    @albums        = @person.albums.where(
      "album_type = ? and takedown_at IS NULL and is_deleted = 0 and finalized_at is not null", @release_class.name
    )
  end
end
