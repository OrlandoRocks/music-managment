class Admin::SystemPropertiesController < Admin::AdminController
  def index
    list
    render action: "list"
  end

  def list
    @system_properties = SystemProperty.paginate page: params[:page], per_page: 10
  end

  def show
    @system_property = SystemProperty.find(params[:id])
  end

  def new
    @system_property = SystemProperty.new
  end

  def create
    @system_property = SystemProperty.new(params[:system_property])
    if @system_property.save
      flash[:notice] = "SystemProperty was successfully created."
      redirect_to action: "list"
    else
      render action: "new"
    end
  end

  def edit
    @system_property = SystemProperty.find(params[:id])
  end

  def update
    @system_property = SystemProperty.find(params[:id])
    if @system_property.update(params[:system_property])
      flash[:notice] = "SystemProperty was successfully updated."
      redirect_to action: "show", id: @system_property
    else
      render action: "edit"
    end
  end

  def destroy
    SystemProperty.find(params[:id]).destroy
    redirect_to action: "list"
  end
end
