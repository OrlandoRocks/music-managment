class Admin::TrackBlocksController < Admin::AdminController
  def create
    Admin::TrackBlockService.send_track(track_params)
  end

  def track_params
    {
      person: current_user.name,
      song_id: params[:song_id],
      store_id: params[:store_id]
    }
  end
end
