class Admin::MassRetrySongsController < Admin::AdminController
  def show
    @takedown = params[:takedown]
    @page_title =
      if @takedown
        "Mass Takedown Tool"
      else
        "Mass Distribution Retry Tool"
      end
    @mass_retry_songs_form = Admin::MassRetrySongsForm.new
  end

  def create
    mass_retry_songs_form = Admin::MassRetrySongsForm.new(mass_retry_song_params)
    if mass_retry_songs_form.save
      songs = mass_retry_songs_form.send(:songs)
      flash[:notice] = "Songs are being retried successfully"
    else
      flash[:error] = "Something went wrong"
    end

    redirect_to admin_mass_retry_songs_path(takedown: mass_retry_song_params[:takedown])
  end

  def encumbrance_info
    mass_retry_songs_form = Admin::MassRetrySongsForm.new(mass_retry_song_params)
    songs = mass_retry_songs_form.send(:songs)
    render json: find_songs_with_encumbrance(songs), status: :ok
  end

  private

  def song_ids_by_album(songs)
    hsh = Hash.new { |hsh, key| hsh[key] = Set.new }
    songs.each_with_object(hsh) do |song, obj|
      obj[song.album_id].add(song.id)
    end.transform_values!(&:to_a)
  end

  def mass_retry_song_params
    params
      .require(
        :admin_mass_retry_songs_form
      ).permit(
        :delivery_type,
        :song_ids_and_isrcs,
        :takedown,
        store_ids: []
      ).merge(
        admin: current_user
      )
  end

  def params_to_store_ids(params)
    params[:store_ids] || params.dig(:admin_mass_retry_songs_form, :store_ids)
  end

  def find_songs_with_encumbrance(songs)
    song_ids_or_isrcs = params[:admin_mass_retry_songs_form][:song_ids_and_isrcs]
    {
      song_ids_and_isrcs: song_ids_or_isrcs.split(", ")
                                           .map { |song_id|
                            songs.map { |song|
                              song_id if person_has_encumbrance?(song, song_id)
                            }
                          }.flatten.compact.uniq
    }
  end

  def person_has_encumbrance?(song, song_id)
    song.person.has_encumbrance? &&
      (song.id.to_s == song_id || song.tunecore_isrc == song_id || song.optional_isrc == song_id)
  end
end
