class Admin::BalanceAdjustmentsController < Admin::AdminController
  layout "admin_application_lightbox", except: :index

  before_action :load_rights, only: [:new, :rollback]
  before_action :redirect_if_cant_refund, only: [:create]

  def index
    @person = Person.find(params[:person_id])
    @balance = @person.person_balance.balance
    @balance_adjustments = BalanceAdjustment
                           .includes(:related)
                           .where(["person_id = ? AND rollback =?", @person.id, false])
                           .order("created_at DESC")
                           .paginate(page: params[:page], per_page: (params[:per_page] || 10).to_i)

    render action: "index", layout: "admin"
  end

  def new
    @person = Person.find(params[:person_id])
    @balance_adjustment = BalanceAdjustment.new
    load_form_variables
  end

  def rollback
    @person = Person.find(params[:person_id])
    @balance_adjustment = BalanceAdjustment.new
    load_rollback_form_variables
  end

  def create
    @person = Person.find(params[:person_id])
    @rollback = params[:balance_adjustment][:rollback]
    @balance_adjustment = BalanceAdjustment.create(
      balance_adjustment_params.merge(
        person_id: @person.id,
        posted_by_id: current_user.id,
        posted_by_name: current_user.name
      )
    )

    return if @balance_adjustment.valid?

    if @rollback
      load_rollback_form_variables
      render action: :rollback
    else
      load_form_variables
      render action: :new
    end
  end

  private

  def balance_adjustment_params
    params
      .require(:balance_adjustment)
      .permit(
        :related_id,
        :rollback,
        :debit_amount,
        :credit_amount,
        :currency,
        :category,
        :customer_note,
        :admin_note,
        :credit_from_form,
        :debit_from_form,
        :disclaimer
      )
  end

  def load_rights
    @user_can_refund = current_user.has_role?("Refunds")
  end

  def redirect_if_cant_refund
    redirect_to controller: :transactions,
                action: :no_permission,
                layout: "admin_application_lightbox" unless current_user.has_role?("Refunds")
  end

  def load_form_variables
    @categories = BalanceAdjustment::CATEGORY_LIST
    @balance = @person.person_balance.balance
    if params[:credit] == "true"
      @credit = true
      @label = "Credit"
    else
      @credit = false
      @label = "Debit"
    end
  end

  def load_rollback_form_variables
    @balance = @person.person_balance.balance
    @original_adjustment = BalanceAdjustment.find(params[:balance_adjustment][:related_id])
    if @original_adjustment.credit_amount.positive?
      @label = "Debit"
      @amount = (@original_adjustment.credit_amount > @balance) ? @balance : @original_adjustment.credit_amount
    else
      @label = "Credit"
      @amount = @original_adjustment.debit_amount
    end
  end
end
