class Admin::TwoFactorAuthController < Admin::AdminController
  before_action :tfa_authorized?, :load_tfa

  def update
    if @tfa.try(:disable!)
      flash[:notice] = "Successfully disabled"
    else
      flash[:error] = "Unable to disable user's two-factor authentication. "
    end
    redirect_to admin_person_path(@tfa.person)
  end

  private

  def tfa_authorized?
    return if current_user.tfa_admin?

    flash[:alert] = "You are not authorized to do this."
    redirect_back fallback_location: home_path
  end

  def load_tfa
    @tfa = Person.find(params[:id]).two_factor_auth
  end
end
