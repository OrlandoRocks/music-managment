class Admin::YoutubeMoniesController < Admin::AdminController
  include YoutubeHelper

  before_action :redirect_if_no_ytm, only: [:setup_ytm_opt_out, :opt_out]
  before_action :require_ytm_approver

  def setup_ytm_opt_out
    setup_ytm_opt_out_form
    render layout: "admin_application_lightbox"
  end

  def opt_out
    note = {
      note_created_by: current_user,
      ip_address: request.remote_ip,
      note: params[:note].try(:note)
    }
    return unless params[:person][:action_type] == "disable"

    if person.terminate_ytm(note, "Tunecore Admin: #{current_user.name}")
      @action_title = "YTM Disabled for #{person.name}"
    else
      setup_ytm_opt_out_form
      render action: "setup_ytm_opt_out"
    end
  end

  def block
    note = {
      note_created_by: current_user,
      ip_address: request.remote_ip
    }
    person.block_ytm(note, current_user.email)
    redirect_to admin_person_path(person.id)
  end

  def unblock
    note = {
      note_created_by: current_user,
      ip_address: request.remote_ip
    }
    person.unblock_ytm(note, current_user.email)
    redirect_to admin_person_path(person.id)
  end

  def mass_takedown
  end

  def mass_takedown_tracks
    isrcs = params[:isrcs].split(" ")
    @songs = Song.where(["songs.tunecore_isrc IN (?) or songs.optional_isrc IN (?)", isrcs, isrcs])
    begin
      failed_song_isrcs = takedown_track_monetizations(@songs)

      create_takedown_notes_for_valid_isrcs(isrcs - failed_song_isrcs)

      flash[:success] = "Successfully took down the tracks" unless isrcs.length == failed_song_isrcs.length
      if failed_song_isrcs.present?
        flash[:error] = "Cannot takedown a song from ytsr that was not distributed. Failed takedown isrcs: #{failed_song_isrcs.join(', ')}"
      end
    rescue => e
      flash[:error] = e.message
    end
    redirect_to admin_youtube_monies_mass_takedown_path
  end

  def create_takedown_notes_for_valid_isrcs(isrcs)
    Song.by_isrc(isrcs).each do |song|
      create_ytsr_track_takedown_note(song)
    end
  end

  private

  def person
    @person ||= Person.find(params[:person_id])
  end

  def redirect_if_no_ytm
    return if person.has_ytsr_proxy?

    @legacy_ytm = person.youtube_monetization
    if @legacy_ytm.nil?
      flash[:error] = "Customer does not have the YouTube #{t(:monetiz).capitalize}ation Product or YTM Release"
      redirect_to admin_home_url
    elsif @legacy_ytm.effective_date.nil?
      flash[:error] = "Customer does not have an active YouTube #{t(:monetiz).capitalize}ation Product or YTM Release"
      redirect_to admin_home_url
    end
  end

  def setup_ytm_opt_out_form
    return unless person.has_active_ytm?

    @action_title = "Disable YTM"
    @action_type = "disable"
  end

  def require_ytm_approver
    return if current_user.has_role?("YTM Approver")

    flash[:error] = "You don't have the YTM Approver role"
    redirect_to dashboard_path
  end
end
