class Admin::AdminController < ApplicationController
  layout "admin"

  skip_before_action :login_required
  before_action      :login_or_oauth_required
  before_action      :authorized?

  skip_before_action :alert_rejections
  before_action      :set_admin_site_flag
  before_action      :check_if_2fa_enabled, if: :force_admin_2fa?

  include QaToolsHelper

  def set_admin_site_flag
    @admin_site = true
  end

  protected

  def login_or_oauth_required
    session[:return_to] = request.fullpath
    super
  end

  def authorized?
    return if super && current_user.try(:is_administrator?)

    current_user.lock_account && current_user.save
    logout
  end
end
