class Admin::YoutubeSrMailTemplateBuildersController < Admin::AdminController
  def index
    @email_types = YoutubeSrMailTemplate.types_for_select
  end

  def new
    @songs_by_person = Ytsr::MonetizedIsrcSearch.search(params[:isrc_string])
    @email_template  = YoutubeSrMailTemplate.return_template(params[:email_type])
  end

  def create
    template = YoutubeSrMailTemplate.return_template(params[:email_type_input])
    builder  = YoutubeSrMailTemplate::EmailBuilder.new(template, params.permit![:email_data].to_h)

    respond_to do |format|
      if builder.process
        @person_ids_processed   = builder.processed_people
        @song_isrcs_processed   = builder.processed_isrcs
        @message                = "Emails have been queued up for the following isrcs: #{@song_isrcs_processed.join(', ')}."

        format.js   { render "create.js.erb"                                   }
        format.html { redirect_to admin_youtube_sr_mail_template_builders_path }
      else
        format.js   { render "fail.js.erb"                                     }
        format.html { redirect_to admin_youtube_sr_mail_template_builders_path }
      end
    end
  end
end
