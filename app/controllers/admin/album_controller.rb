class Admin::AlbumController < Admin::AdminController
  include AdminResource

  before_action :check_admin_login
  # skip_before_action :load_person

  prepend_view_path("views/albums")
end
