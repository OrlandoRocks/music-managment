class Admin::SongDistributionOptionsController < Admin::AdminController
  before_action :load_distribution_option
  after_action  :create_note

  respond_to :js

  def update
    @song_distribution_option.option_value = params[:song_distribution_option_value]
    @option = params[:song_distribution_option_name] if @song_distribution_option.save
    respond_with @song_distribution_option
  end

  private

  def load_distribution_option
    @song_distribution_option = SongDistributionOption.find_or_create_by(
      option_name: params[:song_distribution_option_name], song_id: params[:song_id]
    )
  rescue ActiveRecord::StatementInvalid, ActiveRecord::RecordNotFound
    render status: :not_found
  end

  def create_note
    SongDistributionOption::NoteBuilder.new(
      option: params[:song_distribution_option_name],
      song: @song_distribution_option.song,
      user: current_user,
      user_input: params[:note]
    ).note.save
  end
end
