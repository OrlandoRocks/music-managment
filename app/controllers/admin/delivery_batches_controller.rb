class Admin::DeliveryBatchesController < Admin::AdminController
  include Admin::DeliveryBatchesHelper
  layout "tc_foundation_admin"

  def index
    @active_batches = BatchManager::DeliveryBatchInProgressQueryBuilder.build.paginate(page: params[:active])
    @incomplete_batches = BatchManager::DeliveryBatchIncompleteQueryBuilder.build.paginate(page: params[:incomplete])
  end
end
