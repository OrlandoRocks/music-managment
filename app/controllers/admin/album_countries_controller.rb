class Admin::AlbumCountriesController < Admin::AdminController
  before_action :load_album
  before_action :load_countries

  def edit
    @relation_service_form = AlbumCountry::RelationTypeServiceForm.new(
      album: @album,
      iso_codes: []
    )
  end

  def update
    @relation_service_form = AlbumCountry::RelationTypeServiceForm.new(
      album: @album,
      iso_codes: params[:album_country_relation_type_service_form][:iso_codes]
    )

    if @relation_service_form.save
      flash[:notice] = "Countries Saved #{params}"
      redirect_to admin_album_path(@album)
    else
      flash[:error] = "Countries Not Saved"
      render :edit
    end
  end

  private

  def load_countries
    @countries              ||= Country::COUNTRIES
    @selected_country_codes ||= @album.country_iso_codes
  end

  def load_album
    @album ||= Album.find(params[:album_id])
  end
end
