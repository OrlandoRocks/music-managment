class Admin::EftBatchTransactionsController < Admin::AdminController
  before_action :load_person
  before_action :load_eft_batch_transaction_and_restrict_access, only: [:approve, :reject, :reject_form, :remove_approval]
  before_action :load_multiple_transactions_and_restrict_access, only: :edit_multiple
  before_action :store_location, except: :index

  # the index action controls view for all eft_transactions, and a person's individual eft_transactions
  # the differences between the two are shown/hidden in the views based on the @person_page variable
  def index
    set_search_defaults

    @search =
      if @person.blank?
        EftBatchTransaction.search(permitted_index_params[:search])
      else
        @person.eft_batch_transactions.search(permitted_index_params[:search])
      end

    @search.relation = @search.relation.order(permitted_index_params.dig(:search, :order))

    @eft_batch_transactions = @search.paginate(
      page: params[:page],
      per_page: (params[:per_page] || @per_page).to_i
    ).includes([{ stored_bank_account: [person: :artist] }, :payout_service_fee])
    @pending_approval_count = EftBatchTransaction.pending_approval.count
    return if @eft_batch_transactions.empty?

    statuses = ["pending_approval", "waiting_for_batch"]
    @has_cancelable_transaction = @eft_batch_transactions.any? { |item| statuses.include?(item.status) }
    @all_failures = @eft_batch_transactions.collect(&:status).uniq == ["failure"]
  end

  def approval_summary
    @eft_approvals = EftBatchTransaction.approval_summary
  end

  def show
    @eft_batch_transaction = EftBatchTransaction.includes(:eft_batch_transaction_history).find(params[:id])
    @eft_batch_transaction_history = @eft_batch_transaction.eft_batch_transaction_history
  end

  def new
    @eft_batch_transaction = EftBatchTransaction.new
  end

  def edit_multiple
    case params[:approve]
    when "Approve"
      @eft_batch_transactions.each { |eft| eft.approve(current_user) }
      flash[:notice] = "The EFT Transactions you selected have been approved and will be sent to the bank with the next batch"
    when "Reject"
      @eft_batch_transactions.each { |eft| eft.reject(current_user) }
      flash[:notice] = "The EFT Transactions you selected have been rejected. The Debit Amounts and Service Fees have been refunded to the customers."
    when "Change to Pending"
      @eft_batch_transactions.each { |eft| eft.remove_approval(current_user) }
      flash[:notice] = "The EFT Transactions you selected have been changed to 'pending approval'. These will NOT be sent to the bank until re-approved."
    else
      flash[:error] = "Something went wrong so nothing has been edited"
    end
    redirect_back_or_default(admin_eft_batch_transactions_path)
  end

  def approve
    @eft_batch_transaction.approve(current_user)
    if @eft_batch_transaction.status == "waiting_for_batch"
      flash[:notice] = "EFT Transaction has been approved and will be sent to the bank with the next batch"
    else
      flash[:error] = "Could not approve this transaction"
    end
    redirect_back_or_default(admin_eft_batch_transactions_path)
  end

  def remove_approval
    @eft_batch_transaction.remove_approval(current_user)
    if @eft_batch_transaction.status == "pending_approval"
      flash[:notice] = "EFT Transaction has been changed to a status of 'pending_approval'. This will NOT be sent to the bank until re-approved."
    else
      flash[:error] = "Could not approve this transaction"
    end
    redirect_back_or_default(admin_eft_batch_transactions_path)
  end

  def reject_form
    render action: "reject_form", layout: "admin_application_lightbox"
  end

  def reject
    message = nil
    @eft_batch_transaction.reject(current_user, message, params[:note][:note])
    if @eft_batch_transaction.status == "rejected"
      flash[:notice] = "EFT Transaction has been rejected and the Debit Amount and Service Fee have been refunded to the customer"
    else
      flash[:error] = "Could not reject this transaction"
    end
    render action: "reject", layout: "admin_application_lightbox"
  end

  private

  def set_search_defaults
    params[:search] ||= {}
    params[:search][:stored_bank_account_person_id_equals] = params[:person_id] if params[:person_id].present?
    params[:search][:order] = "created_at ASC" if params[:search][:order].blank?
    @per_page = 20
    @person_page = params[:person_id].present?
    @person_shown = Person.find(params[:person_id]) if @person_page
  end

  def store_location
    session[:return_to] = request.referrer
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def load_eft_batch_transaction_and_restrict_access
    @eft_batch_transaction = EftBatchTransaction.find(params[:id])
    unless !current_user.is_permitted_to?(params[:controller], params[:action]) ||
           current_user == @eft_batch_transaction.person

      return
    end

    flash[:notice] = "You don't have permission to access that page or action"
    redirect_to admin_home_path
  end

  def load_multiple_transactions_and_restrict_access
    if params[:transaction_ids] && params[:transaction_ids].any?
      @eft_batch_transactions = EftBatchTransaction.find(params[:transaction_ids])
      if !current_user.is_permitted_to?(params[:controller], params[:action]) || @eft_batch_transactions.collect { |eft| eft.person.id }.include?(current_user.id)
        flash[:error] = "You don't have permission to access that action or you attempted to edit your own withdrawal"
        redirect_to admin_home_path
      end
    else
      flash[:error] = "You must select at least one transaction to use the update multiple action"
      redirect_back_or_default(admin_eft_batch_transactions_path)
    end
  end

  def load_person
    @person = Person.find(params[:person_id]) if params[:person_id]
  end

  def permitted_index_params
    params.permit(
      :per_page,
      :commit,
      search: [
        :person_status,
        :person_email,
        :person_name,
        :artist_name,
        :id,
        :order,
        :order_id,
        :eft_status,
        :eft_batch_id,
        :eft_query_id,
        :amount_min,
        :amount_max,
        :created_at_min,
        :created_at_max
      ]
    )
  end
end
