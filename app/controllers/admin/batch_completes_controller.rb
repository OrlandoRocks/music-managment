class Admin::BatchCompletesController < Admin::AdminController
  def create
    delivery_batch = DeliveryBatch.find(params[:delivery_batch_id])
    delivery_batch.update(currently_closing: true)
    Delivery::BatchCompleteWorker.perform_async(delivery_batch.batch_id)
    @incomplete_batches = BatchManager::DeliveryBatchIncompleteQueryBuilder.build.paginate(page: params[:incomplete])
  end
end
