class Admin::PayoutProvidersController < Admin::AdminController
  before_action :user_can_view_payoneer_info

  layout "admin_new"

  def index
    @accounts = PayoutProvider
                .joins(:person)
                .where(provider_identifier: params[:search])
  end

  private

  def user_can_view_payoneer_info
    return if payoneer_payout_enabled?

    flash[:error] = "you do not have permission to view payoneer payout credentials"
    redirect_to admin_home_path
  end
end
