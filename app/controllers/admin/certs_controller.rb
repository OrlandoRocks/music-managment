class Admin::CertsController < Admin::AdminController
  def lookup
    @page_title = "Certs Lookup"
    @certs = Cert.where("cert like ?", "%#{params[:cert]}%").preload([:purchase, :person]).paginate(
      page: params[:page], per_page: 500
    )
  end

  def new_redemption
    @cert = Cert.new
    render layout: "admin_application_lightbox"
  end

  # redemption on behalf of customer
  # FIXME: Should not increate attempt count when doing this as admin
  def redeem
    @person = Person.find(params[:person_id])

    opts = {
      product_redemption: true,
      current_user: @person,
      entered_code: params[:cert]
    }

    @verify_result = Cert.verify(opts)

    case @verify_result
    when Cert
      begin
        @cert = @verify_result
        @cert.redeem!

        note = "Redeeming #{@cert.cert} on behalf of customer"
        Note.create(
          related: @person,
          note_created_by: current_user,
          ip_address: request.remote_ip,
          subject: "Cert Redemption",
          note: note
        )

        flash[:notice] = "Successfully redeemed code #{@cert.cert} for customer #{@person.name}"
        redirect_to redemption_success_admin_certs_path
      rescue Tunecore::Certs::UnableToCheckoutException => e
        flash[:error] = "The checkout process for this redemption failed. This is the message returned: #{e.message}"
      end
    when String
      # an error message was returned
      flash.now[:error] = @verify_result
      @cert = Cert.new
      render action: :new_redemption, layout: "admin_application_lightbox"
    else
      flash.now[:error] = "Unexpected result from cert verification."
      @cert = Cert.new
      render action: :new_redemption, layout: "admin_application_lightbox"
    end
  end

  def redemption_success
    render plain: flash[:notice]
  end
end
