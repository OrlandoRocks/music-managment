class Admin::EftBatchesController < Admin::AdminController
  before_action :load_person

  def index
    @per_page = 10
    @eft_batches = EftBatch.includes(:eft_batch_transactions).order("created_at DESC")
                           .paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
  end

  def show
    @has_cancelable_transaction = false
    @person_page = false
    @per_page = 20
    @eft_batch = EftBatch.includes(:eft_batch_transactions).find(params[:id])
    @eft_batch_transactions = @eft_batch.eft_batch_transactions
                                        .includes(:payout_service_fee, stored_bank_account: :person)
                                        .paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
  end
end
