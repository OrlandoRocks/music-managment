class Admin::InventoryAdjustmentsController < Admin::AdminController
  layout "admin_application_lightbox"

  before_action :load_person
  before_action :load_form_variables

  before_action :redirect_if_cant_refund, only: [:create]

  def new
    @inventory_adjustment = InventoryAdjustment.new
  end

  def rollback
    @inventory_adjustment = InventoryAdjustment.new
  end

  def index
    redirect_to admin_person_inventories_path(@person)
  end

  def create
    @rollback = inventory_adjustment_params[:rollback]
    @inventory_adjustment = InventoryAdjustment.new(
      inventory_adjustment_params.merge(
        inventory_id: @inventory.id,
        posted_by_id: current_user.id,
        posted_by_name: current_user.name
      )
    )

    return if @inventory_adjustment.save

    load_form_variables
    if @rollback.nil?
      render action: :new
    else
      render action: :rollback
    end
  end

  private

  def inventory_adjustment_params
    params[:inventory_adjustment].permit(
      :related_id,
      :rollback,
      :amount,
      :category,
      :admin_note,
      :inventory_id,
      :disclaimer
    )
  end

  def load_person
    @person = Person.find(params[:person_id])
  end

  def load_form_variables
    @inventory = Inventory.where(
      id: params[:inventory_id] || inventory_adjustment_params[:inventory_id],
      person_id: @person.id
    ).first
    @categories = InventoryAdjustment::ADJUSTMENT_CATEGORIES
    unless params[:inventory_adjustment] && inventory_adjustment_params && inventory_adjustment_params[:related_id]
      return
    end

    @original_adjustment = InventoryAdjustment.find_by(id: inventory_adjustment_params[:related_id])
  end

  def redirect_if_cant_refund
    redirect_to controller: :transactions,
                action: :no_permission,
                layout: "admin_application_lightbox" unless current_user.has_role?("Refunds")
  end
end
