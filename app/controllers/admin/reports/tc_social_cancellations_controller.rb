class Admin::Reports::TcSocialCancellationsController < Admin::AdminController
  def index
    @cancellation_report_form = TcSocialCancellationsReportForm.new(report_params)
    respond_to do |format|
      format.html do
        @reporter = TcSocialCancellationReporter.build_paginated_report(
          page: params[:page],
          report_form: @cancellation_report_form
        )
      end
      format.csv do
        send_data TcSocialCancellationReporter.build_csv_report(report_form: @cancellation_report_form).report
      end
    end
  end

  private

  def report_params
    {
      person_id: params.try(:[], "tc_social_cancellations_report_form").try(:[], "person_id"),
      person_email: params.try(:[], "tc_social_cancellations_report_form").try(:[], "person_email"),
      subscription_created: params.try(:[], "tc_social_cancellations_report_form").try(:[], "subscription_created"),
      subscription_cancelled: params.try(:[], "tc_social_cancellations_report_form").try(:[], "subscription_cancelled"),
      expected_renewal: params.try(:[], "tc_social_cancellations_report_form").try(:[], "expected_renewal"),
      start_date: params.try(:[], "tc_social_cancellations_report_form").try(:[], "start_date"),
      end_date: params.try(:[], "tc_social_cancellations_report_form").try(:[], "end_date")
    }
  end
end
