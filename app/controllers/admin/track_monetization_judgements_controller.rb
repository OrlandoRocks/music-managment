class Admin::TrackMonetizationJudgementsController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :load_store, only: :index
  before_action :load_track_monetizations, only: :index
  before_action :load_track_monetization_judgement_form, only: [:index, :mass_judgement]

  def mass_judgement
    if @track_monetization_judgement_form.save
      redirect_back fallback_location: home_path
    else
      flash[:error] = "Something went wrong"

      redirect_back fallback_location: home_path
    end
  end

  private

  def load_store
    @store = nil
  end

  def load_track_monetizations
    return if track_monetization_query_params.blank?

    @track_monetizations = ::TrackMonetization::SearchQueryBuilder
                           .build(track_monetization_query_params_hash)
  end

  def load_track_monetization_judgement_form
    @track_monetization_judgement_form = TrackMonetizationJudgementForm
                                         .new(track_monetizations: track_monetization_judgement_params,
                                              current_user: current_user)
  end

  def track_monetization_query_params_hash
    track_monetization_query_params.merge(store_id: @store&.id)
  end

  def track_monetization_query_params
    params.permit(:search_field, :search_text, :page, :per_page, :eligibility_status)
  end

  def track_monetization_judgement_params
    params[:track_monetizations]
  end
end
