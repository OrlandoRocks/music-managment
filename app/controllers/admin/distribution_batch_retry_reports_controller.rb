class Admin::DistributionBatchRetryReportsController < Admin::AdminController
  layout "admin_new"

  def index
    @batches =
      if params[:search].present?
        Distribution::BatchRetryReportService
          .report(search: params[:search])
          .sort_by(&:complete_albums)
          .paginate(page: params[:page], per_page: 100)
      else
        Distribution::BatchRetryReportService
          .report.sort_by(&:complete_albums)
          .paginate(page: params[:page], per_page: 100)
      end
  end

  def show
    @batch             = Distribution::BatchRetryReportService.new("distribution:batch-retry:#{params[:id]}#{params[:job_name]}").tap(&:report)
    @incomplete_albums = BulkAlbumFinder.new(@batch.incomplete_albums.join(",")).execute
    @complete_albums   = BulkAlbumFinder.new(@batch.complete_albums.join(",")).execute
  end
end
