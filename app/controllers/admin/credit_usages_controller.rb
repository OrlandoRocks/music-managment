class Admin::CreditUsagesController < Admin::AdminController
  before_action :load_person

  def destroy_all
    credit_usages = CreditUsage.where(person_id: @person.id, finalized_at: nil)
    Purchase.where(
      related_type: "CreditUsage",
      related_id: credit_usages.pluck(:id),
      person_id: @person.id,
      paid_at: nil,
      status: "new",
    ).destroy_all
    credit_usages.destroy_all
  end

  protected

  def load_person
    @person = Person.find(params[:person_id])
  end
end
