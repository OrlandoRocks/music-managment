class Admin::PublishingCompositionsController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :load_publishing_composer
  before_action :validate_feature_enabled
  before_action :set_hidden, except: :update

  def index
    @publishing_compositions = ::PublishingAdministration::PublishingCompositionsBuilder
                               .build(search_params)
                               .paginate(page: params[:page], per_page: 25)
  end

  def search
    @publishing_compositions = ::PublishingAdministration::PublishingCompositionsBuilder
                               .build(search_params)
                               .paginate(page: params[:page], per_page: 25)

    render "index", params: search_params
  end

  def update
    @publishing_composition = PublishingComposition.find_by(id: params[:id])

    if @publishing_composer.provider_identifier.present? && @publishing_composition.update(publishing_composition_params)
      @error_msg = @publishing_composition.send_to_rights_app
    else
      @error_msg = "Unable to send publishing_composition to Rights App"
    end
  end

  def update_status
    @publishing_composition = PublishingComposition.find_by(id: params[:id])
    @publishing_composition.unhide!
  end

  def update_eligibility
    @publishing_composition = PublishingComposition.find_by(id: params[:id])

    if @publishing_composition.ineligible?
      @publishing_composition.qualify!
    else
      @publishing_composition.disqualify!
    end
  end

  def download_csv
    @publishing_compositions = ::PublishingAdministration::PublishingCompositionsBuilder.build(search_params)
    csv = Admin::CompositionsCsvService.create(@publishing_compositions)
    send_data(csv, type: "text/csv; charset=utf-8; header=present", filename: "#{@publishing_composer.id}_publishing_compositions.csv")
  end

  def populate_missing_compositions
    ::PublishingAdministration::PublishingCompositionCreationWorker.perform_async(@publishing_composer.id)
    redirect_to admin_publishing_composer_publishing_compositions_path(@publishing_composer)
  end

  private

  def search_params
    params.permit(:publishing_composer_id, :search_term, :hidden, :order_by, :order_by_desc, :search_by)
  end

  def publishing_composition_params
    params
      .require(:publishing_composition)
      .permit(:name, :translated_name, :provider_identifier)
  end

  def load_publishing_composer
    @publishing_composer = PublishingComposer.find(params[:publishing_composer_id])
  end

  def validate_feature_enabled
    return if FeatureFlipper.show_feature?(:edit_composer_compositions, current_user)

    redirect_to admin_publishing_composers_manager_path(@publishing_composer.account)
  end

  def set_hidden
    @hidden = params[:hidden].eql?("true")
  end
end
