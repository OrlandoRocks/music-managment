class Admin::FriendReferralsController < Admin::AdminController
  def index
    @friend_referrals = FriendReferral.paid_referrals
  end
end
