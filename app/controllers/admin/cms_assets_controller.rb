class Admin::CmsAssetsController < Admin::AdminController
  def index
    @assets = CmsAsset.all
  end

  def new
    @asset = CmsAsset.new(created_by_id: current_user.id)
    render layout: "admin_application_lightbox"
  end

  def create
    @asset = CmsAsset.create(params[:cms_asset])
    if !@asset.valid?
      render action: :new, layout: "admin_application_lightbox"
    else
      render layout: "admin_application_lightbox"
    end
  end

  def show
    @asset = CmsAsset.find(params[:id])
    redirect_to @asset.relative_url unless @asset.image?
  end

  def edit
    @asset = CmsAsset.find(params[:id])
    render layout: "admin_application_lightbox"
  end

  def destroy
    @asset = CmsAsset.find(params[:id])
    @asset.destroy
    redirect_to action: :index
  end

  def update
    @asset = CmsAsset.find(params[:id])
    @asset.update(params[:cms_asset])
    if !@asset.valid?
      render action: :edit, layout: "admin_application_lightbox"
    else
      render layout: "admin_application_lightbox"
    end
  end
end
