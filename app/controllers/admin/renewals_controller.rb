class Admin::RenewalsController < Admin::AdminController
  layout "admin_application_lightbox"

  before_action :load_form_variables, only: [:update, :edit]

  def edit
  end

  def update
    # update Renewal
    @note = Note.new(note_params.merge(related: @album, note_created_by: current_user, subject: @label))
    if @renewal
      if @renewal.canceled?
        # cancel renewal
        if @note.save && @renewal.keep!
          PersonNotifier.album_renewal_change_cancel_status(@album).deliver
          flash[:notice] = "This Album has been reactivated for annual renewals and an email has been sent to the customer."
        else
          load_form_variables
          render action: :edit
        end
      elsif @note.save && @renewal.cancel!
        # reset renewal canceled_at to NULL
        PersonNotifier.album_renewal_change_cancel_status(@album).deliver
        flash[:notice] = "This Album has been marked as 'Do not renew' and an email has been sent to the customer."
      else
        load_form_variables
        render action: :edit
      end
    else
      flash[:error] = "Error: Renewal for this album was not found."
      redirect_to admin_album_path(@album)
    end
  end

  def mass_update
    begin
      if params[:album_ids].blank?
        nil
      else
        @processed_albums = []
        @albums = BulkAlbumFinder.new(params[:album_ids]).execute
        @albums.each do |album|
          @renewal = Renewal.renewal_for_without_info(album)
          @label = "Album Renewal - Do Not Renew"
          @note = Note.new(related: album, note_created_by: current_user, subject: @label, note: "Via Mass Expire Tool")
          if @note.save && @renewal.cancel!
            PersonNotifier.album_renewal_change_cancel_status(album).deliver
            @processed_albums << album
          end
        end
        flash[:notice] = "The following albums have been marked as 'Do not renew' and an email has been sent to the customer."
      end
    rescue => e
      if @processed_albums.empty?
        flash[:error] = "Something went wrong and no albums were processed"
      else
        flash[:error] = "Something went wrong and only a few album were processed. See below for a list of albums that were processed."
      end
    end
    render layout: "admin"
  end

  private

  def load_form_variables
    @album   = Album.find(params[:album_id])
    @renewal = Renewal.renewal_for_without_info(@album)

    @label =
      if @renewal.canceled?
        "Album Renewal - Reactivate"
      else
        "Album Renewal - Do Not Renew"
      end
  end

  def note_params
    params.require(:note).permit(:note)
  end
end
