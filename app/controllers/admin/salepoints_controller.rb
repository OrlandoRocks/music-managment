class Admin::SalepointsController < Admin::AdminController
  def edit
    @album = Album.find(params[:id])
    @salepoints = Tunecore::Salepoints::VariablePricing.salepoints_eligible_for_admin_update(@album)
    render action: "no_salepoints" if @salepoints.length.zero?
  end

  def update
    @album = Album.find(params[:id])

    variable_price_data = params["salepoints"].select { |_key, val| val["apply"] == "apply" }.values
    @salepoints = Salepoint.where(id: variable_price_data.map { |data| data["id"] })

    @salepoints.each do |salepoint|
      price_data = variable_price_data.find { |data| data["id"].to_i == salepoint.id }
      keys = ["variable_price_id", "track_variable_price_id"]
      salepoint.update(price_data.select { |k, _v| keys.include?(k) })
      create_note(salepoint)
    end
    PetriBundle.for(@album).add_salepoints(
      @salepoints,
      actor: current_user.name,
      message: "Creating distributions for changing track level price points"
    )

    redirect_to admin_album_path(@album)
  end

  def update_variable_price
    @salepoint = Salepoint.find(params[:id])
    @salepoint.variable_price_id = params[:variable_price][:id]
    if @salepoint.save
      respond_to do |format|
        format.html { render plain: "Updated successfully" }
        format.js { render json: { id: @salepoint.id, message: "Updated successfully" }.to_json }
      end
    else
      respond_to do |format|
        format.html { render plain: "Update failed" }
        format.js { render json: { id: @salepoint.id, message: "Update failed" }.to_json }
      end
    end
  end

  def update_aod_template_id
    @salepoint = Salepoint.find(params[:id])
    @salepoint.template_id = params[:template_id][:id]
    if @salepoint.save
      respond_to do |format|
        format.html { render plain: "Updated successfully" }
        format.js { render json: { id: @salepoint.id, message: "Updated successfully" }.to_json }
      end
    else
      respond_to do |format|
        format.html { render plain: "Update failed" }
        format.js { render json: { id: @salepoint.id, message: "Update failed" }.to_json }
      end
    end
  end

  def block_distribution
    @salepoint = Salepoint.find(params[:id])
    @salepoint.block!(actor: "TuneCore Admin: #{current_user.id}", message: "blocking_distribution")
    Note.create(related: @salepoint.salepointable, note_created_by: current_user, subject: "Blocked Salepoint", note: "Salepoint id: #{@salepoint.id} for store, #{@salepoint.store.name}, blocked", ip_address: request.remote_ip)

    respond_to do |format|
      format.html { redirect_to admin_album_path(@salepoint.salepointable) }
    end
  end

  def unblock_distribution
    @salepoint = Salepoint.find_by(id: params[:id])
    if @salepoint
      @salepoint.unblock!(actor: "TuneCore Admin: #{current_user.id}", message: "unblocking_distribution")
      Note.create(
        related: @salepoint.salepointable,
        note_created_by: current_user,
        subject: "Blocked Salepoint",
        note: "Salepoint id: #{@salepoint.id} for store, #{@salepoint.store.name}, blocked",
        ip_address: request.remote_ip
      )
    end

    respond_to do |format|
      format.html { redirect_to @salepoint ? admin_album_path(@salepoint.salepointable) : admin_home_path }
    end
  end

  protected

  def create_note(salepoint)
    track_price = VariablePrice.find(salepoint.track_variable_price_id) if salepoint.track_variable_price_id
    album_price = VariablePrice.find(salepoint.variable_price_id) if salepoint.variable_price_id

    note = "Variable price for #{salepoint.store.name} changed to "
    note += "#{track_price.price_code_display} (track)" if track_price
    note += ", " if track_price && album_price
    note += "#{album_price.price_code_display} (album)" if album_price
    Note.create(
      related: @album,
      note_created_by: current_user,
      subject: "#{salepoint.store.name} Metadata Update",
      note: note,
      ip_address: request.remote_ip
    )
  end
end
