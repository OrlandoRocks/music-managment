class Admin::TransactionsController < Admin::AdminController
  include PopCrit
  include CurrencyHelper

  before_action :load_rights, only: [:index, :show]
  before_action :redirect_if_cant_refund, only: [:refund, :process_refund]

  def index
    @page_title = "Payment Transactions".freeze
    @transaction_type = params[:transaction_type] || CREDIT_CARD
    @default_paypal_action = (params[:d][:paypal_action] rescue nil) || PaypalTransaction::VALID_ACTIONS[:sale]
    @country_websites = CountryWebsite.all.collect { |country_website| [country_website.country, country_website.id] }

    if params[:transaction_type]
      case @transaction_type
      when CREDIT_CARD
        search_credit_card_transactions
      when PAYU_TRANSACTION
        search_payu_transactions
      else
        search_paypal_transactions
      end

    else
      @from_date = Tunecore::StringDateParser.to_s(Date.today)
      @transactions = BraintreeTransaction.search_transactions(["created_at >= ?", Date.today], { page: params[:page], per_page: 25 })
      @action = "sale".freeze
      @status = "success".freeze
    end
  end

  def show
    @transaction_type = params[:transaction_type] || CREDIT_CARD

    if @transaction_type == CREDIT_CARD
      load_vendor(payment_vendor)
      @person = @credit_card_transaction.person
      @invoice = @credit_card_transaction.invoice
      @purchases = @invoice.purchases if @invoice.present?
      @stored_credit_card = @credit_card_transaction.stored_credit_card
    else
      @paypal_transaction = PaypalTransaction.find(params[:id])
      @person = @paypal_transaction.person

      @invoice = @paypal_transaction.invoice

      @purchases = @invoice.purchases unless @invoice.nil?
    end

    render layout: "admin_application_lightbox"
  end

  def refund
    load_refund_categories
    load_vendor(payment_vendor)
    render layout: "admin_application_lightbox"
  end

  def refund_success
    load_vendor(payment_vendor)
    render layout: "admin_application_lightbox"
  end

  def refund_failure
    load_vendor(payment_vendor)
    render layout: "admin_application_lightbox"
  end

  def process_refund
    @refund_transaction = (payment_vendor == BRAINTREE_VENDOR) ? process_braintree_refund : process_payments_os_refund
    if @refund_transaction.valid?
      if @refund_transaction.success?
        redirect_to action: :refund_success, id: @refund_transaction.id, payment_vendor: @payment_vendor
      else
        redirect_to action: :refund_failure, id: @refund_transaction.id, payment_vendor: @payment_vendor
      end
    else
      load_refund_categories
      @credit_card_transaction = load_vendor(payment_vendor)
      render action: :refund, layout: "admin_application_lightbox"
    end
  end

  def no_permission
    render layout: "admin_application_lightbox"
  end

  private

  def decorate(transaction)
    decorator = (transaction.action == PaymentsOSTransaction::SALE) ? PaymentsOSTransactionDecorator : PaymentsOSRefundDecorator
    decorator.new(transaction)
  end

  def process_payments_os_refund
    original_transaction = PaymentsOSTransaction.find_by(id: params[:id])
    @refund_transaction = PaymentsOS::RefundService.new(original_transaction).refund(
      params[:amount],
      params[:category],
      params[:reason]
    )
  end

  def process_braintree_refund
    @refund_transaction = BraintreeTransaction.process_refund(
      params[:id],
      params[:amount],
      ip_address: request.remote_ip,
      refunded_by_id: current_user.id,
      refund_reason: params[:reason],
      refund_category: params[:category]
    )
  end

  def payment_vendor
    @payment_vendor = params.fetch(:payment_vendor, BRAINTREE_VENDOR)
  end

  def country_payment_vendor
    country_website_id = params.dig(:d, :country_website_id).to_i
    (country_website_id == CountryWebsite::INDIA) ? PAYMENTS_OS_VENDOR : BRAINTREE_VENDOR
  end

  def load_braintree_transaction
    @credit_card_transaction = BraintreeTransaction.find(params[:id])
    @refunds = @credit_card_transaction.refunds
    @related_transaction = @credit_card_transaction.related_transaction
    @credit_card_transaction
  end

  def load_payment_os_transaction
    transaction = PaymentsOSTransaction.includes(:refunds).find(params[:id])
    @credit_card_transaction = decorate(transaction)
    @refunds = @credit_card_transaction.refunds
    @credit_card_transaction
  end

  def load_payu_transaction
    transaction = PayuTransaction.find(params[:id])
    @credit_card_transaction = PayuTransactionDecorator.new(transaction)

    # PAYU_TODO: Update this load method to handle refunds
    # @refunds = @credit_card_transaction.refunds

    # @credit_card_transaction
  end

  def load_rights
    @user_can_refund = current_user.has_role?("Refunds") || current_user.has_role?("Issues Refunds Only")
  end

  def redirect_if_cant_refund
    return if show_legacy_refunds?

    redirect_to action: :no_permission, layout: "admin_application_lightbox"
  end

  def show_legacy_refunds?
    current_user.is_permitted_to?(params[:controller], params[:action]) &&
      !current_user.new_refunds_feature_on?
  end

  def load_refund_categories
    @refund_categories = ["Release did not go live", "Not satisfied with service", "Other"]
  end

  # 2009-09-15 - CH - created method
  def search_paypal_transactions
    process_common_fields
    process_amount_field("amount")
    override_options = {
      transaction_id: { field_name: "pp.transaction_id" },
      from_date: { field_name: "pp.updated_at", operator: ">=", value: @from_date_as_date },
      to_date: { field_name: "pp.updated_at", operator: "<=", value: @to_date_as_date },
      payment_status: { field_name: "pp.status" },
      country_website_id: { field_name: "pp.country_website_id" }
    }.merge(@person_override).merge(@amount_override).merge(process_paypal_action_field)
    conditions = process_criteria_params(params[:d], { skip_value: "All", override_fields: override_options })
    @transactions = PaypalTransaction.search_transactions(conditions, { page: params[:page], per_page: 25 })
  end

  # 2009-09-15 - CH - created method
  def search_credit_card_transactions
    (country_payment_vendor == BRAINTREE_VENDOR) ? search_braintree_transactions : search_payments_os_transactions
  end

  def search_payu_transactions
    @payment_vendor = PAYU_VENDOR
    @decorator = PayuTransactionDecorator
    @transactions = PayuTransaction.search_transaction(india_transaction_search_params)
                                   .paginate(page: params.fetch(:page, 1), per_page: params.fetch(:per_page, 10))
  end

  def india_transaction_search_params
    process_common_fields
    basic_params = params.require(:d).permit(
      :person_search,
      :transaction_id,
      :mina,
      :maxa,
      :payments_os_status,
      :action,
      :status
    )
    basic_params.merge(
      from_date: @from_date_as_date,
      to_date: @to_date_as_date,
      mina: currency_superunit_to_subunit(basic_params[:mina]),
      maxa: currency_superunit_to_subunit(basic_params[:maxa])
    )
  end

  def search_payments_os_transactions
    @payment_vendor = PAYMENTS_OS_VENDOR
    @decorator = PaymentsOSTransactionDecorator
    @transactions = PaymentsOSTransaction.search_transaction(india_transaction_search_params)
                                         .paginate(page: params.fetch(:page, 1), per_page: params.fetch(:per_page, 10))
  end

  def search_braintree_transactions
    @payment_vendor = BRAINTREE_VENDOR
    process_common_fields
    process_amount_field("amount")
    override_options = {
      transaction_id: { field_name: "bt.transaction_id" },
      from_date: { field_name: "bt.created_at", operator: ">=", value: @from_date_as_date },
      to_date: { field_name: "bt.created_at", operator: "<=", value: @to_date_as_date },
      status: { field_name: "bt.status" },
      country_website_id: { field_name: "bt.country_website_id" }
    }.merge(@person_override).merge(@amount_override)
    conditions = process_criteria_params(params[:d].except(:paypal_action, :payments_os_status), { skip_value: "All", override_fields: override_options })
    @transactions = BraintreeTransaction.search_transactions(conditions, { page: params[:page], per_page: 25 })
  end

  # ==================================================================================
  # = process_common_fields parses the submitted date text fields into date objects for the criteria
  # = also creates override settings for the PopCrit plugin which is then merged into the search
  # = criteria for the index search functions search_paypal_transactions, search_credit_card_transactions
  # = 2009-9-30 - CH - created method
  # ==================================================================================
  def process_common_fields(_convert_to_pennies = false)
    @from_date_as_date = DateParser.parse(params[:d][:from_date]) rescue nil
    @to_date_as_date = DateParser.parse(params[:d][:to_date]) rescue nil

    # set the person search field based on @ symbol matching
    @person_override =
      if params[:d][:person_search] != nil && params[:d][:person_search].match(/.*(@).*/)
        { person_search: { field_name: "p.email" } }
      else
        { person_search: { field_name: "p.name", operator: "like", value_ends_with: "%" } }
      end
  end

  # =====================================================================
  # = process_amount_field creates the override settings for params[:d][:mina] and params[:d][:maxa]
  # = It additionally ejects malformed numbers submitted in the search.
  # = 2009-10-6 - CH - created method
  # =====================================================================
  def process_amount_field(field_name, convert_to_pennies = false)
    if params[:d][:mina] != nil && params[:d][:mina].match(/^\d+(\.\d{1,2})?$/) # check that this is a submitted number, set to nil if it isn't
      minimum_amount = params[:d][:mina]
    else
      minimum_amount = nil
    end

    if params[:d][:maxa] != nil && params[:d][:maxa].match(/^\d+(\.\d{1,2})?$/) # check that this is a submitted number, set to nil if it isn't
      maximum_amount = params[:d][:maxa]
    else
      maximum_amount = nil
    end

    if convert_to_pennies
      minimum_amount = (minimum_amount.to_f * 100).to_i if minimum_amount != nil
      maximum_amount = (maximum_amount.to_f * 100).to_i if maximum_amount != nil
    end

    @amount_override = {
      mina: { field_name: field_name, value: minimum_amount, operator: ">=" },
      maxa: { field_name: field_name, value: maximum_amount, operator: "<=" }
    }
  end

  def process_paypal_action_field
    if params[:d][:paypal_action] == "Authorization & Void"
      { paypal_action: { field_name: "pp.action", value: ["Authorization", "Void"] } }
    else
      { paypal_action: { field_name: "pp.action" } }
    end
  end

  def load_vendor(payment_vendor)
    case payment_vendor
    when BRAINTREE_VENDOR
      load_braintree_transaction
    when PAYMENTS_OS_VENDOR
      load_payment_os_transaction
    when PAYU_VENDOR
      load_payu_transaction
    end
  end
end
