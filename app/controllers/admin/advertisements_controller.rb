class Admin::AdvertisementsController < Admin::AdminController
  before_action :redirect_if_not_permitted, only: [:index]
  before_action :redirect_if_not_permitted_lightbox, only: [:new, :edit, :create, :update]
  before_action :load_advertisement, except: [:new, :create, :index]
  before_action :scope_country

  def index
    @page_title = "Ads"
    @default_ads = Advertisement.where(
      "is_default = 1 and country_website_id = ?",
      @country_website.id
    ).order("category, sort_order").all
    @ads = Advertisement.where(
      "is_default = 0 and country_website_id = ?",
      @country_website.id
    ).order("category, sort_order").all
  end

  def new
    @advertisement = Advertisement.new(created_by_id: current_user.id)
    @targeted_offer_id = params[:targeted_offer_id] || nil
    render layout: "admin_application_lightbox"
  end

  def edit
    @targeted_offers = TargetedOffer.find(
      @advertisement.targeted_advertisements.collect { |ta|
        [ta.targeted_offer_id]
      }
    )
    render layout: "admin_application_lightbox"
  end

  def create
    advertisement = Advertisement.create(params[:advertisement])
    if params[:targeted_offer_id]
      targeted_offer = TargetedOffer.find(params[:targeted_offer_id])
      TargetedAdvertisement.create(targeted_offer: targeted_offer, advertisement: advertisement)
    end
    render layout: "admin_application_lightbox"
  end

  def update
    @advertisement.update(params[:advertisement])
    render layout: "admin_application_lightbox"
  end

  def show
    @targeted_offers = TargetedOffer.find(
      @advertisement.targeted_advertisements.collect { |ta|
        [ta.targeted_offer_id]
      }
    )
    render layout: "admin_application_lightbox"
  end

  private

  def load_advertisement
    @advertisement = Advertisement.find(params[:id])
  end

  def scope_country
    params[:country] ||= "US" # if params are not set, default to 'US'
    @country_website = CountryWebsite.where("country = ?", params[:country]).first
  end
end
