class Admin::NotesController < Admin::AdminController
  layout "admin_application_lightbox"

  def new
    @note = Note.new
    @person = Person.find(params[:id])
  end

  def create
    @person = Person.find(params[:id])

    @note = Note.new
    if params[:note][:related_type].nil? || params[:note][:related_id].nil?
      @note.related = @person
    else
      @note.related_type = params[:note][:related_type]
      @note.related_id = params[:note][:related_id]
    end
    @note.note_created_by = current_user
    @note.ip_address = request.remote_ip
    @note.subject = params[:note][:subject]
    @note.note = params[:note][:note]
    @note.save

    respond_to do |format|
      format.html { render }
      format.json { render json: { status: 0 } }
    end
  end

  def index
    @person = Person.find(params[:id])
    @notes = @person.all_notes
    @display_recent_notes_warning = @person.display_recent_notes_warning?
    render layout: "admin"
  end
end
