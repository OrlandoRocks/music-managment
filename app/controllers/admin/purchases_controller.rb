class Admin::PurchasesController < Admin::AdminController
  def index
    @page_title = "Open Purchases for #{current_user.name}"
    @person = Person.find(params[:person_id])
    @notes = @person.notes.includes(:note_created_by).limit(5).all
    @total_note_count = @person.notes.count
    @purchases = @person.purchases.unpaid.includes(:product).order("products.display_name")
    @can_adjust = current_user.has_role?("Refunds") || current_user.has_role?("Adjust Prices")
  end

  def edit
    # redirect_if_not_permitted_lightbox
    @person = Person.find(params[:person_id])
    @purchase = Purchase.find(params[:id])
    render layout: "admin_application_lightbox"
  end

  def update
    if current_user.has_role?("Refunds") || current_user.has_role?("Adjust Prices")
      @person = Person.find(params[:person_id])
      @purchase = @person.purchases.find(params[:id])
      new_price = (params[:new_price].to_s.gsub(/(\$|,)/, "").to_f * 100).to_s.to_i
      @purchase.adjust_price(new_price, params[:admin_note], current_user)
      if @purchase.errors.count.positive?
        render action: "edit", layout: "admin_application_lightbox"
      else
        render layout: "admin_application_lightbox"
      end
    else
      redirect_to controller: "rights", action: "not_permitted", layout: "admin_application_lightbox"
    end
  end
end
