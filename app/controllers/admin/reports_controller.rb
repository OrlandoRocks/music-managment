class Admin::ReportsController < Admin::AdminController
  before_action :requires_reporting_role

  def index
    redirect_to action: :totals
  end

  def view
    @new_css = true
    if request.post?
      @report = Tunecore::Reports::Base.generate(params["report"])
      @page_title = @report.title
      send_data(@report.to_csv, filename: @report.title + ".csv", type: "text/csv") if params[:export]
    else
      @report = Tunecore::Reports::Signups.new("interval" => "day", "start_date" => 1.day.ago.localtime)
      @page_title = "Choose Report"
    end
    @report_class = @report.class # used by report options partial
  end

  def upload_trending
    raise "DEACTIVATED"
  end

  def totals
    @page_title = "System Totals"

    @total_people = Person.where(deleted: 0).count
    @total_people_with_albums = Person.connection.select_values("select count(distinct(person_id)) from albums")
    @total_people_with_albums = @total_people_with_albums.instance_of?(Array) ? @total_people_with_albums.first.to_i : @total_people_with_albums.to_i
    @total_people_with_completed_albums = Person.connection.select_values("select count(distinct person_id) from albums where payment_applied=1 and finalized_at is not null")
    @total_people_with_paid_invoices = Person.connection.select_values("select count(distinct(person_id)) from invoices where settled_at is not null")

    @total_albums = Album.where(is_deleted: 0).count
    @completed_albums = Album.where(is_deleted: 0).where(payment_applied: 1).count
    @average_song_count = Song.connection.select_values("select round(avg(songs.total),1) from ( select count(*) as total from songs group by album_id ) songs")
    @average_store_count = Salepoint.connection.select_values("select round(avg(salepoints.total),1) from ( select count(*) as total from salepoints where payment_applied = 1 group by salepointable_id ) salepoints")

    @total_stores = Store.count
    @total_referrers = Person.connection.select_values("select count(distinct(people.referral)) from people")
    @total_campaigns = Person.connection.select_values("select count(distinct(people.referral_campaign)) from people")

    @total_artists = Artist.count
    @total_labels = Label.count
  end

  def orphaned_albums
    @page_title = "Orphaned Albums"
    @albums = Album.where("is_deleted = 0 and person_id is null").all
  end

  def people_with_albums
    @page_title = "People with albums"
    @rows = Person.connection.select_all("select person_id, count(*) as total from albums GROUP BY person_id ORDER BY total DESC").group_by { |x| x["total"].to_i }.sort
  end

  def people_by_album_count
    raise "Missing album count" unless @album_count = params[:album_count]

    @people = Person.with_total_albums(@album_count).order("people.recent_login DESC, people.name DESC")
                    .paginate(page: params[:page], per_page: 50)

    @page_title = "There are #{@people.length} People with #{@album_count} Albums"
  end

  def store_totals
    @rows = Person.connection.select_all("select s.name, count(*) as TOTAL from albums a, salepoints p, stores s where a.id = p.salepointable_id and p.salepointable_type = 'Album' and p.status != 'new' and p.store_id = s.id  GROUP BY s.id")
  end

  def itunes_vs_world
    @page_title = "Itunes vs Other Stores Album Breakdown"

    @itunes_albums = Album.connection.select_all("select distinct(a.id) from albums a join salepoints p on a.id = p.salepointable_id and p.salepointable_type = 'Album' where p.status != 'new' and p.store_id in (1,2,3,4,5)").map { |x| x["id"].to_i }

    @other_albums = Album.connection.select_all("select distinct(a.id) from albums a join salepoints p on a.id = p.salepointable_id and p.salepointable_type = 'Album' where p.status != 'new' and p.store_id not in (1,2,3,4,5)").map { |x| x["id"].to_i }

    @itunes_exclusive_count = (@itunes_albums - @other_albums).size
    @both_count = (@itunes_albums & @other_albums).size
    @non_itunes_count = (@other_albums - @itunes_albums).size
  end

  def show_album_detail_meta
    respond_to do |format|
      format.html { render action: album_detail_meta }
    end
  end

  def album_detail_meta
    respond_to do |format|
      format.html
    end
  end

  def album_store_counts
    @rows = Album.connection.select_all("select a.id, count(*) as TOTAL from albums a join salepoints p on a.id = p.album_id where p.status != 'new' GROUP BY a.id order by TOTAL DESC")
    @rows = @rows[0, 100]
  end

  def serial_totals
    @rows = []
    @rows << {
      program: "t_racks",
      os: "mac",
      claimed: Serial.where("program = ? AND taken = true AND os = ?", "t_racks", "mac").count
    }
    @rows << {
      program: "t_racks",
      os: "windows",
      claimed: Serial.where("program = ? and taken = true and os = ?", "t_racks", "windows").count
    }
    @rows << {
      program: "amplitube",
      os: "n/a",
      claimed: Serial.where("program = ? and taken = true", "amplitube").count
    }
  end

  def logins
    @first_login = Person.where("recent_login is not null").order("recent_login ASC").first
    @people = Person.order("recent_login DESC").limit(30).all

    @page_title = "Login Data (starting %s)" % l(@first_login.recent_login, format: :slash_short_year)
    @total_people = Person.where(deleted: 0).count
    @total_logins = Person.where(deleted: 0).where.not(recent_login: nil).count
    @week_logins = Person.where("deleted = 0 AND recent_login >= '#{7.days.ago.localtime.to_s :db}'").count
    @month_logins = Person.where("deleted = 0 AND recent_login >= '#{30.days.ago.localtime.to_s :db}'").count
  end

  def invoices
    @state = params[:state].to_sym if params[:state]
    @state = :all unless [:settled, :unsettled].include? @state

    @page_title = "#{@state.to_s.titleize} Invoices"

    conditions = { settled: "settled_at IS NOT NULL", unsettled: "settled_at IS NULL" }[@state]

    @invoices = Invoice.includes(:person, :purchases, :invoice_settlements).where(conditions)
                       .order("invoices.created_at desc")
                       .paginate(page: params[:page], per_page: 50)
  end

  def album_song_counts
    @page_title = "Album Song Counts"
    query = <<-EOSTRING
           SELECT count(*) as album_count,
                  sum(payment_applied) as paid_album_count,
                  album.song_count
           FROM albums a
           INNER JOIN
           (SELECT album_id, count(*) as song_count
            FROM songs
            GROUP BY album_id) album
           ON album.album_id = a.id
           GROUP BY album.song_count
           ORDER BY album.song_count ASC
    EOSTRING
    @rows = Song.connection.select_all(query)
  end

  def signup_referrals
    @page_title = "Signup Referrals"
    if params[:campaign]
      @ref_field = "referral_campaign"
      @page_title << " By Campaign"
    else
      @ref_field = "referral"
    end
    query = <<-EOSTRING
           SELECT count(*) as person_count,
                  (sum(payment.total) / 100) as revenue,
                  count(distinct(payment.person_id)) as conversion_count,
                  #{@ref_field} as referral,
                  referral_type
           FROM people p
           LEFT JOIN
           (SELECT person_id, sum(final_settlement_amount_cents) as total
            FROM invoices
            WHERE settled_at is not null
            GROUP BY person_id) payment
           ON payment.person_id = p.id
           WHERE #{@ref_field} is not null
           GROUP BY p.#{@ref_field}
           ORDER BY person_count DESC
    EOSTRING
    @rows = Person.connection.select_all(query)
  end

  def settlement_totals
    @page_title = "Settlement Totals"
    query = <<-EOSTRING
           SELECT min(created_at) as week,
                  count(*) as count,
                  count(settled_at) as settled_count,
                  sum(final_settlement_amount_cents) as settlement_total,
                  avg(final_settlement_amount_cents) as settlement_avg,
                  max(final_settlement_amount_cents) as settlement_max,
                  count(distinct(final_settlement_amount_cents)) as settlement_distinct,
                  stddev(final_settlement_amount_cents) as settlement_stddev,
                  avg(item_count) as item_count_avg,
                  max(item_count) as item_count_max
           FROM invoices i
           INNER JOIN
             (SELECT invoice_id, count(*) as item_count
              FROM purchases
              GROUP BY invoice_id) item
           ON item.invoice_id = i.id
           GROUP BY yearweek(created_at)
           ORDER BY i.created_at DESC
    EOSTRING
    @rows = Invoice.connection.select_all(query)
  end

  def incomplete_album_totals
    @page_title = "Incomplete Album Totals"
    @total_albums = Album.count
    @incomplete_albums = Album.where(finalized_at: nil).count
    @albums_missing_art = Album.where("id NOT IN (select album_id from artworks)").count
    @albums_with_no_songs = Album.where("id NOT IN (select album_id from songs)").count
    @albums_with_upload_errors = Album.where("id IN (select album_id from songs where last_errors is not null)").count
  end

  def signup_overview
    @page_title = "Signup Overview"
    query = <<-EOSTRING
           SELECT min(created_on) as date,
                  count(distinct(p.id)) as signup_count,
                  sum(albums.count) as albums_count,
                  sum(albums.paid_count) as paid_albums_count
           FROM people p
           LEFT JOIN
           (SELECT person_id, count(*) as count, sum(payment_applied) as paid_count
            FROM albums
            GROUP BY person_id) albums
           ON albums.person_id = p.id
           GROUP BY DATE(p.created_on)
           ORDER BY p.created_on DESC
    EOSTRING
    @rows = Person.connection.select_all(query)
  end

  def videos
    @page_title = "Videos"
    unless params[:report].nil?
      @start_date = Date.civil(params[:report][:"start_date(1i)"].to_i, params[:report][:"start_date(2i)"].to_i, params[:report][:"start_date(3i)"].to_i)
    end
    unless params[:report].nil?
      @end_date = Date.civil(params[:report][:"end_date(1i)"].to_i, params[:report][:"end_date(2i)"].to_i, params[:report][:"end_date(3i)"].to_i)
    end
    @videos = Video.where("finalized_at is not NULL and finalized_at > ? and finalized_at < ? AND metadata_picked_up is not NULL", @start_date, @end_date).all || nil
  end

  private

  def requires_reporting_role
    redirect_to admin_home_path and return unless current_user.has_role?("Report Viewer", false)
  end

  def self.ids_of_people_with_multiple_invoices(settled = true)
    connection.select_values(
      "select person_id, settled_at, count(*) as total from invoices GROUP BY person_id HAVING total >= 2 and settled_at is #{if settled
                                                                                                                                'not'
                                                                                                                              end} null"
    ).map(&:to_i)
  end

  def load_reporting_variables
    @first_sales_period = SalesRecordMaster.order("period_sort asc").first&.period_sort
    @last_sales_period = SalesRecordMaster.order("period_sort desc").first&.period_sort
    @reporting_dates = Utilities::FormDateRange.create_range(Date.parse(@first_sales_period.to_s), Date.parse(@last_sales_period.to_s))
  end
end
