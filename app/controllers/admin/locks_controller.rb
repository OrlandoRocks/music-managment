class Admin::LocksController < Admin::AdminController
  def show
    @locked_items = Tunecore::Reports::CurrentLocks.by(current_user)
  end

  def people
    @locked_items = Tunecore::Reports::CurrentLocks.by(:people)
  end

  def invoices
    @locked_items = Tunecore::Reports::CurrentLocks.by(:invoices)
  end

  #  def bundles
  #    @locked_items = Tunecore::Reports::CurrentLocks.by(:bundles)
  #  end
end
