class Admin::Distribution::ErrorsController < Admin::AdminController
  respond_to :js

  def index
    @page_title        = "Petri Error Summary"
    @distro_error_form = Distribution::ErrorForm.new(distribution_error_params).save
    @distributions     = @distro_error_form.distributions.paginate(page: params[:page] || 1, per_page: 1000)
  end

  def create
    @distributions = Distribution.where(id: params[:distribution_ids])
    @distributions.each do |distribution|
      Delivery::Retry.retry(
        delivery: distribution,
        person: current_user,
        delivery_type: distribution.last_delivery_type
      )
    end

    respond_with distribution_ids
  end

  private

  def distribution_error_params
    {
      start_date: params[:start_date],
      end_date: params[:end_date],
      filter_type: params[:filter_type],
      states: params[:states]
    }
  end

  def distribution_ids
    @distribution_ids ||= @distributions.pluck(:id)
  end
end
