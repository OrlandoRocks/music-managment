class Admin::CountryWebsitesController < Admin::AdminController
  include AdminResource

  before_action :check_admin_login
  before_action :load_person, only: [:edit, :update]
  before_action :set_country_websites, only: [:edit, :update]

  def update
    @person.update(country_website: CountryWebsite.find(params[:id]))

    render :edit
  end

  private

  def set_country_websites
    @country_websites = CountryWebsite.all
  end
end
