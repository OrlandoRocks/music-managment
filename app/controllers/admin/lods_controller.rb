class Admin::LodsController < Admin::ComposersController
  before_action :requires_publishing_role, only: [:index]
  before_action :requires_publishing_manager_role, except: [:index]

  def index
    @composers = Composer.is_paid.joins(:lod, :person).order("lods.last_status_at desc").paginate(per_page: 100, page: (params[:page] || 1))
  end

  def download_pdf
    render plain: "Document is not available"
  end

  def update_statuses
    redirect_to action: "index"
  end

  def search
    @composers = search_composers

    render "index", params: search_params
  end

  private

  def search_composers
    Composer::SearchQueryBuilder.build(search_params)
                                .paginate(page: params[:page] || 1, per_page: 100.to_i)
  end

  def search_params
    params.permit(:search_field, :search_text)
  end
end
