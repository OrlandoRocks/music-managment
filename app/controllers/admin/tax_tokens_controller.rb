class Admin::TaxTokensController < Admin::AdminController
  layout "tc_foundation_admin"

  def index
    @tax_tokens_cohort = TaxTokensCohort.find(params[:tax_tokens_cohort_id])
    @tax_tokens = @tax_tokens_cohort.tax_tokens.visible
  end

  def update
    @tax_token_service = TaxTokenService.new(params[:person_id]).remove_user_from_all_cohorts
    message = @tax_token_service ? "Successfully updated user" : "Something went wrong."

    if @tax_token_service && request.referrer.include?("/people")
      redirect_to admin_person_path(request.referrer.last), notice: message
    else
      redirect_to admin_tax_tokens_cohort_tax_tokens_path(params[:tax_tokens_cohort_id]), notice: message
    end
  end
end
