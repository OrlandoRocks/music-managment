class Admin::BlacklistedPaypalPayeesController < Admin::AdminController
  before_action :user_can_blacklist_payees, only: [:create, :destroy]
  before_action :load_blacklisted_payee, only: [:destroy]

  def index
    @blacklisted_payees = BlacklistedPaypalPayee::SearchService
                          .search(payee_params, params[:page], params[:per_page])
  end

  def create
    if BlacklistedPaypalPayee::CreationService.create(bulk_payees_params)
      flash[:notice] = "successfull added payees to blacklist"
    else
      flash[:error]  = "unable to add payees to blacklist"
    end
    redirect_to admin_blacklisted_paypal_payees_path
  end

  def destroy
    if @blacklisted_payee.destroy
      flash[:notice] = "succesfully delete payee #{@blacklisted_payee.payee}"
    else
      flash[:error]  = "unable to delete payee #{@blacklisted_payee}"
    end
    redirect_to admin_blacklisted_paypal_payees_path
  end

  private

  def user_can_blacklist_payees
    return if current_user.has_role?("EFT")

    flash[:errors] = "you do not have permission to add a payee to the blacklist"
    redirect_to admin_blacklisted_paypal_payees_path
  end

  def load_blacklisted_payee
    @blacklisted_payee = BlacklistedPaypalPayee.find(params[:id])
  end

  def payee_params
    params[:blacklisted_paypal_payee]
  end

  def bulk_payees_params
    params[:bulk_blacklisted_paypal_payees]
  end
end
