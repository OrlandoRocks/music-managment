class Admin::BalanceTransactions::FiltersController < Admin::AdminController
  def show
    @filtered_transactions = BalanceHistory::TransactionsQueryBuilder.build(
      params[:filter_transactions].merge(person: current_user)
    )
  end
end
