class Admin::KnowledgebaseLinksController < Admin::AdminController
  layout "admin_new"

  def index
    @page_title = "Knowledgebase Links"
    @help_links = KnowledgebaseLink.all
  end

  def new
    @link = KnowledgebaseLink.new
  end

  def create
    @row = params[:knowledgebase_link].delete(:row_id)
    @link = KnowledgebaseLink.create!(params[:knowledgebase_link])

    respond_to do |format|
      format.js {
        flash.now[:notice] = "Your changes have been saved." if @link.persisted?
        flash.discard
      }
    end
  rescue => e
    flash[:error] = "Unable to create new help link."
    flash.discard
  end

  def update
    @link_id = params[:id]
    @link = KnowledgebaseLink.find(@link_id)
    @link.attributes = params[:knowledgebase_link].except(:row_id)

    respond_to do |format|
      format.js {
        flash.now[:notice] = "Your changes have been saved." if @link.save
        flash.discard
      }
    end
  rescue => e
    flash[:error] = "Unable to save changes."
    flash.discard
  end

  def destroy
    return if params[:id] == -1

    @row = params[:row_id]
    KnowledgebaseLink.find(params[:id]).destroy
  end
end
