class Admin::InventoriesController < Admin::AdminController
  before_action :load_person

  def index
    @inventory_types = Inventory::MAIN_INVENTORY_TYPES

    @inventories_all = Inventory.distribution_credits(@person)
    @credit_usages_in_cart = @person.credit_usages.not_finalized.for_all_types

    @inventories = @inventories_all
                   .includes([{ product_item: :product }, :purchase])
                   .order("created_at DESC")
                   .paginate(page: params[:page], per_page: (params[:per_page] || 10).to_i)
  end

  def show
    @inventory = Inventory.find(params[:id])
    @inventory_adjustments = InventoryAdjustment.joins(:inventory).where(inventory_id: @inventory.id, inventories: { person_id: @person.id }, rollback: false)
    @inventory_usages = InventoryUsage.where({ inventory_id: @inventory.id })
  end

  protected

  def load_person
    @person = Person.find(params[:person_id])
  end
end
