class Admin::ComposerSyncOptInsController < Admin::AdminController
  def destroy
    ::PublishingAdministration::ComposerSyncOptInForm.destroy(destruction_params)

    redirect_to admin_composers_manager_path(params[:id])
  end

  private

  def destruction_params
    params.require(:composer).permit(:id, :sync_opted_updated_at)
  end
end
