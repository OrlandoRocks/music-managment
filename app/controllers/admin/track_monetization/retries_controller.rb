class Admin::TrackMonetization::RetriesController < Admin::AdminController
  before_action :load_track_monetizations, only: [:create]

  def create
    TrackMonetization::RetryService.retry(retry_params)
  end

  private

  def load_track_monetizations
    @track_mon = TrackMonetization.find(params[:id])
  end

  def retry_params
    {
      current_user_id: current_user.id,
      delivery_type: params[:delivery_type],
      track_mon_id: @track_mon.id
    }.with_indifferent_access
  end
end
