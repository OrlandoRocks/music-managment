class Admin::TrackMonetization::TakedownsController < Admin::AdminController
  before_action :load_track_mon

  def create
    TrackMonetization::TakedownService.takedown(takedown_params)
    flash[:success] = "Successfully marked track for takedown"

    redirect_to admin_album_path(@track_mon.album)
  end

  def destroy
    TrackMonetization::TakedownService.remove_takedown(takedown_params)
    flash[:success] = "Successfully removed the takedown"

    redirect_to admin_album_path(@track_mon.album)
  end

  private

  def load_track_mon
    @track_mon = ::TrackMonetization.includes(:store).find(params[:id])
  end

  def takedown_params
    {
      current_user: current_user,
      track_mons: [@track_mon]
    }.with_indifferent_access
  end
end
