class Admin::FacebookTrackMonetizationJudgementsController < Admin::TrackMonetizationJudgementsController
  def index
    @page_title = "Facebook Tracks :: Approve/Reject Tracks"
  end

  private

  def load_store
    @store = Store.find_by(id: Store::FBTRACKS_STORE_ID)
  end
end
