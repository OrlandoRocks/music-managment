class Admin::OauthClientsController < Admin::AdminController
  before_action :get_client_application, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, raise: false

  def index
    @client_applications = current_user.client_applications
    @tokens = current_user.tokens.where("oauth_tokens.invalidated_at is null and oauth_tokens.authorized_at is not null")
  end

  def new
    @client_application = ClientApplication.new
  end

  def create
    params[:client_application][:role_ids] ||= []
    @client_application = current_user.client_applications.build(params[:client_application])
    if @client_application.save
      flash[:notice] = "Registered the information successfully"
      redirect_to action: "show", id: @client_application.id
    else
      render action: "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    params[:client_application][:role_ids] ||= []
    if @client_application.update(params[:client_application])
      flash[:notice] = "Updated the client information successfully"
      redirect_to action: "show", id: @client_application.id
    else
      render action: "edit"
    end
  end

  def destroy
    @client_application.destroy
    flash[:notice] = "Destroyed the client application registration"
    redirect_to action: "index"
  end

  private

  def get_client_application
    return if @client_application = current_user.client_applications.find(params[:id])

    flash.now[:error] = "Wrong application id"
    raise ActiveRecord::RecordNotFound
  end
end
