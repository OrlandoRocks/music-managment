class Admin::CountryHelpLinksController < Admin::AdminController
  layout "admin_new"

  def index
    @page_title = "Country Help Links"
    @help_links = CountryHelpLink.paginate(page: params[:page] || 1, per_page: params[:per_page] || 20)
  end

  def new
    @link = CountryHelpLink.new
  end

  def create
    @row = params[:country_help_link].delete(:row_id)
    @link = CountryHelpLink.create!(params[:country_help_link])
  rescue => e
    @error = "Unable to create new help link: #{e.message}"
  end

  def update
    @link_id = params[:id]
    link = CountryHelpLink.find(@link_id)
    link.attributes = params[:country_help_link].except(:row_id)
    link.save!
  rescue => e
    @error = "Unable to save changes: #{e.message}"
  end

  def destroy
    return if params[:id] == -1

    @row = params[:row_id]
    CountryHelpLink.find(params[:id]).destroy
  end
end
