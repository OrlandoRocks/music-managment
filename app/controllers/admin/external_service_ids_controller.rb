class Admin::ExternalServiceIdsController < Admin::AdminController
  def create
    get_linkable(params)
    ExternalServiceIds::ExternalServiceIdsFetcher.fetch(@linkable, params[:external_service]) if @linkable
    head :ok
  end

  private

  def get_linkable(params)
    klass     = params[:linkable_type].try(:constantize)
    @linkable = klass.try(:find, params[:linkable_id])
  end
end
