class Admin::SoundoutReportsController < Admin::AdminController
  def index
    params[:page]     ||= 1
    params[:per_page] ||= 20

    @person = Person.find(params[:person_id])
    @soundout_reports = @person.soundout_reports.order("ID desc").paginate(page: params[:page], per_page: params[:per_page])
  end

  def toggle_cancel
    @person = Person.find(params[:person_id])
    @soundout_report = @person.soundout_reports.find(params[:id])

    canceled_before = @soundout_report.canceled?
    @soundout_report.toggle_cancel!
    canceled_after = @soundout_report.canceled?

    # Unable to change the canceled status
    if canceled_before == canceled_after

      # Did we start canceled or uncanceled
      flash[:error] =
        if canceled_before
          "Error marking the soundout report as uncanceled"
        else
          "Error marking the soundout report as canceled"
        end
    elsif canceled_after
      Note.create(subject: "Soundout report cancel change", note: "Report for '#{@soundout_report.track.name}' marked as canceled", related: @soundout_report.track, note_created_by: current_user)
      flash[:success] = "Successfully marked the soundout report as canceled"
    else
      Note.create(subject: "Soundout report cancel change", note: "Report for '#{@soundout_report.track.name}' marked as not canceled", related: @soundout_report.track, note_created_by: current_user)
      flash[:success] = "Successfully marked the soundout report as not canceled"
    end

    redirect_to admin_person_soundout_reports_path(person_id: params[:person_id])
  end

  def fetch_report
    @person = Person.find(params[:person_id])
    @soundout_report = @person.soundout_reports.find(params[:id])

    if @soundout_report && @soundout_report.soundout_id.present?
      success = @soundout_report.soundout_product.fetch_report(@soundout_report).first

      if success
        flash[:success] = "Successfully fetched the report"
      else
        flash[:error]   = "Could not fetch the report"
      end
    end

    redirect_to admin_person_soundout_reports_path(person_id: params[:person_id])
  end

  def resend_report
    @person = Person.find(params[:person_id])
    @soundout_report = @person.soundout_reports.find(params[:id])

    if @soundout_report && @soundout_report.paid? && (@soundout_report.errored? || @soundout_report.requested?)
      soundout_id = @soundout_report.soundout_product.order_report(@soundout_report)

      if soundout_id
        flash[:success] = "Successfully resent the report to soundout"
      else
        flash[:error]   = "Failed resending the report to soundout"
      end
    else
      flash[:error] = "Cannot resend this report"
    end

    redirect_to admin_person_soundout_reports_path(person_id: params[:person_id])
  end
end
