class Admin::InvoicesController < Admin::AdminController
  before_action :load_person, except: [:search]
  before_action :load_invoice, except: [:index, :search]
  before_action :load_rights, only: [:show]
  before_action :redirect_if_cant_settle, only: [:settle]

  def index
    @invoices = @person.invoices
  end

  def show
  end

  def destroy
    if @invoice.can_destroy?(true)
      @invoice.credit_and_remove_settlements!
      @invoice = @invoice.reload
      @invoice.destroy
      flash[:notice] = "Destroyed invoice #{@invoice.id}"
      redirect_to admin_person_invoices_path(@person)
    else
      flash[:error] = "Unable to destroy invoice #{@invoice.id}"
      redirect_to admin_person_invoice_path(@invoice.person, @invoice)
    end
  end

  def settle
    if !@invoice.can_settle?
      flash[:error] =
        if @invoice.settled?
          "The invoice has already been marked as settled."
        else
          "The invoice cannot be settled - has it received enough payments?"
        end
    else
      @invoice.settled!
      flash[:notice] = "Marked the invoice as settled."
    end
    redirect_to admin_person_invoice_path(@invoice.person, @invoice)
  end

  def resend
    PersonNotifier.payment_thank_you(@person, @invoice).deliver
    flash[:notice] = "The thank you email was resent to #{@person.email}."
    render action: :show
  end

  def search
    if params[:term] =~ /^\s*(id|bundle|state|album_state|upc|isrc|name|label|artist|genre|all):\s*(.*)$/
      @search_field = $1
      @search_text = $2
    else
      @search_text = params[:term]
      @search_field =
        if @search_text.match?(/^\s*\d+\s*$/)
          "id"
        else
          "all"
        end
    end

    case @search_field
    when "id"
      # conditions = ['albums.id = ?', @search_text]
      # order = 'albums.updated_at ASC'

      invoice = Invoice.find_by(id: @search_text.to_s)

      if invoice.nil?
        flash[:error] = "The invoice ##{@search_text} could not be found."

        redirect_to "/admin/payment"
      else
        @person = invoice.person
        redirect_to admin_person_invoice_path(invoice.person, invoice)
      end
    end
  end

  protected

  def load_person
    @person = Person.find(params[:person_id])
  end

  def load_invoice
    @invoice = @person.invoices.find(params[:id])
  end

  def load_rights
    @user_can_refund = current_user.has_role?("Refunds")
  end

  def redirect_if_cant_settle
    return if current_user.is_permitted_to?(params[:controller], params[:action])

    flash[:notice] = "You don't have permission to access that page"
    redirect_to admin_person_invoice_path(@person.id, @invoice)
  end
end
