class Admin::SubscriptionEventsController < Admin::AdminController
  def index
    @subscription_type = params[:subscription_type]
    @person            = Person.find(params[:person_id])
    @events            = SubscriptionEvent.where(
      subscription_type: @subscription_type,
      person_subscription_status_id: params[:subscription_id],
    )
  end
end
