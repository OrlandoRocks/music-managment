class Admin::NotificationsController < Admin::AdminController
  before_action :require_notification_role

  def index
    @term = params[:term]
    @notifications = \
      TargetedNotification
      .order("created_at DESC")
      .paginate(page: (params[:page] || 1).to_i)

    return unless @term

    @notifications = @notifications.where("lower(targeted_notifications.title) like ? or lower(targeted_notifications.text) like ?", "%#{@term.downcase}%", "%#{@term.downcase}%")
  end

  def show
    @notification = TargetedNotification.find(params[:id])
    @people_in_notifications = \
      Notification
      .where(targeted_notification_id: @notification.id)
      .paginate(page: params[:page])
  end

  def edit
    @notification = TargetedNotification.find(params[:id])
  end

  def update
    @notification = TargetedNotification.find(params[:id])

    if targeted_notification_params[:notification_icon_id]
      @notification.notification_icon_id = targeted_notification_params[:notification_icon_id]
    end

    @notification.notification_icon = NotificationIcon.new(file: params[:file]) if params[:file]

    if @notification.update(targeted_notification_params)
      flash[:success] = "You have successfully updated the notification"
      redirect_to admin_notification_path(@notification)
    else
      render :edit
    end
  end

  def new
    @notification = TargetedNotification.new
  end

  def create
    @notification = TargetedNotification.new(targeted_notification_params)
    @notification.person = current_user

    if targeted_notification_params[:notification_icon_id]
      @notification.notification_icon_id = targeted_notification_params[:notification_icon_id]
    end

    @notification.notification_icon = NotificationIcon.new(file: params[:file]) if params[:file]

    if @notification.save
      flash[:success] = "Successfully created a notification"
      redirect_to admin_notification_path(@notification)
    else
      render :new
    end
  end

  def destroy
    @notification = TargetedNotification.find(params[:id])

    if @notification.destroy
      flash[:success] = "Successfully deleted the notification"
      redirect_to admin_notifications_path
    else
      flash[:error] = "Could not delete the notification"
      render :show
    end
  end

  def add_people
    @notification = TargetedNotification.find(params[:id])

    @new_notifications = @notification.add_people_from_csv(params[:add_people_csv])

    if @new_notifications != false
      flash[:success] = "Successfully added #{@new_notifications.size} from CSV"
      redirect_to admin_notification_path(@notification)
    else
      flash[:error] = "Cannot add people to global notifications"
      redirect_to admin_notification_path(@notification)
    end
  end

  private

  def targeted_notification_params
    params
      .require(:targeted_notification)
      .permit(
        :text,
        :url,
        :global,
        :title,
        :link_text,
        :notification_icon_id
      )
  end

  def require_notification_role
    return if current_user.has_role?("Notifications")

    flash[:error] = "You do not have access to this function"
    redirect_to admin_home_path
  end
end
