class Admin::MassMonetizationBlocksController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :load_mass_monetization_block_form

  def show
    @page_title = "Mass Monetization Block Tool"
  end

  def create
    if @mass_monetization_block_form.save
      redirect_to admin_mass_monetization_block_path
    else
      render :show
    end
  end

  private

  def load_mass_monetization_block_form
    @mass_monetization_block_form = MassMonetizationBlockForm.new(mass_monetization_block_params_hash)
  end

  def mass_monetization_block_params_hash
    return unless mass_monetization_block_params

    mass_monetization_block_params.merge(
      {
        ip_address: request.remote_ip,
        admin: current_user
      }
    )
  end

  def mass_monetization_block_params
    params[:mass_monetization_block_form]
  end

  def redirect_to_admin_index
    redirect_to admin_home_path
  end
end
