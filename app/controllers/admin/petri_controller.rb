class Admin::PetriController < Admin::AdminController
  # 2012-08-21 AK -- to get around http/https restrictions
  # The webservice is hosted in AWS using ElasticBeanstalk, code is at https://tunecore.unfuddle.com/a#/repositories/33618/browse
  def itunes_status
    upc = params[:upc]
    begin
      url = URI.parse("http://applestatus-env-frqmgq3nki.elasticbeanstalk.com/upc/#{upc}")
      req = Net::HTTP::Get.new(url.path)
      res = Net::HTTP.start(url.host, url.port) { |http| http.request(req) }
    rescue => e
      render plain: "{\"errors\":[\"Error connecting to web service: #{e.message}\"]}"
      logger.error "Getting Apple status #{e.message}"
    end
    # update the album's live status if possible
    begin
      album = Album.find_upc(params[:upc])
      json_data = JSON::load(res.body)
      state = json_data["response"]["state"]
      apple_id = json_data["response"]["apple_id"]
      if album != nil
        if album.apple_identifier.nil?
          album.apple_identifier = apple_id
          album.set_external_id_for("apple", apple_id)
        end
        album.known_live_on = Time.now if album.known_live_on.nil?
        album.save!
      end
    rescue => e
      logger.error("Updating album known_live_on or apple_id: #{e.message}")
    end
    render plain: res.body
  end

  def retry_distributions
    @new_css    = true
    @page_title = "Mass Distribution Retry Tool"
    @stores     = Store.mass_retry_unpaused_album_stores
  end

  def retry_distributions_create
    @distro = ::Distribution::MassRetryForm.new(mass_retry_params)

    if @distro.save
      Rails.logger.info("MASSRETRIES: Distribution::MassRetryForm.save called by #{current_user} with #{mass_retry_params.inspect}")
      success_msg =
        ["Successfully retrying distributions"].tap do |arr|
          arr << "with only TC-Distributor SNS" if use_sns_only?
        end.compact.join(" ")

      flash[:success] = success_msg
    else
      flash[:error] = @distro.errors.messages.values.join(" ")
    end

    redirect_to admin_petri_retry_distributions_path
  end

  def status
    @queue_size = 0
    @worker_size = 0
  end

  def show_metadata
    distro_class = params[:distro_class] || "Distribution"
    return unless ["DistributionSong", "Distribution"].include?(distro_class)

    distro = eval(distro_class).find(params[:distro_id])
    url = distro.archived_metadata_url
    redirect_to url
  end

  private

  def mass_retry_params
    {
      person_id: current_user.id,
      album_ids_or_upcs: album_ids_or_upcs,
      store_ids: group_store_ids,
      remote_ip: request.remote_ip,
      job_name: params[:job_name],
      metadata_only: params[:metadata_only].presence,
      daily_retry_limit: params[:daily_retry_limit].presence
    }.tap do |retry_params|
      retry_params[:retry_in_batches] = !ActiveModel::Type::Boolean.new.cast(params[:retry_in_batches]).nil?
      retry_params[:use_sns_only]     = params[:use_sns_only] == "on"
    end
  end

  def album_ids_or_upcs
    if params[:import_csv].present?
      CSV.read(params[:filename].path).flatten
    else
      params[:album_ids_or_upcs].split(",")
    end
  rescue
    []
  end

  def group_store_ids
    store_ids = Array(params[:store_ids])
    store_ids += Store::ITUNES_STORE_IDS if params[:itunes].present?
    store_ids
  end

  def get_correct_store_ids(store_ids)
    if store_ids.include?("all")
      :all
    else
      store_ids += Store::ITUNES_STORE_IDS if store_ids.include?("iTunes")
      store_ids.map(&:to_i)
    end
  end

  def match_either(str1, str2)
    str1 =~ /#{Regexp.escape(str2)}/ || str2 =~ /#{Regexp.escape(str1)}/
  end

  def use_sns_only?
    params[:use_sns_only] == "on"
  end
end
