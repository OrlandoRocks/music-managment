class Admin::BatchBalanceAdjustmentsController < Admin::AdminController
  layout "admin"

  before_action :load_rights, only: [:new]
  before_action :redirect_if_cant_refund, only: [:create, :post_adjustments]

  def index
    @batch_balance_adjustments = BatchBalanceAdjustment.all
  end

  def create
    filename = params[:file].original_filename
    path = "#{BATCH_BALANCE_ADJUSTMENTS_UPLOAD_PATH}/#{filename}"
    File.open(path, "wb") { |f| f.write(params[:file].read) }
    begin
      @batch_balance_adjustment = BatchBalanceAdjustment.ingest_file(filename)
    rescue StandardError => e
      flash[:error] = "File ingestion failed: #{e.message}"
      redirect_to action: "new" and return
    end

    redirect_to admin_batch_balance_adjustment_path(@batch_balance_adjustment)
  end

  def show
    @batch_balance_adjustment = BatchBalanceAdjustment.find params[:id]
    @category                 = ["Songwriter Royalty", "YouTube MCN Royalty"]
    @period_year_options      = ((Time.now.year - 1)..Time.now.year).collect { |y| [y, y] }
    @period_type_options      = [["quarterly", "quarterly"], ["monthly", "monthly"]]
    @period_interval_options  = (1..12).collect { |i| [i, i] }
  end

  def post_adjustments
    posting_periods = {
      period_interval: params[:period_interval],
      period_type: params[:period_type],
      period_year: params[:period_year],
      category: params[:category]
    }
    adjustment_note = generate_adjustment_note(posting_periods)
    options = {
      customer_note: adjustment_note,
      admin_note: adjustment_note,
      category: params[:category],
      exchange_rate: params[:exchange_rate]
    }.merge!(posting_periods)

    BatchBalanceAdjustment::PostAdjustmentWorker.perform_async(params[:id], current_user.id, options)
    flash[:notice] = "Batch Balance Adjustments submitted. You will be email once its completed."
    redirect_to admin_batch_balance_adjustments_path
  end

  private

  def load_rights
    @user_can_refund = current_user.has_role?("Refunds")
  end

  def redirect_if_cant_refund
    redirect_to controller: :batch_balance_adjustments,
                action: :no_permission,
                layout: "admin_application_lightbox" unless current_user.has_role?("Refunds")
  end

  def generate_adjustment_note(posting_periods)
    period_text = "QTR" if posting_periods[:period_type] == "quarterly"
    period_name = get_period_name posting_periods
    type        = get_posting_type posting_periods[:category]

    "#{type} - #{period_name} #{period_text} #{posting_periods[:period_year]}"
  end

  def get_period_name(posting_periods)
    return posting_periods[:period_interval].to_i.ordinalize if posting_periods[:period_type] == "quarterly"

    Date::MONTHNAMES[posting_periods[:period_interval].to_i]
  end

  def get_posting_type(category)
    return "MCN Royalties" if category == "YouTube MCN Royalty"

    "Publishing Royalties"
  end
end
