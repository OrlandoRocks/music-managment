class Admin::CompositionsController < Admin::AdminController
  layout "tc_foundation_admin"

  before_action :load_composer
  before_action :validate_feature_enabled
  before_action :set_hidden, except: :update

  def index
    @compositions = ::PublishingAdministration::CompositionsBuilder
                    .build(search_params)
                    .paginate(page: params[:page], per_page: 25)
  end

  def search
    @compositions = ::PublishingAdministration::CompositionsBuilder
                    .build(search_params)
                    .paginate(page: params[:page], per_page: 25)

    render "index", params: search_params
  end

  def update
    @composition = Composition.find_by(id: params[:id])

    @error_msg =
      if @composer.provider_identifier.present? && @composition.update(composition_params)
        @composition.send_to_rights_app
      else
        "Unable to send composition to Rights App"
      end
  end

  def update_status
    @composition = Composition.find_by(id: params[:id])
    @composition.unhide!
  end

  def update_eligibility
    @composition = Composition.find_by(id: params[:id])

    if @composition.ineligible?
      @composition.qualify!
    else
      @composition.disqualify!
    end
  end

  def download_csv
    @compositions = ::PublishingAdministration::CompositionsBuilder.build(search_params)
    csv = Admin::CompositionsCsvService.create(@compositions)
    send_data(csv, type: "text/csv; charset=utf-8; header=present", filename: "#{@composer.id}_compositions.csv")
  end

  private

  def search_params
    params.permit(:composer_id, :composition_name, :hidden)
  end

  def composition_params
    params
      .require(:composition)
      .permit(:name, :translated_name, :provider_identifier)
  end

  def load_composer
    @composer = Composer.find(params[:composer_id])
  end

  def validate_feature_enabled
    return if FeatureFlipper.show_feature?(:edit_composer_compositions, current_user)

    redirect_to admin_composers_manager_path(@composer.account)
  end

  def set_hidden
    @hidden = params[:hidden].eql?("true")
  end
end
