class Admin::TerminatedComposersController < Admin::AdminController
  include TerminatedComposerHelper

  before_action :parse_params, except: :destroy
  before_action :load_termination_form
  before_action :account_id, except: :destroy

  def create
    @termination_form.save
    redirect_to_composers_manager
  end

  def update
    @termination_form.save
    redirect_to_composers_manager
  end

  def destroy
    account_id = @termination_form.terminated_composer.account.id
    @termination_form.destroy

    redirect_to admin_composers_manager_path(account_id)
  end

  private

  def account_id
    @account_id = Composer.find(@params[:composer_id]).account_id
  end

  def load_termination_form
    form_params = @params || { terminated_composer: TerminatedComposer.find(params[:id]) }
    @termination_form = ::PublishingAdministration::TerminateComposerForm.new(form_params)
  end

  def parse_params
    @params = termination_params(params)
  end

  def redirect_to_composers_manager
    flash[:error] = @termination_form.errors[:effective_date].first if @termination_form.errors.any?
    flash_success_notice unless @termination_form.errors.any?
    redirect_to admin_composers_manager_path(@account_id)
  end
end
