class Admin::PromotionalProductsController < Admin::AdminController
  def index
    @non_current_products = PromotionalProduct.where("is_deleted = false AND online = false").order(start_at: :desc)
    @new_css = true

    render action: "current"
  end

  def current
    @new_css = true
    @page_title = "current products"
  end

  def past
    @new_css = true
    @page_title = "past products"
  end

  def deleted
    @new_css = true
    @page_title = "Deleted Products"

    @deleted_products = PromotionalProduct.where(is_deleted: true).order(start_at: :desc)
  end

  def product
    @new_css = true
    @product = PromotionalProduct.new

    @product = PromotionalProduct.find(params[:id]) || PromotionalProduct.new if params[:id]

    return unless request.post?

    @product.attributes = params[:product]

    return unless @product.save

    flash[:notice] = "The product '#{@product.title}' has been saved"
    redirect_to("/admin/store")
  end

  def delete_product
    if request.post?
      product = PromotionalProduct.find(params[:id])
      product.update_attribute(:is_deleted, true)
    end

    redirect_to "/admin/store"
  end

  def restore_product
    if request.post?
      product = PromotionalProduct.find(params[:id])
      product.update_attribute(:is_deleted, false)
    end

    redirect_to "/admin/store"
  end

  def order
    params[:products].each_with_index { |id, idx| PromotionalProduct.update(id, position: idx) }
    render(partial: "products")
  rescue
    render plain: "there was an error"
  end
end
