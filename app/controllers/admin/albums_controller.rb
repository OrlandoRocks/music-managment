# frozen_string_literal: true

class Admin::AlbumsController < Admin::AdminController
  include ControllerModule::SalepointSongs
  include TimedReleasable

  before_action :load_rights, only: [:show]
  before_action :raise_404_if_cant_mark_as_free, only: [:mark_song_as_free]
  before_action :load_album, only: [:copyright_documents, :create_copyright_document]
  before_action :validate_copyright_document, only: [:create_copyright_document]
  before_action :authorize_album_da_download, only: :recover_s3_assets

  def index
    @search_items = []
    render action: "search"
  end

  def edit
    @album = Album.find(params[:id])
  end

  def list_metrics
    metric = params[:metric]
    conditions = params[:conditions]
    @album_ids = Album.send(metric.to_s, conditions)
    @album_ids = @album_ids[0, 10]
    @albums = Album.find(@album_ids)
    @search_field = "#{metric}(#{conditions})"
    @search_text =  "Showing top 10..."
    @search_items = Album.where(@albums.map(&:id))
                         .order("albums.id DESC")
                         .paginate(page: params[:page], per_page: (params[:per_page] || 10).to_i)
                         .all
    render action: "search"
  end

  def tracks
    @uploadURL    =   BIGBOX_UPLOAD_URL
    @registerURL  =   BIGBOX_REGISTER_URL
    @assetsURL    =   BIGBOX_ASSETS_URL

    @album = Album.find(params[:id])
    @sso_cookie_value = generate_sso_cookie_value(@album.person_id)
  end

  def show
    @album = Album.find(params[:id])
    if @album.petri_bundle
      @distributions = @album.petri_bundle.distributions.preload(distribution_songs: [salepoint_song: [:song]])
    end
    @sso_cookie_value = generate_sso_cookie_value(@album.person_id)
    @album_state = @album.dismissed?
    @album_notes = @album.notes.includes(:related, :note_created_by)
    @song_notes = @album.eager_song_notes.reverse
    @song_creatives = @album.eager_song_creatives
    @old_bundles = PetriBundle.where(["album_id = ? AND state = 'dismissed'", @album.id]).order("id DESC")
    @renewal = Renewal.renewal_for(@album)
    @renewal_notes = Note.where("related_id = ? AND related_type = 'Album' AND (subject='Album Renewal - Do Not Renew' OR subject='Album Renewal - Reactivate')", @album.id).order("id DESC")
    @renewal_history = RenewalHistory.where({ renewal_id: @renewal.id }).order("starts_at DESC") unless @renewal.nil?
    @creatives = @album.creatives
    @album_itunes_status = @album.album_itunes_status || AlbumItunesStatus.new
    @golive_date = convert_golive_to_est(@album.golive_date)

    if FeatureFlipper.show_feature?(:tc_distributor_metadata, current_user)
      monetization_song_ids = @album.track_monetizations.map(&:song_id).uniq
      tc_distributor_metadata = TcDistributor::Base.new(album_id: @album.id, distributions: @distributions, monetization_song_ids: monetization_song_ids).tc_distributor_metadata

      @latest_release = tc_distributor_metadata[:latest_release]
      @latest_releases_stores = tc_distributor_metadata[:latest_releases_stores]
      @latest_releases_stores_tracks = tc_distributor_metadata[:latest_releases_stores_tracks]
    end

    respond_to do |format|
      format.html { render action: "show" }
      format.json { render json: @album.to_json }
    end

    # Allow the user to send this album to the bigbox system as a Zip archive.  It's a secret! (ak 2010-10-12)
    return unless params[:send_to_bigbox]

    logger.info "Sending album #{@album.id} to bigbox with prefix #{current_user.id}"
    begin
      @album.send_to_bigbox(current_user.id)
      flash[:notice] = "Album has been queued to send to Bigbox"
    rescue => e
      logger.error "Couldn't send album to bigbox: #{e.message}"
      flash[:error] = "This album can't be sent to Bigbox (perhaps it was uploaded there originally)"
    end
  end

  def edit_renewal
    @album = Album.find(params[:id])
    render layout: "admin_application_lightbox"
  end

  def update_renewal
    params[:duration], params[:interval] = params[:duration_interval].split(" ") if params[:duration_interval]
    @album = Album.find(params[:id])
    # if the change was a success
    if @album.change_renewal_interval(params[:interval], params[:duration])
      Note.create(
        subject: "Album renewal interval updated",
        note: "Album renewal period has been changed to #{params[:duration]} #{params[:interval].pluralize params[:duration]}",
        related_id: @album.id,
        related_type: "Album",
        note_created_by_id: current_user.id,
      )
      flash.now[:notice] = "Album renewal period has been changed to #{params[:duration]} #{params[:interval].pluralize params[:duration]}"
    else
      flash.now[:notice] = "Album renewal period was not edited, something went wrong"
    end
    render layout: "admin_application_lightbox"
  end

  def send_to_bigbox
    # Allow the user to send this album to the bigbox system as a Zip archive.  It's a secret! (ak 2010-10-12)
    @album = Album.find(params[:id])
    logger.info "Sending album #{@album.id} to bigbox with prefix #{current_user.id}"
    if @album.send_to_bigbox(current_user.id)
      flash[:notice] = "Album has been queued to send to Bigbox"
    else
      flash[:error] = "Couldn't send album to Bigbox - maybe it was uploaded there originally?"
    end
    redirect_back fallback_location: home_path
  end

  def add_creative
    case params[:poly]
    when "Album"
      album = Album.find(params[:id])
      album.send("add_#{params[:type_of].underscore}".to_sym, params[:name])
      album.save # calls the validation which updates the autoformatting on the title
      redirect_to(action: "show", id: params[:id])
    when "Song"
      song = Song.find(params[:id])
      song.send("add_#{params[:type_of].underscore}".to_sym, params[:name])
      song.save # calls the before validation
      redirect_to(action: "show", id: song.album.id)
    end
  end

  def remove_creative
    creative = Creative.find(params[:id])
    creative.destroy

    x = creative.creativeable_type.constantize.find(creative.creativeable_id)
    x.save # calls the autotitization

    redirect_to(action: "show", id: params[:album_id])
  end

  def unfinalize
    @album = Album.find(params[:id])
    if @album.unfinalize(message: "Album was unfinalized.", person: current_user)
      flash[:notice] = "Removed finalization from album."
    else
      flash[:error] = "Unable to unfinalize album."
    end
    redirect_to action: "show", id: @album.id
  end

  def finalize
    @album = Album.find(params[:id])
    if @album.finalize(message: "Album was finalized.", person: current_user)
      @album.update_review_status(params[:review])
      flash[:notice] = "Album's finalized state has been set.  Re-queue any bundles that you need to send out."
    else
      flash[:error] = "Unable to finalize album."
    end
    redirect_to action: "show", id: @album.id
  end

  def remove_optional_upcs
    @album = Album.find(params[:id])

    if @album.has_possibly_been_finalized?
      flash[:notice] = "This albums optional upcs cannot be removed"
    else
      @album.remove_optional_upcs
    end
    redirect_to action: "show", id: @album.id
  end

  def mass_unfinalize_finalize
    @successful_albums = []
    @failed_albums = []
    return unless params[:album_ids]

    @selected_albums = params[:album_ids]
    unless @selected_action = params[:selected_action]
      flash.now[:error] = "You must select an action to perform on these albums."
      return
    end
    @albums = BulkAlbumFinder.new(@selected_albums).execute
    @albums.each do |album|
      if album.send(params[:selected_action].to_s.to_sym, { message: "Mass #{params[:selected_action]} request." })
        @successful_albums << album
      else
        @failed_albums << album
      end
    end
    @successful_albums.each { |album| album.update_review_status(params[:review]) } if @selected_action == "finalize"
  end

  def swap_genres
    @album = Album.find(params[:id])

    if @album.finalized?
      flash[:notice] = "You must unfinalize this album first"
    else
      primary_id = @album.primary_genre_id
      @album.update_attribute(:primary_genre_id, @album.secondary_genre_id)
      @album.update_attribute(:secondary_genre_id, primary_id)
    end

    redirect_to action: "show", id: @album.id
  end

  def set_song_parental_advisory
    song = Song.find(params[:id])
    song.parental_advisory = !song.parental_advisory
    if song.save
      flash[:success] = "Successfully updated parental advisory"
    else
      flash[:error] = "You cannot set song to Explicit, if it is marked as Clean"
    end

    respond_to do |format|
      format.html {
        redirect_to admin_album_path(song.album)
      }
    end
  end

  def set_album_optional_upc
    album = Album.find(params[:id])
    album.optional_upcs << Upc.new(number: params[:value], upc_type: "optional")
    render plain: album.optional_upc.number
  end

  def set_song_clean_version
    song = Song.find(params[:id])
    song.clean_version = !song.clean_version
    if song.save
      flash[:success] = "Successfully updated clean flag"
    else
      flash[:error] = "You cannot set song to Clean, if it is marked as Explicit"
    end

    redirect_to admin_album_path(song.album)
  end

  def set_album_label_name
    album = Album.find(params[:id])

    if params[:value].present?
      label = Label.find_or_create_by(name: params[:value])
      temp = params[:value]
      album.label_id = label.id if label.name.present?
      album.save
    end

    render(text: album.label_name)
  end

  def generate_album_tunecore_upc
    album = Album.find(params[:id])
    return if album.finalized?

    if album.assign_new_tunecore_upc!
      respond_to do |format|
        format.html {
          flash[:success] = "Successfully generated a new tunecore upc"
          redirect_to admin_album_path(album)
        }
      end
    else
      respond_to do |format|
        format.html {
          flash[:error] = "Error creating a new tunecore upc"
          redirect_to admin_album_path(album)
        }
      end
    end
  end

  # set album undeleted
  def set_album_as_undeleted
    album = Album.find(params[:id])
    album.undelete
    flash[:notice] = "Album #{album.id} undeleted"
    redirect_to action: "show", id: album.id
  end

  # Set Customer Release Date
  def set_album_sale_date
    album = Album.find(params[:id])
    old_date = album.sale_date
    album.sale_date = Date.strptime(params[:value], "%Y-%m-%d")
    album.release_date_changes(current_user, "sale_date", "Customer Release Date")
    if album.save
      album.add_note_for_release_date_change(current_user, "sale_date")
      render plain: album.sale_date.to_s
    else
      render plain: old_date.to_s
    end
    # handles empty value
  rescue ArgumentError => e
    render plain: old_date.to_s
  end

  # Set Orig. Release Year
  def set_album_orig_release_year
    album = Album.find(params[:id])
    old_date = album.orig_release_year
    album.orig_release_year = Date.parse(params[:value])
    album.release_date_changes(current_user, "orig_release_year", "Orig. Release Year")
    if album.save
      album.add_note_for_release_date_change(current_user, "orig_release_year")
      render plain: album.orig_release_year.to_s
    else
      render plain: old_date.to_s
    end
    # handles empty value
  rescue
    render plain: old_date.to_s
  end

  # Set Timed Release Date
  def set_album_timed_release_date
    begin
      @album = Album.find_by(id: params[:id])
      @album.assign_attributes(album_date_params)
      @album.release_date_changes(current_user, "golive_date", "Customer Release Date")
      @album.save
      @album.add_note_for_release_date_change(current_user, "golive_date")
      @golive_date = convert_golive_to_est(@album.golive_date)
    rescue ArgumentError => e
      @error = "Invalid Date, enter valid date and try again."
    end
  end

  def set_album_name
    album = Album.find(params[:id])
    old_name = album.name
    album.name = params[:value]
    if album.save
      render plain: album.name
    else
      render plain: old_name
    end
  end

  def audit_s3_assets
    @album = Album.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def dismiss_bundle
    @album = Album.find(params[:album_id])
    @bundle = @album.petri_bundle

    @bundle.dismiss(actor: "Tunecore admin: #{current_user.name}", message: "Dismissing bundle") unless @bundle.nil?

    @album.reload
    @bundle.reload

    respond_to do |format|
      format.html {
        flash[:success] = "Album dismissed successfully.  Bundle updated and shown below."
        redirect_to admin_album_path(@album)
      }
    end
  end

  def bundle_album
    @album = Album.find(params[:album_id])
    begin
      @bundle = PetriBundle.recreate!(@album, { actor: "Tunecore admin: #{current_user.name}", message: "Recreating bundle" })
    rescue StandardError => e
      error_message = e.message
    end

    @album.reload
    if error_message
      respond_to do |format|
        format.html {
          flash[:error] = "Could not bundle album due to #{error_message}.  Album does not meet requirements.  The only requirement that can be ignored, if you choose to force a bundle, is payment."
          redirect_to admin_album_path(@album)
        }
      end
    else
      respond_to do |format|
        format.html {
          flash[:success] = "Album bundled successfully.  New bundle shown below."
          redirect_to admin_album_path(@album)
        }
      end
    end
  end

  def recover_s3_assets
    @songs = []
    @artwork = nil
    id = params[:id] || nil
    album = Album.find(id)
    @album = album
  end

  def flac
  end

  def destroy_flac
    album = params[:album_id].to_i
    if album.nil? || album.zero?
      redirect_to controller: "admin/albums", action: "flac"
      flash[:error] = "You didn't enter a valid album id!"
    else
      @album = Album.find(album)
      @album.songs.each { |s| s.s3_flac_asset.destroy unless s.s3_flac_asset.nil? }
      flash.now[:notice] = "Destroyed Flac For Album"
    end
  end

  def search
    @new_css = true

    unless params[:term] =~ /^\s*(id|upc|isrc|name|label|artist|apple_id):\s*(.*)$/
      redirect_to(controller: "admin/dashboard") and return
    end

    @search_field = $1
    @search_text = $2

    @search_items = Album::SearchQueryBuilder.build(@search_field, @search_text)
                                             .paginate(
                                               page: params[:page],
                                               per_page: (params[:per_page] || 50).to_i
                                             ).all

    redirect_to controller: "albums", action: "show", id: @search_items.first.id if @search_items.count.size == 1
  end

  def change_album_format_flag
    @album = Album.find(params[:id])

    respond_to do |format|
      format.html {
        if @album.flip_format_flag
          if @album.has_non_formattable_language_code?
            flash[:error] = "Auto-formatting is not available for this album because it's language code is '#{@album.language_code}'"
          else
            flash[:success] = "Album and song names are#{' not' if @album.allow_different_format} auto-formatted"
          end
        end

        redirect_to admin_album_path(@album)
      }
    end
  end

  def salepoint_takedown
    sp = Salepoint.find(params[:id])
    sp.takedown!
    @note = Note.create(related: sp.salepointable, note_created_by: current_user, ip_address: request.remote_ip, subject: "Salepoint Takedown", note: "#{sp.store_name} was taken down")

    # if salepoint is itunes then send a takedown distribution. other stores send out a takedown distribution in salepoint model
    sp.send_metadata_update(message = "issuing a takedown distribution") if sp.store.short_name.include?("iTunes")

    send_sns_notification_takedown(sp)
    redirect_to action: "show", id: sp.salepointable_id
  end

  def remove_salepoint_takedown
    sp = Salepoint.find(params[:id])
    # if salepoint is itunes retry distribution
    sp.remove_takedown
    @note = Note.create(related: sp.salepointable, note_created_by: current_user, ip_address: request.remote_ip, subject: "Salepoint Takedown", note: "#{sp.store_name} takedown was cleared")

    # if salepoint is itunes then send a reactivation distribution. other stores send out a reactivation distribution in salepoint model
    sp.send_metadata_update(message = "issuing a reactivation distribution") if sp.store.short_name.include?("iTunes")

    send_sns_notification_untakedown(sp)
    redirect_to action: "show", id: sp.salepointable_id
  end

  def show_edit_renewal_beginning
    @album = Album.find(params[:id])
    last_start = Renewal.renewal_for(@album).created_at

    respond_to do |format|
      format.html {
        render partial: "edit_renewal_beginning", locals: { album: @album, last_start: last_start }, status: :ok
      }
    end
  end

  def update_renewal_date
    album = Album.find(params[:id])
    Renewal.adjust_expiration_date_for(album, params[:renewal_due_on].to_date)
    renewal = Renewal.renewal_for(album)

    respond_to do |format|
      format.html { redirect_to action: "show", album_id: album.id }
      format.js {
        render partial: "annual_fees",
               locals: {
                 album: album,
                 renewal: renewal,
                 renewal_history: RenewalHistory.where({ renewal_id: renewal.id }).order("starts_at DESC"),
                 renewal_notes: Note.where("related_id = ? AND related_type = 'Album' AND (subject='Album Renewal - Do Not Renew' OR subject='Album Renewal - Reactivate')", album.id).order("created_at DESC")
               },
               content_type: "text/html"
      }
    end
  end

  def create_booklet
    @album = Album.find(params[:id])
    Booklet.transaction do
      booklet = Booklet.create!(album: @album)
      asset = S3Asset.new(bucket: BOOKLET_BUCKET_NAME, key: booklet.s3_key, filename: params[:booklet_file].path)
      asset.put!(file: params[:booklet_file].tempfile)
      asset.save
      booklet.s3_asset = asset
      booklet.save
    end

    Product.add_to_cart(@album.person, @album.booklet) if @album.booklet.can_distribute?
    redirect_to action: "show", id: @album
  end

  def replace_booklet
    @album = Album.find(params[:id])
    booklet = @album.booklet
    flash[:error] = "Failed to upload booklet. Please Resubmit" unless booklet.replace_booklet(params[:booklet_file])

    redirect_to action: "show", id: @album
  end

  def delete_booklet
    @album = Album.find(params[:id])
    booklet = @album.booklet
    booklet.destroy
    purchase = @album.person.purchases.select { |x| x.related_id == booklet.id and x.related_type == "Booklet" }
    purchase.first.destroy if purchase.length.positive?
    redirect_to action: "show", id: @album
  end

  def change_status
    @song = Song.find(params[:id])
    @note = Note.new
    render layout: "admin_application_lightbox"
  end

  def remove_all_tracks_as_free
    @album = Album.find(params[:id])
    @album.songs.each do |song|
      song.free_song = false
    end
    @note = Note.create(related: @album, note_created_by: current_user, subject: "Free Song", note: "All songs are no longer marked as free")
    redirect_to action: "show", id: @album
  end

  def mark_all_tracks_as_free
    @album = Album.find(params[:id])
    @album.songs.each do |song|
      song.free_song = true
    end
    @note = Note.create(related: @album, note_created_by: current_user, subject: "Free Song", note: "All songs are marked as free")
    redirect_to action: "show", id: @album
  end

  def copyright_documents
    @copyright_documents = @album.copyright_documents.includes(:s3_asset).order("s3_assets.id desc")
  end

  def create_copyright_document
    document_uploaded = CopyrightDocument.upload_document(@album, params[:copyright_document])
    document_uploaded ? flash[:success] = "CopyrightDocument Uploaded Successfully." : flash[:error] = "Failed to upload document, please contact support."
    redirect_to copyright_documents_admin_person_album_path(@person, @album)
  end

  def download_xml_metadata
    xml_metadata = TcDistributor::Base.new(releases_store_id: params[:releases_store_id]).xml_metadata
    redirect_to s3_read_url(xml_metadata) if xml_metadata.present?
  end

  def download_json_metadata
    json_metadata = TcDistributor::Base.new(release_id: params[:release_id]).json_metadata
    redirect_to s3_read_url(json_metadata) if json_metadata.present?
  end

  private

  def send_sns_notification_takedown(salepoint)
    salepoint&.send_sns_notification_takedown(current_user)
  end

  def send_sns_notification_untakedown(salepoint)
    return unless FeatureFlipper.show_feature?(:use_sns, current_user) &&
                  ENV.fetch("SNS_RELEASE_UNTAKEDOWN_URGENT_TOPIC", nil).present?

    Sns::Notifier.perform(
      topic_arn: ENV.fetch("SNS_RELEASE_UNTAKEDOWN_URGENT_TOPIC"),
      album_ids_or_upcs: [salepoint.salepointable_id],
      store_ids: [salepoint.store_id],
      delivery_type: "untakedown",
      person_id: current_user.id
    )
  end

  def load_rights
    @user_can_mark_song_as_free = current_user.has_role?("Distribution Manager")
  end

  def raise_404_if_cant_mark_as_free
    raise ActiveRecord::RecordNotFound unless current_user.is_permitted_to?(params[:controller], params[:action])
  end

  def load_album
    @person = Person.find(params[:person_id])
    @album = @person.albums.where(id: params[:id]).first
    redirect_to admin_person_path(@person) if !@person.india_user? || @album.nil?
  end

  def validate_copyright_document
    uploaded_doc = params[:copyright_document]
    return if uploaded_doc.blank?

    valid_doc = CopyrightDocument::VALID_CONTENT_TYPES.include?(uploaded_doc.content_type)
    return if valid_doc

    flash[:error] = "File Format is not supported, kindly upload 'pdf/doc/jpg'"
    redirect_to copyright_documents_admin_person_album_path(@person, @album)
  end

  def timed_release_params
    params
      .require(:timed_release_attributes)
      .permit(
        :timed_release_timing_scenario,
        :golive_date
      )
  end

  def album_date_params
    date = DateTime.parse(timed_release_params[:golive_date])
    golive_date = { month: date.month, day: date.day, year: date.year, hour: date.hour, min: date.minute }
    new_golive_date = convert_date_hash_to_golive_date(golive_date)
    new_sale_date   = convert_date_hash_to_sale_date(golive_date)
    timed_release_params.merge(
      golive_date: new_golive_date,
      sale_date: new_sale_date
    )
  end

  def authorize_album_da_download
    return if current_user.is_download_assets_admin?

    flash[:alert] = "You are not authorized to do this."
    redirect_back fallback_location: home_path
  end

  def s3_read_url(s3_details)
    AwsWrapper::S3.read_url(
      bucket: s3_details["bucket"],
      key: s3_details["key"],
      options: {
        expires: 10.minutes
      }
    )
  end
end
