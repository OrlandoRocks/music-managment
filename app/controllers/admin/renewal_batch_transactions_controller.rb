class Admin::RenewalBatchTransactionsController < Admin::AdminController
  before_action :load_person
  before_action :set_search_defaults, only: [:index, :show]

  def index
    params[:search][:order] = "descend_by_batch_date"
    @search = PaymentBatch.search(params[:search])
    @search.relation = @search.relation.order("batch_date DESC")

    @renewal_batch_transactions = @search.paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
  end

  def show
    @payment_batch = PaymentBatch.find(params[:id])
    sanitize_matching_type

    @search = @payment_batch.batch_transactions.search(params[:search])
    @search.relation = @search.relation.order("created_at DESC")

    @transaction_details = @search.paginate(page: params[:page], per_page: (params[:per_page] || @per_page).to_i)
  end

  private

  def sanitize_matching_type
    params[:search][:matching_type] = nil if params[:search][:matching_type] == "None"
    params[:search].delete(:matching_type) if params[:search][:matching_type] == "All"
  end

  def set_search_defaults
    params[:search] ||= {}
    @per_page = 20
  end
end
