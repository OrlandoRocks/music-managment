class Admin::PublishingTerminatedComposersController < Admin::AdminController
  include PublishingTerminatedComposerHelper

  before_action :parse_params, except: :destroy
  before_action :load_termination_form
  before_action :account_id, except: :destroy

  def create
    @termination_form.save

    redirect_to_publishing_composers_manager
  end

  def update
    @termination_form.save

    redirect_to_publishing_composers_manager
  end

  def destroy
    account_id = @termination_form.terminated_composer.account.id

    @termination_form.destroy

    redirect_to redirection_path(account_id)
  end

  private

  def account_id
    @account_id = PublishingComposer.find(@params[:publishing_composer_id]).account_id
  end

  def load_termination_form
    form_params = @params || { terminated_composer: TerminatedComposer.find(params[:id]) }

    @termination_form = ::PublishingAdministration::TerminatePublishingComposerForm.new(form_params)
  end

  def parse_params
    @params = publishing_composer_termination_params(params)
  end

  def redirect_to_publishing_composers_manager
    flash[:error] = @termination_form.errors[:effective_date].first if @termination_form.errors.any?

    flash_success_notice unless @termination_form.errors.any?

    redirect_to redirection_path(@account_id)
  end

  def redirection_path(account_id)
    admin_publishing_composers_manager_path(account_id)
  end
end
