class Admin::DeleteAccountsController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :restricted_access_by_role

  def new
    @delete_account_form = DeleteAccountForm.new
  end

  def create
    @delete_account_form = DeleteAccountForm.new(delete_account_params)

    if @delete_account_form.save
      flash[:notice] = "Account #{@delete_account_form.person_id} as been 'deleted'"
      redirect_to new_admin_delete_account_path
    else
      render :new
    end
  end

  private

  def delete_account_params
    params
      .require(:delete_account_form)
      .permit(
        :person_id,
        :delete_type
      )
      .merge(
        admin: current_user
      )
  end

  def restricted_access_by_role
    redirect_to admin_home_path unless current_user.has_role?("Lock")
  end
end
