class Admin::CmsController < Admin::AdminController
  layout "admin_new"

  def index
    @page_title = "CMS"
    @active_sets = CmsSetFinder.active_sets.group_by(&:country_website_language)
    @upcoming_sets = CmsSetFinder.upcoming_sets
    @unscheduled_sets = CmsSetFinder.unscheduled_and_archived_sets
  end

  def new
    @page_title = "Create Promo"
    @cms_set = CmsSet.new.as_json
    @type = params[:type]
  end

  def create
    if params[:country_languages].blank?
      @cms_set = CmsSet.new.as_json
      @type = params[:type]
      flash[:error] = "No Country Selected"
      render "new"
      nil
    else
      cms_sets = []
      ActiveRecord::Base.transaction do
        params[:country_languages].each do |_country, country_language_id|
          country_website_language = CountryWebsiteLanguage.find(country_language_id)

          country_id =
            country_website_language.redirect_country_website_id ||
            country_website_language.country_website_id

          set = CmsSet.new(
            callout_name: params[:name],
            callout_type: params[:type],
            country_website_id: country_id,
            country_website_language_id: country_language_id,
          )
          cms_sets << set
          set.create_attributes(params[:asset]) if params[:asset]
        end
        cms_sets.map(&:save!)
        redirect_to admin_cms_url
      end
    end
  rescue
    @cms_set = CmsSet.new.as_json
    @type = params[:type]
    flash[:error] = cms_sets.collect(&:errors).map(&:full_messages).flatten.uniq.to_sentence
    render "new"
  end

  def update
    @cms_set = CmsSet.find(params[:id])
    begin
      @cms_set.update!(callout_name: params[:name]) if params[:name]
      @cms_set.update!(start_tmsp: params[:start_tmsp]) if params[:start_tmsp]
      @cms_set.update_cms_set_attributes(params[:asset]) if params[:asset]
      @cms_set.save!
    rescue StandardError => e
      flash.now[:error] = e.message
      @page_title = "Edit Promo"
      @type = params[:type]
      @cms_set = CmsSet.find(params[:id]).as_json(root: false)
      render :edit and return
    end
    flash[:success] = "Set edited successfully"
    redirect_to admin_cms_url
  end

  def activate
    flash[:error] = "Could not activate" unless CmsSet.find(params[:cm_id]).activate
    redirect_back fallback_location: admin_cms_url
  end

  def deactivate
    flash[:error] = "Could not deactivate" unless CmsSet.find(params[:cm_id]).deactivate
    redirect_back fallback_location: admin_cms_url
  end

  def edit
    @page_title = "Edit Promo"
    @type = params[:type]
    @cms_set = CmsSet.find(params[:id]).as_json(root: false)
  end

  def preview
    @cms_set = CmsSet.find(params[:cm_id]).as_json(root: false)
    case CmsSet.find(params[:cm_id]).callout_type
    when "login"
      @person = Person.new(email: "user@website.com")
      render "sessions/new", layout: "registration"
    when "dashboard"
      @main_body = "dashboard/state_3_should_finalize"
      @automator_count = 0
      @person = current_user
      @albums = @person.albums.full_albums.not_deleted.not_taken_down.by_finalized_asc.limit(50).includes(:artwork).all
      @singles = @person.albums.singles.not_deleted.not_taken_down.by_finalized_asc.limit(50).includes(:artwork).all
      @ringtones = @person.albums.ringtones.not_deleted.not_taken_down.by_finalized_asc.limit(50).includes(:artwork).all
      render "/dashboard/index", layout: "application_old"
    when "interstitial"
      render "interstitial/index", layout: "interstitial"
    else

    end
  end

  def destroy
    @cms_set = CmsSet.find(params[:id])

    if @cms_set.update(start_tmsp: nil)
      flash[:success] = "Start date removed for set #{@cms_set.callout_name}"
    else
      flash[:error] = "Date clearing failed"
    end

    redirect_to admin_cms_url
  end
end
