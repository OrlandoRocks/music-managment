class Admin::BulkAlbumApprovalsController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :load_bulk_album_approval_form

  def index
    redirect_to action: :new
  end

  def new
    @low_risk_albums = LowRiskArtistsQueryBuilder.build
  end

  def create
    if @bulk_album_approval_form.save
      redirect_to new_admin_bulk_album_approval_path
    else
      @low_risk_albums = LowRiskArtistsQueryBuilder.build
      render :new
    end
  end

  private

  def load_bulk_album_approval_form
    @bulk_album_approval_form = BulkAlbumApprovalForm.new(bulk_album_approval_params)
  end

  def bulk_album_approval_params
    bulk_album_approval_params = params[:bulk_album_approval_form]
    bulk_album_approval_params ? params.require(:bulk_album_approval_form).permit(:person_id, album_ids: []) : {}
  end
end
