class Admin::Payout::TransferRejectionsController < Admin::AdminController
  ALLOWED_ADMIN_ROLES = ["Payout Service", "EFT"].map(&:freeze).freeze

  before_action :admin_has_appropriate_role

  def create
    rejection_form = PayoutTransfer::RejectionForm.new(
      payout_transfer: payout_transfer
    )

    flash[:error] = rejection_form.errors.full_messages.join("&") unless rejection_form.save

    redirect_back fallback_location: admin_payout_payout_transfers_path
  end

  private

  def payout_transfer
    @payout_transfer ||= PayoutTransfer.find_by_client_reference_id!(params[:payout_transfer_id])
  end

  def admin_has_appropriate_role
    redirect_to admin_dashboard_index_path unless ALLOWED_ADMIN_ROLES.detect { |role| current_user.has_role?(role) }
  end
end
