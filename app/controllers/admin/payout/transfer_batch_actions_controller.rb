class Admin::Payout::TransferBatchActionsController < Admin::AdminController
  ALLOWED_ADMIN_ROLES = ["Payout Service", "EFT"].map(&:freeze).freeze

  before_action :admin_has_appropriate_role

  def create
    if FeatureFlipper.show_feature?(:auto_approved_withdrawals, current_user.id)
      response = Transfers::ApproveService.call(service_params.merge({ provider: "payoneer" }))
      errors = response[:errors]
    else
      errors = Payoneer::BatchActionsService.send_actions(service_params)
    end

    if errors.blank?
      flash[:notice] = "Successfully updated #{params[:selected_transfers]}"
    else
      flash[:error] = errors.join(",")
    end
    redirect_back fallback_location: home_path
  end

  private

  def service_params
    {
      action: params[:commit],
      transfer_ids: params[:selected_transfers],
      person: current_user,
      referrer_url: request.url
    }
  end

  def admin_has_appropriate_role
    redirect_to admin_dashboard_index_path unless ALLOWED_ADMIN_ROLES.detect { |role| current_user.has_role?(role) }
  end
end
