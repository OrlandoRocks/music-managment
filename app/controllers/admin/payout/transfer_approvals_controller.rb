class Admin::Payout::TransferApprovalsController < Admin::AdminController
  def create
    approval_form = PayoutTransfer::ApprovalForm.new(
      payout_transfer: PayoutTransfer.find_by_client_reference_id!(params[:payout_transfer_id]),
      admin: current_user
    )

    flash[:error] = approval_form.errors.full_messages.join("&") unless approval_form.save

    redirect_to admin_payout_payout_transfers_path(tunecore_status: PayoutTransfer::SUBMITTED)
  end
end
