class Admin::Payout::PayoutTransfersController < Admin::AdminController
  include PayoutProvidable

  before_action :restrict_access, unless: :payoneer_payout_enabled?

  TUNECORE_STATUSES = PayoutTransfer::TUNECORE_LIFECYCLE.map { |status| [status, status] }.insert(0, ["---", ""])
  PROVIDER_STATUSES = PayoutTransfer::PROVIDER_LIFECYCLE.map { |status| [status, status] }.insert(0, ["---", ""])

  def index
    @page_title = "Payout Transactions"

    if params[:commit] == "reset"
      redirect_to admin_payout_payout_transfers_path(tunecore_status: PayoutTransfer::SUBMITTED)
    end

    @filter_form = PayoutTransfer::AdminFilterForm.new(form_params)

    redirect_to admin_dashboard_index_path unless @filter_form.save
  end

  private

  def form_params
    params.permit(
      :tunecore_status,
      :provider_status,
      :filter_type,
      :filter_amount,
      :page,
      :withdrawal_method,
      :person_name,
      :pagination_limit,
      :currency,
      :start_date,
      :end_date,
      :rollback,
      :auto_approval_eligible
    )
  end
end
