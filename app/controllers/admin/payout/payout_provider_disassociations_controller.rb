class Admin::Payout::PayoutProviderDisassociationsController < Admin::AdminController
  def create
    @form = PayoutProvider::DisassociationForm.new(form_params)

    flash[:error] = @form.errors.full_messages.join(", ") unless @form.save
    redirect_to admin_person_path(id: params[:person_id])
  end

  private

  def form_params
    params.permit(:provider_id).merge(admin: current_user)
  end
end
