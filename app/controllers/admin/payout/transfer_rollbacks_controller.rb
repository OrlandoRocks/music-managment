class Admin::Payout::TransferRollbacksController < Admin::AdminController
  def index
    @transfers = PayoutTransfer
                 .rolled_back
                 .joins(payout_provider: :person)
                 .order(created_at: :asc)
                 .paginate(page: params[:page] || 1, per_page: 50)
  end
end
