class Admin::Payout::TaxFormsController < Admin::AdminController
  def create
    redirect_back fallback_location: home_path if person_params.blank?

    if check_tax_api?
      flash[:notice] = "Downloaded tax form for user"
    else
      flash[:error] = "Tax Form does not exist for user"
    end

    redirect_to admin_person_path(person_params[:person])
  end

  private

  def check_tax_api?
    TaxFormCheckService.check_api?(person_params[:person])
  end

  def person_params
    params.permit(:person)
  end
end
