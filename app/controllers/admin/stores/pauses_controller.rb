class Admin::Stores::PausesController < Admin::AdminController
  def create
    store = Store.eager_load(:store_delivery_config).find(params[:store_id])
    if store.unpaused
      store.store_delivery_config.pause!
      Delivery::StorePausingWorker.perform_async(params[:store_id])
    end

    redirect_to admin_stores_path
  end

  def destroy
    store = Store.eager_load(:store_delivery_config).find(params[:store_id])
    if store.paused
      store.store_delivery_config.unpause!
      Delivery::StoreUnpausingWorker.perform_async(params[:store_id], current_user.id, request.remote_ip)
    end

    redirect_to admin_stores_path
  end
end
