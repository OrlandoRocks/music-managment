class Admin::Stores::BackfillStartsController < Admin::AdminController
  def create
    store = Store.find(params[:store_id])
    StoreBackfillStartWorker.perform_async(store.id, params[:number_to_backfill])
    flash[:notice] = "Store Backfill for #{store.name} Started Successfully"

    redirect_to admin_stores_path
  end
end
