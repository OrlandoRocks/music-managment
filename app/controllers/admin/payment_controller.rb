class Admin::PaymentController < Admin::AdminController
  helper :admin_payment

  before_action :set_paper_trail_whodunnit, only: [:mass_pay_paypal]

  def index
    set_variables_for_index
    response = Admin::PaymentService.fetch_transfers(transfers_filter_params)
    flash.now[:error] = response[:error] if response[:error]
    @transfers = response[:transfers]
  end

  def process_payment
    @transfer = load_transfer
    unless @transfer.nil?
      PersonBalance.find_and_lock_balance(@transfer.person)
      if @transfer.reload.pending?
        @transfer.approving_transfer(current_user.id)
      else
        flash[:notice] = "That transfer is no longer 'pending', cannot change to 'processing'"
      end
    end
    redirect_to request.referrer || "/admin/payment"
  end

  def mass_pay_check
    check_transfers = ManualCheckTransfer.find(params[:transfer][:id])
    failed_check_transfers = []
    check_transfers.each do |transfer|
      PersonBalance.find_and_lock_balance(transfer.person)
      if transfer.reload.transfer_status == "pending"
        transfer.approving_transfer(current_user.id)
      else
        failed_check_transfers.push(transfer.id)
      end
    end
    flash[:notice] = failed_check_transfers.blank? ? "Payments completed successfully." : "The transfer ids of: #{failed_check_transfers.join(', ')} are no longer 'pending' and cannot change to 'processing'"
    redirect_to request.referrer || "/admin/payment"
  end

  def complete_payment
    @transfer = load_transfer
    unless @transfer.nil?
      PersonBalance.find_and_lock_balance(@transfer.person)
      if @transfer.reload.processing?
        @transfer.mark_as_completed!
      else
        flash[:notice] = "That transfer is not 'processing' and cannot be changed to 'completed'"
      end
    end
    redirect_to request.referrer || "/admin/payment"
  end

  def fail_payment
    @transfer = load_transfer
    unless @transfer.nil?
      if @transfer.reload.processing?
        PersonTransaction.create!(
          person: @transfer.person,
          debit: 0,
          credit: Tunecore::Numbers.cents_to_decimal(@transfer.total_cents),
          target: @transfer,
          comment: "Failed Payment: #{@transfer.person_transaction.comment}"
        )
        @transfer.mark_as_failed!
      else
        flash[:notice] = "That cannot be set to failed - its current status is #{@transfer.transfer_status}"
      end
    end
    redirect_to request.referrer || "/admin/payment"
  end

  def cancel_payment
    @transfer = load_transfer
    if !@transfer.nil? && !@transfer.cancel
      flash[:notice] = "That cannot be set to cancelled - its current status is #{@transfer.transfer_status}"
    end
    redirect_to request.referrer || "/admin/payment"
  end

  # TODO: remove PaypalTransfer.do_mass_pay, replace with Transfers::ApproveService.call
  def mass_pay_paypal
    if FeatureFlipper.show_feature?(:auto_approved_withdrawals, current_user.id)
      response = Transfers::ApproveService.call(mass_pay_service_params)
      errors = response[:errors]
    else
      paypal_transfers = PaypalTransfer.find(mass_pay_params[:id])
      errors = PaypalTransfer.do_mass_pay(paypal_transfers, current_user.id)
    end

    if errors.empty?
      flash[:notice] = "Payments completed successfully."
    else
      flash[:error]  = "Some payments not processed: " + errors.to_sentence
    end
    redirect_to request.referrer || "/admin/payment"
    # Exceptions should feed up to master exception handle
  end

  protected

  def load_counts
    @paypal_under_count = PaypalTransfer.find_all_pending_under_approval_threshold.count
    @paypal_over_count = PaypalTransfer.find_all_pending_over_approval_threshold.count
    @check_pending_count = ManualCheckTransfer.pending.count
    @check_processing_count = ManualCheckTransfer.processing.count
    @check_completed_count = ManualCheckTransfer.completed.count
    @check_failed_count = ManualCheckTransfer.failed.count
    @check_cancelled_count = ManualCheckTransfer.cancelled.count
  end

  def load_transfer
    case params[:type]
    when "paypal"
      transfer = PaypalTransfer.find_by(id: params[:id]) # avoid exceptions by using _by_id
    when "check"
      transfer = ManualCheckTransfer.find_by(id: params[:id]) # avoid exceptions by using _by_id
    end
    flash[:notice] = "Unable to locate the specified payment transfer" if transfer.nil?
    transfer
  end

  private

  def mass_pay_params
    params.require(:transfer).permit(id: [])
  end

  def mass_pay_service_params
    { action: "approve", transfer_ids: mass_pay_params[:id], person: current_user, provider: "paypal" }
  end

  def transfers_filter_params
    params.permit(
      :keyword,
      :order,
      :type,
      :threshold_status,
      :status,
      :auto_approval_eligible,
      :page,
      :per_page
    ).with_defaults!(default_transfers_filters)
  end

  def default_transfers_filters
    { type: "paypal", threshold_status: "under", status: "pending", page: 1, per_page: 10 }
  end

  def set_variables_for_index
    load_counts
    @keyword = transfers_filter_params[:keyword]
    @order = transfers_filter_params[:order]
    @type = transfers_filter_params[:type]
    @threshold_status = transfers_filter_params[:threshold_status]
    @auto_approval_eligible = transfers_filter_params[:auto_approval_eligible]
    @status = transfers_filter_params[:status]
    @page = transfers_filter_params[:page] || 1
    @per_page = transfers_filter_params[:per_page]
    @threshold = SystemProperty.get(:approval_required_mass_pay_threshold)
  end
end
