class Admin::StoredBankAccountsController < Admin::AdminController
  before_action :load_person

  # the index action controls view for all stored_bank_accounts, and a person's individual eft_transactions
  # the differences between the two are shown/hidden in the views based on the @person_page variable
  def index
    set_search_defaults
    @search = StoredBankAccount.search(permitted_params[:search])
    @search.relation = @search.relation.order(permitted_params.dig(:search, :order))
    @stored_bank_accounts = @search.paginate(
      page: params[:page],
      per_page: (params[:per_page] || @per_page).to_i
    ).includes(:person)
  end

  def show
    @stored_bank_account = StoredBankAccount.includes(:person, :eft_batch_transactions).find(params[:id])
    unless @stored_bank_account.nil?
      @pending_total = @stored_bank_account.pending_transaction_total
      @processing_total = @stored_bank_account.processing_transaction_total
      @failed_total = @stored_bank_account.failed_transaction_total
      @success_total = @stored_bank_account.successful_transaction_total
    end

    @eft_batch_transactions = EftBatchTransaction.where(stored_bank_account_id: @stored_bank_account.id).includes({ stored_bank_account: :person }, :payout_service_fee)
    return if @eft_batch_transactions.empty?

    statuses = ["pending_approval", "waiting_for_batch"]
    @has_cancelable_transaction = @eft_batch_transactions.any? { |item| statuses.include?(item.status) }
  end

  def history
    @person_shown = Person.find(params[:person_id])
    @stored_bank_account = @person_shown.stored_bank_accounts.current
    @previous_stored_bank_accounts = StoredBankAccount.where("person_id = ? and deleted_at is not null", @person_shown.id).includes(:eft_batch_transactions)
    unless @stored_bank_account.nil?
      @pending_total = @stored_bank_account.pending_transaction_total
      @processing_total = @stored_bank_account.processing_transaction_total
      @failed_total = @stored_bank_account.failed_transaction_total
      @success_total = @stored_bank_account.successful_transaction_total
    end
    @eft_batch_transactions = EftBatchTransaction.where("stored_bank_account_id IN (?)", @person_shown.stored_bank_accounts.collect(&:id)).order(" created_at DESC").includes({ stored_bank_account: :person }, :payout_service_fee)

    return if @eft_batch_transactions.empty?

    statuses = ["pending_approval", "waiting_for_batch", "pending_approval", "waiting_for_batch", "pending_approval", "waiting_for_batch"]
    @has_cancelable_transaction = @eft_batch_transactions.any? { |item| statuses.include?(item.status) }
  end

  private

  def set_search_defaults
    params[:search] ||= {}
    params[:search][:person_id_equals] = params[:person_id] unless params[:person_id].nil?
    params[:search][:order] = "created_at DESC" if params[:search][:order].nil? # set default order
    @per_page = 20
    @person_page = !params[:person_id].nil?
    @person_shown = Person.find(params[:person_id]) if @person_page
  end

  def permitted_params
    params.permit(
      :person_id,
      :per_page,
      :commit,
      search: [
        :person_id_equals,
        :person_status,
        :person_id,
        :person_email,
        :person_name,
        :archived,
        :id,
        :created_at_min,
        :created_at_max,
        :order
      ]
    )
  end
end
