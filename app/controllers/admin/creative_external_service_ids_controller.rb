class Admin::CreativeExternalServiceIdsController < Admin::AdminController
  layout "tc_foundation_admin"
  before_action :validate_feature_enabled
  before_action :load_person
  before_action :load_creatives, only: :index
  before_action :load_creative, only: :index
  before_action :load_creative_external_service_ids_form, only: :update

  def index; end

  def update
    creative_external_service_ids_updated
    respond_to do |format|
      format.js { render json: { status: 200 } }
    end
  end

  private

  def load_person
    @person = Person.find_by(id: params[:person_id])
  end

  def load_creatives
    @creatives = queried_creatives.paginate(page: params[:page], per_page: 25)
  end

  def load_creative
    @creative = Creative.find_by(id: params[:id])
  end

  def load_creative_external_service_ids_form
    @creative_external_service_ids_form = CreativeExternalServiceIdsForm.new(creative_external_service_ids_params)
  end

  def creative_external_service_ids_updated
    @creative_external_service_ids_form.save
  end

  def creative_external_service_ids_params
    params.require(:creative).permit(:apple_artist_id, :spotify_artist_id).merge(creative_id: params.permit(:id)[:id])
  end

  def validate_feature_enabled
    redirect_to admin_home_path unless FeatureFlipper.show_feature?(:creative_external_service_ids_index, current_user)
  end

  def queried_creatives
    if search_enabled? && search_params.present?
      CreativeExternalServiceId::SearchQueryBuilder.build(search_params_hash)
    else
      initial_query
    end
  end

  def search_enabled?
    FeatureFlipper.show_feature?(:creative_external_service_id_search)
  end

  def search_params
    params.permit(:search_text)
  end

  def search_params_hash
    search_params.merge(initial_query: initial_query)
  end

  def initial_query
    @person.creatives.primary_featuring_or_remixer.joins(:artist).includes(:external_service_ids).distinct
  end
end
