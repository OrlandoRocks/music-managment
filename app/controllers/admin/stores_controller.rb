class Admin::StoresController < Admin::AdminController
  def index
    @page_title = "Stores"
    @stores = Store.includes(:store_delivery_config).order("is_active DESC, in_use_flag DESC")
  end
end
