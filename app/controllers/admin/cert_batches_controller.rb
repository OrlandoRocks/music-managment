class Admin::CertBatchesController < Admin::AdminController
  # GET /cert_batches
  # GET /cert_batches.xml
  def index
    @page_title = "Cert Batches"

    @cert_batches = CertBatch.order("created_at DESC")
                             .paginate(page: params[:page], per_page: 50)

    respond_to do |format|
      format.html # index.rhtml
      format.xml  { render xml: @cert_batches.to_xml }
    end
  end

  # GET /cert_batches/1
  # GET /cert_batches/1.xml
  def show
    @cert_batch = CertBatch.find(params[:id])
    @page_title = @cert_batch.promotion

    respond_to do |format|
      format.html # show.rhtml
      format.xml  { render xml: @cert_batch.to_xml }
    end
  end

  def certs
    @cert_batch = CertBatch.find(params[:id])
    @page_title = "Certs for #{@cert_batch.promotion}"

    respond_to do |format|
      format.html { @page_title = "Redeemed " + @page_title } # certs.rhtml
      format.xml  { render xml: @cert_batch.to_xml }
      format.csv  { send_data(@cert_batch.to_csv, filename: @cert_batch.promotion + ".csv", type: "text/csv") }
    end
  end

  def add
    @cert_batch = CertBatch.find(params[:id])
    @count = params[:count].to_i
    @page_title =
      if @cert_batch.finalized?
        "Add Certs for #{@cert_batch.promotion}"
      else
        "Finalize #{@cert_batch.promotion}"
      end

    return unless request.post?

    # need a count for non-spawning certs
    if !@cert_batch.spawning? and @count < 1
      flash.now[:notice] = "Number of certs to add must be greater than zero"
      return
    end

    # add the certs, this will also finalize the batch
    begin
      @cert_batch.create_certs!(@count)
      flash[:notice] = "Added #{@count} Certs"
      redirect_to admin_cert_batch_url(@cert_batch)
    rescue
      flash[:error] = $!
    end
  end

  def exp
    @page_title = "Update Cert Batch"
    @cert_batch = CertBatch.find(params[:id])
    @cert = Cert.new
    @cert.admin_only = @cert_batch.admin_only?
    return unless request.post?

    @cert = Cert.new(cert_params)
    begin
      @cert.expiry_date = parse_date(cert_params[:expiry_date])
    rescue
      @cert.expiry_date = nil
    end
    @cert.validate_expiry_date_within_2_days
    if @cert.errors[:expiry_date].blank?
      @cert_batch.update_expiry!(@cert.expiry_date)
      redirect_to admin_cert_batch_url(@cert_batch)
    else
      flash.now[:error] = @cert.errors[:expiry_date].first
    end
  end

  def admin_only
    @page_title = "Update Cert Batch"
    @cert_batch = CertBatch.find(params[:id])
    @cert = Cert.new(cert_params)
    if request.post?
      @cert_batch.update_admin_only!(@cert.admin_only?)
      redirect_to admin_cert_batch_url(@cert_batch) and return
    end
    render action: :exp
  end

  # GET /cert_batches/new
  def new
    @cert_batch = CertBatch.new(cert_count: 1)
    @cert = Cert.new(
      expiry_date: 1.year.from_now,
      cert_engine: "CertEngine::DefaultPercent"
    )
  end

  # GET /cert_batches/1;edit
  def edit
    @cert_batch = CertBatch.find(params[:id])
    redirect_to admin_cert_batch_url(@cert_batch) if @cert_batch.finalized?
  end

  # POST /cert_batches
  # POST /cert_batches.xml
  def create
    @cert_batch = CertBatch.new(cert_batch_params)
    assign_expiry_date_from_params
    @cert_batch.person = current_user

    respond_to do |format|
      Cert.transaction do
        if @cert_batch.save
          flash[:notice] = "CertBatch was successfully created."
          format.html { redirect_to admin_cert_batch_url(@cert_batch) }
          format.xml  { head :created, location: admin_cert_batch_url(@cert_batch) }
        else
          format.html { render action: "new" }
          format.xml  { render xml: @cert_batch.errors.to_xml }
        end
      end
    end
  end

  # PUT /cert_batches/1
  # PUT /cert_batches/1.xml
  def update
    @cert_batch = CertBatch.find(params[:id])
    redirect_to admin_cert_batch_url(@cert_batch) if @cert_batch.finalized?

    respond_to do |format|
      @cert_batch.attributes = cert_batch_params
      assign_expiry_date_from_params
      if @cert_batch.save
        flash[:notice] = "CertBatch was successfully updated."
        format.html { redirect_to admin_cert_batch_url(@cert_batch) }
        format.xml  { head :ok }
      else
        format.html { render action: "edit" }
        format.xml  { render xml: @cert_batch.errors.to_xml }
      end
    end
  end

  # DELETE /cert_batches/1
  # DELETE /cert_batches/1.xml
  def destroy
    @cert_batch = CertBatch.find(params[:id])
    begin
      @cert_batch.destroy
    rescue
      flash[:error] = $!
    end

    respond_to do |format|
      format.html { redirect_to admin_cert_batches_url }
      format.xml  { head :ok }
    end
  end

  private

  def cert_params
    params.require(:cert).permit(
      :expiry_date,
      :admin_only
    )
  end

  def cert_batch_params
    params.require(:cert_batch).permit(
      :promotion,
      :description,
      :cert_engine,
      :engine_params,
      :spawning_code,
      :brand_code,
      :expiry_date
    )
  end

  def parse_date(date_string)
    DateParser.parse(date_string)
  end

  def assign_expiry_date_from_params
    @cert_batch.expiry_date = parse_date(cert_batch_params[:expiry_date])
  rescue => e
    @cert_batch.errors.add(:expiry_date, e)
  end
end
