class ArtistMappingsController < ApplicationController
  def create
    ArtistMapping.create_or_update_external_service_ids(apple_artist_id_options.merge(artwork: params[:artwork]))
    ArtistMappingWorker.perform_async(apple_artist_id_options)
    render json: { mappings: mappings }
  end

  private

  def apple_artist_id_options
    {
      is_duplicate: params[:is_duplicate],
      artist_id: params[:my_artist_id],
      service_name: params[:service_name],
      identifier: identifier,
      person_id: current_user.id
    }
  end

  def identifier
    Apple::ArtistIdExtractionService.extract_artist_id(params[:identifier])
  end

  def mappings
    MyArtist.mapping_for(current_user.id, params[:my_artist_id]).as_json(only: [:service_name, :identifier, :state])
  end
end
