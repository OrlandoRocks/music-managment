class ArtworkTemplateController < ApplicationController
  layout "application"
  before_action :load_person, :load_album
  before_action :load_editable_distribution

  def edit
    @page_title = "choose your template"
  end

  def update
    if params[:distribution] && @distribution.can_update_metadata?
      @distribution.template_id = params[:distribution][:template_id].to_i
    end

    if @distribution.save
      redirect_to album_path(@album)
    else
      flash[:error] = "Your choice could not be saved. Please try again."
      render(action: "edit")
    end
  end

  private

  def load_editable_distribution
    @distribution = @album.salepoints.find(params[:salepoint_id].to_i)
  end
end
