# frozen_string_literal: true

class CartsController < ApplicationController
  include PaymentFinalizable
  include VatHelper
  include PeopleHelper
  include PayUsingRoyaltyHelper
  include CartHelper
  include CurrencyHelper

  before_action :login_required
  before_action :load_person
  before_action :switch_error_flash
  before_action :load_cart_finalize_form, only: [
    :confirm_and_pay,
    :finalize,
    :finalize_after_redirect,
    :adyen_finalize_after_redirect
  ]
  before_action :payment_failed_flash, only: [:show]
  before_action :set_stored_credit_card, only: [:generate_stored_card_nonce]
  skip_before_action :set_gon_two_factor_auth_prompt, [:confirm_and_pay], raise: false

  layout "application"

  def show
    @page_title       = custom_t("controllers.carts.shopping_cart")
    @albums_in_cart   = Purchase.albums_in_cart(current_user)
    @tcs_terms_url    = tcs_terms_url

    manage_invoices
    setup_cart_variables
    setup_ctas

    @show_actions = true
    @cart_is_empty = @cart_data.purchases.count <= 0
  end

  def confirm_and_pay
    manage_invoices
    setup_cart_variables
    @show_payment_method_required = show_payment_method_required?
    @compliance_info_form         = ComplianceInfoForm.new(current_user)
    @all_on_tunecore              = @cart_data.total_price_cents <= 0
    @can_pay_with_balance         = @cart_data.can_cover_with_balance?
    @show_actions                 = false
    @show_payment_methods         = show_payment_methods?
    @page_title                   = custom_t("controllers.carts.confirm_place_your_order")
    @paypal_account               = StoredPaypalAccount.currently_active_for_person(@person)
    @paypal_button_text           = @paypal_account ? custom_t("carts.confirm_and_pay.use_another_account") : custom_t("carts.confirm_and_pay.add_new_account")
    @credit_cards                 = @person.stored_credit_cards.active
    @person_preference            = PersonPreference.find_by(person_id: @person.id)
    gon_custom_t("credit_card", "helper.carts.payment_methods.credit_card")
    gon_custom_t("paypal_account", "helper.carts.payment_methods.paypal_account")
    gon_custom_t("self_billing_required_error", "self_billing.required_error")
    gon_custom_t("self_billing_ajax_error", "something_went_wrong")
    gon_custom_t("pay_using", "carts.confirm_and_pay.pay_using")
    initialize_vat_data_and_translations if current_user.vat_and_bi_enabled?
    redirect_to cart_path unless build_outbound_vat_data
  end

  def dropin
    invoice = Invoice.find params[:invoice_id]
    payment_method = params[:payment_method]
    session_info = Adyen::NewSessionService.new(invoice, payment_method)
    session_info.update_adyen_transaction
    @session = {
      session_data: session_info.details[:session_data],
      id: session_info.details[:session_id],
      client_key: session_info.details[:client_key],
      environment: session_info.details[:environment],
      locale: current_user.country_website_language&.yml_languages
    }
    @amount = session_info.formatted_amount_in_original_currency
    @amount_in_local_currency = session_info.formatted_amount_in_local_currency

    gon.push(adyen_translations: custom_t("adyen.sdk", {}, false).to_json)
    gon_custom_t("something_went_wrong", "something_went_wrong")
    gon_custom_t("apple_pay_cancel_event", "adyen.dropin_page.dropin_apple_pay_cancel_event")
    gon_custom_t("dropin_error", "adyen.dropin_page.dropin_error_generic")
    gon_custom_t("payment_confirmed", "adyen.dropin_page.payment_confirmed")

    return if session_info.details[:session_id].present?

    flash[:error_dialog] = custom_t("controllers.carts.issue_placing_your_order")
    redirect_to confirm_and_pay_cart_path
  end

  def handle_finalize_form_error(error_redirect:, show_flash: true)
    send_sift_failed_order
    flash_finalize_error_dialogue if show_flash
    redirect_to error_redirect
  end

  def handle_finalize_form_save(saved, error_redirect)
    handle_finalize_form_error(error_redirect: error_redirect) and return unless saved

    if @finalize_form.otp_redirect_url.present?
      redirect_to @finalize_form.otp_redirect_url
    elsif skip_confirm_and_pay? || payu_invoice_settled?
      send_sift_successful_order
      redirect_to thanks_invoice_path(
        @finalize_form.invoice,
        successful_purchase_after_effects_params
      )
    else
      # Not known to reach this
      handle_finalize_form_error(error_redirect: error_redirect, show_flash: false)
    end
  end

  # Express checkout of only plan-included items
  def skip_confirm_and_pay
    @skipped_confirm_and_pay = true
    @finalize_form           = CartFinalizeForm.new(skip_confirm_and_pay_params)

    Sift::EventService.create_order(@finalize_form, cookies, request)

    handle_finalize_form_save(@finalize_form.save, cart_path)
  end

  def finalize
    if update_compliance_info == false || pre_form_checks_valid? == false
      redirect_to confirm_and_pay_cart_path and return
    end

    Sift::EventService.create_order(@finalize_form, cookies, request)

    handle_finalize_form_save(@finalize_form.save, confirm_and_pay_cart_path)
  end

  # Unused now?
  def finalize_after_otp
    payments_os_payment_id = params[:payment_id]
    payments_os_charge_id = params[:charge_id]
    payment_status = params[:status].downcase
    transaction = PaymentsOSTransaction.find_by(
      payment_id: payments_os_payment_id,
      charge_id: payments_os_charge_id
    )
    redirect_params = transaction.person.purchased_before? ? {} : { f: "first" }
    if payment_status == "succeed"
      unless transaction.payment_completed?
        transaction.succeeded
        successful_purchase_after_effects(transaction.invoice, true)
      end
      redirect_to thanks_invoice_path(transaction.invoice, redirect_params)
    else
      transaction.failed unless transaction.payment_completed?
      flash[:error_dialog] = custom_t("payments_os.failed_transaction")
      redirect_to confirm_and_pay_cart_path
    end
  end

  # Handles the PayU Hosted Checkout redirect back to us
  def finalize_after_redirect
    begin
      PayuRedirectReceiptWorker.perform_async(params[:invoice_id])
      payu_txn = current_user.payu_transactions.find_by!(invoice_id: params[:invoice_id])
      payu_txn.refresh!
      send_sift_successful_order
      successful_purchase_after_effects(payu_txn.invoice, true)
    rescue PayuTransaction::PayuTransactionError, ActiveRecord::RecordNotFound => e
      Tunecore::Airbrake.notify(
        "PayU checkout redirect failed",
        {
          error: e,
          payu_txn: payu_txn,
          current_user: current_user,
          invoice_id: params[:invoice_id]
        }
      )
      send_sift_failed_order
      failed_purchase_after_effects_for_hosted_checkout(payu_txn.invoice) if payu_txn&.invoice.present?
      redirect_to cart_path(invoice_id: params[:invoice_id], failed: "true") and return
    end

    redirect_params = current_user.purchased_before? ? {} : { f: "first" }
    redirect_to thanks_invoice_path(params[:invoice_id], redirect_params)
  end

  def generate_stored_card_nonce
    country_code = params[:country_code] || current_user.country_iso_code
    client_token = current_user.config_gateway_service_by_country(country_code).generate_client_token
    payment_method_nonce = @stored_credit_card.create_payment_method_nonce

    render json: {
      client_token: client_token,
      payment_method_nonce: payment_method_nonce.nonce,
      bin: payment_method_nonce.details[:bin],
      billing_address: @stored_credit_card.billing_address
    }
  end

  def adyen_finalize_after_redirect
    begin
      @finalize_form = CartFinalizeForm.new(skip_confirm_and_pay_params)
      adyen_txn = current_user.adyen_transactions.find_by!(invoice_id: params[:invoice_id])
      if params[:redirectResult].present?
        Adyen::RefreshAdyenTransactionService.new(adyen_txn, params[:redirectResult]).refresh!
      end
      send_sift_successful_order
      successful_purchase_after_effects(adyen_txn.invoice, true)
    rescue AdyenError::AdyenTransactionError, ActiveRecord::RecordNotFound => e
      Tunecore::Airbrake.notify(
        "Adyen checkout redirect failed",
        {
          error: e,
          adyen_response: e.response,
          adyen_txn: adyen_txn,
          current_user: current_user,
          invoice_id: params[:invoice_id]
        }
      )
      failed_purchase_after_effects_for_hosted_checkout(adyen_txn.invoice) if adyen_txn&.invoice.present?
      send_sift_failed_order
      redirect_to cart_path(invoice_id: params[:invoice_id], failed: "true") and return
    end
    redirect_params = current_user.purchased_before? ? {} : { f: "first" }
    redirect_to thanks_invoice_path(params[:invoice_id], redirect_params)
  end

  def adyen_payment_methods
    payment_methods = Adyen::PaymentMethodService.new(params[:country_code], current_user).payment_methods

    render json: { payment_methods: payment_methods }
  end

  def splits_collaborator_popup
    popup_template = render_to_string(
      "carts/splits_collaborator_popup",
      layout: false
    )

    render json: { popup_template: popup_template, purchase_id: params[:purchase_id] }
  end

  private

  def initialize_vat_data_and_translations
    balance_cents = current_user.person_balance.balance * 100.0
    @vat_init_data = {
      user_country: current_user.country_iso_code,
      sub_total_excl_vat: money_to_unformatted_amount(@cart_data.vat_exclusive_price_cents.to_money(@cart_data.currency), round_value: false),
      grand_total: money_to_unformatted_amount(@cart_data.total_price_cents.to_money(@cart_data.currency), round_value: false),
      use_balance: @finalize_form.use_balance,
      feature_flags: {
        vat_tax: current_user.vat_feature_on?,
        bi_transfer: current_user.bi_transfer_feature_on?
      },
      person_balance_cents: money_to_unformatted_amount(balance_cents.to_money(@cart_data.currency), round_value: false),
      country_list: current_user.non_self_country_details,
      inbound_vat_rates: current_user.person_country_vat_rates,
      self_billing_accepted: current_user.self_billing_accepted?,
      vat_info: {
        vat_registration_number: current_user.vat_registration_number,
        vat_company_name: current_user.vat_company_name,
        country_iso_code: current_user.country_iso_code,
        vat_number_sharing: current_user.sharing_vat_number?
      },
      locale: {
        locale: I18n.locale,
        currency: current_user.country_website.india? ? CurrencyCodeType::INR : current_user.country_website.currency
      },
      adyen_enabled: current_user.allow_adyen?,
      enabled_indonesian_wallets: current_user.enabled_indonesian_wallets?,
      preferred_adyen_method: preferred_adyen_payment_method
    }
    gon.push(translations: {})
    gon_custom_t("translations.applied_part_to_order", "carts.confirm_and_pay.applied_part_to_order")
    gon_custom_t("translations.applied_to_order", "carts.confirm_and_pay.applied_to_order")
    gon_custom_t("translations.you_part_pay_order", "carts.confirm_and_pay.you_part_pay_order")
    gon_custom_t("translations.you_pay_part_order", "carts.confirm_and_pay.you_pay_part_order")
    gon_custom_t("translations.use", "use")
    gon_custom_t("translations.use_my", "carts.confirm_and_pay.use_my")
    gon_custom_t("translations.something_went_wrong", "something_went_wrong")
    gon_custom_t("translations.required_field", "required_field")
    gon_custom_t("translations.try_again", "stored_credit_cards.try_again")
    gon_custom_t("translations.verification_failed", "stored_credit_cards.verification_failed")
  end

  def save_compliance_info
    return true unless FeatureFlipper.show_feature?(:bi_transfer, current_user)

    @compliance_info_form = ComplianceInfoForm.new(current_user)
    @compliance_info_form.save(compliance_info_params)

    if @compliance_info_form.errors.any?
      translated_error_field_names = @compliance_info_form.errors.full_messages.map do |field_name|
        custom_t("people.renewal_billing_info.#{field_name}")
      end.join(", ")
      @compliance_errors << custom_t("people.renewal_billing_info.error_saving_info", error: translated_error_field_names)
      return false
    end

    true
  end

  def save_customer_type
    return true unless FeatureFlipper.show_feature?(:bi_transfer, current_user)
    return true if customer_type_params[:customer_type].blank?

    c_type_update = current_user.update(customer_type_params)
    error = current_user.errors.full_messages
    @compliance_errors << custom_t(
      "people.renewal_billing_info.error_saving_info",
      error: error
    ) unless c_type_update

    c_type_update
  end

  def tcs_terms_url
    country = current_user.country_website.country
    default_url = "https://www.tunecore.com/terms?section=tunecore-social"

    urls = {
      US: default_url,
      CA: "http://www.tunecore.ca/terms?section=tunecore-social",
      UK: "https://www.tunecore.co.uk/terms?section=tunecore-social",
      AU: "https://www.tunecore.com.au/terms?section=tunecore-social",
    }.with_indifferent_access

    urls[country] || default_url
  end

  def load_cart_finalize_form
    @finalize_form ||= CartFinalizeForm.new(cart_params)
  end

  def skipped_confirm_and_pay?
    @skipped_confirm_and_pay == true
  end

  def skip_confirm_and_pay_params
    {
      ip_address: request.remote_ip || "unknown",
      person: @person,
      purchases: purchase_items,
      use_balance: false,
    }
  end

  def pay_params
    if params[:state_city_name].present? && params[:state_id].present?
      params[:person][:state] = CountryState.find(params[:state_id]).name
      params[:person][:city] = CountryStateCity.find_by_name(params[:state_city_name]).name
    end
    params
      .permit(:balance,
              :three_d_secure_nonce,
              cart_finalize_form: [
                :balance,
                :use_balance,
                :payment_id,
                :cvv,
                :gstin,
                :mobile_number
              ],
              person: [
                :address1,
                :address2,
                :city,
                :state,
                :postal_code,
                :country,
                {
                  vat_information_attributes: [
                    :id,
                    :vat_registration_number,
                    :company_name
                  ]
                }
              ])
  end

  def vat_params
    return {} if pay_params[:person].nil?

    pay_params[:person][:vat_information_attributes] || {}
  end

  def cart_finalize_params
    pay_params[:cart_finalize_form] || {}
  end

  def cart_params
    {
      person: @person,
      use_balance: cart_finalize_params[:use_balance] || params[:balance],
      payment_id: cart_finalize_params[:payment_id],
      ip_address: request.remote_ip || "unknown",
      purchases: purchase_items,
      cvv: cart_finalize_params[:cvv],
      gstin: cart_finalize_params[:gstin],
      mobile_number: cart_finalize_params[:mobile_number],
      person_address_info: pay_params[:person]&.except(:vat_information_attributes),
      vat_info: vat_params,
      three_d_secure_nonce: pay_params[:three_d_secure_nonce]
    }
  end

  def compliance_info_params
    params.fetch(:person, {}).permit(:customer_type, :country).merge(
      params.permit(:first_name, :last_name, :company_name)
    )
  end

  def customer_type_params
    params.fetch(:person, {}).permit(:customer_type)
  end

  # TODO: Rewrite this ASAP
  # Move remove_* methods to ensure_valid_purchases
  def setup_cart_variables
    @active_stores = Store.active_for_purchase.exclude_discovery(@person).pluck(:name)

    remove_duplicate_plans

    remove_deleted_albums
    remove_deleted_albums_attached_to_credits

    recalculate_dolby_atmos_purchases

    recalculate_additional_artists_for_plan

    apply_proration_discounts!
    set_plan_sufficiency

    # @purchases is the collection displayed in the cart. Purchases are not queried again after this.
    @purchases = purchase_items.includes(:cert, :product, :targeted_product, :related) # AR Relation...
    remove_undistributable_purchases                                                   # to_a called...
    remove_orphaned_purchases                                                          # expects Array

    related_products = Product.related_products_for_purchases(@person, @person.purchases, 2)
    @cart_data = Cart::Purchases::Manager.new(@person, @purchases, related_products)
  end

  def apply_proration_discounts!
    ProrationService.apply_proration_discounts!(@person)
  end

  def remove_duplicate_plans
    plan_purchases = purchase_items.select(&:is_plan?)
    return if plan_purchases.count <= 1

    # Sort the set of plan purchases in increasing order, then remove the last
    # element as it's the most expensive plan and we want to push users towards
    # purchasing the higher plan.
    plan_purchases.sort_by!(&:cost_cents)
    plan_purchases.pop

    plan_purchases.each(&:destroy)
  end

  def setup_ctas
    @ytsr_product_id = Product.find_products_for_country(current_user.country_domain, :ytm)
    @ytsr_product = Product.find(@ytsr_product_id)
    Product.set_targeted_product_and_price(current_user, @ytsr_product)
    @social_cta = Cart::SocialCtaManager.manage(current_user, @purchases)
  end

  def legacy_user_no_carted_plan?
    @person.legacy_user? && @person.cart_plan.nil?
  end

  def set_plan_sufficiency
    @plan_is_sufficient ||=
      Plans::SufficientPlanService.call(@person, @person.cart_plan, :cart) ||
      Plans::SufficientPlanService.call(@person, @person.plan, :cart) ||
      legacy_user_no_carted_plan?
  end

  def setup_freemium_upsell
    return unless show_freemium_upsell?

    @show_freemium_upsell = true
    @upsell_stores = Store.is_active.exclude_discovery(@person).pluck(:name)
    @albums_to_upsell = freemium_purchases
  end

  def user_needs_mobile_number?
    @person.present? && @person.india_user? && !@person.valid_phone_number?
  end

  def user_has_mobile_number_errors?
    @person.errors.messages.fetch(:mobile_number, []).present?
  end

  def formatted_mobile_errors
    custom_t("people.errors.mobile_number_errors") + custom_t("activerecord.errors.models.person.attributes.mobile_number.not_a_number")
  end

  def purchase_items
    ensure_valid_purchases

    @person.purchases.unpaid.not_in_invoice
  end

  def remove_undistributable_purchases
    @purchases.to_a.reject! do |purchase|
      if purchase.related.respond_to?(:is_deleted) && purchase.related.is_deleted?
        flash.now[:error_dialog] = "<strong>#{purchase.related.name}</strong> was deleted. Please contact customer care to restore."
        purchase.destroy
        true
      elsif purchase.related.respond_to?(:can_distribute?) && !purchase.related.can_distribute?
        flash.now[:error_dialog] = custom_t("controllers.carts.deleted_purchase", purchase_name: purchase.related.name)
        purchase.destroy
        true
      elsif purchase.related.respond_to?(:valid_preorder_data?)
        if !purchase.related.album_can_distribute? || !purchase.related.enabled?
          flash.now[:error_dialog] = custom_t("controllers.carts.preorder_not_enabled") unless purchase.related.enabled?
          purchase.destroy
          true
        elsif !purchase.related.valid_preorder_data?
          remove_album_related_purchases(purchase)
          flash[:album_notice] = purchase.related.preorder_data_errors
          redirect_to album_path(purchase.related.album_id)
        end
      end
    end
  end

  def remove_deleted_albums
    deleted_items = @person.purchases.deleted_albums.readonly(false)

    deleted_items.each do |purchase|
      flash.now[:error_dialog] = custom_t("controllers.carts.album_was_deleted", purchase_name: purchase.related.name)
      purchase.destroy
    end
  end

  def remove_deleted_albums_attached_to_credits
    deleted_items = @person.purchases.deleted_albums_attached_to_credits
    deleted_items.each do |purchase|
      flash.now[:error_dialog] = custom_t("controllers.carts.album_was_deleted", purchase_name: purchase.related.related.name)
      purchase.destroy
    end
  end

  def remove_orphaned_purchases
    @purchases.select { |purchase| purchase.related.nil? }.each do |purchase|
      @purchases.delete(purchase)
      purchase.destroy
    end
  end

  def remove_album_related_purchases(purchase)
    album = purchase.related.album
    album_purchase = album.purchases.last || album.credit_usage.purchase

    return if album_purchase.paid_at?

    @purchases.delete(album_purchase)
    @purchases.delete(purchase)
    if album.salepoint_subscription && album.salepoint_subscription.purchase
      @purchases.delete(album.salepoint_subscription.purchase)
    end
    @purchases.delete(album.booklet.purchase) if album.booklet && album.booklet.purchase
    album_purchase.destroy
  end

  #
  # KJL 10/15
  #
  # Temporary code to handle the transition from old cart to new cart.
  # The old implementation would create invoices once moving to the
  # payments page. If no payments were applied, the invoices still
  # remained and purchases associated to it.
  #
  # Before removing. Ensure no invoices are in this state checked against
  # below
  #
  def manage_invoices
    invoices = @person.invoices.includes(:purchases, :invoice_settlements).where("invoices.settled_at is null")

    invoices.each do |invoice|
      # make sure if there are settlements that they are all of type PersonTransaction
      unless !(invoice.invoice_settlements && invoice.invoice_settlements.any? { |s| s.source_type != "PersonTransaction" }) && invoice.can_destroy?
        next
      end

      Rails.logger.info "Removing partial invoice #{invoice.id} for person #{@person.id}"
      invoice.credit_and_remove_settlements!
      invoice.reload
      invoice.destroy
    end
  end

  def show_freemium_upsell?
    freemium_feature? && @purchases.any? { |p| p.related.try(:is_general_discovery_release?, current_user) }
  end

  def freemium_feature?
    (FeatureFlipper.show_feature?(:freemium_flow, current_user) || FeatureFlipper.show_feature?(:discovery_platforms, current_user))
  end

  def freemium_purchases
    @purchases.map(&:related).select { |r| Product.free_release?(r) }
  end

  def switch_error_flash
    return unless flash[:error]

    flash.now[:error_dialog] = flash[:error]
    flash[:error] = nil
  end

  def successful_purchase_after_effects_params
    successful_purchase_after_effects(@finalize_form.invoice)
    @finalize_form.purchased_before? ? {} : { f: "first" }
  end

  def flash_finalize_error_dialogue
    flash[:error_dialog] = @finalize_form.errors.full_messages.join(", ")
    auth_link = view_context.reauthorize_link(@finalize_form.manager.reauthorize, confirm_and_pay_cart_path)
    flash[:error_dialog] << auth_link if auth_link.present?
    flash[:error_dialog] << formatted_mobile_errors if user_needs_mobile_number?
    flash[:error_dialog] << custom_t("controllers.carts.gstin_error") if @gst.present? && @gst.errors.present?

    return unless self_billing_required?(@finalize_form.use_balance)

    flash[:error_dialog] << custom_t("self_billing.required_error")
  end

  def show_payment_method_required?
    return false unless FeatureFlipper.show_feature?(:freemium_flow, current_user)

    @purchases
      .select { |p| p.related_type == Product::ALBUM }
      .all? { |p| p.related.strictly_facebook_release? }
  end

  def build_outbound_vat_data
    return true unless vat_feature_flipper_enabled?

    if tax_info[:errors].present?
      flash[:error] = custom_t(tax_info[:errors])
      return false
    end

    @base_amount = base_amount(@cart_data.total_price)
    @can_pay_with_balance = can_cover_base_amt_with_balance?(@base_amount, current_user.balance)
    @outbound_vat = outbound_vat(@base_amount)
    @amount_payable = amount_payable(@cart_data.total_price, @base_amount, @outbound_vat)

    true
  end

  def only_buying_discovery?
    @cart_data.purchases.map(&:related).all? { |r| Product.free_release?(r) }
  end

  def show_payment_methods?
    return true if only_buying_discovery?

    !@can_pay_with_balance || !@finalize_form.use_balance
  end

  def payment_failed_flash
    return unless params.fetch(:failed, nil) == "true"

    flash.now[:error_dialog] = custom_t("controllers.carts.issue_placing_your_order")
  end

  def payu_invoice_settled?
    return true if @finalize_form.invoice.payu_transactions.empty?

    @finalize_form.invoice.settled?
  end

  def set_stored_credit_card
    @stored_credit_card = current_user.stored_credit_cards.find(params[:id])
  end

  def update_compliance_info
    ActiveRecord::Base.transaction do
      @compliance_errors = []
      if save_compliance_info && save_customer_type
        true
      else
        flash[:error_dialog] = @compliance_errors.join(", ")
        false
      end
    end
  end

  def pre_form_checks_valid?
    is_mobile_valid = @person.india_user? ? @person.valid_phone_number? : true
    pre_form_validations = { mobile_number: is_mobile_valid, gstin: true }

    if FeatureFlipper.show_feature?(:enable_gst, @person)
      cart_params.fetch(:gstin).tap do |c_params|
        next if c_params.blank?

        @gst = GstInfo.find_or_create_by(person: @person, gstin: c_params)
        pre_form_validations[:gstin] = @gst.errors.blank?
      end
    end

    pre_form_validations.values.all?
  end

  def destroy_release_renewal_purchase?(purchase)
    return false unless @person.owns_or_carted_plan?

    return false unless purchase.renewal? && purchase.related.release?

    flash.now[:error_dialog] = custom_t("controllers.carts.release_renewal_with_plan")
    true
  end

  # TODO: expand to more cases
  #
  # #ensure_valid_purchases is a side-effect of #purchase_items,
  # ensuring that we can't have invalid purchase_items
  def ensure_valid_purchases
    @person.purchases.unpaid.not_in_invoice.each do |purchase|
      purchase.destroy if destroy_release_renewal_purchase?(purchase)
    end

    nil
  end

  def preferred_adyen_payment_method
    return unless current_user.person_preference&.preferred_payment_type == AdyenTransaction::ADYEN_NAME

    current_user.prefered_adyen_payment_method&.payment_method_name
  end

  def send_sift_successful_order
    invoice_id = params[:invoice_id] || @finalize_form&.invoice&.id
    Sift::EventService.successful_order(
      person: @person,
      finalize_form: @finalize_form,
      invoice_id: invoice_id,
      cookies: cookies,
      request: request
    )
  end

  def send_sift_failed_order
    invoice_id = params[:invoice_id] || @finalize_form&.invoice&.id
    Sift::EventService.failed_order(
      person: @person,
      finalize_form: @finalize_form,
      invoice_id: invoice_id,
      cookies: cookies,
      request: request
    )
  end

  def recalculate_dolby_atmos_purchases
    cart_albums_with_spatial_audio = @person.albums.distinct.in_cart.joins(:spatial_audios)
    cart_albums_with_spatial_audio.each do |album|
      Album::DolbyAtmosCartService.call(@person, album)
    end
  end

  def recalculate_additional_artists_for_plan
    return unless Plan.enabled?(@person, false)

    pro_plan_renewal_purchase = carted_manual_pro_plan_renewal_purchase
    if pro_plan_renewal_purchase
      # adjust for renewing additional artist purchases
      renewal = pro_plan_renewal_purchase.related
      product = pro_plan_renewal_purchase.product
      # empty out unpaid plan_add_ons and related purchases and force recalculation
      Plans::AdditionalArtistService.clear_unpaid_additional_artist_purchases!(current_user)
      artist_purchases = Plans::RenewAdditionalArtistsService
                         .additional_artist_purchases!(current_user, renewal, product)
      Plans::CancelAdditionalArtistService.cancel_unrenewed_artists(
        current_user,
        Purchase.where(id: artist_purchases.map(&:id))
      )
    end
    # adjust for new additional artist purchases
    Plans::AdditionalArtistService.adjust_additional_artists(@person)
  end

  def carted_manual_pro_plan_renewal_purchase
    return if purchase_items.blank?

    purchase_ids = purchase_items.map(&:id)

    Purchase
      .joins(renewal: :renewal_product_item)
      .find_by(id: purchase_ids, renewals: { product_items: { product_id: Product::PLANS_WITH_ADDITIONAL_ARTISTS_PRODUCT_IDS } })
  end
end
