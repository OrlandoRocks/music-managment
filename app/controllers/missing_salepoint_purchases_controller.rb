class MissingSalepointPurchasesController < ApplicationController
  def show
    @form = MissingSalepointPurchaseForm.new(store_name: params[:store], person: current_user)
    flash[:error] = @form.errors.full_messages.join(" & ") unless @form.save
    redirect_to cart_path
  end
end
