class InvoicesController < ApplicationController
  include TcSocialHelper
  include PublishingAdministrationHelper
  include YoutubeOAC

  before_action :load_person
  before_action :load_invoice
  before_action :ensure_correct_country_domain

  layout "application"

  def thanks
    @page_title = custom_t(:thank_you)
    @newest_store = Store.order("launched_at DESC").first
    @invoice_person_state = InvoicePersonState.new(@invoice)
    @marketing_metrics = PersonMarketingMetrics.new(@person)
    @publishing_link = pub_admin_compositions_page
    @advance_display = CashAdvanceRegistrationService.new(current_user).advance_display
    @show_popup = params[:open_lyric_popup].present?
    @show_youtube_oac = show_youtube_oac
    @lyric_button_translations = TranslationFetcherService.translations_for("javascripts/lyric_button", COUNTRY_LOCALE_MAP[locale.upcase.to_s]).translations["lyric_button"]

    @tier = @person.relevant_tier

    prepare_social_modal

    set_promotional_ticker

    unless !PersonProfileSurveyInfo.get_survey_info(current_user).already_viewed? &&
           @invoice_person_state.has_distro_purchase

      return
    end

    cookies[:profile_survey] = true
  end

  def show
    redirect_to "/" if FeatureFlipper.show_feature?(:block_indian_invoices, @person)

    @page_title = custom_t("controllers.invoices.invoice")
    @newest_store = Store.order("launched_at DESC").first
    @purchases = @invoice.purchases
    @cart_data = Cart::Purchases::Manager.new(@person, @purchases)
  end

  def download
    get_invoice_data(@invoice)

    respond_to do |format|
      format.html
      format.pdf do
        @static_corporate_entity.tunecore_us? ? render_tc_invoice : render_bi_invoice
      end
    end
  end

  def refunds
    render json: { refunds: build_refund_info }
  end

  private

  def load_invoice
    @invoice = @person.invoices.where(id: params[:id]).first
    @invoice = Invoice.find(params[:id]) if @invoice.nil? && current_user.has_role?("Admin")
  end

  def prepare_social_modal
    gon.show_social_modal = @invoice.purchases.detect(&:is_social?).present?
    gon.social_url        = tc_social_index_url
  end

  def pub_admin_compositions_page
    composer_id = Purchase.joins(:invoice)
                          .where(related_type: "PublishingComposer", invoices: { id: params[:id] })
                          .pluck(:related_id).first || current_user.publishing_composers.primary.find(&:is_active?).try(:id)

    publishing_administration_composition_path(composer_id) if composer_id
  end

  def show_youtube_oac
    return unless FeatureFlipper.show_feature?(:yt_oac_self_serve, current_user)

    oac_eligible_artists.any?
  end

  def get_invoice_data(invoice)
    email_notification = InvoiceGenerator::EmailNotification.new(invoice)
    @static_corporate_entity = invoice.invoice_static_corporate_entity
    @invoice_data = email_notification.generate_inbound_data
    @sub_total = email_notification.inbound_sub_total
    @total_discount = email_notification.total_discount
  end

  def render_tc_invoice
    render pdf: @invoice_data[:invoice_number],
           template: "invoice_pdf/tunecore_invoice_pdf.html.erb",
           layout: "layouts/pdf",
           locals: {
             inbound: true,
             pdf: true,
             invoice_data: @invoice_data,
             sub_total: @sub_total,
             discounts: @total_discount
           },
           header: {
             html: {
               template: "invoice_pdf/invoice_pdf_header.html.erb",
               layout: "layouts/pdf",
               locals: {
                 invoice_data: @invoice_data,
                 static_corporate_entity: @static_corporate_entity
               }
             }
           },
           page_size: "A4"
  end

  def render_bi_invoice
    render pdf: @invoice_data[:invoice_number],
           template: "invoice_pdf/believe_invoice_pdf.html.erb",
           layout: "layouts/pdf",
           locals: {
             inbound: true,
             pdf: true,
             invoice_data: @invoice_data,
             static_corporate_entity: @static_corporate_entity,
             discounts: @total_discount
           },
           footer: {
             html: {
               template: "invoice_pdf/invoice_pdf_footer.html.erb",
               layout: "layouts/pdf",
               locals: { static_corporate_entity: @static_corporate_entity }
             }
           },
           header: {
             html: {
               template: "invoice_pdf/invoice_pdf_header.html.erb",
               layout: "layouts/pdf",
               locals: {
                 invoice_data: @invoice_data,
                 static_corporate_entity: @static_corporate_entity
               }
             }
           },
           page_size: "A4"
  end

  def build_refund_info
    refunds = []
    @invoice.refunds_with_credit_note_invoice.each do |refund|
      refund_info = CreditNoteInvoices::InboundPresenter.new(refund).refund_summery
      refund_info[:link] = invoice_credit_note_invoice_path(
        @invoice,
        refund,
        format: :pdf,
      )
      refunds << refund_info
    end
    refunds
  end
end
