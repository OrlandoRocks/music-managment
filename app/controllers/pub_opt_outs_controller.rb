class PubOptOutsController < ApplicationController
  layout "tc-foundation"

  before_action :require_composer

  def show
    @should_show_button = user_is_active?
  end

  def create
    if user_is_active?
      high_earning_composer = HighEarningComposer.where(person_id: current_user.id).first
      if high_earning_composer.present?
        high_earning_composer.update(attempt_to_opt_out: true)
        MailerWorker.perform_async("PubOptOutMailer", :mail_for_high_earnings_user, current_user.id)
      else
        current_user.composers.update_all(terminated_at: Time.now)
        MailerWorker.perform_async("PubOptOutMailer", :mail_for_regular_user, current_user.id)
      end
    end

    redirect_to publishing_opt_out_path
  end

  private

  def require_composer
    redirect_to dashboard_path unless current_user.composers.exists?
  end

  def user_is_active?
    return false if current_user_is_high_earning_and_tried_to_opt_out?

    !current_user_has_terminated_composer?
  end

  def current_user_has_terminated_composer?
    current_user.composers
                .where(Composer.arel_table[:terminated_at].not_eq(nil))
                .exists?
  end

  def current_user_is_high_earning_and_tried_to_opt_out?
    HighEarningComposer.where(person_id: current_user.id, attempt_to_opt_out: true).exists?
  end
end
