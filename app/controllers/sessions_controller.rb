class SessionsController < ApplicationController
  include TcSocialHelper
  include MetaTags::Taggable
  include RecaptchaVerifiable
  include TwoFactorAuthable
  include SignInCallback

  skip_before_action :login_required
  before_action :build_person, only: [:new]
  before_action(only: [:new]) { |c| c.build_referral_data_from_cookies(:login) }
  before_action(only: [:new]) { |c| c.build_referral_data_from_url(:login) }
  before_action :check_for_banned_ip, only: [:new, :show, :create]
  before_action :login_disallowed, only: [:new, :show, :create]
  before_action :set_page_title, only: [:new]
  before_action :set_meta_tags, only: [:new]
  before_action :set_canonical_link, only: [:new]

  after_action :destroy_active_session_cookie, only: :destroy

  skip_before_action :check_for_verification
  skip_before_action :check_for_terms_and_conditions
  skip_before_action :redirect_ytm_approvers, only: :destroy
  skip_before_action :force_pw_reset, only: :destroy
  skip_before_action :redirect_stem_users, only: :destroy

  layout "registration"

  def new
    preset_redirect_url(params[:redirect_url])

    unless request.path.match?(/\/login$/)
      redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1), status: :moved_permanently
      return
    end

    set_pw_reset_notice if params[:f]

    self.current_user = nil
    @cms_set = CmsSet.active_set("login", country_website, country_website_language).as_json(root: false)
    @hide_captcha = hide_captcha?
    session["client_application"] = params[:ref] if params[:ref]

    if reskin?
      render "new_reskin", layout: "registration_reskin"
    else
      render "new"
    end
  end

  def preview
    render "new"
  end

  def show
    redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1), status: :moved_permanently
  end

  def create
    @page_title = "Login"
    @person = Person.authenticate(person_params)
    if @person
      handle_successful_auth
    elsif Person.account_locked?(person_params[:email])
      person = Person.find_by(email: person_params[:email])
      reroute_locked_account(person)
    else
      sign_in_failure
    end
  end

  def destroy
    # current_user_has_tc_social = current_user.try(:has_tc_social?)

    remove_sso_cookie
    remove_takeover_cookie
    remove_cable_token
    remove_oauth_tokens
    reset_session

    if params[:redirect]
      redirect_to params[:redirect]
    elsif params[:force_reset]
      redirect_to login_path(f: true, ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      # elsif current_user_has_tc_social
      # redirect_to tc_social_logout_url
    else
      redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
    end
  end

  protected

  def handle_successful_auth
    @person.update(dormant: false)

    if two_factor_auth_required?(@person)
      two_factor_auth_session_for(@person)
    else
      captcha_on_login(handle_v3_recaptcha) if use_recaptcha?
      sign_in_success
    end
  end

  def logout_redirect
    if Rails.env.production? && ["AU", "DE", "UK"].include?(country_website)
      home_url(protocol: "http").gsub("web.", "www.")
    else
      home_url
    end
  end

  def reroute_no_cookies
    flash[:error] = custom_t("controllers.sessions.enable_cookies_request")
    @person = Person.new

    render action: "new", flash: flash
  end

  def reroute_locked_account(person)
    key = person.generate_invite_code
    if key
      begin
        PasswordResetService.reset(person.email, :user)
        flash[:error] = custom_t("controllers.sessions.locked_account", email: person.email)
      rescue
        flash[:error] = custom_t("controllers.sessions.locked_account_contact_support")
      end
    end
    @person = Person.new
    render action: "new", flash: flash
  end

  def remove_cable_token
    CableAuthService.remove_token(current_user)
  end

  # 2011-04-05 AK -- Should be called when the user logs out
  def remove_oauth_tokens
    return if current_user.nil?

    oauth_tokens = current_user.tokens.joins(:client_application).where(ClientApplication.arel_table[:name].not_eq("tc_social"))
    destroyed_tokens = oauth_tokens.each(&:destroy)
    logger.info("Destroyed #{destroyed_tokens.size} for user='#{current_user.email}'")
    oauth_tokens
  end

  def check_for_banned_ip
    return unless ENV["PERFORM_LOGIN_ATTEMPT_CHECK"].to_s.casecmp?("true")

    return unless LoginAttempt.ban_threshold_reached?(request.remote_ip)

    render(file: File.join(Rails.root, "public/403.html"), status: :forbidden, layout: false)
  end

  def destroy_active_session_cookie
    cookies.delete(:tc_active_session, domain: ".#{get_cookie_domain}")
  end

  def build_person
    @person = Person.new(email: params[:email])
  end

  private

  def person_params
    params.require(:person).permit(
      :email,
      :password
    )
  end

  def set_pw_reset_notice
    flash.now[:notice] =
      if reskin?
        custom_t("reskin.sessions.emailed_pw_reset_instructions")
      else
        custom_t("controllers.sessions.emailed_pw_reset_instructions")
      end
  end
end
