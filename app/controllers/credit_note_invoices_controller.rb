# frozen_string_literal: true

class CreditNoteInvoicesController < ApplicationController
  before_action :load_person
  before_action :ensure_new_refunds_flag
  before_action :load_invoice
  before_action :ensure_correct_country_domain
  before_action :ensure_invoice_has_static_data

  def show
    refund = @invoice.refunds.find(params[:id])
    @refund_presenter = CreditNoteInvoices::InboundPresenter.new(refund)

    respond_to do |format|
      format.pdf do
        render pdf: @refund_presenter.refund_details[:credit_note_invoice_number],
               formats: [:html],
               margin: { bottom: 5 },
               layout: "layouts/pdf"
      end
    end
  end

  private

  def load_invoice
    @invoice = @person.invoices.find(params[:invoice_id])
  end

  def ensure_invoice_has_static_data
    forbidden if @invoice.has_incomplete_static_data?
  end

  def ensure_new_refunds_flag
    forbidden unless @person.new_refunds_feature_on?
  end
end
