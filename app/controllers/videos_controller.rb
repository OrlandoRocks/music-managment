class VideosController < ApplicationController
  before_action :load_person
  before_action :load_video, only: [:show]

  def index
    @page_title = custom_t("controllers.videography.page_title")
    @videos     = @person.videos

    @complete_music_videos    = @videos.music_videos.select(&:finalized?)
    @complete_feature_films   = @videos.feature_films.select(&:finalized?)
    @incomplete_music_videos  = @videos.music_videos.select  { |x| !x.finalized? }
    @incomplete_feature_films = @videos.feature_films.select { |x| !x.finalized? }

    @complete_videos   = @complete_music_videos.length + @complete_feature_films.length
    @incomplete_videos = @incomplete_music_videos.length + @incomplete_feature_films.length

    @vids_to_display = true if @videos.size.positive?
  end

  def show
    @new_css = true # not sure this is still needed...
  end

  private

  def load_video
    @video =
      if current_user.is_administrator?
        Video.find(params[:id])
      else
        @person.videos.find(params[:id])
      end
  end
end
