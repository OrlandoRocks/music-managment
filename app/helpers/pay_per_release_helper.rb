# frozen_string_literal: true

module PayPerReleaseHelper
  PAY_PER_RELEASE = %w[single_free album_free single album].freeze

  SINGLE_FREE = "single_free"
  ALBUM_FREE  = "album_free"
  SINGLE = "single"
  ALBUM  = "album"
  PRICED_PLAN = %w[single album].freeze
  SINGLE_TYPE = %w[single single_free].freeze

  SINGLE_FREE_DETAILS = {
    name: SINGLE,
    bullet_keys: %w[
      social_platform_release
      keep_revenue_freemium
    ]
  }.freeze

  ALBUM_FREE_DETAILS = {
    name: ALBUM,
    bullet_keys: %w[
      social_platform_release
      keep_revenue_freemium
    ]
  }.freeze

  SINGLE_DETAILS = {
    name: SINGLE,
    bullet_keys: %w[
      digital_store_release
      keep_revenue_standard
    ]
  }.freeze

  ALBUM_DETAILS = {
    name: ALBUM,
    bullet_keys: %w[
      digital_store_release
      keep_revenue_standard
    ]
  }.freeze

  PAY_PER_RELEASE_DETAILS = {
    SINGLE_FREE => SINGLE_FREE_DETAILS,
    ALBUM_FREE => ALBUM_FREE_DETAILS,
    SINGLE => SINGLE_DETAILS,
    ALBUM => ALBUM_DETAILS,
  }.freeze

  def pay_per_price(type)
    return custom_t("pay_per_release.free").to_s unless PRICED_PLAN.include?(type)

    public_send("#{type}_price_with_currency", current_user)
  end

  def price_iteration(type)
    return "" unless PRICED_PLAN.include?(type)

    "/ #{custom_t('plans.year')}"
  end

  def release_type_path(type)
    return new_single_path if SINGLE_TYPE.include?(type)

    new_album_path
  end
end
