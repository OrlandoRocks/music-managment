# frozen_string_literal: true

module PayoneerHelper
  def fill_payoneer_info?(person)
    return false unless session[:return_to].blank? || session[:return_to] == plans_path

    FeatureFlipper.show_feature?(:payoneer_login_redirect, person) && redirect_for_taxes?(person)
  end

  def redirect_for_taxes?(person)
    person.tax_forms.any? == false
  end
end
