module ProductHelper
  extend ActiveSupport::Concern
  include CurrencyHelper

  ALBUM_RENEWAL = "yearly_renewal".freeze
  SINGLE_RENEWAL = "single_renewal".freeze
  IN_ALBUM_ID = 427
  IN_SINGLE_ID = 430

  def product_select_label_text(product)
    text =
      if product.display_name_safely_translated == TargetedProductStoreService::FB_FREEMIUM &&
         FeatureFlipper.show_feature?(:discovery_platforms, current_user)

        TargetedProductStoreService::DISCOVERY
      else
        "#{product.display_name_safely_translated} &ndash;"
      end
    text += " <s>#{parse_to_money(product.original_price, product.currency)}</s>" if product.adjusted_price?

    text += " <span class='black'>#{parse_to_money(product.adjusted_price, product.currency)}</span>"

    text += "<span class='deal'>#{product.flag_text_safely_translated}</span>" if product.flag_text.present?

    text.html_safe
  end

  def product_select_notes(product, no_radio = false)
    if product.current_targeted_product
      text = ""
      if product.current_targeted_product.price_adjustment_description.present?
        text += product.current_targeted_product.price_adjustment_description + "."
      end

      if product.current_targeted_product.show_expiration_date
        expiration_date = product.current_targeted_product.targeted_offer.expiration_date_for_person(current_user)
        text += " Offer ends #{fmt_long_date(expiration_date)}"
      end
    end

    text = "(#{text})" if text.present?

    radio_class = (no_radio == true) ? "no-radio" : ""
    content_tag(:p, text, class: radio_class)
  end

  def price_adjustment_text(targeted_product)
    text = ""
    case targeted_product.price_adjustment_type
    when "override"
      "Overriden"
    when "dollars_off"
      "#{money_to_currency(targeted_product.price_adjustment.to_money(targeted_product.currency))} Dollars Off"
    when "percentage_off"
      "#{format_decimal(targeted_product.price_adjustment, false)}% Off"
    end
  end

  #
  # Formats a string describing the renewal interval of the product.
  #
  def product_renewal_interval_text(product)
    return unless product

    product_item = product.product_items.first
    if product_item.renewal_duration == 1
      "a #{product_item.renewal_interval}"
    else
      "every #{product_item.renewal_duration} #{product_item.renewal_interval.to_s.pluralize}"
    end
  end

  def price_for_product_by_type_and_domain(country_domain, type)
    p = Product.find(Product.find_products_for_country(country_domain, type))
    p = p.first if p.instance_of?(Array)

    p.price.to_money(p.currency)
  end

  def price_for_product_by_type_and_person(person, type)
    country_domain = person.nil? ? LOCALE_TO_COUNTRY_MAP[locale.to_s] : person.country_domain
    price_for_product_by_type_and_domain(country_domain, type)
  end

  def find_soundout_product_price(person, type)
    cw = person.nil? ? CountryWebsite.find_by(country: LOCALE_TO_COUNTRY_MAP[locale.to_s]) : person.country_website
    soundout_product = SoundoutProduct.find_for_purchase(type, cw).product

    soundout_product.set_targeted_product(TargetedProduct.targeted_product_for_active_offer(person, soundout_product))
    price = soundout_product.set_adjusted_price(soundout_product.calculate_price(soundout_product))

    price.to_money(soundout_product.currency)
  end

  def product_list_for_select(country_website, skip_ids = [])
    products =
      if skip_ids.empty?
        Product.where(
          "country_website_id = ?",
          country_website.id
        ).order("applies_to_product, product_type, sort_order")
      else
        Product.where("id not in (?) and country_website_id = ?", skip_ids, country_website.id)
               .order("applies_to_product, product_type, sort_order")
      end

    products.map do |p|
      [
        "#{unless p.applies_to_product == 'None'
             p.applies_to_product + ' | '
           end} #{p.product_type} -- #{p.name} #{" - #{money_to_currency(
             p.price.to_money(p.currency), html: false
           )}" if p.price != nil && p.price != 0.00} (#{p.status}, ID:#{p.id})",
        p.id
      ]
    end
  end

  def soundout_product_with_discount(person, type)
    product = SoundoutProduct.find_for_purchase(type, person.country_website).product
    Product.set_targeted_product_and_price(person, product)

    money_with_discount(product.original_price, product.adjusted_price, product.currency)
  end

  def indian_product_price(product_id)
    product = Product.find_by(id: product_id, country_website_id: 8)
    indian_user = current_user_or_last_verified_india_user
    product.current_targeted_product = TargetedProduct.targeted_product_for_active_offer(indian_user, product)
    original_price = product.calculate_price(eval("#{product.applies_to_product}.new"))
    return money_to_currency(
      original_price.to_money,
      iso_code: indian_user.country_domain,
      currency: indian_user.user_currency
    ) if product.current_targeted_product.nil?

    sale_price = product.current_targeted_product.adjust_price(original_price)
    money_with_discount(
      original_price,
      sale_price,
      product.currency,
      { iso_code: indian_user.country_domain, currency: indian_user.user_currency }
    )
  end

  def renewal_price(product_type)
    case product_type
    when :single
      money_to_currency(
        Product.find_by(
          country_website_id: current_user.country_website_id,
          display_name: SINGLE_RENEWAL
        ).price.to_money
      )
    when :album
      money_to_currency(
        Product.find_by(
          country_website_id: current_user.country_website_id,
          display_name: ALBUM_RENEWAL
        ).price.to_money
      )
    end
  end

  def caveat_copy
    return custom_t(".plan_caveat") if FeatureFlipper.show_feature?(:plans_pricing, current_user)

    custom_t(".caveat")
  end

  private

  def parse_to_money(price, currency)
    amount = price.to_money(currency)
    money_to_currency(amount)
  end

  def current_user_or_last_verified_india_user
    return Person.where(country_website_id: CountryWebsite::INDIA, is_verified: 1).last if @person&.id.nil?

    @person
  end
end
