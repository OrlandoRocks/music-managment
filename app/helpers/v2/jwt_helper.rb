# frozen_string_literal: true

# Generate and decode signed JWTs.
# Save JWT digests and nonces to Redis, keyed by digest/nonce: (digest:person_id).
module V2::JwtHelper
  EMPTY_PAYLOAD = {}.freeze

  EXPIRY_DURATION = Integer(
    ENV.fetch(
      "V2_JWT_EXPIRY_DURATION_IN_SECONDS",
      "86400" # 24 hour default
    ), 10
  )

  PUBLIC_KEY_FILE  = File.read(ENV.fetch("V2_JWT_SIGNING_PUBLIC_KEY_FILE"))
  PRIVATE_KEY_FILE = File.read(ENV.fetch("V2_JWT_SIGNING_PRIVATE_KEY_FILE"))

  EMPTY_HEADER_TOKEN = ""
  SIGNING_ALGO       = "RS256"

  mattr_accessor :redis, default: Redis.new(url: ENV["REDIS_URL"])

  # JWTs
  def decode_jwt(token)
    payload           = {}
    public_key        = OpenSSL::PKey::RSA.new(PUBLIC_KEY_FILE)
    payload, _headers = JWT.decode(token, public_key, true, { algorithm: SIGNING_ALGO }) if token.present?

    payload
  rescue JWT::VerificationError, JWT::DecodeError, JWT::ExpiredSignature
    EMPTY_PAYLOAD
  end

  def decode_header_token
    decode_jwt(header_token)
  end

  def generate_jwt(payload: {}, duration: EXPIRY_DURATION)
    payload[:exp] = Time.current.to_i + duration
    private_key   = OpenSSL::PKey::RSA.new(PRIVATE_KEY_FILE)

    JWT.encode(payload, private_key, SIGNING_ALGO)
  end

  # Nonces

  def generate_nonce
    SecureRandom.urlsafe_base64(64)
  end

  # Header

  def header_token
    request.env["HTTP_AUTHORIZATION"] || EMPTY_HEADER_TOKEN
  end

  def header_token_digest
    digest(header_token)
  end

  # Persistence

  def nonce_key(nonce)
    "#{V2::Authentication::AuthenticationService.name}_nonce:#{nonce}"
  end

  def save_nonce(nonce, duration, person_id)
    redis.setex(
      nonce_key(nonce),
      duration,
      person_id
    )
  end

  def delete_nonce(nonce)
    redis.del(nonce)
  end

  # The user's saved token digest matches the header token digest
  def permitted_token?(digest, id)
    return false if digest.blank?

    token_user(digest) == id
  end

  def token_user(digest)
    return if digest.blank?

    redis.get(digest_key(digest))&.to_i
  end

  def permitted_nonce?(nonce, id)
    return false if nonce.blank? || id.blank?
    return false unless nonce_user(nonce) == id

    delete_nonce(nonce).zero?
  end

  def nonce_user(nonce)
    redis.get(nonce_key(nonce))&.to_i
  end

  def digest_key(digest)
    "#{V2::Authentication::AuthenticationService.name}:#{digest}"
  end

  def save_token_digest(jwt, duration, person_id)
    redis.setex(
      digest_key(digest(jwt)),
      duration,
      person_id
    )
  end

  def digest(jwt)
    return if jwt.blank?

    Digest::SHA2.new(256).hexdigest(jwt)
  end

  def delete_digest(digest)
    redis.del(digest_key(digest))
  end

  # The header token passes verification (via #decode_header_token) and its digest is found in Redis
  def saved_token
    return if header_token.blank?

    decoded      = decode_header_token
    token_digest = digest(header_token)
    id           = decoded["id"]
    return unless decoded.present? && permitted_token?(token_digest, id)

    decoded
  end
end
