module CmsSetHelper
  def asset_logo(type)
    case type
    when "color_black"
      "/images/tc_logos/color/TuneCore_PNG_black_color_horizontal.png"
    when "color_white"
      "/images/tc_logos/color/TuneCore_PNG_white_color_horizontal.png"
    when "knockout_black"
      "/images/tc_logos/knockout/TuneCore_PNG_black_knockout_horizontal.png"
    when "knockout_white"
      "/images/tc_logos/knockout/TuneCore_PNG_white_knockout_horizontal.png"
    end
  end

  def button_color(color)
    case color
    when "blue"
      "btn-callout-alt"
    when "orange"
      "btn-callout"
    when "gray"
      "btn-action"
    end
  end
end
