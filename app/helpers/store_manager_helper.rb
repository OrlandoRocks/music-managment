# frozen_string_literal: true

module StoreManagerHelper
  def cost_display(original_price, current_price, currency)
    return custom_t(:free) if current_price <= 0

    money_with_discount(original_price, current_price, currency)
  end

  def ytsr_proxy?(store)
    Store::YTSR_PROXY_ID == store[:id]
  end

  def expander_container_class(disable_expander)
    if disable_expander
      "store_manager__expander-container--disabled"
    else
      "store_manager__expander-container"
    end
  end

  def stores_section_header_class(disable_expander, stores)
    result = "row"
    result += " store_manager-stores__header--no-border" if disable_expander || stores.empty?

    result
  end
end
