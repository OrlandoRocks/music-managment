module SongsHelper
  def ytm_status_span(song, album_monetize = false, album_id = nil)
    if song.blocker
      %(<span data-tooltip="" class="removed">#{custom_t('helpers.songs.status_ineligible')}</span>).html_safe
    elsif song.ytm
      path = "#{youtube_monetizations_path}/mark_as_eligible?song_id=#{song.id}"

      path += "&amp;album_type=#{album_monetize}&amp;album_id=#{album_id}" if album_monetize

      %(<span data-tooltip="" class="ineligible">#{custom_t('helpers.songs.do_not_monetize')}</span><a href="#{path}" class="undo_ineligibility">Undo</a>).html_safe
    elsif song.song_takedown_at || song.salepoint_takedown_at
      %(<span data-tooltip="" class="taken_down">#{custom_t('helpers.songs.status_down')}</span>).html_safe
    elsif song.state.nil?
      %(<span data-tooltip="" class="missing">#{custom_t('helpers.songs.status_not_submitted')}</span>).html_safe
    elsif song.state == "new"
      %(<span data-tooltip="" class="processing">#{custom_t('helpers.songs.status_processing')}</span>).html_safe
    elsif song.state == "approved"
      if song.salepoint_songs.first.distribution_song.try(:state) != "delivered"
        %(<span data-tooltip="" class="processing">#{custom_t('helpers.songs.status_processing')}</span>).html_safe
      else
        %(<span data-tooltip="" class="sent">#{custom_t('helpers.songs.monetized_on_youtube')}</span>).html_safe
      end
    elsif song.state == "rejected"
      %(<span data-tooltip="" class="removed">#{custom_t('helpers.songs.status_ineligible')}</span>).html_safe
    else
      %(<span data-tooltip="" class="error">#{custom_t('helpers.songs.status_error')}</span>).html_safe
    end
  end

  def get_ytm_track_class(song)
    return "highlight" if !song.ytm && !song.blocker && song.state.nil?

    ""
  end
end
