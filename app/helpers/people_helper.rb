# frozen_string_literal: true

module PeopleHelper
  def current_user_cart_items_count
    items_count = current_user.cart_items_count
    if items_count.zero?
      link_to "&nbsp;", cart_path
    else
      link_to "(#{pluralize(items_count, 'item')})", cart_path
    end
  end

  def youtube_list_element(person)
    "<li>#{custom_t('people.new.make_money_on_youtube')}</li>".html_safe unless person.referred_by("applemusicartist")
  end

  def default_country(country_website_code)
    country_website = CountryWebsite.find_by(country: country_website_code)
    country_website ? country_website.country_iso : "US"
  end

  def full_address(person)
    [
      person.address1,
      person.address2,
      person.city,
      person.state,
      person.postal_code,
      person.country
    ].select(&:present?).join(", ")
  end

  def contact_info(person)
    [person.name, person.phone_number, person.mobile_number, person.business_name].select(&:present?).join(", ")
  end

  def load_cyrillic_countries
    %w[Belarus Kazakhstan Russia Ukraine]
  end

  def renewal_price_with_discount(person, renewal)
    product = Product.find(renewal.item_to_renew.renewal_product_id)
    return money_with_discount(0, 0, product.currency) if renewal.discovery_platform_renewal?

    Product.set_targeted_product_and_price(person, product)

    money_with_discount(product.original_price, renewal.price_to_renew_in_money, product.currency)
  end

  def selected_country_dynamic_default(_ip_country_code)
    # TODO we should have an active record relationship between person and country
    #
    # currently defaulting to country website, as this method isn't being used - would only be used
    # by the autocomplete. uncomment the code below to allow for ip_country_code to be used
    # @person.attributes['country'] || ip_country_code || default_country(country_website)

    @person.attributes["country"] || default_country(country_website)
  end

  def selected_country_text_default
    # TODO we should have an active record relationship between person and country
    @person.attributes["country"] || custom_t("people.people_fields.choose_country")
  end

  def has_no_valid_phone_number?
    !current_user.valid_phone_number?
  end

  def person_has_phone_errors?
    @person.errors.present? && @person.errors.messages.present? && @person.errors.messages.fetch(
      :mobile_number,
      []
    ).present?
  end

  def format_phone_errors
    @person.errors.messages.fetch(:mobile_number, []).map(&:capitalize).join(", ")
  end

  def non_us_and_required_address_info?
    return true unless FeatureFlipper.show_feature?(:bi_transfer, @person)

    @person.required_address_info?
  end

  def bi_transfer?
    FeatureFlipper.show_feature?(:bi_transfer, current_user)
  end

  def self_billding_info(self_billing_status)
    return unless self_billing_status

    custom_t(%(self_billing.#{self_billing_status.accepted? ? 'accept_info' : 'declined_info'}))
  end

  def show_self_billing_info?
    FeatureFlipper.show_feature?(:vat_tax, current_user) && current_user.corporate_entity&.affiliated_to_bi?
  end

  def self_billing_on_withdrawal?
    billing_status = current_user.self_billing_status
    show_self_billing_info? && (billing_status.nil? || billing_status.new_record?)
  end

  def show_declined_confirmation?
    current_page?(new_payout_transfer_path) || current_page?(withdraw_paypal_path)
  end

  def self_billing_declined?
    show_self_billing_info? && current_user.self_billing_status&.declined?
  end

  def show_self_billing_row?
    self_billing_declined? || self_billing_on_withdrawal?
  end

  def paypal_kyc_required?
    current_user.legacy_paypal_enabled?
  end

  def country_state_select_options
    [[custom_t("stored_credit_cards.payu_form.please_select_state"), ""]] + CountryState.pluck(:name, :id)
  end

  def show_company_information_form?
    !current_user.from_united_states_and_territories? &&
      current_user.business?
  end

  def vat_tax_flag_enabled?
    current_user.vat_feature_on?
  end

  def company_information_section_message
    return custom_t("people.company_info.messages.country_change") if @country_change
    return company_information_field_values if company_information_field_values.present?

    custom_t("people.company_info.messages.default")
  end

  def company_information_field_values
    return if current_user.company_information.nil?

    current_user
      .company_information
      .slice(*CompanyInformation::FIELDS)
      .values
      .reject(&:blank?)
      .join(", ")
  end

  def truncated_available_balance(person, decimal_limit = 2)
    person.available_balance.truncate(decimal_limit)
  end

  def show_taxpayer_id_tab?
    current_user.from_united_states_and_territories? &&
      FeatureFlipper.show_feature?(:advanced_tax_blocking, current_user) &&
      !current_user.valid_tax_forms? &&
      current_user.payoneer_account?
  end

  def show_taxform_mapping?
    FeatureFlipper.show_feature?(:tax_form_mapping, current_user) &&
      current_user.from_united_states_and_territories? &&
      @primary_tax_form.present? &&
      @secondary_tax_form.present?
  end

  def selected_distribution_tax_form
    return :primary if @user_mapped_distribution_tax_form == @primary_tax_form
    return :secondary if @user_mapped_distribution_tax_form == @secondary_tax_form

    ""
  end

  def selected_publishing_tax_form
    return :primary if @user_mapped_publishing_tax_form == @primary_tax_form
    return :secondary if @user_mapped_publishing_tax_form == @secondary_tax_form

    ""
  end

  def tax_form_select_options
    @tax_form_select_options ||=
      [
        [tax_form_display_text(@primary_tax_form), :primary],
        [tax_form_display_text(@secondary_tax_form), :secondary]
      ]
  end

  def tax_form_display_text(tax_form)
    return unless tax_form.is_a?(TaxForm)

    [
      tax_form.tax_form_type.kind,
      tax_form.tax_entity_name,
      tax_form.submitted_at.strftime("%Y-%m-%d")
    ].compact.join(", ")
  end

  def account_settings_tab_status(current_tab)
    @tab == current_tab ? "show" : "hide"
  end

  def any_contact_info_locked?
    current_user.first_name_locked? ||
      current_user.last_name_locked? ||
      current_user.company_name_locked?
  end

  def show_address_lock_banner?
    current_user.address_locked?
  end

  def address_lock_banner_message
    return custom_t("people.self_identified_address_lock_message") if current_user.self_identified_address_locked?

    custom_t("people.kyc_lock_message")
  end

  def plan_display_price
    country_website_record = CountryWebsite.find_by(country: country_website)
    plan_product_price = Product
                         .joins(:plan)
                         .where(
                           country_website_id: country_website_record.id,
                           plans: { id: Plan::RISING_ARTIST }
                         ).first.price

    money_to_currency(
      plan_product_price.to_money(country_website_record.currency),
      iso_code: country_website_record.country,
      currency: country_website_record.user_currency,
      round_value: country_website_record.india?
    )
  end

  def priority_artist_name
    priority_artist = current_user.priority_artist_artist.presence || current_user.oldest_primary_artist
    priority_artist.name
  end

  def show_priority_artists?
    current_user.has_plan? && current_user.can_do?(:buy_additional_artists) && current_user.creatives.active.exists?
  end
end
