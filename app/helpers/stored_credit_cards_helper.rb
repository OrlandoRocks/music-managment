module StoredCreditCardsHelper
  # produces the hidden fields for braintree post. This replaces the
  # payment_services_for helper provided by the ActiveMerchant Library
  def braintree_fields(order, account, options = {})
    integration_module = ActiveMerchant::Billing::Integrations::Braintree
    service_class = integration_module.const_get("Helper")
    service = service_class.new(order, account, options)
    yield service

    result = ""
    service.form_fields.each do |field, value|
      result << hidden_field_tag(field, value)
    end
    result.html_safe
  end

  def edit_stored_credit_card_link(stored_card)
    if stored_card.is_braintree_card?
      link_to(
        custom_t("carts.confirm_and_pay.edit_card"),
        edit_stored_credit_card_path(stored_card, redirect: request.fullpath)
      )
    else
      ""
    end
  end

  def edit_stored_credit_card_button(stored_card)
    if stored_card.is_braintree_card?
      link_to(
        custom_t(:edit),
        edit_stored_credit_card_path(stored_card),
        class: "button-interface blue-button"
      )
    else
      ""
    end
  end

  def edit_failed_stored_credit_card_button(stored_card)
    return "" unless stored_card.is_braintree_card?

    link_to(
      custom_t("people.edit_profile_payment.update"),
      edit_stored_credit_card_path(stored_card),
      class: "button-interface blue-button warning-update"
    )
  end

  # returns the braintree post url from the activemerchant library
  def braintree_post_url
    integration_module = ActiveMerchant::Billing::Integrations::Braintree
    integration_module.service_url
  end

  def padded_expiration_month(m)
    month = m.to_s
    if month.length == 1
      "0#{month}"
    else
      month
    end
  end

  def india_country_for_dropdown
    in_country = Country.find_by(name: Country::INDIA_NAME)
    [[in_country.name, in_country.iso_code]]
  end
end
