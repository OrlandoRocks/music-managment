module MarketingHelper
  def cta_callout_enabled_for?(user)
    FeatureFlipper.show_feature?(:hubspot_cta, user) && cta_enabled_countries.include?(user.country_domain)
  end

  def cta_enabled_countries
    ["US"]
  end
end
