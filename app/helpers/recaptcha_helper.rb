module RecaptchaHelper
  def use_recaptcha?
    ENV["RECAPTCHA_SITE_KEY"] &&
      ENV["RECAPTCHA_SECRET_KEY"] &&
      ENV["RECAPTCHA_V3_SITE_KEY"] &&
      ENV["RECAPTCHA_V3_SECRET_KEY"] &&
      ENV["RECAPTCHA_THRESHOLD"]
  end

  def use_recaptcha_v3?
    ["sessions", "reset_passwords"].include?(controller.controller_name)
  end
end
