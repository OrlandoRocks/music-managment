module RingtoneHelper
  def ringtone_songlist(_user = current_user)
    albums = current_user.albums.finalized.limit(35)
    albums.map { |album|
      album.songs.map { |song|
        {
          "lc_name" => song.name.downcase,
          "display_name" => song.name,
          "id" => song.id,
          "album" => song.album.name,
          "track_num" => song.track_num
        }
      }
    }.flatten!
  end

  def label_name_display_value(ringtone)
    return ringtone.artist_name if ringtone.label_name.blank?

    ringtone.label_name
  end
end
