module MediaDisplayNameHelper
  def display_name(media)
    begin
      if (media.tc_instance_of?(Song) && song_should_titleize(media)) ||
         (!media.tc_instance_of?(Song) && album_should_titleize(media))

        Utilities::Titleizer.titleize_with_featured_artists(media)
      else
        media.name
      end
    rescue ActiveModel::MissingAttributeError
      media = media.class.find(media.id)
      display_name(media)
    end
  end

  def song_should_titleize(media)
    !media.album.try(:allow_different_format) && !media.featured_artists.empty?
  end

  def album_should_titleize(media)
    !media.featured_artists.empty? && !(media.try(:allow_different_format) || media.try(:has_non_formattable_language_code?))
  end
end
