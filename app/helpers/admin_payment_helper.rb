module AdminPaymentHelper
  include PaymentHelper

  def transfer_actions(item)
    actionlist = []

    case item.transfer_status
    when "pending"
      unless item.is_a?(PaypalTransfer)
        # actionlist << link_to('process', :action => 'process_payment', :id => item.id, :type => (item.is_a? PaypalTransfer) ? 'paypal' : 'check')
        actionlist << link_to(raw("&raquo; process"), { action: "process_payment", id: item.id, type: "check" }, { class: "confirm-link" })
      end
      actionlist << link_to(
        raw("&raquo; cancel"),
        admin_payment_cancel_payment_path(
          id: item.id,
          type: (item.is_a? PaypalTransfer) ? "paypal" : "check"
        ),
        { class: "confirm-link", method: :post }
      )
    when "processing"
      actionlist << link_to(raw("&raquo; complete"), { action: "complete_payment", id: item.id, type: (item.is_a? PaypalTransfer) ? "paypal" : "check" }, { class: "confirm-link" })
      actionlist << link_to(raw("&raquo; fail"), { action: "fail_payment", id: item.id, type: (item.is_a? PaypalTransfer) ? "paypal" : "check" }, { class: "confirm-link" })
    end

    return "&nbsp;" if actionlist.length.zero?

    actionlist.join("<br>").to_s.html_safe
  end

  def transfer_payee(item)
    case item
    when PaypalTransfer
      CGI::escapeHTML(item.paypal_address.to_s)
    when CheckTransfer
      [:payee, :address1, :address2, :city, :state, :postal_code, :country, :phone_number].collect { |field|
        CGI::escapeHTML(item.send(field).to_s)
      }.reject(&:blank?).join("<br />").html_safe
    else
      "unknown transfer type"
    end
  end
end
