module CountryPhoneCodesHelper
  def custom_t_name_and_phone_codes_for_countries
    CountryPhoneCode.codes_with_country_names.map do |record|
      [country_name_with_country_code(record), record.code]
    end
  end

  def country_name_with_country_code(record)
    "#{custom_t("country.name.#{country_name_translation(record.name_raw)}")} (#{record.code})"
  end

  def country_name_translation(country_name)
    I18n.transliterate(country_name).gsub(/\s|'|-/, "_").downcase.gsub(/\(|\)|,/, "")
  end
end
