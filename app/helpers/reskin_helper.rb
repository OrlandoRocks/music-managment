module ReskinHelper
  include RoyaltySplitHelper

  STAGING_CDN    = "https://cdn-staging.tunecore.com".freeze
  PRODUCTION_CDN = "https://cdn-production.tunecore.com".freeze

  def cdn_base_url
    return PRODUCTION_CDN if Rails.env.production?

    STAGING_CDN
  end

  def cdn_url(relative_path)
    "#{cdn_base_url}#{relative_path}"
  end

  def publishing_url
    if current_user_has_composers?
      publishing_administration_composers_url
    else
      music_publishing_admin_link
    end
  end

  def plan_nav_data
    plan = current_user.plan
    return { features: {} } unless plan

    {
      id: plan.id,
      display_name: plan_display_name(plan),
      features: {
        accept_splits: current_user.can_accept_splits?,
        create_splits: current_user.can_create_splits?,
      },
      name: plan.name,
    }
  end

  def v2_nav_translations
    {
      account: t("reskin_nav.account"),
      add_release: t("reskin_nav.add_release"),
      admin: t("reskin_nav.admin"),
      album: t("reskin_nav.album"),
      analytics: t("reskin_nav.analytics"),
      artist_services: t("reskin_nav.artist_services"),
      balance_history: t("reskin_nav.balance_history"),
      created_splits_link_label: t("reskin_nav.created_splits_link_label"),
      discography: t("reskin_nav.discography"),
      distribution_credits: t("reskin_nav.distribution_credits"),
      facebook_instagram_reels: t("reskin_nav.facebook_instagram_reels"),
      give_back_control: t("reskin_nav.give_back_control"),
      go_unlimited: t("reskin_nav.go_unlimited"),
      how_much_cost: t("reskin_nav.how_much_cost"),
      lock_icon_aria_label: t("reskin_nav.lock_icon_aria_label"),
      log_out: t("reskin_nav.log_out"),
      mobile_menu_aria_label: t("reskin_nav.menu"),
      money_and_analytics: t("reskin_nav.money_and_analytics"),
      my_account_display: t("my_account_display"),
      notifications_header: t("notifications_header"),
      publishing: t("reskin_nav.publishing"),
      releases: t("reskin_nav.releases"),
      ringtone: t("reskin_nav.ringtone"),
      sales_report: t("reskin_nav.sales_report"),
      select_a_language: t("reskin_nav.select_a_language"),
      shopping_cart: t("controllers.carts.shopping_cart"),
      single: t("reskin_nav.single"),
      social_monetization: t("reskin_nav.social_monetization"),
      splits_invites_link_label: t("reskin_nav.splits_invites_link_label"),
      store_manager: t("reskin_nav.store_manager"),
      tunecore_cost: knowledgebase_link_for("plans-release-pricing", current_user.country_domain),
      unlimited_plan: t("reskin_nav.unlimited_plan"),
      unlimited_plans: t("reskin_nav.unlimited_plans"),
      view_pricing: t("reskin_nav.view_pricing"),
      what_release: t("reskin_nav.what_release"),
      youtube_content_id: t("reskin_nav.youtube_content_id"),
    }
  end

  def v2_nav_data
    return {} if current_user.nil?

    {
      admin: {
        is_admin: current_user.is_administrator?,
        is_under_admin_control: under_admin_control?
      },
      cart_items_count: current_user.cart_items_count,
      country_website: country_website,
      current_lang: @current_locale_lang,
      current_locale_lang_abbrv: @current_locale_lang_abbrv,
      language_selector_options: @language_selector_options,
      plan: plan_nav_data,
      publishing_url: publishing_url,
      show: {
        created_splits: show_created_splits_menu_link?,
        distribution_credits: !owns_or_carted_plan?,
        fb_monetization: !hide_legacy_fb_monetization?,
        splits_invites: show_invited_splits_menu_link?
      },
      translations: v2_nav_translations,
      unseen_notifications_count: Notification
        .unseen_notification_count(current_user),
      user_id: current_user.id,
    }
  end
end
