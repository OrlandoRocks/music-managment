module NotificationsHelper
  def notification_class(notification)
    if notification.first_clicked.blank?
      "unread"
    else
      "read"
    end
  end
end
