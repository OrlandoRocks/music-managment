module KnowledgebaseLinkHelper
  def knowledgebase_link_for(article_slug, country_code = nil)
    main_knowledgebase_domain + country_path(country_code) + path_to_article(:slug, article_slug)
  end

  # TODO this should all be service instead of adding another entry point
  def knowledgebase_link_for_article_id(article_id, country_code = nil)
    main_knowledgebase_domain + country_path(country_code) + path_to_article(:article_id, article_id)
  end

  def path_to_article(type, article_lookup)
    article = find_article(type, article_lookup)
    article_type = article.route_segment || "/articles/"

    "#{article_type}#{article.article_id}"
  end

  def find_article(type, article_lookup)
    article =
      case type
      when :slug
        KnowledgebaseLink.find_by(article_slug: article_lookup)
      when :article_id
        KnowledgebaseLink.find_by(article_id: article_lookup)
      end

    article.presence || default_knowledgebase_link
  end

  def default_knowledgebase_link
    KnowledgebaseLink.find_by(article_slug: "help-homepage")
  end

  def main_knowledgebase_domain
    "//support.tunecore.com"
  end

  def country_path(country_code)
    {
      "US" => "/hc/en-us",
      "CA" => "/hc/en-ca",
      "UK" => "/hc/en-gb",
      "AU" => "/hc/en-au",
      "DE" => "/hc/de",
      "FR" => "/hc/fr-fr",
      "IT" => "/hc/it",
      "IN" => "/hc/en-in"
    }.fetch(country_code, "/hc/en-us")
  end

  def store_info_knowledgebase_link(store_group, is_ringtone = false)
    article_slug = is_ringtone ? "ringtone" : store_group.abbreviation
    knowledgebase_link_for(Store::STORE_INFO_LINKS[article_slug], current_user.country_domain)
  end

  def help_homepage_for_country
    main_knowledgebase_domain + country_path(country_website)
  end
end
