module PersonSubscriptionsHelper
  def subscription_term_text(product_type)
    {
      monthly: custom_t(:month),
      annually: custom_t(:year).downcase
    }[product_type.to_sym]
  end

  def subscription_type_text(subcription_product)
    custom_t("tc_social_subscription.social_pro") if subcription_product.product_name == "Social"
  end
end
