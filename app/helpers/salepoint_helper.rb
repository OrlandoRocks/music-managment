module SalepointHelper
  REJECTED_STORES = [
    "us",
    "au",
    "ca",
    "eu",
    "jp",
    "uk",
    "mx",
    "la",
    "us",
    "pa",
    "aod_us"
  ]

  def all_stores
    (@album.selected_stores(current_user.id).exclude_discovery(current_user) |
    @album.available_stores(current_user.id).exclude_discovery(current_user)
    ).sort_by(&:position)
  end

  def store_groups
    StoreGroup.all
  end

  def stores_not_in_store_groups
    stores_in_store_groups = store_groups.map(&:stores).flatten
    all_stores - stores_in_store_groups
  end

  def stores_from_plan
    @stores_from_plan ||= current_user.stores_from_plan
  end

  def store_group_views
    options = {
      salepoints: @salepoints,
      salepointable: @album,
      stores_from_plan: stores_from_plan
    }

    store_group_views =
      store_groups.map do |store_group|
        StoreGroupView.new(
          options.merge(
            name: store_group.name,
            store_id: store_group.store_group_stores.first.store_id, # this will cause problems if we expand StoreGroups
            store_group: store_group,
            abbreviation: store_group.abbreviation,
            stores: store_group.stores.joins("left join salepointable_stores ss on stores.id=ss.store_id").where(
              "ss.salepointable_type=? and ss.id IS NOT NULL", @album.class.name
            )
          )
        )
      end
    store_group_views +=
      stores_not_in_store_groups.map do |store|
        StoreGroupView.new(
          options.merge(
            name: store.name,
            abbreviation: store.abbreviation,
            store_id: store.id,
            stores: [store]
          )
        )
      end
    store_group_views
  end

  def digital_stores
    REJECTED_STORES << "ytm" if Store.consolidate_google_ytm_store?
    store_group_views.reject do |sg|
      REJECTED_STORES.include?(sg.abbreviation)
    end
  end

  def discovery_platform_stores
    options = {
      salepoints: @salepoints,
      salepointable: @album,
      stores_from_plan: stores_from_plan
    }

    @discovery_platform_stores ||=
      Store.discovery_platforms(current_user).map do |store|
        StoreGroupView.new(
          options.merge(
            name: store.name,
            abbreviation: store.abbreviation,
            store_id: store.id,
            stores: [store]
          )
        )
      end
  end

  def setup_store_group_views
    @digital_stores = digital_stores
    @all_checked = @digital_stores.reject { |group| group.abbreviation == "aod_us" }.all?(&:is_checked?)
    @physical_stores = store_group_views.select { |sg| sg.abbreviation == "aod_us" }
    @apple_music_checked = @album.apple_music || @digital_stores.select(&:is_itunes?).any?(&:is_checked?)
    @store_type = :digital_stores
  end

  def setup_discovery_platform_views
    @digital_stores = discovery_platform_stores.select { |dps| !dps.ytsr? }
    @all_checked = discovery_platform_stores.reject { |group| group.abbreviation == "aod_us" }.all?(&:is_checked?)
    @ytsr_group = discovery_platform_stores.find(&:ytsr?)
    @store_type = :discovery_platforms
  end

  def existing_salepoints(album, scope)
    salepoints = album.salepoints.send(scope, current_user)

    salepoints.each_with_object({}) do |salepoint, hash|
      hash[salepoint.store_id.to_s] = {

        salepoint: {
          id: salepoint.id.to_s,
          store_id: salepoint.store_id.to_s,
          variable_price_id: salepoint.variable_price_id.to_s,
          has_rights_assignment: salepoint.has_rights_assignment.to_s
        }
      }
    end
  end

  def youtube_music?(store_group)
    store_group.stores.first.id == Store::GOOGLE_STORE_ID
  end

  def show_yt_ytsr?(store_group)
    youtube_music?(store_group) && store_group.stores.first.discovery_platform?
  end

  def store_salepoints_params(stores)
    stores.each_with_object({}) do |store, hash|
      hash[store.id] = {

        salepoint: {
          store_id: store.id.to_s,
          variable_price_id: store.default_variable_price&.id.to_s,
          has_rights_assignment: store.needs_rights_assignment.to_s
        }
      }
    end
  end

  def render_ytsr_blocked?
    ytsr_discovery_platform_option_disabled? || ineligible_for_ytsr? || user_blocked_from_ytsr?
  end

  def ineligible_for_ytsr?
    @tracks = TrackMonetization.where({ song: @songs, store_id: Store::YTSR_STORE_ID })

    @tracks.present? && @tracks.size == @songs.size && (ineligible_for_ytsr_status? || all_ytsr_blocked?)
  end

  def user_blocked_from_ytsr?
    current_user.ytm_blocked?
  end

  def all_ytsr_blocked?
    @blockers = TrackMonetizationBlocker.where({ song: @songs, store_id: Store::YTSR_STORE_ID })
    @blockers.pluck(:song_id).sort == @songs.pluck(:id).sort && @blockers.length != 0
  end

  def ineligible_for_ytsr_status?
    @tracks.all? do |track|
      track.eligibility_status == "ineligible" ||
        (track.state != "new" && track.state != "blocked") ||
        track.song.track_monetization_blockers.exists?(store_id: Store::YTSR_STORE_ID)
    end
  end

  def ytsr_discovery_platform_option_disabled?
    FeatureFlipper.show_feature?(:ytsr_discovery_platform_option_disabled, current_user)
  end

  def all_stores_delivered?
    purchased_stores = @album.salepoints.payment_applied

    case @store_type
    when :digital_stores
      filtered_store_ids = @digital_stores.map(&:store_id)
      filtered_purchased_store_ids = purchased_stores.exclude_discovery(current_user).map(&:store_id)
    when :discovery_platforms
      filtered_store_ids = discovery_platform_stores.map(&:store_id)
      filtered_purchased_store_ids = purchased_stores.discovery(current_user).map(&:store_id)
    end

    (filtered_store_ids - filtered_purchased_store_ids).empty?
  end

  class StoreGroupView
    include ActionView::Helpers
    include CustomTranslationHelper

    attr_accessor :stores, :name, :abbreviation, :salepoints, :salepointable, :store_group, :stores_from_plan,
                  :store_is_from_plan, :store_id

    def initialize(options)
      @name = options[:name] || ""
      @store_id = options[:store_id] || ""
      @stores = options[:stores] || []
      @abbreviation = options[:abbreviation]
      @salepoints = options[:salepoints] || []
      @salepointable = options[:salepointable]
      @store_group = options[:store_group]
      @stores_from_plan = options[:stores_from_plan] || []
      @store_is_from_plan = stores_from_plan.any? { |plans_store| stores.include?(plans_store) }
    end

    def is_itunes?
      @store_group.is_itunes? if @store_group
    end

    def is_discovery_platform?
      stores.any?(&:discovery_platform?)
    end

    def ytsr?
      @stores.find { |s| s.id == Store::YTSR_PROXY_ID }.present?
    end

    def active_stores
      purchased_store_ids = salepoints.collect(&:store_id)
      stores.select { |store| store.is_active? || purchased_store_ids.include?(store.id) }
    end

    def short_name
      name.downcase.tr(" ", "-")
    end

    def default_store
      stores.first
    end

    def salepoint
      salepoints.find { |s| s.store_id == default_store.id }
    end

    def selection_checkbox
      id = default_store.id
      result = ""
      result += check_box_tag(
        "store_group_store_id_#{id}",
        id,
        is_checked?,
        class: "deliver",
        disabled: is_disabled?,
        id: "album_store_selection_#{id}"
      )
      result += label_tag("deliver_here", custom_t(:deliver_here), { for: "album_store_selection_#{id}" })
      result.html_safe
    end

    def salepoint_stores
      salepoints.map(&:store).flatten
    end

    def is_checked?
      is_checked = salepoint_stores.any? { |store| stores.include?(store) }
      is_checked ||= salepointable.includes_automated_store?(stores)
      is_checked ||= store_is_from_plan && salepoint.present?
      is_checked
    end

    def is_disabled?
      dis = salepoint.blank? ? false : salepoint.is_disabled?
      dis ||= (salepointable.payment_applied && salepointable.finalized_at.blank?) if salepointable.present?
      dis ||= salepointable.includes_automated_store?(stores) if salepointable.present?
      dis
    end

    def salepoint_title
      if salepointable.is_a?(Ringtone)
        name.gsub(/iTunes/, "iPhone Ringtone Store")
      else
        name
      end
    end
  end

  class StoreView
    include ActionView::Helpers

    attr_accessor :store, :salepoints, :salepointable, :store_group, :already_checked

    def initialize(options = {})
      @store = options[:store]
      @salepoints = options[:salepoints]
      @salepointable = options[:salepointable]
      @store_group = options[:store_group]
      @already_checked = options[:already_checked]
    end

    def salepoint
      salepoints.find { |s| s.store_id == store.id }
    end

    def id_hidden_field
      hidden_field_tag "album[salepoints]#{store.id}[salepoint][id]", salepoint.id if salepoint
    end

    def store_id_hidden_field
      hidden_field_tag "album[salepoints]#{store.id}[salepoint][store_id]", store.id if is_disabled?
    end

    def is_disabled?
      return false unless salepoint

      salepoint.is_disabled?
    end

    def checkbox
      check_box_tag "album[salepoints]#{store.id}[salepoint][store_id]",
                    store.id,
                    checked,
                    disabled: is_disabled?,
                    id: "salepoint_store_id_#{store.id}",
                    class: checkbox_class
    end

    def checkbox_class
      result = "salepoint-checkbox"
      result += " hide" unless store_group
      result
    end

    def salepoint_label
      return unless store_group

      label :salepoint,
            :store_id,
            title,
            for: "salepoint_store_id_#{store.id}"
    end

    def title
      store.name.gsub(/iTunes/, "")
    end

    def price_label
      return unless store_group
    end

    def variable_price
      variable_price =
        if salepoint
          salepoint.variable_price
        else
          store.default_variable_price
        end
    end

    def variable_price_id_hidden_field
      return unless variable_price

      hidden_field_tag(
        "album[salepoints]#{store.id}[salepoint][variable_price_id]",
        variable_price.id,
        class: "variable-price-hidden-field"
      )
    end

    def checked
      already_checked || salepoints.any? { |salepoint| salepoint.store_id == store.id }
    end
  end
end
