# frozen_string_literal: true

module PlansHelper
  include PayoneerHelper

  NEW_ARTIST      = "new_artist"
  RISING_ARTIST   = "rising_artist"
  BREAKOUT_ARTIST = "breakout_artist"
  PROFESSIONAL    = "professional"
  LEGEND          = "legend"
  TC_DOMAIN       = "tunecore"

  NEW_ARTIST_DETAILS = {
    bg_color: "green",
    bullet_keys: %w[
      monetize
      paid_weekly
      youtube_oac
    ],
  }.freeze

  RISING_ARTIST_DETAILS = {
    bg_color: "blue",
    bullet_keys: %w[
      spotify_verified
      sales_reports
      artist_support
    ],
  }.freeze

  BREAKOUT_ARTIST_DETAILS = {
    bg_color: "red",
    bullet_keys: %w[
      scheduled_release_date
      cover_art
      trends
    ],
  }.freeze

  PROFESSIONAL_DETAILS = {
    bg_color: "yellow",
    bullet_keys: %w[
      custom_label
      pro_panels
      promotion
    ],
  }.freeze

  PLAN_DETAILS = {
    NEW_ARTIST => NEW_ARTIST_DETAILS,
    RISING_ARTIST => RISING_ARTIST_DETAILS,
    BREAKOUT_ARTIST => BREAKOUT_ARTIST_DETAILS,
    PROFESSIONAL => PROFESSIONAL_DETAILS,
  }.freeze

  def splits_product
    @splits_product ||= Product.find_by(
      display_name: "splits_collaborator",
      country_website_id: current_user.country_website_id
    )
  end

  def splits_collaborator_price_without_units
    splits_price_as_money = splits_product.price_as_money

    return splits_price_as_money unless current_user.india_user?

    money_to_unformatted_amount(splits_price_as_money, iso_code: current_user.country_domain).round
  end

  def plan_price(plan)
    product = plan.product_by_person(current_user)
    return custom_t("plans.free") if product.price.zero?

    price_to_money = Product.price(current_user, product)

    return price_to_money unless current_user.india_user?

    # if india user, we need to convert price to INR and round to whole num
    money_to_unformatted_amount(price_to_money, iso_code: current_user.country_domain).round
  end

  def plan_currency(product)
    product.price.zero? ? "" : current_user.country_wesbite_currency_symbol
  end

  def plan_iteration(product)
    product.price.zero? ? "" : "/#{custom_t('plans.year')}"
  end

  def continue_redirect_path(current_user)
    referrer_uri = URI(request.referrer || "")

    return edit_person_path(current_user, tab: "tax_info") if fill_payoneer_info?(current_user)

    return request.referrer if valid_internal_referrer?(referrer_uri, current_user)

    return dashboard_path if current_user.cart_items_count.zero?

    cart_path
  end

  def excluded_redirect_paths(person)
    [
      complete_verify_person_path(person),
      dashboard_create_account_path,
      login_path,
      plans_path,
      signup_path,
    ]
  end

  def valid_internal_referrer?(uri, person)
    return false if uri.host.blank? || (uri.host.exclude? TC_DOMAIN)

    excluded_redirect_paths(person).exclude? uri.path
  end

  def disable_cta_button?(plan, eligible)
    return true if eligible == false
    return false if add_splits_layout?(plan, current_user)

    downgrade?(plan) || current_user.person_plan&.expired?
  end

  def cta_button(plan, eligible)
    return custom_t("plans.add_splits") if add_splits_layout?(plan, current_user) && eligible
    return custom_t("plans.current_plan") if plan == current_user.plan
    return custom_t("plans.ineligible") unless eligible && !downgrade?(plan)

    current_user.has_plan? ? custom_t("plans.upgrade") : custom_t("plans.get_started")
  end

  def add_splits_layout?(plan, user)
    return false unless plan.free_plan?
    return false if user.active_splits_collaborator?

    user.has_unaccepted_split_invites? &&
      (user.current_plan?(Plan::NEW_ARTIST) || user.plan.nil?) &&
      !user.can_accept_splits?
  end

  def continue_button
    current_user.has_plan? ? custom_t("plans.continue_with_current") : custom_t("plans.continue_without_plan")
  end

  def plans_header
    if current_user.has_plan?
      custom_t("plans.upgrade_header")
    elsif current_user.no_plan_and_active_releases?
      custom_t("plans.active_releases_header")
    else
      custom_t("plans.no_plan_header")
    end
  end

  def plans_subheader
    if current_user.has_plan?
      custom_t("plans.upgrade_subheader")
    elsif current_user.no_plan_and_active_releases?
      custom_t("plans.active_releases_subheader")
    else
      custom_t("plans.no_plan_subheader")
    end
  end

  def current_plan?(plan)
    plan == current_user.plan
  end

  def downgrade?(plan)
    PersonPlan.downgrade?(current_user, plan)
  end

  def plan_disabled?(plan)
    return false if add_splits_layout?(plan, current_user)

    current_plan?(plan)
  end

  def hide_from_plan_user?(plan, eligible)
    current_plan?(plan) == false && (eligible == false && downgrade?(plan))
  end

  def hide_from_legacy_user?(eligible)
    current_user.legacy_user? && eligible == false
  end

  def plan_hidden?(plan, eligible)
    hide_from_plan_user?(plan, eligible) || hide_from_legacy_user?(eligible)
  end

  def plan_name(plan)
    custom_t("plans.#{plan.name}.name")
  end

  def learn_more_link
    custom_t(
      "plans.learn_more",
      link: knowledgebase_link_for_article_id(
        KnowledgebaseLink::PLAN_LEARN_MORE_ARTICLE_ID,
        current_user.country_domain
      )
    )
  end

  def additional_artist_price
    price = Product.find_by(
      display_name: "additional_artist",
      country_website: current_user.country_website
    ).price.to_money

    money_to_currency(price) + "/#{custom_t('plans.year')}"
  end

  def splits_collaborator_price
    price = Product.find_by(
      display_name: Product::SPLITS_COLLABORATOR_DISPLAY_NAME,
      country_website: current_user.country_website
    ).price.to_money

    money_to_currency(price) + "/#{custom_t('plans.year')}"
  end

  def show_new_artist_messaging?
    plan_display_valid? && current_user.current_plan?(Plan::NEW_ARTIST)
  end

  def plan_survey_id
    Survey.find_by(name: Survey::PLAN_EMOJI_SURVEY).id
  end

  def user_has_plan_survey?
    survey = Survey.find_by(name: Survey::PLAN_EMOJI_SURVEY)
    current_user.survey_responses.exists?(survey: survey)
  end

  def show_plan_survey?
    FeatureFlipper.show_feature?(:plan_survey, current_user) && !user_has_plan_survey?
  end

  def user_upgradable_plan?(plan)
    @user_upgradable_plan ||= Plan.upgradable_by_user.exists?(id: plan.id)
  end

  def no_vertical_alignment_id(plan)
    "id=no_vertical_alignment_plan_nav" unless user_upgradable_plan?(plan)
  end

  def loading_modal_for_trends?
    current_user.feature_gating_can_do?(:trend_reports)
  end

  def show_pay_per_release_options?
    !current_user.has_plan?
  end
end
