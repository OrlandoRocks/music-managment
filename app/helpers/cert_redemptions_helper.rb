module CertRedemptionsHelper
  def redemption_start_create_link(inventory)
    inventory_types = inventory.collect(&:inventory_type)
    css_class = "button submit"
    if inventory_types.include?("Album")
      link_to "Distribute My Music", new_album_path, class: css_class
    elsif inventory_types.include?("Single")
      link_to "Distribute My Music", new_single_path, class: css_class
    elsif inventory_types.include?("Ringtone")
      link_to "Distribute My Music", new_ringtone_path, class: css_class
    else
      link_to "Distribute My Music", new_album_path, class: css_class
    end
  end

  def redemption_text_copy(brand_code)
    case brand_code
    when "gcdistro"
      render partial: "cert_redemptions/guitar_center"
    when "alfreddistro"
      render partial: "cert_redemptions/alfred"
    when "ourstagedistro"
      render partial: "cert_redemptions/our_stage"
    else
      render partial: "cert_redemptions/guitar_center"
    end
  end
end
