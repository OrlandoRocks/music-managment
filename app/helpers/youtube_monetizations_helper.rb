# frozen_string_literal: true

module YoutubeMonetizationsHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : ""
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
    link_to title, { sort: column, direction: direction }, { class: css_class + " sort" }
  end

  def eligible_add_to_cart?(album_count, youtube_monetization)
    album_count >= 1 &&
      (youtube_monetization.blank? || youtube_monetization.present? && youtube_monetization.terminated?)
  end

  # For the ytm_dash_sprite.png referenced in public/stylesheets/youtube_monetizations.css
  def currency_image_class
    if current_user.user_currency == CountryWebsite::EURO_ABBREVIATION
      "section_ico euro"
    else
      "section_ico"
    end
  end
end
