module IndexHelper
  # TODO remove this and just render the partial in the view
  def render_cms_content(pagename = params[:action])
    filepath     = "#{CONTENT_DIR_REL_PATH}_#{pagename}__tunecore_neo.erb"
    partial_name = "#{CONTENT_DIR_VIEW_PATH}#{pagename}__tunecore_neo"
    render partial: partial_name if File.exist?(filepath)
  end

  def render_terms(term_page)
    render partial: "shared/terms/#{get_country_prefix}_#{term_page}"
  end

  def get_country_prefix
    ["UK", "AU", "DE", "FR", "IT"].include?(country_website) ? country_website.downcase : "us"
  end
end
