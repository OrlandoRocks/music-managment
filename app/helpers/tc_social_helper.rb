module TcSocialHelper
  def tc_social_dashboard_url
    params = { tc: true }.to_query
    tc_social_host + "?" + params
  end

  def tc_social_login_url(token, options = {})
    encrypted_token = SessionEncryptionEngine.encrypt(token)
    params          = {
      token: encrypted_token,
      ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1
    }.merge(options).to_query

    tc_social_host + "/login?" + params
  end

  def tc_social_logout_url
    tc_social_host + "/logout"
  end

  def tc_social_oauth_url
    tc_social_host + "/users/auth/tunecore"
  end

  def tc_social_linkshare_url
    tc_social_host + "/link-share"
  end

  def tc_social_host
    TC_SOCIAL_CONFIG["HOST"]
  end

  def tc_social_allowed_for?(user)
    @tc_social_allowed ||= tc_social_visible_for?(user)
  end

  def tc_social_active_for?(user)
    @tc_social_active ||= tc_social_allowed_for?(user) && user.has_tc_social?
  end

  def tc_social_nav_path
    if tc_social_allowed_for?(current_user) && !current_user.has_tc_social?
      "#{country_specific_domain}/social"
    else
      tc_social_index_path
    end
  end

  def tc_social_visible_for?(user)
    ClientApplication::SOCIAL_ALLOWED_REGIONS.include?(user.country_domain) && user.has_tc_social?
  end

  def tc_social_push_notification_url
    ENV["SOCIAL_API_BASE_URL"] + "/purchases/in_app/notify_user"
  end
end
