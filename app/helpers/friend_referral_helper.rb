module FriendReferralHelper
  def share_tracker_js(share_url)
    js = <<-EOS
    <script type="text/javascript">
        function shareEventHandler(evt){#{' '}
            if (evt.type == 'addthis.menu.share'){
                // set this variable equal to the campaign share URL being shared
                // setting this will be dependent on how you set up AddThis on your site
                var share_url = "#{share_url}";
                // ------------------------------------------------------------------
                var url_fragments = share_url.split("/");
                var short_code = url_fragments[url_fragments.length-1];
                var xmlhttp;
                if (window.XMLHttpRequest){
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp=new XMLHttpRequest();
                }else{
                    // code for IE6, IE5
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }#{'                                        '}

                xmlhttp.open("GET","https://getambassador.com/share_tracker/track_share?short_code="+short_code+"&social_name="+evt.data.service+"",true);
                xmlhttp.send();
            }
        }
        // Attach the listener to AddThis
        addthis.addEventListener('addthis.menu.share', shareEventHandler);
    </script>
    EOS
    js.html_safe
  end
end
