module BalanceHistoryHelper
  MANDATORY_HEADERS = [:date, :description]

  BALANCE_HISTORY_HEADERS = {
    admin_transaction_history: MANDATORY_HEADERS + [:debit, :credit, :balance],
    pending_transactions: MANDATORY_HEADERS + [:amount, :status, :action],
    transaction_history: MANDATORY_HEADERS + [:amount, :balance]
  }.freeze

  def fragment_caching_enabled?
    FeatureFlipper.show_feature?(:redis_fragment_caching, current_user)
  end

  def headers(table_key)
    BALANCE_HISTORY_HEADERS[table_key]
  end

  def get_txn_ids(txns)
    txns.map(&:id)
  end

  def formatted_header(h)
    custom_t("balance_histories.headers.#{h}")
  end

  def pending_payout_status
    custom_t("balance_histories.statuses.pending")
  end

  def table_title(key)
    custom_t("balance_histories.tables.#{key}.title")
  end

  def summary_title
    custom_t("balance_histories.summary.title")
  end

  def learn_more_text
    custom_t("balance_histories.tables.pending_transactions.learn_more")
  end

  def transactions_empty
    custom_t("balance_histories.tables.pending_transactions.no_txns")
  end

  def currency_header
    custom_t(
      "balance_histories.summary.currency_header",
      currency: current_user.country_website.currency
    )
  end

  def available_balance_title
    custom_t("balance_histories.summary.available_balance")
  end

  def pending_withdrawals_title
    custom_t("balance_histories.summary.pending_withdrawals")
  end

  def transaction_desc(person_transaction)
    if person_transaction.target_type == "PersonIntake"
      link_to(person_transaction.comment, "#")
    else
      person_transaction.comment
    end
  end

  def parse_money(amt)
    Money.new(amt * 100)
  end

  def net_value(txn)
    parse_money(txn.credit - txn.debit)
  end

  def parsed_balance(balance)
    format_to_currency(parse_money(balance))
  end

  def balance_change(transaction)
    previous_balance = parse_money(transaction.previous_balance)
    format_to_currency(previous_balance + net_value(transaction))
  end

  def current_transaction_value(transaction)
    format_parsed_transaction(net_value(transaction))
  end

  def pending_transaction_value(transaction)
    format_parsed_transaction(net_value(transaction).abs)
  end

  def format_parsed_transaction(txn_value)
    content_tag(
      :span,
      format_to_currency(txn_value),
      class: txn_value_class(txn_value)
    )
  end

  def txn_value_class(txn_value)
    txn_value.positive? ? "balance-history__value--positive" : "balance-history__value"
  end

  def to_date(datetime)
    fmt_slash_short_year(datetime)
  end

  def filter_options
    options = PersonTransaction.transaction_types_by_user(current_user)
    options.map do |_transaction_text, transaction_name, _|
      [
        transaction_name.underscore,
        custom_t("balance_histories.filter_options.#{transaction_name.underscore}")
      ]
    end
  end

  def parsed_transaction_value(value)
    format_to_currency(parse_money(value))
  end

  def not_selected_currency(selected_currency, currency)
    (selected_currency == currency) ? "" : "not-selected"
  end

  def transaction_currency?
    @selected_currency.present? && @selected_currency == @transaction_currency
  end

  def balance_history_money(money, options = {})
    return format_to_currency(money, options) if @multiple_currency_domain && transaction_currency?

    balance_to_currency(money, options)
  end

  def balance_history_currency(transaction_currency)
    return transaction_currency if @multiple_currency_domain.blank?

    @selected_currency
  end

  # Withholdings are always displayed in USD
  def balance_history_withholding(transaction)
    withholding = IRSTaxWithholding.find_by(withheld_from_person_transaction: transaction)
    return "N/A" if withholding.nil? || withholding.withheld_amount.zero?

    balance_history_money(withholding.withheld_amount.truncate(2).to_money("USD"))
  end
end
