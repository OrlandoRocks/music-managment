module IndiaPaymentsHelper
  include PaymentHelper

  def can_use_payments_os?(user = current_user)
    user.country_domain == "IN"
  end

  def invoice_has_pegged_rate?(invoice)
    invoice&.pegged_rate.present?
  end

  def decorate_transactions(transactions, decorator)
    return transactions if decorator.blank?

    case decorator.to_s
    when PaymentsOSTransactionDecorator.to_s
      transactions.map { |t| PaymentsOSTransactionDecorator.new(t) }
    when PayuTransactionDecorator.to_s
      transactions.map { |t| PayuTransactionDecorator.new(t) }
    end
  end
end
