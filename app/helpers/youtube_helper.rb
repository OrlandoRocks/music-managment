module YoutubeHelper
  def calc_youtube_price
    product = get_product_for_country
    get_product_price product
  end

  def get_product_price(product)
    return product.price.to_money(product.currency) unless current_user

    targeted_product = TargetedProduct.targeted_product_for_active_offer(current_user, product)
    if targeted_product
      targeted_product.adjust_price(product.price).to_money(product.currency)
    else
      product.price.to_money(product.currency)
    end
  end

  def get_product_for_country
    country = current_user ? current_user.country_domain : LOCALE_TO_COUNTRY_MAP[locale.to_s]
    Product.find(Product.find_products_for_country(country, :ytm))
  end

  def create_ytsr_track_takedown_note(song)
    Note.create(
      related: song,
      note_created_by: current_user,
      ip_address: request.remote_ip,
      subject: "Youtube Sound Recording Takedown".freeze,
      note: "YTSR track '#{song.name}' was taken down"
    )
  end

  private

  def takedown_track_monetizations(songs)
    track_monetizations = []
    failed_song_isrcs = []

    songs.map do |song|
      track_monetization = song.track_monetizations.find_by(store_id: Store::YTSR_STORE_ID)
      if track_monetization
        track_monetizations << track_monetization
      else
        failed_song_isrcs << song.isrc
      end
    end

    TrackMonetization::TakedownService.takedown(track_mons: track_monetizations, current_user: current_user)

    failed_song_isrcs
  end
end
