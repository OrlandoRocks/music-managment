module CartHelper
  def reauthorize_link(service, redirect = nil)
    {
      paypal: link_to("RE-#{t(:authoriz).upcase}E MY ACCOUNT", ec_step1_stored_paypal_accounts_path(pay_with_balance: true, redirect: redirect), class: "btn btn-callout btn-upcase btn-bold")
    }[service]
  end

  def show_cross_sell(type)
    return unless type == :ytm
    return if FeatureFlipper.show_feature?(:discovery_platforms, current_user)

    !current_user.has_active_ytm? &&
      (current_user.albums.distributed.not_taken_down.not_ringtones.count.positive? ||
        Purchase.albums_in_cart(current_user).count.positive?) &&
      !current_user.ytm_in_cart?
  end

  def pay_notice(useBalance, canPayWithBalance)
    if useBalance && current_user.balance.positive?
      if !useBalance || !canPayWithBalance
        custom_t("helper.carts.pay_with_balance_cc_and_paypal")
      else
        custom_t("helper.carts.pay_with_balance")
      end
    elsif !useBalance || !canPayWithBalance
      custom_t("helper.carts.pay_with_cc_and_paypal")
    end
  end

  def preferences
    current_user.person_preference
  end

  def pay_by_credit_card?
    preferences&.preferred_payment_type == "CreditCard"
  end

  def pay_by_paypal?
    preferences&.preferred_payment_type == "PayPal"
  end

  def check_paypal_default?
    current_user.preferred_paypal_account? && pay_by_paypal?
  end

  def check_cc_default?(cc)
    current_user.preferred_credit_card? &&
      (cc.id == preferences&.preferred_credit_card_id) &&
      pay_by_credit_card?
  end

  def non_native_currency_user?
    ForeignExchangePeggedRate.non_native_currency_site?(country_website_id: current_user.country_website_id)
  end

  def show_gst_breakdown?(invoice)
    invoice && @person.country_website.has_reseller?
  end

  def show_country_selector?
    return false unless FeatureFlipper.show_feature?(:bi_transfer, @person)

    !@person.address_locked?
  end

  def blank_address_fields?
    return false unless FeatureFlipper.show_feature?(:bi_transfer, @person)

    !@person.has_self_in_atleast_one_source?
  end

  def pay_now_enabled?(use_balance)
    (FeatureFlipper.show_feature?(:bi_transfer, @person) && !@person.required_address_info?) || self_billing_required?(use_balance)
  end

  def countries_for_checkout(person)
    self_identified_country_options =
      [[person.self_identified_country.first.name, person.self_identified_country.first.iso_code]]
    all_country_options = -> { Country.all.map { |c| [c.name, c.iso_code] } }
    non_self_identified_country_options =
      -> { person.countries_from_non_self_sources.map { |c| [c.name, c.iso_code] } }

    if person.countries_from_non_self_sources.blank?
      if person.self_identified_country.none?
        all_country_options.call
      else
        self_identified_country_options
      end
    else
      non_self_identified_country_options.call
    end
  end

  def display_country_select_prompt?(person)
    person.countries_from_non_self_sources.blank? ||
      person.countries_from_non_self_sources.exclude?(person.self_identified_country&.first)
  end

  # Only select the current country if there is more than one option.
  # On account settings page, select person's self identified country if present
  def selected_country_option(person, account_settings_tab = nil)
    person_iso = person.self_identified_country.first&.iso_code

    return person_iso if person.address_locked?
    return person_iso if account_settings_tab.present?
    return person_iso if countries_for_checkout(person).count > 1

    nil
  end

  def self_billing_required?(use_balance)
    vat_feature_flipper_enabled? && (use_balance.to_s == "true") && (self_billing_on_withdrawal? || self_billing_declined?)
  end

  def hide_vat_element?
    return "" if show_vat_info?

    "hide"
  end

  def can_user_pay?
    @paypal_account || @credit_cards.length.positive? || (current_user.balance.positive? && @finalize_form.use_balance) || current_user.allow_adyen?
  end

  def hide_choose_payment_method_text?
    @all_on_tunecore || @paypal_account || @credit_cards.length.positive? || (current_user.balance.positive? and @finalize_form.use_balance)
  end

  def use_user_balance?(finalize_form)
    finalize_form.use_balance
  end

  def pay_with_balance_text(finalize_form, can_pay_with_balance)
    use_balance = use_user_balance?(finalize_form)
    if use_balance && can_pay_with_balance
      custom_t("carts.confirm_and_pay.applied_part_to_order")
    elsif use_balance && !can_pay_with_balance
      custom_t("carts.confirm_and_pay.applied_to_order")
    elsif !use_balance && can_pay_with_balance
      custom_t("carts.confirm_and_pay.you_part_pay_order")
    else
      custom_t("carts.confirm_and_pay.you_pay_part_order")
    end
  end

  def amount_to_use_from_balance(cart_data, can_pay_with_balance, base_amount)
    unless can_pay_with_balance
      return money_to_currency(current_user.balance.to_money(cart_data.currency), round_value: false)
    end

    if vat_feature_flipper_enabled?
      money_to_currency(base_amount.to_money(cart_data.currency), round_value: false)
    else
      money_to_currency(cart_data.total_price.to_money(cart_data.currency), round_value: false)
    end
  end

  def use_balance_button_text_amount(cart_data)
    unless vat_feature_flipper_enabled?
      return money_to_currency(cart_data.total_price.to_money(cart_data.currency), round_value: false)
    end

    if balance_covers_base_amount?
      money_to_currency(current_user.balance.to_money(cart_data.currency), round_value: false)
    elsif current_user.balance >= cart_data.total_price
      money_to_currency(cart_data.total_price.to_money(cart_data.currency), round_value: false)
    end
  end

  def show_self_billing_popup?(finalize_form)
    vat_feature_flipper_enabled? && use_user_balance?(finalize_form)
  end

  def cart_grand_total(finalize_form, cart_data, amount_payable, can_pay_with_balance)
    unless use_user_balance?(finalize_form)
      return money_to_currency(cart_data.total_price.to_money(cart_data.currency), round_value: false)
    end
    if vat_feature_flipper_enabled?
      return money_to_currency(amount_payable.to_money(cart_data.currency), round_value: false)
    end

    if can_pay_with_balance
      money_to_currency(0.to_money(cart_data.currency), round_value: false)
    else
      money_to_currency((cart_data.total_price - current_user.balance).to_money(cart_data.currency), round_value: false)
    end
  end

  def show_freemium_upsell?(purchase)
    return false if current_user.has_plan? || purchase.paid?

    Product.free_release?(purchase.related, current_user) &&
      (FeatureFlipper.show_feature?(:freemium_flow, current_user) || FeatureFlipper.show_feature?(:discovery_platform, current_user))
  end

  def carted_plan_product_id
    @carted_plan_product_id ||=
      @purchases.find { |p| p.product_id.in? Product::PLAN_PRODUCT_IDS }.product_id
  end

  def carted_plan_id
    @carted_plan_id ||= PlansProduct.where(product_id: carted_plan_product_id).pick(:plan_id)
  end

  def carted_plan_image_path
    "/images/cart_icons/plan_#{carted_plan_id}.png"
  end

  def skip_confirm_and_pay?
    @skip_confirm_and_pay ||=
      @purchases&.all? do |purchase|
        purchase.free_with_plan? || purchase.is_free_automator_product?
      end
  end

  def show_appropriate_terms
    case
    when skip_confirm_and_pay? && fb_freemium_purchase?(@purchases)
      custom_t(
        "cart.show.terms_conditions_skip_confirm_with_facebook",
        {
          cta: custom_t("carts.show.skip_confirm_and_pay"),
          fb_link: "https://www.tunecore.com/terms#facebook",
          link: tunecore_terms_link
        }
      )
    when skip_confirm_and_pay?
      custom_t(
        "carts.show.terms_conditions_skip_confirm",
        { cta: custom_t("carts.show.skip_confirm_and_pay"), link: tunecore_terms_link }
      )
    when fb_freemium_purchase?(@purchases)
      custom_t(
        "cart.show.terms_conditions_facebook",
        { fb_link: "https://www.tunecore.com/terms#facebook", link: tunecore_terms_link }
      )
    else
      custom_t("carts.show.terms_conditions", link: tunecore_terms_link)
    end
  end

  def release_country_restrictions(release)
    return custom_t(".carts.cart_item.none") if release.album_countries.blank?

    release.album_countries.map { |album_country| album_country.country.name }.to_sentence
  end

  def purchased_item_artists(release)
    release.artists.map(&:name).to_sentence
  end

  def pending_plan_upgrade?(purchase)
    purchase.unpaid? && purchase.plan_upgrade?
  end

  def show_plan_addon_card?(purchase)
    return false if purchase.paid?

    purchase.plan_upgrade? && purchase.discount_cents_positive?
  end

  def show_proration_card?(purchase)
    purchase.plan_upgrade? &&
      purchase.discount_cents_positive? &&
      purchase.proration_discounted? &&
      !purchase.pre_plan_proration?
  end

  def dolby_atmos_track_header(attachment)
    track_num = attachment.related.song.track_num
    song_name = attachment.related.song.name
    "<strong>#{custom_t('.track')} #{track_num}:</strong> #{song_name}".html_safe
  end
end
