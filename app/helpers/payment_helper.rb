module PaymentHelper
  include Paypal::Helpers
  include RegionHelper
  ONE_TIME_PAYMENT = "one_time_payment_only".freeze
  def one_time_payment_only?(person = current_user)
    available_in_region?(
      person.country_domain,
      ONE_TIME_PAYMENT
    ) && FeatureFlipper.show_feature?(:rbi_enabled, person)
  end
end
