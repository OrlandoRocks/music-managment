# frozen_string_literal: true

module DiscographyHelper
  VIEW_TYPES = %w[grid list table].map(&:freeze).freeze

  def class_for_status(album)
    case album.state
    when "admin lock"  then "album_status_locked"
    when "in invoice"  then "album_status_locked"
    when "processing"  then "album_status_locked"
    when "renewal due" then "album_status_locked"
    when "taken donw"  then "album_status_in_progress"
    when "live"        then "album_status_live"
    when "sent"        then "album_status_finalized"
    when "ready to go" then "album_status_ready_for_payment"
    else
      "album_status_in_progress"
    end
  end

  def annual_renewal_display_date(album)
    if album.renewal.nil?
      ""
    else
      expires = album.renewal.expires_at.to_datetime

      if expires > Time.now + 50.years
        "Lifetime"
      else
        fmt_long_date(album.renewal.expires_at.to_date)
      end
    end
  end

  def salepoints_to_purchase(album)
    album.salepoints.includes(:inventory_usages, :store).select { |sp| !sp.payment_applied? }
  end

  def salepoints_purchase_price(album)
    Product.price(current_user, salepoints_to_purchase(album))
  end

  def all_salepoints_selected?(album)
    (album.available_stores.count do |store|
      /^iTunes/ !~ store.name
    end + 1 - album.selected_stores.length).positive?
  end

  def can_reactivate_renewal?(album)
    album.renewal.try(:canceled_at).present? && album.takedown_at.nil? && album.status != "rejected"
  end

  def merge_qs(hash)
    permitted_discography_params = params.permit(
      :commit,
      :keyword,
      :release_type,
      :sort_by,
      :status,
      :view_type
    )

    raw(permitted_discography_params.merge(hash).to_query)
  end

  def set_view_type
    @view_type = handle_mobile_device ||
                 parse_view_type ||
                 apply_view_type_defaults
    ensure_valid_type
    cookies[:view_type] = @view_type
  end

  private

  def handle_mobile_device
    return "table" if browser.mobile?
  end

  def parse_view_type
    params[:view_type] || cookies[:view_type]
  end

  def apply_view_type_defaults
    return "table" if current_user.albums.not_deleted.count > 250

    "list"
  end

  def ensure_valid_type
    @view_type = "list" unless VIEW_TYPES.include? @view_type
  end
end
