module CustomTranslationHelper
  def custom_t(key, options = {}, is_html = true)
    is_html ? t(key, options).html_safe : t(key, options)
  end

  def gon_main_artist_text
    gon_custom_t("main_artists", :main_artists)
    gon_custom_t("main_artist", :main_artist)
  end

  def gon_datepicker_setup
    gon_custom_t("suggested_date", "controllers.album.suggested_date")
    gon_custom_t("locale_code", "controllers.album.locale_code")
    gon_custom_t("long_date_fmt", "date.formats.long_date_picker", {}, false)
  end

  def gon_custom_t(gon_prop_name, translation_key, options = {}, is_html = true)
    gon.push({ gon_prop_name.to_sym => custom_t(translation_key, options, is_html) })
  end

  def translate_dates
    return unless params.include?(:translated_date_fields)

    params[:translated_date_fields].each do |field, date_format|
      params[controller_name.singularize][field] =
        Tunecore::DateTranslator.translate(
          params[controller_name.singularize][field],
          date_format,
          :en,
          I18n.locale
        )
    end
  end
end
