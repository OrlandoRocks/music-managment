module MassAdjustmentsHelper
  def can_approve_mass_adjustment?
    current_user.has_role?("Mass Adjustments Approver")
  end

  def scheduler_options
    [
      ["Allow All Corrections", true],
      ["Allow Only Positive Corrections", false]
    ]
  end

  def approver_options
    [
      ["Approve", MassAdjustmentBatch::APPROVED],
      ["Decline", MassAdjustmentBatch::DECLINED]
    ]
  end

  def batch_status(batch)
    batch.processing_completed? ? "Processing Complete" : batch.status.capitalize
  end

  def processed_status(batch)
    entries = batch.mass_adjustment_entries

    return if entries.blank?

    processed_negative_entries = entries.successful_entries.where(balance_turns_negative: true).count
    processed_positive_entries = entries.successful_entries.where(balance_turns_negative: false).count
    cancelled_entries = entries.cancelled_entries.count
    unprocessed_entries = batch.unprocessed_mass_adjustment_entries.count

    "<div>Total entries: <b>#{entries.count}</b></div>
     <div>Positive entries processed: <b>#{processed_positive_entries}</b></div>
     <div>Negative entries processed: <b>#{processed_negative_entries}</b></div>
     <div>Cancelled Entries: <b>#{cancelled_entries}</b></div>
     <div>Entries left to process: <b>#{unprocessed_entries}</b></div>".html_safe
  end
end
