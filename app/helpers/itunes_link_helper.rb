module ItunesLinkHelper
  def itunes_link_popup(album)
    return unless album.live_in_itunes? && album.class.name != "Ringtone"

    "<div class=\"itunes_preview\"><a class=\"lightbox\" href=\"#{itunes_link_path(album)}\" target=\"_blank\">Copy your iTunes link</a></div>".html_safe
  end
end
