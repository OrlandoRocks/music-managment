module AdyenSecretsHelper
  def config_for_corporate_entity(corporate_entity_id)
    AdyenMerchantConfig.find_by(corporate_entity_id: corporate_entity_id)
  end

  def config_for_merchant_account(merchant_account)
    AdyenMerchantConfig.find_by(merchant_account: merchant_account)
  end
end
