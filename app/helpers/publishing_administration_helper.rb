module PublishingAdministrationHelper
  include RegionHelper

  def redirect_to_dashboard
    redirect_to dashboard_path
  end

  def publishing_available_in_region?
    available_in_region?(country_website, "publishing")
  end

  def rights_app_enabled?(_person = current_user)
    true
  end

  def ntc_bandaid_url
    base_url = ENV["NTC_FORM_APP_HOST"]
    salt = ENV["NTC_FORM_APP_SALT"]
    path = "/#{current_user.id}/song_writers"
    token = Digest::SHA256.hexdigest("#{salt}--#{path}")
    "#{base_url}#{path}?token=#{token}"
  end

  def redirect_to_publishing_page
    if current_user_has_composers?
      redirect_to publishing_administration_composers_path
    else
      redirect_to new_publishing_administration_enrollment_path
    end
  end

  def redirect_to_legacy_composers_index_page
    redirect_to composers_path
  end

  def redirect_to_legacy_composers_new_page
    redirect_to new_composer_path
  end

  def pro_name(pro_id)
    PerformingRightsOrganization.where(id: pro_id).first.try(:name)
  end

  def composer_state(state)
    {
      pending: content_tag(:em, custom_t("publishing.pending")),
      active: content_tag(:em, custom_t("publishing.active")),
      terminated: content_tag(:em, custom_t("publishing.terminated"), class: "error")
    }[state]
  end

  def pro_for(composer)
    if composer.needs_pro?
      content_tag(:em, custom_t("publishing.missing"), class: "error")
    else
      composer.pro_affiliation
    end
  end

  def cae_number_for(composer)
    if composer.has_cae_number?
      composer.cae_number
    elsif composer.needs_cae_number?
      content_tag(:em, custom_t("publishing.missing"), class: "error")
    end
  end

  def sanitize_name(name)
    return "" if name.nil?

    regex = %r/[%\\;<=>?\/\[\]\^`{}\|~,]/
    I18n.transliterate(name.tr("$", "S").tr("\u2019", "'")).gsub(regex, "")
  end

  def eligible_publishing_pro?(composer_pro_id)
    @eligible_publishing_pro ||= PerformingRightsOrganization.eligible_publishing_pro_ids.include?(composer_pro_id.to_i)
  end

  def has_publisher_errors?(composer)
    @has_publisher_errors ||= composer.errors[:publisher_name].present? || composer.errors[:publisher_cae].present?
  end

  def send_publisher?(composer)
    composer.has_publisher? &&
      composer.publisher.cae.present? &&
      eligible_publishing_pro?(composer.pro.try(:id))
  end

  def cae_start_with_55_valid?
    if composer_pro_id.present? && composer_cae.present? && composer_cae.to_s.start_with?("55")
      errors.add(:composer_cae, I18n.t("publishing.cae_55_error"))
    elsif needs_publisher_cae? && publisher_cae.present? && publisher_cae.to_s.start_with?("55")
      errors.add(:publisher_cae, I18n.t("publishing.cae_55_error"))
    end
  end

  def sentric_writer_params(writer)
    if writer.try(:is_a_primary_composer?)
      {
        FirstName: sanitize_name(writer.first_name),
        MiddleName: writer.middle_name.present? ? sanitize_name(writer.middle_name) : nil,
        LastName: sanitize_name(writer.last_name),
        ArtistAccountCode: writer.account.provider_account_id,
        SocietyId: writer.pro.present? ? writer.pro.provider_identifier : nil,
        CaeipiNumber: writer.pro.present? ? writer.cae : nil,
        PublisherName: send_publisher?(writer) ? sanitize_name(writer.publisher.name) : nil,
        PublisherCaeipi: send_publisher?(writer) ? writer.publisher.cae : nil
      }
    elsif writer.try(:is_a_cowriting_composer?) && writer.is_a?(Cowriter)
      {
        FirstName: sanitize_name(writer.first_name),
        LastName: sanitize_name(writer.last_name),
        UnknownWriter: writer.unknown?,
        ArtistAccountCode: writer.composer.account.provider_account_id
      }
    elsif writer.try(:is_a_cowriting_composer?) && writer.is_a?(PublishingComposer)
      {
        FirstName: sanitize_name(writer.first_name),
        LastName: sanitize_name(writer.last_name),
        UnknownWriter: writer.unknown?,
        ArtistAccountCode: writer.account.provider_account_id
      }
    end
  end

  def name_affixes_translations(affix)
    [[I18n.t("publishing.none"), nil]].concat(I18n.t("publishing.name_#{affix}").map { |_k, v| [v, v] })
  end

  def disable_field_for_non_admins(form, attr)
    form.send(attr).present? && !current_user.is_administrator? && current_user.purchased_pub_admin?
  end

  def current_user_has_composers?
    rights_app_enabled? ? (managed_composer? || managing_composer?) : managed_composer?
  end

  def managed_composer?
    current_user.publishing_composers.any?
  end

  def managing_composer?
    current_user.account && current_user.account.publishing_composers.any?
  end

  def set_enrollment_form(composer_id)
    composer_params = PublishingAdministration::PublishingComposerParamsService.compose(composer_id)
    PublishingAdministration::PublishingEnrollmentForm.new(composer_params)
  end

  def composer_is_account_composer?(composer)
    composer.account_composer&.person_id == composer.person_id
  end

  def performing_rights_organization_collection
    PerformingRightsOrganization.all.order(:name).map { |pro| [pro.name, pro.id] }
                                .unshift([custom_t("publishing.none"), nil])
  end

  def ntc_composition_form_enabled?(composer)
    composer.publishing_administrator.id == current_user.id
  end

  def edit_composition_feature_enabled?(composer)
    composer_presenter = ComposerPresenter.new(composer)
    FeatureFlipper.show_feature?(:edit_compositions, composer.publishing_administrator) &&
      composer.publishing_administrator.id == current_user.id &&
      composer_presenter.active?
  end

  def bulk_upload_compositions_feature_enabled?
    FeatureFlipper.show_feature?(:bulk_upload_compositions, current_user.id)
  end

  def public_domain_feature_enabled?
    FeatureFlipper.show_feature?(:public_domain, current_user.id)
  end

  def has_draft_compositions?(account_id)
    PublishingComposition.where(account_id: account_id, state: "draft").any?
  end

  def dob_required_feature_enabled?
    FeatureFlipper.show_feature?(:publishing_dob_required, current_user)
  end

  def display_composition_manager_link?
    current_user.account.publishing_composers.active.count.one?
  end

  def admin_can_edit?
    under_admin_control? || current_user.is_administrator?
  end

  def tax_info_agreed_to_date(tax_info)
    tax_info.agreed_to_w9_at || tax_info.agreed_to_w8ben_at
  end

  def publishing_sync_opt_in_enabled?
    FeatureFlipper.show_feature?(:publishing_sync_opt_in, current_user)
  end

  def publishing_account_has_approved_album?(account_composer)
    Album.where(person_id: account_composer.person_id).where(legal_review_state: "APPROVED").count.positive?
  end

  def composer_index_split_shares_modal_enabled?
    FeatureFlipper.show_feature?(:composer_index_split_shares_modal, current_user)
  end
end
