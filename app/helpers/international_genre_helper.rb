module InternationalGenreHelper
  PRIMARY_GENRE         = "primary_genre".freeze
  PRIMARY_GENRE_ID      = "primary_genre_id".freeze
  PRIMARY_SUBGENRE_ID   = "sub_genre_id_primary".freeze
  SECONDARY_GENRE       = "secondary_genre".freeze
  SECONDARY_GENRE_ID    = "secondary_genre_id".freeze
  SECONDARY_SUBGENRE_ID = "sub_genre_id_secondary".freeze
  HIDE_CLASS            = "hide".freeze

  def release_primary_genre(release)
    @primary_genre = parse_genre(
      field: PRIMARY_GENRE_ID,
      genre_type: PRIMARY_GENRE,
      is_primary_genre: true,
      release: release
    )
  end

  def release_secondary_genre(release)
    @secondary_genre = parse_genre(
      field: SECONDARY_GENRE_ID,
      genre_type: SECONDARY_GENRE,
      is_primary_genre: false,
      release: release
    )
  end

  def release_primary_subgenre(release)
    return unless release.primary_genre&.india_subgenre?

    @primary_subgenre ||= field_value(PRIMARY_SUBGENRE_ID) || release.primary_genre
  end

  def release_secondary_subgenre(release)
    return unless release.secondary_genre&.india_subgenre?

    @secondary_subgenre ||= field_value(SECONDARY_SUBGENRE_ID) || release.secondary_genre
  end

  def primary_subgenres_display_class(release)
    HIDE_CLASS unless show_subgenre?(
      genre_type: PRIMARY_GENRE,
      field: PRIMARY_GENRE_ID,
      is_primary_genre: true,
      release: release
    )
  end

  def secondary_subgenres_display_class(release)
    HIDE_CLASS unless show_subgenre?(
      genre_type: SECONDARY_GENRE,
      field: SECONDARY_GENRE_ID,
      is_primary_genre: false,
      release: release
    )
  end

  private

  def parse_genre(field:, genre_type:, is_primary_genre:, release:)
    india_genre_value(field, genre_type, is_primary_genre, release) ||
      field_value(field) ||
      instance_variable_get("@#{genre_type}") ||
      release_genre(genre_type, release)
  end

  def field_value(field)
    genre_id = params.dig(:album, field)
    Genre.find(genre_id) if genre_id.present?
  end

  def india_genre_value(field, genre_type, is_primary_genre, release)
    return unless india? && (release.public_send(genre_type)&.india_subgenre? ||
                             india_initial_selection?(field, is_primary_genre, release)
                            )

    Genre.india_parent_genre
  end

  def india?
    return true
    (india_user? || current_user.is_administrator?)
  end

  def new_record?(release)
    release.new_record?
  end

  # get primary/secondary genre from the release
  def release_genre(genre_type, release)
    release.public_send(genre_type)
  end

  def india_genre?(genre_type, field, release)
    field_value(field)&.india_parent_genre? ||
      release.public_send(genre_type)&.india_subgenre? ||
      release.public_send(genre_type)&.india_parent_genre?
  end

  def india_initial_selection?(field, is_primary_genre, release)
    india? && new_record?(release) && is_primary_genre && field_value(field).blank?
  end

  def show_subgenre?(genre_type:, field:, is_primary_genre:, release:)
    india_genre?(genre_type, field, release) ||
      india_initial_selection?(field, is_primary_genre, release)
  end
end
