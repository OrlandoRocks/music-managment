module StoredBankAccountsHelper
  # produces the hidden fields for braintree post. This replaces the
  # payment_services_for helper provided by the ActiveMerchant Library
  def braintree_fields(order, account, options = {})
    integration_module = ActiveMerchant::Billing::Integrations.const_get(:braintree.to_s.classify)
    service_class = integration_module.const_get("Helper")
    service = service_class.new(order, account, options)
    yield service
    result = "\n"
    result << service.form_fields.collect do |field, value|
      hidden_field_tag(field, value)
    end.join("\n")
    concat(result)
  end

  def stored_bank_account_field_header(text, param)
    if stored_bank_search_params[:search].nil? || stored_bank_search_params.dig(:search, :order) == "#{param} DESC"
      next_sort_order = "#{param} ASC"
    else
      next_sort_order = "#{param} DESC"
    end

    link_params = stored_bank_search_params.clone
    link_params[:search] = stored_bank_search_params[:search].clone if stored_bank_search_params[:search].present?
    link_params[:search] ||= {}
    link_params[:search][:order] = next_sort_order
    link_params[:page] = params[:page]
    link_params[:action] = controller.action_name

    link_to(text, link_params, class: "sort-link #{stored_bank_search_sort_class_helper(param)}")
  end

  def stored_bank_search_sort_class_helper(param)
    search_order = stored_bank_search_params.dig(:search, :order)

    return if search_order.nil?

    return "ascending" if search_order == "#{param} ASC"
    return "descending" if search_order == "#{param} DESC"
  end

  def stored_bank_search_params
    params.permit(
      :per_page,
      :commit,
      search: [
        :person_status,
        :person_id,
        :person_email,
        :person_name,
        :archived,
        :id,
        :created_at_min,
        :created_at_max,
        :order
      ]
    )
  end
end
