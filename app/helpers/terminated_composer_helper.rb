module TerminatedComposerHelper
  def existing_partially_terminated_composer(composer_id)
    TerminatedComposer.where(composer_id: composer_id, termination_type: "partially").first
  end

  def flash_success_notice
    flash[:notice] = "Composer has been fully Terminated for Publishing Administration"
  end

  def termination_params(raw_params)
    {
      composer_id: raw_params[:terminated_composer][:composer_id],
      effective_date: raw_params[:terminated_composer][:effective_date],
      admin_note: raw_params[:terminated_composer][:admin_note],
      publishing_termination_reason_id: raw_params[:terminated_composer][:publishing_termination_reason_id]
    }
  end
end
