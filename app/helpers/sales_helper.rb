# frozen_string_literal: true

module SalesHelper
  REPORT_LINK_ACTION = {
    releases: "release_report",
    songs_display: "song_report",
    stores: "store_report",
    countries: "country_report"
  }

  def tencent_mucoin_royalty?(store_id)
    store_id == Store::TENCENT_MUCOIN_STORE_ID
  end

  def sip_tencent_mucoin_royalty?(store_id)
    store_id == SipStore::TENCENT_MUCOIN_SIP_STORE_ID
  end

  def tiktok_royalty?(store_id)
    store_id == Store::TIK_TOK_STORE_ID
  end

  def sip_tiktok_royalty?(store_id)
    store_id == SipStore::TIK_TOK_SIP_STORE_ID
  end

  def report_link_copy(report)
    unless current_user.feature_gating_can_do?(:premium_sales_report)
      return "#{custom_t(report)} #{plan_lock_icon(report)}".html_safe
    end

    custom_t(report)
  end

  def plan_lock_icon(report)
    return image_tag "plans/WhiteLockIcon.png", class: "lock-img" if params[:action] == REPORT_LINK_ACTION[report]

    image_tag "plans/LockIcon.png", class: "lock-img"
  end

  def report_link(report)
    link_to report_link_copy(report), action: REPORT_LINK_ACTION[report]
  end
end
