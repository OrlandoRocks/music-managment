module PersonPreferenceHelper
  def allow_multiple_payment_options?(person)
    [
      person.allow_paypal?,
      allow_braintree_credit_cards?(person),
      person.allow_adyen?
    ].count(true) > 1
  end

  def show_adyen_preference_section?(person)
    person.allow_adyen? &&
      person.adyen_renewals_feature_on? &&
      person.prefered_adyen_payment_method.present?
  end

  def payment_preference_adyen_radio_button_label
    custom_t(".pay_with") + " " + custom_t("adyen.name")
  end
end
