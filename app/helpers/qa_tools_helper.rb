module QaToolsHelper
  def qa_or_dev_environment?
    Rails.env.development? || Rails.env.staging?
  end
end
