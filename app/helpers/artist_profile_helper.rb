module ArtistProfileHelper
  def display_song_name(song)
    song_name = song.name.truncate(75)

    if song.album.is_various? && song.artist.try(:name)
      song_name << content_tag(:span, " by #{song.artist.name}", class: "various")
    end

    song_name.html_safe
  end
end
