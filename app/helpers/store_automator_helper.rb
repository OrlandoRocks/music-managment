module StoreAutomatorHelper
  include ApplicationHelper
  def automator_price_with_discount(album, person = current_user)
    subscription = album.salepoint_subscription || SalepointSubscription.new(album_id: album.id)
    product = Product.find_ad_hoc_product(subscription, person)

    return 0.00.to_money(person.currency) if product.nil?

    Product.set_targeted_product_and_price(person, product)

    money_with_discount(product.original_price, product.adjusted_price, product.currency)
  end

  def show_automator_addon_if_finalized?(album, automator)
    album.supports_subscriptions &&
      album.finalized? &&
      !album.rejected? &&
      !(automator && automator.finalized?) &&
      album.person.can_do?(:store_automator)
  end

  def get_automator_product(person)
    p = Product.default_products_for(SalepointSubscription.new, person.country_website_id)
    p.first.current_targeted_product = TargetedProduct.targeted_product_for_active_offer(person, p.first)
    p
  end

  def automator_disabled(album)
    album.salepoint_subscription && album.salepoint_subscription.finalized?
  end

  def show_addon_header?(album)
    show_automator_addon?(album) || album.dolby_atmos_audios?
  end

  def salepoints_redesign_feature_enabled?
    FeatureFlipper.show_feature?(:salepoints_redesign, current_user)
  end

  def show_automator_addon?(album)
    !salepoints_redesign_feature_enabled? &&
      album.supports_subscriptions &&
      !album.rejected? &&
      !store_automator_disabled(album) &&
      album.person.can_do?(:store_automator)
  end

  # is this used anywhere?
  def check_store_automator?(album)
    album.salepoint_subscription.present?
  end

  def purchased_album_automator_eligible?(album, automator, current_user)
    current_user.can_do?(:store_automator) &&
      feature_enabled?(:store_automator) &&
      album.supports_subscriptions &&
      album.finalized? &&
      (automator.nil? || automator.finalized? == false)
  end
end
