module SpecializedReleasesHelper
  include ApplicationHelper

  def album_preorder_disabled(album = nil)
    return unless specialized_releases_enabled?

    @album_preorder_disabled ||= album_is_specialized_release(album)
  end

  def store_automator_disabled(album = nil)
    return unless specialized_releases_enabled?

    @store_automator_disabled ||= album_is_specialized_release(album)
  end

  def amazon_on_demand_disabled(_album = nil)
    return unless specialized_releases_enabled?

    @amazon_on_demand_disabled
  end

  def get_polymorphic_edit_path(album)
    if specialized_releases_enabled? && album_is_specialized_release(album)
      "/#{specialized_route_segment}/#{album.album_type.downcase}s/#{album.id}/edit"
    else
      edit_polymorphic_path(album)
    end
  end

  def album_is_specialized_release(album)
    return unless specialized_releases_enabled?

    album.strictly_facebook_release? || album.strictly_discovery_release?(current_user)
  end

  def specialized_releases_enabled?
    discovery_platforms_enabled? || facebook_freemium_enabled?
  end

  def specialized_route_segment
    discovery_platforms_enabled? ? "discovery_platforms" : "facebook"
  end

  def facebook_freemium_enabled?
    FeatureFlipper.show_feature?(:freemium_flow, current_user)
  end

  def discovery_platforms_enabled?
    FeatureFlipper.show_feature?(:discovery_platforms, current_user)
  end
end
