module Sync::SearchHelper
  def convert_duration(duration)
    if duration.blank?
      ""
    else
      duration /= 1000
      hours, remaining = duration.divmod(1.hour)
      minutes, seconds = remaining.divmod(1.minute)

      if hours.positive?
        format("%2d:%02d:%02d", hours, minutes, seconds)
      else
        format("%2d:%02d", minutes, seconds)
      end

    end
  end

  #
  # This makes all artwork urls for sync point to the production bucket
  #
  # It was decided that we are always looking at a production replica on
  # staging in development so we want to always point to the production
  # artwork bucket
  #
  def sync_artwork_url(url)
    "//#{PRODUCTION_SCALED_ARTWORK_BUCKET}#{url.match(/\/artwork.*/)}"
  end
end
