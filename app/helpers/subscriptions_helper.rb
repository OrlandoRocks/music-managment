module SubscriptionsHelper
  def subscription_status(subscription)
    !!subscription.canceled_at ? "Inactive" : "Active"
  end

  def subscription_cost(subscription, _amount)
    interval_text =
      if subscription.renewal_duration == 1
        " once a #{subscription.renewal_interval}"
      else
        " every #{subscription.renewal_duration} #{subscription.renewal_interval}s"
      end

    money_to_currency(subscription.price_to_renew_in_money) + interval_text
  end

  def renewal_status(release)
    release_type = release.class.to_s.downcase
    if release.days_til_expiration.negative?
      return "Your #{release_type} was due for renewal, to make sure your music stays live in stores, please #{link_to 'pay now', buy_subscriptions_path(id: release.renewal, product_id: 'n')}."
    end
    if current_user.person_preference && current_user.person_preference.do_not_autorenew?
      return "Your #{release_type} is due for renewal in #{release.days_til_expiration} days. Auto renewal is disabled, to make sure that your music stays live please #{link_to 'Pay Now', buy_subscriptions_path(id: release.renewal, product_id: 'n')} or #{link_to('Enable Auto Renewal', person_preferences_path)}"
    end

    subscription_link = '<span class="alert_link">' + link_to("Payment Preferences", stored_credit_cards_path) + "</span>"
    if current_user.stored_credit_cards.active.present? && current_user.stored_credit_cards.active.all?(&:expired?)
      return "Your credit card is expired. #{subscription_link}"
    end

    credit_card = current_user.stored_credit_cards.active.not_expired.first
    if current_user.person_preference && current_user.person_preference.pay_with_balance?
      if credit_card
        text = "This #{release_type} will automatically renew in #{release.days_til_expiration} days for #{money_to_currency(release.renewal.price_to_renew_in_money)} (#{release.renewal.renewal_interval}ly plan) using your Account Balance or your #{credit_card.cc_type} Card ending in *#{credit_card.last_four}."
      else
        text = "This #{release_type} is due for renewal in #{release.days_til_expiration} days for #{money_to_currency(release.renewal.price_to_renew_in_money)} (#{release.renewal.renewal_interval}ly plan). We will charge your Account Balance, but if it does not cover the full amount, your #{release_type} will be scheduled for removal from all stores. To keep your #{release_type} live in stores, please add a credit card or Paypal account for this subscription."
      end
    elsif credit_card
      text = "This #{release_type} will automatically renew in #{release.days_til_expiration} days for #{money_to_currency(release.renewal.price_to_renew_in_money)} (yearly plan) using your #{credit_card.cc_type} Card ending in *#{credit_card.last_four}."
    else
      text = "Please add a valid payment method to keep this #{release_type} live in stores."
    end
    text << subscription_link
  end

  def display_renewal?(release)
    (release.days_til_expiration.nil? || release.days_til_expiration > 28 || release.renewal.canceled_at?) ? false : true
  end
end
