# frozen_string_literal: true

module RoyaltySplitHelper
  extend ActionView::Helpers::NumberHelper

  def format_split_percent(percent)
    number_to_percentage(percent, { precision: 1, strip_insignificant_zeros: false, locale: bcp47_locale })
  end

  def album_royalty_splits_path(release)
    albums_royalty_splits_path(params: { album_ids: [release.id] })
  end

  def get_albums_royalty_splits_path(releases)
    albums_royalty_splits_path(params: { album_ids: releases.map(&:id) })
  end

  def release_can_be_split?(release)
    # NOTE: This does NOT use current_user.can_create_splits?, because the
    # "split release" option may be displayed, yet the actual link disabled with a pop-up.
    ["live", "sent"].include?(release.status) && current_user.show_splits_based_on_feature_flag?
  end

  def recipient_name_or_email(recipient)
    recipient.person? ? recipient.person.first_initial_and_last_name : recipient.email
  end

  def release_split_description(release:, split:)
    album_title = release.name.truncate(RoyaltySplit::ALBUM_TITLE_MAX_SIZE)
    album_songs = split.songs_on_album(release)
    number_of_songs_on_album = album_songs.size

    raise "Not an album for this split" if number_of_songs_on_album < 1

    # If every song is used on the album, don't list track names
    return album_title if (number_of_songs_on_album == release.songs.size)

    album_song_titles = album_songs.map { |song| song.name.truncate(RoyaltySplit::SONG_TITLE_MAX_SIZE) }.join(", ")

    if number_of_songs_on_album <= RoyaltySplit::DISPLAY_MAX_SONGS_ON_RELEASE
      "#{album_title} (#{album_song_titles})"
    else
      "#{album_title} (#{number_of_songs_on_album} #{I18n.t('royalty_splits.song').pluralize})"
    end
  end

  def recipient_accepted_status(recipient)
    recipient.accepted_at? ? I18n.t("royalty_splits.accepted").downcase : I18n.t("royalty_splits.pending").downcase
  end

  def split_description(split)
    return I18n.t("royalty_splits.no_songs") if split.songs.blank?

    albums_description = split.top_albums
                              .includes(:songs)
                              .map { |release| release_split_description(release: release, split: split) }
                              .join(", ")
    return albums_description if split.album_count <= RoyaltySplit::DISPLAY_MAX_RELEASES

    remaining_releases = split.album_count - RoyaltySplit::DISPLAY_MAX_RELEASES
    sprintf(
      "%s %s %s",
      albums_description,
      I18n.t("royalty_splits.and_X_more", number: remaining_releases),
      I18n.t("royalty_splits.release").pluralize(remaining_releases),
    )
  end

  def splits_link_cannot_create_disabled_classes
    return "" if current_user.can_create_splits?
    return "splits-access-suspended-splits-disabled" if current_user.flagged_suspicious?

    "splits-access-create-splits-disabled"
  end

  def show_invited_splits_menu_link?
    return false unless current_user.show_invited_splits_based_on_feature_flag?

    current_user.can_accept_splits? || current_user.has_split_invites?
  end

  def show_created_splits_menu_link?
    current_user.can_create_splits?
  end

  def email_link(text, path)
    "<a clicktracking=off href=\"#{path}\" target=\"_blank\">#{h text}</a>".html_safe
  end
end
