module CountryDomainHelper
  LANGUAGE_COOKIE_KEY    = "tc-lang".freeze
  USER_SELECT_COOKIE_KEY = "tc-userselect".freeze
  AUTO_SELECT_COOKIE_KEY = "tc-autoselect".freeze

  def country_website
    return current_user.country_domain if current_user && !current_user.is_administrator

    if params[:country_locale]
      session[:country_website] = params[:country_locale].upcase
    elsif session[:country_website]
      session[:country_website]
    elsif country_code = COUNTRY_URLS.detect { |_k, v| v == request.host }
      country_code[0]
    else
      "US"
    end
  end

  def country_specific_domain
    {
      "US" => CountryWebsite::US_DOMAIN,
      "CA" => CountryWebsite::CA_DOMAIN,
      "UK" => CountryWebsite::UK_DOMAIN,
      "AU" => CountryWebsite::AU_DOMAIN,
      "DE" => CountryWebsite::DE_DOMAIN,
      "FR" => CountryWebsite::FR_DOMAIN,
      "IT" => CountryWebsite::IT_DOMAIN,
      "IN" => CountryWebsite::IN_DOMAIN,
    }[country_website]
  end

  def ensure_correct_country_domain(redirect_path = nil)
    if accessing_via_backend_servers?
      if redirect_path
        redirect_to redirect_path
      else
        true
      end
    elsif correct_user_domain?
      if redirect_path
        redirect_to redirect_path
      else
        true
      end
    else
      redirect_to_country_domain(redirect_path || request.fullpath)
    end
  end

  def redirect_to_country_domain(redirect_path)
    domain = current_user.domain
    if [80, 443].include?(request.port)
      redirect_to "#{request.protocol}#{domain}#{redirect_path}", method: request.method
    else
      redirect_to "#{request.protocol}#{domain}:#{request.port}#{redirect_path}", method: request.method
    end
  end

  def india_user?
    country_website == "IN"
  end

  def needs_gst?
    current_user.india_country_and_domain?
  end

  def parent_genres_for_country_user
    results = Genre.global.active

    return results.or(Genre.where(name: "Indian")).by_name if current_user.is_administrator?

    case country_website
    when "IN"
      results = results.or(Genre.where(name: "Indian"))
    end

    results.by_name
  end

  def set_language_cookie(value)
    cookies.permanent[LANGUAGE_COOKIE_KEY] = {
      value: value,
      domain: COOKIE_DOMAINS[country_website],
      same_site: "None",
      secure: true,
    }
  end

  def get_language_cookie
    cookies[LANGUAGE_COOKIE_KEY]
  end

  def get_user_select_cookie
    cookies[USER_SELECT_COOKIE_KEY]
  end

  def get_auto_select_cookie
    cookies[AUTO_SELECT_COOKIE_KEY]
  end

  def compute_user_language
    lang_cookie = get_language_cookie

    return if lang_cookie.blank?

    cookie_locale = USA_LOCALE
    cookie_locale = lang_cookie if I18n.available_locales.include?(lang_cookie.to_sym)

    if current_user
      session[:locale] = I18n.locale = lang_cookie
      save_if_changed(lang_cookie)
    else
      I18n.locale = lang_cookie
    end
  end

  def save_if_changed(lang_cookie)
    user_select_cookie = get_user_select_cookie
    auto_select_cookie = get_auto_select_cookie

    return unless user_select_cookie.present? || auto_select_cookie.present?

    language = CountryWebsiteLanguage.find_by(yml_languages: lang_cookie)
    current_user.update(country_website_language_id: language.id) if language.present?
    cookies.delete USER_SELECT_COOKIE_KEY
    cookies.delete AUTO_SELECT_COOKIE_KEY
  end

  def current_user_can_access_language_selector?
    FeatureFlipper.show_feature?(:language_selector, current_user) && current_user.can_use_language_selector?
  end

  def fetch_language_selector_options
    @choose_a_lang_translation ||= t("language_selector_app.choose_a_language")&.upcase
    @language_selector_options ||=
      if current_user
        options = language_selector_options_for_current_user

        ActiveModel::ArraySerializer.new(options, each_serializer: Api::External::CountryWebsiteLanguageSerializer).to_json(root: false)
      end
  end

  def current_locale_language
    country_website_language = CountryWebsiteLanguage.find_by(yml_languages: I18n.locale)
    @current_locale_lang_abbrv ||= country_website_language&.language_abbreviation
    @current_locale_lang ||= country_website_language&.selector_language
  end

  def country_website_language
    user_country_website = CountryWebsite.find_by(country: country_website)
    session[:country_website_language] =
      if lang_cookie = get_language_cookie
        CountryWebsiteLanguage::LANGUAGE_CODE_TO_COUNTRY_MAP[lang_cookie] ||
          CountryWebsiteLanguage.find_by(yml_languages: lang_cookie)&.selector_country
      elsif current_user && current_user.country_website_language_id
        current_user.reload.country_website_language.selector_country
      elsif country_website == "US"
        user_country_website&.full_country_name
      else
        CountryWebsite::COUNTRY_WEBSITE_ID_TO_LANGUAGE_COUNTRY_MAP[user_country_website&.id]
      end
  end

  private

  def language_selector_options_for_current_user
    CountryWebsiteLanguage
      .where(country_website: current_user.country_website)
      .where.not(yml_languages: nil)
      .filter_locales(current_user, country_website)
      .order(:selector_order)
  end

  def correct_user_domain?
    !(current_user && current_user.domain != request.host)
  end

  def accessing_via_backend_servers?
    !!(request.host =~ /^studio/ || request.host =~ /^next/)
  end

  # show_website_feature? is for when we don't have a current_user
  def cookie_language_setter_enabled?
    FeatureFlipper.show_feature?(:cookie_language_setter, current_user) ||
      FeatureFlipper.show_website_feature?(:cookie_language_setter, country_website)
  end
end
