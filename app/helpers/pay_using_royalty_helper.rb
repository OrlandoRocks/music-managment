module PayUsingRoyaltyHelper
  def base_amount(total_price)
    return unless vat_feature_flipper_enabled?

    base_amount = total_price * (100.00 / (100 + tax_rate))
    base_amount = current_user.balance if base_amount > current_user.balance
    base_amount
  end

  def can_cover_base_amt_with_balance?(base_amount, balance)
    return unless vat_feature_flipper_enabled?

    balance >= base_amount
  end

  def outbound_vat(base_amount)
    return unless vat_feature_flipper_enabled?

    base_amount * (tax_rate / 100.00)
  end

  def amount_payable(total_price, base_amount, outbound_vat)
    return unless vat_feature_flipper_enabled?

    [(total_price - (base_amount + outbound_vat)), 0].max
  end

  def tax_rate
    @tax_rate ||= tax_info[:tax_rate].to_f
  end

  def tax_info
    @tax_info ||= TcVat::OutboundVatCalculator.new(current_user).fetch_vat_info
  end

  def balance_covers_base_amount?
    current_user.balance >= @base_amount && current_user.balance < @cart_data.total_price
  end
end
