module PublishingTerminatedComposerHelper
  def existing_partially_terminated_publishing_composer(id)
    TerminatedComposer.where(publishing_composer_id: id, termination_type: "partially").first
  end

  def flash_success_notice
    flash[:notice] = "Publishing Composer has been fully Terminated for Publishing Administration"
  end

  def publishing_composer_termination_params(raw_params)
    {
      publishing_composer_id: raw_params[:terminated_composer][:publishing_composer_id],
      effective_date: raw_params[:terminated_composer][:effective_date],
      admin_note: raw_params[:terminated_composer][:admin_note],
      publishing_termination_reason_id: raw_params[:terminated_composer][:publishing_termination_reason_id]
    }
  end
end
