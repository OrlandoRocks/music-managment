# frozen_string_literal: true

module MyAccountHelper
  INTAKE_CLASSES = Set[
    'PersonIntake',
    'OutboundInvoice',
    'OutboundRefund',
    'Refund',
    'SplitRoyaltyIntake'
  ]

  PAYOUT_TYPES = {
    ACCOUNT: :account,
    BANK: :bank,
    PREPAID_CARD: :prepaid_card,
    PAYPAL: :paypal,
    PAPER_CHECK: :paper_check,
    PAYONEER_PARTNER: :payoneer_partner
  }.with_indifferent_access.freeze

  FEES_AND_VAT_COMMENTS = Set[
    "Check Service fee",
    VatTaxAdjustment::PAYOUT_TRANSACTION_COMMENT,
    VatTaxAdjustment::VAT_POSTED_COMMENT,
    VatTaxAdjustment::VAT_DEBIT_COMMENT
  ]

  def named_route_month(params = {})
    raise ArgumentError, "Cannot determine named route" unless /^(.*)(_year|_month)$/ =~ controller.action_name

    send("#{$1}_month_url", params)
  end

  def named_route_year(params = {})
    raise ArgumentError, "Cannot determine named route" unless /^(.*)(_year|_month)$/ =~ controller.action_name

    send("#{$1}_year_url", params)
  end

  def csv_report_path(object = nil)
    result = "/#{controller.controller_name}"
    result += "/#{object.id}" if object
    result += ".csv?year=#{@year}&month=#{@month}&order=#{@order}&per_page=#{@per_page}"
    result
  end

  def country_codes
    # sort the country codes, and make sure the US is always at the top.

    CountryCodes.countries_for_select('name', 'a2').sort { |a, b|
      if a[1] == 'US'
        -1
      elsif b[1] == 'US'
        1
      else
        a[0] <=> b[0]
      end
    }
  end

  def reporting_period
    if @reporting_month
      @reporting_month.in_english
    else
      @reporting_year
    end
  end

  def flag_image(sale)
    image_tag("/images/flag-" + country_of_sale(sale).gsub(/\s+/, '_') + ".gif", width: 10, height: 8)
  end

  def item_description(album, item)
    case item
    when Album
      "by #{item.artist_name}"
    when Song
      %Q|from the album "#{album.name}" by: #{album.artist_name}|
    end
  end

  def item_identifier(item)
    case item
    when Album
      "UPC# #{item.upc}"
    when Song
      "ISRC# #{item.isrc}"
    end
  end

  def display_store_name(store)
    return '&nbsp;' if store.nil?
    return store.name unless (/^iTunes / =~ store.name)

    parts = store.name.split(' ', 2)
    parts[1] = %Q|<br/><span class='ancillary'>#{parts[1]}</span>| if parts[1]
    parts.join('')
  end

  def display_store_flag(store)
    return '&nbsp;' if store.nil?

    image_tag("flag-#{store.abbrev}.gif", width: 25, height: 25, class: 'store_flag')
  end

  def copies_sold(sale)
    case sale.sale_type
    when 'st'
      pluralize(sale.units_sold, "trial stream")
    when 'ss'
      pluralize(sale.units_sold, "stream")
    else
      pluralize(sale.units_sold, "download")
    end
  end

  def country_of_sale(sale)
    sale.country_of_sale or "unknown"
  end

  def substore_and_country_of_sale(sale)
    if sale.substore.blank?
      country_of_sale(sale)
    else
      "#{sale.substore} (#{country_of_sale(sale)})"
    end
  end

  def sales_period(sale)
    from = sale.statement_start_date
    to = sale.statement_end_date
    return "unknown" if from.nil? || to.nil?

    "from #{from.strftime('%d-%b-%y')} to #{to.strftime('%d-%b-%y')}"
  end

  def price_per_copy(sale)
    money_to_currency(Money.new(sale.local_unit_cents, sale.local_currency), html: true, suppress_iso: false)
  end

  def exchange_rate(item)
    return "Calculated from #{item.exchange_rate_count} different exchange rates" if item.has_multiple_exchange_rates?
    return '&nbsp;' if item.exchange_symbol.blank?

    rate = item.exchange_rate
    xbase, xother = item.exchange_symbol.scan(/.../)

    "1 #{currency_detail(xbase)} = #{rate} #{currency_detail(xother)}"
  end

  def net_sales_usd(item)
    amt = item.is_a?(Array) ? item.sum(&:net_sales_usd) : item.net_sales_usd
    money_to_currency(Money.new(amt, current_user.currency), html: true, suppress_iso: true)
  end

  def net_sales_local(item, local_currency = nil)
    amt = item.is_a?(Array) ? item.sum(&:net_sales_local) : item.net_sales_local
    money_to_currency(Money.new(amt, local_currency || item.local_currency), html: true, suppress_iso: false)
  end

  def net_earnings_usd(item)
    amt = item.is_a?(Array) ? item.sum(&:net_earnings_usd) : item.net_earnings_usd
    money_to_currency(Money.new(amt, current_user.currency), html: true, suppress_iso: true)
  end

  def songs_sold(item)
    return item.sum(&:songs_sold) if item.is_a? Array

    item.songs_sold
  end

  def songs_streamed(item)
    return item.sum(&:songs_streamed) if item.is_a? Array

    item.songs_streamed
  end

  def albums_sold(item)
    raise item.inspect if item.nil?
    return item.sum(&:albums_sold) if item.is_a? Array

    item.albums_sold
  end

  def display_person_transaction_headline(person_transaction)
    require_relative 'application_helper.rb'
    return if INTAKE_CLASSES.include?(person_transaction.target_type)

    comment = person_transaction.comment
    status, approved_by = render_transaction_status(person_transaction)
    date_status = ''
    unless FEES_AND_VAT_COMMENTS.include?(comment)
      date_status = '<br>Status: ' + status + '<br>Last Update: ' + fmt_country_date(person_transaction.target.updated_at) + '<br> Updated By: ' + approved_by.to_s if @current_user.is_administrator? and ["PaypalTransfer", "CheckTransfer", "PayoutTransfer"].include? person_transaction.target_type
    end

    if comment && comment.include?("Payment for Invoice I")
      # this doesn't print links to invoices that have been deleted
      comment.gsub!(/Payment for Invoice/) { |_com| custom_t('my_account.list_transactions.pay_invoice') }
      if comment.match?(/(\d+)/)
        # comment.gsub!(/(\d+)/) {|match| Invoice.first(:conditions=>{:id=>match.to_i}) ? link_to(match.to_s, invoice_path(Invoice.find(match.to_i))) : match.to_s }
        comment.gsub!(/I\s*(\d+)/) { |invoice_id| invoice_link(invoice_id.tr('I', '')) }
      end
    end

    comment =
      case comment
      when VatTaxAdjustment::PAYOUT_TRANSACTION_COMMENT
        custom_t('my_account.list_transactions.payout_transaction_vat')
      when VatTaxAdjustment::VAT_POSTED_COMMENT
        custom_t('my_account.list_transactions.vat_posted')
      when VatTaxAdjustment::VAT_REVERTED_COMMENT
        custom_t('my_account.list_transactions.vat_posting_reverted')
      when VatTaxAdjustment::VAT_DEBIT_COMMENT
        custom_t('my_account.list_transactions.vat_debit')
      else
        comment
      end

    comment = comment.to_s || nil
    pdf_link = display_songwriter_royalty_link(person_transaction, 'PDF')
    csv_link = display_songwriter_royalty_link(person_transaction, 'CSV')
    other_link = display_songwriter_royalty_link(person_transaction, 'other')
    if pdf_link.present? || csv_link.present?
      %{<span class="transaction-headline">#{comment} #{date_status} - Download #{royalty_link_text(pdf_link, csv_link)} #{other_link}</span>}.html_safe
    else
      %{<span class="transaction-headline">#{comment} #{date_status} #{other_link}</span>}.html_safe
    end
  end

  def royalty_link_text(pdf_link, csv_link)
    "#{pdf_link.presence} #{" / " if pdf_link.present? && csv_link.present?} #{csv_link.presence}"
  end

  def display_intake_comments(person_transaction)
    html = nil
    case person_transaction.target_type
    when 'PersonIntake'
      html = +""
      html << '<span class="transaction-headline"><a class="column-expand" href="#' + person_transaction.id.to_s + '">'
      html << "Sales Posted" unless person_transaction.comment.try(:include?, "2018-05-18")
      html << " " + person_transaction.comment if person_transaction.comment && person_transaction.created_at > Date.new(2011, 04, 01) # this is to show comments added for person_intakes after SIP changeover, particularly related to Amazon US Correction of January 2014 sales
      html << "</a>"
    when 'YouTubeRoyaltyIntake'
      html = +""
      html << '<span class="transaction-headline">You Tube Royalties Posted<span>'
    when 'SplitRoyaltyIntake'
      description =
        if person_transaction.debit.zero?
          "Royalty Payment"
        else
          "Splits Royalty Payout"
        end
      html = "<span class='transaction-headline'><a class='column-expand' href='#' #{person_transaction.id}'>#{description}</a><span>"
    when 'OutboundInvoice'
      # link to download outbound invoice
      html = +""
      html << generate_royalty_invoice_link(person_transaction.target)
    when 'OutboundRefund'
      html = generate_cr_royalty_invoice_link(person_transaction.target)
    when 'Refund'
      html = generate_refund_link(person_transaction.target)
    end
    return if html.nil?

    html.html_safe
  end

  def generate_royalty_invoice_link(outbound_invoice)
    return custom_t('my_account.list_transactions.royalty_invoice') if outbound_invoice.invoice_sequence.nil?

    link_to(
      custom_t('my_account.list_transactions.royalty_invoice'),
      view_royalty_invoice_path(invoice_id: outbound_invoice.id),
      class: 'export'
    )
  end

  def generate_cr_royalty_invoice_link(outbound_refund)
    return custom_t("my_account.list_transactions.cr_royalty_invoice") if outbound_refund.credit_note_invoice_number.blank?

    custom_t("my_account.list_transactions.cr_royalty_invoice") + ": " + link_to(
      outbound_refund.credit_note_invoice_number,
      view_cr_royalty_invoices_path(outbound_refund.id),
      class: "export"
    )
  end

  def generate_refund_link(refund)
    "#{custom_t("my_account.list_transactions.refund_invoice")} #{invoice_link(refund.invoice_id)}"
  end

  def display_person_intake_export(person_transaction)
    return unless person_transaction.target_type == 'PersonIntake'

    return unless person_transaction.target.summarized?

    html = +""
    html << '<tr class="export hidden"><td colspan="5">'
    unless FeatureFlipper.show_feature?(:disable_sales, current_user)
      html << link_to("Export CSV", { controller: '/my_account', action: 'export_intake_transactions', transaction_id: person_transaction.id }, class: 'export')
    else
      html << "--"
    end
    html << '</td></tr>'
    html.html_safe
  end

  def display_comments(transaction)
    if transaction.target_type == "Invoice"
      "Payment for Inv #{link_to 'I' + transaction.target_id.to_s, invoice_path(Invoice.find(transaction.target_id))}"
    else
      h transaction.comment
    end
  end

  def display_store_link(t)
    sales_period_type =
      if t.period_type == 'quarterly'
        "#{identify_quarter_with_year(t.period_interval.to_i, t.period_year)}"
      else
        t.period_sort
      end

    html = +""
    html << "<span class='store-name'>"
    html << link_to("#{t.store_name} - #{t.country} - #{sales_period_type} Sales", store_report_path(stores: t.store_display_group, countries: t.country, date: { start_date: fmt_first_of_month(Date.parse(t.period_sort)), end_date: fmt_first_of_month(Date.parse(t.period_sort)) }, external_page: true))
    html << "</span>"
    html.html_safe
  end

  def display_split_link(split)
    royalty_split = RoyaltySplit.find_by(id: split.royalty_split_id)

    return link_to(split.royalty_split_title, invited_royalty_splits_path(anchor: dom_id(royalty_split))) if split.amount.positive?

    return link_to(split.royalty_split_title, royalty_splits_path(anchor: dom_id(royalty_split))) unless royalty_split.nil?

    song = Song.find_by(id: split.song_id)
    album = Album.find_by(id: song.album_id)
    link_to(split.royalty_split_title, album_path(album))
  end

  def identify_quarter_with_year(interval, year)
    case interval
    when 1
      "Q1 #{year} (Jan, Feb, Mar)"
    when 2
      "Q2 #{year} (Apr, May, Jun)"
    when 3
      "Q3 #{year} (Jul, Aug, Sept)"
    else
      "Q4 #{year} (Oct, Nov, Dec)"
    end
  end

  def display_songwriter_royalty_link(person_transaction, type)
    if person_transaction.target_type == 'BalanceAdjustment' && (person_transaction.target.category == 'Songwriter Royalty' || person_transaction.target.category == 'YouTube MCN Royalty')
      royalty_payment = RoyaltyPayment.where({ posting_type: 'BalanceAdjustment', posting_id: person_transaction.target.id }).first
      link = +""

      if royalty_payment
        if type == 'PDF' && royalty_payment.pdf_exists?
          link << link_to("PDF", royalty_payment.download_pdf_url)
        elsif type == 'CSV' && royalty_payment.csv_exists?
          link << link_to("CSV", royalty_payment.download_csv_url)
        else
          composer = current_user.publishing_composers.primary.first

          if composer
            if royalty_payment.missing_tax_info?
              if person_transaction.person == current_user
                link << "<p>Payment held &mdash; Please #{link_to('complete tax info', new_composer_tax_info_path(composer, tax_form: 'w9'), class: "tax_form_link")}</p>"
              end
            elsif royalty_payment.hold_payment && !royalty_payment.negative_balance?
              link << "<p>Payment held &mdash; #{royalty_payment.show_hold_type}</p>"
            end
          end
        end
      end
      link = link.html_safe
    end
    link
  end

  def bank_account_link
    return unless allow_bank_account?(current_user) && !payoneer_payout_enabled?

    link_to(custom_t("shared.inside_header.bank_account"), edit_person_path(current_user, tab: "payout"))
  end

  def payoneer_payout_link
    return unless payoneer_payout_enabled?

    link_to(custom_t("people.edit.payout_preferences"), edit_person_path(current_user, tab: "payoneer_payout"))
  end

  def render_transaction_status(person_transaction)
    transaction_target = person_transaction.target
    if transaction_target.respond_to? :transfer_status
      status = (transaction_target.transfer_status == 'processing') ? 'Completed' : transaction_target.transfer_status.capitalize
      approved_by = transaction_target.approved_by.try(:name)
    elsif transaction_target.is_a?(PayoutTransfer)
      status = render_payout_transaction_status(transaction_target)
      approved_by = transaction_target.tunecore_processor.try(:name)
    end
    return status, approved_by
  end

  def render_payout_transaction_status(payout_transaction)
    if payout_transaction.try(:completed?)
      "Completed"
    elsif payout_transaction.try(:unsuccessful?)
      "Cancelled"
    else
      "Processing"
    end
  end

  def currency_selection_options
    PayoutProvider::WITHDRAWAL_CURRENCIES.map do |cur|
      [custom_t("my_account.withdraw.currency_labels.#{cur}"), cur]
    end
  end

  def fees_table_values(value, inside_domain)
    domain_type = inside_domain ? :us : :non_us
    if [:bank, :payoneer_partner].include?(withdraw_method_type)
      transfer_fees_table_values(value)
    else
      other_fees_table_values(value, domain_type)
    end
  end

  def transfer_fees_table_values(value)
    case value
    when :country_name
      "#{@default_fees&.country&.name}"
    when :target_country_code
      "#{@default_fees&.target_country_code}"
    when :target_currency
      "#{@default_fees&.target_currency}"
    when :fee
      "#{format_to_currency(Money.new(@default_fees&.transfer_fees.to_f * 100))} #{@default_fees&.source_currency}"
    when :min_withdraw
      "#{format_to_currency(Money.new(@default_fees&.minimum_threshold.to_f * 100))} #{@default_fees&.source_currency}"
    end
  end

  def other_fees_table_values(value, domain_type)
    case value
    when :country_name
      "#{@default_fees&.country&.name}"
    when :target_country_code
      "#{@default_fees&.target_country_code}"
    when :target_currency
      custom_t("payoneer_fees.popup.results.usd")
    when :fee
      options = { payout_type: withdraw_method_type, fee_type: value, domain_type: domain_type }
      modal_payout_fees(options)
    when :min_withdraw
      options = { payout_type: withdraw_method_type, fee_type: value, domain_type: domain_type }
      modal_payout_fees(options)
    end
  end

  def transaction_value_params
    return if params[:transaction_values].nil?

    params[:transaction_values].permit(
      :PersonIntake,
      :Invoice,
      :CheckTransfer,
      :EftBatchTransaction,
      :PaypalTransfer,
      :BalanceAdjustment,
      :FriendReferral,
      :PublishingRoyalty,
      :YouTubeRoyaltyIntake,
      :BatchTransaction,
      :OutboundInvoice,
      :VatTaxAdjustment,
      :SplitRoyaltyIntake
    )
  end

  def has_withdrawn?
    withdraw_method.present?
  end

  def set_payee_details
    version = FeatureFlipper.show_feature?(:use_payoneer_v4_api, current_user) ? :v4 : :v2
    api_service = PayoutProvider::ApiService
                  .new(person: current_user, api_client: Payoneer::PayoutApiClientShim.fetch_for(current_user))
    @payee_details =
      case version
      when :v2
        api_service.payee_extended_details(person: current_user)
      when :v4
        api_service.payee_status(person: current_user)
      end
  end

  def withdraw_method
    set_payee_details
    PayoutTransfer::PAYOUT_METHODS.fetch(@payee_details.withdrawal_type, nil)
  end

  def withdraw_method_type
    set_payee_details
    PAYOUT_TYPES.fetch(@payee_details.withdrawal_type, nil)
  end

  def transaction_filters_for_user(user = current_user)
    user = Person.find(params.fetch(:id)) if controller.instance_of?(Admin::PeopleController)
    return PersonTransaction::TRANSACTION_TYPES unless payoneer_payout_enabled?(user)

    PersonTransaction::TRANSACTION_TYPES + PersonTransaction::PAYOUT_TRANSACTION_TYPE
  end

  def category_filters(_user = current_user)
    PersonTransaction::BALANCE_ADJUSTMENT_CATEGORIES
  end

  def vat_filters
    PersonTransaction::VAT_TRANSACTION_TYPES
  end

  def show_paypal_modal?
    Payoneer::FeatureService.bi_transfer?(current_user) && !current_user.address_locked?
  end

  def upgrade_required_copy
    return if current_user.feature_gating_can_do?(:premium_sales_report)

    "<h3>#{custom_t('plans.upgrade_required')}</h3>".html_safe
  end

  def upgrade_required_image
    return if current_user.feature_gating_can_do?(:premium_sales_report)

    image_tag "plans/LockIcon.png"
  end

  def show_withholding_tooltip?(transaction)
    transaction.target_type == "IRSTaxWithholding"
  end
end
