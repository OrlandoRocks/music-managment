module PayoutTransferHelper
  def has_payout_errors?(payout_transaction_form)
    payout_transaction_form.errors.messages.any? { |_, message| message.include? "unavailable_balance" }
  end

  def generate_currency_toggle_text
    formatted_text = <<-HTML
      #{custom_t('.see_estimated')} <span class="payout-untoggled-value">#{current_user.user_currency}</span>
      <span class="payout-toggled-value">USD</span>
    HTML
    raw(formatted_text)
  end

  def payoneer_fees_disclaimer
    return custom_t(".payoneer_fee_bank_checkout_disclaimer") if @create_form.bank_withdrawal?

    custom_t(".payoneer_fee_general_checkout_disclaimer")
  end

  def transfer_auto_approval_successful(transfer)
    return "N/A" unless transfer.auto_approval_eligible?

    transfer.auto_approved? ? "Yes" : "No"
  end

  def show_tax_withheld_info?
    FeatureFlipper.show_feature?(:us_tax_withholding) && current_user.tax_withheld?
  end
end
