module LowRiskArtistsHelper
  def artist_names_for_release(album)
    album_artists = album.album_artist_names.split(",")
    album_artists << album.song_artist_names.split(",") if album.song_artist_names
    album_artists.flatten.uniq.map(&:strip).join(", ")
  end
end
