module PayoneerFeesHelper
  def payoneer_fees_display_service
    return @payoneer_fees_display_service unless @payoneer_fees_display_service.nil?

    @payoneer_fees_display_service = PayoneerFeesDisplayService.new(current_user)
  end

  def paypal_fees
    payoneer_fees_display_service.paypal_fee_display
  end

  def bank_transfer_fees
    payoneer_fees_display_service.ach_fee_display
  end

  def bank_transfer_disclaimer
    custom_t("payoneer_fees.index.swift_disclaimer")
  end

  def bank_transfer_min_withdrawal
    payoneer_fees_display_service.ach_min_withdraw_display
  end

  def bank_transfer_min_disclaimer
    custom_t("payoneer_fees.index.min_varies_by_country")
  end

  # Payoneer rejects transfer requests lower than $2.
  #
  # For Payoneer-PayPal requests, TC holds the fees and sends to Payoneer the net transfer amount excluding fees.
  # Eg: A requests of $2 gets rejected by Payoneer since TC sends to Payoneer $1.98 after holding 2% fees.
  #
  # For non-PayPal requests, TC send the entire requested amount including fees to Payoneer.
  # Also, the lowest fees set for non-PayPal requests is 1$.
  # Eg: A request of $2 gets approved by Payoneer since TC sends entire $2 to Payoneer for non-PayPal requests.
  #
  # Here, we restrict customers from placing withdrawal requests with net transfer amount lower than $2 for PayPal.
  def minimum_net_transfer_amount_in_cents(withdrawal_type)
    return PayoutTransfer::PAYONEER_PAYPAL_NET_TRANSFER_LIMIT_IN_CENTS if withdrawal_type == PayoutTransfer::PAYPAL

    PayoutTransfer::PAYONEER_NON_PAYPAL_NET_TRANSFER_LIMIT_IN_CENTS
  end

  def paper_check_fees
    payoneer_fees_display_service.paper_check_fee_display
  end

  def paper_check_min_withdrawal
    payoneer_fees_display_service.paper_check_min_withdraw_display
  end

  def mastercard_activation_fee
    price = 1000
    amount = to_currency(price)
    currency = custom_t("payoneer_fees.index.currency")

    if display_localized_fee?
      amount = localize_fee(price)
      currency = ""
    end

    custom_t(
      "payoneer_fees.index.one_time_activation",
      amount: amount,
      currency: currency
    )
  end

  def mastercard_atm_fee
    price = 175
    amount = to_currency(price)
    currency = custom_t("payoneer_fees.index.currency")

    if display_localized_fee?
      amount = localize_fee(price)
      currency = ""
    end

    custom_t(
      "payoneer_fees.index.atm_fee",
      amount: amount,
      currency: currency
    )
  end

  def mastercard_fees
    payoneer_fees_display_service.cc_fee_display
  end

  def mastercard_min_withdrawal
    payoneer_fees_display_service.cc_min_withdraw_display
  end

  def display_localized_fee?
    current_user.country_website.non_native_currency_site?
  end

  def us_resident_modal_program(corporate_entity_name)
    corporate_entity = CorporateEntity.find_by_name(corporate_entity_name)
    config_currency = current_user.site_currency

    # need to check for presence here because the new payout programs don't all exist yet - once they all exist we may
    # remove this safety check
    config = PayoutProviderConfig
             .by_env
             .find_by(
               corporate_entity: corporate_entity,
               currency: config_currency,
               name: PayoutProviderConfig::PAYONEER_NAME
             )
    config.program_id if config.present?
  end

  private

  def localize_fee(price_in_cents)
    html = ""
    html += "<div>#{to_currency(price_in_cents)}*</div>"
    html += "<br><br> #{to_display_currency(price_in_cents)}"
    html.html_safe
  end

  def to_currency(price_in_cents)
    amount = Money.new(price_in_cents)
    balance_to_currency(amount)
  end

  def to_display_currency(price_in_cents)
    amount = Money.new(price_in_cents)
    format_to_currency(amount)
  end
end
