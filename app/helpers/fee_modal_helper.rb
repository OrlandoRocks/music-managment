module FeeModalHelper
  INSIDE_DOMAINS = ["US"].freeze
  MODAL_FEES = {
    paypal: {
      fee: {
        us: "$0.25",
        non_us: "2% up to $20.00 USD"
      },
      min_withdraw: "Fee Minimum"
    },
    paper_check: {
      fee: "$5.00",
      min_withdraw: "$100.00"
    },
    prepaid_card: {
      fee: "$1.00",
      min_withdraw: "$2.00"
    }
  }.freeze

  def modal_payout_fees(options)
    payout_type = options[:payout_type]
    fee_type    = options[:fee_type]
    domain_type = options[:domain_type]
    return MODAL_FEES.dig(payout_type, fee_type, domain_type) if paypay_fee?(payout_type, fee_type)

    MODAL_FEES.dig(payout_type, fee_type)
  end

  def paypay_fee?(payout_type, fee_type)
    payout_type == :paypal && fee_type == :fee
  end

  def inside_domain?(iso_code)
    INSIDE_DOMAINS.include?(iso_code)
  end
end
