module OnDemandTemplateHelper
  include ApplicationHelper

  def template_id_for_slider_index(template_id = 4)
    (template_id == 1 || template_id.nil?) ? 4 : template_id
  end
end
