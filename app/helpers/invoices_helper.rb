# frozen_string_literal: true

# = Description
# Methods that are used to help out the visual display of invoices
#
# = Usage
#
#   <%= album_store_list(album) %>
#
# = Change Log
# [2009-12-18 -- MJL]
# Moved the album_store_list into the invoices helper since the invoice
# thanks page was the only place calling it

module InvoicesHelper
  HIDEABLE_DESCRIPTION_ITEMS = [Product::ALBUM, Product::SINGLE].freeze
  RELEASE_CLASSES = %w[Album Ringtone Single].map(&:freeze).freeze

  def outstanding_amount(invoice)
    Money.new(outstanding_amount_cents(invoice).to_i, invoice.currency)
  end

  def outstanding_amount_cents(invoice)
    invoice.outstanding_amount_cents
  end

  def invoice_amount(invoice)
    Money.new(invoice_amount_cents(invoice).to_i, invoice.currency)
  end

  def invoice_amount_cents(invoice)
    if invoice.settled?
      invoice.final_settlement_amount_cents
    else
      invoice.invoice_amount_cents
    end
  end

  def invoice_vat_exclusive_amount(invoice)
    amount = invoice_amount_cents(invoice) - invoice_vat_amount_cents(invoice)
    Money.new(amount, invoice.currency)
  end

  def invoice_vat_amount(invoice)
    Money.new(invoice_vat_amount_cents(invoice), invoice.currency)
  end

  def invoice_vat_amount_cents(invoice)
    if invoice.settled?
      invoice.vat_amount_cents
    else
      invoice.invoice_vat_amount_cents
    end
  end

  def settlement_description(settlement)
    case settlement.source_type
    when "PaypalIpn"
      "Paypal"
    when "PaypalTransaction"
      "Paypal"
    when "PersonTransaction"
      "Tunecore Balance"
    when "BraintreeTransaction", "PaymentsOSTransaction"
      "Credit Card"
    when "AdyenTransaction"
      settlement.source.payment_method_display_name
    else
      "unknown settlement type"
    end
  end

  def paypal_info(settlement)
    return unless settlement.source_type == "PaypalTransaction"

    html_content = "<br/>"
    html_content << content_tag(:span, settlement.try(:source).try(:email), class: "weak-subtext")
    html_content.html_safe
  end

  def album_stores(album, purchase)
    if purchase && purchase.paid? && album.plan_credit_usage.blank?

      purchase = album.inventory_usage.inventory.purchase if purchase.related_type == "CreditUsage"

      album
        .stores
        .joins("inner join inventory_usages iu on salepoints.id=iu.related_id inner join inventories i on i.id=iu.inventory_id")
        .where("i.purchase_id = ? and inventory_type = 'Salepoint' and iu.id IS NOT NULL and in_use_flag = 1", purchase.id)
    else
      album.stores
    end
  end

  def build_album_store_list(album, purchase)
    filtered_stores = album_stores(album, purchase).exclude_believe

    filtered_stores = filtered_stores.exclude_discovery(current_user) if discovery_feature?

    filtered_stores.map(&:name).join(", ")
  end

  # creates a comma delimited list of stores for a given album
  def album_store_list(album, purchase = nil)
    store_list = build_album_store_list(album, purchase)
    store_list.presence || "No Stores Selected"
  end

  def album_stores?(album, purchase)
    build_album_store_list(album, purchase).present?
  end

  def show_album_stores?(album, purchase = nil)
    return true unless discovery_feature?

    album_stores?(album, purchase)
  end

  def build_free_album_store_list(album, purchase)
    return unless discovery_feature?

    album_stores(album, purchase)
      .discovery_platforms
      .map(&:name).join(", ")
      .presence
  end

  def get_release(purchase)
    if purchase.purchased_item.class.name.in?(RELEASE_CLASSES)
      purchase.purchased_item
    elsif purchase.credited? && purchase.purchased_item&.related.class.name.in?(RELEASE_CLASSES)
      purchase.purchased_item.related
    else
      nil
    end
  end

  def fb_freemium_purchase?(purchases)
    purchases.any? do |purchase|
      release = get_release(purchase)
      next if release.nil?

      release.stores.any? { |s| s.id == Store::FB_REELS_STORE_ID }
    end
  end

  # creates a comma delimited list of stores for a given album
  def free_album_store_list(album, purchase = nil)
    build_free_album_store_list(album, purchase)
  end

  def show_free_album_stores?(album, purchase = nil)
    return false unless discovery_feature?

    build_free_album_store_list(album, purchase).present?
  end

  def show_product_description?(item)
    return true unless HIDEABLE_DESCRIPTION_ITEMS.include?(item.class.name.titleize) &&
                       discovery_feature?

    !item.strictly_facebook_release?
  end

  def invoice_money(money, invoice = @invoice, options = {})
    options[:pegged_rate] = invoice&.pegged_rate
    money_to_currency(money, options)
  end

  def invoice_payment_data
    @balance_service ||= Invoice::BalancePaymentService.new(@invoice)
    @invoice_payment_info ||= {
      payment_methods: @balance_service.payment_methods,
      balance_payment: @balance_service.balance_payment,
    }
  end

  def format_amount_multiple_currencies(amount_in_usd, *fx_pegged_rates)
    formatted_amounts = [format_to_currency(Money.new(amount_in_usd, "USD")).to_s]
    fx_pegged_rates.each do |fx_pegged_rate|
      formatted_amounts << money_to_currency(amount_in_usd, pegged_rate: fx_pegged_rate.pegged_rate, currency: fx_pegged_rate.currency, round_value: false)
    end
    formatted_amounts.join(" | ")
  end

  def invoice_address
    person = @invoice.person
    @city_state ||= { city: person.city, state: person.state }
    payment_methods = invoice_payment_data[:payment_methods]

    credit_card_method = payment_methods.detect { |p| [PaymentsOSTransaction.to_s].include?(p.source_type) }
    cc = credit_card_method.source.stored_credit_card if credit_card_method.present?
    @city_state = { city: cc.country_state_city.name, state: cc.country_state.name } if cc.present?

    @city_state
  end

  def non_euro_invoice?(invoice_data)
    invoice_data[:currency] != EUR
  end

  def show_gst_invoice?
    person = @invoice.person
    return @invoice.gst_config.present? unless FeatureFlipper.show_feature?(:enable_gst, person)

    FeatureFlipper.show_feature?(:bi_transfer, person) ? @invoice.gst_config.present? : person.india_user?
  end

  def show_new_refunds?
    FeatureFlipper.show_feature?(:new_refunds, current_user)
  end

  def discovery_feature?
    FeatureFlipper.show_feature?(:freemium_flow, current_user) || FeatureFlipper.show_feature?(:discovery_platforms, current_user)
  end

  def show_credit_note_invoices?(invoice)
    show_new_refunds? &&
      !invoice.has_incomplete_static_data? &&
      invoice.refunds_with_credit_note_invoice.any?
  end

  def tc_accelerator_modal_content(opted_in)
    if opted_in
      {
        header_text: custom_t("invoices.tc_accelerator_modal.opted_in_header"),
        body_text: custom_t("invoices.tc_accelerator_modal.description", link: tc_accelerator_faq_link),
        button_info: [
          {
            class_name: "opt-out",
            display_text: custom_t("invoices.tc_accelerator_modal.opt_out_button"),
            data_opt: "out"
          },
          {
            class_name: "sounds-great",
            display_text: custom_t("invoices.tc_accelerator_modal.accept_button"),
            data_opt: "none"
          }
        ]
      }
    else
      {
        header_text: custom_t("invoices.tc_accelerator_modal.opted_out_header"),
        body_text: custom_t("invoices.tc_accelerator_modal.description", link: tc_accelerator_faq_link),
        button_info: [
          {
            class_name: "opt-in",
            display_text: custom_t("invoices.tc_accelerator_modal.opt_in_button"),
            data_opt: "in"
          },
          {
            class_name: "do-not-ask-again",
            display_text: custom_t("invoices.tc_accelerator_modal.dont_ask_again"),
            data_opt: "stop-asking"
          }
        ]
      }
    end
  end

  def invoice_artwork(invoice)
    Invoice::ArtworkService.call(invoice)
  end
end
