# = Description
# This helper was created to aggregate all of the purchases in an invoice into line items with product counts.
# For example:
# Instead of:
# Salepoint Distribution
# Salepoint Distribution
# Salepoint Distribution
# Salepoint Distribution
# Salepoint Distribution
#
# We present:
# 5 Salepoint Distributions
#
# = Change Log
# [2010-02-08 -- MJL]
# Created

module HistoryHelper
  # Takes in an array of line items where each
  # entry is a hash containing information about each line item
  def line_item_helper(line_item)
    string = ""
    line_item.each do |options|
      string << "<li>"
      string << pluralize(options[:purchases][:size], options[:purchases][:name])
      string << truncate(
        " - #{options[:albums].join(', ')}",
        length: 75,
        omission: " [...]"
      ) if options[:albums].present?
      string << "</li>"
    end
    string.html_safe
  end

  def account_overview(person, style = "vertical_wide")
    account_overview = Tunecore::AccountOverview.new(person)
    case style
    when "vertical_wide"
      render(partial: "shared/account_overview", locals: { account_overview: account_overview })
    when "vertical_thin"
      render(partial: "shared/account_overview_thin", locals: { account_overview: account_overview })
    when "horizontal"
      render(partial: "shared/account_overview_horizontal", locals: { account_overview: account_overview })
    end
  end

  def distribution_credit_overview(person)
    account_overview = Tunecore::AccountOverview.new(person, false)
    targeted_offers = TargetedOffer.for_person(person)
    render(
      partial: "shared/distribution_credit_overview",
      locals: { account_overview: account_overview, targeted_offers: targeted_offers }
    )
  end
end
