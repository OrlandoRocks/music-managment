module CompositionsHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : ""
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
    per_page = params[:per_page] || 10
    link_to title, { action: "paginate", sort: column, direction: direction }, { class: css_class + " sort" }
  end

  def status(status)
    status_text =
      case status
      when "Split missing"
        custom_t(
          "helpers.compositions.split_missing",
          publishing_splits_path: publishing_splits_path,
          status: status
        )
      when "Split submitted"
        custom_t("helpers.compositions.split_submitted", status: status)
      when "Sent for registration"
        custom_t("helpers.compositions.sent_for_registration", status: status)
      when "Registered"
        custom_t("helpers.compositions.registered", status: status)
      when "Pending"
        custom_t("helpers.compositions.pending", status: status)
      when "Rejected"
        custom_t("helpers.compositions.rejected", status: status)
      when "Not controlled"
        custom_t("helpers.compositions.not_controlled", status: status)
      when "Not distributed yet"
        custom_t("helpers.compositions.not_distributed_yet", status: status)
      else
        custom_t("helpers.compositions.unknown_status")
      end

    status_text.html_safe
  end

  def display_stat(status)
    count, stat = display_stat_count(status)
    return if count.zero?

    large_set = count > 9999
    type = status.split(" ").collect { |w|
      if w == "Split" && count.positive?
        w.pluralize
      else
        w
      end
    }.join(" ").downcase

    class_postfix = status.split(" ").join("_").downcase
    url = (stat[:state] == "total") ? compositions_path.to_s : "?status[]=" + stat[:state]
    %(<div class="stats-pod stats-#{class_postfix} #{'large-set' if large_set}">
      <a href="#{url}">
        <div class="stats-number">#{count}</div>
        <div class="stats-type">#{type}</div>
      </a>
    </div>).html_safe
  end

  def display_stat_count(status)
    stat = @stats.detect { |s| s[:status] == status }
    count = stat ? stat[:count] : 0
    return count, stat
  end
end
