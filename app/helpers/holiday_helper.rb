module HolidayHelper
  def gon_itunes_delays
    gon.push(
      itunesHolidayOneDeadline: holiday_one_deadline,
      itunesHolidayOneDelay: holiday_one_delay,
      itunesHolidayTwoDeadline: holiday_two_deadline,
      itunesHolidayTwoDelay: holiday_two_delay,
      itunesHolidayThreeDeadline: holiday_three_deadline,
      itunesHolidayThreeDelay: holiday_three_delay,
      itunesHolidayFourDeadline: holiday_four_deadline,
      itunesHolidayFourDelay: holiday_four_delay
    )
    itunes_holiday_one_help_text
    itunes_holiday_two_help_text
    itunes_holiday_three_help_text
    itunes_holiday_four_help_text
  end

  def holiday_date_ranges(start_date, end_date)
    (Date.parse(start_date)..Date.parse(end_date)).map do |date|
      date.strftime("%Y-%m-%d").tr(" ", "")
    end
  end

  def holiday_one_deadline
    "2018-11-06"
  end

  def holiday_two_deadline
    "2018-11-13"
  end

  def holiday_three_deadline
    "2018-11-27"
  end

  def holiday_four_deadline
    "2018-12-11"
  end

  def holiday_one_delay
    holiday_date_ranges("2018-11-16", "2018-11-30")
  end

  def holiday_two_delay
    holiday_date_ranges("2018-12-01", "2018-12-07")
  end

  def holiday_three_delay
    holiday_date_ranges("2018-12-08", "2018-12-21")
  end

  def holiday_four_delay
    holiday_date_ranges("2018-12-22", "2019-01-06")
  end

  def method_missing(method_sym, *arguments, &block)
    if method_sym.to_s =~ /^itunes_(.*)_help_text$/
      delay_hash = delay_dates($1)
      gon_custom_t("itunes#{$1.camelize}HelpText", "controllers.album.delay_date", start_date: delay_hash[:start_date], end_date: delay_hash[:end_date], deadline_date: delay_hash[:deadline_date])
    else
      super
    end
  end

  def delay_dates(name)
    delay_array = send((name + "_delay").to_sym)
    deadline_date = send((name + "_deadline").to_sym)
    {
      start_date: fmt_itunes_delay_date(DateTime.parse(delay_array.first)),
      end_date: fmt_itunes_delay_date(DateTime.parse(delay_array.last)),
      deadline_date: fmt_itunes_delay_date(DateTime.parse(deadline_date))
    }
  end

  def holiday_delays
    [
      holiday_one_delay,
      holiday_two_delay,
      holiday_three_delay,
      holiday_four_delay
    ]
  end
end
