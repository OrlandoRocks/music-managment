module PageTitleHelper
  def page_title
    action = controller.action_name
    @page_title ||=
      if restful_methods.include?(action)
        controller.controller_name.titleize
      else
        action.titleize
      end
  end

  def restful_methods
    ["index", "show", "destroy", "create", "update", "new", "edit"]
  end
end
