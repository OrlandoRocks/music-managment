module MarketingTagsHelper
  def data_layer_object(options)
    user_data = options[:page].user_data
    user_data = user_data.merge({ pageCategory: options[:pageCategory] }) if options[:pageCategory]
    user_data = user_data_failed_renewals(user_data)
    javascript_tag("dataLayer = [#{raw user_data.to_json.to_s}];", type: "text/javascript")
  end

  def optimizely_custom_tag(custom_tags_hash)
    custom_tags_hash_with_additions = custom_tags_hash.merge({ customer_language: locale.to_s })

    javascript_tag(
      "
      window['optimizely'] = window['optimizely'] || [];
      window['optimizely'].push({
        'type': 'user', 'attributes': #{custom_tags_hash_with_additions.to_json}
      });
    "
    )
  end

  def optimizely_page_name
    "#{params[:action].to_s.titleize}#{params[:controller].to_s.titleize}"
  end

  def user_data_failed_renewals(user_data)
    return user_data unless current_user

    failed_renewal = current_user.optimizely_failed_renewal
    user_data = user_data.merge(failed_renewal: (failed_renewal ? "1" : "0"))
    return user_data unless failed_renewal

    user_data.merge!(
      {
        failed_release_name: failed_renewal.items.first.related.name,
        failed_release_renewal_date: Time.at(failed_renewal.expires_at).strftime("%b %d, %Y")
      }
    )
  end
end
