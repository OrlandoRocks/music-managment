require "active_support/concern"

module PhysicalStoresHelper
  include SalepointHelper

  extend ActiveSupport::Concern

  # Currently only Amazon-on-Demand exists as a physical store. This should be generalized in the event we add more.
  def load_physical_store_views
    store = Store.find_by(abbrev: "aod_us")

    @aod_storegroup = StoreGroupView.new(
      {
        name: store.name,
        store_group: store,
        abbreviation: store.abbreviation,
        stores: [store]
      }
    )
  end
end
