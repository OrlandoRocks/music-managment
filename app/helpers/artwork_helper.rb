module ArtworkHelper
  def build_artwork_uploader(artwork, album_id)
    file_types = "*.JPG;*.jpg;*.PNG;*.png;*.GIF;*.gif;*.JPEG,*.jpeg,*.TIFF,*.tiff,*.TIF,*.tif"
    upload_url = album_artwork_url(album_id, key: upload_key)

    if artwork.uploaded?
      flash_uploader(
        custom_t(:choose_file),
        custom_t("helpers.artwork.artwork_uploaded"),
        file_types,
        "completed",
        upload_url,
        "upload_refresh()"
      )
    elsif artwork.last_errors.present?
      flash_uploader(
        custom_t(:choose_file),
        custom_t("helpers.artwork.choose_image"),
        file_types,
        "errored",
        upload_url,
        "upload_refresh()"
      )
    else
      flash_uploader(
        custom_t(:choose_file),
        custom_t("helpers.artwork.choose_image"),
        file_types,
        "none",
        upload_url,
        "upload_refresh()"
      )
    end
  end
end
