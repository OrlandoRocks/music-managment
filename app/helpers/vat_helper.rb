# frozen_string_literal: true

# module for vat related helper methods
module VatHelper
  EUROPE = "Europe"
  LUXEMBOURG = "Luxembourg"
  INDIVIDUAL = "individual"
  BUSINESS = "business"
  WITHDRAW_PAYPAL = "withdraw_paypal"
  NEW = "new"
  PAYOUT_TRANSFERS = "payout_transfers"

  def show_vat_info?
    vat_feature_flipper_enabled? && current_user.affiliated_to_bi?
  end

  def show_vat_form?
    vat_feature_flipper_enabled? && current_user.in_eu?
  end

  def confirm_and_pay?
    action_name == CONFIRM_AND_PAY
  end

  def withdraw_paypal?
    action_name == WITHDRAW_PAYPAL
  end

  def withdraw_payoneer?
    action_name == NEW && controller_name == PAYOUT_TRANSFERS
  end

  def vat_feature_flipper_enabled?(user = current_user)
    FeatureFlipper.show_feature?(:vat_tax, user)
  end

  def eu_individual?(country, customer_type)
    eu_except_luxembourg?(country) && individual?(customer_type)
  end

  def eu_business?(country, customer_type)
    eu_except_luxembourg?(country) && business?(customer_type)
  end

  def luxembourg_individual?(country, customer_type)
    luxembourg?(country) && individual?(customer_type)
  end

  def luxembourg_business?(country, customer_type)
    luxembourg?(country) && business?(customer_type)
  end

  def non_eu_individual?(country, customer_type)
    non_eu?(country) && individual?(customer_type)
  end

  def non_eu_business?(country, customer_type)
    non_eu?(country) && business?(customer_type)
  end

  def eu_except_luxembourg?(country_name)
    country = Country.find_by(name: country_name)
    return false if country.nil?

    Country::EU_COUNTRIES.include?(country.iso_code) && country_name != LUXEMBOURG
  end

  def individual?(customer_type)
    customer_type == INDIVIDUAL
  end

  def business?(customer_type)
    customer_type == BUSINESS
  end

  def non_eu?(country_name)
    country = Country.find_by(name: country_name)
    return false if country.nil?

    Country::EU_COUNTRIES.exclude?(country.iso_code)
  end

  def luxembourg?(country)
    country == LUXEMBOURG
  end

  def splitted_address_line(address_line)
    address_line.split(",").map(&:strip)
  end

  def hide_vat_column
    return "" if current_user.affiliated_to_bi?

    "hide"
  end

  def show_vat_to_euro?(customer_type, country, non_euro_invoice)
    business?(customer_type) && luxembourg?(country) && non_euro_invoice
  end

  def show_inbound_supplier_name?(invoice_data, inbound)
    inbound && invoice_data[:items].first[:tax_type].present?
  end

  def show_outbound_supplier_name?(invoice_data, inbound, country)
    (invoice_data[:items].first[:tax_type].present? && !non_eu?(country)) && !inbound
  end

  def show_vat_removal_warning?
    current_user.sharing_vat_number?
  end

  def outbound_optional_fields(person_data)
    element_hash = {
      custom_t("people.company_info.fields.enterprise_number") => person_data[:enterprise_number],
      custom_t("people.company_info.fields.company_registry_data") => person_data[:company_registry_data],
      custom_t("people.company_info.fields.place_of_legal_seat") => person_data[:place_of_legal_seat],
      custom_t("people.company_info.fields.registered_share_capital") => person_data[:registered_share_capital],
      custom_t("invoices.pdf.vat_number") => person_data[:vat_registration_number]
    }

    element_hash.each_with_object([]) do |(key, value), arr|
      arr << key + ":" + content_tag(:span, value, class: "greyed-text") if value.present?
    end
  end
end
