module PayoutHelper
  def needs_to_complete_signup?
    current_user.payout_provider.blank? || current_user.payout_provider&.onboarding?
  end

  def withdraw_money_link(opts = {})
    link_to custom_t(:withdraw_money), withdraw_path, class: opts[:html_class]
  end

  def needs_payoneer_enrollment?
    payoneer_payout_enabled? && current_user.payout_provider.blank?
  end

  def payoneer_payout_enabled?(user = current_user)
    Payoneer::FeatureService.payoneer_payout_enabled?(user) && !payoneer_opted_out?
  end

  def payoneer_mandatory?
    # NOTE: NEEDS TO BE UNCOMMENTED TO CHECK TAX FORMS
    # TaxFormCheckService.new(current_user.id).needs_tax_form? &&
    !payoneer_opted_out? && payoneer_payout_enabled?
  end

  def show_payoneer_redesigned_onboarding?(user = current_user)
    payoneer_mandatory? && Payoneer::FeatureService.payoneer_redesigned_onboarding?(user)
  end

  def show_fee_modal?
    show_payoneer_redesigned_onboarding? && available_in_region?(current_user.country_domain, "payoneer_fee_modal")
  end

  def payout_provider_approved?
    current_user.payout_provider.try(:approved?)
  end

  def useable_payout_provider_approved?
    payout_provider_approved? && !payoneer_opted_out?
  end

  def payout_provider_approved_and_enabled?
    payoneer_payout_enabled? && useable_payout_provider_approved?
  end

  def payout_provider_enabled_and_has_withdrawal_flow?
    payout_provider_approved_and_enabled? && Payoneer::FeatureService.payoneer_redesigned_onboarding?(current_user)
  end

  def payout_provider_active?
    !current_user.payout_provider.try(:disabled?)
  end

  def payout_provider_declined?
    current_user.payout_provider.try(:provider_status) == PayoutProvider::DECLINED
  end

  def payout_provider_pending?
    current_user.payout_provider.try(:provider_status) == PayoutProvider::PENDING
  end

  def payoneer_enabled_and_approved?
    payoneer_payout_enabled? && useable_payout_provider_approved?
  end

  def payout_application_processing?
    payoneer_payout_enabled? && provider_application_completed? && !payout_provider_approved?
  end

  def provider_application_completed?
    provider = current_user.payout_provider
    provider.present? && !provider.needs_resubmission?
  end

  def payoneer_opted_out?
    Payoneer::FeatureService::payoneer_opted_out?(current_user)
  end

  def show_legacy_options?
    if Payoneer::FeatureService::payoneer_redesigned_onboarding?(current_user)
      payoneer_opted_out? || (!payoneer_mandatory? && !(payout_provider_declined? || payout_provider_approved?))
    else
      true
    end
  end

  def show_eft_and_check?
    payoneer_mandatory? &&
      Payoneer::FeatureService.show_eft_and_check?(current_user)
  end

  def needs_tax_form?
    @needs_tax_form ||= TaxFormCheckService.new(current_user.id).needs_tax_form?
  end

  def payoneer_onboard_steps
    return @payoneer_onboard_steps unless @payoneer_onboard_steps.nil?

    approved = payout_provider_approved?

    @payoneer_onboard_steps = [
      needs_to_complete_signup?,
      payout_provider_pending?,
      (approved && !needs_tax_form?),
      (approved && needs_tax_form?)
    ]
  end

  def current_step
    1 + payoneer_onboard_steps.index(true)
  end

  def payoneer_last_onboard_step?
    current_step == payoneer_onboard_steps.length
  end

  def show_payoneer_currency_selector?
    Payoneer::FeatureService.payoneer_currency_dropdown_enabled?(current_user) && !current_user.payout_provider
  end

  def has_used_legacy_system?
    (
      current_user.eft_batch_transactions.present? ||
      current_user.paypal_transfers.present? ||
      current_user.check_transfers.present?
    )
  end

  def calculate_status_bar
    case total_steps_to_complete
    when 4
      current_step * 25
    when 3
      amounts = {
        "1": 33,
        "2": 66,
        "3": 100
      }.with_indifferent_access

      amounts[current_step.to_s]
    end
  end

  def total_steps_to_complete
    needs_tax_form? ? 4 : 3
  end

  def payout_provider
    @payout_provider ||= current_user.payout_provider
  end

  def payout_provider_link?
    payout_provider.present? && payout_provider.provider_link.present?
  end

  def evaluate_tax_blocking?
    FeatureFlipper.show_feature?(:advanced_tax_blocking, current_user)
  end

  def evaluate_and_tax_block_user
    if current_user.from_united_states_and_territories?
      TaxBlocking::PersonEarningsSummarizationService.new(current_user.id, Date.current.year).summarize
      TaxBlocking::TaxformInfoUpdateService.new(current_user.id, Date.current.year).summarize
    end
    TaxBlocking::UpdatePersonTaxblockStatusService.call(current_user)
  end

  def tax_blocked_user_attempting_withdrawal?
    FeatureFlipper.show_feature?(:advanced_tax_blocking, current_user) &&
      request.post? &&
      current_user.tax_blocked?
  end
end
