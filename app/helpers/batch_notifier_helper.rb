module BatchNotifierHelper
  def renewal_purchase_link(purchase)
    renewal_item_link(purchase.renewal.renewal_items.first)
  end

  def renewal_item_link(renewal_item)
    case renewal_item.related_type
    when "Album"        then "albums/#{renewal_item.related_id}"
    when "Entitlement"  then "trend_reports"
    when "Video"        then "videography"
    else nil
    end
  end

  def expired_credit_card_message(person)
    content_tag(
      :div,
      "#{content_tag(:h1, '***Action Required***', style: 'color: red;')}
      #{content_tag(:p, "We noticed your #{person.preferred_credit_card.cc_type.upcase} card ending in #{person.preferred_credit_card.last_four} has expired.<br />Please update your credit card information now.".html_safe)}
      #{content_tag(:p, link_to(image_tag('http://' + person.domain + '/images/email/update_now3.png', border: '0'), 'http://' + person.domain + '/payment_preferences'))}".html_safe,
      style: "border: 2px solid #000; margin-top: 20px; text-align: center;"
    )
  end

  def expired_dont_use_balance_message(person, renewal)
    content_tag(:h4, "How Do I Renew My Release?") +
      content_tag(:p, "Please provide valid credit card information in order for your release to be renewed on the renewal date, #{renewal.renewal_history.order('expires_at ASC').last.expires_at.strftime('%b %d, %Y')}. If your credit card cannot be charged, then your release will not be renewed, your music will be removed from all stores and all related reviews and comments will be deleted.".html_safe) +
      content_tag(:p, link_to(image_tag("http://" + person.domain + "/images/email/update_now3.png", border: "0"), "http://" + person.domain + "/payment_preferences"), style: "text-align: center;")
  end

  def expired_use_balance_message(person, renewal)
    content_tag(:h4, "How Do I Renew My Release?") +
      content_tag(:p, "On your renewal date, #{renewal.renewal_history.order('expires_at ASC').last.expires_at.strftime('%b %d, %Y')}, we will first try to deduct from your TuneCore balance to cover the cost of your renewal. However, if your TuneCore balance does not cover the full renewal amount, your releases will not be renewed until your back-up payment method is updated. If your release cannot be renewed, then your music will be removed from all stores, and all related reviews and comments will be deleted.".html_safe) +
      content_tag(:p, link_to(image_tag("http://" + person.domain + "/images/email/update_now3.png", border: "0"), "http://" + person.domain + "/payment_preferences"), style: "text-align: center;")
  end

  def payment_method_strings(person)
    methods = []
    if person.preferred_credit_card?
      methods << "credit card ending in ************#{person.preferred_credit_card.last_four}"
    end
    methods << "PayPal account" if person.preferred_paypal_account?
    methods << "TuneCore balance" if person.pay_with_balance?
    methods
  end
end
