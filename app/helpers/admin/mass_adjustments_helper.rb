module Admin::MassAdjustmentsHelper
  def display_categories
    BalanceAdjustmentCategory
      .select(:id, :name)
      .where
      .not(
        name: [
          "Refund - Other",
          "Refund - Renewal",
          "Service Adjustment",
          "Songwriter Royalty",
          "YouTube MCN Royalty"
        ]
      )
      .order(:id)
      .pluck(:name)
  end
end
