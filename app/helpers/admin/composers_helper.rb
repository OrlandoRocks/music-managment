module Admin::ComposersHelper
  def ineligible_composer_pro_for_pub?(composer)
    !PerformingRightsOrganization
      .eligible_publishing_pro_ids
      .include?(composer.performing_rights_organization_id.to_i)
  end
end
