module Admin::TranslationsHelper
  AVAILABLE_LANGUAGE_CODES = ["de", "en_au", "en_ca", "en_gb", "fr", "it"]

  def show_translation_diffs(file_name, file_lines, translation_files, language_code)
    translation_file_name = file_name.gsub("en.yml", "#{language_code}.yml")
    string1 = file_lines.join("\n")
    if translation_files[translation_file_name]
      string2 = translation_files[translation_file_name].join("\n")
      if Diffy::SplitDiff.new(string1, string2).left.present?
        "
        <tr>
          <td>
            <table>
              <caption>#{file_name}</caption>
              <thead>
                <tr>
                  <th>English</th>
                  <th>#{language_code}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style='text-align:left;padding-left:10px;width:50%;'>#{Diffy::SplitDiff.new(string1, string2, context: 0, format: :html).left}</td>
                  <td style='text-align:left;padding-left:10px;width:50%;'>#{Diffy::SplitDiff.new(string1, string2, context: 0, format: :html).right}</td>
                </tr>
              <tbody>
            </table>
          <td>
        </tr>".gsub("<del>", "").gsub("</del>", "")
      else
        ""
      end
    else
      "<tr style='padding:10px;'><td style='text-align:left;padding-left:10px;color:red;' colspan='2'>File Missing: #{translation_file_name}</td></tr>"
    end
  end

  def is_valid_language_code?(language_code)
    AVAILABLE_LANGUAGE_CODES.include?(language_code)
  end

  def valid_language_codes
    html_result = "<ul>"
    AVAILABLE_LANGUAGE_CODES.each do |language_code|
      html_result += "<li>#{language_code}</li>"
    end
    html_result += "</ul>"
    html_result.html_safe
  end
end
