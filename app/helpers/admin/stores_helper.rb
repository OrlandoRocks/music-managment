module Admin::StoresHelper
  def admin_store_class(admin_store)
    "store-" + case [admin_store.is_active, admin_store.in_use_flag]
               when [true, true]
                 "active-in-use"
               when [false, true]
                 "inactive-in-use"
               when [false, false]
                 "inactive-not-in-use"
               else
                 "active-not-in-use"
               end
  end
end
