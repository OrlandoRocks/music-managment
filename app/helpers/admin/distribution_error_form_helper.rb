module Admin::DistributionErrorFormHelper
  def decorated_distro_error_form(distro_error_form)
    DistributionErrorFormDecorator.new(distro_error_form)
  end
end
