module Admin::AdvertisementsHelper
  def set_country_links(current_country_website)
    links = []
    CountryWebsite.all.each do |cw|
      links << link_to_unless(
        cw.country == current_country_website.country,
        cw.full_country_name,
        { action: action_name, controller: controller_name, country: cw.country }
      )
    end
    links.join(" | ").html_safe
  end
end
