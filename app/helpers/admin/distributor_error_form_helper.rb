module Admin::DistributorErrorFormHelper
  def decorated_distributor_errors_form(dist_error_form)
    ::DistributorErrorFormDecorator.new(dist_error_form)
  end
end
