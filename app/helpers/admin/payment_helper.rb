module Admin::PaymentHelper
  def decorated_transfer(transfer)
    PaypalTransferDecorator.new(transfer)
  end

  def highlight_new_bank_account(eft_batch_transaction)
    eft_batch_transaction.flag_new_bank_account? ? "suspicious" : nil
  end

  def paypal_auto_approval_eligibility(transfer)
    transfer.transfer_metadatum.present? ? "Yes" : "No"
  end

  def paypal_auto_approval_status(transfer)
    return "N/A" if transfer.transfer_metadatum.blank?

    ActiveRecord::Type::Boolean.new.cast(transfer.transfer_metadatum.auto_approved) ? "Yes" : "No"
  end
end
