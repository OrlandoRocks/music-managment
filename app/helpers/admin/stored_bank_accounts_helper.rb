module Admin::StoredBankAccountsHelper
  def pending_total(bank_account_id)
    number_to_currency(
      EftBatchTransaction.where(
        [
          "stored_bank_account_id = ? AND status IN (?)",
          bank_account_id,
          EftBatchTransaction::CANCELABLE_STATUSES
        ]
      ).sum(&:amount)
    )
  end

  def processing_total(bank_account_id)
    number_to_currency(
      EftBatchTransaction.where(
        [
          "stored_bank_account_id = ? AND status IN (?)",
          bank_account_id,
          ["sent_to_bank", "processing_in_batch"]
        ]
      ).sum(&:amount)
    )
  end

  def failed_total(bank_account_id)
    number_to_currency(
      EftBatchTransaction.where(
        [
          "stored_bank_account_id = ? AND status IN (?)",
          bank_account_id,
          EftBatchTransaction::FAILED_STATUSES
        ]
      ).sum(&:amount)
    )
  end

  def success_total(bank_account_id)
    number_to_currency(
      EftBatchTransaction.where(
        [
          "stored_bank_account_id = ? AND status IN (?)",
          bank_account_id,
          "success"
        ]
      ).sum(&:amount)
    )
  end
end
