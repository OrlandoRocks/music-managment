module Admin::YoutubeSrMailTemplateHelper
  def markup_buttons_header
    raw(
      YoutubeSrMailTemplate::DEFAULT_HEADER_FIELDS.map do |field|
        { name: field.to_s.humanize, replaceWith: "{{ #{field} }}" }
      end.to_json
    )
  end
end
