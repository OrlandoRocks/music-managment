module Admin::LocksHelper
  def locked_item_link(item)
    case item
    when Album
      link_to h(item.name), controller: "admin/albums", action: "show", id: item.id
    else
      raise ArgumentError, "do not know how to display a locked item of #{item.class.name}"
    end
  end

  def locking_entity_link(item)
    case item.locked_by
    when Person
      "Person::#{item.locked_by.id} - #{item.locked_by.name}"
    when Invoice
      link_to "Invoice::#{item.locked_by.id}", admin_person_invoice_path(item.locked_by.person_id, item.locked_by)
    when PetriBundle
      "PetriBundle::#{item.locked_by.id}"
    when nil
      "STALE_LOCK! #{item.locked_by_id}::#{item.locked_by_type}"
    else
      raise ArgumentError, "do not know how to display a locking entity of #{item.locked_by.class.name}"
    end
  end
end
