module Admin::ReportHelper
  include Admin::AlbumsHelper

  def percent(count, total)
    percent = ((count.to_f / total.to_f) * 100).to_f
    number_to_percentage(percent, { precision: 2 })
  end

  def number_and_percent(count, total, single = true)
    if single
      "<b>#{count}</b>  ( #{percent(count, total)} )".html_safe
    else
      "#{count} &nbsp; <i>#{percent(count, total)}</i>".html_safe
    end
  end

  def display_invoice_pages
    ret = ""
    if @invoices.total_entries > @invoices.per_page
      ret << '<div style="text-align: right; border-top: 1px dotted black; padding-top: 3px;">'
      ret << "Jump to page:"
      ret << will_paginate(
        @invoices,
        params: { term: params[:term], state: @state }
      )
      ret << "</div>"
    end
    ret
  end

  def available_reports_selections
    Tunecore::Reports::Base.available_reports.map { |name| [name.titleize, name] }
  end
end
