module Admin::PreorderHelper
  def preorder_purchase_type(preorder_purchase)
    if preorder_purchase.itunes_enabled && preorder_purchase.google_enabled && preorder_purchase.salepoint_preorder_data.by_store("iTunesWW").present? && preorder_purchase.salepoint_preorder_data.by_store("Google").present?
      "iTunes and Google"
    elsif preorder_purchase.itunes_enabled && preorder_purchase.salepoint_preorder_data.by_store("iTunesWW").present?
      "iTunes Only"
    elsif preorder_purchase.google_enabled && preorder_purchase.salepoint_preorder_data.by_store("Google").present?
      "Google Only"
    else
      "No preorder enabled"
    end
  end
end
