module Admin::EftBatchTransactionHelper
  def eft_approve_reject_links(eft)
    return unless eft.can_be_canceled?

    if eft.can_be_approved?
      output = "(#{link_to('Approve', approve_admin_eft_batch_transaction_path(eft), title: 'show_details')}"
    else
      output = "(#{if eft.can_be_unapproved?
                     link_to('Change to Pending', remove_approval_admin_eft_batch_transaction_path(eft), title: 'show_details')
                   end}"
    end
    output << " |  #{link_to('Reject', { controller: 'admin/eft_batch_transactions', action: :reject_form, minWidth: 460, minHeight: 500, id: eft.id }, class: 'lightbox', title: 'reject')})"
    output
  end

  def eft_batch_txn_field_header(text, param)
    if eft_batch_txn_search_params[:search].nil? || eft_batch_txn_search_params.dig(:search, :order) == "#{param} DESC"
      next_sort_order = "#{param} ASC"
    else
      next_sort_order = "#{param} DESC"
    end

    link_params = eft_batch_txn_search_params.clone
    link_params[:search] = eft_batch_txn_search_params[:search].clone if eft_batch_txn_search_params[:search]
    link_params[:search] ||= {}
    link_params[:search][:order] = next_sort_order
    link_params[:page] = params[:page]
    link_params[:action] = controller.action_name

    link_to(text, link_params, class: "sort-link #{eft_batch_txn_search_sort_class_helper(param)}")
  end

  def eft_batch_txn_search_sort_class_helper(param)
    search_order = eft_batch_txn_search_params.dig(:search, :order)

    return if search_order.nil?

    return "ascending" if search_order == "#{param} ASC"
    return "descending" if search_order == "#{param} DESC"
  end

  def eft_batch_txn_search_params
    params.permit(
      :per_page,
      :commit,
      search: [
        :person_status,
        :person_email,
        :person_name,
        :artist_name,
        :id,
        :order,
        :order_id,
        :eft_status,
        :eft_batch_id,
        :eft_query_id,
        :amount_min,
        :amount_max,
        :created_at_min,
        :created_at_max
      ]
    )
  end
end
