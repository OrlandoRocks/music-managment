module Admin::SalepointSongsHelper
  def display_genre(salepoint_song)
    display = salepoint_song.primary_genre
    display += "<br/>(#{salepoint_song.secondary_genre})" if salepoint_song.secondary_genre
    display.html_safe
  end

  def display_account_name(salepoint_song)
    if salepoint_song.account_status == "Suspicious"
      "<span class=\"user_status_suspicious\">#{salepoint_song.account_name} (Suspicious)</span>".html_safe
    else
      salepoint_song.account_name
    end
  end

  def display_primary_artists(salepoint_song)
    salepoint_song.prim_artists_names.split(",").map(&:strip).uniq.join(", ")
  end
end
