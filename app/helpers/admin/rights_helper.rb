module Admin::RightsHelper
  def admin_role_checkbox_class(admin_role)
    if admin_role.name == "Admin"
      "admin-role-checkbox"
    elsif admin_role.is_administrative?
      "other-administrative-role-checkboxes"
    else
      "other-role-checkboxes"
    end
  end

  def admin_role_list_item_class(admin_role)
    if admin_role.name == "Admin"
      "admin-role"
    elsif admin_role.is_administrative?
      "other-administrative-roles"
    else
      "other-roles"
    end
  end

  def should_display_initially?(selected_roles, admin_role)
    admin_role.name == "Admin" || !admin_role.is_administrative? || selected_roles["Admin"]
  end
end
