module Admin::TransactionsHelper
  def format_decimal(arg, with_dollar_sign = true)
    if with_dollar_sign
      sprintf("$%.2f", arg)
    else
      sprintf("%.2f", arg)
    end
  end

  def paypal_action_options_for_select(default = "Sale")
    actions = PaypalTransaction::VALID_ACTIONS
    options = ["All", actions[:sale], actions[:authorization], actions[:void], actions[:refund], "Authorization & Void"]
    options_for_select(options, default)
  end

  def user_can_refund?(has_role, transaction)
    show_legacy_refunds? && transaction.can_refund? && has_role
  end
end
