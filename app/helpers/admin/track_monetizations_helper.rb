module Admin::TrackMonetizationsHelper
  def display_track_genre(track_monetization)
    display = track_monetization.album.primary_genre.name
    if track_monetization&.album&.secondary_genre&.name
      display += "<br/>(#{track_monetization.album.secondary_genre.name})"
    end

    display.html_safe
  end

  def display_track_account_name(track_monetization)
    if track_monetization.song.person.status == "Suspicious"
      "<span class=\"user_status_suspicious\">#{track_monetization.song.person.name} (Suspicious)</span>".html_safe
    else
      track_monetization.song.person.name
    end
  end

  def display_track_primary_artists(track_monetization)
    track_monetization.album.artists.map(&:name).map(&:strip).uniq.join(", ")
  end

  def display_track_secondary_artists(track_monetization)
    secondary_artists = track_monetization.album.creatives.where.not(role: "primary_artist")

    secondary_artists.present? ? secondary_artists.map(&:name).map(&:strip).uniq.join(", ") : "None"
  end

  def display_alert_msg_for(store_id)
    if store_id == Store::SOUNDCLOUD_BLOCK_ID
      "Are you sure you want to remove blocking from this track?
      If blocking is removed, other users will be able to upload this track to their SoundCloud page"
    else
      "Are you sure you want to apply a takedown to this track monetization?"
    end
  end
end
