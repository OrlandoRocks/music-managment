module Admin::ForeignExchangePeggedRatesHelper
  def price_preview_products(current_pegged_rate, updated_pegged_rate)
    Product.where(country_website_id: current_pegged_rate.country_website_id, price: (1..)).distinct(:price).limit(2).map do |country_website_product|
      {
        name: country_website_product.name,
        usd_price: format_to_currency(country_website_product.price.to_money, currency: "USD"),
        pegged_previous_price: money_to_currency(country_website_product.price.to_money, currency: current_pegged_rate.currency, pegged_rate: current_pegged_rate.pegged_rate),
        pegged_updated_price: money_to_currency(country_website_product.price.to_money, currency: current_pegged_rate.currency, pegged_rate: (updated_pegged_rate || current_pegged_rate.pegged_rate).to_f)
      }
    end
  end
end
