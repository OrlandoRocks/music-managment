module Admin::CertBatchesHelper
  include Admin::AlbumsHelper

  def display_cert_batch_pages
    ret = ""
    if @cert_batches.total_entries > @cert_batches.per_page
      ret << '<div style="text-align: right; border-top: 1px dotted black; padding-top: 3px;">'
      ret << "Jump to page:"
      ret << will_paginate(
        @cert_batches,
        params: { term: params[:term], sort_column: @sort_column }
      )
      ret << "</div>"
    end
    ret.html_safe
  end
end
