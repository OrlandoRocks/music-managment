module Admin::EftBatchHelper
  def eft_batch_count_link(eft_batch, status_query = nil)
    if status_query.nil?
      link_to eft_batch.eft_batch_transactions.size, { :controller => "admin/eft_batch_transactions", :action => "index", "search[eft_batch_id]" => eft_batch.id }
    else
      link_to eft_batch.eft_batch_transactions.count { |ebt| ebt.status == status_query },
              { :controller => "admin/eft_batch_transactions", :action => "index", "search[eft_batch_id]" => eft_batch.id, "search[eft_status]" => status_query }
    end
  end
end
