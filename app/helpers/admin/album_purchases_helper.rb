module Admin::AlbumPurchasesHelper
  def purchase_paid(purchase)
    if purchase.paid?
      "on #{fmt_country_date(purchase.paid_at)}"
    else
      "no"
    end
  end

  def purchase_total(purchase)
    if purchase.cost_cents.nil?
      "N/A"
    else
      money_to_currency(purchase.purchase_total)
    end
  end

  def purchase_num_items(purchase)
    return "N/A" unless purchase.respond_to?(:purchase_items)
    # purchase.purchase_items.count
  end

  def purchase_invoice_link(purchase)
    case
    when !purchase.invoice_id?
      "N/A"
    when purchase.invoice_id?
      link_to(purchase.invoice_id, admin_person_invoice_path(purchase.invoice.person, purchase.invoice))
    else
      purchase.ready_to_purchase? ? "No - checkout" : "Not Ready."
    end
  end

  def purchase_base_cost(purchase)
    if purchase.cost_cents.nil?
      "N/A"
    else
      money_to_currency(purchase.cost)
    end
  end

  def purchase_price_calculator_name(purchase)
    price_calculator = purchase.price_calculator rescue nil
    price_calculator ? price_calculator.class.name.gsub(/^PriceCalculator::/, "") : "Unknown"
  end

  def purchase_discount(purchase)
    if purchase.cost_cents.nil?
      "N/A"
    else
      money_to_currency(purchase.discount) << (purchase.has_discounts? ? " (#{purchase.discount_details})" : "")
    end
  end

  def purchaseable_id(purchase_item)
    "#{purchase_item.purchaseable_type}::#{purchase_item.purchaseable_id}"
  end

  def purchaseable_name(purchase_item)
    case (p = purchase_item.purchaseable)
    when Album
      h(p.name)
    when Song
      h(p.name)
    when Salepoint
      h(p.store.name) rescue "Store seems to have been deleted"
    when AnnualRenewal
      "Annual Renewal Fee"
    when nil
      "Stale: the item has been deleted."
    end
  end

  def purchaseable_icon(purchase_item)
    case (p = purchase_item.purchaseable)
    when Song, AnnualRenewal
      h(purchase_item.purchaseable_type)
    when Salepoint
      h(p.store.abbrev) rescue "Salepoint"
    when Album
      if (Album.find(p.id).songs.length == 1)
        "Single"
      else
        "Album"
      end
    end
  end

  def purchaseable_item_name(purchase_item)
    case (p = purchase_item.purchaseable)
    when Album
      if (Album.find(p.id).songs.length == 1)
        "single"
      else
        "album"
      end
    when Song
      "song"
    when AnnualRenewal
      "renewal"
    when Salepoint
      "store"
    end
  end

  def purchaseable_policy(purchase_item)
    if p = purchase_item.purchaseable
      "#{p.price_policy.short_code}/#{p.price_policy.price_calculator}"
    else
      "N/A"
    end
  end
end
