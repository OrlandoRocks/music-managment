module Admin::RenewalBatchTransactionsHelper
  def display_transaction_status(transaction_detail)
    status =
      if matching_transaction?(transaction_detail)
        case transaction_detail.matching
        when PersonTransaction
          "Success" # PersonTransaction is always successful
        when BraintreeTransaction, PaypalTransaction, PaymentsOSTransaction
          transaction_detail.matching.status
        end
      else
        "Returned to Customer"
      end

    case status
    when /success/i
      content_tag(:span, status, class: "success")
    when /(error|Returned to Customer|fraud|declined)/i
      content_tag(:span, status, class: "errored")
    else
      status
    end
  end

  def display_transaction_person(transaction_detail)
    if matching_transaction?(transaction_detail)
      html_content = content_tag(
        "td",
        link_to(
          transaction_detail.matching.person.name,
          admin_person_path(transaction_detail.matching.person)
        )
      )
      html_content << content_tag("td", transaction_detail.matching.person.id)
    else
      html_content = content_tag("td")
      html_content << content_tag("td")
    end
  end

  def batch_transaction_format_money(amount, payment_batch)
    amount.to_money(payment_batch.currency).format
  end

  def display_invoice_settlement_date(invoice)
    invoice.settled? ? fmt_country_datetime(invoice.settled_at) : "not settled" unless invoice.nil?
  end

  def options_for_transaction_types
    [
      "All",
      "PaypalTransaction",
      "BraintreeTransaction",
      "PaymentsOSTransaction",
      "PersonTransaction",
      "None"
    ].map(&:freeze).freeze
  end

  private

  def matching_transaction?(transaction_detail)
    !transaction_detail.matching.nil?
  end
end
