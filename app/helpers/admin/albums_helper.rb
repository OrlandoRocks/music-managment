module Admin::AlbumsHelper
  def display_album_name(album)
    parts = [h(album.name)]
    parts << %Q(<br/><span class='ancillary'>UPC: #{album.upc}</span>)
    parts << %Q(<br/><span class='ancillary'>TCUPC: #{album.tunecore_upc}</span>) if album.optional_upc.present?
    parts.join("")
  end

  def display_yes_or_no?(attribute)
    attribute ? "Yes" : "No"
  end

  def permissioned_distribution_options
    options = SongDistributionOption.all_options.keys
    options.delete(:free_song) unless @user_can_mark_song_as_free
    options
  end

  def song_upload_date(song, _user)
    if song.upload.nil?
      content_tag :span, "never", style: "color: red;"
    elsif song.upload.created_at.nil?
      "created_at is nil"
    else
      song.upload.created_at.to_s(:db)
    end
  end

  def song_on_server?(song)
    if song.upload && FileTest.exist?(File.join(Rails.root, UPLOAD_DIR, song.upload.uploaded_filename))
      true
    else
      false
    end
  end

  def admin_song_status(song, _user)
    if song.uploaded? && song.last_errors.blank?
      "Status : Uploaded"
    elsif song.last_errors.nil?
      "Status : Waiting for upload"
    else
      "Status : Upload Error</dd><dd>Reported Error : #{song.last_errors}"
    end
  end

  def crt_friendly_status(status)
    ret =
      case status
      when "NEEDS REVIEW"
        "<span class ='reviewstate nr'>N</span> Needs Review"
      when "APPROVED"
        "<span class ='reviewstate approved'>A</span> Approved"
      when "REJECTED"
        "<span class ='reviewstate denied'>D</span> Denied"
      when "NOT SURE"
        "<span class ='reviewstate ns'>?</span> Not Sure"
      when "FLAGGED"
        "<span class ='reviewstate suspicious'>S</span> Suspicious"
      when "DO NOT REVIEW"
        "<span class ='reviewstate'>X</span> Do Not Review"
      when "STARTED REVIEW"
        "<span class ='reviewstate sr'>S</span> Started Review"
      end
    ret.blank? ? "" : ret.html_safe
  end

  def display_unpaid_artist_warning?(album)
    additional_artist_product_ids = Product.where(name: "Additional Primary Artist").map(&:id)
    carted_additional_artists = Purchase.exists?(
      [
        "person_id = ? AND invoice_id is ? AND  product_id IN (?)",
        album.person_id,
        nil,
        additional_artist_product_ids
      ]
    )

    last_review_audit = ReviewAudit.where(album_id: album.id).last
    album.legal_review_state == "DO NOT REVIEW" && last_review_audit&.event == "REJECTED" && carted_additional_artists
  end
end
