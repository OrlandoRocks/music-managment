module Admin::TargetedOffersHelper
  def country_select(form)
    if @targeted_offer.targeted_people.count != 0 || @targeted_offer.targeted_products.count != 0
      form.select(:country_website_id, CountryWebsite.all.collect { |cw| [cw.country, cw.id.to_s] }, { label: "Country Website", required: true, contextual_help: "You cannot change the country website after products or people have been assigned", force_display: true }, { disabled: true })
    else
      form.select(:country_website_id, CountryWebsite.all.collect { |cw| [cw.country, cw.id.to_s] }, { label: "Country Website", required: true })
    end
  end

  def offer_type_select
    offer_type_display = {
      "new" => "New Customers",
      "existing" => "Existing Customers",
      "country_targeted" => "Country Targeted Offer"
    }

    TargetedOffer.offer_types.keys.map do |offer_type|
      [
        offer_type_display[offer_type],
        offer_type
      ]
    end
  end
end
