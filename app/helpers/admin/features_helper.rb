# frozen_string_literal: true

module Admin::FeaturesHelper
  def feature_section_title(country_website_name)
    # Website name for Tunecore US is Tunecore.
    # So This is needed for segregation of the US site from the global site
    return "TuneCore US" if country_website_name == "TuneCore"

    country_website_name
  end

  def disclaimer_text
    " Removing a feature flipper from the UI here will remove it from Redis,
    but a code change is still required to actually remove the feature from the application"
  end

  def valid_remove_request?(feature)
    percentage = Integer(feature.percentage)
    (percentage == 100 || percentage.zero?) && feature.users.empty?
  end

  def show_legacy_refunds?
    !current_user.new_refunds_feature_on?
  end
end
