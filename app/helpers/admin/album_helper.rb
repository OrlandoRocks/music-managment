module Admin::AlbumHelper
  # add logic to return based on who is looking at this

  def finalize_link(album, link_text, parenths = false, lead_text = "")
    link =
      if album.review_audits.empty? || album.last_review_state != "APPROVED"
        link_to link_text, { action: "finalize", id: album.id }, method: :post
      else
        "<a href='#finalize-popup' class='open-finalize-popup'>#{link_text}</a>"
      end

    (parenths ? "#{lead_text} (#{link})" : "#{lead_text} #{link}").html_safe
  end

  def album_locked?(_album)
    false
  end

  def note_created_name(note, album)
    if note.note_created_by_id == album.person_id && note.note == "Album was taken down"
      "Customer: #{note.note_created_by_name}"
    else
      note.note_created_by_name
    end
  end

  def show_songwriter_remixer_and_producer(creative, song)
    roles = ""
    creative.creative_song_roles.includes(:song_role).where(song_id: song).order(:song_role_id).each do |c|
      roles += "(S) " if c.song_role.role_type == "songwriter"
      roles += "(P) " if c.song_role.role_type == "producer"
      roles += "(R)"  if c.song_role.role_type == "remixer"
    end
    roles
  end

  # links for the albums pages
  def edit_album_path_helper(_album)
    edit_admin_person_album_path(@album.person_id, @album)
  end

  def unfinalize_link_text
    "Unfinalize (Finalized #{@album.finalized_at.to_s(:db)})"
  end

  def show_curated_artist_view?(distro, person)
    distro.is_itunes? &&
      distro.has_curated_artist_flags? &&
      person.has_role?(Role::CURATED_ARTIST)
  end

  def audio_quality_color(audio_quality)
    file_type_ranking = S3Detail::FILE_TYPE_ORDER.fetch(audio_quality, 0)
    file_type_ranking > 2 ? "red" : ""
  end

  def song_audio_quality(song)
    bits_per_sample = bits_per_sample(song)
    "#{song.s3_asset.s3_detail.file_type.upcase} #{bits_per_sample&.positive? ? bits_per_sample : ''}"
  end

  def bits_per_sample(song)
    song.s3_asset.s3_detail.metadata_json.dig("streams", 0, "bits_per_sample")
  end

  def original_review_state(album)
    album.review_audits.where.not(event: ReviewAudit::STARTED_REVIEW).last&.event
  end

  def display_album_state(album)
    album.state == "ready to go" && album.is_live? ? "live, missing information" : album.state
  end
end
