module Admin::DashboardHelper
  TOOL_REMOVAL_NOTICE_TITLE =
    "This tool will be removed soon, if this is still used, please reach out to Neil/Ace.".freeze

  def link_to_with_removal_notice(name, path, options = {})
    capture do
      concat link_to(name, path, options)
      concat " "
      concat content_tag(
        :i,
        ",",
        class: "fa fa-exclamation-triangle",
        style: "color:red",
        title: TOOL_REMOVAL_NOTICE_TITLE
      )
    end
  end
end
