module Admin::EftQueryHelper
  def eft_query_count_link(eft_query, status_query = nil)
    if status_query.nil?
      link_to eft_query.eft_batch_transactions.size, { :controller => "admin/eft_batch_transactions", :action => "index", "search[eft_query_id]" => eft_query.id }
    else
      link_to eft_query.eft_batch_transactions.count { |ebt| ebt.status == status_query },
              { :controller => "admin/eft_batch_transactions", :action => "index", "search[eft_query_id]" => eft_query.id, "search[eft_status]" => status_query }
    end
  end
end
