module Admin::InvoicesHelper
  include ::InvoicesHelper
  def invoice_purchase_link(purchase)
    case (r = purchase.related)
    when Product
      purchase.product.name
    else
      raise ArgumentError, "don't know how to link to a #{r.class.name}"
    end
  end
end
