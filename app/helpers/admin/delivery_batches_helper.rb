module Admin::DeliveryBatchesHelper
  def batch_joined_unprocessed_percent(batch)
    ((batch.joined_count - batch.processed_count) / batch.max_batch_size.to_f) * 100
  end

  def batch_processed_percent(batch)
    (batch.processed_count / batch.max_batch_size.to_f) * 100
  end
end
