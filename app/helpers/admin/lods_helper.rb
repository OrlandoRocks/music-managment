module Admin::LodsHelper
  def show_days_since_sent(lod)
    "(#{distance_of_time_in_words_to_now(lod.last_status_at)} ago)" if lod.status_text == "Sent"
  end
end
