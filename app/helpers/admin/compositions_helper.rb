module Admin::CompositionsHelper
  def formatted_response_msg(composition, error_msg)
    if error_msg
      "<span class='error'>#{error_msg}</span>".html_safe
    else
      "<span class='success'>Successfully sent #{composition.name} to Rights App</span>".html_safe
    end
  end

  def editable?(composition)
    composition.provider_identifier.blank? || composition.recordings.empty? || composition.recordings.exists?(recording_code: nil)
  end

  def publishing_composition_recording(composition)
    composition_recording(composition)
  end

  def composition_recording(composition)
    composition.recordings.blank? ? composition.recordings.build : composition.recordings.first
  end

  def legal_document_signed_date(account)
    account.person.legal_documents&.last&.signed_at
  end

  def pub_admin_status(composer)
    composer ? ::PublishingAdministration::ComposerStatusService.new(composer).state : "N/A"
  end
end
