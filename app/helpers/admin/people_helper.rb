module Admin::PeopleHelper
  def credits_remaining_sentence(person)
    inventories = CreditUsage.all_available(person)
    temp_array = []
    if !inventories.empty?
      inventories.each do |inventory|
        temp_array << pluralize(inventory.total_left, inventory.inventory_type)
      end
      temp_array.to_sentence
    else
      "none"
    end
  end

  def current_tax_form_status(person)
    statuses = []
    statuses << taxform_submission_status(person)
    statuses << taxform_validation_status(person)
    statuses << withholding_status(person)
    statuses.compact.join(" - ")
  end

  def last_submitted_tax_form(person)
    latest_tax_forms = person.current_tax_forms_across_programs
    return (latest_tax_forms.w9.last || latest_tax_forms.last) if person.from_united_states_and_territories?

    latest_tax_forms.last
  end

  def taxform_submission_status(person)
    tax_form = last_submitted_tax_form(person)

    return "Not Submitted" if tax_form.blank? || (person.from_united_states_and_territories? && !tax_form.w9?)

    "Submitted #{tax_form.submitted_at.strftime('%m/%-d/%y')} - #{tax_form.tax_form_type.kind}"
  end

  def taxform_validation_status(person)
    tax_form = last_submitted_tax_form(person)

    if person.from_united_states_and_territories?
      return "Form Required" unless tax_form

      "Incorrect Type" unless tax_form&.w9?
    else
      "Form Not Required" unless tax_form
    end
  end

  def withholding_status(person)
    return unless person.from_united_states_and_territories?
    return if last_submitted_tax_form(person)&.w9?
    return "Withholding Applied" if total_earnings(person) >= TaxFormCheckService::TAXABLE_THRESHOLD

    "Withholding not applied since total income is less than $#{TaxFormCheckService::TAXABLE_THRESHOLD}"
  end

  def total_earnings(person)
    TaxReports::RevenueReportService
      .new(person.id, Date.current.year)
      .generate_revenue_stats
      .reduce(0) { |acc, (_, v)| acc += v[:earnings]; acc }
  end

  def suspicious_reason(person)
    reason = person.lock_reason
    return if reason.blank?

    if person.blocked_from_distribution_flag
      "(#{reason} - Will Not Distribute)"
    else
      "(#{reason})"
    end
  end

  def plan_date_format(date)
    date.strftime(" %d %b %Y").html_safe if date
  end

  def plan_history(user_id)
    PersonPlanHistory.where(person_id: user_id).last
  end
end
