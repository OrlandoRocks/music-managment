module Admin::UserQueriesHelper
  def tables_pulldown(pd_tables, options = {})
    output = ""
    table_selected = options[:table_selected]
    options.delete(:table_selected)
    logger.debug("table_selected: #{table_selected} || table_selected.classify.tableize: #{table_selected.classify.tableize}")
    pd_tables.each do |table|
      logger.debug("table: #{table}")
      table_selected.classify.tableize.eql?(table) ? options[:selected] = "selected" : options.delete(:selected)
      options[:value] = table
      table_option = content_tag("option", table, options)
      output << table_option
      logger.debug "option: #{table_option}" if options[:selected].eql?("selected")
    end
    output
  end

  def columns_pulldown(all_cols, index, options)
    pd_columns = all_cols[index.to_i] if all_cols[index.to_i].present?
    output = ""
    column_selected = options[:column_selected]
    options.delete(:column_selected)
    pd_columns.each do |column|
      column_selected.eql?(column) ? options[:selected] = "selected" : options.delete(:selected)
      options[:value] = column
      output << content_tag("option", column, options)
    end
    output
    output
  end
end
