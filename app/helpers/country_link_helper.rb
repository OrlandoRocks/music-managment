module CountryLinkHelper
  def link_to_blog_page(country_website)
    {
      "US" => "https://www.tunecore.com/blog",
      "CA" => "https://www.tunecore.ca/blog",
      "UK" => "https://www.tunecore.co.uk/blog",
      "AU" => "https://www.tunecore.com.au/blog",
      "DE" => "https://www.tunecore.de/blog",
      "FR" => "https://www.tunecore.fr/blog",
      "IT" => "https://www.tunecore.it/blog",
      "IN" => "https://www.tunecore.in/blog",
    }[country_website]
  end

  def link_to_help_page(country_website)
    {
      "US" => "//help.tunecore.com/app/home",
      "CA" => "//ca.help.tunecore.com/app/home",
      "UK" => "//uk.help.tunecore.com/app/home",
      "AU" => "//au.help.tunecore.com/app/home",
      "DE" => "//de.help.tunecore.com/app/home",
      "FR" => "//fr.help.tunecore.com/app/home",
      "IT" => "//it.help.tunecore.com/app/home",
      "IN" => "//in.help.tunecore.com/app/home",
    }[country_website]
  end

  def link_to_contact_us_page(country_website)
    {
      "US" => "//help.tunecore.com/app/answers/detail/a_id/72/",
      "CA" => "//ca.help.tunecore.com/app/answers/detail/a_id/112",
      "UK" => "//uk.help.tunecore.com/app/answers/detail/a_id/426",
      "AU" => "//au.help.tunecore.com/app/answers/detail/a_id/657",
      "DE" => "//de.help.tunecore.com/app/answers/detail/a_id/948",
      "FR" => "//fr.help.tunecore.com/app/answers/detail/a_id/1256",
      "IT" => "//it.help.tunecore.com/app/answers/detail/a_id/1332"
    }[country_website]
  end

  def link_to_pub_admin_help_page(country_website)
    {
      "US" => "//pub.help.tunecore.com/app/home",
      "CA" => "//pub.help.tunecore.com/app/home",
      "UK" => "//pub.help.tunecore.com/app/home",
      "AU" => "//pub.help.tunecore.com/app/home",
      "DE" => "//de.help.tunecore.com/app/answers/list/kw/publishing/search/1",
      "FR" => "//fr.help.tunecore.com/app/answers/list/kw/publishing/search/1",
      "IT" => "//it.help.tunecore.com/app/answers/list/kw/publishing/search/1"
    }[country_website]
  end
end
