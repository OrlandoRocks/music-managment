module TaxFormRequestHelper
  # Returns true if:
  #   - This is the first time the person is withdrawing since submitting their most
  #     recent tax forms (no transfers have been made after the last tax form was submitted).
  #   - The person has an approved Payoneer account with a valid tax form.
  #   - The person's revenue meets the taxable threshold.
  #   - The person hasn't already set their revenue streams.
  #
  # Requires the secondary_taxform_submission feature flag.
  def should_show_secondary_tax_form_request_prompt?(person: nil)
    person ||= current_user
    return false unless features_for_secondary_tax_form_request_prompt_enabled?(person: person)

    person.payout_provider&.approved? &&                    # Approved payout prov.
      person.default_revenue_streams? &&                    # Default RS
      person.valid_tax_forms? &&                            # Valid tax form
      person.met_taxable_threshold? &&                      # Met taxable threshold
      !person.withdrawals_since_last_tax_form_submitted?    # No recent withdrawals?
  end

  private

  def features_for_secondary_tax_form_request_prompt_enabled?(person: nil)
    FeatureFlipper.show_feature?(:secondary_taxform_submission, person) &&
      FeatureFlipper.show_feature?(:tax_form_mapping, person)
  end
end
