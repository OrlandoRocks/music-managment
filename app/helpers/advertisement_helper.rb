module AdvertisementHelper
  def display_ad_for_person(_person, category, options = {})
    ads = Advertisement.for_person(current_user, category, options)
    render_ads(ads, options)
  end

  def session_for_person(_person, category, options = {})
    ads = Advertisement.for_person(current_user, category, options)
    render_ads(ads, options)
  end

  def ad_expiration_date(ad)
    "Offer ends #{fmt_long_date(ad.expiration_date)}." if ad.expiration_date
  end

  def product_price_from_action_link(action_link)
    product_id = action_link.scan(/\d+/).first
    advertised_product = Product.find(product_id)
    Product.set_targeted_product_and_price(current_user, advertised_product)
    money_with_discount(
      advertised_product.original_price,
      advertised_product.adjusted_price,
      advertised_product.currency
    )
  end

  private

  def render_ads(ads, options)
    return if ads.empty?

    case options[:display_size]
    when Advertisement::DISPLAY_SIZE_SIDEBAR, nil
      render(partial: "advertisements/sidebar", locals: { ads: ads })
    when Advertisement::DISPLAY_SIZE_WIDE
      render(partial: "advertisements/wide", locals: { ads: ads })
    when Advertisement::DISPLAY_SIZE_THUMBNAIL
      render(partial: "advertisements/thumbnail", locals: { ads: ads })
    end
  end

  def targeted_offer_cms_content(person)
    targeted_offers = TargetedOffer.for_person(person)
    html_out = ""
    targeted_offers.each do |targeted_offer|
      if targeted_offer.include_sidebar_cms?
        puts "\n\n#{targeted_offer.sidebar_cms_page_name}\n\n"
        html_out += (render_cms_content(targeted_offer.sidebar_cms_page_name) || "")
      end
    end
    "<div id='targeted-offer-sidebar'>#{html_out}</div>".html_safe
  end
end
