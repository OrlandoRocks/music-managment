module VideosHelper
  def video_description(video)
    if video.video_type == "FeatureFilm"
      if video.definition == "HD"
        custom_t("helpers.videos.high_definition_hd")
      else
        custom_t("helpers.videos.standard_definition_sd")
      end
    elsif video.video_type = "MusicVideo"
      if video.length < (6 * 60)
        custom_t("helpers.videos.thirty_seconds_to_six_minutes")
      elsif video.length < (12 * 60)
        custom_t("helpers.videos.six_to_twelve_minutes")
      elsif video.length < (21 * 60)
        custom_t("helpers.videos.twelve_to_twenty_minutes")
      elsif video.length < (31 * 60)
        custom_t("helpers.videos.twenty_to_thirty_minutes")
      else
        custom_t("helpers.videos.longer_than_thirty_minutes")
      end
    else
      custom_t("helpers.videos.unknown_type")
    end
  end

  def truncated_video_name_video(video)
    (video.name.size > 25) ? video.name[0..24] + "..." : video.name
  end
end
