# frozen_string_literal: true

LINKABLE_SERVICES = %w[apple spotify].freeze

module ExternalServiceIdHelper
  def build_album_links(album)
    album.external_service_ids
         .where(service_name: LINKABLE_SERVICES)
         .sort_by(&:service_name)
         .flat_map { |external| send("#{external.service_name}_link".to_sym, external.identifier, album) }
  end

  def apple_link(identifier, album)
    ret = []
    ret << { title: "iTunes", link: "http://itunes.apple.com/album/id#{identifier}?ls=1&app=itunes" }
    ret << { title: "Apple Music", link: "http://itunes.apple.com/album/id/#{identifier}" } if album.apple_music
    ret
  end

  def spotify_link(identifier, _)
    [
      { title: "Spotify URL", link: "https://open.spotify.com/album/#{identifier}" },
      { title: "Spotify URI", link: "spotify:album:#{identifier}" }
    ]
  end
end
