module TwoFactorAuthHelper
  include TwoFactorAuthable

  def notification_message(tfa)
    {
      "sms" => custom_t(:we_sent_code),
      "call" => custom_t(:we_are_calling),
      "app" => custom_t(:please_check_app)
    }[tfa.notification_method]
  end

  def formatted_phone_number(phone_number)
    phone_number.slice(-4, 4)
  end

  def authentication_partial(auth)
    "two_factor_auth/authentications/#{auth.tfa_action}_form"
  end

  def preference_partial(auth)
    "two_factor_auth/preferences/#{auth.tfa_action}_form"
  end

  def should_show_tfa_prompt?
    return false unless current_user
    return false if under_admin_control?
    return false unless two_factor_feature_enabled?
    return false unless current_user.is_verified?
    return false if current_user.dashboard_state == "virgin"
    return true if current_user.two_factor_auth_prompt.blank?

    current_user.two_factor_auth_prompt.active?
  end

  def set_gon_two_factor_auth_prompt
    default_prompt_time = 5.seconds.from_now

    tfa_prompt = current_user.two_factor_auth_prompt || current_user.create_two_factor_auth_prompt
    gon.push(tfaPromptAt: tfa_prompt.prompt_at || default_prompt_time) if tfa_prompt.active?
  end

  def set_tfa_gon_variables(person, auth_action, redirect_url, poll_api_endpoint)
    gon.push(
      {
        pollForPushNotificationAuth: person.two_factor_auth.uses_app_notification?,
        restartPollMessage: custom_t("recheck_for_push_authentication"),
        successRedirectUrl: redirect_url,
        pollApiEndpoint: poll_api_endpoint,
        authAction: auth_action
      }
    )
  end

  def two_factor_authentication_title(page)
    if page.split("_").last == "withdrawal"
      custom_t("my_account.withdraw_my_money")
    else
      custom_t("my_account.account_settings")
    end
  end

  def tfa_enrollment_error(error)
    return unless error

    error_text = {
      "invalid_verification_code" => custom_t(:wrong_code),
      "no_pending_verifications" => custom_t("tfa_form.authentication_step.no_pending_verifications")
    }[error]

    error_text || error
  end

  def toll_fraud_validation_enabled?
    FeatureFlipper.show_feature?(:toll_fraud_validation, current_user)
  end
end
