# frozen_string_literal: true

#
#  2009-06-29 -- MJL -- Changing the next step helpers prompts to ignore variable pricing becuase we are now forcing selection on the distribution page.
#
# [2010-05-26 -- CH]
# refactoring to use new purchase state and purchase options, see methods album_distribition_ui and album_purchase_display_state

module AlbumHelper
  include ApplicationHelper
  include ExternalServiceIdHelper
  include HolidayHelper
  include ProductHelper
  include DistributionHelper
  include StoreAutomatorHelper

  def renewal_messaging(album)
    renewal = Renewal.renewal_for(album)
    return unless renewal

    if !renewal.unlimited?
      extension_products = Product.extension_list(current_user, album, false)
      status = AlbumRenewalStatus.status?(album)
      if status
        render(
          partial: "album/#{status}",
          locals: {
            price: Renewal.renewal_price_for(album),
            expiration_date: renewal.expires_at,
            album: album,
            renewal: renewal,
            extension_products: extension_products,
            auto_renew: current_user.autorenew?
          }
        )
      end
    else
      render(partial: "shared/unlimited_renewal_message", locals: { album: album })
    end
  end

  def album_type_name_path(album)
    album.class.name.downcase
  end

  def album_type_name_display(album)
    custom_t("#{album.class.name.downcase}_display".to_sym)
  end

  def delete_release_label(release)
    if release.single?
      custom_t(".delete_this_single")
    else
      custom_t(".delete_this_album", album_type_name: album_type_name_display(release))
    end
  end

  def album_language(album)
    LanguageCode.find_by(code: album.language_code_legacy_support)
  end

  def display_primary_genre(release)
    release.primary_genre&.subgenre? ? release.primary_genre.parent_genre.name : release.primary_genre&.name
  end

  def display_secondary_genre(release)
    release.secondary_genre&.subgenre? ? release.secondary_genre.parent_genre.name : release.secondary_genre&.name
  end

  def display_primary_subgenre(release)
    return unless release.present? && release.primary_genre&.subgenre?

    release.primary_genre&.name
  end

  def display_secondary_subgenre(release)
    return unless release.present? && release.secondary_genre&.subgenre?

    release.secondary_genre&.name
  end

  def checklist_item(link_text, link, completed = false, css = [], icon = "fa-circle-o")
    if completed
      css << "status_complete"
      css << "checked"
      icon = "fa-check-circle-o"
    end
    render(partial: "album/checklist_item", locals: { link_text: link_text, link: link, css: css, icon: icon })
  end

  def album_overview(album)
    name = custom_t("#{album_type_name_path(album)}_overview".to_sym)
    link = album_path(album)

    checklist_item(name, link, false, ["non_status"], "")
  end

  def album_info_status(album)
    checklist_item(custom_t(:details), get_polymorphic_edit_path(album), album.valid?)
  end

  def album_subscription_status(album)
    checklist_item(custom_t(:my_subscription), subscription_path(album), false)
  end

  def album_store_status(album)
    if FeatureFlipper.show_feature?(:freemium_flow, current_user)
      checklist_item(custom_t(:stores_and_discovery_platforms), album_salepoints_path(album), album.has_stores?)
    else
      checklist_item(custom_t(:stores_display), album_salepoints_path(album), album.has_stores?)
    end
  end

  def album_artwork_template_info(salepoint, album)
    link = edit_album_salepoint_artwork_template_path(album, salepoint)

    if !salepoint.has_chosen_template?
      checklist_item(custom_t(:on_demand_template), link, false)
    elsif album.finalized? && salepoint.has_chosen_template?
      nil
    else
      checklist_item(custom_t(:on_demand_template), link, true)
    end
  end

  def album_artwork_status(album)
    css = []
    css << "thick-border" if FeatureFlipper.show_feature?(:distro_flow_changes, current_user)

    checklist_item(custom_t(:artwork_display), album_artwork_path(album), album.has_artwork?, css)
  end

  def album_artwork_high_quality?
    !@album.has_artwork? || @album.artwork.high_quality?
  end

  # This should be overriden in a distributable specific module...
  def album_track_info_status(album)
    if album.instance_of?(Ringtone)
      ringtone_track_upload(album)
    elsif album.instance_of?(Single)
      single_track_upload(album)
    elsif album.instance_of?(Album)
      album_track_upload(album)
    else
      raise "undetermined album type"
    end
  end

  def album_preorder_status(album)
    item = checklist_item(raw("#{custom_t(:preorder)} <span class=\"secondary-font-lt\" style=\"font-weight: normal; font-size: 14px; margin-left: 7px;\">(#{custom_t(:optional)})</span>"), "#{album_path(album)}#preorder-section", (album.preorder_purchase && album.preorder_purchase.enabled?), ["preorder-checklist"])
    unless !album_preorder_disabled(album) &&
           (album.type_supports_preorder? && !album.finalized?) ||
           (album.finalized? && album.preorder_purchase && album.preorder_purchase.paid_at?)

      return
    end

    item
  end

  def album_track_upload(album)
    link   = album_path(album, anchor: "mysongs")
    status = album_songs_status(album)

    css = ["songs-status"]
    css << "thick-border" unless FeatureFlipper.show_feature?(:distro_flow_changes, current_user)
    checklist_item(custom_t(:songs_display), link, status, css)
  end

  def single_track_upload(album)
    link   = single_path(album, anchor: "file-upload")
    status = album_songs_status(album)
    css = ["songs-status"]
    css << "thick-border" unless FeatureFlipper.show_feature?(:distro_flow_changes, current_user)

    checklist_item(custom_t(:upload_word), link, status, css)
  end

  def ringtone_track_upload(album)
    link = ringtone_path(album, anchor: "file-upload")
    checklist_item(custom_t(:upload_word), link, album.has_tracks?)
  end

  def album_purchase_total(album)
    Product.price(album.person, album)
  end

  def display_distribution_ui(partial:, locals:, addons:, addon_locals:, **_opts)
    render(
      partial: partial,
      locals: locals.merge(addons: addons.map { |addon| render(partial: addon, locals: addon_locals) })
    )
  end

  def album_distribution_ui(album)
    validate_credit_usage(current_user, album)
    distro_view_hash = Album::DistributionUIService.call(album)
    # special case bypasses refactored view logic
    if distro_view_hash[:display_state] == :not_ready_no_credit
      return display_ad_for_person(current_user, album.class.name)
    end

    display_distribution_ui(distro_view_hash)
  end

  def freemium_flow_enabled?
    FeatureFlipper.show_feature?(:freemium_flow, current_user)
  end

  def validate_credit_usage(current_user, album)
    credit_usage = CreditUsage.credit_usage_for(current_user, album)
    unless credit_usage.present? &&
           Purchase.where(related_id: credit_usage.id, related_type: CreditUsage.to_s.freeze).blank?

      return
    end

    credit_usage.destroy!
  end

  def create_purchase(to_purchase)
    if to_purchase.is_a?(Array)
      Product.add_to_cart(current_user, to_purchase)
    else
      Product.add_to_cart(current_user, to_purchase) unless Inventory.has_inventory_for?(current_user, to_purchase)
    end
  end

  def album_store_info_status(album)
    album.salepoints.select(&:requires_template?).map { |salepoint| album_artwork_template_info(salepoint, album) }.join.html_safe
  end

  def album_status_info(album)
    status_display = custom_t("helpers.album.album_status_#{album.status}")
    content_tag(:span, class: "#{album.status}_tag", id: "album_status_tag") do
      content_tag(:strong) do
        status_display
      end
    end
  end

  def album_status(album)
    if album.takedown?
      render(partial: "album/album_status_locked", locals: { album: album })
    elsif album.state == "legacy"
      render(partial: "album/album_status_legacy", locals: { album: album })
    elsif album.state == "sent"
      render(partial: "album/album_status_finalized", locals: { album: album })
    elsif album.state == "live"
      render(partial: "album/album_status_live", locals: { album: album })
    elsif album.finalized? && album.state != "sent" && album.state != "live"
      render(partial: "album/album_status_finalized", locals: { album: album })
    else
      render(partial: "album/album_status_in_progress", locals: { album: album })
    end
  end

  def song_status(song)
    if !song.album.finalized?
      link_text =
        if song.last_errors
          custom_t(:error_uploading)
        elsif song.upload.blank?
          custom_t(:upload_word)
        else
          custom_t(:uploaded)
        end
      link_to(link_text, album_songs_path(song.album))
    else
      custom_t(:uploaded)
    end
  end

  def album_monetizations_status(album)
    checked = @songs.present? && @songs.all?(&:ytm?)
    link = "/#{album_type_name_path(album)}s/#{album.id}/monetize"
    checklist_item(custom_t(:monetize_on_youtube), link, checked)
  end

  def suggested_date
    date = Date.today.to_time
    today = date.wday
    increment = 19 - today
    increment += 7 if increment <= 14
    date += increment.day
    holiday_delays.each do |holiday_delay|
      holiday_delay.each do |holiday_delay_day|
        date += 7.days if date == Time.parse(holiday_delay_day)
      end
    end

    date.iso8601
  end

  def album_purchased?(release)
    release.payment_applied
  end

  def get_youtube_monetization_price(_album)
    Product.find(Product::US_YTM)
           .price
           .to_money
  end

  def get_preorder_name(album)
    preorder = album.preorder_purchase
    if preorder && preorder.itunes_enabled && preorder.google_enabled
      "iTunes & Google"
    elsif preorder && preorder.itunes_enabled
      "iTunes"
    elsif preorder && preorder.google_enabled
      "Google"
    else
      ""
    end
  end

  def get_preorder_price(album, count = nil)
    Album::PreorderPricingService.call(album, count, current_user)
  end

  def preorder_pricing_display(album, preorder)
    if album.album_type == "Album" || (album.album_type == "Single" && album.song.duration && album.song.duration >= 600_000)
      start_date_price = preorder.salepoint.variable_price.price_by_country_website(current_user.country_website)
      preorder_price = preorder.preorder_price_display
      track_price = preorder.track_price_display
    elsif album.album_type == "Single"
      start_date_price = preorder.track_price_display
      preorder_price = preorder.track_price_display
    end
    render(
      partial: "album/preorder_pricing",
      locals: {
        album: album,
        track_price: track_price,
        start_date_price: start_date_price,
        preorder_price: preorder_price
      }
    )
  end

  def preorder_inputs_checked_or_disabled(input_type, album)
    case input_type
    when "none"
      "checked='checked'" if !album.preorder_purchase || !album.preorder_purchase.enabled?
    when "all"
      "checked='checked'" if album.preorder_purchase && album.preorder_purchase.enabled_preorder_data_count == 2
      if !album.has_itunes_ww_salepoint? || !album.has_salepoints_by_store?(Store.find_by(short_name: "Google"))
        "disabled='disabled'"
      end
    when "itunes"
      if album.preorder_purchase && album.preorder_purchase.itunes_enabled && !album.preorder_purchase.google_enabled
        "checked='checked'"
      end
      "disabled='disabled'" unless album.has_itunes_ww_salepoint?
    when "google"
      if album.preorder_purchase && album.preorder_purchase.google_enabled && !album.preorder_purchase.itunes_enabled
        "checked='checked'"
      end
      "disabled='disabled'" unless album.has_salepoints_by_store?(Store.find_by(short_name: "Google"))
    end
  end

  def preorder_label_grayed_out_msg(input_type, album)
    store = ""
    not_itunes = !album.has_itunes_ww_salepoint?
    not_google = !album.has_salepoints_by_store?(Store.find_by(short_name: "Google"))
    if not_google
      store = "Google Play"
    elsif not_itunes
      store = "iTunes"
    end

    case input_type
    when "all"
      custom_t("helpers.album.distribute_to_enable", store_name: store) if not_itunes || not_google
    when "itunes"
      custom_t("helpers.album.distribute_to_enable", store_name: "iTunes") if not_itunes
    when "google"
      custom_t("helpers.album.distribute_to_enable", store_name: "Google Play") if not_google
    end
  end

  def single_price_with_currency(person)
    return indian_product_price(ProductHelper::IN_SINGLE_ID) if person.india_user?

    price = Product.price(person, Single.new)
    money_to_currency(price)
  end

  def album_price_with_currency(person)
    return indian_product_price(ProductHelper::IN_ALBUM_ID) if person.india_user?

    price = Product.price(person, Album.new)
    money_to_currency(price)
  end

  def show_preorder?
    current_user.country_website.country_variable_prices.present?
  end

  def discography_source_label(release)
    "<h3>#{release_source_text(release)}</h3>".html_safe unless no_custom_source?(release)
  end

  def source_label(release)
    "<span>#{release_source_text(release)}</span>".html_safe unless no_custom_source?(release)
  end

  def release_source_text(release)
    "(#{custom_t(:from)} #{release.source.titleize})"
  end

  def no_custom_source?(release)
    !release.respond_to?(:source) || release.source.casecmp?("customer")
  end

  def has_itunes_links?(album)
    album.live_in_itunes? && album.class.name != "Ringtone"
  end

  def share_itunes_links_if_eligible(album, current_user)
    return unless has_itunes_links?(album) && tc_social_allowed_for?(current_user) && !album.takedown_at?

    display_itunes_links(album, current_user)
  end

  def display_itunes_links(album, current_user)
    render partial: "tc_social/share_itunes_links",
           locals: {
             album: album,
             button_text: tc_social_active_for?(current_user) ? custom_t(:share_with_social) : custom_t(:activate_social_and_share),
             itunes_link: ItunesLinkinator.itunes_url(album),
             apple_music_link: ItunesLinkinator.itunes_streaming_url(album)
           }
  end

  def select_years_for_selection
    (1900..Time.current.year + 1)
  end

  def get_go_live_date_attribute(album, attribute, default_val = nil)
    if album[:golive_date].is_a?(Hash)
      album[:golive_date][attribute] || default_val
    elsif album[:golive_date].respond_to?(:strftime)
      get_go_live_datetime_attribute(album, attribute) || default_val
    else
      default_val
    end
  end

  def get_go_live_datetime_attribute(album, attribute)
    case attribute
    when :hour
      album[:golive_date].strftime("%I").to_i
    when :meridian
      album[:golive_date].strftime("%p")
    else
      album[:golive_date].send(attribute)
    end
  end

  def today_day
    Time.now.day
  end

  def today_month
    Time.now.month
  end

  def today_year
    Time.now.year
  end

  def record_param_key(record)
    record.class.name.downcase.to_sym
  end

  def yt_music_flow_update_enabled?
    FeatureFlipper.show_feature?(:yt_music_flow_update, current_user)
  end

  def automator_check
    true unless session[:automator_checked] == false
  end

  def ytm_and_salepoints_form_title
    custom_t(:youtube_music_and_monetization)
  end

  def ytm_terms_and_conditions_text
    custom_t("carts.show.agree_youtube", url: "/index/terms")
  end

  def check_youtube_music_and_monetization_form?(_album)
    store_names = Ytsr::AlbumSalepointService::YT_STORE_NAMES
    current_user.ytm_in_cart?
  end

  def btn_class_for_explicit_on(single)
    "option-selected" if single.parental_advisory_enabled?
  end

  def btn_class_for_explicit_off(single)
    "option-selected" if single.parental_advisory_enabled? == false
  end

  def container_class_for_clean_selector(single)
    "hidden" if single.new_record? || (!single.new_record? && single.parental_advisory_enabled?)
  end

  def btn_class_for_clean_on(single)
    "option-selected" if single.clean_version_enabled?
  end

  def btn_class_for_clean_off(single)
    "option-selected" if single.clean_version_enabled? == false
  end

  def spotify_artists_enabled?
    FeatureFlipper.show_feature?(:spotify_artist_ids, current_user)
  end

  def add_artist_text
    spotify_artists_enabled? ? custom_t("creatives.creative_list.add_apple_or_spotify_id") : custom_t("creatives.creative_list.add_apple_or_itunes_id")
  end

  def apple_new_artist_enabled?
    FeatureFlipper.show_feature?(:new_apple_artist_id_creation, current_user)
  end

  def get_all_main_artists
    Creative
      .includes(:artist)
      .joins(:artist)
      .where(person_id: current_user)
      .order("artists.name")
      .distinct
      .pluck("artists.name")
  end

  def gonify_all_main_artists
    gon.mainArtistDropdown = get_all_main_artists
  end

  def scrub_artist_names
    params.dig(controller_name.singularize, :creatives)&.each do |creative|
      creative[:name] = Loofah.fragment(creative[:name]).text(encode_special_chars: false)
    end
  end

  def show_aod?(album)
    salepoints_redesign_feature_enabled? &&
      !album.is_a?(Ringtone) &&
      aod_enabled_in_region? &&
      !amazon_on_demand_disabled(album)
  end

  def aod_enabled_in_region?
    available_in_region?(country_website, "amazon_on_demand")
  end

  def song_copyrights_enabled_in_region?
    available_in_region?(country_website, "song_copyrights") && !cover_song_metadata_enabled?
  end

  def track_level_language_enabled?
    FeatureFlipper.show_feature?(:track_level_language, current_user)
  end

  def track_level_language_permitted?(album)
    SongDataPresenter.permitted_language_codes.exists?(id: album&.language&.id)
  end

  def explicit_song_radio_enabled?
    FeatureFlipper.show_feature?(:explicit_song_radio, current_user)
  end

  def single_clean_version_selector_enabled?
    FeatureFlipper.show_feature?(:single_clean_version_selector, current_user)
  end

  def clean_song_radio_enabled?
    FeatureFlipper.show_feature?(:clean_song_radio, current_user)
  end

  def song_start_time_form_enabled?
    FeatureFlipper.show_feature?(:tiktok_clip, current_user)
  end

  def dj_release_enabled?
    FeatureFlipper.show_feature?(:dj_mix_type, current_user)
  end

  def spatial_audio_enabled_and_allowed?
    spatial_audio_enabled? && (current_user.legacy_user? || plan_allows(Store::ITUNES_WW_ID))
  end

  def spatial_audio_enabled?
    FeatureFlipper.show_feature?(:spatial_audio, current_user)
  end

  def cover_song_metadata_enabled?
    FeatureFlipper.show_feature?(:cover_song_metadata, current_user)
  end

  def plan_allows(store_id)
    current_user.stores_from_plan.map(&:id).include?(store_id)
  end

  def metadata_language_code_id(params)
    language_code = params[:language_code_legacy_support] || params[:language_code]
    Airbrake.notify("language_code nil", params) if language_code.nil?
    LanguageCode.find_by(code: language_code)&.id
  end

  def get_distribution_progress_level(distro)
    return 0 unless distro

    [distro.has_artwork?, album_songs_status(distro), distro.has_stores?, distro.valid?].reduce(0) { |acc, elem| level = elem ? acc + 1 : acc }
  end

  def display_distribution_progress?
    @distribution_progress_level &&
      !(@album&.finalized? || @single&.finalized?) &&
      [AlbumController, SinglesController, ArtworkController, SalepointsController].include?(controller.class) &&
      FeatureFlipper.show_feature?(:flow_progress_bar, current_user)
  end

  def load_upc(release)
    release.optional_upc_number = release.upc.number
  end

  def load_upc_link
    if current_user.country_website.id == CountryWebsite::FRANCE
      @upc_label = "#{custom_t('album.album_fields.upc_ean_code')}</p><a style='font-size: 62%;' href='#{custom_t('album.album_fields.upc_info_link')}'>#{custom_t('album.album_fields.upc_info_link_text')}</a>"
    else
      @upc_label = custom_t("album.album_fields.upc_ean_code").to_s
    end
  end

  def reject_optional_upc_number?(release, params)
    return false unless release && params.key?(:optional_upc_number)

    (params[:optional_upc_number] == release.optional_upc_number) ||
      (params[:optional_upc_number] == release.tunecore_upc_number)
  end

  def reject_optional_upc(params)
    params.except(:optional_upc_number)
  end

  def set_original_release_date(params)
    return unless (params["previously_released"] == "false" || params["is_previously_released"] == false)

    params["orig_release_year"] = ""
  end

  def should_be_single_notification
    album = @album || @single
    return false unless album

    if album.strictly_facebook_release? || album.strictly_discovery_release?(current_user)
      custom_t(".should_be_freemium_single")
    else
      custom_t(".should_be_single", price: single_price_with_currency(current_user))
    end
  end

  def prepare_popup_translations(country_website)
    result = TranslationFetcherService
             .translations_for("javascripts/popup_modal", COUNTRY_LOCALE_MAP[locale.upcase.to_s])
             .translations["popup_modal"]
    result[:fb_terms]["reels_description"] = knowledgebase_link_for("reels-description", country_website)

    @popup_modal_translations = result
  end

  def create_esids(album, params)
    creatives = album.creatives.includes(:artist)
    ExternalServiceIds::CreationService.create_update_match(creatives, params)
  end

  def existing_plan_name
    return custom_t("plans.#{current_user.plan.name}.name") if current_user.plan

    return unless current_user.cart_plan

    "#{custom_t("plans.#{current_user.cart_plan.name}.name")} #{custom_t('album.existing_plan.in_cart')}"
  end

  def show_buy_plan?(release)
    @show_buy_plan ||= release.finalized? && current_user.can_use_store_expander? == false
  end

  def show_add_stores?(release)
    @show_add_stores ||= release.takedown_at.blank? && show_buy_plan?(release) == false
  end

  def purchaseable_salepoints_ready?(salepoints)
    salepoints.any? && salepoints.all?(&:has_chosen_template?)
  end

  def show_distributed_add_to_cart?(album:, automator:, current_user:, salepoints:)
    return false if album.takedown? || album.rejected?

    purchaseable_salepoints_ready?(salepoints) ||
      purchased_album_automator_eligible?(album, automator, current_user)
  end
end
