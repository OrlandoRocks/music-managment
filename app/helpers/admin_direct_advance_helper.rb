module AdminDirectAdvanceHelper
  def latest_report_month_earnings
    *monthly_earnings, _all_previous_earnings = @report.latest_report[:earnings]
    monthly_earnings
  end

  def report_person_suspicious?
    @report.person&.suspicious? || false
  end
end
