module RegionHelper
  def render_if_available_in_region(country = "US", product_key, source)
    render "products/shared/#{product_key}", locals: { page: source } if available_in_region?(country, product_key)
  end

  def available_in_region?(country, product_key)
    PRODUCT_REGIONS[country].include?(product_key)
  end

  def allow_braintree_credit_cards?(person)
    BRAINTREE_MERCHANT_IDS.with_indifferent_access.key?(person.user_currency)
  end

  def allow_bank_account?(person)
    person.country_domain == "US"
  end

  def product_url_for(brand, type)
    Tunecore::ProductUrl.generate(brand, type, country_website.downcase).html_safe
  end

  def all_unsanctioned_dropdown
    Country.unsanctioned.order(name: :asc).pluck(
      :name,
      :iso_code
    ).unshift([
                custom_t("people.people_fields.choose_country"),
                nil
              ])
  end

  def country_dropdown_for_website(country_website_id, code_attr = :iso_code, display_attr = :name, add_all = false)
    [
      grouped_primary_countries(country_website_id, code_attr, display_attr, add_all),
      grouped_other_countries(code_attr, display_attr)
    ]
  end

  def grouped_primary_countries(country_website_id, code_attr, display_attr, add_all)
    ["--------", primary_countries(country_website_id, code_attr, display_attr, add_all)]
  end

  def default_country_website?
    CountryWebsite::COUNTRY_WEBSITE_ID_MAP[country_website] == CountryWebsite::UNITED_STATES
  end

  def default_language_code
    I18n.locale.to_s[0..1]
  end

  def primary_countries(country_website_id, code_attr, display_attr, add_all)
    primary = add_all ? [all_option] : []
    primary + home_country_option(
      country_website_id,
      code_attr,
      display_attr
    ) + supported_countries_options(
      country_website_id,
      code_attr,
      display_attr
    )
  end

  def all_option
    [custom_t(:all), custom_t(:all).downcase]
  end

  def other_countries(code_attr, display_attr)
    all_countries
      .select { |c| c.country_website.nil? }
      .sort_by(&:name)
      .map { |c| [c.send(display_attr), c.send(code_attr)] }
  end

  def home_country_option(country_website_id, code_attr, display_attr)
    all_countries.select { |c|
      country_match_current_site?(c, country_website_id)
    }.map { |c| set_country_option_values(c, code_attr, display_attr) }
  end

  def supported_countries_options(country_website_id, code_attr, display_attr)
    all_countries.select { |c|
      country_dont_match_current_site?(
        c,
        country_website_id
      )
    }.sort { |a, b| a.country_website.id <=> b.country_website.id }.map { |c|
      set_country_option_values(
        c,
        code_attr,
        display_attr
      )
    }
  end

  def set_country_option_values(country, code_attr, display_attr)
    [country.send(display_attr), country.send(code_attr)]
  end

  def country_match_current_site?(country, country_website_id)
    !country.country_website.nil? && country.country_website.id == country_website_id
  end

  def country_dont_match_current_site?(country, country_website_id)
    !country.country_website.nil? && country.country_website.id != country_website_id
  end

  def grouped_other_countries(code_attr, display_attr)
    ["--------", other_countries(code_attr, display_attr)]
  end

  def all_countries
    @countries ||= Country.unsanctioned.includes(:country_website).all
  end

  def excluded_promos
    {
      "confliktarts" => ["UK", "FR", "DE", "IT"]
    }
  end

  def exclude_promo?(product_string, country)
    excluded_promos[product_string].include?(country)
  end

  def is_a_cyrillic_language?(code)
    ["ru", "uk"].include?(code)
  end

  def credit_and_paypal_available?(user)
    (allow_braintree_credit_cards?(user) || can_use_payments_os?) && user.allow_paypal?
  end

  def only_credit_available?(user)
    (allow_braintree_credit_cards?(user) || can_use_payments_os?) && !user.allow_paypal?
  end

  def customer_type_options
    Person.customer_types
  end
end
