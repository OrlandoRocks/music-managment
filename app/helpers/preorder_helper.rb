module PreorderHelper
  include CurrencyHelper

  def preorder_price_options
    country_variable_prices = CountryVariablePrice.album_variable_prices_by_person(current_user)
    country_variable_prices.map { |cvp| [cvp_display_price(cvp), cvp.variable_price_retail_price] }
  end

  def cvp_display_price(country_variable_price)
    price = country_variable_price.display_price
    cvp_currency = country_variable_price.currency
    user_currency = preorder_display_currency(country_variable_price)
    pegged_rate = ForeignExchangePeggedRate.current_rate(currency: user_currency)&.pegged_rate
    money_to_currency(price.to_money(cvp_currency), round_value: false, pegged_rate: pegged_rate)
  end

  def preorder_display_currency(country_variable_price)
    if country_variable_price.non_native_currency_site?
      country_variable_price.country_website.user_currency
    else
      country_variable_price.currency
    end
  end

  def display_wholesale_preorder_album_price?
    CountryVariablePrice.display_wholesale_preorder_album_price?(current_user)
  end

  def preorder_track_price_options
    country_variable_prices = CountryVariablePrice.track_variable_prices_by_person(current_user)
    country_variable_prices.map { |cvp|
      "<option value='#{cvp.variable_price_retail_price}'>#{cvp_display_price(cvp)}</option>"
    }.join("").html_safe
  end

  def show_currency_iso_code?(currency)
    !MONEY_CONFIG[currency][:suppress_iso]
  end
end
