module TrendsHelper
  def trend_song_title(record, user)
    record["name"] || user.albums.find { |album|
                        album.id == record["album_id"]
                      }.try(:songs).try(:find) { |song| song.id == record["id"] }.try(:name)
  end

  def trend_album_title(record, user, key = nil)
    value = record[key] || record["album_name"]
    album_key = record["album_id"] ? "album_id" : "id"
    value || user.albums.find { |album| album.id == record[album_key] }.try(:name)
  end

  def trend_artist_name(record, user)
    album_key = record["album_id"] ? "album_id" : "id"
    record["artist_name"] || user.albums.find { |album| album.id == record[album_key] }.try(:artist_name)
  end

  def trend_release_type(record, user)
    album_key = record["album_id"] ? "album_id" : "id"
    record["album_type"] || user.albums.find { |album| album.id == record[album_key] }.try(:album_type)
  end
end
