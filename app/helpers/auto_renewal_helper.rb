module AutoRenewalHelper
  def should_show_auto_renewal_prompt?(person: nil, skip_controller_check: false)
    if !skip_controller_check &&
       (params[:controller] == "people" && params[:action] == "renewal_billing_info")

      return false
    end

    person ||= current_user
    qualifies_for_modal =
      person.present? &&
      !cookies[:autorenew_remind_me_later] &&
      Country::UNITED_STATES_AND_TERRITORIES.exclude?(person.country_iso_code) &&
      person.has_upcoming_autorenewal?
    return false unless qualifies_for_modal

    !(Person::VatCompliance.new(person).complete? && person.self_billing_accepted?)
  end

  def first_upcoming_renewal_expires_at(person: nil)
    person ||= current_user

    person.first_upcoming_renewal_expires_at
  end
end
