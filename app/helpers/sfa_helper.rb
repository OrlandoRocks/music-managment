# frozen_string_literal: true

module SfaHelper
  def redirect_to_dashboard
    redirect_to dashboard_path
  end

  def amfa_enabled?
    service_enabled?(ExternalServiceId::APPLE_SERVICE, :apple_music_for_artists, "iTunesWW")
  end

  def spotify_enabled?
    service_enabled?(ExternalServiceId::SPOTIFY_SERVICE, :spotify_for_artists, "Spotify")
  end

  def amazon_enabled?
    FeatureFlipper.show_feature?(:amazon_artists, current_user) &&
      !FeatureBlacklist.exists?(person_id: current_user.id, feature: :amazon_artists,) &&
      !current_user.suspicious? &&
      current_user.stores.any? { |store| store.short_name == "Amazon" } &&
      current_user.has_store_distributions?("Amazon")
  end

  # TODO: simplify/optimize
  def service_enabled?(service, feature, store_name)
    store_id = Store.find_by(short_name: store_name).id
    query = ServiceForArtists::QueryBuilder.new(current_user.id, service)
    (feature.to_s == "apple_music_for_artists" || FeatureFlipper.show_feature?(feature, current_user)) &&
      !FeatureBlacklist.exists?(person_id: current_user.id, feature: feature) &&
      (query.artists_with_service_ids.any? || query.albums_with_service_ids.any?) &&
      current_user.salepoints.where(store_id: store_id).any?
  end

  def set_promotional_ticker(reskin: false)
    @tickers = []

    if spotify_enabled?
      @tickers << ticker_hash(
        "spotify",
        :spotify_banner_text,
        :spotify_for_artists,
        spotify_for_artists_path,
        reskin: reskin
      )
    end

    if amfa_enabled?
      @tickers << ticker_hash(
        "apple",
        :apple_banner_text,
        :apple_music_for_artists,
        apple_for_artists_path,
        reskin: reskin
      )
    end

    if show_yt_oac?
      @tickers << ticker_hash(
        "youtube-play",
        :youtube_oac_banner_text,
        :youtube_oac_music_for_artists,
        youtube_oac_path,
        reskin: reskin
      )
    end

    @tickers
  end

  private

  def ticker_item_text(text_key, reskin: false)
    if reskin
      custom_t("reskin.artist_accounts.#{text_key}")
    else
      custom_t("shared.ticker.#{text_key}")
    end
  end

  def ticker_hash(fa_icon, text_key, link_text_key, link_href, reskin: false)
    fa_html = "<i class='fa fa-#{fa_icon}'></i>"

    {
      icon: fa_html,
      text: ticker_item_text(text_key, reskin: reskin),
      link: {
        text: custom_t("shared.ticker.#{link_text_key}"),
        href: link_href
      },
      store: fa_icon
    }
  end
end
