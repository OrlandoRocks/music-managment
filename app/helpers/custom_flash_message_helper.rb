module CustomFlashMessageHelper
  def custom_flash_message(should_display, flash_msg_text, css_classes = [])
    return unless should_display

    render "shared/custom_flash_message", { flash_msg_text: flash_msg_text, style: format_class_names(css_classes) }
  end

  private

  def format_class_names(css_classes)
    "custom-flash-base " + css_classes.join(" ")
  end
end
