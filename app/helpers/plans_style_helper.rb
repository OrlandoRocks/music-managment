# frozen_string_literal: true

module PlansStyleHelper
  LEGACY_USER_COLOR_ID = 1

  PLAN_ID_COLOR_MAP = {
    Plan::NEW_ARTIST => "green",
    Plan::RISING_ARTIST => "blue",
    Plan::BREAKOUT_ARTIST => "red",
    Plan::PROFESSIONAL => "yellow",
  }.freeze

  def plan_color_var(ordinal)
    color = PLAN_ID_COLOR_MAP[color_id]

    "var(--plan-#{color}-#{ordinal})"
  end

  private

  def color_id
    current_user&.plan&.id || LEGACY_USER_COLOR_ID
  end
end
