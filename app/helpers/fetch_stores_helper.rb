module FetchStoresHelper
  def retrieve_song_url(esi)
    song_url = nil
    case esi.service_name
    when ExternalServiceId::APPLE_SERVICE
      album_identifier = @album_esi.where(service_name: ExternalServiceId::APPLE_SERVICE).last.identifier
      song_url = "#{ENV['APPLE_STORE_BASE_URL']}/album/#{album_identifier}?i=#{esi.identifier}"
    when ExternalServiceId::SPOTIFY_SERVICE
      song_url = "#{ENV['SPOTIFY_STORE_BASE_URL']}/track/#{esi.identifier}"
    end
    song_url
  end

  def retrieve_album_url(esi)
    album_url = nil
    case esi.service_name
    when ExternalServiceId::APPLE_SERVICE
      album_url = "#{ENV['APPLE_STORE_BASE_URL']}/album/#{esi.identifier}"
    when ExternalServiceId::SPOTIFY_SERVICE
      album_url = "#{ENV['SPOTIFY_STORE_BASE_URL']}/album/#{esi.identifier}"
    end
    album_url
  end

  def fetch_odesli_stores(stores_response_hash)
    stores_names_ids_combined = stores_response_hash.keys
    stores_names_ids_combined.map { |combined| combined.split("::").first.split("_").first.downcase }
  end

  def create_odesli_hash(available_store, response_body)
    stores_names_ids_combined_list = response_body["entitiesByUniqueId"].keys
    spotify_key = stores_names_ids_combined_list.find { |item| item.include?(available_store.upcase) }
    spotify_key_array = spotify_key.split("::")
    { name: spotify_key_array.first.split("_").first.downcase, id: spotify_key_array.last }
  end
end
