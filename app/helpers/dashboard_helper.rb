module DashboardHelper
  include KnowledgebaseLinkHelper

  def days_since_last_release(_person)
    "<h1>#{custom_t('helper.dashboard.your_releases')}</h1>".html_safe
  end

  def expired_renewal_modal_valid_for(person)
    has_renewal_to_renew = Renewal.renewals_within_date_range_by_person(person, person.renewals.first.created_at, Time.now - 45.days).present?
    Date.today <= ENV["RENEW_EXPIRED_RELEASE_BY"].to_date && has_renewal_to_renew
  end

  def renewal_due_for_album(album, renewals_cache)
    renewal = album.renewals.last
    expires_at = renewals_cache[album.id]
    return false if renewal.nil? || expires_at.nil?

    start_date = Time.current
    end_date = Time.current + Renewal::REMINDERS_LENGTH.weeks
    unpaid_renewal = renewal.takedown_at.nil? &&
                     renewal.canceled_at.nil? &&
                     expires_at >= start_date &&
                     expires_at <= end_date

    expires_soon = expires_at - 4.weeks <= Time.current
    unpaid_renewal && expires_soon && album.person.preferred_payment_account.nil?
  end

  def get_release_type_path(release_type, method, release = nil)
    entity = release_type.underscore.downcase
    case method
    when "new"
      public_send("new_#{entity}_path")
    when "edit"
      public_send("edit_#{entity}_path", release.id)
    when "show"
      public_send("#{entity}_path", release.id)
    end
  end

  def tc_accelerator_faq_link
    knowledgebase_link_for_article_id(KnowledgebaseLink::ACCELERATOR_FAQ_ARTICLE_ID, current_user.country_domain)
  end

  private

  def image_to_display(album)
    if album.artwork && album.artwork.last_errors.blank?
      image_tag(album.artwork.url(:small)).html_safe
    else
      image_tag(Artwork.default_url(:small)).html_safe
    end
  end

  def album_overlay_image(album, renewals_cache)
    if album.rejected?
      return %(<div class="flag"><div class="flag-inner flag-rejected">#{custom_t(:rejected)}</div></div>)
    end

    unless album.payment_applied
      return %(<div class="flag"><div class="flag-inner flag-incomplete">#{custom_t(:incomplete)}</div></div>)
    end

    # if the album has an unpaid renewal, and no renewal payment method (paypal or cc) && is within 28 of expiration
    return if !renewal_due_for_album(album, renewals_cache) || album.person.has_plan?

    %(<div class="flag"><div class="flag-inner flag-due">#{custom_t(:renewal_due)}</div></div>)
  end

  def album_url(album)
    album_path(album)
  end

  def release_cards
    if FeatureFlipper.show_feature?(:freemium_flow, current_user)
      render(partial: "dashboard/release_cards_freemium")
    else
      render(partial: "dashboard/release_cards")
    end
  end

  def rewards_program_tiers_phrase_key(tier, attr)
    custom_t("rewards_program.tiers.#{tier.slug}.#{attr}")
  end

  def plans_phrase_key(plan, attr)
    custom_t("plans.#{plan.name}.#{attr}")
  end

  def see_all_translation(polymorphic_type)
    custom_t("dashboard.see_all_#{polymorphic_type.downcase.pluralize}".to_sym)
  end
end
