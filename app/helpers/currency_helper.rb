module CurrencyHelper
  include ActionView::Helpers::NumberHelper
  include RegionHelper
  INDIAN_RUPEE = "INR".freeze

  # Return currency with the correct symbol based on money's currency
  # instead of locale.
  # This method is mainly used in conjunction with money class type that is currency aware.
  # e.g. sales reporting

  def format_to_currency(money, options = {})
    format_currency_string(money: money, options: options)
  end

  def money_to_currency(money, options = {})
    iso_code = options.delete(:iso_code)
    pegged_rate = options.delete(:pegged_rate)

    money_amount = money_amount_with_pegged_rate(money: money, iso_code: iso_code, pegged_rate: pegged_rate)
    options = options.merge(round_value: true) if current_user_indian? && options[:round_value] != false

    merged_options = options.merge({ currency: get_current_user_currency(options) })
    format_currency_string(money: money, money_amount: money_amount, options: merged_options)
  end

  def balance_to_currency(money, options = {})
    iso_code = options.delete(:iso_code)

    domain = current_user_present? ? current_user.country_domain : iso_code
    needs_conversion = available_in_region?(domain, "foreign_exchange_rates_balance_display")
    money_amount = needs_conversion ? money_amount_with_exchange_rate(money: money, iso_code: iso_code) : money&.amount
    merged_options = options.merge({ currency: get_current_user_currency(options) })
    format_currency_string(money: money, money_amount: money_amount, options: merged_options)
  end

  def needs_currency_conversion?(iso_code: nil, currency: nil)
    if current_user_present?
      return available_in_region?(current_user.country_domain, "foreign_exchange_rates_balance_display")
    end
    return true if iso_code == Country::INDIA_ISO && currency == CurrencyCodeType::INR

    false
  end

  def get_current_user_currency(options)
    return options[:currency]  if options[:currency]

    current_user.user_currency if current_user_present?
  end

  def is_whole_number?(amount)
    (amount % 1) == 0.0
  end

  def cents_to_dash(currency, delimiter)
    currency.gsub(/#{delimiter}[0]+/, "#{delimiter}-")
  end

  def sales_report_money_to_currency(money, options = {})
    config = { format: "%u%n" }.merge(options)
    return balance_history_money(money, config) if @multiple_currency_domain

    money_to_currency(money, config)
  end

  # super unit - Greater Denomination of Currency (E.g Dollars, Rupees)
  # sub unit - Lesser Denomination of Currency (E.g Cents, Paise)

  def currency_superunit_to_subunit(superunit)
    (100 * superunit.to_r).to_i
  end

  def currency_subunit_to_superunit(subunit)
    Rational(subunit.to_i, 100).to_f
  end

  #
  #  Outputs the currency ISO
  #
  #  e.g. USD, AUS, ENG, EUR
  #
  #   options:
  #
  #   suppress_iso:  turn off output of 3 letter ISO code
  #   - default: false
  #
  #   html: output html
  #   - default: true
  #
  def currency_detail(iso, config = MONEY_CONFIG[iso])
    return "" unless config
    return "" if config[:suppress_iso]
    return iso if config[:html] == false

    if config[:description]
      "<abbr class='currency' title='#{config[:description]}'>#{iso}</abbr>"
    else
      "<span>#{iso}</span>"
    end
  end

  def current_user_currency
    current_user.person_balance.currency
  end

  def locale_currency
    @locale_currency ||=
      if current_user.nil?
        country_website_record =
          if LOCALE_TO_COUNTRY_MAP[locale.to_s]
            CountryWebsite.find_by(country: LOCALE_TO_COUNTRY_MAP[locale.to_s])
          else
            CountryWebsite.first
          end

        country_website_record.currency
      else
        current_user_currency
      end
    @locale_currency
  end

  def locale_currency_symbol
    MONEY_CONFIG[locale_currency][:unit]
  end

  def currency_input_tag(model, model_name, main_currency_method, fractional_currency_method, options = {})
    options[:main_currency_css_classes] = "#{options[:main_currency_css_classes]} currency-input-main".strip
    options[:fractional_currency_css_classes] = "#{options[:fractional_currency_css_classes]} currency-input-fractional".strip

    default_options = {
      main_currency_id: "#{model_name}_main_currency",
      fractional_currency_id: "#{model_name}_fractional_currency",
      main_currency_name: "#{model_name}[#{main_currency_method}]",
      fractional_currency_name: "#{model_name}[#{fractional_currency_method}]"
    }

    locals = {
      model: model,
      model_name: model_name,
      main_currency_method: main_currency_method,
      fractional_currency_method: fractional_currency_method,
      options: default_options.merge(options)
    }

    render partial: "shared/currency_input_tag", locals: locals
  end

  def current_fx_rate_updated_at(format)
    rate = current_foreign_exchange_rate
    return if rate.blank?

    case format
    when :date
      rate.updated_at.strftime("%F")
    when :time
      rate.updated_at.strftime("%T%:z")
    end
  end

  def current_foreign_exchange_rate
    return unless current_user_present?

    @current_foreign_exchange_rate ||= ForeignExchangeRate.current_rate(iso: current_user.country_domain)
  end

  def format_vat_amount(vat_amount_cents)
    vat_amount = vat_amount_cents.to_f / 100.0
    format_currency_string(
      money: nil,
      money_amount: vat_amount,
      options: {
        iso_code: @person.country_domain,
        currency: @person.user_currency,
        round_value: false
      }
    )
  end

  def format_vat_rate(rate)
    number_to_percentage(rate, strip_insignificant_zeros: true)
  end

  def money_to_unformatted_amount(money, options = {})
    iso_code = options.delete(:iso_code)
    pegged_rate = options.delete(:pegged_rate)

    money_amount_with_pegged_rate(money: money, iso_code: iso_code, pegged_rate: pegged_rate).to_f
  end

  # Minor unit is the smallest unit of a currency, depending on the number of decimals.
  # For example: 10 GBP: GBP has two decimals, so in minor units submit an amount of 1000
  def money_to_minor_units(amount, currency)
    amount * (10**Money::Currency.new(currency).exponent)
  end

  private

  def money_amount_with_pegged_rate(money: nil, amount: nil, iso_code: nil, pegged_rate: nil)
    money_amount = amount || money&.amount
    return unless money_amount

    pegged_rate ||= current_pegged_rate_value(iso_code)

    (pegged_rate.present? && money.currency.iso_code == "USD") ? (money_amount * pegged_rate) : money_amount
  end

  def money_amount_with_exchange_rate(money: nil, amount: nil, iso_code: nil)
    money_amount = amount || money&.amount
    return unless money_amount

    exchange_rate_value = current_exchange_rate_value(iso_code)

    exchange_rate_value.present? ? (money_amount * exchange_rate_value) : money_amount
  end

  def current_pegged_rate_value(iso_code = nil)
    user_domain = current_user.country_domain if current_user_and_no_iso_code?(iso_code)
    country_domain = [iso_code, user_domain].compact.first
    return unless country_domain

    rate = ForeignExchangePeggedRate.current_rate(iso: country_domain)
    rate.pegged_rate if rate.present?
  end

  def current_exchange_rate_value(iso_code = nil)
    user_domain = current_user.country_domain if current_user_and_no_iso_code?(iso_code)
    country_domain = [iso_code, user_domain].compact.first
    return unless country_domain

    rate = ForeignExchangeRate.current_rate(iso: country_domain)
    rate.exchange_rate if rate.present?
  end

  def current_user_and_no_iso_code?(iso_code)
    current_user_present? && iso_code.nil?
  end

  def current_user_present?
    respond_to?(:current_user) && current_user.present?
  end

  def current_user_indian?
    (current_user_present? && current_user.country_website.india?) || (I18n.locale == :"en-in")
  end

  def format_currency_string(money: nil, money_amount: nil, options: {})
    money_amount ||= money&.amount
    amount = is_whole_number?(money_amount) ? money_amount.round : money_amount

    currency_key = options[:currency] || money&.currency&.iso_code
    config = MONEY_CONFIG.fetch(currency_key, {}).merge(options)

    # full_amount is used to render the amounts with precision beyond 2. Here is a link to the original PR, https://github.com/tunecore/tc-www/pull/279
    # For the India site, we want to render the converted INR amount, not the full_amount which for the India site is in USD.
    amount = options[:full_amount] if !current_user_indian? && options[:full_amount]

    if options[:round_value]
      config[:precision] = 0
      amount = amount.round
    end

    currency = number_to_currency(amount, config)

    currency = cents_to_dash(currency, ",") if I18n.locale == :de && is_whole_number?(money_amount)

    return inr_formatting(amount, config) if config[:currency] == INDIAN_RUPEE

    details = currency_detail(money&.currency, config)
    [currency, details].join(" ").strip.html_safe
  end

  # rubocop:disable Style/NumericPredicate
  def inr_formatting(amount, config)
    inr_formatting_rules = {
      south_asian_number_formatting: true,
      no_cents: config.fetch(:precision, nil) == 0,
      # this will not work now as there is a bug in the money gem. for now, the comma is being manually removed below. this code will work when the bug is fixed
      thousands_seperator: ((amount > 9999) ? "," : "")
    }
    inr_formatting_rules[:symbol] = false if config[:skip_inr_symbol]
    formatted_currency = Money.new(currency_superunit_to_subunit(amount), INDIAN_RUPEE).format(inr_formatting_rules)
    inr_formatting_rules[:thousands_seperator].present? ? formatted_currency : formatted_currency.delete(",")
  end
  # rubocop:enable Style/NumericPredicate

  def adjusted_price_as_money(product)
    product.adjusted_price.to_money(product.currency)
  end
end
