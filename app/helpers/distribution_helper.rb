module DistributionHelper
  def album_songs_status(album, user = nil)
    # for legacy usage - when no user provided
    # fallback on current_user if class implements it, otherwise use album.user
    user = respond_to?(:current_user) ? current_user : album.person if user.blank?
    if album.skip_songwriter_validations?
      album.has_tracks?
    else
      album_report = Album::CompletionReportBuilder.build(album.id, user.id)
      album_report.songs_complete?
    end
  end

  def filtered_distribution_products(album, user = current_user)
    if album.strictly_facebook_release? || album.strictly_discovery_release?(user)
      Product.list(user, album).select { |product| product.sort_order == 1 }
    else
      Product.list(user, album)
    end
  end

  # Use CreditUsage to determine if inventory is available for distributions since
  # it takes into account distributions which are currently in cart and allocated a credit
  def inventory?(item, user = current_user)
    class_name = item.convert_to_single? ? "Single" : item.class.name
    CreditUsage.credit_available_for?(user, class_name)
  end
end
