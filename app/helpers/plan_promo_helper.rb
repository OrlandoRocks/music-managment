# frozen_string_literal: true

module PlanPromoHelper
  def promo_button_content
    return custom_t("plans.upgrade") if current_user.plan_user?

    custom_t("plans.buy_a_plan")
  end

  def promo_header_content
    case person_plan_id
    when Plan::NEW_ARTIST
      custom_t("plans.new_artist.upgrade_cta")
    when Plan::RISING_ARTIST
      custom_t("plans.rising_artist.upgrade_cta")
    when Plan::BREAKOUT_ARTIST
      custom_t("plans.breakout_artist.upgrade_cta")
    else
      custom_t("plans.upgrade_cta")
    end
  end

  def person_plan_id
    current_user.plan&.id
  end
end
