# frozen_string_literal: true

module DashboardReskinHelper
  ALERT_TYPES = %i[
    error
    error_dialog
    persistent_alert
    message
    notice
    success
  ].freeze
  AUTOMATOR_UPSELL_PLAN_IDS = [1, 2].freeze

  #
  # Plans
  #
  def plan_display_name(plan)
    return unless plan

    custom_t("plans.#{plan.name}.name")
  end

  def show_upsell?(plan)
    return true if plan.nil?

    plan.id != Plan::PROFESSIONAL
  end

  def upgradeable_plan?(plan)
    plan.present? && show_upsell?(plan)
  end

  def plan_svg(plan)
    "/images/dashboard/plan_#{plan&.id}.svg"
  end

  def plan_upsell_btn_text(plan)
    return custom_t("plans.buy_a_plan") if plan.nil?

    custom_t("plans.upgrade")
  end

  def plan_upsell_msg(plan)
    return custom_t("plans.upgrade_cta") if plan.nil?

    custom_t("plans.#{plan.name}.upgrade_cta")
  end

  #
  # Payout
  #

  def payoneer_opted_out?
    Payoneer::FeatureService::payoneer_opted_out?(current_user)
  end

  def payoneer_enabled?
    Payoneer::FeatureService.payoneer_payout_enabled?(current_user) &&
      !payoneer_opted_out?
  end

  def needs_payoneer?
    payoneer_enabled? && current_user.payout_provider.blank?
  end

  def payout_link
    return account_settings_path(tab: "payoneer_payout") if needs_payoneer?

    account_settings_path(tab: "payoneer_payout")
  end

  def setup_payout?
    current_user.payout_provider.blank? ||
      current_user.payout_provider&.onboarding?
  end

  #
  # Alerts
  #

  def alerts?
    ALERT_TYPES.any? { |at| flash[at].present? }
  end

  #
  # Recent Releases
  #

  def create_release_helper(release_type)
    case release_type
    when :singles
      new_single_path
    when :full_albums
      new_album_path
    when :ringtones
      new_ringtone_path
    end
  end

  def visibility(release_type)
    return "active" if release_type == :singles
  end

  def display_artwork(release)
    if release.artwork && release.artwork.last_errors.blank?
      image_tag(release.artwork.url(:medium)).html_safe
    else
      image_tag(Artwork.default_url(:medium)).html_safe
    end
  end

  def release_status(release)
    result =
      case
      when release.takedown?
        :takedown
      when release.finalized?
        if release.approved?
          :sent
        elsif release.needs_review?
          :processing
        else
          :needs_action
        end
      else
        :incomplete
      end

    { msg: custom_t("reskin.release_states.#{result}"), value: result }
  end

  def new_release_path(release_type)
    case release_type
    when :full_albums
      new_album_path
    when :singles
      new_single_path
    when :ringtones
      new_ringtone_path
    end
  end

  #
  # Resources
  #

  def upsell_automator?
    plan_id = current_user.plan&.id || current_user.cart_plan&.id
    return true if plan_id.nil?

    plan_id.in?(AUTOMATOR_UPSELL_PLAN_IDS)
  end

  def distribution_credits_locals(person)
    {
      account_overview: Tunecore::AccountOverview.new(person, false),
      targeted_offers: TargetedOffer.for_person(person)
    }
  end

  #
  # Modals
  #

  def show_expiring_cc_modal?(days_until_expiration)
    days_until_expiration && days_until_expiration != "expired"
  end

  def expiring_cc_modal_locals(days_until_expiration)
    {
      variant: :expiring_cc,
      days_until_expiration: days_until_expiration,
      header: custom_t(".keep_the_lights_on"),
      message: custom_t(
        "dashboard.credit_card_expiring_soon_modal.payment_will_expire",
        num_days: days_until_expiration
      ),
      yes_btn: link_to(
        custom_t(".update_payment"),
        person_preferences_path,
        class: "reskin-btn center-h"
      ),
      no_btn: button_tag(
        custom_t("dashboard.credit_card_expiring_soon_modal.no_thanks"),
        class: "simplemodal-close center-h",
      )
    }
  end

  def autorenewals_modal_locals
    {
      variant: :autorenewals,
      header: custom_t("auto_renewal_prompt.heading"),
      message: custom_t(
        "auto_renewal_prompt.body",
        expires_at: l(
          first_upcoming_renewal_expires_at(person: current_user),
          format: :month_day_year
        )
      ),
      yes_btn: link_to(
        custom_t("auto_renewal_prompt.confirm_my_info_button"),
        renewal_billing_info_person_path(id: current_user.id),
        class: "reskin-btn center-h"
      ),
      no_btn: button_tag(
        custom_t("auto_renewal_prompt.remind_me_later_link"),
        class: "simplemodal-close center-h",
      )
    }
  end

  # Show the TFA opt-in prompt (not the TFA submission form).
  #
  # Mirrors legacy impl spanning:
  #  - app/assets/javascripts/two_factor_auth/prompt.js
  #  - app/helpers/two_factor_auth_helper.rb
  def show_tfa_modal?
    return false if under_admin_control?
    return false unless two_factor_feature_enabled?
    return false unless current_user.is_verified?

    prompt = current_user.two_factor_auth_prompt
    return false if prompt.blank?
    return false unless prompt.active?

    prompt.never_prompted? || prompt.prompt_at < Time.zone.now
  end

  def tfa_modal_locals
    {
      variant: :tfa,
      header: image_tag("/images/two_factor_auths/tfa_prompt.png"),
      message: "#{custom_t('shared.two_factor_auth.only_you_access_account')}" \
               "#{custom_t('shared.two_factor_auth.keep_your_money_and_account_safe')}",
      yes_btn: link_to(
        custom_t("shared.two_factor_auth.enroll_btn"),
        two_factor_auth_enrollments_path,
        class: "reskin-btn center-h"
      ),
      no_btn: button_tag(
        custom_t("shared.two_factor_auth.dismiss_btn"),
        class: "simplemodal-close center-h",
      )
    }
  end

  #
  # Widgets
  #
  def plan_widget_locals(plan)
    {
      is_plan_user: plan.present?,
      plan_name: plan_display_name(plan),
      plan_svg: plan_svg(plan),
    }
  end

  def plan_upsell_locals(plan)
    {
      show_upsell: show_upsell?(plan),
      upgradeable_plan: upgradeable_plan?(plan),
      upsell_message: plan_upsell_msg(plan),
      upsell_btn_text: plan_upsell_btn_text(plan),
    }
  end
end
