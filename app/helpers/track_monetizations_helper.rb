module TrackMonetizationsHelper
  def track_status(status)
    custom_t(status)
  end

  def can_monetize_fb_tracks?
    already_subscribed?("FBTracks")
  end

  def facebook_dashboard_accessible?
    redirect_to dashboard_path unless can_monetize_fb_tracks?
  end

  def youtube_dashboard_accessible?
    current_user.has_active_ytm? || already_subscribed?("YTTracks")
  end

  def already_subscribed?(subscription_name)
    current_user.has_active_subscription_product_for?(subscription_name)
  end

  def has_potentially_monetizable_songs?
    current_user.potentially_monetizable_songs.present?
  end

  def prompt_fb_tracks?
    on_dashboard? &&
      valid_user_state?("FBTracks")
  end

  def on_dashboard?
    params[:controller] == "dashboard" && params[:action] == "index"
  end

  def valid_user_state?(subscription_name)
    has_potentially_monetizable_songs? &&
      !already_subscribed?(subscription_name)
  end

  def yt_royalty?
    YouTubeRoyaltyRecord.date_range_of_royalties(current_user).select { |d| !d.nil? }.any?
  end
end
