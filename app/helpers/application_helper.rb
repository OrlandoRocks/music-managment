require "rss"

module ApplicationHelper
  include CurrencyHelper
  include CustomTranslationHelper
  include RegionHelper
  include CountryLinkHelper
  include KnowledgebaseLinkHelper
  include MediaDisplayNameHelper
  include CartPlanable
  include PersonFlags
  include PlansStyleHelper
  include ReskinHelper

  CONTROLLER_ACTIONS = %w[
    pricing
    why_tunecore
    how_it_works
    artists
    index
    sxtc
    sxsw
    friends
    press
    tunecore_logos
    distribution_credits
    sitemap
    get_more_from_youtube
    live
    message
  ]

  def error_messages_for(models, _options = {})
    return if models.blank?

    html = ""
    models_array = nil

    if models.kind_of?(Array)
      models_array = models
    else
      models_array = []
      models_array << models
    end

    models_array.each do |model|
      next unless model.errors.any?

      html += "<div id='errorExplanation' class='errorExplanation'>"
      html += "<ul>"

      model.errors.full_messages.each do |msg|
        html += "<li>#{msg}</li>"
      end

      html += "</ul>"
      html += "</div>"
    end
    html.html_safe
  end

  def note_helper(notetext, type, _opt = "")
    ret =
      case type
      when "red" ### Not Used! ###
        '<div class="message row"><div id="errorExplanation" class="seven columns centered"><span>' + notetext + "</span></div></div>"
      when "check"
        '<div class="message row"><div id="goodErrorExplanation" class="seven columns centered"><span>' + notetext + "</span></div></div>"
      else
        '<div class="message row"><div class="msg seven columns centered"><span>' + notetext + "</span></div></div>"
      end

    ret.html_safe
  end

  def note_error_helper(notetext, _type, _opt = "")
    ret = '<div class="message row"><div id="errorExplanation" class="seven columns centered"><span>' + notetext + "</span></div></div>"
    ret.html_safe
  end

  def persistent_alert_helper(notetext)
    ret = "<div class='message row'><div id='persistent-alert' class='twelve columns' style='#{if controller_name == 'index' && action_name != 'publishing_administration'
                                                                                                 'margin-top:120px;'
                                                                                               end}' ><span>#{notetext}</span></div></div>"
    ret.html_safe
  end

  def release_is_rejected?
    release = @album || @single
    release && release.rejected? && !release.takedown?
  end

  def error_dialog(msg, title)
    ret = '<div id="flashErrorDialog" title="' + (title || "Error") + '" class="hide">' + msg + "</div>"
    ret.html_safe
  end

  def order(text, param)
    key =
      if params[:search].nil? || params[:search][:order] == "#{param} DESC"
        "#{param} ASC"
      else
        "#{param} DESC"
      end

    link_params = params.clone
    link_params[:search] = params[:search].clone if params[:search]
    link_params[:search] ||= {}
    link_params[:search][:order] = key
    link_params[:page] = params[:page]
    link_params[:action] = controller.action_name

    link_to(text, link_params, class: "sort-link #{sort_class_helper(param)}")
  end

  def sort_class_helper(param)
    unless params[:search].nil?
      result = "ascending" if params[:search][:order] == "#{param} ASC"
      result = "descending" if params[:search][:order] == "#{param} DESC"
    end
    result
  end

  def th_order_by(text, order_by, current_order, options = {})
    asc = (current_order == order_by + " ASC")
    desc = (current_order == order_by + " DESC")
    order_by = asc ? order_by + " DESC" : order_by + " ASC"
    sort_class = "sorting"
    sort_class = "sorting_asc" if asc
    sort_class = "sorting_desc" if desc
    permitted_order_by_params = params.permit(:person_id)
    raw("<th class=\"#{sort_class} #{options[:class] || ''}\"><a href=\"?#{permitted_order_by_params.merge(order: order_by).to_query}\">#{text}</th>")
  end

  def html5_time(db_time)
    db_time.in_time_zone("UTC").strftime("%Y-%m-%dT%H:%M:%S")
  end

  def date(date)
    "<time datetime=\"#{html5_time(date)}\"> #{fmt_country_datetime(date)} </time>".html_safe
  rescue
    fmt_country_datetime(date).to_s
  end

  def time(_time)
    "DECIDE ON A TIME FORMAT"
  end

  def fmt_long_date_picker_preselected(date)
    try(:super, date) || fmt_long_date(date)
  end

  def method_missing(method_sym, *arguments, &block)
    if method_sym.to_s =~ /^fmt_(.*)$/
      return "unknown" if arguments.first.nil?

      l(arguments.first, format: $1.to_sym)
    else
      super
    end
  end

  def display_datetime(arg)
    arg.nil? ? "" : fmt_short_datetime(arg)
  end

  def country_name(iso_code)
    Country.find_by(iso_code: iso_code).name
  end

  def flash_uploader(button_text, file_text, file_extensions, fill_type, upload_to, call_after, song_id = "", song_info = "")
    "<object width=\"450\" height=\"45\" classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\">
      <param value=\"/flash/fu5.swf?button_text=#{CGI.escape(button_text)}&file_text=#{CGI.escape(file_text)}&extensions=#{CGI.escape(file_extensions)}&fill=#{fill_type}&upload_to=#{CGI.escape(upload_to)}&call_after=#{CGI.escape(call_after)}&song_id=#{CGI.escape(song_id)}&song_info=#{CGI.escape(song_info)}\" name=\"movie\"/>
      <param value=\"high\" name=\"quality\"/>
      <param name=\"wmode\" value=\"transparent\">
      <embed width=\"450\" height=\"45\" wmode=\"transparent\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" quality=\"high\" src=\"/flash/fu5.swf?button_text=#{CGI.escape(button_text)}&file_text=#{CGI.escape(file_text)}&extensions=#{CGI.escape(file_extensions)}&fill=#{fill_type}&upload_to=#{CGI.escape(upload_to)}&call_after=#{CGI.escape(call_after)}&song_id=#{CGI.escape(song_id)}&song_info=#{CGI.escape(song_info)}\"/>
    </object>".html_safe
  end

  def build_song_uploader(song, upload_server = UploadServer.find_random.upload_server_path, call_back = "upload_completed")
    file_types = "*.mp3;*.MP3;*.m4a;*.M4A;*.wav;*.WAV;*.flac;*.FLAC"
    url = upload_server + "?upload_id=#{song.id}&"

    if song.uploaded?
      flash_uploader("Choose New", "Song File Uploaded", file_types, "completed", url, call_back, song.id.to_s, song_upload_info(song))
    elsif song.last_errors
      flash_uploader("Choose New", "Choose a song file", file_types, "errored", url, call_back, song.id.to_s, song_upload_info(song))
    else
      flash_uploader("Browse", "Choose a song file", file_types, "none", url, call_back, song.id.to_s, song_upload_info(song))
    end
  end

  def build_song_uploader2(song, call_back)
    build_song_uploader(song, UploadServer.find_random.upload_server_path, call_back)
  end

  def song_upload_id
    Time.now.to_i.to_s + rand(1000).to_s
  end

  def song_upload_info(song, download_url = nil, audio_format = nil)
    info = {
      aws_access_key_id: AWS_ACCESS_KEY,
      aws_secret_access_key: AWS_SECRET_KEY_HEAD,
      s3_bucket: FLAC_BUCKET_NAME,
      s3_path: song.s3_orig_key_prefix + ".%{FILEEXT}", # 'my_uploads/file_123',
      notify_url_ok: "http://#{request.host}/upload/success_callback/#{song.id}?fileext=%{FILEEXT}&original_filename=%{FILENAME}&bitrate=%{BITRATE}&log=%{ERRORLOG}",
      notify_url_err: "http://#{request.host}/upload/failure_callback/#{song.id}?error=%{ERROR}&log=%{ERRORLOG}",
      check: {
        m4a_bitrate: { min_rate: 128, max_rate: 600, order: 2 },
        wav_bitrate: { min_rate: 1000, max_rate: 10_000, order: 3 },
        flac_bitrate: { min_rate: 500, max_rate: 10_000, order: 4 },
        mp3_bitrate: { min_rate: 128, max_rate: 500, order: 5 }
      }
    }

    # Ringtone duration must be between 5 and 30 secs, so check for that if this song belongs to a ringtone
    info[:check][:duration] = { min_duration: 5, max_duration: 30, order: 1 } if song.album.instance_of?(Ringtone)

    # this let's us import tracks from outside sources such as SoundCloud
    info[:download_url] = download_url if download_url != nil

    info[:audio_format] = audio_format.downcase unless audio_format.nil?

    to_query(info, "info")
  end

  def to_query(obj, namespace = nil)
    case obj
    when Hash
      obj.collect do |key, value|
        to_query(value, namespace ? "#{namespace}[#{key}]" : key)
      end.sort * "&"
    when Array
      obj.collect { |value| to_query(value, "#{namespace}[]") } * "&"
    else
      "#{CGI.escape(namespace.to_s)}=#{CGI.escape(obj.to_s)}"
    end
  end

  def search_string_formatting(search_string)
    if search_string
      search_array = search_string.split(":")
      search_array[1].strip if search_array[1]
    else
      search_string
    end
  end

  def search_option_maker(search_type, search_type_current, search_term)
    # raise "#{search_type_current}"
    if search_type == search_type_current
      "<option value='#{search_type}' selected='selected'>#{search_term}</option>"
    elsif cookies[:search_type] && cookies[:search_type] == search_type
      # Default to cookie saved search if cookies exist
      "<option value='#{search_type}' selected='selected'>#{search_term}</option>"
    elsif !search_type_current && !cookies[:search_type] && search_type == "person|email"
      # Default to account email search
      "<option value='#{search_type}' selected='selected'>#{search_term}</option>"
    else
      "<option value='#{search_type}'>#{search_term}</option>"
    end
  end

  def add_new_artwork_url
    "/images/albums/noartwork_small.jpg"
  end

  def artwork_100x100(album)
    artwork = album.artwork
    return add_new_artwork_url if !artwork || artwork.last_errors.present?

    album.artwork.url(:small)
  end

  def artwork_400x400(album, _blank = false)
    artwork = album.artwork
    return add_new_artwork_url if !artwork || artwork.last_errors.present?

    artwork.url(:medium)
  end

  def display_in_paragraphs(text)
    paragraphs = text.split(/\r\n|\n\n/).compact

    return_text = ""
    paragraphs.each do |paragraph|
      return_text << "<p>" + paragraph + "</p>"
    end

    return_text.html_safe
  end

  def awesome_truncate(text, length = 30, truncate_string = "...")
    return if text.nil?

    l = length - truncate_string.length
    (text.length > length) ? text[/\A.{#{l}}\w*\;?/m][/.*[\w\;]/m] + truncate_string : text
  end

  def song_error(song)
    if song.last_errors.include?("is too high")
      "We do not accept Apple lossless files at this time.  Please upload your songs in WAV format."
    else
      song.last_errors
    end
  end

  def song_error_upload(song)
    "<span style=\"color:#CC0000;font-weight:bold\">There's a problem with the submitted file: </span> #{song_error(song)} (#{link_to('help', knowledgebase_link_for('genre-not-listed', country_website), popup: ['Audio File Encoding', 'height=900,width=900'])} )<br/>"
  end

  def video_state(video)
    if video.is_deleted?
      "<span class='error'>Deleted</span>"
    elsif video.locked? && video.locked_by_type == "Person"
      "Admin Lock"
    elsif video.locked? && video.locked_by_type == "Invoice"
      "In Invoice"
    elsif video.finalizer.waiting_for_media_delivery?
      "Needs Delivery"
    elsif video.finalizer.media_delivered?
      "Video Received"
    elsif video.finalizer.should_finalize?
      "Ready to Go"
    else
      "#{pluralize(video.steps_required, 'step')} to go"
    end
  end

  def inline_edit_with_cue(object, method, label)
    inplace_html = "<span id=\"#{method}-ext\">#{label}</span>"
    inplace_helper_output = in_place_editor_field(object, method, {}, external_control: "#{method}-ext")
    inplace_html += inplace_helper_output
    inplace_html.html_safe
  end

  def blog_feed_enabled?
    TcWww::Application.config.blog_feed_enabled
  end

  def parse_tcblog_rss
    RSS::Parser.parse(open("http:#{link_to_blog_page(country_website)}/feed").read, false)
  rescue => e
    RSS::Rss.new("1.0") # empty feed
  end

  def show_mypage_link?(person)
    person.albums.detect(&:finalized?)
  end

  def creatives_display(creative_array)
    if creative_array.size.positive?
      h(creative_array.collect(&:name).to_sentence(last_word_connector: " & ", two_words_connector: " & "))
    else
      ""
    end
  end

  def creative_array(creative_array)
    if creative_array.size.positive?
      h(creative_array.collect { |x| "#{x.role.tr('_', ' ').titleize}: #{x.name}" })
    else
      ""
    end
  end

  def select_options_for_roles
    roles = Creative.roles.collect { |x| "<option>#{x}</option>" }.join
    "<select name='type_of'>#{roles}</select>".html_safe
  end

  def select_options_for_soundout_reports(selected = nil)
    options =
      SoundoutProduct
      .soundout_products_detail(current_user.try(:country_website_id) || CountryWebsite::UNITED_STATES, current_user)
      .sort_by { |s| s["price"] }
      .map { |s| [raw("#{s['display_name']} &ndash; #{money_to_currency s['price'].to_money(s['currency'])}"), s["report_type"]] }

    options_for_select(options, selected)
  end

  def creatives_options_for_customers
    Creative.roles.first(3).collect { |x| [x.underscore.sub(/_/, " ").titleize, x] }
  end

  def creatives_options_for_admins
    Creative.roles.collect { |x| [x.underscore.sub(/_/, " ").titleize, x] }
  end

  def css_class_for_track_state(song)
    @album ||= song.album

    if !@album.finalized?
      if song.uploaded? && song.last_errors.blank?
        "song_uploaded"
      elsif song.last_errors
        "song_error"
      else
        "song_no_upload"
      end
    else
      "song_upload"
    end
  end

  def setting_field(object_name, setting_type, value)
    case setting_type
    when "text" then
      text_field(object_name, :value)
    when "font" then
      select(object_name, :value, ["Arial", "LucidaSansTypewriter", "Geneva", "Cracked", "Copperplate"], selected: value)
    when "color" then
      select(object_name, :value, ["white", "red", "green", "blue"], selected: value)
    when "strength" then
      select(object_name, :value, { light: 40, normal: 75, heavy: 100 }, selected: value.to_i)
    end
  end

  def genre_list
    genres = parent_genres_for_country_user
    format_genres(genres)
  end

  def india_sub_genre_list
    format_genres(Genre.india_subgenres)
  end

  def ringtone_genre_list
    genres = parent_genres_for_country_user.available_ringtone_genres
    format_genres(genres)
  end

  def language_list
    languages = LanguageCode.available_album_languages.sort_by(&:description)
    [["", ""]] + languages.map { |language| [language.description, language.code] }
  end

  def language_codes(priority_languages = nil)
    language_options = []

    if priority_languages
      language_options += priority_languages.map { |l, c| [l, c] }
      language_options += ["-----------", nil]
    end

    language_options += Language.select("language, name").collect { |l| [l.name, l.language] } - priority_languages
  end

  def footer_partial
    "footer partial, should not call_after"
  end

  def invoice_link(invoice_id)
    invoice = Invoice.find_by(id: invoice_id.to_i)
    if invoice
      link_to(invoice_print(invoice_id), invoice_path(invoice_id))
    else
      invoice_print(invoice_id)
    end
  end

  # this could be combined with the invoice_link method above with a current_user.has_role?("Admin") check, but I decided that admin might
  # want to see what the customer is seeing, so I kept these seperate - DCD
  def invoice_admin_link(invoice_id)
    invoice = Invoice.find_by(id: invoice_id.to_i)
    if invoice
      link_to(invoice_id, admin_person_invoice_path(invoice.person_id, invoice_id))
    else
      invoice_id
    end
  end

  def use_inside_header?
    if controller.instance_of?(IndexController) && (CONTROLLER_ACTIONS.include? controller.action_name)
      false
    elsif (["publishing_administration"].include? controller.action_name) && controller.instance_of?(IndexController)
      current_user ? true : false
    else
      true
    end
  end

  def use_sync_footer?
    controller.class.parent == Sync
  end

  def is_active_page?(path)
    current_page?(path) ? "active" : nil
  end

  def is_active_page_or_section?(path, section)
    (current_page?(path) || section) ? "active" : nil
  end

  def is_index_page?
    if (controller.controller_name == "index" && controller.action_name == "index")
      true
    else
      false
    end
  end

  def is_publishing?
    if ((["publishing_administration"].include? controller.action_name) && controller.instance_of?(IndexController))
      true
    else
      false
    end
  end

  def is_pricing?
    if ((["pricing", "distribution_credits"].include? controller.action_name) && controller.instance_of?(IndexController))
      true
    else
      false
    end
  end

  def is_i_am_tc?
    if ((["hiphop"].include? controller.action_name) && controller.instance_of?(IndexController))
      true
    else
      false
    end
  end

  def display_user_balance(options = {})
    merge_options = {
      suppress_iso: true,
      precision: 2
    }
    merged_options = options.merge(merge_options)
    balance_to_currency(Money.new(current_user.balance_cents, current_user.site_currency), merged_options)
  end

  def suspicious_class(suspect, key)
    suspect.try(:suspicious?, key.to_s) ? "suspicious" : ""
  end

  def ytm_link
    youtube_monetizations_path
  end

  def music_publishing_admin_link
    return "//www.tunecore.com/music-publishing-administration" if country_website.nil?

    case country_website
    when "AU"
      "//www.tunecore.com.au/publishing-administration"
    when "CA"
      "//www.tunecore.ca/music-publishing-administration"
    when "DE"
      "//www.tunecore.de/musikverlag"
    when "FR"
      "//www.tunecore.fr/music-publishing-administration"
    when "IT"
      "//www.tunecore.it/music-publishing-administration"
    when "UK"
      "//www.tunecore.co.uk/publishing-administration"
    else
      "//www.tunecore.com/music-publishing-administration"
    end
  end

  def route_specific_class_name
    "#{params[:controller].tr('/', '-')}__#{params[:action]}"
  end

  def money_with_discount(original_price, current_price, currency, options = {})
    options_clone = options.clone # why does money_to_currency delete attributes off of the options hash?
    original_amount = money_to_currency(original_price.to_money(currency), options)
    current_amount = money_to_currency(current_price.to_money(currency), options_clone)

    if original_amount == current_amount
      current_amount
    else
      "<s>#{original_amount}</s> #{current_amount}".html_safe
    end
  end

  def artist_services_cms_url
    ARTIST_SERVICES_LOCALE_URLS.fetch(country_website, nil)
  end

  private

  def format_genres(genres)
    [["", ""]] + genres.map { |genre| [genre.name, genre.id] }
  end

  def invoice_print(invoice_id)
    "I" << sprintf("%0#{Invoice::DECIMALS_IN_INVOICE_NUMBER}d", invoice_id.to_i)
  end

  def flag(country_website)
    image_name = "flag-#{country_website.country}-small.png"

    image_tag("/images/#{image_name}") if File.exist?(File.join(Rails.root, "public/images", image_name))
  end

  def pluralize_without_count(count, noun, text = nil)
    return if count.zero?

    (count == 1) ? "#{noun}#{text}" : "#{noun.pluralize}#{text}"
  end

  def link_to_terms_and_conditions(with_content = false)
    link_text  = content_tag(:strong, custom_t(:terms_and_conditions))
    link_text += content_tag(:span, custom_t("shared.inside_header.t_and_c_content")) if with_content
    link_to(link_text, publishing_administration_tac_url)
  end

  def publishing_administration_tac_url
    "/index/terms##{custom_t(:publishing_administration_terms_anchor)}"
  end

  def refer_a_friend_config(key)
    return unless current_user && $refer_a_friend_api_config[current_user.country_domain]

    $refer_a_friend_api_config[current_user.country_domain][key]
  end

  def customer_has_updated_release(release)
    release.updated_at > release.review_audits.where(event: "REJECTED").maximum(:created_at)
  end

  def locale_to_og_locale(locale)
    COUNTRY_LOCALE_OG_MAP[locale]
  end

  def http_or_https
    return "http" unless request.ssl?

    "https"
  end

  def trim_locale(locale)
    locale.split("-").first
  end

  def bi_transfer_and_gst_enabled?
    FeatureFlipper.show_feature?(:enable_gst, @person) && FeatureFlipper.show_feature?(:bi_transfer, @person)
  end

  def tunecore_terms_link
    custom_t(:tunecore_terms_link, default: "https://tunecore.com/terms")
  end

  def hide_legacy_fb_monetization?
    @hide_legacy_fb_monetization ||= freemium_or_discovery_feature? &&
                                     !current_user.has_active_subscription_product_for?(PersonSubscriptionStatus::FB_TRACKS)
  end

  def freemium_or_discovery_feature?
    @freemium_or_discovery_feature ||= (FeatureFlipper.show_feature?(:freemium_flow, current_user) || FeatureFlipper.show_feature?(:discovery_platforms, current_user))
  end

  def plan_display_valid?
    Plan.enabled?(current_user, person_flags[:blocked_from_plans])
  end

  def show_plan_promo?
    user_plan_id = current_user.plan&.id

    [Plan::PROFESSIONAL, Plan::LEGEND].exclude?(user_plan_id)
  end

  def plan_display_name(plan)
    return "" if plan.nil?

    custom_t("plans.#{plan.name}.name")
  end

  def bcp47_locale(locale = I18n.locale)
    BCP_47_LOCALE_MAP.fetch(locale.to_s.downcase, BCP_47_LOCALE_MAP[USA_LOCALE])
  end
end
