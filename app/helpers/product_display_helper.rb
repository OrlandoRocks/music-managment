# = Description
# This module is used as a helper to map purchases and products to the correct
# display partials. The display partials are used on the cart, invoice/thanks,
# distribution/thanks and admin screens.  Be careful when adjusting these!
#
# = Change Log
# [2010-03-04 -- MJL]
# Adding a whole bunch of new partials for albums, singles and ringtone packs with
# super crappy iPhone style badges all over them...

module ProductDisplayHelper
  def purchase_display_mapper(purchase)
    product = purchase.product
    product.set_targeted_product(purchase.targeted_product)
    case purchase.purchased_item.class.name
    when "Album"
      render(partial: "carts/album_purchase", locals: { album: purchase.purchased_item, product: product, purchase: purchase })
    when "Single"
      render(partial: "carts/album_purchase", locals: { album: purchase.purchased_item, product: product, purchase: purchase })
    when "Ringtone"
      render(partial: "carts/album_purchase", locals: { album: purchase.purchased_item, product: product, purchase: purchase })
    when "MusicVideo", "Video", "FeatureFilm"
      render(partial: "carts/video_purchase", locals: { video: purchase.purchased_item, product: product })
    when "Renewal"
      render(partial: "carts/renewal_purchase", locals: { renewal: purchase.purchased_item, product: product, purchase: purchase })
    when "Salepoint"
      render(partial: "carts/salepoint_purchase", object: purchase.purchased_item)
    when "SalepointSubscription"
      render(partial: "carts/salepoint_subscription_purchase", locals: { automator: purchase.purchased_item, product: product })
    when "CreditUsage"
      render(partial: "carts/album_purchase", locals: { album: purchase.related.related, product: product, purchase: purchase })
    when "Booklet"
      render(partial: "carts/booklet", locals: { booklet: purchase.purchased_item, product: product })
    when "YoutubeMonetization"
      render partial: "carts/youtube_money", locals: { product: product }
    when "Composer"
      product_display_mapper(product)
    when "Product"
      product_display_mapper(product)
    else
      %Q(
        <div class="product-purchase clearfix">
          #{render(partial: 'carts/product_info', locals: { product: product })}
        </div>
      ).html_safe
    end
  end

  def product_display_mapper(product)
    case product.name
    when "Ringtone Distribution Credit"
      render(partial: "carts/ringtone_pack", locals: { product: product })
    when "Ringtone Distribution Credit - 3 Pack"
      render(partial: "carts/ringtone_3_pack", locals: { product: product })
    when "Ringtone Distribution Credit - 5 Pack"
      render(partial: "carts/ringtone_5_pack", locals: { product: product })
    when "Ringtone Distribution Credit - 10 Pack"
      render(partial: "carts/ringtone_10_pack", locals: { product: product })
    when "Ringtone Distribution Credit - 20 Pack"
      render(partial: "carts/ringtone_20_pack", locals: { product: product })
    when "Album Distribution Credit", "Single Distribution Credit"
      render(partial: "carts/album_pack", locals: { product: product })
    when "Album Distribution Credit - 5 Pack", "Single Distribution Credit - 5 Pack"
      render(partial: "carts/album_5_pack", locals: { product: product })
    when "Album Distribution Credit - 10 Pack", "Single Distribution Credit - 10 Pack"
      render(partial: "carts/album_10_pack", locals: { product: product })
    when "Album Distribution Credit - 20 Pack", "Single Distribution Credit - 20 Pack"
      render(partial: "carts/album_20_pack", locals: { product: product })
    when "Songwriter Service"
      render(partial: "carts/songwriter_service", locals: { product: product })
    else
      render(partial: "carts/five_pack", locals: { product: product })
    end
  end

  def purchase_display_name_for_album(product, album)
    if product.display_name.include?("Distribution")
      product.display_name
    else
      "#{album.class.name} Distribution - #{product.display_name}"
    end
  end

  def after_payment_messaging(product_purchases)
    display_flags = []

    product_purchases.each { |p|
      case p.purchased_item.class.name
      when "Album", "Single", "Ringtone", "Salepoint"
        display_flags << :distribution
      when "Renewal"
        display_flags << :distribution if p.product.product_type == Product::DISTRO_EXTENSION
      when "Composer"
        display_flags << :songwriter
      end
    }

    display_flags
  end

  def renewal_date_range(item_for_renewal, purchase)
    renewal = Renewal.with_renewal_information.find(item_for_renewal.id)
    if purchase.paid_at.nil?
      "For #{fmt_long_date(renewal.expires_at)} To #{fmt_long_date(Tunecore::AutoRenewalDateCalculator.expiration_date(renewal.expires_at, renewal.renewal_interval, renewal.renewal_duration))}"
    else
      renewal_history_date = renewal.renewal_history_for_purchase(purchase)
      "For #{fmt_long_date(renewal_history_date.starts_at)} To #{fmt_long_date(renewal_history_date.expires_at)}"
    end
  end
end
