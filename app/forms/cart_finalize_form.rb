class CartFinalizeForm < FormObject
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  define_model_callbacks :save
  before_save :add_strategies
  before_save :process_payment

  validate :validate_person, :validate_vat_info, :validate_self_billing
  validates_presence_of :person, :purchases, :ip_address

  attr_accessor :manager,
                :person,
                :payment_id,
                :purchases,
                :payment_method,
                :ip_address,
                :cvv,
                :gstin,
                :mobile_number,
                :person_address_info,
                :vat_info,
                :three_d_secure_nonce

  attr_writer :use_balance

  def initialize(opts = {})
    super
    @manager ||= Cart::Payment::Manager.new(person, purchases, ip_address)
  end

  def add_strategies
    manager.add_strategy(Cart::Payment::Strategies::Balance.new) if use_balance
    if payment_id.blank?
      manager.add_strategy(Cart::Payment::Strategies::Payu.new) if use_payments_os?
      return
    end
    payment_strategies
  end

  def person_preference
    @person_preference ||= PersonPreference.find_by(person_id: person.id)
  end

  # TODO: refactor method, to many if/else/elsif banches; method should end in ?
  def use_balance
    if person.balance.positive?
      return true if FeatureFlipper.show_feature?(:force_pay_with_balance_for_free_albums, person)
      return false if all_purchases_free_albums?(purchases)

      @use_balance = person_preference.try(:pay_with_balance) if @use_balance.blank?
      is_false_value?(@use_balance) ? false : true
    else
      false
    end
  end

  def process_payment
    manager.process!
  end

  def validate_person
    @person.update_address_with_country_audit(CountryAuditSource::CHECKOUT_NAME, person_address_info)
    return true if @person.valid?

    errors.merge!(@person.errors)
    false
  end

  def validate_vat_info
    return true if vat_info.nil?

    vat_information = VatInformation.find_or_initialize_by(person: @person)
    vat_information.update(vat_info)

    @person.reload
    return true if vat_information.vat_valid?

    errors[:base] << vat_validation_errors(vat_information)
    false
  end

  def validate_self_billing
    return false if @person.vat_feature_on? && @person.affiliated_to_bi? && !@person.self_billing_status&.accepted?

    true
  end

  delegate :otp_redirect_url, to: :manager

  delegate :invoice, to: :manager

  def purchased_before?
    @person.purchased_before?
  end

  def save
    return false unless valid?

    run_callbacks :save do
      if manager.successful?
        update_gst_info
        true
      else
        if manager.notice
          errors.add(:base, manager.notice)
        else
          errors.add(:base, I18n.t("controllers.carts.issue_placing_your_order"))
        end
        false
      end
    end
  end

  def payment_strategies
    if payment_id == "paypal"
      manager.add_strategy(
        Cart::Payment::Strategies::Paypal.new(
          StoredPaypalAccount.currently_active_for_person(@person),
          @person
        )
      )
    elsif use_adyen_gateway?
      manager.add_strategy(Cart::Payment::Strategies::Adyen.new(adyen_payment_method))
    elsif use_payments_os?
      manager.add_strategy(Cart::Payment::Strategies::PaymentsOSCard.new(payment_id, cvv))
    else
      manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(payment_id, three_d_secure_nonce))
    end
  end

  private

  def use_payments_os?
    CountryWebsite::PAYMENTS_OS_COUNTRIES.include?(person.country_domain)
  end

  def use_adyen_gateway?
    payment_id.to_s.split("-")[0] == "adyen"
  end

  def adyen_payment_method
    payment_id.to_s.split("-")[1]
  end

  def update_gst_info
    return unless FeatureFlipper.show_feature?(:enable_gst, @person)
    return if gstin.blank?

    gst_info = GstInfo.find_or_create_by(person_id: @person.id, gstin: gstin)
    invoice.update(gst_info: gst_info)
  end

  def all_purchases_free_albums?(purchases)
    purchases.all? { |p| Product.free_release?(p.related) }
  end

  def vat_validation_errors(vat_information)
    return custom_t(vat_information.vat_errors) if vat_information.vat_errors.present?

    vat_information.errors.full_messages.map { |e| custom_t(e) }.join(", ")
  end
end
