class TwoFactorAuth::AuthenticationForm < FormObject
  include Rails.application.routes.url_helpers

  ACTIONS = ["sign_in", "authentication", "complete"].map(&:freeze).freeze

  VALID_SUCCESS_CALLBACKS = [:disable_two_factor_auth].map(&:freeze).freeze

  INITIAL_STEP   = ACTIONS.first
  SEND_CODE_STEP = ACTIONS.first
  AUTH_STEP      = ACTIONS.second
  FINAL_STEP     = ACTIONS.last

  ABANDON_STEP   = "abandoned".freeze

  EVENT_TYPE = "authentication"

  ERROR_MESSAGES = {
    "sign_in" => I18n.t(:wrong_credentials),
    "authentication" => I18n.t(:wrong_code),
    "invalid" => I18n.t(:invalid_request)
  }

  validates_presence_of :person, :page, :tfa_action
  validates :tfa_action, inclusion: { in: ACTIONS }
  validate :valid_event?

  attr_accessor :person,
                :page,
                :tfa_action,
                :email,
                :password,
                :auth_code,
                :person_id,
                :success_callbacks

  def perform
    if two_factor_auth_step.successful?
      send_auth_code if tfa_action == SEND_CODE_STEP
      create_new_event(successful: true, action: tfa_action)
    else
      errors.add(:base, ERROR_MESSAGES[tfa_action])
      create_new_event(successful: false, action: tfa_action)
    end
  end

  def next_action
    if last_event.try(:successful)
      ACTIONS[(ACTIONS.index(tfa_action) + 1)]
    else
      ACTIONS[(ACTIONS.index(tfa_action))]
    end
  end

  def is_complete?
    if tfa_action == FINAL_STEP
      create_new_event(successful: true, action: tfa_action)
      execute_success_callbacks
      true
    else
      false
    end
  end

  def execute_success_callbacks
    valid_success_callbacks.each { |callback| send(callback) } if success_callbacks.present?
  end

  def disable_two_factor_auth
    person.two_factor_auth.try(:disable!)
  end

  def on_authentication_action?
    tfa_action == AUTH_STEP
  end

  def last_event
    person.two_factor_auth.try(:last_event)
  end

  private

  def create_new_event(opts)
    person.two_factor_auth.two_factor_auth_events.create(
      page: page,
      type: EVENT_TYPE,
      successful: opts[:successful],
      action: opts[:action]
    )
  end

  def valid_success_callbacks
    success_callbacks.map(&:to_sym) & VALID_SUCCESS_CALLBACKS
  end

  def valid_event?
    return true if last_event.blank?

    return true if last_event.page == (page) && last_event.action.in?([FINAL_STEP, tfa_action])

    return true if last_event_valid?

    errors.add(:base, ERROR_MESSAGES["invalid"])
    create_new_event(successful: false, action: tfa_action)
  end

  def last_event_valid?
    return false unless last_event.present? && last_event.successful?

    last_event_in_auth_window? && last_action_correct_for_flow?
  end

  def last_event_in_auth_window?
    last_event.created_at >= TwoFactorAuth::AUTHENTICATION_WINDOW.ago
  end

  def last_action_correct_for_flow?
    last_event.page == page || last_event_completed_or_abandoned?
  end

  def last_event_completed_or_abandoned?
    ([last_event.action] & [FINAL_STEP, ABANDON_STEP]).present?
  end

  def two_factor_auth_step
    "TwoFactorAuth::Steps::#{tfa_action.classify}".constantize.check(person, step_params)
  end

  def step_params
    { auth_code: auth_code, email: email, password: password }
  end

  def send_auth_code
    if FeatureFlipper.show_feature?(:verify_2fa, person)
      TwoFactorAuth::ApiClient.new(person).request_authorization
    else
      TwoFactorAuth::AuthyApiClient.new(person).request_authorization
    end
  end
end
