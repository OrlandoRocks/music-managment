class TwoFactorAuth::PreferenceForm < FormObject
  include Rails.application.routes.url_helpers

  ACTIONS = [
    "preferences",
    "authentication",
    "complete"
  ].map(&:freeze).freeze

  INITIAL_STEP = ACTIONS.first
  SEND_CODE_STEP = ACTIONS.first
  AUTHENTICATION_STEP = ACTIONS.second
  FINAL_STEP = ACTIONS.last

  ABANDON_STEP = "abandoned".freeze

  ERROR_MESSAGES = {
    "authentication" => I18n.t(:wrong_code),
    "invalid" => I18n.t(:invalid_request)
  }

  EVENT_TYPE = "change_preferences".freeze
  EVENT_PAGE = "account_settings".freeze

  validates_presence_of :person, :tfa_action
  validates :tfa_action, inclusion: { in: ACTIONS }
  validate :valid_event?

  attr_accessor :person,
                :tfa_action,
                :email,
                :password,
                :auth_code,
                :country_code,
                :phone_number,
                :notification_method

  def initialize(params)
    super

    self.tfa_action = INITIAL_STEP unless tfa_action
  end

  def perform
    execute_two_factor_auth_step.successful? ? handle_success : handle_failure
  end

  def terminate
    create_new_event(successful: true, action: ABANDON_STEP)
  end

  def next_action
    action_index = ACTIONS.index(tfa_action)
    (last_event.try(:successful) && action_index) ? ACTIONS[action_index + 1] : tfa_action
  end

  def is_complete?
    return false unless tfa_action == FINAL_STEP

    record_successful_action && true
  end

  def on_authentication_action?
    tfa_action == AUTHENTICATION_STEP
  end

  def last_event
    person.two_factor_auth.try(:last_event)
  end

  def save_previous_state
    if tfa_action == INITIAL_STEP
      person.two_factor_auth.save_previous_preferences
    else
      false
    end
  end

  def restore_previous_state
    person.two_factor_auth.restore_previous_preferences
  end

  private

  def valid_event?
    event_is_valid_and_correct? ? true : handle_invalid_event
  end

  def event_is_valid_and_correct?
    return true if last_event.blank? || last_action_permitted?

    last_event_present? && event_within_window?
  end

  def last_action_permitted?
    last_event.action.in?([FINAL_STEP, ABANDON_STEP, tfa_action])
  end

  def last_event_present?
    last_event.present?
  end

  def event_within_window?
    last_event.created_at >= TwoFactorAuth::AUTHENTICATION_WINDOW.ago
  end

  def execute_two_factor_auth_step
    step = "TwoFactorAuth::Steps::#{tfa_action.classify}".constantize
    step.check(person, step_params)
  end

  def step_params
    {
      auth_code: auth_code,
      email: email,
      password: password,
      phone_number: phone_number,
      country_code: country_code,
      notification_method: notification_method
    }
  end

  def send_auth_request
    if FeatureFlipper.show_feature?(:verify_2fa, person)
      TwoFactorAuth::ApiClient.new(person).request_authorization
    else
      TwoFactorAuth::AuthyApiClient.new(person).request_authorization
    end
  end

  def handle_success
    send_auth_request if tfa_action === SEND_CODE_STEP
    record_successful_action
  end

  def handle_invalid_event
    errors.add(:base, ERROR_MESSAGES["invalid"])
    record_failed_action
  end

  def handle_failure
    errors.add(:base, ERROR_MESSAGES[tfa_action])
    record_failed_action
  end

  def create_new_event(opts)
    person.two_factor_auth.two_factor_auth_events.create(
      type: EVENT_TYPE,
      successful: opts[:successful],
      page: EVENT_PAGE,
      action: opts[:action]
    )
  end

  def record_successful_action
    create_new_event(successful: true, action: tfa_action)
  end

  def record_failed_action
    create_new_event(successful: false, action: tfa_action)
  end
end
