class TwoFactorAuth::EnrollmentForm < FormObject
  KEY_PREFIX  = "2FA_ENROLLMENT::".freeze
  STEPS       = [
    "intro",
    "sign_in",
    "preferences",
    "authentication",
    "confirmation",
    "finish_and_redirect"
  ].freeze

  attr_reader :person, :cache_key, :current_step, :error

  def initialize(person)
    @person       = person
    @cache_key    = KEY_PREFIX + person.id.to_s
    data          = $redis.get(cache_key)
    data.present? ? set_cached_state(data) : set_new_state
  end

  def submit_step(params)
    submission = "TwoFactorAuth::Steps::#{current_step.classify}".constantize.check(person, params)
    submission.successful? ? step_success : step_failure(submission.error_msg)
  end

  def step_back
    previous_step && save_state
    current_step
  end

  def reset_steps
    set_new_state
  end

  def is_complete?
    @current_step == STEPS[STEPS.length - 1]
  end

  def on_authentication_action?
    current_step == "authentication"
  end

  def step_success
    @error = nil
    next_step && save_state
  end

  private

  def set_cached_state(data)
    data          = JSON.parse(data)
    @current_step = data["current_step"]
    @error        = data["error"]
  end

  def set_new_state
    @current_step = STEPS.first
    @error        = nil
    save_state
  end

  def save_state
    state = { current_step: @current_step, error: @error }.to_json
    $redis.set(@cache_key, state)
  end

  def step_failure(error_msg)
    @error = error_msg
    save_state
  end

  def previous_step
    @current_step = STEPS[(STEPS.index(@current_step) - 1)]
  end

  def next_step
    @current_step = STEPS[(STEPS.index(@current_step) + 1)]
  end
end
