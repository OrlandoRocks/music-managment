class MassTakedownForm < FormObject
  attr_accessor :album_level_stores,
                :track_level_stores,
                :album_ids,
                :send_email,
                :admin,
                :ip_address,
                :template_type
  attr_reader   :notify_list

  define_model_callbacks :save
  validate :selection_made?
  validates_presence_of :album_ids

  ALBUM_TAKEDOWN_SUBJECT = "Mass Album takedown".freeze
  YTSR_TAKEDOWN_SUBJECT  = "Youtube Sound Recording Takedown".freeze
  EMAIL_TEMPLATES        = [
    "Default",
    "Streaming Fraud",
    "Chargeback On Renewals",
    "Internal Review For Rights",
    "Internal Claim from Stores"
  ].freeze

  def initialize(options = {})
    super
    @notify_list = []
  end

  def save
    return false unless valid?

    salepoint_takedowns
    track_level_takedowns
    send_takedown_notifications
    clear_fields
    true
  end

  private

  def salepoint_takedowns
    albums_by_person.each do |person, album_list|
      notification_details = { person: person, albums: [], stores: [] }
      opts = {
        user_id: admin.id,
        ip_address: ip_address,
        subject: ALBUM_TAKEDOWN_SUBJECT,
        note: "Album was taken down"
      }

      album_list.each do |album|
        if album_level_store_ids == :all
          if album.takedown!(opts)
            notification_details[:albums] << { album: album, stores: :all }
            notification_details[:stores] << "All Stores"
          else
            errors.add(:album, "#{album.id} failed to be taken down")
          end
        else
          notification_details[:albums] << { album: album, stores: [] }
          notification_details[:stores] << album_level_store_ids

          salepoints_to_takedown(album).each do |salepoint|
            if salepoint.takedown!
              create_album_takedown_note(album, "Album was taken down from #{salepoint.store.name}")
              notification_details[:albums].last[:stores] << salepoint.store
            else
              errors.add(:album, "#{album.id} failed to be taken down from #{salepoint.store.name}")
            end
          end
          send_sns_notification_takedown(album)
        end
      end
      notify_list << notification_details
    end
  end

  def track_level_takedowns
    return if track_level_stores.blank?

    albums_by_person.each do |person, album_list|
      notification_details = { person: person, albums: [] }
      album_list.each do |album|
        if track_level_stores.include?(fb_tracks_store_id.to_s)
          stores = ig_store_present?(album) ? [*track_level_stores, ig_store_id] : track_level_stores
          album.salepoints.where(store_id: ig_store_id).each(&:takedown!) if ig_store_present?(album)
        else
          stores = track_level_stores
        end

        monetizations = TrackMonetization.joins(:song)
                                         .where(songs: { album_id: album.id }, store_id: track_level_stores)
                                         .readonly(false)

        takedown_params = { current_user: admin, track_mons: monetizations }.with_indifferent_access

        TrackMonetization::TakedownService.takedown(takedown_params)
        notification_details[:albums] << { album: album, stores: stores }
      end
    end
  end

  def send_takedown_notifications
    return unless send_email == 1

    notify_list.each do |notification|
      if template_type == "Default"
        TakedownNotifier.takedown_notification(notification[:person], notification[:albums]).deliver
      else
        notifier = template_type.parameterize.underscore + "_notification"
        TakedownNotifier.send(notifier, notification).deliver
      end
    end
  end

  def album_level_store_ids
    return [] if album_level_stores.blank?
    return :all if album_level_stores.include?("all")

    self.album_level_stores += Store::ITUNES_STORE_IDS if album_level_stores.include?("iTunes")
    self.album_level_stores.map(&:to_i)
  end

  def map_selected_track_monetization_store_ids
    return [] if track_level_stores.blank?

    @selected_track_monetization_ids ||= track_level_stores.map(&:to_i)
  end

  def clear_fields
    self.album_ids = nil
    self.album_level_stores = nil
    self.send_email = nil
  end

  def albums_by_person
    @albums_by_person ||= albums.group_by(&:person)
  end

  def albums
    @albums ||= BulkAlbumFinder.new(album_ids).execute
  end

  def salepoints_to_takedown(album)
    album.salepoints.includes(:store).where(store_id: album_level_store_ids)
  end

  def create_album_takedown_note(album, note)
    Note.create(
      related: album,
      note_created_by: admin,
      ip_address: ip_address,
      subject: ALBUM_TAKEDOWN_SUBJECT,
      note: note
    )
  end

  def create_ytsr_takedown_notes(album)
    return unless album.salepoints.exists?(store_id: ytsr_store_id)

    album.songs.each do |song|
      Note.create(
        related: song,
        note_created_by: admin,
        ip_address: ip_address,
        subject: YTSR_TAKEDOWN_SUBJECT,
        note: "YTSR track '#{song.name}' was taken down"
      )
    end
  end

  def ig_store_present?(album)
    @ig_store_present ||= album.stores.where(id: ig_store_id).present?
  end

  def fb_tracks_store_id
    @fb_tracks_store_id ||= Store.where(abbrev: "fbt").pluck(:id).first
  end

  def ig_store_id
    @ig_store_id ||= Store.where(abbrev: "ig").pluck(:id).first
  end

  def ytsr_store_id
    @ytsr_store_id ||= Store.where(abbrev: "ytsr").pluck(:id).first
  end

  def selection_made?
    return true if album_level_store_ids == :all

    unless album_level_stores.present? || track_level_stores.present?
      errors.add(:stores, "must have at least 1 selection made")
      false
    end
    true
  end

  def send_sns_notification_takedown(album)
    return unless FeatureFlipper.show_feature?(:use_sns, admin.id) &&
                  ENV.fetch("SNS_RELEASE_TAKEDOWN_TOPIC", nil).present?

    Sns::Notifier.perform(
      topic_arn: ENV.fetch("SNS_RELEASE_TAKEDOWN_TOPIC"),
      album_ids_or_upcs: [album.id],
      store_ids: album_level_store_ids,
      delivery_type: "takedown",
      person_id: admin.id
    )
  end
end
