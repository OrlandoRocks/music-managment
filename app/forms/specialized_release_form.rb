class SpecializedReleaseForm < AlbumForm
  def specialized_release_type
    @specialized_release_type ||= album_params.try(:[], :specialized_release_type).presence
  end

  private

  def build_special_salepoints
    return unless specialized_release_type
    return if album.persisted?

    specialized_stores =
      if all_discovery_platforms_enabled
        Store.where(discovery_platform: true)
      elsif specialized_release_type
        Store.where(id: SpecializedReleaseable::SPECIALIZED_RELEASE_TYPE_TO_STORE[specialized_release_type])
      end

    specialized_stores.each do |store|
      album.salepoints.find_or_initialize_by(
        store_id: store.id,
        variable_price_id: store.default_variable_price,
      )
    end
  end
end
