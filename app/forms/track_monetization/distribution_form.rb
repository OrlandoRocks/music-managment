class TrackMonetization::DistributionForm < FormObject
  DISTRIBUTION_ACTOR    = name.freeze
  DISTRIBUTION_MESSAGE  = "Starting distribution".freeze

  NEW_TRACK             = "new".freeze
  MONETIZED             = "monetized".freeze
  NOT_MONETIZED         = "not_monetized".freeze
  PROCESSING            = "processing".freeze
  PROCESSING_TAKEDOWN   = "processing_takedown".freeze

  define_model_callbacks :save

  after_save :enqueue_track_monetizations

  attr_accessor :track_monetization_ids, :takedown, :current_user

  validates_presence_of :track_monetization_ids

  def save
    return false unless valid?

    run_callbacks :save do
      true
    end
  end

  def response_data
    load_track_monetizations.map do |track|
      { track_id: track.id, status: track_status(track) }
    end
  end

  def should_takedown
    @should_takedown ||= boolean_value_of(takedown)
  end

  private

  def load_track_monetizations
    @track_monetizations ||= TrackMonetization
                             .includes(:song)
                             .where(id: track_monetization_ids)
                             .visible_to_user
  end

  def youtube_music_albums
    @youtube_music_albums ||=
      Album
      .joins(:track_monetizations)
      .where(track_monetizations: {
               id: track_monetization_ids,
               store_id: Store::YTSR_STORE_ID
             })
      .includes(:salepoints)
      .select do |album|
        !album.salepoints.pluck(:store_id).include?(Store::GOOGLE_STORE_ID)
      end
  end

  def new_track_monetizations
    load_track_monetizations.where(state: "new")
  end

  def taken_down_track_monetizations
    load_track_monetizations.where(TrackMonetization.arel_table[:takedown_at].not_eq(nil))
  end

  def track_status(track)
    return NEW_TRACK if track.has_never_been_delivered?

    if track.enqueuable?
      track.taken_down? ? NOT_MONETIZED : MONETIZED
    else
      track.taken_down? ? PROCESSING_TAKEDOWN : PROCESSING
    end
  end

  def enqueue_track_monetizations
    handle_takedowns
    deliver_new_tracks
  end

  def handle_takedowns
    takedown_params = {
      track_mons: should_takedown ? load_track_monetizations : taken_down_track_monetizations,
      takedown_source: DISTRIBUTION_ACTOR
    }.with_indifferent_access

    if should_takedown
      TrackMonetization::TakedownService.takedown(takedown_params)
    else
      TrackMonetization::TakedownService.remove_takedown(takedown_params)
    end
  end

  def deliver_new_tracks
    new_track_monetizations.each do |track|
      track.start(actor: DISTRIBUTION_ACTOR, message: DISTRIBUTION_MESSAGE) if track.check_eligibility
    end
  end
end
