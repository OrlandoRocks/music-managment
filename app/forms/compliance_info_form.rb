# frozen_string_literal: true

class ComplianceInfoForm < FormObject
  attr_reader :fields

  FIELD_DEPENDENCY_MAP = {
    TcVat::Base::INDIVIDUAL => Set["first_name", "last_name"],
    TcVat::Base::BUSINESS => Set["company_name"]
  }

  def initialize(person)
    @person = person
    @fields = ComplianceInfoFields::MetaDataService.display(person)
    add_dependency_info
    @fields.keys.map { |field_name| self.class.send(:attr_accessor, field_name) }
    super(
      @fields.each_with_object({}) do |field_item, mem|
        mem[field_item[0]] = field_item.dig(1, "field_value")
      end
    )
  end

  def add_dependency_info
    @fields.map do |field_name, field_attributes|
      field_attributes.merge!(map_dependent_fields(field_name))
    end
  end

  def map_dependent_fields(field_name)
    {
      "dependent_field_type" => "customer_type",
      "dependent_field_value" => FIELD_DEPENDENCY_MAP.index(
        FIELD_DEPENDENCY_MAP.values.find do |possible_field_values|
          possible_field_values.member?(field_name)
        end
      )
    }
  end

  def save(compliance_info_raw_params)
    form_params = compliance_info_params(compliance_info_raw_params).to_h
    editable_fields =
      form_params.reject do |form_field_name, _form_field_value|
        @fields.dig(form_field_name, "locked")
      end

    editable_fields.map do |field_name, field_value|
      field_object = ComplianceInfoField.find_or_initialize_by(
        person_id: @person.id,
        field_name: field_name
      )

      field_object.field_value = field_value
      field_object.save(
        context: disable_presence_validations?(compliance_info_raw_params)
      )

      errors.add(:base, field_name.to_s) if field_object.errors.any?
    end
  end

  private

  attr_accessor :person

  def disable_presence_validations?(opts = {})
    country = opts.fetch(:country) { person.country_iso_code }
    return unless Country::UNITED_STATES_AND_TERRITORIES.include?(country)

    :disable_validations
  end

  def compliance_info_params(params)
    params.slice(*FIELD_DEPENDENCY_MAP[params[:customer_type]&.titleize].to_a)
          .select do |form_field_name, _form_field_value|
      ComplianceInfoField::PERMITTED_FIELD_NAMES.member?(form_field_name.to_sym)
    end
  end
end
