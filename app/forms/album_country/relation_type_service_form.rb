class AlbumCountry::RelationTypeServiceForm < FormObject
  attr_accessor :album, :iso_codes, :countries

  define_model_callbacks :save, only: [:after, :before]
  before_save :load_countries
  validates_presence_of :album, :iso_codes

  def save
    return false unless valid?

    run_callbacks :save do
      AlbumCountry::RelationTypeService.update(album, countries)
    end
  end

  def load_countries
    @countries ||= Country.where(iso_code: iso_codes)
  end
end
