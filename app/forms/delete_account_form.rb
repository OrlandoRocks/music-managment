class DeleteAccountForm < FormObject
  define_model_callbacks :save

  before_save :create_deleted_account
  before_save :scrub_person
  before_save :scrub_login_events
  before_save :scrub_payout_provider
  before_save :create_note

  validates_presence_of :person_id, :delete_type, :admin
  validate :person_exists?

  attr_accessor :person_id, :delete_type, :admin

  def save
    return false unless valid?

    run_callbacks :save do
      true
    end
  end

  private

  def scrub_person
    @person.override_address_lock = true # allows us to scrub addresses of locked accounts
    @person.update!(
      name: "#{DeletedAccount::SCRUB_STRING.upcase} AS OF #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S')}",
      email: "#{DeletedAccount::SCRUB_STRING}-#{@deleted_account.id}@tunecore.com",
      address1: DeletedAccount::SCRUB_STRING,
      address2: DeletedAccount::SCRUB_STRING,
      city: DeletedAccount::SCRUB_STRING,
      state: DeletedAccount::SCRUB_STRING,
      zip: DeletedAccount::SCRUB_STRING,
      phone_number: DeletedAccount::SCRUB_STRING,
      last_logged_in_ip: "",
      status: "Locked",
      lock_reason: "#{@deleted_account.delete_type} ACCOUNT DELETE",
      deleted: 1
    )
  rescue ActiveRecord::RecordInvalid => e
    errors.add(:person_id, e.message)
    false
  end

  def scrub_login_events
    LoginEvent.where(person_id: @person.id).update_all(
      subdivision: DeletedAccount::SCRUB_STRING,
      city: DeletedAccount::SCRUB_STRING,
      country: DeletedAccount::SCRUB_STRING,
      user_agent: DeletedAccount::SCRUB_STRING,
      latitude: 0,
      longitude: 0
    )
  end

  def scrub_payout_provider
    PayoutProvider.where(person_id: @person.id).each(&:disassociate_account)
  end

  def create_note
    Note.create!(
      related: @person,
      subject: "#{@deleted_account.delete_type} LOCKED ACCOUNT PROCESSED",
      note: "#{@deleted_account.delete_type} LOCKED ACCOUNT PROCESSED",
      note_created_by: admin,
    )
  rescue ActiveRecord::RecordInvalid => e
    errors.add(:person_id, e.message)
    false
  end

  def create_deleted_account
    @deleted_account = DeletedAccount.new(
      admin_id: admin.id,
      delete_type: delete_type,
      person_id: @person.id
    )
    @deleted_account.save!
  rescue ActiveRecord::RecordNotUnique
    errors.add(:person_id, "That person has already been deleted")
    false
  end

  def person_exists?
    @person ||= Person.find(person_id)
  rescue ActiveRecord::RecordNotFound
    errors.add(:person_id, "No user with that id")
  end
end
