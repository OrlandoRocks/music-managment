# frozen_string_literal: true

class Api::PeopleFlags::MassUnflagForm < FormObject
  attr_accessor :person_ids,
                :flag_name,
                :untakedown,
                :unblock_from_distribution,
                :admin_id,
                :ip_address,
                :note
  attr_reader :valid_people

  define_model_callbacks :save

  validates_presence_of :person_ids
  validate :validate_people
  before_save :valid?

  def initialize(params = {})
    super
    @valid_people = []
  end

  def save
    run_callbacks :save do
      return false if valid_people.blank?

      ApplicationRecord.transaction do
        valid_people.each do |person|
          flag_transition = Person::FlagService.unflag!(
            person: person,
            flag_attributes: flag_attributes,
            admin_person_id: admin_id
          )

          destroy_block_from_distributions_flag(person) if unblock_from_distribution == "true"
          create_note(flag_transition) if note.present?
          unmark_person_as_suspicious_legacy_flow(person)
          untakedown_releases(person) if untakedown == "true"
        end
      end
    end
  end

  def has_invalid_people?
    errors.messages[:invalid_people].present?
  end

  private

  def destroy_block_from_distributions_flag(person)
    Person::FlagService.unflag!(
      person: person,
      flag_attributes: Flag::BLOCKED_FROM_DISTRIBUTION,
      admin_person_id: admin_id
    )
  end

  def create_note(flag_transition)
    Note.create!(
      related: flag_transition,
      note_created_by_id: admin_id,
      subject: "Remove #{flag_attributes[:name].capitalize} People Flag",
      note: note,
      ip_address: ip_address
    )
  end

  def flag_attributes
    Flag.find_by(name: flag_name).slice(:name, :category)
  end

  def store_ids
    @store_ids ||= Store.is_active.pluck(:id)
  end

  def unmark_person_as_suspicious_legacy_flow(person)
    Note.create!(
      related: person,
      note_created_by_id: admin_id,
      subject: "Suspicion Removed",
      note: note,
      ip_address: ip_address,
    ) if note.present?

    person.remove_suspicion!
  end

  def untakedown_releases(person)
    album_ids = person.albums.where.not(takedown_at: nil).pluck(:id).join(",")
    MassTakedown.mass_untakedown(album_ids, store_ids, person)
  end

  def validate_people
    person_ids.split(/\s|[,]/).reject(&:blank?).each do |account_id_or_email|
      person = find_person(account_id_or_email)

      if person.empty?
        errors.add(:invalid_people, account_id_or_email)
      else
        valid_people.concat(person)
      end
    end
  end

  def find_person(account_id_or_email)
    Person
      .where(id: account_id_or_email)
      .or(Person.where(email: account_id_or_email))
  end
end
