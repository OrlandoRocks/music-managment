# frozen_string_literal: true

class Api::PeopleFlags::MassFlagForm < FormObject
  attr_accessor :person_ids,
                :flag_reason,
                :flag_name,
                :takedown,
                :block_from_distribution,
                :admin_id,
                :ip_address,
                :note,
                :from_claims
  attr_reader :valid_people

  define_model_callbacks :save

  validates_presence_of :person_ids, :flag_reason
  validate :validate_people
  before_save :valid?

  def initialize(params = {})
    super
    @valid_people = []
  end

  def save
    run_callbacks :save do
      return false if valid_people.blank?

      ApplicationRecord.transaction do
        valid_people.each do |person|
          flag_transition = Person::FlagService.flag!(
            person: person,
            flag_attributes: flag_attributes,
            flag_reason: reason,
            admin_person_id: admin_id
          )

          create_block_from_distributions_flag(person) if block_from_distribution
          create_note(flag_transition) if note.present?
          mark_person_as_suspicious_legacy_flow(person)
          takedown_live_releases(person) if takedown
        end
      end
    end
  end

  def is_valid?
    errors.blank?
  end

  private

  def create_block_from_distributions_flag(person)
    Person::FlagService.flag!(
      person: person,
      flag_attributes: Flag::BLOCKED_FROM_DISTRIBUTION,
      admin_person_id: admin_id
    )
  end

  def create_note(flag_transition)
    Note.create!(
      related: flag_transition,
      note_created_by_id: admin_id,
      subject: "Add #{flag_attributes[:name].capitalize} People Flag",
      note: note,
      ip_address: ip_address
    )
  end

  def reason
    @reason ||= FlagReason.find_by(reason: flag_reason)
  end

  def flag_attributes
    Flag.find_by(name: flag_name)&.slice(:name, :category)
  end

  def mark_person_as_suspicious_legacy_flow(person)
    suspicious_note = Note.create!(
      related: person,
      note_created_by_id: admin_id,
      subject: "Marked as Suspicious",
      note: note,
      ip_address: ip_address,
    ) if note.present?

    person.mark_as_suspicious!(suspicious_note, flag_reason)
  end

  def takedown_live_releases(person)
    TakedownPersonReleasesWorker.perform_async(person.id, admin_id, ip_address)
  end

  def validate_people
    account_ids = person_ids.split(/\s|[,]/).reject(&:blank?)
    account_ids.each do |account_id_or_email|
      person = find_person(account_id_or_email)
      add_person_error(person, account_id_or_email)
    end
  end

  def add_person_error(person, account_id_or_email)
    if person.empty?
      errors.add(:invalid_people, account_id_or_email)
    elsif person_has_flag?(person.first)
      errors.add(:already_have_flag, account_id_or_email)
      if from_claims
        flag_person = person.first
        prev_flag = flag_person.flags.where(flag_attributes).first
        if prev_flag
          existing_flag_reason = flag_person.people_flags.where(flag_id: prev_flag.id).first.flag_reason&.reason
        end
        existing_flag_reason ||= flag_name
        errors.add(:existing_flag_reason, existing_flag_reason)
      end
    else
      valid_people.concat(person)
    end
  end

  def find_person(account_id_or_email)
    Person
      .where(id: account_id_or_email)
      .or(Person.where(email: account_id_or_email))
  end

  def person_has_flag?(person)
    person.flags.exists?(flag_attributes) || person_marked_as_suspicious?(person)
  end

  def person_marked_as_suspicious?(person)
    flag_name == Flag::SUSPICIOUS[:name] && person.suspicious?
  end
end
