class BulkAlbumApprovalForm < FormObject
  define_model_callbacks :save

  attr_accessor :album_ids, :person_id

  validates_presence_of :person_id, :album_ids

  def save
    return false unless valid?

    run_callbacks :save do
      return true if BulkAlbumApprovalService.approve(service_params)

      errors.add(:album_ids, "Form not processed")
      false
    end
  end

  private

  def service_params
    {
      person_id: person_id,
      album_ids: album_ids.reject(&:blank?)
    }
  end
end
