class SubscriptionCancellationForm < FormObject
  ALLOWED_ADMIN_ROLES = ["Subscription Admin"].map(&:freeze).freeze

  attr_accessor :subscription, :admin

  validate :admin_has_appropriate_role

  def save
    return false unless valid?

    cancel_person_subscription
  end

  private

  def admin_has_role?
    ALLOWED_ADMIN_ROLES.any? { |role_name| admin.has_role?(role_name) }
  end

  def admin_has_appropriate_role
    errors.add(:admin, "has inappropriate role") unless admin_has_role?
  end

  def cancel_person_subscription
    subscription_request = Subscription::SubscriptionRequest.cancel(subscription.person, subscription.subscription_type)
    if subscription_request.success
      true
    else
      errors.add(:subscription_request, "error processing request")
      false
    end
  end
end
