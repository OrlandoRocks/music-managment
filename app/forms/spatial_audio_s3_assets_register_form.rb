class SpatialAudioS3AssetsRegisterForm < UploadsRegisterForm
  define_model_callbacks :save
  before_save :process_register

  attr_accessor :old_spatial_audio_asset

  def initialize(params)
    super
    load_song
    load_old_spatial_audio_asset
  end

  def save
    return false unless valid?

    run_callbacks :save do
      if song.valid? && song.save && save_spatial_audio_asset
        clear_old_spatial_audio_asset
        true
      else
        false
      end
    end
  end

  private

  def process_register
    return false unless build_s3_asset

    song.last_errors = nil
  end

  def load_old_spatial_audio_asset
    @old_spatial_audio_asset = song.spatial_audio_asset && {
      key: song.spatial_audio_asset&.key,
      bucket: song.spatial_audio_asset&.bucket
    }
  end

  def build_s3_asset
    if song.spatial_audio_asset
      @s3_asset         = song.spatial_audio_asset
      @s3_asset.bucket  = bucket
      @s3_asset.key     = key
      @s3_asset.uuid    = uuid
    else
      @s3_asset = S3Asset.new(bucket: bucket, key: key, uuid: uuid)
    end

    @s3_asset.valid?
  end

  def save_spatial_audio_asset
    @s3_asset.save

    song.spatial_audio_asset = @s3_asset
  end

  def clear_old_spatial_audio_asset
    return unless Rails.env.production? &&
                  old_spatial_audio_asset.present? &&
                  FeatureFlipper.show_feature?(:delete_assets_after_reuploading, person.id)

    AssetCleanupWorker.perform_async(
      old_spatial_audio_asset[:key],
      old_spatial_audio_asset[:bucket]
    ) if old_spatial_audio_asset[:key].present? && old_spatial_audio_asset[:bucket].present?
  end
end
