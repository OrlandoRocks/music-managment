class AlbumSalepointsForm < FormObject
  define_model_callbacks :save

  attr_accessor :album,
                :album_id,
                :store_ids,
                :person,
                :deliver_automator,
                :apple_music

  attr_reader :built_salepoints,
              :saved_salepoints,
              :itunes_ww,
              :itunes_us,
              :itunes_ww_salepoints,
              :itunes_us_salepoints,
              :is_itunes_upgrade,
              :aod_just_added

  validates_presence_of :album
  validate :store_ids_is_an_arry?

  def initialize(options)
    super(options)

    @album = person&.albums&.find_by(id: album_id) || Album.find_by(id: album_id)
  end

  def save
    return false unless valid?

    run_callbacks :save do
      return true if Salepoint.transaction do
        prune_salepoints

        build_salepoints

        save_salepoints

        update_automator_for_album

        update_apple_music_for_album

        true
      end

      false
    end
  end

  private

  def store_ids_is_an_arry?
    errors.add(:store_id, "must be an array") unless store_ids.is_a?(Array)
  end

  def prune_salepoints
    album.salepoints.where.not(store_id: store_ids).destroy_all
  end

  def build_salepoints
    @built_salepoints =
      store_ids.map do |store_id|
        build_salepoint(store_id)
      end
  end

  def build_salepoint(store_id)
    salepoint = Salepoint.find_by(salepointable: album, store_id: store_id)
    store = Store.find(store_id)

    variable_price = salepoint ? salepoint.variable_price : store.default_variable_price

    salepoint ||= Salepoint.new(salepointable: album, store_id: store_id)

    salepoint.assign_attributes(
      variable_price: variable_price,
      has_rights_assignment: store.needs_rights_assignment
    )

    salepoint
  end

  def save_salepoints
    setup_itunes_variables

    @saved_salepoints =
      built_salepoints.map do |salepoint|
        handle_itunes_upgrade(salepoint)

        unless salepoint.save
          errors.add(:salepoints, "Failed to add salepoints")

          raise ActiveRecord::Rollback
        end

        salepoint
      end
  end

  def setup_itunes_variables
    @itunes_ww            = Store.find_by(short_name: "iTunesWW")
    @itunes_us            = Store.find_by(short_name: "iTunesUS")
    @itunes_ww_salepoints = built_salepoints.select { |sp| sp.store_id == itunes_ww.id }
    @itunes_us_salepoints = built_salepoints.select { |sp| sp.store_id == itunes_us.id }
    @is_itunes_upgrade    = upgrading_to_itunes_ww?
  end

  def upgrading_to_itunes_ww?
    itunes_ww_salepoints.present? && itunes_ww_salepoints.first.new_record? && itunes_us_salepoints.present?
  end

  def handle_itunes_upgrade(salepoint)
    use_us_variable_pricing(salepoint) if itunes_ww_salepoint?(salepoint) && is_itunes_upgrade
  end

  def use_us_variable_pricing(salepoint)
    salepoint.variable_price       = itunes_us_salepoints.first.variable_price
    salepoint.track_variable_price = itunes_us_salepoints.first.track_variable_price
  end

  def itunes_ww_salepoint?(salepoint)
    salepoint.store_id == itunes_ww.id
  end

  def update_automator_for_album
    Album::AutomatorService.update_automator(album, deliver_automator) if deliver_automator.to_s.present?
  end

  def update_apple_music_for_album
    album.apple_music =
      if itunes_ww_salepoints || itunes_us_salepoints
        apple_music
      else
        false
      end

    album.save!
  end
end
