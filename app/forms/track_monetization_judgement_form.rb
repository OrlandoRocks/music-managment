class TrackMonetizationJudgementForm < FormObject
  attr_accessor :track_monetizations, :current_user

  validates_presence_of :track_monetizations

  define_model_callbacks :save

  def save
    return false unless valid?

    run_callbacks :save do
      return true if judge_track_monetizations

      errors.add(:base, "Something went wrong")
    end
  end

  private

  def judge_track_monetizations
    TrackMonetizationJudgementService.judge(params.merge(current_user: current_user))
  end

  def params
    {
      eligible_track_monetizations: eligible_track_monetizations,
      ineligible_track_monetizations: ineligible_track_monetizations,
      pending_track_monetizations: pending_track_monetizations
    }
  end

  def eligible_track_monetizations
    select_by_eligibility_status("eligible")
  end

  def ineligible_track_monetizations
    select_by_eligibility_status("ineligible")
  end

  def pending_track_monetizations
    select_by_eligibility_status("pending")
  end

  def select_by_eligibility_status(eligibility_status)
    ids =
      track_monetizations.keys.select do |track_id|
        track_monetizations[track_id]["eligibility_status"] == eligibility_status
      end

    TrackMonetization.where(id: ids) if ids.present?
  end
end
