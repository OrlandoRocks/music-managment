class PersonTransaction::ExportForm < FormObject
  validates_presence_of :transactions, :person

  attr_accessor :transactions, :sales_records, :person, :filename, :csv

  def save
    valid? ? true : false
  end

  def csv
    @csv ||= PersonTransaction.generate_csv(transactions, sales_records)
  end

  def filename
    @filename ||= "tunecore-transactions_#{localized_time}"
  end

  def localized_time
    I18n.l(Time.now, format: :underscore)
  end

  def sales_records
    return [] if sales_disabled?

    PersonTransaction.with_sales_record_information(
      person.id, transactions
    ).group_by(&:transaction_id)
  end

  def sales_disabled?
    FeatureFlipper.show_feature?(:disable_sales, person)
  end
end
