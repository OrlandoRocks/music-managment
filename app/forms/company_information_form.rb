# frozen_string_literal: true

class CompanyInformationForm < FormObject
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  TRANSLATION_KEY = "people.company_info"

  delegate :enterprise_number,
           :company_registry_data,
           :place_of_legal_seat,
           :registered_share_capital,
           to: :company_information

  validate :verify_current_password
  validate :verify_company_information
  validate :verify_vat_flag

  def initialize(person)
    @person = person
  end

  def submit(params)
    self.current_password = params.delete(:current_password)
    company_information.attributes = params.each_value(&:strip!)

    return false if invalid?

    company_information.save!
  end

  def translated_errors
    errors.each_with_object({}) do |(attr, msg), err_hash|
      t_attr = custom_t "#{TRANSLATION_KEY}.fields.#{attr}"
      t_msg = custom_t "#{TRANSLATION_KEY}.errors.#{msg}"

      err_hash[attr] = %(#{t_attr} #{t_msg})
    end
  end

  private

  attr_accessor :current_password
  attr_reader :person

  def company_information
    @company_information ||=
      person.company_information || person.build_company_information
  end

  def verify_current_password
    return if person.is_password_correct? current_password

    errors.add(:current_password, "is_incorrect")
  end

  def verify_company_information
    return if company_information.valid?

    company_information.errors.each do |attr, msg|
      errors.add(attr, msg)
    end
  end

  def verify_vat_flag
    return if person.vat_feature_on?

    errors.add(:base, "vat_tax flag is disabled")
  end
end
