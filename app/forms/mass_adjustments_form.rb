class MassAdjustmentsForm < FormObject
  attr_accessor :admin_message, :customer_message, :file, :negative_values, :positive_values, :current_user, :csv,
                :category
  attr_reader :batch

  validates_presence_of :file, :admin_message, :customer_message, :category
  validate :validate_csv_file
  validate :duplicate_request

  PERSON_ID = "person_id"
  CORRECTION_AMOUNT = "correction_amount"

  def save
    return false unless valid?

    s3_file_name = upload_to_s3
    @batch = create_batch(s3_file_name)
    MassBalanceAdjustment::BatchCsvWorker.perform_async(@batch.id)
  end

  private

  def validate_csv_file
    return if file.blank?
    errors.add(:csv_file, "should have .csv extension") and return if File.extname(file.original_filename) != ".csv"

    person_id_header = PERSON_ID
    correction_amount_header = CORRECTION_AMOUNT
    @csv_contents = file.read
    @csv = CSV.parse(@csv_contents, headers: true)
    return if @csv.headers == [person_id_header, correction_amount_header]

    errors.add(:csv_file, "headers should be #{PERSON_ID}, #{correction_amount_header}")
  end

  def upload_to_s3
    file_name = Time.now.strftime("%Y-%m-%d-%H-%M-%S-%L") + ".csv"
    file_path = "#{Rails.env}/#{file_name}"
    object = S3_MASS_BALANCE_ADJUSTMENT_CLIENT
             .buckets[ENV["MASS_BALANCE_ADJUSTMENT_BUCKET"]]
             .objects[file_path]
    object.write(@csv_contents)
    file_name
  end

  def create_batch(s3_file_name)
    MassAdjustmentBatch.create(
      admin_message: admin_message,
      customer_message: customer_message,
      csv_file_name: file.original_filename,
      created_by_id: current_user.id,
      status: MassAdjustmentBatch::NEW,
      s3_file_name: s3_file_name,
      balance_adjustment_category_id: BalanceAdjustmentCategory.find_by(name: category)&.id
    )
  end

  def duplicate_request
    last_batch = MassAdjustmentBatch.last
    return if last_batch.blank?

    is_duplicate_request = (
      last_batch&.admin_message == admin_message &&
      last_batch&.customer_message == customer_message &&
      last_batch&.csv_file_name == file&.original_filename
    )
    errors.add(
      :csv_file,
      "looks like a duplicate request. Please review the data you are submitting"
    ) if is_duplicate_request
  end
end
