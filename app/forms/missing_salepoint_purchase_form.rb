class MissingSalepointPurchaseForm < FormObject
  attr_accessor :store_name, :person

  define_model_callbacks :save
  validates_presence_of :person, :store_name
  validate :valid_store

  def save
    return false unless valid?

    run_callbacks :save do
      Tunecore::StoreManager.create_store_purchases(person, store.id.to_s => albums.pluck(:id))
    end
  end

  private

  def albums
    @albums ||= Tunecore::StoreManager.qualified_albums(person, store)
  end

  def store
    @store ||= Store.is_active.is_used.find_by(short_name: store_name)
  end

  def valid_store
    errors.add(I18n.t("store"), I18n.t("errors.messages.invalid")) unless store
  end
end
