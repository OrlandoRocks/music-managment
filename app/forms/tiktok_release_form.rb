class TiktokReleaseForm < AlbumForm
  STORE_IDS = [
    Store::TIK_TOK_STORE_ID
  ].freeze

  def initialize(options)
    super(options)

    build_salepoints
  end

  private

  def build_salepoints
    STORE_IDS.each do |store_id|
      album.salepoints.find_or_initialize_by(store_id: store_id)
    end
  end
end
