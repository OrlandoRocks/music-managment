# TODO: this appears to be deprecated, create ticket to investigate and remove from codebase

class PayoutProvider::LoginForm < FormObject
  include Rails.application.routes.url_helpers

  attr_accessor :person, :provider_name, :request

  define_model_callbacks :save
  validates_presence_of :person
  before_save :send_api_request

  def save
    return false unless valid?

    run_callbacks :save do
      payout_provider.update(provider_status: PayoutProvider::ONBOARDING, provider_link: link)
    end
  end

  def link
    response_json["login_link"]
  end

  private

  def response_json
    JSON.parse(response.body)
  end

  attr_reader :response

  def api_client
    case payout_provider.name
    when "payoneer"
      Payoneer::PayoutApiClientShim.fetch_for(person)
    else
      payout_provider.name.classify.constantize::PayoutApiClient
    end
  end

  # TODO: get rid of metaprogramming below.
  def send_api_request
    # api_client = payout_provider.name.classify.constantize::PayoutApiClientShim
    @response = PayoutProvider::ApiService.new(
      person: person,
      api_client: api_client,
    ).register_existing_user(
      person: person,
      redirect_url: payout_provider_login_landings_url(host: person.country_website.url),
    )
  end

  def payout_provider
    @payout_provider ||= PayoutProvider.where(
      tunecore_status: PayoutProvider::ACTIVE,
      person_id: person.id,
      name: provider_name,
      active_provider: true
    ).first_or_create
  end
end
