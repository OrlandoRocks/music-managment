# frozen_string_literal: true

class PayoutProvider::TaxFormRevenueStreamMapperForm < FormObject
  attr_accessor :person, :mappings

  validates :person, :mappings, presence: true
  validate :validate_tax_form_mappings

  VALID_TAXFORM_IDENTIFIERS = [
    "primary",
    "secondary"
  ].freeze

  def save
    return false unless valid?

    map_tax_forms_to_revenue_streams!
  end

  private

  def map_tax_forms_to_revenue_streams!
    TaxReports::TaxFormMappingService.call!(person.id, kind: :user, mapping_definition: mapping_definition)
  end

  def mapping_definition
    [
      {
        revenue_stream: RevenueStream.publishing,
        tax_form: person.send("#{mappings[:publishing].downcase}_tax_form")
      },
      {
        revenue_stream: RevenueStream.distribution,
        tax_form: person.send("#{mappings[:distribution].downcase}_tax_form")
      }
    ]
  end

  def validate_tax_form_mappings
    return if valid_mapping_params? && valid_tax_forms_exist?

    errors.add(:base, "invalid mapping")
  end

  def valid_mapping_params?
    mappings&.dig(:publishing).in?(VALID_TAXFORM_IDENTIFIERS) &&
      mappings&.dig(:distribution).in?(VALID_TAXFORM_IDENTIFIERS)
  end

  def valid_tax_forms_exist?
    person&.primary_tax_form && person&.secondary_tax_form
  end
end
