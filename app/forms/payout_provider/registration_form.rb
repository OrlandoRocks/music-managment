class PayoutProvider::RegistrationForm < FormObject
  include Rails.application.routes.url_helpers
  include Payoneer::Configurable

  attr_accessor :person, :currency, :request, :payout_methods_list, :program_id

  define_model_callbacks :save
  validates_presence_of :person
  validates :currency, inclusion: { in: PayoutProvider::WITHDRAWAL_CURRENCIES }, allow_nil: true
  validates :program_id, inclusion: { in: PayoutProviderConfig::PROGRAM_IDS }, allow_nil: true
  before_save :send_api_request

  def save
    return false unless valid?

    run_callbacks :save do
      payout_provider.update(provider_link: link)
    end
  end

  def link
    response.registration_link
  end

  private

  attr_reader :response

  def api_client
    case payout_provider.name
    when "payoneer"
      Payoneer::PayoutApiClientShim.fetch_for(person)
    else
      payout_provider.name.classify.constantize::PayoutApiClient
    end
  end

  def send_api_request
    # api_client = payout_provider.name.classify.constantize::PayoutApiClientShim
    @response = PayoutProvider::ApiService.new(
      person: person,
      api_client: api_client,
      program_id: program_id,
    ).register_new_user(
      payout_methods_list: payout_methods_list,
      person: person,
      redirect_url: payout_provider_registration_landings_url(host: person.country_website.url)
    )
  end

  def payout_provider
    @payout_provider ||= PayoutProvider.find_or_create_by(
      provider_status: PayoutProvider::ONBOARDING,
      tunecore_status: PayoutProvider::ACTIVE,
      person_id: person.id,
      name: PayoutProvider::PAYONEER,
      active_provider: true,
      currency: currency,
    )
    @payout_provider.update(payout_provider_config: payout_provider_config)

    @payout_provider
  end
end
