class PayoutProvider::DisassociationForm < FormObject
  attr_accessor :provider_id, :admin

  validates_presence_of :provider_id, :admin
  validate :admin_has_role?

  def save
    return false unless valid?

    disassociate_account
  end

  private

  def disassociate_account
    payout_provider = PayoutProvider.find(provider_id)

    payout_provider.disassociate_account
  end

  def admin_has_role?
    errors.add(:admin, "does not have required payout admin role") unless admin.has_role?("Payout Admin")
  end
end
