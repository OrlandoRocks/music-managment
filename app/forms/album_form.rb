class AlbumForm < FormObject
  include ActiveModel::Validations::Callbacks

  define_model_callbacks :save

  after_save :cart_artists_added_post_purchase

  validates_with AlbumFormValidator

  ALBUM = "Album".freeze
  SINGLE = "Single".freeze
  ALBUM_TYPES = [ALBUM, SINGLE].freeze

  NULLABLE_ATTRIBUTES = %w[label_name].freeze

  attr_accessor :album, :album_id, :album_type, :album_params, :person, :skip_special_salepoints,
                :spotify_artists_required
  attr_reader :album_attributes

  def initialize(options)
    super(options)
    album.assign_attributes(album_attributes)

    build_special_salepoints unless skip_special_salepoints

    return unless is_single

    build_song
  end

  def save
    return false unless valid?

    set_default_label_name

    run_callbacks :save do
      album.save
    end
  end

  def is_single
    @is_single ||= album.single?
  end

  def is_new_release
    @is_new_release ||= album_id.nil?
  end

  def album
    @album ||= person&.public_send(album_type.underscore.pluralize)&.build || album_type.constantize.new
  end

  def album_id
    @album_id ||= album.id
  end

  def album_type
    return @album_type if ALBUM_TYPES.include?(@album_type)

    @album_type = ALBUM_TYPES.include?(album_attributes[:album_type]) ? album_attributes[:album_type] : ALBUM
  end

  def prune_album_attributes(attrs)
    missing_keys = NULLABLE_ATTRIBUTES.select { |k| attrs.with_indifferent_access.key?(k) == false }.map(&:to_sym)
    attrs.except(*missing_keys)
  end

  def album_attributes
    return @album_attributes if @album_attributes

    initial_album_attributes = album_params&.except(:specialized_release_type) || {}

    @album_attributes = prune_album_attributes(initial_album_attributes).with_indifferent_access

    if @album_attributes[:album_type] == ALBUM
      @album_attributes.except(:optional_isrc, :parental_advisory, :clean_version, :specialized_release_type)
    else
      @album_attributes
    end
  end

  private

  def build_song
    album.song.name = album.name
    album.song.optional_isrc = album.optional_isrc
  end

  def set_default_label_name
    album.set_default_label_name if @album_attributes.key?(:label_name)
  end

  def build_special_salepoints
    return if !all_discovery_platforms_enabled || album.persisted?

    Store.where(discovery_platform: true).each do |store|
      album.salepoints.find_or_initialize_by(
        store_id: store.id,
        variable_price_id: store.default_variable_price,
      )
    end
  end

  def all_discovery_platforms_enabled
    FeatureFlipper.show_feature?(:discovery_platforms, person)
  end

  # This also carts artists needed by any carted releases, but that's OK
  def cart_artists_added_post_purchase
    return if album.takedown_at.present?
    return unless album.payment_applied?

    Plans::AdditionalArtistService.adjust_additional_artists(person)
  end
end
