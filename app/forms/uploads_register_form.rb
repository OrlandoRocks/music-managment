class UploadsRegisterForm < FormObject
  define_model_callbacks :save
  before_save :process_register

  validates_presence_of :person,
                        :song_id,
                        :bucket,
                        :key,
                        :uuid,
                        :orig_filename,
                        :song

  validates :bit_rate, presence: true, unless: proc { |form|
    form.is_a? SpatialAudioS3AssetsRegisterForm
  }
  validate :song_belongs_to_person?
  validates_with Utf8mb3Validator, fields: [:orig_filename]

  attr_accessor :person,
                :song_id,
                :bucket,
                :key,
                :uuid,
                :orig_filename,
                :bit_rate,
                :song,
                :old_s3_assets,
                :duration

  def initialize(params)
    super
    load_song
    load_old_s3_assets
  end

  def save
    return false unless valid?

    run_callbacks :save do
      if song.valid? && song.save && @s3_asset.save && @upload.save
        clear_old_s3_assets
        true
      else
        false
      end
    end
  end

  private

  def load_song
    @song = Song.find(song_id)
  end

  def load_old_s3_assets
    @old_s3_assets = [
      {
        key: song.s3_flac_asset&.key,
        bucket: song.s3_flac_asset&.bucket
      },
      {
        key: song.s3_orig_asset&.key,
        bucket: song.s3_orig_asset&.bucket
      },
      {
        key: song.s3_asset&.key,
        bucket: song.s3_asset&.bucket
      }
    ].uniq
  end

  def song_belongs_to_person?
    return false if song.album.person_id != person.id && !person.is_administrator?
  end

  def process_register
    return false unless build_s3_asset && build_upload

    song.last_errors = nil
    song.s3_asset    = @s3_asset
    song.s3_orig_asset = @s3_asset
    song.duration_in_seconds = duration / 1000 if duration
    destroy_s3_flac_asset! if song.s3_flac_asset

    @upload.uploaded_filename = @s3_asset.key.split("/").last if @s3_asset.uuid.present?
  end

  def build_s3_asset
    if song.s3_asset
      @s3_asset         = song.s3_asset
      @s3_asset.bucket  = bucket
      @s3_asset.key     = key
      @s3_asset.uuid    = uuid
    else
      @s3_asset = S3Asset.new(bucket: bucket, key: key, uuid: uuid)
    end

    @s3_asset.valid?
  end

  def build_upload
    if song.upload
      @upload                   = song.upload
      @upload.song_id           = song_id
      @upload.uploaded_filename = UUIDTools::UUID.random_create.to_s.to_s
      @upload.original_filename = formatted_orig_filename
      @upload.bitrate           = bit_rate
    else
      @upload = Upload.new(
        {
          song_id: song_id,
          uploaded_filename: UUIDTools::UUID.random_create.to_s.to_s,
          original_filename: formatted_orig_filename,
          bitrate: bit_rate
        }
      )
    end

    @upload.valid?
  end

  def destroy_s3_flac_asset!
    flac_asset = song.s3_flac_asset
    ActiveRecord::Base.transaction do
      flac_asset.destroy
      song.update(s3_flac_asset_id: nil)
    end
  end

  def split_file_name
    orig_filename.split("-")
  end

  def formatted_orig_filename
    split_file_name[1..split_file_name.size].join
  end

  def clear_old_s3_assets
    return unless Rails.env.production? &&
                  old_s3_assets.present? &&
                  FeatureFlipper.show_feature?(:delete_assets_after_reuploading, person.id)

    old_s3_assets.each do |asset|
      next unless asset[:key].present? && asset[:bucket].present?

      AssetCleanupWorker.perform_async(asset[:key], asset[:bucket])
    end
  end
end
