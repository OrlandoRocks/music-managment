class PayoutTransfer::AdminFilterForm < FormObject
  attr_accessor :tunecore_status,
                :provider_status,
                :filter_type,
                :filter_amount,
                :page,
                :payout_transfers,
                :approvals_map,
                :withdrawal_method,
                :person_name,
                :actionable_transfers,
                :pagination_limit,
                :changed_payout_method_ids,
                :currency,
                :start_date,
                :end_date,
                :rollback,
                :auto_approval_eligible

  validate :valid_filter_type?
  validate :valid_withdrawal_method?

  SELECT_OPTIONS = [["---", ""], [">", ">"], ["<", "<"], ["=", "="]]

  PAPER_CHECK_DROPDOWN  = "Paper Check".freeze
  PAYPAL_DROPDOWN       = "Paypal".freeze
  ACCOUNT_DROPDOWN      = "Payoneer Account".freeze
  PREPAID_CARD_DROPDOWN = "Prepaid Card".freeze

  USD                   = "USD".freeze
  CAD                   = "CAD".freeze
  GBP                   = "GBP".freeze
  AUD                   = "AUD".freeze
  EUR                   = "EUR".freeze

  PAYOUT_METHOD_OPTIONS = [
    ["---", ""],
    [PAPER_CHECK_DROPDOWN, PayoutTransfer::PAPER_CHECK],
    [PAYPAL_DROPDOWN, PayoutTransfer::PAYPAL],
    [ACCOUNT_DROPDOWN, PayoutTransfer::ACCOUNT],
    [PREPAID_CARD_DROPDOWN, PayoutTransfer::PREPAID_CARD]
  ]
  CURRENCY_OPTIONS = [
    ["---", ""],
    [USD, USD],
    [CAD, CAD],
    [GBP, GBP],
    [AUD, AUD],
    [EUR, EUR]
  ]
  RECORDS_PER_PAGE = [50, 100, 300, 500, 1000]

  def save(paginated: true)
    return false unless valid?

    paginated ? load_paginated_payout_transfers : load_payout_transfers
    load_approvals_map
    load_actionable_transfers
    load_most_recent_transactions_for_each_user
    true
  end

  private

  def load_paginated_payout_transfers
    @payout_transfers ||= Payoneer::PayoutTransferQueryBuilder.build(build_params)
                                                              .order("payout_transfers.created_at ASC")
                                                              .paginate(page: page || 1, per_page: pagination_limit || 50)
  end

  def load_payout_transfers
    @payout_transfers ||= Payoneer::PayoutTransferQueryBuilder.build(build_params)
                                                              .order("payout_transfers.created_at ASC")
  end

  def build_params
    {
      person_name: person_name,
      withdrawal_method: withdrawal_method,
      filter_type: filter_type,
      filter_amount: filter_amount,
      tunecore_status: tunecore_status,
      provider_status: provider_status,
      currency: currency,
      start_date: start_date,
      end_date: end_date,
      rollback: rollback_value,
      auto_approval_eligible: auto_approval_eligible
    }
  end

  def load_actionable_transfers
    @actionable_transfers = @payout_transfers.where(tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE)
  end

  def load_approvals_map
    provider_ids = payout_transfers.map(&:payout_provider_id)
    @approvals_map = first_time_users(provider_ids)
  end

  def valid_filter_type?
    valid_options = SELECT_OPTIONS.map { |item| item[1] }
    return if valid_options.include?(filter_type) || filter_type.nil?

    errors.add(:payout_transfer, "not a valid filter type")
  end

  def valid_withdrawal_method?
    return if PayoutTransfer::WITHDRAWAL_OPTIONS.include?(withdrawal_method) || withdrawal_method.blank?

    errors.add(:payout_transfer, "not a valid withdrawal type")
  end

  def first_time_users(paginated_payout_provider_ids)
    PayoutTransfer.select("payout_provider_id")
                  .where(tunecore_status: PayoutTransfer::APPROVED)
                  .where(payout_provider_id: paginated_payout_provider_ids)
                  .group(:payout_provider_id)
                  .count
  end

  def load_most_recent_transactions_for_each_user
    ids = payout_transfers.map(&:id).join(",")
    return if ids.blank?

    sql = <<-SQL.strip_heredoc
      SELECT pt1.id FROM payout_transfers pt1
        INNER JOIN (
          SELECT MAX(id) AS max_id
          FROM payout_transfers WHERE id IN (#{ids})
          GROUP BY payout_provider_id
        ) AS pt2
        ON pt1.id = pt2.max_id
    SQL
    most_recent_payouts = ActiveRecord::Base.connection.execute(sql).to_a.flatten
    @changed_payout_method_ids = PayoutTransfer.where(id: most_recent_payouts, withdraw_method_changed: true).pluck(:id)
  end

  def rollback_value
    rollback == "true"
  end
end
