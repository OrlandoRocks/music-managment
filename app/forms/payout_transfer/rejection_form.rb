class PayoutTransfer::RejectionForm < FormObject
  attr_accessor :payout_transfer, :admin

  validates_presence_of :payout_transfer, :admin
  validate :can_be_rejected?

  define_model_callbacks :save

  before_save :set_payout_transfer_attributes
  before_save :process_refund
  after_save  :send_email

  def save
    return false unless valid?

    run_callbacks :save do
      true
    end
  end

  private

  def set_payout_transfer_attributes
    payout_transfer.attributes = {
      tunecore_processor: admin,
      tunecore_status: PayoutTransfer::REJECTED,
      provider_status: PayoutTransfer::NONE,
      tunecore_processed_at: DateTime.now
    }
  end

  def payout_provider
    @payout_provider ||= person.payout_provider
  end

  def can_be_rejected?
    return if payout_transfer.try(:submitted?)

    errors.add(:tunecore_status, "incorrect for transfer ID: #{payout_transfer.try(:id)}")
  end

  def person
    @person ||= payout_transfer.person
  end

  def process_refund
    PayoutTransfer::Refund.refund(payout_transfer: payout_transfer)
  end

  def send_email
    MailerWorker.perform_async(
      "PayoutTransferMailer",
      :mail_for_rejected_transfer,
      payout_transfer.id
    )
  end
end
