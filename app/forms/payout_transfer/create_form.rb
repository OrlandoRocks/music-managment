class PayoutTransfer::CreateForm < FormObject
  include PayoneerFeesHelper
  define_model_callbacks :save

  attr_accessor :person, :amount_cents_raw, :amount, :version
  attr_reader :payout_transfer, :payee_details, :payee_status

  validates :amount_cents_raw, numericality: { greater_than: 0 }
  validate :person_has_available_balance
  validate :has_valid_requested_amount
  validate :validate_withdrawal_type

  after_save :send_email

  def initialize(params)
    super(params)
    @version = FeatureFlipper.show_feature?(:use_payoneer_v4_api, person) ? :v4 : :v2
    @payee_details = get_payee_details
    @payee_status = get_payee_status if version == :v4
    record_payoneer_country
  end

  def save
    return false unless valid?

    run_callbacks :save do
      create_payout_transfer
    end
  end

  delegate :payout_provider, to: :person

  def fees
    fee_options = {
      domain: person.country_domain,
      payoneer_account_domain: provider_domain,
      withdrawal_amount: amount_input,
      withdrawal_type: provider_withdrawal_type,
      person: person,
      payee_details: @payee_details
    }

    fee = Payoneer::FeeCalculatorService.calculate(**fee_options)

    unless fee
      fee = 0

      Airbrake.notify(
        "Unable to calculate fee!",
        fee_options.merge(person_id: person.id)
      )
    end

    Money.new(fee, person.currency)
  end

  def transfer_amount
    Money.new(amount_input - fees.cents, person.currency)
  end

  def requested_amount
    Money.new(amount_input, person.currency)
  end

  def available_balance
    Money.new((person_balance.balance * 100).to_i - amount_input, person.currency)
  end

  def transfer_amount_valid?
    valid_withdrawal_type? and transfer_amount.cents >= minimum_net_transfer_amount_in_cents(provider_withdrawal_type)
  end

  def provider_withdrawal_type
    case version
    when :v2
      payee_details.withdrawal_type
    when :v4
      payee_status.withdrawal_type
    end
  end

  def provider_domain
    @payee_details.country
  end

  def bank_withdrawal?
    provider_withdrawal_type == PayoutTransfer::BANK
  end

  private

  attr_writer :payout_transfer

  def valid_withdrawal_type?
    PayoutTransfer::WITHDRAWAL_OPTIONS.include?(provider_withdrawal_type)
  end

  def validate_withdrawal_type
    errors.add(:withdrawal_type, "unknown_withdrawal_type") unless valid_withdrawal_type?
  end

  def send_email
    MailerWorker.perform_async(
      "PayoutTransferMailer",
      :mail_for_confirmed_transfer,
      payout_transfer.id
    )
  end

  def person_has_available_balance
    errors.add(:balance, "unavailable_balance") unless balance_available?
  end

  def has_valid_requested_amount
    errors.add(:amount, "invalid_net_requested_amount") unless transfer_amount_valid?
  end

  def balance_available?
    available_balance >= Money.new(0, person.currency)
  end

  def amount_cents
    @amount_cents ||= Money.new(amount_input, person.currency).cents
  end

  def amount_input
    amount_cents_raw.to_i
  end

  def person_balance
    @person_balance ||= person.person_balance
  end

  def create_payout_transfer
    self.payout_transfer = build_payout_transfer
    ActiveRecord::Base.transaction do
      person_balance.update_balance(debit_amount) if payout_transfer.save
    end
  end

  def build_payout_transfer
    payout_transfer = person.payout_provider.payout_transfers.build(
      amount_cents: transfer_amount.cents,
      fee_cents: fees.cents,
      tunecore_status: PayoutTransfer::SUBMITTED,
      provider_status: PayoutTransfer::NONE,
      transaction_type: "debit",
      currency: person.currency || "USD",
      withdraw_method: provider_withdrawal_type,
      person_id: person.id
    )
    build_person_transaction(payout_transfer)
    payout_transfer
  end

  def build_person_transaction(payout_transfer)
    payout_transfer.payout_transactions.build(
      person_id: person.id,
      debit: payout_transfer.requested_amount,
      currency: person.currency,
      comment: parsed_payout_method(payout_transfer),
      previous_balance: person_balance.balance
    )
  end

  def debit_amount
    -requested_amount.to_d
  end

  def parsed_payout_method(payout_transfer)
    "Payout Transaction - #{PayoutTransfer::PAYOUT_METHODS[payout_transfer.withdraw_method]}" ||
      PayoutTransfer::PAYOUT_METHODS.withdraw_method
  end

  def get_payee_details
    PayoutProvider::ApiService.new(
      person: person,
      api_client: Payoneer::PayoutApiClientShim.fetch_for(person),
    ).payee_extended_details(person: person)
  end

  def get_payee_status
    PayoutProvider::ApiService.new(
      person: person,
      api_client: Payoneer::PayoutApiClientShim.fetch_for(person),
    ).payee_status(person: person)
  end

  def record_payoneer_country
    country_id = Country.search_by_iso_code(provider_domain)&.id

    if country_id.present?
      PersonCountry.record_country(
        PersonCountry.sources[:payoneer],
        person.id,
        country_id,
      )
    else
      airbrake_details = {
        person: person.sanitize!,
        payee_details: @payee_details
      }
      Airbrake.notify("Unknown ISO code from Payoneer - #{provider_domain}", airbrake_details)
    end
  end
end
