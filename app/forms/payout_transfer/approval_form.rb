class PayoutTransfer::ApprovalForm < FormObject
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  ALLOWED_ADMIN_ROLES = ["Payout Service", "EFT"].map(&:freeze).freeze

  define_model_callbacks :save

  attr_accessor :payout_transfer, :admin, :referrer_url

  validate :admin_has_appropriate_role
  validate :can_be_approved
  validate :tax_info_status

  VAT_TAX = "VAT".freeze

  def save
    return false unless valid?

    build_tax_adjustment unless @tax_info.empty?

    run_callbacks :save do
      self.response = submit_payout
      response.success? ? approve_transfer : record_failed_request
    end
  end

  private

  attr_accessor :response

  def api_client
    case payout_provider.name
    when "payoneer"
      Payoneer::PayoutApiClientShim.fetch_for(person)
    else
      payout_provider.name.classify.constantize::PayoutApiClient
    end
  end

  def submit_payout
    # api_client = payout_provider.name.classify.constantize::PayoutApiClient
    PayoutProvider::ApiService.new(
      person: person,
      api_client: api_client,
    ).submit_payout(
      transfer: payout_transfer,
      person: person,
      referrer_url: referrer_url
    )
  end

  def record_failed_request
    process_refund_and_set_error
    errors.add(:payout_transfer, "#{response.description} for transfer ID: #{payout_transfer.id}")
    send_email(:mail_for_failed_transfer)
    false
  end

  def payout_provider
    @payout_provider ||= person.payout_provider
  end

  def can_be_approved
    return if payout_transfer.submitted? && payout_transfer.provider_status == PayoutTransfer::NONE

    errors.add(:tunecore_status, "is wrong status for transfer ID: #{payout_transfer.id}")
  end

  def admin_has_appropriate_role
    errors.add(:admin, "has inappropriate role") unless ALLOWED_ADMIN_ROLES.any? do |role_name|
      admin.has_role?(role_name)
    end
  end

  def tax_info_status
    errors.add(:base, custom_t(tax_info[:errors])) if tax_info[:errors]
  end

  def person
    @person ||= payout_transfer.person
  end

  def process_refund_and_set_error
    payout_transfer.update(
      tunecore_processed_at: Time.now,
      tunecore_processor: admin,
      tunecore_status: PayoutTransfer::REJECTED,
      provider_status: PayoutTransfer::ERRORED,
      raw_response: response.contents,
      description: response.description,
      code: response.code
    )

    # TODO: Payoneer::V2::PayoutApiClient::ERROR_CODES needs to be behind a shim when V4 is enabled
    response_contains_duplicate = payout_transfer.response["code"] == Payoneer::V2::PayoutApiClient::ERROR_CODES[:payout_already_exists]

    PayoutTransfer::Refund.refund(payout_transfer: payout_transfer) unless response_contains_duplicate
  end

  def approve_transfer
    payout_transfer.update(
      tunecore_processed_at: Time.now,
      tunecore_processor: admin,
      tunecore_status: PayoutTransfer::APPROVED,
      provider_status: PayoutTransfer::REQUESTED,
      raw_response: response.contents,
      payout_id: payout_id(response.contents),
      description: response.description,
      code: response.code
    )

    send_email(:mail_for_approved_transfer)
    payout_transfer.vat_tax_adjustment&.post_vat_transactions
    payout_transfer.create_outbound_invoice
  end

  def payout_id(payload)
    JSON.parse(payload)["payout_id"]
  end

  def send_email(mailer_method)
    MailerWorker.perform_async(
      "PayoutTransferMailer",
      mailer_method,
      payout_transfer.id
    )
  end

  def build_tax_adjustment
    payout_transfer.build_vat_tax_adjustment(
      person_id: person.id,
      amount: tax_amount,
      tax_rate: tax_rate,
      vat_registration_number: person.vat_information&.vat_registration_number,
      tax_type: VAT_TAX,
      trader_name: @tax_info[:trader_name],
      place_of_supply: @tax_info[:place_of_supply],
      error_message: nil,
      customer_type: @tax_info[:customer_type],
      vat_amount_in_eur: vat_amount_in_eur,
      foreign_exchange_rate_id: person.eur_fx_rate&.id
    )
  end

  def tax_amount
    ((payout_transfer.amount_cents + payout_transfer.fee_cents) * tax_rate) / 100
  end

  def tax_rate
    @tax_info[:tax_rate].to_f
  end

  def tax_info
    @tax_info ||= TcVat::OutboundVatCalculator.new(person).fetch_vat_info
  end

  def vat_amount_in_eur
    fx_rate = person.eur_fx_rate
    return if fx_rate.nil?

    tax_amount * fx_rate.exchange_rate
  end
end
