class PublishingAdministration::MultiTenantPublishingSplitsForm < PublishingAdministration::PublishingSplitsForm
  attr_accessor :composer_params, :composer

  validate :composer_fields_present?

  def initialize(args = {})
    super

    @composer_share = accumulated_composers_shares
    @composer = person.account&.composers&.first
  end

  def save
    return false unless valid?

    run_callbacks :save do
      create_cowriter_splits if cowriter_params.present?
      create_unknown_split if total_percent < 100
      create_composer_splits if composer_params.present?
    end
  end

  def composers
    @composers ||= ActiveModel::ArraySerializer.new(
      build_composers_splits,
      each_serializer: Api::Backstage::ComposerSplitsSerializer
    )
  end

  private

  def accumulated_composers_shares
    composer_params ? composer_params.sum { |composer| composer[:composer_share].to_d } : 0
  end

  def build_composers_splits
    PublishingAdministration::ComposerSplitsBuilder.build(composition.id)
  end

  def create_composer_splits
    composer_params.each_with_index do |composer_hash, index|
      composer = Composer.find_by(id:  composer_hash[:id])

      composer_share = composer_hash[:composer_share].to_d

      if index.zero?
        split_difference = 100.to_d - total_percent

        composer_share += split_difference
      end

      create_publishing_split(composer, composer_share)
    end
  end

  def create_publishing_split(writer, share)
    composition.publishing_splits.create!(
      writer: writer,
      composer: writer.is_a?(Composer) ? writer : composer,
      percent: share
    )
  end

  def can_person_submit_split?
    return unless person.id != composer&.account&.person_id || composer&.has_fully_terminated_composers?

    errors.add(:publishing_split, I18n.t("controllers.publishing_split.no_access_error"))
  end

  def composer_fields_present?
    return if composer_params.blank?

    composer_params.each do |composer|
      composer_share  = composer[:composer_share].to_d
      composer_values = composer.values_at(:id)

      unless composer_values.all?(&:present?) && composer_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      end
    end
  end

  def total_percent
    accumulated_composers_shares.to_d + accumulated_cowriters_shares.to_d
  end

  def send_to_rights_app
    composer_ids = composer_params.pluck(:id)

    Composer.where(id: composer_ids).each do |composer|
      if composer.provider_identifier.present?
        PublishingAdministration::WorkRecordingCreationWorker.perform_async(composer.id, composition.id)
      end
    end
  end
end
