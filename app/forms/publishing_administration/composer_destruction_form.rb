class PublishingAdministration::ComposerDestructionForm < FormObject
  attr_accessor :composer, :account, :admin, :ip_address

  define_model_callbacks :validation
  validates_presence_of :composer, :account, :admin, :ip_address, :account_user
  validate :admin_present?
  validate :composer_has_fully_terminated_composers?
  validate :composer_is_one_of_many?
  validate :composer_is_not_account_composer?

  def save
    run_callbacks :validation do
      return false unless valid?
    end

    destroy_publishing_splits
    destroy_composer && create_note
  end

  def account
    @account ||= composer&.account
  end

  def account_user
    @account_user ||= account&.person
  end

  private

  def destroy_publishing_splits
    composer.publishing_splits.destroy_all
  end

  def destroy_composer
    composer.destroy
  end

  def create_note
    account_user.notes.create(
      related: account_user,
      note_created_by: admin,
      subject: "Composer Fully Deleted",
      ip_address: ip_address,
      note: note_content
    )
  end

  def note_content
    "Composer #{composer.name} with writer code #{composer.provider_identifier} was deleted by #{admin.name} at #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S')}"
  end

  def admin_present?
    return true if admin&.is_admin?

    errors.add(:admin, "An administrative user must be present to delete a composer.")
  end

  def composer_has_fully_terminated_composers?
    return true if composer&.has_fully_terminated_composers?

    errors.add(:composer, "does not have a terminated_composer record with a 'fully' termination_type.")
  end

  def composer_is_one_of_many?
    return true if account && account.composers.count > 1

    errors.add(:composer, "cannot be deleted if it is the only one on the account.")
  end

  def composer_is_not_account_composer?
    return true if composer && composer.id != composer.account_composer&.id

    errors.add(:composer, "cannot be deleted if it is the primary account composer.")
  end
end
