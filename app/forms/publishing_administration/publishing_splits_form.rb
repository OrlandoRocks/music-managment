class PublishingAdministration::PublishingSplitsForm < FormObject
  attr_accessor :composer,
                :composition,
                :composer_share,
                :person,
                :cowriter_params,
                :translated_name

  define_model_callbacks :save

  validate :can_person_submit_split?
  validate :shares_do_not_exceed_100?
  validate :cowriter_fields_present?
  validates_numericality_of :composer_share, greater_than_or_equal_to: 1, less_than_or_equal_to: 100
  validates_with CowritersValidator, fields: %i[first_name last_name]

  after_save :send_to_rights_app
  after_save :update_translated_name

  def save
    return false unless valid?

    run_callbacks :save do
      create_cowriter_splits if cowriter_params.present?
      create_unknown_split if total_percent < 100
      create_publishing_split(composer, composer_share)
    end
  end

  def composer_percent
    @composer_percent ||= composition.publishing_splits.where(writer_type: "Composer").sum(:percent)
  end

  def cowriter_percent
    @cowriter_percent ||= composition.publishing_splits.where(writer_type: "Cowriter").sum(:percent)
  end

  def cowriters
    @cowriters ||= ActiveModel::ArraySerializer.new(
      build_cowriters_splits,
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    )
  end

  def unknown_cowriter
    composition.publishing_splits.includes(:cowriter).find_by(cowriters: { is_unknown: true })
  end

  private

  def accumulated_cowriters_shares
    known_cowriter_share = cowriter_params ? cowriter_params.sum { |cowriter| cowriter[:cowriter_share].to_d } : 0
    known_cowriter_share + (unknown_cowriter&.percent || 0)
  end

  def build_cowriters_splits
    PublishingAdministration::CowriterSplitsBuilder.build(composer.id, composition.id)
  end

  def create_cowriter_splits
    cowriter_params.each do |cowriter_hash|
      cowriter = Cowriter.where(
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        composer_id: composer.id
      ).first_or_create

      create_publishing_split(cowriter, cowriter_hash[:cowriter_share])
    end
  end

  def create_publishing_split(writer, share)
    composition.publishing_splits.create(
      writer: writer,
      composer: composer,
      percent: writer.is_a?(Composer) ? composer_share_with_fractional_remainder : share
    )
  end

  def create_unknown_split
    return unless split_difference > 1

    unknown_cowriter = Cowriter.where(composer_id: composer.id, is_unknown: true).first_or_create!
    create_publishing_split(unknown_cowriter, 100 - total_percent)
  end

  def composer_share_with_fractional_remainder
    composer_share + split_difference
  end

  def send_to_rights_app
    return if composer.provider_identifier.blank?

    PublishingAdministration::WorkRecordingCreationWorker.perform_async(composer.id, composition.id)
  end

  def split_difference
    100.to_d - total_percent
  end

  def total_percent
    composer_share.to_d + accumulated_cowriters_shares.to_d
  end

  def update_translated_name
    composition.update(translated_name: translated_name) if translated_name.present?
    composition.submitted!
  end

  def can_person_submit_split?
    return unless person.id != composer.account.person_id || composer.has_fully_terminated_composers?

    errors.add(:publishing_split, I18n.t("controllers.publishing_split.no_access_error"))
  end

  def shares_do_not_exceed_100?
    errors.add(:publishing_split, I18n.t("controllers.publishing_split.total_splits_exceed_100")) if total_percent > 100
  end

  def cowriter_fields_present?
    return if cowriter_params.blank?

    cowriter_params.each do |cowriter|
      cowriter_share  = cowriter[:cowriter_share].to_d
      cowriter_values = cowriter.values_at(:first_name, :last_name, :cowriter_share)

      unless cowriter_values.all?(&:present?) && cowriter_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      end
    end
  end
end
