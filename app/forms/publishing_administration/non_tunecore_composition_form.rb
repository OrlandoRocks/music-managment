class PublishingAdministration::NonTunecoreCompositionForm < FormObject
  attr_accessor :title,
                :artist_name,
                :isrc_number,
                :percent,
                :album_name,
                :album_upc,
                :id,
                :record_label,
                :release_date,
                :created_at,
                :updated_at,
                :person_id,
                :composer_id,
                :cowriter_params

  define_model_callbacks :validation, :save

  validates_presence_of :title, :artist_name, :percent, :person_id

  before_validation :convert_percent_to_decimal

  validates_inclusion_of :percent, in: 1..100
  validate :person_exists?
  validate :composer_exists?
  validate :cowriter_fields_present?
  validate :unique_song_isrc?
  validate :parse_release_date, if: proc { |form| form.release_date.present? }
  validate :duplicate_title
  validates_with CowritersValidator, fields: %i[first_name last_name]

  after_save :send_to_publishing_administration

  def save
    run_callbacks :validation do
      return unless valid?
    end

    run_callbacks :save do
      create_composition
      create_non_tunecore_album
      create_non_tunecore_song
      create_cowriter_splits if cowriter_params.present?
      create_unknown_split if total_percent < 100
      create_composer_split
      composition.submitted!
      true
    end
  end

  def response_body
    {
      id: composition.id,
      appears_on: non_tunecore_album.name,
      composition_title: composition.name,
      performing_artist: artist_name,
      release_date: release_date,
      composer_share: composer_share,
      cowriter_share: cowriter_share,
      cowriters: cowriter_splits,
      errors: errors,
      isrc: non_tunecore_song.isrc,
      submitted_at: composition.share_submitted_date,
      is_ntc_song: true,
      unknown_split_percent: @unknown_split&.percent,
    }
  end

  private

  attr_reader :non_tunecore_album, :non_tunecore_song, :composition

  def create_composition
    @composition = Composition.create!(name: title)
  end

  def create_non_tunecore_album
    @non_tunecore_album = NonTunecoreAlbum.where(
      name: album_name.presence || title,
      upc: album_upc,
      composer_id: composer.id,
      orig_release_year: parse_release_date,
      account_id: composer.account_id
    ).first_or_create!
  end

  def create_non_tunecore_song
    @non_tunecore_song = non_tunecore_album.non_tunecore_songs.where(
      name: composition.name,
      isrc: isrc_number.try(:upcase),
      artist_id: artist,
      composition_id: composition,
      release_date: parse_release_date
    ).first_or_create!
  end

  def create_composer_split
    create_publishing_split(composer, percent)
  end

  def create_cowriter_splits
    stripped_cowriter_params = cowriter_params.each { |cowriter| cowriter.each_value(&:strip!) }

    stripped_cowriter_params.each do |cowriter_hash|
      cowriter = Cowriter.where(
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        composer_id: composer.id
      ).first_or_create!

      create_publishing_split(cowriter, cowriter_hash[:cowriter_share])
    end
  end

  def create_unknown_split
    return unless split_difference >= 1

    remaining_share_percentage = 100 - (percent + cowriter_share)
    @unknown_split = create_publishing_split(unknown_cowriter, remaining_share_percentage)
  end

  def unknown_cowriter
    @unknown_cowriter ||= Cowriter.where(composer_id: composer.id, is_unknown: true).first_or_create!
  end

  def create_publishing_split(writer, percent)
    composition.publishing_splits.where(
      composer_id: composer,
      writer_id: writer,
      writer_type: writer.class.name,
      percent: writer.is_a?(Composer) ? composer_share_with_fractional_remainder : percent.to_d
    ).first_or_create!
  end

  def first_composition_of_day?
    start_of_day  = Time.now.in_time_zone(SERVER_TIMEZONE).beginning_of_day
    end_of_day    = Time.now.in_time_zone(SERVER_TIMEZONE).end_of_day
    composer.non_tunecore_songs.where(created_at: start_of_day..end_of_day).count <= 1
  end

  def person
    @person ||= Person.find(person_id)
  end

  def composer
    @composer ||= find_composer
  end

  def find_composer
    if composer_id.present?
      person.account.composers.find(composer_id)
    else
      person.composers.first
    end
  end

  def artist
    @artist ||= Artist.where(name: artist_name).first_or_create!
  end

  def composer_share
    @composer_share ||= composition.publishing_splits.where(writer_type: "Composer").sum(:percent)
  end

  def cowriter_share
    composition.publishing_splits.where(writer_type: "Cowriter").sum(:percent)
  end

  def unknown_cowriter_share
    composition.publishing_splits.includes(:cowriter).where(cowriters: { is_unknown: true }).sum(:percent)
  end

  def cowriter_splits
    ActiveModel::ArraySerializer.new(
      PublishingAdministration::CowriterSplitsBuilder.build(composer.id, composition.id),
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    )
  end

  def person_exists?
    errors.add(:person, "must exists") unless Person.exists?(person_id)
  end

  def composer_exists?
    errors.add(:composer, "must exists") unless Composer.exists?(id: composer_id)
  end

  def unique_song_isrc?
    return true if isrc_number.blank?

    errors.add(:isrc_number, "isrc_in_use") if isrc_already_used_by_distro_song? || isrc_already_used_by_ntc_song?
  end

  def isrc_already_used_by_distro_song?
    return false if composer.person_id.nil?

    Song.joins(:album).exists?(
      [
        "albums.person_id = ? AND (songs.tunecore_isrc = ? OR songs.optional_isrc = ?) AND albums.is_deleted = 0",
        composer.person_id,
        isrc_number,
        isrc_number
      ]
    )
  end

  def isrc_already_used_by_ntc_song?
    return false if isrc_number.blank?

    NonTunecoreSong.joins(:non_tunecore_album).exists?(
      non_tunecore_albums: { composer_id: composer_id },
      non_tunecore_songs: { isrc: isrc_number.upcase }
    )
  end

  def convert_percent_to_decimal
    @percent = percent.to_d
  end

  def send_to_publishing_administration
    return if composer.provider_identifier.blank?

    PublishingAdministration::WorkRecordingCreationWorker.perform_async(composer.id, composition.id)
  end

  def cowriter_fields_present?
    return true if cowriter_params.blank?

    cowriter_params.each do |cowriter|
      cowriter_share = cowriter[:cowriter_share].to_d

      if cowriter.values.any?(&:blank?) || !cowriter_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      end
    end
  end

  def composer_share_with_fractional_remainder
    percent + split_difference
  end

  def split_difference
    100.to_d - total_percent
  end

  def total_percent
    percent.to_d + accumulated_cowriters_shares.to_d
  end

  def accumulated_cowriters_shares
    known_cowriter_share = cowriter_params ? cowriter_params.sum { |cowriter| cowriter[:cowriter_share].to_d } : 0
    known_cowriter_share + unknown_cowriter_share
  end

  def parse_release_date
    return if release_date.blank?
    return release_date if release_date.respond_to?(:strftime)

    begin
      self.release_date = Date.strptime(release_date, "%m/%d/%Y")
    rescue
      errors.add(:release_date, "release_date")
    end
  end

  def duplicate_title
    composition_titles = PublishingAdministration::CompositionsBuilder.build({ composer_id: composer_id })
    cleaned_titles     = composition_titles.map { |composition| composition.clean_name.downcase }

    errors.add(:title, "duplicate_title") if cleaned_titles.include?(title.downcase)
  end
end
