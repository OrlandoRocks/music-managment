class PublishingAdministration::MultiTenantPublishingCompositionSplitsForm < PublishingAdministration::PublishingCompositionSplitsForm
  attr_accessor :composer_params, :composer

  validate :composer_fields_present?

  def initialize(args = {})
    super

    @composer_share = accumulated_composers_shares
    @composer = person.account&.publishing_composers&.first
  end

  def save
    return false unless valid?

    run_callbacks :save do
      create_cowriter_splits if cowriter_params.present?
      create_primary_composer_splits if composer_params.present?
    end
  end

  def composers
    @composers ||= ActiveModel::ArraySerializer.new(
      build_composers_splits,
      each_serializer: Api::Backstage::ComposerSplitsSerializer
    )
  end

  private

  def accumulated_composers_shares
    composer_params ? composer_params.sum { |composer| composer[:composer_share].to_d } : 0
  end

  def build_composers_splits
    PublishingAdministration::PrimaryPublishingComposerSplitsBuilder.build(composition.id)
  end

  def create_primary_composer_splits
    composer_params.each_with_index do |composer_hash, _index|
      composer = PublishingComposer.primary.find_by(id: composer_hash[:id])

      composer_share = composer_hash[:composer_share].to_d

      create_publishing_composition_split(writer: composer, share: composer_share, right_to_collect: true)
    end
  end

  def create_publishing_composition_split(writer:, share:, right_to_collect: false)
    publishing_composition.publishing_composition_splits.create(
      publishing_composer: writer,
      right_to_collect: right_to_collect,
      percent: share
    )
  end

  def can_person_submit_split?
    return unless person.id != composer&.account&.person_id || composer&.has_fully_terminated_composers?

    errors.add(:publishing_split, I18n.t("controllers.publishing_split.no_access_error"))
  end

  def composer_fields_present?
    return if composer_params.blank?

    composer_params.each do |composer|
      composer_share  = composer[:composer_share].to_d
      composer_values = composer.values_at(:id)

      unless composer_values.all?(&:present?) && composer_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      end
    end
  end

  def total_percent
    accumulated_composers_shares.to_d + accumulated_cowriters_shares.to_d
  end

  def send_to_rights_app
    return if publishing_composition.held_from_rights_app?

    publishing_composer_ids = composer_params.pluck(:id)

    PublishingComposer.where(id: publishing_composer_ids).each do |publishing_composer|
      next if publishing_composer.provider_identifier.blank?

      PublishingAdministration::PublishingWorkRecordingCreationWorker.perform_async(
        publishing_composer.id,
        publishing_composition.id
      )
    end
  end
end
