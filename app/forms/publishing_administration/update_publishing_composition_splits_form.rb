class PublishingAdministration::UpdatePublishingCompositionSplitsForm < FormObject
  include PublishingCompositionSplitValidatable
  attr_accessor :composition_title,
                :translated_name,
                :appears_on,
                :record_label,
                :composer_share,
                :cowriter_share,
                :status,
                :isrc,
                :performing_artist,
                :release_date,
                :is_ntc_song,
                :cowriter_params,
                :composition_id,
                :unknown_split_percent,
                :public_domain

  define_model_callbacks :validation, :update

  validates_presence_of :composition_title, :performing_artist, if: :is_ntc_song
  validates_presence_of :composer_share
  validates_length_of :composition_title, maximum: 160, allow_nil: true
  validates_length_of :translated_name, maximum: 160, allow_nil: true
  validate :total_splits_do_not_exceed_100_percent
  validate :split_percent_is_valid
  validate :cowriter_fields_present?
  validate :unique_song_isrc?
  validate :parse_release_date
  validate :duplicate_title
  validate :modified_composition?

  def update
    run_callbacks :validation do
      return unless valid?
    end

    run_callbacks :update do
      update_splits
      update_public_domain
      update_state
      send_to_rights_app
      true
    end
  end

  def response_body
    {
      composition: {
        id: composition.id,
        composition_title: composition_title,
        translated_name: composition.translated_name,
        appears_on: appears_on,
        record_label: record_label,
        composer_share: composer_share,
        cowriter_share: total_cowriter_share,
        status: composition.state,
        isrc: isrc,
        performing_artist: performing_artist,
        release_date: format_release_date,
        is_ntc_song: is_ntc_song,
        cowriters: cowriter_splits.object.pluck(:id),
        composition_id: composition.id,
        unknown_split_percent: 100.to_d - composition.total_split_pct,
        public_domain: public_domain
      },
      errors: errors,
      cowriters: cowriter_splits,
    }
  end

  private

  def format_release_date
    release_date.present? ? release_date.strftime("%m/%d/%Y") : release_date
  end

  def composition
    @composition ||= PublishingComposition
                     .includes(publishing_composition_splits: :publishing_composer, non_tunecore_songs: [:non_tunecore_album, :artist])
                     .find_by(id: composition_id)
  end

  def account
    @account ||= composition.account
  end

  def update_public_domain
    composition.public_domain = public_domain
  end

  def update_splits
    reset_splits

    existing_cowriters, new_cowriters =
      cowriter_params.partition do |cowriter_hash|
      cowriter_hash[:id].present?
    end

    composition.publishing_composition_splits.reload.each do |pub_split|
      if pub_split.publishing_composer.is_primary_composer
        update_primary_composer_split(pub_split)
      else
        update_or_delete_non_collectable_split(existing_cowriters, pub_split)
      end
    end

    create_new_cowriters(new_cowriters)
  end

  def reset_splits
    composition.publishing_composition_splits.update_all(percent: 0)
  end

  def update_primary_composer_split(pub_split)
    return false unless pub_split.update(percent: composer_share.to_d)

    composition.update(name: composition_title, translated_name: translated_name)
    update_ntc_song if is_ntc_song
  end

  def update_or_delete_non_collectable_split(existing_cowriters, cowriter_split)
    cowriter_hash = existing_cowriters.find { |c| c[:id] == cowriter_split.id }
    if cowriter_hash.present?
      cowriter_split.update(percent: cowriter_hash[:cowriter_share].to_d)

      new_cowriter = account.publishing_composers.where(
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        is_primary_composer: false,
        account_id: account.id,
        person_id: account.person_id
      ).first_or_create!

      cowriter_split.update(publishing_composer: new_cowriter)
    else
      cowriter_split.delete
    end
  end

  def update_ntc_song
    artist = Artist.where(name: performing_artist).first_or_create!
    ntc_song = composition.non_tunecore_songs.first
    ntc_song.update(
      isrc: isrc,
      release_date: release_date,
      artist: artist
    )

    ntc_song.update_album_for_publishing_composer(name: appears_on, record_label: record_label)
  end

  def create_new_cowriters(new_cowriters)
    return if new_cowriters.nil?

    new_cowriters.each do |cowriter_hash|
      new_cowriter = composition.publishing_composers.build(
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        is_primary_composer: false,
        account_id: account.id,
        person_id: account.person_id
      )
      create_publishing_composition_split(new_cowriter, cowriter_hash[:cowriter_share])

      new_cowriter.save
    end
  end

  def create_publishing_composition_split(publishing_composer, percent)
    publishing_composer.publishing_composition_splits.build(
      publishing_composer_id: publishing_composer.id,
      publishing_composition_id: composition.id,
      percent: percent.to_d,
      right_to_collect: false
    )
  end

  def missing_percent
    @missing_percent ||= 100 - (composer_share.to_d + new_cowriter_share_total)
  end

  def new_cowriter_share_total
    cowriter_params.sum { |c| c[:cowriter_share].to_d }
  end

  def total_cowriter_share
    composition.publishing_composition_splits.where(right_to_collect: 0).sum(:percent)
  end

  def cowriter_splits
    @cowriter_splits ||= ActiveModel::ArraySerializer.new(
      PublishingAdministration::NonCollectableSplitsBuilder.build(composition.publishing_composer.id, composition.id),
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    )
  end

  def unique_song_isrc?
    return true unless isrc.present? && is_ntc_song

    errors.add(:isrc, "isrc_in_use") if isrc_already_used_by_distro_song? || isrc_already_used_by_ntc_song?
  end

  def isrc_already_used_by_distro_song?
    primary_composer = composition.publishing_composers.find_by(is_primary_composer: true)
    return false if primary_composer.person_id.blank?

    Song.joins(:album).where(
      albums: { person_id: primary_composer.person_id }
    ).exists?(["songs.tunecore_isrc = ? OR songs.optional_isrc = ?", isrc, isrc])
  end

  def isrc_already_used_by_ntc_song?
    return false if isrc.blank?

    NonTunecoreSong.joins(:non_tunecore_album, :publishing_composition).where.not(
      publishing_compositions: { id: composition.id }
    ).exists?(
      non_tunecore_albums: { composer_id: composition.publishing_composers.find_by(is_primary_composer: true).id },
      non_tunecore_songs: { isrc: isrc.upcase }
    )
  end

  def cowriter_fields_present?
    return true if cowriter_params.blank?

    cowriter_params.each do |cowriter|
      cowriter_share  = cowriter[:cowriter_share].to_d
      cowriter_values = cowriter.values_at(:first_name, :last_name, :cowriter_share)

      next if cowriter_values.all?(&:present?) && cowriter_share.between?(1, 100)

      errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      errors.add(:publishing_split, cowriter.to_s)
      errors.add(:publishing_split, cowriter_values.to_s)
      return false
    end
  end

  def parse_release_date
    return if release_date.blank?

    begin
      self.release_date = Date.strptime(release_date, "%m/%d/%Y")
    rescue
      errors.add(:release_date, "release_date")
    end
  end

  def duplicate_title
    return unless is_ntc_song

    primary_composer   = composition.publishing_composers.find_by(is_primary_composer: true)
    composition_titles = PublishingAdministration::PublishingCompositionsBuilder.build({ publishing_composer_id: primary_composer.id })
    other_titles       = composition_titles.filter { |comp| comp.id != composition.id }
    cleaned_titles     = other_titles.map { |comp| comp.clean_name.downcase }

    errors.add(:composition_title, "duplicate_title") if cleaned_titles.include?(composition_title.downcase)
  end

  def send_to_rights_app
    return if composition.held_from_rights_app?

    PublishingAdministration::PublishingWorkUpdateWorker.perform_async(composition.id)
  end

  def name_unchanged?
    (composition_title && composition.name == composition_title)
  end

  def translated_name_unchanged?
    (!translated_name || composition.translated_name == translated_name)
  end

  def isrc_unchanged?(ntc_song)
    (!isrc || !ntc_song || ntc_song.isrc == isrc)
  end

  def release_date_unchanged?(ntc_song)
    !release_date || !ntc_song || ntc_song.release_date == release_date
  end

  def performing_artist_unchanged?(ntc_song)
    (!performing_artist || !ntc_song || performing_artist == Artist.find_by(id: ntc_song.artist_id).name)
  end

  def ntc_album_unchanged?(ntc_song)
    (!appears_on || !ntc_song || NonTunecoreAlbum.find_by(id: ntc_song.non_tunecore_album_id).name == appears_on)
  end

  def splits_unchanged?(publishing_splits)
    return false if publishing_splits.length != cowriter_params.length

    splits_unchanged = (composer_share && publishing_splits[0].percent == composer_share.to_d)

    return false unless splits_unchanged

    cowriter_params.each do |cowriter|
      cowriter_share = cowriter[:cowriter_share].to_d
      cowriter_first_name = cowriter[:first_name]
      cowriter_last_name = cowriter[:last_name]
      cowriter_uuid = cowriter[:uuid]

      publishing_split = PublishingCompositionSplit.find_by(id: cowriter_uuid)

      unless publishing_split
        splits_unchanged = false
        break
      end
      if (cowriter_share != publishing_split.percent)
        splits_unchanged = false
        break
      end
      split_composer = PublishingComposer.find_by(id: publishing_split.publishing_composer_id)
      if (cowriter_first_name != split_composer.first_name || cowriter_last_name != split_composer.last_name)
        splits_unchanged = false
        break
      end
    end

    splits_unchanged
  end

  def public_domain_unchanged?
    composition.public_domain == public_domain
  end

  def modified_composition?
    return true if composition.nil?

    ntc_song = composition.non_tunecore_songs.first
    publishing_splits = PublishingCompositionSplit.where(publishing_composition_id: @composition.id)

    return true unless name_unchanged?
    return true unless translated_name_unchanged?
    return true unless isrc_unchanged?(ntc_song)
    return true unless release_date_unchanged?(ntc_song)
    return true unless performing_artist_unchanged?(ntc_song)
    return true unless ntc_album_unchanged?(ntc_song)
    return true unless splits_unchanged?(publishing_splits)
    return true unless public_domain_unchanged?

    errors.add(:modified_composition?, "unmodified_composition?")
  end

  def total_splits_do_not_exceed_100_percent
    validate_split_percentage_maximum(composer_share.to_d + new_cowriter_share_total.to_d)
  end

  def split_percent_is_valid
    validate_split_percent_range(composer_share.to_d) && validate_split_percent_range(new_cowriter_share_total.to_d)
  end

  def total_percent
    composer_share.to_d + new_cowriter_share_total.to_d
  end

  def update_state
    total_percent == 100 ? @composition.submitted! : @composition.draft!
  end
end
