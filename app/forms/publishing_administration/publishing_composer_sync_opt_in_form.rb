class PublishingAdministration::PublishingComposerSyncOptInForm < FormObject
  attr_accessor :publishing_composer_id, :sync_opted_updated_at, :publishing_composer

  def self.destroy(params)
    new(params).tap(&:destroy)
  end

  def initialize(params)
    @publishing_composer_id = params[:id] || params[:publishing_composer_id]
    @sync_opted_updated_at = params[:sync_opted_updated_at]
    @publishing_composer = PublishingComposer.find_by(id: @publishing_composer_id)
  end

  def destroy
    publishing_composer&.update(destruction_args)
  end

  private

  def destruction_args
    {
      sync_opted_in: false,
      sync_opted_updated_at: if sync_opted_updated_at.present?
                               Date.strptime(
                                 sync_opted_updated_at,
                                 "%m/%d/%Y"
                               )
                             else
                               Date.today
                             end,
    }
  end
end
