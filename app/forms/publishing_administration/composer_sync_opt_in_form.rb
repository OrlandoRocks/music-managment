class PublishingAdministration::ComposerSyncOptInForm < FormObject
  attr_accessor :composer_id, :sync_opted_updated_at, :composer

  def self.destroy(params)
    new(params).tap(&:destroy)
  end

  def initialize(params)
    @composer_id = params[:id] || params[:composer_id]
    @sync_opted_updated_at = params[:sync_opted_updated_at]
    @composer = Composer.find_by(id: @composer_id)
  end

  def destroy
    composer&.update(destruction_args)
  end

  private

  def destruction_args
    {
      sync_opted_in: false,
      sync_opted_updated_at: if sync_opted_updated_at.present?
                               Date.strptime(
                                 sync_opted_updated_at,
                                 "%m/%d/%Y"
                               )
                             else
                               Date.today
                             end,
    }
  end
end
