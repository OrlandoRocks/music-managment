class PublishingAdministration::PublishingComposerDestructionForm < FormObject
  attr_accessor :publishing_composer, :account, :admin, :ip_address

  define_model_callbacks :validation
  validates_presence_of :publishing_composer, :account, :admin, :ip_address, :account_user
  validate :admin_present?
  validate :publishing_composer_has_fully_terminated_composers?
  validate :publishing_composer_is_one_of_many?
  validate :publishing_composer_is_not_account_composer?

  def save
    run_callbacks :validation do
      return false unless valid?
    end

    destroy_publishing_composition_splits
    destroy_publishing_composer && create_note
  end

  def account
    @account ||= publishing_composer&.account
  end

  def account_user
    @account_user ||= account&.person
  end

  private

  def destroy_publishing_composition_splits
    publishing_composer.publishing_composition_splits.destroy_all
  end

  def destroy_publishing_composer
    publishing_composer.destroy
  end

  def create_note
    account_user.notes.create(
      related: account_user,
      note_created_by: admin,
      subject: "PublishingComposer Fully Deleted",
      ip_address: ip_address,
      note: note_content
    )
  end

  def note_content
    "PublishingComposer #{publishing_composer.name} with writer code #{publishing_composer.provider_identifier} was deleted by #{admin.name} at #{DateTime.now.strftime('%Y-%m-%d %H:%M:%S')}"
  end

  def admin_present?
    return true if admin&.is_admin?

    errors.add(:admin, "An administrative user must be present to delete a publishing_composer.")
  end

  def publishing_composer_has_fully_terminated_composers?
    return true if publishing_composer&.has_fully_terminated_composers?

    errors.add(:publishing_composer, "does not have a terminated_composer record with a 'fully' termination_type.")
  end

  def publishing_composer_is_one_of_many?
    return true if account && account.publishing_composers.count > 1

    errors.add(:publishing_composer, "cannot be deleted if it is the only one on the account.")
  end

  def publishing_composer_is_not_account_composer?
    return true if publishing_composer && publishing_composer.id != publishing_composer.account_composer&.id

    errors.add(:publishing_composer, "cannot be deleted if it is the primary account publishing_composer.")
  end
end
