class PublishingAdministration::ComposerPersonAssociationForm < FormObject
  attr_accessor :composer, :person_id, :person

  validate :valid_person_id?
  validate :person_can_be_associated_to_composer?, if: :still_valid?

  def save
    return false unless valid?

    create_association
  end

  private

  def create_association
    acct_person_id  = composer.account.person_id
    role_name       = (acct_person_id == person_id) ? "SELF" : "OTHER"
    role_id         = "#{PublishingRole}::ON_BEHALF_OF_#{role_name}_ID".constantize

    composer.update!(person_id: person_id, publishing_role_id: role_id)
  end

  def person_can_be_associated_to_composer?
    return true if person_id.nil?

    composer = Composer.where(person_id: person_id).first

    return if composer.blank?

    pub_admin_id = composer.publishing_administrator.id
    errors.add(:person, "Composer with this TC Account/Person ID already exists on the account #{pub_admin_id}")
  end

  def still_valid?
    errors[:person].empty?
  end

  def valid_person_id?
    errors.add(:person, "Invalid TC Account/Person ID.") unless person_id.blank? || Person.exists?(id: person_id)
  end
end
