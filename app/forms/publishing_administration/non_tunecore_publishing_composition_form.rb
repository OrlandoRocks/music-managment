# frozen_string_literal: true

class PublishingAdministration::NonTunecorePublishingCompositionForm < FormObject
  attr_accessor :title,
                :artist_name,
                :isrc_number,
                :percent,
                :album_name,
                :album_upc,
                :id,
                :record_label,
                :release_date,
                :created_at,
                :updated_at,
                :person_id,
                :composer_id,
                :cowriter_params,
                :public_domain

  alias :publishing_composer_id :composer_id
  alias :publishing_composer_id= :composer_id=

  define_model_callbacks :validation, :save

  validates_presence_of :title, :artist_name, :percent, :person_id
  validates_length_of :title, maximum: 160

  before_validation :convert_percent_to_decimal

  validates_inclusion_of :percent, in: 1..100
  validate :person_exists?
  validate :publishing_composer_exists?
  validate :cowriter_fields_present?
  validate :unique_song_isrc?
  validate :parse_release_date, if: proc { |form| form.release_date.present? }
  validate :duplicate_title

  after_save :send_to_publishing_administration

  def save
    run_callbacks :validation do
      return unless valid?
    end

    run_callbacks :save do
      ActiveRecord::Base.transaction do
        create_publishing_composition
        create_non_tunecore_album
        create_non_tunecore_song
        create_cowriter_splits if cowriter_params.present?
        create_primary_composer_split
        update_state
      end
      true
    end
  end

  def response_body
    {
      id: publishing_composition.id,
      appears_on: non_tunecore_album.name,
      record_label: non_tunecore_album.record_label,
      composition_title: publishing_composition.name,
      performing_artist: artist_name,
      release_date: release_date,
      composer_share: primary_composer_share,
      cowriter_share: cowriter_share,
      cowriters: cowriter_splits,
      errors: errors,
      isrc: non_tunecore_song.isrc,
      submitted_at: publishing_composition.share_submitted_date,
      is_ntc_song: true,
      unknown_split_percent: 100.to_d - publishing_composition.total_split_pct,
      public_domain: publishing_composition.public_domain,
    }
  end

  private

  attr_reader :non_tunecore_album, :non_tunecore_song, :publishing_composition, :primary_composer

  def create_publishing_composition
    @publishing_composition = PublishingComposition.create!(name: title, account: person.account, public_domain: parse_public_domain)
  end

  def create_non_tunecore_album
    @non_tunecore_album = NonTunecoreAlbum.where(
      name: album_name.presence || title,
      record_label: record_label,
      upc: album_upc,
      publishing_composer_id: primary_composer.id,
      orig_release_year: parse_release_date
    ).first_or_create!
  end

  def create_non_tunecore_song
    @non_tunecore_song = non_tunecore_album.non_tunecore_songs.where(
      name: publishing_composition.name,
      isrc: isrc_number.try(:upcase),
      artist_id: artist,
      publishing_composition_id: publishing_composition,
      release_date: parse_release_date
    ).first_or_create!
  end

  def create_primary_composer_split
    create_publishing_composition_split(primary_composer, percent, true)
  end

  def create_cowriter_splits
    stripped_cowriter_params = cowriter_params.each { |cowriter| cowriter.each_value(&:strip!) }

    stripped_cowriter_params.each do |cowriter_hash|
      cowriter = primary_composer.account.publishing_composers.where(
        person_id: person_id,
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        is_primary_composer: false,
        is_unknown: false
      ).first_or_create!
      create_publishing_composition_split(cowriter, cowriter_hash[:cowriter_share], false)
    end
  end

  def create_unknown_split
    return unless split_difference >= 1

    remaining_share_percentage = 100 - (percent + cowriter_share)
    @unknown_split = create_publishing_composition_split(unknown_cowriter, remaining_share_percentage, false)
  end

  def unknown_cowriter
    @unknown_cowriter ||= PublishingComposer.where(
      first_name: "",
      last_name: "",
      account_id: publishing_composition.account_id,
      person_id: person_id,
      is_primary_composer: false,
      is_unknown: true
    ).first_or_create!
  end

  def create_publishing_composition_split(writer, percent, right_to_collect)
    publishing_composition.publishing_composition_splits.where(
      publishing_composer_id: writer.id,
      right_to_collect: right_to_collect,
      percent: get_percent(writer, percent)
    ).first_or_create!
  end

  def get_percent(_writer, percent)
    percent.to_d
  end

  def first_publishing_composition_of_day?
    start_of_day  = Time.now.in_time_zone(SERVER_TIMEZONE).beginning_of_day
    end_of_day    = Time.now.in_time_zone(SERVER_TIMEZONE).end_of_day
    primary_composer.non_tunecore_songs.where(created_at: start_of_day..end_of_day).count <= 1
  end

  def person
    @person ||= Person.find(person_id)
  end

  def primary_composer
    @primary_composer ||= find_primary_composer
  end

  def find_primary_composer
    if composer_id.present?
      person.account.publishing_composers.find(composer_id)
    else
      person.account.publishing_composers.where(is_primary_composer: true).first
    end
  end

  def artist
    @artist ||= Artist.where(name: artist_name).first_or_create!
  end

  def primary_composer_share
    @primary_composer_share ||= publishing_composition.publishing_composition_splits.primary.sum(:percent)
  end

  def cowriter_share
    publishing_composition.publishing_composition_splits.non_collectable.sum(:percent)
  end

  def unknown_cowriter_share
    publishing_composition.publishing_composition_splits.unknown.sum(:percent)
  end

  def cowriter_splits
    ActiveModel::ArraySerializer.new(
      PublishingAdministration::NonCollectableSplitsBuilder.build(primary_composer.id, publishing_composition.id),
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    )
  end

  def person_exists?
    unless Person.exists?(person_id)
      errors.add(:person, "must exists")
      false
    end
    true
  end

  def publishing_composer_exists?
    unless PublishingComposer.exists?(id: composer_id)
      errors.add(:publishing_composer, "must exists")
      false
    end
    true
  end

  def unique_song_isrc?
    return true if isrc_number.blank?

    return unless isrc_already_used_by_distro_song? || isrc_already_used_by_ntc_song?

    errors.add(:isrc_number, "isrc_in_use")
    false
  end

  def isrc_already_used_by_distro_song?
    return false if primary_composer.person_id.nil?

    Song.joins(:album).where(
      albums: { person_id: primary_composer.person_id }
    ).exists?(["(songs.tunecore_isrc = ? OR songs.optional_isrc = ?) AND albums.is_deleted = 0", isrc_number, isrc_number])
  end

  def isrc_already_used_by_ntc_song?
    return false if isrc_number.blank?

    NonTunecoreSong.joins(:non_tunecore_album).exists?(
      non_tunecore_albums: { publishing_composer_id: composer_id },
      non_tunecore_songs: { isrc: isrc_number.upcase }
    )
  end

  def convert_percent_to_decimal
    @percent = percent.to_d
  end

  def send_to_publishing_administration
    return if primary_composer.provider_identifier.blank? || publishing_composition.held_from_rights_app?

    PublishingAdministration::PublishingWorkRecordingCreationWorker.perform_async(primary_composer.id, publishing_composition.id)
  end

  def cowriter_fields_present?
    return true if cowriter_params.blank?

    cowriter_params.each do |cowriter|
      cowriter_share = cowriter[:cowriter_share].to_d

      if cowriter.values.any?(&:blank?) || !cowriter_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
        return false
      end
    end
    true
  end

  def primary_composer_share_with_fractional_remainder
    percent + split_difference
  end

  def split_difference
    100.to_d - total_percent
  end

  def total_percent
    percent.to_d + accumulated_cowriters_shares.to_d
  end

  def accumulated_cowriters_shares
    known_cowriter_share = cowriter_params ? cowriter_params.sum { |cowriter| cowriter[:cowriter_share].to_d } : 0
    known_cowriter_share + unknown_cowriter_share
  end

  def parse_release_date
    return if release_date.blank?
    return release_date if release_date.respond_to?(:strftime)

    begin
      self.release_date = Date.strptime(release_date, "%m/%d/%Y")
    rescue ArgumentError
      errors.add(:release_date, "release_date")
    end
  end

  def parse_public_domain
    return false if public_domain.blank?

    return public_domain.strip.casecmp?("true") if public_domain.is_a? String

    public_domain
  end

  def duplicate_title
    publishing_composition_titles = PublishingAdministration::PublishingCompositionsBuilder.build({ publishing_composer_id: composer_id })
    cleaned_titles = publishing_composition_titles.map { |publishing_composition| publishing_composition.clean_name.downcase }

    errors.add(:title, "duplicate_title") if cleaned_titles.include?(title.downcase)
  end

  def update_state
    total_percent == 100 ? publishing_composition.submitted! : publishing_composition.draft!
  end
end
