class PublishingAdministration::PublishingCompositionSplitsForm < FormObject
  attr_accessor :composer, :composition, :composer_share, :person, :cowriter_params, :translated_name, :public_domain

  alias_attribute :publishing_composer, :composer
  alias_attribute :publishing_composition, :composition

  define_model_callbacks :save

  validate :can_person_submit_split?
  validate :shares_do_not_exceed_100?
  validate :cowriter_fields_present?
  validates_length_of :translated_name, maximum: 160, allow_nil: true
  validates_numericality_of :composer_share, greater_than_or_equal_to: 1, less_than_or_equal_to: 100

  after_save :send_to_rights_app
  after_save :update_translated_name

  def save
    return false unless valid?

    run_callbacks :save do
      set_public_domain
      create_cowriter_splits if cowriter_params.present?
      create_primary_composer_split
      update_state
    end
  end

  def account
    @account ||= publishing_composer.account
  end

  def composer_percent
    @composer_percent ||= publishing_composition.publishing_composition_splits.where(right_to_collect: true).sum(:percent)
  end

  def cowriter_percent
    @cowriter_percent ||= publishing_composition.publishing_composition_splits.where(right_to_collect: false).sum(:percent)
  end

  def cowriters
    @cowriters ||= ActiveModel::ArraySerializer.new(
      build_cowriters_splits,
      each_serializer: Api::Backstage::CowriterSplitsSerializer
    )
  end

  private

  def can_person_submit_split?
    return unless person.id != account.person_id || publishing_composer.has_fully_terminated_composers?

    errors.add(:publishing_split, I18n.t("controllers.publishing_split.no_access_error"))
  end

  def shares_do_not_exceed_100?
    errors.add(:publishing_split, I18n.t("controllers.publishing_split.total_splits_exceed_100")) if total_percent > 100
  end

  def cowriter_fields_present?
    return if cowriter_params.blank?

    cowriter_params.each do |cowriter|
      cowriter_share = cowriter[:cowriter_share].to_d
      cowriter_values = cowriter.values_at(:first_name, :last_name, :cowriter_share)

      unless cowriter_values.all?(&:present?) && cowriter_share.between?(1, 100)
        errors.add(:publishing_split, I18n.t("controllers.publishing_split.invalid_cowriter_values"))
      end
    end
  end

  def send_to_rights_app
    return if publishing_composer.provider_identifier.blank? || publishing_composition.held_from_rights_app?

    PublishingAdministration::PublishingWorkRecordingCreationWorker.perform_async(publishing_composer.id, publishing_composition.id)
  end

  def update_translated_name
    publishing_composition.update(translated_name: translated_name) if translated_name.present?
  end

  def create_primary_composer_split
    create_publishing_composition_split(writer: composer, share: composer_share, right_to_collect: true)
  end

  def set_public_domain
    publishing_composition.update(public_domain: public_domain)
  end

  def create_cowriter_splits
    cowriter_params.each do |cowriter_hash|
      cowriter = PublishingComposer.where(
        first_name: cowriter_hash[:first_name],
        last_name: cowriter_hash[:last_name],
        account_id: account.id,
        person_id: account.person_id,
        is_primary_composer: false,
        is_unknown: false,
      ).first_or_create

      create_publishing_composition_split(writer: cowriter, share: cowriter_hash[:cowriter_share])
    end
  end

  def create_publishing_composition_split(writer:, share:, right_to_collect: false)
    publishing_composition.publishing_composition_splits.create(
      publishing_composer: writer,
      right_to_collect: right_to_collect,
      percent: right_to_collect ? composer_share : share
    )
  end

  def split_difference
    100.to_d - total_percent
  end

  def total_percent
    composer_share.to_d + accumulated_cowriters_shares.to_d
  end

  def accumulated_cowriters_shares
    cowriter_params ? cowriter_params.sum { |cowriter| cowriter[:cowriter_share].to_d } : 0
  end

  def build_cowriters_splits
    PublishingAdministration::NonCollectableSplitsBuilder.build(composer.id, composition.id)
  end

  def update_state
    total_percent == 100 ? publishing_composition.submitted! : publishing_composition.draft!
  end
end
