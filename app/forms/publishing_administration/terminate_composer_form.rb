class PublishingAdministration::TerminateComposerForm < FormObject
  include TerminatedComposerHelper

  attr_accessor :terminated_composer,
                :composer_id,
                :effective_date,
                :admin_note,
                :termination_type,
                :publishing_termination_reason_id

  define_model_callbacks :validation
  validate :valid_effective_date?
  after_validation :load_terminated_composer

  def save
    run_callbacks :validation do
      return false unless valid?
    end

    assign_data
    terminated_composer.save
  end

  def destroy
    if update_to_partially?
      terminated_composer.update(termination_type: "partially")
    else
      terminated_composer.destroy
    end
  end

  private

  def assign_data
    if terminated_composer.present?
      terminated_composer.assign_attributes(update_params)
    else
      @terminated_composer = TerminatedComposer.new(create_params)
    end
  end

  def create_params
    {
      composer_id: composer_id,
      effective_date: effective_date.blank? ? Date.today : Date.strptime(effective_date, "%m/%d/%Y"),
      admin_note: admin_note.blank? ? nil : admin_note.strip,
      termination_type: "fully",
      publishing_termination_reason_id: publishing_termination_reason_id
    }
  end

  def load_terminated_composer
    @terminated_composer = existing_partially_terminated_composer(composer_id)
  end

  def update_params
    {
      admin_note: admin_note.blank? ? terminated_composer.admin_note : admin_note.strip,
      termination_type: "fully",
      effective_date: effective_date.blank? ? terminated_composer.effective_date : effective_date.to_date,
      publishing_termination_reason_id: publishing_termination_reason_id
    }
  end

  def update_to_partially?
    terminated_composer.terminated_compositions?
  end

  def valid_effective_date?
    return true if effective_date.blank?

    return if effective_date.split("/").count > 1 && effective_date.split("/").last.length == 4

    errors.add(
      :effective_date,
      "Termination Effective Date -- #{effective_date} -- not formatted correctly. Expecting mm/dd/yyyy."
    )
  end
end
