class PublishingAdministration::PublishingEnrollmentForm < FormObject
  include PublishingAdministrationHelper

  attr_accessor :first_name,
                :middle_name,
                :last_name,
                :dob,
                :email,
                :composer_cae,
                :id,
                :publishing_role_id,
                :composer_pro_id,
                :person_id,
                :publisher_name,
                :publisher_pro_id,
                :name_prefix,
                :name_suffix,
                :publisher_cae,
                :dob_y,
                :dob_m,
                :dob_d,
                :tos_agreed,
                :action
  attr_reader :added_publisher

  alias_attribute :composer, :publishing_composer

  define_model_callbacks :save, :validation

  validates_presence_of :first_name, :last_name, :publishing_role_id
  validates_presence_of :publisher_name, if: :needs_publisher_name?
  validates_presence_of :publisher_name, if: proc { |frm| frm.publisher_cae.present? }
  validates_presence_of :publisher_cae, if: proc { |frm| frm.publisher_name.present? }

  validates_length_of :first_name, maximum: 45
  validates_length_of :last_name, maximum: 45
  validates_length_of :middle_name, maximum: 100, allow_nil: true

  validate :cae_present_with_no_pro?
  validate :cae_length_valid?
  validate :cae_start_with_55_valid?
  validates_numericality_of :composer_cae, allow_blank: true
  validates_numericality_of :publisher_cae, if: :needs_publisher_cae?
  validate :pub_cae_different_from_composer_cae?, if: :needs_publisher_cae?

  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})\z/

  validate :authorized_to_update?
  validate :valid_publishing_role_id?
  validate :validate_and_set_dob
  validate :agreed_to_tos?

  after_validation :publishing_composer_name_changed?

  after_save :create_publishing_composer_for_account, if: :account_needs_publishing_composer?
  after_save :send_to_publishing_administration
  after_save :update_publishing_compositions
  after_save :send_lod_mail
  after_save :add_publishing_administration_product_to_cart

  def save
    run_callbacks :validation do
      return false unless valid?
    end

    @new_record = publishing_composer.new_record?

    run_callbacks :save do
      publishing_composer.assign_attributes(publishing_composer_params)
      set_publisher
      publishing_composer.save
    end
  end

  def person
    @person ||= Person.find_by(id: person_id)
  end

  def publishing_composer
    @publishing_composer ||= load_publishing_composer
  end

  def composer_name_changed?
    publishing_composer_name_changed?
  end

  def publishing_composer_name_changed?
    @publishing_composer_name_changed ||= publishing_composer.full_name_affixed != full_name_affixed
  end

  def status
    @status ||= PublishingAdministration::ComposerStatusService.new(publishing_composer).state
  end

  def submittable?
    (composer_pro_id.present? && publishing_composer.cae.present?) || (composer_pro_id.blank? && publishing_composer.cae.blank?)
  end

  def publisher
    @publisher ||= Publisher.where(publisher_params).first_or_create
  end

  def has_publisher?
    !publisher_params_empty? && publisher && publisher.persisted?
  end

  def set_publisher
    @added_publisher = publishing_composer.publisher.nil? && !publisher_params_empty?
    publishing_composer.publisher = publisher_params_empty? ? nil : publisher
  end

  private

  def validate_and_set_dob
    return unless dob_y.present? || dob_m.present? || dob_d.present?

    begin
      self.dob = Date.strptime([dob_y, dob_m, dob_d].join("-"), "%Y-%m-%d")
      nil
    rescue
      errors.add(:dob, I18n.t("publishing.dob_invalid"))
      self.dob = nil
    end
  end

  def cae_length_valid?
    if composer_pro_id.present? && composer_cae.present? && !composer_cae.length.between?(9, 11)
      errors.add(:composer_cae, I18n.t("publishing.cae_length_error"))
    elsif needs_publisher_cae? && publisher_cae.present? && !publisher_cae.length.between?(9, 11)
      errors.add(:publisher_cae, I18n.t("publishing.cae_length_error"))
    end
  end

  def cae_present_with_no_pro?
    return unless composer_pro_id.blank? && composer_cae.present?

    errors.add(:composer_cae, I18n.t("publishing.cant_be_present_without_pro"))
  end

  def valid_publishing_role_id?
    return if PublishingRole.exists?(publishing_role_id)

    errors.add(:publishing_role_id, I18n.t("publishing.is_not_included_in_the_list"))
  end

  def pub_cae_different_from_composer_cae?
    errors.add(:publisher_cae, I18n.t("publishing.caes_cannot_be_the_same")) if publisher_cae == composer_cae
  end

  def authorized_to_update?
    raise ActionController::MethodNotAllowed if id.present? && !account.publishing_composers.exists?(id: id)
  end

  def needs_publisher_name?
    eligible_publishing_pro?(composer_pro_id) && publisher_cae.present?
  end

  def needs_publisher_cae?
    eligible_publishing_pro?(composer_pro_id) && publisher_name.present?
  end

  def publishing_composer_params
    params = {
      name_prefix: name_prefix,
      first_name: first_name,
      middle_name: middle_name,
      last_name: last_name,
      name_suffix: name_suffix,
      dob: dob,
      cae: composer_cae,
      email: email,
      person_id: on_behalf_of_self? ? person_id : nil,
      account_id: account.id,
      publishing_role_id: publishing_role_id,
      performing_rights_organization_id: composer_pro_id,
      agreed_to_terms_at: DateTime.current,
      is_primary_composer: true
    }

    sanitize_preexisting_publishing_composer_params(params)
  end

  def sanitize_preexisting_publishing_composer_params(params)
    return params if person&.is_administrator? || !purchased_pub_admin?

    attrs = [
      :name_prefix,
      :first_name,
      :middle_name,
      :last_name,
      :name_suffix,
      :dob,
      :cae,
      :performing_rights_organization_id,
      :agreed_to_terms_at
    ]

    attrs.each do |attribute|
      params.delete(attribute) if publishing_composer&.send(attribute).present?
    end

    params
  end

  def load_publishing_composer
    if id.present?
      account.publishing_composers.find(id)
    elsif account.publishing_composers.exists?(email: email)
      account.publishing_composers.find_by(email: email)
    else
      account.publishing_composers.build
    end
  end

  def publisher_params
    params = {
      name: publisher_name,
      cae: publisher_cae,
      performing_rights_organization_id: composer_pro_id
    }

    sanitize_publisher_params(params)
  end

  def sanitize_publisher_params(params)
    return params if person&.is_administrator? || !purchased_pub_admin?

    [:name, :cae].each do |attr|
      params.delete(attr) if publishing_composer.publisher && publishing_composer.publisher.public_send(attr)
    end

    params
  end

  def publisher_params_empty?
    publisher_params.values_at(:name, :performing_rights_organization_id, :cae)
                    .map(&:to_s)
                    .any?(&:empty?)
  end

  def account
    @account ||= Account.where(person_id: person_id, account_type: "publishing_administration").first_or_create
  end

  def on_behalf_of_self?
    PublishingRole.find(publishing_role_id).on_behalf_of_self?
  end

  def account_needs_publishing_composer?
    !on_behalf_of_self? && !PublishingComposer.exists?(person_id: person_id)
  end

  def create_publishing_composer_for_account
    person.publishing_composers.create!(
      first_name: person.first_name,
      last_name: person.last_name,
      email: email,
      dob: "1900-01-01",
      agreed_to_terms_at: "1900-01-01",
      account_id: account.id,
      person_id: person.id,
      publishing_role_id: PublishingRole::ON_BEHALF_OF_SELF_ID
    )
  end

  def full_name_affixed
    [name_prefix, first_name, middle_name, last_name, name_suffix]
      .compact.map(&:strip).reject(&:empty?).join(" ")
  end

  def update_publishing_compositions
    return unless action&.to_sym == :update && status == :active

    PublishingAdministration::PublishingCompositionUpdateWorker.perform_async(publishing_composer.id)
  end

  def send_to_publishing_administration
    return unless status == :active

    if publishing_composer.provider_identifier.present?
      PublishingAdministration::PublishingWriterCreationWorker.perform_async(publishing_composer.id)
    else
      PublishingAdministration::PublishingArtistAccountWriterWorker.perform_async(publishing_composer.id)
    end
  end

  def purchased_pub_admin?
    person&.purchased_pub_admin?
  end

  def send_lod_mail
    DocuSign::LodApi.send_lod_mail(person, publishing_composer) if @added_publisher
  end

  def agreed_to_tos?
    errors.add(:tos_agreed, I18n.t("publishing.tos_error")) if publishing_composer.new_record? && tos_agreed != "1"
  end

  def add_publishing_administration_product_to_cart
    Product.add_to_cart(person, publishing_composer) if @new_record
  end
end
