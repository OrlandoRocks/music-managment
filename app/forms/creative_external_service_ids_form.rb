class CreativeExternalServiceIdsForm < FormObject
  attr_accessor :creative_id, :spotify_artist_id, :apple_artist_id, :creative

  validates_presence_of :creative

  def creative
    @creative ||= Creative.find_by(id: creative_id)
  end

  def save
    return false unless valid?

    update_creative_external_service_ids
  end

  private

  def update_creative_external_service_ids
    CreativeExternalServiceIdsService.update(params)
  end

  def params
    {
      creative: creative,
      spotify_artist_id: spotify_artist_id,
      apple_artist_id: apple_artist_id
    }
  end
end
