class Admin::RecoverUserAssetsForm < FormObject
  attr_accessor :person, :admin, :album_ids_and_isrcs, :include_assets

  validates_presence_of :person, :admin, :include_assets
  validate :check_user_albums

  def save
    return false unless valid?

    create_recover_asset
    enqueue_recover_asset
  end

  private

  def albums
    @albums ||= BulkAlbumFinder.new(album_ids_and_isrcs).execute
  end

  def create_recover_asset
    @recover_asset = RecoverAsset.create(
      person: person,
      admin_id: admin.id,
      custom_fields: { albums_ids: person_albums, include_assets: include_assets }
    )
  end

  def person_albums
    person.albums.where(id: albums.pluck(:id)).pluck(:id)
  end

  def enqueue_recover_asset
    @recover_asset.update(state: "enqueued")
    RecoverUserAssetsWorker.perform_async(@recover_asset.id)
  end

  def check_user_albums
    return unless split_album_ids_and_isrcs.size != person_albums.size

    errors.add(
      :album_ids_and_isrcs,
      "Some of the albums doesn't belong to user, please check the albums and submit again."
    )
  end

  def split_album_ids_and_isrcs
    if album_ids_and_isrcs.include?(",")
      album_ids_and_isrcs.split(/,/).map(&:strip)
    else
      album_ids_and_isrcs.split
    end
  end
end
