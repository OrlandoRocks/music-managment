class Admin::MassRetrySongsForm < FormObject
  TRACK_MONETIZATION = "TrackMonetization".freeze
  DISTRIBUTION_SONG = "DistributionSong".freeze

  attr_accessor :store_ids, :delivery_type, :song_ids_and_isrcs, :admin, :takedown

  validates_presence_of :store_ids, :delivery_type, :song_ids_and_isrcs, :admin
  validate :store_ids_in_retry_song_stores

  def save
    return false unless valid?

    stores.each do |store|
      distributables_for_store(store).find_each do |distributable|
        distributable.update_attribute(:takedown_at, Time.now) if takedown
        distributable.delivery_type = delivery_type
        distributable.retry(
          actor: "TuneCore admin: #{admin.name}",
          message: "Retrying delivery #{distributable.id} (#{delivery_type})"
        )
      end
    end
  end

  private

  def store_ids_in_retry_song_stores
    return if Store.mass_retry_song_stores.where(id: store_ids).exists?

    errors.add(:store_ids, "Store IDs must be in track level distribution stores")
  end

  def distributables_for_store(store)
    distributable_class =
      store.store_delivery_config.distributable_class

    song_ids = songs.map(&:id)

    if distributable_class == DISTRIBUTION_SONG
      DistributionSong.joins(salepoint_song: [:song, :salepoint])
                      .where(salepoints: { store_id: store.id }, songs: { id: song_ids }).readonly(false)
    else
      ::TrackMonetization.where(store_id: store.id, song_id: song_ids)
    end
  end

  def stores
    @stores ||= Store.where(id: store_ids).includes(:store_delivery_config)
  end

  def ids_and_isrcs
    case song_ids_and_isrcs
    when Array
      song_ids_and_isrcs
    when String
      song_ids_and_isrcs.split(/\n|\s|,/).reject(&:blank?)
    else
      []
    end
  end

  def songs
    ids, isrcs = ids_and_isrcs.partition { |identifier| identifier.to_s.match(/^\d/) }

    @songs ||= Song.includes(:album).where(id: ids)
                   .or(Song.includes(:album).where(tunecore_isrc: isrcs))
                   .or(Song.includes(:album).where(optional_isrc: isrcs)).uniq
  end
end
