class Distribution::ErrorForm < FormObject
  attr_writer :start_date, :end_date, :filter_type
  attr_reader :distributions, :state_type
  attr_accessor :states

  define_model_callbacks :save, only: [:after]
  after_save :query_for_erred_distributions

  validates_presence_of :start_date, :end_date, :filter_type, :states

  def initialize(params)
    super
    @states = states_from_params(params) || ["error"]
  end

  def states_from_params(params)
    (params[:states].nil? || params[:states] == "error") ? ["error"] : ["packaged", "gathering_assets"]
  end

  def save
    return false unless valid?

    run_callbacks :save and return self
  end

  def start_date
    if @start_date.respond_to?(:strftime)
      @start_date
    elsif @start_date
      DateTime.strptime(@start_date, "%m/%d/%Y").to_time
    else
      (DateTime.now - 1).to_time
    end
  end

  # TODO: refactor. Super confusing
  # rubocop:disable Lint/NoReturnInBeginEndBlocks
  def end_date
    @end_time ||=
      begin
        return DateTime.now unless @end_date

        DateTime.strptime(@end_date, "%m/%d/%Y") + Time.now.seconds_since_midnight.seconds
      end
  end
  # rubocop:enable Lint/NoReturnInBeginEndBlocks

  def filter_type
    @filter_type ||= "store"
  end

  def state_type
    states.include?("error") ? "error" : "stuck"
  end

  def query_for_erred_distributions
    @distributions = Distribution::ErrorQueryBuilder.build(
      start_date: start_date,
      end_date: end_date,
      filter_type: filter_type,
      states: states
    )
  end
end
