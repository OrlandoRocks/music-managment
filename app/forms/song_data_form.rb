# frozen_string_literal: true

class SongDataForm < FormObject
  include ActiveModel::Validations::Callbacks

  define_model_callbacks :save

  validates_with SongDataFormValidator
  validate :has_songwriter, unless: :skip_has_songwriter
  validate :simple_copyrights, if: :should_build_copyrights
  validate :has_song_start_times, if: :should_build_start_times
  validate :validate_creatives_uniqueness
  validate :song_cannot_be_added, if: :unfinalized_and_creating?
  validate :cover_song_metadata_params_present, if: :should_build_cover_song_metadata

  after_validation :build_song_data, if: :persisted_with_errors?

  after_save  :build_lyrics
  after_save  :build_creatives
  after_save  :build_copyrights, if: :should_build_copyrights
  after_save  :build_song_start_times, if: :should_build_start_times
  after_save  :build_cover_song_metadata, if: :should_build_cover_song_metadata
  after_save  :build_song_data
  after_save  :update_name, if: :song_on_single_or_ringtone?
  after_save  :match_esids

  COPYRIGHTABLE_TYPE = "Song"
  NULLABLE_ATTRIBUTES = %w[
    cover_song
    instrumental
    made_popular_by
    name
    optional_isrc
    explicit
    clean_version
    previously_released_at
    version
    translated_name
  ].freeze

  attr_reader :song,
              :artist_params,
              :song_data,
              :completion_report,
              :song_params,
              :skip_has_songwriter,
              :should_build_copyrights,
              :should_build_start_times,
              :should_build_cover_song_metadata,
              :copyright_param,
              :cover_song_metadata_params

  alias_method :data, :song_data

  def self.build(options)
    new(options).tap(&:build)
  end

  def self.save(options)
    new(options).tap(&:save)
  end

  def initialize(options)
    @person_id   = options[:person_id]
    @album_id    = options[:album_id] || options[:song_params].try(:[], :album_id)
    @person      = Person.find_by(id: @person_id)
    @album       = @person.albums.find_by(id: @album_id) if @person.present?
    @song_params = SongData::ParamsScrubber.scrub((options[:song_params] || {}), @album)
    @copyright_param = extract_copyright_param(options)
    @cover_song_metadata_params = extract_cover_song_metadata_params(options)
    @song = options[:song] || song_from_params
    @skip_has_songwriter = should_skip_has_songwriter?(options)
    @should_build_copyrights = options[:should_build_copyrights] || false
    @should_build_start_times = options[:should_build_start_times] || false
    @should_build_cover_song_metadata = options[:should_build_cover_song_metadata] || false
  end

  def save
    return false unless valid?

    run_callbacks :save do
      pruned_attributes = pruned_song_attributes
      song.update(pruned_attributes)
      valid?
    end
  end

  delegate :valid?, to: :song, prefix: true

  def persisted_with_errors?
    song.persisted? && errors.any?
  end

  # see app/services/song_data.rb
  def build
    build_song_data
  end

  def build_creatives
    clear_creatives
    uniq_creatives.each do |artist_attributes|
      build_creative(artist_attributes)
    end
  end

  def build_song_start_times
    clear_song_start_times

    uniq_song_start_time_params.each do |song_start_time_attributes|
      build_song_start_time(song_start_time_attributes)
    end
  end

  def build_lyrics
    Lyric.where(song_id: @song.id).first_or_create.update(content: @song_params[:lyrics])
  end

  def build_song_data
    @song_data = SongDataBuilder.build(@person_id, @song)
  end

  def song_on_single_or_ringtone?
    @album.single? || @album.ringtone?
  end

  def update_name
    @album.update(name: @song.name)
  end

  private

  def extract_copyright_param(options)
    options[:song_params].try(:[], :copyrights)&.slice(:id, :composition, :recording)
  end

  def extract_cover_song_metadata_params(options)
    options[:song_params].try(:[], :cover_song_metadata)&.slice(:id, :cover_song, :licensed, :will_get_license)
  end

  def has_songwriter
    songwriter_role_id = SongRole.where(role_type: "songwriter").pluck(:id).first
    song_has_at_least_one_songwriter = song_params[:artists] && song_params[:artists].any? do |artist|
      (artist[:role_ids] || []).map(&:to_i).include?(songwriter_role_id)
    end
    errors.add(:songwriter, I18n.t("required_field")) unless song_has_at_least_one_songwriter
  end

  def has_song_start_times
    song_start_times = song_params[:song_start_times]

    all_song_start_times_have_required_values =
      song_start_times.all? do |song_start_time|
        song_start_time_keys = [:store_id, :start_time]

        song_start_time_keys.all? { |key| song_start_time[key].to_s.strip.present? }
      end

    errors.add(:song_start_times, I18n.t("required_field")) unless all_song_start_times_have_required_values
  end

  def unfinalized_and_creating?
    @album && @album.unfinalized? && !song.persisted?
  end

  def song_cannot_be_added
    errors.add(:song, "cannot be added")
    false
  end

  def song_from_params
    song_by_id || new_song_for_album
  end

  def genre
    @genre ||= Genre
               .joins("join albums on albums.primary_genre_id = genres.id")
               .where({ albums: { id: @album_id } })
               .first
  end

  def song_by_id
    Song.find_by(id: @song_params[:id])
  end

  def new_song_for_album
    Song.new(album_id: @album_id, name: @song_params[:name])
  end

  def song_attributes
    {
      cover_song: @song_params[:cover_song],
      instrumental: @song_params[:instrumental],
      metadata_language_code_id: language_id,
      lyric_language_code_id: lyric_language_id,
      made_popular_by: @song_params[:made_popular_by],
      name: @song_params[:name],
      optional_isrc: @song_params[:optional_isrc],
      parental_advisory: @song_params[:explicit],
      clean_version: @song_params[:clean_version],
      previously_released_at: auto_date_format(@song_params[:previously_released_at]),
      song_version: @song_params[:version],
      translated_name: @song_params[:translated_name]
    }
  end

  # The V2 flow omits certain keys in each step in order to not
  # persist default or fallback values prematurely.
  def pruned_song_attributes
    missing_keys = NULLABLE_ATTRIBUTES.select { |k| @song_params.key?(k) == false }.map(&:to_sym)
    song_attributes.except(*missing_keys)
  end

  def language_id
    @song.album.try(:language).try(:id) || LanguageCode.first.id
  end

  def lyric_language_id
    @song_params[:language_code_id] || @song.album.try(:language).try(:id) || LanguageCode.first.id
  end

  def clear_creatives
    creative_ids = uniq_creatives.pluck(:creative_id).compact

    creatives = Creative.where(
      creativeable_id: creative_data[:creativeable_id],
      creativeable_type: creative_data[:creativeable_type]
    )

    creatives.where.not(id: creative_ids).destroy_all
  end

  def clear_song_start_times
    song_start_time_ids = uniq_song_start_time_params.pluck(:id).compact

    song_start_times = SongStartTime.where(song_id: song.id)

    song_start_times.where.not(id: song_start_time_ids).destroy_all
  end

  def uniq_creatives
    @uniq_creatives ||= @song_params[:artists].uniq { |artist| artist[:artist_name] }
  end

  def uniq_copyrights
    @uniq_copyrights ||=
      @song_params[:copyrights].uniq { |copyright|
        [copyright[:holder_name], copyright[:year], copyright[:ownership]]
      }
  end

  def uniq_song_start_time_params
    @uniq_song_start_time_params ||=
      @song_params[:song_start_times].uniq { |song_start_time|
        song_start_time[:store_id]
      }
  end

  def creative_data
    @creative_data ||= {
      creativeable_id: (song.album_type == "Single") ? song.album_id : song.id,
      creativeable_type: (song.album_type == "Single") ? "Album" : "Song"
    }
  end

  def copyright_data
    @copyright_data ||= {
      copyrightable_id: song.id,
      copyrightable_type: song.class.name
    }
  end

  def build_creative(artist_attributes)
    artist       = build_artist(artist_attributes)
    role_ids     = artist_attributes[:role_ids] || []
    creativeable = (artist_attributes[:associated_to] == "Album") ? @song.album : @song
    creative     = create_or_update_creative(creativeable, artist, artist_attributes)
    build_song_roles(creative, role_ids)
  end

  def build_artist(artist_attributes)
    if artist_attributes[:creative_id].blank?
      artist = Artist.create_by_nontitleized_name(artist_attributes[:artist_name])
      return artist
    end

    creative = Creative.find(artist_attributes[:creative_id])

    if artist_attributes[:artist_name] != creative.artist.name
      creative.artist = Artist.create_by_nontitleized_name(artist_attributes[:artist_name])
      creative.save
    end

    creative.artist
  end

  def create_or_update_creative(creativeable, artist, artist_attributes)
    creative = Creative.find_or_create_by(
      creativeable_id: creativeable.id,
      creativeable_type: [Single, Album].include?(creativeable.class) ? "Album" : "Song",
      artist_id: artist.id,
      person_id: @person_id
    )

    if creative.role != artist_attributes[:credit]
      creative.update(role: artist_attributes[:credit])
    elsif creative.role == "with"
      creative.update(role: "featuring")
    end

    creative
  end

  def build_song_roles(creative, role_ids)
    clear_creative_song_roles(creative)
    create_creative_song_roles(creative, role_ids)
  end

  def clear_creative_song_roles(creative)
    CreativeSongRole.where(creative_id: creative.id, song_id: @song.id).delete_all
  end

  def create_creative_song_roles(creative, role_ids)
    role_ids.uniq.each do |role_id|
      next if role_id.to_i.zero?

      CreativeSongRole.create(
        creative_id: creative.id,
        song_role_id: role_id,
        song_id: @song.id
      )
    end
  end

  def creatives
    Creative.arel_table
  end

  def simple_copyrights
    copyrights = copyright_param
    return true if copyrights[:composition] && copyrights[:recording]

    errors.add(:copyrights, I18n.t("required_field"))
  end

  def build_copyrights
    copyright = get_copyright

    copyright.update(copyright_param)

    copyright
  end

  def cover_song_metadata_params_present
    return true if cover_song_metadata_params[:cover_song].to_s.present?

    errors.add(:cover_song_metadata, I18n.t("required_field"))
  end

  def build_cover_song_metadata
    cover_song_metadata = get_cover_song_metadata

    cover_song_metadata.update(cover_song_metadata_params)

    cover_song_metadata
  end

  def get_copyright
    Copyright.find_by(id: copyright_param.try(:[], :id)) || Copyright.find_or_initialize_by(
      copyrightable_id: song.id,
      copyrightable_type: COPYRIGHTABLE_TYPE
    )
  end

  def get_cover_song_metadata
    CoverSongMetadata.find_by(id: cover_song_metadata_params.try(:[], :id)) ||
      CoverSongMetadata.find_or_initialize_by(song_id: song.id)
  end

  def build_song_start_time(song_start_time_attributes)
    song_start_time = @song.song_start_times.find_or_initialize_by(
      id: song_start_time_attributes[:id]
    )

    song_start_time.update(song_start_time_attributes)

    song_start_time
  end

  def auto_date_format(date_string)
    return unless date_string

    strp_str = "%m/%d/" + ((date_string =~ /\d{4}/) ? "%Y" : "%y")
    Date.parse(date_string) rescue Date.strptime(date_string, strp_str)
  end

  def validate_creatives_uniqueness
    return if song_params.blank?

    album_creatives =
      uniq_creatives.find_all { |album_attr|
        album_attr[:credit] == "primary_artist" and album_attr[:associated_to] == "Album"
      }
    if album_creatives.present? && uniq_creatives.find { |album_attr|
         album_creatives.map { |artist|
           album_attr[:credit] != "primary_artist" and artist[:artist_name]
         }.compact.include?(album_attr[:artist_name].strip)
       }

      errors.add(:creatives, I18n.t("songs_app.form_field_warnings.artist_name.duplicate_artist_name"))
    end
  end

  def should_skip_has_songwriter?(options)
    FeatureFlipper.show_feature?(:album_app_v2, @person) ||
      options[:skip_has_songwriter] ||
      false
  end

  def match_esids
    ArtistIdMatcherWorker.perform_async(@album.creatives.map(&:id))
  end
end
