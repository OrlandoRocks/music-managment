class Distributor::ErrorForm < FormObject
  attr_writer :start_date, :end_date
  attr_reader :distributor_errors, :distributor_errors_total_count
  attr_accessor :error_type, :store_id, :filter_type

  define_model_callbacks :save, only: [:after]
  after_save :query_for_errored_releases

  validates_presence_of :start_date, :end_date, :filter_type, :error_type, :store_id

  ERROR_TYPES = [
    ["All States", "All"],
    ["Asset Ingestion Error", "asset_ingestion"],
    ["Delivery Error", "delivery"],
    ["Json Ingestion Error", "json_ingestion"],
    ["Technical Error", "technical"],
    ["TC-Distributor Error", "tc_distributor_error"]
  ].freeze

  ERRORS_LIMIT = 21_000

  def initialize(params)
    super
    @error_type ||= "All"
    @filter_type ||= "store_id"
    @store_id ||= [0]
  end

  def save
    return false unless valid?

    run_callbacks :save and return self
  end

  def start_date
    format_start_date(start_date_param: @start_date.presence || (DateTime.current - 1)).to_time
  end

  def end_date
    format_end_date(end_date_param: @end_date.presence || DateTime.current).to_time
  end

  def error_types
    ERROR_TYPES
  end

  def stores
    stores = Store.where(delivery_service: "tc_distributor").order(:name).pluck(:name, :id)
    [["All", 0]] + stores
  end

  def query_for_errored_releases
    args = {
      start_date: start_date,
      end_date: end_date,
      error_type: error_type,
      limit: ERRORS_LIMIT
    }
    args[:store_id] = store_id unless Array(store_id).include?("0")

    begin
      failed_deliveries = Array(DistributorAPI::ReleaseError.failed_deliveries(args))[0]
      @distributor_errors = failed_deliveries&.http_body["failed_deliveries"]
      @distributor_errors_total_count = failed_deliveries&.http_body["total"]
    rescue Faraday::Error::ConnectionFailed => e
      Rails.logger.error("Tc-Distributor is not responding: #{e.message}")
      {}
    rescue Exception => e
      Rails.logger.error("Tc-Distributor is returning corrupted data: #{e.message}")
      {}
    end
  end

  private

  def format_start_date(start_date_param:)
    return start_date_param if start_date_param.respond_to?(:strftime)

    DateTime.strptime(start_date_param, "%m/%d/%Y")
  end

  def format_end_date(end_date_param:)
    return end_date_param if end_date_param.respond_to?(:strftime)

    DateTime.strptime(end_date_param, "%m/%d/%Y")
  end
end
