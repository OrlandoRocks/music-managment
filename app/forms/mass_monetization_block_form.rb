class MassMonetizationBlockForm < FormObject
  attr_accessor :store_ids, :song_identifiers, :find_by, :admin, :ip_address

  validates_presence_of :store_ids, :song_identifiers

  define_model_callbacks :save

  def save
    return false unless valid?

    run_callbacks :save do
      return true if block_track_monetizations

      errors.add(:base, "Something went wrong")
    end
  end

  private

  def block_track_monetizations
    MassMonetizationBlockService.block(params)
  end

  def params
    {
      song_ids: song_ids,
      store_ids: store_ids,
      admin: admin,
      ip_address: ip_address
    }
  end

  def song_ids
    @song_ids ||= songs.map(&:id)
  end

  def songs
    @songs ||= BulkSongFinder.new(song_identifiers, find_by: find_by).execute
  end
end
