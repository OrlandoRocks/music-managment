class TaxTokensCohortForm < FormObject
  define_model_callbacks :save, :update

  attr_accessor :id, :name, :start_date, :end_date, :file, :tax_tokens_cohort

  validates_presence_of :name, :start_date, :end_date
  validates_presence_of :file, if: :new_record?
  validate :date_validation

  def initialize(params)
    super params
    @tax_tokens_cohort = TaxTokensCohort.where(id: id).first_or_initialize
  end

  def new_record?
    @tax_tokens_cohort.new_record?
  end

  def date_validation
    start_date = Date.parse(params[:start_date])
    end_date = Date.parse(params[:end_date])
    errors.add(:start_date, "Blocked Date cannot be earlier or the same as the send date") if end_date <= start_date
  rescue ArgumentError
    errors.add(:start_date, "Please enter valid dates.")
  end

  def update(attributes)
    return false unless valid?

    run_callbacks :update do
      @tax_tokens_cohort.update(attributes)
      errors.add(:base, "Something went wrong")
    end
  end

  def save
    return false unless valid?

    run_callbacks :save do
      return true if TaxTokensCohortService.create(params)

      errors.add(:base, "Something went wrong")
    end
  end

  private

  def params
    {
      name: name,
      start_date: start_date,
      end_date: end_date,
      file: file
    }
  end
end
