import dayjs from 'dayjs'

import { IS_DEV } from '../album_app/utils/constants'

const onlyDate = (date) => dayjs(date).format('YYYY-MM-DD')

const parseYesNo = (val) => val === 'Yes'

const isEmpty = (v) => {
  switch (true) {
    case typeof v === 'string': {
      return !v || 0 === v.trim().length
    }
    case typeof v === 'object' && v !== null: {
      return Object.keys(v).length === 0
    }
    case Array.isArray(v): {
      return v.length === 0
    }
    case v === null || v === undefined: {
      return true
    }
    default:
      throw 'Type not implemented.'
  }
}

function devLogger(obj, label = '') {
  if (IS_DEV) console.log(label, obj)
}

const VOWELS_REGEX = /^[aeiouAEIOU]/
const FIRST_CHAR_REGEX = /^\w{1}/
function simpleAOrAn(str, caps = false) {
  if (typeof str !== 'string') return str

  let result
  const firstChar = str[0]
  result = VOWELS_REGEX.test(firstChar) ? `an ${str}` : `a ${str}`
  result = caps
    ? result.replace(FIRST_CHAR_REGEX, (match) => match.toLocaleUpperCase())
    : result
  return result
}

export { isEmpty, devLogger, onlyDate, parseYesNo, simpleAOrAn }
