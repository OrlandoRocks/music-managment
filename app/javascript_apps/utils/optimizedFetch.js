export default async function optimizedFetch(url, args = {}) {
  const { headers } = args

  const searchParams = args.searchParams || {}

  const encodedSearchParams = new URLSearchParams(searchParams)

  const encodedSearchParamsString = encodedSearchParams.toString()

  const processedUrl = `${url}?${encodedSearchParamsString}`

  const response = await fetch(processedUrl, { headers: headers })

  return response
}
