import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import ProgressBarContainer from './components/ProgressBarContainer'
const progressBarNode = document.getElementById('distribution_progress_bar_app')

ReactDOM.render(
  <ProgressBarContainer progressBarNode={progressBarNode} />,
  progressBarNode
)
