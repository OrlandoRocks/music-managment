import React, { useLayoutEffect, useEffect, useState } from 'react'
import ProgressText from './ProgressText'
import ProgressBar from './ProgressBar'

export default function ProgressBarContainer({ progressBarNode }) {
  const [songsCompleteInCount, setSongsCompleteInCount] = useState(
    progressBarNode.dataset.songsComplete == 'true'
  )
  const [progressLevel, setProgressLevel] = useState(
    parseInt(progressBarNode.dataset.progressLevel)
  )
  const translations = JSON.parse(progressBarNode.dataset.translations)
  const initialOffsetTop = progressBarNode.offsetTop
  const defaultStyles = {
    backgroundColor: 'white',
    zIndex: '3',
  }

  const [containerStyles, setContainerStyles] = useState(defaultStyles)

  useLayoutEffect(() => {
    const stickTheProgressBar = () => {
      if (window.pageYOffset > initialOffsetTop) {
        setContainerStyles({
          ...containerStyles,
          position: 'fixed',
          top: 0,
          width: '100%',
        })
      } else {
        setContainerStyles(defaultStyles)
      }
    }

    window.addEventListener('scroll', stickTheProgressBar, true)

    return () => {
      window.removeEventListener('scroll', stickTheProgressBar)
    }
  }, [])

  useEffect(() => {
    const handleSongsCompleteListenerApi = async (e) => {
      const albumId = e.detail.albumId

      const res = await fetch(
        '/albums/' + albumId + '/distribution_songs_status'
      )
      const json = await res.json()
      const songsValid = json.status

      if (songsValid && !songsCompleteInCount) {
        setProgressLevel(progressLevel + 1)
        setSongsCompleteInCount(true)
      } else if (!songsValid && songsCompleteInCount) {
        setProgressLevel(progressLevel - 1)
        setSongsCompleteInCount(false)
      }
    }

    const progressBarApp = document.getElementById(
      'distribution_progress_bar_app'
    )
    progressBarApp.addEventListener(
      'update-checkout-button',
      handleSongsCompleteListenerApi
    )

    return () => {
      progressBarApp.removeEventListener(
        'update-checkout-button',
        handleSongsCompleteListenerApi
      )
    }
  }, [songsCompleteInCount, progressLevel])

  return (
    <div className={'distributionProgressBarContainer'} style={containerStyles}>
      <ProgressText level={progressLevel} translations={translations} />
      <ProgressBar level={progressLevel} />
    </div>
  )
}
