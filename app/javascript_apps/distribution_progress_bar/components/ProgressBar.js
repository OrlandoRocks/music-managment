import React, { useEffect, useState } from 'react'

export default function ProgressBar({ level }) {
  const [barWidth, setBarWidth] = useState(0)
  const finalWidth = (level / 4) * 100

  useEffect(() => {
    setBarWidth(finalWidth)
  })

  return (
    <div style={{ position: 'relative', height: '10px' }}>
      <div
        style={{
          height: '10px',
          width: '100%',
          backgroundColor: '#e2f8ff',
          position: 'absolute',
        }}
      ></div>
      <div
        className={`distributionProgressBarLevel${level}`}
        style={{
          height: '10px',
          backgroundImage: 'linear-gradient(to right, lightblue , #3b96cf)',
          width: `${barWidth}%`,
          position: 'absolute',
          transition: 'width 1s',
          transitionDelay: '0.5s',
        }}
      ></div>
    </div>
  )
}
