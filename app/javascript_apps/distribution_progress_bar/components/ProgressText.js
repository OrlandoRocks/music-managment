import React from 'react'

export default function ProgressText({ level, translations }) {
  const { of_steps, completed } = translations.progress_bar

  return (
    <div style={{ textAlign: 'center', padding: '10px', fontSize: '14px' }}>
      {level} {of_steps} <b>{completed}</b>
    </div>
  )
}
