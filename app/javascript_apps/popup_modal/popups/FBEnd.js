import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  body: {
    fontSize: '1.5em',
    fontWeight: 500,
    margin: '10px 20px',
  },
  button: {
    fontSize: '1.5em',
    margin: '1em 0',
  },
  buttonContainer: {
    textAlign: 'center',
  },
  header: {
    fontSize: '2em',
    fontWeight: 700,
    margin: '10px 20px',
  },
  headingSection: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '20px',
  },
  subTitle: {
    fontSize: '1.5em',
    fontWeight: 700,
    margin: '10px 20px',
  },
}

export default function FBEnd({ toggle, translations }) {
  const { header, body_1, body_2, body_3, body_4, button } = translations

  return (
    <>
      <div style={styles.headingSection}>
        <h1 style={styles.header}>{header}</h1>
        <p style={styles.subTitle}>{body_1}</p>
      </div>

      <p style={styles.body}>{body_2}</p>
      <p style={styles.body}>{body_3}</p>
      <p style={styles.body}>{body_4}</p>

      <div style={styles.buttonContainer}>
        <button
          className="button-callout-alt"
          style={styles.button}
          onClick={toggle}
        >
          {button}
        </button>
      </div>
    </>
  )
}
FBEnd.propTypes = {
  toggle: PropTypes.func.isRequired,
  translations: PropTypes.object.isRequired,
}
