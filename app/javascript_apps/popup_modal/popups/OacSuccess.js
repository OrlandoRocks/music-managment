import React from 'react'

export default function OacSuccess({ translations }) {
  const { message } = translations

  return (
    <div className="oac_success_container">
      <div className="oac_success_heading_section">
        <p className="oac_success_message">{message}</p>
      </div>
    </div>
  )
}
