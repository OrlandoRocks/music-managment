import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  button: {
    fontSize: '1em',
    borderRadius: 6,
    background: '#44b1fa',
    color: '#fff',
    cursor: 'pointer',
    padding: '.75em',
    margin: 0,
    verticalAlign: 'middle',
  },
  buttonContainer: {
    textAlign: 'center',
  },
}

export default function FBTerms({ toggle, translations }) {
  const { intro, link_text, list, ok, outro, reels_description } = translations

  const outroWithLink = outro.replace(
    '{{link}}',
    `<a href='${reels_description} rel='noopener noreferrer' target='_blank'>${link_text}</a>`
  )

  return (
    <div style={styles.container}>
      <div
        style={{
          backgroundImage: `url("/images/facebook.png")`,
          backgroundPosition: '-40px 0',
          backgroundRepeat: 'no-repeat',
          height: '40px',
          margin: '0 auto 1em',
          width: '40px',
        }}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: intro,
        }}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: list,
        }}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: outroWithLink,
        }}
      />
      <div style={styles.buttonContainer}>
        <button
          className="button-callout-alt"
          style={styles.button}
          onClick={toggle}
        >
          {ok}
        </button>
      </div>
    </div>
  )
}
FBTerms.propTypes = {
  toggle: PropTypes.func.isRequired,
  translations: PropTypes.object.isRequired,
}
