import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  body: {
    fontSize: '1.5em',
    fontWeight: 500,
    margin: '10px 20px',
  },
  buttonContainer: {
    textAlign: 'center',
  },
  button: {
    fontSize: '1.5em',
  },
  header: {
    fontSize: '2em',
    fontWeight: 700,
    margin: '10px 20px',
  },
  headingSection: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '2em',
  },
}

export default function FBStart({ toggle, translations }) {
  const { header, body, button } = translations

  return (
    <div>
      <div style={styles.headingSection}>
        <h1 style={styles.header}>{header}</h1>
        <p style={styles.body}>{body}</p>
      </div>
      <div style={styles.buttonContainer}>
        <button
          className="button-callout-alt"
          style={styles.button}
          onClick={toggle}
        >
          {button}
        </button>
      </div>
    </div>
  )
}
FBStart.propTypes = {
  toggle: PropTypes.func.isRequired,
  translations: PropTypes.object.isRequired,
}
