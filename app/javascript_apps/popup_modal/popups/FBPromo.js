import React from 'react'
import PropTypes from 'prop-types'

const SINGLES_PATH_SEGMENT = /^.*singles/

const styles = {
  button: {
    fontSize: '1.5em',
    margin: '1em 0',
  },
  header: {
    fontSize: '2em',
    fontWeight: 700,
    margin: '10px 20px',
  },
  headingSection: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '20px',
  },
}

export default function FBPromo({ toggle, translations }) {
  const { headline1, headline2, button1, button2 } = translations

  const storesPage = `${window.location.pathname}/salepoints`.replace(
    SINGLES_PATH_SEGMENT,
    '/albums'
  )

  return (
    <>
      <div style={styles.headingSection}>
        <h1 style={styles.header}>{headline1}</h1>
        <a className="btn btn-callout" style={styles.button} href={storesPage}>
          {button1}
        </a>
      </div>
      <div style={styles.headingSection}>
        <h1 style={styles.header}>{headline2}</h1>
        <button
          className="btn btn-callout"
          style={styles.button}
          onClick={toggle}
        >
          {button2}
        </button>
      </div>
    </>
  )
}
FBPromo.propTypes = {
  toggle: PropTypes.func.isRequired,
  translations: PropTypes.object.isRequired,
}
