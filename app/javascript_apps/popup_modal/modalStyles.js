import { FB_START, FB_END, FB_PROMO, FB_TERMS } from './constants'

export const defaultStyle = {
  content: {
    backgroundColor: '#fff',
    border: '1px solid #9f9f9f',
    borderRadius: '5px',
    height: 'fit-content',
    margin: '60px auto 0',
    maxWidth: '660px',
    minWidth: '280px',
    outline: 'none',
    padding: '20px',
    position: 'relative',
    top: '120px',
  },
  overlay: {
    backgroundColor: 'rgba(255, 255, 255, 0.75)',
    bottom: '0px',
    left: '0px',
    maxWidth: '100vw',
    overflowY: 'auto',
    position: 'fixed',
    right: '0px',
    top: '0px',
    zIndex: 6,
  },
}

const fbStyle = {
  ...defaultStyle,
  content: {
    ...defaultStyle.content,
    top: 'calc(10% - 48px)',
  },
}

export default {
  [FB_END]: fbStyle,
  [FB_PROMO]: fbStyle,
  [FB_START]: fbStyle,
  [FB_TERMS]: fbStyle,
}
