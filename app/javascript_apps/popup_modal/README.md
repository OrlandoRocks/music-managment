Trigger popups with the url query string param `popup`.

Requirements:

- a component in ./popups
- update ./constants
- translations in config/locales/javascripts/popup_modal/, under key matching `constants`
- CSS
  - css in app/assets/stylesheets/singles/
  - update config/initializers/assets.rb
  - update app/assets/stylesheets/main.css.scss
  - update `stylesheet_link_tag`s in app/views/singles/show.html.erb

TODO:

- CSS in JS lib (TBD) to be shared among `/javascript_apps`
