import React, { useState } from 'react'
import queryString from 'query-string'
import Modal from 'react-modal'

import OacSuccess from './popups/OacSuccess'

import FBEnd from './popups/FBEnd'
import FBPromo from './popups/FBPromo'
import FBStart from './popups/FBStart'
import FBTerms from './popups/FBTerms'

import modalStyles, { defaultStyle } from './modalStyles'

import {
  PARAM_NAME_MAP,
  OAC_SUCCESS,
  FB_END,
  FB_PROMO,
  FB_START,
  FB_TERMS,
} from './constants'

const POPUP_MODAL_DOM_ELEMENT = 'popup_modal'

const NAME_COMPONENT_MAP = {
  [OAC_SUCCESS]: OacSuccess,
  [FB_END]: FBEnd,
  [FB_PROMO]: FBPromo,
  [FB_START]: FBStart,
  [FB_TERMS]: FBTerms,
}

// Map user-facing params to components
function getCurrentPopup() {
  const mappedParam = PARAM_NAME_MAP[queryString.parse(location.search).popup]
  if (!mappedParam) return
  return {
    name: mappedParam,
    Popup: NAME_COMPONENT_MAP[mappedParam],
  }
}

export default function PopupContainer({ appElement }) {
  const currentPopup = getCurrentPopup()
  const [showPopUp, setShowPopup] = useState(Boolean(currentPopup))
  if (!currentPopup) return null

  const { name, Popup } = currentPopup
  const toggle = () => setShowPopup((showPopup) => !showPopup)
  const translations = window.translations.popup_modal[name]

  return (
    <Modal
      appElement={
        appElement || document.getElementById(POPUP_MODAL_DOM_ELEMENT)
      }
      className={`${name}_modal`}
      isOpen={showPopUp}
      onRequestClose={toggle}
      overlayClassName={`${name}_modal_overlay`}
      style={modalStyles[name] || defaultStyle}
    >
      <Popup toggle={toggle} translations={translations} />
    </Modal>
  )
}
