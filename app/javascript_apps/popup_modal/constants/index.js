const FB_END = 'fb_end'
const FB_PROMO = 'fb_promo'
const FB_START = 'fb_start'
const FB_TERMS = 'fb_terms'

const OAC_SUCCESS = 'oac_success'

// Map user-facing params to internal name
const PARAM_NAME_MAP = {
  fb_end: FB_END,
  fb_promo: FB_PROMO,
  fb_start: FB_START,
  fb_terms: FB_TERMS,
  oac_success: OAC_SUCCESS,
}

export { FB_START, FB_END, FB_PROMO, FB_TERMS, OAC_SUCCESS, PARAM_NAME_MAP }
