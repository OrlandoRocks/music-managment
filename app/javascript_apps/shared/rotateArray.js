export default function rotateArray(array, splitIndex) {
  const rightSide = array.slice(splitIndex)
  const leftSide = array.slice(0, splitIndex)

  return rightSide.concat(leftSide)
}
