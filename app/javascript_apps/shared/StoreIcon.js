import React from 'react'
import PropTypes from 'prop-types'

export default function StoreIcon({ abbrev }) {
  const { defaultStyle, stores } = storeIconStyles
  const compiledStyles = abbrev
    ? { ...defaultStyle, ...stores[abbrev] }
    : defaultStyle

  return (
    <div
      style={compiledStyles}
      data-testid={'store-icon'}
      className={abbrev || ''}
    ></div>
  )
}

StoreIcon.propTypes = {
  abbrev: PropTypes.string,
}

export const storeIconStyles = {
  defaultStyle: {
    background:
      "transparent url('/images/store_logos/stores_35x35_sprite.png?v=20170412') no-repeat top center",
    height: '35px',
    width: '35px',
  },
  stores: {
    ww: {
      backgroundPosition: '0 -385px',
    },
    az: {
      backgroundPosition: '0 -35px',
    },
    np: {
      backgroundPosition: '0 -70px',
    },
    em: {
      backgroundPosition: '0 -105px',
    },
    gn: {
      backgroundPosition: '0 -140px',
    },
    no: {
      backgroundPosition: '0 -175px',
    },
    la: {
      backgroundPosition: '0 -210px',
    },
    aod_us: {
      backgroundPosition: '0 -245px',
    },
    rh: {
      backgroundPosition: '0 -280px',
    },
    sz: {
      backgroundPosition: '0 -315px',
    },
    gp: {
      backgroundPosition: '0 -350px',
    },
    au: {
      backgroundPosition: '0 -455px',
    },
    jp: {
      backgroundPosition: '0 -490px',
    },
    eu: {
      backgroundPosition: '0 -525px',
    },
    mx: {
      backgroundPosition: '0 -560px',
    },
    ca: {
      backgroundPosition: '0 -595px',
    },
    my: {
      backgroundPosition: '0 -630px',
    },
    zn: {
      backgroundPosition: '0 -665px',
    },
    tp: {
      backgroundPosition: '0 -700px',
    },
    mn: {
      backgroundPosition: '0 -735px',
    },
    sp: {
      backgroundPosition: '0 -770px',
    },
    ac: {
      backgroundPosition: '0 -805px',
    },
    uk: {
      backgroundPosition: '0 -840px',
    },
    gm: {
      backgroundPosition: '0 -875px',
    },
    st: {
      backgroundPosition: '0 -910px',
    },
    sma: {
      backgroundPosition: '0 -945px',
    },
    dz: {
      backgroundPosition: '0 -980px',
    },
    jk: {
      backgroundPosition: '0 -2415px',
    },
    yt: {
      backgroundPosition: '0 -1050px',
    },
    sd: {
      backgroundPosition: '0 -2695px',
    },
    mv: {
      backgroundPosition: '0 -1120px',
    },
    pa: {
      backgroundPosition: '0 -1155px',
    },
    rd: {
      backgroundPosition: '0 -1190px',
    },
    wi: {
      backgroundPosition: '0 -1225px',
    },
    su: {
      backgroundPosition: '0 -1260px',
    },
    jb: {
      backgroundPosition: '0 -1295px',
    },
    sk: {
      backgroundPosition: '0 -1330px',
    },
    bm: {
      backgroundPosition: '0 -1365px',
    },
    gvr: {
      backgroundPosition: '0 -1400px',
    },
    akz: {
      backgroundPosition: '0 -1435px',
    },
    ang: {
      backgroundPosition: '0 -1470px',
    },
    kkb: {
      backgroundPosition: '0 -1505px',
    },
    rvb: {
      backgroundPosition: '0 -1540px',
    },
    spn: {
      backgroundPosition: '0 -1575px',
    },
    nrm: {
      backgroundPosition: '0 -1610px',
    },
    target: {
      backgroundPosition: '0 -1645px',
    },
    yandex: {
      backgroundPosition: '0 -1680px',
    },
    claro: {
      backgroundPosition: '0 -1715px',
    },
    ytm: {
      backgroundPosition: '0 -1750px',
    },
    playme: {
      backgroundPosition: '0 -1785px',
    },
    zvooq: {
      backgroundPosition: '0 -1820px',
    },
    saavn: {
      backgroundPosition: '0 -1855px',
    },
    '8track': {
      backgroundPosition: '0 -1890px',
    },
    nmusic: {
      backgroundPosition: '0 -1925px',
    },
    'apple-music': {
      backgroundPosition: '0 -1960px',
    },
    qsic: {
      backgroundPosition: '0 -1995px',
    },
    cur: {
      backgroundPosition: '0 -2030px',
    },
    mload: {
      backgroundPosition: '0 -2065px',
    },
    dmusic: {
      backgroundPosition: '0 -2100px',
    },
    pndr: {
      backgroundPosition: '0 -2135px',
    },
    tnct: {
      backgroundPosition: '0 -2310px',
    },
    boom: {
      backgroundPosition: '0 -2170px',
    },
    ttunes: {
      backgroundPosition: '0 -2240px',
    },
    island: {
      backgroundPosition: '0 -2275px',
    },
    byda: {
      backgroundPosition: '0 -2345px',
    },
    uma: {
      backgroundPosition: '0 -2380px',
    },
    net: {
      backgroundPosition: '0 -2450px',
    },
    'go-yt': {
      backgroundPosition: '0 -2485px',
    },
    joox: {
      backgroundPosition: '0 -2520px',
    },
    tim: {
      backgroundPosition: '0 -2555px',
    },
    hung: {
      backgroundPosition: '0 -2590px',
    },
    zed: {
      backgroundPosition: '0 -2625px',
    },
    gaana: {
      backgroundPosition: '0 -2660px',
    },
    wkhg: {
      backgroundPosition: '0 -2730px',
    },
    mtn: {
      backgroundPosition: '0 -2765px',
    },
    qob: {
      backgroundPosition: '0 -2800px',
    },
    scld: {
      backgroundPosition: '0 -2835px',
    },
    reels: {
      backgroundPosition: '0 -2870px',
    },
    us: {
      backgroundPosition: '0 -420px',
    },
  },
}
