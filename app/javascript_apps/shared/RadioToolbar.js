import React from 'react'
import PropTypes from 'prop-types'
import ErrorMessage from './ErrorMessage'

class RadioToolbar extends React.Component {
  static propTypes = {
    handleChange: PropTypes.func,
    onAttributeUpdate: PropTypes.func,
    name: PropTypes.string.isRequired,
    klassName: PropTypes.string,
  }

  onRadioChange = (e) => {
    const { handleChange, onAttributeUpdate } = this.props
    const { name, value } = e.target

    const booleanValue = value == 'Yes'
    const payload = { target: { name: name, value: booleanValue } }

    handleChange ? handleChange(payload) : onAttributeUpdate(payload)
  }

  render() {
    const {
      name,
      value,
      prompt,
      yesText,
      noText,
      klassName,
      error,
    } = this.props

    return (
      <div className={`row ${klassName}`}>
        {prompt && (
          <div className={`${klassName} ${klassName}-prompt`}>
            <b>{prompt}</b>
          </div>
        )}
        <div className={`${klassName} radio-toolbar`}>
          <input
            id={`${klassName}-yes`}
            type="radio"
            name={name}
            value="Yes"
            checked={value == true}
            onChange={this.onRadioChange}
          />
          <label className={klassName} htmlFor={`${klassName}-yes`}>
            {yesText || 'Yes'}
          </label>
          <input
            id={`${klassName}-no`}
            type="radio"
            name={name}
            value="No"
            checked={value == false}
            onChange={this.onRadioChange}
          />
          <label className={klassName} htmlFor={`${klassName}-no`}>
            {noText || 'No'}
          </label>
        </div>
        {error && (
          <div className={`${klassName}-error`}>
            <ErrorMessage message={error} />
          </div>
        )}
      </div>
    )
  }
}

export default RadioToolbar
