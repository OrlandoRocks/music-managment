import React from 'react'
import PropTypes from 'prop-types'

export const TooltipContainer = ({ children }) => {
  const columns = children.filter(
    (child) => child.type.displayName === 'TooltipColumn'
  )
  const title = children.find(
    (child) => child.type.displayName === 'TooltipTitle'
  )

  const position = columns.length === 1 ? 'left' : 'right'
  return [
    <div
      key="tooltip-arrow"
      className={`tooltip-arrow tooltip-${position}-arrow`}
    />,
    <div key="tooltip" className={`tooltip tooltip-${position}`}>
      {title}
      {columns}
    </div>,
  ]
}

export const TooltipTitle = ({ children }) => {
  return <div className="tooltip-title">{children}</div>
}

TooltipTitle.displayName = 'TooltipTitle'

export const TooltipColumn = ({ children }) => {
  return <div className="tooltip-column">{children}</div>
}

TooltipColumn.displayName = 'TooltipColumn'

export class Tooltip extends React.Component {
  static propTypes = {
    displayText: PropTypes.string,
    iconClass: PropTypes.string,
  }

  state = {
    showContent: false,
  }

  buildDisplay = (hasLink, firstChild) => {
    const { displayText, iconClass } = this.props
    const iconSpan = <span className={iconClass || 'tooltip-info-icon'}></span>

    if (hasLink) {
      return firstChild
    }
    if (displayText) {
      return (
        <span>
          {displayText} {iconSpan}
        </span>
      )
    }

    return iconSpan
  }

  hideTooltip = () => {
    this.timer = setTimeout(() => {
      this.setState({ showContent: false })
    }, 500)
  }

  showTooltip = () => {
    this.timer && window.clearTimeout(this.timer)
    this.setState({ showContent: true })
  }

  render() {
    const { children } = this.props
    const [firstChild, ...rest] = Object.entries(children)

    const hasLink = firstChild && firstChild.type === 'a'
    const className = `tooltip-container${hasLink ? '-link' : ''}`
    const displayElement = this.buildDisplay(hasLink, firstChild)
    const element = document.getElementsByClassName(
      displayElement.props.className
    )[0]
    const shownElements = hasLink ? rest : children
    let innerStyle = {}
    if (hasLink && element) {
      innerStyle = {
        position: 'relative',
        right: element.offsetWidth + 8,
      }
    }

    return (
      <div
        className={className}
        onMouseEnter={this.showTooltip}
        onMouseLeave={this.hideTooltip}
      >
        {displayElement}
        {this.state.showContent && (
          <div style={innerStyle}>{shownElements}</div>
        )}
      </div>
    )
  }
}
