import React from 'react'
import PropTypes from 'prop-types'

const errorMessageContainerStyle = {
  marginTop: '30px',
}

const errorMessageTextStyle = {
  backgroundColor: '#D0021B',
  color: '#fff',
  padding: '10px',
}

const ErrorMessage = ({
  message,
  containerStyle,
  textStyle,
  noScroll = false,
}) => {
  const classString = noScroll
    ? 'error-message-text'
    : 'error-message-text missing'

  return (
    <div
      className="error-message-container"
      style={containerStyle || errorMessageContainerStyle}
    >
      <span
        className={classString}
        style={textStyle || errorMessageTextStyle}
        data-testid={`${message}-error-message`}
      >
        {message}
      </span>
    </div>
  )
}

ErrorMessage.propTypes = {
  message: PropTypes.string.isRequired,
}

export default React.memo(ErrorMessage)
