import moment from 'moment'
import normalizeStr from '../utils/normalizeStr'
import { LATIN_WITH_ACCENT_AND_SPECIAL_CHARS } from '../utils/regexLibrary'

/* eslint-disable no-useless-escape */
export function containsInvalidCharacters(field) {
  return (
    field !== undefined && /[\$\%\\\/\;\<\>\=\?\[\]\{\}\^\`\|\-]+/.test(field)
  )
}
/* eslint-enable no-useless-escape */

export function containsNonLatinCharacters(field) {
  var hasOnlyLatinAndAccent = LATIN_WITH_ACCENT_AND_SPECIAL_CHARS.test(field)
  return field !== undefined && !hasOnlyLatinAndAccent
}

export function isFieldPresent(field) {
  return !(field === '' || field === null || field === undefined)
}

export function isFieldMissing(field) {
  return !isFieldPresent(field)
}

export function isFieldValid(field) {
  return (
    field === '' ||
    !(containsInvalidCharacters(field) || containsNonLatinCharacters(field))
  )
}

export function isIsrcValid(isrc_number) {
  return /^[A-Z]{2}[A-Z0-9]{3}[0-9]{7}$/.test(isrc_number)
}

export function invalidTextField(textField) {
  return !isFieldValid(textField)
}

export function isrcInUse(isrcNumber, composition, compositions) {
  const filteredCompositions =
    'id' in composition
      ? compositions.filter((c) => c.id !== composition.id)
      : compositions

  return filteredCompositions.some((currentComposition) => {
    return (
      currentComposition.isrc === isrcNumber &&
      currentComposition.id !== composition.id
    )
  })
}

export function invalidIsrcField(isrcNumber) {
  return isFieldPresent(isrcNumber) && !isIsrcValid(isrcNumber)
}

export function invalidReleaseDateField(releaseDate) {
  const startDate = '1900-01-01'
  const correctformat = /^(\d){2}\/(\d){2}\/(\d){4}$/.test(releaseDate)

  return (
    isFieldPresent(releaseDate) &&
    (!correctformat || !moment(releaseDate, 'MM/DD/YYYY').isAfter(startDate))
  )
}

export function invalidPercentField(percent) {
  return (isFieldPresent(percent) && percent < 1) || percent > 100
}

export function invalidTitle(title, composition, compositions) {
  const filteredCompositions =
    'id' in composition
      ? compositions.filter((c) => c.id !== composition.id)
      : compositions
  const normalizedCompositionTitles = filteredCompositions.map((c) => {
    const compositionTitle = c.translated_name
      ? c.translated_name
      : c.composition_title
    return normalizeStr(compositionTitle).toLowerCase()
  })

  let newTitle = normalizeStr(title).toLowerCase()
  let duplicateTitle = normalizedCompositionTitles.includes(newTitle)

  if (invalidTextField(title)) {
    return 'title'
  } else if (duplicateTitle) {
    return 'duplicate_title'
  } else if (isFieldMissing(title)) {
    return 'missing_title'
  } else {
    return ''
  }
}

export function invalidPerformingArtist(artist) {
  if (invalidTextField(artist)) {
    return 'performing_artist'
  } else if (isFieldMissing(artist)) {
    return 'missing_performing_artist'
  } else {
    return ''
  }
}
