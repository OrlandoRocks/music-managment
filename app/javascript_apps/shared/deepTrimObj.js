export const deepTrimObj = (obj) => {
  return JSON.parse(JSON.stringify(obj).replace(/"\s+|\s+"/g, '"'))
}
