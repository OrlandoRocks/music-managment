export default function sum(arr, attribute) {
  return arr.reduce(
    (acc, object) => parseFloat(acc) + parseFloat(object[attribute] || 0),
    0
  )
}
