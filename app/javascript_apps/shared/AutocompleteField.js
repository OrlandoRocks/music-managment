import React from 'react'
import PropTypes from 'prop-types'
import Autocomplete from 'react-autocomplete'
import escapeStringRegexp from 'escape-string-regexp'

const menuStyle = {
  borderRadius: '3px',
  boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
  background: 'rgba(255, 255, 255, 0.9)',
  padding: '2px 0',
  fontSize: '14px',
  overflow: 'auto',
  width: '50%',
  textAlign: 'left',
  zIndex: '998',
  position: 'absolute',
  maxHeight: '155px',
}

const renderMenu = (items) => {
  return (
    <div className="autocomplete-dropdown-menu" style={{ ...menuStyle }}>
      {items}
    </div>
  )
}

class AutocompleteField extends React.Component {
  static propTypes = {
    ariaLabel: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    items: PropTypes.array,
    wrapperStyle: PropTypes.object,
    inputStyle: PropTypes.object,
  }

  handleChange = (e) => {
    this.props.onChange(e)
  }

  handleSelect = (name, value) => {
    this.props.onChange({ target: { name, value } })
  }

  getItemValueForAutocomplete = (value) => {
    return value
  }

  renderItem = (value, isHighlighted) => {
    const style = {
      background: isHighlighted ? 'lightgray' : 'white',
      padding: '5px',
    }

    return (
      <div key={value} style={style}>
        {value}
      </div>
    )
  }

  shouldDisplayItem = (initialValue) => {
    const inputValue = escapeStringRegexp(this.props.value.toLowerCase())
    return (initialValue || '').toLowerCase().match(inputValue)
  }

  render() {
    const {
      ariaLabel,
      type,
      name,
      className,
      value,
      items,
      wrapperStyle = {},
    } = this.props

    return (
      <Autocomplete
        wrapperStyle={wrapperStyle}
        menuStyle={menuStyle}
        items={items}
        getItemValue={this.getItemValueForAutocomplete}
        onChange={this.handleChange}
        onSelect={(newValue) => this.handleSelect(name, newValue)}
        renderItem={this.renderItem}
        value={value}
        shouldItemRender={this.shouldDisplayItem}
        renderMenu={renderMenu}
        inputProps={{
          'aria-label': ariaLabel ? ariaLabel : null,
          autoComplete: 'off',
          className: className,
          name: name,
          type: type,
        }}
      />
    )
  }
}

export default AutocompleteField
