import React from 'react'

function setTransform({ isBack, isOpen }) {
  if (isBack) return 'rotate(90)'
  if (isOpen) return 'rotate(180)'
}

export default function ArrowIcon(props) {
  const transform = setTransform(props)

  return (
    <svg
      viewBox="0 0 20 20"
      preserveAspectRatio="none"
      width={16}
      fill="transparent"
      stroke="#979797"
      strokeWidth="1.1px"
      transform={transform}
    >
      <path d="M1,6 L10,15 L19,6" />
    </svg>
  )
}
