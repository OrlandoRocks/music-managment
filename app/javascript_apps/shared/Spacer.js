import React from 'react'

export default function Spacer({ height = 200 }) {
  return <div style={{ height }} />
}
