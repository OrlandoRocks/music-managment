var path = require('path')
var webpack = require('webpack')
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

const writeBundlesReport = process.env.BUNDLES_REPORT === 'true'

// Code-splitting not possible with current build process.
// See comments on https://tunecore.atlassian.net/browse/SC-560
var config = {
  entry: {
    album: './album_app/app.js',
    compositions: './compositions/app.js',
    distribution_progress_bar: './distribution_progress_bar/app',
    language_selector: './language_selector/app.js',
    popup_modal: './popup_modal/app.js',
    songs: './songs/app.js',
    split_shares_modal: './split_shares_modal/app.js',
    top_nav_bar: './top_nav_bar/app.js',
    vatTooltip: './vat_tooltip/app.js',
  },
  output: {
    path: path.resolve(__dirname, '../../app/assets/javascripts/apps/'),
    filename: './[name].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
    ],
  },
  performance: {
    hints: false,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.WEBPACK_DEV_SERVER': JSON.stringify(
        process.env.WEBPACK_DEV_SERVER
      ),
    }),
    // The local/dev run-time bundleReport profiles the dev bundles.
    // Use 'yarn bundlesReport' to profile the production bundles.
    new BundleAnalyzerPlugin({
      analyzerMode: writeBundlesReport ? 'static' : 'disabled',
      openAnalyzer: false,
      reportFilename: '../../../javascript_apps/bundlesReport.html',
    }),
  ],
  resolve: {
    alias: {
      'lodash-es': 'lodash',
    },
  },
  watchOptions: {
    ignored: [path.resolve(__dirname, 'node_modules')],
    poll: 2000,
  },
}

if (process.env.RAILS_ENV === 'production') {
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        RAILS_ENV: JSON.stringify(process.env.RAILS_ENV),
      },
    })
  )
} else {
  config.mode = 'development'
  config.devtool = 'inline-source-map'
}

module.exports = config
