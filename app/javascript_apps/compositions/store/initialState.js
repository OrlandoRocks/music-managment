import composer from './initialStates/composer'
import compositions from './initialStates/compositions'
import cowriters from './initialStates/cowriters'
import currentComposer from './initialStates/currentComposer'
import currentComposition from './initialStates/currentComposition'
import ntcComposition from './initialStates/ntcComposition'
import pros from './initialStates/pros'

const initialState = {
  composer,
  compositions,
  cowriters,
  currentComposer,
  currentComposition,
  ntcComposition,
  pros,
}

export default initialState
