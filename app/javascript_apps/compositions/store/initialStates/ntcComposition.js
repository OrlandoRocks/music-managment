import { createNtcComposition } from '../../utils/createCompositionData'

let ntcComposition = createNtcComposition()

export default ntcComposition
