import findNextEligibleComposition from '../../utils/findNextEligibleComposition'

let currentComposition = {}
let compositions = JSON.parse(
  document.getElementById('compositions_app').dataset.compositions
)

if (compositions.length > 1) {
  let startFromLastComposition = compositions[compositions.length - 1]
  currentComposition = findNextEligibleComposition(
    compositions,
    startFromLastComposition.id
  )
}

export default currentComposition
