import { normalizeCowriters } from '../schema'

let dbCowriters = JSON.parse(
  document.getElementById('compositions_app').dataset.cowriters
)

const {
  entities: { cowriters },
} = normalizeCowriters(dbCowriters)

export default cowriters
