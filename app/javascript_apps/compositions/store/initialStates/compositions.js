import { normalizeCompositions } from '../schema'

let dbCompositions = JSON.parse(
  document.getElementById('compositions_app').dataset.compositions
)

const {
  entities: { compositions },
} = normalizeCompositions(dbCompositions)

export default compositions
