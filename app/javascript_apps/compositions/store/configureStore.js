import { createStore, applyMiddleware, compose } from 'redux'

import initialState from './initialState'
import rootReducer from '../reducers'
import apiMiddleware from '../middlewares/apiMiddleware'
import updateCowriterValidationMiddleware from '../middlewares/updateCowriterValidationMiddleware'
import cowritersValidationMiddleware from '../middlewares/cowritersValidationMiddleware'
import compositionValidationMiddleware from '../middlewares/compositionValidationMiddleware'
import translatedNameValidationMiddleware from '../middlewares/translatedNameValidationMiddleware'
import ntcCompositionValidationMiddleware from '../middlewares/ntcCompositionValidationMiddleware'
import ntcCowriterValidationMiddleware from '../middlewares/ntcCowriterValidationMiddleware'
import submitNtcCompositionMiddleware from '../middlewares/submitNtcCompositionMiddleware'
import updateSplitsValidationMiddleware from '../middlewares/updateSplitsValidationMiddleware'

let composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(
        updateCowriterValidationMiddleware,
        cowritersValidationMiddleware,
        translatedNameValidationMiddleware,
        compositionValidationMiddleware,
        ntcCompositionValidationMiddleware,
        ntcCowriterValidationMiddleware,
        submitNtcCompositionMiddleware,
        updateSplitsValidationMiddleware,
        apiMiddleware
      )
    )
  )
}
