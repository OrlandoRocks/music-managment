import { CREATE_SPLITS_SUCCESS } from '../actions/actionTypes'
import { updateCowriterErrors } from '../actions/cowriterActions'
import findInvalidCowriters from '../utils/findInvalidCowriters'

const cowritersValidationMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (!(action.payload && action.payload.success === CREATE_SPLITS_SUCCESS)) {
    return next(action)
  }

  const { compositions, cowriters } = getState()
  const { composition_id } = action.payload.params
  const composition = compositions[composition_id]

  let compositionCowriters = Object.values(cowriters).filter((cowriter) => {
    return composition.cowriters.includes(cowriter.uuid)
  })

  let invalidCowriters = findInvalidCowriters(compositionCowriters)

  if (invalidCowriters.length === 0) {
    next(action)
  } else {
    invalidCowriters.map((cowriter) => {
      dispatch(updateCowriterErrors(cowriter.uuid, cowriter.errors))
    })
  }
}

export default cowritersValidationMiddleware
