import { UPDATE_COWRITER } from '../actions/actionTypes'
import { updateCowriterErrors } from '../actions/cowriterActions'
import { updateCompositionErrors } from '../actions/compositionActions'
import updateCowriterShare from '../utils/updateCowriterShare'
import clearCowriterShareErrors from '../utils/clearCowriterShareErrors'
import sum from '../../shared/sum'

const updateCowriterValidationMiddleware = ({ dispatch, getState }) => (
  next
) => (action) => {
  if (action.type !== UPDATE_COWRITER) {
    return next(action)
  }

  const { compositions, cowriters } = getState()
  const cowriter = cowriters[action.uuid]
  const composition = compositions[action.compositionId]
  let invalidSharePercentage
  let invalidTotalPercentage

  // Clear server validation errors
  composition.errors.cowriterErrors = null

  if (action.attribute === 'cowriter_share') {
    let compositionCowriters = updateCowriterShare(
      composition,
      cowriters,
      action.uuid,
      action.value
    )
    let totalCowriterSharesPercentage = sum(
      Object.values(compositionCowriters),
      'cowriter_share'
    )

    let totalSplitPercentage =
      parseFloat(composition.composer_share) + totalCowriterSharesPercentage
    invalidSharePercentage = action.value < 1 || action.value > 100
    invalidTotalPercentage =
      totalSplitPercentage < 1 || totalSplitPercentage > 100

    if (!invalidSharePercentage) {
      clearCowriterShareErrors(dispatch, compositionCowriters, cowriter.uuid)
    }

    if (invalidTotalPercentage) {
      dispatch(
        updateCompositionErrors(composition.id, { total_shares: 'percent' })
      )
    }

    if (
      composition.errors &&
      (composition.errors.composer_share || composition.errors.total_shares) &&
      !invalidTotalPercentage
    ) {
      dispatch(
        updateCompositionErrors(composition.id, {
          composer_share: '',
          total_shares: '',
        })
      )
    }
  }

  let invalidCowriterShare =
    action.attribute === 'cowriter_share' && invalidSharePercentage
  cowriter.errors = {
    ...cowriter.errors,
    [action.attribute]: action.value.length === 0 || invalidCowriterShare,
  }

  let allFieldsAreInvalid = Object.values(cowriter.errors).every(
    (value) => value === true
  )
  let cowriterShareIsBlank =
    action.attribute === 'cowriter_share' && action.value === ''
  if (allFieldsAreInvalid && cowriterShareIsBlank) {
    Object.keys(cowriter.errors).forEach((key) => (cowriter.errors[key] = true))
  }

  dispatch(updateCowriterErrors(cowriter.uuid, cowriter.errors))

  next(action)
}

export default updateCowriterValidationMiddleware
