import { updateCowriterErrors } from '../actions/cowriterActions'
import {
  invalidPercentField,
  isFieldMissing,
} from '../../shared/fieldValidations'
import { hasCowriterErrors } from '../utils/ntcCompositionValidations'
import nonEmptyCowriters from '../utils/nonEmptyCowriters'
import nonEmptyCowriter from '../utils/nonEmptyCowriter'
import { API, UPDATE_SPLITS_SUCCESS } from '../actions/actionTypes'

const updateSplitsValidationMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (
    !(action.type === API && action.payload.success === UPDATE_SPLITS_SUCCESS)
  ) {
    return next(action)
  }

  const { cowriter_params } = action.payload.params

  cowriter_params.map((cowriter) => {
    let cowriterErrors
    let isInvalidField

    if (nonEmptyCowriter(cowriter)) {
      cowriterErrors = Object.keys(cowriter)
        .filter((attribute) => attribute !== 'errors')
        .reduce((acc, attribute) => {
          if (attribute === 'cowriter_share') {
            isInvalidField =
              isFieldMissing(cowriter[attribute]) ||
              invalidPercentField(action.value)
          } else {
            isInvalidField = isFieldMissing(cowriter[attribute])
          }
          return { ...acc, [attribute]: isInvalidField }
        }, {})
      dispatch(updateCowriterErrors(cowriter.uuid, cowriterErrors))
    }
  })

  const updatedCowriters = getState().cowriters

  if (!hasCowriterErrors(updatedCowriters)) {
    // Update the params to only send non empty cowriters to the backend
    let updatedPayload = {
      ...action.payload,
      params: {
        ...action.payload.params,
        cowriter_params: nonEmptyCowriters(cowriter_params),
      },
    }

    return next({
      ...action,
      payload: updatedPayload,
    })
  }
}

export default updateSplitsValidationMiddleware
