import { UPDATE_NTC_COWRITER } from '../actions/actionTypes'
import { updateNtcCowriterErrors } from '../actions/ntcCowriterActions'
import { updateNtcCompositionErrors } from '../actions/ntcCompositionActions'
import {
  isFieldMissing,
  invalidPercentField,
} from '../../shared/fieldValidations'
import { resetCowriterShareError } from '../utils/ntcCompositionValidations'
import updateNtcCowriterShare from '../utils/updateNtcCowriterShare'
import sum from '../../shared/sum'

const ntcCowriterValidationMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (action.type !== UPDATE_NTC_COWRITER) {
    return next(action)
  }

  const ntcComposition = getState().ntcComposition
  const cowriters = ntcComposition.cowriters
  const currentCowriter = cowriters[action.ntcCowriterIndex]
  const isNowEmpty =
    action.value === '' && currentCowriter[action.attribute] !== ''
  let isInvalidField

  if (action.attribute === 'cowriter_share') {
    let updatedNtcCowriters = updateNtcCowriterShare(
      cowriters,
      action.ntcCowriterIndex,
      action.value
    )
    let totalCowriterSharesPercentage = sum(
      Object.values(updatedNtcCowriters),
      'cowriter_share'
    )
    let totalSplitPercentage =
      parseFloat(ntcComposition.percent) + totalCowriterSharesPercentage

    if (totalSplitPercentage >= 1 && totalSplitPercentage <= 100) {
      resetCowriterShareError(cowriters, dispatch)
      dispatch(updateNtcCompositionErrors('percent', ''))
    }

    isInvalidField =
      invalidPercentField(action.value) ||
      totalSplitPercentage < 1 ||
      totalSplitPercentage > 100
  } else {
    isInvalidField = isFieldMissing(action.value)
  }

  let updatedErrors = {
    ...currentCowriter.errors,
    [action.attribute]: !isNowEmpty && isInvalidField,
  }
  dispatch(updateNtcCowriterErrors(updatedErrors, action.ntcCowriterIndex))

  next(action)
}

export default ntcCowriterValidationMiddleware
