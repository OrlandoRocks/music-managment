import { UPDATE_COMPOSITION } from '../actions/actionTypes'
import { updateCompositionErrors } from '../actions/compositionActions'
import sum from '../../shared/sum'
import {
  invalidIsrcField,
  isrcInUse,
  invalidReleaseDateField,
  invalidTitle,
  invalidPerformingArtist,
} from '../../shared/fieldValidations'

const compositionValidationMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  // TODO refactor this whole action dispatch system
  if (action.type !== UPDATE_COMPOSITION) {
    return next(action)
  }

  const { compositions, cowriters } = getState()
  const composition = compositions[action.compositionId]
  let invalidPercentage =
    !composition.errors || composition.errors.composer_share === 'percent'
  let invalidTotalPercentage =
    !composition.errors || composition.errors.total_shares === 'percent'
  let compositionCowriters = Object.values(cowriters).filter((cowriter) => {
    return composition.cowriters.includes(cowriter.uuid)
  })
  let errors = {}

  /* eslint-disable no-case-declarations */
  switch (action.attribute) {
    case 'composition_title':
      errors = {
        composition_title: invalidTitle(
          action.value,
          composition,
          Object.values(compositions)
        ),
      }
      break
    case 'performing_artist':
      errors = { performing_artist: invalidPerformingArtist(action.value) }
      break
    case 'release_date':
      errors = {
        release_date: invalidReleaseDateField(action.value)
          ? 'release_date'
          : '',
      }
      break
    case 'composer_share':
      const totalCowriterSharesPercentage = sum(
        compositionCowriters,
        'cowriter_share'
      )
      const totalSplitPercentage =
        parseFloat(action.value || 0) +
        parseFloat(totalCowriterSharesPercentage || 0)
      invalidPercentage = action.value < 1 || action.value > 100
      invalidTotalPercentage =
        totalSplitPercentage < 1 || totalSplitPercentage > 100

      errors = {
        composer_share: invalidPercentage ? 'percent' : '',
        total_shares: invalidTotalPercentage ? 'percent' : '',
      }
      break
    case 'isrc':
      const invalidIsrcError = invalidIsrcField(action.value)
        ? 'isrc_number'
        : ''
      const duplicateIsrcError = isrcInUse(
        action.value,
        composition,
        Object.values(compositions)
      )
        ? 'isrc_in_use'
        : ''

      errors = {
        isrc: invalidIsrcError !== '' ? invalidIsrcError : duplicateIsrcError,
      }
      break
  }
  /* eslint-enable no-case-declarations */

  dispatch(updateCompositionErrors(composition.id, errors))

  next(action)
}

export default compositionValidationMiddleware
