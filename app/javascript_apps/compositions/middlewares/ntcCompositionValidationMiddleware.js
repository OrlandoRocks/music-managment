import { UPDATE_NTC_COMPOSITION } from '../actions/actionTypes'
import { updateNtcCompositionErrors } from '../actions/ntcCompositionActions'
import updateNtcCowriterShare from '../utils/updateNtcCowriterShare'
import sum from '../../shared/sum'
import { snakeCase } from 'lodash'
import {
  invalidTitle,
  invalidTextField,
  invalidIsrcField,
  isrcInUse,
  invalidReleaseDateField,
  invalidPercentField,
  invalidPerformingArtist,
} from '../../shared/fieldValidations'

const validators = {
  title: invalidTitle,
  performingArtist: invalidPerformingArtist,
  albumName: invalidTextField,
  recordLabel: invalidTextField,
  isrcNumber: invalidIsrcField,
  releaseDate: invalidReleaseDateField,
  publicDomain: () => false,
}

const ntcCompositionValidationMiddleware = ({ dispatch, getState }) => (
  next
) => (action) => {
  if (action.type !== UPDATE_NTC_COMPOSITION) {
    return next(action)
  }

  const { compositions, ntcComposition } = getState()
  let errorKey = ''

  let resetIsrcNumbererror =
    action.attribute === 'isrcNumber' &&
    ntcComposition.errors['isrcNumber'] === 'isrc_in_use'
  if (resetIsrcNumbererror) {
    dispatch(updateNtcCompositionErrors('isrcNumber', ''))
  }

  /* eslint-disable no-case-declarations */

  switch (action.attribute) {
    case 'title':
      const compositionTitles = Object.values(compositions)
      errorKey = invalidTitle(action.value, ntcComposition, compositionTitles)
      break
    case 'performingArtist':
      errorKey = invalidPerformingArtist(action.value)
      break
    case 'percent':
      const percent = action.value
      const cowriters = ntcComposition.cowriters
      let updatedNtcCowriters = updateNtcCowriterShare(
        cowriters,
        action.ntcCowriterIndex,
        percent
      )
      let totalSplitPercentage =
        sum(Object.values(updatedNtcCowriters), 'cowriter_share') +
        parseFloat(percent)

      if (
        invalidPercentField(percent) ||
        totalSplitPercentage < 1 ||
        totalSplitPercentage > 100
      ) {
        errorKey = 'percent'
      }
      break
    case 'isrcNumber':
      if (invalidIsrcField(action.value)) {
        errorKey = 'isrc_number'
      } else if (
        isrcInUse(action.value, ntcComposition, Object.values(compositions))
      ) {
        errorKey = 'isrc_in_use'
      }
      break
    default:
      let isInvalidField = validators[action.attribute](action.value)
      if (isInvalidField) errorKey = snakeCase(action.attribute)
      break
  }
  /* eslint-enable no-case-declarations */

  dispatch(updateNtcCompositionErrors(action.attribute, errorKey))

  next(action)
}

export default ntcCompositionValidationMiddleware
