import { UPDATE_TRANSLATED_NAME } from '../actions/actionTypes'
import { updateCompositionErrors } from '../actions/compositionActions'
import {
  containsNonLatinCharacters,
  containsInvalidCharacters,
} from '../../shared/fieldValidations'

const translatedNameValidationMiddleware = ({ dispatch, getState }) => (
  next
) => (action) => {
  if (action.type !== UPDATE_TRANSLATED_NAME) {
    return next(action)
  }

  const composition = getState().compositions[action.compositionId]

  let invalidTranslatedName =
    containsNonLatinCharacters(action.value) ||
    containsInvalidCharacters(action.value)

  dispatch(
    updateCompositionErrors(composition.id, {
      invalidTranslatedName: invalidTranslatedName,
    })
  )

  next(action)
}

export default translatedNameValidationMiddleware
