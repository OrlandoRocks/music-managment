import {
  API,
  CREATE_SPLITS_SUCCESS,
  CREATE_SPLITS_ERRORS,
  UPDATE_SPLITS_SUCCESS,
  UPDATE_SPLITS_ERRORS,
} from './actionTypes'

export function createSplits(composition, composer, cowriters) {
  return {
    type: API,
    payload: {
      url: '/api/backstage/publishing_administration/publishing_splits',
      method: 'POST',
      params: {
        composer_id: composer.id,
        composer_share: composition.composer_share,
        composition_id: composition.id,
        public_domain: composition.public_domain,
        cowriter_params: cowriters,
        translated_name: composition.translated_name,
      },
      success: CREATE_SPLITS_SUCCESS,
      error: CREATE_SPLITS_ERRORS,
    },
  }
}

export function updateSplits(composition, cowriters) {
  return {
    type: API,
    payload: {
      url: `/api/backstage/publishing_administration/compositions/${composition.id}/publishing_splits`,
      method: 'PUT',
      params: {
        composition_params: composition,
        cowriter_params: cowriters,
      },
      success: UPDATE_SPLITS_SUCCESS,
      error: UPDATE_SPLITS_ERRORS,
    },
  }
}
