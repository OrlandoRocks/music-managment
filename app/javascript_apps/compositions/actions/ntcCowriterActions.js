import {
  ADD_NTC_COWRITER,
  REMOVE_NTC_COWRITER,
  UPDATE_NTC_COWRITER,
  UPDATE_NTC_COWRITER_ERRORS,
} from './actionTypes'

export function addNtcCowriter() {
  return {
    type: ADD_NTC_COWRITER,
  }
}

export function updateNtcCowriter(attribute, value, ntcCowriterIndex) {
  return {
    type: UPDATE_NTC_COWRITER,
    attribute,
    value,
    ntcCowriterIndex,
  }
}

export function updateNtcCowriterErrors(errors, ntcCowriterIndex) {
  return {
    type: UPDATE_NTC_COWRITER_ERRORS,
    errors,
    ntcCowriterIndex,
  }
}

export function removeNtcCowriter(ntcCowriterIndex) {
  return {
    type: REMOVE_NTC_COWRITER,
    ntcCowriterIndex,
  }
}
