export const ADD_COWRITER = 'ADD_COWRITER'
export const ADD_NTC_COMPOSITION_ERRORS = 'ADD_NTC_COMPOSITION_ERRORS'
export const ADD_NTC_COMPOSITION_SUCCESS = 'ADD_NTC_COMPOSITION_SUCCESS'
export const ADD_NTC_COWRITER = 'ADD_NTC_COWRITER'
export const API = 'API'
export const CREATE_SPLITS_SUCCESS = 'CREATE_SPLITS_SUCCESS'
export const CREATE_SPLITS_ERRORS = 'CREATE_SPLITS_ERRORS'
export const NEXT_COMPOSITION = 'NEXT_COMPOSITION'
export const REMOVE_COWRITER = 'REMOVE_COWRITER'
export const REMOVE_NTC_COWRITER = 'REMOVE_NTC_COWRITER'
export const REPLACE_COMPOSITION = 'REPLACE_COMPOSITION'
export const REPLACE_COWRITERS = 'REPLACE_COWRITERS'
export const RESET_COMPOSER = 'RESET_COMPOSER'
export const RESET_COMPOSITION = 'RESET_COMPOSITION'
export const RESET_NTC_COMPOSITION = 'RESET_NTC_COMPOSITION'
export const SAVE_COMPOSER = 'SAVE_COMPOSER'
export const SET_CURRENT_COMPOSITION = 'SET_CURRENT_COMPOSITION'
export const UPDATE_COMPOSER = 'UPDATE_COMPOSER'
export const UPDATE_COMPOSITION = 'UPDATE_COMPOSITION'
export const UPDATE_COMPOSITION_ERRORS = 'UPDATE_COMPOSITION_ERRORS'
export const UPDATE_COMPOSITION_SUCCESS = 'UPDATE_COMPOSITION_SUCCESS'
export const UPDATE_COWRITER = 'UPDATE_COWRITER'
export const UPDATE_COWRITER_ERRORS = 'UPDATE_COWRITER_ERRORS'
export const UPDATE_NTC_COMPOSITION = 'UPDATE_NTC_COMPOSITION'
export const UPDATE_NTC_COMPOSITION_ERRORS = 'UPDATE_NTC_COMPOSITION_ERRORS'
export const UPDATE_NTC_COWRITER = 'UPDATE_NTC_COWRITER'
export const UPDATE_NTC_COWRITER_ERRORS = 'UPDATE_NTC_COWRITER_ERRORS'
export const UPDATE_SPLITS_SUCCESS = 'UPDATE_SPLITS_SUCCESS'
export const UPDATE_SPLITS_ERRORS = 'UPDATE_SPLITS_ERRORS'
export const UPDATE_TRANSLATED_NAME = 'UPDATE_TRANSLATED_NAME'
export const UPDATE_COMPOSITION_CHANGED = 'UPDATE_COMPOSITION_CHANGED'
export const UPDATE_COWRITER_CHANGED = 'UPDATE_COWRITER_CHANGED'
