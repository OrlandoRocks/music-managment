import {
  ADD_NTC_COMPOSITION_ERRORS,
  ADD_NTC_COMPOSITION_SUCCESS,
  API,
  RESET_NTC_COMPOSITION,
  UPDATE_NTC_COMPOSITION,
  UPDATE_NTC_COMPOSITION_ERRORS,
} from './actionTypes'

export function submitNtcComposition(data, composerId) {
  return {
    type: API,
    payload: {
      url: `/api/backstage/publishing_administration/composers/${composerId}/non_tunecore_compositions`,
      method: 'POST',
      params: {
        title: data.title,
        artist_name: data.performingArtist,
        album_name: data.albumName,
        record_label: data.recordLabel,
        isrc_number: data.isrcNumber,
        release_date: data.releaseDate,
        percent: data.percent,
        cowriter_params: data.cowriters,
        public_domain: data.publicDomain,
      },
      success: ADD_NTC_COMPOSITION_SUCCESS,
      error: ADD_NTC_COMPOSITION_ERRORS,
    },
  }
}

export function updateNtcComposition(attribute, value) {
  return {
    type: UPDATE_NTC_COMPOSITION,
    attribute,
    value,
  }
}

export function updateNtcCompositionErrors(attribute, value) {
  return {
    type: UPDATE_NTC_COMPOSITION_ERRORS,
    attribute,
    value,
  }
}

export function resetNtcComposition() {
  return {
    type: RESET_NTC_COMPOSITION,
  }
}
