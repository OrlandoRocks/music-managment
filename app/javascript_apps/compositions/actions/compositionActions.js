import {
  API,
  NEXT_COMPOSITION,
  REPLACE_COMPOSITION,
  SET_CURRENT_COMPOSITION,
  UPDATE_COMPOSITION,
  UPDATE_COMPOSITION_ERRORS,
  UPDATE_COMPOSITION_SUCCESS,
  UPDATE_TRANSLATED_NAME,
  UPDATE_COMPOSITION_CHANGED,
} from './actionTypes'

export function nextComposition(compositions, composition) {
  return {
    type: NEXT_COMPOSITION,
    compositions,
    composition,
  }
}

export function setCurrentComposition(composition) {
  return {
    type: SET_CURRENT_COMPOSITION,
    composition,
  }
}

export function submitCompositionState(composition) {
  return {
    type: API,
    payload: {
      url: `/api/backstage/publishing_administration/compositions_hidden/${composition.id}`,
      method: 'PUT',
      success: UPDATE_COMPOSITION_SUCCESS,
    },
  }
}

export function updateComposition(compositionId, attribute, value) {
  return {
    type: UPDATE_COMPOSITION,
    compositionId,
    attribute,
    value,
  }
}

export function updateCompositionErrors(compositionId, errors) {
  return {
    type: UPDATE_COMPOSITION_ERRORS,
    compositionId,
    errors,
  }
}

export function updateTranslatedName(compositionId, attribute, value) {
  return {
    type: UPDATE_TRANSLATED_NAME,
    compositionId,
    attribute,
    value,
  }
}

export function replaceComposition(composition) {
  return {
    type: REPLACE_COMPOSITION,
    composition,
  }
}

export function updateCompositionChanged(
  compositionId,
  initialCompositionState,
  attribute,
  value
) {
  return {
    type: UPDATE_COMPOSITION_CHANGED,
    compositionId,
    initialCompositionState,
    attribute,
    value,
  }
}
