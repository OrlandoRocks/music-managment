import {
  API,
  RESET_COMPOSER,
  SAVE_COMPOSER,
  UPDATE_COMPOSER,
} from './actionTypes'

export function resetComposer(data) {
  return {
    type: RESET_COMPOSER,
    data,
  }
}

export function saveComposer(composer_id, params = {}) {
  return {
    type: API,
    payload: {
      url: `/api/backstage/publishing_administration/composers/${composer_id}`,
      method: 'PUT',
      params: { composer: { ...params } },
      success: SAVE_COMPOSER,
    },
  }
}

export function updateComposer(newData) {
  return {
    type: UPDATE_COMPOSER,
    newData,
  }
}
