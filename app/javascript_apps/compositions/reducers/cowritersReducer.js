import { toNumber } from 'lodash'
import {
  ADD_COWRITER,
  ADD_NTC_COMPOSITION_SUCCESS,
  REMOVE_COWRITER,
  RESET_COMPOSITION,
  CREATE_SPLITS_SUCCESS,
  UPDATE_COWRITER,
  UPDATE_COWRITER_ERRORS,
  UPDATE_SPLITS_SUCCESS,
  REPLACE_COWRITERS,
  UPDATE_COWRITER_CHANGED,
} from '../actions/actionTypes'

function buildNewCowriters(cowriters) {
  return cowriters.reduce((acc, cowriter) => {
    cowriter.errors = {}
    return {
      ...acc,
      [cowriter.id]: cowriter,
    }
  }, {})
}

export default function cowritersReducer(state = {}, action) {
  /* eslint-disable no-case-declarations */
  switch (action.type) {
    case ADD_COWRITER:
      return {
        ...state,
        [action.cowriter.uuid]: {
          ...action.cowriter,
          errors: {
            first_name: true,
            last_name: true,
            cowriter_share: true,
          },
        },
      }
    case REMOVE_COWRITER:
      const { [action.uuid]: _omit, ...rest } = state
      return rest
    case UPDATE_COWRITER_ERRORS:
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          errors: action.errors,
        },
      }
    case RESET_COMPOSITION:
      const idsToRemove = action.composition.cowriters
      return Object.values(state)
        .filter((cowriter) => !idsToRemove.includes(cowriter.uuid))
        .reduce((accumulator, val) => {
          return {
            ...accumulator,
            [val.uuid]: val,
          }
        }, {})
    case CREATE_SPLITS_SUCCESS:
      return {
        ...state,
        ...buildNewCowriters(action.data.cowriters),
      }
    case UPDATE_COWRITER:
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          [action.attribute]: action.value,
        },
      }
    case ADD_NTC_COMPOSITION_SUCCESS:
      return {
        ...state,
        ...buildNewCowriters(action.data.cowriters),
      }
    case UPDATE_SPLITS_SUCCESS:
      return {
        ...state,
        ...buildNewCowriters(action.data.cowriters),
      }
    case REPLACE_COWRITERS:
      return {
        ...state,
        ...buildNewCowriters(Object.values(action.cowriters)),
      }
    case UPDATE_COWRITER_CHANGED:
      const changedAttribute =
        action.initialCowriterState == {} ||
        (action.attribute == 'cowriter_share'
          ? toNumber(action.value) !=
            toNumber(action.initialCowriterState[action.attribute])
          : action.initialCowriterState[action.attribute] !== action.value)
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          changed: {
            ...state[action.uuid].changed,
            [action.attribute]: changedAttribute,
          },
        },
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
