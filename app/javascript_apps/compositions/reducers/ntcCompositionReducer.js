import { createNtcComposition } from '../utils/createCompositionData'
import { createCowriter } from '../utils/createCompositionData'
import {
  ADD_NTC_COMPOSITION_ERRORS,
  ADD_NTC_COMPOSITION_SUCCESS,
  ADD_NTC_COWRITER,
  REMOVE_NTC_COWRITER,
  RESET_NTC_COMPOSITION,
  UPDATE_NTC_COMPOSITION,
  UPDATE_NTC_COMPOSITION_ERRORS,
  UPDATE_NTC_COWRITER,
  UPDATE_NTC_COWRITER_ERRORS,
} from '../actions/actionTypes'
import {
  addErrorsToCowriters,
  prepareCompositionErrors,
} from '../utils/errorHelpers'

export default function ntcComposition(state = {}, action) {
  /* eslint-disable no-case-declarations */

  switch (action.type) {
    // The .../non_tunecore_compositions endpoint error response handler actionType
    case ADD_NTC_COMPOSITION_ERRORS:
      const { errors } = action.data
      const preparedCompositionErrors = prepareCompositionErrors(errors)
      const { cowriters } = state
      const cowritersWithErrors = addErrorsToCowriters(cowriters, errors)

      return {
        ...state,
        cowriters: cowritersWithErrors,
        errors: preparedCompositionErrors,
      }
    case ADD_NTC_COMPOSITION_SUCCESS:
      return createNtcComposition()
    case ADD_NTC_COWRITER:
      return {
        ...state,
        cowriters: [...state.cowriters, createCowriter()],
      }
    case REMOVE_NTC_COWRITER:
      return {
        ...state,
        cowriters: state.cowriters.filter(
          (cowriter, index) => index !== action.ntcCowriterIndex
        ),
      }
    case RESET_NTC_COMPOSITION:
      return createNtcComposition()
    case UPDATE_NTC_COMPOSITION:
      return {
        ...state,
        [action.attribute]: action.value,
      }
    case UPDATE_NTC_COMPOSITION_ERRORS:
      return {
        ...state,
        errors: {
          ...state.errors,
          [action.attribute]: action.value,
        },
      }
    case UPDATE_NTC_COWRITER:
      return {
        ...state,
        cowriters: state.cowriters.map((cowriter, cowriterIndex) => {
          if (cowriterIndex === action.ntcCowriterIndex) {
            return { ...cowriter, [action.attribute]: action.value }
          } else {
            return cowriter
          }
        }),
      }
    case UPDATE_NTC_COWRITER_ERRORS:
      return {
        ...state,
        cowriters: state.cowriters.map((cowriter, cowriterIndex) => {
          if (cowriterIndex === action.ntcCowriterIndex) {
            return { ...cowriter, errors: action.errors }
          } else {
            return cowriter
          }
        }),
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
