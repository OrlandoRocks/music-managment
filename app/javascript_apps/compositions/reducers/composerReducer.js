import { SAVE_COMPOSER } from '../actions/actionTypes'

export default function composerReducer(state = {}, action) {
  switch (action.type) {
    case SAVE_COMPOSER:
      return { ...state, ...action.data }
    default:
      return state
  }
}
