import { combineReducers } from 'redux'
import composer from './composerReducer'
import compositions from './compositionsReducer'
import cowriters from './cowritersReducer'
import currentComposer from './currentComposerReducer'
import currentComposition from './currentCompositionReducer'
import ntcComposition from './ntcCompositionReducer'
import pros from './proReducer'

const rootReducer = combineReducers({
  composer,
  compositions,
  cowriters,
  currentComposer,
  currentComposition,
  ntcComposition,
  pros,
})

export default rootReducer
