import {
  NEXT_COMPOSITION,
  SET_CURRENT_COMPOSITION,
} from '../actions/actionTypes'
import findNextEligibleComposition from '../utils/findNextEligibleComposition'

export default function currentCompositionsReducer(state = {}, action) {
  let currentComposition

  switch (action.type) {
    case NEXT_COMPOSITION:
      currentComposition = findNextEligibleComposition(
        action.compositions,
        action.composition
      )
      return { ...state, ...currentComposition }

    case SET_CURRENT_COMPOSITION:
      currentComposition = { compositionId: action.composition.id }
      return { ...state, ...currentComposition }

    default:
      return state
  }
}
