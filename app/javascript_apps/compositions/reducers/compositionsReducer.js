import {
  ADD_COWRITER,
  ADD_NTC_COMPOSITION_SUCCESS,
  REMOVE_COWRITER,
  RESET_COMPOSITION,
  REPLACE_COMPOSITION,
  CREATE_SPLITS_ERRORS,
  CREATE_SPLITS_SUCCESS,
  UPDATE_COMPOSITION,
  UPDATE_COMPOSITION_ERRORS,
  UPDATE_COMPOSITION_SUCCESS,
  UPDATE_SPLITS_SUCCESS,
  UPDATE_SPLITS_ERRORS,
  UPDATE_TRANSLATED_NAME,
  UPDATE_COMPOSITION_CHANGED,
} from '../actions/actionTypes'
import { toNumber } from 'lodash'
import { prepareCompositionErrors } from '../utils/errorHelpers'

export default function compositionsReducer(state = {}, action) {
  /* eslint-disable no-case-declarations */

  switch (action.type) {
    case ADD_COWRITER:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          cowriters: [
            ...state[action.compositionId].cowriters,
            action.cowriter.uuid,
          ],
        },
      }
    case REMOVE_COWRITER: {
      let cowriters = state[action.compositionId].cowriters.filter(
        (cowriter) => cowriter !== action.uuid
      )

      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          cowriters,
        },
      }
    }
    case RESET_COMPOSITION:
      return {
        ...state,
        [action.composition.id]: {
          ...state[action.composition.id],
          cowriters: [],
          composer_share: '0.0',
          translated_name: '',
          errors: {},
          changed: [],
        },
      }
    case CREATE_SPLITS_SUCCESS:
      return {
        ...state,
        [action.data.composition_id]: {
          ...state[action.data.composition_id],
          composer_share: action.data.composer_percent,
          cowriter_share: action.data.cowriter_percent,
          cowriters: action.data.cowriters.map((c) => c.id),
          translated_name: action.data.translated_name,
          submitted_at: action.data.submitted_at,
          status: 'submitted',
          unknown_split_percent: action.data.unknown_split_percent,
        },
      }

    // The POST .../id/publishing_splits endpoint error response handler actionType
    case CREATE_SPLITS_ERRORS: {
      const {
        errors: { cowriterErrors },
      } = action.data

      return {
        ...state,
        [action.data.composition_id]: {
          ...state[action.data.composition_id],
          errors: {
            ...state[action.data.composition_id].errors,
            cowriterErrors,
          },
        },
      }
    }
    case UPDATE_SPLITS_SUCCESS:
      return {
        ...state,
        [action.data.composition.composition_id]: {
          ...state[action.data.composition.composition_id],
          ...action.data.composition,
          updated: true,
        },
      }

    // The PUT .../id/publishing_splits endpoint error response handler actionType
    case UPDATE_SPLITS_ERRORS:
      const {
        errors,
        errors: { cowriterErrors },
      } = action.data
      const preparedCompositionErrors = prepareCompositionErrors(errors)

      return {
        ...state,
        [action.data.composition_id]: {
          ...state[action.data.composition_id],
          errors: {
            ...preparedCompositionErrors,
            cowriterErrors,
          },
        },
      }
    case UPDATE_COMPOSITION:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          [action.attribute]: action.value,
        },
      }
    case UPDATE_COMPOSITION_ERRORS:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          errors: { ...state[action.compositionId].errors, ...action.errors },
        },
      }
    case UPDATE_COMPOSITION_SUCCESS:
      let compositions = Object.keys(state)
        .filter((id) => id !== action.data.id.toString())
        .reduce((accumulator, key) => {
          accumulator[key] = state[key]
          return accumulator
        }, {})
      return compositions
    case UPDATE_TRANSLATED_NAME:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          [action.attribute]: action.value,
        },
      }
    case ADD_NTC_COMPOSITION_SUCCESS:
      return {
        ...state,
        [action.data.id]: {
          ...action.data,
          status: 'submitted',
          cowriters: action.data.cowriters.map((cowriter) => cowriter.id),
        },
      }
    case REPLACE_COMPOSITION:
      return {
        ...state,
        [action.composition.id]: action.composition,
      }
    case UPDATE_COMPOSITION_CHANGED:
      if (action.attribute === '' && action.value === '') {
        return {
          ...state,
          [action.compositionId]: {
            ...state[action.compositionId],
            changed: {},
          },
        }
      }

      const changedAttribute =
        action.initialCompositionState == {} ||
        (action.attribute == 'composer_share'
          ? toNumber(action.value) !=
            toNumber(action.initialCompositionState[action.attribute])
          : action.initialCompositionState[action.attribute] !== action.value)
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          changed: {
            ...state[action.compositionId].changed,
            [action.attribute]: changedAttribute,
          },
        },
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
