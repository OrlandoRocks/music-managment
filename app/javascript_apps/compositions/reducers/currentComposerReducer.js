import { RESET_COMPOSER, UPDATE_COMPOSER } from '../actions/actionTypes'

export default function currentComposerReducer(state = {}, action) {
  switch (action.type) {
    case RESET_COMPOSER:
      return { ...action.data }
    case UPDATE_COMPOSER:
      return { ...state, ...action.newData }
    default:
      return state
  }
}
