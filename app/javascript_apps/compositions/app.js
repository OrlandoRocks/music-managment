import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import FeatureContext from './context.js'

import configureStore from './store/configureStore'
import CompositionsApp from './components/CompositionsApp'

const store = configureStore()
const dataset = document.getElementById('compositions_app').dataset
const ntcUrl = dataset.ntcBandAidUrl
const ntcCompositionFormEnabled = dataset.ntcCompositionFormEnabled
const canEditCompositions = dataset.editCompositionsFeatureEnabled
const bulkUploadCompositions = dataset.bulkUploadCompositionsFeatureEnabled
const publicDomainFeatureEnabled = dataset.publicDomainFeatureEnabled
const hasDraftCompositions = dataset.hasDraftCompositions

const features = {
  ntcUrl: ntcUrl,
  hasDraftCompositions: hasDraftCompositions === 'true',
  canEditCompositions: canEditCompositions === 'true',
  ntcCompositionFormEnabled: ntcCompositionFormEnabled === 'true',
  bulkUploadCompositions: bulkUploadCompositions === 'true',
  publicDomainFeatureEnabled: publicDomainFeatureEnabled === 'true',
}

ReactDOM.render(
  <Provider store={store}>
    <FeatureContext.Provider value={features}>
      <CompositionsApp />
    </FeatureContext.Provider>
  </Provider>,
  document.getElementById('compositions_app')
)
