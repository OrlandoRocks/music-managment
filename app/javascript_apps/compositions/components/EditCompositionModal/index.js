import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import Header from '../Modal/Header'
import Footer from '../Modal/Footer'
import CowritersSection from './CowritersSection'
import { addCowriter, replaceCowriters } from '../../actions/cowriterActions'
import {
  updateComposition,
  replaceComposition,
  updateCompositionChanged,
} from '../../actions/compositionActions'
import { updateSplits } from '../../actions/publishingSplitActions'
import EntityData, { TextField, DateField, CheckBoxField } from '../Entity'
import { hasCowriterErrors } from '../../utils/ntcCompositionValidations'
import { isFieldPresent } from '../../../shared/fieldValidations'
import { addErrorsToCowriters } from '../../utils/errorHelpers'
import sum from '../../../shared/sum'

const {
  edit_composition,
  composition_note,
  composition_note_description,
} = window.translations.compositions

const NonTunecoreCompositionForm = ({
  composition,
  onChange,
  publicDomainFeatureEnabled,
}) => {
  return (
    <EntityData source={composition} onChange={onChange}>
      <p className="original-song-note">
        {composition_note} {composition_note_description}
      </p>
      <TextField label="song_title" name="composition_title" required={true} />
      <TextField
        label="performing_artist"
        name="performing_artist"
        required={true}
      />
      <TextField label="album_name" name="appears_on" />
      <TextField label="record_label" name="record_label" />
      <TextField label="isrc_number" name="isrc" />
      <DateField label="release_date" name="release_date" />
      <TextField
        label="ownership_percentage"
        right_text="ownership_percentage_note"
        name="composer_share"
        type="number"
        required={true}
      />
      {publicDomainFeatureEnabled && (
        <CheckBoxField
          label="public_domain"
          description="public_domain_content"
          name="public_domain"
        />
      )}
    </EntityData>
  )
}

const TunecoreCompositionForm = ({
  composition,
  onChange,
  publicDomainFeatureEnabled,
}) => {
  return (
    <EntityData source={composition} onChange={onChange}>
      <p className="original-song-note">
        {composition_note} {composition_note_description}
      </p>
      <TextField
        label="ownership_percentage"
        right_text="ownership_percentage_note"
        name="composer_share"
        type="number"
        required={true}
      />
      {publicDomainFeatureEnabled && (
        <CheckBoxField
          label="public_domain"
          description="public_domain_content"
          name="public_domain"
        />
      )}
    </EntityData>
  )
}

const preValidateTitleLength = (oldValue, newValue) => {
  if (newValue.length > 160) {
    if (Math.abs(newValue.length - oldValue.length) > 1) {
      return newValue.substring(0, 160)
    } else {
      return oldValue
    }
  } else {
    return newValue
  }
}

const EditCompositionModal = ({
  composition,
  cowriters,
  closeModal,
  publicDomainFeatureEnabled,
  dispatch,
}) => {
  const [initialCompositionState] = useState({ ...composition })
  const [initialCowritersState] = useState({ ...cowriters })

  useEffect(() => {
    if (composition.updated) {
      closeModal()
      dispatch(updateComposition(composition.id, 'updated', false))
    }
  }, [closeModal, composition.id, composition.updated, dispatch])

  const handleChange = ({ target: { name, value } }) => {
    if (name == 'composition_title') {
      value = preValidateTitleLength(composition.composition_title, value)
    }

    dispatch(updateComposition(composition.id, name, value))
    dispatch(
      updateCompositionChanged(
        composition.id,
        initialCompositionState,
        name,
        value
      )
    )
  }

  const handleAddCowriter = () => {
    dispatch(addCowriter(composition.id))
  }

  const handleSubmit = () => {
    dispatch(updateSplits(composition, cowriters))
  }

  const handleCancel = () => {
    dispatch(replaceComposition(initialCompositionState))
    dispatch(replaceCowriters(initialCowritersState))
    closeModal()
  }

  const requiredFieldsPresent = () => {
    const { composition_title, performing_artist, composer_share } = composition
    if (composition.is_ntc_song === true) {
      return [composition_title, performing_artist, composer_share].every(
        isFieldPresent
      )
    }
    return isFieldPresent(composer_share)
  }

  const hasCompositionErrors =
    composition.errors &&
    !Object.values(composition.errors).every(
      (value) => value === null || value === ''
    )

  const invalidComposition =
    hasCompositionErrors ||
    hasCowriterErrors(cowriters) ||
    !requiredFieldsPresent()

  const changedComposition =
    composition.changed &&
    !Object.values(composition.changed).every((value) => !value)

  const changedCowriter =
    cowriters.length < Object.values(initialCowritersState).length ||
    (cowriters.length != 0 &&
      !cowriters.every(
        (cowriter) =>
          !cowriter.changed ||
          Object.values(cowriter.changed).every((value) => !value)
      ))

  const cowriterBtnDisabled =
    invalidComposition ||
    parseFloat(composition.composer_share || 0) +
      parseFloat(sum(cowriters, 'cowriter_share') || 0) >=
      100

  const submitBtnDisabled =
    invalidComposition || (!changedComposition && !changedCowriter)

  const hasTotalShareError =
    !composition.errors || composition.errors.total_shares === 'percent'

  const CompositionForm = composition.is_ntc_song
    ? NonTunecoreCompositionForm
    : TunecoreCompositionForm

  return (
    <div className="edit-composition-modal">
      <Header headerText={edit_composition} closeModal={handleCancel} />

      <CompositionForm
        composition={composition}
        onChange={handleChange}
        publicDomainFeatureEnabled={publicDomainFeatureEnabled}
      />
      <CowritersSection
        btnDisabled={cowriterBtnDisabled}
        compositionId={composition.id}
        hasTotalShareError={hasTotalShareError}
        cowriters={cowriters}
        initialCowritersState={initialCowritersState}
        addCowriter={handleAddCowriter}
      />

      <Footer
        btnDisabled={submitBtnDisabled}
        onSubmit={handleSubmit}
        closeModal={handleCancel}
      />
    </div>
  )
}

const mapStateToProps = (storeState) => {
  const { compositions, currentComposition } = storeState
  const composition = compositions[currentComposition.compositionId]
  let cowriters = []

  if (Object.prototype.hasOwnProperty.call(composition, 'cowriters')) {
    cowriters = composition.cowriters.reduce((acc, cowriterId) => {
      let cowriter = storeState.cowriters[cowriterId]
      if (cowriter && !cowriter.is_unknown) {
        acc.push(cowriter)
      }

      return acc
    }, [])
  }

  const cowritersWithErrors = addErrorsToCowriters(
    cowriters,
    composition.errors
  )

  return { composition, cowriters: cowritersWithErrors }
}

export default connect(mapStateToProps)(EditCompositionModal)
