import React from 'react'
import Cowriters from '../SplitSharesModal/Cowriters'

const { list_cowriters } = window.translations.compositions.splits_modal

export default function CowritersSection({
  compositionId,
  hasTotalShareError,
  cowriters,
  initialCowritersState,
  addCowriter,
  btnDisabled,
}) {
  const hasCowriters = cowriters.length > 0

  return (
    <div>
      {hasCowriters ? (
        <Cowriters
          addCowriter={addCowriter}
          cowriters={cowriters}
          initialCowritersState={initialCowritersState}
          compositionId={compositionId}
          hasTotalShareError={hasTotalShareError}
          btnDisabled={btnDisabled}
        />
      ) : (
        <button
          className="list-of-cowriters-link publishing-splits-cowriters-add-cowriter-btn"
          onClick={addCowriter}
          disabled={btnDisabled}
        >
          <b>{list_cowriters}</b>
        </button>
      )}
    </div>
  )
}
