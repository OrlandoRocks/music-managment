import React from 'react'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import EditComposerModal from './EditComposerModal/EditComposerModal'
import SplitSharesModal from './SplitSharesModal/SplitSharesModal'
import AddCompositions from './AddCompositions/AddCompositions'
import UploadCompositions from './AddCompositions/UploadCompositions'
import { resetComposer, saveComposer } from '../actions/composerActions'
import { resetComposition } from '../actions/cowriterActions'

Modal.setAppElement('body')

class ComposerCard extends React.Component {
  state = {
    addSharesModalIsOpen: false,
    editModalIsOpen: false,
  }

  closeModal = ({ target: { id } }) => {
    const modalId = id.includes('edit') ? 'edit' : 'addShares'

    this.setState({
      [`${modalId}ModalIsOpen`]: false,
    })
  }

  numOfCompositions = () => {
    return Object.keys(this.props.compositions).length
  }

  numOfSharesMissing = () => {
    const compositions = this.props.compositions
    return Object.values(compositions).filter(
      (composition) => composition.status === 'shares_missing'
    ).length
  }

  openModal = ({ target: { id } }) => {
    const modalId = id.includes('edit') ? 'edit' : 'addShares'

    this.setState({
      [`${modalId}ModalIsOpen`]: true,
    })
  }

  resetCurrentComposer = () => {
    this.props.dispatch(resetComposer(this.props.composer))

    this.openModal({ target: { id: 'edit' } })
  }

  resetCurrentComposition = () => {
    this.props.dispatch(resetComposition(this.props.composition))
    this.openModal({ target: { id: 'addShares' } })
  }

  saveComposer = () => {
    const { currentComposer } = this.props
    this.props.dispatch(
      saveComposer(currentComposer.id, {
        name_prefix: currentComposer.prefix,
        first_name: currentComposer.first_name,
        middle_name: currentComposer.middle_name,
        last_name: currentComposer.last_name,
        name_suffix: currentComposer.suffix,
        composer_pro_id: currentComposer.pro_id,
        composer_cae: parseInt(currentComposer.cae)
          ? currentComposer.cae
          : null,
      })
    )
  }

  sharesMissingLinkOrText = () => {
    const { composer } = this.props
    const numOfSharesMissing = this.numOfSharesMissing()
    const shouldNotBeLink =
      numOfSharesMissing === 0 || composer.status === 'terminated'
    const translations =
      window.translations.compositions.compositions_card.shares_missing
    const sharesMissingText = translations.replace(
      '{{numSharesMissing}}',
      numOfSharesMissing
    )

    if (shouldNotBeLink) {
      return sharesMissingText
    }

    return (
      <a
        id="addSharesLink"
        onClick={this.resetCurrentComposition}
        className="publishing-dashboard-card-tax-info-link"
      >
        {sharesMissingText}
      </a>
    )
  }

  render() {
    const { addSharesModalIsOpen, editModalIsOpen } = this.state
    const {
      composer,
      cowriters,
      bulkUploadEnabled,
      canEditCompositions,
      publicDomainFeatureEnabled,
    } = this.props
    const translations = window.translations.compositions.compositions_card

    return (
      <div className="publishing-dashboard-react-card">
        <div className="publishing-dashboard-card">
          <div className="publishing-dashboard-card-row grid-x align-justify">
            <div className="small-6">
              <div className="grid-x">
                <div className="react-publishing-card-name publishing-dashboard-card-name cell small-12">
                  <h5>
                    <b>{composer.name}</b>
                  </h5>
                </div>
              </div>
              {bulkUploadEnabled && (
                <div className="grid-x">
                  <UploadCompositions composer={composer} />
                </div>
              )}
            </div>

            <div className="publishing-dashboard-card-name-react small-6 align-right">
              <div className="grid-x">
                <div className="publishing-dashboard-card-right-col-react small-12 align-right">
                  {translations.compositions.replace(
                    '{{numCompositions}}',
                    this.numOfCompositions()
                  )}
                </div>

                <div className="publishing-dashboard-card-right-col-react small-12 publishing-dashboard-card-right-col-react align-right div-shares-missing">
                  {this.sharesMissingLinkOrText()}
                </div>
                {canEditCompositions && (
                  <div className="publishing-dashboard-card-right-col-react small-12 align-right">
                    <AddCompositions
                      composer={composer}
                      cowriters={cowriters}
                      publicDomainFeatureEnabled={publicDomainFeatureEnabled}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <Modal
          isOpen={editModalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="Edit Composer Modal"
          className="composer-modal"
        >
          <EditComposerModal
            closeModal={this.closeModal}
            onComposerSave={this.saveComposer}
          />
        </Modal>
        <Modal
          className="publishing-splits-modal"
          isOpen={addSharesModalIsOpen}
          onRequestClose={this.closeModal}
          overlayClassName="publishing-compositions-add-shares-modal-overlay"
          ariaHideApp={false}
        >
          <SplitSharesModal
            closeModal={this.closeModal}
            publicDomainFeatureEnabled={publicDomainFeatureEnabled}
          />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const {
    compositions,
    composer,
    currentComposer,
    currentComposition,
    cowriters,
  } = storeState
  const composition = compositions[currentComposition.compositionId] || {}

  return {
    composer,
    composition,
    compositions,
    currentComposer,
    cowriters,
  }
}

export default connect(mapStateToProps)(ComposerCard)
