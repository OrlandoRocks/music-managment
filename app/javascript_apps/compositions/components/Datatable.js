import React, { useState, useEffect, useContext } from 'react'
import ReactTable from 'react-table'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import { resetComposition } from '../actions/cowriterActions'
import {
  setCurrentComposition,
  updateCompositionChanged,
} from '../actions/compositionActions'
import SplitSharesModal from './SplitSharesModal/SplitSharesModal'
import EditCompositionModal from './EditCompositionModal'
import compositionTableHeaders from './Datatable/CompositionTableHeaders'
import tableContent from './Datatable/TableContent'
import TableHeader from './Datatable/TableHeader'
import filterCompositions from '../utils/filterCompositions'
import FeatureContext from '../context.js'

const translations = window.translations.compositions

export const DataTable = ({
  compositions,
  composer,
  currentComposition,
  dispatch,
}) => {
  const [modalName, setModalName] = useState('')
  const [searchValue, setSearchValue] = useState('')
  const [searchedCompositions, setSearchedCompositions] = useState(compositions)

  useEffect(() => {
    const searchedCompositions = filterCompositions(compositions, searchValue)
    setSearchedCompositions(searchedCompositions)
  }, [compositions, searchValue])

  const openModal = ({ target: { name, dataset } }) => {
    const currentCompositionId = parseInt(dataset.id, 10)

    setModalName(name)
    dispatch(setCurrentComposition(compositions[currentCompositionId]))
  }

  const closeModal = ({ target: { id } }) => {
    const shouldCleanupModal = id !== 'split_submit'
    if (shouldCleanupModal) {
      handleResetComposition()
    }

    setModalName('')
  }

  const closeEditModal = () => {
    dispatch(
      updateCompositionChanged(currentComposition.compositionId, {}, '', '')
    )
    setModalName('')
  }

  const handleResetComposition = () => {
    dispatch(resetComposition(compositions[currentComposition.compositionId]))
  }

  const resetSearchedCompositions = () => {
    setSearchValue('')
    setSearchedCompositions(compositions)
  }

  const searchCompositions = ({ target: { value } }) => {
    let searchedCompositions = filterCompositions(compositions, value)
    setSearchedCompositions(searchedCompositions)
    setSearchValue(value)
  }

  const numOfCompositions = Object.keys(searchedCompositions).length
  const minRows = numOfCompositions > 0 ? 0 : 10
  const { canEditCompositions } = useContext(FeatureContext)
  const { publicDomainFeatureEnabled } = useContext(FeatureContext)
  const { hasDraftCompositions } = useContext(FeatureContext)

  return (
    <div>
      <div>
        <strong>{translations.please_note}: </strong>
        {translations.ntc_note}
      </div>
      {hasDraftCompositions && (
        <div>
          <br></br>
          {translations.draft} {translations.all_shares_are_required_cta}
        </div>
      )}
      <TableHeader
        searchValue={searchValue}
        searchCompositions={searchCompositions}
        resetCompositions={resetSearchedCompositions}
        numOfCompositions={numOfCompositions}
      />
      <ReactTable
        data={tableContent(composer, openModal, searchedCompositions)}
        columns={compositionTableHeaders(canEditCompositions)}
        defaultPageSize={100}
        minRows={minRows}
        pageSizeOptions={[20, 50, 100, 150]}
      />
      <Modal
        className="publishing-splits-modal"
        isOpen={modalName === 'add-shares'}
        onRequestClose={closeModal}
        overlayClassName="publishing-compositions-add-shares-modal-overlay"
        ariaHideApp={false}
      >
        <SplitSharesModal
          closeModal={closeModal}
          publicDomainFeatureEnabled={publicDomainFeatureEnabled}
        />
      </Modal>
      <Modal
        className="edit-composition-modal"
        overlayClassName="edit-composition-modal-overlay"
        isOpen={modalName === 'edit-composition'}
        onRequestClose={closeEditModal}
        shouldCloseOnOverlayClick={false}
        shouldCloseOnEsc={false}
        ariaHideApp={false}
      >
        <EditCompositionModal
          closeModal={closeEditModal}
          publicDomainFeatureEnabled={publicDomainFeatureEnabled}
        />
      </Modal>
    </div>
  )
}

const mapStateToProps = (storeState) => {
  const {
    compositions,
    composer,
    currentComposition,
    publicDomainFeatureEnabled,
  } = storeState

  return {
    compositions,
    composer,
    currentComposition,
    publicDomainFeatureEnabled,
  }
}

export default connect(mapStateToProps)(DataTable)
