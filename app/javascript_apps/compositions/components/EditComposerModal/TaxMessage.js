import React from 'react'

const TaxMessage = ({ closeModal, translations }) => {
  const taxLink = `https:${window.translations['submit-tax-info-howto']}`

  return (
    <div className="composer-modal-tax-info">
      <div className="composer-modal-tax-info-msg">
        <b>{translations.name_change_matches_tax_records}</b>
      </div>

      <a
        href={taxLink}
        className="composer-modal-tax-info-link"
        target="_blank"
        rel="noopener noreferrer"
      >
        {translations.see_tax_information_here}
      </a>

      <div className="composer-modal-tax-info-btn-wrapper">
        <button className="composer-modal-tax-info-btn" onClick={closeModal}>
          {translations.ok}
        </button>
      </div>
    </div>
  )
}

export default TaxMessage
