import React from 'react'
import PropTypes from 'prop-types'

class ComposerInfo extends React.Component {
  static propTypes = {
    currentComposer: PropTypes.object.isRequired,
    errorMsgs: PropTypes.object.isRequired,
    handleComposerChanges: PropTypes.func.isRequired,
    translations: PropTypes.object.isRequired,
  }

  createDropDownOptions = (attribute) => {
    const data = window.translations.compositions.compositions_card[attribute]
    const options = [<option key="none" value="" />]

    return options.concat(
      Object.keys(data).map((key) => (
        <option key={key} value={key}>
          {data[key]}
        </option>
      ))
    )
  }

  render() {
    const {
      currentComposer,
      errorMsgs,
      handleComposerChanges,
      translations,
    } = this.props

    return (
      <div>
        <table className="composer-modal-table">
          <thead className="composer-modal-table-head">
            <tr>
              <th>{translations.prefix}</th>
              <th>*{translations.first_name}</th>
              <th>{translations.middle_name}</th>
              <th>*{translations.last_name}</th>
              <th>{translations.suffix}</th>
            </tr>
          </thead>

          <tbody className="composer-modal-table-body">
            <tr>
              <td>
                <select
                  id="prefix"
                  name="prefix"
                  className="cell small-1"
                  value={currentComposer.prefix}
                  onChange={handleComposerChanges}
                >
                  {this.createDropDownOptions('prefixes')}
                </select>
              </td>
              <td>
                <input
                  id="firstName"
                  type="text"
                  name="first_name"
                  className={`cell small-3 ${
                    errorMsgs.firstName ? 'error' : ''
                  }`}
                  value={currentComposer.first_name}
                  onChange={handleComposerChanges}
                />
              </td>
              <td>
                <input
                  id="middleName"
                  type="text"
                  name="middle_name"
                  className="cell small-2"
                  value={currentComposer.middle_name}
                  onChange={handleComposerChanges}
                />
              </td>
              <td>
                <input
                  id="lastName"
                  type="text"
                  name="last_name"
                  className={`cell small-3 ${
                    errorMsgs.lastName ? 'error' : ''
                  }`}
                  value={currentComposer.last_name}
                  onChange={handleComposerChanges}
                />
              </td>
              <td>
                <select
                  id="suffix"
                  name="suffix"
                  className="cell small-1"
                  value={currentComposer.suffix}
                  onChange={handleComposerChanges}
                >
                  {this.createDropDownOptions('suffixes')}
                </select>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default ComposerInfo
