import React from 'react'
import PropTypes from 'prop-types'
import ComposerInfo from './ComposerInfo'
import PublishingInfo from './PublishingInfo'
import TaxMessage from './TaxMessage'
import { connect } from 'react-redux'
import { updateComposer } from '../../actions/composerActions'

class EditComposerModal extends React.Component {
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    onComposerSave: PropTypes.func.isRequired,
  }

  state = {
    showTaxMsg: false,
    errorMsgs: {
      firstName: '',
      lastName: '',
      cae: '',
    },
  }

  canSubmit() {
    const { cae, pro_id } = this.props.currentComposer
    const proAndCaeMissing = !pro_id && !cae
    const proAndCaePresent = pro_id && cae.length >= 9 && cae.length <= 11
    return proAndCaeMissing || proAndCaePresent
  }

  handleComposerChanges = ({ target: { value, name } }) => {
    this.props.dispatch(updateComposer({ [name]: value }))
  }

  saveComposer = () => {
    const isValid = this.validateFields()

    if (isValid) {
      this.props.onComposerSave()
      this.taxMsgOrCloseModal()
    }
  }

  showErrorMsgs() {
    let msgs = this.state.errorMsgs
    let msgDivs = Object.keys(msgs).map((key) => (
      <div key={key}>{msgs[key]}</div>
    ))

    return <div className="composer-error">{msgDivs}</div>
  }

  taxMsgOrCloseModal() {
    const {
      has_tax_info,
      full_name,
      prefix,
      first_name,
      middle_name,
      last_name,
      suffix,
    } = this.props.currentComposer

    let newName = `${prefix} ${first_name} ${middle_name} ${last_name} ${suffix}`

    if (has_tax_info && full_name != newName.trim()) {
      this.setState({ showTaxMsg: true })
    } else {
      this.props.closeModal({ target: { id: 'edit' } })
    }
  }

  validateFields() {
    let translations = window.translations.compositions.validations
    let isValid = true
    let newState = {}
    let { first_name, last_name, cae } = this.props.currentComposer

    if (first_name.trim().length === 0) {
      newState.firstName = translations.first_name
      isValid = false
    }

    if (last_name.trim().length === 0) {
      newState.lastName = translations.last_name
      isValid = false
    }

    if (cae.length > 0 && isNaN(cae)) {
      newState.cae = translations.cae_is_not_a_number
      isValid = false
    }

    this.setState({ errorMsgs: { ...this.state.errorMsgs, ...newState } })

    return isValid
  }

  render() {
    const { currentComposer } = this.props
    const { errorMsgs, showTaxMsg } = this.state
    const translations = window.translations.compositions.compositions_card

    return (
      <div>
        <div className="publishing-edit-info-modal">
          <div className="grid-x composer-modal-header">
            <div className="cell small-5">
              <b>{translations.edit_songwriter}</b>
            </div>
            <div className="cell small-6" />
            <div className="cell small-1 close">
              <i
                id="editClose"
                className="fa fa-times"
                onClick={this.props.closeModal}
              ></i>
            </div>
          </div>

          {showTaxMsg ? (
            <TaxMessage
              closeModal={this.props.closeModal}
              translations={translations}
            />
          ) : (
            <div className="composer-modal-body">
              {this.showErrorMsgs()}

              <ComposerInfo
                currentComposer={currentComposer}
                errorMsgs={errorMsgs}
                handleComposerChanges={this.handleComposerChanges}
                translations={translations}
              />

              <PublishingInfo
                currentComposer={currentComposer}
                errorMsgs={errorMsgs}
                handleComposerChanges={this.handleComposerChanges}
                pros={this.props.pros}
                translations={translations}
              />

              <div className="composer-customer-support">
                <b>{translations.customer_support}</b>
              </div>

              <div className="grid-x composer-modal-buttons">
                <span className="cell small-7" />
                <div className="cell small-5 composer-modal-btn-wrapper">
                  <button
                    id="btnSave"
                    className="button save"
                    onClick={this.saveComposer}
                    disabled={!this.canSubmit()}
                  >
                    {translations.save}
                  </button>

                  <button
                    id="editBtnCancel"
                    className="button cancel"
                    onClick={this.props.closeModal}
                  >
                    {translations.cancel}
                  </button>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  return {
    currentComposer: storeState.currentComposer,
    pros: storeState.pros,
  }
}

export default connect(mapStateToProps)(EditComposerModal)
