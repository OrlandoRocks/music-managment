import React from 'react'
import PropTypes from 'prop-types'
import replace from 'react-string-replace'

class PublishingInfo extends React.Component {
  static propTypes = {
    currentComposer: PropTypes.object.isRequired,
    errorMsgs: PropTypes.object.isRequired,
    handleComposerChanges: PropTypes.func.isRequired,
    pros: PropTypes.array.isRequired,
    translations: PropTypes.object.isRequired,
  }

  proDropDown() {
    const { currentComposer, translations, pros } = this.props
    const proLabel = translations.pro
    const options = [<option key="none" value="" />]

    return (
      <div className="cell small-3 composer-pro">
        <b>{proLabel}</b>
        <select
          id="proId"
          name="pro_id"
          className="cell small-3"
          value={currentComposer.pro_id}
          onChange={this.props.handleComposerChanges}
        >
          {options.concat(
            pros.map((pro) => (
              <option key={pro.id} value={pro.id}>
                {pro.name}
              </option>
            ))
          )}
        </select>
      </div>
    )
  }

  tipMsg() {
    const translations = this.props.translations
    let tip_msg = replace(translations.tip_message, '{{TIP}}', () => (
      <b>{translations.tip}</b>
    ))
    return replace(tip_msg, '{{WRITER}}', () => <b>{translations.writer}</b>)
  }

  render() {
    const { currentComposer, errorMsgs, translations } = this.props
    const caeLink = `https:${window.translations['what-is-the-cae-ipi-number']}`

    return (
      <div>
        {this.proDropDown()}

        <div className="cell small-5 composer-cae">
          <b>{translations.cae}</b>
          <div className="grid-x">
            <input
              disabled={!currentComposer.pro_id}
              id="cae"
              type="text"
              name="cae"
              maxLength="11"
              className={`${errorMsgs.cae ? 'error' : ''}`}
              value={currentComposer.cae}
              onChange={this.props.handleComposerChanges}
            />

            <b className="cell small-offset-1 small-7">
              {translations.find_cae_numbers_help_link}&nbsp;
              <a href={caeLink} target="_blank" rel="noopener noreferrer">
                {window.translations.compositions.tooltips.here}
              </a>
            </b>
          </div>

          <div>
            <p>{this.tipMsg()}</p>
          </div>
        </div>
      </div>
    )
  }
}

export default PublishingInfo
