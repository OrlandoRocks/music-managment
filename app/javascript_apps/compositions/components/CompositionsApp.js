import React, { useContext } from 'react'
import ComposerCard from './ComposerCard'
import Datatable from './Datatable'
import FeatureContext from '../context.js'

function CompositionsApp() {
  const {
    bulkUploadCompositions,
    canEditCompositions,
    publicDomainFeatureEnabled,
  } = useContext(FeatureContext)

  return (
    <div>
      <ComposerCard
        bulkUploadEnabled={bulkUploadCompositions}
        canEditCompositions={canEditCompositions}
        publicDomainFeatureEnabled={publicDomainFeatureEnabled}
      />
      <Datatable />
    </div>
  )
}

export default CompositionsApp
