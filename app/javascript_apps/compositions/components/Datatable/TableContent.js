import React from 'react'
import Action from './Action'
import YourShare from './YourShare'
import CowriterShares from './CowriterShares'
import moment from 'moment'

const statuses = window.translations.compositions.statuses

export default function TableContent(
  composer,
  openModal,
  searchedCompositions
) {
  function formatDateForDisplay(date) {
    if (date) {
      return moment(date).format('YYYY-MM-DD')
    } else {
      return 'Not Submitted'
    }
  }

  function formatCowriterShareForDisplay(composition) {
    let cowriterShare = parseInt(composition.cowriter_share, 10)
    let cowriterSharesComponent = (
      <div className="cell-tooltip">
        <CowriterShares composition={composition} />
      </div>
    )

    return cowriterShare >= 1 ? cowriterSharesComponent : '-'
  }

  const composerIsTerminated = composer.status === 'terminated'

  return Object.values(searchedCompositions).map((composition, index) => {
    const compositionStatus = (
      <div className="composition-status">{statuses[composition.status]}</div>
    )
    const shouldDisplayContent = !(
      composerIsTerminated && parseInt(composition.composer_share) < 1
    )
    const yourShareComponent = (
      <YourShare
        index={index}
        composition={composition}
        openModal={openModal}
      />
    )
    const actionComponent = (
      <Action index={index} composition={composition} openModal={openModal} />
    )

    return {
      composition_title:
        composition.translated_name || composition.composition_title,
      appears_on: composition.appears_on,
      isrc: composition.isrc,
      submitted_at: formatDateForDisplay(composition.submitted_at),
      your_share: shouldDisplayContent && yourShareComponent,
      cowriter_share:
        shouldDisplayContent && formatCowriterShareForDisplay(composition),
      status: compositionStatus,
      action: shouldDisplayContent && actionComponent,
    }
  })
}
