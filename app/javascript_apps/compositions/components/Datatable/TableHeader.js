import React from 'react'
import PropTypes from 'prop-types'

class TableHeader extends React.Component {
  static propTypes = {
    searchValue: PropTypes.string.isRequired,
    numOfCompositions: PropTypes.number,
    resetCompositions: PropTypes.func.isRequired,
    searchCompositions: PropTypes.func.isRequired,
  }

  state = {
    showSearchInput: false,
  }

  toggleSearchInput = () => {
    this.setState({ showSearchInput: !this.state.showSearchInput })
  }

  displaySearchInput(searchPlaceholder) {
    const { searchCompositions, resetCompositions, searchValue } = this.props

    return (
      <div className="grid-x">
        <input
          type="text"
          className="datatable-search-input cell small-10"
          onChange={searchCompositions}
          value={searchValue}
          placeholder={searchPlaceholder}
        />
        <i
          className="datatable-cancel-search cell small-2 fa fa-times-circle"
          onClick={resetCompositions}
        ></i>
      </div>
    )
  }

  render() {
    const translations = window.translations.compositions

    return (
      <div className="datatable-columns-header">
        <div className="grid-x">
          <div className="cell small-9">
            <b>{translations.data_table_headers.table_header} </b>
            <span className="datatable-compositions-count">
              ({this.props.numOfCompositions})
            </span>
          </div>
          <div className="cell small-3 datatable-search-field">
            {this.state.showSearchInput ? (
              this.displaySearchInput(translations.search_placeholder)
            ) : (
              <i
                onClick={this.toggleSearchInput}
                className="datatable-search-icon fa fa-search"
              ></i>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default TableHeader
