import React from 'react'

const { add_shares, edit_composition } = window.translations.compositions

const addCompositionButton = (index, composition, openModal) => {
  return (
    <a
      id={`add_shares_button_${composition.id}`}
      name="add-shares"
      onClick={openModal}
      data-id={composition.id}
      data-index={index + 1}
    >
      <i className="fa fa-plus"></i> {add_shares}
    </a>
  )
}

const editCompositionButton = (index, composition, openModal) => {
  return (
    <a
      id={`edit_composition_button_${composition.id}`}
      name="edit-composition"
      onClick={openModal}
      data-id={composition.id}
      data-index={index + 1}
    >
      {edit_composition}
    </a>
  )
}

const Action = ({ index, composition, openModal }) => {
  const actionButton = () => {
    if (composition.status === 'ineligible') {
      return
    }

    if (composition.status === 'shares_missing') {
      return addCompositionButton(index, composition, openModal)
    }

    return editCompositionButton(index, composition, openModal)
  }

  return <div>{actionButton()}</div>
}

export default Action
