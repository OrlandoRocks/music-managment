import React, { useContext } from 'react'
import FeatureContext from '../../context.js'

const addSharesText = window.translations.compositions.add_shares

const displayComposerShare = ({ id, composer_share }) => {
  const composerShare = parseInt(composer_share)

  return (
    <span id={`your_share_percent_${id}`}>
      {composerShare > 1 ? `${composerShare}%` : '-'}
    </span>
  )
}

const yourShareColumnDisplay = (composition, addShareButton) => {
  return composition.status === 'shares_missing'
    ? addShareButton
    : displayComposerShare(composition)
}

const YourShare = ({ index, composition, openModal }) => {
  const { canEditCompositions } = useContext(FeatureContext)

  const addShareButton = (
    <a
      id={`add_shares_button_${composition.id}`}
      name="add-shares"
      onClick={openModal}
      data-id={composition.id}
      data-index={index + 1}
    >
      <i className="fa fa-plus"></i> {addSharesText}
    </a>
  )

  return (
    <div>
      {canEditCompositions
        ? displayComposerShare(composition)
        : yourShareColumnDisplay(composition, addShareButton)}
    </div>
  )
}

export default YourShare
