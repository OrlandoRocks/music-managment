import React from 'react'
import replace from 'react-string-replace'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const { data_table_headers, tooltips } = window.translations.compositions

function yourShareContentToolTipUrl() {
  return replace(tooltips.your_share_content, '{{HERE}}', (match, i) => (
    <a
      href={
        window.translations['What-are-splits-and-how-do-I-figure-out-my-split-']
      }
      key={i}
    >
      {window.translations.compositions.tooltips.here}
    </a>
  ))
}

const YourShareTooltip = (
  <TooltipContainer>
    <TooltipTitle>{tooltips.your_share_header}</TooltipTitle>
    <TooltipColumn>{yourShareContentToolTipUrl()}</TooltipColumn>
  </TooltipContainer>
)

const CowriterShareTooltip = (
  <TooltipContainer>
    <TooltipTitle>{tooltips.cowriter_share_header}</TooltipTitle>
    <TooltipColumn>{tooltips.cowriter_share_content}</TooltipColumn>
  </TooltipContainer>
)

const actionComponent = {
  Header: data_table_headers.action,
  accessor: 'action',
  headerClassName: 'table-header-cell',
  className: 'datatable-action',
}

export default function compositionTableHeaders(canEditCompositions) {
  const columns = [
    {
      Header: data_table_headers.composition_title,
      accessor: 'composition_title',
      headerClassName: 'table-header-cell',
      className: 'datatable-composition-title',
      width: 150,
    },
    {
      Header: data_table_headers.appears_on,
      accessor: 'appears_on',
      headerClassName: 'table-header-cell',
      className: 'datatable-appears-on',
    },
    {
      Header: data_table_headers.isrc,
      accessor: 'isrc',
      headerClassName: 'table-header-cell',
      className: 'datatable-isrc',
      width: 100,
    },
    {
      Header: data_table_headers.submitted_at,
      accessor: 'submitted_at',
      headerClassName: 'table-header-cell',
      className: 'datatable-submitted-at',
    },
    {
      Header: (
        <div>
          {data_table_headers.your_share} <Tooltip>{YourShareTooltip}</Tooltip>
        </div>
      ),
      accessor: 'your_share',
      headerClassName: 'table-header-cell',
      className: 'datatable-your-share',
      width: 125,
    },
    {
      Header: (
        <div>
          {data_table_headers.cowriter_share}{' '}
          <Tooltip>{CowriterShareTooltip}</Tooltip>
        </div>
      ),
      accessor: 'cowriter_share',
      headerClassName: 'table-header-cell',
      className: 'datatable-cowriter-share',
      width: 155,
    },
    {
      Header: data_table_headers.status,
      accessor: 'status',
      headerClassName: 'table-header-cell',
      className: 'datatable-status',
      width: 100,
    },
  ]

  return [
    {
      headerStyle: { display: 'none' },
      columns: [...columns, canEditCompositions && actionComponent].filter(
        Boolean
      ),
    },
  ]
}
