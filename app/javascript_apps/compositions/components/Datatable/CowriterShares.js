import React from 'react'
import { connect } from 'react-redux'
import {
  Tooltip,
  TooltipContainer,
  TooltipTitle,
  TooltipColumn,
} from '../../../shared/Tooltip'

const { unknown_writer } = window.translations.compositions.tooltips

function cowriterSplitDisplay(id, cowriterShare, cowriterName) {
  return (
    <div key={id}>
      {parseFloat(cowriterShare, 10)}%
      <span className="grey-text">{cowriterName}</span>
    </div>
  )
}

function cowriterSplits(composition, cowriters) {
  let cowriter

  return composition.cowriters.map((cowriterId) => {
    cowriter = cowriters[cowriterId]
    return (
      cowriter &&
      cowriterSplitDisplay(
        cowriterId,
        cowriter.cowriter_share,
        cowriter.full_name
      )
    )
  })
}

function unknownCowriterSplit(composition) {
  const { unknown_split_percent } = composition
  if (
    unknown_split_percent === null ||
    parseInt(unknown_split_percent, 10) === 0
  )
    return

  return cowriterSplitDisplay(
    composition.id,
    unknown_split_percent,
    unknown_writer
  )
}

export const CowriterShares = ({ composition, cowriters }) => {
  const formatCowriterSplitsForDisplay = () => {
    return [
      cowriterSplits(composition, cowriters),
      unknownCowriterSplit(composition),
    ].filter(Boolean)
  }

  const cowriterShareTotal = parseInt(composition.cowriter_share)

  return (
    <Tooltip displayText={`${cowriterShareTotal}%`}>
      <TooltipContainer>
        <TooltipTitle />
        <TooltipColumn>{formatCowriterSplitsForDisplay()}</TooltipColumn>
      </TooltipContainer>
    </Tooltip>
  )
}

function mapStateToProps(storeState) {
  const { cowriters } = storeState
  return { cowriters }
}

export default connect(mapStateToProps)(CowriterShares)
