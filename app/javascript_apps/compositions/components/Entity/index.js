import React, { createContext, useContext } from 'react'
import moment from 'moment'

const EntityDataContext = createContext()

const translations = window.translations.compositions.add_compositions
const tooltip_translations = window.translations.compositions.tooltips
const { validations } = translations

export default function EntityData({ children, source, onChange }) {
  const context = { source, onChange }

  return (
    <div className="entity-data-container">
      <EntityDataContext.Provider value={context}>
        {children}
      </EntityDataContext.Provider>
    </div>
  )
}

export const TextField = ({
  label,
  required,
  name,
  right_text = '',
  type = 'text',
  maxLength = '255',
}) => {
  const { source, onChange } = useContext(EntityDataContext)
  const isRequiredLabel = required
    ? `* ${translations[label]}`
    : translations[label]
  const hasError = source.errors[name] && source.errors[name].length > 1
  const hasRightText = right_text != ''
  const textFieldClass =
    (hasError ? 'error-field' : '') +
    (hasRightText ? ' compositions-text-field-has-right-text' : '')
  const inputField = (
    <React.Fragment>
      <input
        type={type}
        name={name}
        value={source[name] || ''}
        className={textFieldClass}
        onChange={onChange}
        maxLength={maxLength}
      />
      {hasError && (
        <ErrorMessage>{validations[source.errors[name]]}</ErrorMessage>
      )}
    </React.Fragment>
  )

  return (
    <div className={`${hasError ? `error-container ${name}` : name}`}>
      <label htmlFor={label}>{isRequiredLabel}</label>
      {hasRightText ? (
        <div className={`${name}-input-wrapper`}>{inputField}</div>
      ) : (
        inputField
      )}
      {hasRightText && (
        <p className="compositions-text-field-right-text">
          {translations[right_text]}
        </p>
      )}
    </div>
  )
}

export const CheckBoxField = ({ label, description, name }) => {
  const { source, onChange } = useContext(EntityDataContext)
  const handleChange = (e) => {
    onChange({
      target: { name: name, value: e.currentTarget.value === 'true' },
    })
  }
  return (
    <div className={name}>
      <label htmlFor={label}>{translations[label]}</label>
      {window.translations.cover_song_toggle_buton_yes}
      <input
        className="public-domain-checkbox-input"
        type="radio"
        name={`${name}_yes`}
        value="true"
        checked={source[name]}
        onChange={handleChange}
      />
      {window.translations.cover_song_toggle_button_no}
      <input
        className="public-domain-checkbox-input"
        type="radio"
        name={`${name}_no`}
        value="false"
        checked={!source[name]}
        onChange={handleChange}
      />
      <p className="publishing-compositions-description-text">
        {tooltip_translations[description]}
      </p>
    </div>
  )
}

const today = () => {
  return moment().format('YYYY-MM-DD')
}

const minDate = () => {
  return moment().format('1900-01-01')
}

export const DateField = ({ label, name }) => {
  const { source, onChange } = useContext(EntityDataContext)
  const hasError = source.errors[name]

  return (
    <div className={`${hasError ? `error-container ${name}` : name}`}>
      <label>{translations[label]}</label>
      <input
        type="text"
        name={name}
        value={source[name]}
        className={hasError ? 'error-field' : ''}
        min={minDate()}
        max={today()}
        placeholder="MM/DD/YYYY"
        onChange={onChange}
      />

      {hasError && (
        <ErrorMessage>{validations[source.errors[name]]}</ErrorMessage>
      )}
    </div>
  )
}

export const ErrorMessage = ({ children }) => {
  return <span className="error-message">{children}</span>
}
