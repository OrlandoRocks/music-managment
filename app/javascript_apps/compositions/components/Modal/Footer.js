import React from 'react'

const { cancel, register } = window.translations.compositions.add_compositions

export default function Footer({ btnDisabled = false, onSubmit, closeModal }) {
  return (
    <div className="footer grid-x">
      <div className="cell small-6">
        <button className="cancel-btn" onClick={closeModal}>
          {cancel}
        </button>
      </div>
      <div className="cell small-6">
        <button
          className="register-btn"
          onClick={onSubmit}
          disabled={btnDisabled}
        >
          {register}
        </button>
      </div>
    </div>
  )
}
