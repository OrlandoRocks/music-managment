import React from 'react'

export default function Header({ composerName = '', closeModal, headerText }) {
  return (
    <div className="header grid-x">
      <div className="cell small-11">
        {headerText} {composerName}
      </div>
      <div className="close-btn cell small-1" onClick={closeModal}>
        <i className="fa fa-times" />
      </div>
    </div>
  )
}
