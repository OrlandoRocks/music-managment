import React from 'react'
import PropTypes from 'prop-types'
import Header from '../Modal/Header'

const translations = window.translations.compositions.bulk_compositions

class UploadCompositionsModal extends React.Component {
  constructor(props) {
    super(props)

    this.fileUploadRef = React.createRef()
  }

  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    handleUpload: PropTypes.func.isRequired,
    uploadState: PropTypes.string,
    uploadError: PropTypes.string,
  }

  onUpload = (e) => {
    this.props.handleUpload(e.target.files[0])
    e.target.value = ''
  }

  handleUploadBtnClick = (e) => {
    this.fileUploadRef.current.click()
    e.preventDefault()
  }

  uploadMessage = () => {
    const { uploadState, uploadError } = this.props
    let uploadMessage = null

    if (uploadState === 'error') {
      uploadMessage = uploadError || translations.upload_error
    } else if (uploadState === 'success') {
      uploadMessage = translations.upload_succeeded
    } else if (uploadState === 'in_progress') {
      uploadMessage = translations.upload_in_progress
    }

    if (uploadMessage) {
      return <p className={`upload-message ${uploadState}`}>{uploadMessage}</p>
    }
  }

  render() {
    const sampleFilePath =
      '/api/backstage/publishing_administration/non_tunecore_compositions/bulk_upload_sample'
    const uploadInProgress = this.props.uploadState === 'in_progress'

    return (
      <div className="upload-compositions-modal">
        <Header
          composerName=""
          closeModal={this.props.closeModal}
          headerText={translations.upload_text}
        />

        <div className="modal-body">
          <div className="upload-text">
            <p>{translations.modal_text_paragraph_instruction}</p>
            <p>{translations.modal_text_paragraph_note}</p>
          </div>

          <div className="upload-link">
            <a href={sampleFilePath} target="_blank" rel="noreferrer">
              {translations.sample_file_name}
            </a>
          </div>

          <div className="upload-controls">
            <form
              onSubmit={this.handleSubmit}
              className="file-upload-form"
              id="bulk-ntc-upload"
            >
              <input
                className="hidden"
                type="file"
                accept=".csv, text/csv"
                onChange={this.onUpload}
                ref={this.fileUploadRef}
              />
            </form>

            <button
              className="upload-btn"
              disabled={uploadInProgress}
              onClick={this.handleUploadBtnClick}
            >
              {translations.upload_button_text}
            </button>

            {this.uploadMessage()}
          </div>
        </div>
      </div>
    )
  }
}

export default UploadCompositionsModal
