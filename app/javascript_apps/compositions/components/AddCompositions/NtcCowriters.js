import React from 'react'
import NtcCowriterTextField from './NtcCowriterTextField'
import NtcCowriterNameField from './NtcCowriterNameField'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const ListCowritersTooltip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.compositions.tooltips.list_cowriters_header}
    </TooltipTitle>
    <TooltipColumn>
      {window.translations.compositions.tooltips.list_cowriters_content}
    </TooltipColumn>
  </TooltipContainer>
)

const NtcCowriters = ({
  cowriters,
  handleCowriterChange,
  addNtcCowriter,
  removeNtcCowriter,
  btnDisabled,
  possibleCowriters,
}) => {
  const {
    cowriter_share,
    cowriters_paragraph,
    first_name,
    last_name,
    list_additional_cowriters,
    list_cowriters,
  } = window.translations.compositions.splits_modal

  const possibleFirstNames = possibleCowriters
    ? [
        ...new Set(
          Object.values(possibleCowriters).map(
            (cowriter) => cowriter.first_name
          )
        ),
      ]
    : []
  const possibleLastNames = possibleCowriters
    ? [
        ...new Set(
          Object.values(possibleCowriters).map((cowriter) => cowriter.last_name)
        ),
      ]
    : []

  return (
    <div className="cowriters-form">
      <div>
        <b>{list_cowriters}</b>&nbsp;
        <Tooltip>{ListCowritersTooltip}</Tooltip>
      </div>
      <p className="cowriters-paragraph">{cowriters_paragraph}</p>

      <div className="cowriter-row grid-x grid-padding-x">
        {cowriters.map((cowriter, index) => {
          return [
            <NtcCowriterNameField
              key={`first-name-${index}`}
              type="text"
              name="first_name"
              className="cell small-4"
              label={first_name}
              value={cowriter.first_name}
              onChange={(e) => handleCowriterChange(index, e)}
              errors={cowriter.errors}
              required={true}
              possibleNames={possibleFirstNames}
            />,

            <NtcCowriterNameField
              key={`last-name-${index}`}
              type="text"
              name="last_name"
              className="cell small-4"
              label={last_name}
              value={cowriter.last_name}
              onChange={(e) => handleCowriterChange(index, e)}
              errors={cowriter.errors}
              required={true}
              possibleNames={possibleLastNames}
            />,

            <NtcCowriterTextField
              key={`cowriter-share-${index}`}
              type="number"
              name="cowriter_share"
              className="cell small-3"
              label={cowriter_share}
              value={cowriter.cowriter_share}
              onChange={(e) => handleCowriterChange(index, e)}
              errors={cowriter.errors}
              required={true}
            />,

            <a
              key={`remove-cowriter-${index}`}
              className="remove-icon cell small-1"
              onClick={() => removeNtcCowriter(index)}
            >
              <i className="fa fa-times" />
            </a>,
          ]
        })}
      </div>

      <button
        className="list-additional-cowriters"
        onClick={addNtcCowriter}
        disabled={btnDisabled}
      >
        <b>
          <i className="fa fa-plus" /> {list_additional_cowriters}
        </b>
      </button>
    </div>
  )
}

export default NtcCowriters
