import React from 'react'
import Header from '../Modal/Header'
import Footer from '../Modal/Footer'
import NtcCowriters from './NtcCowriters'
import EntityData, { TextField, DateField, CheckBoxField } from '../Entity'

const {
  composition_note,
  composition_note_description,
} = window.translations.compositions
const translations = window.translations.compositions.add_compositions

const AddCompositionsModal = ({
  handleSubmit,
  composer,
  ntcComposition,
  handleCompositionChange,
  handleCowriterChange,
  addNtcCowriter,
  removeNtcCowriter,
  btnDisabled,
  closeModal,
  possibleCowriters,
  publicDomainFeatureEnabled,
}) => {
  return (
    <div className="add-compositions-modal">
      <Header
        composerName={composer.name}
        closeModal={closeModal}
        headerText={translations.add_composition_for}
      />

      <div className="add-compositions-form">
        <p className="enter-information">
          {translations.enter_information_about_a_work}
        </p>
        <p className="original-song-note">
          {composition_note} {composition_note_description}
        </p>
        <p className="register-another-song">
          {translations.register_another_song}
        </p>

        <EntityData source={ntcComposition} onChange={handleCompositionChange}>
          <TextField label="song_title" name="title" required={true} />
          <TextField
            label="performing_artist"
            name="performingArtist"
            required={true}
          />
          <TextField label="album_name" name="albumName" />
          <TextField label="record_label" name="recordLabel" />
          <TextField label="isrc_number" name="isrcNumber" maxLength={12} />
          <DateField label="release_date" name="releaseDate" />
          <TextField
            label="ownership_percentage"
            right_text="ownership_percentage_note"
            name="percent"
            type="number"
            required={true}
          />
          {publicDomainFeatureEnabled && (
            <CheckBoxField
              label="public_domain"
              description="public_domain_content"
              name="publicDomain"
            />
          )}
        </EntityData>
      </div>

      {ntcComposition.cowriters.length === 0 ? (
        <a href="#" className="list-cowriter-link" onClick={addNtcCowriter}>
          {window.translations.compositions.splits_modal.list_cowriters}
        </a>
      ) : (
        <NtcCowriters
          addNtcCowriter={addNtcCowriter}
          btnDisabled={btnDisabled}
          cowriters={ntcComposition.cowriters}
          handleCowriterChange={handleCowriterChange}
          removeNtcCowriter={removeNtcCowriter}
          possibleCowriters={possibleCowriters}
        />
      )}

      <Footer
        btnDisabled={btnDisabled}
        closeModal={closeModal}
        onSubmit={handleSubmit}
      />
    </div>
  )
}

export default AddCompositionsModal
