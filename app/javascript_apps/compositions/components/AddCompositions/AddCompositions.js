import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import AddCompositionsModal from './AddCompositionsModal'
import { isFieldPresent } from '../../../shared/fieldValidations'
import { hasCowriterErrors } from '../../utils/ntcCompositionValidations'
import ThankYouModal from './ThankYouModal'
import AddCompositionLink from './AddCompositionLink'
import {
  addNtcCowriter,
  updateNtcCowriter,
  removeNtcCowriter,
} from '../../actions/ntcCowriterActions'
import {
  updateNtcComposition,
  resetNtcComposition,
  submitNtcComposition,
} from '../../actions/ntcCompositionActions'

class AddCompositions extends React.Component {
  static propTypes = {
    composer: PropTypes.object.isRequired,
  }

  state = {
    openModal: null,
  }

  addNtcCowriter = () => {
    this.props.dispatch(addNtcCowriter())
  }

  closeModals = () => {
    this.setState({ openModal: null })
    this.props.dispatch(resetNtcComposition())
  }

  componentDidUpdate(prevProps) {
    let isNewNtcComp =
      prevProps.ntcComposition.uuid !== this.props.ntcComposition.uuid
    let modalName = this.state.openModal

    if (isNewNtcComp && modalName === 'add-compositions-modal') {
      this.setState({ openModal: 'thank-you-modal' })
    }
  }

  handleCompositionChange = ({ target: { name, value } }) => {
    this.props.dispatch(updateNtcComposition(name, value))
  }

  handleCowriterChange = (index, { target: { name, value } }) => {
    this.props.dispatch(updateNtcCowriter(name, value, index))
  }

  handleSubmit = () => {
    const { ntcComposition, composer, dispatch } = this.props

    if (this.isValid()) {
      dispatch(submitNtcComposition(ntcComposition, composer.id))
    }
  }

  hasCompositionErrors() {
    return Object.values(this.props.ntcComposition.errors).some(
      (error) => error
    )
  }

  isValid() {
    return (
      this.requiredFieldsPresent() &&
      !this.hasCompositionErrors() &&
      !hasCowriterErrors(this.props.ntcComposition.cowriters)
    )
  }

  openModal = (e, modal) => {
    this.setState({ openModal: modal })
  }

  removeNtcCowriter = (index) => {
    this.props.dispatch(removeNtcCowriter(index))
  }

  requiredFieldsPresent() {
    const { title, performingArtist, percent } = this.props.ntcComposition
    return [title, performingArtist, percent].every(isFieldPresent)
  }

  render() {
    const {
      composer,
      cowriters,
      publicDomainFeatureEnabled,
      ntcComposition,
    } = this.props
    let btnDisabled = !this.isValid()
    let composerIsTerminated = composer.status === 'terminated'

    return (
      <div>
        <AddCompositionLink
          openModal={this.openModal}
          composerIsTerminated={composerIsTerminated}
        />
        <Modal
          isOpen={this.state.openModal === 'add-compositions-modal'}
          onRequestClose={this.closeModals}
          contentLabel="Add Compositions Modal"
          overlayClassName="add-compositions-modal-overlay"
          className="add-compositions-modal"
        >
          <AddCompositionsModal
            addNtcCowriter={this.addNtcCowriter}
            btnDisabled={btnDisabled}
            closeModal={this.closeModals}
            composer={composer}
            handleCompositionChange={(e) => this.handleCompositionChange(e)}
            handleCowriterChange={this.handleCowriterChange}
            handleSubmit={this.handleSubmit}
            ntcComposition={ntcComposition}
            removeNtcCowriter={this.removeNtcCowriter}
            openModal={this.state.openModal}
            possibleCowriters={cowriters}
            publicDomainFeatureEnabled={publicDomainFeatureEnabled}
          />
        </Modal>
        <Modal
          isOpen={this.state.openModal === 'thank-you-modal'}
          onRequestClose={this.closeModals}
          contentLabel="Thank You Modal"
          overlayClassName="add-compositions-modal-overlay"
          className="thank-you-modal"
        >
          <ThankYouModal closeModal={this.closeModals} composer={composer} />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const { ntcComposition } = storeState
  return { ntcComposition }
}

export default connect(mapStateToProps)(AddCompositions)
