import React from 'react'
import ErrorMessage from './ErrorMessage'

const NtcCowriterTextField = ({
  type,
  name,
  className,
  label,
  value,
  onChange,
  errors,
  required = false,
}) => {
  const hasError = errors && errors[name]
  const isRequiredLabel = required ? `* ${label}` : label
  const klassName = hasError ? `${className} error-container` : className
  const translations = window.translations.compositions.validations
  const errorMsg = translations[name]

  return (
    <div className={klassName}>
      <label htmlFor={name}>{isRequiredLabel}</label>
      <input
        type={type}
        name={name}
        className={`${hasError ? 'error-field' : ''}`}
        maxLength="255"
        value={value || ''}
        onChange={onChange}
      />

      {hasError && <ErrorMessage>{errorMsg}</ErrorMessage>}
    </div>
  )
}

export default NtcCowriterTextField
