import React from 'react'

const Footer = ({ closeModal, translations }) => {
  return (
    <div className="footer grid-x">
      <div className="cell small-offset-6 small-6">
        <button className="cancel-btn" onClick={closeModal}>
          {translations.cancel}
        </button>
      </div>
    </div>
  )
}

export default Footer
