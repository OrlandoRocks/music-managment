import React from 'react'
import PropTypes from 'prop-types'
import AutocompleteField from '../../../shared/AutocompleteField'
import ErrorMessage from './ErrorMessage'

class NtcCowriterNameField extends React.Component {
  static propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    className: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired,
    required: PropTypes.bool,
  }

  handleChange = (e) => {
    this.props.onChange(e)
  }

  render() {
    const {
      errors,
      name,
      required,
      label,
      type,
      className,
      value,
      possibleNames,
    } = this.props

    const hasError = errors && errors[name]
    const isRequiredLabel = required ? `* ${label}` : label
    const klassName = hasError ? `${className} error-container` : className
    const translations = window.translations.compositions.validations
    const hasProvidedErrorMessage = typeof (errors && errors[name]) === 'string'
    const errorMsg = hasProvidedErrorMessage ? errors[name] : translations[name]

    return (
      <div className={klassName}>
        <label htmlFor={name}>{isRequiredLabel}</label>
        <AutocompleteField
          type={type}
          name={name}
          className={`cowriter-name-text-field`}
          value={value}
          onChange={this.handleChange}
          items={possibleNames}
        />
        {hasError && <ErrorMessage>{errorMsg}</ErrorMessage>}
      </div>
    )
  }
}

export default NtcCowriterNameField
