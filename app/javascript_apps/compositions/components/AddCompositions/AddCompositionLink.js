import React, { useContext } from 'react'
import FeatureContext from '../../context.js'

const addCompositionsText =
  window.translations.compositions.compositions_card.add_compositions

const addCompositionsLink = (ntcUrl, ntcCompositionFormEnabled, openModal) => {
  if (ntcCompositionFormEnabled) {
    return (
      <a
        href="#"
        className="add-compositions-link"
        onClick={(e) => openModal(e, 'add-compositions-modal')}
      >
        + &nbsp; {addCompositionsText}
      </a>
    )
  } else {
    return (
      <a
        href={ntcUrl}
        className="legacy-ntc-form-link"
        target="_blank"
        rel="noopener noreferrer"
      >
        + &nbsp; {addCompositionsText}
      </a>
    )
  }
}

export default function AddCompositionLink({
  openModal,
  composerIsTerminated,
}) {
  const { ntcUrl, ntcCompositionFormEnabled } = useContext(FeatureContext)

  return (
    <div>
      {!composerIsTerminated &&
        addCompositionsLink(ntcUrl, ntcCompositionFormEnabled, openModal)}
    </div>
  )
}
