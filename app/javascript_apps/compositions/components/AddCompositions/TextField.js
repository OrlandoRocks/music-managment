import React from 'react'
import ErrorMessage from './ErrorMessage'

const TextField = ({
  type,
  name,
  className,
  label,
  value,
  isRequired,
  onChange,
  errorMessage,
  maxLength = '255',
}) => {
  const isRequiredLabel = isRequired ? `* ${label}` : label
  const hasError = errorMessage && errorMessage.length >= 1

  return (
    <div className={`${hasError ? 'error-container' : ''} ${className}`}>
      <label htmlFor={name}>{isRequiredLabel}</label>
      <input
        type={type}
        name={name}
        className={hasError ? 'error-field' : ''}
        maxLength={maxLength}
        value={value || ''}
        onChange={onChange}
      />

      {hasError && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </div>
  )
}

export default TextField
