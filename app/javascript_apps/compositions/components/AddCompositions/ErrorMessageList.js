import React from 'react'
import ErrorMessage from './ErrorMessage'

const ErrorMessageList = ({ errors }) => {
  const translations = window.translations.compositions.validations

  return (
    <ul className="error-message-list grid-x">
      {errors.map((errorKey, index) => {
        return (
          <li key={`error-message-${index}`} className="cell small-12">
            <ErrorMessage>{translations[errorKey]}</ErrorMessage>
          </li>
        )
      })}
    </ul>
  )
}

export default ErrorMessageList
