import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import UploadCompositionsModal from './UploadCompositionsModal'
import axios from 'axios'

class UploadCompositions extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
      uploadedFile: null,
      uploadState: 'ready',
      uploadError: null,
    }
  }

  static propTypes = {
    composer: PropTypes.object.isRequired,
  }

  openModal = () => {
    this.setState({ isModalOpen: true })
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      uploadState: 'ready',
    })
  }

  uploadToAPI = () => {
    const url =
      '/api/backstage/publishing_administration/non_tunecore_compositions/bulk_upload'
    const { uploadedFile } = this.state

    const formData = new FormData()
    formData.append('composer_id', this.props.composer.id)
    formData.append('compositions_upload', uploadedFile, uploadedFile.name)

    const csrfToken = document.querySelector('[name=csrf-token]').content
    const options = {
      headers: {
        'X-CSRF-TOKEN': csrfToken,
      },
    }

    axios
      .post(url, formData, options)
      .then(this.handleSuccess)
      .catch(this.handleError)
  }

  handleSuccess = () => {
    this.setState({
      uploadState: 'success',
    })
  }

  handleError = (error) => {
    const response = error.response
    let uploadError = null

    if (response && response.status === 422) {
      uploadError = response.data.errors.publishing_compositions
    }

    this.setState({
      uploadState: 'error',
      uploadError,
    })
  }

  handleUpload = (file) => {
    this.setState(
      {
        uploadState: 'in_progress',
        uploadedFile: file,
      },
      this.uploadToAPI
    )
  }

  render() {
    const { isModalOpen, uploadState, uploadError } = this.state
    const bulkUploadText =
      window.translations.compositions.bulk_compositions.upload_text

    return (
      <div>
        <a
          href="#"
          className="upload-compositions-link"
          onClick={this.openModal}
        >
          + &nbsp; {bulkUploadText}
        </a>

        <Modal
          isOpen={isModalOpen}
          onRequestClose={this.closeModal}
          contentLabel="Bulk Upload Compositions"
          overlayClassName="upload-compositions-modal-overlay"
          className="upload-compositions-modal-wrapper"
        >
          <UploadCompositionsModal
            closeModal={this.closeModal}
            handleUpload={this.handleUpload}
            uploadState={uploadState}
            uploadError={uploadError}
          />
        </Modal>
      </div>
    )
  }
}

export default UploadCompositions
