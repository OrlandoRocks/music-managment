import React from 'react'

const Footer = ({ btnDisabled, closeModal, handleSubmit, translations }) => {
  return (
    <div className="ntc-modal-footer grid-x">
      <div className="cell small-6">
        <button className="cancel-btn" onClick={closeModal}>
          {translations.cancel}
        </button>
      </div>
      <div className="cell small-6">
        <button
          className="register-btn"
          onClick={handleSubmit}
          disabled={btnDisabled}
        >
          {translations.register}
        </button>
      </div>
    </div>
  )
}

export default Footer
