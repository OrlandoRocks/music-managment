import React from 'react'
import Header from '../Modal/Header'
import Footer from './ThankYou/Footer'
import replace from 'react-string-replace'

function collectRetroactivelyUrl() {
  const {
    paragraph_2,
    retroactively,
  } = window.translations.compositions.thank_you
  const helpLink =
    window.translations[
      'Does-TuneCore-Publishing-Administration-collect-retroactively'
    ]

  return replace(paragraph_2, '{{retroactively}}', (match, i) => (
    <a href={helpLink} key={i}>
      {retroactively}
    </a>
  ))
}

const ThankYouModal = ({ composer, closeModal }) => {
  const translations = window.translations.compositions.thank_you

  return (
    <div className="thank-you-message">
      <Header
        composerName={composer.name}
        closeModal={closeModal}
        translations={translations.header}
      />
      <strong>
        <p>{translations.headline}</p>
      </strong>
      <p>{translations.paragraph_1}</p>
      <p>{collectRetroactivelyUrl()}</p>
      <Footer closeModal={closeModal} translations={translations} />
    </div>
  )
}

export default ThankYouModal
