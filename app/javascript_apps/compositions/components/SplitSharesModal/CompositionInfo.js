import React from 'react'
import replace from 'react-string-replace'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'
import EntityData, { CheckBoxField } from '../Entity'

function yourWriterSharetoolTipUrl() {
  return replace(
    window.translations.compositions.tooltips.your_writer_share_content,
    '{{HERE}}',
    (match, i) => (
      <a
        href={
          window.translations[
            'What-are-splits-and-how-do-I-figure-out-my-split-'
          ]
        }
        key={i}
      >
        {window.translations.compositions.tooltips.here}
      </a>
    )
  )
}

const yourShareToolTip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.compositions.tooltips.your_writer_share_header}
    </TooltipTitle>
    <TooltipColumn>{yourWriterSharetoolTipUrl()}</TooltipColumn>
  </TooltipContainer>
)

const { composition_note_description } = window.translations.compositions
const {
  ownership_percentage_note,
} = window.translations.compositions.add_compositions

const CompositionInfo = ({
  composition,
  handleComposerShareChange,
  handleComposerPublicDomainChange,
  publicDomainFeatureEnabled,
  composerShare,
  translations,
}) => {
  const showError = composition.errors && composition.errors.composer_share

  return (
    <table className="publishing-splits-composition-info">
      <thead className="publishing-splits-composition-info-headers">
        <tr className="grid-x">
          <th className="cell small-4">{translations.composition_title}</th>
          <th className="cell small-4">{translations.appears_on}</th>
          <th className="cell small-4">
            <div>
              {translations.your_writer_share}&nbsp;
              <Tooltip>{yourShareToolTip}</Tooltip>
            </div>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr className="grid-x">
          <td className="cell small-4 comp-title">
            {composition.composition_title}
          </td>
          <td className="cell small-4">{composition.appears_on}</td>
          <td className={`cell small-4 ${showError ? 'error-container' : ''}`}>
            <input
              id="add_shares_percent_input"
              className="publishing-splits-composition-info-input"
              type="number"
              min="1"
              max="100"
              name="composer_share"
              onChange={handleComposerShareChange}
              placeholder="1-100 %"
              step="any"
              value={composerShare > 0 ? composerShare : ''}
            />
            {showError && (
              <span className="error-text">
                {window.translations.compositions.validations.cowriter_share}
              </span>
            )}
          </td>
        </tr>
        <tr>
          <td className="publishing-splits-notes">
            <p>{ownership_percentage_note}</p>
            <p>{composition_note_description}</p>
          </td>
        </tr>
        <tr>
          <EntityData
            source={composition}
            onChange={handleComposerPublicDomainChange}
          >
            {publicDomainFeatureEnabled && (
              <CheckBoxField
                label="public_domain"
                description="public_domain_content"
                name="public_domain"
              />
            )}
          </EntityData>
        </tr>
      </tbody>
    </table>
  )
}

export default CompositionInfo
