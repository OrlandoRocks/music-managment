import React from 'react'
import { containsNonLatinCharacters } from '../../../shared/fieldValidations'

const TranslatedName = ({
  composition,
  handleTranslatedNameChange,
  translations,
}) => {
  let showError = composition.errors && composition.errors.invalidTranslatedName

  return (
    <div className="cell small-8 publishing-splits-translated-name">
      {containsNonLatinCharacters(composition.composition_title) && (
        <div>
          <p>{translations.translations_latin_characters_only}</p>
          <div className={`${showError ? 'error-container' : ''}`}>
            <input
              type="text"
              name="translated_name"
              className="translated-name-input"
              onChange={handleTranslatedNameChange}
              value={composition.translated_name || ''}
            />

            {showError && (
              <span className="error-text">
                {window.translations.compositions.validations.translated_name}
              </span>
            )}
          </div>
        </div>
      )}
    </div>
  )
}

export default TranslatedName
