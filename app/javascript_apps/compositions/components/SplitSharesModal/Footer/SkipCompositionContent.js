import React from 'react'
import PropTypes from 'prop-types'

class SkipCompositionContent extends React.Component {
  static propTypes = {
    translations: PropTypes.object,
    resetPercentage: PropTypes.func.isRequired,
    handleSkipComposition: PropTypes.func.isRequired,
    toggleSkipContent: PropTypes.func.isRequired,
  }

  onSkipClick = () => {
    const {
      resetPercentage,
      handleSkipComposition,
      toggleSkipContent,
    } = this.props

    resetPercentage()
    handleSkipComposition()
    toggleSkipContent()
  }

  render() {
    const { translations, toggleSkipContent } = this.props

    return (
      <div className="grid-x">
        <div className="publishing-splits-footer-cannot-claim cell small-9">
          <b className="publishing-splits-warning-text">
            {translations.skip_prompt}
          </b>
        </div>

        <div className="publishing-splits-footer-btn-wrapper cell small-3 align-right">
          <b className="publishing-splits-footer-btn-wrapper-done">
            <a id="hide_composition_confirmation" onClick={this.onSkipClick}>
              {translations.skip}
            </a>
          </b>

          <button
            className="publishing-splits-footer-btn-wrapper-submit"
            id="register_and_continue"
            onClick={toggleSkipContent}
          >
            {translations.cancel}
          </button>
        </div>
      </div>
    )
  }
}

export default SkipCompositionContent
