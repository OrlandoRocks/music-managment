import React from 'react'
import PropTypes from 'prop-types'

class HideCompositionContent extends React.Component {
  static propTypes = {
    translations: PropTypes.object,
    resetPercentage: PropTypes.func.isRequired,
    toggleHideContent: PropTypes.func.isRequired,
    handleHideComposition: PropTypes.func.isRequired,
  }

  onHideClick = () => {
    const {
      resetPercentage,
      toggleHideContent,
      handleHideComposition,
    } = this.props

    resetPercentage()
    toggleHideContent()
    handleHideComposition()
  }

  render() {
    const { translations, toggleHideContent } = this.props

    return (
      <div className="grid-x">
        <div className="publishing-splits-footer-cannot-claim cell small-9">
          <b className="publishing-splits-warning-text">
            {translations.hide_prompt}
          </b>
        </div>

        <div className="publishing-splits-footer-btn-wrapper cell small-3 align-right">
          <b className="publishing-splits-footer-btn-wrapper-done">
            <a id="hide_composition_confirmation" onClick={this.onHideClick}>
              {translations.hide}
            </a>
          </b>

          <button
            className="publishing-splits-footer-btn-wrapper-submit"
            id="register_and_continue"
            onClick={toggleHideContent}
          >
            {translations.cancel}
          </button>
        </div>
      </div>
    )
  }
}

export default HideCompositionContent
