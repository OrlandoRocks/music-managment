import React from 'react'
import PropTypes from 'prop-types'

import HideCompositionContent from './HideCompositionContent'
import SkipCompositionContent from './SkipCompositionContent'
import FooterContent from './FooterContent'

class Footer extends React.Component {
  static propTypes = {
    handleSkipComposition: PropTypes.func.isRequired,
    handleHideComposition: PropTypes.func.isRequired,
    handleOnSubmit: PropTypes.func.isRequired,
    resetPercentage: PropTypes.func.isRequired,
    composerShare: PropTypes.number,
    translations: PropTypes.object,
    btnDisabled: PropTypes.bool,
  }

  state = {
    showSkipContent: false,
    showHideContent: false,
  }

  toggleSkipContent = () => {
    const { composerShare } = this.props
    const { showSkipContent } = this.state

    if (!composerShare && !showSkipContent) {
      this.props.handleSkipComposition()
    } else {
      this.setState({
        showSkipContent: !this.state.showSkipContent,
      })
    }
  }

  toggleHideContent = () => {
    this.setState({
      showHideContent: !this.state.showHideContent,
    })
  }

  contentGenerator = () => {
    const { showSkipContent, showHideContent } = this.state
    const {
      handleSkipComposition,
      handleHideComposition,
      handleOnSubmit,
      btnDisabled,
      translations,
      resetPercentage,
    } = this.props

    if (showSkipContent) {
      return (
        <SkipCompositionContent
          translations={translations}
          resetPercentage={resetPercentage}
          handleSkipComposition={handleSkipComposition}
          toggleSkipContent={this.toggleSkipContent}
        />
      )
    } else if (showHideContent) {
      return (
        <HideCompositionContent
          translations={translations}
          resetPercentage={resetPercentage}
          handleHideComposition={handleHideComposition}
          toggleHideContent={this.toggleHideContent}
        />
      )
    } else {
      return (
        <FooterContent
          translations={translations}
          btnDisabled={btnDisabled}
          handleOnSubmit={handleOnSubmit}
          toggleSkipContent={this.toggleSkipContent}
          toggleHideContent={this.toggleHideContent}
        />
      )
    }
  }

  render() {
    const { showSkipContent, showHideContent } = this.state

    const warning =
      showSkipContent || showHideContent ? 'publishing-splits-warning' : ''

    let content = this.contentGenerator()

    return (
      <div className={`publishing-splits-footer ${warning}`}>{content}</div>
    )
  }
}

export default Footer
