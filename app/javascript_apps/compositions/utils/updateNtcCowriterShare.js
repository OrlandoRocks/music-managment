export default function updateNtcCowriterShare(
  ntcCowriters,
  ntcCowriterIndex,
  cowriter_share
) {
  return ntcCowriters.reduce((acc, ntcCowriter, index) => {
    return [
      ...acc,
      index === ntcCowriterIndex
        ? { ...ntcCowriter, cowriter_share }
        : ntcCowriter,
    ]
  }, [])
}
