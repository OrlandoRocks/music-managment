export default function validateCowriter(cowriter) {
  const requiredCowriterFields = ['first_name', 'last_name', 'cowriter_share']

  let errors = requiredCowriterFields.reduce((errorsObj, attribute) => {
    return {
      ...errorsObj,
      [attribute]: cowriter[attribute].length === 0,
    }
  }, {})

  return {
    ...cowriter,
    errors,
  }
}
