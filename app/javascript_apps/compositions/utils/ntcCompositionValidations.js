import { updateNtcCowriterErrors } from '../actions/ntcCowriterActions'

export function hasCowriterErrors(cowriters) {
  return Object.values(cowriters).some((cowriter) => {
    if (!cowriter) return ''
    return Object.values(cowriter.errors).some((error) => error)
  })
}

export function resetCowriterShareError(cowriters, dispatch) {
  cowriters.map((cowriter, index) => {
    if (cowriter.errors.cowriter_share) {
      dispatch(
        updateNtcCowriterErrors(
          { ...cowriter.errors, cowriter_share: false },
          index
        )
      )
    }
  })
}
