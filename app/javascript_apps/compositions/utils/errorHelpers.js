import { camelCase } from 'lodash'

const empty = (val) => !Array.isArray(val) || val.length === 0

export function addErrorsToCowriters(cowriters, errors) {
  if (!errors) return cowriters

  const { cowriterErrors } = groupServerErrors(errors)

  if (empty(cowriterErrors)) return cowriters

  return cowriters.map((cowriter, cowriterIndex) => {
    // Ensures re-render
    const result = { ...cowriter }

    if (!empty(cowriterErrors[cowriterIndex])) {
      cowriterErrors[cowriterIndex].forEach(([field, errorMessage]) => {
        result.errors[field] = errorMessage
      })
    }

    return result
  })
}

// Separate server-validated composition errors from server-validated cowriter errors
export default function groupServerErrors(errors) {
  // eslint-disable-next-line no-unused-vars
  const { cowriterErrors, ...compositionErrors } = errors
  return { cowriterErrors, compositionErrors }
}

export function prepareCompositionErrors(errors) {
  const { compositionErrors } = groupServerErrors(errors)

  return Object.keys(compositionErrors).reduce((errors, errorKey) => {
    errors[camelCase(errorKey)] = compositionErrors[errorKey][0]
    return errors
  }, {})
}

const hasProvidedErrorMessage = (attribute, errors) =>
  typeof (errors && errors[attribute]) === 'string'

export const getErrorMessage = (attribute, errors, fallback) =>
  hasProvidedErrorMessage(attribute, errors) ? errors[attribute] : fallback
