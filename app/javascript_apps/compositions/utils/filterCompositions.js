export default function filterCompositions(compositions, searchValue) {
  if (searchValue.length <= 3) {
    return compositions
  }
  let filteredCompositions = compositionsContainingSearchValue(
    compositions,
    searchValue
  )

  return filteredCompositions.reduce((accumulator, composition) => {
    return {
      ...accumulator,
      [composition.id]: composition,
    }
  }, {})
}

function compositionsContainingSearchValue(compositions, searchValue) {
  return Object.values(compositions).filter(
    ({ composition_title, appears_on }) => {
      return [composition_title, appears_on]
        .map((field) => field.toLowerCase())
        .some((field) => field.includes(searchValue.trim().toLowerCase()))
    }
  )
}
