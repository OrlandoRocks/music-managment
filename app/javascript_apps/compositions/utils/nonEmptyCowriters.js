import nonEmptyCowriter from './nonEmptyCowriter'

export default function nonEmptyCowriters(cowriters) {
  return cowriters.filter((cowriter) => {
    return nonEmptyCowriter(cowriter)
  })
}
