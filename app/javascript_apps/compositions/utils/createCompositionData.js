import uuid from 'uuid/v4'

export function createNtcComposition() {
  return {
    uuid: uuid(),
    title: '',
    performingArtist: '',
    albumName: '',
    recordLabel: '',
    isrcNumber: '',
    releaseDate: '',
    percent: '',
    cowriters: [],
    errors: {},
  }
}

export function createCowriter() {
  return {
    first_name: '',
    last_name: '',
    cowriter_share: '',
    errors: {},
    uuid: uuid(),
  }
}
