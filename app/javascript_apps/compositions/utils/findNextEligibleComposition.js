import rotateArray from '../../shared/rotateArray'

export default function findNextEligibleComposition(
  compositions,
  currentComposition
) {
  const reorderedArray = reorderArray(compositions, currentComposition)
  const nextComposition = reorderedArray.find(
    (composition) => composition.status === 'shares_missing'
  )
  const nextCompositionId = nextComposition ? nextComposition.id : 0

  return { compositionId: nextCompositionId }
}

function reorderArray(compositions, currentComposition) {
  const compositionsArray = Object.values(compositions)
  const indexToRotateOn = compositionsArray.indexOf(currentComposition) + 1

  return rotateArray(compositionsArray, indexToRotateOn)
}
