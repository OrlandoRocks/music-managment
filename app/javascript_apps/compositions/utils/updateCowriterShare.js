export default function updateCowriterShare(
  composition,
  cowriters,
  uuid,
  value
) {
  const compositionCowriters = Object.values(cowriters)
    .filter((cowriter) => {
      return composition.cowriters.includes(cowriter.uuid)
    })
    .reduce((acc, cowriter) => {
      acc[cowriter.uuid] = cowriter
      return acc
    }, {})

  return {
    ...compositionCowriters,
    [uuid]: {
      ...compositionCowriters[uuid],
      cowriter_share: value,
    },
  }
}
