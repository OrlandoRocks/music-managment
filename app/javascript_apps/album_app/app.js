import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter as Router } from 'react-router-dom'

import AlbumContainer from './components/AlbumContainer'
import DistributionStepper from './components/DistributionStepper'

import { albumAppVariables, AlbumProvider } from './contexts/ConfigContext'
import { ESIDDataProvider } from './contexts/ESIDDataContext'
import { SnackbarProvider } from './contexts/SnackbarContext'
import { DistributionProvider } from './contexts/DistributionContext'
import MuiPickersUtilsProviderWrapper from './contexts/MuiPickersUtilsProviderWrapper'

import { ALBUM_APP_V2 } from './utils/constants'

import theme from './components/shared/theme'

const isV2 = albumAppVariables.features[ALBUM_APP_V2]
const Container = isV2 ? DistributionStepper : AlbumContainer

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Router>
      <AlbumProvider value={albumAppVariables}>
        <MuiPickersUtilsProviderWrapper>
          <ESIDDataProvider>
            <SnackbarProvider>
              <DistributionProvider>
                <Container />
              </DistributionProvider>
            </SnackbarProvider>
          </ESIDDataProvider>
        </MuiPickersUtilsProviderWrapper>
      </AlbumProvider>
    </Router>
  </MuiThemeProvider>,
  document.getElementById('album_app')
)
