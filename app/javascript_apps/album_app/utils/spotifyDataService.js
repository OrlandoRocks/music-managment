import {
  spotifyArtistSearchRequest,
  spotifyArtistRecentAlbums,
  spotifyFetchArtistRequest,
} from './spotifyAPIRequests'

export async function getSpotifySearchData(artistName) {
  try {
    const spotifySearchData = await spotifyArtistSearchRequest(artistName)
    const recentAlbums = await getRecentAlbums(spotifySearchData)

    return withRecentAlbums(spotifySearchData, recentAlbums)
  } catch (_) {
    return []
  }
}

const API_ERROR = { error: { status: 500 } }
export async function getSpotifyArtistData(id, name) {
  try {
    const artistP = spotifyFetchArtistRequest(id, name)
    const albumP = spotifyArtistRecentAlbums(id)
    const [artistData, albumData] = await Promise.all([artistP, albumP])

    if (artistData.error) {
      return artistData
    }

    if (!albumData) {
      return artistData
    }

    return withRecentAlbums(artistData, albumData)[0]
  } catch (_) {
    return API_ERROR
  }
}

async function getRecentAlbums(spotifySearchData) {
  const recentAlbumPromises = spotifySearchData.map((d) =>
    spotifyArtistRecentAlbums(d['id'])
  )
  return Promise.all(recentAlbumPromises)
}

function withRecentAlbums(artistData, recentAlbums) {
  return recentAlbums.map((data, i) => {
    const artist = Array.isArray(artistData) ? artistData[i] : artistData
    const {
      id,
      external_urls: { spotify: spotifyUrl },
      name: artistName,
    } = artist
    const image =
      artist.images &&
      artist.images[artist.images.length - 1] &&
      artist.images[artist.images.length - 1].url

    const currentAlbum = Array.isArray(data) ? data[0] : data
    const { name: latest_album } = currentAlbum

    return {
      id,
      image,
      latest_album,
      name: artistName,
      spotifyUrl,
    }
  })
}
