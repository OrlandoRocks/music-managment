import { SERVICES } from '../../utils/constants'

function parseAppleData(record) {
  const {
    id: identifier,
    attributes: { name },
    relationships: {
      topAlbum: {
        attributes: {
          artwork: { url: parameterizedImage },
          name: topAlbum,
        },
      },
    },
  } = record
  const image = parameterizedImage.replace('{w}', 64).replace('{h}', 64)

  return {
    identifier,
    image,
    name,
    topAlbum,
  }
}

function parseArtistData(data, serviceName) {
  switch (serviceName) {
    case SERVICES.spotify.name: {
      return parseSpotifyData(data)
    }
    case SERVICES.apple.name: {
      return parseAppleData(data)
    }
    default:
      break
  }
}

function parseSpotifyData(record) {
  const { id, image, name, latest_album } = record
  return { identifier: id, image, name, latest_album }
}

export { parseAppleData, parseArtistData, parseSpotifyData }
