import { tcFetch } from './fetch'
import { deepTrimObj } from '../../shared/deepTrimObj'

export function salepointRequest(album, values) {
  const method = 'post'
  const album_id = album.id || values.album_id
  const url = `/api/backstage/album_app/salepoints`

  const { stores, deliver_automator, apple_music } = values
  const store_ids = Object.keys(stores).filter(
    (store_id) => stores[store_id].selected === true
  )

  return tcFetch({
    url,
    body: deepTrimObj({
      store_ids: store_ids,
      album_id: album_id,
      deliver_automator: deliver_automator,
      apple_music: apple_music,
    }),
    method,
  })
}

export function parseErrors(errors) {
  return Object.entries(errors).reduce((acc, [k, v]) => {
    acc[k] = v[0]
    return acc
  }, {})
}
