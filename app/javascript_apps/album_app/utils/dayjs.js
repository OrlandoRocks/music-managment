import dayjs from 'dayjs'

import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import LocalizedFormat from 'dayjs/plugin/localizedFormat'

// NOTE: Import and initialize dayjs plugins here only.
// Init is called on app mount.
export function init() {
  dayjs.extend(isSameOrAfter)
  dayjs.extend(LocalizedFormat)
}

init()

export default dayjs
