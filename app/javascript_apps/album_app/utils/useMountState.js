import { useEffect } from 'react'

/**
 * Automatically updates any ref to false during component teardown (similar to the old `componentWillUnmount`).
 *
 * Uses a ref because, unlike state updates, ref mutations don't trigger renders,
 * which would cause the same problem we're solving: an async call that depends on a missing DOM node.
 *
 * Uses the built-in useEffect cleanup callback.
 *
 * Useful for guarding async calls that might only finish after unmount.
 *
 * Usage:
 *  - set a ref to true on mount, and pass it to this function.
 *  - use the ref as a guard for any async call that might only complete after unmount
 *
 * Example: see album_app/components/AlbumFormFields/MainArtistModal/ExternalId/ExternalIdManualEntry.js
 *
 * Docs: https://reactjs.org/docs/hooks-effect.html -- Effects With Cleanup
 */
export default function useMountState(ref) {
  useEffect(() => {
    return () => (ref.current = false)
  }, [ref])
}
