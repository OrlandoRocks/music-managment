import { SERVICES } from '../../album_app/utils/constants'
import guid from '../../shared/guid'

export default function buildAlbum(response) {
  const album =
    response || JSON.parse(document.getElementById('album_app').dataset.album)
  const primaryCreatives = formatAlbumCreatives(
    album.creatives.filter(primaries)
  )

  return { ...album, creatives: primaryCreatives }
}

const primaries = (c) => c.role == 'primary_artist'

function getServiceAttrs(serviceName, creative) {
  const result = creative.external_service_ids.find(
    (e) => e.service_name === serviceName
  )
  if (result) delete result.service_name
  return result || {}
}

function formatAlbumCreatives(creatives) {
  return creatives.reduce((acc, creative) => {
    const uuid = guid()
    const result = {
      ...creative,
      apple: getServiceAttrs(SERVICES.apple.name, creative),
      id: creative.id,
      name: creative.name,
      spotify: getServiceAttrs(SERVICES.spotify.name, creative),
      uuid,
    }
    delete result.external_service_ids

    acc[uuid] = result
    return acc
  }, {})
}
