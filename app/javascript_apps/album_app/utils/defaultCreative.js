import guid from '../../shared/guid'

export const defaultCreative = () => {
  return {
    apple: {},
    name: '',
    role: 'primary_artist',
    spotify: {},
    uuid: guid(),
  }
}
