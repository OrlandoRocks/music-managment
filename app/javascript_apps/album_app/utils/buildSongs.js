import guid from '../../shared/guid'

function buildSong(song) {
  const uuid = guid()

  const result = { ...song }
  result.uuid = uuid
  delete result.errors

  return result
}

function buildSongs(songs) {
  return songs.reduce((acc, s) => {
    const built = buildSong(s)
    acc[built.uuid] = built
    return acc
  }, {})
}

export { buildSong, buildSongs }
