import { useEffect } from 'react'
import produce from 'immer'

import { SONG_DATA_ENDPOINT } from './constants'
import { post, put } from './fetch'
import { isEmpty } from '../../utils'
import { getParam } from '../components/DistributionStepper/routerHelpers'
import { STEPS } from '../components/DistributionStepper/distributionStepperHelpers'

async function createSongP(values) {
  const body = { song: values }

  if (values.id) {
    return put(`${SONG_DATA_ENDPOINT}/${values.id}`, body)
  }
  return post(SONG_DATA_ENDPOINT, body)
}

function useSongSetter({
  location,
  fetchedSongsData,
  setSong,
  setState,
  song,
  songs,
  state,
}) {
  useEffect(() => {
    if (state.ready || !fetchedSongsData) return

    // TODO: remove
    const track = parseInt(getParam(location, 'track'), 10)
    if (!track) {
      setState({ ready: true })
    }

    if (isEmpty(songs)) return

    const correctSong = Object.values(songs).find(
      (s) => s.data.track_number === track
    )
    if (!correctSong) {
      // error...
      return
    }

    if (song.uuid !== correctSong.uuid) {
      setSong(correctSong)
    }
    setState({ ready: true })
  }, [location, fetchedSongsData, setState, setSong, song, songs, state.ready])
}

const BLANK_ROLE_IDS = []
const ALBUM_ASSOCIATION = 'Album'
const PRIMARY_ARTIST_ROLE = 'primary_artist'
function buildPrimaryArtist(c) {
  return {
    artist_name: c.name,
    role_ids: BLANK_ROLE_IDS,
    credit: PRIMARY_ARTIST_ROLE,
    creative_id: c.id,
    associated_to: ALBUM_ASSOCIATION,
  }
}

function getPrimariesForSong(album) {
  return Object.values(album.creatives)
    .filter((c) => c.role === PRIMARY_ARTIST_ROLE)
    .map((c) => buildPrimaryArtist(c))
}

function replaceSongData(song, data) {
  if (!data) return song

  return produce(song, (draft) => {
    draft.data = data
  })
}

const OUT_OF_SCOPE_FIELDS = {
  [STEPS.SONG_FORM_1]: ['clean_version', 'explicit', 'instrumental'],
  [STEPS.SONG_FORM_2]: ['clean_version', 'explicit', 'instrumental'],
}

function pruneOutOfScopeFields(rawValues, form) {
  const fields = OUT_OF_SCOPE_FIELDS[form]
  const result = { ...rawValues }
  fields.forEach((f) => {
    if (result[f] === null) delete result[f]
  })
  return result
}

export {
  OUT_OF_SCOPE_FIELDS,
  createSongP,
  getPrimariesForSong,
  pruneOutOfScopeFields,
  replaceSongData,
  useSongSetter,
}
