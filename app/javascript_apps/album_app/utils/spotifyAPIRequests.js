import optimizedFetch from './../../utils/optimizedFetch'

const SPOTIFY_ARTIST_SEARCH_URL =
  '/api/backstage/album_app/external_service_ids/spotify_search_artists'
const SPOTIFY_GET_ARTIST_PATH =
  '/api/backstage/album_app/external_service_ids/spotify_get_artist'
const SPOTIFY_RECENT_ALBUMS_PATH =
  '/api/backstage/album_app/external_service_ids/spotify_recent_albums/'

const CSRF_TOKEN = document.querySelector('[name=csrf-token]').content

const HEADERS = {
  Accept: 'application/json',
  'X-CSRF-TOKEN': CSRF_TOKEN,
}

export async function spotifyArtistSearchRequest(artist) {
  const response = await optimizedFetch(SPOTIFY_ARTIST_SEARCH_URL, {
    headers: HEADERS,
    searchParams: {
      name: artist,
    },
  })

  return response.json()
}

export async function spotifyArtistRecentAlbums(artist_id) {
  const response = await optimizedFetch(SPOTIFY_RECENT_ALBUMS_PATH, {
    headers: HEADERS,
    searchParams: {
      artist_id: artist_id,
    },
  })

  return response.json()
}

export async function spotifyFetchArtistRequest(id, name) {
  const response = await optimizedFetch(SPOTIFY_GET_ARTIST_PATH, {
    headers: HEADERS,
    searchParams: {
      id: id,
      name: name,
    },
  })

  return response.json()
}

export function parseErrors(errors) {
  return Object.entries(errors).reduce((acc, [k, v]) => {
    acc[k] = v[0]
    return acc
  }, {})
}
