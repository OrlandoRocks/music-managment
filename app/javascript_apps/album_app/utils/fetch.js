const csrfToken = document.querySelector('[name=csrf-token]').content
const HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'X-CSRF-TOKEN': csrfToken,
}

const DELETE = 'DELETE'
const POST = 'POST'
const PUT = 'PUT'
const GET = 'GET'

async function destroy(url, body) {
  return tcFetch({ url, body, method: DELETE })
}

async function post(url, body) {
  return tcFetch({ url, body, method: POST })
}

async function put(url, body) {
  return tcFetch({ url, body, method: PUT })
}

async function get(url, body) {
  return tcFetch({ url, body, method: GET })
}

function tcFetch({ url, body = {}, method }) {
  if (method == GET) {
    return fetch(url, {
      headers: HEADERS,
      method,
    })
  } else {
    return fetch(url, {
      headers: HEADERS,
      method,
      body: JSON.stringify(body),
    })
  }
}

export { destroy, post, put, get, tcFetch }
