import {
  spotifyArtistSearchRequest,
  spotifyArtistRecentAlbums,
} from './spotifyAPIRequests'

export async function getSpotifySearchData(artistName) {
  try {
    const spotifySearchData = await spotifyArtistSearchRequest(artistName)

    const recentAlbumPromises = spotifySearchData.map((d) =>
      spotifyArtistRecentAlbums(d['id'])
    )
    const recentAlbums = await Promise.all(recentAlbumPromises)

    return recentAlbums.map((albums, i) => {
      const artist = spotifySearchData[i]
      const artistName = artist.name
      const image =
        artist.images &&
        artist.images[artist.images.length - 1] &&
        artist.images[artist.images.length - 1].url

      const currentAlbum = albums[0]
      const {
        id,
        external_urls: { spotify: spotifyUrl },
        name: latestAlbum,
      } = currentAlbum

      return {
        id,
        image,
        latestAlbum,
        name: artistName,
        spotifyUrl,
      }
    })
  } catch (_) {
    return []
  }
}
