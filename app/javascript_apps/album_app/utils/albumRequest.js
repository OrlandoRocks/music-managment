import { apiPathSelector } from '../utils/apiPathSelector'
import { deepTrimObj } from '../../shared/deepTrimObj'
import { tcFetch } from './fetch'

export function albumRequest(album, specializedReleaseType, values) {
  const method = album.is_new ? 'post' : 'put'
  const id = album.id
  const url = apiPathSelector({ method, id, specializedReleaseType })

  return tcFetch({
    url,
    body: deepTrimObj({
      album: {
        ...values,
        specialized_release_type: specializedReleaseType,
        creatives: Object.values(values.creatives),
        selected_countries: values.selected_countries.join(','), // match the API's type expectation
      },
    }),
    method,
  })
}

export function parseErrors(errors) {
  return Object.entries(errors).reduce((acc, [k, v]) => {
    acc[k] = v[0]
    return acc
  }, {})
}
