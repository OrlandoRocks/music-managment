import { useEffect } from 'react'

// Get clipboard access level on mount
export default function useClipboard(cb) {
  useEffect(() => {
    async function hasClipboardAccess() {
      try {
        const permissions = await navigator.permissions.query({
          name: 'clipboard-read',
        })
        cb(permissions)
      } catch (e) {
        console.error(e)
      }
    }

    hasClipboardAccess()
  }, [cb])
}

const CLIPBOARD_ACCESS_STATES = ['granted', 'prompt']
export const hasPermission = (permissions) =>
  CLIPBOARD_ACCESS_STATES.includes(permissions.state)

export function tryReadFromClipboard() {
  return navigator.clipboard.readText()
}
