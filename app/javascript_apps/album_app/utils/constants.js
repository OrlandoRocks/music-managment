const IS_DEV = process.env.WEBPACK_DEV_SERVER

const ALBUM_APP_V2 = 'album_app_v2'

const ALBUM_TYPE = 'Album'
const SONG_TYPE = 'Song'

const FALLBACK_ARTIST_IMAGE = '/images/javascript_apps/avatar_default.png'

// The Unicode range for 4-byte chars
const FOUR_BYTE_REGEX = /[\u{10000}-\u{10FFFF}]/gu
const INDIA_COUNTRY_WEBSITE = 'IN'
const NEW_ARTIST_STATE = 'new_artist'
const REQUIRED_FIELD = 'required_field'
const SERVICES = {
  apple: {
    name: 'apple',
    titleCase: 'Apple',
  },
  spotify: {
    name: 'spotify',
    titleCase: 'Spotify',
  },
}
const SONG_DATA_ENDPOINT = '/api/backstage/song_data'
const START_TIME_STORE_NAMES_MAP = new Map([[87, 'TikTok']])
const SPECIALIZED_RELEASE_TYPES = ['facebook', 'discovery_platforms']

export {
  IS_DEV,
  ALBUM_APP_V2,
  ALBUM_TYPE,
  FALLBACK_ARTIST_IMAGE,
  FOUR_BYTE_REGEX,
  INDIA_COUNTRY_WEBSITE,
  NEW_ARTIST_STATE,
  REQUIRED_FIELD,
  SERVICES,
  SONG_DATA_ENDPOINT,
  SONG_TYPE,
  SPECIALIZED_RELEASE_TYPES,
  START_TIME_STORE_NAMES_MAP,
}
