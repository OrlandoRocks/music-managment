import { tcFetch } from './fetch'

export function distributionProgressRequest({ album, albumId }) {
  const method = 'GET'
  const album_id = albumId || album.id
  const url = `/api/backstage/album_app/distribution_progress?album_id=${album_id}`

  return tcFetch({
    url,
    method,
  })
}

export function parseErrors(errors) {
  return Object.entries(errors).reduce((acc, [k, v]) => {
    acc[k] = v[0]
    return acc
  }, {})
}
