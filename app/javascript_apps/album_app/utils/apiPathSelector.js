export const apiPathSelector = ({ method, id, specializedReleaseType }) => {
  if (specializedReleaseType == 'tiktok_release') {
    if (method == 'post') {
      return '/api/backstage/album_app/tiktok_releases'
    } else {
      return `/api/backstage/album_app/tiktok_releases/${id}`
    }
  }

  if (method == 'post') {
    return '/api/backstage/album_app/albums'
  } else {
    return `/api/backstage/album_app/albums/${id}`
  }
}
