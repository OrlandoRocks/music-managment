import optimizedFetch from './../../utils/optimizedFetch'

const APPLE_SEARCH_ARTISTS_PATH =
  '/api/backstage/album_app/external_service_ids/apple_search_artists'
const APPLE_GET_ARTIST_PATH =
  '/api/backstage/album_app/external_service_ids/apple_get_artist'

const CSRF_TOKEN = document.querySelector('[name=csrf-token]').content

const HEADERS = {
  Accept: 'application/json',
  'X-CSRF-TOKEN': CSRF_TOKEN,
}

export async function appleArtistSearchRequest(artist) {
  try {
    const response = await optimizedFetch(APPLE_SEARCH_ARTISTS_PATH, {
      headers: HEADERS,
      searchParams: {
        name: artist,
      },
    })

    const json = await response.json()

    return json
  } catch (_) {
    return []
  }
}

export async function appleFetchArtistRequest(id, name) {
  try {
    const response = await optimizedFetch(APPLE_GET_ARTIST_PATH, {
      headers: HEADERS,
      searchParams: {
        id: id,
        name: name,
      },
    })

    const json = await response.json()

    return json
  } catch (_) {
    return []
  }
}
