import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'

const MAMContextDefaults = {
  snackBarMsgs: [],
  hideStepActions: false,
}

export const MAMContext = React.createContext(MAMContextDefaults)
MAMContext.displayName = 'MAMContext'

export const MAMProvider = ({ children }) => {
  const [mamContextState, setMamContextState] = useState(MAMContextDefaults)

  function setMamContextVals(newVals = {}) {
    setMamContextState({ ...mamContextState, ...newVals })
  }

  return (
    <MAMContext.Provider value={{ mamContextState, setMamContextVals }}>
      {children}
    </MAMContext.Provider>
  )
}

MAMProvider.propTypes = {
  children: PropTypes.node,
}

export const useMAMContext = () => useContext(MAMContext)
