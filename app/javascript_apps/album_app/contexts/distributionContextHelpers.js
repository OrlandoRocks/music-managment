import { useEffect } from 'react'
import { buildSong, buildSongs } from '../utils/buildSongs'

function loadInvalidSong(songs, songsDeps) {
  const savedSong = Object.values(songs)[0]
  return savedSong || buildSong({ ...songsDeps.defaultSong })
}

function prepareSong(defaultSong, songs) {
  const trackNumber = Object.keys(songs).length + 1
  const result = buildSong({ ...defaultSong })
  result.data = { ...result.data, track_number: trackNumber }
  return result
}

// These attributes are JSON-encoded
const JSON_KEYS = ['default_song', 'songs', 'song_roles']

function parseSongsData(data) {
  Object.keys(data).forEach((k) => {
    if (!JSON_KEYS.includes(k)) {
      return
    }
    data[k] = JSON.parse(data[k])
  })
}

function songDataURLBuilder(album) {
  return `/api/backstage/song_data?album_id=${album.id}&person_id=${album.person_id}`
}

// In addition to songs, supporting data related to songs.
async function fetchSongsData(album, initSongsData) {
  try {
    const response = await fetch(songDataURLBuilder(album))
    const data = await response.json()
    parseSongsData(data)
    const result = {
      album,
      song: buildSong(data.default_song),
      songs: buildSongs(data.songs),
      songsDeps: {
        bigboxURL: data.bigbox_url,
        defaultSong: Object.freeze(data.default_song),
        songRoles: Object.freeze(data.song_roles),
        songwriterNames: data.songwriter_names.sort(),
        startTimeStores: data.start_time_stores,
        tcPid: data.tc_pid,
      },
    }
    initSongsData(result)

    return result
  } catch (e) {
    console.log(e)
  }
}

function useSongsData(album, fetchedSongsData, initSongsData) {
  useEffect(() => {
    async function runFetchSongsData() {
      fetchSongsData(album, initSongsData)
    }

    if (album.id && !fetchedSongsData) {
      runFetchSongsData()
    }
  }, [album, fetchedSongsData, initSongsData])
}

export { fetchSongsData, loadInvalidSong, prepareSong, useSongsData }
