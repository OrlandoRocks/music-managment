import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DayJsUtils from '@date-io/dayjs'

import { useConfig } from './ConfigContext'
import { init } from '../utils/dayjs'

import cs from 'dayjs/locale/cs'
import de from 'dayjs/locale/de'
import en from 'dayjs/locale/en'
import en_au from 'dayjs/locale/en-au'
import en_ca from 'dayjs/locale/en-ca'
import en_gb from 'dayjs/locale/en-gb'
import en_in from 'dayjs/locale/en-in'
import es_us from 'dayjs/locale/es-us'
import fr from 'dayjs/locale/fr'
import fr_ca from 'dayjs/locale/fr-ca'
import hu_us from 'dayjs/locale/hu'
import id_us from 'dayjs/locale/id'
import it from 'dayjs/locale/it'
import nl_us from 'dayjs/locale/nl'
import pl_us from 'dayjs/locale/pl'
import pt_us from 'dayjs/locale/pt'
import ro_us from 'dayjs/locale/ro'
import ru_us from 'dayjs/locale/ru'
import th_us from 'dayjs/locale/th'
import tr_us from 'dayjs/locale/tr'

const DAYJS_LOCALE_IMPORT_MAP = {
  cs,
  de,
  en,
  en_au,
  en_ca,
  en_gb,
  en_in,
  es_us,
  fr,
  fr_ca,
  hu_us,
  id_us,
  it,
  nl_us,
  pl_us,
  pt_us,
  ro_us,
  ru_us,
  th_us,
  tr_us,
}

export default function MuiPickersUtilsProviderWrapper({ children }) {
  const [locale, setLocale] = useState(null)
  const { dayjsLocale } = useConfig()[0]

  useEffect(() => {
    async function loadLocale() {
      // TODO: Code-splitting locale data is possible here after Webpacker or V2 migration.
      // const { default: locale } = await import(
      //   // These are the locales that TuneCore supports, for which there are Dayjs locale files.
      //   /* webpackInclude: /(de|en|en-au|en-ca|en-gb|en-in|es-us|fr|fr-ca|it)\.js/ */
      //   /* webpackChunkName: "dayjs-[request]" */ `dayjs/locale/${dayjsLocale}`
      // )

      setLocale(DAYJS_LOCALE_IMPORT_MAP[dayjsLocale])
      init()
    }
    loadLocale()
  }, [dayjsLocale])

  return (
    <MuiPickersUtilsProvider utils={DayJsUtils} locale={locale}>
      {children}
    </MuiPickersUtilsProvider>
  )
}

MuiPickersUtilsProviderWrapper.propTypes = {
  children: PropTypes.node,
}
