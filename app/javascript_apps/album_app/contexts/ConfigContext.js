import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'

import { devLogger } from '../../utils'
import { IS_DEV } from '../utils/constants'

export const albumAppVariables = mapVariablesToContext(getAlbumAppDataset())
export const ConfigContext = React.createContext([])
ConfigContext.displayName = 'ConfigContext'
export const useConfig = () => useContext(ConfigContext)

export const AlbumProvider = ({ children, value }) => {
  const [state, setState] = useState(value)

  return (
    <ConfigContext.Provider value={[state, setState]}>
      {children}
    </ConfigContext.Provider>
  )
}
AlbumProvider.propTypes = {
  children: PropTypes.node.isRequired,
  value: PropTypes.object.isRequired,
}

IS_DEV && devLogger(albumAppVariables, 'Initial Config Context')

function mapVariablesToContext(albumAppVariables) {
  const {
    activeArtists,
    artistUrlDisabled, // TODO: remove?
    countries,
    countryWebsite,
    dayjsLocale,
    features,
    genres,
    languageCodes,
    locale,
    pageTitle,
    person,
    plans,
    possibleArtists,
    releaseTypeVars,
    spotifyArtistsRequired,
  } = albumAppVariables

  return {
    activeArtists: safelyParse(activeArtists).sort(),
    artistUrlDisabled,
    countries: safelyParse(countries),
    countryWebsite,
    dayjsLocale,
    features: safelyParse(features),
    genres: safelyParse(genres),
    languageCodes: safelyParse(languageCodes),
    locale: toBCP47(locale),
    pageTitle: orEmptyString(pageTitle),
    person: safelyParse(person),
    plans: parsePlansToMap(plans),
    possibleArtists: safelyParse(possibleArtists),
    releaseTypeVars: safelyParse(releaseTypeVars),
    translations: getTranslations(),
    spotifyArtistsRequired: safelyParse(spotifyArtistsRequired),
  }
}

function getAlbumAppDataset() {
  return document.getElementById('album_app').dataset
}

function getTranslations() {
  return window.translations.album_app
}

function orEmptyString(attribute) {
  return attribute || ''
}

function safelyParse(jsonString) {
  try {
    return JSON.parse(jsonString)
  } catch (_) {
    return []
  }
}

function parsePlansToMap(plans) {
  const plansJSON = JSON.parse(plans)
  return new Map(Object.entries(plansJSON).map(([k, v]) => [Number(k), v]))
}

// Create an ECMA language tag (BCP47) from Rails locale
// List of tags https://r12a.github.io/app-subtags/
function toBCP47(locale) {
  let result = locale
  const parts = locale.split('-')
  if (parts.length > 1) {
    parts[1] = parts[1].toUpperCase()
    result = parts.join('-')
  }
  if (parts.length > 2) {
    console.error(`Unsupported locale: ${locale}.`)
  }
  return result
}
