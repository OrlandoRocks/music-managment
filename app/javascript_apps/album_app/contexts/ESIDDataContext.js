import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'

const ESIDDataContext = React.createContext(undefined)
ESIDDataContext.displayName = 'ESIDDataContext'
export const useESIDData = () => useContext(ESIDDataContext)

export const ESIDDataProvider = ({ children }) => {
  const [esidData, setESIDData] = useState({})

  return (
    <ESIDDataContext.Provider value={{ setESIDData, esidData }}>
      {children}
    </ESIDDataContext.Provider>
  )
}

ESIDDataProvider.propTypes = {
  children: PropTypes.node,
}
