import React, { useCallback, useState, useContext } from 'react'
import PropTypes from 'prop-types'
import produce from 'immer'

const SnackbarContext = React.createContext(undefined)
SnackbarContext.displayName = 'SnackbarContext'
export const useSnackbars = () => useContext(SnackbarContext)

const AddSnackbarContext = React.createContext(undefined)
AddSnackbarContext.displayName = 'AddSnackbarContext'
export const useAddSnackbar = () => useContext(AddSnackbarContext)

export const SnackbarProvider = ({ children }) => {
  const [state, setState] = useState([])

  const addSnackbar = useCallback(
    (snack) => {
      const newState = produce(state, (draft) => {
        draft.push(snack)
      })
      setState(newState)
    },
    [setState, state]
  )

  return (
    <SnackbarContext.Provider value={{ setState, state }}>
      <AddSnackbarContext.Provider value={addSnackbar}>
        {children}
      </AddSnackbarContext.Provider>
    </SnackbarContext.Provider>
  )
}

SnackbarProvider.propTypes = {
  children: PropTypes.node,
}
