import React, { useCallback, useContext, useReducer } from 'react'
import { useHistory } from 'react-router-dom'
import produce from 'immer'
import PropTypes from 'prop-types'

import { STEP_PARAMS } from '../components/DistributionStepper/distributionStepperHelpers'
import buildAlbum from '../utils/buildAlbum'
import { prepareSong } from './distributionContextHelpers'
import {
  getInitialStep,
  pushSongParams,
  setStepParam,
} from '../components/DistributionStepper/routerHelpers'

import { devLogger } from '../../utils'
import { IS_DEV } from '../utils/constants'
import { albumAppVariables } from './ConfigContext'

const INIT_SONGS_DATA = 'INIT_SONGS_DATA'
const ADD_SONG = 'ADD_SONG'
const DELETE_SONG = 'DELETE_SONG'
const EDIT_SONG = 'EDIT_SONG'
const HANDLE_RELEASE_SUBMISSION = 'HANDLE_RELEASE_SUBMISSION'
const RESTORE_SONG = 'RESTORE_SONG'
const SET_ACTIVE_STEP = 'SET_ACTIVE_STEP'
const SET_ALBUM = 'SET_ALBUM'
const SET_CONTINUE_DISABLED = 'SET_CONTINUE_DISABLED'
const SET_SONG = 'SET_SONG'
const SET_SONGS = 'SET_SONGS'
const SET_STORES = 'SET_STORES'
const SYNC_SONG_WITH_SONGS = 'SYNC_SONG_WITH_SONGS'
const SET_DISTRIBUTION_PROGRESS_LEVEL = 'SET_DISTRIBUTION_PROGRESS_LEVEL'
const SET_SONGS_COMPLETE = 'SET_SONGS_COMPLETE'
const SET_FREEMIUM_STORES = 'SET_FREEMIUM_STORES'
const SET_APPLE_MUSIC = 'SET_APPLE_MUSIC'
const SET_DELIVER_AUTOMATOR = 'SET_DELIVE_RAUTOMATOR'
const SET_FETCHING_PROGRESS_LEVELS = 'SET_FETCHING_PROGRESS_LEVELS'

function distributionReducer(draft, action) {
  IS_DEV && devLogger(action, 'Distribution Dispatch')

  switch (action.type) {
    case INIT_SONGS_DATA: {
      draft.fetchedSongsData = true
      draft.song = prepareSong(action.songsDeps.defaultSong, action.songs)
      draft.songs = action.songs
      draft.songsDeps = action.songsDeps
      break
    }
    case ADD_SONG: {
      draft.song = prepareSong(draft.songsDeps.defaultSong, draft.songs)
      break
    }
    case DELETE_SONG: {
      delete draft.songs[action.uuid]
      break
    }
    case EDIT_SONG: {
      draft.song = draft.songs[action.uuid]
      break
    }
    case HANDLE_RELEASE_SUBMISSION: {
      if (action.data) {
        const {
          album,
          distribution_progress_level,
          songs_complete,
        } = action.data

        draft.album = buildAlbum(album)
        draft.distributionProgressLevel = distribution_progress_level
        draft.songsComplete = songs_complete
      }
      break
    }
    case RESTORE_SONG: {
      const uuid = action.song.uuid
      draft.songs[uuid] = action.song
      break
    }
    case SET_ACTIVE_STEP: {
      draft.activeStep = action.activeStep
      break
    }
    case SET_ALBUM: {
      draft.album = buildAlbum(action.album)
      break
    }
    case SET_CONTINUE_DISABLED: {
      draft.continueDisabled = action.disabled
      break
    }
    case SET_SONG: {
      draft.song = action.song
      break
    }
    case SET_SONGS: {
      draft.songs = action.songs
      break
    }
    case SET_STORES: {
      draft.stores = action.stores
      break
    }
    case SET_FREEMIUM_STORES: {
      draft.freemiumStores = action.freemiumStores
      break
    }
    case SET_APPLE_MUSIC: {
      draft.appleMusic = action.appleMusic
      break
    }
    case SET_DELIVER_AUTOMATOR: {
      draft.deliverAutomator = action.deliverAutomator
      break
    }
    case SET_DISTRIBUTION_PROGRESS_LEVEL: {
      draft.distributionProgressLevel = action.distributionProgressLevel
      break
    }
    case SET_SONGS_COMPLETE: {
      draft.songsComplete = action.songsComplete
      break
    }
    case SYNC_SONG_WITH_SONGS: {
      draft.song = action.song
      draft.songs[action.song.uuid] = action.song
      break
    }
    case SET_FETCHING_PROGRESS_LEVELS: {
      draft.fetchingProgressLevels = action.fetchingProgressLevels
      break
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function useDistributionFlow() {
  const [state, dispatch] = useContext(DistributionContext)
  const history = useHistory()

  const initSongsData = useCallback(
    (data) => dispatch({ type: INIT_SONGS_DATA, ...data }),
    [dispatch]
  )

  const addSong = useCallback(() => {
    setStepParam(STEP_PARAMS.s1, history)
    dispatch({ type: ADD_SONG })
  }, [dispatch, history])

  const editSong = useCallback(
    (uuid, track) => {
      dispatch({ type: EDIT_SONG, uuid })
      pushSongParams(track, history)
    },
    [dispatch, history]
  )

  const handleReleaseSubmission = useCallback(
    (stepParam = null, data = null) => {
      setStepParam(stepParam, history)
      dispatch({ type: HANDLE_RELEASE_SUBMISSION, data })
    },
    [dispatch, history]
  )

  const deleteSong = useCallback(
    (uuid) => dispatch({ type: DELETE_SONG, uuid }),
    [dispatch]
  )
  const restoreSong = useCallback(
    (song) => dispatch({ type: RESTORE_SONG, song }),
    [dispatch]
  )
  const setActiveStep = useCallback(
    (activeStep) => dispatch({ type: SET_ACTIVE_STEP, activeStep }),
    [dispatch]
  )
  const setAlbum = useCallback(
    (album) => dispatch({ type: SET_ALBUM, album }),
    [dispatch]
  )
  const setContinueDisabled = useCallback(
    (disabled) => dispatch({ type: SET_CONTINUE_DISABLED, disabled }),
    [dispatch]
  )
  const setSong = useCallback((song) => dispatch({ type: SET_SONG, song }), [
    dispatch,
  ])
  const setSongs = useCallback(
    (songs) => dispatch({ type: SET_SONGS, songs }),
    [dispatch]
  )
  const setStores = useCallback(
    (stores) => dispatch({ type: SET_STORES, stores }),
    [dispatch]
  )
  const setFreemiumStores = useCallback(
    (freemiumStores) => dispatch({ type: SET_FREEMIUM_STORES, freemiumStores }),
    [dispatch]
  )
  const setAppleMusic = useCallback(
    (appleMusic) => dispatch({ type: SET_APPLE_MUSIC, appleMusic }),
    [dispatch]
  )
  const setDeliverAutomator = useCallback(
    (deliverAutomator) =>
      dispatch({ type: SET_DELIVER_AUTOMATOR, deliverAutomator }),
    [dispatch]
  )
  const setDistributionProgressLevel = useCallback(
    (distributionProgressLevel) =>
      dispatch({
        type: SET_DISTRIBUTION_PROGRESS_LEVEL,
        distributionProgressLevel,
      }),
    [dispatch]
  )
  const setSongsComplete = useCallback(
    (songsComplete) => dispatch({ type: SET_SONGS_COMPLETE, songsComplete }),
    [dispatch]
  )
  const syncSongWithSongs = useCallback(
    (song) => dispatch({ type: SYNC_SONG_WITH_SONGS, song }),
    [dispatch]
  )
  const setFetchingProgressLevels = useCallback(
    (fetchingProgressLevels) =>
      dispatch({ type: SET_FETCHING_PROGRESS_LEVELS, fetchingProgressLevels }),
    [dispatch]
  )

  return [
    state,
    {
      initSongsData,
      addSong,
      deleteSong,
      editSong,
      handleReleaseSubmission,
      history,
      restoreSong,
      setActiveStep,
      setAlbum,
      setContinueDisabled,
      setSong,
      setSongs,
      setStores,
      setFreemiumStores,
      setAppleMusic,
      setDeliverAutomator,
      setDistributionProgressLevel,
      setSongsComplete,
      syncSongWithSongs,
      setFetchingProgressLevels,
    },
  ]
}

const INITIAL_STATE = {
  activeStep: getInitialStep(),
  album: buildAlbum(),
  continueDisabled: false,
  fetchedSongsData: false,
  history: {},
  song: null,
  songs: {},
  songsDeps: {},
  stores: albumAppVariables.stores,
  freemiumStores: albumAppVariables.freemiumStores,
  deliverAutomator: albumAppVariables.deliverAutomator,
  appleMusic: albumAppVariables.appleMusic,
  distributionProgressLevel: albumAppVariables.distributionProgressLevel,
  songsComplete: albumAppVariables.songsComplete,
  fetchingProgressLevels: false,
}

const DistributionContext = React.createContext()
DistributionContext.displayName = 'DistributionContext'

const curriedReducerFunction = produce(distributionReducer)
const DistributionProvider = ({ children, mockInitialState }) => {
  const [state, dispatch] = useReducer(
    curriedReducerFunction,
    mockInitialState || INITIAL_STATE
  )

  const value = [state, dispatch]
  IS_DEV && devLogger(state, 'Distribution Context')

  return (
    <DistributionContext.Provider value={value}>
      {children}
    </DistributionContext.Provider>
  )
}

DistributionProvider.propTypes = {
  mockInitialState: PropTypes.object,
  children: PropTypes.node,
}

export { DistributionProvider, useDistributionFlow }
