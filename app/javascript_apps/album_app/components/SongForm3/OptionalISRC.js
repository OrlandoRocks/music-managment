import React from 'react'
import PropTypes from 'prop-types'

import FieldWrapper from '../shared/FieldWrapper'
import TextField from '../shared/TextField'

export default function OptionalISRC({
  handleChange,
  hasOptionalISRC,
  name,
  value,
}) {
  if (hasOptionalISRC === false) {
    return "No problem, we'll create an ISRC for you"
  }

  return (
    <FieldWrapper>
      <TextField
        id="optional-isrc"
        fullWidth
        label="Optional ISRC"
        name={name}
        onChange={handleChange}
        placeholder="ISRC #"
        rows={4}
        value={value}
        variant="outlined"
      />
    </FieldWrapper>
  )
}
OptionalISRC.propTypes = {
  handleChange: PropTypes.func.isRequired,
  hasOptionalISRC: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
}
