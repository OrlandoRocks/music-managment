import React from 'react'
import PropTypes from 'prop-types'
import dayjs from 'dayjs'
import { KeyboardDatePicker } from '@material-ui/pickers'
import { makeStyles } from '@material-ui/core/styles'

import InputLabel from '../shared/InputLabel'
import FieldWrapper from '../shared/FieldWrapper'
import { onlyDate } from '../../../utils'

const useStyles = makeStyles({
  datePickerContainer: {
    '& .react-datepicker-wrapper': {
      width: '100%',
    },
    '& .react-datepicker-popper': {
      zIndex: 2,
    },
  },
})

export default function PreviouslyReleased({
  hide,
  labelTranslation,
  name,
  setFieldValue,
  value,
}) {
  const classes = useStyles()

  const maxDate = dayjs().subtract(1, 'days').toDate()

  function handleChange(date) {
    setFieldValue(name, onlyDate(date))
  }

  if (hide) return null

  return (
    <FieldWrapper className={classes.datePickerContainer}>
      <InputLabel
        htmlFor={name}
        labelTranslation={labelTranslation}
        name={name}
      />

      <KeyboardDatePicker
        autoOk
        format="L"
        id={name}
        InputAdornmentProps={{ color: 'primary', position: 'end' }}
        inputVariant="outlined"
        maxDate={maxDate}
        onChange={handleChange}
        ToolbarComponent={() => null}
        value={value}
      />
    </FieldWrapper>
  )
}
PreviouslyReleased.propTypes = {
  hide: PropTypes.bool.isRequired,
  labelTranslation: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  value: PropTypes.string,
}
