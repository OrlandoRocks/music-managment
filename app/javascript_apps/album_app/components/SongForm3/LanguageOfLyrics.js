import React from 'react'
import PropTypes from 'prop-types'

import NativeSelect from '../shared/NativeSelect'

import { useConfig } from '../../contexts/ConfigContext'

export default function LanguageOfLyrics({ handleChange, hide, name, value }) {
  const { languageCodes } = useConfig()[0]
  const sortedLanguageOptions = () =>
    [...languageCodes]
      .sort((a, b) => a.description.localeCompare(b.description))
      .map((l) => (
        <option key={l.id} value={l.id}>
          {l.description}
        </option>
      ))

  if (hide) return null

  return (
    <NativeSelect
      handleChange={handleChange}
      labelTranslation="Language of Lyrics*"
      name={name}
      options={sortedLanguageOptions()}
      value={value}
    />
  )
}
LanguageOfLyrics.propTypes = {
  handleChange: PropTypes.func.isRequired,
  hide: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
}
