import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'

import { IconButton, InputAdornment } from '@material-ui/core'
import AssignmentIcon from '@material-ui/icons/Assignment'

import TextField from '../shared/TextField'
import FieldWrapper from '../shared/FieldWrapper'

import useClipboard, {
  hasPermission,
  tryReadFromClipboard,
} from '../../utils/useClipboard'

export default function Lyrics({
  handleChange,
  hide,
  name,
  setFieldValue,
  value,
}) {
  const [showPasteBtn, setShowPasteBtn] = useState(false)

  const useClipboardCallback = (permissions) =>
    setShowPasteBtn(hasPermission(permissions))
  useClipboard(useClipboardCallback)

  async function handlePaste() {
    try {
      const clipboardContent = await tryReadFromClipboard()
      setFieldValue(name, clipboardContent)
    } catch (e) {
      console.error(e)
    }
  }

  const inputEl = useRef(null)

  if (hide) return null

  return (
    <FieldWrapper>
      <TextField
        id="lyrics"
        fullWidth
        inputRef={inputEl}
        InputProps={{
          endAdornment: showPasteBtn ? (
            <InputAdornment position="end">
              <IconButton onClick={handlePaste}>
                <AssignmentIcon />
              </IconButton>
            </InputAdornment>
          ) : null,
        }}
        label="Lyrics"
        multiline
        name={name}
        onChange={handleChange}
        placeholder="Paste Your Lyrics"
        rows={4}
        value={value}
        variant="outlined"
      />
    </FieldWrapper>
  )
}
Lyrics.propTypes = {
  handleChange: PropTypes.func.isRequired,
  hide: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  value: PropTypes.string,
}
