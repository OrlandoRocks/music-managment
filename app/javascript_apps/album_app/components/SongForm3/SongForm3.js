import React, { useState } from 'react'
import { Form, Formik } from 'formik'
import { useHistory, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'

import Skeleton from '@material-ui/lab/Skeleton'

import LanguageOfLyrics from './LanguageOfLyrics'
import Lyrics from './Lyrics'

import Submit from '../shared/Submit'
import ErrorScroller from '../shared/ErrorScroller'
import Spacer from '../../../shared/Spacer'
import PrimaryHeadline from '../shared/PrimaryHeadline'
import TrackInfo from '../shared/TrackInfo'
import RadioButtons from '../shared/RadioButtons'
import OptionalISRC from './OptionalISRC'

import { devLogger } from '../../../utils'
import { IS_DEV } from '../../utils/constants'
import { useSongSetter } from '../../utils/songDataHelpers'
import { prepareInitialValues, songSubmitHandler } from './songForm3Helpers'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import { useConfig } from '../../contexts/ConfigContext'
import PreviouslyReleased from './PreviouslyReleased'
import SongStartTimes from './SongStartTimes'

import ErrorMessage from '../shared/ErrorMessage'
import createSongSchema, { SCHEMAS } from '../shared/songSchema'
import { skipOrContinueSF } from '../shared/songHelpers'

const FIELDS = {
  clean_version: 'clean_version',
  explicit: 'explicit',
  hasOptionalISRC: 'hasOptionalISRC',
  instrumental: 'instrumental',
  lyrics: 'lyrics',
  language_code_id: 'language_code_id',
  optional_isrc: 'optional_isrc',
  previously_released_at: 'previously_released_at',
  previouslyReleased: 'previouslyReleased',
  song_start_times: 'song_start_times',
}
Object.freeze(FIELDS)

export default function SongForm3({ songSubmitRef, submitHandler }) {
  const [
    {
      activeStep,
      fetchedSongsData,
      song,
      songs,
      songsDeps: { startTimeStores },
    },
    distributionActions,
  ] = useDistributionFlow()
  const { setSong } = distributionActions

  const history = useHistory()
  const location = useLocation()
  const config = useConfig()[0]

  const [state, setState] = useState({
    manualErrorScrollTrigger: 0,
  })

  // ensure the correct song is loaded for the form
  useSongSetter({
    location,
    fetchedSongsData,
    setSong,
    setState,
    song,
    songs,
    state,
  })

  // Don't render the form until ready, because it doesn't have a song yet.
  if (!state.ready) {
    const height = 60

    return (
      <>
        <PrimaryHeadline
          Element="h1"
          size="1.6em"
          translation="Tell Us More About This Track"
        />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <br />
        <br />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <Skeleton height={height} />
      </>
    )
  }

  const SongForm3Schema = createSongSchema(config, SCHEMAS.SF3)

  const { asset_filename, name: songName } = song.data

  // Form is initialized from context/state, but thereafter form values are not syndicated to state
  // until submission.
  return (
    <div className="grid-container div-album-form">
      <Formik
        initialValues={prepareInitialValues(song, startTimeStores)}
        onSubmit={async (values, actions) => {
          await submitHandler({
            actions,
            distributionActions,
            history,
            song,
            values,
          })
        }}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={SongForm3Schema}
      >
        {(formikBag) => {
          const {
            errors,
            handleChange,
            isSubmitting,
            setFieldValue,
            values,
          } = formikBag

          IS_DEV && devLogger(errors, 'Song Form 3 Errors')
          IS_DEV && devLogger(values, 'Song Form 3 Values')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            skipOrContinueSF({
              activeStep,
              config,
              formikBag,
              history,
              songs,
              setState,
            })
          }

          const {
            explicit,
            hasOptionalISRC,
            instrumental,
            previouslyReleased,
          } = values

          const isInstrumental = Boolean(instrumental)
          const hideClean =
            explicit === null || explicit === true || instrumental
          const hidePreviouslyReleasedAt = !previouslyReleased

          return (
            <Form data-testid="SongForm3" onSubmit={wrappedHandleSubmit}>
              <fieldset>
                <ErrorScroller
                  errors={errors}
                  trigger={state.manualErrorScrollTrigger}
                />
                <PrimaryHeadline
                  Element="h1"
                  size="1.6em"
                  translation="Tell Us More About This Track"
                />
                <TrackInfo file={asset_filename} name={songName} />
                <Spacer height={20} />

                <ErrorMessage
                  containerStyle={styles.errorStyle}
                  error={errors[FIELDS.instrumental]}
                  touched={true}
                />
                <RadioButtons
                  hide={false}
                  name={FIELDS.instrumental}
                  setFieldValue={setFieldValue}
                  translation="Is this track instrumental?"
                  value={values[FIELDS.instrumental]}
                />

                <ErrorMessage
                  containerStyle={styles.errorStyle}
                  error={errors[FIELDS.explicit]}
                  touched={true}
                />
                <RadioButtons
                  hide={isInstrumental}
                  name={FIELDS.explicit}
                  setFieldValue={setFieldValue}
                  translation="Does this track have explicit lyrics?"
                  value={values[FIELDS.explicit]}
                />

                <ErrorMessage
                  containerStyle={styles.errorStyle}
                  error={errors[FIELDS.clean_version]}
                  touched={values[FIELDS.explicit] !== null && true}
                />
                <RadioButtons
                  hide={hideClean}
                  name={FIELDS.clean_version}
                  setFieldValue={setFieldValue}
                  translation="Is there an explicit version of this song?"
                  value={values[FIELDS.clean_version]}
                />

                <ErrorMessage
                  containerStyle={styles.errorStyle}
                  error={errors[FIELDS.language_code_id]}
                  touched={true}
                />
                <LanguageOfLyrics
                  handleChange={handleChange}
                  hide={isInstrumental}
                  name={FIELDS.language_code_id}
                  value={values[FIELDS.language_code_id]}
                />

                <Lyrics
                  handleChange={handleChange}
                  hide={isInstrumental}
                  name={FIELDS.lyrics}
                  setFieldValue={setFieldValue}
                  value={values[FIELDS.lyrics] || ''}
                />

                <RadioButtons
                  name={FIELDS.previouslyReleased}
                  setFieldValue={setFieldValue}
                  translation="Has this track been previously released?"
                  value={values[FIELDS.previouslyReleased]}
                />
                <PreviouslyReleased
                  hide={hidePreviouslyReleasedAt}
                  labelTranslation="Previous Release Date"
                  name={FIELDS.previously_released_at}
                  setFieldValue={setFieldValue}
                  value={values[FIELDS.previously_released_at]}
                />

                <RadioButtons
                  name={FIELDS.hasOptionalISRC}
                  setFieldValue={setFieldValue}
                  translation="Do you already have an ISRC code for this track?"
                  value={values[FIELDS.hasOptionalISRC]}
                />
                <OptionalISRC
                  handleChange={handleChange}
                  hasOptionalISRC={hasOptionalISRC}
                  name={FIELDS.optional_isrc}
                  value={values[FIELDS.optional_isrc]}
                />

                <SongStartTimes
                  handleChange={handleChange}
                  values={values[FIELDS.song_start_times]}
                />

                <div style={styles.submitContainer}>
                  <Submit
                    customRef={songSubmitRef}
                    isSubmitting={isSubmitting}
                    // translationsKey={}
                  />
                </div>
              </fieldset>
              <Spacer />
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
SongForm3.defaultProps = {
  submitHandler: songSubmitHandler,
}
SongForm3.propTypes = {
  songSubmitRef: PropTypes.object.isRequired,
  submitHandler: PropTypes.func.isRequired,
}

const styles = {
  submitContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    margin: '2em',
  },
}
