import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import NativeSelect from '../shared/NativeSelect'
import FakeLabel from '../shared/FakeLabel'

import { START_TIME_STORE_NAMES_MAP } from '../../utils/constants'

const MINUTES = [...Array(10).keys()].map((m, i) => (
  <option key={i} value={m}>
    {m.toString().padStart(2, '0')}
  </option>
))
const SECONDS = [...Array(60).keys()].map((s, i) => (
  <option key={i} value={s}>
    {s.toString().padStart(2, '0')}
  </option>
))

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    marginTop: '-.25em',

    '& div': {
      marginRight: '1em',
      width: '200px',
    },
  },
}))

export default function SongStartTimes({ handleChange, values }) {
  const classes = useStyles()

  return values.map((t, i) => {
    const { minutes, seconds, store_id } = t
    const store = START_TIME_STORE_NAMES_MAP.get(store_id)

    return (
      <React.Fragment key={store_id}>
        <FakeLabel translation={`${store} Clip Start Time`} />
        <div className={classes.container}>
          <NativeSelect
            aria-label={`${store} Clip Start Time - Minutes`}
            handleChange={handleChange}
            labelTranslation="Minutes"
            name={`song_start_times.${i}.minutes`}
            options={MINUTES}
            showBlank={false}
            value={minutes}
          />
          <NativeSelect
            aria-label={`${store} Clip Start Time - Seconds`}
            handleChange={handleChange}
            labelTranslation="Seconds"
            name={`song_start_times.${i}.seconds`}
            options={SECONDS}
            showBlank={false}
            value={seconds}
          />
        </div>
      </React.Fragment>
    )
  })
}
SongStartTimes.propTypes = {
  handleChange: PropTypes.func.isRequired,
  values: PropTypes.array,
}
