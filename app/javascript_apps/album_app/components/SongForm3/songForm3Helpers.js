import produce from 'immer'

import { parseErrors } from '../../utils/albumRequest'
import { distributionProgressRequest } from '../../utils/distributionProgressRequest'
import { createSongP, replaceSongData } from '../../utils/songDataHelpers'
import { createParams } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'

function prepareSongStartTimes(startTimeStores, song_start_times) {
  return startTimeStores.map((s) => {
    const data = song_start_times.find((d) => d.store_id === s)
    if (data) {
      const seconds = data.start_time % 60
      const minutes = (data.start_time - seconds) / 60
      return { store_id: data.store_id, minutes, seconds }
    }

    return { store_id: s, minutes: 0, seconds: 0 }
  })
}

function prepareInitialValues(song, startTimeStores) {
  return {
    ...song.data,
    hasOptionalISRC: Boolean(song.data.optional_isrc),
    language_code_id: song.data.language_code_id?.toString(),
    previouslyReleased: Boolean(song.data.previously_released_at),
    song_start_times: prepareSongStartTimes(
      startTimeStores,
      song.data.song_start_times
    ),
  }
}

function controlCleanVersion({ clean_version, explicit, instrumental }) {
  if (instrumental || explicit) {
    return false
  }

  return clean_version
}

function controlOptionalISRC({ hasOptionalISRC, optional_isrc }) {
  if (!hasOptionalISRC) return null // nullifies optional_isrc

  return optional_isrc
}

function controlPreviouslyReleasedAt({
  previously_released_at,
  previouslyReleased,
}) {
  if (previouslyReleased) return previously_released_at

  return null // nullifies previously_released_at
}

function parseSongStartTimes(song_start_times) {
  return song_start_times.map((t) => {
    const minutes = t.minutes || 0
    const seconds = t.seconds || 0
    const totalSeconds = minutes * 60 + seconds
    return { store_id: t.store_id, start_time: totalSeconds }
  })
}

function parseValues(values) {
  return produce(values, (draft) => {
    const {
      clean_version,
      explicit,
      hasOptionalISRC,
      instrumental,
      optional_isrc,
      previouslyReleased,
      previously_released_at,
      song_start_times,
    } = draft

    draft.clean_version = controlCleanVersion({
      clean_version,
      explicit,
      instrumental,
    })

    draft.optional_isrc = controlOptionalISRC({
      hasOptionalISRC,
      optional_isrc,
    })

    draft.previously_released_at = controlPreviouslyReleasedAt({
      previouslyReleased,
      previously_released_at,
    })

    draft.song_start_times = parseSongStartTimes(song_start_times)

    delete draft.hasOptionalISRC
    delete draft.previouslyReleased
  })
}

async function songSubmitHandler(form) {
  const { actions, distributionActions, history, song, values } = form
  const {
    setContinueDisabled,
    syncSongWithSongs,
    setDistributionProgressLevel,
    setSongsComplete,
    setFetchingProgressLevels,
  } = distributionActions

  setFetchingProgressLevels(true)
  setContinueDisabled(true)

  // No optimistic UI this time, wait for server validation (TODO: replicate all server checks?)

  const parsedValues = parseValues(values)
  const songRes = await createSongP(parsedValues)
  const { errors, data } = await songRes.json()

  if (!songRes.ok) {
    actions.setErrors(parseErrors(errors))
    return
  }

  const newSong = replaceSongData(song, data)
  syncSongWithSongs(newSong)
  setContinueDisabled(false)

  const distributionProgressResponse = await distributionProgressRequest({
    albumId: data.album_id,
  })
  const distributionProgressData = await distributionProgressResponse.json()

  if (distributionProgressResponse.ok) {
    const {
      distribution_progress_level,
      songs_complete,
    } = distributionProgressData

    setDistributionProgressLevel(distribution_progress_level)
    setSongsComplete(songs_complete)

    setFetchingProgressLevels(false)
  }

  history.push({ search: createParams({ step: STEP_PARAMS.a2 }) })
}

export {
  parseValues,
  prepareInitialValues,
  prepareSongStartTimes,
  songSubmitHandler,
}
