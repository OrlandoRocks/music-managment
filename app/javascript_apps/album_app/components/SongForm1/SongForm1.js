import React, { useState } from 'react'
import { Form, Formik } from 'formik'
import { useHistory, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'

import Skeleton from '@material-ui/lab/Skeleton'

import SongName from './Name'
import FileSelect from './FileSelect'
import Submit from '../shared/Submit'
import Uploader from './Uploader'
import ErrorMessage, { errorStyle } from '../shared/ErrorMessage'
import ErrorScroller from '../shared/ErrorScroller'

import { prepareInitialValues, songSubmitHandler } from './songForm1Helpers'
import { devLogger } from '../../../utils'
import { useSongSetter } from '../../utils/songDataHelpers'
import { IS_DEV } from '../../utils/constants'

import { useConfig } from '../../contexts/ConfigContext'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import PrimaryHeadline from '../shared/PrimaryHeadline'
import createSongSchema, { SCHEMAS } from '../shared/songSchema'
import { skipOrContinueSF } from '../shared/songHelpers'

const FIELD_NAMES = {
  asset_filename: 'asset_filename',
  name: 'name',
}
Object.freeze(FIELD_NAMES)

const INITIAL_STATE = {
  file: null,
  manualErrorScrollTrigger: 0,
  progress: 0,
  ready: false,
  registered: false,
  showUploader: false,
  submitSuccess: false,
  upload: null,
}

export default function SongForm1({ songSubmitRef, submitHandler }) {
  const config = useConfig()[0]
  const location = useLocation()
  const history = useHistory()

  const [
    { activeStep, album, fetchedSongsData, song, songs, songsDeps },
    { setContinueDisabled, setSong, syncSongWithSongs },
  ] = useDistributionFlow()

  const [state, _setState] = useState(INITIAL_STATE)
  function setState(obj) {
    _setState((prev) => ({ ...prev, ...obj }))
  }

  // Ensure the correct song is loaded for the form
  useSongSetter({
    location,
    fetchedSongsData,
    setSong,
    setState,
    song,
    songs,
    state,
  })

  // On abort, form state is handled in the actual XHR abort handler
  function handleUploadAbort() {
    state.upload.abort()
    setState(INITIAL_STATE)
  }

  // Locals

  // Don't render the form until ready, because it doesn't have a song yet.
  if (!state.ready) {
    const height = 60
    const width = 300

    return (
      <>
        <PrimaryHeadline
          Element="h1"
          size="1.6em"
          translation="Let's Upload A Track"
        />
        <Skeleton height={height} />
        <Skeleton height={height} width={width} />
      </>
    )
  }

  const initialValues = prepareInitialValues(song.data, album)
  const SongForm1Schema = createSongSchema(config, SCHEMAS.SF1)

  return (
    <div className="grid-container div-album-form">
      <Formik
        initialValues={initialValues}
        onSubmit={async (values, actions) => {
          await submitHandler({
            actions,
            file: state.file,
            history,
            setContinueDisabled,
            setState,
            song,
            songsDeps,
            syncSongWithSongs,
            values,
          })
        }}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={SongForm1Schema}
      >
        {(formikBag) => {
          const {
            errors,
            handleBlur,
            handleChange,
            isSubmitting,
            setFieldValue,
            values,
          } = formikBag

          IS_DEV && devLogger(errors, 'Song Form 1 Errors')
          IS_DEV && devLogger(values, 'Song Form 1')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            skipOrContinueSF({
              activeStep,
              config,
              file: state.file,
              formikBag,
              history,
              songs,
              setState,
            })
          }

          if (state.showUploader) {
            return (
              <Uploader
                file={state.file}
                handleUploadAbort={handleUploadAbort}
                name={values.name}
                progress={state.progress}
                registered={state.registered}
              />
            )
          }

          return (
            <Form data-testid="SongForm1" onSubmit={wrappedHandleSubmit}>
              <fieldset disabled={state.submitSuccess}>
                <ErrorScroller
                  errors={errors}
                  trigger={state.manualErrorScrollTrigger}
                />
                <PrimaryHeadline
                  Element="h1"
                  size="1.6em"
                  translation="Let's Upload A Track"
                />

                <ErrorMessage error={errors[FIELD_NAMES.name]} touched={true} />
                <SongName
                  autoComplete="false"
                  defaultValue={values.name}
                  error={Boolean(errors[FIELD_NAMES.name])}
                  fullWidth
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  name={FIELD_NAMES.name}
                />
                <ErrorMessage
                  containerStyle={errorStyle}
                  error={errors[FIELD_NAMES.asset_filename]}
                  touched={true}
                />
                <FileSelect
                  asset_filename={values.asset_filename}
                  file={state.file}
                  name={FIELD_NAMES.asset_filename}
                  setFieldValue={setFieldValue}
                  setState={setState}
                />
                <div style={styles.submitContainer}>
                  <Submit
                    customRef={songSubmitRef}
                    isSubmitting={isSubmitting}
                    submitSuccess={state.submitSuccess}
                    // translationsKey={}
                  />
                </div>
              </fieldset>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
SongForm1.defaultProps = {
  submitHandler: songSubmitHandler,
}
SongForm1.propTypes = {
  songSubmitRef: PropTypes.object.isRequired,
  submitHandler: PropTypes.func.isRequired,
}

const styles = {
  submitContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    margin: '1em',
  },
}
