import React from 'react'
import PropTypes from 'prop-types'
import FieldWrapper from '../shared/FieldWrapper'

import TextField from '../shared/TextField'

const SongName = ({
  defaultValue,
  handleBlur,
  handleChange,
  name,
  ...rest
}) => {
  return (
    <FieldWrapper>
      <TextField
        id={name}
        defaultValue={defaultValue}
        label="Song Title*"
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        variant="outlined"
        {...rest}
      />
    </FieldWrapper>
  )
}

SongName.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
}

export default React.memo(SongName)
