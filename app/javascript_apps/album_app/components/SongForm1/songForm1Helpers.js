import produce from 'immer'

import { setParams } from '../DistributionStepper/routerHelpers'
import {
  STEPS,
  STEP_PARAMS,
} from '../DistributionStepper/distributionStepperHelpers'
import { parseErrors } from '../../utils/albumRequest'
import { post } from '../../utils/fetch'
import {
  createSongP,
  getPrimariesForSong,
  pruneOutOfScopeFields,
  replaceSongData,
} from '../../utils/songDataHelpers'

function prepareInitialValues(data, album) {
  const result = { ...data }
  result.asset_filename = data.asset_filename || ''
  result.artists = getPrimariesForSong(album) // must submit album artists with song
  return result
}

async function songSubmitHandler(form) {
  const {
    actions,
    file,
    history,
    setContinueDisabled,
    setState,
    song,
    songsDeps,
    syncSongWithSongs,
    values: rawValues,
  } = form

  const values = pruneOutOfScopeFields(rawValues, STEPS.SONG_FORM_1)

  setContinueDisabled(true)

  const isNameChange = checkChange(song, values, 'name')
  const isOnlyNameChange = !file && isNameChange
  const onlyFileChange = file && !isNameChange
  const nameAndFileChange = file && isNameChange

  let songData
  let asset

  switch (true) {
    case isOnlyNameChange: {
      // Optimistic UI: client validations match server, we shouldn't error at this point
      setParams(
        { step: STEP_PARAMS.s2, track: song.data.track_number },
        history
      )
      updateSong({ asset, file, song, songData, syncSongWithSongs })

      const result = await handleNameChange(actions, values)
      if (result) {
        songData = result
      }
      break
    }

    case onlyFileChange: {
      setState({ showUploader: true })
      const result = await handleFileChange({
        actions,
        file,
        setState,
        song,
        songsDeps,
      })
      songData = song.data
      asset = result
      break
    }

    case nameAndFileChange: {
      setState({ showUploader: true })
      const { data, uploadRes } = await handleNameAndFileChange({
        actions,
        file,
        setState,
        song,
        songsDeps,
        syncSongWithSongs,
        values,
      })
      songData = data
      asset = uploadRes
      break
    }

    default:
      console.error('SongForm1 not handled!')
      return
  }

  updateSong({ asset, file, song, songData, syncSongWithSongs })
  setParams({ track: songData.track_number }, history)
  setContinueDisabled(false)
}

function updateSongAsset(song, asset, file) {
  if (!asset) return song

  return produce(song, (draft) => {
    draft.data.asset_url = asset.streaming_url
    draft.data.asset_filename = file.name
  })
}

function updateSong({ asset, file, song, songData, syncSongWithSongs }) {
  let newSong = replaceSongData(song, songData)
  newSong = updateSongAsset(newSong, asset, file)
  syncSongWithSongs(newSong)
}

async function handleNameChange(actions, values) {
  const songRes = await createSongP(values)
  const { errors, data } = await songRes.json()

  if (songRes.ok) {
    return data
  } else {
    actions.setErrors(parseErrors(errors))
  }
}

async function handleFileChange({ actions, file, setState, song, songsDeps }) {
  try {
    const uploadRes = await uploadFile(actions, file, setState, songsDeps)

    if (!isValidDuration(uploadRes)) {
      actions.setErrors({ asset_filename: 'Duration error' })
      setState({ showUploader: false })
      return
    }

    registerAsset(song.data, uploadRes, setState)
    return uploadRes
  } catch (e) {
    handleUploadRelatedFailure(e, actions, setState)
    return
  }
}

async function handleNameAndFileChange({
  actions,
  file,
  setState,
  songsDeps,
  values,
}) {
  try {
    const [songRes, uploadRes] = await sendSongAndFile({
      values,
      actions,
      file,
      setState,
      songsDeps,
    })
    const { errors, data } = await songRes.json()

    const isValid = validateSongAndFile({
      actions,
      errors,
      setState,
      songRes,
      uploadRes,
    })
    if (!isValid) {
      return
    }

    registerAsset(data, uploadRes, setState)
    return { uploadRes, data }
  } catch (e) {
    handleUploadRelatedFailure(e, actions, setState)
    return
  }
}

async function sendSongAndFile({ values, actions, file, setState, songsDeps }) {
  const songP = createSongP(values)
  const uploadP = uploadFile(actions, file, setState, songsDeps)
  return await Promise.all([songP, uploadP])
}

// Optimistic UI:
// We aren't awaiting the registration result. We have a valid song and asset.
// If it's found that this call can fail, add error handling.
const REGISTRATION_ERROR = 'REGISTRATION_ERROR'
async function registerAsset(song, asset, setState) {
  setState({ registered: true })

  const { bit_rate, bucket, key, orig_filename, uuid } = asset
  const registerPayload = {
    s3_asset: {
      bucket,
      key,
      uuid,
    },
    song_id: song.id,
    upload: {
      bit_rate,
      orig_filename,
      song_id: song.id,
    },
  }

  const res = await post('/uploads/register', registerPayload)
  if (!res.ok) {
    console.error(REGISTRATION_ERROR)
  }
}

const UPLOAD_ERROR = 'UPLOAD_ERROR'
function uploadFile(actions, file, setState, songsDeps) {
  return new Promise((res, rej) => {
    setState({ progress: 0, registered: false, upload: null })

    const formData = new FormData()
    formData.append('Filedata', file)
    formData.append('tc_pid', songsDeps.tcPid)
    formData.append('packager_class', 'BasicPackager')

    // XHR API has an abort method, fetch doesn't
    const request = new XMLHttpRequest()

    setState({ upload: request })

    // Setting listeners before open is not to spec, but recommended:
    // https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/upload
    request.upload.addEventListener('progress', function (e) {
      const progress = Math.round((e.loaded / e.total) * 100)
      setState({ progress })
    })

    request.onabort = () => {
      actions.setSubmitting(false)
      actions.setFieldValue('asset_filename', '')
      actions.setTouched({ asset_filename: false })
    }
    request.onerror = () => rej(new Error(UPLOAD_ERROR))

    // success
    request.onload = () => res(JSON.parse(request.response).response)

    request.open('POST', `//${songsDeps.bigboxURL}/uploader/create`)
    request.send(formData)
  })
}

/* eslint-disable no-fallthrough */
function validateSongAndFile({
  actions,
  errors,
  setState,
  songRes,
  uploadRes,
}) {
  switch (true) {
    case !songRes.ok: {
      actions.setErrors(parseErrors(errors))
    }
    case !isValidDuration(uploadRes): {
      actions.setErrors({ asset_filename: 'Duration error' })
    }
    case !songRes.ok || !isValidDuration(uploadRes): {
      setState({ showUploader: false })
      return false
    }
    default:
      return true
  }
}
/* eslint-enable no-fallthrough */

function handleUploadRelatedFailure(e, actions, setState) {
  setState({ showUploader: false })
  switch (e.message) {
    case UPLOAD_ERROR:
      actions.setErrors({ asset_filename: 'File error' })
      break
    case REGISTRATION_ERROR:
      actions.setErrors({ asset_filename: 'Registration error' })
      break
    default:
      // Set 'general' error?
      console.error(e)
  }
}

const MINIMUM_VALID_DURATION = 2001 // "Must be greater than 2 seconds"
function isValidDuration(uploadRes) {
  return uploadRes.duration >= MINIMUM_VALID_DURATION
}

function checkChange(song, values, attr) {
  return song.data[attr] !== values[attr]
}

export { prepareInitialValues, songSubmitHandler }
