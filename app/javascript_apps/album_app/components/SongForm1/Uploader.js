import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import CheckIcon from '@material-ui/icons/Check'
import CircularProgress from '@material-ui/core/CircularProgress'

import { primaryColor } from '../shared/theme'
import { PrimaryButton, SecondaryButton } from '../shared/MUIButtons'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import { setParams } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'
import PrimaryHeadline from '../shared/PrimaryHeadline'
import TrackInfo from '../shared/TrackInfo'

export default function Uploader(props) {
  const { file, handleUploadAbort, name, progress, registered } = props
  const { song } = useDistributionFlow()[0]

  return (
    <>
      <Header registered={registered} />
      <TrackInfo file={file.name} name={name} />
      <div style={styles.container}>
        <div style={styles.inner}>
          <Loaders progress={progress} registered={registered} />
        </div>
      </div>
      <CancelButton handleUploadAbort={handleUploadAbort} progress={progress} />
      <StatusMessage progress={progress} registered={registered} />
      <ContinueButton registered={registered} song={song} />
    </>
  )
}

Uploader.propTypes = {
  file: PropTypes.instanceOf(File),
  name: PropTypes.string,
  registered: PropTypes.bool,
  progress: PropTypes.number,
  handleUploadAbort: PropTypes.func,
}

function CancelButton({ handleUploadAbort, progress }) {
  if (progress === 100) {
    return null
  }

  return (
    <div style={styles.cancelContainer}>
      <SecondaryButton onClick={handleUploadAbort}>
        Cancel Upload
      </SecondaryButton>
    </div>
  )
}

CancelButton.propTypes = {
  handleUploadAbort: PropTypes.func,
  progress: PropTypes.number,
}

function ContinueButton({ registered, song }) {
  const history = useHistory()

  function handleClick() {
    setParams({ step: STEP_PARAMS.s2, track: song.data.track_number }, history)
  }

  if (!registered) return null

  return (
    <div style={styles.continueButtonContainer}>
      <PrimaryButton onClick={handleClick}>Continue</PrimaryButton>
    </div>
  )
}

ContinueButton.propTypes = {
  registered: PropTypes.bool,
  song: PropTypes.object,
}

function Loaders({ progress, registered }) {
  if (registered) {
    return null
  }

  return (
    <>
      <div style={styles.progressContainer}>
        {progress < 100 ? (
          <CircularProgress
            size={120}
            thickness={4}
            value={progress}
            variant="determinate"
          />
        ) : (
          <CircularProgress size={120} thickness={4} />
        )}
      </div>
      <div style={styles.textContainer}>
        {progress === 100 ? (
          <CheckIcon style={styles.checkIcon} />
        ) : (
          <p style={styles.progressText}>{progress}%</p>
        )}
      </div>
    </>
  )
}

Loaders.propTypes = {
  progress: PropTypes.number,
  registered: PropTypes.bool,
}

function Header({ registered }) {
  switch (true) {
    case !registered:
      return (
        <PrimaryHeadline
          Element="h1"
          size="1.6em"
          translation="Please wait while we..."
        />
      )
    case registered:
      return (
        <PrimaryHeadline
          Element="h1"
          size="1.6em"
          translation="You're ready!"
        />
      )
    default:
      return null
  }
}

Header.propTypes = {
  registered: PropTypes.bool,
}

function StatusMessage({ progress, registered }) {
  switch (true) {
    case progress < 100:
      return null
    case progress === 100 && !registered:
      return <p>Processing audio...</p>
    case registered:
      return (
        <PrimaryHeadline
          Element="h2"
          size="1.2em"
          translation="Great! Tell us..."
        />
      )
    default:
      console.error('Unhandled upload status!')
  }
}

StatusMessage.propTypes = {
  progress: PropTypes.number,
  registered: PropTypes.bool,
}

const styles = {
  checkIcon: {
    color: primaryColor,
    fontSize: '4em',
  },
  cancelContainer: {
    display: 'flex',
    justifyContent: 'center',
    margin: '2em',
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
  },
  continueButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '1em',
  },
  inner: {
    position: 'relative',
    display: 'inline-flex',
  },
  progressContainer: {
    margin: '1.5em 0',
  },
  progressText: {
    fontSize: '2em',
    margin: 0,
  },
  textContainer: {
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    lineHeight: 1,
  },
}
