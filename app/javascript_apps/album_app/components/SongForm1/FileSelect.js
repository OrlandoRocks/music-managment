import React from 'react'
import PropTypes from 'prop-types'

import { PrimaryButton } from '../shared/MUIButtons'

export default function FileSelect({
  asset_filename,
  file,
  name,
  setFieldValue,
  setState,
}) {
  const text = file || asset_filename ? 'Replace' : 'Select'
  const filename = file ? file.name : asset_filename

  function handleChange(e) {
    const file = e.target.files[0]
    setFieldValue(name, file.name)
    setState({ file })
  }

  return (
    <div>
      <p style={styles.label}>Upload Track*</p>
      <p>{filename}</p>
      <PrimaryButton component="label" size="medium">
        {text}
        <input
          accept={FILE_TYPES}
          data-testid="upload-input"
          onChange={handleChange}
          type={TYPE}
          hidden
        />
      </PrimaryButton>
    </div>
  )
}

const FILE_TYPES = '.flac,.mp3,.wav'
const TYPE = 'file'

const styles = {
  label: {
    fontWeight: 500,
    margin: '1em 0',
  },
}

FileSelect.propTypes = {
  asset_filename: PropTypes.string,
  name: PropTypes.string,
  setFieldValue: PropTypes.func,
  setState: PropTypes.func,
  file: PropTypes.instanceOf(File),
}
