import { useEffect, useMemo } from 'react'
import partition from 'lodash/partition'

import guid from '../../../shared/guid'
import {
  createSongP,
  pruneOutOfScopeFields,
  replaceSongData,
} from '../../utils/songDataHelpers'
import { ALBUM_TYPE } from '../../utils/constants'
import {
  STEPS,
  STEP_PARAMS,
} from '../DistributionStepper/distributionStepperHelpers'
import { setStepParam } from '../DistributionStepper/routerHelpers'

const PRIMARY_ARTIST_CREDIT = 'primary_artist'
const PRIMARY_ARTIST_ROLE = 'primary_artist'
const FEATURING_CREDIT = 'featuring'
const FEATURED_ROLE = 'featured'

// TODO: fix translations for role_type
const FAKE_MAIN_ARTIST_ROLE = {
  id: PRIMARY_ARTIST_CREDIT,
  role_type_raw: PRIMARY_ARTIST_ROLE,
  role_type: 'primary artist',
}
// TODO: fix translations for role_type
const FAKE_FEATURED_ROLE = {
  id: FEATURING_CREDIT,
  role_type_raw: FEATURED_ROLE,
  role_type: 'featured',
}

const CREDIT_ROLES = [PRIMARY_ARTIST_ROLE, FEATURED_ROLE]
Object.freeze(CREDIT_ROLES)

const PERFORMER_ROLE = 'performer'
const PRODUCER_ROLE = 'producer'
const PRIORITY_ROLES = [PERFORMER_ROLE, PRODUCER_ROLE]
Object.freeze(PRIORITY_ROLES)

const SONGWRITER_ROLE_ID = '3'

// For role options
const EMPTY_FIXED_ROLES = []
const FIXED_MAIN_ARTIST_ROLE = [FAKE_MAIN_ARTIST_ROLE]

/* eslint-disable no-fallthrough */
function useSongwritersAndCreatives({
  assignableRoles,
  availableSongwriters,
  initialCreatives,
  initialSongwriters,
  setAvailableSongwriters,
  setInitialCreatives,
  setInitialSongwriters,
  song,
  songwriterNames,
  state,
}) {
  useEffect(() => {
    if (
      state.ready &&
      (!availableSongwriters || !initialCreatives || !initialSongwriters)
    ) {
      switch (true) {
        case !availableSongwriters: {
          setAvailableSongwriters([...songwriterNames].sort())
        }
        case !initialCreatives: {
          setInitialCreatives(
            sortCreatives(prepareCreatives(song, assignableRoles))
          )
        }
        case !initialSongwriters: {
          setInitialSongwriters(prepareSongwriters(song))
        }
        default:
          break
      }
    }
  }, [
    assignableRoles,
    availableSongwriters,
    setAvailableSongwriters,
    setInitialSongwriters,
    setInitialCreatives,
    song,
    songwriterNames,
    initialSongwriters,
    initialCreatives,
    state.ready,
  ])
}
/* eslint-enable no-fallthrough */

/**
 *
 * Form helpers
 *
 */

function pruneVirtualValues(values) {
  const result = { ...values }
  delete result.creatives
  delete result.songwriters
  return result
}

async function songSubmitHandler(form) {
  const { distributionActions, history, song, values: rawValues } = form
  const {
    setContinueDisabled,
    setSong,
    syncSongWithSongs,
  } = distributionActions

  setContinueDisabled(true)

  // 1. Parse songwriters and creatives into song.artists format
  const combinedArtists = combineFormArtists(
    rawValues.songwriters,
    rawValues.creatives
  )

  // 2. Prepare data for SF3
  const sf3Values = pruneVirtualValues(rawValues)
  sf3Values.artists = combinedArtists
  const optimisticSong = replaceSongData(song, sf3Values)
  syncSongWithSongs(optimisticSong)

  // Optimistic UI: client validations match server, we shouldn't error at this point
  setStepParam(STEP_PARAMS.s3, history)

  // 3. Submit SF2
  const values = pruneVirtualValues(
    pruneOutOfScopeFields(rawValues, STEPS.SONG_FORM_2)
  )
  values.artists = combinedArtists

  const songRes = await createSongP(values)
  const { errors, data } = await songRes.json()

  if (!songRes.ok) {
    console.log(errors)
    // TODO: return to SF2 or future 'errors' view?
    return
  }

  // 4. Sync response to context
  const newSong = replaceSongData(song, data)
  syncSongWithSongs(newSong)
  setSong(newSong)
  setContinueDisabled(false)
}

function parseCreatives(creatives) {
  return creatives.map((c) => {
    const result = { ...c }
    result.credit = parseRolesToCredit(c)
    result.role_ids = setCreativeRoles(c)

    return result
  })
}

function parseSongwriters(songwriters) {
  return Object.values(songwriters).map((s) => {
    const result = { ...s }
    return { ...result, role_ids: [SONGWRITER_ROLE_ID] }
  })
}

function getMatchingSongwriter(parsedSongwriters, creative) {
  return parsedSongwriters.find((sw) => sw.artist_name === creative.artist_name)
}

function removeStaleSongwriterRoles(parsedCreatives, parsedSongwriters) {
  return parsedCreatives.map((c) => {
    if (!getMatchingSongwriter(parsedSongwriters, c)) {
      c.role_ids = c.role_ids.filter((r) => !(r.id === SONGWRITER_ROLE_ID))
    }
    return c
  })
}

function removeSongwritersWithMatchingCreatives(
  parsedSongwriters,
  parsedCreatives
) {
  return parsedSongwriters.filter((sw) => {
    const match = parsedCreatives.find((c) => c.artist_name === sw.artist_name)

    if (match) {
      match.role_ids.push(SONGWRITER_ROLE_ID)
      return false
    }
    return true
  })
}

// Build artists array attribute with songwriters and creatives
// De-dupe artists with the same name by combining roles, preferring creatives
function combineFormArtists(songwriters, creatives) {
  const parsedSongwriters = parseSongwriters(songwriters)
  let parsedCreatives = parseCreatives(creatives)

  parsedCreatives = removeStaleSongwriterRoles(
    parsedCreatives,
    parsedSongwriters
  )
  const dedupedSongwriters = removeSongwritersWithMatchingCreatives(
    parsedSongwriters,
    parsedCreatives
  )

  return [...parsedCreatives, ...dedupedSongwriters].map(
    deleteUnpermittedParams
  )
}

function deleteUnpermittedParams(artist) {
  delete artist.roles
  delete artist.uuid
  return artist
}

const isPrimaryOrFeaturing = (r) => {
  if (typeof r === 'object') {
    return r.id === PRIMARY_ARTIST_CREDIT || r.id === FEATURING_CREDIT
  }
  return r === PRIMARY_ARTIST_CREDIT || r === FEATURING_CREDIT
}

function parseRolesToCredit(creative) {
  let result = 'contributor'

  const priorityRole = creative.roles.find(isPrimaryOrFeaturing)
  if (priorityRole) result = priorityRole.id
  return result
}

function setCreativeRoles(c) {
  return c.roles
    .filter((r) => Number.isInteger(r.id))
    .map((r) => r.id.toString())
}

/**
 *
 * Songwriter helpers
 *
 */
function useAssignableRoles(songRoles) {
  return useMemo(() => {
    if (!songRoles) {
      return
    }

    const result = [FAKE_MAIN_ARTIST_ROLE, FAKE_FEATURED_ROLE]
    return sortRoles(
      result.concat(songRoles.filter((r) => r.id !== SONGWRITER_ROLE_ID))
    )
  }, [songRoles])
}

function getSongwriters(songwriters) {
  return Object.values(songwriters)
}

const isSongwriter = (comparand) => (a) =>
  a.role_ids.includes(SONGWRITER_ROLE_ID) === comparand
function prepareSongwriters(song) {
  if (!song) return null

  const songwriters = song.data.artists.filter(isSongwriter(true))

  if (songwriters.length === 0) {
    songwriters.push(buildSongwriter())
  }

  return songwriters.reduce((acc, cur) => {
    const uuid = guid()
    acc[uuid] = { ...cur, uuid }
    return acc
  }, {})
}

function buildSongwriter() {
  const uuid = guid()
  return {
    artist_name: '',
    associated_to: 'Song',
    creative_id: null,
    credit: 'contributor',
    role_ids: [SONGWRITER_ROLE_ID],
    uuid,
  }
}

/**
 *
 * Creatives helpers
 *
 */
const isPrimaryRole = (r) =>
  r.role_type_raw === FAKE_MAIN_ARTIST_ROLE.role_type_raw
const isPrimaryCreative = (roles) => roles.some(isPrimaryRole)

const isFeaturedRole = (r) =>
  r.role_type_raw === FAKE_FEATURED_ROLE.role_type_raw
const isFeaturedCreative = (roles) => roles.some(isFeaturedRole)

function creativeRoleFilter({ creatives, input, options, uuid }) {
  let result = options
  const { roles } = creatives.find((c) => c.uuid === uuid)

  if (isPrimaryCreative(roles))
    result = options.filter((r) => !isFeaturedRole(r))
  if (isFeaturedCreative(roles))
    result = options.filter((r) => !isPrimaryRole(r))

  return result.filter((r) =>
    r.role_type.toLocaleLowerCase().startsWith(input.inputValue)
  )
}

function creditToRole(credit) {
  switch (credit) {
    case PRIMARY_ARTIST_CREDIT:
      return FAKE_MAIN_ARTIST_ROLE
    case FEATURING_CREDIT:
      return FAKE_FEATURED_ROLE
    default:
      console.error(`Credit not implemented: ${credit}.`)
  }
}

const isCreditRole = (r) => CREDIT_ROLES.includes(r.role_type_raw)

const isPriorityRole = (r) => PRIORITY_ROLES.includes(r.role_type_raw)

const sortByRoleType = (arr) =>
  arr.sort((a, b) => a.role_type.localeCompare(b.role_type))

function sortRoles(value) {
  const [credits, notCredits] = partition(value, isCreditRole)
  const [priorities, neither] = partition(notCredits, isPriorityRole)
  return [
    ...sortByRoleType(credits),
    ...sortByRoleType(priorities),
    ...sortByRoleType(neither),
  ]
}

function roleIDsToDisplayRoles(roleIDs, roles) {
  return roleIDs.map((r) => roles.find((role) => role.id === parseInt(r, 10)))
}

function prepareRoles(roleIDs, credit, roles) {
  const result = []
  if (isPrimaryOrFeaturing(credit)) {
    result.push(creditToRole(credit))
  }
  const withoutSongwriterRole = roleIDs.filter((r) => r !== SONGWRITER_ROLE_ID)
  return result.concat(roleIDsToDisplayRoles(withoutSongwriterRole, roles))
}

export const CONTRIBUTOR_CREDIT = 'contributor'
function buildCreative(options = {}) {
  const uuid = guid()
  return {
    artist_name: '',
    associated_to: 'Song',
    creative_id: null,
    credit: CONTRIBUTOR_CREDIT,
    role_ids: [],
    roles: [],
    uuid,
    ...options,
  }
}

function prepareCreatives(song, roles) {
  return song.data.artists
    .filter((a) => {
      return (
        // Any creative with more than just songwriter role
        a.role_ids.filter((r) => r !== SONGWRITER_ROLE_ID).length ||
        // Any primary or featured creatives, even if they only have songwriter role
        a.credit !== CONTRIBUTOR_CREDIT
      )
    })
    .reduce((acc, cur) => {
      const uuid = guid()
      acc[uuid] = {
        ...cur,
        roles: prepareRoles(cur.role_ids, cur.credit, roles),
        uuid,
      }
      return acc
    }, {})
}

const sortByName = (a, b) => a.artist_name.localeCompare(b.artist_name)
const isAlbumArtist = (a) => a.associated_to === ALBUM_TYPE
const isSongPriority = (a) =>
  a.credit === PRIMARY_ARTIST_CREDIT || (a.credit = FEATURING_CREDIT)
function sortCreatives(creatives) {
  const [primaries, rest] = partition(Object.values(creatives), isAlbumArtist)
  const [songPrimaries, nonPrimaries] = partition(rest, isSongPriority)
  return [
    ...primaries.sort(sortByName),
    ...songPrimaries.sort(sortByName),
    ...nonPrimaries.sort(sortByName),
  ]
}

function getFixedRoles(creative) {
  return creative.associated_to === ALBUM_TYPE
    ? FIXED_MAIN_ARTIST_ROLE
    : EMPTY_FIXED_ROLES
}

export {
  FAKE_FEATURED_ROLE,
  FAKE_MAIN_ARTIST_ROLE,
  PRIMARY_ARTIST_ROLE,
  SONGWRITER_ROLE_ID,
  buildCreative,
  buildSongwriter,
  creativeRoleFilter,
  getFixedRoles,
  getSongwriters,
  prepareCreatives,
  combineFormArtists,
  prepareSongwriters,
  songSubmitHandler,
  sortCreatives,
  sortRoles,
  useAssignableRoles,
  useSongwritersAndCreatives,
}
