import React, { useState } from 'react'
import produce from 'immer'
import PropTypes from 'prop-types'

import { TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Autocomplete from '@material-ui/lab/Autocomplete'

import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'

import ErrorMessage, { errorStyle } from '../shared/ErrorMessage'
import { CONTRIBUTOR_CREDIT } from './songForm2Helpers'
import { SONG_TYPE } from '../../utils/constants'

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  deleteButton: {
    padding: '0 1em',
  },
}

export default function SongwriterName({
  error,
  handleDelete,
  options,
  setSongwriters,
  songwriter,
  songwriters,
}) {
  // const { translations } = useConfig()[0]
  const [touched, setTouched] = useState(false)
  const classes = useStyles()

  // For typed updates
  function handleBlur(e) {
    const { name, value } = e.target
    updateSongwriter(value, name)
  }

  // For autocomplete/click updates
  function updateSongwriter(value, uuid) {
    if (!touched) setTouched(true)

    setSongwriters(
      produce(songwriters, (draft) => {
        const sw = draft[uuid]
        sw.artist_name = value
        sw.creative_id = null
        sw.credit = CONTRIBUTOR_CREDIT
        sw.associated_to = SONG_TYPE
      })
    )
  }

  if (!options) return null

  const autoFocus = Boolean(!error) && !songwriter.artist_name
  const showErrorStyle = touched && Boolean(error)

  return (
    <>
      <div className={classes.inputContainer}>
        <ErrorMessage
          containerStyle={errorStyle}
          error={error}
          touched={touched}
        />
        <div style={styles.container}>
          <Autocomplete
            className="songwriter-autocomplete"
            disableClearable
            forcePopupIcon
            freeSolo
            fullWidth
            includeInputInList
            onChange={(_, value) => updateSongwriter(value, songwriter.uuid)}
            options={options}
            renderInput={(params) => (
              <TextField
                {...params}
                autoFocus={autoFocus}
                error={showErrorStyle}
                label="First Name Last Name*"
                name={songwriter.uuid}
                onBlur={handleBlur}
                variant="outlined"
              />
            )}
            value={songwriter.artist_name}
          />
          <IconButton
            style={styles.deleteButton}
            edge="end"
            aria-label="delete"
            name={songwriter.uuid}
            onClick={handleDelete}
          >
            <DeleteIcon />
          </IconButton>
        </div>
      </div>
    </>
  )
}

SongwriterName.propTypes = {
  error: PropTypes.string,
  handleDelete: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  setSongwriters: PropTypes.func.isRequired,
  songwriter: PropTypes.object.isRequired,
  songwriters: PropTypes.object.isRequired,
}

const useStyles = makeStyles(() => ({
  inputContainer: {
    padding: '0 0 1em',
    width: '100%',
    '& .songwriter-autocomplete input': {
      border: 'none',
      boxShadow: 'none',
    },
  },
}))
