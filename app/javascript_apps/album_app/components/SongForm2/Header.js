import React from 'react'

export default function Header() {
  return <h1 style={styles.h1}>Tell Us Who Is On This Track</h1>
}

const styles = {
  h1: {
    margin: '1em 0',
  },
}
