import React from 'react'
import produce from 'immer'
import PropTypes from 'prop-types'

import AddIcon from '@material-ui/icons/Add'

import { SecondaryButton } from '../shared/MUIButtons'
import { buildCreative } from './songForm2Helpers'

export default function AddCreative({ creatives, setCreatives }) {
  function handleAddCreative() {
    setCreatives(
      produce(creatives, (draft) => {
        const newCreative = buildCreative()
        draft.push(newCreative)
      })
    )
  }

  return (
    <SecondaryButton onClick={handleAddCreative} startIcon={<AddIcon />}>
      Add Artist / Creative
    </SecondaryButton>
  )
}
AddCreative.propTypes = {
  creatives: PropTypes.array.isRequired,
  setCreatives: PropTypes.func.isRequired,
}
