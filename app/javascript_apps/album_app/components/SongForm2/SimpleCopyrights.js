import React from 'react'
import PropTypes from 'prop-types'

import { FormSectionSubtitle, FormSectionTitle } from '../shared/FormSection'
import { Checkbox, FormControlLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Spacer from '../../../shared/Spacer'
import { useConfig } from '../../contexts/ConfigContext'
import ErrorMessage, { errorStyle } from '../shared/ErrorMessage'

const useStyles = makeStyles(() => ({
  label: {
    alignItems: 'flex-start',

    '& .MuiIconButton-root': {
      paddingTop: 0,
    },
  },
}))

const COMPOSITION_NAME = 'copyrights.composition'
const RECORDING_NAME = 'copyrights.recording'

export default function SimpleCopyrights({ copyrights, errors, handleChange }) {
  const classes = useStyles()
  const { copyrightsFormEnabled } = useConfig()[0]

  if (!copyrightsFormEnabled) return null

  const compositionError = getCopyrightsError(errors, 'composition')
  const recordingError = getCopyrightsError(errors, 'recording')

  return (
    <>
      <FormSectionTitle translation="Musical Composition and Sound Recording Copyright" />
      <FormSectionSubtitle translation="You may be prompted to provide proof of this information before or after your release is delivered to digital stores." />
      <ErrorMessage
        containerStyle={errorStyle}
        error={compositionError}
        touched={true}
      />
      <div>
        <FormControlLabel
          className={classes.label}
          control={
            <Checkbox
              checked={copyrights?.composition || false}
              color="primary"
              name={COMPOSITION_NAME}
              onChange={handleChange}
            />
          }
          label="I am the Musical Composition Copyright Owner and/or have permission to distribute"
        />
      </div>
      <Spacer height={16} />
      <ErrorMessage
        containerStyle={errorStyle}
        error={recordingError}
        touched={true}
      />
      <div>
        <FormControlLabel
          className={classes.label}
          control={
            <Checkbox
              checked={copyrights?.recording || false}
              color="primary"
              name={RECORDING_NAME}
              onChange={handleChange}
            />
          }
          label="I am the Sound Recording Copyright Owner and/or have permission to distribute"
        />
      </div>
      <Spacer height={30} />
    </>
  )
}
SimpleCopyrights.propTypes = {
  copyrights: PropTypes.shape({
    composition: PropTypes.bool,
    recording: PropTypes.bool,
  }),
  errors: PropTypes.object,
  handleChange: PropTypes.func.isRequired,
}

function getCopyrightsError(errors, attr) {
  let result

  const baseError = errors.copyrights
  if (typeof baseError === 'string') {
    result = baseError
  } else {
    result = errors.copyrights?.[attr]
  }

  return result
}
