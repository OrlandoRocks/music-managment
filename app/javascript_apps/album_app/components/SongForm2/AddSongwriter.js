import React from 'react'
import produce from 'immer'
import PropTypes from 'prop-types'

import AddIcon from '@material-ui/icons/Add'

import { SecondaryButton } from '../shared/MUIButtons'
import { buildSongwriter } from './songForm2Helpers'

export default function AddSongwriter({ setSongwriters, songwriters }) {
  function handleAddSongwriter() {
    setSongwriters(
      produce(songwriters, (draft) => {
        const newSongwriter = buildSongwriter()
        draft[newSongwriter.uuid] = newSongwriter
      })
    )
  }

  return (
    <SecondaryButton onClick={handleAddSongwriter} startIcon={<AddIcon />}>
      Add Songwriter
    </SecondaryButton>
  )
}
AddSongwriter.propTypes = {
  setSongwriters: PropTypes.func.isRequired,
  songwriters: PropTypes.object.isRequired,
}
