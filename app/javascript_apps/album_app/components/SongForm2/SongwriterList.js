import React, { useEffect } from 'react'
import produce from 'immer'
import PropTypes from 'prop-types'

import SongwriterName from './SongwriterName'
import { FormSectionSubtitle, FormSectionTitle } from '../shared/FormSection'

export default function SongwriterList({
  availableSongwriters,
  errors,
  setAvailableSongwriters,
  setSongwriters,
  songwriterNames,
  songwriters,
}) {
  // Prune assigned songwriters from available songwriters
  useEffect(() => {
    if (!songwriterNames || !songwriters) return

    const nextAvailable = songwriterNames.filter(
      (n) =>
        !Object.values(songwriters)
          .map((s) => s.artist_name)
          .includes(n)
    )
    if (availableSongwriters.some((s) => nextAvailable.indexOf(s) === -1)) {
      setAvailableSongwriters(nextAvailable)
    }
  }, [
    availableSongwriters,
    setAvailableSongwriters,
    songwriterNames,
    songwriters,
  ])

  function handleDelete(e) {
    const toDelete = e.currentTarget.getAttribute('name')
    setSongwriters(
      produce(songwriters, (draft) => {
        delete draft[toDelete]
      })
    )
  }

  if (!songwriters) {
    return null
  }

  return (
    <>
      <FormSectionTitle translation="Songwriters" />
      <FormSectionSubtitle translation="At least one songwriter is required..." />
      {Object.values(songwriters).map((s) => {
        const error = errors?.[s.uuid]?.artist_name

        return (
          <React.Fragment key={s.uuid}>
            <SongwriterName
              key={s.uuid}
              error={error}
              handleDelete={handleDelete}
              options={availableSongwriters}
              setSongwriters={setSongwriters}
              songwriter={s}
              songwriters={songwriters}
            />
          </React.Fragment>
        )
      })}
    </>
  )
}
SongwriterList.propTypes = {
  availableSongwriters: PropTypes.array.isRequired,
  errors: PropTypes.object,
  setAvailableSongwriters: PropTypes.func.isRequired,
  setSongwriters: PropTypes.func.isRequired,
  songwriterNames: PropTypes.array.isRequired,
  songwriters: PropTypes.object.isRequired,
}
