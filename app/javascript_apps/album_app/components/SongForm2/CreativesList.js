import React from 'react'
import produce from 'immer'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import { Divider } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Chip from '@material-ui/core/Chip'
import { makeStyles } from '@material-ui/core/styles'

import {
  PRIMARY_ARTIST_ROLE,
  creativeRoleFilter,
  getFixedRoles,
  sortRoles,
} from './songForm2Helpers'
import { useConfig } from '../../contexts/ConfigContext'
import { ALBUM_TYPE } from '../../utils/constants'
import ErrorMessage, { errorStyle } from '../shared/ErrorMessage'
import { FormSectionSubtitle, FormSectionTitle } from '../shared/FormSection'

const styles = {
  creativesListContainer: {
    marginBottom: '1em',
  },
  deleteButton: {
    height: '1em',
    marginBottom: 'auto',
    marginTop: 'auto',
  },
}

export default function CreativesList({
  assignableRoles,
  creatives,
  errors,
  setCreatives,
  touched,
}) {
  const { possibleArtists } = useConfig()[0]
  const { setFieldTouched } = useFormikContext()
  const classes = useStyles()

  // For typed updates. Autocomplete/click updates handled onChange, not onBlur.
  function handleNameBlur(e, index) {
    const { name, value } = e.target
    touchCreative(index, 'name')
    updateCreative(value, name)
  }

  function updateCreative(value, uuid, index) {
    touchCreative(index, 'name')
    setCreatives(
      produce(creatives, (draft) => {
        draft.find((c) => c.uuid === uuid).artist_name = value
      })
    )
  }

  function handleRoleChange(uuid, v, fixedRoles) {
    setCreatives(
      produce(creatives, (draft) => {
        const c = draft.find((c) => c.uuid === uuid)
        c.roles = sortRoles([
          ...fixedRoles,
          ...v.filter((option) => fixedRoles.indexOf(option) === -1),
        ])
      })
    )
  }

  function touchCreative(i, attr) {
    setFieldTouched(`creatives.${i}.${attr}`)
  }

  function handleDelete(e) {
    const uuid = e.currentTarget.getAttribute('name')
    const toDelete = creatives.findIndex((c) => c.uuid === uuid)
    setCreatives([
      ...creatives.slice(0, toDelete),
      ...creatives.slice(toDelete + 1),
    ])
  }

  function getAvailablePossibleArtists() {
    return possibleArtists.filter(
      (p) => !creatives.map((c) => c.artist_name).includes(p)
    )
  }
  const availablePossibleArtists = getAvailablePossibleArtists()

  function renderTags({ getRoleOptionDisabledState, getTagProps, value }) {
    const result = sortRoles([...value])
    return result.map((option, index) => (
      <Chip
        {...getTagProps({ index })}
        key={option.role_type}
        label={option.role_type}
        disabled={getRoleOptionDisabledState(option)}
      />
    ))
  }

  if (!creatives) {
    return null
  }

  return (
    <div style={styles.creativesListContainer}>
      <FormSectionTitle translation="Song Artists & Creatives" />
      <FormSectionSubtitle translation="List all artists that appear..." />
      {creatives.map((c, i) => {
        const fixedRoles = getFixedRoles(c)
        const nameError = errors?.[i]?.artist_name
        const rolesError = errors?.[i]?.roles
        const isAlbumArtist = c.associated_to === ALBUM_TYPE
        const isLast = creatives.length === i + 1
        const isNameTouched = touched?.[i]?.name
        const applyNameErrorStyle = isNameTouched && Boolean(nameError)
        const applyRoleErrorStyle = isNameTouched && Boolean(rolesError)

        const focusName = (c) => Boolean(!nameError) && !c.artist_name
        const getRoleOptionDisabledState = (option) =>
          isAlbumArtist && option.role_type_raw === PRIMARY_ARTIST_ROLE

        return (
          <div key={c.uuid}>
            <div className={classes.row}>
              <div className={classes.fields}>
                <div className={classes.creativeNameContainer}>
                  <ErrorMessage
                    containerStyle={errorStyle}
                    error={nameError}
                    touched={isNameTouched}
                  />
                  <Autocomplete
                    className="inputCreative"
                    disableClearable
                    disabled={isAlbumArtist}
                    forcePopupIcon={!isAlbumArtist}
                    freeSolo
                    fullWidth
                    onBlur={(e) => handleNameBlur(e, i)}
                    onChange={(_, value) => updateCreative(value, c.uuid, i)}
                    options={availablePossibleArtists}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        id={c.artist_name}
                        autoFocus={focusName(c)}
                        error={applyNameErrorStyle}
                        fullWidth
                        label="Artist / Creative Name*"
                        name={c.uuid}
                        variant="outlined"
                      />
                    )}
                    value={c.artist_name}
                  />
                </div>

                <div className={classes.rolesContainer}>
                  <ErrorMessage
                    containerStyle={errorStyle}
                    error={rolesError}
                    touched={applyRoleErrorStyle}
                  />
                  <Autocomplete
                    className="inputRole"
                    disableClearable
                    filterOptions={(options, input) =>
                      creativeRoleFilter({
                        creatives,
                        input,
                        options,
                        uuid: c.uuid,
                      })
                    }
                    forcePopupIcon
                    fullWidth
                    getOptionLabel={(o) => o.role_type}
                    multiple
                    onBlur={() => touchCreative(i, 'roles')}
                    onChange={(_, v) => handleRoleChange(c.uuid, v, fixedRoles)}
                    options={assignableRoles}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        id={c.artist_name}
                        error={applyRoleErrorStyle}
                        label="Role*"
                        variant="outlined"
                      />
                    )}
                    renderTags={(value, getTagProps) =>
                      renderTags({
                        getRoleOptionDisabledState,
                        getTagProps,
                        value,
                      })
                    }
                    value={c.roles}
                  />
                </div>
              </div>
              <IconButton
                aria-label="delete"
                className={classes.removeCreative}
                disabled={isAlbumArtist}
                edge="end"
                name={c.uuid}
                onClick={handleDelete}
                style={styles.deleteButton}
              >
                <DeleteIcon />
              </IconButton>
            </div>
            {!isLast && <Divider className={classes.listItemDivider} />}
          </div>
        )
      })}
    </div>
  )
}
CreativesList.propTypes = {
  assignableRoles: PropTypes.array.isRequired,
  creatives: PropTypes.array.isRequired,
  errors: PropTypes.array,
  setCreatives: PropTypes.func.isRequired,
  touched: PropTypes.array,
}

const useStyles = makeStyles(() => ({
  creativeNameContainer: {
    '& .inputCreative input ': {
      border: 'none',
      boxShadow: 'none',
    },
  },
  fields: {
    width: '100%',
  },
  newCreativeContainer: {
    display: 'flex',
    padding: '1em 0',
    '& .inputCreative input': {
      border: 'none',
      boxShadow: 'none',
    },
    '& #add-creative-button': {
      marginLeft: '2em',
    },
  },
  removeCreative: {
    padding: '0 1em',
  },
  rolesContainer: {
    marginTop: '1em',
    '& .inputRole input': {
      border: 'none',
      boxShadow: 'none',
    },
  },
  row: {
    display: 'flex',
    // marginTop: '1em',
  },
  listItemDivider: {
    margin: '2em 0',
  },
}))
