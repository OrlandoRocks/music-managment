import React, { useState } from 'react'
import { Form, Formik } from 'formik'
import { useHistory, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'

import Skeleton from '@material-ui/lab/Skeleton'

import SongwriterList from './SongwriterList'
import AddSongwriter from './AddSongwriter'
import CreativesList from './CreativesList'
import AddCreative from './AddCreative'

import Submit from '../shared/Submit'
import ErrorScroller from '../shared/ErrorScroller'
import Spacer from '../../../shared/Spacer'

import { devLogger, isEmpty } from '../../../utils'
import { IS_DEV } from '../../utils/constants'
import { useSongSetter } from '../../utils/songDataHelpers'
import {
  songSubmitHandler,
  useAssignableRoles,
  useSongwritersAndCreatives,
} from './songForm2Helpers'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import { useConfig } from '../../contexts/ConfigContext'
import SimpleCopyrights from './SimpleCopyrights'
import TrackInfo from '../shared/TrackInfo'
import PrimaryHeadline from '../shared/PrimaryHeadline'
import createSongSchema, { SCHEMAS } from '../shared/songSchema'
import { skipOrContinueSF } from '../shared/songHelpers'

const FIELDS = {
  copyrights: 'copyrights',
  creatives: 'creatives',
  songwriters: 'songwriters',
}
Object.freeze(FIELDS)

/**
 * song.artists are either album-level or song-level creatives.
 * Artists are decomposed here into `songwriters` and `creatives`. They can appear as both.
 *
 * Creatives have:
 *   credits (entity: creative.role) e.g. 'primary_artist'
 *   and roles (entity: creative_song_role.song_role_id) e.g. 'banjo'.
 *
 *   Credits appear here as roles. This simplifies UX(maybe?) and is carried-over from legacy.
 *   A creative without another credit falls-back to credit: 'contributor'.
 *
 * Songwriter:
 *   is just another role, but it is handled separately here.
 *
 * We re-compose `songwriters` and `creatives` into song.artists during submission.
 */
export default function SongForm2({ songSubmitRef, submitHandler }) {
  const [
    {
      activeStep,
      fetchedSongsData,
      song,
      songs,
      songsDeps: { songRoles, songwriterNames },
    },
    distributionActions,
  ] = useDistributionFlow()
  const { setSong } = distributionActions

  const history = useHistory()
  const location = useLocation()
  const config = useConfig()[0]

  const [state, setState] = useState({ manualErrorScrollTrigger: 0 })

  const [initialSongwriters, setInitialSongwriters] = useState(null)
  const [availableSongwriters, setAvailableSongwriters] = useState()

  const [initialCreatives, setInitialCreatives] = useState()
  const assignableRoles = useAssignableRoles(songRoles)

  // ensure the correct song is loaded for the form
  useSongSetter({
    location,
    fetchedSongsData,
    setSong,
    setState,
    song,
    songs,
    state,
  })

  // Setup songwriters and creatives after useSongSetter
  useSongwritersAndCreatives({
    assignableRoles,
    availableSongwriters,
    initialCreatives,
    initialSongwriters,
    setAvailableSongwriters,
    setInitialCreatives,
    setInitialSongwriters,
    song,
    songwriterNames,
    state,
  })

  // Don't render the form until ready, because it doesn't have a song yet.
  if (
    !state.ready ||
    isEmpty(initialSongwriters) ||
    isEmpty(initialCreatives)
  ) {
    const height = 60

    return (
      <>
        <PrimaryHeadline
          Element="h1"
          size="1.6em"
          translation="Tell Us Who Is On This Track"
        />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <br />
        <br />
        <Skeleton height={height} />
        <Skeleton height={height} />
        <Skeleton height={height} />
      </>
    )
  }

  const { asset_filename, name: songName } = song.data
  const SongForm2Schema = createSongSchema(config, SCHEMAS.SF2)

  // Form is initialized from context/state, but thereafter form values are not syndicated to state
  // until submission is successful.
  return (
    <div className="grid-container div-album-form">
      <Formik
        initialValues={{
          ...song.data,
          creatives: initialCreatives,
          songwriters: initialSongwriters,
        }}
        onSubmit={async (values, actions) => {
          await submitHandler({
            actions,
            distributionActions,
            history,
            song,
            values,
          })
        }}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={SongForm2Schema}
      >
        {(formikBag) => {
          const {
            errors,
            handleChange,
            isSubmitting,
            setFieldValue,
            touched,
            values,
          } = formikBag

          IS_DEV && devLogger(errors, 'Song Form 2 Errors')
          IS_DEV && devLogger(values, 'Song Form 2 Values')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            skipOrContinueSF({
              activeStep,
              config,
              formikBag,
              history,
              songs,
              setState,
            })
          }

          const { copyrights, creatives, songwriters } = values

          function setCreatives(newCreatives) {
            setFieldValue('creatives', newCreatives)
          }

          function setSongwriters(newSongwriters) {
            setFieldValue('songwriters', newSongwriters)
          }

          return (
            <Form data-testid="SongForm2" onSubmit={wrappedHandleSubmit}>
              <fieldset>
                <ErrorScroller
                  errors={errors}
                  trigger={state.manualErrorScrollTrigger}
                />
                <PrimaryHeadline
                  Element="h1"
                  size="1.6em"
                  translation="Tell Us Who Is On This Track"
                />
                <TrackInfo file={asset_filename} name={songName} />
                <Spacer height={30} />

                <SimpleCopyrights
                  copyrights={copyrights}
                  errors={errors}
                  handleChange={handleChange}
                />
                <SongwriterList
                  availableSongwriters={availableSongwriters}
                  errors={errors[FIELDS.songwriters]}
                  setAvailableSongwriters={setAvailableSongwriters}
                  setSongwriters={setSongwriters}
                  songwriterNames={songwriterNames}
                  songwriters={songwriters}
                />
                <AddSongwriter
                  setSongwriters={setSongwriters}
                  songwriters={songwriters}
                />
                <Spacer height={30} />
                <CreativesList
                  assignableRoles={assignableRoles}
                  creatives={creatives}
                  errors={errors[FIELDS.creatives]}
                  setCreatives={setCreatives}
                  touched={touched.creatives}
                />
                <AddCreative
                  creatives={creatives}
                  setCreatives={setCreatives}
                />
                <div style={styles.submitContainer}>
                  <Submit
                    customRef={songSubmitRef}
                    isSubmitting={isSubmitting}
                    // translationsKey={}
                  />
                </div>
              </fieldset>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
SongForm2.defaultProps = {
  submitHandler: songSubmitHandler,
}
SongForm2.propTypes = {
  songSubmitRef: PropTypes.object.isRequired,
  submitHandler: PropTypes.func.isRequired,
}

const styles = {
  submitContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    margin: '2em',
  },
}
