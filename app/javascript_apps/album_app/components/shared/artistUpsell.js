const UNLIMITED_ARTIST_PLANS = [4, 5]

const planNeedsUpsell = (person_plan) =>
  person_plan && !UNLIMITED_ARTIST_PLANS.includes(person_plan)

const isOnlyArtist = (creatives) => Object.keys(creatives).length === 1

// The single artist included with all plans has not yet been used
const isFirstArtist = (activeArtists) => activeArtists.length === 0

const isFirstAndOnlyArtist = (activeArtists, creatives) =>
  isOnlyArtist(creatives) && isFirstArtist(activeArtists)

function getShowPlanUpsell(activeArtists, creatives, person_plan) {
  if (isFirstAndOnlyArtist(activeArtists, creatives)) return false

  return (
    planNeedsUpsell(person_plan) &&
    Object.values(creatives).some(
      (c) => Boolean(c.name) && !activeArtists.includes(c.name)
    )
  )
}

function getShowRowUpsell({ activeArtists, index, name, person_plan }) {
  if (
    activeArtists.includes(name) ||
    (isFirstArtist(activeArtists) && index === 0)
  )
    return false

  return Boolean(name && person_plan)
}

export { getShowPlanUpsell, getShowRowUpsell }
