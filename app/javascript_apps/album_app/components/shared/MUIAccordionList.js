import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  List,
  ListItem,
} from '@material-ui/core'
import { ExpandMore, ExpandLess } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

import { lightGrayColor } from './theme'

function PimaryAccordionList(props) {
  const { listItems, description } = props
  const { accordianPrimary, accordianSummaryContentPrimary } = styles()
  const [expanded, setExpanded] = useState(false)

  return (
    <Accordion className={accordianPrimary} expanded={expanded}>
      <AccordionSummary
        classes={{ content: accordianSummaryContentPrimary }}
        expandIcon={<ExpandMore />}
        onClick={() => setExpanded(!expanded)}
      >
        {description}
      </AccordionSummary>
      <AccordionDetails>
        <List>
          {listItems.map((item, index) => (
            <ListItem key={index}>{item}</ListItem>
          ))}
        </List>
      </AccordionDetails>
      <AccordionSummary
        classes={{ content: accordianSummaryContentPrimary }}
        expandIcon={<ExpandLess />}
        onClick={() => setExpanded(!expanded)}
      >
        {description}
      </AccordionSummary>
    </Accordion>
  )
}

export { PimaryAccordionList }

PimaryAccordionList.propTypes = {
  description: PropTypes.string,
  listItems: PropTypes.array.isRequired,
}

const styles = makeStyles(() => ({
  accordianPrimary: {
    '&.MuiPaper-elevation1': {
      boxShadow: 'none',
    },
    '&.MuiAccordion-root:before': {
      opacity: '0',
    },
  },
  accordianSummaryContentPrimary: {
    display: 'inline',
    textAlign: 'right',
    color: lightGrayColor,
  },
}))
