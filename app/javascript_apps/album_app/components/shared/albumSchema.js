import * as yup from 'yup'
import dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
dayjs.extend(isSameOrAfter)

import { INDIAN_GENRE_ID, isSubgenre } from '../AlbumFormFields/Genres'
import { REQUIRED_FIELD } from '../../utils/constants'

export const SCHEMAS = {
  V1: 'v1',
  V2_1: 'v2_1',
  V2_2: 'v2_2',
}
Object.freeze(SCHEMAS)

const { V1, V2_1, V2_2 } = SCHEMAS

/**
 * Use field names for yup keys
 * Use translation keys for messages, will fall back to raw value provided
 */
export function createAlbumSchema(config, form) {
  const {
    genres,
    releaseTypeVars: { explicitFieldsEnabled },
    spotifyArtistsRequired,
  } = config

  const clean_version = cleanVersionValidator(explicitFieldsEnabled)
  const creatives = yup.object().when('is_various', {
    is: false,
    then: creativesValidator({ spotifyArtistsRequired }),
    otherwise: yup.object(),
  })
  const is_various = yup.bool().required(REQUIRED_FIELD)
  const language_code = yup.string().required(REQUIRED_FIELD)
  const name = yup.string().required(REQUIRED_FIELD)
  const optional_upc_number = upcValidator()
  const label_name = albumLabelValidator()
  const parental_advisory = parentalAdvisoryValidator(explicitFieldsEnabled)
  const primary_genre_id = yup.string().required(REQUIRED_FIELD)
  const sale_date = yup.date().when('is_previously_released', {
    is: true,
    then: saleDateValidator(),
    otherwise: yup.date(),
  })
  const sub_genre_id_primary = subgenreValidator('primary', genres)
  const sub_genre_id_secondary = subgenreValidator('secondary', genres)

  switch (form) {
    case V1:
      return yup.object().shape({
        clean_version,
        creatives,
        is_various,
        language_code,
        name,
        optional_upc_number,
        label_name,
        parental_advisory,
        primary_genre_id,
        sale_date,
        sub_genre_id_primary,
        sub_genre_id_secondary,
      })

    case V2_1:
      return yup.object().shape({
        clean_version,
        creatives,
        is_various,
        language_code,
        name,
        optional_upc_number,
        label_name,
        parental_advisory,
        primary_genre_id,
        sub_genre_id_primary,
        sub_genre_id_secondary,
      })

    case V2_2:
      return yup.object().shape({
        sale_date,
      })
    default:
      break
  }
}

function saleDateValidator() {
  return yup.date().test({
    name: 'date',
    message: 'sale_date_error',
    test: function (value) {
      const originalRelease = dayjs(this.parent.orig_release_year)
      return dayjs(value).isSameOrAfter(originalRelease)
    },
  })
}

function betterIsNaN(value) {
  const numberPattern = /\d+/g
  const matchedNumberArray = value.match(numberPattern)
  const matchedNumberString =
    (matchedNumberArray && matchedNumberArray.join('')) || ''

  return (
    matchedNumberString.length !== value.length || isNaN(matchedNumberString)
  )
}
const MIN_UPC_LENGTH = 12
const MAX_UPC_LENGTH = 13
const upcLengthInRange = (v) => v <= MAX_UPC_LENGTH && v >= MIN_UPC_LENGTH

function upcValidator() {
  return yup
    .string()
    .test('type', 'upc_errors.type', function (value) {
      if (!value) return true

      if (betterIsNaN(value)) return false

      return true
    })
    .test('length', 'upc_errors.length', function (value) {
      if (!value) return true

      const trimmedLength = value.trim().length
      return upcLengthInRange(trimmedLength)
    })
}

const MAX_LABEL_LENGTH = 120

function albumLabelValidator() {
  return yup.string().test('length', 'label_errors.length', function (value) {
    if (!value) return true

    if (value.length <= MAX_LABEL_LENGTH) {
      return true
    } else {
      return false
    }
  })
}

function creativesValidator(validatorOptions = {}) {
  const { spotifyArtistsRequired } = validatorOptions

  const initialTest = yup
    .object()
    .test('Uniqueness', 'duplicate_primary_artist', (creatives) => {
      const namesList = Object.values(creatives).map((val) => {
        return val.name == null ? val.name : val.name.toLowerCase()
      })
      return new Set(namesList).size == namesList.length
    })
    .test(
      'Required',
      REQUIRED_FIELD,
      (creatives) =>
        creatives &&
        Object.keys(creatives).length > 0 &&
        Object.values(creatives).every((c) => Boolean(c?.name?.trim()))
    )

  if (spotifyArtistsRequired) {
    return initialTest.test(
      'Required',
      REQUIRED_FIELD,
      (creatives) =>
        creatives &&
        Object.keys(creatives).length > 0 &&
        Object.values(creatives).every(
          (creative) =>
            creative.spotify && Object.values(creative.spotify).length > 0
        )
    )
  } else {
    return initialTest
  }
}

function parentalAdvisoryValidator(explicitFieldsEnabled) {
  return explicitFieldsEnabled
    ? yup.bool().required(REQUIRED_FIELD).typeError(REQUIRED_FIELD)
    : yup.bool().nullable()
}

function cleanVersionValidator(explicitFieldsEnabled) {
  return explicitFieldsEnabled
    ? yup.bool().when('parental_advisory', {
        is: false,
        then: yup.bool().required(REQUIRED_FIELD).typeError(REQUIRED_FIELD),
        otherwise: yup.bool().nullable(),
      })
    : yup.bool().nullable()
}

const hasSubgenres = (id, genres) =>
  genres.some((g) => g.parent_id === parseInt(id, 10))
const hasOrIsSubgenre = (id, genres) =>
  hasSubgenres(id, genres) || isSubgenre(id, genres)
function subgenreValidator(type, genres) {
  return yup.string().when(`${type}_genre_id`, {
    is: (id_str) => {
      const id = parseInt(id_str, 10)
      return id === INDIAN_GENRE_ID && hasOrIsSubgenre(id, genres)
    },
    then: yup.string().required(REQUIRED_FIELD).typeError(REQUIRED_FIELD),
    otherwise: yup.string().nullable(),
  })
}
