import { createMuiTheme } from '@material-ui/core/styles'

export const primaryColor = '#44b1fa'
export const secondaryDarkColor = '#d7e8f5'
export const lightGrayColor = '#777777'
export const infoMessageColor = '#44b1fa'
export const lockMessageColor = '#44b1fa'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#a1d7ff',
      main: primaryColor,
      dark: '#3777b1',
      contrastText: '#fff',
    },
    secondary: {
      light: '#eaf5fe',
      main: '#eaf5fe',
      dark: '#d7e8f5',
      contrastText: '#5ba7e9',
    },
  },
  // override Rails and MUI styles here
  overrides: {
    MuiButton: {
      root: {
        "&[type='file']": {
          width: 'initial',
        },
      },
    },
    MuiIconButton: {
      root: {
        backgroundColor: 'transparent',
        '&:hover': {
          backgroundColor: 'transparent',
        },
      },
    },
  },
})

export default theme
