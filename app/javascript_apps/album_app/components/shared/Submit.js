import React from 'react'
import PropTypes from 'prop-types'

import Spinner from '../shared/Spinner'
import { PrimaryButton } from './MUIButtons'

function Submit({ customRef, isSubmitting, submitSuccess }) {
  return (
    <>
      {isSubmitting || submitSuccess ? (
        <Spinner />
      ) : (
        <div>
          <PrimaryButton
            aria-label="Submit"
            customRef={customRef}
            size="large"
            type="submit"
          >
            Submit
          </PrimaryButton>
        </div>
      )}
    </>
  )
}

Submit.propTypes = {
  customRef: PropTypes.object,
  isSubmitting: PropTypes.bool,
  submitSuccess: PropTypes.bool,
}

export default React.memo(Submit)
