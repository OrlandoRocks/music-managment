import React from 'react'
import PropTypes from 'prop-types'
import get from 'lodash/get'

import { useConfig } from '../../contexts/ConfigContext'
import ErrorMessage from '../../../shared/ErrorMessage'

const containerStyle = {
  marginBottom: '10px',
  marginTop: '30px',
}

// Supports dot-segmented translation paths via lodash.get
// e.g. 'some.nested.translations.key'
function ConditionalErrorMessage({ containerStyle, error, touched }) {
  const { translations } = useConfig()[0]

  if (!error || !touched) return null

  const message = get(translations, error, error)

  return (
    <div className="cell medium-offset-2 medium-10">
      <ErrorMessage message={message} containerStyle={containerStyle} />
    </div>
  )
}
ConditionalErrorMessage.defaultProps = {
  containerStyle,
}

ConditionalErrorMessage.propTypes = {
  containerStyle: PropTypes.object,
  error: PropTypes.any,
  touched: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
}

export default React.memo(ConditionalErrorMessage)

export const errorStyle = {
  margin: '0 0 1.5em',
}
