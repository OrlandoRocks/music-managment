import React from 'react'
import PropTypes from 'prop-types'

import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { secondaryDarkColor } from './theme'

function PrimaryButton(props) {
  const { customRef, ...rest } = props
  return (
    <Button
      ref={customRef}
      color="primary"
      disableElevation
      disableRipple
      variant="contained"
      {...rest}
    />
  )
}

PrimaryButton.propTypes = {
  customRef: PropTypes.object,
}

function AltSecondaryButton(props) {
  return (
    <Button
      {...props}
      color="primary"
      disableElevation
      disableRipple
      variant="outlined"
    />
  )
}

// Looks too much like a link?
function PrimaryTextButton(props) {
  const { textPrimary } = styles()
  return <Button {...props} className={textPrimary} color="primary" />
}

const secondaryButtonStyle = {
  border: `1px solid ${secondaryDarkColor}`,
  margin: '.5em 0',
  padding: '.75em',
}
function SecondaryButton({ customStyle, dataTestId, hide, ...props }) {
  if (hide) return null

  const styleProp = customStyle || {}
  return (
    <Button
      data-testid={dataTestId}
      color="secondary"
      disableElevation
      disableRipple
      style={{ ...secondaryButtonStyle, ...styleProp }}
      variant="contained"
      {...props}
    />
  )
}

SecondaryButton.propTypes = {
  customStyle: PropTypes.object,
  hide: PropTypes.bool,
  dataTestId: PropTypes.string,
}

export { AltSecondaryButton, PrimaryButton, PrimaryTextButton, SecondaryButton }

const styles = makeStyles(() => ({
  textPrimary: {
    '&.Mui-disabled': {
      backgroundColor: 'transparent',
    },
    '&:hover': {
      background: 'none',
    },
  },
}))
