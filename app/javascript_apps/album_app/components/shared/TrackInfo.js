import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    margin: '1em 0 0',
  },
  labels: {
    fontWeight: 300,
    marginBottom: 0,
  },
  labelContainer: {
    marginRight: '1em',
  },
  values: {
    fontStyle: 'italic',
    marginBottom: 0,
  },
}

export default function TrackInfo({ file, name }) {
  return (
    <div style={styles.container}>
      <div style={styles.labelContainer}>
        <p style={styles.labels}>TRACK:</p>
        <p style={styles.labels}>FILE:</p>
      </div>
      <div>
        <p style={styles.values}>{name}</p>
        <p style={styles.values}>{file}</p>
      </div>
    </div>
  )
}
TrackInfo.propTypes = {
  file: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
}
