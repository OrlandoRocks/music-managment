import React from 'react'
import PropTypes from 'prop-types'

import { primaryColor } from './theme'

export default function PrimaryHeadline({ Element, size, translation }) {
  const styles = { color: primaryColor, fontSize: size }
  return <Element style={styles}>{translation}</Element>
}
PrimaryHeadline.propTypes = {
  Element: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  translation: PropTypes.string.isRequired,
}
