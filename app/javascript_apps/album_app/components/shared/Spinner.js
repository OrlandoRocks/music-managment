import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const styles = {
  medium: {
    fontSize: '1.5em',
  },
  large: {
    fontSize: '40px',
  },
}

const Spinner = React.memo(function Spinner({
  show = true,
  showText = true,
  size = 'large',
}) {
  const { translations } = useConfig()[0]

  if (!show) return null

  const style = styles[size]

  return (
    <div style={style}>
      <i className="fa fa-spinner fa-spin"></i>{' '}
      {showText && translations.processing_with_ellipsis}
    </div>
  )
})
Spinner.displayName = 'Spinner'

Spinner.propTypes = {
  show: PropTypes.bool,
  showText: PropTypes.bool,
  size: PropTypes.string,
}

export default Spinner
