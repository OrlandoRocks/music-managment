import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  outer: {
    padding: '1.25em 0',
  },
}

export default function FieldWrapper({ className, children }) {
  return (
    <div className={className} style={styles.outer}>
      {children}
    </div>
  )
}
FieldWrapper.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
}
