import React from 'react'
import PropTypes from 'prop-types'

// Looks like MUI InputLabel
const styles = {
  color: 'rgba(0, 0, 0, 0.54)',
  fontFamily: "Roboto, Helvetica, Arial, 'sans-serif'",
  fontWeight: 400,
  letterSpacing: '0.00938em',
  margin: 0,
  padding: '1.25em 0 0',
}

export default function FakeLabel({ translation }) {
  return <p style={styles}>{translation}</p>
}
FakeLabel.propTypes = {
  translation: PropTypes.string.isRequired,
}
