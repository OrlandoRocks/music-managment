import * as yup from 'yup'

import { REQUIRED_FIELD } from '../../utils/constants'

// The Unicode range for 4-byte chars
const FOUR_BYTE_REGEX = /[\u{10000}-\u{10FFFF}]/gu

export const SCHEMAS = {
  SF1: 'sf1',
  SF2: 'sf2',
  SF3: 'sf3',
}

const { SF1, SF2, SF3 } = SCHEMAS

/**
 * Use field names for yup keys
 * Use translation keys for messages, will fall back to raw value provided
 */
export default function createSongSchema(config, form) {
  switch (form) {
    case SF1:
      return yup.object().shape({
        name: yup
          .string()
          .test(
            'unsupported chars',
            'unsupported_characters',
            function (value) {
              if (!value) {
                return true
              }

              const match = value.match(FOUR_BYTE_REGEX)

              if (match) {
                const chars = Array.isArray(match) ? match.join(', ') : match[0]
                return this.createError(
                  `Unsupported characters found: ${chars}`
                )
              }

              return true
            }
          )
          .trim()
          .required('required_field'),
        asset_filename: yup.string().trim().required('required_field'),
      })

    case SF2: {
      const { copyrightsFormEnabled } = config

      return yup.object().shape({
        copyrights: copyrightsValidator(copyrightsFormEnabled),
        // We validate creatives and songwriters, not artists. If they pass, they're combined into the
        // 'artists' attribute for the request. Creatives and songwriters attrs are not sent.
        creatives: yup.lazy((cArray) =>
          yup.array().of(
            yup.object().shape({
              artist_name: yup
                .string()
                .test(
                  'unsupported chars',
                  'unsupported_characters',
                  function (value) {
                    if (!value) {
                      return true
                    }

                    const match = value.match(FOUR_BYTE_REGEX)

                    if (match) {
                      const chars = Array.isArray(match)
                        ? match.join(', ')
                        : match[0]
                      return this.createError(
                        `Unsupported characters found: ${chars}`
                      )
                    }

                    return true
                  }
                )
                .test('Uniqueness', 'duplicate_creative', (v) => {
                  if (
                    cArray.map((s) => s.artist_name).filter((s) => s === v)
                      .length > 1
                  )
                    return false
                  return true
                })
                .trim()
                .required('required_field'),
              roles: yup
                .array()
                .of(
                  yup
                    .object()
                    .shape({ id: yup.string().required('required_field') })
                )
                .required('required_field'),
            })
          )
        ),
        songwriters: yup.lazy((obj) =>
          yup.object(
            mapValidations(
              obj,
              yup.object({
                artist_name: yup
                  .string()
                  .test('Uniqueness', 'duplicate_songwriter', (v) => {
                    if (
                      Object.values(obj)
                        .map((s) => s.artist_name)
                        .filter((s) => s === v).length > 1
                    )
                      return false
                    return true
                  })
                  .trim()
                  .required('required_field'),
              })
            )
          )
        ),
      })
    }

    case SF3:
      return yup.object().shape({
        clean_version: yup
          .bool()
          .when('explicit', {
            is: false,
            then: yup.bool().required(REQUIRED_FIELD).typeError(REQUIRED_FIELD),
            otherwise: yup.bool().nullable(),
          })
          .when('instrumental', {
            is: true,
            then: yup.bool().nullable(),
            otherwise: yup
              .bool()
              .required(REQUIRED_FIELD)
              .typeError(REQUIRED_FIELD),
          }),
        explicit: yup.bool().when('instrumental', {
          is: true,
          then: yup.bool().nullable(),
          otherwise: yup
            .bool()
            .required(REQUIRED_FIELD)
            .typeError(REQUIRED_FIELD),
        }),
        hasOptionalISRC: yup.bool().required(REQUIRED_FIELD),
        instrumental: yup
          .bool()
          .required(REQUIRED_FIELD)
          .typeError(REQUIRED_FIELD),
        language_code_id: yup.number().when('instrumental', {
          is: false,
          then: yup.number().required(REQUIRED_FIELD),
          otherwise: yup.number().nullable(),
        }),
        optional_isrc: yup
          .string()
          .nullable()
          .test('len', 'Must be exactly 12 characters', (val) => {
            if (!val) return true

            return val.length === 12
          }),
        previouslyReleased: yup.bool().required(REQUIRED_FIELD),
        previously_released_at: yup.string().nullable(),
      })
    default:
      break
  }
}

function copyrightsValidator(copyrightsFormEnabled) {
  if (!copyrightsFormEnabled) return yup.object().nullable()

  return yup
    .object()
    .shape({
      composition: yup
        .bool()
        .oneOf([true], REQUIRED_FIELD)
        .required(REQUIRED_FIELD),
      recording: yup
        .bool()
        .oneOf([true], REQUIRED_FIELD)
        .required(REQUIRED_FIELD),
    })
    .required(REQUIRED_FIELD)
    .typeError(REQUIRED_FIELD)
}

function mapValidations(obj, validation) {
  return Object.keys(obj).reduce(
    (newObj, key) => ({ ...newObj, [key]: validation }),
    {}
  )
}
