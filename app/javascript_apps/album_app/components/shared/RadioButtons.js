import React from 'react'
import PropTypes from 'prop-types'

import InputLabel from '@material-ui/core/InputLabel'

import { PrimaryButton } from './MUIButtons'
import FieldWrapper from './FieldWrapper'

const styles = {
  button: { marginRight: '1em' },
  container: {
    display: 'flex',
    margin: '1em 0 0',
  },
}

const CONTAINED = 'contained'
const OUTLINED = 'outlined'
export default function RadioButtons({
  hide,
  name,
  setFieldValue,
  translation,
  value,
}) {
  function handleClick(newValue) {
    setFieldValue(name, newValue)
  }

  // tri-state: contained = true, outlined = false, both outlined = null
  const yesVariant = getVariant(value, true)
  const noVariant = getVariant(value, false)

  if (hide) return null

  return (
    <FieldWrapper>
      <InputLabel htmlFor={name}>{translation}</InputLabel>
      <div style={styles.container}>
        <div style={styles.button}>
          <PrimaryButton
            aria-selected={value === true}
            data-testid={`${name}-yes`}
            onClick={() => handleClick(true)}
            variant={yesVariant}
          >
            Yes
          </PrimaryButton>
        </div>
        <div style={styles.button}>
          <PrimaryButton
            aria-selected={value === false}
            data-testid={`${name}-no`}
            onClick={() => handleClick(false)}
            value="false"
            variant={noVariant}
          >
            No
          </PrimaryButton>
        </div>
      </div>
    </FieldWrapper>
  )
}
RadioButtons.propTypes = {
  defaultValue: PropTypes.bool,
  hide: PropTypes.bool,
  name: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  translation: PropTypes.string.isRequired,
  value: PropTypes.bool,
}

function getVariant(value, button) {
  if (value === null) return OUTLINED

  if (button === true) {
    return value === true ? CONTAINED : OUTLINED
  } else if (button === false) {
    return value === true ? OUTLINED : CONTAINED
  }
}
