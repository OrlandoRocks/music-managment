import React from 'react'
import { TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

function WrappedTextField(props) {
  const classes = useStyles()

  return <TextField {...props} className={classes.input} />
}

export default React.memo(WrappedTextField)

// Override input styles from Rails
const useStyles = makeStyles(() => ({
  input: {
    '& input, & textarea': {
      border: 'none',
      boxShadow: 'none',
      '&:focus': {
        border: 'none',
        boxShadow: 'none',
      },
    },
  },
}))
