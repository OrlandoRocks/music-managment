import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'

import FieldWrapper from '../shared/FieldWrapper'

const useStyles = makeStyles(() => ({
  selectReset: {
    '&:focus': {
      border: 'none',
    },
  },
}))

export default function NativeSelect({
  handleChange,
  hide,
  labelTranslation,
  name,
  options,
  showBlank = true,
  value,
  ...props
}) {
  const classes = useStyles()

  if (hide) return null

  return (
    <FieldWrapper>
      <FormControl variant="outlined" fullWidth>
        <InputLabel htmlFor={name}>{labelTranslation}</InputLabel>
        <Select
          inputProps={{
            className: classes.selectReset,
            name,
            id: name,
          }}
          label={labelTranslation}
          native
          onChange={handleChange}
          value={value}
          {...props}
        >
          {showBlank && <option aria-label="None" value="" />}
          {options}
        </Select>
      </FormControl>
    </FieldWrapper>
  )
}
NativeSelect.propTypes = {
  handleChange: PropTypes.func.isRequired,
  hide: PropTypes.bool,
  labelTranslation: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  showBlank: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}
