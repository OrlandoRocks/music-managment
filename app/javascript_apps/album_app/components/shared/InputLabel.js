import React from 'react'
import PropTypes from 'prop-types'

import InputLabel from '@material-ui/core/InputLabel'

const styles = {
  margin: '0 0 1em',
}

export default function WrappedInputLabel({ labelTranslation, ...props }) {
  return (
    <InputLabel style={styles} {...props}>
      {labelTranslation}
    </InputLabel>
  )
}
WrappedInputLabel.propTypes = {
  labelTranslation: PropTypes.string.isRequired,
}
