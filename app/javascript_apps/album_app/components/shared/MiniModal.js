import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import { AlbumModal } from '../AlbumModal'
import { NegativePositiveButtons } from '../../components/shared/NegativePositiveButtons'
import { styles as buttonStyles } from '../shared/Button'

const useStyles = makeStyles(() => ({
  miniBody: {
    padding: '1em 0',
    // '@media screen and (max-width: 767px)': {
    //   height: '100vh',
    //   maxWidth: '100vw',
    // },
  },
  miniBTNsContainer: {
    display: 'flex',
    justifyContent: 'center',
    padding: '.5em 0 0',
  },
  mainP: { fontSize: '.9em', lineHeight: '1.4em' },
}))

function MiniModalBody({
  message,
  modalActionCallback,
  negativeAction,
  negativeText,
  positiveAction,
  positiveText,
}) {
  const negativeHandler = () => modalActionCallback(negativeAction)
  const positiveHandler = () => modalActionCallback(positiveAction)

  const classes = useStyles()

  const legacyButtonStyles = {
    positive: { ...buttonStyles.shared, marginRight: '1em' },
    negative: { ...buttonStyles.shared, ...buttonStyles.secondary },
  }

  return (
    <div className={classes.miniBody}>
      <p className={classes.mainP}>{message}</p>
      <div className={classes.miniBTNsContainer}>
        <NegativePositiveButtons
          handleNegative={negativeHandler}
          handlePositive={positiveHandler}
          negativeText={negativeText}
          positiveText={positiveText}
          positiveStyle={legacyButtonStyles.positive}
          negativeStyle={legacyButtonStyles.negative}
        />
      </div>
    </div>
  )
}

const miniStyle = { content: { height: 'auto' } }
export default function MiniModal(props) {
  return (
    <AlbumModal
      BodyComponent={MiniModalBody}
      customStyles={miniStyle}
      {...props}
    />
  )
}

MiniModalBody.propTypes = {
  message: PropTypes.string,
  modalActionCallback: PropTypes.func,
  negativeAction: PropTypes.func,
  negativeText: PropTypes.string,
  positiveAction: PropTypes.func,
  positiveText: PropTypes.string,
}
