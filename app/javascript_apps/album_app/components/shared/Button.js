import React from 'react'
import PropTypes from 'prop-types'

// TODO: migrate to MUI buttons
export const styles = {
  shared: {
    borderRadius: 6,
    background: '#44b1fa',
    color: '#fff',
    cursor: 'pointer',
    padding: '.75em',
    margin: 0,
    verticalAlign: 'middle',
  },
  secondary: {
    background: '#eaf5fe',
    border: '1px solid #eaf5fe',
    color: '#5ba7e9',
  },
  back: {
    background: 'transparent',
    marginLeft: '-1em',
  },
  centered: {
    display: 'block',
    margin: '0 auto',
  },
  disabled: {
    cursor: 'not-allowed',
    opacity: '.2',
  },
  exclusion: {
    background: '#ffffff',
    border: '1px solid #eb3a24',
    color: '#eb3a24',
    padding: '.5em',
  },
  inclusion: {
    background: '#ffffff',
    border: '1px solid #52B453',
    color: '#52B453',
    padding: '.5em',
  },
  large: {
    width: '100%',
  },
  small: {
    margin: 0,
    padding: '.5em',
  },
  unstyled: {
    background: '#F4F5FA',
    color: '#7C8692',
    padding: '.5em',
  },
}

function Button({
  className = '',
  children,
  customRef,
  additionalStyleKey = '',
  ...props
}) {
  const additionalStyles = styles[additionalStyleKey] || {}
  const mergedStyles = { ...styles.shared, ...additionalStyles }
  const finalClassName = className ? `button ${className}` : 'button'

  return (
    <button
      ref={customRef}
      style={mergedStyles}
      className={finalClassName}
      type="button"
      {...props}
    >
      {children}
    </button>
  )
}
Button.propTypes = {
  additionalStyleKey: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node,
  customRef: PropTypes.object,
}

const smallStyles = { ...styles.shared, ...styles.small }

export function SmallButton({ children, styleOverrides = {}, ...props }) {
  const styles = { ...smallStyles, ...styleOverrides }

  return (
    <button style={styles} {...props}>
      {children}
    </button>
  )
}
SmallButton.propTypes = {
  styleOverrides: PropTypes.object,
  children: PropTypes.node,
}

export default React.memo(Button)
