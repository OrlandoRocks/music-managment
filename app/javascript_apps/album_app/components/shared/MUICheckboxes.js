import React from 'react'
import PropTypes from 'prop-types'

import { Checkbox, FormControlLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { primaryColor, lightGrayColor } from './theme'

function PrimaryCheckbox(props) {
  const { label, ...rest } = props
  const { checkboxPrimary, checkboxLabelPrimary } = styles()
  return (
    <FormControlLabel
      className={checkboxLabelPrimary}
      control={
        <Checkbox
          className={checkboxPrimary}
          disableRipple
          variant="contained"
          {...rest}
        />
      }
      label={label}
    />
  )
}

function PrimaryCheckboxWithTagline(props) {
  const { tagline, ...rest } = props
  const { lightGreyText } = styles()

  return (
    <div>
      <PrimaryCheckbox {...rest} />
      {tagline && <p className={lightGreyText}>{tagline}</p>}
    </div>
  )
}

export { PrimaryCheckbox, PrimaryCheckboxWithTagline }

PrimaryCheckbox.propTypes = {
  label: PropTypes.string,
}

PrimaryCheckboxWithTagline.propTypes = {
  tagline: PropTypes.string,
}

const styles = makeStyles(() => ({
  checkboxPrimary: {
    color: primaryColor,
    '&.Mui-checked': {
      color: primaryColor,
    },
  },
  checkboxLabelPrimary: {
    color: primaryColor,
  },
  lightGreyText: {
    color: lightGrayColor,
  },
}))
