import React from 'react'
import PropTypes from 'prop-types'

const styles = {
  h2: {
    fontWeight: 700,
  },
  h4: {
    marginBottom: '2em',
  },
}

export function FormSectionTitle({ translation }) {
  return <h2 style={styles.h2}>{translation}</h2>
}
FormSectionTitle.propTypes = {
  translation: PropTypes.string.isRequired,
}

export function FormSectionSubtitle({ translation }) {
  return <h4 style={styles.h4}>{translation}</h4>
}
FormSectionSubtitle.propTypes = {
  translation: PropTypes.string.isRequired,
}
