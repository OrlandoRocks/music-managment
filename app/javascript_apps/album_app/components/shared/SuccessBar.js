import React, { useEffect } from 'react'

import { Snackbar } from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import { makeStyles } from '@material-ui/core/styles'

import { useSnackbars } from '../../contexts/SnackbarContext'

export default function SuccessBar() {
  const classes = useStyles()
  const { setState, state } = useSnackbars()

  const [open, setOpen] = React.useState(false)
  const [messageInfo, setMessageInfo] = React.useState(undefined)

  const handleClose = (_, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  const handleExited = () => {
    setMessageInfo(undefined)
  }

  useEffect(() => {
    if (state.length && !messageInfo) {
      setMessageInfo(state[0])
      setState(state.slice(1))
      setOpen(true)
    } else if (state.length && messageInfo && open) {
      setOpen(false)
    }
  }, [messageInfo, open, setState, state])

  const message = messageInfo ? messageInfo.message : undefined

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      autoHideDuration={4000}
      className={classes.container}
      key={messageInfo ? messageInfo.key : undefined}
      message={message}
      onClose={handleClose}
      onExited={handleExited}
      open={open}
    >
      <div className={classes.successSnack}>
        <p>{message}</p>
        <CheckIcon />
      </div>
    </Snackbar>
  )
}

const useStyles = makeStyles({
  container: {
    margin: '0 auto',
    width: 'fit-content',
  },
  successSnack: {
    width: '300px',
    boxShadow: '0px 12px 19px #00000042',
    borderRadius: '4px',
    color: 'white',
    background: 'black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '10px',
    '& svg': {
      color: 'lime',
    },
    '& p': {
      marginBottom: '0px',
    },
  },
})
