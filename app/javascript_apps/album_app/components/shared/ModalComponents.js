import React from 'react'
import PropTypes from 'prop-types'

export const styles = {
  modalHeaderText: {
    fontSize: '1.5em',
    fontWeight: 700,
  },
  modalOuter: {
    padding: '1em',
    width: '700px',
  },
  vSpacer1: {
    padding: '0 1em',
  },
}

export function ModalHeader({ children }) {
  return <h1 style={styles.modalHeaderText}>{children}</h1>
}

ModalHeader.propTypes = {
  children: PropTypes.node,
}
