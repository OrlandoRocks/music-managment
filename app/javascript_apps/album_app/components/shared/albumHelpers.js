import produce from 'immer'
import { defaultCreative } from '../../utils/defaultCreative'
import { initialGenres } from '../AlbumFormFields/Genres'
import { SPECIALIZED_RELEASE_TYPES } from '../../utils/constants'

import {
  canSkipSubmit,
  skipSubmit,
} from '../DistributionStepper/distributionStepperHelpers'

const FIELDS = {
  clean_version: 'clean_version',
  creatives: 'creatives',
  is_various: 'is_various',
  golive_date: 'golive_date',
  label_name: 'label_name',
  language_code: 'language_code',
  name: 'name',
  optional_isrc: 'optional_isrc',
  optional_upc_number: 'optional_upc_number',
  parental_advisory: 'parental_advisory',
  recording_location: 'recording_location',
  sale_date: 'sale_date',
  selected_countries: 'selected_countries',
  timed_release_timing_scenario: 'timed_release_timing_scenario',
}
Object.freeze(FIELDS)

const ABSOLUTE = 'absolute_time'
const AM = 'AM'
const GO_LIVE_TZ_DISPLAY_VALUE = 'America/New York' // The user is instructed to submit a value for this zone.
function formattedGoLiveDate(date, scenario, locale) {
  const { day, hour, meridian, min, month, year } = date
  const h = meridian === AM ? hour - 12 : hour
  const d = new Date(year, month - 1, day, h, min, 0)
  const tString = d.toLocaleTimeString(locale, {
    hour: '2-digit',
    minute: '2-digit',
  })

  return scenario === ABSOLUTE
    ? `${tString} ${GO_LIVE_TZ_DISPLAY_VALUE}`
    : tString
}

function formattedReleaseDate(date, locale) {
  const result = new Date(date)
  return result.toLocaleDateString(locale, { timeZone: 'UTC' })
}

function getGenre(genres, id) {
  if (!id) return {}

  return genres.find((v) => v.id === parseInt(id, 10))
}

const getLanguage = (languageCodes, code) =>
  Object.values(languageCodes).find((v) => v.code === code)

const extractCreativeNames = (album) => album.creatives.map((c) => c.name)
function updatePossibleArtists(album, setConfig) {
  const creativeNames = extractCreativeNames(album)
  setConfig((prev) => {
    const newPossibleArtists = new Set([
      ...prev.possibleArtists,
      ...creativeNames,
    ])
    return produce(prev, (draft) => {
      draft.possibleArtists = [...newPossibleArtists]
    })
  })
}

function handleErrors(errors, setManualErrorScollTrigger) {
  if (Object.keys(errors).length) {
    setManualErrorScollTrigger((prev) => prev + 1)
  }
}

function skipOrContinue({
  config,
  distributionActions,
  distributionState,
  formikBag,
  history,
  setManualErrorScollTrigger,
}) {
  const { handleReleaseSubmission } = distributionActions
  const { activeStep, stores } = distributionState
  const { dirty, errors, handleSubmit } = formikBag

  if (canSkipSubmit({ config, dirty, errors })) {
    skipSubmit({
      activeStep,
      config,
      handleReleaseSubmission,
      history,
      stores,
    })
  } else {
    handleErrors(errors, setManualErrorScollTrigger)
    handleSubmit()
  }
}

const isSpecializedRelease = (config) =>
  SPECIALIZED_RELEASE_TYPES.includes(
    config.releaseTypeVars.specializedReleaseType
  )
const needsBlankCreative = (creatives, config) =>
  Object.keys(creatives).length == 0 && isSpecializedRelease(config)

function prepareInitialValues(album, config) {
  const albumCopy = { ...album }
  const { creatives, primary_genre_id, secondary_genre_id } = albumCopy
  const { countryWebsite, genres } = config

  if (needsBlankCreative(creatives, config)) {
    const newCreative = defaultCreative()
    creatives[newCreative.uuid] = newCreative
  }

  return {
    ...albumCopy,
    ...initialGenres({
      countryWebsite,
      genres,
      primary_genre_id,
      secondary_genre_id,
    }),
    creatives,
  }
}

export {
  FIELDS,
  formattedGoLiveDate,
  formattedReleaseDate,
  getGenre,
  getLanguage,
  handleErrors,
  prepareInitialValues,
  skipOrContinue,
  updatePossibleArtists,
}
