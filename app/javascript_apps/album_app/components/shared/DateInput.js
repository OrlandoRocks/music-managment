import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import Button from '../shared/Button'

// This is the button-style date input
export const DateInput = forwardRef(({ id, onClick, value }, _) => (
  <Button
    id={id}
    onClick={(e) => {
      e.preventDefault()
      onClick()
    }}
    type="button"
  >
    {value}
  </Button>
))
DateInput.displayName = 'DateInput'

DateInput.propTypes = {
  id: PropTypes.string,
  onClick: PropTypes.func,
  value: PropTypes.string,
}
