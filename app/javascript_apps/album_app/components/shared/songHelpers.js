import { isEmpty } from '../../../utils'
import {
  canSkipSubmit,
  skipSubmit,
} from '../DistributionStepper/distributionStepperHelpers'

export async function skipOrContinueSF({
  activeStep,
  config,
  file,
  formikBag,
  history,
  songs,
  setState,
}) {
  const { dirty, handleSubmit, validateForm } = formikBag

  const finalErrors = await validateForm()

  switch (true) {
    case canSkipSubmit({ config, dirty, errors: finalErrors, file }):
      skipSubmit({ activeStep, history, songs })
      break
    case !isEmpty(finalErrors):
      setState((prev) => ({
        // must not unset state.ready once it is set, or form will reload and lose state
        ...prev,
        manualErrorScrollTrigger: prev.manualErrorScrollTrigger + 1,
      }))
      break
    default:
      handleSubmit()
  }
}
