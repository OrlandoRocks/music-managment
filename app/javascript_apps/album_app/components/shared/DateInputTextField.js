import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import { IconButton, InputAdornment } from '@material-ui/core'
import EventIcon from '@material-ui/icons/Event'

import TextField from './TextField'

export const DateInputTextField = forwardRef(
  ({ onClick, placeholder, value }, _) => {
    return (
      <TextField
        fullWidth
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton onClick={onClick}>
                <EventIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
        onClick={onClick}
        placeholder={placeholder}
        value={value}
        variant="outlined"
      />
    )
  }
)
DateInputTextField.displayName = 'DateInputTextField'

DateInputTextField.propTypes = {
  onClick: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
}
