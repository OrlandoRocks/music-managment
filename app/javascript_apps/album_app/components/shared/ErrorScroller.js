import React, { useEffect } from 'react'

const PROGRESS_BAR_HEIGHT = -57
function ErrorScroller({ errors, trigger }) {
  useEffect(() => {
    const element = document.getElementsByClassName('missing')[0]
    if (!element) return

    const y =
      element.getBoundingClientRect().top +
      window.pageYOffset +
      PROGRESS_BAR_HEIGHT
    window.scrollTo({ top: y, behavior: 'smooth' })
  }, [errors, trigger])

  return null
}

export default React.memo(ErrorScroller)
