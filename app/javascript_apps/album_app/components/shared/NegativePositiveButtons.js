import React from 'react'
import PropTypes from 'prop-types'

export function NegativePositiveButtons({
  handleNegative,
  handlePositive,
  negativeText,
  positiveText,
  positiveStyle = {},
  negativeStyle = {},
}) {
  return (
    <div className="negative-positive-buttons-container">
      <div className="row">
        <button
          className="button positive-button"
          data-testid="positive button"
          name="positive button"
          onClick={handlePositive}
          style={positiveStyle}
          type="button"
        >
          {positiveText}
        </button>
        <button
          className="button negative-button"
          data-testid="negative button"
          onClick={handleNegative}
          style={negativeStyle}
          type="button"
        >
          {negativeText}
        </button>
      </div>
    </div>
  )
}

NegativePositiveButtons.propTypes = {
  handleNegative: PropTypes.func.isRequired,
  handlePositive: PropTypes.func.isRequired,
  negativeText: PropTypes.string.isRequired,
  positiveText: PropTypes.string.isRequired,
  positiveStyle: PropTypes.object,
  negativeStyle: PropTypes.object,
}
