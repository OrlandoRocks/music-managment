import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'

import Spacer from '../../../shared/Spacer'

import { setStepParam } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'

import { isEmpty } from '../../../utils'

const DEFAULT_ARTWORK_URL = '/images/default_cover_art.jpg'

const styles = {
  editButton: {
    padding: '6px',
  },
  editButtonRed: {
    padding: '6px',
    color: 'red',
  },
  headlineContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
  },
  artwork: {
    height: '335px',
  },
  artworkContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
}

function EditButton({ disabled, handleClick, incomplete }) {
  const style = incomplete ? styles.editButtonRed : styles.editButton

  return (
    <IconButton
      aria-label="edit stores"
      color="primary"
      disabled={disabled}
      onClick={handleClick}
      style={style}
    >
      <EditIcon />
    </IconButton>
  )
}

EditButton.propTypes = {
  disabled: PropTypes.bool,
  incomplete: PropTypes.bool,
  handleClick: PropTypes.func,
}

export default function Artwork({ artworkUrl }) {
  const history = useHistory()

  const incomplete = isEmpty(artworkUrl)

  function handleClick() {
    setStepParam(STEP_PARAMS.art, history)
  }

  return (
    <div>
      <div style={styles.headlineContainer}>
        <h2>Album Artwork:</h2>
        <EditButton handleClick={handleClick} incomplete={incomplete} />
      </div>

      <div style={styles.artworkContainer}>
        <img
          src={artworkUrl || DEFAULT_ARTWORK_URL}
          alt="Add Artwork"
          style={styles.artwork}
        />
      </div>

      <Spacer height={45} />
    </div>
  )
}

Artwork.propTypes = {
  artworkUrl: PropTypes.string,
}
