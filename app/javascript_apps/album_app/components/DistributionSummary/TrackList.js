import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import DragHandleIcon from '@material-ui/icons/DragHandle'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import PauseIcon from '@material-ui/icons/Pause'
import Skeleton from '@material-ui/lab/Skeleton'

import MiniModal from '../shared/MiniModal'

import { destroy } from '../../utils/fetch'
import { distributionProgressRequest } from '../../utils/distributionProgressRequest'
import { isEmpty } from '../../../utils'
import { ALBUM_TYPE, SONG_DATA_ENDPOINT } from '../../utils/constants'

import { useDistributionFlow } from '../../contexts/DistributionContext'

const styles = {
  assetFilename: {
    marginBottom: 0,
    color: 'grey',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  buttonsContainer: {
    width: '230px',
    textAlign: 'right',
  },
  deleteButton: {
    marginRight: '10px',
    padding: '6px',
  },
  dragHandle: {
    marginLeft: '10px',
    padding: '6px',
  },
  editButton: {
    padding: '6px',
  },
  playbackButton: {
    marginRight: '10px',
    padding: '6px',
  },
  trackName: {
    marginBottom: 0,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  trackContainer: {
    alignItems: 'center',
    borderBottom: '1px solid #DBE2E9',
    display: 'flex',
    justifyContent: 'space-between',
    padding: '.5em 20px',
  },
  tracksContainer: {
    borderTop: '1px solid #DBE2E9',
    margin: '0 -20px 1em',
  },
}

function DeleteButton({ handleClick, showDelete, uuid }) {
  if (!showDelete) {
    return null
  }

  return (
    <IconButton
      aria-label="delete"
      onClick={handleClick}
      name={uuid}
      style={styles.deleteButton}
    >
      <DeleteIcon />
    </IconButton>
  )
}

DeleteButton.propTypes = {
  uuid: PropTypes.string,
  showDelete: PropTypes.bool,
  handleClick: PropTypes.func,
}

// TODO: aria translation
function EditButton({ disabled, handleClick, uuid }) {
  return (
    <IconButton
      aria-label="edit song"
      color="primary"
      disabled={disabled}
      onClick={handleClick}
      name={uuid}
      style={styles.editButton}
    >
      <EditIcon />
    </IconButton>
  )
}

EditButton.propTypes = {
  uuid: PropTypes.string,
  disabled: PropTypes.bool,
  handleClick: PropTypes.func,
}

function DragButton({ isDragDisabled }) {
  if (isDragDisabled) {
    return null
  }

  return (
    <IconButton aria-label="reorder" style={styles.dragHandle}>
      <DragHandleIcon />
    </IconButton>
  )
}

DragButton.propTypes = {
  isDragDisabled: PropTypes.bool,
}

function PlaybackButton({ isPlaying, song, togglePlayback }) {
  if (!song.data.asset_url) {
    return null
  }

  if (isPlaying) {
    return (
      <IconButton
        aria-label="pause"
        style={styles.playbackButton}
        onClick={togglePlayback}
      >
        <PauseIcon />
      </IconButton>
    )
  }

  return (
    <IconButton
      aria-label="play"
      style={styles.playbackButton}
      onClick={togglePlayback}
    >
      <PlayArrowIcon />
    </IconButton>
  )
}

PlaybackButton.propTypes = {
  song: PropTypes.object,
  isPlaying: PropTypes.bool,
  togglePlayback: PropTypes.func,
}

function Track({ album, handleDelete, handleEdit, isDragDisabled, song }) {
  const [isPlaying, setIsPlaying] = useState(false)
  const [endHandled, setEndHandled] = useState(false)

  const showDelete = album.album_type === ALBUM_TYPE
  const audioRef = useRef()

  function togglePlayback() {
    if (!endHandled) {
      audioRef.current.onended = () => setIsPlaying(false)
      setEndHandled(true)
    }
    isPlaying ? audioRef.current.pause() : audioRef.current.play()
    setIsPlaying((prev) => !prev)
  }

  return (
    <div style={styles.trackContainer}>
      <p style={styles.trackName}>{song.data.name}</p>
      <p style={styles.assetFilename}>{song.data.asset_filename}</p>
      <div style={styles.buttonsContainer}>
        <audio ref={audioRef} src={song.data.asset_url} />
        <PlaybackButton
          isPlaying={isPlaying}
          song={song}
          togglePlayback={togglePlayback}
        />
        <DeleteButton
          handleClick={handleDelete}
          showDelete={showDelete}
          uuid={song.uuid}
        />
        <EditButton
          disabled={!isDragDisabled}
          handleClick={handleEdit}
          uuid={song.uuid}
        />
        <DragButton isDragDisabled={isDragDisabled} />
      </div>
    </div>
  )
}

Track.propTypes = {
  album: PropTypes.object,
  song: PropTypes.object,
  isDragDisabled: PropTypes.bool,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
}

function DraggableTrack({ index, isDragDisabled, song, ...rest }) {
  return (
    <Draggable
      isDragDisabled={isDragDisabled}
      draggableId={song.data.id.toString()}
      index={index}
    >
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Track isDragDisabled={isDragDisabled} song={song} {...rest} />
        </div>
      )}
    </Draggable>
  )
}

DraggableTrack.propTypes = {
  song: PropTypes.object,
  isDragDisabled: PropTypes.bool,
  index: PropTypes.number,
}

export default function TrackList({
  isDragDisabled,
  onDragEnd,
  removeTrack,
  tracks,
}) {
  const [
    { album, fetchedSongsData, songs },
    {
      deleteSong,
      editSong,
      restoreSong,
      setDistributionProgressLevel,
      setSongsComplete,
      setFetchingProgressLevels,
    },
  ] = useDistributionFlow()
  const [forDeletion, setForDeletion] = useState(null)
  const [showDeleteModal, setShowDeleteModal] = useState(false)

  async function handleDelete(e) {
    const { name: uuid } = e.currentTarget
    setForDeletion(uuid)
    setShowDeleteModal(true)
  }

  function cancelDeleteTrack() {
    setForDeletion(null)
  }

  async function deleteTrack() {
    const id = songs[forDeletion].data.id
    const deletedSong = { ...songs[forDeletion] }

    try {
      setFetchingProgressLevels(true)

      deleteSong(forDeletion)
      removeTrack(forDeletion)
      const res = await destroy(`${SONG_DATA_ENDPOINT}/${id}`)

      if (res.ok) {
        const distributionProgressResponse = await distributionProgressRequest({
          albumId: album.id,
        })
        const distributionProgressData = await distributionProgressResponse.json()

        if (distributionProgressResponse.ok) {
          const {
            distribution_progress_level,
            songs_complete,
          } = distributionProgressData

          setDistributionProgressLevel(distribution_progress_level)
          setSongsComplete(songs_complete)

          setFetchingProgressLevels(false)
        }
      } else {
        restoreSong(deletedSong)
        // TODO: pop error
      }
    } catch (e) {
      // TODO:
    }
  }

  function handleEdit(e) {
    const { name: uuid } = e.currentTarget
    const track = songs[uuid].data.track_number
    editSong(uuid, track)
  }

  if (!fetchedSongsData) {
    const height = 53 // the height of a track row

    return (
      <>
        <Skeleton height={height} />
        <Skeleton height={height} />
        <Skeleton height={height} />
      </>
    )
  }

  if (isEmpty(songs)) {
    return 'No songs found.'
  }

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="tracklist">
          {(provided) => (
            <div
              style={styles.tracksContainer}
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              {tracks.map((s, index) => (
                <DraggableTrack
                  key={s.uuid}
                  index={index}
                  album={album}
                  handleDelete={handleDelete}
                  handleEdit={handleEdit}
                  isDragDisabled={isDragDisabled}
                  song={s}
                />
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      <MiniModal
        contentLabel="Delete Song Confirmation Modal"
        headerMessage="Delete Song"
        message="Are you sure you want to delete the song?"
        negativeAction={cancelDeleteTrack}
        negativeText="Cancel"
        positiveAction={deleteTrack}
        positiveText="Delete"
        setShow={setShowDeleteModal}
        show={showDeleteModal}
      />
    </>
  )
}

TrackList.propTypes = {
  removeTrack: PropTypes.func,
  tracks: PropTypes.array,
  isDragDisabled: PropTypes.bool,
  onDragEnd: PropTypes.func,
}
