import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'

import Box from '@material-ui/core/Box'

import StoreIcon from '../../../shared/StoreIcon'
import Spacer from '../../../shared/Spacer'

import { setStepParam } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'

import { isEmpty } from '../../../utils'

const styles = {
  editButton: {
    padding: '6px',
  },
  editButtonRed: {
    padding: '6px',
    color: 'red',
  },
  headlineContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
  },
}

function EditButton({ disabled, handleClick, incomplete }) {
  const style = incomplete ? styles.editButtonRed : styles.editButton

  return (
    <IconButton
      aria-label="edit stores"
      color="primary"
      disabled={disabled}
      onClick={handleClick}
      style={style}
    >
      <EditIcon />
    </IconButton>
  )
}

EditButton.propTypes = {
  incomplete: PropTypes.bool,
  disabled: PropTypes.bool,
  handleClick: PropTypes.func,
}

function Store({ store }) {
  const { custom_abbrev } = store

  return <StoreIcon abbrev={custom_abbrev} />
}

Store.propTypes = {
  store: PropTypes.object,
}

export default function StoreList({ stores }) {
  const history = useHistory()

  const incomplete = isEmpty(stores)

  function handleClick() {
    setStepParam(STEP_PARAMS.st, history)
  }

  return (
    <div>
      <div style={styles.headlineContainer}>
        <h2>Stores:</h2>
        <EditButton handleClick={handleClick} incomplete={incomplete} />
      </div>

      <Box display="flex" flexWrap="wrap" p={1} bgcolor="background.paper">
        {stores.map((store) => (
          <Box p={1} key={store.id}>
            <Store store={store} />
          </Box>
        ))}
      </Box>

      <Spacer height={45} />
    </div>
  )
}

StoreList.propTypes = {
  stores: PropTypes.array.isRequired,
}
