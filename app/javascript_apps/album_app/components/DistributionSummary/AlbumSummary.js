import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'

import Spacer from '../../../shared/Spacer'
import {
  formattedGoLiveDate,
  formattedReleaseDate,
  getGenre,
  getLanguage,
} from '../shared/albumHelpers'

import { useConfig } from '../../contexts/ConfigContext'
import { setStepParam } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'

const styles = {
  editButton: {
    padding: '6px',
  },
  headlineContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
  },
  light: {
    fontWeight: 200,
    marginBottom: 0,
  },
}

export default function AlbumSummary({ album }) {
  const { genres, languageCodes, locale } = useConfig()[0]
  const history = useHistory()

  function handleClick() {
    setStepParam(STEP_PARAMS.a1, history)
  }

  return (
    <>
      <Spacer height={30} />

      <div style={styles.headlineContainer}>
        <h2>{album.name}</h2>
        <EditButton handleClick={handleClick} />
      </div>
      <Light translationKey="UPC" value={album.optional_upc_number} />
      <Light
        translationKey="Primary Genre"
        value={getGenre(genres, album.primary_genre_id).name}
      />
      <Light
        translationKey="Secondary Genre"
        value={getGenre(genres, album.secondary_genre_id).name}
      />
      <Light
        translationKey="Language"
        value={getLanguage(languageCodes, album.language_code).description}
      />
      <Light translationKey="Label" value={album.label_name} />
      <Light
        translationKey="Recording Location"
        value={album.recording_location}
      />
      <Light
        translationKey="Release Date"
        value={formattedReleaseDate(album.sale_date)}
      />
      <Light
        translationKey="Release Time"
        value={formattedGoLiveDate(
          album.golive_date,
          album.timed_release_timing_scenario,
          locale
        )}
      />

      <Spacer height={45} />
    </>
  )
}

AlbumSummary.propTypes = {
  album: PropTypes.object,
}

const EMPTY_VALUE = ''
function Light({ translationKey, value }) {
  const safeValue = value || EMPTY_VALUE

  return <p style={styles.light}>{`${translationKey}: ${safeValue}`}</p>
}

Light.propTypes = {
  translationKey: PropTypes.string,
  value: PropTypes.string,
}

// TODO: aria translation
function EditButton({ handleClick }) {
  return (
    <IconButton
      aria-label="edit album"
      color="primary"
      onClick={handleClick}
      style={styles.editButton}
    >
      <EditIcon />
    </IconButton>
  )
}

EditButton.propTypes = {
  handleClick: PropTypes.func,
}
