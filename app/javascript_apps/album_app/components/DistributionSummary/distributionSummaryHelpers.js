const FETCH_SONG_IDS_ENDPOINT = '/api/backstage/albums/{{ID}}/fetch_song_ids'

function buildSongIDsURL(album) {
  return FETCH_SONG_IDS_ENDPOINT.replace('{{ID}}', album.id)
}

function reorder(list, startIndex, endIndex) {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}

function sortSongs(songs) {
  return Object.values(songs).sort((a, b) => a.track_num - b.track_num)
}

export { buildSongIDsURL, reorder, sortSongs }
