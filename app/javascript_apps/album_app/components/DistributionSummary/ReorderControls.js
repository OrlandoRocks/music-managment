import React from 'react'
import PropTypes from 'prop-types'

import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered'
import SaveIcon from '@material-ui/icons/Save'

import {
  PrimaryButton,
  SecondaryButton,
  PrimaryTextButton,
} from '../shared/MUIButtons'
import { ALBUM_TYPE } from '../../utils/constants'

const styles = {
  cancelButton: {
    marginRight: '10px',
  },
}

export default function ReorderControls({
  album,
  handleCancel,
  handleEdit,
  handleSave,
  isDragDisabled,
}) {
  if (album.album_type !== ALBUM_TYPE) {
    return null
  }

  return (
    <>
      <CancelSaveButtons
        handleCancel={handleCancel}
        handleSave={handleSave}
        isDragDisabled={isDragDisabled}
      />
      <EditButton handleEdit={handleEdit} isDragDisabled={isDragDisabled} />
    </>
  )
}

ReorderControls.propTypes = {
  album: PropTypes.object,
  isDragDisabled: PropTypes.bool,
  handleCancel: PropTypes.func,
  handleEdit: PropTypes.func,
  handleSave: PropTypes.func,
}

function CancelSaveButtons({ handleCancel, handleSave, isDragDisabled }) {
  if (isDragDisabled) {
    return null
  }

  return (
    <div>
      <SecondaryButton
        aria-label="cancel"
        onClick={handleCancel}
        style={styles.cancelButton}
      >
        Cancel
      </SecondaryButton>
      <PrimaryButton aria-label="save" onClick={handleSave}>
        <SaveIcon />
        &nbsp;Save Song Order
      </PrimaryButton>
    </div>
  )
}

CancelSaveButtons.propTypes = {
  isDragDisabled: PropTypes.bool,
  handleCancel: PropTypes.func,
  handleSave: PropTypes.func,
}

function EditButton({ handleEdit, isDragDisabled }) {
  if (!isDragDisabled) {
    return null
  }

  return (
    <PrimaryTextButton aria-label="re-order" onClick={handleEdit}>
      <FormatListNumberedIcon />
      &nbsp;Edit Song Order
    </PrimaryTextButton>
  )
}

EditButton.propTypes = {
  isDragDisabled: PropTypes.bool,
  handleEdit: PropTypes.func,
}
