import React, { useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

import AddIcon from '@material-ui/icons/Add'
import Skeleton from '@material-ui/lab/Skeleton'

import AlbumSummary from './AlbumSummary'
import StoreList from './StoreList'
import TrackList from './TrackList'
import Artwork from './Artwork'
import ReorderControls from './ReorderControls'

import { useDistributionFlow } from '../../contexts/DistributionContext'
import { put } from '../../utils/fetch'
import {
  buildSongIDsURL,
  reorder,
  sortSongs,
} from './distributionSummaryHelpers'
import Spacer from '../../../shared/Spacer'
import { PrimaryButton, SecondaryButton } from '../shared/MUIButtons'
import { setStepParam } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'
import { ALBUM_TYPE } from '../../utils/constants'

const REORDER_ENDPOINT = '/api/backstage/albums/'

const styles = {
  continueButtonContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    margin: '1em',
  },
  addToCartButtonContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'flex-end',
    position: 'sticky',
    bottom: '0px',
    backgroundColor: 'white',
    padding: '1em',
    margin: '0 -20px',
  },
  footerControls: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  spacer: {
    height: '300px',
  },
}

// SummaryContainer parses songs into a tracks array, and uses tracks
// instead of songs in order to support re-ordering.
export default function SummaryContainer() {
  const [
    {
      album,
      songs,
      stores,
      freemiumStores,
      distributionProgressLevel,
      songsComplete,
      fetchingProgressLevels,
    },
    { addSong, setSongs },
  ] = useDistributionFlow()
  const { artwork_url } = album
  const history = useHistory()

  const [isDragDisabled, setIsDragDisabled] = useState(true)
  const [tracks, setTracks] = useState(sortSongs(songs)) // Array<song>

  useEffect(() => {
    setTracks(sortSongs(songs))
  }, [songs])

  function onDragEnd(result) {
    if (!result.destination) {
      return
    }

    if (result.destination.index === result.source.index) {
      return
    }

    const newTracks = reorder(
      tracks,
      result.source.index,
      result.destination.index
    )
    setTracks(newTracks)
  }

  function toggleDrag() {
    setIsDragDisabled((prev) => !prev)
  }

  function removeTrack(uuid) {
    const newTracks = tracks.filter((t) => t.uuid !== uuid)
    setTracks(newTracks)
    saveOrder(newTracks)
  }

  function saveOrderBody(tArray) {
    return {
      album: {
        song_order: tArray.map((t) => t.data.id),
      },
    }
  }

  const saveOrder = useCallback(
    async (newTracks = null) => {
      const tArray = newTracks || tracks
      try {
        const res = await put(
          `${REORDER_ENDPOINT}${album.id}`,
          saveOrderBody(tArray)
        )
        if (res.ok) {
          const result = tArray.reduce((acc, cur) => {
            acc[cur.uuid] = cur
            return acc
          }, {})

          setSongs(result)
        } else {
          // do something!
        }
      } catch (e) {
        console.error(e)
      }
    },
    [album.id, setSongs, tracks]
  )

  // If user deleted a song on another device, it can still be in the list here.
  // Prune stale tracks in a potentially jarring way, while they're re-ordering...
  async function handleStaleTracks() {
    const res = await fetch(buildSongIDsURL(album))
    if (res.ok) {
      const { song_ids: ids } = await res.json()
      setTracks(tracks.filter((t) => ids.includes(t.data.id)))
    }
  }

  function handleCancel() {
    toggleDrag()
    setTracks(sortSongs(songs))
  }

  function handleEdit() {
    handleStaleTracks()
    toggleDrag()
  }

  function handleSave() {
    toggleDrag()
    saveOrder()
  }

  function handleContinue() {
    setStepParam(STEP_PARAMS.a2, history)
  }

  const hideAddSong = album.album_type !== ALBUM_TYPE

  const selectedStores = [...stores, ...freemiumStores].filter(
    (store) => store.selected == true
  )

  const distributionProgressLevelCompleted = distributionProgressLevel == 4

  const addToCartDisabled = !(
    distributionProgressLevelCompleted && songsComplete
  )

  return (
    <>
      <h1>Review Album and Add Tracks</h1>
      <AlbumSummary album={album} />
      <StoreList stores={selectedStores} />
      <TrackList
        isDragDisabled={isDragDisabled}
        onDragEnd={onDragEnd}
        removeTrack={removeTrack}
        saveOrder={saveOrder}
        tracks={tracks}
      />
      <div style={styles.footerControls}>
        <SecondaryButton
          hide={hideAddSong}
          onClick={addSong}
          startIcon={<AddIcon />}
        >
          Add Song
        </SecondaryButton>
        <ReorderControls
          album={album}
          handleCancel={handleCancel}
          handleEdit={handleEdit}
          handleSave={handleSave}
          isDragDisabled={isDragDisabled}
          toggleDrag={toggleDrag}
        />
      </div>
      <Spacer height={45} />
      <Artwork artworkUrl={artwork_url} />
      <div style={styles.continueButtonContainer}>
        <PrimaryButton
          aria-label="continue"
          onClick={handleContinue}
          size="large"
        >
          Continue
        </PrimaryButton>
      </div>
      <div style={styles.addToCartButtonContainer}>
        {fetchingProgressLevels ? (
          <Skeleton height={30} width={140} />
        ) : (
          <PrimaryButton
            aria-label="add to cart"
            disabled={addToCartDisabled}
            size="large"
          >
            Add to Cart
          </PrimaryButton>
        )}
      </div>
      <Spacer />
    </>
  )
}
