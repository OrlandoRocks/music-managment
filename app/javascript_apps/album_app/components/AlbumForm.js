import React, { useState } from 'react'
import { Form, Formik } from 'formik'
import PropTypes from 'prop-types'

import AlbumCleanVersion from './AlbumFormFields/AlbumCleanVersion'
import AlbumExplicit from './AlbumFormFields/AlbumExplicit'
import AlbumLabel from './AlbumFormFields/AlbumLabel'
import AlbumLanguage from './AlbumFormFields/AlbumLanguage'
import AlbumRecordingLocation from './AlbumFormFields/AlbumRecordingLocation'
import AlbumTitle from './AlbumFormFields/AlbumTitle'
import ErrorMessage from './shared/ErrorMessage'
import ErrorScroller from './shared/ErrorScroller'
import ExternalServiceIDDataFetcher from './data/ExternalServiceIDDataFetcher'
import GenresContainer from './AlbumFormFields/Genres'
import GoLiveDateFields from './AlbumFormFields/GoLiveDateFields'
import IsVarious from './AlbumFormFields/IsVarious'
import MainArtistsChooser from './AlbumFormFields/MainArtistsChooser.js'
import OptionalIsrc from './AlbumFormFields/OptionalIsrc'
import OptionalUpc from './AlbumFormFields/OptionalUpc'
import PreviouslyReleased from './AlbumFormFields/PreviouslyReleased'
import TerritoryPickerContainer from './AlbumFormFields/TerritoryPicker/TerritoryPickerContainer'
import Submit from './AlbumFormFields/Submit'

import { createAlbumSchema, SCHEMAS } from './shared/albumSchema'
import { albumRequest, parseErrors } from '../utils/albumRequest'
import {
  FIELDS,
  handleErrors,
  prepareInitialValues,
} from './shared/albumHelpers'
import { devLogger } from '../../utils'
import { IS_DEV } from '../utils/constants'

import { useConfig } from '../contexts/ConfigContext'
import AdvancedFeaturesHeader from './AlbumFormFields/AdvancedFeaturesHeader'

const AlbumForm = ({ album, albumSubmitRef, submitHandler }) => {
  const [config, setConfig] = useConfig()

  const {
    releaseTypeVars: { timedReleaseDisabled, specializedReleaseType },
  } = config

  const initialValues = prepareInitialValues(album, config)

  const AlbumSchema = createAlbumSchema(config, SCHEMAS.V1)

  const [manualErrorScrollTrigger, setManualErrorScollTrigger] = useState(0)

  // Keep form disabled during redirect to show page
  const [submitSuccess, setSubmitSuccess] = useState(false)

  const [showMainArtistModal, setShowMainArtistModal] = useState(false)

  const isDisabled = album.finalized || submitSuccess

  return (
    <div className="grid-container div-album-form">
      <Formik
        initialValues={initialValues}
        onSubmit={async (values, { setErrors }) => {
          await submitHandler({
            album,
            config,
            history,
            setConfig,
            setErrors,
            setSubmitSuccess,
            specializedReleaseType,
            values,
          })
        }}
        validationSchema={AlbumSchema}
      >
        {(formikBag) => {
          const {
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            setTouched,
            setValues,
            touched,
            values,
          } = formikBag

          IS_DEV && devLogger(values, 'Album Form Values')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            handleErrors(errors, setManualErrorScollTrigger)
            handleSubmit()
          }

          return (
            <Form data-testid="AlbumForm" onSubmit={wrappedHandleSubmit}>
              <fieldset disabled={isDisabled}>
                <ErrorScroller
                  errors={errors}
                  trigger={manualErrorScrollTrigger}
                />
                <ErrorMessage
                  error={errors[FIELDS.name]}
                  touched={touched[FIELDS.name]}
                />
                <ExternalServiceIDDataFetcher creatives={values.creatives} />
                <AlbumTitle
                  albumType={album.album_type}
                  defaultValue={album.name}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  name={FIELDS.name}
                />
                <ErrorMessage
                  error={showMainArtistModal ? '' : errors[FIELDS.creatives]}
                  touched={touched[FIELDS.creatives]}
                />
                <MainArtistsChooser
                  disabled={isDisabled}
                  modalUtils={{ setShowMainArtistModal, showMainArtistModal }}
                />
                <ErrorMessage
                  error={errors[FIELDS.parental_advisory]}
                  touched={touched[FIELDS.parental_advisory]}
                />
                <AlbumExplicit
                  currentValue={values.parental_advisory}
                  handleChange={handleChange}
                  name={FIELDS.parental_advisory}
                />
                <ErrorMessage
                  error={errors[FIELDS.clean_version]}
                  touched={touched[FIELDS.clean_version]}
                />
                <AlbumCleanVersion
                  currentValue={values.clean_version}
                  isCleanVersion={album.clean_version}
                  handleChange={handleChange}
                  name={FIELDS.clean_version}
                  parentalAdvisory={values.parental_advisory}
                />
                <ErrorMessage
                  error={errors[FIELDS.is_various]}
                  touched={touched[FIELDS.is_various]}
                />
                <IsVarious
                  currentValue={values.is_various}
                  isVarious={album.is_various}
                  name={FIELDS.is_various}
                  setFieldValue={setFieldValue}
                />
                <ErrorMessage
                  error={errors[FIELDS.language_code]}
                  touched={touched[FIELDS.language_code]}
                />
                <AlbumLanguage
                  defaultValue={album.language_code}
                  handleChange={handleChange}
                  name={FIELDS.language_code}
                />
                <GenresContainer
                  errors={errors}
                  handleChange={handleChange}
                  primary_genre_id={values.primary_genre_id}
                  secondary_genre_id={values.secondary_genre_id}
                  sub_genre_id_primary={values.sub_genre_id_primary}
                  sub_genre_id_secondary={values.sub_genre_id_secondary}
                  touched={touched}
                />
                <ErrorMessage
                  error={errors[FIELDS.golive_date] || errors[FIELDS.sale_date]}
                  touched={touched[FIELDS.sale_date]}
                />
                <GoLiveDateFields
                  timedReleaseDisabled={timedReleaseDisabled}
                  values={values}
                  setValues={setValues}
                  handleChange={handleChange}
                  timingScenarioFieldName={FIELDS.timed_release_timing_scenario}
                  album={album}
                  setTouched={setTouched}
                />
                <PreviouslyReleased
                  handleChange={handleChange}
                  isPreviouslyReleased={values.is_previously_released}
                  origReleaseYear={values.orig_release_year}
                  setFieldValue={setFieldValue}
                  setTouched={setTouched}
                />
                <OptionalIsrc
                  defaultValue={album.optional_isrc}
                  handleChange={handleChange}
                  name={FIELDS.optional_isrc}
                />

                <AdvancedFeaturesHeader />

                <ErrorMessage
                  error={errors[FIELDS.label_name]}
                  touched={touched[FIELDS.label_name]}
                />
                <AlbumLabel
                  defaultValue={album.label_name}
                  handleChange={handleChange}
                  name={FIELDS.label_name}
                />

                <AlbumRecordingLocation
                  defaultValue={album.recording_location}
                  handleChange={handleChange}
                  name={FIELDS.recording_location}
                />

                <TerritoryPickerContainer />

                <ErrorMessage
                  error={errors[FIELDS.optional_upc_number]}
                  touched={touched[FIELDS.optional_upc_number]}
                />
                <OptionalUpc
                  defaultValue={album.optional_upc_number}
                  handleChange={handleChange}
                  immutable={!album.is_new && album.finalized}
                  name={FIELDS.optional_upc_number}
                />

                <Submit
                  disabled={isDisabled}
                  albumSubmitRef={albumSubmitRef}
                  isSubmitting={isSubmitting}
                  submitSuccess={submitSuccess}
                />
              </fieldset>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
AlbumForm.defaultProps = {
  submitHandler: v1Submit,
}
AlbumForm.propTypes = {
  albumSubmitRef: PropTypes.object,
  album: PropTypes.object.isRequired,
  submitHandler: PropTypes.func.isRequired,
}

export default AlbumForm

async function v1Submit({
  album,
  setErrors,
  setSubmitSuccess,
  specializedReleaseType,
  values,
}) {
  try {
    const response = await albumRequest(album, specializedReleaseType, values)
    if (!response.ok) {
      const { errors } = await response.json()
      console.error(JSON.stringify(errors))
      setErrors(parseErrors(errors))
    } else {
      const json = await response.json()
      setSubmitSuccess(true)
      window.location = json.album_show_url
    }
  } catch (e) {
    console.error(e)
  }
}
