import React, { useState } from 'react'
import { Form, Formik } from 'formik'
import { useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'

import Submit from '../shared/Submit'
import ErrorScroller from '../shared/ErrorScroller'
import Spacer from '../../../shared/Spacer'
import {
  PrimaryCheckbox,
  PrimaryCheckboxWithTagline,
} from '../shared/MUICheckboxes'
import { PimaryAccordionList } from '../shared/MUIAccordionList'

import { devLogger } from '../../../utils'
import { IS_DEV } from '../../utils/constants'
import { storeSubmitHandler } from './storeFormHelpers'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import { useConfig } from '../../contexts/ConfigContext'
import PrimaryHeadline from '../shared/PrimaryHeadline'

import { lightGrayColor } from '../shared/theme'

import { skipOrContinueStoreForm } from '../shared/storeHelpers'

export default function StoreForm({ storeSubmitRef, submitHandler }) {
  const [config, setConfig] = useConfig()
  const [distributionState, distributionActions] = useDistributionFlow()

  const {
    activeStep,
    album,
    stores,
    freemiumStores,
    deliverAutomator,
    appleMusic,
  } = distributionState

  const history = useHistory()
  const { appleMusicTagline, itunesStoreId } = config

  const [state, setState] = useState({ manualErrorScrollTrigger: 0 })

  const [submitSuccess, setSubmitSuccess] = useState()

  const allStores = [...stores, ...freemiumStores]

  const allStoresInitiallyDeselected =
    allStores.filter((store) => store.selected === true).length == 0

  const allStoresObject = allStores.reduce(
    (object, store) => ({
      ...object,
      [store.id]: {
        selected: allStoresInitiallyDeselected ? true : store.selected,
      },
    }),
    {}
  )

  const appleMusicDefaultSelection = allStoresInitiallyDeselected
    ? true
    : appleMusic

  const checkIfallStoresSelected = ({
    storeOptions,
    countBeforeValuesUpdate,
  }) => {
    const selectedStores = Object.keys(storeOptions).filter(
      (store_id) => storeOptions[store_id].selected === true
    )

    if (countBeforeValuesUpdate) {
      return selectedStores.length === allStores.length - 1
    } else {
      return selectedStores.length === allStores.length
    }
  }

  const appleMusicOption = ({ handleChange, values }) => {
    const disabled = !values.stores[itunesStoreId].selected

    return (
      <div key={'apple_music'}>
        <PrimaryCheckboxWithTagline
          label={'Apple Music'}
          id={`apple_music`}
          type="checkbox"
          key={'Apple Music'}
          checked={values.apple_music}
          name={`apple_music`}
          onChange={handleChange}
          disabled={disabled}
          tagline={appleMusicTagline}
        />
      </div>
    )
  }

  const allStoreOptions = ({
    handleChange,
    onStoreChange,
    values,
    onItunesStoreChange,
  }) => {
    const standardStoreDescription = (
      <div style={styles.storeListDescription}>
        {
          'Stores & Streaming Platforms: You will keep 100% of your revenue when you distribute to these stores.'
        }
      </div>
    )

    const standardStoreOptions = stores.map((store) => {
      const onChange =
        store.id == itunesStoreId ? onItunesStoreChange : onStoreChange

      return (
        <div key={store.id}>
          <PrimaryCheckboxWithTagline
            label={store.name}
            id={`store-${store.id}`}
            type="checkbox"
            key={store.name}
            checked={values.stores[store.id].selected}
            name={`stores.${store.id}.selected`}
            onChange={onChange}
            tagline={store.tagline}
          />
        </div>
      )
    })

    const indexOfItunesStore = standardStoreOptions.findIndex(
      (item) => item.key == itunesStoreId
    )

    if (indexOfItunesStore >= 0) {
      const appleMusicOptionInstance = appleMusicOption({
        handleChange,
        values,
      })
      standardStoreOptions.splice(
        indexOfItunesStore + 1,
        0,
        appleMusicOptionInstance
      )
    }

    const freemiumStoreDescription = (
      <div style={styles.storeListDescription}>
        {
          'Free Stores: You will keep 80% of your revenue when you distribute to Facebook, Instagram & Reels.'
        }
      </div>
    )

    const freemiumStoreOptions = freemiumStores.map((freemiumStore) => (
      <div key={freemiumStore.id}>
        <PrimaryCheckboxWithTagline
          label={freemiumStore.name}
          id={`store-${freemiumStore.id}`}
          type="checkbox"
          key={freemiumStore.name}
          checked={values.stores[freemiumStore.id].selected}
          name={`stores.${freemiumStore.id}.selected`}
          onChange={onStoreChange}
        />
      </div>
    ))

    return [
      standardStoreDescription,
      ...standardStoreOptions,
      freemiumStoreDescription,
      ...freemiumStoreOptions,
    ]
  }

  // Upon submission, values from this form will be upstreamed into the Distribution Context (for later use)
  {
    return (
      <div className="grid-container div-album-form">
        <Formik
          initialValues={{
            stores: {
              ...allStoresObject,
            },
            album_id: album.id,
            deliver_to_all_stores: checkIfallStoresSelected({
              storeOptions: allStoresObject,
              countBeforeValuesUpdate: false,
            }),
            deliver_automator: deliverAutomator,
            apple_music: appleMusicDefaultSelection,
          }}
          onSubmit={async (values, { setErrors }) => {
            await submitHandler({
              album,
              distributionActions,
              history,
              setConfig,
              setErrors,
              setSubmitSuccess,
              values,
            })
          }}
          validateOnBlur={false}
          validateOnChange={false}
        >
          {(formikBag) => {
            const {
              errors,
              handleChange,
              isSubmitting,
              setFieldValue,
              values,
            } = formikBag

            IS_DEV && devLogger(values, 'Store Form Values')

            function wrappedHandleSubmit(e) {
              e.preventDefault()

              const allStoresSelected = checkIfallStoresSelected({
                storeOptions: formikBag.values.stores,
                countBeforeValuesUpdate: false,
              })

              if (allStoresInitiallyDeselected && allStoresSelected) {
                formikBag.dirty = true
              }

              skipOrContinueStoreForm({
                activeStep,
                config,
                formikBag,
                history,
                setState,
              })
            }

            function onDeliverToAllStoresChange(e) {
              const { checked } = e.target

              allStores.map((store) =>
                setFieldValue(`stores.${store.id}.selected`, checked)
              )

              setFieldValue('apple_music', checked)

              handleChange(e)
            }

            function onStoreChange(e) {
              const { checked } = e.target

              if (checked) {
                const allStoresSelected = checkIfallStoresSelected({
                  storeOptions: values.stores,
                  countBeforeValuesUpdate: true,
                })

                if (allStoresSelected) {
                  setFieldValue(`deliver_to_all_stores`, true)
                } else {
                  setFieldValue(`deliver_to_all_stores`, false)
                }
              } else {
                setFieldValue(`deliver_to_all_stores`, false)
              }

              handleChange(e)
            }

            function onItunesStoreChange(e) {
              const { checked } = e.target

              if (checked) {
                setFieldValue(`apple_music`, true)
              } else {
                setFieldValue(`apple_music`, false)
              }

              onStoreChange(e)
            }

            return (
              <Form data-testid="StoreForm" onSubmit={wrappedHandleSubmit}>
                <fieldset>
                  <ErrorScroller
                    errors={errors}
                    trigger={state.manualErrorScrollTrigger}
                  />
                  <PrimaryHeadline
                    Element="h1"
                    size="1.6em"
                    translation={'WHERE DO YOU WANT TO DISTRIBUTE?'}
                  />
                  <Spacer height={30} />

                  <div style={styles.storesLabel}>{'Stores*'}</div>
                  <PrimaryCheckbox
                    label={'Deliver to all Digital Stores'}
                    id={`deliver_to_all_stores`}
                    type="checkbox"
                    key={'deliver_to_all_stores'}
                    checked={values.deliver_to_all_stores}
                    name={`deliver_to_all_stores`}
                    onChange={onDeliverToAllStoresChange}
                  />

                  <PimaryAccordionList
                    description={'View Stores'}
                    listItems={[
                      ...allStoreOptions({
                        handleChange,
                        onStoreChange,
                        values,
                        onItunesStoreChange,
                      }),
                    ]}
                  />

                  <PrimaryCheckbox
                    label={'Store Automator'}
                    id={`deliver_automator`}
                    type="checkbox"
                    key={'deliver_automator'}
                    checked={values.deliver_automator}
                    name={`deliver_automator`}
                    onChange={handleChange}
                  />

                  <div style={styles.lightGreyText}>
                    {
                      'Check this option to automatically send this release to all new stores we add in the future. Pay a flat, one-time fee for all future stores and save money!'
                    }
                  </div>

                  <div style={styles.submitContainer}>
                    <Submit
                      customRef={storeSubmitRef}
                      isSubmitting={isSubmitting}
                      submitSuccess={submitSuccess}
                    />
                  </div>
                </fieldset>
              </Form>
            )
          }}
        </Formik>
      </div>
    )
  }
}
StoreForm.defaultProps = {
  submitHandler: storeSubmitHandler,
}
StoreForm.propTypes = {
  submitHandler: PropTypes.func.isRequired,
}

const styles = {
  submitContainer: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    margin: '2em',
  },
  storesLabel: {
    fontWeight: 'bold',
  },
  storeListDescription: {
    marginLeft: '-30px',
  },
  lightGreyText: {
    color: lightGrayColor,
  },
}
