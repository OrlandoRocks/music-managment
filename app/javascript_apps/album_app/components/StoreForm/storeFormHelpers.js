import { salepointRequest, parseErrors } from '../../utils/salepointRequest'
import { distributionProgressRequest } from '../../utils/distributionProgressRequest'
import {
  getPostStoreStepParam,
  releaseEditPath,
  updateStepParam,
} from '../DistributionStepper/routerHelpers'

async function storeSubmitHandler({
  album,
  distributionActions,
  history,
  setErrors,
  setSubmitSuccess,
  values,
}) {
  try {
    const {
      setStores,
      setFreemiumStores,
      setAppleMusic,
      setDeliverAutomator,
      setDistributionProgressLevel,
      setSongsComplete,
      setFetchingProgressLevels,
    } = distributionActions

    setFetchingProgressLevels(true)

    const response = await salepointRequest(album, values)
    const data = await response.json()

    setSubmitSuccess(true)
    setStores(data.stores)
    setFreemiumStores(data.freemium_stores)
    setAppleMusic(data.apple_music)
    setDeliverAutomator(data.deliver_automator)

    const distributionProgressResponse = await distributionProgressRequest({
      album,
    })
    const distributionProgressData = await distributionProgressResponse.json()

    if (distributionProgressResponse.ok) {
      const {
        distribution_progress_level,
        songs_complete,
      } = distributionProgressData

      setDistributionProgressLevel(distribution_progress_level)
      setSongsComplete(songs_complete)

      setFetchingProgressLevels(false)
    }

    const nextStepParam = getPostStoreStepParam()
    const nextPath = releaseEditPath(album.album_type, album.id)

    const newParams = updateStepParam(nextStepParam, history)

    history.replace({
      pathname: nextPath,
      search: newParams.toString(),
    })
  } catch (e) {
    console.error(e)
    const { errors } = e.response.data
    setErrors(parseErrors(errors))
  }
}

export { storeSubmitHandler }
