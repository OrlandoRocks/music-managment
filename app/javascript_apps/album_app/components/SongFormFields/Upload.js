import React from 'react'
import PropTypes from 'prop-types'

import CloudUploadIcon from '@material-ui/icons/CloudUpload'

import { PrimaryButton } from '../shared/MUIButtons'

export default function Upload({ handleUpload }) {
  return (
    <div>
      <span>Upload Track*</span>
      <PrimaryButton
        component="label"
        size="medium"
        startIcon={<CloudUploadIcon />}
      >
        Upload
        <input accept={FILE_TYPES} onChange={handleUpload} type={TYPE} hidden />
      </PrimaryButton>
    </div>
  )
}

const FILE_TYPES = '.flac,.mp3,.wav'
const TYPE = 'file'

Upload.propTypes = {
  handleUpload: PropTypes.func,
}
