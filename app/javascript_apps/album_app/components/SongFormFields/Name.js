import React from 'react'
import PropTypes from 'prop-types'

import TextField from '../shared/TextField'

const AlbumTitle = ({ defaultValue, handleBlur, handleChange, name }) => {
  return (
    <TextField
      id={name}
      defaultValue={defaultValue}
      label="Song Title*"
      name={name}
      onBlur={handleBlur}
      onChange={handleChange}
      variant="outlined"
    />
  )
}

AlbumTitle.propTypes = {
  defaultValue: PropTypes.string,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
  name: PropTypes.string,
}

export default React.memo(AlbumTitle)
