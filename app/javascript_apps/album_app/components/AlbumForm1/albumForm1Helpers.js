import { albumRequest, parseErrors } from '../../utils/albumRequest'
import { distributionProgressRequest } from '../../utils/distributionProgressRequest'
import {
  getNextStepParam,
  releaseEditPath,
  updateStepParam,
} from '../DistributionStepper/routerHelpers'
import { updatePossibleArtists } from '../shared/albumHelpers'
import { fetchSongsData } from '../../contexts/distributionContextHelpers'

async function submitHandler({
  album,
  distributionActions,
  distributionState,
  history,
  setConfig,
  setErrors,
  setSubmitSuccess,
  specializedReleaseType,
  values,
}) {
  try {
    const {
      initSongsData,
      setAlbum,
      setDistributionProgressLevel,
      setSongsComplete,
      setFetchingProgressLevels,
    } = distributionActions

    setFetchingProgressLevels(true)

    const response = await albumRequest(album, specializedReleaseType, values)
    const data = await response.json()

    setSubmitSuccess(true)
    setAlbum(data.album)

    updatePossibleArtists(data.album, setConfig)
    fetchSongsData(data.album, initSongsData)

    const distributionProgressResponse = await distributionProgressRequest({
      albumId: data.album.id,
    })
    const distributionProgressData = await distributionProgressResponse.json()

    if (distributionProgressResponse.ok) {
      const {
        distribution_progress_level,
        songs_complete,
      } = distributionProgressData

      setDistributionProgressLevel(distribution_progress_level)
      setSongsComplete(songs_complete)

      setFetchingProgressLevels(false)
    }

    const { stores } = distributionState
    const selectedStores = stores.filter((store) => store.selected === true)

    const nextStepParam = getNextStepParam({ stores: selectedStores })
    const nextPath = releaseEditPath(data.album.album_type, data.album.id)
    history.replace({
      pathname: nextPath,
      search: updateStepParam(nextStepParam, history).toString(),
    })
  } catch (e) {
    console.error(e)
    const { errors } = e.response.data
    setErrors(parseErrors(errors))
  }
}

export { submitHandler }
