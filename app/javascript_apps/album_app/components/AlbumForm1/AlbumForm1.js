import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Formik } from 'formik'
import PropTypes from 'prop-types'

import Header from '../../components/AlbumFormFields/Header'
import AlbumCleanVersion from '../../components/AlbumFormFields/AlbumCleanVersion'
import AlbumExplicit from '../../components/AlbumFormFields/AlbumExplicit'
import AlbumLanguage from '../../components/AlbumFormFields/AlbumLanguage'
import AlbumTitle from '../../components/AlbumFormFields/AlbumTitle'
import GenresContainer from '../../components/AlbumFormFields/Genres'
import IsVarious from '../../components/AlbumFormFields/IsVarious'
import MainArtistsChooser from '../../components/AlbumFormFields/MainArtistsChooser.js'
import OptionalIsrc from '../../components/AlbumFormFields/OptionalIsrc'
import OptionalUpc from '../../components/AlbumFormFields/OptionalUpc'
import TerritoryPickerContainer from '../../components/AlbumFormFields/TerritoryPicker/TerritoryPickerContainer'
import Submit from '../../components/AlbumFormFields/Submit'

import { submitHandler } from './albumForm1Helpers'

import ErrorMessage from '../shared/ErrorMessage'
import ErrorScroller from '../shared/ErrorScroller'
import ExternalServiceIDDataFetcher from '../data/ExternalServiceIDDataFetcher'
import { devLogger } from '../../../utils'
import { IS_DEV } from '../../utils/constants'

import { useConfig } from '../../contexts/ConfigContext'

// V2
import { useDistributionFlow } from '../../contexts/DistributionContext'
import {
  FIELDS,
  prepareInitialValues,
  skipOrContinue,
} from '../shared/albumHelpers'
import { createAlbumSchema, SCHEMAS } from '../shared/albumSchema'

const AlbumForm1 = ({ album, albumSubmitRef, submitHandler }) => {
  const [config, setConfig] = useConfig()
  const [distributionState, distributionActions] = useDistributionFlow()
  const history = useHistory()

  const {
    releaseTypeVars: { specializedReleaseType },
  } = config

  // eslint-disable-next-line no-unused-vars
  const { label_name, ...initialValues } = prepareInitialValues(album, config)

  const AlbumSchema = createAlbumSchema(config, SCHEMAS.V2_1)

  const [manualErrorScrollTrigger, setManualErrorScollTrigger] = useState(0)

  // Keep form disabled during redirect to show page
  const [submitSuccess, setSubmitSuccess] = useState(false)

  const isDisabled = album.finalized || submitSuccess

  return (
    <div className="grid-container div-album-form">
      <Header album={album} />

      <Formik
        initialValues={initialValues}
        onSubmit={async (values, { setErrors }) => {
          await submitHandler({
            album,
            config,
            distributionActions,
            distributionState,
            history,
            setConfig,
            setErrors,
            setSubmitSuccess,
            specializedReleaseType,
            values,
          })
        }}
        validationSchema={AlbumSchema}
      >
        {(formikBag) => {
          const {
            errors,
            handleBlur,
            handleChange,
            isSubmitting,
            setFieldValue,
            touched,
            values,
          } = formikBag

          IS_DEV && devLogger(values, 'Album Form1 Values')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            skipOrContinue({
              config,
              distributionActions,
              distributionState,
              formikBag,
              history,
              setManualErrorScollTrigger,
            })
          }

          return (
            <Form data-testid="AlbumForm" onSubmit={wrappedHandleSubmit}>
              <fieldset disabled={isDisabled}>
                <ErrorScroller
                  errors={errors}
                  trigger={manualErrorScrollTrigger}
                />
                <ErrorMessage
                  error={errors[FIELDS.name]}
                  touched={touched[FIELDS.name]}
                />
                <ExternalServiceIDDataFetcher creatives={values.creatives} />
                <AlbumTitle
                  albumType={album.album_type}
                  defaultValue={album.name}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  name={FIELDS.name}
                />
                <ErrorMessage
                  error={errors[FIELDS.creatives]}
                  touched={touched[FIELDS.creatives]}
                />
                <MainArtistsChooser disabled={isDisabled} />
                <ErrorMessage
                  error={errors[FIELDS.parental_advisory]}
                  touched={touched[FIELDS.parental_advisory]}
                />
                <AlbumExplicit
                  currentValue={values.parental_advisory}
                  handleChange={handleChange}
                  name={FIELDS.parental_advisory}
                />
                <ErrorMessage
                  error={errors[FIELDS.clean_version]}
                  touched={touched[FIELDS.clean_version]}
                />
                <AlbumCleanVersion
                  currentValue={values.clean_version}
                  isCleanVersion={album.clean_version}
                  handleChange={handleChange}
                  name={FIELDS.clean_version}
                  parentalAdvisory={values.parental_advisory}
                />
                <ErrorMessage
                  error={errors[FIELDS.is_various]}
                  touched={touched[FIELDS.is_various]}
                />
                <IsVarious
                  currentValue={values.is_various}
                  isVarious={album.is_various}
                  name={FIELDS.is_various}
                  setFieldValue={setFieldValue}
                />
                <ErrorMessage
                  error={errors[FIELDS.language_code]}
                  touched={touched[FIELDS.language_code]}
                />
                <AlbumLanguage
                  defaultValue={album.language_code}
                  handleChange={handleChange}
                  name={FIELDS.language_code}
                />
                <GenresContainer
                  errors={errors}
                  handleChange={handleChange}
                  primary_genre_id={values.primary_genre_id}
                  secondary_genre_id={values.secondary_genre_id}
                  sub_genre_id_primary={values.sub_genre_id_primary}
                  sub_genre_id_secondary={values.sub_genre_id_secondary}
                  touched={touched}
                />
                <TerritoryPickerContainer />
                <ErrorMessage
                  error={errors[FIELDS.optional_upc_number]}
                  touched={touched[FIELDS.optional_upc_number]}
                />
                <OptionalUpc
                  defaultValue={album.optional_upc_number}
                  handleChange={handleChange}
                  immutable={!album.is_new && album.finalized}
                  name={FIELDS.optional_upc_number}
                />
                <OptionalIsrc
                  defaultValue={album.optional_isrc}
                  handleChange={handleChange}
                  name={FIELDS.optional_isrc}
                />
                <Submit
                  disabled={isDisabled}
                  albumSubmitRef={albumSubmitRef}
                  isSubmitting={isSubmitting}
                  submitSuccess={submitSuccess}
                />
              </fieldset>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
AlbumForm1.defaultProps = {
  submitHandler,
}
AlbumForm1.propTypes = {
  album: PropTypes.object.isRequired,
  albumSubmitRef: PropTypes.object,
  submitHandler: PropTypes.func.isRequired,
}

export default AlbumForm1
