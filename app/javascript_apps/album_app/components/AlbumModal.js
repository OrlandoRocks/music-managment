import React, { useCallback } from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'

const ALBUM_MODAL_DOM_ELEMENT = 'body'
const ALBUM_MODAL_CONTENT_CLASS = 'album-modal'
const ALBUM_MODAL_OVERLAY_CLASS = 'album-modal-overlay'
Modal.setAppElement(ALBUM_MODAL_DOM_ELEMENT)

/**
 *
 * show/setShow accepts a hook state/setter
 * customStyles keys: 'content', 'overlay'
 */
export function AlbumModal({
  BodyComponent,
  contentLabel,
  customStyles = {},
  headerMessage,
  headerStyle,
  onRequestClose,
  setShow,
  show,
  ...bodyComponentProps
}) {
  const toggleModal = useCallback(
    () =>
      setShow((show) => {
        !show && document.body.scrollIntoView(true)
        setShow(!show)
      }),
    [setShow]
  )

  const modalActionCallback = useCallback(
    (action) => {
      if (!toggleModal) return

      action && action()
      toggleModal()
    },
    [toggleModal]
  )

  function handleOnRequestClose() {
    if (onRequestClose) onRequestClose()

    toggleModal()
  }

  return (
    <Modal
      className={ALBUM_MODAL_CONTENT_CLASS}
      closeTimeoutMS={200}
      contentLabel={contentLabel}
      isOpen={show}
      onRequestClose={handleOnRequestClose}
      overlayClassName={ALBUM_MODAL_OVERLAY_CLASS}
      style={customStyles}
    >
      <div data-testid="AlbumModal">
        <h3 style={headerStyle}>{headerMessage}</h3>
        <BodyComponent
          {...bodyComponentProps}
          modalActionCallback={modalActionCallback}
          toggleModal={toggleModal}
        />
      </div>
    </Modal>
  )
}

AlbumModal.propTypes = {
  BodyComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object])
    .isRequired,
  contentLabel: PropTypes.string,
  customStyles: PropTypes.object,
  headerMessage: PropTypes.string,
  negativeAction: PropTypes.func,
  positiveAction: PropTypes.func,
  setShow: PropTypes.func.isRequired,
  show: PropTypes.bool,
  translations: PropTypes.object,
  headerStyle: PropTypes.object,
  onRequestClose: PropTypes.func,
}
