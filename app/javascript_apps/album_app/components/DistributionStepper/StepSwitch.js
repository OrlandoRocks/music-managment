import React from 'react'
import PropTypes from 'prop-types'
import { useLocation } from 'react-router-dom'

import AlbumContainer from '../AlbumContainer'
import AlbumForm1 from '../AlbumForm1/AlbumForm1'
import AlbumForm2 from '../AlbumForm2/AlbumForm2'
import SongForm1 from '../SongForm1/SongForm1'
import SongForm2 from '../SongForm2/SongForm2'
import SongForm3 from '../SongForm3/SongForm3'
import SummaryContainer from '../DistributionSummary/SummaryContainer'
import ArtworkForm from '../ArtworkForm/ArtworkForm'
import StoreForm from '../StoreForm/StoreForm'

import { STEPS } from './distributionStepperHelpers'
import { useStepParamSync } from './routerHelpers'
import { useConfig } from '../../contexts/ConfigContext'
import { ALBUM_APP_V2 } from '../../utils/constants'
import { submitHandler } from '../AlbumForm2/albumForm2Helpers'
import { storeSubmitHandler } from '../StoreForm/storeFormHelpers'

const {
  ALBUM_FORM_1,
  ALBUM_FORM_2,
  STORE_FORM,
  ARTWORK_FORM,
  SONG_FORM_1,
  SONG_FORM_2,
  SONG_FORM_3,
  SUMMARY,
} = STEPS

export default function StepSwitch(props) {
  const { features } = useConfig()[0]
  const { activeStep, album, setActiveStep, songs } = props
  const location = useLocation()

  // Sync the active step to the URL query params
  useStepParamSync({ activeStep, album, location, setActiveStep, songs })

  switch (activeStep) {
    case ALBUM_FORM_1: {
      const { album, albumSubmitRef, mockAlbumSubmitHandler } = props
      const Component = features[ALBUM_APP_V2] ? AlbumForm1 : AlbumContainer

      return (
        <Component
          album={album}
          albumSubmitRef={albumSubmitRef}
          submitHandler={mockAlbumSubmitHandler}
        />
      )
    }

    case SONG_FORM_1: {
      const { mockSongSubmitHandler, songSubmitRef } = props
      return (
        <SongForm1
          songSubmitRef={songSubmitRef}
          submitHandler={mockSongSubmitHandler}
        />
      )
    }

    case SUMMARY: {
      return <SummaryContainer />
    }

    case SONG_FORM_2: {
      const { mockSongSubmitHandler, songSubmitRef } = props

      return (
        <SongForm2
          songSubmitRef={songSubmitRef}
          submitHandler={mockSongSubmitHandler}
        />
      )
    }

    case SONG_FORM_3: {
      const { mockSongSubmitHandler, songSubmitRef } = props

      return (
        <SongForm3
          songSubmitRef={songSubmitRef}
          submitHandler={mockSongSubmitHandler}
        />
      )
    }

    case ALBUM_FORM_2: {
      const { album, albumSubmitRef, mockAlbumSubmitHandler } = props
      return (
        <AlbumForm2
          album={album}
          albumSubmitRef={albumSubmitRef}
          submitHandler={mockAlbumSubmitHandler || submitHandler}
        />
      )
    }

    case ARTWORK_FORM: {
      return <ArtworkForm />
    }

    case STORE_FORM: {
      const { mockStoreSubmitHandler, storeSubmitRef } = props

      return (
        <StoreForm
          storeSubmitRef={storeSubmitRef}
          submitHandler={mockStoreSubmitHandler || storeSubmitHandler}
        />
      )
    }

    default:
      console.error('Missing step!')
      return null
  }
}

StepSwitch.propTypes = {
  songSubmitRef: PropTypes.object,
  albumSubmitRef: PropTypes.object,
  storeSubmitRef: PropTypes.object,
  songs: PropTypes.object,
  album: PropTypes.object,
  activeStep: PropTypes.number,
  mockSongSubmitHandler: PropTypes.func,
  mockStoreSubmitHandler: PropTypes.func,
  mockAlbumSubmitHandler: PropTypes.func,
  setActiveStep: PropTypes.func,
}
