import { useLayoutEffect } from 'react'

import {
  PARAM_STEP_MAP,
  STEP_PARAMS,
  STEPS,
} from './distributionStepperHelpers'

// The 'step' query param is the source of truth for navigation.
// To change steps, push a new step param onto history.
// This effect ensures that activeStep is synced to the step param.
function useStepParamSync({
  activeStep,
  album,
  location,
  setActiveStep,
  songs,
}) {
  const params = location.search

  useLayoutEffect(() => {
    const stepParam = getStepParam(params)
    const correctStep = getCorrectStep(album, songs, stepParam)
    if (activeStep !== correctStep) {
      setActiveStep(correctStep)
    }
  }, [activeStep, album, params, setActiveStep, songs])
}

const STEP_KEY = 'step'
function updateStepParam(stepParam, history) {
  const params = new URLSearchParams(history.location.search)
  if (stepParam) {
    params.set(STEP_KEY, stepParam)
  } else {
    params.delete(STEP_KEY)
  }
  return params
}

const TRACK_KEY = 'track'
function pushSongParams(track, history) {
  const newParams = updateStepParam(STEP_PARAMS.s1, history)
  newParams.set(TRACK_KEY, track)
  history.push({ search: newParams.toString() })
}

function setParams(newParams, history) {
  const params = new URLSearchParams(history.location.search)
  Object.entries(newParams).forEach(([k, v]) => {
    params.set(k, v)
  })
  history.push({ search: params.toString() })
}

function setStepParam(param, history) {
  const newParams = updateStepParam(param, history)
  history.push({ search: newParams.toString() })
}

function getCorrectStep(album, songs, stepParam) {
  let result = PARAM_STEP_MAP[stepParam]
  if (!result) {
    result = album.id ? STEPS.SUMMARY : STEPS.ALBUM_FORM_1
  }
  return result
}

function getNextStepParam({ stores }) {
  // If we're going through the form for the first time, then we want to jump to the store form
  if (stores.length == 0) {
    return STEP_PARAMS.st
  } else {
    return null
  }
}

function getPostStoreStepParam() {
  return STEP_PARAMS.a2
}

const NEW_PATH = '/new'
function getInitialStep() {
  const isNew = window.location.pathname.includes(NEW_PATH)
  return isNew ? STEPS.ALBUM_FORM_1 : STEPS.SUMMARY
}

function getParam(location, key) {
  const search = new URLSearchParams(location.search)
  return search.get(key)
}

function createParams(params) {
  return new URLSearchParams(params).toString()
}

const releaseEditPath = (type, id) => `/${type.toLowerCase()}s/${id}/edit`

/**
 * utils
 */
const STEP_PARAM = 'step'
function getStepParam(params) {
  const search = new URLSearchParams(params)
  return search.get(STEP_PARAM)
}

export {
  createParams,
  getInitialStep,
  getNextStepParam,
  getPostStoreStepParam,
  getParam,
  releaseEditPath,
  setParams,
  setStepParam,
  pushSongParams,
  updateStepParam,
  useStepParamSync,
}
