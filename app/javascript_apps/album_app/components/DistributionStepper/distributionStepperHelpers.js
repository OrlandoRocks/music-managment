import { ALBUM_APP_V2 } from '../../utils/constants'
import { isEmpty } from '../../../utils'
import { createParams, setStepParam, updateStepParam } from './routerHelpers'

const STEPS = {
  ALBUM_FORM_1: 'ALBUM_FORM_1',
  ALBUM_FORM_2: 'ALBUM_FORM_2',
  ARTWORK_FORM: 'ARTWORK_FORM',
  SONG_FORM_1: 'SONG_FORM_1',
  SONG_FORM_2: 'SONG_FORM_2',
  SONG_FORM_3: 'SONG_FORM_3',
  STORE_FORM: 'STORE_FORM',
  SUMMARY: 'SUMMARY',
}

const {
  ALBUM_FORM_1,
  ALBUM_FORM_2,
  ARTWORK_FORM,
  SONG_FORM_1,
  SONG_FORM_2,
  SONG_FORM_3,
  STORE_FORM,
} = STEPS

// The summary step is the 'root'
const PARAM_STEP_MAP = {
  a1: ALBUM_FORM_1,
  a2: ALBUM_FORM_2,
  art: ARTWORK_FORM,
  s1: SONG_FORM_1,
  s2: SONG_FORM_2,
  s3: SONG_FORM_3,
  st: STORE_FORM,
}

const STEP_PARAMS = {
  a1: 'a1',
  a2: 'a2',
  art: 'art',
  s1: 's1',
  s2: 's2',
  s3: 's3',
  st: 'st',
}

const isSongForm1WithFile = (file) => Boolean(file)
const isV1Flow = (config) => !config.features[ALBUM_APP_V2]
const isCleanWithNoErrors = (dirty, errors) => !dirty && isEmpty(errors)

function canSkipSubmit({ config, dirty, errors, file }) {
  if (isSongForm1WithFile(file) || isV1Flow(config)) {
    return false
  }

  return isCleanWithNoErrors(dirty, errors)
}

function handleAlbumForm1Submission({
  stores,
  handleReleaseSubmission,
  history,
}) {
  const selectedStores = stores.filter((store) => store.selected === true)

  if (selectedStores.length === 0) {
    handleReleaseSubmission(STEP_PARAMS.st)
  } else {
    const newParams = updateStepParam(null, history).toString()

    history.push({
      search: newParams,
    })
  }
}

async function skipSubmit({
  activeStep,
  handleReleaseSubmission,
  history,
  stores,
}) {
  switch (activeStep) {
    case ALBUM_FORM_1:
      handleAlbumForm1Submission({
        stores,
        handleReleaseSubmission,
        history,
      })
      break
    case STORE_FORM:
      history.push({
        search: createParams({ step: STEP_PARAMS.a2 }),
      })
      break
    case SONG_FORM_1:
      setStepParam(STEP_PARAMS.s2, history)
      break
    case SONG_FORM_2:
      setStepParam(STEP_PARAMS.s3, history)
      break
    case SONG_FORM_3: {
      history.push({
        search: createParams({ step: STEP_PARAMS.a2 }),
      })
      break
    }
    case ALBUM_FORM_2:
      handleReleaseSubmission(STEP_PARAMS.art)
      break
    default:
      break
  }
}

export { PARAM_STEP_MAP, STEP_PARAMS, STEPS, canSkipSubmit, skipSubmit }
