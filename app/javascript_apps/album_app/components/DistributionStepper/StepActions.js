import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import { PrimaryButton, PrimaryTextButton } from '../shared/MUIButtons'
import { STEPS } from './distributionStepperHelpers'

export default function StepActions({
  activeStep,
  continueDisabled,
  handleBack,
  handleNext,
}) {
  const styles = actionsStyles()

  return (
    <div className={styles.actionsContainer}>
      <BackButton activeStep={activeStep} handleBack={handleBack} />
      <PrimaryButton disabled={continueDisabled} onClick={handleNext}>
        Continue
      </PrimaryButton>
    </div>
  )
}

StepActions.propTypes = {
  activeStep: PropTypes.number,
  continueDisabled: PropTypes.bool,
  handleBack: PropTypes.func,
  handleNext: PropTypes.func,
}

function WrappedPrimaryTextButton({ children, ...props }) {
  if (props.hidden) return <div />
  return <PrimaryTextButton {...props}>{children}</PrimaryTextButton>
}

WrappedPrimaryTextButton.propTypes = {
  children: PropTypes.node,
  hidden: PropTypes.bool,
}

function BackButton({ activeStep, handleBack }) {
  return (
    <WrappedPrimaryTextButton
      disabled={activeStep === STEPS.ALBUM_FORM_1}
      hidden={activeStep === STEPS.ALBUM_FORM_1}
      onClick={handleBack}
    >
      Back
    </WrappedPrimaryTextButton>
  )
}

BackButton.propTypes = {
  activeStep: PropTypes.number,
  handleBack: PropTypes.func,
}

const actionsStyles = makeStyles(() => ({
  // Sticky element scroll jank on retina macOS Chrome browser???
  actionsContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    background: '#fff',
    borderBottom: '1px solid #cacaca',
    margin: '0 calc((1024px - 100vw) / 2 - 20px) 1em', // desktop max main-container width - viewport - main-container padding
    padding: '.5em 1.2em',
    position: 'sticky',
    top: '51px',
    zIndex: 1,
    '@media screen and (max-width: 1023px)': {
      margin: '0 -20px 1em',
    },
    '& > .MuiButton-textPrimary': {
      minWidth: 0,
      padding: 0,
      '& > .MuiButton-label': {
        width: 'auto',
      },
    },
  },
}))
