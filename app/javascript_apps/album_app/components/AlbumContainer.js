import React from 'react'
import PropTypes from 'prop-types'

import { Header } from '../components/AlbumFormFields/Header'
import AlbumForm from './AlbumForm'
import buildAlbum from '../utils/buildAlbum'
import { devLogger } from '../../utils'
import { IS_DEV } from '../utils/constants'

function AlbumContainer({ album, albumSubmitRef, skipSubmit, submitHandler }) {
  IS_DEV && devLogger(album, 'Album Form: Initial Album')

  return (
    <>
      <Header album={album} />
      <AlbumForm
        album={album}
        albumSubmitRef={albumSubmitRef}
        skipSubmit={skipSubmit}
        submitHandler={submitHandler}
      />
    </>
  )
}

export default AlbumContainer

AlbumContainer.defaultProps = {
  album: buildAlbum(),
}

AlbumContainer.propTypes = {
  album: PropTypes.object.isRequired,
  albumSubmitRef: PropTypes.object,
  skipSubmit: PropTypes.func,
  submitHandler: PropTypes.func,
}
