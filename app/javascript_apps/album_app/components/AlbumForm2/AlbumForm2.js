import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Formik } from 'formik'
import PropTypes from 'prop-types'

import Header from '../../components/AlbumFormFields/Header'
import AlbumLabel from '../../components/AlbumFormFields/AlbumLabel'
import AlbumRecordingLocation from '../../components/AlbumFormFields/AlbumRecordingLocation'
import GoLiveDateFields from '../../components/AlbumFormFields/GoLiveDateFields'
import PreviouslyReleased from '../../components/AlbumFormFields/PreviouslyReleased'
import Submit from '../../components/AlbumFormFields/Submit'

import { submitHandler } from './albumForm2Helpers'

import ErrorMessage from '../shared/ErrorMessage'
import ErrorScroller from '../shared/ErrorScroller'
import { devLogger } from '../../../utils'
import { IS_DEV } from '../../utils/constants'

import { useConfig } from '../../contexts/ConfigContext'
import { useDistributionFlow } from '../../contexts/DistributionContext'
import { FIELDS, skipOrContinue } from '../shared/albumHelpers'
import { createAlbumSchema, SCHEMAS } from '../shared/albumSchema'

const AlbumForm2 = ({ album, albumSubmitRef, submitHandler }) => {
  const [config, setConfig] = useConfig()
  const [distributionState, distributionActions] = useDistributionFlow()
  const history = useHistory()

  const {
    releaseTypeVars: { timedReleaseDisabled, specializedReleaseType },
  } = config

  const initialValues = album

  const AlbumSchema = createAlbumSchema(config, SCHEMAS.V2_2)

  const [manualErrorScrollTrigger, setManualErrorScollTrigger] = useState(0)

  const [submitSuccess, setSubmitSuccess] = useState(false)

  const isDisabled = album.finalized || submitSuccess

  return (
    <div className="grid-container div-album-form">
      <Header album={album} />

      <Formik
        initialValues={initialValues}
        onSubmit={async (values, { setErrors }) => {
          await submitHandler({
            album,
            config,
            distributionActions,
            distributionState,
            history,
            setConfig,
            setErrors,
            setSubmitSuccess,
            specializedReleaseType,
            values,
          })
        }}
        validationSchema={AlbumSchema}
      >
        {(formikBag) => {
          const {
            errors,
            handleChange,
            isSubmitting,
            setFieldValue,
            setTouched,
            setValues,
            touched,
            values,
          } = formikBag

          IS_DEV && devLogger(values, 'Album Form2 Values')

          function wrappedHandleSubmit(e) {
            e.preventDefault()

            skipOrContinue({
              config,
              distributionActions,
              distributionState,
              formikBag,
              history,
              setManualErrorScollTrigger,
            })
          }

          return (
            <Form data-testid="AlbumForm" onSubmit={wrappedHandleSubmit}>
              <fieldset disabled={isDisabled}>
                <ErrorScroller
                  errors={errors}
                  trigger={manualErrorScrollTrigger}
                />
                <ErrorMessage
                  error={errors[FIELDS.golive_date] || errors[FIELDS.sale_date]}
                  touched={touched[FIELDS.sale_date]}
                />
                <GoLiveDateFields
                  timedReleaseDisabled={timedReleaseDisabled}
                  values={values}
                  setValues={setValues}
                  handleChange={handleChange}
                  timingScenarioFieldName={FIELDS.timed_release_timing_scenario}
                  album={album}
                  setTouched={setTouched}
                />
                <PreviouslyReleased
                  handleChange={handleChange}
                  isPreviouslyReleased={values.is_previously_released}
                  origReleaseYear={values.orig_release_year}
                  setFieldValue={setFieldValue}
                  setTouched={setTouched}
                />
                <ErrorMessage
                  error={errors[FIELDS.label_name]}
                  touched={touched[FIELDS.label_name]}
                />
                <AlbumLabel
                  defaultValue={album.label_name}
                  handleChange={handleChange}
                  name={FIELDS.label_name}
                />
                <AlbumRecordingLocation
                  defaultValue={album.recording_location}
                  handleChange={handleChange}
                  name={FIELDS.recording_location}
                />
                <Submit
                  disabled={isDisabled}
                  albumSubmitRef={albumSubmitRef}
                  isSubmitting={isSubmitting}
                  submitSuccess={submitSuccess}
                />
              </fieldset>
            </Form>
          )
        }}
      </Formik>
    </div>
  )
}
AlbumForm2.defaultProps = {
  submitHandler,
}
AlbumForm2.propTypes = {
  album: PropTypes.object.isRequired,
  albumSubmitRef: PropTypes.object,
  submitHandler: PropTypes.func.isRequired,
}

export default AlbumForm2
