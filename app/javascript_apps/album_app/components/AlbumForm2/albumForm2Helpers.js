import { albumRequest, parseErrors } from '../../utils/albumRequest'
import { distributionProgressRequest } from '../../utils/distributionProgressRequest'
import { updateStepParam } from '../DistributionStepper/routerHelpers'
import { STEP_PARAMS } from '../DistributionStepper/distributionStepperHelpers'

async function submitHandler({
  album,
  distributionActions,
  history,
  setErrors,
  setSubmitSuccess,
  specializedReleaseType,
  values,
}) {
  console.log(values)
  try {
    const {
      setAlbum,
      setDistributionProgressLevel,
      setSongsComplete,
      setFetchingProgressLevels,
    } = distributionActions

    setFetchingProgressLevels(true)

    const response = await albumRequest(album, specializedReleaseType, values)
    const data = await response.json()

    setSubmitSuccess(true)
    setAlbum(data.album)

    const distributionProgressResponse = await distributionProgressRequest({
      album,
    })
    const distributionProgressData = await distributionProgressResponse.json()

    if (distributionProgressResponse.ok) {
      const {
        distribution_progress_level,
        songs_complete,
      } = distributionProgressData

      setDistributionProgressLevel(distribution_progress_level)
      setSongsComplete(songs_complete)

      setFetchingProgressLevels(false)
    }

    history.replace({
      search: updateStepParam(STEP_PARAMS.art, history).toString(),
    })
  } catch (e) {
    console.error(e)
    const { errors } = e.response.data
    setErrors(parseErrors(errors))
  }
}

export { submitHandler }
