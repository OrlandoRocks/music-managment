import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const AlbumLabel = ({ defaultValue, handleChange, name }) => {
  const { translations } = useConfig()[0]

  return (
    <div className="grid-x div-album-form-field">
      <label
        className="cell medium-2 optional"
        id="input-album-label"
        htmlFor={name}
      >
        {translations.album_label}
        <br />
        <em>{translations.optional}</em>
      </label>
      <input
        id={name}
        type="text"
        name={name}
        className="cell medium-6 input-album-label"
        defaultValue={defaultValue}
        onChange={handleChange}
      />
      <span className="cell medium-4"></span>
    </div>
  )
}

AlbumLabel.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string,
  handleChange: PropTypes.func,
}

export default React.memo(AlbumLabel)
