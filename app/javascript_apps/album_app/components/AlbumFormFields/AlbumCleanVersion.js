import React from 'react'
import PropTypes from 'prop-types'

import RadioToolbar from '../../../shared/RadioToolbar'
import { useConfig } from '../../contexts/ConfigContext'

function hideComponent(explicitFieldsEnabled, parentalAdvisory) {
  return (
    !explicitFieldsEnabled ||
    (explicitFieldsEnabled &&
      (parentalAdvisory === null || parentalAdvisory === true))
  )
}

const AlbumCleanVersion = ({
  currentValue,
  handleChange,
  name,
  parentalAdvisory,
}) => {
  const {
    releaseTypeVars: { explicitFieldsEnabled },
    translations,
  } = useConfig()[0]

  if (hideComponent(explicitFieldsEnabled, parentalAdvisory)) return null

  return (
    <div className="grid-x div-album-form-field">
      <span className="cell medium-2"></span>
      <div className="cell medium-10">
        <div className="row">{translations.clean_version_warning}</div>
        <RadioToolbar
          handleChange={handleChange}
          klassName="song-clean-version"
          name={name}
          noText={translations.button_no}
          prompt={translations.clean_version_prompt}
          value={currentValue}
          yesText={translations.button_yes}
        />
      </div>
    </div>
  )
}

AlbumCleanVersion.propTypes = {
  currentValue: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  parentalAdvisory: PropTypes.bool,
  name: PropTypes.string,
}

export default React.memo(AlbumCleanVersion)
