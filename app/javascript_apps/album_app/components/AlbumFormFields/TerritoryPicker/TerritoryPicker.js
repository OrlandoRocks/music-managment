import React, { useCallback, useMemo, useState } from 'react'
import produce from 'immer'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import Autocomplete from '@material-ui/lab/Autocomplete'
import { makeStyles } from '@material-ui/core/styles'

import ArrowIcon from '../../../../shared/svg/arrowIcon'
import { useConfig } from '../../../contexts/ConfigContext'
import Button from '../../shared/Button'
import { ModalHeader } from '../../shared/ModalComponents'
import PickerSelectionSummary from './PickerSelectionSummary'
import {
  COMPONENT_TYPES,
  PICKER_TYPES,
  SELECTED_COUNTRIES_FIELD,
} from './constants'
import { CountryListItem } from './CountryList'
import { determineSelectedCountries, selectedAll } from './helpers'
import TextField from '../../shared/TextField'

const { OVERVIEW } = COMPONENT_TYPES
const { EXCLUSION, INCLUSION } = PICKER_TYPES
const EMPTY_SELECTION = []

export default function TerritoryPicker({
  pickerType,
  setCurrentComponent,
  toggleModal,
}) {
  const {
    setFieldValue,
    values: { selected_countries },
  } = useFormikContext()
  const {
    countries,
    translations: { territory_picker: translations },
  } = useConfig()[0]

  const initialSelectedCountries = useMemo(() => {
    if (
      !selected_countries.length ||
      selectedAll(countries, selected_countries)
    )
      return []

    return countries.filter(([_, abbrev]) => {
      switch (pickerType) {
        case EXCLUSION:
          return !selected_countries.includes(abbrev)
        case INCLUSION:
          return selected_countries.includes(abbrev)
        default:
          return []
      }
    })
  }, [countries, pickerType, selected_countries])

  const [selected, setSelected] = useState(initialSelectedCountries)

  function saveSelection() {
    const newSelectedCountries = determineSelectedCountries(
      countries,
      pickerType,
      selected
    )
    setFieldValue(SELECTED_COUNTRIES_FIELD, newSelectedCountries)
  }

  function handleFinish() {
    saveSelection()
    toggleModal()
  }

  function handleBack() {
    saveSelection()
    setCurrentComponent(OVERVIEW)
  }

  const removeSelected = useCallback(
    (country) => {
      const newSelected = produce(selected, (draft) => {
        return draft.filter((c) => c[1] !== country[1])
      })
      setSelected(newSelected)
    },
    [selected, setSelected]
  )

  const reset = useCallback(() => setSelected(EMPTY_SELECTION), [setSelected])

  const hasSelectedItems = Boolean(selected.length)
  const hasSelectedAll = selectedAll(countries, selected)

  const allOrNone = hasSelectedAll || !hasSelectedItems

  const classes = useStyles()

  return (
    <>
      <ModalHeader>{translations.search_headers[pickerType]}</ModalHeader>
      <p>{translations.search_instruction}</p>
      <Autocomplete
        id={`territory-picker-${pickerType}`}
        // territory-picker-inclusion
        // territory-picker-exclusion
        classes={{ option: classes.option }}
        disableClearable
        disableCloseOnSelect
        fullWidth
        getOptionLabel={(option) => option[0]}
        getOptionSelected={([_o, option], [_v, value]) => option === value}
        multiple
        onChange={(_, v) => setSelected(v)}
        options={countries}
        renderInput={(params) => (
          <TextField
            {...params}
            fullWidth
            label="Countries"
            placeholder={translations.search_placeholder}
            variant="outlined"
          />
        )}
        renderOption={(option, state) => (
          <CountryListItem
            country={option}
            isSelected={state.selected}
            key={`${option[0]}`}
            pickerType={pickerType}
          />
        )}
        renderTags={() => null}
        value={selected}
      />
      <div className={classes.finishSelectionButtonContainer}>
        <Button
          additionalStyleKey={allOrNone ? 'disabled' : ''}
          disabled={allOrNone}
          onClick={handleFinish}
        >
          {translations.finish_adding}
        </Button>
      </div>
      <PickerSelectionSummary
        allOrNone={allOrNone}
        pickerType={pickerType}
        removeSelectedItem={removeSelected}
        reset={reset}
        selectedItems={selected}
      />
      <Button
        additionalStyleKey="back"
        aria-label={translations.back_to_overview}
        onClick={handleBack}
      >
        <ArrowIcon isBack />
      </Button>
    </>
  )
}

TerritoryPicker.propTypes = {
  pickerType: PropTypes.string.isRequired,
  setCurrentComponent: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

const useStyles = makeStyles(() => ({
  finishSelectionButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '1em',
  },
  option: {
    width: '100%',
    '&[aria-selected="true"]': {
      // Overriding selected style because selection is indicated by buttons and text
      background: 'transparent',
      '&:hover, &:focus, &[aria-selected="true"][data-focus="true"]': {
        background: 'rgba(0, 0, 0, 0.08)',
      },
    },
  },
}))
