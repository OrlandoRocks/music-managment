import React from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import Button from '../../shared/Button'
import { PICKER_TYPES as SELECTION_TYPES } from './constants'
import { selectedAll } from './helpers'
import { useConfig } from '../../../contexts/ConfigContext'

const { WORLDWIDE } = SELECTION_TYPES

const styles = {
  container: {
    marginLeft: '1.5em',
    paddingBottom: '1em',
  },
  small: {
    display: 'block',
    marginBottom: '.5em',
  },
}

const SelectionSummary = React.memo(function SelectionSummary({
  handleChange,
  hide,
  pickerType,
  selectedCountryNames,
}) {
  const {
    countries,
    translations: { territory_picker: translations },
  } = useConfig()[0]
  const {
    values: { album_type, selected_countries },
  } = useFormikContext()

  if (
    hide ||
    pickerType === WORLDWIDE ||
    selected_countries.length === 0 ||
    selectedAll(countries, selected_countries)
  )
    return null

  const message = `${translations.form_level_selection_descriptions[pickerType]}:`.replace(
    '{{album_type}}',
    album_type.toLowerCase()
  )

  return (
    <div style={styles.container}>
      <small style={styles.small}>
        {message}
        <br />
        {selectedCountryNames}
        <br />
      </small>
      <Button name={pickerType} onClick={handleChange}>
        {translations.edit_list}
      </Button>
    </div>
  )
})
SelectionSummary.displayName = 'SelectionSummary'
SelectionSummary.propTypes = {
  handleChange: PropTypes.func.isRequired,
  hide: PropTypes.bool,
  pickerType: PropTypes.string.isRequired,
  selectedCountryNames: PropTypes.string,
}

export default SelectionSummary
