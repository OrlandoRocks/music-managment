import React, { useCallback } from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import Button from '../../shared/Button'
import { useConfig } from '../../../contexts/ConfigContext'
import { useCountryAbbrevs } from './helpers'
import { SELECTED_COUNTRIES_FIELD } from './constants'

const styles = {
  container: {
    marginTop: '.5em',
  },
  buttonWrapper: {
    display: 'inline-block',
    margin: '0 .25em .25em',
  },
}

const RemovalButton = React.memo(function RemovalButton({
  country,
  pickerType,
  removeSelectedItem,
}) {
  const [name, _] = country

  function syncRemoval() {
    removeSelectedItem(country)
  }

  return (
    <div style={styles.buttonWrapper}>
      <Button
        additionalStyleKey={pickerType}
        name={pickerType}
        onClick={syncRemoval}
      >
        {name}{' '}
        <span>
          <b>x</b>
        </span>
      </Button>
    </div>
  )
})
RemovalButton.displayName = 'RemovalButton'
RemovalButton.propTypes = {
  country: PropTypes.array.isRequired,
  pickerType: PropTypes.string.isRequired,
  removeSelectedItem: PropTypes.func.isRequired,
}

const PickerSelectionSummary = React.memo(function PickerSelectionSummary({
  allOrNone,
  pickerType,
  reset,
  removeSelectedItem,
  selectedItems,
}) {
  const {
    countries,
    translations: { territory_picker: translations },
  } = useConfig()[0]
  const {
    setFieldValue,
    values: { album_type },
  } = useFormikContext()

  const countryAbbrevs = useCountryAbbrevs(countries)

  const removeAll = useCallback(() => {
    setFieldValue(SELECTED_COUNTRIES_FIELD, countryAbbrevs)
    reset()
  }, [countryAbbrevs, reset, setFieldValue])

  const message = `${translations.picker_selection_descriptions[pickerType]}:`.replace(
    '{{album_type}}',
    album_type.toLowerCase()
  )

  if (allOrNone) return null

  return (
    <div style={styles.container}>
      <p>{message}</p>
      <div>
        <Button onClick={removeAll} additionalStyleKey="unstyled">
          {translations.remove_all}
        </Button>
        {selectedItems.sort().map((country) => (
          <RemovalButton
            key={country}
            country={country}
            pickerType={pickerType}
            removeSelectedItem={removeSelectedItem}
          />
        ))}
      </div>
    </div>
  )
})
PickerSelectionSummary.displayName = 'PickerSelectionSummary'
PickerSelectionSummary.propTypes = {
  allOrNone: PropTypes.bool.isRequired,
  pickerType: PropTypes.string.isRequired,
  reset: PropTypes.func.isRequired,
  removeSelectedItem: PropTypes.func.isRequired,
  selectedItems: PropTypes.array.isRequired,
}

export default PickerSelectionSummary
