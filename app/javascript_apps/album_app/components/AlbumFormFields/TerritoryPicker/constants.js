const COMPONENT_TYPES = {
  OVERVIEW: 'overview',
  PICKER: 'picker',
}

const PICKER_TYPES = {
  EXCLUSION: 'exclusion',
  INCLUSION: 'inclusion',
  WORLDWIDE: 'worldwide',
}
const SELECTED_COUNTRIES_FIELD = 'selected_countries'

export { COMPONENT_TYPES, PICKER_TYPES, SELECTED_COUNTRIES_FIELD }
