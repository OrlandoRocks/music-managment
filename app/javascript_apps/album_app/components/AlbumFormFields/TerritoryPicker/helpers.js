import { useMemo } from 'react'

import { COMPONENT_TYPES, PICKER_TYPES } from './constants'

const { OVERVIEW } = COMPONENT_TYPES
const { EXCLUSION, INCLUSION, WORLDWIDE } = PICKER_TYPES

function checkIsWorldwide(countries, selected_countries) {
  if (!countries || !countries.length) return true

  return (
    selectedAll(countries, selected_countries) ||
    selected_countries.length === 0
  )
}

// Filters countries as exclusions or inclusions depending on pickerType
function getSelectedCountryNames(countries, selected_countries) {
  const pickerType = getPickerType(countries, selected_countries)

  return countries
    .filter(([_, abbrev]) => {
      switch (pickerType) {
        case EXCLUSION:
          return !selected_countries.includes(abbrev)
        case INCLUSION:
          return selected_countries.includes(abbrev)
        default:
          return []
      }
    })
    .map(([name, _]) => name)
}

function useSelectedCountryNames(countries, selected_countries) {
  return useMemo(
    () => getSelectedCountryNames(countries, selected_countries).join(', '),
    [countries, selected_countries]
  )
}

function getPickerType(countries, selected_countries) {
  if (checkIsWorldwide(countries, selected_countries)) return WORLDWIDE

  return countries.length / 2 < selected_countries.length
    ? EXCLUSION
    : INCLUSION
}

// Returns array of abbreviations, for form submission
function determineSelectedCountries(countries, pickerType, selectedItems) {
  let result

  switch (pickerType) {
    case PICKER_TYPES.EXCLUSION:
      result = countries
        .filter(([_, abbrev]) => {
          return !selectedItems.some(([_, siAbbrev]) => siAbbrev === abbrev)
        })
        .map(([_, abbrev]) => abbrev)
      break
    case PICKER_TYPES.INCLUSION:
      result = selectedItems.map(([_, abbreviation]) => abbreviation)
      break
    default:
      break
  }

  return result
}

function selectedNothing(
  showModal,
  currentComponent,
  countries,
  selected_countries
) {
  return (
    (selected_countries.length === 0 ||
      selected_countries.length === countries.length) &&
    (currentComponent === OVERVIEW || showModal === false)
  )
}

function selectedAll(countries, selected_countries) {
  return countries.length === selected_countries.length
}

function useCountryAbbrevs(countries) {
  return useMemo(() => countries.map(([_, abbrev]) => abbrev), [countries])
}

export {
  checkIsWorldwide,
  useCountryAbbrevs,
  determineSelectedCountries,
  getPickerType,
  getSelectedCountryNames,
  useSelectedCountryNames,
  selectedAll,
  selectedNothing,
}
