import React from 'react'
import PropTypes from 'prop-types'

const TerritoryRadio = React.memo(function TerritoryRadio({
  checked,
  id,
  translatedLabel,
  onChange,
  ...props
}) {
  return (
    <label className="radio" htmlFor={id}>
      <input
        checked={checked}
        id={id}
        onChange={onChange}
        type="radio"
        {...props}
      />
      {translatedLabel}
    </label>
  )
})
TerritoryRadio.displayName = 'TerritoryRadio'
TerritoryRadio.propTypes = {
  checked: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  translatedLabel: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default TerritoryRadio
