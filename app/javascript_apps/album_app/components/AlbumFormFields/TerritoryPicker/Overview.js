import React, { useCallback } from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import {
  ModalHeader,
  styles as ModalStyles,
} from '../../shared/ModalComponents'
import Button from '../../shared/Button'
import SelectionSummary from './SelectionSummary'
import ArrowIcon from '../../../../shared/svg/arrowIcon'
import TerritoryRadio from './TerritoryRadio'
import { COMPONENT_TYPES, PICKER_TYPES } from './constants'
import { useSelectedCountryNames } from './helpers'

import { useConfig } from '../../../contexts/ConfigContext'

const { PICKER } = COMPONENT_TYPES
const { INCLUSION, EXCLUSION, WORLDWIDE } = PICKER_TYPES

export default function Overview({
  isWorldwide,
  pickerType,
  setCurrentComponent,
  setPickerAndResetSelection,
  toggleModal,
}) {
  const {
    countries,
    translations: { territory_picker: translations },
  } = useConfig()[0]
  const {
    values: { album_type, selected_countries },
  } = useFormikContext()

  const handleChange = useCallback(
    (e) => {
      const { name } = e.target
      setPickerAndResetSelection(name)

      if (name !== WORLDWIDE) {
        setCurrentComponent(PICKER)
      }
    },
    [setCurrentComponent, setPickerAndResetSelection]
  )
  const selectedCountryNames = useSelectedCountryNames(
    countries,
    selected_countries
  )
  const translatedHeader = translations.overview_header.replace(
    '{{album_type}}',
    album_type.toLowerCase()
  )
  const { selection_type_descriptions } = translations

  return (
    <>
      <ModalHeader>{translatedHeader}</ModalHeader>
      <div style={ModalStyles.vSpacer1} />
      <TerritoryRadio
        checked={isWorldwide}
        data-testid="territory-restrictions-worldwide"
        id="territory-worldwide-yes"
        name={WORLDWIDE}
        onChange={handleChange}
        translatedLabel={selection_type_descriptions[WORLDWIDE]}
      />
      <TerritoryRadio
        checked={!isWorldwide && pickerType === EXCLUSION}
        data-testid="territory-restrictions-exclusion"
        id="territory-exclusion"
        name={EXCLUSION}
        onChange={handleChange}
        translatedLabel={selection_type_descriptions[EXCLUSION]}
      />
      <SelectionSummary
        handleChange={handleChange}
        hide={pickerType !== EXCLUSION}
        selectedCountryNames={selectedCountryNames}
        pickerType={pickerType}
      />
      <TerritoryRadio
        checked={!isWorldwide && pickerType === INCLUSION}
        data-testid="territory-restrictions-inclusion"
        id="territory-inclusion"
        name={INCLUSION}
        onChange={handleChange}
        translatedLabel={selection_type_descriptions[INCLUSION]}
      />
      <SelectionSummary
        handleChange={handleChange}
        hide={pickerType !== INCLUSION}
        selectedCountryNames={selectedCountryNames}
        pickerType={pickerType}
      />
      <Button
        additionalStyleKey="back"
        aria-label={translations.back_to_form}
        onClick={toggleModal}
      >
        <ArrowIcon isBack />
      </Button>
    </>
  )
}

Overview.propTypes = {
  isWorldwide: PropTypes.bool.isRequired,
  pickerType: PropTypes.string.isRequired,
  setCurrentComponent: PropTypes.func.isRequired,
  setPickerAndResetSelection: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}
