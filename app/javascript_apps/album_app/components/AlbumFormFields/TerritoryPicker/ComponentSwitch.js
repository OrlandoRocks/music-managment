import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import Overview from './Overview'
import TerritoryPicker from './TerritoryPicker'

import { COMPONENT_TYPES } from './constants'

const { OVERVIEW, PICKER } = COMPONENT_TYPES

function ComponentSwitch({
  currentComponent,
  isWorldwide,
  pickerType,
  setCurrentComponent,
  setPickerAndResetSelection,
  toggleModal,
}) {
  const classes = useStyles()

  switch (currentComponent) {
    case OVERVIEW:
      return (
        <div className={classes.wrapper}>
          <Overview
            isWorldwide={isWorldwide}
            pickerType={pickerType}
            setCurrentComponent={setCurrentComponent}
            setPickerAndResetSelection={setPickerAndResetSelection}
            toggleModal={toggleModal}
          />
        </div>
      )
    case PICKER:
      return (
        <div className={classes.wrapper}>
          <TerritoryPicker
            pickerType={pickerType}
            setCurrentComponent={setCurrentComponent}
            toggleModal={toggleModal}
          />
        </div>
      )
    default:
      return null
  }
}

ComponentSwitch.propTypes = {
  currentComponent: PropTypes.string.isRequired,
  isWorldwide: PropTypes.bool.isRequired,
  pickerType: PropTypes.string.isRequired,
  setCurrentComponent: PropTypes.func.isRequired,
  setPickerAndResetSelection: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

export default React.memo(ComponentSwitch)

const useStyles = makeStyles(() => ({
  wrapper: {
    padding: '1em',
    width: '700px',
    '@media screen and (max-width: 767px)': {
      height: '100vh',
      width: '100vw',
    },
  },
}))
