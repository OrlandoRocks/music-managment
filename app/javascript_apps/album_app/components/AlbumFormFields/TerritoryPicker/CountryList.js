import React from 'react'
import PropTypes from 'prop-types'

import { SmallButton } from '../../shared/Button'

import { useConfig } from '../../../contexts/ConfigContext'

const styles = {
  container: {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'space-between',
    listStyle: 'none',
    padding: '.25em',
    width: '100%',
  },
  exclude: {
    background: '#EB3A24',
    color: '#FFFFFF',
  },
  excluded: {
    background: '#FBE9E6',
    color: '#EB4831',
  },
  include: {
    background: '#52B453',
    color: '#FFFFFF',
  },
  included: {
    background: '#E8F7ED',
    color: '#52B453',
  },
}

const actionTranslationKeys = {
  exclusion: {
    do: 'exclude',
    done: 'excluded',
  },
  inclusion: {
    do: 'include',
    done: 'included',
  },
}

export const CountryListItem = React.memo(function CountryListItem({
  country,
  isSelected,
  pickerType,
}) {
  const {
    translations: { territory_picker: translations },
  } = useConfig()[0]
  const [name, _] = country
  const currentActionKeys = actionTranslationKeys[pickerType]
  const currentAction = isSelected
    ? translations[currentActionKeys.done]
    : translations[currentActionKeys.do]
  const styleKey = isSelected
    ? actionTranslationKeys[pickerType].done
    : actionTranslationKeys[pickerType].do

  return (
    <div style={styles.container}>
      {name}
      <SmallButton styleOverrides={styles[styleKey]}>
        {currentAction}
      </SmallButton>
    </div>
  )
})
CountryListItem.displayName = 'CountryListItem'
CountryListItem.propTypes = {
  country: PropTypes.array.isRequired,
  isSelected: PropTypes.bool.isRequired,
  pickerType: PropTypes.string.isRequired,
}
