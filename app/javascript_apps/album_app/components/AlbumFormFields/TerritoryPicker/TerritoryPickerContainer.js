import React, { useCallback, useEffect, useState } from 'react'
import { useFormikContext } from 'formik'

import { useConfig } from '../../../contexts/ConfigContext'
import { AlbumModal } from '../../AlbumModal'
import ComponentSwitch from './ComponentSwitch'
import {
  COMPONENT_TYPES,
  PICKER_TYPES,
  SELECTED_COUNTRIES_FIELD,
} from './constants'
import {
  getPickerType,
  useCountryAbbrevs,
  useSelectedCountryNames,
  selectedAll,
  selectedNothing,
} from './helpers'
import SelectionSummary from './SelectionSummary'
import TerritoryRadio from './TerritoryRadio'

const styles = {
  modalHeader: {
    display: 'none',
  },
  modalOverrides: {
    content: {
      padding: '0',
    },
  },
}
const { OVERVIEW, PICKER } = COMPONENT_TYPES
const { WORLDWIDE } = PICKER_TYPES

/**
 * - Init component pickerType with getPickerType
 * - Manually set pickerType thereafter
 * - Reset selection when pickerType changes
 * - show/hide details based on pickerType and selection
 * - Empty selection becomes select all when modal is closed
 */
const TerritoryPickerContainer = React.memo(
  function TerritoryPickerContainer() {
    /**
     * CONTEXT
     */

    const {
      countries,
      translations: { territory_picker: translations },
    } = useConfig()[0]
    const {
      setFieldValue,
      values: { selected_countries },
    } = useFormikContext()

    /**
     * MODAL STATE
     */

    const [showModal, setShowModal] = useState(false)
    const toggleModal = useCallback(() => {
      setShowModal((showModal) => !showModal)
    }, [setShowModal])

    /**
     * PICKER STATE
     */

    const [currentComponent, setCurrentComponent] = useState(OVERVIEW)
    const [pickerType, setPickerType] = useState(
      getPickerType(countries, selected_countries)
    )

    /**
     * UTILS
     */

    const countryAbbrevs = useCountryAbbrevs(countries)

    const setWorldwide = useCallback(() => {
      setCurrentComponent(OVERVIEW)
      setPickerType(WORLDWIDE)
      setFieldValue(SELECTED_COUNTRIES_FIELD, countryAbbrevs)
    }, [countryAbbrevs, setFieldValue, setPickerType])

    const selectedCountryNames = useSelectedCountryNames(
      countries,
      selected_countries
    )

    const setPickerAndResetSelection = useCallback(
      (type) => {
        setPickerType((previousType) => {
          if (type !== previousType) {
            setFieldValue(SELECTED_COUNTRIES_FIELD, countryAbbrevs)
          }
          return type
        })
      },
      [countryAbbrevs, setFieldValue, setPickerType]
    )

    const switchComponent = useCallback(() => {
      toggleModal()

      if (name === WORLDWIDE) {
        setFieldValue(SELECTED_COUNTRIES_FIELD, countryAbbrevs)
      }

      setCurrentComponent(PICKER)
    }, [countryAbbrevs, setCurrentComponent, setFieldValue, toggleModal])

    const isWorldwide =
      currentComponent === OVERVIEW &&
      selectedAll(countries, selected_countries)

    /**
     * EFFECTS
     */

    // Set WORLDWIDE when selected nothing AND (on Overview, or hard-exited from picker to closed)
    useEffect(() => {
      if (!Array.isArray(selected_countries)) return
      if (
        selectedNothing(
          showModal,
          currentComponent,
          countries,
          selected_countries
        )
      ) {
        setWorldwide()
      }
    }, [
      countries,
      currentComponent,
      selected_countries,
      setWorldwide,
      showModal,
    ])

    return (
      <div className="grid-x div-album-form-field">
        <label className="cell medium-2" htmlFor={translations.field_label}>
          <span>{translations.field_label}</span>
        </label>
        <div className="cell medium-10">
          <TerritoryRadio
            checked={!isWorldwide}
            data-testid="container-territory-restrictions"
            id="territory-worldwide-no"
            onChange={toggleModal}
            translatedLabel={translations.top_radio.restrictions}
          />
          <SelectionSummary
            handleChange={switchComponent}
            selectedCountryNames={selectedCountryNames}
            pickerType={pickerType}
          />
          <TerritoryRadio
            checked={isWorldwide}
            data-testid="container-territory-worldwide"
            id="opener-territory-worldwide-yes"
            translatedLabel={translations.top_radio.worldwide}
            onChange={setWorldwide}
          />
        </div>
        <AlbumModal
          // the component that will mount:
          BodyComponent={ComponentSwitch}
          // modal props:
          contentLabel={translations.aria_modal_content_label}
          customStyles={styles.modalOverrides}
          headerStyle={styles.modalHeader}
          setShow={setShowModal}
          show={showModal}
          // pass-through props:
          toggleModal={toggleModal}
          currentComponent={currentComponent}
          setCurrentComponent={setCurrentComponent}
          pickerType={pickerType}
          setPickerType={setPickerType}
          isWorldwide={isWorldwide}
          setPickerAndResetSelection={setPickerAndResetSelection}
        />
      </div>
    )
  }
)
TerritoryPickerContainer.displayName = 'TerritoryPickerContainer'

export default TerritoryPickerContainer
