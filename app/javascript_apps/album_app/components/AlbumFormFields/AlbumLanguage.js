import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const langCodeOptions = (languageCodes) =>
  languageCodes.map((lang) => (
    <option key={lang.id} value={lang.code}>
      {lang.description}
    </option>
  ))

const AlbumLanguage = ({ defaultValue, handleChange, name }) => {
  const { languageCodes, translations } = useConfig()[0]

  return (
    <div className="grid-x div-album-form-field">
      <label className="cell medium-2" htmlFor="select-language">
        <span>{translations.language}*</span>
      </label>
      <select
        id="select-language"
        name={name}
        className="cell medium-6 select-language"
        defaultValue={defaultValue}
        onChange={handleChange}
      >
        {langCodeOptions(languageCodes)}
      </select>
      <span className="cell medium-4"></span>
    </div>
  )
}
AlbumLanguage.displayName = 'AlbumLanguage'

AlbumLanguage.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string,
  handleChange: PropTypes.func,
}

export default React.memo(AlbumLanguage)
