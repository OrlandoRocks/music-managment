import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const OptionalIsrc = ({ defaultValue, handleChange, name }) => {
  const {
    releaseTypeVars: { optionalIsrcFieldEnabled },
    translations,
  } = useConfig()[0]

  if (!optionalIsrcFieldEnabled) return null

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <label className="cell medium-2" htmlFor={name}>
          <span className="cell medium-2 optional">
            {translations.optional_isrc}
            <br />
            <em>{translations.optional}</em>
          </span>
        </label>
        <input
          type="text"
          name={name}
          className="cell medium-6 input-upc"
          defaultValue={defaultValue}
          onChange={handleChange}
        />
        <span className="cell medium-4"></span>
      </div>
      <div className="grid-x div-album-form-field">
        <span className="cell medium-2"></span>
        <div className="cell medium-10 note">
          <em>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={translations.optional_isrc_note_url}
            >
              {translations.optional_isrc_note}
            </a>
          </em>
        </div>
      </div>
    </div>
  )
}

OptionalIsrc.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string,
  handleChange: PropTypes.func,
}

export default React.memo(OptionalIsrc)
