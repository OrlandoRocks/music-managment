import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const AlbumTitle = ({
  albumType,
  defaultValue,
  handleBlur,
  handleChange,
  name,
}) => {
  const { translations } = useConfig()[0]
  const translationsKey = `${albumType.toLowerCase()}_title`

  return (
    <div className="grid-x div-album-form-field">
      <label className="cell medium-2" htmlFor={name}>
        <span>{translations[translationsKey]}*</span>
      </label>
      <input
        className="cell medium-6 input-album-name"
        defaultValue={defaultValue}
        id={name}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        type="text"
      />
    </div>
  )
}
AlbumTitle.displayName = 'AlbumTitle'

AlbumTitle.propTypes = {
  name: PropTypes.string,
  defaultValue: PropTypes.string,
  albumType: PropTypes.string,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
}

export default React.memo(AlbumTitle)
