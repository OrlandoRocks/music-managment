import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'
import Spinner from '../shared/Spinner'
import Button from '../shared/Button'

function Submit({ albumSubmitRef, disabled, isSubmitting, submitSuccess }) {
  const { translations } = useConfig()[0]

  return (
    <div className="grid-x div-album-form-field">
      <span className="cell medium-2"></span>
      {isSubmitting || submitSuccess ? (
        <div className="cell medium-10">
          <Spinner />
        </div>
      ) : (
        <div className="cell medium-6" style={styles.buttonContainer}>
          <Button
            additionalStyleKey="large"
            customRef={albumSubmitRef}
            data-testid="AlbumForm Submit"
            disabled={disabled}
            id="btn-save"
            type="submit"
          >
            {translations.save_album_and_add_songs}
          </Button>
        </div>
      )}
    </div>
  )
}

Submit.propTypes = {
  albumSubmitRef: PropTypes.object,
  disabled: PropTypes.bool,
  isSubmitting: PropTypes.bool,
  submitSuccess: PropTypes.bool,
}

export default React.memo(Submit)

const styles = {
  buttonContainer: {
    marginTop: '1em',
  },
}
