import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const AlbumReleaseTimeScenario = ({ handleChange, name, releaseScenario }) => {
  const {
    releaseTypeVars: { timedReleaseDisabled },
    translations,
  } = useConfig()[0]

  if (timedReleaseDisabled) return null

  const isRelativeTime = releaseScenario === 'relative_time'
  const isAbsoluteTime = releaseScenario === 'absolute_time'

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <span className="cell medium-2"></span>
        <div className="cell medium-10" style={styles.releaseTimeExamples}>
          <label className="radio release-time-scenario">
            <input
              id="scenario-relative"
              type="radio"
              value="relative_time"
              name={name}
              onChange={handleChange}
              checked={isRelativeTime}
            />
            {translations.release_scenario_relative}
            <p className="release-example">
              {translations.release_scenario_relative_example}
            </p>
          </label>
          <label className="radio release-time-scenario">
            <input
              id="scenario-absolute"
              type="radio"
              value="absolute_time"
              name={name}
              onChange={handleChange}
              checked={isAbsoluteTime}
            />
            {translations.release_scenario_absolute}
            <p className="release-example">
              {translations.release_scenario_absolute_example}
            </p>
          </label>
        </div>
      </div>
    </div>
  )
}

AlbumReleaseTimeScenario.propTypes = {
  name: PropTypes.string,
  releaseScenario: PropTypes.string,
  handleChange: PropTypes.func,
}

export default React.memo(AlbumReleaseTimeScenario)

const styles = {
  releaseTimeExamples: {
    marginBottom: '-.5em',
  },
}
