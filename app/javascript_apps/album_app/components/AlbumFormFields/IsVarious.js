import React, { useCallback, useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import { AlbumModal } from '../AlbumModal'
import { NegativePositiveButtons } from '../../components/shared/NegativePositiveButtons'
import { useConfig } from '../../contexts/ConfigContext'
import { styles as buttonStyles } from '../shared/Button'

const useStyles = makeStyles(() => ({
  variousBody: {
    padding: '1em 0',
  },
  variousBTNsContainer: {
    display: 'flex',
    justifyContent: 'center',
    padding: '.5em 0 0',
  },
  variousP: { fontSize: '.9em', lineHeight: '1.4em' },
}))

const IsVariousModal = React.memo(function IsVariousModal({
  modalActionCallback,
  positiveAction,
  negativeAction,
}) {
  const { translations } = useConfig()[0]

  const negativeHandler = () => modalActionCallback(negativeAction)
  const positiveHandler = () => modalActionCallback(positiveAction)

  const classes = useStyles()

  const legacyButtonStyles = {
    positive: { ...buttonStyles.shared, marginRight: '1em' },
    negative: { ...buttonStyles.shared, ...buttonStyles.secondary },
  }

  return (
    <div className={classes.variousBody}>
      <p className={classes.variousP}>
        {translations.various_artists_modal_body}
      </p>
      {/* TODO: use MUI buttons */}
      <div className={classes.variousBTNsContainer}>
        <NegativePositiveButtons
          handleNegative={negativeHandler}
          handlePositive={positiveHandler}
          negativeText={translations.various_artists_modal_cancel}
          positiveText={translations.various_artists_modal_continue}
          positiveStyle={legacyButtonStyles.positive}
          negativeStyle={legacyButtonStyles.negative}
        />
      </div>
    </div>
  )
})
IsVariousModal.displayName = 'IsVariousModal'

IsVariousModal.propTypes = {
  modalActionCallback: PropTypes.func.isRequired,
  negativeAction: PropTypes.func,
  positiveAction: PropTypes.func.isRequired,
}

const styles = {
  content: {
    height: 'auto',
  },
}

const IsVarious = ({ currentValue, setFieldValue }) => {
  const [showIsVariousModal, setShowIsVariousModal] = useState(false)
  const {
    releaseTypeVars: { variousFieldEnabled },
    translations,
  } = useConfig()[0]

  const showModal = useCallback(() => setShowIsVariousModal(true), [
    setShowIsVariousModal,
  ])
  const positiveAction = useCallback(
    () => setFieldValue('is_various', !currentValue),
    [currentValue, setFieldValue]
  )

  const negativeAction = useCallback(() => setFieldValue('is_various', false), [
    setFieldValue,
  ])

  function handleClick() {
    if (!currentValue) {
      showModal()
    } else {
      positiveAction()
    }
  }

  if (!variousFieldEnabled) return null

  return (
    <div className="grid-x div-album-form-field">
      <span className="cell medium-2"></span>
      <label htmlFor="cbx-is-various" className="cell medium-10">
        <input
          data-testid="various artist toggle"
          id="cbx-is-various"
          type="checkbox"
          className="cbx-is-various"
          checked={!!currentValue}
          onChange={handleClick}
        />
        &nbsp;
        {translations.various_artists_prompt}
      </label>
      <AlbumModal
        BodyComponent={IsVariousModal}
        contentLabel={translations.various_artists_modal_aria}
        customStyles={styles}
        headerMessage={translations.various_artists_modal_header}
        positiveAction={positiveAction}
        negativeAction={negativeAction}
        setShow={setShowIsVariousModal}
        show={showIsVariousModal}
      />
    </div>
  )
}

IsVarious.propTypes = {
  currentValue: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
}

export default React.memo(IsVarious)
