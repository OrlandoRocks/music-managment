import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const hoursOptions = () => {
  return Array(12)
    .fill()
    .map((_, i) => {
      const hour = String(i + 1).padStart(2, '0')
      return (
        <option key={hour} value={hour}>
          {hour}
        </option>
      )
    })
}

const minutesOptions = () => {
  return Array(60)
    .fill()
    .map((_, i) => {
      const minute = String(i).padStart(2, '0')
      return (
        <option key={minute} value={minute}>
          {minute}
        </option>
      )
    })
}

const meridianOptions = () => {
  return [
    <option key="am" value="am">
      am
    </option>,
    <option key="pm" value="pm">
      pm
    </option>,
  ]
}

const AlbumGoliveDate = ({ defaultValue, handleChange }) => {
  const {
    releaseTypeVars: { timedReleaseDisabled },
    translations,
  } = useConfig()[0]

  if (timedReleaseDisabled) return null

  const hours = hoursOptions()
  const minutes = minutesOptions()
  const meridians = meridianOptions()

  const { hour, min } = defaultValue
  const meridian = defaultValue.meridian && defaultValue.meridian.toLowerCase()

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <label
          className="cell medium-2 optional"
          htmlFor={translations.golive_date}
        >
          <span>{translations.golive_date}</span>
        </label>

        <div className="cell medium-6 input-group golive-input">
          <select
            id="select-hours"
            name="golive_date.hour"
            defaultValue={hour}
            onChange={handleChange}
          >
            {hours}
          </select>

          <span>:</span>

          <select
            id="select-minutes"
            name="golive_date.min"
            defaultValue={min}
            onChange={handleChange}
          >
            {minutes}
          </select>

          <select
            id="select-meridian"
            name="golive_date.meridian"
            defaultValue={meridian}
            onChange={handleChange}
          >
            {meridians}
          </select>
        </div>
      </div>
      <div className="grid-x div-album-form-field">
        <span className="cell medium-2"></span>
        <div className="cell medium-10 note">
          <em>{translations.golive_date_note}</em>
        </div>
      </div>
    </div>
  )
}
AlbumGoliveDate.displayName = 'AlbumGoLiveDate'

AlbumGoliveDate.propTypes = {
  defaultValue: PropTypes.object,
  handleChange: PropTypes.func,
}

export default React.memo(AlbumGoliveDate)
