import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import dayjs from 'dayjs'
import { KeyboardDatePicker } from '@material-ui/pickers'

import { onlyDate, parseYesNo } from '../../../utils'

import { useConfig } from '../../contexts/ConfigContext'

const PreviouslyReleased = ({
  handleChange,
  isPreviouslyReleased,
  origReleaseYear,
  setFieldValue,
  setTouched,
}) => {
  const { translations } = useConfig()[0]

  const defaultDate = dayjs().subtract(1, 'day').toDate()
  const maxDate = defaultDate
  const formattedOriginalReleaseDate = dayjs(
    origReleaseYear || defaultDate
  ).toDate()

  const handleDateChange = useCallback(
    (date) => {
      setFieldValue('orig_release_year', onlyDate(date))
      setTouched({ sale_date: true })
    },
    [setFieldValue, setTouched]
  )

  const handleBoolean = useCallback(
    (e) => {
      const value = parseYesNo(e.target.value)

      handleChange({
        target: { name: e.target.name, value },
      })

      if (value && !origReleaseYear) {
        setFieldValue('orig_release_year', onlyDate(defaultDate))
      }
    },
    [defaultDate, handleChange, origReleaseYear, setFieldValue]
  )

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <label
          className="cell medium-2"
          htmlFor={translations.previously_released}
        >
          <span>{translations.previously_released}</span>
        </label>
        <div className="cell medium-10">
          <label className="radio">
            <input
              checked={!isPreviouslyReleased}
              id="previously-released-no"
              name="is_previously_released"
              onChange={handleBoolean}
              type="radio"
              value="No"
            />
            {translations.not_previously_released}
          </label>
          <label className="radio">
            <input
              checked={isPreviouslyReleased}
              id="previously-released-yes"
              name="is_previously_released"
              onChange={handleBoolean}
              type="radio"
              value="Yes"
            />
            {translations.yes_previously_released}
          </label>
        </div>
      </div>
      {isPreviouslyReleased && (
        <div className="grid-x div-album-form-field">
          <label
            className="cell medium-2"
            htmlFor={translations.original_release_date}
          >
            <span>{translations.original_release_date}</span>
          </label>
          <div className="cell medium-4">
            <div style={styles.buttonContainer}>
              <KeyboardDatePicker
                autoOk
                format="L"
                id={translations.original_release_date}
                InputAdornmentProps={{
                  children: <div>asdf</div>,
                  color: 'primary',
                  position: 'end',
                }}
                inputVariant="outlined"
                maxDate={maxDate}
                onChange={handleDateChange}
                ToolbarComponent={() => null}
                value={formattedOriginalReleaseDate}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  )
}
PreviouslyReleased.displayName = 'PreviouslyReleased'

PreviouslyReleased.propTypes = {
  origReleaseYear: PropTypes.string,
  isPreviouslyReleased: PropTypes.bool,
  handleChange: PropTypes.func,
  setFieldValue: PropTypes.func,
  setTouched: PropTypes.func,
}

export default React.memo(PreviouslyReleased)

const styles = {
  buttonContainer: {
    marginBottom: '1em',
  },
}
