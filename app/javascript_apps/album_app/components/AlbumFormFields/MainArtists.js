// TODO: delete me pending MAM launch
import React, { useCallback, useMemo } from 'react'

import MainArtistInput from './MainArtistInput'
import { defaultCreative } from '../../utils/defaultCreative'

import { useConfig } from '../../contexts/ConfigContext'
import { useFormikContext } from 'formik'
import { PrimaryButton } from '../shared/MUIButtons'
import { getShowPlanUpsell, getShowRowUpsell } from '../shared/artistUpsell'
import LockMessage from './LockMessage'
import { STORE_MAX_ARTIST_WARNING_THRESHOLD } from './StoreMaxArtistsWarning'

const styles = {
  addButton: { marginTop: '-.25em' },
  upsellContainer: {
    margin: '0 0 1em',
  },
}

// This is the condensed Artist Input without Spotify and Apple inputs
const MainArtists = () => {
  const {
    activeArtists,
    person: { person_plan },
    possibleArtists,
    translations: {
      add_main_artist,
      main_artist,
      plans: { artist_purchase_will_apply },
    },
  } = useConfig()[0]
  const {
    setFieldValue,
    values: { creatives, is_various },
  } = useFormikContext()

  const showPlanUpsell = useMemo(
    () => getShowPlanUpsell(activeArtists, creatives, person_plan),
    [activeArtists, creatives, person_plan]
  )

  const rowUpsells = useMemo(() => {
    return Object.values(creatives).reduce((acc, cur, i) => {
      acc.set(
        i,
        getShowRowUpsell({
          activeArtists,
          index: i,
          name: cur.name,
          person_plan,
        })
      )
      return acc
    }, new Map())
  }, [activeArtists, creatives, person_plan])

  const renderInputs = useMemo(() => {
    return Object.values(creatives).map((creative, i) => {
      const isFirst = i === 0
      const showRowUpsell = rowUpsells.get(i)

      return (
        <React.Fragment key={`${creative.uuid}`}>
          {!isFirst && <LabelPlaceholder />}
          <MainArtistInput
            key={`main-artist-${creative.uuid}`}
            creative={creative}
            creatives={creatives}
            possibleNames={possibleArtists.sort()}
            setFieldValue={setFieldValue}
            showRemoveButton={!isFirst}
            showRowUpsell={showRowUpsell}
            showStoreMaxArtistsWarning={
              i + 1 === STORE_MAX_ARTIST_WARNING_THRESHOLD
            }
          />
        </React.Fragment>
      )
    })
  }, [creatives, possibleArtists, rowUpsells, setFieldValue])

  const addCreative = useCallback(
    (e) => {
      e.preventDefault()
      const newCreative = defaultCreative()
      setFieldValue('creatives', {
        ...creatives,
        [newCreative.uuid]: newCreative,
      })
    },
    [creatives, setFieldValue]
  )

  if (is_various) return null

  return (
    <>
      <div className="grid-x div-album-form-field">
        <label className="cell medium-2" htmlFor={main_artist}>
          <span>{main_artist}*</span>
        </label>
        <div className="cell medium-6">
          <div style={showPlanUpsell ? styles.upsellContainer : null}>
            <LockMessage
              message={artist_purchase_will_apply}
              show={showPlanUpsell}
            />
          </div>
          {renderInputs}
        </div>
      </div>
      <div className="grid-x div-album-form-field" style={styles.addButton}>
        <div className="cell medium-2"></div>
        <div className="cell medium-6">
          <PrimaryButton onClick={addCreative} style={{ whiteSpace: 'nowrap' }}>
            {add_main_artist}
          </PrimaryButton>
        </div>
      </div>
    </>
  )
}

const labelStyle = { width: '100%' }
function LabelPlaceholder() {
  return (
    <>
      <div style={labelStyle} />
      <div className="cell medium-2" />
    </>
  )
}

export default MainArtists
