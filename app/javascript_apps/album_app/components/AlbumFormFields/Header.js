import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

export function Header({ album }) {
  const { translations } = useConfig()[0]

  const headerText = albumOrSingleHeaderText(album, translations)

  return (
    <div className="album-app-title">
      <h1>{album.name || headerText} </h1>
      <span>*&nbsp;{translations.required_field}</span>
    </div>
  )
}

Header.propTypes = {
  album: PropTypes.object,
}

export default Header

function albumOrSingleHeaderText(album, translations) {
  const view = album.is_new ? 'create' : 'edit'
  return translations[`${view}_${album.album_type.toLowerCase()}`]
}
