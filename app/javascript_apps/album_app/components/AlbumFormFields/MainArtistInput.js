// TODO: delete me pending MAM launch
import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import AutocompleteField from '../../../shared/AutocompleteField'
import { useConfig } from '../../contexts/ConfigContext'
import LockMessage from './LockMessage'
import { StoreMaxArtistsWarning } from './StoreMaxArtistsWarning'

const useStyles = makeStyles(() => ({
  input: {
    margin: '.75em 0',
  },
}))

const styles = {
  container: { display: 'flex', alignItems: 'center' },
  removalButton: { flex: 1 },
  storeMaxArtistsContainer: {
    alignItems: 'center',
    display: 'flex',
    margin: '.5em 0',
  },
  upsellContainer: {
    marginBottom: '1em',
  },
  upsellRowContainer: {
    margin: '.25em 0',
  },
  wrapperStyle: { flex: 11 },
}

const MainArtistInput = ({
  creative,
  creatives,
  possibleNames,
  setFieldValue,
  showRemoveButton,
  showRowUpsell,
  showStoreMaxArtistsWarning,
}) => {
  const classes = useStyles()
  const {
    translations: {
      artist_name_input: { placeholder },
      plans: { artist_not_yet_purchased },
      remove_button,
      store_max_artists_warning,
    },
  } = useConfig()[0]

  const handleUpdate = useCallback(
    (e) => {
      const newCreatives = { ...creatives }
      newCreatives[creative.uuid].name = e.target.value
      setFieldValue('creatives', newCreatives)
    },
    [creative, creatives, setFieldValue]
  )

  const handleRemove = useCallback(() => {
    const { [creative.uuid]: _omit, ...newCreatives } = creatives
    setFieldValue('creatives', newCreatives)
  }, [creative, creatives, setFieldValue])

  return (
    <div>
      <div
        style={
          showStoreMaxArtistsWarning ? styles.storeMaxArtistsContainer : null
        }
      >
        <StoreMaxArtistsWarning
          message={store_max_artists_warning}
          show={showStoreMaxArtistsWarning}
        />
      </div>
      <div style={styles.container}>
        <AutocompleteField
          ariaLabel={placeholder}
          className={showRemoveButton ? classes.input : null}
          key={`main-artist-${creative.uuid}`}
          type="text"
          name="name"
          value={creative.name || ''}
          onChange={handleUpdate}
          items={possibleNames}
          wrapperStyle={styles.wrapperStyle}
        />
        {showRemoveButton && (
          <a
            aria-label={remove_button}
            className="cell medium-2 remove-main-artist text-center"
            onClick={handleRemove}
            role="button"
            style={styles.removalButton}
          >
            <i className="fa fa-times" />
          </a>
        )}
      </div>
      {showRowUpsell && (
        <div style={styles.upsellRowContainer}>
          <LockMessage
            message={artist_not_yet_purchased}
            show={showRowUpsell}
          />
        </div>
      )}
    </div>
  )
}

MainArtistInput.propTypes = {
  creative: PropTypes.object,
  creatives: PropTypes.object,
  setFieldValue: PropTypes.func,
  dispatch: PropTypes.func,
  possibleNames: PropTypes.array,
  showRemoveButton: PropTypes.bool,
}

export default MainArtistInput
