import React from 'react'
import { makeStyles } from '@material-ui/styles'

import { useConfig } from '../../contexts/ConfigContext'

const ADVANCED_FEATURES = [
  'custom_label',
  'recording_location',
  'ringtones',
  'territory_restrictions',
  'upc',
]
Object.freeze(ADVANCED_FEATURES)

const useStyles = makeStyles(() => ({
  headline: { color: '#3a96cf', fontSize: '1.75em', fontWeight: 700 },
}))

export function AdvancedFeaturesHeader() {
  const {
    features: { plans_pricing },
    person: { person_plan, plan_features },
    translations: {
      advanced_distribution_header,
      advanced_distribution_subheader,
    },
  } = useConfig()[0]

  const classes = useStyles()

  return (
    <div>
      <hr />
      <h1 className={classes.headline}>{advanced_distribution_header}</h1>
      {showSubheader(person_plan, plan_features, plans_pricing) && (
        <p>{advanced_distribution_subheader}</p>
      )}
    </div>
  )
}

function showSubheader(person_plan, plan_features, plans_pricing) {
  if (
    plans_pricing === false ||
    person_plan === null ||
    ADVANCED_FEATURES.every((f) => Boolean(plan_features[f]))
  ) {
    return false
  }

  return true
}

export default AdvancedFeaturesHeader
