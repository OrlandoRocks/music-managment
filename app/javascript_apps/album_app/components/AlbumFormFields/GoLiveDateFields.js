import React from 'react'
import AlbumSaleDate from './AlbumSaleDate'
import AlbumGoliveDate from './AlbumGoliveDate'
import AlbumReleaseTimeScenario from './AlbumReleaseTimeScenario'
import PropTypes from 'prop-types'

const GoLiveDateFields = function GoLiveDateFields({
  album,
  handleChange,
  setTouched,
  setValues,
  timedReleaseDisabled,
  timingScenarioFieldName,
  values,
}) {
  if (timedReleaseDisabled) {
    return (
      <AlbumSaleDate
        saleDate={values.sale_date}
        setTouched={setTouched}
        setValues={setValues}
        values={values}
      />
    )
  }

  return (
    <>
      <AlbumSaleDate
        saleDate={values.sale_date}
        setTouched={setTouched}
        setValues={setValues}
        values={values}
      />
      <AlbumGoliveDate
        defaultValue={album.golive_date}
        handleChange={handleChange}
      />
      <AlbumReleaseTimeScenario
        handleChange={handleChange}
        name={timingScenarioFieldName}
        releaseScenario={values.timed_release_timing_scenario}
      />
    </>
  )
}

GoLiveDateFields.defaultProps = {
  timedReleaseDisabled: false,
}

GoLiveDateFields.propTypes = {
  album: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  setTouched: PropTypes.func.isRequired,
  setValues: PropTypes.func.isRequired,
  timedReleaseDisabled: PropTypes.bool,
  timingScenarioFieldName: PropTypes.string.isRequired,
  values: PropTypes.object.isRequired,
}

export default GoLiveDateFields
