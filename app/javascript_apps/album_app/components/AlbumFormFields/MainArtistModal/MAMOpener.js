import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'

import { addNewCreative } from './mamHelpers'
import { defaultCreative } from '../../../utils/defaultCreative'
import ArtistProfileRow from './ArtistProfileRow'
import Button from '../../shared/Button'

import { useConfig } from '../../../contexts/ConfigContext'
import { getShowPlanUpsell, getShowRowUpsell } from '../../shared/artistUpsell'
import LockMessage from '../LockMessage'
import { STORE_MAX_ARTIST_WARNING_THRESHOLD } from '../StoreMaxArtistsWarning'

const styles = {
  subContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '1em',
  },
}

export const MAMOpener = React.memo(function MAMOpener({
  disabled,
  handleRemoveCreative,
  setCurrentArtistUUID,
  toggleModal,
}) {
  const {
    setFieldValue,
    values: { creatives, is_various },
  } = useFormikContext()
  const {
    activeArtists,
    person: { person_plan },
    translations: {
      add_main_artist,
      plans: { artist_purchase_will_apply },
    },
  } = useConfig()[0]

  function handleClick(e) {
    if (disabled) return

    let currentArtistUUID
    const uuid = e.currentTarget.getAttribute('name')

    if (!uuid) {
      const newArtist = defaultCreative()
      currentArtistUUID = newArtist.uuid
      addNewCreative(creatives, newArtist, setFieldValue)
    } else {
      currentArtistUUID = uuid
    }

    setCurrentArtistUUID(currentArtistUUID)
    toggleModal()
  }

  const showPlanUpsell = useMemo(
    () => getShowPlanUpsell(activeArtists, creatives, person_plan),
    [activeArtists, creatives, person_plan]
  )

  const rowUpsells = useMemo(() => {
    return Object.values(creatives).reduce((acc, cur, i) => {
      acc.set(
        i,
        getShowRowUpsell({
          activeArtists,
          index: i,
          name: cur.name,
          person_plan,
        })
      )

      return acc
    }, new Map())
  }, [activeArtists, creatives, person_plan])

  const showRemoveButton = (i) => !disabled && i !== 0

  if (is_various) return null

  return (
    <div className="cell medium-6" style={styles.subContainer}>
      <LockMessage message={artist_purchase_will_apply} show={showPlanUpsell} />
      {Object.values(creatives).map((c, i) => (
        <ArtistProfileRow
          key={c.uuid}
          currentArtistUUID={c.uuid}
          dataTestId={`main-artist-${i + 1}`}
          handleClick={handleClick}
          handleRemoveCreative={handleRemoveCreative}
          showRemoveButton={showRemoveButton(i)}
          showRowUpsell={rowUpsells.get(i)}
          showStoreMaxArtistsWarning={
            i + 1 === STORE_MAX_ARTIST_WARNING_THRESHOLD
          }
        />
      ))}
      <div>
        <Button className="add-main-artist" onClick={handleClick}>
          {add_main_artist}
        </Button>
      </div>
    </div>
  )
})
MAMOpener.displayName = 'MAMOpener'
MAMOpener.propTypes = {
  disabled: PropTypes.bool.isRequired,
  handleRemoveCreative: PropTypes.func.isRequired,
  setCurrentArtistUUID: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}
