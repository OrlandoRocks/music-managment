import React, { useCallback, useState } from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import { AlbumModal } from '../../AlbumModal'
import { pruneAbandonedEntries, removeCreative } from './mamHelpers'
import { useConfig } from '../../../contexts/ConfigContext'
import { MAMOpener } from './MAMOpener'
import MAMStepper from './MAMStepper'

const styles = {
  modalHeader: {
    display: 'none',
  },
  modalOverrides: {
    content: {
      padding: '0',
    },
  },
}

function MainArtistsModal({
  disabled,
  modalUtils: {
    setShowMainArtistModal: setShowModal,
    showMainArtistModal: showModal,
  },
}) {
  const { translations, spotifyArtistsRequired } = useConfig()[0]
  const {
    setFieldValue,
    values: { creatives },
  } = useFormikContext()

  const toggleModal = useCallback(
    () =>
      setShowModal((show) => {
        !show && document.body.scrollIntoView(true)
        setShowModal(!show)
      }),
    [setShowModal]
  )

  const [currentArtistUUID, setCurrentArtistUUID] = useState(undefined)

  function handleRemoveCreative(e) {
    if (disabled) return

    const key = e.currentTarget.getAttribute('name')
    removeCreative(key, creatives, setFieldValue)
  }

  function onRequestClose() {
    pruneAbandonedEntries(creatives, setFieldValue)
  }

  const { artist_modal_a11y_description, main_artist } = translations

  return (
    <div className="grid-x div-album-form-field">
      <label className="cell medium-2" htmlFor={main_artist}>
        <span>{main_artist}*</span>
      </label>
      <MAMOpener
        disabled={disabled}
        handleRemoveCreative={handleRemoveCreative}
        setCurrentArtistUUID={setCurrentArtistUUID}
        toggleModal={toggleModal}
      />
      <AlbumModal
        BodyComponent={MAMStepper}
        contentLabel={artist_modal_a11y_description}
        customStyles={styles.modalOverrides}
        headerStyle={styles.modalHeader}
        onRequestClose={onRequestClose}
        setShow={setShowModal}
        show={showModal}
        // pass-through props
        currentArtistUUID={currentArtistUUID}
        handleRemoveCreative={removeCreative}
        setCurrentArtistUUID={setCurrentArtistUUID}
        spotifyArtistsRequired={spotifyArtistsRequired}
      />
    </div>
  )
}

export default React.memo(MainArtistsModal)

MainArtistsModal.propTypes = {
  disabled: PropTypes.bool.isRequired,
  modalUtils: PropTypes.shape({
    setShowMainArtistModal: PropTypes.func.isRequired,
    showMainArtistModal: PropTypes.bool.isRequired,
  }),
}
