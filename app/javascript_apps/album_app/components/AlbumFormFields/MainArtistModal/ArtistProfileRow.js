import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'

import { makeStyles } from '@material-ui/core/styles'
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle'
import { IconButton } from '@material-ui/core'

import ExternalIdChip from './ExternalIdChip'

import { FALLBACK_ARTIST_IMAGE, SERVICES } from '../../../utils/constants'

import { useESIDData } from '../../../contexts/ESIDDataContext'
import { useConfig } from '../../../contexts/ConfigContext'
import LockMessage from '../LockMessage'
import { StoreMaxArtistsWarning } from '../StoreMaxArtistsWarning'

const styles = {
  storeMaxArtistsContainer: {
    marginBottom: '1em',
  },
  upsellContainer: {
    padding: '.25em 0 .5em .5em',
  },
}

function ArtistProfileRow({
  currentArtistUUID,
  dataTestId,
  handleClick,
  handleRemoveCreative,
  showRemoveButton,
  showRowUpsell,
  showStoreMaxArtistsWarning,
}) {
  const {
    values: { creatives },
  } = useFormikContext()
  const {
    translations: {
      plans: { artist_not_yet_purchased },
      remove_button,
      store_max_artists_warning,
    },
  } = useConfig()[0]

  const { esidData } = useESIDData()
  const artist = creatives[currentArtistUUID]
  const { apple, name, spotify } = artist

  const profPic = getProfPic(currentArtistUUID, esidData)

  const classes = useStyles()

  return (
    <div>
      <div
        style={
          showStoreMaxArtistsWarning ? styles.storeMaxArtistsContainer : null
        }
      >
        <StoreMaxArtistsWarning
          message={store_max_artists_warning}
          show={showStoreMaxArtistsWarning}
        />
      </div>
      <div className={classes.wrapper}>
        <div className={classes.flexDisplayRow}>
          <div
            className={classes.clickArea}
            data-testid={dataTestId}
            name={currentArtistUUID}
            onClick={handleClick}
            role="button"
          >
            <div className={classes.artistDetails}>
              <img src={profPic} width="auto" height={64} />
              <div className={classes.subContainer}>
                <p className="artistName">{name}</p>
                <div>
                  <ExternalIdChip
                    data={spotify}
                    label={SERVICES.spotify.titleCase}
                  />
                  <ExternalIdChip
                    data={apple}
                    label={SERVICES.apple.titleCase}
                  />
                </div>
              </div>
            </div>
            <div style={showRowUpsell ? styles.upsellContainer : null}>
              <LockMessage
                message={artist_not_yet_purchased}
                show={showRowUpsell}
              />
            </div>
          </div>
          <div className={showRemoveButton ? classes.removalContainer : null}>
            {showRemoveButton && (
              <IconButton
                aria-label={remove_button}
                className={classes.removalButton}
                disableRipple
                name={currentArtistUUID}
                onClick={handleRemoveCreative}
              >
                <RemoveCircleIcon />
              </IconButton>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
ArtistProfileRow.propTypes = {
  currentArtistUUID: PropTypes.string,
  dataTestId: PropTypes.string,
  handleClick: PropTypes.func.isRequired,
  handleRemoveCreative: PropTypes.func.isRequired,
  showRemoveButton: PropTypes.bool.isRequired,
  showRowUpsell: PropTypes.bool,
  showStoreMaxArtistsWarning: PropTypes.bool,
}

export default React.memo(ArtistProfileRow)

// Use the Spotify artist image here, because the Apple Music API only provides album artwork.
function getProfPic(currentArtistUUID, esidData) {
  const record = esidData[currentArtistUUID]
  if (record && record.spotify && record.spotify.image) {
    return record.spotify.image
  }

  return FALLBACK_ARTIST_IMAGE
}

const useStyles = makeStyles({
  wrapper: {
    margin: '0 0 20px',
    'box-shadow': '0px 1px 3px #2A2D3966;',
    'border-radius': '4px',
    'align-items': 'center',
    '& img': {
      'border-radius': '4px',
      height: '64px',
      margin: '10px',
      objectFit: 'cover',
    },
    '& .artistName': {
      'font-weight': 'bold',
    },
  },
  flexDisplayRow: {
    display: 'flex',
  },
  artistDetails: {
    display: 'flex',
    width: '100%',
  },
  clickArea: {
    cursor: 'pointer',
    width: '100%',
    '&:hover': {
      background: 'rgba(211,211,211,.3)',
      transition: 'all 0.2s ease-in-out',
    },
  },
  subContainer: {
    display: 'flex',
    flexDirection: 'column',
    margin: '10px 0',
    '& p': {
      wordBreak: 'break-word',
    },
  },
  removalButton: {
    'align-self': 'center',
    color: '#D0021B',
    'margin-left': 'auto',
  },
  removalContainer: {
    borderLeft: '2px solid rgba(211,211,211,.3)',
    display: 'flex',
    padding: '0 1em',
    visibility: 'visible',
  },
})
