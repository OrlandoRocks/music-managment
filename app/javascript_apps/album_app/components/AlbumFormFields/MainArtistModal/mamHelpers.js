import React from 'react'
import produce from 'immer'

import { appleArtistSearchRequest } from '../../../utils/appleAPIRequests'
import { ArtistNameInput } from './ArtistNameInput'
import { getSpotifySearchData } from '../../../utils/spotifyDataService'
import ExternalIDContainer from './ExternalId/ExternalIdContainer'
import ExternalIDSummary from './ExternalId/ExternalIdSummary'

import { isEmpty } from '../../../../utils'
import { SERVICES } from '../../../utils/constants'

function getStepContent({ index: step, ...props }) {
  switch (step) {
    case 0:
      return <ArtistNameInput {...props} />
    case 1:
      return (
        <ExternalIDContainer serviceName={SERVICES.spotify.name} {...props} />
      )
    case 2:
      return (
        <ExternalIDContainer serviceName={SERVICES.apple.name} {...props} />
      )
    case 3:
      return <ExternalIDSummary {...props} />
    default:
      return null
  }
}

function addNewCreative(creatives, newCreative, setFieldValue) {
  setFieldValue(
    'creatives',
    produce(creatives, (draft) => {
      draft[newCreative.uuid] = newCreative
    })
  )
}

function removeCreative(key, creatives, setFieldValue) {
  const newCreatives = produce(creatives, (draft) => {
    delete draft[key]
  })
  setFieldValue('creatives', newCreatives)
}

function searchRequest(term, serviceName) {
  switch (serviceName) {
    case SERVICES.spotify.name:
      return getSpotifySearchData(term)
    case SERVICES.apple.name:
      return appleArtistSearchRequest(term)
    default:
      break
  }
}

const shouldSearch = (term, searchResults) =>
  !isEmpty(term) && (!searchResults.spotify[term] || !searchResults.apple[term])

function getCurrentName(creatives, currentArtistUUID) {
  if (!currentArtistUUID) return

  const currentCreative = creatives[currentArtistUUID]

  return currentCreative && currentCreative.name
}

// Remove new creative stub if not named by user
function pruneAbandonedEntries(creatives, setFieldValue) {
  const blankCreativeKey = Object.keys(creatives).find((c) =>
    isEmpty(creatives[c].name)
  )

  if (blankCreativeKey)
    removeCreative(blankCreativeKey, creatives, setFieldValue)
}

function creativeData(data) {
  return {
    create_page: data.create_page,
    identifier: data.identifier,
    url: data.url,
  }
}

const SERVICE_NAME_VAR = '{{serviceName}}'
const interpolateServiceName = (str, serviceName) =>
  str.replace(SERVICE_NAME_VAR, SERVICES[serviceName].titleCase)

export {
  addNewCreative,
  creativeData,
  getCurrentName,
  getStepContent,
  interpolateServiceName,
  pruneAbandonedEntries,
  removeCreative,
  searchRequest,
  shouldSearch,
}
