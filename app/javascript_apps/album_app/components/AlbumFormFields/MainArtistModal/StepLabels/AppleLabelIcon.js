import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import SvgApple from '../../../../../shared/svg/apple'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    'justify-content': 'space-between',
    width: '110%',
    'align-items': 'center',
  },
  artistIcon: {
    objectFit: 'cover',
    opacity: 1,
    height: '32px',
  },
  appleArtistContent: {
    display: 'flex',
    'justify-content': 'space-between',
    'font-size': '12px',
    color: '#5E6D7B',
    'align-items': 'center',
    'margin-top': '-4px',
    '& p': {
      'margin-bottom': '0px',
      'margin-left': '10px',
    },
  },
  fixedWidth: {
    width: '80px',
  },
}))

export default function AppleLabelIcon({ active, appleArtistID }) {
  const classes = useStyles()

  let artistContent = null

  if (appleArtistID) {
    artistContent = (
      <div className={classes.appleArtistContent}>
        <div>
          <img
            src={appleArtistID.image}
            width="auto"
            height={32}
            className={classes.artistIcon}
          />{' '}
        </div>
        <p>{appleArtistID.name}</p>
      </div>
    )
  }

  return (
    <div className={classes.root}>
      <div className={classes.fixedWidth}>
        <SvgApple fill={active ? '#000000' : '#CDD2D6'} />
      </div>
      {artistContent}
    </div>
  )
}

AppleLabelIcon.propTypes = {
  appleArtistID: PropTypes.object,
  active: PropTypes.bool,
}
