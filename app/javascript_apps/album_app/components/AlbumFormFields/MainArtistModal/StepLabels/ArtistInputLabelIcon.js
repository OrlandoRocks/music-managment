import PropTypes from 'prop-types'

import { useConfig } from '../../../../contexts/ConfigContext'

export default function ArtistInputLabelIcon({ active }) {
  const {
    translations: {
      artist_name_input: { step_label },
    },
  } = useConfig()[0]

  if (active) return null

  return step_label
}
ArtistInputLabelIcon.propTypes = { active: PropTypes.bool.isRequired }
