import React from 'react'

import ArtistInputLabelIcon from './ArtistInputLabelIcon'
import ServiceLabelIcon from './ServiceLabelIcon'
import HiddenLabelIcon from './HiddenLabelIcon'
import { SERVICES } from '../../../../utils/constants'

export default function StepIcons(props) {
  const icons = {
    1: <ArtistInputLabelIcon {...props} />,
    2: <ServiceLabelIcon {...props} serviceName={SERVICES.spotify.name} />,
    3: <ServiceLabelIcon {...props} serviceName={SERVICES.apple.name} />,
    4: <HiddenLabelIcon />,
  }

  return icons[String(props.icon)]
}
