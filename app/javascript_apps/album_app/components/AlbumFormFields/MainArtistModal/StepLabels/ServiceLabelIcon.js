import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import SvgApple from '../../../../../shared/svg/apple'
import SvgSpotify from '../../../../../shared/svg/spotify'
import { NEW_ARTIST_STATE, SERVICES } from '../../../../utils/constants'
import { useESIDData } from '../../../../contexts/ESIDDataContext'
import { interpolateServiceName } from '../mamHelpers'

import { useConfig } from '../../../../contexts/ConfigContext'

export default function ServiceLabelIcon({
  active,
  currentArtistUUID,
  serviceName,
}) {
  const {
    translations: {
      create_new_artist_id: { create_btn },
    },
  } = useConfig()[0]
  const classes = useStyles()

  const { esidData } = useESIDData()
  const esidLabelData = getESIDLabelData(
    esidData,
    currentArtistUUID,
    serviceName
  )

  let artistContent = null

  switch (true) {
    case Boolean(esidLabelData.identifier):
      artistContent = (
        <div className={classes[serviceName]}>
          <div>
            <img
              src={esidLabelData.image}
              width="auto"
              height={32}
              className={classes.artistIcon}
            />{' '}
          </div>
          <p>{esidLabelData.name}</p>
        </div>
      )
      break
    case Boolean(
      esidLabelData.create_page || esidLabelData.state === NEW_ARTIST_STATE
    ):
      artistContent = (
        <div className={classes[serviceName]}>
          <p>{interpolateServiceName(create_btn, serviceName)}</p>
        </div>
      )
      break
  }

  return (
    <div className={classes.root}>
      <div className={classes.fixedWidth}>
        <ServiceSVG active={active} serviceName={serviceName} />
      </div>
      {artistContent}
    </div>
  )
}
ServiceLabelIcon.propTypes = {
  active: PropTypes.bool.isRequired,
  currentArtistUUID: PropTypes.string.isRequired,
  serviceName: PropTypes.string.isRequired,
}

function getESIDLabelData(esidData, currentArtistUUID, serviceName) {
  if (esidData[currentArtistUUID] && esidData[currentArtistUUID][serviceName]) {
    return esidData[currentArtistUUID][serviceName]
  }
  return {}
}

/**
 * Subcomponents
 */

const ServiceSVG = React.memo(function ServiceSVG({ active, serviceName }) {
  switch (true) {
    case serviceName === SERVICES.apple.name:
      return <SvgApple fill={active ? '#000000' : '#CDD2D6'} />
    case serviceName === SERVICES.spotify.name:
      return <SvgSpotify fill={active ? '#1ED760' : '#CDD2D6'} />
    default:
      return null
  }
})

ServiceSVG.propTypes = {
  active: PropTypes.bool,
  serviceName: PropTypes.string,
}

ServiceSVG.displayName = 'ServiceSVG'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    'justify-content': 'space-between',
    width: '110%',
    'align-items': 'center',
  },
  apple: {
    display: 'flex',
    'justify-content': 'space-between',
    'font-size': '12px',
    color: '#5E6D7B',
    'align-items': 'center',
    'margin-top': '-4px',
    '& p': {
      'margin-bottom': '0px',
      'margin-left': '10px',
    },
  },
  artistIcon: {
    objectFit: 'cover',
    opacity: 1,
    height: '32px',
  },
  fixedWidth: {
    width: '80px',
  },
  spotify: {
    display: 'flex',
    'justify-content': 'space-between',
    'font-size': '12px',
    color: '#5E6D7B',
    'align-items': 'center',
    marginTop: '-4px',
    '& p': {
      'margin-bottom': '0px',
      'margin-left': '10px',
    },
  },
}))
