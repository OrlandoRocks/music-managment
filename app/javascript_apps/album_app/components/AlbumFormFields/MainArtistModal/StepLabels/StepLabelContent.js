import React from 'react'
import PropTypes from 'prop-types'

// MUI Step Labels consist of Icons and Content
// We're making use of MUI's Label icon prop handling to toggle styles on the step icons
// So our Step Labels are mostly just Icon components, except for the first step, here:
const StepLabelContent = React.memo(function StepLabelContent({
  activeStep,
  currentName,
  index,
}) {
  const isArtistInputLabel = index === 0
  const show = isArtistInputLabel && activeStep !== 0 && currentName
  return show ? `: ${currentName}` : null
})
StepLabelContent.displayName = 'StepLabelContent'
StepLabelContent.propTypes = {
  activeStep: PropTypes.number.isRequired,
  currentName: PropTypes.string,
  index: PropTypes.number.isRequired,
}

export default StepLabelContent
