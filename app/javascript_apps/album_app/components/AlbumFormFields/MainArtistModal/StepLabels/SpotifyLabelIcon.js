import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

import SvgSpotify from '../../../../../shared/svg/spotify'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    'justify-content': 'space-between',
    width: '110%',
    'align-items': 'center',
  },
  artistIcon: {
    objectFit: 'cover',
    opacity: 1,
    height: '32px',
  },
  spotifyArtistContent: {
    display: 'flex',
    'justify-content': 'space-between',
    'font-size': '12px',
    color: '#5E6D7B',
    'align-items': 'center',
    marginTop: '-4px',
    '& p': {
      'margin-bottom': '0px',
      'margin-left': '10px',
    },
  },
  fixedWidth: {
    width: '80px',
  },
}))

export default function SpotifyLabelIcon({ active, spotifyArtistID }) {
  const classes = useStyles()

  let artistContent = null

  if (spotifyArtistID) {
    artistContent = (
      <div className={classes.spotifyArtistContent}>
        <img
          src={spotifyArtistID.image}
          width="auto"
          height={32}
          className={classes.artistIcon}
        />
        <p>{spotifyArtistID.name}</p>
      </div>
    )
  }

  return (
    <div className={classes.root}>
      <div className={classes.fixedWidth}>
        <SvgSpotify fill={active ? '#1ED760' : '#CDD2D6'} />
      </div>
      {artistContent}
    </div>
  )
}

SpotifyLabelIcon.propTypes = {
  spotifyArtistID: PropTypes.object,
  active: PropTypes.bool,
}
