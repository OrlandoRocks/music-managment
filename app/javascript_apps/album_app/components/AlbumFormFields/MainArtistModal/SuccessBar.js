import React, { useEffect } from 'react'
import { Snackbar } from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import { useMAMContext } from '../../../contexts/MAMcontext'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  successSnack: {
    width: '304px',
    height: '48px',
    boxShadow: '0px 12px 19px #00000042',
    borderRadius: '4px',
    color: 'white',
    background: 'black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '10px',
    '& svg': {
      color: 'lime',
    },
    '& p': {
      marginBottom: '0px',
    },
  },
})

export default function SuccessBar() {
  const classes = useStyles()
  const {
    mamContextState: { snackBarMsgs },
    setMamContextState,
    setMamContextVals,
  } = useMAMContext()

  const [open, setOpen] = React.useState(false)
  const [messageInfo, setMessageInfo] = React.useState(undefined)

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  const handleExited = () => {
    setMessageInfo(undefined)
  }

  useEffect(() => {
    if (snackBarMsgs.length && !messageInfo) {
      setMessageInfo({ ...snackBarMsgs[0] })
      setMamContextVals({ snackBarMsgs: snackBarMsgs.slice(1) })
      setOpen(true)
    } else if (snackBarMsgs.length && messageInfo && open) {
      setOpen(false)
    }
  }, [messageInfo, open, setMamContextState, setMamContextVals, snackBarMsgs])

  const message = messageInfo ? messageInfo.message : undefined

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      autoHideDuration={4000}
      key={messageInfo ? messageInfo.key : undefined}
      message={message}
      onClose={handleClose}
      onExited={handleExited}
      open={open}
    >
      <div className={classes.successSnack}>
        <p>{message}</p>
        <CheckIcon />
      </div>
    </Snackbar>
  )
}
