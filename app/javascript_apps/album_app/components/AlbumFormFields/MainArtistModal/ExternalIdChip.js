import React from 'react'
import PropTypes from 'prop-types'

import CheckCircle from '@material-ui/icons/CheckCircle'
import Cancel from '@material-ui/icons/Cancel'
import { Chip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { NEW_ARTIST_STATE } from '../../../utils/constants'

import { useConfig } from '../../../contexts/ConfigContext'

function ExternalIdChip({ data, label }) {
  const classes = useStyles()
  const { translations } = useConfig()[0]

  const { ariaLabel, color, icon } = getChipAttributes(data)

  return (
    <Chip
      size={'small'}
      aria-label={translations[ariaLabel]}
      color={color}
      classes={{
        colorSecondary: classes.chipSkippedStyle,
        colorPrimary: classes.chipPrimaryStyle,
        icon: classes.chipIcon,
        root: classes.root,
      }}
      icon={icon}
      label={label}
    />
  )
}
ExternalIdChip.propTypes = {
  data: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
}

export default React.memo(ExternalIdChip)

function getChipAttributes(data) {
  if (notSkipped(data)) {
    return {
      ariaLabel: 'present',
      color: 'primary',
      icon: <CheckCircle />,
    }
  }

  return {
    ariaLabel: 'missing',
    color: 'default',
    icon: <Cancel />,
  }
}

function notSkipped(data) {
  return data.identifier || data.create_page || data.state === NEW_ARTIST_STATE
}

const useStyles = makeStyles({
  root: {
    padding: '0 .5em',
    'margin-right': '1em',
    '& .MuiChip-label': {
      'text-overflow': 'unset',
    },
  },
  chipPrimaryStyle: {
    color: 'green',
    backgroundColor: '#E5F8EC',
  },
  chipSkippedStyle: {
    'background-color': '#F2F3FA',
    color: '#536270',
  },
  chipIcon: {
    marginLeft: '0px',
  },
})
