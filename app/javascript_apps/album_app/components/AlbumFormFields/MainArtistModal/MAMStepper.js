import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'
import clsx from 'clsx'
import produce from 'immer'

import Step from '@material-ui/core/Step'
import StepContent from '@material-ui/core/StepContent'
import StepLabel from '@material-ui/core/StepLabel'
import Stepper from '@material-ui/core/Stepper'
import { makeStyles } from '@material-ui/core/styles'

import { isEmpty } from '../../../../utils'
import { SERVICES } from '../../../utils/constants'
import { useDebounce } from '../../../utils/useDebounce'
import StepLabelContent from './StepLabels/StepLabelContent'
import StepLabelIcons from './StepLabels/StepLabelIcons'
import SuccessBar from '../../shared/SuccessBar'

import {
  getCurrentName,
  getStepContent,
  pruneAbandonedEntries,
  searchRequest,
  shouldSearch,
} from './mamHelpers'

function MAMStepper({
  currentArtistUUID,
  setCurrentArtistUUID,
  toggleModal,
  spotifyArtistsRequired,
}) {
  const {
    values: { creatives },
    setFieldValue,
  } = useFormikContext()
  const [activeStep, setActiveStep] = useState(0)
  const [previousStep, setPreviousStep] = useState(null)
  const [searchResults, setSearchResults] = useState({ spotify: {}, apple: {} })
  const [loadingState, setLoadingState] = useState(null)

  const classes = stepperStyles()

  // The ArtistNameInput/search term
  const currentName = getCurrentName(creatives, currentArtistUUID)
  const debouncedTerm = useDebounce(currentName, 200)

  useEffect(() => {
    let isMounted = true
    async function searchByArtist() {
      if (!shouldSearch(debouncedTerm, searchResults)) return

      setLoadingState(LOADING)
      const term = debouncedTerm
      const spotifyPromise = searchRequest(term, SERVICES.spotify.name)
      const applePromise = searchRequest(term, SERVICES.apple.name)

      try {
        const [spotifyResults, appleResults] = await Promise.all([
          spotifyPromise,
          applePromise,
        ])
        const newResults = produce(searchResults, (draft) => {
          draft.spotify[term] = spotifyResults
          draft.apple[term] = appleResults
        })

        if (isMounted) {
          setSearchResults(newResults)
          setLoadingState(LOADED)
        }
      } catch (e) {
        console.log(e)
        setLoadingState(ERROR) // currently not used, remove API call catches to get here
      }
    }
    searchByArtist()

    return () => (isMounted = false)
  }, [debouncedTerm, searchResults])

  /**
   * Stepper action handlers
   */
  const handleNext = () => {
    const isLast = activeStep === STEPS.length - 1
    if (isLast) toggleModal()

    setPreviousStep(activeStep)
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    if (activeStep === 0) {
      pruneAbandonedEntries(creatives, setFieldValue)
      toggleModal()
    } else {
      setPreviousStep(activeStep)
      setActiveStep((currentActiveStep) => currentActiveStep - 1)
    }
  }

  const forceActive = (i) => {
    setPreviousStep(activeStep)

    switch (i) {
      case 0:
        setActiveStep(i)
        break
      case 1:
      case 2:
        !isEmpty(currentName) && setActiveStep(i)
        break
      default:
        break
    }
  }

  // pruneAbandonedEntries just removed the currentArtist, but this component is going to render
  // one more time before it unmounts and the modal closes
  if (!creatives[currentArtistUUID]) return null

  return (
    <div className={classes.root}>
      <Stepper
        activeStep={activeStep}
        alternativeLabel
        className={classes.stepsContainer}
        connector={null}
        orientation="vertical"
      >
        {STEPS.map((_, index) => {
          const isActive = index === activeStep

          return (
            <Step
              className={clsx(classes.step, {
                [classes.stepActive]: isActive,
              })}
              key={index}
            >
              <StepLabel
                data-testid={`mam-artist-step-row-${index}`}
                className={classes.stepLabel}
                onClick={() => forceActive(index)}
                StepIconComponent={StepLabelIcons}
                StepIconProps={{ active: isActive, currentArtistUUID }}
              >
                <StepLabelContent
                  activeStep={activeStep}
                  currentName={currentName}
                  index={index}
                />
              </StepLabel>
              <StepContent className={classes.stepContent}>
                {getStepContent({
                  activeStep,
                  currentArtistUUID,
                  currentName,
                  handleBack,
                  handleNext,
                  index,
                  loadingState,
                  previousStep,
                  searchResults,
                  setActiveStep,
                  setCurrentArtistUUID,
                  setFieldValue,
                  steps: STEPS,
                  stepperClasses: classes,
                  toggleModal,
                  spotifyArtistsRequired,
                })}
              </StepContent>
            </Step>
          )
        })}

        <SuccessBar />
      </Stepper>
    </div>
  )
}
MAMStepper.propTypes = {
  currentArtistUUID: PropTypes.string.isRequired,
  setCurrentArtistUUID: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
  spotifyArtistsRequired: PropTypes.bool,
}
export default React.memo(MAMStepper)

const ERROR = 'error'
const LOADED = 'loaded'
const LOADING = 'loading'

/**
 * Steps:
 * - Input search term
 * - Spotify
 * - Apple
 * - Summary
 */
const STEPS = Array.from(Array(4))

const stepperStyles = makeStyles(() => ({
  root: {
    borderLeft: 'none',
    height: '100%',
    margin: 0,
    padding: 0,
    width: '100%',
    '& .MuiStep-vertical:nth-child(4) > span': {
      display: 'none',
    },
  },
  step: {
    flex: '0',
    width: '100%',
  },
  stepActive: {
    borderTop: '1px solid #DBE2E9',
    flex: '1',
    overflowY: 'auto',
  },
  stepsContainer: {
    background: 'transparent',
    height: '80vh',
    padding: '0',
    '& .MuiStep-vertical:first-child': {
      borderTop: 'none',
    },
    '& .MuiStep-vertical:nth-child(2), & .MuiStep-vertical:nth-child(3)': {
      borderTop: '1px solid #DBE2E9',
    },
    // values match modal in album_modal.scss
    '@media screen and (max-width: 767px)': {
      // height of two labels
      height: '100vh',
      width: '100vw',
    },

    '@media screen and (min-width: 1280px)': {
      height: '50vh',
    },
  },
  stepContent: {
    borderLeft: 'none',
    margin: 0,
    padding: '0 10px',
  },
  stepLabel: {
    background: 'white',
    display: 'block',
    height: '48px',
    padding: '.75em',
    position: 'sticky',
    top: 0,
    zIndex: 1,
    '& > .MuiStepLabel-iconContainer': {
      display: 'inline-block',
      verticalAlign: 'middle',
    },
    '& > .MuiStepLabel-labelContainer span': {
      display: 'inline',
      fontSize: '1em',
      verticalAlign: 'bottom',
    },
  },
}))
