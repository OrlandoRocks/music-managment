import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'
import produce from 'immer'

import { makeStyles } from '@material-ui/core/styles'

import { PrimaryButton } from '../../shared/MUIButtons'
import BackButton from './BackButton'

import { useConfig } from '../../../contexts/ConfigContext'
import { useESIDData } from '../../../contexts/ESIDDataContext'

function StepActions({
  activeStep,
  currentArtistUUID,
  handleBack,
  handleNext,
  serviceName,
  hideSkipButton,
}) {
  const classes = styles()

  return (
    <div className={classes.actionsContainer}>
      <BackButton onClick={handleBack} />
      {!hideSkipButton && (
        <SkipButton
          activeStep={activeStep}
          currentArtistUUID={currentArtistUUID}
          handleNext={handleNext}
          serviceName={serviceName}
        />
      )}
    </div>
  )
}

StepActions.propTypes = {
  activeStep: PropTypes.number.isRequired,
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
  serviceName: PropTypes.string,
  currentArtistUUID: PropTypes.string,
  hideSkipButton: PropTypes.bool,
}

StepActions.defaultProps = {
  hideSkipButton: false,
}

export default React.memo(StepActions)

const styles = makeStyles(() => ({
  actionsContainer: {
    padding: '1em 0',
    '& > button': {
      margin: '0 1em 0 0',
    },
  },
}))

function SkipButton({
  activeStep,
  currentArtistUUID,
  handleNext,
  serviceName,
}) {
  const { setESIDData } = useESIDData()
  const {
    setFieldValue,
    values: { creatives },
  } = useFormikContext()
  const {
    translations: { save_selections, skip },
  } = useConfig()[0]

  if (activeStep === 0) return null

  const isLast = activeStep === 3
  const buttonText = isLast ? save_selections : skip
  const testIdText = isLast ? 'mam-save-artist' : 'mam-skip'

  function handleClick() {
    // Clear previous values
    if (currentArtistUUID && serviceName) {
      const newCreatives = produce(creatives, (draft) => {
        draft[currentArtistUUID][serviceName] = {}
      })
      setFieldValue('creatives', newCreatives)

      setESIDData((prev) =>
        produce(prev, (draft) => {
          draft[currentArtistUUID] = draft[currentArtistUUID] || {}
          draft[currentArtistUUID][serviceName] = {}
        })
      )
    } else {
      // The button is used elsewhere and we don't have to clear values in those cases.
    }

    handleNext()
  }

  return (
    <PrimaryButton data-testid={`${testIdText}-button`} onClick={handleClick}>
      {buttonText}
    </PrimaryButton>
  )
}

SkipButton.propTypes = {
  activeStep: PropTypes.number.isRequired,
  handleNext: PropTypes.func.isRequired,
  serviceName: PropTypes.string,
  currentArtistUUID: PropTypes.string,
}
