import React from 'react'
import PropTypes from 'prop-types'

import { IconButton } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

import { useConfig } from '../../../contexts/ConfigContext'

const styles = {
  button: {
    paddingLeft: 0,
  },
}

function BackButton({ onClick }) {
  const {
    translations: { back_button },
  } = useConfig()[0]

  return (
    <IconButton
      data-testid="mam-back-button"
      aria-label={back_button}
      onClick={onClick}
      primary="#5E6D7B"
      style={styles.button}
    >
      <ArrowBackIcon />
    </IconButton>
  )
}
BackButton.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default React.memo(BackButton)
