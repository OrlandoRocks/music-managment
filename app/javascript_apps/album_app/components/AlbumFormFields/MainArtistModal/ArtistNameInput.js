import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'
import produce from 'immer'

import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { makeStyles } from '@material-ui/core/styles'

import { PrimaryButton } from '../../shared/MUIButtons'
import StepActions from './StepActions'

import { useConfig } from '../../../contexts/ConfigContext'
import { useESIDData } from '../../../contexts/ESIDDataContext'

export const ArtistNameInput = React.memo(function ArtistNameInput({
  activeStep,
  currentArtistUUID,
  handleBack,
  handleNext,
}) {
  const {
    setFieldValue,
    setTouched,
    values: { creatives },
  } = useFormikContext()
  const { possibleArtists, translations } = useConfig()[0]
  const {
    artist_name_input: { header, placeholder },
    artist_errors: { length },
    continue_button,
  } = translations
  const MAX_LENGTH = 120

  const { setESIDData } = useESIDData()

  const currentValue = creatives[currentArtistUUID].name

  const [inputValue, setInputValue] = React.useState('')

  const classes = useStyles()

  function handleChange(_, value) {
    setTouched({ creatives: true })
    if (creatives[currentArtistUUID].name !== value) {
      const newCreatives = produce(creatives, (draft) => {
        const artist = draft[currentArtistUUID]
        artist.apple = {}
        artist.spotify = {}
        artist.name = value
      })
      setFieldValue('creatives', newCreatives)

      setESIDData((prev) =>
        produce(prev, (draft) => {
          draft[currentArtistUUID] = {}
        })
      )
    }
  }

  return (
    <>
      <h3 className={classes.h3}>{header}</h3>
      <div className={classes.inputContainer}>
        <Autocomplete
          id="artist-name-input"
          autoSelect
          blurOnSelect
          className={classes.combobox}
          disableClearable={!inputValue}
          forcePopupIcon
          freeSolo
          getOptionLabel={(option) => option}
          includeInputInList
          inputValue={inputValue}
          onChange={handleChange}
          onInputChange={(_, newInputValue) => {
            setInputValue(newInputValue)
          }}
          options={possibleArtists.sort()}
          renderInput={(params) => (
            <TextField
              {...params}
              label={placeholder}
              variant="outlined"
              helperText={inputValue.length >= MAX_LENGTH ? length : ''}
              inputProps={{
                ...params.inputProps,
                maxLength: MAX_LENGTH,
                'data-testid': 'mam-artist-name-input',
              }}
            />
          )}
          value={currentValue}
        />
      </div>
      <div className={classes.continueButton}>
        <PrimaryButton
          disabled={!inputValue}
          onClick={handleNext}
          data-testid={'mam-continue-button'}
        >
          {continue_button}
        </PrimaryButton>
      </div>
      <StepActions
        activeStep={activeStep}
        handleBack={handleBack}
        handleNext={handleNext}
      />
    </>
  )
})
ArtistNameInput.displayName = 'ArtistNameInput'
ArtistNameInput.propTypes = {
  activeStep: PropTypes.number.isRequired,
  currentArtistUUID: PropTypes.string.isRequired,
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
  stepperClasses: PropTypes.object.isRequired,
  steps: PropTypes.array.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

const useStyles = makeStyles(() => ({
  combobox: {
    width: '100%',
  },
  continueButton: {
    display: 'flex',
    justifyContent: 'center',
  },
  formLabel: {
    margin: '.5em 0 1em',
  },
  h3: {
    fontSize: '2em',
  },
  input: {
    marginBottom: '1em',
    maxHeight: '48px',
    width: '100%',
    '& > input': {
      background: '#F1F2F6',
      borderRadius: '4px 4px 0px 0px',
      height: '2em',
      padding: '.5em',
      width: '100%',
    },
  },
  inputButton: {
    alignSelf: 'baseline',
  },
  inputContainer: {
    display: 'flex',
    padding: '1em 0',
    '& #artist-name-input': {
      border: 'none',
      boxShadow: 'none',
    },
  },
  list: {
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    maxHeight: '350px',
    overflowY: 'auto',
    margin: '-1em 0',
    borderTop: 0,
    borderRadius: '0 0 4px 4px',
    background: 'white',
    padding: 0,
    position: 'absolute',
    width: 'calc(100% - (5em + 48px))',
    zIndex: 1000,
    '& > li': {
      cursor: 'pointer',
      padding: '0 1em',
      '&:hover': {
        background: '#F1F2F6',
      },
    },
  },
  rightIcon: {
    height: '48px',
    padding: '0 .5em',
  },
}))
