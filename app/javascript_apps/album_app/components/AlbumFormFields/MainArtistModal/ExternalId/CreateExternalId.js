import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'
import produce from 'immer'

import { makeStyles } from '@material-ui/core/styles'

import { useConfig } from '../../../../contexts/ConfigContext'
import { useAddSnackbar } from '../../../../contexts/SnackbarContext'
import { isSubPage, SUB_PAGES } from './ExternalIdContainer'
import { SecondaryButton } from '../../../shared/MUIButtons'
import { useESIDData } from '../../../../contexts/ESIDDataContext'
import { interpolateServiceName } from '../mamHelpers'
import BackButton from '../BackButton'

function CreateExternalId({
  currentArtistUUID,
  handleNext,
  serviceName,
  setSubPage,
  subPage,
}) {
  const {
    setFieldValue,
    values: { creatives },
  } = useFormikContext()
  const {
    artist_modal_summary,
    create_new_artist_id: { create_btn, headline, subtext },
  } = useConfig()[0].translations

  const { setESIDData } = useESIDData()

  const addSnackbar = useAddSnackbar()
  const snackBarMessage = artist_modal_summary[serviceName].will_create_page

  function createNewPage() {
    const newCreatives = produce(creatives, (draft) => {
      draft[currentArtistUUID][serviceName] = { create_page: true }
    })
    setFieldValue('creatives', newCreatives)

    setESIDData((prev) =>
      produce(prev, (draft) => {
        draft[currentArtistUUID] = draft[currentArtistUUID] || {}
        draft[currentArtistUUID][serviceName] = { create_page: true }
      })
    )

    addSnackbar({ message: snackBarMessage, key: new Date().getTime() })
    handleNext()
  }

  const classes = useStyles()

  if (!isSubPage(subPage, SUB_PAGES.create)) return null

  const parsedHeadline = interpolateServiceName(headline, serviceName)
  const parsedCreateButton = interpolateServiceName(create_btn, serviceName)

  const goToSearchResults = () => {
    setSubPage(SUB_PAGES.search)
  }

  return (
    <div>
      <h1 className={classes.header}>{parsedHeadline}</h1>

      <p>{subtext}</p>

      <SecondaryButton onClick={createNewPage}>
        {parsedCreateButton}
      </SecondaryButton>

      <div className={classes.actionBtns}>
        <BackButton onClick={goToSearchResults} />
      </div>
    </div>
  )
}
export default React.memo(CreateExternalId)

CreateExternalId.propTypes = {
  currentArtistUUID: PropTypes.string.isRequired,
  handleNext: PropTypes.func.isRequired,
  serviceName: PropTypes.string.isRequired,
  setSubPage: PropTypes.func.isRequired,
  subPage: PropTypes.string.isRequired,
}

const useStyles = makeStyles({
  createArtistBtn: {
    background: '#E8F5FF',
    borderRadius: '4px',
    color: '#3FA8EF',
    cursor: 'pointer',
    fontSize: '14px',
    margin: '10px 10px 10px 0',
    padding: '1em',
    '&:hover': {
      background: '#dae6f0',
    },
  },
  actionBtns: {
    margin: '10px 10px 10px 0px',
  },
  header: {
    fontSize: '32px',
  },
})
