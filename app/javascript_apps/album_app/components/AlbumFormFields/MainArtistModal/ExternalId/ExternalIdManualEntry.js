import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useFormikContext } from 'formik'
import produce from 'immer'
import PropTypes from 'prop-types'

import { InputAdornment, TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import AssignmentIcon from '@material-ui/icons/Assignment'

import { SERVICES } from '../../../../utils/constants'
import { isSubPage, SUB_PAGES } from './ExternalIdContainer'
import { useConfig } from '../../../../contexts/ConfigContext'
import { useDebounce } from '../../../../utils/useDebounce'
import { useAddSnackbar } from '../../../../contexts/SnackbarContext'
import { creativeData } from '../mamHelpers'
import BackButton from '../BackButton'

import Button from '../../../shared/Button'
import Spinner from '../../../shared/Spinner'

import { appleFetchArtistRequest } from '../../../../utils/appleAPIRequests'
import { getSpotifyArtistData } from '../../../../utils/spotifyDataService'
import { parseArtistData } from '../../../../utils/ExternalServiceIDs/helpers'
import { useESIDData } from '../../../../contexts/ESIDDataContext'
import useClipboard, {
  hasPermission,
  tryReadFromClipboard,
} from '../../../../utils/useClipboard'
import useMountState from '../../../../utils/useMountState'

/**
 *
 * Manually enter a service page URL
 * If valid, verify with API
 * If verified, set external service ID ESID on the creative
 * Prevent Continue unless verified
 */
export default function ExternalIdManualEntry({
  currentArtistUUID,
  handleNext,
  serviceName,
  setSubPage,
  subPage,
}) {
  const addSnackbar = useAddSnackbar()
  const {
    values: { creatives },
    ...formik
  } = useFormikContext()
  const { setESIDData } = useESIDData()

  const artist = creatives[currentArtistUUID]
  const artistID = artist[serviceName]

  const [showPasteBtn, setShowPasteBtn] = useState(false)
  const isMounted = useRef(true)

  const [isLoading, setIsLoading] = useState(false)
  const [isVerified, setIsVerified] = useState(false)
  const [error, setError] = useState('')
  const [fetchedID, setFetchedID] = useState()
  const [isUnchanged, setIsUnchanged] = useState(false)
  const [currentURL, setCurrentURL] = useState(artist[serviceName].url)

  // Don't init to error state
  const [isValidURL, setIsValidURL] = useState(
    validateUrl(currentURL, serviceName) || null
  )

  const classes = useStyles()

  const { translations } = useConfig()[0]
  const translationsKey = `${serviceName}_manual_id`
  const {
    manual_id: { error_messages, paste, url_label },
    snackbars: { url_saved },
  } = translations
  const { headline, placeholder, subtext } = translations[translationsKey]

  const inputEl = useRef(null)
  const headlineRef = useRef()

  /**
   * Helpers
   */

  const setNewCreatives = useCallback(
    (newCreatives) => {
      formik.setFieldValue('creatives', newCreatives)
    },
    [formik]
  )

  const commit = useCallback(() => {
    const newCreatives = produce(creatives, (draft) => {
      const artist = draft[currentArtistUUID]
      artist[serviceName] = { ...creativeData(fetchedID), url: currentURL }
    })
    setNewCreatives(newCreatives)

    setESIDData((prev) =>
      produce(prev, (draft) => {
        draft[currentArtistUUID] = draft[currentArtistUUID] || {}
        draft[currentArtistUUID][serviceName] = fetchedID
      })
    )

    addSnackbar({ message: url_saved, key: new Date().getTime() })
  }, [
    addSnackbar,
    creatives,
    currentArtistUUID,
    currentURL,
    fetchedID,
    serviceName,
    setESIDData,
    setNewCreatives,
    url_saved,
  ])

  const setErrorMessage = useCallback(
    (error) => {
      if (error.status < 500) {
        setError(error_messages.id)
      } else {
        setError(error_messages.fetch)
      }
    },
    [error_messages]
  )

  const parseFetchedID = useCallback(
    (response) => {
      setFetchedID(parseArtistData(response, serviceName))
    },
    [serviceName, setFetchedID]
  )

  const handleResponse = useCallback(
    (response) => {
      if (!response.error) {
        setError(null)
        setIsVerified(true)
        parseFetchedID(response)
        return
      }
      setErrorMessage(response.error)
      setIsVerified(false)
    },
    [parseFetchedID, setErrorMessage]
  )

  const fetchArtistID = useCallback(
    async (url) => {
      const id = parseURLForID(url, serviceName)

      // Prevent re-fetch
      if (id === artistID.identifier) {
        setIsVerified(true)
        setIsUnchanged(true)
        return
      }

      setIsLoading(true)
      let response

      try {
        if (serviceName === SERVICES.apple.name) {
          response = await appleFetchArtistRequest(id, artist.name)
        }
        if (serviceName === SERVICES.spotify.name) {
          response = await getSpotifyArtistData(id, artist.name)
        }
      } catch (_) {
        response = { error: { status: 500 } }
      }

      setIsLoading(false)
      handleResponse(response)
    },
    [artistID, artist.name, handleResponse, serviceName]
  )

  /**
   * Effects
   */

  useMountState(isMounted)

  const useClipboardCallback = (permissions) =>
    isMounted.current && setShowPasteBtn(hasPermission(permissions))
  useClipboard(useClipboardCallback)

  // Eagerly fetch artist data, don't wait for blur
  const debouncedURL = useDebounce(currentURL, 200)
  useEffect(() => {
    if (!debouncedURL) return

    validateUrl(debouncedURL, serviceName) && fetchArtistID(debouncedURL)
  }, [debouncedURL, fetchArtistID, isValidURL, serviceName])

  /**
   * Handlers
   */

  const handleBackBtn = useCallback(() => {
    setSubPage(SUB_PAGES.search)
    setError(null)
  }, [setSubPage])

  // Validate URL, purge bad value from form if invalid
  const handleBlur = useCallback(
    (e) => {
      const { value: url } = e.target
      const isValid = validateUrl(url, serviceName)
      const isPresent = Boolean(url.trim())

      if (isPresent) {
        setIsValidURL(isValid)

        if (!isValid) {
          setError(error_messages.link)
        }
      }
    },
    [error_messages, serviceName]
  )

  const handleContinue = useCallback(() => {
    if (!isUnchanged && isValidURL && isVerified) {
      commit()
    }

    handleNext()
  }, [commit, handleNext, isUnchanged, isValidURL, isVerified])

  // Eagerly show continue button, but supress errors until blur
  const handleInput = useCallback(
    (e) => {
      const { value: url } = e.target
      if (error) setError(null)
      setCurrentURL(url)
      setIsValidURL(validateUrl(url, serviceName) || null)
    },
    [error, serviceName, setIsValidURL]
  )

  const handlePaste = useCallback(async () => {
    const clipboardContent = await tryReadFromClipboard()
    inputEl.current.value = clipboardContent
    triggerBlur(inputEl)
  }, [inputEl])

  /**
   * Render
   */
  if (!isSubPage(subPage, SUB_PAGES.manual)) return null

  return (
    <>
      <h1 className={classes.headline} ref={headlineRef}>
        {headline}
      </h1>
      <p>{subtext}</p>

      <TextField
        autoFocus
        className={classes.artistPageInput}
        defaultValue={currentURL}
        error={isValidURL === false}
        fullWidth
        id="artist-page-input"
        // lower-case = input field
        inputProps={{
          'aria-label': url_label,
        }}
        // upper-case = input component
        InputProps={{
          onKeyDown: (e) => {
            const { key, target } = e
            if (key === 'Escape' || key === 'Enter') {
              target.blur()
            }
            key === 'Escape' && e.stopPropagation()
          },
          startAdornment: showPasteBtn && (
            <InputAdornment
              aria-hidden={false}
              aria-label={paste}
              className={classes.pasteButton}
              id="artist-page-input-start-adornment"
              onClick={handlePaste}
              position="start"
            >
              <AssignmentIcon />
            </InputAdornment>
          ),
        }}
        inputRef={inputEl}
        onBlur={handleBlur}
        onChange={handleInput}
        placeholder={placeholder}
        variant="outlined"
      />

      <ManualEntryError classes={classes} errorMessage={error} />
      <Spinner show={isLoading} size="medium" />
      <ManualEntryContinueButton
        classes={classes}
        handleContinue={handleContinue}
        isValidURL={isValidURL}
        isVerified={isVerified}
      />
      <BackButton onClick={handleBackBtn} />
    </>
  )
}
ExternalIdManualEntry.propTypes = {
  currentArtistUUID: PropTypes.string.isRequired,
  handleNext: PropTypes.func.isRequired,
  serviceName: PropTypes.string.isRequired,
  setSubPage: PropTypes.func.isRequired,
  subPage: PropTypes.string.isRequired,
}

const useStyles = makeStyles({
  artistPageInput: {
    '& #artist-page-input': {
      border: 'none',
      boxShadow: 'none',
      '&:focus': {
        border: 'none',
        boxShadow: 'none',
      },
    },
    '& #artist-page-input-end-adornment': {
      color: 'green',
    },
    '& #artist-page-input-start-adornment': {
      color: '#5e6d7b',
    },
  },
  backBtn: {
    margin: '1em 8px 8px 0px',
  },
  continueButtonContainer: {
    padding: '1em',
  },
  errorMsg: {
    color: 'red',
    fontWeight: 'bold',
  },
  headline: {
    fontSize: '32px',
  },
  pasteButton: { cursor: 'pointer' },
})

/**
 * Helpers
 */

// https://itunes.apple.com/us/artist/madonna/id12345
const APPLE_URL_REGEX = /^https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.+\/(\w|\d){3,}/
// https://open.spotify.com/artist/2eam0iDomRHGBypaDQLwWI
const SPOTIFY_URL_REGEX = /^https:\/\/open.spotify.com\/artist\/\w+(?:\??|(?:[?]\w+=\w+)(?:&\w+=\w+)*)*$|^spotify:artist:\w+$/

function validateUrl(url, serviceName) {
  if (serviceName === SERVICES.apple.name) {
    return APPLE_URL_REGEX.test(url)
  } else if (serviceName === SERVICES.spotify.name) {
    return SPOTIFY_URL_REGEX.test(url)
  }
}

// For validations on blur
function triggerBlur(inputEl) {
  inputEl.current.focus()
  inputEl.current.blur()
}

const SPOTIFY_URI_PREFIX = 'spotify:artist:'

function parseURLForID(url, serviceName) {
  let parts = url.split('/')
  const isSpotifyURI = parts.length === 1

  if (isSpotifyURI) {
    parts = url.split(SPOTIFY_URI_PREFIX)
  }

  let result = parts[parts.length - 1]
  if (serviceName === SERVICES.apple.name) {
    result = result.replace(/^id/, '')
  }

  return result
}

/**
 * Sub-components
 */

const ManualEntryError = React.memo(function ManualEntryError({
  classes,
  errorMessage,
}) {
  if (!errorMessage) return null

  return <p className={classes.errorMsg}>{errorMessage}</p>
})
ManualEntryError.displayName = 'ManualEntryError'

ManualEntryError.propTypes = {
  classes: PropTypes.object,
  errorMessage: PropTypes.string,
}

const ManualEntryContinueButton = React.memo(
  function ManualEntryContinueButton({
    classes,
    handleContinue,
    isValidURL,
    isVerified,
  }) {
    if (!isValidURL || !isVerified) return null

    return (
      <div className={classes.continueButtonContainer}>
        <Button additionalStyleKey="centered" onClick={handleContinue}>
          Continue
        </Button>
      </div>
    )
  }
)
ManualEntryContinueButton.displayName = 'ManualEntryContinueButton'

ManualEntryContinueButton.propTypes = {
  classes: PropTypes.object,
  isVerified: PropTypes.bool,
  isValidURL: PropTypes.bool,
  handleContinue: PropTypes.func,
}
