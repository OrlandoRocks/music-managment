import React, { useEffect, useMemo, useState } from 'react'
import { useFormikContext } from 'formik'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'
import Fade from '@material-ui/core/Fade'

import ExternalIdManualEntry from './ExternalIdManualEntry'
import CreateExternalId from './CreateExternalId'
import ExternalIdSearchResultsPage from './ExternalIdSearchResultsPage'

export default function ExternalIDContainer(props) {
  const {
    activeStep,
    currentArtistUUID,
    currentName,
    previousStep,
    searchResults,
    serviceName,
  } = props
  const {
    values: { creatives },
  } = useFormikContext()
  const classes = useStyles()
  const serviceSearchResults = searchResults[serviceName][currentName]

  const [subPage, setSubPage] = useState(SUB_PAGES.search)
  const [pageOverride, setPageOverride] = useState(false)

  const containerProps = useMemo(
    () => ({
      classes,
      serviceSearchResults,
      setSubPage,
      subPage,
    }),
    [classes, serviceSearchResults, setSubPage, subPage]
  )

  /*
    User travels backwards through their actions, and forwards through the default flow
    EXAMPLE
    --default flow forwards--
    name input, continue => spotify results, manual input, continue => apple results
    --backwards through actions--
    back => manual input (*not spotify results*), back => spotify results, back => name input
    --default flow forwards again--
    continue => spotify results (*not manual input*)
  */
  useEffect(() => {
    if (previousStep < activeStep) return

    const artist = creatives[currentArtistUUID]

    // Chose manual entry
    if (artist[serviceName].url && !pageOverride) {
      setSubPage(SUB_PAGES.manual)
      setPageOverride(true)
    }

    // Chose 'create page'
    if (artist[serviceName].create_page && !pageOverride) {
      setSubPage(SUB_PAGES.create)
      setPageOverride(true)
    }
  }, [
    activeStep,
    creatives,
    currentArtistUUID,
    pageOverride,
    previousStep,
    serviceName,
  ])

  return (
    <div>
      <Fade in={isSubPage(subPage, SUB_PAGES.manual)} timeout={500}>
        <div>
          <ExternalIdManualEntry {...containerProps} {...props} />
        </div>
      </Fade>
      <Fade in={isSubPage(subPage, SUB_PAGES.create)} timeout={500}>
        <div>
          <CreateExternalId {...containerProps} {...props} />
        </div>
      </Fade>
      <Fade in={isSubPage(subPage, SUB_PAGES.search)} timeout={500}>
        <div>
          <ExternalIdSearchResultsPage {...containerProps} {...props} />
        </div>
      </Fade>
    </div>
  )
}
ExternalIDContainer.propTypes = {
  activeStep: PropTypes.number.isRequired,
  currentArtistUUID: PropTypes.string.isRequired,
  currentName: PropTypes.string,
  previousStep: PropTypes.number,
  searchResults: PropTypes.shape({
    apple: PropTypes.object,
    spotify: PropTypes.object,
  }),
  serviceName: PropTypes.string.isRequired,
}

export const SUB_PAGES = {
  create: 'create',
  manual: 'manual',
  search: 'search',
}

export const isSubPage = (subPage, query) => {
  return subPage === SUB_PAGES[query]
}

const useStyles = makeStyles({
  searchResultContainer: {
    margin: '0 -10px 1em 0',
    padding: '0 10px 0 0',
  },
  searchResultRows: {
    overflow: 'hidden',
  },
  searchResult: {
    display: 'flex',
    margin: '15px 2px',
    alignItems: 'center',
    boxShadow: '0px 1px 3px #2A2D3966;',
    borderRadius: '4px',
    '&:hover': {
      background: 'rgba(211,211,211,.3)',
      cursor: 'pointer',
      transition: 'all 0.2s ease-in-out',
    },
  },
  searchResultImg: {
    marginRight: '15px',
    borderRadius: '4px',
    margin: '5px',
  },
  searchResultCopy: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '80%',
    '& p': {
      'margin-bottom': '0px',
    },
  },
  searchResultSmallText: {
    fontSize: '14px',
    color: '#5E6D7B',
  },
  altBtnsContainer: {
    display: 'flex',
  },
})
