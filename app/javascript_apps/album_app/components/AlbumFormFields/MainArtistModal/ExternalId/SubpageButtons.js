import React from 'react'
import PropTypes from 'prop-types'

import { SUB_PAGES } from './ExternalIdContainer'
import { SecondaryButton } from '../../../shared/MUIButtons'
import { interpolateServiceName } from '../mamHelpers'

const styles = {
  manual: {
    margin: '0 1em 0 0',
  },
  create: {
    margin: 0,
  },
}
function SubpageButtons({
  classes,
  create_page_button,
  manual_button,
  serviceName,
  setSubPage,
}) {
  return (
    <div className={classes.altBtnsContainer}>
      <SecondaryButton
        customStyle={styles.manual}
        dataTestId={`${serviceName}-provide-link-btn`}
        onClick={() => setSubPage(SUB_PAGES.manual)}
      >
        {interpolateServiceName(manual_button, serviceName)}
      </SecondaryButton>
      <SecondaryButton
        customStyle={styles.create}
        dataTestId={`${serviceName}-create-page-btn`}
        onClick={() => setSubPage(SUB_PAGES.create)}
      >
        {interpolateServiceName(create_page_button, serviceName)}
      </SecondaryButton>
    </div>
  )
}
SubpageButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  create_page_button: PropTypes.string.isRequired,
  manual_button: PropTypes.string.isRequired,
  serviceName: PropTypes.string.isRequired,
  setSubPage: PropTypes.func.isRequired,
}

export default React.memo(SubpageButtons)
