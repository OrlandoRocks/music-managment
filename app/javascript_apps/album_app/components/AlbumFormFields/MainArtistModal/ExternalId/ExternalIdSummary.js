import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'

import StepActions from '../StepActions'
import { SERVICES } from '../../../../utils/constants'

import { useConfig } from '../../../../contexts/ConfigContext'

// TODO: Test once product finalizes criteria
function ExternalIDSummary(props) {
  const {
    translations: { artist_modal_summary },
  } = useConfig()[0]
  const {
    values: { creatives },
  } = useFormikContext()

  const { currentArtistUUID } = props
  const artist = creatives[currentArtistUUID]
  const spotifyResult = getResult(
    artist,
    SERVICES.spotify.name,
    artist_modal_summary
  )
  const appleResult = getResult(
    artist,
    SERVICES.apple.name,
    artist_modal_summary
  )

  return (
    <>
      <h2 style={styles.headline}>{artist_modal_summary.headline}</h2>
      <p>{`${SERVICES.spotify.titleCase}: ${spotifyResult}`}</p>
      <p>{`${SERVICES.apple.titleCase}: ${appleResult}`}</p>
      <StepActions {...props} />
    </>
  )
}

ExternalIDSummary.propTypes = {
  currentArtistUUID: PropTypes.string,
}

export default React.memo(ExternalIDSummary)

const styles = {
  headline: {
    fontWeight: 700,
  },
}

function getResult(artist, serviceName, translations) {
  const data = artist[serviceName]

  switch (true) {
    case Boolean(data.url):
      return translations.will_save_url
    case Boolean(data.create_page):
      return translations[serviceName].will_create_page
    case Boolean(data.identifier):
      return translations.will_save_selection
    default:
      return translations.step_skipped
  }
}
