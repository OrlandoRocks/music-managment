import React, { useCallback, useLayoutEffect, useRef } from 'react'
import { useFormikContext } from 'formik'
import produce from 'immer'
import PropTypes from 'prop-types'

import { useAddSnackbar } from '../../../../contexts/SnackbarContext'
import { useConfig } from '../../../../contexts/ConfigContext'
import { SERVICES } from '../../../../utils/constants'
import Spinner from '../../../shared/Spinner'
import SubpageButtons from './SubpageButtons'
import StepActions from '../StepActions'
import { isSubPage, SUB_PAGES } from './ExternalIdContainer'
import { parseArtistData } from '../../../../utils/ExternalServiceIDs/helpers'
import { creativeData } from '../mamHelpers'
import { useESIDData } from '../../../../contexts/ESIDDataContext'

/**
 * Non-docker development: Copy Spotify secrets to your .env.development
 */
export const ExternalIdSearchResultsPage = React.memo(
  function ExternalIdSearchResultsPage({
    activeStep,
    classes,
    currentArtistUUID,
    handleBack,
    handleNext,
    loadingState,
    serviceName,
    serviceSearchResults,
    setSubPage,
    subPage,
    spotifyArtistsRequired,
  }) {
    const {
      setFieldValue,
      values: { creatives },
    } = useFormikContext()
    const { setESIDData } = useESIDData()
    const addSnackbar = useAddSnackbar()
    const {
      translations: {
        artist_search: { artist_missing, latest_album, top_album },
        create_new_artist_id: { create_btn },
        manual_id: { button_link: manual_button },
        snackbars: { artist_selected },
      },
    } = useConfig()[0]

    const setArtist = useCallback(
      (artistData) => {
        const newCreatives = produce(creatives, (draft) => {
          const artist = draft[currentArtistUUID]
          artist[serviceName] = creativeData(artistData)
        })
        setFieldValue('creatives', newCreatives)

        setESIDData((prev) =>
          produce(prev, (draft) => {
            draft[currentArtistUUID] = draft[currentArtistUUID] || {}
            draft[currentArtistUUID][serviceName] = artistData
          })
        )

        addSnackbar({ message: artist_selected, key: new Date().getTime() })
        handleNext()
      },
      [
        addSnackbar,
        artist_selected,
        creatives,
        currentArtistUUID,
        handleNext,
        serviceName,
        setESIDData,
        setFieldValue,
      ]
    )

    const getSearchResultText = useCallback(
      (artistData) => {
        switch (serviceName) {
          case SERVICES.apple.name:
            return `${top_album}: ${artistData.topAlbum}`
          case SERVICES.spotify.name:
            return `${latest_album}: ${artistData.latest_album}`
          default:
            return null
        }
      },
      [latest_album, serviceName, top_album]
    )

    const topOfResultsRef = useRef()

    // hax: Scroll to top of results once they're rendered
    useLayoutEffect(() => {
      const scrollTimeout = setTimeout(() => {
        topOfResultsRef.current && topOfResultsRef.current.scrollIntoView(false)
      }, 200)

      return function () {
        clearTimeout(scrollTimeout)
      }
    }, [])

    /**
     * Render
     */

    if (!isSubPage(subPage, SUB_PAGES.search)) return null

    if (loadingState === 'loading') {
      return <Spinner size="medium" />
    }

    const hideSkipButton =
      spotifyArtistsRequired && serviceName == SERVICES.spotify.name

    if (!serviceSearchResults || serviceSearchResults.length === 0) {
      return (
        <>
          <p>No results found.</p>

          <SubpageButtons
            classes={classes}
            create_page_button={create_btn}
            manual_button={manual_button}
            serviceName={serviceName}
            setSubPage={setSubPage}
          />

          <StepActions
            activeStep={activeStep}
            currentArtistUUID={currentArtistUUID}
            handleBack={handleBack}
            handleNext={handleNext}
            serviceName={serviceName}
            hideSkipButton={hideSkipButton}
          />
        </>
      )
    }

    return (
      <>
        <div ref={topOfResultsRef} />
        <div className={classes.searchResultContainer}>
          <div className={classes.searchResultRows}>
            {serviceSearchResults.map((result, index) => {
              const artistData = parseArtistData(result, serviceName)

              const { identifier, image, name } = artistData
              const searchResultText = getSearchResultText(artistData)

              return (
                <div
                  key={`${identifier}-${name}`}
                  className={classes.searchResult}
                  data-testid={`${serviceName}-search-result-${index + 1}`}
                  onClick={() => setArtist(artistData)}
                  role="button"
                >
                  <img
                    src={getImgSrc(image)}
                    width={64}
                    height={64}
                    className={classes.searchResultImg}
                  />
                  <div className={classes.searchResultCopy}>
                    <p>{name}</p>
                    <p className={classes.searchResultSmallText}>
                      {searchResultText}
                    </p>
                  </div>
                </div>
              )
            })}
          </div>
        </div>

        <p>{artist_missing}</p>

        <SubpageButtons
          classes={classes}
          create_page_button={create_btn}
          manual_button={manual_button}
          serviceName={serviceName}
          setSubPage={setSubPage}
        />

        <StepActions
          activeStep={activeStep}
          currentArtistUUID={currentArtistUUID}
          handleBack={handleBack}
          handleNext={handleNext}
          serviceName={serviceName}
          hideSkipButton={hideSkipButton}
        />
      </>
    )
  }
)
ExternalIdSearchResultsPage.displayName = 'ExternalIdSearchResultsPage'
ExternalIdSearchResultsPage.propTypes = {
  activeStep: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
  currentArtistUUID: PropTypes.string.isRequired,
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
  loadingState: PropTypes.string.isRequired,
  serviceName: PropTypes.string.isRequired,
  serviceSearchResults: PropTypes.array,
  setSubPage: PropTypes.func.isRequired,
  steps: PropTypes.array.isRequired,
  stepperClasses: PropTypes.object.isRequired,
  subPage: PropTypes.string.isRequired,
  toggleModal: PropTypes.func.isRequired,
  spotifyArtistsRequired: PropTypes.bool,
}

export default ExternalIdSearchResultsPage

const FALLBACK_IMAGE = '/images/javascript_apps/avatar_default.png'

function getImgSrc(image) {
  return image || FALLBACK_IMAGE
}
