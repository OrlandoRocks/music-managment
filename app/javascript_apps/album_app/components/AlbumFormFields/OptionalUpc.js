import React from 'react'
import PropTypes from 'prop-types'

import { useConfig } from '../../contexts/ConfigContext'

const OptionalUpc = ({ defaultValue, handleChange, immutable, name }) => {
  const { translations } = useConfig()[0]

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <label className="cell medium-2 optional" htmlFor={name}>
          {translations.upc_ean_code}
          <br />
          <em>{translations.optional}</em>
        </label>
        <input
          className="cell medium-6 input-upc"
          defaultValue={defaultValue}
          disabled={immutable}
          id={name}
          name={name}
          onChange={handleChange}
          type="text"
        />
        <span className="cell medium-4"></span>
      </div>
      {immutable || (
        <div className="grid-x div-album-form-field">
          <span className="cell medium-2"></span>
          <div className="cell medium-6 note">
            <em>{translations.upc_note}</em>
          </div>
        </div>
      )}
    </div>
  )
}

OptionalUpc.propTypes = {
  defaultValue: PropTypes.string,
  name: PropTypes.string,
  immutable: PropTypes.bool,
  handleChange: PropTypes.func,
}

export default React.memo(OptionalUpc)
