import React, { useCallback, useMemo, useState } from 'react'
import PropTypes from 'prop-types'

import { INDIA_COUNTRY_WEBSITE } from '../../utils/constants'
import { useConfig } from '../../contexts/ConfigContext'
import ErrorMessage from '../shared/ErrorMessage'
import { getGenre } from '../shared/albumHelpers'

export const INDIAN_GENRE_ID = 40
export const SUBGENRE_PARENT_IDS = [40, 17, 102, 125]

const BLANK = ''
const DEFAULT_SUFFIX = '_id'
const PARENT_ID = 0
const PRIMARY = 'primary'
const PRIMARY_SUFFIX = '_id_primary'
const SECONDARY = 'secondary'
const SECONDARY_SUFFIX = '_id_secondary'
const SUBGENRE = 'sub'

export function initGenre(id, countryWebsite, genres) {
  let result = translateToParentGenreID(getGenre(genres, id))

  if (countryWebsite === INDIA_COUNTRY_WEBSITE) {
    result = result || INDIAN_GENRE_ID
  }
  return result?.toString() || BLANK
}

export const isSubgenre = (id, genres) =>
  SUBGENRE_PARENT_IDS.includes(getGenre(genres, id)?.parent_id)
export function initSubgenre(id, genres) {
  return isSubgenre(id, genres) ? id.toString() : BLANK
}

export function initialGenres({
  countryWebsite,
  genres,
  primary_genre_id,
  secondary_genre_id,
}) {
  return {
    primary_genre_id: initGenre(primary_genre_id, countryWebsite, genres),
    secondary_genre_id: initGenre(secondary_genre_id, countryWebsite, genres),
    sub_genre_id_primary: initSubgenre(primary_genre_id, genres),
    sub_genre_id_secondary: initSubgenre(secondary_genre_id, genres),
  }
}

const GenreField = React.memo(function GenreField({
  defaultValue,
  error,
  genreOptions,
  onChangeHandler,
  suffix = DEFAULT_SUFFIX,
  touched,
  type,
}) {
  const { translations } = useConfig()[0]

  return (
    <>
      <ErrorMessage error={error} touched={touched} />
      <div className="grid-x div-album-form-field">
        <label className="cell medium-2" htmlFor={`${type}_genre${suffix}`}>
          <span>
            {translations[`${type}_genre`]}
            {type === 'primary' && '*'}
          </span>
        </label>
        <select
          className="cell medium-6 select-genre"
          defaultValue={defaultValue || undefined}
          id={`${type}_genre${suffix}`}
          name={`${type}_genre${suffix}`}
          onChange={(e) => onChangeHandler(type, e)}
        >
          {genreOptions}
        </select>
        <span className="cell medium-4"></span>
      </div>
    </>
  )
})
GenreField.displayName = 'GenreField'

GenreField.propTypes = {
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  error: PropTypes.string,
  touched: PropTypes.bool,
  genreOptions: PropTypes.array.isRequired,
  onChangeHandler: PropTypes.func.isRequired,
  suffix: PropTypes.string,
  type: PropTypes.string,
}

function buildOptions(genres) {
  return [
    <option key="none" value=""></option>,
    ...genres.map((genre) => (
      <option key={genre.id} value={genre.id}>
        {genre.name}
      </option>
    )),
  ]
}

const onlyParentsOrMatchingChildren = (parentID) => (g) =>
  parentID === null ? isParentGenre(g) : g.parent_id === parseInt(parentID, 10)
function getGenres(genres, parentID) {
  return genres
    .filter(onlyParentsOrMatchingChildren(parentID))
    .sort((a, b) => a.name.localeCompare(b.name))
}

const isParentGenre = (g) => g && g.parent_id === PARENT_ID

function translateToParentGenreID(genre) {
  if (!genre) return BLANK
  return isParentGenre(genre) ? genre.id : genre.parent_id
}

export const GenresContainer = ({
  errors,
  handleChange,
  primary_genre_id,
  secondary_genre_id,
  sub_genre_id_primary,
  sub_genre_id_secondary,
  touched,
}) => {
  const { genres } = useConfig()[0]

  const [primaryID, setPrimaryID] = useState(primary_genre_id)
  const [secondaryID, setSecondaryID] = useState(secondary_genre_id)

  const genreSetter = useCallback(
    (type, value) => {
      type === PRIMARY && setPrimaryID(value)
      type === SECONDARY && setSecondaryID(value)
    },
    [setPrimaryID, setSecondaryID]
  )

  const onChangeHandler = useCallback(
    (type, e) => {
      const value = e.target.value ? parseInt(e.target.value, 10) : undefined
      genreSetter(type, value)
      handleChange(e)
    },
    [genreSetter, handleChange]
  )

  const parentGenres = useMemo(() => getGenres(genres, null), [genres])
  const parentGenreOptions = useMemo(() => buildOptions(parentGenres), [
    parentGenres,
  ])

  const primarySubgenres = useMemo(() => getGenres(genres, primaryID), [
    genres,
    primaryID,
  ])
  const hasPrimarySubgenres = primarySubgenres.length > 0
  const primarySubgenreOptions = useMemo(() => buildOptions(primarySubgenres), [
    primarySubgenres,
  ])

  const secondarySubgenres = useMemo(() => getGenres(genres, secondaryID), [
    genres,
    secondaryID,
  ])
  const hasSecondarySubgenres = secondarySubgenres.length > 0
  const secondarySubgenreOptions = useMemo(
    () => buildOptions(secondarySubgenres),
    [secondarySubgenres]
  )

  return (
    <>
      <GenreField
        defaultValue={primaryID}
        error={errors.primary_genre_id}
        genreOptions={parentGenreOptions}
        onChangeHandler={onChangeHandler}
        touched={touched.primary_genre_id}
        type={PRIMARY}
      />
      {hasPrimarySubgenres && (
        <GenreField
          defaultValue={sub_genre_id_primary}
          error={errors.sub_genre_id_primary}
          genreOptions={primarySubgenreOptions}
          onChangeHandler={onChangeHandler}
          suffix={PRIMARY_SUFFIX}
          touched={touched.sub_genre_id_primary}
          type={SUBGENRE}
        />
      )}
      <GenreField
        defaultValue={secondaryID}
        error={errors.secondary_genre_id}
        genreOptions={parentGenreOptions}
        onChangeHandler={onChangeHandler}
        touched={touched.secondary_genre_id}
        type={SECONDARY}
      />
      {hasSecondarySubgenres && (
        <GenreField
          defaultValue={sub_genre_id_secondary}
          error={errors.sub_genre_id_secondary}
          genreOptions={secondarySubgenreOptions}
          onChangeHandler={onChangeHandler}
          suffix={SECONDARY_SUFFIX}
          touched={touched.sub_genre_id_secondary}
          type={SUBGENRE}
        />
      )}
    </>
  )
}
GenresContainer.displayName = 'GenresContainer'

GenresContainer.propTypes = {
  errors: PropTypes.object,
  handleChange: PropTypes.func.isRequired,
  primary_genre_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  secondary_genre_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  sub_genre_id_primary: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  sub_genre_id_secondary: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  touched: PropTypes.object,
}

export default React.memo(GenresContainer)
