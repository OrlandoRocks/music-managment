import React from 'react'
import PropTypes from 'prop-types'

import RadioToolbar from '../../../shared/RadioToolbar'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'
import { useConfig } from '../../contexts/ConfigContext'

const AlbumExplicit = ({ currentValue, handleChange }) => {
  const {
    releaseTypeVars: { explicitFieldsEnabled },
    translations,
  } = useConfig()[0]

  if (!explicitFieldsEnabled) return null

  const readMoreLink = (
    <a
      href={translations.explicit_read_more_url}
      target="_blank"
      rel="noopener noreferrer"
    >
      {translations.read_more}
    </a>
  )

  const explicitInfoTooltip = (
    <TooltipContainer>
      <TooltipTitle>{translations.explicit}</TooltipTitle>
      <TooltipColumn>{translations.explicit_info}</TooltipColumn>
    </TooltipContainer>
  )

  const explicitPrompt = (
    <span>
      {translations.explicit_prompt} {readMoreLink}{' '}
      <Tooltip>{explicitInfoTooltip}</Tooltip>
    </span>
  )

  return (
    <div className="grid-x div-album-form-field">
      <span className="cell medium-2"></span>
      <div className="cell medium-10">
        <RadioToolbar
          klassName="song-parental_advisory"
          handleChange={handleChange}
          name="parental_advisory"
          noText={translations.button_no}
          onChange={handleChange}
          prompt={explicitPrompt}
          value={currentValue}
          yesText={translations.button_yes}
        />
      </div>
    </div>
  )
}

AlbumExplicit.propTypes = {
  currentValue: PropTypes.bool,
  errors: PropTypes.object,
  handleChange: PropTypes.func.isRequired,
}

export default React.memo(AlbumExplicit)
