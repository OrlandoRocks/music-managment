import React from 'react'
import PropTypes from 'prop-types'
import LockIcon from '@material-ui/icons/Lock'

import { lockMessageColor } from '../shared/theme'

const styles = {
  container: {
    alignItems: 'center',
    display: 'flex',
  },
  icon: {
    color: lockMessageColor,
    fontSize: '2em',
    padding: '0 .25em 0 0',
  },
  message: {
    color: lockMessageColor,
    fontSize: '.75em',
    fontWeight: 700,
    marginBottom: 0,
  },
}

export function LockMessage({ message, show }) {
  if (!show) return null

  return (
    <div style={styles.container}>
      <LockIcon style={styles.icon} />
      <p style={styles.message}>{message}</p>
    </div>
  )
}

LockMessage.propTypes = {
  message: PropTypes.string,
  show: PropTypes.bool,
}

export default LockMessage
