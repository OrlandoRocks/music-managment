import React from 'react'
import PropTypes from 'prop-types'

import InfoIcon from '@material-ui/icons/Info'

import { infoMessageColor } from '../shared/theme'

export const STORE_MAX_ARTIST_WARNING_THRESHOLD = 4

const maxArtistsStyles = {
  container: {
    alignItems: 'center',
    display: 'flex',
  },
  icon: {
    color: infoMessageColor,
    fontSize: '2em',
    padding: '0 .25em 0 0',
  },
  message: {
    color: infoMessageColor,
    fontSize: '.75em',
    fontWeight: 700,
    marginBottom: 0,
  },
}

export function StoreMaxArtistsWarning({ message, show }) {
  if (!show) return null

  return (
    <div style={maxArtistsStyles.container}>
      <InfoIcon style={maxArtistsStyles.icon} />
      <p style={maxArtistsStyles.message}>{message}</p>
    </div>
  )
}
StoreMaxArtistsWarning.propTypes = {
  message: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
}
