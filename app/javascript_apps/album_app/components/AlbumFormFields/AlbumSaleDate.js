import React from 'react'
import PropTypes from 'prop-types'
import { KeyboardDatePicker } from '@material-ui/pickers'
import dayjs from 'dayjs'

import LockMessage from './LockMessage'

import { onlyDate } from '../../../utils/index'
import { useConfig } from '../../contexts/ConfigContext'

const AlbumSaleDate = ({ saleDate, setTouched, setValues, values }) => {
  const {
    features: { plans_pricing },
    person: {
      person_plan,
      plan_features: { scheduled_release: scheduled_release_feature },
    },
    translations,
  } = useConfig()[0]

  const maxDate = dayjs().add(1, 'years').toDate()
  const value = saleDate ? dayjs(saleDate).toDate() : dayjs().toDate()

  function handleSaleDateChange(date) {
    const formattedDate = dayjs(date)
    const dateObj = {
      day: formattedDate.format('DD'),
      month: formattedDate.format('MM'),
      year: formattedDate.format('YYYY'),
    }
    const goliveObj = { ...values.golive_date, ...dateObj }

    setValues({ ...values, golive_date: goliveObj, sale_date: onlyDate(date) })
    setTouched({ sale_date: true })
  }

  const isFutureDate = dayjs(saleDate)
    .startOf('day')
    .isAfter(dayjs(Date.now()).startOf('day'))

  const showScheduledUpgrade = getShowScheduledUpgrade({
    plans_pricing,
    isFutureDate,
    person_plan,
    scheduled_release_feature,
  })

  return (
    <div>
      <div className="grid-x div-album-form-field">
        <label
          className="cell medium-2 optional"
          htmlFor={translations.sale_date}
        >
          {translations.sale_date}
        </label>
        <div>
          {showScheduledUpgrade && (
            <div style={styles.upgradeContainer}>
              <LockMessage
                message={translations.scheduled_release_upgrade}
                show={showScheduledUpgrade}
              />
            </div>
          )}
          <div className="cell medium-4">
            <div className="testDiv" style={styles.buttonContainer}>
              <KeyboardDatePicker
                autoOk
                cancelLabel={translations.calendar_picker_cancel}
                format="L"
                id={translations.sale_date}
                InputAdornmentProps={{ color: 'primary', position: 'end' }}
                inputVariant="outlined"
                maxDate={maxDate}
                okLabel={translations.calendar_picker_ok}
                onChange={handleSaleDateChange}
                ToolbarComponent={() => null}
                value={value}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="grid-x div-album-form-field">
        <span className="cell medium-2"></span>
        <div className="cell medium-6 callout small primary">
          <p>{translations.sale_date_tip}</p>
        </div>
      </div>
    </div>
  )
}

AlbumSaleDate.propTypes = {
  values: PropTypes.object,
  saleDate: PropTypes.string,
  setTouched: PropTypes.func,
  setValues: PropTypes.func,
}

export default React.memo(AlbumSaleDate)

const styles = {
  buttonContainer: {
    margin: '0 0 .5em',
  },
  upgradeContainer: {
    paddingBottom: '1em',
  },
}

function getShowScheduledUpgrade({
  plans_pricing,
  isFutureDate,
  person_plan,
  scheduled_release_feature,
}) {
  if (Boolean(person_plan) === false) return false
  if (scheduled_release_feature) return false

  return plans_pricing && isFutureDate
}
