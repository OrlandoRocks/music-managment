import React from 'react'
import PropTypes from 'prop-types'
import { useFormikContext } from 'formik'

import MainArtists from './MainArtists'
import MainArtistsModal from './MainArtistModal/MainArtistsModal'
import { SPECIALIZED_RELEASE_TYPES } from '../../utils/constants'
import { useConfig } from '../../contexts/ConfigContext'

export function hasESIDs(creatives) {
  return Object.values(creatives).some((c) => {
    return [c.apple, c.spotify].some(
      (s) => Boolean(s.identifier) || Boolean(s.create_page)
    )
  })
}

/**
 * The user might have entered ESIDs before it became a special release,
 * so thereafter, we have to ensure that provided ESIDs remain visible/editable.
 *
 * Show the MainArtistModal if:
 * it's not a special release
 * OR
 * ESIDs are present
 * OR
 * it's already open
 */
function MainArtistsChooser(props) {
  const {
    releaseTypeVars: { specializedReleaseType },
  } = useConfig()[0]
  const {
    values: { creatives },
  } = useFormikContext()

  if (
    !hasESIDs(creatives) &&
    !props.modalUtils.showMainArtistModal &&
    SPECIALIZED_RELEASE_TYPES.includes(specializedReleaseType)
  ) {
    return <MainArtists {...props} />
  }

  return <MainArtistsModal {...props} />
}

MainArtistsChooser.propTypes = {
  modalUtils: PropTypes.shape({
    showMainArtistModal: PropTypes.bool.isRequired,
  }).isRequired,
}

export default React.memo(MainArtistsChooser)
