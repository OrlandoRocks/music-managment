import produce from 'immer'

import { appleFetchArtistRequest } from '../../utils/appleAPIRequests'
import { getSpotifyArtistData } from '../../utils/spotifyDataService'
import {
  parseAppleData,
  parseSpotifyData,
} from '../../utils/ExternalServiceIDs/helpers'

export default async function esidDataCreator(
  creatives,
  setESIDData,
  setFetched
) {
  const appleMap = new Map()
  const spotifyMap = new Map()

  Object.values(creatives).forEach((c) => {
    const { apple, spotify } = c
    if (apple.identifier) {
      appleMap.set(c.uuid, appleFetchArtistRequest(apple.identifier, c.name))
    }
    if (spotify.identifier) {
      spotifyMap.set(c.uuid, getSpotifyArtistData(spotify.identifier, c.name))
    }
  })

  let newCreatives = creatives
  if (appleMap.size || spotifyMap.size) {
    const [appleData, spotifyData] = await Promise.all(
      [appleMap.values(), spotifyMap.values()].map((arr) => Promise.all(arr))
    )

    newCreatives = produce(creatives, (draft) => {
      appleMap.forEach((_, k) => {
        const response = appleData.shift()
        if (!response.error) {
          draft[k].apple = parseAppleData(response)
        } else {
          draft[k].apple = { identifier: draft[k].apple.identifier }
        }
      })

      spotifyMap.forEach((_, k) => {
        const response = spotifyData.shift()
        if (!response.error) {
          draft[k].spotify = parseSpotifyData(response)
        } else {
          draft[k].spotify = { identifier: draft[k].spotify.identifier }
        }
      })
    })
  }

  setESIDData(newCreatives)
  setFetched(true)
}
