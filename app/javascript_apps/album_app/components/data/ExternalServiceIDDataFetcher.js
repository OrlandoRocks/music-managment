import React, { useEffect, useState } from 'react'

import esidDataCreator from './esidDataCreator'
import { useESIDData } from '../../contexts/ESIDDataContext'

// Fetch data once, on form load (mount)
function ExternalServiceIDDataFetcher({ creatives }) {
  const [fetched, setFetched] = useState(false)
  const { setESIDData } = useESIDData()

  useEffect(() => {
    // Create a promise for each ID
    // Await the results
    // Add the results to the esid context
    async function fetchAndSet() {
      if (fetched || Object.values(creatives).length === 0) return

      try {
        await esidDataCreator(creatives, setESIDData, setFetched)
      } catch (_) {
        // do nothing
      }
    }
    fetchAndSet()
  }, [creatives, fetched, setESIDData])

  return null
}

export default React.memo(ExternalServiceIDDataFetcher)
