import React, { useCallback, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { useLocation, useHistory } from 'react-router-dom'
// import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import StepActions from './DistributionStepper/StepActions'
import StepSwitch from './DistributionStepper/StepSwitch'

import {
  STEPS,
  STEP_PARAMS,
} from './DistributionStepper/distributionStepperHelpers'
import { useSongsData } from '../contexts/distributionContextHelpers'

import { useDistributionFlow } from '../contexts/DistributionContext'
import { setStepParam, setParams } from './DistributionStepper/routerHelpers'

export default function DistributionStepper({ mocks }) {
  // const config = useConfig()[0]
  const history = useHistory()

  const [
    { activeStep, album, continueDisabled, fetchedSongsData, song },
    { initSongsData, setActiveStep },
  ] = useDistributionFlow()

  // Fetch song-related data
  useSongsData(album, fetchedSongsData, initSongsData)

  const location = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [activeStep, location])

  const albumSubmitRef = useRef()
  const songSubmitRef = useRef()
  const storeSubmitRef = useRef()

  // WIP: pending progress bar refactor
  // xxxHandleNext functions use refs to link step Continue and form submit actions
  // so that Continue is equivalent to Submit where needed
  const albumHandleNext = useCallback(() => {
    if (albumSubmitRef.current) {
      albumSubmitRef.current.click()
    }
  }, [])
  const storesHandleNext = useCallback(() => {
    if (storeSubmitRef.current) {
      storeSubmitRef.current.click()
    }
  }, [])
  const songHandleNext = useCallback(() => {
    if (songSubmitRef.current) {
      songSubmitRef.current.click()
    } else {
      // Uploader is on screen, not song form
      setParams(
        { step: STEP_PARAMS.s2, track: song.data.track_number },
        history
      )
    }
  }, [history, song])
  const summaryHandleNext = useCallback(() => {
    setStepParam(STEP_PARAMS.art, history)
  }, [history])

  // Locals

  function handleBack() {
    history.goBack()
  }
  function noop() {
    console.log('Not implemented!')
  }
  function getHandleNext() {
    switch (true) {
      case activeStep === STEPS.ALBUM_FORM_1: {
        return albumHandleNext
      }
      case activeStep === STEPS.STORE_FORM: {
        return storesHandleNext
      }
      case activeStep === STEPS.SONG_FORM_1:
      case activeStep === STEPS.SONG_FORM_2:
      case activeStep === STEPS.SONG_FORM_3: {
        return songHandleNext
      }
      case activeStep === STEPS.SUMMARY: {
        return summaryHandleNext
      }
      default: {
        return noop
      }
    }
  }

  const currentHandleNext = getHandleNext()
  const classes = stepperStyles()

  return (
    <div className={classes.root}>
      <StepActions
        activeStep={activeStep}
        continueDisabled={continueDisabled}
        handleBack={handleBack}
        handleNext={currentHandleNext}
      />
      <StepSwitch
        mockAlbumSubmitHandler={mocks.albumSubmitHandler}
        mockSongSubmitHandler={mocks.songSubmitHandler}
        mockStoreSubmitHandler={mocks.storeSubmitHandler}
        activeStep={activeStep}
        album={album}
        albumSubmitRef={albumSubmitRef}
        fetchedSongsData={fetchedSongsData}
        setActiveStep={setActiveStep}
        storeSubmitRef={storeSubmitRef}
        songSubmitRef={songSubmitRef}
      />
    </div>
  )
}
DistributionStepper.defaultProps = {
  mocks: {},
}

DistributionStepper.propTypes = {
  mocks: PropTypes.object,
}

const stepperStyles = makeStyles(() => ({
  root: {},
  stepActive: {},
  stepLabel: {},
}))
