import {
  appURLs,
  pickArtistServicesCmsUrl,
  replaceTld,
} from '../../../top_nav_bar/linkHelpers'
import {
  buildReleaseAccountURL,
  USER_ID_VAR,
} from '../../../top_nav_bar/linkConstants'

const mockConstants = jest.requireMock('../../../top_nav_bar/linkConstants')
jest.mock('../../../top_nav_bar/linkConstants', () => {
  return jest.requireActual('../../../top_nav_bar/linkConstants')
})

const USER_ID = 123
const ALBUM_ID = 456

describe('helpers', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('.appURLs >', () => {
    const table = [
      ['qa.tunecore.com'],
      ['qa.tunecore.fr'],
      ['stagingwp.tunecore.com'],
      ['web.tunecore.co.uk'],
      ['www.tunecore.co.uk'],
      ['www.tunecore.com'],
    ]

    it.each(table)('host: (%s)', (host) => {
      mockConstants.HOST = host
      expect(appURLs(USER_ID)).toMatchSnapshot()
    })
  })

  describe('.buildReleaseAccountURL', () => {
    describe('release show pages trigger album param', () => {
      const table = ['/albums', '/ringtones', '/singles'].map((p) => [
        `${p}/${ALBUM_ID}`,
        `${mockConstants.RELEASE_ACCOUNT_BASE_PATH}?album=${ALBUM_ID}`,
      ])

      it.each(table)('path: %s -> %s', (path, result) => {
        expect(buildReleaseAccountURL(path)).toStrictEqual(result)
      })
    })

    describe('other pages do NOT trigger album param', () => {
      const table = ['/foo', '/bar', '/baz'].map((p) => [
        p,
        mockConstants.RELEASE_ACCOUNT_BASE_PATH,
      ])

      it.each(table)('path: %s -> %s', (path, result) => {
        expect(buildReleaseAccountURL(path)).toStrictEqual(result)
      })
    })
  })

  describe('.isValidPath >', () => {
    it('throws when invalid', () => {
      mockConstants.HOST = 'www.tunecore.com'
      mockConstants.APP_PATHS = { test: 'no/leading/slash' }

      expect(() => appURLs(USER_ID)).toThrow()
    })

    it('no error when paths are valid', () => {
      mockConstants.HOST = 'www.tunecore.com'
      mockConstants.APP_PATHS = { test: '/with/leading/slash' }

      expect(appURLs(USER_ID)).toMatchSnapshot()
    })
  })

  describe('.withUserID >', () => {
    it('interpolates user_id', () => {
      mockConstants.HOST = 'www.tunecore.com'
      mockConstants.APP_PATHS = { test: `/with/user/id/${USER_ID_VAR}` }

      expect(appURLs(USER_ID).test.endsWith(USER_ID)).toStrictEqual(true)
    })
  })

  describe('.pickArtistServicesCmsUrl >', () => {
    const table = [
      ['foo.tunecore.co.uk'],
      ['foo.tunecore.com.au'],
      ['foo.tunecore.ca'],
      ['foo.tunecore.com'],
      ['foo.tunecore.de'],
      ['foo.tunecore.fr'],
      ['foo.tunecore.in'],
      ['foo.tunecore.it'],
    ]

    it.each(table)('host: (%s)', (host) => {
      mockConstants.HOST = host
      expect(pickArtistServicesCmsUrl()).toMatchSnapshot()
    })
  })

  describe('.replaceTld >', () => {
    const table = [
      ['//tunecore.com'],
      ['//qa.tunecore.com'],
      ['//qa.tunecore.com/with/path'],
      ['//qa.tunecore.fr'],
      ['//stagingwp.tunecore.com'],
      ['//web.tunecore.co.uk'],
      ['//web.tunecore.co.uk/with/path'],
      ['//www.tunecore.co.uk'],
      ['//www.tunecore.com'],
      ['//www.tunecore.com/'],
    ]

    it.each(table)("url: (%s), page TLD: 'foo'", (url) => {
      mockConstants.HOST = 'www.tunecore.foo'
      expect(replaceTld(url)).toMatchSnapshot()
    })
  })
})
