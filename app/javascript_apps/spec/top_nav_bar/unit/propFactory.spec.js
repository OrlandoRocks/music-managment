import { ITEMS, propFactory } from '../../../top_nav_bar/propFactory'

import mockNavData from '../helpers'

const getProps = propFactory(mockNavData)

describe('.propFactory', () => {
  describe('valid keys', () => {
    it.each(Object.values(ITEMS))('key: (%s)', (key) => {
      expect(getProps(key)).toMatchSnapshot()
    })
  })

  describe('unknown key', () => {
    it('throws when unknown', () => {
      expect(() => getProps('foo')).toThrow()
    })
  })

  describe('undefined', () => {
    it('throws when undefined', () => {
      expect(() => getProps(undefined)).toThrow()
    })
  })
})
