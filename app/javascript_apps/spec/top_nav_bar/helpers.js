import {
  appURLs,
  pickArtistServicesCmsUrl,
  replaceTld,
} from '../../top_nav_bar/linkHelpers'

const mockNavData = {
  admin: {
    is_admin: true,
    is_under_admin_control: false,
  },
  cart_items_count: 0,
  country_website: 'US',
  current_lang: 'English',
  current_locale_lang_abbrv: 'en',
  language_selector_options:
    '[{"country_website_id":1,"yml_languages":"en-us","selector_language":"English","selector_country":"United States","selector_order":1,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"pt-us","selector_language":"Português","selector_country":"Brazil","selector_order":9,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"es-us","selector_language":"Español","selector_country":"Latin America","selector_order":10,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"ru-us","selector_language":"Pусский","selector_country":"Russia","selector_order":11,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"id-us","selector_language":"Bahasa Indonesia","selector_country":"Indonesia","selector_order":12,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"ro-us","selector_language":"Română","selector_country":"Romania","selector_order":13,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"th-us","selector_language":"ไทย","selector_country":"Thailand","selector_order":14,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"hu-us","selector_language":"Magyar","selector_country":"Hungary","selector_order":15,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"nl-us","selector_language":"Nederlands","selector_country":"Netherlands","selector_order":16,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"cs-us","selector_language":"Čeština","selector_country":"Czech Republic","selector_order":17,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"pl-us","selector_language":"Polskie","selector_country":"Poland","selector_order":19,"redirect_country_website_id":null},{"country_website_id":1,"yml_languages":"tr-us","selector_language":"Türkçe","selector_country":"Turkey","selector_order":20,"redirect_country_website_id":null}]',
  plan: {
    id: 4,
    display_name: 'Professional',
    features: {
      accept_splits: false,
      create_splits: false,
    },
    name: 'professional',
  },
  publishing_url: '//www.tunecore.com/music-publishing-administration',
  show: {
    created_splits: false,
    distribution_credits: false,
    fb_monetization: false,
    splits_invites: false,
  },
  translations: {
    account: 'ACCOUNT SETTINGS',
    add_release: 'ADD RELEASE',
    admin: 'ADMIN',
    album: 'ALBUM',
    analytics: 'ANALYTICS',
    artist_services: 'ARTIST SERVICES',
    balance_history: 'BALANCE HISTORY',
    created_splits_link_label: 'Created Splits Trans',
    discography: 'DISCOGRAPHY',
    distribution_credits: 'DISTRIBUTION CREDITS',
    facebook_instagram_reels: 'Facebook, Instagram, Reels',
    give_back_control: 'GIVE BACK CONTROL',
    go_unlimited: 'GO UNLIMITED',
    how_much_cost: "You're starting a pay per release distribution.",
    lock_icon_aria_label: 'aria lock translation',
    log_out: 'LOG OUT',
    mobile_menu_aria_label: 'Menu',
    money_and_analytics: 'MONEY & ANALYTICS',
    my_account_display: 'My Account',
    notifications_header: 'Notifications',
    publishing: 'PUBLISHING',
    releases: 'RELEASES',
    ringtone: 'RINGTONE',
    sales_report: 'SALES REPORT',
    select_a_language: 'Select a Language',
    shopping_cart: 'Shopping Cart',
    single: 'SINGLE',
    social_monetization: 'SOCIAL MONETIZATION',
    splits_invites_link_label: 'Splits Invites Trans',
    store_manager: 'STORE MANAGER',
    tunecore_cost: '//support.tunecore.com/hc/en-us',
    unlimited_plan: 'Unlimited Plan',
    unlimited_plans: 'Unlimited Plans',
    view_pricing: 'View Pricing Here.',
    what_release: 'What would you like to release?',
    youtube_content_id: 'YouTube Content ID',
  },
  unseen_notifications_count: 0,
  user_id: 11,
}
mockNavData.urls = {
  ...appURLs(mockNavData.user_id),
  artist_services_cms_url: pickArtistServicesCmsUrl(),
  publishing_url: replaceTld(mockNavData.publishing_url), // Can be an app or marketing URL.
}

export default mockNavData
