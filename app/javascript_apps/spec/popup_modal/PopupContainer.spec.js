import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render } from '@testing-library/react'

import PopupContainer from '../../popup_modal/PopupContainer'

describe('PopupModal', function () {
  const originalLocation = window.location
  delete window.location

  afterAll(() => (window.location = originalLocation))

  it('renders the tiktok_abandonment popup', () => {
    window.location = new URL('https://www.example.com/?popup=fb_end')

    const { getByText } = render(
      <div id="popup_modal">
        <PopupContainer appElement={document.getElementById('popup_modal')} />
      </div>
    )

    const message = getByText('fb end header')
    expect(message).toBeDefined()
  })

  it('renders the tiktok_triggered popup', () => {
    window.location = new URL('https://www.example.com/?popup=fb_start')

    const { getByText } = render(
      <div id="popup_modal">
        <PopupContainer appElement={document.getElementById('popup_modal')} />
      </div>
    )

    const message = getByText('fb start header')
    expect(message).toBeDefined()
  })

  it('renders the fb promo popup', () => {
    window.location = new URL('https://www.example.com/?popup=fb_promo')

    const { getByText } = render(
      <div id="popup_modal">
        <PopupContainer appElement={document.getElementById('popup_modal')} />
      </div>
    )

    const message = getByText('fb promo headline1')
    expect(message).toBeDefined()
  })

  it('renders the fb terms popup', () => {
    window.location = new URL('https://www.example.com/?popup=fb_terms')

    const { getByText } = render(
      <div id="popup_modal">
        <PopupContainer appElement={document.getElementById('popup_modal')} />
      </div>
    )

    const message = getByText('fb terms intro')
    expect(message).toBeDefined()
  })
})
