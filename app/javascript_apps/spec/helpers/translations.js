export default {
  album_app: {
    add_main_artist: 'Add Artist',
    advanced_distribution_header: 'Advanced Distribution Features',
    advanced_distribution_subheader: 'Note: the following features...',
    album_label: 'Album Label',
    album_title: 'Album Title',
    apple_manual_id: {
      headline: 'Paste a link to your Apple profile.',
      placeholder: 'Artist page link',
      subtext: 'Go to your artist page on Apple and copy your URL',
    },
    artist_errors: {
      length: 'Artist name must be shorter than 120 characters',
    },
    artist_modal_a11y_description:
      'Modal form for adding artists to the release.',
    artist_modal_summary: {
      apple: {
        will_create_page: 'An Apple page will be created.',
      },
      headline: 'Summary',
      spotify: {
        will_create_page: 'A Spotify page will be created.',
      },
      step_skipped: 'Step skipped.',
      will_save_selection: 'Your selection will be saved.',
      will_save_url: 'Your page URL will be saved.',
    },
    artist_name_input: {
      header: "What's the artist's name?",
      placeholder: 'Artist Name',
      step_label: 'Artist Name',
    },
    artist_search: {
      artist_missing: "Don't see your artist?",
      latest_album: 'Latest Album',
      top_album: 'Top Album',
    },
    back_button: 'back',
    calendar_picker_cancel: 'Cancel',
    calendar_picker_ok: 'OK',
    clean_version_prompt: 'Does an explicit version of this track exist',
    continue_button: 'Continue',
    create_album: 'Create an Album',
    create_new_artist_id: {
      create_btn: 'Create {{serviceName}} page for me',
      headline: 'Do you want us to create a Spotify account for you?',
      subtext: 'Lorem Ipsum',
      skip_btn: 'Skip',
    },
    create_single: 'Create a Single',
    duplicate_primary_artist: 'Duplicate primary artist',
    edit_album: 'Edit Album',
    edit_single: 'Edit Single',
    language: 'Language',
    main_artist: 'Main Artist',
    manual_id: {
      button_link: 'Provide link',
      error_messages: {
        fetch: 'Fetch failed',
        id: 'Bad ID',
        link:
          'Incorrect link.  Please make sure you’ve copied the entire link correctly.',
      },
      paste: 'Paste',
      url_label: 'URL',
    },
    missing: 'missing',
    not_previously_released: 'No',
    original_release_date: 'Original Release Date',
    optional: 'optional',
    optional_isrc: 'ISRC',
    optional_isrc_note: 'What does ISRC mean?',
    optional_isrc_note_url:
      'https://support.tunecore.com/hc/en-us/articles/115006499567?_ga=2.166511101.2102437598.1591018418-352799260.1588960511',
    plans: {
      artist_not_yet_purchased:
        'Upgrade Required for all new Main Artists released from this account',
      artist_purchase_will_apply:
        'Paid Upgrade Required and will be automatically applied at checkout',
    },
    present: 'present',
    previously_released: 'Previously Released?',
    primary_genre: 'Primary Genre',
    recording_location: 'Recording Location',
    remove_button: 'Remove',
    required_field: 'Required Field',
    save_album_and_add_songs: 'Save Album and Add Songs',
    save_selections: 'Save Selections',
    scheduled_release_upgrade: 'Scheduled release upgrade message',
    secondary_genre: 'Secondary Genre',
    single_title: 'Single Title',
    skip: 'Skip',
    snackbars: {
      artist_selected: 'Artist Selected',
      url_saved: 'Artist Page URL Saved',
    },
    spotify_manual_id: {
      headline: 'Paste a link to your Spotify profile.',
      placeholder: 'Artist page link',
      subtext: 'Go to your artist page on Spotify and copy your Spotify URI',
    },
    store_max_artists_warning: "Spotify won't show some of these!",
    sub_genre: 'Sub Genre',
    territory_picker: {
      aria_modal_content_label: 'Modal form for specifying release territories',
      back_to_form: 'Back To Form',
      back_to_overview: 'Back To Overview',
      edit_list: 'Edit List',
      exclude: 'Exclude',
      excluded: 'Excluded',
      field_label: 'Can this release be sold worldwide?',
      finish_adding: 'Finish Adding',
      form_level_selection_descriptions: {
        exclusion: "This {{album_type}} won't be sold in",
        inclusion: 'This {{album_type}} will be sold in',
      },
      include: 'Include',
      included: 'Included',
      overview_header: 'Do you want to sell this {{album_type}} worldwide?',
      picker_selection_descriptions: {
        exclusion: 'Sell the {{album_type}} worldwide, but exclude',
        inclusion: 'This {{album_type}} will be sold in',
      },
      remove_all: 'Remove All',
      search_headers: {
        exclusion: 'Which countries do you want to exclude from the release?',
        inclusion:
          'In which countries do you want to make this release available?',
      },
      search_instruction: 'Continue typing to add more countries',
      search_placeholder: 'Search country or a region',
      selection_type_descriptions: {
        worldwide: 'Sell worldwide',
        exclusion: 'Sell worldwide, but exclude some countries',
        inclusion: 'Sell only in selected countries',
      },
      top_radio: {
        restrictions: 'No, I have some restrictions',
        worldwide: 'Yes',
      },
      toggle_menu: 'Toggle Menu',
    },
    upc_ean_code: 'UPC/EAN Code',
    upc_errors: {
      length: 'Must be 12 to 13 digits',
      type: 'Must be a number',
    },
    upc_note:
      'If you already have a UPC for this release, please add. If not, no problem, we’ll create one for you.',
    various_artists_modal_aria: 'Modal ARIA',
    various_artists_modal_body: 'Modal body text.',
    various_artists_modal_cancel: 'Cancel',
    various_artists_modal_continue: 'Continue',
    various_artists_modal_header: 'Modal Header',
    various_artists_prompt: 'This is a various artist album',
    yes_previously_released: 'Yes',
  },
  compositions: {
    add_shares: 'Add Shares',
    compositions_card: {
      cae: 'CAE/IPI Number (10 Digits)',
      cae_is_not_a_number: 'CAE/IPI Number is not a number',
      cancel: 'Cancel',
      compositions: '{{numCompositions}} Compositions',
      customer_support:
        'Please contact customer support to change your date of birth, PRO, or add an affiliated PRO.',
      dob: 'Date of Birth',
      edit_info: 'Edit Info',
      edit_songwriter: 'Edit Songwriter',
      find_cae_numbers_help_link:
        'For more information on CAE/IPI Numbers and how to find yours, click',
      first_name: 'First Name',
      last_name: 'Last Name',
      middle_name: 'Middle Name',
      name_blank: 'First Name and Last Name cannot be blank',
      prefix: 'Prefix',
      prefixes: {
        'Mr.': 'Mr.',
        'Ms.': 'Ms.',
        'Mrs.': 'Mrs.',
        'Dr.': 'Dr.',
      },
      pro: 'PRO',
      save: 'Save',
      shares_missing: '{{numSharesMissing}} Shares Missing',
      shares_reported: '{{numShares}} Shares Reported',
      suffix: 'Suffix',
      suffixes: {
        'Jr.': 'Jr.',
        'Sr.': 'Sr.',
        I: 'I',
        II: 'II',
        III: 'III',
        IV: 'IV',
      },
    },
    validations: {
      first_name: 'First Name required',
      last_name: 'Last Name required',
      cowriter_share:
        'Total value must be between .05 - 100. If you did not write this song, hide the composition.',
    },
    data_table_headers: {},
    edit_composition: 'Edit Composition',
    statuses: {
      accepted: 'Accepted',
      shares_missing: 'Shares Missing',
      submitted: 'Submitted',
      resubmitted: 'Resubmitted',
      terminated: 'Terminated',
      pending_distribution: 'Pending Distribution',
    },
    splits_modal: {
      shares:
        'Report Writer & Co-Writer Shares ({{index}} of {{total_compositions}})',
      has_been_hidden: 'has been hidden',
    },
    tooltips: {
      your_writer_share_header: 'Lorem Ipsum',
      your_writer_share_content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    },
    add_compositions: {
      add_composition_for: 'Add Composition for',
      validations: {
        album_name: 'Cannot contain special characters or non-Latin languages',
        duplicate_title: `You have already submitted a song with this title for registration,
          please do not attempt to submit the same song more than once.
          If you would like to make edits to your submitted song please contact Artist Support.`,
        isrc_number: 'Must be a valid ISRC (12 alphanumeric characters)',
        percent: 'Value must be between 1 - 100',
        performing_artist:
          'Cannot contain special characters or non-Latin languages',
        release_date: 'Must be formatted like MM/DD/YYYY',
        title: 'Cannot contain special characters or non-Latin languages',
      },
    },
    bulk_compositions: {
      upload_text: 'Bulk Upload Compositions',
      modal_text_paragraph_instruction: `If you have multiple compositions you'd like to submit at,
        once we offer the ability for you to download this sample file to fill in all of your,
        composition information and upload to your publishing dashboard.`,
      modal_text_paragraph_note: `Please note that you must upload in the exact format provided,
        otherwise your compositions will not be successfully added.`,
      sample_file_name: 'Sample Upload File',
      upload_button_text: 'Upload File',
      limit_exceeded_error:
        'Bulk Upload Failed: Exceeds max Limit (1,000 works)',
      upload_succeeded:
        'Bulk Upload Succeeded. Your compositions will be created shortly.',
      upload_error: 'Error processing the Bulk Upload. Please try again',
      upload_in_progress: 'Uploading & validating your file...',
    },
    thank_you: {
      header: 'Compositions added for',
      headline: 'Thank you for adding your songs!',
      paragraph_1: `Thank you for submitting your Non-Tunecore-Distributed compositions to your TuneCore Publishing Administration account!
                  We will begin processing your submissions and registering your compositions with the worldwide collection sources.
                  Please allow a few weeks for this process to be complete.
                  Please upload your MP3/WAV files for the songs you submitted <a></a>`,
      paragraph_2: `If you have any trouble uploading music to the Dropbox link provided, please submit via file-sharing service,
                  (Dropbox, WeTransfer, Box, etc.) to sync@tunecore.com. Please make sure your mp3s are formatted in the
                  following way: 'Composer name - Song Title'.`,
      cancel: 'Close',
    },
  },
  songs_app: {
    completed: 'Completed',
    copyright_add_copyright_buttons: {
      music_composition: 'Add Composer and/or Lyricist',
      sound_recording: 'Add Performer and/or Producer',
    },
    copyright_input_placeholder: 'Copyright holder name',
    copyright_section_titles: {
      music_composition: 'Musical Composition Copyright ©* (Required)',
      sound_recording: 'Sound Recording Copyright ℗* (Required)',
    },
    copyright_required_messages: {
      music_composition:
        'The rights holder of a musical composition is generally the composers, and/or the lyricists and/or the publishers. If you are owner, please supply your name.​ One name per line.',
      sound_recording:
        'The producer of the master is the author/owner of the sound recording. If you are owner, please supply your name.​ One name per line.',
    },
    role_selected: '1 role selected',
    roles_selected: '%numSelected% roles selected',
    various_artists: 'Various Artists',
    by: 'by',
    cancel: 'cancel',
    song_title_transliteration: 'Song Title Transliteration',
    isrc_placeholder:
      'Please use the ISRC that was assigned to this song at the time of release',
    must_provide_english_track_title_translation:
      'Please provide the English translation of the song title',
    optional_isrc: {
      error: 'ISRC must be 12 characters. Please enter a valid ISRC.',
    },
    form_field_warnings: {
      song_name: {
        featuring:
          'If you\'re trying to add a Featured artist to this song, please add them in the "Artist / Creatives" section.',
      },
      artist_name: {
        featuring:
          'Are you trying to add a featured artist to this song? If so, please enter them in the Featured Artist field below.',
      },
    },
    lyric_do_column: {
      do: 'DO:',
      single_space_lines:
        'Single space lines belonging to a single verse or chorus',
      separate_sections_or_stanza:
        'Separate sections or stanza breaks with a double space',
      numbers_must_be_numerals: 'Numbers must be written as numerals. Ex. 1985',
      write_out_repeated_lines: 'Write out repeated lines and choruses',
      begin_each_line_with_a_capital_letter:
        'Begin each line with a capital letter',
    },
    lyric_dont_column: {
      dont: 'DONT:',
      do_not_include_header:
        'Do not include header or additional text that is not heard on the track',
      do_not_use_italics_or_bold_text: 'Do not use italics or bold text',
      do_not_use_periods_or_commas:
        'Do not use periods or commas at the end of any line or section',
      do_not_include_descriptions:
        'Do not include descriptions or names of sound effects',
    },
  },
  popup_modal: {
    fb_end: {
      header: 'fb end header',
      body_1: 'fb end body 1',
      body_2: 'fb end body 2',
      body_3: 'fb end body 3',
      body_4: 'fb end body 4',
      button: 'fb end button',
    },
    fb_start: {
      header: 'fb start header',
      body: 'fb start body',
      button: 'fb start button',
    },
    fb_promo: {
      headline1: 'fb promo headline1',
      headline2: 'fb promo headline2',
      button1: 'fb promo button1',
      button2: 'fb promo button2',
    },
    fb_terms: {
      intro: 'fb terms intro',
      link_text: 'fb terms link display value',
      list: 'fb terms list',
      ok: 'fb terms OK',
      outro: 'fb terms outro',
      reels_description: 'fb terms kb url',
    },
  },
}
