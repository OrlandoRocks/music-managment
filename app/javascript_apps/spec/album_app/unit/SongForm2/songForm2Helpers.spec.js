import { combineFormArtists } from '../../../../album_app/components/SongForm2/songForm2Helpers'

const albumArtist = {
  artist_name: 'd',
  associated_to: 'Album',
  credit: 'primary_artist',
  role_ids: [],
  creative_id: 97,
  roles: [
    {
      id: 'primary_artist',
      role_type_raw: 'primary_artist',
      role_type: 'primary artist',
    },
  ],
  uuid: 'ca2ddec1-d6e9-6147-8f86-a92ebd690036',
}

const albumArtist2 = {
  artist_name: 'd',
  associated_to: 'Album',
  credit: 'primary_artist',
  role_ids: ['3'],
  creative_id: 97,
  roles: [
    {
      id: 'primary_artist',
      role_type_raw: 'primary_artist',
      role_type: 'primary artist',
    },
  ],
  uuid: 'ca2ddec1-d6e9-6147-8f86-a92ebd690036',
}

const songArtist1 = {
  artist_name: 'Song Artist 1',
  associated_to: 'Song',
  creative_id: null,
  credit: null,
  role_ids: [],
  roles: [
    { id: 2, role_type_raw: 'producer', role_type: 'producer' },
    { id: 5, role_type_raw: 'acoustic guitar', role_type: 'acoustic guitar' },
  ],
  uuid: '5eb8b119-7045-0991-69d9-ea49b7ead86e',
}

const songArtist2 = {
  artist_name: 'Song Artist 2',
  associated_to: 'Song',
  creative_id: null,
  credit: null,
  role_ids: [],
  roles: [
    {
      id: 'primary_artist',
      role_type_raw: 'primary artist',
      role_type: 'primary artist',
    },
    { id: 5, role_type_raw: 'acoustic guitar', role_type: 'acoustic guitar' },
  ],
  uuid: '5eb8b119-7045-0991-69d9-ea49b7ead86e',
}

const songArtist3 = {
  artist_name: 'Song Artist 3',
  associated_to: 'Song',
  creative_id: null,
  credit: null,
  role_ids: [],
  roles: [
    {
      id: 'featuring',
      role_type_raw: 'featured',
      role_type: 'featured',
    },
    { id: 5, role_type_raw: 'acoustic guitar', role_type: 'acoustic guitar' },
  ],
  uuid: '5eb8b119-7045-0991-69d9-ea49b7ead86e',
}

const songwriters1 = {
  '681209de-8291-a33b-4d41-301f7963710e': {
    artist_name: 'd',
    associated_to: 'Song',
    creative_id: null,
    credit: 'contributor',
    role_ids: ['3'],
    uuid: '681209de-8291-a33b-4d41-301f7963710e',
  },
}

const songwriters2 = {
  '681209de-8291-a33b-4d41-301f7963710e': {
    artist_name: 'd2',
    associated_to: 'Album',
    creative_id: 97,
    credit: 'primary_artist',
    role_ids: ['3'],
    uuid: '681209de-8291-a33b-4d41-301f7963710e',
  },
}

const expected1 = [
  {
    artist_name: 'd',
    associated_to: 'Album',
    credit: 'primary_artist',
    role_ids: ['3'],
    creative_id: 97,
  },
  {
    artist_name: 'Song Artist 1',
    associated_to: 'Song',
    credit: 'contributor',
    role_ids: ['2', '5'],
    creative_id: null,
  },
]

const expected2 = [
  {
    artist_name: 'd',
    associated_to: 'Album',
    credit: 'primary_artist',
    role_ids: ['3'],
    creative_id: 97,
  },
  {
    artist_name: 'Song Artist 2',
    associated_to: 'Song',
    credit: 'primary_artist',
    role_ids: ['5'],
    creative_id: null,
  },
]

const expected3 = [
  {
    artist_name: 'd',
    associated_to: 'Album',
    credit: 'primary_artist',
    role_ids: ['3'],
    creative_id: 97,
  },
  {
    artist_name: 'Song Artist 3',
    associated_to: 'Song',
    credit: 'featuring',
    role_ids: ['5'],
    creative_id: null,
  },
]

const expected4 = [
  {
    artist_name: 'd',
    associated_to: 'Album',
    credit: 'primary_artist',
    role_ids: [],
    creative_id: 97,
  },
  {
    artist_name: 'Song Artist 3',
    associated_to: 'Song',
    credit: 'featuring',
    role_ids: ['5'],
    creative_id: null,
  },

  {
    artist_name: 'd2',
    associated_to: 'Album',
    credit: 'primary_artist',
    role_ids: ['3'],
    creative_id: 97,
  },
]

// Roles are combined, IDs preserved, 'credit'-type roles become credits and override 'contributor'

// Note: case 4 --- ${songwriters2} | ${[albumArtist2, songArtist3]} | ${expected4} --- is an imperfect test.
// When a songwriter who is also an album primary artist is edited,
// its Creative record is edited *by the form* to: associated_to: Song, credit: contributor, creative_id: null.
// The form behavior is outside the scope of this unit test.
// This test shows only that 3 creatives are submitted, and the primary (former songwriter) now has the songwriter role stripped.
describe('combineFormArtists', () => {
  describe.each`
    songwriters     | creatives                      | expected
    ${songwriters1} | ${[albumArtist, songArtist1]}  | ${expected1}
    ${songwriters1} | ${[albumArtist, songArtist2]}  | ${expected2}
    ${songwriters1} | ${[albumArtist, songArtist3]}  | ${expected3}
    ${songwriters2} | ${[albumArtist2, songArtist3]} | ${expected4}
  `('table test', ({ songwriters, creatives, expected }) => {
    test(`should combine into expected`, () => {
      expect(combineFormArtists(songwriters, creatives)).toEqual(expected)
    })
  })
})
