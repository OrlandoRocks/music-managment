import produce from 'immer'
import { hasESIDs } from '../../../album_app/components/AlbumFormFields/MainArtistsChooser'

const creatives = {
  123: {
    apple: {},
    spotify: {},
  },
  456: {
    apple: {},
    spotify: {},
  },
}

describe('.hasESIDs', () => {
  describe('creatives without ESIDs', () => {
    it('returns false', () => {
      expect(hasESIDs(creatives)).toBeFalsy()
    })
  })

  describe('creatives with ESIDs', () => {
    const subject = produce(creatives, (draft) => {
      Object.values(draft)[0].apple.identifier = '123'
    })
    it('returns true', () => {
      expect(hasESIDs(subject)).toBeTruthy()
    })
  })

  describe('creatives with create_page flag', () => {
    const subject = produce(creatives, (draft) => {
      Object.values(draft)[0].apple.create_page = true
    })
    it('returns true', () => {
      expect(hasESIDs(subject)).toBeTruthy()
    })
  })
})
