import { STEPS } from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import { pruneOutOfScopeFields } from '../../../album_app/utils/songDataHelpers'

const values = {
  id: 123,
  name: 'Name',
  language_code_id: 1,
  translated_name: null,
  version: null,
  cover_song: false,
  made_popular_by: null,
  explicit: null,
  clean_version: null,
  optional_isrc: 'TCAAA2100083',
  lyrics: '',
  asset_url:
    'https://s3.amazonaws.com:443/tcc.tunecore.com.staging/1%2F1620326621-file_example_WAV_1MG2.wav?Signature=ySdpv1Rc%2Bp2PELvJYOB59bdS4Po%3D&Expires=1621029398&AWSAccessKeyId=AKIAISJL6HSZITWXRMSQ&',
  album_id: 1,
  artists: [],
  track_number: 1,
  previously_released_at: null,
  asset_filename: 'file_example_WAV_1MG2.wav',
  instrumental: null,
  copyrights: null,
  song_start_times: [],
}
const {
  clean_version: _c,
  explicit: _e,
  instrumental: _i,
  ...expected
} = values

describe('pruneOutOfScopeFields', () => {
  it('prunes fields from Song Form 1', () => {
    expect(pruneOutOfScopeFields(values, STEPS.SONG_FORM_1)).toEqual(expected)
  })
  it('prunes fields from Song Form 2', () => {
    expect(pruneOutOfScopeFields(values, STEPS.SONG_FORM_2)).toEqual(expected)
  })
})
