import {
  parseValues,
  prepareSongStartTimes,
} from '../../../../album_app/components/SongForm3/songForm3Helpers'

const baseValues = {
  id: 123,
  name: 'Name',
  language_code_id: 1,
  translated_name: null,
  version: null,
  cover_song: false,
  made_popular_by: null,
  explicit: null,
  clean_version: null,
  optional_isrc: 'TCAAA2100083',
  lyrics: '',
  asset_url:
    'https://s3.amazonaws.com:443/tcc.tunecore.com.staging/1%2F1620326621-file_example_WAV_1MG2.wav?Signature=ySdpv1Rc%2Bp2PELvJYOB59bdS4Po%3D&Expires=1621029398&AWSAccessKeyId=AKIAISJL6HSZITWXRMSQ&',
  album_id: 1,
  artists: [],
  track_number: 1,
  previously_released_at: '2010-01-01',
  asset_filename: 'file_example_WAV_1MG2.wav',
  instrumental: null,
  copyrights: null,
  song_start_times: [],
}

describe('parseValues', () => {
  it('controls clean_version when explicit', () => {
    const values = {
      ...baseValues,
      explicit: true,
    }
    expect(parseValues(values).clean_version).toEqual(false)
  })

  it('controls clean_version when instrumental', () => {
    const values = {
      ...baseValues,
      instrumental: true,
    }
    expect(parseValues(values).clean_version).toEqual(false)
  })

  // Validation catches this before it can happen
  it('does not change clean_version', () => {
    expect(parseValues(baseValues).clean_version).toEqual(null)
  })

  it('controls optional_isrc', () => {
    const values = {
      ...baseValues,
      hasOptionalISRC: false,
    }
    expect(parseValues(values).optional_isrc).toBeNull()
  })

  it('does not change optional_isrc', () => {
    const values = {
      ...baseValues,
      hasOptionalISRC: true,
    }
    expect(parseValues(values).optional_isrc).toEqual(baseValues.optional_isrc)
  })

  it('controls previously_released_at', () => {
    const values = {
      ...baseValues,
      previouslyReleased: false,
    }
    expect(parseValues(values).previously_released_at).toBeNull()
  })

  it('does not change previously_released_at', () => {
    const values = {
      ...baseValues,
      previouslyReleased: true,
    }
    expect(parseValues(values).previously_released_at).toEqual(
      values.previously_released_at
    )
  })
})

const startTimeStores = [87]
const time1 = [{ store_id: 87, start_time: 3 }]
const time2 = [{ store_id: 87, start_time: 308 }]
const expectedTime1 = [{ store_id: 87, minutes: 0, seconds: 3 }]
const expectedTime2 = [{ store_id: 87, minutes: 5, seconds: 8 }]

describe('prepareSongStartTimes', () => {
  describe.each`
    time     | expected
    ${time1} | ${expectedTime1}
    ${time2} | ${expectedTime2}
  `('table test', ({ time, expected }) => {
    test(`returns expected`, () => {
      expect(prepareSongStartTimes(startTimeStores, time)).toEqual(expected)
    })
  })
})
