import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import GenresContainer, {
  initGenre,
  initSubgenre,
} from '../../../album_app/components/AlbumFormFields/Genres'

import Providers from '../helpers/Providers'
import genres from '../helpers/genres'
import { albumAppConfig } from '../../specHelper'

Enzyme.configure({ adapter: new Adapter() })

const sampleGenres = [['Alternative'], ['Blues'], ['Comedy']]
const sampleIndianGenres = [['Konkani'], ['Malayalam'], ['Mappila']]

describe('GenresContainer', () => {
  const handleChange = jest.fn()
  let component

  beforeEach(() => {
    component = mount(
      <Providers>
        <GenresContainer errors={{}} handleChange={handleChange} touched={{}} />
      </Providers>
    )
  })

  afterEach(() => {
    handleChange.mockReset()
  })

  describe('render', () => {
    it('renders new form fields', () => {
      expect(component.find('div').first().text()).toMatch('Primary Genre')
      expect(component.find('div').at(1).text()).toMatch('Secondary Genre')
      expect(component.find('select').first().prop('defaultValue')).toBe(
        undefined
      )
      expect(component.find('select').at(1).prop('defaultValue')).toBe(
        undefined
      )
    })

    test.each(sampleGenres)('Primary Genre dropdown has "%s"', (expected) => {
      expect(component.find('div').first().text()).toMatch(expected)
    })

    test.each(sampleIndianGenres)(
      'Primary Genre dropdown missing "%s"',
      (expected) => {
        expect(component.find('div').first().text()).not.toMatch(expected)
      }
    )

    test.each(sampleGenres)('Secondary Genre dropdown has "%s"', (expected) => {
      expect(component.find('div').at(1).text()).toMatch(expected)
    })

    test.each(sampleIndianGenres)(
      'Secondary Genre dropdown missing "%s"',
      (expected) => {
        expect(component.find('div').at(1).text()).not.toMatch(expected)
      }
    )
  })

  describe('conditionally renders Sub Genre fields', () => {
    beforeEach(() => {
      component
        .find('select')
        .first()
        .simulate('change', { target: { value: 40 } })
      component
        .find('select')
        .at(2)
        .simulate('change', { target: { value: 40 } })
    })

    it('renders Primary Sub Genre field', () => {
      expect(component.find('div').at(1).text()).toMatch('Sub Genre')
      expect(handleChange).toBeCalledTimes(2)
    })

    test.each(sampleGenres)(
      'Primary Sub Genre dropdown missing "%s"',
      (expected) => {
        expect(component.find('div').at(1).text()).not.toMatch(expected)
      }
    )

    test.each(sampleIndianGenres)(
      'Primary Sub Genre dropdown has "%s"',
      (expected) => {
        expect(component.find('div').at(1).text()).toMatch(expected)
      }
    )

    test.each(sampleGenres)(
      'Secondary Sub Genre dropdown missing "%s"',
      (expected) => {
        expect(component.find('div').at(3).text()).not.toMatch(expected)
      }
    )

    test.each(sampleIndianGenres)(
      'Secondary Sub Genre dropdown has "%s"',
      (expected) => {
        expect(component.find('div').at(3).text()).toMatch(expected)
      }
    )
  })

  describe('uses saved and site default values', () => {
    describe('uses saved values', () => {
      beforeEach(() => {
        component = mount(
          <Providers>
            <GenresContainer
              errors={{}}
              handleChange={handleChange}
              primary_genre_id={1}
              secondary_genre_id={3}
              touched={{}}
            />
          </Providers>
        )
      })

      it('use saved values for default/pre-selection', () => {
        expect(component.find('select').first().prop('defaultValue')).toBe(1)
        expect(component.find('select').at(1).prop('defaultValue')).toBe(3)
      })
    })

    describe('shows/hides sub genre selectors', () => {
      beforeEach(() => {
        component = mount(
          <Providers
            mockConfigContext={{
              ...albumAppConfig,
              countryWebsite: 'IN',
            }}
          >
            <GenresContainer
              errors={{}}
              handleChange={handleChange}
              primary_genre_id={40}
              secondary_genre_id={40}
              touched={{}}
            />
          </Providers>
        )
      })

      it('closes Sub Genres when not needed', () => {
        expect(component.find('select').first().prop('defaultValue')).toBe(40)
        expect(component.find('select').at(2).prop('defaultValue')).toBe(40)
        expect(component.find('select').length).toBe(4)

        component
          .find('select')
          .first()
          .simulate('change', { target: { value: 3 } })
        expect(component.find('select').length).toBe(3)

        component
          .find('select')
          .at(1)
          .simulate('change', { target: { value: 6 } })
        expect(component.find('select').length).toBe(2)
      })
    })

    describe('#initGenre', () => {
      it('preserves blank', () => {
        expect(initGenre('', 'US', genres)).toEqual('')
      })
      it('falls back to India default', () => {
        expect(initGenre('', 'IN', genres)).toEqual('40')
      })
      it('preserves saved ID', () => {
        expect(initGenre('3', 'US', genres)).toEqual('3')
      })
      it('preserves saved ID when India', () => {
        expect(initGenre('3', 'IN', genres)).toEqual('3')
      })
    })

    describe('#initSubgenre', () => {
      it('preserves blank', () => {
        expect(initSubgenre('', genres)).toEqual('')
      })
      it('returns valid subgenre', () => {
        expect(initSubgenre('77', genres)).toEqual('77')
      })
      it('return blank rather than invalid subgenre', () => {
        expect(initSubgenre('3', genres)).toEqual('')
      })
    })
  })
})
