import esidDataCreator from '../../../album_app/components/data/esidDataCreator'
import { appleArtistResultNIN, spotifyArtistResultNIN } from '../helpers/mam'

/**
 * Mock the search and artist fetch API calls
 */
import { appleFetchArtistRequest } from '../../../album_app/utils/appleAPIRequests'
import { getSpotifyArtistData } from '../../../album_app/utils/spotifyDataService'
jest.mock('../../../album_app/utils/appleAPIRequests')
jest.mock('../../../album_app/utils/spotifyDataService')

describe('esidDataCreator', () => {
  appleFetchArtistRequest.mockImplementation(() =>
    Promise.resolve(appleArtistResultNIN)
  )
  getSpotifyArtistData.mockImplementation(() =>
    Promise.resolve(spotifyArtistResultNIN)
  )

  const creatives = {
    'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e': {
      id: 311,
      name: 'nine inch nails',
      role: 'primary_artist',
      apple: {
        identifier: '107917',
        state: null,
      },
      spotify: {
        identifier: '0X380XXQSNBYuleKzav5UO',
        state: null,
      },
      uuid: 'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e',
    },
  }

  const creativesNew = {
    'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e': {
      id: 311,
      name: 'nine inch nails',
      role: 'primary_artist',
      apple: {
        state: 'new_artist',
      },
      spotify: {
        state: 'new_artist',
      },
      uuid: 'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e',
    },
  }

  const setESIDData = jest.fn()
  const setFetched = jest.fn()

  afterEach(() => {
    setESIDData.mockClear()
    setFetched.mockClear()
  })

  it('fetches and sets when there are identifiers', async () => {
    await esidDataCreator(creatives, setESIDData, setFetched)

    expect(setESIDData).toHaveBeenCalledWith({
      'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e': {
        apple: {
          identifier: '107917',
          image:
            'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/e7/9f/f4/e79ff471-b461-d01d-bd47-6d6a5ec1f1a1/15UMGIM67680.rgb.jpg/64x64bb.jpeg',
          name: 'Nine Inch Nails',
          topAlbum: 'The Downward Spiral',
        },
        id: 311,
        name: 'nine inch nails',
        role: 'primary_artist',
        spotify: {
          identifier: '0X380XXQSNBYuleKzav5UO',
          image:
            'https://i.scdn.co/image/247c355dde622fe0bd7c633ff1667872efd68351',
          latest_album: 'Ghosts V: Together',
          name: 'Nine Inch Nails',
        },
        uuid: 'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e',
      },
    })
    expect(setFetched).toHaveBeenCalledWith(true)
  })

  it("when missing identifiers, passes 'new_artist' through", async () => {
    await esidDataCreator(creativesNew, setESIDData, setFetched)

    expect(setESIDData).toHaveBeenCalledWith({
      'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e': {
        apple: {
          state: 'new_artist',
        },
        id: 311,
        name: 'nine inch nails',
        role: 'primary_artist',
        spotify: {
          state: 'new_artist',
        },
        uuid: 'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e',
      },
    })
    expect(setFetched).toHaveBeenCalledWith(true)
  })

  it('handles fetch errors', async () => {
    appleFetchArtistRequest.mockImplementationOnce(() =>
      Promise.resolve({ error: {} })
    )
    getSpotifyArtistData.mockImplementationOnce(() =>
      Promise.resolve({ error: {} })
    )

    await esidDataCreator(creatives, setESIDData, setFetched)

    expect(setESIDData).toHaveBeenCalledWith({
      'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e': {
        apple: {
          identifier: '107917',
        },
        id: 311,
        name: 'nine inch nails',
        role: 'primary_artist',
        spotify: {
          identifier: '0X380XXQSNBYuleKzav5UO',
        },
        uuid: 'e58db6f1-33b3-b0cc-3e3c-b45d8df1c96e',
      },
    })
    expect(setFetched).toHaveBeenCalledWith(true)
  })
})
