import buildAlbum from '../../../album_app/utils/buildAlbum'

describe('build album', () => {
  describe('creative with no ESIDs', () => {
    it('still adds required attributes', () => {
      const result = buildAlbum()
      const creative = result.creatives[Object.keys(result.creatives)[0]]

      expect(creative.uuid).toBeTruthy()
      expect(creative.apple).toBeTruthy()
      expect(creative.spotify).toBeTruthy()
    })
  })

  describe('creative with ESIDs', () => {
    const apple = {
      identifier: '123',
      service_name: 'apple',
    }
    const spotify = {
      identifier: '456',
      service_name: 'spotify',
    }

    beforeEach(() => {
      const node = document.getElementById('album_app')
      const album = JSON.parse(node.dataset.album)

      album.creatives = [
        {
          external_service_ids: [apple, spotify],
          role: 'primary_artist',
        },
      ]
      node.setAttribute('data-album', JSON.stringify(album))
    })

    it('populates required attributes', () => {
      const result = buildAlbum()
      const creative = result.creatives[Object.keys(result.creatives)[0]]

      expect(creative.apple).toEqual({ identifier: '123' })
      expect(creative.spotify).toEqual({ identifier: '456' })
    })
  })
})
