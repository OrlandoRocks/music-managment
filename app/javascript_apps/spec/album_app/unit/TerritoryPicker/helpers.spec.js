import { determineSelectedCountries } from '../../../../album_app/components/AlbumFormFields/TerritoryPicker/helpers'
import countries from '../../helpers/countries'

const input1 = [['Heard Island and McDonald Islands', 'HM']]
const input2 = [
  ['vatican city', 'va'],
  ['honduras', 'hn'],
  ['hong kong', 'hk'],
  ['hungary', 'hu'],
]

describe('determineSelectedCountries', () => {
  describe('exclusion', () => {
    describe.each`
      selectedItems | expected
      ${input1}     | ${['HM']}
      ${input2}     | ${['va', 'hn', 'hk', 'hu']}
    `(
      'determineSelectedCountries, exclusion, $selectedItems',
      ({ selectedItems, expected }) => {
        test(`should return an array not containing ${expected}`, () => {
          const filterType = 'exclusion'

          expect(
            determineSelectedCountries(countries, filterType, selectedItems)
          ).toEqual(expect.not.arrayContaining(expected))
        })
      }
    )
  })

  describe('inclusion', () => {
    describe.each`
      selectedItems | expected
      ${input1}     | ${['HM']}
      ${input2}     | ${['va', 'hn', 'hk', 'hu']}
    `(
      'determineSelectedCountries, inclusion, $selectedItem, $selectedItems',
      ({ selectedItems, expected }) => {
        test(`should return an array only containing ${expected}`, () => {
          const filterType = 'inclusion'

          expect(
            determineSelectedCountries(countries, filterType, selectedItems)
          ).toEqual(expect.arrayContaining(expected))
        })
      }
    )
  })
})
