import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import AlbumGoliveDate from '../../../album_app/components/AlbumFormFields/AlbumGoliveDate'

import Providers from '../helpers/Providers'

Enzyme.configure({ adapter: new Adapter() })

describe('AlbumGoliveDate', () => {
  let component

  beforeEach(() => {
    const defaultValue = {
      hour: '12',
      min: '04',
      meridian: 'am',
    }

    component = mount(
      <Providers>
        <AlbumGoliveDate defaultValue={defaultValue} />
      </Providers>
    )
  })

  it('renders the hour selector', () => {
    expect(component.find('#select-hours').props().defaultValue).toEqual('12')
  })

  it('renders the minute selector', () => {
    expect(component.find('#select-minutes').props().defaultValue).toEqual('04')
  })

  it('renders the meridian selector', () => {
    expect(component.find('#select-meridian').props().defaultValue).toEqual(
      'am'
    )
  })
})
