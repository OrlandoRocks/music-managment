import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { AlbumModal } from '../../../album_app/components/AlbumModal'

Enzyme.configure({ adapter: new Adapter() })

function childComponent({
  modalActionCallback,
  negativeAction,
  positiveAction,
}) {
  return (
    <>
      <button onClick={() => modalActionCallback(negativeAction)}>
        Negative
      </button>
      <button onClick={() => modalActionCallback(positiveAction)}>
        Positive
      </button>
    </>
  )
}

describe('render', () => {
  let component
  const setShow = jest.fn()
  const positiveAction = jest.fn()
  const negativeAction = jest.fn()
  const headerMessage = 'Header Message'
  const contentLabel = 'ARIA label message.'

  beforeEach(() => {
    component = mount(
      <AlbumModal
        contentLabel={contentLabel}
        BodyComponent={childComponent}
        customStyles={{
          content: { fontWeight: 700 },
          overlay: { background: 'red' },
        }}
        headerMessage={headerMessage}
        negativeAction={negativeAction}
        onRequestClose={() => setShow(false)}
        positiveAction={positiveAction}
        setShow={setShow}
        show={true}
      />
    )
  })

  it('renders', () => {
    expect(component.text()).toMatch(headerMessage)
    expect(component.find('button')).toHaveLength(2)
  })

  it('button1 calls positive action and closes', () => {
    component.find('button').first().simulate('click')
    expect(negativeAction).toHaveBeenCalledTimes(1)
    expect(setShow).toHaveBeenCalled()
  })

  it('button2 calls negative action and closes', () => {
    component.find('button').at(1).simulate('click')
    expect(positiveAction).toHaveBeenCalledTimes(1)
    expect(setShow).toHaveBeenCalled()
  })

  it('applies custom style overrides', () => {
    expect(component.html()).toMatch(
      'album-modal-overlay" style="background: red;'
    )
    expect(component.html()).toMatch('div style="font-weight: 700;')
  })

  it('closes on ESC key', () => {
    component.simulate('keypress', { key: 'Escape' })
    expect(setShow).toHaveBeenCalled()
  })

  it('closes on clickaway', () => {
    window.document.body.dispatchEvent(new Event('click'))
    expect(setShow).toHaveBeenCalled()
  })

  it('applies ARIA label via contentLabel', () => {
    expect(component.html()).toMatch('aria-label="ARIA label message.')
  })

  it("doesn't render when show is false", () => {
    component.setProps({ show: false })
    expect(component.html()).toBeNull
  })
})
