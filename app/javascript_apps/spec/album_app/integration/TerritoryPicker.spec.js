import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../album_app/components/AlbumForm'
import Providers from '../helpers/Providers'
import { validInitialValues } from '../helpers/form'

const mockSubmitHandler = jest.fn()
function getSubmission(mockSubmitHandler) {
  return mockSubmitHandler.mock.calls[0][0].values.selected_countries
}

/**
 * This suite is slow because it fires a lot of events, and JSDOM is slow in that regard.
 * userEvent was found to be about as fast as fireEvent in some comparison testing.
 * All commands were benchmarked and the vast majority of time is spent firing events. There's
 * currently no faster way to fire events in JSDOM. 3/10/21
 */

describe('TerritoryPicker', function () {
  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  it('excludes a country from submission', async () => {
    const {
      getByLabelText,
      getByPlaceholderText,
      getByTestId,
      getByText,
      queryByText,
    } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const noRadio = getByTestId(/container-territory-restrictions/i)
    userEvent.click(noRadio)

    const exclusion = getByTestId(/territory-restrictions-exclusion/i)
    userEvent.click(exclusion)
    expect(queryByText(/but exclude/i)).toBeFalsy()
    expect(queryByText(/remove all/i)).toBeFalsy()
    expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

    const input = getByPlaceholderText(/search country/i)
    userEvent.type(input, 'cam')
    userEvent.click(getByText(/cambodia/i))
    expect(getByText(/but exclude/i)).toBeTruthy()
    expect(getByText(/remove all/i)).toBeTruthy()
    expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()

    userEvent.click(getByLabelText(/overview/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))
    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler)).toBeInstanceOf(Array)
      expect(getSubmission(mockSubmitHandler)).not.toContain('KH')
    })
  })

  it('excludes two countries from submission', async () => {
    const {
      getByLabelText,
      getByPlaceholderText,
      getByTestId,
      getByText,
      queryByText,
    } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const noRadio = getByTestId(/container-territory-restrictions/i)
    userEvent.click(noRadio)

    const exclusion = getByTestId(/territory-restrictions-exclusion/i)
    userEvent.click(exclusion)

    expect(queryByText(/but exclude/i)).toBeFalsy()
    expect(queryByText(/remove all/i)).toBeFalsy()
    expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()
    expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()

    const input = getByPlaceholderText(/search country/i)
    userEvent.type(input, 'cam')
    userEvent.type(input, '{arrowdown}')
    userEvent.type(input, '{enter}')

    userEvent.type(input, 'mex')
    userEvent.type(input, '{arrowdown}')
    userEvent.type(input, '{enter}')

    expect(getByText(/but exclude/i)).toBeTruthy()
    expect(getByText(/remove all/i)).toBeTruthy()
    expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
    expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()

    userEvent.click(getByLabelText(/overview/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))
    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler)).not.toContain('KH')
      expect(getSubmission(mockSubmitHandler)).not.toContain('MX')
    })
  })

  it('submits one country', async () => {
    const {
      getAllByText,
      getByLabelText,
      getByPlaceholderText,
      getByTestId,
      getByText,
      queryByText,
    } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const noRadio = getByTestId(/container-territory-restrictions/i)
    userEvent.click(noRadio)

    const inclusion = getByTestId(/territory-restrictions-inclusion/i)
    userEvent.click(inclusion)

    expect(queryByText(/will be sold in/i)).toBeFalsy()
    expect(queryByText(/remove all/i)).toBeFalsy()
    expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()

    const input = getByPlaceholderText(/search country/i)

    userEvent.type(input, 'mex')
    userEvent.type(input, '{arrowdown}')
    userEvent.type(input, '{enter}')

    expect(getAllByText(/will be sold in/i)).toBeTruthy()
    expect(getByText(/remove all/i)).toBeTruthy()
    expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()

    userEvent.click(getByLabelText(/overview/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))
    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler)).toContain('MX')
      expect(getSubmission(mockSubmitHandler).length).toEqual(1)
    })
  })

  it('submits two countries', async () => {
    const {
      getAllByText,
      getByLabelText,
      getByPlaceholderText,
      getByTestId,
      getByText,
      queryByText,
    } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const noRadio = getByTestId(/container-territory-restrictions/i)
    userEvent.click(noRadio)

    const inclusion = getByTestId(/territory-restrictions-inclusion/i)
    userEvent.click(inclusion)

    expect(queryByText(/will be sold in/i)).toBeFalsy()
    expect(queryByText(/remove all/i)).toBeFalsy()
    expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()
    expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

    const input = getByPlaceholderText(/search country/i)

    userEvent.type(input, 'mex')
    userEvent.type(input, '{arrowdown}')
    userEvent.type(input, '{enter}')

    userEvent.type(input, 'cam')
    userEvent.type(input, '{arrowdown}')
    userEvent.type(input, '{enter}')

    expect(getAllByText(/will be sold in/i)).toBeTruthy()
    expect(getByText(/remove all/i)).toBeTruthy()
    expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
    expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()

    userEvent.click(getByLabelText(/overview/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))
    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler)).toContain('MX')
      expect(getSubmission(mockSubmitHandler)).toContain('KH')
      expect(getSubmission(mockSubmitHandler).length).toEqual(2)
    })
  })

  context('Adding and removing items via dropdown', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)
      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)
      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      // remove Cambodia by selecting it again
      userEvent.type(input, 'cam')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toContain('MX')
        expect(getSubmission(mockSubmitHandler)).toContain('PK')
        expect(getSubmission(mockSubmitHandler).length).toEqual(2)
      })
    })

    it('submits all countries', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()

      // remove Cambodia by selecting it again
      userEvent.type(input, 'cam')
      userEvent.type(input, '{enter}')
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

      // re-open the dropdown by typing
      userEvent.type(input, 'ma')

      // the picker mode is still 'inclusion'
      const includeButtons = getAllByText(/include/i, { selector: 'button' })
      expect(includeButtons.length).toEqual(30)

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).length).toEqual(228)
      })
    })
  })

  context('Adding and removing items via button/pills', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      // remove Cambodia by clicking its button/pill
      userEvent.click(getByText(/cambodia/i, { selector: 'button' }))

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toContain('MX')
        expect(getSubmission(mockSubmitHandler)).toContain('PK')
        expect(getSubmission(mockSubmitHandler).length).toEqual(2)
      })
    })

    it('submits all countries', async () => {
      const {
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      // remove countries by clicking the button/pills
      userEvent.click(getByText(/mexico/i, { selector: 'button' }))
      userEvent.click(getByText(/cambodia/i, { selector: 'button' }))
      userEvent.click(getByText(/pakistan/i, { selector: 'button' }))

      expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()
      expect(queryByText(/pakistan/i, { selector: 'button' })).toBeFalsy()

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toBeInstanceOf(Array)
        expect(getSubmission(mockSubmitHandler).length).toEqual(228)
      })
    })
  })

  describe('Add by mouse, then remove all', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryAllByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const exclusion = getByTestId(/territory-restrictions-exclusion/i)
      userEvent.click(exclusion)

      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'ma')
      userEvent.click(getByText(/macao/i))
      userEvent.click(getByText(/macedon/i))
      userEvent.click(getByText(/madaga/i))
      userEvent.click(getByText(/malawi/i))

      // removal button/pills
      const selected = getAllByText(/^ma/i, { selector: 'button' })
      expect(selected.length).toEqual(4)

      userEvent.click(getByText(/remove all/i))

      const newSelected = queryAllByText(/^ma/i, { selector: 'button' })
      expect(newSelected.length).toEqual(0)

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).not.toContain('KH')
      })
    })
  })

  context('Returning to Overview after selection', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        getAllByTestId,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)
      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)
      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      // remove Cambodia by selecting it again
      userEvent.type(input, 'cam')
      userEvent.type(input, '{enter}')
      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

      const pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      const overviewBackButton = getByLabelText(/back to form/i)
      userEvent.click(overviewBackButton)

      const radios = getAllByTestId(/^territory-restrictions/i)
      expect(radios.length).toEqual(3)
      expect(radios[0].checked).toBeFalsy()
      expect(radios[1].checked).toBeFalsy()
      expect(radios[2].checked).toBeTruthy()

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toContain('MX')
        expect(getSubmission(mockSubmitHandler)).toContain('PK')
        expect(getSubmission(mockSubmitHandler).length).toEqual(2)
      })
    })

    it('becomes worldwide, submits all countries', async () => {
      const {
        getAllByTestId,
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()

      // remove Cambodia by selecting it again
      userEvent.type(input, 'cam')
      userEvent.type(input, '{enter}')
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()

      // re-open the dropdown by typing
      userEvent.type(input, 'ma')

      // the picker mode is still 'inclusion'
      const includeButtons = getAllByText(/include/i, { selector: 'button' })
      expect(includeButtons.length).toEqual(30)

      const pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      const overviewBackButton = getByLabelText(/back to form/i)
      userEvent.click(overviewBackButton)

      // the overview is set to worldwide
      const radios = getAllByTestId(/^territory-restrictions/i)
      expect(radios.length).toEqual(3)
      expect(radios[0].checked).toBeTruthy()
      expect(radios[1].checked).toBeFalsy()
      expect(radios[2].checked).toBeFalsy()

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).length).toEqual(228)
      })
    })
  })

  context('Preserves selection for editing, open modal', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)
      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)
      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      const pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      const editButton = getAllByText(/edit list/i)[1]
      userEvent.click(editButton)

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()
      expect(getAllByText(/.*x$/i).length).toEqual(3)

      userEvent.click(getByLabelText(/open/i))
      expect(getAllByText(/included/i).length).toEqual(3)

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toContain('MX')
        expect(getSubmission(mockSubmitHandler)).toContain('KH')
        expect(getSubmission(mockSubmitHandler)).toContain('PK')
        expect(getSubmission(mockSubmitHandler).length).toEqual(3)
      })
    })

    it('excludes two countries from submission', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const exclusion = getByTestId(/territory-restrictions-exclusion/i)
      userEvent.click(exclusion)

      expect(queryByText(/but exclude/i)).toBeFalsy()
      expect(queryByText(/remove all/i)).toBeFalsy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()
      expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/but exclude/i)).toBeTruthy()
      expect(getByText(/remove all/i)).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()

      const pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      // inside modal
      const editButton = getAllByText(/edit list/i)[1]
      userEvent.click(editButton)

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getAllByText(/.*x$/i).length).toEqual(2)

      userEvent.click(getByLabelText(/open/i))
      expect(getAllByText(/excluded/i).length).toEqual(2)

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).not.toContain('KH')
        expect(getSubmission(mockSubmitHandler)).not.toContain('MX')
      })
    })
  })

  context('Preserves selection for editing after closed modal', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('submits only two countries', async () => {
      const {
        findByText,
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
        getByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)
      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)
      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      userEvent.type(input, 'pak')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()

      userEvent.click(getByLabelText(/overview/i))
      userEvent.click(getByLabelText(/back/i))

      // form-level territory edit button
      const editButton = await findByText(/edit list/i)
      userEvent.click(editButton)

      expect(getByText(/mexico/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/cambodia/i, { selector: 'button' })).toBeTruthy()
      expect(getByText(/pakistan/i, { selector: 'button' })).toBeTruthy()
      expect(getAllByText(/.*x$/i).length).toEqual(3)

      userEvent.click(getByLabelText(/open/i))
      expect(getAllByText(/included/i).length).toEqual(3)

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler)).toContain('MX')
        expect(getSubmission(mockSubmitHandler)).toContain('KH')
        expect(getSubmission(mockSubmitHandler)).toContain('PK')
        expect(getSubmission(mockSubmitHandler).length).toEqual(3)
      })
    })
  })

  describe('Finish adding button', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('shows/hides depending on inclusions', async () => {
      const { getByPlaceholderText, getByTestId, queryByText } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const finishAddingButton = queryByText(/finish adding/i)
      expect(finishAddingButton).toBeDisabled()

      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      expect(finishAddingButton).not.toBeDisabled()

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      expect(finishAddingButton).toBeDisabled()
    })

    it('shows/hides depending on exclusions', async () => {
      const { getByPlaceholderText, getByTestId, queryByText } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const exclusion = getByTestId(/territory-restrictions-exclusion/i)
      userEvent.click(exclusion)

      expect(queryByText(/but exclude/i)).toBeFalsy()
      expect(queryByText(/remove all/i)).toBeFalsy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()
      expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()

      const finishAddingButton = queryByText(/finish adding/i)
      expect(finishAddingButton).toBeDisabled()

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      await waitFor(() => {
        expect(finishAddingButton).not.toBeDisabled()
      })

      userEvent.type(input, 'cam')
      userEvent.type(input, '{enter}')
      expect(finishAddingButton).toBeDisabled()
    })

    it('closes the modal', async () => {
      const {
        getByPlaceholderText,
        getByTestId,
        queryByPlaceholderText,
        queryByTestId,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noRadio = getByTestId(/container-territory-restrictions/i)
      userEvent.click(noRadio)

      const exclusion = getByTestId(/territory-restrictions-exclusion/i)
      userEvent.click(exclusion)

      expect(queryByText(/but exclude/i)).toBeFalsy()
      expect(queryByText(/remove all/i)).toBeFalsy()
      expect(queryByText(/cambodia/i, { selector: 'button' })).toBeFalsy()
      expect(queryByText(/mexico/i, { selector: 'button' })).toBeFalsy()

      const finishAddingButton = queryByText(/finish adding/i)
      expect(finishAddingButton).toBeDisabled()

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'cam')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      await waitFor(() => {
        expect(finishAddingButton).not.toBeDisabled()
      })

      userEvent.click(finishAddingButton)

      await waitFor(() => {
        expect(queryByPlaceholderText(/search country/i)).toBeFalsy()
        expect(queryByTestId(/territory-restrictions-exclusion/i)).toBeFalsy()
      })
    })
  })

  describe('radio states', function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('toggles form-level radios', async () => {
      const {
        getAllByTestId,
        getByLabelText,
        getByPlaceholderText,
        getByTestId,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const radios = getAllByTestId(/^container-territory/i)
      expect(radios.length).toEqual(2)
      expect(radios[0].checked).toBeFalsy()
      expect(radios[1].checked).toBeTruthy()

      userEvent.click(radios[0])

      const inclusion = getByTestId(/territory-restrictions-inclusion/i)
      userEvent.click(inclusion)

      const input = getByPlaceholderText(/search country/i)

      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')
      userEvent.click(getByLabelText(/overview/i))

      expect(radios[0].checked).toBeTruthy()
      expect(radios[1].checked).toBeFalsy()
    })

    it('toggles Overview radios, resets form', async () => {
      const {
        getAllByTestId,
        getByLabelText,
        getByPlaceholderText,
        queryAllByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      // enter overview
      const radios = getAllByTestId(/^container-territory/i)
      userEvent.click(radios[0])

      // overview radios
      let pickerRadios = getAllByTestId(/^territory-restrictions/i)
      expect(pickerRadios[0].checked).toBeTruthy()
      expect(pickerRadios[1].checked).toBeFalsy()
      expect(pickerRadios[2].checked).toBeFalsy()

      // inclusion
      userEvent.click(pickerRadios[2])

      const input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      let pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      // inclusion is selected
      pickerRadios = getAllByTestId(/^territory-restrictions/i)
      expect(pickerRadios[0].checked).toBeFalsy()
      expect(pickerRadios[1].checked).toBeFalsy()
      expect(pickerRadios[2].checked).toBeTruthy()

      // exclusion
      userEvent.click(pickerRadios[1])

      // the previous inclusion is wiped and has not become an exclusion
      expect(queryAllByText(/excluded/i).length).toEqual(0)

      // having selected nothing...
      pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      // back to worldwide
      pickerRadios = getAllByTestId(/^territory-restrictions/i)
      expect(pickerRadios[0].checked).toBeTruthy()
      expect(pickerRadios[1].checked).toBeFalsy()
      expect(pickerRadios[2].checked).toBeFalsy()
    })
  })

  describe("summary components' visibility", function () {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('shows/hides the summaries depending on picker type and selection', async () => {
      const {
        getAllByTestId,
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        queryByText,
      } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      // enter overview
      const radios = getAllByTestId(/^container-territory/i)
      userEvent.click(radios[0])

      // the summaries
      expect(queryByText(/will be sold/i)).toBeFalsy()
      expect(queryByText(/won't be sold/i)).toBeFalsy()

      // overview radios
      let pickerRadios = getAllByTestId(/^territory-restrictions/i)
      expect(pickerRadios[0].checked).toBeTruthy()
      expect(pickerRadios[1].checked).toBeFalsy()
      expect(pickerRadios[2].checked).toBeFalsy()

      // inclusion
      userEvent.click(pickerRadios[2])

      let input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      let pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      let mexico = getAllByText(/mexico/i)

      // from the overview, we can query the overview's summary and the form's (behind modal) summary
      expect(mexico.length).toEqual(2)
      expect(getAllByText(/will be sold/i).length).toEqual(2)

      // exclusion
      pickerRadios = getAllByTestId(/^territory-restrictions/i)
      userEvent.click(pickerRadios[1])

      input = getByPlaceholderText(/search country/i)
      userEvent.type(input, 'mex')
      userEvent.type(input, '{arrowdown}')
      userEvent.type(input, '{enter}')

      pickerBackButton = getByLabelText(/back to overview/i)
      userEvent.click(pickerBackButton)

      // from the overview, we can query the overview's summary and the form's (behind modal) summary
      mexico = getAllByText(/mexico/i)
      expect(mexico.length).toEqual(2)
      expect(getAllByText(/won't be sold/i).length).toEqual(2)

      // back to worldwide
      pickerRadios = getAllByTestId(/^territory-restrictions/i)
      userEvent.click(pickerRadios[0])

      expect(queryByText(/will be sold/i)).toBeFalsy()
      expect(queryByText(/won't be sold/i)).toBeFalsy()
    })
  })

  describe('form initialization', () => {
    afterEach(() => {
      mockSubmitHandler.mockClear()
    })

    it('inits to exclude when album has more than "countries/2" selected_countries', async () => {
      const largeSlice = validInitialValues().selected_countries.slice(5)
      const expectedExclusions = validInitialValues().selected_countries.slice(
        0,
        5
      )

      const { getByText, getAllByText, queryByText } = render(
        <Providers>
          <AlbumForm
            album={{ ...validInitialValues(), selected_countries: largeSlice }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      // the summaries
      expect(queryByText(/will be sold/i)).toBeFalsy()
      expect(queryByText(/won't be sold/i)).toBeTruthy()

      // enter directyly to picker
      userEvent.click(getByText(/edit list/i))

      expect(getAllByText(/.*x$/i).length).toEqual(expectedExclusions.length)
      expect(getByText(/but exclude/)).toBeTruthy()
    })

    it('inits to include when album has less than "countries/2" selected_countries', async () => {
      const smallSlice = validInitialValues().selected_countries.slice(200)

      const { getByText, getAllByText, queryByText } = render(
        <Providers>
          <AlbumForm
            album={{ ...validInitialValues(), selected_countries: smallSlice }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      // the summaries
      expect(queryByText(/will be sold/i)).toBeTruthy()
      expect(queryByText(/won't be sold/i)).toBeFalsy()

      // enter directly to picker
      userEvent.click(getByText(/edit list/i))

      expect(getAllByText(/.*x$/i).length).toEqual(smallSlice.length)
      expect(getAllByText(/sold in/)).toBeTruthy()
    })

    it('inits to include, and is wiped by change to worldwide', async () => {
      const smallSlice = validInitialValues().selected_countries.slice(200)

      const { getAllByTestId, getByText, queryByText } = render(
        <Providers>
          <AlbumForm
            album={{ ...validInitialValues(), selected_countries: smallSlice }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      // the summaries
      expect(queryByText(/will be sold/i)).toBeTruthy()
      expect(queryByText(/won't be sold/i)).toBeFalsy()

      const radios = getAllByTestId(/^container-territory/i)
      userEvent.click(radios[1])

      userEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).length).toEqual(228)
      })
    })
  })
})
