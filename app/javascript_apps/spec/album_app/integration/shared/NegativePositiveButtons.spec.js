import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { NegativePositiveButtons } from '../../../../album_app/components/shared/NegativePositiveButtons'

Enzyme.configure({ adapter: new Adapter() })

describe('render', () => {
  let component
  const handleNegative = jest.fn()
  const handlePositive = jest.fn()
  const negativeText = 'Negative Message'
  const positiveText = 'Positive Message'
  const negativeStyle = { background: 'red' }
  const positiveStyle = { background: 'blue' }

  beforeEach(() => {
    component = mount(
      <NegativePositiveButtons
        handleNegative={handleNegative}
        handlePositive={handlePositive}
        negativeText={negativeText}
        positiveText={positiveText}
        negativeStyle={negativeStyle}
        positiveStyle={positiveStyle}
      />
    )
  })

  it('renders', () => {
    expect(component.text()).toMatch(positiveText)
    expect(component.text()).toMatch(negativeText)
  })

  it('actions', () => {
    component.find('button').first().simulate('click')
    expect(handlePositive).toHaveBeenCalledTimes(1)
    component.find('button').at(1).simulate('click')
    expect(handleNegative).toHaveBeenCalledTimes(1)
  })

  it('applies custom style overrides', () => {
    expect(component.html()).toMatch('style="background: red;')
    expect(component.html()).toMatch('style="background: blue;')
  })
})
