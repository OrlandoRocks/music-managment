import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import * as helpers from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import mockDistributionContext from '../helpers/mockDistributionContext'
import {
  atLeastOneStoreSelected,
  noStoresSelected,
  songsCompletedAndProgressLevelCompleted,
  progressLevelCompleted,
  songsCompleted,
} from '../helpers/variantMockDistributionContexts'

const mockUseSongsData = jest.fn()
helpers.useSongsData = mockUseSongsData

import * as tcFetch from '../../../album_app/utils/fetch'
const mockDestroy = jest.fn(() => ({ ok: true }))
const mockPost = jest.fn(() => ({ ok: true }))
const mockPut = jest.fn(() => ({ ok: true }))
tcFetch.destroy = mockDestroy
tcFetch.post = mockPost
tcFetch.put = mockPut

const mockSongSubmitHandler = jest.fn()

const mockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
}

xdescribe('Distribution Summary', function () {
  afterEach(() => {
    mockDestroy.mockClear()
    mockPost.mockClear()
    mockPut.mockClear()
    mockSongSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers
        mockConfigContext={mockConfigContext}
        mockDistributionContext={mockDistributionContext}
      >
        <DistributionStepper
          mocks={{
            songSubmitHandler: mockSongSubmitHandler,
          }}
        />
      </Providers>
    )

  describe('Song Deletion', function () {
    it('removes the track, deletes the song, and persists the new track order', async () => {
      const {
        findByText,
        getAllByLabelText,
        getByTestId,
        getByText,
        queryByText,
      } = setup()

      expect(getByText(/song 1/i)).toBeTruthy()
      expect(getByText(/song 2/i)).toBeTruthy()

      userEvent.click(getAllByLabelText(/delete/i)[0])
      await findByText(/are you sure/i)
      await act(async () => userEvent.click(getByTestId(/positive button/i)))
      await waitFor(() => expect(queryByText(/are you sure/i)).toBeFalsy())

      expect(mockDestroy).toHaveBeenCalledWith('/api/backstage/song_data/3')
      await waitFor(() => {
        expect(mockPut).toHaveBeenCalledWith('/api/backstage/albums/198', {
          album: { song_order: [4] },
        })
      })

      expect(queryByText(/song 1/i)).toBeFalsy()
      expect(getByText(/song 2/i)).toBeTruthy()
    })

    it('does not delete when cancel is clicked', async () => {
      const {
        findByText,
        getAllByLabelText,
        getByTestId,
        getByText,
        queryByText,
      } = setup()

      expect(getByText(/song 1/i)).toBeTruthy()
      expect(getByText(/song 2/i)).toBeTruthy()

      userEvent.click(getAllByLabelText(/delete/i)[0])
      await findByText(/are you sure/i)
      await act(async () => userEvent.click(getByTestId(/negative button/i)))
      await waitFor(() => expect(queryByText(/are you sure/i)).toBeFalsy())

      expect(mockDestroy).not.toHaveBeenCalled()
      expect(mockPut).not.toHaveBeenCalledWith()

      expect(getByText(/song 1/i)).toBeTruthy()
      expect(getByText(/song 2/i)).toBeTruthy()
    })
  })

  describe('Song Edit', function () {
    const setup = () =>
      render(
        <Providers
          mockConfigContext={mockConfigContext}
          mockDistributionContext={mockDistributionContext}
        >
          <DistributionStepper />
        </Providers>
      )

    it('Edits track 1', async () => {
      const { getAllByLabelText, getByText } = setup()

      expect(getByText(/song 1/i)).toBeTruthy()
      expect(getByText(/song 2/i)).toBeTruthy()

      userEvent.click(getAllByLabelText(/edit song/i)[0])
      const song1FileName = 'file_example_WAV_1MG2.wav'
      expect(getByText(song1FileName)).toBeTruthy()
    })

    it('Edits track 2', async () => {
      const { getAllByLabelText, getByText } = setup()

      expect(getByText(/song 1/i)).toBeTruthy()
      expect(getByText(/song 2/i)).toBeTruthy()

      userEvent.click(getAllByLabelText(/edit song/i)[1])
      const song2FileName = 'file_example_WAV_1MG2-2.wav'
      expect(getByText(song2FileName)).toBeTruthy()
    })
  })

  describe('Store Summary', function () {
    context('when at least one store is selected', function () {
      const setup = () =>
        render(
          <Providers
            mockConfigContext={mockConfigContext}
            mockDistributionContext={atLeastOneStoreSelected}
          >
            <DistributionStepper />
          </Providers>
        )

      it('shows store icons', function () {
        const { queryByTestId } = setup()

        const storeIcon = queryByTestId('store-icon')

        expect(storeIcon).toBeInTheDocument()
      })
    })

    context('when no stores are selected', function () {
      const setup = () =>
        render(
          <Providers
            mockConfigContext={mockConfigContext}
            mockDistributionContext={noStoresSelected}
          >
            <DistributionStepper />
          </Providers>
        )

      it('shows no store icons', function () {
        const { queryByTestId } = setup()

        const storeIcon = queryByTestId('store-icon')

        expect(storeIcon).not.toBeInTheDocument()
      })
    })
  })

  describe('Add to Cart Button', function () {
    context(
      'when distributionProgressLevel is 4 and songsComplete is true',
      function () {
        const setup = () =>
          render(
            <Providers
              mockConfigContext={mockConfigContext}
              mockDistributionContext={songsCompletedAndProgressLevelCompleted}
            >
              <DistributionStepper />
            </Providers>
          )

        it('is enabled', async () => {
          const { getByText } = setup()

          const addToCartButton = getByText(/Add to Cart/i).closest('button')

          expect(addToCartButton).not.toBeDisabled()
        })
      }
    )

    context(
      'when distributionProgressLevel is 4 and songsComplete is false',
      function () {
        const setup = () =>
          render(
            <Providers
              mockConfigContext={mockConfigContext}
              mockDistributionContext={progressLevelCompleted}
            >
              <DistributionStepper />
            </Providers>
          )

        it('is enabled', async () => {
          const { getByText } = setup()

          const addToCartButton = getByText(/Add to Cart/i).closest('button')

          expect(addToCartButton).toBeDisabled()
        })
      }
    )

    context(
      'when distributionProgressLevel is not 4 and songsComplete is true',
      function () {
        const setup = () =>
          render(
            <Providers
              mockConfigContext={mockConfigContext}
              mockDistributionContext={songsCompleted}
            >
              <DistributionStepper />
            </Providers>
          )

        it('is enabled', async () => {
          const { getByText } = setup()

          const addToCartButton = getByText(/Add to Cart/i).closest('button')

          expect(addToCartButton).toBeDisabled()
        })
      }
    )

    context(
      'when distributionProgressLevel is not 4 and songsComplete is false',
      function () {
        const setup = () =>
          render(
            <Providers
              mockConfigContext={mockConfigContext}
              mockDistributionContext={mockDistributionContext}
            >
              <DistributionStepper />
            </Providers>
          )

        it('is enabled', async () => {
          const { getByText } = setup()

          const addToCartButton = getByText(/Add to Cart/i).closest('button')

          expect(addToCartButton).toBeDisabled()
        })
      }
    )
  })
})
