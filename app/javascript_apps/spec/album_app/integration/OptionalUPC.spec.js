import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../album_app/components/AlbumForm'
import { validInitialValues } from '../helpers/form'
import Providers from '../helpers/Providers'

describe('OptionalUPC', function () {
  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return (
      mockSubmitHandler.mock.calls[0] &&
      mockSubmitHandler.mock.calls[0][0].values
    )
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  describe('validation', function () {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

    it('passes when the length is 12', async () => {
      const { getByLabelText, getByText } = setup()

      const expected = '123456789012'
      const input = getByLabelText(/UPC/i)
      userEvent.type(input, expected)

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getSubmission(mockSubmitHandler).optional_upc_number).toEqual(
        expected
      )
    })

    it('passes when the length is 13', async () => {
      const { getByLabelText, getByText } = setup()

      const expected = '1234567890123'
      const input = getByLabelText(/UPC/i)
      userEvent.type(input, expected)

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getSubmission(mockSubmitHandler).optional_upc_number).toEqual(
        expected
      )
    })

    describe('when the value starts with 0', function () {
      it('passes when the length is 12', async () => {
        const { getByLabelText, getByText } = setup()

        const expected = '012345678901'
        const input = getByLabelText(/UPC/i)
        userEvent.type(input, expected)

        await act(async () => {
          userEvent.click(getByText(/save album/i))
        })

        expect(getSubmission(mockSubmitHandler).optional_upc_number).toEqual(
          expected
        )
      })

      it('passes when the length is 13', async () => {
        const { getByLabelText, getByText } = setup()

        const expected = '0123456789012'
        const input = getByLabelText(/UPC/i)
        userEvent.type(input, expected)

        await act(async () => {
          userEvent.click(getByText(/save album/i))
        })

        expect(getSubmission(mockSubmitHandler).optional_upc_number).toEqual(
          expected
        )
      })
    })

    it('errors for max length', async () => {
      const { getByLabelText, getByText } = setup()

      const input = getByLabelText(/UPC/i)
      userEvent.type(input, '12345678901234')

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getByText(/13 digits/i)).toBeTruthy()
      expect(getSubmission(mockSubmitHandler)).toBeUndefined()
    })

    it('errors for min length', async () => {
      const { getByLabelText, getByText } = setup()

      const input = getByLabelText(/UPC/i)
      userEvent.type(input, '1234')

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getByText(/13 digits/i)).toBeTruthy()
      expect(getSubmission(mockSubmitHandler)).toBeUndefined()
    })

    it('errors for not a number', async () => {
      const { getByLabelText, getByText } = setup()

      const input = getByLabelText(/UPC/i)
      userEvent.type(input, 'abc')

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getByText(/must be a number/i)).toBeTruthy()
      expect(getSubmission(mockSubmitHandler)).toBeUndefined()
    })

    it('catches lazy parseInt behavior', async () => {
      const { getByLabelText, getByText } = setup()

      const input = getByLabelText(/UPC/i)
      userEvent.type(input, '1234abc')

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getByText(/must be a number/i)).toBeTruthy()
      expect(getSubmission(mockSubmitHandler)).toBeUndefined()
    })
  })
})
