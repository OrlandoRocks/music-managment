import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { fireEvent, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import * as helpers from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import mockDistributionContext from '../helpers/mockDistributionContext'
const mockUseSongsData = jest.fn()
helpers.useSongsData = mockUseSongsData

import * as tcFetch from '../../../album_app/utils/fetch'

const mockDestroy = jest.fn(() => ({ ok: true }))
const mockPost = jest.fn(() => ({ ok: true }))
const mockPut = jest.fn(() => ({ ok: true }))
tcFetch.destroy = mockDestroy
tcFetch.post = mockPost
tcFetch.put = mockPut

const mockSongSubmitHandler = jest.fn()

const mockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
}

xdescribe('Song Form 1', function () {
  afterEach(() => {
    mockDestroy.mockClear()
    mockPost.mockClear()
    mockPut.mockClear()
    mockSongSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers
        mockConfigContext={mockConfigContext}
        mockDistributionContext={mockDistributionContext}
      >
        <DistributionStepper
          mocks={{
            songSubmitHandler: mockSongSubmitHandler,
          }}
        />
      </Providers>
    )

  describe('Add Song', function () {
    it('Launches Song Form 1 with incremented track number and submits values', async () => {
      const {
        findByLabelText,
        findByText,
        getByLabelText,
        getByTestId,
        getByText,
      } = setup()

      userEvent.click(getByText(/add song/i))

      await findByLabelText(/song title/i)
      expect(getByText(/select/i)).toBeTruthy()
      fireEvent.change(getByLabelText(/song title/i), {
        target: { value: 'edited name' },
      })

      const file = new File(['mysong'], 'mysong.mp3', { type: 'audio/mp3' })
      const input = getByTestId('upload-input')
      userEvent.upload(input, file)
      await findByText(/mysong/i)

      userEvent.click(getByLabelText(/submit/i))

      await waitFor(() => {
        expect(
          mockSongSubmitHandler.mock.calls[0][0].values.asset_filename
        ).toEqual('mysong.mp3')
        expect(mockSongSubmitHandler.mock.calls[0][0].values.name).toEqual(
          'edited name'
        )
        expect(
          mockSongSubmitHandler.mock.calls[0][0].values.track_number
        ).toEqual(3)
      })
    })
  })
})
