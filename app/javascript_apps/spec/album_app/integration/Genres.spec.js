import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../album_app/components/AlbumForm'
import { validInitialValues } from '../helpers/form'
import Providers from '../helpers/Providers'

describe('Genres', function () {
  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return mockSubmitHandler.mock.calls[0][0].values
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  describe('validation', function () {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

    it('errors when missing required Indian primary subgenre', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByText,
        queryByLabelText,
      } = setup()

      expect(queryByLabelText(/sub genre/i)).toBeFalsy()

      const primaryGenreSelect = getByLabelText(/primary genre/i)
      userEvent.selectOptions(primaryGenreSelect, 'Indian')

      expect(getByLabelText(/sub genre/i)).toBeTruthy()

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getAllByText(/required field/i).length).toEqual(1)
    })

    it('errors when missing required Indian secondary subgenre', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByText,
        queryByLabelText,
      } = setup()

      expect(queryByLabelText(/sub genre/i)).toBeFalsy()

      const secondaryGenreSelect = getByLabelText(/secondary genre/i)
      userEvent.selectOptions(secondaryGenreSelect, 'Indian')

      expect(getByLabelText(/sub genre/i)).toBeTruthy()

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getAllByText(/required field/i).length).toEqual(1)
    })

    it('submits the subgenres', async () => {
      const {
        getAllByLabelText,
        getByLabelText,
        getByText,
        queryByLabelText,
      } = setup()

      expect(queryByLabelText(/sub genre/i)).toBeFalsy()

      const primaryGenreSelect = getByLabelText(/primary genre/i)
      userEvent.selectOptions(primaryGenreSelect, 'Indian')

      const primarySubGenreSelect = getAllByLabelText(/sub genre/i)[0]
      userEvent.selectOptions(primarySubGenreSelect, 'Assamese')

      const secondaryGenreSelect = getByLabelText(/secondary genre/i)
      userEvent.selectOptions(secondaryGenreSelect, 'Indian')

      const secondarySubGenreSelect = getAllByLabelText(/sub genre/i)[1]
      userEvent.selectOptions(secondarySubGenreSelect, 'Bengali')

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(getSubmission(mockSubmitHandler).sub_genre_id_primary).toEqual(
        '41'
      )
      expect(getSubmission(mockSubmitHandler).sub_genre_id_secondary).toEqual(
        '43'
      )
    })
  })
})
