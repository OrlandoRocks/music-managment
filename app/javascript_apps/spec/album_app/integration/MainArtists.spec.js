import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import produce from 'immer'

import AlbumForm from '../../../album_app/components/AlbumForm'
import { validInitialValues } from '../helpers/form'
import Providers from '../helpers/Providers'
import { albumAppConfig } from '../../specHelper'

describe('artist upsell messages', function () {
  describe('plan 1 user', () => {
    it('displays 0 upsells for 2 old artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine', 'Roedelius']
            draft.person.person_plan = 1
            draft.releaseTypeVars.specializedReleaseType = 'discovery_platforms'
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryByText(/upgrade required for all/i)).toBeFalsy()
    })

    it('displays 1 plan and 1 artist upsell for one new artist', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine']
            draft.person.person_plan = 1
            draft.releaseTypeVars.specializedReleaseType = 'discovery_platforms'
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getByText(/upgrade required for all/i)).toBeTruthy()
    })

    it('removes upsells when triggering artists are removed, restores when added', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['Robert Hood']
            draft.person.person_plan = 1
            draft.releaseTypeVars.specializedReleaseType = 'discovery_platforms'
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
                910: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '910',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(2)

      let removalButton = utils.getAllByLabelText(/remove/i)[0]
      userEvent.click(removalButton)
      await waitFor(() => expect(utils.queryByText(/^ppcocaine/i)).toBeFalsy())

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(1)

      removalButton = utils.getAllByLabelText(/remove/i)[0]
      userEvent.click(removalButton)
      await waitFor(() => expect(utils.queryByText(/^roedelius/i)).toBeFalsy())

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryAllByText(/upgrade required for all/i).length).toEqual(
        0
      )

      const addArtistButton = utils.getByText(/add artist/i)
      userEvent.click(addArtistButton)

      const nameInput = utils.getAllByLabelText(/artist name/i)[1]
      userEvent.type(nameInput, 'Eric Copeland')

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(1)
    })

    it('displays 1 plan and 2 artist upsells for two new artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine']
            draft.person.person_plan = 1
            draft.releaseTypeVars.specializedReleaseType = 'discovery_platforms'
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1',
                },
                2: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '2',
                },
                3: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '3',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(2)
    })
  })

  describe('user without plan', () => {
    it('displays 0 upsells for two new artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = []
            draft.person.person_plan = null
            draft.releaseTypeVars.specializedReleaseType = 'discovery_platforms'
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
                910: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '910',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryByText(/upgrade required for all/i)).toBeFalsy()
    })
  })

  describe('plan 5 user', () => {
    describe('without active artists', () => {
      it('displays 0 plan upsells and 2 artists upsells for 3 new artists', async () => {
        const unlimitedArtistPlanID = 5
        const utils = render(
          <Providers
            mockConfigContext={produce(albumAppConfig, (draft) => {
              draft.activeArtists = []
              draft.person.person_plan = unlimitedArtistPlanID
              draft.releaseTypeVars.specializedReleaseType =
                'discovery_platforms'
            })}
          >
            <AlbumForm
              album={{
                ...validInitialValues(),
                creatives: {
                  1234: {
                    apple: {},
                    spotify: {},
                    name: 'ppcocaine',
                    uuid: '1234',
                  },
                  5678: {
                    apple: {},
                    spotify: {},
                    name: 'Roedelius',
                    uuid: '5678',
                  },
                  910: {
                    apple: {},
                    spotify: {},
                    name: 'Robert Hood',
                    uuid: '910',
                  },
                },
              }}
              submitHandler={jest.fn}
            />
          </Providers>
        )

        expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
        expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(
          2
        )
      })
    })
  })
})

describe('max artists message', function () {
  it('displays when artists limit is reached', async () => {
    const utils = render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          releaseTypeVars: {
            specializedReleaseType: 'discovery_platforms',
          },
        }}
      >
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              1234: {
                apple: {},
                spotify: {},
                name: 'ppcocaine',
                uuid: '1234',
              },
              5678: {
                apple: {},
                spotify: {},
                name: "Neil Young's Grandmother",
                uuid: '5678',
              },
              6678: {
                apple: {},
                spotify: {},
                name: 'Roedelius',
                uuid: '6678',
              },
              7678: {
                apple: {},
                spotify: {},
                name: "Anthony Keidis' Monotone",
                uuid: '7678',
              },
            },
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

    expect(utils.getByText(/Spotify won't show some of these!/i)).toBeTruthy()
  })

  it('not shown when under artists limit', async () => {
    const utils = render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          releaseTypeVars: {
            specializedReleaseType: 'discovery_platforms',
          },
        }}
      >
        <AlbumForm
          album={{
            ...validInitialValues(),
            7678: {
              apple: {},
              spotify: {},
              name: "Anthony Keidis' Monotone",
              uuid: '7678',
            },
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

    expect(utils.queryByText(/Spotify won't show some of these!/i)).toBeFalsy()
  })
})
