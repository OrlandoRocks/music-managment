import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import mockDistributionContext from '../helpers/mockDistributionContext'

import { stores, freemiumStores } from '../helpers/storeOptions.js'

import * as tcFetch from '../../../album_app/utils/fetch'

const mockPost = jest.fn(() => ({ ok: true }))
tcFetch.post = mockPost

const mockStoreSubmitHandler = jest.fn()

const defaultMockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
  stores,
  freemiumStores,
}

const defaultMockDistributionContext = mockDistributionContext

xdescribe('Store Form', function () {
  afterEach(() => {
    mockPost.mockClear()

    mockStoreSubmitHandler.mockClear()
  })

  const setup = (args = {}) => {
    const {
      overridenMockConfigContext,
      overridenMockDistributionContext,
    } = args

    return render(
      <Providers
        mockConfigContext={
          overridenMockConfigContext || defaultMockConfigContext
        }
        mockDistributionContext={
          overridenMockDistributionContext || defaultMockDistributionContext
        }
      >
        <DistributionStepper
          mocks={{
            storeSubmitHandler: mockStoreSubmitHandler,
          }}
        />
      </Providers>
    )
  }

  it('Displays stores', async () => {
    const { findByText, getByLabelText, getByText } = setup()

    userEvent.click(getByLabelText(/edit album/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))

    await findByText('Stores*')

    await waitFor(() => {
      expect(getByText(stores[0].name)).toBeTruthy()
    })
  })

  it('Defaults to all stores selected', async () => {
    const { findByText, getByLabelText, getByText } = setup()

    userEvent.click(getByLabelText(/edit album/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))

    await findByText('Stores*')

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(mockStoreSubmitHandler).toHaveBeenCalled()

      const submission = mockStoreSubmitHandler.mock.calls[0][0].values
      const submittedStores = submission.stores

      Object.keys(submittedStores).map(
        (storeId) => expect(submittedStores[storeId]).toBeTruthy
      )
    })
  })

  it('Submits when a change is made in the selections', async () => {
    const { findByText, getByLabelText, getByText, getAllByText } = setup()

    userEvent.click(getByLabelText(/edit album/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))

    await findByText('Stores*')

    const storeOption1 = stores[0]
    const storeOption2 = stores[1]

    userEvent.click(getByLabelText(/deliver to all digital stores/i))
    userEvent.click(getAllByText(/view stores/i)[0])
    userEvent.click(getByLabelText(storeOption1.name))
    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(mockStoreSubmitHandler).toHaveBeenCalled()

      const submission = mockStoreSubmitHandler.mock.calls[0][0].values
      const submittedStores = submission.stores

      expect(submittedStores[storeOption1.id].selected).toBeTruthy
      expect(submittedStores[storeOption2.id].selected).toBeFalsy
    })
  })

  it('Submits with premade selections when a change is made', async () => {
    const overridenMockConfigContext = {
      ...defaultMockConfigContext,
      stores: [
        stores[0],
        {
          ...stores[1],
          selected: true,
        },
        stores[2],
      ],
    }

    const { findByText, getByLabelText, getByText, getAllByText } = setup({
      overridenMockConfigContext,
    })

    userEvent.click(getByLabelText(/edit album/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))

    await findByText('Stores*')

    const storeOption1 = stores[0]
    const storeOption2 = stores[1]

    userEvent.click(getByLabelText(/deliver to all digital stores/i))
    userEvent.click(getAllByText(/view stores/i)[0])
    userEvent.click(getByLabelText(storeOption1.name))
    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(mockStoreSubmitHandler).toHaveBeenCalled()

      const submission = mockStoreSubmitHandler.mock.calls[0][0].values
      const submittedStores = submission.stores

      expect(submittedStores[storeOption1.id].selected).toBeTruthy
      expect(submittedStores[storeOption2.id].selected).toBeTruthy
    })
  })

  it('Skips when no change is made in the selections', async () => {
    const { findByText, getByLabelText, getByText } = setup()

    userEvent.click(getByLabelText(/edit album/i))
    userEvent.click(getByText(/save album/i, { selector: 'button' }))

    await findByText('Stores*')

    userEvent.click(getByLabelText(/deliver to all digital stores/i))
    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(mockStoreSubmitHandler).not.toHaveBeenCalled()
    })
  })
})
