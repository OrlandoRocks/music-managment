import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { fireEvent, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import * as helpers from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import mockDistributionContext from '../helpers/mockDistributionContext'
import {
  atLeastOneStoreSelected,
  noStoresSelected,
} from '../helpers/variantMockDistributionContexts'
const mockUseSongsData = jest.fn()
helpers.useSongsData = mockUseSongsData

const mockAlbumSubmitHandler = jest.fn()
const mockSongSubmitHandler = jest.fn()
const mockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
}

xdescribe('V2 flow', function () {
  describe('Step 1', function () {
    afterEach(() => {
      mockAlbumSubmitHandler.mockClear()
      mockSongSubmitHandler.mockClear()
    })

    const setup = (args = {}) => {
      const {
        overridenMockConfigContext,
        overridenMockDistributionContext,
      } = args

      return render(
        <Providers
          mockConfigContext={overridenMockConfigContext || mockConfigContext}
          mockDistributionContext={
            overridenMockDistributionContext || mockDistributionContext
          }
        >
          <DistributionStepper
            mocks={{
              albumSubmitHandler: mockAlbumSubmitHandler,
              songSubmitHandler: mockSongSubmitHandler,
            }}
          />
        </Providers>
      )
    }

    it('submits the touched album form', async () => {
      const { findByLabelText, getByLabelText, getByText } = setup()

      userEvent.click(getByLabelText(/edit album/i))

      await findByLabelText(/album title/i)

      fireEvent.change(getByLabelText(/album title/i), {
        target: { value: 'edited name' },
      })
      userEvent.click(getByText(/save album/i, { selector: 'button' }))

      await waitFor(() => {
        expect(mockAlbumSubmitHandler).toHaveBeenCalled()
      })
    })

    context('skips album submit', () => {
      context('when at least one store is selected', function () {
        it('goes to summary', async () => {
          const { getByLabelText, getByText } = setup({
            overridenMockDistributionContext: atLeastOneStoreSelected,
          })

          userEvent.click(getByLabelText(/edit album/i))

          userEvent.click(getByText(/save album/i, { selector: 'button' }))

          await waitFor(() => {
            expect(getByText(/review album/i)).toBeTruthy()
          })
        })
      })

      context('when no stores are selected', function () {
        it('goes to stores page', async () => {
          const { getByLabelText, getByText } = setup({
            overridenMockDistributionContext: noStoresSelected,
          })

          userEvent.click(getByLabelText(/edit album/i))

          userEvent.click(getByText(/save album/i, { selector: 'button' }))

          await waitFor(() => {
            expect(getByText(/where do you want to distribute/i)).toBeTruthy()
          })
        })
      })
    })

    it('submits the touched song form 1', async () => {
      const {
        findByText,
        findByLabelText,
        getAllByLabelText,
        getByLabelText,
        getByTestId,
      } = setup()

      userEvent.click(getAllByLabelText(/edit song/i)[0])

      await findByLabelText(/song title/i)
      fireEvent.change(getByLabelText(/song title/i), {
        target: { value: 'edited name' },
      })

      const file = new File(['mysong'], 'mysong.mp3', { type: 'audio/mp3' })
      const input = getByTestId('upload-input')
      userEvent.upload(input, file)
      await findByText(/mysong/i)

      userEvent.click(getByLabelText(/submit/i))

      await waitFor(() => {
        expect(
          mockSongSubmitHandler.mock.calls[0][0].values.asset_filename
        ).toEqual('mysong.mp3')
        expect(mockSongSubmitHandler.mock.calls[0][0].values.name).toEqual(
          'edited name'
        )
      })
    })

    it('skips submit for the untouched song form 1', async () => {
      const { findByLabelText, getAllByLabelText, getByText } = setup()

      userEvent.click(getAllByLabelText(/edit song/i)[0])

      await findByLabelText(/song title/i)
      userEvent.click(getByText(/submit/i))

      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })
})
