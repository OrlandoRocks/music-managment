import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, fireEvent, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../album_app/components/AlbumForm'

import Providers from '../helpers/Providers'
import {
  mockCreative,
  validInitialValues,
  validInitialSpecializedValues,
  validSubmission,
} from '../helpers/form'

jest.mock('moment', () => {
  const TEST_DATE = 1598617502535
  const moment = jest.requireActual('moment')
  const momentMock = () => moment(TEST_DATE)
  return momentMock
})

const mockSubmitHandler = jest.fn()
function getSubmission(mockSubmitHandler) {
  return mockSubmitHandler.mock.calls[0][0]
}

describe('AlbumForm', function () {
  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  it('submits valid form', async () => {
    const { getByLabelText, getByText } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const editedName = 'Edited album name'
    fireEvent.change(getByLabelText(/Album Title/i), {
      target: { value: editedName },
    })
    fireEvent.click(getByText(/save album/i, { selector: 'button' }))

    await waitFor(() =>
      expect(getSubmission(mockSubmitHandler).album).toEqual(
        validInitialValues()
      )
    )
    await waitFor(() =>
      expect(getSubmission(mockSubmitHandler).values).toEqual({
        ...validSubmission(),
        name: editedName,
      })
    )
  })

  it('does not submit invalid form', async () => {
    const { getByLabelText, getByText, getAllByText } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    fireEvent.change(getByLabelText(/Album Title/i), {
      target: { value: '' },
    })
    fireEvent.change(getByLabelText(/primary genre/i), {
      target: { value: '' },
    })

    await act(async () => {
      userEvent.click(getByText(/save album/i))
    })

    await waitFor(() => getAllByText(/required field/i)).then((el) => {
      expect(el.length).toEqual(2)
    })
    expect(mockSubmitHandler).toHaveBeenCalledTimes(0)
  })

  describe('is various checkbox', function () {
    it('hides the main artist fields when checked', function () {
      const { getByText, queryByText } = render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {},
            }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      fireEvent.click(getByText(/various artist/i))
      fireEvent.click(getByText('Continue'))
      expect(queryByText(/add artist/i)).toBeNull()
    })
  })

  describe('optional upc code', () => {
    it('is writeable when creating a new album', () => {
      const { getByLabelText } = render(
        <Providers>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      expect(getByLabelText(/UPC\/EAN Code/)).not.toHaveAttribute('disabled')
    })

    it('is not writeable when editing a finalized album', () => {
      const { getByLabelText } = render(
        <Providers>
          <AlbumForm
            album={{ ...validInitialValues(), is_new: false, finalized: true }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      expect(getByLabelText(/UPC\/EAN Code/)).toHaveAttribute('disabled')
    })
  })

  describe('disabled when finalized', () => {
    it('does not submit form', async () => {
      const { getByText } = render(
        <Providers>
          <AlbumForm
            album={{ ...validInitialValues(), finalized: true }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      await act(async () => {
        userEvent.click(getByText(/save album/i))
      })

      expect(mockSubmitHandler).toHaveBeenCalledTimes(0)
    })

    it('disables artist modal features', async () => {
      const uuid = '456'
      const secondCreative = { [uuid]: mockCreative }
      secondCreative[uuid].uuid = uuid

      const { getByText, queryAllByLabelText, queryByLabelText } = render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              finalized: true,
              creatives: {
                ...validInitialValues.creatives,
                ...secondCreative,
              },
            }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const removalButtons = queryAllByLabelText(/remove/i)
      expect(removalButtons.length).toEqual(0)

      const addArtistButton = getByText(/add artist/i)
      userEvent.click(addArtistButton)
      const nameInput = queryByLabelText(/artist name/i)
      expect(nameInput).toBeFalsy()
    })
  })

  describe('creatives are invalid', () => {
    beforeEach(() => {
      mockSubmitHandler.mockClear()
    })

    describe("they're missing", () => {
      it('renders an error on save', async () => {
        const { getAllByText, getByText } = render(
          <Providers>
            <AlbumForm
              album={{ ...validInitialValues(), creatives: {} }}
              submitHandler={mockSubmitHandler}
            />
          </Providers>
        )

        fireEvent.click(getByText(/save album/i, { selector: 'button' }))

        await waitFor(() => getAllByText(/Required Field/i)).then((el) => {
          expect(el.length).toEqual(1)
        })
      })
    })

    describe('they have the same names', () => {
      it('renders an error on save', async () => {
        const mockCreative = {
          name: 'Bob',
          apple: {},
          spotify: {},
          role: 'primary_artist',
        }

        const { getByText } = render(
          <Providers>
            <AlbumForm
              album={{
                ...validInitialValues(),
                creatives: {
                  1234: {
                    ...mockCreative,
                    uuid: '1234',
                  },
                  567: {
                    ...mockCreative,
                    uuid: '567',
                  },
                },
              }}
              submitHandler={mockSubmitHandler}
            />
          </Providers>
        )

        fireEvent.click(getByText(/save album/i, { selector: 'button' }))

        await waitFor(() =>
          expect(getByText(/duplicate primary artist/i)).toBeTruthy()
        )
      })
    })

    describe('is_various albums', () => {
      beforeEach(() => {
        mockSubmitHandler.mockClear()
      })

      it('does not validate creatives', async () => {
        const { getByText } = render(
          <Providers>
            <AlbumForm
              album={{
                ...validInitialValues(),
                creatives: {},
              }}
              submitHandler={mockSubmitHandler}
            />
          </Providers>
        )

        fireEvent.click(getByText(/various artist/i))
        fireEvent.click(getByText('Continue'))

        fireEvent.click(getByText(/save album/i, { selector: 'button' }))

        await waitFor(() =>
          expect(getSubmission(mockSubmitHandler).album).toEqual({
            ...validInitialValues(),
            creatives: {},
          })
        )
      })
    })

    describe('specialized release albums', () => {
      it('hides the timed release field', () => {
        const { queryByText } = render(
          <Providers>
            <AlbumForm
              album={{
                ...validInitialSpecializedValues(),
                creatives: {},
              }}
              submitHandler={mockSubmitHandler}
            />
          </Providers>
        )

        expect(queryByText(/Release Time/i)).toBeNull()
      })
    })
  })
})
