import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import * as helpers from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import mockDistributionContext from '../helpers/mockDistributionContext'
const mockUseSongsData = jest.fn()
helpers.useSongsData = mockUseSongsData

import * as tcFetch from '../../../album_app/utils/fetch'

const mockDestroy = jest.fn(() => ({ ok: true }))
const mockPost = jest.fn(() => ({ ok: true }))
const mockPut = jest.fn(() => ({ ok: true }))
tcFetch.destroy = mockDestroy
tcFetch.post = mockPost
tcFetch.put = mockPut

const mockSongSubmitHandler = jest.fn()

const mockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
}

xdescribe('Song Form 2', function () {
  afterEach(() => {
    mockDestroy.mockClear()
    mockPost.mockClear()
    mockPut.mockClear()
    mockSongSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers
        mockConfigContext={mockConfigContext}
        mockDistributionContext={mockDistributionContext}
      >
        <DistributionStepper
          mocks={{
            songSubmitHandler: mockSongSubmitHandler,
          }}
        />
      </Providers>
    )

  it('Displays track info', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    expect(getByText(/song 1/i)).toBeTruthy()
    expect(getByText(/file_example_WAV_1MG2.wav/i)).toBeTruthy()
  })

  it('Errors on blank songwriter name', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)
    await findByLabelText(/first name/i)

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getByText(/required field/i)).toBeTruthy()
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Errors on blank creative name', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getAllByText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Song Artist 1'
    userEvent.type(input, songwriterName)

    const addCreativeButton = getByText(/add artist/i)
    userEvent.click(addCreativeButton)

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getAllByText(/required field/i)).toBeTruthy()
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Errors on blank creative role', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Song Artist 1'
    userEvent.type(input, songwriterName)

    const addCreativeButton = getByText(/add artist/i)
    userEvent.click(addCreativeButton)

    const newCreativeField = getAllByLabelText(/artist \/ creative/i)[1]
    userEvent.type(newCreativeField, songwriterName)

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getByText(/required field/i)).toBeTruthy()
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Errors on duplicate creative', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Song Artist 1'
    userEvent.type(input, songwriterName)

    const addCreativeButton = getByText(/add artist/i)
    userEvent.click(addCreativeButton)

    const newCreativeField = getAllByLabelText(/artist \/ creative/i)[1]
    userEvent.type(newCreativeField, 'Album Artist 1')

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getByText(/duplicate/i)).toBeTruthy()
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Errors on missing copyrights', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getAllByText,
      getByText,
    } = render(
      <Providers
        mockConfigContext={{
          ...mockConfigContext,
          copyrightsFormEnabled: true,
        }}
        mockDistributionContext={mockDistributionContext}
      >
        <DistributionStepper
          mocks={{
            songSubmitHandler: mockSongSubmitHandler,
          }}
        />
      </Providers>
    )

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Song Artist 1'
    userEvent.type(input, songwriterName)

    const addCreativeButton = getByText(/add artist/i)
    userEvent.click(addCreativeButton)

    const newCreativeField = getAllByLabelText(/artist \/ creative/i)[1]
    userEvent.type(newCreativeField, songwriterName)

    const newRoleField = getAllByLabelText(/role/i)[1]
    userEvent.type(newRoleField, 'a')
    userEvent.type(newRoleField, '{arrowdown}')
    userEvent.type(newRoleField, '{enter}')
    userEvent.tab()

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getAllByText(/required/i).length).toEqual(3) // It also appears in an instruction
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Submits', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Song Artist 1'
    userEvent.type(input, songwriterName)

    const addCreativeButton = getByText(/add artist/i)
    userEvent.click(addCreativeButton)

    const newCreativeField = getAllByLabelText(/artist \/ creative/i)[1]
    userEvent.type(newCreativeField, songwriterName)

    const newRoleField = getAllByLabelText(/role/i)[1]
    userEvent.type(newRoleField, 'a')
    userEvent.type(newRoleField, '{arrowdown}')
    userEvent.type(newRoleField, '{enter}')
    userEvent.tab()

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      const submission = mockSongSubmitHandler.mock.calls[0][0].values
      expect(submission.creatives.map((c) => c.artist_name)).toEqual([
        'Album Artist 1',
        'Song Artist 1',
      ])
      expect(
        submission.creatives.map((c) => c.roles.map((r) => r.id))
      ).toEqual([['primary_artist'], [5]])

      expect(Object.values(submission.songwriters)[0].artist_name).toEqual(
        songwriterName
      )

      expect(Object.values(submission.songwriters)[0].role_ids).toEqual(['3'])
    })
  })

  it('Nullifies renamed songwriter attrs when the songwriter is an album primary artist', async () => {
    const withSongwriter = { ...mockDistributionContext }
    withSongwriter.songs[
      'c27fc371-cc2d-7658-552a-462482bdd3e2'
    ].data.artists = [
      {
        artist_name: 'Album Artist 1',
        associated_to: 'Album',
        credit: 'primary_artist',
        role_ids: ['3'],
        creative_id: 1,
      },
    ]

    const setup = () =>
      render(
        <Providers
          mockConfigContext={mockConfigContext}
          mockDistributionContext={withSongwriter}
        >
          <DistributionStepper
            mocks={{
              songSubmitHandler: mockSongSubmitHandler,
            }}
          />
        </Providers>
      )

    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    const input = await findByLabelText(/first name/i)
    const songwriterName = 'Renamed'
    userEvent.type(input, songwriterName)

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      const submission = mockSongSubmitHandler.mock.calls[0][0].values
      expect(submission.creatives.map((c) => c.artist_name)).toEqual([
        'Album Artist 1',
      ])
      expect(
        submission.creatives.map((c) => c.roles.map((r) => r.id))
      ).toEqual([['primary_artist']])

      expect(Object.values(submission.songwriters)[0].artist_name).toEqual(
        `Album Artist 1${songwriterName}`
      )
      expect(Object.values(submission.songwriters)[0].artist_name).toEqual(
        'Album Artist 1Renamed'
      )
      expect(Object.values(submission.songwriters)[0].associated_to).toEqual(
        'Song'
      )
      expect(Object.values(submission.songwriters)[0].creative_id).toBeNull()
      expect(Object.values(submission.songwriters)[0].credit).toEqual(
        'contributor'
      )
      expect(Object.values(submission.songwriters)[0].role_ids).toEqual(['3'])
    })
  })
})
