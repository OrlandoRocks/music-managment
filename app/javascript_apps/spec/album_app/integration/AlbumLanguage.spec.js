import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import AlbumLanguage from '../../../album_app/components/AlbumFormFields/AlbumLanguage'
import album from '../helpers/album'
import Providers from '../helpers/Providers'

Enzyme.configure({ adapter: new Adapter() })

describe('Language', () => {
  describe('when creating a new album or single', () => {
    it("uses 'album' translation", () => {
      const component = mount(
        <Providers>
          <AlbumLanguage album={album} />
        </Providers>
      )
      expect(component.text()).toMatch(/language\*/i)
    })
  })
})
