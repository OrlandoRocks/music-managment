import React from 'react'

import '@testing-library/jest-dom/extend-expect'
import { render, waitFor, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import MockDate from 'mockdate'

import AlbumForm from '../../../album_app/components/AlbumForm'

import Providers from '../helpers/Providers'
import { validInitialValues, validSubmission } from '../helpers/form'

const TEST_DATE = 1598617502535
Date.now = jest.spyOn(Date, 'now').mockImplementation(() => TEST_DATE)
MockDate.set(TEST_DATE)

const mockSubmitHandler = jest.fn()
function getSubmission(mockSubmitHandler) {
  return mockSubmitHandler.mock.calls[0][0]
}

describe('PreviouslyReleased', function () {
  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  it('displays original release date picker set to yesterday', async () => {
    const { getAllByText } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const prevYes = getAllByText(/Yes/)[0]
    userEvent.click(prevYes)

    // sale date
    expect(screen.getByDisplayValue(/08\/28\/2020/)).toBeTruthy()

    // orig_release_year
    expect(screen.getByDisplayValue(/08\/27\/2020/)).toBeTruthy()
  })

  it('submits the un-clicked orig_release_year value of yesterday', async () => {
    const { getAllByText, getByText } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const prevYes = getAllByText(/Yes/)[0]
    userEvent.click(prevYes)

    userEvent.click(getByText(/save album/i))

    await waitFor(() =>
      expect(getSubmission(mockSubmitHandler).values).toEqual({
        ...validSubmission(),
        is_previously_released: true,
        orig_release_year: '2020-08-27',
      })
    )
  })

  it('submits clicked value', async () => {
    const { getAllByText, getAllByRole, getByText } = render(
      <Providers>
        <AlbumForm
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

    const prevYes = getAllByText(/Yes/)[0]
    userEvent.click(prevYes)

    // MUI pickers v3 doesn't provide for aria-labels on the calendar icon input adornment,
    // which is also invisible to screen readers.
    userEvent.type(getAllByRole('button')[3], '{space}')

    userEvent.click(getByText(/25/i, { selector: 'p' }))
    userEvent.click(getByText(/ok/i, { selector: 'span' }))

    expect(await screen.findAllByDisplayValue(/08\/25\/2020/)).toBeTruthy()

    userEvent.click(getByText(/save album/i))

    await waitFor(() =>
      expect(getSubmission(mockSubmitHandler).values).toEqual({
        ...validSubmission(),
        is_previously_released: true,
        orig_release_year: '2020-08-25',
      })
    )
  })
})
