import React from 'react'
import dayjs from 'dayjs'

import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'

import AlbumSaleDate from '../../../album_app/components/AlbumFormFields/AlbumSaleDate'

import Providers from '../helpers/Providers'
import { albumAppConfig } from '../../specHelper'

describe('AlbumSaleDate', () => {
  it('renders the album sale date', () => {
    render(
      <Providers>
        <AlbumSaleDate saleDate="2019-08-12" setValues={jest.fn} values={{}} />
      </Providers>
    )

    expect(screen.getByDisplayValue('08/12/2019')).toBeTruthy()
  })

  it('renders an upgrade message for a scheduled release', () => {
    const futureDate = dayjs(Date.now()).add(1, 'day').format('YYYY-MM-DD')

    render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          features: { plans_pricing: true },
          person: {
            person_plan: 2,
            plan_features: { scheduled_release: false },
          },
        }}
      >
        <AlbumSaleDate saleDate={futureDate} setValues={jest.fn} values={{}} />
      </Providers>
    )

    expect(screen.getByText(/scheduled release upgrade/i)).toBeTruthy()
  })

  it('renders NO upgrade message for plan user with scheduled release feature', () => {
    const futureDate = dayjs(Date.now()).add(1, 'day').format('YYYY-MM-DD')

    render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          features: { plans_pricing: false },
          person: {
            person_plan: 2,
            plan_features: { scheduled_release: false },
          },
        }}
      >
        <AlbumSaleDate saleDate={futureDate} setValues={jest.fn} values={{}} />
      </Providers>
    )

    expect(screen.queryByText(/scheduled release upgrade/i)).toBeFalsy()
  })

  it('renders NO upgrade message for a legacy user', () => {
    const futureDate = dayjs(Date.now()).add(1, 'day').format('YYYY-MM-DD')

    render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          features: { plans_pricing: false },
        }}
      >
        <AlbumSaleDate saleDate={futureDate} setValues={jest.fn} values={{}} />
      </Providers>
    )

    expect(screen.queryByText(/scheduled release upgrade/i)).toBeFalsy()
  })

  it('uses localized date format', () => {
    render(
      <Providers mockConfigContext={{ ...albumAppConfig, dayjsLocale: 'fr' }}>
        <AlbumSaleDate saleDate="2019-08-12" setValues={jest.fn} values={{}} />
      </Providers>
    )

    expect(screen.getByDisplayValue('12/08/2019')).toBeTruthy()
  })

  describe('SaleDate not available', () => {
    it('defaults to present date', () => {
      const expectedDate = dayjs().format('MM/DD/YYYY')

      render(
        <Providers>
          <AlbumSaleDate saleDate="" setValues={jest.fn} values={{}} />
        </Providers>
      )

      expect(screen.getByDisplayValue(expectedDate)).toBeTruthy()
    })
  })
})
