import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import AlbumReleaseTimeScenario from '../../../album_app/components/AlbumFormFields/AlbumReleaseTimeScenario'

import Providers from '../helpers/Providers'

Enzyme.configure({ adapter: new Adapter() })

describe('AlbumReleaseTimeScenario', () => {
  it('renders with relative checked', () => {
    const component = mount(
      <Providers>
        <AlbumReleaseTimeScenario
          handleChange={jest.fn}
          releaseScenario="relative_time"
        />
      </Providers>
    )

    expect(component.find('input#scenario-relative').props().checked).toBe(true)
  })

  it('renders with absolute checked', () => {
    const component = mount(
      <Providers>
        <AlbumReleaseTimeScenario
          handleChange={jest.fn}
          releaseScenario="absolute_time"
        />
      </Providers>
    )

    expect(component.find('input#scenario-absolute').props().checked).toEqual(
      true
    )
  })
})
