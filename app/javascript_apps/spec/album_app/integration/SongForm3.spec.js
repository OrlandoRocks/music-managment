import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import DistributionStepper from '../../../album_app/components/DistributionStepper'
import Providers from '../helpers/Providers'

import { ALBUM_APP_V2 } from '../../../album_app/utils/constants'
import { albumAppConfig } from '../../specHelper'

import * as helpers from '../../../album_app/components/DistributionStepper/distributionStepperHelpers'
import mockDistributionContext from '../helpers/mockDistributionContext'
const mockUseSongsData = jest.fn()
helpers.useSongsData = mockUseSongsData

import * as tcFetch from '../../../album_app/utils/fetch'

const mockDestroy = jest.fn(() => ({ ok: true }))
const mockPost = jest.fn(() => ({ ok: true }))
const mockPut = jest.fn(() => ({ ok: true }))
tcFetch.destroy = mockDestroy
tcFetch.post = mockPost
tcFetch.put = mockPut

const mockSongSubmitHandler = jest.fn()

const mockConfigContext = {
  ...albumAppConfig,
  features: { [ALBUM_APP_V2]: true },
}
const mockPermissions = {
  query: jest.fn(() => Promise.resolve({ state: 'granted' })),
}
const mockClipboard = {
  readText: jest.fn(() => Promise.resolve('clipboard contents')),
}
global.navigator.clipboard = mockClipboard
global.navigator.permissions = mockPermissions

xdescribe('Song Form 3', function () {
  afterEach(() => {
    mockDestroy.mockClear()
    mockPost.mockClear()
    mockPut.mockClear()
    mockSongSubmitHandler.mockClear()
  })

  const withSongwriter = { ...mockDistributionContext }
  withSongwriter.songs['c27fc371-cc2d-7658-552a-462482bdd3e2'].data.artists = [
    {
      artist_name: 'Album Artist 1',
      associated_to: 'Album',
      credit: 'primary_artist',
      role_ids: ['3'],
      creative_id: 1,
    },
  ]

  const setup = () =>
    render(
      <Providers
        mockConfigContext={mockConfigContext}
        mockDistributionContext={withSongwriter}
      >
        <DistributionStepper
          mocks={{
            songSubmitHandler: mockSongSubmitHandler,
          }}
        />
      </Providers>
    )

  it('Displays track info', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByLabelText,
      getByLabelText,
      getByText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)

    userEvent.click(getByLabelText(/submit/i))
    await findByText(/Tell Us Who Is On This Track/i)

    expect(getByText(/song 1/i)).toBeTruthy()
    expect(getByText(/file_example_WAV_1MG2.wav/i)).toBeTruthy()
  })

  it('Errors on blank instrumental and explicit', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByText,
      getAllByLabelText,
      getByLabelText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us Who Is On This Track/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us More About This Track/i)
    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getAllByText(/required field/i).length).toEqual(2)
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Errors when not explicit and clean_version is null', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByText,
      getAllByLabelText,
      getByLabelText,
      getByTestId,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us Who Is On This Track/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us More About This Track/i)

    const notInstrumental = getByTestId(/instrumental-no/i)
    userEvent.click(notInstrumental)

    const notExplicit = getByTestId(/explicit-no/i)
    userEvent.click(notExplicit)

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      expect(getAllByText(/required field/i).length).toEqual(1)
      expect(mockSongSubmitHandler).not.toHaveBeenCalled()
    })
  })

  it('Submits', async () => {
    const {
      findByLabelText,
      findByText,
      getAllByText,
      getAllByLabelText,
      getByLabelText,
    } = setup()

    userEvent.click(getAllByLabelText(/edit song/i)[0])
    await findByLabelText(/song title/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us Who Is On This Track/i)
    userEvent.click(getByLabelText(/submit/i))

    await findByText(/Tell Us More About This Track/i)

    const yesButtons = getAllByText(/yes/i)
    userEvent.click(yesButtons[0])

    userEvent.click(getByLabelText(/submit/i))

    await waitFor(() => {
      const submission = mockSongSubmitHandler.mock.calls[0][0].values
      expect(submission).toBeTruthy()
    })
  })
})
