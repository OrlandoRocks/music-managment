import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../album_app/components/AlbumForm'
import { validInitialValues } from '../helpers/form'
import Providers from '../helpers/Providers'

/**
 * Mock the API calls
 */
import {
  appleFetchArtistRequest,
  appleArtistSearchRequest,
} from '../../../album_app/utils/appleAPIRequests'
import {
  getSpotifyArtistData,
  getSpotifySearchData,
} from '../../../album_app/utils/spotifyDataService'
import { albumAppConfig } from '../../specHelper'
import produce from 'immer'

jest.mock('../../../album_app/utils/appleAPIRequests')
jest.mock('../../../album_app/utils/spotifyDataService')

appleFetchArtistRequest.mockImplementation(() => Promise.resolve({ error: {} }))
appleArtistSearchRequest.mockImplementation(() => Promise.resolve([]))
getSpotifyArtistData.mockImplementation(() => Promise.resolve({ error: {} }))
getSpotifySearchData.mockImplementation(() => Promise.resolve([]))

// Unmock default-mocked ESIDDataFetcher
jest.unmock('../../../album_app/components/data/ExternalServiceIDDataFetcher')

describe('with no creatives', function () {
  describe('adding creatives', function () {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {},
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

    it('creates 1 row', async () => {
      const {
        getByLabelText,
        getByText,
        queryAllByLabelText,
        queryByText,
      } = setup()

      const addArtistButton = getByText(/add artist/i)
      userEvent.click(addArtistButton)

      const nameInput = getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'ppcocaine')

      const backButton = getByLabelText('back')
      userEvent.click(backButton)

      await waitFor(() =>
        expect(
          queryByText(/what's the artist's name?/i)
        ).not.toBeInTheDocument()
      )

      // The new row
      const newRow = getByText(/ppcocaine/i)
      expect(newRow).toBeTruthy()

      // The chips aren't green
      const missingChips = queryAllByLabelText(/missing/i)
      expect(missingChips.length).toEqual(2)

      // Removal button not present
      const removalButtons = queryAllByLabelText(/remove/i)
      expect(removalButtons.length).toEqual(0)
    })

    it('creates 2 rows', async () => {
      const {
        getAllByText,
        getByLabelText,
        getByText,
        queryAllByLabelText,
        queryByText,
      } = setup()

      /**
       * Add 1 creative
       */
      const addArtistButton = getByText(/add artist/i)
      userEvent.click(addArtistButton)

      let nameInput = getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'ppcocaine')

      // Close the modal
      let backButton = getByLabelText('back')
      userEvent.click(backButton)
      await waitFor(() =>
        expect(
          queryByText(/what's the artist's name?/i)
        ).not.toBeInTheDocument()
      )

      /**
       * Add 2nd creative
       */
      userEvent.click(addArtistButton)

      nameInput = getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'ppcocaine2')

      backButton = getByLabelText('back')
      userEvent.click(backButton)
      await waitFor(() =>
        expect(
          queryByText(/what's the artist's name?/i)
        ).not.toBeInTheDocument()
      )

      // The new rows
      const newRows = getAllByText(/ppcocaine/i)
      expect(newRows.length).toEqual(2)

      // 1 removal button present
      const removalButtons = queryAllByLabelText(/remove/i)
      expect(removalButtons.length).toEqual(1)
    })
  })
})

describe('existing creatives', function () {
  const setup = () =>
    render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              1234: {
                apple: {},
                spotify: {
                  identifier: '12312344',
                  image: '',
                  name: 'spotifyppcocaine',
                },
                name: 'ppcocaine',
                uuid: '1234',
              },
              5678: {
                apple: {
                  identifier: '56785678',
                  image: '',
                  name: 'appleRoedelius',
                },
                spotify: {},
                name: 'Roedelius',
                uuid: '5678',
              },
            },
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

  it('displays correct chips and labels with service data', async () => {
    getSpotifyArtistData.mockImplementationOnce(() =>
      Promise.resolve({
        id: '7v5E9zviGMsOGHRdMVmhDc',
        image:
          'https://i.scdn.co/image/ab67616d00004851aca0a47820cae26fd08de690',
        latestAlbum: 'Drauf und Dran',
        name: 'spotifyppcocaine',
        spotifyUrl: 'https://open.spotify.com/artist/7v5E9zviGMsOGHRdMVmhDc',
      })
    )
    appleFetchArtistRequest.mockImplementationOnce(() =>
      Promise.resolve({
        id: '107917',
        type: 'artists',
        href: '/v1/catalog/us/artists/107917',
        attributes: {
          genreNames: ['Alternative'],
          name: 'appleRoedelius',
          url: 'https://music.apple.com/us/artist/nine-inch-nails/107917',
        },
        relationships: {
          topAlbum: {
            id: '1440837096',
            type: 'albums',
            href: '/v1/catalog/us/albums/1440837096',
            attributes: {
              artwork: {
                width: 3000,
                height: 3000,
                url:
                  'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/e7/9f/f4/e79ff471-b461-d01d-bd47-6d6a5ec1f1a1/15UMGIM67680.rgb.jpg/{w}x{h}bb.jpeg',
                bgColor: 'ebd9ab',
                textColor1: '120704',
                textColor2: '403117',
                textColor3: '3d3126',
                textColor4: '625235',
              },
            },
          },
        },
      })
    )

    const { getByLabelText, getByText, queryAllByLabelText } = setup()

    const firstRow = getByText(/^ppcocaine/i)
    expect(firstRow).toBeTruthy()

    const secondRow = getByText(/^roedelius/i)
    expect(secondRow).toBeTruthy()

    const missingChips = queryAllByLabelText(/missing/i)
    expect(missingChips.length).toEqual(2)

    const presentChips = queryAllByLabelText(/present/i)
    expect(presentChips.length).toEqual(2)

    const removalButtons = queryAllByLabelText(/remove/i)
    expect(removalButtons.length).toEqual(1)

    userEvent.click(firstRow)
    await waitFor(() => expect(getByText(/spotifyppcocaine/).toBeTruthy))

    let backButton = getByLabelText('back')
    userEvent.click(backButton)

    userEvent.click(secondRow)
    await waitFor(() => expect(getByText(/appleRoedelius/).toBeTruthy))
  })

  it('displays correct chips and labels for new_artist', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {
                    state: 'new_artist',
                  },
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {
                    state: 'new_artist',
                  },
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

    const { getByLabelText, getByText, queryAllByLabelText } = setup()

    const firstRow = getByText(/^ppcocaine/i)
    expect(firstRow).toBeTruthy()

    const secondRow = getByText(/^roedelius/i)
    expect(secondRow).toBeTruthy()

    const missingChips = queryAllByLabelText(/missing/i)
    expect(missingChips.length).toEqual(2)

    const presentChips = queryAllByLabelText(/present/i)
    expect(presentChips.length).toEqual(2)

    const removalButtons = queryAllByLabelText(/remove/i)
    expect(removalButtons.length).toEqual(1)

    userEvent.click(firstRow)
    await waitFor(() => expect(getByText(/page for me/).toBeTruthy))

    let backButton = getByLabelText('back')
    userEvent.click(backButton)

    userEvent.click(secondRow)
    await waitFor(() => expect(getByText(/page for me/).toBeTruthy))
  })

  it('displays correct chips and labels for create_page', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {
                    create_page: true,
                  },
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {
                    create_page: true,
                  },
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

    const { getByLabelText, getByText, queryAllByLabelText } = setup()

    const firstRow = getByText(/^ppcocaine/i)
    expect(firstRow).toBeTruthy()

    const secondRow = getByText(/^roedelius/i)
    expect(secondRow).toBeTruthy()

    const missingChips = queryAllByLabelText(/missing/i)
    expect(missingChips.length).toEqual(2)

    const presentChips = queryAllByLabelText(/present/i)
    expect(presentChips.length).toEqual(2)

    const removalButtons = queryAllByLabelText(/remove/i)
    expect(removalButtons.length).toEqual(1)

    userEvent.click(firstRow)
    await waitFor(() => expect(getByText(/page for me/).toBeTruthy))

    let backButton = getByLabelText('back')
    userEvent.click(backButton)

    userEvent.click(secondRow)
    await waitFor(() => expect(getByText(/page for me/).toBeTruthy))
  })

  describe('removing a creative', function () {
    it('disappears on click', async () => {
      const { getByLabelText, getByText, queryByText } = setup()

      const secondRow = getByText(/^roedelius/i)
      expect(secondRow).toBeTruthy()

      const removalButton = getByLabelText(/remove/i)
      userEvent.click(removalButton)

      await waitFor(() => expect(queryByText(/^roedelius/i)).toBeFalsy())
    })
  })
})

describe('artist upsell messages', function () {
  describe('plan 1 user', () => {
    it('displays 0 plan and 0 artist upsells for 2 existing artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine', 'Roedelius']
            draft.person.person_plan = 1
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryByText(/upgrade required for all/i)).toBeFalsy()
    })

    it('displays 1 plan and 1 artist upsell for 1 new artist', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine']
            draft.person.person_plan = 1
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getByText(/upgrade required for all/i)).toBeTruthy()
    })

    it('removes upsells when triggering artists are removed, restores when added', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['Robert Hood']
            draft.person.person_plan = 1
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
                910: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '910',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(2)

      let removalButton = utils.getAllByLabelText(/remove/i)[0]
      userEvent.click(removalButton)
      await waitFor(() => expect(utils.queryByText(/^ppcocaine/i)).toBeFalsy())

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(1)

      removalButton = utils.getAllByLabelText(/remove/i)[0]
      userEvent.click(removalButton)
      await waitFor(() => expect(utils.queryByText(/^roedelius/i)).toBeFalsy())

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryAllByText(/upgrade required for all/i).length).toEqual(
        0
      )

      const addArtistButton = utils.getByText(/add artist/i)
      userEvent.click(addArtistButton)

      const nameInput = utils.getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'Eric Copeland')

      const backButton = utils.getByLabelText('back')
      userEvent.click(backButton)

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(1)
    })

    it('displays 1 plan and 2 artist upsells for 2 new artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = ['ppcocaine']
            draft.person.person_plan = 1
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1',
                },
                2: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '2',
                },
                3: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '3',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.getByText(/paid upgrade required/i)).toBeTruthy()
      expect(utils.getAllByText(/upgrade required for all/i).length).toEqual(2)
    })
  })

  describe('non-plan user', () => {
    it('displays 0 upsells for 2 new artists', async () => {
      const utils = render(
        <Providers
          mockConfigContext={produce(albumAppConfig, (draft) => {
            draft.activeArtists = []
            draft.person.person_plan = null
          })}
        >
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'ppcocaine',
                  uuid: '1234',
                },
                5678: {
                  apple: {},
                  spotify: {},
                  name: 'Roedelius',
                  uuid: '5678',
                },
                910: {
                  apple: {},
                  spotify: {},
                  name: 'Robert Hood',
                  uuid: '910',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

      expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
      expect(utils.queryByText(/upgrade required for all/i)).toBeFalsy()
    })
  })

  describe('plan 5 user', () => {
    describe('without active artists', () => {
      it('displays 0 plan and 2 artist upsells for 3 new artists', async () => {
        const unlimitedArtistPlanID = 5
        const utils = render(
          <Providers
            mockConfigContext={produce(albumAppConfig, (draft) => {
              draft.activeArtists = []
              draft.person.person_plan = unlimitedArtistPlanID
            })}
          >
            <AlbumForm
              album={{
                ...validInitialValues(),
                creatives: {
                  1234: {
                    apple: {},
                    spotify: {},
                    name: 'ppcocaine',
                    uuid: '1234',
                  },
                  5678: {
                    apple: {},
                    spotify: {},
                    name: 'Roedelius',
                    uuid: '5678',
                  },
                  910: {
                    apple: {},
                    spotify: {},
                    name: 'Robert Hood',
                    uuid: '910',
                  },
                },
              }}
              submitHandler={jest.fn}
            />
          </Providers>
        )

        expect(utils.queryByText(/paid upgrade required/i)).toBeFalsy()
        expect(
          utils.queryAllByText(/upgrade required for all/i).length
        ).toEqual(2)
      })
    })
  })
})

describe('max artists message', function () {
  it('displays when artists limit is reached', async () => {
    const utils = render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              1234: {
                apple: {},
                spotify: {},
                name: 'ppcocaine',
                uuid: '1234',
              },
              5678: {
                apple: {},
                spotify: {},
                name: "Neil Young's Grandmother",
                uuid: '5678',
              },
              6678: {
                apple: {},
                spotify: {},
                name: 'Roedelius',
                uuid: '6678',
              },
              7678: {
                apple: {},
                spotify: {},
                name: "Anthony Keidis' Monotone",
                uuid: '7678',
              },
            },
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

    expect(utils.getByText(/Spotify won't show some of these!/i)).toBeTruthy()
  })

  it('not shown when under artists limit', async () => {
    const utils = render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            7678: {
              apple: {},
              spotify: {},
              name: "Anthony Keidis' Monotone",
              uuid: '7678',
            },
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

    expect(utils.queryByText(/Spotify won't show some of these!/i)).toBeFalsy()
  })
})

describe('abandoning input', function () {
  const setup = () =>
    render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {},
          }}
          submitHandler={jest.fn}
        />
      </Providers>
    )

  describe('via back button', () => {
    it('removes the abandoned row', async () => {
      const {
        getByLabelText,
        getByText,
        queryAllByLabelText,
        queryByText,
      } = setup()

      const addArtistButton = getByText(/add artist/i)
      userEvent.click(addArtistButton)

      const nameInput = getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'ppcocaine')

      // erase the input, now name = ''
      const clearButton = getByLabelText(/clear/i)
      userEvent.click(clearButton)

      const backButton = getByLabelText('back')
      userEvent.click(backButton)

      await waitFor(() =>
        expect(
          queryByText(/what's the artist's name?/i)
        ).not.toBeInTheDocument()
      )

      // the row was removed, no chips are present
      const missingChips = queryAllByLabelText(/missing/i)
      expect(missingChips.length).toEqual(0)
    })
  })

  describe('via esc key', () => {
    // same as above
    it('removes the abandoned row', async () => {
      const {
        getByLabelText,
        getByText,
        queryAllByLabelText,
        queryByText,
      } = setup()

      const addArtistButton = getByText(/add artist/i)
      userEvent.click(addArtistButton)

      const nameInput = getByLabelText(/artist name/i)
      userEvent.type(nameInput, 'ppcocaine')

      // erase the input, now name = ''
      const clearButton = getByLabelText(/clear/i)
      userEvent.click(clearButton)

      userEvent.type(nameInput, '{esc}')

      await waitFor(() =>
        expect(
          queryByText(/what's the artist's name?/i)
        ).not.toBeInTheDocument()
      )

      // the row was removed, no chips are present
      const missingChips = queryAllByLabelText(/missing/i)
      expect(missingChips.length).toEqual(0)
    })
  })
})
