import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import AlbumTitle from '../../../album_app/components/AlbumFormFields/AlbumTitle'

import Providers from '../helpers/Providers'

Enzyme.configure({ adapter: new Adapter() })

describe('Title', () => {
  describe('when creating a new album', () => {
    it("uses 'album' translation", () => {
      const component = mount(
        <Providers>
          <AlbumTitle albumType="Album" />
        </Providers>
      )
      expect(component.text()).toMatch(/album title/i)
    })
  })

  describe('when creating a new single', () => {
    it("uses 'single' translation", () => {
      const component = mount(
        <Providers>
          <AlbumTitle albumType="Single" />
        </Providers>
      )
      expect(component.text()).toMatch(/single title/i)
    })
  })
})
