import React from 'react'

import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'

import AlbumForm from '../../../album_app/components/AlbumForm'

import Providers from '../helpers/Providers'
import { validInitialValues } from '../helpers/form'
import { albumAppConfig } from '../../specHelper'

describe('AdvancedFeaturesHeader', function () {
  it('displays the subheader', async () => {
    const utils = render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          person: { person_plan: 1, plan_features: { foo: true } },
        }}
      >
        <AlbumForm album={validInitialValues()} />
      </Providers>
    )

    expect(utils.getByText('Note: the following features...')).toBeTruthy()
  })

  it('hides the subheader', async () => {
    const utils = render(
      <Providers
        mockConfigContext={{
          ...albumAppConfig,
          person: {
            person_plan: 4,
            plan_features: {
              custom_label: true,
              recording_location: true,
              ringtones: true,
              territory_restrictions: true,
              upc: true,
            },
          },
        }}
      >
        <AlbumForm album={validInitialValues()} />
      </Providers>
    )

    expect(utils.queryByText('Note: the following features...')).toBeFalsy()
  })
})
