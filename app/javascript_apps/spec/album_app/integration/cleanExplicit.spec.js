import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { fireEvent, render, waitFor } from '@testing-library/react'

import AlbumForm from '../../../album_app/components/AlbumForm'
import Providers from '../helpers/Providers'
import { albumAppConfig } from '../../specHelper'
import { validInitialValues } from '../helpers/form'

jest.mock('moment', () => {
  const TEST_DATE = 1598617502535
  const moment = jest.requireActual('moment')
  const momentMock = () => moment(TEST_DATE)
  return momentMock
})

const mockSubmitHandler = jest.fn()

const mockConfigContext = {
  ...albumAppConfig,
  releaseTypeVars: { explicitFieldsEnabled: true },
}

describe('Explicit', function () {
  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  context('explicit content', function () {
    it("doesn't trigger clean component", async () => {
      const { getAllByText, queryByText } = render(
        <Providers mockConfigContext={mockConfigContext}>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const yesExplicit = getAllByText(/^yes/i)[0]
      fireEvent.click(yesExplicit)

      const cleanComponent = queryByText(/does an explicit/i)
      expect(cleanComponent).toBeFalsy()
    })
  })

  context('clean content', function () {
    it('triggers clean component', async () => {
      const { getByText, getAllByText } = render(
        <Providers mockConfigContext={mockConfigContext}>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noExplicit = getAllByText(/^no/i)[0]
      fireEvent.click(noExplicit)

      const cleanComponent = getByText(/explicit version/i)
      expect(cleanComponent).toBeTruthy()
    })

    it('does not submit when clean version is null', async () => {
      const { getByText, getAllByText } = render(
        <Providers mockConfigContext={mockConfigContext}>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noExplicit = getAllByText(/^no/i)[0]
      fireEvent.click(noExplicit)

      const cleanComponent = getByText(/explicit version/i)
      expect(cleanComponent).toBeTruthy()

      fireEvent.click(getByText(/save album/i, { selector: 'button' }))
      await waitFor(() => {
        expect(getByText(/save album/i, { selector: 'button' })).toBeTruthy()
      })
    })

    it('submits when clean is boolean', async () => {
      const { getByText, getAllByText, queryByText } = render(
        <Providers mockConfigContext={mockConfigContext}>
          <AlbumForm
            album={validInitialValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      const noExplicit = getAllByText(/^no/i)[0]
      fireEvent.click(noExplicit)

      const noClean = getAllByText(/^no/i)[1]
      fireEvent.click(noClean)

      fireEvent.click(getByText(/save album/i, { selector: 'button' }))
      expect(
        queryByText(/save album/i, { selector: 'button' })
      ).not.toBeInTheDocument()
    })
  })
})
