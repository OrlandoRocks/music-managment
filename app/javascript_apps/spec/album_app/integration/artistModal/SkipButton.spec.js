import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../../album_app/components/AlbumForm'
import Providers from '../../helpers/Providers'

import { validInitialValues } from '../../helpers/form'
import {
  appleArtistResult,
  appleResults1,
  spotifyArtistResult,
  spotifyResults1,
} from '../../helpers/mam'

/**
 * Mock the search and artist fetch API calls
 */
import {
  appleArtistSearchRequest,
  appleFetchArtistRequest,
} from '../../../../album_app/utils/appleAPIRequests'
import {
  getSpotifyArtistData,
  getSpotifySearchData,
} from '../../../../album_app/utils/spotifyDataService'
jest.mock('../../../../album_app/utils/appleAPIRequests')
jest.mock('../../../../album_app/utils/spotifyDataService')

describe('ExternalIdManualEntry', () => {
  appleArtistSearchRequest.mockImplementation(() =>
    Promise.resolve(appleResults1)
  )
  appleFetchArtistRequest.mockImplementation(() =>
    Promise.resolve(appleArtistResult)
  )

  getSpotifySearchData.mockImplementation(() =>
    Promise.resolve(spotifyResults1)
  )
  getSpotifyArtistData.mockImplementation(() =>
    Promise.resolve(spotifyArtistResult)
  )

  const mockPermissions = {
    query: jest.fn(() => Promise.resolve({ state: 'granted' })),
  }
  const mockClipboard = {
    readText: jest.fn(() => Promise.resolve('clipboard contents')),
  }
  global.navigator.clipboard = mockClipboard
  global.navigator.permissions = mockPermissions

  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return mockSubmitHandler.mock.calls[0][0].values
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              123: {
                apple: {},
                spotify: {},
                name: 'Artist Name',
                uuid: '123',
              },
            },
          }}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

  it('clears the Spotify URL entry', async () => {
    const {
      findByText,
      getAllByLabelText,
      getByLabelText,
      getAllByText,
      getByPlaceholderText,
      getByText,
    } = setup()

    // Enter URL
    await goToStep2(getByText)
    await clickStep2Entry(getByText)
    await enterInput(
      getByPlaceholderText,
      'https://open.spotify.com/artist/123'
    )
    await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())
    userEvent.click(getByText(/continue/i))
    await findByText(/roedelius/i)

    // Back to URL, then Back to Spotify
    let backButton = getByLabelText(/back/i)
    userEvent.click(backButton)
    backButton = getAllByLabelText(/back/i)[0]
    userEvent.click(backButton)

    // Skip Spotify, then Skip Apple
    let skipButton = getAllByText(/skip/i)[0]
    userEvent.click(skipButton)
    skipButton = getAllByText(/skip/i)[1]
    userEvent.click(skipButton)

    await save(findByText, getByText)

    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler).creatives[123].spotify).toEqual({
        create_page: undefined,
        identifier: undefined,
        url: undefined,
      })
    })
  })

  it('clears the Spotify search result selection', async () => {
    const {
      findAllByText,
      findByText,
      getByLabelText,
      getAllByText,
      getByText,
    } = setup()

    // Select first result
    await goToStep2(getByText)
    const latestAlbums = getAllByText(/latest album/i)
    userEvent.click(latestAlbums[0])
    await findAllByText(/nine inch nails/i)

    // Back to Spotify results
    const backButton = getByLabelText(/back/i)
    userEvent.click(backButton)

    // Skip Spotify, then Skip Apple
    let skipButton = getAllByText(/skip/i)[0]
    userEvent.click(skipButton)
    skipButton = getAllByText(/skip/i)[1]
    userEvent.click(skipButton)

    await save(findByText, getByText)

    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler).creatives[123].spotify).toEqual({
        create_page: undefined,
        identifier: undefined,
        url: undefined,
      })
    })
  })

  it('overwrites previous value', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                123: {
                  apple: { identifier: '123' },
                  spotify: {},
                  name: 'Artist Name',
                  uuid: '123',
                },
              },
            }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

    const { findByText, getAllByText, getByText } = setup()

    await goToStep2(getByText)

    let skipButton = getAllByText(/skip/i)[0]
    userEvent.click(skipButton)
    skipButton = getAllByText(/skip/i)[1]
    userEvent.click(skipButton)

    await save(findByText, getByText)

    await waitFor(() => {
      expect(getSubmission(mockSubmitHandler).creatives[123].apple).toEqual({
        create_page: undefined,
        identifier: undefined,
        url: undefined,
      })
    })
  })
})

/**
 *
 * Helpers
 */

async function goToStep2(getByText) {
  await act(async () => {
    userEvent.click(getByText(/Artist Name/i))
  })
  await act(async () => {
    userEvent.click(getByText(/continue/i))
  })
}

async function clickStep2Entry(getByText) {
  const entryButton = getByText(/provide link/i)
  await act(async () => {
    userEvent.click(entryButton)
  })
}

async function enterInput(getByPlaceholderText, inputValue) {
  const input = getByPlaceholderText(/page link/i)
  userEvent.click(input)
  await act(async () => {
    userEvent.type(input, inputValue)
  })
}

async function save(findByText, getByText) {
  const saveButton = await findByText(/save selections/i)
  userEvent.click(saveButton)
  userEvent.click(getByText(/save album/i, { selector: 'button' }))
}
