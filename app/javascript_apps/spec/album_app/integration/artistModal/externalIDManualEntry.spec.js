import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../../album_app/components/AlbumForm'
import Providers from '../../helpers/Providers'

import { validInitialValues } from '../../helpers/form'
import {
  appleArtistResult,
  appleArtistNoAlbumResult,
  appleResults1,
  spotifyArtistResult,
  spotifyArtistNoAlbumResult,
  spotifyResults1,
} from '../../helpers/mam'

/**
 * Mock the search and artist fetch API calls
 */
import {
  appleArtistSearchRequest,
  appleFetchArtistRequest,
} from '../../../../album_app/utils/appleAPIRequests'
import {
  getSpotifyArtistData,
  getSpotifySearchData,
} from '../../../../album_app/utils/spotifyDataService'
jest.mock('../../../../album_app/utils/appleAPIRequests')
jest.mock('../../../../album_app/utils/spotifyDataService')

describe('ExternalIdManualEntry', () => {
  appleArtistSearchRequest.mockImplementation(() =>
    Promise.resolve(appleResults1)
  )
  appleFetchArtistRequest.mockImplementation(() =>
    Promise.resolve(appleArtistResult)
  )

  getSpotifySearchData.mockImplementation(() =>
    Promise.resolve(spotifyResults1)
  )
  getSpotifyArtistData.mockImplementation(() =>
    Promise.resolve(spotifyArtistResult)
  )

  const mockPermissions = {
    query: jest.fn(() => Promise.resolve({ state: 'granted' })),
  }
  const mockClipboard = {
    readText: jest.fn(() => Promise.resolve('clipboard contents')),
  }
  global.navigator.clipboard = mockClipboard
  global.navigator.permissions = mockPermissions

  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return mockSubmitHandler.mock.calls[0][0].values
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              123: {
                apple: {},
                spotify: {},
                name: 'Artist Name',
                uuid: '123',
              },
            },
          }}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

  it('renders Spotify URL input', async () => {
    const { getByText } = setup()
    await goToStep2(getByText)
    await clickStep2Entry(getByText)

    expect(getByText(/spotify profile/i)).toBeTruthy()
  })

  it('renders Apple URL input', async () => {
    const { getAllByText, getByText } = setup()
    await goToStep2(getByText)
    await goFrom2To3ManualEntry(getAllByText)

    expect(getByText(/apple profile/i)).toBeTruthy()
  })

  it('pastes from clipboard', async () => {
    const { getByLabelText, findByPlaceholderText, getByText } = setup()
    await goToStep2(getByText)
    await clickStep2Entry(getByText)

    const pasteButton = getByLabelText(/paste/i)
    userEvent.click(pasteButton)

    const field = await findByPlaceholderText(/page link/i)

    await waitFor(() => {
      expect(field.value).toEqual('clipboard contents')
    })
  })

  describe('validations and errors', () => {
    it("doesn't show error while typing in invalid state", async () => {
      const { getByPlaceholderText, getByText, queryByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(getByPlaceholderText, 'bad input')

      expect(queryByText(/incorrect link/i)).toBeFalsy()
    })

    it('shows error on blur', async () => {
      const { getByPlaceholderText, getByText, queryByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(getByPlaceholderText, 'bad input')

      expect(queryByText(/incorrect link/i)).toBeFalsy()

      userEvent.click(document.body)

      expect(getByText(/incorrect link/i)).toBeTruthy()
    })

    it('shows continue immediately when verified', async () => {
      const { getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )
      await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())
    })
  })

  describe('navigation', () => {
    it('moves backward through manual entry, forward through search results', async () => {
      const {
        findByLabelText,
        getAllByText,
        getByLabelText,
        getByPlaceholderText,
        getByText,
      } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )

      await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())

      userEvent.click(getByText(/continue/i))
      await waitFor(() => expect(getAllByText(/top album/i).length).toEqual(10))

      let backButton = getByLabelText(/back/i)
      userEvent.click(backButton)

      // Backtracked through manual entry
      await waitFor(() => expect(getByText(/paste a link/i)).toBeTruthy())

      backButton = getByLabelText(/back/i)
      userEvent.click(backButton)
      backButton = await findByLabelText(/back/i)
      userEvent.click(backButton)

      await waitFor(() =>
        expect(getByText(/what's the artist's name/i)).toBeTruthy()
      )

      userEvent.click(getByText(/continue/i))

      // Progressed through search results
      await waitFor(() =>
        expect(getAllByText(/latest album/i).length).toEqual(10)
      )
    })
  })

  describe('spotify', () => {
    it('shows fetch error when api response is bad', async () => {
      getSpotifyArtistData.mockImplementationOnce(() =>
        Promise.resolve({ error: { status: 500 } })
      )

      const { getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )
      await waitFor(() => expect(getByText(/fetch failed/i)).toBeTruthy())
    })

    it('shows id error when there is no such id', async () => {
      getSpotifyArtistData.mockImplementationOnce(() =>
        Promise.resolve({ error: { status: 404 } })
      )

      const { getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )
      await waitFor(() => expect(getByText(/bad id/i)).toBeTruthy())
    })

    it('handles case of artist with no album', async () => {
      getSpotifyArtistData.mockImplementationOnce(() =>
        Promise.resolve(spotifyArtistNoAlbumResult)
      )

      const { getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )
      await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())
      userEvent.click(getByText(/continue/i))

      await waitFor(() => expect(getByText(/roedelius/i)).toBeTruthy())
    })

    it('submits the value', async () => {
      const { findByText, getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await clickStep2Entry(getByText)
      await enterInput(
        getByPlaceholderText,
        'https://open.spotify.com/artist/123'
      )
      await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())
      userEvent.click(getByText(/continue/i))

      await findByText(/roedelius/i)

      const skipButton = await findByText(/skip/i)
      userEvent.click(skipButton)

      await save(findByText, getByText)

      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).creatives[123].spotify).toEqual(
          {
            create_page: undefined,
            identifier: '7v5E9zviGMsOGHRdMVmhDc',
            url: 'https://open.spotify.com/artist/123',
          }
        )
      })
    })
  })

  describe('apple', () => {
    it('shows fetch error when api response is bad', async () => {
      appleFetchArtistRequest.mockImplementationOnce(() =>
        Promise.resolve({ error: { status: 500 } })
      )

      const { getAllByText, getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await goFrom2To3ManualEntry(getAllByText)
      await enterInput(
        getByPlaceholderText,
        'https://itunes.apple.com/us/artist/blue-rhythm-combo/494026053'
      )
      await waitFor(() => {
        expect(getByText(/fetch failed/i)).toBeTruthy()
      })
    })

    it('shows id error when there is no such id', async () => {
      appleFetchArtistRequest.mockImplementationOnce(() =>
        Promise.resolve({ error: { status: 404 } })
      )

      const { getAllByText, getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await goFrom2To3ManualEntry(getAllByText)
      await enterInput(
        getByPlaceholderText,
        'https://itunes.apple.com/us/artist/blue-rhythm-combo/494026053'
      )
      await waitFor(() => expect(getByText(/bad id/i)).toBeTruthy())
    })

    it('handles case of artist with no album', async () => {
      appleFetchArtistRequest.mockImplementationOnce(() =>
        Promise.resolve(appleArtistNoAlbumResult)
      )

      const { getAllByText, getByPlaceholderText, getByText } = setup()
      await goToStep2(getByText)
      await goFrom2To3ManualEntry(getAllByText)
      await enterInput(
        getByPlaceholderText,
        'https://itunes.apple.com/us/artist/blue-rhythm-combo/494026053'
      )
      await waitFor(() => expect(getByText(/continue/i)).toBeTruthy())
      userEvent.click(getByText(/continue/i))

      await waitFor(() => expect(getByText(/Blue Rhythm Combo/i)).toBeTruthy())
    })

    it('submits the value', async () => {
      const {
        findByText,
        getAllByText,
        getByPlaceholderText,
        getByText,
      } = setup()
      await goToStep2(getByText)
      await goFrom2To3ManualEntry(getAllByText)
      await enterInput(
        getByPlaceholderText,
        'https://itunes.apple.com/us/artist/blue-rhythm-combo/494026053'
      )
      await waitFor(() => {
        expect(getByText(/continue/i)).toBeTruthy()
      })
      userEvent.click(getByText(/continue/i))

      await waitFor(() => expect(getByText(/Blue Rhythm Combo/i)).toBeTruthy())

      await save(findByText, getByText)

      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).creatives[123].apple).toEqual({
          create_page: undefined,
          identifier: '494026053',
          url: 'https://itunes.apple.com/us/artist/blue-rhythm-combo/494026053',
        })
      })
    })
  })
})

/**
 *
 * Helpers
 */

async function goToStep2(getByText) {
  await act(async () => {
    userEvent.click(getByText(/Artist Name/i))
  })
  await act(async () => {
    userEvent.click(getByText(/continue/i))
  })
}

async function clickStep2Entry(getByText) {
  const entryButton = getByText(/provide link/i)
  await act(async () => {
    userEvent.click(entryButton)
  })
}

async function goFrom2To3ManualEntry(getAllByText) {
  userEvent.click(getAllByText(/skip/i)[0])
  await act(async () => {
    userEvent.click(getAllByText(/provide link/i)[1])
  })
}

async function enterInput(getByPlaceholderText, inputValue) {
  const input = getByPlaceholderText(/page link/i)
  userEvent.click(input)
  await act(async () => {
    userEvent.type(input, inputValue)
  })
}

async function save(findByText, getByText) {
  const saveButton = await findByText(/save selections/i)
  userEvent.click(saveButton)
  userEvent.click(getByText(/save album/i, { selector: 'button' }))
}
