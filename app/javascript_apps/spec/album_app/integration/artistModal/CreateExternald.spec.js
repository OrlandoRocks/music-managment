import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../../album_app/components/AlbumForm'

import { validInitialValues } from '../../helpers/form'
import {
  appleResults1,
  appleArtistResultNIN,
  spotifyResults1,
} from '../../helpers/mam'
import Providers from '../../helpers/Providers'

/**
 * Mock the search API calls
 */
import {
  appleArtistSearchRequest,
  appleFetchArtistRequest,
} from '../../../../album_app/utils/appleAPIRequests'
import { getSpotifySearchData } from '../../../../album_app/utils/spotifyDataService'
jest.mock('../../../../album_app/utils/appleAPIRequests')
jest.mock('../../../../album_app/utils/spotifyDataService')

describe('create page', function () {
  appleFetchArtistRequest.mockImplementation(() =>
    Promise.resolve(appleArtistResultNIN)
  )
  appleArtistSearchRequest.mockImplementation(() =>
    Promise.resolve(appleResults1)
  )
  getSpotifySearchData.mockImplementation(() =>
    Promise.resolve(spotifyResults1)
  )

  const mockPermissions = {
    query: jest.fn(() => ({})),
  }
  global.navigator.permissions = mockPermissions

  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return mockSubmitHandler.mock.calls[0][0].values
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  it('indicates a page will be created and submits page_created', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                123: {
                  apple: {},
                  spotify: {},
                  name: 'nin',
                  uuid: '123',
                },
              },
            }}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

    const { findByText, getAllByText, getByText } = setup()

    await goToStep2(getByText)
    await clickStep2Create(getByText)

    const createForMeButton = getByText(/spotify page for me/i)
    userEvent.click(createForMeButton)

    // The snackbar and label text
    await waitFor(() =>
      expect(getAllByText(/page will be created/i).length).toEqual(1)
    )
    expect(getByText(/spotify page for me/i)).toBeTruthy()

    const skipButton = await findByText(/skip/i)
    userEvent.click(skipButton)

    await save(findByText, getByText)

    await waitFor(() =>
      expect(getSubmission(mockSubmitHandler).creatives[123].spotify).toEqual({
        create_page: true,
      })
    )
  })

  it('overwrites previous value', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: { identifier: '123' },
                  spotify: {},
                  name: 'nin',
                  uuid: '1234',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

    const {
      findAllByText,
      getAllByText,
      getByText,
      queryAllByText,
      queryByText,
    } = setup()

    await goToStep2(getByText)
    expect(getByText(/nine inch nails/i)).toBeTruthy()
    await goFrom2To3Create(getAllByText)

    const createForMeButton = getByText(/apple page for me/i)
    userEvent.click(createForMeButton)

    // The summary and snack
    await findAllByText(/will be created/i)

    expect(queryByText(/nine inch nails/i)).toBeFalsy()

    expect(queryAllByText(/nin/).length).toEqual(2)
  })

  it('remembers path taken', async () => {
    const setup = () =>
      render(
        <Providers>
          <AlbumForm
            album={{
              ...validInitialValues(),
              creatives: {
                1234: {
                  apple: {},
                  spotify: {},
                  name: 'nin',
                  uuid: '1234',
                },
              },
            }}
            submitHandler={jest.fn}
          />
        </Providers>
      )

    const {
      findByLabelText,
      findByText,
      getAllByText,
      getByLabelText,
      getByText,
    } = setup()

    await goToStep2(getByText)
    await clickStep2Create(getByText)
    const createForMeButton = getByText(/spotify page for me/i)
    userEvent.click(createForMeButton)

    let backButton = await findByLabelText(/back/i)
    userEvent.click(backButton)

    // Backtracked through create
    expect(await findByText(/spotify account for you/i)).toBeTruthy()

    backButton = getByLabelText(/back/i)
    userEvent.click(backButton)
    backButton = await findByLabelText(/back/i)
    userEvent.click(backButton)

    expect(getByText(/what's the artist's name/i)).toBeTruthy()

    userEvent.click(getByText(/continue/i))

    // Progressed through search results
    await waitFor(() =>
      expect(getAllByText(/latest album/i).length).toEqual(10)
    )
  })
})

/**
 * Helpers
 */

async function goToStep2(getByText) {
  await act(async () => {
    userEvent.click(getByText(/nin/i))
  })
  await act(async () => {
    userEvent.click(getByText(/continue/i))
  })
}

async function clickStep2Create(getByText) {
  const createButton = getByText(/create.*page/i)
  await act(async () => {
    userEvent.click(createButton)
  })
}

async function goFrom2To3Create(getAllByText) {
  userEvent.click(getAllByText(/skip/i)[0])
  await act(async () => {
    userEvent.click(getAllByText(/create.*page/i)[1])
  })
}

async function save(findByText, getByText) {
  const saveButton = await findByText(/save selections/i)
  userEvent.click(saveButton)
  userEvent.click(getByText(/save album/i, { selector: 'button' }))
}
