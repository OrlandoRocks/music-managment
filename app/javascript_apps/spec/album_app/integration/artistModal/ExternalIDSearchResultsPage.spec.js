import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm from '../../../../album_app/components/AlbumForm'
import Providers from '../../helpers/Providers'
import { validInitialValues } from '../../helpers/form'
import { appleResults1, spotifyResults1 } from '../../helpers/mam'

/**
 * Mock the search API calls
 */
import { appleArtistSearchRequest } from '../../../../album_app/utils/appleAPIRequests'
import { getSpotifySearchData } from '../../../../album_app/utils/spotifyDataService'
jest.mock('../../../../album_app/utils/appleAPIRequests')
jest.mock('../../../../album_app/utils/spotifyDataService')

describe('searching', function () {
  appleArtistSearchRequest.mockImplementation(() =>
    Promise.resolve(appleResults1)
  )
  getSpotifySearchData.mockImplementation(() =>
    Promise.resolve(spotifyResults1)
  )

  const mockPermissions = {
    query: jest.fn(() => ({})),
  }
  global.navigator.permissions = mockPermissions

  const mockSubmitHandler = jest.fn()
  function getSubmission(mockSubmitHandler) {
    return mockSubmitHandler.mock.calls[0][0].values
  }

  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  const setup = () =>
    render(
      <Providers>
        <AlbumForm
          album={{
            ...validInitialValues(),
            creatives: {
              123: {
                apple: {},
                spotify: {},
                name: 'nin',
                uuid: '123',
              },
            },
          }}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

  it('displays results', async () => {
    const { findByText, getAllByText, getByText } = setup()

    await goToStep2(findByText, getByText)
    expect(getAllByText(/latest album/i).length).toEqual(10)

    const skipButton = getAllByText(/skip/i)[0]
    userEvent.click(skipButton)
    expect(getAllByText(/top album/i).length).toEqual(10)
  })

  it('displays no results message', async () => {
    appleArtistSearchRequest.mockImplementationOnce(() => Promise.resolve([]))
    getSpotifySearchData.mockImplementationOnce(() => Promise.resolve([]))

    const { findByText, getAllByText, getByText, queryAllByText } = setup()

    await goToStep2(findByText, getByText)
    expect(queryAllByText(/latest album/i).length).toEqual(0)

    skip(getAllByText)

    expect(queryAllByText(/top album/i).length).toEqual(0)
    expect(getAllByText(/no results found/i).length).toEqual(2)
  })

  it('applies selections and submits', async () => {
    const { findByText, getAllByText, getByText, queryAllByLabelText } = setup()

    let presentChips = queryAllByLabelText(/present/i)
    expect(presentChips.length).toEqual(0)

    await goToStep2(findByText, getByText)

    const latestAlbums = getAllByText(/latest album/i)
    userEvent.click(latestAlbums[0])

    const topAlbums = getAllByText(/top album/i)
    userEvent.click(topAlbums[0])
    await waitFor(() =>
      expect(getAllByText(/nine inch nails/i).length).toEqual(2)
    )

    userEvent.type(document.body, '{esc}')

    presentChips = queryAllByLabelText(/present/i)
    expect(presentChips.length).toEqual(2)

    await save(findByText, getByText)

    await waitFor(() => {
      expect(
        getSubmission(mockSubmitHandler).creatives[123].apple.identifier
      ).toEqual('107917')
      expect(
        getSubmission(mockSubmitHandler).creatives[123].spotify.identifier
      ).toEqual('296zqVG4gI7mDSUD0CytXM')
    })
  })

  it('clears selections on new input', async () => {
    const {
      findByText,
      getAllByText,
      getByText,
      getByLabelText,
      queryAllByText,
    } = setup()

    await goToStep2(findByText, getByText)

    const latestAlbums = getAllByText(/latest album/i)
    userEvent.click(latestAlbums[0])

    const topAlbums = getAllByText(/top album/i)
    userEvent.click(topAlbums[0])

    const selections = getAllByText(/nine inch nails/i)
    expect(selections.length).toEqual(4)

    const firstStep = getAllByText(/artist name/i)[0]
    userEvent.click(firstStep)
    const nameInput = getByLabelText(/artist name/i)
    userEvent.click(nameInput)
    userEvent.type(nameInput, 'a')
    userEvent.click(getByText(/continue/i))
    await waitFor(() => {
      const matches = queryAllByText(/nine inch nails/i)
      expect(matches.length).toEqual(1)
    })
  })
})

/**
 * Helpers
 */

async function goToStep2(findByText, getByText) {
  userEvent.click(getByText(/nin/i))
  await findByText(/continue/i)
  userEvent.click(getByText(/continue/i))
}

function skip(getAllByText) {
  const skipButton = getAllByText(/skip/i)[0]
  userEvent.click(skipButton)
}

async function save(findByText, getByText) {
  const saveButton = await findByText(/save selections/i)
  userEvent.click(saveButton)
  userEvent.click(getByText(/save album/i, { selector: 'button' }))
}
