import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import AlbumForm2 from '../../../album_app/components/AlbumForm2/AlbumForm2'

import { saveForm } from '../helpers/actions'

import Providers from '../helpers/Providers'
import {
  validInitialValues,
  validInitialSpecializedValues,
} from '../helpers/form'

jest.mock('moment', () => {
  const TEST_DATE = 1598617502535
  const moment = jest.requireActual('moment')
  const momentMock = () => moment(TEST_DATE)
  return momentMock
})

const mockSubmitHandler = jest.fn()
function getSubmission(mockSubmitHandler) {
  return mockSubmitHandler.mock.calls[0][0]
}

xdescribe('AlbumForm2', () => {
  afterEach(() => {
    mockSubmitHandler.mockClear()
  })

  describe('specialized release albums', () => {
    it('hides the timed release field', () => {
      const { queryByText } = render(
        <Providers>
          <AlbumForm2
            album={validInitialSpecializedValues()}
            submitHandler={mockSubmitHandler}
          />
        </Providers>
      )

      expect(queryByText(/Release Time/i)).toBeNull()
    })
  })

  const setup = () =>
    render(
      <Providers>
        <AlbumForm2
          album={validInitialValues()}
          submitHandler={mockSubmitHandler}
        />
      </Providers>
    )

  describe('label name', () => {
    it('includes label name in the submission', async () => {
      const { getByLabelText, getByText, queryByText } = setup()

      expect(queryByText(/Release Time/i)).toBeNull()

      const newLabel = 'Reservoir'
      userEvent.type(getByLabelText(/album label/i), newLabel)

      await saveForm(getByText)

      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).album).toEqual(
          validInitialValues()
        )
        expect(getSubmission(mockSubmitHandler).values.label_name).toEqual(
          newLabel
        )
      })
    })

    it('includes label name attr when blank', async () => {
      const { getByLabelText, getByText } = setup()

      const newLocation = 'New York'
      userEvent.type(getByLabelText(/recording location/i), newLocation)

      await saveForm(getByText)

      await waitFor(() => {
        expect(getSubmission(mockSubmitHandler).album).toEqual(
          validInitialValues()
        )
        expect(
          getSubmission(mockSubmitHandler).values.recording_location
        ).toEqual(newLocation)
      })
    })
  })
})
