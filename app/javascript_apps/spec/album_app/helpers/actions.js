import { act } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

async function saveForm(getByText) {
  return act(async () => userEvent.click(getByText(/save album/i)))
}

export { saveForm }
