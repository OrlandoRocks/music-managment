import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'

import { AlbumProvider } from '../../../album_app/contexts/ConfigContext'
import { SnackbarProvider } from '../../../album_app/contexts/SnackbarContext'
import { ESIDDataProvider } from '../../../album_app/contexts/ESIDDataContext'
import { albumAppConfig } from '../../specHelper'
import mockDistributionContext from '../helpers/mockDistributionContext'
import { DistributionProvider } from '../../../album_app/contexts/DistributionContext'
import MuiPickersUtilsProviderWrapper from '../../../album_app/contexts/MuiPickersUtilsProviderWrapper'

export default function Providers(props) {
  const { children, mockConfigContext, mockDistributionContext } = props

  return (
    <Router>
      <AlbumProvider value={mockConfigContext}>
        <MuiPickersUtilsProviderWrapper>
          <ESIDDataProvider>
            <SnackbarProvider>
              <DistributionProvider mockInitialState={mockDistributionContext}>
                {children}
              </DistributionProvider>
            </SnackbarProvider>
          </ESIDDataProvider>
        </MuiPickersUtilsProviderWrapper>
      </AlbumProvider>
    </Router>
  )
}

Providers.defaultProps = {
  mockConfigContext: albumAppConfig,
  mockDistributionContext,
}
