import mockDistributionContext from './mockDistributionContext'

export const atLeastOneStoreSelected = {
  ...mockDistributionContext,
  stores: [
    {
      ...mockDistributionContext.stores[0],
      selected: true,
    },
  ],
  freemiumStores: [
    {
      ...mockDistributionContext.freemiumStores[0],
      selected: false,
    },
  ],
}

export const noStoresSelected = {
  ...mockDistributionContext,
  stores: [
    {
      ...mockDistributionContext.stores[0],
      selected: false,
    },
  ],
  freemiumStores: [
    {
      ...mockDistributionContext.freemiumStores[0],
      selected: false,
    },
  ],
}

export const songsCompletedAndProgressLevelCompleted = {
  ...mockDistributionContext,
  distributionProgressLevel: 4,
  songsComplete: true,
}

export const songsCompleted = {
  ...mockDistributionContext,
  songsComplete: true,
}

export const progressLevelCompleted = {
  ...mockDistributionContext,
  distributionProgressLevel: 4,
}
