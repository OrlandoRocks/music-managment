import moment from 'moment'

import { countryAbbreviations } from '../helpers/countries'

export const mockCreative = {
  apple: {},
  name: 'µ-Ziq',
  role: 'primary_artist',
  spotify: {},
  uuid: '123',
}

export const validInitialValues = () => ({
  album_type: 'Album',
  clean_version: null,
  creatives: {
    [mockCreative.uuid]: mockCreative,
  },
  finalized: false,
  golive_date: {},
  is_new: true,
  is_previously_released: false,
  is_various: false,
  label_name: '',
  language_code: 'en',
  name: 'Album name',
  parental_advisory: null,
  primary_genre_id: 1,
  recording_location: '',
  sale_date: moment().toJSON(),
  secondary_genre_id: 3,
  upc: '',
  is_dj_release: false,
  selected_countries: countryAbbreviations,
  sub_genre_id_primary: '',
  sub_genre_id_secondary: '',
})

export const validInitialSpecializedValues = () => ({
  album_type: 'Album',
  clean_version: null,
  creatives: {
    [mockCreative.uuid]: mockCreative,
  },
  finalized: false,
  golive_date: {},
  is_new: true,
  is_previously_released: false,
  is_various: false,
  label_name: '',
  language_code: 'en',
  name: 'Album name',
  parental_advisory: null,
  primary_genre_id: 1,
  sale_date: moment().toJSON(),
  secondary_genre_id: 3,
  upc: '',
  is_dj_release: false,
  selected_countries: countryAbbreviations,
  sub_genre_id_primary: '',
  sub_genre_id_secondary: '',
  specialized_release_type: 'freemium',
})

export const validSubmission = () => {
  /* eslint-disable no-unused-vars */
  const { primary_genre_id, secondary_genre_id, ...rest } = validInitialValues()
  /* eslint-enable no-unused-vars */

  return {
    ...rest,
    primary_genre_id: primary_genre_id.toString(),
    secondary_genre_id: secondary_genre_id.toString(),
  }
}
