const mockCreative = {
  apple: {},
  external_service_ids: [],
  role: 'primary_artist',
  spotify: {},
  uuid: '123',
}

let newAlbum = {
  album_type: 'Album',
  creatives: [mockCreative],
  finalized: false,
  golive_date: '',
  is_new: true,
  is_previously_released: false,
  is_various: false,
  label_name: '',
  language_code: 'en',
  name: '',
  primary_genre_id: 1,
  sale_date: new Date() + '',
  secondary_genre_id: '',
  upc: '',
  is_dj_release: false,
}

export default newAlbum
