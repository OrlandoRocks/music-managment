export const stores = [
  {
    id: 1,
    name: 'Store 1',
    selected: false,
    tagline: null,
  },
  {
    id: 2,
    name: 'Store 2',
    selected: false,
    tagline: null,
  },
  {
    id: 3,
    name: 'Store 3',
    selected: false,
    tagline: null,
  },
]

export const freemiumStores = [
  {
    id: 10,
    name: 'Freemium Store 10',
    selected: false,
    tagline: "I'm free!",
  },
  {
    id: 20,
    name: 'Freemium Store 20',
    selected: false,
    tagline: "I'm free!",
  },
  {
    id: 30,
    name: 'Freemium Store 30',
    selected: false,
    tagline: "I'm free!",
  },
]
