export const spotifyResults1 = [
  {
    id: '296zqVG4gI7mDSUD0CytXM',
    image: 'https://i.scdn.co/image/247c355dde622fe0bd7c633ff1667872efd68351',
    latestAlbum: 'Ghosts V: Together',
    name: 'Nine Inch Nails',
    spotifyUrl: 'https://open.spotify.com/album/296zqVG4gI7mDSUD0CytXM',
  },
  {
    id: '1dVw3jSdgZp7PfGhCEo32t',
    image: 'https://i.scdn.co/image/29ff21300c63aefe255e915ce0e23e5e79e2786d',
    latestAlbum: 'A Letter To My Younger Self',
    name: 'Quinn XCII',
    spotifyUrl: 'https://open.spotify.com/album/1dVw3jSdgZp7PfGhCEo32t',
  },
  {
    id: '4Wi489dcX4owpM21EcKKz7',
    image: 'https://i.scdn.co/image/2740c54b7171be8d59f9654e9ef223f308e73aae',
    latestAlbum:
      'The Sun Will Come up, The Seasons Will Change & The Flowers Will Fall',
    name: 'Nina Nesbitt',
    spotifyUrl: 'https://open.spotify.com/album/4Wi489dcX4owpM21EcKKz7',
  },
  {
    id: '6N4bEm9XVxGP3YdMwDjwoF',
    image: 'https://i.scdn.co/image/ed8648231d7718a500c67bf312fdfa2d61767218',
    latestAlbum: 'Spotlight on Nina Simone',
    name: 'Nina Simone',
    spotifyUrl: 'https://open.spotify.com/album/6N4bEm9XVxGP3YdMwDjwoF',
  },
  {
    id: '4NImOx3VKytJGEpPcgN5Cx',
    image: 'https://i.scdn.co/image/53a1a7039854e04e55f4be324e743e4f27277b25',
    latestAlbum: 'The Prophecy',
    name: 'Ninja Sex Party',
    spotifyUrl: 'https://open.spotify.com/album/4NImOx3VKytJGEpPcgN5Cx',
  },
  {
    id: '15Zjx7K5ifTVmXa3Nk5B88',
    image: 'https://i.scdn.co/image/0d8f82471c2e3a1267f16eca2376443826a3add1',
    latestAlbum: 'Nina Sky',
    name: 'Nina Sky',
    spotifyUrl: 'https://open.spotify.com/album/15Zjx7K5ifTVmXa3Nk5B88',
  },
  {
    id: '0VyGlmX3Wt2R88OuyH9ewz',
    image: 'https://i.scdn.co/image/a0d7039a576d711d3bf29a8bb26b6739fc63d2b5',
    latestAlbum: 'Cuentale (Radio Edit)',
    name: 'Nino',
    spotifyUrl: 'https://open.spotify.com/album/0VyGlmX3Wt2R88OuyH9ewz',
  },
  {
    id: '0njGbav0C1R6EBZTeVsvC4',
    image: 'https://i.scdn.co/image/19d649f0676ace4c3ef8fee04f4489e07e2412e3',
    latestAlbum: 'Snapshots',
    name: 'Nine Days',
    spotifyUrl: 'https://open.spotify.com/album/0njGbav0C1R6EBZTeVsvC4',
  },
  {
    id: '71DA1lG47JXG6YNggUB55b',
    image: 'https://i.scdn.co/image/342bdcddeb32a6d3e9af1d5ad41508dfb60dbf75',
    latestAlbum: 'Colombiana',
    name: 'Niño de Elche',
    spotifyUrl: 'https://open.spotify.com/album/71DA1lG47JXG6YNggUB55b',
  },
  {
    id: '551pGp1sw4FNItesRbVVzF',
    image: 'https://i.scdn.co/image/64668a59c7478613b42eb71d74ab1f19794dbff6',
    latestAlbum: 'M.I.L.S 3 (Réédition)',
    name: 'Ninho',
    spotifyUrl: 'https://open.spotify.com/album/551pGp1sw4FNItesRbVVzF',
  },
]

export const spotifyArtistResult = {
  id: '7v5E9zviGMsOGHRdMVmhDc',
  image: 'https://i.scdn.co/image/ab67616d00004851aca0a47820cae26fd08de690',
  latestAlbum: 'Drauf und Dran',
  name: 'Roedelius',
  spotifyUrl: 'https://open.spotify.com/artist/7v5E9zviGMsOGHRdMVmhDc',
}

export const spotifyArtistNoAlbumResult = {
  id: '7v5E9zviGMsOGHRdMVmhDc',
  image: 'https://i.scdn.co/image/ab67616d00004851aca0a47820cae26fd08de690',
  name: 'Roedelius',
  spotifyUrl: 'https://open.spotify.com/artist/7v5E9zviGMsOGHRdMVmhDc',
}

export const spotifyArtistResultNIN = {
  id: '0X380XXQSNBYuleKzav5UO',
  image: 'https://i.scdn.co/image/247c355dde622fe0bd7c633ff1667872efd68351',
  latest_album: 'Ghosts V: Together',
  name: 'Nine Inch Nails',
  spotifyUrl: 'https://open.spotify.com/artist/0X380XXQSNBYuleKzav5UO',
}

export const appleResults1 = [
  {
    id: '107917',
    type: 'artists',
    href: '/v1/catalog/us/artists/107917',
    attributes: {
      url: 'https://music.apple.com/us/artist/nine-inch-nails/107917',
      genreNames: ['Alternative'],
      name: 'Nine Inch Nails',
    },
    relationships: {
      topAlbum: {
        id: '1440837096',
        type: 'albums',
        href: '/v1/catalog/us/albums/1440837096',
        attributes: {
          artwork: {
            width: 3000,
            height: 3000,
            url:
              'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/e7/9f/f4/e79ff471-b461-d01d-bd47-6d6a5ec1f1a1/15UMGIM67680.rgb.jpg/{w}x{h}bb.jpeg',
            bgColor: 'ebd9ab',
            textColor1: '120704',
            textColor2: '403117',
            textColor3: '3d3126',
            textColor4: '625235',
          },
          artistName: 'Nine Inch Nails',
          isSingle: false,
          url:
            'https://music.apple.com/us/album/the-downward-spiral/1440837096',
          isComplete: true,
          genreNames: [
            'Alternative',
            'Music',
            'Rock',
            'Electronic',
            'Industrial',
            'Hard Rock',
            'Metal',
            'Adult Alternative',
          ],
          trackCount: 14,
          isMasteredForItunes: true,
          releaseDate: '1994-03-08',
          name: 'The Downward Spiral',
          recordLabel: 'Nothing',
          copyright: '℗ 2015 Nothing/Interscope Records',
          playParams: { id: '1440837096', kind: 'album' },
          editorialNotes: {
            standard:
              'When Trent Reznor submitted <i>The Downward Spiral</i> to Interscope Records cofounder Jimmy Iovine, he offered an apology. “Sorry,” Reznor remembered saying in a 2016 interview with Beats 1 host Zane Lowe. “I had to do it.” \nEven in a shifted mid-’90s paradigm where bands like Nirvana could become famous, <i>Spiral</i> felt extreme—a blast of negativity so thorough that it’s hard to imagine it making headway with any size of audience, let alone the four million or so who ended up buying it. (Reznor himself remains somewhat incredulous—a note on the band’s own website describes the album as a “celebration of self-destruction in the form of a concept record that somehow managed to become a multi-platinum worldwide hit.”)<br />\nInspired by Iggy Pop, Lou Reed, and David Bowie’s Berlin trilogy (<i>Low</i> in particular), <i>Spiral</i> pushed the industrial pop of <i>Pretty Hate Machine</i> and the <i>Broken</i> EP in unexpected directions, experimenting with torch songs (“Piggy”), disco and soul (“Closer”), and ballads of such unnerving fragility that listening to them feels voyeuristic (“Hurt”). Even tracks that found continuity with the band’s earlier music—“Big Man with a Gun,” the stuttering hardcore of “March of the Pigs”—were drastically more aggressive than anything they’d done before, flashes of mania that made the album’s quieter moments feel all the more exhausted.<br />\nWhat emerged was a pattern of emotional whiplash: You feel like you can topple the world, shred your tormentors, vent your toxic depths. Then, suddenly, you feel nothing. That you could whistle along with half of it was perverse but strangely fitting, especially given Reznor’s S&amp;M affectations: Here was pain that felt pretty good. The album’s sound was just as polarized. Mixing digital and analog, sample collages with live performances, densely processed signals with naturalistic ones, <i>Spiral</i> was a drastic renovation to the texture and feel of conventional rock. Even now, it feels damaged, noisy, wrong—the product of both a gleaming future and an already ruined past. If the album has an essentializing moment, it’s the climax of “Closer”: mechanistic synth-funk that gives way to a warped, solitary piano, part music box, part trash. After <i>Spiral</i>, artists didn’t have to decide whether to be a rock band or an electronic producer—Reznor had bridged the two.<br />\n“I was feeling my way around,” Reznor told Zane Lowe of the period leading up to <i>Spiral</i>. “What do I have to say? I didn’t live this exotic life. I grew up in a town in Pennsylvania. Kind of boring. What I did know is how I felt about myself, and my struggles to figure out who and what and why from my own perspective.” And therein lies the album’s healing irony: In facing the abyss, Reznor found his congregation.',
            short: 'A bold, no-look dive into the abyss, 25 years on.',
          },
          isCompilation: false,
          contentRating: 'explicit',
        },
      },
    },
  },
  {
    id: '320366644',
    type: 'artists',
    href: '/v1/catalog/us/artists/320366644',
    attributes: {
      url: 'https://music.apple.com/us/artist/ninho/320366644',
      genreNames: ['Hip-Hop/Rap'],
      name: 'Ninho',
    },
    relationships: {
      topAlbum: {
        id: '1453399177',
        type: 'albums',
        href: '/v1/catalog/us/albums/1453399177',
        attributes: {
          artwork: {
            width: 4000,
            height: 4000,
            url:
              'https://is5-ssl.mzstatic.com/image/thumb/Music114/v4/5e/67/77/5e6777d6-3451-6a54-a003-3eee94490704/190295460259.jpg/{w}x{h}bb.jpeg',
            bgColor: '0073a5',
            textColor1: 'ffffff',
            textColor2: 'ffd080',
            textColor3: 'cbe3ed',
            textColor4: 'cbbd87',
          },
          artistName: 'Ninho',
          isSingle: false,
          url: 'https://music.apple.com/us/album/destin/1453399177',
          isComplete: false,
          genreNames: ['Hip-Hop/Rap', 'Music', 'Rap', 'Worldwide', 'France'],
          trackCount: 18,
          isMasteredForItunes: true,
          releaseDate: '2019-03-22',
          name: 'Destin',
          recordLabel: 'Rec. 118 / Mal Luné Music',
          copyright:
            '℗ 2019 Mal Luné Music, licence exclusive Rec. 118 / Warner Music France, a Warner Music Group Company',
          playParams: { id: '1453399177', kind: 'album' },
          editorialNotes: {
            short:
              'A soul-searching rap opus from the author of “Goutte d’eau”.',
          },
          isCompilation: false,
          contentRating: 'explicit',
        },
      },
    },
  },
  {
    id: '218357060',
    type: 'artists',
    href: '/v1/catalog/us/artists/218357060',
    attributes: {
      url: 'https://music.apple.com/us/artist/nin/218357060',
      genreNames: ['Singer/Songwriter'],
      name: 'Nin',
    },
    relationships: {
      topAlbum: {
        id: '1462527757',
        type: 'albums',
        href: '/v1/catalog/us/albums/1462527757',
        attributes: {
          artwork: {
            width: 3000,
            height: 3000,
            url:
              'https://is3-ssl.mzstatic.com/image/thumb/Music123/v4/b6/9d/4b/b69d4bad-b45c-2d44-8fd2-163d86005c85/8719729452893.png/{w}x{h}bb.jpeg',
            bgColor: 'd5d5d7',
            textColor1: '000000',
            textColor2: '1a1d24',
            textColor3: '2a2a2b',
            textColor4: '3f4248',
          },
          artistName: 'Nin',
          isSingle: true,
          url: 'https://music.apple.com/us/album/3nin02-single/1462527757',
          isComplete: true,
          genreNames: ['Hip-Hop/Rap', 'Music'],
          trackCount: 1,
          isMasteredForItunes: false,
          releaseDate: '2019-05-11',
          name: '3nin02 - Single',
          recordLabel: 'Spinnup',
          copyright: '℗ 2019 NIN, distributed by Spinnup',
          playParams: { id: '1462527757', kind: 'album' },
          isCompilation: false,
          contentRating: 'explicit',
        },
      },
    },
  },
  {
    id: '1498633931',
    type: 'artists',
    href: '/v1/catalog/us/artists/1498633931',
    attributes: {
      url: 'https://music.apple.com/us/artist/jay-nin/1498633931',
      genreNames: ['Hip-Hop/Rap'],
      name: 'Jay Nin',
    },
    relationships: {
      topAlbum: {
        id: '1499393511',
        type: 'albums',
        href: '/v1/catalog/us/albums/1499393511',
        attributes: {
          artwork: {
            width: 3000,
            height: 3000,
            url:
              'https://is4-ssl.mzstatic.com/image/thumb/Music124/v4/26/39/6f/26396fff-37a0-795f-8cdf-e5c081ef16e6/artwork.jpg/{w}x{h}bb.jpeg',
            bgColor: '376f27',
            textColor1: 'fafcf9',
            textColor2: '92ed5a',
            textColor3: 'd3e0cf',
            textColor4: '80d450',
          },
          artistName: 'Jay Nin',
          isSingle: true,
          url: 'https://music.apple.com/us/album/avocado-single/1499393511',
          isComplete: true,
          genreNames: ['Hip-Hop/Rap', 'Music'],
          trackCount: 1,
          isMasteredForItunes: false,
          releaseDate: '2020-02-11',
          name: 'Avocado - Single',
          recordLabel: '1711407 Records DK',
          copyright: '℗ 2020 1711407 Records DK',
          playParams: { id: '1499393511', kind: 'album' },
          isCompilation: false,
        },
      },
    },
  },
  {
    id: '32865021',
    type: 'artists',
    href: '/v1/catalog/us/artists/32865021',
    attributes: {
      url: 'https://music.apple.com/us/artist/khadja-nin/32865021',
      genreNames: ['Worldwide'],
      name: 'Khadja Nin',
    },
    relationships: {
      topAlbum: {
        id: '254280763',
        type: 'albums',
        href: '/v1/catalog/us/albums/254280763',
        attributes: {
          artwork: {
            width: 600,
            height: 600,
            url:
              'https://is5-ssl.mzstatic.com/image/thumb/Music/a9/10/44/mzi.qlyhlxty.jpg/{w}x{h}bb.jpeg',
            bgColor: '28160c',
            textColor1: 'f1e0cf',
            textColor2: 'e6d3b5',
            textColor3: 'c9b8a8',
            textColor4: 'c0ae94',
          },
          artistName: 'Khadja Nin',
          isSingle: false,
          url: 'https://music.apple.com/us/album/ya-pili/254280763',
          isComplete: true,
          genreNames: ['African', 'Music', 'Worldwide', 'Worldbeat'],
          trackCount: 13,
          isMasteredForItunes: false,
          releaseDate: '1994-12-02',
          name: 'Ya Pili',
          recordLabel: 'Ariola',
          copyright: '℗ 1994, BMG Ariola belgium NV/SA',
          playParams: { id: '254280763', kind: 'album' },
          isCompilation: false,
        },
      },
    },
  },
  {
    id: '1542502493',
    type: 'artists',
    href: '/v1/catalog/us/artists/1542502493',
    attributes: {
      url: 'https://music.apple.com/us/artist/n-i-n/1542502493',
      genreNames: ['Hip-Hop/Rap'],
      name: 'N.I.N',
    },
    relationships: {
      topAlbum: {
        id: '1542508376',
        type: 'albums',
        href: '/v1/catalog/us/albums/1542508376',
        attributes: {
          artwork: {
            width: 3000,
            height: 3000,
            url:
              'https://is2-ssl.mzstatic.com/image/thumb/Music124/v4/11/f1/8e/11f18e2e-cbe3-84cb-9fed-51f0ea635f83/859756369011_cover.jpg/{w}x{h}bb.jpeg',
            bgColor: '050503',
            textColor1: 'e1ece3',
            textColor2: 'c6d6c3',
            textColor3: 'b5beb6',
            textColor4: '9fac9d',
          },
          artistName: 'N.I.N',
          isSingle: false,
          url:
            'https://music.apple.com/us/album/nozomi-yamaguchi-presents-n-i-n/1542508376',
          isComplete: true,
          genreNames: ['Hip-Hop/Rap', 'Music'],
          trackCount: 9,
          isMasteredForItunes: false,
          releaseDate: '2020-12-12',
          name: 'Nozomi Yamaguchi Presents N.I.N',
          recordLabel: '10ple. Records',
          copyright: '℗ 2020 10ple. Records',
          playParams: { id: '1542508376', kind: 'album' },
          isCompilation: false,
        },
      },
    },
  },
  {
    id: '294328850',
    type: 'artists',
    href: '/v1/catalog/us/artists/294328850',
    attributes: {
      url: 'https://music.apple.com/us/artist/comunidade-nin-jitsu/294328850',
      genreNames: ['Baile Funk'],
      name: 'Comunidade Nin-jitsu',
    },
    relationships: {
      topAlbum: {
        id: '808750938',
        type: 'albums',
        href: '/v1/catalog/us/albums/808750938',
        attributes: {
          artwork: {
            width: 1440,
            height: 1440,
            url:
              'https://is1-ssl.mzstatic.com/image/thumb/Music4/v4/f1/e8/e1/f1e8e1b9-4cab-cf2d-d8e6-9005f165c5db/cover.jpg/{w}x{h}bb.jpeg',
            bgColor: '3695a7',
            textColor1: '050303',
            textColor2: '161616',
            textColor3: '0f2023',
            textColor4: '1c2f33',
          },
          artistName: 'Comunidade Nin-jitsu',
          isSingle: false,
          url:
            'https://music.apple.com/us/album/ao-vivo-no-opini%C3%A3o/808750938',
          isComplete: true,
          genreNames: ['Rock', 'Music'],
          trackCount: 20,
          isMasteredForItunes: false,
          releaseDate: '2013-12-10',
          name: 'Ao Vivo no Opinião',
          recordLabel: 'Coqueiro Verde Records',
          copyright: '℗ 2013 Coqueiro Verde Records',
          playParams: { id: '808750938', kind: 'album' },
          isCompilation: false,
          contentRating: 'explicit',
        },
      },
    },
  },
  {
    id: '1222137951',
    type: 'artists',
    href: '/v1/catalog/us/artists/1222137951',
    attributes: {
      url: 'https://music.apple.com/us/artist/nin-zi-may/1222137951',
      genreNames: ['Pop'],
      name: 'Nin Zi May',
    },
    relationships: {
      topAlbum: {
        id: '1424571787',
        type: 'albums',
        href: '/v1/catalog/us/albums/1424571787',
        attributes: {
          artwork: {
            width: 1425,
            height: 1425,
            url:
              'https://is2-ssl.mzstatic.com/image/thumb/Music118/v4/00/25/2c/00252c03-e98b-4cd0-d60f-af5938b55be0/5057917249560_cover.jpg/{w}x{h}bb.jpeg',
            bgColor: 'abd3da',
            textColor1: '0d0d13',
            textColor2: '012831',
            textColor3: '2d343b',
            textColor4: '234a53',
          },
          artistName: 'Ye Lay',
          isSingle: true,
          url:
            'https://music.apple.com/us/album/nin-ma-shi-tae-nout-feat-nin-zi-may-single/1424571787',
          isComplete: true,
          genreNames: ['Pop', 'Music'],
          trackCount: 1,
          isMasteredForItunes: false,
          releaseDate: '2018-08-09',
          name: 'Nin Ma Shi Tae Nout (feat. Nin Zi May) - Single',
          recordLabel: 'Legacy Music Network',
          copyright: '℗ 2018 Legacy Music Network',
          playParams: { id: '1424571787', kind: 'album' },
          isCompilation: false,
        },
      },
    },
  },
  {
    id: '937493147',
    type: 'artists',
    href: '/v1/catalog/us/artists/937493147',
    attributes: {
      url: 'https://music.apple.com/us/artist/mr-nin-nin/937493147',
      genreNames: ["Children's Music"],
      name: 'Mr Nin Nin',
    },
    relationships: {
      topAlbum: {
        id: '1135032778',
        type: 'albums',
        href: '/v1/catalog/us/albums/1135032778',
        attributes: {
          artwork: {
            width: 1500,
            height: 1500,
            url:
              'https://is3-ssl.mzstatic.com/image/thumb/Music18/v4/b4/34/e5/b434e5c5-5cb2-5532-12d5-e7ae2c425e29/AZNBn.png/{w}x{h}bb.jpeg',
            bgColor: 'dbe3cc',
            textColor1: '1d252e',
            textColor2: '0a333f',
            textColor3: '434b4d',
            textColor4: '34565b',
          },
          artistName: 'Mr Nin Nin',
          isSingle: false,
          url:
            'https://music.apple.com/us/album/pop-lullabies-vol-1/1135032778',
          isComplete: true,
          genreNames: ["Children's Music", 'Music', 'Soundtrack'],
          trackCount: 13,
          isMasteredForItunes: false,
          releaseDate: '2016-07-05',
          name: 'Pop Lullabies, Vol. 1',
          recordLabel: 'Reminiscence Publishing LLC',
          copyright: '℗ 2016 Reminiscence Publishing LLC',
          playParams: { id: '1135032778', kind: 'album' },
          isCompilation: false,
        },
      },
    },
  },
  {
    id: '597879583',
    type: 'artists',
    href: '/v1/catalog/us/artists/597879583',
    attributes: {
      url: 'https://music.apple.com/us/artist/comunidade-nin-jitsu/597879583',
      genreNames: ['Rock'],
      name: 'Comunidade Nin Jitsu',
    },
    relationships: {
      topAlbum: {
        id: '1000723363',
        type: 'albums',
        href: '/v1/catalog/us/albums/1000723363',
        attributes: {
          artwork: {
            width: 1512,
            height: 1512,
            url:
              'https://is2-ssl.mzstatic.com/image/thumb/Music7/v4/eb/c5/16/ebc516db-fecd-e585-064c-e73d6d3e93ce/886443687981.jpg/{w}x{h}bb.jpeg',
            bgColor: 'd3cbc0',
            textColor1: '0a0000',
            textColor2: '34241d',
            textColor3: '322826',
            textColor4: '53453e',
          },
          artistName: 'Comunidade Nin Jitsu',
          isSingle: false,
          url:
            'https://music.apple.com/us/album/maicou-douglas-syndrome/1000723363',
          isComplete: true,
          genreNames: ['MPB', 'Music', 'Brazilian'],
          trackCount: 12,
          isMasteredForItunes: false,
          releaseDate: '2003-12-15',
          name: 'Maicou Douglas Syndrome',
          recordLabel: 'Chaos',
          copyright: '℗ 2001 Sony Music Entertainment (Brazil) I.C.L.',
          playParams: { id: '1000723363', kind: 'album' },
          isCompilation: false,
          contentRating: 'clean',
        },
      },
    },
  },
]

export const appleArtistResultNIN = {
  id: '107917',
  type: 'artists',
  href: '/v1/catalog/us/artists/107917',
  attributes: {
    genreNames: ['Alternative'],
    name: 'Nine Inch Nails',
    url: 'https://music.apple.com/us/artist/nine-inch-nails/107917',
  },
  relationships: {
    topAlbum: {
      id: '1440837096',
      type: 'albums',
      href: '/v1/catalog/us/albums/1440837096',
      attributes: {
        artwork: {
          width: 3000,
          height: 3000,
          url:
            'https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/e7/9f/f4/e79ff471-b461-d01d-bd47-6d6a5ec1f1a1/15UMGIM67680.rgb.jpg/{w}x{h}bb.jpeg',
          bgColor: 'ebd9ab',
          textColor1: '120704',
          textColor2: '403117',
          textColor3: '3d3126',
          textColor4: '625235',
        },
        artistName: 'Nine Inch Nails',
        isSingle: false,
        url: 'https://music.apple.com/us/album/the-downward-spiral/1440837096',
        isComplete: true,
        genreNames: [
          'Alternative',
          'Music',
          'Electronic',
          'Industrial',
          'Rock',
          'Hard Rock',
          'Metal',
          'Adult Alternative',
        ],
        trackCount: 14,
        isMasteredForItunes: true,
        releaseDate: '1994-03-08',
        name: 'The Downward Spiral',
        recordLabel: 'Nothing',
        copyright: '℗ 2015 Nothing/Interscope Records',
        playParams: { id: '1440837096', kind: 'album' },
        editorialNotes: {
          standard:
            'When Trent Reznor submitted <i>The Downward Spiral</i> to Interscope Records cofounder Jimmy Iovine, he offered an apology. “Sorry,” Reznor remembered saying in a 2016 interview with Beats 1 host Zane Lowe. “I had to do it.” \nEven in a shifted mid-’90s paradigm where bands like Nirvana could become famous, <i>Spiral</i> felt extreme—a blast of negativity so thorough that it’s hard to imagine it making headway with any size of audience, let alone the four million or so who ended up buying it. (Reznor himself remains somewhat incredulous—a note on the band’s own website describes the album as a “celebration of self-destruction in the form of a concept record that somehow managed to become a multi-platinum worldwide hit.”)<br />\nInspired by Iggy Pop, Lou Reed, and David Bowie’s Berlin trilogy (<i>Low</i> in particular), <i>Spiral</i> pushed the industrial pop of <i>Pretty Hate Machine</i> and the <i>Broken</i> EP in unexpected directions, experimenting with torch songs (“Piggy”), disco and soul (“Closer”), and ballads of such unnerving fragility that listening to them feels voyeuristic (“Hurt”). Even tracks that found continuity with the band’s earlier music—“Big Man with a Gun,” the stuttering hardcore of “March of the Pigs”—were drastically more aggressive than anything they’d done before, flashes of mania that made the album’s quieter moments feel all the more exhausted.<br />\nWhat emerged was a pattern of emotional whiplash: You feel like you can topple the world, shred your tormentors, vent your toxic depths. Then, suddenly, you feel nothing. That you could whistle along with half of it was perverse but strangely fitting, especially given Reznor’s S&amp;M affectations: Here was pain that felt pretty good. The album’s sound was just as polarized. Mixing digital and analog, sample collages with live performances, densely processed signals with naturalistic ones, <i>Spiral</i> was a drastic renovation to the texture and feel of conventional rock. Even now, it feels damaged, noisy, wrong—the product of both a gleaming future and an already ruined past. If the album has an essentializing moment, it’s the climax of “Closer”: mechanistic synth-funk that gives way to a warped, solitary piano, part music box, part trash. After <i>Spiral</i>, artists didn’t have to decide whether to be a rock band or an electronic producer—Reznor had bridged the two.<br />\n“I was feeling my way around,” Reznor told Zane Lowe of the period leading up to <i>Spiral</i>. “What do I have to say? I didn’t live this exotic life. I grew up in a town in Pennsylvania. Kind of boring. What I did know is how I felt about myself, and my struggles to figure out who and what and why from my own perspective.” And therein lies the album’s healing irony: In facing the abyss, Reznor found his congregation.',
          short: 'A bold, no-look dive into the abyss, 25 years on.',
        },
        isCompilation: false,
        contentRating: 'explicit',
      },
    },
  },
}

export const appleArtistResult = {
  id: '494026053',
  type: 'artists',
  href: '/v1/catalog/us/artists/494026053',
  attributes: {
    url: 'https://music.apple.com/us/artist/blue-rhythm-combo/494026053',
    name: 'Blue Rhythm Combo',
    genreNames: ['R&B/Soul'],
  },
  relationships: {
    albums: {
      href: '/v1/catalog/us/artists/494026053/albums',
      data: [
        {
          id: '494026002',
          type: 'albums',
          href: '/v1/catalog/us/albums/494026002',
        },
      ],
    },
    topAlbum: {
      id: '494026002',
      type: 'albums',
      href: '/v1/catalog/us/albums/494026002',
      attributes: {
        artwork: {
          width: 800,
          height: 800,
          url:
            'https://is1-ssl.mzstatic.com/image/thumb/Music/b9/dc/c6/mzi.vozaglkx.jpg/{w}x{h}bb.jpeg',
          bgColor: '006ba6',
          textColor1: 'faf8e5',
          textColor2: 'fcece9',
          textColor3: 'c8dbd9',
          textColor4: 'c9d2dc',
        },
        artistName: 'Blue Rhythm Combo',
        isSingle: false,
        url: 'https://music.apple.com/us/album/brcs-groove-ep/494026002',
        isComplete: true,
        genreNames: ['R&B/Soul', 'Music', 'Jazz'],
        trackCount: 6,
        isMasteredForItunes: false,
        releaseDate: '2012-01-30',
        name: "BRC's Groove - EP",
        recordLabel: 'Jazzman',
        copyright: '℗ 2012 Jazzman',
        playParams: {
          id: '494026002',
          kind: 'album',
        },
        isCompilation: false,
      },
    },
  },
}

export const appleArtistNoAlbumResult = {
  id: '494026053',
  type: 'artists',
  href: '/v1/catalog/us/artists/494026053',
  attributes: {
    url: 'https://music.apple.com/us/artist/blue-rhythm-combo/494026053',
    name: 'Blue Rhythm Combo',
    genreNames: ['R&B/Soul'],
  },
  relationships: {
    albums: {
      href: '/v1/catalog/us/artists/494026053/albums',
      data: [
        {
          id: '494026002',
          type: 'albums',
          href: '/v1/catalog/us/albums/494026002',
        },
      ],
    },
    topAlbum: {
      attributes: {
        artwork: { url: '' },
      },
    },
  },
}
