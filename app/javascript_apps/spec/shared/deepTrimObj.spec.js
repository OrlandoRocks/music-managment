import { deepTrimObj } from '../../shared/deepTrimObj'

describe('deepTrimObj', () => {
  it('strips trailing white space from values in object', () => {
    let name = 'Name'
    let artistName = 'Artist Name'
    let obj = {
      name: `${name}      `,
      artists: [{ name: `${artistName}    ` }],
    }

    let trimmedObj = deepTrimObj(obj)

    expect(trimmedObj.name).toEqual(name)
    expect(trimmedObj.artists[0].name).toEqual(artistName)
  })
})
