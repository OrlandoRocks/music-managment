import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SelectorMenu from '../../../language_selector/components/SelectorMenu'

Enzyme.configure({ adapter: new Adapter() })

describe('SelectorMenu', function () {
  let app

  beforeEach(() => {
    app = mount(
      <SelectorMenu
        currentLocaleLang="English"
        languageOptions={[
          {
            country_website_id: 1,
            yml_languages: 'en',
            selector_language: 'English',
            selector_country: 'United States',
            selector_order: 1,
            redirect_country_website_id: null,
          },
          {
            country_website_id: 1,
            yml_languages: 'pt-us',
            selector_language: 'Português',
            selector_country: 'Brazil',
            selector_order: 9,
            redirect_country_website_id: null,
          },
        ]}
      />
    )
  })

  it('renders basic info', function () {
    expect(app.find('.language_display').length).toBe(1)
    expect(app.find('#language_selector').length).toBe(1)
  })

  it('renders language options when clicking the current locale or caret icon', function () {
    app.find('#language_selector .language_display').first().simulate('click')
    expect(app.find('.language_modal_overlay').length).toBe(1)
  })

  it('hides language options by default', function () {
    expect(app.find('.language_modal_overlay').length).toBe(0)
  })
})
