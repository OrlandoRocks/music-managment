import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import LanguageOptions from '../../../language_selector/components/LanguageOptions'

Enzyme.configure({ adapter: new Adapter() })

describe('LanguageOptions', function () {
  let app

  beforeEach(() => {
    app = mount(
      <LanguageOptions
        languageOptions={[
          { selector_language: 'English', yml_languages: 'en' },
          { selector_language: 'Portugues', yml_languages: 'pt-us' },
        ]}
        currentLocaleLang={'English'}
        hideLangOptions={() => {}}
        chooseALang="CHOOSE A LANGUAGE"
      />
    )
  })

  it('renders basic info', function () {
    expect(app.find('.top_section div').first().text()).toMatch(
      'CHOOSE A LANGUAGE'
    )
    expect(app.find('.language_options').length).toBe(1)
  })

  it('displays the current locale as a blue button', function () {
    expect(
      app
        .find('.language_options button[value="en"]')
        .first()
        .hasClass('selected_button')
    ).toEqual(true)
  })

  it('displays other locales as a white button', function () {
    expect(
      app
        .find('.language_options button[value="pt-us"]')
        .first()
        .hasClass('unselected_button')
    ).toEqual(true)
  })
})
