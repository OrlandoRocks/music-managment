import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../compositions/reducers'
import initialStateWithNtcForm from './initialStateWithNtcForm'
import apiMiddleware from '../../../compositions/middlewares/apiMiddleware'
import ntcCompositionValidationMiddleware from '../../../compositions/middlewares/ntcCompositionValidationMiddleware'
import ntcCowriterValidationMiddleware from '../../../compositions/middlewares/ntcCowriterValidationMiddleware'
import submitNtcCompositionMiddleware from '../../../compositions/middlewares/submitNtcCompositionMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialStateWithNtcForm,
    applyMiddleware(
      ntcCompositionValidationMiddleware,
      ntcCowriterValidationMiddleware,
      submitNtcCompositionMiddleware,
      apiMiddleware
    )
  )
})
