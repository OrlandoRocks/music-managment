import composer from './initialStates/composerWithNtcForm'
import compositions from './initialStates/compositions'
import cowriters from './initialStates/cowriters'
import currentComposer from './initialStates/currentComposer'
import ntcComposition from './initialStates/ntcComposition'

export default {
  composer,
  compositions,
  cowriters,
  currentComposer,
  ntcComposition,
}
