import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../compositions/reducers'
import initialStateWithTaxInfo from './initialStateWithTaxInfo'
import apiMiddleware from '../../../compositions/middlewares/apiMiddleware'
import compositionValidationMiddleware from '../../../compositions/middlewares/compositionValidationMiddleware'
import updateCowriterValidationMiddleware from '../../../compositions/middlewares/updateCowriterValidationMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialStateWithTaxInfo,
    applyMiddleware(
      updateCowriterValidationMiddleware,
      compositionValidationMiddleware,
      apiMiddleware
    )
  )
})
