import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../compositions/reducers'
import initialState from './initialState'
import apiMiddleware from '../../../compositions/middlewares/apiMiddleware'
import compositionValidationMiddleware from '../../../compositions/middlewares/compositionValidationMiddleware'
import updateCowriterValidationMiddleware from '../../../compositions/middlewares/updateCowriterValidationMiddleware'
import translatedNameValidationMiddleware from '../../../compositions/middlewares/translatedNameValidationMiddleware'
import updateSplitsValidationMiddleware from '../../../compositions/middlewares/updateSplitsValidationMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      updateCowriterValidationMiddleware,
      translatedNameValidationMiddleware,
      compositionValidationMiddleware,
      updateSplitsValidationMiddleware,
      apiMiddleware
    )
  )
})
