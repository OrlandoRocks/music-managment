import composer from './initialStates/composerWithTaxInfo'
import compositions from './initialStates/compositions'
import currentComposer from './initialStates/currentComposer'
import cowriters from './initialStates/cowriters'
import ntcComposition from './initialStates/ntcComposition'
import pros from './initialStates/pros'

export default {
  composer,
  compositions,
  currentComposer,
  pros,
  cowriters,
  ntcComposition,
}
