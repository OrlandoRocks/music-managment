import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../compositions/reducers'
import initialState from './initialStateWithSplitsSubmitted'
import apiMiddleware from '../../../compositions/middlewares/apiMiddleware'
import compositionValidationMiddleware from '../../../compositions/middlewares/compositionValidationMiddleware'
import updateCowriterValidationMiddleware from '../../../compositions/middlewares/updateCowriterValidationMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      updateCowriterValidationMiddleware,
      compositionValidationMiddleware,
      apiMiddleware
    )
  )
})
