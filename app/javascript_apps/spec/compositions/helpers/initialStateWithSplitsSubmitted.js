import composer from './initialStates/composer'
import compositions from './initialStates/compositionsWithSplitsSubmitted'
import cowriters from './initialStates/cowriters'
import currentComposer from './initialStates/currentComposer'
import currentComposition from './initialStates/currentComposition'
import pros from './initialStates/pros'

export default {
  composer,
  compositions,
  currentComposer,
  currentComposition,
  pros,
  cowriters,
}
