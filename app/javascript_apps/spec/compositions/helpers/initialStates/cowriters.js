import { normalizeCowriters } from '../../../../compositions/store/schema'

let cowritersList = [
  {
    first_name: 'Serena',
    full_name: 'Serena Williams',
    last_name: 'Williams',
    cowriter_share: '1',
    id: 1,
    uuid: 1,
    errors: {},
  },
  {
    first_name: '',
    full_name: '',
    last_name: '',
    cowriter_share: '',
    id: 2,
    uuid: 2,
    errors: {},
  },
  {
    first_name: 'Erik',
    full_name: 'Erik Flowers',
    last_name: 'Flowers',
    cowriter_share: '',
    id: 3,
    uuid: 3,
    errors: {},
  },
]

const {
  entities: { cowriters },
} = normalizeCowriters(cowritersList)
export default cowriters
