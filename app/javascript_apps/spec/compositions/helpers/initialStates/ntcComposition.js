import uuid from 'uuid/v4'

export default {
  uuid: uuid(),
  title: '',
  performingArtist: '',
  albumName: '',
  isrcNumber: '',
  releaseDate: '',
  percent: '',
  cowriters: [],
  errors: {},
}
