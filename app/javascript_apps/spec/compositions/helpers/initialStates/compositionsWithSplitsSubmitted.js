import { normalizeCompositions } from '../../../../compositions/store/schema'

let compositionsList = [
  {
    id: 1,
    appears_on: 'First Album',
    composer_share: '100.0',
    composition_title: 'Song 1',
    cowriter_share: '0.0',
    cowriters: [],
    release_date: '2018-09-27',
    status: 'accepted',
    errors: {},
  },
  {
    id: 2,
    appears_on: 'Second Album',
    composer_share: '75.0',
    composition_title: 'Song 2',
    cowriter_share: '25.0',
    cowriters: [],
    release_date: '2018-09-27',
    status: 'accepted',
    errors: {},
  },
  {
    id: 3,
    appears_on: 'Keep it 100',
    composer_share: '100.0',
    composition_title: 'Song 3',
    cowriter_share: '0.0',
    cowriters: [],
    release_date: '2018-09-27',
    status: 'accepted',
    errors: {},
  },
  {
    id: 4,
    appears_on: 'The Terminator',
    composer_share: '50',
    composition_title: 'Song 4',
    cowriter_share: '0.0',
    cowriters: [],
    release_date: '2018-09-27',
    status: 'terminated',
    errors: {},
  },
]

const {
  entities: { compositions },
} = normalizeCompositions(compositionsList)
export default compositions
