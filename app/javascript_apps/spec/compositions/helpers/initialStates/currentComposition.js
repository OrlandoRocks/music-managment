import compositions from './compositions'
import findNextEligibleComposition from '../../../../compositions/utils/findNextEligibleComposition'

const composition = compositions[compositions.length - 1]
const currentComposition = findNextEligibleComposition(
  compositions,
  composition
)

export default currentComposition
