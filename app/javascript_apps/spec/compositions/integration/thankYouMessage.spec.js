import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStoreWithNtcForm from '../helpers/storeWithNtcForm'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'
import { ADD_NTC_COMPOSITION_SUCCESS } from '../../../compositions/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

describe('Thank You Message Modal', () => {
  describe('registering a non tunecore composition', () => {
    let store, app

    beforeEach(() => {
      let features = {
        ntcUrl: '',
        ntcCompositionFormEnabled: true,
        canEditCompositions: true,
      }

      store = createStoreWithNtcForm()
      app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )

      app.find('.add-compositions-link').simulate('click')

      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let albumName = app.find('.albumName > input')
      albumName.simulate('change', {
        target: { name: 'albumName', value: 'Album Name' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: ADD_NTC_COMPOSITION_SUCCESS,
            data: {
              id: 100,
              appears_on: 'My Album',
              composition_title: 'Composition Name',
              composer_share: 75.0,
              cowriter_share: 10.0,
              cowriters: [],
              cowriter_splits: [],
              errors: {},
            },
          })
        },
      }

      app.find('.register-btn').simulate('click')
    })

    it('should render the thank you message modal after successful registration', () => {
      let headline = app.find('.thank-you-message > strong > p').text()
      let p1 = app.find('.thank-you-message > p').at(0).text()
      let p2 = app.find('.thank-you-message > p').at(1).text()

      expect(headline).toMatch('Thank you')
      expect(p1).toMatch('Thank you for submitting')
      expect(p2).toMatch('If you have any trouble')
    })

    describe('closing thank you modal', () => {
      it('should close on when the user clicks the "X"', () => {
        let presentHeader = app.find('.header > div:first-child').exists()
        expect(presentHeader).toBeTruthy()

        app.find('.close-btn').simulate('click')

        let absentHeader = app.find('.header > div:first-child').exists()
        expect(absentHeader).toBeFalsy()
      })

      it('should close when the user clicks the cancel button', () => {
        let header = app.find('.header > div:first-child').exists()
        expect(header).toBeTruthy()

        app.find('.cancel-btn').simulate('click')

        let absentHeader = app.find('.header > div:first-child').exists()
        expect(absentHeader).toBeFalsy()
      })
    })
  })
})
