import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import {
  UPDATE_COWRITER_ERRORS,
  CREATE_SPLITS_SUCCESS,
} from '../../../compositions/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

describe('Cowriters section', function () {
  let app, store

  beforeEach(() => {
    store = createStore()
    app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    const compositionIndex = 1
    let composition = store.getState().compositions[1]

    let addShareLink = app.find(`#add_shares_button_${composition.id}`)
    addShareLink.simulate('click', {
      target: {
        name: 'add-shares',
        dataset: { id: 1, index: compositionIndex },
      },
    })

    let shareInput = app.find('#add_shares_percent_input')
    shareInput.simulate('change', {
      target: { name: 'composer_share', value: '50' },
    })

    let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
    listCowritersBtn.simulate('click')
  })

  describe('List Additional Co-Writers button', function () {
    it('should be disabled and an error message should appear if the total share percentage exceeds 100 percent', function () {
      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '50.1' },
      })

      let addCowriterBtn = app.find(
        '.publishing-splits-cowriters-add-cowriter-btn'
      )
      expect(addCowriterBtn.prop('disabled')).toBeTruthy()

      let cowriterShareError = app
        .find('.publishing-splits-cowriters-header')
        .parent()
        .hasClass('error-container')
      expect(cowriterShareError).toBeTruthy()
    })

    it('should be disabled and an error message should appear if the individual share percentage exceeds 100 percent', function () {
      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '100.1' },
      })

      let addCowriterBtn = app.find(
        '.publishing-splits-cowriters-add-cowriter-btn'
      )
      expect(addCowriterBtn.prop('disabled')).toBeTruthy()

      let cowriterShareError = app
        .find('.publishing-splits-cowriters-share-input')
        .parent()
        .hasClass('error-container')
      expect(cowriterShareError).toBeTruthy()
    })

    it('should be enabled if the total share percentage is between 1 and 100 percent', function () {
      let firstName = app.find('.publishing-splits-cowriters-first-name').at(1)
      firstName.simulate('change', {
        target: { name: 'first_name', value: 'Serena' },
      })

      let lastName = app.find('.publishing-splits-cowriters-last-name').at(1)
      lastName.simulate('change', {
        target: { name: 'last_name', value: 'Williams' },
      })

      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      let addCowriterBtn = app.find(
        '.publishing-splits-cowriters-add-cowriter-btn'
      )
      expect(addCowriterBtn.prop('disabled')).toBeTruthy()

      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '49' },
      })

      let submitBtn = app.find('.publishing-splits-footer-btn-wrapper-submit')
      expect(submitBtn.prop('disabled')).not.toBeTruthy()

      addCowriterBtn = app.find('.publishing-splits-cowriters-add-cowriter-btn')
      expect(addCowriterBtn.prop('disabled')).not.toBeTruthy()
    })
  })

  describe('Remove cowriter button', function () {
    it('should remove the cowriter from the store', function () {
      const { cowriters, compositions } = store.getState()
      const composition = compositions[1]
      const cowriterUuid = composition.cowriters[0]

      let removeCowriterBtn = app.find(
        '.publishing-splits-cowriters-row-cancel-btn a'
      )
      removeCowriterBtn.simulate('click')

      expect(compositions.cowriters).not.toEqual([])
      expect(cowriters[cowriterUuid]).not.toBeUndefined()

      let updatedCompositions = store.getState().compositions
      let updatedCowriters = store.getState().cowriters

      expect(updatedCompositions[1].cowriters).toEqual([])
      expect(updatedCowriters[cowriterUuid]).toBeUndefined()
    })
  })

  describe('Updating cowriters on composition share submission', function () {
    it('should update the composition cowriters and cowriters state', function () {
      const composition = store.getState().compositions[1]
      const cowriter = store.getState().cowriters[1]

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: CREATE_SPLITS_SUCCESS,
            data: {
              composition_id: composition.id,
              composer_percent: '66',
              cowriter_percent: '33',
              cowriters: [{ ...cowriter, cowriter_share: '33' }],
            },
          })
        },
      }

      let firstName = app.find('.publishing-splits-cowriters-first-name').at(1)
      firstName.simulate('change', {
        target: { name: 'first_name', value: 'Serena' },
      })

      let lastName = app.find('.publishing-splits-cowriters-last-name').at(1)
      lastName.simulate('change', {
        target: { name: 'last_name', value: 'Williams' },
      })

      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '33' },
      })

      let submitBtn = app.find('.publishing-splits-footer-btn-wrapper-submit')
      expect(submitBtn.prop('disabled')).not.toBeTruthy()
      submitBtn.simulate('click')

      let updatedComposition = store.getState().compositions[1]
      expect(updatedComposition.composer_share).toEqual('66')
      expect(updatedComposition.cowriter_share).toEqual('33')

      let updatedCowriter = store.getState().cowriters[cowriter.id]
      expect(updatedCowriter.first_name).toEqual('Serena')
      expect(updatedCowriter.last_name).toEqual('Williams')
      expect(updatedCowriter.cowriter_share).toEqual('33')
      expect(updatedCowriter.uuid).toEqual(cowriter.id)
      expect(updatedComposition.cowriters[0]).toEqual(cowriter.id)
    })
  })

  describe('validations', function () {
    it('updates the cowriters errors state if a required field is blank', function () {
      const composition = store.getState().compositions[1]
      const uuid = composition.cowriters[0]
      let errors = { first_name: true, last_name: false, cowriter_share: true }

      store.dispatch({
        type: UPDATE_COWRITER_ERRORS,
        uuid,
        errors,
      })

      let updatedCowriter = store.getState().cowriters[uuid]
      expect(updatedCowriter.errors.first_name).toEqual(true)
      expect(updatedCowriter.errors.last_name).toEqual(false)
      expect(updatedCowriter.errors.cowriter_share).toEqual(true)
    })
  })
})
