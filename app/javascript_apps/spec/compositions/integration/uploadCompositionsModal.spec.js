import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'
import UploadCompositionsModal from '../../../compositions/components/AddCompositions/UploadCompositionsModal'

Enzyme.configure({ adapter: new Adapter() })

describe('Upload Composition Modal', () => {
  let store, app

  beforeEach(() => {
    store = createStore()

    let features = {
      bulkUploadCompositions: true,
    }

    app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    app.find('.upload-compositions-link').simulate('click')
  })

  it('displays the upload title of the modal', () => {
    let header = app.find('.header > div:first-child')

    expect(header).toHaveText('Bulk Upload Compositions ')
  })

  describe('close modal', () => {
    it('should close on when the user clicks the "X"', () => {
      let presentHeader = app.find('.header > div:first-child').exists()
      expect(presentHeader).toBeTruthy()

      app.find('.close-btn').simulate('click')

      let absentHeader = app.find('.header > div:first-child').exists()
      expect(absentHeader).toBeFalsy()
    })
  })

  describe('Upload message', () => {
    context('upload in progress', () => {
      beforeEach(() => {
        app = mount(
          <UploadCompositionsModal
            handleUpload={() => {}}
            closeModal={() => {}}
            uploadState="in_progress"
          />
        )
      })

      it('should display upload in progress message when state is uploading', () => {
        let uploadMessage = app.find('.upload-controls p.upload-message')

        expect(uploadMessage).toHaveText('Uploading & validating your file...')
      })

      it('should disable button when upload in progress', () => {
        let uploadBtn = app.find('.upload-btn')
        expect(uploadBtn.prop('disabled')).toBeTruthy()
      })
    })

    context('upload errored', () => {
      it('should display default error message when no error message sent', () => {
        app = mount(
          <UploadCompositionsModal
            handleUpload={() => {}}
            closeModal={() => {}}
            uploadState="error"
          />
        )

        let uploadMessage = app.find('.upload-controls p.upload-message')

        expect(uploadMessage).toHaveText(
          'Error processing the Bulk Upload. Please try again'
        )
      })

      it('should display error message when provided', () => {
        app = mount(
          <UploadCompositionsModal
            handleUpload={() => {}}
            closeModal={() => {}}
            uploadState="error"
            uploadError="Oh no error."
          />
        )

        let uploadMessage = app.find('.upload-controls p.upload-message')

        expect(uploadMessage).toHaveText('Oh no error.')
      })
    })

    context('upload success', () => {
      beforeEach(() => {
        app = mount(
          <UploadCompositionsModal
            handleUpload={() => {}}
            closeModal={() => {}}
            uploadState="success"
          />
        )
      })

      it('should display upload in progress message when state is uploading', () => {
        let uploadMessage = app.find('.upload-controls p.upload-message')

        expect(uploadMessage).toHaveText(
          'Bulk Upload Succeeded. Your compositions will be created shortly.'
        )
      })
    })
  })
})
