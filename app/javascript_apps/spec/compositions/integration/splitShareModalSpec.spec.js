import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import {
  CREATE_SPLITS_SUCCESS,
  UPDATE_COMPOSITION_SUCCESS,
} from '../../../compositions/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

describe('Split Share Modal', function () {
  let app, store, compositions

  beforeEach(() => {
    store = createStore()
    compositions = store.getState().compositions
    app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )
  })

  it('should display the index of the composition in the modal header', function () {
    const composition = compositions[1]
    const compositionIndex = 1
    const compostionsLength = Object.keys(compositions).length

    let addShareLink = app.find(`#add_shares_button_${composition.id}`)
    addShareLink.simulate('click', {
      target: {
        name: 'add-shares',
        dataset: { id: composition.id, index: compositionIndex },
      },
    })

    let header = app.find('.publishing-splits-header').at(1)
    expect(header).toHaveText(
      `Report Writer & Co-Writer Shares (${compositionIndex} of ${compostionsLength})`
    )
  })

  describe('List Co-Writers button', function () {
    it('should be disabled if the composers share is empty', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
      expect(listCowritersBtn.prop('disabled')).toBeTruthy()
    })

    it('should be disabled if the composers share is over 100 percent', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '100.1' },
      })

      let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
      expect(listCowritersBtn.prop('disabled')).toBeTruthy()
    })

    it('should be enabled if the composers share is between 1 and 100 percent', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
      expect(listCowritersBtn.prop('disabled')).not.toBeTruthy()
    })

    it('should be enabled if the composers adds a cowriter and then removes it', () => {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      app.find('.publishing-splits-cowriters-link-btn').simulate('click')

      let missingListCowritersBtn = app.find(
        '.publishing-splits-cowriters-link-btn'
      )
      expect(missingListCowritersBtn.length).toEqual(0)

      app
        .find('.publishing-splits-cowriters-row-cancel-btn a')
        .simulate('click')

      let newListCowritersBtn = app.find(
        '.publishing-splits-cowriters-link-btn'
      )
      expect(newListCowritersBtn.length).toEqual(1)
    })
  })

  describe('translated name', function () {
    it('should show translated name if composer name has invalid characters', function () {
      const composition = compositions[5]

      const addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 5, index: 5 } },
      })

      const shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      const translatedNameInput = app.find('.translated-name-input')
      expect(translatedNameInput.length).toEqual(1)
    })

    it('should not show translated name input if composer name has accented characters', function () {
      const composition = compositions[6]

      const addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 6, index: 6 } },
      })

      const shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      const translatedNameInput = app.find('.translated-name-input')
      expect(translatedNameInput.length).toEqual(0)
    })

    it('should not show translated name input if composer name has numbers', function () {
      const composition = compositions[3]

      const addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 3, index: 3 } },
      })

      const shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      const translatedNameInput = app.find('.translated-name-input')
      expect(translatedNameInput.length).toEqual(0)
    })
  })

  describe('Register and Continue button', function () {
    it('should be disabled if the composers share is empty', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeTruthy()
    })

    it('should be disabled and an error msg should appear if the composers share is over 100 percent', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '100.1' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeTruthy()

      let composerShareError = app
        .find('#add_shares_percent_input')
        .parent()
        .hasClass('error-container')
      expect(composerShareError).toBeTruthy()
    })

    it('should be disabled if the total shares are over 100 percent', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      app.find('.publishing-splits-cowriters-link-btn').simulate('click')

      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '55' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeTruthy()
    })

    it('should be disabled if the composers translated name is empty', function () {
      const composition = compositions[5]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 5, index: 5 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      let translatedNameInput = app.find('.translated-name-input')
      translatedNameInput.simulate('change', {
        target: { name: 'translated_name', value: '' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeTruthy()
    })

    it('should be disabled if the composers translated name contains invalid characters', function () {
      const composition = compositions[5]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 5, index: 5 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      let translatedNameInput = app.find('.translated-name-input')
      translatedNameInput.simulate('change', {
        target: { name: 'translated_name', value: 'Tom Brady is so dѲpe' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeTruthy()

      let translatedNameError = app
        .find('.translated-name-input')
        .parent()
        .hasClass('error-container')
      expect(translatedNameError).toBeTruthy()
    })

    it('should be enabled if the composers translated name contains only latin / accented characters', function () {
      const composition = compositions[5]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 5, index: 5 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '10' },
      })

      let translatedNameInput = app.find('.translated-name-input')
      translatedNameInput.simulate('change', {
        target: { name: 'translated_name', value: 'Tom Brady is śo dopé' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).toBeFalsy()
    })

    it('should be enabled if the composers share is between 1 and 100 percent', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      let registerAndContinueBtn = app.find('#register_and_continue')
      expect(registerAndContinueBtn.prop('disabled')).not.toBeTruthy()
    })
  })

  describe('Close modal button', function () {
    it('should remove cowriters when the button is clicked', function () {
      const composition = compositions[1]

      expect(composition.cowriters.length).toEqual(0)

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      let addCowriterBtn = app.find('#add-cowriter-btn')
      addCowriterBtn.simulate('click')

      let compositionWithCowriter = store.getState().compositions[1]
      expect(compositionWithCowriter.cowriters.length).toEqual(1)

      let cancelBtn = app.find('#cancel-btn')
      cancelBtn.simulate('click')

      let updatedComposition = store.getState().compositions[1]

      expect(updatedComposition.cowriters.length).toEqual(0)
    })
  })

  describe('Submit shares for composition', function () {
    it('should update the compositions composer share and cowriter share', function () {
      const composition = compositions[1]

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: CREATE_SPLITS_SUCCESS,
            data: {
              composition_id: composition.id,
              composer_percent: '75.0',
              cowriter_percent: '25.0',
              cowriters: [store.getState().cowriters[1]],
            },
          })
        },
      }

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '75' },
      })

      expect(composition.composer_share).toEqual('0.0')

      let submitBtn = app.find('.publishing-splits-footer-btn-wrapper-submit')
      submitBtn.simulate('click')

      let updatedCompositions = store.getState().compositions
      let updatedComposition = updatedCompositions[1]

      expect(updatedComposition.composer_share).toEqual('75.0')
      expect(updatedComposition.cowriter_share).toEqual('25.0')
    })
  })

  describe('Hide a composition', function () {
    it("should update the composition's state to hidden and remove the composition from the view", function () {
      const composition = compositions[1]

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: UPDATE_COMPOSITION_SUCCESS,
            data: {
              id: 1,
              composition_title: 'Song 1',
              state: 'hidden',
            },
          })
        },
      }

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let hideCompositionPrompt = app.find('#hide_composition_link')
      hideCompositionPrompt.simulate('click')

      let hideCompositionConfirmation = app.find(
        '#hide_composition_confirmation'
      )
      hideCompositionConfirmation.simulate('click')

      let hiddenComposition = store.getState().compositions[1]
      expect(hiddenComposition).toBeUndefined()
    })
  })

  describe('skipping a composition', function () {
    it('should generate a prompt when a share has been entered after pressing the skip button', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '75' },
      })

      let hideCompositionPrompt = app.find('#skip_composition_link')
      hideCompositionPrompt.simulate('click')

      let hideCompositionConfirmation = app.find(
        '#skip_composition_confirmation'
      )

      expect(hideCompositionConfirmation).toBeTruthy()
    })
  })

  describe('composer_share', function () {
    it('updates the composer share', function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '75' },
      })

      let updatedComposition = store.getState().compositions[1]
      expect(updatedComposition.composer_share).toEqual(75)
    })
  })

  describe('composition share validation', function () {
    it(`should remove the total share validation when updating the composer share
        percentage to a value that puts the total share percentage below 100`, function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '25' },
      })

      let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
      listCowritersBtn.simulate('click')

      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '75.1' },
      })

      let cowriterShareError = app
        .find('.publishing-splits-cowriters-header')
        .parent()
        .hasClass('error-container')
      expect(cowriterShareError).toBeTruthy()

      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '2' },
      })

      let updatedCowriterShareError = app
        .find('.publishing-splits-cowriters-header')
        .parent()
        .hasClass('error-container')
      expect(updatedCowriterShareError).not.toBeTruthy()
    })

    it(`should remove the total share validation when updating the cowriter share
        percentage to a value that puts the total share percentage below 100`, function () {
      const composition = compositions[1]

      let addShareLink = app.find(`#add_shares_button_${composition.id}`)
      addShareLink.simulate('click', {
        target: { name: 'add-shares', dataset: { id: 1, index: 1 } },
      })

      let shareInput = app.find('#add_shares_percent_input')
      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '25' },
      })

      let listCowritersBtn = app.find('.publishing-splits-cowriters-link-btn')
      listCowritersBtn.simulate('click')

      let cowriterShare = app.find('.publishing-splits-cowriters-share-input')
      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      shareInput.simulate('change', {
        target: { name: 'composer_share', value: '75' },
      })

      let shareInputError = app
        .find('.publishing-splits-cowriters-header')
        .parent()
        .hasClass('error-container')
      expect(shareInputError).toBeTruthy()

      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      let cowriterShareError = app
        .find('.publishing-splits-cowriters-header')
        .parent()
        .hasClass('error-container')
      expect(cowriterShareError).toBeTruthy()

      cowriterShare.simulate('change', {
        target: { name: 'cowriter_share', value: '5' },
      })

      let updatedShareInputError = app
        .find('#add_shares_percent_input')
        .parent()
        .hasClass('error-container')
      expect(updatedShareInputError).not.toBeTruthy()

      let updatedCowriterShareError = app
        .find('.publishing-splits-cowriters-share-input')
        .parent()
        .hasClass('error-container')
      expect(updatedCowriterShareError).not.toBeTruthy()
    })
  })
})
