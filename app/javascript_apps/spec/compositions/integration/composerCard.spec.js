import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import createStoreWithTaxInfo from '../helpers/storeWithTaxInfo'
import createStoreWithSplitsSubmitted from '../helpers/storeWithSplitsSubmitted'
import createStoreWithNtcForm from '../helpers/storeWithNtcForm'
import { Provider } from 'react-redux'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'
import {
  RESET_COMPOSER,
  SAVE_COMPOSER,
  UPDATE_COMPOSER,
} from '../../../compositions/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

xdescribe('opening the edit composer modal', function () {
  it('overwrites unsaved changes', function () {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    app.find('#editLink').simulate('click')

    let modal = app.find('.composer-modal')
    const firstName = modal.find('#firstName')
    let currentComposer = store.getState().currentComposer
    currentComposer.first_name = 'Carlos'

    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: UPDATE_COMPOSER,
          data: { ...store.getState().currentComposer, ...currentComposer },
        })
      },
    }

    firstName.simulate('change', {
      target: { value: 'Carlos', name: 'first_name' },
    })

    modal.find('#editBtnCancel').simulate('click')

    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: RESET_COMPOSER,
          data: {
            ...store.getState().currentComposer,
            ...store.getState().composer,
          },
        })
      },
    }

    app.find('#editLink').simulate('click')
    modal = app.find('.composer-modal')

    expect(modal.find('#firstName')).not.toBe('Carlos')
  })
})

xdescribe('editing a composer', function () {
  let store, app

  beforeEach(() => {
    store = createStore()
    app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    app.find('#editLink').simulate('click')
  })

  it('updates a composer on save', function () {
    let currentComposer = store.getState().currentComposer
    currentComposer.first_name = 'Carlos'

    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: SAVE_COMPOSER,
          data: { ...store.getState().composer, ...currentComposer },
        })
      },
    }

    app.find('.composer-modal').find('#btnSave').simulate('click')

    const composer = store.getState().composer
    expect(composer.first_name).toBe('Carlos')
  })

  it('shows error and does not save if there is a validation error', function () {
    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: UPDATE_COMPOSER,
          data: { ...store.getState().currentComposer },
        })
      },
    }

    const modal = app.find('.composer-modal')

    modal
      .find('#firstName')
      .simulate('change', { target: { value: '', name: 'first_name' } })
    modal.find('#btnSave').simulate('click')
    expect(modal.find('.composer-error').length).toBeGreaterThan(0)

    const composer = store.getState().composer
    expect(composer.first_name).not.toBe('')
  })

  it('shows the PRO drop down if the composer does not have a performing rights org', function () {
    app.find('.composer-modal').find('#editBtnCancel').simulate('click')

    let composer = store.getState().composer
    composer.pro_id = ''
    composer.pro_name = ''

    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: RESET_COMPOSER,
          data: { composer },
        })
      },
    }

    app.find('#editLink').simulate('click')

    const modal = app.find('.composer-modal')

    expect(modal.find('#proId').length).toBeGreaterThan(0)
    expect(modal.find('#proName').length).toBe(0)
  })

  it('displays the legacy ntc form link if the ntc_composition_form feature is not active', () => {
    const features = {
      ntcUrl: '',
      ntcCompositionFormEnabled: false,
    }

    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    let addCompositionsLink = app.find('.legacy-ntc-form-link').exists()
    expect(addCompositionsLink).toBeTruthy()
  })

  it('displays the add compositions link when the ntc_composition_form feature is active', () => {
    const features = {
      ntcUrl: '',
      ntcCompositionFormEnabled: true,
    }

    const store = createStoreWithNtcForm()
    const app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    let addCompositionsLink = app.find('.add-compositions-link').exists()
    expect(addCompositionsLink).toBeTruthy()
  })
})

xdescribe('tax message', function () {
  it('replaces the modal fields if the composer name changes and the composer has tax info submitted', function () {
    const store = createStoreWithTaxInfo()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    app.find('#editLink').simulate('click')

    let currentComposer = store.getState().currentComposer
    currentComposer.has_tax_info = true
    currentComposer.first_name = 'Carlos'

    window.jQuery = {
      ajax: function () {
        store.dispatch({
          type: SAVE_COMPOSER,
          data: { ...store.getState().composer, ...currentComposer },
        })
      },
    }

    app.find('.composer-modal').find('#btnSave').simulate('click')
    const modal = app.find('.composer-modal')

    expect(modal.find('#firstName').length).toBe(0)
    expect(modal.find('.composer-modal-tax-info').length).toBeGreaterThan(0)
  })
})

describe('shares missing', function () {
  it('is a link if there are any shares missing', function () {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    expect(
      app.find('.div-shares-missing').find('#addSharesLink').length
    ).toBeGreaterThan(0)
  })

  it('is not a link if there are 0 shares missing', function () {
    const store = createStoreWithSplitsSubmitted()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    expect(app.find('.div-shares-missing').find('#addSharesLink').length).toBe(
      0
    )
  })

  it('opens the split shares model and loads the first composition with splits missing', function () {
    const store = createStore()
    const compositions = store.getState().compositions
    const currentComp = store.getState().currentComposition
    const composition = compositions[currentComp.compositionId]
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    app.find('#addSharesLink').simulate('click')
    const modal = app.find('.publishing-splits-modal')

    expect(modal.find('.publishing-splits').length).toBeGreaterThan(0)
    expect(composition.composition_title).toBe('Song 1')
  })
})

describe('when the composer is not editable', () => {
  it('the "Edit Info" link does not appear', () => {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    let editLink = app.find('#editLink')

    expect(editLink).not.toHaveText('Edit Info')
  })
})

describe('bulk upload link', () => {
  it('displays the bulk upload compositions link when the bulk upload feature is active', () => {
    const features = {
      bulkUploadCompositions: true,
    }

    const store = createStoreWithNtcForm()
    const app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    let uploadCompositionsLink = app.find('.upload-compositions-link').exists()
    expect(uploadCompositionsLink).toBeTruthy()
  })

  it('does not display the upload compositions link when the bulk upload feature is not active', () => {
    const features = {
      bulkUploadCompositions: false,
    }

    const store = createStoreWithNtcForm()
    const app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    let uploadCompositionsLink = app.find('.upload-compositions-link').exists()
    expect(uploadCompositionsLink).toBeFalsy()
  })
})
