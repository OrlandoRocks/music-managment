import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStoreWithNtcForm from '../helpers/storeWithNtcForm'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'
import { ADD_NTC_COMPOSITION_SUCCESS } from '../../../compositions/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

describe('Add Composition Modal', () => {
  let store, app

  beforeEach(() => {
    let features = {
      ntcUrl: '',
      ntcCompositionFormEnabled: true,
      canEditCompositions: true,
    }

    store = createStoreWithNtcForm()
    app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    app.find('.add-compositions-link').simulate('click')
  })

  it('displays the composer name in the header of the modal', () => {
    let composer = store.getState().composer
    let header = app.find('.header > div:first-child')

    expect(header).toHaveText(`Add Composition for ${composer.name}`)
  })

  describe('close modal', () => {
    it('should close on when the user clicks the "X"', () => {
      let presentHeader = app.find('.header > div:first-child').exists()
      expect(presentHeader).toBeTruthy()

      app.find('.close-btn').simulate('click')

      let absentHeader = app.find('.header > div:first-child').exists()
      expect(absentHeader).toBeFalsy()
    })

    it('should close when the user clicks the cancel button', () => {
      let header = app.find('.header > div:first-child').exists()
      expect(header).toBeTruthy()

      app.find('.cancel-btn').simulate('click')

      let absentHeader = app.find('.header > div:first-child').exists()
      expect(absentHeader).toBeFalsy()
    })
  })

  describe('registering a non tunecore composition', () => {
    it('should add the composition to the compositions manager table', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let albumName = app.find('.albumName > input')
      albumName.simulate('change', {
        target: { name: 'albumName', value: 'Album Name' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: ADD_NTC_COMPOSITION_SUCCESS,
            data: {
              id: 100,
              appears_on: 'Album Name',
              composition_title: 'Composition Title',
              composer_share: 50.0,
              cowriter_share: 0.0,
              cowriters: [],
              cowriter_splits: [],
              errors: {},
            },
          })
        },
      }

      app.find('.register-btn').simulate('click')
      app.find('.cancel-btn').simulate('click')

      let nonTunecoreCompositionRow = app.find('.rt-tr-group').last()

      let appearsOn = nonTunecoreCompositionRow
        .find('.datatable-appears-on')
        .at(1)
      let compositionTitle = nonTunecoreCompositionRow
        .find('.datatable-composition-title')
        .at(1)
      let yourShare = nonTunecoreCompositionRow
        .find('.datatable-your-share')
        .at(1)
      let cowriterShare = nonTunecoreCompositionRow
        .find('.datatable-cowriter-share')
        .at(1)
      let status = nonTunecoreCompositionRow.find('.datatable-status').at(1)

      expect(appearsOn).toHaveText('Album Name')
      expect(compositionTitle).toHaveText('Composition Title')
      expect(yourShare).toHaveText('50%')
      expect(cowriterShare).toHaveText('-')
      expect(status).toHaveText('Submitted')
    })

    it('should not register the composition if there are blank cowriter fields', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let albumName = app.find('.albumName > input')
      albumName.simulate('change', {
        target: { name: 'albumName', value: 'Album Name' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      app.find('.list-cowriter-link').simulate('click')

      let firstNameInput = app.find('input[name="first_name"]')
      firstNameInput.simulate('change', {
        target: { name: 'first_name', value: 'First Name' },
      })

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      app.find('.register-btn').simulate('click')

      let lastNameInput = app
        .find('input[name="last_name"]')
        .closest('.cell')
        .hasClass('error-container')

      expect(lastNameInput).toBeTruthy()

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })
  })

  describe('Register button', () => {
    it('is disabled if the song title field is not populated', () => {
      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })

    it('is disabled if the performing artist field is not populated', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })

    it('is disabled if the percent field is not populated', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })

    it('is disabled if the form has errors', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title 𐌈' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })

    it('is disabled if the cowriter field has an error', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      app.find('.list-cowriter-link').simulate('click')

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '150' },
      })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })

    it('is enabled if the required fields are present and the form does not have errors', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeFalsy()
    })
  })

  describe('remove ntcCowriter', () => {
    it('removes the cowriter from the ntc composition', () => {
      const { ntcComposition } = store.getState()
      expect(ntcComposition.cowriters).toEqual([])

      app.find('.list-cowriter-link').simulate('click')

      const ntcCompositionWithCowriter = store.getState().ntcComposition
      expect(ntcCompositionWithCowriter.cowriters).not.toEqual([])

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      app.find('.remove-icon').simulate('click')

      const ntcCompositionWithRemovedCowriter = store.getState().ntcComposition
      expect(ntcCompositionWithRemovedCowriter.cowriters).toEqual([])
    })
  })

  describe('list additional co-writers', () => {
    it('adds another cowriter to the ntcComposition', () => {
      let songTitle = app.find('.title > input')
      songTitle.simulate('change', {
        target: { name: 'title', value: 'Composition Title' },
      })

      let performingArtist = app.find('.performingArtist > input')
      performingArtist.simulate('change', {
        target: { name: 'performingArtist', value: 'Performing Artist' },
      })

      let percent = app.find('.percent-input-wrapper > input')
      percent.simulate('change', { target: { name: 'percent', value: '50' } })

      app.find('.list-cowriter-link').simulate('click')

      const ntcCompositionCowriters = store.getState().ntcComposition.cowriters
      expect(ntcCompositionCowriters.length).toEqual(1)

      app.find('.list-additional-cowriters').simulate('click')

      const updatedNtcCompositionCowriters = store.getState().ntcComposition
        .cowriters
      expect(updatedNtcCompositionCowriters.length).toEqual(2)
    })
  })
})
