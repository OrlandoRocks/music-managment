import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'

Enzyme.configure({ adapter: new Adapter() })

describe('Edit Composition Modal', () => {
  describe('when the edit composition feature is not enabled', () => {
    it('does not display the "Edit Composition" link', () => {
      let store = createStore()
      let app = mount(
        <Provider store={store}>
          <CompositionsApp />
        </Provider>
      )

      let composition = store.getState().compositions[1]
      let editCompositionBtn = app
        .find(`#edit_composition_button_${composition.id}`)
        .exists()

      expect(editCompositionBtn).toBeFalsy()
    })
  })

  describe('when the edit composition feature is enabled', () => {
    it('displays the "Edit Composition" link', () => {
      let store = createStore()
      let features = { canEditCompositions: true }
      let app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )

      let composition = store.getState().compositions[2]
      let editCompositionBtn = app
        .find(`#edit_composition_button_${composition.id}`)
        .exists()

      expect(editCompositionBtn).toBeTruthy()
    })
  })

  describe('when the composition is associated to a Tunecore Song', () => {
    let app, store, composition

    beforeEach(() => {
      store = createStore()
      let features = { canEditCompositions: true }
      app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )
      composition = store.getState().compositions[2]

      let editCompositionBtn = app.find(
        `#edit_composition_button_${composition.id}`
      )
      editCompositionBtn.simulate('click', {
        target: {
          name: 'edit-composition',
          dataset: { id: composition.id, index: 2 },
        },
      })
    })

    it('displays "Edit Composition" in the modal header', () => {
      let headerText = app.find('.header').text().trim()

      expect(headerText).toEqual('Edit Composition')
    })

    it('closes the modal if you click the "X" in the header', () => {
      let closeBtn = app.find('.close-btn')
      closeBtn.simulate('click')

      let editCompositionModal = app
        .find('.edit-composition-modal .close-btn')
        .exists()
      expect(editCompositionModal).toBeFalsy()
    })

    it('resets composition share to old data when modal is closed', () => {
      let composerShareInput = app.find('.composer_share input')
      composerShareInput.simulate('change', {
        target: { name: 'composer_share', value: '25' },
      })

      let afterUpdate = store.getState().compositions[2]
      expect(afterUpdate.composer_share).toEqual('25')

      let closeBtn = app.find('.close-btn')
      closeBtn.simulate('click')

      let updatedComposition = store.getState().compositions[2]
      expect(updatedComposition.composer_share).toEqual('75.0')
    })

    it('resets cowriter details to old data when modal is closed', () => {
      let store = createStore()
      let features = { canEditCompositions: true }
      let app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )

      let composition = store.getState().compositions[7]
      let cowriter = store.getState().cowriters[composition.cowriters[0]]
      let oldCowriterName = cowriter.first_name

      let editCompositionBtn = app.find(
        `#edit_composition_button_${composition.id}`
      )
      editCompositionBtn.simulate('click', {
        target: {
          name: 'edit-composition',
          dataset: { id: composition.id, index: 7 },
        },
      })

      let cowriterFirstNameInput = app
        .find('.publishing-splits-cowriters-first-name')
        .at(0)
      expect(cowriterFirstNameInput.props().value).toEqual(oldCowriterName)
      cowriterFirstNameInput.simulate('change', {
        target: { name: 'first_name', value: 'Venus' },
      })

      app.find('.close-btn').simulate('click')

      let updatedCowriter = store.getState().cowriters[composition.cowriters[0]]

      expect(updatedCowriter.first_name).toEqual(oldCowriterName)
    })

    it('allows the user to update the composer share percentage', () => {
      let composerShareInput = app.find('.composer_share input')
      expect(composerShareInput.props().value).toBe('75.0')

      composerShareInput.simulate('change', {
        target: { name: 'composer_share', value: '25' },
      })

      let updatedComposition = store.getState().compositions[2]
      expect(updatedComposition.composer_share).toEqual('25')
    })

    it('allows the user to add a new cowriter', () => {
      let addCowriterBtn = app.find('.list-of-cowriters-link')
      addCowriterBtn.simulate('click')

      let cowriterFirstNameInput = app
        .find('.publishing-splits-cowriters-first-name')
        .at(1)
      cowriterFirstNameInput.simulate('change', {
        target: { name: 'first_name', value: 'First Name' },
      })

      let cowriterLastNameInput = app
        .find('.publishing-splits-cowriters-last-name')
        .at(1)
      cowriterLastNameInput.simulate('change', {
        target: { name: 'last_name', value: 'Last Name' },
      })

      let cowriterShareInput = app.find(
        '.publishing-splits-cowriters-share-input'
      )
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '10' },
      })

      let cowriterId = store.getState().compositions[2].cowriters[0]
      let cowriter = store.getState().cowriters[cowriterId]

      expect(cowriter.first_name).toEqual('First Name')
      expect(cowriter.last_name).toEqual('Last Name')
      expect(cowriter.cowriter_share).toEqual('10')
    })

    it('allows the user to update an existing cowriter', () => {
      let store = createStore()
      let features = { canEditCompositions: true }
      let app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )
      let composition = store.getState().compositions[7]
      let cowriter = store.getState().cowriters[composition.cowriters[0]]

      let editCompositionBtn = app.find(
        `#edit_composition_button_${composition.id}`
      )
      editCompositionBtn.simulate('click', {
        target: {
          name: 'edit-composition',
          dataset: { id: composition.id, index: 7 },
        },
      })

      let cowriterFirstNameInput = app
        .find('.publishing-splits-cowriters-first-name')
        .at(1)
      expect(cowriterFirstNameInput.props().value).toEqual(cowriter.first_name)
      cowriterFirstNameInput.simulate('change', {
        target: { name: 'first_name', value: 'Venus' },
      })

      let cowriterLastNameInput = app
        .find('.publishing-splits-cowriters-last-name')
        .at(1)
      expect(cowriterLastNameInput.props().value).toEqual(cowriter.last_name)
      cowriterLastNameInput.simulate('change', {
        target: { name: 'last_name', value: '' },
      })

      let cowriterShareInput = app.find(
        '.publishing-splits-cowriters-share-input'
      )
      expect(cowriterShareInput.props().value).toEqual(cowriter.cowriter_share)
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '10' },
      })

      let updatedCowriter = store.getState().cowriters[composition.cowriters[0]]
      expect(updatedCowriter.first_name).toEqual('Venus')
      expect(updatedCowriter.last_name).toEqual('')
      expect(updatedCowriter.cowriter_share).toEqual('10')
    })

    it('allows the user to add more than one cowriter', () => {
      let store = createStore()
      let features = { canEditCompositions: true }
      let app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )
      let composition = store.getState().compositions[7]

      expect(composition.cowriters.length).toEqual(1)

      let editCompositionBtn = app.find(
        `#edit_composition_button_${composition.id}`
      )
      editCompositionBtn.simulate('click', {
        target: {
          name: 'edit-composition',
          dataset: { id: composition.id, index: 7 },
        },
      })

      let addCowriterBtn = app.find(
        '.publishing-splits-cowriters-add-cowriter-btn'
      )
      addCowriterBtn.simulate('click')

      let updatedComposition = store.getState().compositions[7]
      expect(updatedComposition.cowriters.length).toEqual(2)
    })
  })

  describe('when the composition is associated to a NonTunecore Song', () => {
    let app, store, composition

    beforeEach(() => {
      store = createStore()
      let features = { canEditCompositions: true }
      app = mount(
        <Provider store={store}>
          <FeatureContext.Provider value={features}>
            <CompositionsApp />
          </FeatureContext.Provider>
        </Provider>
      )
      composition = store.getState().compositions[8]

      let editCompositionBtn = app.find(
        `#edit_composition_button_${composition.id}`
      )
      editCompositionBtn.simulate('click', {
        target: {
          name: 'edit-composition',
          dataset: { id: composition.id, index: 8 },
        },
      })
    })

    it('allows the user to update a compositions title', () => {
      let songTitleInput = app.find('.composition_title input')
      expect(songTitleInput.props().value).toEqual(
        composition.composition_title
      )
      songTitleInput.simulate('change', {
        target: { name: 'composition_title', value: 'New Title' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.composition_title).toEqual('New Title')
    })

    it('allows the user to update the performing artist', () => {
      let performingArtistInput = app.find('.performing_artist input')
      expect(performingArtistInput.props().value).toEqual(
        composition.performing_artist
      )
      performingArtistInput.simulate('change', {
        target: { name: 'performing_artist', value: 'New Artist' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.performing_artist).toEqual('New Artist')
    })

    it('allows the user to update the album name', () => {
      let albumNameInput = app.find('.appears_on input')
      expect(albumNameInput.props().value).toEqual(composition.appears_on)
      albumNameInput.simulate('change', {
        target: { name: 'appears_on', value: 'New Album Name' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.appears_on).toEqual('New Album Name')
    })

    it('allows the user to update the isrc number', () => {
      let isrcInput = app.find('.isrc input')
      expect(isrcInput.props().value).toEqual(composition.isrc)
      isrcInput.simulate('change', {
        target: { name: 'isrc', value: 'IHTKAS061519' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.isrc).toEqual('IHTKAS061519')
    })

    it('allows the user to update the release date field', () => {
      let releaseDateInput = app.find('.release_date input')
      expect(releaseDateInput.props().value).toEqual(composition.release_date)
      releaseDateInput.simulate('change', {
        target: { name: 'release_date', value: '01/01/2020' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.release_date).toEqual('01/01/2020')
    })

    it('allows the user to update the composer share', () => {
      let composerShareInput = app.find('.composer_share input')
      expect(composerShareInput.props().value).toEqual(
        composition.composer_share
      )
      composerShareInput.simulate('change', {
        target: { name: 'composer_share', value: '50' },
      })

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.composer_share).toEqual('50')
    })

    it('resets composition to old data when modal is closed', () => {
      let isrcInput = app.find('.isrc input')
      let albumNameInput = app.find('.appears_on input')

      let originalISRC = isrcInput.props().value
      let originalName = albumNameInput.props().value

      isrcInput.simulate('change', {
        target: { name: 'isrc', value: 'IHTKAS061519' },
      })
      albumNameInput.simulate('change', {
        target: { name: 'appears_on', value: 'New Album Name' },
      })

      let afterUpdateComposition = store.getState().compositions[8]
      expect(afterUpdateComposition.isrc).toEqual('IHTKAS061519')
      expect(afterUpdateComposition.appears_on).toEqual('New Album Name')

      let closeBtn = app.find('.close-btn')
      closeBtn.simulate('click')

      let updatedComposition = store.getState().compositions[8]
      expect(updatedComposition.isrc).toEqual(originalISRC)
      expect(updatedComposition.appears_on).toEqual(originalName)
    })
  })
})
