import React from 'react'
import { Provider } from 'react-redux'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStoreWithNtcForm from '../helpers/storeWithNtcForm'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import FeatureContext from '../../../compositions/context.js'

Enzyme.configure({ adapter: new Adapter() })

describe('Add Composition Modal validations', () => {
  let store, app

  beforeEach(() => {
    let features = {
      ntcUrl: '',
      ntcCompositionFormEnabled: true,
      canEditCompositions: true,
    }

    store = createStoreWithNtcForm()
    app = mount(
      <Provider store={store}>
        <FeatureContext.Provider value={features}>
          <CompositionsApp />
        </FeatureContext.Provider>
      </Provider>
    )

    app.find('.add-compositions-link').simulate('click')
  })

  describe('song title', () => {
    it('is invalid if the title contains special characters', () => {
      let titleInput = app.find('.title > input')

      titleInput.simulate('change', { target: { name: 'title', value: '`$' } })

      let titleInputWithError = app
        .find('.title > input')
        .hasClass('error-field')
      expect(titleInputWithError).toBeTruthy()
    })

    it('is invalid if the title contains latin characters', () => {
      let titleInput = app.find('.title > input')

      titleInput.simulate('change', {
        target: { name: 'title', value: '𐌈 Gunna Give It To Ya' },
      })

      let titleInputWithError = app
        .find('.title > input')
        .hasClass('error-field')
      expect(titleInputWithError).toBeTruthy()
    })
  })

  describe('performing artist', () => {
    it('is invalid if the performing artist contains special characters', () => {
      let performingArtistInput = app.find('.performingArtist > input')

      performingArtistInput.simulate('change', {
        target: { name: 'performingArtist', value: '`$' },
      })

      let performingArtistInputWithError = app
        .find('.performingArtist > input')
        .hasClass('error-field')
      expect(performingArtistInputWithError).toBeTruthy()
    })

    it('is invalid if the performing artist contains latin characters', () => {
      let performingArtistInput = app.find('.performingArtist > input').first()

      performingArtistInput.simulate('change', {
        target: { name: 'performingArtist', value: '𐌈 Gunna Give It To Ya' },
      })

      let performingArtistInputWithError = app
        .find('.performingArtist > input')
        .hasClass('error-field')
      expect(performingArtistInputWithError).toBeTruthy()
    })
  })

  describe('isrc number', () => {
    it('is invalid if the isrc number is incorrectly formatted', () => {
      let isrcNumberInput = app.find('.isrcNumber > input')

      isrcNumberInput.simulate('change', {
        target: { name: 'isrcNumber', value: '12345' },
      })

      let isrcNumberInputWithError = app
        .find('.isrcNumber > input')
        .hasClass('error-field')
      expect(isrcNumberInputWithError).toBeTruthy()
    })

    it('is valid if the isrc number is correctly formatted', () => {
      let isrcNumberInput = app.find('.isrcNumber > input')

      isrcNumberInput.simulate('change', {
        target: { name: 'isrcNumber', value: 'USTCZ0999998' },
      })

      let isrcNumberInputWithError = app
        .find('.isrcNumber > input')
        .hasClass('error-field')
      expect(isrcNumberInputWithError).toBeFalsy()
    })
  })

  describe('release date', () => {
    it('is invalid if the release date is before 1900-01-01', () => {
      let releaseDateInput = app.find('.releaseDate > input')

      releaseDateInput.simulate('change', {
        target: { name: 'releaseDate', value: '01/01/1899' },
      })

      let releaseDateInputWithError = app
        .find('.releaseDate > input')
        .hasClass('error-field')
      expect(releaseDateInputWithError).toBeTruthy()
    })

    it('is valid if the release date is after 1900-01-01', () => {
      let releaseDateInput = app.find('.releaseDate > input')

      releaseDateInput.simulate('change', {
        target: { name: 'releaseDate', value: '01/02/1990' },
      })

      let releaseDateInputWithError = app
        .find('.releaseDate > input')
        .hasClass('error-field')
      expect(releaseDateInputWithError).toBeFalsy()
    })

    it('is valid if the release date is in the future', () => {
      let releaseDateInput = app.find('.releaseDate > input')

      let futureDate = '12/12/5000'

      releaseDateInput.simulate('change', {
        target: { name: 'releaseDate', value: futureDate },
      })

      let releaseDateInputWithError = app
        .find('.releaseDate > input')
        .hasClass('error-field')
      expect(releaseDateInputWithError).toBeFalsy()
    })
  })

  describe('percent', () => {
    it('is invalid if the percent is less than 1', () => {
      let percentInput = app.find('.percent-input-wrapper > input')

      percentInput.simulate('change', {
        target: { name: 'percent', value: '.5' },
      })

      let percentInputWithError = app
        .find('.percent-input-wrapper > input')
        .hasClass('error-field')
      expect(percentInputWithError).toBeTruthy()
    })

    it('is invalid if the percent is greater than 100', () => {
      let percentInput = app.find('.percent-input-wrapper > input')

      percentInput.simulate('change', {
        target: { name: 'percent', value: '100.1' },
      })

      let percentInputWithError = app
        .find('.percent-input-wrapper > input')
        .hasClass('error-field')
      expect(percentInputWithError).toBeTruthy()
    })

    it('is invalid if the accumulated share percentage exceeds 100', () => {
      app.find('.list-cowriter-link').simulate('click')

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '75' },
      })

      let percentInput = app.find('.percent-input-wrapper > input')
      percentInput.simulate('change', {
        target: { name: 'percent', value: '50' },
      })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()

      let percentInputWithError = app
        .find('.percent-input-wrapper > input')
        .hasClass('error-field')
      expect(percentInputWithError).toBeTruthy()
    })

    it('is valid if the percent is between 1 and 100', () => {
      let percentInput = app.find('.percent-input-wrapper > input')

      percentInput.simulate('change', {
        target: { name: 'percent', value: '100' },
      })

      let percentInputWithError = app
        .find('.percent-input-wrapper > input')
        .hasClass('error-field')
      expect(percentInputWithError).toBeFalsy()
    })
  })

  describe('cowriter share', () => {
    it('is invalid if the cowiter share if the cowriter share is above 100', () => {
      app.find('.list-cowriter-link').simulate('click')

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '150' },
      })

      let cowriterShareInputWithError = app
        .find('input[name="cowriter_share"]')
        .hasClass('error-field')
      expect(cowriterShareInputWithError).toBeTruthy()
    })

    it('is invalid if the accumulated share percentage exceeds 100', () => {
      let percentInput = app.find('.percent-input-wrapper > input')
      percentInput.simulate('change', {
        target: { name: 'percent', value: '75' },
      })

      app.find('.list-cowriter-link').simulate('click')

      let cowriterShareInput = app.find('input[name="cowriter_share"]')
      cowriterShareInput.simulate('change', {
        target: { name: 'cowriter_share', value: '50' },
      })

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()

      let cowriterShareInputWithError = app
        .find('input[name="cowriter_share"]')
        .hasClass('error-field')
      expect(cowriterShareInputWithError).toBeTruthy()
    })
  })

  describe('when a user creates a ntc song with a duplicate song title', () => {
    it('disables the register btn and displays an error message', () => {
      let titleInput = app.find('.title > input')

      titleInput.simulate('change', {
        target: { name: 'title', value: 'song 1   ' },
      })

      let titleInputWithError = app
        .find('.title > input')
        .hasClass('error-field')
      expect(titleInputWithError).toBeTruthy()

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })
  })

  describe('when a user creates a ntc song with a duplicate translated name title', () => {
    it('disables the register btn and displays an error message', () => {
      let titleInput = app.find('.title > input')

      titleInput.simulate('change', {
        target: { name: 'title', value: 'this song is dope' },
      })

      let titleInputWithError = app
        .find('.title > input')
        .hasClass('error-field')
      expect(titleInputWithError).toBeTruthy()

      let registerBtn = app.find('.register-btn')
      expect(registerBtn.prop('disabled')).toBeTruthy()
    })
  })
})
