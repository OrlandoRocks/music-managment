import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import CompositionsApp from '../../../compositions/components/CompositionsApp'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('Compositions Manager Table', function () {
  it('should display the composition title', function () {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    let songName = app.find('.datatable-composition-title').at(1)
    expect(songName).toHaveText('Song 1')
  })

  it('should display the album name in the appears on column', function () {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    let albumName = app.find('.datatable-appears-on').at(1)
    expect(albumName).toHaveText('First Album')
  })

  it('should display song isrc under isrc column', function () {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    let isrc = app.find('.datatable-isrc').at(1)
    expect(isrc).toHaveText('SKTIML12390VK')
  })

  describe('submitted at column', function () {
    context('when the shares are not submitted for compositions', function () {
      it("should display 'Not Submitted'", function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let albumSubmittedAt = app.find('.datatable-submitted-at').at(1)
        expect(albumSubmittedAt).toHaveText('Not Submitted')
      })
    })

    context('when the shares are submitted for compositions', function () {
      it('should display the submitted at date', function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let albumSubmittedAt = app.find('.datatable-submitted-at').at(2)
        expect(albumSubmittedAt).toHaveText('2019-01-06')
      })
    })
  })

  describe('your share column', function () {
    context('when the composition does not have shares', function () {
      it("should display the 'Add Shares' link", function () {
        const store = createStore()
        const { compositions } = store.getState()
        const composition = compositions[1]

        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let yourShare = app.find(`#add_shares_button_${composition.id}`)
        expect(yourShare).toHaveText(' Add Shares')
      })
    })

    context('when the composition has shares', function () {
      it('should displays the composers share percentage', function () {
        const store = createStore()
        const { compositions } = store.getState()
        const composition = compositions[2]

        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let yourShare = app.find(`#your_share_percent_${composition.id}`)
        expect(yourShare).toHaveText('75%')
      })
    })
  })

  it('should display the cowriter percentage', function () {
    const store = createStore()

    const app = mount(
      <Provider store={store}>
        <CompositionsApp />
      </Provider>
    )

    let cowritersShare = app
      .find('.cell-tooltip')
      .find('.tooltip-container')
      .find('span')
      .first()
    expect(cowritersShare).toHaveText('25% ')
  })

  describe('status column', function () {
    context("when the composition's status is 'shares_missing'", function () {
      it("should display 'Shares Missing'", function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let status = app
          .find('.datatable-status')
          .at(1)
          .find('.composition-status')
        expect(status).toHaveText('Shares Missing')
      })
    })

    context("when the composition's status is 'accepted'", function () {
      it("should display 'Accepted'", function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let status = app
          .find('.datatable-status')
          .at(2)
          .find('.composition-status')
        expect(status).toHaveText('Accepted')
      })
    })

    context("when the composition's status is 'resubmitted'", function () {
      it("should display 'Resubmitted'", function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let status = app.find('.datatable-status .composition-status').at(6)
        expect(status).toHaveText('Resubmitted')
      })
    })

    context('when the compositions status is "terminated"', function () {
      it('should display "Terminated" within the status column', function () {
        const store = createStore()
        const app = mount(
          <Provider store={store}>
            <CompositionsApp />
          </Provider>
        )

        let status = app.find('.composition-status').at(3)
        expect(status).toHaveText('Terminated')
      })
    })
  })

  describe('composition search', function () {
    it('should display the search input when clicking on the search icon', function () {
      const store = createStore()
      const app = mount(
        <Provider store={store}>
          <CompositionsApp />
        </Provider>
      )

      let hiddenSearchInput = app.find('.datatable-search-input')
      expect(hiddenSearchInput.length).toEqual(0)

      let searchIcon = app.find('.datatable-search-icon')
      searchIcon.simulate('click')

      let searchInput = app.find('.datatable-search-input')
      expect(searchInput.length).toEqual(1)
    })

    it('should display all the compositions after clearing the search field', function () {
      const store = createStore()
      const compositions = store.getState().compositions
      const compositionsLength = Object.keys(compositions).length
      const app = mount(
        <Provider store={store}>
          <CompositionsApp />
        </Provider>
      )

      let searchIcon = app.find('.datatable-search-icon')
      searchIcon.simulate('click')

      let searchInput = app.find('.datatable-search-input')
      searchInput.simulate('change', { target: { value: 'First' } })

      let numOfFilteredCompositions = app.find('.rt-tr-group').length
      expect(numOfFilteredCompositions).not.toBe(compositionsLength)

      let cancelIcon = app.find('.datatable-cancel-search')
      cancelIcon.simulate('click')

      let numOfCompositions = app.find('.rt-tr-group').length
      expect(numOfCompositions).toBe(compositionsLength)
    })
  })
})
