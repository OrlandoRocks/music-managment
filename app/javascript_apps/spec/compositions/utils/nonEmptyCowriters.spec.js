import createStore from '../helpers/store'
import nonEmptyCowriters from '../../../compositions/utils/nonEmptyCowriters'

describe('nonEmptyCowriters', () => {
  it('returns cowriters that have at least one required field present', () => {
    const store = createStore()
    const { cowriters } = store.getState()

    let cowritersLength = Object.keys(cowriters).length
    let presentCowriters = nonEmptyCowriters(Object.values(cowriters))
    expect(presentCowriters.length).not.toEqual(cowritersLength)
    expect(presentCowriters[0].last_name).toEqual('Williams')
  })
})
