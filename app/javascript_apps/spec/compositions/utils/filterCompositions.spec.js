import createStore from '../helpers/store'
import filterCompositions from '../../../compositions/utils/filterCompositions'

describe('filterCompositions', () => {
  context('when the search term is less than 3 characters', function () {
    it('returns all compositions', () => {
      const store = createStore()
      const { compositions } = store.getState()
      let searchValue = 'Fir'

      expect(filterCompositions(compositions, searchValue)).toEqual(
        compositions
      )
    })
  })

  context('when the search term is greater than 3 characters', function () {
    it('returns compositions that match the search term', () => {
      const store = createStore()
      const { compositions } = store.getState()
      let searchValue = 'First'
      let filteredCompositions = { 1: compositions[1] }

      expect(filterCompositions(compositions, searchValue)).toEqual(
        filteredCompositions
      )
    })
  })
})
