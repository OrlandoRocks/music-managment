import rotateArray from '../../../shared/rotateArray'

describe('rotateArray', () => {
  it('rotates array around a specified index', () => {
    let array = [0, 1, 2, 3, 4, 5, 6]

    expect(rotateArray(array, 2)).toEqual([2, 3, 4, 5, 6, 0, 1])
  })
})
