import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { render } from '@testing-library/react'

import ProgressBarContainer from '../../../distribution_progress_bar/components/ProgressBarContainer'

describe('ProgressBarContainer', function () {
  it('renders the progress bar text', () => {
    let baseElement = document.createElement('div')
    baseElement.setAttribute('id', 'distribution_progress_bar_app')
    baseElement.setAttribute('data-progress-level', '3')
    baseElement.setAttribute(
      'data-translations',
      JSON.stringify({
        progress_bar: { of_steps: 'of 4 Steps', complete: 'Complete!' },
      })
    )

    const { queryByText } = render(
      <div id={'distribution_progress_bar_app'}>
        <ProgressBarContainer progressBarNode={baseElement} />
      </div>
    )

    expect(queryByText(/3 of 4 Steps/)).toBeInTheDocument()
  })
})
