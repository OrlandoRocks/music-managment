require('../node_modules/jest-enzyme/lib/index.js')
import { setAutoFreeze } from 'immer'

// https://github.com/reduxjs/redux-toolkit/issues/424
// Might be because Jest tries to mutate frozen objects
setAutoFreeze(false)

jest.mock(
  '../album_app/components/data/ExternalServiceIDDataFetcher',
  () => () => 'ExternalServiceIDDataFetcher'
)

import countries from './album_app/helpers/countries'
import genres from './album_app/helpers/genres'
import translations from './helpers/translations'
import album from './album_app/helpers/album.js'
import languageCodes from './album_app/helpers/languageCodes'

export const albumAppConfig = {
  activeArtists: [],
  countries,
  features: [],
  stores: [],
  freemiumStores: [],
  distributionProgressLevel: 1,
  songsComplete: false,
  genres,
  languageCodes: languageCodes,
  locale: 'en',
  pageTitle: undefined,
  person: {
    person_plan: null,
    plan_features: {},
  },
  plans: [],
  possibleArtists: [],
  releaseTypeVars: {
    variousFieldEnabled: true,
  },
  translations: translations.album_app,
}
;(function injectTestConfig() {
  const root = document.createElement('div')
  root.setAttribute('id', 'album_app')
  root.setAttribute('data-album', JSON.stringify(album))
  Object.keys(albumAppConfig).forEach((k) => {
    root.setAttribute(`data-${k}`, JSON.stringify(albumAppConfig[k]))
  })

  document.body.appendChild(root)
})()
;(function addCSRFTokenToDOM() {
  const tokenNode = document.createElement('div')
  tokenNode.setAttribute('name', 'csrf-token')
  document.body.appendChild(tokenNode)
})()

// For songs_app tests
window.translations = translations

// JSDOM doesn't implement scrollIntoView
window.HTMLElement.prototype.scrollIntoView = function () {}
window.scrollTo = function () {}
