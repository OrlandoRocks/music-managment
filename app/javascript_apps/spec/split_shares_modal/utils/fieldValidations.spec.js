import {
  containsInvalidCharacters,
  containsNonLatinCharacters,
} from '../../../shared/fieldValidations'

describe('containsInvalidCharacters', () => {
  it('returns false if the string does not contain invalid characters', () => {
    let str = 'this is valid'
    expect(containsInvalidCharacters(str)).toBeFalsy()
  })

  it('returns true if the string contains invalid characters', () => {
    let str = 'this-is-invalid'
    expect(containsInvalidCharacters(str)).toBeTruthy()
  })
})

describe('containsNonLatinCharacters', () => {
  it('returns false if the str is undefined', () => {
    let str = undefined
    expect(containsNonLatinCharacters(str)).toBeFalsy()
  })

  it('returns false if the str contains latin characters', () => {
    let str = 'dope'
    expect(containsNonLatinCharacters(str)).toBeFalsy()
  })

  it('returns true if the str contains non latin characters', () => {
    let str = 'dѲpe'
    expect(containsNonLatinCharacters(str)).toBeTruthy()
  })
})
