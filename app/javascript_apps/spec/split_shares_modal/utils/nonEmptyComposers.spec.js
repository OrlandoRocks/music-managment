import createStore from '../helpers/store'
import nonEmptyComposers from '../../../split_shares_modal/utils/nonEmptyComposers'

describe('nonEmptyComposers', () => {
  it('returns composers that have at least one required field present', () => {
    const store = createStore()
    const { composers } = store.getState()

    let composersLength = Object.keys(composers).length
    let presentComposers = nonEmptyComposers(Object.values(composers))
    expect(presentComposers.length).not.toEqual(composersLength)
    expect(presentComposers[0].id).toEqual(1)
  })
})
