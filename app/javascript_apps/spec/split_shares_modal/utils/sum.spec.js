import sum from '../../../shared/sum'

describe('sum', () => {
  it('sums the values of an object', () => {
    let objects = [{ value: '25.0' }, { value: '50.0' }]
    expect(sum(objects, 'value')).toEqual(75)
  })
})
