import createStore from '../helpers/store'
import findInvalidCowriters from '../../../split_shares_modal/utils/findInvalidCowriters'

describe('findInvalidCowriters', () => {
  it('returns cowriters that are missing required fields', () => {
    const store = createStore()
    const { cowriters } = store.getState()

    let invalidCowriters = findInvalidCowriters(Object.values(cowriters))
    expect(invalidCowriters[0].first_name).toEqual('Erik')
  })

  it('should not return cowriters where all the required fields are blank', () => {
    const store = createStore()
    const { cowriters } = store.getState()

    let emptyCowriter = cowriters[2]
    let invalidCowriters = findInvalidCowriters(Object.values(cowriters))
    expect(invalidCowriters).not.toContain(emptyCowriter)
  })
})
