import { normalizeComposers } from '../../../../split_shares_modal/store/schema'

let composersList = [
  {
    id: 1,
    account_composer: true,
    cae: '123456789',
    dob: new Date() + '',
    first_name: 'Charles',
    full_name: 'Charles Massry',
    middle_name: '',
    last_name: 'Massry',
    name: 'Charles Massry',
    has_tax_info: false,
    prefix: '',
    pro_id: 36,
    pro_name: 'ASCAP',
    suffix: '',
    uuid: 1,
  },
  {
    id: 2,
    account_composer: true,
    cae: '923456789',
    dob: new Date() + '',
    first_name: 'Daniel',
    full_name: 'Daniel Massry',
    middle_name: '',
    last_name: 'Massry',
    name: 'Daniel Massry',
    has_tax_info: false,
    prefix: '',
    pro_id: 36,
    pro_name: 'ASCAP',
    suffix: '',
    uuid: 2,
  },
]

const possibleComposers = normalizeComposers(composersList).entities.composers
export default possibleComposers
