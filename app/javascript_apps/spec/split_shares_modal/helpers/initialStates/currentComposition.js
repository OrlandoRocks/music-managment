import compositions from './compositions'
import findNextEligibleComposition from '../../../../split_shares_modal/utils/findNextEligibleComposition'

const composition = compositions[compositions.length - 1]
const currentComposition = findNextEligibleComposition(
  compositions,
  composition
)

export default currentComposition
