import { normalizeComposers } from '../../../../split_shares_modal/store/schema'

let composersList = [
  {
    id: 1,
    composer_share: 50,
    uuid: 1,
    errors: {},
  },
  {
    id: 2,
    composer_share: '',
    uuid: 2,
    errors: {},
  },
]

const {
  entities: { composers },
} = normalizeComposers(composersList)
export default composers
