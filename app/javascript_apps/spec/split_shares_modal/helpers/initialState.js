import composer from './initialStates/composer'
import composers from './initialStates/composers'
import possibleComposers from './initialStates/possibleComposers'
import compositions from './initialStates/compositions'
import currentComposer from './initialStates/currentComposer'
import currentComposition from './initialStates/currentComposition'
import pros from './initialStates/pros'
import cowriters from './initialStates/cowriters'
import ntcComposition from './initialStates/ntcComposition'

export default {
  composer,
  composers,
  possibleComposers,
  compositions,
  currentComposer,
  currentComposition,
  pros,
  cowriters,
  ntcComposition,
}
