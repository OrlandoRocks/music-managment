import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../split_shares_modal/reducers'
import initialState from './initialState'
import apiMiddleware from '../../../split_shares_modal/middlewares/apiMiddleware'
import compositionValidationMiddleware from '../../../split_shares_modal/middlewares/compositionValidationMiddleware'
import updateCowriterValidationMiddleware from '../../../split_shares_modal/middlewares/updateCowriterValidationMiddleware'
import translatedNameValidationMiddleware from '../../../split_shares_modal/middlewares/translatedNameValidationMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      updateCowriterValidationMiddleware,
      translatedNameValidationMiddleware,
      compositionValidationMiddleware,
      apiMiddleware
    )
  )
})
