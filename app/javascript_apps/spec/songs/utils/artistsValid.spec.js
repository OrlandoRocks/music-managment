import artistsValid from '../../../songs/utils/artistsValid'

describe('artistsValid', () => {
  it('returns true if given artists are complete', () => {
    const artists = [
      { artist_name: 'Sophie', credit: 'primary_artist', role_ids: [1] },
    ]
    const valid = artistsValid(artists)
    expect(valid).toEqual(true)
  })

  it('returns false if not all of the given artists are complete', () => {
    const artists = [
      { artist_name: 'Sophie', credit: 'primary_artist', role_ids: [1] },
      { artist_name: 'Frank', credit: '', role_ids: [1] },
    ]
    const valid = artistsValid(artists)
    expect(valid).toEqual(false)
  })
})
