import songErrorBuilder from '../../../songs/utils/songErrorBuilder'
import createStore from '../helpers/store'

describe('songErrorBuilder', () => {
  context('cleaning errors', () => {
    it('updates errors for a song previously_released_at attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 1,
          is_previously_released: true,
        },
        errors: {
          previously_released_at: true,
        },
      }

      expect(
        songErrorBuilder(song, { previously_released_at: '2018-02-12' }, state)
      ).toEqual({ previously_released_at: false })
    })

    it('updates errors for a song translated_name attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
          name: 'שיר ראשון',
        },
        errors: {
          translated_name: true,
        },
      }

      expect(
        songErrorBuilder(song, { translated_name: 'First song' }, state)
      ).toEqual({ translated_name: false })
    })

    it('updates errors for a song name attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
          name: 'First song',
        },
        errors: {
          name: true,
        },
      }

      expect(songErrorBuilder(song, { name: 'שיר ראשון' }, state)).toEqual({
        name: false,
      })
    })

    it('updates errors for a song language code id attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
        },
        errors: {
          name: true,
        },
      }

      expect(songErrorBuilder(song, { language_code_id: 1 }, state)).toEqual({
        name: false,
        translated_name: false,
      })
    })
  })

  context('retaining errors', () => {
    it('retains errors for a song previously_released_at attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 1,
          is_previously_released: true,
        },
        errors: {
          previously_released_at: true,
        },
      }

      expect(
        songErrorBuilder(song, { previously_released_at: null }, state)
      ).toEqual({ previously_released_at: true })
    })

    it('retains errors for a song translated_name attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
          name: 'שיר ראשון',
        },
        errors: {
          translated_name: true,
        },
      }

      expect(songErrorBuilder(song, { translated_name: '' }, state)).toEqual({
        translated_name: true,
      })
    })

    it('retains errors for a song name attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
          name: 'First song',
        },
        errors: {
          name: true,
        },
      }

      expect(
        songErrorBuilder(song, { name: 'Still first song' }, state)
      ).toEqual({ name: true })
    })

    it('retains errors for a song language code id attribute', () => {
      let state = createStore().getState()
      let song = {
        data: {
          language_code_id: 22,
          name: 'First Song',
          translated_name: '',
        },
        errors: {
          name: true,
        },
      }

      expect(songErrorBuilder(song, { language_code_id: 15 }, state)).toEqual({
        name: true,
        translated_name: true,
      })
    })
  })
})
