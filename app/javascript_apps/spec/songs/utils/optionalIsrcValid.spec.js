import optionalIsrcValid from '../../../songs/utils/optionalIsrcValid'

describe('optionalIsrcValid', () => {
  it('returns true if the isrc is valid with 12 characters', () => {
    const isrc = '123456789111'
    const valid = optionalIsrcValid({ data: { optional_isrc: isrc } })
    expect(valid).toEqual(true)
  })

  it('returns true if there is not isrc', () => {
    const valid = optionalIsrcValid({ data: {} })
    expect(valid).toEqual(true)
  })

  it("returns false if the isrc is the same as another song's isrc", () => {
    const isrc = '123456789111'
    const valid = optionalIsrcValid(
      { data: { optional_isrc: isrc } },
      { fakeuuid: { data: { optional_isrc: isrc } } }
    )
    expect(valid).toEqual(false)
  })
})
