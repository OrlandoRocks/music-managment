import songNameStyleGuideValidator from '../../../songs/utils/songNameStyleGuideValidator'

describe('songNameStyleGuideValidator', () => {
  context('song name', () => {
    let songName
    let store = function () {
      return {}
    }
    context('featuring', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty feat. Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty Feat. Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty feat Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty Feat Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty featuring Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty Featuring Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (feat. Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (Feat. Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (feat Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (Feat Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (featuring Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty (Featuring Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [feat. Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [Feat. Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [feat Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [Feat Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [featuring Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')

        songName = 'Get Schwifty [Featuring Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('featuring')
      })
    })

    context('explict', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty (explicit)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty [explicit]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty explicit'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty (Explicit)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty [Explicit]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty Explicit'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty (dirty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty [dirty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty dirty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty (Dirty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty [Dirty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')

        songName = 'Get Schwifty Dirty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('explicit')
      })
    })

    context('producer/produced', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty (produced by Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty (Produced by Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty [produced by Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty [Produced by Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty produced by Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty Produced by Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty (prod Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty (Prod Morty)'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty [prod Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty [Prod Morty]'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty prod Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')

        songName = 'Get Schwifty Prod Morty'
        expect(songNameStyleGuideValidator(songName, store)).toBe('produced')
      })
    })
  })
})
