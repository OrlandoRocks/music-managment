import songComplete from '../../../songs/utils/songComplete'

describe('songComplete', () => {
  context('when song is missing name', () => {
    let song = {
      data: {
        name: '',
        asset_url: 'www.example.com',
        artists: [{ role_ids: ['1'] }],
      },
    }

    it('returns false', () => {
      expect(songComplete(song)).toBe(false)
    })
  })

  context('when song is missing asset', () => {
    let song = {
      data: {
        name: 'Song Name',
        asset_url: '',
        artists: [{ role_ids: ['1'] }],
      },
    }

    it('returns false', () => {
      expect(songComplete(song, 1)).toBe(false)
    })
  })

  context('when song is missing songwriter', () => {
    let song = {
      data: {
        name: 'Song Name',
        asset_url: 'www.example.com',
        artists: [{ role_ids: ['2'] }],
      },
    }

    it('returns false', () => {
      expect(songComplete(song, 1)).toBe(false)
    })
  })

  context('when song has name, asset and songwriter', () => {
    let song = {
      data: {
        name: 'Song Name',
        asset_url: 'www.example.com',
        artists: [{ role_ids: ['1'] }],
      },
    }

    it('returns true', () => {
      expect(songComplete(song, 1)).toBe(true)
    })
  })
})
