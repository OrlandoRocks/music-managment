import songsForPreorderCalendar from '../../../songs/utils/songsForPreorderCalendar'
import createStore from '../helpers/store'

describe('songsForPreorderCalendar', () => {
  it('returns the correctly formatted list of songs', () => {
    const store = createStore()
    const { songs, creatives } = store.getState()
    const song = Object.values(songs)[0]
    const songsForCal = songsForPreorderCalendar(
      { [song.uuid]: song },
      creatives
    )
    expect(songsForCal).toEqual([
      {
        id: song.data.id,
        name: song.data.name,
        artist: 'Charlie',
      },
    ])
  })
})
