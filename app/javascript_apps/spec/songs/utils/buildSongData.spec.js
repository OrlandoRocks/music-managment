import createStore from '../helpers/store'
import buildSongData from '../../../songs/utils/buildSongData'

describe('buildSongData', () => {
  it('builds song data for api', () => {
    const {
      songs,
      creatives,
      songwriters,
      songwriterRoleId,
    } = createStore().getState()
    const song = Object.values(songs)[0]
    const songData = buildSongData(
      song,
      creatives,
      songwriters,
      songwriterRoleId
    )
    expect(songData.data.artists[1].role_ids).toContain(songwriterRoleId)
  })
})
