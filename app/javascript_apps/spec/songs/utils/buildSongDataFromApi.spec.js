import buildSongDataFromApi from '../../../songs/utils/buildSongDataFromApi'
import guid from '../../../shared/guid'

describe('buildSongDataFromApi', () => {
  it('builds song data from api', () => {
    const song = {
      data: {
        id: 7,
        name: 'My Fireball',
        language_code_id: 1,
        translated_name: null,
        version: null,
        cover_song: true,
        made_popular_by: null,
        explicit: true,
        optional_isrc: null,
        lyrics: null,
        asset_url: '',
        album_id: 6,
        instrumental: false,
        track_number: 1,
        artists: [
          {
            creative_id: 1,
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['3'],
            uuid: guid(),
          },
          {
            creative_id: 2,
            artist_name: 'Troy',
            associated_to: 'Song',
            credit: 'contributor',
            role_ids: ['2'],
            uuid: guid(),
          },
        ],
      },
      errors: {},
      uuid: guid(),
    }

    const songData = buildSongDataFromApi(song, '2')

    expect(songData.data.artists.length).toBe(1)
    expect(songData.songwriters.length).toBe(1)
    expect(songData.songwriters[0].name).toEqual('Troy')
  })
})
