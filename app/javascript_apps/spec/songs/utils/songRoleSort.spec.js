import songRoleSort from '../../../songs/utils/songRoleSort'
import createStore from '../helpers/store'

describe('songRoleSort', () => {
  it('returns the correct order of song roles', () => {
    const store = createStore()
    const { songRoles } = store.getState()
    const sortedSongRoles = songRoleSort(songRoles)
    const prioritySongRoles = sortedSongRoles
      .slice(0, 3)
      .map((role) => role.role_type_raw)
    expect(prioritySongRoles).toEqual(['performer', 'producer', 'songwriter'])
  })
})
