import songNameValid from '../../../songs/utils/songNameValid'
import createStore from '../helpers/store'

describe('songNameValid', () => {
  it('returns true if song language does not require translation', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 1
    let language = languages[language_code_id]
    const valid = songNameValid(language, 'Song Name')
    expect(valid).toEqual(true)
  })

  it('returns true if song language requires translation, does not have latin characters', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 22
    let language = languages[language_code_id]
    const valid = songNameValid(language, 'אָלֶף־בֵּית עִבְרִי')
    expect(valid).toEqual(true)
  })

  it('returns false if song language requires translation, has latin characters', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 22
    let language = languages[language_code_id]
    const valid = songNameValid(language, 'Song Name')
    expect(valid).toEqual(false)
  })
})
