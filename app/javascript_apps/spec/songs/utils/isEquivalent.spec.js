import isEquivalent from '../../../songs/utils/isEquivalent'

describe('isEquivalent', () => {
  it('returns true for the two identical objects', () => {
    const obj1 = { name: 'Charlie', roles: ['contributor', 'songwriter'] }
    const obj2 = { name: 'Charlie', roles: ['contributor', 'songwriter'] }
    expect(isEquivalent(obj1, obj2)).toBe(true)
  })

  it('returns false for the two unidentical objects', () => {
    const obj1 = {
      name: 'Charlie',
      roles: ['contributor', 'songwriter', 'producer'],
    }
    const obj2 = { name: 'Charlie', roles: ['contributor', 'songwriter'] }
    expect(isEquivalent(obj1, obj2)).toBe(false)
  })
})
