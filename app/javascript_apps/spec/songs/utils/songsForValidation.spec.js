import createStore from '../helpers/store'
import songsForValidation from '../../../songs/utils/songsForValidation'

describe('songsForValidation', () => {
  const songs = createStore().getState().songs
  context('when action type is REMOVE_SONG', () => {
    let song = Object.values(songs)[0]
    let action = {
      type: 'REMOVE_SONG',
      song: { uuid: song.uuid },
    }

    it('returns the list of songs without the removed song', () => {
      let songsToValidate = songsForValidation(action, songs)
      expect(Object.keys(songsToValidate).length).toBe(
        Object.keys(songs).length - 1
      )
      expect(songsToValidate[song.uuid]).toBe(undefined)
    })
  })
  context('when action type is CREATE_SONG or UPDATE_SONG', () => {
    let song = { ...Object.values(songs)[0], data: { name: 'New Song Name' } }
    let action = {
      type: 'UPDATE_SONG',
      song,
    }
    it('returns the list of songs with the updated song', () => {
      let songsToValidate = songsForValidation(action, songs)
      let updatedSongName = songsToValidate[song.uuid].data.name
      expect(songsToValidate.length).toBe(songs.length)
      expect(updatedSongName).toBe('New Song Name')
    })
  })
})
