import isCompleteSong from '../../../songs/utils/isCompleteSong'
import guid from '../../../shared/guid'
import { normalizeSongs } from '../../../songs/store/schema'
import buildSongData from '../../../songs/utils/buildSongData'

describe('isCompleteSong', () => {
  const songwriterRoleId = '3'

  it('returns true for a song that has a title, songwriter, and asset', () => {
    const song = {
      data: {
        name: 'My Song',
        artists: [
          {
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['2'],
            uuid: guid(),
          },
        ],
        asset_url: 'http://example.com/song.mp3',
      },
      songwriters: [{ uuid: guid(), name: 'Charlie', index: 0 }],
      uuid: guid(),
    }

    const {
      entities: { creatives, songs, songwriters },
    } = normalizeSongs([song])
    const songData = buildSongData(
      Object.values(songs)[0],
      creatives,
      songwriters,
      songwriterRoleId
    )
    expect(isCompleteSong(songData)).toBeTruthy()
  })

  it("returns false for a song that doesn't have a title", () => {
    const song = {
      data: {
        name: '',
        artists: [
          {
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['2'],
            uuid: guid(),
          },
        ],
        asset_url: 'http://example.com/song.mp3',
      },
      songwriters: [{ uuid: guid(), name: 'Charlie', index: 0 }],
      uuid: guid(),
    }

    const {
      entities: { creatives, songs, songwriters },
    } = normalizeSongs([song])
    const songData = buildSongData(
      Object.values(songs)[0],
      creatives,
      songwriters,
      songwriterRoleId
    )
    expect(isCompleteSong(songData)).toBeFalsy()
  })

  it("returns false for a song that doesn't have a songwriter", () => {
    const song = {
      data: {
        name: 'My Song',
        artists: [
          {
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['4'],
            uuid: guid(),
          },
        ],
        asset_url: 'http://example.com/song.mp3',
      },
      songwriters: [],
      uuid: guid(),
    }

    const {
      entities: { creatives, songs, songwriters },
    } = normalizeSongs([song])
    const songData = buildSongData(
      Object.values(songs)[0],
      creatives,
      songwriters,
      songwriterRoleId
    )
    expect(isCompleteSong(songData)).toBeFalsy()
  })

  it("returns false for a song that doesn't have a asset", () => {
    const song = {
      data: {
        name: 'My Song',
        artists: [
          {
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['2'],
            uuid: guid(),
          },
        ],
        asset_url: '',
      },
      songwriters: [],
      uuid: guid(),
    }

    const {
      entities: { creatives, songs, songwriters },
    } = normalizeSongs([song])
    const songData = buildSongData(
      Object.values(songs)[0],
      creatives,
      songwriters,
      songwriterRoleId
    )
    expect(isCompleteSong(songData)).toBeFalsy()
  })
})
