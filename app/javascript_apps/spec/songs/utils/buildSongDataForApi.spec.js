import createStore from '../helpers/store'
import buildSongDataForApi from '../../../songs/utils/buildSongDataForApi'

describe('buildSongDataForApi', () => {
  it('builds song data for api', () => {
    const {
      songs,
      creatives,
      songwriters,
      songwriterRoleId,
    } = createStore().getState()
    const song = Object.values(songs)[0]

    const songData = buildSongDataForApi(
      song,
      creatives,
      songwriters,
      songwriterRoleId
    )

    expect(songData.artists[1].role_ids.length).toBe(1)
    expect(songData.artists[1].role_ids).toContain(songwriterRoleId.toString())
  })
})
