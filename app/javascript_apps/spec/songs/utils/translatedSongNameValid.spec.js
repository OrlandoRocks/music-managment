import translatedSongNameValid from '../../../songs/utils/translatedSongNameValid'
import createStore from '../helpers/store'

describe('translatedSongNameValid', () => {
  it('returns true if song language does not require translation', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 1
    let language = Object.values(languages).find(
      (language) => language.id === language_code_id
    )
    const valid = translatedSongNameValid(language, 'Song Name')
    expect(valid).toEqual(true)
  })

  it('returns true if song language requires translation, does have translated name', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 22
    let language = Object.values(languages).find(
      (language) => language.id === language_code_id
    )
    const valid = translatedSongNameValid(language, 'English Version')
    expect(valid).toEqual(true)
  })

  it('returns false if song language requires translation, does not have translated name', () => {
    const store = createStore()
    const languages = store.getState().languages
    const language_code_id = 22
    let language = Object.values(languages).find(
      (language) => language.id === language_code_id
    )
    const valid = translatedSongNameValid(language, '')
    expect(valid).toEqual(false)
  })
})
