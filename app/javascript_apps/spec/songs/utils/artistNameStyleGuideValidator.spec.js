import artistNameStyleGuideValidator from '../../../songs/utils/artistNameStyleGuideValidator'
import variousArtistStore from '../helpers/variousArtistStore'
import guid from '../../../shared/guid'
import { cloneDeep } from 'lodash'

describe('artistNameStyleGuideValidator', () => {
  let artistName
  const artist = {
    associated_to: 'Album',
    credit: 'primary_artist',
    artist_name: 'Rick',
    uuid: guid(),
  }
  const store = {
    album: { is_various: false },
    currentSong: {
      data: {
        artists: [artist.uuid],
      },
    },
    currentCreatives: {
      [artist.uuid]: artist,
    },
  }

  context('featuring', () => {
    it('returns the warning key', () => {
      artistName = 'Rick feat. Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick Feat. Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick feat Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick Feat Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick featuring Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick Featuring Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (feat. Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (Feat. Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (feat Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (Feat Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (featuring Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick (Featuring Morty)'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [feat. Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [Feat. Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [feat Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [Feat Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [featuring Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick [Featuring Morty]'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')
    })
  })

  context('and', () => {
    it('returns the warning key', () => {
      artistName = 'Rick and Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('and')

      artistName = 'Rick And Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('and')
    })
  })

  context('with', () => {
    it('returns the warning key', () => {
      artistName = 'Rick with Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')

      artistName = 'Rick With Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('featuring')
    })
  })

  context('versus', () => {
    it('returns the warning key', () => {
      artistName = 'Rick versus Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('versus')

      artistName = 'Rick Versus Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('versus')
    })
  })

  context('.', () => {
    it('returns the warning key', () => {
      artistName = 'Rick. Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('.')
    })
  })

  context(',', () => {
    it('returns the warning key', () => {
      artistName = 'Rick, Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe(',')

      artistName = 'Rick, Morty and Jerry'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe(',')
    })
  })

  context('-', () => {
    it('returns the warning key', () => {
      artistName = 'Rick - Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('-')
    })
  })

  context('&', () => {
    it('returns the warning key', () => {
      artistName = 'Rick & Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('&')
    })
  })

  context('/', () => {
    it('returns the warning key', () => {
      artistName = 'Rick/Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('/')

      artistName = 'Rick /Morty'
      expect(artistNameStyleGuideValidator(artistName, store)).toBe('/')
    })
  })

  context('duplicate primary artist name', () => {
    it('returns the warning key', () => {
      artistName = 'Rick'
      let dupCreatives = cloneDeep(store)
      dupCreatives.currentCreatives[guid()] =
        dupCreatives.currentCreatives[artist.uuid]

      const action = {
        artistUuid: artist.uuid,
        newName: 'Rick',
        artists: dupCreatives,
      }

      expect(
        artistNameStyleGuideValidator(
          artistName,
          dupCreatives,
          action.artistUuid,
          action
        )
      ).toBe('duplicate_artist_name')
    })
  })

  context("artist named 'various artists'", () => {
    it('returns the warning key', () => {
      artistName = 'Various Artists'
      expect(
        artistNameStyleGuideValidator(artistName, variousArtistStore())
      ).toBe('cannot_be_various_artists')
    })
  })
})
