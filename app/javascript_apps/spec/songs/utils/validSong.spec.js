import createStore from '../helpers/store'
import validSong from '../../../songs/utils/validSong'
import guid from '../../../shared/guid'
import { normalizeSongs } from '../../../songs/store/schema'

describe('validSong', () => {
  it('returns true if a song is valid and has no errors', () => {
    const state = createStore({ explicitFieldsEnabled: true }).getState()
    const song = Object.values(state.songs)[0]
    const storeState = {
      ...state,
      currentCreatives: song.data.artists
        .map((artistId) => state.creatives[artistId])
        .reduce((accumulator, artist) => {
          return {
            ...accumulator,
            [artist.uuid]: artist,
          }
        }, {}),
      currentSongwriters: song.songwriters
        .map((songwriterId) => state.songwriters[songwriterId])
        .reduce((accumulator, songwriter) => {
          return {
            ...accumulator,
            [songwriter.uuid]: songwriter,
          }
        }, {}),
    }

    expect(
      Object.values(validSong(song, storeState)).filter(
        (value) => value !== false
      ).length
    ).toEqual(0)
  })

  it('returns false if a song is invalid and has errors', () => {
    const state = createStore({ explicitRadioEnabled: true }).getState()
    let song = {
      data: {
        name: 'First Song',
        language_code_id: 22,
        artists: [
          {
            artist_name: 'Charlie',
            associated_to: 'Album',
            credit: 'primary_artist',
            role_ids: ['11'],
            uuid: guid(),
          },
          {
            artist_name: '',
            associated_to: 'Song',
            credit: '',
            role_ids: ['22'],
          },
        ],
        translated_name: '',
        explicit: null,
      },
      uuid: guid(),
    }
    const {
      entities: { songs, creatives },
    } = normalizeSongs([song])
    const storeState = {
      ...state,
      songs: [song],
      currentCreatives: creatives,
      currentSongwriters: {},
    }

    song = Object.values(songs)[0]

    expect(
      Object.values(validSong(song, storeState)).filter(
        (value) => value !== false
      ).length
    ).toEqual(5)
  })
})
