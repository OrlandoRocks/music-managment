import songsCompleteCount from '../../../songs/utils/songsCompleteCount'
import { normalizeSongs } from '../../../songs/store/schema'
import guid from '../../../shared/guid'
import buildSongDataFromApi from '../../../songs/utils/buildSongDataFromApi'

describe('songsCompleteCount', () => {
  it('returns the count of completed songs', () => {
    const songwriterRoleId = '2'
    const songData = [
      {
        data: {
          name: 'My Song',
          artists: [
            {
              artist_name: 'Charlie',
              associated_to: 'Album',
              credit: 'primary_artist',
              role_ids: ['2'],
              uuid: guid(),
            },
          ],
          asset_url: 'http://example.com/song.mp3',
        },
        uuid: guid(),
      },
      {
        data: {
          name: 'My Song',
          artists: [
            {
              artist_name: 'Charlie',
              associated_to: 'Album',
              credit: 'primary_artist',
              role_ids: ['4'],
              uuid: guid(),
            },
          ],
          asset_url: 'http://example.com/song.mp3',
        },
        uuid: guid(),
      },
    ]

    const {
      entities: { creatives, songs, songwriters },
    } = normalizeSongs(
      songData.map((song) => buildSongDataFromApi(song, songwriterRoleId))
    )
    expect(
      songsCompleteCount(songs, creatives, songwriters, songwriterRoleId)
    ).toEqual(1)
  })
})
