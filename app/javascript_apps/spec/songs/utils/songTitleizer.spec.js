import songTitleizer from '../../../songs/utils/songTitleizer'

describe('songTitleizer', () => {
  context('capitalize', () => {
    it('capitalizes the song name on a title for the first and last words', () => {
      const song = {
        data: {
          name: 'a girl worth fighting for',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('A Girl Worth Fighting For')
    })

    it('capitalizes the song name on a title for the middle words that are not excluded', () => {
      const song = {
        data: {
          name: 'welcome to the black parade',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('Welcome to the Black Parade')
    })

    it('capitalizes the first word of song name on a title for cyrillic characters', () => {
      const song = {
        data: {
          name: 'приятно познакомиться',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('Приятно познакомиться')
    })

    it('uppercases roman numerals in song titles', () => {
      const song = {
        data: {
          name: 'my song xiii',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('My Song XIII')
    })

    it('uppercases common uppercased words', () => {
      const song = {
        data: {
          name: 'dj cable rules the world',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('DJ Cable Rules the World')
    })

    it('maintains mix-cased song titles', () => {
      const song = {
        data: {
          name: 'iNterSteLLar',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('iNterSteLLar')
    })

    it('downcases after an apostrophe in posessives and contractions', () => {
      const song = {
        data: {
          name: "Jeff Price'S Revenge",
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual("Jeff Price's Revenge")
    })

    it("doesn't downcase random letters after an apostrophe", () => {
      const song = {
        data: {
          name: "'Twas the night before Christmas",
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual(
        "'Twas the Night Before Christmas"
      )
    })

    it('leaves integer titles as is', () => {
      const song = {
        data: {
          name: '1234567890',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('1234567890')
    })

    it('leaves titles with just integers and symbols as is', () => {
      const song = {
        data: {
          name: '& ^ 12345 - !!! ? * 67890',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('& ^ 12345 - !!! ? * 67890')
    })

    it('leaves trailing integers as is', () => {
      const song = {
        data: {
          name: 'song 123',
          artists: [],
        },
      }

      expect(songTitleizer(song.data)).toEqual('Song 123')
    })

    context('with accented letters', () => {
      it('maintains the proper casing of accented letters', () => {
        const song = {
          data: {
            name: 'Piña Colada',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('Piña Colada')
      })

      it('maintains mix-cased song titles with accented characters', () => {
        const song = {
          data: {
            name: 'ćhełšëÂ',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('ćhełšëÂ')
      })

      it('capitalizes the leading accented letter in the middle of a song title', () => {
        const song = {
          data: {
            name: 'i ñame what i yam',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('I Ñame What I Yam')
      })

      it('capitalizes the leading accented letter at the start and in the middle of a song title', () => {
        const song = {
          data: {
            name: 'yüreğim i̇mparator',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('Yüreğim İmparator')
      })
    })

    context('trims', () => {
      it('whitespace characters', () => {
        const song = {
          data: {
            name: '  trimmed   song   title  ',
            artists: [],
          },
        }
        expect(songTitleizer(song.data)).toEqual('Trimmed Song Title')
      })
    })
  })

  context('shows featuring artists', () => {
    it("using the 'featuring' word", () => {
      const song = {
        data: {
          name: 'iNterStellar',
          artists: [
            {
              artist_name: 'Frank',
              credit: 'featuring',
            },
          ],
        },
      }

      expect(songTitleizer(song.data)).toEqual('iNterStellar (feat. Frank)')
    })

    it("using the 'featuring' word with more than one artist", () => {
      const song = {
        data: {
          name: 'iNterStellar',
          artists: [
            {
              artist_name: 'Frank',
              credit: 'featuring',
            },
            {
              artist_name: 'Charlie',
              credit: 'featuring',
            },
          ],
        },
      }

      expect(songTitleizer(song.data)).toEqual(
        'iNterStellar (feat. Frank & Charlie)'
      )
    })

    it("using many 'featuring' artists", () => {
      const song = {
        data: {
          name: 'iNterStellar',
          artists: [
            {
              artist_name: 'Frank',
              credit: 'featuring',
            },
            {
              artist_name: 'Sophie',
              credit: 'featuring',
            },
            {
              artist_name: 'Troy',
              credit: 'featuring',
            },
            {
              artist_name: 'Michael',
              credit: 'featuring',
            },
          ],
        },
      }

      expect(songTitleizer(song.data)).toEqual(
        'iNterStellar (feat. Frank, Sophie, Troy & Michael)'
      )
    })
  })

  context('with a sub-title using parenthesises', () => {
    it('wraps featuring in square brackets', () => {
      const song = {
        data: {
          name: 'iNterStellar (live @ MSG)',
          artists: [
            {
              artist_name: 'Frank',
              credit: 'featuring',
            },
            {
              artist_name: 'Sophie',
              credit: 'featuring',
            },
          ],
        },
      }

      expect(songTitleizer(song.data)).toEqual(
        'iNterStellar (Live @ MSG) [feat. Frank & Sophie]'
      )
    })
  })

  context(
    'preventing a user from manually adding an artist in the title',
    () => {
      it('prevents adding a featuring artist', () => {
        const song = {
          data: {
            name: 'iNterStellar ( featuring Charlie )',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('iNterStellar')
      })

      it('prevents adding a featuring with artist', () => {
        const song = {
          data: {
            name: 'iNterStellar ( with Charlie )',
            artists: [],
          },
        }

        expect(songTitleizer(song.data)).toEqual('iNterStellar')
      })
    }
  )
})
