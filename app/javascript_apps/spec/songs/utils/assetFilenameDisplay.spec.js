import assetFilenameDisplay from '../../../songs/utils/assetFilenameDisplay'

describe('assetFilenameDisplay', () => {
  it('returns the assetFilename', () => {
    const assetFilename = '2s-beats.mp3'
    expect(assetFilenameDisplay(assetFilename)).toBe(assetFilename)
  })

  it("returns a truncated assetFilename if it's too long", () => {
    const assetFilename = '222222222222222222222222222222s-beats.mp3'

    expect(assetFilenameDisplay(assetFilename)).toBe(
      '2222222222222222222...mp3'
    )
  })
})
