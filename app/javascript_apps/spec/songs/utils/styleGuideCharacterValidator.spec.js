import styleGuideCharacterValidator from '../../../songs/utils/styleGuideCharacterValidator'

describe('styleGuideCharacterValidator', () => {
  context('song name', () => {
    let songName
    context('featuring', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty feat. Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty Feat. Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty feat Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty Feat Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty featuring Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty Featuring Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (feat. Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (Feat. Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (feat Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (Feat Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (featuring Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (Featuring Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [feat. Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [Feat. Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [feat Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [Feat Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [featuring Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [Featuring Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty ft Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty Ft Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (ft Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty (FT Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [ft Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty [Ft Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )

        songName = 'Get Schwifty Ft. Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'featuring'
        )
      })
    })

    context('explict', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty (explicit)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty [explicit]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty explicit'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty (Explicit)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty [Explicit]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty Explicit'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty (dirty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty [dirty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty dirty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty (Dirty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty [Dirty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )

        songName = 'Get Schwifty Dirty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'explicit'
        )
      })
    })

    context('producer/produced', () => {
      it('returns the warning key', () => {
        songName = 'Get Schwifty (produced by Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty (Produced by Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty [produced by Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty [Produced by Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty produced by Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty Produced by Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty (prod Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty (Prod Morty)'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty [prod Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty [Prod Morty]'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty prod Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )

        songName = 'Get Schwifty Prod Morty'
        expect(styleGuideCharacterValidator(songName, 'song_name')).toBe(
          'produced'
        )
      })
    })
  })
  context('artist name', () => {
    let artistName
    context('featuring', () => {
      it('returns the warning key', () => {
        artistName = 'Rick feat. Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick Feat. Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick feat Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick Feat Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick featuring Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick Featuring Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (feat. Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (Feat. Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (feat Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (Feat Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (featuring Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick (Featuring Morty)'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [feat. Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [Feat. Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [feat Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [Feat Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [featuring Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick [Featuring Morty]'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )
      })
    })

    context('and', () => {
      it('returns the warning key', () => {
        artistName = 'Rick and Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'and'
        )

        artistName = 'Rick And Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'and'
        )
      })
    })

    context('with', () => {
      it('returns the warning key', () => {
        artistName = 'Rick with Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )

        artistName = 'Rick With Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'featuring'
        )
      })
    })

    context('versus', () => {
      it('returns the warning key', () => {
        artistName = 'Rick versus Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'versus'
        )

        artistName = 'Rick Versus Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          'versus'
        )
      })
    })

    context('.', () => {
      it('returns the warning key', () => {
        artistName = 'Rick. Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          '.'
        )
      })
    })

    context(',', () => {
      it('returns the warning key', () => {
        artistName = 'Rick, Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          ','
        )

        artistName = 'Rick, Morty and Jerry'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          ','
        )
      })
    })

    context('-', () => {
      it('returns the warning key', () => {
        artistName = 'Rick - Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          '-'
        )
      })
    })

    context('&', () => {
      it('returns the warning key', () => {
        artistName = 'Rick & Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          '&'
        )
      })
    })

    context('/', () => {
      it('returns the warning key', () => {
        artistName = 'Rick/Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          '/'
        )

        artistName = 'Rick /Morty'
        expect(styleGuideCharacterValidator(artistName, 'artist_name')).toBe(
          '/'
        )
      })
    })
  })
})
