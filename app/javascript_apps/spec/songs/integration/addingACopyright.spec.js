import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('adding a copyright', () => {
  const store = createStore({ copyrightsFormEnabled: true })
  const song = Object.values(store.getState().songs)[0]
  const app = mount(
    <Provider store={store}>
      <SongsContainer />
    </Provider>
  )

  it('adds a copyright', () => {
    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    const editButton = trackListItem.find('.edit-song')
    editButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const compositionCbx = form.find('#song-copyrights-composition').first()
    const recordingCbx = form.find('#song-copyrights-recording').first()
    compositionCbx.simulate('click')
    recordingCbx.simulate('click')
    const currentCopyrights = Object.values(
      store.getState().currentSong.data.copyrights
    )
    expect(currentCopyrights).toEqual([true, true])
  })
})
