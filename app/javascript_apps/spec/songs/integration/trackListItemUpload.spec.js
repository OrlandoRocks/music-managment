import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('upload file on track list item', () => {
  context('upload failed', () => {
    const store = createStore()

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    it('shows the error', () => {
      const errorMessage = app.find('.track-list-item-asset-upload-failure')
      expect(errorMessage.length).toBe(1)
    })
  })

  context('duration failure', () => {
    const store = createStore()

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    it('shows the error', () => {
      const errorMessage = app.find('.track-list-item-asset-duration-failure')
      expect(errorMessage.length).toBe(2)
    })
  })
})
