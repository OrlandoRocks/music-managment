import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import createSingleStore from '../helpers/singleStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('previously released form section', () => {
  context('single has not been previously released', () => {
    it('does not show the previously released checkbox', () => {
      const store = createSingleStore()
      const song = Object.values(store.getState().songs)[0]

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const previouslyReleasedCheckBox = app.find('#previouslyReleased')
      expect(previouslyReleasedCheckBox.length).toBe(0)
    })
  })

  context('single has been previously released', () => {
    it('disables the previously released checkbox', () => {
      const store = createSingleStore()
      const song = Object.values(store.getState().songs)[0]
      const single = store.getState().album
      single.previously_released_at = '2018-01-01'

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const previouslyReleasedCheckBox = app.find('#previouslyReleased')
      expect(previouslyReleasedCheckBox.prop('disabled')).toBe(true)
    })
  })

  context('album has not been previously released', () => {
    it('enables the previously released checkbox', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const previouslyReleasedCheckBox = app.find('#previouslyReleased')
      expect(previouslyReleasedCheckBox.prop('disabled')).toBe(false)
    })
  })

  context('album has been previously released', () => {
    it('disables the previously released checkbox', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]
      const album = store.getState().album
      album.previously_released_at = '2018-01-01'

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const previouslyReleasedCheckBox = app.find('#previouslyReleased')
      expect(previouslyReleasedCheckBox.prop('disabled')).toBe(true)
    })
  })

  context('album previously released at date is an empty string', () => {
    it('does not disable the previously released checkbox', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]
      const album = store.getState().album
      album.previously_released_at = ''

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const previouslyReleasedCheckBox = app.find('#previouslyReleased')
      expect(previouslyReleasedCheckBox.prop('disabled')).toBeFalsy()
    })
  })
})
