import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'
import { changeCurrentSong } from '../../../songs/actions/songActions'

Enzyme.configure({ adapter: new Adapter() })

describe('copy songwriters from previous song', () => {
  const store = createStore()
  const { songs, creatives } = store.getState()
  const songsList = Object.values(songs)
  const song = songsList[3]
  const previousSongArtists = songsList[2].data.artists.map(
    (artistId) => creatives[artistId]
  )
  store.dispatch(changeCurrentSong(song.uuid))
  const app = mount(
    <Provider store={store}>
      <SongsContainer />
    </Provider>
  )

  const copyPreviousButton = app.find('.copy-songwriters')
  copyPreviousButton.simulate('click')

  it("replaces the song's songwriters with the songwriters from the previous song", () => {
    const newForm = app.find(`form#song_form_${song.uuid}`)
    expect(
      newForm.find('.songwriter-text-container input').at(1).props().value
    ).toBe(previousSongArtists[1].artist_name)
  })

  it('disables the copy from previous button if the songwriters match', () => {
    const newForm = app.find(`form#song_form_${song.uuid}`)
    expect(newForm.find('.copy-songwriters').props().disabled).toBe(true)
  })
})
