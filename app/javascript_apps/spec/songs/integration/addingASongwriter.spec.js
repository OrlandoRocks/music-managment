import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('adding a songwriter', () => {
  const store = createStore()
  const song = Object.values(store.getState().songs)[0]
  const app = mount(
    <Provider store={store}>
      <SongsContainer />
    </Provider>
  )

  it('adds a songwriter', () => {
    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    const editButton = trackListItem.find('.edit-song')
    editButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const songwriterForm = form.find('.songwriter-form')
    const addSongwriterBtn = songwriterForm.find('.add-songwriter')
    addSongwriterBtn.simulate('click')
    const songwriterInput = form
      .find('.songwriter-text-container')
      .last()
      .find('input')
    songwriterInput.simulate('change', { target: { value: 'Frank' } })
    const currentSongwriterNames = Object.values(
      store.getState().currentSongwriters
    ).map((songwriter) => songwriter.name)
    expect(currentSongwriterNames).toContain('Frank')
  })

  it('removes a songwriter', () => {
    let song = Object.values(store.getState().songs)[2]
    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    const editButton = trackListItem.find('.edit-song')
    editButton.simulate('click')
    const initialLength = song.songwriters.length
    const form = app.find(`form#song_form_${song.uuid}`)
    const songwriterForm = form.find('.songwriter-form')
    const songwriterIndividualContainer = songwriterForm
      .find('.songwriter-text-container')
      .last()
    const removeSongwriterBtn = songwriterIndividualContainer.find(
      '.songwriter-remove-btn'
    )
    removeSongwriterBtn.simulate('click')
    expect(store.getState().currentSong.songwriters.length).toEqual(
      initialLength - 1
    )
  })
})
