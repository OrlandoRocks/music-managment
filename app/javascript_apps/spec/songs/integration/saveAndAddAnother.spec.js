import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/singleStore'
import { changeCurrentSong } from '../../../songs/actions/songActions'
import setjQueryUpdate from '../helpers/setjQueryUpdate'

Enzyme.configure({ adapter: new Adapter() })

describe('saving a song', () => {
  context('editing a saved song', () => {
    const store = createStore()
    let { songs } = store.getState()
    const song = Object.values(songs)[0]
    store.dispatch(changeCurrentSong(song.uuid))
    setjQueryUpdate({ ...song.data }, song, store)

    it('save button does not add another new song', () => {
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )
      const form = app.find(`form#song_form_${song.uuid}`)
      form.simulate('submit')
      const newForm = app.find(`form`)
      expect(newForm.length).toBe(0)
    })
  })
})
