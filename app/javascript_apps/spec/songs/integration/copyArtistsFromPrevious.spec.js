import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'
import { changeCurrentSong } from '../../../songs/actions/songActions'

Enzyme.configure({ adapter: new Adapter() })

describe('copy artists from previous song', () => {
  const store = createStore()
  const song = Object.values(store.getState().songs)[3]
  store.dispatch(changeCurrentSong(song.uuid))
  const app = mount(
    <Provider store={store}>
      <SongsContainer />
    </Provider>
  )

  const copyPreviousButton = app.find('.copy-artists')
  copyPreviousButton.simulate('click')

  it("replaces the song's artists with the artists from the previous song", () => {
    const newForm = app.find(`form#song_form_${song.uuid}`)
    expect(
      newForm.find('.artist-row .artist-name-autocomplete-input').at(1).props()
        .value
    ).toBe('Troy')
  })

  it('disables the copy from previous button if the artists match', () => {
    const newForm = app.find(`form#song_form_${song.uuid}`)
    expect(newForm.find('.copy-artists').props().disabled).toBe(true)
  })
})
