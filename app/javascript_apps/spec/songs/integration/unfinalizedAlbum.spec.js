import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/unfinalizedStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('unfinalized album', () => {
  it("doesn't allow a user to edit song order", () => {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    expect(app.find('.edit-song-order').length).toBe(0)
  })

  it("doesn't allow a user to add songs", () => {
    const store = createStore()
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    expect(app.find('.add-song').length).toBe(0)
  })

  it("doesn't allow a user to delete songs", () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    expect(app.find('.remove-song').length).toBe(0)
    app.find(`#track_list_item_${song.uuid} .edit-song`).simulate('click')
    expect(app.find('.form-remove-song').length).toBe(0)
  })

  it("doesn't allow a user to edit ISRC", () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    app.find(`#track_list_item_${song.uuid} .edit-song`).simulate('click')

    expect(app.find('.song-optional-isrc input').props().disabled).toBe(true)
  })

  context('when the album is Instrumental', () => {
    it('does not display the instrumental checkbox or the lyrics section', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      app.find(`#track_list_item_${song.uuid} .edit-song`).simulate('click')

      const instrumentalCheckbox = app.find('.song-is-instrumental')
      expect(instrumentalCheckbox.length).toBe(0)

      const lyricsSection = app.find('.song-lyrics')
      expect(lyricsSection.length).toBe(0)
    })
  })
})
