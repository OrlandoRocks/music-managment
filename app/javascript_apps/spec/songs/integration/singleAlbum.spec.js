import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/singleStore'
import { Provider } from 'react-redux'
import setjQueryUpdate from '../helpers/setjQueryUpdate'

Enzyme.configure({ adapter: new Adapter() })

describe('editing a single', () => {
  const setup = () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]
    setjQueryUpdate({}, song, store)
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    return {
      store,
      song,
      app,
    }
  }

  it('does not render the songForm after update', () => {
    const { song, app } = setup()
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    form.simulate('submit')
    const newForm = app.find(`form`)
    expect(newForm.length).toBe(0)
  })

  it('does not show the remove song button', () => {
    const { song, app } = setup()
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const removeButton = form.find('button.form-remove-song')
    expect(removeButton.length).toBe(0)
  })

  it("does not show the 'I am done adding songs' button", () => {
    const { song, app } = setup()
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const doneButton = form.find('.cancel-song')
    expect(doneButton.length).toBe(0)
  })

  it('does not allow ISRC to be edited for a single', () => {
    const { song, app } = setup()
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const isrcField = form.find('.song-optional-isrc input')
    expect(isrcField.props().disabled).toBe(true)
  })

  it('does not render the add-song button in the songs header', () => {
    const { app } = setup()
    const addSongButton = app.find('.add-song')
    expect(addSongButton.length).toBe(0)
  })

  it('renders only the edit and play tracklist item buttons for a track', () => {
    const { song, app } = setup()

    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    trackListItem.simulate('mouseenter')
    const buttonSet = app.find(
      '.track-list-item-buttons-for-spatial-audio button'
    )

    expect(buttonSet.length).toBe(2)
    expect(buttonSet.find('.song-file-upload-button').length).toBe(0)
    expect(buttonSet.find('.edit-song').length).toBe(1)
    expect(buttonSet.find('.tracklist-remove-song').length).toBe(0)
  })

  it("displays the Single's file name", () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]

    song.data = { ...song.data, asset_filename: 'asset_filename' }
    setjQueryUpdate({}, song, store)

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    trackListItem.simulate('mouseenter')
    const filename = app.find('.track-list-item-asset-filename')

    expect(filename).toHaveText('asset_filename')
  })
})
