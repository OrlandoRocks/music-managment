import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('style guide warnings', () => {
  context('song name contains feat.', () => {
    it('shows a warning', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const form = app.find(`form#song_form_${song.uuid}`)
      const titleField = form.find('.song-title input')
      titleField.simulate('change', { target: { value: 'Feat. ' } })
      expect(app.find(`.field-warning-text`).length).toBe(1)
    })
  })

  context('artist name contains feat.', () => {
    it('shows a warning', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[1]

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )
      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const form = app.find(`form#song_form_${song.uuid}`)
      const artistRowNameInput = form
        .find('.artist-song-name')
        .at(1)
        .find('input')
      artistRowNameInput.simulate('change', { target: { value: 'Feat.' } })
      expect(app.find(`.songwriter-messagetext p`).length).toBe(1)
    })
  })
})
