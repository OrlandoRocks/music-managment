import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/variousArtistAlbumStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('various artists', () => {
  it("displays 'Various Artists' in the track list item", () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    expect(trackListItem.find('.main-artist').text()).toEqual(
      'by Various Artists'
    )
  })
})
