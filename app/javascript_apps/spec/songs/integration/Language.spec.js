import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/variousArtistAlbumStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('Language selection', () => {
  xit('shows second form field with correct label', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    let form = app.find(`form#song_form_${song.uuid}`)
    form.find('.language-menu').simulate('change', { target: { value: 22 } })
    form = app.find(`form#song_form_${song.uuid}`)
    const label = form.find('.translated-name-label')
    expect(label.text()).toBe('Song Title Transliteration*')
  })

  xit('shows the language descriptions in alphabetical order', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    let form = app.find(`form#song_form_${song.uuid}`)
    const selectMenu = form.find('.language-menu')
    expect(selectMenu.childAt(0).text()).toBe('Arabic')
  })
})
