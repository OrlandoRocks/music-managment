import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import TrackListItem from '../../../songs/components/TrackListItem'
import createStore from '../helpers/store'
import createFinalized from '../helpers/finalizedStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('TrackListItem', () => {
  it('checks validity of the song', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[1]
    const trackListItem = mount(
      <Provider store={store}>
        <TrackListItem songUuid={song.uuid} />
      </Provider>
    )

    expect(trackListItem.find('.invalid-song').length).toBe(0)
  })

  it('checks invalidity of the song', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]
    const trackListItem = mount(
      <Provider store={store}>
        <TrackListItem songUuid={song.uuid} artistIds={song.data.artists} />
      </Provider>
    )

    expect(trackListItem.find('.invalid-song').length).toBe(1)
  })

  it('does not render buttons that allow changes to a finalized album', () => {
    const store = createFinalized()
    const song = Object.values(store.getState().songs)[0]
    const trackListItem = mount(
      <Provider store={store}>
        <TrackListItem songUuid={song.uuid} artistIds={song.data.artists} />
      </Provider>
    )

    expect(trackListItem.children().length).toBe(1)
    expect(trackListItem.find('.edit-song').length).toBe(0)
  })
})
