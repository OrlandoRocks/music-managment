import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createStore from '../helpers/store'
import TrackListItemButtons from '../../../songs/components/TrackListItemButtons'

Enzyme.configure({ adapter: new Adapter() })

describe('TrackListItemButtons', () => {
  it('does not render delete icon for atmos file after deletion', () => {
    const store = createStore({ spatialAudioEnabled: true })
    let song = Object.values(store.getState().songs)[1]
    window.sidebar = () => {}

    // Mock deletion of Atmos file on backend
    const mockRemoveAtmos = () => {
      store.dispatch({
        data: { songUuid: song.uuid },
        type: 'REMOVE_SONG_ATMOS_ASSET',
      })
    }

    const trackItemButtons = mount(
      <TrackListItemButtons
        song={song}
        albumFinalized={false}
        spatialAudioEnabled={true}
        albumIsEditable={true}
        isHovering={true}
        albumIsLoading={false}
        showsDragging={false}
        onRemoveSongAtmosAsset={mockRemoveAtmos}
      />
    )
    let deleteAtmosButton = trackItemButtons.find('.remove-dolby-atmos').first()
    expect(deleteAtmosButton.get(0).props.display).toBe('inline-block')
    deleteAtmosButton.simulate('click')

    // Manually update props as mapStateToProps is set on parent (TrackListItem) component
    // which would pass the new state/prop to child (TrackListItemButtons)
    song = Object.values(store.getState().songs)[1]
    trackItemButtons.setProps({ song: song })
    deleteAtmosButton = trackItemButtons.find('.remove-dolby-atmos').first()
    expect(deleteAtmosButton.get(0).props.display).toBe('none')
  })

  it('deactivates spatial audio modal for stereo upload', () => {
    const store = createStore({ spatialAudioEnabled: true })
    let song = Object.values(store.getState().songs)[1]
    song.errors = {
      validate_duration: 'immersive_duration_error',
    }
    const mockDeactivateSpatialModal = jest.fn()
    const songFile = new File(['song'], 'song.wav', { type: 'audio/wav' })
    const trackItemButtons = mount(
      <TrackListItemButtons
        song={song}
        albumFinalized={false}
        spatialAudioEnabled={true}
        onFileUpload={jest.fn()}
        onEditSong={() => true}
        albumIsLoading={false}
        currentlyUploading={false}
        deactivateSpatialAudioModalNotificationState={
          mockDeactivateSpatialModal
        }
      />
    )
    let stereoUpload = trackItemButtons.find('.song-file-upload-input').first()
    stereoUpload.simulate('change', { target: { files: [songFile] } })
    expect(mockDeactivateSpatialModal.mock.calls).toHaveLength(1)
  })
})
