import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'

import createStore from '../helpers/store'
import SongsHeader from '../../../songs/components/SongsHeader'
import { ALBUM_RELOAD } from '../../../songs/actions/actionTypes'

Enzyme.configure({ adapter: new Adapter() })

describe('SongsHeader', () => {
  let app, store

  beforeEach(() => {
    store = createStore()
    app = mount(
      <Provider store={store}>
        <SongsHeader />
      </Provider>
    )
  })

  it('renders the song header', () => {
    const topBar = app.find('.songs_top-bar')
    expect(topBar).toExist()
  })

  context('add songs button', () => {
    it('should show', () => {
      expect(app.find('button.add-song')).toExist()
    })

    it('should not show if track limit has reached', () => {
      store.dispatch({
        type: ALBUM_RELOAD,
        data: {
          album: {
            track_limit_reached: true,
          },
        },
      })
      app.update()
      expect(app.find('button.add-song').length).toBe(0)
    })
  })
})
