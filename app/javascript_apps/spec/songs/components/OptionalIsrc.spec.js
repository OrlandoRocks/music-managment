import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import OptionalIsrc from '../../../songs/components/sections/OptionalIsrc'

Enzyme.configure({ adapter: new Adapter() })

describe('OptionalIsrc', () => {
  context('when the album has been previously released', () => {
    it('shows the placeholder text', () => {
      const optionalIsrc = mount(
        <OptionalIsrc
          onAttributeUpdate={function () {}}
          previouslyReleased="2017-01-01"
        />
      )
      expect(optionalIsrc.find('input').props().placeholder).toBe(
        'Please use the ISRC that was assigned to this song at the time of release'
      )
    })
  })

  context('when the album has not been previously released', () => {
    it('shows the placeholder text', () => {
      const optionalIsrc = mount(
        <OptionalIsrc onAttributeUpdate={function () {}} />
      )
      expect(optionalIsrc.find('input').props().placeholder).toBe('')
    })
  })

  context("when it's a single", () => {
    it("doesn't disable the field.", () => {
      const optionalIsrc = mount(
        <OptionalIsrc
          onAttributeUpdate={function () {}}
          albumType={'Single'}
          isEditable={true}
        />
      )

      expect(optionalIsrc.find('input').props().disabled).toBe(false)
    })
  })
})
