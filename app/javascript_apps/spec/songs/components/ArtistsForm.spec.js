import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ArtistsForm from '../../../songs/components/sections/ArtistsForm'
import createStore from '../helpers/currentSongStore'
import { Provider } from 'react-redux'
import { changeCurrentSong } from '../../../songs/actions/songActions'

Enzyme.configure({ adapter: new Adapter() })

describe('ArtistsForm', () => {
  it('adds an artist', () => {
    const store = createStore()
    const songs = Object.values(store.getState().songs)
    const song = songs[1]
    const artistIds = song.data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    store.dispatch(changeCurrentSong(song.uuid))
    const artistsForm = mount(
      <Provider store={store}>
        <ArtistsForm
          addSongwriter={() => {
            artists.push({
              artist_name: '',
              associated_to: 'Song',
              credit: '',
              role_ids: ['2'],
            })
          }}
          songwriterRoleId={'1'}
          onRemoveArtist={() => {}}
          onEditArtist={() => {}}
          onAddArtist={() => {
            artists.push({
              artist_name: '',
              associated_to: 'Song',
              credit: '',
              role_ids: [],
            })
          }}
        />
      </Provider>
    )

    let length = artists.length

    artistsForm.find('.add-artist').simulate('click')
    expect(artists.length).toBe(length + 1)
  })

  it('removes an artist', () => {
    const store = createStore()
    const songs = Object.values(store.getState().songs)
    const song = songs[1]
    const artistIds = song.data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    store.dispatch(changeCurrentSong(song.uuid))
    const artistsForm = mount(
      <Provider store={store}>
        <ArtistsForm
          addSongwriter={() => {
            artists.push({
              artist_name: '',
              associated_to: 'Song',
              credit: '',
              role_ids: ['2'],
            })
          }}
          songwriterRoleId={'1'}
          onRemoveArtist={() => {
            artists.pop()
          }}
          onEditArtist={() => {}}
          onAddArtist={() => {}}
        />
      </Provider>
    )

    let length = artists.length
    const removeArtistButton = artistsForm
      .find('.artist-row')
      .last()
      .find('.remove-artist')
    removeArtistButton.simulate('click')
    expect(artists.length).toBe(length - 1)
  })

  it('does not allow an Album level artist to be removed', () => {
    const store = createStore()
    const songs = Object.values(store.getState().songs)
    const song = songs[1]
    const artistIds = song.data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    store.dispatch(changeCurrentSong(song.uuid))
    const artistsForm = mount(
      <Provider store={store}>
        <ArtistsForm
          addSongwriter={() => {
            artists.push({
              artist_name: '',
              associated_to: 'Song',
              credit: '',
              role_ids: ['2'],
            })
          }}
          onRemoveArtist={() => {}}
          onEditArtist={() => {}}
          onAddArtist={() => {}}
        />
      </Provider>
    )

    const albumLevelArtist = artists.find(
      (artist) => artist.associated_to === 'Album'
    )
    const removeButtons = artistsForm
      .find(`#artist_row_${albumLevelArtist.artist_name}`)
      .find('.remove-artist')
    expect(removeButtons.length).toBe(0)
  })

  it('allows a Song level artist to be removed', () => {
    const store = createStore()
    const songs = Object.values(store.getState().songs)

    const song = songs[1]
    const artistIds = song.data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    store.dispatch(changeCurrentSong(song.uuid))
    const artistsForm = mount(
      <Provider store={store}>
        <ArtistsForm
          addSongwriter={() => {
            artists.push({
              artist_name: '',
              associated_to: 'Song',
              credit: '',
              role_ids: ['2'],
            })
          }}
          songwriterRoleId={'1'}
          onRemoveArtist={() => {}}
          onEditArtist={() => {}}
          onAddArtist={() => {}}
        />
      </Provider>
    )

    const songLevelArtist = artists.find(
      (artist) => artist.associated_to === 'Song'
    )
    const removeButtons = artistsForm
      .find(`#artist_row_${songLevelArtist.artist_name}`)
      .find('.remove-artist')
    expect(removeButtons.length).toBe(1)
  })
})
