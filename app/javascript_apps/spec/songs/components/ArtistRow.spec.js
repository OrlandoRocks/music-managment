import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ArtistRow from '../../../songs/components/sections/ArtistRow'
import createStore from '../helpers/currentSongStore'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('ArtistRow', () => {
  it('renders the artist', () => {
    const store = createStore()
    const artist = {
      artist_name: 'Chance the Rapper',
      associated_to: 'Album',
      credit: 'primary_artist',
      role_ids: [17],
    }

    const artistRow = mount(
      <Provider store={store}>
        <ArtistRow
          artist={artist}
          onRemoveArtist={() => {}}
          onEditArtist={() => {}}
          index={1}
          songwriterRoleId={'1'}
          moreThanOneSongwriter={true}
          moreThanOnePrimaryArtist={false}
          albumType="Album"
        />
      </Provider>
    )
    expect(artistRow.find('input').first().props().value).toBe(
      'Chance the Rapper'
    )
  })

  it('edits the artist name', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[1].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={false}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const artistRowNameInput = artistRows
      .find('.artist-song-name')
      .at(1)
      .find('input')
    artistRowNameInput.simulate('change', { target: { value: 'Drake' } })
    artistRowNameInput.simulate('blur')
    expect(artists[1].artist_name).toBe('Drake')
  })

  it('track level primary artists are enabled', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[7].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={true}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Dana')
    const primaryArtistTwo = artistRows.find('#artist_row_Frank')

    expect(primaryArtistOne.find('button').length).toBe(0)
    expect(
      primaryArtistOne.find('.artist-song-name_no_edit').props().disabled
    ).toBe(true)

    expect(primaryArtistTwo.find('button').length).toBe(1)
    expect(
      primaryArtistTwo.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)
  })

  it('all primary artists are enabled if there is more than one on a Single', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[8].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={true}
                albumType="Single"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Dana')
    const primaryArtistTwo = artistRows.find('#artist_row_Frank')

    expect(primaryArtistOne.find('button').length).toBe(1)
    expect(
      primaryArtistOne.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)

    expect(primaryArtistTwo.find('button').length).toBe(1)
    expect(
      primaryArtistTwo.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)
  })

  it('all primary artists are removable if there is more than one on a Single', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[8].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={true}
                albumType="Single"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Dana')
    const primaryArtistTwo = artistRows.find('#artist_row_Frank')

    expect(primaryArtistOne.find('button').length).toBe(1)
    expect(primaryArtistOne.find('.remove-artist-container').length).toBe(1)

    expect(primaryArtistTwo.find('button').length).toBe(1)
    expect(primaryArtistTwo.find('.remove-artist-container').length).toBe(1)
  })

  it('primary artist is not enabled if there is only one on a Single', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[0].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={false}
                albumType="Single"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Charlie')

    expect(primaryArtistOne.find('button').length).toBe(0)
    expect(
      primaryArtistOne.find('.artist-song-name_no_edit').props().disabled
    ).toBe(true)
  })

  it('primary artist is not removable if there is only one on a Single', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[0].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={false}
                albumType="Single"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Charlie')

    expect(primaryArtistOne.find('button').length).toBe(0)
    expect(primaryArtistOne.find('.remove-artist-container').length).toBe(0)
  })

  it('all primary artists are enabled if there is more than one on a Various Artists track', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[9].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={true}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Dana')
    const primaryArtistTwo = artistRows.find('#artist_row_Frank')

    expect(primaryArtistOne.find('button').length).toBe(1)
    expect(
      primaryArtistOne.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)

    expect(primaryArtistTwo.find('button').length).toBe(1)
    expect(
      primaryArtistTwo.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)
  })

  it('primary artist is not removable if there is only one on a Various Artists track', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[0].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={false}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Charlie')

    expect(primaryArtistOne.find('button').length).toBe(0)
    expect(primaryArtistOne.find('.remove-artist-container').length).toBe(0)
  })

  it('all primary artists are enabled if there is more than one on a DJ release track', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[9].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={true}
                isDJRelease={true}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Dana')
    const primaryArtistTwo = artistRows.find('#artist_row_Frank')

    expect(primaryArtistOne.find('button').length).toBe(1)
    expect(
      primaryArtistOne.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)

    expect(primaryArtistTwo.find('button').length).toBe(1)
    expect(
      primaryArtistTwo.find('.artist-name-autocomplete-input').props().disabled
    ).toBe(undefined)
  })

  it('primary artist is not removable if there is only one on a DJ release', () => {
    const store = createStore()
    const artistIds = Object.values(store.getState().songs)[0].data.artists
    const artists = artistIds.map(
      (artistId) => store.getState().creatives[artistId]
    )
    const artistRows = mount(
      <Provider store={store}>
        <div>
          {artists.map((artist, index) => {
            return (
              <ArtistRow
                artist={artist}
                onRemoveArtist={() => {}}
                onEditArtist={(attribute, value) => {
                  artist[attribute] = value
                }}
                key={index}
                index={index}
                songwriterRoleId={'1'}
                moreThanOneSongwriter={true}
                moreThanOnePrimaryArtist={false}
                isDJRelease={true}
                albumType="Album"
              />
            )
          })}
        </div>
      </Provider>
    )

    const primaryArtistOne = artistRows.find('#artist_row_Charlie')

    expect(primaryArtistOne.find('button').length).toBe(0)
    expect(primaryArtistOne.find('.remove-artist-container').length).toBe(0)
  })
})
