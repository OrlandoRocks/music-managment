import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsCompleteIndicator from '../../../songs/components/SongsCompleteIndicator'
import createStore from '../helpers/store'
import { Provider } from 'react-redux'

Enzyme.configure({ adapter: new Adapter() })

describe('SongsCompleteIndicator', () => {
  it('shows the number of completed songs', () => {
    const store = createStore()
    const songsCompleteIndicator = mount(
      <Provider store={store}>
        <SongsCompleteIndicator />
      </Provider>
    )
    let space = new RegExp(String.fromCharCode(160), 'g')
    let completedText = songsCompleteIndicator.text().trim().replace(space, ' ')
    expect(completedText).toContain('10 / 12  Completed')
  })
})
