import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SongsContainer from '../../../songs/components/SongsContainer'
import createStore from '../helpers/store'
import createFinalized from '../helpers/finalizedStore'
import songIsUploadingStore from '../helpers/songIsUploadingStore'
import songFailedUploadStore from '../helpers/songFailedUploadStore'
import { Provider } from 'react-redux'
import { REGISTER_ASSET, REMOVE_SONG } from '../../../songs/actions/actionTypes'
import {
  changeCurrentSong,
  registerAsset,
} from '../../../songs/actions/songActions'
import { albumIsNotLoading } from '../../../songs/actions/albumActions'
import guid from '../../../shared/guid'
import moment from 'moment'
import setjQueryUpdate from '../helpers/setjQueryUpdate'

Enzyme.configure({ adapter: new Adapter() })

describe('SongsContainer', () => {
  it('edits a song title', () => {
    const store = createStore()
    const { songs } = store.getState()
    let song = Object.values(songs)[0]
    setjQueryUpdate({ name: 'Fireball' }, song, store)

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const titleField = form.find('.song-title input')
    titleField.simulate('change', { target: { value: 'Fireball' } })

    form.simulate('submit')

    expect(Object.values(store.getState().songs)[0].data.name).toBe('Fireball')
  })

  it('marks a song as explicit', () => {
    const store = createStore({
      explicitFieldsEnabled: true,
      explicitRadioEnabled: true,
    })
    const song = Object.values(store.getState().songs)[1]
    setjQueryUpdate({ explicit: true }, song, store)

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const explicit = app.find('.song-explicit input').at(0)
    explicit.simulate('change', { target: { value: 'Yes' } })
    expect(store.getState().currentSong.data.explicit).toBe(true)
    form.simulate('submit')
    expect(Object.values(store.getState().songs)[1].data.explicit).toBe(true)
  })

  it('marks a song as not explicit', () => {
    const store = createStore({
      explicitFieldsEnabled: true,
      explicitRadioEnabled: true,
    })
    const song = Object.values(store.getState().songs)[1]
    setjQueryUpdate({ explicit: false }, song, store)

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const explicit = app.find('.song-explicit input').at(1)
    explicit.simulate('change', { target: { value: 'No' } })
    expect(store.getState().currentSong.data.explicit).toBe(false)
    form.simulate('submit')
    expect(Object.values(store.getState().songs)[1].data.explicit).toBe(false)
  })

  it('displays a remove artist button if there are more than one songwriters', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[2]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const buttonValue = form.find('#artist_row_Troy .remove-artist').type()
    expect(buttonValue).toBe('button')
  })

  it('does not submit the form if artist rows are incomplete', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[3]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const titleField = form.find('.song-title input')
    titleField.simulate('change', { target: { value: 'New Title' } })

    form.simulate('submit')
    expect(Object.values(store.getState().songs)[3].data.name).toBe(
      'Pretty good song'
    )
  })

  it('does not submit the form if copyrights are missing', () => {
    const store = createStore({ copyrightsFormEnabled: true })
    const song = Object.values(store.getState().songs)[3]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    const editButton = trackListItem.find('.edit-song')
    editButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const compositionCbx = form.find('#song-copyrights-composition').first()
    compositionCbx.simulate('click')

    form.simulate('submit')
    const formCheck = app.find(`form#song_form_${song.uuid}`)
    expect(formCheck).toBeFalsy
  })

  it('does submit the form if copyrights are present', () => {
    const store = createStore({ copyrightsFormEnabled: true })
    const song = Object.values(store.getState().songs)[3]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    const editButton = trackListItem.find('.edit-song')
    editButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const compositionCbx = form.find('#song-copyrights-composition').first()
    const recordingCbx = form.find('#song-copyrights-recording').first()
    compositionCbx.simulate('click')
    recordingCbx.simulate('click')

    form.simulate('submit')
    const formCheck = app.find(`form#song_form_${song.uuid}`)
    expect(formCheck).toBeTruthy
  })

  xit('submits a file', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)

    const file = {}
    const assetUrl =
      'http://tcc.tunecore.com.staging.s3.amazonaws.com/2/1515536513-2s-beats.mp3.mp3?Signature=JX4XP9d9QOG6ohEf03otvIgPi%2F0%3D&Expires=1515540116&AWSAccessKeyId=AKIAIM5RJKCF2UKVL2VA'

    window.jQuery = {
      ajax: function () {
        window.jQuery = {
          ajax: function () {
            store.dispatch({
              type: REGISTER_ASSET,
              data: {
                uuid: song.uuid,
                asset_url: assetUrl,
                filename: '1515536513-2s-beats.mp3',
              },
            })
          },
        }

        let asset = {
          key: 'key',
          uuid: guid(),
          bucket: 'tunecore.com',
          orig_filename: '1515536513-2s-beats.mp3',
          bit_rate: 320000,
        }

        store.dispatch(registerAsset(asset, song))
      },
    }

    form
      .find('.song-file-upload-input')
      .simulate('change', { target: { files: [file] } })

    expect(Object.values(store.getState().songs)[4].data.asset_url).toBe(
      assetUrl
    )
  })

  it('adds an empty artist row if the song only has one artist', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const emptyRow = form.find('#artist_row_')
    expect(emptyRow.length).toBe(1)
  })

  it("shows the 'I am done adding songs' button", () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const doneButton = form.find('.cancel-song')
    expect(doneButton.length).toBe(1)
  })

  it('removes the song from the track list', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]

    window.jQuery = {
      ajax: function () {
        store.dispatch(albumIsNotLoading())
        store.dispatch({
          type: REMOVE_SONG,
          data: {
            songUuid: song.uuid,
          },
        })
      },
    }

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const trackList = app.find('.track-list-item')
    expect(trackList.length).toBe(12)

    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    trackListItem.simulate('mouseenter')

    const removeSongButton = trackListItem.find(
      `.track-list-item-buttons-for-spatial-audio #remove_song_${song.uuid}`
    )
    removeSongButton.simulate('click')

    const updatedTrackList = app.find('.track-list-item')
    expect(updatedTrackList.length).toBe(11)
  })

  it('displays the remove song spinner when a song is removed', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[4]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const trackListItem = app.find(`#track_list_item_${song.uuid}`)
    trackListItem.simulate('mouseenter')
    expect(trackListItem.find('.is-deleting').length).toBe(0)

    const removeSongButton = trackListItem.find(
      '.track-list-item-buttons-for-spatial-audio .remove-song'
    )
    removeSongButton.simulate('click')

    const removedTrackListItem = app.find(`#track_list_item_${song.uuid}`)
    expect(removedTrackListItem.find('.is-deleting').length).toBe(1)
  })

  it('allows a user to edit a previously released at song', () => {
    const store = createStore()
    const { songs } = store.getState()
    const song = Object.values(songs)[0]
    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )
    window.moment = moment

    setjQueryUpdate(
      { is_previously_released: true, previously_released_at: '1999-02-08' },
      song,
      store
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')
    const form = app.find(`form#song_form_${song.uuid}`)
    const previouslyReleasedFieldCheckbox = form.find('#previouslyReleased')
    previouslyReleasedFieldCheckbox.simulate('change', {
      target: { checked: true },
    })

    const previouslyReleasedDate = app.find('#previouslyReleasedDate')
    previouslyReleasedDate.simulate('change', {
      target: { value: '1999-02-08' },
    })

    expect(store.getState().currentSong.data.previously_released_at).toBe(
      '1999-02-08'
    )
    form.simulate('submit')
    expect(
      Object.values(store.getState().songs)[0].data.previously_released_at
    ).toBe('1999-02-08')
  })

  context('Asset form section', () => {
    context('song does not have an asset', () => {
      xit('shows the upload button', () => {
        const store = createStore()
        const song = Object.values(store.getState().songs)[0]
        const app = mount(
          <Provider store={store}>
            <SongsContainer />
          </Provider>
        )
        const editSongButton = app.find(
          `#track_list_item_${song.uuid} .edit-song`
        )
        editSongButton.simulate('click')
        const uploadButton = app.find(`#song_upload_${song.uuid}`)
        expect(uploadButton.length).toBe(1)
      })
    })

    context('song does have an asset', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[1]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')

      xit('shows the media player', () => {
        const mediaPlayer = app.find(`#song_media_player_${song.uuid}`)
        expect(mediaPlayer.length).toBe(1)
      })

      xit('shows the replace audio file button', () => {
        const uploadButton = app.find(`#song_upload_${song.uuid}`)
        expect(uploadButton.length).toBe(1)
      })

      xit('displays the asset filename', () => {
        const fileName = app.find('.asset-filename')
        expect(fileName.text()).toBe('song_upload_file.mp3')
      })
    })

    context('upload is in progress', () => {
      const store = songIsUploadingStore()
      let { songs, creatives } = store.getState()
      const song = Object.values(songs)[5]
      creatives = song.data.artists
        .map((artistId) => creatives[artistId])
        .reduce((accumulator, artist) => {
          return {
            ...accumulator,
            [artist.uuid]: artist,
          }
        }, {})

      store.dispatch(changeCurrentSong(song.uuid))

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      let form = app.find(`form#song_form_${song.uuid}`)

      xit('shows the progress spinner', () => {
        const progressBar = form.find(`#song_progress_spinner_${song.uuid}`)
        expect(progressBar.length).toBe(1)
      })

      xit('shows the cancel button', () => {
        const cancelButton = form.find(`#song_cancel_upload_${song.uuid}`)
        expect(cancelButton.length).toBe(1)
      })
    })

    context('upload failed', () => {
      const store = songFailedUploadStore()
      const songs = store.getState().songs
      const song = Object.values(songs)[6]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const form = app.find(`form#song_form_${song.uuid}`)

      xit('shows the error progress bar', () => {
        const uploadFailedBar = form.find(`#upload_failure_bar_${song.uuid}`)
        expect(uploadFailedBar.length).toBe(1)
      })

      xit('shows the retry button', () => {
        const uploadButton = form.find(`#song_upload_${song.uuid}`)
        expect(uploadButton.length).toBe(1)
      })

      xit('shows the cancel button', () => {
        const cancelButton = form.find(`#song_cancel_upload_${song.uuid}`)
        expect(cancelButton.length).toBe(1)
      })
    })
  })

  context('deleting a song', () => {
    it('removes the song from state', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[4]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      window.jQuery = {
        ajax: function () {
          store.dispatch({
            type: REMOVE_SONG,
            data: {
              songUuid: song.uuid,
            },
          })
        },
      }
      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')
      const deleteButton = app
        .find(`#remove_song_${song.uuid}`)
        .find('.form-remove-song')
        .at(0)
      deleteButton.simulate('click')
      expect(store.getState().songs[song.uuid]).toBe(undefined)
    })
  })

  context('loading state', () => {
    it('disables the delete and submit buttons', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')

      const form = app.find(`form#song_form_${song.uuid}`)
      form.simulate('submit')
      const deleteButton = app
        .find(`#remove_song_${song.uuid}`)
        .find('.form-remove-song')
        .at(0)
      const submitButton = app.find('.submit-song')

      expect(deleteButton.props().disabled).toBe(true)
      expect(submitButton.props().disabled).toBe(true)
    })
  })

  it('does not disable the cancel edit button while editing', () => {
    const store = createStore()
    const song = Object.values(store.getState().songs)[0]

    const app = mount(
      <Provider store={store}>
        <SongsContainer />
      </Provider>
    )

    const editSongButton = app.find(`#track_list_item_${song.uuid} .edit-song`)
    editSongButton.simulate('click')

    const form = app.find(`form#song_form_${song.uuid}`)
    form.simulate('submit')

    const cancelButton = app.find('.cancel-song')
    const { disabled } = cancelButton.props()
    expect(disabled).toBeFalsy()
  })

  context('finalized album', () => {
    it('does not render the songsHeader', () => {
      const store = createFinalized()
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const songsHeader = app.find(`.songs_top-bar`)

      expect(songsHeader.length).toBe(0)
    })
  })

  context('when a user clicks on the "Is Instrumental" checkbox', () => {
    it('sets the instrumental attribute to true', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]
      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')

      const isInstrumentalCheckbox = app.find('#is-instrumental')
      isInstrumentalCheckbox.simulate('change', { target: { checked: true } })

      const currentSong = store.getState().currentSong.data
      expect(currentSong.instrumental).toBeTruthy()
    })

    it('removes the lyrics section', () => {
      const store = createStore()
      const song = Object.values(store.getState().songs)[0]

      const app = mount(
        <Provider store={store}>
          <SongsContainer />
        </Provider>
      )

      const editSongButton = app.find(
        `#track_list_item_${song.uuid} .edit-song`
      )
      editSongButton.simulate('click')

      const songLyrics = app.find('.song-lyrics')
      expect(songLyrics.length).toBe(1)

      const isInstrumentalCheckbox = app.find('#is-instrumental')
      isInstrumentalCheckbox.simulate('change', { target: { checked: true } })

      const emptySongLyrics = app.find('.song-lyrics')
      expect(emptySongLyrics.length).toBe(0)
    })
  })
})
