import guid from '../../../../shared/guid'
import { normalizeSongs } from '../../../../songs/store/schema'
import songwriterRoleId from './songwriterRoleId'
import buildSongFromApi from '../../../../songs/utils/buildSongDataFromApi'

let variousArtistSongs = [
  {
    data: {
      id: 7,
      name: 'My Fireball',
      language_code_id: 1,
      translated_name: null,
      version: null,
      cover_song: true,
      made_popular_by: null,
      explicit: true,
      optional_isrc: null,
      lyrics: null,
      asset_url: '',
      album_id: 6,
      instrumental: false,
      artists: [],
    },
    errors: {},
    uuid: guid(),
  },
]

let {
  entities: { creatives, songs, songwriters },
} = normalizeSongs(
  variousArtistSongs.map((song) => {
    return buildSongFromApi(song, songwriterRoleId)
  })
)

export { songs, creatives, songwriters }
