export default {
  isLoading: false,
  album_type: 'Single',
  previously_released_at: null,
  finalized: false,
}
