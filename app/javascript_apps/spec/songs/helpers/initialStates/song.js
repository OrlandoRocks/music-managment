import guid from '../../../../shared/guid'
import { normalizeSongs } from '../../../../songs/store/schema'
import songwriterRoleId from './songwriterRoleId'
import buildSongDataFromApi from '../../../../songs/utils/buildSongDataFromApi'

let songData = [
  {
    data: {
      id: 1,
      name: 'My Fireball',
      language_code_id: 1,
      localized_name: null,
      version: null,
      cover_song: true,
      made_popular_by: null,
      explicit: true,
      optional_isrc: 'TC1234567891',
      lyrics: null,
      asset_url: 'TBD',
      album_id: 6,
      instrumental: false,
      artists: [
        {
          creative_id: 1,
          artist_name: 'Charlie',
          associated_to: 'Album',
          credit: 'primary_artist',
          role_ids: ['2'],
          uuid: guid(),
        },
      ],
      copyrights: {
        composition: true,
        recording: true,
      },
    },
    errors: {},
    uuid: guid(),
  },
]

let {
  entities: { creatives, songs, songwriters },
} = normalizeSongs(
  songData.map((song) => buildSongDataFromApi(song, songwriterRoleId))
)

export { songs, creatives, songwriters }
