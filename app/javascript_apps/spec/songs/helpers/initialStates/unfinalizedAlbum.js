export default {
  genre: 'Instrumental',
  isLoading: false,
  album_type: 'Album',
  finalized: false,
  previously_released_at: null,
  is_editable: false,
}
