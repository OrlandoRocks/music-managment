import { songs } from './songs'
import guid from '../../../../shared/guid'
import defaultSong from '../../../../songs/store/initialStates/defaultSong'

const currentSong =
  Object.keys(songs).length === 0 ? { ...defaultSong, uuid: guid() } : null

export default currentSong
