export default {
  genre: 'Instrumental',
  isLoading: false,
  album_type: 'Album',
  finalized: false,
  is_various: true,
}
