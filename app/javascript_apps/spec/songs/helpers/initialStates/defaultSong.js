import guid from '../../../../shared/guid'

export default {
  uuid: guid(),
  data: {
    id: null,
    name: '',
    language_code_id: null,
    translated_name: null,
    version: null,
    cover_song: false,
    made_popular_by: null,
    explicit: false,
    optional_isrc: null,
    lyrics: null,
    asset: 'TBD',
    album_id: 6,
    artists: [
      {
        artist_name: 'Charlie',
        associated_to: 'Album',
        credit: 'primary_artist',
        role_ids: [],
        uuid: guid(),
      },
    ],
  },
  errors: {},
}
