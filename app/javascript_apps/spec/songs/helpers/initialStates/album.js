export default {
  genre: 'Pop',
  isLoading: false,
  album_type: 'Album',
  finalized: false,
  previously_released_at: null,
  is_editable: true,
  track_limit_reached: false,
}
