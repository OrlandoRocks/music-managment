import defaultSong from './initialStates/defaultSong'
import { creatives, songs, songwriters } from './initialStates/songs'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import finalAlbum from './initialStates/finalizedAlbum'

const initialState = {
  songs,
  creatives,
  songwriters,
  artists,
  languages,
  currentSong,
  songRoles,
  defaultSong,
  tcPid,
  album: finalAlbum,
  bigBoxUrl,
}

export default initialState
