import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../songs/reducers'
import initialState from './currentSongInitialState'
import songValidationMiddleware from '../../../songs/middleware/songValidationMiddleware'
import apiMiddleware from '../../../songs/middleware/apiMiddleware'
import uploadSongMiddleware from '../../../songs/middleware/uploadSongMiddleware'
import songLookupMiddleware from '../../../songs/middleware/songLookupMiddleware'
import defaultSongInjectionMiddleware from '../../../songs/middleware/defaultSongInjectionMiddleware'
import copyFromPreviousMiddleware from '../../../songs/middleware/copyFromPreviousMiddleware'
import saveSongSuccessMiddleware from '../../../songs/middleware/saveSongSuccessMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      songValidationMiddleware,
      saveSongSuccessMiddleware,
      uploadSongMiddleware,
      apiMiddleware,
      songLookupMiddleware,
      defaultSongInjectionMiddleware,
      copyFromPreviousMiddleware
    )
  )
})
