import defaultSong from './initialStates/defaultSong'
import { creatives, songs, songwriters } from './initialStates/song'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import single from './initialStates/single'
import songwriterRoleId from './initialStates/songwriterRoleId'

const initialState = {
  songs,
  creatives,
  songwriters,
  artists,
  languages,
  currentSong,
  songRoles,
  defaultSong,
  tcPid,
  bigBoxUrl,
  songwriterRoleId,
  album: single,
}

export default initialState
