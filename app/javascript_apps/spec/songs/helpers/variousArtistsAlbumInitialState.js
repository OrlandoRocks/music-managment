import defaultSong from './initialStates/defaultSong'
import { songs, creatives } from './initialStates/variousArtistsSongs'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import album from './initialStates/variousArtistsAlbum'

const initialState = {
  songs,
  creatives,
  artists,
  languages,
  currentSong,
  songRoles,
  defaultSong,
  tcPid,
  album,
  bigBoxUrl,
}

export default initialState
