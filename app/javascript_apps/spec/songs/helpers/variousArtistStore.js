import guid from '../../../shared/guid'
import songwriterRoleId from './initialStates/songwriterRoleId'

export default function variousArtistStore() {
  const artist = {
    associated_to: 'Album',
    credit: 'primary_artist',
    artist_name: 'Rick',
    uuid: guid(),
    role_ids: [songwriterRoleId],
  }
  return {
    album: { is_various: true },
    currentSong: {
      data: {
        artists: [artist.uuid],
      },
    },
    currentCreatives: {
      [artist.uuid]: artist,
    },
    currentSongwriters: {
      [artist.uuid]: {
        uuid: artist.uuid,
        name: artist.artist_name,
        index: 0,
      },
    },
  }
}
