import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../songs/reducers'
import state from './initialState'
import songValidationMiddleware from '../../../songs/middleware/songValidationMiddleware'
import apiMiddleware from '../../../songs/middleware/apiMiddleware'
import uploadSongMiddleware from '../../../songs/middleware/uploadSongMiddleware'
import songStyleGuideMiddleware from '../../../songs/middleware/songStyleGuideMiddleware'
import artistStyleGuideMiddleware from '../../../songs/middleware/artistStyleGuideMiddleware'
import defaultSongInjectionMiddleware from '../../../songs/middleware/defaultSongInjectionMiddleware'
import songLookupMiddleware from '../../../songs/middleware/songLookupMiddleware'
import copyFromPreviousMiddleware from '../../../songs/middleware/copyFromPreviousMiddleware'
import saveSongSuccessMiddleware from '../../../songs/middleware/saveSongSuccessMiddleware'
export default (function configureStore(options = {}) {
  let initialState = state

  if (options.copyrightsFormEnabled) {
    initialState.copyrightsFormEnabled = true
  }

  if (options.explicitFieldsEnabled) {
    initialState.explicitFieldsEnabled = true
  }

  if (options.explicitRadioEnabled) {
    initialState.explicitRadioEnabled = true
  }

  if (options.songStartTimesFormEnabled) {
    initialState.songStartTimesFormEnabled = true
  }

  if (options.spatialAudioEnabled) {
    initialState.spatialAudioEnabled = true
  }

  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      songLookupMiddleware,
      saveSongSuccessMiddleware,
      songValidationMiddleware,
      songStyleGuideMiddleware,
      artistStyleGuideMiddleware,
      uploadSongMiddleware,
      apiMiddleware,
      copyFromPreviousMiddleware,
      defaultSongInjectionMiddleware
    )
  )
})
