import defaultSong from './initialStates/defaultSong'
import { songs, creatives, songwriters } from './initialStates/songs'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import songwriterRoleId from './initialStates/songwriterRoleId'
import album from './initialStates/album'

const initialState = {
  songs,
  creatives,
  songwriters,
  artists,
  languages,
  currentSong,
  songRoles,
  defaultSong,
  songwriterRoleId,
  tcPid,
  album,
  bigBoxUrl,
}

export default initialState
