import defaultSong from './initialStates/defaultSong'
import { songs, creatives, songwriters } from './initialStates/songs'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import {
  song as currentSong,
  creatives as currentCreatives,
  songwriters as currentSongwriters,
} from './initialStates/activeCurrentSong'
import album from './initialStates/album'

const initialState = {
  songs,
  creatives,
  songwriters,
  artists,
  languages,
  currentSong,
  currentCreatives,
  currentSongwriters,
  songRoles,
  defaultSong,
  tcPid,
  album,
  bigBoxUrl,
}

export default initialState
