import { UPDATE_SONG } from '../../../songs/actions/actionTypes'
import buildSongData from '../../../songs/utils/buildSongData'

export default function setjQueryUpdate(newData, song, store) {
  const { creatives, songwriters, songwriterRoleId } = store.getState()
  window.jQuery = {
    ajax: function () {
      let updatedSong = {
        ...song,
        data: {
          ...song.data,
          ...newData,
        },
      }
      let songData = buildSongData(
        updatedSong,
        creatives,
        songwriters,
        songwriterRoleId
      )
      store.dispatch({
        type: UPDATE_SONG,
        data: {
          uuid: song.uuid,
          ...songData,
        },
      })
    },
  }
}
