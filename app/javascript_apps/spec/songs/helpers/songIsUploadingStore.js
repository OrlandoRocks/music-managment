import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../../../songs/reducers'
import initialState from './initialState'
import songValidationMiddleware from '../../../songs/middleware/songValidationMiddleware'
import apiMiddleware from '../../../songs/middleware/apiMiddleware'
import uploadSongMiddleware from '../../../songs/middleware/uploadSongMiddleware'
import defaultSongInjectionMiddleware from '../../../songs/middleware/defaultSongInjectionMiddleware'
import songLookupMiddleware from '../../../songs/middleware/songLookupMiddleware'
import copyFromPreviousMiddleware from '../../../songs/middleware/copyFromPreviousMiddleware'
import saveSongSuccessMiddleware from '../../../songs/middleware/saveSongSuccessMiddleware'

export default (function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      songValidationMiddleware,
      saveSongSuccessMiddleware,
      copyFromPreviousMiddleware,
      uploadSongMiddleware,
      apiMiddleware,
      songLookupMiddleware,
      defaultSongInjectionMiddleware
    )
  )
})
