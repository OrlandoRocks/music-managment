import defaultSong from './initialStates/defaultSong'
import { creatives, songs, songwriters } from './initialStates/songs'
import artists from './initialStates/artists'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import unfinalAlbum from './initialStates/unfinalizedAlbum'

const initialState = {
  songs,
  creatives,
  songwriters,
  artists,
  languages,
  currentSong,
  songRoles,
  defaultSong,
  tcPid,
  album: unfinalAlbum,
  bigBoxUrl,
}

export default initialState
