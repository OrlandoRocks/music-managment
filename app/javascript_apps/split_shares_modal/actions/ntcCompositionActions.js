import { UPDATE_NTC_COMPOSITION_ERRORS } from './actionTypes'

export function updateNtcCompositionErrors(attribute, value) {
  return {
    type: UPDATE_NTC_COMPOSITION_ERRORS,
    attribute,
    value,
  }
}
