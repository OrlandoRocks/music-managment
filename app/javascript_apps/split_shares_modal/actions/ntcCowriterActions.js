import { UPDATE_NTC_COWRITER_ERRORS } from './actionTypes'

export function updateNtcCowriterErrors(errors, ntcCowriterIndex) {
  return {
    type: UPDATE_NTC_COWRITER_ERRORS,
    errors,
    ntcCowriterIndex,
  }
}
