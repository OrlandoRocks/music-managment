import { API, CREATE_SPLITS_SUCCESS } from './actionTypes'

export function createSplits(composition, composers, cowriters) {
  return {
    type: API,
    payload: {
      url:
        '/api/backstage/publishing_administration/multi_tenant_publishing_splits',
      method: 'POST',
      params: {
        composition_id: composition.id,
        composer_params: composers,
        cowriter_params: cowriters,
        translated_name: composition.translated_name,
      },
      success: CREATE_SPLITS_SUCCESS,
    },
  }
}
