import {
  ADD_COWRITER,
  UPDATE_COWRITER,
  UPDATE_COWRITER_ERRORS,
  REMOVE_COWRITER,
  RESET_COMPOSITION,
} from './actionTypes'
import { createCowriter } from '../utils/createCompositionData'

export function addCowriter(compositionId) {
  return {
    type: ADD_COWRITER,
    cowriter: createCowriter(),
    compositionId,
  }
}

export function updateCowriter(attribute, value, uuid, compositionId) {
  return {
    type: UPDATE_COWRITER,
    attribute,
    value,
    uuid,
    compositionId,
  }
}

export function removeCowriter(compositionId, uuid) {
  return {
    type: REMOVE_COWRITER,
    compositionId,
    uuid,
  }
}

export function resetComposition(composition, composers, possibleComposers) {
  return {
    type: RESET_COMPOSITION,
    composition,
    composers,
    possibleComposers,
  }
}

export function updateCowriterErrors(uuid, errors) {
  return {
    type: UPDATE_COWRITER_ERRORS,
    uuid,
    errors,
  }
}
