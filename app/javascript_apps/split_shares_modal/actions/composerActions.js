import { ADD_COMPOSER, UPDATE_COMPOSER, REMOVE_COMPOSER } from './actionTypes'
import { createComposer } from '../utils/createCompositionData'

export function addComposer(compositionId, possibleComposers, composers) {
  const composerIds = composers.map((composer) => composer.id)
  const id = possibleComposers.filter(
    (possibleComposer) =>
      !composerIds.includes(parseInt(possibleComposer.id, 10))
  )[0].id

  return {
    type: ADD_COMPOSER,
    composer: createComposer(id),
    compositionId,
  }
}

export function updateComposer(attribute, value, uuid, compositionId) {
  return {
    type: UPDATE_COMPOSER,
    attribute,
    value,
    uuid,
    compositionId,
  }
}

export function removeComposer(compositionId, uuid) {
  return {
    type: REMOVE_COMPOSER,
    compositionId,
    uuid,
  }
}
