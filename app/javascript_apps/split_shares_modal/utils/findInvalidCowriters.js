import nonEmptyCowriters from './nonEmptyCowriters'
import validateCowriter from './validateCowriter'

export default function findInvalidCowriters(cowriters) {
  let validatedCowriters = nonEmptyCowriters(cowriters).map((cowriter) =>
    validateCowriter(cowriter)
  )

  return validatedCowriters.filter((cowriter) => {
    return (
      !cowriter.is_unknown &&
      Object.values(cowriter.errors).some((value) => value === true)
    )
  })
}
