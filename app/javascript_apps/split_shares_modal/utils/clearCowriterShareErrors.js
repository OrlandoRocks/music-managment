import { updateCowriterErrors } from '../actions/cowriterActions'

export default function clearCowriterShareErrors(dispatch, cowriters, uuid) {
  let cowritersWithCowriterShareErrors = Object.values(cowriters).filter(
    (cowriter) => {
      return cowriter.errors.cowriter_share && uuid !== cowriter.uuid
    }
  )

  cowritersWithCowriterShareErrors.map((c) => {
    dispatch(
      updateCowriterErrors(c.uuid, { ...c.errors, cowriter_share: false })
    )
  })
}
