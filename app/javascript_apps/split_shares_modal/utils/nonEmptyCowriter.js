export default function nonEmptyCowriter(cowriter) {
  const requiredCowriterFields = ['first_name', 'last_name', 'cowriter_share']
  return requiredCowriterFields.some((field) => cowriter[field] !== '')
}
