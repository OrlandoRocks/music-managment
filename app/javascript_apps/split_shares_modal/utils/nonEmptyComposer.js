export default function nonEmptyComposer(composer) {
  const requiredComposerFields = ['composer_share']
  return requiredComposerFields.some((field) => composer[field] !== '')
}
