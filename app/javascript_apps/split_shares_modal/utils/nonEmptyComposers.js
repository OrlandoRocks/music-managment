import nonEmptyComposer from './nonEmptyComposer'

export default function nonEmptyComposers(composers) {
  return composers.filter((composer) => {
    return nonEmptyComposer(composer)
  })
}
