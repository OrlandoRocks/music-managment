import uuid from 'uuid/v4'

export function createNtcComposition() {
  return {
    uuid: uuid(),
    title: '',
    performingArtist: '',
    albumName: '',
    isrcNumber: '',
    releaseDate: '',
    percent: '',
    cowriters: [],
    errors: {},
  }
}

export function createCowriter() {
  return {
    first_name: '',
    last_name: '',
    cowriter_share: '',
    errors: {},
    uuid: uuid(),
  }
}

export function createComposer(id) {
  return {
    id: id,
    composer_share: '',
    errors: {},
    uuid: uuid(),
  }
}
