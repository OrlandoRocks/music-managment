import { API } from '../actions/actionTypes'
import { deepTrimObj } from '../../shared/deepTrimObj'

const apiMiddleware = ({ dispatch }) => (next) => (action) => {
  if (action.type !== API) {
    return next(action)
  }

  jQuery.ajax({
    url: action.payload.url,
    type: action.payload.method,
    data: JSON.stringify(deepTrimObj(action.payload.params || {})),
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      dispatch({
        type: action.payload.success,
        data: data,
      })
    },
    error: function (data) {
      dispatch({
        type: action.payload.error,
        data: data.responseJSON,
      })
    },
  })
}

export default apiMiddleware
