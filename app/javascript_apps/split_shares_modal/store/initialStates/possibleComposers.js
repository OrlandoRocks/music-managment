import { normalizeComposers } from '../schema'

let dbComposers = JSON.parse(
  document.getElementById('split_shares_modal_app').dataset.composers
)

const possibleComposers = normalizeComposers(dbComposers).entities.composers

export default possibleComposers
