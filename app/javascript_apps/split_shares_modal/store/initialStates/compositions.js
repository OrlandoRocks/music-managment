import { normalizeCompositions } from '../schema'

let dbCompositions = JSON.parse(
  document.getElementById('split_shares_modal_app').dataset.compositions
)

const {
  entities: { compositions },
} = normalizeCompositions(dbCompositions)

export default compositions
