import { normalizeCowriters } from '../schema'

let dbCowriters = JSON.parse(
  document.getElementById('split_shares_modal_app').dataset.cowriters
)

const {
  entities: { cowriters },
} = normalizeCowriters(dbCowriters)

export default cowriters
