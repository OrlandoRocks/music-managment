import { normalize, schema } from 'normalizr'

const composition = new schema.Entity('compositions')
const cowriters = new schema.Entity('cowriters')
const composers = new schema.Entity('composers')

export const normalizeCompositions = (originalData) =>
  normalize(originalData, [composition])
export const normalizeCowriters = (originalData) =>
  normalize(originalData, [cowriters])
export const normalizeComposers = (originalData) =>
  normalize(originalData, [composers])
