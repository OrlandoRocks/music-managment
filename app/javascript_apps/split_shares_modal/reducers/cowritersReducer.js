import {
  ADD_COWRITER,
  REMOVE_COWRITER,
  RESET_COMPOSITION,
  CREATE_SPLITS_SUCCESS,
  UPDATE_COWRITER,
  UPDATE_COWRITER_ERRORS,
} from '../actions/actionTypes'

function buildNewCowriters(cowriters) {
  return cowriters.reduce((acc, cowriter) => {
    cowriter.errors = {}
    return {
      ...acc,
      [cowriter.id]: cowriter,
    }
  }, {})
}

export default function cowritersReducer(state = {}, action) {
  /* eslint-disable no-case-declarations */
  switch (action.type) {
    case ADD_COWRITER:
      return {
        ...state,
        [action.cowriter.uuid]: action.cowriter,
      }
    case REMOVE_COWRITER:
      const { [action.uuid]: _omit, ...rest } = state
      return rest
    case UPDATE_COWRITER_ERRORS:
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          errors: action.errors,
        },
      }
    case RESET_COMPOSITION:
      const idsToRemove = action.composition.cowriters
      return Object.values(state)
        .filter((cowriter) => !idsToRemove.includes(cowriter.uuid))
        .reduce((accumulator, val) => {
          return {
            ...accumulator,
            [val.uuid]: val,
          }
        }, {})
    case CREATE_SPLITS_SUCCESS:
      return {
        ...state,
        ...buildNewCowriters(action.data.cowriters),
      }
    case UPDATE_COWRITER:
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          [action.attribute]: action.value,
        },
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
