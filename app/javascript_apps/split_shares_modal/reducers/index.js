import { combineReducers } from 'redux'
import composer from './composerReducer'
import possibleComposers from './possibleComposersReducer'
import composers from './composersReducer'
import compositions from './compositionsReducer'
import cowriters from './cowritersReducer'
import currentComposer from './currentComposerReducer'
import currentComposition from './currentCompositionReducer'
import ntcComposition from './ntcCompositionReducer'
import pros from './proReducer'

const rootReducer = combineReducers({
  currentComposition,
  composer,
  possibleComposers,
  composers,
  compositions,
  cowriters,
  currentComposer,
  ntcComposition,
  pros,
})

export default rootReducer
