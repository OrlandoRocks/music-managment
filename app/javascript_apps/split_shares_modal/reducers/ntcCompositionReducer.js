import {
  UPDATE_NTC_COMPOSITION_ERRORS,
  UPDATE_NTC_COWRITER_ERRORS,
} from '../actions/actionTypes'

export default function ntcComposition(state = {}, action) {
  switch (action.type) {
    case UPDATE_NTC_COMPOSITION_ERRORS:
      return {
        ...state,
        errors: {
          ...state.errors,
          [action.attribute]: action.value,
        },
      }
    case UPDATE_NTC_COWRITER_ERRORS:
      return {
        ...state,
        cowriters: state.cowriters.map((cowriter, cowriterIndex) => {
          if (cowriterIndex === action.ntcCowriterIndex) {
            return { ...cowriter, errors: action.errors }
          } else {
            return cowriter
          }
        }),
      }
    default:
      return state
  }
}
