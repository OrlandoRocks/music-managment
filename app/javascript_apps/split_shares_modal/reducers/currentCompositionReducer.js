import { NEXT_COMPOSITION } from '../actions/actionTypes'
import findNextEligibleComposition from '../utils/findNextEligibleComposition'

export default function currentCompositionsReducer(state = {}, action) {
  let currentComposition

  switch (action.type) {
    case NEXT_COMPOSITION:
      currentComposition = findNextEligibleComposition(
        action.compositions,
        action.composition
      )

      action.currentCompositionId = currentComposition.compositionId

      return { ...state, ...currentComposition }
    default:
      return state
  }
}
