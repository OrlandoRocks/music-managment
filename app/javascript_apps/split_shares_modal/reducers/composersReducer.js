import {
  ADD_COMPOSER,
  CREATE_SPLITS_SUCCESS,
  UPDATE_COMPOSER,
  RESET_COMPOSITION,
  REMOVE_COMPOSER,
  NEXT_COMPOSITION,
} from '../actions/actionTypes'

import { createComposer } from '../utils/createCompositionData'

function buildNewComposers(composers) {
  return composers.reduce((acc, composer) => {
    composer.errors = {}
    return {
      ...acc,
      [composer.id]: composer,
    }
  }, {})
}

export default function composersReducer(state = {}, action) {
  let firstAvaibleComposer
  let composer

  /* eslint-disable no-case-declarations */
  switch (action.type) {
    case ADD_COMPOSER:
      return {
        ...state,
        [action.composer.uuid]: action.composer,
      }
    case NEXT_COMPOSITION:
      firstAvaibleComposer = Object.values(action.possibleComposers)[0]
      composer = createComposer(firstAvaibleComposer.id)

      action.composerUuid = composer.uuid

      return {
        [composer.uuid]: composer,
      }
    case REMOVE_COMPOSER:
      const { [action.uuid]: _omit, ...rest } = state
      return rest
    case RESET_COMPOSITION:
      const idsToRemove = action.composition.composers || []

      const leftOverComposers = Object.values(state)
        .filter((composer) => !idsToRemove.includes(composer.uuid))
        .reduce((accumulator, val) => {
          return {
            ...accumulator,
            [val.uuid]: val,
          }
        }, {})

      firstAvaibleComposer = Object.values(action.possibleComposers)[0]
      composer = createComposer(firstAvaibleComposer.id)

      action.composerUuid = composer.uuid

      return {
        ...leftOverComposers,
        [composer.uuid]: composer,
      }
    case CREATE_SPLITS_SUCCESS:
      return {
        ...state,
        ...buildNewComposers(action.data.composers),
      }
    case UPDATE_COMPOSER:
      return {
        ...state,
        [action.uuid]: {
          ...state[action.uuid],
          [action.attribute]: action.value,
        },
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
