import {
  ADD_COWRITER,
  ADD_COMPOSER,
  REMOVE_COMPOSER,
  REMOVE_COWRITER,
  RESET_COMPOSITION,
  CREATE_SPLITS_SUCCESS,
  NEXT_COMPOSITION,
  UPDATE_COMPOSITION,
  UPDATE_COMPOSITION_ERRORS,
  UPDATE_COMPOSITION_SUCCESS,
  UPDATE_TRANSLATED_NAME,
} from '../actions/actionTypes'

export default function compositionsReducer(state = {}, action) {
  /* eslint-disable no-case-declarations */
  switch (action.type) {
    case ADD_COWRITER:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          cowriters: [
            ...state[action.compositionId].cowriters,
            action.cowriter.uuid,
          ],
        },
      }
    case ADD_COMPOSER:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          composers: [
            ...state[action.compositionId].composers,
            action.composer.uuid,
          ],
        },
      }
    case NEXT_COMPOSITION:
      return {
        ...state,
        [action.currentCompositionId]: {
          ...state[action.currentCompositionId],
          composers: [action.composerUuid],
          cowriters: [],
          composer_share: '0.0',
          translated_name: '',
          errors: {},
        },
      }
    case REMOVE_COWRITER:
      let cowriters = state[action.compositionId].cowriters.filter(
        (cowriter) => cowriter !== action.uuid
      )

      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          cowriters,
        },
      }
    case REMOVE_COMPOSER:
      let composers = state[action.compositionId].composers.filter(
        (composers) => composers !== action.uuid
      )

      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          composers,
        },
      }
    case RESET_COMPOSITION:
      return {
        ...state,
        [action.composition.id]: {
          ...state[action.composition.id],
          composers: [action.composerUuid],
          cowriters: [],
          composer_share: '0.0',
          translated_name: '',
          errors: {},
        },
      }
    case CREATE_SPLITS_SUCCESS:
      return {
        ...state,
        [action.data.composition_id]: {
          ...state[action.data.composition_id],
          composer_share: action.data.composer_percent,
          cowriter_share: action.data.cowriter_percent,
          cowriters: action.data.cowriters.map((c) => c.id),
          composers: action.data.composers.map((c) => c.id),
          translated_name: action.data.translated_name,
          submitted_at: action.data.submitted_at,
          status: 'submitted',
          unknown_split_percent: action.data.unknown_split_percent,
        },
      }
    case UPDATE_COMPOSITION:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          [action.attribute]: action.value,
        },
      }
    case UPDATE_COMPOSITION_ERRORS:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          errors: { ...state[action.compositionId].errors, ...action.errors },
        },
      }
    case UPDATE_COMPOSITION_SUCCESS:
      return Object.keys(state)
        .filter((id) => id !== action.data.id.toString())
        .reduce((accumulator, key) => {
          accumulator[key] = state[key]
          return accumulator
        }, {})
    case UPDATE_TRANSLATED_NAME:
      return {
        ...state,
        [action.compositionId]: {
          ...state[action.compositionId],
          [action.attribute]: action.value,
        },
      }
    default:
      return state
  }
  /* eslint-enable no-case-declarations */
}
