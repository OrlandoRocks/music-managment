import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import SplitSharesModalApp from './components/SplitSharesModalApp'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <SplitSharesModalApp />
  </Provider>,
  document.getElementById('split_shares_modal_app')
)
