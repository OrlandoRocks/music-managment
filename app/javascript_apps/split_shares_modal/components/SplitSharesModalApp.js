import React from 'react'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import SplitSharesModal from './SplitSharesModal/SplitSharesModal'
import { resetComposition } from '../actions/cowriterActions'

class SplitSharesModalApp extends React.Component {
  state = {
    addSharesModalIsOpen: false,
  }

  closeModal = ({ target: { id } }) => {
    const modalId = id.includes('edit') ? 'edit' : 'addShares'

    this.setState({
      [`${modalId}ModalIsOpen`]: false,
    })
  }

  openModal = ({ target: { id } }) => {
    const modalId = id.includes('edit') ? 'edit' : 'addShares'

    this.setState({
      [`${modalId}ModalIsOpen`]: true,
    })
  }

  numOfSharesMissing = () => {
    const compositions = this.props.compositions

    return Object.values(compositions).filter(
      (composition) => composition.status === 'shares_missing'
    ).length
  }

  resetCurrentComposition = () => {
    this.props.dispatch(
      resetComposition(
        this.props.composition,
        this.props.composers,
        this.props.possibleComposers
      )
    )
    this.openModal({ target: { id: 'addShares' } })
  }

  sharesMissingLinkOrText = () => {
    const { composer } = this.props
    const numOfSharesMissing = this.numOfSharesMissing()
    const shouldNotBeLink =
      numOfSharesMissing === 0 || composer.status === 'terminated'
    const translations =
      window.translations.compositions.compositions_card.shares_missing
    const sharesMissingText = translations.replace(
      '{{numSharesMissing}}',
      numOfSharesMissing
    )

    if (shouldNotBeLink) {
      return sharesMissingText
    }

    return (
      <a
        id="addSharesLink"
        onClick={this.resetCurrentComposition}
        className="publishing-dashboard-card-tax-info-link"
      >
        {sharesMissingText}
      </a>
    )
  }

  render() {
    const { addSharesModalIsOpen } = this.state

    return (
      <div>
        <div className="publishing-dashboard-card-right-col-react small-12 publishing-dashboard-card-right-col-react align-right div-shares-missing">
          {this.sharesMissingLinkOrText()}
        </div>
        <Modal
          className="publishing-splits-modal"
          isOpen={addSharesModalIsOpen}
          onRequestClose={this.closeModal}
          overlayClassName="publishing-compositions-add-shares-modal-overlay"
          ariaHideApp={false}
        >
          <SplitSharesModal closeModal={this.closeModal} />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const {
    compositions,
    composer,
    composers,
    currentComposer,
    currentComposition,
    cowriters,
    possibleComposers,
  } = storeState
  const composition = compositions[currentComposition.compositionId] || {}

  return {
    composer,
    possibleComposers,
    composers,
    composition,
    compositions,
    currentComposer,
    cowriters,
  }
}

export default connect(mapStateToProps)(SplitSharesModalApp)
