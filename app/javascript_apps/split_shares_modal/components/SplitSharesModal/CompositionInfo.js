import React from 'react'
import replace from 'react-string-replace'
import Composers from './Composers.js'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

function yourWriterSharetoolTipUrl() {
  return replace(
    window.translations.compositions.tooltips.your_writer_share_content,
    '{{HERE}}',
    (match, i) => (
      <a
        href={
          window.translations[
            'What-are-splits-and-how-do-I-figure-out-my-split-'
          ]
        }
        key={i}
      >
        {window.translations.compositions.tooltips.here}
      </a>
    )
  )
}

const yourShareToolTip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.compositions.tooltips.your_writer_share_header}
    </TooltipTitle>
    <TooltipColumn>{yourWriterSharetoolTipUrl()}</TooltipColumn>
  </TooltipContainer>
)

const CompositionInfo = ({
  composition,
  handleComposerShareChange,
  composers,
  possibleComposers,
  translations,
  addComposer,
  compositionId,
}) => {
  const possibleComposersArray = Object.values(possibleComposers)
  const allComposersAdded = composers.length === possibleComposersArray.length

  return (
    <table className="publishing-splits-composition-info">
      <thead className="publishing-splits-composition-info-headers">
        <tr className="grid-x">
          <th className="cell small-3">{translations.composition_title}</th>
          <th className="cell small-3">{translations.appears_on}</th>
          <th className="cell small-3">
            {translations.primary_writers || 'Primary Writer(s)'}
          </th>
          <th className="cell small-2">
            <div>
              {translations.your_writer_share}&nbsp;
              <Tooltip>{yourShareToolTip}</Tooltip>
            </div>
          </th>
          <th className="cell small-1"></th>
        </tr>
      </thead>
      <Composers
        handleComposerShareChange={handleComposerShareChange}
        possibleComposers={possibleComposersArray}
        composition={composition}
        composers={composers}
        compositionId={compositionId}
      />
      <div className="grid-x">
        <div className="cell small-8"></div>
        <div className="publishing-splits-cowriters-link align-right cell small-4">
          <button
            className="publishing-splits-cowriters-add-cowriter-btn publishing-splits-composers-add-composer-btn"
            onClick={addComposer}
            disabled={allComposersAdded}
          >
            <b>
              <i className="fa fa-plus" />{' '}
              {translations.list_additional_composers ||
                'List Additional Writers'}
            </b>
          </button>
        </div>
      </div>
    </table>
  )
}

export default CompositionInfo
