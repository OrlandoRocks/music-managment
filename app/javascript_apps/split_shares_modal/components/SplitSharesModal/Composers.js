import React from 'react'
import PropTypes from 'prop-types'
import ComposerRow from './ComposerRow.js'

class Composers extends React.Component {
  static propTypes = {
    handleComposerShareChange: PropTypes.func.isRequired,
    possibleComposers: PropTypes.array.isRequired,
    composition: PropTypes.object.isRequired,
    composers: PropTypes.array.isRequired,
    compositionId: PropTypes.number,
  }

  renderComposerRows({ composers, possibleComposers }) {
    return composers.map((composer) => {
      const isFirstComposer = composers.indexOf(composer) === 0

      const otherComposerIds = composers.map((otherComposer) =>
        otherComposer.id == composer.id ? '' : parseInt(otherComposer.id, 10)
      )

      const filteredPossibleComposers = possibleComposers.filter(
        (possibleComposer) =>
          !otherComposerIds.includes(parseInt(possibleComposer.id, 10))
      )

      return (
        <ComposerRow
          key={composer.uuid}
          isFirstComposer={isFirstComposer}
          composer={composer}
          possibleComposers={filteredPossibleComposers}
          composition={this.props.composition}
          handleComposerShareChange={this.props.handleComposerShareChange}
          compositionId={this.props.compositionId}
        />
      )
    })
  }

  render() {
    const { possibleComposers, composers } = this.props

    return (
      <tbody>{this.renderComposerRows({ composers, possibleComposers })}</tbody>
    )
  }
}

export default Composers
