import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addCowriter } from '../../actions/cowriterActions'
import { addComposer } from '../../actions/composerActions'
import { createSplits } from '../../actions/publishingSplitActions'
import CompositionInfo from './CompositionInfo'
import Cowriters from './Cowriters'
import Footer from './Footer/Footer'
import Header from './Header'
import ListCowritersButton from './ListCowritersButton'
import TranslatedName from './TranslatedName'
import sum from '../../../shared/sum'
import nonEmptyCowriters from '../../utils/nonEmptyCowriters'
import nonEmptyComposers from '../../utils/nonEmptyComposers'
import {
  nextComposition,
  submitCompositionState,
  updateComposition,
  updateTranslatedName,
} from '../../actions/compositionActions'
import { containsNonLatinCharacters } from '../../../shared/fieldValidations'

class SplitSharesModal extends React.Component {
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
  }

  addCowriter = () => {
    this.props.dispatch(addCowriter(this.props.composition.id))
  }

  addComposer = () => {
    const possibleComposers = Object.values(this.props.possibleComposers)

    this.props.dispatch(
      addComposer(
        this.props.composition.id,
        possibleComposers,
        this.props.composers
      )
    )
  }

  btnDisabled() {
    const { composer_share, errors } = this.props.composition
    const compShare = isNaN(composer_share) ? 0 : composer_share
    const hasErrors = errors && Object.values(errors).some((error) => error)
    let totalPercentage =
      compShare + sum(this.props.cowriters, 'cowriter_share')

    return (
      hasErrors ||
      compShare < 1 ||
      totalPercentage < 1 ||
      totalPercentage > 100 ||
      this.isTranslationUnavailable()
    )
  }

  componentDidUpdate(prevProps) {
    let prevComposition = prevProps.composition
    let currentComposition = this.props.composition
    let isSameComposition = prevComposition.id === currentComposition.id

    let lastComposition =
      prevComposition.status === 'submitted' && isSameComposition

    let validComposition =
      prevComposition.status === 'shares_missing' &&
      currentComposition.status === 'submitted' &&
      isSameComposition

    if (validComposition) {
      this.nextEligibleComposition()
    } else if (
      lastComposition ||
      !Object.prototype.hasOwnProperty.call(currentComposition, 'id')
    ) {
      this.props.closeModal({ target: { id: 'split_submit' } })
    }
  }

  isTranslationUnavailable = () => {
    const { composition_title, translated_name } = this.props.composition
    const translationPresent = !!translated_name && !!translated_name.trim()

    return containsNonLatinCharacters(composition_title) && !translationPresent
  }

  compositionNum = () => {
    const { compositions, composition } = this.props
    return Object.values(compositions).indexOf(composition) + 1
  }

  handleComposerShareChange = ({ target: { name, value } }) => {
    const { id } = this.props.composition
    this.props.dispatch(updateComposition(id, name, parseFloat(value)))
  }

  handleTranslatedNameChange = ({ target: { name, value } }) => {
    const { id } = this.props.composition
    this.props.dispatch(updateTranslatedName(id, name, value))
  }

  handleHideComposition = () => {
    this.props.dispatch(submitCompositionState(this.props.composition))
    this.nextEligibleComposition()
  }

  handleOnSubmit = () => {
    this.props.dispatch(
      createSplits(
        this.props.composition,
        nonEmptyComposers(this.props.composers),
        nonEmptyCowriters(this.props.cowriters)
      )
    )
  }

  handleSkipComposition = () => {
    this.nextEligibleComposition()
  }

  nextEligibleComposition = () => {
    const { compositions, composition } = this.props

    const possibleComposers = Object.values(this.props.possibleComposers)

    this.props.dispatch(
      nextComposition(compositions, composition, possibleComposers)
    )
  }

  resetPercentage = () => {
    const { id } = this.props.composition
    this.props.dispatch(updateComposition(id, 'composer_share', null))
  }

  render() {
    const {
      composition,
      composers,
      cowriters,
      compositions,
      possibleCowriters,
      possibleComposers,
      closeModal,
    } = this.props
    const btnDisabled = this.btnDisabled()
    const hasTotalShareError = composition?.errors?.total_shares === 'percent'
    const compositionNum = this.compositionNum()
    const compositionsCount = Object.keys(compositions).length
    const hasCowriters = cowriters.length > 0
    const translations = window.translations.compositions.splits_modal

    return (
      <div className="publishing-splits">
        <Header
          index={compositionNum}
          translations={translations}
          compositionsCount={compositionsCount}
          closeModal={closeModal}
        />
        <CompositionInfo
          composition={composition}
          translations={translations}
          handleComposerShareChange={this.handleComposerShareChange}
          composerShare={parseFloat(composition.composer_share)}
          composers={composers}
          possibleComposers={possibleComposers}
          addComposer={this.addComposer}
          compositionId={composition.id}
        />
        <div className="grid-x">
          <TranslatedName
            composition={composition}
            translations={translations}
            handleTranslatedNameChange={this.handleTranslatedNameChange}
          />
          <ListCowritersButton
            addCowriter={this.addCowriter}
            compositionId={composition.id}
            btnDisabled={btnDisabled && false}
            showBtn={cowriters.length === 0}
            listCowritersText={translations.list_cowriters}
          />
        </div>
        {hasCowriters && (
          <Cowriters
            addCowriter={this.addCowriter}
            cowriters={cowriters}
            compositionId={composition.id}
            btnDisabled={btnDisabled}
            possibleCowriters={possibleCowriters}
            hasTotalShareError={hasTotalShareError}
          />
        )}
        <Footer
          btnDisabled={btnDisabled}
          resetPercentage={this.resetPercentage}
          composerShare={parseFloat(composition.composer_share)}
          translations={translations}
          handleHideComposition={this.handleHideComposition}
          handleSkipComposition={this.handleSkipComposition}
          handleOnSubmit={this.handleOnSubmit}
        />
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  let { compositions, currentComposition } = storeState
  let composition = compositions[currentComposition.compositionId] || {}
  let cowriters = []
  let composers = []
  let possibleCowriters = storeState.cowriters
  let possibleComposers = storeState.possibleComposers

  if (Object.prototype.hasOwnProperty.call(composition, 'cowriters')) {
    cowriters = composition.cowriters.map(
      (cowriterId) => storeState.cowriters[cowriterId]
    )
  }

  if (Object.prototype.hasOwnProperty.call(composition, 'composers')) {
    composers = composition.composers.map(
      (composerId) => storeState.composers[composerId]
    )
  }

  return {
    composer: storeState.composer,
    composition: composition,
    compositions: compositions,
    cowriters: cowriters,
    composers: composers,
    possibleCowriters: possibleCowriters,
    possibleComposers: possibleComposers,
  }
}

export default connect(mapStateToProps)(SplitSharesModal)
