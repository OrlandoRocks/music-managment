import React from 'react'
import PropTypes from 'prop-types'
import CowriterRow from './CowriterRow.js'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const translations = window.translations.compositions.splits_modal

const ListCowritersTooltip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.compositions.tooltips.list_cowriters_header}
    </TooltipTitle>
    <TooltipColumn>
      {window.translations.compositions.tooltips.list_cowriters_content}
    </TooltipColumn>
  </TooltipContainer>
)

class Cowriters extends React.Component {
  static propTypes = {
    addCowriter: PropTypes.func.isRequired,
    cowriters: PropTypes.array.isRequired,
    compositionId: PropTypes.number,
    btnDisabled: PropTypes.bool,
  }

  renderCowriterRows() {
    return this.props.cowriters.map((cowriter) => {
      return (
        <CowriterRow
          key={cowriter.id || cowriter.uuid}
          cowriter={cowriter}
          possibleCowriters={this.props.possibleCowriters}
          compositionId={this.props.compositionId}
        />
      )
    })
  }

  render() {
    const { addCowriter, hasTotalShareError } = this.props

    return (
      <div className="publishing-splits-cowriters">
        <div className={hasTotalShareError ? 'error-container' : ''}>
          <div className="publishing-splits-cowriters-header">
            <b>{translations.list_cowriters}</b>&nbsp;
            <Tooltip>{ListCowritersTooltip}</Tooltip>
            {hasTotalShareError && (
              <span className="error-text">
                {window.translations.compositions.validations.total_percent}
              </span>
            )}
          </div>
        </div>
        <p className="publishing-splits-cowriters-text">
          {translations.cowriters_paragraph}
        </p>
        {this.renderCowriterRows()}

        <button
          className="publishing-splits-cowriters-add-cowriter-btn"
          onClick={addCowriter}
        >
          <b>
            <i className="fa fa-plus" />{' '}
            {translations.list_additional_cowriters}
          </b>
        </button>
      </div>
    )
  }
}

export default Cowriters
