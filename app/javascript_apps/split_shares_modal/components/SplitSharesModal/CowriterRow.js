import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { updateCowriter, removeCowriter } from '../../actions/cowriterActions'
import AutocompleteField from '../../../shared/AutocompleteField'

const translations = window.translations.compositions.splits_modal

class CowriterRow extends React.Component {
  static propTypes = {
    cowriter: PropTypes.object.isRequired,
    compositionId: PropTypes.number,
  }

  handleChange = ({ target: { name, value } }) => {
    this.props.dispatch(
      updateCowriter(
        name,
        value,
        this.props.cowriter.uuid,
        this.props.compositionId
      )
    )
  }

  handleRemoveCowriter = () => {
    this.props.dispatch(
      removeCowriter(this.props.compositionId, this.props.cowriter.uuid)
    )
  }

  render() {
    const { possibleCowriters, cowriter } = this.props
    const { first_name, last_name, cowriter_share, errors } = cowriter

    const possibleFirstNames = possibleCowriters
      ? [
          ...new Set(
            Object.values(possibleCowriters).map(
              (possibleCowriter) => possibleCowriter.first_name
            )
          ),
        ]
      : []
    const possibleLastNames = possibleCowriters
      ? [
          ...new Set(
            Object.values(possibleCowriters).map(
              (possibleCowriter) => possibleCowriter.last_name
            )
          ),
        ]
      : []

    return (
      <div className="publishing-splits-cowriters-row grid-x grid-padding-x">
        <div className="cell small-4">
          <p>
            <b>{translations.first_name}*</b>
          </p>
          <div className={errors.first_name ? 'error-container' : ''}>
            <AutocompleteField
              type="text"
              name="first_name"
              className="publishing-splits-cowriters-first-name"
              value={first_name}
              onChange={this.handleChange}
              items={possibleFirstNames}
            />
            {errors.first_name && (
              <span className="error-text">
                {window.translations.compositions.validations.first_name}
              </span>
            )}
          </div>
        </div>

        <div className="cell small-4">
          <p>
            <b>{translations.last_name}*</b>
          </p>
          <div className={errors.last_name ? 'error-container' : ''}>
            <AutocompleteField
              type="text"
              name="last_name"
              className="publishing-splits-cowriters-last-name"
              value={last_name}
              onChange={this.handleChange}
              items={possibleLastNames}
            />
            {errors.last_name && (
              <span className="error-text">
                {window.translations.compositions.validations.last_name}
              </span>
            )}
          </div>
        </div>

        <div className="cell small-3">
          <p>
            <b>{translations.cowriter_share}*</b>
          </p>
          <div className={errors.cowriter_share ? 'error-container' : ''}>
            <input
              name="cowriter_share"
              type="number"
              className="publishing-splits-cowriters-share-input"
              placeholder={translations.cowriter_placeholder}
              onChange={this.handleChange}
              value={cowriter_share}
            />
            {errors.cowriter_share && (
              <span className="error-text">
                {window.translations.compositions.validations.cowriter_share}
              </span>
            )}
          </div>
        </div>

        <div className="publishing-splits-cowriters-row-cancel-btn cell small-1">
          <a onClick={this.handleRemoveCowriter}>
            <i className="fa fa-times" />
          </a>
        </div>
      </div>
    )
  }
}

export default connect()(CowriterRow)
