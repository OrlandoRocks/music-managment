import React from 'react'

const ListCowritersButton = ({
  addCowriter,
  btnDisabled,
  showBtn,
  listCowritersText,
}) => {
  const hideListCowriterBtn = () => {
    addCowriter()
  }

  return (
    <div className="publishing-splits-cowriters-link align-right cell small-4">
      {showBtn && (
        <button
          id="add-cowriter-btn"
          className="publishing-splits-cowriters-link-btn"
          onClick={hideListCowriterBtn}
          disabled={btnDisabled}
        >
          <i className="fa fa-plus"></i>&#160;
          <b>{listCowritersText}</b>
        </button>
      )}
    </div>
  )
}

export default ListCowritersButton
