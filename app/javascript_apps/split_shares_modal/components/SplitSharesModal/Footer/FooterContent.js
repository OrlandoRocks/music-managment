import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import replace from 'react-string-replace'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../../shared/Tooltip'

function cannotClaimSharesToolTipUrl() {
  const {
    cannot_claim_shares_content,
    here,
  } = window.translations.compositions.tooltips
  const helpLink =
    window.translations[
      'Can-I-submit-a-work-that-contains-a-sample-or-interpolation-of-a-copyrighted-composition-'
    ]

  return replace(cannot_claim_shares_content, '{{HERE}}', (match, i) => (
    <a href={helpLink} key={i}>
      {here}
    </a>
  ))
}

const CannotClaimShareTooltip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.compositions.tooltips.cannot_claim_shares_header}
    </TooltipTitle>
    <TooltipColumn>{cannotClaimSharesToolTipUrl()}</TooltipColumn>
  </TooltipContainer>
)

class FooterContent extends React.Component {
  static propTypes = {
    btnDisabled: PropTypes.bool,
    handleOnSubmit: PropTypes.func.isRequired,
    toggleHideContent: PropTypes.func.isRequired,
    toggleSkipContent: PropTypes.func.isRequired,
    translations: PropTypes.object,
  }

  numOfSharesMissing = () => {
    const compositions = this.props.compositions
    return Object.values(compositions).filter(
      (composition) => composition.status === 'shares_missing'
    ).length
  }

  render() {
    const {
      handleOnSubmit,
      toggleHideContent,
      toggleSkipContent,
      translations,
    } = this.props
    const shouldShowSkipBtn = this.numOfSharesMissing() > 1

    return (
      <div className="grid-x">
        <div className="publishing-splits-footer-cannot-claim cell small-6">
          <b>
            <a id="hide_composition_link" onClick={toggleHideContent}>
              {translations.do_not_register} &nbsp;
            </a>
            <Tooltip>{CannotClaimShareTooltip}</Tooltip>
          </b>
        </div>

        <div className="publishing-splits-footer-btn-wrapper cell small-6 align-right">
          <b className="publishing-splits-footer-btn-wrapper-done">
            {shouldShowSkipBtn && (
              <a id="skip_composition_link" onClick={toggleSkipContent}>
                {translations.skip}
              </a>
            )}
          </b>

          <button
            className="publishing-splits-footer-btn-wrapper-submit"
            id="register_and_continue"
            onClick={handleOnSubmit}
          >
            {translations.register_and_continue}
          </button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const { compositions } = storeState
  return { compositions }
}

export default connect(mapStateToProps)(FooterContent)
