import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { updateComposer, removeComposer } from '../../actions/composerActions'

class ComposerRow extends React.Component {
  static propTypes = {
    composer: PropTypes.object.isRequired,
  }

  handleChange = ({ target: { name, value } }) => {
    this.props.dispatch(
      updateComposer(
        name,
        value,
        this.props.composer.uuid,
        this.props.compositionId
      )
    )
  }

  handleRemoveComposer = () => {
    this.props.dispatch(
      removeComposer(this.props.compositionId, this.props.composer.uuid)
    )
  }

  possibleComposerOptions = () => {
    return this.props.possibleComposers.map((composer) => (
      <option key={composer.id} value={composer.id}>
        {composer.full_name}
      </option>
    ))
  }

  render() {
    const { composer, composition, isFirstComposer } = this.props
    const { composer_share } = composer

    const showError = false

    const composerOptions = this.possibleComposerOptions()

    return (
      <tr className="grid-x">
        {(isFirstComposer && (
          <td className="cell small-3 comp-title">
            {composition.composition_title}
          </td>
        )) || <td className="cell small-3 comp-title"></td>}
        {(isFirstComposer && (
          <td className="cell small-3">{composition.appears_on}</td>
        )) || <td className="cell small-3"></td>}
        <td className="cell small-3">
          <select name="id" onChange={this.handleChange}>
            {composerOptions}
          </select>
        </td>
        <td className={`cell small-2 ${showError ? 'error-container' : ''}`}>
          <input
            id="add_shares_percent_input"
            className="publishing-splits-composition-info-input"
            type="number"
            min="1"
            max="100"
            name="composer_share"
            placeholder="1-100 %"
            step="any"
            value={composer_share > 0 ? composer_share : ''}
            onChange={this.handleChange}
          />

          {showError && (
            <span className="error-text">
              {window.translations.compositions.validations.composer_share}
            </span>
          )}
        </td>
        {(isFirstComposer && <td className="cell small-1"></td>) || (
          <td className="cell small-1">
            <a onClick={this.handleRemoveComposer}>
              <i className="fa fa-times" />
            </a>
          </td>
        )}
      </tr>
    )
  }
}

export default connect()(ComposerRow)
