import React from 'react'

const Header = ({ index, compositionsCount, closeModal, translations }) => {
  const title = translations.shares
    .replace('{{index}}', index)
    .replace('{{total_compositions}}', compositionsCount)

  return (
    <div className="publishing-splits-header grid-x">
      <div className="publishing-splits-header cell small-11">
        <b>{title}</b>
      </div>
      <div
        className="publishing-splits-header-close-btn cell small-1"
        onClick={closeModal}
      >
        <i id="cancel-btn" className="fa fa-times" />
      </div>
    </div>
  )
}

export default Header
