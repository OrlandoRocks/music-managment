import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import Tooltip from 'rc-tooltip'

function renderTooltips() {
  const vatTooltipContainers = document.getElementsByClassName('vat_tooltip')

  for (const container of vatTooltipContainers) {
    let { title = '', content = '', placement = 'right' } = container.dataset

    ReactDOM.render(
      <Tooltip
        placement={placement}
        prefixCls="vat-tooltip"
        overlay={
          <div className="vat-tooltip-body">
            <div className="vat-tooltip-title">{title}</div>
            <div className="vat-tooltip-content">{content}</div>
          </div>
        }
        arrowContent={<div className="arrow"></div>}
      >
        <span className="tooltip-info-icon"></span>
      </Tooltip>,
      container
    )
  }
}

if (
  document.readyState === 'complete' ||
  document.readyState === 'interactive'
) {
  setTimeout(renderTooltips, 1)
} else {
  document.addEventListener('DOMContentLoaded', renderTooltips)
}
