import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import SelectorContainer from './components/SelectorContainer'

const multipleNodes = document.getElementsByClassName(
  'foundation_header_lang_selector'
)
const desktopNode = document.getElementById('language_selector_app')
const mobileNode = document.getElementById('language_selector_app_mobile')

if (multipleNodes) {
  Array.from(multipleNodes).map((node) => {
    ReactDOM.render(<SelectorContainer />, node)
  })
}

if (desktopNode) {
  ReactDOM.render(<SelectorContainer />, desktopNode)
}

if (mobileNode) {
  ReactDOM.render(<SelectorContainer />, mobileNode)
}
