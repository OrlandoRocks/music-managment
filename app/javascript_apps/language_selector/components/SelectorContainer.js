import React from 'react'
import SelectorMenu from './SelectorMenu'

class SelectorContainer extends React.Component {
  constructor(props) {
    super(props)

    const languageOptions = JSON.parse(
      document.getElementById('language_selector_app').dataset
        .languageSelectorOptions
    )
    const currentLocaleLang = document.getElementById('language_selector_app')
      .dataset.currentLocaleLanguage
    const chooseALang = document.getElementById('language_selector_app').dataset
      .chooseALanguage

    this.state = {
      languageOptions,
      currentLocaleLang,
      chooseALang,
    }
  }

  render() {
    return (
      <SelectorMenu
        languageOptions={this.state.languageOptions}
        currentLocaleLang={this.state.currentLocaleLang}
        chooseALang={this.state.chooseALang}
      />
    )
  }
}

export default SelectorContainer
