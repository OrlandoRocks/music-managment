import React from 'react'
import Modal from 'react-modal'
import LanguageOptions from './LanguageOptions'

const SELECTOR_MODAL_DOM_ELEMENT = 'body'
Modal.setAppElement(SELECTOR_MODAL_DOM_ELEMENT)

class SelectorMenu extends React.Component {
  constructor(props) {
    super(props)

    this.wrapperRef = React.createRef()

    this.state = {
      showModal: false,
    }
  }

  hideLangOptions = () => {
    this.setState({
      showModal: false,
    })
  }

  showLangOptions = () => {
    this.setState({
      showModal: true,
    })
  }

  selectorToggleClassName() {
    let className = 'fa fa-lg '

    if (this.state.showModal) {
      return className + 'fa-caret-up'
    } else {
      return className + 'fa-caret-down'
    }
  }

  render() {
    return (
      <div id="language_selector" ref={this.wrapperRef}>
        <div className="language_display" onClick={this.showLangOptions}>
          <p className="language_display_text">
            {this.props.currentLocaleLang}
          </p>
          <i
            className={
              'language_display_caret ' + this.selectorToggleClassName()
            }
          ></i>
        </div>

        <Modal
          overlayClassName="language_modal_overlay"
          className="language_options_container"
          shouldCloseOnOverlayClick={false}
          isOpen={this.state.showModal}
        >
          <LanguageOptions
            languageOptions={this.props.languageOptions}
            currentLocaleLang={this.props.currentLocaleLang}
            hideLangOptions={this.hideLangOptions}
            chooseALang={this.props.chooseALang}
          />
        </Modal>
      </div>
    )
  }
}

export default SelectorMenu
