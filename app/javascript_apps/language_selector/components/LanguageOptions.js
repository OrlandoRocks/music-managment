import axios from 'axios'
import React from 'react'
import queryString from 'query-string'

const csrfToken = document.querySelector('[name=csrf-token]').content
const headers = {
  'X-CSRF-TOKEN': csrfToken,
}

class LanguageOptions extends React.Component {
  updatePreference(locale) {
    const params = {
      language: {
        locale,
      },
    }

    return axios.put('/api/backstage/people/preference', params, { headers })
  }

  handleSelect = ({ target: { value } }) => {
    let parsedQuery = queryString.parse(location.search)
    parsedQuery.locale = value

    this.updatePreference(value)

    location.search = queryString.stringify(parsedQuery)
  }

  renderOptionButtons = () => {
    return this.props.languageOptions.map((option, i) => {
      const cssClass =
        this.props.currentLocaleLang === option.selector_language
          ? 'selected_button'
          : 'unselected_button'

      return (
        <button
          key={i}
          onClick={this.handleSelect}
          value={option.yml_languages}
          className={`language_option_button ${cssClass}`}
        >
          {option.selector_language}
        </button>
      )
    })
  }

  render() {
    const { chooseALang, hideLangOptions } = this.props

    return (
      <div>
        <div className="top_section">
          <div className="choose_a_language">{chooseALang}</div>
          <span onClick={hideLangOptions}>×</span>
        </div>
        <div className="language_options">{this.renderOptionButtons()}</div>
      </div>
    )
  }
}

export default LanguageOptions
