import React, { useCallback, useState } from 'react'
import { styled } from '@material-ui/core/styles'
import { Badge } from '@material-ui/core'
import PropTypes from 'prop-types'

import SvgNotificationIcon from '../assets/svg/notificationIcon'
import SvgCartIcon from '../assets/cartIcon'
import SvgLanguageSelectorIcon from '../assets/svg/languageSelectorIcon'
import LanguageSelectorModal from './LanguageSelectorModal'
import MobileOnlyHamburgerIcon from './MobileOnlyHamburgerIcon'
import { IconAnchor } from './IconAnchor'
import AccountMenu from './AccountMenu'

import { ITEMS, propFactory } from '../propFactory'

export default function Icons({ navData, closeMobileMenu, toggleMobileMenu }) {
  const { current_lang: currentLang, translations } = navData

  const [showLanguageModal, setShowLanguageModal] = useState(false)

  const closeLanguageModal = useCallback(() => setShowLanguageModal(false), [
    setShowLanguageModal,
  ])

  function handleLanguageSelectorClick() {
    closeMobileMenu()
    setShowLanguageModal(true)
  }

  const languageSelectorOptions = JSON.parse(navData.language_selector_options)

  const iconLanguageText = navData.current_locale_lang_abbrv?.toLocaleUpperCase()

  const modalHeader = navData.translations.select_a_language

  const { cart, notifications } = ITEMS

  const getProps = propFactory(navData)

  return (
    <>
      <IconsContainer>
        <StyledBadge
          color="error"
          badgeContent={navData.unseen_notifications_count}
          overlap="circle"
        >
          <IconAnchor {...getProps(notifications)}>
            <SvgNotificationIcon />
          </IconAnchor>
        </StyledBadge>
        <StyledBadge
          color="error"
          badgeContent={navData.cart_items_count}
          overlap="circle"
        >
          <IconAnchor {...getProps(cart)}>
            <SvgCartIcon />
          </IconAnchor>
        </StyledBadge>
        <AccountMenu navData={navData} />
        {languageSelectorOptions && (
          <IconAnchor
            aria-label={translations.select_a_language}
            onClick={handleLanguageSelectorClick}
            className="language"
          >
            <SvgLanguageSelectorIcon />
            <span>{iconLanguageText}</span>
          </IconAnchor>
        )}
        <MobileOnlyHamburgerIcon
          ariaLabel={translations.mobile_menu_aria_label}
          onClick={toggleMobileMenu}
        />
      </IconsContainer>
      {languageSelectorOptions && (
        <LanguageSelectorModal
          closeLanguageModal={closeLanguageModal}
          currentLang={currentLang}
          header={modalHeader}
          isOpen={showLanguageModal}
          languageOptions={languageSelectorOptions}
        />
      )}
    </>
  )
}

Icons.propTypes = {
  closeMobileMenu: PropTypes.func,
  navData: PropTypes.object,
  toggleMobileMenu: PropTypes.func,
}

const IconsContainer = styled('div')({
  margin: '0 13px',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',

  '& button': {
    width: 'fit-content',
  },
})

const StyledBadge = styled(Badge)({
  height: '40px',
})
