import React, { useCallback, useEffect, useState } from 'react'
import { styled } from '@material-ui/core/styles'

import AddReleaseModal from './AddReleaseModal'
import SvgTCWhiteLogo from '../assets/svg/tcWhiteLogo'
import Icons from './Icons'
import MobileMenu from './MobileMenu'
import DesktopMenu from './DesktopMenu'

import navPropTypes from '../navPropTypes'

const NavigationBar = ({ navData }) => {
  const [showReleaseModal, setShowReleaseModal] = useState(false)
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false)

  const closeReleaseModal = useCallback(() => {
    setShowReleaseModal(false)
  }, [setShowReleaseModal])
  const openReleaseModal = useCallback(() => setShowReleaseModal(true), [
    setShowReleaseModal,
  ])

  useAddReleaseModalOnElement('#get-started-btn', openReleaseModal)
  useAddReleaseModalOnElement(
    '[data-role=add-release-trigger]',
    openReleaseModal
  )

  const toggleMobileMenu = () => setMobileMenuOpen(!mobileMenuOpen)
  const closeMobileMenu = useCallback(() => {
    setMobileMenuOpen(false)
  }, [])

  const { urls } = navData

  return (
    <>
      <NavigationContainer>
        <Left>
          <a href={urls.dashboard}>
            <SvgTCWhiteLogo />
          </a>
        </Left>
        <Right>
          <DesktopMenu navData={navData} openReleaseModal={openReleaseModal} />
          <Icons
            closeMobileMenu={closeMobileMenu}
            navData={navData}
            toggleMobileMenu={toggleMobileMenu}
          />
        </Right>
      </NavigationContainer>
      <MobileMenu
        isOpen={mobileMenuOpen}
        navData={navData}
        openReleaseModal={openReleaseModal}
      />
      <AddReleaseModal
        closeReleaseModal={closeReleaseModal}
        isOpen={showReleaseModal}
        navData={navData}
      />
    </>
  )
}

NavigationBar.propTypes = {
  navData: navPropTypes.navData,
}

export default NavigationBar

const useAddReleaseModalOnElement = (selector, openReleaseModal) => {
  const handleClick = useCallback(
    (e) => {
      e.preventDefault()
      openReleaseModal()
    },
    [openReleaseModal]
  )

  useEffect(() => {
    if (window.location.pathname != '/dashboard') return

    const elements = document.querySelectorAll(selector)
    if (!elements || elements.length < 1) return

    elements.forEach((ele) => ele.addEventListener('click', handleClick))

    return () => {
      elements.forEach((ele) => ele.removeEventListener('click', handleClick))
    }
  }, [selector, handleClick])
}

// Refer to this PR for an easy way to convert this object syntax for CSS into a pure CSS when migrating to MUI 5 styled components
// https://github.com/tunecore/tc-web-react/pull/63/files#diff-7816d0a3fc571ad8df869668ce73d0a49b1958bf5df678f98422576dd76c57bbR15
const NavigationContainer = styled('div')({
  boxSizing: 'border-box',
  position: 'fixed',
  height: '80px',
  width: '100%',
  background: 'black',
  left: 0,
  top: 0,
  zIndex: 97,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '0 20px',
  fontFamily: 'Roboto, sans-serif',
  columnGap: '10px',

  '.visible-sm, .visible-sm-flex': {
    display: 'none',
  },

  '.visible-md': {
    display: 'none',
  },

  '@media screen and (max-width: 768px)': {
    padding: '0 10px',
    '.hidden-sm': {
      display: 'none',
    },

    '.visible-sm': {
      display: 'block',
    },

    '.visible-sm-flex': {
      display: 'flex',
    },
  },

  '@media screen and (max-width: 1200px)': {
    height: '60px',
    '.hidden-md': {
      display: 'none',
    },

    '.visible-md': {
      display: 'block',
    },
  },
})

const containerStyles = {
  display: 'flex',
  height: '100%',
  alignItems: 'center',
}

const Left = styled('div')(containerStyles)

const Right = styled('div')(containerStyles)
