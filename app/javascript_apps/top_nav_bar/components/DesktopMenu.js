import React from 'react'
import { Button } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import { Dropdown, DropdownItem, DropdownItemSubOption } from './Dropdown'
import MenuItem from './MenuItem'

import navPropTypes from '../navPropTypes'
import { ITEMS, propFactory } from '../propFactory'

const INDIA_COUNTRY_CODE = 'IN'

export default function DesktopMenu({ navData, openReleaseModal }) {
  const { translations } = navData

  const {
    artistServices,
    createdSplits,
    discography,
    distributionCredits,
    facebookDashboard,
    moneyAnalytics,
    publishing,
    releases,
    salesReport,
    socialMonetization,
    splitsInvites,
    storeManager,
    transactions,
    trends,
    youtubeMonetizations,
  } = ITEMS

  const getProps = propFactory(navData)

  return (
    <MenuItemsContainer>
      <MenuItem {...getProps(releases)}>
        <Dropdown className="dropdown">
          <DropdownItem {...getProps(discography)} />
          <DropdownItem {...getProps(storeManager)} />
          <DropdownItem {...getProps(socialMonetization)}>
            <DropdownItemSubOption {...getProps(youtubeMonetizations)} />
            {navData.show_fb_monetization && (
              <DropdownItemSubOption {...getProps(facebookDashboard)} />
            )}
          </DropdownItem>
          {navData.show.distribution_credits && (
            <DropdownItem {...getProps(distributionCredits)} />
          )}
        </Dropdown>
      </MenuItem>
      <MenuItem {...getProps(moneyAnalytics)}>
        <Dropdown className="dropdown">
          <DropdownItem {...getProps(transactions)} />
          <DropdownItem {...getProps(salesReport)} />
          <DropdownItem {...getProps(trends)} />
          <DropdownItem {...getProps(createdSplits)} />
          <DropdownItem {...getProps(splitsInvites)} />
        </Dropdown>
      </MenuItem>
      {navData.country_website !== INDIA_COUNTRY_CODE && (
        <MenuItem {...getProps(publishing)} />
      )}
      <MenuItem {...getProps(artistServices)} />
      <div>
        <AddReleaseButtonContainer size="small" onClick={openReleaseModal}>
          {translations.add_release}
        </AddReleaseButtonContainer>
      </div>
    </MenuItemsContainer>
  )
}

DesktopMenu.propTypes = {
  navData: navPropTypes.navData,
  openReleaseModal: PropTypes.func,
}

const MenuItemsContainer = styled('div')({
  alignItems: 'center',
  display: 'flex',

  '@media screen and (max-width: 1200px)': {
    display: 'none',
  },
})

const AddReleaseButtonContainer = styled(Button)(({ theme }) => ({
  backgroundImage: `linear-gradient(to bottom right, ${theme.palette.primary.main}, ${theme.palette.secondary.main})`,
  whiteSpace: 'nowrap',
  width: '100%',
}))
