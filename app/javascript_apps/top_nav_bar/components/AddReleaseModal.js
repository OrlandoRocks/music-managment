import React from 'react'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Modal from './Modal'
import PerReleasePricingInfo from './PerReleasePricingInfo'

const LinkWrapper = styled('div')({
  display: 'flex',
  gap: '20px',
  justifyContent: 'center',
  marginTop: '30px',

  '@media screen and (max-width: 1200px)': {
    flexDirection: 'column',
  },
})

const Link = styled('a')(({ theme }) => ({
  border: '3px solid rgba(255, 255, 255, 0.2)',
  borderRadius: '10px',
  cursor: 'pointer',
  flexGrow: 1,
  fontSize: '13px',
  fontWeight: 700,
  padding: '20px',
  textAlign: 'center',
  textDecoration: 'none',
  textTransform: 'uppercase',

  '&&': {
    // Hack to override master.css
    color: 'white',
  },
  '&&:hover': {
    // Hack to override master.css
    color: 'white',
  },

  '&:hover': {
    borderColor: theme.palette.primary.main,
    boxShadow: `${theme.palette.primary.main} 0px 0px 20px`,
  },
}))

export default function AddReleaseModal({
  closeReleaseModal,
  isOpen,
  navData,
}) {
  const {
    translations,
    urls: { newAlbum, newRingtone, newSingle },
  } = navData

  return (
    <Modal
      closeModal={closeReleaseModal}
      header={translations.what_release}
      isOpen={isOpen}
    >
      <LinkWrapper>
        <Link href={newSingle}>{translations.single}</Link>
        <Link href={newAlbum}>{translations.album}</Link>
        <Link href={newRingtone}>{translations.ringtone}</Link>
      </LinkWrapper>

      <PerReleasePricingInfo navData={navData} translations={translations} />
    </Modal>
  )
}

AddReleaseModal.propTypes = {
  closeReleaseModal: PropTypes.func,
  isOpen: PropTypes.bool,
  navData: PropTypes.object,
}
