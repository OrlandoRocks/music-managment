import React, { useCallback, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { styled } from '@material-ui/core/styles'

import { useOnClickOutside } from '../hooks/useOnClickOutside'

const MenuItem = ({
  CustomComponent,
  ccAriaLabel,
  children,
  href,
  name,
  showCaret,
}) => {
  const [open, setOpen] = useState(false)

  const toggleOpen = useCallback(() => {
    setOpen((open) => !open)
  }, [setOpen])

  const ref = useRef()
  useOnClickOutside(ref, () => {
    open && setOpen(false)
  })

  return (
    <StyledMenuItem ref={ref} onClick={toggleOpen}>
      {CustomComponent ? (
        <CustomComponent aria-label={ccAriaLabel} />
      ) : (
        <MenuItemLabel href={href}>
          {name}
          {showCaret && <Caret open={open} />}
        </MenuItemLabel>
      )}
      {open && children}
    </StyledMenuItem>
  )
}

MenuItem.propTypes = {
  CustomComponent: PropTypes.func,
  ccAriaLabel: PropTypes.string,
  name: PropTypes.string,
  children: PropTypes.node,
  href: PropTypes.string,
  showCaret: PropTypes.bool,
}

const StyledMenuItem = styled('button')({
  alignItems: 'center',
  background: 'transparent',
  border: 'none',
  color: 'white',
  display: 'flex',
  flexDirection: 'column',
  fontSize: '11px',
  fontWeight: '400',
  letterSpacing: '2px',
  minHeight: '80px',
  padding: '0 15px',
  position: 'relative',
  textAlign: 'left',
  textTransform: 'uppercase',
  whiteSpace: 'nowrap',
  width: '100%',
  zIndex: 1,

  '&:has(.icon)': {
    padding: 0,
    minHeight: '80px',
  },

  '&:hover': {
    cursor: 'pointer',
  },

  '@media screen and ( max-width: 1200px )': {
    fontSize: '14px',
    minHeight: '0',
  },
})

const MenuItemLabel = (props) => {
  const tag = props.href ? 'a' : 'div'
  const Component = styled(tag)(({ theme }) => ({
    // Hack to increase specificity to override master.css
    // https://stackoverflow.com/a/64486501
    '&&': {
      color: 'white',
    },
    '&&:hover': {
      color: theme.palette.primary.main,
    },

    display: 'flex',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '80px',
    textDecoration: 'none',

    '@media screen and ( max-width: 1200px )': {
      minHeight: '50px',
      fontSize: '14px',
    },
  }))

  return <Component {...props} />
}

export const Caret = ({ open }) => {
  const ROTATION = {
    down: 135,
    up: 315,
  }

  const degrees = open ? ROTATION.up : ROTATION.down

  const StyledCaret = styled('i')(({ theme }) => ({
    content: '',
    width: '7px',
    height: '7px',
    marginLeft: '5px',
    marginTop: '-3px',
    borderTop: `2px solid ${theme.palette.primary.main}`,
    borderRight: `2px solid ${theme.palette.primary.main}`,
    transform: `rotateZ(${degrees}deg)`,
  }))

  return <StyledCaret />
}

Caret.defaultProps = {
  open: false,
}

Caret.propTypes = {
  open: PropTypes.bool,
}

MenuItemLabel.propTypes = {
  children: PropTypes.node,
  href: PropTypes.string,
  name: PropTypes.string,
  showCaret: PropTypes.bool,
}

export default MenuItem
