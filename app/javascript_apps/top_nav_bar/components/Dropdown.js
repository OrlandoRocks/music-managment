import React from 'react'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import SvgLockIcon from '../assets/svg/lock'

const Dropdown = styled('div')({
  '@media screen and (min-width: 1201px)': {
    position: 'absolute',
    top: '100%',
    transform: 'translateY(-20px)',
    minWidth: '200px',
    left: 0,
    background: 'black',
  },

  padding: 0,
  transition: 'all 200ms ease-in-out',
  zIndex: 97,

  '&.right': {
    left: 'unset',
    right: '0',
  },

  '@media screen and (max-width: 1200px) &.open': {
    display: 'block',
  },
})

const Description = styled('div')(({ theme }) => ({
  color: theme.palette.primary.main,
  marginTop: '10px',
  textTransform: 'initial',
}))

const StyledName = styled('div')({
  alignItems: 'center',
  display: 'flex',

  '& svg': {
    marginLeft: '0.5rem',
    width: '24px',

    '& path': {
      fill: '#fff',
      opacity: '50%',
    },
  },
})

function Name({ isLocked, name }) {
  return (
    <StyledName>
      {name}
      <SvgLockIcon show={isLocked} />
    </StyledName>
  )
}

Name.propTypes = {
  isLocked: PropTypes.bool,
  name: PropTypes.string,
}

const DropdownItem = ({
  children,
  description,
  href,
  isLocked,
  lockIconAriaLabel,
  name,
  show = true,
  ...props
}) => {
  const tag = href ? 'a' : 'div'
  const Component = styled(tag)(({ theme }) => ({
    '&&': {
      color: 'white',
    },
    '&&:hover': {
      color: `${tag === 'a' ? theme.palette.primary.main : 'white'}`,
      cursor: `${tag === 'div' ? 'auto' : 'inherit'}`,
    },

    fontWeight: 400,
    textTransform: 'uppercase',
    letterSpacing: '2px',
    whiteSpace: 'nowrap',
    fontSize: '11px',
    padding: '15px 20px',
    display: 'block',
    borderTop: '1px solid rgba(255, 255, 255, .1)',

    '&.locked': {
      pointerEvents: 'none',
    },

    '&:link': {
      textDecoration: 'none',
    },

    '&:last-child': {
      paddingBottom: '20px',
    },

    '@media screen and ( max-width: 1200px )': {
      fontSize: '14px',
      color: 'rgba(255, 255, 255, .8)',
      border: 'none',
      textAlign: 'center',
      letterSpacing: '1px',
      padding: '10px 0',
    },
  }))

  const lockableHref = isLocked ? '/#' : href

  function handleSelection(e) {
    if (isLocked) e.preventDefault()
  }

  if (!show) return null

  return (
    <Component
      aria-disabled={isLocked}
      aria-label={isLocked ? lockIconAriaLabel : null}
      className={isLocked ? 'locked' : null}
      href={lockableHref}
      onClick={handleSelection}
      onKeyUp={handleSelection}
      {...props}
    >
      <Name isLocked={isLocked} name={name} />
      {description && <Description>{description}</Description>}
      {children}
    </Component>
  )
}

const StyledSubOptionAnchor = styled('a')(({ theme }) => ({
  color: 'white',
  whiteSpace: 'nowrap',
  display: 'block',

  '&&': {
    color: 'white',
  },
  '&&:hover, &&:focus': {
    color: theme.palette.primary.main,
  },

  fontWeight: 'normal',
  fontSize: '13px',
  textTransform: 'none',
  letterSpacing: 0,
  padding: '10px 0',
  border: 'none',
  textDecoration: 'none',

  '&:hover': {
    color: theme.palette.primary.main,
  },

  '&:first-child': {
    paddingTop: '20px',
  },

  '&:last-child': {
    paddingBottom: 0,
  },
}))

const DropdownItemSubOption = ({ name, ...props }) => {
  return <StyledSubOptionAnchor {...props}>{name}</StyledSubOptionAnchor>
}

DropdownItem.propTypes = {
  children: PropTypes.node,
  description: PropTypes.string,
  href: PropTypes.string,
  isLocked: PropTypes.bool,
  lockIconAriaLabel: PropTypes.string,
  name: PropTypes.string,
  show: PropTypes.bool,
}

DropdownItemSubOption.propTypes = {
  name: PropTypes.string,
  href: PropTypes.string,
}

export { Dropdown, DropdownItem, DropdownItemSubOption }
