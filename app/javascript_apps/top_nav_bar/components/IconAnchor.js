import { styled } from '@material-ui/core/styles'

export const IconAnchor = styled('a')(({ theme }) => ({
  alignItems: 'center',
  boxSizing: 'border-box',
  color: 'white',
  display: 'flex',
  flexDirection: 'row',
  fontSize: '11px',
  fontWeight: 700,
  height: '100%',
  letterSpacing: '2px',
  padding: '0 13px',
  position: 'relative',
  textTransform: 'uppercase',
  whiteSpace: 'nowrap',
  width: '50px',
  zIndex: 1,

  '&&': {
    color: 'white',
  },

  '&.language': {
    textDecoration: 'none',
    width: '70px',
  },

  '&.language span': {
    marginLeft: '5px',
  },

  '&:link, &:hover, &:active, &:focus': {
    color: 'white',
    textDecoration: 'none',
    animation: 'none',
  },

  '& svg': {
    '& path': {
      fill: 'white',
    },
    width: '20px',
  },

  '&:hover': {
    cursor: 'pointer',
  },

  '&:hover svg': {
    '& path': {
      fill: theme.palette.primary.main,
    },
  },

  '@media screen and (max-width: 768px)': {
    position: 'initial',
  },
}))
