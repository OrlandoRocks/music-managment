import React from 'react'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const LineContainer = styled('div')({
  margin: 'auto',
  position: 'absolute',
  top: 0,
  right: 0,
  left: 0,
  bottom: 0,
  width: '22px',
  height: '12px',
})

const Line = styled('span')({
  position: 'absolute',
  display: 'block',
  width: '100%',
  height: '2px',
  backgroundColor: 'white',
  borderRadius: '1px',
  transition: 'all 0.2s cubic-bezier(0.1, 0.82, 0.76, 0.965)',

  '&:first-of-type': {
    top: 0,
  },

  '&:last-of-type': {
    bottom: '1px',
  },
})

const InvisibleCheckbox = styled('input')({
  display: 'block',
  width: '100%',
  height: '100%',
  cursor: 'pointer',
  zIndex: 2,
  '-webkit-touch-callout': 'none',
  position: 'absolute',
  opacity: 0,

  '&:checked ~ div span:first-of-type': {
    transform: 'rotate(45deg)',
    top: '5px',
  },

  '&:checked ~ div span:last-of-type': {
    transform: 'rotate(-45deg)',
    bottom: '5px',
  },
})

const MenuIcon = styled('div')({
  position: 'relative',
  width: '50px',
  height: '50px',
  cursor: 'pointer',
  display: 'none',

  '@media screen and ( max-width: 1200px )': {
    display: 'block',
  },
})

export default function MobileOnlyHamburgerIcon({ ariaLabel, onClick }) {
  return (
    <MenuIcon aria-label={ariaLabel} role="button" onClick={onClick}>
      <InvisibleCheckbox type="checkbox" id="hamburger-icon" />
      <LineContainer>
        <Line />
        <Line />
      </LineContainer>
    </MenuIcon>
  )
}

MobileOnlyHamburgerIcon.propTypes = {
  ariaLabel: PropTypes.string,
  onClick: PropTypes.func,
}
