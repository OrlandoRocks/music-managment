import React from 'react'
import PropTypes from 'prop-types'
import { styled } from '@material-ui/core/styles'

import MenuItem from './MenuItem'
import { Dropdown, DropdownItem } from './Dropdown'
import { IconAnchor } from './IconAnchor'
import SvgAccountIcon from '../assets/svg/accountIcon'

import { ITEMS, propFactory } from '../propFactory'

export default function AccountMenu({ navData }) {
  const { translations } = navData

  const { account, admin, logout, plans, releaseAccount } = ITEMS

  const getProps = propFactory(navData)

  return (
    <AccountMenuContainer>
      <MenuItem
        CustomComponent={AccountMenuItem}
        ccAriaLabel={translations.my_account_display}
      >
        <DesktopOnlyDropdown className="dropdown right">
          <DropdownItem {...getProps(account)} />
          <DropdownItem {...getProps(plans)} />
          {navData.admin.is_admin && <DropdownItem {...getProps(admin)} />}
          {navData.admin.is_under_admin_control && (
            <DropdownItem {...getProps(releaseAccount)} />
          )}
          <DropdownItem {...getProps(logout)} />
        </DesktopOnlyDropdown>
      </MenuItem>
    </AccountMenuContainer>
  )
}

AccountMenu.propTypes = {
  navData: PropTypes.object,
}

const desktopOnly = {
  '@media screen and (max-width: 1200px)': {
    display: 'none',
  },
}

const DesktopOnlyIconAnchor = styled(IconAnchor)(desktopOnly)

const DesktopOnlyDropdown = styled(Dropdown)(desktopOnly)

const AccountMenuContainer = styled('div')({ display: 'flex' })

function AccountMenuItem(props) {
  return (
    <DesktopOnlyIconAnchor className="icon" {...props}>
      <SvgAccountIcon />
    </DesktopOnlyIconAnchor>
  )
}
