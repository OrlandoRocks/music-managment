import React from 'react'
import { styled } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import PropTypes from 'prop-types'

import MenuItem from './MenuItem'
import { Dropdown, DropdownItem, DropdownItemSubOption } from './Dropdown'
import SvgAccountIcon from '../assets/svg/accountIcon'

import { ITEMS, propFactory } from '../propFactory'

export default function MobileMenu({ isOpen, navData, openReleaseModal }) {
  const { translations } = navData

  const {
    account,
    admin,
    artistServices,
    createdSplits,
    discography,
    distributionCredits,
    facebookDashboard,
    logout,
    moneyAnalytics,
    plans,
    publishing,
    releaseAccount,
    releases,
    salesReport,
    socialMonetization,
    splitsInvites,
    storeManager,
    transactions,
    trends,
    youtubeMonetizations,
  } = ITEMS

  const getProps = propFactory(navData)

  if (!isOpen) return null

  return (
    <MenuContainer className={isOpen ? 'open' : ''}>
      <MobileMenuItems>
        <MobileMenuItem {...getProps(releases)} showCaret={true}>
          <Dropdown className="dropdown mobile">
            <DropdownItem {...getProps(discography)} />
            <DropdownItem {...getProps(storeManager)} />
            <DropdownItem {...getProps(socialMonetization)}>
              <DropdownItemSubOption {...getProps(youtubeMonetizations)} />
              {navData.show_fb_monetization && (
                <DropdownItemSubOption {...getProps(facebookDashboard)} />
              )}
            </DropdownItem>
            {navData.show.distribution_credits && (
              <DropdownItem {...getProps(distributionCredits)} />
            )}
          </Dropdown>
        </MobileMenuItem>
        <MobileMenuItem {...getProps(moneyAnalytics)} showCaret={true}>
          <Dropdown className="dropdown mobile">
            <DropdownItem {...getProps(transactions)} />
            <DropdownItem {...getProps(salesReport)} />
            <DropdownItem {...getProps(trends)} />
            <DropdownItem {...getProps(createdSplits)} />
            <DropdownItem {...getProps(splitsInvites)} />
          </Dropdown>
        </MobileMenuItem>
        <MobileMenuItem {...getProps(publishing)} />
        <MobileMenuItem {...getProps(artistServices)} />
        <MobileMenuItem {...getProps(plans)} />

        <AddReleaseContainer>
          <StyledMobileButton size="large" onClick={openReleaseModal}>
            {translations.add_release}
          </StyledMobileButton>
        </AddReleaseContainer>

        {navData.admin.is_admin && <MobileMenuItem {...getProps(admin)} />}
        {navData.admin.is_under_admin_control && (
          <MobileMenuItem {...getProps(releaseAccount)} />
        )}
      </MobileMenuItems>
      <MobileFooter>
        <MobileIconAnchor
          aria-label={translations.account}
          {...getProps(account)}
        >
          <SvgAccountIcon />
        </MobileIconAnchor>
        <MobileMenuItem {...getProps(logout)} />
      </MobileFooter>
    </MenuContainer>
  )
}

MobileMenu.propTypes = {
  isOpen: PropTypes.bool,
  navData: PropTypes.object,
  openReleaseModal: PropTypes.func,
}

const MenuContainer = styled('div')({
  boxSizing: 'border-box',
  fontFamily: 'Roboto, sans-serif',
  inset: 0,
  display: 'flex',
  flexDirection: 'column',
  position: 'fixed',
  '&&': {
    top: '60px',
  },
  justifyContent: 'space-between',
  alignItems: 'center',
  background: 'black',
  padding: '20px',
  transition: 'all 500ms ease-in-out',
  clipPath: 'circle(0% at 50% -100%)',
  zIndex: 0,

  '&.open': {
    clipPath: 'circle(250% at 50% 0%)',
    zIndex: 99,
  },

  '&.open &.mobile-footer': {
    animation: 'footer 300ms ease',
    animationDelay: '200ms',
    animationFillMode: 'forwards',
  },
})

const MobileMenuItems = styled('div')({
  width: '100%',
  flex: '1 1',
  flexDirection: 'column',
  overflow: 'auto',
  maskImage:
    'linear-gradient(to top, rgba(0, 0, 0, 0) 0px, rgba(0, 0, 0, 1) 15px)',
})

const MobileFooter = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',

  '& button': {
    width: 'fit-content',
  },
})

const MobileMenuItem = ({ children, ...props }) => (
  <MenuItem {...props}>
    {children && (
      <>
        <InvisibleCheckbox type="checkbox" />
        {children}
      </>
    )}
  </MenuItem>
)

MobileMenuItem.propTypes = {
  name: PropTypes.string,
  children: PropTypes.node,
  href: PropTypes.string,
  showCaret: PropTypes.bool,
}

const InvisibleCheckbox = styled('input')({
  display: 'block',
  width: '100%',
  height: '100%',
  cursor: 'pointer',
  '-webkit-touch-callout': 'none',
  position: 'absolute',
  opacity: 0,

  '&:checked ~ .dropdown': {
    display: 'block',
  },
})

const AddReleaseContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
})

const StyledMobileButton = styled(Button)({
  margin: '24px auto',
})

const MobileIconAnchor = styled('a')(({ theme }) => ({
  width: '50px',
  height: '100%',
  color: 'white',
  fontWeight: 700,
  textTransform: 'uppercase',
  letterSpacing: '2px',
  whiteSpace: 'nowrap',
  fontSize: '11px',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  position: 'relative',
  padding: '0 13px',
  zIndex: 1,

  '& svg': {
    '& path': {
      fill: 'white',
    },
    width: '24px',
  },

  '&:hover': {
    cursor: 'pointer',
  },

  '&:hover svg': {
    '& path': {
      fill: theme.palette.primary.main,
    },
  },
}))
