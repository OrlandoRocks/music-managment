import React, { useEffect, useRef } from 'react'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import CloseIcon from '../assets/svg/CloseIcon'
import { useOnClickOutside } from '../hooks/useOnClickOutside'

const Outline = styled('div')({
  alignItems: 'center',
  backdropFilter: 'blur(5px)',
  background: 'rgba(20, 20, 20, 0.7)',
  display: 'flex',
  height: '100%',
  justifyContent: 'center',
  position: 'fixed',
  right: 0,
  top: 0,
  width: '100%',
  zIndex: 100,
})

const Content = styled('div')({
  background: '#282828',
  borderRadius: '10px',
  boxShadow: 'rgba(0, 0, 0, 0.2) 0px 10px 30px',
  color: 'white',
  display: 'flex',
  flexDirection: 'column',
  minWidth: '500px',
  overflow: 'auto',
  padding: '30px',
  position: 'fixed',
  zIndex: 101,

  '@media screen and (max-width: 1200px)': {
    height: '100%',
    justifyContent: 'center',
    minWidth: '100%',
    paddingTop: '60px',
    width: '100%',
  },
})

const Header = styled('h2')({
  color: 'white',
  fontSize: '24px',
  fontWeight: 700,
  letterSpacing: '-0.2px',
  lineHeight: '1.235',
})

const CloseButton = styled('button')({
  background: 'none',
  border: 'none',
  opacity: '.8',
  position: 'absolute',
  right: '15px',
  top: '15px',
  zIndex: '1',

  '& svg': {
    fill: 'white',
    height: '15px',
    pointerEvents: 'none',
    width: '15px',
  },

  '&:hover': {
    cursor: 'pointer',
    opacity: '1',
    transform: 'scale(1.1)',
  },

  '@media screen and (max-width: 1200px)': {
    transform: 'scale(2.5)',
    top: '35px',
    right: '25px',

    '&:hover': {
      transform: 'scale(2.5)',
    },
  },
})

export default function Modal({ children, closeModal, header, isOpen }) {
  useEffect(() => {
    const handler = ({ key }) => key === 'Escape' && closeModal()

    if (isOpen) {
      window.addEventListener('keyup', handler)
    } else {
      window.removeEventListener('keyup', handler)
    }

    return () => window.removeEventListener('keyup', handler)
  }, [closeModal, isOpen])

  const ref = useRef()
  useOnClickOutside(ref, () => {
    isOpen && closeModal()
  })

  if (!isOpen) return null

  return (
    <Outline>
      <Content
        ref={ref}
        role="alertdialog"
        aria-modal="true"
        aria-labelledby="modal-header"
      >
        <CloseButton
          onClick={closeModal}
          role="button"
          aria-label="Close modal"
          autoFocus
        >
          <CloseIcon />
        </CloseButton>
        <Header id="modal-header">{header}</Header>
        {children}
      </Content>
    </Outline>
  )
}

Modal.propTypes = {
  children: PropTypes.node,
  closeModal: PropTypes.func,
  header: PropTypes.node,
  isOpen: PropTypes.bool,
}
