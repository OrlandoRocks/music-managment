import React from 'react'
import { styled } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const PricingLink = styled('a')({
  lineHeight: '18px',
  color: 'white',
  marginLeft: '5px',
})

const PricingInfo = styled('div')({
  fontSize: '14px',
  fontWeight: 400,
  marginTop: '25px',
  color: 'rgba(255, 255, 255, 0.8)',
})

const PerReleasePricingInfo = ({ navData, translations }) => {
  if (navData.plan) return null
  return (
    <PricingInfo>
      {translations.how_much_cost}
      <PricingLink href={translations.tunecore_cost} target="_blank">
        {translations.view_pricing}
      </PricingLink>
    </PricingInfo>
  )
}

PerReleasePricingInfo.propTypes = {
  navData: PropTypes.object,
  translations: PropTypes.object,
}

export default PerReleasePricingInfo
