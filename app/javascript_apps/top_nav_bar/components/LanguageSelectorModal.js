import React from 'react'
import axios from 'axios'
import queryString from 'query-string'
import PropTypes from 'prop-types'
import { styled } from '@material-ui/core/styles'

import Modal from './Modal'

const LanguageOptionsContainer = styled('div')({
  marginTop: '1rem',
  display: 'flex',
  flexDirection: 'column',
})

const LanguageOption = styled('button')(({ theme }) => ({
  fontFamily: 'Roboto, sans-serif',
  fontWeight: 400,
  fontSize: '1rem',
  lineHeight: 1.5,
  letterSpacing: '0.00938em',
  color: 'rgb(255, 255, 255, 0.7)',
  width: 'fit-content',
  backgroundColor: 'transparent',
  border: 'none',
  cursor: 'pointer',

  '&:hover': {
    color: 'white',
  },

  '&.current': {
    fontWeight: 700,
    color: theme.palette.primary.main,
  },
}))

const CSRF_TOKEN = document.querySelector('[name=csrf-token]').content

export default function LanguageSelectorModal({
  closeLanguageModal,
  currentLang,
  header,
  isOpen,
  languageOptions,
}) {
  const onClick = async ({ target: { value } }) => {
    const headers = {
      'X-CSRF-TOKEN': CSRF_TOKEN,
    }
    const parsedQuery = queryString.parse(location.search)
    parsedQuery.locale = value

    const params = {
      language: {
        value,
      },
    }

    await axios.put('/api/backstage/people/preference', params, { headers })

    location.search = queryString.stringify(parsedQuery)
  }

  return (
    <Modal closeModal={closeLanguageModal} header={header} isOpen={isOpen}>
      <LanguageOptionsContainer>
        {languageOptions.map((option, index) => (
          <LanguageOption
            key={index}
            className={
              option.selector_language === currentLang ? 'current' : ''
            }
            value={option.yml_languages}
            onClick={onClick}
          >
            {option.selector_language}
          </LanguageOption>
        ))}
      </LanguageOptionsContainer>
    </Modal>
  )
}

LanguageSelectorModal.propTypes = {
  closeLanguageModal: PropTypes.func,
  currentLang: PropTypes.string,
  header: PropTypes.string,
  isOpen: PropTypes.bool,
  languageOptions: PropTypes.array,
}
