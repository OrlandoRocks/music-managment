const isFalsey = (x) => Boolean(x) === false
export const isLocked = (feature) => isFalsey(feature)
