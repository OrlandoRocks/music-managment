import {
  APP_PATHS,
  ARTIST_SERVICES_URLS,
  COM,
  HOST,
  LOCAL,
  PROD_APP_SUBDOMAIN,
  PROD_MARKETING_SUBDOMAIN,
  STAGING_MARKETING_SUBDOMAIN,
  UAT_APP_SUBDOMAIN,
  USER_ID_VAR,
  US_TLD,
} from './linkConstants'

const TC_HOST_PARSER_RE = /(?<subdomain>^.*)(?:\.tunecore\.)(?<tld>.*$)/
const URL_PARSER_RE = /\/\/((?<subdomain>.+?)\.)?(?<domain>.+?)\.(?<tld>.+?)(?<path>\/.*|$)/

// All nav links must be absolute URLs so that they work on the marketing site.

///////////////////////////////////////////////////////////////////////////////
// tc-www URL utils
///////////////////////////////////////////////////////////////////////////////

const withUserID = (path, userID) => path.replace(USER_ID_VAR, userID)

export function appURLs(userID) {
  return Object.entries(APP_PATHS).reduce((acc, [name, path]) => {
    if (!isValidPath(path)) throw 'Paths must start with a /'

    const preparedPath = withUserID(path, userID)
    acc[name] = toAppURL(preparedPath)

    return acc
  }, {})
}

function toAppURL(path) {
  const match = HOST.match(TC_HOST_PARSER_RE)
  if (!match) return `//${PROD_APP_SUBDOMAIN}.tunecore.${US_TLD}${path}`

  let { subdomain, tld } = match.groups
  subdomain = handleMarketingSubdomains(subdomain)

  return `//${subdomain}.tunecore.${tld}${path}`
}

function handleMarketingSubdomains(subdomain) {
  switch (subdomain) {
    case PROD_MARKETING_SUBDOMAIN:
      return PROD_APP_SUBDOMAIN
    case STAGING_MARKETING_SUBDOMAIN:
      return UAT_APP_SUBDOMAIN
    default:
      return subdomain
  }
}

function isValidPath(path) {
  return path.startsWith('/')
}

///////////////////////////////////////////////////////////////////////////////
// other URL utils
///////////////////////////////////////////////////////////////////////////////

export function pickArtistServicesCmsUrl() {
  const match = HOST.match(TC_HOST_PARSER_RE)
  if (!match) return ARTIST_SERVICES_URLS.com

  const { tld } = match.groups
  const result = ARTIST_SERVICES_URLS[tld]

  return result || ARTIST_SERVICES_URLS.com
}

// Ensure input's TLD matches HOST TLD
export function replaceTld(url) {
  const currentMatch = HOST.match(TC_HOST_PARSER_RE)
  if (!currentMatch) return url

  const { tld: currentTld } = currentMatch.groups
  if (currentTld === LOCAL) return url.replace(urlTld, COM)

  const urlMatch = url.match(URL_PARSER_RE)
  if (!urlMatch) return url

  const { tld: urlTld } = urlMatch.groups

  return url.replace(urlTld, currentTld)
}
