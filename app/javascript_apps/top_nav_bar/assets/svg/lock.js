import * as React from 'react'
import PropTypes from 'prop-types'

export default function SvgLockIcon({ show }) {
  if (!show) return null

  return (
    <svg
      fill="white"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 142.9 142.9"
    >
      <g id="Layer_2" data-name="Layer 2">
        <g id="Component_97_2" data-name="Component 97 2">
          <g id="Ellipse_272" data-name="Ellipse 272">
            <path d="M71.45,142.9A71,71,0,0,1,0,71.45,71,71,0,0,1,71.45,0,71,71,0,0,1,142.9,71.45,71,71,0,0,1,71.45,142.9Zm0-132.69a61.24,61.24,0,1,0,61.24,61.24A61.42,61.42,0,0,0,71.45,10.21Z" />
          </g>
          <path d="M107.86,74.05v31.21a7.81,7.81,0,0,1-7.8,7.81H42.84a7.81,7.81,0,0,1-7.8-7.81V74.05a7.8,7.8,0,0,1,7.8-7.8h3.9V54.54a24.71,24.71,0,1,1,49.42,0V66.25h3.9A7.8,7.8,0,0,1,107.86,74.05ZM83.15,54.54a11.7,11.7,0,0,0-23.4,0V66.25h23.4Z" />
        </g>
      </g>
    </svg>
  )
}

SvgLockIcon.propTypes = {
  show: PropTypes.bool,
}
