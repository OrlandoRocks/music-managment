import {
  createGenerateClassName,
  StylesProvider,
  ThemeProvider,
} from '@material-ui/core/styles'
import React from 'react'
import ReactDOM from 'react-dom'

import NavigationBar from './components/NavigationBar'

import { appURLs, pickArtistServicesCmsUrl, replaceTld } from './linkHelpers'
import { getPlanTheme } from './themes'

// Prevent hash collisions with MUI classnames from other TC React apps.
function generateClassName() {
  const cssNamespace = 'nav'

  return createGenerateClassName({
    productionPrefix: cssNamespace,
    seed: cssNamespace, // dev
  })
}

function prepareData() {
  const scriptTag = document.querySelector('#v2-nav-js')
  const result = JSON.parse(scriptTag.dataset.nav)
  result.urls = {
    ...appURLs(result.user_id),
    artist_services_cms_url: pickArtistServicesCmsUrl(),
    publishing_url: replaceTld(result.publishing_url), // Can be an app or marketing URL.
  }
  delete result.publishing_url

  return result
}

const navData = prepareData()

ReactDOM.render(
  <ThemeProvider theme={getPlanTheme(navData)}>
    <StylesProvider generateClassName={generateClassName()}>
      <NavigationBar navData={navData} />
    </StylesProvider>
  </ThemeProvider>,
  document.querySelector('#v2-nav-mountpoint')
)
