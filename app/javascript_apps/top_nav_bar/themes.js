import { createMuiTheme } from '@material-ui/core/styles'

// Copied from https://github.com/tunecore/storybook-tunecore/blob/main/src/themes/themes.ts

const PLAN_THEME_NAMES = {
  new_artist: 'Green',
  rising_artist: 'Blue',
  breakout_artist: 'Red',
  professional: 'Yellow',
}
export const getPlanTheme = (navData) => {
  const plan = navData.plan?.name

  const themeName = PLAN_THEME_NAMES[plan] || 'Green'
  return generatedPlanThemes.find((theme) => theme.name === themeName).muiTheme
}

const green = {
  name: 'Green',
  muiTheme: createMuiTheme({
    palette: {
      mode: 'dark',
      primary: {
        main: '#00EF86',
      },
      secondary: {
        main: '#008EB9',
      },
    },
  }),
}

const blue = {
  name: 'Blue',
  muiTheme: createMuiTheme({
    palette: {
      mode: 'dark',
      primary: {
        main: '#008CFF',
      },
      secondary: {
        main: '#3D00CC',
      },
    },
  }),
}

const red = {
  name: 'Red',
  muiTheme: createMuiTheme({
    palette: {
      mode: 'dark',
      primary: {
        main: '#EC3258',
      },
      secondary: {
        main: '#C000B6',
      },
    },
  }),
}

const yellow = {
  name: 'Yellow',
  muiTheme: createMuiTheme({
    palette: {
      mode: 'dark',
      primary: {
        main: '#FCDA00',
      },
      secondary: {
        main: '#EC3258',
      },
    },
  }),
}

// Merges themes from above with a base theme
// Note: Since this was copied from an MUI 5 repo, parts of the themes don't work as intended. Commented out parts that break current functionality
const generateTheme = (planTheme) => {
  const theme = planTheme.muiTheme
  return {
    name: planTheme.name,
    muiTheme: createMuiTheme(theme, {
      typography: {
        fontFamily: 'Roboto, "Helvetica Neue", Arial',
        button: {
          letterSpacing: 2,
          fontWeight: 900,
        },
        h1: {
          color: theme.palette.text.primary,
          fontSize: '1.875rem',
          fontWeight: 900,
          letterSpacing: -0.031,
          lineHeight: 1.2,
          [theme.breakpoints.down('md')]: {
            fontSize: '1.625rem',
          },
        },
      },
      overrides: {
        MuiButton: {
          root: {
            backgroundImage: `linear-gradient(-45deg, ${theme.palette.primary.main}, ${theme.palette.secondary.main})`,
            padding: '13px 30px',
            borderRadius: 400,
            letterSpacing: 1,
            fontSize: '0.875rem',
            boxShadow: 'none',
            color: 'white',
            '&:hover': {
              boxShadow: `0 0 10px ${theme.palette.primary.dark}30`,
              filter: 'brightness(1.1)',
            },
            '&:disabled': {
              opacity: 0.5,
            },
          },
          outlined: {
            borderWidth: 2,
            borderColor: theme.palette.grey.A400,
            color: theme.palette.text.primary,
            background: 'transparent',
            '&:hover': {
              borderWidth: 2,
              borderColor: theme.palette.primary.main,
              background: 'transparent',
            },
          },
          sizeSmall: {
            padding: '8px 15px',
            fontSize: '0.75rem',
          },
          sizeLarge: {
            padding: '12px 20px',
            fontSize: '0.8125rem',
          },
        },
      },
    }),
  }
}

const generatedPlanThemes = [
  generateTheme(green),
  generateTheme(blue),
  generateTheme(red),
  generateTheme(yellow),
]
