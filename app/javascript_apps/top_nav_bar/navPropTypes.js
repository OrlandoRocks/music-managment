import PropTypes from 'prop-types'

const plan = PropTypes.shape({
  id: PropTypes.number,
  display_name: PropTypes.string,
  features: PropTypes.shape({
    accept_splits: PropTypes.bool,
    create_splits: PropTypes.bool,
  }),
  name: PropTypes.string,
})

const show = PropTypes.shape({
  distribution_credits: PropTypes.bool,
  fb_monetization: PropTypes.bool,
  splits_invites: PropTypes.bool,
})

const navData = PropTypes.shape({
  artist_services_cms_url: PropTypes.string,
  country_website: PropTypes.string,
  plan,
  publishing_url: PropTypes.string,
  show,
  show_fb_monetization: PropTypes.bool,
  translations: PropTypes.object,
  urls: PropTypes.object,
})

const propTypes = {
  navData,
}

export default propTypes
