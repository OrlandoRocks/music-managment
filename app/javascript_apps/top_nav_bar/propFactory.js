import { isLocked } from './helpers'

export const ITEMS = {
  account: 'account',
  admin: 'admin',
  artistServices: 'artistServices',
  cart: 'cart',
  createdSplits: 'createdSplits',
  discography: 'discography',
  distributionCredits: 'distributionCredits',
  facebookDashboard: 'facebookDashboard',
  logout: 'logout',
  moneyAnalytics: 'moneyAnalytics',
  notifications: 'notifications',
  plans: 'plans',
  publishing: 'publishing',
  releaseAccount: 'releaseAccount',
  releases: 'releases',
  salesReport: 'salesReport',
  socialMonetization: 'socialMonetization',
  splitsInvites: 'splitsInvites',
  storeManager: 'storeManager',
  transactions: 'transactions',
  trends: 'trends',
  youtubeMonetizations: 'youtubeMonetizations',
}

// Ensure navData-based props are consistent between DesktopMenu, Icons and MobileMenu.
//
// Accepts `navData` obj
// Returns a function that accepts a constant and returns a props object.
export function propFactory({ plan, show, translations, urls }) {
  return (item) => {
    if (item === undefined) throw `item undefined`

    switch (item) {
      case ITEMS.discography:
        return {
          href: urls.discography,
          name: translations.discography,
        }
      case ITEMS.storeManager:
        return {
          href: urls.storeManager,
          name: translations.store_manager,
        }
      case ITEMS.socialMonetization:
        return {
          name: translations.social_monetization,
        }
      case ITEMS.youtubeMonetizations:
        return {
          href: urls.youtubeMonetizations,
          name: translations.youtube_content_id,
        }
      case ITEMS.facebookDashboard:
        return {
          href: urls.facebookDashboard,
          name: translations.facebook_instagram_reels,
        }
      case ITEMS.distributionCredits:
        return {
          href: urls.credits,
          name: translations.distribution_credits,
        }
      case ITEMS.moneyAnalytics:
        return {
          name: translations.money_and_analytics,
        }
      case ITEMS.transactions:
        return {
          href: urls.myAccountTransactions,
          name: translations.balance_history,
        }
      case ITEMS.salesReport:
        return {
          href: urls.salesReport,
          name: translations.sales_report,
        }
      case ITEMS.trends:
        return {
          href: urls.trendsReport,
          name: translations.analytics,
        }
      case ITEMS.createdSplits:
        return {
          href: urls.createdSplits,
          isLocked: isLocked(plan.features.create_splits),
          lockIconAriaLabel: translations.lock_icon_aria_label,
          name: translations.created_splits_link_label,
          show: show.created_splits,
        }
      case ITEMS.splitsInvites:
        return {
          href: urls.splitsInvites,
          isLocked: isLocked(plan.features.accept_splits),
          lockIconAriaLabel: translations.lock_icon_aria_label,
          name: translations.splits_invites_link_label,
          show: show.splits_invites,
        }
      case ITEMS.publishing:
        return {
          href: urls.publishing_url,
          name: translations.publishing,
        }
      case ITEMS.artistServices:
        return {
          href: urls.artist_services_cms_url,
          name: translations.artist_services,
        }
      case ITEMS.account:
        return {
          href: urls.editAccount,
          name: translations.account,
        }
      case ITEMS.plans:
        return {
          name: plan
            ? translations.unlimited_plan
            : translations.unlimited_plans,
          description: plan?.display_name ?? translations.go_unlimited,
          href: urls.plans,
        }
      case ITEMS.admin:
        return {
          href: urls.admin,
          name: translations.admin,
        }
      case ITEMS.releaseAccount:
        return {
          href: urls.releaseAccount,
          name: translations.give_back_control,
        }
      case ITEMS.logout:
        return {
          href: urls.logout,
          name: translations.log_out,
        }
      case ITEMS.releases:
        return {
          name: translations.releases,
        }
      case ITEMS.notifications:
        return {
          'aria-label': translations.notifications_header,
          href: urls.notifications,
        }
      case ITEMS.cart:
        return {
          'aria-label': translations.shopping_cart,
          href: urls.cart,
        }
      default:
        throw `No match for item: '${item}'.`
    }
  }
}
