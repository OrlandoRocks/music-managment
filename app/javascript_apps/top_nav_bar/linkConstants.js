const HOST = window.location.host
const PATH = window.location.pathname

const COM = 'com'
const LOCAL = 'local'

const STAGING_MARKETING_SUBDOMAIN = 'stagingwp'
const PROD_APP_SUBDOMAIN = 'web'
const PROD_MARKETING_SUBDOMAIN = 'www'
const UAT_APP_SUBDOMAIN = 'uat'
const USER_ID_VAR = '{{user_id}}'
const US_TLD = 'com'

const ALBUM_ID_RE = /^\/(?:albums|ringtones|singles)\/(?<album>\d+)$/
const RELEASE_ACCOUNT_BASE_PATH = '/account/release_account'

// Internal tc-www routes. Values begin with a '/'.
const APP_PATHS = {
  admin: '/admin',
  cart: '/cart',
  createdSplits: '/royalty_splits',
  credits: '/credits',
  dashboard: '/dashboard',
  discography: '/discography',
  editAccount: `/people/${USER_ID_VAR}/edit`,
  facebookDashboard: '/facebook_tracks/dashboard',
  logout: '/logout',
  myAccountTransactions: '/my_account/list_transactions',
  newAlbum: '/albums/new',
  newRingtone: '/ringtones/new',
  newSingle: '/singles/new',
  notifications: '/notifications',
  plans: '/plans',
  releaseAccount: buildReleaseAccountURL(),
  salesReport: '/sales/date_report',
  splitsInvites: '/royalty_splits/invited',
  storeManager: '/store_manager',
  trendsReport: '/trend_reports',
  youtubeMonetizations: '/youtube_monetizations',
}

function buildReleaseAccountURL(path = PATH) {
  const match = path.match(ALBUM_ID_RE)
  if (!match) return RELEASE_ACCOUNT_BASE_PATH

  return `${RELEASE_ACCOUNT_BASE_PATH}?album=${match.groups.album}`
}

const ARTIST_SERVICES_URLS = {
  'co.uk': 'https://www.tunecore.co.uk/artist-services',
  'com.au': 'https://www.tunecore.com.au/artist-services',
  ca: 'https://www.tunecore.ca/artist-services',
  com: 'https://www.tunecore.com/artist-services',
  de: 'https://www.tunecore.de/kuenstler_service',
  fr: 'https://www.tunecore.fr/services-partenaires',
  it: 'https://www.tunecore.it/servizi-aggiuntivi',
}

export {
  APP_PATHS,
  ARTIST_SERVICES_URLS,
  COM,
  HOST,
  LOCAL,
  PROD_APP_SUBDOMAIN,
  PROD_MARKETING_SUBDOMAIN,
  RELEASE_ACCOUNT_BASE_PATH,
  STAGING_MARKETING_SUBDOMAIN,
  UAT_APP_SUBDOMAIN,
  USER_ID_VAR,
  US_TLD,
  buildReleaseAccountURL,
}
