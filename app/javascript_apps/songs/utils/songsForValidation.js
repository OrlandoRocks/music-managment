import { REMOVE_SONG } from '../actions/actionTypes'
import updateSongs from './updateSongs'

export default function songsForValidation(action, songs) {
  if (action.type === REMOVE_SONG) {
    let { [action.song.uuid]: _omit, ...rest } = songs
    return rest
  }
  return updateSongs(action, songs)
}
