import guid from '../../shared/guid'

export default function buildDefaultSongStartTime(storeId) {
  return {
    uuid: guid(),
    store_id: storeId,
    start_time: 0,
    index: 0,
  }
}
