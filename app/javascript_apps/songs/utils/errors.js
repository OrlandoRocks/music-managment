export const MESSAGE_LEVEL = 'error'

export const errorStyles = {
  display: 'flex',
  justifyContent: 'center',
}

export function fetchError(key, errors) {
  const error = errors[key]
  return error && error[0]
}
