import toSentenceList from './toSentenceList'

export default function songsForPreorderCalendar(songs, creatives) {
  return Object.values(songs).map((song) => {
    const { data } = song
    const artists = data.artists.map((artistId) => creatives[artistId])
    return { id: data.id, name: data.name, artist: artistName(artists) }
  })
}

function artistName(artists) {
  let artist = artists.find((artist) => artist.associated_to === 'Album')
  if (artist) {
    return artist.artist_name
  }

  artists = artists.filter((artist) => artist.credit === 'primary_artist')

  if (artists.length > 0) {
    return toSentenceList(artists.map((artist) => artist.artist_name))
  }

  return ''
}
