export default function reduceCreatives(creativesState, action) {
  return {
    ...creativesState,
    ...action.creatives,
  }
}
