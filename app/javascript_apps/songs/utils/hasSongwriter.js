export default function hasSongwriter(songwriters) {
  return songwriters.some((songwriter) => songwriter.name.length > 0)
}
