import hasSongwriter from './hasSongwriter'
import buildSongDataForApi from './buildSongDataForApi'

export default function buildSongData(
  song,
  creatives,
  songwriters,
  songwriterRoleId,
  songStartTimes
) {
  let songData = buildSongDataForApi(
    song,
    creatives,
    songwriters,
    songwriterRoleId,
    songStartTimes
  )

  const songSongwriters = song.songwriters.map(
    (songwriterId) => songwriters[songwriterId]
  )

  return {
    ...song,
    data: songData,
    hasSongwriter: hasSongwriter(songSongwriters, songwriterRoleId),
  }
}
