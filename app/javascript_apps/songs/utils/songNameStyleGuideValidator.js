import styleGuideCharacterValidator from './styleGuideCharacterValidator'

export default function songNameStyleGuideValidator(name) {
  return styleGuideCharacterValidator(name, 'song_name')
}
