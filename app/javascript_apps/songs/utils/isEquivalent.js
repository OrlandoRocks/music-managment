export default function isEquivalent(a, b, attrsToIgnore = []) {
  const aProps = Object.getOwnPropertyNames(a)
  const bProps = Object.getOwnPropertyNames(b)
  let equivalent = true
  if (aProps.length != bProps.length) {
    return false
  }
  aProps.forEach((propName) => {
    if (attrsToIgnore.includes(propName)) {
      return
    } else if (a[propName].constructor == Array) {
      if (a[propName].length === 0 && b[propName].length !== 0) {
        equivalent = false
      }
      a[propName].forEach((element, i) => {
        if (element !== b[propName][i]) {
          equivalent = false
        }
      })
    } else if (a[propName] !== b[propName]) {
      equivalent = false
    }
  })
  return equivalent
}
