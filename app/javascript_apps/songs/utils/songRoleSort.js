import partition from './partition'

function songRoleSort(songRoles) {
  const priorityRoles = ['songwriter', 'performer', 'producer']
  const [priorities, rest] = partition(Object.values(songRoles), (role) => {
    return priorityRoles.includes(role.role_type_raw)
  })

  return priorities
    .sort((obj1, obj2) => obj1.role_type.localeCompare(obj2.role_type))
    .concat(
      rest.sort((obj1, obj2) => obj1.role_type.localeCompare(obj2.role_type))
    )
}

export default songRoleSort
