export default function emitCheckoutButtonEvent(albumId) {
  var event = new CustomEvent('update-checkout-button', { detail: { albumId } })
  document.getElementById('distribution-panel').dispatchEvent(event)

  const progressBarApp = document.getElementById(
    'distribution_progress_bar_app'
  )
  if (progressBarApp) {
    progressBarApp.dispatchEvent(event)
  }
}
