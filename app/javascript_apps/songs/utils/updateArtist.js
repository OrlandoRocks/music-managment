export default function updateArtist(action, artist, warningAttr, warning) {
  let { attribute, value } = action
  artist = { ...artist, [attribute]: value }
  artist.errors = updateArtistErrors(artist, attribute)
  artist.styleGuideWarnings = updateStyleGuideWarnings(
    artist,
    warningAttr,
    warning
  )
  return artist
}

function updateArtistErrors(artist, attribute) {
  return {
    ...artist.errors,
    [`${attribute}_blank`]:
      artist[`${attribute}_blank`] && artist[attribute].length === 0,
  }
}

function updateStyleGuideWarnings(artist, warningAttr, warning) {
  if (warningAttr) {
    return {
      ...artist.styleGuideWarnings,
      [warningAttr]: warning,
    }
  } else {
    return artist.styleGuideWarnings
  }
}
