export default function songPreviouslyReleasedValid(
  is_previously_released,
  previously_released_at
) {
  if (is_previously_released) {
    return !!previously_released_at
  }

  return true
}
