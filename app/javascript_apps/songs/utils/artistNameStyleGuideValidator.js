import styleGuideCharacterValidator from './styleGuideCharacterValidator'

export default function artistNameStyleGuideValidator(
  name,
  state,
  artistUuid,
  action
) {
  let warning = styleGuideCharacterValidator(name, 'artist_name')
  if (warning) {
    return warning
  }

  let artistName = cleanNameOfSpecialCharacters(name)
  const {
    album: { is_various: isAlbumVarious },
  } = state

  if (isAlbumVarious && isArtistNameVarious(artistName)) {
    return 'cannot_be_various_artists'
  } else if (duplicateArtistName(artistName, state, artistUuid, action)) {
    return 'duplicate_artist_name'
  }
}

function duplicateArtistName(artistName, state, artistUuid, action) {
  const currentCreatives = state.currentCreatives
  for (const creativeId in currentCreatives) {
    let name = cleanNameOfSpecialCharacters(
      creativeId == action.artistUuid
        ? action.newName.trim()
        : currentCreatives[creativeId].artist_name
    )

    if (
      artistUuid !== creativeId &&
      name.toLowerCase() === artistName.toLowerCase() &&
      name.length !== 0
    ) {
      return true
    }
  }

  return false
}

function isArtistNameVarious(artistName) {
  return artistName.toLocaleLowerCase().startsWith('various artist')
}

function cleanNameOfSpecialCharacters(name) {
  let regexSpecialCharsPattern = /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g

  return name.replace(regexSpecialCharsPattern, '')
}
