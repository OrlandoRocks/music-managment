import guid from '../../shared/guid'
import { DEFAULT_COPYRIGHTS } from '../utils/defaultCopyrights'
import { DEFAULT_COVER_SONG_METADATA } from '../utils/defaultCoverSongMetadata.js'

export default function buildSongFromApi(song, songwriterRoleId) {
  const artists = song.data.artists
    .filter((artist) => {
      return !onlySongwriter(artist, songwriterRoleId)
    })
    .map((artist) => {
      return {
        ...artist,
        role_ids: artist.role_ids.filter(
          (roleId) => roleId.toString() !== songwriterRoleId
        ),
        uuid: guid(),
      }
    })

  const songwriters = song.data.artists
    .filter((artist) => {
      return isSongwriter(artist, songwriterRoleId)
    })
    .map((artist, index) => {
      return {
        name: artist.artist_name,
        uuid: guid(),
        index,
      }
    })

  const songStartTimes =
    (song.data.song_start_times &&
      song.data.song_start_times.map((song_start_time, index) => {
        return {
          ...song_start_time,
          uuid: guid(),
          index,
        }
      })) ||
    []

  const copyrights = song.data.copyrights || DEFAULT_COPYRIGHTS

  const cover_song_metadata =
    song.data.cover_song_metadata || DEFAULT_COVER_SONG_METADATA

  return {
    ...song,
    data: {
      ...song.data,
      copyrights,
      artists,
      cover_song_metadata,
    },
    uuid: guid(),
    songwriters,
    songStartTimes,
  }
}

function onlySongwriter(artist, songwriterRoleId) {
  return (
    artist.role_ids.length === 1 &&
    artist.role_ids[0].toString() === songwriterRoleId.toString() &&
    !['primary_artist', 'featuring'].includes(artist.credit)
  )
}

function isSongwriter(artist, songwriterRoleId) {
  return artist.role_ids
    .map((roleId) => roleId.toString())
    .includes(songwriterRoleId.toString())
}
