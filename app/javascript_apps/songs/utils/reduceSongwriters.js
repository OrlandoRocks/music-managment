export default function reduceSongwriters(songwritersState, action) {
  return {
    ...songwritersState,
    ...action.songwriters,
  }
}
