import isEquivalent from './isEquivalent'

export default function artistsEquivalent(artists, otherArtists) {
  let equivalent
  if (artists.length !== otherArtists.length) {
    return false
  }
  artists.forEach((artist, index) => {
    equivalent = isEquivalent(artist, otherArtists[index], [
      'creative_id',
      'uuid',
    ])
  })
  return equivalent
}
