export default function translatedSongNameValid(language, translatedName) {
  if (!language) {
    return true
  } else {
    return language.needs_translated_song_name
      ? validatePresenceOfTranslatedName(translatedName)
      : true
  }
}

function validatePresenceOfTranslatedName(name) {
  return !!(name && name.length > 0)
}
