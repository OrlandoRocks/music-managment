const latinCharRegex = /^[A-z\u00C0-\u00ff\s'\.,-\/#!$%\^&\*;:{}=\-_`~()]+$/

export default function songNameValid(language, name) {
  if (name.length === 0) {
    return false
  }
  // This is to bypass RESTRICTED_LANGUAGE_CODES, that don't get passed in here.
  // If a language code is restricted, language comes as undefined.
  if (!language) {
    return true
  } else {
    return language.needs_translated_song_name
      ? !hasLatinCharacters(name)
      : true
  }
}

function hasLatinCharacters(string) {
  return latinCharRegex.test(string)
}
