import attributeValidators from './attributeValidators'

export default function songErrorBuilder(song, newAttributes, storeState) {
  let attributeNames = Object.keys(newAttributes)
  return attributeNames.reduce((acc, attrName) => {
    if (Object.keys(attributeValidators).includes(attrName)) {
      let value = newAttributes[attrName]
      let attributeErrors =
        attributeValidators[attrName](song, value, storeState) || {}
      return { ...acc, ...attributeErrors }
    }
  }, {})
}
