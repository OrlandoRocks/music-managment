export default function assetFilenameDisplay(assetFilename) {
  if (!assetFilename) {
    return ''
  }

  const fileNameLength = assetFilename.length

  if (fileNameLength > 23) {
    const regexp = /(.*)\.(mp3|wav|flac)$/
    const matcher = assetFilename.toLowerCase().match(regexp)
    const extention = matcher[2]
    const filename = matcher[1].slice(0, 19)
    return filename + '...' + extention
  } else {
    return assetFilename
  }
}
