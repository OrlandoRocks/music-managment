import {
  CREATE_SONG,
  REGISTER_ASSET,
  REGISTER_SPATIAL_AUDIO_ASSET,
  REMOVE_SONG,
  UPDATE_SONG,
  REMOVE_SONG_ATMOS_ASSET,
} from '../actions/actionTypes'
import getAssetFilename from './getAssetFilename'
export default function updateSongs(action, songs) {
  if (action.type === REMOVE_SONG) {
    if (action.song) {
      const { [action.song.uuid]: _omit, ...rest } = songs
      return rest
    } else {
      const { [action.data.songUuid]: _omit, ...rest } = songs
      return rest
    }
  }

  if (action.type === REMOVE_SONG_ATMOS_ASSET) {
    let song = songs[action.data.songUuid]

    /* Update sidebar to display Dolby Atmos track price */
    try {
      window.sidebar(song.data.album_id)
    } catch (error) {
      console.error('Error with sidebar!', error)
    }

    return {
      ...songs,
      [song.uuid]: {
        ...song,
        data: {
          ...song.data,
          spatial_audio_asset_url: null,
          atmos_asset_filename: null,
        },
      },
    }
  }

  if (action.type === CREATE_SONG) {
    const { song: songData } = action
    const { data: actionData, asset_data: assetData } = songData

    if (assetData && actionData) {
      const { streaming_url: dataAssetUrl } = assetData
      const updatedData = { ...actionData, asset_url: dataAssetUrl }

      return {
        ...songs,
        [action.song.uuid]: {
          ...songData,
          data: updatedData,
        },
      }
    } else {
      return {
        ...songs,
        [action.song.uuid]: action.song,
      }
    }
  }

  if (action.type === UPDATE_SONG) {
    return {
      ...songs,
      [action.song.uuid]: action.song,
    }
  }

  if (action.type === REGISTER_ASSET) {
    let song = songs[action.data.uuid]
    return {
      ...songs,
      [song.uuid]: {
        ...song,
        data: {
          ...song.data,
          asset_url: action.data.asset_url,
          asset_filename: getAssetFilename(action.data.filename),
        },
        errors: {
          ...song.errors,
          asset_url: null,
        },
      },
    }
  }

  if (action.type === REGISTER_SPATIAL_AUDIO_ASSET) {
    let spatialAudioSong = songs[action.data.uuid]

    /* Update sidebar to display Dolby Atmos track price */
    try {
      window.sidebar(spatialAudioSong.data.album_id)
    } catch (error) {
      console.error('Error with sidebar!', error)
    }

    return {
      ...songs,
      [spatialAudioSong.uuid]: {
        ...spatialAudioSong,
        data: {
          ...spatialAudioSong.data,
          spatial_audio_asset_url: action.data.spatial_audio_asset_url,
          atmos_asset_filename: getAssetFilename(action.data.filename),
        },
        errors: {
          ...spatialAudioSong.errors,
          asset_url: null,
        },
      },
    }
  }
}
