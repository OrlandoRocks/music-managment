export default function isCompleteSong(song) {
  const {
    data: { name, asset_url },
    hasSongwriter,
  } = song
  const titleIsValid = name && name.length > 0
  return titleIsValid && hasSongwriter && asset_url
}
