import songPreviouslyReleasedValid from './songPreviouslyReleasedValid'
import songNameValid from './songNameValid'
import translatedSongNameValid from './translatedSongNameValid'
import optionalIsrcValid from './optionalIsrcValid'

const attributeValidators = {
  previously_released_at: previouslyReleasedValid,
  is_previously_released: previouslyReleasedValid,
  name: nameValid,
  hasSongwriter: songHasSongwriter,
  translated_name: translatedNameValid,
  language_code_id: languageCodeValid,
  optional_isrc: optionalISRCValid,
  explicit: explicitValid,
  clean_version: cleanVersionValid,
}

function previouslyReleasedValid(song, value) {
  if (song.errors.previously_released_at) {
    let valid = songPreviouslyReleasedValid(
      song.data.is_previously_released,
      value
    )
    return {
      previously_released_at: !valid,
    }
  }
}

function nameValid(song, name, storeState) {
  if (song.errors.name) {
    let language = storeState.languages[song.data.language_code_id]
    let valid = songNameValid(language, name)
    return {
      name: !valid,
    }
  }
}

function songHasSongwriter(song, value) {
  if (!song.hasSongwriter) {
    return {
      has_songwriter: !value,
    }
  }
}

function translatedNameValid(song, value, storeState) {
  if (song.errors.translated_name) {
    let language = storeState.languages[song.data.language_code_id]
    let valid = translatedSongNameValid(language, value)
    return {
      translated_name: !valid,
    }
  }
}

function languageCodeValid(song, languageCodeId, storeState) {
  if (song.errors.name || song.errors.translated_name) {
    let language = storeState.languages[languageCodeId]
    let valid = translatedSongNameValid(language, song.translated_name)

    return {
      name: !valid,
      translated_name: !valid,
    }
  }
}

function optionalISRCValid(song, isrc, storeState) {
  if (song.errors.optional_isrc) {
    return {
      optional_isrc: !optionalIsrcValid(song, isrc, storeState),
    }
  }
}

function explicitValid(song, explicit) {
  if (song.errors.explicit) {
    return {
      explicit: ![true, false].includes(explicit),
    }
  }
}

function cleanVersionValid(song, clean_version) {
  if (song.errors.clean_version) {
    return {
      clean_version: ![true, false].includes(clean_version),
    }
  }
}

export default attributeValidators
