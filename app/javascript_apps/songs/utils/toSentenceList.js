export default function toSentenceList(
  array,
  joiner = '&',
  hasOxfordComma = false
) {
  if (array.length > 2) {
    return `${array.slice(0, array.length - 1).join(', ')}${
      hasOxfordComma ? ',' : ''
    } ${joiner} ${array.slice(-1)}`
  } else if (array.length === 2) {
    return array.join(` ${joiner} `)
  } else {
    return array[0]
  }
}
