import {
  CREATE_SONG,
  UPDATE_SONG,
  REMOVE_SONG,
  REGISTER_ASSET,
} from '../actions/actionTypes'

export default [CREATE_SONG, UPDATE_SONG, REMOVE_SONG, REGISTER_ASSET]
