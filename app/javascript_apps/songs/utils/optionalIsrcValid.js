export default function optionalIsrcValid(song, isrc, storeState) {
  if (!isrc) {
    return true
  }

  if (!(isrc.length === 12)) {
    return false
  }

  for (const uuid in storeState.songs) {
    const thisSongISRC = storeState.songs[uuid].data.optional_isrc

    if (song.uuid !== uuid && thisSongISRC === isrc) {
      return false
    }
  }

  return true
}
