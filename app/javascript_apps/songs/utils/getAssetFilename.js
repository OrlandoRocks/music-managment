export default function getAssetFilename(origFilename) {
  const regex = /-(.*)/
  return regex.exec(origFilename)[1]
}
