export default function emitUpdateChecklistEvent(songsValid) {
  var event = new CustomEvent('update-checklist', { detail: { songsValid } })
  document.getElementById('distribution-panel').dispatchEvent(event)

  const progressBarApp = document.getElementById(
    'distribution_progress_bar_app'
  )
  if (progressBarApp) {
    progressBarApp.dispatchEvent(event)
  }
}
