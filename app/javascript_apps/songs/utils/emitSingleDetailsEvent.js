export default function emitSingleDetailsEvent(name, primaryArtistNames) {
  var singleDetails = {
    detail: {
      name: name,
      primary_artist_names: primaryArtistNames,
    },
  }
  var event = new CustomEvent('update_single_name', singleDetails)
  document.dispatchEvent(event)
}
