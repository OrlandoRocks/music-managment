export default function artistsValid(artists) {
  return artists.every((artist) => {
    return artist.artist_name.length > 0 && artist.credit.length > 0
  })
}
