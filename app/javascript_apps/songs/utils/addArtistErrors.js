export default function addArtistErrors(artists) {
  return artists.map((artist) => {
    return {
      ...artist,
      errors: {
        artist_name_blank: artist.artist_name.length === 0,
        credit_blank: artist.credit.length === 0,
      },
    }
  })
}
