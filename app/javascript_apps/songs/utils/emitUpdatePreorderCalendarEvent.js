export default function emitUpdatePreorderCalendarEvent(songs) {
  var event = new CustomEvent('update_calendar', { detail: { songs: songs } })
  document.dispatchEvent(event)
}
