export default function filledOutArtists(artists) {
  return artists.filter((artist) => {
    return (
      artist.artist_name.length > 0 ||
      artist.credit.length > 0 ||
      artist.role_ids.length > 0
    )
  })
}
