import translatedSongNameValid from './translatedSongNameValid'
import songNameValid from './songNameValid'
import songPreviouslyReleasedValid from './songPreviouslyReleasedValid'
import filledOutArtists from './filledOutArtists'
import artistsValid from './artistsValid'
import optionalIsrcValid from './optionalIsrcValid'
import hasSongwriter from './hasSongwriter'

export default function validSong(song, storeState) {
  const {
    name,
    language_code_id,
    artists: artistIds,
    copyrights,
    translated_name,
    explicit,
    clean_version,
    cover_song_metadata,
  } = song.data

  let {
    languages,
    currentCreatives,
    currentSongwriters,
    copyrightsFormEnabled,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    coverSongMetadataEnabled,
  } = storeState

  let language = languages[language_code_id]
  let completeArtists = filledOutArtists(
    artistIds.map((artistId) => currentCreatives[artistId])
  )

  return {
    previously_released_at: !songPreviouslyReleasedValid(
      song.data.is_previously_released,
      song.data.previously_released_at
    ),
    name: !songNameValid(language, name),
    has_songwriter: !hasSongwriter(Object.values(currentSongwriters)),
    translated_name: !translatedSongNameValid(language, translated_name),
    artists: !artistsValid(completeArtists),
    optional_isrc: !optionalIsrcValid(
      song,
      song.data.optional_isrc,
      storeState
    ),
    explicit:
      explicitFieldsEnabled && explicitRadioEnabled
        ? ![true, false].includes(explicit)
        : false,
    clean_version:
      cleanVersionRadioEnabled && explicit == false
        ? ![true, false].includes(clean_version)
        : false,
    compositionCopyright: copyrightsFormEnabled
      ? !copyrights.composition
      : false,
    recordingCopyright: copyrightsFormEnabled ? !copyrights.recording : false,
    coverSong:
      coverSongMetadataEnabled && cover_song_metadata
        ? ![true, false].includes(cover_song_metadata.cover_song)
        : false,
  }
}
