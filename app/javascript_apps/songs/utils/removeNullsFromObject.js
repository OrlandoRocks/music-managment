export default function removeNullsFromObject(obj) {
  return Object.values(obj)
    .filter((val) => val)
    .reduce((accumulator, val) => {
      return {
        ...accumulator,
        [val.uuid]: val,
      }
    }, {})
}
