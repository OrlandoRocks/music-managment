import isCompleteSong from './isCompleteSong'
import buildSongData from './buildSongData'

export default function songsCompleteCount(
  songs,
  creatives,
  songwriters,
  songwriterRoleId,
  songStartTimes
) {
  return Object.values(songs).filter((song) => {
    let songData = buildSongData(
      song,
      creatives,
      songwriters,
      songwriterRoleId,
      songStartTimes
    )
    return isCompleteSong(songData)
  }).length
}
