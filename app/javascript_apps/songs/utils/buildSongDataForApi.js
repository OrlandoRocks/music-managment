import partition from './partition'

export default function buildSongDataForApi(
  song,
  creatives,
  songwriters,
  songwriterRoleId,
  songStartTimes
) {
  const songSongwriters = song.songwriters
    .map((songwriterId) => songwriters[songwriterId])
    .filter((songwriter) => songwriter.name && songwriter.name.length > 0)
  const songArtists = song.data.artists.map((artistId) => creatives[artistId])
  const songwriterNames = songSongwriters.map((songwriter) => songwriter.name)
  const songArtistNames = songArtists.map((artist) => artist.artist_name)
  const [songwriterInCreativesForm, songwriterNotInCreativesForm] = partition(
    songwriterNames,
    (songwriterName) => {
      return songArtistNames.includes(songwriterName)
    }
  )

  const artists = songArtists
    .map((artist) => {
      const role_ids = artist.role_ids.map((roleId) => roleId.toString())
      if (
        songwriterInCreativesForm.includes(artist.artist_name) &&
        !role_ids.includes(songwriterRoleId)
      ) {
        role_ids.push(songwriterRoleId)
      }

      return {
        artist_name: artist.artist_name,
        role_ids,
        credit: artist.credit,
        creative_id: artist.creative_id,
        associated_to: artist.associated_to,
      }
    })
    .concat(
      songwriterNotInCreativesForm.map((artistName) => {
        return {
          artist_name: artistName,
          role_ids: [songwriterRoleId.toString()],
          credit: 'contributor',
          associated_to: 'Song',
        }
      })
    )

  const copyrights = song.data.copyrights

  const songStartTimesArray =
    songStartTimes &&
    Object.keys(songStartTimes).map((songStartTimeUuid) => {
      const rawSongStartTime = songStartTimes[songStartTimeUuid]

      const songStartTime = Object.keys(rawSongStartTime).reduce(
        (object, key) => {
          if (!['uuid', 'index'].includes(key)) {
            object[key] = rawSongStartTime[key]
          }

          return object
        },
        {}
      )

      return songStartTime
    })

  return {
    ...song.data,
    copyrights,
    artists,
    song_start_times: songStartTimesArray,
    instrumental: Boolean(song.data.instrumental),
  }
}
