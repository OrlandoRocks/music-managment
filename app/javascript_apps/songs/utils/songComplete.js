export default function songComplete(song, songwriterRoleId) {
  return (
    nameComplete(song) &&
    assetComplete(song) &&
    songwriterComplete(song, songwriterRoleId)
  )
}

function nameComplete(song) {
  const { name } = song.data
  return name ? name.length > 0 : false
}

function assetComplete(song) {
  const { asset_url } = song.data
  return asset_url ? asset_url.length > 0 : false
}

function songwriterComplete(song, songwriterRoleId) {
  const { artists } = song.data
  return artists.some((artist) =>
    artist.role_ids.includes(songwriterRoleId.toString())
  )
}
