export default function isClosable(song) {
  return !hasErrors(song)
}

function hasErrors(song) {
  !Object.keys(song.errors).length === 0
}
