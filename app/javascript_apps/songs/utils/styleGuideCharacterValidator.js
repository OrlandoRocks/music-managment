const HAS_FEATURING_REGEX = /(f(eat|t)(\.{0,1}|(ur(ing|e(d|s))){1})|\sw(ith|\/|\.){0,1})+\s/i //https://regexr.com/48rtl
const HAS_EXPLICIT_REGEX = /(expli(cit){0,1}|dirt(y){0,1})/i
const HAS_PRODUCED_REGEX = /prod(\.|(u|uc(e(r|d)|tion)){0,1}){0,1}\b/i

const styleGuide = {
  song_name: {
    featuring: HAS_FEATURING_REGEX,
    explicit: HAS_EXPLICIT_REGEX,
    produced: HAS_PRODUCED_REGEX,
  },
  artist_name: {
    featuring: HAS_FEATURING_REGEX,
    '.': /\./,
    ',': /\,/,
    '/': /\//,
    '&': /\s\&\s/,
    '+': /\s\+\s/,
    '-': /\s\-\s/,
    and: /\sand\s/i,
    meets: /\smeets\s/i,
    versus: /\sv(s|ersus)\s/i,
  },
}

export default function styleGuideCharacterValidator(
  value,
  attributeToValidate
) {
  const styles = styleGuide[attributeToValidate]
  const warning = Object.entries(styles).find(([_warning, regex]) => {
    return regex.test(value)
  })
  if (warning) {
    return warning[0]
  }
}
