export default function reduceSongStartTimes(songStartTimesState, action) {
  return {
    ...songStartTimesState,
    ...action.songStartTimes,
  }
}
