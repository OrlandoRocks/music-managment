import isEquivalent from './isEquivalent'

export default function songwritersEquivalent(songwriters, otherSongwriters) {
  let equivalent
  if (songwriters.length !== otherSongwriters.length) {
    return false
  }
  songwriters.forEach((songwriter, index) => {
    equivalent = isEquivalent(songwriter, otherSongwriters[index], [
      'creative_id',
      'uuid',
    ])
  })
  return equivalent
}
