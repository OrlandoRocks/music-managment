import toSentenceList from './toSentenceList'
import {
  ALL_UPPERCASE_CHARS,
  ALL_LOWERCASE_CHARS,
  ALL_ALPHA_CHARS,
} from './../../utils/regexLibrary'

export default function songTitleizer(songData) {
  let trimmedTitle = songData.name.trim().replace(/ +(?= )/g, '')
  let title = capitalizedTitle(scrubFeaturing(trimmedTitle))
  let featuringArtists = songData.artists
    .filter(
      (artist) =>
        (artist.credit === 'featuring' || artist.credit === 'with') &&
        artist.artist_name
    )
    .map((artist) => artist.artist_name)

  let delimiter = ['(', ')']

  if (title.includes('(')) {
    delimiter = ['[', ']']
  }

  if (featuringArtists.length !== 0) {
    return `${title} ${delimiter[0]}feat. ${toSentenceList(featuringArtists)}${
      delimiter[1]
    }`
  }

  return title
}

function scrubFeaturing(name) {
  return (
    name.split(
      /\s*[\[(]\s*(f(?:ea)?t|with)(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i
    )[0] || ''
  )
}

const articleList = ['a', 'an', 'da', 'n', 'the', 'tha']
const conjunctionList = ['and', 'but', 'nor', 'or']
const otherList = ['as', 'so', 'vs', 'yet']
const prepositionList = [
  'at',
  'by',
  'for',
  'fo',
  'from',
  'in',
  'into',
  'of',
  'off',
  'on',
  'onto',
  'out',
  'over',
  'to',
  'up',
  'with',
]

const exclusionList = [
  ...articleList,
  ...conjunctionList,
  ...otherList,
  ...prepositionList,
]

const commonUppercasedWords = [
  'TV',
  'MC',
  'DJ',
  'EP',
  'CIA',
  'FBI',
  'IRS',
  'OK',
  'WWE',
  'BPM',
  'AJ',
  'JJ',
  'BJ',
  'MVP',
  'VP',
  'IOU',
  'OMG',
  'NSFW',
  'LOL',
]

const romanRegex = /^(?:M{0,3})(?:D?C{0,3}|C[DM])(?:L?X{0,3}|X[LC])(?:V?I{0,3}|I[VX])$/

const mixCasedRegex = new RegExp(
  `^[${ALL_UPPERCASE_CHARS}]*[${ALL_LOWERCASE_CHARS}]+[${ALL_UPPERCASE_CHARS}]`
)

const wordBoundaryMatchRegex = new RegExp(
  `[${ALL_ALPHA_CHARS}]+|\\W|\\d`,
  'gmi'
)

const noLettersRegex = /^[\d\W]+$/

const cyrillicRegex = /[а-яА-ЯЁё]/

function isMixCasedWord(word) {
  return mixCasedRegex.test(word)
}

function capitalizedTitle(title) {
  if (cyrillicRegex.test(title)) {
    return capitalize(title)
  }

  if (noLettersRegex.test(title)) {
    return title
  }

  if (!wordBoundaryMatchRegex.test(title)) {
    return title
  }

  const titleArray = title.match(wordBoundaryMatchRegex)

  if (!titleArray) {
    return title
  }

  const titleLength = titleArray.length

  return titleArray.reduce((accumulator, word, index) => {
    const previousWord = titleArray[index - 1]
    const nextWord = titleArray[index + 1]

    return (
      accumulator +
      capitalizeWord(word, index, titleLength, nextWord, previousWord)
    )
  }, '')
}

function needsApostropheFormatting(word, index, previousWord) {
  if (!previousWord) {
    return false
  }

  const thisWordHasApostrophe = word.match(/[’']/)
  const previousWordHasApostrophe = previousWord.match(/[’']/)
  const isFirstWordAfterApostrophe = index - 1 === 0
  return (
    !thisWordHasApostrophe &&
    previousWordHasApostrophe &&
    !isFirstWordAfterApostrophe
  )
}

function capitalizeWord(word, index, titleLength, nextWord, previousWord) {
  if (isMixCasedWord(word)) {
    return word
  }

  if (needsApostropheFormatting(word, index, previousWord)) {
    return word.toLowerCase()
  }

  if (commonUppercasedWords.includes(word.toUpperCase())) {
    return word.toUpperCase()
  }

  if (isRomanNumeral(word)) {
    return word.toUpperCase()
  }

  if (index === 0 || index === titleLength - 1) {
    return capitalize(word)
  }

  if (!exclusionList.includes(word.toLowerCase())) {
    return capitalize(word)
  }

  return word.toLowerCase()
}

function isRomanNumeral(word) {
  return romanRegex.test(word.toUpperCase())
}

function capitalize(word) {
  return word.charAt(0).toUpperCase() + word.slice(1)
}
