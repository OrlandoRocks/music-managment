import React from 'react'
import PropTypes from 'prop-types'

class PlayButton extends React.Component {
  static propTypes = {
    song: PropTypes.object.isRequired,
    playSong: PropTypes.func,
    pauseSong: PropTypes.func,
  }

  state = {
    currentTime: 0,
  }

  componentWillUnmount() {
    this.audio = null
  }

  componentDidUpdate(prevProps) {
    if (prevProps.song.isPlaying && !this.props.song.isPlaying) {
      this.audio.pause()
    }
  }

  handlePlay = () => {
    const { song, pauseSong, playSong } = this.props
    if (!this.audio) {
      this.audio = new Audio(song.data.asset_url)
    }
    if (this.props.song.isPlaying) {
      pauseSong(song)
      this.audio.pause()
    } else {
      playSong(song)
      this.audio.play()
      this.audio.onended = () => pauseSong(song)
    }
  }

  render() {
    const { isPlaying } = this.props.song
    const title = isPlaying
      ? window.translations.songs_app.pause
      : window.translations.songs_app.play

    return (
      <button key="play" onClick={this.handlePlay} title={title}>
        {isPlaying ? (
          <i className="fa fa-pause tracklist-item-icon" />
        ) : (
          <i className="fa fa-play tracklist-item-icon" />
        )}
      </button>
    )
  }
}

export default PlayButton
