import React from 'react'
import PropTypes from 'prop-types'

const RemoveSongButton = ({
  uuid,
  disabled,
  display,
  onClick,
  children,
  className,
  displayTooltip,
}) => {
  return (
    <button
      disabled={disabled}
      id={`remove_song_${uuid}`}
      title={displayTooltip && window.translations.songs_app.delete}
      style={{ display: display || 'inline-block' }}
      className={`remove-song ${className}`}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

RemoveSongButton.propTypes = {
  display: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  uuid: PropTypes.string.isRequired,
  className: PropTypes.string,
}

export default RemoveSongButton
