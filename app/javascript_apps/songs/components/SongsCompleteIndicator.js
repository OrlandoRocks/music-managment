import React from 'react'
import { connect } from 'react-redux'
import songsCompleteCount from '../utils/songsCompleteCount'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../shared/Tooltip'

const SongsCompleteTooltip = (
  <TooltipContainer>
    <TooltipTitle>{window.translations.songs_app.songs_complete}</TooltipTitle>
    <TooltipColumn>
      {window.translations.songs_app.songs_complete_tooltip_text}
    </TooltipColumn>
  </TooltipContainer>
)

class SongsCompleteIndicator extends React.Component {
  render() {
    const { completedSongs, totalSongs } = this.props
    return (
      <div className="song-complete-indicator left">
        {completedSongs} / {totalSongs} &nbsp;
        {window.translations.songs_app.completed} &nbsp;
        <Tooltip>{SongsCompleteTooltip}</Tooltip>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  return {
    totalSongs: Object.keys(storeState.songs).length,
    completedSongs: songsCompleteCount(
      storeState.songs,
      storeState.creatives,
      storeState.songwriters,
      storeState.songwriterRoleId,
      storeState.songStartTimes
    ),
  }
}

export default connect(mapStateToProps)(SongsCompleteIndicator)
