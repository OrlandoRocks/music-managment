import React from 'react'
import PropTypes from 'prop-types'

class Message extends React.Component {
  static propTypes = {
    level: PropTypes.string.isRequired,
    onToggleMessage: PropTypes.func,
  }

  componentDidMount() {
    const { timed, onToggleMessage } = this.props
    if (timed) {
      this.timeout = setTimeout(onToggleMessage, 5000)
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  render() {
    return (
      <div
        className={`songwriter-message songwriter-message-${this.props.level}`}
      >
        <span className="songwriter-messagetext">{this.props.children}</span>
      </div>
    )
  }
}

export default Message
