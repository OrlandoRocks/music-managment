import React from 'react'
import PropTypes from 'prop-types'

const UploadSongButton = ({
  children,
  className,
  onSongUpload,
  uuid,
  disabled,
  assetUrl,
  displayTooltip,
  isAtmos,
}) => {
  const title = assetUrl
    ? window.translations.songs_app.replace_audio
    : window.translations.songs_app.upload_audio

  const fileTypes = isAtmos ? '.wav' : '.wav,.mp3,.flac'
  return [
    <label
      key="label"
      htmlFor={`song_upload_${uuid}`}
      title={displayTooltip && title}
      className={className || ''}
      onChange={onSongUpload}
    >
      {children}
    </label>,
    <input
      key="input"
      id={`song_upload_${uuid}`}
      className="song-file-upload-input"
      onChange={onSongUpload}
      type={`${disabled ? '' : 'file'}`}
      accept={fileTypes}
      value=""
    />,
  ]
}

UploadSongButton.propTypes = {
  className: PropTypes.string,
  onSongUpload: PropTypes.func.isRequired,
  uuid: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  isAtmos: PropTypes.bool,
}

export default UploadSongButton
