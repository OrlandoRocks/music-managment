import React from 'react'
import SongForm from './SongForm'
import TrackListItem from './TrackListItem'
import * as songActions from '../actions/songActions'
import { reorderSongs } from '../actions/albumActions'
import { removeSongFromStore } from '../actions/songActions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { arrayMove } from './sortable'
import SortableSongs from './SortableSongs'
import SongsHeader from './SongsHeader'
import SongUploadInfo from './SongUploadInfo'
import isEmpty from 'lodash/isEmpty'

class SongsContainer extends React.Component {
  constructor(props) {
    super(props)

    // This variable is set to ensure the spatial audio modal doesn't pop up all the time
    const spatialAudioEducationModalEnabled = !this.props.songs.some(
      (song) => song && song.data && song.data.spatial_audio_asset_url
    )

    this.state = {
      isReordering: false,
      shouldShowError: false,
      songs: this.props.songs,
      spatialAudioEducationModalEnabled: spatialAudioEducationModalEnabled,
      spatialAudioModalNotificationEnabled: false,
    }
  }

  UNSAFE_componentWillMount() {
    if (this.props.songs.length === 0) {
      this.props.actions.createInitialSong()
    }

    if (this.props.album.album_type === 'Single') {
      const singleSong = this.props.songs[0]
      if (singleSong.songwriters.length === 0) {
        this.props.actions.changeCurrentSong(singleSong.uuid)
      }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!(this.props.album.isLoading || nextProps.album.isLoading)) {
      this.setState({ songs: nextProps.songs })
    }
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState({
      songs: arrayMove(this.state.songs, oldIndex, newIndex),
    })
  }

  editSong = (song) => {
    this.props.actions.changeCurrentSong(song.uuid)
  }

  cancelEditSong = () => {
    this.props.actions.clearCurrentSong()
  }

  addSong = () => {
    const trackNum = this.state.songs.length + 1
    this.props.actions.addSong(this.props.defaultSong, trackNum)
  }

  songSelected(song) {
    const { currentSong } = this.props
    return currentSong && currentSong.uuid === song.uuid
  }

  deactivatespatialAudioEducationModalState = () => {
    this.setState({ spatialAudioEducationModalEnabled: false })
  }

  deactivateSpatialAudioModalNotificationState = () => {
    this.setState({ spatialAudioModalNotificationEnabled: false })
  }

  activateSpatialAudioModalNotificationState = () => {
    this.setState({ spatialAudioModalNotificationEnabled: true })
  }

  handleReorderClick = () => {
    const album_id = this.props.album.id
    const { songs } = this.props
    const { removeSongFromStore } = this.props.actions
    jQuery.ajax({
      url: `/api/backstage/albums/${album_id}/fetch_song_ids`,
      type: 'get',
      dataType: 'json',
      contentType: 'application/json',
      success: function (data) {
        songs.forEach((song) => {
          if (!data['song_ids'].includes(song.data.id)) {
            removeSongFromStore(song)
          }
        })
      },
    })
    this.setState({ isReordering: true })
  }

  handleReorderCancel = (e) => {
    e.preventDefault()
    this.setState({ isReordering: false, songs: this.props.songs })
  }

  handleReorderSave = (e) => {
    e.preventDefault()
    this.props.actions.reorderSongs(this.state.songs)
    this.setState({ isReordering: false })
  }

  handleSortEnd = ({ oldIndex, newIndex }) => {
    let songs = arrayMove(this.state.songs, oldIndex, newIndex).map(
      (song, index) => {
        return {
          ...song,
          data: {
            ...song.data,
            track_number: index + 1,
          },
        }
      }
    )

    this.setState({ songs })
  }

  songForm(song, index) {
    return (
      <SongForm
        index={index}
        key={song.uuid}
        cancelEditSong={this.cancelEditSong}
        trackNum={index + 1}
      />
    )
  }

  trackListItem(song, index) {
    const {
      spatialAudioEducationModalEnabled,
      spatialAudioModalNotificationEnabled,
    } = this.state

    return (
      <TrackListItem
        key={song.uuid}
        songUuid={song.uuid}
        artistIds={song.data.artists}
        index={index}
        onEditSong={this.editSong}
        onRemoveSong={this.props.actions.removeSong}
        onRemoveSongAtmosAsset={this.props.actions.removeSongAtmosAsset}
        onFileUpload={this.props.actions.uploadSong}
        onSpatialAudioFileUpload={this.props.actions.uploadSpatialAudioSong}
        setSpatialUploadActive={this.props.actions.setSpatialUploadActive}
        setSpatialUploadInactive={this.props.actions.setSpatialUploadInactive}
        onCancelUpload={this.props.actions.cancelUpload}
        showProgressBar={true}
        spatialAudioEnabled={this.props.spatialAudioEnabled}
        deactivatespatialAudioEducationModalState={
          this.deactivatespatialAudioEducationModalState
        }
        spatialAudioEducationModalEnabled={spatialAudioEducationModalEnabled}
        deactivateSpatialAudioModalNotificationState={
          this.deactivateSpatialAudioModalNotificationState
        }
        activateSpatialAudioModalNotificationState={
          this.activateSpatialAudioModalNotificationState
        }
        spatialAudioModalNotificationEnabled={
          spatialAudioModalNotificationEnabled && song.isUploading
        }
        dolbyAtmosArticleLink={this.props.dolbyAtmosArticleLink}
        coverSongMetadataEnabled={this.props.coverSongMetadataEnabled}
        coverSongArticleLink={this.props.coverSongArticleLink}
        planUpgradeLink={this.props.planUpgradeLink}
      />
    )
  }

  isNewSong(song) {
    return isEmpty(song.data.id)
  }

  trackShow(song, index) {
    if (this.songSelected(song)) {
      let { album, currentSong } = this.props
      if (
        song === currentSong &&
        this.isNewSong(currentSong) &&
        album.track_limit_reached
      ) {
        return null
      }
      return this.songForm(song, index)
    } else {
      return this.trackListItem(song, index)
    }
  }

  songsForRender() {
    let { currentSong, songs } = this.props
    if (currentSong && !songs.find((song) => song.uuid === currentSong.uuid)) {
      songs = [...songs, currentSong]
    }
    return songs
  }

  trackList() {
    const songs = this.songsForRender()
    const trackListItems = songs.map((song, index) =>
      this.trackShow(song, index)
    )
    return (
      <div className="song-list" key="song_list">
        {trackListItems}
      </div>
    )
  }

  render() {
    const { isReordering } = this.state

    let defaultComponents = [<SongUploadInfo key="song_upload_info" />]

    if (this.props.album.finalized) {
      return [...defaultComponents, this.trackList()]
    }

    if (isReordering) {
      return [
        ...defaultComponents,
        <SongsHeader
          isReordering={isReordering}
          key="songs_header"
          onReorderSave={this.handleReorderSave}
          onReorderCancel={this.handleReorderCancel}
        />,
        <SortableSongs
          key="sortable_songs"
          songs={this.state.songs}
          onSortEnd={this.handleSortEnd}
        />,
      ]
    }

    return [
      ...defaultComponents,
      <SongsHeader
        isReordering={isReordering}
        key="songs_header"
        onReorderClick={this.handleReorderClick}
        onAddSong={this.addSong}
      />,
      this.trackList(),
    ]
  }
}

function mapStateToProps(storeState) {
  const {
    currentSong,
    defaultSong,
    album,
    copyrightsFormEnabled,
    trackLevelLanguageEnabled,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    songStartTimesFormEnabled,
    songStartTimeStoreOptions,
    spatialAudioEnabled,
    dolbyAtmosArticleLink,
    coverSongMetadataEnabled,
    coverSongArticleLink,
    planUpgradeLink,
  } = storeState

  let songs = Object.values(storeState.songs).sort(
    (song1, song2) => song1.data.track_number - song2.data.track_number
  )
  return {
    songs,
    currentSong,
    defaultSong,
    album,
    copyrightsFormEnabled,
    trackLevelLanguageEnabled,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    songStartTimesFormEnabled,
    songStartTimeStoreOptions,
    spatialAudioEnabled,
    dolbyAtmosArticleLink,
    coverSongMetadataEnabled,
    coverSongArticleLink,
    planUpgradeLink,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { ...songActions, reorderSongs, removeSongFromStore },
      dispatch
    ),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SongsContainer)
