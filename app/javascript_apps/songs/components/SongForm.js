import React from 'react'
import Title from './sections/Title'
import ExplicitRadio from './sections/ExplicitRadio'
import ExplicitCheckBox from './sections/ExplicitCheckBox'
import CleanVersionRadio from './sections/CleanVersionRadio'
import OptionalIsrc from './sections/OptionalIsrc'
import Lyrics from './sections/Lyrics'
import ArtistsForm from './sections/ArtistsForm'
import SongStartTimesForm from './sections/SongStartTimesForm'
import Language from './sections/Language'
import TrackListItem from './TrackListItem'
import AdditionalSongInformation from './sections/AdditionalSongInformation'
import PreviouslyReleased from './sections/PreviouslyReleased'
import RemoveSongButton from './RemoveSongButton'
import IsInstrumental from './sections/IsInstrumental'
import * as songActions from '../actions/songActions'
import * as artistActions from '../actions/artistActions'
import artistsEquivalent from '../utils/artistsEquivalent'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import SongwriterForm from './sections/SongwriterForm'
import songwritersEquivalent from '../utils/songwritersEquivalent'
import { get } from 'lodash'
import SimpleCopyrights from './sections/SimpleCopyrights'
import CoverSongMetadataForm from './sections/CoverSongMetadataForm'

class SongForm extends React.Component {
  state = {
    seenError: false,
  }

  componentDidUpdate(prevProps) {
    const { currentSong } = this.props
    const prevSong = prevProps.currentSong
    if (prevSong.uuid === currentSong.uuid) {
      const hasError = this.songHasErrors(currentSong)
      if (hasError && !this.state.seenError) {
        setTimeout(() => {
          this.setState({ seenError: true })
          this.scrollToError()
        }, 20)
      }
    }
  }

  songHasErrors(song) {
    return (
      song &&
      Object.keys(song.errors).length > 0 &&
      Object.keys(song.errors)[0] !== 'asset_url'
    )
  }

  scrollToError() {
    const element = document.getElementsByClassName(`missing`)[0]
    element && element.scrollIntoView({ behavior: 'smooth' })
  }

  updateAttributes = (songAttributes) => {
    this.props.actions.updateCurrentSong(songAttributes)
  }

  addArtist = (e) => {
    e && e.preventDefault()
    this.props.actions.addArtist()
  }

  removeArtist = (artistUuid) => {
    const {
      album: { is_various: isVarious },
      currentSong: {
        data: { artists },
      },
    } = this.props
    if (artists.length === 1 && isVarious) {
      return
    }
    this.props.actions.removeArtist(artistUuid)
  }

  editArtist = (attribute, value, artistUuid) => {
    if (attribute === 'artist_name') {
      this.props.actions.updateArtistName(
        artistUuid,
        value,
        this.props.currentCreatives
      )
    } else {
      this.props.actions.updateArtist(attribute, value, artistUuid)
    }
  }

  copyArtistsFromPrevious = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.props.actions.copyArtistsFromPrevious(this.props.previousSongArtists)
  }

  copySongwritersFromPrevious = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.props.actions.copySongwritersFromPrevious(
      this.props.previousSongSongwriters
    )
  }

  handleRemoveSong = (e) => {
    e.preventDefault()
    e.stopPropagation()
    const { currentSong } = this.props
    if (currentSong.data.id) {
      this.props.actions.removeSong(this.props.currentSong)
    } else {
      this.props.cancelEditSong()
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({ seenError: false })
    const {
      currentSong: {
        data: { id: songId },
      },
    } = this.props
    if (!songId) {
      this.props.actions.createSong(this.props.currentSong)
    } else {
      this.props.actions.updateSong(this.props.currentSong)
    }
  }

  creativeHasDuplicateErrors() {
    const { currentCreatives } = this.props
    let hasErrors = false

    for (const id in currentCreatives) {
      if (
        get(currentCreatives, `[${id}].styleGuideWarnings.artist_name`) ===
        'duplicate_artist_name'
      ) {
        hasErrors = true
      }
    }

    return hasErrors
  }

  buttonEnabled() {
    const { currentSong } = this.props
    return (
      currentSong.data.name != undefined &&
      currentSong.data.name.length !== 0 &&
      !currentSong.isLoading &&
      !currentSong.isUploading
    )
  }

  albumIsSingle() {
    return this.props.album.album_type === 'Single'
  }

  albumIsAlbum() {
    return this.props.album.album_type === 'Album'
  }

  saveButtonText() {
    const {
      currentSong: {
        data: { id },
      },
    } = this.props
    let save_type = id ? 'save' : 'save_and_add_another'
    return window.translations.songs_app[save_type]
  }

  songTitleRequiredText() {
    return window.translations.songs_app.song_title_required
  }

  canCopyPreviousArtists() {
    const { currentSong, previousSongArtists, currentCreatives } = this.props
    const { data } = currentSong
    const artists = data.artists.map((artistId) => currentCreatives[artistId])

    if (this.isFirstSong()) {
      return false
    }

    if (!previousSongArtists) {
      return false
    }
    return !artistsEquivalent(artists, previousSongArtists)
  }

  canCopyPreviousSongwriters() {
    const {
      currentSong,
      previousSongSongwriters,
      currentSongwriters,
    } = this.props
    const { songwriters } = currentSong
    const artists = songwriters.map((artistId) => currentSongwriters[artistId])

    if (this.isFirstSong()) {
      return false
    }

    if (!previousSongSongwriters) {
      return false
    }
    return !songwritersEquivalent(artists, previousSongSongwriters)
  }

  isFirstSong() {
    const { trackNum } = this.props

    return trackNum && trackNum === 1
  }

  render() {
    const {
      currentSong,
      album,
      trackNum,
      copyrightsFormEnabled,
      trackLevelLanguageEnabled,
      explicitRadioEnabled,
      explicitFieldsEnabled,
      cleanVersionRadioEnabled,
      songStartTimesFormEnabled,
      songStartTimeStoreOptions,
      coverSongMetadataEnabled,
      coverSongArticleLink,
    } = this.props
    const isFirstSong = this.isFirstSong()
    const {
      name,
      explicit,
      optional_isrc,
      language_code_id,
      lyrics,
      previously_released_at,
      instrumental,
      clean_version,
    } = currentSong.data
    const { errors, uuid, isLoading } = currentSong
    const duplicateCreative = this.creativeHasDuplicateErrors()
    const { album_type } = album
    const isAlbum = album_type === 'Album'
    const buttonEnabled = this.buttonEnabled()
    const is_previously_released =
      currentSong.data.is_previously_released ||
      !!previously_released_at ||
      !!album.previously_released_at
    const showsRemoveSongButton = this.albumIsAlbum()
      ? album.is_editable
      : false
    const instrumentalTrack = instrumental || album.genre === 'Instrumental'
    const removeSongBtn = (
      <RemoveSongButton
        uuid={uuid}
        disabled={!buttonEnabled}
        className="form-remove-song"
        onClick={this.handleRemoveSong}
      >
        <i className="fa fa-remove" />
        {window.translations.songs_app.delete_track}
      </RemoveSongButton>
    )

    return [
      <TrackListItem
        key="track_list_item"
        songUuid={currentSong.uuid}
        artistIds={currentSong.data.artists}
        showsBottomBar={false}
        customClass={'form-list-item'}
        isCurrentSong={true}
        trackNum={trackNum}
      />,
      <div key="song_form">
        <form
          onSubmit={this.handleSubmit}
          className="song-form"
          id={`song_form_${uuid}`}
        >
          <Title
            name={name}
            onAttributeUpdate={this.updateAttributes}
            errors={errors}
          />
          <SongwriterForm
            isFirstSong={isFirstSong}
            canCopyPreviousSongwriters={this.canCopyPreviousSongwriters()}
            onCopySongwriterFromPrevious={this.copySongwritersFromPrevious}
          />
          <ArtistsForm
            onAddArtist={this.addArtist}
            onEditArtist={this.editArtist}
            onRemoveArtist={this.removeArtist}
            canCopyPreviousArtists={this.canCopyPreviousArtists()}
            onCopyArtistsFromPrevious={this.copyArtistsFromPrevious}
            isFirstSong={isFirstSong}
          />
          <SimpleCopyrights
            errors={errors}
            simpleCopyrightsFormEnabled={copyrightsFormEnabled}
          />
          <CoverSongMetadataForm
            errors={errors}
            coverSongMetadataEnabled={coverSongMetadataEnabled}
            coverSongArticleLink={coverSongArticleLink}
          />
          <AdditionalSongInformation>
            {explicitFieldsEnabled &&
              (explicitRadioEnabled ? (
                <ExplicitRadio
                  isExplicit={explicit}
                  errors={errors}
                  onAttributeUpdate={this.updateAttributes}
                />
              ) : (
                <ExplicitCheckBox
                  isExplicit={explicit}
                  onAttributeUpdate={this.updateAttributes}
                />
              ))}
            {cleanVersionRadioEnabled && explicit == false && (
              <CleanVersionRadio
                isCleanVersion={clean_version}
                errors={errors}
                onAttributeUpdate={this.updateAttributes}
              />
            )}
            {album.genre !== 'Instrumental' && (
              <IsInstrumental
                isInstrumental={instrumentalTrack}
                onAttributeUpdate={this.updateAttributes}
              />
            )}
            <PreviouslyReleased
              isPreviouslyReleased={is_previously_released}
              previouslyReleasedDate={previously_released_at}
              albumPreviouslyReleasedDate={album.previously_released_at}
              albumIsSingle={this.albumIsSingle()}
              errors={errors}
              onAttributeUpdate={this.updateAttributes}
            />
            <OptionalIsrc
              optionalIsrc={optional_isrc}
              onAttributeUpdate={this.updateAttributes}
              albumType={album_type}
              previouslyReleased={album.previously_released_at}
              isEditable={album.is_editable}
              errors={errors}
            />
            {trackLevelLanguageEnabled && !instrumentalTrack && (
              <Language
                language_code_id={language_code_id}
                onAttributeUpdate={this.updateAttributes}
                errors={errors}
              />
            )}
            {!instrumentalTrack && (
              <Lyrics
                errors={errors}
                lyrics={lyrics}
                onAttributeUpdate={this.updateAttributes}
              />
            )}
            {songStartTimesFormEnabled &&
              songStartTimeStoreOptions &&
              songStartTimeStoreOptions.map((storeId) => {
                return <SongStartTimesForm key={storeId} storeId={storeId} />
              })}
          </AdditionalSongInformation>
          <div className="bottom-form-buttons">
            <div className="action-button hide-for-mobile">
              {showsRemoveSongButton && removeSongBtn}
            </div>
            <div className="action-button show-only-mobile">
              {showsRemoveSongButton && removeSongBtn}
            </div>
            {isAlbum && (
              <div className={'cancel-song-wrapper'}>
                <button
                  onClick={this.props.cancelEditSong}
                  className="cancel-song"
                >
                  {window.translations.songs_app.done_adding_songs}
                </button>
              </div>
            )}
            <div className="action-button submit-button">
              <button
                type="submit"
                className="submit-song"
                disabled={isLoading || duplicateCreative}
              >
                {isLoading ? (
                  <span>
                    <i className="fa fa-spinner fa-pulse" />{' '}
                    {window.translations.songs_app.saving}
                  </span>
                ) : (
                  this.saveButtonText()
                )}
              </button>
            </div>
          </div>
          <div className="song-title-required row">
            <div className="twelve columns">
              <p>{this.songTitleRequiredText()}</p>
            </div>
          </div>
        </form>
      </div>,
    ]
  }
}

function mapStateToProps(storeState, componentProps) {
  const {
    currentSong,
    songs,
    album,
    defaultSong,
    currentCreatives,
    currentSongwriters,
    currentSongStartTimes,
    creatives,
    songwriters,
    copyrightsFormEnabled,
    trackLevelLanguageEnabled,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    songStartTimesFormEnabled,
    songStartTimeStoreOptions,
    coverSongMetadataEnabled,
    coverSongArticleLink,
  } = storeState

  const previousSong = Object.values(songs).sort(
    (song1, song2) => song1.data.track_number - song2.data.track_number
  )[componentProps.index - 1]
  const previousSongArtistIds = previousSong && previousSong.data.artists
  const previousSongSongwriterIds = previousSong && previousSong.songwriters

  let previousSongArtists, previousSongSongwriters

  if (previousSongArtistIds) {
    previousSongArtists = previousSongArtistIds.map(
      (artistId) => creatives[artistId]
    )
  }

  if (previousSongSongwriterIds) {
    previousSongSongwriters = previousSongSongwriterIds.map(
      (songwriterId) => songwriters[songwriterId]
    )
  }

  return {
    currentSong,
    currentCreatives,
    currentSongwriters,
    currentSongStartTimes,
    album,
    defaultSong,
    previousSongArtists,
    previousSongSongwriters,
    songwriterRoleId: storeState.songwriterRoleId,
    copyrightsFormEnabled,
    trackLevelLanguageEnabled,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    songStartTimesFormEnabled,
    songStartTimeStoreOptions,
    coverSongMetadataEnabled,
    coverSongArticleLink,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...songActions, ...artistActions }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SongForm)
