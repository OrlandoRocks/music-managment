import React from 'react'
import { connect } from 'react-redux'
import SongsCompleteIndicator from './SongsCompleteIndicator'
import PropTypes from 'prop-types'

class SongsHeader extends React.Component {
  static propTypes = {
    isReordering: PropTypes.bool,
    onReorderSave: PropTypes.func,
    onReorderCancel: PropTypes.func,
    onReorderClick: PropTypes.func,
    onAddSong: PropTypes.func,
  }

  static defaultProps = {
    isReordering: false,
  }

  reorderButtons() {
    const {
      isReordering,
      onReorderSave,
      onReorderCancel,
      onReorderClick,
      albumIsLoading,
      songIsDeleting,
      songIsLoading,
    } = this.props

    return [
      isReordering && (
        <button
          className="right reorder-save"
          key="reorder_save"
          onClick={onReorderSave}
        >
          {window.translations.songs_app.save_song_order}
        </button>
      ),
      isReordering && (
        <button
          className="right reorder-cancel"
          key="reorder_cancel"
          onClick={onReorderCancel}
        >
          {window.translations.songs_app.cancel}
        </button>
      ),
      !isReordering && (
        <button
          className="right edit-song-order"
          key="reorder"
          onClick={onReorderClick}
          disabled={songIsDeleting || songIsLoading}
        >
          <i className="fa fa-sort-amount-desc" disabled={songIsDeleting} />
          {window.translations.songs_app.edit_song_order}
        </button>
      ),
      albumIsLoading && !songIsDeleting && (
        <button
          disabled={true}
          className="right reorder-save"
          key="reorder_save"
          onClick={onReorderSave}
        >
          <span>
            <i className="fa fa-spinner fa-pulse" />{' '}
            {window.translations.songs_app.saving}
          </span>
        </button>
      ),
    ].filter((button) => button !== null)
  }

  albumIsSingle() {
    return this.props.album.album_type === 'Single'
  }

  showsReorderButtons() {
    const { album } = this.props
    return !this.albumIsSingle() && album.is_editable
  }

  showsAddButton() {
    const { album, isReordering } = this.props
    return (
      !this.albumIsSingle() &&
      !album.track_limit_reached &&
      album.is_editable &&
      !isReordering
    )
  }

  render() {
    let { songIsDeleting, songIsLoading } = this.props

    return (
      <div className="songs_top-bar" key="top_bar">
        <SongsCompleteIndicator key="songs_completion" />
        {this.showsReorderButtons() && this.reorderButtons()}
        {this.showsAddButton() && (
          <button
            disabled={songIsDeleting || songIsLoading}
            className="right add-song"
            onClick={this.props.onAddSong}
          >
            <i className="fa fa-plus" />
            {window.translations.songs_app.add_songs}
          </button>
        )}
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const { album } = storeState
  return {
    albumIsLoading: album.isLoading,
    songIsDeleting: album.songIsDeleting,
    songIsLoading: album.songIsLoading,
    album,
  }
}

export default connect(mapStateToProps)(SongsHeader)
