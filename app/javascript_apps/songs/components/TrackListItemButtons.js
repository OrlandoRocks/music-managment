import React from 'react'
import PropTypes from 'prop-types'
import UploadSongButton from './UploadSongButton'
import PlayButton from './PlayButton'
import RemoveSongButton from './RemoveSongButton'
import { SortableHandle } from './sortable'
import assetFilenameDisplay from '../utils/assetFilenameDisplay'
import SongFormModal from './SongFormModal'

const SongDragHandle = SortableHandle(() => (
  <div className="right">
    <i className="fa fa-bars" />{' '}
  </div>
))

function AssetUploadBtn(props) {
  let { assetUploadText, assetUploadBtnClass, disabled, titleText } = props

  let btnClass = assetUploadBtnClass || 'asset-upload-btn'
  let disabledClass = disabled ? 'disabled' : ''
  let assetUploadBtnTextClass = `${btnClass}-text`

  return (
    <div title={titleText} className={`${btnClass} ${disabledClass}`}>
      <div className={assetUploadBtnTextClass}>{assetUploadText}</div>
    </div>
  )
}

function hasErrors(errors) {
  return (
    errors.constructor === Object &&
    Object.values(errors).filter(Boolean).length !== 0
  )
}

function renderAssetUploadText(props) {
  let {
    isUploading,
    hasErrors,
    errors,
    asset_url,
    uploadText,
    replacementText,
  } = props

  let assetUploadText = ''
  if (isUploading) {
    assetUploadText = (
      <i
        className="fa fa-spinner fa-spin spacer cancel-button-spinner"
        key="spinner"
      />
    )
  } else if (hasErrors(errors) && !asset_url) {
    assetUploadText = uploadText
  } else if (asset_url) {
    assetUploadText = replacementText
  } else {
    assetUploadText = uploadText
  }

  return assetUploadText
}

function renderAssetUploadBtn(props) {
  let {
    onEditSong,
    albumIsLoading,
    currentlyUploading,
    isHovering,
    isLoading,
    songUuid,
    handleFileUpload,
    asset_url,
    assetUploadText,
    assetUploadBtnClass,
    disabled,
    titleText,
    isAtmos,
  } = props

  return onEditSong && !albumIsLoading && !currentlyUploading ? (
    <span
      key="file-upload"
      style={{
        display: isHovering && !isLoading ? 'inline-block' : 'none',
      }}
    >
      {!disabled ? (
        <UploadSongButton
          className="song-file-upload-button"
          uuid={songUuid}
          onSongUpload={handleFileUpload}
          assetUrl={asset_url}
          displayTooltip={true}
          isAtmos={isAtmos}
        >
          <AssetUploadBtn
            assetUploadText={assetUploadText}
            assetUploadBtnClass={assetUploadBtnClass}
          />
        </UploadSongButton>
      ) : (
        <AssetUploadBtn
          assetUploadText={assetUploadText}
          assetUploadBtnClass={assetUploadBtnClass}
          disabled={disabled}
          titleText={titleText}
        />
      )}
    </span>
  ) : null
}
function renderCancelUploadNotificationButton(props) {
  let {
    handleCancelUpload,
    cancellationText,
    cancellationBtnClass,
    spatialAudioModalNotificationEnabled,
  } = props
  let cancelUploadButton = spatialAudioModalNotificationEnabled ? (
    <button
      key="cancel-button"
      onClick={handleCancelUpload}
      className="cancel_notification_modal_button"
    >
      <AssetUploadBtn
        assetUploadText={cancellationText}
        assetUploadBtnClass={cancellationBtnClass}
      />
    </button>
  ) : null

  return cancelUploadButton
}
function renderCancelUploadBtn(props) {
  let {
    onEditSong,
    albumIsLoading,
    currentlyUploading,
    handleCancelUpload,
    cancellationText,
    cancellationBtnClass,
  } = props

  let cancelUploadButton =
    onEditSong && !albumIsLoading && currentlyUploading ? (
      <button key="cancel-button" onClick={handleCancelUpload}>
        <AssetUploadBtn
          assetUploadText={cancellationText}
          assetUploadBtnClass={cancellationBtnClass}
        />
      </button>
    ) : null

  return cancelUploadButton
}

function renderUploadSpinner(props) {
  let {
    isUploading,
    isCurrentSong,
    spatialAudioModalNotificationEnabled,
    assetCancelUploadNotificationButtonProps,
  } = props

  let uploadSpinner = spatialAudioModalNotificationEnabled ? (
    renderSpatialAudioNotificationUploadModal({
      spatialAudioModalNotificationEnabled,
      assetCancelUploadNotificationButtonProps,
    })
  ) : isUploading && !isCurrentSong ? (
    <i
      className="fa fa-spinner fa-spin spacer tracklist-item-icon"
      key="spinner"
    />
  ) : null

  return uploadSpinner
}

function renderAssetData(props) {
  let {
    isCurrentSong,
    assetUrlError,
    uploadFailedText,
    durationError,
    durationFailureText,
    maxDurationError,
    maxDurationFailureText,
    origFilenameError,
    assetFilename,
    assetFilenameDisplay,
    atmosDurationError,
    atmosDurationFailureText,
    fileTypeError,
    fileTypeFailureText,
  } = props

  let assetData = ''
  if (!isCurrentSong) {
    if (assetUrlError) {
      assetData = (
        <div className="track-list-item-asset-upload-failure">
          {uploadFailedText}
        </div>
      )
    } else if (durationError) {
      assetData = (
        <div className="track-list-item-asset-duration-failure">
          {durationFailureText}
        </div>
      )
    } else if (maxDurationError) {
      assetData = (
        <div className="track-list-item-asset-duration-failure">
          {maxDurationFailureText}
        </div>
      )
    } else if (origFilenameError) {
      assetData = (
        <div className="track-list-item-asset-duration-failure">
          {origFilenameError[0]}
        </div>
      )
    } else if (atmosDurationError) {
      assetData = (
        <div className="track-list-item-asset-duration-failure">
          {atmosDurationFailureText}
        </div>
      )
    } else if (fileTypeError) {
      assetData = (
        <div className="track-list-item-asset-duration-failure">
          {fileTypeFailureText}
        </div>
      )
    } else if (assetFilename) {
      assetData = <div>{assetFilenameDisplay(assetFilename)}</div>
    }
  }

  return assetData
}

function renderSpatialAudioPlanRequiredModal(props) {
  let {
    isHovering,
    isLoading,
    planUpgradeLink,
    onEditSong,
    albumIsLoading,
    currentlyUploading,
    assetUploadText,
    assetUploadBtnClass,
    disabled,
    titleText,
  } = props

  let modalButton = (
    <span
      key="file-upload"
      style={{
        display: isHovering && !isLoading ? 'inline-block' : 'none',
      }}
    >
      {AssetUploadBtn({
        assetUploadText,
        assetUploadBtnClass,
        disabled,
        titleText,
      })}
    </span>
  )

  let upgradeLink = (
    <a href={planUpgradeLink} rel="noopener noreferrer" target="_blank">
      {window.translations.songs_app.upgrade_now}
    </a>
  )

  if (onEditSong && !albumIsLoading && !currentlyUploading) {
    if (disabled) {
      return modalButton
    } else {
      return (
        <div>
          <SongFormModal modalButton={modalButton}>
            <div className="padding-left-50 padding-right-50">
              <div className="text-align-center">
                <div className="margin-bottom-20">
                  <h1>
                    {
                      window.translations.songs_app
                        .dolby_atmos_upgrade_your_plan_header
                    }
                  </h1>
                </div>
                <div className="margin-bottom-20">
                  <div className="font-size-14">
                    <p>
                      {
                        window.translations.songs_app
                          .dolby_atmos_upgrade_your_plan_description
                      }{' '}
                      {upgradeLink}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </SongFormModal>
        </div>
      )
    }
  } else {
    return null
  }
}

function renderspatialAudioEducationModal(props) {
  let { dolbyAtmosArticleLink, assetUploadButtonProps } = props
  let {
    onEditSong,
    albumIsLoading,
    currentlyUploading,
    isHovering,
    isLoading,
    songUuid,
    handleFileUpload,
    asset_url,
    assetUploadText,
    assetUploadBtnClass,
    disabled,
    titleText,
    isAtmos,
  } = assetUploadButtonProps

  let fileUploadButtonForModal = renderAssetUploadBtn(assetUploadButtonProps)

  let modalButton = AssetUploadBtn({
    assetUploadText,
    assetUploadBtnClass,
    disabled,
    titleText,
  })

  const {
    dolby_atmos_article_link_description,
    dolby_atmos_article_link_text,
  } = window.translations.songs_app

  const dolbyAtmosTextWithLink =
    dolby_atmos_article_link_description &&
    dolby_atmos_article_link_description.replace(
      '{{link}}',
      `<a href='${dolbyAtmosArticleLink}' rel='noopener noreferrer' target='_blank'>${dolby_atmos_article_link_text}</a>`
    )

  return (
    <div>
      <SongFormModal modalButton={modalButton}>
        <div className="padding-left-50 padding-right-50">
          <div className="text-align-center">
            <div className="margin-bottom-20">
              <h1>{window.translations.songs_app.dolby_atmos_title}</h1>
            </div>
            <div className="margin-bottom-20">
              <div className="font-size-14">
                <p>
                  {
                    window.translations.songs_app
                      .dolby_atmos_overview_description
                  }
                </p>
              </div>
            </div>
            <div className="margin-bottom-20">
              <div
                className="font-size-14"
                dangerouslySetInnerHTML={{ __html: dolbyAtmosTextWithLink }}
              />
            </div>
            <div className="margin-bottom-20">{fileUploadButtonForModal}</div>
          </div>
        </div>
      </SongFormModal>
    </div>
  )
}

function renderSpatialAudioCompleteModal(props) {
  let { onCloseModal } = props

  let modalButton = <span />

  return (
    <div>
      <SongFormModal
        modalButton={modalButton}
        showModal={true}
        onCloseModal={onCloseModal}
      >
        <div className="padding-left-50 padding-right-50">
          <div className="text-align-center">
            <div className="margin-bottom-20">
              <h1>
                {
                  window.translations.songs_app
                    .dolby_atmos_upload_completion_title
                }
              </h1>
            </div>
            <div className="margin-bottom-20">
              <div className="font-size-14">
                <p>
                  {
                    window.translations.songs_app
                      .dolby_atmos_upload_completion_description
                  }
                </p>
              </div>
            </div>
            <div className="margin-bottom-20">
              <span
                key="atmos-file-upload-complete"
                style={{
                  display: 'inline-block',
                }}
              >
                <div className="atmos-asset-upload-btn" onClick={onCloseModal}>
                  <div className="atmos-asset-upload-btn-text">
                    {
                      window.translations.songs_app
                        .dolby_atmos_upload_completion_button
                    }
                  </div>
                </div>
              </span>
            </div>
          </div>
        </div>
      </SongFormModal>
    </div>
  )
}

function renderSpatialAudioNotificationUploadModal(props) {
  const {
    spatialAudioModalNotificationEnabled,
    assetCancelUploadNotificationButtonProps,
  } = props
  let modalAnchor = <span></span>
  let cancelUploadNotificationButton = !!assetCancelUploadNotificationButtonProps
    ? renderCancelUploadNotificationButton(
        assetCancelUploadNotificationButtonProps
      )
    : null

  return (
    <div>
      <SongFormModal
        showModal={spatialAudioModalNotificationEnabled}
        modalButton={modalAnchor}
      >
        <div className="padding-left-50 padding-right-50">
          <div className="text-align-center">
            <div className="margin-bottom-20">
              <h1>
                <i
                  className="fa fa-spinner fa-spin spacer cancel-button-spinner"
                  key="spinner"
                />{' '}
                {window.translations.songs_app.dolby_atmos_notification_title}
              </h1>
            </div>
            <div className="margin-bottom-20">
              <div className="font-size-14">
                <p>
                  {
                    window.translations.songs_app
                      .upload_dolby_atmos_notification_description
                  }
                </p>
              </div>
            </div>
            <div className="margin-bottom-20">
              {cancelUploadNotificationButton}
            </div>
          </div>
        </div>
      </SongFormModal>
    </div>
  )
}

function renderSpatialUploadErrorModal(props) {
  let { onCloseModal } = props

  let modalButton = <span />

  return (
    <div>
      <SongFormModal
        modalButton={modalButton}
        showModal={true}
        onCloseModal={onCloseModal}
      >
        <div className="padding-left-50 padding-right-50">
          <div className="text-align-center">
            <div className="margin-bottom-20">
              <h1>
                {window.translations.songs_app.dolby_atmos_upload_error_title}
              </h1>
            </div>
            <div className="margin-bottom-20">
              <div className="font-size-14">
                <p>
                  {
                    window.translations.songs_app
                      .dolby_atmos_upload_error_description
                  }
                </p>
              </div>
            </div>
            <div className="margin-bottom-20">
              <span
                key="atmos-file-upload-complete"
                style={{
                  display: 'inline-block',
                }}
              >
                <div className="atmos-asset-upload-btn" onClick={onCloseModal}>
                  <div className="atmos-asset-upload-btn-text">
                    {
                      window.translations.songs_app
                        .dolby_atmos_upload_error_button
                    }
                  </div>
                </div>
              </span>
            </div>
          </div>
        </div>
      </SongFormModal>
    </div>
  )
}

class TrackListItemButtons extends React.Component {
  static propTypes = {
    song: PropTypes.object.isRequired,
    albumFinalized: PropTypes.bool.isRequired,
    showsDragging: PropTypes.bool,
    isCurrentSong: PropTypes.bool,
    onEditSong: PropTypes.func,
    onRemoveSong: PropTypes.func,
    onRemoveSongAtmosAsset: PropTypes.func,
    onFileUpload: PropTypes.func,
    onSpatialAudioFileUpload: PropTypes.func,
    onCancelUpload: PropTypes.func,
    playSong: PropTypes.func,
    pauseSong: PropTypes.func,
    closeSpatialAudioCompleteModal: PropTypes.func,
    closeSpatialAudioUploadErrorModal: PropTypes.func,
    albumIsLoading: PropTypes.bool,
    albumIsSingle: PropTypes.bool,
    deactivatespatialAudioEducationModalState: PropTypes.func,
    deactivateSpatialAudioModalNotificationState: PropTypes.func,
    activateSpatialAudioModalNotificationState: PropTypes.func,
  }

  handleFileUpload = (e) => {
    this.props.deactivateSpatialAudioModalNotificationState()
    this.props.onFileUpload(e)
  }

  handleSpatialAudioFileUpload = (e) => {
    let thisProps = this.props
    this.props.deactivatespatialAudioEducationModalState()
    this.props.activateSpatialAudioModalNotificationState()
    this.props.onSpatialAudioFileUpload(e)
  }

  handleCancelUpload = () => {
    this.props.onCancelUpload(this.props.song)
  }

  handleEditSong = () => {
    this.props.onEditSong(this.props.song)
  }

  handleRemoveSong = () => {
    this.props.onRemoveSong(this.props.song)
  }

  handleRemoveSongAtmosAsset = () => {
    this.props.onRemoveSongAtmosAsset(this.props.song)
  }

  render() {
    const {
      song,
      showsDragging,
      onEditSong,
      onRemoveSong,
      onRemoveSongAtmosAsset,
      isHovering,
      albumIsLoading,
      albumFinalized,
      albumIsSingle,
      albumIsEditable,
      isCurrentSong,
      spatialAudioEnabled,
      spatialAudioEducationModalEnabled,
      dolbyAtmosArticleLink,
      planUpgradeLink,
      spatialAudioModalNotificationEnabled,
      deactivateSpatialAudioModalNotificationState,
    } = this.props

    const {
      data,
      errors = {},
      isUploading,
      showSpatialAudioCompleteModal,
      showSpatialAudioUploadErrorModal,
    } = song
    const stereo_audio_asset_url = data.asset_url
    const spatial_audio_asset_url = data.spatial_audio_asset_url

    let stereoAssetUploadText = renderAssetUploadText({
      isUploading,
      hasErrors,
      errors,
      asset_url: stereo_audio_asset_url,
      uploadText: window.translations.songs_app.upload_stereo,
      replacementText: window.translations.songs_app.replace_stereo,
    })

    let atmosAssetUploadText = renderAssetUploadText({
      isUploading,
      hasErrors,
      errors,
      asset_url: spatial_audio_asset_url,
      uploadText: window.translations.songs_app.upload_atmos,
      replacementText: window.translations.songs_app.replace_atmos,
    })

    const sortButton = showsDragging ? <SongDragHandle key="sort" /> : null

    const stereoUploadSpinner = renderUploadSpinner({
      isUploading,
      isCurrentSong,
    })

    const atmosUploadSpinner = renderUploadSpinner({
      isUploading,
      isCurrentSong,
      spatialAudioModalNotificationEnabled,
      assetCancelUploadNotificationButtonProps: {
        handleCancelUpload: this.handleCancelUpload,
        cancellationText: window.translations.songs_app.cancel.toLowerCase(),
        cancellationBtnClass: 'stereo-asset-upload-btn',
        spatialAudioModalNotificationEnabled: spatialAudioModalNotificationEnabled,
      },
    })

    const removeSongButton = albumIsEditable ? (
      <RemoveSongButton
        key="remove-song"
        display={
          song.data.id &&
          onRemoveSong &&
          !showsDragging &&
          isHovering &&
          !albumIsLoading
            ? 'inline-block'
            : 'none'
        }
        className={`tracklist-remove-song ${
          albumIsLoading ? 'action-icons_album-loading' : ''
        }`}
        uuid={song.uuid}
        onClick={this.handleRemoveSong}
        displayTooltip={true}
      >
        <i className="fa fa-trash-o tracklist-item-icon" />
      </RemoveSongButton>
    ) : null

    const removeSongImmersiveAssetButton = albumIsEditable ? (
      <RemoveSongButton
        key="remove-song"
        display={
          song.data.id &&
          onRemoveSongAtmosAsset &&
          spatial_audio_asset_url &&
          !showsDragging &&
          isHovering &&
          !albumIsLoading
            ? 'inline-block'
            : 'none'
        }
        className={`tracklist-remove-song remove-dolby-atmos ${
          albumIsLoading ? 'action-icons_album-loading' : ''
        }`}
        uuid={song.uuid}
        onClick={this.handleRemoveSongAtmosAsset}
        displayTooltip={true}
      >
        <i className="fa fa-trash-o tracklist-item-icon" />
      </RemoveSongButton>
    ) : null

    const editSongButton =
      onEditSong && !showsDragging ? (
        <button
          key="edit_song"
          className={`edit-song ${
            albumIsLoading ? 'action-icons_album-loading' : ''
          }`}
          onClick={this.handleEditSong}
          title={window.translations.songs_app.edit}
        >
          <i className="fa fa-edit tracklist-item-icon" />
        </button>
      ) : null

    const playButton =
      stereo_audio_asset_url &&
      isHovering &&
      !showsDragging &&
      !albumIsLoading &&
      !isUploading ? (
        <PlayButton
          className={`${albumIsLoading ? 'action-icons_album-loading' : ''}`}
          key="play_button"
          playSong={this.props.playSong}
          pauseSong={this.props.pauseSong}
          song={song}
        />
      ) : null

    const currentlyUploading = song.isUploading && !song.isFinishedUploading

    let stereoFileUploadButton = renderAssetUploadBtn({
      onEditSong,
      albumIsLoading,
      currentlyUploading,
      isHovering,
      isLoading: this.props.isLoading,
      songUuid: song.uuid,
      handleFileUpload: this.handleFileUpload,
      asset_url: stereo_audio_asset_url,
      assetUploadText: stereoAssetUploadText,
      assetUploadBtnClass: 'stereo-asset-upload-btn',
    })

    let atmosFileUploadButton = null

    if (spatialAudioEnabled) {
      if (!spatialAudioEducationModalEnabled || !stereo_audio_asset_url) {
        atmosFileUploadButton = renderAssetUploadBtn({
          onEditSong,
          albumIsLoading,
          currentlyUploading,
          isHovering,
          isLoading: this.props.isLoading,
          // The songUuid here needed to be appended with an identifier that kept it
          // distinct from the stereo asset upload button's songUuid, otherwise the
          // uploading functionality would default to stereo asset uploading.
          songUuid: `${song.uuid}_spatial`,
          handleFileUpload: this.handleSpatialAudioFileUpload,
          asset_url: spatial_audio_asset_url,
          assetUploadText: atmosAssetUploadText,
          assetUploadBtnClass: 'atmos-asset-upload-btn',
          disabled: !stereo_audio_asset_url,
          titleText: !stereo_audio_asset_url
            ? window.translations.songs_app.atmos_upload_disabled
            : undefined,
          isAtmos: true,
        })
      } else if (spatialAudioEducationModalEnabled) {
        atmosFileUploadButton = renderspatialAudioEducationModal({
          dolbyAtmosArticleLink,
          assetUploadButtonProps: {
            onEditSong,
            albumIsLoading,
            currentlyUploading,
            isHovering,
            isLoading: this.props.isLoading,
            // The songUuid here needed to be appended with an identifier that kept it
            // distinct from the stereo asset upload button's songUuid, otherwise the
            // uploading functionality would default to stereo asset uploading.
            songUuid: `${song.uuid}_spatial`,
            handleFileUpload: this.handleSpatialAudioFileUpload,
            asset_url: spatial_audio_asset_url,
            assetUploadText: atmosAssetUploadText,
            assetUploadBtnClass: 'atmos-asset-upload-btn',
            disabled: !stereo_audio_asset_url,
            titleText: !stereo_audio_asset_url
              ? window.translations.songs_app.atmos_upload_disabled
              : undefined,
            isAtmos: true,
          },
        })
      }
    } else {
      atmosFileUploadButton = renderSpatialAudioPlanRequiredModal({
        planUpgradeLink,
        isHovering,
        isLoading: this.props.isLoading,
        onEditSong,
        albumIsLoading,
        currentlyUploading,
        assetUploadText: atmosAssetUploadText,
        assetUploadBtnClass: 'atmos-asset-upload-btn',
        disabled: !stereo_audio_asset_url,
        titleText: !stereo_audio_asset_url
          ? window.translations.songs_app.atmos_upload_disabled
          : undefined,
      })
    }

    let cancelStereoUploadButton = renderCancelUploadBtn({
      onEditSong,
      albumIsLoading,
      currentlyUploading,
      handleCancelUpload: this.handleCancelUpload,
      cancellationText: window.translations.songs_app.cancel.toLowerCase(),
      cancellationBtnClass: 'stereo-asset-upload-btn',
    })

    let cancelAtmosUploadButton = renderCancelUploadBtn({
      onEditSong,
      albumIsLoading,
      currentlyUploading,
      handleCancelUpload: this.handleCancelUpload,
      cancellationText: window.translations.songs_app.cancel.toLowerCase(),
      cancellationBtnClass: 'stereo-asset-upload-btn',
    })

    let studioButtonSet = [
      playButton,
      removeSongButton,
      editSongButton,
      sortButton,
    ].filter((button) => button != null)

    let atmosButtonSet =
      spatialAudioEnabled && stereo_audio_asset_url
        ? [removeSongImmersiveAssetButton].filter((button) => button != null)
        : null

    if (albumFinalized) {
      return studioButtonSet.filter((button) => button.key == 'play_button')
    }

    if (albumIsSingle) {
      studioButtonSet = studioButtonSet.filter(
        (button) => button.key != 'remove-song'
      )
    }

    const stereoUploadSet = [
      stereoUploadSpinner,
      stereoFileUploadButton,
      cancelStereoUploadButton,
    ].filter((button) => button != null)

    const atmosUploadSet = [
      atmosUploadSpinner,
      atmosFileUploadButton,
      cancelAtmosUploadButton,
    ].filter((button) => button != null)

    let stereoAssetData = renderAssetData({
      isCurrentSong,
      assetUrlError: song.errors.asset_url,
      uploadFailedText: window.translations.songs_app.upload_failed,
      durationError: song.errors.duration,
      durationFailureText: window.translations.songs_app.duration_failure,
      maxDurationError: song.errors.max_duration,
      maxDurationFailureText:
        window.translations.songs_app.max_duration_failure,
      fileTypeError: song.errors.validate_file_type,
      fileTypeFailureText:
        window.translations.songs_app.stereo_file_type_failure,
      origFilenameError: song.errors.orig_filename,
      assetFilename: song.data.asset_filename,
      assetFilenameDisplay: assetFilenameDisplay,
    })

    let atmosAssetData =
      spatialAudioEnabled &&
      renderAssetData({
        isCurrentSong,
        assetUrlError: song.errors.atmos_asset_url,
        uploadFailedText: window.translations.songs_app.upload_failed,
        durationError: song.errors.duration,
        durationFailureText: window.translations.duration_failure,
        atmosDurationError: song.errors.validate_duration,
        atmosDurationFailureText: window.translations.atmos_duration_failure,
        fileTypeError: song.errors.validate_file_type,
        fileTypeFailureText:
          window.translations.songs_app.atmos_file_type_failure,
        origFilenameError: song.errors.orig_filename,
        assetFilename: song.data.atmos_asset_filename,
        assetFilenameDisplay: assetFilenameDisplay,
      })

    let spatialAudioCompleteModal =
      showSpatialAudioCompleteModal &&
      renderSpatialAudioCompleteModal({
        onCloseModal: this.props.closeSpatialAudioCompleteModal,
      })

    let spatialUploadErrorModal =
      showSpatialAudioUploadErrorModal &&
      renderSpatialUploadErrorModal({
        onCloseModal: this.props.closeSpatialAudioUploadErrorModal,
      })

    return (
      <div className="button-grid-container">
        {spatialAudioCompleteModal}
        <div className="button-grid-row bottom-spacer">
          <div className="action-buttons" key="button-set">
            {studioButtonSet}
          </div>
          {stereoAssetData && (
            <div
              key="filename"
              className="track-list-item-asset-filename spatial-audio-asset-filename text-align-left"
            >
              {stereoAssetData}
            </div>
          )}
          <div className="spatial-audio-upload-set" key="upload-set">
            {stereoUploadSet}
          </div>
        </div>
        <div className="button-grid-row">
          <div className="action-buttons" key="button-set">
            {atmosButtonSet}
          </div>
          {atmosAssetData && (
            <div
              key="filename"
              className="track-list-item-asset-filename spatial-audio-asset-filename text-align-left"
            >
              {atmosAssetData}
            </div>
          )}
          <div className="spatial-audio-upload-set" key="upload-set">
            {atmosUploadSet}
          </div>
        </div>
        {spatialUploadErrorModal}
      </div>
    )
  }
}
export default TrackListItemButtons
