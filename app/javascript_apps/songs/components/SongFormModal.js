import React from 'react'
import Modal from 'react-modal'
import CloseIcon from '@material-ui/icons/Close'

const SONG_FORM_MODAL_DOM_ELEMENT = 'body'
const SONG_FORM_MODAL_DOM_ELEMENT_CONTENT_CLASS = 'song-form-modal'
const SONG_FORM_MODAL_DOM_ELEMENT_OVERLAY_CLASS = 'song-form-modal-overlay'
Modal.setAppElement(SONG_FORM_MODAL_DOM_ELEMENT)

class SongFormModal extends React.Component {
  constructor(props) {
    super(props)

    this.wrapperRef = React.createRef()

    this.state = {
      showModal: !!props.showModal,
    }
  }

  hideSongFormModal = () => {
    const { onCloseModal } = this.props

    this.setState({
      showModal: false,
    })

    if (onCloseModal) {
      onCloseModal()
    }
  }

  showSongFormModal = () => {
    this.setState({
      showModal: true,
    })
  }

  defaultModalButton = () => {
    return <div>Show Modal</div>
  }

  render() {
    const { modalButton, children } = this.props

    return (
      <div className="song-form-modal-wrapper" ref={this.wrapperRef}>
        <div
          className="song_form_modal_display"
          onClick={this.showSongFormModal}
        >
          {modalButton || this.defaultModalButton()}
        </div>

        <Modal
          overlayClassName={SONG_FORM_MODAL_DOM_ELEMENT_OVERLAY_CLASS}
          className={SONG_FORM_MODAL_DOM_ELEMENT_CONTENT_CLASS}
          shouldCloseOnOverlayClick={true}
          onRequestClose={this.hideSongFormModal}
          isOpen={this.state.showModal}
        >
          <div className="top_section">
            <div className="text-align-right">
              <span onClick={this.hideSongFormModal}>
                <CloseIcon />
              </span>
            </div>
          </div>
          <div>{children}</div>
        </Modal>
      </div>
    )
  }
}

export default SongFormModal
