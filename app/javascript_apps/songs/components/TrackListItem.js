import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import isCompleteSong from '../utils/isCompleteSong'
import TrackListItemButtons from './TrackListItemButtons'
import songTitleizer from '../utils/songTitleizer'
import toSentenceList from '../utils/toSentenceList'
import buildSongData from '../utils/buildSongData'
import {
  playSong,
  pauseSong,
  closeSpatialAudioCompleteModal,
  closeSpatialAudioUploadErrorModal,
} from '../actions/songActions'

class TrackListItem extends React.Component {
  static propTypes = {
    songUuid: PropTypes.string,
    showsDragging: PropTypes.bool,
    onEditSong: PropTypes.func,
    onCancelUpload: PropTypes.func,
    onRemoveSong: PropTypes.func,
    onRemoveSongAtmosAsset: PropTypes.func,
    onFileUpload: PropTypes.func,
    onSpatialAudioFileUpload: PropTypes.func,
    setSpatialUploadActive: PropTypes.func,
    setSpatialUploadInactive: PropTypes.func,
    showsBottomBar: PropTypes.bool,
    isCurrentSong: PropTypes.bool,
    trackNum: PropTypes.number,
    deactivatespatialAudioEducationModalState: PropTypes.func,
    deactivateSpatialAudioModalNotificationState: PropTypes.func,
    activateSpatialAudioModalNotificationState: PropTypes.func,
  }

  static defaultProps = {
    showsDragging: false,
    showsBottomBar: true,
  }

  state = {
    uploadingFileName: null,
  }

  primaryArtistNames() {
    const { album, song } = this.props
    const primaryArtists = song.data.artists.filter(
      (artist) => artist.credit === 'primary_artist' && artist.artist_name
    )
    if (primaryArtists.length > 0) {
      var artistNames = toSentenceList(
        primaryArtists.map((artist) => artist.artist_name),
        '&',
        false
      )
      return this.sliceStringByLength(artistNames, 15)
    } else if (album.is_various) {
      return window.translations.songs_app.various_artists
    }
  }

  handleCancelUpload = () => {
    const { song } = this.props
    this.setState({ uploadingFileName: null })
    this.props.onCancelUpload(song)
    this.props.deactivateSpatialAudioModalNotificationState()
  }

  handleFileUpload = (e) => {
    const { song } = this.props
    const file = e.target.files[0]
    this.setState({ uploadingFileName: file.name })
    this.props.setSpatialUploadInactive(song)
    this.props.onFileUpload(song, file)
  }

  handleSpatialAudioFileUpload = (e) => {
    const { song } = this.props
    const file = e.target.files[0]
    this.setState({ uploadingFileName: file.name })
    this.props.setSpatialUploadActive(song)
    this.props.onSpatialAudioFileUpload(song, file)
  }

  playSong = () => {
    this.props.dispatch(playSong(this.props.song))
  }

  pauseSong = () => {
    this.props.dispatch(pauseSong(this.props.song))
  }

  closeSpatialAudioCompleteModal = () => {
    this.props.deactivateSpatialAudioModalNotificationState()
    this.props.dispatch(closeSpatialAudioCompleteModal(this.props.song))
  }
  closeSpatialAudioUploadErrorModal = () => {
    this.props.dispatch(closeSpatialAudioUploadErrorModal(this.props.song))
  }

  sliceStringByLength = (text, maxLength) => {
    if (text.length > maxLength) {
      return text.slice(0, maxLength) + '...'
    } else {
      return text
    }
  }

  displayTrackListItem() {
    const {
      song,
      showsDragging,
      onEditSong,
      onRemoveSong,
      onRemoveSongAtmosAsset,
      album,
      isCurrentSong,
      trackNum,
      spatialAudioEnabled,
      spatialAudioEducationModalEnabled,
      deactivatespatialAudioEducationModalState,
      spatialAudioModalNotificationEnabled,
      deactivateSpatialAudioModalNotificationState,
      activateSpatialAudioModalNotificationState,
      dolbyAtmosArticleLink,
      planUpgradeLink,
    } = this.props

    const { data } = song
    const isSingle = album.album_type == 'Single'
    const completeClassName = isCompleteSong(song) ? '' : 'invalid-song'
    let songTitle
    if (data.name) {
      songTitle = album.allow_different_format
        ? data.name
        : songTitleizer(song.data)
      songTitle = this.sliceStringByLength(songTitle, 20)
    } else {
      songTitle = '...'
    }

    let trackListItemTitleDisplayClass =
      'track-list-item-title_display-for-spatial-audio'

    let trackListItemButtonsClass = 'track-list-item-buttons-for-spatial-audio'

    return [
      <div
        className="track-list-item-row"
        key={`track-list-item-row-${song.uuid}`}
      >
        <div
          key={`track-list-item-${song.uuid}`}
          className={`track-list-item-track_number ${
            data.track_number % 5 === 0 ? 'needs-top-border' : ''
          }`}
        >
          <p>{trackNum || data.track_number}</p>
        </div>
        <div
          key={`track-list-title-${song.uuid}`}
          className={`${completeClassName} ${trackListItemTitleDisplayClass}`}
        >
          <div>{songTitle}</div>
          <div className="main-artist">
            {window.translations.songs_app.by} {this.primaryArtistNames()}
          </div>
          {song.data.optional_isrc && (
            <span className="isrc-display">
              ISRC: {song.data.optional_isrc}
            </span>
          )}
        </div>
        <div
          key={`track-list-buttons-${song.uuid}`}
          className={`right ${trackListItemButtonsClass}`}
        >
          <TrackListItemButtons
            song={song}
            isHovering={!isCurrentSong}
            isCurrentSong={isCurrentSong}
            showsDragging={showsDragging}
            onEditSong={onEditSong}
            onRemoveSong={onRemoveSong}
            onRemoveSongAtmosAsset={onRemoveSongAtmosAsset}
            onCancelUpload={this.handleCancelUpload}
            onFileUpload={this.handleFileUpload}
            onSpatialAudioFileUpload={this.handleSpatialAudioFileUpload}
            playSong={this.playSong}
            pauseSong={this.pauseSong}
            closeSpatialAudioCompleteModal={this.closeSpatialAudioCompleteModal}
            closeSpatialAudioUploadErrorModal={
              this.closeSpatialAudioUploadErrorModal
            }
            albumFinalized={album.finalized}
            albumIsLoading={album.isLoading}
            isUploading={song.isUploading}
            albumIsEditable={album.is_editable}
            albumIsSingle={isSingle}
            spatialAudioEnabled={spatialAudioEnabled}
            spatialAudioEducationModalEnabled={
              spatialAudioEducationModalEnabled
            }
            deactivatespatialAudioEducationModalState={
              deactivatespatialAudioEducationModalState
            }
            spatialAudioModalNotificationEnabled={
              spatialAudioModalNotificationEnabled
            }
            deactivateSpatialAudioModalNotificationState={
              deactivateSpatialAudioModalNotificationState
            }
            activateSpatialAudioModalNotificationState={
              activateSpatialAudioModalNotificationState
            }
            dolbyAtmosArticleLink={dolbyAtmosArticleLink}
            planUpgradeLink={planUpgradeLink}
          />
        </div>
      </div>,
    ]
  }

  render() {
    const {
      customClass,
      song,
      showsDragging,
      showsBottomBar,
      album,
      isCurrentSong,
    } = this.props
    const { data } = song
    return (
      <div
        className={`track-list-item ${customClass}
        ${
          isCurrentSong || showsDragging || album.songIsDeleting
            ? ''
            : 'can-hover'
        }
        ${song.isDeleting ? 'is-deleting' : ''}
        ${showsBottomBar ? 'shows-bottom-bar' : ''}
        ${data.id ? 'song_item_for_preorder' : ''}`}
        id={`track_list_item_${song.uuid}`}
      >
        {this.displayTrackListItem()}
      </div>
    )
  }
}

function getSongPropsFromStore(storeObject, ids) {
  return (ids || [])
    .map((id) => {
      return storeObject[id]
    })
    .reduce((accumulator, val) => {
      return {
        ...accumulator,
        [val.uuid]: val,
      }
    }, {})
}

function mapStateToProps(storeState, componentProps) {
  let song = componentProps.isCurrentSong
    ? storeState.currentSong
    : storeState.songs[componentProps.songUuid]
  let storeCreatives, storeSongwriters, storeSongStartTimes

  if (componentProps.isCurrentSong) {
    storeCreatives = storeState.currentCreatives
    storeSongwriters = storeState.currentSongwriters
    storeSongStartTimes = storeState.currentSongStartTimes
  } else {
    storeCreatives = storeState.creatives
    storeSongwriters = storeState.songwriters
    storeSongStartTimes = storeState.songStartTimes
  }
  let creatives = getSongPropsFromStore(storeCreatives, song.data.artists)
  let songwriters = getSongPropsFromStore(storeSongwriters, song.songwriters)
  let songStartTimes = getSongPropsFromStore(
    storeSongStartTimes,
    song.songStartTimes
  )
  let { album, songwriterRoleId } = storeState
  let songData = buildSongData(
    song,
    creatives,
    songwriters,
    songwriterRoleId,
    songStartTimes
  )

  return {
    song: songData,
    album,
  }
}

export default connect(mapStateToProps)(TrackListItem)
