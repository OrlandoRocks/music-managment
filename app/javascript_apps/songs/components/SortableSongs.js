import React from 'react'
import TrackListItem from './TrackListItem'
import { SortableElement, SortableContainer } from './sortable'

const SortableSong = SortableElement(({ song }) => {
  return (
    <div>
      {
        <TrackListItem
          artistIds={song.data.artists}
          songUuid={song.uuid}
          showsDragging={true}
        />
      }
    </div>
  )
})

const SortableList = SortableContainer(({ songs }) => {
  return (
    <div key="song_list" className="track-list">
      {songs.map((song, index) => (
        <SortableSong key={`song-${index}`} index={index} song={song} />
      ))}
    </div>
  )
})

class SortableSongs extends React.Component {
  render() {
    return (
      <SortableList
        songs={this.props.songs}
        onSortEnd={this.props.onSortEnd}
        lockAxis="y"
        useDragHandle={true}
        useWindowAsScrollContainer={true}
      />
    )
  }
}

export default SortableSongs
