import React from 'react'

export default function SongUploadInfo() {
  return (
    <div className="song-upload-info">
      {window.translations.songs_app.asset_upload_message}
    </div>
  )
}
