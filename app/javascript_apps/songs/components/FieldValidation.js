import React from 'react'
import PropTypes from 'prop-types'

class FieldValidation extends React.Component {
  static propTypes = {
    styleGuideType: PropTypes.string.isRequired,
    styleGuideWarning: PropTypes.string,
    handleBlur: PropTypes.func.isRequired,
    handleFocus: PropTypes.func,
  }

  showWarningSpan() {
    const { styleGuideWarning, styleGuideType } = this.props

    if (styleGuideWarning) {
      const { form_field_warnings } = window.translations.songs_app
      const text = form_field_warnings[styleGuideType][styleGuideWarning]
      return <span className="field-warning-text">{text}</span>
    }
  }

  clearWarning = () => {
    const { styleGuideType, handleBlur, styleGuideWarning } = this.props
    if (styleGuideWarning) {
      handleBlur(styleGuideType)
    }
  }

  render() {
    const { handleFocus, children } = this.props
    return (
      <span
        className="field-warning"
        onFocus={handleFocus}
        onBlur={this.clearWarning}
      >
        {children}
        {this.showWarningSpan()}
      </span>
    )
  }
}

export default FieldValidation
