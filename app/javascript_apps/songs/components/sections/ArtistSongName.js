import React from 'react'
import PropTypes from 'prop-types'
import Autocomplete from 'react-autocomplete'
import escapeStringRegexp from 'escape-string-regexp'

const menuStyle = {
  borderRadius: '3px',
  boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
  background: 'rgba(255, 255, 255, 0.9)',
  padding: '2px 0',
  fontSize: '14px',
  overflow: 'auto',
  position: 'absolute',
  top: '23px',
  left: 0,
  width: '100%',
  zIndex: '998',
}

const renderMenu = (items) => {
  return (
    <div className="autocomplete-dropdown-menu" style={{ ...menuStyle }}>
      {items}
    </div>
  )
}

class ArtistSongName extends React.Component {
  static propTypes = {
    artist: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    remainingArtists: PropTypes.array,
    showSongwriterWarning: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    remainingArtists: [],
  }

  handleChange = (e, name) => {
    this.props.onChange(name)
  }

  handleFocus = (e) => {
    this.props.onChange(e.target.value)
  }

  clearStyleGuideWarning = () => {
    this.props.handleClearStyleGuideWarning()
  }

  handleSelect = (name) => {
    this.props.onChange(name)
  }

  getArtistForAutocomplete = (artist) => {
    return artist
  }

  renderArtist = (artist, isHighlighted) => {
    const style = {
      background: isHighlighted ? 'lightgray' : 'white',
      padding: '5px',
    }

    return (
      <div key={artist} style={style}>
        {artist}
      </div>
    )
  }

  shouldDisplayArtist = (artist) => {
    const inputArtist = escapeStringRegexp(
      this.props.artist.artist_name.toLowerCase()
    )
    return (escapeStringRegexp(artist) || '').toLowerCase().match(inputArtist)
  }

  styleGuideWarning() {
    const {
      artist: { styleGuideWarnings: styleGuideWarnings },
    } = this.props
    if (styleGuideWarnings) {
      return styleGuideWarnings.artist_name
    }
  }

  render() {
    const {
      artist,
      remainingArtists,
      showSongwriterWarning,
      isEditable,
    } = this.props
    const name = this.props.artist.artist_name
    if (!isEditable) {
      return (
        <input
          disabled={true}
          className="artist-song-name_no_edit"
          value={name}
        />
      )
    }
    let errorClass =
      artist.errors && artist.errors.artist_name_blank ? 'missing' : ''
    let warningClass = showSongwriterWarning ? 'warning' : ''

    return (
      <span className="field-warning">
        <Autocomplete
          wrapperStyle={{ display: 'block' }}
          menuStyle={menuStyle}
          items={remainingArtists}
          getItemValue={this.getArtistForAutocomplete}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
          renderItem={this.renderArtist}
          value={name}
          shouldItemRender={this.shouldDisplayArtist}
          renderMenu={renderMenu}
          inputProps={{
            placeholder: window.translations.songs_app.add_artist_or_creative,
            className: `artist-name-autocomplete-input ${errorClass} ${warningClass}`,
          }}
        />
      </span>
    )
  }
}

export default ArtistSongName
