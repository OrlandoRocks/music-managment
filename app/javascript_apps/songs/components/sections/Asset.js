import React from 'react'
import CancelUpload from './CancelUpload'
import AssetInformation from './AssetInformation'
import AssetUpload from './AssetUpload'
import ProgressSpinner from './ProgressSpinner'
import UploadSongButton from '../UploadSongButton'

class Asset extends React.Component {
  assetContents() {
    const { song } = this.props
    const { asset_url } = song.data
    const { isUploading, errors } = song
    if (isUploading) {
      return this.loadingView()
    } else if (errors.asset_url) {
      return this.errorView()
    } else if (asset_url) {
      return this.displayAssetView()
    } else {
      return this.uploadView()
    }
  }

  loadingView() {
    const { song, onCancelUpload } = this.props
    const { uuid } = song

    return [
      <ProgressSpinner
        key="progress-spinner"
        size={2}
        textClass="progress-spinner-text"
        text={window.translations.songs_app.uploading}
        uuid={uuid}
      />,
      <CancelUpload
        key="cancel-upload"
        className="pointer cancel-audio-upload cancel-in-progress-upload"
        uuid={uuid}
        onCancelUpload={onCancelUpload}
      >
        {' '}
        {window.translations.songs_app.cancel_upload}
      </CancelUpload>,
    ]
  }

  errorView() {
    const { song, onSongUpload, onCancelUpload } = this.props
    const { uuid } = song

    return [
      <div
        key="upload-failure-bar"
        id={`upload_failure_bar_${uuid}`}
        className="upload-progress-failure"
      ></div>,
      <div key="upload-failure-options" className="row">
        <div className="three columns upload-failure-options">
          <UploadSongButton
            className="song-file-upload-button retry pointer"
            uuid={uuid}
            onSongUpload={onSongUpload}
          >
            <a>
              <i className="fa fa-refresh" />{' '}
              {window.translations.songs_app.retry}
            </a>
          </UploadSongButton>
        </div>
        <div className="three columns">
          <CancelUpload
            className="cancel-audio-upload cancel-failed-upload pointer"
            uuid={uuid}
            onCancelUpload={onCancelUpload}
          >
            {' '}
            {window.translations.songs_app.cancel}
          </CancelUpload>
        </div>
      </div>,
    ]
  }

  displayAssetView() {
    const { song, onSongUpload, disabled } = this.props
    const { asset_url } = song.data

    return (
      <AssetInformation
        disabled={disabled}
        assetUrl={asset_url}
        song={song}
        onSongUpload={onSongUpload}
      />
    )
  }

  uploadView() {
    const { song, onSongUpload, disabled } = this.props
    const { uuid } = song
    return (
      <AssetUpload
        disabled={disabled}
        onSongUpload={onSongUpload}
        uuid={uuid}
      />
    )
  }

  render() {
    const { assetFilename } = this.props
    return (
      <fieldset className="asset-information_fieldset">
        <legend className="asset-information_legend">
          {window.translations.songs_app.audio_asset}
        </legend>
        <div className="row">
          {assetFilename && <p className="asset-filename">{assetFilename}</p>}
          {this.assetContents()}
        </div>
      </fieldset>
    )
  }
}

export default Asset
