import React from 'react'

const IsInstrumental = ({ isInstrumental, onAttributeUpdate }) => {
  const onChange = ({ target: { checked } }) => {
    onAttributeUpdate({ instrumental: checked })
  }

  return (
    <div className="row song-is-instrumental song-checkbox">
      <input
        id="is-instrumental"
        type="checkbox"
        checked={isInstrumental}
        onChange={onChange}
      />
      <label htmlFor="is-instrumental">
        <b>{window.translations.songs_app.is_instrumental}</b>
      </label>
    </div>
  )
}

export default IsInstrumental
