import React from 'react'
import UploadSongButton from '../UploadSongButton'

const AssetUpload = ({ onSongUpload, uuid, disabled }) => {
  return (
    <div>
      <p className="row asset-upload-message">
        {window.translations.songs_app.asset_upload_message}
      </p>
      <UploadSongButton
        disabled={disabled}
        onSongUpload={onSongUpload}
        uuid={uuid}
        className={`song-file-upload ${disabled ? 'disabled' : ''}`}
      >
        <i className="fa fa-cloud-upload" />
        {window.translations.songs_app.upload_audio}
      </UploadSongButton>

      <p className="asset-upload-help">
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={window.translations.how_to_convert_your_audio_files}
        >
          {window.translations.songs_app.asset_upload_help}
        </a>
      </p>
    </div>
  )
}

export default AssetUpload
