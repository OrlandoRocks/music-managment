import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import FieldValidation from '../FieldValidation'
import * as styleGuideActions from '../../actions/styleGuideActions'
import { errorStyles, fetchError, MESSAGE_LEVEL } from '../../utils/errors'
import Message from '../Message'

class Title extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onAttributeUpdate: PropTypes.func.isRequired,
    errors: PropTypes.object,
  }

  static defaultProps = {
    errors: {},
  }

  handleChange = (e) => {
    this.props.onAttributeUpdate({ name: e.target.value })
  }

  handleBlur = () => {
    const { onAttributeUpdate, name } = this.props
    onAttributeUpdate({ name: name })
  }

  errorClass() {
    const { errors } = this.props
    return errors && errors.name ? 'missing' : ''
  }

  render() {
    const { name, styleGuideWarnings, actions, errors } = this.props
    const nameError = fetchError('name', errors)
    const { clearStyleGuideWarning } = actions

    return (
      <div className="row song-input song-title">
        <label htmlFor="song-title">
          {window.translations.songs_app.song_title}
        </label>
        <a
          href={window.translations.how_do_i_format_my_album}
          className="song-title_formatting_guide"
          target="_blank"
          rel="noopener noreferrer"
        >
          {window.translations.songs_app.formatting_guide}
        </a>
        <FieldValidation
          styleGuideType={'song_name'}
          handleFocus={this.handleChange}
          handleBlur={clearStyleGuideWarning}
          styleGuideWarning={styleGuideWarnings && styleGuideWarnings.song_name}
        >
          <input
            className={`song-title ${this.errorClass()}`}
            id="song-title"
            name="song-title"
            type="text"
            value={name}
            onBlur={this.handleBlur}
            onChange={this.handleChange}
          />
          {nameError && (
            <div style={errorStyles}>
              <Message level={MESSAGE_LEVEL}>
                <p>{nameError}</p>
              </Message>
            </div>
          )}
        </FieldValidation>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const {
    currentSong: { styleGuideWarnings: styleGuideWarnings },
  } = storeState
  return {
    styleGuideWarnings,
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(styleGuideActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Title)
