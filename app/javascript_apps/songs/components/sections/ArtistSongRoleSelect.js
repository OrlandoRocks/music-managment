import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Select, { components } from 'react-select'
import songRoleSort from '../../utils/songRoleSort'
import partition from '../../utils/partition'
import { get } from 'lodash'

const MAIN_ARTIST_CREDIT = {
  id: 'primary_artist',
  role_type_raw: 'primary artist',
  role_type: window.translations.songs_app.main_artist,
}
const FEATURED_CREDIT = {
  id: 'featuring',
  role_type_raw: 'featured',
  role_type: window.translations.songs_app.featured,
}

class ArtistSongRoleSelect extends React.Component {
  static propTypes = {
    defaultValue: PropTypes.number,
    songRoles: PropTypes.object.isRequired,
    artist: PropTypes.object.isRequired,
    onCreditChange: PropTypes.func.isRequired,
    onRoleChange: PropTypes.func.isRequired,
    moreThanOneSongwriter: PropTypes.bool.isRequired,
    toggleSongwriterWarning: PropTypes.func.isRequired,
    showSongwriterWarning: PropTypes.bool.isRequired,
    mainArtistRemovable: PropTypes.bool.isRequired,
  }

  state = {
    isOpen: false,
  }

  formatRoles = (roles) => {
    const {
      numSelected,
      isMainArtist,
      isAlbumLevelArtist,
      isFeaturingArtist,
      mainArtistRemovable,
    } = this.props

    if (
      numSelected === 0 &&
      !isMainArtist &&
      !isFeaturingArtist &&
      !this.state.isOpen
    ) {
      return [
        {
          label: window.translations.songs_app.choose,
          value: '',
          isFixed: true,
        },
      ]
    }

    return roles.map((role) => {
      if (
        role.id === 'primary_artist' &&
        isMainArtist &&
        (isAlbumLevelArtist || !mainArtistRemovable)
      ) {
        return {
          label: MAIN_ARTIST_CREDIT.role_type,
          value: MAIN_ARTIST_CREDIT.id,
          isFixed: true,
        }
      } else if (role.id === 'primary_artist' && isMainArtist) {
        return {
          label: MAIN_ARTIST_CREDIT.role_type,
          value: MAIN_ARTIST_CREDIT.id,
        }
      } else if (role.id === 'featuring' && isFeaturingArtist) {
        return { label: FEATURED_CREDIT.role_type, value: FEATURED_CREDIT.id }
      }
      return { label: role.role_type, value: role.id.toString() }
    })
  }

  handleChange = (values) => {
    let selectedRoleIds = values
      .map((val) => val.value.toString())
      .filter((roleId) => roleId !== '0')

    let [credits, roles] = partition(selectedRoleIds, (roleId) => {
      return roleId === 'featuring' || roleId === 'primary_artist'
    })

    let notChoosingMainOrFeaturing = credits.length === 0
    let noSelectedRoles = selectedRoleIds.length === 0

    if (notChoosingMainOrFeaturing && noSelectedRoles) {
      credits[0] = ''
    } else if (notChoosingMainOrFeaturing) {
      credits[0] = 'contributor'
    }

    this.props.onCreditChange(credits[0])
    this.props.onRoleChange(roles || [])
  }

  handleOnOpen = () => {
    this.setState({ isOpen: true })
  }

  handleOnClose = () => {
    this.setState({ isOpen: false })
  }

  toggleMainOrFeaturedArtistRole(roles, selectedRoleIds) {
    if (selectedRoleIds.includes('primary_artist')) {
      roles = roles.filter((role) => role.id !== 'featuring')
    } else if (selectedRoleIds.includes('featuring')) {
      roles = roles.filter((role) => role.id !== 'primary_artist')
    }
    return roles
  }

  unselectedRoles() {
    const { isMainArtist } = this.props
    let songRoles = this.props.songRolesCredits

    songRoles = this.toggleMainOrFeaturedArtistRole(
      songRoles,
      this.selectedRoles().map((role) => role.id)
    )

    let returnedValue = songRoles.filter((role) => {
      if (isMainArtist) {
        return (
          !this.props.artist.role_ids.includes(role.id.toString()) &&
          role.id !== 'primary_artist' &&
          role.id !== 'featuring'
        )
      } else {
        return !this.props.artist.role_ids.includes(role.id.toString())
      }
    })

    return returnedValue
  }

  selectedRoles() {
    const { isMainArtist, artist, isFeaturingArtist } = this.props
    let roles = []
    if (isMainArtist) {
      roles.push(MAIN_ARTIST_CREDIT)
    } else if (isFeaturingArtist) {
      roles.push(FEATURED_CREDIT)
    }

    return roles.concat(
      artist.role_ids.map((roleId) =>
        this.props.songRolesCredits.find(
          (item) => item.id.toString() === roleId.toString()
        )
      )
    )
  }

  render() {
    const { artist } = this.props
    let unselectedRoles = this.formatRoles(this.unselectedRoles())
    let selectedRolesValue = this.formatRoles(this.selectedRoles())
    let customClass =
      (artist.errors && artist.errors.credit_blank) ||
      get(artist, 'styleGuideWarnings.artist_name')
        ? 'missing'
        : ''

    return (
      <Select
        backspaceRemovesValue={false}
        className={customClass}
        closeMenuOnSelect={false}
        hideSelectedOptions
        isClearable={false}
        isMulti
        onChange={this.handleChange}
        onMenuClose={this.handleOnClose}
        onMenuOpen={this.handleOnOpen}
        options={unselectedRoles}
        value={selectedRolesValue}
        components={{
          DropdownIndicator: () => null,
          IndicatorSeparator: () => null,
          MultiValueLabel,
        }}
        styles={{
          multiValue: () => styles.multiValue,
          multiValueLabel: () => styles.multiValueLabel,
          multiValueRemove: (base, state) => {
            return state.data.isFixed
              ? { ...base, display: 'none' }
              : {
                  ...base,
                  ...styles.multiValueRemove,
                }
          },
        }}
      />
    )
  }
}

function mapStateToProps(storeState, componentProps) {
  const {
    artist: { role_ids: roleIds },
  } = componentProps

  const { artist } = componentProps
  const { songRoles, songwriterRoleId } = storeState
  const numSelected = roleIds.length
  const { [songwriterRoleId]: _omit, ...nonSongwriterSongRoles } = songRoles
  const isMainArtist = artist.credit === 'primary_artist'
  const isFeaturingArtist = artist.credit === 'featuring'
  const isAlbumLevelArtist = artist.associated_to === 'Album'
  const songRolesCredits = [MAIN_ARTIST_CREDIT, FEATURED_CREDIT].concat(
    songRoleSort(nonSongwriterSongRoles)
  )

  return {
    songRoles: nonSongwriterSongRoles,
    numSelected,
    songRolesCredits,
    isMainArtist,
    isFeaturingArtist,
    isAlbumLevelArtist,
  }
}

function MultiValueLabel(props) {
  return (
    <components.MultiValueLabel
      {...props}
      innerProps={{
        ...props.innerProps,
        role: 'option',
        'aria-selected': 'true',
      }}
    />
  )
}

const styles = {
  multiValue: {
    backgroundColor: 'rgba(0,126,255,0.08)',
    border: '1px solid rgba(0,126,255,0.24)',
    borderRadius: '2px',
    color: '#007eff',
    display: 'flex',
    flexDirection: 'row-reverse',
    fontSize: '0.9em',
    lineHeight: '1.4',
    margin: '2px 5px',
    padding: '2px',
  },
  multiValueLabel: {
    color: '#007eff',
    cursor: 'default',
    padding: '2px 5px',
  },
  multiValueRemove: {
    borderRight: '1px solid rgba(0,126,255,0.24)',
    borderRadius: 'none',
    '&:hover': {
      background: 'rgba(0,113,230,0.08)',
      color: '#007eff',
    },
  },
}

export default connect(mapStateToProps)(ArtistSongRoleSelect)
