import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import SongStartTimeRow from './SongStartTimeRow'
import { updateSongStartTime } from '../../actions/songStartTimeActions'

class SongStartTimesForm extends React.Component {
  static propTypes = {
    storeId: PropTypes.number.isRequired,
  }

  handleSongStartTimeChange = (songStartTime) => {
    this.props.dispatch(updateSongStartTime(songStartTime))
  }

  render() {
    const { storeId } = this.props
    return (
      <div className="song-start-time-form">
        <legend className="artists-creatives_legend">
          {
            window.translations.songs_app.song_start_time_section_titles[
              `store_${storeId}`
            ]
          }{' '}
          &nbsp;
        </legend>

        <div>
          {this.props.songStartTimes
            .sort((songStartTimeA, songStartTimeB) => {
              return songStartTimeA.index - songStartTimeB.index
            })
            .map((songStartTime) => {
              return (
                <SongStartTimeRow
                  key={songStartTime.uuid}
                  onChange={this.handleSongStartTimeChange}
                  songStartTime={songStartTime}
                />
              )
            })}
        </div>
      </div>
    )
  }
}

function mapStateToProps(storeState, componentProps) {
  const { storeId } = componentProps

  const songStartTimes = Object.values(storeState.currentSongStartTimes).filter(
    (songStartTime) => {
      return songStartTime.store_id == storeId
    }
  )

  return {
    songStartTimes,
  }
}

export default connect(mapStateToProps)(SongStartTimesForm)
