import React from 'react'
import PropTypes from 'prop-types'

class CoverSong extends React.Component {
  static propTypes = {
    isCoverSong: PropTypes.bool.isRequired,
    madePopularBy: PropTypes.string,
    onAttributeUpdate: PropTypes.func.isRequired,
  }

  static defaultProps = {
    madePopularBy: '',
  }

  handleCoverSongChange = (e) => {
    const isChecked = e.target.checked

    if (isChecked) {
      this.props.onAttributeUpdate({ cover_song: true })
    } else {
      this.props.onAttributeUpdate({ cover_song: false, made_popular_by: null })
    }
  }

  handleMadePopularByChange = (e) => {
    this.props.onAttributeUpdate({
      cover_song: true,
      made_popular_by: e.target.value,
    })
  }

  render() {
    const { isCoverSong, madePopularBy } = this.props

    return (
      <div className="row song-cover">
        <label>Cover Song</label>
        <input
          className="cover-song-checkbox"
          type="checkbox"
          checked={isCoverSong || false}
          onChange={this.handleCoverSongChange}
        />
        {isCoverSong && (
          <div>
            <div>
              <label>made popular by</label>
              <input
                className="made-popular-by-text-field"
                type="text"
                value={madePopularBy || ''}
                onChange={this.handleMadePopularByChange}
              />
            </div>
            <p>Did you know distributing cover songs requires a license?</p>
          </div>
        )}
      </div>
    )
  }
}

export default CoverSong
