import React from 'react'
import PropTypes from 'prop-types'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'
import ErrorMessage from '../ErrorMessage'

const ExplicitInfoTooltip = (
  <TooltipContainer>
    <TooltipTitle>{window.translations.songs_app.explicit}</TooltipTitle>
    <TooltipColumn>{window.translations.songs_app.explicit_info}</TooltipColumn>
  </TooltipContainer>
)

class ExplicitRadio extends React.Component {
  static propTypes = {
    onAttributeUpdate: PropTypes.func.isRequired,
  }

  onRadioChange = (e) => {
    const { value } = e.target

    const booleanValue = value == 'Yes'

    this.props.onAttributeUpdate({ explicit: booleanValue })
  }

  render() {
    const { isExplicit, errors } = this.props
    const explicitError = errors && errors.explicit

    return (
      <div className="row song-explicit">
        <div className="song-explicit song-explicit-prompt">
          <b class="explicit-lyrics-radio-label">
            {window.translations.songs_app.explicit_prompt}
          </b>
          <Tooltip>{ExplicitInfoTooltip}</Tooltip>
        </div>
        <div className="song-explicit song-radio-toolbar">
          <input
            id="song-explicit-yes"
            type="radio"
            name="explicit"
            value="Yes"
            checked={isExplicit == true}
            onChange={this.onRadioChange}
          />
          <label className="song-explicit" htmlFor="song-explicit-yes">
            {window.translations.songs_app.button_yes}
          </label>
          <input
            id="song-explicit-no"
            type="radio"
            name="explicit"
            value="No"
            checked={isExplicit == false}
            onChange={this.onRadioChange}
          />
          <label className="song-explicit" htmlFor="song-explicit-no">
            {window.translations.songs_app.button_no}
          </label>
        </div>
        {explicitError && (
          <div className="song-explicit-error">
            <ErrorMessage
              message={window.translations.songs_app.explicit_error}
            />
          </div>
        )}
      </div>
    )
  }
}

export default ExplicitRadio
