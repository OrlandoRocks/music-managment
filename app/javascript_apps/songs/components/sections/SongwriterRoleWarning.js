import React from 'react'

const SongwriterRoleWarning = () => {
  const style = {
    backgroundColor: '#F5A623',
    color: 'white',
    padding: '10px',
    margin: '-4px -6px',
    border: 'none',
  }

  return (
    <div style={style}>
      <div>
        <h4>{window.translations.songs_app.songwriter_role_warning_title}</h4>
        {window.translations.songs_app.songwriter_role_warning_message}
      </div>
    </div>
  )
}

export default SongwriterRoleWarning
