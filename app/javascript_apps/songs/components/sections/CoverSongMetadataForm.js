import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import DOMPurify from 'dompurify'

import RadioToolbar from '../../../shared/RadioToolbar'

import { UPDATE_COVER_SONG_METADATA } from '../../actions/actionTypes'

const ERROR_CLASS = ' missing'
const NO_ERROR_CLASS = ''
const COVER_SONG_NAME = 'cover_song'
const LICENSED_NAME = 'licensed'
const WILL_GET_LICENSE_NAME = 'will_get_license'

function replaceTextWithLink({ text, url }) {
  return text && text.replace('{{url}}', url)
}

function CoverSongMetadataRadio({
  errorClass,
  handleOnChange,
  name,
  prompt,
  value,
  disabled,
  labelForyes,
  labelForNo,
}) {
  return (
    <div className={`${errorClass} song-radio`}>
      {prompt && (
        <div
          className={`song-cover-song-metadata-cover-song song-cover-song-metadata-cover-song-prompt`}
        >
          <b
            dangerouslySetInnerHTML={{
              __html: prompt,
            }}
          />
        </div>
      )}
      <div className={`song-cover-song-metadata-cover-song`}>
        <input
          id={`song-cover-song-metadata-cover-song-yes`}
          type="radio"
          name={name}
          value={'Yes'}
          checked={value == true}
          onChange={handleOnChange}
          disabled={disabled}
        />
        <label
          className={
            'song-cover-song-metadata-cover-song radio-label font-weight-light'
          }
          dangerouslySetInnerHTML={{
            __html: labelForyes || window.translations.songs_app.button_yes,
          }}
        />
        <br />
        <input
          id={`song-cover-song-metadata-cover-song-no`}
          type="radio"
          name={name}
          value={'No'}
          checked={value == false}
          onChange={handleOnChange}
          disabled={disabled}
        />
        <label
          className={
            'song-cover-song-metadata-cover-song radio-label font-weight-light'
          }
          dangerouslySetInnerHTML={{
            __html: labelForNo || window.translations.songs_app.button_no,
          }}
        />
      </div>
    </div>
  )
}

function CoverSongMetadataCheckBox({
  errorClass,
  handleOnChange,
  name,
  label,
  value,
  disabled,
}) {
  return (
    <div
      className={`song-checkbox song-cover-song-metadata-checkbox-field${errorClass}`}
    >
      <input
        id={`song-cover-song-metadata-${name}`}
        checked={value}
        name={name}
        onChange={handleOnChange}
        type="checkbox"
        disabled={disabled}
      />
      <label
        htmlFor={`song-cover-song-metadata-${name}`}
        className={'font-weight-light'}
        dangerouslySetInnerHTML={{
          __html: label,
        }}
      />
    </div>
  )
}

CoverSongMetadataRadio.proptypes = {
  errorClass: PropTypes.string.isRequired,
  handleOnChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  prompt: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
}

CoverSongMetadataCheckBox.proptypes = {
  errorClass: PropTypes.string.isRequired,
  handleOnChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
}

class CoverSongMetadataForm extends React.Component {
  static propTypes = {
    cover_song_metadata: PropTypes.object.isRequired,
  }

  updateCoverSongMetadataForCheckBox = (e) => {
    const { dispatch } = this.props
    const type = e.target.name
    const value = e.target.checked

    dispatch({
      type: UPDATE_COVER_SONG_METADATA,
      payload: { type, value },
    })
  }

  updateCoverSongMetadataForRadio = (e) => {
    const { dispatch } = this.props
    const type = e.target.name
    const { value } = e.target

    const booleanValue = value == 'Yes'

    dispatch({
      type: UPDATE_COVER_SONG_METADATA,
      payload: { type, value: booleanValue },
    })
  }

  updateCoverSongMetadataForLicensedRadio = (e) => {
    const { dispatch } = this.props

    this.updateCoverSongMetadataForRadio(e)

    const { value } = e.target

    const booleanValue = value == 'Yes'

    dispatch({
      type: UPDATE_COVER_SONG_METADATA,
      payload: { type: 'will_get_license', value: !booleanValue },
    })
  }

  render() {
    const {
      cover_song_metadata: { cover_song, licensed, will_get_license },
      errors,
      coverSongMetadataEnabled,
      coverSongArticleLink,
    } = this.props

    const coverSongError = errors.coverSong ? ERROR_CLASS : NO_ERROR_CLASS

    if (!coverSongMetadataEnabled) return null

    const coverSongChildrenFieldsDisabled = cover_song !== true

    const coverSongPrompt = replaceTextWithLink({
      text: window.translations.songs_app.cover_song_metadata_cover_song_prompt,
      url: coverSongArticleLink,
    })

    const licensingPrompt = replaceTextWithLink({
      text: window.translations.songs_app.cover_song_metadata_licensing_prompt,
      url: coverSongArticleLink,
    })

    const licensedLabel =
      window.translations.songs_app.cover_song_metadata_licensed_label

    const willGetLicenseLabel =
      window.translations.songs_app.cover_song_metadata_will_get_license_label

    const licensingNote =
      window.translations.songs_app.cover_song_metadata_licensing_note

    return (
      <div className="copyright-form">
        <fieldset className="artists-creatives_fieldset">
          <legend className="artists-creatives_legend">
            {window.translations.songs_app.cover_song_metadata_header}
          </legend>
          <div className="song-cover-song-metadata">
            <CoverSongMetadataRadio
              errorClass={coverSongError}
              handleOnChange={this.updateCoverSongMetadataForRadio}
              name={COVER_SONG_NAME}
              prompt={coverSongPrompt}
              value={cover_song}
            />
            <br />
          </div>
          <div
            className={`song-cover-song-metadata ${
              coverSongChildrenFieldsDisabled ? 'disabled-text' : ''
            }`}
          >
            <CoverSongMetadataRadio
              errorClass={NO_ERROR_CLASS}
              handleOnChange={this.updateCoverSongMetadataForLicensedRadio}
              name={LICENSED_NAME}
              prompt={licensingPrompt}
              value={licensed}
              disabled={coverSongChildrenFieldsDisabled}
              labelForyes={licensedLabel}
              labelForNo={willGetLicenseLabel}
            />
          </div>
        </fieldset>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const {
    currentSong: {
      data: { cover_song_metadata },
    },
  } = storeState

  return {
    cover_song_metadata: cover_song_metadata || {},
  }
}

export default connect(mapStateToProps)(CoverSongMetadataForm)
