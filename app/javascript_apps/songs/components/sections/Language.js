import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import TranslatedSongName from './TranslatedSongName'
import spaceship from '../../utils/spaceship'

const instrumantalLang = { id: 'instrumental', description: 'Instrumental' }

class Language extends React.Component {
  static defaultProps = {
    language_code_id: 1,
    errors: {},
  }

  static propTypes = {
    language_code_id: PropTypes.number,
    translatedName: PropTypes.string,
    onAttributeUpdate: PropTypes.func.isRequired,
    errors: PropTypes.object,
  }

  constructor(props) {
    super(props)
    const { language_code_id, languages } = this.props
    let language = languages.find(
      (language) => language.id === language_code_id
    )
    this.state = {
      language: language || { description: null },
      translatedName: this.props.translatedName || '',
    }
  }

  onLanguageChange = (e) => {
    let languageId = parseInt(e.target.value)
    let language = this.props.languages.find((lang) => lang.id === languageId)
    let attributes = { language_code_id: parseInt(languageId) }
    if (!language.needs_translated_song_name) {
      attributes.translated_name = ''
    }
    this.setState({ language: language })
    this.props.onAttributeUpdate(attributes)
  }

  onTranslatedNameChange = (e) => {
    this.setState({ translatedName: e.target.value })
    this.props.onAttributeUpdate({ translated_name: e.target.value })
  }

  shouldShowTranslatedName() {
    return this.state.language.needs_translated_song_name
  }

  render() {
    const { language_code_id, languages, errors, isInstrumental } = this.props
    const { translatedName, language } = this.state
    let defaultValue = isInstrumental ? 'instrumental' : language_code_id
    let shouldShowTranslatedName = this.shouldShowTranslatedName()

    return (
      <div className="row song-language song-select">
        <label>{window.translations.songs_app.song_language}</label>
        <div className="dropsdown">
          <select
            className="language-menu"
            onChange={this.onLanguageChange}
            defaultValue={defaultValue}
            disabled={isInstrumental}
          >
            {languages.map((language) => (
              <option key={language.id} value={language.id}>
                {language.description}
              </option>
            ))}
          </select>
        </div>
        {shouldShowTranslatedName && (
          <TranslatedSongName
            handleChange={this.onTranslatedNameChange}
            errors={errors}
            translatedName={translatedName}
            language={language}
          />
        )}
      </div>
    )
  }
}

function languageSorter(a, b) {
  const descriptionA = a.description.toUpperCase()
  const descriptionB = b.description.toUpperCase()
  return spaceship(descriptionA, descriptionB)
}

function mapStateToProps(storeState) {
  let genre = storeState.album.genre
  let isInstrumental = genre === 'Instrumental'
  let storeLanguages = Object.values(storeState.languages).sort(languageSorter)
  let languages = isInstrumental
    ? [...storeLanguages, instrumantalLang]
    : storeLanguages
  return {
    languages: languages,
    isInstrumental: isInstrumental,
  }
}

export default connect(mapStateToProps)(Language)
