import React from 'react'
import PropTypes from 'prop-types'
import Message from '../Message'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const IsrcInfoTooltip = (
  <TooltipContainer>
    <TooltipTitle>{window.translations.songs_app.isrc_number}</TooltipTitle>
    <TooltipColumn>
      {window.translations.songs_app.isrc_number_tooltip_text}
    </TooltipColumn>
  </TooltipContainer>
)

class OptionalIsrc extends React.Component {
  static propTypes = {
    optionalIsrc: PropTypes.string,
    albumType: PropTypes.string,
    onAttributeUpdate: PropTypes.func.isRequired,
    previouslyReleased: PropTypes.string,
  }

  static defaultProps = {
    errors: {},
  }

  onChange = (e) => {
    this.props.onAttributeUpdate({ optional_isrc: e.target.value })
  }

  isrcClassName() {
    if (this.props.errors.optional_isrc) {
      return 'missing'
    }
  }

  render() {
    const { optionalIsrc, previouslyReleased, errors, isEditable } = this.props
    return (
      <div className="row song-optional-isrc song-input-box">
        <label>{window.translations.songs_app.isrc_number}</label>
        <Tooltip>{IsrcInfoTooltip}</Tooltip>
        <p className="song-optional-isrc_additional_info">
          {window.translations.songs_app.optional_isrc_dont_have_one}
        </p>
        <input
          placeholder={
            previouslyReleased
              ? window.translations.songs_app.isrc_placeholder
              : ''
          }
          className={this.isrcClassName()}
          disabled={!isEditable}
          type="text"
          value={optionalIsrc || ''}
          onChange={this.onChange}
        />
        {errors.optional_isrc && (
          <div
            key={`optional_isrc_${optionalIsrc}_message`}
            className="optional-isrc-message"
          >
            <Message level={'error'}>
              <p>{window.translations.songs_app.optional_isrc.error}</p>
            </Message>
          </div>
        )}
      </div>
    )
  }
}

export default OptionalIsrc
