import React from 'react'

const AdditionalSongInformation = ({ children }) => {
  return (
    <fieldset className="additional-song-info_fieldset">
      <legend className="additional-song-info_legend">
        {window.translations.songs_app.additional_information} &nbsp;
      </legend>
      {children}
    </fieldset>
  )
}

export default AdditionalSongInformation
