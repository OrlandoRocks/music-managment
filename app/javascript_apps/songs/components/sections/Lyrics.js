import React from 'react'
import PropTypes from 'prop-types'

import Message from '../Message'
import { errorStyles, fetchError, MESSAGE_LEVEL } from '../../utils/errors'

class Lyrics extends React.Component {
  static defaultProps = {
    lyrics: '',
  }

  static propTypes = {
    lyrics: PropTypes.string,
    onAttributeUpdate: PropTypes.func.isRequired,
  }

  attributeUpdate = (e) => {
    this.props.onAttributeUpdate({ lyrics: e.target.value })
  }

  adjustHeight = () => {
    document.getElementsByTagName('textarea')[0].style.height = '80px'
  }

  render() {
    const { errors } = this.props
    const lyricsError = fetchError('lyrics', errors)

    return (
      <div className="row song-lyrics">
        <label>{window.translations.songs_app.lyrics}</label>
        <textarea
          onBlur={this.adjustHeight}
          rows="4"
          cols="50"
          maxLength="10000"
          value={this.props.lyrics || ''}
          onChange={this.attributeUpdate}
        />
        {lyricsError && (
          <div style={errorStyles}>
            <Message level={MESSAGE_LEVEL}>
              <p>{lyricsError}</p>
            </Message>
          </div>
        )}
      </div>
    )
  }
}

export default Lyrics
