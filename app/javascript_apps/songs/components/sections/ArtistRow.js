import React from 'react'
import PropTypes from 'prop-types'
import ArtistSongRoleSelect from './ArtistSongRoleSelect'
import ArtistSongName from './ArtistSongName'
import Message from '../Message'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const RoleInfoTooltip = (
  <TooltipContainer>
    <TooltipTitle>{window.translations.songs_app.roles}</TooltipTitle>
    <TooltipColumn>
      {window.translations.songs_app.role_tooltip_text}
    </TooltipColumn>
  </TooltipContainer>
)

class ArtistRow extends React.Component {
  static propTypes = {
    artist: PropTypes.object.isRequired,
    onRemoveArtist: PropTypes.func.isRequired,
    onEditArtist: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    remainingArtists: PropTypes.array,
    moreThanOneSongwriter: PropTypes.bool.isRequired,
    moreThanOnePrimaryArtist: PropTypes.bool.isRequired,
    songwriterRoleId: PropTypes.string.isRequired,
    albumType: PropTypes.string.isRequired,
    isDJRelease: PropTypes.bool,
  }

  static defaultProps = {
    remainingArtists: [],
  }

  state = {
    showSongwriterWarning: false,
  }

  handleCreditChange = (credit) => {
    this.props.onEditArtist('credit', credit, this.props.artist.uuid)
  }

  handleRoleChange = (roleIds) => {
    this.props.onEditArtist('role_ids', roleIds, this.props.artist.uuid)
  }

  handleClearStyleGuideWarning = () => {
    this.props.onEditArtist('styleGuideWarnings', {}, this.props.artist.uuid)
  }

  handleRemoveArtist = (e) => {
    e.preventDefault()
    if (!this.props.moreThanOneSongwriter && this.hasSongwriterRole()) {
      this.toggleSongwriterWarning()
    } else {
      this.props.onRemoveArtist(this.props.artist.uuid)
    }
  }

  toggleSongwriterWarning = () => {
    this.setState({ showSongwriterWarning: !this.state.showSongwriterWarning })
  }

  handleArtistNameChange = (artist_name) => {
    this.props.onEditArtist('artist_name', artist_name, this.props.artist.uuid)
  }

  isArtistEnabled() {
    const { albumType, artist, moreThanOnePrimaryArtist } = this.props
    const isPrimaryArtist = artist.credit === 'primary_artist'
    const isAssociatedToSong = artist.associated_to === 'Song'
    const { isVariousArtistAlbum, isDJRelease } = this.props

    if (!artist.creative_id) {
      return true
    } else if (
      !isPrimaryArtist &&
      (albumType === 'Single' || isAssociatedToSong)
    ) {
      return true
    } else if (
      isPrimaryArtist &&
      moreThanOnePrimaryArtist &&
      (albumType === 'Single' || isAssociatedToSong)
    ) {
      return true
    } else if (isVariousArtistAlbum || isDJRelease) {
      return true
    }

    return false
  }

  isRoleRemovable() {
    const { artist, moreThanOnePrimaryArtist } = this.props
    const isPrimaryArtist = artist.credit === 'primary_artist'

    return !isPrimaryArtist || moreThanOnePrimaryArtist
  }

  isArtistRowRemovable() {
    return this.isArtistEnabled() && this.isRoleRemovable()
  }

  hasSongwriterRole() {
    return this.props.artist.role_ids.includes(this.props.songwriterRoleId)
  }

  showMissingFieldError() {
    const { artist } = this.props
    return (
      artist.errors &&
      (artist.errors.artist_name_blank || artist.errors.credit_blank)
    )
  }

  styleGuideWarning(warning) {
    const { form_field_warnings } = window.translations.songs_app
    return form_field_warnings['artist_name'][warning]
  }

  render() {
    const { artist, moreThanOneSongwriter, index } = this.props
    const isEditable = this.isArtistEnabled()

    const artistRow = (
      <div
        className="artist-row"
        id={`artist_row_${artist.artist_name}`}
        key={`artist_row_${index}`}
      >
        <div className="artist-table_header artist-table_header_name">
          <strong>{window.translations.songs_app.artist_creative_name}</strong>
        </div>

        <div className="artist-song-name">
          <ArtistSongName
            showSongwriterWarning={this.state.showSongwriterWarning}
            artist={artist}
            onChange={this.handleArtistNameChange}
            handleClearStyleGuideWarning={this.handleClearStyleGuideWarning}
            remainingArtists={this.props.remainingArtists}
            isEditable={isEditable}
          />
        </div>

        <div className="artist-table_header artist-table_header_role">
          <strong>{window.translations.songs_app.role}</strong>&nbsp;
          <Tooltip>{RoleInfoTooltip}</Tooltip>
        </div>

        <div className="artist-song-role-select">
          <ArtistSongRoleSelect
            showSongwriterWarning={this.state.showSongwriterWarning}
            artist={artist}
            onRoleChange={this.handleRoleChange}
            onCreditChange={this.handleCreditChange}
            moreThanOneSongwriter={moreThanOneSongwriter}
            mainArtistRemovable={this.isRoleRemovable()}
            toggleSongwriterWarning={this.toggleSongwriterWarning}
          />
        </div>
        {this.isArtistRowRemovable() && (
          <div className="remove-artist-container">
            <button className="remove-artist" onClick={this.handleRemoveArtist}>
              <i className="fa fa-remove" />
            </button>
          </div>
        )}
      </div>
    )

    if (this.state.showSongwriterWarning) {
      return [
        artistRow,
        <div key={`artist_row_${artist.artist_name}_message`}>
          <Message
            level={'warning'}
            onToggleMessage={this.toggleSongwriterWarning}
            timed={true}
          >
            <p>
              {window.translations.songs_app.songwriter_removal_error_message}
            </p>
          </Message>
        </div>,
      ]
    } else if (this.showMissingFieldError()) {
      return [
        artistRow,
        <div
          className={index == 0 ? 'first-artist-error' : ''}
          key={`artist_row_${artist.artist_name}_message`}
        >
          <Message level={'error'}>
            <p>{window.translations.songs_app.artist_creative_fields_error}</p>
          </Message>
        </div>,
      ]
    } else if (
      artist.styleGuideWarnings &&
      artist.styleGuideWarnings.artist_name
    ) {
      return [
        artistRow,
        <div
          className={index == 0 ? 'first-artist-error' : ''}
          key={`artist_row_${artist.artist_name}_message`}
        >
          <Message level={'error'}>
            <p>
              {this.styleGuideWarning(artist.styleGuideWarnings.artist_name)}
            </p>
          </Message>
        </div>,
      ]
    }

    return artistRow
  }
}

export default ArtistRow
