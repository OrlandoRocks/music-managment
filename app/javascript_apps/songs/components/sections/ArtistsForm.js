import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import replace from 'react-string-replace'
import ArtistRow from './ArtistRow'
import Message from '../Message'
import spaceship from '../../utils/spaceship'

class ArtistsForm extends React.Component {
  static propTypes = {
    onAddArtist: PropTypes.func.isRequired,
    onEditArtist: PropTypes.func.isRequired,
    onRemoveArtist: PropTypes.func.isRequired,
    songwriterRoleId: PropTypes.string.isRequired,
    isFirstSong: PropTypes.bool,
  }

  state = {
    remainingArtists: this.reduceRemainingArtists(this.props),
  }

  constructor(props) {
    super(props)
    const {
      album: { is_various: isVarious },
      artists,
    } = this.props
    if (artists.length === 0 && isVarious) {
      this.props.onAddArtist()
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ remainingArtists: this.reduceRemainingArtists(nextProps) })
  }

  moreThanOneSongwriter() {
    return this.numSongwriters() > 1
  }

  noSongwriter() {
    return this.props.songId && this.numSongwriters() == 0
  }

  numSongwriters() {
    return this.props.artists.filter((artist) =>
      artist.role_ids.includes(this.props.songwriterRoleId)
    ).length
  }

  numPrimaryArtists() {
    return this.props.artists.filter(
      (artist) => artist.credit === 'primary_artist'
    ).length
  }

  moreThanOnePrimaryArtist() {
    return this.numPrimaryArtists() > 1
  }

  artistSorter(a, b) {
    const artistAssociationA = a.associated_to.toUpperCase()
    const artistAssociationB = b.associated_to.toUpperCase()
    return spaceship(artistAssociationA, artistAssociationB)
  }

  artistRows() {
    const albumType = this.props.album.album_type
    let moreThanOneSongwriter = this.moreThanOneSongwriter()
    let moreThanOnePrimaryArtist = this.moreThanOnePrimaryArtist()
    const isVariousArtistAlbum = this.props.album.is_various
    const isDJRelease = this.props.album.is_dj_release

    return this.props.artists
      .sort(this.artistSorter)
      .map((artist, index) => (
        <ArtistRow
          key={index}
          index={index}
          onEditArtist={this.props.onEditArtist}
          onRemoveArtist={this.props.onRemoveArtist}
          artist={artist}
          moreThanOneSongwriter={moreThanOneSongwriter}
          songwriterRoleId={this.props.songwriterRoleId}
          remainingArtists={this.state.remainingArtists}
          moreThanOnePrimaryArtist={moreThanOnePrimaryArtist}
          isVariousArtistAlbum={isVariousArtistAlbum}
          isDJRelease={isDJRelease}
          albumType={albumType}
        />
      ))
  }

  reduceRemainingArtists(props) {
    const artistNames = props.artists.map((artist) => artist.artist_name)
    return props.userArtists
      .filter((artistName) => {
        return !artistNames.includes(artistName)
      })
      .filter((artistName) => artistName != undefined)
  }

  render() {
    const {
      onAddArtist,
      isFirstSong,
      canCopyPreviousArtists,
      onCopyArtistsFromPrevious,
    } = this.props
    const isVariousArtistAlbum = this.props.album.is_various

    return (
      <div>
        <fieldset className="artists-creatives_fieldset">
          <legend className="artists-creatives_legend">
            {isVariousArtistAlbum
              ? window.translations.songs_app.song_artists_creatives_required
              : window.translations.songs_app.song_artists_creatives}
          </legend>
          <p>
            {replace(
              window.translations.songs_app.song_role_list,
              '{{FAQ}}',
              (match, i) => {
                return (
                  <a
                    key={i}
                    target="_blank"
                    rel="noopener noreferrer"
                    href={window.translations.songwriter_song_roles_info}
                  >
                    {window.translations.songs_app.faq}
                  </a>
                )
              }
            )}
          </p>
          <div className="artist-table">{this.artistRows()}</div>
          {this.noSongwriter() && (
            <div key={`artist_form_songwriter_message`} className="artist-form">
              <Message level={'warning'}>
                <p>{window.translations.songs_app.songwriter_role_message}</p>
              </Message>
            </div>
          )}

          <button className="add-artist" onClick={onAddArtist}>
            <i className="fa fa-plus" />
            &nbsp;{window.translations.songs_app.add_artist_creative}
          </button>
          {!isFirstSong ? (
            <button
              disabled={!canCopyPreviousArtists}
              className="copy-artists"
              onClick={onCopyArtistsFromPrevious}
            >
              <span className="copy-from-previous-icon"></span>
              {window.translations.songs_app.copy_from_previous}
            </button>
          ) : null}
        </fieldset>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  return {
    artists: storeState.currentSong.data.artists.map(
      (artistId) => storeState.currentCreatives[artistId]
    ),
    album: storeState.album,
    userArtists: storeState.artists,
    songwriterRoleId: storeState.songwriterRoleId,
  }
}

export default connect(mapStateToProps)(ArtistsForm)
