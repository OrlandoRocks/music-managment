import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import DOMPurify from 'dompurify'

import { UPDATE_COPYRIGHTS } from '../../actions/actionTypes'

const COMPOSITION_NAME = 'composition'
const ERROR_CLASS = ' missing'
const NO_ERROR_CLASS = ''
const RECORDING_NAME = 'recording'

// Translations
const COMPOSITION_COPYRIGHT = 'composition_copyright'
const COPYRIGHT_REQUIRED_MESSAGE = 'copyright_required_message'
const COPYRIGHT_SECTION_TITLE = 'copyright_section_title'
const RECORDING_COPYRIGHT = 'recording_copyright'

const safeTranslation = (key) => {
  const value = window.translations.songs_app[key]
  if (!value) return ''
  return DOMPurify.sanitize(window.translations.songs_app[key])
}

function CopyrightField({
  errorClass,
  handleOnChange,
  name,
  translationKey,
  value,
}) {
  return (
    <div className={`song-simple-copyrights song-checkbox${errorClass}`}>
      {errorClass && (
        <p style={{ color: 'red', fontWeight: 'bold' }}>
          You must agree and check this box in order to distribute.
        </p>
      )}
      <input
        id={`song-copyrights-${name}`}
        checked={value}
        name={name}
        onChange={handleOnChange}
        type="checkbox"
      />
      <label
        dangerouslySetInnerHTML={{
          __html: safeTranslation(translationKey),
        }}
        htmlFor={`song-copyrights-${name}`}
      />
    </div>
  )
}

CopyrightField.proptypes = {
  errorClass: PropTypes.string.isRequired,
  handleOnChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
}

class SimpleCopyrightsForm extends React.Component {
  static propTypes = {
    copyrights: PropTypes.object.isRequired,
  }

  updateCopyrights = (e) => {
    const { dispatch } = this.props
    const type = e.target.name
    const value = e.target.checked

    dispatch({ type: UPDATE_COPYRIGHTS, payload: { type, value } })
  }

  render() {
    const {
      copyrights: { composition, recording },
      errors,
      simpleCopyrightsFormEnabled,
    } = this.props

    const compositionError = errors.compositionCopyright
      ? ERROR_CLASS
      : NO_ERROR_CLASS
    const recordingError = errors.recordingCopyright
      ? ERROR_CLASS
      : NO_ERROR_CLASS

    if (!simpleCopyrightsFormEnabled) return null

    return (
      <div className="copyright-form">
        <fieldset className="artists-creatives_fieldset">
          <legend
            className="artists-creatives_legend"
            dangerouslySetInnerHTML={{
              __html: safeTranslation(COPYRIGHT_SECTION_TITLE),
            }}
          />
          <p
            dangerouslySetInnerHTML={{
              __html: safeTranslation(COPYRIGHT_REQUIRED_MESSAGE),
            }}
          />
          <CopyrightField
            errorClass={compositionError}
            handleOnChange={this.updateCopyrights}
            name={COMPOSITION_NAME}
            translationKey={COMPOSITION_COPYRIGHT}
            value={composition}
          />
          <CopyrightField
            errorClass={recordingError}
            handleOnChange={this.updateCopyrights}
            name={RECORDING_NAME}
            translationKey={RECORDING_COPYRIGHT}
            value={recording}
          />
        </fieldset>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const {
    currentSong: {
      data: { copyrights },
    },
  } = storeState

  return {
    copyrights: copyrights || {},
  }
}

export default connect(mapStateToProps)(SimpleCopyrightsForm)
