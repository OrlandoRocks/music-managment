import React from 'react'
import PropTypes from 'prop-types'
import {
  Tooltip,
  TooltipContainer,
  TooltipColumn,
  TooltipTitle,
} from '../../../shared/Tooltip'

const PreviouslyReleasedTooltip = (
  <TooltipContainer>
    <TooltipTitle>
      {window.translations.songs_app.previously_released}
    </TooltipTitle>
    <TooltipColumn>
      {window.translations.songs_app.previously_released_tooltip_text}
    </TooltipColumn>
  </TooltipContainer>
)

class PreviouslyReleased extends React.Component {
  static propTypes = {
    onAttributeUpdate: PropTypes.func.isRequired,
    isPreviouslyReleased: PropTypes.bool,
    previouslyReleasedDate: PropTypes.string,
    albumPreviouslyReleasedDate: PropTypes.string,
    albumIsSingle: PropTypes.bool,
    errors: PropTypes.object,
  }

  static defaultProps = {
    isPreviouslyReleased: false,
    errors: {},
  }

  handleCheckboxChange = (e) => {
    const is_previously_released = e.target.checked

    this.props.onAttributeUpdate({ is_previously_released })

    if (!is_previously_released && this.props.previouslyReleasedDate !== null) {
      this.props.onAttributeUpdate({ previously_released_at: null })
    }
  }

  handleDateChange = (e) => {
    this.props.onAttributeUpdate({ previously_released_at: e.target.value })
  }

  previouslyReleasedDate() {
    return (
      this.props.previouslyReleasedDate ||
      this.props.albumPreviouslyReleasedDate ||
      ''
    )
  }

  originalReleaseDateClassName() {
    if (this.props.errors.previously_released_at) {
      return 'missing'
    }
  }

  shouldShowPreviouslyReleasedCheckbox() {
    return (
      !this.props.albumIsSingle ||
      (this.props.albumIsSingle && this.props.albumPreviouslyReleasedDate)
    )
  }

  today() {
    return window.moment().format('YYYY-MM-DD')
  }

  minDate() {
    return window.moment().format('1900-01-01')
  }

  render() {
    const { albumPreviouslyReleasedDate, isPreviouslyReleased } = this.props
    const disabled =
      albumPreviouslyReleasedDate !== null && albumPreviouslyReleasedDate !== ''

    const renderedComponents = [
      this.shouldShowPreviouslyReleasedCheckbox() && (
        <div
          key="previously_released_checkbox"
          className="row song-previously-released song-checkbox"
        >
          <input
            id="previouslyReleased"
            type="checkbox"
            value=""
            disabled={disabled}
            checked={isPreviouslyReleased}
            onChange={this.handleCheckboxChange}
          />
          <label htmlFor="previouslyReleased">
            {window.translations.songs_app.previously_released}
          </label>
          <Tooltip>{PreviouslyReleasedTooltip}</Tooltip>
        </div>
      ),
      isPreviouslyReleased && !albumPreviouslyReleasedDate && (
        <div
          key="previously_released_date"
          className="row song-previously-released-date"
        >
          <label htmlFor="previouslyReleasedDate">
            {window.translations.songs_app.original_release_date}
            <span className="red">*</span>
          </label>
          <h4 className="song-previously-released-error red">
            {this.props.errors.previously_released_at}
          </h4>
          <input
            id="previouslyReleasedDate"
            type="date"
            min={this.minDate()}
            max={this.today()}
            className={this.originalReleaseDateClassName()}
            value={this.previouslyReleasedDate()}
            onChange={this.handleDateChange}
            placeholder={window.translations.songs_app.date_format}
          />
        </div>
      ),
    ]

    return renderedComponents.filter((component) => component !== null)
  }
}

export default PreviouslyReleased
