import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import SongwriterRow from './SongwriterRow'
import {
  addSongwriter,
  removeSongwriter,
  updateSongwriter,
} from '../../actions/artistActions'

class SongwriterForm extends React.Component {
  static propTypes = {
    onCopySongwriterFromPrevious: PropTypes.func.isRequired,
    canCopyPreviousSongwriters: PropTypes.bool,
    trackNum: PropTypes.number,
    isFirstSong: PropTypes.bool,
  }

  handleSongwriterChange = (songwriter) => {
    this.props.dispatch(updateSongwriter(songwriter))
  }

  handleSongwriterAdd = (e) => {
    e.preventDefault()
    this.props.dispatch(addSongwriter(this.props.songwriters.length))
  }

  handleSongwriterRemove = (songwriterUuid) => {
    this.props.dispatch(removeSongwriter(songwriterUuid))
  }

  render() {
    const {
      isFirstSong,
      canCopyPreviousSongwriters,
      onCopySongwriterFromPrevious,
    } = this.props
    return (
      <div className="songwriter-form">
        <fieldset className="artists-creatives_fieldset">
          <legend className="artists-creatives_legend">
            {window.translations.songs_app.songwriter_section_title} &nbsp;
          </legend>
          <p>
            {window.translations.songs_app.songwriter_role_required_message}
          </p>

          <div className="artist-table">
            {this.props.songwriters
              .sort((songwrtierA, songwrtierB) => {
                return songwrtierA.index - songwrtierB.index
              })
              .map((songwriter) => {
                return (
                  <SongwriterRow
                    possibleSongwriters={this.props.possibleSongwriterNames}
                    hasSongwriterError={this.props.hasSongwriterError}
                    key={songwriter.uuid}
                    onChange={this.handleSongwriterChange}
                    onRemove={this.handleSongwriterRemove}
                    songwriter={songwriter}
                  />
                )
              })}
          </div>

          <button
            className="add-artist add-songwriter"
            onClick={this.handleSongwriterAdd}
          >
            <i className="fa fa-plus" />
            &nbsp;
            {window.translations.songs_app.songwriter_add_songwriter_button}
          </button>

          {!isFirstSong ? (
            <button
              disabled={!canCopyPreviousSongwriters}
              className="copy-songwriters"
              onClick={onCopySongwriterFromPrevious}
            >
              <span className="copy-from-previous-icon"></span>
              {window.translations.songs_app.copy_from_previous}
            </button>
          ) : null}
        </fieldset>
      </div>
    )
  }
}

function mapStateToProps(storeState) {
  const { currentSong } = storeState
  const songwriterNames = Object.values(storeState.currentSongwriters).map(
    (songwriter) => songwriter.name
  )
  const possibleSongwriterNames = storeState.songwriterNames
    .filter((artistName) => {
      return !songwriterNames.includes(artistName)
    })
    .filter((artistName) => artistName != undefined)
  return {
    hasSongwriterError: currentSong.errors.has_songwriter,
    possibleSongwriterNames,
    songwriters: Object.values(storeState.currentSongwriters),
  }
}

export default connect(mapStateToProps)(SongwriterForm)
