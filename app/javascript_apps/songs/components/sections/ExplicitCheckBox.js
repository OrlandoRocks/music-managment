import React from 'react'
import PropTypes from 'prop-types'

class ExplicitCheckBox extends React.Component {
  static propTypes = {
    isExplicit: PropTypes.bool.isRequired,
    onAttributeUpdate: PropTypes.func.isRequired,
  }

  onChange = (e) => {
    this.props.onAttributeUpdate({ explicit: e.target.checked })
  }

  render() {
    return (
      <div className="row song-explicit song-checkbox">
        <input
          id="explicit"
          type="checkbox"
          checked={this.props.isExplicit}
          onChange={this.onChange}
        />
        <label htmlFor="explicit">
          <b className="song-explicit_warn">
            {window.translations.songs_app.explicit}
          </b>{' '}
          - {window.translations.songs_app.explicit_description}
        </label>
      </div>
    )
  }
}

export default ExplicitCheckBox
