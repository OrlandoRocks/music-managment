import React from 'react'
import PropTypes from 'prop-types'
import MediaPlayer from './MediaPlayer'
import UploadSongButton from '../UploadSongButton'

class AssetInformation extends React.Component {
  static propTypes = {
    song: PropTypes.object.isRequired,
    onSongUpload: PropTypes.func.isRequired,
    assetUrl: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
  }

  render() {
    const { assetUrl, song, onSongUpload, disabled } = this.props
    return (
      <div>
        <MediaPlayer uuid={song.uuid} assetUrl={assetUrl} />
        <div className="replace-audio-file">
          <UploadSongButton
            disabled={disabled}
            className="song-file-upload-button pointer"
            uuid={song.uuid}
            onSongUpload={onSongUpload}
            assetUrl={assetUrl}
          >
            <a className={`${disabled ? 'disabled' : ''}`}>
              <i className="fa fa-refresh" />
              {window.translations.songs_app.replace_audio_file}
            </a>
          </UploadSongButton>
        </div>
      </div>
    )
  }
}

export default AssetInformation
