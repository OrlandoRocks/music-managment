import React from 'react'

const ProgressSpinner = ({ uuid, size, text, textClass }) => {
  const sizeClass = size && (size === 2 || size === 3) ? `fa-${size}x` : ''
  return (
    <div id={`song_progress_spinner_${uuid}`} className="song-progress-spinner">
      <i className={`progress-spinner fa fa-spinner ${sizeClass} fa-pulse`} />
      {text && <div className={textClass}>{text}</div>}
    </div>
  )
}

export default ProgressSpinner
