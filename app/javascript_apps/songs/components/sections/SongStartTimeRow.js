import React from 'react'
import PropTypes from 'prop-types'

const selectionOptions = (maxNumber) => {
  return Array(maxNumber)
    .fill()
    .map((_, i) => {
      const value = String(i)
      const paddedValue = value.padStart(2, '0')
      return (
        <option key={value} value={value}>
          {paddedValue}
        </option>
      )
    })
}

class SongStartTimeRow extends React.Component {
  static propTypes = {
    songStartTime: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
  }

  handleTimeChange = (minutes, seconds) => {
    const min = parseInt(minutes)
    const sec = parseInt(seconds)

    const value = min * 60 + sec

    this.props.onChange({
      ...this.props.songStartTime,
      start_time: value,
    })
  }

  render() {
    const { start_time, uuid } = this.props.songStartTime
    const { store_id } = start_time
    const minutes = Math.floor(start_time / 60)
    const seconds = start_time - minutes * 60

    return (
      <div>
        <div className="song-start-time-text-container">
          <div className="selector-heading">
            {window.translations.songs_app.song_start_time.minutes}
          </div>
          <div className="dropsdown">
            <select
              id={`song_start_time_minutes_${store_id}_${uuid}`}
              style={{ display: 'inline-block' }}
              name={'start_time'}
              value={minutes}
              onChange={(e) => this.handleTimeChange(e.target.value, seconds)}
            >
              {selectionOptions(10)}
            </select>
          </div>
        </div>
        <div className="song-start-time-text-container">:</div>
        <div className="song-start-time-text-container">
          <div className="selector-heading">
            {window.translations.songs_app.song_start_time.seconds}
          </div>
          <div className="dropsdown">
            <select
              id={`song_start_time_seconds_${store_id}_${uuid}`}
              style={{ display: 'inline-block' }}
              name={'start_time'}
              value={seconds}
              onChange={(e) => this.handleTimeChange(minutes, e.target.value)}
            >
              {selectionOptions(60)}
            </select>
          </div>
        </div>
      </div>
    )
  }
}

export default SongStartTimeRow
