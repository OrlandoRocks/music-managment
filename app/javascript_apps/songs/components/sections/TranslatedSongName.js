import React from 'react'
import PropTypes from 'prop-types'
import Message from '../Message'

const TranslatedSongName = ({
  errors,
  translatedName,
  handleChange,
  language,
}) => {
  const {
    songs_app: {
      song_title_transliteration: songTransliteration,
      song_title_translation: songTranslation,
    },
  } = window.translations
  let label =
    language.description == 'Hebrew' ? songTransliteration : songTranslation

  return (
    <div className="row song-translated-name song-input">
      <label className="translated-name-label">
        {label}
        <span className="red">*</span>
      </label>
      <input
        className={errors && errors.translated_name ? 'missing' : ''}
        type="text"
        value={translatedName}
        placeholder={
          window.translations.songs_app.translated_song_name_placeholder
        }
        onChange={handleChange}
      />
      {errors && errors.translated_name && (
        <div key={`translated-song-name_${translatedName}_message`}>
          <Message level={'error'}>
            <p>
              {
                window.translations.songs_app
                  .must_provide_english_track_title_translation
              }
            </p>
          </Message>
        </div>
      )}
    </div>
  )
}

TranslatedSongName.propTypes = {
  errors: PropTypes.object,
  translatedName: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
}

export default TranslatedSongName
