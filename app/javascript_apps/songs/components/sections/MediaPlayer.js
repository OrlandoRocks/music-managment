import React from 'react'

const MediaPlayer = ({ assetUrl, uuid }) => {
  return (
    <div id={`song_media_player_${uuid}`} className="song-media-player">
      <audio className="media-player" controls preload="none" src={assetUrl}>
        <source src={assetUrl} type="audio/mp3" />
        {window.translations.songs_app.browser_unsupported_audio_element}
      </audio>
    </div>
  )
}

export default MediaPlayer
