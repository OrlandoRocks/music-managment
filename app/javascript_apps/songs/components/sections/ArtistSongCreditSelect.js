import React from 'react'
import PropTypes from 'prop-types'

const credits = [
  { value: '', name: window.translations.songs_app.choose },
  { value: 'primary_artist', name: window.translations.songs_app.primary },
  { value: 'featuring', name: window.translations.songs_app.featured_feat },
  { value: 'contributor', name: window.translations.songs_app.contributor },
]

const ArtistSongCreditSelect = ({
  artist,
  onChange,
  showSongwriterWarning,
  isEditable,
}) => {
  if (!isEditable) {
    return (
      <div className="primary_artist_input">
        <input
          className="artist-song-credit-select_no_edit"
          disabled={true}
          value={window.translations.songs_app.primary}
        />
      </div>
    )
  }

  return (
    <div className="dropsdown">
      <select
        onChange={onChange}
        value={artist.credit === 'with' ? 'featuring' : artist.credit}
        className={`artist-song-credit-select-element ${
          artist.errors && artist.errors.credit_blank ? 'missing' : ''
        } ${showSongwriterWarning ? 'warning' : ''}`}
      >
        {credits.map((credit) => (
          <option key={credit.value} value={credit.value}>
            {credit.name}
          </option>
        ))}
      </select>
    </div>
  )
}

ArtistSongCreditSelect.propTypes = {
  artist: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  showSongwriterWarning: PropTypes.bool.isRequired,
  isEditable: PropTypes.bool.isRequired,
}

export default ArtistSongCreditSelect
