import React from 'react'
import PropTypes from 'prop-types'
import Autocomplete from 'react-autocomplete'
import escapeStringRegexp from 'escape-string-regexp'

const menuStyle = {
  borderRadius: '3px',
  boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
  background: 'rgba(255, 255, 255, 0.9)',
  padding: '2px 0',
  fontSize: '14px',
  overflow: 'auto',
  width: '91%',
  zIndex: '998',
  position: 'absolute',
}

const renderMenu = (items) => {
  return (
    <div className="autocomplete-dropdown-menu" style={{ ...menuStyle }}>
      {items}
    </div>
  )
}

class SongwriterRow extends React.Component {
  static propTypes = {
    songwriter: PropTypes.object.isRequired,
    hasSongwriterError: PropTypes.bool,
    possibleSongwriters: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
  }

  handleChange = (e) => {
    this.props.onChange({
      ...this.props.songwriter,
      name: e.target.value,
    })
  }

  handleSelect = (name) => {
    this.props.onChange({
      ...this.props.songwriter,
      name,
    })
  }

  handleRemove = () => {
    this.props.onRemove(this.props.songwriter.uuid)
  }

  getSongwriterForAutocomplete = (songwriter) => {
    return songwriter
  }

  renderSongwriter = (songwriter, isHighlighted) => {
    const style = {
      background: isHighlighted ? 'lightgray' : 'white',
      padding: '5px',
    }

    return (
      <div key={songwriter} style={style}>
        {songwriter}
      </div>
    )
  }

  shouldDisplaySongwriter = (songwriter) => {
    const inputSongwriter = escapeStringRegexp(
      this.props.songwriter.name.toLowerCase()
    )
    return (songwriter || '').toLowerCase().match(inputSongwriter)
  }

  render() {
    return (
      <div
        className="songwriter-text-container"
        id={`songwriter_${this.props.songwriter.uuid}`}
      >
        <Autocomplete
          wrapperStyle={{ display: 'inline-block', width: '95%' }}
          menuStyle={menuStyle}
          items={this.props.possibleSongwriters}
          getItemValue={this.getSongwriterForAutocomplete}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
          renderItem={this.renderSongwriter}
          value={this.props.songwriter.name}
          shouldItemRender={this.shouldDisplaySongwriter}
          renderMenu={renderMenu}
          inputProps={{
            autoComplete: 'off',
            placeholder:
              window.translations.songs_app.songwriter_input_placeholder,
            className: `songwriter-text-field ${
              this.props.hasSongwriterError ? 'missing' : ''
            }`,
          }}
        />
        {this.props.songwriter.index === 0 ? null : (
          <button className="songwriter-remove-btn" onClick={this.handleRemove}>
            <i className="fa fa-remove" />
          </button>
        )}
      </div>
    )
  }
}

export default SongwriterRow
