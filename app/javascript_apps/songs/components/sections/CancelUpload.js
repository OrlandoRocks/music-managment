import React from 'react'
import PropTypes from 'prop-types'

const CancelUpload = ({ children, className, onCancelUpload, uuid }) => {
  return (
    <div
      onClick={onCancelUpload}
      id={`song_cancel_upload_${uuid}`}
      className={`cancel-upload ${className}`}
    >
      <i className={`fa fa-times cancel-upload_icon`} />
      {children}
    </div>
  )
}

CancelUpload.propTypes = {
  className: PropTypes.string,
  onCancelUpload: PropTypes.func.isRequired,
  uuid: PropTypes.string.isRequired,
}

export default CancelUpload
