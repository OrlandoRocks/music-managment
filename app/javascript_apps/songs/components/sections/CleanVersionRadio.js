import React from 'react'
import PropTypes from 'prop-types'
import ErrorMessage from '../ErrorMessage'

class CleanVersionRadio extends React.Component {
  static propTypes = {
    onAttributeUpdate: PropTypes.func.isRequired,
  }

  onRadioChange = (e) => {
    const { value } = e.target

    const booleanValue = value == 'Yes'

    this.props.onAttributeUpdate({ clean_version: booleanValue })
  }

  render() {
    const { isCleanVersion, errors } = this.props
    const cleanVersionError = errors && errors.clean_version

    return (
      <div className="row song-clean-version">
        <p class="clean-version-radio-warning">
          {window.translations.songs_app.clean_version_warning}
        </p>
        <div className="song-clean-version song-clean-version-prompt">
          <b>{window.translations.songs_app.clean_version_prompt}</b>
        </div>
        <div className="song-clean-version song-radio-toolbar">
          <input
            id="song-clean-version-yes"
            type="radio"
            name="clean_version"
            value="Yes"
            checked={isCleanVersion == true}
            onChange={this.onRadioChange}
          />
          <label
            className="song-clean-version"
            htmlFor="song-clean-version-yes"
          >
            {window.translations.songs_app.button_yes}
          </label>
          <input
            id="song-clean-version-no"
            type="radio"
            name="clean_version"
            value="No"
            checked={isCleanVersion == false}
            onChange={this.onRadioChange}
          />
          <label className="song-clean-version" htmlFor="song-clean-version-no">
            {window.translations.songs_app.button_no}
          </label>
        </div>
        {cleanVersionError && (
          <div className="song-clean-version-error">
            <ErrorMessage
              message={window.translations.songs_app.clean_version_error}
            />
          </div>
        )}
      </div>
    )
  }
}

export default CleanVersionRadio
