import React from 'react'

const ProgressBar = ({ uuid, className }) => {
  return (
    <div id={`song_progress_bar_${uuid}`} className="song-progress-bar">
      <progress className={className} />
    </div>
  )
}

export default ProgressBar
