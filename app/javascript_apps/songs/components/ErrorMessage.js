import React from 'react'
import PropTypes from 'prop-types'

const errorMessageContainerStyle = {
  marginTop: '30px',
}

const errorMessageTextStyle = {
  backgroundColor: '#D0021B',
  color: '#fff',
  padding: '10px',
}

class ErrorMessage extends React.Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
  }

  render() {
    return (
      <div
        className="error-message-container"
        style={errorMessageContainerStyle}
      >
        <span
          className="error-message-text missing"
          style={errorMessageTextStyle}
        >
          {this.props.message}
        </span>
      </div>
    )
  }
}

export default ErrorMessage
