import {
  S3_UPLOAD_SONG,
  S3_UPLOAD_SPATIAL_AUDIO_SONG,
} from '../actions/actionTypes'

import {
  invalidSongAttributes,
  songIsNotLoading,
  songIsNotUploading,
  uploadFinished,
  validateAsset,
  validateSpatialAudioAsset,
} from '../actions/songActions'

window.requests = {}

const uploadSongMiddleware = ({ dispatch, getState }) => (next) => (action) => {
  if (![S3_UPLOAD_SONG, S3_UPLOAD_SPATIAL_AUDIO_SONG].includes(action.type)) {
    return next(action)
  }

  const { bucket_name, key_name, file, presigned_url, song } = action.data
  const { currentSong } = getState()

  if (
    key_name.endsWith('.mp3') ||
    key_name.endsWith('.flac') ||
    key_name.endsWith('.wav')
  ) {
    const request = jQuery.ajax({
      url: presigned_url,
      data: file,
      type: 'PUT',
      cache: false,
      processData: false,
      headers: {
        'Content-Type': file.type,
      },
    })
    window.requests[song.uuid] = request
    request.done(() => {
      window.requests[song.uuid] = null
      dispatch(uploadFinished(song.uuid))
      const noCurrentSongOrCurrentSongNotCancelled =
        !currentSong || (currentSong && !currentSong.cancelUpload)
      if (song.data.id && noCurrentSongOrCurrentSongNotCancelled) {
        if (action.type == S3_UPLOAD_SONG) {
          dispatch(validateAsset(song, key_name, bucket_name))
        } else if (action.type == S3_UPLOAD_SPATIAL_AUDIO_SONG) {
          dispatch(validateSpatialAudioAsset(song, key_name, bucket_name))
        }
      } else {
        dispatch(songIsNotUploading(song))
        return next({ ...action, asset })
      }
    })

    request.fail(() => {
      dispatch(songIsNotLoading(song))
      dispatch(songIsNotUploading(song))
      dispatch(invalidSongAttributes({ asset_url: 'upload failed' }, song))
    })
  } else {
    dispatch(songIsNotLoading(song))
    dispatch(songIsNotUploading(song))
    dispatch(invalidSongAttributes({ asset_url: 'upload failed' }, song))
  }
}

export default uploadSongMiddleware
