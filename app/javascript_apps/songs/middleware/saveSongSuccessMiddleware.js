import { CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import { registerAsset } from '../actions/songActions'
import { normalizeSongs } from '../store/schema'
import buildSongDataFromApi from '../utils/buildSongDataFromApi'

const saveSongSuccessMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (![CREATE_SONG, UPDATE_SONG].includes(action.type)) {
    return next(action)
  }

  if (
    action.type === CREATE_SONG &&
    action.actionParams &&
    action.actionParams.asset_data
  ) {
    const { song } = action
    let songWithAsset = {
      ...action.payload.actionParams,
      data: {
        ...action.payload.actionParams.data,
        id: song.data.id,
      },
    }
    let options = { shouldCloseForm: true, shouldNotShowProgressBar: true }
    dispatch(registerAsset(songWithAsset.asset_data, songWithAsset, options))
  }

  const { songwriterRoleId } = getState()
  let song = buildSongDataFromApi(action.data, songwriterRoleId)
  const {
    entities: { songs, creatives, songwriters, songStartTimes },
  } = normalizeSongs([song])

  song = Object.values(songs)[0]
  if (action.type === UPDATE_SONG) {
    song.uuid = action.data.uuid
  }

  return next({
    ...action,
    song,
    albumType: getState().album.album_type,
    creatives,
    songwriters,
    songStartTimes,
  })
}

export default saveSongSuccessMiddleware
