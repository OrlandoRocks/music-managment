import updateSongs from '../utils/updateSongs'
import emitUpdatePreorderCalendarEvent from '../utils/emitUpdatePreorderCalendarEvent'
import eventEmitActions from '../utils/eventEmitActions'
import songsForPreorderCalendar from '../utils/songsForPreorderCalendar'

const preorderCalendarDispatcherMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (eventEmitActions.includes(action.type)) {
    const { songs, creatives } = getState()
    const updatedSongs = updateSongs(action, songs)
    const songsData = songsForPreorderCalendar(updatedSongs, {
      ...creatives,
      ...action.creatives,
    })
    emitUpdatePreorderCalendarEvent(songsData)
  }
  return next(action)
}

export default preorderCalendarDispatcherMiddleware
