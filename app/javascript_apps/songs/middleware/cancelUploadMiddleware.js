import { CANCEL_SONG_UPLOAD } from '../actions/actionTypes'

const cancelUploadMiddleware = () => (next) => (action) => {
  if (action.type !== CANCEL_SONG_UPLOAD) {
    return next(action)
  }
  const request = window.requests[action.song.uuid]

  request && request.abort()

  window.requests[action.song.uuid] = null

  return next(action)
}

export default cancelUploadMiddleware
