import { UPDATE_CURRENT_SONG } from '../actions/actionTypes'
import {
  styleGuideWarning,
  clearStyleGuideWarning,
} from '../actions/styleGuideActions'
import songNameStyleGuideValidator from '../utils/songNameStyleGuideValidator'

const songStyleGuideMiddleware = ({ dispatch }) => (next) => (action) => {
  let warning
  if (updatingSongName(action)) {
    warning = songNameStyleGuideValidator(action.newAttributes.name)
    if (warning) {
      dispatch(styleGuideWarning('song_name', warning))
    } else {
      dispatch(clearStyleGuideWarning('song_name'))
    }
  }

  return next(action)
}

function updatingSongName(action) {
  return (
    action.type === UPDATE_CURRENT_SONG &&
    Object.keys(action.newAttributes).includes('name')
  )
}

export default songStyleGuideMiddleware
