import songComplete from '../utils/songComplete'
import updateSongs from '../utils/updateSongs'
import emitUpdateChecklistEvent from '../utils/emitUpdateChecklistEvent'
import eventEmitActions from '../utils/eventEmitActions'

const albumChecklistDispatcherMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (eventEmitActions.includes(action.type)) {
    let { songs, creatives, songwriterRoleId } = getState()
    creatives = { ...creatives, ...action.creatives }
    const songsToValidate = Object.values(updateSongs(action, songs))
    const songsPresent = songsToValidate.length > 0
    const completeSongs = songsToValidate.every((song) =>
      songComplete(
        {
          ...song,
          data: {
            ...song.data,
            artists: song.data.artists.map((artistId) => creatives[artistId]),
          },
        },
        songwriterRoleId
      )
    )
    emitUpdateChecklistEvent(songsPresent && completeSongs)
  }
  return next(action)
}

export default albumChecklistDispatcherMiddleware
