import { API, CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import buildSongDataForApi from '../utils/buildSongDataForApi'

const songSaveParameterBuilderMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (
    !(
      action.type === API &&
      [UPDATE_SONG, CREATE_SONG].includes(action.payload.success)
    )
  ) {
    return next(action)
  }

  const {
    currentSong,
    currentCreatives,
    currentSongwriters,
    currentSongStartTimes,
    songwriterRoleId,
  } = getState()
  const songData = buildSongDataForApi(
    currentSong,
    currentCreatives,
    currentSongwriters,
    songwriterRoleId,
    currentSongStartTimes
  )

  return next({
    ...action,
    payload: {
      ...action.payload,
      params: JSON.stringify({ song: songData }),
    },
  })
}

export default songSaveParameterBuilderMiddleware
