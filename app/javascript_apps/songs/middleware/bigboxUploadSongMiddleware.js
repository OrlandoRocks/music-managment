import { BIGBOX_UPLOAD_SONG } from '../actions/actionTypes'
import {
  registerAsset,
  songIsUploading,
  invalidSongAttributes,
  songIsNotLoading,
  songIsNotUploading,
  uploadFinished,
  resetSongErrors,
} from '../actions/songActions'

window.requests = {}
const MINIMUM_VALID_DURATION = 2001
const DURATION_ERROR = 'Duration is 2 seconds or less.'
const S3_SUBDOMAIN_URL = 'http://bigbox.tunecore.com.s3.amazonaws.com'
const S3_ACCESSIBLE_URL = 'https://s3.amazonaws.com/bigbox.tunecore.com'
const S3_QA_SUBDOMAIN_URL = 'http://tcc.tunecore.com.staging.s3.amazonaws.com'
const S3_QA_ACESSIBLE_URL =
  'https://s3.amazonaws.com:443/tcc.tunecore.com.staging'

const bigboxUploadSongMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (action.type !== BIGBOX_UPLOAD_SONG) {
    return next(action)
  }

  const { song, file } = action
  const { bigBoxUrl, tcPid, currentSong } = getState()

  dispatch(resetSongErrors(song))

  let formData = new FormData()
  formData.append('Filedata', file)
  formData.append('tc_pid', tcPid)
  formData.append('packager_class', 'BasicPackager')

  const request = jQuery.ajax({
    url: `//${bigBoxUrl}/uploader/create/`,
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
  })

  if (
    action.songShowsUploadingIndicator &&
    action.song &&
    !action.song.isUploading
  ) {
    dispatch(songIsUploading(action.song))
  }

  window.requests[action.song.uuid] = request
  request.done((bigBoxResponse) => {
    window.requests[action.song.uuid] = null
    dispatch(uploadFinished(action.song.uuid))
    let asset = JSON.parse(bigBoxResponse).response
    asset.streaming_url = handleStreamingUrl(asset.streaming_url)
    const isValidDuration = asset.duration >= MINIMUM_VALID_DURATION
    const noCurrentSongOrCurrentSongNotCancelled =
      !currentSong || (currentSong && !currentSong.cancelUpload)
    if (!isValidDuration) {
      dispatch(songIsNotUploading(action.song))
      dispatch(invalidSongAttributes({ duration: DURATION_ERROR }, action.song))
    } else if (song.data.id && noCurrentSongOrCurrentSongNotCancelled) {
      dispatch(registerAsset(asset, song))
    } else {
      dispatch(songIsNotUploading(song))
      return next({ ...action, asset })
    }
  })

  request.fail(() => {
    dispatch(songIsNotLoading(action.song))
    dispatch(songIsNotUploading(action.song))
    dispatch(invalidSongAttributes({ asset_url: 'upload failed' }, action.song))
  })
}

function handleStreamingUrl(asset_url) {
  if (typeof asset_url !== 'string') return asset_url

  if (asset_url.includes(S3_SUBDOMAIN_URL)) {
    return asset_url.replace(S3_SUBDOMAIN_URL, S3_ACCESSIBLE_URL)
  } else if (asset_url.includes(S3_QA_SUBDOMAIN_URL)) {
    return asset_url.replace(S3_QA_SUBDOMAIN_URL, S3_QA_ACESSIBLE_URL)
  } else {
    return asset_url
  }
}

export default bigboxUploadSongMiddleware
