import { UPDATE_ARTIST } from '../actions/actionTypes'
import updateArtist from '../utils/updateArtist'

const artistAttributeValidationMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (action.type !== UPDATE_ARTIST) {
    return next(action)
  }

  const { currentCreatives } = getState()

  const currentCreative = currentCreatives[action.artistUuid]

  const artist = updateArtist(action, currentCreative)
  next({
    ...action,
    artist,
  })
}

export default artistAttributeValidationMiddleware
