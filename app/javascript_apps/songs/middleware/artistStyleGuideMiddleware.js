import { UPDATE_ARTIST_NAME } from '../actions/actionTypes'
import artistNameStyleGuideValidator from '../utils/artistNameStyleGuideValidator'
import updateArtist from '../utils/updateArtist'

const artistStyleGuideMiddleware = ({ getState }) => (next) => (action) => {
  if (updatingArtistName(action)) {
    const state = getState()
    const { artists } = action
    let warning

    for (const creativeId in artists) {
      if (creativeId == action.artistUuid) {
        warning = artistNameStyleGuideValidator(
          action.newName,
          state,
          action.artistUuid,
          action
        )
      } else if (
        artists[creativeId].artist_name ===
          artists[action.artistUuid].artist_name &&
        artists[creativeId].artist_name !== action.newName
      ) {
        warning = artistNameStyleGuideValidator(
          artists[creativeId].artist_name,
          state,
          artists[creativeId].uuid,
          action
        )
      } else {
        continue
      }

      const currentCreative = artists[creativeId]

      artists[creativeId] = updateArtist(
        action,
        currentCreative,
        'artist_name',
        warning
      )
    }

    return next(action)
  }

  return next(action)
}

function updatingArtistName(action) {
  return action.type === UPDATE_ARTIST_NAME
}

export default artistStyleGuideMiddleware
