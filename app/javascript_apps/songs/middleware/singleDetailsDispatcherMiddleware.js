import emitSingleDetailsEvent from '../utils/emitSingleDetailsEvent'
import {
  CREATE_SONG,
  S3_UPLOAD_SONG,
  BIGBOX_UPLOAD_SONG,
  UPDATE_SONG,
} from '../actions/actionTypes'
import songTitleizer from '../utils/songTitleizer'
import toSentenceList from '../utils/toSentenceList'
import buildSongDataFromApi from '../utils/buildSongDataFromApi'

const singleDetailsDispatcherMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (
    [CREATE_SONG, UPDATE_SONG, S3_UPLOAD_SONG, BIGBOX_UPLOAD_SONG].includes(
      action.type
    ) &&
    action.data
  ) {
    const song = action.data
    let { songwriterRoleId } = getState()

    const songData = buildSongDataFromApi(song, songwriterRoleId).data
    const songTitle = songTitleizer(songData)
    const primaryArtists = songData.artists.filter(
      (artist) => artist.credit === 'primary_artist'
    )
    const primaryArtistNames = toSentenceList(
      primaryArtists.map((artist) => artist.artist_name),
      '&',
      false
    )

    emitSingleDetailsEvent(songTitle, primaryArtistNames)
  }
  return next(action)
}

export default singleDetailsDispatcherMiddleware
