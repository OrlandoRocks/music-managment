import eventEmitActions from '../utils/eventEmitActions'
import emitCheckoutButtonEvent from '../utils/emitCheckoutButtonEvent'

const albumCheckoutButtonDispatcherMiddleware = ({ getState }) => (next) => (
  action
) => {
  if (eventEmitActions.includes(action.type)) {
    const { album } = getState()
    emitCheckoutButtonEvent(album.id)
  }
  return next(action)
}

export default albumCheckoutButtonDispatcherMiddleware
