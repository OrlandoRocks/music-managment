import { ADD_SONG } from '../actions/actionTypes'

const trackNumberInjectionActions = [ADD_SONG]

const trackNumberInjectionMiddleware = ({ getState }) => (next) => (action) => {
  if (!trackNumberInjectionActions.includes(action.type)) {
    return next(action)
  }

  return next({
    ...action,
    trackNum: Object.keys(getState().songs).length + 1,
  })
}

export default trackNumberInjectionMiddleware
