import {
  API,
  REGISTER_ASSET,
  REGISTER_SPATIAL_AUDIO_ASSET,
  REMOVE_SONG,
  S3_UPLOAD_SONG,
  S3_UPLOAD_SPATIAL_AUDIO_SONG,
} from '../actions/actionTypes'
import {
  songIsLoading,
  songIsNotLoading,
  invalidSongAttributes,
  songIsNotUploading,
  songIsDeleting,
  resetSongErrors,
  songIsUploading,
  registerAsset,
  registerSpatialAudioAsset,
  saveStreamingAsset,
} from '../actions/songActions'
import {
  albumIsLoading,
  albumIsNotLoading,
  reloadAlbum,
} from '../actions/albumActions'

const apiMiddleware = ({ dispatch }) => (next) => (action) => {
  if (action.type !== API) {
    return next(action)
  }

  if (
    action.songShowsLoadingIndicator &&
    action.payload &&
    ![REGISTER_ASSET, REGISTER_SPATIAL_AUDIO_ASSET, REMOVE_SONG].includes(
      action.payload.success
    )
  ) {
    dispatch(songIsLoading(action.song))
  }

  if (action.albumShowsLoadingIndicator) {
    dispatch(albumIsLoading())
  }

  if (action.payload.success === REMOVE_SONG) {
    dispatch(songIsDeleting(action.song))
  }

  if (
    [S3_UPLOAD_SONG, S3_UPLOAD_SPATIAL_AUDIO_SONG].includes(
      action.payload.success
    )
  ) {
    dispatch(resetSongErrors(action.song))
    dispatch(songIsUploading(action.song))
  }

  jQuery.ajax({
    url: action.payload.url,
    type: action.payload.method,
    data: action.payload.params,
    dataType: 'json',
    contentType: 'application/json',
    success: function (data) {
      if (action.songShowsLoadingIndicator) {
        dispatch(songIsNotLoading(action.song))
      }

      if (action.songShowsUploadingIndicator) {
        dispatch(songIsNotUploading(action.song))
      }

      if (action.albumShowsLoadingIndicator) {
        dispatch(albumIsNotLoading())
      }

      if (action.reloadAlbum) {
        dispatch(reloadAlbum())
      }

      if (action.initiateRegisterAsset) {
        dispatch(registerAsset(data, action.song))
        dispatch(
          saveStreamingAsset(
            action.payload.actionParams.song,
            action.payload.actionParams.key_name,
            action.payload.actionParams.bucket_name
          )
        )
      }

      if (action.initiateRegisterSpatialAudioAsset) {
        dispatch(registerSpatialAudioAsset(data, action.song))
      }

      dispatch({
        type: action.payload.success,
        data: {
          ...action.payload.actionParams,
          ...data,
          defaultSong: action.defaultSong,
        },
      })
    },
    error: function (response) {
      action.song.upload_error = true
      dispatch(songIsNotLoading(action.song))
      dispatch(songIsNotUploading(action.song))
      try {
        const errors = JSON.parse(response.responseText).errors
        if (errors) {
          // Remove the upload_error tag so the upload error modal does not show
          delete action.song.upload_error
        }
        Object.keys(errors).forEach((attribute) => {
          dispatch(
            invalidSongAttributes(
              { [attribute]: errors[attribute] },
              action.song
            )
          )
        })
      } catch (e) {
        // TO DO: Log the exact that is not due to s3.
        invalidSongAttributes({ status: response.statusText }, action.song)
      }
    },
  })
}

export default apiMiddleware
