import { UPDATE_CURRENT_SONG, UPDATE_SONGWRITER } from '../actions/actionTypes'
import songErrorBuilder from '../utils/songErrorBuilder'
import buildSongData from '../utils/buildSongData'

const songErrorCleaningEvents = [UPDATE_CURRENT_SONG, UPDATE_SONGWRITER]

const songErrorCleanerMiddleware = ({ getState }) => (next) => (action) => {
  if (!songErrorCleaningEvents.includes(action.type)) {
    return next(action)
  }

  const state = getState()
  const {
    currentSong,
    currentCreatives,
    currentSongwriters,
    songwriterRoleId,
    currentSongStartTimes,
  } = state

  let newAttributes = action.newAttributes
  action.errors = songErrorBuilder(
    buildSongData(
      currentSong,
      currentCreatives,
      currentSongwriters,
      songwriterRoleId,
      currentSongStartTimes
    ),
    newAttributes,
    state
  )
  return next(action)
}

export default songErrorCleanerMiddleware
