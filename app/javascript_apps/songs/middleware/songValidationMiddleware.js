import { CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import { invalidSongAttributes } from '../actions/songActions'
import { invalidArtists } from '../actions/artistActions'
import addArtistErrors from '../utils/addArtistErrors'
import validSong from '../utils/validSong'
import filledOutArtists from '../utils/filledOutArtists'

const songValidationMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (
    !(
      action.payload &&
      [UPDATE_SONG, CREATE_SONG].includes(action.payload.success)
    )
  ) {
    return next(action)
  }

  const { song } = action
  const state = getState()
  const errors = validSong(song, state)

  const numberOfErrors = Object.values(errors).filter((value) => value).length

  if (numberOfErrors === 0) {
    return next(action)
  }

  if (errors.artists) {
    const creatives = song.data.artists
      .map((artistId) => state.currentCreatives[artistId])
      .filter((artist) => artist)
    dispatch(invalidArtists(addArtistErrors(filledOutArtists(creatives))))
  }

  dispatch(invalidSongAttributes(errors))
}

export default songValidationMiddleware
