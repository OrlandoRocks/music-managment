import {
  COPY_ARTISTS_FROM_PREVIOUS,
  COPY_SONGWRITERS_FROM_PREVIOUS,
} from '../actions/actionTypes'
import guid from '../../shared/guid'

const copyFromPreviousMiddleware = () => (next) => (action) => {
  if (action.type === COPY_ARTISTS_FROM_PREVIOUS) {
    const artists = action.previousSongArtists
      .map(mapArtists)
      .reduce(accumulateArtists, {})

    return next({ ...action, artists })
  }

  if (action.type === COPY_SONGWRITERS_FROM_PREVIOUS) {
    const songwriters = action.previousSongSongwriters
      .map(mapArtists)
      .reduce(accumulateArtists, {})

    return next({ ...action, songwriters })
  }

  return next(action)
}

function accumulateArtists(accumulator, artist) {
  return {
    ...accumulator,
    [artist.uuid]: artist,
  }
}

function mapArtists(artist) {
  return {
    ...artist,
    uuid: guid(),
  }
}

export default copyFromPreviousMiddleware
