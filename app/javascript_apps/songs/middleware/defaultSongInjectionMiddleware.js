import {
  ADD_SONG,
  CREATE_INITIAL_SONG,
  CREATE_SONG,
  UPDATE_SONG,
} from '../actions/actionTypes'
import { normalizeSongs } from '../store/schema'
import buildDefaultSongStartTime from '../utils/buildDefaultSongStartTime'
import guid from '../../shared/guid'
import { DEFAULT_COPYRIGHTS } from '../utils/defaultCopyrights'
import { DEFAULT_COVER_SONG_METADATA } from '../utils/defaultCoverSongMetadata.js'

const defaultSongInjectionActions = [
  CREATE_INITIAL_SONG,
  CREATE_SONG,
  UPDATE_SONG,
  ADD_SONG,
]

const defaultSongInjectionMiddleware = ({ getState }) => (next) => (action) => {
  if (!defaultSongInjectionActions.includes(action.type)) {
    return next(action)
  }

  let {
    defaultSong,
    songs: currentSongs,
    explicitRadioEnabled,
    explicitFieldsEnabled,
    cleanVersionRadioEnabled,
    songStartTimeStoreOptions,
    songStartTimesFormEnabled,
    coverSongMetadataEnabled,
  } = getState()
  const defaultSongwriter = {
    uuid: guid(),
    name: '',
    index: 0,
  }

  const defaultSongStartTimes =
    songStartTimesFormEnabled &&
    songStartTimeStoreOptions &&
    songStartTimeStoreOptions.reduce((obj, storeId) => {
      let defaultSongStartTime = buildDefaultSongStartTime(storeId)

      obj[defaultSongStartTime.uuid] = defaultSongStartTime

      return obj
    }, {})

  const defaultCreative = {
    artist_name: '',
    uuid: guid(),
    associated_to: 'Song',
    role_ids: [],
    credit: 'primary_artist',
  }

  const defaultArtists =
    defaultSong.data.artists.length === 0
      ? [defaultCreative]
      : defaultSong.data.artists.map((artist) => {
          return {
            ...artist,
            uuid: guid(),
          }
        })

  const newSong = {
    ...defaultSong,
    uuid: guid(),
    data: {
      ...defaultSong.data,
      explicit: explicitFieldsEnabled && explicitRadioEnabled ? null : false,
      clean_version: cleanVersionRadioEnabled ? null : false,
      artists: defaultArtists,
      copyrights: DEFAULT_COPYRIGHTS,
      coverSongMetadata: DEFAULT_COVER_SONG_METADATA,
    },
    songwriters: {
      [defaultSongwriter.uuid]: defaultSongwriter,
    },
    songStartTimes: defaultSongStartTimes,
  }

  const {
    entities: { creatives, songs, songwriters, songStartTimes },
  } = normalizeSongs([newSong])

  return next({
    ...action,
    defaultSong: Object.values(songs)[0],
    defaultCreatives: creatives,
    defaultSongwriters: songwriters,
    defaultSongStartTimes: songStartTimes,
    trackNum: Object.keys(currentSongs).length,
  })
}

export default defaultSongInjectionMiddleware
