import { REMOVE_SONG } from '../actions/actionTypes'
import { addSong, clearCurrentSong } from '../actions/songActions'

const removeSongSuccessMiddleware = ({ dispatch, getState }) => (next) => (
  action
) => {
  if (action.type === REMOVE_SONG) {
    dispatch(clearCurrentSong())

    const { songs } = getState()
    if (Object.keys(songs).length === 1) {
      dispatch(addSong())
    }
  }

  return next(action)
}

export default removeSongSuccessMiddleware
