import guid from '../../shared/guid'
import buildDefaultSongStartTime from '../utils/buildDefaultSongStartTime'

const songLookupMiddleware = ({ getState }) => (next) => (action) => {
  if (action.songUuid) {
    const state = getState()
    let {
      songs,
      creatives,
      songwriters,
      songStartTimes,
      songStartTimeStoreOptions,
    } = state
    let song = songs[action.songUuid]
    if (!song) {
      return next(action)
    }
    creatives = song.data.artists
      .map((artistId) => creatives[artistId])
      .reduce((accumulator, artist) => {
        return {
          ...accumulator,
          [artist.uuid]: artist,
        }
      }, {})

    if (song.songwriters.length === 0) {
      let defaultSongwriter = {
        uuid: guid(),
        name: '',
        index: 0,
      }
      songwriters = {
        [defaultSongwriter.uuid]: defaultSongwriter,
      }
      song = {
        ...song,
        songwriters: Object.keys(songwriters),
      }
    } else {
      songwriters = song.songwriters
        .map((songwriterId) => songwriters[songwriterId])
        .reduce((accumulator, songwriter) => {
          return {
            ...accumulator,
            [songwriter.uuid]: songwriter,
          }
        }, {})
    }

    if (song.songStartTimes.length === 0) {
      let defaultSongStartTimes =
        songStartTimeStoreOptions &&
        songStartTimeStoreOptions.reduce((obj, storeId) => {
          let defaultSongStartTime = buildDefaultSongStartTime(storeId)

          obj[defaultSongStartTime.uuid] = defaultSongStartTime

          return obj
        }, {})

      songStartTimes = defaultSongStartTimes

      song = {
        ...song,
        songStartTimes: Object.keys(songStartTimes),
      }
    } else {
      songStartTimes = song.songStartTimes
        .map((songStartTimeId) => songStartTimes[songStartTimeId])
        .reduce((accumulator, songStartTime) => {
          return {
            ...accumulator,
            [songStartTime.uuid]: songStartTime,
          }
        }, {})
    }

    return next({
      ...action,
      song,
      songwriters,
      creatives,
      songStartTimes,
    })
  }

  return next(action)
}

export default songLookupMiddleware
