import guid from '../../shared/guid'
import isClosable from '../utils/isClosable'
import getAssetFilename from '../utils/getAssetFilename'

import {
  ADD_ARTIST,
  ADD_SONG,
  CANCEL_SONG_UPLOAD,
  CHANGE_CURRENT_SONG,
  CLEAR_CURRENT_SONG,
  CREATE_SONG,
  INVALID_ARTISTS,
  INVALID_SONG_ATTRIBUTE,
  REGISTER_ASSET,
  REMOVE_ARTIST,
  SONG_LOADING,
  SONG_NOT_LOADING,
  SONG_NOT_UPLOADING,
  SONG_UPLOADING,
  UPDATE_CURRENT_SONG,
  UPDATE_SONG,
  UPDATE_SONGWRITER,
  BIGBOX_UPLOAD_SONG,
  S3_UPLOAD_SONG,
  S3_UPLOAD_SPATIAL_AUDIO_SONG,
  COPY_ARTISTS_FROM_PREVIOUS,
  STYLE_GUIDE_WARNING,
  CLEAR_STYLE_GUIDE_WARNING,
  CREATE_INITIAL_SONG,
  ADD_SONGWRITER,
  REMOVE_SONGWRITER,
  COPY_SONGWRITERS_FROM_PREVIOUS,
  UPDATE_SONG_START_TIME,
  UPDATE_COPYRIGHTS,
  UPDATE_COVER_SONG_METADATA,
} from '../actions/actionTypes'

function canTransitionToCurrentSongPresentStates(state, action) {
  return (
    state === null &&
    ![CHANGE_CURRENT_SONG, ADD_SONG, CREATE_INITIAL_SONG].includes(action.type)
  )
}

export default function currentSongReducer(state = null, action) {
  if (canTransitionToCurrentSongPresentStates(state, action)) {
    return state || null
  }

  let artists, song, defaultSong
  switch (action.type) {
    case ADD_SONG:
      song = {
        ...action.defaultSong,
        data: {
          ...action.defaultSong.data,
          track_number: action.trackNum,
        },
        uuid: guid(),
      }
      return song
    case CREATE_SONG:
      song = { ...action.song, isCurrentSong: true }
      defaultSong = { ...action.defaultSong, uuid: guid(), isCurrentSong: true }
      if (isClosable(song)) {
        if (action.albumType === 'Album') {
          return defaultSong
        } else {
          return null
        }
      } else {
        return song
      }
    case UPDATE_SONG:
      ;({ song } = action)
      if (isClosable(song)) {
        return null
      } else {
        return song
      }
    case UPDATE_CURRENT_SONG:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.newAttributes,
        },
        errors: { ...state.errors, ...action.errors },
      }
    case BIGBOX_UPLOAD_SONG:
    case S3_UPLOAD_SONG:
      return {
        ...state,
        asset_data: action.asset,
        data: {
          ...state.data,
          asset_url: action.asset.streaming_url,
        },
      }
    case S3_UPLOAD_SPATIAL_AUDIO_SONG:
      return {
        ...state,
        asset_data: action.asset,
        data: {
          ...state.data,
          asset_url: action.asset.streaming_url,
        },
      }
    case CREATE_INITIAL_SONG:
      return { ...action.defaultSong, isCurrentSong: true }
    case CLEAR_CURRENT_SONG:
      return null
    case CHANGE_CURRENT_SONG:
      return { ...action.song, isCurrentSong: true }
    case ADD_ARTIST:
      artists = [...state.data.artists, action.artist.uuid]
      return { ...state, data: { ...state.data, artists } }
    case ADD_SONGWRITER:
      return {
        ...state,
        songwriters: state.songwriters.concat(action.songwriter.uuid),
      }
    case REMOVE_SONGWRITER:
      return {
        ...state,
        songwriters: state.songwriters.filter(
          (songwriterId) => songwriterId !== action.songwriterUuid
        ),
      }
    case UPDATE_SONGWRITER:
      return {
        ...state,
        errors: {
          ...state.errors,
          ...action.errors,
        },
      }
    case UPDATE_SONG_START_TIME:
      return {
        ...state,
        errors: {
          ...state.errors,
          ...action.errors,
        },
      }
    case REMOVE_ARTIST:
      artists = state.data.artists.filter(
        (artistUuid) => artistUuid !== action.artistUuid
      )
      return { ...state, data: { ...state.data, artists } }
    case REGISTER_ASSET:
      if (state.uuid !== action.data.uuid) {
        return state
      }
      const {
        data: stateData,
        data: { asset_filename: stateAssetFilename },
      } = state

      const {
        data: { asset_url: actionAssetUrl, filename: actionFilename },
      } = action

      const { options } = action
      let closeForm
      if (options) {
        let { shouldCloseForm } = options
        closeForm = shouldCloseForm
      }

      const currentSongChangedToDefault = stateAssetFilename === null
      if (currentSongChangedToDefault) {
        return {
          ...state,
          data: {
            ...state.data,
            asset_url: actionAssetUrl,
            asset_filename: actionFilename,
          },
        }
      }

      song = {
        ...state,
        data: {
          ...stateData,
          asset_url: actionAssetUrl,
          asset_filename: getAssetFilename(actionFilename),
        },
      }
      return isClosable(song) && closeForm ? null : song
    case SONG_LOADING:
      if (state.uuid !== action.song.uuid) {
        return state
      }
      return state
        ? {
            ...state,
            isLoading: true,
            errors: { ...state.errors, ...{ asset_url: null } },
          }
        : state
    case SONG_NOT_LOADING:
      if (state.uuid !== action.song.uuid) {
        return state
      }
      return state
        ? {
            ...state,
            isLoading: false,
            errors: { ...state.errors, ...{ asset_url: null } },
          }
        : state
    case CANCEL_SONG_UPLOAD:
      if (state.uuid !== action.song.uuid) {
        return state
      }
      return {
        ...state,
        cancelUpload: true,
        errors: { ...state.errors, asset_url: null },
      }
    case SONG_UPLOADING:
      if (state.uuid !== action.song.uuid) {
        return state
      }
      return { ...action.song, isUploading: true }
    case SONG_NOT_UPLOADING:
      if (state.uuid !== action.song.uuid) {
        return state
      }
      return {
        ...action.song,
        errors: { ...state[action.song.uuid].errors },
        isUploading: false,
      }
    case COPY_ARTISTS_FROM_PREVIOUS:
      return {
        ...state,
        data: {
          ...state.data,
          artists: Object.keys(action.artists),
        },
      }
    case COPY_SONGWRITERS_FROM_PREVIOUS:
      return {
        ...state,
        songwriters: Object.keys(action.songwriters),
      }
    case STYLE_GUIDE_WARNING:
      song = {
        ...state,
        styleGuideWarnings: {
          ...state.styleGuideWarnings,
          [action.attribute]: action.warning,
        },
      }
      return song
    case CLEAR_STYLE_GUIDE_WARNING:
      song = {
        ...state,
        styleGuideWarnings: {
          ...state.styleGuideWarnings,
          [action.attribute]: null,
        },
      }
      return song
    case INVALID_ARTISTS:
      return {
        ...state,
        data: {
          ...state.data,
          artists: action.artists.map((artist) => artist.uuid),
        },
      }
    case UPDATE_COPYRIGHTS:
      return {
        ...state,
        data: {
          ...state.data,
          copyrights: {
            ...state.data.copyrights,
            [action.payload.type]: action.payload.value,
          },
        },
      }
    case UPDATE_COVER_SONG_METADATA:
      return {
        ...state,
        data: {
          ...state.data,
          cover_song_metadata: {
            ...state.data.cover_song_metadata,
            [action.payload.type]: action.payload.value,
          },
        },
        errors: {
          ...state.errors,
          coverSong: false,
        },
      }
    case INVALID_SONG_ATTRIBUTE:
      return {
        ...state,
        errors: {
          ...state.errors,
          ...action.errors,
        },
      }
    default:
      return state || null
  }
}
