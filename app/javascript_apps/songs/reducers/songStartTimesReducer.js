import { CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import removeNullsFromObject from '../utils/removeNullsFromObject'
import reduceSongStartTimes from '../utils/reduceSongStartTimes'

export default function songStartTimesReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SONG:
      return removeNullsFromObject(reduceSongStartTimes(state, action))
    case CREATE_SONG:
      return removeNullsFromObject(reduceSongStartTimes(state, action))
    default:
      return state
  }
}
