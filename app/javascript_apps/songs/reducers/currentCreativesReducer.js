import {
  ADD_ARTIST,
  ADD_SONG,
  CHANGE_CURRENT_SONG,
  CLEAR_CURRENT_SONG,
  COPY_ARTISTS_FROM_PREVIOUS,
  CREATE_INITIAL_SONG,
  CREATE_SONG,
  REMOVE_ARTIST,
  UPDATE_ARTIST,
  UPDATE_ARTIST_NAME,
  UPDATE_SONG,
  INVALID_ARTISTS,
} from '../actions/actionTypes'
import isClosable from '../utils/isClosable'
import removeNullsFromObject from '../utils/removeNullsFromObject'

export default function currentCreativesReducer(state = {}, action) {
  let defaultCreatives, song, creatives
  switch (action.type) {
    case ADD_SONG:
      return action.defaultCreatives
    case CREATE_INITIAL_SONG:
      return action.defaultCreatives
    case CHANGE_CURRENT_SONG:
      return action.creatives
    case CLEAR_CURRENT_SONG:
      return null
    case UPDATE_SONG:
      ;({ defaultCreatives, song } = action)
      if (isClosable(song)) {
        return null
      } else {
        return defaultCreatives
      }
    case CREATE_SONG:
      ;({ defaultCreatives, creatives, song } = action)
      if (isClosable(song)) {
        if (action.albumType === 'Album') {
          return defaultCreatives
        } else {
          return null
        }
      } else {
        return creatives
      }
    case UPDATE_ARTIST:
      return {
        ...state,
        [action.artistUuid]: action.artist,
      }
    case UPDATE_ARTIST_NAME:
      let newState = { ...state }
      for (const artistId in action.artists) {
        newState[artistId] = action.artists[artistId]
        if (artistId === action.artistUuid) {
          newState[artistId].artist_name = action.newName
        }
      }

      return newState
    case REMOVE_ARTIST:
      const { [action.artistUuid]: _omit, ...rest } = state
      return removeNullsFromObject(rest)
    case ADD_ARTIST:
      return {
        ...state,
        [action.artist.uuid]: action.artist,
      }
    case COPY_ARTISTS_FROM_PREVIOUS:
      return action.artists
    case INVALID_ARTISTS:
      return action.artists.reduce((accumulator, artist) => {
        return {
          ...accumulator,
          [artist.uuid]: artist,
        }
      }, {})
    default:
      return state
  }
}
