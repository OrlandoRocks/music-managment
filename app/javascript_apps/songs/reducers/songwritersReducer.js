import { CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import removeNullsFromObject from '../utils/removeNullsFromObject'
import reduceSongwriters from '../utils/reduceSongwriters'

export default function songwritersReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SONG:
      return removeNullsFromObject(reduceSongwriters(state, action))
    case CREATE_SONG:
      return removeNullsFromObject(reduceSongwriters(state, action))
    default:
      return state
  }
}
