import { CREATE_SONG, UPDATE_SONG } from '../actions/actionTypes'
import removeNullsFromObject from '../utils/removeNullsFromObject'
import reduceCreatives from '../utils/reduceCreatives'

export default function creativesReducer(state = {}, action) {
  switch (action.type) {
    case UPDATE_SONG:
      return removeNullsFromObject(reduceCreatives(state, action))
    case CREATE_SONG:
      return removeNullsFromObject(reduceCreatives(state, action))
    default:
      return state
  }
}
