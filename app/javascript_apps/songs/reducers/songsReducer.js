import {
  CREATE_SONG,
  UPDATE_SONG,
  REMOVE_SONG,
  REGISTER_ASSET,
  REGISTER_SPATIAL_AUDIO_ASSET,
  CLOSE_SPATIAL_AUDIO_COMPLETE_MODAL,
  CLOSE_SPATIAL_AUDIO_UPLOAD_ERROR_MODAL,
  SET_SPATIAL_UPLOAD_ACTIVE,
  SET_SPATIAL_UPLOAD_INACTIVE,
  REORDER_SONGS,
  SONG_LOADING,
  SONG_NOT_LOADING,
  SONG_IS_DELETING,
  SONG_UPLOADING,
  SONG_NOT_UPLOADING,
  INVALID_SONG_ATTRIBUTE,
  CANCEL_SONG_UPLOAD,
  SONG_FINISHED_UPLOADING,
  PLAY_SONG,
  PAUSE_SONG,
  RESET_SONG_ERRORS,
  REMOVE_SONG_ATMOS_ASSET,
} from '../actions/actionTypes'
import updateSongs from '../utils/updateSongs'
import removeNullsFromObject from '../utils/removeNullsFromObject'

export default function songsReducer(state = {}, action) {
  let song
  switch (action.type) {
    case CREATE_SONG:
      return updateSongs(action, state)
    case UPDATE_SONG:
      return updateSongs(action, state)
    case REGISTER_ASSET:
      song = state[action.data.uuid]

      if (!song) {
        return state
      }

      return updateSongs(action, state)
    case REGISTER_SPATIAL_AUDIO_ASSET:
      song = state[action.data.uuid]

      if (!song) {
        return state
      }

      return updateSongs(action, {
        ...state,
        [action.data.uuid]: {
          ...state[action.data.uuid],
          showSpatialAudioCompleteModal: true,
        },
      })
    case REORDER_SONGS:
      const songsById = Object.values(state).reduce((accumulator, song) => {
        return {
          ...accumulator,
          [song.data.id]: song,
        }
      }, {})
      return action.data.song_order.reduce((accumulator, songId, index) => {
        const song = songsById[songId]
        return {
          ...accumulator,
          [song.uuid]: {
            ...song,
            data: {
              ...song.data,
              track_number: index + 1,
            },
          },
        }
      }, {})
    case REMOVE_SONG:
      return Object.values(updateSongs(action, state))
        .sort(
          (song1, song2) => song1.data.track_number - song2.data.track_number
        )
        .reduce((accumulator, song, index) => {
          return {
            ...accumulator,
            [song.uuid]: {
              ...song,
              data: {
                ...song.data,
                track_number: index + 1,
              },
            },
          }
        }, {})
    case REMOVE_SONG_ATMOS_ASSET:
      return updateSongs(action, state)
    case SONG_LOADING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isLoading: true,
            }
          : null,
      })
    case SONG_UPLOADING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isUploading: true,
              isFinishedUploading: false,
            }
          : null,
      })
    case SONG_NOT_UPLOADING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              errors: { ...state[action.song.uuid].errors },
              isUploading: false,
              isFinishedUploading: true,
              showSpatialAudioUploadErrorModal:
                state[action.song.uuid]['spatialUploadActive'] &&
                action.song.upload_error
                  ? true
                  : false,
            }
          : null,
      })
    case CLOSE_SPATIAL_AUDIO_COMPLETE_MODAL:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              showSpatialAudioCompleteModal: false,
            }
          : null,
      })
    case CLOSE_SPATIAL_AUDIO_UPLOAD_ERROR_MODAL:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              showSpatialAudioUploadErrorModal: false,
            }
          : null,
      })
    case SET_SPATIAL_UPLOAD_ACTIVE:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              spatialUploadActive: true,
            }
          : null,
      })
    case SET_SPATIAL_UPLOAD_INACTIVE:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              spatialUploadActive: false,
            }
          : null,
      })
    case SONG_NOT_LOADING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isLoading: false,
            }
          : null,
      })
    case SONG_IS_DELETING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isDeleting: true,
            }
          : null,
      })
    case INVALID_SONG_ATTRIBUTE:
      if (action.song) {
        return {
          ...state,
          [action.song.uuid]: {
            ...state[action.song.uuid],
            errors: { ...state[action.song.uuid].errors, ...action.errors },
            showSpatialAudioUploadErrorModal:
              state[action.song.uuid]['spatialUploadActive'] &&
              action.song.upload_error
                ? true
                : false,
          },
        }
      } else {
        return state
      }
    case RESET_SONG_ERRORS:
      if (action.song) {
        return {
          ...state,
          [action.song.uuid]: {
            ...state[action.song.uuid],
            errors: {},
          },
        }
      } else {
        return state
      }
    case SONG_FINISHED_UPLOADING:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isFinishedUploading: true,
            }
          : null,
      })
    case CANCEL_SONG_UPLOAD:
      return removeNullsFromObject({
        ...state,
        [action.song.uuid]: state[action.song.uuid]
          ? {
              ...state[action.song.uuid],
              isLoading: false,
              isUploading: false,
              errors: {
                ...action.song.errors,
                asset_url: null,
              },
            }
          : null,
      })
    case PLAY_SONG:
      return Object.values(state).reduce((accumulator, song) => {
        return {
          ...accumulator,
          [song.uuid]: {
            ...state[song.uuid],
            isPlaying: action.song.uuid === song.uuid,
          },
        }
      }, {})
    case PAUSE_SONG:
      return {
        ...state,
        [action.song.uuid]: {
          ...state[action.song.uuid],
          isPlaying: false,
        },
      }
    default:
      return state
  }
}
