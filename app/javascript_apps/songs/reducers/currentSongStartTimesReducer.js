import {
  ADD_SONG,
  CHANGE_CURRENT_SONG,
  CREATE_INITIAL_SONG,
  CREATE_SONG,
  UPDATE_SONG,
  UPDATE_SONG_START_TIME,
  INVALID_SONG_START_TIMES,
} from '../actions/actionTypes'
import isClosable from '../utils/isClosable'

export default function currentSongStartTimes(state = {}, action) {
  switch (action.type) {
    case CREATE_SONG:
      if (isClosable(action.song)) {
        if (action.albumType === 'Album') {
          return action.defaultSongStartTimes || null
        } else {
          return null
        }
      } else {
        return state
      }
    case UPDATE_SONG:
      if (isClosable(action.song)) {
        return null
      } else {
        return state
      }
    case CHANGE_CURRENT_SONG:
      return action.songStartTimes
    case CREATE_INITIAL_SONG:
      return action.defaultSongStartTimes || null
    case ADD_SONG:
      return action.defaultSongStartTimes || null
    case UPDATE_SONG_START_TIME:
      return {
        ...state,
        [action.songStartTime.uuid]: action.songStartTime,
      }
    case INVALID_SONG_START_TIMES:
      return action.songStartTimes.reduce((accumulator, songStartTime) => {
        return {
          ...accumulator,
          [songStartTime.uuid]: songStartTime,
        }
      }, {})
    default:
      return state
  }
}
