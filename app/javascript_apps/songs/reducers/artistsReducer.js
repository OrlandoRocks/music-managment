function combineArtists(state, action) {
  return [
    ...state,
    ...action.song.data.artists
      .map((artistId) => action.creatives[artistId])
      .map((artist) => artist.artist_name),
  ].filter((value, index, self) => {
    return self.indexOf(value) === index
  })
}

export default function artistsReducer(state = [], action) {
  switch (action.type) {
    case 'CREATE_SONG':
      return combineArtists(state, action)
    case 'UPDATE_SONG':
      return combineArtists(state, action)
    default:
      return state
  }
}
