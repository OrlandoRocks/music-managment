import {
  ALBUM_RELOAD,
  ALBUM_LOADING,
  ALBUM_NOT_LOADING,
  REMOVE_SONG,
  SONG_IS_DELETING,
  SONG_LOADING,
  SONG_NOT_LOADING,
} from '../actions/actionTypes'

export default function albumReducer(state = {}, action) {
  switch (action.type) {
    case ALBUM_LOADING:
      return { ...state, isLoading: true }
    case ALBUM_NOT_LOADING:
      return { ...state, isLoading: false }
    case SONG_IS_DELETING:
      return { ...state, songIsDeleting: true }
    case SONG_LOADING:
      return { ...state, songIsLoading: true }
    case SONG_NOT_LOADING:
      return { ...state, songIsLoading: false }
    case REMOVE_SONG:
      return { ...state, songIsDeleting: false }
    case ALBUM_RELOAD:
      return { ...state, ...action.data.album }
    default:
      return state
  }
}
