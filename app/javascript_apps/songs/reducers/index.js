import { combineReducers } from 'redux'
import songs from './songsReducer'
import creatives from './creativesReducer'
import languages from './languagesReducer'
import songRoles from './songRolesReducer'
import artists from './artistsReducer'
import songwriterNames from './songwriterNamesReducer'
import currentSong from './currentSongReducer'
import defaultSong from './defaultSongReducer'
import tcPid from './tcPidReducer'
import bigBoxUrl from './bigBoxUrlReducer'
import album from './albumReducer'
import currentCreatives from './currentCreativesReducer'
import songwriterRoleId from './songwriterRoleIdReducer'
import currentSongwriters from './currentSongwritersReducer'
import songwriters from './songwritersReducer'
import copyrightsFormEnabled from './copyrightsFormEnabledReducer'
import trackLevelLanguageEnabled from './trackLevelLanguageEnabledReducer'
import explicitRadioEnabled from './explicitRadioEnabledReducer'
import explicitFieldsEnabled from './explicitFieldsEnabledReducer'
import cleanVersionRadioEnabled from './cleanVersionRadioEnabledReducer'
import songStartTimes from './songStartTimesReducer'
import currentSongStartTimes from './currentSongStartTimesReducer'
import songStartTimeStoreOptions from './songStartTimeStoreOptionsReducer'
import songStartTimesFormEnabled from './songStartTimesFormEnabledReducer'
import spatialAudioEnabled from './spatialAudioEnabledReducer'
import dolbyAtmosArticleLink from './dolbyAtmosArticleLinkReducer'
import coverSongMetadataEnabled from './coverSongMetadataEnabledReducer'
import coverSongArticleLink from './coverSongArticleLinkReducer'
import planUpgradeLink from './planUpgradeLinkReducer'

const rootReducer = combineReducers({
  songs,
  creatives,
  songwriters,
  songStartTimes,
  artists,
  songwriterNames,
  album,
  currentSong,
  currentCreatives,
  currentSongwriters,
  currentSongStartTimes,
  songwriterRoleId,
  languages,
  songRoles,
  defaultSong,
  tcPid,
  bigBoxUrl,
  copyrightsFormEnabled,
  trackLevelLanguageEnabled,
  explicitRadioEnabled,
  explicitFieldsEnabled,
  cleanVersionRadioEnabled,
  songStartTimeStoreOptions,
  songStartTimesFormEnabled,
  spatialAudioEnabled,
  dolbyAtmosArticleLink,
  coverSongMetadataEnabled,
  coverSongArticleLink,
  planUpgradeLink,
})

export default rootReducer
