function combineSongwriterNames(state, action) {
  return [
    ...state,
    ...action.song.songwriters.map((songwriter) => songwriter.name),
  ].filter((value, index, self) => {
    return self.indexOf(value) === index
  })
}

export default function songwriterNamesReducer(state = [], action) {
  switch (action.type) {
    case 'CREATE_SONG':
      return combineSongwriterNames(state, action)
    case 'UPDATE_SONG':
      return combineSongwriterNames(state, action)
    default:
      return state
  }
}
