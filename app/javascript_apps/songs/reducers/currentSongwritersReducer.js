import {
  ADD_SONG,
  ADD_SONGWRITER,
  CHANGE_CURRENT_SONG,
  COPY_SONGWRITERS_FROM_PREVIOUS,
  CREATE_INITIAL_SONG,
  CREATE_SONG,
  REMOVE_SONGWRITER,
  UPDATE_SONG,
  UPDATE_SONGWRITER,
} from '../actions/actionTypes'
import isClosable from '../utils/isClosable'

export default function currentSongwriters(state = {}, action) {
  switch (action.type) {
    case CREATE_SONG:
      if (isClosable(action.song)) {
        if (action.albumType === 'Album') {
          return action.defaultSongwriters
        } else {
          return null
        }
      } else {
        return state
      }
    case UPDATE_SONG:
      if (isClosable(action.song)) {
        return null
      } else {
        return state
      }
    case CHANGE_CURRENT_SONG:
      return action.songwriters
    case CREATE_INITIAL_SONG:
      return action.defaultSongwriters
    case ADD_SONG:
      return action.defaultSongwriters
    case ADD_SONGWRITER:
      return {
        ...state,
        [action.songwriter.uuid]: action.songwriter,
      }
    case UPDATE_SONGWRITER:
      return {
        ...state,
        [action.songwriter.uuid]: action.songwriter,
      }
    case REMOVE_SONGWRITER:
      const { [action.songwriterUuid]: _omit, ...rest } = state
      return rest
    case COPY_SONGWRITERS_FROM_PREVIOUS:
      return action.songwriters
    default:
      return state
  }
}
