import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import initialState from './initialState'
import apiMiddleware from '../middleware/apiMiddleware'
import uploadSongMiddleware from '../middleware/uploadSongMiddleware'
import bigboxUploadSongMiddleware from '../middleware/bigboxUploadSongMiddleware'
import albumChecklistDispatcherMiddleware from '../middleware/albumChecklistDispatcherMiddleware'
import preorderCalendarDispatcherMiddleware from '../middleware/preorderCalendarDispatcherMiddleware'
import songValidationMiddleware from '../middleware/songValidationMiddleware'
import songErrorCleanerMiddleware from '../middleware/songErrorCleanerMiddleware'
import singleDetailsDispatcherMiddleware from '../middleware/singleDetailsDispatcherMiddleware'
import artistAttributeValidationMiddleware from '../middleware/artistAttributeValidationMiddleware'
import albumCheckoutButtonDispatcherMiddleware from '../middleware/albumCheckoutButtonDispatcherMiddleware'
import songStyleGuideMiddleware from '../middleware/songStyleGuideMiddleware'
import artistStyleGuideMiddleware from '../middleware/artistStyleGuideMiddleware'
import defaultSongInjectionMiddleware from '../middleware/defaultSongInjectionMiddleware'
import songLookupMiddleware from '../middleware/songLookupMiddleware'
import copyFromPreviousMiddleware from '../middleware/copyFromPreviousMiddleware'
import cancelUploadMiddleware from '../middleware/cancelUploadMiddleware'
import removeSongSuccessMiddleware from '../middleware/removeSongSuccessMiddleware'
import saveSongSuccessMiddleware from '../middleware/saveSongSuccessMiddleware'
import songSaveParameterBuilderMiddleware from '../middleware/songSaveParameterBuilderMiddleware'
import trackNumberInjectionMiddleware from '../middleware/trackNumberInjectionMiddleware'

let composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function configureStore() {
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(
        songSaveParameterBuilderMiddleware,
        songLookupMiddleware,
        defaultSongInjectionMiddleware,
        cancelUploadMiddleware,
        trackNumberInjectionMiddleware,
        copyFromPreviousMiddleware,
        songStyleGuideMiddleware,
        songErrorCleanerMiddleware,
        artistAttributeValidationMiddleware,
        artistStyleGuideMiddleware,
        songValidationMiddleware,
        uploadSongMiddleware,
        bigboxUploadSongMiddleware,
        apiMiddleware,
        saveSongSuccessMiddleware,
        removeSongSuccessMiddleware,
        albumChecklistDispatcherMiddleware,
        preorderCalendarDispatcherMiddleware,
        singleDetailsDispatcherMiddleware,
        albumCheckoutButtonDispatcherMiddleware
      )
    )
  )
}
