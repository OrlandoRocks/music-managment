import songsData from '../songsData'

let album = {
  ...songsData.album,
  isLoading: false,
}

export default album
