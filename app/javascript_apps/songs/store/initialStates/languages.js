import songsData from '../songsData'
import { normalizeLanguages } from '../schema'

export default normalizeLanguages(songsData.languages).entities.languages
