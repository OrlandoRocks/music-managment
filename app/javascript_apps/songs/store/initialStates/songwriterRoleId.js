import songRoles from './songRoles'

export default Object.values(songRoles)
  .find((role) => role.role_type_raw === 'songwriter')
  .id.toString()
