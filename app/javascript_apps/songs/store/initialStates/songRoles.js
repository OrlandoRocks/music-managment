import songsData from '../songsData'
import { normalizeSongRoles } from '../schema'

export default normalizeSongRoles(songsData.songRoles).entities.song_roles
