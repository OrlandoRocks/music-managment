import songsData from '../songsData'
import { normalizeSongs } from '../schema'
import songwriterRoleId from './songwriterRoleId'
import buildSongFromApi from '../../utils/buildSongDataFromApi'

let {
  entities: { creatives, songs, songwriters, songStartTimes },
} = normalizeSongs(
  songsData.songs.map((song) => buildSongFromApi(song, songwriterRoleId))
)

export { songs, songwriters, creatives, songStartTimes }
