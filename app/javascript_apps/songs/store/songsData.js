let songsData = {}
const songsElement = document.getElementById('songs_app')

if (songsElement) {
  let songsDataset = songsElement.dataset
  for (let key in songsDataset) {
    if (songsDataset[key].match(/{/) || songsDataset[key].match(/\[/)) {
      songsData[key] = JSON.parse(songsDataset[key])
    } else {
      songsData[key] = songsDataset[key]
    }
  }
} else {
  songsData = {}
}

export default songsData
