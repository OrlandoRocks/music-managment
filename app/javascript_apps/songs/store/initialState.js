import defaultSong from './initialStates/defaultSong'
import {
  songs,
  creatives,
  songwriters,
  songStartTimes,
} from './initialStates/songs'
import artists from './initialStates/artists'
import songwriterNames from './initialStates/songwriterNames'
import languages from './initialStates/languages'
import songRoles from './initialStates/songRoles'
import tcPid from './initialStates/tcPid'
import bigBoxUrl from './initialStates/bigBoxUrl'
import currentSong from './initialStates/currentSong'
import currentCreatives from './initialStates/currentCreatives'
import album from './initialStates/album'
import songwriterRoleId from './initialStates/songwriterRoleId'
import currentSongwriters from './initialStates/currentSongwriters'
import copyrightsFormEnabled from './initialStates/copyrightsFormEnabled'
import trackLevelLanguageEnabled from './initialStates/trackLevelLanguageEnabled'
import explicitRadioEnabled from './initialStates/explicitRadioEnabled'
import explicitFieldsEnabled from './initialStates/explicitFieldsEnabled'
import cleanVersionRadioEnabled from './initialStates/cleanVersionRadioEnabled'
import currentSongStartTimes from './initialStates/currentSongStartTimes'
import songStartTimeStoreOptions from './initialStates/songStartTimeStoreOptions'
import songStartTimesFormEnabled from './initialStates/songStartTimesFormEnabled'
import spatialAudioEnabled from './initialStates/spatialAudioEnabled'
import dolbyAtmosArticleLink from './initialStates/dolbyAtmosArticleLink'
import coverSongMetadataEnabled from './initialStates/coverSongMetadataEnabled'
import coverSongArticleLink from './initialStates/coverSongArticleLink'
import planUpgradeLink from './initialStates/planUpgradeLink'

const initialState = {
  songs,
  creatives,
  songwriters,
  songStartTimes,
  artists,
  songwriterNames,
  album,
  languages,
  currentSong,
  currentCreatives,
  currentSongwriters,
  currentSongStartTimes,
  songRoles,
  defaultSong,
  tcPid,
  songwriterRoleId,
  bigBoxUrl,
  copyrightsFormEnabled,
  trackLevelLanguageEnabled,
  explicitRadioEnabled,
  explicitFieldsEnabled,
  cleanVersionRadioEnabled,
  songStartTimeStoreOptions,
  songStartTimesFormEnabled,
  spatialAudioEnabled,
  dolbyAtmosArticleLink,
  coverSongMetadataEnabled,
  coverSongArticleLink,
  planUpgradeLink,
}

export default initialState
