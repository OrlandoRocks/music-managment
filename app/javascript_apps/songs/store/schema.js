import { normalize, schema } from 'normalizr'

const songRole = new schema.Entity('song_roles')
const language = new schema.Entity('languages')
const creative = new schema.Entity(
  'creatives',
  { song_roles: [songRole] },
  { idAttribute: 'uuid' }
)
const songwriter = new schema.Entity('songwriters', {}, { idAttribute: 'uuid' })
const songStartTime = new schema.Entity(
  'songStartTimes',
  {},
  { idAttribute: 'uuid' }
)
const song = new schema.Entity(
  'songs',
  {
    data: { artists: [creative], language },
    songwriters: [songwriter],
    songStartTimes: [songStartTime],
  },
  { idAttribute: 'uuid' }
)

export const normalizeSongs = (originalData) => normalize(originalData, [song])
export const normalizeSongRoles = (originalData) =>
  normalize(originalData, [songRole])
export const normalizeCreatives = (originalData) =>
  normalize(originalData, [creative])
export const normalizeSongStartTimes = (originalData) =>
  normalize(originalData, [songStartTime])
export const normalizeLanguages = (originalData) =>
  normalize(originalData, [language])
