import { API_BASE } from '../api_config.js'
import {
  CREATE_SONG,
  UPDATE_SONG,
  ADD_SONG,
  UPDATE_CURRENT_SONG,
  CHANGE_CURRENT_SONG,
  CLEAR_CURRENT_SONG,
  API,
  BIGBOX_UPLOAD_SONG,
  S3_UPLOAD_SONG,
  S3_UPLOAD_SPATIAL_AUDIO_SONG,
  REGISTER_ASSET,
  REGISTER_SPATIAL_AUDIO_ASSET,
  CLOSE_SPATIAL_AUDIO_COMPLETE_MODAL,
  CLOSE_SPATIAL_AUDIO_UPLOAD_ERROR_MODAL,
  SET_SPATIAL_UPLOAD_ACTIVE,
  SET_SPATIAL_UPLOAD_INACTIVE,
  SONG_LOADING,
  SONG_NOT_LOADING,
  REMOVE_SONG,
  INVALID_SONG_ATTRIBUTE,
  CANCEL_SONG_UPLOAD,
  UPLOAD_FAILURE,
  SONG_UPLOADING,
  SONG_NOT_UPLOADING,
  SONG_IS_DELETING,
  COPY_ARTISTS_FROM_PREVIOUS,
  CREATE_INITIAL_SONG,
  COPY_SONGWRITERS_FROM_PREVIOUS,
  SONG_FINISHED_UPLOADING,
  PLAY_SONG,
  PAUSE_SONG,
  RESET_SONG_ERRORS,
  NO_ACTION,
  REMOVE_SONG_ATMOS_ASSET,
} from './actionTypes'

export function createSong(song) {
  return {
    type: API,
    reloadAlbum: true,
    songShowsLoadingIndicator: true,
    payload: {
      url: `${API_BASE}/song_data`,
      method: 'POST',
      actionParams: song,
      success: CREATE_SONG,
    },
    song,
  }
}

export function songIsLoading(song) {
  return {
    type: SONG_LOADING,
    song,
  }
}

export function songIsNotLoading(song) {
  return {
    type: SONG_NOT_LOADING,
    song,
  }
}

export function songIsUploading(song) {
  return {
    type: SONG_UPLOADING,
    song,
  }
}

export function songIsNotUploading(song) {
  return {
    type: SONG_NOT_UPLOADING,
    song,
  }
}

export function closeSpatialAudioCompleteModal(song) {
  return {
    type: CLOSE_SPATIAL_AUDIO_COMPLETE_MODAL,
    song,
  }
}

export function closeSpatialAudioUploadErrorModal(song) {
  return {
    type: CLOSE_SPATIAL_AUDIO_UPLOAD_ERROR_MODAL,
    song,
  }
}

export function updateSong(song) {
  return {
    type: API,
    reloadAlbum: true,
    songShowsLoadingIndicator: true,
    payload: {
      url: `${API_BASE}/song_data/${song.data.id}`,
      method: 'PUT',
      actionParams: { uuid: song.uuid },
      success: UPDATE_SONG,
    },
    song,
  }
}

export function uploadSong(song, file) {
  if (song.data.bigbox_disabled) {
    return {
      type: API,
      payload: {
        url: `/uploads/get_put_presigned_url`,
        method: 'POST',
        params: JSON.stringify({
          filename: file.name,
        }),
        actionParams: { song: song, file: file },
        success: S3_UPLOAD_SONG,
      },
      song,
    }
  } else {
    return {
      type: BIGBOX_UPLOAD_SONG,
      songShowsUploadingIndicator: true,
      file,
      song,
    }
  }
}

export function uploadSpatialAudioSong(song, file) {
  return {
    type: API,
    payload: {
      url: `/spatial_audio_uploads/get_put_presigned_url`,
      method: 'POST',
      params: JSON.stringify({
        filename: file.name,
      }),
      actionParams: { song: song, uuid: song.uuid, file: file },
      success: S3_UPLOAD_SPATIAL_AUDIO_SONG,
    },
    song,
  }
}

export function saveStreamingAsset(song, key_name, bucket_name) {
  return {
    type: API,
    payload: {
      url: `/uploads/save_streaming_asset`,
      method: 'POST',
      params: JSON.stringify({
        song_id: song.data.id,
        key_name: key_name,
        bucket_name: bucket_name,
      }),
      success: NO_ACTION,
    },
    song,
  }
}

export function validateAsset(song, key_name, bucket_name) {
  return {
    type: API,
    initiateRegisterAsset: true,
    payload: {
      url: `/uploads/validate_asset`,
      method: 'POST',
      params: JSON.stringify({
        song_id: song.data.id,
        key_name: key_name,
        bucket_name: bucket_name,
      }),
      actionParams: {
        song: song,
        key_name: key_name,
        bucket_name: bucket_name,
      },
      success: NO_ACTION,
    },
    song,
  }
}

export function validateSpatialAudioAsset(song, key_name, bucket_name) {
  return {
    type: API,
    initiateRegisterSpatialAudioAsset: true,
    payload: {
      url: `/spatial_audio_uploads/validate_asset`,
      method: 'POST',
      params: JSON.stringify({
        song_id: song.data.id,
        key_name: key_name,
        bucket_name: bucket_name,
      }),
      actionParams: {
        song: song,
        key_name: key_name,
        bucket_name: bucket_name,
      },
      success: NO_ACTION,
    },
    song,
  }
}

export function registerAsset(asset, song, options = {}) {
  let data = {
    song_id: song.data.id,
    s3_asset: {
      key: asset.key,
      uuid: asset.uuid,
      bucket: asset.bucket,
    },
    upload: {
      song_id: song.data.id,
      orig_filename: asset.orig_filename,
      bit_rate: asset.bit_rate,
      duration: asset.duration,
    },
  }

  return {
    type: API,
    songShowsUploadingIndicator: !options.shouldNotShowProgressBar,
    payload: {
      url: '/uploads/register',
      method: 'POST',
      params: JSON.stringify(data),
      actionParams: {
        uuid: song.uuid,
        asset_url: asset.streaming_url,
        filename: asset.orig_filename,
        isLoading: false,
        options,
      },
      success: REGISTER_ASSET,
    },
    song,
  }
}

export function registerSpatialAudioAsset(asset, song, options = {}) {
  let data = {
    song_id: song.data.id,
    s3_asset: {
      key: asset.key,
      uuid: song.uuid,
      bucket: asset.bucket,
    },
    upload: {
      song_id: song.data.id,
      orig_filename: asset.orig_filename,
      bit_rate: asset.bit_rate,
      duration: asset.duration,
    },
  }

  return {
    type: API,
    songShowsUploadingIndicator: !options.shouldNotShowProgressBar,
    payload: {
      url: '/spatial_audio_uploads/register',
      method: 'POST',
      params: JSON.stringify(data),
      actionParams: {
        uuid: song.uuid,
        asset_url: asset.streaming_url,
        filename: asset.orig_filename,
        isLoading: false,
        options,
      },
      success: REGISTER_SPATIAL_AUDIO_ASSET,
    },
    song,
  }
}

export function addSong() {
  return {
    type: ADD_SONG,
  }
}

export function updateCurrentSong(newAttributes) {
  return {
    type: UPDATE_CURRENT_SONG,
    newAttributes,
  }
}

export function changeCurrentSong(songUuid) {
  return {
    type: CHANGE_CURRENT_SONG,
    songUuid,
  }
}

export function uploadFinished(songUuid) {
  return {
    type: SONG_FINISHED_UPLOADING,
    songUuid,
  }
}

export function createInitialSong() {
  return {
    type: CREATE_INITIAL_SONG,
  }
}

export function clearCurrentSong() {
  return {
    type: CLEAR_CURRENT_SONG,
  }
}

export function removeSong(song) {
  return {
    type: API,
    reloadAlbum: true,
    albumShowsLoadingIndicator: true,
    songShowsLoadingIndicator: true,
    payload: {
      url: `${API_BASE}/song_data/${song.data.id}`,
      method: 'DELETE',
      actionParams: { songUuid: song.uuid },
      success: REMOVE_SONG,
    },
    song,
  }
}

export function removeSongAtmosAsset(song) {
  return {
    type: API,
    reloadAlbum: true,
    songShowsLoadingIndicator: true,
    payload: {
      url: `${API_BASE}/song_data/immersive_audio/${song.data.id}`,
      method: 'DELETE',
      actionParams: { songUuid: song.uuid },
      success: REMOVE_SONG_ATMOS_ASSET,
    },
    song,
  }
}

export function invalidSongAttributes(errors, song) {
  return {
    type: INVALID_SONG_ATTRIBUTE,
    errors,
    song,
  }
}

export function cancelUpload(song) {
  return {
    type: CANCEL_SONG_UPLOAD,
    song,
  }
}

export function uploadFailure(errors) {
  return {
    type: UPLOAD_FAILURE,
    errors,
  }
}

export function songIsDeleting(song) {
  return {
    type: SONG_IS_DELETING,
    song,
  }
}

export function removeSongFromStore(song) {
  return {
    type: REMOVE_SONG,
    song,
  }
}

export function copyArtistsFromPrevious(previousSongArtists) {
  return {
    type: COPY_ARTISTS_FROM_PREVIOUS,
    previousSongArtists,
  }
}

export function copySongwritersFromPrevious(previousSongSongwriters) {
  return {
    type: COPY_SONGWRITERS_FROM_PREVIOUS,
    previousSongSongwriters,
  }
}

export function playSong(song) {
  return {
    type: PLAY_SONG,
    song: song,
  }
}

export function pauseSong(song) {
  return {
    type: PAUSE_SONG,
    song,
  }
}

export function resetSongErrors(song) {
  return {
    type: RESET_SONG_ERRORS,
    song,
  }
}
export function setSpatialUploadActive(song) {
  return {
    type: SET_SPATIAL_UPLOAD_ACTIVE,
    song,
  }
}

export function setSpatialUploadInactive(song) {
  return {
    type: SET_SPATIAL_UPLOAD_INACTIVE,
    song,
  }
}
