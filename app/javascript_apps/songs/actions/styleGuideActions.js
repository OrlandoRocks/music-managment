import { STYLE_GUIDE_WARNING, CLEAR_STYLE_GUIDE_WARNING } from './actionTypes'

export function styleGuideWarning(attribute, warning) {
  return {
    type: STYLE_GUIDE_WARNING,
    attribute,
    warning,
  }
}

export function clearStyleGuideWarning(attribute) {
  return {
    type: CLEAR_STYLE_GUIDE_WARNING,
    attribute,
  }
}
