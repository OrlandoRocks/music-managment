import { UPDATE_SONG_START_TIME, INVALID_SONG_START_TIMES } from './actionTypes'

export function updateSongStartTime(songStartTime) {
  return {
    type: UPDATE_SONG_START_TIME,
    songStartTime,
  }
}

export function invalidSongStartTimes(songStartTimes) {
  return {
    type: INVALID_SONG_START_TIMES,
    songStartTimes,
  }
}
