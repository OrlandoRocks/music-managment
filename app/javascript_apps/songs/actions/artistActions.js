import {
  ADD_ARTIST,
  REMOVE_ARTIST,
  UPDATE_ARTIST,
  UPDATE_ARTIST_NAME,
  INVALID_ARTISTS,
  UPDATE_SONGWRITER,
  ADD_SONGWRITER,
  REMOVE_SONGWRITER,
} from './actionTypes'
import guid from '../../shared/guid'

export function addArtist() {
  return {
    type: ADD_ARTIST,
    artist: {
      uuid: guid(),
      artist_name: '',
      credit: '',
      role_ids: [],
      associated_to: 'Song',
    },
  }
}

export function removeArtist(artistUuid) {
  return {
    type: REMOVE_ARTIST,
    artistUuid,
  }
}

export function updateArtist(attribute, value, artistUuid) {
  return {
    type: UPDATE_ARTIST,
    attribute,
    value,
    artistUuid,
  }
}

export function invalidArtists(artists) {
  return {
    type: INVALID_ARTISTS,
    artists,
  }
}

export function updateSongwriter(songwriter) {
  return {
    type: UPDATE_SONGWRITER,
    songwriter,
    newAttributes: {
      hasSongwriter: songwriter.name.length > 0,
    },
  }
}

export function addSongwriter(index) {
  return {
    type: ADD_SONGWRITER,
    songwriter: {
      uuid: guid(),
      name: '',
      index,
    },
  }
}

export function updateArtistName(artistUuid, newName, artists) {
  return {
    type: UPDATE_ARTIST_NAME,
    artistUuid,
    newName,
    artists,
  }
}

export function removeSongwriter(songwriterUuid) {
  return {
    type: REMOVE_SONGWRITER,
    songwriterUuid,
  }
}
