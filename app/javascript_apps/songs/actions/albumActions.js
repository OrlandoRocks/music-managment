import {
  API,
  REORDER_SONGS,
  ALBUM_LOADING,
  ALBUM_NOT_LOADING,
  ALBUM_RELOAD,
} from './actionTypes'
import { API_BASE } from '../api_config'

export function albumIsLoading(song) {
  return {
    type: ALBUM_LOADING,
    song,
  }
}

export function albumIsNotLoading(song) {
  return {
    type: ALBUM_NOT_LOADING,
    song,
  }
}

export function reorderSongs(songs) {
  return {
    type: API,
    albumShowsLoadingIndicator: true,
    payload: {
      url: `${API_BASE}${location.pathname}`,
      method: 'PUT',
      params: JSON.stringify({
        album: { song_order: songs.map((song) => song.data.id) },
      }),
      success: REORDER_SONGS,
    },
  }
}

export function reloadAlbum() {
  return {
    type: API,
    payload: {
      url: `${API_BASE}${location.pathname}`,
      method: 'GET',
      success: ALBUM_RELOAD,
    },
  }
}
