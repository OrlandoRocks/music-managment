import { UPDATE_COVER_SONG_METADATA } from './actionTypes'

export function updateCoverSongMetadata(args = {}) {
  const { cover_song, licensed, will_get_license } = args

  return {
    type: UPDATE_COVER_SONG_METADATA,
    cover_song,
    licensed,
    will_get_license,
  }
}
