class ArtistUrlsPresenter
  def self.build_artist_urls(album)
    new(album).build_artist_urls
  end

  attr_reader :album

  def initialize(album)
    @album = album
  end

  def build_artist_urls
    external_service_ids.map do |esi|
      {}.tap do |hash|
        hash[:artist_name]     = esi.linkable.name
        hash[esi.service_name] = esi.send("#{esi.service_name}_url")
        hash[:is_new_artist]   = esi.new_artist_id?
        hash[:freeze_new_artist_editing] = esi.freeze_new_artist_editing?
      end
    end
  end

  private

  def external_service_ids
    ExternalServiceId.by_creative_ids(album.creatives.pluck(:id))
                     .by_service([ExternalServiceId::SPOTIFY_SERVICE, ExternalServiceId::APPLE_SERVICE])
  end
end
