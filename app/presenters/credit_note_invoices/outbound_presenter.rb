# frozen_string_literal: true

module CreditNoteInvoices
  class OutboundPresenter < BasePresenter
    def initialize(outbound_refund, preview = false)
      super(outbound_refund.refund)
      @outbound_refund = outbound_refund
      @preview = preview
    end

    def refund_details
      {
        credit_note_invoice_number: outbound_refund.credit_note_invoice_number,
        refund_date: formatted_date(outbound_refund.created_at),
        currency: invoice_currency,
        refund_reason: refund.reason,
        admin_note: refund.admin_note,
        base_amount: formatted_invoice_amount(outbound_refund.base_amount),
        tax_amount: formatted_invoice_amount(outbound_refund.tax_amount),
        total_amount: formatted_invoice_amount(outbound_refund.total_amount),
        vat_amount_in_euro: formatted_invoice_amount(outbound_refund.vat_amount_in_euro),
        tax_type: outbound_refund.tax_type,
        tax_rate: format("%.2f%%", outbound_refund.tax_rate),
      }.merge(refunded_invoice_details)
    end

    def show_applied_tax_info?
      outbound_refund.tax_type.present?
    end

    def show_supplier_name?
      show_applied_tax_info? &&
        refunded_outbound_invoice.invoice_static_customer_address.eu_country?
    end

    def inbound?
      false
    end

    def luxembourg_customer?
      refunded_outbound_invoice.invoice_static_customer_address.luxembourg_customer?
    end

    def customer_info
      refunded_outbound_invoice.invoice_static_customer_info
                               .slice(*InvoiceStaticCustomerInfo::FIELDS)
    end

    def preview?
      @preview
    end

    def show_vat_not_charged?
      !vat_registered? && lux_customer?
    end

    def show_vat_converted_to_euro?
      refunded_person_business_user? && luxembourg_customer? && refund.currency != CurrencyCodeType::EUR
    end

    def show_eu_directive?
      !country.eu? && refunded_person_business_user?
    end

    private

    attr_reader :outbound_refund

    def vat_registered?
      refunded_invoice.vat_tax_adjustment&.vat_registered?
    end

    def lux_customer?
      refunded_invoice.invoice_static_customer_address.luxembourg_customer?
    end

    def refunded_outbound_invoice
      @refunded_outbound_invoice ||= outbound_refund.outbound_invoice
    end

    def refunded_invoice_details
      {
        refunded_invoice_number: refunded_outbound_invoice.prefixed_invoice_number,
        refunded_invoice_date: formatted_date(refunded_outbound_invoice.created_at),
        refunded_invoice_country: invoice_country
      }
    end

    def invoice_country
      refunded_invoice.invoice_static_customer_address.country
    end
  end
end
