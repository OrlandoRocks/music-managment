module CreditNoteInvoices
  class InboundPresenter < BasePresenter
    def refund_details
      {
        credit_note_invoice_number: refund.credit_note_invoice_number,
        refund_date: formatted_refund_date,
        currency: invoice_currency,
        refund_reason: refund.reason,
        admin_note: refund.admin_note,
        base_amount: formatted_invoice_amount(refund.base_amount),
        tax_amount: formatted_invoice_amount(refund.tax_amount),
        total_amount: formatted_total_amount,
        vat_amount_in_euro: formatted_invoice_amount(refund.vat_amount_in_euro),
        tax_type: refund.tax_type,
        tax_rate: format("%.2f%%", (refund.tax_rate || 0)),
      }.merge(refunded_invoice_details)
    end

    def refund_summery
      {
        id: refund.id,
        currency: invoice_currency,
        total_amount: formatted_total_amount,
        refund_date: formatted_refund_date,
        credit_note_invoice_number: refund.credit_note_invoice_number
      }
    end

    def settlements
      Utils::SettlementFormatter
        .new(refund.refund_settlements, refunded_person.country_domain, inbound?, pegged_rate)
        .formatted_settlements
    end

    def show_auto_liquidation_message?
      country.eu? &&
        country.iso_code != Country::LUXEMBOURG_ISO &&
        refunded_person_business_user?
    end

    private

    def refunded_invoice_details
      {
        refunded_invoice_number: refunded_invoice.invoice_number,
        refunded_invoice_date: formatted_date(refunded_invoice.settled_at),
      }
    end

    def formatted_total_amount
      formatted_invoice_amount(refund.total_amount)
    end

    def formatted_refund_date
      formatted_date(refund.created_at)
    end
  end
end
