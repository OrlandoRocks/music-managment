module CreditNoteInvoices
  class BasePresenter
    def initialize(refund)
      @refund = refund
    end

    def tunecore_invoice?
      corporate_entity.tunecore_us?
    end

    def inbound?
      refund.is_a? Refund
    end

    def corporate_entity_address
      refunded_invoice
        .invoice_static_corporate_entity
        .slice(*InvoiceStaticCorporateEntity::FIELDS)
    end

    def customer_info
      refunded_invoice.invoice_static_customer_info
                      .slice(*InvoiceStaticCustomerInfo::FIELDS)
    end

    def customer_address
      refunded_invoice.invoice_static_customer_address
                      .slice(*InvoiceStaticCustomerAddress::FIELDS)
                      .reject { |_, v| v.blank? }
    end

    def show_customer_vat_registration_number?
      customer_info[:vat_registration_number].present?
    end

    def show_applied_tax_info?
      refund.tax_type.present?
    end

    def show_admin_note?
      refund.admin_note.present?
    end

    def show_vat_converted_to_euro?
      refund.vat_cents_in_euro.nonzero? && refund.currency != CurrencyCodeType::EUR
    end

    def euro_exchange_rate
      return unless show_vat_converted_to_euro?
      return inr_to_euro_exchange_rate if refunded_person.india_user?

      refund.euro_exchange_rate
    end

    def show_eu_directive?
      !country.eu?
    end

    private

    attr_reader :refund

    def refunded_invoice
      @refunded_invoice ||= refund.invoice
    end

    def refunded_person
      @refunded_person ||= refund.person
    end

    def invoice_currency
      @invoice_currency ||=
        if refunded_person.india_user?
          CurrencyCodeType::INR
        else
          refund.currency
        end
    end

    def refunded_person_business_user?
      customer_info[:customer_type].casecmp? TcVat::Base::BUSINESS
    end

    def country
      Country
        .find_by(name: refunded_invoice.invoice_static_customer_address.country)
    end

    def corporate_entity
      @corporate_entity ||= refunded_invoice.invoice_static_corporate_entity
    end

    def formatted_invoice_amount(money)
      Utils::MoneyFormatter
        .new(money, refunded_person.country_domain, inbound?, pegged_rate)
        .format_money
    end

    def formatted_date(time)
      time.strftime("%m/%d/%Y")
    end

    def inr_to_euro_exchange_rate
      ForeignExchangeRate.latest_by_currency(
        source: CurrencyCodeType::INR,
        target: CurrencyCodeType::EUR
      ).exchange_rate
    end

    def pegged_rate
      refunded_invoice.pegged_rate
    end
  end
end
