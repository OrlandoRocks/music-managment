module CreditNoteInvoices
  module Utils
    class MoneyFormatter
      include CurrencyHelper

      def initialize(money, domain, inbound, pegged_rate = nil)
        @money = money
        @domain = domain
        @inbound = inbound
        @pegged_rate = pegged_rate
      end

      def format_money
        inbound? ? inbound_formatted_money : outbound_formatted_money
      end

      private

      attr_reader :money, :domain, :pegged_rate

      def inbound?
        @inbound
      end

      def inbound_formatted_money
        money_to_currency(
          money,
          iso_code: domain,
          currency: currency,
          unit: "",
          strip_insignificant_zeros: false,
          round_value: false,
          skip_inr_symbol: true,
          pegged_rate: pegged_rate,
          suppress_iso: true
        )
      end

      def outbound_formatted_money
        balance_to_currency(
          money,
          iso_code: domain,
          unit: "",
          strip_insignificant_zeros: false,
          round_value: false,
          skip_inr_symbol: true
        )
      end

      def currency
        domain == Country::INDIA_ISO ? CurrencyCodeType::INR : money.currency
      end
    end
  end
end
