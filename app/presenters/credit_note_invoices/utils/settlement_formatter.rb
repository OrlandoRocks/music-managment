# frozen_string_literal: true

module CreditNoteInvoices
  module Utils
    class SettlementFormatter
      include ActionView::Helpers::TranslationHelper
      include CustomTranslationHelper

      def initialize(settlements, domain, inbound, pegged_rate = nil)
        @settlements = settlements
        @domain = domain
        @inbound = inbound
        @pegged_rate = pegged_rate
      end

      def formatted_settlements
        settlements.each_with_object([]) do |settlement, arr|
          source = dispute_adjusted_source(settlement.source)
          arr << {
            desc: InvoiceSettlementFormatter::Factory.for(source).credit_note_invoice_description,
            amount: formatted_settlement_amount(settlement.settlement_amount)
          }
        end
      end

      private

      attr_reader :settlements, :domain, :pegged_rate

      def inbound?
        @inbound
      end

      def dispute_adjusted_source(source)
        return source unless source.is_a? Dispute

        source.source
      end

      def formatted_settlement_amount(settlement_amount)
        Utils::MoneyFormatter
          .new(settlement_amount, domain, inbound?, pegged_rate)
          .format_money
      end
    end
  end
end
