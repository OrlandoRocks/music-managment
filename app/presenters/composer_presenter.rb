class ComposerPresenter
  include Rails.application.routes.url_helpers

  attr_reader :composer, :composer_status

  delegate :pub_admin_product_in_cart?,
           :needs_pro?,
           :needs_cae_number?,
           :has_cae_number?,
           :cae_number,
           :state,
           :active?,
           :paid_for?,
           :terminated?,
           :pub_admin_product_in_cart?,
           to: :composer_status

  delegate :valid_letter_of_direction?, :sync_opted_in?, :sync_opted_updated_at, to: :composer

  alias_method :status, :state

  def initialize(composer)
    @composer         = composer
    @composer_status  = PublishingAdministration::ComposerStatusService.new(composer)
  end

  delegate :id, to: :composer

  delegate :account_id, to: :composer

  delegate :full_name_affixed, to: :composer

  delegate :full_name, to: :composer

  def dob
    composer.dob.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def display_lod_link?
    composer.publisher.present? && legal_document.present? && legal_document.signed_at
  end

  def display_lod_not_yet_signed?
    composer.publisher.present? && legal_document.present? && legal_document.signed_at.nil?
  end

  def display_composer_lod_cta?
    composer.publisher.present? && composer.letter_of_direction && composer.letter_of_direction.signed_at.blank?
  end

  def legal_document
    if composer.is_a?(PublishingComposer)
      composer.legal_documents.where(subject_type: "PublishingComposer", provider: "Docusign").last
    else
      composer.legal_documents.where(subject_type: "Composer", provider: "Docusign").last
    end
  end

  def link_to_cart
    if pub_admin_product_in_cart?
      cart_path
    else
      products_add_composer_registration_to_cart_path(composer_id: composer.id)
    end
  end

  def composer_compositions
    if composer.is_a?(PublishingComposer)
      @composer_compositions ||= PublishingAdministration::PublishingCompositionsBuilder.build({ publishing_composer_id: composer.id })
    else
      @composer_compositions ||= PublishingAdministration::CompositionsBuilder.build({ composer_id: composer.id })
    end
  end

  def number_of_compositions
    composer_compositions.to_a.size
  end

  def number_of_shares
    number_of_shares_present
  end

  def number_of_shares_missing
    @number_of_shares_missing ||= number_of_compositions - number_of_shares
  end

  def number_of_shares_present
    if composer.is_a?(PublishingComposer)
      @number_of_shares_present ||= PublishingCompositionSplit
                                    .where(publishing_composition_id: composer_compositions.pluck(:id))
                                    .group(:publishing_composition_id)
                                    .having("sum(percent) > 0")
                                    .length
    else
      @number_of_shares_present ||= PublishingSplit
                                    .where(composition_id: composer_compositions.pluck(:id))
                                    .group(:composition_id)
                                    .having("sum(percent) > 0")
                                    .length
    end
  end

  def on_behalf_of_self?
    composer.publishing_role.on_behalf_of_self?
  end

  def pro_affiliation
    composer.pro.try(:name)
  end

  def fully_terminated?
    composer.has_fully_terminated_composers?
  end

  def registration_date
    composer.agreed_to_terms_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def manage_compositions_link
    I18n.t("composers.index.manage_compositions")
  end

  def sync_opted_updated_at_formated
    sync_opted_updated_at&.strftime("%m/%d/%Y")
  end
end
