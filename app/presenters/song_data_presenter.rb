class SongDataPresenter
  attr_reader :person_id, :tc_pid, :bigbox_url

  RESTRICTED_LANGUAGE_CODES = [
    "cmn-Hans",
    "cmn-Hant",
    "gu",
    "he",
    "ko",
    "mi",
    "mr",
    "th",
    "ar",
    "ru",
    "uk",
    "bg"
  ].freeze

  def self.permitted_language_codes
    LanguageCode.where(
      LanguageCode.arel_table[:code].not_in(RESTRICTED_LANGUAGE_CODES)
    )
  end

  def initialize(album, person_id, tc_pid = nil)
    @person_id  = person_id
    @album      = album
    @tc_pid     = tc_pid
    @bigbox_url = BIGBOX_ELB_NAME
  end

  def songs
    ActiveModel::ArraySerializer.new(
      song_data_form_collection,
      each_serializer: song_data_form_serializer
    ).to_json
  end

  def language_codes
    self.class.permitted_language_codes.to_json(except: :code, root: false)
  end

  def default_song
    song_data_form_serializer.new(new_song_data_form, root: false).to_json
  end

  def song_roles
    SongRole.all.map do |song_role|
      {
        id: song_role.id,
        role_type_raw: song_role.role_type_raw,
        role_type: song_role.role_type_safely_translated
      }
    end.to_json(root: false)
  end

  def artists
    MyArtist.all_artists_for(person_id).pluck("artists.name")
  end

  # Songwriters differ from artist names in that they are generally legal names.
  # Therefore we don't add artist names to the list of available songwriter names,
  # however, free-input is allowed.
  def songwriter_names
    Songwriter::NameQueryBuilder.build(person_id).pluck(:name)
  end

  def album
    album_serializer.new(@album, root: false).to_json
  end

  private

  def song_data_form_serializer
    Api::Backstage::SongDataFormSerializer
  end

  def album_serializer
    Api::Backstage::AlbumSerializer
  end

  def song_data_form_collection
    SongDataFormBuilder.song_data_forms_for(@album.id, person_id)
  end

  def new_song_data_form
    SongDataFormBuilder.new_song_form_for(@album.id, person_id)
  end
end
