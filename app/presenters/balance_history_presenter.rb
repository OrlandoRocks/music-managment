class BalanceHistoryPresenter
  attr_reader :person, :page, :transactions

  def initialize(params)
    @person       = params[:person]
    @page         = params[:page]
    @transactions = BalanceHistory::TransactionsQueryBuilder.build(params)
  end

  def transaction_history
    @transaction_history ||= transactions.paginate_for_balance_history(page)
  end

  def pending_transactions
    @pending_transactions ||= transactions.pending_payout_transactions
  end

  def person_balance
    person.person_balance.balance
  end

  def pending_txn_balance
    BigDecimal(pending_transactions.sum("credit - debit").values.sum).abs
  end

  def on_last_page?
    current_cursor = page.to_i * PersonTransaction::PAGINATION_LIMIT
    current_cursor >= transactions.pluck(:id).size
  end
end
