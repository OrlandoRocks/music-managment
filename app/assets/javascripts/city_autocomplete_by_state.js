$(document).ready(function () {
  initStateCityAutocomplete();
});

function initStateCityAutocomplete() {
  var stateSelector = $('.country_state_selector');
  var cityInput = $('#state_city_autocomplete');

  if (stateSelector.is(':visible')) {
    stateSelector.attr('required', true);
  }
  stateSelector.on('change', function (e) {
    setCityInputState();
    cityInput.val("");
  });

  // Init the city input state
  setCityInputState();

  cityInput.autocomplete({
    source: function (request, response) {
      var stateSelected = stateSelector.val();
      $.ajax({
        url: "/stored_credit_cards/country_cities",
        dataType: "json",
        data: {
          query: request.term,
          state_id: stateSelected
        },
        success: function (data) {
          response(data.city_names);
        }
      });
    },
    search: function (event, ui) {
      $('.state-city-spinner').show();
    },
    open: function (event, ui) {
      $('.state-city-spinner').hide();
    },
    autoFocus: true,
    minLength: 1,
    change: function (event, ui) {
      if (!ui.item) {
        cityInput.val("");
      }
    },
    select: function (event, ui) {
      if (!ui.item) {
        event.preventDefault();
        cityInput.val("");
      } else if (ui.item.value === "No cities found, please enter valid city") {
        event.preventDefault();
        cityInput.val("");
      }
    }
  });
  cityInput.attr('autocomplete', 'no');
  addressInfoValidations();
}

function setCityInputState() {
  var stateSelector = $('.country_state_selector');
  var cityInput = $('#state_city_autocomplete');
  var noStateSelected = stateSelector.val() === "";
  cityInput.attr('disabled', noStateSelected);
  cityInput.attr('autocomplete', 'no');
}
