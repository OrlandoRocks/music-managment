jQuery(document).ready(function($) {
  if ($(".publishing_administration-composers__index").length !== 1) return;

  $(".publishing-dashboard-card-tax-info-link").click(function() {
    var composerId   = this.dataset.id;
    var taxModalBody = $(".publishing-tax-info-modal-content").html();
    $(".publishing-tax-info-modal-body").html(taxModalBody);

    $(".publishing-dashboard-card-tax-info-link").magnificPopup({
      src:      ".publishing-tax-info-modal",
      midClick: true
    }).magnificPopup("open");
  });

  $(".publishing_administration-composers__index").on("click", ".publishing-edit-info-modal-tax-info-prompt__link", function() {
    var taxInfoBody = $(".publishing-tax-info-modal-content").html();
    $(".publishing-edit-info-modal-body").html(taxInfoBody);
  });

  $(".publishing_administration-composers__index").on("click", ".publishing-edit-info-modal-tax-info-prompt__ok-btn-wrapper-btn", function() {
    $.magnificPopup.close();
  });
});
