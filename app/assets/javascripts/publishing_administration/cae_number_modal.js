jQuery(document).ready(function($) {
  var $page = $('.publishing_administration-composers__index');
  if ($page.length !== 1) return;

  $(".publishing-cae-number-modal-link").click(function() {
    var composerId       = this.dataset.composerId;
    var caeNumberContent = $("#publishing-cae-number-container-" + composerId)
                             .find(".publishing-cae-number-wrapper").html();
    $(".publishing-cae-number-modal-body").html(caeNumberContent);

    $(".publishing-cae-number-modal-link").magnificPopup({
      src:      "#publishing-cae-number-modal",
      midClick: true
    }).magnificPopup("open");
    caeFormValidate();
  });

  $page.on("keyup", "#publishing_administration_enrollment_form_composer_cae", caeFormValidate);
  $page.on("click", ".publishing-cae-number-modal-btn-wrapper__cancel-btn", function() {
    $.magnificPopup.close();
  });

  function caeFormValidate() {
    var cae = $('#publishing_administration_enrollment_form_composer_cae').val();
    var isValidLength = cae.length === 9 || cae.length === 10;

    if (isValidLength) {
      $('#cae-modal-save-btn').prop("disabled", false)
        .removeClass('publishing-cae-number-modal-btn-wrapper__save-btn-disabled')
        .addClass('publishing-cae-number-modal-btn-wrapper__save-btn');
    } else {
      $('#cae-modal-save-btn').prop("disabled", true)
        .removeClass('publishing-cae-number-modal-btn-wrapper__save-btn')
        .addClass('publishing-cae-number-modal-btn-wrapper__save-btn-disabled');
    }
  }
});
