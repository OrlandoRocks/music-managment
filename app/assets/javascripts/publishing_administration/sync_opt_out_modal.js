jQuery(document).ready(function($) {
  if ($(".publishing_administration-composers__index").length !== 1) return;
  var $page = $('.publishing_administration-composers__index')

  $(".publishing-dashboard-card-sync-opt-out-link").click(function() {
    var composerId = this.dataset.id;
    var modalBody = $(`.publishing-dashboard-card-sync-opt-out-modal-wrapper-${composerId}`).html();
    $(".publishing-sync-opt-out-modal-body").html(modalBody);

    $(".publishing-dashboard-card-sync-opt-out-link").magnificPopup({
      src: ".publishing-sync-opt-out-modal",
      midClick: true,
    }).magnificPopup("open");
  });

  $page.on("click", ".sync-opt-out-cancel", function(e) {
    e.preventDefault()
    $.magnificPopup.close();
  });
});
