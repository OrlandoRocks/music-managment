jQuery(document).ready(function($) {
  if ($(".publishing_administration-composers__index").length !== 1) return;

  checkInputDisplays();

  $(".publishing-dashboard-card-edit-info-link").click(function() {
    $('.publishing-dashboard-card-wrapper').hide();
    $('.publishing-dashboard-card-edit-info-card-wrapper').show();
    enrollmentFormValidate();
    checkInputDisplays();
  });

  $(document).ajaxSuccess(function(event, xhr, settings){
    if (/^\/publishing_administration\/composers\/\d+$/.test(settings.url)) {
      checkInputDisplays();
      enrollmentFormValidate()
    }
  });

  $(".publishing_administration-composers__index").on("click", ".publishing-edit-info-card-body-btn-wrapper__cancel-btn", function(e) {
    e.preventDefault();
    $('.publishing-dashboard-card-wrapper').show();
    $('.publishing-dashboard-card-edit-info-card-wrapper').hide();
    checkInputDisplays();
  });

  $(".publishing_administration-composers__index").on("click", ".publishing-edit-info-card-body-company-btn-wrapper__cancel-btn", function(e) {
    e.preventDefault();
    $('.publishing-dashboard-card-wrapper').show();
    $('.publishing-dashboard-card-edit-info-card-wrapper').hide();
    checkInputDisplays();
  });

  $(document).on('click', '.publishing-edit-info-card-body-company-section .form-link', function() {
    $(this).toggle();

    $('.publishing-edit-info-card-body-company-btn-wrapper').show();
    $('.publishing-edit-info-card-body-btn-wrapper').hide();
    $('.publishing-edit-info-card-body-company-section .grid-x').toggle();
  });

  $(document).on('change', '.publishing_administration_enrollment_form_composer_pro_id select', function() {
      checkPRODisplay();
  });

  $(document).on('change', '.publishing_administration_publishing_enrollment_form_composer_pro_id select', function() {
      checkPRODisplay();
  });
});

function updateComposerCard(composerId, fullName, dob, proName, cae) {
  const $composerCard = $("div[data-composer-id=" + composerId + "]");

  const nameField = $composerCard.find("div.publishing-dashboard-card-name b");
  nameField.text(fullName);

  const dobField = $composerCard.find("span.publishing-dashboard-card-dob");
  const dobValue = (dob != "")? dob.replace(/(\d{4})\-(\d{2})\-(\d{2})/, '$2/$3/$1'): "-/-/-";
  dobField.text(dobValue);

  const proField = $composerCard.find("span.publishing-dashboard-card-pro-name");
  proField.text(proName);

  const caeField  = $composerCard.find("span.publishing-dashboard-card-cae");
  const updateCae = cae !== "" || (caeField.text() !== "" && cae === "");

  if (updateCae) {
    caeField.text(cae);
  }
}

function enrollmentFormValidate() {
  const dobRequired = $('#publishing-edit-info-card').attr('data-dob-required-feature-enabled')

  const pro = (
    $('#publishing_administration_enrollment_form_composer_pro_id').val() ||
      $('#publishing_administration_publishing_enrollment_form_composer_pro_id').val() ||
      ""
  ).trim();

  const cae = (
    $('#publishing_administration_enrollment_form_composer_cae').val() ||
      $('#publishing_administration_publishing_enrollment_form_composer_cae').val() ||
      ""
  ).trim();

  const month = (
    $('#publishing_administration_enrollment_form_dob_2i').val() ||
      $('#publishing_administration_publishing_enrollment_form_dob_2i').val() ||
      ""
  )

  const day = (
    $('#publishing_administration_enrollment_form_dob_3i').val() ||
      $('#publishing_administration_publishing_enrollment_form_dob_3i').val() ||
      ""
  )

  const year = (
    $('#publishing_administration_enrollment_form_dob_1i').val() ||
      $('#publishing_administration_publishing_enrollment_form_dob_1i').val() ||
      ""
  )

  const proAndCaeMissing = pro === '' && cae === '';
  var caeStartWith55 = cae.startsWith("55") ;
  const proAndCaePresent = pro !== '' && 9 <= cae.length && cae.length <= 11;
  const dobPresent = (month !== '' && day !== '' && year !== '') || dobRequired === 'false';

  if((proAndCaeMissing && !dobPresent) || (proAndCaePresent && dobPresent && !caeStartWith55)) {
    $('#save-btn').prop("disabled",false)
      .removeClass('publishing-edit-info-card-body-btn-wrapper__save-btn-disabled')
      .addClass('publishing-edit-info-card-body-btn-wrapper__save-btn');
    $('#company-save-btn').prop("disabled",false)
      .removeClass('publishing-edit-info-card-body-company-btn-wrapper__save-btn-disabled')
      .addClass('publishing-edit-info-card-body-company-btn-wrapper__save-btn');
  } else {
    $('#save-btn').prop("disabled",true)
      .removeClass('publishing-edit-info-card-body-btn-wrapper__save-btn')
      .addClass('publishing-edit-info-card-body-btn-wrapper__save-btn-disabled');
    $('#company-save-btn').prop("disabled",true)
      .removeClass('publishing-edit-info-card-body-company-btn-wrapper__save-btn')
      .addClass('publishing-edit-info-card-body-company-btn-wrapper__save-btn-disabled');
  }
}

function checkInputDisplays(){
  checkPRODisplay();
  checkExistingPublisherDisplay();
}

function checkPRODisplay() {
    const selectedPro = $('.publishing_administration_enrollment_form_composer_pro_id select option:selected').html() ||
      $('.publishing_administration_publishing_enrollment_form_composer_pro_id select option:selected').html();
    const PROS = ['BMI', 'ASCAP', 'SESAC'];

    if ($.inArray(selectedPro, PROS) !== -1) {
        $('.publishing-edit-info-card-body-company-section').show();
        $('.publishing-edit-info-card-body-company-section .form-link').show();
    } else {
        $('.publishing-edit-info-card-body-company-section').hide();
        $('.publishing-edit-info-card-body-company-section .form-link').hide();
        $('.publishing-edit-info-card-body-company-section .grid-x').hide();
        $('.publishing-edit-info-card-body-btn-wrapper').show();
    }
}

function checkExistingPublisherDisplay() {
  const hasPublisherValues = ($("#publishing_administration_enrollment_form_publisher_name").val() ||
    $("#publishing_administration_enrollment_form_publisher_cae").val() ||
    $("#publishing_administration_publishing_enrollment_form_publisher_name").val() ||
    $("#publishing_administration_publishing_enrollment_form_publisher_cae").val()  ||
    ""
  ).length > 0;

  if (hasPublisherValues) {
    $('.publishing-edit-info-card-body-company-section .form-link').hide();
    $('.publishing-edit-info-card-body-company-section .grid-x').show();
    $('.publishing-edit-info-card-body-btn-wrapper').hide();
    $('.publishing-edit-info-card-body-company-btn-wrapper').show();
  } else {
    $('.publishing-edit-info-card-body-company-section .form-link').show();
    $('.publishing-edit-info-card-body-company-section .grid-x').hide();
    $('.publishing-edit-info-card-body-btn-wrapper').show();
    $('.publishing-edit-info-card-body-company-btn-wrapper').hide();
  }
}
