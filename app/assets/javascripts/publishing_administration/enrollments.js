(function() {
  var $              = jQuery;
  var eligibleProIds = gon.eligble_publishing_pro_ids;

  if ($('.publishing_administration-enrollments__show').length === 1 && history.pushState) {
    redirectToEditAction(window.location.pathname);
  }

  var enrollmentContainer = $(".publishing-enrollment");

  enrollmentContainer.on("click", ".agree-tac input", function() {
    var selected = $(this).prop("checked");
    $(".publishing-enrollment .tac-submit").attr("disabled", !selected);
  })
})();
