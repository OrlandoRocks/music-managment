jQuery(document).ready(function($) {
  if ($(".publishing_administration-composers__index").length !== 1) return;
  var $page = $('.publishing_administration-composers__index')

  $(".publishing-dashboard-card-sync-opt-in-link").click(function() {
    var composerId = this.dataset.id;
    var modalBody = $(`.publishing-dashboard-card-sync-opt-in-modal-wrapper-${composerId}`).html();
    $(".publishing-sync-opt-in-modal-body").html(modalBody);

    $(".publishing-dashboard-card-sync-opt-in-link").magnificPopup({
      src: ".publishing-sync-opt-in-modal",
      midClick: true,
    }).magnificPopup("open");

    $(".sync-opt-in-submit").attr("disabled", true)
  });

  $page.on("click", ".sync-opt-in-tac", function() {
    var selected = $(this).prop("checked");
    $(".sync-opt-in-submit").attr("disabled", !selected);
  })

  $page.on("click", ".sync-opt-in-cancel", function(e) {
    e.preventDefault()
    $.magnificPopup.close();
  });
});
