(function() {

  var $ = jQuery;

  $('.change-payout-info-trigger').on('click', function () {
    $.magnificPopup.open({
      items: {src: '.change-payout-info', type: 'inline'}
    });
  });
  $('.change-payout-pref-info-trigger').on('click', function () {
    $.magnificPopup.open({
      items: {src: '.change-payout-pref-info', type: 'inline'}
    });
  });
})();
