(function() {

  var $ = jQuery;
  var usaSelectCurrencyBtn = $('.choose-currency-action');
  var currencySelectionModal = $('.payoneer-currency-selection-content');
  var payoutMethod;
  var programId;

  $(".payout-method-data").on("click", function() {
    var button = $(this);
    payoutMethod = button.data('payoutMethod');
  });

  $("button[name='program_id']").on("click", function() {
    var button = $(this);
    programId = button.val();
  });

  if (usaSelectCurrencyBtn.length !== 0) {
    usaSelectCurrencyBtn.click(function() {
      currencySelectionModal.css('display', 'block');
      $.magnificPopup.open({
        items: {src: '.payoneer-currency-selection-content', type: 'inline'}
      });
    });
  }
  
  $("#currency_selector_popup_form").on("submit", function(e) {
    e.preventDefault();

    $.ajax({
      url: "/payout_provider_registrations",
      type: "POST",
      data: {
        payout_methods_list: payoutMethod,
        program_id: programId,
        js_redirect: true,
      },
      success: function(data){
        if (data.location) {
          window.location.href = data.location;
        }
      }
    });
  });
})();
