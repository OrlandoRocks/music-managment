(function() {
  // Soft stop payoneer popup
  var $ = jQuery;
  if ($(".payoneer-soft-stop").length !== 0) {
    $.magnificPopup.open({
      items: {src: '.payoneer-soft-stop', type: 'inline'}
    });
    $("#sign-up").on('click', function (e) {
      $.magnificPopup.close();
      handlePayoneerSignUp(e);
    });
    $('#popup-no-thanks').on('click', function () {
      $.magnificPopup.close();
    });
  }

// Hard stop payoneer page
  $("#sign-up").on('click', handlePayoneerSignUp);


  function handlePayoneerSignUp(e) {
    e.preventDefault();
    var form = $('#sign-up').parents("form:first");
    redirectToPayoneer(form);
  }
})();

