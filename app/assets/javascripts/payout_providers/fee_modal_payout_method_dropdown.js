(function() {
  var $ = jQuery;

  var selectPayoutDropdownOption = function (payoutMethods, selectedCountry) {
    var allPayoutCountries = ["US"];
    var noPaypalCountries = ["CA", "AU"];
    var noCardCountries = ["IN"];
    var payouts = {
      "PAYPAL": true,
      "BANK": true,
      "PAPER_CHECK": true,
      "PREPAID_CARD": true
    };

    var setPayouts = function(selectedCountry) {
      // this isn't the most readable, but it seemed the most efficient to get what we want. As it stands at this
      // writing: US gets all methods, CA and AU don't have paypal, IN only has bank and paypal. Every other country
      // has bank, paypal, and card. There may be a simpler way to do this, especially if we end up with more
      // restrictinos, but for now this accomplishes what we need pretty efficiently, if not readably.
      if (noPaypalCountries.includes(selectedCountry)) {
        payouts["PAYPAL"] = false;
      } else if (!allPayoutCountries.includes(selectedCountry)) {
        payouts["PAPER_CHECK"] = false;
      }
      if(noCardCountries.includes(selectedCountry)) {
        payouts["PREPAID_CARD"] = false;
      }
    };
    setPayouts(selectedCountry);

    var filterPayouts = function() {
      var payoutMethod = this.value;
      this.hidden = !payouts[payoutMethod];
    };
    payoutMethods.each(filterPayouts);
  };
  var handleFeeModalClick = function() {
    var payoutMethods = $("#fee-modal-payout-method-dropdown option");
    var selectedCountry = $("#fee-modal-country-dropdown option")[0].value;
    selectPayoutDropdownOption(payoutMethods, selectedCountry);
  };
  var handleFeeModalDropdownChange = function() {
    var payoutMethods = $("#fee-modal-payout-method-dropdown option");
    var selectedCountry = this.options[this.selectedIndex].value;
    selectPayoutDropdownOption(payoutMethods, selectedCountry);
  };

  $(".payoneer-fee-modal-trigger").on('click', handleFeeModalClick);
  $("#fee-modal-country-dropdown").on('change', handleFeeModalDropdownChange);
})();
