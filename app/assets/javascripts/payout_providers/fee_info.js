(function() {
  var $ = jQuery;

  $(".fee-info-trigger").on('click', function () {
    $.magnificPopup.open({
      items: {src: '.payoneer-fee-info', type: 'inline'}
    });
  });

  $(".payoneer-fee-modal-trigger").on('click', function (e) {
    e.stopPropagation();
    $.magnificPopup.open({
      items: {src: '.payoneer-fee-modal-container', type: 'inline'}
    });
  });

  $(".payoneer-details-modal-trigger").on('click', function() {
    $.magnificPopup.open({
      items: {src: '.payoneer-details-modal-container', type: 'inline'}
    });
  });

})();
