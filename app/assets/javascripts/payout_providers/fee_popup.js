(function() {

  var $ = jQuery;

  $(".payoneer-fee-modal-container .preview-fees").on('click', function () {
    var container = $('.payoneer-fee-modal-container');
    var selectedPayoutMethod = container.find('.payout-method').find(":selected").val();
    var selectedCountry = container.find('.country').find(":selected").val();
    var optionToShow = ".fee-row." + selectedPayoutMethod;

    if (selectedPayoutMethod === 'BANK') {
      optionToShow += "." + selectedCountry;
    } else if (selectedPayoutMethod === 'PAYPAL') {
      optionToShow += "." + selectedCountry;
    }
    container.find('.no-records').hide();
    container.find('.fee-row').hide();
    container.find(optionToShow).show();
    container.find('.results table').show();
  });

})();
