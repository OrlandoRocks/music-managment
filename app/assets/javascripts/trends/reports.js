jQuery(document).ready(function($) {

  $('.trend_reports__index').on('click', '.btn-download-report',  function() {
    gtmLogEvent(
      'click-download-report',
      'click-download-report-button-on-trends',
      'trends'
    );
  });

  $(".trend_reports__index").on('click', '.top-us-markets-itunes', function() {

    gtmLogEvent(
      'click-download-report',
      'click-download-report-button-on-trends',
      'trends'
    );
  });
});
