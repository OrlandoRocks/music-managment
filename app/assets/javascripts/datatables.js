//= require jquery.dataTables.min.js
//= require dataTables.foundation.min.js

//= require dataTables.select.min.js
//= require select.foundation.min.js

//= require dataTables.buttons.min.js
//= require buttons.foundation.min.js

//= require dataTables.responsive.min.js
//= require responsive.foundation.min.js
