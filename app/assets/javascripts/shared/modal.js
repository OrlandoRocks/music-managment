var ModalHelpers = {
  modalSelector: "",

  init(selector) {
    ModalHelpers.modalSelector = selector;
    $(selector).dialog({
      autoOpen: false,
      resizable: false,
      modal: true,
      draggable: false,
      width: 375,
      maxWidth: 375,
      height: 250,
      maxHeight: 250,
    });
  },

  open(selector) {
    $(selector).dialog("open");
  },

  close(selector) {
    $(selector).dialog("close");
  },

  closeAll() {
    ModalHelpers.close(ModalHelpers.modalSelector);
  },

  setContents(selector, contents = "") {
    $(selector).html(contents);
  },

  setContentsAndShow(selector, contents = "") {
    ModalHelpers.setContents(selector, contents);
    ModalHelpers.open(selector);
  },
};

$(() => {
  ModalHelpers.init(".generic-modal");
});
