jQuery(function($) {
  const modalClass = '.auto-renewal-prompt';

  if ($(modalClass).length) {
    setTimeout(openModal, 10000);
  }

  $(`${modalClass} .close-prompt-modal, ${modalClass} .remind-me-later, ${modalClass} .confirm-my-info`).on('click', closeModal);

  function openModal() {
    $.magnificPopup.open({
      inline: true,
      modal: true,
      preloader: false,
      items: {
        src: modalClass
      }
    });
    $(modalClass).show();
  }

  function closeModal(e) {
    Cookies.set('autorenew_remind_me_later', 'true', { expires: 1 });
    $.magnificPopup.close();
    $(modalClass).hide();
  }
});
