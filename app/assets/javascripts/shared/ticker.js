jQuery(document).ready(function($) {

  if ($('#ticker .message').length > 1) {
    initTicker();
  }

  function initTicker() {
    var messages = $('#ticker .message');
    var currentIndex = 0
    var nextIndex;

    setInterval(function() {
      nextIndex = (currentIndex + 1) % messages.length;
      rotateMessage(messages[currentIndex], messages[nextIndex]);
      currentIndex = nextIndex;
    }, 5000)
  }

  function rotateMessage(currentMessage, nextMessage) {
    $(currentMessage).hide();
    $(nextMessage).show("slide", { direction: "down" }, 700)
  }

});
