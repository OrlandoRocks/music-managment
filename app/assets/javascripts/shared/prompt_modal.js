jQuery(function($) {
  const modalClass = '.prompt-modal';

  function init() {
    if ($(modalClass).length) openModal();
    $("body").on('click', `${modalClass} .close-prompt-modal`, closeModal);
  }

  function openModal() {
    $.magnificPopup.open({
      modal: true,
      items: {
        src: modalClass,
        type: "inline"
      }
    });
  }

  function closeModal(event) {
    event.preventDefault();
    $.magnificPopup.close();
  }

  init();
});
