function redirectToEditAction(path) {
  window.onpopstate = function() {
    if (window.history.state && window.history.state.previousStep) {
      window.location = window.history.state.previousStep;
    }
  }

  var pathPrefix = path.substring(0, path.lastIndexOf('/') + 1);
  var id         = path.substring(path.lastIndexOf('/') + 1);
  var editPath   = pathPrefix + id + '/edit';

  window.history.replaceState({ previousStep: editPath }, document.title, window.location);
  window.history.pushState({}, document.title, window.location);
};
