(function() {
  var $ = jQuery;
  var payNowButton = $('button.pay_button[type="submit"]');
  payNowButton.on('click', function(){
    Cookies.set('autorenew_remind_me_later', 'true', { expires: 1 });
  });
})();
