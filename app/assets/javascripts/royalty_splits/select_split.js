jQuery(function ($) {
  const $container = $('.select-split-container')
  const $select = $container.find('.select-royalty-split')
  const $presets = $container.find('.split-presets')
  const $preset = $presets.find('.split-preset')
  const $newSplitForm = $('.new-royalty-split-form-container')

  function showSelectedSplitDetails() {
    const selectedPresetId = $select.find(':selected').val();
    if (!selectedPresetId) {
      // We've selected "Create New Preset"
      $newSplitForm.show()
      $newSplitForm.find('.royalty-split-title-input').prop('required', true)
    } else {
      $newSplitForm.hide()
      $newSplitForm.find('.royalty-split-title-input').prop('required', false)
    }
    $preset.removeClass('show')
    $preset.filter(`[data-split-id="${selectedPresetId}"]`).addClass('show')
  }

  $select.change(showSelectedSplitDetails)
  showSelectedSplitDetails()
})