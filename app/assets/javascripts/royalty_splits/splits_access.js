jQuery(() => {
  // Create split popup
  $(".splits-access-create-splits-disabled").magnificPopup({
    items: { src: ".splits-access-create-splits-popup" }
  })

  // Accept split popup
  $(".splits-access-accept-splits-disabled").magnificPopup({
    items: { src: ".splits-access-accept-splits-popup" }
  })

  $(".splits-access-suspended-splits-disabled").magnificPopup({
    items: { src: ".splits-access-suspended-splits-popup" }
  })
})
