jQuery(($) => {
  const $royaltySplit = $('.split-preset')
  const $royaltySplitSong = $('.split-tracks-form-track')

  $royaltySplit.find('[data-delete-royalty-split-id]').click((e) => {
    const royaltySplitId = e.currentTarget.dataset.deleteRoyaltySplitId;
    if (!confirm(gon.remove_split_config_confirmation)) {
      return
    }
    $.ajax({
      url: `/royalty_splits/${royaltySplitId}.json`,
      beforeSend: window.$.rails.CSRFProtection,
      type: 'delete',
      success({ royalty_split, error }) {
        $royaltySplit.has(e.target).remove();
        alert(gon.deleted_split_config)
      },
      error: function ({ status, responseText }) {
        console.error({ status, responseText })
        if (responseText) {
          alert(responseText)
        }
        if (status == 404) {
          $royaltySplit.has(e.target).remove();
        }
      },
    })
  })

  $royaltySplitSong.find('[data-delete-royalty-split-song-id]').click((e) => {
    e.preventDefault()
    e.stopPropagation()
    const royaltySplitSongId = e.currentTarget.dataset.deleteRoyaltySplitSongId;
    const $track = $royaltySplitSong.has(e.target)
    const $trackSplit = $track.find('.track-split')
    $trackSplit.fadeTo('fast', 0.3, 'linear')
    $.ajax({
      url: `/royalty_split_songs/${royaltySplitSongId}.json`,
      beforeSend: window.$.rails.CSRFProtection,
      type: 'delete',
      success({ royalty_split_song, error }) {
        $trackSplit.empty()
      },
      error: function ({ status, responseText }) {
        console.error({ status, responseText })
        if (responseText) {
          alert(responseText)
        }
        if (status == 404) {
          $trackSplit.empty()
        }
      },
      complete: function () {
        $trackSplit.fadeIn();
      }
    })
  })
})