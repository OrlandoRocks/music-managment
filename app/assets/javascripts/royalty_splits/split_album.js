jQuery(function ($) {
  const $container = $('.split-tracks-form-container')
  const $albumCheckboxes = $container.find('.row-split-album')
  const $tracks = $container.find('.split-tracks-form-track')
  const $albumContainers = $container.find('.split-album')

  // Checkboxes for album vs track
  function unselectAllTracks({ currentTarget }) {
    const $albumContainer = $albumContainers.has(currentTarget)
    const $albumTracks = $albumContainer.find($tracks)
    $albumTracks.removeClass('selected')
    $albumTracks.find('.track-id-input').prop('disabled', true)
    checkTracksForSelectAll({ currentTarget })
    checkDisableSubmit()
  }
  function selectAllTracks({ currentTarget }) {
    const $albumContainer = $albumContainers.has(currentTarget)
    const $albumTracks = $albumContainer.find($tracks)
    $albumTracks.addClass('selected')
    $albumTracks.find('.track-id-input').prop('disabled', false)
    checkTracksForSelectAll({ currentTarget })
    checkDisableSubmit()
  }
  function checkDisableSubmit() {
    // if ANY tracks are selected
    if ($tracks.hasClass('selected')) {
      $(document.body).addClass('can-submit-royalty-splits-albums-form')
    } else {
      $(document.body).removeClass('can-submit-royalty-splits-albums-form')
    }
  }
  checkDisableSubmit()

  function checkTracksForSelectAll({ currentTarget }) {
    const $albumContainer = $albumContainers.has(currentTarget)
    const $albumTracks = $albumContainer.find($tracks)
    const $notSelectedTracks = $albumTracks.not(".selected")
    if ($notSelectedTracks.length) {
      $albumContainer.removeClass('all-tracks-selected')
    } else {
      $albumContainer.addClass('all-tracks-selected')
    }
  }

  $albumCheckboxes.click((e) => {
    const $albumContainer = $albumContainers.has(e.target)
    const toggleAlbumOn = !$albumContainer.hasClass('all-tracks-selected')
    if (toggleAlbumOn) {
      selectAllTracks(e)
    } else {
      unselectAllTracks(e)
    }
  })

  // Select track
  $tracks.click((e) => {
    const $track = $(e.currentTarget);
    const $albumContainer = $albumContainers.has(e.target)
    const wasSelected = $track.hasClass('selected')
    // Select track
    if (wasSelected) {
      $track.removeClass('selected')
      $track.find('.track-id-input').prop('disabled', true)
    } else {
      $track.addClass('selected')
      $track.find('.track-id-input').prop('disabled', false)
    }
    // If no tracks are selected automatically uncheck "Split tracks"
    if (!$tracks.is('.selected')) {
      $(document.body).removeAttr('data-album-toggle')
    }
    checkDisableSubmit()
    checkTracksForSelectAll(e)
  })

  // Don't submit unless form is filled out
  $container.find('form#royalty-split-recipients-form').submit((e) => {
    // Require title if the input is visible
    const $titleInput = $('.royalty-split-title-input')
    $titleInput.prop('required', $titleInput.is(':visible'))

    // if user hasn't selected any tracks do not submit
    if (!$(document.body).hasClass('can-submit-royalty-splits-albums-form')) {
      e.preventDefault()
      alert(gon.you_must_select_a_song)
    }
  })
})