window.jQuery(($) => {
  const $body = $(document.body)
  $(".toggle-accepted-invited-splits").click((e) => {
    $body.removeClass("pending-royalty-splits-only").toggleClass("accepted-royalty-splits-only")
    $(".split-preset.pending").slideToggle()
    $(".split-preset.accepted").slideDown()
  })
  $(".toggle-pending-invited-splits").click((e) => {
    $body.removeClass("accepted-royalty-splits-only").toggleClass("pending-royalty-splits-only")
    $(".split-preset.accepted").slideToggle()
    $(".split-preset.pending").slideDown()
  })
})
