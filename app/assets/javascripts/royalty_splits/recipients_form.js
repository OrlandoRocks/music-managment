jQuery(function ($) {
  const $editFormSection = $(".edit-recipients-form-section")
  const $recipientTemplate = $("#split-collaborator-template")
  const $splitAmountsContainer = $(".split-collaborators-section")
  const $addCollaborator = $(".add-recipient-section")
  const $emailInput = $addCollaborator.find("input.add-recipient-email")

  // Don't submit duplicate emails
  $emailInput.on("input", (e) => {
    const $emailInput = $addCollaborator.find(".add-recipient-email")
    const email = $emailInput.val().toLowerCase()
    const emails = $splitAmountsContainer
      .find(".split-collaborator")
      .not(".destroyed")
      .toArray()
      .map((x) => x.dataset.email)
    if (emails.includes(email)) {
      $emailInput[0].setCustomValidity(gon.email_already_exists_in_form)
      return
    }
    $emailInput[0].setCustomValidity("")
  })

  // Add new recipient to list
  $addCollaborator.find("form.add-recipient-form").submit((e) => {
    e.preventDefault()
    const email = $emailInput.val().toLowerCase()

    // If we're re-adding a deleted recipient, then remove that deleted (and hidden) row, and re-add them.
    const $removedRow = $splitAmountsContainer.find(`.split-collaborator.destroyed[data-email="${email}"]`)
    if ($removedRow.length) {
      $removedRow.removeClass("destroyed")
      $removedRow.find(".destroy-row-field").attr("disabled", true)
      $removedRow.find(".form-percent input").val("")
      return
    }

    // Use blank template to make new row
    const $template = $($recipientTemplate.html())
    $emailInput.val("") //empty the input
    const $newRow = $template.appendTo($splitAmountsContainer)
    $newRow.find(".form-email").text(email)
    $newRow.find(".email-field").val(email)
    $newRow.attr("data-email", email)
  })

  // Split Equally
  $editFormSection.find(".button-equal-split").click((e) => {
    const numberOfSplitUsers = $splitAmountsContainer.find(".split-collaborator").not(".destroyed").length
    const eachUserPercent = (Math.floor(1000 / numberOfSplitUsers) / 10).toFixed(1)
    $splitAmountsContainer.not(".split-collaborator.current-user").find(".form-percent > input").val(eachUserPercent)
    updateOwnerTotal()
  })

  // Prevent double submitting
  $("form#royalty-split-recipients-form").submit((e) => {
    if (e.target.checkValidity()) {
      return $editFormSection.addClass("submitting")
    }
    $editFormSection.removeClass("submitting")
  })

  // Update percent for owner/current user
  function updateOwnerTotal() {
    const $otherUsersPercentInputs = $splitAmountsContainer
      .find(".split-collaborator")
      .not(".current-user")
      .not(".destroyed")
      .find(".form-input.form-percent > input")
    const sum = $otherUsersPercentInputs.toArray().reduce((sum, elem) => {
      return sum + (parseFloat(elem.value) || 0)
    }, 0)
    const $ownerCollaboratorRow = $splitAmountsContainer.find(".split-collaborator.current-user")
    let ownerPercent
    if (sum > 100) {
      $ownerCollaboratorRow.addClass("form-row-error")
      ownerPercent = 0
    } else {
      $ownerCollaboratorRow.removeClass("form-row-error")
      ownerPercent = 100 - sum
    }
    $splitAmountsContainer.find(".split-collaborator.current-user .form-percent > input").val(ownerPercent.toFixed(1))
  }

  // Change percent
  $splitAmountsContainer.on("change", ".form-input.form-percent > input", (e) => {
    $splitAmountsContainer.find(".split-collaborator").has(e.target).removeClass("form-row-error")
    updateOwnerTotal()
  })

  // Delete split recipient
  $splitAmountsContainer.on("click", ".row-remove-button", function (e) {
    const $row = $splitAmountsContainer.find(".split-collaborator").has(e.target)
    $row.addClass("destroyed")
    $row.find(".form-percent > input").prop("required", false)
    $row.find("input.destroy-row-field").prop("disabled", false)
    updateOwnerTotal()
  })

  $("#edit-split-form").submit((e) => {
    const { songsCount, releasesCount } = e.currentTarget.dataset
    const confirmText = gon.will_affect_X_songs_Y_releases.replace("{{songs}}", songsCount).replace("{{releases}}", releasesCount)
    if (!confirm(confirmText)) {
      e.preventDefault()
    }
  })
})
