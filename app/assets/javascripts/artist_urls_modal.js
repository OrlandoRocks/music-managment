(function(){
  handleAddArtistUrlClick();
  handleOnCancel();
  handleOnSave();
  handleOnBlur();
  handleOnPageRefresh();
  handleNewArtist();

  function handleAddArtistUrlClick() {
    $(".primary_artists").on("click", ".add-artist-url", function(e) {
      e.preventDefault();

      var $artistRow  = $(e.target.parentElement);
      var artistIndex = $artistRow.find(".creative_number").html();
      var artistName  = $artistRow.find(".songwriter-artist").val();
      var modalKey    = ".creative[data-creative-row='" + artistIndex + "']";

      var appleArtistUrl   = getArtistUrlValue(e.target.parentElement, "apple", "value");
      var spotifyArtistUrl = getArtistUrlValue(e.target.parentElement, "spotify", "value");

      $.magnificPopup.open({
        inline: true,
        items: {
          src: ".artist-url-modal"
        },
        key: modalKey,
        mainClass: "artist-url-modal-show",
        preloader: false,
        fixedBgPos: true,
        modal: true,
        callbacks: {
          open: function() {
            $(".artist-modal-creative-name").text(artistName);
            setArtistUrlValue(".artist-url-modal", "apple", "input", appleArtistUrl);
            setArtistUrlValue(".artist-url-modal", "spotify", "input", spotifyArtistUrl);
            setupServiceNewArtistOnModalOpen(spotifyArtistUrl, "spotify", this.st.key);
            setupServiceNewArtistOnModalOpen(appleArtistUrl, "apple", this.st.key);
          },
          close: function() {
            var modalInstance = this.st;

            if (!modalInstance.isCancelButton) {
              var creativeRow      = modalInstance.key;
              var appleArtistUrl   = $(".apple-artist-url-input").val();
              var spotifyArtistUrl = $(".spotify-artist-url-input").val();

              setArtistUrlValue(creativeRow, "apple", "value", appleArtistUrl);
              setArtistUrlValue(creativeRow, "spotify", "value", spotifyArtistUrl);
              setNewArtist();
            }
          }
        }
      });
    });
  };

  function handleOnCancel() {
    $(".artist-url-modal-cancel").click(function(e) {
      e.preventDefault();
      $.magnificPopup.instance.st.isCancelButton = true;
      $.magnificPopup.close();
      $(".artist-urls-container").find(".invalid-url-error-msg").each(function(i, row) {
        removeErrors($(row));
      });
    });
  }

  function handleOnSave() {
    $(".artist-url-modal-save").click(function(e) {
      e.preventDefault();

      var $artistUrlContainer = $(".artist-urls-container");
      $artistUrlContainer.find(":input").each(function(i, row) {
        displayErrorsIfInvalidUrl(row);
      });

      var noErrors = $artistUrlContainer.find(".invalid-url-error-msg").length === 0;
      if (noErrors) $.magnificPopup.close();
    });
  }

  function handleOnBlur(e) {
    $(".artist-url-input").blur(function(e) {
      if (!elIsCancelButton(e)) {
        displayErrorsIfInvalidUrl(e.target);
      }
    });
  }

  function handleOnPageRefresh() {
    var artistUrls = gon.artist_urls;

    if (artistUrls) {
      artistUrls.forEach(function(artistUrl) {
        var $input = $("input.main_artist[value='" + artistUrl.artist_name + "'").parent();

        setupServiceOnLoad(artistUrl, $input);
      });
    }
  }

  function setNewArtist(){
    var services = ['spotify', 'apple'];

    services.forEach(function(serviceName) {
      var modalInstanceKey = $.magnificPopup.instance.st.key;
      var $newArtistVal = $(modalInstanceKey).find("." + serviceName + "-new-artist-url-value");
      var $newArtistInput = $("." + serviceName + "-new-artist-url-input");
      var isChecked = $newArtistInput.is(":checked");

      $newArtistVal.val(isChecked);
    });
  }

  function handleNewArtist(){
    var services = ['spotify', 'apple'];

    services.forEach(function(service) {
      var newInputClass = newArtistInputClass(service);
      var inputClass = artistUrlInputClass(service);

      $(newInputClass).click(function(e){
        var isChecked = $(e.target).is(":checked");

        hideShowArtistUrlInput($(e.target).parent(), service, isChecked);

        if (isChecked) {
          $(inputClass).val('');
        }
      });
    });
  }

  function newArtistInputClass(service) {
    return "." + service + "-new-artist-url-input";
  }

  function newArtistValueClass(service) {
    return "." + service + "-new-artist-url-value";
  }

  function artistUrlInputClass(service) {
    return "." + service + "-artist-url-input";
  }

  function hideShowArtistUrlInput($input, serviceName, shouldDisable){
    var inputClass = artistUrlInputClass(serviceName);
    $input.find(inputClass).prop('disabled', shouldDisable);
  }

  function getArtistUrlValue(el, serviceName, elementType) {
    return $(el).find("." + serviceName + "-artist-url-" + elementType).val();
  }

  function setArtistUrlValue(el, serviceName, elementType, value) {
    return $(el).find("." + serviceName + "-artist-url-" + elementType).val(value);
  }

  function setNewArtistValue(el, serviceName, val) {
    var valueClass = newArtistValueClass(serviceName);
    return $(el).find(valueClass).val(val);
  }

  function addErrors(row) {
    row.addClass("invalid-url-error-msg");
    row.find(".error-msg").removeClass("hidden");
  }

  function removeErrors(row) {
    row.removeClass("invalid-url-error-msg");
    row.find(".error-msg").addClass("hidden");
  }

  function displayErrorsIfInvalidUrl(element) {
    var artistUrl     = element.value;
    var serviceName   = element.id.split("__")[1];
    var $artistUrlRow = $("." + serviceName + "-row");
    var mainArtistRow = $.magnificPopup.instance.st.key;
    var $checkmark    = $(mainArtistRow).find("." + serviceName + "-url-checkmark");
    var validUrl      = validateArtistUrl(artistUrl, serviceName);
    validUrl ? removeErrors($artistUrlRow) : addErrors($artistUrlRow);

    if (artistUrl !== "" && validUrl) {
      $checkmark.addClass("green");
    } else {
      $checkmark.removeClass("green");
    }
  }

  function validateArtistUrl(url, serviceName) {
    if (url === "") return true;

    if (serviceName === "apple") {
      // https://regexr.com/4f5nk | https://regexr.com/4f5ou
      return /^https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/id.*/.test(url) ||
        /^https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/.*/.test(url);
    } else if (serviceName === "spotify") {
      return /^https:\/\/open.spotify.com\/artist\/\w+(?:\??|(?:[?]\w+=\w+)(?:&\w+=\w+)*)*$|^spotify:artist:\w+$/.test(url);
    }
  }

  function disableInput(element) {
    element.prop('disabled', true);
  }

  function setupServiceOnLoad(artistUrl, $input) {
    var serviceName;

    if (artistUrl.apple) {
      serviceName = 'apple';
    }

    if (artistUrl.spotify) {
      serviceName = 'spotify';
    }

    var newArtistInput = newArtistInputClass(serviceName);
    var newArtistValue = newArtistValueClass(serviceName);

    if (artistUrl.is_new_artist) {
      $input.find(newArtistValue).val(true);
    } else {
      $input.find("." + serviceName + "-artist-url-value").val(artistUrl[serviceName]);
      $input.find(newArtistValue).val(false);
      if (artistUrl.freeze_new_artist_editing) {
        disableInput($input.find(newArtistInput));
      }
    }
  }

  function setupServiceNewArtistOnModalOpen(thisUrl, serviceName, creativeRow){
    var newInputClass = newArtistInputClass(serviceName);
    var newArtistValue = newArtistValueClass(serviceName);
    var urlInputClass = artistUrlInputClass(serviceName);

    var newArtistValSet = $(creativeRow).find(newArtistValue).val();

    $(urlInputClass).prop('disabled', newArtistValSet === 'true');

    $(newInputClass).prop('disabled', false);
    $(newInputClass).prop('checked', newArtistValSet === 'true');

    if (gon.artist_urls) {
      gon.artist_urls.forEach(function(url) {
        // Handle if there's a preexisting artist url in system.
        if (url[serviceName] === thisUrl) {
          if (url.freeze_new_artist_editing) {
            disableInput($(newInputClass));
          } else {
            $(newInputClass).prop('disabled', false);
          }
        }
      });
    }
  }

  function elIsCancelButton(e){
    return $([e.target, e.relatedTarget]).hasClass("artist-url-modal-cancel");
  }
}());
