const RefundModal = {
  showElement: function (elementSelector, boolean = true) {
    if (boolean) {
      $(elementSelector).removeClass("hide");
      $(elementSelector).show();
    } else {
      $(elementSelector).addClass("hide");
      $(elementSelector).hide();
    }
  },
  createFromTemplate(templateSelector) {
    const template = $(templateSelector)[0];
    const errorDiv = template.content.firstElementChild.cloneNode(true);
    return errorDiv;
  },
  showLoader: function (boolean = true) {
    RefundModal.showElement(
      "#refund-invoices-list-modal .modal-loader",
      boolean
    );
  },
  showTable: function (boolean = true) {
    RefundModal.showElement("#refund-invoices-table", boolean);
  },
  showError: function (boolean, message) {
    if (!boolean) {
      $("#refund-modal-table-body .error-item").remove();
      return;
    }

    const element = RefundModal.createFromTemplate("#refund-modal-error-item");
    message && $(element).find(".error-col").html(message);
    $("#refund-modal-table-body").html(element);
    RefundModal.showTable();
  },
  convertAPIData: function ({
    refund_date,
    credit_note_invoice_number,
    total_amount,
    currency,
    link,
  }) {
    return {
      date: refund_date,
      id: credit_note_invoice_number,
      amount: total_amount,
      currency,
      link,
    };
  },
  fetchData: function (invoiceId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `/invoices/${invoiceId}/refunds`,
        type: "get",
        success: function (data) {
          try {
            const refundList = data.refunds.map(RefundModal.convertAPIData);
            resolve(refundList);
          } catch (error) {
            reject(error);
          }
        },
        error: function (error) {
          reject(error.statusText);
        },
      });
    });
  },
  showModal: function () {
    $("#modal-bind")
      .magnificPopup({
        items: {
          src: "#refund-invoices-list-modal",
        },
        closeOnContentClick: false,
        closeOnBgClick: false,
        showCloseBtn: true,
        enableEscapeKey: false,
        midClick: true,
      })
      .magnificPopup("open");
  },
  resetModal: function () {
    RefundModal.showLoader(true);
    RefundModal.showError(false);
    RefundModal.showTable(false);
  },
  /**
   * Parses the data passed into Table Rows and adds to the Modal Table.
   * @param {Array of {date, id, amount, currency, link}} data
   */
  showRefundData: function (data) {
    if (!Array.isArray(data)) {
      throw new Error("Bad Data Format");
    }

    let rows = [];
    if (data.length === 0) {
      const emptyRow = RefundModal.createFromTemplate(
        "#refund-modal-empty-item"
      );
      rows.push(emptyRow);
    } else {
      rows = data.map(({ date, id, amount, currency, link }) => {
        const dataRow = RefundModal.createFromTemplate(
          "#refund-modal-list-item"
        );
        $(dataRow).find(".date-col").html(date);
        $(dataRow).find(".id-col").html(id);
        $(dataRow).find(".amount-col").html(amount);
        $(dataRow).find(".currency-col").html(currency);
        $(dataRow).find(".link-col a").attr("href", link);
        return dataRow;
      });
    }

    $("#refund-modal-table-body").empty();
    rows.forEach((element) => $("#refund-modal-table-body").append(element));
    RefundModal.showTable();
  },
  showRefundsList: function (invoiceId) {
    RefundModal.resetModal();
    RefundModal.showModal();
    const response = RefundModal.fetchData(invoiceId);
    response
      .then((data) => {
        RefundModal.showLoader(false);
        RefundModal.showRefundData(data);
      })
      .catch(() => {
        RefundModal.showLoader(false);
        RefundModal.showError(true);
      });
  },
};
