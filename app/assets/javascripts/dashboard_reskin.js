jQuery(document).ready(function($) {
  /**
   * Selectors
   */

  const balanceIcon = 'i.foreign-exchange-balance-display'
  const balanceIconEl = $(balanceIcon)

  const releaseTypeHeadings = ".recent-releases__types h3"
  const releaseTypeHeadingEls = $(releaseTypeHeadings)

  const releaseSections = $(".recent-releases")
  const splitsPendingBanner = $('#close-splits-pending-banner')

  /**
   * Listeners
   */

  $(document).on('mouseover', balanceIcon, toggleBalanceIcon);
  $(document).on('click', releaseTypeHeadings, toggleReleaseType);
  $(splitsPendingBanner).on('click', closeSplitsPendingBanner);

  /**
   * Functions
   */

  function toggleReleaseType(e) {
    const { type } = e.target.dataset;
    const typeClass = `.recent-releases.${type}`;

    [releaseTypeHeadingEls, releaseSections].forEach(el => el.removeClass("active"));
    [$(e.target), $(typeClass)].forEach(el => el.addClass("active"));
  }

  function toggleBalanceIcon(e) {
    const tooltip = balanceIconEl.data('tooltip')

    // Docs for qtip are no longer online, but they're archived here:
    // https://web.archive.org/web/20200217030108/http://qtip2.com/guides
    balanceIconEl.qtip({
      overwrite: false,
      content: tooltip,
      show: { event: e.type, ready: true },
      // disable hide for development:
      // hide: {
      //   fixed: true,
      //   event: ''
      // },
      position: {
        adjust: { mouse: false },
        at: 'bottom left',
        my: 'bottom left',
        target: 'mouse',
      },
      style: { classes: 'balance__qtip' }
    }, e)
  }

  function closeSplitsPendingBanner(e) {
    const banner = $('#splits-pending-banner')
    banner.remove()
    $.post( "/dashboard/close_splits_pending_banner");
  }
})
