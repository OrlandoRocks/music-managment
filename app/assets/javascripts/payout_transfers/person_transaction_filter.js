jQuery(document).on("ready", function() {
  var $ = jQuery;

  var $clearSelection = $("#completed_transaction_filter_clear_selection");
  var $selectAll = $("#completed_transaction_filter_select_all");
  var $filterButton = $("#balance_history_table_filter_button");
  var $filterForm = $("#balance_history_table_filter_form");
  var $filterOption = $(".balance-history-table-filter__form__transaction-types__input");
  var $filterCancelButton = $("#balance_history_table_filter_cancel_button");

  $clearSelection.on("click", function(e) {
    e.preventDefault();
    $filterOption.each(function() {
      this.checked = false;
    })
  });

  $selectAll.on("click", function(e) {
    e.preventDefault();
    $filterOption.each(function() {
      this.checked = true;
    })
  });

  $filterButton.on("click", toggleForm);
  $filterCancelButton.on("click", toggleForm);

  function toggleForm() {
    $filterForm.toggleClass("hide");
  }
});
