//= require jquery.maskMoney

jQuery('.show-exchanged-balance').on('click', function(){
  jQuery('.user-currency-balance-display').show();
});

jQuery('.toggle-foreign-currency').on('click', function(){
  if(jQuery('.payout-toggled-value').is(':visible')){
    jQuery('.payout-toggled-value').hide();
    jQuery('.payout-untoggled-value').show();
  } else {
    jQuery('.payout-toggled-value').show();
    jQuery('.payout-untoggled-value').hide();
  }
})

jQuery(document).on("ready", function() {
  var currency = {
    "USD": "$",
    "AUD": "$",
    "CAD": "$",
    "EUR": "€",
    "GBP": "£"
  }[gon.currency];

  var $payoutTransferInput = jQuery(".payout-transfer-input");
  var $payoutTransferButton = jQuery(".payout-transfer-container .payout-transfer-btn.submit");
  var $amountCentsRaw = jQuery("#payout_transfer_create_form_amount_cents_raw");
  var $errorsSection = jQuery(".payout-transfer-container .errors");
  var availableAmount = jQuery("#payout_transfer_create_form_available_amount").val();
  var amountTooHighMessage = jQuery(".payout-transfer-container .amount_too_high_error").html();

  $payoutTransferInput.maskMoney({
    allowZero: true,
    prefix: currency,
    thousands: gon.currency_separator,
    decimal: gon.currency_delimiter
  });

  $payoutTransferInput.on('keyup', function() {
    var selectedAmount = parseFloat($payoutTransferInput.maskMoney('unmasked')[0]);
    var isAmountTooHigh = selectedAmount > availableAmount;
    var zeroAmount = (selectedAmount === 0);

    $payoutTransferInput.toggleClass("with-error", isAmountTooHigh);
    $errorsSection.html(isAmountTooHigh ? amountTooHighMessage : "");
    $payoutTransferButton.attr("disabled", isAmountTooHigh || zeroAmount);
    $amountCentsRaw.val(Math.round(selectedAmount * 100)); //Converts to lowest denominator (like cents)
  });

  $payoutTransferInput.trigger("keyup"); // To trigger initial validation after load
  $payoutTransferInput.focus();

});
