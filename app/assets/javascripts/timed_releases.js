(function($){
    var getDaysInMonth = function(month,year) {
        return new Date(year, month, 0).getDate();
    };

    var getSelectedDay = function() {
        return $("#timed-release-day-select option:selected").val();
    };

    var adjustDaySelectOptions = function(daysNum) {
        var html = '';
        var selectedDay = getSelectedDay();

        for (var i = 1; i <= daysNum; i++) {
            if (selectedDay === i.toString()) {
                html += "<option value='" + i + "' selected='selected'>" + i + "</option>";
            } else {
                html += "<option value='" + i + "'>" + i + "</option>";
            }
        }

        $("#timed-release-day-select").html(html);
    };

    var resetDaySelect = function() {
        var month = $("#timed-release-month-select option:selected").val();
        var year = $("#timed-release-year-select option:selected").val();

        var days = getDaysInMonth(month, year);
        adjustDaySelectOptions(days);
    };

    var watchSelectors = function (selectors) {
        $(document).on('change', selectors.join(', '), function () {
            resetDaySelect();
        });
    };

    resetDaySelect();

    watchSelectors(['#timed-release-month-select, #timed-release-year-select']);
}(jQuery));
