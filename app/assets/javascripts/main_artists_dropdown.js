jQuery(document).ready(function($) {
  var main_artists_list = gon.mainArtistDropdown;

  function deleteList() {
    $(".main-artists-dropdown").remove();
  }

  function highlightArtist(element) {
    unhighlightArtist('.main-artist-suggestion');
    $(element).addClass('highlighted-option');
    $(element).attr('data-selected-artist', "1");
  }

  function unhighlightArtist(element) {
    $(element).removeClass('highlighted-option');
    $(element).removeAttr('data-selected-artist');
  }

  function fillInArtist(thisElement) {
    var closestInput = $(thisElement).closest('li[data-creative-row]').find('input.album_creatives__name');

    closestInput.val($(thisElement).html());

    deleteList();
  }

  function makeDropdownList(appendToThis) {
    var listHTML = "<div class='main-artists-dropdown'><ul></ul></div>";

    $(appendToThis).closest('li[data-creative-row]').append(listHTML);

    $('.main-artists-dropdown ul').css('width', '100%');
    adjustLeftAlignOnDropdown();
  }

  function filterList(input){
    $(".main-artists-dropdown ul").html('');

    var artistCount = 0;

    main_artists_list.forEach(function(artist) {
      if (artist.match(new RegExp(input, 'gi'))) {
        var artistRow = "<li class='main-artist-suggestion'>" + artist + "</li>";
        $(".main-artists-dropdown ul").append(artistRow);
        var $newEl = $(".main-artists-dropdown ul").children().last();
        $newEl.hover(function(){highlightArtist($newEl);}, function(){unhighlightArtist($newEl);});
        $newEl.mousedown(function(){fillInArtist($newEl);});

        if (artistCount == 0) { //Select first artist in list by default.
          highlightArtist($newEl);
        }
        artistCount++;
      }
    });

    !artistCount ? deleteList() : null;
  }

  function showAllArtist(){
    $(".main-artists-dropdown ul").html('');

    var artistCount = 0;

    main_artists_list.forEach(function(artist) {
      var artistRow = "<li class='main-artist-suggestion'>" + artist + "</li>";
      $(".main-artists-dropdown ul").append(artistRow);
      var $newEl = $(".main-artists-dropdown ul").children().last();
      $newEl.hover(function(){highlightArtist($newEl);}, function(){unhighlightArtist($newEl);});
      $newEl.mousedown(function(){fillInArtist($newEl);});

      if (artistCount == 0) { //Select first artist in list by default.
        highlightArtist($($newEl));
      }
      artistCount++;
    });
  }

  function adjustLeftAlignOnDropdown(){
    var dropdown = $(".main-artists-dropdown");
    var creativeNumberWidth = $('.creatives_label').outerWidth();

    if ($('[data-creative-row]').length > 1 && dropdown.length) {
      $(".main-artists-dropdown").css({left: creativeNumberWidth + 'px'});
    } else {
      $(".main-artists-dropdown").css({left: ''});
    }
  }

  function runInputListener() {
    $(document).on('input', '.album_creatives__name', function(e) {
      deleteList();
      makeDropdownList(e.target);

      var input = $(e.target).val();

      if (!input.trim()) {
        return showAllArtist();
      }

      filterList(input);
    });
  }

  function handleArrowUp(){
    var selected = $('[data-selected-artist]');
    if (selected.length) {
      unhighlightArtist(selected);
      var nextArtist = selected.prev().length ? selected.prev() : selected.siblings().last();
      highlightArtist(nextArtist);
    }
  }

  function handleArrowDown() {
    var selected = $('[data-selected-artist]');
    if (selected.length) {
      unhighlightArtist(selected);
      var nextArtist = selected.next();
      if (nextArtist.length) {
        highlightArtist(nextArtist);
      } else {
        highlightArtist(selected.siblings().first());
      }
    }
  }

  function handleEnter(){
    var selected = $('[data-selected-artist]');

    fillInArtist(selected);
    deleteList();
  }

  function showOnFocus(){
    $(document).on('focus', '.album_creatives__name', function(e){
      deleteList();
      makeDropdownList(e.target);

      var input = $(e.target).val();

      if (!input.trim()) {
        return showAllArtist();
      } else {
        return filterList(input);
      }
    });
  }

  function hideOnFocusOut(){
    $(document).on('blur', '.album_creatives__name', function(){
      deleteList();
    });
  }

  function handleKeyBoardSelection() {
    $(document).keydown(function(e) {
      if (!$('.main-artists-dropdown').length) {
        return;
      }

      switch(e.keyCode) {
        case 38: //up
          handleArrowUp();
          break;
        case 40: //down
          handleArrowDown();
          break;
        case 13: //enter
          handleEnter();
        case 27: //esc
          deleteList();
        default: return;
      }
      e.preventDefault();
    });
  }

  runInputListener();

  handleKeyBoardSelection();

  showOnFocus();

  hideOnFocusOut();
});
