jQuery(document).ready(function($){
  function show_tooltip(e) {
    var $element = $(e.target)
    var tooltip = $element.data('tooltip');

    $element.qtip({
      overwrite: false,
      content: tooltip,
      position: {
        my: 'left middle',
        at: 'right middle',
      },
      style: { 
          classes: 'qtip-light',
      },
      show: { event: e.type, ready: true },
      hide: { delay: 200, fixed: true }
    }, e);
  }
  $(".unique-payout-tooltip").on('hover', 'i.qtip-unique-payout', show_tooltip)
});
