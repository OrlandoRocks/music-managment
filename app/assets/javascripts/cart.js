jQuery(document).ready(function ($) {

  $('#checkout-proceed').on('click', function () {
    $('.loading-cart-spinner').show();
  });

  $('.skip-confirm-and-pay').on('click', function () {
    // There are two submit buttons on the page, so:
    // Disable other buttons after Rails UJS handles the click and it disables the clicked button
    setTimeout(() => $('button').prop("disabled", true), 0)
  })

  window.TC = window.TC || {};
  TC.Cart = function () {
    var toggleReleasesLinks = $('.show_releases'),
      paypalConnectDialog = $('#paypal_connect'),
      paymentProcessingDialog = $('#payment_processing'),
      errorDialog = $('#error_dialog'),
      form = $('#payment_form'),
      radios = $('.payment_select'),
      alreadySubmitted = false,
      self = {
        init: function () {
          return self.bind()
            .switchBilledTo(),
            self;
        },

        bind: function () {
          return self.bindClickToggleReleases()
            .bindPaypalConnectDialog()
            .bindPaypalConnectLinks()
            .bindPromoLink()
            .bindApplyPromoBtn()
            .bindCancelPromoBtn()
            .bindRadioManager()
            .bindErrorDialog()
            .bindPaymentProcessingDialog(),
            self;
        },

        bindClickToggleReleases: function () {
          toggleReleasesLinks.click(function (e) {
            self.toggleReleases($(this));
            e.preventDefault();
          });
          return self;
        },

        bindPaypalConnectDialog: function () {
          paypalConnectDialog.dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            draggable: false,
            width: 375,
            maxWidth: 375,
            height: 250,
            maxHeight: 250
          });
          return self;
        },

        bindPaypalConnectLinks: function () {
          $('.paypal_connect').on('click', function (e) {
            paypalConnectDialog.dialog('open');
            e.preventDefault();
          });
          return self;
        },

        bindPromoLink: function () {
          $('.link-promo').on('click', function (e) {
            var t = $(this);
            t.hide();
            t.next().show();
            e.preventDefault();
          });
          return self;
        },

        bindApplyPromoBtn: function () {
          $('.box-promo .btn-action.apply').on('click', function (e) {
            var t = $(this),
              parent = t.parent(),
              code = parent.find('input:first').val(),
              action = parent.attr('action'),
              form = $('#new_cert');
            form.find('input#cert_cert').val(code);
            form.attr('action', action).submit();
            e.preventDefault();
          });
          return self;
        },

        bindCancelPromoBtn: function () {
          $('.box-promo .btn-action.cancel').on('click', function (e) {
            var t = $(this);
            t.parent().hide();
            t.parent().prev().show();
            e.preventDefault();
          });
          return self;
        },

        bindRadioManager: function () {
          radios.on('click', function (e) {
            $('.ccbox').removeClass('selected');
            // Remove 'selected' from any adyen-payment-selection
            // resets the selected item styles
            $(".adyen-payment-selection").removeClass("selected");

            $(this).closest('.ccbox').addClass('selected');
            self.switchBilledTo();
          });
          if (radios.is(':checked')) {
            // Click the default selected radio button to trigger the click handler (defined above)
            radios.filter(":checked").click();
          }
          return self;
        },

        bindErrorDialog: function () {
          errorDialog.dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            draggable: false,
            width: 375,
            maxWidth: 375,
            height: 250,
            maxHeight: 250
          });
          return self;
        },

        toggleReleases: function (link) {
          var releases = link.parent().parent().next();
          if (releases.is(':visible')) {
            releases.hide();
            link.html('Show releases');
          }
          else {
            releases.show();
            link.html('Hide releases');
          }
          return self;
        },

        switchBilledTo: function () {
          var billedToID = $('.payment_select:checked').val();
          $('.billed_to').hide();
          $('#billed_to-' + billedToID).show();

          var txt;
          if (billedToID == 'paypal') {
            txt = gon.paypal_account;
          } else {
            txt = gon.credit_card;
          }
          $('.charge_item').html(txt);
          return self;
        },

        bindPaymentProcessingDialog: function () {
          paymentProcessingDialog.dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            draggable: false,
            width: 375,
            maxWidth: 375,
            height: 250,
            maxHeight: 250,
          });
          form.on('submit', function (event) {
            const result = handleConfirmPageFormSubmit(event);
            if (!result) {
              event.preventDefault();
              return false;
            }

            const paymentMethodsPresent = isPaymentMethodPresent();
            const hasPaymentMethod = hasAPaymentMethodSelected();
            if (paymentMethodsPresent && !hasPaymentMethod) {
              ModalHelpers.open("#no-payment-method-alert-modal");
              event.preventDefault();
              return false;
            }

            let cvv_input = $('.ccbox.selected input.cvv:visible');
            if (cvv_input.length > 0 && cvv_input.val() === "") {
              alert("Please enter CVV for the selected card");
              return false;
            } else {
              $("#cart_finalize_form_cvv").val(cvv_input.val());
              paymentProcessingDialog.dialog('open');
            }

            if (typeof clearConfirmAndPayStorage === "function") {
              clearConfirmAndPayStorage();
            }

            if (
              ThreeDSecure.enabled &&
              ThreeDSecure.selectedCardEnabled() &&
              CheckoutForm.hasAmountToPay() &&
              CheckoutForm.isCountryAffiliatedToBI()
            ) {
              ThreeDSecure.verify().then((result) => {
                if (result) {
                  form[0].submit();
                } else {
                  paymentProcessingDialog.dialog("close");
                }
              });
              return false;
            } else {
              CheckoutForm.remove3DSecureDataFromForm();
            }
          });
          return self;
        }

      };
    return self.init(),
      self;
  }();

  function show_tooltip(e) {
    var $element = $(e.target)
    var tooltip = $element.data('tooltip');

    $element.qtip({
      overwrite: false,
      content: tooltip,
      show: { event: e.type, ready: true },
      hide: { delay: 200, fixed: true }
    }, e);
  }
  $(".cart_items").on('click', 'i.qtip-it', show_tooltip)

  if (typeof addressInfoValidations === "function") {
    addressInfoValidations();
  }

  initializeConfirmInformationInputErrors();

  if (window.onCartPage && typeof clearConfirmAndPayStorage === "function") {
    clearConfirmAndPayStorage();
  }
});

var addressInputChangeHandlersAdded = false;
var mapConfirmInputsAndError = [];

function isInputRequired(element) {
  const isFEValidationRequired = $(element).data("feValidationRequired");
  return JSON.parse(isFEValidationRequired || false);
}

function hideErrorOnInputChange(input, errorDiv) {
  $(input).on("change", function () {
    $(errorDiv).hide();
  });
}

function addErrorDivAfterInput(input) {
  const errorMessage =
    gon && gon.translations ? gon.translations.required_field : "Required";

  const id = Math.random().toString(36).substring(7);
  const errorDivId = `address_field_${id}_error_text`;

  $(input).closest('.context-wrapper').append(
    `<div id="${errorDivId}" class="address-field-error error-text hide">${errorMessage}</div>`
  );

  return errorDivId;
}

function initializeConfirmInformationInputErrors() {
  const formInputs = $(
    ".compliance-info-input, .address-input, .country-select-input"
  );

  formInputs.each(function () {
    const input = this;
    const errorDivId = addErrorDivAfterInput(input);
    const errorDiv = $(`#${errorDivId}`)[0];
    mapConfirmInputsAndError.push({ input, errorDiv });
    hideErrorOnInputChange(input, errorDiv);
  });
}

function clearConfirmInformationErrors() {
  $(".address-field-error").hide();
}

function isInputInvalid(input) {
  return (
    $(input).is(":visible") &&
    isInputRequired(input) &&
    (!input.checkValidity() || input.value === "")
  );
}

function handleConfirmPageFormSubmit() {
  try {
    clearConfirmInformationErrors();
    let isFormComplete = true;
    let firstIncompleteInput = null;

    mapConfirmInputsAndError.forEach(({ input, errorDiv }) => {
      if (isInputInvalid(input)) {
        $(errorDiv).show();

        isFormComplete = false;

        if (!firstIncompleteInput) {
          firstIncompleteInput = input;
        }
      }
    });

    if (!isFormComplete) {
      firstIncompleteInput.focus();
      return false;
    }
  } catch (error) {
    console.error("Address Form Exception! ", error);
  }

  return true;
}

function selectedUSAndTerritories(country_input) {
  var us_and_territories = $('.country-select-input').data('optionalAddressCountries');
  return (us_and_territories || []).includes(country_input);
}

function clearConfirmAndPayStorage() {
  sessionStorage.removeItem("confirmAndPayUserData");
}
