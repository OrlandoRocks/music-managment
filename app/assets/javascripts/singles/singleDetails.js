$( document ).ready(function() {
  songNameListener()
});

function songNameListener() {
  $(document).on("update_single_name", function(e) {
    var eventDetail = e.originalEvent.detail
    var name = eventDetail.name;
    var primaryArtistNames  = eventDetail.primary_artist_names;
    $("#single_name").html(name);
    $("#single_artist_name").html(primaryArtistNames);
  });
}
