// autoReload is a utility function to reload page every x seconds.
//
// USAGE INSTRUCTIONS:
// ===================
//
// Include an HTML element in the page you would like to refresh with the id "auto-reload-page".
// The default interval to reload is 30 seconds, which you can override by specifying "data-reload-interval"
// in the marker element.

jQuery(document).ready(function($) {
  var autoReloadMarkerID = '#auto-reload-page';
  var autoReloadElement = $(autoReloadMarkerID);

  function reloadPage() {
    window.location.reload();
  }

  if (autoReloadElement.length > 0) {
    var options = {
      intervalInSeconds: autoReloadElement.data("reload-interval") || 30,
    };

    setTimeout(reloadPage, (options.intervalInSeconds * 1000));
  }
});
