$(document).ready(function() {
  $('#sad, #happy').on('click', function (e) {
    e.preventDefault();
    const button = $(e.target);
    const emojisContainer = $('.plan-survey-emojis');
    const textResponse = $('#plan_survey_text_response');
    const followUp = $('.plan-survey-follow-up');
    const thankYou = $('#plan_survey_thank_you');
    const surveyContainer = $('.survey-container');
    const somethingWentWrong = $('.something-went-wrong-plan-survey');
    const { personId, surveyId, requestType, happy } = button.data();
    $.ajax({
      url: "/survey_responses",
      type: requestType,
      data: {
        survey_response:
          {
            json: { happy: happy, text_response: textResponse.val() },
            person_id: personId,
            survey_id: surveyId
          }
      }
    }).done(function(response) {
      if (response.success) {
        emojisContainer.hide();
        if (!happy) {
          textResponse.data('surveyResponseId', response.survey_response_id);
          followUp.show();
        } else {
          thankYou.show();
        }
      } else {
        surveyContainer.hide();
        somethingWentWrong.show();
      }
    });
  });

  $('#plan_survey_text_response').keydown(function(e){
    const keycode = (e.keyCode ? e.keyCode : e.which);
    const textResponse = $(e.target);
    const followUp = $('.plan-survey-follow-up');
    const thankYou = $('#plan_survey_thank_you');
    const charCounter = $('#char_counter');
    const surveyContainer = $('.survey-container');
    const somethingWentWrong = $('.something-went-wrong-plan-survey');
    const { personId, surveyId, requestType, happy, surveyResponseId } = textResponse.data();
    if(keycode == '13'){
      e.preventDefault();
      $.ajax({
        url: '/survey_responses/' + surveyResponseId,
        type: requestType,
        data: {
          survey_response:
            {
              json: { happy: happy, text_response: textResponse.val() },
              person_id: personId,
              survey_id: surveyId
            }
        }
      }).done(function(response) {
        if(response.success) {
          followUp.hide();
          thankYou.show();
        } else {
          surveyContainer.hide();
          somethingWentWrong.show();
        }
      });
    } else {
      charCounter.text((textResponse.val().length) + "/140");
    }
  });

  $('.plans-toggle .plan-option').click(function () {
    $('.plan-option').toggleClass('active');
    $('.per-release-plan').toggle();
    $('.plans').toggle();
  });
});
