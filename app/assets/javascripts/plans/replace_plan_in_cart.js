var $ = jQuery;

$(document).ready(function() {
  $('.plan-cta').on('click', function(e) {
    var button = $(e.target);
    var planId = button.data('planId');
    var clickedPlanPrice = button.data('clickedPlanPrice');
    e.preventDefault();

    $.ajax({
      url: "/plans/verify_cart_already_has_a_plan",
      type: "get",
      data: { clicked_plan_id: planId, clicked_plan_price: clickedPlanPrice }
    }).done(function(response) {
      if(response.pricier_plan_in_cart) {
        $.magnificPopup.open({
          items: {src: response.popup_template, type: 'inline'}
        });
      } else {
        button.closest('form').unbind('submit').submit();
      }
    });
  });
});
