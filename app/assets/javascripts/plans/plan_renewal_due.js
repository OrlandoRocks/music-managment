var $ = jQuery;

$(document).ready(function() {
  $.ajax({
    url: "/plans/plan_renewal_due",
    type: "get"
  }).done(function(response) {
    if(response.plan_renewal_due) {
      $.magnificPopup.open({
        items: {src: response.popup_template, type: 'inline'},
        }
      );
    }
  });
});
