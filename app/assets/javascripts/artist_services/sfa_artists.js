(function() {
  var $ = jQuery;

  $('#spotify-artist-select').on('change', function() {
    if ($(this).val() == "") {
      $('.sfa-button > a').addClass('grey-out');
    } else {
      $('.sfa-button > a').removeClass('grey-out');
      $('a.sfa-link.button')[0].href = '/authenticate_spotify_artists?artists=' + $(this).val();
    }
  });

  $('.sfa-button').on('click', function(e) {
    if ($('#spotify-artist-select').val() == "") {
      e.preventDefault();
      $('.sfa-button > a').addClass('grey-out');
    };
  });
})();
