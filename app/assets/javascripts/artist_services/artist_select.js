(function() {
  var $ = jQuery;

  $(".artist-for-service-select").on("change", function() {
    var selectionValue = $(this).val();
    var submissionButton = ".artist-service-submission-button";

    if(selectionValue == "") {
      $(submissionButton).addClass("grey-out");
      $(submissionButton).prop('disabled', true);
    } else {
      $(submissionButton).removeClass("grey-out");
      $(submissionButton).prop('disabled', false);
    }
  })

  $('.artist-service-submission-button').on('click', function(e) {
    if ($(".artist-for-service-select").val() == "") {
      e.preventDefault();
    };
  });
})();
