(function() {
  var $ = jQuery;

  $('#amfa-select-dropdown').on('change', function() {
    if ($(this).val() == "") {
      $('.amfa-button > a').addClass('grey-out');
    } else {
      $('.amfa-button > a').removeClass('grey-out');
      $('a.amfa-link.button')[0].href = "https://artists.apple.com/a/request/" + $(this).val();
    }
  });

  $('.amfa-button').on('click', function(e) {
    if ($('#amfa-select-dropdown').val() == "") {
      e.preventDefault();
      $('.amfa-button > a').addClass('grey-out');
    };
  });
})();
