$(function() {
  checklistListener();
});

function checklistListener() {
  $("#distribution-panel").on("update-checklist", function(e) {
    var songsValid       = e.originalEvent.detail.songsValid;
    var $songsStatus     = $(".songs-status");
    var $songsStatusIcon = $(".songs-status i");

    $songsStatus.toggleClass("status_complete", songsValid);
    $songsStatus.toggleClass("checked", songsValid);
    $songsStatusIcon.addClass(addChecklistClass(songsValid));
    $songsStatusIcon.removeClass(removeChecklistClass(songsValid));
  });
}

function addChecklistClass(shouldCheck) {
  return "fa-" + (shouldCheck ? "check-" : "") + "circle-o";
}

function removeChecklistClass(shouldCheck) {
  return "fa-" + (shouldCheck ? "" : "check-") + "circle-o";
}
