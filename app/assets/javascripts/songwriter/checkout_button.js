$(function() {
  checkoutButtonListener();
});

function checkoutButtonListener() {
  $("#distribution-panel").on("update-checkout-button", function(e) {
    var albumId = e.originalEvent.detail.albumId;
    var url     = "/albums/" + albumId + "/distribution_panel";

    $.get(url, function(response) {
      $("#distribution-panel").empty().append(response);
    });
  });
}
