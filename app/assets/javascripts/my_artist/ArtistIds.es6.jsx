class ArtistIds extends React.Component {
  componentDidMount() {
    $(".mappings-list").foundation();
  }

  render() {
    return <div className="card">
      <div className="card-divider">
        <div>{this.props.translations.current_mappings.toUpperCase()}</div>
      </div>
      <div className="card-section mappings-list">
        <ul className="accordion" data-accordion data-allow-all-closed="true">
          {
            this.props.mappings.map( mapping => (
              <Mapping mapping={mapping}
                       releases={this.props.releases}
                       uploadWithArtwork={this.props.uploadWithArtwork}
                       uploadWithoutArtwork={this.props.uploadWithoutArtwork}
                       updateArtistId={this.props.updateArtistId}
                       translations={this.props.translations}
                       images={this.props.images}
                       artistName={this.props.artistName}
                       serviceReleases={this.props[`${mapping.service_name}Releases`]}
                       key={mapping.service_name} />
            ))
          }
        </ul>
        <div className="more-stores-issue">
          <p className="text-center mapping-copy"><em>{this.props.translations.other_mapping_issues}</em></p>
          <p className="text-center mapping-copy"><CustomerCareLink translations={this.props.translations}/></p>
        </div>
      </div>
    </div>;
  }
}
