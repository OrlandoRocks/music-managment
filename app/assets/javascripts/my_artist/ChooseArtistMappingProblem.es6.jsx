class ChooseArtistMappingProblem extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
  }

  onNext(e) {
    e.preventDefault();

    if (e.target.dataset.state === "same") { this.props.isDuplicate(); }
    this.props.onNext("ChooseArtistMappingSolution");
  }

  render() {
    let caretRight = <span className="fa fa-caret-right"></span>
    return (
      <div>
        <p>{this.props.translations.wizard.something_wrong_with_release}</p>
        <p>{this.props.translations.wizard.tunecore_apple_together}</p>
        <p>{this.props.translations.wizard.indicate_issue_below}</p>
        <div><a href="#" onClick={this.onNext} data-state="same">{this.props.translations.wizard.same_name_issue} {caretRight}</a></div>
        <div><a href="#" onClick={this.onNext} data-state="diff">{this.props.translations.wizard.different_name_issue} {caretRight}</a></div>
      </div>
    );
  }
}