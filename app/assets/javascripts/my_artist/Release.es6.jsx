function Release(props) {
  let role = {
    primary_artist: props.translations.wizard.main_artist,
    featuring: props.translations.wizard.featured_artist
  }[props.release.creative_role];

  return <li className="release-container">
    <span>{props.release.name}</span>
    <span>{role}</span>
  </li>;
}
