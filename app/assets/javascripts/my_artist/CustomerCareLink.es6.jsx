function CustomerCareLink(props) {
  return (
    <a target="_blank" href={props.translations.contact_customer_care_link}>{props.translations.contact_customer_care}</a>
  )
}
