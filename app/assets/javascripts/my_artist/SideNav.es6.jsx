class SideNav extends React.Component {
  componentWillMount() {
    this.navChange = this.navChange.bind(this);
  }

  navChange(e) {
    e.preventDefault();
    if (this.props.currentLink !== e.target.innerText) {
      this.props.onChange(e.target.innerText)
    }
  }

  render() {
    return <ul className="vertical menu my-artist-side-nav">
      <li className={this.props.currentLink === this.props.translations.releases ? "is-active" : ""}>
        <a href="#" onClick={this.navChange}>{this.props.translations.releases}</a>
      </li>
      <li className={this.props.currentLink === this.props.translations.artist_ids ? "is-active" : ""}>
        <a href="#" onClick={this.navChange}>{this.props.translations.artist_ids}</a>
      </li>
      {/*<li className={this.props.currentLink === this.props.translations.images ? "is-active" : ""}>*/}
        {/*<a href="#" onClick={this.navChange}>{this.props.translations.images}</a>*/}
      {/*</li>*/}
    </ul>;
  }
}
