class SuccessMapping extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
    this.props.onSubmit();
  }

  onNext(e) {
    e.preventDefault();
    this.props.onClose();
  }

  render() {
    return <div>
      <div className="inner-content">
        <p>{this.props.isCreated ? this.props.translations.wizard.success_create : this.props.translations.wizard.success_update }</p>
        <p>{this.props.translations.wizard.allow_for_change_timeline}</p>
        <p>{this.props.translations.wizard.issues_with_more_stores} <CustomerCareLink translations={this.props.translations}/></p>
      </div>
      <div className="float-right clearfix">
        <a href="#" className="button" onClick={this.onNext}>{this.props.translations.wizard.close}</a>
      </div>
    </div>;
  }
}
