function MappingAccordionListItem(props) {
  if (props.shouldDropdown) {
    return <li className={`${props.mapping.state !== "processing" && "accordion-item"} mapping-accordion-item mapping-accordion-item-${props.mapping.state}`} data-accordion-item="true">
      {props.children}
    </li>;
  } else {
    return <li>
      {props.children}
    </li>;
  }
}
