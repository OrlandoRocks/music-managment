class ArtistWizardBody extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
    this.isDuplicate = this.isDuplicate.bind(this);
    this.onArtworkChange = this.onArtworkChange.bind(this);
    this.onIdentifierChange = this.onIdentifierChange.bind(this);
    this.state = {
      isCreated: false
    };
    this.progressComponents = {
      "ChooseArtistMappingSolution": ChooseArtistMappingSolution,
      "CreateArtistIDMapping": CreateArtistIDMapping,
      "UpdateArtistIDMapping": UpdateArtistIDMapping,
      "SuccessMapping": SuccessMapping
    };
  }

  onNext(nextComponent) {
    if (nextComponent === "CreateArtistIDMapping") {
      this.setState({
        isCreated: true
      });
    } else if (nextComponent === "UpdateArtistIDMapping") {
      this.setState({
        isCreated: false
      });
    }
    this.props.onNext(nextComponent);
  }

  isDuplicate() {
    this.props.isDuplicate();
  }

  onArtworkChange(image) {
    this.props.onArtworkChange(image);
  }

  onIdentifierChange(identifier) {
    this.props.onIdentifierChange(identifier);
  }

  render() {
    const ProgressComponent = this.progressComponents[this.props.currentComponent];
    return <ProgressComponent onNext={this.onNext}
                              onCancel={this.props.onCancel}
                              onClose={this.props.onClose}
                              isDuplicate={this.isDuplicate}
                              isCreated={this.state.isCreated}
                              onArtworkChange={this.onArtworkChange}
                              onIdentifierChange={this.onIdentifierChange}
                              onSubmit={this.props.onSubmit}
                              identifier={this.props.identifier}
                              translations={this.props.translations}
                              images={this.props.images}
                              releases={this.props.releases}
                              serviceReleases={this.props.serviceReleases}
                              serviceName={this.props.serviceName}
                              artistName={this.props.artistName}
                              artwork={this.props.artwork}/>
  }
}
