class ErrorMapping extends React.Component {
  componentWillMount() {
    this.onNext  = this.onNext.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  onNext(e) {
    e.preventDefault();
    this.props.onNext();
  }

  onClose(e) {
    e.preventDefault();
    this.props.onClose();
  }

  render() {
    const error =  this.props.mapping.error === "did_not_match" ? ` ${this.props.translations.wizard.bad_name_match}` : "";
    return <div>
      <div className="inner-content">
        <p className="red">{this.props.translations.wizard.redelivery_error}</p>
        <p>{this.props.translations.wizard.redelivery_error_explanation}{error}.</p>
        <p>{this.props.translations.wizard.redelivery_error_action}</p>
      </div>
      <div className="float-center">
        <a href="#" onClick={this.onNext} className="button warning">{this.props.translations.wizard.resubmit_url}</a>
        <div className="float-right"><a href="#" onClick={this.onClose} className="button">{this.props.translations.wizard.close}</a></div>
      </div>
    </div>;
  }
}
