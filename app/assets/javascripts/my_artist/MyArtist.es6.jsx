class MyArtist extends React.Component {
    componentWillMount() {
      this.state = {
          currentLink: this.props.translations.releases,
          mappings: this.props.mappings
      };

      this.sidebarChange        = this.sidebarChange.bind(this);
      this.uploadWithArtwork    = this.uploadWithArtwork.bind(this);
      this.uploadWithoutArtwork = this.uploadWithoutArtwork.bind(this);
      this.updateArtistId       = this.updateArtistId.bind(this);
    }

    sidebarChange(newLink) {
      this.setState({ currentLink: newLink });
    }

    uploadWithArtwork(formData) {
      let that = this;
      $.ajax({
          url: location.pathname + "/mapping",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success : function(data) {
            that.setState({mappings: data.mappings})
          }
      });
    }

    uploadWithoutArtwork(data) {
      let that = this;
      $.ajax({
        url: location.pathname + "/mapping",
        type: "POST",
        data: data,
        success: function(data) {
          that.setState({ mappings: data.mappings })
        }
      })
    }

    updateArtistId(artistParams) {
      let that = this;
      $.ajax({
        url: location.pathname + "/mapping",
        type: "POST",
        data: artistParams,
        success: function(data) {
          that.setState({ mappings: data.mappings })
        }
      })
    }

    getMain() {
      let main = {};
      main[this.props.translations.releases] = Releases;
      main[this.props.translations.artist_ids] = ArtistIds;
      main[this.props.translations.images] = Images;
      return main[this.state.currentLink];
    }

    render() {
      const Main = this.getMain();
      return <div className="grid-x grid-margin-x">
        <div className="cell medium-3">
          <SideNav currentLink={this.state.currentLink}
                   translations={this.props.translations}
                   onChange={this.sidebarChange} />
        </div>
        <div className="cell auto">
          <Main releases={this.props.releases}
                artistName={this.props.artist_name}
                mappings={this.state.mappings}
                translations={this.props.translations}
                images={this.props.images}
                uploadWithArtwork={this.uploadWithArtwork}
                uploadWithoutArtwork={this.uploadWithoutArtwork}
                appleReleases={this.props.apple_releases}
                updateArtistId={this.updateArtistId}/>
        </div>
      </div>;
    }
}
