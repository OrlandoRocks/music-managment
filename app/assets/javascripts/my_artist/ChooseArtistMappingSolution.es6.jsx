class ChooseArtistMappingSolution extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
  }

  onNext(e) {
    e.preventDefault();
    let nextComponent = e.target.dataset.state === "update" ? "UpdateArtistIDMapping" : "CreateArtistIDMapping";
    this.props.onNext(nextComponent);
  }

  render() {
    const caretRight = <i className="fa fa-caret-right"></i>;
    return <div className="inner-content">
      <p>{this.props.translations.wizard.first_find_apple_id}</p>
      <img src={this.props.images.getting_your_artist_id_demo}
           alt="Get Your Artist ID"
           width="50%"
           className="bottom-spacing"/>

      <div>
        <a href="#" onClick={this.onNext} data-state="update">
          {this.props.translations.wizard.i_have_apple_artist_url} {caretRight}
        </a>
      </div>

      <div>
        <a href="#" onClick={this.onNext} data-state="create">
          {this.props.translations.wizard.i_need_apple_artist_url} {caretRight}
        </a>
      </div>
    </div>;
  }
}
