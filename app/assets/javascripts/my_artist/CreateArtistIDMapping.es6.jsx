class CreateArtistIDMapping extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
    this.onArtworkChange = this.onArtworkChange.bind(this);
    this.createRequest = this.createRequest.bind(this);
    this.state = {};
  }

  onNext(e) {
    e.preventDefault();
    this.props.onNext("SuccessMapping");
  }

  createRequest(e) {

    this.onNext(e);
  }

  onArtworkChange(artwork) {
    this.props.onArtworkChange(artwork)
  }

  oldUploadImageView() {
    return <div>
      <p>{this.props.translations.wizard.getting_new_apple_id_easy}</p>
      <p>{this.props.translations.wizard.optionally_submit_new_image}
        { this.props.artwork ? "" : "The image must be at least 2400x2400, square, and in PNG format." }
      </p>

      <UploadFileImage artwork={this.props.artwork} onArtworkChange={this.onArtworkChange} translations={this.props.translations}/>

      <div className="float-right"><a href="#" onClick={this.onNext} className="button success">{this.props.translations.wizard.request_new_apple_artist}</a></div>
    </div>;
  }

  render() {
    const following_release_maps = this.props.translations.wizard.following_release_maps
      .replace("{{number_of_releases}}", this.props.serviceReleases.length)
      .replace("{{release_pluralization}}", this.props.serviceReleases.length === 1 ? "release" : "releases")
      .replace("{{artist_name}}", this.props.artistName);
    return <div>
      <div className="inner-content">
        <p>{following_release_maps}</p>
        <ul className="no-bullet">
          { this.props.serviceReleases.map( release => <li key={release.album_id}>{release.album_name}</li>) }
        </ul>
      </div>
      <div className="float-right clearfix">
        <a href="#" className="button warning" onClick={this.createRequest}>{this.props.translations.wizard.create_request}</a>
      </div>
    </div>
  }
}
