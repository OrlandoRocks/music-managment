function MappingAccordionAnchor(props) {
  let className = `accordion-title mapping-accordion-title mapping-accordion-title-${props.mapping.state}`;
  if (props.shouldDropdown) {
    return <a href="#" onClick={props.onClick} className={className}>
      {props.children}
    </a>;
  } else {
    return <div className={`${className} no-dropdown`}>
      {props.children}
    </div>
  }
}
