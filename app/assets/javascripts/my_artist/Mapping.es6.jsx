class Mapping extends React.Component {
  componentWillMount() {
    this.onClose                  = this.onClose.bind(this);
    this.onCancel                  = this.onCancel.bind(this);
    this.setAccordionContentRef    = this.setAccordionContentRef.bind(this);
    this.clickAccordionLink       = this.clickAccordionLink.bind(this);
    this.state = {
      isExpanded: false
    };

    this.storeDisplayName = {
      apple: "iTunes/Apple"
    }[this.props.mapping.service_name];
  }

  setAccordionContentRef(accordionContentRef) {
    this.accordionContentRef = accordionContentRef;
  }

  onClose() {
    let $accordionRef = $(".accordion");
    let $accordionContentRef = $(this.accordionContentRef);
    $accordionRef.foundation("toggle", $accordionContentRef);
    $accordionRef.foundation("toggle", $accordionContentRef);
    this.setState({ isExpanded: false });
  }

  onCancel() {
    let $accordionRef = $(".accordion");
    let $accordionContentRef = $(this.accordionContentRef);
    $accordionRef.foundation("toggle", $accordionContentRef);
    this.setState({ isExpanded: false });
  }

  clickAccordionLink(e) {
    let isExpanded = this.accordionContentRef && this.accordionContentRef.getAttribute("aria-hidden") === "false";
    if (this.isProcessing()) {
      e.preventDefault();
    }
    this.setState({ isExpanded });
  }

  stateDisplay() {
    let mappingState;
    if (this.isProcessing()) {
      mappingState = this.props.translations.wizard.processing;
      mappingState = mappingState.charAt(0).toUpperCase() + mappingState.slice(1);
      mappingState = `${mappingState} ${this.props.mapping.processed_at}`;
    } else if (!this.state.isExpanded) {
      mappingState = this.props.translations.wizard.manage;
    } else {
      mappingState = "";
    }

    return <div className={`mapping_state-${this.props.mapping.state}`}>{mappingState}</div>;
  }

  redeliveryErrorDisplay() {
    if (this.isError()) {
      return (
        <div>
          <div className="mapping_state-redelivery_error">
            <span className="fa fa-warning"></span>&nbsp;{this.props.translations.wizard.redelivery_error}
          </div>
          <div>
            <CustomerCareLink translations={this.props.translations}/>
          </div>
        </div>
      )
    }
  }

  isProcessing() {
    return this.props.mapping.state === "processing"
  }

  isError() {
    return this.props.mapping.state === "error"
  }

  render() {
    let hasStoreReleases = this.props.serviceReleases.length > 0;
    return <MappingAccordionListItem mapping={this.props.mapping}
                                      shouldDropdown={!this.isProcessing() && hasStoreReleases && !this.isError()}>
      <MappingAccordionAnchor shouldDropdown={(!this.isProcessing() && !this.isError() || this.state.isExpanded) && hasStoreReleases}
                              onClick={this.clickAccordionLink}
                              mapping={this.props.mapping}>
        <div className="release-container">
          <div className="store-name">{this.storeDisplayName}</div>
          { !this.state.isExpanded ? <div className="accordion-title-state-display">
            { this.redeliveryErrorDisplay() || this.stateDisplay()}
          </div> : "" }
        </div>
      </MappingAccordionAnchor>
      <div className="accordion-content mapping-accordion-content" data-tab-content ref={this.setAccordionContentRef}>
        <ArtistIdWizard mapping={this.props.mapping}
                        releases={this.props.releases}
                        artistName={this.props.artistName}
                        uploadWithArtwork={this.props.uploadWithArtwork}
                        uploadWithoutArtwork={this.props.uploadWithoutArtwork}
                        updateArtistId={this.props.updateArtistId}
                        storeDisplayName={this.storeDisplayName}
                        translations={this.props.translations}
                        images={this.props.images}
                        serviceReleases={this.props.serviceReleases}
                        onCancel={this.onCancel}
                        onClose={this.onClose}/>
      </div>
    </MappingAccordionListItem>;
  }
}


