class UploadFileImage extends React.Component {
  componentWillMount() {
    this.chooseFile = this.chooseFile.bind(this);
    this.setFileRef = this.setFileRef.bind(this);
    this.onArtworkChange = this.onArtworkChange.bind(this);
    this.removeArtwork = this.removeArtwork.bind(this);
    this.state = {};
  }

  chooseFile(e) {
    e.preventDefault();
    this.setState({
      error: undefined
    });
    this.fileRef.click();
  }

  onArtworkChange(e) {
    this.props.onArtworkChange(undefined);
    let file = e.target.files[0];
    this.isValidArtwork(file).then(() => {
      this.props.onArtworkChange(file)
    }).catch((msg) => {
      this.setState({error: msg})
    });
  }

  isValidArtwork(file) {
    return ImageValidator.validExtension(file)
      .then(() => ImageValidator.validSize(file))
      .then(() => ImageValidator.hasNoTransparency(file));
  }

  setFileRef(ref) {
    this.fileRef = ref;
  }

  removeArtwork(e) {
    e.preventDefault();
    this.props.onArtworkChange(undefined);
  }

  render() {
    let form = <form style={{height:0, overflow:"hidden"}}>
      <input name="file" type="file" ref={this.setFileRef} onChange={this.onArtworkChange} />
    </form>;

    let errorMsg = this.props.translations.wizard[this.state.error];
    if (this.props.artwork) {
      return <div className="clearfix small-centered">
        { errorMsg }
        {form}
        <div className="center-display">
          <img className="artist-mapping-image-edit" src={URL.createObjectURL(this.props.artwork)} />
        </div>

        <div className="center-display-buttons">
          <a href="#" onClick={this.removeArtwork}><span className="fa fa-close red"></span><span className="change-image-text">{this.props.translations.wizard.remove_image}</span></a>
          <a href="#" onClick={this.chooseFile}><span className="fa fa-refresh green"></span><span className="change-image-text">{this.props.translations.wizard.change_image}</span></a>
        </div>
      </div>;
    } else {
      return <div className="clearfix small-centered">
        {errorMsg}
        {form}
        <div className="center-display">
          <a href="#" onClick={this.chooseFile} className="button warning">{this.props.translations.wizard.upload_artist_image}</a>
        </div>
      </div>;
    }
  }
}
