function ArtistWizardHeader(props) {
  function getProgress() {
    return Math.floor(props.progress / props.totalSteps * 100);
  }

  let progress = getProgress();
  let coloredClass = progress === 100 ? "success" : "";
  return <div>
    <div className={`progress ${coloredClass}`} role="progressbar" tabIndex="0" aria-valuenow={progress} aria-valuemin="0" aria-valuetext="50 percent" aria-valuemax="100">
        <div className="progress-meter" style={{width: `${progress}%`}}></div>
    </div>
  </div>;
}
