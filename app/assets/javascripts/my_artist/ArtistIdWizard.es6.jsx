class ArtistIdWizard extends React.Component {
  componentWillMount() {
    this.onNext = this.onNext.bind(this);
    this.tryAgain = this.tryAgain.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onClose = this.onClose.bind(this);
    this.isDuplicate = this.isDuplicate.bind(this);
    this.onArtworkChange = this.onArtworkChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onIdentifierChange = this.onIdentifierChange.bind(this);
    this.initialState = {
      progress: 1,
      isDuplicate: false,
      error: this.props.mapping.state === "error",
      currentComponent: "ChooseArtistMappingSolution",
      artwork: undefined,
      identifier: undefined
    };
    this.state = this.initialState;
  }

  tryAgain() {
    this.setState({ error: false });
  }

  onNext(nextComponent) {
    this.setState({
        progress: this.state.progress + 1,
        currentComponent: nextComponent
    });
  }

  isDuplicate() {
    this.setState({ isDuplicate: true });
  }

  onCancel(e) {
    e && e.preventDefault();
    this.setState(this.initialState);
    this.props.onCancel();
  }

  onClose(e) {
    e && e.preventDefault();
    this.setState(this.initialState);
    this.props.onClose();
  }

  onArtworkChange(artwork) {
      this.setState({ artwork });
  }

  onIdentifierChange(identifier) {
      this.setState({ identifier });
  }

  onSubmit() {
    if (this.state.identifier) {
      this.updateArtistId();
    } else if (this.state.artwork) {
      this.uploadWithArtwork();
    } else {
      this.uploadWithoutArtwork();
    }
  }

  uploadWithArtwork() {
    let formData = new FormData();
    formData.append("artwork", this.state.artwork);
    formData.append("is_duplicate", this.state.isDuplicate);
    formData.append("service_name", this.props.mapping.service_name);
    this.props.uploadWithArtwork(formData)
  }

  uploadWithoutArtwork() {
    let data = {
      is_duplicate: this.state.isDuplicate,
      service_name: this.props.mapping.service_name
    };
    if (this.state.identifier && this.state.identifier !== "") {
      data.identifier = this.state.identifier
    }

    this.props.uploadWithoutArtwork(data)
  }

  updateArtistId() {
      this.props.updateArtistId({
        identifier: this.state.identifier,
        service_name: this.props.mapping.service_name
      });
  }

  render() {
    if (this.state.error) {
      return <ErrorMapping onNext={this.tryAgain}
                            onClose={this.onCancel}
                            translations={this.props.translations}
                            mapping={this.props.mapping}/>
    } else {
      return <div>
        <ArtistWizardHeader progress={this.state.progress}
                            translations={this.props.translations}
                            totalSteps={3}/>
        <ArtistWizardBody onNext={this.onNext}
                          onCancel={this.onCancel}
                          onClose={this.onClose}
                          isDuplicate={this.isDuplicate}
                          currentComponent={this.state.currentComponent}
                          onArtworkChange={this.onArtworkChange}
                          onIdentifierChange={this.onIdentifierChange}
                          onSubmit={this.onSubmit}
                          identifier={this.state.identifier}
                          translations={this.props.translations}
                          images={this.props.images}
                          releases={this.props.releases}
                          serviceReleases={this.props.serviceReleases}
                          serviceName={this.props.mapping.service_name}
                          artistName={this.props.artistName}
                          artwork={this.state.artwork}/>
        {
          this.state.currentComponent !== "SuccessMapping" &&
            <a href="#" className="button secondary" onClick={this.onCancel}>
              {this.props.translations.wizard.cancel}
            </a>
        }
      </div>;
    }
  }
}
