function Releases(props) {
  return <div className="card">
    <div className="card-divider release-container">
      <span>{props.translations.release_title.toUpperCase()}</span>
      <span>{props.translations.role.toUpperCase()}</span>
    </div>
    <div className="card-section">
      <ul className="release-list">
        { props.releases.map(release => (
          <Release release={release}
                   translations={props.translations}
                   key={release.id} />
          )
        )}
      </ul>
    </div>
  </div>;
}
