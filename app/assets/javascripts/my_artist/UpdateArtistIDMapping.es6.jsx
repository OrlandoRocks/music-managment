class UpdateArtistIDMapping extends React.Component {
  componentWillMount() {
    this.state = {
      validatorMessage: null,
      identifier: null
    };
    this.onNext = this.onNext.bind(this);
    this.onIdentifierChange = this.onIdentifierChange.bind(this);
    this.onIdentifierSubmit = this.onIdentifierSubmit.bind(this);
  }
  onNext(e) {
    e.preventDefault();
    if (this[this.props.serviceName + "ValidIdentifier"](this.state.identifier)) {
      this.props.onNext("SuccessMapping");
    } else{
      this.setState({error: true})
    }
  }

  onIdentifierChange(e) {
    let id = e.target.value;
    this.setState({identifier: id});
    this.props.onIdentifierChange(id);
  }

  onIdentifierSubmit(e) {
    this.onNext(e);
  }

  appleValidIdentifier(id) {
    // https://regexr.com/4f5nk | https://regexr.com/4f5ou
    // validates against https://itunes.apple.com/us/artist/madonna/id12345
    // OR https://music.apple.com/us/artist/ryan-moe/1168770775
    return /https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/id.*/.test(id) ||
      /https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/.*/.test(id)
  }

  render() {
    return <div>
      <div className="inner-content">
        <div className="bottom-spacing">{this.props.translations.wizard.update_release_mapping_paste}</div>
        <form onSubmit={this.onIdentifierSubmit} className="bottom-spacing">
          <input type="text" onChange={this.onIdentifierChange}/>
        </form>
        {this.state.error ?
          <p className="error-block">{this.props.translations.wizard.invalid_url_format}</p>
          : ""}
      </div>
      <div className="float-right">
        <a href="#" className="button success" onClick={this.onNext}>
          {this.props.translations.wizard.update_releases}
        </a>
      </div>
    </div>;
  }
}
