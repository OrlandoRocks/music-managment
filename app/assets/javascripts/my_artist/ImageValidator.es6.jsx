class ImageValidator {
  static validExtension(file) {
    return new Promise((resolve, reject) => {
      if (/(jpeg|jpg|png)$/.test(file.type)) {
        resolve();
      } else {
        reject("file_invalid_format");
      }
    });
  }

  static validSize(file) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.onload = function() {
        let minimumDimensions = 2400;
        let validDimensions = this.height >= minimumDimensions && this.width >= minimumDimensions;
        let isSquare = this.height === this.width;

        if (!validDimensions) {
          reject("file_invalid_dimensions")
        } else if (!isSquare) {
          reject("file_not_square");
        } else {
          resolve();
        }
      };
      img.src = URL.createObjectURL(file);
    });
  }

  static hasNoTransparency(file) {
    return new Promise((resolve, reject) => {
      if (!/png$/.test(file.type)) {
        resolve();
      } else {
        let img = new Image();
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d");
        img.onload = function() {
          canvas.width = this.width;
          canvas.height = this.height;
          ctx.drawImage(this,0,0, this.width, this.height);
          let data = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
          let hasAlphaPixels = false;

          for (let i = 3, n = data.length; i < n; i+=4) {
            if (data[i] < 255) {
              hasAlphaPixels = true;
              break;
            }
          }

          if (hasAlphaPixels) {
            reject("file_transparency");
          } else {
            resolve();
          }
        };
        img.src = URL.createObjectURL(file);
      }
    });
  }
}