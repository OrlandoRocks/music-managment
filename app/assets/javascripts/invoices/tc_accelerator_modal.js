$(document).ready(() => {
    $.magnificPopup.open({
        items: [{
          src: '#tc-accelerator-modal',
          type: 'inline'
        }]
    })

    $('.tc-accelerator-modal-button-row button').on('click', (e) => {
        const { opt } = $(e.target).data()
        optClick(e, $.magnificPopup.close, console.error, opt)
    })
})
