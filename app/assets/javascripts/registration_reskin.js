jQuery(document).ready(function ($) {
  const telInputs = document.querySelectorAll("input[type=tel]");
  telInputs.forEach((input) => {
    input.addEventListener("input", (event) => {
      event.target.value = event.target.value.replace(/[^0-9+]/g, '');
    });
  });
})
