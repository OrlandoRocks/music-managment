(function(){
  $ = jQuery;

  var newArtworkSuggestionPath = gon.new_artwork_suggestion_path;

  bindPreviewCloseClick();
  bindGenerateArtworkClick();
  getPreviewArtwork();

  function getPreviewArtwork() {
    $.ajax({
      type: "GET",
      url: newArtworkSuggestionPath,
    });
  }
  function bindGenerateArtworkClick() {
    $(document).on('click', '#generate-more-artwork', function (e) {
      $('.suggested_artwork').html("<img src=\"/images/spinner01.gif\" style=\"padding:140px 0 0 135px;\"/>");
    });
  };
  function bindPreviewCloseClick() {
    $(document).on('click', '.cancel-preview-selection', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
      $('#loading_preview').addClass("mfp-hide");
    });
  };
}());
