$(document).ready(function() {
  const toggleElem = document.querySelector("#tc-accelerator-check")
  const toggleParent = toggleElem.parentElement
  
  toggleElem.checked = gon?.current_user?.tc_accelerator?.opt || false

  const somethingWentWrongText = gon?.tc_accelerator?.something_went_wrong || "Something went wrong"
  let loading = false

  function toggleSwitch(optedIn) {
    loading = false
    toggleElem.checked = !!optedIn
  }

  function acceleratorAlert() {
    loading = false
    alert(somethingWentWrongText)
  }

  const handleClick = (e) => {
    e.preventDefault()
    if (loading) { return }

    loading = true
    opt = toggleElem.checked ? "out" : "in"
    optClick(e, toggleSwitch, acceleratorAlert, opt)
  }

  toggleParent.addEventListener('click', handleClick)
})
