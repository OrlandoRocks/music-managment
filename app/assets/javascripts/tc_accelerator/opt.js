function optClick(e, successCallback, failureCallback, opt) {
  e.preventDefault();
  const personId = gon?.current_user?.person?.id
  const requestType = "POST"

  if (!personId) { return failureCallback() }
  if (opt === "none") { return successCallback() }
  
  const url = opt === "stop-asking" 
    ? "/tc_accelerator/do_not_notify"
    : "/tc_accelerator/opt"

  $.ajax({
    url,
    type: requestType,
    data: {
      tc_accelerator:
        {
          person_id: personId,
          opt: opt
        }
    }
  }).done(function(response) {
    if(response.success) {
      successCallback(response.opted_in);
    } else {
      failureCallback()
    }
  })
}
