jQuery(document).ready(function($) {

  if ($('.fbtracks-subscription-modal').length > 0) {
    openSignUpModal();
  }

  $('.close-modal').on('click', function (e) {
    closeSignUpModal(e);
  });

  $('.fbtracks-modal-cancel').on('click', function(e){
    closeSignUpModal(e);
  });

  function openSignUpModal() {
    $.magnificPopup.open({
      inline: true,
      modal: true,
      preloader: false,
      items: {
        src: '.fbtracks-subscription-modal'
      }
    });
  }

  function closeSignUpModal(e) {
    e.preventDefault();
    $.magnificPopup.close();
    $('.fbtracks-subscription-modal').hide();
  }

});
