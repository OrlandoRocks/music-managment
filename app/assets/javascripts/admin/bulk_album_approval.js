$(document).on("ready", function() {
  if($(".admin-bulk_album_approvals__new").length == 1 || $(".admin-bulk_album_approvals__create").length == 1) {
    $('.select-all-checkboxes').on('click', function(e) {
      e.preventDefault();
      $('.selectable-checkbox').each(function() {
        var is_checked = this.checked;
        this.checked = !is_checked;
      });
    });
  };
});
