jQuery(document).ready(function(){
  new SelectAllButton('#select_all', '#eft-transaction-section');

  function findCheckedBoxesLength() {
    var $boxes = jQuery('#eft-transaction-section input:checked');
    if ($boxes.length === 0) {
      // disable button
      jQuery('#eft-submit-btn').prop('disabled', true);
      jQuery('#eft-submit-btn:hover').css('cursor', 'default');
    } else {
      jQuery('#eft-submit-btn').prop('disabled', false);
      jQuery('#eft-submit-btn:hover').css('cursor', 'pointer');
    }
  }

  // disables button when no items are selected
  findCheckedBoxesLength();
  jQuery(':checkbox').change(findCheckedBoxesLength);
});
