jQuery(document).on("ready", function() {
  var $findByRadios = $("[name='mass_monetization_block_form[find_by]']")
  var $findByDescriptions = $(".find_by_description")

  $findByRadios.on("click", function () {
    $findByDescriptions.prop("hidden", true)

    var radioValue = $(this).val()

    var respectiveDescription = $('.find_by_description_for_' + radioValue)

    $(respectiveDescription).prop("hidden", false)
  });
})
