$("#paypal-transfers-report-download-button").on('click', function(event) {
  event.preventDefault();
  disablePaypalTransfersReportDownloadBtn();
  const downloadSocket = create_socket_connection();
  const channelName    = 'DownloadChannel';

  downloadSocket.onopen = () => {
    subscribe_to_socket_channel(downloadSocket, channelName);
  }

  downloadSocket.onmessage = event => {
    const eventData = JSON.parse(event.data);
    if(eventData.type === "ping" || eventData.type === "welcome") return;

    switch(eventData.type) {
      case "confirm_subscription":
        requestPaypalTransfersReportData(downloadSocket, channelName);
        break;
      default:
        handlePaypalTransfersReportData(eventData, downloadSocket);
        break;
    }
  }
});

const disablePaypalTransfersReportDownloadBtn = () => {
  $("#paypal-transfers-report-download-button").text("Downloading...");
  $("#paypal-transfers-report-download-button").attr('disabled', 'disabled');
};

const enablePaypalTransfersReportDownloadBtn = () => {
  $("#paypal-transfers-report-download-button").text("Download report");
  $("#paypal-transfers-report-download-button").removeAttr('disabled');
}

const requestPaypalTransfersReportData = (socket, channelName) => {
  // There is a race condition issue where server responds 
  // with a subscribed message but subscription has not been created.
  // This timeout avoids that
  setTimeout(() => {
    invoke_cable_action(
      socket,
      channelName,
      "fetch_report",
      extractPaypalReportFormData($(".admin-paypal-transfers-filters")[0]))
  }, 100);
}

const extractPaypalReportFormData = (form_object) => {
  var form_data       = new FormData(form_object);
  var serialized_form = Object.fromEntries(form_data.entries());
  if (serialized_form.type === "paypal") delete serialized_form.status;
  if (serialized_form.type === "check") delete serialized_form.threshold_status;
  delete serialized_form.per_page;
  serialized_form.payout_provider = serialized_form.type;
  return serialized_form;
}

const handlePaypalTransfersReportData = (eventData, socket) => {
  const message = eventData.message;
  if(message && message.file) {
    const blob = new Blob([message.file.content], {
      type: `${message.file.content};charset=${message.file.encoding}`
    });

    saveAs(blob, message.file.name);
    socket.close();
  }
  enablePaypalTransfersReportDownloadBtn();
}
