$(document).on('ready', function() {

  $('.remove-feature').on('click', function(e) {
    e.preventDefault()
    var removable = $(this).closest("td").find("#removable_flag").val()
    if (removable === 'true') {
      var feature_name = $(this).closest("td").find("#feature_name").val()
      var country_iso_code = $(this).closest("td").find("#country_iso_code").val()
      openModal(feature_name, country_iso_code)
    } else
      alert("Please deactivate this feature flag by setting it to 0% or remove the user IDs before deleting")
  });

  function openModal(feature_name, country_iso_code) {
    $('#modal-content').modal('show');
    $('.simplemodal-data:visible:first').find("#feature_name").val(feature_name)
    $('.simplemodal-data:visible:first').find("#country_iso_code").val(country_iso_code)
  }

  $('.save-remove-feature').on('click', function(e) {
    $('#modal-content').modal('hide');
  });
});
