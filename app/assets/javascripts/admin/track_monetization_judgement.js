jQuery(document).on("ready", function() {
  var $approved = $("#comp-table tbody .approve-check");
  var $rejected = $("#comp-table tbody .reject-check");
  var $pending = $("#comp-table tbody .pending-check");
  var $checkboxWrapper = $('.checkbox-wrapper');
  var approved = $.makeArray($approved);
  var rejected = $.makeArray($rejected);
  var pending = $.makeArray($pending);
  var checkboxWrapper = $.makeArray($checkboxWrapper);
  var $field_change = $("#field_change");
  var $salepoint_song_row = $(".salepoint_song_row");
  var salepoint_song_row = $.makeArray($salepoint_song_row);

  $checkboxWrapper.each(function () {
    if ($(this).find(".reject-check").prop("checked")) {
      $(this).addClass("red");
    } else if ($(this).find(".approve-check").prop("checked")) {
      $(this).addClass("green");
    }
  });

  $approved.change(function () {
    var approvedIndex = approved.indexOf(this);
    var $this = $(this)
    var $thisCell = $this.parent().parent()
    var $thisRow = $thisCell.parent()

    if($(this).prop("checked")) {
      $(rejected[approvedIndex]).prop("checked", false);
      $(pending[approvedIndex]).prop("checked", false);
      $(checkboxWrapper[approvedIndex]).removeClass("red").addClass("green");
    } else {
      $(pending[approvedIndex]).prop("checked", true);
      $(checkboxWrapper[approvedIndex]).removeClass("green red");
    }
  });

  $rejected.change(function () {
    var rejectedIndex = rejected.indexOf(this);
    var $this = $(this)
    var $thisCell = $this.parent().parent()
    var $thisRow = $thisCell.parent()

    if($(this).prop("checked")) {
      $(approved[rejectedIndex]).prop("checked", false);
      $(pending[rejectedIndex]).prop("checked", false);
      $(checkboxWrapper[rejectedIndex]).removeClass("green").addClass("red");
    } else {
      $(pending[rejectedIndex]).prop("checked", true);
      $(checkboxWrapper[rejectedIndex]).removeClass("green red");
    }
  });

  $(".approve-all").on("click", function () {
    $checkboxWrapper.removeClass("red").addClass("green");

    $pending.prop("checked", false);
    $approved.prop("checked", true);
    $rejected.prop("checked", false);
  });

  $(".reject-all").on("click", function () {
    $checkboxWrapper.addClass("red").removeClass("green");

    $pending.prop("checked", false);
    $approved.prop("checked", false);
    $rejected.prop("checked", true);
  });

  $(".clear-all").on("click", function () {
    $checkboxWrapper.removeClass("red green");

    $approved.each(function () {
      $(this).prop("checked", false);
    });

    $rejected.each(function () {
      $(this).prop("checked", false);
    });

    $pending.each(function () {
      $(this).prop("checked", true);
    });
  });

  $("#per_page, #valid_state, #query").change(function () {
    $field_change.val("true");
  });

  $("#salepoint-songs-search").on('input', function () {
    $field_change.val("true");
  });

  $(".successful-update").delay(2500).fadeOut('normal');

  $(".play-link").on("click", function (e) {
    e.preventDefault();

    var $this = $(this)
    var playAudio = $this.hasClass("fa-play")

    $("audio").remove()
    $(".yellow").removeClass("yellow")
    $(".fa-pause").toggleClass("fa-pause fa-play")

    if ( playAudio ) {
      $this.parent().parent().addClass("yellow")
      $this.toggleClass("fa-play fa-spinner fa-spin")
      $.ajax({
        url: $this.attr("href"),
        success: function (data) {
          $td = $this.parent()
          $audio = $("<audio src=\"" + data.song_url + "\"/>")
          $td.append($audio)
          $audio[0].play()
          setTimeout(function () { $this.toggleClass("fa-pause fa-spinner fa-spin") }, 5000)
        }
      })
    }
  });
});
