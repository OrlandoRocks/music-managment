$(document).on('ready', function() {
  $('.remove-user-icon').on('click', function() {
    var person_id = this.dataset.personId;
    $('#person_id').val(person_id);
  });

  $('.remove-cohort-icon').on('click', function() {
    var cohort_id = this.dataset.cohortId;
    $('#cohort_id').val(cohort_id)
  });
});
