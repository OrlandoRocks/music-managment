var $ = jQuery;

const extract_report_form_data = (form_object) => {
  let form_data       = new FormData(form_object);
  let serialized_form = Object.fromEntries(form_data.entries());
  ['pagination_limit', 'page'].forEach(e => delete serialized_form[e]);
  serialized_form["payout_provider"] = "payoneer";
  return serialized_form;
}

const request_report_data = (socket, channel_name) => {
  // There is a race condition issue where server responds 
  // with a subscribed message but subscription has not been created.
  // This timeout avoids that
  setTimeout(() => {
    invoke_cable_action(
      socket,
      channel_name,
      "fetch_report",
      extract_report_form_data($(".admin-payout-transfers-filters")[0]))
  }, 100);
}

const disable_download_report_btn = () => {
  $("#download-payoneer-payouts-report").text("Downloading...");
  $("#download-payoneer-payouts-report").attr('disabled', 'disabled');
}

const enable_download_report_btn = () => {
  $("#download-payoneer-payouts-report").text("Download report");
  $("#download-payoneer-payouts-report").removeAttr('disabled');
}

const handle_report_data = (event_data, socket) => {
  const message = event_data.message;
  if(message && message.file) {
    const blob = new Blob([message.file.content], {
      type: `${message.file.content};charset=${message.file.encoding}`
    });

    saveAs(blob, message.file.name);
    socket.close();
  }
  enable_download_report_btn();
}

$("#download-payoneer-payouts-report").on('click', (event) => {
  event.preventDefault();
  disable_download_report_btn();
  const download_socket = create_socket_connection();
  const channel_name    = 'DownloadChannel';

  download_socket.onopen = () => {
    subscribe_to_socket_channel(download_socket, channel_name);
  }

  download_socket.onmessage = event => {
    const event_data = JSON.parse(event.data);
    if(event_data.type === "ping" || event_data.type === "welcome") return;

    switch(event_data.type) {
      case "confirm_subscription":
        request_report_data(download_socket, channel_name);
        break;
      default:
        handle_report_data(event_data, download_socket);
        break;
    }
  }
});
