jQuery(document).ready(function($) {
  $('.admin-publishing-composer-edit').on('change', '.publishing-composer-pro-id', function() {
    var eligibleProIds = gon.eligble_publishing_pro_ids;
    var publishingComposerProId = $(this).val()
    var eligiblePro = eligibleProIds.includes(parseInt(publishingComposerProId))

    var $publisherInputs = $('.admin-publishing-composer-edit .publisher-info')
    var $publisherName = $publisherInputs.find('.publisher-name')
    var $publisherCae = $publisherInputs.find('.publisher-cae')

    $publisherName.prop('disabled', !eligiblePro)
    $publisherCae.prop('disabled', !eligiblePro)

    if (!eligiblePro) {
      $publisherName.val('')
      $publisherCae.val('')
    }
  })

  $(".publishing-composer-dob").change(function() {
    input = $(this)
    dobValidate(input)
  });

  function dobValidate(input) {
    if (isValidDate(input.val()))
    {
      input.parent().find('span').remove();
      return true
    }
    else if (input.parent().find('span').length == 0)
    {
      input.parent().append("<span class='input_error'>Date of Birth Invalid. Correct Format (YYYY/MM/DD)</span>");
      return false
    }
  }

  function isValidDate(dateString) {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regEx)) return false;
    var d = new Date(dateString);
    var dNum = d.getTime();
    if(d > new Date()) return false;
    if(!dNum && dNum !== 0) return false;
    return d.toISOString().slice(0,10) === dateString;
  }
});
