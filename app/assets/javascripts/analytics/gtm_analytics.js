function pushToDataLayer(opts) {
  dataLayer.push(opts);
}

function gtmLogEvent(eventName, eventAction, eventCategory, options) {
  if ($.isEmptyObject(options)) {
    options = {}
  }

  pushToDataLayer({
    event: eventName,
    eventCategory: eventCategory,
    eventAction: eventAction,
    eventLabel: options.label ? options.label : '',
    eventValue: options.value ? options.value : ''
  });
}
