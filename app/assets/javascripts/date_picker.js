//
//  Date Picker adds a jQuery date selector to a form field
//
//  sets up default options for our application
//
//  usage:  new DatePicker(".some-selector", { OPTIONS });
//
var DatePicker = function(selector,options){

  var $ = jQuery;

  var default_options = {
    showOn: 'both',
    dateFormat: gon.long_date_fmt,
    showAnim: 'fold',
    buttonImage: '/version/tunecore_neo/images/inputs/datepicker.gif',
    buttonImageOnly: true
  };

  var self = {
    init: function(){
      var opts = $.extend(default_options,options);
      if (!!$.prototype.datepicker) {
        $(selector).datepicker(opts);
      }
    }
  };

  self.init();
  return self;
};

jQuery(document).ready(function(){
  new DatePicker('.date-field');
});
