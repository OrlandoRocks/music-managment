$(document).ready(function(){
  $(".auto-complete.country-input.ui-autocomplete-input").blur(function(){
    if ( $(this).val() == "Japan" ) {
      $.magnificPopup.open({
        items: {
          src: "#japan-redirect",
          type: "inline"
        }
      });
    }
  });

  $("#japan-redirect").on("click", ".not-japanese-link, .mfp-close", function(e){
    e.preventDefault();
    $("#person_country option:selected").removeAttr("selected");
    $(".auto-complete.country-input.ui-autocomplete-input").val(null);
    $.magnificPopup.close({
      items: {
        src: "#japan-redirect",
        type: "inline"
      }
    });
  });
});

$(function () {
  try {
    SignupErrorHandler.addPasswordFormatValidation();
    SignupErrorHandler.addFormValidations();
    SignupErrorHandler.addEmailEventValidations();
    SignupErrorHandler.checkInputTouches();
  } catch (error) {
    console.error("Error w adding validations", error);
  }
});

var SignupErrorHandler = {
  formId: "#new_person",
  termsAndConditionsId: "#person_accepted_terms_and_conditions",
  errorClass: "form-field-error",
  termsAndConditionsClass: "terms-and-conditions",
  inputErrorHighlightClass: "fieldWithErrors",
  inputNames: {
    name: "person[name]",
    country: "person[country]",
    email: "person[email]",
    password: "person[password]",
    passwordConfirmation: "person[password_confirmation]",
  },
  touchedInputs: {},

  getTranslations: function () {
    try {
      if (gon && gon.translations) {
        return {
          ...gon.translations,
        };
      }
    } catch (error) {
      console.error("Error w getting translations");
    }
    return {};
  },

  // translate
  t: function (key = "") {
    const translations = this.getTranslations();
    if (typeof translations !== "object") return key;
    return translations[key] || key;
  },

  validateTermsAndConditions: function () {
    const checkbox = $(this.termsAndConditionsId);
    const isChecked = checkbox.prop("checked");
    if (isChecked) {
      this.toggleTermsError(checkbox, false);
    } else {
      this.toggleTermsError(checkbox, true, this.t("tc_not_accepted"));
    }
    return isChecked;
  },

  toggleTermsError: function (checkbox, bool = false, message) {
    const errorParent = $(checkbox).parent().parent();
    const existingErrorDiv = $(errorParent).find(`.${this.errorClass}`);

    if (bool) {
      if (existingErrorDiv.length) {
        this.showTermsError(existingErrorDiv, message);
      } else {
        this.addTermsError(errorParent, message);
      }
    } else {
      this.hideTermsError(errorParent);
    }
  },

  addTermsError: function (errorParent, message) {
    const newErrorDiv = document.createElement("div");
    newErrorDiv.className = `${this.errorClass} ${this.termsAndConditionsClass}`;
    newErrorDiv.innerHTML = message || "";
    $(errorParent).append(newErrorDiv);
  },

  showTermsError: function (errorDiv, message) {
    if (message) {
      errorDiv.innerHTML = message;
    }
    $(errorDiv).show();
  },

  hideTermsError: function (errorParent) {
    $(errorParent).find(`.${this.errorClass}`).hide();
  },

  highlightInputError: function (element) {
    $(element).parent().addClass(this.inputErrorHighlightClass);
  },

  unhighlightInputError: function (element) {
    $(element).parent().removeClass(this.inputErrorHighlightClass);
  },

  getValidationRules: function () {
    try {
      return {
        [this.inputNames.name]: "required",
        [this.inputNames.country]: "required",
        [this.inputNames.password]: { required: true, pwcheck: true },
        [this.inputNames.email]: {
          required: true,
          email: true,
        },
        [this.inputNames.passwordConfirmation]: {
          required: true,
          equalTo: `input[name='${this.inputNames.password}']`,
        },
      };
    } catch (error) {
      return {};
    }
  },

  getValidationErrorMessages: function () {
    try {
      return {
        [this.inputNames.name]: `<strong>${this.t("name")}</strong> ${this.t(
          "blank"
        )}`,
        [this.inputNames.country]: `<strong>${this.t(
          "country_display"
        )}</strong> ${this.t("blank")}`,
        [this.inputNames.password]: {
          required: `<strong>${this.t("password")}</strong> ${this.t(
            "blank"
          )}`,
          pwcheck: `<strong>${this.t("password")}</strong> ${this.t(
            "password_requirements"
          )}`,
        },
        [this.inputNames.email]: {
          required: `<strong>${this.t("email")}</strong> ${this.t("blank")}`,
          email: `<strong>${this.t("email")}</strong> ${this.t("invalid")}`,
        },
        [this.inputNames.passwordConfirmation]: {
          required: `<strong>${this.t("confirm_password")}</strong> ${this.t(
            "blank"
          )}`,
          pwcheck: `<strong>${this.t("confirm_password")}</strong> ${this.t(
            "password_requirements"
          )}`,
          equalTo: `<strong>${this.t("confirm_password")}</strong> ${this.t(
            "passwords_do_not_match"
          )}`,
        },
      };
    } catch (error) {
      return {};
    }
  },

  addPasswordFormatValidation: function () {
    $.validator.addMethod("pwcheck", function (value, element) {
      const pattern = $(element).attr("pattern");
      if (!pattern) return true;

      try {
        const regexp = new RegExp(pattern);
        return regexp.test(value);
      } catch (error) {
        return true;
      }
    });
  },

  validateElement: function (element) {
    const elementName = $(element).attr("name");
    if (this.touchedInputs[elementName]) {
      $(this.formId).validate().element(element);
    }
  },

  validateEmailInputs: function () {
    const email = $(`input[name='${this.inputNames.email}`);
    this.validateElement(email);
  },

  validatePasswordInputs: function () {
    const password = $(`input[name='${this.inputNames.password}`);
    const passwordConfirmation = $(
      `input[name='${this.inputNames.passwordConfirmation}`
    );
    this.validateElement(password);
    this.validateElement(passwordConfirmation);
  },

  addEmailEventValidations: function () {
    $(`input[name='${this.inputNames.email}`).on("focusout", function () {
      SignupErrorHandler.validateEmailInputs();
    });
  },

  onFocusOutHandler: function (element) {
    const elementName = $(element).attr("name");
    const names = SignupErrorHandler.inputNames;

    if ([names.password, names.passwordConfirmation].includes(elementName)) {
      SignupErrorHandler.validatePasswordInputs();
    }
  },

  checkInputTouches: function () {
    $(this.formId)
      .find("input,select:not(:hidden)")
      .on("focusin", function () {
        SignupErrorHandler.touchedInputs[this.name] = true;
      });
  },

  onFormSubmit: function (form) {
    let tcAgreed = true;
    try {
      tcAgreed = SignupErrorHandler.validateTermsAndConditions();
    } catch (error) {
      console.error("Error w t&c validation");
    }

    if (tcAgreed) {
      form.submit();
    }
  },

  onFormInvalid: function () {
    try {
      SignupErrorHandler.validateTermsAndConditions();
    } catch (error) {
      console.error(error);
    }
  },

  addFormValidations: function () {
    $(this.formId).validate({
      errorClass: this.errorClass,
      errorElement: "div",
      highlight: this.highlightInputError,
      unhighlight: this.unhighlightInputError,
      rules: this.getValidationRules(),
      messages: this.getValidationErrorMessages(),
      submitHandler: this.onFormSubmit,
      invalidHandler: this.onFormInvalid,
      onfocusout: this.onFocusOutHandler,
      onkeyup: false,
    });
  },
};
