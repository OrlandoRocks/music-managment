var $redirectModal = $('#domain-redirect-modal');

$.magnificPopup.open({
  items: { src: $redirectModal, type: 'inline' }
});

$('#close-modal').click(function() {
  $.magnificPopup.close();
});
