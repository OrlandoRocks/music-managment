const INDIA_COUNTRY_CODE = "IN";
const INDIA_DEFAULT_PREFIX = "+91";

document.addEventListener("DOMContentLoaded", () => {
  const countrySelect = document.querySelector(
    "[data-testid=signup-country-select]"
  );
  const phoneInputWrapper = document.querySelector(
    "[data-testid=signup-phone-input-wrapper]"
  );
  const prefixInput = document.querySelector("#person_mobile_number_prefix");

  if (countrySelect.value === INDIA_COUNTRY_CODE) {
    handleIndiaSelection(phoneInputWrapper, prefixInput);
  }

  countrySelect.addEventListener("change", (event) => {
    const selectedCountry = event.target.value;

    if (selectedCountry === INDIA_COUNTRY_CODE) {
      handleIndiaSelection(phoneInputWrapper, prefixInput);
    } else {
      handleIndiaDeselection(phoneInputWrapper, prefixInput);
    }
  });
});

function handleIndiaDeselection(phoneInputWrapper, prefixInput) {
  phoneInputWrapper.classList.add("hidden");
  prefixInput.value = '';
}

function handleIndiaSelection(phoneInputWrapper, prefixInput) {
  phoneInputWrapper.classList.remove("hidden");
  prefixInput.value = INDIA_DEFAULT_PREFIX
}