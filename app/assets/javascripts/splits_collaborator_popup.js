var $ = jQuery;

$(document).ready(function() {
  $('#splits_collab_delete_button').on('click', function(e) {
    var button = $(e.target);
    var purchaseId = button.data('purchase-id');
    e.preventDefault();

    $.ajax({
      url: "/cart/splits_collaborator_popup",
      type: "get",
      data: { purchase_id: purchaseId }
    }).done(function(response) {
      $.magnificPopup.open({
        items: {src: response.popup_template, type: 'inline'},
        callbacks: {
          open: function() {
            $("#delete_splits_collaborator").click(function(e) {
              e.preventDefault();
              e.stopPropagation()

              var button = $(e.target);
              button.closest('form').submit();
            });
            $(".collaborator-dialog-close").click(function(e) {
              e.preventDefault();
              $.magnificPopup.close();
            });
          },
        }
      });
    });
  });
});
