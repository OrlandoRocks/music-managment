//
//  StoreSelection:
//
//  Controls hiding and showing of rights assignment and variable pricing
//  on the store selection page.
//
jQuery(document).ready(function($) {

    var $itunesCheckbox = $("#album_store_selection_36");
    var $appleMusicCheckbox = $("#apple-music-opt-in");

    var $youtubeMusicCheckbox = $("#album_store_selection_28");
    var $ytsrProxyCheckbox = $("#album_store_selection_105");

    var StoreSelection = function () {
        var $ = jQuery,
            store_checkbox = '.selection-control input:checkbox',
            price_radio = ':input:radio',
            store_group = '.store-selection',
            accordion_header = ".accordion-header",
            accordion_body = ".accordion-body",
            deliver_check_box = ".deliver",
            variable_price_select = ".variable-price-select",
            variable_price_label = ".price-code",
            salepoint_check_box = ".salepoint-checkbox";

        //cache selection
        var $store_icons = $('.store-icon'),
            // $pricing_tables = $('.pricing-table'),
            $deliver_all = $('#deliver_all'),
            $openEditor = $('.openEditor'),
            $closeEditor = $('.closeEditor');

        var self = {

            init: function () {
                // Disable amazon radios if finalized and something is selected
                if ($('#store-group-amazon-mp3 input[type="radio"]:checked').length > 0 && AlbumFinalized) $('#store-group-amazon-mp3 input[type="radio"]').prop('disabled', false);

                // Swap pricing table by default for amazon
                self.swap_pricing_tables.call($('#store-group-amazon-mp3 input[type="radio"]:checked'));

                // Default AOD regardless of if checked or not
                self.swap_pricing_tables.call($('#store-group-amazon-on-demand input[type="radio"]:checked'));

                // setup event handlers
                self.check_for_selected_table();
                self.position_icons();
                self.init_accordion();

                $(price_radio).click(self.swap_pricing_tables);
                $(salepoint_check_box).click(self.salepoint_clicked);

                $deliver_all.click(function (e) {
                    var ch = $('.store-selection').not('#store-group-amazon-on-demand').find('.deliver');

                    var is_checked = $(this).prop('checked');

                    if (is_checked) {
                        let unchecked = ch.not(':checked')
                        unchecked.click().each(function () {
                            self.deliver_clicked($(this), is_checked)
                        });
                    } else {
                        ch.filter(':checked').click().each(function () {
                            self.deliver_clicked($(this), is_checked)
                        });
                    }

                    $appleMusicCheckbox.prop("checked", is_checked);
                });

                $openEditor.click(function (e) {
                    var p = $(this).parents('.editor');
                    p.find('.editorSummary').hide();
                    p.find('.editorContent').show();
                    return false;
                });

                $closeEditor.click(function (e) {
                    var p = $(this).parents('.editor');
                    p.find('.editorSummary').show();
                    p.find('.editorContent').hide();
                    return false;
                });

                $appleMusicCheckbox.prop("disabled", self.childCheckboxDisabled($appleMusicCheckbox, $itunesCheckbox));
                $ytsrProxyCheckbox.prop("disabled", self.childCheckboxDisabled($ytsrProxyCheckbox, $youtubeMusicCheckbox));
            },

            // the child itself is disabled (purchased), or the parent is unchecked
            childCheckboxDisabled: function($child, $parent) {
                return $child.is(":disabled") || !$parent.prop("checked")
            },
          
            init_accordion: function () {
                $(store_group).simple_accordion({
                    toggle: deliver_check_box,
                    body: accordion_body,
                    header: accordion_header,
                    after_toggle: self.deliver_clicked
                });
            },

            //
            //  Event Handlers
            //

            syncDeliverAll: function () {
                const notChecked = $('.store-selection')
                    .not('#store-group-amazon-on-demand')
                    .not('#store-group-pandora')
                    .find('.deliver')
                    .not(':checked')

                return notChecked.length ?
                   $('#deliver_all').prop('checked', false) :
                   $('#deliver_all').prop('checked', 'checked');
            },

            deliver_clicked: function (el, is_checked = false) {
                // el is the checkbox
                self.syncDeliverAll();
                is_checked && $ytsrProxyCheckbox.prop("checked", is_checked);
                if (el.is(":checked")) {
                    self.populate_salepoint_checkboxes(el);
                } else {
                    self.unpopulate_salepoint_checkboxes(el);
                }

                // iTunes Regional Check
                if (!AlbumFinalized && $('#store-group-itunes .deliver').is(':checked')) {
                    $('.itunes_regional').remove();
                }
            },

            salepoint_clicked: function () {
                var el = $(this);
                var store_group_wrapper = el.closest(store_group);
                if (store_group_wrapper.find(salepoint_check_box + ":checked").length == 0) {
                    self.close_store_group(store_group_wrapper);
                }
            },

            close_store_group: function (store_group_wrapper) {
                store_group_wrapper.find(accordion_body).addClass("hide");
                store_group_wrapper.find(deliver_check_box).prop("checked", false);
            },

            //  Autopopulation of Select Boxes
            populate_salepoint_checkboxes: function (el) {
                var checkboxes = self.find_checkboxes_inside_store_group(el);
                checkboxes.prop("checked", true);
            },

            //  Clearing of Select Boxes when unselecting Deliver Here
            unpopulate_salepoint_checkboxes: function (el) {
                var checkboxes = self.find_checkboxes_inside_store_group(el);
                checkboxes.prop("checked", false);
            },

            //place the store icon inbetween the checkbox and the label
            position_icons: function () {
                $store_icons.each(function () {
                    var icon = $(this);
                    $(this).parent().find('label').before(icon);
                });
            },


            //
            //  Utilities
            //
            find_checkboxes_inside_store_group: function (el) {
                var store_group_wrapper = el.closest(store_group);
                var checkboxes = store_group_wrapper.find(salepoint_check_box);
                return checkboxes;
            },


            //
            //  Showing Pricing Tables
            //
            check_for_selected_table: function () {
                //check to see if a price option has previously been set
                if ($(':radio:checked').length !== 0) {
                    selected_price = $.trim($('input[type=radio]:checked').next().html().toLowerCase());
                    self.show_selected_table();
                } else {
                    return false;
                }
                ;
            },

            show_selected_table: function () {
                $('#' + selected_price + '_table').removeClass('hide');
            },

            swap_pricing_tables: function () {
                var $this = $(this),
                    $pricing_table = $this.parents('.store-selection').find('.pricing-table'),
                    $store_price = $this.parents('.store-selection').find('.store-price'),
                    $sale_price = $this.parents('.store-selection').find('.sale-price'),
                    price_display = $this.attr('price_display'),
                    price = $.trim($this.next().html());
                if (price) price = price.toLowerCase();
                $pricing_table.addClass('hide').filter('#' + price + '_table').removeClass('hide');
                $store_price.html(price.split(' ')[0]);
                if (price_display || !1) $sale_price.html(price_display.split(' ')[4]);
            }
        };

        return self.init();
    };


    $itunesCheckbox.on("change", function () {
        $appleMusicCheckbox.prop("disabled", !$itunesCheckbox.prop("checked"));
        $appleMusicCheckbox.prop("checked", $itunesCheckbox.prop("checked"));
    })

    $youtubeMusicCheckbox.on("change", function () {
        $ytsrProxyCheckbox.prop("disabled", !$youtubeMusicCheckbox.prop("checked"));
        $ytsrProxyCheckbox.prop("checked", $youtubeMusicCheckbox.prop("checked"));
    })

    new StoreSelection();
})
