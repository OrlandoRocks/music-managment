var $ = jQuery;

function saveSelfBillingInfo(event) {
  if (event) {
    event.preventDefault();
  }

  var selfBillingChecked = $("#person_self_billing_status_attributes_status")[0]
    .checked;

  if (!selfBillingChecked) {
    const formErrorElement = $(
      ".self-billing-component #self-billing-required-error"
    );

    formErrorElement.css({
      display: "inline-block",
    });

    if (formErrorElement.hasClass("hide")) {
      formErrorElement.removeClass("hide");
    }

    $("#self-billing-customer-care-error").hide();
  } else {
    submitSelfBillingForm();
  }
}

function submitSelfBillingForm(event) {
  if (event) {
    event.preventDefault();
  }

  $(".self-billing-component form").trigger("submit");
  closeModal();
}

function onCheckChange() {
  var selfBilling = $("#person_self_billing_status_attributes_status")[0];

  if (selfBilling.checked) {
    if (typeof setSelfBillingAccepted === "function") {
      setSelfBillingAccepted(true);
    }

    $("#modal-bind")
      .magnificPopup({
        items: {
          src: "#self-billing-positive-modal",
        },
        closeOnContentClick: false,
        closeOnBgClick: false,
        showCloseBtn: true,
        enableEscapeKey: false,
        midClick: true,
      })
      .magnificPopup("open");

    $("#self-billing-positive-modal").on("click", ".mfp-close", function () {
      $("#self-billing-required-error").hide();
      $("#self-billing-customer-care-error").show();
      $("#person_self_billing_status_attributes_status").prop("checked", false);

      if (typeof setSelfBillingAccepted === "function") {
        setSelfBillingAccepted(false);
      }
    });
  }
}

function closeModal(event) {
  if (event) {
    event.preventDefault();
  }
  $.magnificPopup.close();
}

function acceptSelfBilling(event) {
  // if confirm and pay, then ajax call
  var id = $("#person_self_billing_status_attributes_id").val();
  var ip_address = $("#person_self_billing_status_attributes_ip_address").val();
  var version = $("#person_self_billing_status_attributes_version").val();
  var status = $("#person_self_billing_status_attributes_status").val();
  var person_id = $("#person_id").val();
  var partial = $("#person_account_settings_partial").val();

  if (window.location.href.match("cart/confirm_and_pay")) {
    $.ajax({
      url: `/people/${person_id}/update_self_billing_status.json`,
      type: "patch",
      data: {
        person: {
          self_billing_status_attributes: { id, ip_address, version, status },
          account_settings_partial: partial,
        },
      },
      success: function () {
        $("#self-billing-required-error").hide();
        $("#self-billing-customer-care-error").hide();
        $("#person_self_billing_status_attributes_status").attr(
          "disabled",
          true
        );
        $(".button.pay_button").attr("disabled", false);
        if (typeof setSelfBillingAccepted === "function") {
          setSelfBillingAccepted(true);
        }
      },
      error: function () {
        $("#person_self_billing_status_attributes_status").prop(
          "checked",
          false
        );
        $("#self-billing-required-error")
          .show()
          .children("span")
          .text(gon.self_billing_ajax_error);

        if (typeof setSelfBillingAccepted === "function") {
          setSelfBillingAccepted(false);
        }
      },
    });
  }

  $("#self-billing-required-error").hide();
  $("#self-billing-customer-care-error").hide();

  closeModal(event);
}

$(".button.pay_button").on("click", function (e) {
  var selfBillingRow = $("#self-billing-row")
  var selfBilling = $("#person_self_billing_status_attributes_status")[0];

  if (selfBillingRow.is(":visible") && !selfBilling.checked) {
    e.preventDefault();
    $("#self-billing-customer-care-error").hide();
    $("#self-billing-required-error")
      .show()
      .children("span")
      .text(gon.self_billing_required_error)
      .show();

    selfBilling.focus();
  } else {
    $("#self-billing-required-error").hide();
  }
});
$(() => {
  $("#update-renewal-billing-info-form").on("submit", (event) => {
    if (!handleConfirmPageFormSubmit(event)) {
      event.preventDefault();
      event.stopPropagation();
      console.log("handleConfirmPageFormSubmit -- FAILED")
      return false;
    }
    return true;
  })
})