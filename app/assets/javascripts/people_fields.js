var $ = jQuery;

function executeInputListeners() {
  var countrySelect = $('.country-input');
  var mobileNumberFieldset = $('#person_mobile_number_list_item');
  var mobileNumberInput = $('#person_mobile_number');
  var phoneNumberInput = $('#person_phone_number');
  var prefixInput = $('#person_prefix');
  var selectedCountry = $('#person_country').val();
  var inputsAllowingPlus = [prefixInput]
  var isAccountSettings = mobileNumberFieldset.data('isAccountSettings');
  var isCheckoutPage = mobileNumberFieldset.data('isCheckoutPage');

  var countryWebsite = mobileNumberFieldset.data('countryWebsite');
  var signup = mobileNumberFieldset.data('signup');
  var currentMobileNumber = mobileNumberFieldset.data('currentMobileNumber');
  var phoneRequiredIsos = mobileNumberFieldset.data('phoneRequiredIsos');
  var selectedCountry = $('#person_country').val();
  var showFieldset = (phoneRequiredIsos || []).includes(selectedCountry);

  var internationalFormatNeeded = (phoneRequiredIsos || []).includes(countryWebsite);

  function prependPlusAndCountry(input) {
    var defaultCountryCode = input.data('countryCode') ? input.data('countryCode') : '';

    if (input.val() === "") {
      input.val("+" + defaultCountryCode);
    } else if (!input.val().startsWith("+")) {
      input.val("+" + input.val());
    }
  }

  if (isAccountSettings && internationalFormatNeeded) {
    inputsAllowingPlus.push(mobileNumberInput);
    inputsAllowingPlus.push(phoneNumberInput);
    prependPlusAndCountry(mobileNumberInput);
    prependPlusAndCountry(phoneNumberInput);
  } else if (isCheckoutPage && internationalFormatNeeded) {
    inputsAllowingPlus.push(mobileNumberInput);
    prependPlusAndCountry(mobileNumberInput);
  } else if (internationalFormatNeeded) {
    prependPlusAndCountry(prefixInput);
  }

  phoneNumberInput.inputFilter(function (value) {
    return /^[+]*[0-9]{0,15}$/.test(value);
  });

  mobileNumberInput.inputFilter(function (value) {
    return /^[+]*[0-9]{0,15}$/.test(value);
  });

  function showMobileFieldWithoutPrefix() {
    return (currentMobileNumber && showFieldset) || isAccountSettings || isCheckoutPage
  }

  var toggleNumberFieldset = function (showFieldset) {
    if (showMobileFieldWithoutPrefix()) {
      mobileNumberFieldset.show();
      return;
    }
    if (showFieldset) {
      $.ajax({
        type: "GET",
        url: "/people/set_default_phone_prefix",
        data: { selected_country: countrySelect.val() },
        dataType: "json",
        success: function (data) {
          prefixInput.val("+" + data.phone_code);
        },
        error: function () {
          // leaving this blank for now because it's unclear what we want to do with an error
        }
      }).then(function () {
        mobileNumberFieldset.show();
      });
    } else if (signup) {
      mobileNumberFieldset.hide();
    }
    mobileNumberInput.prop('required', showFieldset);
  };

  toggleNumberFieldset(showFieldset);

  countrySelect.on("change", function () {
    var selectedCountry = $(this).val();
    var showFieldset = phoneRequiredIsos.includes(selectedCountry);
    toggleNumberFieldset(showFieldset);
  });
}

$(() => {
  executeInputListeners();
  // initializeConfirmInformationInputErrors();
  $("#update-contact-info-form").on("submit", (event) => {
    if (!handleConfirmPageFormSubmit(event)) {
      event.preventDefault();
      event.stopPropagation();
      console.log("handleConfirmPageFormSubmit -- FAILED")
      return false;
    }
    return true;
  });
});

