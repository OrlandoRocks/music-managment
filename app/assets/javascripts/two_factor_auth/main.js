jQuery(document).ready(function($) {
  var resend_count = 0;
  $(".resend-link").on("click", function(e){
    e.preventDefault();
    resend_count ++;
    resendButtonToggle(this, resend_count);
    $.get(this.href);
  });
});

function resendButtonToggle(button, resend_count){
  var timeout = resend_count * 2000;

  button.disabled = true;
  button.style.color = "gray";

  setTimeout(function(){
    if (resend_count >= 3){
      if (button.id === "resend-normal"){
        $(".resend-code").toggle();
        return;
      }
    }
    button.disabled = false;
    button.style.color = "#fff";
  }, timeout);
};
