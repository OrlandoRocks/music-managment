jQuery(document).ready(function($) {

  var shouldPollForPushNotifications = gon.pollForPushNotificationAuth;
  var restartPollMessage             = gon.restartPollMessage;
  var successRedirectUrl             = gon.successRedirectUrl;
  var authAction                     = gon.authAction;
  var pollApiEndpoint                = gon.pollApiEndpoint;
  var maxPollRequests                = 15;
  var pollRequestCount               = 0;
  var pollRequestInterval            = 2500;

  var success_callbacks = {
    enrollment: function() {
      $.ajax({
        url: "/two_factor_auth/enrollments.json",
        data: { person_id: gon.personId },
        type: "PUT",
        dataType: "JSON",
        success: function(result,status,xhr) {
          window.location.href = successRedirectUrl;
        },
        error: function(result,status,xhr) {
          window.location.href = successRedirectUrl;
        }
      });
    },
    authentication: function() {
      window.location.href = successRedirectUrl;
    },
    session: function() {
      window.location.href = successRedirectUrl;
    }
  };

  if (shouldPollForPushNotifications) {
    poll();
  }

  function poll() {
    if (maxPollRequestsReached()) {
      displayExpiredPushMessage();
    } else {
      setPollTimeout();
    }
  }

  function setPollTimeout() {
    setTimeout(function() {
      $.ajax({
        url: pollApiEndpoint,
        type: "GET",
        success: function(result,status,xhr) {
          if(result.status == "successful") {
            success_callbacks[authAction]();
          } else {
            poll();
          };
        }
      });
    }, pollRequestInterval);
    incrementPollRequestCount();
  }

  function incrementPollRequestCount() {
    pollRequestCount = pollRequestCount + 1;
  }

  function maxPollRequestsReached() {
    return pollRequestCount >= maxPollRequests;
  }

  function displayExpiredPushMessage() {
    var expiredPushMessage = "<a id='restart-poll' href='#'>" + restartPollMessage + "</a>"
    $("#code_sent").replaceWith(expiredPushMessage)
    $("#restart-poll").on("click", restartPoll)
  }

  function restartPoll() {
    pollRequestCount = 0;
    poll();
  }
});
