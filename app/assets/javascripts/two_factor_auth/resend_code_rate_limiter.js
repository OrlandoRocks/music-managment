(function() {
  var $ = jQuery;

  var $loginResendCodeBtn = $("#two-factor-resend-btn");
  var $accountSettingsResendBtn = $("#two-factor-resend-preferences-btn");
  var $tryAnotherWayBtn = $("#two-factor-resend-try-another-way");
  var $spinner = $("#two-factor-resend-spinner");
  var $authCodeInput = $("#tfa_form_auth_code");
  var $resendBanModal = $("#call-resend-banned-modal");
  var $tooManyResendsModal = $("#too-many-resends-modal");
  var $resendBanModalLink = $("#alternate-resend-link");

  if ($loginResendCodeBtn.length != 0) {
    var loginPayload = { token: $("#two-factor-resend-btn").data().target };
    $loginResendCodeBtn.click(function() {
      handleResendClick("/api/frontstage/two_factor_auth/resends", loginPayload, $loginResendCodeBtn)
    })
  }

  if ($accountSettingsResendBtn.length != 0 || $tryAnotherWayBtn.length != 0) {
    let body;
    if (onAuthenticationStep()) {
      body = {page: "authentication"};
    }

    $accountSettingsResendBtn.click(function() {
      handleResendClick("/api/backstage/two_factor_auth/resends", body, $accountSettingsResendBtn)
    })

    $tryAnotherWayBtn.click(function() {
      body["alternate_method"] = true;
      handleResendClick("/api/backstage/two_factor_auth/resends", body, $accountSettingsResendBtn)
    })
  }

  $(document).on('ajax:success', '.try-another-way a', function(e, response) {
    showBanMessage(response)
  })

  function onAuthenticationStep() {
    return ($accountSettingsResendBtn.attr("page") === "authentication") ||
    ($tryAnotherWayBtn.attr("page") === "authentication")
  }

  function handleResendClick(url, body, htmlElement) {
    // send code, disable button, and show spinner
    send_code(url, body, htmlElement);
    htmlElement.prop("disabled", true);
    $spinner.toggleClass("hidden");
    $authCodeInput.focus();

    // timer to enable button and hide spinner
    setTimeout(function() {
      htmlElement.prop("disabled", false);
      $spinner.toggleClass("hidden");
    }, 60000);
  }

  function send_code(url, payload, htmlElement) {
    $.ajax({
      type: "POST",
      url: url,
      data: payload
    }).done(function(response) {
      showExceedsResendsMessage(response, htmlElement)
      showBanMessage(response)
    })
  }

  function showExceedsResendsMessage(response, htmlElement) {
    if (response.exceeds_daily_resends || response.exceeds_resends_per_code) {
      $tooManyResendsModal.modal({
        minHeight: 100,
        containerCss: {
          'font-size': '14px'
        }
      });
      htmlElement.prop("disabled", true);
    }
  }

  function showBanMessage(response) {
    if (response.banned && $resendBanModal.length > 0) {
      $resendBanModal.modal({
        minHeight: 100,
        containerCss: {
          'font-size': '14px'
        }
      });

      initializeModalClickHandler()
    }
  }

  function initializeModalClickHandler() {
    var payload = { alternate_method: true };
    var token = $resendBanModalLink.data().token
    var url = '/api/backstage/two_factor_auth/resends'

    if (token.length > 0) {
      payload.token = token
      url = '/api/frontstage/two_factor_auth/resends'
    }

    $resendBanModalLink.click(function() {
      handleResendClick(url, payload, $resendBanModalLink)
    })
  }
})();
