jQuery(document).ready(function($) {
  var $nextBtn    = $(".tfa-form-bottom").find(".next-btn");
  var currentStep = $nextBtn.data("current-step");
  var tollFraudValidationEnabled = $("#toll_fraud_validation").data("enabled")
  var tollFraudValidationCodes = $("#toll_fraud_validation").data("codes")
  var methodCallRadios = ["#tfa_form_notification_method_call", "#two_factor_auth_preference_form_notification_method_call"]
  var methodCallRadioLabels = ['label[for="tfa_form_notification_method_call"]', 'label[for="two_factor_auth_preference_form_notification_method_call"]']
  var methodTextRadios = ["#tfa_form_notification_method_sms", "#two_factor_auth_preference_form_notification_method_sms"]
  var potentialTollFraudCodes = tollFraudValidationEnabled ? tollFraudValidationCodes : []
  var tollFraudValidationMessage = $("#toll_fraud_validation_message")

  var requiredInputsByStep = {
    sign_in:        { tfa_form_email: false, tfa_form_password: false },
    preferences:    {},
    authentication: { tfa_form_auth_code:     false }
  }

  var textInputs = [
    '#tfa_form_email',
    '#tfa_form_password',
    '#tfa_form_phone_number',
    '#tfa_form_auth_code',
    '#two_factor_auth_preference_form_phone_number'
  ]
  var selectInputs = ['#tfa_form_country_code', '#two_factor_auth_preference_form_country_code']

  if (requiredInputsByStep[currentStep]) {
    disableNextButton();
    attachEventListeners();
    setTimeout(checkForSignInAutofill, 1000);
    setTimeout(setRequiredInputsForPreferenceStep, 1000);
  }

  function attachEventListeners() {
    $(textInputs.join(", ")).on("keyup", updateInputValidity)
    $(selectInputs.join(", ")).on("change", updateInputValidity)

    if(tollFraudValidationEnabled) {
      $(selectInputs.join(", ")).on("change", validatePotentialTollFraud)
    }

    attachAutoCompleteListeners();
  }

  function attachAutoCompleteListeners() {
    var inputs = textInputs.slice(0, 2);

    inputs.forEach(function(input) {
      $(input).on("blur input", function() {
        requiredInputsByStep[currentStep][input.slice(1, input.length)] = isValidInput($(input)[0]);
        checkAllReqStepInputs();
      });
    });
  }

  function updateInputValidity(e) {
    requiredInputsByStep[currentStep][e.target.id] = isValidInput(e.target)
    checkAllReqStepInputs();
  }

  function isValidInput(element) {
    var inputType = element.nodeName

    if (inputType === "INPUT") {
      return element.value.length >= 4;
    } else if (inputType === "SELECT") {
      return element.value != "";
    } else {
      return false;
    }
  }

  function checkAllReqStepInputs() {
    (allReqStepInputsAreValid() && !exceededMaxSignups()) ? enableNextButton() : disableNextButton()
  }

  function allReqStepInputsAreValid() {
    return !Object.values(requiredInputsByStep[currentStep]).includes(false)
  }

  function exceededMaxSignups() {
    return $nextBtn.hasClass("max-signups")
  }

  function enableNextButton() {
    $nextBtn.prop("disabled", false)
  }

  function disableNextButton() {
    $nextBtn.prop("disabled", true)
  }

  function checkForSignInAutofill() {
    if (currentStep !== "sign_in") return;
    var $emailInput = $("#tfa_form_email")[0];

    if (isValidInput($emailInput)) {
      requiredInputsByStep[currentStep]["tfa_form_email"]    = true;
      requiredInputsByStep[currentStep]["tfa_form_password"] = true;
    }
    checkAllReqStepInputs();
  }

  function validatePotentialTollFraud(e) {
    var value = e.target.value

    if (potentialTollFraudCodes.includes(value)) {
      $(methodTextRadios.join(", ")).click()
      $(methodCallRadios.join(", ")).prop('disabled', true)
      $(tollFraudValidationMessage).removeClass("hidden")
      $(methodCallRadioLabels.join(", ")).addClass("text-grey")
    } else {
      $(methodCallRadios.join(", ")).prop('disabled', false)
      $(tollFraudValidationMessage).addClass("hidden")
      $(methodCallRadioLabels.join(", ")).removeClass("text-grey")
    }
  }

  function setRequiredInputsForPreferenceStep() {
    if (currentStep !== "preferences") return;

    var $country_code = $("#tfa_form_country_code")[0] || $("#two_factor_auth_preference_form_country_code")[0];
    var $phone_number = $("#tfa_form_phone_number")[0] || $("#two_factor_auth_preference_form_phone_number")[0];

    requiredInputsByStep[currentStep][$country_code.id] = isValidInput($country_code);
    requiredInputsByStep[currentStep][$phone_number.id] = isValidInput($phone_number);
    checkAllReqStepInputs();
  }

});
