jQuery(document).ready(function($) {
  var promptAtString = gon.tfaPromptAt;

  if ($(".tfa-prompt-modal").length > 0 && promptAtString) {
    var currentTime     = moment();
    var promptAtTime    = moment(promptAtString);
    var secsUntilPrompt = promptAtTime.diff(currentTime, 'seconds');
    if (secsUntilPrompt <= 0) {
      openPromptModal();
    } else if (secsUntilPrompt <= 3600) {
      setTimeout(openPromptModal, (secsUntilPrompt * 1000))
    }
  }

  function openPromptModal() {
    $.magnificPopup.open({
      inline: true,
      items: {
        src: ".tfa-prompt-modal"
      },
      mainClass: "tfa-prompt-show",
      preloader: false,
      fixedBgPos: true,
      modal: true
    });
  }

  function disableButton(_, button) {
    button.disabled = true;
  }

  var buttons = $("#dismissBtn, #enrollBtn");
  $("#dismissBtn").on("click", function() {
    buttons.each(disableButton);
    var person_id = gon.current_user.person.id;
    $.ajax({
      url: "/api/backstage/two_factor_auth/prompts/" + person_id,
      type: "PUT",
      success: function () {
        $.magnificPopup.close()
      }
    });
  });
});
