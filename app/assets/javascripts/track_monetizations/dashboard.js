//= require datatables
(function() {

  var $ = jQuery;
  var dashboardBEM = 'track-monetizations__dashboard__';
  var tracksDashboard = $('.track-monetizations__dashboard');

  if (tracksDashboard.length > 0) {

    var FACEBOOK = 'facebook'
    var YOUTUBE = 'youtube'

    var service = tracksDashboard[0].classList.contains(FACEBOOK) ? FACEBOOK : YOUTUBE;
    var totalRows;

    var translations        = getTranslations();
    var pageLength          = 100;

    var tableId             = '#' + service + '-' + dashboardBEM + 'data-table';
    var tableWrapper        = service + '-' + dashboardBEM + 'table-wrapper';

    var tableTitle          = dashboardBEM + 'table-title';
    var tableTitleWrapper   = dashboardBEM + 'table-title-wrapper';

    var searchBox           = dashboardBEM + 'search-box';
    var searchBoxWrapper    = dashboardBEM + 'search-box__wrapper';
    var searchBoxContainer  = dashboardBEM + 'search-box__container';

    var actionToggle        = dashboardBEM + 'action-toggle';
    var actionToggleWrapper = dashboardBEM + 'action-toggle__wrapper';
    var actionToggleLabel   = dashboardBEM + 'action-toggle__label';

    var sendAllWrapper      = dashboardBEM + 'button-wrapper--send-all';

    var footerWrapper       = dashboardBEM + 'footer';

    var pagingControls      = dashboardBEM + 'footer__paging-wrapper';
    var pagingButton        = dashboardBEM + 'footer__paging-control';

    var pagingInfo          = dashboardBEM + 'footer__info-wrapper';
    var pagingInfoText      = dashboardBEM + 'footer__info-text';

    var discoveryRow        = dashboardBEM + 'row--discovery';
    var discoveryTip        = dashboardBEM + 'discovery__tip';

    function addFoundationClasses() {
      $('.dataTables_wrapper').addClass(tableWrapper);
      $('.dataTables_filter').addClass(searchBoxWrapper);
      $('.dataTables_filter input').addClass(searchBox);
      $('.dataTables_info').addClass(pagingInfoText);
    }

    function rowByID(id) {
      return $(`tr[id="${id}"]`)
    }

    function applyDisabledStyle(json) {
      json
        .filter(r => r.discovery)
        .forEach(r => {
          rowByID(r.id).addClass(discoveryRow)
        })
    }

    function applyDiscoveryTips() {

      $(document).on('mouseover', `.${discoveryRow} .${actionToggleLabel}`, function(e) {
        $(this).qtip({
          content: {
            text: translations.tooltip_text,
            title: translations.tooltip_title,
          },
          hide: { delay: 200, fixed: true },
          overwrite: true,
          position: { my: 'bottom center', at: 'top center', target: 'mouse' },
          show: { event: e.type, ready: true },
          style: { classes: `${discoveryTip} qtip-blue`, widget: true, def: false },
        }, e);
      });
    }

    function buildToggle(track_id, opts) {
      var isChecked   = Boolean(opts.isChecked);
      var isDisabled  = Boolean(opts.isDisabled);
      var checked     = isChecked ? 'checked' : '';
      var disabled    = '';

      if (isDisabled || (service === "youtube" && isChecked)) {
        disabled = 'disabled';
      }

      return (
        '<div class="switch ' + actionToggleWrapper + '"> ' +
            '<input ' +
              'id="' + track_id + '-toggle" ' +
              'type="checkbox" ' +
              checked + ' ' +
              disabled + ' ' +
              'class="switch-input ' + actionToggle + '" ' +
              'data-takedown=' + isChecked + ' /> ' +
            '<label ' +
              'class="switch-paddle rounded ' + actionToggleLabel + '" ' +
              'for="' + track_id + '-toggle" > ' +
              '<span class="show-for-sr">' + translations.monetize_tracks + '</span> ' +
            '</label> ' +
        '</div>'
      );
    }

    function getTranslations() {
      if (service === FACEBOOK) {
        return window.translations.facebook_tracks_dashboard;
      } else if (service === "youtube") {
        return window.translations.youtube_tracks_dashboard;
      }
    }

    function statusSpan(text, type) {
      var statusClass = dashboardBEM + 'status--' + type;
      return '<span class=' + statusClass + '>' + text + '</span>';
    }

    var parseRowStatus = function(data, type, row) {
      return {
        "new":                 statusSpan(translations.new, 'new'),
        "monetized":           statusSpan(translations.monetized, 'submitted'),
        "not_monetized":       statusSpan(translations.not_monetized, 'taken-down'),
        "processing":          statusSpan(translations.pending, 'processing'),
        "processing_takedown": statusSpan(translations.taken_down, 'taken-down'),
        "blocked":             statusSpan(translations.ineligible, 'taken-down')
      }[data];
    };

    var parseRowAction = function(data, type, row) {
      return {
        "new":                  buildToggle(row.id, { isDisabled : row.discovery }),
        "monetized":            buildToggle(row.id, { isChecked: true, isDisabled: row.discovery }),
        "not_monetized":        buildToggle(row.id, { isDisabled: true }),
        "processing_takedown":  buildToggle(row.id, { isDisabled: true }),
        "processing":           buildToggle(row.id, { isChecked: true, isDisabled: true }),
        "blocked":              buildToggle(row.id, { isDisabled: true })
      }[row.status];
    };

    function sendTracks(trackMonetizationIds, takedown) {
      var url = '/api/backstage/track_monetization/dashboard';

      var data = {
        track_monetization: {
          track_monetization_ids: [trackMonetizationIds],
          takedown: takedown
        },
      };

      $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(data),
        dataType: 'JSON',
        contentType: 'application/json',
        success: function(response) { updateRowSet(response); }
      });
    }

    // Overwrite default styles for DataTables
    $.fn.dataTable.ext.classes.sPaging     = pagingControls + ' paging_';
    $.fn.dataTable.ext.classes.sPageButton = pagingButton;
    $.fn.dataTable.ext.classes.sInfo       = pagingInfoText;

    var sendAllDOM      = "<'" + sendAllWrapper + "'B>";
    var titleDOM        = "<'" + tableTitle + "'>";
    var filterDOM       = "<'" + searchBoxContainer + "'f>";
    var tableTitleDOM   = "<'" + tableTitleWrapper + "'" + titleDOM + filterDOM + ">";
    var tableBodyDOM    = 'rt';

    var pagingInfoDOM   = "<'" + pagingInfo + "'i>";
    var footerDOM       = "<'" + footerWrapper + "'" + pagingInfoDOM + "p>";

    var dashboard = $(tableId).DataTable({
      ajax: { url: 'dashboard', dataSrc: '' },
      buttons: [], // Must keep this empty array for datatables to work
      columns: [
        { data: 'id' },
        { data: 'song' },
        { data: 'isrc' },
        { data: 'artist' },
        { data: 'appears_on' },
        { data: 'upc' },
        { data: 'status', render: parseRowStatus },
        { data: 'action', render: parseRowAction }
      ],
      columnDefs: [
        { 'targets': [0], 'visible': false }
      ],
      dom: sendAllDOM + tableTitleDOM + tableBodyDOM + footerDOM,
      fnCreatedRow: function (nRow, aData, iDataIndex) { nRow.id = aData.id; },
      language: {
        info: translations.paging_info,
        infoEmpty: '',
        infoFiltered: '',
        loadingRecords: translations.processing,
        emptyTable: translations.processing,
        paginate: {
          previous: '<span class="track-monetizations__dashboard__paging-icon--previous"/>',
          next: '<span class="track-monetizations__dashboard__paging-icon--next" />'
        },
        search: '_INPUT_',
        searchPlaceholder: translations.search_placeholder,
        zeroRecords: translations.no_matching_records
      },
      pagingType: 'simple',
      pageLength: pageLength,
      initComplete: function(settings, json) {
        addFoundationClasses();
        applyDiscoveryTips();

        totalRows = json.length;

        if (!totalRows || totalRows == 0) {
          pollForCompletion();
        }

        $('.' + tableTitle).html(translations.dashboard_title + ' (' + json.length + ')');
      }
    });

    var retryCount = 0;

    function pollForCompletion() {
      totalRows = dashboard.rows().count();
      if (totalRows && totalRows > 0) { return true; }

      if (retryCount < 10) {
        retryCount += 1;
        reloadTable();
        return setTimeout(function() {
          return reloadTable(pollForCompletion);
        }, 1500);
      }

      return updateEmptyTableDiv();
    }

    function updateEmptyTableDiv() {
      return $('.dataTables_empty').html(translations.table_empty);
    }

    function reloadTable(callback) {
      if (!callback) { callback = null; }
      return dashboard.ajax.reload(null, false);
    }

    function fieldsToUpdate(obj) {
      return { status: obj.status };
    }

    function updateRowData(row, changedData) {
      return Object.assign(row.data(), changedData);
    }

    function updateRowSet(response) {
      response.data.forEach(function(responseObject) {
        var row = dashboard.row('#' + responseObject.track_id);
        row.data(updateRowData(row, fieldsToUpdate(responseObject)));
      });
    }

    function updateTitle(count) {
      $('.' + tableTitle).html(translations.dashboard_title + ' (' + count + ')');
    }

    dashboard.on('search.dt', function(e) {
      var rowCount = $(e.target).DataTable().rows({ search: 'applied' }).count();

      updateTitle(rowCount);
    });

    $(tableId).on('click', '.' + actionToggle, function(e) {
      var toggle              = $(this);
      var $tableRow           = toggle.parents('tr');
      var takedown            = toggle.data('takedown');
      var trackMonetizationId = dashboard.row($tableRow).data().id;

      sendTracks([trackMonetizationId], takedown);
    });

    dashboard.on('draw.dt', function (_, table) {
      applyDisabledStyle(table.json);
    });
  }
})();
