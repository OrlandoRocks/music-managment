(function() {
  var $ = jQuery;
  $('.learn-more-video').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false,

    iframe: {
      markup: '<div class="mfp-iframe-scaler">' +
        '<div class="mfp-close"></div>' +
        '<iframe id="player" class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
        '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

      patterns: {
        youtube: {

          index: 'youtube.com/',

          id: 'v=',

          src: '//www.youtube.com/embed/pqgsF9Y1sH4?autoplay=1&rel=0&showinfo=0&enablejsapi=1'

        }
      }
    }
  });
})();
