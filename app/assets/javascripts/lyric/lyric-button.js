var $ = jQuery;
var agreeMessage = window.translations.agree_message

var lyric;
lyric = new LyricSnippet(agreeMessage, null, null, "https://vatm.lyricfinancial.com");

lyric.confirm();

// need this to make sure it doesn't popup every refresh after clicking the button
window.history.replaceState({}, document.title, window.location.pathname);

document.addEventListener('confirmationComplete', function (e) {
  $.ajax({
    type: "POST",
    url: "/cash_advances",
    beforeSend: $.rails.CSRFProtection,
    dataType: "json",
    success: function(data){
      lyric.advanceRequestComplete(data.token);
    },
    error: function(data) {
      window.location.reload(true);
      alert(data.responseText);
    }
  });
});
