function solve_field_dependencies(selected_ct) {
  $('input[data-depn-input="person[customer_type]"]').each(function () {
    if ($(this).attr('data-depn-value') == selected_ct) {
      $(this).closest('.input-wrapper').show();
      $(this).attr('data-required', true);
      $(this).data("feValidationRequired", true);
    } else {
      $(this).closest('.input-wrapper').hide();
      $(this).attr('data-required', false);
      $(this).data("feValidationRequired", false);
    }
  });

  if (selectedUSAndTerritories($('#person_country').val())) disable_required_on_ci_fields();
}

function disable_required_on_ci_fields() {
  $('.compliance_info_field_input').attr('data-required', 'false');
  $('.compliance_info_field_input').data("feValidationRequired", false);
  $('.compliance_info_field_input').closest('.input-wrapper').find('.ci-required-star').hide();
}

function enable_required_on_ci_fields() {
  $('.compliance_info_field_input').attr('data-required', 'true');
  $('.compliance_info_field_input').data("feValidationRequired", true);
  $('.compliance_info_field_input').closest('.input-wrapper').find('.ci-required-star').show();
}

// don't remove!
function toggle_required_ci_based_on_country() {
  if (selectedUSAndTerritories($('#person_country').val())) {
    disable_required_on_ci_fields();
  } else {
    enable_required_on_ci_fields();
  }
}

function compliance_info_dependencies() {
  $(document).ready(function () {
    const qtip_fields = [
      '.locked-field-first_name',
      '.locked-field-last_name',
      '.locked-field-company_name',
      '.info-field-first_name',
      '.info-field-last_name',
      '.info-field-company_name'
    ]

    qtip_fields.forEach((field_name) => {
      $(field_name).qtip({
        style: { classes: 'locked-field-qtip-box qtip-blue', widget: true, def: false },
        content: {
          text: $(field_name).attr('data-tooltip'),
          title: $(field_name).attr('title')
        }
      });
    });

    var customer_type = $('#customer_type_list_item').data('customerType');
    $('#person_customer_type_' + customer_type).prop('checked', true);
    solve_field_dependencies(customer_type);
    addressInfoValidations()
    $('input.radio-input, input.compliance-radio-input').change(function () {
      var selected_customer_type = $(this).val();
      solve_field_dependencies(selected_customer_type);
      addressInfoValidations();
    });

    if (typeof populateUserDataFromStorage === "function") {
      populateUserDataFromStorage();
    }
  });
};
compliance_info_dependencies();
