(function($){
  $('.automator-linkbar #deliver_automator').on('click', function() {
    var albumId = $(".automator-linkbar").data('album-id')
    var deliverAutomator = +$(this).is(':checked')

    $.post('/albums/' + albumId + '/salepoints', { deliver_automator: deliverAutomator }, function (data, status) {
      if (status !== 'success') {
        console.error("There's been a problem submitting your automator selection.")
      }
    });

    var $addAutomatorToCart = $('.add-automator-to-cart')
    if ($addAutomatorToCart.length === 1) {
      $addAutomatorToCart.toggle(deliverAutomator)
    }
  });
}(jQuery));
