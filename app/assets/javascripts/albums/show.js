(function($){
  ['album', 'singles', 'ringtones'].forEach(function(action) {
    if ($('.' + action + '__show').length === 1 && history.pushState) {
      redirectToEditAction(window.location.pathname);
    };
  });
}(jQuery));
