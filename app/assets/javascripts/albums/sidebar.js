function sidebar(albumId) {
  const distPanel = $("#distribution-panel");
  $.ajax({
    url: "/spatial_audio_uploads/checklist_partial",
    type: "GET",
    data: { album_id: albumId }
  }).done(function(response) {
    if (response.partial) {
      distPanel.html(response.partial);
    }
  });
}
