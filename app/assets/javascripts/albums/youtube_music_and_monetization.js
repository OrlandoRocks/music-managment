(function ($) {
  $('#youtube_music_and_monetization_form').on('click', function () {
    var albumId = $(".automator-linkbar").data('album-id')
    var isChecked = +$(this).is(':checked')
    var monetizationValue = isChecked ? 'monetize' : 'opt_out';
    var salepointsValue = isChecked ? 'add_salepoints' : 'remove_salepoints';

    $.ajax({
      url: '/ytm/salepoints/update',
      type: 'PUT',
      data: { youtube_music_and_monetization_form: { album_id: albumId, monetization: monetizationValue, salepoints: salepointsValue } },
      error: function () {
        console.error("There's been a problem submitting your Youtube Music and Monitzation Form selection.")
      }
    })
  });
}(jQuery));
