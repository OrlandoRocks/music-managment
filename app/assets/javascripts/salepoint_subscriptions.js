jQuery(($) => {
  $('#salepoint_subscription_checkbox').click(function (e) {
    const salepointSubscriptionText = $("#automator-is-on-off-text")
    const automatorIsOff = salepointSubscriptionText.data("automator-is-off-text")
    const automatorIsOn = salepointSubscriptionText.data("automator-is-on-text")

    const checkbox = $(e.target)
    const releaseId = checkbox.data('release-id')
    const salpointSubscriptionId = checkbox.data('salepoint-subscription-id')

    let salepointStatus = salepointSubscriptionText.text().trim() == automatorIsOn

    $.ajax({
      type: "GET",
      url: `/albums/${releaseId}/salepoint_subscriptions/${salpointSubscriptionId}/toggle_active`,
      success: function () {
        salepointSubscriptionText.text(!salepointStatus ? automatorIsOn : automatorIsOff)
      },
      error: function (error) {
        checkbox.prop( "checked", false )
      },
    });
  })
})
