function getConfirmInformationSectionData() {
  const confirmInformation = {
    customerType: getConfirmInformationCustomerType(),
    complianceInfo: getConfirmInformationComplianceInfo(),
    addressInfo: getConfirmInformationAddressInfo(),
  };

  return confirmInformation;
}

function getVATInformationSectionData() {
  const vatInptus = $("#vat-information-section input");
  return getSectionDataFromInputs(vatInptus);
}

function getConfirmInformationCustomerType() {
  const checkedRadio = $(
    "#confirm-information-section input[type='radio'][name='person[customer_type]']:checked"
  );
  return getInputDataFromInputElement(checkedRadio);
}

function getConfirmInformationComplianceInfo() {
  const complianceInputs = $(
    "#confirm-information-section .compliance-info-input"
  );
  return getSectionDataFromInputs(complianceInputs);
}

function getConfirmInformationAddressInfo() {
  const addressInputs = $("#confirm-information-section .address-input").not(
    ".country-select-input"
  );
  const countryInput = $("#confirm-information-section .country-select-input");
  const addressData = getSectionDataFromInputs(addressInputs);
  const countryData = getSectionDataFromInputs(countryInput);
  return [...countryData, ...addressData];
}

function setConfirmInformationSectionData(confirmInformationSectionData) {
  if (typeof confirmInformationSectionData !== "object") {
    return;
  }

  const { customerType, complianceInfo, addressInfo } =
    confirmInformationSectionData;

  setConfirmInformationComplianceInfo(complianceInfo);
  setConfirmInformationAddressInfo(addressInfo);

  if (typeof customerType === "object") {
    const { value } = customerType;
    setConfirmInformationCustomerTypeRadio(value);
  }
}

function setVATInformationSectionData(vatInformationSectionData) {
  setVATInformationData(vatInformationSectionData);
  const vatRegistrationNumber = getVATRegistrationNumberFromSection();
  if (vatRegistrationNumber && typeof vatRegistrationNumber === "string") {
    onVATInformationConfirmClicked();
  }
}

let __calculatedGrandTotal = 0;
function getCalculatedGrandTotal() {
  return __calculatedGrandTotal;
}

function setCalculatedGrandTotal(grandTotal = 0) {
  __calculatedGrandTotal = grandTotal;
}
