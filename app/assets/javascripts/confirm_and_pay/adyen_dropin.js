$(function () {
  if (window.inAdyenDropInPage === true) {
    // In custom Adyen DropIn Payment Page

    AdyenDropinPage.setupDropin();
    AdyenDropinPage.setupActionChannel();
  }
});

const AdyenDropinPage = {
  /**
   * Gets session data from the controller and initializes the dropin
   */
  async setupDropin() {
    try {
      $("#dropin-fallback").hide();
      $(".back-warning").show();
      AdyenDropinPage.showLoader(true);
      const sessionData = AdyenDropinPage.getSessionData();
      const pageLocale = AdyenDropinPage.getPageLocale();
      const locale = pageLocale || sessionData.locale || "en-US";
      const translationsForAdyen = {
        [locale]: AdyenDropinPage.getAdyenTranslations(),
      };

      AdyenDropinPage.checkSessionData(sessionData);
      const config = AdyenDropinPage.getConfig({
        ...sessionData,
        locale,
        translations: translationsForAdyen,
      });
      const checkout = await AdyenCheckout(config);

      // Check if the payment method is China UnionPay (or credit card)
      // Remove GooglePay and ApplePay from the drop-in
      if (AdyenDropinPage.isCardPaymentMethod()) {
        checkout.paymentMethodsResponse.paymentMethods =
          checkout.paymentMethodsResponse.paymentMethods.filter(
            (m) => !["applepay", "googlepay"].includes(m.type)
          );
        checkout.paymentMethodsResponse.storedPaymentMethods =
          checkout.paymentMethodsResponse.storedPaymentMethods.filter((m) => {
            const brand = m.brand;
            return !(brand.includes("googlepay") || brand.includes("applepay"));
          });
      }

      checkout.create("dropin").mount("#dropin-container");
    } catch (error) {
      console.error("Error while setting up payment service");
      ModalHelpers.setContentsAndShow("#error-modal", gon.dropin_error);
      $("#dropin-fallback").show();
      $(".back-warning").hide();
    } finally {
      AdyenDropinPage.showLoader(false);
    }
  },

  /**
   * Returns object in the shape for Adyen Dropin
   * @param {object} param
   * @returns object for Adyen
   */
  getConfig: ({
    id,
    session_data,
    environment,
    client_key,
    locale,
    translations,
  }) => ({
    paymentMethodsConfiguration: {
      card: {
        brands: ["cup"],
      },
    },
    environment,
    clientKey: client_key,
    session: {
      id,
      sessionData: session_data,
    },
    onPaymentCompleted: (result) => {
      if (result.resultCode === "Authorised") {
        ModalHelpers.open("#info-modal");
      }
    },
    onError: (error, component) => {
      const errorMessage = error.message;
      const message = errorMessage.includes("ApplePayCancelEvent")
        ? gon.apple_pay_cancel_event
        : errorMessage;

      ModalHelpers.setContentsAndShow(
        "#error-modal",
        message || gon.something_went_wrong
      );
    },

    locale,
    translations,
  }),

  getSessionData: () => {
    const data = $("#adyen-payment-container").data();
    return data ? data.session : {};
  },

  checkSessionData: ({ id, session_data }) => {
    if (!id || !session_data) {
      throw new Error("Invalid Session Data");
    }
  },

  /**
   * Toggles the loader for the dropin
   * @param {boolean} bool
   */
  showLoader(bool = true) {
    if (bool) {
      $("#adyen-payment-container .loader").show();
    } else {
      $("#adyen-payment-container .loader").hide();
    }
  },

  getURLParams() {
    return new URLSearchParams(document.location.search);
  },

  getPageLocale() {
    const params = AdyenDropinPage.getURLParams();
    const locale = params.get("locale");
    return locale;
  },

  /**
   * Recursively un-nest a nested object.
   * Note: This is not a general purpose implementation!
   * @param {nested object} jsonObject
   * @returns {object}
   */
  flattenTranslationJSON(jsonObject) {
    const result = {};
    Object.keys(jsonObject).forEach((key) => {
      const value = jsonObject[key];
      if (
        typeof value === "object" &&
        value !== null &&
        !Array.isArray(value)
      ) {
        // Nested Object
        const flattenedJSON = this.flattenTranslationJSON(value);
        Object.keys(flattenedJSON).forEach((innerKey) => {
          result[`${key}.${innerKey}`] = flattenedJSON[innerKey];
        });
      } else {
        result[key] = value;
      }
    });
    return result;
  },

  isCardPaymentMethod() {
    const params = AdyenDropinPage.getURLParams();
    const paymentMethod = params.get("payment_method");
    return paymentMethod === "scheme";
  },

  /**
   * Special case for Adyen.
   * eg: Adyen requires payButton = "Pay Now" // text for the button
   * and also: payButton.redirecting = "Redirecting..." // for when redirecting.
   * But JSON or YAML does not allow payButton to be both string and an object.
   * __self__ is a custom escape to solve this exact problem.
   * @param {object} flattenedJSON
   * @returns object
   */
  handleSpecialCases(flattenedJSON) {
    const result = {};
    Object.keys(flattenedJSON).forEach((key) => {
      if (key.indexOf("__self__") !== -1) {
        const beforeMarker = key.split(".__self__")[0];
        result[beforeMarker] = flattenedJSON[key];
      } else {
        result[key] = flattenedJSON[key];
      }
    });
    return result;
  },

  /**
   * Translations passed from controller is formatted for Adyen and returned.
   * @returns object
   */
  getAdyenTranslations() {
    let translations = {};
    try {
      const parsedTranslations = JSON.parse(gon.adyen_translations);
      const flattenedJSON =
        AdyenDropinPage.flattenTranslationJSON(parsedTranslations);
      translations = AdyenDropinPage.handleSpecialCases(flattenedJSON);
    } catch (err) {
      console.error("Error while parsing translations");
      translations = {};
    }
    return translations;
  },

  /**
   * Setup a socket connection so that users are notified
   * about the result of Adyen webhook result.
   */
  async setupActionChannel() {
    const socket = create_socket_connection();
    const channelName = "AdyenChannel";

    socket.onopen = () => {
      subscribe_to_socket_channel(socket, channelName);
    };

    socket.onmessage = (event) => {
      const event_data = JSON.parse(event.data);
      if (event_data.type === "ping" || event_data.type === "welcome") return;

      const message = event_data?.message || {};
      const resultCode = message.result_code;

      if (resultCode === "Authorised") {
        const invoiceId = AdyenDropinPage.getInvoiceId();
        const sessionId = AdyenDropinPage.getSessionId();

        if (
          String(sessionId) === String(message.session_id) &&
          String(invoiceId) === String(message.invoice_id)
        ) {
          ModalHelpers.setContentsAndShow("#info-modal", gon.payment_confirmed);
          window.location.href = message.redirect_url;
        }
      } else if (resultCode === "unauthorised") {
        window.location.href = message.redirect_url;
      }
    };
  },

  getInvoiceId() {
    const params = AdyenDropinPage.getURLParams();
    return params.get("invoice_id") || null;
  },

  getSessionId: () => {
    const sessionData = AdyenDropinPage.getSessionData();
    AdyenDropinPage.checkSessionData(sessionData);
    return sessionData.id;
  },
};
