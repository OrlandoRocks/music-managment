const AdyenIntegrationAPI = {
  enabled: getInitialValues().adyenEnabled || false,

  /**
   * Adyen is only available for countries affiliated to BI
   * @param {(ISO 3166-1 alpha-2) string} countryCode
   * @returns boolean
   */
  isAdyenAvailable: (countryCode) => {
    try {
      const country = getCountryObject(countryCode);
      return country?.affiliated_to_bi || false;
    } catch (error) {
      return false;
    }
  },

  /**
   * Toggles the Adyen portion
   * @param {boolean} bool
   */
  showAdyenPaymentRow: (bool) => {
    showElements($(".adyen-row"), bool);
  },

  /**
   * Toggles the Adyen payment methods section inside the Adyen portion
   * Not including the loader.
   * @param {boolean} bool
   */
  showAdyenPaymentMethods: (bool) => {
    showElements($("#adyen-templates-container"), bool);
  },

  /**
   * Unchecks any Adyen payment method selection
   * @param {boolean} bool
   */
  uncheckAdyenPaymentMethods: () => {
    $(".adyen-row input[type=radio]").prop("checked", false);
  },

  /**
   * Toggle the loading indicator in Adyen portion of the Payment Methods section
   * bool is 'true' by default
   * @param {boolean} bool
   */
  showLoader(bool = true) {
    bool ? $("#adyen-loader").show() : $("#adyen-loader").hide();
  },

  /**
   * Fetch Adyen payment methods for the country,
   * update the payment methods section,
   * bind on change handlers to Adyen Payment Methods.
   * If multiple calls are happening, the last will succeed.
   * Rest are aborted.
   * @param {(ISO 3166-1 alpha-2) string} countryCode
   */
  fetchAndUpdateSection: async (countryCode) => {
    AdyenIntegrationAPI.showAdyenPaymentMethods(false);
    AdyenIntegrationAPI.showLoader(true);

    try {
      const methods = await AdyenIntegrationAPI.fetchAdyenPaymentMethods(
        countryCode
      );
      const processedMethods = AdyenIntegrationAPI.preprocessMethods(methods);

      if (
        !Array.isArray(processedMethods) ||
        !AdyenIntegrationAPI.shouldRerender(processedMethods)
      ) {
        AdyenIntegrationAPI.showAdyenPaymentMethods(true);
        AdyenIntegrationAPI.showLoader(false);
        return;
      }
      AdyenIntegrationAPI.clearSection();
      processedMethods.forEach(AdyenIntegrationAPI.addPaymentMethod);
    } catch (error) {
      if (error.statusText === "abort") {
        return;
      }
      AdyenIntegrationAPI.clearSection();
      AdyenIntegrationAPI.showGenericPaymentMethod();
      console.error("Error while fetching Adyen payment methods");
    }

    AdyenIntegrationAPI.bindChangeHandlerToPaymentMethods();
    AdyenIntegrationAPI.showAdyenPaymentMethods(true);
    AdyenIntegrationAPI.showLoader(false);
  },

  /**
   * Check if the passed payment methods are all already rendered.
   * No more, no less.
   * Any difference, it might be better to re-render
   * @param {Array<{ name, type }>} paymentMethods
   * @returns boolean; true if it should re-render
   */
  shouldRerender(paymentMethods) {
    const calculatedIds = paymentMethods.map(({ type }) =>
      AdyenIntegrationAPI.makeId(type)
    );
    const renderedIds = $(".adyen-payment-selection .adyen-payment-item")
      .map((index, el) => $(el).attr("id"))
      .toArray();

    if (
      renderedIds.length === calculatedIds.length &&
      renderedIds.every((id) => calculatedIds.includes(id))
    ) {
      return false;
    }

    return true;
  },

  /**
   * Stores the actively fetching API's XHR object
   * Used to abort when necessary
   */
  activeFetchAPIXHR: null,

  /**
   * Call API and fetch Adyen payment methods corresponding to the country code
   * @param {(ISO 3166-1 alpha-2) string} countryCode
   * @returns Array<{ name, type }>
   */
  fetchAdyenPaymentMethods: async (countryCode) => {
    if (AdyenIntegrationAPI.activeFetchAPIXHR) {
      AdyenIntegrationAPI.activeFetchAPIXHR?.abort();
    }

    return new Promise((resolve, reject) => {
      AdyenIntegrationAPI.activeFetchAPIXHR = $.ajax({
        url: `/cart/adyen_payment_methods`,
        type: "get",
        data: { country_code: countryCode },
        success: (data) => resolve(data.payment_methods),
        error: (error) => reject(error),
        complete: () => {
          AdyenIntegrationAPI.activeFetchAPIXHR = null;
        },
      });
    });
  },

  /**
   * Do any renaming, image url, or any other change here
   * @param {Array<{ name, type }>} methods
   * @returns {Array<{ name, type, imageId }>}
   */
  preprocessMethods: (methods) => {
    if (!Array.isArray(methods)) return [];
    return methods
      .filter(({ type }) => isSafariBrowser() || type !== "applepay")
      .map(({ name, type }) => ({
        name: type === "scheme" ? "UnionPay" : name,
        type,
        imageId: type === "scheme" ? "cup" : type,
      }));
  },

  /**
   * Clears the Adyen portion of the Payment Methods
   */
  clearSection: () => {
    $("#adyen-templates-container").html("");
    AdyenIntegrationAPI.showGenericPaymentMethod(false);
  },

  /**
   * Create an Adyen payment method selection row
   * and append it to the Adyen portion of the Payment Methods Section
   * @param {Object<{
   *    name: string,
   *    type: string,
   *    imageId: string
   * }>} paymentMethod
   */
  addPaymentMethod: ({ name, type, imageId }) => {
    const dom = AdyenIntegrationAPI.createPaymentTemplate(name, type, imageId);
    AdyenIntegrationAPI.appendToSection(dom);
  },

  /**
   * Creates and appends the generic payment method template to the
   * Adyen portion of Payment Methods section.
   */
  showGenericPaymentMethod: (bool = true) => {
    showElements("#adyen-generic-wrapper", bool);
  },

  /**
   * Creates an id for the input radio
   * @param {paymentMethod.type} type
   * @returns string
   */
  makeId: (type) => `adyen-payment-${type}`,

  /**
   * Creates an instance of Adyen payment method template
   * @param {string} name
   * @param {string} type
   * @param {string} imageId
   * @returns HTMLElement
   */
  createPaymentTemplate: (name, type, imageId) => {
    const dom = AdyenIntegrationAPI.createTemplate(
      "adyen-payment-item-template"
    );
    $(dom).find("input[type=radio]").val(`adyen-${type}`);
    $(dom)
      .find("input[type=radio]")
      .attr("id", AdyenIntegrationAPI.makeId(type));
    $(dom).find("label").attr("for", `adyen-payment-${type}`);
    $(dom).find("span").html(name);
    const imageUrl = AdyenIntegrationAPI.makeImageURL(imageId);
    $(dom).find("img").attr("src", imageUrl);
    $(dom).find("img").attr("alt", `${gon.pay_using} ${name}`);
    return dom;
  },

  /**
   * Generic Method to create a template given an id referencing an HTML template
   * @param {string} templateId
   * @returns HTMLElement
   */
  createTemplate: (templateId) => {
    const template = $(`#${templateId}`)[0];
    const contentDOM = template.content.firstElementChild.cloneNode(true);
    return contentDOM;
  },

  /**
   * Appends the element to Adyen portion
   * @param {HTMLElement} domElement
   */
  appendToSection: (domElement) => {
    $("#adyen-templates-container").append(domElement);
  },

  /**
   * create URL for Adyen supported payment methods
   * https://docs.adyen.com/online-payments/api-only/downloading-logos
   * @param {string} urlId
   * @returns {string} Image URL
   */
  makeImageURL: (urlId) => {
    return `https://checkoutshopper-live.adyen.com/checkoutshopper/images/logos/small/${urlId}-xhdpi.png`;
  },

  /**
   * Bind on change handlers for these adyen payment methods.
   * This function does the following
   * 1. hide the Choose Payment Method text
   * 2. show the Billed To Section
   * 3. any payment method related logic in cart.js is mirrored for Adyen payment methods
   * 4. shows Adyen payment method in the 'Billed To' section at the end
   * 5. toggles the selected item styles for adyen-payment-selection
   */
  bindChangeHandlerToPaymentMethods: () => {
    // toggle Billed To Section when selected
    bindOnPaymentMethodSelect(".adyen-payment-item");

    // toggle Billed To Section's contents when selected
    $(".adyen-payment-item").on("click", (e) => {
      const radio = e.target;

      // Remove 'selected' to reset any logic that depends on the class
      // see cart.js > bindRadioManager
      $(".ccbox").removeClass("selected");

      // Remove 'selected' from any adyen-payment-selection
      // resets the selected item styles
      $(".adyen-payment-selection").removeClass("selected");

      // Add 'selected' to the parent of the selected adyen-payment-selection
      // adds the selected item styles
      $(radio).closest(".adyen-payment-selection").addClass("selected");

      // Hides existing billed to
      // see cart.js > switchBilledTo
      $(".billed_to").hide();

      // Copy the payment method name and logo to the section
      const label = AdyenIntegrationAPI.getPaymentMethodLabel(radio);
      $("#adyen-charge-templates-container").html(label);

      // Show Adyen 'billed to' section
      // (it uses the same billed_to class from cart.js so it will revert back when cc is selected)
      $("#billed_to-adyen").show();

      // Update the pay notice text, under the 'Pay Now' Buttons
      const payNoticeName = AdyenIntegrationAPI.getPayNoticeName(radio);
      $(".pay_notice .charge_item").html(payNoticeName);
    });
  },

  /**
   * Returns the name to be use in Pay Notice
   * @param {HTMLRadioElement} methodRadio
   * @returns string
   */
  getPayNoticeName: (methodRadio) => {
    const paymentMethodName = $(methodRadio)
      .parent()
      .find("label span")
      .text()
      .trim();
    return paymentMethodName;
  },

  /**
   * Returns a copy of the UI representing the payment method correspoding to the radio element
   * @param {HTMLRadioElement} methodRadio
   * @returns HTMLElement
   */
  getPaymentMethodLabel: (methodRadio) => {
    return $(methodRadio).parent().find("label").clone();
  },

  /**
   * returns the ID of the DOM element, referring to the payment method identified by `method`
   * `method` is Adyen's payment method type (except for UnionPay which should be 'scheme')
   * See: paymentMethod.type in https://docs.adyen.com/payment-methods#credit-and-debit-cards
   * @param {paymentMethod.type | 'cup'} methodType
   * @returns HTMLElement
   */
  getPaymentMethodInput: (methodType) => {
    const type = methodType === "cup" ? "scheme" : methodType;
    const id = AdyenIntegrationAPI.makeId(type);
    return $(`#${id}`);
  },

  /**
   * Selects the preferred Adyen payment method.
   * Selects nothing if not preferred.
   */
  selectPreferredMethod: () => {
    const preferredMethod = getInitialValues().preferredAdyenMethod || "";
    const element = AdyenIntegrationAPI.getPaymentMethodInput(preferredMethod);
    preferredMethod && $(element).trigger("click");
  },
};
