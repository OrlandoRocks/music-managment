const parser = new DOMParser();

function getInitialValues() {
  try {
    const dataDiv = $("#confirm-and-pay-page-data-div");
    const initialValues = dataDiv.data("vatInitData");
    const {
      user_country: userCountry,
      sub_total_excl_vat: subtotalExclVATCents,
      grand_total: grandTotalCents,
      use_balance: useBalance,
      feature_flags: {
        vat_tax: vatTaxFeatureFlag,
        bi_transfer: biTransferFeatureFlag,
      },
      person_balance_cents: personBalanceCents,
      country_list: countryList,
      inbound_vat_rates: inboundVATRates,
      self_billing_accepted: selfBillingAccepted,
      vat_info: {
        vat_company_name: vatCompanyName,
        vat_registration_number: vatRegistrationNumber,
        country_iso_code: userCountryCode,
        vat_number_sharing: vatNumberSharing,
      },
      locale,
      adyen_enabled: adyenEnabled,
      enabled_indonesian_wallets: enabledIndonesianWallets,
      preferred_adyen_method: preferredAdyenMethod,
    } = initialValues;
    const translations = gon.translations;
    const personId = gon.current_user.person.id;

    return {
      userCountry,
      personId,
      subtotalExclVATCents,
      grandTotalCents,
      useBalance,
      vatTaxFeatureFlag,
      biTransferFeatureFlag,
      personBalanceCents,
      countryList,
      inboundVATRates,
      locale,
      translations,
      selfBillingAccepted,
      vatInfo: {
        vatCompanyName,
        vatRegistrationNumber,
        userCountryCode,
        vatNumberSharing,
      },
      adyenEnabled,
      enabledIndonesianWallets,
      preferredAdyenMethod,
    };
  } catch (error) {
    return {};
  }
}

function t(key, options, raw = false) {
  const initialValues = getInitialValues();
  const { translations = {} } = initialValues;

  const keyString = typeof key === "string" ? key : key.toString();

  const keys = keyString.split(".");

  let translation = translations || {};
  for (let i = 0; i < keys.length; ++i) {
    translation = translation[keys[i]];
    if (!translation) {
      break;
    }
  }

  if (translation) {
    if (typeof options === "object") {
      for (const key in options) {
        translation = translation.replace(`%{${key}}`, options[key]);
      }
    }

    return raw ? translation : convertStringToHTML(translation);
  }
  return keyString;
}

function convertStringToHTML(string) {
  try {
    const doc = parser.parseFromString(string, "text/html");
    return doc.body.textContent;
  } catch (error) {
    return string;
  }
}

function formatAmountCentsToCurrency(amountCents) {
  try {
    const initialValues = getInitialValues();
    const { locale } = initialValues;

    const amount = Math.round(amountCents) / 100;

    return new Intl.NumberFormat(locale.locale, {
      style: "currency",
      currency: locale.currency,
    }).format(amount);
  } catch (error) {
    console.error("Error with currency formatting.", error);
  }

  return amount;
}

function showElements(elements, bool) {
  if (bool) {
    $(elements).removeClass("hide");
    $(elements).show();
  } else {
    $(elements).addClass("hide");
    $(elements).hide();
  }
}

function combineErrors(errors) {
  let combinedErrors = "";

  if (Array.isArray(errors)) {
    combinedErrors = errors.join(", ");
  } else if (typeof errors === "string") {
    combinedErrors = errors;
  }

  return combinedErrors;
}

function getPersonBalanceTranslation(useBalance, canPayWithBalance) {
  let translation = "";
  if (useBalance) {
    if (canPayWithBalance) {
      translation = t("applied_part_to_order");
    } else {
      translation = t("applied_to_order");
    }
  } else {
    if (canPayWithBalance) {
      translation = t("you_part_pay_order");
    } else {
      translation = t("you_pay_part_order");
    }
  }

  return translation;
}

function getPersonBalanceButtonText(canPayWithBalance) {
  return canPayWithBalance ? t("use") : t("use_my");
}

function isCountryAffiliatedToBI(countryCode, countryList) {
  let isBIAffiliated = false;

  const countryObject = countryList.find(
    ({ iso_code: isoCode }) => isoCode === countryCode
  );

  if (typeof countryObject === "object") {
    isBIAffiliated = countryObject.affiliated_to_bi;
  }

  return isBIAffiliated;
}

function isCountryChangedToBIAffiliatedCountry(
  previousCountry,
  newCountry,
  countryList = []
) {
  const isPrevDefined = typeof previousCountry !== "undefined";
  const isPrevBI = isCountryAffiliatedToBI(previousCountry, countryList);
  const isNewBI = isCountryAffiliatedToBI(newCountry, countryList);
  return isPrevDefined && !isPrevBI && isNewBI;
}

function saveUserDataToStorage(userData) {
  const dataString = JSON.stringify(userData);
  sessionStorage.setItem("confirmAndPayUserData", dataString);
}

function getUserDataFromStorage() {
  try {
    const dataString = sessionStorage.getItem("confirmAndPayUserData");
    return JSON.parse(dataString);
  } catch (error) {
    console.error("Error while parsing user session data", error);
    return null;
  }
}

function getInputDataFromInputElement(element) {
  try {
    const input = $(element);
    const inputData = {
      name: input.attr("name"),
      value: input.val(),
    };
    return inputData;
  } catch (error) {
    return null;
  }
}

function getInputElementFromInputData(inputData, parent = "") {
  let inputElement = null;
  if (inputData && typeof inputData === "object") {
    const { name } = inputData;
    inputElement = $(`${parent} [name='${name}']`);
  }
  return inputElement;
}

function setSectionDataToInputs(sectionData, parent = "") {
  if (Array.isArray(sectionData)) {
    sectionData.forEach((inputData) => {
      const inputElement = getInputElementFromInputData(inputData, parent);
      const { value } = inputData;
      $(inputElement).val(value).trigger("change");
    });
  }
}

function getSectionDataFromInputs(inputs) {
  const data = [];
  $(inputs).each(function () {
    const inputData = getInputDataFromInputElement(this);
    if (inputData) {
      data.push(inputData);
    }
  });
  return data;
}

function getAttrNameFromInputName(inputName) {
  const match = inputName.match(/\[([^)]+)\]/);

  return match != null ? match[1] : inputName;
}

function isSafariBrowser() {
  const isSafari =
    navigator.vendor &&
    navigator.vendor.indexOf("Apple") > -1 &&
    navigator.userAgent &&
    navigator.userAgent.indexOf("CriOS") == -1 &&
    navigator.userAgent.indexOf("FxiOS") == -1;
  return isSafari;
}
