function calculateBaseAmountCents(
  totalPriceCents,
  taxRate,
  personBalanceCents,
  vatTaxFeatureFlag
) {
  let baseAmountCents = 0;
  if (vatTaxFeatureFlag) {
    baseAmountCents = totalPriceCents * (100 / (100 + taxRate));
    baseAmountCents =
      baseAmountCents > personBalanceCents
        ? personBalanceCents
        : baseAmountCents;
  }

  return baseAmountCents;
}

function calculateAmountPayableCents(
  totalPriceCents,
  baseAmountCents,
  outboundVATAmountCents,
  vatTaxFeatureFlag
) {
  let amountPayableCents = 0;

  if (vatTaxFeatureFlag) {
    const difference =
      totalPriceCents - (baseAmountCents + outboundVATAmountCents);

    amountPayableCents = difference > 0 ? difference : 0;
  }

  return amountPayableCents;
}

function calculateOutboundVATAmountCents(
  baseAmountCents,
  outboundTaxRate,
  vatTaxFeatureFlag
) {
  let outboundVATAmountCents = 0;

  if (vatTaxFeatureFlag) {
    outboundVATAmountCents = baseAmountCents * (outboundTaxRate / 100);
  }

  return outboundVATAmountCents;
}

function doesBalanceCoverBaseAmount(
  personBalanceCents,
  baseAmountCents,
  totalPriceCents
) {
  return (
    personBalanceCents >= baseAmountCents &&
    personBalanceCents < totalPriceCents
  );
}

function calculatePayBalanceRowAmount(
  useBalance,
  personBalanceCents,
  baseAmountCents,
  totalPriceCents,
  canPayWithBalance,
  vatTaxFeatureFlag
) {
  const balanceCoversBaseAmount = doesBalanceCoverBaseAmount(
    personBalanceCents,
    baseAmountCents,
    totalPriceCents
  );

  let amount = "";
  if (useBalance) {
    if (canPayWithBalance) {
      if (vatTaxFeatureFlag) {
        amount = `-${formatAmountCentsToCurrency(baseAmountCents)}`;
      } else {
        amount = `-${formatAmountCentsToCurrency(totalPriceCents)}`;
      }
    } else {
      amount = `-${formatAmountCentsToCurrency(personBalanceCents)}`;
    }
  } else {
    if (canPayWithBalance) {
      if (vatTaxFeatureFlag) {
        if (balanceCoversBaseAmount) {
          amount = formatAmountCentsToCurrency(personBalanceCents);
        } else if (canPayWithBalance) {
          amount = formatAmountCentsToCurrency(totalPriceCents);
        }
      } else {
        amount = formatAmountCentsToCurrency(totalPriceCents);
      }
    } else {
      amount = formatAmountCentsToCurrency(personBalanceCents);
    }
  }

  return amount;
}

function calculateGrandTotalAmount(
  useBalance,
  vatTaxFeatureFlag,
  amountPayableCents,
  canPayWithBalance,
  totalPriceCents,
  personBalanceCents
) {
  let grandTotal = 0;
  if (useBalance) {
    if (vatTaxFeatureFlag) {
      grandTotal = amountPayableCents;
    } else {
      if (canPayWithBalance) {
        grandTotal = 0;
      } else {
        grandTotal = totalPriceCents - personBalanceCents;
      }
    }
  } else {
    grandTotal = totalPriceCents;
  }
  return grandTotal;
}

function calculateCompleteOrderPaybalanceRowAmount(
  useBalance,
  vatTaxFeatureFlag,
  personBalanceCents,
  baseAmountCents,
  totalPriceCents,
  outboundVATAmountCents,
  canPayWithBalance
) {
  const balanceCoversBaseAmount = doesBalanceCoverBaseAmount(
    personBalanceCents,
    baseAmountCents,
    totalPriceCents
  );

  let amount = 0;
  if (useBalance) {
    if (vatTaxFeatureFlag) {
      amount = baseAmountCents + outboundVATAmountCents;
    } else {
      if (canPayWithBalance) {
        amount = totalPriceCents;
      } else {
        amount = personBalanceCents;
      }
    }
  } else {
    if (vatTaxFeatureFlag) {
      if (balanceCoversBaseAmount) {
        amount = personBalanceCents;
      } else if (canPayWithBalance) {
        amount = totalPriceCents;
      }
    } else {
      if (canPayWithBalance) {
        amount = totalPriceCents;
      } else {
        amount = personBalanceCents;
      }
    }
  }

  return amount;
}

function calculateCompleteOrderPaymentMethodRowAmount(
  useBalance,
  vatTaxFeatureFlag,
  amountPayableCents,
  totalPriceCents,
  personBalanceCents
) {
  let amount = 0;

  if (useBalance) {
    if (vatTaxFeatureFlag) {
      amount = amountPayableCents;
    } else {
      amount = totalPriceCents - personBalanceCents;
    }
  } else {
    amount = totalPriceCents;
  }

  return amount;
}
