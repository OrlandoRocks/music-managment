function showCartVATHeaderItems(bool) {
  const elements = $(".cart_item_headers .vat-header-item");
  showElements(elements, bool);
}

function showCartVATColumns(bool) {
  const elements = $(".vat-column");
  showElements(elements, bool);
}

function showSubtotalExclVATRow(bool) {
  const row = $("#subtotal-excluding-vat-row");
  showElements(row, bool);
}

function showTotalVATRow(bool) {
  const row = $("#total-vat-row");
  showElements(row, bool);
}

function showOutboundVATRow(bool) {
  const row = $("#outbound-vat-row");
  showElements(row, bool);
}

function showSelfbillingRow(bool) {
  const row = $("#self-billing-row");
  showElements(row, bool);
}

function showVATInformationSection(bool) {
  const section = $("#vat-information-section");
  showElements(section, bool);
}

function showOutboundVATRow(bool) {
  const row = $("#outbound-vat-row");
  showElements(row, bool);
}

function showVATRegistrationNumberContainer(bool) {
  const container = $("#total-vat-row .vat-txt");
  showElements(container, bool);
}

function showVATFormAPILoader(bool) {
  const loader = $("#vat-information-section .loading-cart-spinner");
  showElements(loader, bool);
}

function disableVATFormAPIButton(bool) {
  const button = $("#vat-form-button");
  button.attr("disabled", bool);
}

function showVATFormAPICallStatus(bool) {
  showVATFormAPILoader(bool);
  disableVATFormAPIButton(bool);
}

function showVATCompanyNameContainer(bool) {
  const container = $("#vat-information-section .vat_company_name_container");
  showElements(container, bool);
}

function showVATFormAPIError(bool) {
  const container = $("#vat-information-section .error-text");
  showElements(container, bool);
}

function disableSelfBillingCheckbox(bool) {
  const checkbox = $("#person_self_billing_status_attributes_status");
  checkbox.attr("disabled", bool);
}

function setSelfBillingCheckboxChecked(bool) {
  const checkbox = $("#person_self_billing_status_attributes_status");
  checkbox.attr("checked", bool);
}

function resetSelfBillingCheckbox() {
  disableSelfBillingCheckbox(false);
  setSelfBillingCheckboxChecked(false);
  setSelfBillingAccepted(false);
}

function showChoosePaymentMethodText(bool = true) {
  showElements($("#choose-payment-method"), bool);
}

function showBilledToSection(bool = true) {
  showElements($("#complete-order-section .payment-method-row"), bool);
}

function showPayButtonNotice(bool = true) {
  showElements($(".pay_notice"), bool);
}

function showSelectPaymentNotice(bool = true) {
  showElements($(".pay_notice_select_payment_method"), bool);
}
