$(function () {
  const initialValues = getInitialValues();
  const {
    vatTaxFeatureFlag,
    biTransferFeatureFlag,
    selfBillingAccepted,
    vatInfo: { vatNumberSharing } = {},
  } = initialValues;

  initializeAdyenPaymentSection();
  storeInitialGrandTotal();
  bindOnPaymentMethodSelect(".payment_select");
  PaymentMethods.listenForUserSelection();

  if (!vatTaxFeatureFlag || !biTransferFeatureFlag) {
    return;
  }

  const customerTypeRadios = $(
    "#customer_type_list_item input[name='person[customer_type]']"
  );

  customerTypeRadios.on("change", function () {
    const customerType = $(this).val();
    updateVATCompanyNameOnCustomerTypeChange(customerType);
  });

  showVATCompanyNameContainer(
    customerTypeRadios.filter(":checked").val() === "business"
  );
  setSelfBillingAccepted(selfBillingAccepted);

  if (typeof compliance_info_dependencies === "undefined") {
    populateUserDataFromStorage();
  }

  $(".person-balance-button").on("click", saveUserCurrentData);
  $("a[href='/logout']").on("click", clearConfirmAndPayStorage);

  const confirmInformationSection = $(
    "#confirm-information-section:not(#person_country)"
  );

  if (vatNumberSharing) {
    confirmInformationSection.on("change", () => {
      clearVATInputs();
      onVATInformationConfirmClicked();
    });
  }
});

function populateUserDataFromStorage() {
  const { personId } = getInitialValues();
  const userData = getUserDataFromStorage();
  if (
    userData &&
    typeof userData === "object" &&
    userData.personId === personId
  ) {
    const { confirmInformation, vatInformation } = userData;
    setConfirmInformationSectionData(confirmInformation);
    setVATInformationSectionData(vatInformation);
  } else {
    clearConfirmAndPayStorage();
  }
}

function saveUserCurrentData() {
  const { personId } = getInitialValues();
  const userData = {
    personId,
    confirmInformation: getConfirmInformationSectionData(),
    vatInformation: getVATInformationSectionData(),
  };
  saveUserDataToStorage(userData);
}

function onUserCountryAffiliatedToBI(isAffiliated) {
  showCartVATHeaderItems(isAffiliated);
  showCartVATColumns(isAffiliated);
  showSubtotalExclVATRow(isAffiliated);
  showTotalVATRow(isAffiliated);
}

function onVATRatesChanged(inboundVATRate, outboundVATRate, initialValues) {
  const {
    subtotalExclVATCents,
    useBalance,
    vatTaxFeatureFlag,
    personBalanceCents,
  } = initialValues;

  const totalVATCents = (subtotalExclVATCents * inboundVATRate) / 100;
  const totalPriceCents = subtotalExclVATCents + totalVATCents;
  const baseAmountCents = calculateBaseAmountCents(
    totalPriceCents,
    outboundVATRate,
    personBalanceCents,
    vatTaxFeatureFlag
  );
  const outboundVATAmountCents = calculateOutboundVATAmountCents(
    baseAmountCents,
    outboundVATRate,
    vatTaxFeatureFlag
  );
  const amountPayableCents = calculateAmountPayableCents(
    totalPriceCents,
    baseAmountCents,
    outboundVATAmountCents,
    vatTaxFeatureFlag
  );

  const canPayWithBalance = personBalanceCents >= totalPriceCents;

  storeGrandTotal(
    amountPayableCents,
    totalPriceCents,
    vatTaxFeatureFlag,
    useBalance,
    canPayWithBalance,
    personBalanceCents
  );

  updateTotalVATRow(totalVATCents);
  updateSubtotalRow(totalPriceCents);
  updatePayBalanceRow(
    useBalance,
    canPayWithBalance,
    vatTaxFeatureFlag,
    baseAmountCents,
    totalPriceCents,
    personBalanceCents
  );
  onUpdateOutboundVATAmount(
    outboundVATAmountCents,
    vatTaxFeatureFlag,
    useBalance
  );
  updateGrandTotalRow(
    amountPayableCents,
    totalPriceCents,
    vatTaxFeatureFlag,
    useBalance,
    canPayWithBalance,
    personBalanceCents
  );
  updateCompleteOrderPaybalanceRow(
    useBalance,
    canPayWithBalance,
    baseAmountCents,
    outboundVATAmountCents,
    totalPriceCents,
    personBalanceCents,
    vatTaxFeatureFlag
  );
  updateCompleteOrderPaymentMethodRow(
    useBalance,
    vatTaxFeatureFlag,
    amountPayableCents,
    totalPriceCents,
    personBalanceCents
  );
  updateCartItemVATInformation(inboundVATRate);
}

function onUserCountryIsInEU(isEU) {
  showVATInformationSection(isEU);
}

function onVATInformationConfirmClicked() {
  const initialValues = getInitialValues();
  const {
    vatTaxFeatureFlag,
    biTransferFeatureFlag,
    personId,
    vatInfo = {},
  } = initialValues;

  if (!vatTaxFeatureFlag || !biTransferFeatureFlag) {
    return;
  }

  const vatRegistrationNumber = getVATRegistrationNumberFromSection();
  let country = "";
  let addressValues = {};

  if (areAddressInputsPresent()) {
    country = getCountryCode();
    addressValues = getAddressValues();
  } else {
    const { userCountry } = getInitialValues();
    country = userCountry;
    addressValues = { addressLocked: true };
  }

  updateVATInformationAPIError("");

  if (vatRegistrationNumber) {
    showVATFormAPICallStatus(true);
    confirmVATAPI(personId, country, vatRegistrationNumber, addressValues);
  } else {
    const userCountry = vatInfo ? vatInfo.userCountryCode || "" : "";
    const updateCountry =
      typeof country === "undefined" ? userCountry : country;
    updateOrderSummaryWithInboundRate(updateCountry);
    updateVATRegistrationNumber("");
  }
}

function confirmVATAPI(
  personId,
  countryCode,
  vatRegistrationNumber,
  addressValues
) {
  $.ajax({
    url: `/people/${personId}/confirm_vat_info`,
    type: "post",
    data: {
      vat_info: {
        country_code: countryCode,
        vat_registration_number: vatRegistrationNumber,
      },
      address_values: addressValues,
    },
    success: onVATConfirmAPISuccess,
    error: onVATConfirmAPIError,
  });
}

function updateOrderSummaryWithInboundRate(countryCode) {
  const initialValues = getInitialValues();
  const inboundVATRate = getInboundVATRate(countryCode);
  onVATRatesChanged(inboundVATRate, 0, initialValues);
}

function updateVATInformationOnCountryChange(countryCode, previousCountryCode) {
  const initialValues = getInitialValues();
  const {
    countryList = [],
    vatTaxFeatureFlag,
    biTransferFeatureFlag,
    vatInfo,
  } = initialValues;

  if (!vatTaxFeatureFlag || !biTransferFeatureFlag) {
    return;
  }

  const countryObject = getCountryObject(countryCode);
  if (!countryObject) {
    console.error("Country was not matched! ", { countryCode });
    return;
  }

  const { is_eu: isEU, affiliated_to_bi: isBIAffiliated } = countryObject;
  const inboundVATRate = getInboundVATRate(countryCode);

  onVATRatesChanged(inboundVATRate, 0, initialValues);
  updateVATFormInputs(countryCode, vatInfo);
  updateVATRegistrationNumber("");
  onUserCountryAffiliatedToBI(isBIAffiliated);
  onUserCountryIsInEU(isEU);
  updateSelfBillingOnCountryChange(
    previousCountryCode,
    countryCode,
    countryList,
    isBIAffiliated
  );
}

function updateVATCompanyNameOnCustomerTypeChange(customerType) {
  const countryCode = getCountryCode();
  const initialValues = getInitialValues();
  const {
    vatInfo: { userCountryCode, vatCompanyName },
  } = initialValues;
  const isBusiness = customerType === "business";

  if (isBusiness && countryCode === userCountryCode) {
    updateVATCompanyNameInput(vatCompanyName);
  } else {
    updateVATCompanyNameInput("");
  }

  showVATCompanyNameContainer(isBusiness);
}

function updateVATFormInputs(countryCode, vatInfo) {
  const { vatRegistrationNumber, vatCompanyName, userCountryCode } = vatInfo;

  if (countryCode === userCountryCode) {
    updateVATCompanyNameInput(vatCompanyName);
    updateVATRegistrationNumberInput(vatRegistrationNumber);
    onVATInformationConfirmClicked();
  } else {
    clearVATInputs();
  }
}

function updateSelfBillingOnCountryChange(
  previousCountry,
  newCountry,
  countryList,
  isBIAffiliated
) {
  if (!isBIAffiliated) {
    resetSelfBillingCheckbox();
  }

  const isSelfBillingAccepted = getSelfBillingAccepted();
  const isGoingToBI = isCountryChangedToBIAffiliatedCountry(
    previousCountry,
    newCountry,
    countryList
  );

  if (isSelfBillingAccepted) {
    if (isGoingToBI) {
      resetSelfBillingCheckbox();
      showSelfbillingRow(true);
    } else {
      showSelfbillingRow(false);
    }
  } else {
    showSelfbillingRow(isBIAffiliated);
  }
}

function onVATConfirmAPISuccess(response) {
  try {
    showVATFormAPICallStatus(false);

    const {
      valid_vat: validVAT,
      inbound_rate: inboundVATRate,
      outbound_rate: outboundVATRate,
      errors,
    } = response;

    const vatRegistrationNumberInput = $("#vat-registration-input");
    const vatRegistrationNumber = vatRegistrationNumberInput.val();

    if (validVAT) {
      updateVATRegistrationNumber(vatRegistrationNumber);
      updateVATInformationAPIError("");
    } else {
      updateVATRegistrationNumber("");
      updateVATInformationAPIError(combineErrors(errors));
    }

    const initialValues = getInitialValues();
    onVATRatesChanged(inboundVATRate, outboundVATRate, initialValues);
  } catch (error) {
    console.error("Error while updating values", error);
    showVATFormAPICallStatus(false);
    updateVATInformationAPIError(t("something_went_wrong"));
  }
}

function onVATConfirmAPIError() {
  showVATFormAPICallStatus(false);
  updateVATInformationAPIError(t("something_went_wrong"));
}

function getCountryObject(countryCode) {
  const { countryList = [] } = getInitialValues();
  return countryList.find(({ iso_code: isoCode }) => isoCode === countryCode);
}

function getInboundVATRate(countryCode) {
  const { inboundVATRates = [] } = getInitialValues();

  const countryObject = getCountryObject(countryCode);

  if (!countryObject) {
    console.error("Country was not matched! ", { countryCode });
    return 0;
  }

  const { name } = countryObject;

  const inboundObject = inboundVATRates.find(({ country }) => country === name);
  const inboundVATRate = inboundObject
    ? Number(inboundObject.tax_rate) || 0
    : 0;

  return inboundVATRate;
}

function storeInitialGrandTotal() {
  const { useBalance, personBalanceCents, grandTotalCents } =
    getInitialValues();
  const difference =
    Math.trunc(grandTotalCents) - Math.trunc(personBalanceCents);
  const grandTotal = useBalance ? Math.max(difference, 0) : grandTotalCents;
  setCalculatedGrandTotal(grandTotal);
}

function storeGrandTotal(
  amountPayableCents,
  totalPriceCents,
  vatTaxFeatureFlag,
  useBalance,
  canPayWithBalance,
  personBalanceCents
) {
  const grandTotal = calculateGrandTotalAmount(
    useBalance,
    vatTaxFeatureFlag,
    amountPayableCents,
    canPayWithBalance,
    totalPriceCents,
    personBalanceCents
  );
  setCalculatedGrandTotal(grandTotal);
}

/**
 * Gets active country code,
 * toggles the Adyen portion depending on country code,
 * fetch and updates the Adyen payment methods.
 *
 * active country code:
 * 1. selected country (because its dynamic)
 * 2. user country (if selection is unavailable);
 *
 * @returns void
 */
async function initializeAdyenPaymentSection() {
  try {
    if (!AdyenIntegrationAPI.enabled) return;

    // Get active country code. selected > user country
    const hasCountrySelector = CheckoutForm.isCountryInputPresent();
    const selectedCountryCode = CheckoutForm.getSelectedCountryCode();
    const userCountry = CheckoutForm.getUserCountryCode();
    const countryCode = hasCountrySelector ? selectedCountryCode : userCountry;

    toggleAdyenPortionOnCountryCode(countryCode);
    await fetchAndUpdateAdyenPaymentMethods(countryCode);
    if (!PaymentMethods.didUserSelect) {
      AdyenIntegrationAPI.selectPreferredMethod();
    }
  } catch (error) {
    console.error("Error while initializing Adyen");
  }
}

/**
 * There are different payment methods available for different countries
 * @param {(ISO 3166-1 alpha-2) string} countryCode
 * @returns
 */
async function updateAdyenPaymentMethodsOnCountryChange(countryCode) {
  if (!AdyenIntegrationAPI.enabled) return;

  toggleAdyenPortionOnCountryCode(countryCode);
  await fetchAndUpdateAdyenPaymentMethods(countryCode);
  if (!PaymentMethods.didUserSelect) {
    AdyenIntegrationAPI.selectPreferredMethod();
  }
  /**
   * Only Adyen methods are unchecked. CC, Paypal, etc are kept.
   * In which case, we don't want to show the "Choose Payment Method".
   */
  const hasMethod = hasAPaymentMethodSelected();
  if (!hasMethod) {
    showBilledToSection(false);
    showPayButtonNotice(false);
    showSelectPaymentNotice(true);
    showChoosePaymentMethodText(true);
  }
}

/**
 * Adyen payment methods are only available for certain countries
 * This function toggles the Adyen portion, and any cleanups necessary.
 * @param {(ISO 3166-1 alpha-2) string} countryCode
 */
function toggleAdyenPortionOnCountryCode(countryCode) {
  if (AdyenIntegrationAPI.isAdyenAvailable(countryCode)) {
    AdyenIntegrationAPI.showAdyenPaymentRow(true);
  } else {
    AdyenIntegrationAPI.showAdyenPaymentRow(false);
    AdyenIntegrationAPI.uncheckAdyenPaymentMethods();
  }
}

/**
 * Fetch and update Adyen portion
 * @param {(ISO 3166-1 alpha-2) string} countryCode
 * @returns void
 */
async function fetchAndUpdateAdyenPaymentMethods(countryCode) {
  try {
    if (!AdyenIntegrationAPI.isAdyenAvailable(countryCode)) return;
    await AdyenIntegrationAPI.fetchAndUpdateSection(countryCode);
  } catch (error) {
    console.error("Error updating Adyen payment methods");
  }
}

/**
 * Checks if any of the payment method radio inputs are selected or using balance
 * @returns boolean
 */
function hasAPaymentMethodSelected() {
  const selected = $(".payment_select:checked").length > 0;
  const { useBalance, personBalanceCents } = getInitialValues();
  const usingBalance = personBalanceCents > 0 && useBalance;
  return selected || usingBalance || false;
}

function isPaymentMethodPresent() {
  return $("#payment-methods-section").length > 0;
}

function bindOnPaymentMethodSelect(selector) {
  $(selector).on("click", () => {
    showChoosePaymentMethodText(false);
    showBilledToSection(true);
    showPayButtonNotice(true);
    showSelectPaymentNotice(false);
  });
}

const PaymentMethods = {
  didUserSelect: false,
  listenForUserSelection: () => {
    $(".payment_select").on("click", () => {
      PaymentMethods.didUserSelect = true;
    });
  },
};
