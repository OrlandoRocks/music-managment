function updateVATInformationAPIError(message) {
  const vatFormErrorContainer = $("#vat-information-section .error-text");

  if (message) {
    vatFormErrorContainer.html(message);
    showVATFormAPIError(true);
  } else {
    showVATFormAPIError(false);
  }
}

function updateVATRegistrationNumber(vatRegistrationNumber) {
  const vatRegistrationNumberPlaceholder = $(
    "#total-vat-row .vat-registration-number-placeholder"
  );
  if (vatRegistrationNumber) {
    vatRegistrationNumberPlaceholder.html(vatRegistrationNumber);
    showVATRegistrationNumberContainer(true);
  } else {
    vatRegistrationNumberPlaceholder.html("");
    showVATRegistrationNumberContainer(false);
  }
}

function updateTotalVATRow(totalVATCents) {
  const totalVATPlaceholder = $("#total-vat-row .cart-total_price");
  totalVATPlaceholder.html(formatAmountCentsToCurrency(totalVATCents));
}

function updateSubtotalRow(subtotalCents) {
  $("#subtotal-row .cart-total_price").html(
    formatAmountCentsToCurrency(subtotalCents)
  );
}

function updatePayBalanceRow(
  useBalance,
  canPayWithBalance,
  vatTaxFeatureFlag,
  baseAmountCents,
  totalPriceCents,
  personBalanceCents
) {
  updatePayBalanceRowTranslation(useBalance, canPayWithBalance);

  const newAmount = calculatePayBalanceRowAmount(
    useBalance,
    personBalanceCents,
    baseAmountCents,
    totalPriceCents,
    canPayWithBalance,
    vatTaxFeatureFlag
  );

  if (useBalance) {
    const amountContainer = $("#pay-balance-row .cart_deduction");
    amountContainer.html(newAmount);
  } else {
    const amountButtonContainer = $("#pay-balance-row .button-callout-alt");
    const buttonText = getPersonBalanceButtonText(canPayWithBalance);
    amountButtonContainer.html(`${buttonText} ${newAmount}`);
  }
}

function updatePayBalanceRowTranslation(useBalance, canPayWithBalance) {
  const translationContainer = $("#pay-balance-row .translation");
  const newTranslation = getPersonBalanceTranslation(
    useBalance,
    canPayWithBalance
  );
  translationContainer.html(newTranslation);
}

function updateOutboundVATRow(outboundVATAmountCents) {
  $("#outbound-vat-row .cart_deduction").html(
    `-${formatAmountCentsToCurrency(outboundVATAmountCents)}`
  );
}

function updateGrandTotalRow(
  amountPayableCents,
  totalPriceCents,
  vatTaxFeatureFlag,
  useBalance,
  canPayWithBalance,
  personBalanceCents
) {
  const amountContainer = $("#grand-total-row .amount-container");

  const grandTotal = calculateGrandTotalAmount(
    useBalance,
    vatTaxFeatureFlag,
    amountPayableCents,
    canPayWithBalance,
    totalPriceCents,
    personBalanceCents
  );

  amountContainer.html(formatAmountCentsToCurrency(grandTotal));
}

function updateCompleteOrderPaybalanceRow(
  useBalance,
  canPayWithBalance,
  baseAmountCents,
  outboundVATAmountCents,
  totalPriceCents,
  personBalanceCents,
  vatTaxFeatureFlag
) {
  if (!useBalance) {
    updateCompleteOrderPaybalanceRowTranslation(useBalance, canPayWithBalance);
  }

  const newAmount = calculateCompleteOrderPaybalanceRowAmount(
    useBalance,
    vatTaxFeatureFlag,
    personBalanceCents,
    baseAmountCents,
    totalPriceCents,
    outboundVATAmountCents,
    canPayWithBalance
  );

  if (useBalance) {
    const amountContainer = $(
      "#complete-order-section .pay-balance-row .cart-balance_button span"
    );
    amountContainer.html(formatAmountCentsToCurrency(newAmount));
  } else {
    const amountButtonContainer = $(
      "#complete-order-section .pay-balance-row .cart-balance_button .button-callout-alt"
    );

    const buttonText = getPersonBalanceButtonText(canPayWithBalance);

    amountButtonContainer.html(
      `${buttonText} ${formatAmountCentsToCurrency(newAmount)}`
    );
  }
}

function updateCompleteOrderPaybalanceRowTranslation(
  useBalance,
  canPayWithBalance
) {
  const translationContainer = $(
    "#complete-order-section .pay-balance-row .translation"
  );

  const translation = getPersonBalanceTranslation(
    useBalance,
    canPayWithBalance
  );

  translationContainer.html(translation);
}

function updateCompleteOrderPaymentMethodRow(
  useBalance,
  vatTaxFeatureFlag,
  amountPayableCents,
  totalPriceCents,
  personBalanceCents
) {
  const container = $(
    "#complete-order-section .payment-method-row .cart-total_price"
  );

  const newAmount = calculateCompleteOrderPaymentMethodRowAmount(
    useBalance,
    vatTaxFeatureFlag,
    amountPayableCents,
    totalPriceCents,
    personBalanceCents
  );

  container.html(formatAmountCentsToCurrency(newAmount));
}

function updateCartItemVATInformation(vatRate = 0) {
  updateCartItemVATRate(vatRate);
  updateCartItemVATAmount(vatRate);
}

function updateCartItemVATAmount(vatRate) {
  const itemPriceContainers = $(".vat-column .final-price-placeholder");

  for (let i = 0; i < itemPriceContainers.length; ++i) {
    const container = itemPriceContainers[i];

    const itemPriceCents = Number(container.innerHTML || 0);
    const vatAmountCents = vatRate ? (itemPriceCents * vatRate) / 100 : 0;

    const vatAmountContainer = container.parentElement.getElementsByClassName(
      "vat-amount-placeholder"
    )[0];

    $(vatAmountContainer).html(formatAmountCentsToCurrency(vatAmountCents));
  }
}

function updateCartItemVATRate(vatRate) {
  const vatRateContainers = $(".vat-column .vat-rate-placeholder");
  vatRateContainers.html(`${vatRate}%`);
}

function clearVATInputs() {
  updateVATRegistrationNumberInput("");
  updateVATCompanyNameInput("");
  updateVATInformationAPIError("");
}

function onUpdateOutboundVATAmount(
  outboundVATAmountCents,
  vatTaxFeatureFlag,
  useBalance
) {
  updateOutboundVATRow(outboundVATAmountCents);

  const show = vatTaxFeatureFlag && useBalance && outboundVATAmountCents > 0;
  showOutboundVATRow(show);
}

function updateVATCompanyNameInput(vatCompanyName) {
  $("#vat-company-name-input").val(vatCompanyName);
}

function updateVATRegistrationNumberInput(vatRegistrationNumber) {
  $("#vat-registration-input").val(vatRegistrationNumber);
}

function setSelfBillingAccepted(bool) {
  selfBillingAccepted = bool;
}

function getSelfBillingAccepted() {
  return selfBillingAccepted;
}

function setConfirmInformationCustomerTypeRadio(value) {
  const radio = $(
    `#confirm-information-section input[type='radio'][name='person[customer_type]'][value='${value}']`
  );
  radio.trigger("click");
}

function setConfirmInformationComplianceInfo(complianceInfo) {
  setSectionDataToInputs(complianceInfo, "#confirm-information-section");
}

function setConfirmInformationAddressInfo(addressInfo) {
  setSectionDataToInputs(addressInfo, "#confirm-information-section");
}

function setVATInformationData(vatInformation) {
  setSectionDataToInputs(vatInformation, "#vat-information-section");
}

function getVATRegistrationNumberFromSection() {
  return $("#vat-information-section #vat-registration-input").val();
}

function areAddressInputsPresent() {
  const countryInput = $("#person_country");
  return countryInput.length > 0;
}

function getCountryCode() {
  return $("#person_country").val();
}

function getAddressValues() {
  const { customerType, complianceInfo, addressInfo } =
    getConfirmInformationSectionData();

  let addressValues = {};
  let complianceValues = {};

  addressInfo.forEach(({ name, value }) => {
    let obj_key = getAttrNameFromInputName(name);
    addressValues[obj_key] = value;
  });

  complianceInfo.forEach(({ name, value }) => {
    complianceValues[name] = value;
  });

  const { corporate_entity_id } = getCountryObject(addressValues.country);

  addressValues = {
    ...addressValues,
    customer_type: customerType.value,
    corporate_entity_id: corporate_entity_id,
    compliance_info: complianceValues,
  };

  return addressValues;
}

var selfBillingAccepted = false;
