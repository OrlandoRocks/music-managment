/**
 * I18n deals with translations
 */
const I18n = {
  translations: (() => gon.translations || {})(),

  t(key) {
    const translations = I18n.translations || {};
    return translations[key] || "";
  },
};

/**
 * CheckoutForm deals with any interaction with the form or page elements.
 */
const CheckoutForm = {
  hasAmountToPay() {
    return CheckoutForm.getCartDetails().amount > 0;
  },
  getSelectedCreditCardId() {
    const selected = $(".cc-select input.payment_select[type='radio']:checked");
    if (!selected.length) {
      return null;
    }
    const id = selected.val();
    return id || null;
  },

  getSelectedCreditCardDataParams() {
    const selected = $(".cc-select input.payment_select[type='radio']:checked");
    if (!selected.length) {
      return {};
    }
    return selected.data();
  },

  /**
   * Fetches data required for 3dsecure verification of a credit card
   * @param {number} cardId
   * @returns bin, billing address, payment token, and Client Token
   */
  async getCardDetails(cardId, countryCode) {
    return await $.post("/cart/generate_stored_card_nonce", {
      id: cardId,
      country_code: countryCode,
    });
  },

  addNonceToForm({ nonce, authenticationId }) {
    $("#three_d_secure_nonce").val(nonce);
    $("#braintree_3dsecure_auth_id").val(authenticationId);
  },

  remove3DSecureDataFromForm() {
    $("#three_d_secure_nonce").val(null);
    $("#braintree_3dsecure_auth_id").val(null);
  },

  getCartDetails() {
    const amount = getCalculatedGrandTotal();
    return { amount };
  },

  getUserCountryCode() {
    const { userCountry } = getInitialValues();
    return userCountry;
  },

  isCountryInputPresent() {
    return $("select.country-select-input").length > 0;
  },

  getSelectedCountryCode() {
    return $("select.country-select-input").val();
  },

  findCountry(countryCode, countryList) {
    const selectedCountry = countryList.find(
      ({ iso_code }) => iso_code === countryCode
    );
    return selectedCountry;
  },

  isCountryAffiliatedToBI() {
    const { countryList } = getInitialValues();
    if (!countryList || !Array.isArray(countryList)) {
      return false;
    }

    const hasCountrySelector = CheckoutForm.isCountryInputPresent();
    if (hasCountrySelector) {
      const countryCode = CheckoutForm.getSelectedCountryCode();
      const selectedCountry = CheckoutForm.findCountry(
        countryCode,
        countryList
      );
      return selectedCountry ? selectedCountry.affiliated_to_bi : false;
    }

    const userCountryCode = CheckoutForm.getUserCountryCode();
    const userCountry = CheckoutForm.findCountry(userCountryCode, countryList);
    return userCountry ? userCountry.affiliated_to_bi : false;
  },
};

/**
 * ThreeDSecure deals with card verification and data processing for 3DS
 */
const ThreeDSecure = {
  threeDS: null,
  enabled: (() => $("#three_d_secure_nonce").length > 0)(),
  selectedCardEnabled: () => {
    const data = CheckoutForm.getSelectedCreditCardDataParams();
    return data.requireThreeDSecureTransaction || false;
  },

  async initializeInstance(clientToken) {
    ThreeDSecure.threeDS = await braintree.threeDSecure.create({
      authorization: clientToken,
      version: 2,
    });
  },

  async verifyCard({ amount, nonce, bin, billingAddress }) {
    return await ThreeDSecure.threeDS.verifyCard({
      amount,
      nonce,
      bin,
      billingAddress,
      onLookupComplete: function (data, next) {
        next();
      },
    });
  },

  onCardVerified(payload) {
    if (!payload.liabilityShifted) {
      ModalHelpers.closeAll();
      ModalHelpers.setContentsAndShow(
        "#error-modal",
        `${I18n.t("verification_failed")} ${I18n.t("try_again")}`
      );
      return false;
    }

    CheckoutForm.addNonceToForm({
      nonce: payload.nonce,
      authenticationId: payload?.threeDSecureInfo?.threeDSecureAuthenticationId,
    });
    return true;
  },

  onVerificationError() {
    ModalHelpers.closeAll();
    ModalHelpers.setContentsAndShow(
      "#error-modal",
      `${I18n.t("verification_failed")} ${I18n.t("try_again")}`
    );
  },

  async verify() {
    try {
      const cardId = CheckoutForm.getSelectedCreditCardId();
      if (!cardId) {
        console.error("No Card Selected");
        throw new Error("No Card Selected");
      }
      const countryCode = CheckoutForm.getSelectedCountryCode() || undefined;
      const data = await CheckoutForm.getCardDetails(cardId, countryCode);
      const { amount } = CheckoutForm.getCartDetails();
      const dataForVerification = ThreeDSecure.processData(amount, data);
      await ThreeDSecure.initializeInstance(data.client_token);
      const payload = await ThreeDSecure.verifyCard(dataForVerification);
      return await ThreeDSecure.onCardVerified(payload);
    } catch (error) {
      ThreeDSecure.onVerificationError(error);
      return false;
    }
  },

  /**
   * Converts data into the required format
   * @param {number} amountInCents
   * @param {object} data
   * @returns object
   */
  processData(amountInCents, data) {
    const amount = amountInCents / 100;
    return {
      amount: amount.toFixed(2),
      nonce: data.payment_method_nonce,
      bin: data.bin,
      billingAddress: {
        givenName: data?.billing_address?.first_name || "",
        surname: data?.billing_address?.last_name || "",
        streetAddress: data?.billing_address?.street_address || "",
        extendedAddress: data?.billing_address?.extended_address || "",
        locality: data?.billing_address?.locality || "",
        // region: data?.billing_address?.region || "",
        postalCode: data?.billing_address?.postal_code || "",
        countryCodeAlpha2: data?.billing_address?.country_code_alpha2 || "",
      },
    };
  },
};
