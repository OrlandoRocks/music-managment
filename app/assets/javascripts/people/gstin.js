(function() {
  var cart_gstinput = $('#cart_finalize_form_gstin');
  var card_gstinput = $('#gstin');
  var gstinputs = [cart_gstinput, card_gstinput];
  gstinputs.map(function(inputField) {
    inputField.inputFilter(function (value) {
      return /^\w*$/.test(value);
    });
  });
})();
