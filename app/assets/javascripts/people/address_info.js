var visibility = {
  "hide": function (listItems) {
    listItems.forEach(function (listItem) {
      listItem.hide();
    });
  },
  "show": function (listItems) {
    listItems.forEach(function (listItem) {
      listItem.show();
    });
  }
};

function addressInfoValidations() {
  var countrySelector = $('#person_country');
  var biTransfer = countrySelector.data('biTransferValidations');
  var enableGst = countrySelector.data('enableGst');

  var personAddress1ListItem = $('#person_address1_list_item');
  var zipCodeListItem = $('#person_postal_code_list_item');
  var requiredFieldLabels = [personAddress1ListItem, zipCodeListItem];

  var personAddress1Input = $('#person_address1');
  var personAddress2Input = $('#person_address2');
  var cityInput = $('#person_city');
  var stateInput = $('#person_state');
  var zipCodeInput = $('#person_postal_code');
  var gstPersonStateInput = $('#gst_person_state');
  var cityStateAutoCompleteInput = $('#state_city_autocomplete');
  var addressInputs = [
    personAddress1Input,
    personAddress2Input,
    cityInput,
    stateInput,
    zipCodeInput
  ];

  var complianceInfoFields = [];
  $(".compliance-info-input").each(function(){
    complianceInfoFields.push($(this));
  });

  var stateListItem = $('#person_state_list_item');
  var cityListItem = $('#person_city_list_item');
  var nonGstListItems = [cityListItem, stateListItem];

  var gstAddressInputs = { gstState: gstPersonStateInput, gstCity: cityStateAutoCompleteInput };
  var nonGstStateCity = { state: stateInput, city: cityInput };

  var gstStateListItem = $('#stored_credit_card_state_list_item');
  var gstCityListItem = $('#stored_credit_card_state_city_list_item');
  var gstListItems = [gstCityListItem, gstStateListItem];

  window.onCartsPage = typeof window.onCartsPage !== 'undefined' && window.onCartsPage;
  var payNowButton = $('button.pay_button[type="submit"]');
  var withdrawButton = $(".self-billing-and-address-container form input[type='submit'");

  applyPreviousVal(countrySelector);
  if (enableGst) {
    toggleGstFields(countrySelector, gstListItems, nonGstListItems, biTransfer, addressInputs, gstAddressInputs, nonGstStateCity);
    var change = determineChange(countrySelector);
    toggleGstRequired(gstAddressInputs, change);
  }
  if (!biTransfer) { return; }

  if (window.onPaypalPage) {
    applyRequiredAndToggleToFields(countrySelector, requiredFieldLabels, withdrawButton, biTransfer, gstListItems,
      nonGstListItems, addressInputs, gstAddressInputs, nonGstStateCity, cityListItem);
  } else {
    applyRequiredAndToggleToFields(countrySelector, requiredFieldLabels, payNowButton, biTransfer, gstListItems,
      nonGstListItems, addressInputs, gstAddressInputs, nonGstStateCity, cityListItem);
  }
  applyRegexToInputs(addressInputs);
  applyRegexToInputs(complianceInfoFields);
}

function determineChange(countrySelector) {
  var gstCountries = countrySelector.data('gstCountries');
  var countryWebsiteCountry = countrySelector.data('countryWebsiteCountry');
  var selectedCountry = countrySelector.val();
  var biTransfer = countrySelector.data('biTransferValidations');
  var countryWebsiteGst = gstCountries.includes(countryWebsiteCountry);
  var selectedCountryGst = gstCountries.includes(selectedCountry);
  var toGst = biTransfer ? countryWebsiteGst && selectedCountryGst : countryWebsiteGst;

  if (toGst) {
    return "toGst";
  } else {
    return "fromGst";
  }
}

function applyRegexToInputs(addressInputs) {
  addressInputs.forEach(function (input) {
    var regex = new RegExp(input.data('regex'), input.data('regexFlag'));
    input.inputFilter(function (value) {
      return regex.test(value);
    });
  });
}

function getRequiredFieldLabels(countrySelector, requiredFieldLabels, gstListItems, cityListItem) {
  var gstCountries = countrySelector.data('gstCountries');
  var countryWebsiteCountry = countrySelector.data('countryWebsiteCountry');
  var selectedCountry = countrySelector.val();
  var selectedCountryGst = gstCountries.includes(selectedCountry);
  var countryWebsiteGst = gstCountries.includes(countryWebsiteCountry);
  var gstCountryAndDomain = selectedCountryGst && countryWebsiteGst

  var appendFieldLabels = gstCountryAndDomain ? gstListItems : cityListItem;
  var newRequiredFieldLabels = requiredFieldLabels.concat(appendFieldLabels).concat(get_ci_required_fields());
  var allRequiredFieldLabels = requiredFieldLabels.concat(gstListItems).concat(cityListItem).concat(get_ci_required_fields());
  return { newRequiredFieldLabels: newRequiredFieldLabels, allRequiredFieldLabels: allRequiredFieldLabels };
}


function applyRequiredAndToggleToFields(countrySelector, requiredFieldLabels, proceedButton, biTransfer, gstListItems,
  nonGstListItems, addressInputs, gstAddressInputs, nonGstStateCity, cityListItem) {
  var optionalAddressCountries = countrySelector.data('optionalAddressCountries');

  var { newRequiredFieldLabels, allRequiredFieldLabels } = getRequiredFieldLabels(countrySelector, requiredFieldLabels, gstListItems, cityListItem);

  if (optionalAddressCountries.includes(countrySelector.val())) {
    toggleRequiredFields(newRequiredFieldLabels, "hide", proceedButton);
  } else {
    toggleRequiredFields(newRequiredFieldLabels, "show");
  }

  if (window.onPaypalPage) {
    toggleButton(countrySelector, newRequiredFieldLabels, proceedButton);
    allRequiredFieldLabels.forEach((field) => {
      field.find('input').on('input', () => {
        var { newRequiredFieldLabels } = getRequiredFieldLabels(countrySelector, requiredFieldLabels, gstListItems, cityListItem);
        toggleButton(countrySelector, newRequiredFieldLabels, proceedButton);
      });
    });

    var required_ci_fields = get_ci_required_fields();
    toggleButton(countrySelector, [].concat(newRequiredFieldLabels, required_ci_fields), proceedButton);

    $('[data-required="true"]').on('keyup', function () {
      var ci_required_fields = get_ci_required_fields();
      toggleButton(countrySelector, [].concat(newRequiredFieldLabels, ci_required_fields), proceedButton);
    })
  }

  countrySelector.on('change', function () {
    if (typeof updateAdyenPaymentMethodsOnCountryChange === "function") {
      updateAdyenPaymentMethodsOnCountryChange(countrySelector.val());
    }

    if (typeof updateVATInformationOnCountryChange === "function") {
      updateVATInformationOnCountryChange(
        countrySelector.val(),
        countrySelector.data('previousValue')
      );
    }

    if (typeof clearConfirmInformationErrors === "function") {
      clearConfirmInformationErrors();
    }
  })

  countrySelector.on('change', function() {
    if(selectedUSAndTerritories(countrySelector.val())) {
      $('.compliance_info_field_input').attr('data-required', 'false');
      $('.compliance_info_field_input').data("feValidationRequired", false);
      $('.compliance_info_field_input').closest('.input-wrapper').find('.ci-required-star').hide();
    } else {
      $('.compliance_info_field_input').attr('data-required', 'true');
      $('.compliance_info_field_input').data("feValidationRequired", true);
      $('.compliance_info_field_input').closest('.input-wrapper').find('.ci-required-star').show();
    }
    var { newRequiredFieldLabels } = getRequiredFieldLabels(countrySelector, requiredFieldLabels, gstListItems, cityListItem);
    var enableGst = $(this).data('enableGst');

    if (enableGst) {
      toggleGstFields(countrySelector, gstListItems, nonGstListItems, biTransfer, addressInputs, gstAddressInputs, nonGstStateCity);
    }
    if (optionalAddressCountries.includes(countrySelector.val())) {
      toggleRequiredFields(newRequiredFieldLabels, "hide");
    } else {
      toggleRequiredFields(newRequiredFieldLabels, "show");
    }
    if (window.onCartsPage || window.onPaypalPage) {
      $('.address-input').val('');
    }
    if (window.onPaypalPage) {
      toggleButton(countrySelector, newRequiredFieldLabels, proceedButton);
    }
  });
}

function get_ci_required_fields() {
  var required_ci_fields = [];
  $('[data-required="true"]:visible').each(function () {
    required_ci_fields.push($(this).parents('.input-wrapper'));
  });
  return required_ci_fields;
}

function toggleRequiredFields(fields, type) {
  switch (type) {
    case "hide":
      fields.forEach(function (field) {
        field.find('.required').hide();
        field.find("input").data("feValidationRequired", false);
      });
      break;
    case "show":
      fields.forEach(function (field) {
        field.find('.required').show();
        field.find("input").data("feValidationRequired", true);
      });
      break;
    default:
      break;
  }
}

function toggleButton(countrySelector, fields, button) {
  if (!button) {
    return;
  }
  var optionalAddressCountries = countrySelector.data('optionalAddressCountries');
  if (optionalAddressCountries.includes(countrySelector.val())) {
    button.prop('disabled', false);
    return;
  }
  let valid = true;
  fields.forEach(function (field) {
    var input = typeof field.find('input').val() !== "undefined" ? field.find('input') : field.find('select');
    valid = valid && input.val().trim().length !== 0;
  });
  button.prop('disabled', !valid);
}

function toggleGstFields(countrySelector, gstListItems, nonGstListItems, biTransfer, addressInputs, gstAddressInputs, nonGstStateCity) {
  var gstCountries = countrySelector.data('gstCountries');
  var countryWebsiteCountry = countrySelector.data('countryWebsiteCountry');
  var countryWebsiteGst = gstCountries.includes(countryWebsiteCountry);

  if (!biTransfer) {
    if (countryWebsiteGst) {
      visibility["hide"](nonGstListItems);
      return toggleGstRequired(gstAddressInputs, "toGst");
    } else {
      visibility["hide"](gstListItems);
      return toggleGstRequired(gstAddressInputs, "fromGst");
    }
  }
  var selectedCountry = countrySelector.val();
  var selectedCountryGst = gstCountries.includes(selectedCountry);

  countryChangeToggles(countrySelector.data('previousValue'), selectedCountry, gstCountries, addressInputs, gstAddressInputs, nonGstStateCity);

  if (!countryWebsiteGst) {
    visibility["hide"](gstListItems);
    return toggleGstRequired(gstAddressInputs, "fromGst");
  }

  if (selectedCountryGst) {
    visibility["hide"](nonGstListItems);
    visibility["show"](gstListItems);
  } else {
    visibility["hide"](gstListItems);
    visibility["show"](nonGstListItems);
  }
}

function applyPreviousVal(countrySelector) {
  countrySelector.on('focusin', function () {
    $(this).data('previous-value', $(this).val());
  });
}

function toggleGstRequired(gstAddressInputs, change) {
  switch (change) {
    case "fromGst":
      setCityInputState();
      Object.keys(gstAddressInputs).forEach(function (key) {
        gstAddressInputs[key].removeAttr('required');
      });
      break;
    case "toGst":
      setCityInputState();
      Object.keys(gstAddressInputs).forEach(function (key) {
        gstAddressInputs[key].attr('required', true);
      });
      break;
    default:
      break;
  }
}

function clearAddressInputs(addressInputs) {
  addressInputs.forEach(function (field) {
    field.val("");
  });
}

function countryChangeToggles(previousValue, selectedCountry, gstCountries, addressInputs, gstAddressInputs) {
  var bothValuesDefined = typeof previousValue !== "undefined" && typeof selectedCountry !== "undefined";
  var selectedCountryGst = gstCountries.includes(selectedCountry);
  var previousCountryGst = gstCountries.includes(previousValue);
  var changedFromGst = bothValuesDefined && previousCountryGst && !selectedCountryGst;
  var changedToGst = bothValuesDefined && !previousCountryGst && selectedCountryGst;
  var anyCountryChange = bothValuesDefined && previousValue !== selectedCountry;

  var gstState = gstAddressInputs.gstState;
  var gstCity = gstAddressInputs.gstCity;

  if (anyCountryChange) {
    clearAddressInputs(addressInputs);
  }

  if (changedFromGst) {
    $('option:selected', gstState).removeAttr('selected');
    gstCity.val("");
    toggleGstRequired(gstAddressInputs, "fromGst");

  } else if (changedToGst) {
    $('option:selected', gstState).removeAttr('selected');
    gstCity.val("");
    toggleGstRequired(gstAddressInputs, "toGst");
  }
}
