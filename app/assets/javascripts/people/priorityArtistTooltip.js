(function() {
  let tooltip = $('#priority-artist-tooltip');
  tooltip.on('mouseover', function(e) {
    tooltip.qtip({
      content: {
        text: tooltip.data("tooltip")
      },
      show: { event: e.type, ready: true },
      hide: { event: 'mouseout' }
    });
  });
})();
