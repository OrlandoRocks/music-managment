var AccountSettingsAccordion;

jQuery(document).ready(function($) {

  AccountSettingsAccordion = (function() {

    var initialize = function() {
      $('.edit-btn').on('click', function(e) {
        openSection(e.target.dataset.section);
        e.preventDefault();
      })

      $('.cancel-btn').on('click', function(e) {
        closeSection(e.target.dataset.section);
        e.preventDefault();
      });
    };

    var openSection = function(sectionName) {
      var $editBtn = $('.edit-btn.' + sectionName);
      $editBtn.hide();
      $('.section.' + sectionName).show();
      var countrySelector = $('#person_country');
      var enableGst = countrySelector ? countrySelector.data('enableGst') : null;

      // remap the event listeners to the inputs here
      executeInputListeners();
      addressInfoValidations();
      if (enableGst) {
        setCityInputState();
      }

      toggleSelfBillingNoticeText(sectionName);
    };

    var closeSection = function(sectionName) {
      var $editBtn = $('.edit-btn.' + sectionName);
      $editBtn.show();
      $('.section.' + sectionName).hide();
      toggleSelfBillingNoticeText(sectionName);
    };

    var handleSubmission = function(anyErrorsPresent, sectionName) {
      initialize();
      if (anyErrorsPresent === "true") {
        openSection(sectionName)
      }
    };

    return {
      initialize:       initialize,
      openSection:      openSection,
      closeSection:     closeSection,
      handleSubmission: handleSubmission
    };
  })();

  AccountSettingsAccordion.initialize();

});

// inputFilter allows us to place custom restraints on input fields using custom validations, eg. regex
// see people_fields.js to see it in action
$.fn.inputFilter = function(inputFilter) {
  var input = $(this);
  return input.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
    var changeInput = $(this);
    if (inputFilter(changeInput.val())) {
      changeInput.attr('prevValue', changeInput.val());
      changeInput.attr('prevSelectionStart', this.selectionStart);
      changeInput.attr('prevSelectionEnd', this.selectionEnd);
    } else if (!!changeInput.attr('prevValue')) { // reverts back to last correct input when user inputs improperly
      changeInput.val(changeInput.attr('prevValue'));
      this.setSelectionRange(changeInput.attr('prevSelectionStart'), changeInput.attr('prevSelectionEnd'));
    } else {
      changeInput.val("");
    }
  });
};

function toggleSelfBillingNoticeText(sectionName) {
  if(sectionName === "self-billing-info") {
    const errorTextElement = $("#self-billing-info .self-billing-error-text");
    if(errorTextElement) {
      errorTextElement.toggle();
    }
  }
}
