const companyInfoErrors = {
  enterpriseNumber: null,
  companyRegistryData: null,
  placeOfLegalSeat: null,
  registeredShareCapital: null,
  formError: null,
};

const fieldErrors = {
  enterpriseNumber: null,
  companyRegistryData: null,
  placeOfLegalSeat: null,
  registeredShareCapital: null,
  formError: null,
};

const companyInfoInputIds = {
  company_information_form_enterprise_number: "enterpriseNumber",
  company_information_form_company_registry_data: "companyRegistryData",
  company_information_form_place_of_legal_seat: "placeOfLegalSeat",
  company_information_form_registered_share_capital: "registeredShareCapital",
  company_information_form_current_password: "formError",
};

$(function () {
  $("#company-information .company-info-input").each(function () {
    CompanyInfo.applyRegexToInput(this);
    CompanyInfo.initializeInputError(this);
    CompanyInfo.initializeTooltips();
  });
});

const CompanyInfo = {
  applyRegexToInput(input) {
    const regexString = $(input).data("regex");
    const regex = new RegExp(regexString || "");
    regexString && $(input).inputFilter((value) => regex.test(value));
  },

  initializeTooltips() {
    $("#company-information .tooltip-trigger").each(function () {
      $(this).qtip({
        style: {
          classes: "company-info-tooltip",
          widget: true,
          def: false,
        },
        content: {
          text: $(this).attr("data-tooltip"),
          title: $(this).attr("title"),
        },
      });
    });
  },

  initializeInputError(input) {
    const inputId = $(input).attr("id");
    const fieldName = companyInfoInputIds[inputId];
    if (
      !companyInfoInputIds.hasOwnProperty(inputId) ||
      companyInfoErrors[fieldName] !== null
    ) {
      return;
    }

    const errorDiv = this.createErrorDiv(inputId);
    $(input).after(errorDiv);
    $(input).on("keydown", () => this.toggleElement(errorDiv, false));
    companyInfoErrors[fieldName] = errorDiv;
  },

  toggleElement(element, bool) {
    if (bool) {
      $(element).removeClass("hide");
      $(element).show();
    } else {
      $(element).addClass("hide");
      $(element).hide();
    }
  },

  createErrorDiv(id, content = "", visible = false) {
    const template = $("#field-error-template")[0];
    const errorDiv = template.content.firstElementChild.cloneNode(true);
    $(errorDiv).attr("id", id);
    $(errorDiv).html(content);
    visible ? $(errorDiv).removeClass("hide") : $(errorDiv).addClass("hide");
    return errorDiv;
  },

  toggleInputError(fieldName, bool = false, error) {
    const errorDiv = companyInfoErrors[fieldName];
    errorDiv && this.toggleElement(errorDiv, bool);
    typeof error === "string" && error && $(errorDiv).html(error);
  },

  clearPasswordInput() {
    $("#company_information_form_current_password").val("");
  },

  /**
   * fieldNames eg:
   * {
   *   enterpriseNumber: "Error text", // string will set the error text
   *   companyRegistryData: true, // other truthy value will show the error div
   *   placeOfLegalSeat: false, // falsy values will hide the error
   *   registeredShareCapital, // omission will not make any changes.
   *   formError, // omitting key => errors that exist, keep existing
   * }
   */
  showInputErrors(fieldNames = {}) {
    for (const fieldName in fieldNames) {
      const errorValue = fieldNames[fieldName];
      this.toggleInputError(fieldName, !!errorValue, errorValue);
    }
  },

  hideAllErrors() {
    for (const fieldName in companyInfoErrors) {
      const errorDiv = companyInfoErrors[fieldName];
      errorDiv && this.toggleElement(errorDiv);
    }
  },

  showSection(bool = true) {
    this.toggleElement($("#company-information-section-row"), bool);
  },

  showSubtitle(message, bool = true) {
    $("#company-information .subtitle").html(message);
    this.toggleElement($("#company-information .subtitle"), bool);
  },

  validateFields() {
    let hasInvalidFields = false;
    const errors = {};
    $("#company-information .company-info-input").each(function () {
      const fieldName = companyInfoInputIds[this.id];
      if (!this.checkValidity()) {
        errors[fieldName] = "This field is invalid";
        hasInvalidFields = true;
      }
    });

    return {
      hasInvalidFields,
      errors,
    };
  },

  submit() {
    const form = $("#confirm-information-form");
    const { hasInvalidFields, errors } = this.validateFields();
    if (hasInvalidFields) {
      this.showInputErrors(errors);
      return false;
    }
    $(form).trigger("submit");
  },
};
