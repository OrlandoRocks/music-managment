var VatDetailsAccordion;

jQuery(document).ready(function($) {

  VatDetailsAccordion = (function() {
    var initialVATReigistrationNumber = "";

    var initialize = function() {
      $('.vat-edit-btn').on('click', function(e) {
        openSection(e.target.dataset.section);
        e.preventDefault();
      })

      $('.vat-cancel-btn').on('click', function(e) {
        if (e.target.dataset.section != "") {
          closeSection(e.target.dataset.section);
          e.preventDefault();
        }

      })
    }

    var openSection = function(sectionName) {
      var $editBtn = $('.vat-edit-btn.' + sectionName);
      initialVATReigistrationNumber = $(`.section.${sectionName} .vat_number_input_container input`).val();
      $editBtn.hide();
      $('.vat-header').hide();
      $('.section.' + sectionName).show();
      // remap the event listeners to the inputs here
      executeInputListeners();
    }

    var closeSection = function(sectionName) {
      var $editBtn = $('.vat-edit-btn.' + sectionName);
      $editBtn.show();
      $('.vat-header').show();
      $('.section.' + sectionName).hide();
      $(".section." + sectionName + " #flash_notice").empty();
      $(`.section.${sectionName} .vat_number_input_container input`).val(initialVATReigistrationNumber);
    }

    var handleSubmission = function(sectionName) {
      initialize();
    }

    return {
      initialize:       initialize,
      openSection:      openSection,
      closeSection:     closeSection,
      handleSubmission: handleSubmission
    }
  })();

  VatDetailsAccordion.initialize();

});
