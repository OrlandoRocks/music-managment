jQuery(document).ready(function($) {
  if (window.location.href.includes('renewal_billing_info')) {
    initStateCityAutocomplete();
    selfBillingValidations();
  }
});

function selfBillingValidations() {
  var selfBillingCheckbox = $('#person_self_billing_status_attributes_status');
  var renewalSubmitBtn = $('#renewal_submit_button');

  selfBillingCheckbox.on('change', function() {
    if (this.checked) {
      renewalSubmitBtn.prop('disabled', false);
      $('#self-billing-customer-care-error').hide();

      $('#self-billing-positive-modal').on('click', '.mfp-close', function () {
        renewalSubmitBtn.prop('disabled', true);
      });
    } else {
      renewalSubmitBtn.prop('disabled', true);
      $('#self-billing-customer-care-error').show();
    }
  });
}
