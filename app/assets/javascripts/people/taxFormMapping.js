$(() => {
  const $submitButton = $(".tax-form-mapping-container input[type=submit]");
  const $selectBoxes = $(".tax-form-mapping-container select");
  const isFormInvalid = () => Array.from($selectBoxes).some(element => $(element).val() === "");
  
  const handleFormValidity = () => {
    isFormInvalid()
      ? $submitButton.attr("disabled", "disabled").addClass("disabled")
      : $submitButton.removeAttr("disabled").removeClass("disabled");
  };
  
  $selectBoxes.on("change", handleFormValidity);
});
