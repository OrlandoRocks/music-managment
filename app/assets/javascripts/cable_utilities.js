// This Javascript File contains all the functions required to handle action cable connections from the client 

/* creating a socket connection */

const fetch_socket_protocol = () => {
  return (window.location.protocol == "https:") ? "wss:" : "ws:";
}

const generate_socket_query = () => {
  return new URLSearchParams({
    "email": gon.current_user.person.email,
    "cable_auth_token": gon.current_user.cable_auth_token
  }).toString();
}

const fetch_socket_port = () => {
  if(window.location.port !== "") return `:${window.location.port}`;
}

const generate_socket_connection_url = () => {
  return [
    fetch_socket_protocol(),
    "//",
    window.location.hostname,
    fetch_socket_port(),
    "/cable?",
    generate_socket_query()
  ].join("")
}

const create_socket_connection = () => {
  return new WebSocket(generate_socket_connection_url());
}

/* socket communications */

const subscribe_to_socket_channel = (socket, channel_name) => {
  socket.send(
    JSON.stringify({
      "command": "subscribe",
      "identifier": JSON.stringify({
        "channel": channel_name,
        "cable_auth_token": gon.current_user.cable_auth_token
      })
    })
  );
}

const unsubscribe_from_socket_channel = (socket, channel_name) => {
  socket.send(
    JSON.stringify({
      "command": "unsubscribe",
      "identifier": JSON.stringify({
        "channel": channel_name,
        "cable_auth_token": gon.current_user.cable_auth_token
      })
    })
  );
}

const invoke_cable_action = (socket, channel, action, params) => {
  socket.send(
    JSON.stringify({
      "command": "message",
      "identifier": JSON.stringify({
        "channel": channel,
        "cable_auth_token": gon.current_user.cable_auth_token,
      }),
      "data": JSON.stringify({
        "action": action,
        "params": params,
        "cable_auth_token": gon.current_user.cable_auth_token
      })
    })
  );
}
