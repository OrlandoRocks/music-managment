module CountryWebsiteUpdater
  class Policy
    # Required dependencies for ActiveModel::Errors
    extend ActiveModel::Naming
    extend ActiveModel::Translation

    def self.can_update?(person:)
      new(person: person).send(:perform)
    end

    attr_reader :errors

    private

    attr_reader :person

    def initialize(person:)
      @person = person
      @errors = ActiveModel::Errors.new(self)
    end

    def perform
      add_error if cannot_switch_country_website?

      self
    end

    def add_error
      errors.add(
        :policy,
        message: "A user cannot change their country website if they have finalized purchases."
      )
    end

    def cannot_switch_country_website?
      [
        has_transactions?,
        has_a_balance?,
        has_invoices?
      ].any?
    end

    def has_transactions?
      person.person_transactions.any?
    end

    def has_a_balance?
      person.person_balance.balance.positive?
    end

    def has_invoices?
      person.invoices.any?
    end

    # Required dependency for ActiveModel::Errors
    def read_attribute_for_validation(attr)
      send(attr)
    end
  end
end
