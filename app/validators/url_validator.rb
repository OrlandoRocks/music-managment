class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?

    begin
      uri = URI.parse(value)
      resp = uri.is_a?(URI::HTTP)
    rescue URI::InvalidURIError
      resp = false
    end
    return if resp == true

    record.errors[attribute] << (options[:message] || "is not a valid url, please make sure that your url starts with http://")
  end
end
