class SongDataFormValidator < ActiveModel::Validator
  UNICODE_VALIDATION_FIELDS = { fields: %i[lyrics name] }.freeze

  def validate(record)
    validate_name(record)
    validate_album_id(record)
    validate_song(record)
    validate_unicode(record)
  end

  def validate_name(record)
    record.errors.add(:name, "name can't be blank") if record.song_params[:name].blank?
  end

  def validate_album_id(record)
    record.errors.add(:album_id, "album_id can't be blank") if record.song_params[:album_id].blank?
  end

  def validate_song(record)
    record.song.errors.messages.each do |attribute, messages|
      messages.each do |message|
        record.errors.add(attribute, message)
      end
    end
  end

  def validate_unicode(record)
    return unless record&.song_params

    obj = record.song_params
    Utf8mb3Validator.validate(record, UNICODE_VALIDATION_FIELDS, obj)
  end
end
