class CowritersValidator < ActiveModel::Validator
  def validate(record)
    validate_unicode(record)
  end

  def validate_unicode(record)
    return unless record&.cowriter_params

    total_errors = []
    record.cowriter_params.each do |cowriter|
      # placeholder if we need index-based mapping
      cowriter_errors = []
      options[:fields].each do |field|
        str = cowriter.respond_to?(:[]) ? cowriter[field] : cowriter.send(field)
        parsed = Utf8ParserService.new(str, record.class.name)
        unsupported_chars = parsed.four_byte_chars

        cowriter_errors << [field, Utf8mb3Validator.error_message(unsupported_chars)] if unsupported_chars.any?
      end

      total_errors << cowriter_errors
    end

    record.errors[:cowriterErrors].concat(total_errors) if any_errors?(total_errors)
  end

  private

  def any_errors?(total_errors)
    total_errors.any?(&:any?)
  end
end
