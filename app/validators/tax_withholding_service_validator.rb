class TaxWithholdingServiceValidator < ActiveModel::Validator
  def validate(service)
    record = service.person_transaction
    return if record.instance_of?(PersonTransaction) || record.instance_of?(Integer)

    service.errors.add(
      :base,
      "#{service.class.name}#person_transaction is instance of #{record.class}, not #{PersonTransaction.name}"
    )
  end
end
