class AlbumFormValidator < ActiveModel::Validator
  UNICODE_VALIDATION_FIELDS = { fields: %i[label_name] }.freeze

  def validate(record)
    return record.errors.add(:album, "album params can't be blank") if record.album_params.blank?

    validate_album(record)
    validate_label_name(record)
    validate_creatives_uniqueness(record)
    validate_golive_date(record)
    validate_language_code(record)
  end

  def validate_album(record)
    return record.errors.add(:album, "album can't be blank") if record.album.blank?

    return if record.album.valid?

    record.album.errors.messages.each do |attribute, messages|
      messages.each do |message|
        record.errors.add(attribute, message)
      end
    end
  end

  def validate_label_name(record)
    record_label_name = record&.album_params&.dig(:label_name)
    return unless record_label_name

    Utf8mb3Validator.validate(record, UNICODE_VALIDATION_FIELDS, record.album_params)

    record.errors.add(:label_name, "Label name must be less than 120 characters") if record_label_name.length > 120
  end

  def validate_golive_date(record)
    return if record.album_params.dig(:specialized_release_type).present?

    if record.album_params.dig(:golive_date).blank?
      record.errors.add(:golive_date, "Golive date is required") and return
    end

    if record.album_params.dig(:timed_release_timing_scenario).blank?
      record.errors.add(:golive_date, "Timing scenario required") and return
    end

    return if record.album_params.dig(:orig_release_year).blank?

    return unless record.album_params.dig(:golive_date).in_time_zone.utc.to_date <
                  record.album_params.dig(:orig_release_year).to_date

    record.errors.add(:golive_date, "Release date must be greater than or equal to original release date") and return
  end

  def validate_creatives_uniqueness(record)
    return if record.album_params[:is_various]

    creatives = record.album_params.dig(:creatives)

    record.errors.add(:creatives, "Creatives required") and return if creatives.blank?

    if record.spotify_artists_required && creatives.any? { |creative|
         !creative[:spotify] || creative[:spotify].values.empty?
       }

      record.errors.add(:creatives, "Creatives need spotify artist id") and return
    end

    names = creatives.map { |c| c["name"] }
    record.errors.add(:creatives, "Creatives names can't be duplicated") if names.find { |c| names.count(c) > 1 }
  end

  def validate_language_code(record)
    codes = LanguageCode.where(code: record.album_params.dig(:language_code))

    record.errors.add(:language_code, "Invalid language code") unless codes.any?
  end
end
