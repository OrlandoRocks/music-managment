class Utf8mb3Validator < ActiveModel::Validator
  def self.error_message(unsupported_chars)
    "#{I18n.t('four_byte_chars')} #{unsupported_chars.join(', ')}"
  end

  def self.validate(record, options, obj = nil)
    validator = new(options)
    validator.validate(record, obj)
  end

  # Four-byte Unicode chars are unsupported (three-bye limit) in MySQL 5.7 UTF-8 columns.
  # Support downstream for four-byte chars is unknown, changing to UTF8-mb4 is not recommended.
  def validate(record, obj = nil)
    raise unless options.key?(:fields)

    obj ||= record
    options[:fields].each do |field|
      str = obj.respond_to?(:[]) ? obj[field] : obj.send(field)
      parsed = Utf8ParserService.new(str, record.class.name)
      unsupported_chars = parsed.four_byte_chars
      next unless unsupported_chars.any?

      record.errors[field] << Utf8mb3Validator.error_message(unsupported_chars)
    end
  end
end
