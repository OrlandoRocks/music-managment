# frozen_string_literal_true: true

class FlagReasonValidator < ActiveModel::Validator
  def validate(record)
    validate_flag_reason_presence(record)
    validate_flag_reason_belongs_to_flag(record)
  end

  private

  def validate_flag_reason_presence(record)
    return unless record.flag.in? Flag.with_mandatory_flag_reason

    record.errors.add(
      :flag_reason,
      "required for given flag"
    ) if record.flag_reason.blank?
  end

  def validate_flag_reason_belongs_to_flag(record)
    return if record.flag_reason.blank?

    record.errors.add(
      :flag_reason,
      "doesn't belongs to given flag"
    ) unless record.flag_reason.in? record.flag.flag_reasons
  end
end
