# frozen_string_literal: true

class PayuTransactionDecorator < SimpleDelegator
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper
  include CurrencyHelper

  SALE = "sale"
  SUCCESS = "Successful"
  FAILURE = "Failure"

  # PAYU_TODO: These methods with #try are missing from the PayuTransaction model.
  # When we add refunds, we will likely want to add some or all.
  def action
    transaction_obj.try(:action) || "sale"
  end

  def can_refund?
    transaction_obj.try(:can_refund?) || false
  end

  def currency
    transaction_obj.try(:currency) || "INR"
  end

  def refunded?
    transaction_obj.try(:refunded?) || false
  end

  def transaction_obj
    __getobj__
  end

  def name
    transaction_obj.person.name
  end

  delegate :country_website, to: :transaction_obj

  # PAYU_TODO: Do we want something like this on the model itself?
  def status
    transaction_obj.status ? SUCCESS : FAILURE
  end

  def amount
    currency_subunit_to_superunit(transaction_obj.amount)
  end

  def response_code
    transaction_obj.error_code
  end

  def transaction_id
    transaction_obj.payu_id
  end

  def avs_response
    nil
  end

  def cvv_response
    nil
  end

  def ip_address
    "N/A"
  end

  def raw_response
    "N/A"
  end

  # PAYU_TODO: These refund methods should be updated when PayU refunds are implemented
  def refunds
    transaction_obj.refunds.map { |o| PayuRefundDecorator.new(o) }
  end

  def refunded_amount
    currency_subunit_to_superunit(transaction_obj.refunded_amount)
  end

  def stored_credit_card
    nil
  end
end
