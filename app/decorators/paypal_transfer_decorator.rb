class PaypalTransferDecorator < SimpleDelegator
  def suspicious_class
    "suspicious" if SuspiciousTransferDeterminationService.transfer_suspicious?(self)
  end
end
