class PaymentsOSTransactionDecorator < SimpleDelegator
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper
  include CurrencyHelper

  def transaction_obj
    __getobj__
  end

  def name
    transaction_obj.person.name
  end

  delegate :country_website, to: :transaction_obj

  def status
    (transaction_obj.action == PaymentsOSTransaction::SALE) ? payment_status : refund_status
  end

  def response_code
    nil
  end

  def amount
    currency_subunit_to_superunit(attributes["amount"])
  end

  def avs_response
    nil
  end

  def cvv_response
    nil
  end

  def refunds
    transaction_obj.refunds.map { |o| PaymentsOSRefundDecorator.new(o) }
  end

  def refunded_amount
    currency_subunit_to_superunit(transaction_obj.refunded_amount)
  end

  def raw_response
    # JSON.pretty_generate(charge_raw_response)
    charge_raw_response
  end
end
