class DistributionErrorFormDecorator < SimpleDelegator
  def date_start
    start_date.strftime("%m/%d/%Y")
  end

  def date_end
    end_date.strftime("%m/%d/%Y")
  end

  def table_title(table_title)
    (filter_type == "store") ? store_map[table_title] : table_title
  end

  def by_store?
    filter_type == "store"
  end

  def by_error?
    filter_type == "error"
  end

  def by_release?
    filter_type == "release"
  end

  def message_table_header
    (state_type == "error") ? "Error Message" : "Distribution State"
  end

  def store_map
    Distribution::ErrorQueryBuilder.new.store_map
  end
end
