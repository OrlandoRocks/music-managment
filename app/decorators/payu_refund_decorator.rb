# frozen_string_literal: true

# PAYU_TODO: This class is a skeleton & should be updated when PayU refunds are implemented
class PayuRefundDecorator < SimpleDelegator
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper
  include CurrencyHelper

  def status
    refund_status
  end

  def success?
    refund_status != PaymentsOSTransaction::FAILED
  end

  def amount
    currency_subunit_to_superunit(attributes["amount"])
  end

  def response_code
    nil
  end

  def avs_response
    nil
  end

  def cvv_response
    nil
  end

  def raw_response
    refund_raw_response
  end
end
