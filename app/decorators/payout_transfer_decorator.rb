class PayoutTransferDecorator < SimpleDelegator
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  def status
    return custom_t(:completed) if provider_status == PayoutTransfer::COMPLETED
    return custom_t(:declined)  if provider_status == PayoutTransfer::DECLINED

    custom_t(tunecore_status.to_sym)
  end

  def to_money
    Money.new(amount_cents, currency)
  end

  delegate :name, to: :person, prefix: true
end
