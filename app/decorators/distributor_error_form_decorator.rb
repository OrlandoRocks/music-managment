class DistributorErrorFormDecorator < SimpleDelegator
  def date_start
    start_date.strftime("%m/%d/%Y")
  end

  def date_end
    end_date.strftime("%m/%d/%Y")
  end

  def table_title(table_head)
    return store_map[table_head] if filter_type == "store_id" && table_head.is_a?(Integer)

    table_head
  end

  def by_store?
    filter_type == "store_id"
  end

  def by_error?
    filter_type == "error_type"
  end

  def by_album?
    filter_type == "album_id"
  end

  def store_map
    Distribution::ErrorQueryBuilder.new.store_map
  end
end
