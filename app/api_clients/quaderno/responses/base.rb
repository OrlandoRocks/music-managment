module Quaderno::Responses
  class Base
    def initialize(raw_response:)
      @raw_response = raw_response
    end

    def body
      @json_response ||= JSON.parse(@raw_response.body)
    end

    def endpoint
      @endpoint ||= @raw_response.env.url.to_s
    end

    def http_status
      @http_status ||= @raw_response.status
    end
  end
end
