module Quaderno::Responses
  class CalculateTax < Quaderno::Responses::Base
    def name
      body["name"]
    end

    def rate
      body["rate"]
    end

    def country
      body["country"]
    end

    def city
      body["city"]
    end

    def region
      body["region"]
    end

    def county
      body["county"]
    end

    def county_tax_code
      body["county_tax_code"]
    end

    def city_tax_code
      body["city_tax_code"]
    end

    def transaction_type
      body["transaction_type"]
    end
  end
end
