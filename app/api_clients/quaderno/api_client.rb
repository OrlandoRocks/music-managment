require "./app/api_clients/quaderno/requests/errors"

module Quaderno
  class ApiClient
    REQUESTS_PATH = "Quaderno::Requests".freeze
    RESPONSE_PATH = "Quaderno::Responses".freeze
    QUADERNO_CLIENT_NAME = ENV["QUADERNO_CLIENT_NAME"] || "ninive-9895".freeze
    QUADERNO_API_KEY = ENV["QUADERNO_API_KEY"] || "sk_live_g6A_1sU-h2GEp3zydArs".freeze

    attr_reader :http_client

    def initialize
      build_client
      build_headers if http_client
      setup_basic_auth if http_client
    end

    def send_request(name, params = {})
      request = build_request(name).new(params)
      send(
        request.method,
        request.url,
        request.params,
        request.response_class,
      )
    end

    private

    def build_client
      @http_client = Faraday.new("https://#{QUADERNO_CLIENT_NAME}.quadernoapp.com/") if !!QUADERNO_CLIENT_NAME
    end

    def build_headers
      http_client.headers["Content-Type"] = "application/json"
    end

    def setup_basic_auth
      http_client.basic_auth(QUADERNO_API_KEY, "x")
    end

    def build_request(name)
      "#{REQUESTS_PATH}::#{name.to_s.classify}".constantize
    end

    def get(url, params, response_class)
      res =
        http_client.get(url) do |req|
          req.params = params
        end
      response_class.new(raw_response: res)
    end
  end
end
