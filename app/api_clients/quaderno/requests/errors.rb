module Quaderno::Requests
  class Errors < StandardError; end

  class CountryMissingError < StandardError; end
end
