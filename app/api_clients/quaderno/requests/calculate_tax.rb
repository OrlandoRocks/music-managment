require "./app/api_clients/quaderno/requests/errors"

module Quaderno::Requests
  class CalculateTax
    attr_reader :options

    def initialize(options = {})
      raise CountryMissingError, "country is a required parameter" unless options[:country]

      @options = options
    end

    def url
      "/api/taxes/calculate.json"
    end

    def method
      :get
    end

    def params
      {
        country: options[:country], # mandatory
        city: options[:city],
        tax_id: options[:tax_id],
        postal_code: options[:postal_code],
        transaction_type: options[:transaction_type],
      }.compact
    end

    def response_class
      Quaderno::Responses::CalculateTax
    end
  end
end
