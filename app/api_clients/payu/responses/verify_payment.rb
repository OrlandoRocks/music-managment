# frozen_string_literal: true

class Payu::Responses::VerifyPayment
  attr_accessor :raw_response, :body

  SUCCESS = "success"
  NOT_FOUND = "Not Found"

  def initialize(response)
    @response_status = response.status
    @raw_response    = response
    @body            = Oj.load(response.body)
  end

  def success?
    (200..299).cover?(@response_status)
  end

  def status
    @body.fetch("status")
  end

  def txn_found?
    status == 1
  end

  def msg
    @body.fetch("msg")
  end

  def transaction_details
    @body.fetch("transaction_details")
  end

  # we only ever expect one transaction result
  def transaction_body
    _, body = transaction_details.first
    body
  end

  # confusingly, an error code of "E000" means the payment was successful
  # https://devguide.payu.in/docs/developers-guide/miscellaneous/error-codes/
  def error_code
    transaction_body.fetch("error_code", nil)
  end

  def payment_method
    transaction_body.fetch("PG_TYPE", nil)
  end

  def payment_succeeded?
    transaction_body.fetch("status") == SUCCESS
  end

  def transaction_id
    payu_id = transaction_body.fetch("mihpayid")
    return if payu_id == NOT_FOUND

    payu_id
  end
end
