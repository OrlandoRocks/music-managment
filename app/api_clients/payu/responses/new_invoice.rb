# frozen_string_literal: true

class Payu::Responses::NewInvoice
  attr_accessor :raw_response, :body

  def initialize(response)
    @response_status = response.status
    @raw_response    = response
    @body            = JSON.parse(response.body, object_class: OpenStruct)
  end

  def success?
    (200..299).cover?(@response_status)
  end

  def response_code
    @body.responseCode
  end

  def response_message
    @body.responseMsg
  end
end
