# frozen_string_literal: true

class Payu::Responses::RefundTransaction
  attr_accessor :raw_response, :body

  def initialize(response)
    @response_status = response.status
    @raw_response    = response
    @body            = Oj.load(response.body)
  end

  def success?
    @body["status"] == 1
  end

  def msg
    @body["msg"]
  end

  def error_code
    @body["error_code"]
  end

  def payu_id
    @body["mihpayid"]
  end

  def request_id
    @body["request_id"]
  end
end
