# frozen_string_literal: true

class Payu::Responses::VerifyRefund
  attr_accessor :raw_response, :body

  FAILURE = "failure"
  FAILURE_CODE = 0

  def initialize(response)
    @response_status = response.status
    @raw_response    = response
    @body            = Oj.load(response.body)
  end

  def failure?(request_id)
    @body["status"] == FAILURE_CODE || transaction_details(request_id)["status"] == FAILURE
  end

  def status(request_id)
    return if failure?(request_id)

    transaction_details(request_id)["status"]
  end

  def mode(request_id)
    return if failure?(request_id)

    transaction_details(request_id)["mode"]
  end

  def transaction_details(request_id)
    @body["transaction_details"][request_id][request_id]
  end
end
