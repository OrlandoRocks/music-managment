# frozen_string_literal: true

class Payu::Responses::NewPayment
  attr_accessor :raw_response, :body

  def initialize(response)
    @response_status = response.status
    @raw_response    = response
  end

  def success?
    (200..399).cover?(@response_status) # include redirects as successes in this call.
  end

  def response_code
    @body.responseCode
  end

  def redirect
    @raw_response.headers["Location"]
  end
end
