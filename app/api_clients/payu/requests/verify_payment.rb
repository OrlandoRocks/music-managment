# frozen_string_literal: true

class Payu::Requests::VerifyPayment
  VERIFY_PAYMENT = "verify_payment"
  URL = "/merchant/postservice.php?form=2"

  def initialize(options)
    @invoice_id = options[:invoice_id]
  end

  def url
    URL
  end

  def response_parser
    Payu::Responses::VerifyPayment
  end

  def request_method
    :post
  end

  def params
    {
      key: key,
      command: command,
      var1: var1,
      hash: compute_request_auth_hash,
    }
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def key
    Payu::ApiClient::MERCHANT_KEY
  end

  def command
    VERIFY_PAYMENT
  end

  # this is what the PayU API calls the id we generate on our end ¯\_(ツ)_/¯
  def var1
    @invoice_id
  end

  def salt
    Payu::ApiClient::MERCHANT_SALT
  end

  def compute_request_auth_hash
    Digest::SHA512.hexdigest(
      "#{key}|#{command}|#{var1}|#{salt}"
    )
  end
end
