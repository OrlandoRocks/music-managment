# frozen_string_literal: true

class Payu::Requests::VerifyRefund
  VERIFY_REFUND = "check_action_status"
  URL = "/merchant/postservice.php?form=2"

  def initialize(options)
    @request_id = options[:request_id]
  end

  def url
    URL
  end

  def response_parser
    Payu::Responses::VerifyRefund
  end

  def request_method
    :post
  end

  def params
    {
      key: key,
      command: command,
      var1: @request_id,
      hash: compute_request_auth_hash,
    }
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def key
    Payu::ApiClient::MERCHANT_KEY
  end

  def command
    VERIFY_REFUND
  end

  def salt
    Payu::ApiClient::MERCHANT_SALT
  end

  def compute_request_auth_hash
    Digest::SHA512.hexdigest(
      "#{key}|#{command}|#{@request_id}|#{salt}"
    )
  end
end
