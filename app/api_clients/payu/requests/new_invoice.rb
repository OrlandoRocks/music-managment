class Payu::Requests::NewInvoice
  def initialize(options)
    @invoice      = options[:invoice]
    @invoice_file = options[:invoice_file]
  end

  def url
    "/merchant/postservice.php?form=2"
  end

  def response_parser
    Payu::Responses::NewInvoice
  end

  def request_method
    :post
  end

  def params
    {
      command: :opgsp_upload_invoice_awb,
      var1: payu_id,
      var2: @invoice.id,
      var3: :invoice,
      key: Payu::ApiClient::MERCHANT_KEY,
      hash: compute_request_auth_hash,
      file: @invoice_file
    }
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def payu_id
    @payu_id ||= @invoice
                 .invoice_settlements
                 .find_by(source_type: "PayuTransaction")
                 .source
                 .payu_id
  end

  def compute_request_auth_hash
    Digest::SHA512.hexdigest(
      "#{Payu::ApiClient::MERCHANT_KEY}|opgsp_upload_invoice_awb|#{payu_id}|#{Payu::ApiClient::MERCHANT_SALT}"
    )
  end
end
