class Payu::Requests::NewPayment
  include Rails.application.routes.url_helpers
  def initialize(options)
    @invoice = options[:invoice]
  end

  def url
    "/_payment"
  end

  def response_parser
    Payu::Responses::NewPayment
  end

  def request_method
    :post
  end

  def params
    {
      key: key,
      txnid: txnid,
      amount: amount,
      productinfo: productinfo,
      firstname: firstname,
      email: email,
      phone: phone,
      surl: surl, # success URL
      furl: furl, # failure URL
      hash: compute_request_auth_hash,
      udf5: txnid
    }
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def key
    Payu::ApiClient::MERCHANT_KEY
  end

  def txnid
    @invoice.id
  end

  def amount
    Float(@invoice.outstanding_amount_in_local_currency) / 100
  end

  def productinfo
    "Your TuneCore Payment"
  end

  def firstname
    @invoice.person.first_name
  end

  def email
    @invoice.person.email
  end

  def phone
    @invoice.person.phone_number?
  end

  def surl
    finalize_after_redirect_cart_url(invoice_id: @invoice.id, host: @invoice.person.domain) # https://www.tunecore.com/cart/finalize_after_redirect?invoice_id=1234
  end

  def furl
    finalize_after_redirect_cart_url(invoice_id: @invoice.id, failed: "true", host: @invoice.person.domain) # https://www.tunecore.com/cart/finalize_after_redirect?invoice_id=1234?failed=true
  end

  # rubocop:disable Layout/LineLength
  # I do not want to push our luck with PayU by adding line breaks to this request string
  def compute_request_auth_hash
    Digest::SHA512.hexdigest(
      "#{key}|#{txnid}|#{amount}|#{productinfo}|#{firstname}|#{email}|||||#{txnid}||||||#{Payu::ApiClient::MERCHANT_SALT}"
    )
    # key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT is the pattern, according to https://devguide.payu.in/docs/developers-guide/checkout/encryption-2/
  end
  # rubocop:enable Layout/LineLength
end
