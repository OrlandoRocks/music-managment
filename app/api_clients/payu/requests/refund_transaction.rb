# frozen_string_literal: true

class Payu::Requests::RefundTransaction
  REFUND_TRANSACTION = "cancel_refund_transaction"
  URL = "/merchant/postservice.php?form=2"

  def initialize(options)
    @refund_id = options[:refund_id]
    @amount = options[:amount]
  end

  def url
    URL
  end

  def response_parser
    Payu::Responses::RefundTransaction
  end

  def request_method
    :post
  end

  def params
    {
      key: key,
      command: command,
      var1: payu_id,
      var2: @refund_id,
      var3: @amount,
      hash: compute_request_auth_hash,
    }
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def key
    Payu::ApiClient::MERCHANT_KEY
  end

  def command
    REFUND_TRANSACTION
  end

  def payu_id
    refund = Refund.find(@refund_id)
    refund.invoice.invoice_settlements
          .find_by(source_type: "PayuTransaction")
          .source
          .payu_id
  end

  def salt
    Payu::ApiClient::MERCHANT_SALT
  end

  def compute_request_auth_hash
    Digest::SHA512.hexdigest(
      "#{key}|#{command}|#{payu_id}|#{salt}"
    )
  end
end
