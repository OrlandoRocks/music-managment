class Payu::ApiClient
  BASE_URL          = ENV["PAYU_API_BASE_URL"]
  MERCHANT_KEY      = ENV["PAYU_MERCHANT_KEY"]
  MERCHANT_SALT     = ENV["PAYU_MERCHANT_SALT"]

  def initialize
    @http_client =
      Faraday.new(url: BASE_URL) do |f|
        f.request :multipart
        f.request :url_encoded
        f.adapter :net_http
      end
  end

  def send_request(req_type, options)
    @invoice_id = options[:invoice]&.id
    request = "Payu::Requests::#{req_type.to_s.classify}".constantize.new(options)
    response =
      @http_client.send(request.request_method, request.url, request.params) do |req|
        request.attach_request_params(req)
      end
    trace(response, request)
    request.response_parser.new(response)
  end

  private

  def trace(response, request)
    ApiCallTracer.call(response, @invoice_id, request.params, request.url)
  end
end
