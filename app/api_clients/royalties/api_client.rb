class Royalties::ApiClient
  def initialize
    @http_client = Faraday.new(url: url)
    @http_client.headers["Content-Type"] = "application/json"
  end

  def finalize(posting_ids)
    response = @http_client.post(url, params(posting_ids))

    return if response.success?

    notify_airbrake(posting_ids)
    raise
  end

  private

  def url
    ENV.fetch("SIP_BASE_URL") + "/api/tcwww/posting_complete"
  end

  def api_key
    ENV.fetch("SIP_API_KEY")
  end

  def params(posting_ids)
    { api_key: api_key, posting_ids: posting_ids }.to_json
  end

  def notify_airbrake(posting_ids)
    Airbrake.notify(
      "Unable to call SIP finalization API",
      {
        posting_ids: posting_ids
      }
    )
  end
end
