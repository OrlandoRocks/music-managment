class Payoneer::PayoutApiClientShim
  def self.fetch_for(current_user)
    if FeatureFlipper.show_feature?(:use_payoneer_v4_api, current_user)
      Payoneer::V4::PayoutApiClient
    else
      Payoneer::V2::PayoutApiClient
    end
  end
end
