# frozen_string_literal: true

class Payoneer::Responses::PayeeExtendedDetail
  attr_reader :body, :version, :status

  BUSINESS = "BUSINESS"
  COMPANY = "COMPANY"
  INDIVIDUAL = "INDIVIDUAL"
  SUCCESS = "Success"
  SUCCESS_STATUS_CODE = 200

  def initialize(raw_response, version = :v2)
    @version = version
    @body = Oj.load(raw_response.body).with_indifferent_access
    @status = raw_response.status if version == :v4
  end

  # TODO: Why some empty strings and some nil?
  def withdrawal_type
    case version
    when :v2
      body.dig(:payout_method, :type) || ""
    when :v4
      ""
    end
  end

  def withdrawal_currency
    case version
    when :v2
      body.dig(:payout_method, :currency) || ""
    when :v4
      ""
    end
  end

  def withdrawal_bank
    case version
    when :v2
      body.dig(:payout_method, :details, :bank_name) || ""
    when :v4
      ""
    end
  end

  def withdrawal_bank_account_type
    case version
    when :v2
      body.dig(:payout_method, :details, :account_type) || ""
    when :v4
      ""
    end
  end

  def withdrawal_details
    case version
    when :v2
      body.dig(:payout_method, :details)
    when :v4
      nil
    end
  end

  def country
    case version
    when :v2
      body.dig(:address, :country)
    when :v4
      body.dig(:result, :address, :country)
    end
  end

  def address_line_1
    case version
    when :v2
      body.dig(:address, :address_line_1)
    when :v4
      body.dig(:result, :address, :address_line_1)
    end
  end

  def address_line_2
    case version
    when :v2
      body.dig(:address, :address_line_2)
    when :v4
      body.dig(:result, :address, :address_line_2)
    end
  end

  def city
    case version
    when :v2
      body.dig(:address, :city)
    when :v4
      body.dig(:result, :address, :city)
    end
  end

  def state
    case version
    when :v2
      body.dig(:address, :state)
    when :v4
      body.dig(:result, :address, :state)
    end
  end

  def zip_code
    case version
    when :v2
      body.dig(:address, :zip_code)
    when :v4
      body.dig(:result, :address, :zip_code)
    end
  end

  def company_name
    case version
    when :v2
      body.dig(:company, :name)
    when :v4
      body.dig(:result, :contact, :first_name) if company?
    end
  end

  def first_name
    case version
    when :v2
      body.dig(:contact, :first_name)
    when :v4
      if company?
        body.dig(:result, :contact, :last_name)&.split&.first
      else
        body.dig(:result, :contact, :first_name)
      end
    end
  end

  def last_name
    case version
    when :v2
      body.dig(:contact, :last_name)
    when :v4
      if company?
        body.dig(:result, :contact, :last_name)&.split&.last
      else
        body.dig(:result, :contact, :last_name)
      end
    end
  end

  def tc_customer_type
    return BUSINESS.downcase if company?

    INDIVIDUAL.downcase if individual?
  end

  def company?
    case version
    when :v2
      body.dig(:type) == COMPANY
    when :v4
      body.dig(:result, :type) == COMPANY
    end
  end

  def individual?
    case version
    when :v2
      body.dig(:type) == INDIVIDUAL
    when :v4
      body.dig(:result, :type) == INDIVIDUAL
    end
  end

  def success?
    case version
    when :v2
      body.dig(:description) == SUCCESS
    when :v4
      status == SUCCESS_STATUS_CODE
    end
  end
end
