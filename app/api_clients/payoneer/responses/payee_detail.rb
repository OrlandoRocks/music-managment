class Payoneer::Responses::PayeeDetail
  attr_accessor :body, :version

  def initialize(json_response, version = :v2)
    @version = version
    @body    = JSON.parse(json_response.body)
  end

  def withdrawal_type
    case version
    when :v2
      (@body["payout_method"] && @body["payout_method"]["type"]) || ""
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def country
    case version
    when :v2
      (@body["address"] && @body["address"]["country"]) || ""
    when :v4
      # TODO: implement V4 Shim
    end
  end
end
