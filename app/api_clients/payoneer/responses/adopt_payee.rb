# frozen_string_literal: true

class Payoneer::Responses::AdoptPayee
  attr_reader :body, :version, :status

  SUCCESS_STATUS_CODE = 200
  ERROR_CODES = {
    v2: {
      payee_id_exists: 12_103
    },
    v4: {
      payee_id_exists: 2111
    }
  }

  def initialize(raw_response, version = :v2)
    @version = version
    @body = Oj.load(raw_response.body).with_indifferent_access
    @status = raw_response.status if version == :v4
  end

  def success?
    case version
    when :v2
      body[:description] == "Success"
    when :v4
      status == SUCCESS_STATUS_CODE
    end
  end

  def payee_id_exists?
    case version
    when :v2
      ERROR_CODES[:v2][:payee_id_exists] == body[:code]
    when :v4
      ERROR_CODES[:v4][:payee_id_exists] == body.dig(:error_details, :code)
    end
  end
end
