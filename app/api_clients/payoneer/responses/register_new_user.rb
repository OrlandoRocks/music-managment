class Payoneer::Responses::RegisterNewUser
  attr_reader :body, :version, :status

  SUCCESS_STATUS_CODE = 200

  def initialize(raw_response, version = :v2)
    @version = version
    @body = Oj.load(raw_response.body).with_indifferent_access
    @status = raw_response.status if version == :v4
  end

  def registration_link
    case version
    when :v2
      body.dig(:registration_link) || ""
    when :v4
      body.dig(:result, :registration_link) || ""
    end
  end

  def success?
    case version
    when :v2
      @body["description"] == "Success"
    when :v4
      status == SUCCESS_STATUS_CODE
    end
  end
end
