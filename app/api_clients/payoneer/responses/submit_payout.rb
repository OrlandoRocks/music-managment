class Payoneer::Responses::SubmitPayout
  attr_reader :body, :version

  def initialize(json_response, version = :v2)
    @version       = version
    @json_response = json_response
    @body          = json_response.body
    @parsed_body   = JSON.parse(@body)
  end

  def description
    case version
    when :v2
      @parsed_body["description"]
    when :v4
      nil
    end
  end

  def code
    case version
    when :v2
      @parsed_body["code"]
    when :v4
      nil
    end
  end

  def contents
    @body
  end

  def success?
    @json_response.success?
  end
end
