class Payoneer::Responses::PayeeStatus
  attr_accessor :body, :version

  def initialize(json_response, version = :v2)
    @version = version
    @body = Oj.load(json_response.body).with_indifferent_access
  end

  def withdrawal_type
    case version
    when :v2
      ""
    when :v4
      @body.dig(:result, :payout_method, :type) || ""
    end
  end

  def withdrawal_currency
    case version
    when :v2
      ""
    when :v4
      body.dig(:result, :payout_method, :currency) || ""
    end
  end
end
