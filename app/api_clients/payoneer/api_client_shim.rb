class Payoneer::ApiClientShim
  def initialize(person = nil)
    # rubocop:disable Lint/DuplicateBranch

    if FeatureFlipper.show_feature?(:use_payoneer_v4_api, person)
      # TODO: Create v4 version of the client, replace line below, its there to make tests pass
      Payoneer::V2::ApiClient.new
    else
      Payoneer::V2::ApiClient.new
    end
    # rubocop:enable Lint/DuplicateBranch
  end

  def self.fetch_for(current_user)
    # rubocop:disable Lint/DuplicateBranch

    if FeatureFlipper.show_feature?(:use_payoneer_v4_api, current_user)
      # TODO: Create v4 version of the client, replace line below, its there to make tests pass
      Payoneer::V2::ApiClient
    else
      Payoneer::V2::ApiClient
    end
    # rubocop:enable Lint/DuplicateBranch
  end
end
