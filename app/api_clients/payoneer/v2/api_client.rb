class Payoneer::V2::ApiClient
  class ApiError < StandardError; end
  BASE_URL = ENV["PAYONEER_API_BASE"]

  def initialize
    @http_client = Faraday.new(BASE_URL)
  end

  def post(params)
    response = execute_post(params)
    parse_response(response, params)
  end

  def execute_post(params)
    @http_client.post do |req|
      setup_request(req, params)
    end
  end

  def setup_request(req, params)
    req.headers["Content-Type"]  = "application/x-www-form-urlencoded"
    req.headers["authorization"] = "Basic #{ENV['PAYONEER_AUTH_SECRET']}"
    req.params                   = params.merge(self.class.credentials)
  end

  def parse_response(response, params)
    send("parse_#{params[:mname].underscore}_response", response.body, params)
  end

  def parse_initialize_tax_form_for_payee_response(response_body, params)
    body          = Hash.from_xml(response_body)
    response_code = body[params[:mname]]["Code"]
    raise_payoneer_error(body, params[:mname]) unless response_code == "000"
  end

  def parse_get_pending_tax_form_invited_payees_response(response_body, params)
    body = Hash.from_xml(response_body)
    payee_link_data = body["GetPendingTaxFormInvitedPayees"]["TaxFormLinks"]["Link"]
    case payee_link_data
    when Array
      process_links(payee_link_data, params[:PartnerPayeeID])
    when Hash
      process_link(payee_link_data)
    end
  end

  def process_links(links, payee_id)
    payee = links.reverse.find { |l| l["PayeeId"].to_s == payee_id.to_s }
    payee["LinkFormInviteUrl"].try(:split, "tk=").try(:last) if payee
  end

  def process_link(link)
    link["LinkFormInviteUrl"].try(:split, "tk=").try(:last)
  end

  def raise_payoneer_error(body, mname)
    raise ApiError,
          "Error communicating with Payoneer API.
      \nError Code:#{body[mname]['Code']}, Description: #{body[mname]['Description']}"
  end

  def self.credentials
    {
      p1: ENV["PAYONEER_CLIENT_USERNAME"],
      p2: ENV["PAYONEER_CLIENT_PASSWORD"],
      p3: ENV["PAYONEER_CLIENT_ID"]
    }
  end
end
