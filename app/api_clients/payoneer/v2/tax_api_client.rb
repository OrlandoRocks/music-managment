class Payoneer::V2::TaxApiClient
  include Payoneer::Configurable
  attr_reader :person, :program_id

  class ApiError < StandardError; end

  BASE_URL                  = ENV["PAYONEER_PAYOUT_API_BASE"]
  DEFAULT_URL               = "/Payouts/HttpApi/API.aspx".freeze
  DEFAULT_ROOT_ELEMENT_NAME = "PayoneerResponse".freeze

  attr_accessor :formatted_response, :response_code, :missing_params, :root_element_name

  def self.post(person, params)
    new(person: person).tap { |client| client.post(params) }
  end

  def initialize(person:, program_id: nil)
    @person = person
    @program_id = program_id
    @http_client = Faraday.new(BASE_URL)
  end

  # NOTE: The Payoneer Tax Form Collection API allows multiple date formats,
  #       including "2022-01-31" and "1/31/2022".
  #       Rails automatically converts Date objects to "yyyy-mm-dd" format.
  def post(params)
    raise_payoneer_error(description: missing_params_description) unless valid_params?(params)

    @root_element_name = params[:mname] || DEFAULT_ROOT_ELEMENT_NAME
    post_params = params.except(:unsanitized)
    response = @http_client.post(DEFAULT_URL, credentials.merge(post_params))
    trace_response = ApiCallTracer.call(response, person.id, post_params, DEFAULT_URL)
    parse_response(trace_response.body, post_params)
  end

  def missing_params_description
    "Missing or incorrect params: #{missing_params.join(', ')}"
  end

  def parse_response(response_body, params)
    @formatted_response =
      params[:unsanitized] ? Hash.from_xml(response_body) : desensitize_response(response_body)
    @response_code = response_root_element["Code"]

    # Uncomment the following if we want to raise an exception for all API errors.
    # raise_payoneer_error(error_params) unless response_code == "000"

    check_for_unauthorized_access
  end

  def raise_payoneer_error(params = {})
    base_message = "Error communicating with Payoneer API.\n"
    base_message += "Error Code: #{params[:code]}\n" if params[:code]
    base_message += "Description: #{params[:description]}" if params[:description]
    raise ApiError, base_message
  end

  def check_for_unauthorized_access
    description = response_root_element["Description"]
    raise ApiError, "Unauthorized Access" if description && description.include?("Unauthorized Access")
  end

  private

  def response_root_element
    formatted_response[root_element_name]
  end

  def error_params
    {
      code: response_root_element["Code"],
      description: response_root_element["Description"]
    }
  end

  def credentials
    {
      p1: payout_provider_config.username,
      p2: payout_provider_config.decrypted_password,
      p3: payout_provider_config.program_id,
    }
  end

  def valid_params?(params)
    @missing_params =
      [:mname, :FromDate, :ToDate, :PartnerPayeeID].select do |key|
        params[key].nil?
      end

    @missing_params.empty?
  end

  def desensitize_response(response_body)
    response = Hash.from_xml(response_body)
    if response[root_element_name] && response[root_element_name]["TaxForms"]
      tax_forms = response[root_element_name]["TaxForms"]["Form"]
      if tax_forms.is_a?(Array)
        tax_forms.each { |tax_form| clear_sensitive_info(tax_form) }
      else
        clear_sensitive_info(tax_forms)
      end
    end
    response
  end

  def clear_sensitive_info(tax_form)
    tax_form["TaxPayerIdType"] = ""
    tax_form["TaxPayerIdNumber"] = ""
  end
end
