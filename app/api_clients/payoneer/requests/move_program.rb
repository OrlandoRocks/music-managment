class Payoneer::Requests::MoveProgram
  class WrongEnvError < StandardError; end

  attr_reader :version

  def initialize(options = {}, version = :v2)
    raise WrongEnvError, "Sorry, this is not available in production" if Rails.env.production?

    @version        = version
    @person         = options[:person]
    @target_program = options[:target_program]
  end

  def url
    case version
    when :v2
      "/payees/#{person.payout_provider.client_payee_id}/move-program"
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def request_method
    case version
    when :v2
      :post
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def params
    case version
    when :v2
      { target_program_id: target_program.program_id }
    when :v4
      # TODO: implement V4 Shim
    end
  end

  private

  attr_reader :person, :target_program
end
