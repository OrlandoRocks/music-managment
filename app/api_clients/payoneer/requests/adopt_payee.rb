class Payoneer::Requests::AdoptPayee
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version
    @person = options[:person]
  end

  def url
    case version
    when :v2
      "/payees/#{person.client_payee_id}/adopt-payee-id"
    when :v4
      "/payees/adopt_payee"
    end
  end

  def request_method
    :post
  end

  def params
    case version
    when :v2
      {
        partner_id_to_adopt: person.payout_provider_config.program_id,
        payee_id_to_adopt: person.client_payee_id
      }
    when :v4
      {
        partner_id_to_adopt: person.payout_provider_config.program_id,
        payee_id_to_adopt: person.client_payee_id,
        calling_partner_payee_id: person.client_payee_id
      }
    end
  end

  private

  attr_reader :person
end
