class Payoneer::Requests::PayeeReport
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
  end

  def url
    case version
    when :v2
      "/reports/payee_details"
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def request_method
    case version
    when :v2
      :get
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def params
    case version
    when :v2
      {
        payee_id: person.client_payee_id
      }
    when :v4
      # TODO: implement V4 Shim
    end
  end

  private

  attr_reader :person
end
