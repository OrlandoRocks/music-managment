class Payoneer::Requests::RegisterExistingUser
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
    @redirect_url = options[:redirect_url]
  end

  def url
    case version
    when :v2
      "/payees/login-link"
    when :v4
      "/payees/registration-link"
    end
  end

  def request_method
    :post
  end

  def params
    case version
    when :v2
      {
        payee_id: person.client_payee_id,
        language_id: Payoneer::V2::PayoutApiClient::LANGUAGE_MAP[person.locale],
        redirect_url: redirect_url,
        redirect_time: 0,
      }
    when :v4
      {
        payee_id: person.client_payee_id,
        language_id: Payoneer::V4::PayoutApiClient::LANGUAGE_MAP[person.locale],
        redirect_url: redirect_url,
        already_have_an_account: true
      }
    end
  end

  private

  attr_reader :person, :redirect_url
end
