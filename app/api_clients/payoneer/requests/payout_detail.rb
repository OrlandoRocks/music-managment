class Payoneer::Requests::PayoutDetail
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @transfer = options[:transfer]
  end

  def url
    case version
    when :v2
      "/payouts/#{transfer.client_reference_id}"
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def request_method
    case version
    when :v2
      :get
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def params
    case version
    when :v2
      {}
    when :v4
      # TODO: implement V4 Shim
    end
  end

  private

  attr_reader :transfer
end
