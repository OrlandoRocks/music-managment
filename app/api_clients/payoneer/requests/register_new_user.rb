class Payoneer::Requests::RegisterNewUser
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
    @redirect_url = options[:redirect_url]
    @payout_methods_list = options[:payout_methods_list]
  end

  def url
    "/payees/registration-link"
  end

  def request_method
    :post
  end

  def params
    case version
    when :v2
      v2_params
    when :v4
      v4_params
    end
  end

  private

  def v2_params
    {
      payee_id: person.client_payee_id,
      language_id: Payoneer::V2::PayoutApiClient::LANGUAGE_MAP[person.locale],
      type: "INDIVIDUAL",
      redirect_url: redirect_url,
      contact: {
        email: person.email
      },
      address: {
        country: person.country,
        state: person.state,
        address_line_1: person.address1,
        address_line_2: person.address2,
        city: person.city,
        zip_code: person.zip
      },
      payout_methods_list: [payout_methods_list].compact
    }
  end

  def v4_params
    {
      payee_id: person.client_payee_id,
      language_id: Payoneer::V4::PayoutApiClient::LANGUAGE_MAP[person.locale],
      redirect_url: redirect_url,
      payee: {
        type: "Individual",
        contact: {
          email: person.email
        },
        address: {
          country: person.country_iso_code,
          state: person.state,
          address_line_1: person.address1,
          address_line_2: person.address2,
          city: person.city,
          zip_code: person.zip
        }
      },
      payout_methods: [payout_methods_list].compact
    }
  end

  attr_reader :person, :redirect_url, :payout_methods_list
end
