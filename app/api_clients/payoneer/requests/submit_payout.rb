class Payoneer::Requests::SubmitPayout
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version
    @person   = options[:person]
    @transfer = options[:transfer]
  end

  def url
    case version
    when :v2
      "/payouts"
    when :v4
      "/masspayouts"
    end
  end

  def request_method
    :post
  end

  def params
    case version
    when :v2
      {
        payee_id: person.client_payee_id,
        amount: formatted_amount_to_transfer,
        client_reference_id: transfer.client_reference_id,
        description: "TuneCore payout transaction",
        group_id: person.id
      }
    when :v4
      {
        Payments: [
          {
            payee_id: person.client_payee_id,
            amount: formatted_amount_to_transfer,
            client_reference_id: transfer.client_reference_id,
            description: "TuneCore payout transaction",
            group_id: person.id
          }
        ]
      }
    end
  end

  private

  def formatted_amount_to_transfer
    # Payoneer expects the amount with a period/dot decimal separator as per the international convention.
    I18n.with_locale(COUNTRY_LOCALE_MAP["US"]) { transfer.amount_to_transfer.to_s }
  end

  attr_reader :person, :transfer
end
