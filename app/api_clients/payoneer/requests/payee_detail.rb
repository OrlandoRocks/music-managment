class Payoneer::Requests::PayeeDetail
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
  end

  def url
    case version
    when :v2
      "/payees/#{person.payout_provider.client_payee_id}/details"
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def request_method
    case version
    when :v2
      :get
    when :v4
      # TODO: implement V4 Shim
    end
  end

  def params
    case version
    when :v2
      {}
    when :v4
      # TODO: implement V4 Shim
    end
  end

  private

  attr_reader :person
end
