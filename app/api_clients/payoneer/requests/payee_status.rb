class Payoneer::Requests::PayeeStatus
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
  end

  def url
    "/payees/#{person.client_payee_id}/status"
  end

  def request_method
    :get
  end

  def params
    {}
  end

  private

  attr_reader :person
end
