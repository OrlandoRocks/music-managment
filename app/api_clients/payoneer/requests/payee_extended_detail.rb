class Payoneer::Requests::PayeeExtendedDetail
  attr_reader :version

  def initialize(options = {}, version = :v2)
    @version = version

    @person = options[:person]
  end

  def url
    case version
    when :v2
      "/payees/#{person.payout_provider.client_payee_id}/extended_details"
    when :v4
      "/payees/#{person.payout_provider.client_payee_id}/details"
    end
  end

  def request_method
    :get
  end

  def params
    {}
  end

  private

  attr_reader :person
end
