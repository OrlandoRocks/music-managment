module Payoneer
  module Configurable
    extend ActiveSupport::Concern
    # there are four potential avenues for arriving at the payout_provider_config
    #
    # if bi_transfer is turned off
    #   we will never get a program_id (because the modal that provides this is also behind this feature flag),
    #   and so every call will use the else statement
    #   the else statement finds the Payoneer config for TuneCore US by country website and environment
    #
    # if bi_transfer is turned on
    #   if we are given a program_id, it means that the user selected US Resident/non-US resident from the modal on
    #   signing up for Payoneer. In that case, we want to send them to Payoneer with the program associated with that
    #   selection
    #
    #   if the payout_provider_config has not been set on the user's payout_provider, we send them to Payoneer with the
    #   program associated with the user's self-identified country and country_website currency
    #
    #   when the user goes to Payoneer we set a payout_provider_config on their payout_provider
    #   if that has been set, we want to use that config.
    #     The reason we have this mechanism is in the instance where their US/non US resident selection differs from the
    #   automatic selection based on the user's self-identified country. When Payoneer sends us approval, we attempt
    #   to find that payee using an api call in order to update, and lock, the user's address information. If we relied
    #   on the automatic selection based on the user's self-identified country, we would be unable to find the payee
    #   because we would be looking for them in the wrong Payoneer program. Therefore, we want to use whatever program
    #   we last used to send them to Payoneer, which is stored on the payout_provider.
    #
    #
    def payout_provider_config
      return @payout_provider_config unless @payout_provider_config.nil?

      @payout_provider_config =
        if program_id.present?
          PayoutProviderConfig.by_env.find_by(program_id: program_id)
        elsif FeatureFlipper.show_feature?(:bi_transfer, person)
          bi_transfer_payout_provider_config
        else
          PayoutProviderConfig.by_env.find_by(
            name: PayoutProviderConfig::PAYONEER_NAME,
            corporate_entity: CorporateEntity.find_by_name(CorporateEntity::TUNECORE_US_NAME),
            currency: config_currency
          )
        end
    end

    def bi_transfer_payout_provider_config
      person_payout_provider_config = person.payout_provider_config
      if person_payout_provider_config.present?
        return PayoutProviderConfig.by_env.find_by(program_id: person_payout_provider_config.program_id)
      end

      PayoutProviderConfig.by_env.find_by(
        name: PayoutProviderConfig::PAYONEER_NAME,
        corporate_entity: person_corporate_entity,
        currency: config_currency
      )
    end

    def person_corporate_entity
      # we have to use the default and guard clause because corporate_entity_id on the people table
      # currently allows null - a future ticket will resolve that, and make the default below unnecessary
      return person.corporate_entity if person.corporate_entity.present?

      Country.find_by_iso_code(person.attributes["country"]).corporate_entity
    end

    def config_currency
      person.payout_provider&.currency || person.site_currency
    end
  end
end
