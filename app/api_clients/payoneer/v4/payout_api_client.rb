# frozen_string_literal: true

class Payoneer::V4::PayoutApiClient
  include Payoneer::Configurable
  attr_reader :person, :program_id

  ERROR_CODES = {
    insufficient_balance: 10_301,
    invalid_currency: 10_302,
    currency_unavailable_for_payee: 10_303,
    payout_already_exists: 10_304,
    partner_as_payee_payout_to_same_partner_forbidden: 10_307,
    negative_payout_amount: 10_308,
    sending_payouts_not_enabled: 10_309,
    min_limit_not_met: 10_403,
    max_limit_exceeded: 10_404
  }.with_indifferent_access.freeze

  BASE_URL = ENV["PAYONEER_PAYOUT_API_BASE"]
  API_BASE = "/v4/programs/"
  REQUEST_TEXT = "request"
  RESPONSE_TEXT = "response"

  ENGLISH_ID = 1
  GERMAN_ID = 13
  FRENCH_ID = 9
  ITALIAN_ID = 20

  LANGUAGE_MAP = {
    USA_LOCALE => ENGLISH_ID,
    CANADA_LOCALE => ENGLISH_ID,
    GREAT_BRITAIN_LOCALE => ENGLISH_ID,
    AUSTRALIA_LOCALE => ENGLISH_ID,
    GERMANY_LOCALE => GERMAN_ID,
    FRANCE_LOCALE => FRENCH_ID,
    ITALY_LOCALE => ITALIAN_ID
  }.freeze

  def initialize(person:, program_id: nil)
    @person = person
    @program_id = program_id
    @http_client =
      Faraday.new(BASE_URL) do |conn|
        conn.request :authorization, "Bearer", Payoneer::V4::TokenGenerationService.fetch_token
        conn.request :retry, retry_options
        conn.adapter Faraday.default_adapter
      end
    @http_client.headers["Content-Type"] = "application/json"
  end

  def send_request(request_name, options = {})
    referrer_url = options.delete(:referrer_url)
    request = "Payoneer::Requests::#{request_name.to_s.classify}".constantize.new(options, :v4)

    case request.request_method
    when :post then post_response(request.url, request.params, request_name, referrer_url)
    when :get then get_response(request.url, request.params)
    end
  end

  private

  attr_reader :http_client

  def build_url(url)
    API_BASE + payout_provider_config.program_id.to_s + url
  end

  def post_response(url, params, request_name, referrer_url = nil)
    build_url = build_url(url)
    response =
      http_client.post(build_url) do |req|
        req.body = params.to_json
        log_request(req.marshal_dump, params, request_name, referrer_url)
        raise_on_malformed_payout_amount(request_name, params, referrer_url)
      end

    log_response(response.body, params, request_name, referrer_url)
    trace(response, params, build_url)
  end

  def get_response(url, params)
    build_url = build_url(url)
    response = http_client.get(build_url) { |req| req.params = params }

    trace(response, params, build_url)
  end

  def trace(response, params, build_url)
    ApiCallTracer.call(response, person.id, params, build_url)
  end

  def retry_options
    {
      max: 2,
      interval: 5,
      interval_randomness: 0.5,
      backoff_factor: 2,
      retry_block: ->(_env, _options, retries, exc) {
                     Rails.logger.info "Faraday Error! Retrying in 5 seconds. Retry number #{retries}. Error #{exc}"
                   },
      exceptions: [Faraday::ConnectionFailed]
    }
  end

  def log_request(request_body, params, request_name, referrer_url = nil)
    case request_name
    when :submit_payouts
      transfer = PayoutTransfer.find_by(client_reference_id: params[:Payments][0][:client_reference_id])
      transfer.payout_transfer_api_logs.create!(kind: REQUEST_TEXT, body: request_body, url: referrer_url)
      Rails.logger.info "*** Payout Transfer Request *** "\
        "Person ID: #{person.id}, "\
        "Transfer ID: #{transfer.id}, "\
        "URL: #{referrer_url}, "\
        "Params: #{params}"
    end
  end

  def log_response(response_body, params, request_name, referrer_url = nil)
    case request_name
    when :submit_payouts
      transfer = PayoutTransfer.find_by(client_reference_id: params[:Payments][0][:client_reference_id])
      transfer.payout_transfer_api_logs.create!(kind: RESPONSE_TEXT, body: response_body, url: referrer_url)
      Rails.logger.info "*** Payout Transfer Response *** "\
        "Person ID: #{person.id}, "\
        "Transfer ID: #{transfer.id}, "\
        "URL: #{referrer_url}, "\
        "Response: #{response_body}"
    end
  end

  def raise_on_malformed_payout_amount(request_name, params, referrer_url)
    case request_name
    when :submit_payouts
      if serialized_amount_malformed?(params)
        Airbrake.notify(
          "Malformed amount in payout transfer API request",
          {
            person_id: person.id,
            client_reference_id: params[:client_reference_id],
            amount: params[:amount],
            referrer_url: referrer_url
          }
        )
        raise "Malformed amount in payout transfer API request"
      end
    end
  end

  def serialized_amount_malformed?(params)
    JSON.parse(params.to_json)["Payments"][0]["amount"]&.include?(",")
  end
end
