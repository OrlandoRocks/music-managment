# frozen_string_literal: true

# https://developer.payoneer.com/docs/mass-payouts-v4.html#/9514e3ef96c62-requesting-an-application-token
class Payoneer::V4::ApiTokenGenerationClient
  class ClientIDMissingError < StandardError; end

  class ClientSecretMissingError < StandardError; end

  class ClientLoginUrlMissingError < StandardError; end

  class ApiError < StandardError; end

  # Request a token and yields the resp to the caller
  def self.request_token
    new.request_token do |resp|
      yield resp
    end
  end

  # Revokes a token and yields the resp to the caller
  def self.revoke_token
    new.revoke_token do |resp|
      yield resp
    end
  end

  # Example
  # curl -X POST \
  #    https://login.sandbox.payoneer.com/api/v2/oauth2/token \
  #   -H 'Authorization: Basic OWRkNxYmE2OW...nFNOHNNN2FR' \
  #   -H 'Content-Type: application/x-www-form-urlencoded' \
  #   -d 'grant_type=client_credentials&scope=read write'
  def request_token
    response =
      http_client.post("/api/v2/oauth2/token") do |request|
        request.body = {
          grant_type: "client_credentials",
          scope: "read write"
        }
      end

    raise ApiError.new(response.body) unless response.success?

    yield Oj.load(response.body).with_indifferent_access
  end

  # POST  [Authorization URL]/api/v2/oauth2/revoke
  # https://login.sandbox.payoneer.com/api/v2/oauth2/revoke \
  # -H 'Authorization: Basic OWRkNxYmE2OW...nFNOHNNN2FR' \
  # -H 'Content-Type: application/x-www-form-urlencoded' \
  # -d 'token_type_hint=access_token&token=AAJ0zlhgWgsfU275...hR33JcQKT'
  def revoke_token
    response =
      http_client.post("/api/v2/oauth2/revoke") do |request|
        request.body = {
          token_type_hint: "access_token",
          token: Rails.cache.fetch(cache_key)
        }
      end

    raise ApiError.new(response.body) unless response.success?

    yield Oj.load(response.body).with_indifferent_access
  end

  private

  def http_client
    @http_client ||=
      Faraday.new(
        url: payoneer_v4_base_url,
        headers: {
          "Authorization" => "Basic #{encoded_secret}",
          "Content-Type" => "application/x-www-form-urlencoded"
        }
      )
  end

  # Secret must be encoded in base64 as per the API docs
  def encoded_secret
    @encoded_secret ||=
      Base64.strict_encode64("#{client_id}:#{client_secret}").strip
  end

  def client_id
    ENV.fetch("PAYONEER_V4_CLIENT_ID") do
      raise ClientIDMissingError.new(
        "ENV missing, expected key: PAYONEER_V4_CLIENT_ID, to define it"
      )
    end
  end

  def client_secret
    ENV.fetch("PAYONEER_V4_CLIENT_SECRET") do
      raise ClientSecretMissingError.new(
        "ENV missing, expected key: PAYONEER_V4_CLIENT_SECRET, to define it"
      )
    end
  end

  def payoneer_v4_base_url
    ENV.fetch("PAYONEER_V4_LOGIN_URL") do
      raise ClientLoginUrlMissingError.new(
        "ENV missing, expected key: PAYONEER_V4_LOGIN_URL, to define it"
      )
    end
  end

  def cache_key
    Payoneer::V4::TokenGenerationService::CACHE_KEY
  end
end
