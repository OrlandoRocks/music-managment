# frozen_string_literal: true

module Bytedance
  class ApiClient
    BYTEDANCE_BASE_URL = ENV.fetch("BYTEDANCE_BASE_URL", "https://www.soundon.global/open-api/v1/")

    REVIEW_AUDIT_STATE_MAP = {
      ReviewAudit::APPROVED => "approved",
      ReviewAudit::NEEDS_REVIEW => "inprocess",
      ReviewAudit::REJECTED => "rejected",
      ReviewAudit::FLAGGED => "inprocess",
      ReviewAudit::NOT_SURE => "inprocess",
      ReviewAudit::SKIPPED => "inprocess",
      ReviewAudit::STARTED_REVIEW => "inprocess",
    }.freeze

    EXPIRE_TIME = 600
    VERSION = "auth-v1"

    attr_reader :request, :http_client

    def initialize(request_klass, review_audit_id, album_id)
      @request = request_klass.constantize.new(review_audit_id, album_id)
      @http_client = set_http_client
    end

    def post
      response = http_client.post(request.url, request.params.to_json)

      return response if response.success?

      notify_airbrake(response)

      response
    end

    private

    def set_http_client
      @http_client =
        Faraday.new(BYTEDANCE_BASE_URL) do |conn|
          conn.headers["Content-Type"] = "application/json"
          conn.headers["Agw-Auth"] = auth_header
          conn.adapter Faraday.default_adapter
        end
    end

    def auth_header
      bd_access_key = ENV.fetch("BD_ACCESS_KEY")
      bd_secret_key = ENV.fetch("BD_SECRET_KEY")
      sign_key_info = "#{VERSION}/#{bd_access_key}/#{timestamp}/#{EXPIRE_TIME}"
      sign_key = sha256_hmac(bd_secret_key, sign_key_info)
      signature = sha256_hmac(sign_key, request.params.to_json)
      "#{sign_key_info}/#{signature}"
    end

    def timestamp
      @timestamp ||= DateTime.now.to_i
    end

    def sha256_hmac(key, data)
      digest = OpenSSL::Digest.new("SHA256")
      OpenSSL::HMAC.hexdigest(digest, key, data)
    end

    def notify_airbrake(response)
      Airbrake.notify(
        "Unable to send Bytedance API post request to #{request.url} endpoint",
        {
          album_id: request.params[:tc_album_id],
          source_album_id: request.params[:source_id],
          error: response.body
        }
      )
    end
  end
end
