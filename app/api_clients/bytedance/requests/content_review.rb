module Bytedance::Requests
  class DistributionApiAlbumNotFound < StandardError; end

  class ContentReview
    def initialize(review_audit_id, album_id)
      @review_audit = ReviewAudit.find_by(id: review_audit_id)
      retry_api_client_worker && return if @review_audit.blank?

      @album = Album.find_by(id: album_id)
      @distribution_api_album = @album.distribution_api_album

      raise_distribution_api_error_not_found_error if @distribution_api_album.blank?
    end

    def url
      "review"
    end

    def params
      {
        tc_album_id: @album.id,
        source_id: @distribution_api_album.source_album_id,
        upc: @album.upc.number,
        state: review_audit_state,
        time: @review_audit.updated_at,
        content_review_notes: @review_audit.note,
        errors: build_album_errors,
        songs: build_songs
      }
    end

    private

    def review_audit_state
      ::Bytedance::ApiClient::REVIEW_AUDIT_STATE_MAP.fetch(@review_audit.event, nil)
    end

    def build_album_errors
      return [] unless @review_audit.event == ReviewAudit::REJECTED

      @review_audit.review_reasons.map do |review_reason|
        {
          error: review_reason.id,
          error_message: review_reason.reason
        }
      end
    end

    def build_songs
      @distribution_api_album.distribution_api_songs.map do |distribution_api_song|
        {
          tc_song_id: distribution_api_song.song_id,
          source_song_id: distribution_api_song.source_song_id,
          isrc: distribution_api_song.song.isrc
        }
      end
    end

    def raise_distribution_api_error_not_found_error
      raise DistributionApiAlbumNotFound, "DistributionApiAlbum record related to Album was not found"
    end

    def retry_api_client_worker(review_audit_id, album_id)
      Bytedance::ApiClientWorker.perform_in(5.minutes, Bytedance::Requests::ContentReview, review_audit_id, album_id)
    end
  end
end
