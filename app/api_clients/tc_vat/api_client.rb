# frozen_string_literal: true

module TcVat
  # A Service that can be used to communicate to the vat_service.
  class ApiClient
    TC_VAT_SERVICE_COMPLETE_URL = %(#{ENV.fetch('TC_VAT_SERVICE_URL')}/api/v1/vat_services/)

    def initialize
      @http_client =
        Faraday.new(url: TC_VAT_SERVICE_COMPLETE_URL) do |conn|
          conn.request :url_encoded
          conn.adapter Faraday.default_adapter
        end
    end

    def send_request!(request_name, options = {})
      request = "TcVat::Requests::#{request_name.to_s.classify}".constantize.new(options)

      call_service!(request)
    end

    private

    def call_service!(request)
      response_text =
        @http_client.send(request.request_method, request.url, request.params) do |req|
          attach_headers(req)
        end
      request.response_class.new(response_text)
    rescue Faraday::ConnectionFailed, JSON::ParserError
      raise TcVat::Requests::VatServiceDownError
    end

    def attach_headers(req)
      req.headers["content-type"] = "application/json"
      req.headers["api-key"] = ENV["TC_VAT_SERVICE_API_KEY"]
    end
  end
end
