module TcVat::Responses
  class Calculate < Base
    def place_of_supply
      @place_of_supply ||= @body["place_of_supply"]
    end

    def tax_rate
      @tax_rate ||= @body["tax_rate"].to_f
    end

    def tax_type
      @tax_type ||= @body["tax_type"]
    end
  end
end
