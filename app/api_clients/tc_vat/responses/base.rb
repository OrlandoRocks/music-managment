# frozen_string_literal: true

module TcVat::Responses
  class Base
    def initialize(response)
      @data = JSON.parse(response.body)
      @body = @data["data"]
      @errors = Array(@data["errors"] || @data["error"])
    end

    def response_valid?
      @errors.blank?
    end

    def error_message
      return if response_valid?

      @error_message ||= @errors.join(", ")
    end
  end
end
