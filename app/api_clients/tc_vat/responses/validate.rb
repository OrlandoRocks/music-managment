# frozen_string_literal: true

module TcVat::Responses
  class Validate < Base
    SERVICE_ERRORS = %W[
      SERVICE_UNAVAILABLE
      MS_UNAVAILABLE
      TIMEOUT
      IP_BLOCKED
      GLOBAL_MAX_CONCURRENT_REQ
      GLOBAL_MAX_CONCURRENT_REQ_TIME
      MS_MAX_CONCURRENT_REQ
      MS_MAX_CONCURRENT_REQ_TIME
    ]

    def initialize(response)
      super
      validate_response!
    end

    def valid?
      @valid ||= (@body["valid"] || false)
    end

    def vat_number
      @vat_number ||= @body["vat_number"]
    end

    def name
      @name ||= @body["name"]
    end

    def country
      @country ||= @body["country"]
    end

    def address
      @address ||= @body["address"]
    end

    def error_code
      @error_code ||= @data["vies_error_code"]
    end

    private

    def validate_response!
      return unless (vies_down? || any_soap_faults?)

      raise TcVat::Requests::ViesDownError, error_message
    end

    def vies_down?
      return false if response_valid?

      error_message.include?("Invalid response from VIES")
    end

    def any_soap_faults?
      return false if error_code.blank?

      SERVICE_ERRORS.include? error_code
    end
  end
end
