# frozen_string_literal: true

module TcVat::Requests
  class Validate
    VALIDATE_URL = "validate_vat_number"

    attr_accessor :vat_registration_number, :country

    def initialize(options = {})
      @vat_registration_number = options[:vat_registration_number]
      @country = options[:country]
    end

    def url
      VALIDATE_URL
    end

    def request_method
      :post
    end

    def response_class
      TcVat::Responses::Validate
    end

    def params
      { vat_registration_number: vat_registration_number, country: country }.to_json
    end
  end
end
