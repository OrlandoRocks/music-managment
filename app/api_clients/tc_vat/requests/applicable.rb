# frozen_string_literal: true

module TcVat::Requests
  class Applicable
    OUTBOUND = "Outbound"
    APPLICABILITY_URL = "vat_applicability"

    attr_accessor :transaction_type

    def initialize(options = {})
      @transaction_type = options.fetch(:transaction_type, OUTBOUND)
    end

    def request_method
      :get
    end

    def url
      APPLICABILITY_URL
    end

    def response_class
      TcVat::Responses::Applicable
    end

    def params
      {
        transaction_type: transaction_type
      }
    end
  end
end
