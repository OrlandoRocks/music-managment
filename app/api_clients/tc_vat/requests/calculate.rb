# frozen_string_literal: true

module TcVat::Requests
  class Calculate
    CALCULATE_URL = "calculate_vat"

    attr_accessor :country, :transaction_type, :customer_type, :corporate_entity

    def initialize(options = {})
      @country = options[:country]
      @transaction_type = options[:transaction_type]
      @customer_type = options[:customer_type]
      @corporate_entity = options[:corporate_entity]
    end

    def url
      CALCULATE_URL
    end

    def request_method
      :post
    end

    def response_class
      TcVat::Responses::Calculate
    end

    def params
      {
        country: country,
        transaction_type: transaction_type,
        customer_type: customer_type,
        corporate_entity: corporate_entity
      }.to_json
    end
  end
end
