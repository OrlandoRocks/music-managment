class ApiCallTracer
  attr_reader :response, :person_id, :request_params, :url

  def self.call(response, person_id, request_params, url)
    new(response, person_id, request_params, url).call
  end

  def initialize(response, person_id, request_params, url)
    @response = response
    @person_id = person_id
    @request_params = request_params
    @url = url
  end

  def call
    case status
    when 200..299
      handle_good_response
    else
      handle_bad_response
    end

    response
  end

  private

  def handle_good_response
    Rails.logger.info "API Client Call Response : #{response.body}"
  end

  def handle_bad_response
    Rails.logger.info trace_exception
    Tunecore::Airbrake.notify(trace_exception)
  end

  def status
    @status ||= response.status
  end

  def trace_exception
    return if @trace_exception

    @trace_exception = "
      API Client Call Failed: #{response.body}
      Person_Id: #{person_id}
      Request_Params: #{request_params}
      URL: #{url}"
  end
end
