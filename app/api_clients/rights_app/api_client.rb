class RightsApp::ApiClient
  REQUEST_PATH  = "RightsApp::Requests".freeze
  RESPONSE_PATH = "RightsApp::Responses".freeze
  SESSION_TOKEN = "RightsApp::SessionToken".freeze

  attr_reader :e_tag, :http_client, :token, :res

  def initialize
    set_client
    build_content_type_header if http_client
  end

  def build_content_type_header
    http_client.headers["Content-Type"] = "application/json"
  end

  def build_headers
    http_client.headers["Authorization"]  = $redis.get(SESSION_TOKEN)
    http_client.headers["If-Match"]       = e_tag if e_tag.present?
  end

  def build_path(name, type)
    "#{type}::#{name.to_s.classify}".constantize
  end

  def login
    request = ::RightsApp::Requests::Login.new
    @res    = post(request.url, request.params)

    set_auth_token
  end

  def send_request(request_name, options = {}, attempts = 0)
    login if $redis.get(SESSION_TOKEN).nil?

    @e_tag = options[:e_tag]
    build_headers

    request = build_path(request_name, REQUEST_PATH).new(options)
    response = send(
      request.request_method,
      request.url,
      request.params
    )

    if response.status == 401 && attempts < 2
      login
      return send_request(request_name, options, attempts + 1)
    elsif response.status == 500 && attempts < 3
      sleep 2**attempts
      return send_request(request_name, options, attempts + 1)
    end

    parsed_response = request.response_class.new(response)

    PublishingAdministration::ErrorService.log_errors(request, parsed_response) if should_raise_error?(parsed_response)

    parsed_response
  end

  def set_client
    return unless !!RIGHTS_APP_BASE_URL && !!RIGHTS_APP_API_VERSION

    @http_client = Faraday.new(RIGHTS_APP_BASE_URL + RIGHTS_APP_API_VERSION)
  end

  private

  def get(url, params)
    http_client.get(url) do |req|
      req.params = params
    end
  end

  def post(url, params)
    http_client.post(url) do |req|
      req.body = params.to_json
    end
  end

  def put(url, params)
    http_client.put(url) do |req|
      req.body = params.to_json
    end
  end

  def invalid_api_session?(status)
    [401, 500].include?(status)
  end

  def set_auth_token
    token = "Basic #{JSON.parse(res.body)['Token']}"
    $redis.set(SESSION_TOKEN, token) unless $redis.nil?
  end

  def should_raise_error?(response)
    return false if response.messages.blank?
    return false if response.respond_to?(:already_exists?) && response.already_exists?
    return false if response.respond_to?(:empty_list?) && response.empty_list?

    true
  end
end
