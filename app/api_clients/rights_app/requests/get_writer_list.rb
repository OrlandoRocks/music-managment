class RightsApp::Requests::GetWriterList
  def initialize(opts = {})
    @artist_account_code  = opts[:artist_account_code]
    @page                 = opts[:page]
  end

  def url
    "Writers/#{@artist_account_code}"
  end

  def request_method
    :get
  end

  def params
    { Page: @page, PageSize: 100 }
  end

  def requestable
    @account ||= Account.find_by(provider_account_id: @artist_account_code)
  end

  def response_class
    RightsApp::Responses::WriterList
  end
end
