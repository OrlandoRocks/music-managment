class RightsApp::Requests::GetWorkList
  def initialize(opts)
    @artist_account_code  = opts[:artist_account_code]
    @page                 = opts[:page]
  end

  def url
    "Works/#{@artist_account_code}"
  end

  def request_method
    :get
  end

  def params
    { Page: @page, PageSize: 100 }
  end

  def requestable
    @requestable ||= Account.find_by(provider_account_id: @artist_account_code)
  end

  def response_class
    RightsApp::Responses::WorkList
  end
end
