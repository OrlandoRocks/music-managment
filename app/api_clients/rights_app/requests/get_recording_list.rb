class RightsApp::Requests::GetRecordingList
  def initialize(opts)
    @composition  = opts[:composition]
    @page         = opts[:page]
    @work_code    = opts[:work_code] || @composition.provider_identifier
  end

  def url
    "Recordings/#{@work_code}"
  end

  def request_method
    :get
  end

  def params
    { Page: @page, PageSize: 100 }
  end

  def requestable
    @composition
  end

  def response_class
    RightsApp::Responses::RecordingList
  end
end
