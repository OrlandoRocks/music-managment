class RightsApp::Requests::CreateWriter
  include PublishingAdministrationHelper

  def initialize(writer)
    @writer = writer
  end

  def url
    "Writers"
  end

  def request_method
    :post
  end

  def params
    sentric_writer_params(@writer)
  end

  def requestable
    @writer
  end

  def response_class
    RightsApp::Responses::Writer
  end
end
