class RightsApp::Requests::Login
  def initialize(opts = {})
    @opts = opts
  end

  def url
    "Login"
  end

  def request_method
    :post
  end

  def params
    {
      TenantApiKey: RIGHTS_APP_TENANT_ID,
      Username: RIGHTS_APP_USERNAME,
      Password: RIGHTS_APP_PASSWORD
    }
  end

  def requestable
    nil
  end

  def response_class
    nil
  end
end
