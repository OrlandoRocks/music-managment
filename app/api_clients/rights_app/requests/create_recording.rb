class RightsApp::Requests::CreateRecording
  include PublishingAdministrationHelper
  RIGHTS_APP_CHAR_LIMIT = 60
  ARTIST_MAX_CHAR = 45

  def initialize(opts)
    @composition  = opts[:composition]
    @song         = opts[:song]
    @ntc_song     = opts[:non_tunecore_song]
    @album        = @song.try(:album) || @ntc_song.try(:non_tunecore_album)
  end

  def url
    "Recordings"
  end

  def request_method
    :post
  end

  def params
    {
      WorkCode: @composition.provider_identifier,
      Title: sanitize_name(@composition.clean_composition_name.first(RIGHTS_APP_CHAR_LIMIT)),
      PerformingArtist: sanitize_name(performing_artist),
      Isrc: isrc,
      RecordLabel: sanitize_name(label),
      ReleaseDate: @album.orig_release_year
    }
  end

  def requestable
    @composition
  end

  def response_class
    RightsApp::Responses::Recording
  end

  private

  def isrc
    @song.try(:isrc) || @ntc_song.try(:isrc)
  end

  def label
    @album.is_a?(Album) ? @album.label_name : @album.record_label.to_s
  end

  def performing_artist
    artist = @song.try(:artist) || @ntc_song.try(:artist)
    artist_name = artist.try(:name).to_s

    artist_name[0, ARTIST_MAX_CHAR]
  end
end
