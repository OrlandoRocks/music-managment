class RightsApp::Requests::GetWriter
  def initialize(opts = {})
    @writer       = opts[:writer]
    @writer_code  = opts[:writer_code]
    @account      = opts[:account] || @writer.account
    @publishing_changeover_enabled = opts[:publishing_changeover_enabled]
  end

  def url
    "Writers/#{writer_code}/#{@account.provider_account_id}"
  end

  def request_method
    :get
  end

  def params
    {}
  end

  def requestable
    @requestable ||=
      if @publishing_changeover_enabled
        (@writer || PublishingComposer.find_by(provider_identifier: @writer_code))
      else
        (@writer || Composer.find_by(provider_identifier: @writer_code))
      end
  end

  def response_class
    RightsApp::Responses::Writer
  end

  private

  def writer_code
    @writer_code || @writer.provider_identifier
  end
end
