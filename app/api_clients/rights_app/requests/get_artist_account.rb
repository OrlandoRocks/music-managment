class RightsApp::Requests::GetArtistAccount
  def initialize(opts = {})
    @account = opts[:account]
  end

  def url
    "ArtistAccount/#{@account.provider_account_id}"
  end

  def request_method
    :get
  end

  def params
    {}
  end

  def requestable
    @account
  end

  def response_class
    RightsApp::Responses::ArtistAccount
  end
end
