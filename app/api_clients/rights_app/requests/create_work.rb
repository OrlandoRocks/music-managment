class RightsApp::Requests::CreateWork
  attr_reader :composer, :composition, :publishing_splits

  RIGHTS_APP_CHAR_LIMIT = 60
  RA_WRITER_CODES = {
    UNKNOWN: 0,
    AUTHOR: 1,
    COMPOSER: 2,
    COMPOSER_AUTHOR: 3,
    ARRANGER: 4,
  }.freeze

  def initialize(opts = {})
    @composer           = opts[:composer]
    @composition        = opts[:composition]

    @publishing_splits =
      if @composer.is_a?(PublishingComposer) || @composition.is_a?(PublishingComposition)
        PublishingCompositionSplit.where(publishing_composition_id: @composition.id)
      else
        PublishingSplit.where(composition_id: @composition.id)
      end
  end

  def url
    "Works"
  end

  def request_method
    :post
  end

  def params
    {
      ArtistAccountCode: composer.account.provider_account_id,
      Title: composition.clean_composition_name.first(RIGHTS_APP_CHAR_LIMIT),
      IsRemix: false,
      ContainsSamples: false,
      WriterSplits: writer_splits,
      IsPublicDomain: @composition.public_domain?
    }
  end

  def requestable
    @composition
  end

  def response_class
    RightsApp::Responses::Work
  end

  private

  def get_split(writer)
    if writer.is_a?(PublishingComposer)
      publishing_splits.where(publishing_composer_id: writer.id).first
    else
      publishing_splits.where(writer_id: writer.id, writer_type: writer.class.to_s).first
    end
  end

  def writer_code(writer)
    return RA_WRITER_CODES[:ARRANGER] if @composition.public_domain?
    return RA_WRITER_CODES[:COMPOSER] if writer.is_a_primary_composer?

    writer.unknown? ? RA_WRITER_CODES[:UNKNOWN] : RA_WRITER_CODES[:COMPOSER_AUTHOR]
  end

  def writers
    @writers ||= publishing_splits.map { |split| split.try(:writer) || split.try(:publishing_composer) }
  end

  def writer_splits
    writers.map do |writer|
      {
        WriterCode: writer.provider_identifier,
        WriterDesignationCode: writer_code(writer),
        RightToCollect: writer.is_a_primary_composer?,
        WriterShare: get_split(writer).percent
      }
    end
  end
end
