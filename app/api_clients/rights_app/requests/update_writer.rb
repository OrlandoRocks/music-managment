class RightsApp::Requests::UpdateWriter
  include PublishingAdministrationHelper

  def initialize(params)
    @writer = params[:writer]
  end

  def url
    "Writers"
  end

  def request_method
    :put
  end

  def params
    sentric_writer_params(@writer).merge(WriterCode: @writer.provider_identifier)
  end

  def requestable
    @writer
  end

  def response_class
    RightsApp::Responses::Writer
  end
end
