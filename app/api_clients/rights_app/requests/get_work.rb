class RightsApp::Requests::GetWork
  attr_reader :work_code, :publishing_changeover_enabled

  def initialize(opts)
    @work_code = opts[:work_code]
    @publishing_changeover_enabled = opts[:publishing_changeover_enabled]
  end

  def url
    "Works/#{work_code}"
  end

  def request_method
    :get
  end

  def params
    {}
  end

  def requestable
    @requestable ||=
      if publishing_changeover_enabled
        PublishingComposition.find_by(provider_identifier: work_code)
      else
        Composition.find_by(provider_identifier: work_code)
      end
  end

  def response_class
    RightsApp::Responses::Work
  end
end
