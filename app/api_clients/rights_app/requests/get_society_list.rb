class RightsApp::Requests::GetSocietyList
  def initialize(opts)
    @page = opts[:page]
  end

  def url
    "Society"
  end

  def request_method
    :get
  end

  def params
    { Page: @page, PageSize: 100 }
  end

  def requestable; end

  def response_class
    RightsApp::Responses::SocietyList
  end
end
