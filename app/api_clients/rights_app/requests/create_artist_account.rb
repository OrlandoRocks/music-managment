class RightsApp::Requests::CreateArtistAccount
  attr_reader :account, :name

  def initialize(opts = {})
    @account = opts[:account]
    @name    = account.artist_account_name
  end

  def url
    "ArtistAccount"
  end

  def request_method
    :post
  end

  def params
    {
      AccountName: account.sanitize_name(name).strip,
      CatalogueCode: account.catalogue_code,
      PerformedLiveLastSixMonths: false
    }
  end

  def requestable
    @account
  end

  def response_class
    RightsApp::Responses::ArtistAccount
  end
end
