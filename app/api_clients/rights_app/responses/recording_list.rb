class RightsApp::Responses::RecordingList < RightsApp::Responses::BaseResponse
  def errors?
    messages.any?
  end

  def empty_list?
    work_not_found? || writer_not_found? || no_recordings_found?
  end

  def recordings
    @recordings ||=
      body["Records"].map do |record|
        OpenStruct.new(record.transform_keys(&:underscore))
      end
  end

  def total_count
    @total_count ||= body["TotalCount"].to_i
  end

  private

  def no_recordings_found?
    errors? && messages.grep(/no recordings found/i).present?
  end

  def work_not_found?
    errors? && messages.grep(/no work found/i).present?
  end

  def writer_not_found?
    errors? && messages.grep(/writer not found/i).present?
  end
end
