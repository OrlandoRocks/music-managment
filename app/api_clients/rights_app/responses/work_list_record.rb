class RightsApp::Responses::WorkListRecord
  attr_reader :work

  def initialize(work)
    @work = work
  end

  def should_not_update?
    changes_pending?
  end

  def work_code
    @work_code ||= work["WorkCode"] if work["WorkCode"].present?
  end

  def title
    @title ||= work["Title"]
  end

  def version
    @version ||= work["Version"]
  end

  def verified_date
    @verified_date ||= work["VerifiedDate"]
  end

  def writers
    @writers ||=
      work["Writers"].map do |writer|
        OpenStruct.new(writer.transform_keys(&:underscore))
      end
  end

  def is_deleted?
    @is_deleted ||= work["IsDeleted"]
  end

  def is_deactivated?
    @is_deactivated ||= work["IsDeactivated"]
  end

  def number_of_recordings
    @number_of_recordings ||= work["Recordings"]
  end

  def changes_pending?
    @changes_pending ||= work["ChangesPending"]
  end
end
