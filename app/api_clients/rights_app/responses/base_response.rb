class RightsApp::Responses::BaseResponse
  attr_reader :json_response

  def initialize(json_response)
    @json_response = json_response
  end

  def body
    @body ||= JSON.parse(json_response.body)
  end

  def messages
    @messages ||= Array(body["Messages"] || body["Message"]).compact
  end

  def api_endpoint
    @api_endpoint ||= json_response.env.url.to_s
  end

  def http_status
    @http_status ||= json_response.status
  end
end
