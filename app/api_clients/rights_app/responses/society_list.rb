class RightsApp::Responses::SocietyList < RightsApp::Responses::BaseResponse
  def total_count
    @total_count ||= body["TotalCount"].to_i
  end

  def societies
    @societies ||= body["Records"]
  end

  def errors?
    messages.any?
  end
end
