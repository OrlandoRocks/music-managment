class RightsApp::Responses::WriterList < RightsApp::Responses::BaseResponse
  def artist_account_code
    @artist_account_code ||= body["ArtistAccountCode"]
  end

  def writers
    @writers ||= body["Records"]
  end

  def total_count
    @total_count ||= body["TotalCount"].to_i
  end

  def errors?
    messages.any?
  end
end
