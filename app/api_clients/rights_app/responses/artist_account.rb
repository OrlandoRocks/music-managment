class RightsApp::Responses::ArtistAccount < RightsApp::Responses::BaseResponse
  def artist_account_code
    @artist_account_code ||= body["ArtistAccountCode"]
  end

  def is_deleted
    @is_deleted ||= body["IsDeleted"]
  end

  def retry?
    http_status == 429
  end
end
