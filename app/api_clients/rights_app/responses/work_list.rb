class RightsApp::Responses::WorkList < RightsApp::Responses::BaseResponse
  def artist_account_code
    @artist_account_code ||= body["ArtistAccountCode"]
  end

  def total_count
    @total_count ||= body["TotalCount"].to_i
  end

  def works
    @works ||=
      body["Records"].map do |work|
        RightsApp::Responses::WorkListRecord.new(work)
      end
  end

  def errors?
    messages.any?
  end
end
