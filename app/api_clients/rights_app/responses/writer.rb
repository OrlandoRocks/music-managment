class RightsApp::Responses::Writer < RightsApp::Responses::BaseResponse
  include PublishingAdministrationHelper

  def initialize(json_response = {})
    super
    check_for_society_error
  end

  def e_tag
    @e_tag ||= json_response.headers["etag"]
  end

  def first_name
    @first_name ||= body["FirstName"]
  end

  def middle_name
    @middle_name ||= body["MiddleName"]
  end

  def last_name
    @last_name ||= body["LastName"]
  end

  def society_id
    @society_id ||= body["SocietyId"]
  end

  def cae
    @cae ||= body["CaeipiNumber"]
  end

  def publisher_name
    @publisher_name ||= body["PublisherName"]
  end

  def publisher_cae
    @publisher_cae ||= body["PublisherCaeipi"]
  end

  def writer_code
    @writer_code ||= body["WriterCode"] if body["WriterCode"].present?
  end

  def changes_pending
    @changes_pending ||= body["ChangesPending"]
  end

  def already_exists?
    messages.present? && messages.map(&:downcase).include?("writer already exists.")
  end

  def needs_update?(writer)
    return false if changes_pending

    sentric_writer_params(writer) != parsed_writer(writer)
  end

  def unknown?
    middle_name.present? && middle_name.downcase.include?("unknown")
  end

  def has_publisher?
    publisher_name.present? || publisher_cae.present?
  end

  private

  def check_for_society_error
    return unless messages.include?("No society found.")

    raise "Rights App UAT has different Society IDs than Rights App PROD.
    Double check that the PRO provider_identifier matches the Rights App Society ID.
    If not you may need to run rake transient:seed_performing_rights_organizations"
  end

  def parsed_writer(writer)
    acct_code = { ArtistAccountCode: writer.account.provider_account_id }
    writer.try(:is_a_primary_composer?) ? acct_code.merge(parsed_composer) : acct_code.merge(parsed_cowriter)
  end

  def parsed_composer
    {
      FirstName: first_name,
      MiddleName: middle_name,
      LastName: last_name,
      SocietyId: society_id,
      CaeipiNumber: cae,
      PublisherName: publisher_name,
      PublisherCaeipi: publisher_cae
    }
  end

  def parsed_cowriter
    {
      FirstName: first_name,
      LastName: last_name,
      UnknownWriter: unknown?
    }
  end
end
