class RightsApp::Responses::Work < RightsApp::Responses::BaseResponse
  def e_tag
    @e_tag ||= json_response.headers["etag"]
  end

  def artist_account_code
    @artist_account_code ||= body["ArtistAccountCode"]
  end

  def is_deactivated
    @is_deactivated ||= body["IsDeactivated"]
  end

  def title
    @title ||= body["Title"]
  end

  def work_code
    @work_code ||= body["WorkCode"] if body["WorkCode"].present?
  end

  def writer_splits
    @writer_splits ||= body["WriterSplits"]
  end

  def already_exists?
    errors? && messages.map(&:downcase).any? { |m| m.include?("there is an existing record") }
  end

  def composer_splits
    writer_splits.find_all { |split| split["RightToCollect"] }
  end

  def cowriter_splits
    writer_splits.find_all { |split| !split["RightToCollect"] }
  end

  def errors?
    messages.any?
  end

  def existing_work_code
    return work_code unless already_exists?

    parse_message_for_work_code
  end

  private

  # example message: "There is an existing record, Work code(s) 'TUN000001018S', matching the criteria you have tried to enter for song title: TOO MANY COOKS"
  def parse_message_for_work_code
    sub_message     = messages.first.split("Work code(s) ").last.split(":").first
    last_index      = sub_message.rindex(",")
    return work_code if last_index.nil?

    work_codes_str = sub_message.slice(0..last_index).delete("'")
    work_codes_str.split(",").first
  end
end
