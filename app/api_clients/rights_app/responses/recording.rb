class RightsApp::Responses::Recording < RightsApp::Responses::BaseResponse
  def isrc
    @isrc ||= body["Isrc"]
  end

  def performing_artist
    @performing_artist ||= body["PerformingArtist"]
  end

  def title
    @title ||= body["Title"]
  end

  def recording_id
    @recording_id ||= body["RecordingId"]
  end

  def record_label
    @record_label ||= body["RecordLabel"]
  end

  def release_date
    @release_date ||= body["ReleaseDate"]
  end

  def work_code
    @work_code ||= body["WorkCode"] if work["WorkCode"].present?
  end
end
