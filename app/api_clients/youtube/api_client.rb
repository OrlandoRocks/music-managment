class Youtube::ApiClient
  class AccountError < StandardError; end

  class ApiError < StandardError; end

  class ServerError < StandardError; end

  class ApiQuotaError < StandardError; end

  TOKEN_URL = "https://oauth2.googleapis.com/token".freeze
  CHANNEL_URL = "https://www.googleapis.com/youtube/v3/channels".freeze

  SERVER_ERROR_CODES = [500, 502, 503].freeze
  QUOTA_ERROR = "quotaExceeded".freeze
  ACCOUNT_SUSPENDED = "authenticatedUserAccountSuspended".freeze

  attr :http_client, :client_url, :client_secret, :token, :code, :auth_code, :access_token, :refresh_token

  def initialize(options = {})
    @http_client = Faraday.new
    @client_id = ENV.fetch("YOUTUBE_OAC_CLIENT_ID")
    @client_secret = ENV.fetch("YOUTUBE_OAC_CLIENT_SECRET")
    @auth_code = options[:authorization]
    @refresh_token = options[:refresh_token]
  end

  def fetch_channel_id
    fetch_tokens unless access_token
    response = execute_channel_request
    extract_channel_id(response)
  end

  def fetch_tokens
    response = refresh_token ? execute_refresh_post : execute_token_post
    token_hash = parse_token_response(response)
  end

  def fetch_refresh_token
    fetch_tokens
    refresh_token
  end

  def execute_token_post
    http_client.url_prefix = TOKEN_URL
    http_client.post do |req|
      req.headers["Content-Type"] = "application/x-www-form-urlencoded"
      req.params = token_params
    end
  end

  def execute_refresh_post
    http_client.url_prefix = TOKEN_URL
    http_client.post do |req|
      req.params = refresh_params
    end
  end

  def execute_channel_request
    http_client.url_prefix = CHANNEL_URL
    http_client.get do |req|
      req.headers["Authorization"] = "Bearer #{access_token}"
      req.params = { part: "snippet", mine: "true" }
    end
  end

  def parse_token_response(response)
    raise_youtube_error(response) unless response.success?
    parsed_response = JSON.parse(response.body).with_indifferent_access
    @access_token = parsed_response[:access_token]
    @refresh_token = parsed_response[:refresh_token]
  end

  def extract_channel_id(response)
    raise_youtube_error(response) unless response.success?
    parsed_response = JSON.parse(response.body).with_indifferent_access
    return parsed_response[:items] if parsed_response[:items].nil?

    parsed_response[:items].first[:id]
  end

  def parse_response(response)
    JSON.parse(response.body).with_indifferent_access
  end

  def token_params
    {
      code: auth_code,
      client_id: ENV["YOUTUBE_OAC_CLIENT_ID"],
      client_secret: ENV["YOUTUBE_OAC_CLIENT_SECRET"],
      redirect_uri: ENV["YOUTUBE_OAC_REDIRECT_URL"],
      grant_type: "authorization_code"
    }
  end

  def refresh_params
    {
      client_id: ENV["YOUTUBE_OAC_CLIENT_ID"],
      client_secret: ENV["YOUTUBE_OAC_CLIENT_SECRET"],
      refresh_token: refresh_token,
      grant_type: "refresh_token"
    }
  end

  def raise_youtube_error(response)
    failure_reasons = failure_reasons(response)
    if failure_reasons.include? QUOTA_ERROR
      raise ApiQuotaError,
            "Youtube API Quota has been reached.
            \n Error Code:#{response.status}, Description: #{response.body}"
    elsif failure_reasons.include? ACCOUNT_SUSPENDED
      raise AccountError,
            "Youtube OAC Account error.
        \nError Code:#{response.status}, Description: #{response.body}"
    elsif SERVER_ERROR_CODES.include?(response.status)
      raise ServerError,
            "Youtube OAC Server error.
          \nError Code:#{response.status}, Description: #{response.body}"
    else
      raise ApiError,
            "Youtube OAC API error.
          \nError Code:#{response.status}, Description: #{response.body}"
    end
  end

  def failure_reasons(response)
    errors = JSON.parse(response.body).dig("error")
    return [] unless errors.is_a?(Hash)

    errors["errors"]&.map do |error|
      error["reason"]
    end
  end
end
