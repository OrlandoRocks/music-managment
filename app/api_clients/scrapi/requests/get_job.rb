module Scrapi::Requests
  class GetJob
    def initialize(job_id)
      @job_id = job_id
    end

    def path
      "#{Constants::RECOGNITIONJOB}/#{@job_id}"
    end
  end
end
