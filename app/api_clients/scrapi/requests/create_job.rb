module Scrapi::Requests
  class CreateJob
    def initialize(song)
      @song = song
    end

    def body
      {
        uploadType: Constants::AWS_TUNECORE,
        uploadParams: s3_path,
        callback: scrapi_callback_url,
      }
    end

    def path
      Constants::RECOGNITIONJOB
    end

    def s3_path
      "#{@song.song_file.bucket}:#{@song.song_file.key}"
    end

    def scrapi_callback_url
      urls = Rails.application.routes.url_helpers
      urls.api_external_scrapi_callback_url(
        host: ENV["SCRAPI_CALLBACK_DOMAIN"] || "api.tunecore.com",
        api_key: API_CONFIG["API_KEY"],
        protocol: "https",
      )
    end
  end
end
