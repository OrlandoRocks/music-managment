require "./lib/slack_notifier"

module Scrapi
  class ApiError < StandardError
    attr_reader :details

    def initialize(msg, details)
      @details = details
      msg
    end
  end

  class ApiClient
    HTTP_SCRAPI_AUTH_URL = ENV["HTTP_SCRAPI_AUTH_URL"] || "http://api-authv1.blvws.com/"
    HTTP_SCRAPI_BASE_URL = ENV["HTTP_SCRAPI_BASE_URL"] || "http://clientapi-system-contentrecognitionv1.blvws.com/"
    AUTH_TOKEN = "Believe::ScrapiAuthToken".freeze

    attr_reader :access_token

    def initialize(username, password)
      @username = username
      @password = password
    end

    def get_access_token(retries = 3)
      # rubocop:disable Style/GlobalVars
      @access_token = $redis.get(AUTH_TOKEN)
      scrapi_client(true) && return if @access_token.present?

      resp = auth_client.post("/auth/getAccessToken", { grant_type: "client_credentials" })
      raise ApiError.new("Unable to get access token", JSON.parse(resp.body)) unless resp.success?

      @access_token = JSON.parse(resp.body)["access_token"]
      $redis.set(AUTH_TOKEN, @access_token) unless $redis.nil?
      scrapi_client(true)
      @access_token
    # rubocop:enable Style/GlobalVars
    rescue Net::OpenTimeout, Faraday::ConnectionFailed, Faraday::SSLError => e
      Rails.logger.info "TRY #{retries}/n ERROR: timed out while trying to connect #{e}"
      raise ApiError.new("Too many retries for a request", e) if retries <= 1

      retry_get_access_token(retries - 1)
    end

    def create_job(song)
      req = Requests::CreateJob.new(song)
      resp = scrapi_client.post(req.path, req.body)
      response = JSON.parse(resp.body)
      if resp.success?
        begin
          job = Job.from_content(resp.body)
          job.song_id = song.id
          job.save
        rescue
          raise RuntimeError, "Unable to create scrapi job #{song.id}: #{response}"
        end
      else
        # rubocop:disable Style/GlobalVars
        $redis.set(AUTH_TOKEN, nil)
        response = JSON.parse(resp.body)
        raise RuntimeError, "Invaid Response -> #{song.id}: #{response}"
        # rubocop:enable Style/GlobalVars
      end
      job
    end

    def get_job(job_id)
      req = Requests::GetJob.new(job_id)
      resp = scrapi_client.get(req.path)
      unless resp.success?
        # rubocop:disable Style/GlobalVars
        $redis.set(AUTH_TOKEN, nil)
        raise ApiError.new("Unable to get job", JSON.parse(resp.body))
        # rubocop:enable Style/GlobalVars

      end

      job = Job.from_id(job_id)
      job.content = resp.body
      job.save
      job
    end

    def scrapi_client(force_new = false)
      @scrapi_client = nil if force_new

      @scrapi_client ||=
        Faraday.new(
          url: HTTP_SCRAPI_BASE_URL,
          headers: { Authorization: "Bearer #{@access_token}" }
        ) do |conn|
          conn.request :url_encoded
          conn.adapter Faraday.default_adapter
        end
    end

    def auth_client
      @auth_client =
        Faraday.new(HTTP_SCRAPI_AUTH_URL) do |conn|
          conn.request :url_encoded
          conn.basic_auth(@username, @password)
          conn.adapter Faraday.default_adapter
        end
      @auth_client
    end

    private

    def retry_get_access_token(retries)
      get_access_token(retries)
    end
  end
end
