class Sift::Responses::Score
  attr_reader :response, :request

  def self.sanitize_api_key(response)
    return unless response.present? && response.body.present?

    response.body["$api_key"] = "<SECRET>"
    response
  end

  def initialize(response, request)
    @response = Sift::Responses::Score.sanitize_api_key(response)
    @request = request

    update_person_sift_score
    store_response
  end

  def status
    @response&.body&.dig("status")
  end

  def score
    @response&.body&.dig("score_response", "scores", "payment_abuse", "score")
  end

  def workflow_decision_id
    @response&.body&.dig("score_response", "workflow_statuses", 0, "history", 0, "config", "decision_id")
  end

  private

  def update_person_sift_score
    return unless request.order_id

    person_sift_score = PersonSiftScore.find_or_initialize_by(person_id: request.person_id)
    person_sift_score.update(sift_order_id: request.order_id)
    person_sift_score.update(score: score) if request.return_workflow_status
  end

  def store_response
    return unless request.return_workflow_status

    readable_score = (score * 100).round

    json = JSON.parse(response&.body&.dig("score_response").to_json)

    file_name = File.join(request.person_id.to_s, "score-#{readable_score}-#{DateTime.now.to_i}.json")

    file_path = File.join(
      Rails.root,
      "tmp",
      ENV["RAILS_ENV"],
      file_name
    )

    FileUtils.mkdir_p(File.dirname(file_path))
    File.open(file_path, "w+") do |file|
      file.write(json)
    end

    RakeDatafileService.new(
      bucket_name: "sift.tunecore.com",
      file_key: file_name,
      file_path: file_path,
      action: "upload",
      overwrite: "y"
    )
  end
end
