class Sift::ApiClient
  attr_reader :client, :request

  def self.send_request(event_name, body, force_return_workflow_status = false)
    if event_name == Sift::EventService::TRANSACTION
      if (body.size > 1)
        new(event_name, body[0], body[1][:$amount].zero? && force_return_workflow_status).send_request
        new(event_name, body[1], force_return_workflow_status).send_request
      else
        new(event_name, body[0], force_return_workflow_status).send_request
      end
    else
      new(event_name, body, force_return_workflow_status).send_request
    end
  end

  def initialize(event_name, body, force_return_workflow_status)
    return unless FeatureFlipper.show_feature?(:sift)

    @client = set_api_client
    @request = build_path(event_name).new(body, force_return_workflow_status)
  end

  def build_path(event_name)
    "Sift::Requests::#{event_name.to_s.classify}".constantize
  end

  def send_request
    return unless client.present? && request.valid?

    begin
      @response = client.track(
        request.request_type,
        request.body,
        return_workflow_status: request.return_workflow_status
      )

      Sift::Responses::Score.new(@response, request) if request.return_workflow_status
    rescue Sift::ApiError
      log_api_errors
    end
  end

  private

  def log_api_errors
    @response = Sift::Responses::Score.sanitize_api_key(@response)

    Tunecore::Airbrake.notify(
      "Sift::Base - Sift API Error Status: #{@response.body['status']}",
      {
        event_type: event_type,
        body: @response.body
      }
    )

    true
  end

  def set_api_client
    Sift::Client.new(api_key: sift_api_key, account_id: sift_account_id)
  end

  def sift_api_key
    ENV.fetch("SIFT_API_KEY") do
      Tunecore::Airbrake.notify("ENV variable SIFT_API_KEY is missing")
      raise "ENV variable SIFT_API_KEY is missing"
    end
  end

  def sift_account_id
    ENV.fetch("SIFT_ACCOUNT_ID") do
      Tunecore::Airbrake.notify("ENV variable SIFT_ACCOUNT_ID is missing")
      raise "ENV variable SIFT_ACCOUNT_ID is missing"
    end
  end
end
