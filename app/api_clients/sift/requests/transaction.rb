class Sift::Requests::Transaction < Sift::Requests::Base
  def initialize(body, force_return_workflow_status = false)
    super(body, force_return_workflow_status)

    @request_type = "$#{Sift::EventService::TRANSACTION}"
  end

  def order_id
    body&.dig(:$order_id)
  end

  def amount
    body&.dig(:$amount)
  end

  def valid?
    return false unless super

    amount.positive?
  end
end
