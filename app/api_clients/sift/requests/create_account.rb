class Sift::Requests::CreateAccount < Sift::Requests::Base
  def initialize(body, force_return_workflow_status = false)
    super(body, force_return_workflow_status)

    @request_type = "$#{Sift::EventService::CREATE_ACCOUNT}"
  end
end
