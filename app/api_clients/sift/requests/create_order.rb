class Sift::Requests::CreateOrder < Sift::Requests::Base
  def initialize(body, force_return_workflow_status = true)
    super(body, force_return_workflow_status)

    @request_type = "$#{Sift::EventService::CREATE_ORDER}"
  end

  def order_id
    body&.dig(:$order_id)
  end
end
