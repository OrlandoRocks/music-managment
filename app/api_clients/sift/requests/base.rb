class Sift::Requests::Base
  attr_reader :body, :request_type, :return_workflow_status

  CUSTOM_FIELD_TYPES = {
    releases_active: [Integer],
    releases_taken_down: [Integer],
    corporate_entity: [String],
    is_first_time_buyer: [TrueClass, FalseClass]
  }

  def initialize(body, force_return_workflow_status)
    @body = body
    @return_workflow_status = force_return_workflow_status && sift_scores_active?
  end

  def person_id
    body&.dig(:$user_id)
  end

  def valid?
    return log_invalid_body("Body is Blank") if body.blank?
    return false unless custom_field_types_valid?

    true
  end

  private

  def sift_scores_active?
    body.dig(:$user_id).present? && FeatureFlipper.show_feature?(:sift_scores, body.dig(:$user_id))
  end

  def custom_field_types_valid?
    errors = {}

    CUSTOM_FIELD_TYPES.each do |field, type|
      next unless field.present? && !body[field].nil?

      error = "#{field} is required to be type #{type} but is type #{body[field].class}"
      errors[field] = error unless body[field].class.in? type
    end

    return log_invalid_body("TypeError", errors) if errors.present?

    true
  end

  def log_invalid_body(reason = "", errors = {})
    Tunecore::Airbrake.notify(
      "Sift::Base - Sift Body is Invalid: #{reason}",
      {
        event_type: request_type,
        body: body,
        errors: errors
      }
    )

    false
  end
end
