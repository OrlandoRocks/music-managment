# frozen_string_literal: true

class Currencylayer::ApiClient
  REQUEST_CLASSES = {
    historical: Currencylayer::Requests::Historical
  }.freeze

  def initialize
    base_url = ENV.fetch("CURRENCYLAYER_BASE_URL")
    access_key = ENV.fetch("CURRENCYLAYER_ACCESS_KEY")

    @http_client =
      Faraday.new(
        base_url,
        params: {
          access_key: access_key
        }
      ) do |faraday|
        faraday.response :logger
        faraday.request :url_encoded
        faraday.adapter Faraday.default_adapter
      end
  end

  def send_request(req_type, options)
    request = REQUEST_CLASSES.fetch(req_type).new(options)
    response =
      @http_client.send(request.request_method, request.url, request.params) do |req|
        request.attach_request_params(req)
      end
    trace(response)
    request.response_parser.new(response)
  end

  def get_forex(source, target)
    response =
      @http_client.get("/live") do |request|
        request.params["source"] = source
        request.params["currencies"] = target
      end
    trace(response)
    JSON.parse(response.body)
  end

  private

  def trace(response)
    ApiCallTracer.call(response, nil, response.env.params, response.env.url)
  end
end
