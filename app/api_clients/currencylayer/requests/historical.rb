# forzen_string_literal: true

class Currencylayer::Requests::Historical
  URL = "/historical"

  def initialize(options)
    @date = options.fetch(:date)
    @source_currency = options.fetch(:source_currency)
    @target_currencies = Array(options[:target_currencies])
  end

  def url
    URL
  end

  def response_parser
    Currencylayer::Responses::Historical
  end

  def request_method
    :get
  end

  def params
    {
      date: date,
      source: source_currency,
      currencies: target_currencies.join(",")
    }
  end

  def attach_request_params(faraday_rquest)
    faraday_rquest.params.merge(params)
  end

  private

  attr_reader :date, :source_currency, :target_currencies
end
