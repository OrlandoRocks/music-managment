# frozen_string_literal: true

module Currencylayer
  module Responses
    class Historical < Base
      def date
        body["date"]&.to_date
      end

      def historical?
        body["historical"] || false
      end
    end
  end
end
