# frozen_string_literal: true

class Currencylayer::Responses::Base
  attr_reader :response_status, :raw_response, :body

  def initialize(response)
    @response_status = response.status
    @raw_response = response
    @body = Oj.load(response.body)
  end

  def success?
    @success ||= body["success"]
  end

  def source
    body["source"]
  end

  def timestamp
    body["timestamp"]
  end

  def quotes
    body["quotes"]
  end
end
