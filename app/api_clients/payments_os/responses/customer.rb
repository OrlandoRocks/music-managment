class PaymentsOS::Responses::Customer
  def initialize(response)
    @response = response
    @body = JSON.parse(@response.body, object_class: OpenStruct)
  end

  def customer_reference
    @body.customer_reference
  end

  def customer_id
    @body.id
  end

  def errors
    @body.more_info unless @response.status == 200
  end

  def created?
    @body.created.present?
  end

  def payment_methods
    @body.payment_methods
  end
end
