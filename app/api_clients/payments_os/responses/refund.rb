class PaymentsOS::Responses::Refund
  def initialize(response)
    @response = response
    @body = JSON.parse(response.body, object_class: OpenStruct)
  end

  def raw_response
    @body
  end

  def refund_transaction_id
    @body.id
  end

  def message
    provider_data.description
  end

  def amount
    @body.amount
  end

  def created?
    @body.created.present?
  end

  def errors
    return "Gateway Error: #{@response.status}" unless (200..299).cover?(@response.status)

    @body.more_info.presence
  end

  def success?
    errors.blank?
  end

  def status
    errors.blank? ? @body.result&.status : PaymentsOSTransaction::FAILED
  end

  def provider_data
    @body.provider_data
  end
end
