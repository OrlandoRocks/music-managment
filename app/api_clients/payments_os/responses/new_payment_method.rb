class PaymentsOS::Responses::NewPaymentMethod
  def initialize(response)
    @response = response
    @body = JSON.parse(response.body, object_class: OpenStruct)
  end

  def card_created?
    @body.state == "created"
  end

  def errors
    return "Gateway Error: #{@response.status}" unless (200..299).cover?(@response.status)

    @body.more_info
  end

  def expiration_date
    @expiration_date ||= @body.expiration_date.to_s.split("/")
  end

  def last_4_digits
    @body.last_4_digits
  end

  def token
    @body.token
  end

  def expiration_month
    expiration_date[0]
  end

  def cc_type
    @body.vendor
  end

  def expiration_year
    expiration_date[1]
  end
end
