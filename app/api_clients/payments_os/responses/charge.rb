class PaymentsOS::Responses::Charge
  def initialize(response)
    @response_status = response.status
    @body = JSON.parse(response.body, object_class: OpenStruct)
  end

  def success?
    (200..299).cover?(@response_status)
  end

  def id
    @body.id
  end

  def status
    @body.result.status
  end

  def response_json
    @body.to_json
  end

  def redirect_url
    @body.redirection&.url
  end

  def reconciliation_id
    @body.reconciliation_id
  end
end
