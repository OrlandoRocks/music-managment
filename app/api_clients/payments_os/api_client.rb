class PaymentsOS::ApiClient
  BASE_URL = ENV["PAYMENTS_OS_BASE_URL"]

  def initialize(person)
    @person = person
    @http_client = Faraday.new(BASE_URL)
  end

  def send_request(request_name, options)
    request = "PaymentsOS::Requests::#{request_name.to_s.classify}".constantize.new(options)
    response =
      @http_client.send(request.request_method, request.url) do |req|
        attach_headers(req)
        case request.request_method
        when :post
          req.body = request.params.to_json
        when :get
          req.params = request.params
        end
      end
    trace(response, request)
    request.response_parser.new(response)
  end

  private

  def attach_headers(req)
    req.headers["app_id"] = ENV["PAYMENTS_OS_PAYU_APP_ID"]
    req.headers["private_key"] = ENV["PAYMENTS_OS_PAYU_PRIVATE_KEY"]
    req.headers["x-payments-os-env"] = ENV["PAYMENTS_OS_ENV"]
    req.headers["api-version"] = ENV["PAYMENTS_OS_API_VERSION"]
    req.headers["content-type"] = "application/json"
  end

  def trace(response, request)
    ApiCallTracer.call(response, @person.id, request.params, request.url)
  end
end
