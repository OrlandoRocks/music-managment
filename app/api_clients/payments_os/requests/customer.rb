class PaymentsOS::Requests::Customer
  def initialize(options = {})
    @person = options[:person]
  end

  def url
    "/customers/#{@person.payments_os_customer&.customer_id}"
  end

  def response_parser
    PaymentsOS::Responses::Customer
  end

  def request_method
    :get
  end

  def params
    {}
  end
end
