class PaymentsOS::Requests::Refund
  include IndiaPaymentsHelper

  def initialize(options = {})
    @payment_id = options[:payment_id]
    @amount = options[:amount]
  end

  def url
    "/payments/#{@payment_id}/refunds"
  end

  def request_method
    :post
  end

  def response_parser
    PaymentsOS::Responses::Refund
  end

  def params
    {
      amount: @amount
    }
  end
end
