class PaymentsOS::Requests::Payment
  def initialize(options = {})
    @stored_cc = options[:stored_cc]
    @order_id = options[:order_id]
    @amount = options[:amount]
  end

  def url
    "/payments"
  end

  def request_method
    :post
  end

  def response_parser
    PaymentsOS::Responses::Payment
  end

  def params
    {
      amount: @amount,
      currency: "INR",
      billing_address: {
        country: "IND",
        city: @stored_cc.city,
        line1: @stored_cc.address1,
        line2: @stored_cc.address2,
        zip_code: @stored_cc.zip,
        first_name: first_name,
        last_name: last_name
      },
      order: {
        id: @order_id.to_s
      }
    }
  end

  def compliance_info_fields
    @stored_cc.person.compliance_info_fields
  end

  def first_name
    compliance_first_name = compliance_info_fields.where(field_name: "first_name").pluck(:field_value)
    compliance_first_name.presence&.last || @stored_cc.person.first_name
  end

  def last_name
    compliance_last_name = compliance_info_fields.where(field_name: "last_name").pluck(:field_value)
    compliance_last_name.presence&.last || @stored_cc.person.last_name
  end
end
