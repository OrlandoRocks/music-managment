class PaymentsOS::Requests::Charge
  def initialize(options = {})
    @payment_id = options[:payment_id]
    @invoice = PaymentsOSTransaction.find_by(payment_id: @payment_id).invoice
    @card_token = options[:card_token]
    @cvv = options[:cvv]
    @return_url = options[:return_url]
  end

  def url
    "/payments/#{@payment_id}/charges"
  end

  def request_method
    :post
  end

  def response_parser
    PaymentsOS::Responses::Charge
  end

  def params
    # reconciliation_id cannot be greater than 25 characters as per PayU integration team
    reconciliation_id = SecureRandom.alphanumeric(25)
    {
      merchant_site_url: @return_url,
      reconciliation_id: reconciliation_id,
      provider_specific_data: {
        payu_india: {
          additional_details: {
            invoice_id: @invoice.id.to_s,
            si: "1"
          }
        }
      },
      payment_method: {
        type: "tokenized",
        token: @card_token,
        credit_card_cvv: @cvv
      }
    }
  end
end
