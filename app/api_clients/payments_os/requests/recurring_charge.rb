class PaymentsOS::Requests::RecurringCharge
  def initialize(options = {})
    @payment_id = options[:payment_id]
    @payment    = PaymentsOSTransaction.find_by(payment_id: @payment_id)
    @card_token = options[:card_token]
  end

  def url
    "/payments/#{@payment_id}/charges"
  end

  def request_method
    :post
  end

  def response_parser
    PaymentsOS::Responses::Charge
  end

  def params
    # reconciliation_id cannot be greater than 25 characters as per PayU integration team
    reconciliation_id = SecureRandom.alphanumeric(25)
    {
      reconciliation_id: reconciliation_id,
      provider_specific_data: {
        payu_india: {
          additional_details: {
            recurring: "1",
            invoice_id: @payment.invoice_id.to_s
          }
        }
      },
      payment_method: {
        type: "tokenized",
        token: @card_token
      }
    }
  end
end
