class PaymentsOS::Requests::NewPaymentMethod
  def initialize(options = {})
    @customer_id = options[:customer_id]
    @payment_token = options.dig(:stored_cc_params, :payments_os_token)
  end

  def url
    "/customers/#{@customer_id}/payment-methods/#{@payment_token}"
  end

  def response_parser
    PaymentsOS::Responses::NewPaymentMethod
  end

  def request_method
    :post
  end

  def params
    {}
  end
end
