class PaymentsOS::Requests::NewCustomer
  def initialize(options = {})
    @person = options[:person]
  end

  def url
    "/customers"
  end

  def request_method
    :post
  end

  def response_parser
    PaymentsOS::Responses::Customer
  end

  def params
    {
      customer_reference: SecureRandom.uuid,
      first_name: @person.first_name,
      last_name: @person.last_name,
      email: @person.email
    }
  end
end
