class DocuSign::ApiClient
  HOST           = ENV["DOCUSIGN_HOST"]
  INTEGRATOR_KEY = ENV["DOCUSIGN_INTEGRATOR_KEY"]
  AUTH_SERVER    = ENV["DOCUSIGN_AUTH_SERVER"]
  USER_ID        = ENV["DOCUSIGN_USER_ID"]
  ACCOUNT_ID     = ENV["DOCUSIGN_ACCOUNT_ID"]
  EXPIRES_IN     = ENV["DOCUSIGN_EXPIRATION_SECONDS"].to_i.freeze
  RSA_PATH       = "#{Dir.home}/.ssh/docusign_rsa".freeze

  JWT_AUTH_ARGS = [RSA_PATH, AUTH_SERVER, INTEGRATOR_KEY, USER_ID, EXPIRES_IN].freeze

  def api_client
    @api_client ||= set_api_client
  end

  private

  def set_api_client
    docusign_api_client = DocuSign_eSign::ApiClient.new(configuration)
    docusign_api_client.configure_jwt_authorization_flow(*JWT_AUTH_ARGS)
    docusign_api_client
  end

  def configuration
    config      = DocuSign_eSign::Configuration.new
    config.host = HOST
    config
  end
end
