class DocuSign::TemplatesApi < DocuSign::ApiClient
  def self.list_templates
    new.list_templates
  end

  def self.get_template(template_id)
    new.get_template(template_id)
  end

  attr_reader :templates_api, :response

  def initialize
    @templates_api = DocuSign_eSign::TemplatesApi.new(api_client)
  end

  def list_templates
    templates_api.list_templates(ACCOUNT_ID)
  end

  def get_template(template_id)
    templates_api.get(ACCOUNT_ID, template_id)
  end
end
