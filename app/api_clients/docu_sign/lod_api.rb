class DocuSign::LodApi < DocuSign::ApiClient
  include Rails.application.routes.url_helpers

  PUBLISHER_PRO = {
    ASCAP: "TuneCore Publishing (ASCAP)",
    BMI: "TuneCore Digital Music (BMI)",
    SESAC: "TuneCore Songs (SESAC)"
  }

  def self.send_lod_mail(person, composer)
    new(person, composer).send_mail
  end

  def self.resend_lod_mail(person, composer)
    lod = composer.legal_documents.where(name: DocumentTemplate::LOD).last

    return if lod.signed_at.present?

    instance = new(person, composer)

    lod.voided? ? instance.send_mail : instance.resend_mail(lod)
  end

  def self.update_legal_document(person, composer, envelope_id)
    new(person, composer, envelope_id).update_legal_document
  end

  def initialize(person, composer, envelope_id = nil)
    @person = person
    @composer = composer
    @envelope_id = envelope_id
    @template = DocumentTemplate.where(
      document_title: DocumentTemplate::LOD,
      language_code: @person.fallback_locale
    ).first
    @envelopes_api = DocuSign_eSign::EnvelopesApi.new(api_client)
  end

  def send_mail
    begin
      definition = create_envelop_definition
      result = @envelopes_api.create_envelope(ACCOUNT_ID, definition)
      create_legal_document(result.envelope_id)
    rescue DocuSign_eSign::ApiError => e
      Tunecore::Airbrake.notify(
        "Unable to send Publishing Letter of Direction mail",
        {
          person: @person,
          composer: @composer,
          template: @template,
          error: JSON.parse(e.response_body)
        }
      )
    end
  end

  def resend_mail(lod)
    begin
      envelope_id = lod.provider_identifier
      envelope = DocuSign_eSign::Envelope.new
      envelope.status = "sent"
      options = DocuSign_eSign::UpdateOptions.new
      options.resend_envelope = true
      @envelopes_api.update(ACCOUNT_ID, envelope_id, envelope, options)
    rescue DocuSign_eSign::ApiError => e
      Tunecore::Airbrake.notify(
        "Unable to resend Publishing Letter of Direction mail",
        {
          composer: @composer,
          envelope_id: envelope_id,
          error: JSON.parse(e.response_body)
        }
      )
    end
  end

  def update_legal_document
    lod = @composer.legal_documents.find_by(provider_identifier: @envelope_id)
    create_legal_document(@envelope_id) if lod.blank?
    update_envelope_status(@envelope_id)
  end

  private

  def create_envelop_definition
    envelope_definition = DocuSign_eSign::EnvelopeDefinition.new
    envelope_definition.template_id = @template.template_id
    envelope_definition.email_subject = I18n.t("api.letter_of_direction_mail.subject")
    envelope_definition.email_blurb = I18n.t("api.letter_of_direction_mail.body", name: @person.name)
    envelope_definition.status = "sent"
    envelope_definition.event_notification = build_notifier

    signer = DocuSign_eSign::TemplateRole.new(
      {
        email: @person.email,
        name: @composer.full_name_affixed,
        roleName: "Signer",
        tabs: build_tabs
      }
    )
    envelope_definition.template_roles = [signer]
    envelope_definition
  end

  def build_notifier
    completed_event = DocuSign_eSign::EnvelopeEvent.new
    completed_event.envelope_event_status_code = "Completed"

    voided_event = DocuSign_eSign::EnvelopeEvent.new
    voided_event.envelope_event_status_code = "Voided"

    event_notification = DocuSign_eSign::EventNotification.new
    event_notification.envelope_events = [completed_event, voided_event]
    event_notification.logging_enabled = true
    event_notification.require_acknowledgment = true

    event_notification.url = callback_url
    event_notification
  end

  def build_tabs
    publisher_name = @composer.publisher.name
    publisher_pro = @composer.publisher.performing_rights_organization.try(:name)

    {
      textTabs: [
        {
          tabLabel: '\\*company_name',
          value: "#{publisher_name} (#{publisher_pro})"
        },
        {
          tabLabel: '\\*publisher_pro',
          value: PUBLISHER_PRO[publisher_pro&.to_sym] || ""
        }
      ]
    }
  end

  def create_legal_document(envelope_id)
    @person.legal_documents.create!(
      name: @template.document_title,
      subject: @composer,
      provider: "Docusign",
      document_template: @template,
      provider_identifier: envelope_id,
      status: :sent
    )
  end

  def callback_url
    country = @composer.publishing_administrator.country_website.country
    urls = Rails.env.production? ? WEB_ONLY_URLS : COUNTRY_URLS
    country_url = urls[country]

    api_v1_docusign_document_status_url(
      host: "https://#{country_url}#{ENV['DOCUSIGN_PORT_NUMBER']}"
    )
  end

  def update_envelope_status(envelope_id)
    begin
      lod = @composer.legal_documents.find_by(provider_identifier: @envelope_id)
      options = DocuSign_eSign::GetEnvelopeOptions.default
      envelope = @envelopes_api.get_envelope(ACCOUNT_ID, envelope_id, options)
      lod.update(
        status: envelope.status,
        signed_at: envelope.status == "completed" ? envelope.status_changed_date_time : nil
      ) if lod.present?

      return if lod.signed_at.present?

      resend_mail(lod)
    rescue DocuSign_eSign::ApiError => e
      Tunecore::Airbrake.notify(
        "Unable to find envelope having the supplied envelope id",
        {
          person: @person,
          composer: @composer,
          template: @template,
          error: JSON.parse(e.response_body)
        }
      )
    end
  end
end
