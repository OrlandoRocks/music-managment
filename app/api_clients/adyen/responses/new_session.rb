# frozen_string_literal: true

class Adyen::Responses::NewSession
  attr_reader :body

  def initialize(response)
    @body = JSON.parse(response.body)
  end

  def session_data
    body.fetch("sessionData")
  end

  def id
    body.fetch("id")
  end

  def payment_method
    body.fetch("allowedPaymentMethods")[0]
  end

  def amount_in_local_currency
    body.fetch("amount").fetch("value")
  end

  def error_code
    body.key?("errorCode") ? body.fetch("errorCode") : nil
  end
end
