# frozen_string_literal: true

class Adyen::Responses::Refund
  attr_reader :body

  def initialize(response)
    @body = JSON.parse(response.body)
  end

  def result_code
    body.fetch("status")
  end

  def psp_reference
    body.fetch("pspReference")
  end
end
