# frozen_string_literal: true

class Adyen::Responses::PaymentDetail
  attr_reader :body

  def initialize(response)
    @body = JSON.parse(response.body)
  end

  def psp_reference
    body["pspReference"]
  end

  def result_code
    body["resultCode"]
  end

  def merchant_reference
    body["merchantReference"]
  end

  def payment_succeeded?
    result_code == AdyenTransaction::AUTHORISED
  end

  def recurring_reference
    body.dig("additionalData", "recurringDetailReference")
  end

  def error_message
    body.key?("errorCode") ? body.fetch("message") : nil
  end
end
