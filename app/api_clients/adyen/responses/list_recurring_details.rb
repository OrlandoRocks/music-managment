# frozen_string_literal: true

class Adyen::Responses::ListRecurringDetails
  attr_reader :raw_response, :body

  def initialize(response)
    @raw_response = response
    @body = Oj.load(response.body)
  end

  def details
    @details ||= body.fetch("details")
  end

  def detail_for_psp_reference(psp_reference)
    details.find { |f| f.fetch("firstPspReference") == psp_reference }
  end

  def recurring_detail_reference_for_psp_reference(psp_reference)
    detail_for_psp_reference(psp_reference)
      &.fetch("recurringDetailReference")
  end
end
