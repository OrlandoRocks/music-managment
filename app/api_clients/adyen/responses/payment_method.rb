# frozen_string_literal: true

class Adyen::Responses::PaymentMethod
  attr_reader :body

  def initialize(response)
    @body = JSON.parse(response.body)
  end

  def payment_methods
    body.fetch("paymentMethods")
  end
end
