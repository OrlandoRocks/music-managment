# frozen_string_literal: true

class Adyen::Responses::Renewal
  attr_reader :body

  def initialize(response)
    @body = JSON.parse(response.body)
  end

  def psp_reference
    body.fetch("pspReference")
  end

  def result_code
    body.fetch("resultCode")
  end

  def merchant_reference
    body.fetch("merchantReference")
  end

  def payment_succeeded?
    result_code == AdyenTransaction::AUTHORISED
  end
end
