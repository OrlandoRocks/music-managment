# frozen_string_literal: true

class Adyen::PaymentsApiClient
  include AdyenSecretsHelper

  BASE_URL = ENV["ADYEN_PAYMENTS_API_BASE_URL"]

  def initialize(corporate_entity_id)
    @merchant_config = config_for_corporate_entity(corporate_entity_id)
    @http_client =
      Faraday.new(url: BASE_URL) do |f|
        f.request :multipart
        f.request :url_encoded
        f.adapter :net_http
      end
    @http_client.headers["content-type"] = "application/json"
    @http_client.headers["x-API-key"] = @merchant_config.api_key
  end

  def send_request(req_type, options)
    @invoice_id = options[:invoice]&.id

    request = "Adyen::Requests::#{req_type.to_s.classify}".constantize.new(request_options(options))

    response =
      @http_client.send(request.request_method, request.url, request.params) do |req|
        request.attach_request_params(req)
      end

    trace(response, request)
    request.response_parser.new(response)
  end

  private

  def trace(response, request)
    ApiCallTracer.call(response, @invoice_id, request.params, request.url)
  end

  def request_options(options)
    options.merge({ merchant_account: @merchant_config.merchant_account })
  end
end
