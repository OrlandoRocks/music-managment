# frozen_string_literal: true

class Adyen::RecurringApiClient
  include AdyenSecretsHelper

  REQUEST_CLASSES = {
    list_recurring_details: Adyen::Requests::ListRecurringDetails
  }.freeze

  def initialize(corporate_entity_id)
    @merchant_config = config_for_corporate_entity(corporate_entity_id)
    @http_client =
      Faraday.new(**request_opts) do |f|
        f.adapter :net_http
      end
  end

  def send_request(req_type, options)
    request = REQUEST_CLASSES.fetch(req_type).new(request_options(options))
    response =
      @http_client.send(request.request_method, request.path, request.params) do |req|
        request.attach_request_params(req)
      end
    trace(response, request)
    request.response_parser.new(response)
  end

  private

  def request_opts
    {
      url: base_url,
      headers: {
        "x-API-key" => api_key,
        "Content-Type" => "application/json"
      }
    }
  end

  def trace(response, request)
    ApiCallTracer.call(response, nil, request.params, request.path)
  end

  def base_url
    ENV.fetch("ADYEN_RECURRING_API_BASE_URL")
  end

  def api_key
    @merchant_config.api_key
  end

  def request_options(options)
    options.merge({ merchant_account: @merchant_config.merchant_account })
  end
end
