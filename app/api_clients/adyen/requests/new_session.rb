# frozen_string_literal: true

class Adyen::Requests::NewSession
  include Rails.application.routes.url_helpers

  attr_reader :invoice, :person, :amount_formatter, :adyen_merchant_account

  SCHEME_PAYMENT_METHOD = "scheme"

  BLOCKED_PAYMENT_METHODS = [
    AdyenPaymentMethodInfo::GOOGLEPAY_PAYMENT_METHOD,
    AdyenPaymentMethodInfo::APPLEPAY_PAYMENT_METHOD,
  ].freeze

  def initialize(options)
    @invoice = options[:invoice]
    @person = invoice.person
    @payment_method = options[:payment_method]
    @amount_formatter = options[:amount_formatter]
    @adyen_merchant_account = options[:merchant_account]
  end

  def url
    "sessions"
  end

  def response_parser
    Adyen::Responses::NewSession
  end

  def request_method
    :post
  end

  def params
    {
      reference: payment_reference,
      amount: amount,
      shopperEmail: shopper_email,
      shopperReference: unique_shopper_reference,
      countryCode: person.country_iso_code,
      merchantAccount: merchant_account,
      returnUrl: return_url,
      allowedPaymentMethods: allowed_payment_method,
      recurringProcessingModel: "CardOnFile",
      shopperInteraction: shopper_interaction,
      storePaymentMethod: true,
      blockedPaymentMethods: blocked_payment_methods
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def amount
    {
      value: amount_formatter.amount,
      currency: amount_formatter.currency
    }
  end

  def shopper_interaction
    return "ContAuth" if later_payment?

    "Ecommerce"
  end

  def payment_method_already_stored?
    AdyenStoredPaymentMethod.find_by(person: @person, adyen_payment_method_info: payment_method_info).present?
  end

  def payment_method_info
    AdyenPaymentMethodInfo.find_by(payment_method_name: payment_method_name)
  end

  def payment_method_name
    return "cup" if @payment_method == SCHEME_PAYMENT_METHOD

    @payment_method
  end

  def merchant_account
    adyen_merchant_account
  end

  def shopper_ip
    @ip_address
  end

  def unique_shopper_reference
    person.adyen_customer_reference
  end

  def shopper_email
    @person.email
  end

  def payment_reference
    invoice.id
  end

  def allowed_payment_method
    payment_methods = []
    payment_methods.push(@payment_method)
  end

  def return_url
    adyen_finalize_after_redirect_cart_url(invoice_id: invoice.id, host: person.domain, protocol: return_protocol)
  end

  def return_protocol
    Rails.env.development? ? "http" : "https"
  end

  def later_payment?
    payment_method_already_stored? && payment_method_name != AdyenPaymentMethodInfo::IDEAL_PAYMENT_METHOD
  end

  def blocked_payment_methods
    return [] unless @payment_method == SCHEME_PAYMENT_METHOD

    BLOCKED_PAYMENT_METHODS
  end
end
