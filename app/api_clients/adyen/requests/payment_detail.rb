# frozen_string_literal: true

class Adyen::Requests::PaymentDetail
  attr_reader :redirect_result

  URL = "payments/details"

  def initialize(options)
    @redirect_result = options[:redirect_result]
  end

  def url
    URL
  end

  def response_parser
    Adyen::Responses::PaymentDetail
  end

  def request_method
    :post
  end

  def params
    {
      details: {
        redirectResult: redirect_result
      }
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end
end
