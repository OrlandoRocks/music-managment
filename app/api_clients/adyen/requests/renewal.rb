# frozen_string_literal: true

class Adyen::Requests::Renewal
  include CurrencyHelper
  include Rails.application.routes.url_helpers

  CUP_PAYMENT_METHOD = "cup"

  SEPADIRECTDEBIT = "sepadirectdebit"

  attr_reader :invoice, :person, :adyen_merchant_account

  def initialize(options)
    @invoice = options[:invoice]
    @person = invoice.person
    @adyen_merchant_account = options[:merchant_account]
  end

  def url
    "payments"
  end

  def response_parser
    Adyen::Responses::Renewal
  end

  def request_method
    :post
  end

  def params
    {
      amount: amount,
      paymentMethod: payment_method,
      reference: payment_reference,
      shopperReference: unique_shopper_reference,
      merchantAccount: merchant_account,
      returnUrl: return_url,
      recurringProcessingModel: "CardOnFile",
      shopperInteraction: "ContAuth",
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def stored_payment_method_id
    preferred_adyen_payment_method.recurring_reference
  end

  def person_preference
    PersonPreference.find_by(person: person, preferred_payment_type: "Adyen")
  end

  def preferred_adyen_payment_method
    person_preference.preferred_adyen_payment_method
  end

  def payment_method_info
    preferred_adyen_payment_method.adyen_payment_method_info
  end

  def amount
    {
      value: amount_formatter.amount,
      currency: amount_formatter.currency
    }
  end

  def amount_formatter
    Adyen::RequestAmountFormatter::Factory
      .for(
        payment_method: payment_method_info.payment_method_name,
        original_amount: invoice.outstanding_amount_cents,
        original_currency: person.currency,
        country_code: country_code
      )
  end

  def merchant_account
    adyen_merchant_account
  end

  def country_code
    person.country_iso_code
  end

  def unique_shopper_reference
    person.adyen_customer_reference
  end

  def payment_reference
    invoice.id
  end

  def return_url
    adyen_finalize_after_redirect_cart_url(invoice_id: invoice.id, host: person.domain, protocol: return_protocol)
  end

  def return_protocol
    Rails.env.development? ? "http" : "https"
  end

  def ideal_payment_method?
    payment_method_info.payment_method_name == AdyenPaymentMethodInfo::IDEAL_PAYMENT_METHOD
  end

  def payment_method_type
    return unless ideal_payment_method?

    SEPADIRECTDEBIT
  end

  def payment_method
    {
      type: payment_method_type,
      storedPaymentMethodId: stored_payment_method_id
    }.compact
  end
end
