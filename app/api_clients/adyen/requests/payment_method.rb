# frozen_string_literal: true

class Adyen::Requests::PaymentMethod
  attr_reader :country_code, :adyen_merchant_account

  BLOCKED_PAYMENT_METHODS = ["sepadirectdebit"]

  def initialize(options)
    @country_code = options[:country_code]
    @adyen_merchant_account = options[:merchant_account]
  end

  def url
    "paymentMethods"
  end

  def response_parser
    Adyen::Responses::PaymentMethod
  end

  def request_method
    :post
  end

  def params
    {
      countryCode: country_code,
      channel: "Web",
      merchantAccount: adyen_merchant_account,
      blockedPaymentMethods: BLOCKED_PAYMENT_METHODS
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end
end
