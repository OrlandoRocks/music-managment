# frozen_string_literal: true

class Adyen::Requests::ListRecurringDetails
  PATH = "listRecurringDetails"
  RECURRING = "RECURRING"

  def initialize(options)
    @shopper_reference = options.fetch(:shopper_reference)
    @adyen_merchant_account = options[:merchant_account]
  end

  def path
    PATH
  end

  def response_parser
    Adyen::Responses::ListRecurringDetails
  end

  def request_method
    :post
  end

  def params
    {
      recurring: {
        contract: RECURRING
      },
      shopperReference: shopper_reference,
      merchantAccount: merchant_account
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  attr_reader :shopper_reference, :adyen_merchant_account

  def merchant_account
    adyen_merchant_account
  end
end
