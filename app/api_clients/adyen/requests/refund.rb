class Adyen::Requests::Refund
  attr_reader :converted_refund_amount, :adyen_transaction, :currency, :adyen_merchant_account

  def initialize(options)
    @adyen_transaction = options[:adyen_transaction]
    @converted_refund_amount = options[:converted_refund_amount]
    @currency = options[:currency]
    @adyen_merchant_account = options[:merchant_account]
  end

  def url
    "payments/#{adyen_transaction.psp_reference}/refunds"
  end

  def response_parser
    Adyen::Responses::Refund
  end

  def request_method
    :post
  end

  def params
    {
      reference: adyen_transaction.invoice.id,
      amount: amount,
      merchantAccount: merchant_account,
    }.to_json
  end

  def attach_request_params(faraday_request)
    faraday_request.body = params
  end

  private

  def amount
    {
      value: converted_refund_amount,
      currency: currency
    }
  end

  def payment_method
    adyen_transaction.payment_method_name
  end

  def merchant_account
    adyen_merchant_account
  end
end
