class SidekiqQueueDataService
  attr_reader :batch_size

  def initialize(batch_size = 1000)
    @batch_size = batch_size
  end

  def number_by_store_in_queue(queue_name)
    Sidekiq::Queue.new(queue_name).each_slice(batch_size).reduce(Hash.new(0)) do |acc, slice|
      groups =
        slice.select { |j| j.args[0] == "Distribution" || j.args[0] == "TrackMonetization" }
             .map(&:args).group_by { |args| args[0] }
      acc.merge(get_store_count_by_args(groups)) { |_, x, y| x + y }
    end.sort_by { |_key, value| -value }.to_h
  end

  def number_by_store_in_workers
    groups =
      Sidekiq::Workers.new.select { |worker| worker[2]["queue"].include?("delivery") }
                      .map { |worker| worker[2]["payload"]["args"][0..1] }
                      .select { |job_args| job_args[0] == "Distribution" || job_args[0] == "TrackMonetization" }
                      .group_by { |args| args[0] }

    get_store_count_by_args(groups).sort_by { |_key, value| -value }.to_h
  end

  def distro_jobs_by_store_in_queue(queue_name)
    Sidekiq::Queue.new(queue_name).each_slice(batch_size).reduce({}) do |acc, slice|
      job_args = slice.reduce({}) { |acc, elem| acc.merge([elem.args[0], elem.args[1]] => elem) }
      acc.merge(get_distros_and_jobs_by_args(job_args))
    end
  end

  def distros_by_store_in_workers
    job_args =
      Sidekiq::Workers.new.select { |worker| worker[2]["queue"].include?("delivery") }
                      .reduce({}) do |acc, worker|
        args = worker[2]["payload"]["args"][0..1]
        acc.merge([args[0], args[1]] => worker)
      end
    get_distros_and_jobs_by_args(job_args)
  end

  private

  def get_store_count_by_args(args)
    distro_stores = {}
    track_mon_stores = {}

    if args["Distribution"]
      distro_ids = args["Distribution"].map { |_, id| id }
      distro_stores = Store.joins(salepoints: :distributions).where(distributions: { id: distro_ids }).group("stores.short_name").count
    end

    if args["TrackMonetization"]
      track_mon_ids = args["TrackMonetization"].map { |_, id| id }
      track_mon_stores = Store.joins(:track_monetizations).where(track_monetizations: { id: track_mon_ids }).group("stores.short_name").count
    end

    distro_stores.merge(track_mon_stores)
  end

  def get_distros_and_jobs_by_args(args)
    distro_jobs_by_store = Hash.new { |hash, key| hash[key] = [] }
    groups = args.keys.group_by { |job| job[0] }
    distro_args = groups["Distribution"]
    track_mon_args = groups["TrackMonetization"]

    if distro_args
      distro_ids = distro_args.map { |j| j[1] }
      Distribution.joins(salepoints: :store).where(distributions: { id: distro_ids }).select("distributions.*, stores.short_name as store_short_name").each do |distro|
        job = args[[distro.class.name, distro.id]]
        distro_jobs_by_store[distro.store_short_name] << [distro, job]
      end
    end

    if track_mon_args
      track_mon_id = track_mon_args.map { |j| j[1] }
      TrackMonetization.joins(:store).where(track_monetizations: { id: track_mon_id }).select("track_monetizations.*, stores.short_name as store_short_name").each do |track_monetization|
        job = args[[track_monetization.class.name, track_monetization.id]]
        distro_jobs_by_store[track_monetization.store_short_name] << [track_monetization, job]
      end
    end

    distro_jobs_by_store
  end
end
