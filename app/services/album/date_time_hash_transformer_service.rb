class Album::DateTimeHashTransformerService < Album::DateTransformerService
  def self.datetime_hash_to_iso(date_time_hash)
    return unless date_time_hash

    new(date_time_hash).datetime_hash_to_iso
  end

  def self.datetime_hash_to_sale_date(date_time_hash)
    return unless date_time_hash

    new(date_time_hash).datetime_hash_to_sale_date
  end

  def initialize(date_time_hash)
    @date_time_hash = date_time_hash.with_indifferent_access

    build_date_params
  end

  def datetime_hash_to_iso
    check_missing_params

    local_datetime_in_iso
  end

  def datetime_hash_to_sale_date
    parse_date
  end

  private

  def build_date_params
    @hour = @date_time_hash[:hour] || 0
    @minute = @date_time_hash[:min] || 0
    @time_meridian = @date_time_hash[:meridian]
    @month = @date_time_hash[:month]
    @day = @date_time_hash[:day]
    @year = @date_time_hash[:year]
  end

  def check_missing_params
    raise ArgumentError if [@month, @day, @year, @hour, @minute].any?(&:blank?)
  end
end
