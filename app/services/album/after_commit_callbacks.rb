module Album::AfterCommitCallbacks
  def self.after_commit(album)
    process_legal_review_state(album)

    if album.saved_change_to_finalized_at? && album.finalized?
      flag_duplicate_songs(album)
      create_song_fingerprints(album)
    end

    write_album_to_cache(album) if FeatureFlipper.show_feature?(:crt_cache, nil)
    process_renewal_status(album)
  end

  def self.process_legal_review_state(album)
    return unless album.saved_change_to_legal_review_state? && album.legal_review_state == "APPROVED"

    Delivery::Believe::ChangeStoreRestrictionWorker.perform_async(album.id, "allow")
  end

  def self.write_album_to_cache(album)
    Rails.logger.info "-------------#{album.id}-------------"
    caller.each { |row| Rails.logger.info row }
    Rails.logger.info "-------------#{album.id}-------------"

    if album.saved_change_to_finalized_at? && album.finalized?
      Rails.logger.info "Calling CRT write to cache for album #{album.id}"
      Rails.logger.info "Changed: #{album.changed}"
      CrtCacheWorker.perform_async(album.id)
    end

    Rails.logger.info "-------------#{album.id}-------------"
  end

  def self.flag_duplicate_songs(album)
    FlagDuplicateSongsWorker.perform_async(album.id)
  end

  def self.create_song_fingerprints(album)
    return unless scrapi_job_enabled?

    unfinalized_date = album.notes.where(subject: "Unfinalized").order("notes.created_at desc").first&.created_at
    album.songs.each do |song|
      next if song_already_processed?(song, unfinalized_date)

      Sidekiq.logger.info "Sidekiq Job initiated CreateJobWorker after_commit: #{song.id}"
      Scrapi::CreateJobWorker.perform_async(song.id)
    end
  end

  def self.process_renewal_status(album)
    return unless album.saved_change_to_legal_review_state?

    if album.legal_review_state == "REJECTED"
      album.cancel_renewal
    elsif album.legal_review_state_before_last_save == "REJECTED"
      album.reset_renewal
    end
  end

  def self.scrapi_job_enabled?
    ENV["SCRAPI_JOB_ENABLED"].to_s == "true"
  end

  def self.song_already_processed?(song, unfinalized_date)
    unfinalized_date && song.s3_asset && song.s3_asset.updated_at < unfinalized_date
  end
end
