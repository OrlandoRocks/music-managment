# frozen_string_literal: true

class Album::PreorderPricingService
  include InvoicesHelper
  include ApplicationHelper

  attr_reader :album, :count, :user

  def self.call(album, count = nil, user = nil)
    user = album.person if user.blank?
    new(album, count, user).call
  end

  def initialize(album, count, user)
    @album = album
    @count = count
    @user = user
  end

  def call
    if count.blank? && album.preorder_purchase.blank?
      0.to_money(album.person.person_balance.currency)
    else
      preorder_with_discount
    end
  end

  def preorder_with_discount
    return invoice_money(original_price.to_money) if offer.blank?

    money_with_discount(original_price, discounted_price, product.currency)
  end

  private

  def original_price
    @original_price ||=
      begin
        case count
        when 1
          Product.price(
            user,
            PreorderPurchase.new(album_id: album.id, itunes_enabled: false, google_enabled: true), product
          )
        when 2
          Product.price(
            user,
            PreorderPurchase.new(album_id: album.id, itunes_enabled: true, google_enabled: true), product
          )
        else
          album.preorder_purchase
          Product.price(album.person, album.preorder_purchase, product)
        end
      end
  end

  def offer
    @offer ||= TargetedProduct.targeted_product_for_active_offer(user, product)
  end

  def discounted_price
    @discounted_price ||= offer.adjust_price(original_price)
  end

  def product
    @product ||= album.find_correct_preorder_product
  end
end
