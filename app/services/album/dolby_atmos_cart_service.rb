class Album::DolbyAtmosCartService
  include AfterCommitEverywhere
  attr_reader :person, :album

  def self.call(person, album)
    new(person, album).call
  end

  def initialize(person, album)
    @person = person
    @album = album
  end

  def call
    return :no_dolby_tracks unless album.dolby_atmos_audios?

    ApplicationRecord.transaction do
      after_rollback { return :failure }
      after_commit { return :success }

      album.songs.joins(:spatial_audio).each do |song|
        Product.add_to_cart(person, song.spatial_audio, person.dolby_atmos_product)
      end
    end
  end
end
