class Album::DateTransformerService
  TIMED_RELEASE_FORMAT = "%m %d, %Y %H %M %p".freeze

  def initialize(record)
    @record = record
    build_golive_params if @record.golive_date.present?
  end

  def self.translate_release_date_time(record)
    return unless record

    new(record).translate_release_date_time
  end

  def self.convert_to_hash(datetime)
    {
      hour: datetime.strftime("%I"),
      min: datetime.strftime("%M"),
      meridian: datetime.strftime("%p"),
      day: datetime.day,
      month: datetime.month,
      year: datetime.year
    }
  end

  def build_golive_params
    date_obj = @record.golive_date

    date_obj = self.class.convert_to_hash(date_obj) unless date_obj.is_a?(Hash)

    @hour = date_obj[:hour]
    @minute = date_obj[:min]
    @time_meridian = date_obj[:meridian]
    @month = date_obj[:month]
    @day = date_obj[:day]
    @year = date_obj[:year]
  end

  def translate_release_date_time
    check_missing_params

    local_datetime_in_iso
  end

  def check_missing_params
    raise ArgumentError if [@month, @day, @year, @hour, @minute].any?(&:blank?)
  end

  def parse_date
    Time.zone = "Eastern Time (US & Canada)"
    input_time = Time.zone.parse("#{@year}-#{@month}-#{@day} #{@hour}:#{@minute} #{@time_meridian}")

    raise ArgumentError, I18n.t("album_timed_release_fields.error_message") if @day.to_i != input_time.day

    input_time
  end

  def local_datetime_in_iso
    parse_date.getlocal.iso8601
  end

  def self.convert_eastern_date_to_localtime(input_date)
    Time.find_zone("Eastern Time (US & Canada)").parse(input_date.to_s).getlocal.iso8601
  end
end
