class Album::CompletionReport
  attr_reader :song_data_reports

  def initialize(person_id, album_id, song_data_reports)
    @person_id         = person_id
    @album_id          = album_id
    @song_data_reports = song_data_reports
  end

  def songs_complete?
    song_data_reports.present? && song_data_reports.all?(&:completed_required_fields?)
  end
end
