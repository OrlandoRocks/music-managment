# frozen_string_literal: true

# Service class to handle view logic related to Album sidebar
class Album::DistributionUIService
  include DistributionHelper
  include StoreAutomatorHelper
  include SpecializedReleasesHelper

  attr_reader :album, :user, :has_plan, :available_credit, :ready, :distributed, :credit_usage

  PARTIALS = {
    ready_already_credited: "album/already_carted",
    ready_credit: "album/select_price",
    ready_no_credit: "album/select_price",
    not_ready_credit: "album/existing_credit",
    distributed: "album/distributed_price",
    not_ready_plan: "album/existing_plan",
    ready_plan: "album/select_price"
  }.freeze

  HEADER = "album/addon_header"
  ATMOS = "album/dolby_atmos_items"
  AUTOMATOR = "album/automator_addon"
  PREORDER = "album/preorder_addon"
  BOOKLET = "album/booklet_addon"

  ATMOS_ADDONS = [HEADER, ATMOS].freeze
  ATMOS_NO_HEADER_ADDONS = [ATMOS].freeze
  ATMOS_AND_AUTOMATOR_ADDONS = [HEADER, AUTOMATOR, ATMOS].freeze
  ALL_ADDONS = [HEADER, AUTOMATOR, ATMOS, PREORDER, BOOKLET].freeze

  ADDONS = {
    ready_already_credited: ATMOS_ADDONS,
    ready_credit: ALL_ADDONS,
    ready_no_credit: ALL_ADDONS,
    not_ready_credit: ATMOS_ADDONS,
    distributed: ATMOS_AND_AUTOMATOR_ADDONS,
    not_ready_plan: ATMOS_NO_HEADER_ADDONS,
    ready_plan: ALL_ADDONS
  }.freeze

  ADDON_LOCAL_KEYS = [
    :album,
    :automator,
    :automator_product,
    :automator_price,
    :preorder_price,
    :show_automator
  ].freeze

  DISPLAY_STATES = [
    :distributed,
    :ready_already_credited,
    :ready_plan,
    :not_ready_plan,
    :ready_no_credit,
    :ready_credit,
    :not_ready_credit,
    :not_ready_no_credit
  ].freeze

  def initialize(album, user)
    @album = album
    @user = user || album.person
    setup_album_vars
  end

  def self.call(album, user = nil)
    new(album, user).call
  end

  def compute_partial_locals
    common_locals = {
      album: album,
      show_automator: false
    }
    common_locals.merge(partial_specific_locals)
  end

  def call
    # early return condition to defer to AdvertisementHelper for view logic
    return { display_state: album_purchase_display_state } if album_purchase_display_state == :not_ready_no_credit

    {
      display_state: album_purchase_display_state,
      partial: PARTIALS[album_purchase_display_state],
      addons: ADDONS[album_purchase_display_state],
      locals: locals,
      addon_locals: locals.filter { |local| add_on_local?(local) }
    }
  end

  # for included helpers that expect current_user to be defined
  def current_user
    user
  end

  def album_purchase_display_state
    @album_purchase_display_state ||= compute_display_state
  end

  def locals
    @locals ||= compute_partial_locals
  end

  def distribution_products
    @distribution_products ||= filtered_distribution_products(album, user)
  end

  def automator_product
    @automator_product ||= get_automator_product(user).first
  end

  def automator_price
    @automator_price ||= automator_price_with_discount(album, user)
  end

  def preorder_price
    @preorder_price ||= Album::PreorderPricingService.call(album)
  end

  def automator
    @automator ||= album.salepoint_subscription
  end

  def salepoints_to_purchase
    @salepoints_to_purchase ||= Salepoint.salepoints_to_add_to_cart(album)
  end

  def price
    @price ||= Product.price(user, salepoints_to_purchase)
  end

  def store_names
    @store_names ||= salepoints_to_purchase.map { |s| s.store.name }.sort
  end

  def inventory_count
    @inventory_count ||= CreditUsage.number_credits_available(user, album.class.name)
  end

  private

  def setup_album_vars
    @has_plan = user.owns_or_carted_plan?
    @available_credit = inventory?(album, user) || has_plan
    @ready = album.should_finalize? && album_songs_status(album, user)
    @distributed = album.payment_applied?
    @credit_usage = CreditUsage.credit_usage_for(user, album)
  end

  # boolean methods to determine display state

  def distributed?
    distributed
  end

  def ready_already_credited?
    ready && credit_usage.present?
  end

  def ready_plan?
    ready && has_plan
  end

  def not_ready_plan?
    !ready && has_plan
  end

  def ready_no_credit?
    ready && (freemium? || !available_credit)
  end

  def ready_credit?
    ready && available_credit
  end

  def not_ready_credit?
    !ready && available_credit
  end

  def not_ready_no_credit?
    !ready && !available_credit
  end

  def compute_display_state
    DISPLAY_STATES.find { |sym| send(((sym.to_s + "?"))) }
  end

  def add_on_local?(local)
    ADDON_LOCAL_KEYS.include?(local)
  end

  # moved to its own service to reduce complexity here (and in case it needs to be reused elsewhere)
  def freemium?
    Album::FreemiumAlbumService.call(album)
  end

  # legacy render methods from album_helper.rb
  # These are now just returning a hash of locals for the view partials

  def ready_with_plan_or_credit_display(partial = :credit)
    credit_partial = partial == :credit
    submit_tag_key = credit_partial ? ".distribute_with_credit" : ".distribute_with_plan"
    {
      distribution_products: distribution_products,
      automator: automator,
      automator_product: automator_product,
      automator_price: automator_price,
      preorder_price: preorder_price,
      submit_tag_key: submit_tag_key,
      credit_partial: credit_partial,
      with_credit: true,
      show_automator: show_automator_addon?(album)
    }
  end

  def distributed_display
    {
      price: price,
      salepoints: salepoints_to_purchase,
      album: album,
      store_names: store_names,
      automator: automator,
      automator_product: automator_product,
      automator_price: automator_price,
      show_automator: show_automator_addon_if_finalized?(album, automator)
    }
  end

  def ready_without_credit_display
    {
      products: distribution_products,
      automator: automator,
      automator_product: automator_product,
      automator_price: automator_price,
      preorder_price: preorder_price,
      with_credit: false,
      show_automator: show_automator_addon?(album)
    }
  end

  def not_ready_with_credit_display
    {
      inventory_count: inventory_count,
    }
  end

  def partial_specific_locals
    case album_purchase_display_state
    when :ready_credit
      ready_with_plan_or_credit_display
    when :ready_no_credit
      ready_without_credit_display
    when :not_ready_credit
      not_ready_with_credit_display
    when :distributed
      distributed_display
    when :ready_plan
      ready_with_plan_or_credit_display(:plan)
    else
      {}
    end
  end
end
