# frozen_string_literal: true

class Album::FreemiumAlbumService
  attr_reader :person, :album

  def self.call(album, person = nil)
    person = album.person if person.blank?
    new(album, person).call
  end

  def initialize(album, person)
    @album = album
    @person = person
  end

  def call
    freemium?
  end

  private

  def freemium?
    tiktok_freemium? || general_discovery? || facebook_freemium?
  end

  def facebook_freemium?
    FeatureFlipper.show_feature?(
      :freemium_flow,
      person
    ) && facebook_offer_active? && album.strictly_facebook_release?
  end

  def offer_active?(product_name)
    TargetedProductStore.active_offer?(person.country_website_id, product_name)
  end

  def tiktok_offer_active?
    @tiktok_offer_active ||= offer_active?(:tiktok)
  end

  def tiktok_freemium?
    (FeatureFlipper.show_feature?(:tiktok_releases, person) ||
        FeatureFlipper.show_feature?(:tiktok_freemium, person)) &&
      tiktok_offer_active? &&
      # is_tiktok_release?  method does not exist - why are we checking for it?
      # updated to try to not cause errors but will never be true
      album.try(:is_tiktok_release?)
  end

  # change to discovery platform nomenclature
  def discovery_offer_active?
    @discovery_offer_active ||= offer_active?(:discovery_platforms)
  end

  def facebook_offer_active?
    @facebook_offer_active ||= offer_active?(:facebook)
  end

  def general_discovery?
    FeatureFlipper.show_feature?(:discovery_platforms, person) &&
      discovery_offer_active? &&
      album.is_general_discovery_release?(person)
  end
end
