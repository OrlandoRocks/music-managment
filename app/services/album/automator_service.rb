class Album::AutomatorService
  attr_reader :album, :deliver_automator

  def self.update_automator(album, deliver_automator)
    new(album, deliver_automator).update_automator
  end

  def self.add_and_finalize_automator(album, deliver_automator = true)
    new(album, deliver_automator).add_and_finalize_automator
  end

  def initialize(album, deliver_automator)
    @album             = album
    @deliver_automator = deliver_automator
  end

  def update_automator
    if deliver_automator
      album.add_automator
    elsif remove_automator?
      album.salepoint_subscription.destroy
    end
  end

  def add_and_finalize_automator
    salepoint_subscription = album.add_automator
    salepoint_subscription.finalize! if salepoint_subscription
  end

  private

  def remove_automator?
    album.salepoint_subscription && !album.salepoint_subscription.finalized?
  end
end
