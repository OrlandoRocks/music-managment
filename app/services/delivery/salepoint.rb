class Delivery::Salepoint
  def self.deliver(salepoint_id)
    new(salepoint_id).tap(&:deliver)
  end

  def initialize(salepoint_id)
    @salepoint    = Salepoint.find(salepoint_id)
    @petri_bundle = PetriBundle.where(album_id: @salepoint.salepointable_id).first
    @salepoints   = [@salepoint]
  end

  def deliver
    @salepoint.update_post_purchase unless @salepoint.payment_applied
    @petri_bundle.add_salepoints(@salepoints, actor: "Delivery::Salepoint#deliver")
  end
end
