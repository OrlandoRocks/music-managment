class Delivery::Believe
  def self.deliver(distribution_id, album_id, person_id)
    new(distribution_id, album_id, person_id).tap(&:deliver)
  end

  attr_accessor :distribution, :album, :person

  def initialize(distribution_id, album_id, person_id)
    @distribution = Distribution.find(distribution_id)
    @album        = Album.find(album_id)
    @person       = Person.find(person_id)
  end

  def deliver
    return unless something_has_been_updated_after_last_distribution

    distribution.retry(
      actor: person.name.to_s,
      message: "Sending distribution #{distribution.id}"
    )
  end

  private

  def something_has_been_updated_after_last_distribution
    !versions_match_for?(album, distribution.updated_at) ||
      !versions_match_for_collection?(album.songs, distribution.updated_at) ||
      !versions_match_for_collection?(song_distribution_options, distribution.updated_at)
  end

  def versions_match_for_collection?(collection, previous_time)
    collection.select { |item| !versions_match_for?(item, previous_time) }.any?
  end

  def versions_match_for?(item, previous_time)
    item.paper_trail.version_at(previous_time).attributes.values == item.attributes.values
  end

  def song_distribution_options
    SongDistributionOption.joins(song: :album).where(Album.arel_table[:id].eq(album.id))
  end
end
