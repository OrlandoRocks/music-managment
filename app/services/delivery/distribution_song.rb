class Delivery::DistributionSong
  def self.deliver(salepoint_song_id)
    new(salepoint_song_id).deliver
  end

  attr_reader :salepoint_song,
              :song,
              :petri_bundle,
              :album,
              :salepoint,
              :distribution,
              :distribution_song,
              :converter_class

  def initialize(salepoint_song_id)
    @salepoint_song  = SalepointSong.find(salepoint_song_id)
    @song            = salepoint_song.song
    @petri_bundle    = salepoint_song.song.album.petri_bundle
    @album           = salepoint_song.song.album
    @salepoint       = salepoint_song.salepoint
    @converter_class = salepoint.store.converter_class
  end

  def deliver
    return if song.ytm_blocked_song

    find_or_create_distribution
    add_salepoint_to_distribution
    create_distribution_song
    deliver_distribution_song
  end

  def find_or_create_distribution
    @distribution = petri_bundle.distributions.where(converter_class: converter_class).first_or_create!
  end

  private

  def add_salepoint_to_distribution
    distribution.salepoints << salepoint unless distribution.salepoints.include?(salepoint)
  end

  def create_distribution_song
    @distribution_song = ::DistributionSong.where(
      distribution_id: distribution.id,
      salepoint_song_id: salepoint_song.id
    ).first_or_create!
  end

  def deliver_distribution_song
    distribution_song.start(actor: "System", message: "Starting distribution") if album.ready_for_distribution?
  end
end
