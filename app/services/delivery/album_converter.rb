class Delivery::AlbumConverter
  def self.convert(distrubtable, delivery_type_param = nil)
    new(distrubtable, delivery_type_param).convert
  end

  def initialize(distributable, delivery_type_param = nil)
    @distributable  = distributable
    @tunecore_album = @distributable.album
    @store          = @distributable.store
    @delivery_type  = delivery_type_param
  end

  def convert
    begin
      conversion_args = send("#{@distributable.class.name.underscore}_conversion_args")
      converter.convert(*conversion_args)
    rescue DistributionSystem::GenreNotSupported => e
      @distributable.errored message: "Genre not supported by store: #{e.message}", backtrace: e.backtrace
    end
  end

  def has_valid_converter_class?
    if @distributable.respond_to?(:converter_class)
      Object.const_defined?(tunecore_converter_class(@distributable.converter_class))
    else
      Object.const_defined?(tunecore_converter_class(@store.converter_class))
    end
  end

  def distribution_conversion_args
    [@tunecore_album, @distributable.salepoints, get_delivery_type]
  end

  def distribution_song_conversion_args
    [
      @tunecore_album,
      [@distributable.salepoint_song.salepoint],
      get_delivery_type,
      [@distributable.salepoint_song.song]
    ]
  end

  def track_monetization_conversion_args
    [@distributable, get_delivery_type]
  end

  def get_delivery_type
    @delivery_type || @distributable.delivery_type
  end

  def converter
    tunecore_converter_class(@store.converter_class).constantize.new
  end

  def tunecore_converter_class(converter_class)
    "DistributionSystem::#{converter_type(converter_class)}::Converter"
  end

  def converter_type(converter_class)
    converter_class.split("::")[1]
  end
end
