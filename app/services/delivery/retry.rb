class Delivery::Retry
  def self.retry(params = {})
    new(params).tap(&:retry)
  end

  def initialize(params = {})
    @delivery      = params[:delivery] # distribution or track_monetization
    @priority      = params[:priority].to_i
    @person        = params[:person]
    @actor         = params[:actor]
    @delivery_type = params[:delivery_type] || "full_delivery"
    @mass_retry    = params[:mass_retry]
  end

  attr_accessor :delivery, :priority, :person, :delivery_type

  def retry
    return unless retry_required?

    delivery.delivery_type = delivery_type
    if priority == 1
      Rails.logger.info("MASSRETRIES: Delivery::Retry.retry delivery = #{delivery.inspect}")
      if delivered_by_tc_distributor?
        if delivery.instance_of?(TrackMonetization)
          retry_delivery
        else
          delivery.update(state: "delivered_via_tc_distributor")
        end
      else
        Delivery::DistributionCleanupWorker.perform_async(delivery.class.name, delivery.id)
        Delivery::DistributionWorker.set(
          queue: "delivery-critical"
        ).perform_async(
          delivery.class.name,
          delivery.id,
          "full_delivery"
        )
        delivery.update(state: "enqueued")
      end
      send_sns_notification(urgent: true) if send_sns?

    else
      retry_delivery
      send_sns_notification if send_sns?
    end
  end

  def retry_delivery
    delivery.retry(
      actor: actor_message,
      message: "Retrying delivery #{delivery.id} (#{delivery_type})"
    )
  end

  def send_sns?
    FeatureFlipper.show_feature?(:use_sns, @person || @actor) && !delivery.instance_of?(TrackMonetization)
  end

  private

  def delivered_by_tc_distributor?
    (
      !delivery.instance_of?(TrackMonetization) &&
      tc_distributor_delivery?(:tc_distributor_album_cutover_users)
    ) ||
      (
        delivery.instance_of?(TrackMonetization) &&
        tc_distributor_delivery?(:tc_distributor_track_cutover_users)
      )
  end

  def tc_distributor_delivery?(tc_distributor_feature_flag)
    # rubocop:disable Style/GlobalVars
    delivery.store.delivered_by_tc_distributor? ||
      (
        delivery.store.delivered_by_tunecore? &&
        FeatureFlipper.show_feature?(tc_distributor_feature_flag, person || @actor) &&
        $redis.smembers(:TC_DISTRIBUTOR_CUTOVER_STORES).include?(store_id.to_s)
      )
    # rubocop:enable Style/GlobalVars
  end

  def retry_required?
    delivery.is_a?(TrackMonetization) || delivery.retriable?
  end

  def actor_message
    @person ? "Tunecore admin: #{person.name}" : @actor
  end

  def send_sns_notification(urgent: false)
    return if @mass_retry.present?

    topic_arn = urgent ? ENV["SNS_RELEASE_UPDATE_URGENT_TOPIC"] : ENV["SNS_RELEASE_UPDATE_TOPIC"]
    Sns::Notifier.perform(
      topic_arn: topic_arn,
      album_ids_or_upcs: [@delivery.album.id],
      store_ids: [dual_delivery_store_id(store_id)],
      person_id: (@person || @actor)&.id,
      delivery_type: delivery_type
    )
  end

  def store_id
    return @delivery&.salepoints&.first&.store_id if @delivery.is_a?(Distribution)

    @delivery.store_id # @delivery is track_monetization
  end

  def dual_delivery_store_id(store_id)
    return store_id unless store_id.to_i.in?(Store::TRACK_MONETIZATION_STORES)

    Store::DUAL_DELIVERY_STORE_IDS[store_id]
  end
end
