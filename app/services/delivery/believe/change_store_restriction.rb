class Delivery::Believe::ChangeStoreRestriction
  def self.change_store_restriction(album_id, delivery_type, finalized_store_ids = nil)
    $believe_api_client.post(
      "delivery/changestorerestriction",
      {
        idAlbumRemote: album_id,
        idPlatformRemote: finalized_store_ids || Album.find(album_id).get_finalized_store_ids,
        actionType: delivery_type,
        actionTimestamp: Time.now.utc.to_i.to_s
      }
    )
  end
end
