class Delivery::Believe::NotifyDelivery
  def self.notify_delivery(album_id, store_ids, delivery_type = "INSERT")
    $believe_api_client.post(
      "delivery/notify",
      {
        idAlbumRemote: album_id,
        idPlatformRemote: store_ids,
        actionType: delivery_type
      }
    )
  end
end
