class Delivery::Distribution
  def self.deliver(distributable_type, distributable_id, delivery_type = nil, queue = nil, job_id = nil)
    new(distributable_type, distributable_id, delivery_type, queue, job_id).tap(&:deliver)
  end

  attr_reader :distributable_type, :distributable_id, :delivery_type, :queue, :distributable

  def initialize(distributable_type, distributable_id, delivery_type = nil, queue = nil, job_id = nil)
    @distributable_type = distributable_type
    @distributable_id   = distributable_id
    @delivery_type      = delivery_type
    @queue              = queue
    @distributable      = distributable_type.constantize.find(distributable_id)

    return unless job_id

    @distributable.job_id = job_id
    @distributable.save
  end

  def deliver
    distributable.deliver(delivery_type, queue)
  end
end
