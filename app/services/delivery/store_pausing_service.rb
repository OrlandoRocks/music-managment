class Delivery::StorePausingService
  attr_reader :store, :distributable_ids, :distributable_class

  delegate :store_delivery_config, :distributable_class, to: :store

  QUEUES = [
    Sidekiq::DeadSet.new,
    Sidekiq::ScheduledSet.new,
    Sidekiq::RetrySet.new
  ].freeze

  RETRIABLE_STATES = %w[enqueued error]

  def self.pause!(store_id)
    new(store_id).pause!
  end

  def initialize(store_id)
    @store = Store.find(store_id)
  end

  def pause!
    pause_distributions!
    delete_jobs_from_queue!
  end

  private

  def delete_jobs_from_queue!
    Sidekiq::Queue.new(@store.delivery_queue).each do |job|
      distributable_job_class, distributable_job_id = job.args
      job.delete if distributable_job_class == distributable_class && distributable_ids.include?(distributable_job_id)
    end
  end

  def pause_distributions!
    distributables.each do |distributable|
      distributable.pause(actor: "Delivery::StorePausingService", message: "Pausing store #{store.short_name}")
    end
  end

  def distributables
    @distributables ||= store.send(distributable_class.tableize).where(state: RETRIABLE_STATES)
  end

  def distributable_ids
    @distributable_ids ||= distributables.map(&:id)
  end
end
