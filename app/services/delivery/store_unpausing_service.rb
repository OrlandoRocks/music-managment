class Delivery::StoreUnpausingService
  attr_accessor :store, :person_id, :remote_ip

  PAUSED_SNS_QUEUE = "delivery-sns-pause".freeze

  def self.schedule_backfill!(store_id, person_id, remote_ip)
    new(store_id, person_id, remote_ip).schedule_backfill!
  end

  def initialize(store_id, person_id, remote_ip)
    @store = Store.find(store_id)
    @person_id = person_id
    @remote_ip = remote_ip
    @album_ids = []
    @sidekiq_job_ids = []
  end

  def schedule_backfill!
    return if store.paused?

    schedule_tc_distributor_paused_distributions!
  end

  private

  def schedule_tc_distributor_paused_distributions!
    fetch_albums_from_pause_queue

    return if @album_ids.empty?

    # Add to REDIS for batch_retrier to start picking these jobs
    add_albums_to_batch_retrier_redis

    delete_jobs_from_pause_queue
  end

  def fetch_albums_from_pause_queue
    Sidekiq::Queue.new(PAUSED_SNS_QUEUE).each do |job|
      if store.id == job.args[1]
        @album_ids += job.args[0]
        @sidekiq_job_ids.push(job.jid)
      end
    end
  end

  def add_albums_to_batch_retrier_redis
    key = uuid.to_s + job_name
    # rubocop:disable Style/GlobalVars
    $redis.rpush("distribution:batch-retry-albums:#{uuid}", @album_ids.uniq)
    $redis.hmset(
      # rubocop:enable Style/GlobalVars
      "distribution:batch-retry:#{key}",
      "person_id",
      person_id,
      "store_ids",
      [store.id],
      "daily_retry_limit",
      daily_retry_limit,
      "remote_ip",
      remote_ip,
      "current_index",
      0,
      "uuid",
      uuid,
      "job_name",
      job_name,
      "use_sns_only",
      true
    )
  end

  def uuid
    @uuid ||= SecureRandom.uuid
  end

  def job_name
    @job_name ||= "StoreUnpause#{store.name.capitalize}#{Time.now.to_i}"
  end

  def daily_retry_limit
    ENV.fetch("BATCH_RETRY_DAILY_LIMIT", Store::BATCH_RETRY_DAILY_LIMIT_DEFAULT)
  end

  def delete_jobs_from_pause_queue
    Sidekiq::Queue.new(PAUSED_SNS_QUEUE).each do |job|
      job.delete if @sidekiq_job_ids.include?(job.jid)
    end
  end
end
