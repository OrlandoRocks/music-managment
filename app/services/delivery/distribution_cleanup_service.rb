class Delivery::DistributionCleanupService
  def self.cleanup(delivery_class, delivery_id)
    new(delivery_class, delivery_id).cleanup
  end

  attr_reader :delivery_class, :delivery_id

  def initialize(delivery_class, delivery_id)
    @delivery_class = delivery_class
    @delivery_id = delivery_id
  end

  def cleanup
    job = nil
    [
      Sidekiq::ScheduledSet.new,
      Sidekiq::DeadSet.new,
      Sidekiq::RetrySet.new,
      Sidekiq::Queue.new("delivery-default"),
      Sidekiq::Queue.new("delivery-low"),
      Sidekiq::Queue.new("delivery-pause")
    ].each do |queue|
      job = find_job(queue)
      break if job
    end
    job.try(:delete)
  end

  def find_job(queue)
    queue.find do |job|
      job.args[0] == delivery_class && job.args[1] == delivery_id
    end
  end
end
