class Delivery::Album
  class AlbumNotUnderReviewError < StandardError; end

  def self.deliver(album_id)
    new(album_id).tap(&:deliver)
  end

  def initialize(album_id)
    @album      = Album.find(album_id)
    @salepoints = @album.salepoints
                        .joins("left join inventory_usages on related_id = salepoints.id AND related_type = 'Salepoint'")
                        .where("inventory_usages.id IS NOT NULL AND finalized_at IS NULL").readonly(false)
  end

  def deliver
    @album.mark_as_needs_review!
    deliver_salepoints
    @album.calculate_steps_required
  end

  def deliver_salepoints
    raise AlbumNotUnderReviewError.new("Album: #{@album.id} not under review") unless @album.reload.under_review?

    @album.salepoints_to_distribute(@salepoints).each do |salepoint|
      Delivery::Salepoint.deliver(salepoint.id)
    end
  end
end
