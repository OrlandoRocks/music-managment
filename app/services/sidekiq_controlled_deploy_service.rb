class SidekiqControlledDeployService
  PAUSE_WORKER_FILE_PATH = "#{Rails.root}/tmp/pause_worker_args.dump"
  def self.pause!
    ["delivery-critical", "delivery-default"].each { |q| Sidekiq::Queue.new(q).pause! }
  end

  def self.write_inflight_workers
    pause_args = Sidekiq::Workers.new.map { |_, _, w| { queue: w["queue"], args: w["payload"]["args"] } }
    marshal_dump = Marshal.dump(pause_args)
    File.open(PAUSE_WORKER_FILE_PATH, "wb") do |file|
      file.write(marshal_dump)
    end
  end

  def self.reenqueue_inflight_workers
    pause_args = Marshal.load(File.read(PAUSE_WORKER_FILE_PATH)).map(&:with_indifferent_access)
    pause_args.each do |obj|
      Delivery::DistributionWorker.set(queue: obj[:queue], unique_for: false).perform_async(
        obj[:args][0],
        obj[:args][1]
      )
    end

    File.delete(PAUSE_WORKER_FILE_PATH)
  end

  def self.unpause!
    ["delivery-critical", "delivery-default"].each { |q| Sidekiq::Queue.new(q).unpause! }
  end
end
