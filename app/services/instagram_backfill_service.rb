class InstagramBackfillService
  attr_accessor :eligible_albums, :store, :person_id, :messages, :limit

  def self.backfill_all
    new.tap(&:find_and_backfill)
  end

  def self.limited_backfill(limit = 1000)
    new(limit: limit).tap(&:limited_backfill)
  end

  def self.backfill_by_person(person_id)
    new(person_id: person_id).tap(&:find_and_backfill_by_person)
  end

  def initialize(args = {})
    @person_id = args[:person_id]
    @limit = args[:limit]
    @store = Store.find_by(short_name: "Instagram")
  end

  def find_and_backfill
    collect_eligible_albums
    add_store_to_albums
    distribute_salepoints
  end

  def limited_backfill
    @eligible_albums = InstagramQueryBuilder.eligible_needing_backfill.limit(limit)
    add_store_to_albums
    distribute_salepoints
  end

  def collect_eligible_albums
    @eligible_albums = InstagramQueryBuilder.eligible_needing_backfill
  end

  def add_store_to_albums
    @eligible_albums.each do |album|
      album.add_stores([@store])
    end
  end

  def find_and_backfill_by_person
    @eligible_albums = InstagramQueryBuilder.eligible_by_person(person_id)
    add_store_to_albums
    distribute_salepoints
  end

  def distribute_salepoints
    eligible_albums.each do |album|
      sp = album.salepoints.where(store_id: store.id).last
      album.petri_bundle.add_salepoints([sp])
    end
  end
end
