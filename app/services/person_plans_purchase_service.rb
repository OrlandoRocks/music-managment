# frozen_string_literal: true

class PersonPlansPurchaseService
  class PersonPlansPurchaseServiceError < StandardError; end

  def self.process(invoice)
    new(invoice).tap(&:process)
  end

  attr_reader :invoice

  def initialize(invoice)
    @invoice = invoice
  end

  def process
    invoice.purchases.each do |purchase|
      begin
        purchase.related.mark_as_paid if purchase.plan_addon?
        log_plan_credit_usage_purchase(purchase) if purchase.plan_credit?

        next unless purchase.is_plan?

        person = purchase.person
        plan = purchase.related.try(:plan)

        PersonPlanCreationService.call(
          person_id: person.id,
          plan_id: plan.id,
          purchase_id: purchase.id
        )

        update_zendesk(person)
      rescue => e
        airbrake_and_log(e, purchase, invoice)
        next
      end
    end
  end

  def airbrake_and_log(error, purchase, invoice)
    Rails.logger.error "Failure in class PersonPlansPurchaseService#process: #{error.message}"
    Airbrake.notify(
      PersonPlansPurchaseServiceError,
      {
        invoice: invoice,
        message: error.message,
        purchase_id: purchase.id
      }
    )
  end

  def log_plan_credit_usage_purchase(purchase)
    PlanCreditUsagePurchase.log_purchase(
      {
        plan_id: purchase.person.plan.id,
        purchase_id: purchase.id,
        credit_usage_id: purchase.related.id
      }
    )
  end

  private

  def update_zendesk(person)
    Zendesk::Person.create_apilogger(person.id)
  end
end
