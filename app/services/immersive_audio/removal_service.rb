# frozen_string_literal: true

class ImmersiveAudio::RemovalService
  attr_reader :immersive_audio, :s3_key, :s3_bucket

  def initialize(options)
    @immersive_audio = ImmersiveAudio.find_by(id: options[:immersive_audio_id])
    @s3_key, @s3_bucket = s3_attrs(options)
  end

  def call
    return unless (s3_key && s3_bucket)

    S3_CLIENT.client.delete_object(
      bucket_name: s3_bucket, key: s3_key
    )
    Rails.logger.info "Deleting ImmersiveAudio from S3: #{s3_bucket + '/' + s3_key}"
    immersive_audio.destroy! if immersive_audio
  end

  private

  def s3_attrs(options)
    return [options[:key], options[:bucket]] unless immersive_audio

    [immersive_audio.s3_asset.key, immersive_audio.s3_asset.bucket]
  end
end
