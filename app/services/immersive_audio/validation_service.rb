class ImmersiveAudio::ValidationService
  include S3MetadataFetchable
  attr_reader :s3_stereo_asset, :key, :bucket, :immersive_duration, :stereo_duration, :audio_type
  attr_accessor :validation_errors

  VALIDATIONS_BY_AUDIO_TYPE = {
    SpatialAudio::AUDIO_TYPE => [:validate_duration, :validate_file_type]
  }

  VALIDATION_ERRORS = {
    validate_duration: :immersive_duration_error,
    validate_file_type: :atmos_file_type_error
  }

  MIN_DURATION_DIFFERENCE = 50

  def initialize(s3_stereo_asset, key, bucket, audio_type)
    @s3_stereo_asset = s3_stereo_asset
    @key = key
    @bucket = bucket
    @audio_type = audio_type
    @validation_errors = {}
    @stereo_duration = fetch_stereo_duration
    @immersive_duration = fetch_immersive_duration
  end

  def call
    VALIDATIONS_BY_AUDIO_TYPE[audio_type].each do |action|
      validation_errors[action] = VALIDATION_ERRORS[action] unless send(action)
    end

    {
      valid: validation_errors.empty?,
      errors: validation_errors
    }
  end

  private

  # validates duration of both audio files within in 50 milliseconds of each other
  def validate_duration
    (stereo_duration - immersive_duration).abs < MIN_DURATION_DIFFERENCE
  end

  def fetch_immersive_duration
    # calls method in s3_metadata_fetchable.rb
    immersive_metadata = fetch_asset_metadata
    extract_duration_in_ms(immersive_metadata)
  end

  def fetch_stereo_duration
    # when user replaces stereo file
    # validation can be invoked from AssetsUploader::Transcoder#validate_asset
    # in this case the s3_asset record will not be persisted
    if set_asset_metadata?
      # if the async process GetMetadataPriorityWorker took too long call it here
      s3_stereo_asset.set_asset_metadata!
      s3_stereo_asset.reload.s3_detail.metadata_json
    end

    stereo_metadata =
      if s3_stereo_asset.s3_detail
        s3_stereo_asset.reload.s3_detail.metadata_json
      else
        s3_stereo_asset.fetch_asset_metadata
      end

    extract_duration_in_ms(stereo_metadata)
  end

  def extract_duration_in_ms(metadata)
    Float(metadata["streams"][0]["duration"]) * 1000
  end

  def set_asset_metadata?
    # will return true if GetMetadataPriorityWorker has not yet executed
    # GetMetadataPriorityWorker is async, there is a possiblity it has not completed for initial upload
    # if initial file is replaced with new file then un-updated metadata may exist
    (s3_stereo_asset.persisted? && !s3_stereo_asset.s3_detail) ||
      (s3_stereo_asset.persisted? && (s3_stereo_asset.updated_at > s3_stereo_asset.s3_detail.created_at))
  end

  def validate_file_type
    amtos_file_format = (media_metadata&.dig("media", "track") || [])[-1]&.dig("extra", "AdmProfile_Format")

    amtos_file_format.present?
  end

  def media_metadata
    return @media_metadata if @media_metadata

    s3_object = Aws::S3::Resource.new.bucket(bucket).object(key)
    presigned_get_url = s3_object.presigned_url(:get, expires_in: 3600)
    output = `mediainfo --Output=JSON "#{presigned_get_url}"`
    @media_metadata = JSON.parse(output)
  end
end
