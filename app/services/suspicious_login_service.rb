class SuspiciousLoginService
  PAST_EVENTS_TO_CHECK = 5
  MIN_PAST_EVENTS_THRESHOLD = 2

  def initialize(login_event_id)
    @login_event = LoginEvent.find_by(id: login_event_id)
  end

  def investigate
    return if @login_event.blank? || incomplete_required_info?
    return if @login_event.person.two_factor_auth&.active?
    return if @login_event.person.suspicious_email_sent_recently?

    past_events = get_past_events
    return if past_events.size < MIN_PAST_EVENTS_THRESHOLD
    return if past_events.any? do |event|
      event.city == @login_event.city && event.country == @login_event.country
    end

    send_suspicious_email
  end

  private

  def incomplete_required_info?
    @login_event.city.blank? || @login_event.country.blank?
  end

  def get_past_events
    LoginEvent
      .where(person_id: @login_event.person_id)
      .where.not(city: nil, country: nil)
      .where.not(id: @login_event.id)
      .order(id: :desc)
      .limit(PAST_EVENTS_TO_CHECK)
  end

  def send_suspicious_email
    SessionsMailer
      .suspicious_login_mailer(
        @login_event.person,
        nil,
        @login_event.created_at.in_time_zone("Eastern Time (US & Canada)"),
        email_options
      ).deliver_now
  end

  def email_options
    {
      ip_address: @login_event.ip_address,
      location: [@login_event.city, @login_event.country].join(" ")
    }
  end
end
