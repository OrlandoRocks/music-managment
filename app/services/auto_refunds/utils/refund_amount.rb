module AutoRefunds
  module Utils
    class RefundAmount
      def initialize(request_amount, purchase, tax_inclusive, refund_type)
        @request_amount = request_amount
        @purchase = purchase
        @tax_inclusive = tax_inclusive
        @refund_type = refund_type
      end

      # Tax exclusive amount will only be applicable for fixed_amount_refund,
      # because in both full_refund and percentage_refund we're calculating
      # the tax split up from total avaialbe amount.
      def base_amount_cents
        @base_amount_cents ||=
          if tax_exclusive_fixed_amount_refund?
            request_amount_cents
          else
            (request_amount_cents).fdiv(1 + tax_rate)
          end
      end

      def tax_amount_cents
        @tax_amount_cents ||= base_amount_cents * tax_rate
      end

      private

      attr_reader :request_amount, :purchase, :refund_type

      def currency
        @currency ||= purchase.currency
      end

      def tax_inclusive?
        @tax_inclusive
      end

      def refundable_amount_cents
        @refundable_amount_cents ||= purchase.refundable_amount_cents
      end

      def tax_exclusive_fixed_amount_refund?
        !tax_inclusive? && refund_type == Refund::FIXED_AMOUNT
      end

      def tax_rate
        purchase.purchase_tax_information&.tax_rate&.fdiv(100) || 0
      end

      def request_amount_cents
        @request_amount_cents ||=
          case refund_type
          when Refund::FIXED_AMOUNT
            request_amount * 100
          when Refund::FULL_REFUND
            refundable_amount_cents
          when Refund::PERCENTAGE
            refundable_amount_cents * request_amount.fdiv(100)
          end
      end
    end
  end
end
