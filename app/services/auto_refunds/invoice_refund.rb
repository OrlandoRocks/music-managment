module AutoRefunds
  class InvoiceRefund
    def initialize(refund, request_amount, refund_type)
      @refund = refund
      @request_amount = request_amount
      @refund_type = refund_type
    end

    def create!
      @amount_to_refund = refund_amount_cents

      refundable_purchases.each do |purchase|
        break unless @amount_to_refund.positive?

        line_item = create_refund_line_item(purchase)
        @amount_to_refund -= line_item.total_amount_cents
      end
    end

    private

    attr_reader :refund, :request_amount, :refund_type, :amount_to_refund

    def refundable_purchases
      @refundable_purchases ||= invoice.refundable_purchases
    end

    def invoice
      @invoice ||= refund.invoice
    end

    def refundable_invoice_amount_cents
      @refundable_invoice_amount_cents ||= invoice.refundable_amount_cents
    end

    def refundable_invoice_amount
      refundable_invoice_amount_cents.fdiv(100)
    end

    def refund_ratio
      @refund_ratio ||=
        case refund_type
        when Refund::FIXED_AMOUNT
          (request_amount).fdiv(refundable_invoice_amount)
        when Refund::FULL_REFUND
          1
        when Refund::PERCENTAGE
          request_amount.fdiv(100)
        end
    end

    def refund_amount_cents
      amount_cents = refundable_invoice_amount_cents * refund_ratio
      return amount_cents if tax_inclusive?

      (1 + invoice_tax_rate) * amount_cents
    end

    # Under the hood both invoice_level refund and line_item_level refund
    # treated as line_item_level refund with refund_type fixed_amount.
    # The amount to refund for each purchase will be in the same ratio of
    # request_amount to refundable_invoice_amount.
    # Rounding issues due to the Integer 'cents' column will be adjusted in the
    # last purchase of the invoice.
    def create_refund_line_item(purchase)
      AutoRefunds::LineItemRefund.new(
        refund,
        purchase.id,
        item_request_amount(purchase),
        Refund::FIXED_AMOUNT,
        tax_inclusive?
      ).create!
    end

    def item_request_amount(purchase)
      item_request_amount_cents(purchase).fdiv(100)
    end

    def item_request_amount_cents(purchase)
      refundable_amount_cents = purchase.refundable_amount_cents
      request_amount_cents = refundable_amount_cents * refund_ratio
      return request_amount_cents unless require_price_adjustment?(purchase, request_amount_cents)

      [
        adjusted_request_amount_cents,
        maximum_purchase_request_amount_cents(purchase)
      ].min
    end

    def adjusted_request_amount_cents
      tax_inclusive? ? amount_to_refund : tax_exclusive_amount_to_refund
    end

    def require_price_adjustment?(purchase, request_amount_cents)
      return true if last_purchase_to_refund?(purchase)

      (amount_to_refund - request_amount_cents).negative?
    end

    def tax_exclusive_amount_to_refund
      amount_to_refund.fdiv(1 + invoice_tax_rate)
    end

    # Tax exclusive amount will only be applicable for fixed_amount_refund,
    # because in both full_refund and percentage_refund we're calculating the
    # tax split up from total avaialbe amount.
    # So setting tax_inclusive as true for full_refund and percentage_refund
    # for calculation. The original value will remain in the DB.
    def tax_inclusive?
      return refund.tax_inclusive if refund_type == Refund::FIXED_AMOUNT

      true
    end

    def last_purchase_to_refund?(purchase)
      purchase.equal? refundable_purchases.last
    end

    def invoice_tax_rate
      invoice.tax_rate&.fdiv(100) || 0
    end

    def purchase_tax_rate(purchase)
      purchase.purchase_tax_information&.tax_rate&.fdiv(100) || 0
    end

    def maximum_purchase_request_amount_cents(purchase)
      return purchase.refundable_amount_cents if tax_inclusive?

      purchase.refundable_amount_cents.fdiv(1 + purchase_tax_rate(purchase))
    end
  end
end
