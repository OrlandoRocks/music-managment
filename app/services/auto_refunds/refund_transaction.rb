module AutoRefunds
  class RefundTransaction
    attr_reader :refund

    def initialize(**args)
      @invoice_id = args[:invoice_id]
      @refund_level = args[:refund_level]
      @refund_items = args[:refund_items]
      @refund_type = args[:refund_type]
      @request_amount = args[:request_amount]
      @refund_attr = args.slice(
        :invoice_id,
        :tax_inclusive,
        :refund_reason_id,
        :refunded_by_id,
        :admin_note
      )
    end

    def create_refund_record!
      self.refund = Refund.create!(refund_attr)
    end

    def create_refund_items!
      create_refund_record! if refund.blank?

      invoice_level_refund? ? create_invoice_level_refund : create_line_item_level_refund
    end

    private

    attr_writer :refund
    attr_reader :invoice_id, :refund_level, :refund_items,
                :refund_type, :request_amount, :refund_attr

    def invoice
      @invoice ||= Invoice.find_by(invoice_id)
    end

    def invoice_level_refund?
      refund_level == Refund::INVOICE
    end

    def create_line_item_level_refund
      refund_items.each do |item|
        AutoRefunds::LineItemRefund.new(
          refund,
          item[:purchase_id],
          item[:request_amount],
          item[:refund_type],
          refund.tax_inclusive
        ).create!
      end
    end

    def create_invoice_level_refund
      AutoRefunds::InvoiceRefund
        .new(refund, request_amount, refund_type)
        .create!
    end
  end
end
