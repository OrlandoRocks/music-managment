# frozen_string_literal: true

class AutoRefunds::TransactionsService
  DEFAULT_PAGE = 1
  DEFAUT_PER_PAGE = 100
  MAX_PER_PAGE = 5000
  SORT_ORDERS = ["asc", "desc"]
  SORT_COLUMNS = ["id", "settled_at", "final_settlement_amount_cents"]

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def invoices_for_refund
    Invoice.joins(:person)
           .paid
           .where(fetch_query, email_or_name: email_or_name)
           .order(sorted_by)
           .paginate(page: page, per_page: per_page)
           .select(fields_to_select)
  end

  private

  def email_or_name
    "#{@params[:email_or_name]}%"
  end

  def sorted_by
    return "id asc" unless valid_order?

    "#{@params[:sort_column]} #{@params[:sort_order]}"
  end

  def valid_order?
    SORT_ORDERS.include?(@params[:sort_order]) && (SORT_COLUMNS.include? @params[:sort_column])
  end

  def page
    @params.permit(:page).fetch(:page, DEFAULT_PAGE)
  end

  def per_page
    numb_per_page = @params.permit(:per_page).fetch(:per_page, DEFAUT_PER_PAGE)
    return MAX_PER_PAGE if Integer(numb_per_page) > MAX_PER_PAGE

    numb_per_page
  end

  def fetch_query
    (query_invoice_fields + query_people_fields).compact.join(" AND ")
  end

  def query_invoice_fields
    query = []
    query << "invoices.id = '#{@params[:id]}'" if @params[:id]
    query << "settled_at >= '#{start_date}'" if @params[:start_date]
    query << "settled_at <= '#{end_date}'" if @params[:end_date]
    query << "final_settlement_amount_cents >= '#{minimum_amount}'" if @params[:minimum_amount]
    query << "final_settlement_amount_cents <= '#{maximum_amount}'" if @params[:maximum_amount]
    query
  end

  def query_people_fields
    query = []
    query << email_or_name_query if @params[:email_or_name]
    query << "people.country_website_id = '#{@params[:country_website_id]}'" if @params[:country_website_id]
    query
  end

  def email_or_name_query
    "(people.name like :email_or_name OR people.email like :email_or_name)"
  end

  def start_date
    @params[:start_date].to_date.beginning_of_day
  end

  def end_date
    @params[:end_date].to_date.end_of_day
  end

  def minimum_amount
    amount_to_cents(@params[:minimum_amount])
  end

  def maximum_amount
    amount_to_cents(@params[:maximum_amount])
  end

  def amount_to_cents(amount)
    Integer(Float(amount) * 100)
  end

  def fields_to_select
    "invoices.id, settled_at, final_settlement_amount_cents, currency,
     people.country_website_id as website_id, people.name"
  end
end
