# frozen_string_literal: true

module AutoRefunds
  class RefundBackfill
    REFUND_LEVEL = "invoice"
    REFUND_TYPE = "fixed_amount"

    def initialize(transaction_id, transaction_type)
      @transaction = transaction_type.constantize.find_by(id: transaction_id)
    end

    def process
      refund_transaction = AutoRefunds::RefundTransaction.new(refund_args)

      ActiveRecord::Base.transaction do
        refund_transaction.create_refund_record!
        refund_transaction.create_refund_items!
        refund_transaction.refund.process_settlement(transaction, request_amount_cents)
        refund_transaction.refund.update(total_amount_cents: request_amount_cents)
      end
    end

    private

    attr_reader :transaction

    def refund_args
      {
        refunded_by_id: system_admin.id,
        refund_level: REFUND_LEVEL,
        refund_reason_id: refund_reason.id,
        admin_note: transaction.refund_reason,
        invoice_id: transaction.invoice_id,
        refund_type: REFUND_TYPE,
        tax_inclusive: true,
        request_amount: request_amount
      }
    end

    def system_admin
      @system_admin ||= Person.find_by(email: ENV["SYSTEM_ADMIN"])
    end

    def refund_reason
      @refund_reason ||= RefundReason.find_by(reason: "Backfill")
    end

    def request_amount
      @request_amount ||=
        case transaction.class.name
        when BraintreeTransaction.name
          transaction.amount
        when PaymentsOSTransaction.name
          transaction.amount / 100.00
        else
          0
        end
    end

    def request_amount_cents
      request_amount * 100
    end
  end
end
