module AutoRefunds
  class Settlement
    attr_reader :invoice, :refund

    def initialize(refund, args)
      @refund = refund
      @invoice = Invoice.find_by(id: args[:invoice_id])
      @dispute = args[:dispute]
    end

    def settle!
      dispute.present? ? create_dispute_settlment! : create_refund_settlments!
    end

    private

    attr_reader :dispute

    def create_refund_settlments!
      invoice_settlements.each do |settlement|
        next unless settlement.can_refund?

        amount = settlement_amount(settlement)

        transaction = settlement.source_type.constantize.process_auto_refund!(refund, amount, settlement)

        set_refund_to_pending && next if settlement.source.is_a?(AdyenTransaction)

        refund_settlement = refund.process_settlement!(transaction, amount)

        process_outbound_invoice(settlement, refund_settlement) if settlement.settled_by_balance?
        break if refund.settled?
      end
    end

    def create_dispute_settlment!
      refund.process_settlement!(dispute, dispute.amount_cents)
    end

    def total_amount
      @total_amount ||= refund.total_amount_cents
    end

    def invoice_settlements
      @invoice_settlements ||= invoice.invoice_settlements
    end

    def settlement_amount(invoice_settlement)
      [invoice_settlement.max_refund_amount, total_amount - settled_amount].min
    end

    def settled_amount
      refund.settled_amount
    end

    def process_outbound_invoice(invoice_settlement, refund_settlement)
      AutoRefunds::OutboundCreditNote.new(refund, invoice_settlement, refund_settlement).process!
    end

    def set_refund_to_pending
      refund.update(status: "pending")
    end
  end
end
