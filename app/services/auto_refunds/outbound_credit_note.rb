# frozen_string_literal: true

module AutoRefunds
  class OutboundCreditNote
    def initialize(refund, invoice_settlement, refund_settlement)
      @refund = refund
      @outbound_invoice = invoice_settlement.outbound_invoice
      @refund_settlement = refund_settlement
    end

    def process!
      return if empty_outbound_invoice?

      create_credit_note_invoice
      debit_vat_posted
    end

    private

    attr_reader :refund,
                :outbound_invoice,
                :refund_settlement

    attr_accessor :outbound_refund

    def empty_outbound_invoice?
      outbound_invoice.nil?
    end

    def create_credit_note_invoice
      self.outbound_refund = OutboundRefund.create!(
        refund: refund,
        outbound_invoice: outbound_invoice,
        refund_settlement: refund_settlement,
        currency: refund.currency,
        user_invoice_prefix: refund.person.cr_outbound_invoice_prefix
      )
    end

    def vat_tax_adjustment
      @vat_tax_adjustment ||= outbound_invoice.vat_tax_adjustment
    end

    def tax_rate
      @tax_rate ||= Float(vat_tax_adjustment&.tax_rate || 0)
    end

    def vat_amount_cents
      return @vat_amount_cents if @vat_amount_cents.present?

      total_amount = refund_settlement.settlement_amount_cents
      base_amount = Integer((total_amount * 100 / (100.0 + tax_rate)).round)
      @vat_amount_cents = Integer((base_amount * tax_rate / 100.0).round)
      return @vat_amount_cents if outbound_invoice.vat_amount_remaining >= @vat_amount_cents

      # To Adjust the rounding issue:
      # If the remaining vat to debit is less than the calculated amount,
      # use the remaining vat_amount
      @vat_amount_cents = outbound_invoice.vat_amount_remaining
    end

    def vat_amount_cents_in_eur
      balance_rate = vat_tax_adjustment.foreign_exchange_rate
      return vat_amount_cents * balance_rate.exchange_rate if balance_rate.present?

      vat_amount_cents
    end

    def debit_vat_posted
      return if vat_tax_adjustment.nil?

      refund_adjustment = create_refund_vat_adjustment
      create_balance_transaction(refund_adjustment)
      assign_to_invoice(refund_adjustment)
    end

    def create_refund_vat_adjustment
      VatTaxAdjustment.create(
        {
          person: refund.person,
          amount: vat_amount_cents,
          tax_rate: tax_rate,
          related: refund,
          vat_amount_in_eur: vat_amount_cents_in_eur,
          foreign_exchange_rate: vat_tax_adjustment.foreign_exchange_rate
        }.merge(vat_tax_adjustment
            .attributes
            .slice(
              *%w[vat_registration_number tax_type trader_name place_of_supply customer_type]
            ))
      )
    end

    def create_balance_transaction(vat_adjustment)
      PersonTransaction.create!(
        person: refund.person,
        debit: Tunecore::Numbers.cents_to_decimal(vat_amount_cents),
        credit: 0,
        target: vat_adjustment,
        comment: "VAT Debit"
      )
    end

    def assign_to_invoice(refund_adjustment)
      outbound_refund.update(vat_tax_adjustment: refund_adjustment)
    end
  end
end
