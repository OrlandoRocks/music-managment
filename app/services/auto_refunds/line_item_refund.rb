module AutoRefunds
  class LineItemRefund
    def initialize(refund, purchase_id, request_amount, refund_type, tax_inclusive)
      @refund = refund
      @purchase_id = purchase_id
      @request_amount = request_amount
      @refund_type = refund_type
      @tax_inclusive = tax_inclusive
    end

    def create!
      refund.refund_items.create!(
        purchase: purchase,
        base_amount_cents: refund_amount.base_amount_cents.round,
        tax_amount_cents: refund_amount.tax_amount_cents.round
      )
    end

    private

    attr_reader :refund, :purchase_id, :request_amount, :refund_type

    def purchase
      @purchase ||= Purchase.find(purchase_id)
    end

    def tax_inclusive?
      @tax_inclusive
    end

    def refund_amount
      @refund_amount ||= Utils::RefundAmount
                         .new(request_amount, purchase, tax_inclusive?, refund_type)
    end
  end
end
