# frozen_string_literal: true

module AutoRefunds
  class DisputeRefund
    def initialize(dispute)
      @dispute = dispute
    end

    def process
      return unless features_on? && refundable?

      refund_service.process
      self.refund = refund_service.refund
      refund.error? ? handle_failure : update_dispute_refund
    end

    private

    attr_reader :dispute
    attr_accessor :refund

    delegate :refundable?, to: :dispute

    def invoice_id
      dispute.invoice.id
    end

    def dispute_amount
      dispute.amount_cents.fdiv(100)
    end

    def processed_by
      @processed_by ||= (dispute.processed_by || system_admin)
    end

    def system_admin
      Person.find_by(email: ENV.fetch("SYSTEM_ADMIN"))
    end

    def features_on?
      processed_by.new_refunds_feature_on? &&
        processed_by.chargebacks_feature_on?
    end

    def refund_service
      @refund_service ||=
        AutoRefundService.new(
          processed_by,
          invoice_id: invoice_id,
          refund_level: Refund::INVOICE,
          request_amount: dispute_amount,
          refund_type: Refund::FIXED_AMOUNT,
          refund_reason_id: refund_reason.id,
          tax_inclusive: true,
          refunded_by_id: processed_by.id,
          dispute: dispute
        )
    end

    def refund_reason
      RefundReason.find_by(reason: "Chargebacks")
    end

    def handle_failure
      Tunecore::Airbrake.notify(
        "Failed to create refund for dispute",
        dispute: dispute,
        refund: refund
      )
    end

    def update_dispute_refund
      dispute.update(refund: refund)
    end
  end
end
