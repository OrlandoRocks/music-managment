class TransactionExpulsionService
  attr_accessor :expellable_transactions

  BALANCE_VALIDITY_PRECISION = 6

  def initialize(transactions_block)
    @expellable_transactions = PersonTransaction.where(id: transactions_block).order(:created_at)
  end

  def expel!
    return unless expulsion_integrity.eql?(:valid)

    expellable_transactions.delete_all
  end

  private

  def expulsion_integrity
    return :transaction_belongs_to_multiple_users unless transactions_belong_to_same_user?
    return :net_effect_non_zero                   unless net_effect_zero?
    return :breaks_transaction_chain              unless pre_post_balances_match?
    return :non_contiguous_transactions           unless transactions_contiguous?

    :valid
  end

  def transactions_contiguous?
    expellable_transactions_created_at_range = Range.new(*expellable_transactions.pluck(:created_at).minmax)
    person_transaction_query_range_ids =
      PersonTransaction
      .where(person_id: person.id, created_at: expellable_transactions_created_at_range)
      .pluck(:id)

    (person_transaction_query_range_ids - expellable_transactions.pluck(:id)).none?
  end

  def transactions_belong_to_same_user?
    expellable_transactions.pluck(:person_id).minmax.reduce(&:eql?)
  end

  def net_effect_zero?
    (expellable_transactions.sum(:debit) - expellable_transactions.sum(:credit)).zero?
  end

  def pre_post_balances_match?
    pre_block_transaction_rolling_balance
      .round(BALANCE_VALIDITY_PRECISION)
      .eql?(post_block_transaction_previous_balance.round(BALANCE_VALIDITY_PRECISION))
  end

  def complete_person_transaction_block
    person.person_transactions.order(:created_at).to_a
  end

  def pre_block_transaction
    return @pre_block_transaction if @pre_block_transaction

    block_beginning_pointer = complete_person_transaction_block.index(expellable_transactions.min_by(&:created_at))
    @pre_block_transaction  = complete_person_transaction_block[block_beginning_pointer.pred]
  end

  def post_block_transaction
    return @post_block_transaction if @post_block_transaction

    block_ending_pointer    = complete_person_transaction_block.index(expellable_transactions.max_by(&:created_at))
    @post_block_transaction = complete_person_transaction_block[block_ending_pointer.next]
  end

  def pre_block_transaction_rolling_balance
    return 0 if pre_block_transaction.blank?

    pre_block_transaction.previous_balance + pre_block_transaction.credit - pre_block_transaction.debit
  end

  def post_block_transaction_previous_balance
    return person.person_balance.balance unless post_block_transaction

    post_block_transaction.previous_balance
  end

  def person
    @person ||= expellable_transactions.take.person
  end
end
