# frozen_string_literal: true

class VatAssesmentService
  include VatAssesmentAddressBuilder

  def initialize(person, vat_number, transient_address = {})
    @person = person
    @vat_number = vat_number
    @transient_address = transient_address.to_h
  end

  def acceptable?
    return true unless vat_already_taken?

    same_address?
  end

  private

  attr_reader :person, :vat_number, :transient_address

  def shared_vat_info
    @shared_vat_info ||= VatInformation
                         .where(vat_registration_number: vat_number)
                         .where.not(person_id: person.id)
  end

  def vat_already_taken?
    shared_vat_info.any?
  end

  def person_with_same_vat
    shared_vat_info.first.person
  end

  def same_address?
    # Do not accept same VAT number if users didn't update any address fields
    return false unless address_values_updated? address_hash_for_person_with_same_vat

    address_hash_for_person == address_hash_for_person_with_same_vat
  end

  def address_hash_for_person
    transient_address.present? ? format(transient_address) : complete_address_for(person)
  end

  def address_hash_for_person_with_same_vat
    @address_hash_for_person_with_same_vat ||= complete_address_for(person_with_same_vat)
  end
end
