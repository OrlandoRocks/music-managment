class AutoRefundService
  attr_reader :refund, :error

  def initialize(admin, **args)
    @admin = admin
    @args = args
  end

  def process
    return unless admin.new_refunds_feature_on?

    initialize_refund
    finalize_refund_transaction
    refund.reload
  rescue ActiveRecord::RecordInvalid, AutoRefunds::Utils::SettlementFailed => e
    self.error = e.message
    update_refund_status_to_error
  end

  def process_dispute_refund
    return unless admin.new_refunds_feature_on?

    create_dispute_refund_record
  end

  def success?
    return false if refund.blank?

    refund.success? || api_success?
  end

  private

  attr_writer :refund, :error
  attr_reader :args, :admin

  def refund_transaction
    @refund_transaction ||= AutoRefunds::RefundTransaction.new(args)
  end

  def initialize_refund
    self.refund = refund_transaction.create_refund_record!
  end

  def create_dispute_refund_record
    ActiveRecord::Base.transaction do
      initialize_refund
      refund_transaction.create_refund_items!
    end
  end

  def finalize_refund_transaction
    ActiveRecord::Base.transaction do
      refund_transaction.create_refund_items!
      create_settlements
    end
  end

  def update_refund_status_to_error
    return false if refund.blank?

    refund.reload
    refund.update!(status: Refund.statuses[:error], failure_reason: error)
  end

  def create_settlements
    AutoRefunds::Settlement.new(refund, args).settle!
  end

  # For Adyen refund, AutoRefundService return success? as true,
  # if refund API is successgully triggered
  # Refund authorization is notified as a webhooks and not in refund API response
  def api_success?
    refund.pending?
  end
end
