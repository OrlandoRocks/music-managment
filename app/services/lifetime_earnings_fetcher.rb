class LifetimeEarningsFetcher
  attr_reader :asset_id, :asset_type

  def self.fetch(asset_id, asset_type)
    new(asset_id, asset_type).fetch
  end

  def initialize(asset_id, asset_type)
    @asset_id = asset_id
    @asset_type = asset_type
  end

  def fetch
    raise_error unless ["Album", "Song"].include?(asset_type)

    sql_str =
      if asset_type == "Album"
        album_earnings_sql_str
      else
        song = Song.find(asset_id)
        song_and_album_earnings_sql_str(song.album)
      end

    Snowflake.fetch(sql_str)
  end

  private

  def album_earnings_sql_str
    "SELECT COALESCE(sum(client_amount), 0) as lifetime_earnings " \
    "FROM ROYALTIES " \
    "WHERE release_id = #{asset_id};"
  end

  def song_and_album_earnings_sql_str(album)
    "SELECT sum(client_amount) as lifetime_earnings " \
    "FROM ( " \
    "SELECT COALESCE(SUM(client_amount), 0) / #{album.songs.size} AS client_amount " \
    "FROM ROYALTIES " \
    "WHERE release_id = #{album.id} " \
    "AND track_id IS NULL " \
    "UNION ALL " \
    "SELECT COALESCE(SUM(client_amount), 0) AS client_amount " \
    "FROM royalties r2 " \
    "WHERE r2.release_id = #{album.id} " \
    "AND r2.track_id = #{asset_id} " \
    ") AS tbl"
  end

  def raise_error
    raise "You can only fetch lifetime earnings for an " \
      "Album or Song."
  end
end
