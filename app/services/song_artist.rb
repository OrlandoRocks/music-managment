class SongArtist
  include ArelTableMethods
  include ArelNamedMysqlFunctions
  include ArelSongContributorsJoins
  include ArelCommonJoins

  def self.for_song(song)
    new(song: song).for_song
  end

  def self.for_crt(song)
    new(song: song).for_crt
  end

  attr_reader :song, :creative

  def initialize(opts = {})
    @song = opts[:song]
    @creative = opts[:creative]
  end

  def for_song
    conditions = song_conditions

    conditions.blank? ? [] : base_query.where(conditions)
  end

  def self.for_creative(creative)
    new(creative: creative).apple_artist_id
  end

  def apple_artist_id
    creative = Creative.find(@creative.creative_id)
    creative.get_apple_artist_id
  end

  def for_crt
    base_query.where(crt_conditions)
  end

  def base_query
    Creative.select(
      select_statement
    ).joins(
      artist_join
    ).joins(
      blacklisted_artist_join
    ).joins(
      creative_song_roles_join
    ).joins(
      song_roles_join
    ).group(creative_t[:id])
  end

  def creative_song_roles_join
    creative_t.join(
      creative_song_role_t, Arel::Nodes::OuterJoin
    ).on(
      creative_t[:id].eq(creative_song_role_t[:creative_id]).and(creative_song_role_t[:song_id].eq(@song.id))
    ).join_sources
  end

  private

  def select_statement
    [
      creative_t[:id].as("creative_id"),
      creative_t[:role].as("credit"),
      creative_t[:creativeable_type].as("associated_to"),
      artist_t[:name].as("artist_name"),
      artist_t[:id].as("artist_id"),
      blacklisted_artist_t[:active].as("blacklisted"),
      group_concat(song_role_t[:role_type]).as("role_types"),
      group_concat(creative_song_role_t[:song_role_id]).as("role_ids"),
      creative_t[:person_id].as("person_id")
    ]
  end

  def song_conditions
    dj_release = @song&.album&.is_dj_release?

    return "" if dj_release && @song.id.blank?

    # #NULL value in creativable_id from rails 4.1 upgrade giving out wrong data, so checking if song exists and omitting the condition if its nil
    if @song.id
      query = creative_t[:creativeable_type].eq("Song")
                                            .and(creative_t[:creativeable_id].eq(@song.id))

      return query if dj_release

      query.or(
        creative_t[:creativeable_type].eq("Album")
                .and(creative_t[:creativeable_id].eq(@song.album_id))
      )
    else
      creative_t[:creativeable_type].eq("Album")
                                    .and(creative_t[:creativeable_id].eq(@song.album_id))
    end
  end

  def crt_conditions
    creative_t[:creativeable_type].eq("Song")
                                  .and(creative_t[:creativeable_id].eq(@song.id))
                                  .and(creative_t[:role].eq("featuring").or(
                                         creative_t[:role].eq("with").or(creative_t[:role].eq("primary_artist"))
                                               .or(creative_song_role_t[:song_role_id].eq(songwriter_role_id))
                                       ))
  end

  def songwriter_role_id
    SongRole.where(role_type: "songwriter").pluck(:id).first
  end
end
