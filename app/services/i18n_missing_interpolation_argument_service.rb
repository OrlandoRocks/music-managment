class I18nMissingInterpolationArgumentService
  def call(wildcard, provided_wildcards, phrase_copy)
    Airbrake.notify(
      "Missing interpolation argument for custom_t/I18n.t text",
      {
        wildcard: wildcard,
        provided_wildcards: provided_wildcards,
        phrase_copy: phrase_copy
      }
    )

    wildcard.to_s
  end
end
