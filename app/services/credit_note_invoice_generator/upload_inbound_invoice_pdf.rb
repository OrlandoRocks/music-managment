# frozen_string_literal: true

class CreditNoteInvoiceGenerator::UploadInboundInvoicePdf
  attr_reader :refund

  def initialize(refund)
    @refund = refund
  end

  def process
    process_invoice if refund.person.new_refunds_feature_on?
  end

  private

  def process_invoice
    CreditNoteInvoiceGenerator::InvoiceUpload.new(refund).upload_invoice
  end
end
