# frozen_string_literal: true

class CreditNoteInvoiceGenerator::UploadOutboundInvoicePdf
  attr_reader :outbound_refund

  def initialize(outbound_refund)
    @outbound_refund = outbound_refund
  end

  def process
    process_invoice if outbound_refund.person.new_refunds_feature_on?
  end

  private

  def process_invoice
    CreditNoteInvoiceGenerator::InvoiceUpload.new(outbound_refund).upload_invoice
  end
end
