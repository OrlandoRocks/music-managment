# frozen_string_literal: true

module CreditNoteInvoiceGenerator
  class Outbound
    attr_reader :outbound_refund

    def initialize(outbound_refund)
      @outbound_refund = outbound_refund
    end

    def render_to_pdf
      WickedPdf.new.pdf_from_string(render_to_string)
    end

    private

    def presenter
      @presenter ||= CreditNoteInvoices::OutboundPresenter.new(outbound_refund)
    end

    def render_to_string
      CreditNoteInvoicesController.render :show,
                                          assigns: { refund_presenter: presenter },
                                          layout: "layouts/pdf"
    end
  end
end
