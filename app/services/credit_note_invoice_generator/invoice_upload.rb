# frozen_string_literal: true

class CreditNoteInvoiceGenerator::InvoiceUpload
  BELIEVE = "Believe"
  TUNECORE = "Tunecore"
  INBOUND = "credit note inbound"
  OUTBOUND = "credit note outbound"

  attr_reader :invoice

  def initialize(invoice)
    @invoice = invoice
  end

  def upload_invoice
    bucket.object(key).put(body: invoice_pdf)
  end

  private

  def invoice_pdf
    if outbound_invoice?
      CreditNoteInvoiceGenerator::Outbound.new(invoice).render_to_pdf
    else
      CreditNoteInvoiceGenerator::Inbound.new(invoice).render_to_pdf
    end
  end

  def key
    @key ||= "#{Rails.env}/#{directory_name}/#{file_name}"
  end

  def bucket
    @bucket ||= S3_INVOICE_CLIENT.bucket(INVOICE_BUCKET)
  end

  def directory_name
    corporate_entity_name = invoice.invoice_static_corporate_entity.tunecore_us? ? TUNECORE : BELIEVE
    invoice_type = outbound_invoice? ? OUTBOUND : INBOUND
    child_directory = %(#{corporate_entity_name} #{invoice_type})

    %(#{invoice_date.strftime('%Y/%B/%d')}/#{child_directory})
  end

  def file_name
    date = invoice_date.strftime("%Y-%m-%d")
    invoice_number = invoice.credit_note_invoice_number

    %(#{invoice_number},#{date}.pdf)
  end

  def invoice_date
    invoice.created_at
  end

  def outbound_invoice?
    invoice.is_a? OutboundRefund
  end
end
