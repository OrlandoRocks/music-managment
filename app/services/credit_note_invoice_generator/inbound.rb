# frozen_string_literal: true

module CreditNoteInvoiceGenerator
  class Inbound
    attr_reader :refund

    def initialize(refund)
      @refund = refund
    end

    def render_to_pdf
      WickedPdf.new.pdf_from_string(render_to_string)
    end

    private

    def presenter
      @presenter ||= CreditNoteInvoices::InboundPresenter.new(refund)
    end

    def render_to_string
      CreditNoteInvoicesController.render :show,
                                          assigns: { refund_presenter: presenter },
                                          layout: "layouts/pdf"
    end
  end
end
