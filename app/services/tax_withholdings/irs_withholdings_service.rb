# frozen_string_literal: true

class TaxWithholdings::IRSWithholdingsService
  include Ruleable
  include ActiveModel::Validations

  IRS_PERCENTAGE = 0.24

  attr_reader :person_transaction

  validates :person_transaction, presence: { strict: true }
  validates_with TaxWithholdingServiceValidator

  define_rules do
    # Checks if the person has crossed the earnings threshold for current year
    rule :current_year_earnings_threshold, ->(config, person) do
      return :fail if person.accurate_current_tax_year_total_earnings >= config[:current_year_threshold]

      :ok
    end
  end

  def initialize(person_transaction)
    @person_transaction = person_transaction
    validate!
  end

  def call
    return :ff_flag_disabled    unless FeatureFlipper.show_feature?(:us_tax_withholding)
    return :wrong_year          unless transaction_in_current_year?
    return :transaction_debit   if transaction_debit?
    return :amount_irs_withheld if irs_withheld?
    return :user_exempt         if user_exempt?
    return :tax_form_submitted  if tax_form_submitted?
    return :under_tax_threshold if under_tax_threshold?

    withhold_taxes_for_irs!
  end

  private

  def person
    @person ||= person_transaction.person
  end

  def transaction_in_current_year?
    person_transaction.created_at.year == Date.current.year
  end

  def taxable_year
    person_transaction.created_at.year
  end

  def not_us_user?
    !person.from_united_states_and_territories?
  end

  # TODO: remove once feature flag progressive_withholding_exemption_enabled? is at 100%
  def user_exempt?
    return false if progressive_withholding_exemption_enabled?

    not_us_user? || person.exempt_from_withholding?
  end

  def tax_form_submitted?
    return true if person.tax_forms.current.active.w9.exists?

    TaxFormCheckService.check_api?(person.id) unless FeatureFlipper.show_feature?(:block_taxform_sandbox_payoneer_api)
    person.tax_forms.current.active.w9.exists?
  end

  # Checks that the user is under the IRS threshold defined in the rules above
  def under_tax_threshold?
    under_tax_threshold
  end

  def transaction_debit?
    person_transaction.debit?
  end

  def irs_withheld?
    person_transaction.irs_tax_withholding
  end

  def person_tansaction_year_range
    person_transaction.created_at.beginning_of_year..person_transaction.created_at.end_of_year
  end

  # Finds all PersonTrasactions that are yet to be tax
  # and with holds taxes for the IRS
  def withhold_taxes_for_irs!
    us_date_ranges.flat_map do |us_date_range|
      person.person_transactions
            .with_no_irs_tax_withholdings
            .positive_credit
            .for_all_us_taxable_targets
            .where(created_at: us_date_range)
            .where.not(created_at: special_exemption_ranges)
            .order(:created_at, :id).map { |target| create_irs_tax_withholding!(target) }
    end
  end

  # Creates a irs_tax_withholding record to signify the ammout we are withholding
  # on behalf of the user for the IRS
  def create_irs_tax_withholding!(person_transaction)
    amount_to_be_withheld = person_transaction.credit * IRS_PERCENTAGE

    ApplicationRecord.transaction do
      irs_withholding = IRSTaxWithholding.create!(
        withheld_from_person_transaction: person_transaction,
        withheld_amount: amount_to_be_withheld,
        foreign_exchange_rate_id: exchange_rate_id
      )
      PersonTransaction.create!(
        person: person,
        currency: person_transaction.currency,
        debit: amount_to_be_withheld,
        target: irs_withholding,
        comment: withholding_txn_comment(person_transaction)
      )
    end
  end

  def under_tax_threshold
    @under_tax_threshold ||=
      evaluate_rules(against: person, category: :us_tax_withholding).exclude?(:fail)
  end

  # Specifies EXCLUSIVE ranges of dates that a user should not have taxes applied to thier taxable earning
  # Based on reasons in the WithholdingExemptionReason model.
  def special_exemption_ranges
    TaxWithholdings::ExemptionRangeFinderService.call(person.id)
  end

  # Specifies INCLUSIVE ranges of dates that a user should have taxes applied to thier taxable earning
  # Based on Them collecting earning while in the US
  def us_date_ranges
    TaxWithholdings::TransactionRangeFinderService
      .new(person.id, taxable_year)
      .call
      .select { |dates| dates[:country] == "US" }
      .pluck(:range)
      .presence || [person_tansaction_year_range]
  end

  def progressive_withholding_exemption_enabled?
    FeatureFlipper.show_feature?(:progressive_withholding_exemption)
  end

  def withholding_txn_comment(person_transaction)
    return "IRS Backup Withholding for 2021" if person_transaction.target_type == "TaxableEarningsRollover"

    "IRS Backup Withholding for Posting #{person_transaction.created_at.strftime('%m/%d/%Y')}"
  end

  def exchange_rate_id
    return if person.currency == CurrencyCodeType::USD

    ForeignExchangeRate.latest_by_currency(
      source: person.currency,
      target: CurrencyCodeType::USD
    ).id
  end
end
