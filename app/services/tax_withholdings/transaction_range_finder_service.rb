# frozen_string_literal: true

# Produces an array of hashes that shows when a user was apart of a specific country and the
# range of dates  they where apart of said country for the current year
# Example output:
# => [ {:range=>Sat, 01 Jan 2022 00:00:00 +0000..2022-01-01 19:00:00 -0500, :country=>"US"},
#      {:range=>2022-01-01 19:00:00 -0500..2022-02-01 19:00:00 -0500, :country=>"FR"},
#      {:range=>2022-01-14 19:00:00 -0500..2022-02-13 19:00:00 -0500, :country=>"DE"},
#      {:range=>2022-02-13 19:00:00 -0500..2022-03-01 19:00:00 -0500, :country=>"FR"},
#      {:range=>2022-03-01 19:00:00 -0500..Sat, 31 Dec 2022 23:59:59 +0000, :country=>"US"}
#     ]
class TaxWithholdings::TransactionRangeFinderService
  attr_reader :person_id, :year

  def initialize(person_id, year)
    @person_id = person_id
    @year      = Integer(year)
  end

  # Formats each chunk and returns an array of date ranges by country,
  # note the special handling of the last chunk
  def call
    *head_chunks, tail_chunk = chunked_audits
    [
      *head_chunks.map { |chunk| formatter(chunk) },
      formatter(tail_chunk, :last)
    ]
  end

  private

  def person
    @person ||= Person.find(person_id)
  end

  # Grabs last years country audit so we know what country the
  # user will be coming into the current year with
  def last_years_last_country_audit
    @last_years_last_country_audit ||= [
      person.country_audits
            .where(CountryAudit.arel_table[:created_at].lt(Date.new(year).to_datetime.beginning_of_year))
            .eager_load(:country)&.last
    ].compact
  end

  # All country audits for the current year
  def country_audits
    return @country_audits unless @country_audits.nil?

    audits = [
      *last_years_last_country_audit,
      *person.country_audits
             .for_year(year)
             .eager_load(:country)
    ]

    @country_audits =
      audits.presence || [person.country_audits&.last].compact
  end

  # Chunks all audits into an array of arrays if the countries are US territories
  # or the countries share the same country ID, then it links them so all
  # The date ranges (should they be combinded) will cover an entire year

  def chunked_audits
    return @chunked_audits unless @chunked_audits.nil?

    chunks =
      country_audits.chunk_while do |current_audit, next_audit|
        (current_audit.country_id == next_audit.country_id) ||
          both_audits_are_us_and_territories?(current_audit, next_audit)
      end
    @chunked_audits = link_chunks(chunks.to_a)
  end

  # Links the chucks so that the start and end dates of the chunk line up with
  # the next countries chunk's start date
  def link_chunks(chunks, linked_chunks = [])
    return linked_chunks if chunks.empty?

    head, *rest = chunks
    linked = [*head, *rest.first.dup]
    link_chunks(rest, linked_chunks.push(linked))
  end

  # Checks if both audits are US territories
  def both_audits_are_us_and_territories?(current_audit, next_audit)
    pair_of_audits = [current_audit.country.iso_code, next_audit.country.iso_code]
    (Country::UNITED_STATES_AND_TERRITORIES & pair_of_audits) == pair_of_audits
  end

  # Formats/reduces the chunk of audits into ranges, by country,
  # Taking into consideration audits need to line up with teh beginning and teh end of the year
  def formatter(chunk, position = nil)
    {
      range: handle_range_consolidation(chunk, position),
      country: handle_country_name(chunk)
    }
  end

  # Returns chunk Iso_code or for US and territoris it just assigns them to US
  def handle_country_name(chunk)
    if Country::UNITED_STATES_AND_TERRITORIES.include?(chunk.first.country.iso_code)
      "US"
    else
      chunk.first.country.iso_code
    end
  end

  # Range consolidation and special handling by position for each chunk
  def handle_range_consolidation(chunk, position)
    min, max = chunk.map(&:created_at)
                    .map(&:utc)
                    .minmax
    if position == :last
      ([min, beginning_of_taxable_year].max..end_of_taxable_year)
    else
      ([min, beginning_of_taxable_year].max..max)
    end
  end

  def beginning_of_taxable_year
    @beginning_of_taxable_year ||=
      Date.new(year).to_datetime.beginning_of_year.utc
  end

  def end_of_taxable_year
    @end_of_taxable_year ||=
      Date.new(year).to_datetime.end_of_year.utc
  end
end
