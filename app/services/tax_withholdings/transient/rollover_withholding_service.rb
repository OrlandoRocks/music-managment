# frozen_string_literal: true

class TaxWithholdings::Transient::RolloverWithholdingService
  attr_accessor :person, :tax_year

  MIN_ROLLOVER_THRESHOLD = 10.0

  def initialize(person_id, reported_amount = 0, rollover_tax_year = 2021)
    @person          = Person.find_by(id: person_id)
    @tax_year        = rollover_tax_year
    @reported_amount = reported_amount
  end

  def rollover!
    if qualified_rollover?
      corrections = [
        { id: rollover_debit_transaction.id,  debit: rollover_amount, credit: 0 },
        { id: rollover_credit_transaction.id, debit: 0, credit: rollover_amount }
      ]

      rollover_status = TransactionCorrectionService.new(corrections).exec!
      unless rollover_status.eql?(:valid_correction)
        Rails.logger.error "ROLLOVER FAILED FOR PERSON ID: #{person&.id}; REASON: #{rollover_status}"
      end
    else
      Rails.logger.error <<-ERROR
      PERSON_ID: #{person&.id} DISQUALIFIED FOR ROLLOVER; REASON: #{rollover_disqualification_reason}"
      ERROR
    end
  end

  private

  def qualified_rollover?
    rollover_disqualification_reason.eql?(:valid_rollover)
  end

  def rollover_disqualification_reason
    return :person_not_found                if person.blank?
    return :no_taxable_income               unless taxable_income_transactions.exists?
    return :taxable_income_under_threshold  if rollover_amount < MIN_ROLLOVER_THRESHOLD
    return :pre_existing_rollovers_non_zero unless pre_existing_rollovers_zero?

    :valid_rollover
  end

  def pre_existing_rollovers_zero?
    taxable_income_rollover_transactions
      .pluck(:debit, :credit)
      .map(&:sum)
      .all?(&:zero?)
  end

  def taxable_income_rollover_transactions
    create_new_rollover_transactions if existing_rollover_transactions.blank?
    existing_rollover_transactions
  end

  def existing_rollover_transactions
    PersonTransaction.where(
      person_id: person.id,
      target_type: TaxableEarningsRollover
    )
  end

  def create_new_rollover_transactions
    ActiveRecord::Base.transaction do
      PersonTransaction.create!(
        person_id: person.id,
        debit: rollover_amount,
        target_type: TaxableEarningsRollover,
        target_id: person_tax_metadata&.id,
        comment: TaxBlocking::TaxableEarningsRolloverService::TAX_ROLLOVER_DEBIT_COMMENT
      )

      PersonTransaction.create!(
        person_id: person.id,
        credit: rollover_amount,
        target_type: TaxableEarningsRollover,
        target_id: person_tax_metadata&.id,
        comment: TaxBlocking::TaxableEarningsRolloverService::TAX_ROLLOVER_CREDIT_COMMENT
      )
    end
  end

  def person_tax_metadata
    @person_tax_metadata ||=
      person.person_earnings_tax_metadata.find_by(tax_year: tax_year)
  end

  def rollover_debit_transaction
    @rollover_debit_transaction ||=
      taxable_income_rollover_transactions
      .order(:created_at)
      .take
  end

  def rollover_credit_transaction
    @rollover_credit_transaction ||=
      taxable_income_rollover_transactions
      .order(created_at: :desc)
      .take
  end

  def rollover_amount
    @rollover_amount ||=
      taxable_income_transactions.sum(:credit) - @reported_amount
  end

  def taxable_income_transactions
    @taxable_income_transactions ||=
      begin
        year       = DateTime.new(Integer(tax_year))
        year_range = [year.beginning_of_year..year.end_of_year]

        PersonTransaction
          .for_all_us_taxable_targets
          .where(person_id: person.id)
          .where(credit: 0..)
          .where(created_at: year_range)
      end
  end
end
