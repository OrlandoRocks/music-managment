class TaxWithholdings::Transient::ManualRolloverWithholdingCorrectionService
  attr_accessor :person

  def initialize(person_id)
    @person = Person.find(person_id)
  end

  def update_rollovers!
    rolling_balance = debit_rollover_transaction.previous_balance - rollover_amount
    # rubocop:disable Rails/SkipsModelValidations
    debit_rollover_transaction.update_columns(credit: 0, debit: rollover_amount)
    credit_rollover_transaction.update_columns(credit: rollover_amount, debit: 0, previous_balance: rolling_balance)
    # rubocop:enable Rails/SkipsModelValidations
  end

  def expunge_withholding_transactions!
    withholding_transactions.delete_all
  end

  private

  def rollover_amount
    year = DateTime.new(Integer(2021))
    year_range = [year.beginning_of_year..year.end_of_year]
    taxable_income_transactions =
      PersonTransaction
      .for_all_us_taxable_targets
      .where(person_id: person.id)
      .where(credit: 0..)
      .where(created_at: year_range)

    taxable_income_transactions.sum(:credit)
  end

  def debit_rollover_transaction
    PersonTransaction.where(person_id: person.id, target_type: "TaxableEarningsRollover").order(:created_at).take
  end

  def credit_rollover_transaction
    PersonTransaction.where(person_id: person.id, target_type: "TaxableEarningsRollover").order("created_at desc").take
  end

  def withholding_transactions
    user_transactions =
      PersonTransaction
      .where(person_id: person.id)
      .where(created_at: DateTime.now.beginning_of_year.ago(1.day)..DateTime.now)

    user_transactions
      .where(target_type: "TransactionErrorAdjustment")
      .or(user_transactions.where(target_type: "BalanceAdjustment", comment: "IRS Tax Withholding Reversal"))
  end
end
