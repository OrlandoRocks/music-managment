# frozen_string_literal: true

# THIS SERVICE IS A TRANSIENT SERVICE CREATED FOR CF-619 THAT REVERSES
# ANY EXCESS WITHHOLDING THAT WAS CHARGED ON A USER'S BALANCE
class TaxWithholdings::Transient::TaxWithholdingReversalService
  attr_accessor :person_id

  def initialize(person_id)
    @person_id = person_id
  end

  def revert!
    person = Person.find(person_id)
    withholding_transactions = person.person_transactions.where(target_type: "IRSTaxWithholding")
    txn_amount               = withholding_transactions.sum("debit")
    new_person_transaction   =
      ActiveRecord::Base.transaction do
        withholding_transactions.delete_all

        error_adjustment = TransactionErrorAdjustment.create(
          person_id: person_id,
          adjusted_credit: 0.0,
          adjusted_debit: txn_amount,
          note: "Reverted IRSTaxWithholding - CF-619"
        )

        PersonTransaction.create(
          person_id: person_id,
          debit: 0,
          credit: 0,
          target: error_adjustment,
          comment: "IRS Tax Withholding",
          created_at: debit_correction.created_at - 1.second,
          previous_balance: person.person_balance.balance
        )
      end

    # rubocop:disable Rails/SkipsModelValidations
    new_person_transaction&.update_column(:debit, txn_amount)
    # rubocop:enable Rails/SkipsModelValidations
  end

  private

  def debit_correction
    BalanceAdjustment.find_by(
      posted_by_id: 331_961,
      created_at: DateTime.now.beginning_of_year..DateTime.now,
      person_id: person_id
    )
  end
end
