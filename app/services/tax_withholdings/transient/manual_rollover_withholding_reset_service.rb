class TaxWithholdings::Transient::ManualRolloverWithholdingResetService
  attr_accessor :person

  def initialize(person_id)
    @person = Person.find(person_id)
  end

  def reset!
    rolling_balance = initial_rolling_balance
    ActiveRecord::Base.transaction do
      person_transaction_correctable_block.map do |current_correctable_transaction|
        unless current_correctable_transaction.previous_balance == rolling_balance
          # rubocop:disable Rails/SkipsModelValidations
          current_correctable_transaction.update_column(:previous_balance, rolling_balance)
          # rubocop:enable Rails/SkipsModelValidations
        end

        rolling_balance =
          (rolling_balance + current_correctable_transaction.credit) - current_correctable_transaction.debit
      end

      unless rolling_balance == post_rolling_balance
        Rails.logger.error("Couldn't fix previous balances for person #{person.id}")
        raise ActiveRecord::Rollback
      end
    end

    if post_previous_balance_fix_correction_for_rollovers.present?
      # Reset Rollovers
      TransactionCorrectionService
        .new(post_previous_balance_fix_correction_for_rollovers)
        .exec!
    end

    return if post_previous_balance_fix_correction_for_withholdings.blank?

    # Reset Withholdings
    TransactionCorrectionService
      .new(post_previous_balance_fix_correction_for_withholdings)
      .exec!
  end

  private

  def post_previous_balance_fix_correction_for_rollovers
    PersonTransaction.where(person_id: person.id, target_type: "TaxableEarningsRollover").map do |transaction|
      { id: transaction.id, debit: 0, credit: 0 }
    end
  end

  def post_previous_balance_fix_correction_for_withholdings
    person_transactions      = PersonTransaction.where(person_id: person.id)
    correctable_withholdings =
      person_transactions
      .where(target_type: "TransactionErrorAdjustment")
      .or(person_transactions.where(target_type: "BalanceAdjustment", comment: "IRS Tax Withholding Reversal"))

    correctable_withholdings.map do |transaction|
      { id: transaction.id, debit: 0, credit: 0 }
    end
  end

  def person_transaction_correctable_block
    rollover_transactions    = person_transaction_complete_block.where(target_type: "TaxableEarningsRollover")
    withholding_transactions =
      person_transaction_complete_block
      .where(target_type: "TransactionErrorAdjustment")
      .or(
        person_transaction_complete_block.where(
          target_type: "BalanceAdjustment",
          comment: "IRS Tax Withholding Reversal"
        )
      )

    person_transaction_complete_block.where(
      created_at: Range.new(
        *[
          *rollover_transactions,
          *withholding_transactions
        ].sort_by(&:created_at).pluck(:created_at).minmax
      )
    ).to_a
  end

  def pre_correctable_block_transaction
    person_transaction_complete_block_array[
      person_transaction_complete_block_array.index(person_transaction_correctable_block.first).pred
    ]
  end

  def post_correctable_block_transaction
    person_transaction_complete_block_array[
      person_transaction_complete_block_array.index(person_transaction_correctable_block.last).next
    ]
  end

  def initial_rolling_balance
    (pre_correctable_block_transaction.previous_balance + pre_correctable_block_transaction.credit) -
      pre_correctable_block_transaction.debit
  end

  def post_rolling_balance
    post_correctable_block_transaction&.previous_balance || person.person_balance.balance
  end

  def person_transaction_complete_block_array
    @person_transaction_complete_block_array ||= person_transaction_complete_block.to_a
  end

  def person_transaction_complete_block
    @person_transaction_complete_block ||= PersonTransaction.where(person_id: person.id).order(:created_at)
  end
end
