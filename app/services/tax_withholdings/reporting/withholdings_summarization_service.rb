class TaxWithholdings::Reporting::WithholdingsSummarizationService
  def summarize!
    return :no_unreported_withholdings if unreported_irs_withholdings.blank?

    # rubocop:disable Rails/SkipsModelValidations
    ActiveRecord::Base.transaction do
      current_report_irs_transaction = IRSTransaction.create!
      unreported_irs_withholdings
        .update_all(irs_transaction_id: current_report_irs_transaction.id, updated_at: DateTime.current)
      current_report_irs_transaction.advance_status!

      current_report_irs_transaction
    end
    # rubocop:enable Rails/SkipsModelValidations
  end

  private

  def unreported_irs_withholdings
    @unreported_irs_withholdings ||= IRSTaxWithholding.unreported
  end
end
