class TaxWithholdings::Reporting::IRSPayablesReportCsvService
  REPORT_DATA_HEADERS = ["person_id", "person_email", "total_tax_withholding_amount", "currency"].freeze

  def generate_csv!(irs_transaction, report_filename)
    raw_report_data  = report_data(irs_transaction)
    report_stats     = generate_report_stats(raw_report_data, irs_transaction)
    target_file_path = File.join("tmp", report_filename)

    File.open(target_file_path, "w+") do |report_file|
      report_file.print(write_report_data_to_csv(raw_report_data))
    end

    [target_file_path, report_stats]
  end

  private

  def write_report_data_to_csv(raw_report_data)
    CSV.generate do |csv|
      csv << REPORT_DATA_HEADERS
      raw_report_data.each do |person_id, withholdings|
        csv << [
          person_id,
          withholdings.dig(0, 0),
          withholdings.pluck(1).compact.sum,
          withholdings.dig(0, 2)
        ]
      end
    end
  end

  def irs_tax_withholdings(reporting_irs_transaction)
    IRSTaxWithholding
      .for_irs_transaction(reporting_irs_transaction.id)
      .with_people_and_transactions
  end

  def report_data(reporting_irs_transaction)
    irs_tax_withholdings(reporting_irs_transaction)
      .pluck(
        "people.id",
        "people.email",
        "irs_tax_withholdings.withheld_amount",
        "person_transactions.currency",
        "irs_tax_withholdings.created_at"
      )
      .group_by(&:shift)
  end

  def generate_report_stats(raw_report_data, irs_transaction)
    withholding_period_init = [
      DateTime.current.beginning_of_year,
      IRSTransaction.where.not(id: irs_transaction.id).last&.created_at
    ].compact.max.strftime("%m/%d/%Y")

    currency_wise_withheld_amount =
      raw_report_data
      .values
      .flatten(1)
      .group_by { |person_values| person_values[2] }
      .transform_values { |person_currency_values| person_currency_values.pluck(1).sum }

    withholding_period_end  = irs_transaction.created_at.strftime("%m/%d/%Y")
    withheld_accounts_count = raw_report_data.keys.count

    {
      irs_transaction_id: irs_transaction.id,
      currency_wise_withheld_amount: currency_wise_withheld_amount,
      total_withheld_accounts: withheld_accounts_count,
      withholding_init_date: withholding_period_init,
      withholding_end_date: withholding_period_end,
    }
  end
end
