class TaxWithholdings::ExemptionRangeFinderService
  attr_accessor :person_id

  def self.call(person_id)
    new(person_id).collect_ranges
  end

  def initialize(person_id)
    @person_id = person_id
  end

  def collect_ranges
    return [] unless progressive_withholding_exemption_enabled?

    Person.find(person_id)
          .withholding_exemptions
          .active
          .pluck(:started_at, :ended_at)
          .map { |started_date, ended_date| Range.new(started_date, ended_date) }
  end

  private

  def progressive_withholding_exemption_enabled?
    FeatureFlipper.show_feature?(:progressive_withholding_exemption)
  end
end
