class AddPaidSalepointsService
  attr_reader :person, :album, :salepoints, :apple_music

  def self.call(person, album, salepoints, apple_music = false)
    new(person, album, salepoints, apple_music).call
  end

  def initialize(person, album, salepoints, apple_music)
    @person = person
    @album = album
    @salepoints = salepoints
    @apple_music = apple_music
  end

  def call
    SalepointService.call(person, album, formatted_salepoint_params, apple_music)

    mark_salepoints_as_paid

    return unless PetriBundle.exists?(album_id: album.id)

    album.salepoints_to_distribute(salepoints).each do |salepoint|
      Delivery::Salepoint.deliver(salepoint.id)
    end
  end

  private

  def mark_salepoints_as_paid
    salepoints.each(&:paid_post_proc)
  end

  def formatted_salepoint_params
    salepoints.each_with_object({}) do |salepoint, hash|
      hash[salepoint.store_id] = {
        salepoint: {
          id: salepoint.id,
          store_id: salepoint.store_id,
          variable_price_id: nil
        }
      }
    end
  end
end
