class SuccessfulPurchaseService
  include ReferrerService

  class SuccessfulPurchaseServiceProcessLoopError < StandardError; end

  def self.process(invoice)
    new(invoice).tap(&:process)
  end

  attr_reader :invoice

  def initialize(invoice)
    @invoice = invoice
  end

  def process
    invoice.purchases.each do |purchase|
      begin
        if respond_to?("#{purchase.related_type.underscore}_purchase")
          send("#{purchase.related_type.underscore}_purchase", purchase)
        end
      rescue => e
        Rails.logger.error "Failure in SuccessfulPurchaseService#process: #{e.message}"
        Airbrake.notify(
          SuccessfulPurchaseServiceProcessLoopError,
          {
            invoice: invoice,
            message: e.message,
            purchase: purchase.id,
          }
        )
        next
      end
    end
    after_purchase
  end

  def publishing_composer_purchase(purchase)
    composer_purchase(purchase)
  end

  def composer_purchase(purchase)
    PersonNotifier.publishing_welcome(invoice.person).deliver
    purchase.related.process_paid
  end

  def album_purchase(purchase)
    Delivery::AlbumWorker.perform_async(purchase.related.id)
  end

  def credit_usage_purchase(purchase)
    Delivery::AlbumWorker.perform_async(purchase.related.related.id)
  end

  def youtube_monetization_purchase(purchase)
    purchase.related.after_successful_purchase
  end

  private

  def after_purchase
    invoice.send_refer_friend_purchase_notification(is_first_time_dist_customer?) if invoice.person.referred_by_friend?
    record_referrer_event(invoice, invoice.person)
    PersonNotifier.payment_thank_you(invoice.person, invoice).deliver
    invoice.update_person_analytics
  rescue => e
    create_invoice_log
    Airbrake.notify("SuccessfulPurchaseService error after_purchase: #{invoice.id}", e)
  end

  def is_first_time_dist_customer?
    @is_first_time_dist_customer ||= invoice.check_and_set_first_time_distribution_or_credit
  end

  def create_invoice_log
    person = invoice.person
    message = "Comment: SuccessfulPurchaseService for an Invoice failed, email: #{person&.email}"
    options = {
      person: person,
      invoice: invoice,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
  end
end
