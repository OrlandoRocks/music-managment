# frozen_string_literal: true

class CashAdvanceRegistrationService
  attr_reader :current_user

  def initialize(current_user)
    @current_user = current_user
  end

  def token
    case advance_display
    when :advance
      token_success_json || token_failure_json
    else
      token_error_json
    end
  end

  def advance_display
    return :advance       if override_ineligibility?
    return :none          unless (user_eligible? && tax_requirements_met_for_advance?)
    return :under_review  if user_suspicious?

    :advance
  end

  def user_needed_override?
    (!user_eligible? || !tax_requirements_met_for_advance? || user_suspicious?) && override_ineligibility?
  end

  private

  def user_eligible?
    @eligibility ||= FeatureFlipper.show_feature?(:lyric_advance, current_user) && eligible_record?
  end

  def user_suspicious?
    current_user.suspicious?
  end

  def override_ineligibility?
    current_user.override_ineligibility?
  end

  def eligible_record?
    latest_eligibility_record.nil? ? false : latest_eligibility_record.eligible
  end

  def latest_eligibility_record
    DirectAdvance::EligibilityRecord.where(person_id: current_user.id).last
  end

  def tax_requirements_met_for_advance?
    return true unless current_user.from_united_states_and_territories?
    return true if Payoneer::FeatureService.payoneer_exclude_tax_check?(current_user)

    any_active_tax_forms?
  end

  def any_active_tax_forms?
    current_user.tax_forms.active.exists?
  end

  def token_error_message
    'Your account is currently under review and cannot receive an advance.
    If you feel this is an error please contact support here: https://support.tunecore.com/hc/en-us/requests/new/'
  end

  def token_failure_message
    'We were unable to process your advance.
    Please contact support here: https://support.tunecore.com/hc/en-us/requests/new/'
  end

  def token_error_json
    {
      json: token_error_message,
      status: 422
    }
  end

  def token_failure_json
    {
      json: token_failure_message,
      status: 500
    }
  end

  def token_success_json
    token = CashAdvances::Registration.register(current_user)

    return unless token

    {
      json: { token: token },
      status: 200
    }
  end
end
