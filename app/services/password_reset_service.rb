class PasswordResetService
  include Rails.application.routes.url_helpers

  def self.reset(email, initiator = :user, url: nil)
    new(email).reset(initiator, url: url)
  end

  attr_reader :person, :email_delivery_service

  def initialize(email, email_delivery_service = :sendgrid)
    @person = Person.find_by(email: email)
    @email_delivery_service = email_delivery_service
    person.unlock_account if person.try(:account_locked?)
  end

  def reset(initiator = :user, url: nil)
    @initiator = initiator

    return false unless valid?

    url ||= fallback_url

    if email_delivery_service == :hubspot
      TransactionalEmail::ForgotPassword.deliver(person, url: url)
    else
      PasswordResetMailer.reset(person, { url: url, initiator: initiator }).deliver
    end
    true
  rescue
    false
  end

  def fallback_url
    if @initiator == :social
      ENV["TC_SOCIAL_REDIRECT_URL"] + "?key=" + invite_code.to_s + "&person_id=" + person.id.to_s
    else
      reset_password_url(
        person_id: person.id,
        key: invite_code,
        host: url_hash[person.country_domain]
      )
    end
  end

  private

  def url_hash
    return WEB_ONLY_URLS if Rails.env.production?

    COUNTRY_URLS
  end

  def invite_code
    @invite_code ||= person.generate_invite_code
  end

  def valid?
    person && !person.account_locked? && invite_code.present?
  end
end
