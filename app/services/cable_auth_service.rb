# frozen_string_literal: true

class CableAuthService
  def initialize(person)
    @person = person
  end

  def self.refresh_token(person)
    new(person).refresh_token
  end

  def self.remove_token(person)
    new(person).remove_token
  end

  def self.validate_token(person, token)
    new(person).validate_token(token)
  end

  def refresh_token
    cable_auth_token = CableAuthToken.find_or_initialize_by(person_id: person.id)
    cable_auth_token.assign_attributes(
      token: generate_token,
      expires_at: get_expiry_time
    )
    cable_auth_token.save
  end

  def remove_token
    CableAuthToken.where(person: person).destroy_all
  end

  def validate_token(token)
    ActiveSupport::SecurityUtils.secure_compare(person.cable_auth_token&.token, token)
  end

  private_class_method :new

  private

  attr_accessor :person

  def generate_token
    SecureRandom.uuid
  end

  def get_expiry_time
    DateTime.now.in(CableAuthToken::CABLE_TOKEN_VALIDITY)
  end
end
