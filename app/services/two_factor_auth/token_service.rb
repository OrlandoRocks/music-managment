require "jwt"

class TwoFactorAuth::TokenService
  def self.issue(payload)
    JWT.encode(payload.merge(expiration_payload), auth_secret, algorithm)
  end

  def self.decode(token)
    JWT.decode(token, auth_secret, { algorithm: algorithm })
  rescue JWT::ExpiredSignature, JWT::DecodeError
    [{}]
  end

  def self.expiration_payload
    { exp: (Time.now + TwoFactorAuth::AUTHENTICATION_WINDOW).to_i }
  end

  def self.auth_secret
    ENV["JWT_ENCRYPTION_KEY"]
  end

  def self.algorithm
    ENV["JWT_ALGORITHM"]
  end
end
