class TwoFactorAuth::AuthyApiClient
  class AuthCodeSubmission
    attr_accessor :error_message

    def initialize(successful, error_message = nil)
      @successful    = successful
      @error_message = error_message
    end

    def successful?
      @successful
    end
  end

  ERROR_CODES = {
    "60022" => :invalid_verification_code,
    "60023" => :no_pending_verifications
  }

  attr_reader :person, :tfa, :phone_number, :country_code

  def initialize(person, two_factor_auth = nil, params = {})
    @person       = person
    @tfa          = two_factor_auth || person.two_factor_auth
    @phone_number = params[:phone_number]
    @country_code = params[:country_code]
    @e164         = params[:e164]
    @fraud_prevention = TwoFactorAuth::FraudPrevention.new(@person.id)
    @client = Twilio::REST::Client.new(ENV["TWILIO_SID"], ENV["TWILIO_TOKEN"])
  end

  def register_new_user
    register_authy_user
  end

  def get_e164
    validate_phone_number
  end

  def delete_user
    delete_authy_user
  end

  def request_authorization(alternate_method = false)
    request_method = alternate_method ? tfa.alternate_method : tfa.notification_method
    @fraud_prevention.register_request(request_method)

    send("request_via_#{request_method}")
  end

  def submit_auth_code(auth_code)
    response = Authy::API.verify(id: tfa.authy_id, token: auth_code).tap { |res| log(res) }

    if response.ok?
      AuthCodeSubmission.new(true)
    else
      AuthCodeSubmission.new(false, ERROR_CODES[response["error_code"]])
    end
  end

  private

  def request_via_app
    Authy::OneTouch.send_approval_request(
      id: tfa.authy_id,
      message: "Login to TuneCore", # TODO: need translation
      details: {
        I18n.t(:email_address) => person.email,
      }
    ).tap { |res| log(res) }
  end

  def request_via_sms
    Authy::API.request_sms(id: tfa.authy_id, force: true).tap { |res| log(res) }
  end

  def request_via_call
    return if @fraud_prevention.tfa_call_banned?

    Authy::API.request_phone_call(id: tfa.authy_id, force: true).tap { |res| log(res) }
  end

  def validate_phone_number
    begin
      response = @client.lookups.v1.phone_numbers("#{country_code}#{phone_number}").fetch
      response.phone_number
    rescue Twilio::REST::RestError => e
      Rails.logger.error "Error in validating phone number - Twilio error code #{e.code}"
      false
    end
  end

  def register_authy_user
    Authy::API.register_user(
      email: person.email,
      country_code: country_code,
      cellphone: phone_number
    ).tap { |res| log(res) }
  end

  def delete_authy_user
    Authy::API.delete_user(
      id: tfa.authy_id
    ).tap { |res| log(res) }
  end

  def log(response)
    Rails.logger.info "#{Time.now} || AUTHY API REQUEST || #{caller_locations.first.label} || #{response.to_json}"
  end
end
