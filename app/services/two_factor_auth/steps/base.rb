class TwoFactorAuth::Steps::Base
  StepResponse = Struct.new(:successful?, :error_msg)

  def self.check(_person, _params)
    new.success
  end

  def success
    StepResponse.new(true, nil)
  end

  def failure(error_msg)
    StepResponse.new(false, error_msg)
  end
end
