class TwoFactorAuth::Steps::Preference < TwoFactorAuth::Steps::Base
  include TwoFactorAuthable

  def self.check(person, params)
    new.check(person, params)
  end

  def check(person, params)
    return failure(error_msg) unless all_params_present?(params)

    set_preferences(person, params) ? success : failure(error_msg)
  end

  private

  def all_params_present?(params)
    [params[:country_code], params[:phone_number], params[:notification_method]].compact.length == 3
  end

  def set_preferences(person, params)
    get_e164(
      person,
      params
    ) && make_update(person, params) && send_to_authy(person, params) && send_code(person, params)
  end

  def make_update(person, params)
    unless person.two_factor_auth
      person.create_two_factor_auth
      create_authentication_event(person)
    end

    two_factor_auth = person.two_factor_auth

    previous_country_code = two_factor_auth.country_code

    two_factor_auth.update(
      {
        notification_method: params[:notification_method],
        country_code: params[:country_code],
        phone_number: params[:phone_number],
        e164: @e164,
        previous_country_code: previous_country_code
      }
    )
  end

  # Get e164 formatted phone number required by Twilio
  def get_e164(person, params)
    if exceeds_enrollment_page_attempts?(person, params)
      @error_msg = toll_fraud_daily_limit_error
      return false
    end

    api_client = TwoFactorAuth::ApiClient.new(person, nil, params)
    formatted_number = api_client.get_e164

    return false if formatted_number.blank?

    @e164 = formatted_number
  end

  def send_code(person, params)
    if FeatureFlipper.show_feature?(:verify_2fa, person)
      TwoFactorAuth::ApiClient.new(person, nil, params).request_authorization
    else
      TwoFactorAuth::AuthyApiClient.new(person, nil, params).request_authorization
    end
    create_authentication_event(person)
  end

  def send_to_authy(person, params)
    api_client = TwoFactorAuth::AuthyApiClient.new(person, nil, params)
    tfa        = api_client.tfa
    response   = api_client.register_new_user

    if response["id"]
      tfa.update_attribute("authy_id", response["id"])
    else
      false
    end
  end

  # Check if enrollment page has exceeded max daily attempts
  def exceeds_enrollment_page_attempts?(person, params)
    on_the_page?(params, :enrollments) && exceeds_max_daily_attempts?(person, :signup)
  end

  # Create TwoFactorAuth enrollment event
  def create_authentication_event(person)
    return unless person.two_factor_auth

    event_params = {
      type: AUTHENTICATION,
      page: ENROLLMENTS,
      action: AUTHENTICATION,
      successful: true,
      two_factor_auth_id: person.two_factor_auth.id
    }
    TwoFactorAuthEvent.create(event_params)
  end

  def error_msg
    @error_msg ||= I18n.t(:could_not_change_preference)
    @error_msg
  end
end
