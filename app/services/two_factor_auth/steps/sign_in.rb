class TwoFactorAuth::Steps::SignIn < TwoFactorAuth::Steps::Base
  def self.check(person, params)
    new.check(person, params)
  end

  def check(person, params)
    login_verified?(person, params) ? success : failure(error_msg)
  end

  private

  def login_verified?(person, params)
    Person.authenticate(params) == person
  end

  def error_msg
    I18n.t(:wrong_credentials)
  end
end
