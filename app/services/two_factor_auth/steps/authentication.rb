class TwoFactorAuth::Steps::Authentication < TwoFactorAuth::Steps::Base
  def self.check(person, params)
    new(person, params).check
  end

  def initialize(person, params)
    @person = person
    @params = params
  end

  def check
    auth_submission.successful? ? record_success : record_failure(auth_submission.error_message)
  end

  private

  attr_reader :person, :params

  def auth_submission
    @auth_submission ||=
      if FeatureFlipper.show_feature?(:verify_2fa, person)
        TwoFactorAuth::ApiClient.new(person).submit_auth_code(params[:auth_code])
      else
        TwoFactorAuth::AuthyApiClient.new(person).submit_auth_code(params[:auth_code])
      end
  end

  def record_success
    person.two_factor_auth.auth_success!
    success
  end

  # An invalid code error is NOT returned by Verify
  def record_failure(error_message)
    person.two_factor_auth.auth_failure!
    failure(error_message)
  end
end
