class TwoFactorAuth::ApiClient
  class AuthCodeSubmission
    attr_accessor :error_message

    def initialize(successful, error_message = nil)
      @successful    = successful
      @error_message = error_message
    end

    def successful?
      @successful
    end
  end

  STATUSES = {
    "pending" => :invalid_verification_code,
    "cancelled" => :no_pending_verifications
  }

  ERROR_CODES = {
    "60200" => :invalid_verification_code,
    "60023" => :no_pending_verifications
  }

  attr_reader :person, :tfa, :log, :phone_number, :country_code, :client

  def initialize(person, two_factor_auth = nil, params = {})
    @person       = person
    @tfa          = two_factor_auth || person.two_factor_auth
    @phone_number = params.fetch(:phone_number, @tfa&.phone_number)
    @country_code = params.fetch(:country_code, @tfa&.country_code)
    @e164         = params.fetch(:e164, @tfa&.e164)
    @fraud_prevention = TwoFactorAuth::FraudPrevention.new(@person.id)
    @client = Twilio::REST::Client.new(ENV["TWILIO_SID"], ENV["TWILIO_TOKEN"])
  end

  def get_e164
    validate_phone_number
  end

  def request_authorization(alternate_method = false)
    request_method = alternate_method ? tfa.alternate_method : tfa.notification_method
    @fraud_prevention.register_request(request_method)

    send("request_via_#{request_method}")
  end

  def submit_auth_code(auth_code)
    begin
      response =
        @client.verify.v2.services(ENV["VERIFY_SID"]).verification_checks.create(
          to: @e164,
          code: auth_code
        ).tap { |res|
          log(res)
        }
    rescue Twilio::REST::RestError => e # Twilio API errors if code is left blank
      Rails.logger.error "Error in validating auth code - Twilio error code #{e.code}"
      return AuthCodeSubmission.new(false, ERROR_CODES[e.code])
    end

    if response.status == "approved"
      AuthCodeSubmission.new(true)
    else
      AuthCodeSubmission.new(false, STATUSES[response.status])
    end
  end

  private

  def request_via_app
    Authy::OneTouch.send_approval_request(
      id: tfa.authy_id,
      message: "Login to TuneCore", # TODO: need translation
      details: {
        I18n.t(:email_address) => person.email,
      }
    ).tap { |res| log(res) }
  end

  def request_via_sms
    begin
      @client.verify.v2.services(ENV["VERIFY_SID"]).verifications.create(to: @e164, channel: "sms").tap { |res|
        log(res)
      }
    rescue Twilio::REST::RestError => e # Twilio API can hit rate limit
      Rails.logger.error "Error requesting auth code - Twilio error code #{e.code}"
    end
  end

  def request_via_call
    return if @fraud_prevention.tfa_call_banned?

    begin
      @client.verify.v2.services(ENV["VERIFY_SID"]).verifications.create(to: @e164, channel: "call").tap { |res|
        log(res)
      }
    rescue Twilio::REST::RestError => e # Twilio API can hit rate limit
      Rails.logger.error "Error requesting auth code - Twilio error code #{e.code}"
    end
  end

  def validate_phone_number
    begin
      response = @client.lookups.v1.phone_numbers("#{country_code}#{phone_number}").fetch
      response.phone_number
    rescue Twilio::REST::RestError => e
      Rails.logger.error "Error in validating phone number - Twilio error code #{e.code}"
      false
    end
  end

  def log(response)
    res_data = "channel: #{response.channel}, status: #{response.status}, valid: #{response.valid}"
    Rails.logger.info "#{Time.now} || VERIFY API REQUEST || #{caller_locations.first.label} || #{res_data}"
  end
end
