class TwoFactorAuth::FraudPrevention
  TIME_RANGE_FOR_REPEATED_TRIES = 60.seconds.to_i
  TIME_RANGE_FOR_CALL_BAN = 5.minutes.to_i
  MAX_ALLOWED_TRIES = 3

  def self.request_banned?(user_id, request_method)
    return false if request_method != TwoFactorAuth::CALL

    new(user_id).tfa_call_banned?
  end

  def initialize(user_id)
    @user_id = user_id
  end

  def register_request(request_method)
    return if request_method != TwoFactorAuth::CALL
    return if tfa_call_banned?

    record_request
    true
  end

  def tfa_call_banned?
    $redis.get(call_ban_key).present?
  end

  private

  def request_key
    "tfa_call_request:#{@user_id}"
  end

  def call_ban_key
    "tfa_call_banned:#{@user_id}"
  end

  def get_requested_count(key)
    $redis.get(key).to_i
  end

  def record_request
    requested_times = get_requested_count(request_key)
    count = requested_times + 1

    if requested_times.zero?
      $redis.setex(request_key, TwoFactorAuth::FraudPrevention::TIME_RANGE_FOR_REPEATED_TRIES, count)
    else
      $redis.incr(request_key)
    end

    impose_call_ban if count >= TwoFactorAuth::FraudPrevention::MAX_ALLOWED_TRIES
  end

  def impose_call_ban
    $redis.setex(call_ban_key, TwoFactorAuth::FraudPrevention::TIME_RANGE_FOR_CALL_BAN, true)
  end
end
