class TwoFactorAuth::PhoneNumberImportService
  def self.import(tfa_id)
    tfa = TwoFactorAuth.find(tfa_id)
    return unless tfa && tfa.authy_id != nil

    http_client = Faraday.new(url: "https://api.authy.com", headers: { "X-AUTHY-API-KEY" => ENV["AUTHY_API_KEY"] })
    response = http_client.get("/protected/json/users/#{tfa.authy_id}/phone_number")
    body = JSON.parse(response.body)

    if body["e164"].present?
      tfa.country_code = body["country_code"]
      tfa.phone_number = body["phone_number"]
      tfa.e164 = body["e164"]
      tfa.save!
    else
      Rails.logger.info "Could not retrieve phone number for two_factor_auth id #{tfa_id}: #{response.body}"
    end
  end
end
