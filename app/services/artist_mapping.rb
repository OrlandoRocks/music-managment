class ArtistMapping
  def self.create_or_update_external_service_ids(options)
    new(options).tap(&:create_or_update_external_service_ids)
  end

  def initialize(options)
    @person_id     = options[:person_id]
    @artist_id     = options[:artist_id]
    @identifier    = options[:identifier]
    @service_name  = options[:service_name]
    @artwork       = options[:artwork]
  end

  def create_or_update_external_service_ids
    artwork = ExternalServiceIdArtwork.create(asset: @artwork) if @artwork
    Creative.person_artists(@person_id, @artist_id).each do |creative|
      es = ExternalServiceId.find_or_create_by(external_service_id_attributes(creative))
      es.update(artwork: @artwork) if @artwork
      es.update(identifier: @identifier) if @identifier
      es.update(state: "processing")
    end
  end

  def external_service_id_attributes(creative)
    { linkable_id: creative.id, linkable_type: "Creative", service_name: @service_name }
  end
end
