class PhysicalUpcService
  include UpcChecksumable

  PHYSICAL_UPC_BASE_LENGTH = 11

  WARNING_THRESHOLD = 15_000

  class NoPrefixes < StandardError; end

  class PrefixLimit < StandardError; end

  class UpcThresholdExceeded < StandardError; end

  def initialize(prefixes)
    raise NoPrefixes.new("No prefixes provided") if prefixes.blank?

    @prefixes = prefixes
  end

  def reserve_physical_upc(album)
    Upc.create(
      upc_type: "physical",
      number: get_upc,
      upcable: album
    )
  end

  private

  attr_reader :prefixes

  def get_upc
    prefixes.each do |p|
      begin
        upc = get_next_upc(p)
        notify(upc, p) if (p == prefixes.last)
        return upc
      rescue PrefixLimit => e
        next
      end
    end
    raise Upc::PhysicalUpcLimit.new("UPC limits reached for prefixes #{prefixes.join(', ')}")
  end

  def notify(upc, prefix)
    return unless (max_base(prefix) - get_base(upc).to_i) <= WARNING_THRESHOLD

    e = UpcThresholdExceeded.new("Physical Upc count is approaching its limit for prefix #{prefix}.")
    Airbrake.notify(e)
  end

  def get_next_upc(prefix)
    last_number = Upc.physical.where(number: first_possible_upc_number(prefix)..last_possible_upc_number(prefix)).maximum(:number)
    if last_number.blank?
      first_possible_upc_number(prefix)
    elsif last_number < last_possible_upc_number(prefix)
      checksummed(get_base(last_number).to_i + 1)
    else
      raise PrefixLimit.new "No physical upc prefixes left for #{prefix}"
    end
  end

  def first_possible_upc_number(prefix)
    checksummed(min_base(prefix))
  end

  def last_possible_upc_number(prefix)
    checksummed(max_base(prefix))
  end

  def get_base(number)
    number[0..-2]
  end

  def max_base(prefix)
    (prefix + "9" * (PHYSICAL_UPC_BASE_LENGTH - prefix.length)).to_i
  end

  def min_base(prefix)
    (prefix + "0" * (PHYSICAL_UPC_BASE_LENGTH - prefix.length)).to_i
  end
end
