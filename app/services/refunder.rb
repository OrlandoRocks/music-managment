class Refunder
  def self.refund_double_charges(opts)
    new(opts).tap(&:refund_double_charges)
  end

  def self.refund_double_charges_dry_run(opts)
    new(opts).tap(&:refund_double_charges_dry_run)
  end

  attr_reader :start_date, :admin, :successful_balance_refunds, :successful_cc_refunds, :failed_balance_refunds, :failed_cc_refunds

  def initialize(opts)
    @start_date                 = opts[:start_date]
    @admin                      = Person.find(opts[:admin_id])
    @for_real                   = opts[:for_real]
    @successful_balance_refunds = []
    @failed_balance_refunds     = []
    @successful_cc_refunds      = []
    @failed_cc_refunds          = []
  end

  def refund_double_charges
    if @for_real
      refund_credit_card_transactions(double_credit_card_transactions, "person was charged twice", "other")
      refund_balance_transactions(double_balance_transaction_invoices, "Refund - Other", "Redfunding double charge", "customer was charged twice")
      log_results
    else
      put_and_log "Performing dry run. Pass in for_real: true to process refunds"
      refund_double_charges_dry_run
    end
  end

  def refund_double_charges_dry_run
    put_and_log "INVOICES FOR BALANCE TRANSACTIONS WITHOUT PURCHASE"
    put_and_log "total: #{double_balance_transaction_invoices.count}"
    put_and_log "invoice IDs: #{double_balance_transaction_invoices.pluck(:id).join("\n")}"
    put_and_log "CREDIT CARD TRANSACTIONS FOR INVOICES WITHOUT PURCHASE"
    put_and_log "total: #{double_credit_card_transactions.count}"
    put_and_log "cc transaction IDs: #{double_credit_card_transactions.pluck(:id).join("\n")}"
  end

  private

  def refund_credit_card_transactions(cc_transactions, refund_reason, category)
    cc_transactions.each do |cc_transaction|
      options = {
        refund_reason: refund_reason,
        refund_category: category,
        refunded_by_id: admin.id,
        ip_address: current_server_ip
      }
      refund = BraintreeTransaction.process_refund(
        cc_transaction.id,
        cc_transaction.amount,
        options
      )
      if refund.valid? && refund.success?
        @successful_cc_refunds << refund
      else
        @failed_cc_refunds << refund
        unless refund.valid? && refund.success?
          put_and_log "unable to enact refund for cc transaction #{cc_transaction.id}", "error"
        end
      end
    end
  end

  def refund_balance_transactions(invoices, category, admin_note, customer_note)
    invoices.each do |invoice|
      adjustment  = BalanceAdjustment.create(
        credit_amount: invoice.final_settlement_amount_cents.to_i / 100.0,
        person_id: invoice.person_id,
        category: category,
        admin_note: admin_note,
        posted_by_id: admin.id,
        posted_by_name: admin.name,
        customer_note: customer_note
      )
      if adjustment.valid?
        @successful_balance_refunds << adjustment
      else
        @failed_balance_refunds << adjustment
        put_and_log "Unable to credit person balance for person #{invoice.person_id} invoice #{invoice.id}", "error"
      end
    end
  end

  def double_credit_card_transactions
    @double_credit_card_transactions ||= double_braintree_transactions_query
  end

  def double_balance_transaction_invoices
    @double_balance_transaction_invoices ||= double_invoices_query
  end

  def double_braintree_transactions_query
    BraintreeTransaction.where(
      invoices_without_purchase.and(bts[:status].eq("success"))
    ).joins(
      bts_invoices_join
    ).joins(
      bts_purchases_join
    ).order(invoices[:created_at]).reverse_order
  end

  def double_invoices_query
    Invoice.select(
      [
        invoices[:id],
        invoices[:person_id],
        invoices[:final_settlement_amount_cents]
      ]
    ).joins(
      purchases_join
    ).joins(
      person_transactions_join
    ).where(
      invoices_without_purchase
    ).order(invoices[:created_at]).reverse_order
  end

  def bts
    BraintreeTransaction.arel_table
  end

  def invoices
    Invoice.arel_table
  end

  def purchases
    Purchase.arel_table
  end

  def bts_invoices_join
    bts.join(invoices).on(bts[:invoice_id].eq(invoices[:id])).join_sources
  end

  def person_transactions
    PersonTransaction.arel_table
  end

  def bts_purchases_join
    bts.join(purchases, Arel::Nodes::OuterJoin).on(purchases[:invoice_id].eq(invoices[:id])).join_sources
  end

  def purchases_join
    invoices.join(purchases, Arel::Nodes::OuterJoin).on(purchases[:invoice_id].eq(invoices[:id])).join_sources
  end

  def person_transactions_join
    invoices.join(person_transactions).on(invoices[:id].eq(person_transactions[:target_id]).and(person_transactions[:target_type].eq("Invoice"))).join_sources
  end

  def invoices_without_purchase
    purchases[:id].eq(nil)
                  .and(invoices[:settled_at].not_eq(nil)
      .and(invoices[:final_settlement_amount_cents].not_eq(nil)
      .and(invoices[:settled_at].gt(start_date))))
  end

  def current_server_ip
    Socket.ip_address_list.detect(&:ipv4_private?).try(:ip_address)
  end

  def log_results
    put_and_log "Successful Balance Adjustments created:"
    successful_balance_refunds.each do |balance_adjustment|
      put_and_log "balance adjustment id: #{balance_adjustment.id}"
      put_and_log "person id: #{balance_adjustment.person_id}"
      put_and_log "credit amount: #{balance_adjustment.credit_amount}"
    end

    put_and_log "Failed Balance Adjustments created:"
    failed_balance_refunds.each do |balance_adjustment|
      put_and_log "balance adjustment id: #{balance_adjustment.id}"
      put_and_log "person id: #{balance_adjustment.person_id}"
      put_and_log "credit amount: #{balance_adjustment.credit_amount}"
    end

    put_and_log "Successful Credit Card refund transactions created:"
    successful_cc_refunds.each do |cc_refund|
      put_and_log "cc_refund id: #{cc_refund.id}"
      put_and_log "person id: #{cc_refund.person_id}"
      put_and_log "invoice id: #{cc_refund.invoice_id}"
      put_and_log "amount: #{cc_refund.amount}"
    end

    put_and_log "Failed Credit Card refund transactions created:"
    failed_cc_refunds.each do |cc_refund|
      put_and_log "cc_refund id: #{cc_refund.id}"
      put_and_log "person id: #{cc_refund.person_id}"
      put_and_log "invoice id: #{cc_refund.invoice_id}"
      put_and_log "amount: #{cc_refund.amount}"
    end
  end

  def put_and_log(statement, level = "info")
    puts statement
    Rails.logger.send(level, statement)
  end
end
