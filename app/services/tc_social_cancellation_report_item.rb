class TcSocialCancellationReportItem
  attr_accessor :subscription_event_id,
                :person_id,
                :person_email,
                :subscription_created,
                :subscription_cancelled,
                :expected_renewal,
                :user_note

  def initialize(subscription_event)
    @subscription_event_id  = subscription_event.id
    @person_id              = subscription_event.try(:person_subscription_status).try(:person_id)
    @person_email           = subscription_event.try(:person_subscription_status).try(:person).try(:email)
    @subscription_created   = subscription_event.try(:subscription_purchase).try(:created_at)
    @subscription_cancelled = subscription_event.try(:created_at)
    @expected_renewal       = subscription_event.try(:person_subscription_status).try(:termination_date)
    @user_note              = subscription_event.notes.try(:first).try(:note)
  end

  def to_h
    attrs = {}
    instance_variables.each do |var|
      attrs[var.to_s.split("@").last] = instance_variable_get(var)
    end
    attrs
  end

  def self.column_names
    [
      "subscription_event_id",
      "person_id",
      "person_email",
      "subscription_created",
      "subscription_cancelled",
      "expected_renewal",
      "user_note"
    ]
  end
end
