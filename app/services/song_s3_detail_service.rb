# Helpers for querying S3Detail records and their metadata. Presence of s3_details is not guaranteed.
class SongS3DetailService
  include S3MetadataFetchable

  def self.by_album(album)
    new(album.songs.map(&:id))
  end

  attr_accessor :mp3s, :low_bitrate, :low_sample_rate
  attr_reader :songs

  def initialize(song_ids)
    @songs = Song.includes(:s3_detail).where(id: song_ids)

    @mp3s            = []
    @low_bitrate     = []
    @low_sample_rate = []

    process
  end

  def lo_fi
    low_bitrate | low_sample_rate | mp3s
  end

  private

  def process
    songs.each do |song|
      next if song.s3_detail.blank?

      filter_mp3s(song)
      filter_low_bitrate(song)
      filter_low_sample_rate(song)
    end
  end

  def filter_mp3s(song)
    @mp3s.push(song) if song.s3_detail.file_type == S3Detail::MP3_TYPE
  end

  # Adds low bitrate WAVs and FLACs to low_bitrate
  def filter_low_bitrate(song)
    return if song.s3_detail.file_type == S3Detail::MP3_TYPE

    case song.s3_detail.file_type
    when S3Detail::FLAC_TYPE
      low_bitrate.push(song) if low_bitrate_flac?(song.s3_detail)
    when S3Detail::WAV_TYPE
      low_bitrate.push(song) if low_bitrate_wav?(song.s3_detail)
    else
      # Do nothing, should not reach because of file type filters.
    end
  end

  # Adds low sample rate WAVs and FLACs to low_sample_rate
  def filter_low_sample_rate(song)
    return if song.s3_detail.file_type == S3Detail::MP3_TYPE

    low_sample_rate.push(song) if low_sample_rate?(song.s3_detail)
  end

  def audio_stream?(stream)
    stream["codec_type"] == S3Detail::CODEC_TYPE_AUDIO
  end

  def low_bitrate_flac?(detail)
    detail.metadata_json["streams"].any? do |stream|
      next unless audio_stream?(stream)

      Integer(stream["bits_per_raw_sample"], 10) < S3Detail::NORMAL_BITRATE
    end
  end

  def low_bitrate_wav?(detail)
    detail.metadata_json["streams"].any? do |stream|
      next unless audio_stream?(stream)

      stream["bits_per_sample"] < S3Detail::NORMAL_BITRATE
    end
  end

  def low_sample_rate?(detail)
    detail.metadata_json["streams"].any? do |stream|
      next unless audio_stream?(stream)

      Integer(stream["sample_rate"], 10) < S3Detail::NORMAL_SAMPLE_RATE
    end
  end
end
