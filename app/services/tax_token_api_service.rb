class TaxTokenApiService
  attr_reader :person_id, :tax_token, :tax_api

  def self.update(person_id, tax_token, tax_api = Payoneer::TaxTokenService)
    new(person_id, tax_token, tax_api).update
  end

  def initialize(person_id, tax_token, tax_api)
    @person_id = person_id
    @tax_token = tax_token
    @tax_api   = tax_api
  end

  def update
    new_token = tax_api.update(person_id)
    tax_token.update(token: new_token, tax_form_type: nil) if new_token
    tax_token
  end
end
