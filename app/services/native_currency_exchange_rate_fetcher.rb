# frozen_string_literal: true

class NativeCurrencyExchangeRateFetcher
  class ExchangeRateNotFoundError < StandardError; end

  class CountryNativeCurrencyMapNotFoundError < StandardError; end

  def initialize(country_iso_code:, source_currency:)
    @country_iso_code = country_iso_code
    @source_currency = String(source_currency)
  end

  def foreign_exchange_rate
    @foreign_exchange_rate ||=
      ForeignExchangeRate.latest_by_currency!(
        source: source_currency,
        target: native_currency
      )
  rescue ActiveRecord::RecordNotFound => e
    raise ExchangeRateNotFoundError.new(e)
  end

  private

  attr_reader :country_iso_code, :source_currency

  def native_currency
    @native_currency ||=
      Country::NATIVE_CURRENCY_MAP.fetch(country_iso_code) do |country_iso_code|
        raise CountryNativeCurrencyMapNotFoundError,
              "#{country_iso_code} doesn't have any native currency"
      end
  end
end
