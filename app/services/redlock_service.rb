class RedlockService
  class MissingKeyError < StandardError; end

  class MissingBlockError < StandardError; end

  attr_accessor :resource, :callback

  cattr_accessor :redlock_manager, default: Redlock::Client.new([ENV["REDIS_URL"]])
  attr_reader :result

  def initialize(resource, &callback)
    @resource          = resource
    @callback          = callback
    @result            = [:failure, nil]

    raise MissingKeyError.new if resource.blank?
    raise MissingBlockError.new unless callback
  end

  def with_lock
    redlock_manager.lock(resource, 2000) do |locked|
      next unless locked

      @result = [:success, callback.call]
    end

    result
  end

  def self.with_lock(resource, &callback)
    new(resource, &callback).with_lock
  end
end
