class TcSocialCancellationsReportForm < FormObject
  extend ActiveModel::Naming
  include ActiveModel::AttributeMethods

  attr_accessor :subscription_event_id,
                :person_id,
                :person_email,
                :subscription_created,
                :subscription_cancelled,
                :expected_renewal,
                :user_note,
                :start_date,
                :end_date

  def attributes
    attrs = {}
    instance_variables.each do |var|
      attrs[var.to_s.split("@").last.to_s] = instance_variable_get(var)
    end
    attrs
  end
end
