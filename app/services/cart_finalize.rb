class CartFinalize < FormObject
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  define_model_callbacks :save

  before_save :add_strategies
  before_save :process_payment

  validate :validate_vat_info
  validates_presence_of :person, :purchases, :ip_address

  attr_accessor :manager, :person, :payment_id, :purchases, :payment_method,
                :ip_address, :three_d_secure_nonce
  attr_writer :use_balance

  def initialize(opts = {})
    super
    @manager ||= Cart::Payment::Manager.new(person, purchases, ip_address)
  end

  def add_strategies
    manager.add_strategy(Cart::Payment::Strategies::Balance.new) if use_balance
    return if payment_id.blank?

    if payment_id == "paypal"
      manager.add_strategy(
        Cart::Payment::Strategies::Paypal.new(
          StoredPaypalAccount.currently_active_for_person(@person),
          @person
        )
      )
    else
      manager.add_strategy(
        Cart::Payment::Strategies::CreditCard.new(
          payment_id,
          three_d_secure_nonce
        )
      )
    end
  end

  def person_preference
    @person_preference ||= PersonPreference.find_by_person_id(person.id)
  end

  def use_balance
    if person.balance.positive?
      @use_balance = person_preference.try(:pay_with_balance) if @use_balance.blank?
      is_false_value?(@use_balance) ? false : true
    else
      false
    end
  end

  def process_payment
    manager.process!
  end

  def validate_vat_info
    vat_info = person.vat_information
    return true if vat_info.nil?

    # Calling save method to run the validations and save
    # the vat_registration_status attribute
    vat_info.save
    return true if vat_info.vat_valid?

    errors[:base] << (vat_info.vat_errors || vat_info.errors.full_messages.first)
    false
  end

  delegate :invoice, to: :manager

  def purchased_before?
    @person.purchased_before?
  end

  def save
    return false unless valid?

    run_callbacks :save do
      if manager.successful?
        true
      else
        if manager.notice
          errors.add(:base, manager.notice)
        else
          errors.add(:base, I18n.t("controllers.carts.issue_placing_your_order"))
        end
        false
      end
    end
  end
end
