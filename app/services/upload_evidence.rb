class UploadEvidence
  def initialize(options)
    @evidence = options[:evidence]
    @dispute = options[:dispute]
  end

  def process
    bucket.object(key).put(body: File.open(@evidence.path, "r"))
  end

  private

  def key
    "#{Rails.env}/#{directory_name}/#{file_name}"
  end

  def bucket
    S3_INVOICE_CLIENT.bucket(EVIDENCE_BUCKET)
  end

  def directory_name
    "#{@dispute.source_type}/#{@dispute.id}"
  end

  def file_name
    email = @dispute.person.email.split("@")[0]
    content_type = @evidence.content_type.split("/")[1]
    "#{email}-#{DateTime.now.strftime('%Y%d%m%H%M%S')}.#{content_type}"
  end
end
