class SongDistributionOptionUpdater < FormObject
  attr_accessor :option_name, :option_value, :upc_codes, :rejected_upc_codes

  validates_presence_of :option_name, :option_value, :upc_codes

  validates_inclusion_of :option_name, in: SongDistributionOption.all_options.keys.map(&:to_s)
  validates_inclusion_of :option_value, in: ["true", "false", true, false]

  def initialize(params = {})
    super
    @rejected_upc_codes = []
  end

  def self.update(args)
    new(args).tap(&:update)
  end

  def update
    return false unless valid?

    @upc_codes.each do |upc_code|
      if code = Upc.find_by(number: upc_code)
        code.upcable.songs.each { |song| song.send("#{@option_name}=", @option_value) }
      else
        @rejected_upc_codes << upc_code
      end
    end
  end
end
