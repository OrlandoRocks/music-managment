class DeliveryTcDistributorErrorNotifierService
  DAYS_IN_PAST_STANDARD = 3.days.freeze
  DAYS_IN_PAST_VIP      = 2.days.freeze
  EXTRACT_DURATION_DAYS = 1.day.freeze

  def self.notify(from_date = nil, to_date = nil, excluded_store_ids = [])
    new(from_date, to_date, excluded_store_ids).notify
  end

  attr_reader :excluded_store_ids,
              :from_date_standard,
              :to_date_standard,
              :from_date_vip,
              :to_date_vip

  def initialize(from_date = nil, to_date = nil, excluded_store_ids = [])
    @from_date_standard = from_date.nil? ? (Date.current - DAYS_IN_PAST_STANDARD) : from_date.to_date
    @from_date_vip      = to_date.nil?   ? (Date.current - DAYS_IN_PAST_VIP)      : from_date.to_date
    @to_date_standard   = @from_date_standard + EXTRACT_DURATION_DAYS
    @to_date_vip        = @from_date_vip      + EXTRACT_DURATION_DAYS
    @excluded_store_ids = Array(excluded_store_ids)
  end

  def notify
    notify_albums_standard
    notify_albums_vip
  end

  def notify_albums_standard
    failed_deliveries =
      DistributorAPI::ReleaseError.failed_deliveries(
        {
          start_date: from_date_standard,
          end_date: to_date_standard
        }
      )[0].http_body["failed_deliveries"]

    failed_deliveries_album_ids = failed_deliveries.pluck(:album_id).uniq
    vip_album_ids =
      Album.joins(:person).where(id: failed_deliveries_album_ids)
           .where(people: { vip: true }).pluck(:id)

    failed_deliveries_by_album_id =
      failed_deliveries.delete_if do |failed_delivery|
        failed_delivery[:store_id].in?(excluded_store_ids) ||
          failed_delivery[:album_id].in?(vip_album_ids)
      end&.group_by { |object| object["album_id"] }

    album_errors =
      album_errors(failed_deliveries_by_album_id: failed_deliveries_by_album_id)

    send_delivery_errors_from_tc_distributor(album_errors: album_errors, from_date: from_date_standard)
  end

  def notify_albums_vip
    failed_deliveries =
      DistributorAPI::ReleaseError.failed_deliveries(
        {
          start_date: from_date_vip,
          end_date: to_date_vip
        }
      )[0].http_body["failed_deliveries"]

    failed_deliveries_album_ids = failed_deliveries.pluck(:album_id).uniq
    standard_album_ids =
      Album.joins(:person).where(id: failed_deliveries_album_ids)
           .where(people: { vip: false }).pluck(:id)

    failed_deliveries_by_album_id =
      failed_deliveries.delete_if do |failed_delivery|
        failed_delivery[:store_id].in?(excluded_store_ids) ||
          failed_delivery[:album_id].in?(standard_album_ids)
      end&.group_by { |object| object["album_id"] }

    album_errors =
      album_errors(failed_deliveries_by_album_id: failed_deliveries_by_album_id)

    send_delivery_errors_from_tc_distributor(album_errors: album_errors, from_date: from_date_vip)
  end

  def album_errors(failed_deliveries_by_album_id:)
    failed_deliveries_by_album_id.each_with_object([]) do |(album_id, failed_deliveries), arr|
      errors =
        failed_deliveries.sort_by! { |h| h["store_name"] }.flat_map do |failed_delivery|
          failed_delivery["errors"].each { |error| error["store_name"] = failed_delivery["store_name"] }
        end
      arr << { album_id: album_id, errors: errors }
    end
  end

  def send_delivery_errors_from_tc_distributor(album_errors:, from_date:)
    Array(album_errors).each do |album_error|
      next if album_error[:album_id].blank?

      DeliveryErrorFromTcDistributorMailer.delivery_errors_email(
        album_id: album_error[:album_id],
        errors: album_error[:errors],
        from_date: from_date
      ).deliver
    end
  end
end
