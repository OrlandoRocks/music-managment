# frozen_string_literal: true

module AddressLockedCountry
  module Exceptions
    class AddressLockNotFoundError < StandardError
      def message
        "Address lock not found for person"
      end
    end

    class CountryNotMappedError < StandardError
      def initialize(flag_reason)
        super()
        @flag_reason = flag_reason
      end

      def message
        "Country is not mapped for reason - #{@flag_reason.reason}"
      end
    end
  end
end
