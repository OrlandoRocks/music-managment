# frozen_string_literal: true

# This class provides an interface to find country of
# addrss_locked user based on flag_reason

module AddressLockedCountry
  class Finder
    include Exceptions

    def self.call(person)
      new(person).address_locked_country
    end

    def initialize(person)
      @person = person
      @address_lock_flag = Flag.find_by(Flag::ADDRESS_LOCKED)
      @address_lock_flag_reasons = FlagReason.address_lock_flag_reasons
    end

    def address_locked_country
      raise AddressLockNotFoundError unless person.address_locked?

      case reason
      when kyc_completed
        kyc_country
      when self_identified_address_locked
        self_identified_country
      else
        raise CountryNotMappedError.new(reason)
      end
    end

    private

    attr_reader :person, :address_lock_flag, :address_lock_flag_reasons

    delegate :kyc_country,
             :self_identified_country,
             :address_locked?,
             to: :person,
             private: true

    def reason
      @reason ||= person
                  .people_flags
                  .latest_for_flag(address_lock_flag)
                  .flag_reason
    end

    def kyc_completed
      address_lock_flag_reasons.find_by(
        reason: FlagReason::PAYONEER_KYC_COMPLETED
      )
    end

    def self_identified_address_locked
      address_lock_flag_reasons.find_by(
        reason: FlagReason::SELF_IDENTIFIED_ADDRESS_LOCKED
      )
    end
  end
end
