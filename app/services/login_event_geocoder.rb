class LoginEventGeocoder
  def self.geocode(login_event_id)
    login_event = LoginEvent.find(login_event_id)
    geocoded_attributes = IpAddressGeocoder.geocoded_attributes(login_event.ip_address)
    login_event.update(geocoded_attributes)
  end
end
