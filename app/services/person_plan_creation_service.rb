# frozen_string_literal: true

class PersonPlanCreationService
  include AfterCommitEverywhere

  class PurchasePlanCreationServiceError < StandardError; end

  def self.call(args)
    new(args).call
  end

  attr_reader :person, :plan, :change_type, :purchase, :expiration_date, :start_date

  def initialize(args)
    @person = Person.find(args[:person_id])
    @plan = Plan.find(args[:plan_id])
    @purchase = Purchase.find(args[:purchase_id]) if args[:purchase_id].present?
    @change_type = default_change_type(args[:change_type])
    @expiration_date = args[:expires_at] || Date.today + 1.year
    @start_date = args[:starts_at] || Date.today
  end

  def call
    begin
      if person.has_plan?
        in_transaction do
          person_plan = person.person_plan
          person_plan.update!(plan: plan)

          person_plan.renew!(purchase) unless change_type == PersonPlanHistory::REQUESTED_DOWNGRADE

          create_plan_history(change_type, purchase&.discount_reason)
        end
      else
        in_transaction do
          person_plan = PersonPlan.create!(
            plan: plan,
            person: person
          )
          person_plan.renew!(purchase)
          create_plan_history(PersonPlanHistory::INITIAL_PURCHASE, purchase.discount_reason)
        end
      end
    rescue => e
      log_airbrake_and_raise(e)
      raise
    end
  end

  def create_plan_history(change_type, discount_reason)
    PersonPlanHistory.create!(
      person: person,
      plan: plan,
      purchase: purchase,
      change_type: change_type,
      discount_reason: discount_reason,
      plan_start_date: start_date,
      plan_end_date: expiration_date
    )
  end

  def log_airbrake_and_raise(error)
    Airbrake.notify(
      PurchasePlanCreationServiceError,
      {
        message: error.message,
        person_id: person.id,
        plan_id: plan.id,
        change_type: change_type
      }
    )
    raise error
  end

  def default_change_type(change_type)
    return PersonPlanHistory::INITIAL_PURCHASE unless person.has_plan?
    return PersonPlanHistory::RENEWAL if person.plan == plan
    return PersonPlanHistory::USER_UPGRADE if person.plan.upgrade_eligible?(plan.id)
    return change_type if downgrade?
    return PersonPlanHistory::DEFAULT_DOWNGRADE if change_type.nil?

    return change_type if PersonPlanHistory::VALID_CHANGE_TYPES.value?(change_type)

    error_msg = "Change Type Is Invalid"

    error = PurchasePlanCreationServiceError.new(
      "#{error_msg} for person_id: #{person.id} plan_id: #{plan.id}"
    )
    log_airbrake_and_raise(error)
  end

  private

  def downgrade?
    PersonPlanHistory::DOWNGRADES.include?(change_type)
  end
end
