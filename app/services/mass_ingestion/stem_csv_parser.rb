class MassIngestion::StemCsvParser
  DATE_FORMAT = "%m/%d/%Y"
  GOLIVE_DATE_STR = "07/29/2019"
  GOLIVE_DATE = Date.strptime(GOLIVE_DATE_STR, DATE_FORMAT)
  APPLE_ID_URL_BASE = ->(id) { "https://itunes.apple.com/country/artist/artist-slug/#{id}" }
  SPOTIFY_ID_URL_BASE = ->(id) { "https://open.spotify.com/artist/#{id}" }
  STEM_TC_ROLE_MAP = { "primary" => "primary_artist", "featured" => "featuring" }
  STORES_TO_BE_SKIPPED = ["AmazonOD", "YoutubeSR", "FBTracks", "Instagram"]
  STORE_VARIABLE_PRICE_MAP = {
    "iTunesWW" => {
      "front" => "4",
      "mid" => "3",
      "back" => "2"
    },
    "Amazon" => {
      "front" => "FRONTLINE",
      "mid" => "MIDLINE",
      "back" => "CATALOG"
    }
  }
  SOURCE = "STEM"
  PRICE_ADJUSTMENT_TEXT = "Price adjusted for STEM"

  attr_accessor :csv_rows,
                :first_csv_row

  # STEM DOC https://docs.google.com/document/d/1s24kdCIWJ9HA0opPZSXPclUDG4fv3cFwVNcJ1NfoAco/edit
  #
  # CSV HEADERS
  # album: stem_account_email_address
  # album: tunecore_optin_email_address
  # album: album_title
  # album: album_artist
  # album: album_role
  # album: various_artist
  # album: apple_artist_id
  # album: spotify_artist_id
  # album: label
  # album: genre
  # album: upc
  # album: original_release
  # album: album_language
  # album: release
  # album: territory
  # album: variable_pricing
  # album: reviewed_at
  # track: track_title
  # track: sequence
  # track: duration
  # track: lyric
  # track: track_artist
  # track: track_role
  # track: isrc
  # track: track_language
  # track: clean_version
  # track: songwriter
  # track: publishing
  # track: cline

  def initialize(csv_io)
    @csv_rows = CSV.new(csv_io, headers: true, header_converters: [:symbol])
                   .read
                   .by_row
                   .map(&:to_h)

    @first_csv_row = @csv_rows[0]
  end

  def person
    Person.find_by(email: first_csv_row[:tunecore_optin_email_address]) ||
      Person.find_by(email: first_csv_row[:stem_account_email_address])
  end

  def primary_genre_id
    genre = first_csv_row[:genre]
    genre = "Christian/Gospel" if genre == "Christian & Gospel"

    Genre.available_album_genres.find_by(name: genre).try(:id) ||
      Genre.find_by(name: "Rock").id
  end

  def language_code
    album_language = first_csv_row[:album_language]
    (album_language == "zh") ? "cmn-Hant" : album_language
  end

  def countries
    country_iso_codes = first_csv_row[:territory].split(":").map(&:upcase)
    countries = Country.available_countries

    if country_iso_codes.include?("WW")
      countries
    else
      countries.where(iso_code: country_iso_codes)
    end
  end

  def orig_release_year
    Date.strptime(first_csv_row[:original_release], DATE_FORMAT) if first_csv_row[:original_release].present?
  end

  def album_artists_info
    artist_names = (first_csv_row[:album_artist] || "").split(":")
    apple_artist_ids = (first_csv_row[:apple_artist_id] || "").split(":")
    spotify_artist_ids = (first_csv_row[:spotify_artist_id] || "").split(":")
    album_roles = (first_csv_row[:album_role] || "").split(":")

    artists_info =
      artist_names.zip(
        apple_artist_ids,
        spotify_artist_ids,
        album_roles
      ).map do |name_appleid_spotifyid_role|
        {
          name: name_appleid_spotifyid_role[0],
          apple_artist_id: name_appleid_spotifyid_role[1],
          spotify_artist_id: name_appleid_spotifyid_role[2],
          role: STEM_TC_ROLE_MAP[name_appleid_spotifyid_role[3]]
        }
      end

    artists_info.select { |a| a[:role] == "primary_artist" }
  end

  def album_creatives
    return [] if is_various?

    album_artists_info.map { |artist_info|
      { name: artist_info[:name], role: artist_info[:role] }
    }.map(&:stringify_keys)
  end

  def apple_artist_url(a_id)
    return "" if a_id.blank?

    APPLE_ID_URL_BASE.call(a_id)
  end

  def spotify_artist_url(s_id)
    return "" if s_id.blank?

    SPOTIFY_ID_URL_BASE.call(s_id)
  end

  def artist_urls
    album_artists_info.map do |artist_info|
      {
        artist_name: artist_info[:name],
        apple: apple_artist_url(artist_info[:apple_artist_id]),
        spotify: spotify_artist_url(artist_info[:spotify_artist_id])
      }
    end
  end

  def album_type
    (csv_rows.length == 1) ? "Single" : "Album"
  end

  def available_stores
    klass = (album_type == "Single") ? Single : Album

    available_stores = []
    available_stores += klass.stores.is_active

    available_stores << Store.find_by(short_name: "BelieveLiv")

    # adding only iTunesWW and remove all other iTunes stores
    available_stores.reject! { |s| s.short_name.include?("iTunes") }
    available_stores << Store.find_by(short_name: "iTunesWW")

    available_stores.reject! { |s| STORES_TO_BE_SKIPPED.include?(s.short_name) }

    if Genre.spoken_word_genre === Genre.find_by(id: primary_genre_id)
      available_stores.reject! { |s| s.short_name == "Shazam" }
    end

    available_stores
  end

  def variable_price_for_store(store)
    store_map = STORE_VARIABLE_PRICE_MAP[store.short_name]

    return if store_map.blank?

    price_code = store_map[first_csv_row[:variable_pricing].to_s]

    store.variable_prices.find_by(price_code: price_code) if price_code.present?
  end

  def album_attributes
    {
      name: first_csv_row[:album_title],
      language_code_legacy_support: language_code,
      orig_release_year: orig_release_year,
      primary_genre_id: primary_genre_id,
      sale_date: GOLIVE_DATE,
      previously_released: true,
      label_name: first_csv_row[:label],
      optional_upc_number: first_csv_row[:upc],
      golive_date: GOLIVE_DATE,
      creatives: album_creatives,
      created_with: Album.created_withs[:songwriter_relaxed],
      is_various: is_various?,
      source: SOURCE,
      apple_music: true
    }
  end

  def track_artists_info(row)
    artist_names = (row[:track_artist] || "").split(":")
    track_roles = (row[:track_role] || "").split(":")

    artist_names.zip(track_roles).map do |name_role|
      { name: name_role[0], role: STEM_TC_ROLE_MAP[name_role[1]] }
    end
  end

  def song_data_form_artists(row)
    artists = track_artists_info(row).map { |artist|
      is_primary_in_album_level = album_artists_info.any? { |a| a[:name] == artist[:name] }

      if !is_primary_in_album_level || album_type == "Single"
        { artist_name: artist[:name], associate_to: "Song", credit: artist[:role], role_ids: [] }
      end
    }.compact

    songwriter_names = (row[:songwriter] || "").split(":")
    return artists if songwriter_names.empty?

    songwriter_song_role_id = SongRole.find_by(role_type: "songwriter").id

    songwriter_names.map do |sw_name|
      artist = artists.find { |a| a[:artist_name] == sw_name }
      if artist.present?
        artist[:role_ids] << songwriter_song_role_id
      else
        artists << {
          artist_name: sw_name,
          associate_to: "Song",
          credit: "contributor",
          role_ids: [songwriter_song_role_id]
        }
      end
    end

    artists
  end

  def songs_attributes(album_id = nil)
    @csv_rows
      .sort_by { |r| r[:sequence].to_i }
      .map do |row|
        {
          person_id: person.id,
          skip_has_songwriter: true,
          song_params: {
            name: row[:track_title],
            lyrics: row[:lyric],
            optional_isrc: row[:isrc],
            explicit: (row[:clean_version] || "").casecmp?("false"),
            artists: song_data_form_artists(row),
            album_id: album_id
          }
        }
      end
  end

  def is_various?
    (first_csv_row[:album_artist] || "") == "Various Artists"
  end
end
