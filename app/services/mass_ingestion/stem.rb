class MassIngestion::Stem
  FREE_TRIAL_DAYS = 90
  REFERRAL_IDENTIFIER = "stem"

  def self.free_trial_days_left
    (free_trial_end_date - Date.today).to_i
  end

  def self.free_trial_end_date
    rollout_date + FREE_TRIAL_DAYS
  end

  def self.rollout_date
    Date.parse(ENV["STEM_ROLLOUT_DATE"] || "29-Jul-2019")
  end
end
