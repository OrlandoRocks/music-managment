class MassIngestion::PersonService
  def self.ingest(filepath, source)
    new(filepath, source).ingest
  end

  def initialize(filepath, source)
    @filepath = filepath
    @source   = source
  end

  def ingest
    newbies = CSV.read(filepath, headers: true)
    who_are_these_people(newbies)
  end

  private

  attr_reader :filepath, :source

  def who_are_these_people(peeps)
    peeps.each do |peep|
      optin_email   = peep["tunecore_optin_email"]
      source_email  = peep["#{source}_account_email"]

      if optin_email.present? && Person.exists?(email: optin_email)
        adopt(optin_email)
      elsif Person.exists?(email: source_email)
        adopt(source_email)
      else
        birth(peep)
      end
    end
  end

  def adopt(email)
    Person.find_by(email: email).update(
      referral: source,
      referral_type: "migration",
      referral_campaign: "tc-#{source}"
    )
  end

  def birth(source_person)
    email = source_person["#{source}_account_email"]
    pw    = SecureRandom.hex(5) + "#{source.upcase}45!"
    code  = source_person["country"]
    name  = source_person["name"]&.titleize&.ljust(5, " ")

    person = Person.create!(
      email: email,
      password: pw,
      password_confirmation: pw,
      name: name,
      country: Country.exists?(iso_code: code) ? code : "US",
      referral: source,
      referral_type: "migration",
      referral_campaign: source,
      accepted_terms_and_conditions_on: source_person["accepted_terms_and_conditions_on"]&.to_datetime,
      country_audit_source: CountryAuditSource::SELF_IDENTIFIED_NAME
    )
    person.mark_as_verified
  end
end
