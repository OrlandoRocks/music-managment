class MassIngestion::AlbumService
  include ApplicationHelper
  include SsoCookies

  INGESTION_LOG_DIR = Rails.root.join("log", "mass_ingestions")
  ALBUM_LOG_DIR = Rails.root.join("log", "mass_ingestions", "albums")

  attr_accessor :bucket_name,
                :bucket_album_dir,
                :upc_str,
                :parser_klass,
                :upc_download_dir,
                :parser,
                :resource,
                :album_attributes,
                :person,
                :resource_class_name,
                :purchase,
                :bigbox_responses,
                :artwork,
                :logger

  def initialize(bucket_name, bucket_album_dir, parser_klass: MassIngestion::StemCsvParser)
    # (https://tunecore.atlassian.net/browse/SC-851)
    # adding the feature flipper so rubo doesn't get mad, we won't ever add this feature so it will always raise
    unless FeatureFlipper.show_feature?(:dont_show)
      raise "The spec is broken, fix the spec before enabling this feature"
    end

    @bucket_name = bucket_name
    @bucket_album_dir = bucket_album_dir
    @upc_str = bucket_album_dir.split("/")[-1]
    @parser_klass = parser_klass
    @upc_download_dir = "/tmp/#{@upc_str}"
    @successful = false
    @logger = ActiveSupport::TaggedLogging.new(Logger.new(STDOUT))
  end

  def csv_file
    csv_wildcard = File.join(upc_download_dir, "*.csv")
    csv_path = Dir.glob(csv_wildcard, File::FNM_CASEFOLD).first
    File.new(csv_path)
  end

  def audio_files
    audio_wildcard = File.join(upc_download_dir, "*.{wav,mp3,flac}")
    Dir.glob(audio_wildcard, File::FNM_CASEFOLD)
       .map { |f| File.new(f) }
       .sort_by { |f| File.basename(f.path).split(" ")[0].to_i }
  end

  def artwork_file
    artwork_wildcard = File.join(upc_download_dir, "*.{jpg,jpeg}")
    artwork_path = Dir.glob(artwork_wildcard, File::FNM_CASEFOLD).first
    File.new(artwork_path)
  end

  def skip_upc_check_and_optional_upc_creation?
    File.exist?(File.join(upc_download_dir, "skip_upc_check_and_optional_upc_creation"))
  end

  def process
    remove_files_in_upc_download_dir
    download

    if !skip_upc_check_and_optional_upc_creation? && Upc.exists?(number: upc_str)
      record_and_log_status("skipped")
      trace_log("Skipping because #{upc_str} already exists")
      return
    end

    @parser = parser_klass.new(csv_file)

    set_person
    create_resource
    upload_audio_files_to_bigbox
    create_artwork

    ActiveRecord::Base.transaction do
      create_songs
      associate_artwork
      add_stores
      associate_countries
      associate_product
      create_note

      @successful = true
    end

    if @successful
      enqueue_jobs
      record_and_log_status("successful")
    end

    remove_files_in_upc_download_dir
  rescue => e
    cleanup
    trace_log(e)
    trace_log(e.message)
    trace_log(e.backtrace)
    record_and_log_status("failed", e)
  end

  private

  def bucket
    AWS::S3.new(
      region: "us-east-1",
      access_key_id: ENV["VENDOR_AWS_ACCESS_KEY"],
      secret_access_key: ENV["VENDOR_AWS_SECRET_KEY"]
    ).buckets[bucket_name]
  end

  def record_and_log_status(status, e = nil)
    status_log(status)

    error_info = {}
    if e.present?
      error_info[:error] = e.to_s
      error_info[:error_message] = e.message
    end

    info = {
      bucket_name: bucket_name,
      bucket_album_dir: bucket_album_dir,
      parser: parser_klass.to_s
    }.merge(error_info)

    MassIngestionStatus.create(
      identifier: upc_str,
      identifier_type: "UPC",
      status: status,
      info: info.to_json
    )
  end

  def status_log(message)
    logger.tagged("ingestion-status upc: #{upc_str}") { logger.info(message) }
  end

  def trace_log(message)
    logger.tagged("ingestion-trace upc: #{upc_str}") { logger.info(message) }
  end

  def download
    trace_log("Creating directory #{upc_download_dir}")
    FileUtils.mkdir_p(upc_download_dir)

    trace_log("Downloading files from S3 #{bucket_album_dir} to Local #{upc_download_dir}")
    bucket
      .as_tree(prefix: bucket_album_dir)
      .children
      .select(&:leaf?)
      .each do |leaf_node|
      filepath = "#{upc_download_dir}/#{File.basename(leaf_node.key)}"
      trace_log("Downloading #{leaf_node.key}")

      File.open(filepath, "wb") do |file|
        leaf_node.object.read do |chunk|
          file.write(chunk)
        end
      end
    end
  end

  def set_person
    trace_log("--- parsing person ---")
    @person = parser.person
    trace_log("parsed person {person_id: #{person.id}, email: #{person.email}}")
  end

  def create_resource
    trace_log("--- processing album ---")
    @album_attributes = parser.album_attributes
    if skip_upc_check_and_optional_upc_creation?
      trace_log("optional upc #{@album_attributes[:optional_upc_number]} removed from album attributes")
      @album_attributes.delete(:optional_upc_number)
    end
    trace_log("album attributes: #{album_attributes}")

    trace_log("--- Creating Album/Single ---")
    @resource =
      if parser.album_type == "Album"
        person.albums.build(album_attributes)
      else
        Single.new(album_attributes.merge(person: person))
      end

    resource.save!
    @resource_class_name = resource.class.to_s
    trace_log("Album created successfully album_id: #{resource.id}")
  end

  def create_note
    note_params = {
      related: resource,
      note_created_by: person,
      subject: "Ingested for STEM",
      note: "#{resource_class_name} was created. UPC: #{upc_str}."
    }
    trace_log("--- create Note ---")
    trace_log("Note params: #{note_params}")
    Note.create!(note_params)
    trace_log("Note created successfully")
  end

  def create_songs
    trace_log("--- processing songs ---")
    songs_attributes = parser.songs_attributes(resource.id)
    trace_log("parsed songs count - #{songs_attributes.length}")

    songs_attributes.each_with_index do |song_attributes, i|
      trace_log("--- creating song #{song_attributes[:song_params][:name]} ---")
      trace_log("song attributes: #{song_attributes}")
      song_attributes[:song] = resource.song if resource_class_name == "Single"
      sdf = SongDataForm.new(song_attributes)
      raise "Unable to save Song - #{sdf.errors.full_messages}" unless sdf.save

      trace_log("Song saved successfully")

      song = sdf.song
      bigbox_response = bigbox_responses[i]
      uploads_registration_form_params = {
        key: bigbox_response["key"],
        uuid: bigbox_response["uuid"],
        bucket: bigbox_response["bucket"],
        song_id: song.id,
        orig_filename: bigbox_response["orig_filename"],
        bit_rate: bigbox_response["bit_rate"],
        person: person
      }
      trace_log("uploads_registration_form_params: #{uploads_registration_form_params}")
      upload_register_form = UploadsRegisterForm.new(uploads_registration_form_params)
      raise "Unable to register upload - #{upload_register_form.errors.full_messages}" unless upload_register_form.save

      trace_log("Upload registration successful")
    end
  end

  def associate_artwork
    trace_log("--- associating artwork ---")
    artwork.album = resource

    raise "Unable to upload artwork: #{artwork.last_errors}" unless artwork.save && artwork.uploaded?

    trace_log("artwork associated successfully")
  end

  def associate_countries
    trace_log("--- adding countries ---")
    parsed_countries = parser.countries
    trace_log("Parsed countries: #{parsed_countries.map(&:name)}")
    resource.countries = parsed_countries
    trace_log("countries added successfully")
  end

  def associate_product
    trace_log("--- Associating product ---")
    product_type = "one_year_#{parser.album_type}".downcase.to_sym
    product_id = Product.find_products_for_country(parser.person.country_domain, product_type)
    product = Product.find(product_id)
    @purchase = Product.add_to_cart(person, resource, product)
    purchase.adjust_price(0.0, parser_klass.const_get("PRICE_ADJUSTMENT_TEXT"), person)
    cart_finalize_form_params = {
      person: person,
      use_balance: true,
      ip_address: "tunecore_dev_center",
      purchases: [purchase]
    }
    trace_log("CartFinalizeFormParams: #{cart_finalize_form_params}")
    cf_form = CartFinalizeForm.new(cart_finalize_form_params)
    raise "Unable to associate product" unless cf_form.save

    trace_log("product associated successfully ---")

    trace_log("--- Overriding renewal expires to 90 days ---")
    renewal_history = resource.renewal_histories.first
    renewal_history.update!(expires_at: MassIngestion::Stem.free_trial_end_date)
    trace_log("successfully set renewal_history expires to #{renewal_history.expires_at} ---")
  end

  def upload_audio_files_to_bigbox
    trace_log("--- generating tc pid ---")
    tc_pid = "#{person.id}-#{generate_hmac_sha1(person.id.to_s)}"
    trace_log("tc_pid: #{tc_pid}")

    @bigbox_responses =
      audio_files.map do |af|
        upload_params = {
          Filedata: af,
          tc_pid: tc_pid,
          packager_class: "BasicPackager"
        }
        trace_log("--- uploading audio file ---")
        trace_log("upload params: #{upload_params}")
        response = RestClient::Request.execute(
          method: :post,
          url: BIGBOX_UPLOAD_URL,
          payload: upload_params,
          timeout: 10 * 60
        )
        trace_log("audio upload successful")
        response_json = JSON.parse(response.to_s)["response"]
        trace_log("audio upload response #{response_json}")
        response_json
      end
    trace_log("successfully uploaded audio files to bigbox")
  end

  def create_artwork
    trace_log("--- uploading artwork ---")
    @artwork = Artwork.new(album_id: resource.id)
    artwork.assign_artwork(artwork_file)

    raise "Unable to upload artwork: #{artwork.last_errors}" unless artwork.save && artwork.uploaded?

    trace_log("artwork added successfully")
  end

  def remove_files_in_upc_download_dir
    trace_log("Removing folder #{upc_download_dir}")
    FileUtils.rm_f(upc_download_dir)
  end

  def cleanup
    remove_files_in_upc_download_dir

    if resource.present? && resource.persisted?
      resource.songs.map(&:destroy!)
      resource.creatives.map(&:destroy!)
      resource.destroy!
    end

    artwork.destroy! if artwork.present? && artwork.persisted?
  end

  def enqueue_jobs
    trace_log("--- calling SuccessfulPurchaseWorker ---")
    SuccessfulPurchaseWorker.perform_async(purchase.invoice.id)

    trace_log("--- calling ArtistIdCreationWorker ---")
    trace_log("Parsed artist urls: #{parser.artist_urls}")

    ArtistIdCreationWorker.perform_async(album_id: resource.id, artist_urls: parser.artist_urls)
  end

  def add_stores
    trace_log("--- adding stores ---")
    parsed_stores = parser.available_stores
    trace_log("Parsed stores: #{parsed_stores.map(&:name)}")
    salepoints, stores_not_added =
      resource.add_stores(parsed_stores) do |salepoint_values|
      variable_price = parser.variable_price_for_store(salepoint_values[:store])
      salepoint_values[:variable_price_id] = variable_price.id if variable_price.present?
      salepoint_values
    end

    raise "Unable to add stores: #{stores_not_added}" if stores_not_added.length.positive?
  end
end
