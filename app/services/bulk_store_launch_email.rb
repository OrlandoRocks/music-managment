class BulkStoreLaunchEmail
  attr_accessor :store

  def self.send_store_launch_emails(store_id)
    new(store_id).tap(&:send_store_launch_emails)
  end

  def initialize(store_id)
    @store = Store.find(store_id)
    raise "store has not been launched" unless store.launched_at

    @email_list = get_email_list
  end

  def send_store_launch_emails
    @email_list.keys.each do |person_id|
      Rails.logger.info "Queuing store email for #{store.name} for person #{person_id}"
      StoreLaunchEmailWorker.perform_async(store.id, person_id, @email_list[person_id])
    end
  end

  def get_email_list
    email_list = $redis.get("StoreEmails:#{store.short_name}")
    if email_list
      Rails.logger.info "Found existing email list in redis for #{store.name}"
      email_list = JSON.parse(email_list)
    else
      Rails.logger.info "Generating email list for #{store.name}"
      email_list = create_email_list(store)
    end
  end

  def create_email_list(store)
    releases_by_person = build_releases_by_person(grouped_by_person)
    $redis.set("StoreEmails:#{store.short_name}", releases_by_person.to_json)
    releases_by_person
  end

  def build_releases_by_person(grouped_by_person)
    releases_by_person = {}
    grouped_by_person.keys.each do |person_id|
      releases_by_person[person_id] = grouped_by_person[person_id].map(&:album_id)
    end
    releases_by_person
  end

  def grouped_by_person
    sp_subs = SalepointSubscription
              .includes(:album)
              .joins(:album)
              .order("albums.person_id ASC")
              .where("salepoint_subscriptions.effective < ? and salepoint_subscriptions.is_active = ? and salepoint_subscriptions.finalized_at is not null and albums.takedown_at is null and albums.is_deleted = 0", store.launched_at, true)
    sp_subs.group_by { |ss| ss.album.person_id }
  end
end
