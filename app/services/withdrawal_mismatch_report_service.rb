# frozen_string_literal: true

class WithdrawalMismatchReportService
  attr_accessor :date

  REPORT_HEADERS = %w[
    transaction_id
    processed_at
    processed_by
    processor_last_login_ip
    processor_env
    raw_request
    raw_response
    has_discrepency
    discrepency_amount
    person_id
    country_website_id
    request_url
  ]

  def initialize(date = Date.current)
    @date = date
  end

  def generate_report
    return :no_mismatched_transactions_found unless mismatched_transactions.any?

    CSV.generate(headers: true) do |csv|
      csv << REPORT_HEADERS
      process_transactions.map do |transaction_meta|
        csv << transaction_meta
      end
    end
  end

  private

  def process_transactions
    mismatched_transactions.map do |mismatched_transaction|
      [
        mismatched_transaction.id,
        format_report_date(mismatched_transaction.tunecore_processed_at),
        mismatched_transaction.tunecore_processor_id,
        processor_last_login_ip(mismatched_transaction),
        Rails.env,
        mismatched_transaction.raw_api_request,
        mismatched_transaction.raw_api_response,
        true,
        discrepency_amount(mismatched_transaction),
        mismatched_transaction.person_id,
        mismatched_transaction.person&.country_website_id,
        mismatched_transaction.api_request_url
      ]
    end
  end

  def processor_last_login_ip(transaction)
    return if transaction.tunecore_processed_at.blank?

    LoginEvent
      .where(person_id: transaction.tunecore_processor_id)
      .where(created_at: transaction.tunecore_processed_at.all_day)
      .pluck(:ip_address)
      .last
  end

  def format_report_date(raw_date)
    raw_date&.strftime("%m/%d/%Y %I:%M:%S %p %Z")
  end

  def discrepency_amount(payout_transfer)
    request_amount    = BigDecimal(String(Money.new(payout_transfer.amount_cents)))
    processed_amount  = parse_processed_amount(payout_transfer)

    (request_amount - processed_amount).abs
  end

  def mismatched_transactions
    @mismatched_transactions ||=
      load_transactions_for_date
      .filter { |transaction| !transaction_amounts_matched?(transaction) }
  end

  def transaction_amounts_matched?(payout_transfer)
    return true if discrepency_amount(payout_transfer).zero?

    fee_amount = BigDecimal(String(Money.new(payout_transfer.fee_cents)))
    (discrepency_amount(payout_transfer) - fee_amount).abs < 0.10
  end

  def parse_processed_amount(payout_transfer)
    payout_transfer
      .payout_transfer_api_logs
      .responses
      .last
      .parse_body&.dig(:amount)
  end

  def load_transactions_for_date
    @load_transactions_for_date ||= PayoutTransfer
                                    .where(tunecore_processed_at: @date.all_day)
                                    .where(
                                      provider_status: [
                                        PayoutTransfer::REQUESTED,
                                        PayoutTransfer::COMPLETED,
                                        PayoutTransfer::PENDING
                                      ]
                                    )
                                    .eager_load(:payout_transfer_api_logs)
  end
end
