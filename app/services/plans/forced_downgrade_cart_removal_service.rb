# frozen_string_literal: true

class Plans::ForcedDowngradeCartRemovalService
  attr_reader :person

  def self.call(person)
    new(person).call
  end

  def initialize(person)
    @person = person
  end

  def call
    ApplicationRecord.transaction(requires_new: true) do
      person.purchases.unpaid.plan_related.destroy_all
    end
  end
end
