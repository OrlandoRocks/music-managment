# frozen_string_literal: true

class Plans::CanDoService
  class NotSymbolFeatureError < StandardError; end

  def self.can_do?(plan, feature)
    new(plan).can_do?(feature)
  end

  def initialize(plan)
    @plan = plan
  end

  attr_reader :plan

  def can_do?(feature)
    return true if plan.blank?
    raise NotSymbolFeatureError unless feature.instance_of?(Symbol)

    plan.plan_features.exists?(feature: feature)
  end
end
