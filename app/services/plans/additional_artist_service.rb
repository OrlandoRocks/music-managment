# frozen_string_literal: true

class Plans::AdditionalArtistService
  attr_reader :user

  def self.adjust_additional_artists(user)
    new(user).adjust_additional_artists
  end

  def self.calculate_new_artists(user, release)
    new(user).calculate_new_artists(release)
  end

  def self.clear_unpaid_additional_artist_purchases!(user)
    new(user).clear_unpaid_additional_artist_purchases!
  end

  def initialize(user)
    @user = user
  end

  def adjust_additional_artists
    return unless user.owns_or_carted_plan?

    clear_unpaid_additional_artist_purchases! if necessary_to_remove?

    add_additional_artists_to_cart
  end

  def necessary_to_remove?
    carted_artists? && carted_artist_count > artists_needed_to_purchase
  end

  def create_additional_artist_purchase
    additional_artist_product =
      Product.find_by(
        display_name: PlanAddon::ADDITIONAL_ARTIST,
        country_website_id: user.country_website.id
      )

    additional_artist = AdditionalArtist.create!(person_id: user.id)
    Product.add_to_cart(user, additional_artist, additional_artist_product)
  end

  def calculate_new_artists(release)
    active_artists_names = user.active_artists(release.finalized_at)

    release.artists.to_a.map(&:serializable_hash).count do |obj|
      !obj["name"].in?(active_artists_names)
    end
  end

  def clear_unpaid_additional_artist_purchases!
    user
      .additional_artists
      .active.purchase_unpaid.each(&:destroy_unpaid)
  end

  private

  def add_additional_artists_to_cart
    required_additional_artists_count.times { create_additional_artist_purchase }
  end

  def carted_purchases
    @carted_purchases ||= user.purchases.unpaid
  end

  def purchased_additional_artists
    user.additional_artists.paid.active.purchase_paid.count
  end

  def eligible_artist_count
    @eligible_artist_count ||= Plans::ArtistAuditService.new(user).current_required_artist_count
  end

  def artists_needed_to_purchase # subtraction does not include carted artists
    [eligible_artist_count - 1 - purchased_additional_artists, 0].max
  end

  def require_additional_artists?
    required_additional_artists_count.positive?
  end

  def required_additional_artists_count # subtraction includes carted artists
    return @required_additional_artists_count unless @required_additional_artists_count.nil?

    @required_additional_artists_count = [eligible_artist_count - 1 - carted_and_paid_additional_artist_count, 0].max
  end

  def carted_artists?
    carted_artist_count.positive?
  end

  def need_plan_upgrade?
    user.can_do?(:buy_additional_artists) == false
  end

  def carted_and_paid_additional_artist_count
    user.additional_artists.active.count
  end

  def carted_artist_count
    user.additional_artists.unpaid.active.purchase_unpaid.count
  end

  def create_plan_purchase
    plan_product = Plan
                   .find(Plan::PROFESSIONAL)
                   .products
                   .find_by!(country_website_id: user.country_website.id)

    Product.add_to_cart(user, plan_product, plan_product)
  end

  def no_releases_in_cart?
    @no_releases_in_cart ||= Purchase.releases_in_cart(user).blank?
  end

  def no_plan_in_cart?
    @no_plan_in_cart ||= !user.plan_in_cart?
  end
end
