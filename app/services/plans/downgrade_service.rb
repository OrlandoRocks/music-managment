# frozen_string_literal: true

class Plans::DowngradeService
  include AfterCommitEverywhere
  # Service to compute what takedowns need to occur and route to proper services
  # and finally change PersonPlan and related table(s)

  # The service will be provided with a user and the target plan.  The logic surrounding
  # whether this is a user requested or forced downgrade (and what plan the user is
  # downgrading to) will need to go elsewhere
  class NoUserError < StandardError; end

  class NotPlanUserError < StandardError; end

  class NoPlanError < StandardError; end

  class NotDowngradeError < StandardError; end

  attr_reader :user, :target_plan, :change_type, :purchase, :expires_at, :starts_at, :note_opts

  def self.call(user, target_plan, options = {})
    new(user, target_plan, options).call
  end

  def initialize(user, target_plan, options)
    raise NoUserError unless user.is_a?(Person)
    raise NotPlanUserError unless user.plan_user?
    raise NoPlanError unless target_plan.is_a?(Plan)

    raise NotDowngradeError unless user.plan.downgrade_eligible?(target_plan.id)

    @user = user

    @target_plan = target_plan

    @purchase = options[:purchase]
    @change_type = options[:change_type] # possible values in PersonPlanHistory::DOWNGRADES

    @starts_at = options[:starts_at] || Date.today
    @expires_at = options[:expires_at] || @starts_at + 1.year # for use in PersonPlanCreationService
    @note_opts = options[:note_opts] # hash used in Album#takedown!
  end

  def call
    in_transaction do
      if user.has_live_release?
        Plans::AdditionalArtistsTakedownService.downgrade_takedown!(
          user,
          note_opts
        ) if remove_additional_artists?
        Plans::StoreAutomatorTakedownService.call(user) if remove_automator?
        Plans::DspTakedownService.call(user, stores_to_remove) if remove_dsps?
      end
      downgrade_plan
    end
  end

  private

  def stores_to_remove
    @stores_to_remove ||=
      begin
        user_store_ids = Plans::RequiredStoresService.call(user, :takedown_related)
        plan_store_ids = target_plan.plans_stores.pluck(:store_id).index_with { |_id| true }
        user_store_ids.reject { |store_id| plan_store_ids[store_id] }
      end
  end

  def remove_additional_artists?
    user.active_additional_artists? &&
      !Plans::CanDoService.can_do?(target_plan, :buy_additional_artists)
  end

  def remove_dsps?
    stores_to_remove.length.positive?
  end

  def remove_automator?
    user.can_do?(:store_automator) && !Plans::CanDoService.can_do?(target_plan, :store_automator)
  end

  def downgrade_plan
    # we now allow nil purchases (and for our current flow, purchase WILL be nil)

    PersonPlanCreationService.call(
      person_id: user.id,
      plan_id: target_plan.id,
      purchase_id: purchase&.id,
      change_type: change_type,
      expires_at: expires_at,
      starts_at: starts_at
    )
  end
end
