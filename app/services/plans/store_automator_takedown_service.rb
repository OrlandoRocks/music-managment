# frozen_string_literal: true

class Plans::StoreAutomatorTakedownService
  class NoUserError < StandardError; end

  attr_reader :user

  def self.call(user)
    new(user).call
  end

  def initialize(user)
    raise NoUserError unless user.instance_of?(Person)

    @user = user
  end

  def call
    user.salepoint_subscriptions.each do |salepoint_subscription|
      salepoint_subscription.update!(is_active: false, effective: nil)
    end
  end
end
