# frozen_string_literal: true

class Plans::RequestedDowngradeService
  include AfterCommitEverywhere
  # For use within auto-renewal flow
  # Pre-processes any plan related downgrades, modifies
  # and the renewals array to pass along to the rest of the renewals flow

  attr_reader :person, :all_renewals, :non_plan_renewals, :downgrade_request
  attr_accessor :purchase, :invoice, :plan_renewal

  class NoPersonError < StandardError; end

  class NoDowngradeRequestError < StandardError; end

  class NoPlanProductItemError < StandardError; end

  def self.call(person, renewals, downgrade_request)
    new(person, renewals, downgrade_request).call
  end

  def initialize(person, renewals, downgrade_request)
    @person = person
    raise NoPersonError unless @person.is_a?(Person)

    @downgrade_request = downgrade_request
    raise NoDowngradeRequestError unless @downgrade_request.is_a?(PlanDowngradeRequest)

    @plan_renewal = renewals.find(&:plan_renewal?)
    @non_plan_renewals = renewals.reject(&:plan_renewal?)
  end

  def call
    return non_plan_renewals if @plan_renewal.nil?

    in_transaction do
      call_downgrade_service

      modify_renewal_plan
      mark_downgrade_request_as_processed
      non_plan_renewals + [plan_renewal]
    end
  end

  private

  def call_downgrade_service
    Plans::DowngradeService.call(
      person, downgrade_request.requested_plan,
      {
        purchase: nil,
        change_type: PersonPlanHistory::REQUESTED_DOWNGRADE,
        starts_at: plan_renewal.expires_at,
        note_opts: note_opts
      }
    )
  end

  def mark_downgrade_request_as_processed
    # we need to mark as "Completed" so it won't be used again
    downgrade_request.update(status: PlanDowngradeRequest::PROCESSED_STATUS)
  end

  def modify_renewal_plan
    downgraded_plan_product_item = downgrade_request
                                   .requested_plan
                                   .products
                                   .find_by(country_website_id: person.country_website_id)
                                   &.product_items
                                   &.first
    raise NoPlanProductItemError if downgraded_plan_product_item.nil?

    plan_renewal.item_to_renew = downgraded_plan_product_item
    plan_renewal.save!
  end

  def note_opts
    @note_opts ||= {
      user_id: 0,
      subject: "Requested Plan Downgrade",
      note: "person_id: #{person.id} downgrading from plan_id: #{person.plan.id} "\
            "to plan_id: #{downgrade_request.requested_plan_id}"
    }
  end
end
