# frozen_string_literal: true

class Plans::AdditionalArtistsTakedownService
  include AfterCommitEverywhere

  class NoPlanError < StandardError; end

  class NoReleasesError < StandardError; end

  class NoteOptionsError < StandardError; end

  attr_reader :plan_user, :preserved_artist, :note_opts

  def self.downgrade_takedown!(plan_user, note_opts)
    new(plan_user, note_opts).downgrade_takedown!
  end

  # note_opts: {
  #         user_id: admin.id,
  #         ip_address: ip_address,
  #         subject: "Plan Downgrade :(",
  #         note: "Bye Bye Album CYA"
  #       }
  def initialize(plan_user, note_opts)
    raise NoPlanError unless plan_user.has_plan?
    raise NoReleasesError unless plan_user.has_live_release?
    raise NoteOptionsError if [:user_id, :subject, :note].any? { |key| note_opts[key].blank? }

    priority_artist = plan_user.valid_priority_artist&.artist
    @plan_user = plan_user
    @preserved_artist = priority_artist.presence || plan_user.oldest_primary_artist
    @note_opts = note_opts
  end

  def downgrade_takedown!
    ApplicationRecord.transaction do
      after_rollback { :failure }
      after_commit { :success }

      takedown_albums.each do |a|
        a.takedown!(note_opts)
      end
    end
  end

  private

  def takedown_albums
    plan_user
      .albums
      .with_active_creatives
      .where.not(artists: { id: preserved_artist.id })
  end
end
