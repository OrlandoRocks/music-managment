# frozen_string_literal: true

class Plans::RenewAdditionalArtistsService
  attr_reader :person, :renewal, :product, :created_purchases

  class NoPersonError < StandardError; end

  class NoRenewalError < StandardError; end

  class NoProductError < StandardError; end

  class NonPlanProductError < StandardError; end

  class NoPlanError < StandardError; end

  def self.plan_and_additional_artist_purchases!(person, renewal, product)
    new(person, renewal, product).plan_and_additional_artist_purchases!
  end

  def self.additional_artist_purchases!(person, renewal, product)
    new(person, renewal, product).additional_artist_purchases!
  end

  def initialize(person, renewal, product)
    raise NoPersonError if person.blank?
    raise NoRenewalError if renewal.blank?
    raise NoProductError if product.blank?
    raise NonPlanProductError unless product.is_plan?
    raise NoPlanError unless person.has_plan?

    @person = person
    @renewal = renewal
    @product = product
    @created_purchases = []
  end

  def plan_and_additional_artist_purchases!
    plan_purchase!
    additional_artist_purchases!
  end

  def additional_artist_purchases!
    additional_artist_addons.each_with_index do |additional_artist, idx|
      # breaking to account for artists that have been taken down
      break if num_artists_to_renew == idx

      purchase = Product.add_to_cart(person, additional_artist, additional_artist_product)

      if purchase.invoice_id.nil?
        created_purchases << purchase
      else
        Rails.logger.info logger_message
      end
    end
    created_purchases
  end

  private

  def plan_purchase!
    purchase = Product.add_to_cart(person, renewal, product)
    if purchase.invoice_id.nil?
      created_purchases << purchase
    else
      Rails.logger.info logger_message
    end
  end

  def logger_message
    "Payment Batch | comment = Purchase skipped because it is in use person_id=#{person.id} renewal_id=#{renewal.id}"
  end

  def additional_artist_addons
    @additional_artist_addons ||= person.additional_artists.paid.active
  end

  def additional_artist_product
    @additional_artist_product ||= Product
                                   .find_by(
                                     display_name: PlanAddon::ADDITIONAL_ARTIST,
                                     country_website_id: person.country_website.id
                                   )
  end

  def num_artists_to_renew
    @num_artists_to_renew ||= person.active_artists.length - 1
  end
end
