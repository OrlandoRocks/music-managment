# frozen_string_literal: true

class Plans::PlanRenewalCardService
  include CurrencyHelper

  def initialize(person)
    @person = person
  end

  attr_reader :person

  delegate :country_website, :currency, :plan, :plan_user?, :plan_renewals, to: :person

  def renewal
    @renewal ||= plan_renewals.last
  end

  def renewal_price
    money_to_currency(
      renewal.price_to_renew_in_money,
      iso_code: country_website.country,
      currency: country_website.user_currency,
      round_value: country_website.india?
    )
  end

  def renewal_date
    renewal.expires_at.to_date
  end
end
