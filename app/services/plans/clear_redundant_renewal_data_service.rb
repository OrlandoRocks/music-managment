# frozen_string_literal: true

class Plans::ClearRedundantRenewalDataService
  PLAN_NAMES = ["new_artist", "rising_artist", "breakout_artist", "professional"].freeze

  attr_reader :delete_log
  attr_accessor :renewal_histories_to_cancel

  class NoValidFileError < StandardError; end

  def initialize(log_file = nil)
    raise NoValidFileError if invalid_file?(log_file)

    @bucket_name = "rake-data"
    @bucket_path = ENV.fetch("RAILS_ENV").to_s
    @file_name = "clear_redundant_renewal_data_for_plans_#{Time.now.iso8601}.log"
    @delete_log = log_file || Tempfile.new
    @renewal_histories_to_cancel = []
  end

  def call
    queue_redundant_renewal_histories_to_remove
    begin
      destroy_and_log_all
      delete_log.write("\nAll Done!")
    rescue
      delete_log.write("\nSomething Went Wrong! Aborting!")
    ensure
      delete_log.close
      upload_to_s3
      delete_log.unlink if delete_log.instance_of?(Tempfile)
    end
  end

  def self.call
    new.call
  end

  private

  def invalid_file?(file)
    file.present? && (file.instance_of?(File) || file.instance_of?(TempFile))
  end

  def destroy_and_log_all
    ActiveRecord::Base.transaction do
      delete_log.write("Deleting renewal items:\n\n")
      redundant_renewal_items.each { |renewal_item| destroy_and_log(renewal_item) }

      delete_log.write("\nDeleting renewal history:\n\n")
      renewal_histories_to_cancel.each { |renewal_history| destroy_and_log(renewal_history) }

      delete_log.write("\nDeleting renewals:\n\n")
      redundant_renewal_ids.each do |renewal_id|
        renewal = Renewal.find(renewal_id)
        destroy_and_log(renewal)
      end
    end
  end

  def purchase_ids_with_multiple_renewal_hists
    @purchase_ids_with_multiple_renewal_hists ||= RenewalHistory
                                                  .joins(purchase: :product)
                                                  .where(products: { display_name: PLAN_NAMES })
                                                  .group("purchases.id").having("COUNT(*) >= 2")
                                                  .pluck(:purchase_id)
  end

  def redundant_renewal_histories
    @redundant_renewal_histories ||= RenewalHistory
                                     .where(purchase_id: purchase_ids_with_multiple_renewal_hists)
                                     .distinct.order(id: :asc)
  end

  def queue_redundant_renewal_histories_to_remove
    renewal_histories_to_cancel.clear
    renewal_hist_hash = {}
    redundant_renewal_histories.each do |renewal_history|
      purchase_id = renewal_history.purchase_id
      renewal_histories_to_cancel << renewal_hist_hash[purchase_id] if renewal_hist_hash[purchase_id]
      renewal_hist_hash[purchase_id] = renewal_history
    end
  end

  def destroy_and_log(obj)
    Rails.logger.info("Deleting: #{obj}")
    delete_log.write("#{obj.to_json}\n")

    begin
      obj.destroy
    rescue StandardError => e
      Rails.logger.error(e)
      delete_log.write("#{e}\n")
      raise ActiveRecord::Rollback
    end

    return if obj.errors.blank?

    messages = "Model Errors: #{obj.errors.full_messages.join(',')}"
    Rails.logger.error(messages)
    delete_log.write("#{messages}\n")
    raise ActiveRecord::Rollback
  end

  def redundant_renewal_ids
    @redundant_renewal_ids ||= renewal_histories_to_cancel.map(&:renewal_id).uniq
  end

  def redundant_renewal_items
    @redundant_renewal_items ||= RenewalItem.where(renewal_id: redundant_renewal_ids)
  end

  def upload_to_s3
    begin
      S3LogService.upload_only(s3_options)
    rescue StandardError => e
      Rails.logger.error(e)
    end
  end

  def s3_options
    {
      bucket_name: @bucket_name,
      bucket_path: @bucket_path,
      file_name: @file_name,
      data: delete_log
    }
  end
end
