# frozen_string_literal: true

class Plans::PlanEligibilityService
  attr_reader :person, :plans_hash

  def self.plans_by_upgrade_status(person)
    new(person).plans_by_upgrade_status
  end

  def self.lower_plans_by_downgrade_status(person)
    new(person).lower_plans_by_downgrade_status
  end

  def initialize(person)
    @person = fetch_person(person)
  end

  def plans_by_upgrade_status
    Plan.purchasable_by_user.map do |plan|
      {
        plan: plan,
        eligible: Plans::SufficientPlanService.new(person, plan, :combined).call
      }
    end
  end

  def lower_plans_by_downgrade_status
    Plan.where(id: person.plan.lower_plan_ids).map do |lower_plan|
      service = Plans::SufficientPlanService.new(person, lower_plan, :combined)

      {
        plan_name: lower_plan.name,
        plan: lower_plan,
        downgrade_eligible: service.call,
        automator_ineligibility: automator_ineligible?(lower_plan),
        artist_ineligibility: service.sufficient_artists? == false,
        active_release_ineligibility: service.sufficient_stores? == false
      }
    end
  end

  private

  def fetch_person(person)
    return person if person.instance_of? Person

    Person.find(person)
  end

  # False if either the person doesn't have automator, or they have automator but the plan does not
  def automator_ineligible?(plan)
    person.has_automator? && (Plans::CanDoService.can_do?(plan, :store_automator) == false)
  end
end
