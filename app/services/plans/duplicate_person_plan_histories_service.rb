# frozen_string_literal: true

class Plans::DuplicatePersonPlanHistoriesService
  def self.call
    new.call
  end

  def initialize; end

  def call
    purchase_ids = PersonPlanHistory.group(:purchase_id).having("count(*) > 1").pluck(:purchase_id)
    purchase_ids.each do |purchase_id|
      histories = PersonPlanHistory.where(purchase_id: purchase_id).order(id: :asc)
      histories.drop(1).each(&:destroy!)
    end
  end
end
