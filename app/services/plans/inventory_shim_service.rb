# frozen_string_literal: true

# Create the inventories needed to match the legacy release/distribution credit flow.
# Required for post-purchase processing of salepoints, via the SuccessfulPurchaseService.
#
# What:
# Replicates the effect of product_item_rule processing for legacy purchases.
#
# Why:
# Plan release purchases don't have the associated product_item_rules needed for inventory creation.
class Plans::InventoryShimService
  include AfterCommitEverywhere

  CHILD_INVENTORY_TYPES = [Salepoint.name, Song.name].freeze

  def self.call(person, purchases)
    new(person, purchases).call
  end

  def initialize(person, purchases)
    @person    = person
    @purchases = purchases.instance_of?(Purchase) ? [purchases] : purchases
  end

  attr_reader :person, :purchases

  def call
    return unless user_has_plan?

    purchases.each do |purchase|
      transaction = create_inventories(purchase)
      Airbrake.notify(self.class.to_s, { purchase: purchase }) if transaction == :failure
    end
  end

  private

  def create_inventories(purchase)
    return unless purchase.plan_credit?

    parent_inventory = build_parent_inventory(purchase)

    child_inventories = build_child_inventories(parent_inventory, purchase)

    Inventory.transaction(requires_new: true) do
      after_rollback { return :failure }

      parent_inventory.save!
      child_inventories.each do |child|
        child.parent_id = parent_inventory.id
        child.save!
      end
    end
  end

  def build_parent_inventory(purchase)
    release_type   = purchase.related.related.album_type
    product_item   = get_product_item(release_type)
    inventory_type = release_type
    title          = product_item.name

    Inventory.new(
      {
        inventory_type: inventory_type,
        person: person,
        product_item_id: product_item.id,
        purchase: purchase,
        quantity: 1,
        title: title,
        unlimited: false,
      }
    )
  end

  def build_child_inventories(parent_inventory, purchase)
    CHILD_INVENTORY_TYPES.map do |inventory_type|
      Inventory.new(
        {
          inventory_type: inventory_type,
          person: person,
          product_item_id: parent_inventory.product_item_id,
          purchase: purchase,
          title: inventory_type,
          unlimited: get_unlimited(inventory_type, parent_inventory),
        }
      )
    end
  end

  def get_product_item(release_type)
    product_id = Product.localized_one_year_release_product_id_by_type(person, release_type)

    ProductItem.find_by(product_id: product_id)
  end

  def get_unlimited(inventory_type, parent_inventory)
    if inventory_type == Salepoint.name || parent_inventory.inventory_type == Album.name
      true
    else
      false # inventory_type is 'Song' for Single or Ringtone
    end
  end

  def user_has_plan?
    return true if person.has_plan?

    purchases.any?(&:is_plan?)
  end
end
