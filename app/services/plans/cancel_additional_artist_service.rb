# frozen_string_literal: true

class Plans::CancelAdditionalArtistService
  attr_reader :user, :purchases

  def self.cancel_unrenewed_artists(person, purchases)
    new(person, purchases).cancel_unrenewed_artists
  end

  def initialize(user, purchases)
    @user = user
    @purchases = purchases
  end

  def cancel_unrenewed_artists
    purchases_artist_ids = purchases.active_additional_artists.pluck(:related_id)
    active_addon_artists = user.additional_artists.active.paid

    return false if active_addon_artists.length <= purchases_artist_ids.length

    cancel_extra_plan_addon_artists(active_addon_artists, purchases_artist_ids)
    true
  end

  private

  def cancel_extra_plan_addon_artists(active_addon_artists, purchases_artist_ids)
    active_addon_artists.where.not(id: purchases_artist_ids).each do |addon_artist|
      addon_artist.update(canceled_at: Date.current)
    end
  end
end
