class Plans::DowngradeRequestCreationService
  include AfterCommitEverywhere

  attr_reader :person_id, :downgrade_other_reason, :reason_id, :status, :requested_plan_id

  def self.call(params)
    new(params).call
  end

  def initialize(params)
    # error handling in PlanDowngradeRequest model
    @person_id = params[:person_id]
    @reason_id = params[:reason_id]
    @status = params[:status] || PlanDowngradeRequest::ACTIVE_STATUS
    @downgrade_other_reason = params[:downgrade_other_reason]
    @requested_plan_id = params[:requested_plan_id]
  end

  def call
    in_transaction do
      plan_downgrade_request = PlanDowngradeRequest.new(
        person_id: person_id,
        status: status,
        reason_id: reason_id,
        requested_plan_id: requested_plan_id
      )

      if reason_id == other_reason_id
        plan_downgrade_request.build_downgrade_other_reason(description: downgrade_other_reason)
      end
      # find all existing plan downgrade requests for the user and mark them as Inactive
      if plan_downgrade_request.status == PlanDowngradeRequest::ACTIVE_STATUS
        PlanDowngradeRequest.where(
          person_id: person_id,
          status: PlanDowngradeRequest::ACTIVE_STATUS
        ).update_all(status: PlanDowngradeRequest::CANCELED_STATUS) # rubocop:disable Rails/SkipsModelValidations
      end

      # always return the plan_downgrade_request, regardless of whether save is
      # successful - error handling in caller
      plan_downgrade_request.tap(&:save)
    end
  end

  def other_reason_id
    @@other_reason_id ||= DowngradeCategoryReason.find_by(name: "Other")&.id
  end
end
