# frozen_string_literal: true

# Detect if a person has enough Additional Artists
#
# Can be extended to flesh-out "artist seats" count, such as counting
# if a user has too many additional artists.
class Plans::ArtistAuditService
  def initialize(person)
    @person = person
  end

  attr_reader :person

  # The person's *carted and purchased* releases require more artists than they have purchased
  def current_required_artist_count
    @current_required_artist_count ||=
      begin
        album_ids = Plans::RequirementsHelperService.new(person, :combined).scoped_releases.pluck(:id)

        Creative
          .distinct
          .joins("inner join albums on creatives.creativeable_type = 'Album'" \
                 " and creatives.creativeable_id = albums.id")
          .joins(:artist)
          .where(albums: { id: album_ids },
                 creatives: { role: Creative::PRIMARY_ARTIST_ROLE })
          .pluck("artists.name")
          .count
      end
  end
end
