# frozen_string_literal: true

class Plans::RequiredStoresService
  def self.call(person, scope)
    new(person, scope).call
  end

  def initialize(person, scope)
    @person = person
    @scope  = scope
  end

  attr_reader :person, :scope

  def call
    required_store_ids
  end

  private

  def required_store_ids
    @required_store_ids ||=
      case scope
      when :cart
        cart_stores
      when :combined
        (live_stores + cart_stores).uniq
      when :takedown_related
        live_stores_for_takedown
      end
  end

  def base_query
    Store.distinct
         .exclude_believe
         .joins(salepoints: :album)
         .where(albums: {
                  takedown_at: nil,
                  is_deleted: false,
                  person_id: person.id
                },
                salepoints: { takedown_at: nil })
  end

  def exclude_inactive_stores
    base_query.is_active
  end

  def live_stores
    exclude_inactive_stores.where(live_condition).pluck(:id)
  end

  def live_stores_for_takedown
    # when this service is used for plan downgrades logic, we want to include inactive stores
    # to make sure associated sale_points for those stores are also removed
    base_query.where(live_condition).pluck(:id)
  end

  def cart_stores
    # joins taken from Album#in_cart scope
    exclude_inactive_stores
      .joins("LEFT OUTER JOIN purchases p1 ON albums.id = p1.related_id AND p1.related_type = 'Album'")
      .joins(
        "LEFT OUTER JOIN credit_usages ON albums.id = credit_usages.related_id " \
        "AND credit_usages.related_type = 'Album'"
      )
      .joins("LEFT OUTER JOIN purchases p2 ON credit_usages.id = p2.related_id AND p2.related_type = 'CreditUsage'")
      .where("p1.id IS NOT NULL OR p2.id IS NOT NULL")
      .where("p1.paid_at IS NULL OR p1.paid_at = ''")
      .where("p2.paid_at IS NULL OR p2.paid_at = ''")
      .pluck(:id)
  end

  def live_condition
    { albums: { legal_review_state: Album::OPTIMISTIC_APPROVAL_STATUSES } }
  end
end
