# frozen_string_literal: true

class Plans::ForcedDowngradeService
  class NoPlanError < StandardError; end

  attr_reader :person, :target_plan, :options

  def self.call(person)
    new(person).call
  end

  def initialize(person)
    raise NoPlanError unless person.has_plan?

    @person = person
    @target_plan = Plan.find(Plan::NEW_ARTIST)
    @options = {
      purchase: downgrade_purchase,
      change_type: PersonPlanHistory::FORCED_DOWNGRADE,
      note_opts: note_opts
    }
  end

  def call
    Plans::DowngradeService.call(person, target_plan, options)
    Plans::ForcedDowngradeCartRemovalService.call(person)
  end

  def downgrade_purchase
    @downgrade_purchase ||=
      ApplicationRecord.transaction do
        plan_product = target_plan.products.find_by(country_website_id: person.country_website_id)
        purchase = Product.add_to_cart(person, plan_product, plan_product)
        invoice = purchase.invoice.presence || Invoice.factory(person, purchase)
        invoice.settled! unless invoice.settled?
        purchase.reload
      end
  end

  def note_opts
    @note_opts ||= {
      user_id: 0,
      subject: "Forced Plan Downgrade",
      note: "person_id: #{person.id} downgrading from plan_id: #{person.plan.id} to plan_id: #{target_plan.id}"
    }
  end
end
