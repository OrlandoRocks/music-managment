# frozen_string_literal: true

class Plans::RequirementsHelperService
  def initialize(person, scope)
    @person = person
    @scope  = scope
  end

  attr_reader :person, :scope

  def scoped_releases
    @scoped_releases ||=
      begin
        result = Purchase.releases_in_cart(person)
        result += person.albums.paid.not_taken_down.approved_or_pending_approval if combined_scope?

        result
      end
  end

  private

  def combined_scope?
    scope == :combined
  end
end
