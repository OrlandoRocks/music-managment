# frozen_string_literal: true

# Features required by carted releases
class Plans::RequiredFeaturesService
  class NotImplementedError < StandardError; end

  # Requirements gathering needs to check releases
  RELEASE_FEATURES = %i[
    custom_label
    recording_location
    scheduled_release
    territory_restrictions
    upc
  ].freeze

  def initialize(person, context = {})
    @person       = person
    @context      = context
    @requirements = []
  end

  attr_reader :context, :person, :requirements

  def call
    set_required_person_features
    set_required_release_features

    required_features
  end

  private

  def required_features
    @required_features ||= requirements
  end

  # Person Features

  def set_required_person_features
    requirements.push(:buy_additional_artists) if additional_artists?
    requirements.push(:store_automator)        if automator?
  end

  def additional_artists?
    return precalculated_additional_artists? if requires_additional_artists_precalculated?

    1 < artist_audit_service.current_required_artist_count
  end

  def automator?
    person.has_automator? || person.purchases.unpaid.automator.any?
  end

  # Release Features

  # Check releases only until all features are required
  def set_required_release_features
    return unless FeatureFlipper.show_feature?(:plans_pricing, person)

    scoped_releases.each do |release|
      break if (RELEASE_FEATURES - requirements).empty?

      add_requirements_for_release(release)
    end
  end

  def custom_label?(release)
    release.label_name.strip != release.artist_name.strip
  end

  def recording_location?(release)
    release.recording_location.present?
  end

  def scheduled_release?(release)
    Date.today < release.sale_date
  end

  def territory_restrictions?(release)
    release.album_countries.any?
  end

  def upc?(release)
    release.upcs.any? { |upc| upc.upc_type != Upc::TUNECORE_TYPE }
  end

  # Helpers

  def precalculated_additional_artists?
    context[:requires_additional_artists]
  end

  def requires_additional_artists_precalculated?
    context[:requires_additional_artists].present?
  end

  # Scope is hard-coded to the cart for these feature requirement checks.
  def scoped_releases
    @scoped_releases ||=
      Plans::RequirementsHelperService
      .new(person, :cart)
      .scoped_releases
      .includes(:album_countries, :upcs)
  end

  def artist_audit_service
    @artist_audit_service ||= Plans::ArtistAuditService.new(person)
  end

  def add_requirements_for_release(release)
    requirements.push(:custom_label)       if custom_label?(release)
    requirements.push(:recording_location) if recording_location?(release)
    requirements.push(:scheduled_release)  if scheduled_release?(release)
    requirements.push(:territory_restrictions) if territory_restrictions?(release)
    requirements.push(:upc) if upc?(release)
  end
end
