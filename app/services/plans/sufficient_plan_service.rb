# frozen_string_literal: true

# Determine if a plan is sufficient for a user's needs, for a given scope, where:
# needs include: stores, additional artists and features.
# and
# scope is either: :cart, :combined.
#
# The :combined scope is used for plan upgrade/downgrade eligibility checks.
# The :cart scope is used at checkout.
#
# Additional Artist checks are never scoped in the context of plan eligibility,
# because we must always check all purchased and carted releases.
class Plans::SufficientPlanService
  def self.call(person, plan, scope)
    new(person, plan, scope).call
  end

  def initialize(person, plan, scope)
    @person = person
    @plan   = plan
    @scope  = scope
  end

  attr_reader :person, :plan, :scope

  def call
    return false if plan.nil?

    sufficient_stores? &&
      sufficient_artists? &&
      sufficient_features?
  end

  def sufficient_stores?
    @sufficient_stores ||=
      required_stores_service.call.all? { |req_store_id| plan_store_ids.key?(req_store_id) }
  end

  def sufficient_artists?
    @sufficient_artists ||= Plans::CanDoService.can_do?(plan, :buy_additional_artists) ||
                            requires_additional_artists? == false
  end

  def sufficient_features?
    @sufficient_features ||=
      required_features_service.call.all? { |f| Plans::CanDoService.can_do?(plan, f) }
  end

  private

  def plan_store_ids
    @plan_store_ids ||= plan.plans_stores.pluck(:store_id).index_with { |_id| true }
  end

  # Services

  def required_stores_service
    @required_stores_service ||= Plans::RequiredStoresService.new(person, scope)
  end

  def required_features_service
    @required_features_service ||=
      Plans::RequiredFeaturesService.new(
        person, {
          requires_additional_artists: requires_additional_artists?
        }
      )
  end

  def requires_additional_artists?
    @requires_additional_artists ||=
      1 < Plans::ArtistAuditService.new(person).current_required_artist_count
  end
end
