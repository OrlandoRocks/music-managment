# frozen_string_literal: true

class Plans::DspTakedownService
  include AfterCommitEverywhere

  class NoUserError < StandardError; end

  class InvalidStoreIdsError < StandardError; end

  attr_reader :user, :stores_to_remove

  def self.call(user, stores_to_remove)
    new(user, stores_to_remove).call
  end

  def initialize(user, stores_to_remove)
    raise NoUserError unless user.present? && user.is_a?(Person)

    @user = user

    @stores_to_remove = stores_to_remove.nil? ? [] : stores_to_remove
    raise InvalidStoreIdsError unless @stores_to_remove.is_a?(Array)
  end

  def call
    in_transaction do
      user.salepoints.where(store_id: stores_to_remove).each do |sp|
        sp.takedown!(false)
        sp.send_sns_notification_takedown(user)
      end
    end
  end
end
