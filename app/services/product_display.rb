class ProductDisplay
  attr_reader :person, :product, :targeted_product

  def initialize(person, product)
    @person             = person
    @product            = product
    @targeted_product   = TargetedProduct.targeted_product_for_active_offer(person, product)
  end

  def to_hash
    {
      full_price: money_string(full_price),
      discount_price: money_string(discount_price),
      show_discount_price: show_discount_price?,
      flag_text: flag_text,
      price_adj_description: price_adj_description
    }
  end

  def display_price
    price = (show_discount_price? ? discount_price : full_price)
    (price / 100.0).to_money(product.currency)
  end

  def discount_price
    (full_price - discount_amount) if targeted_product
  end

  def full_price
    @full_price ||= product.price.to_money.fractional
  end

  def show_discount_price?
    targeted_product.try(:show_discount_price?) || false
  end

  def flag_text
    targeted_product.try(:flag_text)
  end

  def price_adj_description
    targeted_product.try(:price_adjustment_description)
  end

  private

  def discount_amount
    targeted_product.try(:discount_amount_cents, full_price)
  end

  def money_string(price_in_cents)
    dollar_amt = (price_in_cents / 100.0)
    sprintf("%.2f", dollar_amt).to_s
  end
end
