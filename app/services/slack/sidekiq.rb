class Slack::Sidekiq < Slack::Client
  def self.notify(_exception, context_hash)
    new(context_hash).notify
  end

  attr_reader :job_id, :job

  def initialize(context_hash)
    @job_id = context_hash["jid"]
    @job    = job_data.item.with_indifferent_access
  end

  def notify
    super unless job_retriable?
  end

  def channel
    ENV["SLACK_SIDEKIQ_CHANNEL_URL"]
  end

  def body
    {
      attachments: [
        {
          pretext: "Failed Job: *#{job[:class]}*",
          author_name: filepath_error_msg,
          title: "View Full Backtrace",
          title_link: sidekiq_web_url,
          fields: fields,
          color: "#ff4d4d",
          text: "",
          mrkdwn_in: %w[pretext fields],
          ts: job[:failed_at].to_s
        }
      ]
    }
  end

  private

  def job_data
    @job_data ||= Sidekiq::DeadSet.new.find_job(job_id)
  end

  def job_retriable?
    job[:retry_count] < 25 && job[:retry] != job[:retry_count]
  end

  def filepath_error_msg
    return unless job[:error_backtrace]

    file, method = job[:error_backtrace].first.split("tc-www/").last.split(" ")
    "Error occured in \"#{method.match(/[a-z]+/)}\" in #{file.chomp(':in')}"
  end

  def sidekiq_web_url
    "#{ENV['SIDEKIQ_WEB_BASE_URL']}/#{job_data.score}-#{job_id}"
  end

  def fields
    [
      {
        title: "#{job[:error_class]}:",
        value: job[:error_message].tr("'", "`").to_s,
        short: false
      },
      {
        title: "Arguments:",
        value: "`#{job[:args]}`",
        short: false
      }
    ]
  end
end
