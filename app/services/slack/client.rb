class Slack::Client
  def notify
    Faraday.new(slack_api_url).post do |req|
      req.url channel
      req.headers["Content-type"] = "application/json"
      req.body = body.to_json
    end
  end

  def channel
    raise NotImplementedError, "Slack::Client subclasses must implement #channel."
  end

  def body
    raise NotImplementedError, "Slack::Client subclasses must implement #body."
  end

  private

  def slack_api_url
    ENV["SLACK_API_URL"]
  end
end
