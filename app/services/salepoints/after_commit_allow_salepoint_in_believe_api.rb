module Salepoints
  class AfterCommitAllowSalepointInBelieveApi
    def self.call(salepoint)
      new.send(:call, salepoint)
    end

    private

    def call(salepoint)
      return unless conditions_met?(salepoint)

      Delivery::Believe::ChangeStoreRestrictionWorker.perform_async(
        salepoint.salepointable.id,
        "allow",
        [salepoint.store.id]
      )
    end

    def conditions_met?(salepoint)
      [
        salepoint.salepointable.is_a?(Album),
        salepoint.salepointable.try(:legal_review_state) == "APPROVED",
        salepoint.finalized_at.present?,
        salepoint.saved_change_to_finalized_at?
      ].all?
    end
  end
end
