module AssetsBackfill
  class MigrateFlac
    attr_accessor :asset, :song, :destination_asset_key

    ALLOWED_FLAC_ENCODINGS = [
      {
        SamplingRate: "44100",
        BitDepth: "16",
        Channels: 2,
      }
    ].freeze

    SAMPLING_RATES = {
      s16: "16",
      s32: "24"
    }.freeze

    RINGTONE_DURATION = 30

    def initialize(options)
      options.each do |k, v|
        if respond_to?(k)
          public_send "#{k}=", v
        else
          puts "Missing accessor for #{k}"
        end
      end
    end

    def run
      return handle_valid_track if valid_track?

      MigrateNonFlac.new(
        {
          song: song,
          asset: asset,
          destination_asset_key: destination_asset_key
        }
      ).run
    end

    def ffprobe_metadata
      return @ffprobe_metadata if @ffprobe_metadata

      resp = HTTParty.post(ENV.fetch("FFPROBE_URI"), body: ffprobe_body, headers: ffprobe_headers)
      raise FFProbeError.new("Call to FFProbe Failed -- #{resp.body}") if resp.code != 200

      @ffprobe_metadata = JSON.parse resp.body

      @ffprobe_metadata
    end

    def ffprobe_body
      {
        source: {
          bucket: asset.bucket,
          key: asset.key
        }
      }.to_json
    end

    def ffprobe_headers
      {
        "Content-Type": "application/json"
      }
    end

    def handle_valid_track
      upload_to_destination_bucket

      {
        create_s3_flac_asset: !legacy_asset_valid_bucket?,
        duration: duration,
        key: destination_asset_key
      }
    end

    def valid_track?
      ALLOWED_FLAC_ENCODINGS.include?(
        {
          SamplingRate: sampling_rate,
          BitDepth: bit_depth,
          Channels: channels
        }
      ) && !process_ringtone?
    end

    def sampling_rate
      audio_stream.dig("sample_rate")
    end

    def bit_depth
      SAMPLING_RATES[audio_stream.dig("sample_fmt").to_sym]
    end

    def channels
      audio_stream.dig("channels")
    end

    def duration
      audio_stream.dig("duration").to_i
    end

    def audio_stream
      ffprobe_metadata.dig("streams")[0]
    end

    def upload_to_destination_bucket
      return if legacy_asset_valid_bucket?

      s3_client.copy_object(
        copy_source: "#{asset.bucket}/#{asset.key}",
        key: destination_asset_key,
        bucket: ENV["ASSETS_BACKFILL_DESTINATION_BUCKET"]
      )
    end

    def legacy_asset_valid_bucket?
      asset.bucket == ENV["ASSETS_BACKFILL_DESTINATION_BUCKET"]
    end

    def s3_client
      @s3_client ||= Aws::S3::Client.new
    end

    def process_ringtone?
      song.album.ringtone? && duration > RINGTONE_DURATION
    end
  end
end
