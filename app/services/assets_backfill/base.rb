module AssetsBackfill
  class Base
    attr_accessor :album_ids, :force

    def initialize(options = {})
      options.each do |k, v|
        if respond_to?(k)
          public_send "#{k}=", v
        else
          puts "Missing accessor for #{k}"
        end
      end
    end

    def run
      albums = find_by_album_ids || Album

      albums = albums.joins(:songs).where(songs: { s3_flac_asset: nil }) unless force?

      start_time = Time.now
      @number_of_songs_migrated = 0
      albums.includes(:songs).finalized.distinct.find_in_batches do |albums|
        albums.each do |album|
          Rails.logger.info "Started migration for album #{album.id}"
          album.songs.each do |song|
            Rails.logger.info "Started migration for song #{song.id}"
            migrate(song)
            Rails.logger.info "Completed migration for song #{song.id}"
            @number_of_songs_migrated += 1
            sleep 0.02
          end
          Rails.logger.info "Completed migration for album #{album.id}"
        end
      end
      time_taken = Time.now - start_time
      Rails.logger.info "Migrated #{@number_of_songs_migrated} songs in #{time_taken} seconds"
    end

    def find_by_album_ids
      Album.where(id: album_ids.uniq).distinct if album_ids.present?
    end

    def migrate(song)
      begin
        s3_asset = get_s3_asset(song)

        response = process(song, s3_asset) if s3_asset

        process_response(song: song, response: response, asset: s3_asset) if response
      rescue => e
        error_message = "Failed while migrating song #{song.id}"
        Rails.logger.error error_message
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.to_json
        Airbrake.notify(
          error_message,
          {
            error: e.message,
            backtrace: e.backtrace.to_json
          }
        )
      end
    end

    def get_s3_asset(song)
      return song.s3_flac_asset if s3_asset?(song.s3_flac_asset)
      return song.s3_orig_asset if s3_asset?(song.s3_orig_asset)
      return song.s3_asset if s3_asset?(song.s3_asset)
    end

    def process(song, s3_asset)
      return MigrateFlac.new(
        {
          song: song,
          asset: s3_asset,
          destination_asset_key: destination_asset_key(song)
        }
      ).run if flac?(s3_asset)

      MigrateNonFlac.new(
        {
          song: song,
          asset: s3_asset,
          destination_asset_key: destination_asset_key(song)
        }
      ).run
    end

    def process_response(song:, response:, asset: nil)
      asset = create_s3_flac_asset(response[:key]) if response[:create_s3_flac_asset]

      song.update!(
        {
          duration_in_seconds: response[:duration],
          s3_flac_asset: asset
        }
      )
    end

    def parse_transcoder_response(response)
      return unless response[:state] == "COMPLETED"

      output = response[:outputs][0]

      process_response(
        song: Song.find(response[:songId]),
        response: {
          create_s3_flac_asset: true,
          duration: output[:duration].to_i,
          key: output[:key]
        }
      )
    end

    def s3_asset?(asset)
      asset&.key && asset&.bucket
    end

    def flac?(asset)
      asset.key.ends_with? ".flac"
    end

    def force?
      !!ActiveModel::Type::Boolean.new.cast(force)
    end

    def destination_asset_key(song)
      "#{song.person.id}/#{Time.now.strftime('%s%3N')}-#{song.track_num}-#{song.id}-#{sanitized_name(song)}.flac"
    end

    def create_s3_flac_asset(key)
      S3Asset.create(
        {
          key: key,
          bucket: ENV["ASSETS_BACKFILL_DESTINATION_BUCKET"]
        }
      )
    end

    def sanitized_name(song)
      song.name[0..100].gsub(/(\/|\s|'|-|\(|\)|,|\\)/, "_")
    end
  end
end
