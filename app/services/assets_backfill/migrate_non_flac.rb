module AssetsBackfill
  class MigrateNonFlac
    attr_accessor :asset, :song, :destination_asset_key, :callback_url, :output_format

    DEFAULT_OUTPUT_FORMAT = {
      extension: "flac"
    }

    DEFAULT_CALLBACK_URL = "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_asset_data".freeze

    def initialize(options)
      options = {
        callback_url: DEFAULT_CALLBACK_URL,
        output_format: DEFAULT_OUTPUT_FORMAT
      }.merge(options)

      options.each do |k, v|
        if respond_to?(k)
          public_send "#{k}=", v
        else
          puts "Missing accessor for #{k}"
        end
      end
    end

    def run
      response = transcode_request
      Rails.logger.info response
      nil
    end

    def transcode_request
      HTTParty.post(ENV["TC_TRANSCODER_URL"], body: post_body, headers: post_headers)
    end

    def post_body
      body = {
        callbackURL: "#{callback_url}?songId=#{song.id}",
        source: {
          key: asset.key,
          bucket: asset.bucket
        },
        destination: {
          key: destination_asset_key,
          bucket: ENV["ASSETS_BACKFILL_DESTINATION_BUCKET"]
        },
        format: output_format,
      }
      body = body.merge(duration: AssetsBackfill::MigrateFlac::RINGTONE_DURATION) if song.album.ringtone?
      body.to_json
    end

    def post_headers
      {
        "Content-Type" => "application/json"
      }
    end
  end
end
