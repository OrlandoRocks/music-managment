module AssetsBackfill
  # Backfills missing song.s3_orig_asset_id when there is a matching asset, but doesn't
  # check if the asset actually exists. Assets will be verified in a subsequent task.
  #
  # A match is when the bucket is BIGBOX_BUCKET, and the key matches the original uploaded file name
  class S3OrigAsset
    BACKFILL_CSV_PATH = "/home/deploy/tmp/backfill_s3_orig_asset_id.csv"
    BIGBOX_BUCKET = "bigbox.tunecore.com".freeze
    FIX_CSV_PATH = "/home/deploy/tmp/fix_s3_orig_asset_id.csv"
    HEADERS = %w[
      song_id
      orig_asset_id
      asset_id
      flac_asset_id
      uploaded_filename
      bucket
      key
      message
    ].freeze
    SDB_DOMAIN = "bigbox_assets_production".freeze

    attr_accessor :song
    attr_reader :key, :overwrite, :path, :sdb

    # If missing s3_orig_asset_id and s3_asset_id, use s3_flac_asset_id if it matches,
    # else use s3_asset_id if it matches,
    # else create a new s3_asset record, which may or may not exist.
    def backfill_missing(overwrite)
      @sdb = AWS::SimpleDB.new(AwsWrapper::Sdb::CONFIG)
      @path = BACKFILL_CSV_PATH

      Song
        .joins(:upload, :person)
        .where(s3_orig_asset_id: nil)
        .find_in_batches do |batch|
          batch.each do |song|
            @song      = song
            @key       = asset_key
            @overwrite = overwrite

            if missing_s3_asset? && flac_asset?
              flac_asset = find_asset("s3_flac_asset_id")
              next unless flac_asset

              unless valid_asset?(flac_asset)
                log_event(flac_asset, "flac_asset bad bucket")
                next
              end
              log_event(flac_asset, "flac_asset available")
              update_song(flac_asset)
            elsif !missing_s3_asset?
              s3_asset = find_asset("s3_asset_id")
              next unless s3_asset

              if match?(s3_asset)
                log_event(s3_asset, "s3_asset available")
                update_song(s3_asset)
                next
              end

              log_event(s3_asset, "s3_asset_id does not match upload")

              next unless asset_lookup_by_key_successful?

              new_asset_record = create_asset_record
              log_event(new_asset_record, "created new asset record from matching upload")
              update_song(new_asset_record)
            else
              log_event(nil, "song does not have any associated asset")
            end
          end
        end
    end

    def fix_originals(overwrite)
      @path = FIX_CSV_PATH
      Song
        .joins(:upload, :person)
        .where.not(s3_orig_asset_id: nil)
        .where.not(s3_asset_id: nil)
        .find_in_batches do |batch|
          batch.each do |song|
            @song      = song
            @key       = asset_key
            @overwrite = overwrite

            orig_asset = find_asset("s3_orig_asset_id")
            next if orig_asset.nil? || match?(orig_asset)

            log_event(orig_asset, "s3_orig_asset does not match upload")

            s3_asset = find_asset("s3_asset_id")
            next if s3_asset.nil?

            unless match?(s3_asset)
              log_event(s3_asset, "s3_orig_asset and s3_asset don't match upload")
              next
            end

            log_event(s3_asset, "s3_asset_id should overwrite s3_orig_asset_id")
            update_song(s3_asset)
          end
        end
    end

    private

    def log_event(asset, message)
      CSV.open(path, "w").add_row(HEADERS).close unless File.exist?(path)

      CSV.open(path, "a")
         .add_row([
                    song.id,
                    song.s3_orig_asset_id,
                    song.s3_asset_id,
                    song.s3_flac_asset_id,
                    song.upload.uploaded_filename,
                    asset&.bucket,
                    asset&.key,
                    message
                  ])
         .close
    end

    def find_asset(field)
      asset = S3Asset.find_by(id: song.send(field))
      log_event(nil, "bad #{field}") if asset.blank?
      asset
    end

    def asset_lookup_by_key_successful?
      S3_CLIENT.buckets[BIGBOX_BUCKET].objects[key].exists?
    end

    def create_asset_record
      S3Asset.create(
        {
          bucket: BIGBOX_BUCKET,
          key: key
        }
      )
    end

    def asset_key
      "#{song.person.id}/#{song.upload.uploaded_filename}"
    end

    def missing_s3_asset?
      song.s3_asset_id.nil?
    end

    def flac_asset?
      song.s3_flac_asset_id.present?
    end

    def update_song(asset)
      return unless overwrite

      song.update(s3_orig_asset_id: asset.id)
      log_event(asset, "updated song record")
    end

    def matching_asset?(asset)
      asset&.key == key
    end

    def valid_asset?(asset)
      asset&.bucket == BIGBOX_BUCKET
    end

    def match?(asset)
      matching_asset?(asset) && valid_asset?(asset)
    end
  end
end
