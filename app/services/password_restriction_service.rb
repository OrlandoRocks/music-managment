class PasswordRestrictionService
  def self.restricted?(password)
    return false unless TcWww::Application.config.enable_password_restrictions

    $redis.sismember("restricted_passwords", password)
  rescue => e
    Tunecore::Airbrake.notify(e)
    false
  end
end
