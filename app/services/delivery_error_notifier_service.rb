class DeliveryErrorNotifierService
  DAYS_IN_PAST_STANDARD = 3.days.freeze
  DAYS_IN_PAST_VIP      = 2.days.freeze
  EXTRACT_DURATION_DAYS = 1.day.freeze

  def self.notify(from_date = nil, to_date = nil, excluded_store_ids = [])
    new(from_date, to_date, excluded_store_ids).notify
  end

  attr_reader :excluded_store_ids,
              :from_date_standard,
              :to_date_standard,
              :from_date_vip,
              :to_date_vip

  def initialize(from_date = nil, to_date = nil, excluded_store_ids = [])
    @from_date_standard = from_date.nil? ? (Date.current - DAYS_IN_PAST_STANDARD) : from_date.to_date
    @from_date_vip      = to_date.nil?   ? (Date.current - DAYS_IN_PAST_VIP)      : from_date.to_date
    @to_date_standard   = @from_date_standard + EXTRACT_DURATION_DAYS
    @to_date_vip        = @from_date_vip      + EXTRACT_DURATION_DAYS
    @excluded_store_ids = Array(excluded_store_ids)
  end

  def notify
    notify_albums_standard
    notify_albums_vip
  end

  def notify_albums_standard
    albums = album_query
             .where(distributions[:updated_at].gteq(from_date_standard))
             .where(distributions[:updated_at].lt(to_date_standard))
             .where(people[:vip].eq(false))

    send_delivery_error_summary(albums: albums, from_date: from_date_standard)
  end

  def notify_albums_vip
    albums = album_query
             .where(distributions[:updated_at].gteq(from_date_vip))
             .where(distributions[:updated_at].lt(to_date_vip))
             .where(people[:vip].eq(true))

    send_delivery_error_summary(albums: albums, from_date: from_date_vip)
  end

  def send_delivery_error_summary(albums:, from_date:)
    Rails.logger.info("Found #{albums.length} errored albums")
    albums.each do |album|
      Rails.logger.info("Sending email to Zendesk for UPC #{album.upc}")
      DeliveryErrorSummaryMailer.delivery_errors_email(album, album.upc.number, from_date).deliver
    end
  end

  def album_query
    Album
      .select(album_selects)
      .joins(:person, :petri_bundles, { salepoints: :store })
      .joins("INNER JOIN distributions USE INDEX (index_distributions_on_updated_at) ON distributions.petri_bundle_id = petri_bundles.id")
      .joins("INNER JOIN upcs ON upcs.upcable_id = albums.id AND upcs.upcable_type = 'Album'")
      .where(distributions[:state].eq("error"))
      .where(distributions[:converter_class].not_eq("MusicStores::Believe::Converter"))
      .where(salepoints[:store_id].not_eq(76))
      .where(salepoints[:store_id].not_in(excluded_store_ids))
      .where(stores[:in_use_flag].eq(true))
      .where(takedown_at: nil)
      .group(:id)
  end

  def album_selects
    [
      "albums.*",
      "upcs.number as upc",
      "people.name as person_name",
      "people.email",
      "group_concat(distinct(distributions.id)) as distros"
    ].join(", ")
  end

  def distributions
    Distribution.arel_table
  end

  def salepoints
    Salepoint.arel_table
  end

  def stores
    Store.arel_table
  end

  def albums
    Album.arel_table
  end

  def petri_bundles
    PetriBundle.arel_table
  end

  def people
    Person.arel_table
  end
end
