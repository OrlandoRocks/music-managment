class LocaleUrls
  COUNTRIES = GLOBAL_URLS_CONFIG.keys

  def self.build_locale_urls(hostname)
    COUNTRIES.each_with_object({}) do |url_value, url_acc|
      url_acc[url_value] = "#{hostname}#{GLOBAL_URLS_CONFIG[url_value]}".freeze
    end
  end
end
