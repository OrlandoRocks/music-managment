class SalepointStoreCompilerService
  REJECTED_STORES = [
    "us",
    "au",
    "ca",
    "eu",
    "jp",
    "uk",
    "mx",
    "la",
    "us",
    "pa",
    "aod_us",
    "ytm",
  ].freeze

  attr_accessor :person, :album

  def initialize(args = {})
    @person = args[:person]
    @album = args[:album]
  end

  def serialized_digital_stores_json
    serialized_digital_stores.to_json(root: false)
  end

  def serialized_digital_stores
    ActiveModel::ArraySerializer.new(
      sorted_digital_stores,
      each_serializer: Api::Backstage::AlbumApp::StoreSerializer,
      context: { salepoint_store_ids: salepoint_store_ids },
    )
  end

  def serialized_freemium_stores_json
    serialized_freemium_stores.to_json(root: false)
  end

  def serialized_freemium_stores
    ActiveModel::ArraySerializer.new(
      freemium_stores,
      each_serializer: Api::Backstage::AlbumApp::StoreSerializer,
      context: { salepoint_store_ids: salepoint_store_ids },
    )
  end

  def digital_stores
    @digital_stores ||= all_available_stores.exclude_discovery(person).where.not(abbrev: REJECTED_STORES)
  end

  def freemium_stores
    @freemium_stores ||= Store.discovery_platforms(person)
  end

  private

  def sorted_digital_stores
    # Needed because some stores are (vexingly) missing :position values
    @sorted_digital_stores ||= digital_stores.sort_by(&:position)
  end

  def all_available_stores
    @all_available_stores ||= Store.where(
      id: (album.selected_stores(person.id).pluck(:id) | album.available_stores(person.id).pluck(:id))
    )
  end

  def salepoint_store_ids
    album.salepoints.pluck(:store_id).uniq
  end
end
