# frozen_string_literal: true

class Payu::WebhookValidatorService
  # Confirm that the provided hash is authentic by regenerating it and comparing
  def self.valid?(params)
    new(params).valid?
  end

  def self.invalid?(params)
    new(params).valid? == false
  end

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def valid?
    return false if params[:hash].blank?

    ActiveSupport::SecurityUtils.secure_compare(generated_hash, params[:hash])
  end

  private

  def generated_hash
    Digest::SHA512.hexdigest(payu_hash_parameters)
  end

  # The reverse of app/api_clients/payu/requests/new_payment.rb,
  # with the addition of the status param.
  # https://devguide.payu.in/merchant-integration/encryption-of-request/
  def payu_hash_parameters
    status, email, first_name, product_info, amount, txn_id, key =
      params.values_at(:status, :email, :firstname, :productinfo, :amount, :txnid, :key)

    "#{salt}|#{status}|||||||||||#{email}|#{first_name}|#{product_info}|#{amount}|#{txn_id}|#{key}"
  end

  def salt
    Payu::ApiClient::MERCHANT_SALT
  end
end
