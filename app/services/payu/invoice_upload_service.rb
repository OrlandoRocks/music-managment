# frozen_string_literal: true

class Payu::InvoiceUploadService
  attr_reader :invoice, :payu_transaction

  def initialize(invoice)
    @invoice = invoice
    @payu_transaction = invoice_payu_transaction
  end

  def api_client
    @api_client ||= Payu::ApiClient.new
  end

  def upload
    invoice_pdf_filename = "invoice_#{invoice.id}_#{Time.zone.now.to_i}"
    invoice_pdf = Tempfile.new([invoice_pdf_filename, ".pdf"], binmode: true)
    invoice_pdf.write(Invoice::PdfGenerationService.generate(invoice.id))

    response = api_client.send_request(
      :new_invoice,
      invoice: invoice,
      invoice_file: Faraday::UploadIO.new(
        invoice_pdf.path,
        "application/pdf",
        "#{invoice_pdf_filename}.pdf"
      )
    )

    if response.body.present?
      parsed_response = JSON.parse(response.body)
      PayuInvoiceUploadResponse
        .create!(
          related: payu_transaction,
          api_response_code: parsed_response["responseCode"],
          api_message: parsed_response["responseMsg"]
        )
    end

    invoice_pdf.unlink
    response
  end

  def invoice_payu_transaction
    invoice.payu_transactions.find_by(status: true) ||
      invoice.payu_transactions.order(created_at: :desc).first
  end
end
