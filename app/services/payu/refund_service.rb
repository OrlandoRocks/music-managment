module Payu
  class RefundService
    include PayuError

    attr_reader :payu_transaction

    def initialize(refund, amount_cents, original_transaction)
      @refund = refund
      @amount_cents = amount_cents
      @original_transaction = original_transaction
    end

    def process!
      initiate_refund!
      check_refund_status!
      payu_transaction
    end

    private

    attr_reader :refund, :amount_cents, :original_transaction

    def initiate_refund!
      payu_response = send_refund_request
      log_response_to_s3(payu_response)
      create_refund_transaction(payu_response)
      handle_failure!(payu_response) unless payu_response.success?
    end

    def check_refund_status!
      payu_response = send_refund_status_request
      log_response_to_s3(payu_response)
      handle_failure! if payu_response.failure?(request_id)
      update_payu_transaction(payu_response)
    end

    def create_refund_transaction(payu_response)
      @payu_transaction = PayuTransaction.create!(
        invoice: refund.invoice,
        person: refund.person,
        amount: amount_in_local_currency,
        action: PayuTransaction::REFUND,
        error_code: payu_response.error_code,
        payu_id: payu_response.payu_id,
        refund_request_id: payu_response.request_id,
        original_transaction: original_transaction
      )
    end

    def handle_failure!(response = nil)
      payu_transaction.failed!
      raise PayuRefundTransactionError.new(response&.error_code, response&.msg)
    end

    def api_client
      @api_client ||= Payu::ApiClient.new
    end

    def request_id
      @request_id ||= payu_transaction&.refund_request_id
    end

    def send_refund_request
      api_client.send_request(
        :refund_transaction,
        refund_id: refund.id,
        amount: amount_in_local_currency
      )
    end

    def send_refund_status_request
      api_client.send_request(
        :verify_refund,
        request_id: payu_transaction.refund_request_id,
      )
    end

    def amount_in_local_currency
      @amount_in_local_currency ||=
        if refund.pegged_rate.present?
          (amount_cents * refund.pegged_rate).fdiv(100)
        else
          amount_cents.fdiv(100)
        end
    end

    def log_response_to_s3(payu_response)
      PayuRefundTransactionLogWorker.new.async.write_to_bucket(refund.id, payu_response)
    end

    def update_payu_transaction(payu_response)
      payu_transaction.update!(
        payment_method: payu_response.mode(request_id),
        status: true
      )
    end
  end
end
