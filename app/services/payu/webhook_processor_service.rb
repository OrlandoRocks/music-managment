# frozen_string_literal: true

class Payu::WebhookProcessorService
  SUCCESS = "success"

  def self.call(params)
    new(params).call
  end

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def call
    log_request

    log_error

    handle_payment
  end

  private

  # Helpers

  def log_request
    PayuWebhookLogWorker.perform_async(params[:txnid], params)
  end

  def log_error
    return if transaction.present?

    Tunecore::Airbrake.notify(self.class.name, { txnid: params[:txnid] })
  end

  def handle_payment
    return if transaction.nil? || transaction.success?

    transaction.update(
      error_code: params[:error],
      payment_method: params[:PG_TYPE],
      payu_id: params[:mihpayid],
      status: params[:status] == SUCCESS
    )
  end

  def transaction
    @transaction ||= PayuTransaction.find_by(invoice_id: params[:txnid])
  end
end
