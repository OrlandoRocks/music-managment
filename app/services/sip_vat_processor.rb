class SipVatProcessor < TcVat::Base
  include ActiveModel::Validations

  attr_reader :status, :message, :person

  validates_presence_of :person, message: "Invalid person_id"
  validate :vat_feature_flag
  validate :sales_masters_or_yt_records

  INDIVIDUAL = "individual".freeze
  BUSINESS   = "business".freeze

  def initialize(person_id, sales_record_masters, you_tube_royalty_records)
    @person = Person.find_by(id: person_id)
    @sales_record_masters = sales_record_masters
    @you_tube_royalty_records = you_tube_royalty_records
    @person_id = person_id # for Airbrake notification
    @tax_rate = 0
    @tax_info = {}
  end

  def process!
    validate!
    @status = :ok
    if tax_rate <= 0
      @message = "VAT is not applicable for the person"
    else
      process_intakes
      @message = "Successfully completed VAT posting"
    end
  rescue ActiveModel::ValidationError => e
    Tunecore::Airbrake.notify("SIP VAT Posting - Validation failure - Person ID #{@person_id}. Error: #{e}")
    @message = e.model.errors.values.flatten&.join(";")
    @status = :unprocessable_entity
  rescue StandardError => e
    Tunecore::Airbrake.notify("SIP VAT Posting - Internal failure - Person ID #{@person_id}. Error: #{e}")
    @message = e.message
    @status = :unprocessable_entity
  end

  private

  def vat_feature_flag
    return if FeatureFlipper.show_feature?(:vat_tax)

    errors.add(:base, "VAT Tax feature is currently disabled")
  end

  def sales_masters_or_yt_records
    return if @sales_record_masters.present? || @you_tube_royalty_records.present?

    errors.add(:base, "Either sales_record_master_ids or you_tube_royalty_record_ids needs a value")
  end

  def tax_rate
    @customer_type = validate_vat_info ? BUSINESS : INDIVIDUAL
    countries = vat_applicable_countries
    country_tax_rate =
      countries[@customer_type].find do |c|
        c["country"] == @person.country && c["tax_rate"].to_f.positive?
      end
    if country_tax_rate.present?
      @tax_rate = country_tax_rate["tax_rate"].to_f
      @tax_info[:place_of_supply] = country_tax_rate["place_of_supply"]
      @tax_info[:balance_rate] = balance_rate
    end
    @tax_rate
  end

  def balance_rate
    ForeignExchangeRate.latest_by_currency(
      source: @person.currency,
      target: CurrencyCodeType::EUR
    )
  end

  def validate_vat_info
    args = {
      vat_registration_number: @person.vat_information&.vat_registration_number,
      country: country_name
    }
    response = api_client.send_request!(:validate, args)
    @tax_info[:trader_name] = response.name
    @tax_info[:error_code] = response.error_message

    response.valid?
  end

  def process_intakes
    ActiveRecord::Base.transaction do
      process_person_intakes if @sales_record_masters.present?
      process_youtube_intakes if @you_tube_royalty_records.present?
    end
  end

  def create_vat_tax_adjustment_entry(intake, vat_amount)
    VatTaxAdjustment.find_or_initialize_by(
      person: @person,
      amount: vat_amount,
      tax_rate: @tax_rate,
      related: intake,
      vat_registration_number: @person.vat_information&.vat_registration_number,
      tax_type: "VAT",
      trader_name: @tax_info[:trader_name],
      place_of_supply: @tax_info[:place_of_supply],
      error_message: @tax_info[:error_code],
      customer_type: @customer_type,
      vat_amount_in_eur: vat_amount_in_eur(vat_amount),
      foreign_exchange_rate: @tax_info[:balance_rate]
    )
  end

  def vat_amount_in_eur(amt)
    amt * (@tax_info[:balance_rate]&.exchange_rate || 1)
  end

  def create_person_transaction_entry(vat_rax_adjustment, vat_amount)
    PersonTransaction.create!(
      person: @person,
      debit: 0,
      credit: vat_amount,
      target: vat_rax_adjustment,
      comment: "VAT Posted"
    )
  end

  def process_person_intakes
    person_intakes = PersonIntake.joins(:sales_record_masters)
                                 .where(sales_record_masters: { id: @sales_record_masters }, person_id: @person.id)

    raise "No PersonIntakes found for given sales_record_master_ids" if person_intakes.empty?

    person_intakes.distinct.find_each do |intake|
      vat_amount = intake.amount * @tax_rate / 100
      vat_rax_adjustment = create_vat_tax_adjustment_entry(intake, vat_amount)
      break unless vat_rax_adjustment.new_record?

      vat_rax_adjustment.save!
      create_person_transaction_entry(vat_rax_adjustment, vat_amount)
    end
  end

  def process_youtube_intakes
    you_tube_royalty_intakes = YouTubeRoyaltyIntake.joins(:you_tube_royalty_records)
                                                   .where(you_tube_royalty_records: {
                                                            id: @you_tube_royalty_records,
                                                            person_id: @person.id
                                                          })

    raise "No YouTubeRoyaltyIntakes found for given you_tube_royalty_record_ids" if you_tube_royalty_intakes.empty?

    you_tube_royalty_intakes.distinct.find_each do |yt_intake|
      vat_amount = yt_intake.amount * @tax_rate / 100
      vat_rax_adjustment = create_vat_tax_adjustment_entry(yt_intake, vat_amount)
      break unless vat_rax_adjustment.new_record?

      vat_rax_adjustment.save!
      create_person_transaction_entry(vat_rax_adjustment, vat_amount)
    end
  end
end
