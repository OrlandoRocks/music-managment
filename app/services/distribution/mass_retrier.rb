class Distribution::MassRetrier
  def self.mass_retry(params)
    new(params).mass_retry
  end

  attr_reader :person_id, :store_ids, :remote_ip, :metadata_only, :album_ids_or_upcs, :retry_in_batches, :use_sns_only

  def initialize(params = {})
    @person_id         = params[:person_id]
    @store_ids         = params[:store_ids]
    @remote_ip         = params[:remote_ip]
    @metadata_only     = params[:metadata_only]
    @album_ids_or_upcs = params[:album_ids_or_upcs].try(:join, ",")
    @retry_in_batches  = params[:retry_in_batches]
    @use_sns_only      = params[:use_sns_only]
  end

  def mass_retry
    Rails.logger.info("MASSRETRIES: Distribution::MassRetrier.mass_retry albums = #{albums.inspect}")
    process_distribution_retry_form if albums
  end

  private

  def albums
    @albums ||= BulkAlbumFinder.new(album_ids_or_upcs).execute
  end

  def process_distribution_retry_form
    albums.each do |album|
      distros, itunes_distros = album.retriable_distributions_by_stores(store_ids)
      distros -= itunes_distros
      distros << itunes_distros.last unless itunes_distros.size.zero?
      retriable_distributions = build_retriable_distributions(distros, album)

      Rails.logger.info("MASSRETRIES: Distribution::MassRetrier.process_distribution_retry_form retriable_distributions = #{retriable_distributions.inspect}")
      Rails.logger.info("MASSRETRIES: Distribution::MassRetrier.process_distribution_retry_form album = #{album.inspect}")

      DistributionRetryForm.new(
        retriable_distributions: retriable_distributions,
        ip_address: remote_ip,
        album: album
      ).save
    end unless use_sns_only?

    send_sns_notification if send_sns_notification?
  end

  def build_retriable_distributions(distributions, album)
    distributions.map do |distribution|
      next unless distribution

      delivery_type = metadata_only?(album, distribution) ? "metadata_only" : "full_delivery"

      Delivery::Retry.new(
        {
          delivery: distribution,
          priority: 0,
          person: person,
          delivery_type: delivery_type,
          mass_retry: 1
        }
      )
    end
  end

  def send_sns_notification
    delivery_type = metadata_only == "on" ? "metadata_only" : "full_delivery"
    Sns::Notifier.perform(
      topic_arn: ENV["SNS_RELEASE_RETRY_TOPIC"],
      album_ids_or_upcs: album_ids_or_upcs,
      store_ids: store_ids,
      delivery_type: delivery_type,
      person_id: person_id
    )
    Rails.logger.info("MASSRETRIES: SNS Notification sent for Albums: #{album_ids_or_upcs}")
  end

  def metadata_only?(album, distribution)
    metadata_only.present? ||
      album.takedown? ||
      distribution.salepoints.all.count(&:taken_down?) == distribution.salepoints.length
  end

  def send_sns_notification?
    use_sns_only? || !retry_in_batches?
  end

  def retry_in_batches?
    !!ActiveModel::Type::Boolean.new.cast(retry_in_batches)
  end

  def use_sns_only?
    !!ActiveModel::Type::Boolean.new.cast(use_sns_only)
  end

  def send_sns_notification?
    use_sns_only? || !retry_in_batches?
  end

  def retry_in_batches?
    !!ActiveModel::Type::Boolean.new.cast(retry_in_batches)
  end

  def use_sns_only?
    !!ActiveModel::Type::Boolean.new.cast(use_sns_only)
  end

  def person
    @person ||= Person.find(person_id)
  end
end
