class Distribution::StateUpdateService
  def self.update(distributable, params)
    new(distributable, params).tap(&:update)
  end

  attr_reader :distributable, :parent, :params

  def initialize(distributable, params)
    @distributable = distributable
    @parent        = distributable.distribution if has_distribution
    @params        = params
  end

  def update
    if params[:isrcs] && distributable.distribution_songs.any?
      update_distribution_songs
    elsif distributable.state != params[:state]
      create_transition(distributable)
      update_state(distributable)
      check_and_update_parent if parent
    end
  end

  def check_and_update_parent
    return unless all_distribution_songs_delivered?

    create_transition(parent)
    update_state(parent)
  end

  def update_distribution_songs
    distributable.distribution_songs.each do |distribution_song|
      if params[:isrcs].include?(distribution_song.salepoint_song.song.isrc)
        create_transition(distribution_song)
        update_state(distribution_song)
      end
    end

    return unless params[:state] != "delivered" || (params[:state] == "delivered" && all_distribution_songs_delivered?)

    create_transition(distributable)
    update_state(distributable)
  end

  def update_state(state_machine)
    state_machine.update(state: params[:state])
  end

  def create_transition(state_machine)
    state_machine.transitions.create(
      {
        actor: params[:actor] || "TC Distributor",
        message: params[:message],
        backtrace: params[:backtrace],
        old_state: distributable.state,
        new_state: params[:state],
      }
    )
  end

  def all_distribution_songs_delivered?
    distribution_songs = parent ? parent.reload.distribution_songs : distributable.distribution_songs
    distribution_songs.all?(&:delivered?)
  end

  def has_distribution
    distributable.is_a?(DistributionSong)
  end
end
