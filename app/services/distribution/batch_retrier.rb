class Distribution::BatchRetrier < Distribution::MassRetrier
  def self.retry_batches
    incomplete_batches = $redis.keys("distribution:batch-retry:*")

    incomplete_batches.each do |batch_key|
      batch = $redis.hgetall(batch_key).with_indifferent_access
      batch[:batch_key] = batch_key
      new(batch).retry_batch
    end
  end

  attr_reader :uuid, :store_ids, :current_index, :daily_retry_limit, :album_ids_or_upcs, :batch_key

  def initialize(params = {})
    super

    @uuid              = params[:uuid]
    @store_ids         = params[:store_ids].split(",")
    @current_index     = params[:current_index].to_i
    @daily_retry_limit = params[:daily_retry_limit].to_i
    @album_ids_or_upcs = $redis.lrange("distribution:batch-retry-albums:#{uuid}", current_index, last)
    @batch_key         = params[:batch_key]
  end

  def retry_batch
    if album_ids_or_upcs.present?
      increment_current_index!
      Distribution::MassRetryWorker.perform_async(as_json)
    else
      $redis.rename(batch_key, "distribution:batch-retry-complete:#{uuid}")
    end
  end

  private

  def last
    (current_index - 1) + daily_retry_limit
  end

  def increment_current_index!
    $redis.hincrby(batch_key, "current_index", daily_retry_limit)
  end
end
