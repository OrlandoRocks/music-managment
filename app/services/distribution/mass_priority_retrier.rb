class Distribution::MassPriorityRetrier < Distribution::MassRetrier
  attr_reader :person_id, :store_ids, :remote_ip, :metadata_only, :album_ids_or_upcs, :retry_in_batches, :use_sns_only

  def initialize(params = {})
    super(params)
  end

  def build_retriable_distributions(distributions, album)
    distributions.map do |distribution|
      next unless distribution

      delivery_type = metadata_only?(album, distribution) ? "metadata_only" : "full_delivery"

      Delivery::Retry.new(
        {
          delivery: distribution,
          priority: 1,
          person: person,
          delivery_type: delivery_type,
          mass_retry: 1
        }
      )
    end
  end
end
