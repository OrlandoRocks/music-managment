class Distribution::StalledRetryService
  def self.retry
    distributions = execute_query
    distribute(distributions)
  end

  def self.execute_query
    base_query = Distribution
                 .joins(:petri_bundle, { salepoints: :store })
                 .select("distributions.id")
    DistributionRetryQueryBuilder.new(base_query)
                                 .excluding_defunct_stores
                                 .in_stalled_state
                                 .query
  end

  def self.distribute(distributions)
    distributions.each do |distribution|
      distribution.start(actor: "Distribution::StalledRetryService")
    end
  end
end
