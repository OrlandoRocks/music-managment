# frozen_string_literal: true

class Distribution::TcDistributorBackfillService
  attr_reader :start_date, :end_date, :store_ids, :file_name, :report_type

  REPORTS = {
    date_range: :generate_backfill_data_by_date
  }

  def initialize(options)
    @start_date = options[:start_date]
    @end_date = options[:end_date]
    @store_ids = options[:store_ids]
    @file_name = options[:file_name]
    @report_type = options[:report_type]
  end

  def generate_backfill_data_by_date
    params = { start_date: start_date, end_date: end_date }
    release_data = fetch_release_data(params)
    release_hash = format_album_release_hash(release_data)
    release_ids = release_hash.keys
    releases_store_data = fetch_relese_store_data({ release_id: release_ids })
    format_report_data(releases_store_data, release_hash)
  end

  def create_backfill_report
    report_method = REPORTS[report_type]
    formatted_data = send(report_method.to_s)
    filepath = File.join(Rails.root, "tmp/", file_name)
    CSV.open(filepath, "w") do |file|
      formatted_data.each { |row| file << [row] }
    end
    RakeDatafileService.upload(filepath, overwrite: "y")
  end

  private

  def fetch_release_data(params)
    releases = DistributorAPI::Release.list(params)
    releases[0].http_body["releases"]
  end

  def fetch_relese_store_data(params)
    releases_stores = DistributorAPI::ReleasesStore.list(params)
    releases_stores[0].http_body["releases_stores"]
  end

  def format_album_release_hash(release_data)
    release_hash = {}
    release_data.each do |release|
      release_hash[release["id"]] = { album_id: release["album"]["id"], store_ids: [] }
    end
    #   {
    #   release_id => {:album_id=>4139242, :store_ids=>[]},
    #   release_id => {:album_id=>4139242, :store_ids=>[]}
    #   }
    release_hash.with_indifferent_access
  end

  def format_report_data(releases_store_data, release_hash)
    # {
    #   'object' => 'releases_store',
    #   'id' => '1000',
    #   'release_id' => '1',
    #   'store_id' => '27',
    #   'delivery_type' => 'full_delivery',
    #   'state' => 'delivered',
    #   'xml_url' => 'https://s3.amazonaws.com/859736171160/1.xml',
    #   'created_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00',
    #   'updated_at' => 'Sun, 22 Mar 2020 18:04:52 UTC +00:00'
    # }
    # add stores to releases

    releases_store_data.each do |release_store|
      store_id = release_store["store_id"]
      release_id = release_store["release_id"]
      release_hash[release_id]["store_ids"] << store_id
    end

    # create ablums store hash
    # { "album_id" => ["store_id", "store_id"]}

    album_stores_hash = {}
    release_hash.each_value do |row|
      if album_stores_hash[row[:album_id]]
        album_stores_hash[row[:album_id]] << row[:store_ids]
        album_stores_hash[row[:album_id]].flatten
      else
        album_stores_hash[row[:album_id]] = row[:store_ids]
      end
    end

    album_stores_hash.map do |album_id, store_ids|
      [album_id, store_ids.join(",")]
    end
  end
end
