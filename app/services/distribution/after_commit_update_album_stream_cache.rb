class Distribution::AfterCommitUpdateAlbumStreamCache
  def self.call(distribution)
    new(distribution).send(:call)
  end

  private

  def initialize(distribution)
    @distribution = distribution
  end

  def call
    return unless conditions_met?

    # TODO:
    # - verify that this logic is correct
    # - write a spec for it
    album.update(can_stream_cache: album_was_taken_down?)
  end

  def conditions_met?
    [
      @distribution.saved_change_to_state?,
      @distribution.state == "delivered",
      @distribution.converter_class == "MusicStores::Streaming::Converter"
    ].all?
  end

  def album
    @distribution.petri_bundle.album
  end

  def album_was_taken_down?
    album.takedown_at.present?
  end
end
