class Distribution::BatchRetryReportService
  def self.report(options = {})
    key = "distribution:batch-retry:*"
    key << "#{options[:search]}*" if options[:search]
    incomplete_batches = $redis.keys(key)
    incomplete_batches.map do |batch_key|
      Distribution::BatchRetryReportService.new(batch_key).tap(&:report)
    end
  end

  attr_reader :uuid,
              :stores,
              :current_index,
              :daily_retry_limit,
              :incomplete_albums,
              :metadata_only,
              :complete_albums,
              :job_name,
              :complete_batches,
              :size

  def initialize(batch_key)
    batch_data         = $redis.hgetall(batch_key).with_indifferent_access
    @uuid              = batch_data[:uuid]
    @metadata_only     = batch_data[:metadata_only].present?
    @stores            = Store.where(id: batch_data[:store_ids])
    @current_index     = batch_data[:current_index].to_i
    @daily_retry_limit = batch_data[:daily_retry_limit].to_i
    @job_name          = batch_data[:job_name]
  end

  def report
    @incomplete_albums ||= incomplete_albums_from_ids_or_upcs
    @complete_albums   ||= complete_albums_from_ids_or_upcs
    @size              ||= incomplete_albums.count + complete_albums.count
  end

  def incomplete_albums_from_ids_or_upcs
    $redis.lrange("distribution:batch-retry-albums:#{uuid}", current_index, batch_size)
  end

  def complete_albums_from_ids_or_upcs
    return [] if current_index.zero?

    $redis.lrange("distribution:batch-retry-albums:#{uuid}", 0, current_index - 1)
  end

  def batch_size
    $redis.llen("distribution:batch-retry-albums:#{uuid}")
  end

  def completion_date
    days_until_complete = 0 || (incomplete_albums.count / daily_retry_limit)
    days_until_complete.days.from_now.strftime("%a %B %d, %Y")
  end
end
