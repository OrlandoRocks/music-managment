module DistributionSystem
  class DistributorFactory
    def self.distributor_for(converted_album, config)
      case converted_album.class.name
      when "DistributionSystem::TrackMonetization::Album"
        DistributionSystem::Believe::Distributor.new(
          {
            bundle: DistributionSystem::Believe::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name),
            )
          }
        )

      when "DistributionSystem::DDEX::Album"
        DistributionSystem::Believe::Distributor.new(
          {
            bundle: DistributionSystem::Believe::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Believe::Album"
        DistributionSystem::Believe::Distributor.new(
          {
            bundle: DistributionSystem::Believe::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Itunes::Album"
        DistributionSystem::Itunes::Distributor.new(
          {
            bundle: DistributionSystem::Itunes::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album
            )
          }
        )

      when "DistributionSystem::Amazon::Album"
        DistributionSystem::Amazon::Distributor.new(
          {
            bundle: DistributionSystem::Amazon::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::SevenDigital::Album"
        DistributionSystem::Believe::Distributor.new(
          {
            bundle: DistributionSystem::SevenDigital::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Amazonod::Album"
        DistributionSystem::Amazonod::Distributor.new(
          {
            bundle: DistributionSystem::Amazonod::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::GroupieTunes::Album"
        DistributionSystem::GroupieTunes::Distributor.new(
          {
            schema: DistributionSystem::Schema.new(
              Rails.root.join("app", "services", "distribution_system", "schemas", "groupie_tunes.dtd")
            ),
            bundle: DistributionSystem::GroupieTunes::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Streaming::Album"
        DistributionSystem::Streaming::Distributor.new(
          {
            work_dir: config.work_dir,
            s3_streaming: config.s3_streaming,
            bundle: DistributionSystem::Streaming::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Medianet::Album"
        DistributionSystem::Medianet::Distributor.new(
          {
            bundle: DistributionSystem::Medianet::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Spotify::Album"
        DistributionSystem::Spotify::Distributor.new(
          {
            work_dir: config.work_dir,
            bundle: DistributionSystem::Spotify::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Google::Album"
        DistributionSystem::Google::Distributor.new(
          {
            schema: DistributionSystem::Schema.new(
              Rails.root.join("app", "services", "distribution_system", "schemas", "ddex37.xsd")
            ),
            bundle: DistributionSystem::Google::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Wimp::Album"
        DistributionSystem::Wimp::Distributor.new(
          {
            bundle: DistributionSystem::Wimp::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::YoutubeMusic::Album"
        DistributionSystem::YoutubeMusic::Distributor.new(
          {
            bundle: DistributionSystem::YoutubeMusic::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Juke::Album"
        DistributionSystem::Juke::Distributor.new(
          {
            bundle: DistributionSystem::Juke::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Gracenote::Album"
        DistributionSystem::Gracenote::Distributor.new(
          {
            bundle: DistributionSystem::Gracenote::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::Shazam::Album"
        DistributionSystem::Shazam::Distributor.new(
          {
            bundle: DistributionSystem::Shazam::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::YoutubeSr::Album"
        DistributionSystem::YoutubeSr::Distributor.new(
          {
            bundle: DistributionSystem::YoutubeSr::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )

      when "DistributionSystem::SimfyAfrica::Album"
        DistributionSystem::SimfyAfrica::Distributor.new(
          {
            bundle: DistributionSystem::SimfyAfrica::Bundle.new(
              config.work_dir,
              config.transcoder,
              config.s3_connection,
              converted_album,
              StoreDeliveryConfig.for(converted_album.store_name)
            )
          }
        )
      else
        raise "Do not know how to distribute converted_album with type #{converted_album.class}"
      end
    end
  end
end
