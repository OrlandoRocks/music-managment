module DistributionSystem::Rhapsody
  class Converter < DistributionSystem::Believe::Converter
    description "Rhapsody Music"
    handle_salepoints(/^RhapsodyRH/)
  end
end
