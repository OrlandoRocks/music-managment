module DistributionSystem::YoutubeDeliverable
  # defines territories for store_id 28(YouTube Music) and track_monetization store_id 48(YouTube Content Id)
  def territory_codes(album)
    return countries(album) if album.album_countries.any?

    territories = Country.unscoped.pluck(:iso_code)
    territories.map { |t| Country::COUNTRIES_MAP[t] || t }.uniq
  end
end
