module DistributionSystem::Streaming
  class Track < DistributionSystem::Track
    attr_accessor :duration, :artist_name

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def filename(bitrate = "128")
      "#{album.upc}_#{number}_#{bitrate}.mp3"
    end
  end
end
