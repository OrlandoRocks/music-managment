class DistributionSystem::Streaming::Converter < DistributionSystem::Converter
  description "Streaming Music"
  handle_salepoints(/^Streaming/)

  ALBUM_KLASS           = DistributionSystem::Streaming::Album
  TRACK_KLASS           = DistributionSystem::Streaming::Track

  #    covert methods converts a tunecore album to a Streaming album
  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    this_year = Date.today.year.to_s

    album = DistributionSystem::Streaming::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        orig_release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        release_date: tunecore_album.sale_date.strftime("%Y-%m-%d"),
        genres: tunecore_album.genres.collect(&:name),
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artist_name: tunecore_album.is_various? ? "Various Artists" : tunecore_album.artist_name,
        delivery_type: delivery_type,
        copyright_year: this_year,
        takedown: set_takedown(tunecore_album, tc_salepoints, "Streaming")
      }
    )

    tunecore_album.songs.each do |song|
      add_track(album, song)
    end
    # validate the album
    raise "Album validation failed. One or more required attributes are missing. #{album.inspect}" unless album.valid?

    album
  end

  # add album tracks
  def add_track(album, song)
    track = DistributionSystem::Streaming::Track.new(
      {
        album: album,
        number: song.track_num.to_s,
        title: song.name,
        artist_name: song.artist_name,
        sku: song.sku,
        isrc: song.isrc,
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
        s3_key: song_s3key(song),
        s3_bucket: song_s3bucket(song)

      }
    )
    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end
end
