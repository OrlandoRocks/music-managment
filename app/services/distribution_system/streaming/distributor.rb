module DistributionSystem::Streaming
  class Distributor < DistributionSystem::Distributor
    def initialize(options)
      super
      @remote_server = options[:s3_streaming]
      @work_dir      = options[:work_dir]
    end

    def distribute(album, distribution)
      Rails.logger.info "Preparing to upload album #{album.upc}| album_id=#{album.tunecore_id} #{album}"
      raise "Invalid Album" unless album.valid?

      begin
        @bundle.create_bundle
        collect_files(distribution)
        send_bundle
      rescue StandardError => e
        raise e
      end
    end

    def do_send_bundle
      @bundle.album.tracks.each do |track|
        @remote_server.put(
          track_filename(track),
          "#{@bundle.album.tunecore_id}/#{track.number}.mp3",
          "#{track.artist_name}-#{track.title}.mp3"
        )
      end
    end

    def clean_up
      Rails.logger.info "Cleaning up #{@bundle.dirname} from the local work directory. #{@bundle.album}"
      FileUtils.remove_dir(@bundle.dir.path, true)
    end

    def track_filename(track, bitrate = "128")
      File.join(@work_dir, "Streaming", @bundle.album.upc, track.filename(bitrate))
    end
  end
end
