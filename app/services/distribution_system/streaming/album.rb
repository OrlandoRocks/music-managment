class DistributionSystem::Streaming::Album < DistributionSystem::Album
  # Streaming specific metadata elements.
  attr_accessor :copyright_year

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.album do |album|
      # manual_override until we find this bug
      delivery_type = "full_delivery"

      case delivery_type
      when "full_delivery"
        album.metadata "new"
      when "metadata_update"
        album.metadata "update"
      when "takedown"
        album.metadata "delete"
      else
        raise "Uknown delivery type: #{album.delivery_type}"
      end

      album.upc upc.to_s
      album.title title
      album.version "Original"
      album.artist artist_name
      album.label label_name
      album.genre genres.first
      # we do the tag! thing because of the hyphen
      album.tag!("sub-genre", (genres.size > 1) ? genres.last : "")
      album.explicit explicit_lyrics
      album.format "Album"
      album.release_date release_date
      album.sales_start_date release_date
      album.stream_start_date release_date
      album.copyright_year copyright_year
      album.tag!("c-line", copyright_cline)
      album.tag!("p-line", copyright_pline)
      album.volume_count "1"
      album.track_count tracks.size

      # unless self.delivery_type == "metadata_only"
      #   artwork_file.to_xml(album)
      # end

      album.tracks do |_track|
        tracks.each { |t| t.to_xml(album) }
      end
    end
  end

  def valid?
    # Check required fields
    # Check required fields
    req_attrs = [@upc, @title, @release_date, @label_name, @genres, @explicit_lyrics, @delivery_type]
    # puts req_attrs.inspect
    req_attrs.each do |required_field|
      return false if required_field.nil?
    end

    # Value tests
    # return false unless @upc.to_s =~ /^\d{12,14}/
    # return false unless @artwork_file.valid?
    [@tracks].each do |nested|
      nested.each { |child| return false unless child.valid? }
    end

    true
  end
end
