module DistributionSystem::Streaming
  class Bundle < DistributionSystem::Bundle
    def write_metadata
      File.open(File.join(@dir.path, @album.upc.to_s + ".xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
        @album.to_xml(xml)
      end
    end

    def collect_files
      @preset = :lala_streaming
      # get the album tracks
      @album.tracks.each do |track|
        orig = original_file(track)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(orig, track.s3_key)
        track.audio_file = track_filename(@dir.path, track, "128")
        transcode_track(track)
        begin
          FileUtils.rm_f(orig)
        rescue
        end
      end
    end

    def track_filename(path, track, bitrate)
      File.join(path, "#{track.album.upc}_#{track.number}_#{bitrate}.mp3")
    end
  end
end
