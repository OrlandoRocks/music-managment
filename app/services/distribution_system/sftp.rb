class DistributionSystem::Sftp
  attr_reader :path, :host, :username, :password, :port, :keyfile

  def initialize(delivery_config)
    @host     = delivery_config.hostname or raise "No hostname"
    @username = delivery_config.username or raise "No username."
    @port     = delivery_config.port || 22
    @password = delivery_config.password
    @keyfile  = File.expand_path(File.join(SSH_KEYFILE_DIR, delivery_config.keyfile.to_s)) if delivery_config.keyfile
    Rails.logger.debug "Created #{self.class} instance using net-ssh #{Net::SSH::Version::STRING}"
  end

  def connect(&_block)
    begin
      sftp = nil
      sftp =
        if @keyfile
          Net::SFTP.start(
            @host,
            @username,
            password: @password,
            keys: [@keyfile],
            port: @port,
            verify_host_key: false,
            timeout: 10
          )
        else
          Net::SFTP.start(
            @host,
            @username,
            password: @password,
            port: @port,
            verify_host_key: false,
            timeout: 10
          )
        end

      begin
        yield Interpreter.new(sftp)
      ensure
        sftp.close(:handle)
      end
    rescue StandardError => e
      Rails.logger.error("SFTP ERROR: #{e.message}")
      Rails.logger.error(e.backtrace.join('\n'))
      raise DistributionSystem::DeliveryError.new("ssh process failed. Status: #{e.message}")
    end
  end

  class Interpreter
    def initialize(sftp)
      @sftp = sftp
    end

    def mkdir_p(dirname)
      path = dirname.split("/")
      path.each_with_index do |element, idx|
        next if element == ""

        path_to_this_element = path.slice(0..idx).join("/")
        mkdir(path_to_this_element) unless fexist?(path_to_this_element)
      end
    end

    def upload(local_path, remote_path)
      Rails.logger.info "Uploading #{local_path} to #{remote_path}"
      @sftp.upload!(local_path, remote_path)
    end

    def opendir(remote_path)
      @sftp.opendir(remote_path)
    end

    def readdir(remote_path)
      @sftp.readdir(remote_path)
    end

    def rename(oldname, newname)
      @sftp.rename(oldname, newname)
    end

    def remove(fname)
      @sftp.remove(fname)
    end

    def mkdir(path)
      @sftp.mkdir!(path)
    end

    def rmdir(remote_path)
      @sftp.rmdir(remote_path)
    end

    def cp_r(local_path, remote_path, delivery_type = "full_delivery", takedown = false)
      Rails.logger.info "cp_r: #{local_path} -> #{remote_path}"
      basename = File.basename(local_path)
      if File.directory?(local_path)
        remote_dir = remote_path.strip.empty? ? basename.to_s : "#{remote_path}/#{basename}"
        mkdir_p(remote_dir)
        cur_or_parent_dir = [".", ".."]
        Dir.new(local_path).each do |entry|
          next if cur_or_parent_dir.member?(entry)

          if takedown
            cp_r "#{local_path}/#{entry}", remote_dir.to_s if entry.include?("xml") # send only the xml
          else
            case delivery_type
            when "full_delivery"
              cp_r "#{local_path}/#{entry}", remote_dir.to_s
            when "metadata_only"
              cp_r "#{local_path}/#{entry}", remote_dir.to_s if entry.include?("xml") # send only the xml
            else
            end
          end
        end
      else
        upload(local_path, "#{remote_path}/#{File.basename(local_path)}")
      end
    end

    def fexist?(filename)
      begin
        @sftp.stat!(filename, Net::SFTP::Protocol::V06::Attributes::F_SIZE)
        true
      rescue Net::SFTP::StatusException => e
        raise e unless e.code == Net::SFTP::Constants::StatusCodes::FX_NO_SUCH_FILE

        false
      end
    end
  end
end
