require "net/ftp"

module DistributionSystem
  class Ftp
    def initialize(delivery_config)
      @location = "ftp://#{delivery_config.username}:#{delivery_config.password}@#{delivery_config.hostname}"

      begin
        @use_uri  = true
        @uri      = ::URI.parse(@location)
      rescue URI::InvalidURIError => e
        @use_uri  = false
        @user     = delivery_config.username
        @password = delivery_config.password
        @host     = delivery_config.hostname
        @path     = @host.split("/", 2)[1]
      end
      @port = delivery_config.port || 21
    end

    def connect(&_block)
      begin
        ftp = nil
        ftp = initialize_ftp

        begin
          ftp.passive = true
          session = Session.new(ftp)
          if @use_uri
            session.cd @uri.path unless ["", "/"].member?(@uri.path.strip)
          elsif @path
            session.cd @path
          end

          yield session
        ensure
          ftp.close
        end
      rescue Timeout::Error => e
        Rails.logger.error("Timeout during ftp connection: #{e.message}")
        exit
      end
    end

    class Session
      def initialize(ftp)
        @ftp = ftp
      end

      def mkdir_p(dirname)
        Rails.logger.info "mkdir_p: #{dirname}"
        path                 = dirname.split("/")
        current_dir          = pwd
        begin
          path.each do |element|
            next if element == "."

            mkdir(element) # unless ls.member?(element)
            cd element
          end
        ensure
          cd current_dir
        end
      end

      def mkdir(dirname)
        Rails.logger.info "FTP MKDIR #{dirname}"
        begin
          @ftp.mkdir(dirname)
        rescue => e
          unless e.message[/File exists|file already exists|550/]
            raise DistributionSystem::DeliveryError.new("mkdir issues...going to retry. Error: #{e.message}")
          end

          Rails.logger.info("DIR #{dirname} exists.  Continuing.")
        end
      end

      def cp_r(local_path, remote_path = "", delivery_type = "full_delivery", takedown = false)
        Rails.logger.info "cp_r: #{local_path} -> #{remote_path}"
        basename = File.basename(local_path)
        if File.directory?(local_path)
          remote_dir = remote_path.strip.empty? ? basename.to_s : "#{remote_path}/#{basename}"
          mkdir_p remote_dir
          cur_or_parent_dir = [".", ".."]
          Dir.new(local_path).each do |entry|
            next if cur_or_parent_dir.member?(entry)

            if takedown
              cp_r "#{local_path}/#{entry}", remote_dir.to_s if entry.include?("xml") # send only the xml
            else
              case delivery_type
              when "full_delivery"
                cp_r "#{local_path}/#{entry}", remote_dir.to_s
              when "metadata_only"
                cp_r "#{local_path}/#{entry}", remote_dir.to_s if entry.include?("xml") # send only the xml
              else
              end
            end
          end
        else
          put(local_path, "#{remote_path}/#{File.basename(local_path)}")
        end
      end

      def ls
        Rails.logger.info "FTP NLST"
        @ftp.nlst
      end

      def pwd
        Rails.logger.info "FTP PWD"
        @ftp.pwd
      end

      def cd(dirname)
        @ftp.chdir(dirname)
      end

      def put(local_file, remote_file)
        Rails.logger.info "FTP PUT: #{local_file} -> #{remote_file}"
        begin
          @ftp.put(local_file, remote_file)
        rescue => e
          raise e unless e.message[/Connection reset/]

          Rails.logger.info("Connection reset by peer during FTP put.  Will raise DeliveryError")
          raise DistributionSystem::DeliveryError.new("Connection reset by peer during FTP put")
        end
      end

      def upload(local_file, remote_file)
        put(local_file, remote_file)
      end
    end

    private

    def initialize_ftp
      @host, @user, @password = @uri.host, @uri.user, @uri.password if @use_uri
      ftp = Net::FTP.new
      ftp.connect(@host, @port)
      ftp.login(@user, @password)
      ftp.read_timeout = 20
      ftp
    end
  end
end
