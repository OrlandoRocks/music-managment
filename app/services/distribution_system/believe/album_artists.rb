module DistributionSystem::Believe::AlbumArtists
  def primary_and_featured_artists
    primary_artists | featured_artists
  end

  def primary_artists
    artists.select(&:is_primary?)
  end

  def featured_artists
    artists.select(&:is_featured?)
  end

  def primary_artist
    artists.find(&:is_primary?)
  end

  def show_display_name?
    true
  end

  def contributors_for(contributor_type)
    contributors = []
    resource_contributors.each do |artist|
      roles = artist.roles_for(contributor_type)
      roles.each do |role|
        contributors << contributor(artist, role)
      end
    end
    contributors
  end

  def contributor(artist, role)
    {
      name: artist.name,
      role: role.role_type.camelize,
      user_defined: role.user_defined
    }
  end
end
