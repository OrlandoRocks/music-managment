class DistributionSystem::Believe::Album < DistributionSystem::Album
  include DistributionSystem::XmlFormattable
  include DistributionSystem::Believe::AlbumArtists
  include DistributionSystem::Believe::ResourceArtists
  xml_format :believe
  xml_validation_with :ddex

  def indication
    if metadata_only_delivery? || takedown
      "UpdateMessage"
    else
      "OriginalMessage"
    end
  end

  def price_code
    (album_type == "Single") ? track_price_code : album_price_code
  end

  def xml_timestamp
    Time.now.iso8601
  end

  def get_album_type
    (album_type == "Ringtone") ? "RingtoneRelease" : album_type
  end

  def version
    ddex_version
  end

  def supports_multiple_roles
    (ddex_version.length > 2) ? (ddex_version.to_f / 10 >= 37) : ddex_version.to_f >= 37
  end

  def genre_name
    DistributionSystem::Believe::GenreHelper.get_genre(genre, store_name)
  end

  def is_ep?
    return false if duration.blank?

    tracks.length.between?(2, 6) && duration < 1800
  end
end
