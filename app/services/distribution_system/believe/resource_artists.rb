module DistributionSystem::Believe::ResourceArtists
  FEATURING_REGEXES = [
    /(feat(\.{0,1}|(ur(ing|e(d|s))){1})|\sw(ith|\/|\.){0,1})+\s/i,
    /\./,
    /\,/,
    /\//,
    /\s\&\s/,
    /\s\+\s/,
    /\s\-\s/,
    /\sand\s/i,
    /\smeets\s/i,
    /\sv(s|ersus)\s/i
  ]

  def artist_display_name
    artists_have_clean_names? ? display_name : primary_artist.name
  end

  def artist_original_name(track)
    artists_have_original_clean_names?(track) ? track_artists_array(track).to_sentence : primary_artist.name
  end

  def artists_have_original_clean_names?(track)
    track_artists_array(track).detect do |name|
      FEATURING_REGEXES.detect { |regex| regex.match(name) }
    end.blank?
  end

  def display_name
    primary_names  = primary_names_array.to_sentence
    featured_names = featured_names_array.to_sentence
    featured_names.empty? ? primary_names : "#{primary_names} (feat. #{featured_names})"
  end

  def artists_have_clean_names?
    (primary_names_array + featured_names_array).detect do |name|
      FEATURING_REGEXES.detect { |regex| regex.match(name) }
    end.blank?
  end

  def primary_names_array
    primary_artists.map(&:name)
  end

  def featured_names_array
    featured_artists.map(&:name)
  end

  def track_artists_array(track)
    track.primary_artists.map(&:name)
  end
end
