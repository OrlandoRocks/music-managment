module DistributionSystem::Believe
  class Artist < DistributionSystem::Artist
    include DistributionSystem::XmlFormattable
    xml_format :believe
    xml_validation_with :ddex

    MAIN_ARTIST     = "MainArtist".freeze
    FEATURED_ARTIST = "FeaturedArtist".freeze
    PRIMARY_ROLES   = ["primary_artist", "primary"].map(&:freeze).freeze
    FEATURING_ROLES = ["featuring", "with"].map(&:freeze).freeze

    attr_accessor :roles,
                  :tunecore_artist_id,
                  :artwork_s3_key,
                  :artwork_s3_bucket,
                  :artwork_s3_bucket_location,
                  :primary,
                  :artist_roles

    def primary_or_featured_role_name
      if is_primary?
        MAIN_ARTIST
      elsif is_featured?
        FEATURED_ARTIST
      end
    end

    def is_primary?
      (roles & PRIMARY_ROLES).present?
    end

    def is_featured?
      (roles & FEATURING_ROLES).present?
    end

    def roles_for(contributor_type)
      artist_roles.select do |artist_role|
        artist_role.send("#{contributor_type}_contributor_role")
      end
    end

    # For artists equality
    def ==(other_artist)
      super ||
        other_artist.instance_of?(self.class) &&
          !name.nil? &&
          name == other_artist.name &&
          roles == other_artist.roles &&
          artist_roles == other_artist.artist_roles
    end
    alias :eql? :==

    def hash
      "#{name}#{roles&.join(',')}#{artist_roles&.map(&:role_type)&.join(',')}".hash
    end
  end
end
