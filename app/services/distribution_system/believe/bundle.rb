class DistributionSystem::Believe::Bundle
  include DistributionSystem::Transcodable
  attr_accessor :dir,
                :album,
                :transcoder,
                :s3,
                :resources,
                :remote_dir,
                :dirname,
                :skip_batching,
                :batch_id,
                :remote_dir_timestamped,
                :use_resource_dir,
                :timestamp,
                :send_batch_complete,
                :batch_size,
                :use_manifest,
                :manifest_params,
                :preset

  def initialize(work_dir, transcoder, s3, album, store_delivery_config)
    @album               = album
    @transcoder          = transcoder
    @s3                  = s3
    @resources           = []
    @timestamp           = Time.now.strftime("%H%M%S")
    @send_batch_complete = true

    setup_directories(work_dir)
    set_data_from_config(store_delivery_config)
  end

  def set_data_from_config(store_delivery_config)
    @remote_dir             = store_delivery_config["remote_dir"]             || "."
    @skip_batching          = store_delivery_config["skip_batching"]          || false
    @remote_dir_timestamped = store_delivery_config["remote_dir_timestamped"] || false
    @use_resource_dir       = store_delivery_config["use_resource_dir"]
    @use_manifest           = store_delivery_config["use_manifest"]
    @album.set_data_from_config(store_delivery_config)
  end

  def path_id
    @path_id ||= @album.track_only ? album.tracks.first.isrc : album.upc
  end

  def setup_directories(work_dir)
    base_upc_dir = File.join(work_dir, album.store_name, album.upc.to_s)
    @dirname = @album.track_only ? File.join(base_upc_dir, path_id) : base_upc_dir
    FileUtils.mkdir_p(@dirname)
    @dir = Dir.new(@dirname)
  end

  def resources_for(delivery_type)
    resources.select { |r| r[:delivery].include?(delivery_type.to_sym) }
  end

  def write_metadata
    local_filepath = file(:local, :xml)

    File.open(local_filepath, "w") do |f|
      f.write(@album.to_xml)
    end

    @manifest_params[:xml_hashsum] = Digest::MD5.file(local_filepath).to_s if use_manifest
  end

  def collect_files
    # booklet
    if album.deliver_booklet && album.booklet_file && album.booklet_file[:asset]
      booklet_source = file(:local, :pdf)
      s3.bucket = album.booklet_file[:bucket] if album.booklet_file[:bucket]
      s3.get(booklet_source, album.booklet_file[:asset])
      album.booklet_file[:asset] = booklet_source
    end

    # cover
    unless album.skip_image?
      image_source = file(:local, :jpg)
      s3.bucket = album.artwork_file[:bucket] if album.artwork_file[:bucket]
      s3.get(image_source, album.artwork_file[:asset])
      DistributionSystem::ImageUtils.resize(image_source, album.image_size, album.image_size)
      album.artwork_file[:asset] = image_source
    end

    # tracks
    pull_and_transcode_tracks unless album.skip_media
  end

  def pull_and_transcode_tracks
    album.tracks.each do |track|
      original_file = File.join(dir.path, track.temp_filename)
      s3.bucket = track.s3_bucket if track.s3_bucket != nil
      s3.get(original_file, track.s3_key)
      track.audio_file = file(:local, album.audio_codec_type, "01_#{track.number}")
      handle_transcoding(track, original_file)
      begin
        track.set_duration
      rescue => e
        raise "Couldn't get duration for track #{track.number}: #{e.message}"
      end
      FileUtils.rm(original_file)
      wav_files = File.join(dir.path, "*.wav")
      FileUtils.rm_f Dir.glob(wav_files)
    end
  end

  def handle_transcoding(track, original_file)
    transcode_file?(track) ? transcode_track(track) : move_file_to_destination(track, original_file)
  end

  def transcode_file?(track)
    track.s3_key.split(".").last != "flac" && track.album.audio_codec_type == "flac"
  end

  def move_file_to_destination(track, original_file)
    FileUtils.cp(original_file, track.audio_file)
  end

  def track_filename(extension, file_name_suffix)
    if file_name_suffix.empty?
      "#{path_id}.#{extension}"
    else
      "#{path_id}_#{file_name_suffix}.#{extension}"
    end
  end

  def complete_file_name
    if skip_batching
      "delivery.complete"
    else
      "BatchComplete_#{batch_id}.xml"
    end
  end

  def complete_file_path
    if skip_batching
      File.join(remote_dir, full_remote_dir)
    else
      File.join(remote_dir, batch_id.to_s)
    end
  end

  def write_complete_file(delivery_batch = nil)
    local_filepath = File.join(dir.path, complete_file_name)

    if use_manifest
      manifest = DistributionSystem::DeliveryBatchManifest.write_manifest(delivery_batch)
      File.open(local_filepath, "w") do |f|
        f.write(manifest)
      end
    else
      FileUtils.touch(local_filepath)
    end
  end

  def remote_file_path
    if skip_batching
      File.join(remote_dir, full_remote_dir)
    else
      File.join(remote_dir, batch_id.to_s, full_remote_dir)
    end
  end

  def full_remote_dir
    if remote_dir_timestamped
      File.join(Date.today.strftime("%Y%m%d"), "#{@timestamp}_#{album.tunecore_id}_#{path_id}")
    else
      path_id.to_s
    end
  end

  def create_bundle
    unless album.skip_media
      album.tracks.each do |track|
        resources << {
          type: :track,
          delivery: [:full_delivery],
          local_file: file(:local, album.audio_codec_type, "01_#{track.number}"),
          remote_file: file(:remote, album.audio_codec_type, "01_#{track.number}"),
        }
      end
    end
    if album.deliver_booklet && album.booklet_file && album.booklet_file[:asset]
      resources << {
        type: :booklet,
        delivery: [:full_delivery],
        local_file: file(:local, :pdf),
        remote_file: file(:remote, :pdf)
      }
    end
    unless album.skip_image?
      resources << {
        type: :artwork,
        delivery: [:full_delivery],
        local_file: file(:local, :jpg),
        remote_file: file(:remote, :jpg)
      }
    end
    resources << {
      type: :metadata,
      delivery: [:full_delivery, :metadata_only],
      local_file: file(:local, :xml),
      remote_file: file(:remote, :xml)
    }
    assemble_manifest_params if @use_manifest
  end

  def assemble_manifest_params
    metadata_path = resources.detect { |r| r[:type] == :metadata }[:remote_file]
    @manifest_params = {
      xml_remote_path: metadata_path,
      uuid: @album.get_uuid,
      upc: @album.upc,
      takedown: @album.takedown
    }
  end

  def complete_file
    {
      local_file: File.join(dir.path, complete_file_name),
      remote_file: File.join(complete_file_path, complete_file_name)
    }
  end

  private

  def use_resource_dir?(type)
    [:pdf, :jpg, :flac, :wav, :mp3].include?(type.downcase.to_sym) && use_resource_dir
  end

  def file(type, extension, file_name_suffix = "")
    paths = { local: dir.path, remote: remote_file_path }
    paths[:remote] = File.join(paths[:remote], "resources") if use_resource_dir?(extension)
    File.join(
      paths[type],
      track_filename(extension, file_name_suffix)
    )
  end
end
