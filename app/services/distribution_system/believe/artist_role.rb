module DistributionSystem
  class Believe::ArtistRole
    attr_reader :resource_contributor_role, :indirect_contributor_role, :user_defined, :role_type

    def initialize(role)
      @role_type                 = role.role_type
      @resource_contributor_role = role.resource_contributor_role
      @indirect_contributor_role = role.indirect_contributor_role
      @user_defined              = role.respond_to?(:user_defined) ? role.user_defined : false
    end

    def ==(other_artist_role)
      super ||
        other_artist_role.instance_of?(self.class) &&
          role_type == other_artist_role.role_type
    end
    alias :eql? :==

    delegate :hash, to: :role_type
  end
end
