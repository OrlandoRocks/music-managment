module DistributionSystem::Believe::TrackArtists
  def primary_and_featured_artists
    primary_artists | album_featured_artists | featured_artists
  end

  def primary_artists
    track_primary_artists =
      artists.select do |artist|
        artist.is_primary? && !artist.name.casecmp?("various artists")
      end
    if album_primary_artists.map(&:name) == track_primary_artists.map(&:name)
      track_primary_artists
    elsif album_primary_artists.try(:first).try(:name).try(:downcase) == "various artists"
      track_primary_artists
    else
      album_primary_artists | track_primary_artists
    end
  end

  delegate :primary_artists, to: :album, prefix: true

  delegate :featured_artists, to: :album, prefix: true

  def featured_artists
    artists.select(&:is_featured?)
  end

  def primary_artist
    artists.find(&:is_primary?) || album.primary_artist
  end

  def contributors_for(contributor_type)
    contributors = []
    artists.each do |artist|
      roles = artist.roles_for(contributor_type)
      roles.each do |role|
        contributors << contributor(artist, role)
      end
    end
    contributors
  end

  def contributor(artist, role)
    {
      name: artist.name,
      role: role.role_type.camelize,
      user_defined: role.user_defined
    }
  end

  def show_display_name?
    artists_by_name_and_role(album.primary_and_featured_artists) != artists_by_name_and_role(primary_and_featured_artists)
  end

  def artists_by_name_and_role(artists)
    artists.map do |artist|
      { name: artist.name, roles: artist.roles }
    end
  end
end
