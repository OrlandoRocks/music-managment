module DistributionSystem::Believe
  class Distributor < DistributionSystem::Distributor
    MAX_BATCH_WAIT_TIME = 21_600 # 6 hours

    class BatchWaitTimeout < StandardError; end

    attr_accessor :distributable

    def initialize(options)
      super
      @batch_complete = false
    end

    def distribute(album, distributable)
      @distributable = distributable
      set_batch_id unless @bundle.skip_batching
      super
    rescue => e
      remove_from_batch(distributable) unless @bundle.skip_batching || batch_error(e)
      raise e
    end

    def do_send_bundle
      upload_resources
      mark_batch_item_as_sent
      delivery_batch.increment_processed_count
      send_batch_complete_file if should_send_batch_complete?
    end

    def mark_batch_item_as_sent
      item = delivery_batch.delivery_batch_items
                           .where(deliverable_type: distributable.class.name,
                                  deliverable_id: distributable.id)
                           .first
      item.mark_as_sent
      item.add_to_manifest(@bundle.manifest_params) if @bundle.use_manifest
    end

    def send_batch_complete_file
      @remote_server.connect do |server|
        send_complete_file(server)
      end

      delivery_batch.update(batch_complete_sent_at: Time.now, currently_closing: false)
    end

    def upload_resources
      remote_dir   = @bundle.remote_file_path
      resource_dir = assign_resource_dir(remote_dir)

      @remote_server.connect do |server|
        begin
          server.mkdir_p(remote_dir)
          server.mkdir_p(resource_dir) if full_delivery? && @bundle.use_resource_dir
          @bundle.resources_for(@bundle.album.delivery_type).each do |resource|
            server.upload(resource[:local_file], resource[:remote_file])
          end
        rescue DistributionSystem::DeliveryError, Exception => e
          Rails.logger.error "Error during distributable of #{@bundle.album.upc}, Error Message: #{e.message} #{@bundle.album}"
          Rails.logger.error e.backtrace.join('\n')
          raise e
        end
      end
    end

    def assign_resource_dir(remote_dir)
      (@bundle.use_resource_dir && @bundle.album.include_assets) ? File.join(@bundle.remote_file_path, "resources") : remote_dir
    end

    def send_complete_file(server)
      @bundle.write_complete_file(delivery_batch)
      complete_file = @bundle.complete_file
      server.upload(complete_file[:local_file], complete_file[:remote_file])
    end

    def set_batch_id
      @bundle.batch_id = delivery_batch.batch_id
    end

    def should_send_batch_complete?
      return false if delivery_batch.active
      return entire_batch_sent? if !@bundle.skip_batching && @bundle.send_batch_complete

      @bundle.send_batch_complete
    end

    def batch_full
      delivery_batch.reload.batch_complete?
    end

    def entire_batch_sent?
      DistributionSystem::BatchManager.entire_batch_sent?(delivery_batch)
    end

    def delivery_batch
      @delivery_batch ||= DistributionSystem::BatchManager.join_batch(distributable.class.name, distributable.id, @bundle.album.store_id)
    end

    def remove_from_batch(distributable)
      DistributionSystem::BatchManager.remove_from_batch(distributable.class.name, distributable.id, delivery_batch.id)
      send_batch_complete_file if should_send_batch_complete?
    end

    def full_delivery?
      @bundle.album.include_assets && !@bundle.album.metadata_only_delivery?
    end

    def batch_error(e)
      e.is_a?(DistributionSystem::BatchManager::JoinBatchError) || e.is_a?(DistributionSystem::BatchManager::RemoveFromBatchError)
    end
  end
end
