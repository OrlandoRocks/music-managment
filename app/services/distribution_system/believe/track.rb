module DistributionSystem::Believe
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength
    include DistributionSystem::XmlFormattable
    include TrackArtists
    include ResourceArtists

    xml_format :believe
    xml_validation_with :ddex

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger,
                  :artists,
                  :song_id,
                  :custom_id,
                  :asset_exclusion,
                  :explicit_lyrics_label,
                  :language_code

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    delegate :store_name, to: :album

    def rights_percentage
      takedown ? "0" : "100"
    end

    delegate :supports_multiple_roles, to: :album

    delegate :party_id, to: :album

    def ytsr_track_details_by_territory_countries?
      FeatureFlipper.show_feature?(:ytsr_countries_track_sr_detials, ::Album.find_by(id: album.tunecore_id)&.person)
    end

    def spotify_explicit_label?
      FeatureFlipper.show_feature?(:spotify_explicit_label, ::Album.find_by(id: album.tunecore_id)&.person)
    end
  end
end
