module DistributionSystem::Believe
  class Converter < DistributionSystem::Converter
    description "Believe"
    handle_salepoints(/^(Believe|Spotify)/)

    ALBUM_KLASS           = DistributionSystem::Believe::Album
    TRACK_KLASS           = DistributionSystem::Believe::Track
    ARTIST_KLASS          = DistributionSystem::Believe::Artist
    PRIMARY_ARTIST_ROLES  = ["primary"].freeze
    FEATURED_ARTIST_ROLES = ["with", "featuring"].map(&:freeze).freeze

    INSTRUMENTAL_LANGUAGE_CODE = "zxx".freeze

    class StoreError < StandardError; end

    def initialize
      @supports_metadata_update = true
      @supports_takedown        = true
    end

    def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
      build_album(tunecore_album, tc_salepoints, delivery_type)
      add_tracks(tunecore_album)
      album
    end

    def build_track(album, song)
      @track = self.class::TRACK_KLASS.new(
        {
          album: album,
          song_id: song.id,
          number: song.track_num,
          title: song.name,
          artists: set_song_artists(song),
          sku: song.sku,
          isrc: song.isrc,
          explicit_lyrics: song.parental_advisory?,
          orig_filename: song.upload.uploaded_filename,
          temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
          s3_key: song_s3key(song),
          s3_bucket: song_s3bucket(song),
          album_only: song.album_only,
          free_song: song.free_song,
          asset_url: song.external_asset_url,
          duration: calculate_duration(song),
          available_for_streaming: song.available_for_streaming,
          song_start_time: get_song_start_time(song, album.store_id),
          language_code: track_language_code(song)
        }
      )
    end

    def normalize_salepoints(salepoints)
      sp = salepoints.select { |s| handles_store?(s.store) && s.store.in_use_flag }
      sp.sort { |a, b| b.id <=> a.id }.uniq(&:store_id)
    end

    def set_label(tunecore_album)
      tunecore_album.label ? tunecore_album.label.name : tunecore_album.artist_name
    end

    def set_artwork(tunecore_album)
      { asset: tunecore_album.artwork.s3_asset.key, bucket: tunecore_album.artwork.s3_asset.bucket }
    end

    def set_delivery_type(tunecore_album, salepoint, delivery_type)
      (tunecore_album.takedown? || salepoint.taken_down?) ? "metadata_only" : delivery_type
    end

    def build_album(tunecore_album, tc_salepoints, delivery_type)
      @salepoints = normalize_salepoints(tc_salepoints)
      @salepoint  = salepoints.first
      raise StoreError unless salepoint

      @album = self.class::ALBUM_KLASS.new(
        {
          tunecore_id: tunecore_album.id,
          copyright_name: tunecore_album.copyright_name,
          upc: tunecore_album.upc.to_s,
          sku: tunecore_album.sku,
          title: tunecore_album.name,
          copyright_pline: pline(tunecore_album),
          copyright_cline: cline(tunecore_album),
          release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
          genre: tunecore_album.genres.first.id,
          store_name: salepoint.store.short_name,
          store_id: salepoint.store_id,
          artwork_s3_key: tunecore_album.artwork.s3_asset.key,
          tracks: [],
          label_name: set_label(tunecore_album),
          explicit_lyrics: tunecore_album.parental_advisory,
          liner_notes: tunecore_album.liner_notes,
          artists: set_album_artists(tunecore_album),
          resource_contributors: collect_resource_contributors(tunecore_album),
          artwork_file: set_artwork(tunecore_album),
          booklet_file: tunecore_album.booklet ? { asset: tunecore_album.booklet.s3_asset.key, bucket: tunecore_album.booklet.s3_asset.bucket } : {},
          delivery_type: set_delivery_type(tunecore_album, salepoint, delivery_type),
          album_type: tunecore_album.class.name,
          takedown: tunecore_album.takedown? || salepoint.taken_down?,
          countries: countries(tunecore_album),
          is_various: tunecore_album.is_various?,
          language: tunecore_album.language.description,
          customer_name: tunecore_album.person.name,
          customer_id: tunecore_album.person_id,
          actual_release_date: tunecore_album.sale_date,
          created_with_songwriter: tunecore_album.created_with_songwriter?,
          language_code: tunecore_album.language.ISO_639_1_code || tunecore_album.language.code
        }
      )
    end

    def add_tracks(tunecore_album)
      tunecore_album.songs.each do |song|
        @album.tracks << build_track(album, song)
      end
    end

    def format_takedown_at_date(tunecore_album)
      end_date = tunecore_album.takedown_at || @salepoint.takedown_at
      end_date.strftime("%Y-%m-%d")
    end

    def set_album_artists(album)
      if album.creatives.present?
        album.creatives.map do |creative|
          self.class::ARTIST_KLASS.new(
            name: creative.artist.name,
            roles: [creative.role]
          )
        end
      else
        [
          self.class::ARTIST_KLASS.new(
            name: album.artist_name,
            roles: PRIMARY_ARTIST_ROLES
          )
        ]
      end
    end

    def collect_resource_contributors(tunecore_album)
      return if tunecore_album.song_creatives.blank?

      AlbumSongArtists.song_contributors(tunecore_album).map do |contributor|
        build_artist_for_track(contributor)
      end
    end

    def set_song_artists(song)
      unsorted =
        SongArtist
        .for_song(song)
        .map do |creative|
          artist = build_artist_for_track(creative)
          add_primary_or_featured_role(artist, creative) if should_add_role(artist, creative)
          artist
        end
      unsorted.sort_by { |c1, _c2| sort_conditions(c1) }
    end

    def build_artist_for_track(creative)
      artist = self.class::ARTIST_KLASS.new(
        name: creative.artist_name,
        roles: creative.role_types.try(:split, ",") || [],
        artist_roles: artist_roles(creative)
      )
    end

    def add_primary_or_featured_role(artist, creative)
      artist.roles.unshift(creative.credit)
    end

    def should_add_role(artist, creative)
      !role_present(artist, creative) && !creative_belongs_to_album(creative)
    end

    def role_present(artist, creative)
      artist.roles.include?(creative.credit)
    end

    def creative_belongs_to_album(creative)
      creative.associated_to == "Album"
    end

    def artist_roles(creative)
      song_roles = ArtistRoleBuilder.from_role_ids(creative.role_ids.try(:split, ","))
      song_roles.map do |artist_role|
        DistributionSystem::Believe::ArtistRole.new(artist_role)
      end
    end

    def featured_artist(creative)
      (creative.roles & FEATURED_ARTIST_ROLES).present?
    end

    def sort_conditions(creative)
      [creative.primary ? 0 : 1, featured_artist(creative) ? 0 : 1]
    end

    def get_song_start_time(song, store_id)
      song.song_start_times&.find { |sst| sst.store_id == store_id }&.start_time
    end

    private

    attr_reader :salepoints, :salepoint, :album

    def track_language_code(song)
      if FeatureFlipper.show_feature?(:track_level_language, song.person)
        song.lyric_language_code.try(:ISO_639_1_code) || song.lyric_language_code.try(:code) || album.language_code
      else
        DistributionSystem::LanguageCodeHelper.code(album.language)
      end
    end
  end
end
