contributors_for("indirect").each do |contributor|
  xml.IndirectResourceContributor do |indirect_resource_contributor|
    indirect_resource_contributor.PartyName do |party_name|
      party_name.FullName contributor[:name]
    end
    if contributor[:user_defined]
      indirect_resource_contributor.IndirectResourceContributorRole({"UserDefinedValue" => contributor[:role], "Namespace" => DDEX_SENDER_ID}, 'UserDefined')
    else
      indirect_resource_contributor.IndirectResourceContributorRole contributor[:role]
    end
  end
end
