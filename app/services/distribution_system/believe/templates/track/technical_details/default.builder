technical_details.TechnicalResourceDetailsReference "T#{number}"
render_partial "track/audio_codec", self, xml: technical_details, track_codec: album.audio_codec_type, album_party_id: album.party_id
technical_details.NumberOfChannels 2
technical_details.SamplingRate 44.1
technical_details.IsPreview false
technical_details.File do |file|
  file.FileName audio_file.split("/").last
  file.FilePath "resources/"
  file.HashSum do |hash_sum|
    hash_sum.HashSum Digest::MD5.file(audio_file).to_s
    hash_sum.HashSumAlgorithmType "MD5"
  end
end