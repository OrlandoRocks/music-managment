xml.SoundRecording do |sound_recording|
  sound_recording.SoundRecordingType ["Spoken Word", "Comedy"].include?(DistributionSystem::Believe::GenreHelper.get_genre(album.genre, album.store_name)) ? "SpokenWordSoundRecording" : "MusicalWorkSoundRecording"
  sound_recording.SoundRecordingId do |sound_recording_id|
    sound_recording_id.ISRC isrc
    sound_recording.ProprietaryId(custom_id, {"Namespace" => "DPID:#{DDEX_SENDER_ID}"})
  end
  sound_recording.ResourceReference "A1"
  sound_recording.ReferenceTitle do |reference_title|
    reference_title.TitleText title
  end
  sound_recording.LanguageOfPerformance DistributionSystem::LanguageCodeHelper.code(album.language) if album.display_track_language
  sound_recording.Duration formatted_duration || "PT99H01M01S"
  sound_recording.SoundRecordingDetailsByTerritory do |recording_details|
    if takedown
      recording_details.TerritoryCode "Worldwide"
    else
      render_partial "deal_list/countries", self, xml: recording_details, countries: album.countries
    end

    recording_details.Title(:TitleType=>"DisplayTitle") do |display_title|
      display_title.TitleText title
    end

    render_partial("artists", self, xml: recording_details)

    recording_details.LabelName album.label_name

    recording_details.RightsController do |rights_controller|
      rights_controller.PartyName do |party_name|
        party_name.FullName "TuneCore"
      end
      rights_controller.PartyId DDEX_SENDER_ID
      rights_controller.RightsControllerRole "RightsController"
      rights_controller.RightSharePercentage rights_percentage
    end

    recording_details.PLine do |pline|
      pline.Year album.copyright_pline.split(' ')[0]
      pline.PLineText album.copyright_pline
    end
    recording_details.Genre do |genre|
      genre.GenreText DistributionSystem::Believe::GenreHelper.get_genre(album.genre, album.store_name)
    end
    recording_details.ParentalWarningType explicit_lyrics ? "Explicit" : "NotExplicit"
    if album.delivery_type == "full_delivery" && !album.skip_media
      recording_details.TechnicalSoundRecordingDetails do |technical_details|
        technical_details.TechnicalResourceDetailsReference "T#{number}"
        technical_details.File do |file|
          file.FileName audio_file.split("/").last
          file.FilePath "resources/"
        end
      end
    end
  end
end
