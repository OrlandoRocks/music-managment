xml.SoundRecording do |sound_recording|
  sound_recording.SoundRecordingType ["Spoken Word", "Comedy"].include?(DistributionSystem::Believe::GenreHelper.get_genre(album.genre, album.store_name)) ? "NonMusicalWorkSoundRecording" : "MusicalWorkSoundRecording"
  sound_recording.SoundRecordingId do |sound_recording_id|
    sound_recording_id.ISRC isrc
    sound_recording_id.ProprietaryId(song_id, { "Namespace" => "TuneCore" }) if album.display_track_id
  end
  sound_recording.ResourceReference "A#{number}"
  sound_recording.ReferenceTitle do |reference_title|
    reference_title.TitleText title
  end
  sound_recording.LanguageOfPerformance language_code
  sound_recording.Duration formatted_duration || "PT99H01M01S"
  sound_recording.SoundRecordingDetailsByTerritory do |recording_details|
    recording_details.TerritoryCode "Worldwide"
    recording_details.Title(:TitleType=>"FormalTitle") do |formal_title|
      formal_title.TitleText title
    end
    recording_details.Title(:TitleType=>"DisplayTitle") do |display_title|
      display_title.TitleText title
    end
    recording_details.Title(:TitleType=>"AbbreviatedDisplayTitle") do |abbrev_title|
      abbrev_title.TitleText title
    end
    render_partial("artists", self, xml: recording_details)
    render_partial "resource_contributors", self, xml: recording_details if album.supports_multiple_roles
    render_partial "track/indirect_contributors", self, xml: recording_details if album.supports_multiple_roles
    recording_details.DisplayArtistName artist_display_name if show_display_name? && supports_multiple_roles
    recording_details.PLine do |pline|
      pline.Year album.copyright_pline.split(' ')[0]
      pline.PLineText album.copyright_pline
    end
    recording_details.Genre do |genre|
      if album.try(:is_classical_genre?)
        genre.GenreText "Classical"
      else
        genre.GenreText DistributionSystem::Believe::GenreHelper.get_genre(album.genre, album.store_name)
      end
    end
    if spotify_explicit_label? && store_name == "Spotify"
      recording_details.ParentalWarningType explicit_lyrics_label
    else
      recording_details.ParentalWarningType (explicit_lyrics ? 'Explicit' : 'NotExplicit')
    end
    if album.delivery_type == "full_delivery" && !album.skip_media
      recording_details.TechnicalSoundRecordingDetails do |technical_details|
        render_partial "track/technical_details", self, technical_details: technical_details
      end
    end
  end
end
