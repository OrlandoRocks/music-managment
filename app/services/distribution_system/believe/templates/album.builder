tag_options = {
  "xmlns:ern"              => "http://ddex.net/xml/ern/#{ddex_version}",
  "xmlns:xsi"              => "http://www.w3.org/2001/XMLSchema-instance",
  "xsi:schemaLocation"     => "http://ddex.net/xml/ern/#{ddex_version} http://ddex.net/xml/ern/#{ddex_version}/release-notification.xsd",
  "MessageSchemaVersionId" => "ern/#{ddex_version}"
}
xml.instruct!(:xml, :version=>"1.0", :encoding=>"utf-8")
# start building xml
xml.tag!("ern:NewReleaseMessage", tag_options) do |ern|
  ern.MessageHeader  do |message_header|
    render_partial "message_header", self, xml: message_header
  end
  ern.UpdateIndicator indication if indication
  ern.ResourceList do |resource_list|
    render "tracks", self, xml: resource_list
    render_partial "album_image", artwork_file, xml: resource_list, album: self unless skip_image?
    if delivery_type == "full_delivery" && deliver_booklet && booklet_file && booklet_file[:asset]
      render "booklet_file", booklet_file, xml: resource_list, album: self
    end
  end

  ern.ReleaseList do |release_list|
    render_partial "release_list", self, xml: release_list
  end
  render_partial "deal_list", self, xml: xml
end
