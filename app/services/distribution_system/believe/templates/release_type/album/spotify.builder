if is_compilation_feature? || dj_release?
  xml.ReleaseType({ "UserDefinedValue" => "Compilation" }, "UserDefined")
elsif is_ep?
  xml.ReleaseType({ "UserDefinedValue" => "EP" }, "UserDefined")
else
  xml.ReleaseType get_album_type
end
