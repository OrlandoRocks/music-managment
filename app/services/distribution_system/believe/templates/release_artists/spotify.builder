rollup_artists.each do |artist|
  if artist.is_primary?
    xml.DisplayArtist do |display_artist|
      display_artist.PartyName do |party_name|
        party_name.FullName artist.name
      end
      render("artists/spotify/artist_detail", artist, xml: display_artist) if artist.spotify_id.present?
      display_artist.ArtistRole artist.primary_or_featured_role_name
    end
  end
  if !artist.is_primary? || artist.artist_roles.present?
    xml.DisplayArtist do |display_artist|
      display_artist.PartyName do |party_name|
        party_name.FullName artist.name
      end
      render("artists/spotify/artist_detail", artist, xml: display_artist) if artist.spotify_id.present?
      display_artist.ArtistRole artist.primary_or_featured_role_name if artist.is_featured?
      artist.artist_roles&.each do |artist_role|
        if artist_role.user_defined
          display_artist.ArtistRole(
            {
              "UserDefinedValue" => artist_role.role_type.capitalize,
              "Namespace"        => party_id
            },
            'UserDefined'
          )
        else
          display_artist.ArtistRole artist_role.role_type.capitalize
        end
      end
    end
  end
end
