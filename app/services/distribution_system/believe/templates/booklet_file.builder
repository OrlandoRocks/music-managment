xml.Text do |booklet|
  booklet.TextType 'NonInteractiveBooklet'
  booklet.TextId do |booklet_id|
    booklet_id.ProprietaryId("#{album.upc}.pdf", {"Namespace" => album.party_id})
  end
  booklet.ResourceReference "A#{album.tracks.size + 2}"
  booklet.Title do |title|
    title.TitleText album.title
  end
  booklet.TextDetailsByTerritory do |booklet_details|
    booklet_details.TerritoryCode 'Worldwide'
    booklet_details.TechnicalTextDetails do |technical_booklet_details|
      technical_booklet_details.TechnicalResourceDetailsReference "T#{album.tracks.size + 2}"
      technical_booklet_details.TextCodecType "PDF"
      technical_booklet_details.File do |file|
        file.FileName "#{album.upc}.pdf"
        file.FilePath "resources/"
        file.HashSum do |hashsum|
          hashsum.HashSum Digest::MD5.file(album.booklet_file[:asset]).to_s
          hashsum.HashSumAlgorithmType "MD5"
        end
      end
    end
  end
end
