xml.Image do |image|
  image.ImageType 'FrontCoverImage'
  image.ImageId do |image_id|
    image_id.ProprietaryId("#{album.upc}.jpg", {"Namespace" => "DPID:#{DDEX_SENDER_ID}"})
  end
  image.ResourceReference "A#{album.tracks.size + 1}"
  image.ImageDetailsByTerritory do |image_details|
    image_details.TerritoryCode 'Worldwide'
    if album.delivery_type == 'full_delivery'
      image_details.TechnicalImageDetails do |techimagedetails|
        techimagedetails.TechnicalResourceDetailsReference "T#{album.tracks.size + 1}"
        techimagedetails.ImageCodecType "JPEG"
        techimagedetails.ImageHeight album.image_size
        techimagedetails.ImageWidth album.image_size
        techimagedetails.File do |file|
          file.FileName "#{album.upc}.jpg"
          file.FilePath "resources/"
          file.HashSum do |hashsum|
            hashsum.HashSum Digest::MD5.file(album.artwork_file[:asset]).to_s
            hashsum.HashSumAlgorithmType "MD5"
          end
        end
      end
    end
  end
end
