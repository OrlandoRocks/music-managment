xml.Release do |release|
  render "release_id", self, xml: release
  release.ReleaseReference 'R0'
  release.ReferenceTitle do |reference_title|
    reference_title.TitleText title
  end
  render_partial "release_resource_reference_list", self, xml: release
  render_partial "release_type/album", self, xml: release
  render_partial "release_details_by_territory", self, xml: release
  render "pline", self, xml: xml
  render "cline", self, xml: xml
end
