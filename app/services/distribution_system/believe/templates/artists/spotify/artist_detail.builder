xml.PartyId({"Namespace" => "tunecore"}, artist_id)
spotify_id_display = (spotify_id == 'NEW') ? spotify_id : "spotify:artist:#{spotify_id}"
xml.PartyId({"Namespace" => "spotify"}, "#{spotify_id_display}")
