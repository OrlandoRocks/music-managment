primary_and_featured_artists.uniq(&:name).each do |artist|
  xml.DisplayArtist do |display_artist|
    display_artist.PartyName do |party_name|
      party_name.FullName artist.name
    end
    display_artist.ArtistRole artist.primary_or_featured_role_name
  end
end
