xml.MessageThreadId upc
xml.MessageId get_uuid
xml.MessageSender do |message_sender|
  render "message_sender", self, xml: message_sender
end
xml.MessageRecipient do |message_recipient|
 render "message_recipient", self, xml: message_recipient
end
xml.MessageCreatedDateTime xml_timestamp
xml.MessageControlType message_control
