xml.MessageThreadId upc
xml.MessageId get_uuid
xml.MessageSender do |message_sender|
  render "message_sender", self, xml: message_sender
end
if include_behalf
  xml.SentOnBehalfOf do |behalf_of|
    behalf_of.PartyId(customer_id, { "Namespace" => "TuneCore" })
  end
end
party_ids, party_full_names = party_id.split(','), party_full_name.split(',')
party_ids.each_with_index do |party_id, index|
  xml.MessageRecipient do |message_recipient|
    xml.PartyId party_id
    xml.PartyName do |party_name|
      xml.FullName party_full_names[index]
    end
  end
end
xml.MessageCreatedDateTime xml_timestamp
xml.MessageControlType "TestMessage" if test_delivery
