tracks.each do |track|
  render_partial "track", track, xml: xml, album: self
end
