xml.ReleaseId do |release_id|
  release_id.ICPN(upc, {'IsEan' => 'false'})
  release_id.ProprietaryId(upc, {"Namespace" => "DPID:#{party_id}"}) if use_proprietary_id
end
