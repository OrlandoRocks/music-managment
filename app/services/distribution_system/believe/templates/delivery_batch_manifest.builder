tag_options = {
  "xs:schemaLocation" => "http://ddex.net/xml/2011/echo/12 http://ddex.net/xml/2011/echo/12/echo.xsd",
  "xmlns:ernm"        => "http://ddex.net/xml/ern/#{ddex_version}",
  "xmlns:echo"        => "http://ddex.net/xml/2011/echo/12",
  "xmlns:amep"        => "http://ddex.net/xml/2011/amep/12",
  "xmlns:xs"          => "http://www.w3.org/2001/XMLSchema-instance",
  "xmlns:ds"          => "http://www.w3.org/2000/09/xmldsig#"
}
xml.instruct!(:xml, version: "1.0", encoding: "utf-8", standalone: "yes")

xml.tag!("echo:ManifestMessage", tag_options) do |echo|
  echo.MessageHeader do |message_header|
    render "manifest_header", self, xml: message_header, recipient: recipient_info
  end
  echo.IsTestFlag false
  echo.RootDirectory remote_dir
  echo.NumberOfMessages messages_count
    batch_items.each do |delivery_batch_item|
    render "message_in_batch", self, xml: echo, item: delivery_batch_item
  end
end
