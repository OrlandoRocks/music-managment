xml.ReleaseResourceReferenceList do |reference_list|
  tracks.each do |track|
    reference_list.ReleaseResourceReference( "A#{track.number}", {'ReleaseResourceType'=>'PrimaryResource'})
  end
  reference_list.ReleaseResourceReference( "A#{tracks.size + 1}",{'ReleaseResourceType'=>'SecondaryResource'})
  if deliver_booklet && booklet_file && booklet_file[:asset]
    reference_list.ReleaseResourceReference( "A#{tracks.size + 2}",{'ReleaseResourceType'=>'SecondaryResource'})
  end
end


