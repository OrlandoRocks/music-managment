contributors_for("resource").each do |contributor|
    xml.ResourceContributor do |resource_contributor|
      resource_contributor.PartyName do |party_name|
        party_name.FullName contributor[:name]
      end
      artist = artists.select {|artist| artist.name == contributor[:name]}.first
      if contributor[:role] == 'Remixer' && artist.present? && artist.spotify_id.present?
        render("artists/spotify/artist_detail", artist, xml: resource_contributor) 
      end
      if contributor[:user_defined]
        resource_contributor.ResourceContributorRole(
          {
            "UserDefinedValue" => contributor[:role],
            "Namespace"        => party_id
          },
          'UserDefined'
        )
      else
        resource_contributor.ResourceContributorRole contributor[:role]
      end
    end
  end
  