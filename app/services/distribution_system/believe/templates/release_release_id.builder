xml.ReleaseId do |release_id|
  release_id.ISRC track.isrc
  release_id.ProprietaryId("#{upc}_01_#{track.number.to_s.rjust(3, "0")}", {"Namespace" => "DPID:#{party_id}"}) if use_proprietary_id
end
