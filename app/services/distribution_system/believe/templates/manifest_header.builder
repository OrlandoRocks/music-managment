xml.MessageSender do |message_sender|
  message_sender.party_id DDEX_SENDER_ID
  message_sender.PartyName do |party_name|
    party_name.FullName DDEX_SENDER_NAME
  end
end
xml.MessageRecipient do |message_recipient|
  message_recipient.PartyId recipient[:id]
  message_recipient.PartyName do |party_name|
    party_name.FullName recipient[:name]
  end
end
xml.MessageCreatedDateTime xml_timestamp
