xml.ReleaseDetailsByTerritory do |release_details|
  release_details.TerritoryCode "Worldwide"
  release_details.DisplayArtistName artist_display_name if show_display_name? && supports_multiple_roles
  release_details.LabelName label_name if label_name
  release_details.Title("TitleType"=>"FormalTitle") do |_title|
    _title.TitleText title
  end
  release_details.Title("TitleType"=>"DisplayTitle") do |_title|
    _title.TitleText title
  end
  render_partial "release_artists", self, xml: release_details
  release_details.IsMultiArtistCompilation is_various
  release_details.ParentalWarningType explicit_lyrics ? 'Explicit' : 'NotExplicit'
  release_details.ResourceGroup do |resource_group1|
    resource_group1.ResourceGroup do |resource_group2| #ugh
      resource_group2.SequenceNumber "1"
      tracks.each do |track|
        resource_group2.ResourceGroupContentItem do |content_item|
          content_item.SequenceNumber track.number
          content_item.ResourceType 'SoundRecording'
          content_item.ReleaseResourceReference "A#{track.number}"
        end
      end
    end # resource group 2
    resource_group1.ResourceGroupContentItem do |content_item| #artwork
      content_item.SequenceNumber (tracks.size + 1)
      content_item.ResourceType 'Image'
      content_item.ReleaseResourceReference "A#{tracks.size + 1}"
    end
    if deliver_booklet && booklet_file && booklet_file[:asset]
      resource_group1.ResourceGroupContentItem do |content_item| #booklet
        content_item.SequenceNumber (tracks.size + 2)
        content_item.ResourceType 'Text'
        content_item.ReleaseResourceReference "A#{tracks.size + 2}"
      end
    end
  end # resource group 1
  release_details.Genre do |genre_xml|
    if is_classical_genre?
      genre_xml.GenreText "Classical"
    else
      genre_xml.GenreText DistributionSystem::Believe::GenreHelper.get_genre(genre, store_name)
    end
  end
  release_details.OriginalReleaseDate release_date
end
