xml.MessageInBatch do |message|
  message.MessageType "NewReleaseDelivery"
  message.MessageId item.uuid
  message.URL item.xml_remote_path
  message.IncludedReleaseId do |id|
        id.ICPN(item.upc, "IsEan" => "false")
  end
  message.DeliveryType item.takedown ? "TakeDown" : "NewReleaseDelivery"
  message.ProductType "AudioProduct"
  message.HashSum do |hash_sum|
    hash_sum.HashSum item.xml_hashsum
    hash_sum.HashSumAlgorithmType "MD5"
  end
end