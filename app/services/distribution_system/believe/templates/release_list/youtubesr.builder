track = tracks.first
xml.Release do |release|
  render "release_release_id", self, xml: release, track: track
  release.ReleaseReference "R1"
  render "release_reference_title", self, xml: release, track: track
  render_partial "release_resource_reference_list", self, xml: release, track: track
  release.ReleaseType "SingleResourceRelease"
  render_partial "album_details_by_territory", self, xml: release, track: track
  render "pline", self, xml: release
  render "cline", self, xml: release
end # release
