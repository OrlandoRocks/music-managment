render "release", self, xml: xml
tracks.each do |track|
  xml.Release do |release|
    render "release_release_id", self, xml: release, track: track
    release.ReleaseReference "R#{track.number}"
    render "release_reference_title", self, xml: release, track: track
    render "reference_list", self, xml: release, track: track
    render_partial "release_type/track", self, xml: release
    render_partial "album_details_by_territory", self, xml: release, track: track
    render "pline", self, xml: release
    render "cline", self, xml: release
  end # release
end # tracks.each
