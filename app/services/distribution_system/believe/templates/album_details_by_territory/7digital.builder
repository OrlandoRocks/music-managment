xml.ReleaseDetailsByTerritory do |details_by_territory|
  details_by_territory.TerritoryCode 'Worldwide'
  details_by_territory.DisplayArtistName track.artist_display_name if track.show_display_name? && track.supports_multiple_roles
  details_by_territory.LabelName label_name if label_name
  details_by_territory.Title({'TitleType' => 'FormalTitle'}) do |title|
    title.TitleText track.title
  end # title
  details_by_territory.Title({'TitleType' => 'DisplayTitle'}) do |title|
    title.TitleText track.title
  end # title
  render_partial("artists", track, xml: details_by_territory)
  details_by_territory.ParentalWarningType track.explicit_lyrics ? 'Explicit' : 'NotExplicit'
  details_by_territory.ResourceGroup do |resource_group|
    resource_group.SequenceNumber track.number
    resource_group.ResourceGroupContentItem do |content_item|
      content_item.SequenceNumber track.number
      content_item.ResourceType 'SoundRecording'
      content_item.ReleaseResourceReference({'ReleaseResourceType' => 'PrimaryResource'}, "A#{track.number}")
    end # content_item
  end #resource_group
  details_by_territory.Genre do |genre_xml|
    genre_xml.GenreText DistributionSystem::SevenDigital::GenreHelper.get_genre(genre)
  end #genre
  details_by_territory.OriginalReleaseDate release_date
end
