xml.ReleaseDetailsByTerritory do |details_by_territory|
  details_by_territory.TerritoryCode 'Worldwide'
  details_by_territory.DisplayArtistName artist_display_name if show_display_name? && supports_multiple_roles
  details_by_territory.LabelName label_name if label_name
  details_by_territory.Title({'TitleType' => 'FormalTitle'}) do |title|
    title.TitleText track.title
  end # title
  details_by_territory.Title({'TitleType' => 'DisplayTitle'}) do |title|
    title.TitleText track.title
  end # title
  render_partial("artists", self, xml: details_by_territory)
  details_by_territory.ParentalWarningType track.explicit_lyrics ? 'Explicit' : 'NotExplicit'
  details_by_territory.ResourceGroup do |resource_group|
    resource_group.SequenceNumber track.number
    resource_group.ResourceGroupContentItem do |content_item|
      content_item.SequenceNumber track.number
      content_item.ResourceType 'SoundRecording'
      content_item.ReleaseResourceReference "A#{track.number}"
    end # content_item
  end #resource_group
  details_by_territory.Genre do |genre_xml|
    genre_xml.GenreText DistributionSystem::Believe::GenreHelper.get_genre(genre, store_name)
  end #genre
  details_by_territory.OriginalReleaseDate release_date
end
