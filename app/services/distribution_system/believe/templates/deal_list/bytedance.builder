xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference 'R0'
    release_deal.Deal do |deal|
      deal.DealTerms do |deal_terms|
        deal_terms.CommercialModelType 'AdvertisementSupportedModel'
        deal_terms.CommercialModelType 'SubscriptionModel'
        deal_terms.TakeDown 'true' if takedown
        render_partial "deal_list/usage", self, xml: deal_terms if !takedown
        render_partial "deal_list/countries", self, xml: deal_terms
        render_partial "deal_list/channels", self, xml: deal_terms if channels
        render_partial "deal_list/price_information", self, xml: deal_terms
        render_partial "deal_list/validity_period", self, xml: deal_terms
        deal_terms.ReleaseDisplayStartDateTime "#{actual_release_date}T00:00:00"
        deal_terms.TrackListingPreviewStartDateTime "#{actual_release_date}T00:00:00"
        deal_terms.CoverArtPreviewStartDateTime "#{actual_release_date}T00:00:00"
        deal_terms.ClipPreviewStartDateTime "#{actual_release_date}T00:00:00"
      end
    end
  end
  tracks.each do |track|
    if !track.album_only
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            deal_terms.CommercialModelType 'AdvertisementSupportedModel'
            deal_terms.CommercialModelType 'SubscriptionModel'
            deal_terms.TakeDown 'true' if takedown
            render_partial "deal_list/usage", self, xml: deal_terms if !takedown
            render_partial "deal_list/countries", self, xml: deal_terms
            render_partial "deal_list/channels", self, xml: deal_terms if channels
            render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: track_price_code, namespace: "DPID:#{DDEX_SENDER_ID}"}
            render_partial "deal_list/validity_period", self, xml: deal_terms
            deal_terms.ReleaseDisplayStartDateTime "#{actual_release_date}T00:00:00"
            deal_terms.TrackListingPreviewStartDateTime "#{actual_release_date}T00:00:00"
            deal_terms.CoverArtPreviewStartDateTime "#{actual_release_date}T00:00:00"
            deal_terms.ClipPreviewStartDateTime "#{actual_release_date}T00:00:00"
          end
        end
      end
    end
  end
end
