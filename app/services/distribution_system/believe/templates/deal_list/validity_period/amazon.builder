xml.ValidityPeriod do |validity_period|
  if prime
    validity_period.StartDate (actual_release_date + 90)
  else
    validity_period.StartDate actual_release_date
  end
end
xml.PreOrderReleaseDate actual_release_date
xml.PreOrderPreviewDate actual_release_date
