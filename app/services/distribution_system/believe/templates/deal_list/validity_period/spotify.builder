xml.ValidityPeriod do |validity_period|
# temporary hack, user-restricted feature for Spotify Gold requirement demo
  if has_track_takedowns?
    if takedown && track.try(:number) == 1
      validity_period.EndDateTime takedown_at
    else
      validity_period.StartDateTime actual_release_date
    end
  elsif takedown
    validity_period.EndDateTime takedown_at
  else
    validity_period.StartDateTime actual_release_date
  end
end
