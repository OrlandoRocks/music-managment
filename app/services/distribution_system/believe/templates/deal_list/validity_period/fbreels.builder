xml.ValidityPeriod do |validity_period|
  if takedown
    validity_period.EndDate actual_release_date
  else
    validity_period.StartDate actual_release_date
  end
end
