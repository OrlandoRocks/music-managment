xml.ValidityPeriod do |validity_period|
  if takedown
    validity_period.EndDate takedown_at
  else
    validity_period.StartDate actual_release_date
  end
end
