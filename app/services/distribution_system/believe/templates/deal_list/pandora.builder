xml.DealList do |deal_list|
  deal_structures = Proc.new do |release_deal|
    common_use_types  = ["NonInteractiveStream", "OnDemandStream"]
    structures = [
      { commercial_model: "AdvertisementSupportedModel",  use_types: common_use_types  },
      { commercial_model: "SubscriptionModel",            use_types: ["ConditionalDownload"] + common_use_types }]

    rendered_structures = structures.each do |deal_structure|
      release_deal.Deal do |deal|
        deal.DealTerms do |deal_terms|
          deal_terms.CommercialModelType deal_structure[:commercial_model]
          if takedown
            deal_terms.TakeDown 'true'
          else
            deal_terms.Usage do |usage|
              deal_structure[:use_types].each do |use_type|
                usage.UseType use_type
              end
            end
          end
          render_partial "deal_list/countries", self, xml: deal_terms
          render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: price_code, namespace: "DPID:#{DDEX_SENDER_ID}"}
          render_partial "deal_list/validity_period", self, xml: deal_terms
        end
      end
    end

    rendered_structures
  end

  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference "R0"
    deal_structures.call(release_deal)
  end

  tracks.each do |track|
    if !track.album_only
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"
        deal_structures.call(release_deal)
      end
    end
  end
end
