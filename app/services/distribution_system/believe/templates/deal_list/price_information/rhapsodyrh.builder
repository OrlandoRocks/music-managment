xml.PriceInformation do |price_information|
  if tracks.size < 10
    price_information.WholesalePricePerUnit((tracks.size * 0.70).round(2), {"CurrencyCode" => 'USD'})
  else
    price_information.WholesalePricePerUnit("8.00", {"CurrencyCode" => 'USD'})
  end
end
