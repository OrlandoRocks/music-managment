xml.PriceInformation do |price_information|
  if album_type == "Single" || use_track_price
    price_information.PriceType(track_price_code, {"Namespace"=>"DPID:#{party_id}"})
  else
    price_information.PriceType(album_price_code, {"Namespace"=>"DPID:#{party_id}"})
  end
end
