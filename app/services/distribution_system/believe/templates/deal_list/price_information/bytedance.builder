xml.PriceInformation do |price_information|
  if album_type == "Single"
    price_information.PriceType(track_price_code, {"Namespace"=>"DPID:#{DDEX_SENDER_ID}"})
  else
    price_information.PriceType(album_price_code, {"Namespace"=>"DPID:#{DDEX_SENDER_ID}"})
  end
end
