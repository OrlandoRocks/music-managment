xml.PriceInformation do |price_information|
  if free_track
    price_information.PriceType("PROMOTIONAL", {"Namespace"=>"DPID:#{party_id}"})
  else
    price_information.PriceType(wholesale_price_tier, {"Namespace"=>"DPID:#{party_id}"})
  end
end
