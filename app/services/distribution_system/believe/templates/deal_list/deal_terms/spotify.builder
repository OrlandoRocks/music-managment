xml.CommercialModelType type
render_partial "deal_list/usage", self, xml: xml, conditional_download: conditional_download, can_stream: can_stream
# temporary hack, user-restricted feature for Spotify Gold requirement demo
if use_track_price && try(:has_track_level_territories?)
  countries = self.countries
  countries -= ['US'] if try(:has_no_us_track_territory?) && track.try(:number) == 1
  render_partial "deal_list/countries", self, xml: xml, countries: countries
else
  render_partial "deal_list/countries", self, xml: xml
end
render_partial "deal_list/price_information", self, xml: xml, use_track_price: use_track_price
render_partial "deal_list/validity_period", self, xml: xml, track: track
