xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference 'R0'
    release_deal.Deal do |deal|
      deal.DealTerms do |deal_terms|
        deal_terms.CommercialModelType 'AsPerContract'
        render_partial "deal_list/usage", self, xml: deal_terms
        if takedown
          deal_terms.TerritoryCode "Worldwide"
        else
          render_partial "deal_list/countries", self, xml: deal_terms
        end
        render_partial "deal_list/validity_period", self, xml: deal_terms
      end
    end
  end
  tracks.each do |track|
    if !track.album_only
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            deal_terms.CommercialModelType 'AsPerContract'
            render_partial "deal_list/usage", self, xml: deal_terms
            if takedown
              deal_terms.TerritoryCode "Worldwide"
            else
              render_partial "deal_list/countries", self, xml: deal_terms
            end
            render_partial "deal_list/validity_period", self, xml: deal_terms
          end
        end
      end
    end
  end
end
