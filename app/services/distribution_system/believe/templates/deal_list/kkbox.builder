xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference 'R0'
    if countries && countries.length > 0
      release_deal.Deal do |deal|
        deal.DealTerms do |deal_terms|
          deal_terms.CommercialModelType 'PayAsYouGoModel'
          if takedown
            deal_terms.TakeDown 'true'
          else
            deal_terms.Usage do |usage|
              usage.UseType 'PermanentDownload'
            end # usage
          end

          render_partial "deal_list/countries", self, xml: deal_terms
          render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: "UNPRICED", namespace: party_id}
          render_partial "deal_list/validity_period", self, xml: deal_terms
        end# deal_terms
      end #deal 1
    end
    if countries && countries.length > 0
      release_deal.Deal do |deal|
        deal.DealTerms do |deal_terms|
          deal_terms.CommercialModelType 'SubscriptionModel'

          if takedown
            deal_terms.TakeDown 'true'
          else
            deal_terms.Usage do |usage|
              usage.UseType 'ConditionalDownload'
              usage.UseType 'OnDemandStream'
            end #usage
          end

          render_partial "deal_list/countries", self, xml: deal_terms
          render_partial "deal_list/validity_period", self, xml: deal_terms
        end
      end #deal 2
    end
    release_deal.EffectiveDate actual_release_date
  end # release_deal

  # Track deals
  tracks.each do |track|
    deal_list.ReleaseDeal do |release_deal|
      release_deal.DealReleaseReference "R#{track.number}"
      if countries && countries.length > 0
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            deal_terms.CommercialModelType 'PayAsYouGoModel'

            if takedown
              deal_terms.TakeDown 'true'
            else
              deal_terms.Usage do |usage|
                usage.UseType 'PermanentDownload'
              end # usage
            end

            render_partial "deal_list/countries", self, xml: deal_terms
            render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: "UNPRICED", namespace: party_id}
            render_partial "deal_list/validity_period", self, xml: deal_terms
          end# deal_terms
        end #deal 1
      end

      if countries && countries.length > 0
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            deal_terms.CommercialModelType 'SubscriptionModel'

            if takedown
              deal_terms.TakeDown 'true'
            else
              deal_terms.Usage do |usage|
                usage.UseType 'ConditionalDownload'
                usage.UseType 'OnDemandStream'
              end #usage
            end

            render_partial "deal_list/countries", self, xml: deal_terms
            render_partial "deal_list/validity_period", self, xml: deal_terms
          end
        end #deal 2
      end
      release_deal.EffectiveDate actual_release_date
    end # release_Deal
  end # tracks.each
end # deal_list
