xml.CommercialModelType usage_hash[:commercial_model]
if takedown
  xml.TakeDown 'true'
else
  xml.Usage do |usage|
    if usage_hash[:use_type] == "PrimeOnDemandStream"
      usage.UseType("UserDefined", { "UserDefinedValue" => usage_hash[:use_type] })
    else
      usage.UseType usage_hash[:use_type]
    end
  end
end
