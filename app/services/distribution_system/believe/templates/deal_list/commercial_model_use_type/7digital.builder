xml.CommercialModelType usage_hash[:commercial_model]
xml.Usage do |usage|
  usage.UseType usage_hash[:use_type]
  usage.TechnicalInstantiation do |ti|
    ti.DrmEnforcementType "NotDrmEnforced"
  end
end
