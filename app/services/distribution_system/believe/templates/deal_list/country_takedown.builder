xml.Deal do |deal|
  deal.DealTerms do |deal_terms|
    deal_terms.TakeDown "true"
    takedown_countries.each do |country|
      deal_terms.TerritoryCode country
    end
    deal_terms.ValidityPeriod do | validity_period |
      validity_period.StartDate actual_release_date
    end
  end
end
