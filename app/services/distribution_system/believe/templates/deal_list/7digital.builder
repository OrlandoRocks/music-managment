xml.DealList do |deal_list|
  commercial_model_usages.each do |usage_hash|
    if usage_hash[:use_type] != "Stream"
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference 'R0'
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            deal_terms.IsPreOrderDeal false
            render_partial "deal_list/commercial_model_use_type", self, xml: deal_terms, usage_hash: usage_hash, streaming: true
            render_partial "deal_list/countries", self, xml: deal_terms
            render_partial "deal_list/price_information", self, xml: deal_terms
            render_partial "deal_list/validity_period", self, xml: deal_terms
          end
        end
      end
    end
  end
  tracks.each do |track|
    if !track.album_only
      commercial_model_usages.each do |usage_hash|
        deal_list.ReleaseDeal do |release_deal|
          release_deal.DealReleaseReference "R#{track.number}"
          release_deal.Deal do |deal|
            deal.DealTerms do |deal_terms|
              render_partial "deal_list/commercial_model_use_type", track, xml: deal_terms, usage_hash: usage_hash, streaming: track.streaming, takedown: takedown
              render_partial "deal_list/countries", self, xml: deal_terms
              render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: track_price_code, namespace: "DPID:#{party_id}"} unless usage_hash[:use_type] == "Stream"
              render_partial "deal_list/validity_period", self, xml: deal_terms
            end
          end
        end
      end
    end
  end
end
