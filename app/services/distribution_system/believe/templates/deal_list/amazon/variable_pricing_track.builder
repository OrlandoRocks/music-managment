
tracks.each do |track|
  if takedown_countries.present?
    xml.ReleaseDeal do | release_deal |
      release_deal.DealReleaseReference "R#{track.number}"
      render "deal_list/country_takedown", self, xml: release_deal
    end
  end

  if !track.album_only
    commercial_model_usages.each do |usage_hash|
      xml.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            render "deal_list/commercial_model_use_type/amazon", self, xml: deal_terms, usage_hash: usage_hash
            all_products.each do |prod|
              deal_terms.TerritoryCode prod.territory
            end

            if usage_hash[:use_type] == "PermanentDownload"
              render "deal_list/price_information/amazon_track", self, xml: deal_terms, wholesale_price_tier: track.wholesale_price_tier, free_track: track.free_song
            end

            render "deal_list/validity_period/default", self, xml: deal_terms, prime: usage_hash[:use_type] == "PrimeOnDemandStream"
          end
        end
      end
    end
  end
end
