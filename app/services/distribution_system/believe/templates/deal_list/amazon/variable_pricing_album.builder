if takedown_countries.present?
  render "deal_list/country_takedown", self, xml: xml
end
all_products.each do |product|
  commercial_model_usages.each do |usage_hash|
    xml.Deal do |deal|
      deal.DealTerms do |deal_terms|
        render "deal_list/commercial_model_use_type/amazon", self, xml: deal_terms, usage_hash: usage_hash
        deal_terms.TerritoryCode product.territory
        if usage_hash[:use_type] == "PermanentDownload"
          render "deal_list/price_information/amazon_album", self, xml: deal_terms, wholesale_price_tier: product.wholesale_price_tier
        end
        render "deal_list/validity_period/amazon", self, xml: deal_terms, prime: usage_hash[:use_type] == "PrimeOnDemandStream"
      end
    end
  end
end
