xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference "R1"
    release_deal.Deal do |deal|
      deal.DealTerms do |deal_terms|
        deal_terms.CommercialModelType "RightsClaimModel"
        deal_terms.Usage do |usage|
          usage.UseType "UserMakeAvailableUserProvided"
        end
        if takedown
          deal_terms.TerritoryCode "Worldwide"
        else
          render_partial "deal_list/countries", self, xml: deal_terms
        end
        deal_terms.ValidityPeriod do |validity_period|
          if takedown
            validity_period.EndDate actual_release_date
          else
            validity_period.StartDate actual_release_date
          end
        end
        unless takedown
          deal_terms.RightsClaimPolicy do |rights_policy|
            rights_policy.Condition do |condition|
              condition.Value 10
              condition.Unit "Percent"
              condition.RelationalRelator "MoreThanOrEqualTo"
            end
            rights_policy.RightsClaimPolicyType "Monetize"
          end
        end
      end
    end
  end
end
