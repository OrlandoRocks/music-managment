xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference 'R0'
    render "deal_list/amazon/variable_pricing_album", self, xml: release_deal
  end
  render "deal_list/amazon/variable_pricing_track", self, xml: deal_list
end
