xml.Usage do |usage|
  usage.UseType "OnDemandStream"
  usage.UseType "PermanentDownload"
  usage.TechnicalInstantiation do |ti|
    ti.DrmEnforcementType "NotDrmEnforced"
  end
end
