xml.Usage do |usage|
  usage.UseType "PermanentDownload"
  usage.UseType "OnDemandStream"
  usage.UseType "NonInteractiveStream"
  usage.DistributionChannelType "InternetAndMobile"
end
