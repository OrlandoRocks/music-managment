xml.Usage do |usage|
  usage.UseType "ConditionalDownload" if conditional_download
  usage.UseType "Stream" if can_stream
end
