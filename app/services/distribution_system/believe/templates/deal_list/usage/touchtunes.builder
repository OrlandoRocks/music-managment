xml.Usage do |usage|
  usage.UseType "ConditionalDownload"
  usage.UseType "NonInteractiveStream"
  usage.UseType "OnDemandStream"
end
