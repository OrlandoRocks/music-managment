xml.Usage do |usage|
  usage.UseType "ConditionalDownload"
  usage.UseType "Stream"
  usage.TechnicalInstantiation do |ti|
    ti.DrmEnforcementType "NotDrmEnforced"
  end
end
