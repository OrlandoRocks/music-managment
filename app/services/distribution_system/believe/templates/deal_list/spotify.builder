xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference 'R0'
    release_deal.Deal do |deal|
      deal.DealTerms do |deal_terms|
        render_partial "deal_list/deal_terms", self,
          can_stream: available_for_streaming,
          xml: deal_terms,
          type: "SubscriptionModel",
          conditional_download: true,
          use_track_price: false,
          track: nil
      end
    end
    if available_for_streaming
      release_deal.Deal do |deal|
        deal.DealTerms do |deal_terms|
          render_partial "deal_list/deal_terms", self,
            can_stream: true,
            xml: deal_terms,
            type: "AdvertisementSupportedModel",
            conditional_download: false,
            use_track_price: false,
            track: nil
        end
      end
    end
  end
  tracks.each do |track|

    if !track.album_only
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"
        release_deal.Deal do |deal|
          deal.DealTerms do |deal_terms|
            render_partial "deal_list/deal_terms", self,
            can_stream: track.available_for_streaming,
            xml: deal_terms,
            type: "SubscriptionModel",
            conditional_download: true,
            use_track_price: true,
            track: track
          end
        end
        if track.available_for_streaming
          release_deal.Deal do |deal|
            deal.DealTerms do |deal_terms|
              render_partial "deal_list/deal_terms", self,
              can_stream: true,
              xml: deal_terms,
              type: "AdvertisementSupportedModel",
              conditional_download: false,
              use_track_price: true,
              track: track
            end
          end
        end
      end
    end
  end
end
