xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference "R0"

    [ { commercial_model: "PayAsYouGoModel",  use_type: "PermanentDownload" },
      { commercial_model: "SubscriptionModel", use_type: "ConditionalDownload" },
      { commercial_model: "SubscriptionModel", use_type: "OnDemandStream" },
      { commercial_model: "SubscriptionModel", use_type: "NonInteractiveStream" }
    ].each do |deal_structure|
      release_deal.Deal do |deal|
        deal.DealTerms do |deal_terms|
          deal_terms.CommercialModelType deal_structure[:commercial_model]
          if takedown
            deal_terms.TakeDown 'true'
          else
            deal_terms.Usage { |usage| usage.UseType deal_structure[:use_type] }
          end
          render_partial "deal_list/countries", self, xml: deal_terms
          render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: price_code, namespace: "DPID:#{DDEX_SENDER_ID}"}
          render_partial "deal_list/validity_period", self, xml: deal_terms

        end
      end
    end
  end

  # Track deals
  tracks.each do |track|
    if !track.album_only
      deal_list.ReleaseDeal do |release_deal|
        release_deal.DealReleaseReference "R#{track.number}"

      [ { commercial_model: "PayAsYouGoModel",  use_type: "PermanentDownload" },
        { commercial_model: "SubscriptionModel", use_type: "ConditionalDownload" },
        { commercial_model: "SubscriptionModel", use_type: "OnDemandStream" },
        { commercial_model: "SubscriptionModel", use_type: "NonInteractiveStream" }
      ].each do |deal_structure|
          release_deal.Deal do |deal|
            deal.DealTerms do |deal_terms|
              deal_terms.CommercialModelType deal_structure[:commercial_model]
              if takedown
                deal_terms.TakeDown 'true'
              else
                deal_terms.Usage { |usage| usage.UseType deal_structure[:use_type] }
              end

              render_partial "deal_list/countries", self, xml: deal_terms
              render_partial "deal_list/price_range_information", self, {xml: deal_terms, price_code: track_price_code, namespace: "DPID:#{DDEX_SENDER_ID}"}
              render_partial "deal_list/validity_period", self, xml: deal_terms
            end
          end
        end
      end
    end
  end
end
