xml.DealList do |deal_list|
  deal_list.ReleaseDeal do |release_deal|
    release_deal.DealReleaseReference "R1"
    release_deal.Deal do |deal|
      if takedown
        deal.DealTerms do |deal_terms|
          deal_terms.Usage do |usage|
            usage.UseType "Stream"
          end
          render_partial "deal_list/countries", self, xml: deal_terms
          deal_terms.ValidityPeriod do |validity_period|
            validity_period.EndDate actual_release_date
          end
        end
      else
        deal.DealTerms do |deal_terms|
          deal_terms.CommercialModelType "RightsClaimModel"
          deal_terms.Usage do |usage|
            usage.UseType "UserMakeAvailableUserProvided"
          end
          render_partial "deal_list/countries", self, xml: deal_terms
          deal_terms.ValidityPeriod do |validity_period|
            validity_period.StartDate actual_release_date
          end
          deal_terms.RightsClaimPolicy do |rights_policy|
            rights_policy.RightsClaimPolicyType "Monetize"
          end
        end
      end
    end
  end
end