module DistributionSystem::Transcodable
  def transcode_track(track)
    transcoder.transcode(original_file(track), track.audio_file, transcoding_params(track))
  end

  def transcoding_params(track)
    if preset
      { preset: preset }
    else
      {
        normalize_bit_depth: convert_track_bit_depth?(track),
        bit_depth: target_bit_depth
      }
    end
  end

  def convert_track_bit_depth?(track)
    bit_depth = track_bit_depth(track).to_i

    if album.normalize_bit_depth
      bit_depth != album.bit_depth
    else
      bit_depth.zero? || (track_encoding(track.audio_file) == ".flac" && bit_depth > 24)
    end
  end

  def track_bit_depth(track)
    transcoder.get_bit_depth(File.join(@dirname, track.temp_filename))
  end

  def track_encoding(filename)
    File.extname(filename)
  end

  def original_file(track)
    File.join(@dir.path, track.temp_filename)
  end

  def target_bit_depth
    album.bit_depth || 16
  end
end
