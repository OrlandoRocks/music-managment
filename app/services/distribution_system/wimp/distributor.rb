module DistributionSystem::Wimp
  class Distributor < DistributionSystem::Believe::Distributor
    def do_send_bundle
      @remote_server.connect do |server|
        server.mkdir_p @bundle.batch_dir
        server.mkdir_p(@bundle.remote_file_path)
        server.mkdir_p(@bundle.resource_dir) if full_delivery? && @bundle.use_resource_dir
        @bundle.resources_for(@bundle.album.delivery_type).each do |resource|
          server.upload(resource[:local_file], resource[:remote_file])
        end

        send_complete_file(server)
      end
    rescue DistributionSystem::DeliveryError => e
      Rails.logger.error "Error during distribution of #{@bundle.album.upc}, Error Message: #{e.message} #{@bundle.album}"
      Rails.logger.error e.backtrace.join('\n')
      raise e
    end

    def send_complete_file(server)
      local_complete_file = FileUtils.touch(File.join(Rails.root, "tmp", "delivery.complete")).first
      server.upload(local_complete_file, "#{@bundle.remote_file_path}/delivery.complete")
    end

    def full_delivery?
      @bundle.album.delivery_type == "full_delivery"
    end
  end
end
