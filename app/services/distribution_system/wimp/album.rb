module DistributionSystem::Wimp
  class Album < DistributionSystem::Believe::Album
    xml_format :believe
    xml_validation_with :ddex
  end
end
