module DistributionSystem::Wimp
  class Converter < DistributionSystem::Believe::Converter
    description "Wimp Music"
    handle_salepoints(/^Wimp$/)

    ALBUM_KLASS = DistributionSystem::Wimp::Album
  end
end
