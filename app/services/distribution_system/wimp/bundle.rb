module DistributionSystem::Wimp
  class Bundle < DistributionSystem::Believe::Bundle
    def batch_dir
      "#{@remote_dir}/TUNECORE_WIMP_#{timestamp}"
    end

    def remote_file_path
      bundle_id = album.upc
      "#{batch_dir}/UPC#{bundle_id}"
    end

    def resource_dir
      "#{remote_file_path}/resources"
    end

    def timestamp
      Time.now.strftime("%Y%m%d")
    end
  end
end
