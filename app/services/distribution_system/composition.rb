module DistributionSystem
  class Composition
    attr_accessor :composition_id,
                  :title,
                  :composers,
                  :iswc,
                  :delivery_type,
                  :mech_collect_share,
                  :sync_right,
                  :mech_right,
                  :perf_right,
                  :song_isrc,
                  :song_code

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end
  end
end
