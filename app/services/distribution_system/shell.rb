class DistributionSystem::Shell
  def command(command = nil, *args)
    # lame command doesn't set the correct exit status when there's an error
    lame = command.include? "lame"
    shell_command = [command, args].join(" ")

    Rails.logger.info shell_command

    stdout, stderr = "", ""
    status = open4.spawn shell_command, ignore_exit_failure: true, timeout: 600, stdout: stdout, stderr: stderr

    if status.exitstatus != 0 or (lame and !stderr.empty?)
      raise RuntimeError.new("Command: #{shell_command}\nError output: #{stderr}, stdout: #{stdout}")
    end

    Rails.logger.debug "stdout: #{stdout}, stderr: #{stderr}"
  end
end
