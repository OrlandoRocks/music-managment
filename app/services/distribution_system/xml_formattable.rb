module DistributionSystem::XmlFormattable
  def self.included(base)
    base.extend ClassMethods
  end

  def to_xml
    formatter.format
  end

  def render(file_name, object, options = {})
    formatter.render(file_name, object, options)
  end

  def render_partial(filename, object, options = {})
    formatter.render_partial(filename, object, options)
  end

  def validate_xml
    formatter.valid?
  end

  def formatter
    @formatter ||= DistributionSystem::XmlFormatter.new(self)
  end

  def xml_validation_errors
    formatter.validation_errors
  end

  module ClassMethods
    @xml_format  = :believe
    @schema_type = :ddex

    def format
      @xml_format
    end

    def xml_format(name)
      @xml_format = name
    end

    def schema_file(version = nil)
      "#{@schema_type}#{version}.xsd"
    end

    def xml_validation_with(schema_type)
      @schema_type = schema_type
    end
  end
end
