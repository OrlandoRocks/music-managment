module DistributionSystem::SevenDigital
  class Converter < DistributionSystem::Believe::Converter
    description "SevenDigital"
    handle_salepoints(/^7Digital$/)  # this must match the short_name in the stores table!! WTF!

    ALBUM_KLASS  = DistributionSystem::SevenDigital::Album
    TRACK_KLASS  = DistributionSystem::SevenDigital::Track
    ARTIST_KLASS = DistributionSystem::SevenDigital::Artist

    def initialize
      @supports_metadata_update = true
      @supports_takedown        = true
    end

    def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
      build_album(tunecore_album, tc_salepoints, delivery_type)
      add_tracks(tunecore_album)
      album
    end

    def build_album(tunecore_album, tc_salepoints, delivery_type)
      super
      @album.store_id       = tc_salepoints.pluck(:store_id).first
      @album.album_type     = set_album_type(tunecore_album)
      @album.language_code  = tunecore_album.language_code
      @album.takedown_at    = format_takedown_at_date(tunecore_album) if @album.takedown
    end

    def add_tracks(tunecore_album)
      tunecore_album.songs.each do |song|
        @album.tracks << build_track(album, song)
      end
    end

    def build_track(album, song)
      super(album, song)
      @track.number          = song.track_num.to_s.rjust(3, "0")
      @track.streaming       = song.available_for_streaming?
      @track.label_name      = album.label_name
      @track.copyright_pline = album.copyright_pline
      @track
    end

    def set_album_type(tunecore_album)
      case tunecore_album.class.name
      when "Ringtone", "Single"
        "Single"
      when "Album"
        tunecore_album.class.name.to_s
      else
        ""
      end
    end
  end
end
