module DistributionSystem::SevenDigital
  class Bundle < DistributionSystem::Believe::Bundle
    def initialize(work_dir, transcoder, s3, album, store_delivery_config)
      super
      @send_batch_complete = false
      @timestamp = Time.now.strftime("%Y%m%d%H%M%S")
    end

    def remote_file_path
      if remote_dir_timestamped
        "#{remote_dir}/#{album.upc}_#{@timestamp}"
      else
        album.upc.to_s
      end
    end
  end
end
