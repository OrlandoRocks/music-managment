module DistributionSystem::SevenDigital
  class Album < DistributionSystem::Believe::Album
    xml_format :believe
    xml_validation_with :ddex
    attr_accessor :vendor_code

    def set_data_from_config(store_delivery_config)
      super(store_delivery_config)
      album_price_code = store_delivery_config["album_price_code"] || "TUALFR"
      track_price_code = store_delivery_config["track_price_code"] || "TUSIMI"
    end

    def commercial_model_usages
      [
        { commercial_model: "PayAsYouGoModel",   use_type: "PermanentDownload" },
        { commercial_model: "SubscriptionModel", use_type: "Stream" },
        { commercial_model: "AdvertisementSupportedModel", use_type: "Stream" }
      ]
    end
  end
end
