module DistributionSystem::SevenDigital
  class Track < DistributionSystem::Believe::Track
    include DistributionSystem::XmlFormattable
    xml_format :believe
    xml_validation_with :ddex

    attr_accessor :length, :streaming
  end
end
