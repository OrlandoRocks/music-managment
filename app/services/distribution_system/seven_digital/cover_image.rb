module DistributionSystem::SevenDigital
  class CoverImage
    attr_accessor :asset

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end
  end
end
