require_relative "../shell"
require "builder"
module DistributionSystem::Muve
  class MuveStore
    attr_accessor :muve_provider

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No muve directory!"
      @muve_dir = File.join(@work_dir, "Muve")
      @logger = options[:logger] or raise "No logger!"
      @muve_server = options[:muve_server] or raise "No Muve server!"
    end

    def validate_bundle(bundle)
      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"))
    end

    def send_bundle(bundle)
      bundle_id = bundle.album.upc
      date = Time.now.strftime("%Y%m%d")
      @muve_server.mkdir_p "tunecore/#{date}"
      remote_dir = "tunecore/#{date}/" + bundle_id

      @muve_server.mkdir_p(remote_dir)
      if bundle.album.delivery_type == "full_delivery"
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(remote_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @muve_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_01_#{track.number}.flac")
          remote_track_file = File.join(remote_dir, "#{bundle_id}_01_#{track.number}.flac")
          @muve_server.upload(track_file, remote_track_file)
        end
      end

      @logger.info "  uploading metatata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(remote_dir, "#{bundle_id}.xml")
      @muve_server.upload(metadata_filename, remote_metadata_filename)

      # upload delivery complete
      delivery_complete = File.join(bundle.dirname.to_s, "delivery.complete")
      File.open(delivery_complete, "a")
      remote_delivery_complete = File.join(remote_dir, "delivery.complete")
      @muve_server.upload(delivery_complete, remote_delivery_complete)

      @muve_server.start_processing

      true
    end
  end
end
