class DistributionSystem::Muve::Album < DistributionSystem::Album
  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :logger

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.delivery(xmlns: "http://www.cricket.com/schema/muve_standard_v1.xsd") do |delivery|
      delivery.tag!("reporting-identifiers") do |reporting_id|
        reporting_id.tag!("member-organization", "TuneCore")
      end
      delivery.album do |album|
        album.tag!("label", label_name)
        album.copyright copyright_cline
        if delivery_type == "full_delivery"
          album.action "new"
        elsif delivery_type == "metadata_only"
          album.action "update"
        elsif takedown
          album.action "delete"
        end
        album.upc upc
        album.tag!("digital-upc", upc)
        album.tag!("album-id", tunecore_id)
        album.tag!("album-name", title)
        album.duration # TODO
        album.tag!("disc-count", 1)
        album.tag!("track-count", tracks.size)
        album.release_date actual_release_date
        album.tag!("is-explicit", explicit_lyrics)
        album.tag!("album-artist-name", artists.first.name)
        album.genre genres[0]
        album.subgenre genres[1]
        album.contributors do |contributors|
          artists.each do |artist|
            contributors.contributor({ role: artist.role }, artist.name)
          end # artists
        end # contributors
        album.tag!("album-rights") do |album_rights|
          countries.each do |country|
            album_rights.territory do |territory|
              territory.country country
              territory.tag!("sales-start-date", sale_date)
              territory.download "Y"
              territory.stream "Y"
            end # territory
          end # countries
        end # album_rights
        album.artwork do |artwork|
          artwork.image({ codec: "JPEG", width: 600, height: 600, type: "cover" }, "#{upc}_cover.jpg")
        end # artwork
        album.tracks do |album_tracks|
          tracks.each do |track|
            track.to_xml(album_tracks, self)
          end
        end # album_tracks
      end # album
    end # delivery
  end

  def valid?
    [@upc, @title, @release_date, @label_name, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
    end

    true
  end

  def price_band
    if tracks.size < 6
      "T#{tracks.size}"
    else
      "A1"
    end
  end
end
