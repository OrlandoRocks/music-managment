require "tmpdir"
require File.dirname(__FILE__) + "/../image_utils"

module DistributionSystem::Muve
  # helper class to create the Muve bundle.
  class BundleHelper
    attr_reader :dir, :album, :dirname, :incoming_dir, :feed_dir, :feed_name

    def initialize(work_dir, provider_id, transcoder, s3, album)
      raise "Invalid Album" unless album.valid?

      @album = album
      @work_dir = work_dir
      @provider = provider_id
      @muve_dir = File.join(@work_dir, "Muve")
      @transcoder = transcoder
      @s3 = s3
    end

    def create_bundle
      upc = @album.upc
      @dirname = File.join(@muve_dir, upc.to_s)
      FileUtils.mkdir_p(@dirname)
      @dir = Dir.new(@dirname)
    end

    def write_metadata
      File.open(File.join(@dir.path, @album.upc.to_s + ".xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
        @album.vendor_code = @provider
        @album.to_xml(xml)
      end
    end

    def collect_files
      image_source = File.join(@dir.path, "#{@album.upc}.jpg")
      @s3.get(image_source, @album.artwork_file.asset)
      DistributionSystem::ImageUtils.resize(image_source, 1400, 1400)
      @album.artwork_file.asset = image_source
      @album.tracks.each do |track|
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(original_file, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)
        @transcoder.transcode(original_file, track.audio_file)
        raise "Transcoding failed because file #{track.audio_file} doesn't exist!" unless File.exist?(track.audio_file)

        FileUtils.rm(original_file)
      end
    end

    def track_filename(path, track)
      File.join(path, "#{track.album.upc}_01_#{track.number}.flac")
    end
  end
end
