# 2009-9-9 AK Using seconds rather than set_duration to calculate track duration.

require_relative "../track"
require_relative "../track_length"

module DistributionSystem::Muve
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    # Muve specific metadata elements
    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new, album = nil)
      @duration = seconds(@audio_file)

      xml.track do |track|
        track.tag!("label", album.label_name)
        track.copyright album.copyright_cline
        track.isrc isrc
        track.tag!("disc-number", 1)
        track.tag!("track-number", number)
        track.name title
        track.version
        track.duration # TODO
        track.tag!("is-explicit", explicit_lyrics)
        track.tag!("track-artist-name", artist.name)
        track.genre album.genres[0]
        track.subgenre album.genres[1]
        track.tag!("track-contributors") do |track_contribs|
          track_contribs.contributor({ role: "Artist" }, artist.name)
        end # track_contribs
        track.tag!("track-rights") do |track_rights|
          album.countries.each do |country|
            track_rights.territory do |territory|
              territory.country country
              territory.tag!("sales-start-date", album.sale_date)
              territory.download "Y"
              territory.stream "Y"
            end # territory
          end # countries
        end # track_rights
        track.media do |media|
          media.audio({ codec: "FLAC", bitrate: "44k", duration: "TODO" }, "#{album.upc}_1_#{number}.flac")
        end # media
      end
    end

    # calculate the price band for this track
    def price_band
      if @free_song == true
        "FREE"
      else
        "T1"
      end
    end
  end
end
