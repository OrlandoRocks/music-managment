module DistributionSystem::Juke
  class JukeStore
    attr_accessor :juke_provider
    attr_reader :schema

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No juke directory!"
      @juke_dir = File.join(@work_dir, "Juke")
      @logger = options[:logger] or raise "No logger!"
      @juke_server = options[:juke_server] or raise "No juke server!"
      @remote_root = options[:remote_dir]
      @schema = options[:schema] or raise "No Juke schema!"
    end

    def validate_bundle(bundle)
      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"), true)
    end

    def send_bundle(bundle)
      time = Time.now.strftime("%Y%m%d%H%M%S").to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s

      bundle_id = bundle.album.upc

      batch_dir = "#{@remote_root}/#{time.to_i}"
      remote_dir = "#{@remote_root}/#{time.to_i}/" + bundle_id + "/resources"

      @juke_server.mkdir_p(batch_dir)
      @juke_server.mkdir_p "#{batch_dir}/" + bundle_id
      @juke_server.mkdir_p(remote_dir)

      unless bundle.album.takedown
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(remote_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @juke_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_01_#{track.number}.flac")
          remote_track_file = File.join(remote_dir, "#{bundle_id}_01_#{track.number}.flac")
          @juke_server.upload(track_file, remote_track_file)
        end
      end

      @logger.info "  uploading metadata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(remote_dir + "/../", "#{bundle_id}.xml")
      @juke_server.upload(metadata_filename, remote_metadata_filename)
      `touch /tmp/BatchComplete_#{time}.xml`
      `touch /tmp/#{time}.complete`
      @juke_server.upload("/tmp/BatchComplete_#{time}.xml", "#{batch_dir}/BatchComplete_#{time}.xml")
      @juke_server.upload("/tmp/#{time}.complete", "#{@remote_root}/#{time}.complete")
      @juke_server.start_processing

      true
    end
  end
end
