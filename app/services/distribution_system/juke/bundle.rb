module DistributionSystem::Juke
  class Bundle < DistributionSystem::Bundle
    def write_metadata
      File.open(File.join(@dir.path, @album.upc.to_s + ".xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
        @album.vendor_code = @provider
        @album.to_xml(xml)
      end
    end

    def collect_files
      image_source = File.join(@dir.path, "#{@album.upc}.jpg")
      @s3.get(image_source, @album.artwork_file.asset)
      DistributionSystem::ImageUtils.resize(image_source, 1400, 1400)
      @album.artwork_file.asset = image_source
      # get the album tracks
      @album.tracks.each do |track|
        original_file = File.join(@dir.path, track.temp_filename)
        if track.asset_url
          DistributionSystem::ExternalAsset.get(track.asset_url, original_file)
        else
          @s3.bucket = track.s3_bucket if track.s3_bucket != nil
          @s3.get(original_file, track.s3_key)
        end
        track.audio_file = track_filename(@dir.path, track)
        transcode_track(track)
        raise "Transcoding failed because file #{track.audio_file} doesn't exist!" unless File.exist?(track.audio_file)

        FileUtils.rm(original_file)
      end
    end

    # It is highly recommended to send FLAC.
    def track_filename(path, track)
      File.join(path, "#{track.album.upc}_01_#{track.number}.flac")
    end
  end
end
