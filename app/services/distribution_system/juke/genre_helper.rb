module DistributionSystem::Juke
  class GenreHelper
    def self.get_genres(genres)
      juke_genres = []
      genres.map do |genre|
        juke_genre = get_juke_genre(genre.id)
        juke_genres.push(juke_genre)
      end
      juke_genres
    end

    def self.get_juke_genre(genre_id)
      case genre_id
      when 1
        "ALTERNATIVE/INDIE"
        # when 2    Audiobooks    audiobooks is being removed as an option
      when 3
        "BLUES"
      when 4
        "CHILDREN'S MUSIC"
      when 5
        "CLASSICAL"
      when 6
        "COMEDY/SPOKEN WORD/OTHER"
      when 7
        "DANCE/ELECTRONIC"
      when 8
        "DANCE/ELECTRONIC"
      when 9
        "FOLK"
      when 10
        "POP"
      when 11
        "FOLK"
      when 12
        "POP"
      when 13
        "HIP-HOP/RAP"
      when 14
        "ROCK"
      when 15
        "CHRISTIAN/GOSPEL"
      when 16
        "JAZZ"
      when 17
        "LATIN"
      when 18
        "NEW AGE"
      when 19
        "CLASSICAL"
      when 20
        "POP"
      when 21
        "R&B/SOUL"
      when 22
        "REGGAE/SKA"
      when 23
        "ROCK"
      when 24
        "SOUNDTRACKS/CAST ALBUMS"
      when 26
        "VOCAL/EASY LISTENING"
      when 27
        "WORLD"
      when 28
        "FOLK"
      when 29
        "COUNTRY"
      when 30
        "COMEDY/SPOKEN WORD/OTHER"
      when 31
        "METAL"
      when 32
        "POP"
      when 33
        "POP"
      when 34
        "FOLK"
      when 35
        "JAZZ"
      when 36
        "COMEDY/SPOKEN WORD/OTHER"
      when 37
        "CLASSICAL"
      when 38
        "NEW AGE"
      when 39
        "POP"
      when (40..100)
        "WORLD"
      when 101
        "AMBIENT"
      when 102
        "BRAZILIAN"
      when 103
        "BALADAS"
      when 104
        "BOLEROS"
      when 105
        "CARIBBEAN"
      when 106
        "CUBAN"
      when 107
        "LATIN HIP-HOP"
      when 108
        "LATIN TRAP"
      when 109
        "LATIN URBAN"
      when 110
        "RANCHERA"
      when 111
        "REGIONAL MEXICANO"
      when 112
        "SALSA/MERENGUE"
      when 113
        "TANGO"
      when 114
        "TROPICAL"
      when 115
        "AXÉ"
      when 116
        "BAILE FUNK"
      when 117
        "BOSSA NOVA"
      when 118
        "CHORINHO"
      when 119
        "FORRÓ"
      when 120
        "FREVO"
      when 121
        "MPB"
      when 122
        "PAGODE"
      when 123
        "SAMBA"
      when 124
        "SERTANEJOS"
      when 125
        "AFRICAN"
      when 126
        "AFROBEATS"
      when 127
        "AFROPOP"
      when 128
        "AFRO-FUSION"
      when 129
        "AFRO-SOUL"
      when 130
        "AFRO HOUSE"
      when 131
        "AMAPIANO"
      when 132
        "BONGO FLAVA"
      when 133
        "HIGHLIFE"
      when 134
        "MASKANDI"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
