module DistributionSystem::Juke
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      bundle_id = @bundle.album.upc

      @batch_dir = "#{@remote_dir}/#{timestamp}"
      @remote_server.mkdir_p(@batch_dir)

      remote_dir = "#{@batch_dir}/#{bundle_id}"
      @remote_server.mkdir_p(remote_dir)

      resource_dir = "#{remote_dir}/resources"
      @remote_server.mkdir_p(resource_dir)

      upload_assets(bundle_id, resource_dir, "flac") if full_delivery?
      upload_metadata(bundle_id, remote_dir)
      upload_batch_complete(remote_dir)
      @remote_server.start_processing
    end

    def timestamp
      @timestamp ||= Time.now.strftime("%Y%m%d%H%M%S%L")
    end

    def full_delivery?
      !@bundle.album.takedown
    end

    def upload_batch_complete(remote_dir)
      `touch /tmp/BatchComplete_#{timestamp}.xml`
      `touch /tmp/#{timestamp}.complete`
      @remote_server.upload("/tmp/BatchComplete_#{timestamp}.xml", "#{@batch_dir}/BatchComplete_#{timestamp}.xml")
      @remote_server.upload("/tmp/#{timestamp}.complete", "#{remote_dir}/#{timestamp}.complete")
    end
  end
end
