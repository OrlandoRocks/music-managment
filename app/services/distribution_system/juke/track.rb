module DistributionSystem::Juke
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger,
                  :artists

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      @duration = audio_file_duration

      xml.SoundRecording do |sound_recording|
        sound_recording.SoundRecordingId do |sound_recording_id|
          sound_recording_id.ISRC isrc
        end
        sound_recording.ResourceReference "A#{number}"
        sound_recording.ReferenceTitle do |reference_title|
          reference_title.TitleText title
        end
        sound_recording.Duration @duration
        sound_recording.SoundRecordingDetailsByTerritory do |recording_details|
          recording_details.TerritoryCode "Worldwide"
          recording_details.Title(TitleType: "FormalTitle") do |_title|
            _title.TitleText title
          end
          recording_details.Title(TitleType: "DisplayTitle") do |_title|
            _title.TitleText title
          end
          recording_details.Title(TitleType: "AbbreviatedDisplayTitle") do |_title|
            _title.TitleText title
          end

          primary_track = false
          # If there's a primary artist on the song level, use that
          artists.select { |a| a.role == "primary" or a.role == "primary_artist" }.each do |artist|
            # we shouldn't put various artist at the track level
            next if artist.name.casecmp?("various artists")

            primary_track = true
            recording_details.DisplayArtist do |display_artist|
              display_artist.PartyName do |party_name|
                party_name.FullName artist.name
              end
              display_artist.ArtistRole "MainArtist"
            end
          end

          unless primary_track
            # otherwise use the primary artist from the album
            album.artists.select { |a| a.role == "primary" or a.role == "primary_artist" }.each do |artist|
              next if artist.name.casecmp?("various artists")

              recording_details.DisplayArtist do |display_artist|
                display_artist.PartyName do |party_name|
                  party_name.FullName artist.name
                end
                display_artist.ArtistRole "MainArtist"
              end
            end
          end

          artists.select { |a| a.role == "featuring" }.each do |artist|
            recording_details.DisplayArtist do |display_artist|
              display_artist.PartyName do |party_name|
                party_name.FullName artist.name
              end
              display_artist.ArtistRole "FeaturedArtist"
            end
          end

          recording_details.PLine do |pline|
            pline.Year album.copyright_pline.split(" ")[0]
            pline.PLineText album.copyright_pline
          end
          recording_details.Genre do |genre|
            genre.GenreText album.genres.first
          end
          unless album.takedown
            recording_details.TechnicalSoundRecordingDetails do |technical_details|
              technical_details.TechnicalResourceDetailsReference "T#{number}"
              technical_details.AudioCodecType(
                { "UserDefinedValue" => "FLAC", "Namespace" => DDEX_SENDER_ID.to_s },
                "UserDefined"
              )
              technical_details.NumberOfChannels 2
              technical_details.SamplingRate 44.1
              technical_details.IsPreview false
              technical_details.File do |file|
                file.FileName audio_file.split("/").last
                file.FilePath "resources/"
                file.HashSum do |hash_sum|
                  hash_sum.HashSum DistributionSystem::CheckSumHelper::checksum_file(audio_file)
                  hash_sum.HashSumAlgorithmType "MD5"
                end
              end
            end
          end # end if
        end
      end
    end

    def audio_file_duration
      if audio_file
        iso8601(audio_file)
      elsif duration
        format_duration(duration)
      end
    end
  end
end
