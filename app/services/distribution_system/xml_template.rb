module DistributionSystem
  class XmlTemplate
    def initialize(filename)
      @filename = filename
    end

    def render(options)
      album = options[:album] or raise "must give an album"
      target = options[:target] or raise "no target output stream specified"
      filename = @filename
      Builder::XmlMarkup.new(target: target, indent: 2).instance_eval do
        @album = album
        instance_eval(
          File.read(filename),
          "#{Rails.root}/app/services/distribution_system/groupie_tunes/manifest.rxml",
          1
        )
      end
    end
  end
end
