module DistributionSystem::GroupieTunes
  class Bundle < DistributionSystem::Bundle
    def initialize(work_dir, transcoder, s3, album, store_delivery_config)
      super
      @template = DistributionSystem::XmlTemplate.new(
        Rails.root.join(
          "app",
          "services",
          "distribution_system",
          "groupie_tunes",
          "manifest.rxml"
        )
      )
    end

    def collect_files
      @preset = :amiestreet
      album.tracks.each do |track|
        source_file = File.join(@dir.path, track.temp_filename)
        track.audio_file = File.join(@dirname, track.filename)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(source_file, track.s3_key)
        transcode_track(track)
      end

      artwork_source_file = File.join(@dirname, album.artwork_s3_key)
      @logger.info "  downloading artwork -> #{artwork_source_file}"
      @s3.get(artwork_source_file, album.artwork_s3_key)
      DistributionSystem::ImageUtils.resize(artwork_source_file, 600, 600)
      DistributionSystem::ImageUtils.cp(artwork_source_file, File.join(@dirname, album.artwork_filename.to_s))
    end

    def write_metadata
      metadata_filename = File.join(@dirname, "#{album.upc}.xml")
      File.open(metadata_filename, "w") do |file|
        @template.render(target: file, album: album)
      end
    end
  end
end
