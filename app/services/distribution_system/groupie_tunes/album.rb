class DistributionSystem::GroupieTunes::Album < DistributionSystem::Album
  PROVIDER_NAME = "Tunecore"
  attr_accessor :street_date

  def artwork_filename
    "#{upc}.jpg"
  end
end
