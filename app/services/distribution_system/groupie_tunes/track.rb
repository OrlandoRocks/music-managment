module DistributionSystem::GroupieTunes
  class Track < DistributionSystem::Track
    def filename
      "#{number}.mp3"
    end
  end
end
