module DistributionSystem::GroupieTunes
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      @remote_server.connect do |ftp|
        ftp.cp_r @bundle.dir.path, folder_name, @bundle.album.delivery_type, @bundle.album.takedown
      end
    end

    def folder_name
      Date.today.strftime("%Y-%m-%d")
    end
  end
end
