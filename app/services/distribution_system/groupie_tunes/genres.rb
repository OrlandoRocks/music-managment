module DistributionSystem::GroupieTunes
  class Genres
    @genres = {
      1 => "Alternative Rock", # Alternative
      2 => "Audiobooks", # Audiobooks
      3 => "Blues", # Blues
      4 => "Family", # Children's Music
      5 => "Classical", # Classical
      6 => "Comedy", # Comedy
      7 => "Dance", # Dance
      8 => "Electronica", # Electronic
      9 => "Folk", # Folk
      10 => ["Pop", "World Music"], # French Pop
      11 => ["Folk", "World Music"], # German Folk
      12 => ["Pop", "World Music"], # German Pop
      13 => ["Hip Hop", "Rap"], # Hip Hop/Rap
      14 => "Spiritual", # Holiday
      15 => "Contemporary Christian", # Inspirational
      16 => "Jazz", # Jazz
      17 => "Latin", # Latin
      18 => "New Age", # New Age
      19 => "Classical", # Opera
      20 => "Pop", # Pop
      21 => ["RnB", "Soul"], # R&B/Soul
      22 => "Reggae", # Reggae
      23 => "Rock", # Rock
      24 => "Sound Tracks", # Soundtrack
      26 => ["Jazz", "Blues", "Motown", "Show Tunes", "Soul"], # Vocal
      27 => "World Music", # World
      28 => "Americana", # Americana
      29 => "Country", # Country
      30 => "Spoken Word", # Spoken Word
      31 => "Metal", # Heavy Metal
      32 => "Pop", # J-Pop
      33 => "Pop", # K-Pop
      34 => "Singer-Songwriter", # Singer/Songwriter
      35 => "Jazz", # Big Band
      36 => "Dance", # Fitness & Workout
      37 => "Classical", # High Classical
      38 => "Instrumental", # Instrumental
      39 => "Instrumental",
      101 => "Ambient",  # Ambient
      102 => "Brazilian",
      103 => "Baladas",
      104 => "Boleros",
      105 => "Caribbean",
      106 => "Cuban",
      107 => "Latin Hip-Hop",
      108 => "Latin Trap",
      109 => "Latin Urban",
      110 => "Ranchera",
      111 => "Regional Mexicano",
      112 => "Salsa/Merengue",
      113 => "Tango",
      114 => "Tropical",
      115 => "Axé",
      116 => "Baile Funk",
      117 => "Bossa Nova",
      118 => "Chorinho",
      119 => "Forró",
      120 => "Frevo",
      121 => "MPB",
      122 => "Pagode",
      123 => "Samba",
      124 => "Sertanejo",
      125 => "African",
      126 => "Afrobeats",
      127 => "Afropop",
      128 => "Afro-fusion",
      129 => "Afro-soul",
      130 => "Afro house",
      131 => "Amapiano",
      132 => "Bongo Flava",
      133 => "Highlife",
      134 => "Maskandi",
    }

    def self.genres_for(tunecore_ids)
      tunecore_ids.inject([]) do |genres, id|
        g = @genres[id]
        g = [g] unless g.kind_of? Array
        genres.concat(g)
      end
    end
  end
end
