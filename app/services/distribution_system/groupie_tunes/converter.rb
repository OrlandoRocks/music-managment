class DistributionSystem::GroupieTunes::Converter < DistributionSystem::Converter
  description "GroupieTunes"
  handle_salepoints(/GroupieTun/)

  ALBUM_KLASS           = DistributionSystem::GroupieTunes::Album
  TRACK_KLASS           = DistributionSystem::GroupieTunes::Track

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
    @supports_album_only        = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    DistributionSystem::GroupieTunes::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        artist_name: tunecore_album.artist_name,
        title: tunecore_album.name,
        release_date: tunecore_album.orig_release_date,
        street_date: tunecore_album.sale_date,
        genres: DistributionSystem::GroupieTunes::Genres.genres_for(tunecore_album.genres.collect(&:id)),
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        explicit_lyrics: tunecore_album.parental_advisory,
        delivery_type: delivery_type,
        takedown: set_takedown(tunecore_album, tc_salepoints, "GroupieTun"),
        label_name: set_label(tunecore_album),
        actual_release_date: tunecore_album.sale_date,
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        tracks: tunecore_album.songs.map do |song|
                  DistributionSystem::GroupieTunes::Track.new(
                    {
                      isrc: song.isrc,
                      title: song.name,
                      number: song.track_num,
                      explicit_lyrics: song.parental_advisory?,
                      s3_key: song_s3key(song),
                      s3_bucket: song_s3bucket(song),
                      album_only: song.album_only,
                      temp_filename: "#{tunecore_album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}"
                    }
                  )
                end
      }
    )
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end
end
