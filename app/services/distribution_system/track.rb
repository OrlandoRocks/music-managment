module DistributionSystem
  class Track
    # Metadata elements
    attr_accessor :isrc, #
                  :title, #
                  :label_name, #
                  :genres, #
                  :copyright_pline, #
                  :explicit_lyrics, #
                  :clean_version,
                  :number,
                  :audio_file,
                  :album,
                  :s3_key,
                  :orig_filename,
                  :temp_filename,
                  :artist,
                  :sku,
                  :album_only,
                  :available_for_streaming,
                  :free_song,
                  :asset_url,
                  :creatives,
                  :duration,
                  :s3_bucket,
                  :preview_start_index,
                  :english_title,
                  :phonetic_title,
                  :japanese_title,
                  :instrumental,
                  :composition,
                  :song_id,
                  :takedown,
                  :song_start_time

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def valid?
      return false unless @isrc =~ /^[A-Za-z0-9]{5}\d{7}$/ or @isrc =~ /^[A-Za-z0-9]{2}-[A-Za-z0-9]{3}-\d{2}-\d{5}$/
      return false if @title.to_s == ""
      return false unless @number.to_i.positive?

      # Nested checks
      return false if @artist && !@artist.valid?

      true
    end

    def formatted_duration
      if audio_file
        iso8601(audio_file)
      elsif duration
        format_duration(duration)
      end
    end

    def get_duration(format = "ss")
      case format
      when "ss"
        @duration.to_s
      when "mm:ss"
        if @duration && @duration.positive?
          seconds = @duration % 60
          minutes = @duration / 60
          if minutes < 10
            # appends a 0 to seconds so it'll be format as m::ss... 09,08,07...00
            (seconds < 10) ? "0#{minutes}:0#{seconds}" : "0#{minutes}:#{seconds}"
          else
            # appends a 0 to seconds so it'll be format as m::ss... 09,08,07...00
            (seconds < 10) ? "#{minutes}:0#{seconds}" : "#{minutes}:#{seconds}"
          end
        else
          "0#{minutes}:0#{seconds}"
        end
      else
        raise "get_duration exception, format #{format} unsupported"
      end
    end

    def format_duration(duration)
      return "PT99H01M01S" if duration.blank? || duration.zero?

      seconds = duration % 60
      minutes = duration / 60
      hours = minutes / 60

      "PT#{hours.to_s.rjust(2, '0')}H#{minutes.to_s.rjust(2, '0')}M#{seconds.to_s.rjust(2, '0')}S"
    end
  end
end
