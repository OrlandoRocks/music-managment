module DistributionSystem::Spinlet
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Spinlet/)
  end
end
