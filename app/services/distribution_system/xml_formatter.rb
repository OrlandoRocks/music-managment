module DistributionSystem
  class XmlFormatter
    attr_reader :formattable

    def initialize(formattable)
      @formattable = formattable
      @xml         = Builder::XmlMarkup.new(indent: 2)
    end

    def format
      render formattable_name, @formattable, xml: @xml, formatter: self
    end

    def render(file_name, object, options = {})
      file = "#{template_path}/#{file_name}.builder"
      Tilt.new(file).render(object, options)
    end

    def valid?
      xsd.valid?(Nokogiri::XML(@formattable.to_xml))
    end

    def validation_errors
      xsd.validate(Nokogiri::XML(@formattable.to_xml)).map(&:message)
    end

    def xsd
      doc = Nokogiri::XML::Document.parse(File.open(validation_path))
      @xsd ||= Nokogiri::XML::Schema.from_document(doc)
    end

    def template_path
      Rails.root.join("app", "services", "distribution_system", template_format, "templates")
    end

    def validation_path
      Rails.root.join("app", "services", "distribution_system", "schemas", schema_file)
    end

    def render_partial(path, object, options)
      file     = File.join(template_path, path, "#{@formattable.store_name.downcase}.builder")
      filename = File.exist?(file) ? File.join(path, @formattable.store_name.downcase) : File.join(path, "default")
      render(filename, object, options)
    end

    def channels_partial(path)
      full_path = File.join(template_path, path, "#{@formattable.store_name.downcase}_channels.builder")

      if File.exist?(full_path)
        File.join(
          path,
          "#{@formattable.store_name.downcase}_channels"
        )
      else
        File.join(path, "default")
      end
    end

    private

    def formattable_name
      @formattable.class.name.demodulize.underscore
    end

    def template_format
      @formattable.class.format.to_s
    end

    def schema_file
      @formattable.class.schema_file(@formattable.version)
    end
  end
end
