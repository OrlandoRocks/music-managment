module DistributionSystem::Shazam
  class Product
    def initialize(options = {})
      # Defaults
      # options = {:territory => 'world'}.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    # Metadata elements
    attr_accessor :sales_start_date, :territory, :album_wholesale_price_tier, :track_wholesale_price_tier
    attr_accessor :sales_end_date, :cleared_for_sale, :product_type, :track, :album

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.product do
        # xml.territory            territory
        # if product_type == "album"
        #   xml.wholesale_price_tier album_wholesale_price_tier
        # end
        # if product_type == "track"
        #   xml.wholesale_price_tier track_wholesale_price_tier
        # end
        xml.sales_start_date     sales_start_date.strftime("%Y-%m-%d") if product_type != "track"
        # uncomment the following line to enable pre-orders (AK 2011-10-26)
        # xml.preorder_sales_start_date pre_order_date.strftime('%Y-%m-%d') if pre_order_date && product_type != "track" && album.album_type != "ringtone"
        xml.cleared_for_sale     cleared_for_sale.to_s
      end
    end

    # returns the pre-order date, or nil
    def pre_order_date
      if sales_start_date > Date.today
        Date.today
      else
        nil
      end
    end

    def valid?
      # Required fields
      # return false if territory.nil?
      # return false if wholesale_price_tier.nil?
      return false if cleared_for_sale.nil?

      # Check Values
      return false if sales_start_date && !sales_start_date.respond_to?(:strftime)

      true
    end
  end
end
