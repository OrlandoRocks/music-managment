module DistributionSystem::Shazam
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      bundle_id     = @bundle.album.upc
      @batch_dir    = "#{@remote_dir}/#{timestamp}"
      remote_dir    = "#{@batch_dir}/#{bundle_id}"
      resource_dir  = remote_dir.to_s
      @remote_server.mkdir_p @batch_dir
      @remote_server.mkdir_p remote_dir
      upload_assets(bundle_id, resource_dir, "mp3") if full_delivery?
      upload_metadata(bundle_id, remote_dir)
      upload_batch_complete(remote_dir)
      @remote_server.start_processing
    end

    def timestamp
      Time.now.strftime("%Y%m%d")
    end

    def full_delivery?
      !@bundle.album.takedown
    end

    def upload_batch_complete(remote_dir)
      `touch /tmp/delivery.complete`
      @remote_server.upload("/tmp/delivery.complete", "#{remote_dir}/delivery.complete")
    end
  end
end
