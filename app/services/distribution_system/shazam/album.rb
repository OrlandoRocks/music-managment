class DistributionSystem::Shazam::Album < DistributionSystem::Album
  attr_accessor :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :album_type,
                :takedown
  # iTunes specific metadata sub-elements, should be arrays
  attr_accessor :products

  def to_xml(xml = Builder::XmlMarkup.new, provider = nil)
    xml.package(xmlns: "http://apple.com/itunes/importer", version: "music5.0") do |package|
      if !language_code
        package.language "en"
      else
        package.language language_code
      end
      package.provider provider
      package.album do |album|
        album.album_type  album_type if !album_type.nil? and album_type == "ringtone"
        album.grid              grid if grid
        album.upc               upc

        #  2012-07-25 Takashi Egawa: title has 3 names
        #  2012-08-01 Takashi Egawa: changes 'translate' to 'translation'
        album.title             determine_album_name(title), "translation" => "native"
        album.title           determine_album_name(@phonetic_title), "translation" => "phonetic" if @phonetic_title
        album.title           determine_album_name(@english_title), "translation" => "english" if @english_title
        #  2012-09-03 Takashi Egawa: adds xml output for japanese_title
        album.title           determine_album_name(@japanese_title), "translation" => "japanese" if @japanese_title

        album.title_version title_version if title_version
        album.original_release_date actual_release_date
        album.label_name label_name

        # 2010-01-08 -- ED -- added uniq which should fixed duplicated genre
        if genres
          album.genres do |album_genres|
            genres.uniq.each { |g| album_genres.genre g }
          end
        end

        album.copyright_pline   copyright_pline if copyright_pline
        album.copyright_cline   copyright_cline if copyright_cline

        #  2012-08-01 Takashi Egawa: explicit_content not allowed at album level.
        # if explicit_lyrics == true
        #  album.explicit_content "explicit"
        #         TODO: implement this rule in the tunecore app.
        #          else explicit_lyrics # there is a explicit version out there but this is the clean version
        #          xml.explicit_content "clean"
        # end

        # ak
        if @delivery_type == "full_delivery" and !@takedown
          artwork_file = @artwork_file.asset.split(".")[0] + ".jpg"
          album.artwork_files do |artwork_files|
            artwork_files.file do |file_xml|
              file_xml.file_name(File.basename(artwork_file))
              #  2012-07-25 Takashi Egawa: size required
              file_xml.size(File.size(artwork_file))
              file_xml.checksum(DistributionSystem::CheckSumHelper.checksum_file(artwork_file), type: DistributionSystem::CheckSumHelper.algorithm)
            end
          end
        end

        album.liner_notes       liner_notes if (liner_notes && !album_type.nil? && album_type != "ringtone")
        album.description_short description_short if description_short
        album.description_long  description_long if description_long
        album.volume_number     volume_number if volume_number
        if booklet_file
          album.track_count       tracks.size.to_i + 1
        else
          album.track_count       tracks.size
        end

        unless creatives.empty?
          album.artists do |artists|
            creatives.each { |c| YAML::load(c.to_yaml).to_xml(artists) }
          end
        end

        if products
          album.products do |album_products|
            products.each { |p| p.album = self; p.to_xml(album_products) }
          end
        end

        # ak
        unless @takedown
          album.tracks do |album_tracks|
            tracks.each { |t| t.to_xml(album_tracks) }
          end
        end
      end
    end
  end

  def is_single?
    if tracks.size <= 3 &&
       !tracks.detect { |track| track.duration > 600 }

      true
    else
      false
    end
  end

  def is_ep?
    if tracks.size <= 3 && # 1–3 tracks, one or more tracks with a running time of 10 minutes or more, total running time less than 30 minutes
       tracks.detect { |track| track.duration > 600 } &&
       album_length < 1800

      true
    elsif tracks.size >= 4 && # 4–6 tracks, with a running time of less than 30 minutes
          tracks.size <= 6 &&
          album_length < 1800

      true
    else
      false
    end
  end

  def album_length
    sum = 0
    tracks.each { |track| sum += track.duration }
    sum
  end

  def determine_album_name(album_title)
    return album_title if takedown

    if is_single? && !album_title[/Single$/] && album_type != "ringtone"
      album_title + " - Single"
    elsif is_ep? && !album_title.rstrip[/EP$/]
      album_title + " - EP"
    else
      album_title
    end
  end

  def valid?
    # Check required fields
    [@upc, @title, @release_date, @label_name, @products, @genres, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?

      return false if @artwork_asset_url.nil? && @artwork_s3_key.nil?
    end

    true
  end
end
