module DistributionSystem::Shazam
  class GenreHelper
    def self.get_genres(genres)
      shazam_genres = []
      genres.each do |genre|
        shazam_genres.push(get_shazam_genre(genre.id))
      rescue DistributionSystem::GenreNotSupported => e
        if (genre == genres.last) && shazam_genres.empty?
          raise
        end # looks like we don't have any supported genres, so raise error
      end
      shazam_genres
    end

    def self.get_shazam_genre(genre_id)
      case genre_id
      when 1
        "Alternative"
        # when 2Audiobooksaudiobooks is being removed as an option
      when 3
        "Blues"
      when 4
        "Children's Music"
      when 5
        "Classical"
      when 6
        "Comedy"
      when 7
        "Dance"
      when 8
        "Electronic"
      when 9
        "Folk"
      when 10
        "French Pop"
      when 11
        "German Folk"
      when 12
        "German Pop"
      when 13
        "Hip Hop/Rap"
      when 14
        "Holiday"
      when 15
        "Inspirational"
      when 16
        "Jazz"
      when 17
        "Latin"
      when 18
        "New Age"
      when 19
        "Opera"
      when 20
        "Pop"
      when 21
        "R&B"
      when 22
        "Reggae"
      when 23
        "Rock"
      when 24
        "Soundtrack"
      when 26
        "Vocal"
      when 27
        "World"
      when 28
        "Folk"
      when 29
        "Country"
      # when 30
      # "Spoken Word"
      when 31
        "Heavy Metal"
      when 32
        "J-Pop"
      when 33
        "K-Pop"
      when 34
        "Singer/Songwriter"
      when 35
        "Big Band"
      when 36
        "Fitness & Workout"
      when 37
        "High Classical"
      when 38
        "Instrumental"
      when 39
        "Instrumental"
      when (40..100)
        "World"
      when 101
        "Ambient"
      when 102
        "Brazilian"
      when 103
        "Baladas"
      when 104
        "Boleros"
      when 105
        "Caribbean"
      when 106
        "Cuban"
      when 107
        "Latin Hip-Hop"
      when 108
        "Latin Trap"
      when 109
        "Latin Urban"
      when 110
        "Ranchera"
      when 111
        "Regional Mexicano"
      when 112
        "Salsa/Merengue"
      when 113
        "Tango"
      when 114
        "Tropical"
      when 115
        "Axé"
      when 116
        "Baile Funk"
      when 117
        "Bossa Nova"
      when 118
        "Chorinho"
      when 119
        "Forró"
      when 120
        "Frevo"
      when 121
        "MPB"
      when 122
        "Pagode"
      when 123
        "Samba"
      when 124
        "Sertanejo"
      when 125
        "African"
      when 126
        "Afrobeats"
      when 127
        "Afropop"
      when 128
        "Afro-fusion"
      when 129
        "Afro-soul"
      when 130
        "Afro house"
      when 131
        "Amapiano"
      when 132
        "Bongo Flava"
      when 133
        "Highlife"
      when 134
        "Maskandi"
      else
        raise DistributionSystem::GenreNotSupported, "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
