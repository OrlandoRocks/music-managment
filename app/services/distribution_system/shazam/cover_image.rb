module DistributionSystem::Shazam
  class CoverImage
    attr_accessor :asset

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def valid?
      extension = File.extname(@asset)
      return false unless [".tif", ".jpg"].include?(extension)

      true
    end
  end
end
