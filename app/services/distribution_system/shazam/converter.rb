require "date"
require "ostruct"

module DistributionSystem::Shazam
  class Converter < DistributionSystem::Converter
    description "Shazam"
    handle_salepoints(/^Shazam/)

    ALBUM_KLASS           = DistributionSystem::Shazam::Album
    TRACK_KLASS           = DistributionSystem::Shazam::Track

    def initialize
      @supports_metadata_update = true
      @supports_takedown        = true
    end

    def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
      this_year = Date.today.year.to_s

      album = DistributionSystem::Shazam::Album.new(
        {
          tunecore_id: tunecore_album.id,
          copyright_name: tunecore_album.copyright_name,
          upc: tunecore_album.upc.to_s,
          sku: tunecore_album.sku,
          copyright_pline: "#{tunecore_album.orig_release_date.year} #{tunecore_album.copyright_name}",
          copyright_cline: "#{tunecore_album.orig_release_date.year} #{tunecore_album.copyright_name}",
          release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
          genres: DistributionSystem::Shazam::GenreHelper.get_genres(tunecore_album.genres),
          store_id: tc_salepoints.pluck(:store_id).first,
          store_name: tc_salepoints.map(&:store).first.short_name,
          tracks: [],
          label_name: set_label(tunecore_album),
          explicit_lyrics: tunecore_album.parental_advisory?,
          liner_notes: tunecore_album.liner_notes,
          products: set_products(tunecore_album, tc_salepoints),
          artwork_file: DistributionSystem::Shazam::CoverImage.new,
          creatives: [],
          delivery_type: delivery_type,
          album_type: set_album_type(tunecore_album),
          takedown: set_takedown(tunecore_album, tc_salepoints, "Shazam"),
          language_code: get_language_code(tunecore_album),
          countries: countries(tunecore_album),
          title: tunecore_album.name,
          artwork_s3_key: tunecore_album.artwork.s3_asset.key,
          actual_release_date: tunecore_album.sale_date
        }
      )

      tunecore_album.creatives.each do |creative|
        album.creatives << DistributionSystem::Shazam::Creative.new(name: creative.name, role: set_role(creative.role_for_petri))
      end

      # Add the songs to this petri album
      tunecore_album.songs.each do |song|
        add_track(album, song, tunecore_album, tc_salepoints)
      end

      if tunecore_album.is_various
        album.creatives << DistributionSystem::Shazam::Creative.new(name: "Various Artists", role: "Artist")
      end

      # validate the album
      raise "Album validation failed. One or more required attributes are missing." unless album.valid?

      album
    end

    # add album tracks
    def add_track(album, song, tunecore_album, tc_salepoints)
      track = DistributionSystem::Shazam::Track.new(
        {
          album: album,
          number: song.track_num,
          sku: song.sku,
          isrc: song.isrc,
          temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
          explicit_lyrics: song.parental_advisory,
          s3_key: song_s3key(song),
          s3_bucket: song_s3bucket(song),
          asset_url: song.external_asset_url,
          creatives: [],
          title: song.name,
          duration: song.duration_in_seconds || calculate_duration(song)
        }
      )
      # set the preview start time
      if song.respond_to?(:preview_start_time) && song.preview_start_time != nil
        track.preview_start_index = song.preview_start_time
      end

      # set the creatives from the song
      song.creatives.each do |creative|
        track.creatives << DistributionSystem::Shazam::Creative.new(name: creative.name, role: set_role(creative.role_for_petri))
      end

      # set the product on this track, this lets us specify a track level price tier
      track.products = set_products(tunecore_album, tc_salepoints, "track")

      track.orig_filename = song.upload.uploaded_filename if song.upload
      album.tracks << track
    end

    def set_album_type(tunecore_album)
      case tunecore_album.album_type
      when "Ringtone", "Album", "Single"
        tunecore_album.album_type.to_s.downcase
      else
        ""
      end
    end

    def set_label(tunecore_album)
      if tunecore_album.label
        #  2012-07-25 Takashi Egawa: label is a String object, and it doesn't have 'name' method.
        if tunecore_album.label.instance_of?(Label)
          tunecore_album.label.name
        else
          tunecore_album.label.to_s
        end
      else
        tunecore_album.artist_name
      end
    end

    # product type can be either 'album' or 'track'
    def set_products(tunecore_album, salepoints, product_type = "album")
      products = []
      salepoints.select { |s| s.store.short_name =~ /^Shazam/ }.map do |salepoint|
        salepoint.store.countries.each do |_territory|
          i_product = DistributionSystem::Shazam::Product.new
          i_product.product_type                = product_type
          # i_product.territory                   = territory
          # i_product.album_wholesale_price_tier  = salepoint.album_price_code
          # i_product.track_wholesale_price_tier  = salepoint.track_price_code
          i_product.sales_start_date            = Date.strptime(tunecore_album.sale_date.to_s, "%Y-%m-%d")
          i_product.cleared_for_sale            = !set_takedown(tunecore_album, [salepoint], "Shazam")

          products.push(i_product)
        end
      end

      products
    end

    def set_role(role = "")
      case role
      when "primary_artist", "primary"
        "Performer"
      else
        role.capitalize
      end
    end
  end
end
