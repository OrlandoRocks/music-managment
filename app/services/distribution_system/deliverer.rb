class DistributionSystem::Deliverer
  attr_reader :s3_connection, :s3_streaming, :metadata_archive_bucket, :work_dir, :transcoder

  def initialize(config)
    @work_dir = File.join(Rails.root, config["work_dir"])
    configure_s3_connection(config)
    configure_transcoder
  end

  def configure_s3_connection(config)
    @s3_connection           = DistributionSystem::S3.new(config["asset_bucket"])
    @s3_streaming            = DistributionSystem::S3.new("streaming.tunecore.com")
    @metadata_archive_bucket = DistributionSystem::S3.new(config["archive_bucket"])
  end

  def configure_transcoder
    tempdir     = DistributionSystem::TempDir.new
    shell       = DistributionSystem::Shell.new
    @transcoder = DistributionSystem::Transcoder.new(
      {
        mp3_decoder: "lame",
        alac_decoder: "ffmpeg",
        aac_decoder: "faad",
        flac_encoder: "flac",
        flac_decoder: "flac",
        aac_encoder: "faac",
        shell: shell,
        tempdir: tempdir,
      }
    )
  end

  def distribute(album, distribution)
    @distributor = DistributionSystem::DistributorFactory.distributor_for(album, self)
    @distributor.distribute(album, distribution)
  end

  def clean_up
    @distributor.try(:clean_up)
  end

  def archive_metadata(store, upc, job_id)
    @distributor.archive_metadata(metadata_archive_bucket, store, upc, job_id)
  end

  def uuid
    @distributor.uuid
  end
end
