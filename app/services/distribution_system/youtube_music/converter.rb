module DistributionSystem::YoutubeMusic
  class Converter < DistributionSystem::Believe::Converter
    description "YouTube Music"
    handle_salepoints(/^YTMusic/)

    ALBUM_KLASS = DistributionSystem::YoutubeMusic::Album
  end
end
