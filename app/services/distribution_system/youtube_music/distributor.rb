module DistributionSystem::YoutubeMusic
  class Distributor < DistributionSystem::Believe::Distributor
    def upload_resources
      remote_dir   = @bundle.remote_file_path
      resource_dir = assign_resource_dir(remote_dir)

      @remote_server.connect do |server|
        begin
          server.mkdir_p(remote_dir)
          server.mkdir_p(resource_dir) if full_delivery? && @bundle.use_resource_dir
          @bundle.resources_for(@bundle.album.delivery_type).each do |resource|
            server.upload(resource[:local_file], resource[:remote_file])
          end
          send_complete_file(server, remote_dir)
        rescue DistributionSystem::DeliveryError, Exception => e
          Rails.logger.error "Error during distributable of #{@bundle.album.upc}, Error Message: #{e.message} #{@bundle.album}"
          Rails.logger.error e.backtrace.join('\n')
          raise e
        end
      end
    end

    def send_complete_file(server, remote_dir)
      batch_complete_file = "BatchComplete_#{@bundle.album.upc}.xml"
      local_complete_file = FileUtils.touch(File.join(Rails.root, "tmp", batch_complete_file)).first
      server.upload(local_complete_file, "#{remote_dir}/#{batch_complete_file}")
    end
  end
end
