module DistributionSystem::Akazoo
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Akazoo/)
  end
end
