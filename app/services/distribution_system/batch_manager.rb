module DistributionSystem
  class BatchManager
    class JoinBatchError < StandardError; end

    class RemoveFromBatchError < StandardError; end

    DEFAULT_AGE_CAP = 25 # hours

    def self.close_stale_batches(store_id, cutoff_time, age_cap)
      store = Store.find(store_id)
      upper_limit = age_cap || DEFAULT_AGE_CAP
      batches = DeliveryBatch.where(store_id: store.id)
                             .where("timestampdiff(MINUTE,created_at,now())>?", cutoff_time)
                             .where("batch_complete_sent_at is null")
                             .where("timestampdiff(HOUR,created_at,now())<?", upper_limit)

      batches.each do |batch|
        if batch.count == batch.processed_count
          close_batch(batch.batch_id)
          next
        end

        currently_processing = batch.distributions.select(&:processing?)
        close_batch(batch.batch_id) if currently_processing.size.zero?
      rescue StandardError => e
        Rails.logger.error("Stale batch cleanup error: #{e.message}")
      end
    end

    def self.join_batch(deliverable_type, deliverable_id, store_id)
      new(deliverable_type, deliverable_id, store_id).join_batch
    end

    def self.remove_from_batch(deliverable_type, deliverable_id, delivery_batch_id)
      new(deliverable_type, deliverable_id).remove_from_batch(delivery_batch_id)
    end

    def self.entire_batch_sent?(delivery_batch)
      items = delivery_batch.delivery_batch_items
      items.any? && items.where(DeliveryBatchItem.arel_table[:status].not_in(["sent", "removed"])).count.zero?
    end

    def self.close_batch(batch_id)
      delivery_batch = DeliveryBatch.find_by(batch_id: batch_id)
      return false if delivery_batch.blank?

      @remote_server = DistributionSystem::Server.server_for(delivery_batch.store.short_name)
      remote_dir = delivery_batch.store.store_delivery_config.remote_dir
      local_batch_complete_file = generate_local_batch_complete(delivery_batch)
      remote_batch_complete_file = File.join(remote_dir || ".", batch_id, "BatchComplete_#{batch_id}.xml")
      @remote_server.connect do |server|
        Rails.logger.debug "Started Copying #{local_batch_complete_file} to #{remote_batch_complete_file} for #{delivery_batch.store_id}"
        server.upload(local_batch_complete_file, remote_batch_complete_file)
        Rails.logger.debug "Finished Copying #{local_batch_complete_file} to #{remote_batch_complete_file} for #{delivery_batch.store_id}"
      end
      unless delivery_batch.update(batch_complete_sent_at: Time.now, currently_closing: false)
        Rails.logger.debug("Unable to update batch complete sent at #{delivery_batch.errors.inspect}")
      end
      delivery_batch.delivery_batch_items.where(status: "present").each do |dbi|
        Rails.logger.debug "Requeuing #{dbi.deliverable_type} ##{dbi.deliverable_id} "
        Delivery::DistributionWorker.perform_async(dbi.deliverable_type, dbi.deliverable_id)
        dbi.update(status: "removed")
      end
      delivery_batch.reload
      delivery_batch.deactivate
    end

    def self.generate_local_batch_complete(delivery_batch)
      deliverable = delivery_batch.delivery_batch_items.last.deliverable
      new(deliverable.class.to_s, deliverable.id, delivery_batch.store_id).generate_local_batch_complete
    end

    attr_reader :store_id, :deliverable, :retry_count

    def initialize(deliverable_type, deliverable_id, store_id = nil)
      @store_id        = store_id
      @deliverable     = deliverable_type.classify.constantize.find(deliverable_id)
      @retry_count     = 0
    end

    def join_batch
      DeliveryBatch.transaction do
        active_batch = active_batch_for_store
        active_batch.increment_joined_count
        create_deliverable_batch_item(active_batch)
        active_batch
      end
    rescue ActiveRecord::StatementInvalid => e
      raise unless e.message.match?(/[tT]imeout/)

      @retry_count += 1
      join_batch unless @retry_count > 30
      raise JoinBatchError, "could not join active batch for store ID: #{store_id}, distribution ID: #{deliverable}. Error: #{e.message}"
    end

    def remove_from_batch(delivery_batch_id)
      DeliveryBatch.transaction do
        batch      = DeliveryBatch.find(delivery_batch_id)
        batch_item = DeliveryBatchItem.where(
          deliverable_type: deliverable.class.name,
          deliverable_id: deliverable.id,
          delivery_batch_id: delivery_batch_id
        ).last
        batch.decrement_joined_count if batch.active
        batch_item.remove_from_batch
      end
    rescue ActiveRecord::StatementInvalid => e
      raise unless e.message.match?(/[tT]imeout/)

      @retry_count += 1
      remove_from_batch(delivery_batch_id) unless @retry_count > 30
      raise RemoveFromBatchError, "could not remove distribution id #{deliverable.id} from batch #{delivery_batch_id}. Error: #{e.message}"
    end

    def create_deliverable_batch_item(active_batch)
      DeliveryBatchItem.find_or_create_by(
        delivery_batch_id: active_batch.id,
        deliverable_type: deliverable.class.name,
        deliverable_id: deliverable.id
      )
    end

    def generate_local_batch_complete
      batch = deliverable.delivery_batches.last
      local_path = File.join(Rails.root, "tmp", "BatchComplete_#{batch.batch_id}.xml")
      if Store.find(store_id).use_manifest
        Rails.logger.debug "Generating local batch complete with manifest for #{store_id}"
        manifest = DistributionSystem::DeliveryBatchManifest.write_manifest(batch)
        File.open(local_path, "w") { |f| f.write(manifest) }
        Rails.logger.debug "Generated local batch complete with manifest for #{store_id} at #{local_path}"
      else
        FileUtils.touch(local_path)
      end

      local_path
    end

    private

    attr_writer :retry_count

    def active_batch_for_store
      DeliveryBatch
        .where(store_id: store_id, active: true)
        .lock(true)
        .first_or_create
    end
  end
end
