module DistributionSystem::TrackLength
  def seconds(audio_file)
    unescaped_audio_file = audio_file
    # because of stupidity, sometimes the original filename from the customer is kept which may contain all sorts of crap
    audio_file = Shellwords.escape audio_file
    raise "Error determining duration: audio_file not set" if audio_file.nil?
    raise "Error determining duration: File does not exist: #{audio_file}" unless File.exist?(unescaped_audio_file)

    @logger.info("grabbing playtime from #{audio_file}") if @logger
    cmd = "mediainfo #{audio_file}"
    @logger.info("command about to execute: #{cmd}") if @logger
    output = `#{cmd}`
    @logger.debug("TrackLength: file: #{audio_file}, output: #{output}") if @logger

    duration = get_duration_in_sec(output)

    @logger.info("playtime for #{audio_file} is #{duration} secs") if @logger
    duration
  end

  def iso8601(audio_file)
    unescaped_audio_file = audio_file
    # because of stupidity, sometimes the original filename from the customer is kept which may contain all sorts of crap
    audio_file = Shellwords.escape audio_file
    cmd = "mediainfo --Inform='General;%Duration/String3%' #{audio_file}"
    output = `#{cmd}`
    @logger.info("TrackLengh: #{audio_file} length output: #{output}") if @logger
    hr, mins, secs, ms = 0
    times = output.match(/(\d*):(\d*):(\d*)\.(\d*)/)

    if times.nil?
      msg = "Cannot get track length info! File: #{audio_file.inspect} file size: #{File.size? unescaped_audio_file}. Output: #{output}"
      @logger.error(msg) if @logger
      raise msg
    end

    if times.length == 5
      hr = times[1].to_i
      mins = times[2].to_i
      secs = times[3].to_i
      ms = times[4].to_i
    end

    secs += 1 if ms > 500
    # could push the value to 60 because of the ms adjustment above.
    if secs == 60
      mins += 1
      secs = 0
    end

    if mins == 60
      hr += 1
      mins = 0
    end

    if hr.positive? and mins >= 0
      "PT#{hr.to_s.rjust(2, '0')}H#{mins.to_s.rjust(2, '0')}M#{secs.to_s.rjust(2, '0')}S"
    elsif hr.zero? and mins.positive?
      "PT00H#{mins.to_s.rjust(2, '0')}M#{secs.to_s.rjust(2, '0')}S"
    elsif hr.zero? and mins.zero? and secs > 1
      "PT00H00M#{secs.to_s.rjust(2, '0')}S"
    elsif hr.zero? and mins.zero? and secs == 1
      # not sure this is valid
      @logger.warn("TrackLengh: #{audio_file} has a suspect length: #{output}") if @logger
      "PT00H00M01S"
    else
      # this should cause a schema validation
      # TODO: we should have a way to error out a package here
      @logger.error("TrackLengh: #{audio_file} has an invalid length: #{output}") if @logger
      raise "Track duration is zero for #{audio_file}."
    end
  end

  # Added 4/16/2009 by Alex
  #
  # Sets the duration of the track in seconds
  #
  # This seems to be a better way to get the duration of a track.  The Mediainfo program will read many kinds of audio files,
  # where shntool will only read wavs.  Just include this class in your track object and it will attempt to get the duration
  # of the audio_file
  def set_duration
    raise "Duration: audio_file not set" if @audio_file.nil?
    raise "Duration: File does not exist: #{@audio_file}" unless File.exist?(@audio_file)

    @logger.info("about to extract duration info from #{@audio_file}") if @logger
    @logger.info("command about to execute: mediainfo #{@audio_file}") if @logger

    @duration = seconds(@audio_file)
  end

  def get_duration_in_sec(media_info)
    # if the duration is less than 1 min, mediainfo reports it as sec:ms instead of min:sec
    # so we have to change the regex to handle that
    duration = nil
    media_info[/Duration\s*:\s*(\d+)\s*min\s(\d*)\s*s/] # extract the duration info
    mins = $1 # first parenthesis
    secs = $2 # second paranthesis
    duration = (mins.to_i * 60) + secs.to_i # convert to seconds
    if mins.nil? && secs.nil?
      media_info[/Duration\s*:\s*(\d*)\s*s\s(\d*)\s*ms/] # extract the duration info using the sec:ms format
      secs = $1 # first parenthesis
      ms = $2 # second paranthesis
      duration = secs.to_i # convert to secs.ms
    end

    # if duration is over an hour, mediainfo reports it as h:min
    if mins.nil? && secs.nil?
      media_info[/Duration\s*:\s*(\d+)\s*h\s(\d*)\s*min\s(\d*)\s*s\s(\d*)\s*ms/]
      hr =   $1 # first parenthesis
      mins = $2 # second parenthesis
      secs = $3 # third parenthesis

      duration = (hr.to_i * 3600) + (mins.to_i * 60) + secs.to_i # convert to seconds
    end

    duration
  end
end
