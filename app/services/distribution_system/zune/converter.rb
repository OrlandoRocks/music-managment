module DistributionSystem::Zune
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Zune$/)
  end
end
