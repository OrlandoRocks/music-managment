class DistributionSystem::Nokia::Album < DistributionSystem::Album
  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :logger

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.collection(
      productUPC: upc,
      genre: genres.first,
      explicitLyrics: explicit_lyrics,
      parentalAdvisory: explicit_lyrics ? "18" : "0",
      label: label_name,
      primaryRightsHolder: "Tunecore",
      trackCount: tracks.size,
      cLine: copyright_pline,
      pLine: copyright_pline,
      od2VersionNumber: "3.0",
      title: title,
      packshot: "#{upc}.jpg",
      originalReleaseDate: release_date.delete("-")
    ) do |collection|
      collection.offerings do |offerings|
        offering_options = { priceBand: price_band, releaseDate: actual_release_date.delete("-") }
        offering_options[:deleteDate] = Time.now.strftime("%Y%m%d") if takedown

        countries.each do |country|
          offerings.offering(offering_options.merge(territory: country)) do |offering|
            offering.usage(type: "AlaCarte")
          end
        end
      end

      # Artists
      collection.artists do |collection_artists|
        collection_artists.artist(knownAs: artists.first.name, isPrimary: "true", role: "Performer")
      end

      # Volumes
      collection.volumes(totalNumberOfVolumes: "1") do |volumes|
        volumes.volume(sequenceNumber: "1") do |volume|
          volume.tracks do |vol_tracks|
            tracks.each { |t| t.to_xml(vol_tracks) }
          end
        end
      end
    end
  end

  def valid?
    [@upc, @title, @release_date, @label_name, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
    end

    true
  end

  def price_band
    if tracks.size < 6
      "T#{tracks.size}"
    else
      "A1"
    end
  end
end
