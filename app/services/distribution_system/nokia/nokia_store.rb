require_relative "../shell"
require "builder"
module DistributionSystem::Nokia
  class NokiaStore
    attr_accessor :nokia_provider

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No nokia directory!"
      @nokia_dir = File.join(@work_dir, "Nokia")
      @logger = options[:logger] or raise "No logger!"
      @nokia_server = options[:nokia_server] or raise "No Nokia server!"
    end

    def validate_bundle(bundle)
      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"))
    end

    def send_bundle(bundle)
      bundle_id = bundle.album.upc
      date = Time.now.strftime("%Y%m%d")
      @nokia_server.mkdir_p date.to_s
      @nokia_server.mkdir_p "#{date}/Tunecore"
      @nokia_server.mkdir_p "#{date}/Tunecore/batch001"
      remote_dir = "#{date}/Tunecore/batch001/" + bundle_id

      @nokia_server.mkdir_p(remote_dir)
      if bundle.album.delivery_type == "full_delivery"
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(remote_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @nokia_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_01_#{track.number}.flac")
          remote_track_file = File.join(remote_dir, "#{bundle_id}_01_#{track.number}.flac")
          @nokia_server.upload(track_file, remote_track_file)
        end
      end

      @logger.info "  uploading metatata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(remote_dir, "#{bundle_id}.xml")
      @nokia_server.upload(metadata_filename, remote_metadata_filename)
      @nokia_server.start_processing

      true
    end
  end
end
