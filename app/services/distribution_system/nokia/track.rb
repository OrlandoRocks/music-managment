# 2009-9-9 AK Using seconds rather than set_duration to calculate track duration.

require_relative "../track"
require_relative "../track_length"

module DistributionSystem::Nokia
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    # Nokia specific metadata elements
    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      @duration = seconds(@audio_file)

      xml.track(
        duration: "00:#{'%02d' % (@duration.to_i / 60)}:#{'%02d' % (@duration.to_i % 60)}",
        explicitLyrics: explicit_lyrics,
        genre: @album.genres.first,
        parentalAdvisory: @album.explicit_lyrics ? "18" : "0",
        trackSequence: number,
        pLine: @album.copyright_pline,
        isrc: isrc,
        title: title
      ) do |track|
        # Track offerings
        track.offerings do |offerings|
          offerings.offering(territory: "Global", priceBand: price_band) do |offering|
            offering.usage(type: "AlaCarte")
          end
        end

        # Track artists
        track.artists do |artists|
          artists.artist(
            knownAs: artist.name,
            isPrimary: "true",
            role: "Performer"
          )
        end

        # AV Media items
        track.avMediaItems do |avmediaitems|
          avmediaitems.avMediaItem(
            format: "WAV",
            name: "#{album.upc}_01_#{number}.flac",
            bitrate: "1411",
            length: "Full"
          ) if @album.delivery_type == "full_delivery"
        end
      end
    end

    # calculate the price band for this track
    def price_band
      if @free_song == true
        "FREE"
      else
        "T1"
      end
    end
  end
end
