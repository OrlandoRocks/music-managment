class DistributionSystem::Nokia::Converter < DistributionSystem::Converter
  description "Nokia Music"
  handle_salepoints(/^Nokia$/)

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
  end

  #    covert methods converts a tunecore album to an itunes album
  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    album = DistributionSystem::Nokia::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: DistributionSystem::Nokia::GenreHelper.get_genres(tunecore_album.genres),
        stores: tc_salepoints.map do |salepoint| salepoint.store.short_name end,
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artists: [set_artist(tunecore_album)],
        artwork_file: DistributionSystem::Nokia::CoverImage.new(asset: tunecore_album.artwork.s3_asset.key),
        delivery_type: delivery_type,
        takedown: set_takedown(tunecore_album, tc_salepoints, "Nokia"),
        countries: countries(tunecore_albumm),
        actual_release_date: tunecore_album.sale_date.strftime("%Y-%m-%d")
      }
    )

    tunecore_album.songs.each do |song|
      add_track(album, song)
    end
    # validate the album
    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    album
  end

  #        add album tracks
  def add_track(album, song)
    track = DistributionSystem::Nokia::Track.new(
      {
        album: album,
        number: song.track_num.to_s.rjust(3, "0"),
        title: song.name,
        artist: DistributionSystem::Nokia::Artist.new(name: song.artist_name),
        sku: song.sku,
        isrc: song.isrc,
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
        s3_key: song_s3key(song),
        s3_bucket: song_s3bucket(song),
        album_only: song.album_only,
        free_song: song.free_song,
        asset_url: song.external_asset_url
      }
    )
    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end

  def set_artist(tunecore_album)
    if !tunecore_album.is_various?
      a_artist = DistributionSystem::Nokia::Artist.new
      a_artist.name    = tunecore_album.artist_name
      a_artist.role    = "Performer"
      a_artist.primary = true
      a_artist
    else
      DistributionSystem::Nokia::Artist.new(name: tunecore_album.artist_name)
    end
  end
end
