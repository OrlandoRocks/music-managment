require_relative "../distributor"

module DistributionSystem::Nokia
  class Distributor < DistributionSystem::Distributor
    attr_accessor :logger

    def initialize(options)
      @work_dir = options[:work_dir]
      @nokia_store = options[:nokia_store]
      super
    end

    def distribute(album, distribution)
      album.logger = @logger
      album.tracks.each do |track|
        track.logger = @logger
      end
      @logger.info "Preparing to upload album #{album.upc}| album_id=#{album.tunecore_id}"
      raise "Invalid Album" unless album.valid?

      begin
        @bundle = BundleHelper.new(@work_dir, @nokia_store.nokia_provider, @transcoder, @s3, album)
        @bundle.create_bundle
        @logger.info "Preparing to collect album's assets."
        Distribution::StateUpdateService.update(distribution, { state: "gathering_assets" })
        @bundle.collect_files
        @logger.info "Preparing to write the metadata file."
        @bundle.write_metadata
        Distribution::StateUpdateService.update(distribution, { state: "packaged" })
        send_to_nokia!
      end
    end

    def send_to_nokia!
      @logger.info "Sending to Nokia #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id}"
      shipped = @nokia_store.send_bundle(@bundle)
      @logger.info "completed shipping of the #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id}"
    end
  end
end
