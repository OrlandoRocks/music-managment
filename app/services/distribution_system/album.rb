class DistributionSystem::Album
  attr_reader :timestamp, :created_by, :created_because

  attr_accessor :artwork_asset_url,
                :artist_name,
                :tunecore_id,
                :copyright_name,
                :upc,
                :sku,
                :title,
                :copyright_pline,
                :copyright_cline,
                :release_date,
                :genre,
                :store_id,
                :store_name,
                :artwork_s3_key,
                :tracks,
                :creatives,
                :language_code,
                :label_name,
                :explicit_lyrics,
                :liner_notes,
                :artists,
                :artwork_file,
                :booklet_file,
                :delivery_type,
                :album_type,
                :takedown,
                :takedown_at,
                :countries,
                :worldwide,
                :is_various,
                :language,
                :customer_name,
                :customer_id,
                :actual_release_date,
                :party_id,
                :party_full_name,
                :audio_codec_type,
                :use_proprietary_id,
                :ddex_version,
                :album_price_code,
                :track_price_code,
                :include_behalf,
                :skip_media,
                :xsd,
                :display_track_language,
                :display_track_id,
                :uuid,
                :deal_list_type,
                :image_size,
                :description_short,
                :description_long,
                :volume_number,
                :track_count,
                :english_title,
                :phonetic_title,
                :japanese_title,
                :download_countries,
                :streaming_countries,
                :genres,
                :channels,
                :include_assets,
                :normalize_bit_depth,
                :bit_depth,
                :deliver_booklet,
                :version,
                :test_delivery,
                :track_only,
                :created_with_songwriter,
                :resource_contributors

  def initialize(options = {})
    @store_id       = options[:store_id]
    @timestamp      = Time.now
    @delivery_type  = "full_delivery" if @delivery_type.nil? || @delivery_type == ""
    @uuid           = nil
    @track_only     = false

    options.each { |k, v| send("#{k}=", v) if respond_to?(k) }
  end

  def duration
    tracks.map(&:duration).compact.sum
  end

  def get_duration(format = "ss")
    case format
    when "ss"
      duration.to_s
    when "mm:ss"
      if duration && duration.positive?
        seconds = duration % 60
        minutes = duration / 60
        if minutes < 10
          (seconds < 10) ? "0#{minutes}:0#{seconds}" : "0#{minutes}:#{seconds}"
        else
          (seconds < 10) ? "#{minutes}:0#{seconds}" : "#{minutes}:#{seconds}"
        end
      else
        "0#{minutes}:0#{seconds}"
      end
    else
      raise "get_duration exception, format #{format} unsupported"
    end
  end

  def sale_date
    return products.first.sales_start_date if respond_to?(:products)
  end

  def get_uuid
    require "uuid" unless defined? UUID
    @uuid = UUID.new if @uuid.nil?
    @uuid.generate :compact
  end

  def metadata_only_delivery?
    delivery_type == "metadata_only"
  end

  def set_data_from_config(config)
    self.party_id               = config["party_id"]
    self.party_full_name        = config["party_full_name"]
    self.audio_codec_type       = config["file_type"]               || "flac"
    self.use_proprietary_id     = config["use_proprietary_id"]      || false
    self.ddex_version           = config["ddex_version"]            || "341"
    self.album_price_code       = config["album_price_code"]        || "AMid"
    self.track_price_code       = config["track_price_code"]        || "TMid"
    self.display_track_id       = config["display_track_id"]        || false
    self.display_track_language = config["display_track_language"]  || false
    self.include_behalf         = config["include_behalf"]          || false
    self.skip_media             = config["skip_media"]              || false
    self.image_size             = config["image_size"].to_i
    self.channels               = config["channels"]                || false
    self.include_assets         = config["include_assets"]          || true
    self.normalize_bit_depth    = config["normalize_bit_depth"]     || false
    self.bit_depth              = config["bit_depth"]               || 16
    self.deliver_booklet        = config["deliver_booklet"]
    self.test_delivery          = config["test_delivery"]
  end

  def skip_image?
    image_size&.zero?
  end

  def valid?
    true
  end

  def deliver_assets?
    @delivery_type == "full_delivery" && !@takedown
  end

  def message_control
    test_delivery ? "TestMessage" : "LiveMessage"
  end
end
