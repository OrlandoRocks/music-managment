class DistributionSystem::Bundle
  include DistributionSystem::Transcodable

  attr_reader :dir, :album, :dirname, :incoming_dir, :feed_dir, :feed_name, :preset, :transcoder

  def initialize(work_dir, transcoder, s3, album, store_delivery_config = nil)
    raise "Invalid Album" unless album.valid?

    @album      = album
    @work_dir   = work_dir
    @provider   = store_delivery_config.try(:provider)
    @store_dir  = File.join(@work_dir, (store_delivery_config.try(:store).try(:short_name) || SecureRandom.uuid))
    @transcoder = transcoder
    @s3         = s3
    @logger     = Rails.logger

    set_data_from_config(store_delivery_config) if store_delivery_config
  end

  def set_data_from_config(store_delivery_config)
    @remote_dir             = store_delivery_config["remote_dir"]             || "."
    @skip_batching          = store_delivery_config["skip_batching"]          || false
    @remote_dir_timestamped = store_delivery_config["remote_dir_timestamped"] || false
    @use_resource_dir       = store_delivery_config["use_resource_dir"]
    @use_manifest           = store_delivery_config["use_manifest"]
    @album.set_data_from_config(store_delivery_config)
  end

  def create_bundle
    @dirname = File.join(@store_dir, album.upc.to_s)
    FileUtils.mkdir_p(@dirname)
    @dir = Dir.new(@dirname)
  end

  def handle_transcoding(track, original_file)
    transcode_file?(track) ? transcode_track(track) : move_file_to_destination(track, original_file)
  end

  def transcode_file?(track)
    track.s3_key.split(".").last != "flac" && track.album.audio_codec_type != "flac"
  end

  def move_file_to_destination(track, original_file)
    FileUtils.cp(original_file, track.audio_file)
  end
end
