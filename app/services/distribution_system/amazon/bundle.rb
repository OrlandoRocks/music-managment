module DistributionSystem::Amazon
  class Bundle < DistributionSystem::Believe::Bundle
    def initialize(work_dir, transcoder, s3, album, store_delivery_config)
      super
      @send_batch_complete = false
      @timestamp = Time.now.strftime("%Y%m%d%H%M%S")
    end

    def full_remote_dir
      if remote_dir_timestamped
        "#{album.upc}_#{@timestamp}"
      else
        album.upc.to_s
      end
    end
  end
end
