module DistributionSystem::Amazon
  class Product
    def initialize(options = {})
      # Defaults
      options = { territory: "world" }.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    # Metadata elements
    attr_accessor :sales_start_date,
                  :territory,
                  :wholesale_price_tier,
                  :sales_end_date,
                  :cleared_for_sale,
                  :aod_retail,
                  :takedown,
                  :currency

    def valid?
      # Required fields
      return false if territory.nil?
      return false if wholesale_price_tier.nil?
      return false if cleared_for_sale.nil?

      # Check Values
      return false if sales_start_date && !sales_start_date.respond_to?(:strftime)

      true
    end

    def is_number?(string)
      true if Float(string) rescue false
    end
  end
end
