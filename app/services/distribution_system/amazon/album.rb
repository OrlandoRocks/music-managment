module DistributionSystem::Amazon
  class Album < DistributionSystem::Believe::Album
    xml_format :believe
    xml_validation_with :ddex

    attr_accessor :vendor_code,
                  :grid,
                  :title_version,
                  :related_albums,
                  :delivery_type,
                  :related_upc,
                  :wholesale_price_tier,
                  :products,
                  :stores,
                  :takedown_countries

    def validate_album_attributes
      [
        @upc,
        @title,
        @release_date,
        @label_name,
        @products,
        @genres,
        @artwork_s3_key,
        @explicit_lyrics
      ].each do |required_field|
        return false if required_field.nil?
        return false if required_field.to_s.length.zero?
      end
      true
    end

    def commercial_model_usages
      [
        { commercial_model: "PayAsYouGoModel",   use_type: "PermanentDownload" },
        { commercial_model: "SubscriptionModel", use_type: "PrimeOnDemandStream" },
        { commercial_model: "SubscriptionModel", use_type: "OnDemandStream" }
      ]
    end

    def takedown_products
      products.select { |p| takedown_countries.include?(p.territory) } if takedown_countries.present?
    end

    def all_products
      takedown_products.present? ? products - takedown_products : products
    end

    def wholesale_price_tier
      products.first.wholesale_price_tier
    end

    def instrumental?
      genre == Genre::INSTRUMENTAL_GENRE_ID
    end
  end
end
