module DistributionSystem::Amazon
  class GenreHelper
    def self.get_genres(genres)
      amazon_genres = []
      genres.map do |genre|
        amazon_genre = get_amazon_genre(genre.id)
        amazon_genres.push(amazon_genre)
      end
      amazon_genres
    end

    def self.get_amazon_genre(genre_id)
      case genre_id
      when 1
        "ALTERNATIVE & INDIE"
      when 3
        "BLUES"
      when 4
        "CHILDREN'S MUSIC"
      when 5
        "CLASSICAL"
      when 6
        "COMEDY"
      when 7
        "DANCE & ELECTRONIC"
      when 8
        "DANCE & ELECTRONIC"
      when 9
        "FOLK"
      when 10
        "WORLD/EUROPE/FRANCE"
      when 11
        "GERMAN/ROCK"
      when 12
        "GERMAN/POP"
      when 13
        "RAP & HIP-HOP"
      when 14
        "MISCELLANEOUS/HOLIDAY"
      when 15
        "CHRISTIAN/PRAISE & WORSHIP"
      when 16
        "JAZZ"
      when 17
        "LATIN MUSIC"
      when 18
        "NEW AGE"
      when 19
        "OPERA & CHORAL"
      when 20
        "POP"
      when 21
        "R&B/SOUL"
      when 22
        "REGGAE"
      when 23
        "ROCK"
      when 24
        "SOUNDTRACKS"
      when 26
        "BROADWAY & VOCALISTS"
      when 27
        "WORLD"
      when 28
        "COUNTRY/ALT-COUNTRY & AMERICANA"
      when 29
        "COUNTRY"
      when 30
        "SPOKEN WORD"
      when 31
        "HARD ROCK & METAL"
      when 32
        "J-POP"
      when 33
        "WORLD/FAR EAST & ASIA/KOREA"
      when 34
        "ALTERNATIVE & INDIE/SINGER-SONGWRITERS"
      when 35
        "JAZZ/BIG BAND & ORCHESTRAL JAZZ"
      when 36
        "MISCELLANEOUS/EXERCISE"
      when 37
        "CLASSICAL/CLASSICAL (C. 1770-1830)"
      when 38
        "MISCELLANEOUS"
      when 39
        "MISCELLANEOUS"
      when (40..100)
        "WORLD"
      when 101
        "AMBIENT"
      when 102
        "BRAZILIAN"
      when 103
        "BALADAS"
      when 104
        "BOLEROS"
      when 105
        "CARIBBEAN"
      when 106
        "CUBAN"
      when 107
        "LATIN HIP-HOP"
      when 108
        "LATIN TRAP"
      when 109
        "LATIN URBAN"
      when 110
        "RANCHERA"
      when 111
        "REGIONAL MEXICANO"
      when 112
        "SALSA/MERENGUE"
      when 113
        "TANGO"
      when 114
        "TROPICAL"
      when 115
        "AXÉ"
      when 116
        "BAILE FUNK"
      when 117
        "BOSSA NOVA"
      when 118
        "CHORINHO"
      when 119
        "FORRÓ"
      when 120
        "FREVO"
      when 121
        "MPB"
      when 122
        "PAGODE"
      when 123
        "SAMBA"
      when 124
        "SERTANEJOS"
      when 125
        "AFRICAN"
      when 126
        "AFROBEATS"
      when 127
        "AFROPOP"
      when 128
        "AFRO-FUSION"
      when 129
        "AFRO-SOUL"
      when 130
        "AFRO HOUSE"
      when 131
        "AMAPIANO"
      when 132
        "BONGO FLAVA"
      when 133
        "HIGHLIFE"
      when 134
        "MASKANDI"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
