module DistributionSystem::Amazon
  class Converter < DistributionSystem::Believe::Converter
    description "Amazon Music"
    handle_salepoints(/^Amazon$/)
    ALBUM_KLASS  = DistributionSystem::Amazon::Album
    TRACK_KLASS  = DistributionSystem::Amazon::Track

    def initialize
      super
      @supports_album_only = true
    end

    def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
      build_album(tunecore_album, tc_salepoints, delivery_type)
      build_tracks(tunecore_album)
      unless album.validate_album_attributes
        raise "Album validation failed. One or more required attributes are missing."
      end

      album
    end

    def add_track(album, song, _tunecore_album, salepoints)
      track = Track.new(
        {
          album: album,
          number: song.track_num.to_s.rjust(3, "0"),
          artists: set_song_artists(song),
          sku: song.sku,
          isrc: DistributionSystem::StringExt.fmt(song.isrc, "##-###-##-#####", "", true),
          explicit_lyrics: song.parental_advisory,
          orig_filename: song.upload.uploaded_filename,
          temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
          s3_key: song_s3key(song),
          s3_bucket: song_s3bucket(song),
          album_only: song.album_only,
          available_for_streaming: use_attribute_if_available?(:available_for_streaming, song),
          free_song: song.free_song,
          asset_url: song.external_asset_url,
          language_code: track_language_code(song)
        }
      )

      if (salepoints && salepoints.length.positive? && salepoints.last.track_variable_price)
        track.wholesale_price_tier = salepoints.last.track_variable_price.price_code
      elsif ["CATALOG", "EXCLUSIVE", "FRONTLINE", "MIDLINE", "SPECIAL"].include?(album.wholesale_price_tier)
        track.wholesale_price_tier = album.wholesale_price_tier
      else
        track.wholesale_price_tier = "FRONTLINE"
      end

      track.title = song.name
      album.tracks << track
    end

    def set_label(tunecore_album)
      if tunecore_album.label
        if tunecore_album.label.instance_of?(Label)
          tunecore_album.label.name
        else
          tunecore_album.label.to_s
        end
      else
        tunecore_album.artist_name
      end
    end

    def set_products(tunecore_album, salepoints)
      products = []

      salepoints.map do |salepoint|
        enabled_isos = tunecore_album.country_iso_codes
        enabled_isos.each do |territory|
          raise "Missing price_code (variable_price_id)" if salepoint.price_code.nil?

          product = DistributionSystem::Amazon::Product.new
          product.takedown = set_takedown(tunecore_album, salepoints, "Amazon")
          product.takedown = true unless enabled_isos.include?(territory)
          product.territory = territory
          product.wholesale_price_tier = (tunecore_album.all_songs_are_free? && territory == "US") ? "PROMOTIONAL" : salepoint.price_code.upcase
          product.sales_start_date = tunecore_album.sale_date
          product.cleared_for_sale = "IMMEDIATE"

          products.push(product)
        end
      end

      products
    end

    def set_delivery_type(_tunecore_album, _salepoint, delivery_type)
      delivery_type
    end

    def set_genres(tunecore_album)
      GenreHelper.get_genres(tunecore_album.genres)
    end

    def set_stores(tc_salepoints)
      tc_salepoints.map { |salepoint| salepoint.store.short_name }
    end

    def build_album(tunecore_album, tc_salepoints, delivery_type)
      super
      album.products           = set_products(tunecore_album, salepoints)
      album.genres             = set_genres(tunecore_album)
      album.stores             = set_stores(salepoints)
      album.title              = tunecore_album.name
      album.takedown_countries = tunecore_album.excluded_countries.map(&:iso_code)
    end

    def build_tracks(tunecore_album)
      tunecore_album.songs.each do |song|
        add_track(album, song, tunecore_album, salepoints)
      end
    end
  end
end
