module DistributionSystem::Amazon
  class Track < DistributionSystem::Believe::Track
    xml_format :believe
    xml_validation_with :ddex

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :wholesale_price_tier     # 2014-04-07 Amit Gulati: support new request from vip to customize track level prices for amazon and google
  end
end
