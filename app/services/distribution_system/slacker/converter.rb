module DistributionSystem::Slacker
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Slacker/)
  end
end
