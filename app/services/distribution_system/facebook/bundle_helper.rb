require "tmpdir"
require File.dirname(__FILE__) + "/../image_utils"

module DistributionSystem::Facebook
  # helper class to create the Facebook bundle.
  class BundleHelper
    attr_reader :dir, :album, :dirname, :incoming_dir, :feed_dir, :feed_name

    def initialize(work_dir, provider_id, transcoder, s3, album)
      raise "Invalid Album" unless album.valid?

      @album = album
      @work_dir = work_dir
      @provider = provider_id
      @facebook_dir = File.join(@work_dir, "Facebook")
      @transcoder = transcoder
      @s3 = s3
      @preset = :facebook
    end

    def create_bundle
      upc = @album.upc
      @dirname = File.join(@facebook_dir, upc.to_s)
      FileUtils.mkdir_p(@dirname)
      @dir = Dir.new(@dirname)
    end

    def write_metadata
      File.open(File.join(@dir.path, @album.upc.to_s + ".xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
        @album.vendor_code = @provider
        @album.to_xml(xml)
      end
    end

    def collect_files
      image_source = File.join(@dir.path, "#{@album.upc}.jpg")
      @s3.get(image_source, @album.artwork_file.asset)
      DistributionSystem::ImageUtils.resize(image_source, 1400, 1400)
      @album.artwork_file.asset = image_source
      # get the album tracks
      @album.tracks.each do |track|
        orig = original_file(track)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(orig, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)
        transcode_track(track)
        raise "Transcoding failed because file #{track.audio_file} doesn't exist!" unless File.exist?(track.audio_file)

        FileUtils.rm(orig)
      end
    end

    def track_filename(path, track)
      File.join(path, "#{track.album.upc}_01_#{track.number}.mp3")
    end
  end
end
