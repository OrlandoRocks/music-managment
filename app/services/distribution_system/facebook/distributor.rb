require_relative "../distributor"

module DistributionSystem::Facebook
  class Distributor < DistributionSystem::Distributor
    attr_accessor :logger

    def initialize(options)
      @work_dir = options[:work_dir]
      @facebook_store = options[:facebook_store]
      super
    end

    def distribute(album, distribution)
      album.logger = @logger
      album.tracks.each do |track|
        track.logger = @logger
      end
      @logger.info "Preparing to upload album #{album.upc}| album_id=#{album.tunecore_id}"
      raise "Invalid Album" unless album.valid?

      begin
        @bundle = BundleHelper.new(@work_dir, @facebook_store.facebook_provider, @transcoder, @s3, album)
        @bundle.create_bundle
        @logger.info "Preparing to collect album's assets."
        Distribution::StateUpdateService.update(distribution, { state: "gathering_assets" })
        @bundle.collect_files
        @logger.info "Preparing to write the metadata file."
        @bundle.write_metadata
        Distribution::StateUpdateService.update(distribution, { state: "packaged" })
        send_to_facebook!
      end
    end

    def send_to_facebook!
      valid = @facebook_store.validate_bundle(@bundle)
      @logger.info "completed validation of the #{@bundle.dirname} and package is valid? #{valid} (schema: #{@facebook_store.schema.inspect})| album_id=#{@bundle.album.tunecore_id}"
      unless valid
        raise DistributionSystem::DeliveryError.new("Invalid Bundle: schema validation failure: #{@facebook_store.schema.inspect}")
      end

      @logger.info "Sending to Facebook #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id}"
      shipped = @facebook_store.send_bundle(@bundle)
      @logger.info "completed shipping of the #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id}"
    end
  end
end
