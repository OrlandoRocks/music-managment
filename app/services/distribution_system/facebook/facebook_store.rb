require_relative "../shell"
require "builder"

module DistributionSystem::Facebook
  class FacebookStore
    attr_accessor :facebook_provider
    attr_reader :schema

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No facebook directory!"
      @facebook_dir = File.join(@work_dir, "Facebook")
      @logger = options[:logger] or raise "No logger!"
      @facebook_server = options[:facebook_server] or raise "No facebook server!"
      @remote_root = options[:remote_dir]
      @schema = options[:schema]
    end

    def validate_bundle(bundle)
      return true unless @schema

      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"), true)
    end

    def send_bundle(bundle)
      time = Time.now.strftime("%Y%m%d%H%M%S").to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s

      bundle_id = bundle.album.upc

      batch_dir = @remote_root ? "#{@remote_root}/#{time}" : time.to_s
      bundle_dir = "#{batch_dir}/#{bundle_id}"
      remote_dir = "#{bundle_dir}/resources"

      @facebook_server.mkdir_p(@remote_root)
      @facebook_server.mkdir_p(batch_dir)
      @facebook_server.mkdir_p(bundle_dir)
      @facebook_server.mkdir_p(remote_dir)

      if bundle.album.delivery_type == "full_delivery"
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(remote_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @facebook_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_01_#{track.number}.mp3")
          remote_track_file = File.join(remote_dir, "#{bundle_id}_01_#{track.number}.mp3")
          @facebook_server.upload(track_file, remote_track_file)
        end
      else
        @logger.info "Not delivering full assets because delivery_type is #{bundle.album.delivery_type}"
      end

      @logger.info "  uploading metadata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(remote_dir + "/../", "#{bundle_id}.xml")
      @facebook_server.upload(metadata_filename, remote_metadata_filename)
      `touch /tmp/BatchComplete_#{time}.xml`
      @facebook_server.upload("/tmp/BatchComplete_#{time}.xml", "#{batch_dir}/BatchComplete_#{time}.xml")
      @facebook_server.start_processing

      true
    end
  end
end
