require_relative "../track"
require_relative "../track_length"

module DistributionSystem::Facebook
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger,
                  :artists

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      @duration = iso8601(@audio_file)

      xml.SoundRecording do |sound_recording|
        sound_recording.SoundRecordingId do |sound_recording_id|
          sound_recording_id.ISRC isrc
        end
        sound_recording.ResourceReference "A#{number}"
        sound_recording.ReferenceTitle do |reference_title|
          reference_title.TitleText title
        end
        sound_recording.Duration @duration
        sound_recording.SoundRecordingDetailsByTerritory do |recording_details|
          recording_details.TerritoryCode "Worldwide"
          recording_details.Title(TitleType: "FormalTitle") do |_title|
            _title.TitleText title
          end
          recording_details.Title(TitleType: "DisplayTitle") do |_title|
            _title.TitleText title
          end
          recording_details.Title(TitleType: "AbbreviatedDisplayTitle") do |_title|
            _title.TitleText title
          end

          produce_artist_info(album, recording_details, self)

          recording_details.PLine do |pline|
            pline.Year album.copyright_pline.split(" ")[0]
            pline.PLineText album.copyright_pline
          end
          recording_details.Genre do |genre|
            genre.GenreText album.genres.first
          end
          unless album.takedown
            recording_details.TechnicalSoundRecordingDetails do |technical_details|
              technical_details.TechnicalResourceDetailsReference "T#{number}"
              technical_details.AudioCodecType(
                { "UserDefinedValue" => "FLAC", "Namespace" => DDEX_SENDER_ID.to_s },
                "UserDefined"
              )
              technical_details.NumberOfChannels 2
              technical_details.SamplingRate 44.1
              technical_details.IsPreview false
              technical_details.File do |file|
                file.FileName audio_file.split("/").last
                file.FilePath "resources/"
                file.HashSum do |hash_sum|
                  hash_sum.HashSum DistributionSystem::CheckSumHelper::checksum_file(audio_file)
                  hash_sum.HashSumAlgorithmType "MD5"
                end
              end
            end
          end # end if
        end
      end
    end
  end
end
