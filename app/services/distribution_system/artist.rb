module DistributionSystem
  class Artist
    # Metadata elements
    attr_accessor :name,
                  #  2013-06-19 Koh Asakura: for Japanese translation.
                  :english_name,
                  :phonetic_name,
                  :japanese_name,
                  #  2013-07-22 Koh Asakura: for artist's id
                  :artist_id,
                  #  2013-07-22 Koh Asakura: for artist's url about hpmepage, blog, etc...
                  :url_official,
                  :url_blog,
                  :url_facebook,
                  :url_twitter,
                  :url_youtube_channel

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def valid?
      true
    end
  end
end
