module DistributionSystem::Anghami
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Anghami/)
  end
end
