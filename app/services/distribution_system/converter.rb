class DistributionSystem::Converter
  attr_accessor :supports_metadata_update, :supports_takedown, :supports_album_only, :supports_track_selection

  def initialize
    @supports_metadata_update       = false
    @supports_takedown              = false
    @supports_album_only            = false
    @supports_metadata_with_assets  = false
    @supports_track_selection       = false
  end

  def supports_metadata_update?
    @supports_metadata_update
  end

  def supports_takedown?
    @supports_takedown
  end

  def supports_album_only?
    @supports_album_only
  end

  def supports_metadata_with_assets?
    @supports_metadata_with_assets
  end

  def supports_track_selection?
    @supports_track_selection
  end

  def set_takedown(tunecore_album, tc_salepoints, store_short_name)
    salepoint = tc_salepoints.find { |s| s.store.short_name == store_short_name }
    tunecore_album.takedown? || (!salepoint.nil? && salepoint.taken_down?)
  end

  def self.salepoint_matcher
    @salepoint_matcher
  end

  def set_takedown_at(tunecore_album, tc_salepoints, store_short_name)
    salepoint = tc_salepoints.find { |s| s.store.short_name == store_short_name }
    if tunecore_album.takedown?
      tunecore_album.takedown_at.strftime("%Y%m%d")
    elsif !salepoint.nil? && salepoint.taken_down?
      salepoint.takedown_at.strftime("%Y%m%d")
    else
      nil
    end
  end

  def self.inherited(subclass)
    subclass.class_eval do
      def self.handle_salepoints(regexp)
        @salepoint_matcher = regexp
      end

      def self.description(description)
        @description = description
      end

      def handles_store?(store)
        self.class.instance_eval do
          raise "#{self} does not define what salepoints it handles." unless @salepoint_matcher

          store.short_name =~ @salepoint_matcher
        end
      end

      def description
        self.class.instance_eval do
          @description
        end
      end

      def countries(album)
        album.country_iso_codes.map do |code|
          Country::COUNTRIES_MAP[code] || code
        end.uniq
      end

      def song_s3key(song)
        # 2010-12-08 AK Add support for bigbox assets
        #  2012-07-25 Takashi Egawa: TunecoreSong doesn't have 's3_asset', but 's3_orig_asset'
        return song.s3_asset.key if song.s3_asset != nil && song.s3_asset.uuid != nil

        return unless song.s3_orig_asset || song.s3_flac_asset

        (song.s3_orig_asset || song.s3_flac_asset).key
      end

      def song_s3bucket(song)
        # 2010-12-08 AK Add support for bigbox assets
        #  2012-07-25 Takashi Egawa: TunecoreSong doesn't have 's3_asset', but 's3_orig_asset'
        #  2013-01-31 Takashi Egawa: change 's3_orig_asset' to 's3_asset', and modify album_factory.
        return song.s3_asset.bucket if song.s3_asset != nil && song.s3_asset.uuid != nil

        return unless song.s3_orig_asset || song.s3_flac_asset

        (song.s3_orig_asset || song.s3_flac_asset).bucket
      end

      def cline(tunecore_album)
        date_to_use = tunecore_album.orig_release_year || tunecore_album.sale_date || Date.today
        "#{date_to_use.year} #{tunecore_album.copyright_name}"
      end

      alias :pline :cline
    end
  end

  #  each music store sub module should implement this method and returns a music store specific album.
  def convert(_tunecore_album, _salepoints, _delivery_type = "full_delivery")
    raise "#{self.class} does not define method convert(tunecore_album, salepoints, delivery_type=\"full_delivery\")"
  end

  def set_artist(media)
    # media can be an instance of tunecore's song or album
    artist_array = []
    if media.creatives.empty?
      a_artist = self.class::ARTIST_KLASS.new
      a_artist.name    = media.artist_name
      a_artist.role    = "primary"
      artist_array << a_artist
    else
      media.creatives.each do |creative|
        a_artist = self.class::ARTIST_KLASS.new
        a_artist.name    = creative.artist.name
        a_artist.role    = creative.role
        artist_array << a_artist
      end

      # 2012-1-12 AK
      # if there's a creative on the track level, also include the album level artists in the track level
      # unless the album is_various
      if media.instance_of?(Song) && !media.album.is_various
        a_artist = self.class::ARTIST_KLASS.new
        a_artist.name    = media.album.artist_name
        a_artist.role    = "primary"
        artist_array << a_artist
      end

      # 2012-05-09 AK
      # If there's a featured artist on a track, also include the album level artist as primary artist
      if media.instance_of?(Song) && !media.creatives.detect { |c|
           c.role == "featuring"
         } && !media.album.artist_name != "Various Artists"

        a_artist = self.class::ARTIST_KLASS.new
        a_artist.name    = media.album.artist_name
        a_artist.role    = "primary"
        artist_array << a_artist
      end
    end

    artist_array
  end

  # Latest get artists method that handles various artist collection properly
  # When the method is proven to work on youtube, then we can swap
  # all other converters to use this method
  def get_album_artists(album)
    artists = []
    c_artist = "#{self.class.name.gsub("::#{self.class.name.demodulize}", '')}::Artist".constantize
    if album.is_various
      artists << c_artist.new(name: album.artist_name, role: "primary_artist")
    else
      artists = get_artists(album)
    end

    artists.flatten
  end

  def get_track_artists(song)
    album_artists = []
    song_artists = []
    unless song.album.is_various
      # when album is not various artist, we add the album's artist
      # on top of the song's artist
      album_artists = get_artists(song.album)
    end

    song_artists = get_artists(song)

    # remove potential artist duplications
    album_artist_names = album_artists.collect(&:name)
    artists = album_artists + song_artists.select { |a| !album_artist_names.include?(a.name) }

    artists.flatten
  end

  def use_attribute_if_available?(attribute, object)
    object.respond_to?(attribute) ? object.send(attribute) : true
  end

  def calculate_duration(song)
    song.duration
  end

  def temp_filename_ext(song)
    song.s3_flac_asset_id.present? ? ".flac" : File.extname(song.upload.uploaded_filename)
  end

  def s3_flac_key(song)
    song.s3_flac_asset&.key || song_s3key(song)
  end

  def s3_flac_bucket(song)
    song.s3_flac_asset&.bucket || song_s3bucket(song)
  end

  def get_language_code(album)
    if FeatureFlipper.show_feature?(:track_level_language, album.person)
      album.metadata_language_code&.code
    else
      album.language_code
    end
  end

  protected

  def get_artists(media)
    # Due to namespacing issue, we need to figure out the correct Artist
    # class to call
    c_artist = "#{self.class.name.gsub("::#{self.class.name.demodulize}", '')}::Artist".constantize
    artists = []
    media.creatives.each do |creative|
      artists << c_artist.new(name: creative.artist.name, role: creative.role)
    end

    artists
  end
end
