module DistributionSystem::Gracenote
  class Creative < DistributionSystem::Creative
    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v) if respond_to? k
      end
    end

    def primary
      non_primary_roles = %w[Featuring Composer Producer Engineer]

      if non_primary_roles.include? @role
        false
      else
        true
      end
    end

    # 2010-01-08 -- ED -- should be upto date with itunes music 4.0 xml spec
    def to_xml(xml = Builder::XmlMarkup.new)
      xml.artist do
        #  2012-07-25 Takashi Egawa: name has 3 names
        xml.name name, "translation" => "native"

        xml.name  @phonetic_name, "translation" => "phonetic" if @phonetic_name

        xml.name  @english_name, "translation" => "english" if @english_name

        #  2012-09-03 Takashi Egawa: adds xml output for Japanese translation
        xml.name  @japanese_name, "translation" => "japanese" if @japanese_name

        xml.roles do |r|
          r.role role
        end
        xml.primary primary.to_s
      end
    end

    def valid?
      return false if @name.to_s == ""
      return false unless @primary.to_s == "true" || @primary.to_s == "false"

      true
    end
  end
end
