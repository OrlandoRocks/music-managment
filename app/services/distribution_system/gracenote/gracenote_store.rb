module DistributionSystem::Gracenote
  class GracenoteStore
    attr_accessor :gracenote_provider

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No gracenote directory!"
      @gracenote_dir = File.join(@work_dir, "Gracenote")
      @logger = options[:logger] or raise "No logger!"
      @gracenote_server = options[:gracenote_server] or raise "No store server!"
      @remote_root = options[:remote_dir]
    end

    def validate_package(bundle)
      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"))
    end

    def send_package(bundle)
      bundle_id = bundle.album.upc

      date = Time.now.strftime("%Y%m%d")
      batch_dir = "#{@remote_root}/#{date}"
      release_dir = "#{batch_dir}/#{bundle_id}"
      resource_dir = release_dir.to_s

      @gracenote_server.mkdir_p batch_dir
      @gracenote_server.mkdir_p release_dir

      unless bundle.album.takedown
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @gracenote_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_#{track.number}.mp3")
          remote_track_file = File.join(resource_dir, "#{bundle_id}_#{track.number}.mp3")
          @gracenote_server.upload(track_file, remote_track_file)
        end
      end

      @logger.info "  uploading metatata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(release_dir, "#{bundle_id}.xml")
      @gracenote_server.upload(metadata_filename, remote_metadata_filename)
      `touch /tmp/delivery.complete`
      @gracenote_server.upload("/tmp/delivery.complete", "#{release_dir}/DELIVERY.COMPLETE.dat")
      @gracenote_server.start_processing

      true
    end
  end
end
