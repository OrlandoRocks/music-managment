module DistributionSystem::Gracenote
  class Bundle < DistributionSystem::Bundle
    include DistributionSystem::TrackLength
    attr_reader :success_dir, :failure_dir

    def write_metadata
      File.open(File.join(@dir.path, "#{@album.upc}.xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "UTF-8")
        @album.to_xml(xml, @provider)
      end
    end

    def collect_files
      @preset = :gracenote
      artwork_extension = @album.artwork_s3_key.split(".").last
      image_source = File.join(@dir.path, "#{@album.upc}.#{artwork_extension}")
      image_dest = image_source.split(".")[0..-2].join(".") + ".jpg"

      unless @album.takedown
        @s3.get(image_source, @album.artwork_s3_key)
        DistributionSystem::ImageUtils.convert_image(image_source, image_dest)
        DistributionSystem::ImageUtils.resize(image_dest, 1400, 1400)
      end

      @album.artwork_file.asset = image_dest
      if @album.booklet_file
        booklet_source = File.join(@dir.path, "#{@album.upc}_booklet.pdf")
        @s3.get(booklet_source, @album.booklet_file.asset)
        @album.booklet_file.asset = booklet_source
      end

      @album.tracks.each do |track|
        next if @album.takedown

        orig = original_file(track)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(orig, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)

        @transcoder.until = nil
        if @album.album_type == "ringtone"
          duration = seconds(orig)
          @transcoder.until = "00:30.00" if duration.to_f >= 30.0
        else
          @transcoder.until = nil
        end

        transcode_track(track)

        FileUtils.rm(orig)
      end
    end

    def track_filename(path, track)
      File.join(path, "#{track.album.upc}_01_#{track.number}.mp3")
    end
  end
end
