module DistributionSystem::Gracenote
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    # Itunes specific metadata elements
    attr_accessor :grid,
                  :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :products

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.track do
        #          xml.track_vendor_id           sku
        xml.type album.album_type if !album.album_type.nil? && album.album_type == "ringtone"
        xml.isrc                isrc
        xml.grid                grid if grid
        #  title has 3 names
        xml.title title, "translation" => "native"
        xml.title   @phonetic_title, "translation" => "phonetic" if @phonetic_title
        xml.title   @english_title, "translation" => "english" if @english_title
        #  2012-09-03 Takashi Egawa: adds xml output for Japanese translation
        xml.title   @japanese_title, "translation" => "japanese" if @japanese_title

        xml.title_version       title_version if title_version
        xml.gapless_play        gapless_play if gapless_play
        xml.label_name          label_name if label_name

        if genres
          xml.genres do
            genres.each { |g| xml.genre g }
          end
        end

        xml.products do |track_products|
          products.each { |p| p.track = self; p.to_xml(track_products) }
        end

        xml.copyright_pline copyright_pline if copyright_pline
        if explicit_lyrics == true
          xml.explicit_content "explicit"
          # TODO: implement this rule in the tunecore app.
          #          else explicit_lyrics # there is a explicit version out there but this is the clean version
          #              xml.explicit_content "clean"
        end
        xml.track_lyrics lyrics if lyrics
        xml.track_liner_notes liner_notes if (liner_notes && !album.album_type.nil? && album.album_type != "ringtone")
        xml.track_beats_per_minute beats_per_minute if beats_per_minute
        xml.track_number number

        if album.delivery_type == "full_delivery"
          xml.audio_file do
            xml.file_name(File.basename(audio_file))
            #  2012-07-25 Takashi Egawa: size required
            xml.size(File.size(audio_file))
            xml.checksum(DistributionSystem::CheckSumHelper::checksum_file(audio_file))
          end
        end

        xml.preview_start_index preview_start_index if preview_start_index

        if creatives.empty?
          xml.artists do
            @album.creatives.each { |c| c.to_xml(xml) }
          end
        else
          has_primary = false
          xml.artists do
            creatives.each do |c|
              has_primary = true if c.primary
              c.to_xml(xml)
            end
            unless has_primary
              @album.creatives.each do |c|
                c.to_xml(xml) if c.primary
              end
            end
          end
        end
      end
    end
  end
end
