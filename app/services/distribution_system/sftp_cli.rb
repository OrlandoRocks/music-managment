class DistributionSystem::SftpCli
  def initialize(delivery_config)
    @host           = delivery_config[:hostname]  or raise "No hostname"
    @username       = delivery_config[:username]  or raise "No username."
    @port           = delivery_config[:port] || 22
    @password       = delivery_config[:password]
    @remote_dir     = delivery_config[:remote_dir]
    @keyfile        = File.expand_path(
      File.join(
        SSH_KEYFILE_DIR,
        (delivery_config[:keyfile]).to_s
      )
    ) if delivery_config[:keyfile]
    @batch_commands = []

    # set a dummy password to use as a placeholder if there is no password
    @password = "dummy_password" if @password.nil? || @password == ""
  end

  attr_reader :batch_commands

  # add a batch command to create a remote dir
  def mkdir_p(dir_name)
    add_command "-mkdir #{dir_name}"
  end

  # add an upload line to the batch_commands
  def upload(local_file, remote_file)
    add_command "put #{local_file} #{remote_file}"
  end

  # add an rename line to the batch_commands
  def rename(old_file, new_file)
    add_command "rename #{old_file} #{new_file}"
  end

  # Connect to the server and execute @batch_commands
  def start_processing
    batch_filename = create_batch_file
    cmd = "#{Rails.root}/bin/sftp.exp #{@host} #{@username} #{@password} #{@port} #{batch_filename} #{@keyfile} 2>&1"

    output = `#{cmd}`
    Rails.logger.info "SFTP output: #{output}"
    raise DistributionSystem::DeliveryError.new("SFTP session failed:\n#{output}") unless $? == 0

    File.delete(batch_filename)
    true
  end

  private

  def add_command(cmd)
    @batch_commands << cmd
  end

  # Creates the batch file and clears the @batch_commands
  def create_batch_file
    batch_file = File.new("#{Rails.root}/tmp/petri-sftp-#{Process.pid}.#{rand(9999)}.txt", "w+")
    Rails.logger.debug "Using batch file #{batch_file.path} for SFTP session"
    @batch_commands.each { |c| batch_file.puts c }
    @batch_commands = []
    batch_file.close
    batch_file.path
  end
end
