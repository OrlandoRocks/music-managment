module DistributionSystem
  class CheckSumHelper
    def self.checksum_file(filename)
      `md5sum -b #{filename}`.split(" ").first
    end

    def self.algorithm
      "md5"
    end
  end
end
