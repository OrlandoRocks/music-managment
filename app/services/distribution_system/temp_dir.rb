module DistributionSystem
  class TempDir
    @seq = 0
    @defaults = {
      delete: true,
      base: Dir.tmpdir
    }
    def self.enter(options = {})
      config = OpenStruct.new(@defaults.merge(options))
      @seq += 1
      path = File.join(config.base, $$.to_s, @seq.to_s)
      FileUtils.mkdir_p path
      begin
        yield path
      ensure
        FileUtils.rm_rf(path) if config.delete
      end
    end

    def initialize(options = {})
      @options = options
    end

    def enter(&block)
      self.class.enter(@options, &block)
    end
  end
end
