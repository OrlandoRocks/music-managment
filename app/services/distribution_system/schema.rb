module DistributionSystem
  class Schema
    attr_reader :schema_file, :errors

    def initialize(schema_file)
      @schema_file = schema_file
      @errors = []
    end

    def validate(filename, nokogiri = true)
      @errors = []
      if File.extname(@schema_file).casecmp?(".DTD")
        validate_with_dtd(filename)
      elsif nokogiri
        validate_with_nokogiri(filename)
      else
        validate_with_xsd(filename)
      end
    end

    def validate_with_xsd(filename)
      cmd = "java -jar oiTools.jar validate-schema #{@schema_file} #{filename}"
      output = `#{cmd}`
      output.include? "XML document is valid" or raise "Invalid xml: #{output}"
      true
    end

    def validate_with_dtd(_filename)
      true
      # dtd = XML::Dtd.new(File.read(@schema_file))
      # doc = XML::Document.file(filename)
      # doc.validate(dtd)
      # cmd = "java -jar oiTools.jar validate-schema dtd #{@schema_file} #{filename}"
      # output = `#{cmd}`
      # output.include? "XML document is valid" or raise "Invalid xml: #{output}"
      # return true
    end

    def validate_with_nokogiri(filename)
      require "nokogiri" unless defined? Nokogiri
      doc = Nokogiri::XML::Document.parse(File.open(schema_file))
      xsd = Nokogiri::XML::Schema.from_document(doc)
      album_xml = Nokogiri::XML(File.read(filename))
      xsd.validate(album_xml).each do |error|
        @errors.push error.message
      end
      @errors.empty?
    end

    def validate_input(input)
      require "nokogiri" unless defined? Nokogiri
      doc = Nokogiri::XML::Document.parse(File.open(schema_file))
      xsd = Nokogiri::XML::Schema.from_document(doc)
      xsd.validate(input).each do |error|
        @errors.push error.message
      end
      @errors.empty?
    end
  end
end
