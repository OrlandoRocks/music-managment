module DistributionSystem::Deezer
  class Converter < DistributionSystem::Believe::Converter
    description "Deezer Music"
    handle_salepoints(/^Deezer$/)
  end
end
