module DistributionSystem::Amazonod
  class Artist < DistributionSystem::Artist
    # Amazon specific metadata elements
    attr_accessor :role, :primary

    def initialize(options = {})
      # Defaults
      options = {
        role: "Performer",
        primary: true
      }.merge(options)

      super
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.PRIMARY_ARTIST(name)
    end

    def valid?
      return false if @name.to_s == ""

      true
    end
  end
end
