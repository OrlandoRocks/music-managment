module DistributionSystem::Amazonod
  class Bundle < DistributionSystem::Bundle
    attr_accessor :work_dir, :dirname, :dir, :timestamp, :metadata_filename, :remote_dir

    ZIPPED_TRACKS = "AudioSource.zip".freeze

    def initialize(work_dir, transcoder, s3, album, store_delivery_config)
      @work_dir      = File.join(work_dir, album.store_name, album.upc.to_s)
      @dirname       = @work_dir
      FileUtils.mkdir_p(@work_dir)
      @dir           = Dir.new(@work_dir)
      @timestamp     = Time.now.strftime("%H%M%S")
      super
      set_metadata_filename
    end

    def write_metadata
      [metadata_path, metadata_upc_path].each do |file_path|
        File.open(file_path, "w") do |f|
          xml = Builder::XmlMarkup.new(target: f, indent: 2)
          xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
          @album.to_xml(xml)
        end
      end
    end

    def set_metadata_filename
      @metadata_filename = "AudioCDMetadata_#{@album.upc}.xml"
    end

    def metadata_path
      File.join(@dir.path, @metadata_filename)
    end

    def metadata_upc_path
      File.join(@dir.path, "#{@album.upc}.xml")
    end

    def collect_files
      image_name = @album.upc
      image_source = File.join(@dir.path, "#{image_name}.tif")

      @s3.get(image_source, @album.artwork_file.asset)
      converted_image_source = File.join(@dir.path, "#{image_name}.jpg")
      DistributionSystem::ImageUtils.resize(image_source, 1417, 1417)
      DistributionSystem::ImageUtils.convert_image(image_source, converted_image_source)

      # Get the original image again since resize changes it
      # Refer : https://manufacturing.amazon.com/help?pageID=GDZJPYHU653J2AWX&language=en_US
      @s3.get(image_source, @album.artwork_file.asset)
      converted_image_source_1500 = File.join(@dir.path, "#{image_name}_1500.jpg")
      DistributionSystem::ImageUtils.resize(image_source, 1500, 1500)
      DistributionSystem::ImageUtils.convert_image(image_source, converted_image_source_1500)

      @album.artwork_file.asset = converted_image_source
      # get the album tracks
      @album.tracks.each do |track|
        orig = original_file(track)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(orig, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)
        transcode_track(track)
        track.set_duration
        FileUtils.rm(orig)
        wav_files = File.join(@dir.path, "*.wav")
        FileUtils.rm_f Dir.glob(wav_files)
      end

      zip_track_files
    end

    def track_filename(path, track)
      File.join(path, "#{track.number}_AudioTrack.flac")
    end

    def target_zipfile
      File.join(@dir.path, ZIPPED_TRACKS)
    end

    def zip_track_files
      trackfiles = Dir.glob(File.join(@dir.path, "*_AudioTrack.flac"))
      Zip::ZipFile.open(target_zipfile, Zip::ZipFile::CREATE) do |z|
        trackfiles.each do |f|
          z.add(f.split("/").pop, f)
        end
      end
    end
  end
end
