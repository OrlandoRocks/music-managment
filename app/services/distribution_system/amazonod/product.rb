module DistributionSystem::Amazonod
  class Product
    def initialize(options = {})
      # Defaults
      options = { territory: "world" }.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    # Metadata elements
    attr_accessor :sales_start_date,
                  :territory,
                  :wholesale_price_tier,
                  :sales_end_date,
                  :cleared_for_sale,
                  :aod_retail,
                  :takedown

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.TERRITORY_CODE "US"
      if takedown # allow takedowns
        xml.DISABLED_OFFER do |disabled_offer|
        end
      else
        xml.PRICE do |price|
          price.DOD_RETAIL @aod_retail
        end
      end
      xml
    end

    def valid?
      # Required fields
      # Check Values
      return false if aod_retail.nil?
    end
  end
end
