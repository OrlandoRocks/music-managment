module DistributionSystem::Amazonod
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.TRACK do |track|
        track.TRACK_INFO do |track_info|
          track_info.ISRC isrc
        end
        track.TRACK_METADATA do |track_metadata|
          track_metadata.TITLE title
          track_metadata.DISC_NUMBER "01"
          track_metadata.TRACK_NUMBER number
          artist.to_xml(track_metadata)
        end

        @album.products.each do |_product|
          track.TRACK_TERRITORY do |track_territory|
            track_territory.TERRITORY_CODE "**"
            track_territory.ALBUM_ONLY "true" if album_only
          end
        end

        if album.delivery_type == "full_delivery"
          track.AUDIO_CONTENT_FILE do |audio_content_file|
            audio_content_file.CHECKSUM DistributionSystem::CheckSumHelper.checksum_file(audio_file)
          end
        end
      end
    end

    def valid?
      return false if @isrc.nil?
      return false if @title.to_s == ""
      return false unless @number.to_i.positive?
      return false unless @artist.valid?
      return false if @album.genres.nil?

      true
    end
  end
end
