class DistributionSystem::Amazonod::Converter < DistributionSystem::Converter
  description "Amazon On Demand"
  handle_salepoints(/^AmazonOD$/)

  ALBUM_KLASS           = DistributionSystem::Amazonod::Album
  TRACK_KLASS           = DistributionSystem::Amazonod::Track
  ARTIST_KLASS          = DistributionSystem::Amazonod::Artist

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = false
    @supports_album_only      = true
  end

  def convert(tunecore_album, tc_salepoints, _delivery_type = "full_delivery")
    album = DistributionSystem::Amazonod::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        physical_upc: tunecore_album.physical_upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: DistributionSystem::Amazonod::GenreHelper.get_genres(tunecore_album.genres),
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artists: [set_artist(tunecore_album)],
        products: set_products(tunecore_album, tc_salepoints),
        artwork_file: DistributionSystem::Amazonod::CoverImage.new(asset: tunecore_album.artwork.s3_asset.key),
        delivery_type: "full_delivery", # TODO: hardcoded until we hear bar from AOD about the audio file duration tag for metadata_only deliveries
        takedown: set_takedown(tunecore_album, tc_salepoints, "AmazonOD"),
        actual_release_date: tunecore_album.sale_date
      }
    )

    album.artwork_file.aod_template_id = tc_salepoints.detect { |s| s.store.short_name =~ /^AmazonOD/ }.template_id

    tunecore_album.songs.each do |song|
      add_track(album, song)
    end

    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    album
  end

  def add_track(album, song)
    track = DistributionSystem::Amazonod::Track.new(
      {
        album: album,
        number: song.track_num.to_s.rjust(3, "0"),
        title: song.name,
        artist: DistributionSystem::Amazonod::Artist.new(name: song.artist_name),
        sku: song.sku,
        isrc: DistributionSystem::StringExt.fmt(song.isrc, "##-###-##-#####", "", true),
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
        s3_key: song_s3key(song),
        s3_bucket: song_s3bucket(song),
        album_only: song.album_only,
        free_song: song.free_song,
        asset_url: song.external_asset_url,
        duration: calculate_duration(song)
      }
    )
    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end

  def set_products(tunecore_album, salepoints)
    products = []
    amazonod_salepoints = salepoints.select { |s| s.store.short_name =~ /^AmazonOD/ }

    amazonod_salepoints.each do |salepoint|
      raise "Missing price_code (variable_price_id)" if salepoint.price_code.nil?

      a_product = DistributionSystem::Amazonod::Product.new
      a_product.aod_retail = salepoint.price_code
      a_product.takedown = set_takedown(tunecore_album, salepoints, "AmazonOD")
      products.push(a_product)
    end

    products
  end

  def set_artist(tunecore_album)
    if !tunecore_album.is_various?
      a_artist = DistributionSystem::Amazonod::Artist.new
      a_artist.name    = tunecore_album.artist_name
      a_artist.role    = "Performer"
      a_artist.primary = true
      a_artist
    else
      DistributionSystem::Amazonod::Artist.new(name: tunecore_album.artist_name)
    end
  end
end
