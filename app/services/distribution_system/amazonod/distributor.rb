module DistributionSystem::Amazonod
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      @bundle_id        = @bundle.album.upc
      @remote_bundle_id = @bundle.album.physical_upc
      @remote_dir       = File.join(@bundle.remote_dir, @remote_bundle_id)

      @remote_server.connect do |server|
        Rails.logger.info "trying to create ftp directory #{@remote_dir} #{@bundle.album}"
        server.mkdir_p(@remote_dir)

        upload_assets(server) if full_delivery?
        upload_metadata(server)

        Rails.logger.info "successfully delivered to amazonod  #{@bundle.album}"
      end
    end

    def update_delivery?
      ["metadata_only", "metadata_with_assets"].include?(@bundle.album.delivery_type)
    end

    def upload_assets(server)
      @server = server
      upload_artwork
      upload_tracks
    end

    def upload_artwork
      artwork_file        = File.join(@bundle.dir.path, "#{@bundle_id}.jpg")
      artwork_1500_file   = File.join(@bundle.dir.path, "#{@bundle_id}_1500.jpg")
      remote_artwork_file = File.join(@remote_dir, "AssetImage0.jpg")
      remote_detail_page_artwork_file = File.join(@remote_dir, "DetailPageImage1.jpg")
      Rails.logger.info "  uploading artwork.  #{@bundle.album}"

      @server.upload(artwork_file, remote_artwork_file)
      @server.upload(artwork_1500_file, remote_detail_page_artwork_file)
    end

    def upload_tracks
      Rails.logger.info "Uploading tracks zipfile. #{@bundle.album}"

      source_zip_file      = File.join(@bundle.dir.path, "AudioSource.zip")
      destination_zip_file = File.join(@remote_dir, "AudioSource.zip")
      @server.upload(source_zip_file, destination_zip_file)
    end

    def upload_metadata(server)
      Rails.logger.info "  uploading metatata.  #{@bundle.album}"
      metadata_filename        = @bundle.metadata_path
      remote_metadata_filename = File.join(@remote_dir, @bundle.metadata_filename)
      server.upload(metadata_filename, remote_metadata_filename)
    end

    def full_delivery?
      ["full_delivery", "metadata_with_assets"].include?(@bundle.album.delivery_type)
    end
  end
end
