module DistributionSystem::Amazonod
  class GenreHelper
    def self.get_genres(genres)
      amazon_genres = []
      genres.map do |genre|
        amazon_genre = get_amazon_genre(genre.id)
        amazon_genres.push(amazon_genre)
      end
      amazon_genres
    end

    def self.get_amazon_genre(genre_id)
      case genre_id
      when 1
        "ALTERNATIVE ROCK"
      when 3
        "BLUES"
      when 4
        "CHILDREN'S MUSIC"
      when 5
        "CLASSICAL"
      when 6
        "MISCELLANEOUS"
      when 7
        "DANCE & DJ"
      when 8
        "DANCE & DJ"
      when 9
        "FOLK"
      when 10
        "INTERNATIONAL"
      when 11
        "FOLK"
      when 12
        "INTERNATIONAL"
      when 13
        "RAP & HIP-HOP"
      when 14
        "MISCELLANEOUS"
      when 15
        "CHRISTIAN & GOSPEL"
      when 16
        "JAZZ"
      when 17
        "LATIN MUSIC"
      when 18
        "NEW AGE"
      when 19
        "OPERA & VOCAL"
      when 20
        "POP"
      when 21
        "R&B"
      when 22
        "INTERNATIONAL"
      when 23
        "ROCK"
      when 24
        "SOUNDTRACKS"
      when 26
        "OPERA & VOCAL"
      when 27
        "INTERNATIONAL"
      when 28
        "FOLK"
      when 29
        "COUNTRY"
      when 30
        "MISCELLANEOUS"
      when 31
        "HARD ROCK & METAL"
      when 32
        "POP"
      when 33
        "POP"
      when 34
        "ROCK"
      when 35
        "JAZZ"
      when 36
        "MISCELLANEOUS"
      when 37
        "CLASSICAL"
      when 38
        "MISCELLANEOUS"
      when 39
        "MISCELLANEOUS"
      when (40..100)
        "INTERNATIONAL"
      when 101
        "AMBIENT"
      when 102
        "BRAZILIAN"
      when 103
        "BALADAS"
      when 104
        "BOLEROS"
      when 105
        "CARIBBEAN"
      when 106
        "CUBAN"
      when 107
        "LATIN HIP-HOP"
      when 108
        "LATIN TRAP"
      when 109
        "LATIN URBAN"
      when 110
        "RANCHERA"
      when 111
        "REGIONAL MEXICANO"
      when 112
        "SALSA/MERENGUE"
      when 113
        "TANGO"
      when 114
        "TROPICAL"
      when 115
        "AXÉ"
      when 116
        "BAILE FUNK"
      when 117
        "BOSSA NOVA"
      when 118
        "CHORINHO"
      when 119
        "FORRÓ"
      when 120
        "FREVO"
      when 121
        "MPB"
      when 122
        "PAGODE"
      when 123
        "SAMBA"
      when 124
        "SERTANEJOS"
      when 125
        "AFRICAN"
      when 126
        "AFROBEATS"
      when 127
        "AFROPOP"
      when 128
        "AFRO-FUSION"
      when 129
        "AFRO-SOUL"
      when 130
        "AFRO HOUSE"
      when 131
        "AMAPIANO"
      when 132
        "BONGO FLAVA"
      when 133
        "HIGHLIFE"
      when 134
        "MASKANDI"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
