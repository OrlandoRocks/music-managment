module DistributionSystem::Amazonod
  class CoverImage
    attr_accessor :asset, :aod_template_id

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def valid?
      extension = File.extname(@asset)
      return false unless [".tif", ".jpg"].include?(extension)

      true
    end

    def aod_template
      templates = {
        "1" => "9830011",
        "2" => "9830021",
        "3" => "9830031",
        "4" => "9830041",
        "5" => "9830051",
        "6" => "9830061",
        "7" => "9830071",
        "8" => "9830081",
        "9" => "9830091",
        "10" => "9830101"
      }
      templates[@aod_template_id.to_s]
    end
  end
end
