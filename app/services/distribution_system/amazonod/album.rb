class DistributionSystem::Amazonod::Album < DistributionSystem::Album
  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :logger,
                :products,
                :physical_upc

  def upc
    (physical_upc.nil? || physical_upc == "") ? super : physical_upc
  end

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.ALBUM do |album|
      album.SPEC_VERSION "3.0"
      album.CHANNEL_SELECT "DOD"
      album.ALBUM_INFO do |info|
        unless ["full_delivery", "metadata_only", "metadata_with_assets"].include?(@delivery_type)
          raise "Unsuppported delivery type: #{@delivery_type}"
        end

        info.LABEL_NAME label_name
        info.VENDOR_CODE "335721"

        info.ALBUM_ID upc
        info.RELATED_UPC upc

        info.EAN upc if upc.size == 13

        info.STREET_DATE actual_release_date
      end

      album.ALBUM_METADATA do |album_metadata|
        album_metadata.TITLE title
        album_metadata.NUM_DISCS "01"
        album_metadata.NUM_TRACKS tracks.size.to_s

        if duration >= (79 * 60 + 48)
          raise "ERROR: album upc=#{upc} exceeded duration of 79 minutes and 48 seconds. Fail to meet Amazonod's spec"
        end

        album_metadata.DURATION get_duration("mm:ss")

        album_metadata.PRIMARY_GENRE genres.first
        genres[1, genres.length].each do |genre|
          album_metadata.ADDITIONAL_GENRE genre
        end

        artists.each { |a| a.to_xml(album_metadata) } if artists
        if explicit_lyrics == false
          album_metadata.EXPLICIT_LYRICS "false"
        elsif explicit_lyrics
          album_metadata.EXPLICIT_LYRICS "true"
        end
        album_metadata.COPYRIGHT_LINE copyright_pline if copyright_pline
      end

      if products
        album.DOD_TERRITORY do |album_territory|
          products.each { |p| p.to_xml(album_territory) }
        end
      end

      if ["full_delivery", "metadata_with_assets"].include?(@delivery_type)
        album.COVER_ART_FILE do |cover_art_file|
          cover_art_file.CHECKSUM(DistributionSystem::CheckSumHelper.checksum_file(artwork_file.asset))
          cover_art_file.DOD_TEMPLATE_ID artwork_file.aod_template
        end
      end

      tracks.each { |t| t.to_xml(album) }
    end
  end

  def valid?
    [@upc, @title, @release_date, @label_name, @products, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
    end

    true
  end

  def validate_against_schema
    puts "validating against schema"
    schema = LibXML::XML::Schema.new("spec/distribution_fixtures/schema/CSP_AudioFeedSchema_3.0.1.xsd")
    libxml_album = LibXML::XML::Document.string(to_xml)
    libxml_album.validate_schema(schema)
  end
end
