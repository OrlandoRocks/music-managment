class DistributionSystem::S3
  attr_accessor :bucket

  def initialize(bucket)
    @bucket = bucket
  end

  def get(local_filename, s3_key)
    @bucket = "tunecore.com" if @bucket.nil? || @bucket == ""
    Rails.logger.debug "downloading #{@bucket || 'tunecore.com'}/#{s3_key} to #{local_filename}."

    begin
      bucket    = S3_CLIENT.buckets[@bucket || "tunecore.com"]
      local_dir = File.dirname(local_filename)
      FileUtils.mkdir_p(local_dir) unless File.exist?(local_dir)

      file = File.new(local_filename, "wb")
      bucket.objects[s3_key].read { |chunk| file.write(chunk) }
    rescue => e
      Rails.logger.warn("Error downloading #{s3_key}: #{e.message}")
      raise DistributionSystem::DeliveryError.new("S3 read failed: #{e.message}, key: #{s3_key}")
    ensure
      file.close
      @bucket = @default_bucket
    end
  end

  def put(local_filename, s3_key, _download_filename = nil)
    begin
      Rails.logger.debug "uploading #{local_filename} to #{s3_key}"
      bucket = S3_CLIENT.buckets[@bucket]
      object = bucket.objects[s3_key]
      object.write(file: local_filename)
    rescue => e
      raise DistributionSystem::DeliveryError.new("S3 put failed:#{e.message}")
    end
  end
end
