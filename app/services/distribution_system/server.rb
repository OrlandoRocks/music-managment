class DistributionSystem::Server
  def self.server_for(store_name)
    new(store_name).server_for
  end

  def initialize(store_name)
    @store_name      = store_name
    @delivery_config = StoreDeliveryConfig.for(@store_name)
    @transfer_type = @delivery_config.try(:transfer_type)
  end

  def server_for
    if @store_name.downcase.include?("itunes")
      $apple_transporter_client
    elsif Rails.env.development? || prevent_external_deliveries?
      "DistributionSystem::#{@transfer_type.classify}".constantize.new(
        OpenStruct.new(
          hostname: ENV["SFTP_HOSTNAME"] || "ftp.tunecore.com",
          username: ENV["SFTP_USERNAME"] || "petridelivery",
          port: ENV["SFTP_PORT"] || "22",
          password: ENV["SFTP_PASSWORD"] || "AQfa!hBb",
          keyfile: ENV["SFTP_KEYFILE"]
        )
      )
    else
      "DistributionSystem::#{@transfer_type.classify}".constantize.new(@delivery_config)
    end
  end

  def prevent_external_deliveries?
    !TcWww::Application.config.allow_external_deliveries
  end
end
