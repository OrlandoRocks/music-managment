module DistributionSystem::Itunes
  class BookletFile
    include DistributionSystem::XmlFormattable
    xml_format :itunes

    attr_accessor :asset, :album

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def valid?
      File.extname(@asset) == ".pdf"
    end
  end
end
