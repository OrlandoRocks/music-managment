module DistributionSystem::Itunes
  class Lyric
    include DistributionSystem::XmlFormattable
    xml_format :itunes

    attr_accessor :file_name,
                  :file_size,
                  :file_checksum,
                  :content,
                  :language_code,
                  :track_title

    def initialize(opts = {})
      assign_attributes(opts)
    end

    def update(opts = {})
      assign_attributes(opts)
    end

    def lyric_paragraphs
      content.split("\n\n")
    end

    def sanitized_track_title
      @sanitized_track_title ||= track_title.gsub(/\s/, "_").gsub(/^\W|[^a-zA-Z0-9\-.\_]/, "")
    end

    private

    def assign_attributes(opts)
      opts.each do |k, v|
        send("#{k}=", v)
      end
    end
  end
end
