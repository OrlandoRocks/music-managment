class DistributionSystem::Itunes::Converter < DistributionSystem::Converter
  attr_reader :country_codes

  description "iTunes"
  handle_salepoints(/^iTunes/)

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    preorder_salepoint = tc_salepoints.detect { |salepoint| salepoint.preorder_data.present? }
    this_year = Date.today.year.to_s
    @country_codes = tunecore_album.country_iso_codes

    album = DistributionSystem::Itunes::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        copyright_pline: "#{tunecore_album.orig_release_date.year} #{tunecore_album.copyright_name}",
        copyright_cline: "#{tunecore_album.orig_release_date.year} #{tunecore_album.copyright_name}",
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: DistributionSystem::Itunes::GenreHelper.get_genres(tunecore_album.genres),
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory?,
        liner_notes: tunecore_album.liner_notes,
        products: set_products(tunecore_album, tc_salepoints),
        artwork_file: DistributionSystem::Itunes::CoverImage.new,
        creatives: [],
        delivery_type: delivery_type,
        album_type: set_album_type(tunecore_album),
        takedown: tunecore_album.takedown?,
        language_code: get_language_code(tunecore_album),
        preorder_data: preorder_salepoint ? preorder_salepoint.preorder_data : nil,
        actual_release_date: tunecore_album.sale_date,
        customer_id: tunecore_album.person_id,
        curated_artists: curated_artists(tunecore_album),
        is_approved: tunecore_album.approved?
      }
    )

    album.title = tunecore_album.name

    if tunecore_album.artwork.s3_asset
      album.artwork_s3_key = tunecore_album.artwork.s3_asset.key
    else
      album.artwork_asset_url = tunecore_album.artwork.external_asset_url
    end
    tunecore_album.creatives.each do |creative|
      role       = set_role(creative.role_for_petri)
      is_primary = role == "Performer"
      apple_artist_id = DistributionSystem::Itunes::Creative.valid_apple_id_or_nil(
        creative: creative,
        is_track: false
      )

      album.creatives << DistributionSystem::Itunes::Creative.new(
        primary: is_primary,
        name: creative.name,
        roles: [role],
        apple_artist_id: apple_artist_id,
        id: creative.id
      )
    end
    tunecore_album.songs.each { |song| add_track(album, song, tunecore_album, tc_salepoints) }
    if tunecore_album.is_various
      album.creatives << DistributionSystem::Itunes::Creative.new(
        name: "Various Artists",
        roles: ["Artist"],
        primary: true
      )
    end

    raise "Missing booklet file" if tunecore_album.booklet && tunecore_album.booklet.s3_asset.nil?

    if tunecore_album.booklet
      album.booklet_file = DistributionSystem::Itunes::BookletFile.new(asset: tunecore_album.booklet.s3_asset.key, album: album)
    end

    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    album
  end

  # add album tracks
  def add_track(album, song, tunecore_album, tc_salepoints)
    preorder_salepoint = tc_salepoints.detect { |salepoint| salepoint.preorder_data.present? }

    track = DistributionSystem::Itunes::Track.new(
      {
        album: album,
        number: song.track_num,
        sku: song.sku,
        isrc: song.isrc,
        temp_filename: "#{album.tunecore_id}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
        explicit_lyrics: song.parental_advisory,
        clean_version: song.clean_version?,
        s3_key: song_s3key(song),
        s3_bucket: song_s3bucket(song),
        asset_url: song.external_asset_url,
        creatives: [],
        duration: song.duration,
        title: song.name,
        original_release_date: song.previously_released_at,
        language_code: get_track_language(song),
        english_title: song.translated_name,
        instrumental: song.instrumental
      }
    )

    track.lyrics = build_lyrics(song, album) if song.lyric.try(:content)
    if preorder_salepoint
      track.instant_grat        = preorder_salepoint.preorder_data[:instant_grat_tracks].include?(song.id)
    end
    if song.respond_to?(:preview_start_time) && song.preview_start_time != nil
      track.preview_start_index = song.preview_start_time
    end

    track.set_creatives(album, song)

    track.products = set_products(tunecore_album, tc_salepoints, "track")

    track.orig_filename = song.upload.uploaded_filename if song.upload
    album.tracks << track
  end

  def set_album_type(tunecore_album)
    case tunecore_album.album_type
    when "Ringtone", "Album", "Single"
      tunecore_album.album_type.to_s.downcase
    else
      ""
    end
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      if tunecore_album.label.instance_of?(Label)
        tunecore_album.label.name
      else
        tunecore_album.label.to_s
      end
    else
      tunecore_album.artist_name
    end
  end

  # product type can be either 'album' or 'track'
  def set_products(tunecore_album, salepoints, product_type = "album")
    preorder_salepoint = salepoints.detect { |salepoint| salepoint.preorder_data.present? }
    products = []

    allow_stream = !!tunecore_album.try(:allow_itunes_streaming)

    if !salepoints.detect { |tc_salepoint| tc_salepoint.store.short_name[/iTunesWW/] }
      included_itunes_salepoints = get_included_itunes_salepoints(salepoints, tunecore_album)
      included_itunes_salepoints.map do |salepoint|
        countries_for_product(salepoint, tunecore_album).each do |territory|
          i_product = DistributionSystem::Itunes::Product.new
          i_product.product_type                = product_type
          i_product.territory                   = territory
          i_product.album_wholesale_price_tier  = salepoint.album_price_code
          i_product.track_wholesale_price_tier  = salepoint.track_price_code
          i_product.sales_start_date            = Date.strptime(tunecore_album.sale_date.to_s, "%Y-%m-%d")
          i_product.cleared_for_sale            = salepoint.takedown_at ? false : true
          i_product.allow_stream                = allow_stream
          products.push(i_product)
        end
      end

      exluded_itunes_salepoints = get_excluded_itunes_salepoints(salepoints, tunecore_album)
      exluded_itunes_salepoints.map do |salepoint|
        countries_for_product(salepoint, tunecore_album).each do |territory|
          i_product = DistributionSystem::Itunes::Product.new
          i_product.product_type                = product_type
          i_product.territory                   = territory
          i_product.album_wholesale_price_tier  = salepoint.album_price_code || 3
          i_product.track_wholesale_price_tier  = salepoint.track_price_code || 99
          i_product.sales_start_date            = Date.strptime(tunecore_album.sale_date.to_s, "%Y-%m-%d")
          i_product.cleared_for_sale            = false
          i_product.allow_stream                = allow_stream
          products.push(i_product)
        end
      end
    else
      salepoint = salepoint("iTunesWW", nil, tunecore_album)
      i_product = DistributionSystem::Itunes::Product.new
      i_product.product_type                = product_type
      i_product.territory                   = "WW"
      i_product.album_wholesale_price_tier  = salepoint.album_price_code || 3
      i_product.track_wholesale_price_tier  = salepoint.track_price_code || 99
      i_product.sales_start_date            = Date.strptime(tunecore_album.sale_date.to_s, "%Y-%m-%d")
      i_product.cleared_for_sale            = salepoint.takedown_at ? false : true
      i_product.preorder_data = preorder_salepoint.preorder_data if i_product.cleared_for_sale && preorder_salepoint
      i_product.allow_stream = allow_stream
      products.push(i_product)

      valid_country_codes = itunes_country_codes(tunecore_album)

      valid_country_codes.each do |territory|
        i_product = DistributionSystem::Itunes::Product.new
        i_product.product_type                = product_type
        i_product.territory                   = territory
        i_product.album_wholesale_price_tier  = salepoint.album_price_code || 3
        i_product.track_wholesale_price_tier  = salepoint.track_price_code || 99
        i_product.sales_start_date            = Date.strptime(tunecore_album.sale_date.to_s, "%Y-%m-%d")
        i_product.cleared_for_sale            = country_codes.include?(territory) && (salepoint.takedown_at ? false : true)
        i_product.preorder_data = preorder_salepoint.preorder_data if i_product.cleared_for_sale && preorder_salepoint
        i_product.allow_stream = allow_stream
        products.push(i_product)
      end
      return products
    end

    if defined?(Rails.logger)
      Rails.logger.info("Created products for itunes distribution album_id=#{tunecore_album.id}:\n#{products.collect(&:territory).join(',')}")
    end

    products
  end

  def get_track_language(track)
    if FeatureFlipper.show_feature?(:track_level_language, track.person) && track.lyric_language_code
      track.lyric_language_code&.code
    else
      track.metadata_language_code.try(:code) || track.album.metadata_language_code.try(:code) || track.album.language_code
    end
  end

  # returns an array of salepoints that HAVE been selected
  def get_included_itunes_salepoints(salepoints, tunecore_album)
    #  2012-07-25 Takashi Egawa: get_static_salepoints requires salepoints
    static_salepoints = get_static_salepoints(tunecore_album, salepoints)
    itunes_salepoints = salepoints.select { |s| s.store.short_name =~ /^iTunes/ }
    static_salepoints.select do |static_s|
      selected_stores =
        itunes_salepoints.collect { |s|
          s.store.short_name unless s.takedown_at
        } # don't consider taken down stores selected
      selected_stores.include?(static_s.store.short_name)
    end
  end

  # returns an array of salepoints that HAVE NOT been selected
  def get_excluded_itunes_salepoints(salepoints, tunecore_album)
    #  2012-07-25 Takashi Egawa: get_static_salepoints requires salepoints
    static_salepoints = get_static_salepoints(tunecore_album, salepoints)
    itunes_salepoints = salepoints.select { |s| s.store.short_name =~ /^iTunes/ }
    static_salepoints.select do |static_s|
      selected_stores =
        itunes_salepoints.collect { |s|
          s.store.short_name unless s.takedown_at
        } # don't consider taken down stores selected
      !selected_stores.include?(static_s.store.short_name)
    end
  end

  #  2012-07-25 Takashi Egawa: add argument 'salepoints'
  #  2013-01-09 AK -- This now only gets called for legacy salepoints, now iTunesWW
  def get_static_salepoints(album, salepoints)
    #  2012-07-25 Takashi Egawa: 'album' doesn't have salepoints
    album_price_code = salepoints.detect { |sp| sp.store.short_name[/iTunes/] }.price_code || 3
    track_price_code = salepoints.detect { |sp| sp.store.short_name[/iTunes/] }.track_price_code || 99

    itunesUS   = salepoint "iTunesUS", ["US"], album
    itunesAU   = salepoint "iTunesAU", ["AU", "NZ"], album
    itunesCA   = salepoint "iTunesCA", ["CA"], album
    if album.album_type == "Ringtone"
      itunesEU   = salepoint "iTunesEU", ["BE", "DK", "ES", "FI", "FR", "GR", "IT", "LU", "NL", "NO", "AT", "PT", "CH", "SE", "GB", "BG"], album
    else
      itunesEU   = salepoint "iTunesEU", ["BE", "DK", "DE", "ES", "FI", "FR", "GR", "IE", "IT", "LU", "NL", "NO", "AT", "PT", "CH", "SE", "GB", "BG", "CY", "CZ", "EE", "HU", "LV", "LT", "MT", "PL", "RO", "SK", "SI"], album
    end
    itunesJP   = salepoint "iTunesJP", ["JP"], album
    itunesMX   = salepoint "iTunesMX", ["MX"], album
    itunesLA   = salepoint "iTunesLA", ["BR", "AR", "BO", "CL", "CO", "CR", "DO", "EC", "SV", "GT", "HN", "NI", "PA", "PY", "PE", "VE"], album
    itunesPA   = salepoint "iTunesPA", ["BN", "KH", "HK", "ID", "LA", "MO", "MY", "PH", "SG", "LK", "TW", "TH", "VN"], album

    [itunesUS, itunesAU, itunesCA, itunesEU, itunesJP, itunesMX, itunesLA, itunesPA]
  end

  def salepoint(short_name, countries, album)
    album_price_code = 3 # defaults are here for stores that are not selected
    track_price_code = 99
    takedown_at = album.takedown_at

    salepoint = album.salepoints.detect { |sp| sp.store.short_name[/#{short_name}/] }

    if salepoint
      album_price_code = salepoint.price_code
      track_price_code = salepoint.track_price_code
      takedown_at = salepoint.takedown_at
    end

    OpenStruct.new(
      {
        store: OpenStruct.new(
          {
            short_name: short_name,
            countries: countries
          }
        ),
        album_price_code: album_price_code,
        track_price_code: track_price_code,
        takedown_at: takedown_at
      }
    )
  end

  def set_role(role = "")
    case role
    when "primary_artist", "primary"
      "Performer"
    else
      role.capitalize
    end
  end

  def set_primary(role = "")
    case role
    when "primary_artist", "primary"
      true
    else
      false
    end
  end

  def build_lyrics(song, album)
    DistributionSystem::Itunes::Lyric.new(
      content: song.lyric.content,
      language_code: album.language_code,
      track_title: song.name
    )
  end

  def countries_for_product(salepoint, tunecore_album)
    if tunecore_album.takedown? && salepoint.store.abbrev == "ww"
      Country.select(:iso_code).pluck(:iso_code) << "WW"
    else
      salepoint.store.countries
    end
  end

  def curated_artists(tc_album)
    tc_album.artists.joins(:creatives)
            .where(creatives: { curated_artist_flag: true }).distinct
  end

  def itunes_country_codes(album)
    # remove territories via ENV until we can validate problematic territories with iTunes
    excluded_codes = ENV.fetch("ITUNES_TERRITORY_EXCLUSIONS", nil)
    return album.all_country_iso_codes unless excluded_codes

    album.all_country_iso_codes - excluded_codes.split(",")
  end

  def valid_apple_artist_id(creative)
    result = creative.get_apple_artist_id

    return if result == ExternalServiceId::UNAVAILABLE

    result
  end
end
