xml.product do
  xml.territory            territory
  xml.wholesale_price_tier album_wholesale_price_tier if product_type == "album"
  xml.wholesale_price_tier track_wholesale_price_tier if product_type == "track"

  if preorder_data_and_album
    xml.preorder_wholesale_price_tier preorder_data[:wholesale_price_tier] if preorder_data[:wholesale_price_tier]
    xml.preorder_sales_start_date preorder_data[:preorder_start_date].strftime('%Y-%m-%d') if preorder_data[:preorder_start_date]
  elsif preorder_data_and_track
    xml.cleared_for_itunes_radio_during_preorder true if track.instant_grat
  end

  xml.sales_start_date sales_start_date.strftime('%Y-%m-%d') if product_type == "album"

  xml.cleared_for_sale cleared_for_sale.to_s

  if allow_stream && cleared_for_sale
    xml.cleared_for_stream "true"
    if preorder_data.present? 
      if (product_type=="track" && track.instant_grat) || product_type == "album"
        xml.stream_start_date preorder_data[:preorder_start_date].strftime('%Y-%m-%d')
      else
        xml.stream_start_date sales_start_date.strftime('%Y-%m-%d')
      end
    else
      xml.stream_start_date sales_start_date.strftime('%Y-%m-%d')
    end
  else
    xml.cleared_for_stream "false"
  end
end
