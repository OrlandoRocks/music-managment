xml.package(xmlns: "http://apple.com/itunes/importer", version:'music5.3' ) do |package|
  package.language !language_code ? 'en' : language_code
  package.provider "TuneCore"
  package.album do |album|
    album.album_type            album_type if album_type == "ringtone"
    album.grid                  grid if grid
    album.upc                   upc
    album.title                 determine_album_name( title )
    album.title_version         title_version if title_version
    album.original_release_date release_date
    album.label_name            label_name

    if genres
      if genres.any? { |g| g =~ /-00/ }
        album.genres do |album_genres|
          genres.uniq.each { |g| album_genres.genre 'code' => g }
        end
      else
        album.genres do |album_genres|
          genres.uniq.each {|g| album_genres.genre g}
        end
      end
    end

    album.copyright_pline   copyright_pline if copyright_pline
    album.copyright_cline   copyright_cline if copyright_cline

    if deliver_assets?
      album.artwork_files do |artwork_files|
        artwork_files.file do |file_xml|
          file_xml.file_name( File.basename(artwork_file.asset))
          file_xml.size( File.size(artwork_file.asset) )
          file_xml.checksum(DistributionSystem::CheckSumHelper.checksum_file(artwork_file.asset), :type => DistributionSystem::CheckSumHelper.algorithm)
        end
      end
    end

    album.liner_notes       liner_notes if (liner_notes && !album_type.nil? && album_type != "ringtone")
    album.description_short description_short if description_short
    album.description_long  description_long if description_long
    album.volume_number     volume_number if volume_number
    if booklet_file
      album.track_count       tracks.size.to_i + 1
    else
      album.track_count       tracks.size
    end

    album.preorder_previews preorder_data[:preview_songs] if preorder_data

    unless creatives.empty?
      render "creatives", self, xml: album
    end

    render "products", self, xml: xml, products_for: "album" if products
    if !@takedown
      album.tracks do |album_tracks|
        tracks.each do |track|
          render "track", track, xml: album_tracks
        end
        render "booklet_file", booklet_file, xml: album_tracks if booklet_file
      end
    end
  end
end
