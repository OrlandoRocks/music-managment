xml.lyrics_file do |lyrics_file|
  lyrics_file.file_name file_name
  lyrics_file.size      file_size
  lyrics_file.checksum  file_checksum
end
