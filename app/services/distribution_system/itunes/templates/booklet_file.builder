xml.booklet do |booklet|
  booklet.vendor_id "TUN#{album.tunecore_id}BOOKLET"
  booklet.title "Digital Booklet - #{album.title}"
  booklet.copyright album.copyright_pline
  booklet.track_number album.tracks.size.to_i + 1
  booklet.products do |booklet_products|
    album.tracks.last.products.each do |product|
      booklet_products.product do |product_xml|
        product_xml.territory product.territory
        product_xml.cleared_for_sale product.cleared_for_sale
      end
    end
  end
  render "creatives", album, xml: booklet

  if album.deliver_assets?
    booklet.file do |file|
      file.file_name File.basename(asset)
      file.size File.size(asset)
      file.checksum(DistributionSystem::CheckSumHelper.checksum_file(asset), :type => DistributionSystem::CheckSumHelper.algorithm)
    end
  end
end
