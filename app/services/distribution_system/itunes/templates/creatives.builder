xml.artists do |artists_xml|
  creatives.each do |creative|
    render "creative", creative, xml: artists_xml
  end
end
