tag_options = {
  "xmlns"        => "http://www.w3.org/ns/ttml",
  "xmlns:tts"    => "http://www.w3.org/ns/ttml#styling",
  "xmlns:itunes" => "http://itunes.apple.com/lyric-ttml-extensions",
  "xmlns:ttm"    => "http://www.w3.org/ns/ttml#metadata",
  "xml:lang"     => language_code
}
xml.instruct!(:xml, :version=>"1.0", :encoding=>"utf-8")

xml.tag!("tt", tag_options) do |tt|
  tt.head  do |head|
    head.metadata do |metadata|
      metadata.ttm(:title, sanitized_track_title)
    end
  end

  tt.body do |body|
    lyric_paragraphs.each do |lyric_div|
      body.div do |div|
        lyric_div.split("\n").each do |line|
          div.p line
        end
      end
    end
  end
end
