xml.audio_file do
  xml.file_name(File.basename(audio_file))
  xml.size( File.size(audio_file) )
  xml.checksum(DistributionSystem::CheckSumHelper::checksum_file(audio_file))
end
