xml.locales do |locale_xml|
  locale_xml.locale name: "en" do |title_xml|
    title_xml.title english_title
  end
end