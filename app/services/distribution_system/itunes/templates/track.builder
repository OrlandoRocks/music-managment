xml.track do
  xml.type                  album.album_type  if !album.album_type.nil? && album.album_type == "ringtone"
  xml.isrc                  isrc
  xml.grid                  grid                  if grid
  xml.title                 title
  xml.title_version         title_version         if title_version
  render "locales", self, xml: xml                if needs_locale?
  xml.original_release_date original_release_date if original_release_date
  xml.gapless_play          gapless_play          if gapless_play
  xml.label_name            label_name            if label_name

  xml.genres { genres.each { |g| xml.genre g } } if genres

  render "products", self, xml: xml, products_for: "track"

  xml.preorder_type           "instant-gratification" if self.instant_grat
  xml.copyright_pline         copyright_pline         if copyright_pline
  xml.explicit_content        "explicit"              if explicit_lyrics
  xml.explicit_content        "clean"                 if clean_version
  render "lyrics_info", lyrics, xml: xml if lyrics
  xml.track_liner_notes       liner_notes             if (liner_notes && !album.album_type.nil? && album.album_type != "ringtone")
  xml.track_beats_per_minute  beats_per_minute        if beats_per_minute
  xml.track_number            number

  render "audio_file", self, xml: xml if album.delivery_type == "full_delivery"

  if album.genres.include?("INSTRUMENTAL-00") || instrumental
    xml.audio_language "zxx"
  else
    xml.audio_language audio_language_code(language_code)
  end

  xml.preview_start_index preview_start_index if preview_start_index
  render "creatives", self, xml: xml
end
