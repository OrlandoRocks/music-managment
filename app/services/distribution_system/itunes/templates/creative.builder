xml.artist do
  xml.apple_id apple_artist_id if apple_artist_id
  xml.artist_name name
  xml.roles do |r|
    roles.uniq.each do |role|
      r.role role
    end
  end
  xml.primary primary.to_s
end
