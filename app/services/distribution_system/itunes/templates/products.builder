xml.products do |products_xml|
  products.each do |product|
    product.send("#{products_for}=", self)
    render "product", product, xml: products_xml
  end
end
