class DistributionSystem::Itunes::Album < DistributionSystem::Album
  include DistributionSystem::XmlFormattable
  xml_format :itunes

  attr_accessor :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :album_type,
                :takedown,
                :products,
                :preorder_data,
                :curated_artists,
                :is_approved

  def is_single?
    tracks.size <= 3 && !tracks.detect { |track| track.duration > 600 }
  end

  def is_ep?
    (tracks.size <= 3 && tracks.detect { |track|
       track.duration > 600
     } && album_length < 1800) || (tracks.size >= 4 && tracks.size <= 6 && album_length < 1800)
  end

  def album_length
    sum = 0
    tracks.each { |track| sum += track.duration }
    sum
  end

  def determine_album_name(album_title)
    return album_title if takedown

    if is_single? && !album_title[/Single$/] && album_type != "ringtone"
      album_title + " - Single"
    elsif is_ep? && !album_title.rstrip[/EP$/]
      album_title + " - EP"
    else
      album_title
    end
  end

  def valid?
    [@upc, @title, @release_date, @label_name, @products, @genres, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
      return false if @artwork_asset_url.nil? && @artwork_s3_key.nil?
    end

    true
  end
end
