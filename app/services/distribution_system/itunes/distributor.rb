module DistributionSystem::Itunes
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      Rails.logger.info "Preparing to validate and send the #{@bundle.dirname} for album_id=#{@bundle.album.tunecore_id}  #{@bundle.album}"
      if validate_bundle
        Rails.logger.info "#{@bundle.dirname} VALID  #{@bundle.album}"
        send_to_itunes
        Rails.logger.info "completed shipping of the #{@bundle.dirname} #{@bundle.album}"
      elsif !@bundle.album.is_approved
        @distribution.discard!
      else
        Rails.logger.error "Album failed during verify | album_id=#{@bundle.album.tunecore_id} #{@bundle.album}"

        raise "Itunes package verify failed: #{@result[:output]}" unless remove_invalid_apple_ids

        @distribution.retry({ actor: "System", message: "Bad External Service ID'S deleted" })
      end
    end

    def remove_invalid_apple_ids
      id_errors = Nokogiri::XML(@result[:output]).xpath("//itunes_transporter/log[@level='error']/message[contains(text(), 'ERROR ITMS-4088')]")

      return unless id_errors.any?

      album_ids = ExternalServiceId.where(
        linkable_id: @bundle.album.creatives.map(&:id),
        linkable_type: "Creative",
        identifier: @bundle.album.creatives.map(&:apple_artist_id).filter(&:present?),
        service_name: "apple"
      )

      ids_deleted = false
      id_errors.each do |error_msg|
        this_id = album_ids.select { |id| error_msg.content.include?(id.identifier) if id.identifier }

        if this_id.any?
          this_id.map(&:destroy)
          ids_deleted = true
        end
      end

      ids_deleted
    end

    def validate_bundle
      if @bundle.album.curated_artists.present? && full_delivery?
        @result = {
          output: "album #{@bundle.album} contains curated artist(s): #{@bundle.album.curated_artists.join(', ')}",
          valid: false
        }
        return @result[:valid]
      end

      @result = @remote_server.validate_package(@bundle)
      Rails.logger.info "iTMSTransporter output: #{@result[:output]}  #{@bundle.album}"
      @result[:valid]
    end

    def validate_curated_artists
      artist_names = @bundle.album.creatives.map(&:name)
      @remote_server.curated_artists(@bundle, artist_names)
    end

    def send_to_itunes
      Rails.logger.info "sending package #{@bundle.dir.path} to iTunes with the iTMSTransporter executable #{@bundle.album}"
      result = @remote_server.send_package(@bundle)
      return if result[:success]

      Rails.logger.error "Album failed during upload | album_id=#{bundle.album.tunecore_id} #{@bundle.album}"
      raise DistributionSystem::DeliveryError.new("Itunes package upload failed: #{result[:output]}")
    end
  end
end
