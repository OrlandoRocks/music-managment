module DistributionSystem::Itunes
  class Product
    include DistributionSystem::XmlFormattable
    xml_format :itunes

    def initialize(options = {})
      options.each { |k, v| send("#{k}=", v) }
    end

    attr_accessor :album,
                  :album_wholesale_price_tier,
                  :allow_stream,
                  :cleared_for_sale,
                  :sales_start_date,
                  :sales_end_date,
                  :territory,
                  :product_type,
                  :track,
                  :track_wholesale_price_tier,
                  :preorder_data

    def valid?
      # Required fields
      return false if territory.nil?
      return false if wholesale_price_tier.nil?
      return false if cleared_for_sale.nil?

      # Check Values
      return false if sales_start_date && !sales_start_date.respond_to?(:strftime)

      true
    end

    def preorder_data_and_track
      preorder_data.present? && product_type == "track"
    end

    def preorder_data_and_album
      preorder_data.present? && product_type == "album"
    end
  end
end
