module DistributionSystem::Itunes
  class GenreHelper
    def self.get_genres(genres)
      itunes_genres = []
      genres.map do |genre|
        itunes_genre = get_itunes_genre(genre.id)
        itunes_genres.push(itunes_genre)
      end
      itunes_genres
    end

    def self.get_itunes_genre(genre_id)
      case genre_id
      when 1
        "ALTERNATIVE-00"
      when 3
        "BLUES-00"
      when 4
        "CHILDREN-MUSIC-00"
      when 5
        "CLASSICAL-00"
      when 6
        "COMEDY-00"
      when 7
        "DANCE-00"
      when 8
        "ELECTRONIC-00"
      when 9
        "TRADITIONAL-FOLK-00"
      when 10
        "FRENCH-POP-00"
      when 11
        "GERMAN-FOLK-00"
      when 12
        "GERMAN-POP-00"
      when 13
        "HIP-HOP-RAP-00"
      when 14
        "HOLIDAY-00"
      when 15
        "CHRISTIAN-GOSPEL-00"
      when 16
        "JAZZ-00"
      when 17
        "LATIN-00"
      when 18
        "NEW-AGE-00"
      when 19
        "OPERA-00"
      when 20
        "POP-00"
      when 21
        "R-B-SOUL-00"
      when 22
        "REGGAE-00"
      when 23
        "ROCK-00"
      when 24
        "SOUNDTRACK-00"
      when 26
        "VOCAL-00"
      when 27
        "WORLD-00"
      when 28
        "AMERICANA-00"
      when 29
        "COUNTRY-00"
      when 30
        "SPOKEN-WORD-00"
      when 31
        "HEAVY-METAL-00"
      when 32
        "J-POP-00"
      when 33
        "K-POP-00"
      when 34
        "SINGER-SONGWRITER-00"
      when 35
        "BIG-BAND-00"
      when 36
        "FITNESS-WORKOUT-00"
      when 37
        "HIGH-CLASSICAL-00"
      when 38
        "INSTRUMENTAL-00"
      when 39
        "KARAOKE-00"
      when 40
        "INDIAN-00"
      when  41
        "ASSAMESE-00"
      when  42
        "ASSAMESE-00"
      when  43
        "BENGALI-00"
      when  44
        "BENGALI-00"
      when  45
        "INDIAN-00"
      when  46
        "BHOJPURI-00"
      when  47
        "BHOJPURI-00"
      when  48
        "BOLLYWOOD-00"
      when  49
        "CLASSICAL-00"
      when  50
        "CLASSICAL-00"
      when  51
        "CHILDREN-MUSIC-00"
      when  52
        "CLASSICAL-00"
      when  53
        "CLASSICAL-00"
      when  54
        "CLASSICAL-00"
      when  55
        "DEVOTIONAL-SPIRITUAL-00"
      when  56
        "SPOKEN-WORD-00"
      when  57
        "DANCE-00"
      when  58
        "INDIAN-FOLK-00"
      when  59
        "INDIAN-00"
      when  60
        "GHAZALS-00"
      when  61
        "GUJARATI-00"
      when  62
        "GUJARATI-00"
      when  63
        "HARYANVI-00"
      when  64
        "HARYANVI-00"
      when  65
        "INDIAN-00"
      when  66
        "INDIAN-00"
      when  67
        "FOREIGN-CINEMA-00"
      when  68
        "CLASSICAL-00"
      when  69
        "CLASSICAL-00"
      when  70
        "INDIAN-00"
      when  71
        "KANNADA-00"
      when  72
        "KANNADA-00"
      when  73
        "INDIAN-00"
      when  74
        "MALAYALAM-00"
      when  75
        "MALAYALAM-00"
      when  76
        "INDIAN-00"
      when  77
        "MARATHI-00"
      when  78
        "MARATHI-00"
      when  79
        "ODIA-00"
      when  80
        "ODIA-00"
      when  81
        "SPOKEN-WORD-00"
      when  82
        "INDIAN-POP-00"
      when  83
        "PUNJABI-00"
      when  84
        "PUNJABI-00"
      when  85
        "RABINDRA-SANGEET-00"
      when  86
        "RAJASTHANI-00"
      when  87
        "RAJASTHANI-00"
      when  88
        "REGIONAL-INDIAN-00"
      when  89
        "REGIONAL-INDIAN-00"
      when  90
        "INDIAN-00"
      when  91
        "INDIAN-00"
      when  92
        "SPOKEN-WORD-00"
      when  93
        "SUFI-AND-GHAZALS-00"
      when  94
        "TAMIL-00"
      when  95
        "TAMIL-00"
      when  96
        "TELUGU-00"
      when  97
        "TELUGU-00"
      when  98
        "INDIAN-00"
      when  99
        "URDU-00"
      when  100
        "URDU-00"
      when 101
        "AMBIENT-00"
      when 102
        "BRAZILIAN-00"
      when 103
        "BALADAS-Y-BOLEROS-00"
      when 104
        "BALADAS-Y-BOLEROS-00"
      when 105
        "CARIBBEAN-00"
      when 106
        "CUBAN-00"
      when 107
        "LATIN-RAP-00"
      when 108
        "REGGAETON-Y-HIP-HOP-00"
      when 109
        "REGGAETON-Y-HIP-HOP-00"
      when 110
        "REGIONAL-MEXICANO-00"
      when 111
        "REGIONAL-MEXICANO-00"
      when 112
        "SALSA-Y-TROPICAL-00"
      when 113
        "TANGO-00"
      when 114
        "SALSA-Y-TROPICAL-00"
      when 115
        "AXE-00"
      when 116
        "BAILE-FUNK-00"
      when 117
        "BOSSA-NOVA-00"
      when 118
        "CHORO-00"
      when 119
        "FORRO-00"
      when 120
        "FREVO-00"
      when 121
        "MPB-00"
      when 122
        "PAGODE-00"
      when 123
        "SAMBA-00"
      when 124
        "SERTANEJO-00"
      when 125
        "AFRICAN-00"
      when 126
        "AFROBEATS-00"
      when 127
        "AFRO-POP-00"
      when 128
        "AFRO-FUSION-00"
      when 129
        "AFRO-SOUL-00"
      when 130
        "AFRO-HOUSE-00"
      when 131
        "AMAPIANO-00"
      when 132
        "BONGO-FLAVA-00"
      when 133
        "HIGHLIFE-00"
      when 134
        "MASKANDI-00"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
