module DistributionSystem::Itunes
  class Artist < DistributionSystem::Artist
    # iTunes specific metadata elements
    attr_accessor :role, :primary

    def initialize(options = {})
      # Defaults
      options = {
        role: "Performer",
        primary: @primary
      }.merge(options)

      super
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.artist do |artist|
        artist.name    name
        artist.primary primary.to_s
        artist.roles do |r|
          r.role role
        end
      end
    end

    def valid?
      return false if @name.to_s == ""
      return false unless @primary.to_s == "true" || @primary.to_s == "false"

      true
    end
  end
end
