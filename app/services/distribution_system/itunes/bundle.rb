module DistributionSystem::Itunes
  class Bundle
    include DistributionSystem::TrackLength
    include DistributionSystem::Transcodable

    attr_reader :dir, :album, :upc, :dirname, :incoming_dir, :transcoder, :preset

    def initialize(work_dir, transcoder, s3, album)
      raise "Invalid Album" unless album.valid?

      @album        = album
      @upc          = @album.upc
      @bundle_name  = @upc.to_s + ".itmsp"
      @work_dir     = work_dir
      @itunes_dir   = File.join(@work_dir, "Itunes")
      @dirname      = File.join(@itunes_dir, @bundle_name)
      @transcoder   = transcoder
      @s3           = s3
    end

    def create_bundle
      FileUtils.mkdir_p(dirname)
      @dir = Dir.new(dirname)
    end

    def write_metadata
      write_lyrics_metadata
      write_album_metadata
    end

    def write_album_metadata
      File.open(File.join(@dir.path, "metadata.xml"), "w") do |f|
        f.write @album.to_xml
      end
    end

    def write_lyrics_metadata
      tracks_with_lyrics.each do |track|
        write_track_lyrics_metadata(track)
      end
    end

    def write_track_lyrics_metadata(track)
      filename = "#{track.isrc}.ttml"
      filepath = File.join(@dir.path, filename)
      File.open(filepath, "w") do |f|
        f.write track.lyrics.to_xml
      end
      track.lyrics.update(
        file_name: filename,
        file_size: File.size(filepath),
        file_checksum: DistributionSystem::CheckSumHelper.checksum_file(filepath)
      )
    end

    def tracks_with_lyrics
      @album.tracks.select { |track| track.lyrics.try(:content) }
    end

    def collect_files
      artwork_extension = @album.artwork_s3_key.split(".").last
      image_source = File.join(@dir.path, "#{upc}_cover.#{artwork_extension}")
      if @album.deliver_assets?
        @s3.get(image_source, @album.artwork_s3_key)
        new_image = image_source.split(".")[0..-2].join(".") + ".jpg"
        DistributionSystem::ImageUtils.convert_image(image_source, new_image)

        @album.artwork_file.asset = new_image
        # get the booklet if there is one
        if @album.booklet_file
          booklet_source = File.join(@dir.path, "#{upc}_booklet.pdf")
          @s3.get(booklet_source, @album.booklet_file.asset)
          @album.booklet_file.asset = booklet_source
        end
      end

      # get the album tracks
      @album.tracks.each do |track|
        next if @album.takedown # don't download the tracks if we're doing a takedown
        next if @album.delivery_type != "full_delivery" && !track.duration.nil?

        original_file = File.join(@dir.path, track.temp_filename)
        @s3.bucket = track.s3_bucket unless track.s3_bucket.nil?
        @s3.get(original_file, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)
        track.duration = seconds(original_file) if track.duration.nil?

        if @album.delivery_type == "full_delivery"
          # Truncate ringtones to EXACTLY 30s, not 30.001
          @transcoder.until = nil
          if @album.album_type == "ringtone"
            duration = seconds(original_file)
            @transcoder.until = "00:30.00" if duration.to_f >= 30.0
          else
            @transcoder.until = nil
          end

          transcode_track(track)
        end

        FileUtils.rm(original_file)
      end
    end

    def sanitized_filename(track)
      track.title.gsub(/\s/, "_").gsub(/^\W|[^a-zA-Z0-9\-.\_]/, "")
    end

    def track_filename(path, track)
      if @transcoder.get_bit_depth(File.join(@dir.path, track.temp_filename)).to_i == 24
        File.join(path, "#{track.album.upc}_#{track.number}.wav")
      else
        File.join(path, "#{track.album.upc}_#{track.number}.flac")
      end
    end
  end
end
