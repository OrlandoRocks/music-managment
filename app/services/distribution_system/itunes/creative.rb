module DistributionSystem::Itunes
  class Creative < DistributionSystem::Creative
    include DistributionSystem::XmlFormattable
    xml_format :itunes

    def self.valid_apple_id_or_nil(creative:, is_track:)
      result = is_track ? SongArtist.for_creative(creative) : creative.get_apple_artist_id

      return if ExternalServiceId::INVALID_APPLE_IDENTIFIERS.include?(result)

      result
    end

    attr_accessor :apple_artist_id, :primary, :roles, :id

    def initialize(options = {})
      options.each { |k, v| send("#{k}=", v) if respond_to? k }
    end

    def valid?
      @name.to_s != "" && ["true", "false"].include?(@primary.to_s)
    end
  end
end
