module DistributionSystem::Itunes
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength
    include DistributionSystem::XmlFormattable
    xml_format :itunes

    LOCALE_LANGUAGES = ["cmn-Hans", "cmn-Hant", "he", "ja", "ko", "th"].freeze

    attr_accessor :grid,
                  :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :products,
                  :instant_grat,
                  :original_release_date,
                  :language_code,
                  :english_title,
                  :instrumental

    def initialize(options = {})
      options.each { |k, v| send("#{k}=", v) }
    end

    def set_creatives(_album, song)
      self.creatives =
        SongArtist.for_song(song)
                  .map { |creative| build_creative(creative) }
                  .sort_by { |c1, _c2| sort_conditions(c1) }
    end

    def build_creative(creative)
      apple_artist_id = DistributionSystem::Itunes::Creative.valid_apple_id_or_nil(
        creative: creative,
        is_track: true
      )
      is_primary_artist = creative.credit == "primary_artist" || creative.credit == "primary"
      role_from_credit  = set_role(creative.credit)
      itunes_creative   = DistributionSystem::Itunes::Creative.new(
        primary: is_primary_artist,
        name: creative.artist_name,
        roles: creative.role_types.try(:split, ",").try(:map, &:capitalize) || [],
        apple_artist_id: apple_artist_id
      )
      itunes_creative.roles.unshift(role_from_credit) unless itunes_creative.roles.include?(role_from_credit)
      itunes_creative
    end

    def featured_artist(creative)
      creative.roles.include?("With") || creative.roles.include?("Featuring")
    end

    def sort_conditions(creative)
      [creative.primary ? 0 : 1, featured_artist(creative) ? 0 : 1]
    end

    def set_role(role = "")
      case role
      when "primary_artist", "primary"
        "Performer"
      else
        role.capitalize
      end
    end

    def needs_locale?
      LOCALE_LANGUAGES.include?(language_code)
    end

    private

    def audio_language_code(language_code)
      case language_code
      when language_code.include?("cmn-")
        "cmn"
      when language_code.include?("yue-")
        "yue"
      else
        language_code
      end
    end

    def valid_apple_artist_id(creative)
      result = SongArtist.for_creative(creative)

      return if result == ExternalServiceId::UNAVAILABLE

      result
    end
  end
end
