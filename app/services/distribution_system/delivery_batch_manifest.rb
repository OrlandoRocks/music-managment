class DistributionSystem::DeliveryBatchManifest
  include DistributionSystem::XmlFormattable
  xml_format :believe
  xml_validation_with :ddex
  attr_accessor :batch_items, :batch, :delivery_config, :store_name

  def self.write_manifest(delivery_batch)
    new(delivery_batch).to_xml
  end

  def initialize(delivery_batch)
    @batch           = delivery_batch
    @batch_items     = batch.delivery_batch_items.sent
    @delivery_config = StoreDeliveryConfig.where(store_id: batch.store_id).first
  end

  delegate :ddex_version, to: :delivery_config

  def store_name
    Store.find(delivery_config.store_id).short_name
  end

  delegate :remote_dir, to: :delivery_config

  def recipient_info
    { id: delivery_config.party_id, name: delivery_config.party_full_name }
  end

  def xml_timestamp
    Time.now.iso8601
  end

  def messages_count
    batch_items.count
  end
end
