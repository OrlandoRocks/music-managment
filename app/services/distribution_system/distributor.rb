class DistributionSystem::Distributor
  attr_accessor :logger, :transcoder, :remote_dir, :incoming_dir, :s3, :bundle, :batch_id, :delivery_config, :remote_server

  def initialize(options)
    @transcoder       = options[:transcoder]
    @incoming_dir     = options[:incoming_dir]
    @s3               = options[:s3]
    @bundle           = options[:bundle]
    @schema           = options[:schema]
    @delivery_config  = StoreDeliveryConfig.for(@bundle.album.store_name)
    @remote_dir       = delivery_config.try(:remote_dir)
    @remote_server    = DistributionSystem::Server.server_for(@bundle.album.store_name)
    @logger           = options[:logger] || Rails.logger
  end

  def distribute(album, distribution)
    @distribution = distribution

    Rails.logger.info "Preparing to upload album #{album.upc}| album_id=#{album.tunecore_id} #{album}"
    raise "Invalid Album" unless album.valid?

    @bundle.create_bundle
    collect_files(distribution) if full_delivery?
    write_metadata(distribution)
    send_bundle
  end

  def validate_bundle
    raise "XML metadata is not valid: #{validation_errors}" unless validate!
  end

  def validate!
    @schema.present? ? validate_with_schema : validate_with_xml_formatter
  end

  def can_be_validated?
    @schema.present? || @bundle.album.respond_to?(:validate_xml)
  end

  def validate_with_schema
    @schema.validate(File.join(@bundle.dir.path, "#{@bundle.album.upc}.xml"))
  end

  def validate_with_xml_formatter
    ["37", "341"].include?(@bundle.album.ddex_version) ? @bundle.album.validate_xml : true
  end

  def validation_errors
    @schema.present? ? @schema.errors : @bundle.album.xml_validation_errors
  end

  def full_delivery?
    @bundle.album.delivery_type == "full_delivery"
  end

  def collect_files(distribution)
    Rails.logger.info "Preparing to collect album's assets. #{@bundle.album}"
    Distribution::StateUpdateService.update(distribution, { state: "gathering_assets" })
    @bundle.collect_files
  end

  def write_metadata(distribution)
    Rails.logger.info "Preparing to write the metadata file. #{@bundle.album}"
    @bundle.write_metadata
    Distribution::StateUpdateService.update(distribution, { state: "packaged" })
  end

  def send_bundle
    validate_bundle if can_be_validated?
    Rails.logger.info "Sending to #{@bundle.album.store_name} #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id}  #{@bundle.album}"
    do_send_bundle
    Rails.logger.info "completed shipping of the #{@bundle.dirname} | album_id=#{@bundle.album.tunecore_id} #{@bundle.album}"
  end

  def do_send_bundle
    raise NotImplementedError
  end

  def upload_assets(bundle_id, resource_dir, file_extension, remote_server_conn = @remote_server)
    upload_artwork(bundle_id, resource_dir, remote_server_conn)
    upload_tracks(bundle_id, resource_dir, file_extension, remote_server_conn)
  end

  def upload_artwork(bundle_id, resource_dir, remote_server_conn)
    artwork_file        = File.join(@bundle.dir.path, "#{bundle_id}.jpg")
    remote_artwork_file = File.join(resource_dir, "#{bundle_id}.jpg")
    Rails.logger.info "  uploading artwork. | album_id=#{@bundle.album.tunecore_id} #{@bundle.album}"
    remote_server_conn.upload(artwork_file, remote_artwork_file)
  end

  def upload_tracks(bundle_id, resource_dir, file_extension, remote_server_conn)
    @bundle.album.tracks.each do |track|
      Rails.logger.info "  uploading track #{track.number}. | album_id=#{@bundle.album.tunecore_id}  #{@bundle.album}"
      track_file = File.join(@bundle.dir.path, "#{bundle_id}_01_#{track.number}.#{file_extension}")
      remote_track_file = File.join(resource_dir, "#{bundle_id}_01_#{track.number}.#{file_extension}")
      remote_server_conn.upload(track_file, remote_track_file)
    end
  end

  def upload_metadata(bundle_id, remote_dir, remote_server_conn = @remote_server)
    Rails.logger.info "  uploading metadata. | album_id=#{@bundle.album.tunecore_id}  #{@bundle.album}"
    metadata_filename        = File.join(@bundle.dir.path, "#{bundle_id}.xml")
    remote_metadata_filename = File.join(remote_dir, "#{bundle_id}.xml")
    remote_server_conn.upload(metadata_filename, remote_metadata_filename)
  end

  def send_complete_file(batch_dir, bundle_dir, batch_id)
  end

  def clean_up
    return unless Rails.env.production?

    dir = nil
    if @package_dir
      dir = @package_dir
    elsif @bundle
      dir = @bundle.dirname
    end

    if dir.nil?
      @logger.error("Distributor doesn't have a directory to cleanup!")
      return
    end

    begin
      if dir.respond_to?(:each)
        dir.each do |d|
          FileUtils.rm_rf(d)
        end
      else
        FileUtils.rm_rf(dir)
      end
      @logger.info "Distributor has deleted #{dir}"
    rescue # Ignore if directory already cleaned up
      @logger.error("Distributor had a problem deleting directory: #{dir.inspect}")
    end
  end

  def archive_metadata(bucket, store, upc, filename)
    return unless Rails.env.production?

    begin
      metadata_path = ""
      metadata_path =
        if store.match?(/itunes/i)
          File.join(@bundle.dirname, "metadata.xml")
        elsif @bundle && @bundle.respond_to?(:metadata_filepath)
          @bundle.metadata_filepath
        else
          File.join(@bundle.dirname, "#{upc}.xml")
        end

      bucket.put(metadata_path, "#{upc}/#{store}/#{filename}.xml")

      true
    rescue => e
      Rails.logger.error "Error archiving metadata: #{e.message}  #{@bundle.album}"
      false
    end
  end
end
