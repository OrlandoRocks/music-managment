module DistributionSystem::YoutubeSr
  class Converter < DistributionSystem::TrackMonetization::Converter
    include DistributionSystem::YoutubeDeliverable
    ALBUM_KLASS = DistributionSystem::YoutubeSr::Album

    def build_album(tc_album, delivery_type)
      super
      album.genres = DistributionSystem::YoutubeSr::GenreHelper.get_genres(tc_album.genres)
      album.countries = territory_codes(tc_album)
      deliver_album_to_google_play(tc_album) unless art_track_takedown?(tc_album) || yt_cutover?
      raise "Album validation failed. One or more required attributes are missing." unless album.valid?

      album
    end

    def deliver_album_to_google_play(tc_album)
      TrackMonetization::YoutubeSrUpdateService.send_track_album(tc_album)
    end

    def art_track_takedown?(tc_album)
      tc_album.takedown? || tc_album.salepoints
                                    .where(store_id: Store::GOOGLE_STORE_ID)
                                    .any?(&:taken_down?)
    end

    def yt_cutover?
      Store.find(Store::GOOGLE_STORE_ID).delivery_service == Store::TC_DISTRIBUTOR
    end
  end
end
