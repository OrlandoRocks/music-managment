module DistributionSystem::YoutubeSr
  class GenreHelper
    def self.get_genres(genres)
      youtube_genres = []
      genres.map do |genre|
        youtube_genre = get_youtube_sr_genre(genre.id)
        youtube_genres.push(youtube_genre)
      end
      youtube_genres
    end

    def self.get_youtube_sr_genre(genre_id)
      case genre_id
      when 1
        "Alternative & Punk"
      when 3
        "Blues"
      when 4
        "Other"
      when 5
        "Classical"
      when 6
        "Other"
      when 7
        "Dance & Electronic"
      when 8
        "Dance & Electronic"
      when 9
        "Country & Folk"
      when 10
        "Pop"
      when 11
        "Country & Folk"
      when 12
        "Pop"
      when 13
        "Hip Hop & Rap"
      when 14
        "Holiday"
      when 15
        "Gospel & Religious"
      when 16
        "Jazz"
      when 17
        "World"
      when 18
        "Moods"
      when 19
        "Classical"
      when 20
        "Pop"
      when 21
        "R&B"
      when 22
        "World"
      when 23
        "Rock"
      when 24
        "Soundtrack"
      when 26
        "Other"
      when 27
        "World"
      when 28
        "Country & Folk"
      when 29
        "Country & Folk"
      when 30
        "Other"
      when 31
        "Metal"
      when 32
        "Pop"
      when 33
        "Pop"
      when 34
        "Rock"
      when 35
        "Jazz"
      when 36
        "Other"
      when 37
        "Classical"
      when 38
        "Instrumental"
      when 39
        "Instrumental"
      when (40..100)
        "World"
      when 101
        "Ambient"
      when 102
        "Brazilian"
      when 103
        "Baladas"
      when 104
        "Boleros"
      when 105
        "Caribbean"
      when 106
        "Cuban"
      when 107
        "Latin Hip-Hop"
      when 108
        "Latin Trap"
      when 109
        "Latin Urban"
      when 110
        "Ranchera"
      when 111
        "Regional Mexicano"
      when 112
        "Salsa/Merengue"
      when 113
        "Tango"
      when 114
        "Tropical"
      when 115
        "Axé"
      when 116
        "Baile Funk"
      when 117
        "Bossa Nova"
      when 118
        "Chorinho"
      when 119
        "Forró"
      when 120
        "Frevo"
      when 121
        "MPB"
      when 122
        "Pagode"
      when 123
        "Samba"
      when 124
        "Sertanejo"
      when 125
        "African"
      when 126
        "Afrobeats"
      when 127
        "Afropop"
      when 128
        "Afro-fusion"
      when 129
        "Afro-soul"
      when 130
        "Afro house"
      when 131
        "Amapiano"
      when 132
        "Bongo Flava"
      when 133
        "Highlife"
      when 134
        "Maskandi"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
