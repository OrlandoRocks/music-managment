module DistributionSystem::YoutubeSr
  class Album < DistributionSystem::Believe::Album
    include DistributionSystem::XmlFormattable
    xml_format :believe
    xml_validation_with :ddex

    attr_accessor :vendor_code,
                  :grid,
                  :title_version,
                  :related_albums,
                  :logger,
                  :excluded_countries

    ASSET_LABEL_COUNTRIES = [
      "vietnam",
    ].freeze

    def valid?
      return false if tracks.count > 1

      super
    end

    def indication
      false
    end

    def customer_country_name
      @customer_country_name ||= Person.find_by(id: customer_id)&.country_name
    end

    def needs_asset_label?
      ASSET_LABEL_COUNTRIES.include?(customer_country_name.to_s.downcase)
    end
  end
end
