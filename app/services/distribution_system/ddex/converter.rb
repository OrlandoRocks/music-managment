module DistributionSystem::DDEX
  class Converter < DistributionSystem::Believe::Converter
    description "DDEX"
    handle_salepoints(/.*/)
  end
end
