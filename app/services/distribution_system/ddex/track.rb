module DistributionSystem::DDEX
  class Track < DistributionSystem::Believe::Track
    xml_format :believe
    xml_validation_with :ddex
  end
end
