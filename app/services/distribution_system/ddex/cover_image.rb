module DistributionSystem::DDEX
  class CoverImage
    attr_accessor :asset, :size

    def initialize(options = {})
      options.each { |k, v| self[k] = v }
    end

    # forward compatability
    def [](opt)
      instance_variable_get("@#{opt}")
    end

    def []=(opt, val)
      instance_variable_set("@#{opt}", val)
    end
  end
end
