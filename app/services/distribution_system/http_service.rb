module DistributionSystem
  class HttpService
    def post(url, parameters = {})
      uri = URI.parse(url)
      path = (uri.path == "") ? "/" : uri.path
      request = Net::HTTP::Post.new(path)
      request.set_form_data(parameters)
      response =
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.request(request)
        end
      response.error! unless response.kind_of? Net::HTTPSuccess
      response.body.to_s
    end

    def get(url, headers = {})
      uri = URI.parse(url)
      path = (uri.path == "") ? "/" : uri.path
      response =
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.get path, headers
        end
      response.error! unless response.kind_of? Net::HTTPSuccess
      response.body.to_s
    end

    def put(url, content, headers = {})
      uri = URI.parse(url)
      path = (uri.path == "") ? "/" : uri.path
      request = Net::HTTP::Put.new(path, headers)
      request.body = content
      response =
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.request(request)
        end
      response.error! unless response.kind_of? Net::HTTPSuccess
      response.body.to_s
    end
  end
end
