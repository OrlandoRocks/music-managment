class DistributionSystem::Jbhifi::Album < DistributionSystem::Album
  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :logger

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.tag!(
      "ern:NewReleaseMessage",
      {
        "xmlns:ern" => "http://ddex.net/xml/ern/341",
        "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation" => "http://ddex.net/xml/ern/341 http://ddex.net/xml/ern/341/release-notification.xsd",
        "MessageSchemaVersionId" => "ern/341"
      }
    ) do |ern|
      ern.MessageHeader do |message_header|
        message_header.MessageThreadId 1
        message_header.MessageId get_uuid
        message_header.MessageSender do |message_sender|
          message_sender.PartyId DDEX_SENDER_ID
          message_sender.PartyName do |party_name|
            party_name.FullName "TuneCore"
          end
        end
        message_header.MessageRecipient do |message_recipient|
          message_recipient.PartyId "PADPIDA2011090201Q"
          message_recipient.PartyName do |_party_name|
            message_recipient.FullName "Jbhifi"
          end
        end
        message_header.MessageCreatedDateTime Time.now.iso8601
        message_header.MessageControlType "LiveMessage"
      end # message header

      if %w[metadata_only metadata_with_assets].include?(delivery_type) || takedown
        ern.UpdateIndicator "UpdateMessage"
      else
        ern.UpdateIndicator "OriginalMessage"
      end

      ern.ResourceList do |resource_list|
        tracks.each do |track|
          track.to_xml(resource_list)
        end
        artwork_file.to_xml(resource_list, self)
      end # resource list

      ern.ReleaseList do |release_list|
        release_list.Release do |release|
          release.ReleaseId do |release_id|
            release_id.ICPN(upc, { "IsEan" => "false" })
          end
          release.ReleaseReference "R0"
          release.ReferenceTitle do |reference_title|
            reference_title.TitleText title
          end
          release.ReleaseResourceReferenceList do |reference_list|
            tracks.each do |track|
              reference_list.ReleaseResourceReference(
                "A#{track.number}",
                { "ReleaseResourceType" => "PrimaryResource" }
              )
            end
            reference_list.ReleaseResourceReference(
              "A#{tracks.size + 1}",
              { "ReleaseResourceType" => "SecondaryResource" }
            )
          end
          release.ReleaseType album_type
          release.ReleaseDetailsByTerritory do |release_details|
            release_details.TerritoryCode "Worldwide"
            release_details.DisplayArtistName artists.detect { |a|
                                                a.role == "primary_artist" or a.role == "primary"
                                              }.name
            release_details.LabelName @label_name
            release_details.Title("TitleType" => "DisplayTitle") do |_title|
              _title.TitleText title
            end

            display_artist_tag(release_details, self)

            release_details.ParentalWarningType explicit_lyrics ? "Explicit" : "NotExplicit"

            release_details.ResourceGroup do |resource_group1|
              resource_group1.ResourceGroup do |resource_group2| # ugh
                resource_group2.SequenceNumber "1"
                tracks.each do |track|
                  resource_group2.ResourceGroupContentItem do |content_item|
                    content_item.SequenceNumber track.number
                    content_item.ResourceType "SoundRecording"
                    content_item.ReleaseResourceReference "A#{track.number}"
                  end
                end
              end # resource group 2
              resource_group1.ResourceGroupContentItem do |content_item| # artwork
                content_item.ResourceType "Image"
                content_item.ReleaseResourceReference "A#{tracks.size + 1}"
              end
            end # resource group 1
            release_details.Genre do |genre|
              genre.GenreText genres.first
            end
            release_details.OriginalReleaseDate release_date
          end
          release.PLine do |pline|
            pline.Year copyright_pline.split(" ")[0]
            pline.PLineText copyright_pline
          end
          release.CLine do |cline|
            cline.Year copyright_cline.split(" ")[0]
            cline.CLineText copyright_cline
          end
        end

        # Track release info
        tracks.each do |track|
          release_list.Release do |release|
            release.ReleaseId do |release_id|
              release_id.ISRC track.isrc
            end
            release.ReleaseReference "R#{track.number}"
            release.ReferenceTitle do |reference_title|
              reference_title.TitleText track.title
            end
            release.ReleaseResourceReferenceList do |reference_list|
              reference_list.ReleaseResourceReference(
                { "ReleaseResourceType" => "PrimaryResource" },
                "A#{track.number}"
              )
            end
            release.ReleaseType "TrackRelease"
            release.ReleaseDetailsByTerritory do |details_by_territory|
              details_by_territory.TerritoryCode "Worldwide"
              details_by_territory.DisplayArtistName track.artists.first.name
              details_by_territory.LabelName @label_name
              details_by_territory.Title({ "TitleType" => "DisplayTitle" }) do |title|
                title.TitleText track.title
              end # title

              display_artist_tag(details_by_territory, track)

              details_by_territory.ParentalWarningType track.explicit_lyrics ? "Explicit" : "NotExplicit"

              details_by_territory.ResourceGroup do |resource_group|
                resource_group.SequenceNumber track.number
                resource_group.ResourceGroupContentItem do |content_item|
                  content_item.SequenceNumber track.number
                  content_item.ResourceType "SoundRecording"
                  content_item.ReleaseResourceReference "A#{track.number}"
                end # content_item
              end # resource_group
              details_by_territory.Genre do |genre|
                genre.GenreText @genres.first
              end # genre
              details_by_territory.OriginalReleaseDate release_date
            end # details_by_territory
            release.PLine do |pline|
              pline.Year @copyright_pline.split(" ")[0]
              pline.PLineText @copyright_pline
            end # pline
            release.CLine do |cline|
              cline.Year @copyright_cline.split(" ")[0]
              cline.CLineText @copyright_cline
            end # cline
          end # release
        end # tracks.each
      end # release list

      # deals
      ern.DealList do |deal_list|
        deal_list.ReleaseDeal do |release_deal|
          release_deal.DealReleaseReference "R0"
          release_deal.Deal do |deal|
            deal_terms_tag(deal)
          end
          release_deal.EffectiveDate actual_release_date
        end

        # Track deals
        tracks.each do |track|
          next if track.album_only

          deal_list.ReleaseDeal do |release_deal|
            release_deal.DealReleaseReference "R#{track.number}"
            release_deal.Deal do |deal|
              deal_terms_tag(deal)
            end # deal
            release_deal.EffectiveDate actual_release_date
          end # release_Deal
          # if !track.album_only
        end # tracks.each
      end
    end # ern
  end

  def display_artist_tag(parent_tag, media)
    statuses = ["primary", "primary_artist"]
    media.artists.select { |a| statuses.include?(a.role) }.each do |artist|
      parent_tag.DisplayArtist do |display_artist|
        display_artist.PartyName do |party_name|
          party_name.FullName artist.name
        end
        display_artist.ArtistRole "MainArtist"
      end
    end
  end

  def deal_terms_tag(parent_tag)
    parent_tag.DealTerms do |deal_terms|
      deal_terms.CommercialModelType "SubscriptionModel"

      if takedown
        deal_terms.TakeDown "true"
      else
        deal_terms.Usage do |usage|
          usage.UseType "OnDemandStream"
        end
      end

      @countries.each do |country_code|
        deal_terms.TerritoryCode country_code
      end

      deal_terms.ValidityPeriod do |validity_period|
        validity_period.StartDate actual_release_date
      end
    end
  end

  def valid?
    [@upc, @title, @release_date, @label_name, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
    end

    true
  end
end
