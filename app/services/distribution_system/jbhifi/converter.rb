class DistributionSystem::Jbhifi::Converter < DistributionSystem::Converter
  description "Jbhifi"
  handle_salepoints(/^Jbhifi/)

  ALBUM_KLASS           = DistributionSystem::Jbhifi::Album
  TRACK_KLASS           = DistributionSystem::Jbhifi::Track
  ARTIST_KLASS          = DistributionSystem::Jbhifi::Artist

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
    @supports_metadata_with_assets = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    album = DistributionSystem::Jbhifi::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: GDistributionSystem::Jbhifi::enreHelper.get_genres(tunecore_album.genres),
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artists: get_album_artists(tunecore_album),
        artwork_file: DistributionSystem::Jbhifi::CoverImage.new(asset: tunecore_album.artwork.s3_asset.key),
        delivery_type: delivery_type,
        takedown: set_takedown(tunecore_album, tc_salepoints, "Jbhifi"),
        countries: countries(tunecore_album),
        album_type: tunecore_album.album_type,
        actual_release_date: tunecore_album.sale_date
      }
    )

    tunecore_album.songs.each do |song|
      add_track(album, song)
    end

    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    album
  end

  def add_track(album, song)
    track = DistributionSystem::Jbhifi::Track.new(
      {
        album: album,
        number: song.track_num,
        title: song.name,
        artists: get_track_artists(song),
        sku: song.sku,
        isrc: song.isrc,
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{File.extname(song.upload.uploaded_filename)}",
        s3_key: song_s3key(song),
        s3_bucket: song_s3bucket(song),
        album_only: song.album_only,
        free_song: song.free_song,
        asset_url: song.external_asset_url,
        song_id: song.id
      }
    )
    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end
end
