require_relative "../track"
require_relative "../track_length"

module DistributionSystem::Jbhifi
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger,
                  :artists

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      @duration = iso8601(@audio_file)

      xml.SoundRecording do |sound_recording|
        sound_recording.SoundRecordingId do |sound_recording_id|
          sound_recording_id.ISRC isrc
          # sound_recording_id.ProprietaryId("GGL_USR_ID:#{song_id}", {'NameSpace' => "DPID:#{album.ddex_sender_id}"})
        end
        sound_recording.ResourceReference "A#{number}"
        sound_recording.ReferenceTitle do |reference_title|
          reference_title.TitleText title
        end
        sound_recording.Duration @duration
        sound_recording.SoundRecordingDetailsByTerritory do |recording_details|
          recording_details.TerritoryCode "Worldwide"
          recording_details.Title(TitleType: "DisplayTitle") do |_title|
            _title.TitleText title
          end

          album.display_artist_tag(recording_details, self)

          artists.select { |a| a.role == "featuring" }.each do |artist|
            recording_details.DisplayArtist do |display_artist|
              display_artist.PartyName do |party_name|
                party_name.FullName artist.name
              end
              display_artist.ArtistRole "FeaturedArtist"
            end
          end

          recording_details.LabelName album.label_name
          recording_details.PLine do |pline|
            pline.Year album.copyright_pline.split(" ")[0]
            pline.PLineText album.copyright_pline
          end
          recording_details.Genre do |genre|
            genre.GenreText album.genres.first
          end

          recording_details.ParentalWarningType explicit_lyrics ? "Explicit" : "NotExplicit"

          unless album.metadata_only_delivery?
            recording_details.TechnicalSoundRecordingDetails do |technical_details|
              technical_details.TechnicalResourceDetailsReference "T#{number}"
              technical_details.File do |file|
                file.FileName audio_file.split("/").last
                file.FilePath "resources/"
                file.HashSum do |hash_sum|
                  hash_sum.HashSum DistributionSystem::CheckSumHelper::checksum_file(audio_file)
                  hash_sum.HashSumAlgorithmType "MD5"
                end
              end
            end
          end # end if
        end
      end
    end
  end
end
