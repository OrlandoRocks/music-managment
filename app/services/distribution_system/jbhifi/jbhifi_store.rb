require_relative "../shell"
require "builder"

module DistributionSystem::Jbhifi
  class JbhifiStore
    attr_accessor :jbhifi_provider
    attr_reader :schema

    def initialize(options)
      @work_dir =  options[:work_dir] or raise "No Jbhifi directory!"
      @jbhifi_dir = File.join(@work_dir, "Jbhifi")
      @logger = options[:logger] or raise "No logger!"
      @jbhifi_server = options[:jbhifi_server] or raise "No Jbhifi server!"
      @remote_root = options[:remote_dir]
      @schema = options[:schema] or raise "No Jbhifi schema!"
    end

    def validate_bundle(bundle)
      @schema.validate(File.join(bundle.dir.path, "#{bundle.album.upc}.xml"), true)
    end

    def send_bundle(bundle)
      time = Time.now.strftime("%Y%m%d%H%M%S").to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s

      bundle_id = bundle.album.upc

      batch_dir = "#{@remote_root}/#{time.to_i}"
      remote_dir = "#{@remote_root}/#{time.to_i}/" + bundle_id + "/resources"

      @jbhifi_server.mkdir_p(batch_dir)
      @jbhifi_server.mkdir_p "#{batch_dir}/" + bundle_id
      @jbhifi_server.mkdir_p(remote_dir)

      unless bundle.album.metadata_only_delivery?
        artwork_file = File.join(bundle.dir.path, "#{bundle_id}.jpg")
        remote_artwork_file = File.join(remote_dir, "#{bundle_id}.jpg")
        @logger.info "  uploading artwork. | album_id=#{bundle.album.tunecore_id}"
        @jbhifi_server.upload(artwork_file, remote_artwork_file)
        bundle.album.tracks.each do |track|
          @logger.info "  uploading track #{track.number}. | album_id=#{bundle.album.tunecore_id}"
          track_file = File.join(bundle.dir.path, "#{bundle_id}_01_#{track.number}.mp3")
          remote_track_file = File.join(remote_dir, "#{bundle_id}_01_#{track.number}.mp3")
          @jbhifi_server.upload(track_file, remote_track_file)
        end
      end

      @logger.info "  uploading metadata. | album_id=#{bundle.album.tunecore_id}"
      metadata_filename = File.join(bundle.dir.path, "#{bundle_id}.xml")
      remote_metadata_filename = File.join(remote_dir + "/../", "#{bundle_id}.xml")
      @jbhifi_server.upload(metadata_filename, remote_metadata_filename)
      `touch /tmp/BatchComplete_#{time}.xml`
      @jbhifi_server.upload("/tmp/BatchComplete_#{time}.xml", "#{batch_dir}/BatchComplete_#{time}.xml")
      @jbhifi_server.start_processing

      true
    end
  end
end
