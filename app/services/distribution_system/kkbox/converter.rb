module DistributionSystem::Kkbox
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^KKBox$/)
  end
end
