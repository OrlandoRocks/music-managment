module DistributionSystem::SimfyAfrica
  class Distributor < DistributionSystem::Distributor
    def distribute(album, distribution)
      Rails.logger.info "Preparing to upload album #{album.upc}| album_id=#{album.tunecore_id} #{album}"
      raise "Invalid Album" unless album.valid?

      @bundle.create_bundle
      collect_files(distribution)
      write_metadata(distribution)
      send_bundle
    end

    def do_send_bundle
      bundle_id  = @bundle.album.upc
      @batch_dir = "#{datestamp}_#{timestamp}"
      remote_dir = "#{@batch_dir}/#{bundle_id}"
      @remote_server.mkdir_p @batch_dir
      @remote_server.mkdir_p remote_dir
      upload_assets(bundle_id, remote_dir, "mp3") if full_delivery?
      upload_metadata(bundle_id, remote_dir)
      upload_batch_complete(remote_dir)
      @remote_server.start_processing
    end

    def datestamp
      Date.today.to_s
    end

    def timestamp
      Time.now.to_i
    end

    def full_delivery?
      !@bundle.album.takedown
    end

    def upload_batch_complete(remote_dir)
      `touch /tmp/delivery.complete`
      @remote_server.upload("/tmp/delivery.complete", "#{remote_dir}/delivery.complete")
    end
  end
end
