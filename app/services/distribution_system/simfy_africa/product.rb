module DistributionSystem::SimfyAfrica
  class Product
    def initialize(options = {})
      # Defaults
      options = { territory: "world" }.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    # Metadata elements
    attr_accessor :sales_start_date, :territory, :wholesale_price_tier, :sales_end_date, :cleared_for_sale, :aod_retail

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.TERRITORY_CODE "**"
      xml.PRICE do |price|
        price.CODE "CUSTOM" # use this custom code if we're offering the album with a different price
        price.EFFECTIVE_DATE cleared_for_sale # 'IMMEDIATE' required
      end
      xml.STREET_DATE sales_start_date.strftime("%Y-%m-%d")
      xml.DOWNLOAD_SUBSCRIPTION_ELIGIBLE "true"
      xml
    end

    def valid?
      # Required fields
      return false if territory.nil?
      return false if wholesale_price_tier.nil?
      return false if cleared_for_sale.nil?

      # Check Values
      return false if sales_start_date && !sales_start_date.respond_to?(:strftime)

      true
    end
  end
end
