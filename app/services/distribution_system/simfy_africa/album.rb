class DistributionSystem::SimfyAfrica::Album < DistributionSystem::Album
  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :delivery_type,
                :logger

  def to_xml(xml = Builder::XmlMarkup.new)
    xml.album do |album|
      album.code upc
      album.licensor "TuneCore"
      album.label label_name
      album.p_line copyright_pline
      album.c_line copyright_cline
      album.upc upc
      album.artist_name artists.first.name
      album.title title
      album.genre genres[0]
      album.original_released_on actual_release_date.to_s
      album.tracks_count tracks.size
      album.disks_count 1
      album.contributions do |contributions|
        artists.each do |artist|
          contributions.contribution({ role: artist.role }, artist.name)
        end # artists
      end # contributions
      album.cover do |cover|
        cover.width 1000
        cover.height 1000
        cover.file_name "#{upc}.jpg"
        cover.checksum DistributionSystem::CheckSumHelper::checksum_file(artwork_file.asset)
        cover.file_size File.size(artwork_file.asset)
      end # cover
      album.updated_at Time.now.iso8601
      album.tracks do |track_xml|
        tracks.each { |track| track.to_xml(track_xml, self) }
      end # track_xml
    end # album
  end

  def valid?
    true
  end
end
