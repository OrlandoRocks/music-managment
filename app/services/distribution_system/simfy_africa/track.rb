module DistributionSystem::SimfyAfrica
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    # SimfyAfrica specific metadata elements
    attr_accessor :title_version,
                  :gapless_play,
                  :lyrics,
                  :liner_notes,
                  :beats_per_minute,
                  :preview_start_index,
                  :chapters,
                  :logger

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new, album = nil)
      @duration = seconds(@audio_file)
      xml.track do |track|
        track.track_code "#{album.upc}_#{number}"
        track.isrc isrc
        track.artist_name artist.name
        track.track_number number
        track.disk_number 1
        track.title title
        track.genre album.genres[0]
        track.duration @duration
        track.file_name "#{album.upc}_01_#{number}.mp3"
        track.size File.size(audio_file)
        track.file_checksum DistributionSystem::CheckSumHelper::checksum_file(audio_file)
        track.rights do |rights|
          album.countries.each do |countries|
            rights.right do |right|
              right.country_code countries

              if album.countries.include?(countries) && !album.takedown
                right.allows_streaming "true"
                right.allows_download "true"
                right.streamable_from album.actual_release_date.to_s
              else
                right.allows_streaming "false"
              end # if
            end # right
          end # countries
        end # rights
        track.explicit_lyrics explicit_lyrics
        track.contributions do |contributions|
          artists = Array(artist)
          artists.each do |artist|
            contributions.contribution({ role: artist.role }, artist.name)
          end # artist
        end # contributins
      end # xml.track
    end

    def valid?
      true
    end
  end
end
