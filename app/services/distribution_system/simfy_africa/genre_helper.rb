module DistributionSystem::SimfyAfrica
  class GenreHelper
    def self.get_genres(genres)
      simfy_genres = []
      genres.map do |genre|
        simfy_genre = get_simfy_genre(genre.id)
        simfy_genres.push(simfy_genre)
      end
      simfy_genres
    end

    def self.get_simfy_genre(genre_id)
      case genre_id
      when 1
        "ALTERNATIVE ROCK"
      when 3
        "BLUES"
      when 4
        "CHILDREN'S MUSIC"
      when 5
        "CLASSICAL"
      when 6
        "MISCELLANEOUS / COMEDY"
      when 7
        "DANCE & DJ"
      when 8
        "DANCE & DJ / ELECTRONICA"
      when 9
        "FOLK"
      when 10
        "INTERNATIONAL / EUROPE"
      when 11
        "FOLK"
      when 12
        "INTERNATIONAL / EUROPE"
      when 13
        "RAP & HIP-HOP"
      when 14
        "MISCELLANEOUS / HOLIDAY"
      when 15
        "CHRISTIAN & GOSPEL"
      when 16
        "JAZZ"
      when 17
        "LATIN MUSIC"
      when 18
        "NEW AGE"
      when 19
        "OPERA & VOCAL"
      when 20
        "POP"
      when 21
        "R&B"
      when 22
        "INTERNATIONAL / REGGAE"
      when 23
        "ROCK"
      when 24
        "SOUNDTRACKS"
      when 26
        "OPERA & VOCAL / VOCAL NON-OPERA"
      when 27
        "INTERNATIONAL"
      when 28
        "COUNTRY / ALT-COUNTRY & AMERICANA"
      when 29
        "COUNTRY"
      when 30
        "MISCELLANEOUS / POETRY, SPOKEN WORD & INTERVIEWS"
      when 31
        "HARD ROCK & METAL"
      when 32
        "J-POP"
      when 33
        "K-POP"
      when 34
        "ROCK / SINGER-SONGWRITER"
      when 35
        "BIG BAND"
      when 36
        "FITNESS / EXERCISE"
      when 37
        "HIGH CLASSICAL"
      when 38
        "INSTRUMENTAL"
      when 39
        "INSTRUMENTAL"
      when (40..100)
        "INTERNATIONAL"
      when 101
        "AMBIENT"
      when 102
        "BRAZILIAN"
      when 103
        "BALADAS"
      when 104
        "BOLEROS"
      when 105
        "CARIBBEAN"
      when 106
        "CUBAN"
      when 107
        "LATIN HIP-HOP"
      when 108
        "LATIN TRAP"
      when 109
        "LATIN URBAN"
      when 110
        "RANCHERA"
      when 111
        "REGIONAL MEXICANO"
      when 112
        "SALSA/MERENGUE"
      when 113
        "TANGO"
      when 114
        "TROPICAL"
      when 115
        "AXÉ"
      when 116
        "BAILE FUNK"
      when 117
        "BOSSA NOVA"
      when 118
        "CHORINHO"
      when 119
        "FORRÓ"
      when 120
        "FREVO"
      when 121
        "MPB"
      when 122
        "PAGODE"
      when 123
        "SAMBA"
      when 124
        "SERTANEJOS"
      when 125
        "AFRICAN"
      when 126
        "AFROBEATS"
      when 127
        "AFROPOP"
      when 128
        "AFRO-FUSION"
      when 129
        "AFRO-SOUL"
      when 130
        "AFRO HOUSE"
      when 131
        "AMAPIANO"
      when 132
        "BONGO FLAVA"
      when 133
        "HIGHLIFE"
      when 134
        "MASKANDI"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
