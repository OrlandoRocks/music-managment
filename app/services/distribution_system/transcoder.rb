module DistributionSystem
  class Transcoder
    attr_accessor :logger
    attr_accessor :until # when encoding FLACs stop at this timestamp.  Useful for itunes ringtones.
    attr_accessor :warnings_as_errors

    @@file_extensions = {
      ".mp3" => :mp3,
      ".mp2" => :mp2,
      ".flac" => :flac,
      ".aac" => :aac,
      ".m4a" => :aac,
      ".mp4" => :aac,
      ".wav" => :wav
    }

    def initialize(options)
      @shell        = options[:shell] or raise "No shell!"
      @mp3_decoder  = options[:mp3_decoder]  || MP3_DECODER
      @alac_decoder = options[:alac_decoder] || ALAC_DECODER
      @aac_decoder  = options[:aac_decoder]  || MP4_DECODER
      @flac_encoder = options[:flac_encoder] || FLAC_ENCODER
      @flac_decoder = options[:flac_decoder] || FLAC_DECODER
      @aac_encoder  = options[:aac_encoder]
    end

    def transcode(source, destination, options = {})
      Rails.logger.info("Transcoding... SOURCE: #{source} DESTINATION: #{destination}")
      unless File.exist?(source)
        raise DistributionSystem::DeliveryError.new("Cannot transcode because file #{source} doesn't exist!")
      end

      convert_bit_depth_to(source, options[:bit_depth]) if options[:normalize_bit_depth]
      source_encoding = get_encoding(source)
      dest_encoding   = get_encoding(destination)
      preset          = options[:preset]
      decoder         = "decode_#{source_encoding}"
      encoder         = "encode_#{dest_encoding}"

      @warnings_as_errors = options[:warnings_as_errors] || false

      raise "Cannot decode format #{source_encoding} (decoder: #{decoder})" unless respond_to?(decoder)
      raise "Cannot encode format #{dest_encoding} (encoder #{encoder})" unless respond_to?(encoder)

      Rails.logger.warn "Transcoder: no preset! Consider using a preset for the store." if preset.nil?

      TempDir.enter do |path|
        intermediate_filename = File.join(path, File.basename(source, File.extname(source))) + ".wav"
        send(decoder, source, intermediate_filename)
        send(encoder, *[intermediate_filename, destination, preset].compact)
      end

      return if File.exist?(destination)

      raise DistributionSystem::DeliveryError.new("Transcoding failed because file #{destination} doesn't exist!")
    end

    def get_encoding(filename)
      @@file_extensions[File.extname(filename).downcase] or raise "Unknown file type for #{filename}"
    end

    def decode_flac(flac_filename, dest_filename)
      @shell.command(@flac_decoder, "-d", "-f", "-o", escape_filename(dest_filename), escape_filename(flac_filename))
    end

    def decode_mp3(mp3_filename, dest_filename)
      @shell.command("ffmpeg", "-loglevel fatal", "-y", "-i #{escape_filename(mp3_filename)}", escape_filename(dest_filename))
    end

    def decode_mp2(mp2_filename, dest_filename)
      @shell.command(@mp3_decoder, "--decode", "--silent", mp2_filename, dest_filename)
    end

    def decode_aac(aac_filename, dest_filename)
      begin
        @shell.command(@aac_decoder, "-o", escape_filename(dest_filename), escape_filename(aac_filename))
        decode_alac(aac_filename, dest_filename) unless File.exist?(dest_filename)
      rescue => e
        raise "Unsupported file type: #{aac_filename}.  Error: #{e.message}"
      end
    end

    def decode_alac(alac_filename, dest_filename)
      @shell.command(@alac_decoder, "-i", escape_filename(alac_filename), escape_filename(dest_filename))
    end

    def decode_wav(wav_filename, dest_filename)
      res = FileUtils.cp(wav_filename, dest_filename)
    end

    def encode_flac(source_filename, flac_filename)
      ensure_correct_sample_rate(source_filename)
      mono2stereo(source_filename)
      if @until
        Rails.logger.info("Truncating file at #{@until}")
        begin
          if @warnings_as_errors
            @shell.command(@flac_encoder, "-f", "--until=#{@until}", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
          else
            @shell.command(@flac_encoder, "-f", "--no-warnings-as-errors", "--until=#{@until}", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
          end
        rescue => e
          if e.message[/until value is after end of input/]
            if @warnings_as_errors
              @shell.command(@flac_encoder, "-f", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
            else
              @shell.command(@flac_encoder, "-f", "--no-warnings-as-errors", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
            end
          end
        end
      elsif @warnings_as_errors
        @shell.command(@flac_encoder, "-f", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
      else
        @shell.command(@flac_encoder, "-f", "--no-warnings-as-errors", "-o", escape_filename(flac_filename), "-s", escape_filename(source_filename))
      end
    end

    def encode_aac(source_filename, aac_filename)
      @shell.command(@aac_encoder, "-q 256", "-o", aac_filename, source_filename)
    end

    def encode_mp3(source_filename, mp3_filename, preset = nil)
      # correct_bit_depth(source_filename)
      case preset
      when :tunecore_streaming
        # we use the lala streaming preset for this
      when :amiestreet, :tunecore_streaming_256, :shazam, :jbhifi, :anghami, :revibe, :facebook, :spinlet, :qobuz, :neurotic_media
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "-b 320", escape_filename(source_filename), mp3_filename)
      when :thumbplay
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "-b 300", escape_filename(source_filename), mp3_filename)
      when :lala_download
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "-V 0", escape_filename(source_filename), mp3_filename)
      when :lala_streaming
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "--cbr", "-b 128", escape_filename(source_filename), mp3_filename)
      when :gracenote
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "--cbr", "-b 256", escape_filename(source_filename), mp3_filename)
      when :shockhound
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "-V 2", "--vbr-new", escape_filename(source_filename), mp3_filename)
      when nil
        @shell.command(@mp3_decoder, "--silent", "--id3v1-only", "-b 320", escape_filename(source_filename), mp3_filename)
      else
        raise "cannot transcode to unknown preset #{preset}"
      end
    end

    def encode_wav(source_filename, wav_filename)
      FileUtils.cp(source_filename, wav_filename)
    end

    def wav_filename(filename)
      File.join(File.dirname(filename), "temp", File.basename(filename, File.extname(filename)) + ".wav")
    end

    def to_wma(source, destination, bitrate, sampling_rate, channels)
      ffmpeg_cmd = "ffmpeg -i #{source} -y -acodec wmav2  -ar #{sampling_rate} -ab #{bitrate} -ac #{channels} #{destination}  2>/dev/null"
      output = `#{ffmpeg_cmd}`
      raise "Transcoding Error: #{output}" if $?.exitstatus != 0

      true
    end

    def to_mp3(source, destination, bitrate, sampling_rate, channels)
      ffmpeg_cmd = "ffmpeg -i \"#{source}\" -y -ar #{sampling_rate} -ab #{bitrate} -ac #{channels} #{destination}  2>/dev/null"
      output = `#{ffmpeg_cmd}`
      raise "Transcoding Error: #{output}" if $?.exitstatus != 0

      true
    end

    def convert_bit_depth_to(sourcefile, bit_depth)
      begin
        Rails.logger.info("Converting bit depth to #{bit_depth}")
        @shell.command("sox #{escape_filename(sourcefile)} -b #{bit_depth} #{escape_filename(sourcefile + ".#{bit_depth}bit.wav")} ")
        @shell.command("rm #{escape_filename(sourcefile)}")
        @shell.command("mv #{escape_filename(sourcefile + ".#{bit_depth}bit.wav")} #{escape_filename(sourcefile)}")
      rescue => e
        Rails.logger.warn("Error converting bit depth: #{e.message}")
      end
    end

    def get_bit_depth(file)
      info = `mediainfo #{escape_filename(file)}`
      info[/(\d{2}) bits/, 1]
    end

    def get_channels(file)
      info = `mediainfo #{escape_filename(file)}`
      info[/(\d) channel/, 1]
    end

    def get_sample_rate(file)
      info = `mediainfo #{escape_filename(file)}`
      extract_sample_rate(info)
    end

    def extract_sample_rate(info)
      info[/(\d+\.\d+) KHz/i, 1]
    end

    def ensure_correct_sample_rate(filename)
      sample_rate = get_sample_rate(filename)
      resample_audio(filename) unless sample_rate == "44.1"
    end

    def resample_audio(filename)
      file_ext            = File.extname(filename)
      resampled_filename  = File.basename(filename, file_ext) + "_resampled" + file_ext

      @shell.command("sox #{escape_filename(filename)} #{escape_filename(resampled_filename)} rate 44100")
      @shell.command("rm #{escape_filename(filename)}")
      @shell.command("mv #{escape_filename(resampled_filename)} #{escape_filename(filename)}")
    end

    def mono2stereo(wav_file) # expects the filename to be escaped already
      file = wav_file
      channels = get_channels(file)
      bit_depth = get_bit_depth(file)
      if channels == 2
        Rails.logger.info("#{file} is already stereo, doing nothing")
        nil
      else
        Rails.logger.info("Converting #{escape_filename(file)} to stereo")
        @shell.command("sox #{escape_filename(file)} -c 2 #{escape_filename(file + '.stereo.wav')}")
        @shell.command("rm #{escape_filename(file)}")
        @shell.command("mv #{escape_filename(file + '.stereo.wav')} #{escape_filename(file)}")
      end
    end

    def duration(wav_file)
      playtime = `shntool len -ct "#{wav_file}"`.match(/\s+(\d+):(\d+)/)
      "#{playtime[1]}:#{playtime[2]}"
    end

    def escape_dollar(str)
      str.gsub("$") { '\\$' }
    end

    def escape_backtick(str)
      str.gsub("`") { '\\`' }
    end

    def escape_hash(str)
      str.gsub("#") { '\\#' }
    end

    def escape_single_quotes(str)
      "\"#{str.tr("'", "\'")}\""
    end

    def escape_filename(filename)
      escape_backtick(escape_dollar(escape_single_quotes(filename)))
    end
  end
end
