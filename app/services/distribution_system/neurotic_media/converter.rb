module DistributionSystem::NeuroticMedia
  class Converter < DistributionSystem::Believe::Converter
    handle_salepoints(/^Neurotic/)
  end
end
