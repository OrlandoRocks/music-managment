module DistributionSystem::Spotify
  class GenreHelper
    def self.get_genres(genres)
      spotify_genres = []
      genres.map do |genre|
        spotify_genre = get_spotify_genre(genre.id)
        spotify_genres.push(spotify_genre)
      end
      spotify_genres
    end

    def self.get_spotify_genre(genre_id)
      case genre_id
      when 1
        "Alternative/Indie"
      when 3
        "Blues"
      when 4
        "Kids"
      when 5
        "Classical"
      when 6
        "Comedy"
      when 7
        "Dance"
      when 8
        "Electronic"
      when 9
        "FOLK"
      when 10
        "World/Modern Roots"
      when 11
        "Folk"
      when 12
        "World/Modern Roots"
      when 13
        "Hip Hop/Rap"
      when 14
        "Seasonal"
      when 15
        "Christian"
      when 16
        "Jazz"
      when 17
        "Latin"
      when 18
        "New Age"
      when 19
        "Opera"
      when 20
        "Pop"
      when 21
        "R&B"
      when 22
        "Reggae"
      when 23
        "Rock"
      when 24
        "Soundtrack"
      when 26
        "Vocal"
      when 27
        "World/Modern Roots"
      when 28
        "Country"
      when 29
        "COUNTRY"
      when 30
        "Spoken Word"
      when 31
        "Metal"
      when 32
        "J-Pop"
      when 33
        "Pop"
      when 34
        "Singer-Songwriter"
      when 35
        "Jazz"
      when 36
        "Pop"
      when 37
        "Classical"
      when 38
        "Pop"
      when 39
        "Pop"
      when (40..100)
        "World/Modern Roots"
      when 101
        "Ambient"
      when 102
        "Brazilian"
      when 103
        "Baladas"
      when 104
        "Boleros"
      when 105
        "Caribbean"
      when 106
        "Cuban"
      when 107
        "Latin Hip-Hop"
      when 108
        "Latin Trap"
      when 109
        "Latin Urban"
      when 110
        "Ranchera"
      when 111
        "Regional Mexicano"
      when 112
        "Salsa/Merengue"
      when 113
        "Tango"
      when 114
        "Tropical"
      when 115
        "Axé"
      when 116
        "Baile Funk"
      when 117
        "Bossa Nova"
      when 118
        "Chorinho"
      when 119
        "Forró"
      when 120
        "Frevo"
      when 121
        "MPB"
      when 122
        "Pagode"
      when 123
        "Samba"
      when 124
        "Sertanejo"
      when 125
        "African"
      when 126
        "Afrobeats"
      when 127
        "Afropop"
      when 128
        "Afro-fusion"
      when 129
        "Afro-soul"
      when 130
        "Afro house"
      when 131
        "Amapiano"
      when 132
        "Bongo Flava"
      when 133
        "Highlife"
      when 134
        "Maskandi"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
