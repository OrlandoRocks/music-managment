module DistributionSystem::Spotify
  class Track < DistributionSystem::Believe::Track
    xml_format :spotify
    xml_validation_with :ddex

    attr_accessor :artists
  end
end
