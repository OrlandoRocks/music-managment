module DistributionSystem::Spotify
  class Artist < DistributionSystem::Believe::Artist
    include DistributionSystem::XmlFormattable
    xml_format :spotify

    attr_accessor :role, :role_type, :tunecore_artist_id, :spotify_id, :artist_id

    def self.role_map
      {
        "primary_artist" => "main artist",
        "primary" => "main artist",
        "featuring" => "featured artist",
        "composer" => "composer",
        "songwriter" => "composer",
        "conductor" => "conductor",
        "lyricist" => "lyricist",
        "with" => "with"
      }
    end

    def role_for_spotify
      self.class.role_map[role] || self.class.role_map[role_type] || "contributor"
    end

    def valid?
      @name.to_s != ""
    end

    def url_for_spotify
      url_official || url_blog || url_facebook || url_twitter || url_youtube_channel
    end
  end
end
