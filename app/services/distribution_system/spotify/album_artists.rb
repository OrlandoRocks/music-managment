module DistributionSystem::Spotify::AlbumArtists
  DDEX_ROLLUP_CONTRIBUTOR_ROLES = [
    "remixer",
    "composer",
    "lyricist",
    "producer",
    "conductor",
    "arranger",
    "orchestra",
    "actor"
  ].map(&:freeze).freeze

  def rollup_track_artists
    track_artists = tracks.map(&:artists).flatten.uniq
    track_artists.select do |artist|
      tracks.all? { |t| t.artists.include?(artist) } && (artist.is_featured? || (artist.artist_roles.map(&:role_type) & DDEX_ROLLUP_CONTRIBUTOR_ROLES).present?)
    end
  end

  def rollup_artists
    primary_and_featured_artists | rollup_track_artists
  end
end
