class DistributionSystem::Spotify::Converter < DistributionSystem::Believe::Converter
  include ArelTableMethods
  include ArelSongContributorsJoins
  description "Spotify Music"
  handle_salepoints(/^Spotify$/)

  ALBUM_KLASS = DistributionSystem::Spotify::Album
  ARTIST_KLASS = DistributionSystem::Spotify::Artist

  US_COUNTRIES = ["VI", "UM", "US", "PR", "GU", "AS"].freeze
  SPOKEN_WORD_GENRE_IDS = [2, 6, 30, 36, 56, 81, 92]

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    super
    takedown_at = (tunecore_album.takedown_at || salepoint.takedown_at)
    album.takedown_at = takedown_at.midnight.iso8601[0..18] if takedown_at
    album.actual_release_date = get_timed_release(tunecore_album)
    album.countries = album.countries - US_COUNTRIES if spoken_words_genre?(
      tunecore_album.genres,
      tunecore_album.person
    )

    album.genres = DistributionSystem::Spotify::GenreHelper.get_genres(tunecore_album.genres)
    album
  end

  def get_timed_release(tunecore_album)
    if tunecore_album.timed_release_timing_scenario.present?
      if tunecore_album.timed_release_absolute_time?
        tunecore_album.golive_date.utc.iso8601
      else # relative time
        tunecore_album.golive_date.in_time_zone("Eastern Time (US & Canada)").iso8601[0..18]
      end
    else
      tunecore_album.sale_date.midnight.iso8601[0..18]
    end
  end

  def spoken_words_genre?(genres, person)
    return false unless FeatureFlipper.show_feature?(:spoken_word_genres, person)

    (SPOKEN_WORD_GENRE_IDS - genres.pluck(:id)).length != SPOKEN_WORD_GENRE_IDS.length if genres.present?
  end

  def build_track(album, song)
    super
    @track.explicit_lyrics_label = set_explicit_label(song)
    @track
  end

  def set_label(tunecore_album)
    tunecore_album.label.name if tunecore_album.label
  end

  def set_album_artists(album)
    if album.creatives.present?
      album.creatives.map do |creative|
        self.class::ARTIST_KLASS.new(
          name: creative.artist.name,
          roles: [creative.role],
          spotify_id: creative.get_spotify_artist_id,
          artist_id: creative.artist_id
        )
      end
    else
      [
        self.class::ARTIST_KLASS.new(
          name: album.artist_name,
          roles: PRIMARY_ARTIST_ROLES
        )
      ]
    end
  end

  def build_artist_for_track(creative)
    artist = super
    artist.spotify_id = creative.get_spotify_artist_id
    artist.artist_id = creative.artist_id
    artist
  end

  def set_artist(media)
    artist_array = []
    if media.creatives.empty?
      if media.class.to_s == "Song"
        release_creatives(media.album, "Album").each do |creative|
          a_artist = DistributionSystem::Spotify::Artist.new
          a_artist.name      = creative.name
          a_artist.role      = creative.role
          a_artist.role_type = creative.role_type
          artist_array << a_artist
        end
      else
        a_artist = DistributionSystem::Spotify::Artist.new
        a_artist.name      = media.artist_name
        a_artist.role      = "primary_artist"
        artist_array << a_artist
        media.artist_name
      end
    else
      media_class = media.class.base_class.to_s
      release_creatives(media, media_class).each do |creative|
        a_artist = DistributionSystem::Spotify::Artist.new
        a_artist.name      = creative.name
        a_artist.role      = creative.role
        a_artist.role_type = creative.role_type
        artist_array << a_artist
      end
    end

    if media.class.to_s == "Song" && !media.album.is_various && media.creatives.present?
      release_creatives(media.album, "Album").each do |creative|
        a_artist = DistributionSystem::Spotify::Artist.new
        a_artist.name      = creative.name
        a_artist.role      = creative.role
        a_artist.role_type = creative.role_type
        artist_array.unshift(a_artist) unless artist_array.include?(a_artist)
      end
    end
    artist_array
  end

  def is_album_explicit(songs)
    songs.each do |song|
      return true if to_bool(song.parental_advisory)
    end
    false
  end

  def to_bool(value)
    if value.nil?
      ret = false
    else
      case value
      when "true"
        ret = true
      when "false"
        ret = false
      when true
        ret = true
      when false
        ret = false
      else
        raise "Invalid value : #{value}"
      end
    end
    ret
  end

  def release_creatives(release, release_type)
    Creative.joins(creative_song_roles_join)
            .joins(song_roles_join)
            .where(creativeable_id: release.id)
            .where(creativeable_type: release_type)
            .select([creative_t[Arel.star], song_role_t[:role_type]])
  end

  def set_explicit_label(song)
    if song.clean_version && !song.parental_advisory?
      "ExplicitContentEdited"
    elsif song.parental_advisory?
      "Explicit"
    else
      "NotExplicit"
    end
  end
end
