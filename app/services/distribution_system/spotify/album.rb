class DistributionSystem::Spotify::Album < DistributionSystem::Believe::Album
  include DistributionSystem::XmlFormattable
  include DistributionSystem::Spotify::AlbumArtists
  xml_format :believe
  xml_validation_with :ddex

  def action
    takedown ? "delete" : self.class.action_type_map[delivery_type]
  end

  def template
    action
  end

  def type
    result = is_various ? "compilation" : self.class.album_type_map[album_type]
    return result if result

    raise "album_type: #{result} is not an acceptable type for Spotify"
  end

  def self.action_type_map
    {
      "full_delivery" => "insert",
      "metadata_only" => "update",
      "metadata_update" => "update"
    }
  end

  def self.upc_type_map
    {
      12 => "upc",
      13 => "ean"
    }
  end

  def self.album_type_map
    {
      "Album" => "album",
      "Single" => "single"
    }
  end

  def available_for_streaming
    tracks.all?(&:available_for_streaming)
  end

  def valid?
    [@upc, @title, @release_date, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
      return false if required_field.nil?
    end
    true
  end

  def is_compilation_feature?
    FeatureFlipper.show_feature?(:spotify_compilation_tag, Album.find_by(id: tunecore_id)&.person)
  end

  def is_classical_genre?
    FeatureFlipper.show_feature?(:spotify_classical_tag, Album.find_by(id: tunecore_id)&.person)
  end

  def has_track_level_territories?
    FeatureFlipper.show_feature?(:spotify_track_territories, ::Album.find_by(id: tunecore_id)&.person)
  end

  def has_no_us_track_territory?
    FeatureFlipper.show_feature?(:spotify_no_us_track_territory, ::Album.find_by(id: tunecore_id)&.person)
  end

  def has_track_takedowns?
    FeatureFlipper.show_feature?(:spotify_track_takedowns, ::Album.find_by(id: tunecore_id)&.person)
  end

  def dj_release?
    album = ::Album.find_by(id: tunecore_id)
    FeatureFlipper.show_feature?(:dj_mix_type, album&.person) && album.is_dj_release?
  end
end
