module DistributionSystem::Spotify
  class Distributor < DistributionSystem::Believe::Distributor
    attr_reader :schema,
                :bundle,
                :bundle_id,
                :remote_dir,
                :remote_bundle_dir,
                :manifest_path,
                :manifest_file,
                :remote_manifest_file,
                :remote_delivery_complete
  end
end
