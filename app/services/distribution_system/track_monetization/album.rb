class DistributionSystem::TrackMonetization::Album < DistributionSystem::Believe::Album
  include DistributionSystem::XmlFormattable
  xml_format :believe
  xml_validation_with :ddex

  attr_accessor :vendor_code,
                :grid,
                :title_version,
                :related_albums,
                :logger,
                :excluded_countries

  def valid?
    return false if tracks.count > 1

    super
  end

  def indication
    false
  end
end
