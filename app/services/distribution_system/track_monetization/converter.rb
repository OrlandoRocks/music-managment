class DistributionSystem::TrackMonetization::Converter < DistributionSystem::Believe::Converter
  description "Track Monetization"

  ALBUM_KLASS = DistributionSystem::TrackMonetization::Album

  def initialize
    @supports_metadata_update    = true
    @supports_takedown           = true
    @supports_track_selection    = true
  end

  def convert(track_monetization, delivery_type = "metadata_only")
    @track_monetization = track_monetization
    converted_album     = build_album(track_monetization.album, delivery_type)
    converted_album.tracks << build_track(converted_album, track_monetization.song)
    converted_album
  end

  def build_album(tunecore_album, _delivery_type)
    @album = self.class::ALBUM_KLASS.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genre: tunecore_album.genres.first.id,
        store_name: track_monetization.store.short_name,
        store_id: track_monetization.store_id,
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artists: set_album_artists(tunecore_album),
        artwork_file: set_artwork(tunecore_album),
        booklet_file: if tunecore_album.booklet
                        {
                          asset: tunecore_album.booklet.s3_asset.key,
                          bucket: tunecore_album.booklet.s3_asset.bucket
                        }
                      else
                        {}
                      end,
        delivery_type: track_monetization.delivery_type,
        album_type: tunecore_album.class.name,
        takedown: track_monetization.taken_down?,
        countries: countries(tunecore_album),
        is_various: tunecore_album.is_various?,
        language: tunecore_album.language.description,
        customer_name: tunecore_album.person.name,
        customer_id: tunecore_album.person_id,
        actual_release_date: tunecore_album.sale_date,
        track_only: true
      }
    )
    raise "Album validation failed. One or more required attributes are missing." unless @album.valid?

    @album
  end

  def build_track(album, song)
    @track = self.class::TRACK_KLASS.new(
      {
        album: album,
        song_id: song.id,
        number: song.track_num,
        title: song.name,
        artists: set_song_artists(song),
        sku: song.sku,
        isrc: song.isrc,
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{temp_filename_ext(song)}",
        s3_key: s3_flac_key(song),
        s3_bucket: s3_flac_bucket(song),
        album_only: song.album_only,
        free_song: song.free_song,
        asset_url: song.external_asset_url,
        duration: calculate_duration(song),
        takedown: track_monetization.taken_down?,
        custom_id: "SID_#{song.id}",
        asset_exclusion: add_asset_tag?
      }
    )
  end

  private

  def add_asset_tag?
    track_monetization.store_id == Store::YTSR_STORE_ID && google_takedown? && !album.takedown
  end

  def google_takedown?
    track_monetization.album.takedown_at? || google_salepoint_takedown?
  end

  def google_salepoint_takedown?
    track_monetization.album.salepoints.any? { |sp| sp.store_id == Store::GOOGLE_STORE_ID && sp.takedown_at? }
  end

  attr_reader :album, :track_monetization
end
