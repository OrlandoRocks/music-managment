module DistributionSystem
  class LanguageCodeHelper
    class << self
      attr_accessor :language_code_config
    end

    def self.code(language)
      language_code = language_code_config["language_code"][language]
      raise LanguageCodeNotFound, "The language code for #{language} is not found" unless language_code

      language_code
    end

    def self.language_code_config
      @@language_code_config ||= YAML.load_file(Rails.root.join("config", "language_iso639a2.yaml"))
    end
  end
end
