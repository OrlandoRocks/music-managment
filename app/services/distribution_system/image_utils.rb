module DistributionSystem
  module ImageUtils
    # rubocop:disable Style/NumericPredicate
    def resize(file, width, height)
      cmd = "convert #{file} -resize #{width}x#{height} #{file}"
      out = `#{cmd}`
      raise "Error resizing artwork: #{out}" unless $?.exitstatus == 0
    end
    # rubocop:enable Style/NumericPredicate

    def cp(src_file, dest_file)
      convert_image(src_file, dest_file)
    end

    # rubocop:disable Style/NumericPredicate
    def width_height(file)
      cmd = "python #{File.dirname(__FILE__)}/../../bin/image_dims.py #{file}"
      dims = `#{cmd}`
      raise "Error inspecting artwork" unless $?.exitstatus == 0

      dims.split("x")
    end
    # rubocop:enable Style/NumericPredicate

    # rubocop:disable Style/NumericPredicate
    def convert_image(orig_image, new_image)
      `touch #{new_image}`
      cmd = "convert #{orig_image} #{new_image}"
      output = `#{cmd}`
      raise "Error converting artwork: #{output}" unless $?.exitstatus == 0
    end
    # rubocop:enable Style/NumericPredicate

    module_function :resize, :cp, :width_height, :convert_image
  end
end
