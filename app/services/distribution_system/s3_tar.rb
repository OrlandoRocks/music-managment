module DistributionSystem
  class S3Tar
    def initialize(options)
      @bucket = options[:bucket] or raise "No S3 bucket."
      @shell = options[:shell] or raise "No shell."
    end

    def create(dirpath, s3_key)
      TempDir.enter do |path|
        tarball = File.join(path, "tarball.tgz")
        @shell.command "tar czf #{tarball} -C #{dirpath} ."
        @bucket.put(tarball, s3_key)
      end
    end
  end
end
