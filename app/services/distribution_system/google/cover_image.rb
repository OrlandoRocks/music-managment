module DistributionSystem::Google
  class CoverImage
    attr_accessor :asset

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml, album)
      xml.Image do |image|
        image.ImageType "FrontCoverImage"
        image.ImageId do |image_id|
          image_id.ProprietaryId("#{album.upc}.jpg", { "Namespace" => "DPID:#{DDEX_SENDER_ID}" })
        end
        image.ResourceReference "A#{album.tracks.size + 1}"
        image.ImageDetailsByTerritory do |image_details|
          image_details.TerritoryCode "Worldwide"
          if !album.takedown && album.delivery_type != "metadata_only"
            image_details.TechnicalImageDetails do |techimagedetails|
              techimagedetails.TechnicalResourceDetailsReference "T#{album.tracks.size + 1}"
              techimagedetails.ImageCodecType "JPEG"
              techimagedetails.ImageHeight "1400"
              techimagedetails.ImageWidth "1400"
              techimagedetails.File do |file|
                file.FileName "#{album.upc}.jpg"
                file.FilePath "resources/"
                file.HashSum do |hashsum|
                  hashsum.HashSum DistributionSystem::CheckSumHelper::checksum_file(album.artwork_file.asset)
                  hashsum.HashSumAlgorithmType "MD5"
                end
              end
            end
          end # end if
        end
      end
    end

    def valid?
      extension = File.extname(@asset)
      return false unless [".tif", ".jpg"].include?(extension)

      true
    end
  end
end
