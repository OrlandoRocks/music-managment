module DistributionSystem::Google
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      @batch_id   = set_batch_id
      @batch_dir  = "NewRelease/#{@batch_id}"

      bundle_id  = @bundle.album.upc
      remote_dir = "#{@batch_dir}/#{bundle_id}"
      @remote_server.connect do |sftp|
        sftp.mkdir_p(remote_dir)
        resource_dir = "#{remote_dir}/resources"
        sftp.mkdir_p(resource_dir)
        upload_assets(bundle_id, resource_dir, "flac", sftp) if full_delivery?
        upload_metadata(bundle_id, remote_dir, sftp)
        upload_batch_complete(sftp)
      end
    end

    def upload_batch_complete(sftp)
      `touch /tmp/BatchComplete_#{@batch_id}.xml`
      sftp.upload("/tmp/BatchComplete_#{@batch_id}.xml", "#{@batch_dir}/BatchComplete_#{@batch_id}.xml")
    end

    def set_batch_id
      Time.now.strftime("%Y%m%d%H%M%S").to_s + rand(10).to_s + rand(10).to_s + rand(10).to_s
    end
  end
end
