module DistributionSystem::Google
  class Album < DistributionSystem::Album
    attr_accessor :vendor_code,
                  :grid,
                  :title_version,
                  :related_albums,
                  :delivery_type,
                  :price_tier,
                  :preorder_data,
                  :artist,
                  :featured_artist_names

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.tag!(
        "ern:NewReleaseMessage",
        {
          "xmlns:ern" => "http://ddex.net/xml/ern/37",
          "xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance",
          "xsi:schemaLocation" => "http://ddex.net/xml/ern/37 http://ddex.net/xml/ern/37/release-notification.xsd",
          "MessageSchemaVersionId" => "ern/37"
        }
      ) do |ern|
        ern.MessageHeader do |message_header|
          message_header.MessageThreadId 1
          message_header.MessageId get_uuid
          message_header.MessageSender do |message_sender|
            message_sender.PartyId "PADPIDA2011101104E"
            message_sender.PartyName do |party_name|
              party_name.FullName "TuneCore"
            end
          end
          message_header.MessageRecipient do |message_recipient|
            message_recipient.PartyId "PADPIDA2010120902Y"
            message_recipient.PartyName do |_party_name|
              message_recipient.FullName "Google"
            end
          end
          message_header.MessageCreatedDateTime Time.now.iso8601
        end

        if @delivery_type == "metadata_only" || takedown
          ern.UpdateIndicator "UpdateMessage"
        else
          ern.UpdateIndicator "OriginalMessage"
        end

        ern.ResourceList do |resource_list|
          tracks.each do |track|
            track.to_xml(resource_list)
          end
          artwork_file.to_xml(resource_list, self)
        end

        ern.ReleaseList do |release_list|
          release_list.Release do |release|
            release.ReleaseId do |release_id|
              release_id.ICPN(upc, { "IsEan" => "false" })
            end
            release.ReleaseReference "R0"
            release.ReferenceTitle do |reference_title|
              reference_title.TitleText title
            end
            release.ReleaseResourceReferenceList do |reference_list|
              tracks.each do |track|
                reference_list.ReleaseResourceReference("A#{track.number}", { "ReleaseResourceType" => "PrimaryResource" })
              end
              reference_list.ReleaseResourceReference("A#{tracks.size + 1}", { "ReleaseResourceType" => "SecondaryResource" })
            end
            release.ReleaseType "Album"
            release.ReleaseDetailsByTerritory do |release_details|
              release_details.TerritoryCode "Worldwide"
              release_details.LabelName @label_name
              release_details.Title("TitleType" => "FormalTitle") do |_title|
                _title.TitleText title
              end
              release_details.Title("TitleType" => "DisplayTitle") do |_title|
                _title.TitleText title
              end
              release_details.DisplayArtist do |display_artist|
                display_artist.PartyName do |party_name|
                  party_name.FullName artist.name
                end
                if yt_oac_feature? && artist.channel_id
                  xml.PartyId({ "Namespace" => "DPID:PADPIDA2013020802I" }, artist.channel_id)
                end
                xml.PartyId({ "Namespace" => "DPID:PADPIDA2011101104E" }, artist.tunecore_id)
                display_artist.ArtistRole "MainArtist"
              end
              release_details.ParentalWarningType "Explicit" if explicit_lyrics
              release_details.ResourceGroup do |resource_group1|
                resource_group1.ResourceGroup do |resource_group2| # ugh
                  resource_group2.SequenceNumber "1"
                  tracks.each do |track|
                    resource_group2.ResourceGroupContentItem do |content_item|
                      content_item.SequenceNumber track.number
                      content_item.ResourceType "SoundRecording"
                      content_item.ReleaseResourceReference "A#{track.number}"
                    end
                  end
                end # resource group 2
                resource_group1.ResourceGroupContentItem do |content_item| # artwork
                  content_item.ResourceType "Image"
                  content_item.ReleaseResourceReference "A#{tracks.size + 1}"
                end
              end # resource group 1
              release_details.Genre do |genre|
                genre.GenreText genres.first
              end
              release_details.OriginalReleaseDate release_date
            end
            release.PLine do |pline|
              pline.Year copyright_pline.split(" ")[0]
              pline.PLineText copyright_pline
            end
            release.CLine do |cline|
              cline.Year copyright_cline.split(" ")[0]
              cline.CLineText copyright_cline
            end
          end

          # Track release info
          tracks.each do |track|
            release_list.Release do |release|
              release.ReleaseId do |release_id|
                release_id.ISRC track.isrc
              end
              release.ReleaseReference "R#{track.number}"
              release.ReferenceTitle do |reference_title|
                reference_title.TitleText track.title
              end
              release.ReleaseResourceReferenceList do |reference_list|
                reference_list.ReleaseResourceReference({ "ReleaseResourceType" => "PrimaryResource" }, "A#{track.number}")
              end
              release.ReleaseType "TrackRelease"
              release.ReleaseDetailsByTerritory do |details_by_territory|
                details_by_territory.TerritoryCode "Worldwide"
                details_by_territory.DisplayArtistName track.artist.name
                details_by_territory.LabelName @label_name
                details_by_territory.Title({ "TitleType" => "FormalTitle" }) do |title|
                  title.TitleText track.title
                end # title
                details_by_territory.Title({ "TitleType" => "DisplayTitle" }) do |title|
                  title.TitleText track.title
                end # title
                details_by_territory.DisplayArtist do |display_artist|
                  display_artist.PartyName do |party_name|
                    party_name.FullName track.artist.name
                  end # party_name
                  display_artist.ArtistRole "MainArtist"
                end
                track.featured_artist_names.each do |name|
                  details_by_territory.DisplayArtist do |display_artist|
                    display_artist.PartyName do |party_name|
                      party_name.FullName name
                    end
                    display_artist.ArtistRole "FeaturedArtist"
                  end
                end
                details_by_territory.ParentalWarningType "Explicit" if track.explicit_lyrics
                # details_by_territory.ParentalWarningType
                details_by_territory.ResourceGroup do |resource_group|
                  resource_group.SequenceNumber track.number
                  resource_group.ResourceGroupContentItem do |content_item|
                    content_item.SequenceNumber track.number
                    content_item.ResourceType "SoundRecording"
                    content_item.ReleaseResourceReference "A#{track.number}"
                  end # content_item
                end # resource_group
                details_by_territory.Genre do |genre|
                  genre.GenreText @genres.first
                end # genre
                details_by_territory.OriginalReleaseDate release_date
              end # details_by_territory
              release.PLine do |pline|
                pline.Year @copyright_pline.split(" ")[0]
                pline.PLineText @copyright_pline
              end # pline
              release.CLine do |cline|
                cline.Year @copyright_cline.split(" ")[0]
                cline.CLineText @copyright_cline
              end # cline
            end # release
          end # tracks.each
        end # release list

        # deals
        ern.DealList do |deal_list|
          deal_list.ReleaseDeal do |release_deal|
            release_deal.DealReleaseReference "R0"
            release_deal.Deal do |deal|
              deal.DealReference "something"
              deal.DealTerms do |deal_terms|
                deal_terms.CommercialModelType "AsPerContract"

                if takedown
                  deal_terms.TakeDown "true"
                else
                  deal_terms.Usage do |usage|
                    usage.UseType("UserDefined", "UserDefinedValue" => "GoogleMusicBasic")
                    if all_tracks_are_streamable?
                      usage.UseType("UserDefined", "UserDefinedValue" => "GoogleMusicSubscription")
                    else
                      usage.UseType("UserDefined", "UserDefinedValue" => "NoGoogleMusicSubscription")
                    end
                  end
                end
                countries.each do |country_code|
                  deal_terms.TerritoryCode country_code
                end
                deal_terms.PriceInformation do |price_information|
                  # uncomment this to use price tiers from the rate card
                  price_information.PriceType(album_wholesale_price_tier, { "Namespace" => "DPID:PADPIDA2011101104E" })
                  # price_information.WholesalePricePerUnit(album_wholesale_price(), {"CurrencyCode" => 'USD'})
                end
                deal_terms.ValidityPeriod do |validity_period|
                  if takedown
                    validity_period.EndDate takedown_at
                  else
                    validity_period.StartDate actual_release_date
                  end
                end
                if preorder_data
                  deal_terms.PreOrderReleaseDate preorder_data[:preorder_start_date].strftime("%Y-%m-%d")
                  if preorder_data[:preview_songs]
                    deal_terms.PreOrderPreviewDate preorder_data[:preorder_start_date].strftime("%Y-%m-%d")
                  end
                end
              end
            end
          end

          # Track deals
          tracks.each do |track|
            deal_list.ReleaseDeal do |release_deal|
              release_deal.DealReleaseReference "R#{track.number}"
              release_deal.Deal do |deal|
                deal.DealReference "something"
                deal.DealTerms do |deal_terms|
                  deal_terms.CommercialModelType "AsPerContract"

                  if takedown
                    deal_terms.TakeDown "true"
                  else
                    deal_terms.Usage do |usage|
                      if track.album_only
                        usage.UseType("UserDefined", "UserDefinedValue" => "NoGoogleMusicBasic")
                        usage.UseType("UserDefined", "UserDefinedValue" => "NoGoogleMusicSubscription")
                      elsif !track.available_for_streaming
                        usage.UseType("UserDefined", "UserDefinedValue" => "GoogleMusicBasic")
                        usage.UseType("UserDefined", "UserDefinedValue" => "NoGoogleMusicSubscription")
                      else
                        usage.UseType("UserDefined", "UserDefinedValue" => "GoogleMusicBasic")
                        usage.UseType("UserDefined", "UserDefinedValue" => "GoogleMusicSubscription")
                      end
                    end
                  end
                  countries.each do |country_code|
                    deal_terms.TerritoryCode country_code
                  end
                  deal_terms.PriceInformation do |price_information|
                    price_information.PriceType(track_wholesale_price_tier(track), { "Namespace" => "DPID:PADPIDA2011101104E" })
                  end

                  deal_terms.ValidityPeriod do |validity_period|
                    if takedown
                      validity_period.EndDate takedown_at
                    else
                      validity_period.StartDate actual_release_date
                    end
                  end
                  if preorder_data
                    deal_terms.PreOrderReleaseDate preorder_data[:preorder_start_date].strftime("%Y-%m-%d")
                    if preorder_data[:preview_songs]
                      deal_terms.PreOrderPreviewDate preorder_data[:preorder_start_date].strftime("%Y-%m-%d")
                    end
                  end
                end
              end # deal
            end # release_Deal
          end # tracks.each
        end
      end # ern
    end

    def valid?
      [@upc, @title, @release_date, @label_name, @genres, @artwork_s3_key, @explicit_lyrics].each do |required_field|
        return false if required_field.nil?
      end

      true
    end

    def album_wholesale_price
      if @tracks.size < 10
        (@tracks.size * 0.70).round(2)
      else
        7.00
      end
    end

    def track_wholesale_price(_track)
      0.70
    end

    def price_tiers
      {
        "0.0" => "free",
        "0.49" => 1,
        "0.7" => 2,
        "0.91" => 3,
        "1.4" => 4,
        "1.75" => 5,
        "2.1" => 6,
        "2.8" => 7,
        "3.5" => 8,
        "4.2" => 9,
        "4.9" => 10,
        "5.6" => 11,
        "6.3" => 12,
        "7.0" => 13
      }
    end

    def album_wholesale_price_tier
      price_tier || price_tiers[album_wholesale_price.to_s] || raise("Unable to find a price code for value: #{album_wholesale_price}")
    end

    def track_wholesale_price_tier(track)
      track.price_tier || ("free" if track.free_song) || 2
    end

    def all_tracks_are_streamable?
      !!tracks.detect { |t| t.available_for_streaming == true }
    end

    def yt_oac_feature?
      FeatureFlipper.show_feature?(:yt_oac_self_serve, artist&.album&.person_id)
    end
  end
end
