module DistributionSystem::Google
  class Artist < DistributionSystem::Artist
    attr_accessor :role, :primary, :channel_id, :album, :name, :artist_id

    def initialize(options = {})
      # Defaults
      options = {
        role: "Performer",
        primary: true,
      }.merge(options)
      super
      @channel_id = artist_channel_id
      @artist_id = fetch_artist_id
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.PRIMARY_ARTIST(name)
    end

    def valid?
      return false if @name.to_s == ""

      true
    end

    def artist_channel_id
      channel = Creative.select("external_service_ids.identifier")
                        .person_by_artist_name(album&.person&.id, name)
                        .joins(:external_service_ids)
                        .where(external_service_ids: {
                                 service_name: ExternalServiceId::YOUTUBE_CHANNEL
                               }).first&.identifier
      return false unless channel
      return channel if channel_id_valid?
    rescue Youtube::ApiClient::AccountError => e
      Airbrake.notify("Youtube Account Error: #{e}")

      false
    end

    def fetch_artist_id
      ::Artist.where(name: name).last&.id
    end

    def tunecore_id
      "#{artist_id}_#{album&.person&.id}"
    end

    def channel_id_valid?
      ::YoutubeMusic::OfficialArtistChannelService.channel_id_active_status(album)
    end
  end
end
