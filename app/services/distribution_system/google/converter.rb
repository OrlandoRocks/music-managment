# frozen_string_literal: true

class DistributionSystem::Google::Converter < DistributionSystem::Converter
  include DistributionSystem::YoutubeDeliverable
  description "Google Music"
  handle_salepoints(/^Google$/)

  ALBUM_KLASS           = DistributionSystem::Google::Album
  TRACK_KLASS           = DistributionSystem::Google::Track
  ARTIST_KLASS          = DistributionSystem::Google::Artist

  EMPTY_FEATURED_ARTISTS = [].freeze

  GOOGLE_TAKEDOWN_DATE_FORMAT = "%Y-%m-%d"

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    @person = tunecore_album.person
    preorder_salepoint = tc_salepoints.detect { |salepoint| salepoint.preorder_data.present? }
    price_tier = tc_salepoints.first.variable_price.price_code if tc_salepoints && tc_salepoints.first.variable_price
    if tc_salepoints && tc_salepoints.first.track_variable_price
      track_price_tier = tc_salepoints.first.track_variable_price.price_code
    end
    album = DistributionSystem::Google::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: DistributionSystem::Google::GenreHelper.get_genres(tunecore_album.genres),
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artist: set_artist(tunecore_album),
        artwork_file: DistributionSystem::Google::CoverImage.new(asset: tunecore_album.artwork.s3_asset.key),
        delivery_type: delivery_type,
        takedown: set_takedown(tunecore_album, tc_salepoints, "Google"),
        countries: territory_codes(tunecore_album),
        price_tier: price_tier,
        preorder_data: preorder_salepoint ? preorder_salepoint.preorder_data : nil,
        actual_release_date: tunecore_album.sale_date,
        takedown_at: google_takedown_at(tunecore_album, tc_salepoints)
      }
    )

    tunecore_album.songs.each do |song|
      add_track(album, song, track_price_tier)
    end

    # validate the album
    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    ::TrackMonetization::YoutubeSrUpdateService.deliver_sr_metadata_update(tunecore_album) unless ytsr_cutover?
    album
  end

  #        add album tracks
  def add_track(album, song, track_price_tier)
    track = DistributionSystem::Google::Track.new(
      {
        album: album,
        number: song.track_num,
        title: song.name,
        artist: DistributionSystem::Google::Artist.new(name: song.artist_name),
        sku: song.sku,
        isrc: song.isrc,
        explicit_lyrics: song.parental_advisory?,
        orig_filename: song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{song.isrc}#{temp_filename_ext(song)}",
        s3_key: s3_flac_key(song),
        s3_bucket: s3_flac_bucket(song),
        album_only: song.album_only,
        free_song: song.free_song,
        available_for_streaming: use_attribute_if_available?(:available_for_streaming, song),
        asset_url: song.external_asset_url,
        price_tier: track_price_tier,
        duration: calculate_duration(song),
        featured_artist_names: featured_artist_names(song)
      }
    )

    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end

  # Combines all primary_artists into list (string), but with ampersand as the last separator
  # "One, two & three"
  def set_artist(tunecore_album)
    DistributionSystem::Google::Artist.new(name: tunecore_album.artist_name, album: tunecore_album)
  end

  def featured_artist_names(song)
    return EMPTY_FEATURED_ARTISTS unless FeatureFlipper.show_feature?(
      :google_featured_artists,
      @person
    )

    song.featured_artists.map(&:name)
  end

  def google_takedown_at(tunecore_album, tc_salepoints)
    salepoint = tc_salepoints.find { |s| s.store.short_name == "Google" }
    if tunecore_album.takedown?
      tunecore_album.takedown_at.strftime(GOOGLE_TAKEDOWN_DATE_FORMAT)
    elsif !salepoint.nil? && salepoint.taken_down?
      salepoint.takedown_at.strftime(GOOGLE_TAKEDOWN_DATE_FORMAT)
    else
      nil
    end
  end

  def ytsr_cutover?
    Store.find(Store::YTSR_STORE_ID).delivery_service == Store::TC_DISTRIBUTOR
  end
end
