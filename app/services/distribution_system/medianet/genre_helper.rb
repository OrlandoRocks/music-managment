module DistributionSystem::Medianet
  class GenreHelper
    def self.get_genres(genres)
      medianet_genres = []
      genres.map do |genre|
        medianet_genre = get_medianet_genre(genre.id)
        medianet_genres.push(medianet_genre)
      end
      medianet_genres
    end

    def self.get_medianet_genre(genre_id)
      case genre_id
      when 1
        "Alternative/Indie"
      when 3
        "Blues"
      when 4
        "Childrens"
      when 5
        "Classical/Opera"
      when 6
        "Comedy/Spoken Word"
      when 7
        "Electronica/Dance"
      when 8
        "Electronica/Dance"
      when 9
        "Folk"
      when 10
        "Pop"
      when 11
        "World"
      when 12
        "Pop"
      when 13
        "Rap/Hip Hop"
      when 14
        "Seasonal"
      when 15
        "Christian/Gospel"
      when 16
        "Jazz"
      when 17
        "Latin"
      when 18
        "New Age"
      when 19
        "Classical/Opera"
      when 20
        "Pop"
      when 21
        "Soul/R&B"
      when 22
        "Reggae/Ska"
      when 23
        "Rock"
      when 24
        "Soundtracks"
      when 26
        "Vocals"
      when 27
        "World"
      when 28
        "Folk"
      when 29
        "Country"
      when 30
        "Comedy/Spoken Word"
      when 31
        "Rock"
      when 32
        "Pop"
      when 33
        "Pop"
      when 34
        "Folk"
      when 35
        "Jazz"
      when 36
        "Electronica/Dance"
      when 37
        "Classical/Opera"
      when 38
        "Instrumental"
      when 39
        "Instrumental"
      when (40..100)
        "World"
      when 101
        "Ambient"
      when 102
        "Brazilian"
      when 103
        "Baladas"
      when 104
        "Boleros"
      when 105
        "Caribbean"
      when 106
        "Cuban"
      when 107
        "Latin Hip-Hop"
      when 108
        "Latin Trap"
      when 109
        "Latin Urban"
      when 110
        "Ranchera"
      when 111
        "Regional Mexicano"
      when 112
        "Salsa/Merengue"
      when 113
        "Tango"
      when 114
        "Tropical"
      when 115
        "Axé"
      when 116
        "Baile Funk"
      when 117
        "Bossa Nova"
      when 118
        "Chorinho"
      when 119
        "Forró"
      when 120
        "Frevo"
      when 121
        "MPB"
      when 122
        "Pagode"
      when 123
        "Samba"
      when 124
        "Sertanejo"
      when 125
        "African"
      when 126
        "Afrobeats"
      when 127
        "Afropop"
      when 128
        "Afro-fusion"
      when 129
        "Afro-soul"
      when 130
        "Afro house"
      when 131
        "Amapiano"
      when 132
        "Bongo Flava"
      when 133
        "Highlife"
      when 134
        "Maskandi"
      else
        raise "Do not know how to map the tunecore genre with id #{genre_id}"
      end
    end
  end
end
