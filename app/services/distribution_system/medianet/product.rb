module DistributionSystem::Medianet
  class Product
    def initialize(options = {})
      options = {}.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    # Metadata elements
    attr_accessor :effective_from_date, :price_category

    def to_xml(xml = Builder::XmlMarkup.new, album = nil)
      price_data(album).each do |data|
        xml.PRICE do |price|
          price.TYPE "LABEL_COST"
          price.TERRITORY_CODE  data[:territory_code]
          price.CURRENCY_CODE   data[:currency_code]
          price.PRICE_CATEGORY  price_category # either SINGLE, ALBUM, or TRACK which is preset in the converter
          price.EFFECTIVE_FROM_DATE effective_from_date
        end
      end
      xml
    end

    # 2013-01-08 AK -- Add territory info here.
    def price_data(album)
      supported_territories = [
        { territory_code: "CA", currency_code: "CAD", price_category: "midline", cost: "7.02" },
        { territory_code: "FR", currency_code: "EUR", price_category: "midline", cost: "6.30" },
        { territory_code: "DE", currency_code: "EUR", price_category: "midline", cost: "6.30" },
        { territory_code: "GB", currency_code: "GBP", price_category: "midline", cost: "4.55" },
        { territory_code: "US", currency_code: "USD", price_category: "midline", cost: "7.00" }
      ]

      supported_territories.select { |t| album.countries.include?(t[:territory_code]) }
    end

    def valid?
      # Required fields
      return false if effective_from_date.nil?

      # Check Values
      return false if effective_from_date && !sales_start_date.respond_to?(:strftime)

      true
    end
  end
end
