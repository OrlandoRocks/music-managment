module DistributionSystem::Medianet
  class Artist < DistributionSystem::Artist
    attr_accessor :role

    def initialize(options = {})
      { role: "ARTIST" }.merge(options)
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new)
      xml.ARTIST do |artist|
        artist.TYPE(role)
        artist.VALUE(name)
      end
    end

    def valid?
      return false if @name.to_s == ""

      true
    end
  end
end
