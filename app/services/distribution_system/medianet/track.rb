module DistributionSystem::Medianet
  class Track < DistributionSystem::Track
    include DistributionSystem::TrackLength

    attr_accessor :lyrics, :liner_notes, :song_id, :products, :artists

    def initialize(options = {})
      options.each do |k, v|
        send("#{k}=", v)
      end
    end

    def to_xml(xml = Builder::XmlMarkup.new, album = nil)
      xml.MUSICNET_COMPONENT do |component|
        component.MUSICNET_COMPONENT_CODE "#{@album.upc}_01_#{number}"
        component.COMPONENT_TYPE "TRACK"
        component.CORP_CODE @album.corp_code
        component.LABEL_CODE @album.label_name

        album.supported_countries.each do |country_code|
          component.STREAM_RIGHT do |stream|
            stream.VALUE album_only ? "FALSE" : "TRUE"
            stream.TERRITORY_CODE country_code
          end

          component.DOWNLOAD_RIGHT do |download|
            download.VALUE album_only ? "FALSE" : "TRUE"
            download.TERRITORY_CODE country_code
          end

          if album.stores.include?("Medianet")
            component.NON_DRM_STREAM do |stream|
              stream.VALUE album_only ? "FALSE" : "TRUE"
              stream.TERRITORY_CODE country_code
            end
          end

          if album.stores.include?("Cur")
            component.NON_DRM_STREAM do |stream|
              stream.VALUE album_only ? "FALSE" : "TRUE"
              stream.TERRITORY_CODE country_code
              stream.RETAILER_CODE "CURUS"
            end
          end

          if album.stores.include?("Medianet")
            component.CD_BURN_RIGHT do |burn_right|
              burn_right.VALUE album_only ? "FALSE" : "TRUE"
              burn_right.TERRITORY_CODE country_code
            end
          end

          if album.stores.include?("Cur")
            component.CD_BURN_RIGHT do |burn_right|
              burn_right.VALUE album_only ? "FALSE" : "TRUE"
              burn_right.TERRITORY_CODE country_code
              burn_right.RETAILER_CODE "CURUS"
            end
          end

          if album.stores.include?("Medianet")
            component.NON_DRM_PURCHASE do |drm_purchase|
              drm_purchase.VALUE album_only ? "FALSE" : "TRUE"
              drm_purchase.TERRITORY_CODE country_code
            end
          end

          if album.stores.include?("Cur")
            component.NON_DRM_PURCHASE do |drm_purchase|
              drm_purchase.VALUE album_only ? "FALSE" : "TRUE"
              drm_purchase.TERRITORY_CODE country_code
              drm_purchase.RETAILER_CODE "CURUS"
            end
          end

          component.DIGITAL_AVAILABILITY do |availability|
            availability.BEGIN_DATE @album.begin_date
            availability.TERRITORY_CODE country_code
          end

          component.LABEL_APPR_IND do |md|
            md.VALUE (@album.takedown["Medianet"] && @album.takedown["Cur"]) ? "FALSE" : "TRUE"
            md.TERRITORY_CODE country_code
          end

          component.PUBLISHER_OVERRIDE_CODE do |md|
            md.VALUE (@album.takedown["Medianet"] && @album.takedown["Cur"]) ? "REMOVE" : "FORCE"
            md.TERRITORY_CODE country_code
          end
        end # supported_countries

        component.METADATA do |md|
          md.TYPE "ISRC"
          md.VALUE isrc
        end

        component.METADATA do |md|
          md.TYPE "DISC_NUMBER"
          md.VALUE "01"
        end

        component.METADATA do |md|
          md.TYPE "DURATION"
          md.VALUE get_duration("mm:ss")
        end

        component.METADATA do |md|
          md.TYPE "GENRE"
          md.VALUE @album.genres.first
        end

        if @album.genres.size > 1
          component.METADATA do |md|
            md.TYPE "ALTERNATE_GENRE"
            md.VALUE @album.genres[1]
          end
        end

        component.METADATA do |md|
          md.TYPE "PARENTAL_ADVISORY"
          if @album.explicit_lyrics
            md.VALUE "TRUE"
          else
            md.VALUE "FALSE"
          end
        end

        component.METADATA do |md|
          md.TYPE "PROVIDER_COMP_ID"
          md.VALUE song_id
        end

        component.METADATA do |md|
          md.TYPE "RELEASE_DATE"
          md.VALUE @album.actual_release_date
        end

        if artists
          artists.each { |a| a.to_xml(component) } # required
        end

        component.METADATA do |md|
          md.TYPE "TITLE"
          md.VALUE title
        end

        component.METADATA do |md|
          md.TYPE "TRACK_NUMBER"
          md.VALUE number
        end

        if @album.copyright_pline
          component.METADATA do |md|
            md.TYPE "PLINE"
            md.VALUE @album.copyright_pline
          end
        end

        if @album.copyright_cline
          component.METADATA do |md|
            md.TYPE "CLINE"
            md.VALUE @album.copyright_cline
          end
        end

        products.each { |p| p.to_xml(component, @album) }
      end
    end

    def valid?
      [
        number,
        @title,
        @album.parental_advisory,
        @album.products,
        @album.genres,
        @album.release_date
      ].each do |required_field|
        return false if (required_field.nil? || require_field == "")
      end
      true
    end
  end
end
