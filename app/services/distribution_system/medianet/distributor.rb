module DistributionSystem::Medianet
  class Distributor < DistributionSystem::Distributor
    def do_send_bundle
      bundle_id    = bundle.album.upc
      remote_dir   = "#{@delivery_config['remote_dir']}/#{bundle_id}"
      resource_dir = remote_dir

      @remote_server.mkdir_p remote_dir
      upload_assets(bundle_id, resource_dir, "flac") if full_delivery?
      upload_metadata(bundle_id, remote_dir)
      upload_batch_complete(remote_dir)
      @remote_server.start_processing
    end

    def upload_batch_complete(remote_dir)
      Rails.logger.info "uploading delivery.complete file #{@bundle.album}"
      `touch /tmp/delivery.complete`
      Rails.logger.info "from /tmp/delivery.complete #{@bundle.album}"
      remote_complete_filename = File.join(remote_dir, "delivery.complete")
      Rails.logger.info "to #{remote_complete_filename} #{@bundle.album}"
      @remote_server.upload("/tmp/delivery.complete", remote_complete_filename)
    end

    def upload_tracks(bundle_id, resource_dir, file_extension, remote_server_conn)
      @bundle.album.tracks.each do |track|
        Rails.logger.info "  uploading track #{track.number}. | album_id=#{@bundle.album.tunecore_id}  #{@bundle.album}"
        track_file = File.join(@bundle.dir.path, "#{bundle_id}_01_#{track.number}.#{file_extension}")
        remote_track_file = File.join(resource_dir, "#{bundle_id}_01_#{track.number}_FLAC.#{file_extension}")
        remote_server_conn.upload(track_file, remote_track_file)
      end
    end
  end
end
