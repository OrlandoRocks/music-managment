class DistributionSystem::Medianet::Album < DistributionSystem::Album
  attr_accessor :corp_code,
                :products,
                :begin_date,
                :takedown_at,
                :supported_countries,
                :stores

  def initialize(options = {})
    super
    @supported_countries = [
      "BE",
      "BR",
      "CN",
      "DE",
      "ES",
      "FR",
      "IE",
      "IN",
      "JP",
      "KR",
      "MX",
      "MY",
      "SG",
      "TW",
      "US",
      "AU",
      "CA",
      "GB",
      "ZA",
      "IT",
      "NL",
      "NZ",
      "RO"
    ]
  end

  def to_xml(xml = Builder::XmlMarkup.new(indent: 2))
    xml.DATA("xmlns:fo" => "http://www.w3.org/1999/XSL/Format") do |data|
      data.MUSICNET_COMPONENT do |component|
        component.MUSICNET_COMPONENT_CODE upc
        component.COMPONENT_TYPE "ALBUM"
        component.CORP_CODE @corp_code
        component.LABEL_CODE @label_name

        @supported_countries.each do |country_code|
          if stores.include?("Medianet")
            component.CD_BURN_RIGHT do |burn_right|
              burn_right.VALUE (takedown["Medianet"] || !countries.include?(country_code)) ? "FALSE" : "TRUE"
              burn_right.TERRITORY_CODE country_code
            end
          end

          if stores.include?("Cur")
            component.CD_BURN_RIGHT do |burn_right|
              burn_right.VALUE (takedown["Cur"] || !countries.include?(country_code)) ? "FALSE" : "TRUE"
              burn_right.TERRITORY_CODE country_code
              burn_right.RETAILER_CODE "CURUS"
            end
          end

          if stores.include?("Medianet")
            component.NON_DRM_PURCHASE do |drm_purchase|
              drm_purchase.VALUE (takedown["Medianet"] || !countries.include?(country_code)) ? "FALSE" : "TRUE"
              drm_purchase.TERRITORY_CODE country_code
            end
          end

          if stores.include?("Cur")
            component.NON_DRM_PURCHASE do |drm_purchase|
              drm_purchase.VALUE (takedown["Cur"] || !countries.include?(country_code)) ? "FALSE" : "TRUE"
              drm_purchase.TERRITORY_CODE country_code
              drm_purchase.RETAILER_CODE "CURUS"
            end
          end

          component.DIGITAL_AVAILABILITY do |availability|
            availability.BEGIN_DATE begin_date
            availability.TERRITORY_CODE country_code
          end
        end

        if takedown["Medianet"] && stores.include?("Medianet")
          component.CD_BURN_RIGHT do |burn_right|
            burn_right.VALUE "FALSE"
            burn_right.TERRITORY_CODE "WW-MN"
          end

          component.NON_DRM_PURCHASE do |drm_purchase|
            drm_purchase.VALUE "FALSE"
            drm_purchase.TERRITORY_CODE "WW-MN"
          end
        end

        if takedown["Cur"] && stores.include?("Cur")
          component.CD_BURN_RIGHT do |burn_right|
            burn_right.VALUE "FALSE"
            burn_right.TERRITORY_CODE "WW-MN"
            burn_right.RETAILER_CODE "CURUS"
          end
          component.NON_DRM_PURCHASE do |drm_purchase|
            drm_purchase.VALUE "FALSE"
            drm_purchase.TERRITORY_CODE "WW-MN"
            drm_purchase.RETAILER_CODE "CURUS"
          end
        end

        if takedown["Cur"] || takedown["Medianet"]
          component.DIGITAL_AVAILABILITY do |availability|
            availability.BEGIN_DATE begin_date
            availability.TERRITORY_CODE "WW-MN"
          end
        end

        component.METADATA do |md|
          md.TYPE "RELATED_UPC"
          md.VALUE upc
        end

        if upc.size == 13
          component.METADATA do |md|
            md.TYPE "EAN"
            md.VALUE upc
          end
        end

        component.METADATA do |md|
          md.TYPE "PROVIDER_COMP_ID"
          md.VALUE tunecore_id
        end

        component.METADATA do |md|
          md.TYPE "IMAGE_TYPE"
          md.VALUE "FRONT_COVER"
        end

        if artists
          artists.each { |a| a.to_xml(component) } # required
        end

        component.METADATA do |md|
          md.TYPE "TITLE"
          md.VALUE title
        end

        component.METADATA do |md|
          md.TYPE "RELEASE_DATE"
          md.VALUE actual_release_date
        end

        component.METADATA do |md|
          md.TYPE "GENRE"
          md.VALUE genres.first
        end

        if genres.size > 1
          component.METADATA do |md|
            md.TYPE "ALTERNATE_GENRE"
            md.VALUE genres[1]
          end
        end

        component.METADATA do |md|
          md.TYPE "DURATION"
          md.VALUE get_duration("mm:ss")
        end

        component.METADATA do |md|
          md.TYPE "PARENTAL_ADVISORY"
          if explicit_lyrics
            md.VALUE "TRUE"
          else
            md.VALUE "FALSE"
          end
        end

        component.METADATA do |md|
          md.TYPE "TRACKS"
          if tracks.size < 10
            md.VALUE "0#{tracks.size}"
          else
            md.VALUE tracks.size
          end
        end

        if copyright_pline
          component.METADATA do |md|
            md.TYPE "PLINE"
            md.VALUE copyright_pline
          end
        end

        if copyright_cline
          component.METADATA do |md|
            md.TYPE "CLINE"
            md.VALUE copyright_cline
          end
        end

        products.each { |p| p.to_xml(component, self) }
        tracks.each { |t| t.to_xml(component, self) }
      end
    end
  end

  def valid?
    [@upc, @label_name, @artists, release_date, @title, @genres, @explicit_lyrics, @products].each do |required_field|
      return false if (required_field.nil? || required_field == "")
    end
    true
  end
end
