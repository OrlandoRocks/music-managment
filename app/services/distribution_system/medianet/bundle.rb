module DistributionSystem::Medianet
  class Bundle < DistributionSystem::Bundle
    def write_metadata
      File.open(File.join(@dir.path, @album.upc.to_s + ".xml"), "w") do |f|
        xml = Builder::XmlMarkup.new(target: f, indent: 2)
        xml.instruct!(:xml, version: "1.0", encoding: "utf-8")
        @album.corp_code = "TUN"
        @album.to_xml(xml)
      end
    end

    def collect_files
      # Requirements for image
      image_source = File.join(@dir.path, "#{@album.upc}.jpg")
      @s3.get(image_source, @album.artwork_file.asset)
      DistributionSystem::ImageUtils.resize(image_source, 1400, 1400)
      @album.artwork_file.asset = image_source
      # get the album tracks
      @album.tracks.each do |track|
        orig = original_file(track)
        @s3.bucket = track.s3_bucket if track.s3_bucket != nil
        @s3.get(orig, track.s3_key)
        track.audio_file = track_filename(@dir.path, track)
        transcode_track(track)
        FileUtils.rm(orig)
        wav_files = File.join(@dir.path, "*.wav")
        FileUtils.rm_f Dir.glob(wav_files)
      end
    end

    def track_filename(path, track)
      File.join(path, "#{track.album.upc}_01_#{track.number}.flac")
    end
  end
end
