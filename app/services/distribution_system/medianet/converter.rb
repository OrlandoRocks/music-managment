class DistributionSystem::Medianet::Converter < DistributionSystem::Converter
  description "MediaNet Music"
  handle_salepoints(/^Medianet$|^Cur$/)

  ALBUM_KLASS           = DistributionSystem::Medianet::Album
  TRACK_KLASS           = DistributionSystem::Medianet::Track
  ARTIST_KLASS          = DistributionSystem::Medianet::Artist

  def initialize
    @supports_metadata_update = true
    @supports_takedown        = true
    @supports_album_only      = true
  end

  def convert(tunecore_album, tc_salepoints, delivery_type = "full_delivery")
    album = DistributionSystem::Medianet::Album.new(
      {
        tunecore_id: tunecore_album.id,
        copyright_name: tunecore_album.copyright_name,
        upc: tunecore_album.upc.to_s,
        sku: tunecore_album.sku,
        title: tunecore_album.name,
        copyright_pline: pline(tunecore_album),
        copyright_cline: cline(tunecore_album),
        release_date: tunecore_album.orig_release_date.strftime("%Y%m%d"),
        begin_date: tunecore_album.orig_release_date.strftime("%Y-%m-%d"),
        genres: DistributionSystem::Medianet::GenreHelper.get_genres(tunecore_album.genres),
        store_id: tc_salepoints.pluck(:store_id).first,
        store_name: tc_salepoints.map(&:store).first.short_name,
        stores: tc_salepoints.map { |salepoint| salepoint.store.short_name },
        artwork_s3_key: tunecore_album.artwork.s3_asset.key,
        tracks: [],
        label_name: set_label(tunecore_album),
        explicit_lyrics: tunecore_album.parental_advisory,
        liner_notes: tunecore_album.liner_notes,
        artists: set_artist(tunecore_album, { role: "ARTIST" }),
        products: set_products(tunecore_album, { price_category: determine_album_category(tunecore_album) }),
        artwork_file: DistributionSystem::Medianet::CoverImage.new(asset: tunecore_album.artwork.s3_asset.key),
        delivery_type: delivery_type,
        takedown: { "Cur" => set_takedown(tunecore_album, tc_salepoints, "Cur"), "Medianet" => set_takedown(tunecore_album, tc_salepoints, "Medianet") },
        takedown_at: set_takedown_at(tunecore_album, tc_salepoints, "Medianet"),
        countries: countries(tunecore_album),
        actual_release_date: tunecore_album.sale_date.strftime("%Y%m%d")
      }
    )

    tunecore_album.songs.each do |tunecore_song|
      add_track(album, tunecore_album, tunecore_song, tc_salepoints)
    end

    raise "Album validation failed. One or more required attributes are missing." unless album.valid?

    album
  end

  def add_track(album, tc_album, tc_song, _tc_salepoints)
    track = DistributionSystem::Medianet::Track.new(
      {
        song_id: tc_song.id,
        album: album,
        number: tc_song.track_num.to_s.rjust(2, "0"),
        title: tc_song.name,
        artists: tc_album.is_various? ? set_artist(tc_song, { role: "PERFORMER" }) : set_artist(tc_album, { role: "PERFORMER" }),
        sku: tc_song.sku,
        isrc: tc_song.isrc,
        explicit_lyrics: tc_song.parental_advisory?,
        orig_filename: tc_song.upload.uploaded_filename,
        temp_filename: "#{album.upc}_#{tc_song.isrc}#{File.extname(tc_song.upload.uploaded_filename)}",
        s3_key: song_s3key(tc_song),
        s3_bucket: song_s3bucket(tc_song),
        album_only: tc_song.album_only,
        free_song: tc_song.free_song,
        asset_url: tc_song.external_asset_url,
        products: set_products(tc_album, { price_category: "TRACK" }),
        duration: tc_song.duration_in_seconds || calculate_duration(tc_song)
      }
    )
    album.tracks << track
  end

  def set_label(tunecore_album)
    if tunecore_album.label
      tunecore_album.label.name
    else
      tunecore_album.artist_name
    end
  end

  def set_products(album, options = {})
    products = []

    a_product = DistributionSystem::Medianet::Product.new(options)
    a_product.effective_from_date = album.sale_date
    products << a_product

    products
  end

  def determine_album_category(tunecore_album)
    if tunecore_album.songs.count > 9
      "MIDLINE"
    else
      total_track_number = tunecore_album.songs.count
      "SNG0#{total_track_number}"
    end
  end

  def set_artist(tc_obj, options = {})
    case tc_obj.class.name
    when "Album", "Single", "Ringtone"
      if tc_obj.is_various?
        # returns Array of "Various Artists"
        [DistributionSystem::Medianet::Artist.new(name: tc_obj.artist_name, role: options[:role])]
      else
        # returns Array of artist names
        tc_obj.creatives.collect { |c| DistributionSystem::Medianet::Artist.new(name: c.artist.name, role: options[:role]) }
      end
    when "Song"
      tc_obj.creatives.collect { |c| DistributionSystem::Medianet::Artist.new(name: c.artist.name, role: options[:role]) }
    else
      raise "parameter class #{tc_obj.class} is unacceptable- must be an instance of an album, single, ringtone or song"
    end
  end
end
