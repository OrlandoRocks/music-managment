class Api::Social::AuthenticationRequest
  attr_reader :user, :plan_status, :auth_params, :token

  delegate :plan, :plan_expires_at, :grace_period_ends_on, to: :plan_status

  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport

  def initialize(params)
    @auth_params = params
    authenticate_person
    load_plan_status if @user.present?
  end

  def granted?
    if user && user.is_verified?
      @token = set_token
      true
    else
      false
    end
  end

  private

  def authenticate_person
    @user = Person.authenticate(auth_params)
  end

  def set_token
    generate_token unless user.tc_social_token
    user.tc_social_token
  end

  def generate_token
    client_application = ClientApplication.where(name: "tc_social").first
    Oauth2Token.create!(user: user, client_application: client_application, scope: nil)
  end

  def load_plan_status
    @plan_status = ::Social::PlanStatus.for(user)
  end
end
