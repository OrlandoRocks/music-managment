# frozen_string_literal: true

class Api::PeopleFlags::SearchService
  attr_reader :person_ids, :flag_name, :limit, :offset, :query

  def initialize(person_ids:, flag_name:, limit:, offset:)
    @person_ids = person_ids.split(/\s|[,]/)
    @flag_name = flag_name
    @limit = limit
    @offset = offset
    @query = suspicious_people_query
  end

  def search
    query
  end

  private

  def get_people_id_and_email_from_params
    emails, ids = person_ids.reject(&:blank?).partition { |person_id| person_id.include?("@") }
    [emails, ids]
  end

  def filter_people_id
    ids = get_people_id_and_email_from_params[1].map { |id| Integer(id) rescue 0 }
    "AND people.id IN (#{ids.join(',')})" if ids.present?
  end

  def filter_people_email
    emails, ids = get_people_id_and_email_from_params
    emails = emails.map { |s| "'#{s}'" }.join(",")
    return "AND people.email IN (#{emails})" if emails.present? && ids.blank?

    "OR people.email IN (#{emails})" if emails.present? && ids.present?
  end

  def suspicious_people_query
    # STU-677 - Quick fix
    sql = <<-SQL
      SELECT people.id, people.name, people.email, people.country, people.lock_reason, people.status_updated_at
      FROM people
      LEFT OUTER JOIN people_flags pf ON people.id = pf.person_id
      LEFT OUTER JOIN flags f1 ON pf.flag_id = f1.id AND (f1.name = "#{flag_name}" OR f1.name = "#{Flag::BLOCKED_FROM_DISTRIBUTION[:name]}")
      WHERE people.status = "#{flag_name}" #{filter_people_id} #{filter_people_email}
      GROUP BY people.id
      LIMIT #{limit}
      OFFSET #{offset}
    SQL
    Person.find_by_sql(sql)
  end
end
