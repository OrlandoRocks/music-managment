# frozen_string_literal: true

class Api::SongsService
  attr_accessor :sort_param, :songs

  def initialize(sort_param = nil, songs = nil)
    @sort_param = sort_param
    @songs = songs
  end

  def song_sort
    case sort_param
    when "old"
      "albums.sale_date asc"
    when "new"
      "albums.sale_date desc"
    when "asc"
      "songs.name asc"
    when "desc"
      "songs.name desc"
    end
  end

  def create_spotify_esi_songs
    song_ids =
      songs.flat_map do |song|
        resp = Spotify::ApiClient.new.find_by_isrc(song.isrc)
        next if resp.blank?

        identifier = resp.split(":").last
        song.external_service_ids
            .create(service_name: ExternalServiceId::SPOTIFY_SERVICE,
                    identifier: identifier)
        song.id
      end
    song_ids.compact
  end

  def create_apple_esi_songs
    songs.each do |song|
      next if song.external_service_ids
                  .where(service_name: ExternalServiceId::APPLE_SERVICE).present?

      album_upc = song.album.upc.number
      next if album_upc.nil?

      song_identifier = Itunes::ApiClient.new(album_upc, song.name).fetch_song_id
      next if song_identifier.nil?

      song.external_service_ids
          .create(service_name: ExternalServiceId::APPLE_SERVICE,
                  identifier: song_identifier)
    end
  end
end
