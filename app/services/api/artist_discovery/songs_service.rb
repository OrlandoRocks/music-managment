class Api::ArtistDiscovery::SongsService
  attr_accessor :params, :albums

  def initialize(params, albums)
    @params = params
    @albums = albums
  end

  def retrieve_songs
    create_spotify_esi_songs!

    if randomize?
      spotify_songs_with_artwork.where(id: random_song_ids).order("FIELD(songs.id, #{random_song_ids.join(',')})") +
        spotify_songs_with_artwork.where.not(id: random_song_ids).order(song_order)
    elsif song_ids?
      spotify_songs_with_artwork.where(id: song_ids).order("FIELD(songs.id, #{song_ids.join(',')})") +
        spotify_songs_with_artwork.where.not(id: song_ids).order(song_order)
    else
      spotify_songs_with_artwork.order(song_order)
    end
  end

  private

  def song_ids
    return if params[:song_ids].blank?

    params[:song_ids]
  end

  def song_ids?
    params[:song_ids].present?
  end

  def randomize?
    params[:randomize_releases]
  end

  def random_song_ids
    @random_song_ids ||= Song.spotify_songs_with_artwork(albums: albums, search_key: search_key).ids.sample(3)
  end

  def null_esi_songs
    @null_esi_songs ||= Song.null_esi_songs(albums: albums)
  end

  def null_esi_songs?
    null_esi_songs.present?
  end

  def create_spotify_esi_songs!
    return unless null_esi_songs?

    Api::SongsService.new(nil, null_esi_songs).create_spotify_esi_songs
  end

  def search_key
    params[:search].present? ? "%#{params[:search]}%" : "%"
  end

  def song_order
    @song_order ||= Api::SongsService.new(params[:sort], nil).song_sort
  end

  def spotify_songs_with_artwork
    @spotify_songs_with_artwork ||= Song.spotify_songs_with_artwork(albums: albums, search_key: search_key)
  end
end
