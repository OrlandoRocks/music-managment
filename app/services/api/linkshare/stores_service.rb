class Api::Linkshare::StoresService
  attr_reader :album, :stores

  APPLE = "apple".freeze
  AMAZON = Api::Linkshare::FetchStoresService::STORES_NAMES[0]
  STORE_TYPE = ["buy", "play"].freeze
  ITUNES = Api::Linkshare::FetchStoresService::STORES_NAMES[1]
  SPOTIFY_PUBREF_MAX_LENGTH = 255

  def initialize(album, stores, song)
    @stores = stores
    @album = album
    @song = song
    @apple_album = @album.find_by(service_name: APPLE)&.identifier if @album.present?
    @amazon_album = @album.find_by(service_name: AMAZON)&.identifier if @album.present?
  end

  def fetch_urls
    urls = []
    @stores.each do |store|
      @type = ([AMAZON, ITUNES].include? store.service_name) ? STORE_TYPE[0] : STORE_TYPE[1]
      case store.service_name
      when "spotify"
        pubref = spotify_pubref(@song)
        url = encode_spotify_url(store, pubref)
        service_name = "spotify"
      when "apple", "Apple"
        next if @apple_album.nil?

        url = "#{ENV['APPLE_STORE_BASE_URL']}/album/#{@apple_album}?i=#{store.identifier}"
      when "amazon"
        next if @amazon_album.nil?

        url = "#{ENV['AMAZONE_MUSIC_BASE_URL']}/albums/#{@amazon_album}?trackAsin=#{store.identifier}&do=play"
        urls << uri_params(url, store, "amazon_music")
        url = "#{ENV['AMAZONE_STORE_BASE_URL']}/#{store.identifier}"
      when "google"
        url = "#{ENV['GOOGLE_STORE_BASE_URL']}/#{store.identifier}"
      when "youtube"
        url = "#{ENV['YOUTUBE_STORE_BASE_URL']}#{store.identifier}"
      when "deezer"
        url = "#{ENV['DEEZER_STORE_BASE_URL']}/track/#{store.identifier}"
      when "tidal"
        url = "#{ENV['TIDAL_STORE_BASE_URL']}/track/#{store.identifier}"
      when "napster"
        url = "#{ENV['NAPSTER_STORE_BASE_URL']}/track/#{store.identifier}"
      when "pandora"
        url = "#{ENV['PANDORA_STORE_BASE_URL']}/track/#{store.identifier}"
      when "itunes"
        next if @apple_album.nil?

        url = "#{ENV['APPLE_STORE_BASE_URL']}/album/#{@apple_album}?i=#{store.identifier}"
      end
      urls << uri_params(url, store, store.service_name.downcase)
    end
    preserve_order(urls)
  end

  def encode_spotify_url(store, pubref)
    track_url = "#{ENV['SPOTIFY_STORE_BASE_URL']}/track/#{store.identifier}"
    url = "#{ERB::Util.url_encode(pubref)}/spotifyurl:#{track_url}/destination:#{track_url}"
    "#{ENV['SPOTIFY_AFFILIATE_LINK']}/pubref:#{url}"
  end

  def spotify_pubref(song)
    artist_name = song.artist.name
    user_id = song.album.person_id.to_s
    song_name = song.name

    # condition - artist name and user id > 255 to truncat, omit song name
    artistid_and_userid = "#{artist_name}-#{user_id}"
    if artistid_and_userid.length > SPOTIFY_PUBREF_MAX_LENGTH
      diff = artistid_and_userid.length - SPOTIFY_PUBREF_MAX_LENGTH
      return "#{artist_name[0, (artist_name.length - diff)]}-#{user_id}"
    end

    # condition - for Eliminate symbol check with artist name and user id between 255, 254 - omit song name
    return artistid_and_userid if artist_name_user_id_max_allowed?(artistid_and_userid)

    artistid_and_userid_and_songname = "#{artist_name}-#{user_id}-#{song_name}"

    # condition - artistid, userid, songname equal to 255
    return artistid_and_userid_and_songname if artistid_and_userid_and_songname.length == SPOTIFY_PUBREF_MAX_LENGTH

    # condition - artistid, userid, songname where exceeded 255 truncating song name
    diff = artistid_and_userid_and_songname.length - SPOTIFY_PUBREF_MAX_LENGTH
    song_name = song_name[0, (song_name.length - diff)]
    "#{artist_name}-#{user_id}-#{song_name}"
  end

  def artist_name_user_id_max_allowed?(str)
    # Eliminate adding the last '-' of song_name (i.e. "-#{song_name}")
    without_last_dash = SPOTIFY_PUBREF_MAX_LENGTH - 1
    [SPOTIFY_PUBREF_MAX_LENGTH, without_last_dash].include?(str.length)
  end

  def uri_params(url, store, service_name)
    {
      uri: url,
      service_name: service_name,
      type: service_name == "amazon_music" ? "play" : @type,
      id: store.id
    }
  end

  def preserve_order(uris)
    order = Utilities::StringToHashConverter.convert_to_hash(ENV["API_STORE_ORDER"])
    uris.sort { |a, b| order[a[:service_name]].to_i <=> order[b[:service_name]].to_i }
  end
end
