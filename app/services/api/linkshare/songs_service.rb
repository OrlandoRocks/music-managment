class Api::Linkshare::SongsService
  attr_accessor :params, :albums

  def initialize(params, albums)
    @params = params
    @albums = albums
  end

  def retrieve_songs
    null_esi_songs = Song.null_esi_songs(albums: albums)
    Api::SongsService.new(nil, null_esi_songs).create_spotify_esi_songs if null_esi_songs.present?

    apple_albums = albums.joins(:external_service_ids)
                         .where(external_service_ids: { service_name: ExternalServiceId::APPLE_SERVICE })
    apple_album_songs = Song.where(album: apple_albums)
    Api::SongsService.new(nil, apple_album_songs).create_apple_esi_songs if apple_album_songs.present?

    song_ids = Song.where(album: albums).joins(:external_service_ids).distinct.pluck(:id)
    song_order = Api::SongsService.new(params[:sort], nil).song_sort
    search_key = params[:search].present? ? "%#{params[:search]}%" : "%"
    Song.select("distinct(songs.name), songs.id, songs.album_id")
        .includes(album: :artwork).where(id: song_ids)
        .where("songs.name LIKE ?", search_key)
        .order(song_order)
  end
end
