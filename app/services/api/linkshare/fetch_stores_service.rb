class Api::Linkshare::FetchStoresService
  include FetchStoresHelper

  RELEASE_CHECK_DAYS = ENV["RELEASE_CHECK_DAYS"].to_i
  DISTRIBUTION_CHECK_DAYS = ENV["DISTRIBUTION_CHECK_DAYS"].to_i
  API_LIMIT_STATUS_CODE = 429
  STORES_NAMES = [
    "amazon",
    "itunes",
    "pandora",
    "youtube",
    "deezer",
    "tidal",
    "napster"
  ].map(&:freeze).freeze

  # TC Distributor gem returns store_id in its response and not store names.
  # These ids are consistent and do not change across the different environments
  # The Stores are seeded using stores.csv
  # For Linkshare we are interested only in the below store ids.
  STORE_IDS = [7, 13, 26, 27, 28, 32, 36, 39, 45, 72, 78].freeze
  APPLE = "apple".freeze
  SPOTIFY = "spotify".freeze
  NAPSTER = "napster".freeze

  def initialize(song, source, scrape_count = nil)
    @song = song
    @source = source
    @scrape_count = scrape_count.presence || 0
    @song_esi = song.external_service_ids
    @album_esi = song.album.external_service_ids
  end

  def fetch_odesli_url
    return if urls_already_fetched?

    scrape_and_create_album_esi if album_esi_update
    if @song_esi.present?
      count = @song_esi.count { |esi| STORES_NAMES.include?(esi.service_name) }
      scrape_and_create_esi if count.zero? && @song_esi.map { |esi| [APPLE, SPOTIFY].include?(esi.service_name) }
      case @source
      when ENV["LINKSHARE_STORES_API"]
        scrape_and_create_esi if count.positive? && song_esi_last.created_at < RELEASE_CHECK_DAYS.days.ago
      when ENV["NIGHTLY_RAKE_NEW_DISTROS"]
        scrape_and_create_esi if count.positive? && song_esi_last.created_at < DISTRIBUTION_CHECK_DAYS.days.ago
      end
    elsif @album_esi.where(service_name: SPOTIFY).present?
      scrape_and_create_esi
    elsif @album_esi.where(service_name: APPLE).present?
      fetch_apple_songs
      scrape_and_create_esi
    end
    @scrape_count if @source == ENV["NIGHTLY_RAKE_NEW_DISTROS"]
  end

  def album_esi_update
    @album_esi.where(service_name: STORES_NAMES[0]).blank? && album_stores.include?(STORES_NAMES[0])
  end

  def scrape_and_create_esi
    api_key = fetch_odesli_api_key
    fetch_spotify_songs(api_key) if @song.album.album_type == "Single"
    scrape_and_create_album_esi
    esi = @song_esi.where(service_name: [APPLE, SPOTIFY]).last
    song_url = retrieve_song_url(esi)
    return if song_url.nil?

    create_from_url(song_url, api_key, "song")
  end

  def scrape_and_create_album_esi
    api_key = fetch_odesli_api_key
    esi = @album_esi.where(service_name: [APPLE, SPOTIFY]).last
    @album = esi.linkable
    album_url = retrieve_album_url(esi) if esi.present?
    return if album_url.nil?

    create_from_url(album_url, api_key, "album")
  end

  def create_from_url(url, api_key, type)
    @scrape_count += 1 if @source == ENV["NIGHTLY_RAKE_NEW_DISTROS"]
    response_body = Odesli::ApiClient.new(url, api_key).get
    return if response_body.nil?

    available_stores = fetch_available_stores(response_body["entitiesByUniqueId"])
    return if available_stores.blank?

    available_stores.each do |available_store|
      create_esi(available_store, type, response_body: response_body)
    end
    add_napster_esi
  end

  def convert_to_keys(store_string)
    store_hash = Utilities::StringToHashConverter.convert_to_hash(store_string)
    store_hash.keys
  end

  def fetch_available_stores(stores_response_hash)
    # Fetches stores from where scraping has to be done
    stores_to_be_fetched = convert_to_keys(ENV["STORE_ORDER"])
    # Select only needed stores
    stores_needed = album_stores & stores_to_be_fetched
    stores_obtained_from_odesli = fetch_odesli_stores(stores_response_hash)
    stores_needed & stores_obtained_from_odesli
  end

  def create_esi(available_store, type, **odesli_args)
    response_body = odesli_args.blank? ? nil : odesli_args[:response_body]
    odesli_hash = odesli_args.blank? ? nil : odesli_args[:odesli_hash]
    odesli_hash = create_odesli_hash(available_store, response_body) if odesli_hash.nil?
    linkable = (type == "album") ? @album : @song
    esi = ExternalServiceId.where(external_services_id_params(linkable, odesli_hash))
    ExternalServiceId.create!(external_services_id_params(linkable, odesli_hash)) if esi.blank?
  rescue StandardError => e
    Rails.logger.info "Error in creating esi in Store Fetching #{e}"
    Airbrake.notify("Error in creating esi in Store Fetching #{e}")
  end

  def fetch_spotify_songs(_api_key)
    album_identifier = @album_esi.where(service_name: SPOTIFY).last&.identifier
    return if album_identifier.nil?

    spot_client = Spotify::ApiClient.new
    tracks = spot_client.get_album_tracks(album_identifier)
    return if tracks.blank?

    tracks.each do |track|
      song_identifier = track["external_urls"]["spotify"].split("/").last
      odesli_hash = { name: SPOTIFY, id: song_identifier }
      song_esi = @song.external_service_ids.where(service_name: SPOTIFY).last
      song_esi.update(identifier: song_identifier) if song_esi.present?
      create_esi(SPOTIFY, "song", odesli_hash: odesli_hash)
    end
    @scrape_count += 1 if @source == ENV["NIGHTLY_RAKE_NEW_DISTROS"]
    @song_esi = @song.external_service_ids
  end

  def fetch_apple_songs
    return if @song.external_service_ids.where(service_name: APPLE).present?

    album_upc = @song.album.upc.number
    return if album_upc.nil?

    client = Faraday.new
    url = "https://itunes.apple.com/lookup?upc=#{album_upc}&entity=song"
    response = client.get url
    return if response.nil?

    json_response = JSON.parse(response.body)
    results = json_response["results"]
    song_identifier = nil
    results.each do |result|
      next if result["wrapperType"] == "collection"

      song_identifier = result["trackId"] if result["trackName"].include? @song.name
    end
    odesli_hash = { name: APPLE, id: song_identifier }
    create_esi(APPLE, "song", odesli_hash: odesli_hash)
    @song_esi = @song.external_service_ids
  end

  def add_napster_esi
    return if @song.external_service_ids.where(service_name: NAPSTER).present?

    id = Napster::ApiClient.new(@song.isrc).fetch_data
    return if id.nil?

    @song.external_service_ids.create(service_name: NAPSTER, identifier: id)
  end

  def fetch_odesli_api_key
    api_key =
      case @source
      when ENV["LINKSHARE_STORES_API"]
        ENV["ODESLI_API_KEY"]
      when ENV["NIGHTLY_RAKE_NEW_DISTROS"]
        ENV["ODESLI_API_KEY_2"]
      else
        ENV["ODESLI_API_KEY"]
      end
  end

  def external_services_id_params(linkable, odesli_hash)
    {
      linkable: linkable,
      service_name: odesli_hash[:name],
      identifier: odesli_hash[:id]
    }
  end

  # Fetch stores where the album has been distributed either via tc-www
  # or tc-distributor
  def album_stores
    return @album_stores if defined?(@album_stores)

    tc_www_delivered = linkshare_distributions.where(state: "delivered")
    total_delivered = tc_www_delivered.or(tc_distributor_delivered)
    @album_stores =
      total_delivered.joins(:stores)
                     .pluck("stores.name").split("/").first
                     .uniq
                     .map { |store_name| store_name.downcase.split.first }
  end

  def tc_distributor_delivered
    album = @song.album
    distributor = TcDistributor::Base.new(album_id: album.id, distributions: linkshare_distributions)
    delivered_stores = distributor.latest_releases_stores.select { |_key, dist| dist.state == "delivered" }
    store_ids = delivered_stores.map { |_key, val| val.store_id }
    album.distributions.joins(:salepoints).where(salepoints: { store_id: store_ids })
  end

  def linkshare_distributions
    @linkshare_distributions ||= @song.album.distributions
                                      .joins(:salepoints)
                                      .where(salepoints: { store_id: STORE_IDS })
  end

  def urls_already_fetched?
    STORES_NAMES.difference(@song_esi.pluck(:service_name)).empty?
  end

  def song_esi_last
    @song_esi.where.not(created_at: nil).order(:created_at).last
  end
end
