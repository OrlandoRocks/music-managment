# frozen_string_literal: true

class Api::CertService
  attr_accessor :purchase

  def initialize(purchase)
    @purchase = purchase
  end

  def remove_cert
    purchase.cert.disassociate
    purchase.reload
    purchase.product.set_targeted_product(
      TargetedProduct.targeted_product_for_active_offer(purchase.person, purchase.product)
    )
    purchase.recalculate
    purchase
  end
end
