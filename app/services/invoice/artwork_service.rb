class Invoice::ArtworkService
  attr_reader :invoice, :icons, :covers

  def self.call(invoice)
    new(invoice).call
  end

  def initialize(invoice)
    @invoice = invoice
    @icons = []
    @covers = []
  end

  def call
    collect_icons_and_covers!
    covers.length > icons.length ? covers : icons
  end

  private

  def collect_icons_and_covers!
    invoice.purchases.each do |i|
      case i.related_type
        # product icons
      when "Composer"
        icons.push("/images/cart_icons/pubadmin_big.jpg")
      when "Product"
        handle_product_icons(i)
      when "Album"
        handle_album_icons(i)
      when "SalepointSubscription", "Booklet"
        handle_artwork_covers(i)
      when "Salepoint"
        handle_salepoint_covers(i)
      when "CreditUsage"
        handle_credit_covers(i)
      when "Renewal"
        handle_renewal_covers_and_icons(i)
      when "PlanAddon"
        handle_plan_addon_icons(i)
      when "SoundoutReport"
        covers.push("/images/cart_icons/tracksmarts_big.jpg")
      when "YoutubeMonetization"
        covers.push("/images/cart_icons/ytm_big.png")
      end
    end
  end

  def handle_product_icons(purchase)
    if (purchase.related.id.in? Product::PLAN_PRODUCT_IDS)
      # plans icons
      plan_id = PlansProduct.find_by(product_id: purchase.related.id).plan_id
      icons << "/images/cart_icons/plan_#{plan_id}_big.png"
    else
      credit_count = purchase.product.name.gsub(/\D/, "");
      credit_count = 1 if credit_count.empty?
      icons.push("/images/cart_icons/credit_#{credit_count}_big.jpg")
    end
    # distro covers
  end

  def handle_renewal_covers_and_icons(purchase)
    if purchase.renewal.items.first.related.instance_of?(PersonPlan)
      icons.push("/images/cart_icons/plan_#{purchase.renewal.items.first.related.plan_id}_big.png")
    elsif purchase.renewal.items.first.related.artwork
      covers.push(purchase.renewal.items.first.related.artwork.url(:medium))
    end
  end

  def handle_album_icons(purchase)
    covers.push(purchase.purchased_item.artwork.url(:medium)) if purchase.purchased_item.artwork
  end

  def handle_artwork_covers(purchase)
    covers.push(purchase.related.album.artwork.url(:medium)) if purchase.related.album.artwork
  end

  def handle_salepoint_covers(purchase)
    covers.push(purchase.related.salepointable.artwork.url(:medium)) if purchase.related.salepointable.artwork
  end

  def handle_credit_covers(purchase)
    covers.push(purchase.related.related.artwork.url(:medium)) if purchase.related.related.artwork
  end

  def handle_plan_addon_icons(purchase)
    icons.push("/images/cart_icons/splits_collaborator_big_icon.png") if purchase.splits_collaborator?
  end
end
