class Invoice::BalancePaymentService
  attr_accessor :balance_payment, :payment_methods

  def initialize(invoice)
    @payment_methods, @tcbalance = invoice.invoice_settlements.partition { |i| i.source_type != "PersonTransaction" }
    @balance_payment = calculate_balance_payment
  end

  private

  def calculate_balance_payment
    balance_payment = 0
    @tcbalance.each do |b|
      balance_payment += b.settlement_amount_cents
    end
    balance_payment.to_f / 100
  end
end
