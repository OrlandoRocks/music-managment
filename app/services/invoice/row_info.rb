class Invoice::RowInfo
  include CurrencyHelper
  include InvoicesHelper

  attr_accessor :balance_used, :total_charged, :total_price, :pretax_subtotal, :igst_total, :cgst_total, :sgst_total

  def initialize(balance_payment, cart_data, invoice, order, person)
    @balance_payment = balance_payment
    @cart_data = cart_data
    @invoice = invoice
    @order = order
    @person = person

    @balance_used = (order == 1) ? "balance_used" : ""
    @total_charged = (order == 2) ? "cart-total_price" : ""
    calculate_totals
  end

  private

  def calculate_totals
    tax_amount = @invoice.gst_tax_amount
    tax_paid_with_balance = @invoice.gst_tax_amount(@balance_payment.to_money(@cart_data.currency))

    case @order
    when 0
      price = @cart_data.total_price.to_money(@cart_data.currency)
      @pretax_subtotal = pretax_invoice_subtotal(@order, { price: price })
      @total_price = money_to_currency(price, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    when 1
      price = @balance_payment.to_money(@cart_data.currency)
      @igst_total = tax_paid_with_balance.igst * -1 if @invoice.gst_config.igst?
      @cgst_total = tax_paid_with_balance.cgst * -1 if @invoice.gst_config.cgst?
      @sgst_total = tax_paid_with_balance.sgst * -1 if @invoice.gst_config.sgst?
      @pretax_subtotal = pretax_invoice_subtotal(@order, { price: price, balance_payment: @balance_payment })
      @total_price = money_to_currency(price * -1, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    when 2
      price = @cart_data.total_price.to_money(@cart_data.currency)
      @pretax_subtotal = pretax_invoice_subtotal(@order, { price: price, balance_payment: @balance_payment })

      price -= @balance_payment.to_money(@cart_data.currency)
      @igst_total = tax_amount.igst + (tax_paid_with_balance.igst * -1) if @invoice.gst_config.igst?
      @cgst_total = tax_amount.cgst + (tax_paid_with_balance.cgst * -1) if @invoice.gst_config.cgst?
      @sgst_total = tax_amount.sgst + (tax_paid_with_balance.sgst * -1) if @invoice.gst_config.sgst?
      @total_price = money_to_currency(price, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    end

    if @invoice.gst_config.igst?
      @igst_total = money_to_currency(@igst_total || tax_amount.igst, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    else
      @cgst_total = money_to_currency(@cgst_total || tax_amount.cgst, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
      @sgst_total = money_to_currency(@sgst_total || tax_amount.sgst, iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    end
  end

  def pretax_invoice_subtotal(order, payment_data = {})
    case order
    when 0
      money_to_currency(@invoice.final_settlement_amount_without_tax(payment_data[:price]), iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    when 1
      price = payment_data[:balance_payment].to_money(@cart_data.currency)
      money_to_currency((@invoice.final_settlement_amount_without_tax(payment_data[:price]) * -1), iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    when 2
      pretax_subtotal = @invoice.final_settlement_amount_without_tax(payment_data[:price])
      balance_paid_amount = (payment_data[:balance_payment]).to_money(@cart_data.currency)
      pretax_paid_with_balance = @invoice.final_settlement_amount_without_tax(balance_paid_amount) * -1
      money_to_currency((pretax_subtotal + pretax_paid_with_balance), iso_code: @person.country_domain, currency: @person.user_currency, round_value: false)
    end
  end
end
