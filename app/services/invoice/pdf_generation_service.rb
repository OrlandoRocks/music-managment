class Invoice::PdfGenerationService
  def self.generate(invoice_id)
    invoice          = Invoice.find_by(id: invoice_id)
    person           = invoice.person
    domain           = "CountryWebsite::#{person.country_website.country}_DOMAIN".constantize
    invoice_template = PersonNotifier.payment_thank_you(person, invoice, domain).body
    WickedPdf.new.pdf_from_string(invoice_template)
  end
end
