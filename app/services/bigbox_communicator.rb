class BigboxCommunicator
  def self.enqueue_for_bigbox(queue_name, job_type, locker_name, urls, aws_access_key_id, aws_secret_access_key, options)
    new(queue_name, job_type, locker_name, urls, aws_access_key_id, aws_secret_access_key, options)
      .tap(&:enqueue)
  end

  def initialize(queue_name, job_type, locker_name, urls, _aws_access_key_id, _aws_secret_access_key, options = {})
    @job_type     = job_type
    @urls         = urls
    @locker_name  = locker_name
    @queue        = SQS_CLIENT.queues.named(queue_name)
    @package_name = options[:package_name] if options
    @song_id      = options[:song_id] if options
    @person_id    = options[:person_id] if options
  end

  def enqueue
    body   = ActiveSupport::JSON.encode({ "job_type" => @job_type, "urls" => @urls, "prefix" => @locker_name, "packager_class" => @packager, "id" => "fromtc", "package_name" => @package_name, "song_id" => @song_id, "person_id" => @person_id })
    body64 = Base64.encode64(body)
    @queue.send_message(body64)
  end
end
