# frozen_string_literal: true

module Indlocal
  class InvoiceStatementsService
    GENERIC_SELECT_STATEMENTS = {
      state: "people.state as state",
      buyer_name: "people.name as buyer_name",
      person_id: "people.id as person_id",
      person_city: "people.city as city",
      gstin: "gst_infos.gstin as gstin",
      buyer_email: "people.email as buyer_email",
      buyer_phone_number: "if(people.mobile_number is not null, people.mobile_number, people.phone_number) as buyer_phone_number",
      pegged_rate: "foreign_exchange_pegged_rates.pegged_rate as usd_to_inr_conversion_rate"
    }.freeze

    PURCHASE_HEADERS = [
      "tunecore_invoice_id",
      "invoice_date",
      "usd_amount_charged",
      "usd_to_inr_conversion_rate",
      "inr_amount_charged",
      "buyer_name",
      "person_id",
      "city",
      "state",
      "usd_sgst",
      "usd_cgst",
      "usd_igst",
      "inr_sgst",
      "inr_cgst",
      "inr_igst",
      "gstin",
      "buyer_email",
      "buyer_phone_number",
      "bi_invoice_number"
    ].freeze

    CREDIT_NOTE_HEADERS = [
      "credit_note_id",
      "credit_note_date",
      "usd_amount_of_credit",
      "usd_to_inr_conversion_rate",
      "inr_amount_of_credit",
      "buyer_name",
      "person_id",
      "city",
      "state",
      "usd_sgst",
      "usd_cgst",
      "usd_igst",
      "inr_sgst",
      "inr_cgst",
      "inr_igst",
      "admin_note",
      "refund_invoice_id",
      "gstin",
      "buyer_email",
      "buyer_phone_number"
    ].freeze

    attr_reader :date, :updated_gst_calc_date, :disabled_gstin_date

    def self.send_invoice_statements_email(date)
      new(date).send_invoice_statements_email
    end

    def initialize(date = Date.current)
      @date = date
      @updated_gst_calc_date = ENV.fetch("UPDATED_GST_CALC_DATE", "2020-12-20")
      @disabled_gstin_date = ENV.fetch("DISABLED_GSTIN_DATE", "2021-03-31")
    end

    def send_invoice_statements_email
      purchase_s3_options = {
        data: get_purchase_csv_array,
        headers: PURCHASE_HEADERS,
        file_name: "#{date.strftime('%Y-%m-%d')}.csv",
        bucket_name: "indlocal",
        bucket_path: "purchase_invoice_statements"
      }

      credit_note_s3_options = {
        data: get_credit_note_csv_array,
        headers: CREDIT_NOTE_HEADERS,
        file_name: "#{date.strftime('%Y-%m-%d')}.csv",
        bucket_name: "indlocal",
        bucket_path: "credit_note_invoice_statements"
      }

      S3StreamingService.write_and_upload(purchase_s3_options)
      S3StreamingService.write_and_upload(credit_note_s3_options)

      # for now we are going to create csv strings for both of these to attach
      # eventually we want to instead give them s3 links to download the csvs, which the
      # S3StreamingService makes possible - for now uploading to s3 is only for logging purposes

      purchase_csv = get_csv_string(purchase_s3_options[:headers], purchase_s3_options[:data])

      IndlocalMailer.monthly_invoice_statements(
        purchase_csv: purchase_csv,
        options: { date: date }
        # commenting out credit note gst attachment for now - we don't currently use it for tax calculation,
        # but very well may in future
        # get_csv_string(credit_note_s3_options[:headers], credit_note_s3_options[:data])
      ).deliver
    end

    def get_purchase_csv_array
      query_result = purchase_query
      query_result.map do |invoice|
        [
          invoice.tunecore_invoice_id,
          invoice.invoice_date.strftime("%d/%m/%Y"),
          invoice.usd_amount_inclusive_of_gst,
          invoice.usd_to_inr_conversion_rate,
          invoice.inr_amount_inclusive_of_gst,
          invoice.buyer_name,
          invoice.person_id,
          invoice.city,
          invoice.state,
          invoice.usd_sgst,
          invoice.usd_cgst,
          invoice.usd_igst,
          invoice.inr_sgst,
          invoice.inr_cgst,
          invoice.inr_igst,
          invoice.gstin,
          invoice.buyer_email,
          invoice.buyer_phone_number,
          invoice.bi_invoice_number
        ]
      end
    end

    def get_credit_note_csv_array
      balance_adjustment_query_result = balance_adjustment_query.to_a
      payments_os_transaction_query_result = payments_os_transaction_query.to_a

      combined_query_result = payments_os_transaction_query_result.concat(balance_adjustment_query_result)
      sorted_combined_query_result = combined_query_result.sort_by(&:credit_note_date)

      sorted_combined_query_result.map do |credit_note|
        [
          credit_note.credit_note_id,
          credit_note.credit_note_date.strftime("%d/%m/%Y"),
          credit_note.usd_amount_of_credit,
          credit_note.usd_to_inr_conversion_rate,
          credit_note.inr_amount_of_credit,
          credit_note.buyer_name,
          credit_note.person_id,
          credit_note.city,
          credit_note.state,
          credit_note.usd_sgst,
          credit_note.usd_cgst,
          credit_note.usd_igst,
          credit_note.inr_sgst,
          credit_note.inr_cgst,
          credit_note.inr_igst,
          credit_note.admin_note,
          credit_note.refund_invoice_id,
          credit_note.gstin,
          credit_note.buyer_email,
          credit_note.buyer_phone_number
        ]
      end
    end

    def purchase_query
      cc_invoices = Invoice
                    .select(invoice_select_statement(:person_city))
                    .left_joins(:gst_config)
                    .joins(:payments_os_transactions)
                    .left_joins(:corporate_entity)
                    .joins(:foreign_exchange_pegged_rate)
                    .for_gst_by_country_website(CountryWebsite::INDIA)
                    .within_month_of_date(date)
                    .where_settled
                    .where(payments_os_transactions: { action: "sale" })
      balance_invoices = Invoice
                         .select(invoice_select_statement(:person_city))
                         .left_joins(:gst_config)
                         .left_joins(:payments_os_transactions)
                         .joins(:foreign_exchange_pegged_rate)
                         .left_joins(:corporate_entity)
                         .for_gst_by_country_website(CountryWebsite::INDIA)
                         .within_month_of_date(date)
                         .where_settled
                         .where(payments_os_transactions: { id: nil })
      (cc_invoices + balance_invoices).sort_by(&:invoice_date)
    end

    def balance_adjustment_query
      BalanceAdjustment
        .select(balance_adjustment_select_statement)
        .for_gst_info_by_country_website(CountryWebsite::INDIA)
        .for_refunds
        .within_month_of_date(date)
        .order(:created_at)
    end

    def payments_os_transaction_query
      PaymentsOSTransaction
        .select(payments_os_transaction_select_statement)
        .for_gst_info_by_country_website(CountryWebsite::INDIA)
        .for_refunds
        .joins(:foreign_exchange_pegged_rate)
        .within_month_of_date(date)
        .order(:created_at)
    end

    def invoice_select_statement(city_type)
      inr_amount = "(invoices.final_settlement_amount_cents * foreign_exchange_pegged_rates.pegged_rate)"
      final_amt_cents = "invoices.final_settlement_amount_cents"

      usd_sgst_calculus = "(#{final_amt_cents} * (gst_configs.sgst / 100)) / 100"
      usd_cgst_calculus = "(#{final_amt_cents} * (gst_configs.cgst / 100)) / 100"
      usd_igst_calculus = "(#{final_amt_cents} * (gst_configs.igst / 100)) / 100"

      inr_sgst_calculus = "(#{inr_amount} * (gst_configs.sgst / 100))/100"
      inr_cgst_calculus = "(#{inr_amount} * (gst_configs.cgst / 100))/100"
      inr_igst_calculus = "(#{inr_amount} * (gst_configs.igst / 100))/100"

      new_usd_sgst_calculus = "(#{final_amt_cents} - (#{final_amt_cents} / (1+(gst_configs.sgst / 100)))) / 100"
      new_usd_cgst_calculus = "(#{final_amt_cents} - (#{final_amt_cents} / (1+(gst_configs.cgst / 100)))) / 100"
      new_usd_igst_calculus = "(#{final_amt_cents} - (#{final_amt_cents} / (1+(gst_configs.igst / 100)))) / 100"

      new_inr_sgst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.sgst / 100))))/100"
      new_inr_cgst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.cgst / 100))))/100"
      new_inr_igst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.igst / 100))))/100"

      type = "invoices"

      [
        "invoices.id as tunecore_invoice_id",
        "invoices.created_at as invoice_date",
        "invoices.final_settlement_amount_cents / 100 as usd_amount_inclusive_of_gst",
        GENERIC_SELECT_STATEMENTS[:pegged_rate],
        "round(#{inr_amount} / 100, 2) as inr_amount_inclusive_of_gst",
        GENERIC_SELECT_STATEMENTS[:buyer_name],
        GENERIC_SELECT_STATEMENTS[:person_id],
        GENERIC_SELECT_STATEMENTS[city_type],
        GENERIC_SELECT_STATEMENTS[:state],
        sgst_select_statement(usd_sgst_calculus, new_usd_sgst_calculus, "usd", type),
        cgst_select_statement(usd_cgst_calculus, new_usd_cgst_calculus, "usd", type),
        igst_select_statement(usd_igst_calculus, new_usd_igst_calculus, "usd", type),
        sgst_select_statement(inr_sgst_calculus, new_inr_sgst_calculus, "inr", type),
        cgst_select_statement(inr_cgst_calculus, new_inr_cgst_calculus, "inr", type),
        igst_select_statement(inr_igst_calculus, new_inr_igst_calculus, "inr", type),
        gstin_null_condition(type),
        GENERIC_SELECT_STATEMENTS[:buyer_email],
        GENERIC_SELECT_STATEMENTS[:buyer_phone_number],
        "CONCAT(corporate_entities.inbound_invoice_prefix, invoices.invoice_sequence) AS bi_invoice_number"
      ]
    end

    def balance_adjustment_select_statement
      balance_adjustment_gross = "(balance_adjustments.credit_amount - balance_adjustments.debit_amount)"
      sgst_calculus = "(#{balance_adjustment_gross} * (gst_configs.sgst / 100))"
      cgst_calculus = "(#{balance_adjustment_gross} * (gst_configs.cgst / 100))"
      igst_calculus = "(#{balance_adjustment_gross} * (gst_configs.igst / 100))"

      new_sgst_calculus = "#{balance_adjustment_gross} - (#{balance_adjustment_gross} / (1+(gst_configs.sgst / 100)))"
      new_cgst_calculus = "#{balance_adjustment_gross} - (#{balance_adjustment_gross} / (1+(gst_configs.cgst / 100)))"
      new_igst_calculus = "#{balance_adjustment_gross} - (#{balance_adjustment_gross} / (1+(gst_configs.igst / 100)))"

      type = "balance_adjustments"
      [
        "balance_adjustments.id as credit_note_id",
        "balance_adjustments.created_at as credit_note_date",
        "#{balance_adjustment_gross} as usd_amount_of_credit",
        "'No Rate Available' as usd_to_inr_conversion_rate",
        "'N/A' as inr_amount_of_credit",
        GENERIC_SELECT_STATEMENTS[:buyer_name],
        GENERIC_SELECT_STATEMENTS[:person_id],
        GENERIC_SELECT_STATEMENTS[:person_city],
        GENERIC_SELECT_STATEMENTS[:state],
        sgst_select_statement(sgst_calculus, new_sgst_calculus, "usd", type),
        cgst_select_statement(cgst_calculus, new_cgst_calculus, "usd", type),
        igst_select_statement(igst_calculus, new_igst_calculus, "usd", type),
        "'N/A' as inr_sgst",
        "'N/A' as inr_cgst",
        "'N/A' as inr_igst",
        "balance_adjustments.admin_note as admin_note",
        "null as refund_invoice_id",
        gstin_null_condition(type),
        GENERIC_SELECT_STATEMENTS[:buyer_email],
        GENERIC_SELECT_STATEMENTS[:buyer_phone_number]
      ]
    end

    def payments_os_transaction_select_statement
      inr_amount = "(payments_os_transactions.amount * foreign_exchange_pegged_rates.pegged_rate)"
      payments_os_amount = "payments_os_transactions.amount"

      usd_sgst_calculus = "(#{payments_os_amount} * (gst_configs.sgst / 100))/100"
      usd_cgst_calculus = "(#{payments_os_amount} * (gst_configs.cgst / 100))/100"
      usd_igst_calculus = "(#{payments_os_amount} * (gst_configs.igst / 100))/100"

      inr_sgst_calculus = "(#{inr_amount} * (gst_configs.sgst / 100))/100"
      inr_cgst_calculus = "(#{inr_amount} * (gst_configs.cgst / 100))/100"
      inr_igst_calculus = "(#{inr_amount} * (gst_configs.igst / 100))/100"

      new_usd_sgst_calculus = "(#{payments_os_amount} - (#{payments_os_amount} / (1+ (gst_configs.sgst / 100)))) /100"
      new_usd_cgst_calculus = "(#{payments_os_amount} - (#{payments_os_amount} / (1+ (gst_configs.cgst / 100)))) /100"
      new_usd_igst_calculus = "(#{payments_os_amount} - (#{payments_os_amount} / (1+ (gst_configs.igst / 100)))) /100"

      new_inr_sgst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.sgst / 100)))) /100"
      new_inr_cgst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.cgst / 100)))) /100"
      new_inr_igst_calculus = "(#{inr_amount} - (#{inr_amount} / (1+(gst_configs.igst / 100)))) /100"

      type = "payments_os_transactions"

      [
        "payments_os_transactions.id as credit_note_id",
        "payments_os_transactions.updated_at as credit_note_date",
        "(payments_os_transactions.amount / 100) as usd_amount_of_credit",
        GENERIC_SELECT_STATEMENTS[:pegged_rate],
        "round((#{inr_amount} / 100),2) as inr_amount_of_credit",
        GENERIC_SELECT_STATEMENTS[:buyer_name],
        GENERIC_SELECT_STATEMENTS[:person_id],
        GENERIC_SELECT_STATEMENTS[:person_city],
        GENERIC_SELECT_STATEMENTS[:state],
        sgst_select_statement(usd_sgst_calculus, new_usd_sgst_calculus, "usd", type),
        cgst_select_statement(usd_cgst_calculus, new_usd_cgst_calculus, "usd", type),
        igst_select_statement(usd_igst_calculus, new_usd_igst_calculus, "usd", type),
        sgst_select_statement(inr_sgst_calculus, new_inr_sgst_calculus, "inr", type),
        cgst_select_statement(inr_cgst_calculus, new_inr_cgst_calculus, "inr", type),
        igst_select_statement(inr_igst_calculus, new_inr_igst_calculus, "inr", type),
        "null as admin_note",
        "payments_os_transactions.invoice_id as refund_invoice_id",
        gstin_null_condition(type),
        GENERIC_SELECT_STATEMENTS[:buyer_email],
        GENERIC_SELECT_STATEMENTS[:buyer_phone_number]
      ]
    end

    def sgst_select_statement(old_calculus, new_calculus, currency, type)
      "if(gst_configs.sgst is not null, #{gst_calculus_condition(old_calculus, new_calculus, currency, type)}, null) as #{currency}_sgst"
    end

    def cgst_select_statement(old_calculus, new_calculus, currency, type)
      "if(gst_configs.cgst is not null, #{gst_calculus_condition(old_calculus, new_calculus, currency, type)}, null) as #{currency}_cgst"
    end

    def igst_select_statement(old_calculus, new_calculus, currency, type)
      "if(gst_configs.igst is not null, #{gst_calculus_condition(old_calculus, new_calculus, currency, type)}, null) as #{currency}_igst"
    end

    def gst_calculus_condition(old_calculus, new_calculus, currency, type)
      "if(#{type}.created_at > '#{updated_gst_calc_date}', #{calculus_concat(new_calculus, currency)}, #{calculus_concat(old_calculus, currency)})"
    end

    def gstin_null_condition(type)
      return GENERIC_SELECT_STATEMENTS[:gstin] if FeatureFlipper.show_feature?(:enable_gst)

      "if(#{type}.created_at > '#{disabled_gstin_date}', null, gst_infos.gstin) as gstin"
    end

    def calculus_concat(calculus, currency)
      "concat(round(#{calculus}, 2), ' #{currency}')"
    end

    def get_csv_string(headers, data)
      CSV.generate(headers: true) do |csv|
        csv << headers
        data.each do |row|
          csv << row
        end
      end
    end
  end
end
