class EmailVerifier
  attr_reader :email, :person_id

  def initialize(email)
    @email = email
  end

  def verify!
    person = Person.where(email: email, status: "Active", deleted: false).first
    @person_id = person.try(:id)
    person.present?
  end
end
