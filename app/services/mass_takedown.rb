class MassTakedown
  attr_reader :errors, :success

  def self.mass_untakedown(album_ids, store_ids, person = nil)
    new(album_ids, store_ids, person).tap(&:mass_untakedown)
  end

  def initialize(album_ids, store_ids, person)
    finder     = BulkAlbumFinder.new(album_ids)
    @albums    = finder.execute
    @store_ids = store_ids&.map(&:to_s)
    @errors    = finder.messages.values || []
    @success   = []
    @person    = person

    process_store_ids
  end

  def mass_untakedown
    @albums.each do |album|
      salepoints = salepoints_to_un_takedown(album)

      if @track_monetization_store_ids.present?
        remove_track_monetization_take_downs(album, @track_monetization_store_ids)
      end

      if salepoints.empty?
        @errors << "Unable to undo takedowns for album ID: #{album.id}" if @track_monetization_store_ids.blank?
      else
        remove_takedowns(album, salepoints)
        album_untakedown(album)
        send_sns_notification_untakedown(album, salepoints)
        album.process_missing_automator_salepoints
      end
    end
  end

  def salepoints_to_un_takedown(album)
    Salepoint.includes(:store).where(
      "salepointable_id = ?
        AND salepointable_type = ?
        AND store_id IN (?)
        AND takedown_at IS NOT NULL ",
      album.id,
      "Album",
      @store_ids
    )
  end

  def remove_takedowns(album, salepoints)
    salepoints.each do |sp|
      if sp.remove_takedown
        @success << album.upc
        if sp.store.short_name.include?("iTunes")
          sp.send_metadata_update(message = "issuing a reactivation distribution")
        end
      else
        @errors << "Unable to undo takedown for album ID: #{album.id} for store #{sp.store_id}"
      end
    end
  end

  private

  def process_store_ids
    @has_fbt_store = @store_ids&.include?(fbt_store_id.to_s)

    track_mon_ids = Store::TRACK_MONETIZATION_STORES.map(&:to_s)

    @track_monetization_store_ids = @store_ids&.select { |id| track_mon_ids.include?(id.to_s) }

    @store_ids -= track_mon_ids
  end

  def remove_track_monetization_take_downs(album, store_ids)
    monetizations = TrackMonetization.joins(:song)
                                     .where(store_id: store_ids, songs: { album_id: album.id })
                                     .readonly(false)

    takedown_params = { current_user: @person, track_mons: monetizations }.with_indifferent_access

    TrackMonetization::TakedownService.remove_takedown(takedown_params)
  end

  def fbt_store_id
    @fb_store_id ||= Store.find_by(abbrev: "fbt").id
  end

  def ig_store_id
    @ig_store_id ||= Store.find_by(abbrev: "ig").id
  end

  def album_untakedown(album)
    album.update(takedown_at: nil)
    ::Renewal.renewal_for(album)&.remove_takedown!
    opts = {
      user_id: @person.id,
      subject: "Takedown",
      note: "Album takedown was cleared"
    }
    album.create_note(opts)
  end

  def send_sns_notification_untakedown(album, salepoints)
    return unless FeatureFlipper.show_feature?(:use_sns, @person.id) &&
                  ENV.fetch("SNS_RELEASE_UNTAKEDOWN_TOPIC", nil).present?

    Sns::Notifier.perform(
      topic_arn: ENV.fetch("SNS_RELEASE_UNTAKEDOWN_TOPIC"),
      album_ids_or_upcs: [album.id],
      store_ids: salepoints.pluck(:store_id),
      delivery_type: "untakedown",
      person_id: @person.id
    )
  end
end
