# frozen_string_literal: true

class ComplianceInfoFields::MetaDataService
  include CustomTranslationHelper
  include ActionView::Helpers

  attr_reader :person

  ALPHA_ONLY = [:first_name, :last_name]

  def self.display(person)
    new(person).display
  end

  def initialize(person)
    @person = Person.select([:id])
                    .includes(:compliance_info_fields, people_flags: :flag)
                    .where(id: person.id)
                    .take
  end

  # This requires person to implement a person."#{info.field_name}_locked?" interface to work.
  #
  # Default values do not go in this method. Default values go in the default_fields_meta_data
  # this method overrides the default values if there's something in the compliance info fields
  def display
    person.compliance_info_fields.each do |info|
      default_fields_meta_data[info.field_name].tap do |meta|
        meta[:compliance_info_field_id] = info.id
        meta[:field_value]              = info.field_value
        meta[:field_name]               = info.field_name
        meta[:locked]                   = person.public_send("#{info.field_name}_locked?")
        next unless meta[:locked]

        meta[:tooltip_trigger]          = "locked-field-#{info.field_name}"
        meta[:tooltip_text]             = custom_t("people.renewal_billing_info.address_fields_locked")
        meta[:tooltip_title]            = custom_t("people.renewal_billing_info.locked_contact_info")
      end
    end

    default_fields_meta_data
  end

  private

  def field_names
    ComplianceInfoField::PERMITTED_FIELD_NAMES
  end

  def default_fields_meta_data
    return @fields_meta_data unless @fields_meta_data.nil?

    data              = {}.with_indifferent_access
    @fields_meta_data =
      field_names.each_with_object(data) do |field_name, obj|
        obj[field_name] = {
          compliance_info_field_id: nil,
          field_name: nil,
          field_value: nil,
          locked: false,
          alpha_only_validation: ALPHA_ONLY.include?(field_name.to_sym),
          tooltip_trigger: "info-field-#{field_name}",
          tooltip_text: custom_t("account_settings.first_name_legal_info"),
          tooltip_title: custom_t("account_settings.legal_name")
        }
      end

    @fields_meta_data
  end
end
