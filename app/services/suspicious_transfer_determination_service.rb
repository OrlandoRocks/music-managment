class SuspiciousTransferDeterminationService
  def self.transfer_suspicious?(transfer, rules_engine = BlacklistedPaypalPayee::SuspiciousTransferRulesService)
    new(transfer, rules_engine).transfer_suspicious?
  end

  def initialize(transfer, rules_engine)
    @transfer     = transfer
    @rules_engine = rules_engine
  end

  def transfer_suspicious?
    rules_engine.suspicious?(transfer) || blacklist_includes_payee
  end

  private

  attr_reader :transfer, :rules_engine

  def blacklist_includes_payee
    "Blacklisted#{transfer_type}Payee".constantize.exists?(payee: transfer.payee)
  end

  def transfer_type
    transfer.class.to_s[/(\w+)Transfer/, 1]
  end
end
