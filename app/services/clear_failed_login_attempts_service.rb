class ClearFailedLoginAttemptsService
  def self.clear(id)
    person = Person.find(id)
    person.update_attribute(:recent_login, Time.now)
    person.update_attribute(:login_attempts, 0) if person.login_attempts != 0
  end
end
