class Ytsr::Salepoint
  def self.create(person_id)
    new(person_id).create
  end

  attr_reader :person

  def initialize(person_id)
    @person = Person.includes(:albums, :salepoints).find(person_id)
  end

  def create
    return false if albums.empty?

    albums.each { |album| Ytsr::Track.create(album.id) }
  end

  private

  def albums
    @albums ||= eligible_albums_for_ytsr_salepoint
  end

  def eligible_albums_for_ytsr_salepoint
    person.albums
          .where(payment_applied: true, takedown_at: nil, is_deleted: false)
          .where(Album.arel_table[:album_type].not_eq("Ringtone"))
          .select { |album| has_or_needs_ytsr_salepoint?(album) }
  end

  def has_or_needs_ytsr_salepoint?(album)
    album.salepoints.exists?(store_id: ytsr_store_id, status: "new") ||
      !album.salepoints.exists?(store_id: ytsr_store_id)
  end

  def ytsr_store_id
    @ytsr_store_id ||= Store.find_by(abbrev: "ytsr").id
  end
end
