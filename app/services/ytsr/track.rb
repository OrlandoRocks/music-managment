class Ytsr::Track
  def self.create(album_id)
    new(album_id).create
  end

  attr_reader :album, :ytsr_store, :ytm_purchase

  def initialize(album_id)
    @album        = Album.includes(:person, :primary_genre, :secondary_genre, :songs).find(album_id)
    @ytsr_store   = Store.find_by(abbrev: "ytsr")
    @ytm_purchase = @album.person.youtube_monetization.try(:purchase)
  end

  def create
    return false unless eligible_for_ytsr?

    invalid_track_explanation? && create_rejected_salepoints
    youtube_salepoint.update(status: "complete")
  end

  private

  def eligible_for_ytsr?
    album.approved_for_distribution? && album.person.has_purchased_ytm?
  end

  def youtube_salepoint
    Salepoint.where(salepointable_id: album.id, store_id: ytsr_store.id).first ||
      Salepoint.add_youtubesr_store(album, ytm_purchase)
  end

  def invalid_track_explanation?
    invalid_genre? || invalid_album_name? || invalid_track_name?
  end

  def invalid_genre?
    return false unless ineligible_album_genre

    @reason   = "Invalid Genre"
    @comments = "#{ineligible_album_genre[:genre_level]}: #{ineligible_album_genre[:genre_name]}."
    true
  end

  def ineligible_album_genre
    @ineligible_genre ||=
      begin
        genre_level, genre =
          {
            primary_genre: album.primary_genre,
            secondary_genre: album.secondary_genre
          }.find { |_key, genre| genre.present? && !genre.you_tube_eligible? }

        { genre_level: genre_level.to_s.titleize, genre_name: genre.name } unless genre.nil?
      end
  end

  def invalid_album_name?
    ineligible_keyword = YouTubeIneligibleKeyword.keyword_present?(album.name)
    return false unless ineligible_keyword

    @reason   = "Ineligible Keyword"
    @comments = "#{ineligible_keyword} found in release name."
    true
  end

  def invalid_track_name?
    album.songs.each do |song|
      ineligible_keyword = YouTubeIneligibleKeyword.keyword_present?(song.name)

      next unless ineligible_keyword

      @reason   = "Ineligible Keyword"
      @comments = "#{ineligible_keyword} found in track name (Track #{song.track_num})"
      return true
    end
    false
  end

  def create_rejected_salepoints
    SalepointSong.create_rejected_salepoints_for_album(album, @reason, @comments)
  end
end
