class Ytsr::MonetizationService
  attr_reader :person, :params

  GOOD_PARAMS = ["monetize", "opt_out"]

  def initialize(person:, params:)
    @person = person
    @params = params
  end

  def call
    return :invalid_params if invalid_params?
    return :has_ytm if paid_for_monetization_in_past?

    process_youtube_music_and_monetization_form
  end

  private

  def ytm_product
    @ytm_product ||= Product.find_by(id: Product::US_YTM)
  end

  def paid_for_monetization_in_past?
    @paid_for_monetization_in_past ||= person.has_active_ytm?
  end

  def invalid_params?
    GOOD_PARAMS.exclude?(params.dig(:monetization))
  end

  # Handles the form data from the Ajax request. if they already been aproved
  # they should not be able to opt out.
  # this is important because when a person comes back to the site and wants
  # to put up another album when they, themselves have alrady purchased YTM
  # with a previous release.
  def process_youtube_music_and_monetization_form
    case params[:monetization]
    when "monetize" then opt_into_monetization
    when "opt_out"  then opt_out_of_monetization
    end
  end

  # Handles if person opts into monetization.
  # Events should not fire if opting in fails
  def opt_into_monetization
    ActiveRecord::Base.transaction do
      youtube_monetization
      add_ytm_product_to_cart
    end
  end

  # Handles if person opts out of monetization after opting in.
  # Events should not fire if opting out fails
  def opt_out_of_monetization
    ActiveRecord::Base.transaction do
      remove_ytm_product_from_cart
      youtube_monetization.destroy
    end
  end

  # Find or Create youtube monetization for person
  def youtube_monetization
    @youtube_monetization ||=
      YoutubeMonetization.where(person: person)
                         .first_or_create({
                                            person: person,
                                            agreed_to_terms_at: Time.now.utc
                                          })
  end

  # Adds youtube_monetization to the cart
  def add_ytm_product_to_cart
    youtube_monetization.create_youtube_purchase(ytm_product)
  end

  # Removes youtube_monetization from the cart
  def remove_ytm_product_from_cart
    youtube_monetization&.purchase&.destroy
  end
end
