class Ytsr::MonetizedIsrcSearch
  def self.search(isrcs)
    new(isrcs).search
  end

  attr_accessor :isrcs, :results, :songs

  def initialize(isrcs)
    @isrcs    = isrcs
    @results  = {}
  end

  def search
    get_songs
    add_song_data_by_person
    group_songs_by_monetization
    results
  end

  private

  def get_songs
    @songs = SongIsrcSearch.search(isrcs)
  end

  def add_song_data_by_person
    songs.each do |song|
      person    = song.album.person
      key       = person.id

      if results[key]
        results[key][:songs] << song
      else
        results[key]          = {}
        results[key][:person] = person
        results[key][:songs]  = [song]
      end
    end
  end

  def group_songs_by_monetization
    results.each do |_person_id, data|
      person           = data[:person]

      monetized_isrcs  = monetized_songs(person).map(&:tunecore_isrc)

      data[:monetized_songs]     = []
      data[:non_monetized_songs] = []

      data[:songs].each do |song|
        if monetized_isrcs.include?(song.tunecore_isrc)
          data[:monetized_songs] << song
        else
          data[:non_monetized_songs] << song
        end
      end

      data.delete(:songs)
    end
  end

  def monetized_songs(person)
    person.songs.monetized_for(Store::YTSR_STORE_ID)
  end
end
