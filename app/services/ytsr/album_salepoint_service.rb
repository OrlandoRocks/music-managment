class Ytsr::AlbumSalepointService
  attr_reader :person, :params

  # More may be added later
  YT_STORE_NAMES = [
    "YouTube Music / YouTube Shorts",
    "YouTube Content ID Monetization"
  ].freeze

  # More may be added later
  YT_STORE_SHORT_NAMES = [
    "YTMusic",
    "YoutubeSR"
  ].freeze

  def initialize(person:, params:)
    @person = person
    @params = params
  end

  def call
    return :invalid_params if invalid_params?

    process_youtube_music_and_monetization_form
  end

  private

  def valid_form_params?
    ["add_salepoints", "remove_salepoints"].include?(params[:salepoints])
  end

  def invalid_params?
    !album? || !valid_form_params?
  end

  def album
    @album ||= Album.includes(:salepoints)
                    .find_by(id: params[:album_id], person: person)
  end

  def album?
    album.present?
  end

  def yt_stores
    @yt_stores ||= Store.where(name: YT_STORE_NAMES)
  end

  def yt_stores?
    yt_stores.present?
  end

  # Handles the form data from the Ajax request
  def process_youtube_music_and_monetization_form
    return unless album? && yt_stores?

    case params[:salepoints]
    when "add_salepoints"    then add_salespoints
    when "remove_salepoints" then remove_salepoints
    end
  end

  # Creates new youTube salespoints and add then to cart
  # if they dont already exists
  def add_salespoints
    yt_stores.reject { |yt_store| existing_salepoint_store_ids.include?(yt_store.id) }
             .map { |store| create_salepoint_for(store) }
  end

  # Removes youTube Salepoints
  def remove_salepoints
    album.salepoints
         .where(store: yt_stores)
         .destroy_all
  end

  # Helper method that finds the existing youtube salepoint ids if they have already been created or added.
  def existing_salepoint_store_ids
    @existing_salepoint_store_ids ||= album.salepoints.map(&:store_id) & yt_stores.map(&:id)
  end

  def create_salepoint_for(store)
    Salepoint.create(store: store, salepointable: album)
  end
end
