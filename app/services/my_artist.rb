class MyArtist
  attr_reader :person_id, :artist_id

  def self.mapping_for(person_id, artist_id)
    new(person_id, artist_id).mappings
  end

  def self.releases_for(service_name, person_id, artist_id)
    new(person_id, artist_id).releases_for(service_name)
  end

  def self.artists_for(person_id)
    MyArtist::NameQueryBuilder.build(person_id)
  end

  def self.title_artists_for(person_id)
    artists_for(person_id).where(creatives: { role: title_roles })
  end

  def self.all_artists_for(person_id)
    artists_for(person_id).where(creatives: { role: all_roles })
  end

  def self.title_roles
    %w[primary_artist featuring]
  end

  def self.all_roles
    title_roles + %w[contributor]
  end

  def initialize(person_id, artist_id)
    @person_id = person_id
    @artist_id = artist_id
  end

  def releases
    @releases ||= MyArtist::ReleasesQueryBuilder.build(person_id, artist_id)
  end

  def mappings
    redistribution_package = RedistributionPackage.for(person_id, artist_id, "apple")
    store_mapping = MyArtistMapping.new(apple_external_service_id, redistribution_package)

    [store_mapping]
  end

  def artwork
    @artwork ||= apple_external_service_id.artwork
  end

  def apple_external_service_id
    @apple_external_service_id ||= ExternalServiceId.artist_ids_for(person_id, artist_id, "apple").first || ExternalServiceId.new_artist_id_for_service("apple", person_id, artist_id)
  end

  def releases_for(service_name)
    @releases_for ||= {}
    @releases_for[service_name] ||= MyArtist::StoreReleaseQueryBuilder.build(person_id, artist_id, service_name)
  end
end
