class Subscription::Android::Renewal < Subscription::Renewal
  attr_accessor :subscription,
                :receipt,
                :person,
                :subscription_product,
                :generator,
                :renewal_data,
                :purchase_token,
                :product_id

  def initialize(subscription)
    @subscription = subscription
    @receipt = fetch_receipt
  end

  def renew
    purchase_token, product_id = fetch_purchase_details
    @renewal_data = renewal_data_android(purchase_token, product_id)
    return unless renewal_data

    @person = Person.find(receipt.person_id)
    @subscription_product = set_subscription_product
    @generator = generate_renewal
  end

  def fetch_purchase_details
    return unless receipt && receipt.receipt_data

    purchase_token = receipt.receipt_data.split(" ")[0]
    product_id = receipt.receipt_data.split(" ")[1]
    [purchase_token, product_id]
  end

  def fetch_receipt
    subscription.subscription_events.last.subscription_purchase.payment_channel_receipt
  end

  def renewal_data_android(purchase_token, product_id)
    purchase_details = Subscription::Android::PurchaseVerifier.new.purchase_verifier(purchase_token, product_id)
    Subscription::Android::ParamsBuilder.build(purchase_details)
  rescue Subscription::Android::PurchaseVerifier::VerificationError => e
    Rails.logger.info "Unable to process renewal: #{e.message}"
    Airbrake.notify("Unable to process renewal: #{e.message}")
    false
  end

  def set_subscription_product
    SubscriptionProduct.active_by_product_type_for(
      person,
      renewal_data[:product_name],
      renewal_data[:product_type]
    )
  end

  def successful?
    generator && generator.success? && renewed?
  end

  def generate_renewal
    generat = Subscription::Android::PurchaseGenerator.generate(
      subscription_product,
      person,
      renewal_data[:purchase_date],
      renewal_data[:expires_date],
      renewal_data[:purchase_token],
      renewal_data[:product_id]
    )
  end

  def renewed?
    generator.subscription_purchase.termination_date.utc > Time.now.utc
  end
end
