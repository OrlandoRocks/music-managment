class Subscription::Android::PurchaseRequest < FormObject
  include ::Subscription::PurchaseRequestable

  attr_accessor :purchase_date, :expires_date, :purchase_token, :product_id

  define_model_callbacks :finalize, only: [:before, :after]
  before_finalize :load_person
  before_finalize :check_for_valid_subscription_product
  before_finalize :load_subscription_product
  before_finalize :create_purchases

  def finalize
    run_callbacks :finalize do
      if errors.any?
        rollback_purchases
        false
      else
        handle_successful_purchase
        true
      end
    end
  end

  private

  def create_purchases
    @generator = Subscription::Android::PurchaseGenerator.generate(
      subscription_product,
      person,
      purchase_date,
      expires_date,
      purchase_token,
      product_id
    )
    if @generator.success?
      @subscription_purchase = @generator.subscription_purchase
    else
      errors.add(:base, "Unable to create purchase")
    end
  end

  def handle_successful_purchase
    @plan            = "pro"
    @plan_expires_at = @subscription_purchase.reload.termination_date.strftime("%Y-%m-%d %H:%M:%S")
    SocialNotifier.android_purchase(@person.id, { plan_expires_at: plan_expires_at }).deliver
  end

  def rollback_purchases
    subscription_purchase.destroy if subscription_purchase
  end
end
