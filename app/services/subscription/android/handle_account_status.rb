class Subscription::Android::HandleAccountStatus
  include TcSocialHelper

  RECOVERED = 1
  CANCELED = 3
  HOLD = 5
  RESTARTED = 7

  def initialize(params)
    @notification_type = params["notificationType"]
    @product_id = params["subscriptionId"]
    @purchase_token = params["purchaseToken"]
  end

  def process
    case @notification_type
    when CANCELED, HOLD
      handle_account_hold
    when RECOVERED, RESTARTED
      handle_account_recover
    end
  end

  def handle_account_hold
    update_subscription_status
    notify_user
  end

  def handle_account_recover
    purchase_details = Subscription::Android::PurchaseVerifier.new.purchase_verifier(@purchase_token, @product_id)
    update_subscription_status(purchase_details[:expires_date])
    notify_user
  end

  def update_subscription_status(termination_date = nil)
    receipt_data = "#{@purchase_token} #{@product_id}"
    payment_channel_receipt = PaymentChannelReceipt.find_by(receipt_data: receipt_data)
    return unless payment_channel_receipt

    termination_date ||= Time.now
    person_id = payment_channel_receipt.person_id
    @person = Person.find(person_id)
    subscription_status = @person.subscription_status_for("Social")
    subscription_status.update(termination_date: termination_date) if subscription_status
  end

  def notify_user
    invoke_tcs_push_notification_api
  end

  def invoke_tcs_push_notification_api
    return if Rails.env.test?

    uri = URI.parse(tc_social_push_notification_url)
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true unless Rails.env.development?
    req = Net::HTTP::Post.new(uri.request_uri, "api-key" => ENV["SOCIAL_API_KEY_BACKEND"])
    req.body = push_notification_params
    req.content_type = "application/json"
    https.request(req)
  end

  def push_notification_params
    plan_status = ::Social::PlanStatus.for(@person)
    params = {
      plan: plan_status.plan,
      plan_expires_at: plan_status.plan_expires_at.to_s,
      user_email: @person.email,
      notification_type: @notification_type
    }
    params.to_json
  end
end
