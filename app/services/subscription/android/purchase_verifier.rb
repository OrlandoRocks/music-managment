class Subscription::Android::PurchaseVerifier
  class VerificationError < StandardError; end

  def purchase_verifier(purchase_token, product_id)
    result = playstore_verifier.verify_subscription_purchase(
      package_name: ENV["PLAYSTORE_PACKAGE"],
      subscription_id: product_id,
      token: purchase_token
    )

    raise VerificationError,
          "Error status from Playstore" if result.instance_of? CandyCheck::PlayStore::VerificationFailure

    verifier_details(result, purchase_token, product_id)
  end

  def playstore_verifier
    CandyCheck::PlayStore::Verifier.new(authorization: playstore_config)
  end

  def playstore_config
    CandyCheck::PlayStore.authorization("config/tunecore-social-2-971bdac22313.json")
  end

  def verifier_details(result, purchase_token, product_id)
    expire_sec = (result.expiry_time_millis.to_f / 1000).to_s
    expire_date = Date.strptime(expire_sec, "%s")
    purchase_sec = (result.start_time_millis.to_f / 1000).to_s
    purchase_date = Date.strptime(purchase_sec, "%s")
    {
      purchase_date: purchase_date,
      expires_date: expire_date,
      purchase_token: purchase_token,
      product_id: product_id
    }
  end
end
