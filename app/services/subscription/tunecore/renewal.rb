class Subscription::Tunecore::Renewal < Subscription::Renewal
  def initialize(subscription)
    super
    @person          = @subscription.person
    @payment_channel = "Tunecore"
    @successful      = false
  end

  attr_reader :person, :payment_channel, :subscription_purchase, :purchase

  def renew
    create_renewal
    process_renewal if @purchase
  end

  def successful?
    return false unless @purchase

    @successful
  end

  private

  def create_renewal
    subscription_product = SubscriptionPurchase
                           .joins(:subscription_event)
                           .where("subscription_events.person_subscription_status_id = #{@subscription.id}")
                           .includes(:subscription_product)
                           .first.subscription_product

    term_length = subscription_product.term_length
    termination_date = Time.now + term_length.month if term_length.positive?

    @subscription_purchase = SubscriptionPurchase.create!(
      payment_channel: payment_channel,
      subscription_product_id: subscription_product.id,
      person: person,
      termination_date: termination_date
    )

    @purchase = Purchase.recalculate_or_create(person, subscription_product.product, subscription_purchase)
  end

  def process_renewal
    @manager           = Cart::Payment::Manager.new(person, [@purchase], "renewal")
    @person_preference = PersonPreference.find_by(person_id: person.id)
    # TODO: commenting the ability to use balance for now, needs a revisit once we refactor
    # manager.add_strategy(Cart::Payment::Strategies::Balance.new) if person_preference.pay_with_balance == 1

    if @person_preference
      payment_type = @person_preference.preferred_payment_type.underscore
      send("process_with_#{payment_type}")
    end

    if @manager.process! && @manager.successful?
      @successful = true
    else
      rollback
    end
  rescue => e
    Rails.logger.info "Unable to process renewal: #{e.message}"
    Tunecore::Airbrake.notify("Unable to process renewal: #{e.message}")
    rollback
  end

  def process_with_credit_card
    return unless person.preferred_credit_card?

    @manager.add_strategy(Cart::Payment::Strategies::CreditCard.new(person.preferred_credit_card_id))
  end

  def process_with_pay_pal
    return unless person.preferred_paypal_account?

    @manager.add_strategy(Cart::Payment::Strategies::Paypal.new(person.preferred_paypal_account, person))
  end

  def rollback
    @manager.try(:invoice).try(:destroy)
    subscription_purchase.destroy
  end
end
