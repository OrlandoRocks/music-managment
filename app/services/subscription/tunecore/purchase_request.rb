class Subscription::Tunecore::PurchaseRequest < FormObject
  include Subscription::PurchaseRequestable
  attr_accessor :payment_method, :request_ip, :last_four_cc

  attr_reader :payment_strategy

  define_model_callbacks :finalize, only: [:before, :after]

  before_finalize :load_person
  before_finalize :check_required_params
  before_finalize :check_for_valid_subscription_product
  before_finalize :load_subscription_product
  before_finalize :check_payment_method
  before_finalize :create_purchases
  before_finalize :set_payment_strategy
  after_finalize  :aggregate_trend_data

  def finalize
    run_callbacks :finalize do
      if @purchase && @subscription_purchase
        manager.process!
        if manager.successful?
          handle_successful_purchase
          true
        else
          rollback_purchases
          if manager.notice
            errors.add(:base, manager.notice)
          else
            errors.add(:base, "Unable to finalize purchase")
          end
          false
        end
      end
    end
  end

  def manager
    @manager ||= Cart::Payment::Manager.new(@person, [@purchase], request_ip)
  end

  private

  def check_required_params
    return if product_name && product_type

    errors.add(:base, "Missing product_name and/or product_type params")
    throw(:abort)
  end

  def check_payment_method
    if SUBSCRIPTION_TYPE_CONFIG[product_name][:allowed_payment_methods].include?(payment_method)
      true
    else
      errors.add(:base, "Unsupported payment type for #{product_name}.")
      throw(:abort)
    end
  end

  def set_payment_strategy
    # TODO: this logic probably belongs in its own class
    case payment_method
    when "paypal"
      paypal_account = StoredPaypalAccount.currently_active_for_person(@person)
      @payment_strategy = Cart::Payment::Strategies::Paypal.new(paypal_account, @person) if paypal_account
    when "credit_card"
      credit_card = @person.stored_credit_cards.find_by(last_four: last_four_cc)
      @payment_strategy = Cart::Payment::Strategies::CreditCard.new(credit_card.id) if credit_card
    when "balance"
      @payment_strategy = Cart::Payment::Strategies::Balance.new
    end

    if @payment_strategy && manager.add_strategy(@payment_strategy)
      true
    else
      errors.add(:base, "Unable to add payment strategy.")
      rollback_purchases
      throw(:abort)
    end
  end

  def create_purchases
    termination_date = Time.now + @subscription_product.term_length.month if purchase_expires?
    @person.cleanup_unpaid_subscriptions(@product_name)

    @subscription_purchase ||= SubscriptionPurchase.create!(
      payment_channel: "Tunecore",
      subscription_product_id: @subscription_product.id,
      person: @person,
      termination_date: termination_date
    )

    @purchase = Product.create_purchase(@person, @subscription_purchase, @subscription_product.product)
  end

  def handle_successful_purchase
    # These dynamic methods make it harder to find where exactly a class is being used. Hence this comment
    # Subscription::SocialSuccessfulPurchaseService is used here
    # Subscription::FBTrackSuccessfulPurchaseService is used here
    "Subscription::#{product_name.classify}SuccessfulPurchaseService".constantize.new(self).handle
  end

  def rollback_purchases
    @subscription_purchase.destroy
  end

  def purchase_expires?
    @subscription_product.term_length.positive?
  end
end
