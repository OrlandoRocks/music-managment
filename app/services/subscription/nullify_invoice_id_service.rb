# Wipe previously-applied invoice_id from a failed auto-renewal purchase
class Subscription::NullifyInvoiceIDService
  class NullifyInvoiceIDServiceError < StandardError; end

  def self.nullify!(purchase)
    new(purchase).nullify!
  end

  def initialize(purchase)
    @purchase = purchase
    validate_purchase
  end

  attr_reader :purchase

  def nullify!
    return unless nullable_invoice_id?

    purchase.update(invoice_id: nil)
  end

  private

  def validate_purchase
    raise NullifyInvoiceIDServiceError unless purchase.related.instance_of?(Renewal)
  end

  def nullable_invoice_id?
    return false unless purchase.invoice?

    inv = purchase.invoice

    inv.is_visible_to_customer? &&
      inv.settled? == false &&
      inv.final_settlement_amount_cents.nil? &&
      inv.has_settlements? == false
  end
end
