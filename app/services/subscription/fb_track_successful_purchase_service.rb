class Subscription::FbTrackSuccessfulPurchaseService
  def initialize(purchase_request)
    @person_id = purchase_request.person_id
    @service   = purchase_request.product_name
    @send_all  = purchase_request.send_all
  end

  def handle
    params = {
      person_id: @person_id,
      service: @service,
      send_all: @send_all
    }.with_indifferent_access

    Sidekiq.logger.info "Sidekiq Job initiated TrackMonetization::SubscriptionWorker AfterPurchase: #{params}"
    TrackMonetization::SubscriptionWorker.perform_async(params)
  end
end
