class Subscription::Renewal::QueryBuilder
  def self.build(filter, subscription_type, payment_channel)
    new(filter, subscription_type, payment_channel).tap(&:build)
  end

  attr_accessor :subscriptions

  def initialize(filter, subscription_type, payment_channel)
    @filter            = filter
    @payment_channel   = payment_channel
    @subscription_type = subscription_type
  end

  def build
    @subscriptions = send("#{@filter}_subscriptions".to_sym, base_subscriptions).to_a
  end

  private

  def base_subscriptions
    PersonSubscriptionStatus
      .joins(subscription_events: :subscription_purchase)
      .where("person_subscription_statuses.canceled_at IS NULL")
      .where("person_subscription_statuses.subscription_type = ?", @subscription_type)
      .where("subscription_purchases.payment_channel IN (?)", [@payment_channel].flatten)
      .includes(subscription_events: [subscription_purchase: [:payment_channel_receipt]])
      .distinct
  end

  def all_subscriptions(base)
    base
      .where("person_subscription_statuses.termination_date < ?", DateTime.now.beginning_of_day)
  end

  def today_subscriptions(base)
    base
      .where("person_subscription_statuses.termination_date >= ?", DateTime.now.beginning_of_day)
      .where("person_subscription_statuses.termination_date < ?", (DateTime.now + 1).beginning_of_day)
  end

  def grace_period_subscriptions(base)
    all_subscriptions(base).select(&:within_grace_period?)
  end

  def today_and_grace_period_subscriptions(base)
    today_subscriptions(base) + grace_period_subscriptions(base)
  end
end
