class Subscription::Renewal::BulkProcessor
  def self.renew(subscriptions, payment_channel)
    new(subscriptions, payment_channel).tap(&:renew)
  end

  attr_reader :renewed, :declined

  def initialize(subscriptions, payment_channel)
    @subscriptions = subscriptions
    @renewal_klass = "Subscription::#{payment_channel}::Renewal".classify.constantize
    @renewed       = []
    @declined      = []
  end

  def renew
    return false unless @subscriptions && @subscriptions.any?

    @subscriptions.each do |subscription|
      renewal = @renewal_klass.new(subscription)
      renewal.renew
      renewal.successful? ? process_success(subscription) : process_failure(subscription)
    end
  end

  private

  def process_success(subscription)
    Rails.logger.info "User #{subscription.person.email} subscription renewal: SUCCESS"
    @renewed << subscription.person
  end

  def process_failure(subscription)
    Rails.logger.info "User #{subscription.person.email} subscription renewal: FAILURE"
    @declined << subscription.person
  end
end
