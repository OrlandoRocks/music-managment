module Subscription::PurchaseRequestable
  include TrendsAggregateable

  attr_accessor :product_name, :product_type, :person_id, :plan, :plan_expires_at, :gtm_invoice_data, :send_all
  attr_reader :subscription_purchase, :person, :subscription_product

  private

  def load_person
    @person = Person.find(person_id)
  rescue ActiveRecord::RecordNotFound
    errors.add(:base, "Unable to locate user.")
    throw(:abort)
  end

  def check_for_valid_subscription_product
    return if SUBSCRIPTION_TYPE_CONFIG[product_name].present?

    errors.add(:base, "Invalid product name.")
    throw(:abort)
  end

  def load_subscription_product
    if @subscription_product = SubscriptionProduct.active_by_product_type_for(@person, product_name, product_type)
      return
    end

    errors.add(:base, "Cannot find product for #{product_name} with type: #{product_type}")
    throw(:abort)
  end
end
