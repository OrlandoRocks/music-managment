class Subscription::PaymentOptionsFetcher
  include ActiveModel::Serialization

  attr_reader :paypal, :credit_cards

  def initialize(user)
    @user = user
  end

  def fetch_payment_options
    @paypal       = user.stored_paypal_accounts.where(archived_at: nil).first
    @credit_cards = user.stored_credit_cards.active
  end

  private

  attr_reader :user
end
