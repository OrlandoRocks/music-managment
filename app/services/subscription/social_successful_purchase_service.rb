class Subscription::SocialSuccessfulPurchaseService
  attr_accessor :purchase_request, :person, :manager

  def initialize(purchase_request)
    @manager          = purchase_request.manager
    @person           = purchase_request.person
    @purchase_request = purchase_request
  end

  def handle
    purchase_request.plan             = "pro"
    purchase_request.plan_expires_at  = get_expiration_date
    purchase_request.gtm_invoice_data = Gtm::Page.new(
      person,
      currency: person.currency,
      page_type: Gtm::Invoice,
      invoice: manager.invoice
    )

    manager.invoice.update_person_analytics

    SocialNotifier.upgrade_confirmation(
      person.id,
      {
        plan_expires_at: purchase_request.plan_expires_at,
        renewal_period: purchase_request.product_type,
        payment_amount: manager.invoice.final_settlement_amount_cents
      },
      person.domain
    ).deliver
  end

  def get_expiration_date
    termination_date = purchase_request.subscription_purchase.reload.termination_date
    termination_date.present? ? termination_date.strftime("%Y-%m-%d %H:%M:%S") : termination_date
  end
end
