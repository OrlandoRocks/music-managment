class Subscription::Subscription
  include ActiveModel::Serialization

  attr_reader :payment_plan,
              :plan_status,
              :payment_channel,
              :effective_date,
              :termination_date,
              :canceled_at,
              :person_subscription_status,
              :user,
              :purchase

  delegate :plan, :plan_expires_at, to: :plan_status

  def initialize(user, subscription_type = nil)
    @user                       = user
    if subscription_type.blank? || subscription_type == "Social"
      @plan_status                = Social::PlanStatus.for(user)
      @person_subscription_status = plan_status.subscription_status
    else
      @person_subscription_status = user.subscription_status_for(subscription_type)
    end
    assign_attributes
  end

  def assign_attributes
    return unless person_subscription_status

    @purchase            = user.subscription_purchase_for(person_subscription_status)
    @payment_channel     = purchase.payment_channel if purchase
    @payment_plan        = purchase.subscription_product.product_type if payment_channel
    @canceled_at         = person_subscription_status.canceled_at
    @effective_date      = person_subscription_status.effective_date
    @termination_date    = person_subscription_status.termination_date
  end

  def cancel(note = nil)
    do_cancel(note) ? true : false
  end

  private

  # TODO: Create Note when adding subscription cancellation functionality in new admin tool.
  # A note never actually gets created here, because in both user cancellation and admin cancellation,
  # there is no place to insert a note.

  def do_cancel(note = nil)
    return unless person_subscription_status && person_subscription_status.cancel

    @canceled_at = person_subscription_status.canceled_at
    sub_event = create_subscription_event
    create_cancellation_note(sub_event, note) if note.present?
    notify_user
  end

  def create_subscription_event
    SubscriptionEvent.create(
      {
        subscription_purchase_id: purchase.id,
        person_subscription_status_id: person_subscription_status.id,
        event_type: "Cancellation",
        subscription_type: person_subscription_status.subscription_type
      }
    )
  end

  def create_cancellation_note(sub_event, note)
    Note.create(
      subject: "subscription cancellation",
      note: note,
      related: sub_event,
      note_created_by_id: user.id
    )
  end

  def notify_user
    SocialNotifierWorker.perform_async(:pro_user_cancellation, [user.id])
  end
end
