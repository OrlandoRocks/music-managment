class Subscription::SubscriptionRequest
  extend ActiveModel::Naming
  include ActiveModel::Serialization
  attr_reader :errors, :subscription, :success

  def self.get(user, subscription_type)
    new(user, subscription_type)
  end

  def self.cancel(user, subscription_type, note = nil)
    new(user, subscription_type, note).tap(&:cancel_subscription)
  end

  def initialize(user, subscription_type, note = nil)
    @subscription = Subscription::Subscription.new(user, subscription_type)
    @errors       = ActiveModel::Errors.new(self)
    @note         = note
  end

  def cancel_subscription
    if subscription.cancel(@note)
      @success = true
    else
      @errors.add(:base, "could not cancel subscription")
    end
  end
end
