class Subscription::PlansFetcher
  include ActiveModel::Serialization
  attr_accessor :plans

  def initialize(user)
    @user = user
  end

  def fetch_plans
    @plans = SubscriptionProduct.subscription_products_by_country_website("Social", user.country_website_id)
    fetch_discounts if @plans.any?
  end

  def fetch_discounts
    products = Product.find(@plans.pluck(:product_id))

    @plans.each do |plan|
      product         = products.find { |p| p.id == plan.product_id }
      product_display = ProductDisplay.new(user, product)
      plan.discount   = product_display.as_json if product_display.targeted_product
    end
  end

  private

  attr_reader :user
end
