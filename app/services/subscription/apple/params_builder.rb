class Subscription::Apple::ParamsBuilder
  def self.build(purchase_details, user_id = nil)
    builder = new(purchase_details, user_id)
    builder.build_params
  end

  def initialize(purchase_details, user_id)
    @purchase_details = purchase_details
    @user_id          = user_id
  end

  def build_params
    purchase_details.merge({ person_id: user_id, product_name: "Social", product_type: product_type })
  end

  private

  attr_reader :purchase_details, :user_id

  def product_type
    (purchase_details[:expires_date].to_date > 1.month.from_now.to_date) ? "annually" : "monthly"
  end
end
