class Subscription::Apple::PurchaseVerifier
  VERIFICATION_URL = "/verifyReceipt"

  class VerificationError < StandardError; end

  def self.execute(receipt_blob)
    verifier = new(receipt_blob).tap(&:verify!)
    verifier.verification_details
  end

  attr_reader :verification_details

  def initialize(receipt_blob)
    @receipt_blob = receipt_blob
  end

  def verify_sandbox
    result =
      if @response.code == 21_007
        $sandbox_apple_client.post(VERIFICATION_URL, { "receipt-data" => @receipt_blob })
      else
        raise VerificationError, "#{@response.code}: error status from Apple"
      end

    raise VerificationError, "#{result.code}: error status from Apple" unless result.is_successful

    @verification_details = result.data
  end

  def verify!
    @response = $apple_client.post(VERIFICATION_URL, { "receipt-data" => @receipt_blob })
    if @response.is_successful
      @verification_details = @response.data
    else
      verify_sandbox
    end
  end
end
