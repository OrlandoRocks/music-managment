class Subscription::Apple::PurchaseGenerator
  extend ActiveModel::Callbacks
  extend ActiveModel::Naming

  define_model_callbacks :create_subscription_purchase, only: [:after]

  after_create_subscription_purchase :create_or_update_person_subscription_status
  after_create_subscription_purchase :create_subscription_event
  after_create_subscription_purchase :create_payment_channel_receipt

  attr_reader :subscription_product, :person, :purchase_date, :expires_date, :subscription_purchase, :person_subscription_status, :event_type, :errors, :receipt_data

  def self.generate(subscription_product, person, purchase_date, expires_date, receipt_data)
    new(subscription_product, person, purchase_date, expires_date, receipt_data).tap do |generator|
      generator.create_subscription_purchase unless generator.subscription_purchase_exists
    end
  end

  def initialize(subscription_product, person, purchase_date, expires_date, receipt_data)
    @subscription_product = subscription_product
    @person               = person
    @purchase_date        = purchase_date
    @expires_date         = expires_date
    @receipt_data         = receipt_data
    @errors               = ActiveModel::Errors.new(self)
  end

  def subscription_purchase_exists
    @subscription_purchase = SubscriptionPurchase.where(person_id: person.id, subscription_product_id: subscription_product, termination_date: expires_date).last
    !@subscription_purchase.nil?
  end

  def success?
    !errors.any?
  end

  def create_subscription_purchase
    run_callbacks :create_subscription_purchase do
      unless @subscription_purchase = SubscriptionPurchase.create!(
        {
          subscription_product_id: subscription_product.id,
          payment_channel: "Apple",
          person: person,
          effective_date: purchase_date,
          termination_date: expires_date
        }
      )

        errors.add(:base, "Unable to create subscription purchase.")
      end
    end
  end

  def create_or_update_person_subscription_status
    if subscription_purchase
      @person_subscription_status = subscription_purchase.find_related_person_subscription_status(subscription_product.product_name)
    end
    if person_subscription_status
      update_person_subscription_status
      @event_type = person_subscription_status.canceled_at? ? "Purchase" : "Renewal"
    else
      create_person_subscription_status
      @event_type = "Purchase"
    end
  end

  def create_subscription_event
    subscription_event = SubscriptionEvent.create!(
      {
        subscription_purchase: subscription_purchase,
        person_subscription_status: person_subscription_status,
        subscription_type: subscription_product.product_name,
        event_type: event_type
      }
    ) if person_subscription_status
    errors.add(:base, "Unable to create subscription event.") unless subscription_event
  end

  def create_payment_channel_receipt
    payment_channel_receipt = PaymentChannelReceipt.create!(
      {
        person_id: person.id,
        subscription_purchase_id: subscription_purchase.id,
        receipt_data: receipt_data
      }
    ) if subscription_purchase
    errors.add(:base, "Unable to create payment channel receipt.") unless payment_channel_receipt
  end

  private

  def update_person_subscription_status
    unless person_subscription_status.update(
      {
        effective_date: purchase_date,
        termination_date: expires_date,
        canceled_at: nil
      }
    )

      errors.add(:base, "Unable to update person subscription status.")
    end
  end

  def create_person_subscription_status
    unless @person_subscription_status = PersonSubscriptionStatus.create!(
      {
        person: person,
        effective_date: purchase_date,
        termination_date: expires_date,
        subscription_type: subscription_product.product_name,
        grace_period_length: 0
      }
    )

      errors.add(:base, "Unable to create person subscription status.")
    end
  end
end
