class Subscription::Apple::Renewal < Subscription::Renewal
  def renew
    @data = renewal_data_from_apple
    return unless @data

    @person               = Person.find(receipt.person_id)
    @subscription_product = set_subscription_product
    @generator            = generate_renewal
  end

  def successful?
    @generator && @generator.success? && renewed?
  end

  private

  def receipt
    @receipt ||= @subscription
                 .subscription_events
                 .last.subscription_purchase
                 .payment_channel_receipt
  end

  def renewal_data_from_apple
    purchase_details = Subscription::Apple::PurchaseVerifier.execute(receipt.receipt_data)
    Subscription::Apple::ParamsBuilder.build(purchase_details)
  rescue => e
    Rails.logger.info "Unable to process renewal: #{e.message}"
    Tunecore::Airbrake.notify("Unable to process renewal: #{e.message}")
    false
  end

  def set_subscription_product
    SubscriptionProduct.active_by_product_type_for(
      @person,
      @data[:product_name],
      @data[:product_type]
    )
  end

  def generate_renewal
    Subscription::Apple::PurchaseGenerator.generate(
      @subscription_product,
      @person,
      @data[:purchase_date],
      @data[:expires_date],
      @data[:receipt_data]
    )
  end

  def renewed?
    @generator.subscription_purchase.termination_date > Time.now
  end
end
