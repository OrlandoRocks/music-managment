class Renderforest::Android::PurchaseVerifier
  class VerificationError < StandardError; end

  def purchase_verifier(purchase_token, product_id)
    result = playstore_verifier.verify_product_purchase(
      package_name: ENV["PLAYSTORE_PACKAGE"],
      product_id: product_id,
      token: purchase_token
    )

    raise VerificationError,
          "Error status from Playstore" if result.instance_of? CandyCheck::PlayStore::VerificationFailure

    verifier_details(result, purchase_token, product_id)
  end

  def playstore_verifier
    CandyCheck::PlayStore::Verifier.new(authorization: playstore_config)
  end

  def playstore_config
    CandyCheck::PlayStore.authorization(ANDROID_PURCHASE_JSON_CONFIG)
  end

  def verifier_details(result, purchase_token, product_id)
    purchase_sec = (result.purchase_time_millis.to_f / 1000).to_s
    purchase_date = Date.strptime(purchase_sec, "%s")
    {
      purchase_date: purchase_date,
      purchase_token: purchase_token,
      product_id: product_id
    }
  end
end
