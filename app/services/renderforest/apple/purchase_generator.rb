class Renderforest::Apple::PurchaseGenerator
  include AfterCommitEverywhere
  extend ActiveModel::Naming

  attr_reader :product,
              :person,
              :purchase_date,
              :add_on_purchase,
              :errors,
              :receipt_data

  def self.generate(product, person, purchase_date, receipt_data)
    new(product, person, purchase_date, receipt_data).tap do |generator|
      generator.create_add_on_purchase unless generator.add_on_purchase_exists
    end
  end

  def initialize(product, person, purchase_date, receipt_data)
    @product              = product
    @person               = person
    @purchase_date        = purchase_date
    @receipt_data         = receipt_data
    @errors               = ActiveModel::Errors.new(self)
  end

  def add_on_purchase_exists
    @add_on_purchase = AddOnPurchase.where(add_on_purchase_params).last
    !@add_on_purchase.nil?
  end

  def success?
    errors.none?
  end

  def create_add_on_purchase
    ActiveRecord::Base.transaction(requires_new: true) do
      after_commit { log_error }
      create_purchase
    end
  end

  def create_purchase
    @add_on_purchase = AddOnPurchase.create!(add_on_purchase_params)
  end

  def log_error
    errors.add(:base, "api.tc.message.unable_to_create_add_on_purchase") unless @add_on_purchase
  end

  def add_on_purchase_params
    {
      product_id: product.id,
      payment_channel: "Apple",
      person: person,
      payment_date: purchase_date,
      receipt_data: receipt_data
    }
  end
end
