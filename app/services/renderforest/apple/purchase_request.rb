class Renderforest::Apple::PurchaseRequest < FormObject
  include AfterCommitEverywhere

  attr_accessor :purchase_date, :receipt_data, :product, :person, :purchase_id, :paid_at, :add_on_purchase

  def finalize
    create_purchases
    ActiveRecord::Base.transaction do
      after_commit { handle_purchase }
    end
    handle_finalize
  end

  private

  def handle_finalize
    errors.any? ? false : true
  end

  def handle_purchase
    errors.any? ? rollback_purchases : handle_successful_purchase
  end

  def create_purchases
    @generator = Renderforest::Apple::PurchaseGenerator
                 .generate(product, person, purchase_date, receipt_data)
    if @generator.success?
      @add_on_purchase = @generator.add_on_purchase
    else
      errors.add(:base, "api.tc.message.unable_to_create_purchase")
    end
  end

  def handle_successful_purchase
    @purchase_id = @add_on_purchase.reload.id
    @paid_at = @add_on_purchase.reload.payment_date.strftime("%Y-%m-%d %H:%M:%S")
  end

  def rollback_purchases
    add_on_purchase.destroy if add_on_purchase
  end
end
