# frozen_string_literal: true

class Payoneer::V4::TokenGenerationService
  TOKEN_EXPIRES_IN = 25.days
  CACHE_KEY        = "payoneer/v4/oauth2token"

  def self.fetch_token
    new.fetch_token
  end

  def self.revoke_token
    new.revoke_token
  end

  # Hits the cache, if the key is missing it grabs
  # a new token and returns the new token
  def fetch_token
    Rails.cache.fetch(cache_key, expires_in: expires_in) do
      Payoneer::V4::ApiTokenGenerationClient.request_token do |resp|
        resp[:access_token]
      end
    end
  end

  # Revokes the token on payoneer side, then busts the cache
  def revoke_token
    return :no_token_cached unless token_cached?

    Payoneer::V4::ApiTokenGenerationClient.revoke_token do |resp|
      next unless successful?(resp)

      Rails.cache.delete(cache_key)
    end
  end

  private

  def successful?(resp)
    resp[:status].to_sym == :success
  end

  def token_cached?
    Rails.cache.fetch(cache_key).present?
  end

  def expires_in
    Payoneer::V4::TokenGenerationService::TOKEN_EXPIRES_IN
  end

  def cache_key
    Payoneer::V4::TokenGenerationService::CACHE_KEY
  end
end
