# frozen_string_literal: true

module Payoneer
  class TaxFormUpdaterService
    attr_accessor :program_id

    def initialize(program_id:)
      @program_id = program_id
    end

    def call
      update_tax_forms
    end

    private

    def update_tax_forms
      Payoneer::TaxFormBatchUpdaterWorker.perform_async(program_id)
    end
  end
end
