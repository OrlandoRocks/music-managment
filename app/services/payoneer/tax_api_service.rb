class Payoneer::TaxApiService
  include ArelTableMethods

  attr_accessor :client, :person
  attr_reader :params

  def initialize(params)
    @params = params
    @person = find_person_by_payee_id(params[:PartnerPayeeID])
    program_id = params.delete(:program_id)
    options = { person: @person }
    options[:program_id] = program_id if program_id
    @client = Payoneer::TaxApiClientShim.new(options)
  end

  def has_active_forms?
    tax_forms.present?
  end

  def tax_forms
    return @tax_forms unless @tax_forms.nil?
    return if active_form.nil? || get_tax_form_type.blank?

    @tax_forms = extract_form
  end

  def extract_form
    {
      tax_form_type_id: get_tax_form_type.id,
      submitted_at: parse_payoneer_date(active_form["DateOfSignature"]),
      expires_at: parse_payoneer_date(active_form["ValidUntil"]),
      person_id: person.id,
      provider: person.payout_provider&.name,
      payload: client.formatted_response,
      payout_provider_config_id: client&.payout_provider_config&.id,
      tax_entity_name: active_form["Name"],
    } if active_form.present?
  end

  def active_form
    return @active_form unless @active_form.nil?
    return if client_raw_response["TaxForms"].nil?

    tax_form_response = client_raw_response["TaxForms"]["Form"]
    tax_forms_list =
      Array
      .wrap(tax_form_response)
      .map { |raw_tax_form| Payoneer::APITaxForm.new(raw_tax_form) }
    recent_submission_date = tax_forms_list.max_by(&:submission_date).submission_date_raw
    recent_forms = tax_forms_list.select { |tf| tf.submission_date_raw == recent_submission_date }
    @active_form = pick_valid_form(recent_forms)
  end

  def pick_valid_form(recent_forms)
    valid_form =
      case recent_forms.size
      when proc { |n| n == 1 }
        recent_forms.reverse.find(&:valid?)
      when proc { |n| n > 1 }
        form = recent_forms.reject(&:status_expired?).reverse.find(&:valid?)
        form ||= recent_forms.last if recent_forms.last.w9?
        form
      end
    valid_form&.raw_tax_form
  end

  def client_raw_response
    return @client_raw_response unless @client_raw_response.nil?

    client.post(tax_form_params.merge!(params))
    @client_raw_response = client.formatted_response[tax_form_params[:mname]]
    @client_raw_response
  end

  def tax_form_params
    {
      mname: "GetSubmittedTaxFormsForPayee",
      FromDate: Date.parse("2000-01-01"),
      ToDate: Date.today,
    }
  end

  def find_person_by_payee_id(payee_id)
    Person
      .joins(:payout_providers)
      .joins(:country_website)
      .where(payout_providers: { client_payee_id: payee_id }).first
  end

  def parse_payoneer_date(date)
    Date.strptime(date, "%m/%d/%Y")
  end

  def save_tax_form
    return if tax_forms.blank?

    person.tax_forms.find_or_initialize_by(
      submitted_at: tax_forms[:submitted_at],
      expires_at: tax_forms[:expires_at],
      tax_form_type_id: tax_forms[:tax_form_type_id],
      payout_provider_config_id: tax_forms[:payout_provider_config_id]
    ).update(tax_forms)
  end

  def get_tax_form_type
    # TaxFormType is returned as a name.
    # Possible values: W8BEN, W9 - Individual, W9 – Entity, W8BEN-E, W8ECI - Individual, W8ECI – Entity
    tax_form_type = TaxFormType.find_by(kind: active_form["TaxFormType"])
    return tax_form_type if tax_form_type.present?

    Airbrake.notify(
      "Unknown TaxFormType(#{active_form['TaxFormType']}) received from GetSubmittedTaxFormsForPayee API",
      { person_id: person.id, TaxFormType: active_form["TaxFormType"] }
    )
    nil
  end
end
