# frozen_string_literal: true

class Payoneer::PersonKycBackfillService
  attr_accessor :person, :config, :api_response

  def initialize(person)
    @person = person
    [2, 1].find do |entity_id|
      @config       = payout_provider_configs(entity_id)
      @api_response = payoneer_api
      api_response_success?
    end
  end

  def call!
    notify_airbreak! unless api_response_success?

    ApplicationRecord.transaction do
      person.payout_provider.update!(payout_provider_config: config)
      PayoutProvider::VerifyPayeeComplianceInfo.new(person, provider_payee_details: api_response).call
    end
  end

  private

  def payoneer_api
    PayoutProvider::ApiService
      .new(
        person: person,
        api_client: Payoneer::PayoutApiClientShim.fetch_for(person),
        program_id: config.program_id
      )
      .payee_extended_details(person: person)
  end

  def payout_provider_configs(corporate_entity_id)
    return @payout_provider_configs[corporate_entity_id] unless @payout_provider_configs.nil?

    @payout_provider_configs = {
      1 => PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::TC_US_PROGRAM_ID),
      2 => PayoutProviderConfig.by_env.find_by(program_id: PayoutProviderConfig::BI_US_PROGRAM_ID)
    }
    @payout_provider_configs[corporate_entity_id]
  end

  def notify_airbreak!
    Airbrake.notify(
      "PayoneerKycBackfill::VerifyPersonPayoneerConfigWorker failed, might succeed later!",
      person: person.sanitize!,
      payout_provider_config: config,
      payoneer_api_response: api_response
    )
    raise
  end

  def api_response_success?
    api_response&.success?
  end
end
