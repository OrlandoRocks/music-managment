class Payoneer::APITaxForm
  attr_reader :raw_tax_form

  def initialize(raw_tax_form)
    @raw_tax_form = raw_tax_form
  end

  def status_expired?
    raw_tax_form["TaxFormStatus"] == "Expired"
  end

  def past_expiry_date?
    parse_payoneer_date(raw_tax_form["ValidUntil"]) < Date.today
  end

  def expired?
    status_expired? || past_expiry_date?
  end

  def w9?
    TaxFormType::W9_TYPES.include?(raw_tax_form["TaxFormType"])
  end

  def valid?
    return true if w9?

    !expired?
  end

  def submission_date
    parse_payoneer_date(raw_tax_form["DateOfSignature"])
  end

  def submission_date_raw
    raw_tax_form["DateOfSignature"]
  end

  private

  def parse_payoneer_date(date)
    Date.strptime(date, "%m/%d/%Y")
  end
end
