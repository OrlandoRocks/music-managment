class Payoneer::TaxTokenService
  def self.update(person_id, client = Payoneer::ApiClientShim)
    new(person_id, client).update
  end

  def initialize(person_id, tax_client)
    @person_id         = person_id
    @tax_client        = tax_client.fetch_for(Person.find(person_id)).new
    @token_retry_count = 0
  end

  def update
    tax_client.post({ PartnerPayeeID: person_id, mname: "InitializeTaxFormForPayee" })
    get_token
  rescue Payoneer::V2::ApiClient::ApiError => e
    Tunecore::Airbrake.notify(e)
    false
  end

  def get_token
    raise "cannot find reinitialized token from Payoneer" if @token_retry_count >= 10

    token = make_token_api_request
    return token if token

    @token_retry_count += 1
    get_token
  end

  def make_token_api_request
    sleep(5)
    tax_client.post(
      {
        PartnerPayeeID: person_id,
        mname: "GetPendingTaxFormInvitedPayees",
        fromDate: Date.yesterday.to_s,
        toDate: Date.today.to_s
      }
    )
  end

  private

  attr_reader :person_id, :tax_client
end
