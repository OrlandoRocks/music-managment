class Payoneer::FeeCalculatorService
  EU = "european_union".freeze

  REGIONS = {
    US: "united_states",
    IN: "united_states",
    CA: "canada",
    AU: "australia",
    UK: "united_kingdom",
    FR: EU,
    DE: EU,
    IT: EU,
  }.with_indifferent_access

  PAYOUT_METHODS = {
    ACCOUNT: "payoneer",
    PREPAID_CARD: "payoneer",
    BANK: "payoneer",
    PAYONEER_PARTNER: "payoneer",
    PAYPAL: "paypal",
    PAPER_CHECK: "check"
  }.with_indifferent_access

  attr_reader :domain, :payoneer_account_domain, :withdrawal_type, :withdrawal_amount, :person, :payee_details

  def self.calculate(domain:, payoneer_account_domain:, withdrawal_amount:, withdrawal_type:, person:, payee_details:)
    new(
      domain: domain,
      payoneer_account_domain: payoneer_account_domain,
      withdrawal_amount: withdrawal_amount,
      withdrawal_type: withdrawal_type,
      person: person,
      payee_details: payee_details
    ).calculate
  end

  def initialize(domain:, payoneer_account_domain:, withdrawal_amount:, withdrawal_type:, person:, payee_details:)
    @domain = domain
    @person = person
    @payee_details = payee_details
    @payoneer_account_domain = payoneer_account_domain
    @withdrawal_amount = withdrawal_amount
    @withdrawal_type = withdrawal_type
    @payout_program = PayoutProgram.where(
      payout_type: PAYOUT_METHODS[withdrawal_type],
      region: REGIONS[domain],
      inside_region: domain == payoneer_account_domain
    ).first

    validate_args
  end

  def calculate
    if PayoutTransfer::BANK.casecmp?(@withdrawal_type)
      PayoneerAchFee.get_fee_cents(@domain, person, payee_details)
    else
      return if @payout_program.try(:fee_type).blank?

      @payout_program.calculate_fee(withdrawal_amount)
    end
  end

  private

  def validate_args
    if REGIONS[@domain].blank?
      Airbrake.notify(
        "Domain (#{@domain}) not supported.",
        { domain: @domain, payoneer_account_domain: @payoneer_account_domain }
      )
    end

    return if PAYOUT_METHODS[@withdrawal_type].present?

    Airbrake.notify(
      "Withdrawal method (#{@withdrawal_type}) not supported.",
      { withdrawal_type: @withdrawal_type }
    )
  end
end
