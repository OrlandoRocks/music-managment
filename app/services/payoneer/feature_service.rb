class Payoneer::FeatureService
  def self.payoneer_payout_enabled?(user)
    new(user).payoneer_payout_enabled?
  end

  def self.payoneer_opted_out?(user)
    new(user).payoneer_opted_out?
  end

  def self.enabled_and_active?(user)
    new(user).enabled_and_active?
  end

  def self.payoneer_redesigned_onboarding?(user)
    FeatureFlipper.show_feature?(:payoneer_redesigned_onboarding, user)
  end

  def self.payoneer_currency_dropdown_enabled?(user)
    new(user).payoneer_currency_dropdown_enabled?
  end

  def self.show_countries_selection?(user)
    new(user).show_countries_selection?
  end

  def self.payoneer_exclude_tax_check?(user)
    new(user).payoneer_exclude_tax_check?
  end

  def self.show_eft_and_check?(user)
    new(user).show_eft_and_check?
  end

  def self.bi_transfer?(user)
    new(user).bi_transfer?
  end

  def self.secondary_taxform_submission?(user)
    new(user).secondary_taxform_submission?
  end

  def initialize(user)
    @user = user
  end

  def enabled_and_active?
    payoneer_payout_enabled? && !payoneer_opted_out?
  end

  def show_eft_and_check?
    FeatureFlipper.show_feature?(:payoneer_show_eft_and_check, @user)
  end

  def payoneer_payout_enabled?
    FeatureFlipper.show_feature?(:payoneer_payout, @user)
  end

  def payoneer_opted_out?
    FeatureFlipper.show_feature?(:payoneer_opt_out, @user)
  end

  def payoneer_currency_dropdown_enabled?
    FeatureFlipper.show_feature?(
      :payoneer_payout_usa_withdrawal_currency_dropdown,
      @user
    ) && @user.country_website_id != CountryWebsite::INDIA
  end

  def bi_transfer?
    FeatureFlipper.show_feature?(:bi_transfer, @user)
  end

  def show_countries_selection?
    bi_transfer? && (@user.has_no_countries? || @user.has_multiple_countries? || @user.has_invalid_country?)
  end

  def payoneer_exclude_tax_check?
    FeatureFlipper.show_feature?(:payoneer_exclude_tax_check, @user)
  end

  def secondary_taxform_submission?
    FeatureFlipper.show_feature?(:secondary_taxform_submission, @user)
  end
end
