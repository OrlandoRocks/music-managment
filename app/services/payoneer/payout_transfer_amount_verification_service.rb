# frozen_string_literal: true

class Payoneer::PayoutTransferAmountVerificationService
  attr_accessor :payout_transfer, :amount, :referrer_url

  def self.verify(**kwargs)
    new(**kwargs).verify
  end

  def initialize(payout_transfer:, amount:, referrer_url: nil)
    @payout_transfer = payout_transfer
    @amount = amount
    @referrer_url = referrer_url
  end

  def verify
    notify_airbrake_of_mismatch if amount_mismatch?

    !amount_mismatch?
  end

  private

  def notify_airbrake_of_mismatch
    Rails.logger.warn "Payout transfer amount mismatch found for #{payout_transfer.person.id}"

    Tunecore::Airbrake.notify(
      "Payout Transfer Amount Mismatch",
      {
        person_id: payout_transfer.person.id,
        transfer_id: payout_transfer.id,
        url: referrer_url,
        requested_amount: payout_transfer.requested_amount.to_s,
        compared_amount: amount
      }
    )
  end

  def amount_mismatch?
    return @amount_mismatch if @amount_mismatch.present?
    return if payout_transfer.blank? || amount.blank?

    requested_amount_in_cents_from_db = payout_transfer.amount_cents
    requested_amount_in_cents_from_request = Integer(BigDecimal(amount, 16) * 100)
    difference_amount_in_cents = (requested_amount_in_cents_from_request - requested_amount_in_cents_from_db).abs
    @amount_mismatch = difference_amount_in_cents > payout_transfer.fee_cents
  end
end
