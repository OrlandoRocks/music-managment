# frozen_string_literal: true

class Payoneer::DisassociatePayoneerService
  attr_accessor :person, :payout_providers

  def initialize(person)
    @person = person
    @payout_providers = person.payout_providers
    disassociate_accounts
  end

  private

  def disassociate_accounts
    payout_providers.each(&:disassociate_account)
  end
end
