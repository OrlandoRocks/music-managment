class Payoneer::BatchActionsService
  def self.send_actions(action:, transfer_ids:, person:, referrer_url: nil)
    new(action: action, transfer_ids: transfer_ids, person: person, referrer_url: referrer_url).send_actions
  end

  def initialize(action:, transfer_ids:, person:, referrer_url:)
    @action = action
    @transfer_ids = transfer_ids
    @person = person
    @referrer_url = referrer_url
  end

  attr_accessor :action, :transfer_ids, :person, :errors, :payout_transfers, :referrer_url, :completed_payout_transfers

  def send_actions
    load_payout_transfers

    form_type = (action == "approve") ? PayoutTransfer::ApprovalForm : PayoutTransfer::RejectionForm
    @errors = []
    @completed_payout_transfers = []

    payout_transfers.each do |transfer|
      form_options = { payout_transfer: transfer, admin: person }
      form_options[:referrer_url] = referrer_url if action == "approve"
      form = form_type.new(form_options)
      form.save
      form.errors.present? ? errors << form.errors.full_messages.join("&") : completed_payout_transfers << transfer.id
    end
    log_human_carried_out_payout_transfers
    errors
  end

  private

  def load_payout_transfers
    @payout_transfers ||= PayoutTransfer
                          .includes(person: :payout_providers)
                          .where(id: transfer_ids)
  end

  def log_human_carried_out_payout_transfers
    Rails.logger.info(
      "Topic: PayoutTransfers, Person => #{person.id},
      #{action_type} the following payout_transfers:
      (#{completed_payout_transfers.join(', ')})"
    )
  end

  def action_type
    (action == "approve") ? "Approved" : "Rejected"
  end
end
