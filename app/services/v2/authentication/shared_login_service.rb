# frozen_string_literal: true

# Retrieve and validate a nonce.
# The intention of this flow is to be more secure than reading a cookie without a CSRF header.
#
# Step 1: Check for V2 cookie here.
# Step 2: Send user to V2, so that they can return with a nonce from V2 localStorage.
# Step 3: Read and destroy nonce, then attempt authentication.
#
# If V2 ends up living on the same subdomain as legacy, then the redirects are unnecessary,
# and this can be refactored to simply read the token from localStorage.
#
class V2::Authentication::SharedLoginService
  include V2::JwtHelper
  include SignInCallback

  BASE_URL          = ENV.fetch("V2_BASE_URL")
  PERMITTED_REFERER = URI.parse(BASE_URL).host
  LOGIN_URL         = "#{BASE_URL}/login"

  def initialize(request)
    @request = request
    @cookies = request.cookies
    @params  = request.params
    @session = request.session

    login if permitted_login_request?
  end

  attr_accessor :person_id
  attr_reader :cookies, :params, :request, :session

  def redirect_to_v2?
    return false if login_success?
    return false unless redirectable_path?

    v2_login_cookie? && v2_login_live?
  end

  def v2_url
    if nonce.present?
      # Login attempt has just failed
      LOGIN_URL
    else
      # Attempt shared login
      "#{BASE_URL}/login-receiver?returnTo=#{return_to_url}"
    end
  end

  private

  # Exchange an unused V2 nonce for authentication.
  def login
    return unless redirectable_path?
    return if nonce.blank?
    return unless v2_login_live?
    return unless permitted_nonce?(nonce, id)

    # This is sufficient to trigger the legacy auth flow in AccountSystem#current_user.
    session[:person] = id
    @person_id       = id

    nil
  end

  def permitted_login_request?
    return false if nonce.blank?
    return false unless v2_login_live?
    return false unless v2_login_cookie?
    return false unless permitted_nonce?(nonce, id)
    return false if referer.blank?

    permitted_referer?(referer)
  end

  def referer
    request.referrer
  end

  def permitted_referer?(referer)
    host = URI(referer).host

    host == PERMITTED_REFERER
  end

  def login_success?
    person_id.present?
  end

  def id
    return if params[:id].nil?

    Integer(params[:id], 10)
  end

  def nonce
    params[:nonce]
  end

  def v2_login_live?
    FeatureFlipper.show_feature?(:v2_login)
  end

  # Step 1
  # The user logged-in via V2 and might still have an active session,
  # but we shouldn't check for a session yet because we don't have a nonce.
  def v2_login_cookie?
    cookies["v2_login"] == "true"
  end

  # Step 2
  # Go to V2, which might pass the user back to us with nonce and id params.

  # Provide a return URL for basic validation by V2
  def return_to_url
    request.fullpath
  end

  def redirectable_path?
    ["/login", "/logout"].exclude?(request.path)
  end

  # Step 3
  # permitted_nonce? via V2::JwtHelper
end
