##
# Contains logic to be called after a user successfully authenticates.
# This is shared between v2 and legacy auth flows.
class V2::Authentication::SharedSignInSuccessCallback
  include FeatureFlag
  include PlansRedirector
  include ReferralData::Parsable
  include SsoCookies

  def self.call(*args)
    new(*args).call
  end

  def initialize(person, request, cookies, params)
    @person = person
    @current_user = person
    @request = request
    @cookies = cookies
    @params = params
  end

  def call
    reset_plans_redirect(person)
    create_login_event
    save_login_info_to_person
    add_sso_cookie(for_user_id: person.id)
    create_referral_data(person, referral_attributes)
    create_active_session_cookie
    clear_referral_cookies
    retrieve_publishing_data
    reconcile_tier_eligibility
  end

  private

  attr_reader :cookies
  attr_reader :current_user
  attr_reader :params
  attr_reader :person
  attr_reader :request

  def create_login_event
    # TODO: Should we use request.headers["Client-Remote-IP"] insetad? what's
    # the difference?
    person.create_logged_in_event(request.remote_ip, request.user_agent)
  end

  def save_login_info_to_person
    person.update!(
      {
        recent_login: Time.zone.now,
        dormant: 0, # TODO: do we need dormant, or is it okay to call person.record_login?
        last_logged_in_ip: request.remote_ip
      }
    )
  end

  def create_active_session_cookie
    return unless person

    cookie_data = Gtm::Page.new(
      person,
      page_type: Gtm::Cookie,
      currency: person.person_balance.currency
    ).user_data.to_json

    cookies[:tc_active_session] = {
      domain: COOKIE_DOMAIN,
      value: SessionEncryptionEngine.encrypt(cookie_data).to_s
    }
  end

  def retrieve_publishing_data
    RetrievePublishingData.call(person, request, params)
  end

  def reconcile_tier_eligibility
    RewardSystem::TierEligibilityReconcileWorker.perform_async(person.id)
  end
end
