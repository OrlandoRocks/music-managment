# frozen_string_literal: true

class V2::Authentication::TfaResendService
  include V2::Authentication::Helpers

  RESEND = "resend"

  def self.call(params:)
    new(params).call
  end

  def initialize(params)
    @params = params
  end

  attr_reader :params, :person

  def call
    @person = authenticate
    return unless verified_person?

    # TODO: This is called blindly here and in legacy,
    # but this can fail if too many codes were issued to the same user in the last hour.
    # This should be addressed in the new 2FA service implementation.
    # Alternatively, existing Authy impl can be refactored to support error messaging of this case.
    if FeatureFlipper.show_feature?(:verify_2fa, person)
      TwoFactorAuth::ApiClient.new(@person).request_authorization
    else
      TwoFactorAuth::AuthyApiClient.new(@person).request_authorization
    end
    log_new_code_sent(person, RESEND)

    { tfa_session_token: create_tfa_session_token }
  end

  private

  def authenticate
    tfa_session_token_user
  end
end
