# frozen_string_literal: true

class V2::Authentication::RefreshTokenService
  include V2::JwtHelper

  def self.refresh(request)
    new(request).refresh
  end

  def initialize(request)
    @request = request
  end

  attr_reader :request

  def refresh
    return if token_person.nil?

    duration = V2::JwtHelper::EXPIRY_DURATION
    jwt      = generate_jwt(payload: token_person, duration: duration)
    save_token_digest(jwt, duration, token_person[:id])

    nonce = generate_nonce
    save_nonce(nonce, duration, token_person[:id])

    { jwt: jwt, nonce: nonce }
  end

  private

  def token_person
    @token_person ||= decoded
  end

  def decoded
    result = decode_header_token.with_indifferent_access
    return if result.blank?

    {
      id: result[:id],
      email: result[:email],
      name: result[:name],
      roles: result[:roles]
    }
  end
end
