# frozen_string_literal: true

# TFA users, either:
#   - Auth via TFA code
#   - Issue TFA session token
# Non-TFA users:
#   - Auth via email and password
class V2::Authentication::AuthenticationService
  include V2::Authentication::Helpers

  TFA_ERROR = "tfa error"

  def self.call(params:, cookies:, request:)
    new(params, cookies, request).call
  end

  def initialize(params, cookies, request)
    @params  = params
    @cookies = cookies
    @request = request
  end

  attr_reader :cookies, :params, :person, :request

  delegate :auth_code?, to: V2::Authentication::TFAService

  def call
    @person = authenticate
    return error_result unless verified_person?

    tfa.send_auth_code and return tfa_required_result if tfa.new_auth_code_required?

    # captcha_on_login(handle_v3_recaptcha) if use_recaptcha?

    run_side_effects

    auth_result
  end

  private

  # Results

  def auth_result
    jwt = generate_jwt(payload: jwt_payload, duration: V2::JwtHelper::EXPIRY_DURATION)
    save_token_digest(jwt, V2::JwtHelper::EXPIRY_DURATION, person.id)

    nonce = generate_nonce
    save_nonce(nonce, V2::JwtHelper::EXPIRY_DURATION, person.id)

    build_result(jwt: jwt, nonce: nonce)
  end

  def error_result
    result = @tfa_check&.error_msg || TFA_ERROR
    build_result(errors: result)
  end

  def tfa_required_result
    build_result(tfa_required: true, tfa_session_token: create_tfa_session_token)
  end

  def build_result(
    errors: Mutations::Base::NO_ERRORS,
    jwt: nil,
    nonce: nil,
    tfa_required: false,
    tfa_session_token: nil
  )
    {
      errors: errors,
      jwt: jwt,
      nonce: nonce,
      tfa_required: tfa_required,
      tfa_session_token: tfa_session_token
    }
  end

  # Authenticate via TFA or email/pass
  def authenticate
    return tfa.checked_person if auth_code?(params)

    email    = params[:email]
    password = params[:password]
    return unless email.present? && password.present?

    Person.authenticate({ email: email, password: password })
  end

  def verified_person?
    person.present? && person.is_verified?
  end

  def jwt_payload
    {
      id: person.id,
      email: person.email,
      name: person.name,
    }
  end

  def run_side_effects
    V2::Authentication::SharedSignInSuccessCallback.call(
      person,
      request,
      cookies,
      params
    )
  end

  def update_person_login
    person.update(recent_login: Time.zone.now, dormant: 0, last_logged_in_ip: request.remote_ip)
  end

  def tfa
    @tfa ||= V2::Authentication::TFAService.new(params, request, person)
  end
end
