class V2::Authentication::SharedSignInSuccessCallback
  class RetrievePublishingData
    include CountryDomainHelper
    include FeatureFlag

    def self.call(*args)
      new(*args).call
    end

    def initialize(person, request, params)
      @person = person
      @params = params

      # CountryDomainHelper requires these things to be set, and we figure it's
      # better to use the existing helper than to create a new one that doesn't
      # require current_user or session
      @current_user = person
      @session = {}
      @request = request
    end

    def call
      rights_app_enabled = feature_enabled?(:rights_app, country_website_code: country_website)
      # TODO: Since all accounts must have account_type:
      # publishing_administration, could we just check for account presence?
      has_publishing_administration = person.accounts&.publishing_administration&.exists?
      account_blocked = !rights_app_enabled || !has_publishing_administration
      return if account_blocked

      account_id = person.account.id
      PublishingAdministration::PublishingIngestionBlockerWorker.perform_async(account_id)
    end

    private

    attr_reader :current_user
    attr_reader :params
    attr_reader :person
    attr_reader :request
    attr_reader :session
  end
end
