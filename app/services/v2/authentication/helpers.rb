# frozen_string_literal: true

module V2::Authentication::Helpers
  include V2::JwtHelper

  def create_tfa_session_token
    generate_jwt(payload: { person_id: person.id }, duration: TwoFactorAuth::AUTHENTICATION_WINDOW)
  end

  def tfa_session_token_user
    return @tfa_session_token_user if defined?(@tfa_session_token_user)

    @tfa_session_token_user = nil
    decoded                 = decode_jwt(params[:tfa_session_token])
    return if decoded.empty?

    @tfa_session_token_user = Person.find(decoded["person_id"])
  end

  def verified_person?
    person.present? && person.is_verified?
  end

  def log_new_code_sent(person, type)
    TwoFactorAuthEvent.create(
      type: "authentication",
      page: "v2",
      action: type,
      successful: true,
      two_factor_auth_id: person.two_factor_auth.id
    )
  end
end
