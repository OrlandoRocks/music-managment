# frozen_string_literal: true

class V2::Authentication::TFAService
  include V2::Authentication::Helpers

  SEND = "send"

  def self.auth_code?(params)
    params[:auth_code].present?
  end

  def initialize(params, request, unchecked_person)
    @params = params
    @request = request
    @unchecked_person = unchecked_person
  end

  attr_reader :params, :request, :unchecked_person

  delegate :auth_code?, to: :class

  def checked_person
    return @checked_person if defined?(@checked_person)

    @checked_person = nil
    return if tfa_session_token_user.nil?

    @checked_person = tfa_session_token_user if auth_code_check(tfa_session_token_user).successful?
  end

  def new_auth_code_required?
    return false if auth_code?(params)
    return false if under_admin_control?
    return false unless tfa_feature_enabled?
    return false unless tfa_user?

    tfa_expired?
  end

  def send_auth_code
    result = TwoFactorAuth::ApiClient.new(unchecked_person).request_authorization
    log_new_code_sent(unchecked_person, SEND)

    result
  end

  private

  def tfa_session_token_user
    return @tfa_session_token_user if defined?(@tfa_session_token_user)

    @tfa_session_token_user = nil
    decoded                 = decode_jwt(params[:tfa_session_token])
    return if decoded.empty?

    @tfa_session_token_user = Person.find(decoded["person_id"])
  end

  def under_admin_control?
    request.session[:admin].present? && request.session[:admin] != unchecked_person.id
  end

  def tfa_feature_enabled?
    ENV["TWO_FACTOR_AUTHENTICATION_ENABLED"].to_s == "true"
  end

  def tfa_record
    @tfa_record ||= unchecked_person.two_factor_auth
  end

  def tfa_user?
    tfa_record.present? && tfa_record.active?
  end

  def tfa_expired?
    return false if tfa_record.nil?

    tfa_record.within_authentication_window? == false
  end

  def log_auth_check_result(person, result)
    if result.successful?
      person.two_factor_auth.auth_success!
    else
      person.two_factor_auth.auth_failure!
    end
  end

  # API Helpers

  def auth_code_check(person)
    result = TwoFactorAuth::ApiClient.new(person).submit_auth_code(params[:auth_code])
    log_auth_check_result(person, result)

    result
  end
end
