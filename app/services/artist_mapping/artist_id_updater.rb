class ArtistMapping::ArtistIdUpdater
  def self.update_artist_id(options)
    "#{options['service_name'].classify}::ArtistIdUpdater".constantize.update_artist_id(options)
  end
end
