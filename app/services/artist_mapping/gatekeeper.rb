class ArtistMapping::Gatekeeper
  def initialize(options)
    @identifier   = options["identifier"]
    @person_id    = options["person_id"]
    @artist_id    = options["artist_id"]
    @service_name = options["service_name"]
  end

  def can_proceed?
    true unless external_service_id_requested? && creating? || redistribution_pending? && updating?
  end

  def updating?
    identifier.present?
  end

  def creating?
    !updating?
  end

  private

  attr_reader :identifier, :person_id, :artist_id, :service_name

  def external_service_id_requested?
    ExternalServiceId.artist_ids_for(person_id, artist_id, service_name).first.requested?
  end

  def redistribution_pending?
    RedistributionPackage.for(person_id, artist_id, service_name).try(:pending?)
  end
end
