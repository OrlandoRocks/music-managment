class ArtistMapping::TicketCreator
  def self.create_ticket(options)
    "#{options['service_name'].classify}::TicketCreator".constantize.create_ticket(options)
  end
end
