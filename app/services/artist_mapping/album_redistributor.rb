class ArtistMapping::AlbumRedistributor
  def self.redistribute(options)
    new(options).tap(&:redistribute)
  end

  def initialize(options)
    @options       = options
    @person_id     = options["person_id"]
    @artist_id     = options["artist_id"]
    @identifier    = options["identifier"]
    @service_name  = options["service_name"]
  end

  def redistribute
    create_or_update_redistribution_package
    retry_distributions
  end

  private

  def create_or_update_redistribution_package
    @redistribution_package = RedistributionPackage.find_or_create_by(artist_id: @artist_id, person_id: @person_id, converter_class: RedistributionPackage::STORE_CONVERTER_CLASS_MAP[@service_name])
    @redistribution_package.update(state: "pending") unless @redistribution_package.pending?
  end

  def retry_distributions
    @redistribution_package.distributions.each do |distro|
      Delivery::Retry.retry(delivery: distro, delivery_type: "metadata_only", actor: "Artist ID Mapping Wizard")
    end
  end
end
