class ArtistMapping::ArtistIdCreator
  def self.create_artist_id(options)
    new(options).tap(&:create_artist_id)
  end

  def initialize(options)
    @is_duplicate     = options["is_duplicate"]
    @artist_id        = options["artist_id"]
    @person_id        = options["person_id"]
    @service_name     = options["service_name"]
  end

  def create_artist_id
    submit_to_store
    update_external_service_ids_status
  rescue
    ExternalServiceId.artist_ids_for(@person_id, @artist_id, @service_name).update_all(state: "error")
  end

  private

  def update_external_service_ids_status
    ExternalServiceId.artist_ids_for(@person_id, @artist_id, @service_name).update_all(state: "requested")
  end

  def submit_to_store
    "#{@service_name.classify}::ArtistIdCreator".constantize.create_artist_id(@person_id, @artist_id, @is_duplicate)
  end
end
