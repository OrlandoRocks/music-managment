class AssetCleanupService
  def initialize(key:, bucket:)
    @key = key
    @bucket = bucket
  end

  def call
    AwsWrapper::S3.delete(key: @key, bucket: @bucket)
  end
end
