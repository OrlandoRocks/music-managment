class BackfillGoliveDateService
  def initialize(verbose: false)
    @verbose = verbose
  end

  def run
    albums.find_each do |album|
      run_on_album(album)
    end
  end

  def display_count
    puts "Number of album with golive_date nil : `#{count}`."
  end

  delegate :count, to: :albums

  def run_on_album(album)
    return unless album.golive_date.nil? && album.sale_date.present?

    success = album.update_columns(
      golive_date: parse_from_us_canada(album.sale_date.to_s),
      timed_release_timing_scenario: "relative_time"
    )
    log("album id : #{album.id}, sale_date:#{album.sale_date}, golive_date:#{date_format(album.golive_date)}, #{success ? 'update success' : 'error'}")
  end

  private

  def albums
    Album.where(golive_date: nil)
  end

  def parse_from_us_canada(date_str)
    ActiveSupport::TimeZone["Eastern Time (US & Canada)"].parse(date_str)
  end

  def date_format(date)
    date.localtime.strftime("%Hh%M %Y-%m-%d")
  end

  def log(message)
    return unless @verbose

    puts message
  end
end
