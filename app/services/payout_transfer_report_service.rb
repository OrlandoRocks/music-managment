# frozen_string_literal: true

class PayoutTransferReportService
  include CurrencyHelper
  include AdminPaymentHelper

  PAYONEER_REPORT_HEADERS = [
    "Payout Transfer ID",
    "Person ID",
    "Person Email",
    "Customer Name",
    "Company Name",
    "Address",
    "Country Website",
    "Withdrawal Method",
    "Tunecore Status",
    "Payout Provider Status",
    "First Time",
    "Payoneer KYC",
    "Withdrawal Amount",
    "Withdrawal Date",
    "Available Balance",
    "VIP Status",
    "2FA Enabled",
    "Multiple Linked Accounts",
    "Request Auto Approved",
    "Request Manually Approved",
    "Auto-Approval Assessable",
    "Auto-Approval Successful"
  ]

  PAYPAL_REPORT_HEADERS = [
    "Person ID",
    "Customer Name",
    "Customer Email",
    "Payee Email",
    "Company Name",
    "Address",
    "Country Website",
    "Withdrawal Method",
    "Payoneer KYC",
    "Withdrawal Amount",
    "Withdrawal Date",
    "Available Balance",
    "VIP Status",
    "2FA Enabled",
    "Multiple Linked Accounts",
    "Request Auto Approved",
    "Request Manual Approved"
  ]

  attr_accessor :payout_provider

  def initialize(provider)
    @payout_provider = provider
  end

  def self.generate_report_csv(provider, query_params)
    return new(provider).generate_payoneer_transfers_report(query_params) if provider == PayoutProvider::PAYONEER

    new(provider).generate_paypal_transfers_report(query_params)
  end

  def generate_payoneer_transfers_report(query_params)
    report_result = PayoutTransfer::AdminFilterForm.new(query_params)
    report_result.save(paginated: false)
    @approvals_map = report_result.approvals_map
    payoneer_build_report_csv(report_result.payout_transfers)
  end

  def generate_paypal_transfers_report(query_params)
    response = Admin::PaymentService.fetch_transfers(query_params)
    paypal_build_report_csv(response[:transfers])
  end

  private_class_method :new

  private

  def payoneer_build_report_csv(payout_transfer_records)
    report_file = Tempfile.new([generate_report_filename, ".csv"])
    report_file << CSV.generate_line(PAYONEER_REPORT_HEADERS)
    payout_transfer_records
      .eager_load(payout_provider: { person: [:compliance_info_fields, :two_factor_auth] })
      .map do |report_record|
        report_file << payoneer_build_report_record(report_record)
      end

    report_file.tap(&:rewind)
  end

  def payoneer_build_report_record(payout_record)
    CSV.generate_line(
      [
        payout_record.id,
        payout_record.payout_provider.person_id,
        payout_record.payout_provider.person.email,
        handle_compliance_info_name(payout_record),
        handle_compliance_info_company_name(payout_record),
        handle_person_address(payout_record),
        payout_record.payout_provider.person.country_website_id,
        payout_record.withdraw_method,
        payout_record.tunecore_status,
        payout_record.provider_status,
        handle_first_time_check(payout_record),
        handle_person_kyc_status(payout_record),
        handle_withdrawal_amount(payout_record),
        handle_withdrawal_date(payout_record),
        handle_availale_balance(payout_record),
        payout_record.payout_provider.person.vip,
        handle_person_2fa_status(payout_record),
        handle_multiple_linked_accounts_status(payout_record),
        handle_request_auto_approved(payout_record),
        handle_request_manually_approved(payout_record),
        payout_record.auto_approval_eligible? ? "Yes" : "No",
        auto_approval_successful(payout_record)
      ]
    )
  end

  def handle_request_auto_approved(payout_record)
    return "N/A" if payout_record.transfer_metadatum.blank?

    ActiveRecord::Type::Boolean.new.cast(payout_record.transfer_metadatum.auto_approved)
  end

  def handle_first_time_check(payout_record)
    is_first_time_payout = !ActiveRecord::Type::Boolean.new.cast(
      @approvals_map[payout_record.payout_provider&.id].to_i
    )
    is_first_time_payout ? "Yes" : "No"
  end

  def handle_request_manually_approved(payout_record)
    return "N/A" if payout_record.transfer_metadatum.blank?

    !ActiveRecord::Type::Boolean.new.cast(payout_record.transfer_metadatum.auto_approved)
  end

  def handle_person_address(payout_record)
    fetch_person(payout_record).slice(:address1, :address2, :city, :state, :country, :zip).values.join(", ")
  end

  def handle_compliance_info_name(payout_record)
    record_compliance_info(payout_record).slice(:first_name, :last_name).values.join(" ")
  end

  def handle_compliance_info_company_name(payout_record)
    record_compliance_info(payout_record).fetch(:company_name, "")
  end

  def handle_person_2fa_status(payout_record)
    payout_record.payout_provider.person.two_factor_auth&.active?
  end

  def handle_person_kyc_status(payout_record)
    payout_record.payout_provider.person.address_locked?
  end

  def fetch_person(payout_record)
    payout_record.respond_to?(:payout_provider) ? payout_record.payout_provider.person : payout_record.person
  end

  def record_compliance_info(payout_record)
    fetch_person(payout_record)
      .compliance_info_fields
      .pluck(:field_name, :field_value)
      .to_h.with_indifferent_access
  end

  def handle_multiple_linked_accounts_status(payout_record)
    PayoutProvider
      .where.not(person_id: payout_record.payout_provider.person_id)
      .where(client_payee_id: payout_record.payout_provider.client_payee_id)
      .any?
  end

  def handle_withdrawal_amount(payout_record)
    money_to_currency(payout_record.dollar_amount)
  end

  def handle_availale_balance(payout_record)
    money_to_currency(Money.new(fetch_person(payout_record).available_balance_cents), suppress_iso: true)
  end

  def handle_withdrawal_date(payout_record)
    payout_record.created_at.strftime("%m/%d/%Y %I:%M%p %Z")
  end

  def auto_approval_successful(payout_record)
    return "N/A" unless payout_record.auto_approval_eligible?

    payout_record.auto_approved? ? "Yes" : "No"
  end

  def paypal_build_report_csv(transfers_records)
    report_file = Tempfile.new([generate_report_filename, ".csv"])
    report_file << CSV.generate_line(PAYPAL_REPORT_HEADERS)
    transfers_records
      .eager_load(person: [:compliance_info_fields, :two_factor_auth])
      .map do |transfer_record|
        report_file << paypal_build_report_record(transfer_record)
      end

    report_file.tap(&:rewind)
  end

  def paypal_build_report_record(transfer_record)
    CSV.generate_line(
      [
        transfer_record.person_id,
        handle_compliance_info_name(transfer_record),
        transfer_record.person.email,
        transfer_payee(transfer_record),
        handle_compliance_info_company_name(transfer_record),
        handle_person_address(transfer_record),
        transfer_record.person.country_website_id,
        "PayPal",
        transfer_record.person.address_locked?,
        money_to_currency(Money.new(transfer_record.payment_cents), suppress_iso: true),
        handle_withdrawal_date(transfer_record),
        handle_availale_balance(transfer_record),
        transfer_record.person.vip,
        transfer_record.person.two_factor_auth&.active?,
        "N/A",
        handle_request_auto_approved(transfer_record),
        handle_request_manually_approved(transfer_record)
      ]
    )
  end

  def generate_report_filename
    "#{@payout_provider}_payout_transfers_#{DateTime.now.to_i}"
  end
end
