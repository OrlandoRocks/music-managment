class SongIsrcSearch
  def self.search(isrcs)
    new(isrcs).search
  end

  attr_accessor :isrcs

  def initialize(isrcs)
    @isrcs = isrcs
  end

  def search
    format_isrcs
    find_songs
  end

  private

  def format_isrcs
    return unless isrcs.instance_of?(String)

    @isrcs = (isrcs.include?(",") ? isrcs.split(",") : isrcs.split).map(&:strip)
  end

  def find_songs
    Song.where(Song.arel_table[:tunecore_isrc].in(isrcs).or(Song.arel_table[:optional_isrc].in(isrcs)))
  end
end
