class TransactionalEmail::Base
  include Rails.application.routes.url_helpers

  attr_accessor :client, :body

  def self.deliver(*args)
    new(args.first).deliver
  end

  def initialize(args = {})
    @logger        = Rails.logger
    @client        = $hubspot_email_api_client
    @should_raise  = args.fetch(:should_raise, true)
  end

  def deliver
    resp =
      client.post do |req|
        req.body = body.to_json
      end
    log_response(resp)
    resp
  end

  def format_request_props(props = {})
    contact_props = []
    props.each { |k, v| contact_props << { name: k.to_s, value: v } }
    contact_props
  end

  def log_response(response)
    body = JSON.parse(response.body)
    @logger.info("HUBSPOT: #{body['sendResult']}: #{body['message']}")
    raise body["message"] if @should_raise && response.status > 400
  end
end
