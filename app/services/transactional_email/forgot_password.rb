class TransactionalEmail::ForgotPassword < TransactionalEmail::Base
  EMAIL_ID = 4_236_217_914

  def initialize(person, options = {})
    custom_props = format_request_props(reset_password_url: options[:url])

    @body =
      {
        emailId: EMAIL_ID,
        message: {
          to: person.email,
        },
        customProperties: custom_props
      }

    super(options)
  end
end
