class Currencylayer::ExchangeService
  def store_exchange_rates(source_currency, target_currencies)
    api_client = Currencylayer::ApiClient.new
    forex_data = api_client.get_forex(source_currency, target_currencies)

    if (forex_data["success"])
      timestamp = Time.at(forex_data["timestamp"])
      quotes = parse_quotes(forex_data["quotes"], source_currency, target_currencies)
      quotes.each do |source_currency:, target_currency:, exchange_rate:|
        ForeignExchangeRate.create(
          source_currency: source_currency,
          target_currency: target_currency,
          exchange_rate: exchange_rate,
          valid_from: timestamp
        )
      end
    else
      Tunecore::Airbrake.notify(
        "Call to currencylayer failed",
        {
          source_currency: source_currency,
          target_currency: target_currencies,
          response: forex_data
        }
      )
    end
  end

  private

  def parse_quotes(quotes_map, source_currency, target_currencies)
    Currencylayer::QuotesFormatter
      .call!(quotes_map, source_currency, target_currencies.split(","))
  end
end
