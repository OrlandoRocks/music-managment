# frozen_string_literal: true

module Currencylayer
  class CallFailedError < StandardError
    DEFAULT_ERR_MSG = "Call to currency layer failed"

    attr_reader :response

    def initialize(msg: DEFAULT_ERR_MSG, response: nil)
      @response = response
      super(msg)
    end
  end
end
