module Currencylayer
  class StoreExchangeRates
    class InvalidRateError < StandardError; end

    def self.call!(rate_fetcher_klass, **opts)
      new(rate_fetcher_klass, opts).store!
    end

    def initialize(rate_fetcher_klass, **opts)
      @rate_fetcher_klass = rate_fetcher_klass
      @opts = opts
      @response = fetch_from_currencylayer!
    end

    def store!
      formatted_quotes.map do |source_currency:, target_currency:, exchange_rate:|
        ForeignExchangeRate.create!(
          source_currency: source_currency,
          target_currency: target_currency,
          exchange_rate: exchange_rate,
          valid_from: timestamp.beginning_of_day,
          valid_till: timestamp.end_of_day
        )
      end
    rescue ActiveRecord::RecordInvalid => e
      raise InvalidRateError.new(e)
    end

    private

    attr_reader :rate_fetcher_klass, :opts, :response

    def fetch_from_currencylayer!
      rate_fetcher_klass.fetch!(**opts)
    rescue Currencylayer::CallFailedError => e
      raise InvalidRateError.new(e)
    end

    def timestamp
      @timestamp ||= Time.at(response.timestamp).to_datetime
    end

    def formatted_quotes
      QuotesFormatter.call!(
        response.quotes,
        opts[:source_currency],
        opts[:target_currencies]
      )
    rescue UnknownQuotesFormatError => e
      raise InvalidRateError.new(e)
    end
  end
end
