# frozen_string_literal: true

module Currencylayer
  class UnknownQuotesFormatError < StandardError
    DEFAULT_MESSAGE = "Unknown quotes returned from currencylayer"

    def initialize(msg: DEFAULT_MESSAGE, quotes:)
      @quotes = quotes
      @msg = msg
      super(msg)
    end

    def to_s
      "#{@msg}: #{@quotes}"
    end
  end
end
