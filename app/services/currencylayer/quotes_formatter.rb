# forzen_string_literal: true

# Currencylayer API returns exchangerate in the following format
# {
#  "USDAED": 3.67315,
#  "USDAFN": 60.790001,
#  "USDALL": 126.194504,
# }
# This class validates the currecies and format it to a readable form as
# bellow
# [
#   { source_currency: "USD", target_currency: "AED", exchange_rate: 3.67315 },
#   { source_currency: "USD", target_currency: "AFN", exchange_rate: 60.790001 },
#   { source_currency: "USD", target_currency: "AFN", exchange_rate: 126.194504 }
# ]

module Currencylayer
  class QuotesFormatter
    SOURCE_CURRENCY_RANGE = (0..2).freeze
    TARGET_CURRENCY_RANGE = (3..).freeze

    def self.call!(*args)
      new(*args).exchange_rates
    end

    def initialize(quotes, source_currency, target_currencies)
      @quotes = quotes
      @source_currency = source_currency
      @target_currencies = Array(target_currencies)

      validate!
    end

    def exchange_rates
      quotes.each_with_object([]) do |(currency_str, exchange_rate), arr|
        source_currency = currency_str[SOURCE_CURRENCY_RANGE]
        target_currency = currency_str[TARGET_CURRENCY_RANGE]
        arr << {
          source_currency: source_currency,
          target_currency: target_currency,
          exchange_rate: exchange_rate
        }
      end
    end

    private

    attr_reader :quotes, :source_currency, :target_currencies

    def validate!
      raise UnknownQuotesFormatError.new(quotes: quotes) unless valid_currency_formats?
    end

    def valid_currency_formats?
      valid_source_currency? && valid_target_currencies?
    end

    def valid_source_currency?
      source_currency_from_quotes == source_currency
    end

    def valid_target_currencies?
      target_currencies_from_quotes == target_currencies
    end

    def source_currency_from_quotes
      quotes.keys
            .map { |currency_str| currency_str[SOURCE_CURRENCY_RANGE] }
            .uniq
            .first
    end

    def target_currencies_from_quotes
      quotes.keys.map { |currency_str| currency_str[TARGET_CURRENCY_RANGE] }
    end
  end
end
