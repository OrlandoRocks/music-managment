# frozen_string_literal: true

module Currencylayer
  class HistoricRateFetcher
    def self.fetch!(**args)
      new(**args).response
    end

    def initialize(source_currency:, target_currencies:, date:)
      @source_currency = source_currency
      @target_currencies = Array(target_currencies)
      @date = date.to_date
      @api_client = Currencylayer::ApiClient.new
    end

    def response
      @response ||= call_currencylayer!
    end

    private

    attr_reader :date, :source_currency, :target_currencies, :api_client

    def call_currencylayer!
      response = api_client.send_request(
        :historical,
        date: date,
        source_currency: source_currency,
        target_currencies: target_currencies
      )
    rescue Faraday::Error, Oj::ParseError => e
      airbrake_notify(error: e)
      raise CallFailedError.new(msg: e)
    else
      handle_response(response)
    end

    def handle_response(response)
      return response if response.success?

      airbrake_notify(response_body: response.body)
      raise CallFailedError.new(response: response.body)
    end

    def airbrake_notify(error: CallFailedError::DEFAULT_ERR_MSG, response_body: nil)
      Tunecore::Airbrake.notify(
        error,
        source_currency: source_currency,
        target_currencies: target_currencies,
        data: response_body
      )
    end
  end
end
