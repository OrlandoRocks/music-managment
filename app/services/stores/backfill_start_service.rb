class Stores::BackfillStartService
  DAY_OF_SIDEKIQ_CUTOVER = Time.parse("2018-05-24")

  def self.start(store_id, backfill_amount = 1000)
    store = Store.find(store_id)
    store.distributions.where(state: "paused", created_at: DAY_OF_SIDEKIQ_CUTOVER..Time.now).order("created_at DESC").limit(backfill_amount).each do |distribution|
      Delivery::DistributionWorker.set(queue: "delivery-default", unique_for: false).perform_async(distribution.class.name, distribution.id)
    end
  end
end
