class ForeignExchangePeggedRateService
  def self.current_pegged_rate_id(person)
    new(person)&.current_pegged_rate_id
  end

  def initialize(person)
    @person = person
    @country_website = CountryWebsite.find_by(country: person.country_domain)
  end

  def current_pegged_rate_id
    @country_website&.current_pegged_rate&.id
  end
end
