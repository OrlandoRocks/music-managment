class YoutubeMusic::OfficialArtistChannelService
  AUTH_URL = "https://accounts.google.com/o/oauth2/v2/auth".freeze
  SCOPE_URLS = "https://www.googleapis.com/auth/youtube.readonly".freeze
  attr_reader :client_id,
              :client_url,
              :artist_name,
              :code,
              :person,
              :channel_id,
              :access_token,
              :refresh_token,
              :creatives

  def self.authorization_redirect_url
    new({ client_url: AUTH_URL }).build_authorization_url
  end

  def self.channel_id_active_status(album)
    new({ artist_name: album.artist.name, person: album.person }).channel_id_status
  end

  def initialize(params = {})
    @code = params[:code]
    @client_url = params[:client_url]
    @artist_name = params[:artist_name]
    @person = params[:person]
    @client_id = ENV.fetch("YOUTUBE_OAC_CLIENT_ID")
  end

  def create_esi_entries(esi_type, identifier)
    @creatives = Creative.person_by_artist_name(person.id, artist_name)
    if lookup_esis(esi_type).any?
      creatives.each do |creative|
        ExternalServiceId.find_or_initialize_by(
          linkable_id: creative.id,
          linkable_type: "Creative",
          service_name: esi_type
        ).update!(identifier: identifier, verified_at: Time.now)
      end
    else
      esi_entries =
        creatives.each_with_object([]) do |creative, array|
          entry = {
            linkable_id: creative.id,
            linkable_type: "Creative",
            service_name: esi_type,
            identifier: identifier,
            verified_at: Time.now
          }
          array << entry
        end
      ExternalServiceId.create!(esi_entries)
    end
  end

  def channel_id_status
    refresh_token = lookup_esis(ExternalServiceId::YOUTUBE_AUTH).first.identifier
    channel = lookup_esis(ExternalServiceId::YOUTUBE_CHANNEL).first.identifier
    return false unless channel && refresh_token
    return true if channel_active?(channel, refresh_token)

    mark_channel_as_deleted
    false
  end

  def fetch_and_store_channel
    youtube_client = Youtube::ApiClient.new(authorization: code)
    refresh_token = youtube_client.fetch_refresh_token
    create_esi_entries(ExternalServiceId::YOUTUBE_AUTH, refresh_token)

    channel_id = youtube_client.fetch_channel_id
    create_esi_entries(ExternalServiceId::YOUTUBE_CHANNEL, channel_id)
  end

  def enqueue_distribution!
    distribution = Distribution.join_creatives
                               .joins(:salepoints)
                               .joins(petri_bundle: :album)
                               .where(creatives: { id: creatives.pluck(:id) })
                               .where(salepoints: { store_id: Store::GOOGLE_STORE_ID, takedown_at: nil })
                               .where(albums: { takedown_at: nil })
                               .where(state: ["delivered", "packaged"])
                               .order(:updated_at).first

    return unless distribution

    distribution.retry({ actor: "YT OAC Service", message: "Sending Delivery Update for first time OAC Channel ID" })
  end

  def build_authorization_url
    [AUTH_URL, encoded_params].join("?")
  end

  private

  def mark_channel_as_deleted
    esis = lookup_esis(ExternalServiceId::YOUTUBE_CHANNEL)
    esis.update_all(identifier: "deleted")
  end

  def query_params
    {
      client_id: client_id,
      redirect_uri: redirect_path,
      scope: encodeded_scopes,
      response_type: "code",
      access_type: "offline",
      prompt: "consent"
    }
  end

  def encodeded_scopes
    URI.escape(SCOPE_URLS.to_s)
  end

  def encoded_params
    URI.encode_www_form(query_params).gsub("%2520", "+")
  end

  def redirect_path
    URI.parse(ENV["YOUTUBE_OAC_REDIRECT_URL"])
  end

  def lookup_esis(esi_type)
    ExternalServiceId.by_creatives(Creative.person_by_artist_name(person.id, artist_name))
                     .by_service(esi_type)
  end

  def fetch_channel_id(token)
    Youtube::ApiClient.new(token).fetch_channel
  end

  def channel_active?(channel_id, token)
    begin
      response = Youtube::ApiClient.new(refresh_token: token).fetch_channel_id
      return true if response == channel_id
    rescue Youtube::ApiClient::ApiError => e
      Airbrake.notify("Youtube API Error: #{e}")
      return true
    rescue Youtube::ApiClient::ApiQuotaError => e
      Airbrake.notify("Youtube API Quota Reached: #{e}") # let it fail for retry
      raise
    rescue Youtube::ApiClient::ServerError => e
      Airbrake.notify("Youtube API Server Error: #{e}") # let it fail for retry
      raise
    rescue Youtube::ApiClient::AccountError => e
      Airbrake.notify("Youtube Account Error: #{e}")
      return false
    end
    false
  end
end
