require "./lib/slack_notifier"
class YoutubeMusic::YoutubeChannelVerificationService
  attr_reader :channel_data

  def self.verify_channels(data)
    new(data).verify_channels
  end

  def initialize(channel_data)
    @channel_data = channel_data
  end

  def verify_channels
    failures = []

    channel_data.each do |row|
      begin
        response = Youtube::ApiClient.new(refresh_token: row["token"]).fetch_channel_id
      rescue Youtube::ApiClient::ApiError => e
        Airbrake.notify("Youtube API Error: #{e}")
        failures << row

        next
      rescue Youtube::ApiClient::ApiQuotaError => e
        Airbrake.notify("Youtube API Quota Reached: #{e}")
        failures << row

        next
      rescue Youtube::ApiClient::ServerError => e
        Airbrake.notify("Youtube API Server Error: #{e}")
        failures << row

        next
      rescue Youtube::ApiClient::AccountError => e
        Airbrake.notify("Youtube Account Error: #{e}")
      end

      if response == row["channel"]
        ids = ExternalServiceId.where(identifier: row["channel"], service_name: ExternalServiceId::YOUTUBE_CHANNEL)
        ids.update(verified_at: Time.now)
      else
        delete_channel_data(row)
      end
    end

    notify_slack(failures, channel_data)
  end

  def notify_slack(failures, dataset)
    total = dataset.count
    alert_message = "Youtube Channel Service Verification Processed #{total} records."
    fail_total = failures.count
    failed_channels = failures.map { |row| row["channel"] }.join(",")
    message_body = "#{total} Channels verified with #{fail_total} failures: #{failed_channels}"
    notify_in_slack(alert_message + message_body, "Youtube Channel Verification")
  end

  def delete_channel_data(row)
    ExternalServiceIdDelete.create!(
      identifier: row["channel"],
      service_name: ExternalServiceId::YOUTUBE_CHANNEL,
      person_id: row["person_id"],
      artist_name: row["artist_name"]
    )

    ExternalServiceIdDelete.create!(
      identifier: row["token"],
      service_name: ExternalServiceId::YOUTUBE_AUTH,
      person_id: row["person_id"],
      artist_name: row["artist_name"]
    )

    ids_to_delete = ExternalServiceId.where(
      identifier: row["channel"],
      service_name: ExternalServiceId::YOUTUBE_CHANNEL
    )

    tokens_to_delete = ExternalServiceId.where(
      identifier: row["token"],
      service_name: ExternalServiceId::YOUTUBE_AUTH
    )

    ids_to_delete.destroy_all
    tokens_to_delete.destroy_all
  end
end
