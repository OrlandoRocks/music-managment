# frozen_string_literal: true

class SipInvoiceProcessor
  TUNECORE = "TuneCore US"
  PERSON_INTAKE = "PersonIntake"
  YOUTUBE_INTAKE = "YouTubeRoyaltyIntake"
  RELATED_TYPE = "OutboundInvoice"

  def initialize(posting_id)
    @posting_id = posting_id
  end

  def process!
    @last_bi_invoice_num = OutboundInvoice.where(invoice_prefix: "BI-INV#").last&.invoice_number.to_i
    @last_tc_invoice_num = OutboundInvoice.where(invoice_prefix: "TC-INV#").last&.invoice_number.to_i
    process_intakes
  end

  private

  def process_intakes
    return if @posting_id.blank?

    process_person_intakes
    process_youtube_intakes
  end

  def process_person_intakes
    loop do
      intake_batch = PersonIntake.with_posting_id(@posting_id).limit(1000)
      break unless intake_batch.any?

      process_batch(intake_batch, PERSON_INTAKE)
    end
  end

  def process_youtube_intakes
    loop do
      intake_batch = YouTubeRoyaltyIntake.with_posting_id(@posting_id).limit(1000)
      break unless intake_batch.any?

      process_batch(intake_batch, YOUTUBE_INTAKE)
    end
  end

  def process_batch(intake_batch, intake_type)
    @outbound_invoices = []
    @invoice_static_contents = {}

    build_invoice_static_content(intake_batch)
    intake_batch.each do |intake|
      create_invoice_hash(intake, intake_type)
    end
    return unless @outbound_invoices.any?

    ActiveRecord::Base.transaction do
      OutboundInvoice.import @outbound_invoices, validate: true, validate_uniqueness: true, raise_error: true
      perform_post_create_operations(intake_type)
    end
  end

  def create_invoice_hash(intake, type)
    static_content = @invoice_static_contents[intake.person_id]
    invoice_number = invoice_number_details(static_content[:corporate_entity_info][:name])
    @outbound_invoices << {
      person_id: intake.person_id,
      invoice_number: invoice_number[:value],
      invoice_prefix: invoice_number[:prefix],
      related_type: type,
      related_id: intake.id,
      vat_tax_adjustment_id: intake.vat_tax_adjustment_id,
      invoice_date: DateTime.now,
      currency: static_content[:customer_info][:person_currency]
    }
  end

  def invoice_number_details(corporate_entity)
    if corporate_entity == TUNECORE
      @last_tc_invoice_num += 1
      invoice_number = @last_tc_invoice_num
      invoice_prefix = "TC-INV#"
    else
      @last_bi_invoice_num += 1
      invoice_number = @last_bi_invoice_num
      invoice_prefix = "BI-INV#"
    end
    { value: invoice_number, prefix: invoice_prefix }
  end

  def perform_post_create_operations(intake_type)
    args = {
      transactions: [],
      address_info: [],
      corporate_entity_info: [],
      customer_info: []
    }

    intake_ids = @outbound_invoices.pluck(:related_id)
    OutboundInvoice.where(related_id: intake_ids, related_type: intake_type).find_each do |invoice|
      args[:transactions] << build_person_transaction(invoice)
      %i[address_info corporate_entity_info customer_info].each do |key|
        args[key] << build_static_data(invoice, key)
      end
    end
    create_transactions(args)
  end

  def create_transactions(args)
    PersonTransaction.import args[:transactions]
    InvoiceStaticCustomerAddress.import args[:address_info]
    InvoiceStaticCorporateEntity.import args[:corporate_entity_info]
    InvoiceStaticCustomerInfo.import args[:customer_info]
  end

  def build_person_transaction(invoice)
    {
      person_id: invoice.person_id,
      debit: 0,
      credit: 0,
      target_id: invoice.id,
      target_type: "OutboundInvoice",
      comment: "Royalty Invoice",
      currency: invoice.currency
    }
  end

  def build_static_data(invoice, key)
    @invoice_static_contents[invoice.person_id][key].merge(related_id: invoice.id, related_type: RELATED_TYPE)
  end

  def build_invoice_static_content(intake_batch)
    persons = Person.where(id: intake_batch.pluck(:person_id))
                    .includes(:corporate_entity, :country_website, :vat_information)
    persons.find_each do |person|
      @invoice_static_contents[person.id] = {
        address_info: person_address_attributes(person),
        customer_info: person_info(person),
        corporate_entity_info: person.corporate_entity.affiliated_to_bi? ? believe_data : tunecore_data
      }
    end
  end

  def person_address_attributes(person)
    person.attributes.slice("address1", "address2", "city", "state")
          .merge(country: person.country_name_untranslated,
                 zip: person.foreign_postal_code)
  end

  def person_info(person)
    {
      name: person.name,
      person_currency: person.currency,
      customer_type: person.vat_information&.customer_type,
      vat_registration_number: person.vat_information&.vat_registration_number
    }
  end

  def believe_data
    @believe_data ||= CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME).attributes.except("id")
  end

  def tunecore_data
    @tunecore_data ||= CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME).attributes.except("id")
  end
end
