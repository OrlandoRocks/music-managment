# frozen_string_literal: true

# Helpers for the /albums/{id}/album_meta_data endpoint.
class AlbumMetadataService
  include SalepointQuery

  EMPTY_LIST = [].freeze

  attr_reader :album, :salepoints

  def initialize(album)
    @album = album
    @salepoints = album.salepoints
  end

  def lo_fi_songs
    @lo_fi_songs ||= SongS3DetailService.by_album(album).lo_fi
  end

  def visible_lo_fi_songs
    return EMPTY_LIST unless only_salepoint?(salepoints, Store::QOBUZ_STORE_ID)

    lo_fi_songs.map(&:name)
  end

  def show_remove_qobuz_confirmation?
    return false unless other_salepoints?(salepoints, Store::QOBUZ_STORE_ID)

    lo_fi_songs.any?
  end
end
