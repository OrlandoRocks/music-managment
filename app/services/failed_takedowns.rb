class FailedTakedowns
  FailedTakedown = Struct.new(:album_id, :renewal_id, :expires_at, :canceled_at)

  attr_reader :failed_takedowns, :album_ids_strings

  def initialize
    @failed_takedowns = takedowns
    @album_ids_strings = @failed_takedowns.map { |ft| ft.album_id.to_s }
  end

  def grace_period_cutoff_date
    (Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days).to_s
  end

  private

  def takedowns
    arel = renewals
           .project(
             albums[:id].as("album_id"),
             renewals[:id].as("renewal_id"),
             renewal_history_subquery[:expires_at],
             renewals[:canceled_at]
           )
           .join(renewal_history_subquery)
           .on(renewals[:id].eq(renewal_history_subquery[:renewal_id]))
           .join(renewal_items)
           .on(renewal_items[:renewal_id].eq(renewals[:id]))
           .join(albums)
           .on(albums[:id].eq(renewal_items[:related_id])
        .and(renewal_items[:related_type].eq("Album")))
           .where(renewals[:takedown_at].eq(nil))
           .where(
             renewal_history_subquery[:expires_at].lteq(grace_period_cutoff_date)
             .or(renewals[:canceled_at].not_eq(nil)
             .and(renewal_history_subquery[:expires_at].lt(Date.today)))
           )
           .order(albums[:id].desc)

    ActiveRecord::Base
      .connection
      .execute(arel.to_sql)
      .map { |ft| FailedTakedown.new(*ft) }
  end

  def renewal_history_subquery
    renewal_histories.project(
      renewal_histories[:expires_at].maximum.as("expires_at"),
      renewal_histories[:renewal_id].as("renewal_id")
    ).group(
      renewal_histories[:renewal_id]
    ).as("renewal_history")
  end

  def albums
    Album.arel_table
  end

  def renewals
    Renewal.arel_table
  end

  def renewal_histories
    RenewalHistory.arel_table
  end

  def renewal_items
    RenewalItem.arel_table
  end
end
