class InvoiceGenerator::UploadInboundInvoicePdf < InvoiceGenerator::InvoiceUpload
  attr_accessor :invoice, :person, :static_corporate_entity, :invoice_data

  def initialize(invoice)
    @invoice = invoice
    @person = invoice.person
  end

  def process
    process_invoice if person.vat_feature_on?
  end

  private

  def process_invoice
    with_locale do
      generate_inbound_data
      generate_invoice_pdf
      upload_invoice
    end
  end
end
