# frozen_string_literal: true

class InvoiceGenerator::EmailNotification < InvoiceGenerator::Base
  EUR = "EUR"
  attr_accessor :static_corporate_entity, :invoice_data

  def initialize(invoice)
    super(invoice)

    @errors = []
  end

  def perform!
    return if invoice.has_incomplete_static_data? || !invoice.settled?
    return { errors: @errors } if @errors.present?

    process_invoice if feature_on?
    update_invoice
  end

  private

  def feature_on?
    FeatureFlipper.show_feature?(:vat_tax, person)
  end

  def process_invoice
    with_locale do
      generate_inbound_data(:email_invoice)
      generate_invoice_pdf
      send_invoice
    end
  end

  def send_invoice
    InvoiceMailer.send_pdf_invoice(invoice: invoice, pdf: @pdf).deliver
  end

  def update_invoice
    invoice.update(invoice_sent_at: Time.zone.now)
  end

  def with_locale
    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) { yield }
  end
end
