# frozen_string_literal: true

class InvoiceGenerator::GenerateOutboundData < InvoiceGenerator::Base
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  VAT = "VAT"

  attr_reader :invoice_data, :static_corporate_entity

  def initialize(outbound_invoice)
    super(outbound_invoice)

    @static_corporate_entity = @invoice.invoice_static_corporate_entity
    @invoice_data = generate_invoice_data
  end

  def sub_total
    total = invoice_data[:items].sum { |item| item[:original_amount].to_f }

    invoice_formatted_currency(total)
  end

  private

  def exchange_rate
    return inr_to_euro_exchange_rate if person.country_website.india?

    vat_tax_adjustment&.foreign_exchange_rate&.exchange_rate || 1
  end

  def generate_invoice_data
    invoice_details.merge(
      person_data: person_details,
      items: [invoice_item_details]
    ).deep_symbolize_keys
  end

  def vat_tax_adjustment
    invoice.vat_tax_adjustment
  end

  def vat_amount
    vat_tax_adjustment.nil? ? 0 : vat_tax_adjustment.amount
  end

  def transfer
    invoice.related
  end

  def invoice_details
    {
      id: invoice.id,
      currency: currency,
      invoice_number: invoice.prefixed_invoice_number,
      date: invoice.invoice_date.to_date,
      vat_amount_cents_converted: euro_formated_currency(converted_vat_amount),
      net_payable: invoice_formatted_currency(total_amount),
      vat_amount_cents: invoice_formatted_currency(vat_amount),
      is_vat_registered: invoice.vat_tax_adjustment&.vat_registered?,
      is_luxembourg_customer: invoice.invoice_static_customer_address.luxembourg_customer?
    }.tap do |h|
      h[:total_excl_taxes] = invoice_formatted_currency(total_amount.to_f - vat_amount.to_f)
      h[:total_incl_taxes] = h[:net_payable]
    end
  end

  def invoice_item_details
    {
      item_name: item_name,
      original_amount: invoice_formatted_currency(invoice_amount),
      currency: currency,
      euro_exchange_rate: exchange_rate,
      vat_amount: invoice_formatted_currency(vat_amount),
      total_amount: invoice_formatted_currency(total_amount),
      tax_rate: (vat_tax_adjustment&.tax_rate || 0),
      place_of_supply: (vat_tax_adjustment&.place_of_supply || static_corporate_entity.country),
      tax_type: (vat_tax_adjustment&.tax_type || VAT),
      sub_total: invoice_formatted_currency(invoice_amount)
    }.tap do |h|
      h[:id] = transfer.id if transfer
    end
  end

  def converted_vat_amount
    return 0 if vat_tax_adjustment.nil?

    vat_tax_adjustment.vat_amount_in_eur.to_i
  end

  def item_name
    case static_corporate_entity.name
    when CorporateEntity::TUNECORE_US_NAME
      custom_t("invoices.royalty_invoices.invoice_item_tc")
    when CorporateEntity::BI_LUXEMBOURG_NAME
      custom_t("invoices.royalty_invoices.invoice_item_bi")
    else
      custom_t("invoices.royalty_invoices.invoice_item_tc")
    end
  end

  def total_amount
    vat_amount + invoice_amount
  end

  def invoice_amount
    return 0 unless transfer

    case transfer
    when PayoutTransfer
      transfer.amount_cents + transfer.fee_cents
    when InvoiceSettlement
      transfer.settlement_amount_cents - vat_amount
    when PaypalTransfer
      transfer.payment_cents
    end
  end
end
