# frozen_string_literal: true

class InvoiceGenerator::Base
  include CurrencyHelper
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  NET_PAYABLE = 0
  attr_reader :invoice, :person

  def initialize(invoice)
    # Since this base class is common for both inbound and oubound inovice,
    # the @inovice will be either invoice or outbound_invoice.
    @invoice = invoice
    @person = invoice.person
  end

  def generate_inbound_data(invoice_type = nil)
    invoice.generate_invoice_number if invoice_type == :email_invoice
    @static_corporate_entity = invoice.invoice_static_corporate_entity
    @invoice_data = inbound_invoice_details
  end

  def inbound_sub_total
    invoice_formatted_currency(invoice.purchases.sum { |purchase| purchase.cost_cents.to_f })
  end

  def total_discount
    invoice_formatted_currency(invoice.purchases.sum { |purchase| purchase.discount_cents.to_f })
  end

  private

  def currency
    person.country_website.india? ? CurrencyCodeType::INR : invoice.currency
  end

  def person_details
    return {} if invoice.invoice_static_customer_address.nil?

    invoice.invoice_static_customer_address
           .attributes
           .slice("address1", "address2", "city", "state", "country", "zip")
           .merge(region: country&.region)
           .merge(customer_info)
  end

  def customer_info
    return {} if invoice.invoice_static_customer_info.nil?

    invoice.invoice_static_customer_info
           .attributes
           .slice(*InvoiceStaticCustomerInfo::FIELDS)
  end

  def invoice_formatted_currency(cents)
    money = Money.new(cents, invoice.currency)

    if invoice.is_a? OutboundInvoice
      outbound_invoice_converted_money(money)
    else
      inbound_invoice_converted_money(money)
    end
  end

  def euro_formated_currency(cents)
    money = Money.new(cents, CurrencyCodeType::EUR)

    if invoice.is_a? OutboundInvoice
      outbound_invoice_converted_money(money, money_currency: CurrencyCodeType::EUR)
    else
      inbound_invoice_converted_money(money, money_currency: CurrencyCodeType::EUR)
    end
  end

  def country
    country_name = invoice.invoice_static_customer_address.country || person.country_name_untranslated

    Country.find_by(name: country_name)
  end

  def inr_to_euro_exchange_rate
    ForeignExchangeRate.latest_by_currency(
      source: CurrencyCodeType::INR,
      target: CurrencyCodeType::EUR
    ).exchange_rate
  end

  def outbound_invoice_converted_money(money, money_currency: nil)
    # ForeignExchangeRate is used for currency conversion
    balance_to_currency(
      money,
      iso_code: person.country_domain,
      currency: money_currency || currency,
      unit: "",
      strip_insignificant_zeros: false,
      skip_inr_symbol: true
    )
  end

  def generate_invoice_pdf
    @ac = ApplicationController.new
    @pdf =
      if static_corporate_entity.tunecore_us?
        generate_tc_invoice
      else
        generate_bi_invoice
      end
  end

  def generate_tc_invoice
    WickedPdf.new.pdf_from_string(
      @ac.render_to_string(
        "invoice_pdf/tunecore_invoice_pdf",
        layout: "layouts/pdf",
        locals: {
          inbound: true,
          pdf: true,
          invoice_data: invoice_data,
          sub_total: inbound_sub_total,
          discounts: total_discount
        }
      ),
      header: pdf_header,
      page_size: "A4"
    )
  end

  def generate_bi_invoice
    WickedPdf.new.pdf_from_string(
      @ac.render_to_string(
        "invoice_pdf/believe_invoice_pdf",
        layout: "layouts/pdf",
        locals: {
          inbound: true,
          pdf: true,
          invoice_data: invoice_data,
          static_corporate_entity: static_corporate_entity,
          discounts: total_discount
        }
      ),
      footer: pdf_footer,
      header: pdf_header,
      page_size: "A4"
    )
  end

  def pdf_header(bound_type = nil)
    template = bound_type == :outbound ? "outbound_invoice_pdf_header" : "invoice_pdf_header"
    {
      content: @ac.render_to_string(
        "invoice_pdf/#{template}",
        layout: "layouts/pdf",
        locals: { invoice_data: invoice_data, static_corporate_entity: static_corporate_entity }
      )
    }
  end

  def pdf_footer(bound_type = nil)
    template = bound_type == :outbound ? "outbound_footer" : "invoice_pdf_footer"
    {
      content: @ac.render_to_string(
        "invoice_pdf/#{template}",
        layout: "layouts/pdf",
        locals: { invoice_data: invoice_data, static_corporate_entity: static_corporate_entity }
      )
    }
  end

  def inbound_invoice_details
    {
      id: invoice.id,
      currency: currency,
      invoice_number: invoice.invoice_number,
      date: invoice.settled_at.to_date,
      vat_amount_cents_converted: converted_amount,
      total_payments: inbound_invoice_converted_money(invoice.final_settlement_amount),
      person_data: person_details,
      vat_amount_cents: inbound_invoice_converted_money(invoice.vat_amount),
      items: item_details,
      settlement_info: settlement_info,
      net_payable: inbound_invoice_converted_money(NET_PAYABLE.to_money)
    }.tap do |h|
      h[:total_excl_taxes] = total_excl_taxes
      h[:total_incl_taxes] = h[:total_payments]
    end.deep_symbolize_keys
  end

  def settlement_info
    invoice.invoice_settlements.map do |settlement|
      {
        desc: InvoiceSettlementFormatter::Factory.for(settlement.source).invoice_description,
        amount: invoice_formatted_currency(settlement.settlement_amount_cents)
      }
    end
  end

  def total_excl_taxes
    invoice_formatted_currency(invoice.final_settlement_amount_cents.to_f - invoice.vat_amount_cents.to_f)
  end

  def converted_amount
    return inbound_invoice_converted_money(invoice.vat_amount) if invoice.currency == EUR

    @converted_amount ||= euro_formated_currency(invoice.vat_amount_cents_in_eur)
  end

  def item_details
    invoice.purchases.includes(:product).map do |purchase|
      {
        id: purchase.id,
        item_name: purchase.product.name,
        original_amount: inbound_invoice_converted_money(purchase.cost),
        discount_amount: inbound_invoice_converted_money(purchase.discount),
        currency: purchase.currency,
        euro_exchange_rate: inbound_exchange_rate,
        vat_amount: inbound_invoice_converted_money(purchase.vat_amount),
        sub_total: invoice_formatted_currency(purchase.sub_total_excl_vat),
        total_amount: inbound_invoice_converted_money(purchase.purchase_total)
      }.merge(purchase_tax_info(purchase))
    end
  end

  def inbound_exchange_rate
    return inr_to_euro_exchange_rate if person.country_website.india?
    return invoice.foreign_exchange_rate.exchange_rate if invoice.foreign_exchange_rate.present?

    1
  end

  # inbound transactions use ForeignExchangePeggedRate for currency conversion
  def inbound_invoice_converted_money(money, money_currency: nil)
    # ForeignExchangePeggedRate is used for currency conversion
    money_to_currency(
      money,
      iso_code: person.country_domain,
      currency: money_currency || currency,
      unit: "",
      strip_insignificant_zeros: false,
      round_value: false,
      skip_inr_symbol: true,
      pegged_rate: invoice.pegged_rate
    )
  end

  def purchase_tax_info(purchase)
    tax_info = purchase.purchase_tax_information
    return {} if tax_info.nil?

    tax_type = static_corporate_entity.affiliated_to_bi? ? "VAT" : tax_info.tax_type
    tax_info.attributes.slice(
      "tax_rate", "place_of_supply"
    ).merge(
      tax_type: tax_type
    )
  end

  def with_locale
    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) { yield }
  end

  def outbound_invoice?
    invoice.is_a? OutboundInvoice
  end
end
