# frozen_string_literal: true

class InvoiceGenerator::InvoiceUpload < InvoiceGenerator::Base
  BELIEVE = "Believe"
  TUNECORE = "Tunecore"

  private

  def upload_invoice
    bucket.object(key).put(body: @pdf)
  end

  def key
    @key ||= "#{Rails.env}/#{directory_name}/#{file_name}"
  end

  def bucket
    @bucket ||= S3_INVOICE_CLIENT.bucket(INVOICE_BUCKET)
  end

  def directory_name
    corporate_entity_name = invoice.invoice_static_corporate_entity.tunecore_us? ? TUNECORE : BELIEVE
    invoice_type = outbound_invoice? ? TcVat::Base::OUTBOUND : TcVat::Base::INBOUND
    child_directory = %(#{corporate_entity_name} #{invoice_type})

    %(#{invoice_date.strftime('%Y/%B/%d')}/#{child_directory})
  end

  def file_name
    date = invoice_date.strftime("%Y-%m-%d")
    invoice_number = outbound_invoice? ? invoice.prefixed_invoice_number : invoice.invoice_number

    %(#{invoice_number},#{date}.pdf)
  end

  def invoice_date
    outbound_invoice? ? invoice.invoice_date : invoice.settled_at
  end
end
