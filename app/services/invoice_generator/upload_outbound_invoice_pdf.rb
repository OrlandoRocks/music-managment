class InvoiceGenerator::UploadOutboundInvoicePdf < InvoiceGenerator::InvoiceUpload
  include CurrencyHelper
  attr_reader :static_corporate_entity, :invoice_data

  def initialize(invoice)
    super

    @static_corporate_entity = invoice.invoice_static_corporate_entity
  end

  def process
    process_invoice if person.vat_feature_on?
  end

  private

  def process_invoice
    with_locale do
      generate_outbound_data
      generate_outbound_pdf
      upload_invoice
    end
  end

  def generate_outbound_data
    outbound_data = InvoiceGenerator::GenerateOutboundData.new(invoice)
    @invoice_data = outbound_data.invoice_data
    @sub_total = outbound_data.sub_total
  end

  def generate_outbound_pdf
    @ac = ApplicationController.new
    @pdf =
      if static_corporate_entity.tunecore_us?
        generate_tc_outbound_invoice
      else
        generate_bi_outbound_invoice
      end
  end

  def generate_tc_outbound_invoice
    WickedPdf.new.pdf_from_string(
      @ac.render_to_string(
        "invoice_pdf/tunecore_invoice_pdf",
        layout: "layouts/pdf",
        locals: {
          inbound: false,
          pdf: true,
          invoice_data: invoice_data,
          sub_total: @sub_total,
          discounts: 0
        }
      ),
      header: pdf_header(:outbound),
      page_size: "A4"
    )
  end

  def generate_bi_outbound_invoice
    WickedPdf.new.pdf_from_string(
      @ac.render_to_string(
        "invoice_pdf/believe_invoice_pdf",
        layout: "layouts/pdf",
        locals: {
          inbound: false,
          pdf: true,
          invoice_data: invoice_data,
          static_corporate_entity: static_corporate_entity
        }
      ),
      footer: pdf_footer(:outbound),
      header: pdf_header(:outbound),
      page_size: "A4",
      margin: { top: 50, bottom: 25 }
    )
  end
end
