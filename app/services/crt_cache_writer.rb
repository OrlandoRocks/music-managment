class CrtCacheWriter
  attr_reader :album

  def self.write_album_to_cache(album_id)
    new(album_id).tap(&:write_album_to_cache)
  end

  def initialize(_album_id)
    @album = Album.find(tc_album_id)
  end

  def write_album_to_cache
    Tunecore::Crt.write("crt:#{tc_album.id}", album_data)
  end

  def album_data
    Tunecore::Crt.create_album_hash(album)
  end
end
