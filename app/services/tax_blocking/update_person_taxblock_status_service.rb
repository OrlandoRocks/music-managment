class TaxBlocking::UpdatePersonTaxblockStatusService
  include Ruleable
  include ActiveModel::Model

  attr_accessor :person

  validates :person, presence: { strict: true }

  define_rules do
    # Checks if the person has crossed the earnings threshold for current year
    rule :current_year_earnings_threshold, ->(config, person) do
      return :fail if person.current_tax_year_total_earnings >= config[:current_year_threshold]

      :ok
    end

    # Checks if the person has crossed the earnings threshold for configured
    # previous years
    rule :past_years_earnings_threshold, ->(config, person) do
      years                     = config[:previous_evaluation_years].keys
      eligible_evaluation_years =
        person
        .person_earnings_tax_metadata
        .taxform_not_submitted
        .where(tax_year: years)

      breached_years =
        eligible_evaluation_years.select do |meta_datum|
          config[:previous_evaluation_years][meta_datum.tax_year.to_s] <= meta_datum.total_earnings
        end

      return :fail if breached_years.any?

      :ok
    end
  end

  def initialize(person)
    @person = person

    valid?
  end

  def self.call(person)
    new(person).update_tax_blocking_status!
  end

  def not_us_user?
    !person.from_united_states_and_territories?
  end

  def tax_form_submitted?
    latest_tax_metadata&.taxforms?
  end

  def update_tax_blocking_status!
    return if latest_tax_metadata.blank?

    if eligible_to_withdraw?
      latest_tax_metadata.tax_unblock!
    else
      latest_tax_metadata.tax_block!
    end
  end

  def eligible_to_withdraw?
    return true if not_us_user?
    return true if tax_form_submitted?

    eligible_to_withdraw
  end

  private

  def eligible_to_withdraw
    @eligible_to_withdraw ||=
      evaluate_rules(against: @person, category: :tax_blocking).exclude?(:fail)
  end

  def latest_tax_metadata
    @latest_tax_metadata ||=
      person.person_earnings_tax_metadata
            .find_by(tax_year: Date.current.year)
  end
end
