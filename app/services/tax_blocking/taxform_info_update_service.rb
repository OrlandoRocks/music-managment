class TaxBlocking::TaxformInfoUpdateService
  def initialize(person_id, years)
    @person_id  = person_id
    @year_range = parse_year_range(years)
  end

  def summarize
    return if init_taxform_records.blank?

    init_earnings_tax_records

    @year_range.map do |year|
      summarize_for_year(year)
    end

    persist_earnings_tax_data_changes
  end

  private

  def parse_year_range(years)
    [*years].map do |year|
      DateTime.strptime(year.to_s, "%Y")
    end
  end

  def summarize_for_year(date_year)
    retro_active_taxform = active_form_for_year(date_year)
    return if retro_active_taxform.blank?

    @person_earnings_tax_data[date_year.year].assign_attributes(
      dist_taxform_id: retro_active_taxform.id,
      dist_taxform_submitted_at: retro_active_taxform.submitted_at
    )
  end

  def persist_earnings_tax_data_changes
    PersonEarningsTaxMetadatum.transaction do
      @person_earnings_tax_data.values.flatten.map(&:save)
    end
  end

  def active_form_for_year(year)
    valid_taxforms =
      @tax_forms.select do |tax_form|
        (tax_form.submitted_at..tax_form.expires_at)
          .overlaps?(year.beginning_of_year..year.end_of_year)
      end
    valid_taxforms.max
  end

  def init_taxform_records
    @tax_forms = TaxForm.where(
      provider: PayoutProvider::PAYONEER,
      person_id: @person_id,
      tax_form_type_id: TaxFormType.w9_types.ids
    )
  end

  def init_earnings_tax_records
    etax_data =
      @year_range.map do |date|
        [
          date.year,
          PersonEarningsTaxMetadatum.find_or_initialize_by(
            person_id: @person_id, tax_year: date.year
          )
        ]
      end
    @person_earnings_tax_data = Hash[etax_data]
  end
end
