# frozen_string_literal: true

class TaxBlocking::TaxableEarningsRolloverService
  DIST_TYPES = [
    "PersonIntake",
    "YouTubeRoyaltyIntake"
  ].freeze

  ADV_TAX_BLOCKING_DATE = "2021-11-22"
  US_COUNTRY_WEBSITE_ID = 1

  TAX_ROLLOVER_DEBIT_COMMENT = "
    Income the artist did not have access to in
    the current year to be taxed in the next year.
  "

  TAX_ROLLOVER_CREDIT_COMMENT = "
    Income the artist did not have access to in
    the previous year to be taxed in the current year.
  "

  attr_reader :person_id, :year

  def initialize(person_id, year = 2021)
    @person_id = person_id
    @year = year
  end

  def call
    return :earnings_rolledover unless needs_taxable_earnings_rolledover?

    rollover_earnings!
    :ok
  end

  private

  def advance_block_date
    @advance_block_date ||=
      ADV_TAX_BLOCKING_DATE.to_datetime
  end

  def country_website
    @country_website ||=
      Person.find(person_id).country_website
  end

  def us_website?
    country_website.id == US_COUNTRY_WEBSITE_ID
  end

  def year_range
    Time.new(year).beginning_of_year..Time.new(year).end_of_year
  end

  def taxable_earnings_rollover_records
    @taxable_earnings_rollover_records ||=
      TaxableEarningsRollover.where(
        person_id: person_id,
        tax_year: year,
        tax_blocked: true,
        tax_blocked_at: nil
      )
  end

  def taxable_distribution_earnings
    @taxable_distribution_earnings ||=
      PersonTransaction.where(
        person_id: person_id,
        target_type: DIST_TYPES,
        created_at: (Time.new(year).beginning_of_year..tax_blocked_at)
      ).sum(:credit)
  end

  def taxable_publishing_earnings
    @taxable_publishing_earnings ||=
      Float(
        PersonTransaction.for_us_taxable_balance_adjustment_target
                       .where(
                         person_id: person_id,
                         created_at: (Time.new(year).beginning_of_year..tax_blocked_at)
                       )
                       .sum(:credit)
      )
  end

  def needs_taxable_earnings_rolledover?
    taxable_earnings_rollover_records.any?
  end

  def tax_blocked_at
    return advance_block_date unless us_website?

    taxable_earnings_rollover = taxable_earnings_rollover_records.first

    @tax_blocked_at ||=
      if taxable_earnings_rollover.distribution_earnings?
        transaction =
          PersonTransaction.where(
            person_id: person_id,
            target_type: DIST_TYPES,
            created_at: year_range
          ).first

        [
          (transaction.created_at - 0.01.seconds).to_datetime,
          advance_block_date
        ].min

      elsif taxable_earnings_rollover.publishing_earnings?
        advance_block_date
      end
  end

  def handle_taxable_earnings_rollover_record_update!
    taxable_earnings_rollover_records.take.tap do |petm|
      petm.total_taxable_distribution_earnings = taxable_distribution_earnings
      petm.total_taxable_publishing_earnings = taxable_publishing_earnings
      petm.tax_blocked_at = tax_blocked_at
      petm.rollover_earnings = Float(petm.total_earnings) - Float(petm.total_taxable_earnings)
      petm.save!
    end
  end

  def rollover_earnings!
    ApplicationRecord.transaction do
      taxable_earnings_rollover = handle_taxable_earnings_rollover_record_update!
      next unless taxable_earnings_rollover.rollover_earnings.positive?

      PersonTransaction.create!(
        person_id: person_id,
        debit: taxable_earnings_rollover.rollover_earnings,
        target_type: TaxableEarningsRollover,
        target_id: taxable_earnings_rollover.id,
        comment: TAX_ROLLOVER_DEBIT_COMMENT,
        created_at: Time.new(year).end_of_year
      )

      pt = PersonTransaction.create!(
        person_id: person_id,
        credit: taxable_earnings_rollover.rollover_earnings,
        target_type: TaxableEarningsRollover,
        target_id: taxable_earnings_rollover.id,
        comment: TAX_ROLLOVER_CREDIT_COMMENT,
        created_at: Time.new(year).end_of_year + 1.second
      )

      TaxWithholdings::IRSWithholdingsService.new(pt).call
    end
  end
end
