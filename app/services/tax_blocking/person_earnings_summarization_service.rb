# frozen_string_literal: true

class TaxBlocking::PersonEarningsSummarizationService
  attr_accessor :person, :year_range

  INCOME_SOURCES = ["distribution", "publishing"].freeze
  DISTRIBUTION_INCOME_SOURCES = ["YouTubeRoyaltyIntake", "PersonIntake"].freeze
  PUBLISHING_INCOME_SOURCE = "Songwriter Royalty"

  # Params:
  # - person_id: a person ID
  # - year(optional): defaults to current year, accepts a single or a list of years. Eg: "2021" or ["2022", "2023"]
  def initialize(person_id, year = nil)
    @person = Person.find_by(id: person_id)
    year ||= DateTime.current.year
    years = [*year].map { |year_string| DateTime.strptime(year_string.to_s, "%Y") }
    @year_range = years.min.beginning_of_year..years.max.end_of_year
  end

  def summarize
    INCOME_SOURCES.each do |source|
      send("yearly_#{source}_income").each { |year, income| send("update_#{source}_earnings", year, income) }
    end
  end

  private

  def yearly_distribution_income
    PersonTransaction
      .where(
        person_id: person.id,
        created_at: year_range,
        target_type: DISTRIBUTION_INCOME_SOURCES
      )
      .group("YEAR(person_transactions.created_at)").sum(:credit)
  end

  def yearly_publishing_income
    PersonTransaction
      .joins(
        "INNER JOIN balance_adjustments ON balance_adjustments.id = person_transactions.target_id"
      ).where(
        person_id: person.id,
        created_at: year_range,
        target_type: "BalanceAdjustment",
        balance_adjustments: { category: PUBLISHING_INCOME_SOURCE }
      )
      .group("YEAR(person_transactions.created_at)").sum(:credit)
  end

  INCOME_SOURCES.each do |income_source|
    define_method("update_#{income_source}_earnings") do |year, income|
      earnings_meta_data = PersonEarningsTaxMetadatum.find_or_create_by(tax_year: year, person_id: person.id)
      earnings_meta_data&.update("total_#{income_source}_earnings".to_sym => income)
    end
  end
end
