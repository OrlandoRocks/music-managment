class BackfillSalepoint
  attr_accessor :source_store_id, :destination_source_id, :store, :copy_distributions

  def initialize(source_store_id, destination_source_id, copy_distributions)
    @source_store_id = source_store_id
    @destination_source_id = destination_source_id
    @copy_distributions = copy_distributions
    validate!
    @store = Store.find(destination_source_id)
  end

  def run
    source_store_name = Store.find(source_store_id).short_name
    destination_store_name = Store.find(destination_source_id).short_name

    Rails.logger.info "Starting backfill salepoints from #{source_store_name} to #{destination_store_name}"
    records = ApplicationRecord.connection.exec_query(
      Salepoint.sanitize_sql_for_assignment(
        [
          Salepoint.get_missing_salepoint_query,
          {
            source_store_id: source_store_id,
            destination_store_id: destination_source_id
          }
        ]
      )
    )

    salepoints_processed = 0
    salepointable_ids = records.rows&.flatten
    salepoints = Salepoint.where(id: salepointable_ids)

    salepoints.find_each do |salepoint|
      salepoints_processed += 1
      create_salepoint(salepoint)
    end

    total_salepoints = salepoints.count
    remaining_salepoints = total_salepoints - salepoints_processed
    Rails.logger.info "Salepoints Processed: #{salepoints_processed} Salepoints remaining: #{remaining_salepoints}"
    Rails.logger.info "Task completed"
  end

  def create_salepoint(salepoint)
    # Create new salepoint for destination_source_id
    new_salepoint = salepoint.dup
    new_salepoint.store_id = destination_source_id

    # Invalid salepoint
    return if new_salepoint.invalid? &&
              Salepoint.exists?(
                salepointable_id: new_salepoint.salepointable_id,
                store_id: destination_source_id
              )

    new_salepoint.save!
    p "New Salepoint added #{new_salepoint.id}"

    # Do not create distributions for destination_source_id
    # if no distribution exists for source_store_id

    return if !copy_distributions || salepoint.distributions.empty?

    distro = create_distribution(salepoint.distributions.first)
    distro.salepoints << new_salepoint
  end

  def create_distribution(distribution)
    Distribution.create!(
      state: "new",
      converter_class: store.converter_class,
      delivery_type: "full_delivery",
      petri_bundle_id: distribution.petri_bundle_id
    )
  end

  def validate!
    if source_store_id.nil? && destination_source_id.nil?
      raise "Please provide source_store_id and destination_source_id"
    end

    raise "Please provide source_store_id" if source_store_id.nil?
    raise "Please provide destination_source_id" if destination_source_id.nil?

    raise "Source_store_id and destination_source_id should be different" if source_store_id == destination_source_id

    raise "Source store does not exist" unless Store.exists?(id: source_store_id)
    raise "Destination store does not exist" unless Store.exists?(id: destination_source_id)
  end
end
