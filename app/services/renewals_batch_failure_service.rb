class RenewalsBatchFailureService
  def self.log(date)
    RenewalsBatchFailure.find_or_create_by(date: date).update(resolved: false)
  end

  def self.resolve(date)
    RenewalsBatchFailure.find_by(date: date)&.update(resolved: true)
  end
end
