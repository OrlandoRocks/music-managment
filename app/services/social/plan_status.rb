class Social::PlanStatus
  attr_reader :user, :subscription_status, :plan, :plan_expires_at, :grace_period_ends_on

  def self.for(user)
    new(user)
  end

  def initialize(user)
    @user = user
    get_subscription_status
    set_plan_attributes
  end

  private

  attr_reader :max_takedown_date

  def set_plan_attributes
    @plan, @plan_expires_at = plan_calculation

    @grace_period_ends_on = subscription_status.grace_period_ends_on unless no_subscription?
  end

  def plan_calculation
    case
    when active_subscription?
      ["pro", subscription_status.get_termination_date]
    when no_subscription? && active_distro?
      ["free", max_takedown_date]
    when no_subscription? && inactive_distro?
      ["free_expired", date_of_distro_takedown]
    when no_subscription?
      [nil, nil]
    when inactive_subscription? && active_distro?
      ["pro_expired_free", max_takedown_date]
    when inactive_subscription?
      ["pro_expired", subscription_status.get_termination_date]
    end
  end

  def active_distro?
    max_takedown_date ? max_takedown_date > Date.today : false
  end

  def inactive_distro?
    @distros = user.albums.where("takedown_at < ?", Date.today)
    !@distros.empty?
  end

  def date_of_distro_takedown
    @distros.order("takedown_at DESC").limit(1).first.takedown_at.to_date
  end

  def max_takedown_date
    @max_takedown_date ||= user.live_releases.last.try(:renewal).try(:expires_at).try(:to_date)
  end

  def active_subscription?
    subscription_status.try(:is_active?)
  end

  def inactive_subscription?
    !!(subscription_status && !subscription_status.is_active?)
  end

  def no_subscription?
    subscription_status.nil?
  end

  def get_subscription_status
    @subscription_status ||= user.subscription_status_for("Social")
  end
end
