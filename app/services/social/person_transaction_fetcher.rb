class Social::PersonTransactionFetcher
  def self.execute(person_id, params)
    new(person_id, params).tap(&:get_transactions)
  end

  attr_reader :transactions, :person_id, :start_date, :end_date, :page_num, :page_size

  def initialize(person_id, params)
    @person_id    = person_id
    @start_date   = params[:start_date]
    @end_date     = params[:end_date]
    @page_size    = set_page_size(params[:page_size])
    @page_num     = set_page_num(params[:page_num])
  end

  def get_transactions
    total_transactions = PersonTransaction.where(
      person_id: person_id,
      created_at: start_date..end_date
    ).order(created_at: :desc)
    @transactions =
      if page_size.to_i.zero?
        total_transactions
      else
        total_transactions.to_a.paginate(
          page: page_num,
          per_page: page_size
        )
      end
  end

  def set_page_num(page_num)
    (page_num.to_i.zero? || page_num.nil?) ? 1 : page_num
  end

  def set_page_size(page_size)
    page_size || 20
  end
end
