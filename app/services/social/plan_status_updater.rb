class Social::PlanStatusUpdater
  class PlanStatusError < StandardError; end

  def self.update(people)
    new(people).update
  end

  attr_reader :updated

  def initialize(people)
    @people = [people].flatten
    @params = { users: {} }
  end

  def update
    @people.each do |person|
      @params[:users][encrypted_token(person.tc_social_token)] = plan_status_for(person)
    end

    if @params[:users].empty?
      false
    else
      make_social_api_request(@params)
    end
  end

  private

  def make_social_api_request(params)
    response = $social_api_client.post("api/bulk_plan_update", params)
    raise PlanStatusError.new(parse_errors(response)) unless successful_request?(response)

    true
  end

  def successful_request?(response)
    response.status == 200 && parsed_body(response).try(:[], "status") == "success"
  end

  def parsed_body(response)
    JSON.parse(response.body)
  end

  def encrypted_token(token)
    SessionEncryptionEngine.encrypt64(token)
  end

  def parse_errors(response)
    message = parsed_body(response).try(:[], "message")
    error = (message && message.respond_to?(:join)) ? message.join(", ") : message
    error || "Unable to update plans for #{@people.map(&:email).join(', ')}"
  end

  def plan_status_for(person)
    plan_status = Social::PlanStatus.for(person)
    [plan_status.plan, plan_status.plan_expires_at]
  end
end
