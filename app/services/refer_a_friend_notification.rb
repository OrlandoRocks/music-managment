class ReferAFriendNotification
  def self.send_notification(invoice_id, new_customer)
    new(invoice_id, new_customer).tap(&:send_notification)
  end

  attr_reader :referral

  def initialize(invoice_id, new_customer)
    invoice = Invoice.find(invoice_id)
    @referral = ReferAFriend::PurchaseHandler.new(invoice, new_customer)
  end

  def send_notification
    $refer_a_friend_api_client.post(referral.prepare_data, "/purchases")
  end
end
