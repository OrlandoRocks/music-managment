class PersonTransactionExporter
  CSV_HEADERS = [
    "Date", "Description", "Sales", "Debit", "Credit", "Balance", "Currency"
  ].freeze

  ROW_SEPARATOR = "\r\n"

  attr_accessor :transactions, :sales

  def self.export(transactions, sales)
    new(transactions, sales).build_csv
  end

  def initialize(transactions, sales)
    @transactions = transactions
    @sales = sales || {}
  end

  def build_csv
    CSV.generate("", row_sep: ROW_SEPARATOR) do |csv|
      csv << csv_headers
      @transactions.each do |txn|
        sale_records = @sales[txn.id] || []
        parse_csv_row(txn, sale_records).each { |row| csv << row }
      end
    end
  end

  def csv_headers
    CSV_HEADERS
  end

  def parse_csv_row(txn, txn_sales)
    if txn.target_type == "PersonIntake"
      [txn_for_csv(txn, "Sales Posted")] + txn_sales.map { |s| sale_for_csv(s) }
    else
      [txn_for_csv(txn)]
    end
  end

  def txn_for_csv(txn, overwrite_comment = nil)
    [
      I18n.l(txn.created_at, format: :dash),
      overwrite_comment || txn.comment,
      nil,
      txn.debit,
      txn.credit,
      txn.previous_balance + txn.credit - txn.debit,
      txn.currency
    ]
  end

  def sale_for_csv(sale)
    [nil, "#{sale.store_name} #{sale.country} #{sale.period_sort}", sale.store_amount]
  end
end
