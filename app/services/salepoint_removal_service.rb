# Gathers behavior for removing salepoints before purchase, depending on attributes of a release.
class SalepointRemovalService
  include SalepointQuery

  attr_reader :album, :salepoints

  def self.remove_unsupported_salepoints(album)
    new(album).remove_unsupported_salepoints
  end

  def initialize(album)
    @album = album
    @salepoints = album.salepoints
  end

  def remove_unsupported_salepoints
    remove_unsupported_salepoint(Store::QOBUZ_STORE_ID, :valid_qobuz?)
    nil
  end

  private

  # Remove the salepoint if it's not the only salepoint and invalid.
  # (When it's the only salepoint, we instead notify the user and block further action.)
  def remove_unsupported_salepoint(store_id, invalid_check)
    return unless salepoint?(salepoints, store_id)
    return if only_salepoint?(salepoints, store_id) ||
              method(invalid_check).call

    destroy_by_store_id(store_id)
  end

  def destroy_by_store_id(store_id)
    salepoints.where(store_id: store_id).destroy_all
  end

  #
  # Release requirements by salepoint:
  #
  def valid_qobuz?
    SongS3DetailService.by_album(album).mp3s.empty?
  end
end
