# frozen_string_literal: true

# Create or Update External Service ID for creatives with user's input, or create a stub.
class ExternalServiceIds::CreationService
  def self.create_update_match(creatives, params)
    creatives.each do |c|
      new(c, params)
    rescue ArgumentError => e
      Airbrake.notify(e.message, { creative: c.id })
      next
    end
    ArtistIds::CreationService.match(creatives)
  end

  attr_reader :creative, :apple, :spotify

  def initialize(creative, params)
    @creative = creative
    param = matching_param(creative, params)
    raise ArgumentError, "A creative's External Service ID param should not be nil" unless param

    @apple = param["apple"]
    @spotify = param["spotify"]
    handle_apple
    handle_spotify
  end

  private

  def handle_apple
    # Apple doesn't want IDs for non-primary artists (2021-01-27)
    return unless creative.is_primary?

    identifier = apple_identifier(apple["identifier"], creative.name)
    create_or_update_external_service_id(identifier, apple, ExternalServiceId::APPLE_SERVICE)
  end

  def handle_spotify
    identifier = spotify_identifier(spotify["identifier"], creative.name)
    create_or_update_external_service_id(identifier, spotify, ExternalServiceId::SPOTIFY_SERVICE)
  end

  def create_or_update_external_service_id(identifier, service_attr, service_name)
    state = get_esid_state(service_attr)
    creative.set_external_id_for(service_name, identifier, state)
  end

  # Params might have been submitted before Creative was created.
  # Match creative to param by ID for persisted creatives, by name for new creatives.
  def matching_param(creative, params)
    name = creative.artist.name
    params.find { |p| (p["id"] == creative.id) || (p["name"] == name) }
  end

  def apple_identifier(identifier, name)
    return unless identifier

    valid = Apple::ArtistIdValidator.valid?(
      {
        identifier: identifier,
        artist_id: creative.artist_id,
        name: name
      }.with_indifferent_access
    )

    valid ? identifier : nil
  # For lower ENVs, which don't have Transporter
  rescue RuntimeError => _e
    identifier
  end

  def spotify_identifier(identifier, name)
    return unless identifier

    valid = Spotify::ArtistIdValidator.valid?(name: name, id: identifier, spotify_name: nil)
    valid ? identifier : nil
  end

  def get_esid_state(service_attr)
    return service_attr["state"] if service_attr["state"]
    return ExternalServiceId::NEW_ARTIST if service_attr["create_page"]
  end
end
