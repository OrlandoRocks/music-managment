class ExternalServiceIds::ExternalServiceIdsFetcher
  def self.fetch(linkable, external_service)
    new(linkable, external_service).fetch
  end

  attr_accessor :linkable, :external_service

  def initialize(linkable, external_service)
    @linkable         = linkable
    @external_service = external_service
  end

  def fetch
    send("fetch_" + external_service)
  end

  private

  def fetch_spotify_uris
    service = SpotifyService.new
    service.search_by_upc([@linkable])
    album_uri = linkable.external_id_for("spotify")
    service.get_tracks_for_album(album_uri) if album_uri.present?
  end

  def fetch_apple_album_id
    Apple::AlbumItunesInfoWorker.perform_async(@linkable.id)
  end
end
