module CrtCache::Invalidator
  def self.invalidate_cache_if_changed(record)
    return unless FeatureFlipper.show_feature?(:crt_cache, nil)

    invalidate_cache(record) if cache_field_changed?(record) || album_unfinalized?(record)
  end

  def self.finalized?(record)
    if record.is_a?(Album)
      record.finalized?
    else
      record.album.finalized?
    end
  end

  def self.album_unfinalized?(record)
    if record.is_a?(Album)
      record.saved_change_to_finalized_at? && !record.finalized?
    else
      false
    end
  end

  def self.cache_field_changed?(record)
    return false unless finalized?(record)

    (record.saved_changes.transform_values(&:first).keys & monitored_fields).present?
  end

  def self.invalidate_cache(record)
    cache_object = record.is_a?(Album) ? record : record.album
    Tunecore::Crt.invalidate_album_in_cache(cache_object)
  end

  def self.monitored_fields
    %w[parental_advisory clean sale_date orig_release_year event relation_type]
  end
end
