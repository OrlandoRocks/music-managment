class NegativeBalanceAlertingService
  def self.alert(start_date = Date.yesterday)
    new(start_date).tap(&:alert)
  end

  attr_reader :negative_person_balances, :start_date

  def initialize(start_date)
    @start_date               = start_date
    @negative_person_balances = query_person_balances
  end

  def query_person_balances
    PersonBalance
      .where(person_balances[:balance].lteq(-200)
      .and(person_balances[:updated_at].lteq(start_date)))
  end

  def alert
    Rails.logger.info("Sending Emails about #{person_balances}.count person_balances")
    NegativeBalanceAlertMailer.alert_email(negative_person_balances, start_date).deliver
  end

  private

  def person_balances
    PersonBalance.arel_table
  end
end
