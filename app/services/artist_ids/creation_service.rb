class ArtistIds::CreationService
  APPLE_SERVICE          = "apple".freeze
  CREATIVE_TYPE          = "Creative".freeze
  MATCH_STATE            = "matched".freeze
  PRIMARY_ARTIST_ROLE    = "primary_artist".freeze
  SUPPORTED_SERVICES     = %w[apple spotify youtube_authorization youtube_channel_id].freeze

  ArtistUrl = Struct.new(:url, :service_name, :new_artist)

  def self.create(album, artist_urls_collection, person = nil)
    artist_urls_collection.each { |artist_urls| new(album, artist_urls, person).create }
    match(album.creatives)
  end

  # Fill the creatives' missing/bad ESIs with values from a matching artist-person combination
  def self.match(creatives)
    creatives.each do |creative|
      # Giving User ability to re-request new artist page on Spotify
      # Skip matching for new Spotify Artist even if ESI's already exist
      new_spotify_artist = creative.external_service_ids.spotify.find_by(state: "new_artist")
      next if new_spotify_artist

      matching_creatives = Creative
                           .joins(:artist)
                           .where(person_id: creative.person_id, artists: { name: creative.artist.name })

      good_esids = ExternalServiceId
                   .where.not(identifier: nil)
                   .where.not(identifier: "unavailable")
                   .where(linkable_id: matching_creatives.map(&:id), linkable_type: CREATIVE_TYPE)
                   .group(:service_name)

      good_esids.each do |good_esid|
        matching_creatives.each do |mc|
          esid = ExternalServiceId.find_or_create_by(
            linkable_id: mc.id,
            linkable_type: CREATIVE_TYPE,
            service_name: good_esid.service_name
          )
          next unless esid.identifier.nil? || esid.identifier == "unavailable"

          esid.update(
            identifier: good_esid.identifier,
            state: "matched"
          )
        end
      end
    end
  end

  attr_reader :album, :apple_url, :spotify_url, :artist_name

  def initialize(album, artist_urls, person = nil)
    @album       = album
    @apple_url   = ArtistUrl.new(artist_urls["apple"], "apple", artist_urls["apple_new_artist"])
    @spotify_url = ArtistUrl.new(artist_urls["spotify"], "spotify", artist_urls["spotify_new_artist"])
    @artist_name = artist_urls["artist_name"]
    @person = person
  end

  def create
    return unless creative

    handle_user_identifier_input

    # Create stub ESID for new artists, used for matching and backfills
    create_new_artist_service_ids
  end

  private

  # Legacy - not used by album_app, replaced by handle_esids
  # Parse URL for ID
  # Create/Update ESID if valid ID
  def handle_user_identifier_input
    eligible_artist_urls.each do |artist_url|
      service_name  = artist_url.service_name
      service_class = service_name.classify.constantize
      identifier    = service_class::ArtistIdExtractionService.extract_artist_id(artist_url.url)

      # No valid value from user
      next unless identifier

      identifier = handle_apple_identifier(identifier, creative) if apple_service?(service_name)
      external_service_id = creative.external_service_ids.by_service(service_name).first

      if external_service_id.present?
        external_service_id.update(identifier: identifier, state: nil)
      else
        creative.external_service_ids.create(
          service_name: service_name,
          identifier: identifier
        )
      end
    end
  end

  # Ensures existing identifier is nullified if there is a valid URL but it fails validation
  # with Transporter
  def handle_apple_identifier(identifier, creative)
    valid_status = Apple::ArtistIdValidator.new(
      {
        identifier: identifier,
        artist_id: creative.artist_id
      }.with_indifferent_access
    ).valid?

    valid_status ? identifier : nil
  end

  def create_new_artist_service_ids
    new_artist_services.each do |service_obj|
      create_new_artist_service_id(service_obj.service_name)
    end
  end

  def create_new_artist_service_id(service_name)
    return if service_with_identifier_exists(service_name)

    identifier = get_identifier(creative, service_name)
    creative.external_service_ids.find_or_create_by(
      service_name: service_name,
      state: "new_artist",
      identifier: identifier
    )
  end

  def get_identifier(creative, service_name)
    "unavailable" unless non_primary_for_apple?(creative, service_name)
  end

  def non_primary_for_apple?(creative, service_name)
    apple_service?(service_name) && !creative.is_primary?
  end

  def apple_service?(service_name)
    service_name == APPLE_SERVICE
  end

  def creative
    @creative ||= @album.creatives.joins(:artist).where(artists: { name: artist_name }).first
  end

  def eligible_artist_urls
    [apple_url, spotify_url].select { |artist_url| artist_url.url.present? }
  end

  def new_artist_services
    new_artists = []

    new_artists << spotify_url if spotify_enabled?

    new_artists << apple_url if apple_enabled?

    new_artists
  end

  def spotify_enabled?
    FeatureFlipper.show_feature?(:spotify_artist_ids, @person) &&
      spotify_url.new_artist == "true"
  end

  def apple_enabled?
    FeatureFlipper.show_feature?(:new_apple_artist_id_creation, @person) &&
      apple_url.new_artist == "true"
  end

  def service_with_identifier_exists(service_name)
    creative.external_service_ids.by_service(service_name)
            .where.not(external_service_ids: { identifier: nil })
            .where.not(external_service_ids: { identifier: "unavailable" }).any?
  end
end
