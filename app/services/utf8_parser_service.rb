class Utf8ParserService
  class NonEmojiFourByteCharError < StandardError; end

  attr_reader :class_name, :input

  EMOJI_REGEX = /\p{emoji}|
                 \p{emoji_presentation}|
                 \p{emoji_modifier}|
                 \p{emoji_modifier_base}|
                 \p{emoji_component}|
                 \p{extended_pictographic}/x.freeze

  def initialize(input, class_name)
    @class_name = class_name
    @input = input
    @four_byte_chars ||= four_byte_chars
    @non_emoji_four_byte_chars ||= non_emoji_four_byte_chars
    send_non_emoji_notification
  end

  def four_byte_chars
    return [] if input.blank?

    @four_byte_chars = input.each_char.select { |c| c.bytes.count == 4 }
  end

  def non_emoji_four_byte_chars
    @non_emoji_four_byte_chars = @four_byte_chars.reject { |c| EMOJI_REGEX.match?(c) }
    return [] unless @non_emoji_four_byte_chars.any?

    @non_emoji_four_byte_chars
  end

  private

  def send_non_emoji_notification
    return unless @non_emoji_four_byte_chars.any?

    Airbrake.notify(
      NonEmojiFourByteCharError,
      {
        class_name: class_name,
        non_emoji_four_byte_chars: @non_emoji_four_byte_chars
      }
    )
  end
end
