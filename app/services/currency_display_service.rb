class CurrencyDisplayService
  # may want to check these with the db in the future - as of now we don't have the information in the db
  # once we settle on the current conversation around the country_websites table (whether to have India's
  # country website currency be INR or USD) we may be able to use that
  def initialize(person)
    @person = person
    @country_website = @person.country_website
  end

  def multiple_currency_domain?
    @country_website.non_native_currency_site?
  end

  def display_currency
    @person.user_currency
  end

  def transaction_currency
    @person.site_currency
  end
end
