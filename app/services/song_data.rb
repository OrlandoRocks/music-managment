class SongData
  include ActiveModel::Serialization
  attr_writer :name

  def self.build(song, person_id, artist_names)
    new(song, person_id, artist_names).tap(&:build)
  end

  attr_reader :id,
              :name,
              :language_code_id,
              :translated_name,
              :version,
              :cover_song,
              :made_popular_by,
              :explicit,
              :clean_version,
              :optional_isrc,
              :lyrics,
              :asset_url,
              :spatial_audio_asset_url,
              :album_id,
              :song,
              :person_id,
              :artist_names,
              :track_number,
              :previously_released_at,
              :asset_filename,
              :atmos_asset_filename,
              :instrumental,
              :bigbox_disabled

  def initialize(song, person_id, artist_names)
    @song         = song
    @person_id    = person_id
    @artist_names = artist_names
  end

  def build
    @id                     = song.id
    @name                   = song.name
    @language_code_id       = language_code_id
    @version                = song.song_version
    @cover_song             = song.cover_song?
    @made_popular_by        = song.made_popular_by
    @explicit               = song.parental_advisory
    @clean_version          = song.clean_version
    @optional_isrc          = song.isrc
    @artists                = artists
    @lyrics                 = song.lyric.try(:content)
    @album_id               = song.album_id
    @asset_url              = song.s3_url(36_000) if song.s3_asset
    @spatial_audio_asset_url = song.spatial_audio_url(36_000) if song.spatial_audio_asset
    @asset_filename         = song.s3_asset.key[/-(.*)/, 1] if song.s3_asset
    @atmos_asset_filename   = song.spatial_audio_asset.key[/-(.*)/, 1] if song.spatial_audio_asset
    @translated_name        = song.translated_name
    @track_number           = song.track_num
    @previously_released_at = song.previously_released_at
    @instrumental           = song.instrumental
    @copyrights             = copyrights
    @song_copyright_claim   = song_copyright_claims
    @song_start_times       = song_start_times
    @cover_song_metadata    = cover_song_metadata
    @bigbox_disabled        = bigbox_disabled?
  end

  def artists
    SongArtist.for_song(song)
  end

  def copyrights
    song.copyright
  end

  def song_copyright_claims
    song.song_copyright_claim
  end

  delegate :song_start_times, to: :song

  delegate :cover_song_metadata, to: :song

  def name?
    name.present?
  end

  def asset_url?
    asset_url.present?
  end

  def language_code_id
    song.lyric_language_code_id || song.album&.language&.id || default_language_code_id
  end

  def default_language_code_id
    CountryWebsite::LANGUAGE_CODE_MAP[country_website_id]
  end

  def country_website_id
    Person.find(person_id).country_website_id
  end

  def bigbox_disabled?
    FeatureFlipper.show_feature?(:disable_bigbox_uploader, person_id)
  end
end
