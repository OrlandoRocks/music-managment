class TrackMonetization::Deliverer
  attr_accessor :track_monetizations_by_album_store_delivery_types

  DELIVERY_TYPE_TAKEDOWN = "takedown".freeze

  def self.build(track_monetizations_by_album_store_delivery_types: TrackMonetization.for_tc_distributor_delivery)
    new(track_monetizations_by_album_store_delivery_types: track_monetizations_by_album_store_delivery_types)
  end

  def initialize(track_monetizations_by_album_store_delivery_types:)
    @track_monetizations_by_album_store_delivery_types = track_monetizations_by_album_store_delivery_types
  end

  def call
    track_monetizations_by_album_store_delivery_types.each do |track_monetizations_by_album_delivery_type|
      track_monetizations = track_monetizations_by_album_delivery_type[1]
      store_id = track_monetizations_by_album_delivery_type[0][:store_id]
      store_ids = Array(store_id)
      delivery_type = track_monetizations_by_album_delivery_type[0][:delivery_type]
      topic_action = delivery_type.to_s == "metadata_only" ? "RETRY" : "NEW"
      topic_arn =
        ENV.fetch("SNS_RELEASE_#{topic_action}_URGENT_TOPIC", ENV["SNS_RELEASE_RETRY_URGENT_TOPIC"])

      Sns::Notifier.perform(
        topic_arn: topic_arn,
        album_ids_or_upcs: Array(track_monetizations_by_album_delivery_type[0][:album_id]),
        store_ids: store_ids,
        delivery_type: delivery_type,
        song_ids: Array(track_monetizations.pluck(:song_id)),
        person_id: track_monetizations.last&.person_id,
        # Removing Dual Deliveries for Track Monetization
        # is_dual_delivery: deliver_with_album?(track_monetizations_by_album_delivery_type)
      )

      track_monetizations.each do |track_monetization|
        next unless track_monetizations_by_album_delivery_type[0][:delivery_type] == DELIVERY_TYPE_TAKEDOWN ||
                    track_monetization.check_eligibility

        if track_monetization.delivered_via_tc_distributor?
          track_monetization.update(tc_distributor_state: "enqueued")
          next
        end

        # We do not need any update for this delivery from tc-distributor as the delivery happened via tc-www
        track_monetization.update(tc_distributor_state: "new")
      end
    end
  end

  # NOTE: regular album deliveries for Facebook(100) and YouTube(28) will trig deliveries
  # to track monetization stores. In that case, we do not want a track monetization delivery
  # to deliver the album to the regular stores again.
  # To prevent that behavior, we use a dedicated state :start_without_album (vs :start)
  # to exclude that dual delivery.
  # Also, the album delivery should not occur in case of a takedown track monetization delivery.
  def deliver_with_album?(track_monetizations_by_album_delivery_type)
    track_monetizations_by_album_delivery_type[0][:delivery_type] != DELIVERY_TYPE_TAKEDOWN &&
      track_monetizations_by_album_delivery_type[1].select { |track|
        track.delivered_via_tc_distributor? && track.tc_distributor_start_without_album?
      }.none?
  end
end
