class TrackMonetization::InstagramDeliveryService
  def self.deliver(params)
    new(params).tap(&:put_it_on_the_gram)
  end

  def initialize(params)
    @albums       = params[:albums]
    @ig           = Store.find_by(short_name: "Instagram")
    @person_name  = params[:person_name]
  end

  def put_it_on_the_gram
    @albums.each do |album|
      new_salepoints, existing_salepoints = album.add_stores([@ig])

      salepoint = album.salepoints_by_store(@ig).first
      next if salepoint.errors.any?

      salepoint.update(payment_applied: true, finalized_at: Time.now) if new_salepoints.present?

      distro = Distribution.create!(
        state: "new",
        converter_class: @ig.converter_class,
        delivery_type: "full_delivery"
      )

      distro.salepoints << salepoint
      album.petri_bundle.distributions << distro

      distro.start(actor: @person_name.to_s, message: "Starting distribution")
    end
  end
end
