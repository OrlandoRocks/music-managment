class TrackMonetization::EligibilityEngineService
  def initialize(track_monetization)
    @store = track_monetization.store
    @song = track_monetization.song
    @track_monetization = track_monetization
  end

  def validate!
    store.ineligibility_rules.each do |rule|
      next unless rule.matches?(song)

      track_monetization.ineligibility_rules << rule

      create_note(rule)

      rule_message = [rule.property, rule.operator, rule.value].join(" ")

      track_monetization.block_and_disqualify(
        {
          actor: "TrackMonetization::EligibilityEngineService",
          message: "ineligibility_rule: #{rule_message}"
        }
      )
    end if track_monetization.manual_approval.nil?

    track_monetization.update(eligibility_status: "eligible") unless track_monetization.ineligible?
  end

  private

  attr_reader :store, :song, :track_monetization

  def create_note(rule)
    store = track_monetization.store.name
    note = "Song isrc: #{song.isrc} for store, #{store}, was blocked " \
      "due to #{rule.property} #{rule.operator} #{rule.value}"

    song.album.notes.create!(
      note_created_by_id: 0,
      subject: "Automatically Blocked Track Monetization".freeze,
      note: note
    )
  end
end
