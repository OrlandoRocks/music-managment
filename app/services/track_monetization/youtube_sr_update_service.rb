class TrackMonetization::YoutubeSrUpdateService
  attr_reader :album, :track_monetizations, :art_track_distribution

  def self.deliver_sr_metadata_update(album)
    new(album).tap(&:deliver_sr_metadata_update)
  end

  def self.send_track_album(album)
    new(album).tap(&:send_track_album)
  end

  def initialize(album)
    @album = album
    @track_monetizations = monetized_ytsr_tracks
    @art_track_distribution = fetch_art_track_distribution
  end

  def deliver_sr_metadata_update
    return unless has_youtube_tracks?

    track_monetizations.each do |track|
      track.update(delivery_type: "metadata_only")
      deliver_metadata_update(track)
    end
  end

  def send_track_album
    if send_full_album?
      create_art_track_distribution
    elsif art_track_distribution
      deliver_metadata_update(art_track_distribution)
    else
      Rails.logger.info("No Art Track Update was triggered for album id #{album.id}")
    end
  end

  def deliver_metadata_update(distribution)
    Delivery::DistributionWorker.set(queue: ENV["METADATA_UPDATE_QUEUE"] || "delivery-default")
                                .perform_async(distribution.class.name, distribution.id, "metadata_only")
  end

  def create_art_track_distribution
    DistributionCreator.create(album, "Google")
  end

  private

  def has_youtube_tracks?
    monetized_ytsr_tracks.present?
  end

  def send_full_album?
    album.salepoints.where.not(finalized_at: nil).where(store_id: Store::GOOGLE_STORE_ID).blank?
  end

  def monetized_ytsr_tracks
    album.track_monetizations.where(
      {
        store_id: Store::YTSR_STORE_ID,
        eligibility_status: TrackMonetization::ELIGIBLE,
        state: ["delivered", "enqueued"]
      }
    )
  end

  def fetch_art_track_distribution
    album.distributions.where(state: "delivered")
         .where("distributions.updated_at < ?", 1.day.ago)
         .joins(:salepoints)
         .where(salepoints: {
                  store_id: Store::GOOGLE_STORE_ID
                }).last
  end
end
