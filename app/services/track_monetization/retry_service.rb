class TrackMonetization::RetryService
  def self.retry(params)
    new(params).tap(&:retry_delivery)
  end

  def initialize(params)
    @current_user  = Person.find(params[:current_user_id])
    @delivery_type = params[:delivery_type]
    @track_mon     = TrackMonetization.find(params[:track_mon_id])
    @priority      = 1
  end

  def retry_delivery
    return unless @track_mon.eligible?

    @track_mon.update(retry_count: @track_mon.retry_count.to_i + 1) if @priority == 1
    Delivery::Retry.retry(
      delivery: @track_mon,
      delivery_type: @delivery_type,
      person: @current_user,
      priority: @priority
    )
  end
end
