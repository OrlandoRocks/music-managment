class TrackMonetization::StateUpdater
  attr_reader :track_monetizations, :song_ids

  ACCEPTABLE_STATES = ["error", "delivered"].freeze
  def self.build(track_monetizations: TrackMonetization.enqueued_tc_distributor_delivery)
    new(track_monetizations: track_monetizations)
  end

  def initialize(track_monetizations:)
    @track_monetizations = track_monetizations
    @song_ids = Array(track_monetizations.pluck(:song_id))
  end

  def call
    return if track_monetizations.empty?

    track_monetizations.upsert_all(updatable_track_monetizations) unless updatable_track_monetizations.empty?  # rubocop:disable Rails/SkipsModelValidations
  end

  private

  def releases_stores
    @releases_stores ||=
      DistributorAPI::ReleasesStore.list(
        {
          song_id: song_ids,
          greatest_song: true
        }
      )[0].http_body&.dig("releases_stores")
  end

  def track_monetization_by_song_and_store_ids
    @track_monetization_by_song_and_store_ids ||=
      track_monetizations.each_with_object({}) do |track_monetization, h|
        h[[track_monetization.song_id, track_monetization.store_id]] =
          track_monetization.attributes
      end
  end

  def updatable_track_monetizations
    @updatable_track_monetizations ||=
      releases_stores.each_with_object([]) do |releases_store, a|
        track_monetization_h =
          track_monetization_by_song_and_store_ids[[releases_store["song_id"], releases_store["store_id"]]]
        next unless track_monetization_h

        a << {
          id: track_monetization_h["id"],
          tc_distributor_state: releases_store["state"],
          created_at: track_monetization_h["created_at"],
          updated_at: DateTime.current
        } if ACCEPTABLE_STATES.include?(releases_store["state"])
      end
  end
end
