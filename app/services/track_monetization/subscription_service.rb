class TrackMonetization::SubscriptionService
  include ArelCommonJoins

  STORE_ID_DIRECTORY = {
    "FBTracks" => Store::FBTRACKS_STORE_ID,
    "YTTracks" => Store::YTSR_STORE_ID
  }

  def self.subscribe(params)
    new(params).tap(&:create_subscription)
  end

  def initialize(params)
    @params    = params.with_indifferent_access
    @person    = Person.find(@params["person_id"])
    @service   = @params["service"]
    @send_all  = @params["send_all"]
    @store_id  = Store.find_by(short_name: @service)&.id || STORE_ID_DIRECTORY[@service]
  end

  def create_subscription
    create_track_monetizations
  end

  def create_track_monetizations
    track_mons =
      potentially_monetizable_songs.map do |song|
        next if TrackMonetization.exists?(song_id: song.id, store_id: @store_id)

        TrackMonetization.create(
          song_id: song.id,
          person_id: @person.id,
          store_id: @store_id,
          state: "new",
          delivery_type: "full_delivery",
          eligibility_status: "pending"
        )
      end

    check_eligibility_and_send(track_mons, @send_all)
  end

  def check_eligibility_and_send(tracks, send_all = nil)
    albums = []

    tracks.each do |track|
      track.check_eligibility

      if track.eligible? && send_all
        track.start(actor: @person.name.to_s, message: "Starting distribution")
        albums << track.album unless albums.include?(track.album)
      end
    end

    deliver_albums_to_instagram(albums) if @service == "FBTracks"
  end

  def deliver_albums_to_instagram(albums)
    params = { albums: albums, person_name: @person.name }
    TrackMonetization::InstagramDeliveryService.deliver(params)
  end

  def potentially_monetizable_songs
    if @service == "YTTracks"
      @person.albums.youtube_monetizable
             .joins(albums_to_songs)
             .joins(songs_yttracks_outer_join)
             .where(track_monetization_t[:id].eq(nil))
             .select(song_t[Arel.star])
    else
      @person.albums.facebook_monetizable
             .joins(albums_to_songs)
             .joins(songs_fbtracks_outer_join)
             .where(track_monetization_t[:id].eq(nil))
             .select(song_t[Arel.star])
    end
  end
end
