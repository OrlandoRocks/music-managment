# frozen_string_literal: true

class TrackMonetization::TakedownService
  NOTE_BY_STORE = {
    takedown: {
      "YoutubeSR" => "YTSR Takedown",
      "FBTracks" => "FBTracks Takedown"
    },
    remove_takedown: {
      "YoutubeSR" => "YTSR Remove Takedown",
      "FBTracks" => "FBTracks Remove Takedown"
    }
  }

  def self.takedown(params)
    new(params).tap(&:takedown)
  end

  def self.remove_takedown(params)
    new(params).tap(&:remove_takedown)
  end

  def initialize(params)
    @current_user        = params[:current_user] if params[:current_user]
    @track_monetizations = params[:track_mons] || params[:track_monetizations]
    @takedown_source     = params[:takedown_source] if params[:takedown_source]
  end

  def delivery_actor
    if @current_user.present?
      @current_user.is_administrator? ? "Admin: #{@current_user.name}" : @current_user.name
    else
      @takedown_source
    end
  end

  def takedown
    @track_monetizations.each do |track|
      track.update(delivery_type: "metadata_only", takedown_at: DateTime.now)
      deliver_takedown("takedown", track)
      note = NOTE_BY_STORE[:takedown].fetch(track.store.short_name, nil)
      create_note(track, note) unless note.nil?
    end
  end

  def remove_takedown
    @track_monetizations.each do |track|
      # Send full delivery request in case of untakedown
      track.update(delivery_type: "full_delivery", takedown_at: nil)
      track.mark_eligible_and_destroy_blocker
      deliver_takedown("remove takedown", track)
      note = NOTE_BY_STORE[:remove_takedown].fetch(track.store.short_name, nil)
      create_note(track, note) unless note.nil?
    end
  end

  def deliver_takedown(takedown_type, track)
    track.start(
      actor: delivery_actor,
      message: "Delivering #{takedown_type} for Track Monetization: #{track.id}"
    )
  end

  def create_note(track, note)
    Note.create(
      note: note + ": Song Isrc - #{track.song.isrc}",
      subject: note,
      related: track.song.album,
      note_created_by: @current_user,
    )
  end
end
