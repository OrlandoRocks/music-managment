class TrackMonetization::CreationService
  attr_reader :album, :store

  def initialize(params)
    @album = Album.find(params[:album_id])
    @store = set_store(params[:store_id])
  end

  def self.monetize_album(params)
    new(params).create_monetizations
  end

  def create_monetizations
    return false unless store

    album.songs.each do |song|
      track = TrackMonetization.find_or_create_by(
        song_id: song.id,
        person_id: album.person.id,
        store_id: store.id
      )
      if track.track_delivered?
        Rails.logger.info("TrackMonetization already delivered. Track id: #{track.id}")
        next
      end
      track.update(state: "new") unless track.state
      track.check_eligibility
      track.start(actor: "Track Monsetization Creation Service", message: "Starting distribution") if track.eligible?
    end
  end

  def set_store(store_id)
    return unless [Store::FBTRACKS_STORE_ID, Store::YTSR_STORE_ID].include? store_id

    Store.find(store_id)
  end
end
