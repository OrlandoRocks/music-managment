class BackfillDistribution
  attr_accessor :source_store_id, :destination_store_id, :store

  def initialize(source_store_id, destination_store_id)
    @source_store_id = source_store_id
    @destination_store_id = destination_store_id
    validate!
    @store = Store.find(destination_store_id)
  end

  def run
    salepoint_ids = Salepoint
                    .where(store_id: destination_store_id, salepointable_type: "Album")
                    .pluck(:id)

    salepoint_with_distribution_ids = DistributionSalepoint
                                      .where(salepoint_id: salepoint_ids)
                                      .pluck(:salepoint_id)

    salepoints = Salepoint
                 .includes(salepointable: [salepoints: :distributions])
                 .where(store_id: destination_store_id, salepointable_type: "Album")
                 .where.not(id: salepoint_with_distribution_ids)

    salepoints.find_each do |salepoint|
      petri_bundle_id = get_petri_bundle_id(salepoint)
      next if petri_bundle_id.nil?

      distribution = create_distribution(petri_bundle_id)
      distribution.salepoints << salepoint
      p "Salepoint #{salepoint.id}"
    end
  end

  def get_petri_bundle_id(salepoint)
    return if salepoint.distributions.present?

    copy_salepoint = salepoint.salepointable.salepoints.where(store_id: source_store_id)&.first
    return if copy_salepoint.nil? || copy_salepoint.distributions.empty?

    copy_salepoint&.distributions&.first&.petri_bundle_id
  end

  def create_distribution(petri_bundle_id)
    Distribution.create!(
      state: "new",
      converter_class: store.converter_class,
      delivery_type: "full_delivery",
      petri_bundle_id: petri_bundle_id
    )
  end

  def validate!
    raise "Please provide source_store_id and destination_store_id" if source_store_id.nil? && destination_store_id.nil?

    raise "Please provide source_store_id" if source_store_id.nil?
    raise "Please provide destination_store_id" if destination_store_id.nil?

    raise "Source_store_id and destination_store_id should be different" if source_store_id == destination_store_id

    raise "Source store does not exist" unless Store.exists?(id: source_store_id)
    raise "Destination store does not exist" unless Store.exists?(id: destination_store_id)
  end
end
