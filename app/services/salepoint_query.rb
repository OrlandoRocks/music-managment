# Helpers for querying salepoints before purchase.
module SalepointQuery
  # It exists, regardless of payment.
  def salepoint?(salepoints, store_id)
    salepoints.find { |sp| sp.store_id == store_id }.present?
  end

  # It is the only salepoint, regardless of payment. Ignores Believe salepoint.
  def only_salepoint?(salepoints, store_id)
    salepoint?(salepoints, store_id) &&
      salepoints
        .find { |sp| sp.store_id != Store::BELIEVE_STORE_ID && sp.store_id != store_id }
        .nil?
  end

  def only_new_salepoint?(salepoints, store_id)
    salepoint?(salepoints, store_id) &&
      salepoints
        .find { |sp| sp.store_id != Store::BELIEVE_STORE_ID && sp.store_id != store_id && !sp.payment_applied }
        .nil?
  end

  # Salepoints includes other than provided store_id
  def other_salepoints?(salepoints, store_id)
    salepoint?(salepoints, store_id) && !only_salepoint?(salepoints, store_id)
  end
end
