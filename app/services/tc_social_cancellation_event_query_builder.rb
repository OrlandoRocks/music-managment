class TcSocialCancellationEventQueryBuilder
  attr_accessor :query, :report_form

  def self.build_query(report_form)
    query_conditions = collect_query_methods(report_form)
    query_builder    = new(report_form: report_form)
    query_conditions.inject(query_builder) { |acc, elem| acc.send(elem) }.query
  end

  def self.collect_query_methods(report_form)
    report_form.attributes.keys.map do |key|
      "with_#{key}" if report_form.attributes[key].present?
    end.compact
  end

  def initialize(options)
    @report_form = options[:report_form]
    @query       = options[:query] || base_query
  end

  def base_query
    SubscriptionEvent
      .includes([:subscription_purchase, { person_subscription_status: :person }])
      .where("subscription_events.event_type = 'Cancellation' AND subscription_events.subscription_type = 'Social'")
  end

  def with_person_id
    reflect(
      query.joins(person_subscription_status: :person).where(people[:id].eq(report_form.person_id))
    )
  end

  def with_person_email
    reflect(
      query.joins(person_subscription_status: :person)
        .where(people[:email]
        .eq(report_form.person_email))
    )
  end

  def with_subscription_created
    date = Date.strptime(report_form.subscription_created, "%m/%d/%Y")
    reflect(
      query.joins(:subscription_purchase)
        .where(subscription_purchases[:created_at]
        .in(date.midnight..date.end_of_day))
    )
  end

  def with_subscription_cancelled
    date = Date.strptime(report_form.subscription_cancelled, "%m/%d/%Y").to_time
    reflect(
      query.where(
        subscription_events[:created_at]
                .in(date.midnight..date.end_of_day)
      )
    )
  end

  def with_expected_renewal
    date = Date.strptime(report_form.expected_renewal, "%m/%d/%Y")
    reflect(
      query.joins(:person_subscription_status)
        .where(person_subscription_statuses[:termination_date]
        .in(date.midnight..date.end_of_day))
    )
  end

  def with_start_date
    date = Date.strptime(report_form.start_date, "%m/%d/%Y")
    reflect(
      query.where(
        subscription_events[:created_at]
                .gteq(date.midnight)
      )
    )
  end

  def with_end_date
    date = Date.strptime(report_form.end_date, "%m/%d/%Y")
    reflect(
      query.where(
        subscription_events[:created_at]
                .lteq(date.midnight)
      )
    )
  end

  private

  def subscription_events
    SubscriptionEvent.arel_table
  end

  def people
    Person.arel_table
  end

  def subscription_purchases
    SubscriptionPurchase.arel_table
  end

  def person_subscription_statuses
    PersonSubscriptionStatus.arel_table
  end

  def reflect(query)
    self.class.new({ report_form: report_form, query: query })
  end
end
