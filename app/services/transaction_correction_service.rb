class TransactionCorrectionService
  attr_reader :correctable_transactions, :current_person, :corrections

  CHAIN_VALIDITY_PRECISION = 6

  # NOTE: CONTIGUOUS TRANSACTIONS ONLY & THE NET EFFECT OF THE CORRECTIONS HAS TO AMOUNT TO ZERO!
  # corrections = [{id: 1, debit: 10.0, credit: 10.0}]
  def initialize(corrections)
    @corrections              = corrections
    @correctable_transactions = PersonTransaction
                                .where(id: corrections.pluck(:id))
                                .order(:created_at)

    @current_person           = correctable_transactions.take.person
  end

  def exec!
    if correction_integrity.eql?(:valid_correction)
      update_transaction!
    else
      log_error!
    end

    correction_integrity
  end

  private

  def update_transaction!
    ActiveRecord::Base.transaction do
      corrections.reduce(previous_transaction_adjusted_balance) do |curr_prev_balance, correction|
        # rubocop:disable Rails/SkipsModelValidations
        correctable_transactions.find_by(id: correction[:id]).update_columns(
          previous_balance: curr_prev_balance,
          debit: correction[:debit],
          credit: correction[:credit]
        )
        # rubocop:enable Rails/SkipsModelValidations

        (curr_prev_balance + correction[:credit]) - correction[:debit]
      end

      unless post_correction_transaction_chain_valid?
        log_error!
        raise ActiveRecord::Rollback
      end
    end
  end

  def correction_integrity
    return :transaction_not_found      if correctable_transactions.blank?
    return :net_effect_non_zero        unless transactions_net_effect_zero?
    return :non_contiguous_transaction unless transactions_contiguous?
    return :breaks_transaction_chain   unless pre_correction_transaction_chain_valid?

    :valid_correction
  end

  def transactions_net_effect_zero?
    corrections.pluck(:debit).sum.eql?(corrections.pluck(:credit).sum)
  end

  def transactions_contiguous?
    correctable_transaction_indices.each_cons(2).all? do |prev_id, curr_id|
      curr_id.eql?(prev_id.next)
    end
  end

  def pre_correction_transaction_chain_valid?
    if next_transaction.present?
      correctable_transactions_adjusted_previous_balance
        .round(CHAIN_VALIDITY_PRECISION)
        .eql?(next_transaction.previous_balance.round(CHAIN_VALIDITY_PRECISION))
    else
      correctable_transactions_adjusted_previous_balance
        .round(CHAIN_VALIDITY_PRECISION)
        .eql?(current_person_balance.round(CHAIN_VALIDITY_PRECISION))
    end
  end

  def post_correction_transaction_chain_valid?
    if next_transaction.present?
      adjusted_balance(correctable_transactions.reload.max)
        .round(CHAIN_VALIDITY_PRECISION)
        .eql?(next_transaction.previous_balance.round(CHAIN_VALIDITY_PRECISION))
    else
      adjusted_balance(correctable_transactions.reload.max)
        .round(CHAIN_VALIDITY_PRECISION)
        .eql?(current_person_balance.round(CHAIN_VALIDITY_PRECISION))
    end
  end

  def current_person_balance
    current_person.person_balance.balance
  end

  def correctable_transactions_adjusted_previous_balance
    corrections.reduce(previous_transaction_adjusted_balance) do |adj_balance, correction|
      (adj_balance + correction[:credit]) - correction[:debit]
    end
  end

  def previous_transaction_adjusted_balance
    return 0 if previous_transaction.blank?

    adjusted_balance(previous_transaction)
  end

  def adjusted_balance(transaction)
    (transaction.previous_balance + transaction.credit) - transaction.debit
  end

  def next_transaction
    @next_transaction ||= person_transaction_chain[correctable_transaction_indices.max.next]
  end

  def previous_transaction
    @previous_transaction ||= person_transaction_chain[correctable_transaction_indices.min.pred]
  end

  def person_transaction_chain
    @person_transaction_chain ||= PersonTransaction
                                  .where(person_id: current_person.id)
                                  .order(:created_at)
                                  .to_a
  end

  def correctable_transaction_indices
    @correctable_transaction_indices ||=
      correctable_transactions.map do |correctable_transaction|
        person_transaction_chain.index(correctable_transaction)
      end
  end

  def log_error!
    Rails.logger.error <<-ERROR
      TRANSACTION IDS: #{correctable_transactions.pluck(:id).join(',')} CANNOT BE ADJUSTED.
      REASON: #{correction_integrity}
    ERROR
  end
end
