class DistributionApi::S3Downloader
  attr_accessor :key, :bucket, :asset_url

  def initialize(key: nil, bucket: nil, asset_url: nil)
    @key = key
    @bucket = bucket
    @asset_url = asset_url
    parse_asset_url if asset_url
  end

  def download
    Rails.logger.info "Downloading File #{key} from S3"

    begin
      File.open(local_path, "wb") do |file|
        S3_CLIENT.client.get_object(
          bucket_name: bucket,
          key: key
        ) do |chunk|
          file.write(chunk)
        end
      end
    rescue StandardError => e
      Airbrake.notify("Failed to download File from key: #{key}, bucket: #{bucket}", e)
      raise "Failed to download File from key: #{key}, bucket: #{bucket}, error: #{e.message}"
    end

    local_path
  end

  def cleanup
    File.delete(local_path) if File.exist?(local_path)
  end

  def local_path
    return @local_path if @local_path.present?

    @local_path = Rails.root.join("tmp", "#{SecureRandom.uuid}/#{File.basename(key)}")
    FileUtils.mkdir_p(File.dirname(@local_path))
    @local_path
  end

  def parse_asset_url
    @bucket = asset_uri.hostname
    @key = asset_uri.path[1..-1]
  end

  def asset_uri
    @asset_uri ||= URI.parse(asset_url)
  end
end
