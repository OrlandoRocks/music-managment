# frozen_string_literal: true

class DistributionApi::Update::Song
  include DistributionApi::Helpers::SongsHelper

  LOCKED_SONG_ATTRS = %i[
    id
    instrumental
    song_start_times
    track_number
  ]

  def self.update(json_parser:, tunecore_album:)
    new(
      json_parser: json_parser,
      tunecore_album: tunecore_album
    ).update
  end

  attr_reader :json_parser, :tunecore_album

  delegate :person, to: :tunecore_album

  def initialize(json_parser:, tunecore_album:)
    @json_parser = json_parser
    @tunecore_album = tunecore_album
  end

  def update
    songs_params.each do |song_params|
      sdf = SongDataForm.new(update_params(song_params))
      raise DistributionApi::Errors::IngestionError.new(
        error_messages: ["Unable to save Song - #{sdf.errors.full_messages}"],
        error_state: "technical_error",
        source_song_id: song_params[:source_song_id]
      ) unless sdf.save

      upload_asset(sdf.song, song_params[:asset_url], song_params[:source_song_id])
    end
  end

  private

  # Songs keyed by source_song_id
  def saved_songs
    @saved_songs ||=
      tunecore_album
      .songs
      .includes(:distribution_api_song, creatives: [:creative_song_roles])
      .index_by { |s| s.distribution_api_song.source_song_id }
  end

  # Overwrite all incoming values (via tunecore_song_attributes) with persisted values,
  # except those that are permitted for updates.
  def update_params(song_params)
    saved_song = saved_songs[song_params[:source_song_id].to_s]

    result = creation_params(song_params)
    result[:song_params] = result[:song_params].merge!(saved_song.slice(*LOCKED_SONG_ATTRS).symbolize_keys)
    result[:song_params][:lyrics] = saved_song&.lyric&.content

    result
  end
end
