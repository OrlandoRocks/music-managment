# frozen_string_literal: true

class DistributionApi::Update::Base
  CRT_REQUIREMENTS = %i[
    finalize
    mark_as_needs_review!
    calculate_steps_required
  ]

  attr_reader :json_parser, :tunecore_album

  def initialize(json_parser:, tunecore_album:)
    @json_parser    = json_parser
    @tunecore_album = tunecore_album
  end

  def run
    update_artwork
    update_album
    update_songs
    send_to_crt

    true
  end

  def update_artwork
    DistributionApi::Ingest::Artwork.ingest(json_parser, tunecore_album)
  end

  def update_album
    DistributionApi::Update::Album.update(
      json_parser: json_parser,
      tunecore_album: tunecore_album
    )
  end

  def update_songs
    DistributionApi::Update::Song.update(
      json_parser: json_parser,
      tunecore_album: tunecore_album
    )
  end

  def send_to_crt
    CRT_REQUIREMENTS.each { |r| tunecore_album.send(r) }
  end

  def person
    @person ||= Person.find(ENV["BYTEDANCE_USER_ID"])
  end
end
