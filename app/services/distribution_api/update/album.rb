# frozen_string_literal: true

class DistributionApi::Update::Album
  include DistributionApi::Helpers::AlbumHelper

  LOCKED_ALBUM_ATTRS = %i[
    album_type
    golive_date
    optional_upc_number
    orig_release_year
    recording_location
    sale_date
    timed_release_timing_scenario
  ]

  def self.update(json_parser:, tunecore_album:)
    new(
      json_parser: json_parser,
      tunecore_album: tunecore_album
    ).update
  end

  attr_reader :json_parser, :tunecore_album

  delegate :person, to: :tunecore_album

  def initialize(json_parser:, tunecore_album:)
    @json_parser = json_parser
    @tunecore_album = tunecore_album
  end

  def update
    scrub_artist_names

    @album_form = AlbumForm.new(update_params)

    unless @album_form.save
      raise DistributionApi::Errors::IngestionError.new(
        error_messages: ["Unable to save Album #{@album_form.errors.full_messages}"],
        error_state: "technical_error"
      )
    end

    create_esids(tunecore_album, creatives_params)

    tunecore_album
  end

  private

  # Overwrite all incoming values (via tunecore_album_attributes) with persisted values,
  # except those that are permitted for updates.
  def update_params
    result = creation_params

    result.deep_merge!(album_params: locked_values)
  end

  def locked_values
    tunecore_album.slice(*LOCKED_ALBUM_ATTRS).symbolize_keys
  end
end
