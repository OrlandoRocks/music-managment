# frozen_string_literal: true

class DistributionApi::JsonParser
  TIKTOK_STORE_ID = 87

  attr_reader :raw_json

  def initialize(raw_json)
    @raw_json = raw_json
  end

  def parsed_json
    @parsed_json = JSON.parse(raw_json, object_class: OpenStruct)
  end

  # Some values are hard-coded for compatibility with the AlbumForm
  def tunecore_album_attributes
    {
      apple_music: deliver_to_apple_music?,
      clean_version: nil,
      creatives: album_creatives_attributes,
      golive_date: album_golive_date,
      is_various: various_artists,
      label_name: label_name,
      language_code: metadata_language,
      name: title,
      optional_isrc: "",
      optional_upc_number: upcs&.find { |upc| upc.type == "optional" }&.upc || "",
      orig_release_year: "",
      parental_advisory: nil,
      primary_genre_id: primary_genre_id,
      recording_location: "",
      sale_date: "",
      secondary_genre_id: secondary_genre_id,
      specialized_release_type: "",
      timed_release_timing_scenario: timed_release_type.presence || "relative_time"
    }
  end

  def distribution_api_album_attributes
    {
      distribution_api_service_id: distribution_api_service.id,
      source_user_id: source_user_id,
      source_album_id: source_album_id,
      source_contentreview_notes: source_contentreview_notes
    }
  end

  def stores
    store_ids = album_stores.map(&:store_id) - stores_failing_mp3_validation - [Store::SHAZAM_STORE_ID]
    Store.where(id: store_ids)
  end

  def stores_failing_mp3_validation
    return [] unless tunecore_song_attributes(nil).pluck(:asset_url).any? { |song| song.ends_with?(".mp3") }

    [Store::QOBUZ_STORE_ID]
  end

  def album_creatives_attributes
    artists.map do |artist|
      {}.tap do |hash|
        hash[:name] = artist.name
        hash[:role] = "primary_artist" # TODO: Get this value from the JSON in case new roles get added in the future
        hash[:apple] = {}
        hash[:spotify] = {}
        generate_apple_artist_id(hash, artist)
        generate_spotify_artist_id(hash, artist)
      end
    end
  end

  def generate_spotify_artist_id(hash, artist)
    return hash if artist.spotify_artist_id.blank?

    hash[:spotify][:create_page] = true if artist.spotify_artist_id == "NEW"
    hash[:spotify][:identifier] = artist.spotify_artist_id if artist.spotify_artist_id != "NEW"

    hash
  end

  def generate_apple_artist_id(hash, artist)
    return hash if artist.apple_artist_id.blank?

    hash[:apple][:create_page] = true if artist.apple_artist_id == "NEW"
    hash[:apple][:identifier] = artist.apple_artist_id if artist.apple_artist_id != "NEW"

    hash
  end

  def album_golive_date
    return {} if album_release_date.blank?

    golive_date = Time.parse(album_release_date)

    {
      hour: golive_date.strftime("%-I"),
      min: golive_date.strftime("%-M"),
      meridian: golive_date.strftime("%p"),
      day: golive_date.strftime("%-d"),
      month: golive_date.strftime("%-m"),
      year: golive_date.strftime("%-Y")
    }
  end

  def distribution_api_service
    @distribution_api_service ||= DistributionApiService.find_by(uuid: source_id)
  end

  def orig_release_year
    return if original_release_date_et.blank?

    Time.parse(original_release_date_et).year
  end

  def album_release_date
    @album_release_date ||=
      if timed_release_type.present?
        timed_release_date.presence || timed_release_date_utc
      else
        release_date.presence || release_date_utc
      end
  end

  def primary_genre_id
    return if primary_genres.blank?

    primary_genres[0].id
  end

  def secondary_genre_id
    return if secondary_genres.blank?

    secondary_genres[0].id
  end

  # Some values are hard-coded for compatibility with the SongForm
  def tunecore_song_attributes(album_id)
    attributes =
      songs.map do |song|
        {
          name: song.title,
          album_id: album_id,
          language_code_id: track_lyric_language_code_id(song),
          language_name: song.track_lyric_language,
          version: nil,
          cover_song: false,
          made_popular_by: nil,
          explicit: song.explicit_lyrics,
          clean_version: song.clean_version,
          optional_isrc: song.isrc&.first&.id || "",
          lyrics: song.lyrics,
          asset_url: song.asset_url,
          previously_released_at: song.original_release_date,
          instrumental: song.instrumental,
          track_number: song.sequence,
          artists: song_creatives_attributes(song),
          copyrights: nil,
          song_copyright_claims: nil,
          song_start_times: song_start_times(song),
          source_song_id: song.source_song_id
        }
      end

    attributes.sort_by { |hash| Integer(hash[:track_number]) }
  end

  def song_creatives_attributes(song)
    song.artists.map do |artist|
      {}.tap do |hash|
        hash[:artist_name] = artist.name
        hash[:credit] = artist.creative_role
        hash[:associated_to] = "Song"
        hash[:name] = artist.name
        hash[:role_ids] = SongRole.where(role_type: artist.roles).pluck(:id).uniq
      end
    end
  end

  def song_start_times(song)
    return [] if song.sample_start_time.blank? || song.sample_start_time == "00:00"

    [
      {
        store_id: TIKTOK_STORE_ID,
        start_time: Time.parse("00:#{song.sample_start_time}").seconds_since_midnight
      }
    ]
  end

  def track_lyric_language_code_id(song)
    return if song.track_lyric_language.blank?

    LanguageCode.find_by(code: song.track_lyric_language)&.id
  end

  def release_type
    return "Single" if songs.count == 1

    "Album"
  end

  def deliver_to_apple_music?
    album_stores.find { |as| as.store_id == Store::ITUNES_WW_ID }.present?
  end

  delegate :source_id, :source_user_id, :source_album_id, :source_contentreview_notes, :message_id,
           :delivery_type, :title, :metadata_language, :primary_genres, :secondary_genres,
           :release_date_utc, :release_date, :artists, :original_release_date_et, :various_artists,
           :territories, :album_stores, :artwork_file, :songs, :upcs, :label_name, :timed_release_type,
           :timed_release_date_utc, :timed_release_date, to: :parsed_json
end
