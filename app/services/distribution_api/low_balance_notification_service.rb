class DistributionApi::LowBalanceNotificationService
  AMOUNT_FALLBACK = 1_000

  PERSON_ID_VARS_MAP = {
    3_820_050 => { amount: ENV["BD_BALANCE_ALERT_AMOUNT"], to: "tiktoklowbalance@tunecore.com" } # ByteDance
  }

  def self.run
    new.run
  end

  def run
    Person
      .includes(:person_balance)
      .where(id: PERSON_ID_VARS_MAP.keys)
      .each do |person|
      key    = PERSON_ID_VARS_MAP[person.id]
      amount = key[:amount].present? ? Integer(key[:amount], 10) : AMOUNT_FALLBACK
      to     = key[:to]

      handle_balance(amount: amount, person: person, to: to)
    end
  end

  # Method split for testing purposes
  def handle_balance(amount:, person:, to:)
    return unless person.balance < amount

    LowBalanceMailer.low_balance(amount: amount, person: person, to: to).deliver
  end
end
