class DistributionApi::ExternalServiceIdApi::SendService
  attr_accessor :album_id, :esid_creation_date, :json

  def self.call(params)
    new(params).call
  end

  def initialize(params)
    @album_id = params[:album_id]
    @esid_creation_date = params[:esid_creation_date]
    @json = params[:json]
  end

  def call
    response = DistributionApi::SendResponseService.call(url: "meta_info", response_json: json)
    save_response(response)
  end

  def save_response(response)
    distribution_api_external_service_id_api_status.update(response_status: response)
  end

  def distribution_api_external_service_id_api_status
    DistributionApiExternalServiceIdApiStatus.where(
      album_id: album_id,
      creation_date: esid_creation_date
    )
  end
end
