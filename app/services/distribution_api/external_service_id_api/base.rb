class DistributionApi::ExternalServiceIdApi::Base
  attr_accessor :esid_creation_date, :force_send_esid, :distribution_api_service_name

  def self.run(esid_creation_date:, force_send_esid: false, distribution_api_service_name:)
    new(
      esid_creation_date: esid_creation_date,
      force_send_esid: force_send_esid,
      distribution_api_service_name: distribution_api_service_name
    ).run
  end

  def initialize(esid_creation_date:, force_send_esid:, distribution_api_service_name:)
    @esid_creation_date = esid_creation_date
    @force_send_esid = force_send_esid
    @distribution_api_service_name = distribution_api_service_name
  end

  def run
    # In force mode, delete all Distribution Api External Service Id API Statuses for passed date and start fresh
    delete_distribution_api_esid_statuses if force_send_esid

    return retry_failed_esids if distribution_api_esid_statuses.present?

    save_distribution_api_esids

    send_distribution_api_esids
  ensure
    delete_all_distribution_api_esid_statuses_after_90_days
  end

  def delete_distribution_api_esid_statuses
    distribution_api_esid_statuses.delete_all
  end

  def retry_failed_esids
    return if unsuccessful_distribution_api_esid_statuses.blank?

    send_distribution_api_esids
  end

  def save_distribution_api_esids
    album_ids_to_retry.each do |album_id|
      create_distribution_api_esid_status(album_id)
    end
  end

  def send_distribution_api_esids
    album_ids_to_retry.map do |album_id|
      DistributionApi::ExternalServiceIdApiSendWorker.perform_async(
        album_id: album_id,
        esid_creation_date: esid_creation_date,
        json: esids_json_for(album_id: album_id)
      )
    end
  end

  def delete_all_distribution_api_esid_statuses_after_90_days
    DistributionApiExternalServiceIdApiStatus.where(
      "creation_date < ?", Time.current - 90.days
    ).delete_all
  end

  def esids_json_for(album_id:)
    esids = esids_hash[album_id]

    {
      count_total: count_total,
      count_itemized: count_itemized(album_id),
      album: album_esids_json_for(album_id: album_id, esids: esids)
    }
  end

  def album_esids_json_for(album_id:, esids:)
    album_esids = esids.select { |e| e.linkable_type == "Album" }

    {
      tc_album_id: album_id,
      source_album_id: distribution_api_album_id_for(album_id),
      external_service_identifiers: {
        apple: identifier_for(album_esids, ExternalServiceId::APPLE_SERVICE),
        itunes: identifier_for(album_esids, ExternalServiceId::ITUNES_SERVICE),
        spotify: identifier_for(album_esids, ExternalServiceId::SPOTIFY_SERVICE)
      },
      artists: artist_esids_json_for(
        esids: esids.select { |e| e.linkable_type == "Creative" }.group_by(&:linkable_id)
      )
    }
  end

  def artist_esids_json_for(esids:)
    esids.map do |creative_id, creative_esids|
      creative = Creative.includes(:artist).find(creative_id)
      {
        name: creative.artist.name,
        tc_creative_id: creative.id,
        apple_artist_id: identifier_for(creative_esids, ExternalServiceId::APPLE_SERVICE),
        spotify_artist_id: identifier_for(creative_esids, ExternalServiceId::SPOTIFY_SERVICE)
      }
    end
  end

  def identifier_for(esids, type)
    esids.find { |e| e.service_name == type }&.identifier.presence || ""
  end

  def esids_hash
    @esids_hash ||=
      album_esids_hash.merge!(artist_esids_hash) { |_, album_esids, artist_esids|
        album_esids + artist_esids
      }
  end

  def album_esids_hash
    ExternalServiceId.album_ids_by_person_id_and_date(
      person_id,
      esid_creation_date,
    ).group_by(&:linkable_id)
  end

  def artist_esids_hash
    ExternalServiceId.artist_ids_for_albums_by_person_id_and_date(
      person_id,
      esid_creation_date,
    ).group_by { |esid| esid.linkable.creativeable_id }
  end

  def distribution_api_album_id_for(album_id)
    DistributionApiAlbum.find_by(album_id: album_id)&.source_album_id.presence || ""
  end

  def count_total
    @count_total ||= esids_hash.keys.count
  end

  def count_itemized(album_id)
    esids_hash.keys.sort.index(album_id) + 1
  end

  def create_distribution_api_esid_status(album_id)
    DistributionApiExternalServiceIdApiStatus.where(
      album_id: album_id,
      creation_date: esid_creation_date,
      distribution_api_service: distribution_api_service
    ).first_or_create!
  end

  def distribution_api_esid_statuses
    DistributionApiExternalServiceIdApiStatus.where(
      creation_date: esid_creation_date,
      distribution_api_service: distribution_api_service
    )
  end

  def distribution_api_service
    @distribution_api_service ||= DistributionApiService.find_by(name: distribution_api_service_name)
  end

  def unsuccessful_distribution_api_esid_statuses
    @unsuccessful_distribution_api_esid_statuses ||= distribution_api_esid_statuses.where(
      response_status: nil
    )
  end

  def album_ids_to_retry
    # If failed deliveries exist only retry those, else retry all albums
    if unsuccessful_distribution_api_esid_statuses.present?
      return unsuccessful_distribution_api_esid_statuses.pluck(:album_id).sort
    end

    esids_hash.keys.sort
  end

  def person_id
    @person_id ||= ENV["BYTEDANCE_USER_ID"]
  end
end
