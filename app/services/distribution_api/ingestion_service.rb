# frozen_string_literal: true

class DistributionApi::IngestionService
  attr_reader :key, :bucket
  attr_accessor :local_json_path

  SUCCESS = "success"
  ERROR = "error"
  INGESTED = "ingested"
  IN_PROCESS = "in_process"

  def self.call(key, bucket)
    new(key, bucket).send(:call)
  end

  def initialize(key, bucket)
    @key = key
    @bucket = bucket
  end

  private

  def call
    begin
      Rails.logger.info "Started Distribution Api Ingestion for #{s3_path}"
      return unless download_json

      ingest if preprocess_and_add_to_ingestion_logs
    rescue DistributionApi::Errors::IngestionError => e
      log_and_send_response(ERROR, e.error_response)
    rescue => e
      log_and_send_response(ERROR, generic_error_message(e.message))
    ensure
      s3_downloader.cleanup
    end
  end

  def download_json
    @local_json_path = s3_downloader.download
    unless local_json_path
      Airbrake.notify("No Valid JSON found for s3_path: #{s3_path}")
      return
    end

    true
  end

  def preprocess_and_add_to_ingestion_logs
    if json_ingested?
      Airbrake.notify(json_ingested_error_message)
      return
    end

    add_to_distribution_api_ingestion_logs

    raise_album_errors(validation_service.error_messages) unless valid_json_data?
    raise_album_errors(
      [
        "Album already Ingested for s3_path: #{s3_path}. Please contact Customer Care."
      ]
    ) if album_finalized?

    true
  end

  def add_to_distribution_api_ingestion_logs
    @distribution_api_ingestion_log = DistributionApiIngestionLog.where(
      json_path: s3_path,
      distribution_api_service: distribution_api_service,
      source_album_id: source_album_id,
      message_id: message_id
    ).first_or_create!

    @distribution_api_ingestion_log.assign_attributes(
      source_contentreview_notes: source_contentreview_notes,
      delivery_type: delivery_type,
      status: IN_PROCESS
    )

    @distribution_api_ingestion_log.save!
  end

  def ingest
    success =
      if rejected?
        DistributionApi::Update::Base.new(
          json_parser: json_parser,
          tunecore_album: tunecore_album
        ).run
      else
        delete_resources if deletable?

        DistributionApi::Ingest::Base.new(json_parser).run
      end

    return unless success

    log_and_send_response(INGESTED, success_message)
  end

  def s3_downloader
    @s3_downloader ||= DistributionApi::S3Downloader.new(key: key, bucket: bucket)
  end

  def json_ingested_error_message
    "JSON #{s3_path} already ingested for "\
    "source_album_id: #{source_album_id} and "\
    "distribution_api_service: #{source_id}"
  end

  def json_ingested?
    DistributionApiIngestionLog.where(
      json_path: s3_path,
      status: INGESTED
    ).present?
  end

  def log_and_send_response(status, message)
    # Do not send any response if distribution api service is not valid
    return if !local_json_path || distribution_api_service.blank? || @distribution_api_ingestion_log.blank?

    response_json = response_message.merge(message)

    @distribution_api_ingestion_log.update!(
      status: status,
      response_json: response_json.to_json
    )

    DistributionApi::SendResponseService.call(url: "ingestion", response_json: response_json)

    raise "Distribution Api Ingestion failed for #{s3_path}" if status != INGESTED
  end

  def album_finalized?
    tunecore_album&.finalized?
  end

  def last_audit
    return if tunecore_album.blank?

    @last_audit ||= tunecore_album.review_audits.last
  end

  def rejected?
    return false if tunecore_album.blank?

    last_audit&.event == "REJECTED"
  end

  def never_reviewed?
    tunecore_album.blank? || last_audit.nil?
  end

  def tunecore_album
    DistributionApiAlbum.find_by(source_album_id: source_album_id)&.album
  end

  def deletable?
    tunecore_album.present? && never_reviewed?
  end

  def delete_resources
    DistributionApi::DeletionService.call(tunecore_album.id)
  end

  def response_message
    {
      message_id: message_id,
      tc_album_id: tunecore_album&.id,
      source_album_id: source_album_id,
      time: DateTime.now.to_i,
      errors: [],
      songs: []
    }
  end

  def success_message
    {
      state: INGESTED
    }
  end

  def generic_error_message(message)
    {
      state: ERROR,
      errors: [
        {
          error_state: "technical_error",
          error_code: nil,
          message: message
        }
      ]
    }
  end

  def json_parser
    @json_parser ||= DistributionApi::JsonParser.new(File.read(local_json_path))
  end

  def valid_json_data?
    validation_service.valid?
  end

  def validation_service
    @validation_service ||= DistributionApi::ValidationService.new(json_parser)
  end

  def s3_path
    "S3://#{bucket}/#{key}"
  end

  def raise_album_errors(error_messages)
    raise DistributionApi::Errors::IngestionError.new(error_messages: error_messages)
  end

  delegate :source_id, :source_album_id, :message_id, :source_contentreview_notes, :delivery_type,
           :distribution_api_service, to: :json_parser
end
