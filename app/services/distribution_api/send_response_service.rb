class DistributionApi::SendResponseService
  attr_reader :response_json, :url

  EXPIRE_TIME = 600
  VERSION = "auth-v1"

  def self.call(url:, response_json:)
    new(url: url, response_json: response_json).send(:call)
  end

  def initialize(url:, response_json:)
    @response_json = JSON.generate(response_json)
    @url = Rails.env.production? ? url : ""
  end

  def call
    http_client =
      Faraday.new(url: base_url) do |conn|
        conn.headers["Content-Type"] = "application/json"
        conn.headers["Agw-Auth"] = auth_header
        conn.adapter Faraday.default_adapter
      end

    response = http_client.post(url, response_json)

    response.body.to_json
  end

  def auth_header
    bd_access_key = ENV.fetch("BD_ACCESS_KEY")
    bd_secret_key = ENV.fetch("BD_SECRET_KEY")
    sign_key_info = "#{VERSION}/#{bd_access_key}/#{timestamp}/#{EXPIRE_TIME}"
    sign_key = sha256_hmac(bd_secret_key, sign_key_info)
    signature = sha256_hmac(sign_key, response_json)
    "#{sign_key_info}/#{signature}"
  end

  def timestamp
    @timestamp ||= DateTime.now.to_i
  end

  def sha256_hmac(key, data)
    digest = OpenSSL::Digest.new("SHA256")
    OpenSSL::HMAC.hexdigest(digest, key, data)
  end

  def base_url
    ENV.fetch("BYTEDANCE_BASE_URL", "https://www.soundon.global/open-api/v1/")
  end
end
