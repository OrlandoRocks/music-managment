# frozen_string_literal: true

module DistributionApi::Helpers::SongsHelper
  include ApplicationHelper
  include SsoCookies

  def ingest_distribution_api_song(song, source_song_id)
    DistributionApiSong.create(
      {
        distribution_api_album: tunecore_album.distribution_api_album,
        song: song,
        source_song_id: source_song_id
      }
    )
  end

  def creation_params(song_params)
    song_params = song_params.merge(
      {
        id: tunecore_album.song.id
      }
    ) if json_parser.release_type == "Single"

    song_params[:artists] = song_artists(song_params)

    {
      song_params: song_params.except(:asset_url, :source_song_id),
      person_id: person.id,
      should_build_copyrights: false,
      should_build_song_copyright_claims: false,
      should_build_start_times: song_start_time_form_enabled?
    }
  end

  def song_artists(song_params)
    song_params[:artists].map do |song_artist|
      album_creative = tunecore_album.creatives.find { |creative| song_artist[:name] == creative.artist.name }
      song_artist = song_artist.merge(
        {
          creative_id: album_creative.id,
          associated_to: "Album"
        }
      ) if album_creative.present?

      song_artist
    end
  end

  def upload_asset(song, asset_url, source_song_id)
    transcoder_response = AssetsUploader::Transcoder.new(
      asset_url: asset_url,
      song: song
    ).upload

    upload_register_form = UploadsRegisterForm.new(
      uploads_registration_form_params(transcoder_response).merge(
        person: person,
        song_id: song.id
      )
    )

    raise DistributionApi::Errors::IngestionError.new(
      error_messages: ["Unable to register upload - #{upload_register_form.errors.full_messages}"],
      error_state: "technical_error",
      source_song_id: source_song_id
    ) unless upload_register_form.save
  end

  def uploads_registration_form_params(transcoder_response)
    {
      key: transcoder_response[:key],
      uuid: transcoder_response[:uuid],
      bucket: transcoder_response[:bucket],
      orig_filename: transcoder_response[:orig_filename],
      bit_rate: transcoder_response[:bit_rate]
    }
  end

  def songs_params
    @songs_params ||= json_parser.tunecore_song_attributes(tunecore_album.id)
  end

  def song_start_time_form_enabled?
    FeatureFlipper.show_feature?(:tiktok_clip, person)
  end
end
