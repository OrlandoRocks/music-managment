# frozen_string_literal: true

module DistributionApi::Helpers::AlbumHelper
  include AlbumHelper
  include TimedReleasable

  def creation_params
    params_copy = album_params.merge(created_with: Album.created_withs[:songwriter])

    params_copy = params_copy.except(
      :optional_isrc, :parental_advisory, :clean_version,
      :specialized_release_type
    ) if json_parser.release_type == "Album"

    {
      album: tunecore_album,
      album_id: tunecore_album&.id,
      album_type: json_parser.release_type,
      person: person,
      skip_special_salepoints: true
    }.merge(album_params: params_copy)
  end

  def album_params
    result = initial_album_params
             .merge(metadata_language_code_id: metadata_language_code_id(initial_album_params))

    return result.except(:golive_date) if result[:specialized_release_type].present?

    handle_golive_date(result)
  end

  def initial_album_params
    @initial_album_params ||= json_parser.tunecore_album_attributes
  end

  def creatives_params
    @creatives_params ||= JSON.parse(initial_album_params[:creatives].to_json, class_name: OpenStruct)
  end

  def handle_golive_date(result)
    return result if result[:golive_date].blank?

    new_golive_date = convert_date_hash_to_golive_date(initial_album_params[:golive_date])
    new_sale_date = convert_date_hash_to_sale_date(initial_album_params[:golive_date])

    result.merge(
      golive_date: new_golive_date,
      sale_date: new_sale_date
    )
  end

  def scrub_artist_names
    creatives_params&.each do |creative|
      creative[:name] = Loofah.fragment(creative[:name]).text(encode_special_chars: false)
    end
  end
end
