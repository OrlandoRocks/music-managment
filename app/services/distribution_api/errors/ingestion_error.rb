class DistributionApi::Errors::IngestionError < StandardError
  attr_reader :error_messages, :error_state, :error_type, :source_song_id

  ERROR = "error"

  def initialize(error_messages:, error_state: "validation_error", source_song_id: nil)
    super
    @error_messages = error_messages
    @error_state = error_state
    @error_type = error_type
    @source_song_id = source_song_id
  end

  def error_response
    return album_error_response unless source_song_id

    song_error_response
  end

  def album_error_response
    message = {
      state: ERROR,
      errors: [],
      songs: []
    }

    error_messages.each do |error_message|
      message[:errors].push(
        {
          error_state: error_state,
          error_code: nil,
          message: error_message
        }
      )
    end

    message
  end

  def song_error_response
    message = {
      state: ERROR,
      errors: [],
      songs: [
        {
          source_song_id: source_song_id,
          state: ERROR,
          errors: []
        }
      ]
    }

    error_messages.each do |error_message|
      message[:songs][0][:errors].push(
        {
          error_state: error_state,
          error_code: nil,
          message: error_message
        }
      )
    end

    message
  end
end
