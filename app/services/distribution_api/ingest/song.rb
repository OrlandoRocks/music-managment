# frozen_string_literal: true

class DistributionApi::Ingest::Song
  include DistributionApi::Helpers::SongsHelper

  attr_reader :json_parser, :person, :tunecore_album

  def self.ingest(json_parser, person, tunecore_album)
    new(json_parser, person, tunecore_album).ingest
  end

  def initialize(json_parser, person, tunecore_album)
    @json_parser = json_parser
    @person = person
    @tunecore_album = tunecore_album
  end

  def ingest
    songs_params.each do |song_params|
      sdf = SongDataForm.new(creation_params(song_params))
      raise DistributionApi::Errors::IngestionError.new(
        error_messages: ["Unable to save Song - #{sdf.errors.full_messages}"],
        error_state: "technical_error",
        source_song_id: song_params[:source_song_id]
      ) unless sdf.save

      upload_asset(sdf.song, song_params[:asset_url], song_params[:source_song_id])

      ingest_distribution_api_song(sdf.song, song_params[:source_song_id])
    end
  end

  def ingest_distribution_api_song(song, source_song_id)
    DistributionApiSong.create(
      {
        distribution_api_album: tunecore_album.distribution_api_album,
        song: song,
        source_song_id: source_song_id
      }
    )
  end
end
