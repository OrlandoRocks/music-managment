# frozen_string_literal: true

class DistributionApi::Ingest::Album
  include DistributionApi::Helpers::AlbumHelper

  attr_reader :json_parser, :person

  def self.ingest(json_parser, person)
    new(json_parser, person).ingest
  end

  def initialize(json_parser, person)
    @json_parser = json_parser
    @person = person
  end

  def ingest
    scrub_artist_names

    @album_form = AlbumForm.new(creation_params)

    unless @album_form.save
      raise DistributionApi::Errors::IngestionError.new(
        error_messages: ["Unable to save Album #{@album_form.errors.full_messages}"],
        error_state: "technical_error"
      )
    end

    ingest_distribution_api_album

    add_optional_upc

    set_release_countries

    create_esids(tunecore_album, creatives_params)

    tunecore_album
  end

  def create_params
    params = creation_params
    # Not saving optional UPC during album creation to avoid duplicate UPC error
    # if the sidekiq message gets killed before creation of distribution api album
    params[:album_params] = params[:album_params].except(:optional_upc_number)

    params
  end

  def add_optional_upc
    return if optional_upc_number.blank?

    tunecore_album.optional_upc_number = optional_upc_number
    tunecore_album.make_optional_upc
  end

  def optional_upc_number
    creation_params[:album_params][:optional_upc_number]
  end

  def tunecore_album
    @tunecore_album ||=
      if json_parser.release_type == "Single"
        person.singles.build
      else
        person.albums.build
      end
  end

  def ingest_distribution_api_album
    return if tunecore_album&.id.blank?

    DistributionApiAlbum.create(
      json_parser.distribution_api_album_attributes.merge(
        album_id: tunecore_album.id
      )
    )
  end

  def set_release_countries
    return if album_territories.blank?

    tunecore_album.countries = Country.where(iso_code: album_territories.split(","))
  end

  def album_territories
    return tunecore_album.country_iso_codes.join(",") if json_parser.territories.blank?

    json_parser.territories.map(&:upcase).join(",")
  end
end
