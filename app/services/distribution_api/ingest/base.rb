# frozen_string_literal: true

class DistributionApi::Ingest::Base
  attr_reader :json_parser

  def initialize(json_parser)
    @json_parser = json_parser
  end

  def run
    tunecore_album = ingest_album
    ingest_artwork(tunecore_album)
    ingest_songs(tunecore_album)
    ingest_salepoints(tunecore_album)
    create_purchase(tunecore_album)
    add_and_finalize_automator(tunecore_album)

    true
  end

  def ingest_album
    DistributionApi::Ingest::Album.ingest(json_parser, person)
  end

  def ingest_artwork(tunecore_album)
    DistributionApi::Ingest::Artwork.ingest(json_parser, tunecore_album)
  end

  def ingest_songs(tunecore_album)
    DistributionApi::Ingest::Song.ingest(json_parser, person, tunecore_album)
  end

  def ingest_salepoints(tunecore_album)
    DistributionApi::Ingest::Salepoints.ingest(json_parser, tunecore_album)
  end

  def create_purchase(tunecore_album)
    DistributionApi::Ingest::Purchase.create(person, tunecore_album)
  end

  def add_and_finalize_automator(tunecore_album)
    Album::AutomatorService.add_and_finalize_automator(tunecore_album)
  end

  def person
    @person ||= Person.find(ENV["BYTEDANCE_USER_ID"])
  end
end
