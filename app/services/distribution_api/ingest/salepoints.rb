class DistributionApi::Ingest::Salepoints
  def self.ingest(json_parser, album)
    new(json_parser, album).ingest
  end

  attr_reader :album, :json_parser

  def initialize(json_parser, album)
    @json_parser = json_parser
    @album = album
  end

  def ingest
    salepoints, stores_not_added = album.add_stores(json_parser.stores)

    if stores_not_added.length.positive?
      raise DistributionApi::Errors::IngestionError.new(
        error_messages: ["Unable to add stores: #{stores_not_added}"],
        error_state: "technical_error"
      )
    end
    raise DistributionApi::Errors::IngestionError.new(
      error_messages: ["Unable to add salepoints"],
      error_state: "technical_error"
    ) if salepoints.empty?
  end
end
