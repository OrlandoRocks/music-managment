# frozen_string_literal: true

class DistributionApi::Ingest::Purchase
  def self.create(person, album)
    new(person, album).create
  end

  attr_reader :album, :person

  def initialize(person, album)
    @person = person
    @album = album
  end

  def create
    targeted_product = TargetedProduct.for_targeted_person_by_item(album, person.id).first
    purchase = Product.add_to_cart(person, album, product)

    apply_discount(purchase, targeted_product) if targeted_product.present?

    form = CartFinalizeForm.new(cart_finalize_form_params(purchase))

    raise DistributionApi::Errors::IngestionError.new(
      error_messages: ["Unable to purchase"],
      error_state: "technical_error"
    ) unless form.save

    SuccessfulPurchaseWorker.perform_async(purchase.invoice.id)
  end

  private

  def apply_discount(purchase, targeted_product)
    discount = targeted_product.discount_amount_cents(purchase.cost_cents) || 0
    purchase.update(
      discount_cents: discount,
      discount_reason: Purchase::VALID_DISCOUNT_REASONS[:non_plan_discount],
      targeted_product: targeted_product
    )
  end

  def cart_finalize_form_params(purchase)
    {
      person: person,
      use_balance: true,
      ip_address: "tunecore_dev_center",
      purchases: [purchase]
    }
  end

  def product
    product_type = "one_year_#{album.album_type}".downcase.to_sym
    product_id = Product.find_products_for_country(person.country_domain, product_type)
    Product.find(product_id)
  end
end
