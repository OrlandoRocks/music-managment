class DistributionApi::Ingest::Artwork
  attr_reader :json_parser, :tunecore_album

  def self.ingest(json_parser, tunecore_album)
    new(json_parser, tunecore_album).ingest
  end

  def initialize(json_parser, tunecore_album)
    @json_parser = json_parser
    @tunecore_album = tunecore_album
  end

  def ingest
    begin
      artwork = tunecore_album.artwork || ::Artwork.new(album_id: tunecore_album.id)
      artwork.assign_artwork(File.new(artwork_file_local_path))

      unless artwork.save && artwork.uploaded?
        raise DistributionApi::Errors::IngestionError.new(
          error_messages: ["Unable to upload artwork: #{artwork.last_errors}"],
          error_state: "technical_error"
        )
      end
    ensure
      s3_downloader.cleanup
    end
  end

  def s3_downloader
    @s3_downloader ||= DistributionApi::S3Downloader.new(asset_url: artwork_remote_path)
  end

  def artwork_file_local_path
    s3_downloader.download
  end

  def artwork_remote_path
    json_parser.artwork_file.asset
  end
end
