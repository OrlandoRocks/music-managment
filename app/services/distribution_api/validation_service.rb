class DistributionApi::ValidationService
  attr_reader :json_parser

  attr_accessor :error_messages

  def initialize(json_parser)
    @json_parser = json_parser
    @error_messages = []
  end

  def valid?
    source_id_valid?
    upc_valid?
    stores_valid?

    @error_messages.length.zero?
  end

  def source_id_valid?
    distribution_api_service = DistributionApiService.find_by(uuid: json_parser.source_id)

    set_error_message("Invalid Source Id") if distribution_api_service.blank?
  end

  def stores_valid?
    valid_ids = Store.is_active.is_used.map(&:id)

    set_error_message("No store found") if json_parser.stores.blank?

    set_error_message("Invalid store Found") unless (json_parser.stores.pluck(:id) - valid_ids).empty?
  end

  def upc_valid?
    return if json_parser.upcs.blank?

    json_parser.upcs.each do |upc|
      upc = Upc.find_by(number: upc.upc)
      next if upc.blank? || same_source_album?(upc: upc)

      set_upc_validation_error_message(upc: upc)
    end
  end

  def set_upc_validation_error_message(upc:)
    error_message = "UPC #{upc.number} already exists"

    unless upc.upcable.person_id == Integer(ENV["BYTEDANCE_USER_ID"], 10)
      error_message = "#{error_message} and belongs to a different tunecore user"
    end

    set_error_message(error_message)
  end

  def same_source_album?(upc:)
    json_parser.source_album_id == upc&.upcable&.distribution_api_album&.source_album_id &&
      json_parser.source_id == upc&.upcable&.distribution_api_album&.distribution_api_service&.uuid
  end

  def set_error_message(error_message)
    @error_messages << error_message
  end
end
