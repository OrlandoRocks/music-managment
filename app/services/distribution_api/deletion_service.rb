class DistributionApi::DeletionService
  attr_reader :album_id

  def self.call(album_id)
    new(album_id).send(:call)
  end

  def initialize(album_id)
    @album_id = album_id
  end

  def call
    return unless album.present? && album.persisted?

    destroy_related_resources

    album.destroy!
  end

  # rubocop:disable Metrics/PerceivedComplexity
  def destroy_related_resources
    album.creatives.map(&:destroy!)
    album.distribution_api_songs.map(&:destroy!)
    album.distribution_api_album&.destroy!
    album.songs.map(&:destroy!)
    album.salepoints.map(&:destroy!)
    album.purchases.map(&:destroy!)
    distribution_api_external_service_id_api_status.map(&:destroy!)
    artwork.destroy! if artwork.present? && artwork.persisted?
  end
  # rubocop:enable Metrics/PerceivedComplexity

  def album
    @album ||= Album.find(album_id)
  end

  def artwork
    @artwork ||= Artwork.find_by(album_id: album_id)
  end

  def distribution_api_external_service_id_api_status
    DistributionApiExternalServiceIdApiStatus.where(album_id: album_id)
  end
end
