module Soundout
  class Transcoder
    attr_accessor :soundout_report

    OUTPUT_FORMAT = "mp3".freeze

    def initialize(options)
      options.each do |k, v|
        if respond_to?(k)
          public_send "#{k}=", v
        else
          puts "Missing accessor for #{k}"
        end
      end
    end

    delegate :id, to: :soundout_report

    def song
      soundout_report.track
    end

    def asset
      song.s3_asset
    end

    def run
      response = transcode_request
      Rails.logger.info response
      nil
    end

    def transcode_request
      HTTParty.post(ENV["TC_TRANSCODER_URL"], body: post_body, headers: post_headers)
    end

    def post_body
      body = {
        callbackURL: callback_url,
        source: {
          key: asset.key,
          bucket: asset.bucket
        },
        destination: {
          key: song.s3_streaming_key,
          bucket: song.s3_streaming_bucket
        },
        format: { extension: OUTPUT_FORMAT },
      }

      body.to_json
    end

    def post_headers
      {
        "Content-Type" => "application/json"
      }
    end

    def callback_url
      "https://#{CALLBACK_API_URLS['US']}/api/transcoder/soundout_reports/#{id}/complete_order".freeze
    end
  end
end
