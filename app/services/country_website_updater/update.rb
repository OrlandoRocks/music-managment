module CountryWebsiteUpdater
  class Update
    def self.update(person:)
      new(person: person).send(:perform)
    end

    private

    attr_accessor :person

    def initialize(person:)
      @person = person
    end

    def perform
      ActiveRecord::Base.transaction do
        # Note: this change intentionally left in memory. Person#save needs to
        #  be invoked in order for the change be persisted.
        person.accepted_terms_and_conditions_on = nil

        person.person_balance.update!(
          currency: person.country_website.currency
        )

        destroy_unfinalized_purchases
      end
    end

    def destroy_unfinalized_purchases
      person.purchases.destroy_all
    end

    # def notify_user
    #   PersonNotifier.lead_migrated(person).deliver
    # end
  end
end
