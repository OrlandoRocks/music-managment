# frozen_string_literal: true

class PayinProviderConfigBackfillService
  attr_accessor :payment_payin_object

  def initialize(payment_payin_object)
    @payment_payin_object = payment_payin_object
  end

  def call!
    paypal = proc { |x| [StoredPaypalAccount.to_s, PaypalTransaction.to_s].include?(x) }
    braintree = proc { |x| [StoredCreditCard.to_s, BraintreeTransaction.to_s].include?(x) }

    payment_payin_object_type = payment_payin_object.class.to_s

    payin_provider_config =
      case payment_payin_object_type
      when paypal
        person = payment_payin_object.person
        if person.blank?
          country_website_paypal_payin_provider_config(payment_payin_object.country_website)
        else
          person.paypal_payin_provider_config
        end
      when braintree
        payment_payin_object.person.braintree_config_by_corporate_entity
      end
    ApplicationRecord.transaction do
      # we don't want to run validations here
      payment_payin_object.payin_provider_config_id = payin_provider_config.id
      payment_payin_object.save(validate: false)
    end
  end

  def country_website_paypal_payin_provider_config(country_website)
    PayinProviderConfig.find_by!(
      name: PayinProviderConfig::PAYPAL_NAME,
      corporate_entity: CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME),
      currency: country_website.currency
    )
  end
end
