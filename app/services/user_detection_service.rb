class UserDetectionService
  def initialize(request)
    @request = request
  end

  def ip
    @request.remote_ip
  end

  def browser_name
    Browser.new(@request.user_agent, accept_language: "en-us").name
  end

  def location
    results = ::Geocoder.search(@request.remote_ip)
    return unless results.any?
    return if results.first.city.nil? && results.first.state.nil? && results.first.country.nil?

    result = results.first

    @location ||= [[result.city, result.state].join(", "), result.country].join(" ")
  end
end
