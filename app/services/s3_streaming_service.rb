class S3StreamingService
  attr_reader :file_name, :headers, :data, :s3_file

  class NoFileNameProvidedError < StandardError; end

  class NoDataProvidedError < StandardError; end

  class NoBucketFoundError < StandardError; end

  class NoBucketPathError < StandardError; end

  def self.write_and_upload(options)
    new(options).write_and_upload
  end

  def initialize(options)
    @file_name  = options.fetch(:file_name) { raise NoFileNameProvidedError }
    @headers    = options.fetch(:headers) { :no_headers_provided }
    @data       = format_data(options.fetch(:data) { raise NoDataProvidedError })

    bucket      = options.fetch(:bucket_name) { raise NoBucketFoundError }
    bucket_path = options.fetch(:bucket_path) { raise NoBucketPathError }

    key         = set_s3_key(bucket_path)
    credentials = Aws::Credentials.new(AWS_ACCESS_KEY, AWS_SECRET_KEY)
    client      = Aws::S3::Client.new(
      region: ENV.fetch("AWS_DEFAULT_REGION", "us-east-1"),
      credentials: credentials
    )

    @s3_file = Aws::S3::Object.new(bucket, key, client: client)
  end

  def headers_provided?
    headers != :no_headers_provided
  end

  def write_and_upload
    s3_file.upload_stream do |write_stream|
      write_stream << CSV.generate_line(headers) if headers_provided?
      data.each do |line|
        write_stream << CSV.generate_line(line)
      end
    end
    Rails.logger.info "Uploaded file to S3 - #{file_name}"
  end

  def set_s3_key(bucket_path)
    "#{bucket_path}/#{file_name}"
  end

  def format_data(data)
    return data if headers_provided?

    [data]
  end
end
