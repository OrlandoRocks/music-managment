class PhoneNumberVerifier
  def self.register_authy_user(params)
    Authy::API.register_user(
      email: params[:email],
      country_code: params[:phone_code],
      cellphone: params[:phone_number]
    )
  end

  def self.request_sms(authy_id)
    Authy::API.request_sms(id: authy_id, force: true)
  end

  def self.verify_auth_code(authy_id, auth_code)
    Authy::API.verify(id: authy_id, token: auth_code)
  end
end
