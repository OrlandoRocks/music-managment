class TranslationFetcherService
  include KnowledgebaseLinkHelper

  def self.translations_for(page, locale)
    user_locale_path = ["config", "locales", page.split("/"), (locale || "en") + ".yml"].flatten.join("/")
    english_path = ["config", "locales", page.split("/"), "en.yml"].flatten.join("/")
    new(locale, english_path, user_locale_path)
  end

  attr_reader :translations, :locale

  COUNTRY_WEBSITES = COUNTRY_LOCALE_MAP.invert

  def initialize(locale, english_path, user_locale_path)
    @locale = locale
    user_locale_fullpath = Rails.root.join(user_locale_path).to_s

    user_translation_file =
      if File.exist?(user_locale_fullpath)
        Rails.root.join(user_locale_path).read
      else
        Rails.root.join(english_path).read
      end

    @translations = YAML.load(Rails.root.join(english_path).read)["en"].with_indifferent_access.deep_merge(
      YAML.load(user_translation_file)[locale].try(:with_indifferent_access) || {}
    )
  end

  def with_support_links(links = {})
    links.each do |key, value|
      translations[key] = knowledgebase_link_for(
        value,
        COUNTRY_WEBSITES[locale]
      )
    end

    self
  end

  def method_missing(method_name, *args, &block)
    if translations.respond_to?(method_name)
      translations.send(method_name, *args, &block)
    else
      super
    end
  end

  def respond_to_missing?(method_name, include_private = false)
    translations.respond_to?(method_name) || super
  end
end
