# frozen_string_literal: true

class Admin::PaymentService
  def initialize(filters)
    @type = filters[:type]
    @page = filters[:page]
    @per_page = filters[:per_page]
    @keyword = filters[:keyword]
    @order = filters[:order]
    @threshold_status = filters[:threshold_status]
    @status = filters[:status]
    @auto_approval_eligible = filters[:auto_approval_eligible]
  end

  def self.fetch_transfers(filters)
    new(filters).fetch_transfers
  end

  def fetch_transfers
    case @type
    when "paypal"
      transfers =
        if (@threshold_status == "under")
          PaypalTransfer.find_all_pending_under_approval_threshold
        else
          PaypalTransfer.find_all_pending_over_approval_threshold
        end
      transfers = transfers.auto_approval_eligible_transactions(@auto_approval_eligible)
    when "check"
      transfers = ManualCheckTransfer.send_scope(@status)
    else
      return { error: "There is no type of #{@type}" }
    end

    payee = (@type == "paypal") ? :paypal_address : :payee
    transfers = transfers.joins(:person) if @order.present? and @order.include? "people.name"
    transfers = transfers.order(@order) if @order.present?
    transfers = transfers.where(transfers.arel_table[payee].matches("%#{@keyword}%")) if @keyword.present?
    if transfers and @page and @per_page and @per_page.to_s != "all"
      return { transfers: transfers.page(@page.to_i).per_page(@per_page.to_i) }
    end

    { transfers: transfers }
  end

  private_class_method :new
end
