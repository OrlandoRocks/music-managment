# frozen_string_literal: true

class Admin::PaypalPayoutService
  class ApiError < StandardError; end
  include AfterCommitEverywhere

  attr_accessor :admin, :paypal_transfer

  SUCCESS_RESPONSES = ["Success"].freeze

  def self.call(admin, paypal_transfer)
    new(admin, paypal_transfer).call
  end

  def initialize(admin, paypal_transfer)
    @admin           = admin
    @paypal_transfer = paypal_transfer
  end

  # Guards for invalid or transfers in the wrong status.
  # Calls the mass_payout methd of the paypal-business gem
  # and pays just the single transfer.
  def call
    return :transfer_in_wrong_transfer_status unless processing?
    return create_audit(:skip)                unless valid?

    create_audit(:proccessing)
    handle_mass_pay_request_api do |response|
      raise ApiError.new(response.errors.inspect) unless response.ack.in? successful_responses

      paypal_transfer.with_lock do
        after_commit { handle_completed_paypal_transfer_payout }

        paypal_transfer.reload
        paypal_transfer.update!(correlation_id: response&.correlationID)
        paypal_transfer.mark_as_completed!
      end
    end
  end

  private

  delegate :create_outbound_invoice, to: :paypal_transfer
  delegate :payee_receiver,          to: :paypal_transfer
  delegate :processing?,             to: :paypal_transfer
  delegate :valid?,                  to: :paypal_transfer
  delegate :vat_tax_adjustment,      to: :paypal_transfer

  delegate :post_vat_transactions,   to: :vat_tax_adjustment

  def payee_country_website
    @payee_country_website ||=
      payee_receiver.country_website
  end

  def payee_corporate_entity_id
    @payee_corporate_entity_id ||=
      payee_receiver.corporate_entity_id.presence || fall_back_corporate_entity_id
  end

  def fall_back_corporate_entity_id
    Country.find_by(iso_code: payee_receiver.attributes["country"]).corporate_entity_id
  end

  def handle_completed_paypal_transfer_payout
    create_audit(:completed)
    post_vat_transactions if vat_tax_adjustment.present?
    create_outbound_invoice
  end

  ### PayoutProvider_client specific code below

  def formatted_payout_request
    @formatted_payout_request ||=
      [
        {
          id: paypal_transfer.id,
          amount: paypal_transfer.payment_amount,
          currency: paypal_transfer.currency,
          email: paypal_transfer.paypal_address,
        }
      ]
  end

  def payout_client
    @payout_client ||=
      PayPalBusiness::API.new(
        {
          username: payout_provider_config.username,
          password: payout_provider_config.decrypted_password,
          cert_file: payout_provider_config.cert_file,
          key_file: payout_provider_config.key_file
        }, Rails.env.production?
      )
  end

  def payout_provider_config
    @payout_provider_config ||=
      PayoutProviderConfig
      .by_env
      .find_by(
        name: PayoutProviderConfig::PAYPAL_NAME,
        currency: payee_country_website.currency,
        corporate_entity_id: payee_corporate_entity_id
      )
  end

  # Wraps the client and the logging and returns a soap response
  def handle_mass_pay_request_api
    resp = payout_client.mass_pay(items: formatted_payout_request)
    log(soap_response: resp)

    yield resp
  end

  def successful_responses
    SUCCESS_RESPONSES
  end

  # We'll continue to log soap responses using the previous logging strategy
  def log(soap_response:)
    response_data = {
      ack: soap_response.ack,
      timestamp: soap_response.timestamp,
      correlation_id: "[REDACTED]",
      paypal_transfer: paypal_transfer.attributes
    }
    Rails.logger.info("PayPal Mass Pay Response: #{response_data}")
  end

  ### AuditEntry specific code below

  def create_audit(status)
    AuditEntry.create(
      action: "MASS_PAY",
      comment: transfer_message(status)
    )
  end

  def transfer_message(status)
    case status
    when :skip       then skip_transfer_message
    when :processing then proccessing_transfer_message
    when :completed  then completed_transfer_message
    end
  end

  def skip_transfer_message
    "via Sidekiq, Warning: transfer ##{paypal_transfer.id} is not valid! Skipping for....... \n
    #{country_and_corporate_entity_id_message}"
  end

  def completed_transfer_message
    "via Sidekiq, Completed transfer id #{paypal_transfer.id}. \n
    #{country_and_corporate_entity_id_message}"
  end

  def proccessing_transfer_message
    "via Sidekiq, processing paypal_transfer, id: #{paypal_transfer.id}. \n
    #{country_and_corporate_entity_id_message}"
  end

  def country_and_corporate_entity_id_message
    "Country: #{payee_country_website.country}. Corporate Entity Id: #{payee_corporate_entity_id}"
  end
end
