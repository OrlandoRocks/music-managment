# frozen_string_literal: true

class Admin::PaypalMasspayService
  attr_accessor :transfers, :country_website, :user_id, :msgs, :paypal_responses

  SUCCESS_RESPONSE  = "Success"
  NO_VALID_PAYMENTS = "No valid payments selected"
  MASS_PAY_ACTION   = "MASS_PAY"

  def self.call(transfers, country_website, user_id)
    new(transfers, country_website, user_id).call
  end

  def initialize(transfers, country_website, user_id)
    @transfers = transfers
    @country_website = country_website
    @user_id = user_id
    @msgs = []
    @paypal_responses = {}
  end

  # TODO we have to use this success thing for now, because this is behind a feature flag and the code in front of the
  # flag expects a boolean and messages. Should replace boolean with a symbol once the flag is removed,
  # and this code is fully deployed
  def call
    msgs << NO_VALID_PAYMENTS unless valid_payments?
    return [false, msgs]      unless valid_payments?

    success = true
    paypal_payments_groups.each do |corporate_entity_id, paypal_payments|
      paypal_payments_subset =
        paypal_payments.map { |pp| { amount: pp[:amount], id: pp.delete(:id) } }
      result = withdrawal_account(corporate_entity_id).mass_pay(items: paypal_payments)
      paypal_responses[corporate_entity_id] = result
      log_request(paypal_transfers: paypal_payments_subset)
      log_response(
        soap_response: result,
        paypal_transfer_ids: paypal_payments_subset.pluck(:id)
      )
      next if [SUCCESS_RESPONSE].include?(result.ack)

      msgs << payment_failure_message(result)
      success = false
    end

    complete_transfers if success

    [success, msgs]
  end

  def valid_payments?
    return @valid_payments unless @valid_payments.nil?

    @valid_payments = processing_transfers.present? && paypal_payments_groups.present?
  end

  def processing_transfers
    return @processing_transfers unless @processing_transfers.nil?

    @processing_transfers = []
    transfers.each do |transfer|
      corporate_entity_id = transfer.person.corporate_entity_id
      msgs << transfer_message(:skip, transfer, corporate_entity_id) unless transfer.valid?
      next unless transfer.valid?

      if transfer.reload.transfer_status == PaypalTransfer::PENDING_STATUS # In case PaypalTransfers were passed and now stale.
        transfer.approving_transfer(user_id)
        @processing_transfers << transfer
        AuditEntry.create(
          action: MASS_PAY_ACTION,
          comment: transfer_message(:processing, transfer, corporate_entity_id)
        )
      else
        AuditEntry.create(
          action: MASS_PAY_ACTION,
          comment: transfer_message(:not_processing, transfer, corporate_entity_id)
        )
        msgs << transfer_message(:cannot_process, transfer, corporate_entity_id)
      end
    end

    @processing_transfers
  end

  def config_map
    return @config_map unless @config_map.nil?

    @config_map =
      PayoutProviderConfig
      .by_env
      .where(
        name: PayoutProviderConfig::PAYPAL_NAME,
        currency: country_website.currency
      ).index_by(&:corporate_entity_id)
  end

  def paypal_payments_groups
    return @paypal_payments_groups unless @paypal_payments_groups.nil?

    @paypal_payments_groups =
      transfers.each_with_object({}) do |transfer, hash|
        corporate_entity_id = transfer_corporate_entity_id(transfer)
        mass_pay_object = {
          email: transfer.paypal_address,
          amount: transfer.payment_amount,
          currency: transfer.currency,
          id: transfer.id
        }

        (hash[corporate_entity_id] ||= []).push(mass_pay_object)
      end
  end

  def withdrawal_account(corporate_entity_id)
    config = config_map[corporate_entity_id]
    creds = {
      username: config.username,
      password: config.decrypted_password,
      cert_file: config.cert_file,
      key_file: config.key_file
    }

    PayPalBusiness::API.new(creds, Rails.env.production?)
  end

  def complete_transfers
    processing_transfers.each do |transfer|
      next unless transfer.valid?

      corporate_entity_id = transfer.person.corporate_entity_id
      paypal_response = paypal_responses[corporate_entity_id]
      transfer.update(correlation_id: paypal_response&.correlationID)
      transfer.mark_as_completed!
      transfer.vat_tax_adjustment.post_vat_transactions if transfer.vat_tax_adjustment.present?
      transfer.create_outbound_invoice
      AuditEntry.create(
        action: MASS_PAY_ACTION,
        comment: transfer_message(:completed, transfer, corporate_entity_id)
      )
    end
  end

  def transfer_corporate_entity_id(transfer)
    person = transfer.person
    return person.corporate_entity_id if person.corporate_entity_id.present?

    Country.find_by_iso_code(person.attributes["country"]).corporate_entity_id
  end

  def flag_exists?(person, flag_attributes)
    Person::FlagService.person_flag_exists?({ person: person, flag_attributes: flag_attributes })
  end

  def corp_entity_id(name)
    CorporateEntity.find_by(name: name).id
  end

  def transfer_message(type, transfer, corporate_entity_id)
    country_and_ce_msg = country_and_corporate_entity_id_message(corporate_entity_id)
    case type
    when :skip
      "#{skip_transfer_message(transfer)} #{country_and_ce_msg}"
    when :processing
      "#{processing_transfer_message(transfer)} #{country_and_ce_msg}"
    when :not_processing
      "#{not_processing_transfer_message(transfer)} #{country_and_ce_msg}"
    when :cannot_process
      "#{cannot_process_transfer_message(transfer)} #{country_and_ce_msg}"
    when :completed
      "#{completed_transfer_message(transfer)} #{country_and_ce_msg}"
    end
  end

  def skip_transfer_message(transfer)
    "Warning: transfer ##{transfer.id} is not valid! Skipping for......."
  end

  def processing_transfer_message(transfer)
    country = country_website.country
    "Processing transfer id #{transfer.id} for #{transfer.payment_amount} #{transfer.currency} for #{country}."
  end

  def not_processing_transfer_message(transfer)
    "NOT Processing transfer id #{transfer.id}. Invalid status: #{transfer.transfer_status}."
  end

  def cannot_process_transfer_message(transfer)
    "Transfer ##{transfer.id} is no longer 'pending', cannot change to 'processing'"
  end

  def completed_transfer_message(transfer)
    "Completed transfer id #{transfer.id}."
  end

  def country_and_corporate_entity_id_message(corporate_entity_id)
    "Country: #{country_website.country}. Corporate Entity Id: #{corporate_entity_id}"
  end

  def payment_failure_message(result)
    country = country_website.country
    "Request to PayPal failed. Country: #{country}. #{result.errors.longMessage}"
  end

  private

  def log_request(paypal_transfers:)
    request_data =
      paypal_transfers.map do |paypal_transfer|
        paypal_transfer.tap do |pt|
          pt[:email] = "[REDACTED]"
          pt[:amount] = pt[:amount].to_s
          pt[:timestamp] = Time.now.utc.iso8601
          pt[:paypal_transfer_id] = pt[:id]
        end
      end
    Rails.logger.info("PayPal Mass Pay Request: #{request_data}")
  end

  def log_response(soap_response:, paypal_transfer_ids:)
    response_data = {
      ack: soap_response.ack,
      timestamp: soap_response.timestamp,
      correlation_id: "[REDACTED]",
      paypal_transfer_ids: paypal_transfer_ids
    }
    Rails.logger.info("PayPal Mass Pay Response: #{response_data}")
  end
end
