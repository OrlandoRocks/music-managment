class Admin::PublishingComposerUpdaterService
  def self.update(publishing_composer, publishing_composer_params, publisher_attributes)
    new(publishing_composer, publishing_composer_params, publisher_attributes).tap(&:update)
  end

  def initialize(publishing_composer, publishing_composer_params, publisher_attributes)
    @publishing_composer = publishing_composer
    @publishing_composer_params = publishing_composer_params
    @publisher_attributes = publisher_attributes
  end

  def update
    @publishing_composer.create_or_update_publisher(@publisher_attributes || {})

    @update_success = @publishing_composer.update(@publishing_composer_params)
    @rights_app_error = @publishing_composer.send_update_to_publishing_administration if @update_success

    @error = nil
    if !@update_success
      @error = {
        update_success: false,
        message: "PublishingComposer #{@publishing_composer.id} was unable to be updated"
      }
    elsif @rights_app_error.present?
      @error = { update_success: true, message: @rights_app_error }
    end
  end
end
