class Admin::CompositionsCsvService
  def self.create(compositions)
    new(compositions).create
  end

  def initialize(compositions)
    @compositions = compositions
  end

  def create
    csv_to_download =
      CSV.generate do |csv|
        csv << [
          "Composition Name",
          "Composition ISRC",
          "Share Submitted Date",
          "Share %",
          "RightsApp Work Code",
          "RightsApp Recording ID"
        ]
        @compositions.each do |composition|
          csv << [
            composition.name,
            composition.isrc,
            composition.share_submitted_date,
            (composition.composer_share + composition.cowriter_share),
            composition.provider_identifier,
            composition.recording_code
          ]
        end
      end
  end
end
