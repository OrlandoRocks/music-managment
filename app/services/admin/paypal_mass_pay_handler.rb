# frozen_string_literal: true

class Admin::PaypalMassPayHandler
  include AfterCommitEverywhere

  attr_accessor :admin, :paypal_transfers, :messages

  def initialize(admin, paypal_transfers)
    @admin            = admin
    @paypal_transfers = Array.wrap(paypal_transfers)
    @messages         = []
  end

  def self.call(admin, paypal_transfers)
    new(admin, paypal_transfers).call
  end

  def call
    paypal_transfers.each do |paypal_transfer|
      next unless paypal_transfer.valid?

      ApplicationRecord.transaction(requires_new: true) do
        after_commit { trigger_async_payout_for(paypal_transfer) }
        paypal_transfer.lock!
        paypal_transfer.reload
        paypal_transfer.approving_transfer(admin.id)
      rescue Workflow::NoTransitionAllowed => e
        messages << "PaypalTransfer id: #{paypal_transfer.id}, #{e} \n"
      end
    end

    messages
  end

  def trigger_async_payout_for(transfer)
    PaypalTransfers::PayoutWorker.perform_async(admin.id, transfer.id)
  end
end
