# frozen_string_literal: true

class Admin::CopyrightStatusUpdaterService
  attr_reader :copyright_status, :status, :admin, :note, :error

  def self.update(copyright_status, admin, params)
    new(copyright_status, admin, params).tap(&:update)
  end

  def initialize(copyright_status, admin, params)
    @copyright_status = copyright_status
    @status = params[:status]
    @admin = admin
    @note = note_params(params)
  end

  def update
    ActiveRecord::Base.transaction do
      copyright_status.copyright_state_transitions.create!(
        prev_state: copyright_status.status,
        state: status,
        person: admin
      )
      copyright_status.related.notes.create!(note)
      copyright_status.update!(status: status)
    rescue => e
      @error = e
    end
  end

  private

  def note_params(params)
    params[:note].merge!(
      subject: "Legal Review Status Updated to '#{@status}'",
      note_created_by_id: admin.id
    )
  end
end
