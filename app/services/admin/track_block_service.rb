# frozen_string_literal: true

class Admin::TrackBlockService
  attr_reader :person, :song_id, :store_id

  def initialize(params = {})
    @song_id = params[:song_id]
    @person = params[:person]
    @store_id = params[:store_id]
  end

  def self.send_track(params)
    new(params).create_track_monetization
  end

  def create_track_monetization
    account_id = Song.find(song_id).album.person.id
    track = TrackMonetization.find_or_create_by(
      person_id: account_id,
      song_id: song_id,
      store_id: store_id
    )
    track.update!(
      state: "new",
      delivery_type: "full_delivery",
      eligibility_status: "eligible"
    )
    send_for_delivery(track)
  end

  def send_for_delivery(track)
    track.start(actor: person.to_s, message: "Starting Delivery To #{Store.find(store_id).name}")
  end
end
