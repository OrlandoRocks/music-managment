class StoreLaunchEmail
  attr_reader :store, :person, :releases

  def self.send_store_launch_email(store_id, person_id, releases)
    new(store_id, person_id, releases).tap(&:send_store_launch_email)
  end

  def initialize(store_id, person_id, releases)
    @store     = Store.find(store_id)
    @person    = Person.find(person_id)
    @releases  = releases
  end

  def send_store_launch_email
    Rails.logger.info "Sending email to #{person.id}"
    AutomatorMailer.store_launch(store, person, release_data).deliver
    remove_person_from_email_list_redis
  end

  def release_data
    Rails.logger.info "Fetching album data for #{releases.length} releases for person #{person.id}"
    releases.map do |album_id|
      album = Album.find(album_id)
      {
        artist_name: album.artist_name,
        album_type: album.album_type,
        album_name: album.name,
        upc: album.upc.number
      }
    end
  end

  def remove_person_from_email_list_redis
    email_list = JSON.parse($redis.get("StoreEmails:#{store.short_name}"))
    email_list.delete(person.id.to_s)
    $redis.set("StoreEmails:#{store.short_name}", email_list.to_json)
  end
end
