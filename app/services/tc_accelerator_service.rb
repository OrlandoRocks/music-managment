# frozen_string_literal: true

class TcAcceleratorService
  attr_reader :person, :opt, :flag_attributes

  def self.opt!(person, opt)
    new(person, opt).opt!
  end

  def self.do_not_notify!(person, opt)
    new(person, opt).do_not_notify!
  end

  def initialize(person, opt)
    @person = person
    @opt = opt
    @flag_attributes = opt.nil? ? Flag::TC_ACCELERATOR_DO_NOT_NOTIFY : Flag::TC_ACCELERATOR_OPTED_OUT
  end

  def opt!
    case opt
    when :in
      opt_in!
    when :out
      opt_out!
    end
  end

  def do_not_notify!
    return if person.tc_accelerator_do_not_notify?

    Person::FlagService.flag!(person: person, flag_attributes: flag_attributes)
  end

  private

  def opt_in!
    return if person.tc_accelerator_opted_in?

    Person::FlagService.unflag!(person: person, flag_attributes: flag_attributes)
  end

  def opt_out!
    return if person.tc_accelerator_opted_out?

    Person::FlagService.flag!(person: person, flag_attributes: flag_attributes)
  end
end
