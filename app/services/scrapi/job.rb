class Scrapi::Job
  attr_reader :id, :status, :size, :duration, :format, :metadata
  attr_accessor :song_id

  WAITING_STATES = %w[waiting_file in_queue in_progress]

  def content=(content)
    @old_status = @status
    @content = JSON.parse(content)

    @song_id ||= @content["song_id"] if @content["song_id"]
    @id = @content["data"]["id"]
    @status = @content["data"]["status"]
    return unless is_done? and @content["data"]["recognitionAnalysis"].present?

    recognitionAnalysis = @content["data"]["recognitionAnalysis"]
    @size = recognitionAnalysis["size"].to_i
    return unless recognitionAnalysis["content"]

    if recognitionAnalysis["content"]["fileInfo"]
      if recognitionAnalysis["content"]["fileInfo"]["duration"]
        @duration = recognitionAnalysis["content"]["fileInfo"]["duration"]["raw"].to_f.ceil
      end
      @format = recognitionAnalysis["content"]["fileInfo"]["format"]["raw"]
    end
    @metadata = recognitionAnalysis["content"]["metadata"]
  end

  def to_model
    dbo = ScrapiJob.find_by(job_id: id)
    if dbo.present?
      dbo.update(job_id: id, status: status, song_id: song_id)
      if metadata
        dbo.scrapi_job_details.destroy_all
        metadata.map { |d| job_detail_attributes(d) }.each do |attrs|
          dbo.scrapi_job_details << ScrapiJobDetail.new(attrs)
        end
        dbo.update(is_matched: true)
      end
    else
      dbo = ScrapiJob.new(job_id: id, status: status, song_id: song_id)
      if metadata
        metadata.map { |d| job_detail_attributes(d) }.each do |attrs|
          dbo.scrapi_job_details << ScrapiJobDetail.new(attrs)
        end
        dbo.is_matched = true
      end
    end
    dbo.save
    dbo
  end

  def save
    error! if transitioned_to_error?
  end

  def self.from_content(content)
    job = new
    job.content = content
    job
  end

  def self.from_id(id)
    scrapi_job = ScrapiJob.find_by(job_id: id)
    raise RuntimeError, "No job found with id #{id}" unless scrapi_job

    job = new
    job.content = { data: { status: scrapi_job.status, id: scrapi_job.job_id }, song_id: scrapi_job.song_id }.to_json
    job
  end

  def transitioned_to_error?
    WAITING_STATES.include? @old_status and @status == "error"
  end

  def error!
    Tunecore::Airbrake.notify("Scrapi Job Failed #{id}")
  end

  def is_done?
    status == "done"
  end

  def is_error?
    status == "error"
  end

  def is_waiting?
    WAITING_STATES.include? status
  end

  private

  def job_detail_attributes(detail)
    track = detail["track"]
    album = detail["album"]
    {
      track_title: track["title"],
      isrc: track["ISRC"],
      duration: (track["duration"] * 60).ceil,
      label: track["label"],
      album_title: album["title"],
      album_upc: album["UPC"],
      artists: track["artists"],
      genres: track["genres"],
      score: detail["score"].to_i
    }
  end
end
