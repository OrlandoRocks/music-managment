class PaymentsOS::CustomerService
  def initialize(person)
    @person = person
    @api_client = PaymentsOS::ApiClient.new(person)
  end

  def find_or_create_customer
    parsed_customer_response = @person.payments_os_customer.present? ? fetch_customer : create_customer
    unless parsed_customer_response.created? && parsed_customer_response.errors.blank?
      return nil, parsed_customer_response
    end

    payments_os_customer = PaymentsOSCustomer.find_or_create_by(
      customer_reference: parsed_customer_response.customer_reference,
      customer_id: parsed_customer_response.customer_id,
      person_id: @person.id
    )
    return payments_os_customer, parsed_customer_response if payments_os_customer.errors.blank?

    return nil, parsed_customer_response
  end

  private

  def create_customer
    @api_client.send_request(:new_customer, person: @person)
  end

  def fetch_customer
    @api_client.send_request(:customer, person: @person)
  end
end
