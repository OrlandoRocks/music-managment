class PaymentsOS::PaymentsService
  include Rails.application.routes.url_helpers

  def initialize(invoice, stored_credit_card, cvv)
    @invoice = invoice
    @stored_credit_card = stored_credit_card
    @person = stored_credit_card.person
    @cvv = cvv
  end

  def pay(renew = false)
    payments_os_transaction = PaymentsOSTransaction.create(
      person: @person,
      stored_credit_card: @stored_credit_card,
      invoice: @invoice,
      action: PaymentsOSTransaction::SALE,
      amount: @invoice.outstanding_amount_cents,
      currency: @person.site_currency
    )
    @api_client = PaymentsOS::ApiClient.new(@person)

    payment = create_payment(payments_os_transaction.id)
    unless payment.success?
      payments_os_transaction.update(
        payment_status: PaymentsOSTransaction::FAILED,
        payment_raw_response: format(payment.response_json)
      )
      set_error_details_in_invoice
      return payments_os_transaction
    end
    payments_os_transaction.update(
      payment_id: payment.id,
      payment_status: payment.status,
      payment_raw_response: format(payment.response_json)
    )

    charge = renew ? create_recurring_charge(payment.id) : create_charge(payment.id)
    unless charge.success?
      payments_os_transaction.update(
        charge_status: PaymentsOSTransaction::FAILED,
        charge_raw_response: format(charge.response_json)
      )
      set_error_details_in_invoice
      return payments_os_transaction
    end
    payments_os_transaction.update(
      charge_id: charge.id,
      charge_status: charge.status,
      charge_raw_response: format(charge.response_json),
      reconciliation_id: charge.reconciliation_id,
      otp_redirect_url: charge.redirect_url
    )
    payments_os_transaction
  end

  private

  def create_payment(order_id)
    converted_amount = @invoice.outstanding_amount_in_local_currency.to_i
    @api_client.send_request(
      :payment,
      {
        stored_cc: @stored_credit_card,
        order_id: order_id,
        amount: converted_amount
      }
    )
  end

  def create_charge(payment_id)
    return_url = finalize_after_otp_cart_url(
      host: url_hash[@stored_credit_card.person.country_domain]
    )
    @api_client.send_request(
      :charge,
      {
        payment_id: payment_id,
        card_token: @stored_credit_card.payments_os_token,
        cvv: @cvv,
        return_url: return_url
      }
    )
  end

  def create_recurring_charge(payment_id)
    @api_client.send_request(
      :recurring_charge,
      {
        payment_id: payment_id,
        card_token: @stored_credit_card.payments_os_token
      }
    )
  end

  def url_hash
    return WEB_ONLY_URLS if Rails.env.production?

    COUNTRY_URLS
  end

  def set_error_details_in_invoice
    @invoice.errors[:base] << "Payment gateway failure"
  end

  def format(response)
    JSON.pretty_generate(JSON.parse(response))
  end
end
