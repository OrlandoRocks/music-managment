class PaymentsOS::CardService
  def initialize(person)
    @person = person
    @api_client = PaymentsOS::ApiClient.new(person)
  end

  def store_payment_method(stored_cc_params = {})
    return [:no_cc_params, "no cc params given"] if stored_cc_params.blank?

    @stored_cc_params = stored_cc_params.with_indifferent_access
    @payments_os_customer = @person.payments_os_customer
    @store_payment_response = @api_client.send_request(
      :new_payment_method,
      customer_id: @payments_os_customer.customer_id,
      stored_cc_params: @stored_cc_params
    )
    return [:card_not_created, @store_payment_response] unless @store_payment_response.card_created?

    @stored_credit_card = @person.stored_credit_cards.create(stored_credit_card_params)
    status = @stored_credit_card.errors.blank? ? :ok : :invalid_card
    [status, @stored_credit_card]
  end

  private

  def stored_credit_card_params
    {
      first_name: @stored_cc_params.fetch(:firstname, @person.first_name),
      last_name: @stored_cc_params.fetch(:lastname, @person.last_name),
      cc_type: @store_payment_response.cc_type,
      last_four: @store_payment_response.last_4_digits,
      expiration_month: @store_payment_response.expiration_month,
      expiration_year: @store_payment_response.expiration_year,
      payments_os_token: @store_payment_response.token,
      customer_vault_id: "0", # not null field applicable only for braintree credit card
      company: @stored_cc_params[:company],
      address1: @stored_cc_params[:address1],
      address2: @stored_cc_params[:address2],
      city: @stored_cc_params[:state_city_name],
      state: get_state_name,
      country: @stored_cc_params[:country],
      zip: @stored_cc_params[:zip],
      status: "current",
      state_id: @stored_cc_params[:state_id],
      state_city_id: get_state_city_id
    }
  end

  def get_state_city_id
    city = CountryStateCity
           .for_state(@stored_cc_params[:state_id])
           .exact_city_name(@stored_cc_params[:state_city_name])
    city.pluck(:id).first
  end

  def get_state_name
    CountryState.find(@stored_cc_params[:state_id]).iso_code
  end
end
