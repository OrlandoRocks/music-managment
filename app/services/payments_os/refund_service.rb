class PaymentsOS::RefundService
  include IndiaPaymentsHelper
  include CurrencyHelper

  def initialize(payments_os_transaction, decorate = true)
    @original_transaction = payments_os_transaction
    @api_client = PaymentsOS::ApiClient.new(@original_transaction.person)
    @decorate = decorate
  end

  def refund(amount, refund_category, refund_reason)
    pegged_rate = @original_transaction.invoice.pegged_rate
    exchanged_amount = pegged_rate.present? ? (amount.to_f * pegged_rate) : amount.to_f
    refund_response = @api_client.send_request(
      :refund,
      payment_id: @original_transaction.payment_id,
      amount: currency_superunit_to_subunit(exchanged_amount)
    )

    create_refund_transaction(
      refund_reason: refund_reason,
      amount: currency_superunit_to_subunit(amount),
      refund_category: refund_category,
      refund_id: refund_response.refund_transaction_id,
      refund_status: refund_response.status,
      refund_raw_response: refund_response.raw_response.to_json
    )
  end

  private

  def create_refund_transaction(refund_data)
    refund_transaction = PaymentsOSTransaction.create(refund_transaction_params(refund_data))
    return refund_transaction unless @decorate

    PaymentsOSRefundDecorator.new(refund_transaction)
  end

  def refund_transaction_params(refund_data)
    {
      person: @original_transaction.person,
      stored_credit_card: @original_transaction.stored_credit_card,
      invoice: @original_transaction.invoice,
      action: PaymentsOSTransaction::REFUND,
      currency: @original_transaction.currency,
      original_transaction_id: @original_transaction.id
    }.merge(refund_data)
  end
end
