module Disputes
  class BraintreeDispute
    class InvalidDisputeError < StandardError; end

    def initialize(bt_signature, bt_payload)
      @bt_signature = bt_signature
      @bt_payload = bt_payload
    end

    def update_or_create_dispute!
      self.parsed_response = parse_webhook!
      update_or_create_record!
      CurrencyConversionWorker.perform_async(dispute.id) if need_currency_conversion?

      dispute
    end

    private

    attr_accessor :parsed_response
    attr_reader :bt_signature, :bt_payload

    def parse_webhook!
      Braintree::WebhookParser.new.parse!(bt_signature, bt_payload).dispute
    rescue Braintree::InvalidChallenge, Braintree::InvalidSignature => e
      Tunecore::Airbrake.notify(
        error: e,
        bt_signatue: bt_signature,
        bt_payload: bt_payload
      )
      raise InvalidDisputeError.new(e)
    end

    def update_or_create_record!
      dispute.new_record? ? dispute.save! : update_dispute_status!
    rescue ActiveRecord::RecordInvalid => e
      Tunecore::Airbrake.notify(e, dispute: dispute)
      raise InvalidDisputeError.new(e)
    end

    def dispute
      @dispute ||=
        Dispute.find_or_initialize_by(dispute_identifier: dispute_identifier) do |dispute|
          dispute.reason = parsed_response.reason
          dispute.initiated_at = parsed_response.created_at
          dispute.transaction_identifier = transaction_identifier
          dispute.source = source
          dispute.dispute_status = dispute_status
          dispute.payload_amount = parsed_response.amount
          dispute.payload_currency = parsed_response.currency_iso_code
          dispute.amount_cents = amount_cents
          dispute.currency = source.currency
        end
    end

    def dispute_identifier
      parsed_response.id
    end

    def transaction_identifier
      @transaction_identifier ||= parsed_response.transaction.id
    end

    def source
      BraintreeTransaction.find_by!(transaction_id: transaction_identifier)
    rescue ActiveRecord::RecordNotFound => e
      Tunecore::Airbrake.notify(e, transaction_id: transaction_identifier)
      raise InvalidDisputeError.new(e)
    end

    def dispute_status
      @dispute_status =
        DisputeStatus.find_by!(
          source_type: DisputeStatus::BRAINTREE,
          status: dispute_status_name
        )
    rescue ActiveRecord::RecordNotFound => e
      Tunecore::Airbrake.notify(
        e,
        source_type: DisputeStatus::BRAINTREE,
        status: dispute_status_name
      )
      raise InvalidDisputeError.new(e)
    end

    # Populate amount_cents as 0 if origianl currency and payload currency are
    # different. Currency conversion will be performed later in a worker.
    def amount_cents
      return 0 if need_currency_conversion?

      (parsed_response.amount * 100).round
    end

    def dispute_status_name
      @dispute_status_name ||= parsed_response.status
    end

    def update_dispute_status!
      dispute.update!(dispute_status: dispute_status)
    end

    def need_currency_conversion?
      source.currency != parsed_response.currency_iso_code
    end
  end
end
