# frozen_string_literatl: true

module Disputes
  class TransactionListingService
    DEFAULT_PAGE = 1
    DEFAULT_PER_PAGE = 100
    MAX_PER_PAGE = 5000

    DEFAULT_SORT_ORDER = "asc"
    DEFAULT_SORT_COLUMN = "disputes.initiated_at"

    SORT_ORDERS = %w[asc desc].freeze
    SORT_COLUMNS = %w[
      initiated_at
      transaction_identifier
      dispute_identifier
      amount_cents
    ].freeze

    FIELDS_TO_SELECT = %w[
      disputes.id
      disputes.initiated_at\ as\ created_at
      disputes.updated_at
      disputes.action_type
      disputes.transaction_identifier
      disputes.dispute_identifier
      disputes.amount_cents
      disputes.currency
      dispute_statuses.source_type
      dispute_statuses.status
      people.name
    ].freeze

    FILTERS = {
      source_type: ->(scope, source_type) {
        scope.where(
          dispute_statuses: {
            source_type: source_type
          }
        )
      },
      status: ->(scope, status) {
        scope.where(
          dispute_statuses: {
            id: status
          }
        )
      },
      transaction_identifier: ->(scope, transaction_identifier) {
        scope.where(transaction_identifier: transaction_identifier)
      },
      dispute_identifier: ->(scope, dispute_identifier) {
        scope.where(dispute_identifier: dispute_identifier)
      },
      start_date: ->(scope, start_date) {
        scope.where("disputes.initiated_at >= ?", start_date.to_date.beginning_of_day)
      },
      end_date: ->(scope, end_date) {
        scope.where("disputes.initiated_at <= ?", end_date.to_date.end_of_day)
      },
      minimum_amount: ->(scope, minimum_amount) {
        minimum_amount_cents = Integer(Float(minimum_amount) * 100)
        scope.where("disputes.amount_cents >= ?", minimum_amount_cents)
      },
      maximum_amount: ->(scope, maximum_amount) {
        maximum_amount_cents = Integer(Float(maximum_amount) * 100)
        scope.where("disputes.amount_cents <= ?", maximum_amount_cents)
      }
    }.freeze

    def initialize(
      page: DEFAULT_PAGE,
      per_page: DEFAULT_PER_PAGE,
      sort_column: DEFAULT_SORT_COLUMN,
      sort_order: DEFAULT_SORT_ORDER,
      **search_filters
    )
      @page = Integer(page)
      @per_page = Integer(per_page)
      @sort_column = formatted_sort_column(sort_column)
      @sort_order = sort_order
      @search_filters = search_filters
    end

    def disputes
      fetch_disputes.merge(max_chargeback_amount)
    end

    def formatted_disputes
      disputes.map do |dispute|
        dispute.tap do |obj|
          obj.status = obj.status.humanize
          obj.source_type = DisputeStatus::SOURCE_TYPE_NAMES[obj.source_type]
        end
      end
    end

    def self.transaction_types
      DisputeStatus
        .select(:source_type, :status, :id)
        .group_by(&:source_type)
        .map do |source_type, status_obj_array|
          {
            source_type: DisputeStatus::SOURCE_TYPE_NAMES[source_type],
            statuses: status_obj_array.map do |status_object|
              {
                id: status_object.id,
                status: status_object.status.humanize
              }
            end
          }
        end
    end

    private

    attr_reader :page, :per_page, :sort_column, :sort_order, :search_filters

    def fetch_disputes
      apply_filters
        .order(order_by_clause)
        .paginate(
          page: page,
          per_page: [per_page, MAX_PER_PAGE].min
        ).select(*FIELDS_TO_SELECT)
    end

    def apply_filters
      scope = Dispute
              .joins(:dispute_status, :person)
              .group("disputes.id")

      search_filters.each_pair do |filter_name, value|
        scope = FILTERS[filter_name].call(scope, value) if FILTERS[filter_name].present?
      end

      scope
    end

    def order_by_clause
      return "disputes.#{sort_column} #{sort_order}" if valid_order_by_clause?

      "#{DEFAULT_SORT_COLUMN} #{DEFAULT_SORT_ORDER}"
    end

    def valid_order_by_clause?
      SORT_ORDERS.include?(sort_order) &&
        SORT_COLUMNS.include?(sort_column)
    end

    def formatted_sort_column(sort_column)
      return "initiated_at" if sort_column == "created_at"

      sort_column
    end

    def max_chargeback_amount
      Disputes::QueryObjects::MaxChargebackAmount.new.query
    end
  end
end
