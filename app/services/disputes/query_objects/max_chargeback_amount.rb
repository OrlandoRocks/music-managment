# frozen_string_literal: true

module Disputes
  module QueryObjects
    class MaxChargebackAmount
      include ArelTableMethods

      def initialize(scope = Dispute.all)
        @scope = initial_scope.merge(scope)
      end

      def query
        scope.select(projection)
      end

      def for_dispute_id(id)
        query.find_by(disputes: { id: id })
      end

      private

      attr_reader :scope, :dispute_id

      def initial_scope
        InvoiceSettlementJoin
          .new
          .query
          .merge(RefundSettlementJoin.new.query)
      end

      def projection
        Arel::Nodes::Case
          .new
          .when(dispute_t[:refund_id].not_eq(nil))
          .then(0)
          .else(
            calculate_max_chargeback_amount_cents
          ).as("max_chargeback_amount_cents")
      end

      def calculate_max_chargeback_amount_cents
        Arel::Nodes::Group.new(
          invoice_settlement_t[:settlement_amount_cents] - refunded_amount_cents
        )
      end

      def refunded_amount_cents
        Arel::Nodes::NamedFunction.new(
          "COALESCE", [
            Arel::Nodes::NamedFunction.new(
              "SUM", [
                refund_settlement_t[:settlement_amount_cents]
              ]
            ),
            0
          ]
        )
      end
    end
  end
end
