# frozen_string_literal: true

module Disputes
  module QueryObjects
    class InvoiceSettlementJoin
      include ArelTableMethods

      def query
        Dispute.joins(invoice_settlements_join)
      end

      private

      def invoice_settlements_join
        constraints = invoice_settlement_t[:source_id]
                      .eq(dispute_t[:source_id])
                      .and(
                        dispute_t[:source_type]
                        .eq(invoice_settlement_t[:source_type])
                      )

        dispute_t.join(invoice_settlement_t, Arel::Nodes::OuterJoin)
                 .on(constraints).join_sources
      end
    end
  end
end
