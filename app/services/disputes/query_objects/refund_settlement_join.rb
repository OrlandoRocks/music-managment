# frozen_string_literal: true

module Disputes
  module QueryObjects
    class RefundSettlementJoin
      include ArelTableMethods

      def query
        Dispute.joins(refunds_join, refund_settlements_join)
      end

      private

      def refunds_join
        constraints = refund_t[:invoice_id].eq(dispute_t[:invoice_id])

        dispute_t
          .join(refund_t, Arel::Nodes::OuterJoin)
          .on(constraints)
          .join_sources
      end

      def refund_settlements_join
        constraints = refund_settlement_t[:refund_id]
                      .eq(refund_t[:id])
                      .and(disputes_join_constraint.or(refunds_join_constraint))

        dispute_t
          .join(refund_settlement_t, Arel::Nodes::OuterJoin)
          .on(constraints)
          .join_sources
      end

      def disputes_join_constraint
        refund_settlement_t[:source_type]
          .eq("Dispute")
      end

      def refunds_join_constraint
        refund_settlement_t[:source_type]
          .eq(dispute_t[:source_type])
      end

      def historic_dispute_ids
        dispute_t.project(:id).where(
          dispute_t[:source_type].eq(refund_settlement_t[:source_type])
        )
      end
    end
  end
end
