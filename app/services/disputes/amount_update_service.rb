module Disputes
  class AmountUpdateService
    delegate :currency,
             :payload_amount,
             :payload_currency,
             :initiated_at, to: :dispute, private: true

    def self.call!(dispute_id)
      new(dispute_id).update!
    end

    def initialize(dispute_id)
      @dispute = Dispute.find(dispute_id)
    end

    def update!
      dispute.update!(
        amount_cents: amount_cents,
        foreign_exchange_rate: currency_converter.fx_rate
      )
    end

    private

    attr_reader :dispute

    def amount_cents
      (currency_converter.amount_in_original_currency * 100).round
    end

    def currency_converter
      @currency_converter ||=
        Disputes::CurrencyConverter.new(
          original_currency: currency,
          payload_amount: payload_amount,
          payload_currency: payload_currency,
          dispute_date: initiated_at
        )
    end
  end
end
