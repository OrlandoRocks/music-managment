# frozen_string_literal: true

module Disputes
  class CurrencyConverter
    class ExchangeRateFetchFailedError < StandardError; end

    def initialize(original_currency:, payload_currency:, payload_amount:, dispute_date:)
      @original_currency = original_currency
      @payload_currency = payload_currency
      @payload_amount = Float(payload_amount)
      @dispute_date = dispute_date.to_datetime
    end

    def amount_in_original_currency
      return payload_amount if same_currency?

      payload_amount * fx_rate.exchange_rate
    end

    def fx_rate
      return if same_currency?

      @fx_rate ||= get_fx_rate!
    end

    private

    attr_reader :original_currency, :payload_currency, :payload_amount, :dispute_date

    def get_fx_rate!
      fetch_fx_rate_from_db || store_fx_rate_from_currency_layer!
    end

    def same_currency?
      original_currency == payload_currency
    end

    def fetch_fx_rate_from_db
      ForeignExchangeRate
        .for_date(source: payload_currency, target: original_currency, date: dispute_date)
    end

    def store_fx_rate_from_currency_layer!
      Currencylayer::StoreExchangeRates.call!(
        Currencylayer::HistoricRateFetcher,
        source_currency: payload_currency,
        target_currencies: original_currency,
        date: dispute_date
      ).first
    rescue Currencylayer::StoreExchangeRates::InvalidRateError => e
      raise ExchangeRateFetchFailedError.new(e)
    end
  end
end
