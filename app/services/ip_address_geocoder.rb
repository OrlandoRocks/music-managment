class IpAddressGeocoder
  def self.geocoded_attributes(ip_address, options = {})
    response = Faraday.new({ url: "#{ENV['GEOLOCATION_ENDPOINT']}/#{ip_address}" }.merge(options)).get
    return {} unless response.success?

    JSON.parse(response.body)
  end

  def self.geocode(ip_address, options = {})
    response_body = geocoded_attributes(ip_address, options)
    return {} if response_body.empty?

    response_body["country_code"] = Country.find_by(name: response_body["country"])&.iso_code
    response_body
  end
end
