class Apple::CreateAppleArtistsServiceWorker
  include Sidekiq::Worker
  sidekiq_options queue: "delivery-apple", backtrace: 45, retry: 0

  RATE_LIMIT_WAIT_TIMEOUT = 300
  RATE_LIMIT_LOCK_TIMEOUT = 10_000

  TRANSPORTER_RATE_LIMITER = Sidekiq::Limiter.concurrent(
    "transporter-rate-limit",
    50,
    wait_timeout: RATE_LIMIT_WAIT_TIMEOUT,
    lock_timeout: RATE_LIMIT_LOCK_TIMEOUT
  )

  def perform(salepointable_id)
    TRANSPORTER_RATE_LIMITER.within_limit do
      Apple::CreateAppleArtistsService.for_album(salepointable_id)
    end
  end
end
