# frozen_string_literal: true

# Validate a creative's Apple External Service ID.
# Valid if Apple's name matches our name (after name is parsed).
#
# Used:
# Client-side, on artist modal form input:
#  - Filters 'search results' and 'manual input' response
#  - Transporter not used
#
# Server-side, synchronous, during album POST:
#  - Persisted artist name is compared with Apple record, which is now fetched by identifier
#    that was on the submitted Creative hash
#  - Transporter used to fetch ID
#
# Legacy, server-side only, Sidekiq job following form submission
#  - Same as current server-side
#  - Ringtones will still depend on this after Album App rollout
class Apple::ArtistIdValidator
  INVALID_APPLE_ARTIST_NAME = "Apple ID Validator: Invalid Apple artist name."
  APPLE_ID_VALIDATOR_ERROR  = "Apple ID Validator: apple_name required when check_id == false."

  def self.valid?(options)
    new(options).valid?
  end

  attr_reader :apple_name, :artist, :check_id, :identifier, :name_param, :result

  def initialize(options)
    artist_id = options["artist_id"]

    @apple_name = options["apple_name"]
    @artist     = artist_id ? Artist.find(artist_id) : nil
    @check_id   = options.fetch("check_id", true)
    @identifier = options["identifier"]
    @name_param = options["name"]
    @result     = set_result

    validate_configuration
  end

  def valid?
    valid_apple_id? && valid_artist_name?
  end

  def valid_artist_name?
    artist_name        = ArtistNameUtils.new(name)
    result_artist_name = ArtistNameUtils.new(result[:name])

    if artist_name.normalize.present? && result_artist_name.normalize.present?
      artist_name.parse_for_comparison == result_artist_name.parse_for_comparison
    else
      Airbrake.notify(
        INVALID_APPLE_ARTIST_NAME,
        {
          artist_name: artist_name,
          apple_artist_name: result_artist_name
        }
      )
      false
    end
  end

  private

  def validate_configuration
    raise APPLE_ID_VALIDATOR_ERROR if !check_id && !apple_name
  end

  def set_result
    return $apple_transporter_client.transport(:lookup_item, "artist", identifier) if check_id

    { name: apple_name }
  end

  def valid_apple_id?
    return true unless check_id

    identifier == result[:apple_id]
  end

  def name
    artist ? artist.name : name_param
  end
end
