class Apple::ArtistIdCsvDbLoader
  def self.upload(path_to_csv, from_index = nil, to_index = nil)
    new(path_to_csv).upload(from_index, to_index)
  end

  def initialize(path_to_csv)
    @path_to_csv = path_to_csv
  end

  def upload(from_index = nil, to_index = nil)
    CSV.foreach(@path_to_csv, headers: true).with_index(1) do |row, index|
      break if has_processed_last_index?(index, to_index)
      next if is_on_starting_index?(index, from_index)

      album = Album.find_by_upc(row["upc"])

      next unless album

      create_external_service_ids_for_release([album] + album.songs, row)
    end
  end

  private

  def create_external_service_ids_for_release(releases, row)
    releases.each do |release|
      primary_creatives = release.creatives.joins(:artist).where(role: "primary_artist").where(
        "LOWER(artists.name) = ?", row["artist_name"].downcase
      )

      primary_creatives.each do |creative|
        artist_id = row["artist_id"]

        ExternalServiceId.where(
          linkable_id: creative.id, service_name: "apple", linkable_type: "Creative"
        ).first_or_create(
          linkable: creative,
          identifier: artist_id,
          service_name: "apple",
          state: "matched"
        )
      end
    end
  end

  def has_processed_last_index?(index, to_index)
    to_index && index > to_index
  end

  def is_on_starting_index?(index, from_index)
    from_index && index < from_index
  end
end
