class Apple::TicketCreator
  def self.create_ticket(options)
    ZendeskTicketCreateMailer.create_ticket(options).deliver
  end
end
