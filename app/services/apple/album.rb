class Apple::Album
  def self.get_itunes_info(album_id)
    new(album_id).tap(&:get_itunes_info)
  end

  def self.create_or_update_itunes_status_failure(album_id)
    new(album_id).tap(&:create_or_update_itunes_status_failure)
  end

  attr_reader :album

  def initialize(album_id)
    @album = ::Album.find(album_id)
  end

  def get_itunes_info
    result = $apple_transporter_client.transport(:release_status, album.upc.number)
    Rails.logger.info("TRANSPORTER RESULT FOR ALBUM ID: #{album.id}")
    Rails.logger.info(result)

    if result[:apple_id]
      album.update(apple_identifier: result[:apple_id], known_live_on: Time.now)
      album.set_external_id_for("apple", result[:apple_id])
      create_or_update_apple_artist_ids(result) unless album.is_various
      create_or_update_itunes_status_success(result[:document])
    else
      create_or_update_itunes_status_failure
    end
  end

  def create_or_update_apple_artist_ids(result)
    album.artists.each do |artist|
      creatives = artist.creatives.where(creativeable_type: "Album", role: "primary_artist")
      apple_artist_id = get_valid_apple_artist_id(artist, result)

      if creatives.present? && apple_artist_id
        Rails.logger.info("TRANSPORTER RESULT FOR ALBUM ID: #{album.id}, ARTIST ID: #{artist.id}, APPLE_ARTIST_ID: #{apple_artist_id}")
        artist.update_or_create_external_service_id("apple", apple_artist_id, creatives)
      else
        Rails.logger.error("NO CREATIVES FOR ALBUM ID: #{album.id}") if creatives.blank?
        Rails.logger.error("NO APPLE ID FOR ALBUM ID: #{album.id}") if apple_artist_id.nil?
      end
    end
  end

  def create_or_update_itunes_status_failure
    Rails.logger.error("COULD NOT ASSIGN APPLE ID FOR ALBUM #{album.id}")
    find_or_initialize_album_itunes_status.update_failure
  end

  def create_or_update_itunes_status_success(xml_doc)
    find_or_initialize_album_itunes_status.update_success_xml(xml_doc)
  end

  def find_or_initialize_album_itunes_status
    album.album_itunes_status || AlbumItunesStatus.new(album: album)
  end

  private

  def get_valid_apple_artist_id(artist, result)
    result[:document].xpath("//artist").each do |artist_element|
      apple_id = artist_element["apple_id"] rescue nil

      return nil if apple_id.blank?

      options = { identifier: apple_id, artist_id: artist.id }.with_indifferent_access
      return (Apple::ArtistIdValidator.valid?(options) ? apple_id : nil)
    end
  end
end
