class Apple::ArtistIdExtractionService
  ARTIST_URL_WITH_ID    = /https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/id(\d*)/
  ARTIST_URL_WITHOUT_ID = /https:\/\/(itunes|music).apple.com\/[a-zA-Z]+\/artist\/.*\/(\d*)/

  def self.extract_artist_id(artist_id_url)
    new(artist_id_url).extract_artist_id
  end

  def initialize(artist_id_url)
    @identifier = artist_id_url || ""
  end

  def extract_artist_id
    identifier[ARTIST_URL_WITH_ID, 2] || identifier[ARTIST_URL_WITHOUT_ID, 2]
  end

  private

  attr_reader :identifier
end
