class Apple::ArtistIdCreator
  def self.create_artist_id(person_id, artist_id, is_duplicate)
    new(person_id, artist_id, is_duplicate).tap(&:create_artist_id)
  end

  def initialize(person_id, artist_id, is_duplicate)
    @person_id    = person_id
    @artist_id    = artist_id
    @is_duplicate = is_duplicate
  end

  def create_artist_id
    build_package
    submit_to_store
  end

  private

  def build_package
    @package = Apple::Artist::Package.build_package(@artist_id, @person_id, @is_duplicate)
  end

  def submit_to_store
    raise "artist XML is not valid" unless package_valid?
    raise "failed to send package to iTunes" unless send_package
  ensure
    @package.cleanup
  end

  def package_valid?
    $apple_transporter_client.transport(:validate_package, @package)[:valid]
  end

  def send_package
    $apple_transporter_client.transport(:send_package, @package)[:success]
  end
end
