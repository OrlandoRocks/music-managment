class Apple::Artist::XmlFormatter
  TEMPLATE_PATH = Rails.root.join("app", "services", "apple", "artist", "templates")

  def self.format(formattable, options)
    new(formattable, options).format
  end

  def initialize(formattable, options)
    @formattable = formattable
    @options     = options
    @xml         = Builder::XmlMarkup.new(indent: 2)
  end

  def format
    render "artist", @formattable, { xml: @xml, formatter: self }.merge(@options)
  end

  def render(file_name, object, options = {})
    file = "#{TEMPLATE_PATH}/#{file_name}.builder"
    Tilt.new(file).render(object, options)
  end
end
