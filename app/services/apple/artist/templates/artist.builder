xml.instruct!(:xml, :version=>"1.0", :encoding=>"utf-8")
xml.package(:xmlns=>"http://apple.com/itunes/importer", :version => 'music5.3' ) do |package|
  formatter.render "header", self, xml: package
  package.artist do |artist|
    apple_id_options = {generate: true}
    apple_id_options = apple_id_options.merge({allow_duplicate_name: true}) if is_duplicate
    artist.apple_id(apple_id_options)
    artist.artist_name self.linkable.artist.name
    formatter.render "image", self, xml: package if artwork.try(:asset)
  end
end
