xml.hero_image do |image|
  image.vendor_id self.linkable.artist.name.parameterize.underscore
  image.hero_image_file do |image_file|
    image_file.file_name artwork.asset_file_name
    image_file.checksum artwork.asset.fingerprint, type: "md5"
    image_file.size artwork.asset.size
  end
end
