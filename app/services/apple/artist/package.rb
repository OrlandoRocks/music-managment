class Apple::Artist::Package
  attr_reader :external_service_id, :artwork, :dir

  def self.build_package(artist_id, person_id, is_duplicate)
    new(artist_id, person_id, is_duplicate).tap(&:build_package)
  end

  def initialize(artist_id, person_id, is_duplicate)
    @is_duplicate        = is_duplicate
    @external_service_id = ExternalServiceId.artist_ids_for(person_id, artist_id, "apple").first
    @artwork             = external_service_id.artwork
    @artist_name         = ::Artist.find(artist_id).name.parameterize.underscore
    @work_dir            = ITUNES_TRANSPORTER_CONFIG[:work_dir]
  end

  def build_package
    build_package_dir
    write_metadata
    write_artwork_file if @artwork && @artwork.asset
  end

  def cleanup
    FileUtils.rm_rf(dir)
  end

  private

  def build_package_dir
    package_dir = "#{@work_dir}/#{@artist_name}.itmsp"
    FileUtils.mkdir_p(package_dir)
    @dir = Dir.new(package_dir)
  end

  def write_metadata
    File.open(File.join(dir.path, "metadata.xml"), "w") do |f|
      f.write artist_xml
    end
  end

  def write_artwork_file
    File.open(File.join(dir.path, artwork.asset_file_name.to_s), "wb") do |f|
      f.write(s3_object)
    end
  end

  def artist_xml
    Apple::Artist::XmlFormatter.format(external_service_id, { is_duplicate: @is_duplicate })
  end

  def s3_object
    Apple::Artist::ArtworkDownloader.download_artwork(
      object_key: artwork.asset.path
    )[:data]
  end
end
