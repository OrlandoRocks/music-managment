class Apple::Artist::ArtworkDownloader
  def self.download_artwork(options)
    new(options).download_artwork
  end

  def initialize(options)
    @object_key = options[:object_key]
  end

  def download_artwork
    S3_ARTWORK_CLIENT.client.get_object(bucket_name: SCALED_ARTWORK_BUCKET_NAME, key: @object_key)
  end
end
