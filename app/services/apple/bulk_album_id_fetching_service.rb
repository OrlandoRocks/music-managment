class Apple::BulkAlbumIdFetchingService
  DEFAULT_DAYS_AGO = 1095

  def self.fetch_for_albums(days_ago = nil)
    new(days_ago).tap(&:fetch_for_albums)
  end

  def initialize(days_ago)
    @days_ago = days_ago.try(:to_i) || DEFAULT_DAYS_AGO
  end

  def fetch_for_albums
    scheduled_time = 0
    albums_to_update.find_in_batches do |batch|
      batch.each do |album|
        Apple::AlbumItunesInfoWorker.perform_in(scheduled_time, album.id)
        scheduled_time += 6.seconds # 600 albums an hour is 10 every minute which is 1 every 6 seconds
      end
    end
  end

  private

  def albums_to_update
    time_condition = (Date.today - @days_ago.days).strftime.to_s
    album_ids = DistributorAPI::Album
                .delivered_albums({ store: Store::ITUNES_WW_ID, from: time_condition })[0]
                .http_body["albums"]

    Rails.logger.info("Finding updateable albums for the past #{@days_ago} days")
    Rails.logger.info("Populating Apple Ids for albums")

    ::Album
      .select("distinct albums.id")
      .joins(salepoints_join)
      .joins(distributions_salepoints_join)
      .joins(distributions_join)
      .where(id: album_ids)
      .where(distributions[:updated_at].gteq(time_condition)
        .and(salepoints[:store_id].in(Store::ITUNES_STORE_IDS))
        .and(albums[:known_live_on].eq(nil).or(albums[:apple_identifier].eq(nil)))
        .and(albums[:takedown_at].eq(nil))
        .and(albums[:finalized_at].not_eq(nil)))
  end

  def salepoints_join
    "JOIN salepoints ON salepoints.salepointable_id = albums.id AND salepoints.salepointable_type = 'Album'"
  end

  def distributions_salepoints_join
    "JOIN distributions_salepoints ON salepoints.id = distributions_salepoints.salepoint_id"
  end

  def distributions_join
    "JOIN distributions ON distributions_salepoints.distribution_id = distributions.id"
  end

  def albums
    Album.arel_table
  end

  def distributions
    Distribution.arel_table
  end

  def salepoints
    Salepoint.arel_table
  end
end
