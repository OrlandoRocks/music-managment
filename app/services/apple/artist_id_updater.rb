class Apple::ArtistIdUpdater
  attr_reader :options

  def self.update_artist_id(options)
    new(options).tap(&:update_artist_id)
  end

  def initialize(options)
    @options = options
  end

  def update_artist_id
    if Apple::ArtistIdValidator.valid?(options)
      update_external_service_ids("matched")
      ArtistMapping::AlbumRedistributor.redistribute(options)
    else
      update_external_service_ids("did_not_match")
      ArtistMappingMailer.failed_apple_artist_match_email(options).deliver
    end
  end

  def update_external_service_ids(state)
    ExternalServiceId.artist_ids_for(
      options["person_id"],
      options["artist_id"],
      options["service_name"]
    ).update_all(state: state)
  end
end
