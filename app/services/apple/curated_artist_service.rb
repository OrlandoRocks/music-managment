class Apple::CuratedArtistService
  attr_reader :album, :bundle, :converter, :deliverer, :distributor, :curated_artists

  def self.verify_curated_artist(album_id)
    new(album_id).tap(&:update_curated_creatives)
  end

  def initialize(album_id)
    @album = ::Album.find(album_id)
    @distributor = generate_bundle
    @curated_artists = curated_apple_artists
  end

  def generate_bundle
    converter = DistributionSystem::Itunes::Converter.new
    deliverer = DistributionSystem::Deliverer.new(DELIVERY_CONFIG)
    converted_album = converter.convert(album, itunes_salepoints, :metadata_only)
    DistributionSystem::DistributorFactory.distributor_for(converted_album, deliverer)
  end

  def curated_apple_artists
    write_metadata
    distributor.validate_curated_artists
  end

  def itunes_salepoints
    @itunes_salepoints ||= album.salepoints.where(store_id: Store::ITUNES_STORE_IDS)
  end

  def remove_curated_creatives_flags
    album.creatives.where(curated_artist_flag: true).update(curated_artist_flag: nil)
  end

  def update_curated_creatives
    creatives = album.person.creatives.joins(:artist)
                     .where(artists: { name: curated_artists })

    creatives.update(curated_artist_flag: true)
  end

  def write_metadata
    distributor.bundle.create_bundle
    distributor.bundle.write_metadata
  end
end
