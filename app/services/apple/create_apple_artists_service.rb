class Apple::CreateAppleArtistsService
  attr_reader :album

  def self.for_album(salepointable_id)
    new(salepointable_id).tap(&:create_apple_artists)
  end

  def initialize(salepointable_id)
    @album = ::Album.find(salepointable_id)
  end

  # Update ExternalServiceId for each primary_artist that is a new Apple artist
  def create_apple_artists
    new_apple_artists.each do |creative|
      next if existing_id_for_creative_name(creative, "apple")

      begin
        Rails.logger.info "Creating new artist #{creative.name} on iTunes with the iTMSTransporter"
        result = $apple_transporter_client.create_artist(creative.name, album.language_code)
      rescue => e
        Tunecore::Airbrake.notify(
          Apple::Transporter::TransporterCreateArtistError.new(
            "Error Creating New Artist Apple Id for #{creative.name} on Album #{album.id}"
          ),
          e
        )
      ensure
        handle_result(creative, result)
      end
    end
  end

  private

  def handle_result(creative, result)
    if success?(result)
      update_esi(creative.id, { identifier: result[:apple_id], state: "matched" })
    else
      update_esi(creative.id, state: "error")
      log_errors(creative.name, result[:output]) unless bad_result?(result)
    end
  end

  def new_apple_artists
    album.artists.map(&:creatives).flatten
         .select { |c| c.is_primary? && c&.apple_artist_id == "NEW" }
  end

  def success?(result)
    return false if bad_result?(result)

    result[:success] && result[:apple_id]
  end

  def bad_result?(result)
    !result.is_a?(Hash)
  end

  def update_esi(creative_id, attributes)
    ExternalServiceId.apple_service.by_creatives([creative_id]).first_or_create.update(attributes)
  end

  def log_errors(creative_name, output)
    Rails.logger.error "Failed to create artist on iTunes | " \
                       "artist_name=#{creative_name} album_id=#{album.id}"
    Airbrake.notify(
      Apple::Transporter::TransporterCreateArtistError.new(
        "Itunes create " \
              "artist failed for #{creative_name}: #{output}"
      )
    )
  end

  def existing_id_for_creative_name(creative, service)
    existing_id = creative.existing_external_id_on_acct(service)

    return unless existing_id

    update_esi(creative.id, { identifier: existing_id, state: "matched" })
  end
end
