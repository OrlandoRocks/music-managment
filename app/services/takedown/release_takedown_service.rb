class Takedown::ReleaseTakedownService
  class ReleaseTakedownServiceError < StandardError; end
  attr_reader :album_id, :store_ids, :opts, :batch_name

  def initialize(album_id:, store_ids: [], opts: {}, batch_name:)
    @album_id = album_id
    @store_ids = store_ids
    @opts = opts
    @batch_name = batch_name
  end

  def run
    takedown_release = store_ids.any? ? takedown_salepoints! : takedown_album!
    notify_airbake unless takedown_release
  end

  private

  def takedown_album!
    album = Album.find(album_id)
    album.takedown!(opts)
    album_store_ids = album.stores.pluck(:id)
    return if album_store_ids.empty?

    paused_store_ids = StoreDeliveryConfig.paused_stores(album_store_ids).pluck(:store_id)
    send_sns_notification(paused_store_ids) if paused_store_ids.present?
  end

  def takedown_salepoints!
    salepoints = Salepoint.where(
      salepointable_id: album_id,
      salepointable_type: "Album",
      store_id: store_ids
    )

    taken_down_salepoints = salepoints.select { |sp| sp.takedown!(false) }

    send_sns_notification(store_ids)

    # return false if not all salepoints were takendown
    (store_ids.count == taken_down_salepoints.count)
  end

  def send_sns_notification(store_ids)
    Sns::TakedownNotifier.perform(
      topic_arn: ENV.fetch("SNS_RELEASE_TAKEDOWN_URGENT_TOPIC"),
      album_ids_or_upcs: [album_id],
      store_ids: store_ids,
      delivery_type: "takedown"
    )
  end

  def notify_airbake
    Airbrake.notify(
      ReleaseTakedownServiceError,
      message: "Takedown for album_id: #{album_id} and store_ids: #{store_ids} failed in Batch: #{batch_name}"
    )
  end
end
