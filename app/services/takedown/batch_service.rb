class Takedown::BatchService
  attr_reader :album_ids, :send_email, :opts

  def initialize(album_ids: [], send_email: false, opts: {})
    @album_ids = album_ids
    @send_email = send_email
    @opts = opts
  end

  def run
    return if album_ids.empty?

    album_ids.each do |album_id|
      Takedown::ExpiredRenewalWorker.perform_async(album_id, send_email, opts)
    end
  end
end
