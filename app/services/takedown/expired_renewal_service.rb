# frozen_string_literal: true

class Takedown::ExpiredRenewalService
  class ExpiredRenewalTakedownError < StandardError; end
  attr_reader :album, :send_email, :opts

  def initialize(album_id, send_email, opts = {})
    @album = Album.find(album_id)
    @send_email = send_email
    @opts = opts
  end

  def run
    return if album_person_has_plan? || !renewal_expired?
    return log_error! unless album.takedown!(opts)

    BatchNotifier.takedown(album.person, [album.renewal], album.takedown_at).deliver if send_email
  end

  private

  def log_error!
    Airbrake.notify(ExpiredRenewalTakedownError, "album_id #{album.id} was unable to be takendown")
  end

  def renewal_expired?
    album.renewal && album.renewal.expired?
  end

  def album_person_has_plan?
    album.person.has_plan?
  end
end
