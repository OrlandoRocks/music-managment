class TrendDataAggregator
  TREND_TYPES = [
    "top_downloaded_releases",
    "top_streamed_releases",
    "top_downloaded_songs",
    "top_streamed_songs",
    "top_downloaded_countries",
    "top_streamed_countries"
  ].freeze

  def self.execute(person_id, start_date, end_date)
    start_date  ||= 90.days.ago.to_date.strftime("%Y-%m-%d")
    end_date    ||= Date.yesterday.strftime("%Y-%m-%d")
    aggregator    = new(person_id, start_date, end_date)
    aggregator.aggregate_and_store_top_trends
    aggregator.aggregate_and_store_trends_by_day
  end

  def initialize(person_id, start_date, end_date)
    @user       = Person.find(person_id)
    @start_date = format_date(start_date)
    @end_date   = format_date(end_date)
    @providers  = Provider.all.sort_by(&:name)
  end

  def aggregate_and_store_top_trends
    TREND_TYPES.each do |trend_type|
      records = TrendDataSummary.send(trend_type, user, providers, start_date, end_date)
      results = records.nil? ? records : records.take(50)
      store_in_redis(trend_type, results)
    end
    records = TrendDataSummary.top_us_markets(user, start_date, end_date)
    results = records.nil? ? records : records.take(50)
    store_in_redis("top_us_markets", results)
  end

  def aggregate_and_store_trends_by_day
    @amazon_itunes_trends, @itunes_trends, @amazon_trends = get_trends_by_day_for_providers

    aggregate_streamed_songs_by_day_spotify
    aggregate_downloaded_songs_by_day_itunes_amazon
    aggregate_downloaded_songs_by_day_itunes
    aggregate_downloaded_songs_by_day_amazon
    aggregate_downloaded_releases_by_day_itunes_amazon
    aggregate_downloaded_releases_by_day_itunes
    aggregate_downloaded_releases_by_day_amazon
  end

  private

  attr_reader :user, :start_date, :end_date, :providers, :amazon_itunes_trends, :itunes_trends, :amazon_trends

  def store_in_redis(trend_type, results)
    $trends_redis.set(key(trend_type), results.to_json)
    $trends_redis.expireat(key(trend_type), expiration)
  end

  def key(trend_type)
    "person_#{user.id}:#{trend_type}:#{start_date}:#{end_date}"
  end

  def expiration
    @expiration ||= 7.days.from_now.to_time.to_i
  end

  def format_date(date)
    date.is_a?(String) ? date : date.to_date.to_s
  end

  def get_trends_by_day_for_providers
    amazon, itunes = providers[0..1]

    [
      TrendDataSummary.trend_data_counts_by_day(user, [itunes, amazon], start_date, end_date),
      TrendDataSummary.trend_data_counts_by_day(user, [amazon], start_date, end_date),
      TrendDataSummary.trend_data_counts_by_day(user, [itunes], start_date, end_date)
    ]
  end

  def aggregate_streamed_songs_by_day_spotify
    spotify = providers.last
    results = TrendDataSummary.trend_data_counts_by_day(user, [spotify], start_date, end_date)
    store_in_redis("streamed_songs_by_day_spotify", results)
  end

  def aggregate_downloaded_releases_by_day_itunes_amazon
    results = amazon_itunes_trends.select { |item| item["trans_type_id"] == 2 }
    store_in_redis("downloaded_releases_by_day_itunes_amazon", results)
  end

  def aggregate_downloaded_releases_by_day_itunes
    results = itunes_trends.select { |item| item["trans_type_id"] == 2 }
    store_in_redis("downloaded_releases_by_day_itunes", results)
  end

  def aggregate_downloaded_releases_by_day_amazon
    results = amazon_trends.select { |item| item["trans_type_id"] == 2 }
    store_in_redis("downloaded_releases_by_day_amazon", results)
  end

  def aggregate_downloaded_songs_by_day_itunes_amazon
    results = amazon_itunes_trends.select { |item| item["trans_type_id"] == 1 }
    store_in_redis("downloaded_songs_by_day_itunes_amazon", results)
  end

  def aggregate_downloaded_songs_by_day_itunes
    results = itunes_trends.select { |item| item["trans_type_id"] == 1 }
    store_in_redis("downloaded_songs_by_day_itunes", results)
  end

  def aggregate_downloaded_songs_by_day_amazon
    results = amazon_trends.select { |item| item["trans_type_id"] == 1 }
    store_in_redis("downloaded_songs_by_day_amazon", results)
  end
end
