class AdminDormancyService
  include AfterCommitEverywhere
  attr_reader :error

  def self.remove_person_dormancy(admin, target_person, request)
    instance = new(admin, target_person, request)
    instance.remove_person_dormancy
    instance
  end

  def initialize(admin, target_person, request)
    @admin          = admin
    @target_person  = target_person
    @request        = request
    @error = ""
  end

  def remove_person_dormancy
    begin
      ActiveRecord::Base.transaction(requires_new: true) do
        after_rollback { log_error }
        update_person!
        create_note!
      end
    rescue
    end
  end

  def errors?
    @error.present?
  end

  private

  def update_person!
    @target_person.update!(dormant: false, recent_login: Time.current)
  end

  def log_error
    @error = "Unable to save dormancy removal"
  end

  def dormancy_note
    "Removed Dormant Status for Person_id: #{@target_person.id}. Recent Login Updated."
  end

  def create_note!
    Note.create!(
      related: @target_person,
      note_created_by: @admin,
      ip_address: @request.remote_ip,
      subject: "Dormancy Removed",
      note: dormancy_note
    )
  end
end
