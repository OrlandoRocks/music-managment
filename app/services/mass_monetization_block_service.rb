class MassMonetizationBlockService
  def self.block(params)
    new(params).tap(&:block)
  end

  attr_reader :song_ids, :store_ids, :admin, :ip_address

  def initialize(params = {})
    @song_ids = params[:song_ids] || []
    @store_ids = params[:store_ids] || []
    @admin = params[:admin]
    @ip_address = params[:ip_address]
  end

  def block
    store_ids.each do |store_id|
      song_ids.each do |song_id|
        block_monetization_for_song(song_id, store_id)
      end
    end
  end

  private

  def block_monetization_for_song(song_id, store_id)
    song = Song.find_by(id: song_id)
    store = Store.find_by(id: store_id)

    return unless song && store

    monetization = TrackMonetization.find_by({ song_id: song_id, store_id: store_id })
    if monetization
      monetization.block_and_disqualify(
        {
          actor: "Mass Monetization Block",
          message: "Monetization has been blocked for #{store.name}"
        }
      )
    else
      apply_track_monetization_blocker(song, store)
    end
  end

  def apply_track_monetization_blocker(song, store)
    blocked = TrackMonetizationBlocker.find_or_create_by(song_id: song.id, store_id: store.id)

    create_blocker_note(song, store) if blocked
  end

  def create_blocker_note(song, store)
    return unless admin && ip_address

    Note.create(
      related: song.album,
      note_created_by: admin,
      ip_address: ip_address,
      subject: "Blocked Track Monetization",
      note: "Song isrc: #{song.isrc} for store, #{store.name}, blocked"
    )
  end
end
