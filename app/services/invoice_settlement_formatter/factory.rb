# frozen_string_literal: true

module InvoiceSettlementFormatter
  class Factory
    include Singleton

    class FormatterNotImplementedError < StandardError; end

    # Keep keys Alphabatical
    FORMATTERS = {
      AdyenTransaction: :Adyen,
      BraintreeTransaction: :CreditCard,
      PaymentsOSTransaction: :CreditCard,
      PaypalTransaction: :Paypal,
      PayuTransaction: :Payu,
      PersonTransaction: :Balance
    }.freeze

    def self.for(source)
      source_klass = source.class.name.to_sym
      formatter_klass =
        FORMATTERS
        .fetch(source_klass) do
          raise FormatterNotImplementedError,
                "SettlementFormatter is not implemented for #{source_klass}"
        end

      "InvoiceSettlementFormatter::Formatters::#{formatter_klass}".constantize.new(source)
    end
  end
end
