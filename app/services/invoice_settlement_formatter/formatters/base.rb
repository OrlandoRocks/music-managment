# frozen_string_literal: true

module InvoiceSettlementFormatter
  module Formatters
    class Base
      include ActionView::Helpers::TranslationHelper
      include CustomTranslationHelper

      def initialize(source)
        @source = source
      end

      def invoice_description
        raise NoMethodError, "#{__method__} must be implemented by subclass"
      end

      def credit_note_invoice_description
        raise NoMethodError, "#{__method__} must be implemented by subclass"
      end

      private

      attr_reader :source
    end
  end
end
