# frozen_string_literal: true

module InvoiceSettlementFormatter
  module Formatters
    class Paypal < Base
      def invoice_description
        custom_t("person_notifier.payment_thank_you.total_charged_to_pp")
      end

      def credit_note_invoice_description
        custom_t("credit_note_invoices.settlements.credit_to_paypal")
      end
    end
  end
end
