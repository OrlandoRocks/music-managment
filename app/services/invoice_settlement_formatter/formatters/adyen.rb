# frozen_string_literal: true

module InvoiceSettlementFormatter
  module Formatters
    class Adyen < Base
      def invoice_description
        "#{custom_t('invoices.show.total_charged').humanize}: #{payment_method_name}"
      end

      def credit_note_invoice_description
        "#{custom_t('credit_note_invoices.settlements.credit_to')} #{payment_method_name}"
      end

      def thank_you_mailer_description
        "#{custom_t('invoices.show.total_charged').humanize}: #{payment_method_name}"
      end

      private

      def payment_method_name
        source.payment_method_display_name
      end
    end
  end
end
