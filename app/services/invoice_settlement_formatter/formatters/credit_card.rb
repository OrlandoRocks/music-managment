# a frozen_string_literal: true

module InvoiceSettlementFormatter
  module Formatters
    class CreditCard < Base
      def invoice_description
        custom_t("person_notifier.payment_thank_you.total_charged_to_cc") + "(#{padded_cc_number})"
      end

      def credit_note_invoice_description
        custom_t("credit_note_invoices.settlements.credit_to_credit_card") + "(#{padded_cc_number})"
      end

      private

      def padded_cc_number
        source.stored_credit_card.padded_cc_number
      end
    end
  end
end
