# frozen_string_literal: true

module InvoiceSettlementFormatter
  module Formatters
    class Payu < Base
      def invoice_description
        custom_t("invoices.show.total_charged").humanize + ": PayU " + payment_method
      end

      def credit_note_invoice_description
        custom_t("credit_note_invoices.settlements.credit_to_payu") + " " + payment_method
      end

      private

      def payment_method
        source.payment_method
      end
    end
  end
end
