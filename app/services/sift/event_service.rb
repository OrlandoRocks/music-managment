# frozen_string_literal: true

class Sift::EventService
  extend VatAssesmentAddressBuilder

  attr_reader :body

  CREATE_ACCOUNT = "create_account"
  UPDATE_ACCOUNT = "update_account"
  CREATE_ORDER = "create_order"
  ORDER_STATUS = "order_status"
  TRANSACTION = "transaction"

  def self.create_account(person, cookies, request)
    return unless FeatureFlipper.show_feature?(:sift)

    create_account_options = {
      person: person,
      cookies: cookies,
      request: request
    }

    new(
      Sift::EventBodySerializer.create_account_body(create_account_options)
    ).create_account
  end

  def self.update_account_password(person, cookies, request)
    update_account(
      {
        person: person,
        params: {},
        cookies: cookies,
        request: request,
        update_type: :password
      }
    )
  end

  def self.update_account_email(person, cookies, request)
    update_account(
      {
        person: person,
        params: {},
        cookies: cookies,
        request: request,
        update_type: :email
      }
    )
  end

  def self.update_account_contact_info(person, params, cookies, request)
    params[:billing_address] = complete_address_for(person)
    params[:billing_address]["phone_number"] = person.phone_number

    update_account(
      {
        person: person,
        params: params,
        cookies: cookies,
        request: request,
        update_type: :contact_info
      }
    )
  end

  def self.update_account_payment_methods(person, cookies, request)
    update_account(
      {
        person: person,
        params: {},
        cookies: cookies,
        request: request,
        update_type: :payment_methods
      }
    )
  end

  def self.create_order(finalize_form, cookies, request)
    return unless FeatureFlipper.show_feature?(:sift)
    return if finalize_form.blank?

    create_order_body_json = nil
    transaction_bodies_json = nil

    begin
      new(
        order_status_body: Sift::EventBodySerializer.order_status_body(
          person: finalize_form.person,
          order_status: "$canceled",
          cookies: cookies,
          request: request
        ),
        create_order_body: create_order_body(finalize_form, cookies, request),
        transaction_bodies: pending_transaction_bodies(finalize_form, cookies, request)
      ).create_order
    rescue => e
      Tunecore::Airbrake.notify(
        "Sift::EventService - Synchronous create_order failed to execute properly", {
          person_id: finalize_form&.person&.id,
          error: e.message,
          purchases: finalize_form&.purchases&.pluck(:id),
          create_order_body: create_order_body_json,
          transaction_bodies: transaction_bodies_json
        }
      )
    end
  end

  def self.successful_order(person:, finalize_form:, invoice_id:, cookies:, request:)
    return unless FeatureFlipper.show_feature?(:sift)

    transaction_bodies = successful_transaction_bodies(person, finalize_form, invoice_id, cookies, request)
    order_status_body = Sift::EventBodySerializer.order_status_body(
      person: person,
      order_status: "$approved",
      cookies: cookies,
      request: request
    )

    complete_order(transaction_bodies, order_status_body)
  end

  def self.failed_order(person:, finalize_form:, invoice_id:, cookies:, request:)
    return unless FeatureFlipper.show_feature?(:sift)

    transaction_bodies = failed_transaction_bodies(person, finalize_form, invoice_id, cookies, request)

    complete_order(transaction_bodies)
  end

  # =====================
  # PRIVATE CLASS METHODS
  # =====================

  def self.update_account(update_account_options)
    return unless FeatureFlipper.show_feature?(:sift)

    new(
      Sift::EventBodySerializer.update_account_body(update_account_options)
    ).update_account
  end

  def self.complete_order(transaction_bodies, order_status_body = {})
    new(
      transaction_bodies: transaction_bodies,
      order_status_body: order_status_body
    ).complete_order
  end

  # These private class functions below were required because rubocop gave no other way
  # to disable line number limits on functions
  def self.pending_transaction_bodies(finalize_form, cookies, request)
    return [] unless finalize_form&.purchases&.present?

    transaction_options = {
      finalize_form: finalize_form,
      cookies: cookies,
      request: request,
      person: finalize_form.person,
      invoice_id: nil,
      transaction_status: "$pending"
    }

    Sift::EventBodySerializer.transaction_bodies(transaction_options)
  end

  def self.successful_transaction_bodies(person, finalize_form, invoice_id, cookies, request)
    transaction_options = {
      person: person,
      finalize_form: finalize_form,
      invoice_id: invoice_id,
      transaction_status: "$success",
      cookies: cookies,
      request: request
    }

    Sift::EventBodySerializer.transaction_bodies(transaction_options)
  end

  def self.failed_transaction_bodies(person, finalize_form, invoice_id, cookies, request)
    transaction_options = {
      person: person,
      finalize_form: finalize_form,
      invoice_id: invoice_id,
      transaction_status: "$failure",
      cookies: cookies,
      request: request
    }

    Sift::EventBodySerializer.transaction_bodies(transaction_options)
  end

  def self.create_order_body(finalize_form, cookies, request)
    create_order_options = {
      finalize_form: finalize_form,
      cookies: cookies,
      request: request
    }

    Sift::EventBodySerializer.create_order_body(create_order_options)
  end

  private_class_method :update_account,
                       :complete_order,
                       :pending_transaction_bodies,
                       :successful_transaction_bodies,
                       :failed_transaction_bodies,
                       :create_order_body

  # =========================
  # END PRIVATE CLASS METHODS
  # =========================

  def initialize(body = {})
    @body = body
  end

  def create_account
    send_async(CREATE_ACCOUNT, body)
  end

  def update_account
    send_async(UPDATE_ACCOUNT, body)
  end

  def complete_order
    send_async(TRANSACTION, body[:transaction_bodies]) if body[:transaction_bodies].present?

    return if body[:order_status_body].blank?

    send_async(ORDER_STATUS, body[:order_status_body])
    person_sift_score = PersonSiftScore.find_by(person_id: body.dig(:order_status_body, :$user_id))
    person_sift_score.update(sift_order_id: nil) if person_sift_score.present?
  end

  def create_order
    order_status_body = body[:order_status_body]
    create_order_body = body[:create_order_body]
    transaction_bodies = body[:transaction_bodies]

    should_create_order = should_create_order?

    send_async(ORDER_STATUS, order_status_body) if should_create_order && should_cancel_previous_order?

    Sift::ApiClient.send_request(
      CREATE_ORDER, create_order_body,
      true
    ) if create_order_body.present? && should_create_order

    return if transaction_bodies.blank?

    if should_create_order
      send_async(TRANSACTION, transaction_bodies)
    else
      Sift::ApiClient.send_request(TRANSACTION, transaction_bodies, true)
    end
  end

  private

  def should_create_order?
    return false if body.dig(:create_order_body, :$user_id).blank?

    person_sift_score = PersonSiftScore.find_by(person_id: body.dig(:create_order_body, :$user_id))
    return true if person_sift_score.blank?

    person_sift_score.sift_order_id != body.dig(:create_order_body, :$order_id)
  end

  def should_cancel_previous_order?
    return false if body.dig(:order_status_body, :$user_id).blank?

    person_sift_score = PersonSiftScore.find_by(person_id: body.dig(:order_status_body, :$user_id))
    return false if person_sift_score.blank? || person_sift_score.pending_v1_order?

    person_sift_score.sift_order_id.present?
  end

  def send_async(event_type, body = {})
    Sift::EventWorker.perform_async(event_type, body)
  end
end
