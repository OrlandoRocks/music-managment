# frozen_string_literal: true

class Sift::WebhookService
  attr_reader :person, :note, :releases_active, :options
  attr_accessor :errors

  def initialize(options)
    return unless FeatureFlipper.show_feature?(:sift_webhooks)

    @options = options.with_indifferent_access
    @person = Person.find_by(id: @options[:person_id]) if @options[:person_id].present?
    @note = Note.create(@options[:note_options])
    @releases_active = @options[:releases_active]
  end

  def process
    return unless FeatureFlipper.show_feature?(:sift_decisions, person)
    log_invalid_request and return unless valid_request?

    mark_as_suspicious
  end

  private

  def mark_as_suspicious
    begin
      PeopleFlag.where(person: person, flag: Flag.blocked_from_distribution_flag).first_or_create!
      person.mark_as_suspicious!(note, FlagReason.sift_identified_suspicious.reason)
    rescue => e
      log_activerecord_error(e)
    end
  end

  def valid_request?
    errors = {}

    errors[:person] = "Person is not defined" if person.blank?
    errors[:note] = note.errors.messages if note.errors.present?

    errors.blank?
  end

  def log_invalid_request
    Tunecore::Airbrake.notify(
      "Sift::WebhookService#process - Sift Webhook Decision Failed",
      {
        failed_webhook: options,
        errors: errors
      }
    )

    true
  end

  def log_activerecord_error(error)
    Tunecore::Airbrake.notify(
      "Sift::WebhookService#mark_as_suspicious - Sift Webhook ActiveRecord Error While Marking as Suspicious",
      {
        failed_webhook: options,
        message: error.message
      }
    )
  end
end
