require "./lib/slack_notifier"

class Royalties::DataExportNotificationService
  ROYALTIES = "Royalties"
  CHANNEL = "tech_sip_dev"

  attr_accessor :posting_id

  def self.run(posting_id)
    new(posting_id).run
  end

  def initialize(posting_id)
    @posting_id = posting_id
  end

  def run
    csv_contents = Royalties::DataExportService.new.generate_csv
    upload_to_s3(csv_contents)
    notify_in_slack(notification_message, ROYALTIES, CHANNEL)
  end

  private

  def file_path
    file_name = "posting_royalties_" + posting_id
    "export-data/#{Rails.env}/#{file_name}.csv"
  end

  def s3_bucket
    @s3_bucket ||= Aws::S3::Resource.new.bucket(ENV["SYMPHONY_CSV_BUCKET"])
  end

  def get_report_link
    S3_CLIENT.buckets[ENV["SYMPHONY_CSV_BUCKET"]].objects[file_path].url_for(:read, expires: 7 * 86_400)
  end

  def notification_message
    "Posting royalties report link for #{posting_id} " + get_report_link.to_s
  end

  def upload_to_s3(data)
    object = s3_bucket.object(file_path)
    object.put(body: data)
  end
end
