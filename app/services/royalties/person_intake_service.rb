class Royalties::PersonIntakeService
  BATCH_SIZE = 25
  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id, :last_person_intake_id, :last_encumbrance_detail_id

  def initialize(batch_id)
    @batch_id = batch_id
    @last_person_intake_id = get_last_person_intake_id
    @last_encumbrance_detail_id = get_last_encumbrance_detail_id
  end

  def run
    unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
      ActiveRecord::Base.transaction do
        temp_person_royalties = temp_person_royalties_for(person_ids: person_ids_batch)
        old_person_balances = get_old_person_balances_for(person_ids: person_ids_batch)

        create_person_intakes(temp_person_royalties)

        person_intakes = newly_created_person_intakes(person_ids_batch)
        new_person_balances = create_person_intake_transactions(
          temp_person_royalties,
          old_person_balances,
          person_intakes
        )
        update_royalty(temp_person_royalties, person_intakes)

        create_encumbrance_details(person_ids_batch)
        encumbrance_summary_ids = update_encumbrance_summary(person_ids_batch)
        add_encumbrance_transactions(
          temp_person_royalties,
          encumbrance_summary_ids,
          new_person_balances
        ) if encumbrance_summary_ids.present?

        update_person_balance(temp_person_royalties, old_person_balances)
      end
    end

    update_batch_state
    Royalties::SummarizationWorker.perform_async(batch_id)
  end

  def temp_person_royalties_for(person_ids:)
    TempPersonRoyalty.where(person_id: person_ids)
  end

  def get_old_person_balances_for(person_ids:)
    PersonBalance.select(
      :id,
      :person_id,
      :balance,
      :currency
    ).where(
      person_id: person_ids
    )
  end

  def create_person_intakes(temp_person_royalties)
    person_intakes_to_add =
      temp_person_royalties.map do |temp_person_royalty|
        {
          person_id: temp_person_royalty.person_id,
          amount: temp_person_royalty.total_amount_in_user_currency,
          currency: temp_person_royalty.user_currency,
          created_at: Time.current
        }
      end

    PersonIntake.insert_all(person_intakes_to_add) # rubocop:disable Rails/SkipsModelValidations
  end

  def create_person_intake_transactions(temp_person_royalties, old_person_balances, person_intakes)
    # Insert Person Transactions
    new_person_balances = {}

    person_transactions_to_add =
      temp_person_royalties.map do |temp_person_royalty|
        old_person_balance = old_person_balances.find { |pb|
          pb.person_id == temp_person_royalty.person_id
        }.balance
        new_person_balances[temp_person_royalty.person_id] =
          old_person_balance + temp_person_royalty.total_amount_in_user_currency
        person_intake_id = person_intakes.find { |pi| pi.person_id == temp_person_royalty.person_id }.id

        {
          person_id: temp_person_royalty.person_id,
          debit: [temp_person_royalty.total_amount_in_user_currency, 0].min,
          credit: [temp_person_royalty.total_amount_in_user_currency, 0].max,
          previous_balance: old_person_balance,
          target_id: person_intake_id,
          target_type: "PersonIntake",
          currency: temp_person_royalty.user_currency,
          created_at: Time.current
        }
      end

    PersonTransaction.insert_all(person_transactions_to_add) # rubocop:disable Rails/SkipsModelValidations

    new_person_balances
  end

  def create_encumbrance_details(person_ids)
    temp_encumbrance_details = TempEncumbranceDetail.where(person_id: person_ids)

    encumbrance_details_to_add =
      temp_encumbrance_details.map do |temp_encumbrance_detail|
        {
          encumbrance_summary_id: temp_encumbrance_detail.encumbrance_summary_id,
          transaction_amount: temp_encumbrance_detail.transaction_amount,
          transaction_date: Date.today,
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    return if encumbrance_details_to_add.blank?

    EncumbranceDetail.insert_all(encumbrance_details_to_add) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_encumbrance_summary(person_ids)
    encumbrance_summary_details = encumbrance_summary_details(person_ids)

    encumbrance_summary_ids = encumbrance_summary_details.pluck(:encumbrance_summary_id)

    encumbrance_summaries = EncumbranceSummary.where(id: encumbrance_summary_ids)

    encumbrance_summaries_to_update =
      encumbrance_summaries.map do |encumbrance_summary|
        transaction_amount = encumbrance_summary_details.find { |esd|
                               esd[:encumbrance_summary_id] == encumbrance_summary.id
                             } [:transaction_amount]

        encumbrance_summary.attributes.except(:outstanding_amount).merge(
          {
            outstanding_amount: encumbrance_summary.outstanding_amount + transaction_amount,
            updated_at: Time.current
          }
        )
      end

    return if encumbrance_summaries_to_update.blank?

    EncumbranceSummary.upsert_all(encumbrance_summaries_to_update) # rubocop:disable Rails/SkipsModelValidations

    encumbrance_summary_ids
  end

  def add_encumbrance_transactions(temp_person_royalties, encumbrance_summary_ids, new_person_balances)
    encumbrance_details = newly_created_encumbrance_details(encumbrance_summary_ids)

    encumbrance_transactions_to_add =
      encumbrance_details.each_with_object([]) do |encumbrance_detail, object|
        user_currency = temp_person_royalties.find { |tpr|
          tpr.person_id == encumbrance_detail.person_id
        }.user_currency

        object << {
          person_id: encumbrance_detail.person_id,
          debit: -1 * encumbrance_detail.transaction_amount,
          credit: 0,
          currency: user_currency,
          previous_balance: new_person_balances[encumbrance_detail.person_id],
          target_id: encumbrance_detail.id,
          target_type: "EncumbranceDetail",
          comment: "TuneCore Direct Advance Recoupment",
          created_at: Time.current,
        }

        new_person_balances[encumbrance_detail.person_id] += encumbrance_detail.transaction_amount
        object
      end

    return if encumbrance_transactions_to_add.blank?

    PersonTransaction.insert_all(encumbrance_transactions_to_add) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_person_balance(temp_person_royalties, old_person_balances)
    # Update the person balance
    person_balance_to_update =
      temp_person_royalties.map do |temp_person_royalty|
        old_person_balance =
          old_person_balances.find { |pb|
            pb.person_id == temp_person_royalty.person_id
          }

        new_person_balance = old_person_balance.balance +
                             temp_person_royalty.total_amount_in_user_currency +
                             temp_person_royalty.encumbrance_amount_in_user_currency

        old_person_balance.attributes.except("balance").merge(
          {
            balance: new_person_balance,
            updated_at: Time.current
          }
        )
      end
    PersonBalance.upsert_all(person_balance_to_update) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_royalty(temp_person_royalties, person_intakes)
    person_royalties_to_update =
      temp_person_royalties.map do |temp_person_royalty|
        temp_person_royalty.person_intake_id =
          person_intakes.find { |pi|
            pi.person_id == temp_person_royalty.person_id
          }.id

        temp_person_royalty.attributes
      end
    TempPersonRoyalty.upsert_all(person_royalties_to_update) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::PERSON_INTAKE_COMPLETED)
  end

  def newly_created_person_intakes(person_ids)
    PersonIntake.select(:person_id, :id).where(
      person_id: person_ids
    ).where(
      "id > ?", last_person_intake_id
    )
  end

  def newly_created_encumbrance_details(encumbrance_summary_ids)
    EncumbranceDetail.joins(:encumbrance_summary).select(
      :person_id, :id, :transaction_amount
    ).where(
      encumbrance_summary_id: encumbrance_summary_ids
    ).where(
      "encumbrance_details.id > ?", last_encumbrance_detail_id
    )
  end

  def get_last_person_intake_id
    PersonIntake.last&.id || 0
  end

  def get_last_encumbrance_detail_id
    EncumbranceDetail.last&.id || 0
  end

  def unprocessed_person_ids
    TempPersonRoyalty.where(
      person_id: person_ids,
      person_intake_id: nil
    ).where.not(total_amount_in_user_currency: nil).pluck(:person_id)
  end

  def person_ids
    @person_ids ||= Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def encumbrance_summary_details(person_ids)
    TempEncumbranceDetail.select(
      "
        sum(transaction_amount) as transaction_amount,
        person_id as person_id,
        encumbrance_summary_id as encumbrance_summary_id
      "
    ).where(
      person_id: person_ids
    ).group(
      :encumbrance_summary_id
    )
  end
end
