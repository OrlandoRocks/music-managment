class Royalties::CopyYoutubeRecordsService
  def self.run
    new.run
  end

  def run
    batch_ids.each do |batch_id|
      temp_person_royalties_by_currency = temp_person_royalties_by_currency_for(batch_id)

      next if temp_person_royalties_by_currency.blank?

      temp_person_royalties_by_currency.map do |currency, temp_person_royalties|
        fx_rate = fx_rate_for(currency)
        person_ids = temp_person_royalties.pluck(:person_id)

        ActiveRecord::Base.transaction do
          insert_you_tube_royalty_records(person_ids, fx_rate)

          TempPersonRoyalty.where(
            person_id: person_ids
          ).update_all( # rubocop:disable Rails/SkipsModelValidations
            youtube_records_copied: 1
          )
        end
      end

      update_batch_state(batch_id)
    end

    update_posting_state
    copy_sales_records
  end

  def temp_person_royalties_by_currency_for(batch_id)
    TempPersonRoyalty.select(
      :person_id,
      :user_currency,
      :you_tube_royalty_intake_id
    ).where(
      person_id: get_person_ids_for(batch_id),
      youtube_records_copied: 0
    ).group_by(&:user_currency)
  end

  def insert_you_tube_royalty_records(person_ids, fx_rate)
    exec_sql(
      "
        INSERT INTO you_tube_royalty_records
        (
          person_id, sip_you_tube_royalty_record_id, song_id, total_views,
          you_tube_policy_type, gross_revenue, exchange_rate,
          tunecore_commision, net_revenue, net_revenue_currency,
          sales_period_start, you_tube_video_id, song_name,
          album_name, label_name, artist_name,
          updated_at, created_at, you_tube_royalty_intake_id,
          posting_id
        )
        SELECT you_tube_royalty_records_new.person_id,
          sip_you_tube_royalty_record_id, song_id, total_views,
          you_tube_policy_type, gross_revenue,
          IFNULL(exchange_rate, 1) * #{fx_rate},
          tunecore_commision, net_revenue * #{fx_rate},
          net_revenue_currency, sales_period_start, you_tube_video_id, song_name,
          album_name, label_name, artist_name,
          you_tube_royalty_records_new.updated_at, you_tube_royalty_records_new.created_at,
          temp_person_royalties.you_tube_royalty_intake_id,
          posting_id
        FROM you_tube_royalty_records_new
        INNER JOIN temp_person_royalties ON you_tube_royalty_records_new.person_id = temp_person_royalties.person_id
        WHERE you_tube_royalty_records_new.person_id in (#{person_ids.join(',')})
      "
    )
  end

  def batch_ids
    Royalties::BatchService.get_all_batch_ids
  end

  def get_person_ids_for(batch_id)
    Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def copy_sales_records
    Royalties::CopySalesRecordsWorker.perform_async
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::YOUTUBE_RECORDS_COPIED)
  end

  def update_batch_state(batch_id)
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::YOUTUBE_RECORDS_COPIED)
  end

  def exec_sql(sql)
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
