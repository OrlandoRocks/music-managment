class Royalties::CopySalesRecordMastersService
  def self.run
    new.run
  end

  def run
    copy_sales_record_masters_new_to_sales_record_masters

    schedule_batches_for_processing
  end

  def copy_sales_record_masters_new_to_sales_record_masters
    return unless Royalties::Posting.get_state == Royalties::Posting::FINANCE_APPROVED

    records_to_copy =
      SalesRecordMasterNew.all.map do |sales_record_master_new|
        sales_record_master_new.created_at = Time.current
        sales_record_master_new.exchange_rate = get_exchange_rate(sales_record_master_new)
        sales_record_master_new.attributes
      end

    SalesRecordMaster.insert_all( # rubocop:disable Rails/SkipsModelValidations
      records_to_copy
    ) if records_to_copy.present?

    update_posting_state
  end

  def get_exchange_rate(sales_record_master)
    usd_to_user_currency_fx_rate = Royalties::Posting.get_fx_rate_for(sales_record_master.amount_currency)
    store_currency_to_usd_fx_rate = sales_record_master.exchange_rate || 1.0

    usd_to_user_currency_fx_rate * store_currency_to_usd_fx_rate
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::SALES_RECORD_MASTERS_COPIED)
  end

  def schedule_batches_for_processing
    Royalties::BatchService.get_all_batch_ids.each do |batch_id|
      Royalties::CurrencyConversionWorker.perform_async(batch_id)
    end
  end
end
