class Royalties::PostFinanceApprovalService
  attr_accessor :rates, :posting_id

  def initialize(rates)
    @rates = rates
    @posting_id = Royalties::Posting.get_id
  end

  def valid?
    return false unless Royalties::Posting.get_state == Royalties::Posting::FINANCE_APPROVAL_REQUESTED

    currencies_provided = rates.map { |rate| rate["currency"] }
    all_currencies = PostingRoyalty.where(posting_id: posting_id).pluck(:currency).uniq
    (all_currencies - currencies_provided).empty?
  end

  def current_posting_royalties
    posting_royalties = PostingRoyalty.where(posting_id: posting_id)
    currencies = CountryWebsite.distinct.pluck(:currency)
    return posting_royalties, currencies
  end

  def process
    add_rates_to_posting

    rates.map do |rate|
      update_posting_royalties(rate)
    end

    update_posting_state
    Royalties::CopySalesRecordMastersWorker.perform_async
  end

  def apply_fx_rates(submitting_user)
    add_rates_to_posting
    rates.each do |rate|
      update_posting_royalties(rate, submitting_user)
    end
  end

  private

  def add_rates_to_posting
    hash =
      rates.map do |rate|
        {
          currency: rate["currency"],
          fx_rate: final_fx_rate(rate)
        }
      end
    Royalties::Posting.add_currency_hash(hash)
  end

  def update_posting_royalties(rate, submitting_user = nil)
    royalties = PostingRoyalty.where(currency: rate["currency"], posting_id: posting_id)
    return if royalties.empty?

    final_fx_rate = final_fx_rate(rate)
    royalties.map do |royalty|
      royalty.update(
        {
          base_fx_rate: Float(rate["fx_rate"]),
          fx_coverage_rate: coverage_rate(rate),
          fx_rate: final_fx_rate,
          converted_amount: converted_amount(royalty, final_fx_rate),
          state: submitting_user ? PostingRoyalty::SUBMITTED : PostingRoyalty::APPROVED,
          approved_by: submitting_user ? nil : PostingRoyalty::RAKE_USER,
          approved_on: submitting_user ? nil : Time.current,
          fx_rate_submitted_by: submitting_user ? submitting_user.name : nil,
          fx_rate_submitted_at: submitting_user ? Time.current : nil,
          updated_at: Time.current
        }
      )
    end
  end

  def final_fx_rate(rate)
    Float(rate["fx_rate"]) * (1 - coverage_rate(rate) / 100)
  end

  def converted_amount(royalty, fx_rate)
    total_amount(royalty) * fx_rate
  end

  def total_amount(royalty)
    Float(royalty.total_amount_in_usd || 0) +
      Float(royalty.total_you_tube_amount_in_usd || 0) +
      Float(royalty.encumbrance_amount_in_usd || 0) +
      Float(royalty.split_adjustments_in_usd || 0)
  end

  def coverage_rate(rate)
    rate["coverage_rate"].nil? ? 0 : Float(rate["coverage_rate"])
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVED)
  end
end
