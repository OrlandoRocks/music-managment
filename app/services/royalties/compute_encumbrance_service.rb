class Royalties::ComputeEncumbranceService
  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id

  def initialize(batch_id)
    @batch_id = batch_id
  end

  def run
    unless unprocessed_person_ids.count.zero?
      people_with_encumbrances = people_with_encumbrances(unprocessed_person_ids)
      people_without_encumbrances = unprocessed_person_ids - people_with_encumbrances

      update_person_royalty(people_without_encumbrances, 0, 0) unless people_without_encumbrances.count.zero?

      people_with_encumbrances.map do |person_id|
        ActiveRecord::Base.transaction do
          earnings = earnings(person_id)
          if earnings.zero?
            update_person_royalty(Array(person_id), 0, 0)
          else
            amount = compute(person_id)
            update_person_royalty(Array(person_id), -1 * amount, (amount / earnings) * 100)
          end
        end
      end
    end

    # Update batch state to Encumbrance Computed
    update_batch_state

    Royalties::Splits::ComputeSplitsWorker.perform_async(batch_id)
  end

  private

  def people_with_encumbrances(person_ids)
    EncumbranceSummary.select(
      :person_id
    ).where(
      person_id: person_ids
    ).where(
      "outstanding_amount > 0"
    ).pluck(:person_id).uniq
  end

  def unprocessed_person_ids
    @unprocessed_person_ids ||= TempPersonRoyalty.where(
      person_id: person_ids,
      encumbrance_amount_in_usd: nil,
      encumbrance_percentage: nil
    ).where.not(total_amount_in_usd: nil).pluck(:person_id)
  end

  def encumbrances(person_id)
    EncumbranceSummary.where(
      person_id: person_id
    ).where(
      "outstanding_amount > 0"
    )
  end

  def update_person_royalty(person_ids, amount, percentage)
    TempPersonRoyalty.where(
      person_id: person_ids
    ).update_all( # rubocop:disable Rails/SkipsModelValidations
      encumbrance_amount_in_usd: amount,
      encumbrance_percentage: percentage
    )
  end

  def compute(person_id)
    remaining_drawdown = nil
    amount_earned = earnings(person_id)
    total_amount = 0

    encumbrances(person_id).map do |encumbrance|
      max_drawdown = (encumbrance_max_drawdown(encumbrance.reference_source) * amount_earned).truncate(2)
      remaining_drawdown = max_drawdown if remaining_drawdown.nil?

      transaction_amount =
        if encumbrance.outstanding_amount >= remaining_drawdown
          remaining_drawdown
        else
          encumbrance.outstanding_amount
        end

      create_encumbrance_detail(encumbrance, transaction_amount, person_id)

      total_amount += transaction_amount
      remaining_drawdown -= transaction_amount

      break if remaining_drawdown.zero?
    end
    total_amount
  end

  def earnings(person_id)
    TempPersonRoyalty.find_by(person_id: person_id).total_amount_in_usd
  end

  def encumbrance_max_drawdown(reference_source)
    encumbrance_option = EncumbranceOption.where(
      option_name: "WITHHOLDING_MAX_PERCENTAGE",
      reference_source: reference_source
    )

    return 0 if encumbrance_option.blank?

    Integer(encumbrance_option.first.option_value, 10) / 100
  end

  def create_encumbrance_detail(encumbrance, transaction_amount, person_id)
    TempEncumbranceDetail.create!(
      encumbrance_summary_id: encumbrance.id,
      transaction_amount: -1 * transaction_amount,
      person_id: person_id
    )
  end

  def person_ids
    @person_ids ||= Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::ENCUMBRANCE_COMPUTED)
  end
end
