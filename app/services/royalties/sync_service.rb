class Royalties::SyncService
  def self.run(batch_state, old_posting_state, new_posting_state, success_worker)
    new(batch_state, old_posting_state, new_posting_state, success_worker).run
  end

  attr_accessor :batch_state, :old_posting_state, :new_posting_state, :success_worker

  def initialize(batch_state, old_posting_state, new_posting_state, success_worker)
    @batch_state = batch_state
    @old_posting_state = old_posting_state
    @new_posting_state = new_posting_state
    @success_worker = success_worker
  end

  def run
    return unless current_posting_state == old_posting_state

    return unless Array(batch_state) == uniq_batch_states

    Royalties::Posting.update_state(new_posting_state)
    success_worker.constantize.perform_async
  end

  def uniq_batch_states
    Royalties::BatchService.get_uniq_batch_states
  end

  def current_posting_state
    Royalties::Posting.get_state
  end
end
