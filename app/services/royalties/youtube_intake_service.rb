class Royalties::YoutubeIntakeService
  BATCH_SIZE = 25
  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id, :last_intake_id, :last_encumbrance_detail_id

  def initialize(batch_id)
    @batch_id = batch_id
    @last_intake_id = get_last_intake_id
  end

  def run
    unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
      ActiveRecord::Base.transaction do
        temp_person_royalties = temp_person_royalties_for(person_ids: person_ids_batch)
        old_person_balances = get_old_person_balances_for(person_ids: person_ids_batch)

        create_intakes(temp_person_royalties)

        intakes = newly_created_intakes(person_ids_batch)
        create_intake_transactions(
          temp_person_royalties,
          old_person_balances,
          intakes
        )
        update_royalty(temp_person_royalties, intakes)

        update_person_balance(temp_person_royalties, old_person_balances)
      end
    end

    update_batch_state
    Royalties::PersonIntakeWorker.perform_async(batch_id)
  end

  def temp_person_royalties_for(person_ids:)
    TempPersonRoyalty.where(person_id: person_ids)
  end

  def get_old_person_balances_for(person_ids:)
    PersonBalance.select(
      :id,
      :person_id,
      :balance,
      :currency
    ).where(
      person_id: person_ids
    )
  end

  def create_intakes(temp_person_royalties)
    intakes_to_add =
      temp_person_royalties.map do |temp_person_royalty|
        {
          person_id: temp_person_royalty.person_id,
          amount: temp_person_royalty.you_tube_amount_in_user_currency,
          currency: temp_person_royalty.user_currency,
          created_at: Time.current
        }
      end

    YouTubeRoyaltyIntake.insert_all(intakes_to_add) # rubocop:disable Rails/SkipsModelValidations
  end

  def create_intake_transactions(temp_person_royalties, old_person_balances, intakes)
    # Insert Person Transactions
    new_person_balances = {}

    person_transactions_to_add =
      temp_person_royalties.map do |temp_person_royalty|
        old_person_balance = old_person_balances.find { |pb|
          pb.person_id == temp_person_royalty.person_id
        }.balance
        new_person_balances[temp_person_royalty.person_id] =
          old_person_balance + temp_person_royalty.you_tube_amount_in_user_currency
        intake_id = intakes.find { |i| i.person_id == temp_person_royalty.person_id }.id

        {
          person_id: temp_person_royalty.person_id,
          debit: 0,
          credit: [temp_person_royalty.you_tube_amount_in_user_currency, 0].max,
          previous_balance: old_person_balance,
          target_id: intake_id,
          target_type: "YouTubeRoyaltyIntake",
          currency: temp_person_royalty.user_currency,
          created_at: Time.current
        }
      end

    PersonTransaction.insert_all(person_transactions_to_add) # rubocop:disable Rails/SkipsModelValidations

    new_person_balances
  end

  def update_person_balance(temp_person_royalties, old_person_balances)
    # Update the person balance
    person_balance_to_update =
      temp_person_royalties.map do |temp_person_royalty|
        old_person_balance =
          old_person_balances.find { |pb|
            pb.person_id == temp_person_royalty.person_id
          }

        old_person_balance.attributes.except("balance").merge(
          {
            balance: old_person_balance.balance + temp_person_royalty.you_tube_amount_in_user_currency,
            updated_at: Time.current
          }
        )
      end
    PersonBalance.upsert_all(person_balance_to_update) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_royalty(temp_person_royalties, intakes)
    person_royalties_to_update =
      temp_person_royalties.map do |temp_person_royalty|
        temp_person_royalty.you_tube_royalty_intake_id =
          intakes.find { |i|
            i.person_id == temp_person_royalty.person_id
          }.id

        temp_person_royalty.attributes
      end
    TempPersonRoyalty.upsert_all(person_royalties_to_update) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::YOUTUBE_INTAKE_COMPLETED)
  end

  def newly_created_intakes(person_ids)
    YouTubeRoyaltyIntake.select(:person_id, :id).where(
      person_id: person_ids
    ).where(
      "id > ?", last_intake_id
    )
  end

  def get_last_intake_id
    YouTubeRoyaltyIntake.last&.id
  end

  def unprocessed_person_ids
    TempPersonRoyalty.where(
      person_id: person_ids,
      you_tube_royalty_intake_id: nil
    ).where("you_tube_amount_in_user_currency > 0").pluck(:person_id)
  end

  def person_ids
    @person_ids ||= Royalties::BatchService.get_batch_person_ids(batch_id)
  end
end
