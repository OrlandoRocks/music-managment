class Royalties::PopulatePersonIntakeSalesRecordMastersService
  def self.run
    new.run
  end

  def run
    batch_ids.each do |batch_id|
      temp_person_royalties = temp_person_royalties_for(batch_id: batch_id)

      records_to_insert = []

      temp_person_royalties.each do |temp_person_royalty|
        next if temp_person_royalty.sales_record_master_ids.blank?

        temp_person_royalty.sales_record_master_ids.split(",").each do |sales_record_master|
          records_to_insert << {
            sales_record_master_id: sales_record_master,
            person_intake_id: temp_person_royalty.person_intake_id
          }
        end
      end

      PersonIntakeSalesRecordMaster.insert_all( # rubocop:disable Rails/SkipsModelValidations
        records_to_insert
      ) if records_to_insert.present?

      update_batch_state(batch_id)
    end

    update_posting_state

    update_sales_record_masters
  end

  def temp_person_royalties_for(batch_id:)
    TempPersonRoyalty.select(
      :sales_record_master_ids,
      :person_intake_id,
    ).where(
      person_id: get_person_ids_for(batch_id)
    ).where.not(
      person_intake_id: nil
    )
  end

  def batch_ids
    Royalties::BatchService.get_all_batch_ids
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::PERSON_INTAKE_SALES_RECORD_MASTERS_POPULATED)
  end

  def update_batch_state(batch_id)
    Royalties::BatchService.update_batch_state(
      batch_id,
      Royalties::BatchService::PERSON_INTAKE_SALES_RECORD_MASTERS_POPULATED
    )
  end

  def get_person_ids_for(batch_id)
    Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def update_sales_record_masters
    Royalties::UpdateSalesRecordMastersWorker.perform_async
  end
end
