# frozen_string_literal: true

class Royalties::BatchService
  BATCH_PREFIX = "ROYALTIES_BATCH"
  BATCH_CREATED = "batch_created"
  PERSON_ROYALTIES_COMPUTED = "person_royalties_computed"
  ENCUMBRANCE_COMPUTED = "encumbrance_computed"
  SPLITS_COMPUTED = "splits_computed"
  CURRENCY_CONVERSION_COMPLETED = "currency_conversion_completed"
  SALES_RECORD_CURRENCY_CONVERSION_COMPLETED = "sales_record_currency_conversion_completed"
  PERSON_INTAKE_COMPLETED = "person_intake_completed"
  YOUTUBE_INTAKE_COMPLETED = "youtube_intake_completed"
  SUMMARIZED = "royalties_summarized"
  SALES_RECORDS_COPIED = "sales_records_copied"
  PERSON_INTAKE_SALES_RECORD_MASTERS_POPULATED = "person_intake_sales_record_masters_populated"
  YOUTUBE_RECORDS_COPIED = "youtube_records_copied"

  def self.add_to_batch(batch_id, person_ids)
    Rails.cache.write(
      "#{BATCH_PREFIX}_#{batch_id}", {
        person_ids: person_ids,
        state: BATCH_CREATED
      }.to_json
    )
  end

  def self.update_batch_state(batch_id, state)
    batch_details = get_batch_details(batch_id)
    batch_details["state"] = state
    Rails.cache.write(
      "#{BATCH_PREFIX}_#{batch_id}", batch_details.to_json
    )
  end

  def self.get_batch_person_ids(batch_id)
    batch_details = get_batch_details(batch_id)
    batch_details["person_ids"] if batch_details.present?
  end

  def self.get_batch_state(batch_id)
    batch_details = get_batch_details(batch_id)
    batch_details["state"] if batch_details.present?
  end

  def self.delete_existing_batches
    Rails.cache.delete_matched("#{BATCH_PREFIX}_*")
  end

  def self.get_uniq_batch_states
    batch_keys
      .map { |key| get_batch_state(key.split("#{BATCH_PREFIX}_").last) }
      .uniq
  end

  def self.get_all_batch_ids
    all_batch_details = {}

    batch_keys
      .map { |key| key.split("#{BATCH_PREFIX}_").last }
      .map { |batch_id| all_batch_details[batch_id] = get_batch_person_ids(batch_id).count }

    all_batch_details.sort_by { |_key, value| value }.to_h.keys
  end

  def self.get_batch_details(batch_id)
    batch_details = Rails.cache.fetch("#{BATCH_PREFIX}_#{batch_id}")
    JSON.parse(batch_details) if batch_details.present?
  end

  def self.batch_keys
    cache = Rails.cache.instance_variable_get(:@data)
    cache.keys.select { |key| key.starts_with?("#{BATCH_PREFIX}_") }
  end
end
