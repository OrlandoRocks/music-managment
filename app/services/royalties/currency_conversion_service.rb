class Royalties::CurrencyConversionService
  BATCH_SIZE = 1000

  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id

  def initialize(batch_id)
    @batch_id = batch_id
  end

  def run
    unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
      temp_person_royalties = TempPersonRoyalty.where(person_id: person_ids_batch)

      records_to_update =
        temp_person_royalties.map { |temp_person_royalty|
          modified_temp_person_royalty(
            temp_person_royalty,
            fx_rate_for(temp_person_royalty.user_currency)
          ) if temp_person_royalty.present?
        }

      next if records_to_update.blank?

      # Bulk update temp person royalties table
      TempPersonRoyalty.upsert_all(records_to_update) # rubocop:disable Rails/SkipsModelValidations
    end

    update_batch_state

    Royalties::YoutubeIntakeWorker.perform_async(batch_id)
  end

  def unprocessed_person_ids
    TempPersonRoyalty.where(
      fx_rate: nil,
      total_amount_in_user_currency: nil,
      encumbrance_amount_in_user_currency: nil,
      you_tube_amount_in_user_currency: nil,
      person_id: person_ids
    ).pluck(:person_id)
  end

  def person_ids
    Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::CURRENCY_CONVERSION_COMPLETED)
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end

  def modified_temp_person_royalty(temp_person_royalty, fx_rate)
    temp_person_royalty.attributes.merge(
      {
        total_amount_in_user_currency: get_converted_amount(
          temp_person_royalty.total_amount_in_usd, fx_rate
        ),
        encumbrance_amount_in_user_currency: get_converted_amount(
          temp_person_royalty.encumbrance_amount_in_usd, fx_rate
        ),
        you_tube_amount_in_user_currency: get_converted_amount(
          temp_person_royalty.you_tube_amount_in_usd, fx_rate
        ),
        fx_rate: fx_rate,
        updated_at: Time.current
      }
    )
  end

  def get_converted_amount(amount_in_usd, fx_rate)
    amount_in_usd.nil? ? nil : amount_in_usd * fx_rate
  end
end
