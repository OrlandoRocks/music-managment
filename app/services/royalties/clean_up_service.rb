class Royalties::CleanUpService
  def self.run
    new.run
  end

  def run
    return if Royalties::Posting.get_state != Royalties::Posting::COMPLETED

    start_tax_processing
    clean_www_tables
    delete_entries_from_temp_tables
    delete_entries_from_cache
  end

  private

  def start_tax_processing
    TaxWithholdings::SipBatchWorker.perform_async if FeatureFlipper.show_feature?(:us_tax_withholding)
  end

  def delete_entries_from_cache
    Royalties::Posting.remove
    Royalties::BatchService.delete_existing_batches
  end

  def delete_entries_from_temp_tables
    TempPersonRoyalty.delete_all
    TempEncumbranceDetail.delete_all
    TempRoyaltySplitDetail.delete_all
  end

  def clean_www_tables
    return unless Royalties::Posting.get_source == Royalties::Posting::SOURCE_SYMPHONY

    [
      "sales_records_new",
      "sales_record_masters_new",
      "you_tube_royalty_records_new"
    ].each do |table_name|
      ActiveRecord::Base.connection.execute("TRUNCATE #{table_name}")
    end
  end
end
