class Royalties::BatchCreationService
  WORKER_THRESHOLD = 5000
  PERSON_THRESHOLD = 5000

  attr_accessor :posting_id

  def self.run
    new.run
  end

  def initialize
    @posting_id = Royalties::Posting.get_id
  end

  def run
    if posting_state == Royalties::Posting::STARTED
      create_batches
      update_posting_state
    end

    schedule_batch_processing
  end

  private

  def create_batches
    results = get_records_count_per_person
    return if results.nil?

    threshold = WORKER_THRESHOLD
    threshold = results.first[1] if results.first[1] > WORKER_THRESHOLD

    delete_existing_batches
    batch = new_batch
    results.each do |result|
      if batch[:size] + result[1] > threshold || batch[:list].count >= PERSON_THRESHOLD
        save_batch_in_cache(batch)
        batch = new_batch
      end

      batch[:size] = batch[:size] + result[1]
      batch[:list].append(result[0])
    end

    save_batch_in_cache(batch)
  end

  def get_records_count_per_person
    sales_records = SalesRecordNew.fetch_count_of_data_per_person.to_h
    you_tube_records = YouTubeRoyaltyRecordNew.fetch_count_of_data_per_person.to_h

    results =
      sales_records.merge(you_tube_records) do |_, sales_records_count, you_tube_records_count|
        sales_records_count + you_tube_records_count
      end

    results.sort_by { |_k, v| -v }
  end

  def schedule_batch_processing
    Royalties::BatchService.get_all_batch_ids.each do |batch_id|
      Royalties::ComputePersonRoyaltiesWorker.perform_async(batch_id)
    end
  end

  def generate_uuid
    SecureRandom.uuid
  end

  def delete_existing_batches
    Royalties::BatchService.delete_existing_batches

    # Delete all temp person royalty records
    TempPersonRoyalty.delete_all
  end

  def save_batch_in_cache(batch)
    Royalties::BatchService.add_to_batch(batch[:id], batch[:list])

    add_to_temp_person_royalties(batch[:list])
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::BATCHES_CREATED)
  end

  def posting_state
    Royalties::Posting.get_state
  end

  def batch_state(batch_id)
    Royalties::BatchService.get_batch_state(batch_id)
  end

  def add_to_temp_person_royalties(person_ids)
    persons = Person.includes(country_website: :foreign_exchange_pegged_rates).where(id: person_ids)

    persons.find_in_batches do |person_batches|
      temp_person_royalties =
        person_batches.map do |person|
          {
            person_id: person.id,
            user_currency: person.user_currency == "INR" ? "USD" : person.user_currency,
            corporate_entity_id: person.corporate_entity_id,
            created_at: Time.current,
            updated_at: Time.current
          }
        end
      TempPersonRoyalty.insert_all(temp_person_royalties) # rubocop:disable Rails/SkipsModelValidations
    end
  end

  def new_batch
    { id: generate_uuid, size: 0, list: [] }
  end
end
