class Royalties::ComputePersonRoyaltiesService
  BATCH_SIZE = 1000

  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id

  def initialize(batch_id)
    @batch_id = batch_id
  end

  def run
    unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
      sales_records = sales_records_for(person_ids: person_ids_batch)
      you_tube_records = you_tube_records_for(person_ids: person_ids_batch)
      temp_person_royalties = TempPersonRoyalty.where(person_id: person_ids_batch)

      records_to_update =
        temp_person_royalties.map do |royalty|
          you_tube_record = you_tube_records.find { |record| record["person_id"] == royalty.person_id }
          sales_record = sales_records.find { |record| record["person_id"] == royalty.person_id }

          royalty.attributes.merge(
            build_royalty_object(royalty, sales_record, you_tube_record)
          ) if royalty.present?
        end

      TempPersonRoyalty.upsert_all(records_to_update) # rubocop:disable Rails/SkipsModelValidations
    end

    # Update Batch State to person royalties computed
    update_batch_state

    # Compute Encumbrance worker for this batch
    Royalties::ComputeEncumbranceWorker.perform_async(batch_id)
  end

  def unprocessed_person_ids
    TempPersonRoyalty.where(
      person_id: person_ids,
      total_amount_in_usd: nil,
      you_tube_amount_in_usd: nil,
      sales_record_master_ids: nil
    ).pluck(:person_id)
  end

  def sales_records_for(person_ids:)
    SalesRecordNew.select(
      "
        person_id,
        SUM(amount) AS total_amount_in_usd,
        group_concat(distinct sales_record_master_id) as sales_record_master_ids
      "
    ).where(
      person_id: person_ids
    ).group(
      :person_id
    )
  end

  def you_tube_records_for(person_ids:)
    YouTubeRoyaltyRecordNew.select(
      "
        person_id,
        sum(net_revenue) AS amount_in_usd
      "
    ).where(
      person_id: person_ids
    ).group(
      :person_id
    )
  end

  def build_royalty_object(_royalty, sales_record, you_tube_record)
    {
      total_amount_in_usd: sales_record.nil? ? nil : Float(sales_record["total_amount_in_usd"]),
      you_tube_amount_in_usd: you_tube_record.nil? ? nil : Float(you_tube_record["amount_in_usd"]),
      sales_record_master_ids: sales_record.nil? ? nil : sales_record["sales_record_master_ids"],
      updated_at: Time.current
    }
  end

  def person_ids
    @person_ids ||= Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::PERSON_ROYALTIES_COMPUTED)
  end
end
