class Royalties::NotifySipService
  def self.run
    new.run
  end

  def run
    return unless Royalties::Posting.get_state == Royalties::Posting::SALES_RECORDS_COPIED

    Royalties::ApiClient.new.finalize(posting_ids)
    update_posting_state
    Royalties::CleanUpWorker.perform_async
  end

  private

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::COMPLETED)
  end

  def posting_ids
    Royalties::Posting.get_external_posting_ids
  end
end
