class Royalties::UpdateSalesRecordMastersService
  def self.run
    new.run
  end

  def run
    SalesRecordMaster.where(
      id: SalesRecordMasterNew.all.select(:id)
    ).update_all( # rubocop:disable Rails/SkipsModelValidations
      summarized: true,
      status: "posted"
    )

    update_posting_state

    copy_youtube_records
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::SALES_RECORD_MASTERS_UPDATED)
  end

  def copy_youtube_records
    Royalties::CopyYoutubeRecordsWorker.perform_async
  end
end
