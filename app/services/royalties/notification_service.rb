require "./lib/slack_notifier"

class Royalties::NotificationService
  FILENAME_PREFIX = "posting_royalties_"
  INSERT_BLOCK_SIZE = 1000
  ROYALTIES = "Royalties"
  CHANNEL = "tech_sip_dev"

  attr_accessor :posting_id

  def self.run
    new.run
  end

  def initialize
    @posting_id = Royalties::Posting.get_id
  end

  def run
    return unless Royalties::Posting.get_state == Royalties::Posting::CALCULATIONS_COMPLETED

    royalties = fetch_royalties
    split_earnings = fetch_splits_earnings
    populate_person_royalties(royalties, split_earnings)
    populate_posting_mappings
    update_posting_state
    export_royalty_data_report
    notify_in_slack(notification_message, ROYALTIES, CHANNEL)
  end

  private

  def populate_posting_mappings
    posting_mappings =
      Royalties::Posting.get_external_posting_ids.map do |external_posting_id|
        {
          posting_id: posting_id,
          external_posting_id: external_posting_id,
          source: Royalties::Posting.get_source,
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    PostingMapping.insert_all(posting_mappings) # rubocop:disable Rails/SkipsModelValidations
  end

  def fetch_royalties
    TempPersonRoyalty.select(
      "
        corporate_entity_id,
        user_currency,
        SUM(total_amount_in_usd) as total_amount,
        SUM(you_tube_amount_in_usd) as total_you_tube_amount_in_usd,
        SUM(encumbrance_amount_in_usd) as encumbrance_amount
      "
    ).group(
      :user_currency,
      :corporate_entity_id
    )
  end

  def fetch_splits_earnings
    TempRoyaltySplitDetail.select(
      "corporate_entity_id, currency, SUM(transaction_amount_in_usd) as amount"
    ).group(:currency, :corporate_entity_id)
  end

  def populate_person_royalties(royalties, split_earnings)
    royalties.each_slice(INSERT_BLOCK_SIZE) do |royalty_batches|
      partial_royalties =
        royalty_batches.map do |r|
          {
            posting_id: posting_id,
            corporate_entity_id: r["corporate_entity_id"],
            currency: r["user_currency"],
            total_amount_in_usd: r["total_amount"],
            total_you_tube_amount_in_usd: r["total_you_tube_amount_in_usd"],
            encumbrance_amount_in_usd: r["encumbrance_amount"],
            split_adjustments_in_usd: splits_for(r, split_earnings),
            state: PostingRoyalty::PENDING,
            created_at: Time.current,
            updated_at: Time.current
          }
        end
      PostingRoyalty.insert_all(partial_royalties) # rubocop:disable Rails/SkipsModelValidations
    end
  end

  def splits_for(royalty, split_earnings)
    return 0 if split_earnings.empty?

    split =
      split_earnings.select { |s|
        s["corporate_entity_id"] == royalty["corporate_entity_id"] &&
          s["currency"] == royalty["user_currency"]
      }
    return 0 if split.empty?

    Float(split.first["amount"])
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::FINANCE_APPROVAL_REQUESTED)
  end

  def file_name
    FILENAME_PREFIX + posting_id
  end

  def notification_message
    "Posting royalties computed for " + posting_id + ". Please check for data in currency conversion tool.
    You can find the report in the link " + get_report_url.to_s
  end

  def export_royalty_data_report
    csv_contents = Royalties::DataExportService.new.generate_csv
    upload_to_s3(csv_contents)
  end

  def s3_bucket
    @s3_bucket ||= Aws::S3::Resource.new.bucket(ENV.fetch("SYMPHONY_CSV_BUCKET"))
  end

  def upload_to_s3(data)
    file_path = "export-data/#{Rails.env}/#{file_name}.csv"
    object = s3_bucket.object(file_path)
    object.put(body: data)
  end

  def get_report_url
    file_path = "export-data/#{Rails.env}/#{file_name}.csv"
    S3_CLIENT.buckets[ENV.fetch("SYMPHONY_CSV_BUCKET")].objects[file_path].url_for(:read, expires: 7 * 86_400)
  end
end
