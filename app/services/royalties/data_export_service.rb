class Royalties::DataExportService
  def generate_csv
    CSV.generate("", row_sep: "\n\r") do |csv|
      csv << [
        "Sales Period",
        "Store Name",
        "Corporate Entity Id",
        "Exchange Rate",
        "Amount",
        "Amount Currency"
      ]

      get_data.each do |data|
        fx_rate = fx_rate_for(data[:amount_currency])
        data[:exchange_rate] = fx_rate
        data[:amount] = (Float(data[:amount]) * fx_rate)
        csv << data.values
      end
    end
  end

  def get_data
    get_youtube_data + get_sales_data
  end

  def get_youtube_data
    YouTubeRoyaltyRecordNew.select(
      "
        sales_period_start,
        'Youtube' as store_name,
        corporate_entity_id as corporate_entity_id,
        exchange_rate,
        sum(net_revenue) as amount,
        net_revenue_currency as amount_currency
      "
    ).joins(
      "
        join people on people.id = you_tube_royalty_records_new.person_id
      "
    ).group(
      :sales_period_start,
      :corporate_entity_id,
      :net_revenue_currency
    ).map { |record|
      {
        sales_period: record.sales_period_start,
        store_name: record.store_name,
        corporate_entity_id: record.corporate_entity_id,
        exchange_rate: record.exchange_rate,
        amount: record.amount,
        amount_currency: record.amount_currency
      }
    }
  end

  def get_sales_data
    SalesRecordNew.select(
      "
        period_sort, sip_stores_display_groups.name as store_name,
        corporate_entity_id as corporate_entity_id, exchange_rate,
        sum(amount) as amount, amount_currency
      "
    ).joins(
      "
        join sales_record_masters_new on sales_records_new.sales_record_master_id = sales_record_masters_new.id
        join sip_stores on sip_stores.id = sales_record_masters_new.sip_store_id
        join sip_stores_display_groups on sip_stores.display_group_id = sip_stores_display_groups.id
        join people on people.id = sales_records_new.person_id
      "
    ).group(
      "sip_stores_display_groups.name", :period_sort,
      :corporate_entity_id, :amount_currency
    ).map { |record|
      {
        sales_period: record.period_sort,
        store_name: record.store_name,
        corporate_entity_id: record.corporate_entity_id,
        exchange_rate: record.exchange_rate,
        amount: record.amount,
        amount_currency: record.amount_currency
      }
    }
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end
end
