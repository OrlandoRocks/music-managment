class Royalties::CopySalesRecordsService
  BATCH_SIZE = 100

  def self.run
    new.run
  end

  def run
    batch_ids.each do |batch_id|
      unprocessed_person_ids_by_currency = unprocessed_person_ids_by_currency_for(batch_id)

      next if unprocessed_person_ids_by_currency.blank?

      unprocessed_person_ids_by_currency.each do |currency, persons|
        fx_rate = fx_rate_for(currency)
        person_ids = persons.pluck(:person_id)

        person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
          ActiveRecord::Base.transaction do
            add_sales_records(person_ids_batch, fx_rate)
            TempPersonRoyalty.where(
              person_id: person_ids_batch
            ).update_all( # rubocop:disable Rails/SkipsModelValidations
              sales_records_copied: 1
            )
          end
        end
      end

      update_batch_state(batch_id)
    end

    update_posting_state
    Royalties::NotifySipWorker.perform_async
  end

  def batch_ids
    Royalties::BatchService.get_all_batch_ids
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end

  def add_sales_records(person_ids_batch, fx_rate)
    exec_sql(
      "
      INSERT INTO sales_records
      (
        person_id, person_intake_id, sip_sales_record_id,
        sales_record_master_id, related_id, related_type,
        distribution_type, quantity, revenue_per_unit,
        revenue_total, amount, posting_id
      )
      SELECT person_id, person_intake_id, sip_sales_record_id,
      sales_record_master_id, related_id, related_type,
      distribution_type, quantity, revenue_per_unit, revenue_total,
      truncate(amount * #{fx_rate}, 6) as amount,
      posting_id
      FROM sales_records_new
      WHERE person_id in (#{person_ids_batch.join(',')})
      "
    )
  end

  def unprocessed_person_ids_by_currency_for(batch_id)
    TempPersonRoyalty.select(
      :person_id,
      :user_currency
    ).where(
      person_id: get_person_ids_for(batch_id),
      sales_records_copied: 0
    ).group_by(&:user_currency)
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::SALES_RECORDS_COPIED)
  end

  def update_batch_state(batch_id)
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::SALES_RECORDS_COPIED)
  end

  def get_person_ids_for(batch_id)
    Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def exec_sql(sql)
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
