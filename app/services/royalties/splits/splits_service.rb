class Royalties::Splits::SplitsService
  BATCH_SIZE = 1000

  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id

  def initialize(batch_id)
    @batch_id = batch_id
  end

  def run
    unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids|
      Royalties::Splits::ComputeBatchSplitsService.run(person_ids)
    end

    TempPersonRoyalty.where(
      person_id: person_ids,
      split_computed: false
    ).update_all( # rubocop:disable Rails/SkipsModelValidations
      split_computed: true
    )

    update_batch_state

    Royalties::SyncWorker.perform_in(
      5.minutes,
      Royalties::BatchService::SPLITS_COMPUTED,
      Royalties::Posting::BATCHES_CREATED,
      Royalties::Posting::CALCULATIONS_COMPLETED,
      Royalties::NotificationWorker
    )
  end

  private

  def unprocessed_person_ids
    suspicious_flag = Flag.find_by!(Flag::SUSPICIOUS)
    TempPersonRoyalty
      .joins(:person)
      .left_outer_joins(person: :people_flags)
      .where(people_flags: { id: nil })
      .or(
        TempPersonRoyalty
        .joins(:person)
        .left_outer_joins(person: :people_flags)
        .where.not(people_flags: { flag_id: suspicious_flag.id })
      )
      .where(TempPersonRoyalty.arel_table[:encumbrance_amount_in_usd].eq(0))
      .where.not(people: { status: "Suspicious" })
      .where.not(total_amount_in_usd: nil)
      .where(person_id: person_ids)
      .where(split_computed: false)
      .pluck(:person_id)
  end

  def person_ids
    Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::SPLITS_COMPUTED)
  end

  def update_person_royalty(person_ids, amount)
    TempPersonRoyalty.where(
      person_id: person_ids
    ).update_all( # rubocop:disable Rails/SkipsModelValidations
      split_amount_in_usd: amount
    )
  end
end
