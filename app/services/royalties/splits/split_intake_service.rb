class Royalties::Splits::SplitIntakeService
  BATCH_SIZE = 25
  def self.run
    new.run
  end

  def run
    split_details_per_person = get_unprocessed_split_details
    split_details_per_person.each_slice(BATCH_SIZE) do |split_details_batch|
      last_split_intake_id = get_last_split_intake_id
      ActiveRecord::Base.transaction do
        person_ids_batch = split_details_batch.pluck("person_id")
        old_person_balances = get_old_person_balances_for(person_ids: person_ids_batch)

        create_split_intakes(split_details_batch)
        new_split_intakes = newly_created_split_intakes(person_ids_batch, last_split_intake_id)

        temp_split_details = TempRoyaltySplitDetail.where(person_id: person_ids_batch)
        temp_person_royalties = TempPersonRoyalty.where(person_id: temp_split_details.pluck("owner_id"))
        create_split_details(temp_split_details, new_split_intakes, temp_person_royalties)
        create_transactions(old_person_balances, new_split_intakes)
        update_person_balance(new_split_intakes, old_person_balances)

        update_flag_for_idempotency(temp_split_details)
      end
    end

    update_posting_state
    Royalties::PopulatePersonIntakeSalesRecordMastersWorker.perform_async
  end

  def get_unprocessed_split_details
    exec_sql(
      "
      SELECT
        person_id,
        sum(transaction_amount_in_usd) as amount,
        currency
      FROM
        temp_royalty_split_details
      WHERE processed = 0
      GROUP BY person_id
    "
    )
  end

  def update_flag_for_idempotency(split_details)
    TempRoyaltySplitDetail.where(
      id: split_details.select(:id)
    ).update_all( # rubocop:disable Rails/SkipsModelValidations
      processed: true
    )
  end

  def create_split_intakes(split_details)
    split_intakes_to_insert =
      split_details.map do |split_detail|
        {
          person_id: split_detail["person_id"],
          amount: split_detail["amount"] * fx_rate_for(split_detail["currency"]),
          currency: split_detail["currency"],
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    RoyaltySplitIntake.insert_all(split_intakes_to_insert) # rubocop:disable Rails/SkipsModelValidations
  end

  def newly_created_split_intakes(person_ids, last_split_intake_id)
    RoyaltySplitIntake.where(
      "id > ?", last_split_intake_id
    ).where(person_id: person_ids)
  end

  def create_split_details(split_details, split_intakes, temp_person_royalties)
    split_details_to_insert =
      split_details.map do |detail|
        {
          person_id: detail["person_id"],
          royalty_split_intake_id: split_intake_id_for(split_intakes, detail),
          royalty_split_id: detail["royalty_split_id"],
          song_id: detail["song_id"],
          person_intake_id: temp_person_royalties.find { |tpr| tpr.person_id == detail["owner_id"] }.person_intake_id,
          amount: detail["transaction_amount_in_usd"] * fx_rate_for(detail["currency"]),
          currency: detail["currency"],
          owner_id: detail["owner_id"],
          royalty_split_config: detail["royalty_split_config"],
          royalty_split_title: detail["royalty_split_title"],
          created_at: Time.current,
          updated_at: Time.current
        }
      end

    RoyaltySplitDetail.insert_all(split_details_to_insert) # rubocop:disable Rails/SkipsModelValidations
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end

  def create_transactions(old_person_balances, split_intakes)
    person_transactions_to_add =
      split_intakes.map do |split_intake|
        {
          person_id: split_intake.person_id,
          debit: [split_intake["amount"], 0].min * -1,
          credit: [split_intake["amount"], 0].max,
          previous_balance: old_person_balances.find { |pb| pb.person_id == split_intake["person_id"] }.balance,
          target_id: split_intake["id"],
          target_type: "SplitRoyaltyIntake",
          currency: split_intake["currency"],
          comment: "Splits Royalty",
          created_at: Time.current
        }
      end
    PersonTransaction.insert_all(person_transactions_to_add) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_person_balance(split_intakes, old_person_balances)
    person_balance_to_update =
      split_intakes.map do |split_intake|
        old_person_balance =
          old_person_balances.find { |pb|
            pb.person_id == split_intake["person_id"]
          }

        {
          id: old_person_balance.id,
          person_id: split_intake["person_id"],
          balance: old_person_balance.balance + split_intake["amount"],
          currency: split_intake["currency"],
          updated_at: Time.current
        }
      end
    PersonBalance.upsert_all(person_balance_to_update) # rubocop:disable Rails/SkipsModelValidations
  end

  def update_posting_state
    Royalties::Posting.update_state(Royalties::Posting::SPLIT_INTAKE_COMPLETED)
  end

  def get_old_person_balances_for(person_ids:)
    PersonBalance.select(
      :id,
      :person_id,
      :balance
    ).where(
      person_id: person_ids
    )
  end

  def split_intake_id_for(intakes, detail)
    intake =
      intakes.select do |i|
        i["person_id"] == detail["person_id"]
      end
    intake.first.id
  end

  def get_last_split_intake_id
    RoyaltySplitIntake.last&.id || 0
  end

  def exec_sql(sql)
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
