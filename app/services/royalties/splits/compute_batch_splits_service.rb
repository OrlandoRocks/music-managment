class Royalties::Splits::ComputeBatchSplitsService
  def self.run(person_ids)
    new(person_ids).run
  end

  attr_accessor :person_ids

  def initialize(person_ids)
    @person_ids = person_ids
    @records_to_insert = []
    @album_earnings_per_song = []
  end

  def run
    return if royalty_splits.blank?

    compute_album_earnings_per_song

    royalty_splits.each do |royalty_split, royalty_split_songs|
      song_ids = royalty_split_songs.pluck(:song_id)
      process_royalty_split(
        royalty_split: royalty_split,
        song_ids: song_ids
      )
    end

    ActiveRecord::Base.transaction do
      TempRoyaltySplitDetail.insert_all( # rubocop:disable Rails/SkipsModelValidations
        @records_to_insert
      ) if @records_to_insert.present?

      TempPersonRoyalty.where(
        person_id: person_ids
      ).update(
        split_computed: true
      )
    end
  end

  def compute_album_earnings_per_song
    album_earnings = SalesRecordNew
                     .where("related_type = 'Album' AND person_id in (#{person_ids.join(',')})")
                     .group(:related_id)
                     .select(["related_id, sum(amount) as revenue"])
    song_details = Song
                   .where(album_id: album_earnings.pluck(:related_id))
                   .group(:album_id)
                   .select(["album_id, count(id) as no_of_songs, GROUP_CONCAT(id SEPARATOR ',') as song_ids"])

    album_earnings.map do |album_earning|
      song_detail = song_details.find { |song| song.album_id == album_earning.related_id }
      @album_earnings_per_song << {
        album_id: album_earning.related_id,
        revenue: album_earning.revenue,
        no_of_songs: song_detail.no_of_songs,
        song_ids: song_detail.song_ids
      }
    end
  end

  def royalty_splits
    @royalty_splits ||=
      RoyaltySplitSong.joins(
        :royalty_split
      ).where(
        "royalty_splits.owner_id in (#{person_ids.join(',')})"
      ).where(
        "royalty_splits.deactivated_at is null or royalty_splits.deactivated_at > now()"
      ).group_by(&:royalty_split)
  end

  def process_royalty_split(royalty_split:, song_ids:)
    song_ids.each do |song_id|
      song_royalty_amount = song_royalty_amount_for(song_id: song_id)
      song_royalty_amount += album_royalty_amount_for(song_id: song_id)

      next unless song_royalty_amount.present? && song_royalty_amount.positive?

      process_split_recipients(
        royalty_split: royalty_split,
        song_id: song_id,
        song_royalty_amount: song_royalty_amount
      )
    end
  end

  def song_royalty_amount_for(song_id:)
    song_royalty = song_royalties.find { |royalty| royalty["song_id"] == song_id }
    song_royalty.present? ? Float(song_royalty["total_amount_in_usd"]) : 0
  end

  def album_royalty_amount_for(song_id:)
    return 0 if @album_earnings_per_song.empty?

    earnings_for_song = @album_earnings_per_song.find { |earning| earning[:song_ids].include?(song_id.to_s) }
    return Float(earnings_for_song[:revenue] / earnings_for_song[:no_of_songs]) if earnings_for_song.present?

    0
  end

  def process_split_recipients(royalty_split:, song_id:, song_royalty_amount:)
    mah_split_percentage = mah_percentage_for(royalty_split: royalty_split)

    mah_split_debit = song_royalty_amount * (100 - mah_split_percentage) / 100

    royalty_split_recipients = royalty_split_recipients_for(
      royalty_split_id: royalty_split.id
    )

    royalty_split_recipients.each do |recipient|
      next if recipient.person_id == royalty_split.owner_id

      recipient_amount = song_royalty_amount * recipient.percent / 100

      unless recipient.active?
        mah_split_debit -= recipient_amount
        next
      end

      @records_to_insert << temp_split_detail_for(
        royalty_split: royalty_split,
        song_id: song_id,
        person_id: recipient.person_id,
        owner_id: royalty_split.owner_id,
        amount: recipient_amount
      )
    end

    @records_to_insert << temp_split_detail_for(
      royalty_split: royalty_split,
      song_id: song_id,
      person_id: royalty_split.owner_id,
      owner_id: royalty_split.owner_id,
      amount: -1 * mah_split_debit
    )
  end

  def temp_split_detail_for(royalty_split:, song_id:, person_id:, owner_id:, amount:)
    {
      person_id: person_id,
      royalty_split_id: royalty_split.id,
      song_id: song_id,
      owner_id: owner_id,
      transaction_amount_in_usd: amount,
      currency: currency_for(person_id: person_id),
      corporate_entity_id: corporate_entity_id_for(person_id: person_id),
      royalty_split_config: royalty_split_config_for(royalty_split: royalty_split),
      royalty_split_title: royalty_split.title,
      created_at: Time.current,
      updated_at: Time.current
    }
  end

  def song_royalties
    @song_royalties ||= exec_sql(
      "
        SELECT
          person_id AS person_id,
          SUM(amount) AS total_amount_in_usd,
          related_id as song_id
        FROM
          sales_records_new
        WHERE
          related_id in (#{royalty_song_ids.join(',')}) AND related_type = 'Song'
        GROUP BY
          related_id
      "
    )
  end

  def currency_for(person_id:)
    person = people.find { |p| p.id == person_id }

    person.site_currency
  end

  def corporate_entity_id_for(person_id:)
    person = people.find { |p| p.id == person_id }

    person.corporate_entity_id
  end

  def royalty_split_recipients
    @royalty_split_recipients ||= RoyaltySplitRecipient.where(
      royalty_split_id: royalty_splits.keys
    ).group_by(&:royalty_split_id)
  end

  def royalty_song_ids
    @royalty_song_ids ||= royalty_splits.values.flatten.pluck(:song_id)
  end

  def people
    ids = royalty_split_recipients.values.flatten.pluck(:person_id).uniq

    @people ||= Person.includes(
      :country_website
    ).where(
      id: ids
    )
  end

  def royalty_split_recipients_for(royalty_split_id:)
    royalty_split_recipients[royalty_split_id]
  end

  def royalty_split_config_for(royalty_split:)
    royalty_split_config = {
      title: royalty_split.title,
      owner_id: royalty_split.owner_id,
      recipients: []
    }

    royalty_split_recipients_for(
      royalty_split_id: royalty_split.id
    ).each do |recipient|
      royalty_split_config[:recipients] << {
        person_id: recipient.person_id,
        percent: recipient.percent,
        active: recipient.active?,
        email: recipient.email
      }
    end

    royalty_split_config.to_json
  end

  def mah_percentage_for(royalty_split:)
    recipients = royalty_split_recipients_for(royalty_split_id: royalty_split.id)
    recipients.find { |r| r.person_id == royalty_split.owner_id }.percent
  end

  def exec_sql(sql)
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
