# frozen_string_literal: true

class Royalties::Posting
  POSTING_KEY = "ROYALTIES_POSTING"
  STARTED = "started"
  BATCHES_CREATED = "batches_created"
  CALCULATIONS_COMPLETED = "calculations_completed"
  FINANCE_APPROVAL_REQUESTED = "finance_approval_requested"
  FINANCE_APPROVED = "finance_approved"
  SALES_RECORD_MASTERS_COPIED = "sales_record_masters_copied"
  SALES_RECORDS_COPIED = "sales_records_copied"
  PERSON_INTAKE_SALES_RECORD_MASTERS_POPULATED = "person_intake_sales_record_masters_populated"
  SALES_RECORD_MASTERS_UPDATED = "sales_record_masters_updated"
  SUMMARIZATION_COMPLETED = "summarization_completed"
  YOUTUBE_RECORDS_COPIED = "youtube_records_copied"
  COMPLETED = "completed"
  CURRENCY_HASH_KEY = "ROYALTIES_CURRENCY_HASH"
  SPLIT_INTAKE_COMPLETED = "SPLIT_INTAKE_COMPLETED"

  SOURCE_SIP = "sip"
  SOURCE_SYMPHONY = "symphony"

  def self.add(posting_id, external_posting_ids = nil, source = SOURCE_SIP)
    Rails.cache.write(
      POSTING_KEY, {
        posting_id: posting_id,
        state: STARTED,
        external_posting_ids: external_posting_ids,
        source: source
      }.to_json
    )
  end

  def self.get_details
    posting_details = Rails.cache.fetch(POSTING_KEY)
    JSON.parse(posting_details) if posting_details.present?
  end

  def self.remove
    Rails.cache.delete(POSTING_KEY)
    Rails.cache.delete(CURRENCY_HASH_KEY)
  end

  def self.update_state(state)
    posting_details = get_details
    posting_details["state"] = state
    Rails.cache.write(
      POSTING_KEY, posting_details.to_json
    )
  end

  def self.get_id
    posting_details = get_details
    posting_details["posting_id"] if posting_details.present?
  end

  def self.get_state
    posting_details = get_details
    posting_details["state"] if posting_details.present?
  end

  def self.add_currency_hash(currency_hash)
    Rails.cache.write(CURRENCY_HASH_KEY, currency_hash.to_json)
  end

  def self.get_fx_rate_for(currency)
    currency_hash = Rails.cache.fetch(CURRENCY_HASH_KEY)
    return 1 if currency_hash.blank?

    currency_hash = JSON.parse(currency_hash)
    currency_rates = currency_hash.find { |c| c["currency"] == currency }
    return 1 if currency_rates.nil?

    Float(currency_rates["fx_rate"])
  end

  def self.get_external_posting_ids
    posting_details = get_details
    posting_details["external_posting_ids"] if posting_details.present?
  end

  def self.get_source
    posting_details = get_details
    posting_details["source"] if posting_details.present?
  end
end
