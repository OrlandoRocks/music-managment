class Royalties::SummarizationService
  BATCH_SIZE = 10
  def self.run(batch_id)
    new(batch_id).run
  end

  attr_accessor :batch_id

  def initialize(batch_id)
    @batch_id = batch_id
  end

  def run
    sales_record_masters_new_by_currency = SalesRecordMasterNew.select(
      :id,
      :amount_currency
    ).group_by(&:amount_currency)

    sales_record_masters_new_by_currency.map do |currency, sales_record_masters|
      fx_rate = fx_rate_for(currency)
      sales_record_master_ids = sales_record_masters.pluck(:id)

      unprocessed_person_ids.each_slice(BATCH_SIZE) do |person_ids_batch|
        ActiveRecord::Base.transaction do
          add_sales_record_summaries(person_ids_batch, sales_record_master_ids, fx_rate)
          TempPersonRoyalty.where(person_id: person_ids_batch, user_currency: currency).update_all(summarized: 1) # rubocop:disable Rails/SkipsModelValidations
        end
      end
    end

    update_batch_state

    Royalties::SyncWorker.perform_in(
      5.minutes,
      Royalties::BatchService::SUMMARIZED,
      Royalties::Posting::SALES_RECORD_MASTERS_COPIED,
      Royalties::Posting::SUMMARIZATION_COMPLETED,
      Royalties::Splits::SplitIntakeWorker
    )
  end

  def fx_rate_for(currency)
    Royalties::Posting.get_fx_rate_for(currency)
  end

  def add_sales_record_summaries(person_ids_batch, sales_record_master_ids, fx_rate)
    exec_sql(
      "
      INSERT INTO sales_record_summaries
      SELECT person_id, sales_record_master_id, related_type, related_id,
      IF( related_type = 'Video', 'Video',
        IF( related_type = 'Song',
          (select album_type from songs s inner join albums a on a.id = s.album_id where s.id = related_id),
        (select album_type from albums where albums.id = related_id))) as release_type,
      IF( related_type = 'Video', related_id,
        IF( related_type = 'Album', related_id,
        (select album_id from songs where songs.id = related_id))) as release_id,
      SUM(IF(distribution_type='Download',quantity,0)) as downloads_sold,
      SUM(IF(distribution_type='Streaming',quantity,0)) AS streams_sold,
      SUM(IF(distribution_type='Download', amount * #{fx_rate}, 0)) as download_amount,
      SUM(IF(distribution_type='Streaming', amount * #{fx_rate},0)) as stream_amount,
      person_intake_id
      FROM sales_records_new
      WHERE person_id in (#{person_ids_batch.join(',')})
      AND sales_record_master_id in (#{sales_record_master_ids.join(',')})
      GROUP BY person_id, related_type, related_id, sales_record_master_id;"
    )
  end

  def person_ids
    @person_ids ||= Royalties::BatchService.get_batch_person_ids(batch_id)
  end

  def unprocessed_person_ids
    TempPersonRoyalty.where(
      person_id: person_ids,
      summarized: 0
    ).where.not(total_amount_in_usd: nil).pluck(:person_id)
  end

  def update_batch_state
    Royalties::BatchService.update_batch_state(batch_id, Royalties::BatchService::SUMMARIZED)
  end

  def exec_sql(sql)
    ActiveRecord::Base.connection.exec_query(sql)
  end
end
