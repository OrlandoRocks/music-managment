class MassRetryService
  attr_accessor :person_id, :job_name, :retry_limit, :file_name, :priority

  CSV_ROW = {
    upc: 0,
    store: 1
  }

  def initialize(person_id:, job_name:, retry_limit:, file_name:, priority: 0)
    @person_id = person_id
    @job_name = job_name
    @retry_limit = retry_limit
    @file_name = file_name
    validate_envs!
    @priority = priority
  end

  def run
    # csv data formatted as:
    # upc, store_id
    retry_data = CSV.read(RakeDatafileService.download(file_name).local_file)
    current_store_id = retry_data.first[CSV_ROW[:store]]
    upcs = []

    retry_data.each do |row|
      if current_store_id != row[CSV_ROW[:store]]
        enqueue_mass_retry_job!(upcs, current_store_id, 1)
        current_store_id = row[CSV_ROW[:store]]
        upcs = [row[CSV_ROW[:upc]]]
      end
      upcs << row[CSV_ROW[:upc]]
    end

    last_store_id = retry_data.last[CSV_ROW[:store]]
    return if last_store_id != current_store_id

    enqueue_mass_retry_job!(upcs, current_store_id, 1)
  end

  def enqueue_mass_retry_job!(upcs, store_id, priority)
    store = Store.find(store_id)

    retry_params = {
      person_id: person_id,
      album_ids_or_upcs: upcs,
      store_ids: [store.id],
      remote_ip: 1,
      job_name: "#{job_name}_#{store.short_name}",
      metadata_only: nil,
      daily_retry_limit: retry_limit,
      retry_in_batches: true,
      use_sns_only: "on",
      priority: priority
    }

    retry_form = Distribution::MassRetryForm.new(retry_params)
    unless retry_form.save
      raise "MassRetry for #{store.short_name} failed due to following errors:
      #{retry_form.errors.messages.values.join(' ')}"
    end

    Rails.logger.info "MassRetry Job Eneuqued for #{store.short_name}, total job size is #{upcs.count} albums"
  end

  def validate_envs!
    var_keys = ["retry_limit", "job_name", "person_id", "file_name"]
    errors = []
    errors << "Enter RETRY_LIMIT of less than 5000" if Integer(retry_limit, 10) > 5000

    var_keys.each do |key|
      errors << "#{key.upcase} is missing" if send(key).nil?
    end
    raise "Job failed due to the following incorrect arguments:\n#{errors.join("\n")}" if errors.any?
  end
end
