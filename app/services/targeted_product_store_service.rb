class TargetedProductStoreService
  FB_FREEMIUM = "Facebook / Instagram / Reels".freeze
  DISCOVERY = "Discovery Platforms".freeze
  DISCOVERY_TARGETED_PRODUCT_NAMES = [FB_FREEMIUM, DISCOVERY].freeze

  def self.fetch_offer(params, item)
    constraints = "
    (
      (
        date_constraint = 'expiration'
        AND CURDATE() >= DATE(targeted_offers.start_date)
        AND CURDATE() <= DATE(targeted_offers.expiration_date)
      )
      OR
      (
        date_constraint = 'join plus'
        AND CURDATE() <= DATE_ADD(DATE(:created_on), INTERVAL targeted_offers.join_plus_duration DAY)
      )
      OR
      (
        date_constraint = 'permanent'
      )
    )
    AND products.applies_to_product = :applies_to_product
    AND products.product_type = :product_type
    AND targeted_offers.country_website_id = :country_website_id
    "

    targeted_stores_offers = TargetedOffer.joins(targeted_products: [:targeted_product_stores, :product])
                                          .where([constraints, params])
                                          .active
    validate_targeted_store_offers(targeted_stores_offers, item)
  end

  def self.validate_targeted_store_offers(targeted_stores_offers, item)
    targeted_stores_offers.find do |targeted_stores_offer|
      item_stores = (item.stores.pluck(:id) - Store::BELIEVE_STORE_IDS)

      break if item_stores.empty?

      # we are offering different discovery stores to a subset of users via feature flag
      # this assigns discovery platform eligible store ids based off Store.discovery_platforms which assess feature flag value

      targeted_store_ids =
        if targeted_stores_offer.targeted_products.any? { |tp| tp.display_name.in? DISCOVERY_TARGETED_PRODUCT_NAMES }
          targeted_stores_offer.targeted_offer_stores_ids & Store.discovery_platforms(item.person).pluck(:id)
        else
          targeted_stores_offer.targeted_offer_stores_ids
        end

      # only a subset of targeted_product stores needed for targeted_offer
      # do not apply offer if any store not in targeted store ids
      item_stores.all? { |store_id| targeted_store_ids.include?(store_id) }
    end
  end
end
