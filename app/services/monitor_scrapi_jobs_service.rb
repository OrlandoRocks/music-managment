# frozen_string_literal: true

require "./lib/slack_notifier"

class MonitorScrapiJobsService
  JOB_NOT_CREATED_ALERT = "There's no scrapi job added to the database in the last 12 hours"
  JOB_NOT_PROCESSED_ALERT = "There are no processed scrapi jobs for the last 24 hours"
  ALERT_TITLE = "Scrapi Jobs Error"

  def self.notify
    new.tap(&:notify)
  end

  def notify
    notify_jobs_not_created
    notify_jobs_not_processed
  end

  def notify_jobs_not_created
    jobs = ScrapiJob.where("created_at > ?", 12.hours.ago)
    send_alerts(ALERT_TITLE, JOB_NOT_CREATED_ALERT) if jobs.blank?
  end

  def notify_jobs_not_processed
    jobs_statuses = ScrapiJob.where("created_at > ?", 24.hours.ago).map(&:status).uniq
    send_alerts(ALERT_TITLE, JOB_NOT_PROCESSED_ALERT) if jobs_statuses.present? && jobs_statuses.exclude?("done")
  end

  def send_alerts(alert_title, error)
    ScrapiJobFailureSummaryMailer.scrapi_jobs_errors_email(alert_title, error).deliver
    notify_in_slack(alert_title, error)
  end
end
