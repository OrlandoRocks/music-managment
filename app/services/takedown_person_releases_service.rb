class TakedownPersonReleasesService
  attr_reader :person, :admin, :ip_address

  def self.takedown_releases(person_id, admin_id, ip_address)
    new(person_id, admin_id, ip_address).takedown_releases
  end

  def initialize(person_id, admin_id, ip_address)
    @person = Person.includes(:albums).find_by(id: person_id)
    @admin = Person.find_by(id: admin_id)
    @ip_address = ip_address
  end

  def takedown_releases
    ApplicationRecord.transaction do
      MassTakedownForm.new(options).save
    end
  end

  private

  def options
    {
      album_ids: person.live_releases.map(&:id).map(&:to_s).join(","),
      track_level_stores: Store::TRACK_MONETIZATION_STORES,
      album_level_stores: "all",
      send_email: "0",
      ip_address: ip_address,
      admin: admin
    }
  end
end
