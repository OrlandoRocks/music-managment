class YoutubePreferencesExportService
  HEADERS = [
    "TuneCore ID",
    "First Name",
    "Last Name",
    "Email",
    "Channel ID",
    "Whitelist?",
    "MCN",
    "MCN Agreed At",
    "Updated At"
  ].freeze

  CSV_PATH = "/tmp/youtube/exports/".freeze

  def self.export(from_time = nil)
    new(from_time).tap(&:export)
  end

  attr_reader :update_from,
              :update_to,
              :preferences,
              :export_csv,
              :reset_csv,
              :export_filename,
              :reset_filename,
              :preferences_export

  def initialize(from_time = nil)
    @update_from = start_time(from_time)
    @update_to   = Time.now
    @preferences = preferences_from_range
  end

  def export
    return unless preferences

    @preferences_export = YoutubePreferencesExport.create(from: update_from)
    build_csvs
    update_export
    send_email
  end

  private

  def start_time(from_time)
    from_time.present? ? Time.parse(from_time) : YoutubePreferencesExport.maximum("to") || Time.at(0)
  end

  def preferences_from_range
    YoutubePreference.where(
      "updated_at > :start AND updated_at <= :end",
      { start: update_from, end: update_to }
    ).includes(:person)
  end

  def build_csvs
    path = Pathname.new(CSV_PATH).join(update_to.end_of_week.strftime("%Y-%m-%d"))
    FileUtils.mkdir_p(path) unless File.exist?(path)

    @export_filename = path.join("export_#{preferences_export.id}.csv")
    @reset_filename  = path.join("reset_#{preferences_export.id}.csv")

    @export_csv     = CSV.open(export_filename, "wb")
    @reset_csv      = CSV.open(reset_filename, "wb")
    begin
      write_csv_headers
      write_csvs
    ensure
      close_csvs
    end
  end

  def write_csv_headers
    [export_csv, reset_csv].each do |csv_file|
      csv_file << HEADERS
    end
  end

  def write_csvs
    preferences.each do |pref|
      person = pref.person
      next if person.blank?

      write_export_row(pref, person) if pref.channel_id.present?
      if pref.paper_trail.previous_version
        write_reset_row_previous_version(pref, person)
      elsif pref.created_at != pref.updated_at
        write_reset_row(pref, person)
      end
    end
  end

  def close_csvs
    export_csv.close
    reset_csv.close
  end

  def write_export_row(pref, person)
    export_csv << [
      pref.person_id,
      person.first_name,
      person.last_name,
      person.email,
      pref.channel_id,
      pref.whitelist,
      pref.mcn,
      pref.mcn_agreement,
      pref.updated_at.strftime("%Y/%m/%d %I:%M %p")
    ]
  end

  def write_reset_row_previous_version(pref, person)
    prev_version = pref.paper_trail.version_at(update_to - 1.day)
    reset_csv << [
      pref.person_id,
      person.first_name,
      person.last_name,
      person.email,
      prev_version&.channel_id,
      prev_version&.whitelist,
      prev_version&.mcn,
      prev_version&.mcn_agreement,
      prev_version&.updated_at&.strftime("%Y/%m/%d %I:%M %p")
    ]
  end

  def write_reset_row(pref, person)
    reset_csv << [
      pref.person_id,
      person.first_name,
      person.last_name,
      person.email,
      "",
      "",
      "",
      "",
      ""
    ]
  end

  def update_export
    preferences_export.update(to: update_to, filename: export_filename)
  end

  def send_email
    AdminNotifier.youtube_preferences_export(
      preferences_export,
      export_filename,
      reset_filename
    ).deliver
  end
end
