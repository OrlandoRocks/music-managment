# frozen_string_literal: true

require "./lib/slack_notifier"

class Paypal::RefundFailedTransactionService
  AIRBRAKE_MESSAGE = "User erroneously charged by Paypal and automatically refunded, see GS-9709"
  SLACK_ALERT_MESSAGE = "A failed Paypal transaction was refunded. Transaction id: "

  attr_accessor :txn

  def self.refund_if_charged_and_failed(txn)
    refund_service = new(txn)
    return unless refund_service.charged_by_paypal_and_refund_failed_transactions?

    refund_service.refund
  end

  def initialize(txn)
    @txn = txn
  end

  def charged_by_paypal_and_refund_failed_transactions?
    charged_by_paypal? && refund_failed_transactions?
  end

  def refund
    paypal_transaction = txn
    credentials = txn.person.country_website.paypal_checkout_credentials

    refunded_transaction = PayPalAPI.refund(credentials, paypal_transaction.transaction_id)
    PaypalTransaction.record_refunded_transaction(refunded_transaction, paypal_transaction)

    log_and_airbrake
  end

  private

  def charged_by_paypal?
    txn.completed? && txn.sale?
  end

  def error_handler # we have this in the Paypal cart payment strategy. Do we want it in all payment flows?
    case txn.raw_response
    when /L_ERRORCODE0=10201/
      stored_paypal_account.destroy(person, skip_validation: true)
      self.authorizer = :paypal
    end
  end

  def log_and_airbrake
    method_metadata = {
      current_method_name: :refund,
      caller_method_path: caller&.first
    }
    log_options("refunding failed paypal transaction", method_metadata)

    send_paypal_refund_airbrake(txn.id)
  end

  def log_options(message, method_metadata)
    options = {
      person: txn&.person,
      invoice: txn&.invoice,
      paypal_transaction_id: txn&.id,
      message: message
    }.merge!(method_metadata)

    InvoiceLogService.log(options)
  end

  def refund_failed_transactions?
    FeatureFlipper.show_feature?(:refund_failed_paypal_transactions, txn.person)
  end

  def send_paypal_refund_airbrake(paypal_transaction_id)
    airbrake_options = {
      person: txn&.person,
      paypal_transaction: txn,
      caller_method_path: caller&.first
    }
    Tunecore::Airbrake.notify(AIRBRAKE_MESSAGE, airbrake_options)
    notify_in_slack(SLACK_ALERT_MESSAGE + paypal_transaction_id.to_s, "Paypal Transactions")
  end
end
