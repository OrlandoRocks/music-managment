class TaxTokensCohortsMailService
  def self.send_mail_for_cohort(tax_tokens_cohort_id)
    new.send_mail_for_cohort(tax_tokens_cohort_id)
  end

  def send_mail_for_cohort(tax_tokens_cohort_id)
    tax_tokens_cohort = TaxTokensCohort.find(tax_tokens_cohort_id)

    tax_tokens_cohort.tax_tokens.visible.incomplete.find_each do |tax_token|
      MailerWorker.perform_async("TaxTokenMailer", "tax_token_notification", tax_token.id)
    end
  end
end
