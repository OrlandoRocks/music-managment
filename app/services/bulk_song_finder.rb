class BulkSongFinder
  FIND_BY_OPTIONS = {
    "Song Identifiers" => "song",
    "Album Identifiers" => "album"
  }.freeze

  FIND_BY_DEFAULT = FIND_BY_OPTIONS.values.first.freeze

  attr_reader :song_identifiers, :find_by, :messages

  def initialize(song_identifiers, find_by: FIND_BY_DEFAULT)
    @song_identifiers = song_identifiers
    @find_by = find_by
    @messages = {}
  end

  def execute
    find_songs
  end

  def find_songs
    @song_identifiers =
      if comma_separated?
        song_identifiers.split(/,/).collect(&:strip)
      else
        song_identifiers.split
      end

    send(find_method)
  end

  def comma_separated?
    song_identifiers.include?(",")
  end

  def find_method
    find_by_method_name = "find_by_#{find_by}_identifiers"

    respond_to?(find_by_method_name) ? find_by_method_name : "find_by_song_identifiers"
  end

  def find_by_song_identifiers
    song_identifiers.map do |identifier|
      song = find_song_by_id(identifier)
      song ||= find_song_by_isrc(identifier)
      song || set_missing_song_message(identifier)
    end.compact
  end

  def find_by_album_identifiers
    song_identifiers.map do |identifier|
      songs = find_songs_by_album_id(identifier)
      songs = songs.presence || find_songs_by_album_upc(identifier)
      songs.presence || set_missing_album_message(identifier)
    end.flatten.compact
  end

  def find_song_by_id(identifier)
    Song.find_by(id: identifier)
  end

  def find_song_by_isrc(identifier)
    Song.find_by("songs.tunecore_isrc = ? OR songs.optional_isrc = ?", identifier, identifier)
  end

  def find_songs_by_album_id(identifier)
    Song.where("album_id = ?", identifier)
  end

  def find_songs_by_album_upc(identifier)
    Song.joins(album: :upcs).where("upcs.number = ?", identifier)
  end

  def set_missing_song_message(identifier)
    @messages[identifier] = "Song with ID of #{identifier} or song ISRC of #{identifier} not found."
    nil
  end

  def set_missing_album_message(identifier)
    @messages[identifier] = "Song with album ID of #{identifier} or album UPC of #{identifier} not found."
    nil
  end
end
