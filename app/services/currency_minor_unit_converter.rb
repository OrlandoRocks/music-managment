# frozen_string_literal: true

#  Minor unit is the smallest unit of a currency, depending on the number of decimals.
#  Most currencies have two decimals. Some currencies do not have decimals,
#  and some have three decimals
#  10 GBP: GBP has two decimals, so in minor units submit an amount of 1000
#  10 JPY: JPY has no decimals, so in minor units submit an amount of 10
#  10 BHD: BHD has three decimals, so in minor units submit an amount of 10000
#  See: https://en.wikipedia.org/wiki/ISO_4217#Minor_unit_fractions
class CurrencyMinorUnitConverter
  def initialize(source_currency, target_currency)
    @source_currency = Money::Currency.new(source_currency)
    @target_currency = Money::Currency.new(target_currency)
  end

  def adjust_minor_unit_for(amount)
    amount.to_d * coefficent
  end

  private

  attr_reader :source_currency, :target_currency

  def coefficent
    10**conversion_exponent
  end

  def conversion_exponent
    target_currency_minor_unit_fractions - source_currency_minor_unit_fractions
  end

  def source_currency_minor_unit_fractions
    source_currency.exponent
  end

  def target_currency_minor_unit_fractions
    target_currency.exponent
  end
end
