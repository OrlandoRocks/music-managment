class SongData::ParamsScrubber
  def self.scrub(params, album)
    new(params, album).scrub
  end

  attr_accessor :params_copy

  def initialize(params, album)
    @params = params
    @album = album
    @params_copy = params.deep_dup.with_indifferent_access
  end

  def scrub
    return params_copy if params_copy.empty?

    finalized_params if @album && !@album.is_editable?
    persisted_params if @album&.persisted? && reject_optional_upc_number?

    params_copy
  end

  private

  def finalized_params
    params_copy.reject! { |key, _| %i[isrc upc optional_upc_number].include?(key.to_sym) }
  end

  def persisted_params
    params_copy.except!(:optional_upc_number)
  end

  def reject_optional_upc_number?
    (params_copy[:optional_upc_number] == @album.optional_upc_number) ||
      (params_copy[:optional_upc_number] == @album.tunecore_upc_number)
  end
end
