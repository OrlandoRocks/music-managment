class SongData::CompletionReport
  attr_reader :song_data

  def initialize(song_data)
    @song_data = song_data
  end

  def completed_required_fields?
    song_data.name? && song_data.asset_url? && songwriter?
  end

  def completed_suggested_fields?
    artists_fields_complete?
  end

  def breakdown
    {
      required_fields: required_fields,
      suggested_fields: suggested_fields
    }
  end

  private

  def required_fields
    {
      name: song_data.name?,
      asset_url: song_data.asset_url?,
      songwriter: songwriter?
    }
  end

  def suggested_fields
    {
      artists: artists_fields_complete?
    }
  end

  def songwriter?
    @songwriter ||=
      song_data.artists.any? do |artist|
        artist.role_ids && artist.role_ids.include?(self.class.songwriter_role_id.to_s)
      end
  end

  def artists_fields_complete?
    @artist_fields_complete ||=
      song_data.artists.all? do |artist|
        artist.credit && artist.name && artist.role_ids
      end
  end

  def self.songwriter_role_id
    @@songwriter_role_id ||= SongRole
                             .select(:id)
                             .where(role_type: "songwriter")
                             .pluck(:id).first
  end
end
