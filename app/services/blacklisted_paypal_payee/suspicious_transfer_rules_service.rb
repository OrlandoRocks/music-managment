class BlacklistedPaypalPayee::SuspiciousTransferRulesService
  def self.suspicious?(transfer)
    new(transfer).suspicious?
  end

  def initialize(transfer)
    @transfer = transfer
  end

  def suspicious?
    previous_succesful_non_paypal_transfers && no_previous_successful_paypal_transfers
  end

  private

  attr_reader :transfer

  def previous_succesful_non_paypal_transfers
    previously_approved_check_transfers || previously_approved_eft_transfer
  end

  def no_previous_successful_paypal_transfers
    !PaypalTransfer.exists?(person_id: transfer.person_id, transfer_status: "completed")
  end

  def previously_approved_check_transfers
    CheckTransfer.exists?(person_id: transfer.person_id, transfer_status: "completed")
  end

  def previously_approved_eft_transfer
    EftBatchTransaction.joins(stored_bank_account: :person).exists?({ stored_bank_accounts: { person_id: transfer.person_id }, status: "success" })
  end
end
