class BlacklistedPaypalPayee::SearchService
  def self.search(payee, page, per_page)
    new(payee, page, per_page).search
  end

  def initialize(payee, page, per_page)
    @payee    = payee
    @page     = page     || 1
    @per_page = per_page || 25
  end

  def search
    payee ? by_payee : paginate_all
  end

  private

  attr_reader :payee, :page, :per_page

  def by_payee
    BlacklistedPaypalPayee.where(payee: payee)
  end

  def paginate_all
    BlacklistedPaypalPayee.paginate(page: page, per_page: per_page)
  end
end
