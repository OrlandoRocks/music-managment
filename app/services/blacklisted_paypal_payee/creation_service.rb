class BlacklistedPaypalPayee::CreationService
  attr_reader :payees

  def self.create(payees)
    new(payees).create
  end

  def initialize(payees)
    @payees = sanitize_payees(payees)
  end

  def create
    payees.each do |payee|
      BlacklistedPaypalPayee.create(payee: payee.strip)
    end
  rescue => e
    false
  end

  def sanitize_payees(payees)
    payees.split(/[\r\n ,]+/).uniq
  end
end
