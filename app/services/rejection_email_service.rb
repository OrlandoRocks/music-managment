class RejectionEmailService
  attr_reader :review_audit

  def initialize(review_audit)
    @review_audit = review_audit
  end

  def notify
    template_type = review_audit
                    .review_reasons
                    .pluck(:template_type)
                    .compact
                    .uniq

    if template_type.blank?
      Airbrake.notify(error_msg, album: review_audit.album)
      return
    end

    if template_type.first === ReviewReason::WILL_NOT_DISTRIBUTE
      review_audit.review_reasons.each do |review_reason|
        send_rejection_email(review_reason.email_template_path)
      end
    elsif template_type.size === 1
      send_rejection_email(template_type.first)
    else
      notify_airbrake
    end
  end

  private

  def send_rejection_email(template_type)
    RejectionEmailMailer
      .send(template_type.to_sym, review_audit)
      .deliver
  end

  def error_msg
    "error delivering release rejected email for album_id=#{review_audit.album.id}. "\
      "No template type was found"
  end

  def notify_airbrake
    album = review_audit.album
    error_msg = "Unable to send rejection email for Album ID: #{album}\n" \
      "Review Audit contains Review Reasons from more than one template"

    Airbrake.notify(error_msg, album: album)
  end
end
