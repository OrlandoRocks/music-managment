class AlbumCountry::RelationTypeService
  def self.update(album, new_countries)
    new(album, new_countries).tap(&:update)
  end

  def self.included(album)
    new(album, nil).included
  end

  def self.excluded(album)
    new(album, nil).excluded
  end

  attr_reader :album, :new_countries

  def initialize(album, new_countries)
    @album         = album
    @new_countries = new_countries
  end

  def update
    new_excluded = Country.unsanctioned - new_countries
    if new_countries.all?(&:blank?)
      relation_type = "exclude"
      countries = Country.unsanctioned
    elsif new_countries.size > new_excluded.size
      relation_type = "exclude"
      countries = new_excluded
    else
      relation_type = "include"
      countries = new_countries
    end

    ActiveRecord::Base.transaction do
      album.album_countries.destroy_all
      countries.each do |country|
        AlbumCountry.create(album: album, country: country, relation_type: relation_type)
      end
    end

    album.album_countries.reload
    true
  end

  def included
    return Country.unsanctioned if album.album_countries.empty?

    relation_type = album.album_countries.first.relation_type

    case relation_type
    when "include"
      Country.by_album_country(album.album_countries) - Country.sanctioned
    when "exclude"
      Country.unsanctioned - Country.by_album_country(album.album_countries)
    else
      raise "Invalid AlbumCountry.relation_type: #{relation_type}"
    end
  end

  def excluded
    Country.unsanctioned - album.countries
  end
end
