module Braintree
  class WebhookParser
    def initialize(payin_provider_config = default_payin_provider_config)
      @payin_provider_config = payin_provider_config
    end

    def parse!(bt_signature, bt_payload)
      gateway
        .webhook_notification
        .parse(bt_signature, bt_payload)
    end

    private

    attr_reader :payin_provider_config

    def default_payin_provider_config
      PayinProviderConfig.find_by(name: PayinProviderConfig::BRAINTREE_NAME)
    end

    def gateway
      Braintree::ConfigGatewayService.new(payin_provider_config).gateway
    end
  end
end
