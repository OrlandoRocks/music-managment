# frozen_string_literal: true

require "./lib/slack_notifier"

class Braintree::RefundFailedTransactionService
  attr_accessor :txn

  STATUS = {
    ERROR: "error",
    SUCCESS: "success"
  }.freeze

  ACTION = {
    REFUND: "refund",
    SALE: "sale"
  }.freeze

  AIRBRAKE_MESSAGE = "User erroneously charged by Braintree and automatically refunded, see GS-7471"
  SLACK_ALERT_MESSAGE = "A failed Braintree transaction was refunded. Transaction id: "
  REFUND_REASON = "Rollback after error processing charge"

  def self.refund_if_charged_and_failed(txn)
    refund_service = new(txn)
    return unless refund_service.charged_by_braintree_and_autorefund_failed_braintree_transactions?

    refund_service.refund
  end

  def initialize(txn)
    @txn = txn
  end

  def autorefund_failed_braintree_transactions?
    FeatureFlipper.show_feature?(:autorefund_failed_braintree_transactions, txn.person)
  end

  def charged_by_braintree?
    txn.status == STATUS[:SUCCESS] && txn.action == ACTION[:SALE]
  end

  def charged_by_braintree_and_autorefund_failed_braintree_transactions?
    charged_by_braintree? && autorefund_failed_braintree_transactions?
  end

  def refund
    # Sometimes txns may not have an id. Sometimes txns do not have an invoice.
    braintree_transaction_id = txn.id || txn.invoice.braintree_transactions.first.id
    BraintreeTransaction.process_refund(braintree_transaction_id, txn.amount, refund_reason: REFUND_REASON)
    mark_txn_as_refunded
    send_braintree_refund_airbrake(braintree_transaction_id)
  end

  def mark_txn_as_refunded
    txn.update(action: ACTION[:REFUND], status: STATUS[:ERROR])
  end

  def send_braintree_refund_airbrake(braintree_transaction_id)
    airbrake_options = {
      person: txn&.person,
      invoice: txn&.invoice,
      braintree_transaction: txn,
      caller_method_path: caller&.first
    }
    Tunecore::Airbrake.notify(AIRBRAKE_MESSAGE, airbrake_options)
    notify_in_slack(SLACK_ALERT_MESSAGE + braintree_transaction_id.to_s, "Braintree Transactions")
  end
end
