class Braintree::RemoveCardService
  attr_reader :person_ids

  STORED_CREDIT_CARD = "StoredCreditCard".freeze

  def self.process(person_ids)
    return unless person_ids.is_a?(Array) && person_ids.compact.present?

    new(person_ids).delete_all_cards
  end

  def initialize(person_ids)
    @person_ids = person_ids
  end

  def delete_all_cards
    people_with_ccs = Person.where(id: person_ids).joins(:stored_credit_cards).distinct
    num_of_ccs = StoredCreditCard.braintree_cards.where(person: people_with_ccs).length
    total_cards_deleted = 0

    Rails.logger.info("Starting cc removal for #{people_with_ccs.length} users, #{num_of_ccs} credit cards")

    people_with_ccs.each do |p|
      braintree_cards = p.stored_credit_cards.braintree_cards
      braintree_cards.each do |cc|
        last_vault_transaction = BraintreeVaultTransaction
                                 .where(related_type: STORED_CREDIT_CARD)
                                 .where(related_id: cc.id)
                                 .where(person_id: p.id)
                                 .last
        ip_address = last_vault_transaction.ip_address if last_vault_transaction.present?

        # skipping validation because we want to delete all of their cards, and validations disallow that
        cc.destroy(ip_address, p, { skip_validation: true }) if ip_address.present?

        if cc.deleted_at.present?
          total_cards_deleted += 1
          Rails.logger.info("Removed stored_credit_card_id: #{cc.id} for person_id: #{p.id}")
        else
          Rails.logger.info("Failed to remove stored_credit_card_id: #{cc.id} for person_id: #{p.id}")
        end
      end
    end

    Rails.logger.info("Removed #{total_cards_deleted} cards, #{num_of_ccs - total_cards_deleted} failed to remove")
  end
end
