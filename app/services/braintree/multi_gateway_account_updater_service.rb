# frozen_string_literal: true

class Braintree::MultiGatewayAccountUpdaterService
  attr_reader :config, :bt_signature, :bt_payload

  def self.account_updater_report(config, request)
    new(config, request).account_updater_report
  end

  def initialize(config, request)
    @bt_signature = request.params["bt_signature"]
    @bt_payload = request.params["bt_payload"]
    @config = config
  end

  def account_updater_report
    gateway = Braintree::Gateway.new(
      environment: Rails.env.production? ? :production : :sandbox,
      merchant_id: config.merchant_id,
      public_key: config.public_key,
      private_key: config.decrypted_private_key,
    )
    account_updater_kind = Braintree::WebhookNotification::Kind::AccountUpdaterDailyReport
    webhook_notification = gateway.webhook_notification.parse(
      bt_signature,
      bt_payload
    )
    if webhook_notification.kind == account_updater_kind
      report_url = webhook_notification.account_updater_daily_report.report_url
      Rails.logger.info("Triggering Braintree Account Updater worker for csv: #{report_url}")
      Braintree::AccountUpdaterWorker.perform_async(report_url)
    else
      message = "Not configured to process Braintree webhook of type #{webhook_notification.kind}"
      Airbrake.notify(message, webhook_notification.inspect)
    end
  end
end
