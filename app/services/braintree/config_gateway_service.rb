# frozen_string_literal: true

class Braintree::ConfigGatewayService
  include ActiveModel::Model

  attr_reader :config, :gateway

  validates :config, presence: true
  validate :config_validations

  def initialize(config)
    @config = config
    @gateway = Braintree::Gateway.new(
      environment: ENV["RAILS_ENV"] == "production" ? :production : :sandbox,
      merchant_id: config.merchant_id,
      public_key: config.public_key,
      private_key: config.decrypted_private_key,
    )
    valid?
  end

  def payment_method
    return @payment_method unless @payment_method.nil?

    @payment_method = gateway.payment_method
  end

  def create_payment_method(payment_method_data)
    payment_method.create(payment_method_data)
  end

  def update_payment_method(bt_token, payment_method_data)
    payment_method.update(bt_token, payment_method_data)
  end

  def find_payment_method(bt_token)
    payment_method.find(bt_token)
  end

  def delete_payment_method(bt_token)
    payment_method.delete(bt_token)
  end

  def generate_client_token
    gateway.client_token.generate(merchant_account_id: config.merchant_account_id)
  end

  def transaction
    return @transaction unless @transaction.nil?

    @transaction = gateway.transaction
  end

  delegate :sale, to: :transaction

  delegate :refund, to: :transaction

  delegate :void, to: :transaction

  def find_transaction(transaction_id)
    return @transaction_result unless @transaction_result.nil?

    @transaction_result =
      begin
        transaction.find(transaction_id)
      rescue Braintree::NotFoundError
        nil
      end
  end

  def transaction_in_config_gateway?(transaction_id)
    find_transaction(transaction_id).present?
  end

  def customer
    return @customer unless @customer.nil?

    @customer = gateway.customer
  end

  def create_customer(customer_data)
    customer.create(customer_data)
  end

  def find_customer(customer_id)
    return @customer_result unless @customer_result.nil?

    @customer_result =
      begin
        customer.find(customer_id)
      rescue Braintree::NotFoundError
        nil
      end
  end

  def customer_in_config_gateway?(customer_id)
    find_customer(customer_id).present?
  end

  def config_validations
    errors.add(:config, "merchant_id but be present") unless merchant_id_present?
    errors.add(:config, "public key must be present") unless public_key_present?
    errors.add(:config, "decrypted private key must be present") unless private_key_present?
  end

  def merchant_id_present?
    config.merchant_id.present?
  end

  def public_key_present?
    config.public_key.present?
  end

  def private_key_present?
    config.decrypted_private_key.present?
  end
end
