class Braintree::AccountUpdaterService
  def process(token, update_type, cc_last_4_digits, expiration_date_str)
    stored_credit_card = StoredCreditCard.find_by(bt_token: token)

    unless stored_credit_card
      create_log_record(
        token,
        update_type,
        cc_last_4_digits,
        expiration_date_str,
        "error",
        "Stored credit card record not found"
      )
      return
    end

    unless expected_update_type(update_type)
      create_log_record(
        token,
        update_type,
        cc_last_4_digits,
        expiration_date_str,
        "error",
        "Not configured to process this update_type"
      )
      return
    end

    expiration_date = parse_date(expiration_date_str)
    stored_credit_card.update(
      last_four: cc_last_4_digits,
      expiration_month: expiration_date.month,
      expiration_year: expiration_date.year,
      updated_at: Time.now
    )
    create_log_record(
      token,
      update_type,
      cc_last_4_digits,
      expiration_date_str,
      "success",
      "Credit card details updated"
    )
  end

  private

  def expected_update_type(update_type)
    ["New expiry date", "New account number"].include?(update_type)
  end

  def create_log_record(token, update_type, cc_last_4_digits, expiration_date, status, message)
    BraintreeAccountUpdaterLog.create(
      token: token,
      update_type: update_type,
      cc_last_four_digits: cc_last_4_digits,
      expiration_date: expiration_date,
      status: status,
      message: message
    )
  end

  def parse_date(date_str)
    begin
      Date.strptime(date_str, "%b-%y")
    rescue ArgumentError
      Date.strptime(date_str, "%m/%y")
    end
  end
end
