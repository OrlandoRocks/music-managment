class Braintree::DisputeService
  attr_reader :dispute

  def initialize(dispute = nil)
    @dispute = dispute
  end

  def accept
    begin
      result = gateway.dispute.accept(dispute.dispute_identifier)
      response_hash(result)
    rescue *exceptions => e
      handle_exception(e)
    end
  end

  def upload_document(file_evidence)
    begin
      result = gateway.document_upload.create(
        {
          kind: Braintree::DocumentUpload::Kind::EvidenceDocument,
          file: file_evidence
        }
      )
      response_hash(result, true)
    rescue *exceptions => e
      handle_exception(e)
    end
  end

  def attach_file_evidence(document_id)
    begin
      result = gateway.dispute.add_file_evidence(
        dispute.dispute_identifier,
        document_id
      )
      response_hash(result)
    rescue *exceptions => e
      handle_exception(e)
    end
  end

  def attach_text_evidence(text_evidence)
    begin
      result = gateway.dispute.add_text_evidence(
        dispute.dispute_identifier,
        text_evidence
      )
      response_hash(result)
    rescue *exceptions => e
      handle_exception(e)
    end
  end

  def finalize
    begin
      result = gateway.dispute.finalize(dispute.dispute_identifier)
      response_hash(result)
    rescue *exceptions => e
      handle_exception(e)
    end
  end

  private

  def gateway
    @gateway ||= Braintree::ConfigGatewayService.new(payin_provider_config).gateway
  end

  def payin_provider_config
    dispute.source.payin_provider_config
  end

  def response_hash(result, is_document_upload = nil)
    if result.success?
      return { success: true, document_id: result.document_upload.id, errors: [] } if is_document_upload

      { success: true, errors: [] }
    else
      { success: false, errors: result.errors.map(&:message) }
    end
  end

  def handle_exception(exception)
    Tunecore::Airbrake.notify(
      exception.message,
      {
        dispute: dispute,
        person: dispute.person
      }
    )
    { success: false, errors: [exception.message] }
  end

  def exceptions
    [
      Braintree::NotFoundError,
      Braintree::AuthenticationError,
      Braintree::ServerError,
      Braintree::UnexpectedError,
      Net::ReadTimeout,
      Braintree::ValidationsFailed,
      ArgumentError
    ]
  end
end
