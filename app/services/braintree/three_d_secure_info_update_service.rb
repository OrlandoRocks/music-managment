class Braintree::ThreeDSecureInfoUpdateService
  def process(braintree_transaction_id)
    braintree_transaction = BraintreeTransaction.find(braintree_transaction_id)
    person = braintree_transaction.person
    transaction = person.config_gateway_service.find_transaction(braintree_transaction.transaction_id)
    return if transaction.three_d_secure_info.nil?

    info = transaction.three_d_secure_info
    liability_shifted_at = DateTime.now if info.liability_shifted
    braintree_transaction.update(
      liability_shifted: info.liability_shifted,
      liability_shifted_at: liability_shifted_at,
      ds_transaction_id: info.ds_transaction_id
    )
  end
end
