# frozen_string_literal: true

class PlanService
  include AfterCommitEverywhere

  def self.adjust_cart_plan!(person, plan_id)
    new(person).adjust_cart_plan!(plan_id)
  end

  def initialize(person)
    @person = person
  end

  attr_reader :person

  delegate :renewal_expires_at, :plan, to: :person_plan

  def person_plan
    @person_plan ||= person.person_plan
  end

  def invoice_id
    @invoice_id ||= person.purchases.plans.paid.maximum(:invoice_id)
  end

  def purchased_for
    @purchased_for ||= calculate_purchased_for
  end

  def cost_cents
    @cost_cents ||= calculate_cost_cents
  end

  def duration
    1.year
  end

  def adjust_cart_plan!(plan_id)
    status, result =
      RedlockService.with_lock("plan_for_#{person.id}") do
      ApplicationRecord.transaction(requires_new: true) do
        after_commit { return :success }

        after_rollback { return :failure }

        plan = Plan.find(plan_id)
        product = plan.products.find_by!(country_website: person.country_website)
        person.cart_plan_purchase.destroy! if person.cart_plan_purchase.present?
        if new_artist_adjustment?(plan_id) && person.has_unaccepted_split_invites?
          person.add_splits_collaborator_to_cart!
        end

        Product.add_to_cart(person, product, product) # we're using the product as the related_at
      end
    end

    return result if status == :success

    :failure
  end

  private

  def calculate_purchased_for
    person.purchases.current_plan(invoice_id).first.sub_total_excl_vat
  end

  def calculate_cost_cents
    Integer(person.purchases.current_plan(invoice_id).first.product.price * 100)
  end

  def new_artist_adjustment?(plan_id)
    Integer(plan_id, exception: false) == Plan::NEW_ARTIST
  end
end
