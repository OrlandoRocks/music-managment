class InvoiceLogService
  # all the safety operators and other like measures are because this will get logged in rescues and because we're
  # not sure what caused the error we won't be sure what information will be available
  INITIALIZE_OPTIONS_KEYS = [
    :person,
    :invoice,
    :purchase,
    :braintree_transaction_id,
    :paypal_transaction_id,
    :batch_transaction_id,
    :payment_batch_id,
    :stored_paypal_account_id,
    :renewal_id,
    :message,
    :current_method_name,
    :caller_method_path
  ]
  def self.log(options)
    new(options).tap(&:log)
  end

  def initialize(options)
    @has_values                 = INITIALIZE_OPTIONS_KEYS.any? { |key| !options[key].nil? }
    @no_logs_created            = true

    @person                     = options[:person]
    @invoice                    = options[:invoice]
    @purchase                   = options[:purchase]

    @braintree_transaction_id   = options[:braintree_transaction_id]
    @payments_os_transaction_id = options[:payments_os_transaction_id]
    @paypal_transaction_id      = options[:paypal_transaction_id]
    @batch_transaction_id       = options[:batch_transaction_id]

    @payment_batch_id           = options[:payment_batch_id]
    @stored_paypal_account_id   = options[:stored_paypal_account_id]
    @renewal_id                 = options[:renewal_id]

    @message                    = options[:message]
    @current_method_name        = options[:current_method_name]
    @caller_method_path         = options[:caller_method_path]

    @created_at                 = Date.current
    @updated_at                 = Date.current
  end

  def has_values?
    @has_values
  end

  def no_logs_created?
    @no_logs_created
  end

  def invoice_present?
    @invoice.present? && @invoice.is_a?(Invoice)
  end

  def purchase_present?
    @purchase.present? && @purchase.is_a?(Purchase)
  end

  def log
    create_transaction_logs     if invoice_present?
    create_purchase_logs        if invoice_present? || purchase_present?
    create_log(default_options) if !invoice_present? && no_logs_created?
    create_log(default_options) if no_logs_created?
  end

  private

  def create_transaction_logs
    create_braintree_transaction_logs
    create_paypal_transaction_logs
    create_batch_transaction_logs
  end

  # this isn't very dry but doing it dynamically is messier than it's worth, I think
  # in particular with how essential it is that none of this breaks because this service will largely be called
  # within rescues
  def create_braintree_transaction_logs
    return unless @braintree_transaction_id.nil?

    @invoice.braintree_transactions.each do |transaction|
      options = default_options
      options[:braintree_transaction_id] = transaction.id

      create_log(options)
    end
  end

  def create_paypal_transaction_logs
    return unless @paypal_transaction_id.nil?

    @invoice.paypal_transactions.each do |transaction|
      options = default_options
      options[:paypal_transaction_id] = transaction.id

      create_log(options)
    end
  end

  def create_batch_transaction_logs
    return unless @batch_transaction_id.nil?

    @invoice.batch_transactions.each do |transaction|
      options = default_options
      options[:batch_transaction_id] = transaction.id

      create_log(options)
    end
  end

  def create_purchase_logs
    purchase_array =
      if invoice_present?
        @invoice.purchases
      elsif purchase_present?
        [@purchase]
      else
        []
      end
    purchase_array.each do |purchase|
      options = default_options
      options[:purchase_id]                               = purchase.id
      options[:purchase_created_at]                       = purchase.created_at
      options[:purchase_product_id]                       = purchase.product_id
      options[:purchase_cost_cents]                       = purchase.cost_cents
      options[:purchase_discount_cents]                   = purchase.discount_cents
      options[:purchase_currency]                         = purchase.currency
      options[:purchase_paid_at]                          = purchase.paid_at
      options[:purchase_salepoint_id]                     = purchase.salepoint_id
      options[:purchase_no_purchase_album_id]             = purchase.no_purchase_album_id
      options[:purchase_related_id]                       = purchase.related_id
      options[:purchase_related_type]                     = purchase.related_type
      options[:purchase_targeted_product_id]              = purchase.targeted_product_id
      options[:purchase_price_adjustment_histories_count] = purchase.price_adjustment_histories_count
      options[:purchase_vat_amount_cents]                 = purchase.vat_amount_cents
      options[:purchase_status]                           = purchase.status

      create_log(options)
    end
  end

  def create_log(options)
    return unless has_values?

    InvoiceLog.create(
      person_id: options[:person_id],
      batch_transaction_id: options[:batch_transaction_id],
      payment_batch_id: options[:payment_batch_id],
      braintree_transaction_id: options[:braintree_transaction_id],
      payments_os_transaction_id: options[:payments_os_transaction_id],
      paypal_transaction_id: options[:paypal_transaction_id],
      stored_paypal_account_id: options[:stored_paypal_account_id],
      renewal_id: options[:renewal_id],
      current_method_name: options[:current_method_name],
      caller_method_path: options[:caller_method_path],
      message: options[:message],
      invoice_id: options[:invoice_id],
      purchase_id: options[:purchase_id],
      invoice_created_at: options[:invoice_created_at],
      invoice_final_settlement_amount_cents: options[:invoice_final_settlement_amount_cents],
      invoice_currency: options[:invoice_currency],
      invoice_batch_status: options[:invoice_batch_status],
      invoice_settled_at: options[:invoice_settled_at],
      invoice_vat_amount_cents: options[:invoice_vat_amount_cents],
      invoice_foreign_exchange_pegged_rate_id: options[:invoice_foreign_exchange_pegged_rate_id],
      purchase_created_at: options[:purchase_created_at],
      purchase_product_id: options[:purchase_product_id],
      purchase_cost_cents: options[:purchase_cost_cents],
      purchase_discount_cents: options[:purchase_discount_cents],
      purchase_currency: options[:purchase_currency],
      purchase_paid_at: options[:purchase_paid_at],
      purchase_salepoint_id: options[:purchase_salepoint_id],
      purchase_no_purchase_album_id: options[:purchase_no_purchase_album_id],
      purchase_related_id: options[:purchase_related_id],
      purchase_related_type: options[:purchase_related_type],
      purchase_targeted_product_id: options[:purchase_targeted_product_id],
      purchase_price_adjustment_histories_count: options[:purchase_price_adjustment_histories_count],
      purchase_vat_amount_cents: options[:purchase_vat_amount_cents],
      purchase_status: options[:purchase_status],
      created_at: options[:created_at],
      updated_at: options[:updated_at]
    )
    @no_logs_created = false if @no_logs_created
  end

  def default_options
    {
      person_id: @person&.id,
      batch_transaction_id: @batch_transaction_id,
      payment_batch_id: @payment_batch_id,
      braintree_transaction_id: @braintree_transaction_id,
      payments_os_transaction_id: @payments_os_transaction_id,
      paypal_transaction_id: @paypal_transaction_id,
      stored_paypal_account_id: @stored_paypal_account_id,
      renewal_id: @renewal_id,
      current_method_name: @current_method_name,
      caller_method_path: @caller_method_path,
      message: @message,
      invoice_id: @invoice&.id,
      purchase_id: nil,
      invoice_created_at: @invoice&.created_at,
      invoice_final_settlement_amount_cents: @invoice&.final_settlement_amount_cents,
      invoice_currency: @invoice&.currency,
      invoice_batch_status: @invoice&.batch_status,
      invoice_settled_at: @invoice&.settled_at,
      invoice_vat_amount_cents: @invoice&.vat_amount_cents,
      invoice_foreign_exchange_pegged_rate_id: @invoice&.foreign_exchange_pegged_rate_id,
      purchase_created_at: nil,
      purchase_product_id: nil,
      purchase_cost_cents: nil,
      purchase_discount_cents: nil,
      purchase_currency: nil,
      purchase_paid_at: nil,
      purchase_salepoint_id: nil,
      purchase_no_purchase_album_id: nil,
      purchase_related_id: nil,
      purchase_related_type: nil,
      purchase_targeted_product_id: nil,
      purchase_price_adjustment_histories_count: nil,
      purchase_vat_amount_cents: nil,
      purchase_status: nil,
      created_at: @created_at,
      updated_at: @updated_at
    }
  end
end
