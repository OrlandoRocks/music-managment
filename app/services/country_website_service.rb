class CountryWebsiteService
  attr_reader :user

  NO_PAYPAL_COUNTRY_WEBSITE_IDS = [CountryWebsite::INDIA].freeze
  # hardcoding this because the only alternative we can think of is
  # CountryWebsite.find(CountryWebsite::INDIA).country
  # which feels worse. A better way of structuring these rules would be nice though
  PHONE_REQUIRED_ISO_CODES = [CountryWebsite.find(CountryWebsite::INDIA).country].freeze

  def self.paypal_available?(user)
    new(user).paypal_available?
  end

  def initialize(user)
    @user = user
  end

  def paypal_available?
    !NO_PAYPAL_COUNTRY_WEBSITE_IDS.include?(user.country_website_id)
  end
end
