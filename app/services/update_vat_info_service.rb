class UpdateVatInfoService
  def initialize(options)
    @person = options[:person]
    @country_code = options[:country_code]
  end

  def process
    return {} unless FeatureFlipper.show_feature?(:vat_tax, @person)

    new_country = Country.find_by(iso_code: @country_code)
    is_country_changed = new_country ? new_country.name != @person.country_name_untranslated : false
    return {} unless is_country_changed

    old_country = Country.find_by(name: @person.country_name_untranslated)
    is_old_country_in_eu = old_country ? Country::EU_COUNTRIES.include?(old_country.iso_code) : false
    is_new_country_in_eu = new_country ? Country::EU_COUNTRIES.include?(@country_code) : false
    show_vat_warning = is_old_country_in_eu && !is_new_country_in_eu
    redirect_to_vat_form = is_new_country_in_eu
    # delete vat_details on country change
    @person.vat_information&.invalidate!
    { show_vat_warning: show_vat_warning, redirect_to_vat_form: redirect_to_vat_form }
  end
end
