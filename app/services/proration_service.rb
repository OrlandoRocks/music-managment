# frozen_string_literal: true

# ProrationService applies discounts to purchase collections according to the determined scenario.
#
# Note: overwrites existing discounts!
# Additional, unrelated discounts must be applied after this runs in the cart flow.
#
class ProrationService
  class ProrationDiscountError < StandardError; end

  ALBUM_RENEWAL_TYPE = "Album"

  PRORATION_DISCOUNT_TYPE_ERROR = "Discount value type is invalid."

  SCENARIOS = %i[
    first_plan
    multi_to_multi
    only_adding_artists
    upgrading_plan
  ].freeze

  def self.apply_proration_discounts!(person)
    new(person).apply_proration_discounts!
  end

  def initialize(person)
    @person    = person
    @purchases = person.purchases.unpaid
  end

  attr_reader :person, :purchases

  delegate :person_plan, to: :plan_service

  def apply_proration_discounts!
    return if proratable_purchases.empty?
    return unless scenario.in?(SCENARIOS)

    case scenario
    when :first_plan
      discount_greedily!
    when :multi_to_multi, :only_adding_artists, :upgrading_plan
      discount_by_percentage!
    end

    nil
  end

  private

  def scenario
    @scenario ||=
      if first_plan?
        :first_plan
      elsif only_adding_artists?
        :only_adding_artists
      elsif upgrading_plan?
        :upgrading_plan
      elsif multi_to_multi?
        :multi_to_multi
      end
  end

  def proration_discount
    @proration_discount ||=
      case scenario
      when :first_plan
        first_plan_discount
      when :only_adding_artists
        artist_discount
      when :upgrading_plan, :multi_to_multi
        upgrade_discount
      end
  end

  def proratable_purchases
    @proratable_purchases ||=
      begin
        result =
          case scenario
          when :first_plan          then plan_and_all_artist_purchases
          when :only_adding_artists then all_artist_purchases
          when :upgrading_plan      then pending_plan_purchase
          when :multi_to_multi      then nil # TODO
          end

        Array.wrap(result)
      end
  end

  #
  # Scenario checks
  #

  def first_plan?
    person_plan.blank?
  end

  def upgrading_plan?
    return false if first_plan?
    return false if multi_to_multi?
    return false if person_plan.expired?

    pending_plan_purchase.present?
  end

  def only_adding_artists?
    pending_plan_purchase.nil? && person.can_do?(:buy_additional_artists)
  end

  # TODO: This upgrade scenario is not yet implemented.
  def multi_to_multi?
    pending_plan_purchase.present? && false # Second condition TBD
  end

  #
  # Discount types
  #

  # Sum the value of remaining release renewals until total_cost is <= sum
  def first_plan_discount
    active_renewals.reduce(0) do |discount, renewal|
      break discount if discountable_cost <= discount

      renewal_history   = renewal.renewal_history.last
      prorated_duration = renewal_history.expires_at - plan_expiration
      next discount if prorated_duration.negative?

      renewal_duration        = renewal_history.expires_at - renewal_history.starts_at
      renewal_cost            = renewal_history.purchase.sub_total_excl_vat
      remaining_renewal_value = (proration_fraction(prorated_duration, renewal_duration) *
                                renewal_cost).round

      discount + remaining_renewal_value
    end
  end

  def artist_discount
    return 0.0 if unused_duration.negative?

    1 - proration_fraction(unused_duration, plan_service.duration)
  end

  def upgrade_discount
    return 0.0 if unused_duration.negative?

    proration_fraction(unused_duration, plan_service.duration)
  end

  #
  # Discount algorithms
  #

  # Apply as much discount as possible to each purchase, in purchase.cost_cents DESC order.
  # Don't break early to ensure that old discounts are wiped out.
  def discount_greedily!
    raise ProrationDiscountError, PRORATION_DISCOUNT_TYPE_ERROR unless proration_discount.integer?

    proratable_purchases
      .sort { |a, b| b.cost_cents - a.cost_cents }
      .reduce(proration_discount) do |remaining_discount, purchase|
        discount_cents = [purchase.cost_cents, remaining_discount].min
        apply_discount(purchase, discount_cents)
        TcVat::Calculator.new(purchase).perform!(purchase.cost_cents, purchase.discount_cents)

        remaining_discount - discount_cents
      end

    nil
  end

  def discount_by_percentage!
    raise ProrationDiscountError, PRORATION_DISCOUNT_TYPE_ERROR if proration_discount.integer?

    proratable_purchases.each do |purchase|
      proratable_amount = purchase.is_plan? ? plan_service.cost_cents : purchase.cost_cents
      discount_cents    = (proratable_amount * proration_discount).round

      apply_discount(purchase, discount_cents)
      TcVat::Calculator.new(purchase).perform!(purchase.cost_cents, purchase.discount_cents)
    end

    nil
  end

  #
  # Purchase collections
  #

  def pending_plan_purchase
    @pending_plan_purchase ||= purchases.find(&:plan_product_id?)
  end

  def plan_and_all_artist_purchases
    purchases.select(&:plan_related_product_id?)
  end

  def all_artist_purchases
    purchases.select(&:additional_artist_product_id?)
  end

  #
  # TODO:
  # replacement_artists_count   = plan_addons.active.additional_artists.count
  # proratable_artist_purchases = purchases.additional_artists.take(replacement_artists_count)
  # result                      = [pending_plan_purchase, proratable_artist_purchases]
  #
  # def plan_and_replacement_artist_purchases
  # end

  #
  # Helpers
  #

  def apply_discount(purchase, discount_cents)
    return if purchase.discount_cents > discount_cents
    return if discount_cents.zero?

    purchase.update(
      {
        discount_cents: discount_cents,
        discount_reason: discount_reason,
        targeted_product_id: nil,
      }
    )
  end

  def plan_service
    @plan_service ||= PlanService.new(person)
  end

  def plan_expiration
    @plan_expiration ||= DateTime.now.beginning_of_day + 1.year
  end

  def discountable_cost
    @discountable_cost ||= proratable_purchases.sum(&:cost_cents)
  end

  def active_renewals
    @active_renewals ||= person
                         .renewals
                         .active
                         .takendown(false)
                         .includes(:renewal_items, renewal_history: [:purchase])
                         .where(renewal_items: { related_type: ALBUM_RENEWAL_TYPE })
  end

  def unused_duration
    @unused_duration ||= plan_service.renewal_expires_at - DateTime.now.beginning_of_day
  end

  def proration_fraction(unused_duration, total_duration)
    to_days_f(unused_duration) / to_days_f(total_duration)
  end

  def to_days_f(duration)
    Float((duration / 1.day).round)
  end

  def discount_reason
    person_plan.present? ? Purchase::PLAN_PRORATION : Purchase::PRE_PLAN_PRORATION
  end
end
