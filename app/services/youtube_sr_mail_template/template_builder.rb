class YoutubeSrMailTemplate::TemplateBuilder
  def self.build(person_id, template_name, message_data)
    new(person_id, template_name, message_data)
  end

  attr_reader :message_data, :person

  def initialize(person_id, template_name, message_data)
    @person = Person.find(person_id)
    @template = YoutubeSrMailTemplate.template_for_country_website_id(template_name, @person.country_website_id)
    song_ids = message_data.pluck(:song_id)
    songs = Song.find(song_ids)

    @message_data =
      message_data.map do |message_datum|
        song = songs.find { |inner_song| inner_song.id == message_datum[:song_id].to_i }

        {
          song: song,
          custom_fields: message_datum[:custom_fields].values
        }
      end
  end

  def to_email
    person.email
  end

  def subject
    template.evaluate_attribute(:subject, message_data)
  end

  def header
    template.evaluate_attribute(:header, message_data)
  end

  def body
    Mustache.render(
      template.body,
      {
        data: message_data
      }
    )
  end

  delegate :footer, to: :template

  def country
    person.country_domain
  end

  private

  attr_reader :template
end
