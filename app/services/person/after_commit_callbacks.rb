module Person::AfterCommitCallbacks
  def self.after_commit(person)
    return unless person.is_verified?

    send_to_believe(person)
    add_to_applicable_targeted_offer(person)
    send_to_zendesk(person)
  end

  def self.send_to_believe(person)
    unless person.saved_change_to_is_verified? ||
           Believe::Person.contains_believe_attributes?(person.saved_changes.keys)

      return
    end

    if DEFER_BELIEVE
      Believe::PersonWorker.perform_async(person.id)
    else
      Believe::Person.add_or_update(person.id)
    end
  end

  def self.add_to_applicable_targeted_offer(person)
    return unless person.saved_change_to_attribute?(:is_verified, from: 0, to: 1)

    offers = TargetedOffer
             .offer_type_country_targeted
             .active
             .where(country_website_id: person.country_website_id)

    offers.each do |offer|
      offer.targeted_people.find_or_create_by(person_id: person.id)
    end
  end

  def self.send_to_zendesk(person)
    unless (person.saved_change_to_is_verified? || person.is_verified?) &&
           Zendesk::Person.contains_zendesk_attributes?(person.saved_changes.keys)

      return
    end

    Zendesk::Person.create_apilogger(person.id, person.saved_changes)
  end
end
