# frozen_string_literal: true

class Person::CountryBackfillService
  INVALID_COUNTRIES = [nil, "", "Unidentified", 233, "我们"]
  attr_accessor :invalid_country_people, :kyc_people, :only_backfill_kyc, :limit, :kyc_counter, :tfa_counter, :ip_counter, :credit_card_counter, :default_counter

  def self.perform_backfill(backfill_method)
    case backfill_method
    when "all"
      new.tap do |service|
        service.backfill_country_audit
        service.backfill_kyc_and_invalid_people
      end
    when "country_audit"
      new.tap(&:backfill_country_audit)
    when "payoneer_kyc"
      new(only_backfill_kyc: true).tap(&:backfill_kyc_and_invalid_people)
    when "kyc_and_invalid"
      new.tap(&:backfill_kyc_and_invalid_people)
    when "invalid"
      new.tap(&:backfill_invalid)
    end
  end

  def initialize(args = {})
    @only_backfill_kyc = args[:only_backfill_kyc]
    @limit = args[:limit]
    @kyc_counter = 0
    @tfa_counter = 0
    @ip_counter = 0
    @credit_card_counter = 0
    @default_counter = 0
  end

  def backfill_country_audit(limit: nil)
    @limit ||= limit
    Rails.logger.info "Beginning Country Audit Backfill"
    source_id = CountryAuditSource.find_by(name: CountryAuditSource::SELF_IDENTIFIED_NAME).id

    Person.where.not(country: INVALID_COUNTRIES).left_outer_joins(:country_audits).where(country_audits: { country_id: nil }).limit(@limit).in_batches do |batch|
      batch.each do |person|
        current_country = Country.search_by_iso_code(person[:country])
        next if current_country.nil?

        write_to_country_audits_table(person, current_country, source_id)
      end
    end

    Rails.logger.info "Finished Country Audit Backfill"
  end

  def backfill_kyc_and_invalid_people
    collect_kyc_people
    Rails.logger.info "Beginning Payoneer KYC backfill - #{@kyc_people.length} people to backfill"
    backfill_via_kyc
    Rails.logger.info "Finished Payoneer KYC backfill - #{kyc_counter} people updated"
    return if @only_backfill_kyc

    backfill_invalid
  end

  def backfill_via_kyc
    @kyc_people.find_each do |person|
      PayoutProvider::VerifyPayeeComplianceInfo.new(person).call
    end
  end

  def backfill_invalid
    @invalid_country_people = people_with_invalid_country
    Rails.logger.info "Beginning invalid country backfill - #{@invalid_country_people.length} people to backfill"

    backfill_via_two_factor_auth
    backfill_via_ip
    backfill_via_credit_card
    backfill_default_to_country_us

    Rails.logger.info "Finished invalid country backfill!\n
    TFA: #{@tfa_counter}\n
    IP: #{@ip_counter}\n
    CC: #{@credit_card_counter}\n
    Default: #{@default_counter}"
  end

  def collect_kyc_people
    # only people who do not already have a country_audit with Payoneer KYC source
    @kyc_people = Person.for_country_website([1, 8]).joins(:payout_providers).where(payout_providers: { name: "payoneer", provider_status: "approved" })
  end

  def people_with_invalid_country
    Person.where(country: INVALID_COUNTRIES)
  end

  def backfill_via_two_factor_auth
    unchanged_people = []

    @invalid_country_people.each do |person|
      unchanged_people << person && next if person.two_factor_auth.nil?

      tfa_country = find_tfa_country(person)
      current_country_iso = person[:country]
      unchanged_people << person && next if tfa_country.nil? || same_country?(tfa_country.iso_code, current_country_iso)

      update_person_and_write_to_audits(
        audit_source: CountryAuditSource::TWO_FACTOR_AUTH_NAME,
        counter: "tfa_counter",
        person: person,
        updated_country: tfa_country
      )
    end

    @invalid_country_people = unchanged_people
    Rails.logger.info "TFA backfill complete. people remaining #{@invalid_country_people.length}"
  end

  def backfill_via_ip
    unchanged_people = []
    @invalid_country_people.each do |person|
      ip_country = find_ip_country(person)
      current_country_iso = person[:country]
      unchanged_people << person && next if ip_country.nil? || same_country?(ip_country.iso_code, current_country_iso)

      update_person_and_write_to_audits(
        audit_source: CountryAuditSource::IP_ADDRESS_NAME,
        counter: "ip_counter",
        person: person,
        updated_country: ip_country
      )
    end
    @invalid_country_people = unchanged_people
    Rails.logger.info "IP backfill complete. people remaining #{@invalid_country_people.length}"
  end

  def backfill_via_credit_card
    unchanged_people = []
    @invalid_country_people.each do |person|
      stored_card_country = find_credit_card_country(person)
      current_country_iso = person[:country]
      if stored_card_country.nil? || same_country?(current_country_iso, stored_card_country.iso_code)
        unchanged_people << person && next
      end

      update_person_and_write_to_audits(
        audit_source: CountryAuditSource::STORED_CREDIT_CARD_NAME,
        counter: "credit_card_counter",
        person: person,
        updated_country: stored_card_country
      )
    end
    @invalid_country_people = unchanged_people
    Rails.logger.info "Credit Card backfill complete. people remaining #{@invalid_country_people.length}"
  end

  def backfill_default_to_country_us
    us_country = Country.search_by_iso_code("US")

    @invalid_country_people.each do |person|
      update_person_and_write_to_audits(
        audit_source: CountryAuditSource::DEFAULT_TO_US_NAME,
        counter: "default_counter",
        person: person,
        updated_country: us_country
      )
    end
  end

  def find_tfa_country(person)
    tfa_country_phone_code = person.two_factor_auth&.country_code
    return if tfa_country_phone_code.nil?

    tfa_countries = Country.find_by_country_phone_code(tfa_country_phone_code)
    return unless one_valid_country?(tfa_countries)

    tfa_countries.first
  end

  def find_ip_country(person)
    person_login_countries = person.login_events.map(&:country).compact.uniq
    return unless one_valid_country?(person_login_countries)

    Country.find_by_name_or_iso_code(person_login_countries.first).first # handles UK -> GB iso mismatch
  end

  def find_credit_card_country(person)
    credit_card_countries = person.stored_credit_cards.map(&:country).compact.uniq
    return unless one_valid_country?(credit_card_countries)

    Country.find_by_name_or_iso_code(credit_card_countries.first).first # handles UK -> GB iso mismatch
  end

  def same_country?(current_country, next_country)
    current_country == next_country
  end

  def write_to_country_audits_table(person, country, source_id)
    CountryAudit.find_or_create_by(person_id: person.id, country_id: country.id, country_audit_source_id: source_id)
  end

  def one_valid_country?(countries)
    countries.length == 1 && countries.first != "--"
  end

  def update_person_and_write_to_audits(person:, updated_country:, audit_source:, counter:)
    ApplicationRecord.transaction do
      person.country_audit_source = audit_source

      # because this backfill updates thousands of very old accounts,
      # some fail recent validations for name.length and email being downcase.
      # For this reason we need to use #update_attribute instead of #update
      updated = person.update_attribute(:country, updated_country.iso_code)

      send((counter + "="), send(counter) + 1) if updated
    end
  end
end
