# frozen_string_literal: true

class Person::ActiveSocialSubscriptionPeopleService < Person::SubscriptionBaseService
  def query
    super.with_active_social_subscription_with_termination_date
  end
end
