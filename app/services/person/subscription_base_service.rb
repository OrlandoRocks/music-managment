# frozen_string_literal: true

class Person::SubscriptionBaseService
  attr_accessor :relation,
                :last_logged_in_before,
                :last_logged_in_after,
                :corporate_entity_id,
                :limit,
                :offset

  def initialize(params = {}, relation = Person.all)
    @relation = relation
    @last_logged_in_before = params[:last_logged_in_before]
    # The following can default to `nil` after upgrading to ruby 2.7+
    @last_logged_in_after = params[:last_logged_in_after] || Time.at(0)
    @corporate_entity_id =
      params[:corporate_entity_id] ||
      CorporateEntity.where(name: CorporateEntity::BI_LUXEMBOURG_NAME).pluck(:id)
    @limit = params[:limit]
    @offset = params[:offset]
  end

  def call
    query.all
  end

  def query
    relation
      .then { |r| last_logged_in_between(r) }
      .then { |r| select_attrs(r) }
      .then { |r| can_auto_renew(r) }
      .without_completed_compliance_info
      .without_sent_auto_renewal_email
      .then { |r| with_optional_conditions(r) }
      .where(corporate_entity_id: corporate_entity_id)
      .group("people.id")
  end

  private

  def select_attrs(relation)
    relation.select("people.id")
  end

  def with_optional_conditions(relation)
    relation
      .then { |q| limit ? q.limit(limit) : q }
      .then { |q| offset ? q.offset(offset) : q }
  end

  def last_logged_in_between(relation)
    return relation unless last_logged_in_before || last_logged_in_after

    date_range = last_logged_in_after..last_logged_in_before
    relation
      .joins(:login_events)
      .where(<<~SQL)
        login_events.created_at = (
          SELECT MAX(login_events.created_at)
          FROM login_events WHERE login_events.person_id = people.id
        )
      SQL
      .where(login_events: { created_at: date_range })
  end

  def can_auto_renew(relation)
    relation
      .left_outer_joins(person_preference: [:preferred_credit_card])
      .left_outer_joins(:person_balance)
      .where(<<~SQL)
        (
          person_preferences.id IS NOT NULL
          AND person_preferences.do_not_autorenew = 0
          AND
          (
            (
              person_preferences.preferred_payment_type = 'CreditCard'
              AND person_preferences.preferred_credit_card_id IS NOT NULL
              AND stored_credit_cards.deleted_at IS NULL
              AND
              (
                stored_credit_cards.expiration_year > YEAR(CURDATE())
                OR
                (
                  stored_credit_cards.expiration_month >= MONTH(CURDATE())
                  AND stored_credit_cards.expiration_year = YEAR(CURDATE())
                )
              )
            )
            OR
            (
              person_preferences.preferred_payment_type = "PayPal"
              AND person_preferences.preferred_paypal_account_id IS NOT NULL
            )
            OR
            (
              person_preferences.pay_with_balance = 1
              AND person_balances.balance > 0
            )
          )
        )
        OR person_preferences.id IS NULL
      SQL
  end
end
