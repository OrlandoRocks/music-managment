# frozen_string_literal: true

class Person::AutoRenewalPeopleService < Person::SubscriptionBaseService
  def query
    super.then { |r| with_upcoming_renewals(r) }
  end

  private

  def with_upcoming_renewals(r)
    r.joins(:renewals).merge(Renewal.first_expiration_after_date(Time.now.to_date))
  end
end
