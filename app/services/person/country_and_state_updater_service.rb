# frozen_string_literal: true

class Person::CountryAndStateUpdaterService
  attr_accessor :person

  def initialize(person)
    @person = person
    update_location
  end

  def update_location
    update_state if person.state_changed?
    update_city if person.city_changed?
    update_country_state if person.country_state_id_changed?
    update_country_state_city if person.country_state_city_id_changed?
    update_country if person.country_changed?
    clear_country_state_columns unless person.country_state_country?
  end

  def update_state
    return unless person.country_state_country?
    return if !new_country_state || (person.country_state == new_country_state)

    person.country_state = new_country_state
  end

  def update_city
    return if !person.country_state_country? || person.country_state.nil?
    return if !new_country_state_city || (person.country_state_city == new_country_state_city)

    person.country_state_city = new_country_state_city
  end

  def update_country
    return unless !country_state_info? && person.country_state_country?

    person.country_state = new_country_state
    person.country_state_city = new_country_state_city
  end

  def update_country_state
    # allows us to set country_state_city to nil
    return if !person.country_state_country? && person.country_state.present?
    return if person.country_state.nil? || (person.country_state.name == person.state_was)

    person.state = person.country_state.name
  end

  def update_country_state_city
    # allows us to set country_state_city to nil
    return if !person.country_state_country? && person.country_state_city.present?
    return if person.country_state_city.nil? || (person.country_state_city.name == person.city_was)

    person.city = person.country_state_city.name
  end

  def clear_country_state_columns
    person.country_state = nil
    person.country_state_city = nil
  end

  def country_state_info?
    person.country_state.present? && person.country_state_city.present?
  end

  def new_country_state
    CountryState.find_by(name: person.state)
  end

  def new_country_state_city
    return unless person.country_state

    CountryStateCity.find_by(name: person.city, state_id: person.country_state.id)
  end
end
