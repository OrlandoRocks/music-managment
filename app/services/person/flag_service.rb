# frozen_string_literal: true

class Person::FlagService
  include AfterCommitEverywhere

  class NoPersonProvidedError < StandardError; end

  class PersonInvalidError < StandardError; end

  class NoFlagProvidedError < StandardError; end

  ERRORS = {
    invalid_flag: {
      error: :invalid_flag,
      message: "Sorry, your flag is invalid"
    },
    flag_creation_failed: {
      error: :flag_creation_failed,
      message: "Sorry, your flag creation has failed you"
    },
    flag_destruction_failed: {
      error: :flag_destruction_failed,
      message: "Sorry, your flag destruction has failed you"
    }
  }

  def self.flag!(options)
    new(options).call(:flag!)
  end

  def self.unflag!(options)
    new(options).call(:unflag!)
  end

  def self.person_flag_exists?(options)
    new(options).call(:person_flag_exists?)
  end

  def initialize(options) # options => :person, :flag_attributes, :flag_reason, :admin_person_id
    @options = options
  end

  def call(action)
    case action
    when :flag!
      flag!
    when :unflag!
      unflag!
    when :person_flag_exists?
      person_flag_exists?
    end
  end

  private

  attr_reader :options

  def person
    return @person unless @person.nil?

    @person = options.fetch(:person) { raise NoPersonProvidedError, options }
    raise PersonInvalidError, options if @person.blank?

    @person
  end

  def flag
    return @flag unless @flag.nil?

    flag_attributes = options.fetch(:flag_attributes) { raise NoFlagProvidedError, options }
    @flag = Flag.find_by(flag_attributes) if flag_attributes.present?
    add_error(ERRORS[:invalid_flag]) if @flag.blank?

    @flag
  end

  def admin_person_id
    return @admin_person_id unless @admin_person_id.nil?

    @admin_person_id = options.fetch(:admin_person_id, 0)
  end

  def flag_reason
    return @flag_reason unless @flag_reason.nil?

    @flag_reason = options[:flag_reason]
  end

  def errors_present?
    person.errors.present?
  end

  def flag!
    return if errors_present?
    return add_error(ERRORS[:flag_creation_failed]) if flag.blank?

    ApplicationRecord.transaction(requires_new: true) do
      after_rollback { add_error(ERRORS[:flag_creation_failed]) }
      after_commit { log_flag("flagged") }

      person.people_flags.create(query_condition)
      create_flag_transition!("add")
    end
  end

  def unflag!
    return unless person_flag_exists? || errors_present?
    return add_error(ERRORS[:flag_destruction_failed]) if flag.blank?

    ApplicationRecord.transaction(requires_new: true) do
      after_rollback { add_error(ERRORS[:flag_destruction_failed]) }
      after_commit { log_flag("unflagged") }

      person.people_flags
            .where(query_condition)
            .order(created_at: :desc)
            .take
            .destroy

      create_flag_transition!("remove")
    end
  end

  def person_flag_exists?
    person.people_flags.where(query_condition).exists?
  end

  def query_condition
    @query_condition ||=
      {
        flag: flag
      }.merge(
        flag_reason.present? ? { flag_reason: flag_reason } : {}
      )
  end

  def create_flag_transition!(add_remove)
    person.flag_transitions.create!(
      updated_by: admin_person_id,
      add_remove: add_remove,
      flag_id: flag.id,
      comment: "Flag: #{flag.name}, Flag action: #{add_remove}",
      flag_reason: flag_reason
    )
  end

  def add_error(error)
    Rails.logger.info "person_id: #{person.id}, #{error}"
    person.errors.add(error[:error], error[:message])
  end

  def log_flag(action)
    Rails.logger.info "Person Id: #{person.id}, #{action} flag_name: #{flag.name}, flag_id: #{flag.id}"
  end
end
