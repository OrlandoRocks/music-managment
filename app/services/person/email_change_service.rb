class Person::EmailChangeService
  attr_reader :person, :original_password, :email, :email_confirmation

  def self.call(person, params)
    new(person, params).call
  end

  def initialize(person, params)
    @person = person
    @original_password = params[:original_password]
    @email = params[:email]
    @email_confirmation = params[:email_confirmation]
  end

  def call
    return unless person.is_password_correct?(original_password)

    if valid_email_change?
      EmailChangeWorker.perform_async(person.id, email)
      "email_reset_success_modal.js"
    elsif email.present?
      "email_reset_failure_modal.js"
    end
  end

  private

  def valid_email_change?
    return false unless email && email_confirmation
    return false unless email.casecmp(email_confirmation).zero?
    return false if person.email == email

    !Person.exists?(email: email)
  end
end
