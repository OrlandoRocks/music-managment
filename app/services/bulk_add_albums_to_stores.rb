class BulkAddAlbumsToStores
  def self.add(store, album)
    new(store, album).tap(&:add)
  end

  attr_reader :store, :albums
  attr_accessor :messages

  def initialize(store, albums)
    @store    = store
    @albums   = albums
    @messages = {}
  end

  def add
    create_salepoints_for_albums
  end

  def create_salepoints_for_albums
    albums.each do |album|
      person = album.person
      product = get_product(person)
      # Don't add stores twice
      if album_already_has_store?(album)
        set_already_taken_message(album)
      else
        salepoint = create_salepoint(album)
        if album.finalized?
          Product.add_to_cart(person, salepoint, product) unless free_to_add
          set_success_message(album)
        else
          set_album_finalize_message(album)
        end
      end
    end
  end

  def get_product(person)
    Product.find(Product.find_products_for_country(person.country_domain, :add_store))
  end

  def album_already_has_store?(album)
    album.salepoints.detect { |salepoint| salepoint.store_id == store.id }
  end

  def set_already_taken_message(album)
    messages[album.id] = "Album #{album.id}-#{album.name} already has store #{store.id}"
  end

  def set_success_message(album)
    messages[album.id] = "Added store #{store.id} to album #{album.id}-#{album.name}"
  end

  def set_album_finalize_message(album)
    messages[album.id] = "album not yet finalized"
  end

  def create_salepoint(album)
    salepoint_attrs = { store: store }
    salepoint_attrs[:has_rights_assignment] = 1 if store.needs_rights_assignment?
    salepoint_attrs[:variable_price_id] = store.default_variable_price.id if store.variable_pricing_required?
    album.salepoints.create(salepoint_attrs)
  end

  def free_to_add
    Store::FREE_TO_ADD_STORES.include?(@store.abbrev)
  end
end
