class Transient::YtsrTrackMonetizationMigrator
  MAX_LIMIT = 500_000
  STORE_ID = Store::YTSR_STORE_ID.freeze
  SALEPOINT_SONG_NEW = "new".freeze
  SALEPOINT_SONG_APPROVED = "approved".freeze
  SALEPOINT_SONG_REJECTED = "rejected".freeze

  attr_reader :limit, :results, :time_now, :insertion_values

  def self.migrate(params = {})
    new(params).migrate
  end

  def initialize(params = {})
    @limit = params[:limit] || 100
  end

  def migrate
    return false if limit > MAX_LIMIT

    @time_now = Time.current

    create_youtube_track_monetizations
  end

  def store_id
    STORE_ID
  end

  private

  def create_youtube_track_monetizations
    fetch

    return false if results.empty?

    execute_insertion

    true
  end

  def fetch
    initial_results = ActiveRecord::Base.connection.execute(query_statement).to_a

    @results = initial_results.present? ? initial_results.uniq { |result| result[0] } : []
  end

  def query_statement
    <<-SQL.strip_heredoc
      SELECT DISTINCT
        `songs`.id AS song_id,
        `people`.id AS person_id,
        `distribution_songs`.`delivery_type` as delivery_type,
        `distribution_songs`.`retry_count` as retry_count,
        `salepoint_songs`.`takedown_at` as takedown_at,
        `distribution_songs`.`updated_at` as distribution_song_updated_at,
        `salepoint_songs`.`updated_at` as salepoint_song_updated_at,
        `ytm_ineligible_songs`.`id` as ineligibile,
        `ytm_blocked_songs`.`id` as blocked,
        `distribution_songs`.`state` as distribution_song_state,
        `salepoint_songs`.`state` as salepoint_song_state
      FROM `songs`
      INNER JOIN `albums` ON `albums`.`id` = `songs`.`album_id`
      INNER JOIN `people` ON `people`.`id` = `albums`.`person_id`
      LEFT OUTER JOIN `youtube_monetizations` ON `youtube_monetizations`.`person_id` = `people`.`id`
      LEFT OUTER JOIN (
        SELECT `track_monetizations`.* FROM `track_monetizations`
          WHERE `track_monetizations`.`store_id` = #{STORE_ID}
      ) AS `track_monetizations` ON `track_monetizations`.`song_id` = `songs`.`id`
      INNER JOIN `salepoint_songs` ON `salepoint_songs`.`song_id` = `songs`.`id`
      LEFT OUTER JOIN `distribution_songs` ON `distribution_songs`.`salepoint_song_id` = `salepoint_songs`.`id`
      LEFT OUTER JOIN `ytm_ineligible_songs` ON `ytm_ineligible_songs`.`song_id` = `songs`.`id`
      LEFT OUTER JOIN `ytm_blocked_songs` ON `ytm_blocked_songs`.`song_id` = `songs`.`id`
      WHERE
        `track_monetizations`.`id` IS NULL
      AND
        `albums`.`payment_applied` = 1
      AND
       `albums`.`album_type` != 'Ringtone'
      LIMIT #{limit};
    SQL
  end

  def execute_insertion
    @insertion_values = map_values.join(", ")

    ActiveRecord::Base.connection.execute(insertion_statement)
  end

  def map_values
    results.map do |result|
      map_value(result)
    end
  end

  def map_value(result)
    song_id,
      person_id,
      distribution_song_delivery_type,
      retry_count,
      takedown_at,
      distribution_song_updated_at,
      salepoint_song_updated_at,
      ineligibile,
      blocked,
      distribution_song_state,
      salepoint_song_state = result

    delivery_type = distribution_song_delivery_type || "full_delivery"

    updated_at = distribution_song_updated_at || salepoint_song_updated_at || time_now

    eligibility_status = process_eligibility_status(ineligibile, salepoint_song_state)

    blocked_state = "blocked" if blocked.present?

    state = blocked_state || distribution_song_state || "new"

    created_at = time_now

    value_array = [
      song_id,
      person_id,
      store_id,
      delivery_type,
      retry_count,
      takedown_at,
      updated_at,
      eligibility_status,
      state,
      created_at
    ]

    mapped_value_array = value_array.map { |value| ActiveRecord::Base.connection.quote(value) }

    "(#{mapped_value_array.join(',')})"
  end

  def process_eligibility_status(ineligibile = nil, salepoint_song_state = nil)
    if ineligibile.present?
      TrackMonetization::INELIGIBLE
    elsif salepoint_song_state.present?
      return TrackMonetization::INELIGIBLE if salepoint_song_state == SALEPOINT_SONG_REJECTED
      return TrackMonetization::ELIGIBLE if salepoint_song_state == SALEPOINT_SONG_APPROVED
      return TrackMonetization::PENDING if salepoint_song_state == SALEPOINT_SONG_NEW
    else
      TrackMonetization::ELIGIBLE
    end
  end

  def insertion_statement
    <<-SQL.strip_heredoc
      INSERT INTO track_monetizations (
        song_id, person_id, store_id, delivery_type, retry_count, takedown_at, updated_at, eligibility_status, state, created_at
      ) VALUES #{insertion_values}
    SQL
  end
end
