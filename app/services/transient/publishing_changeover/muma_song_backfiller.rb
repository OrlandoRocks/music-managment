class Transient::PublishingChangeover::MumaSongBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `muma_songs`.`code`
      FROM
        `muma_songs`
      WHERE
        `muma_songs`.`publishing_composition_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `muma_songs`
      INNER JOIN
        `compositions` ON `compositions`.`id` = `muma_songs`.`composition_id`
      INNER JOIN
        `publishing_compositions` ON `publishing_compositions`.`legacy_composition_id` = `compositions`.`id`
      SET
        `muma_songs`.`publishing_composition_id` = `publishing_compositions`.`id`
      WHERE
        `muma_songs`.`code` IN #{backfillable_record_ids}
    SQL
  end
end
