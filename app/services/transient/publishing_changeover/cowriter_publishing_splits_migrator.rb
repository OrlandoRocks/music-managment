class Transient::PublishingChangeover::CowriterPublishingSplitsMigrator < Transient::PublishingChangeover::MigratorBase
  private

  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`publishing_splits`.`id`) AS publishing_split_id,
        `publishing_splits`.`percent`,
        `publishing_splits`.`updated_by`,
        `publishing_composers`.`id` AS publishing_composer_id,
        `publishing_compositions`.`id` AS publishing_composition_id
      FROM
        `publishing_splits`
      LEFT OUTER JOIN
        `cowriters` ON `publishing_splits`.`writer_id` = `cowriters`.`id` AND `publishing_splits`.`writer_type` = "Cowriter"
      LEFT OUTER JOIN
        `publishing_composers` ON `cowriters`.`id` = `publishing_composers`.`legacy_cowriter_id`
      LEFT OUTER JOIN
        `compositions` ON `publishing_splits`.`composition_id` = `compositions`.`id`
      LEFT OUTER JOIN
        `publishing_compositions` ON `compositions`.`id` = `publishing_compositions`.`legacy_composition_id`
      WHERE
        `publishing_splits`.`id` in #{joined_ids_for_query_statement}
    SQL
  end

  def id_query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`publishing_splits`.`id`)
      FROM
        `publishing_splits`
      LEFT OUTER JOIN
        `publishing_composition_splits` ON `publishing_composition_splits`.`legacy_publishing_split_id` = `publishing_splits`.`id`
      WHERE
        `publishing_splits`.`writer_type` = "Cowriter"
        AND
        `publishing_composition_splits`.`id` IS NULL
    SQL
  end

  def map_insertion_row(row)
    publishing_split_id,
      percent,
      updated_by,
      publishing_composer_id,
      publishing_composition_id = row

    legacy_publishing_split_id = publishing_split_id

    right_to_collect = false

    created_at = time_now
    updated_at = time_now

    [
      publishing_composer_id,
      publishing_composition_id,
      right_to_collect,
      percent,
      updated_by,
      created_at,
      updated_at,
      legacy_publishing_split_id
    ]
  end

  def insertion_statement
    <<-SQL.strip_heredoc
      INSERT INTO publishing_composition_splits (
        publishing_composer_id,
        publishing_composition_id,
        right_to_collect,
        percent,
        updated_by,
        created_at,
        updated_at,
        legacy_publishing_split_id
      ) VALUES #{insertion_values}
    SQL
  end
end
