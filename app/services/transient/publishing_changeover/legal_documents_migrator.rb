class Transient::PublishingChangeover::LegalDocumentsMigrator < Transient::PublishingChangeover::MigratorBase
  private

  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `publishing_composers`.`id` as `publishing_composer_id`,
        `legal_documents`.`name`,
        `legal_documents`.`external_uuid`,
        `legal_documents`.`document_template_id`,
        `legal_documents`.`person_id`,
        `legal_documents`.`raw_response`,
        `legal_documents`.`signed_at`,
        `legal_documents`.`provider_identifier`,
        `legal_documents`.`asset_file_name`,
        `legal_documents`.`asset_content_type`,
        `legal_documents`.`asset_file_size`,
        `legal_documents`.`asset_updated_at`,
        `legal_documents`.`provider`,
        `legal_documents`.`status`
      FROM
        `legal_documents`
      LEFT OUTER JOIN
        `composers` ON `legal_documents`.`subject_id` = `composers`.`id` AND `legal_documents`.`subject_type` = "Composer"
      LEFT OUTER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      WHERE
        `legal_documents`.`id` in #{joined_ids_for_query_statement}
    SQL
  end

  def id_query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`legal_documents`.`id`)
      FROM
        `legal_documents`
      LEFT OUTER JOIN
        `composers` ON `legal_documents`.`subject_id` = `composers`.`id` AND `legal_documents`.`subject_type` = "Composer"
      LEFT OUTER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      LEFT OUTER JOIN
        `legal_documents` `publishing_composer_legal_documents`
        ON
        `publishing_composer_legal_documents`.`subject_type` = "PublishingComposer"
        AND
        `publishing_composer_legal_documents`.`subject_id` = `publishing_composers`.`id`
      WHERE
        `publishing_composers`.`id` IS NOT NULL
        AND
        `publishing_composer_legal_documents`.`id` IS NULL;
    SQL
  end

  def map_insertion_row(row)
    publishing_composer_id,
      name,
      external_uuid,
      document_template_id,
      person_id,
      raw_response,
      signed_at,
      provider_identifier,
      asset_file_name,
      asset_content_type,
      asset_file_size,
      asset_updated_at,
      provider,
      status = row

    created_at = time_now
    updated_at = time_now

    subject_id = publishing_composer_id
    subject_type = "PublishingComposer"

    [
      name,
      external_uuid,
      document_template_id,
      person_id,
      subject_id,
      subject_type,
      raw_response,
      signed_at,
      created_at,
      updated_at,
      provider_identifier,
      asset_file_name,
      asset_content_type,
      asset_file_size,
      asset_updated_at,
      provider,
      status
    ]
  end

  def insertion_statement
    <<-SQL.strip_heredoc
      INSERT INTO legal_documents (
        name,
        external_uuid,
        document_template_id,
        person_id,
        subject_id,
        subject_type,
        raw_response,
        signed_at,
        created_at,
        updated_at,
        provider_identifier,
        asset_file_name,
        asset_content_type,
        asset_file_size,
        asset_updated_at,
        provider,
        status
      ) VALUES #{insertion_values}
    SQL
  end
end
