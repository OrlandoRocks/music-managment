class Transient::PublishingChangeover::PublishingChangeoverMigrator < Transient::PublishingChangeover::MigratorBase
  def migrate
    migrate_composers
    migrate_cowriters
    migrate_compositions
    migrate_composer_publishing_splits
    migrate_cowriter_publishing_splits

    migrate_legal_documents

    backfill_publishing_composer_ids
    backfill_publishing_composition_ids
    backfill_account_ids

    backfill_unpaid_purchases
  end

  private

  def migrate_composers
    # Migrate composers to publishing_composers retaining original id and setting primary_composer column to TRUE

    Rails.logger.info("Migrating composers")
    Transient::PublishingChangeover::ComposerMigrator.migrate
    Rails.logger.info("Finished migrating composers")
  end

  def migrate_cowriters
    # Migrate cowriters to publishing_composers creating new id and also retaining original id in the new column legacy_cowriter_id and setting primary_composer column to FALSE

    Rails.logger.info("Migrating cowriters")
    Transient::PublishingChangeover::CowriterMigrator.migrate
    Rails.logger.info("Finished migrating cowriters")
  end

  def migrate_compositions
    # Migrate compositions retaining same id in the new table publishing_compositions

    Rails.logger.info("Migrating compositions")
    Transient::PublishingChangeover::CompositionMigrator.migrate
    Rails.logger.info("Finished migrating compositions")
  end

  def migrate_composer_publishing_splits
    # Migrate publishing_splits that have a writer_type of Composer to publishing_composition_splits and set the publishing_composer_id to the original composer_id / publishing_composer_id for that composer and set right_to_collect to TRUE

    Rails.logger.info("Migrating composer publishing splits")
    Transient::PublishingChangeover::ComposerPublishingSplitsMigrator.migrate
    Rails.logger.info("Finished migrating composer publishing splits")
  end

  def migrate_cowriter_publishing_splits
    # Migrate the publishing_splits that have a writer_type of Cowriter to publishing_composition_splits and select the publishing_composer from Step 2 that has the corresponding legacy_cowriter_id and set their new publishing_composition_split record to their new publishing_composer_id and set right_to_collect to FALSE

    Rails.logger.info("Migrating cowriter publishing splits")
    Transient::PublishingChangeover::CowriterPublishingSplitsMigrator.migrate
    Rails.logger.info("Finished migrating cowriter publishing splits")
  end

  def migrate_legal_documents
    # Create duplicate legal_documents of the original legal_documents (which were for Composers) for the newly created PublishingComposers

    Rails.logger.info("Migrating legal_documents")
    Transient::PublishingChangeover::LegalDocumentsMigrator.migrate
    Rails.logger.info("Finished migrating legal_documents")
  end

  def backfill_publishing_composer_ids
    # Fill out the publishing_composer_id column for the following tables:

    #   non_tunecore_albums
    Rails.logger.info("Backfilling non_tunecore_albums")
    Transient::PublishingChangeover::NonTunecoreAlbumBackfiller.backfill
    Rails.logger.info("Finished backfilling non_tunecore_albums")

    #   terminated_composers
    Rails.logger.info("Backfilling terminated_composers")
    Transient::PublishingChangeover::TerminatedComposerBackfiller.backfill
    Rails.logger.info("Finished backfilling terminated_composers")

    #   tax_info
    Rails.logger.info("Backfilling tax_info")
    Transient::PublishingChangeover::TaxInfoBackfiller.backfill
    Rails.logger.info("Finished backfilling tax_info")

    #   royalty_payments
    Rails.logger.info("Backfilling royalty_payments")
    Transient::PublishingChangeover::RoyaltyPaymentBackfiller.backfill
    Rails.logger.info("Finished backfilling royalty_payments")
  end

  def backfill_publishing_composition_ids
    # Fill out the publishing_composition_id column for the following tables:

    #   non_tunecore_songs
    Rails.logger.info("Backfilling non_tunecore_songs")
    Transient::PublishingChangeover::NonTunecoreSongBackfiller.backfill
    Rails.logger.info("Finished backfilling non_tunecore_songs")

    #   songs
    Rails.logger.info("Backfilling songs")
    Transient::PublishingChangeover::SongBackfiller.backfill
    Rails.logger.info("Finished backfilling songs")

    #   muma_songs
    Rails.logger.info("Backfilling muma_songs")
    Transient::PublishingChangeover::MumaSongBackfiller.backfill
    Rails.logger.info("Finished backfilling muma_songs")

    #   recordings
    Rails.logger.info("Backfilling recordings")
    Transient::PublishingChangeover::RecordingBackfiller.backfill
    Rails.logger.info("Finished backfilling recordings")
  end

  def backfill_account_ids
    # Fill out the account_id column for publishing_compositions

    Rails.logger.info("Backfilling publishing_compositions with publishing_composition_splits")
    Transient::PublishingChangeover::SplitPublishingCompositionBackfiller.backfill
    Rails.logger.info("Finished backfilling publishing_compositions with publishing_composition_splits")

    Rails.logger.info("Backfilling publishing_compositions with songs")
    Transient::PublishingChangeover::TcSongPublishingCompositionBackfiller.backfill
    Rails.logger.info("Finished backfilling publishing_compositions with songs")

    Rails.logger.info("Backfilling publishing_compositions with non_tunecore_songs")
    Transient::PublishingChangeover::NtcSongPublishingCompositionBackfiller.backfill
    Rails.logger.info("Finished backfilling publishing_compositions with non_tunecore_songs")
  end

  def backfill_unpaid_purchases
    # Updates the related record of unpaid composer purchases from a composer record to its respective publishing_composer record
    Rails.logger.info("Backfilling unpaid publishing purchases")
    Transient::PublishingChangeover::UnpaidPurchasesBackfiller.backfill
    Rails.logger.info("Finished backfilling unpaid publishing purchases")
  end
end
