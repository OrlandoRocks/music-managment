class Transient::PublishingChangeover::PublishingCompositionBackfillerBase < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `publishing_compositions`.`id`
      FROM
        `publishing_compositions`
      WHERE
        `publishing_compositions`.`account_id` IS NULL
    SQL
  end
end
