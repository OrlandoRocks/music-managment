class Transient::PublishingChangeover::CompositionMigrator < Transient::PublishingChangeover::MigratorBase
  private

  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`compositions`.`id`) AS composition_id,
        `compositions`.`name`,
        `compositions`.`iswc`,
        `compositions`.`is_unallocated_sum`,
        `compositions`.`state`,
        `compositions`.`prev_state`,
        `compositions`.`state_updated_on`,
        `compositions`.`provider_identifier`,
        `compositions`.`provider_recording_id`,
        `compositions`.`translated_name`,
        `compositions`.`verified_date`
      FROM
        `compositions`
      WHERE
        `compositions`.`id` in #{joined_ids_for_query_statement}
    SQL
  end

  def id_query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`compositions`.`id`)
      FROM
        `compositions`
      LEFT OUTER JOIN
        `publishing_compositions` ON `publishing_compositions`.`legacy_composition_id` = `compositions`.`id`
      WHERE
        `publishing_compositions`.`id` IS NULL
    SQL
  end

  def map_insertion_row(row)
    composition_id,
      name,
      iswc,
      is_unallocated_sum,
      state,
      prev_state,
      state_updated_on,
      provider_identifier,
      provider_recording_id,
      translated_name,
      verified_date = row

    created_at = time_now
    updated_at = time_now

    legacy_composition_id = composition_id

    [
      name,
      iswc,
      is_unallocated_sum,
      state,
      prev_state,
      state_updated_on,
      provider_identifier,
      translated_name,
      verified_date,
      created_at,
      updated_at,
      legacy_composition_id
    ]
  end

  def insertion_statement
    <<-SQL.strip_heredoc
      INSERT INTO publishing_compositions (
        name,
        iswc,
        is_unallocated_sum,
        state,
        prev_state,
        state_updated_on,
        provider_identifier,
        translated_name,
        verified_date,
        created_at,
        updated_at,
        legacy_composition_id
      ) VALUES #{insertion_values}
    SQL
  end
end
