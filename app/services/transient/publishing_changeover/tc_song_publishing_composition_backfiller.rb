# rubocop:disable Layout/LineLength
class Transient::PublishingChangeover::TcSongPublishingCompositionBackfiller < Transient::PublishingChangeover::PublishingCompositionBackfillerBase
  # rubocop:enable Layout/LineLength
  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `publishing_compositions`
      INNER JOIN
        `songs` ON `songs`.`publishing_composition_id` = `publishing_compositions`.`id`
      INNER JOIN
        `albums` ON `albums`.`id` = `songs`.`album_id`
      INNER JOIN
        `accounts` ON `accounts`.`person_id` = `albums`.`person_id`
      SET
        `publishing_compositions`.`account_id` = `accounts`.`id`
      WHERE
        `publishing_compositions`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
