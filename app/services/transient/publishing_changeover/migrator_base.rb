class Transient::PublishingChangeover::MigratorBase
  MAX_LIMIT = 5000

  attr_reader :time_now, :migration_data, :insertion_values, :joined_ids_for_query_statement

  def self.migrate(params = {})
    new(params).migrate
  end

  def initialize(params = {}); end

  def migrate
    @time_now = Time.now

    ids_for_query_statement.to_a.flatten.in_groups_of(MAX_LIMIT, false) do |batch|
      @joined_ids_for_query_statement = "(#{batch.join(',')})"

      fetch_migration_data_and_insert_into_table
    end
  end

  def fetch_migration_data_and_insert_into_table
    fetch_migration_data

    return false if migration_data.empty?

    execute_insertion

    true
  end

  def fetch_migration_data
    initial_results = ActiveRecord::Base.connection.execute(query_statement).to_a

    @migration_data = initial_results.present? ? initial_results.uniq { |result| result[0] } : []
  end

  def ids_for_query_statement
    ActiveRecord::Base.connection.execute(id_query_statement)
  end

  private

  def execute_insertion
    @insertion_values = map_insertion_values.join(", ")

    ActiveRecord::Base.connection.execute(insertion_statement)
  end

  def map_insertion_values
    migration_data.map do |result|
      mapped_insertion_row = map_insertion_row(result)
      cleaned_insertion_row = mapped_insertion_row.map { |value| ActiveRecord::Base.connection.quote(value) }

      "(#{cleaned_insertion_row.join(',')})"
    end
  end
end
