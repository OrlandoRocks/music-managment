class Transient::PublishingChangeover::TerminatedComposerBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `terminated_composers`.`id`
      FROM
        `terminated_composers`
      WHERE
        `terminated_composers`.`publishing_composer_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `terminated_composers`
      INNER JOIN
        `composers` ON `composers`.`id` = `terminated_composers`.`composer_id`
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      SET
        `terminated_composers`.`publishing_composer_id` = `publishing_composers`.`id`
      WHERE
        `terminated_composers`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
