class Transient::PublishingChangeover::CowriterMigrator < Transient::PublishingChangeover::ComposerMigrator
  private

  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`cowriters`.`id`) as cowriter_id,
        `cowriters`.`first_name`,
        `cowriters`.`last_name`,
        `cowriters`.`updated_at`,
        `cowriters`.`provider_identifier`,
        `cowriters`.`email`,
        `cowriters`.`is_unknown`,
        `composers`.`person_id`,
        `composers`.`account_id`
      FROM
        `cowriters`
      LEFT OUTER JOIN
        `composers` ON `cowriters`.`composer_id` = `composers`.`id`
      WHERE
        `cowriters`.`id` in #{joined_ids_for_query_statement}
    SQL
  end

  def id_query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`cowriters`.`id`)
      FROM
        `cowriters`
      LEFT OUTER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_cowriter_id` = `cowriters`.`id`
      WHERE
        `publishing_composers`.`id` IS NULL
    SQL
  end

  def map_insertion_row(row)
    cowriter_id,
      first_name,
      last_name,
      updated_at,
      provider_identifier,
      email,
      is_unknown,
      person_id,
      account_id = row

    lod_id = nil
    publisher_id = nil
    publishing_role_id = nil
    performing_rights_organization_id = nil

    first_name = first_name.to_s
    last_name = last_name.to_s
    address_1 = nil
    address_2 = nil
    city = nil
    state = nil
    zip = nil
    cae = nil
    phone = nil
    agreed_to_terms_at = nil
    name_prefix = nil
    name_suffix = nil
    alternate_email = nil
    alternate_phone = nil
    middle_name = nil
    dob = nil
    nosocial = nil
    terminated_at = nil
    perform_live = nil
    sync_opted_in = nil
    sync_opted_updated_at = nil

    country_id = nil
    created_at = time_now
    updated_at = time_now
    is_primary_composer = false
    legacy_composer_id = nil
    legacy_cowriter_id = cowriter_id

    [
      person_id,
      account_id,
      lod_id,
      publisher_id,
      country_id,
      publishing_role_id,
      performing_rights_organization_id,
      first_name,
      last_name,
      address_1,
      address_2,
      city,
      state,
      zip,
      cae,
      email,
      phone,
      agreed_to_terms_at,
      name_prefix,
      name_suffix,
      alternate_email,
      alternate_phone,
      middle_name,
      dob,
      nosocial,
      terminated_at,
      provider_identifier,
      perform_live,
      sync_opted_in,
      sync_opted_updated_at,
      created_at,
      updated_at,
      is_primary_composer,
      is_unknown,
      legacy_composer_id,
      legacy_cowriter_id
    ]
  end
end
