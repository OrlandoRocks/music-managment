class Transient::PublishingChangeover::NonTunecoreAlbumBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `non_tunecore_albums`.`id`
      FROM
        `non_tunecore_albums`
      WHERE
        `non_tunecore_albums`.`publishing_composer_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `non_tunecore_albums`
      INNER JOIN
        `composers` ON `composers`.`id` = `non_tunecore_albums`.`composer_id`
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      SET
        `non_tunecore_albums`.`publishing_composer_id` = `publishing_composers`.`id`
      WHERE
        `non_tunecore_albums`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
