class Transient::PublishingChangeover::RecordingBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `recordings`.`id`
      FROM
        `recordings`
      WHERE
        `recordings`.`publishing_composition_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `recordings`
      INNER JOIN
        `compositions` ON `compositions`.`id` = `recordings`.`composition_id`
      INNER JOIN
        `publishing_compositions` ON `publishing_compositions`.`legacy_composition_id` = `compositions`.`id`
      SET
        `recordings`.`publishing_composition_id` = `publishing_compositions`.`id`
      WHERE
        `recordings`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
