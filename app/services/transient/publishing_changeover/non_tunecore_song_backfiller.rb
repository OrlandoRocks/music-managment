class Transient::PublishingChangeover::NonTunecoreSongBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `non_tunecore_songs`.`id`
      FROM
        `non_tunecore_songs`
      WHERE
        `non_tunecore_songs`.`publishing_composition_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `non_tunecore_songs`
      INNER JOIN
        `compositions` ON `compositions`.`id` = `non_tunecore_songs`.`composition_id`
      INNER JOIN
        `publishing_compositions` ON `publishing_compositions`.`legacy_composition_id` = `compositions`.`id`
      SET
        `non_tunecore_songs`.`publishing_composition_id` = `publishing_compositions`.`id`
      WHERE
        `non_tunecore_songs`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
