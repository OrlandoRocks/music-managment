# rubocop:disable Layout/LineLength
class Transient::PublishingChangeover::SplitPublishingCompositionBackfiller < Transient::PublishingChangeover::PublishingCompositionBackfillerBase
  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `publishing_compositions`
      INNER JOIN
        `publishing_composition_splits` ON `publishing_composition_splits`.`publishing_composition_id` = `publishing_compositions`.`id`
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`id` = `publishing_composition_splits`.`publishing_composer_id`
      INNER JOIN
        `accounts` ON `accounts`.`id` = `publishing_composers`.`account_id`
      SET
        `publishing_compositions`.`account_id` = `accounts`.`id`
      WHERE
        `publishing_compositions`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
# rubocop:enable Layout/LineLength
