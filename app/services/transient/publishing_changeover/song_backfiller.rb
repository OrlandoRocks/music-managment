class Transient::PublishingChangeover::SongBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `songs`.`id`
      FROM
        `songs`
      WHERE
        `songs`.`publishing_composition_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `songs`
      INNER JOIN
        `compositions` ON `compositions`.`id` = `songs`.`composition_id`
      INNER JOIN
        `publishing_compositions` ON `publishing_compositions`.`legacy_composition_id` = `compositions`.`id`
      SET
        `songs`.`publishing_composition_id` = `publishing_compositions`.`id`
      WHERE
        `songs`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
