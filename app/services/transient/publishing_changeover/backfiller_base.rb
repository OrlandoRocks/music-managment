class Transient::PublishingChangeover::BackfillerBase
  MAX_LIMIT = 5000

  attr_reader :migration_data, :joined_ids_for_query_statement, :backfillable_record_ids

  def self.backfill(params = {})
    new(params).backfill
  end

  def initialize(params = {}); end

  def backfill
    fetched_backfillable_record_ids = fetch_backfillable_record_ids.to_a.flatten

    return if fetched_backfillable_record_ids.blank?

    fetched_backfillable_record_ids.in_groups_of(max_limit, false) do |batch|
      @backfillable_record_ids = "(#{batch.join(',')})"

      execute_update
    end
  end

  def max_limit
    MAX_LIMIT
  end

  def fetch_backfillable_record_ids
    ActiveRecord::Base.connection.execute(query_statement)
  end

  private

  def execute_update
    ActiveRecord::Base.connection.execute(update_statement)
  end
end
