class Transient::PublishingChangeover::ComposerMigrator < Transient::PublishingChangeover::MigratorBase
  private

  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`composers`.`id`) AS composer_id,
        `composers`.`person_id`,
        `composers`.`account_id`,
        `composers`.`lod_id`,
        `composers`.`publisher_id`,
        `composers`.`publishing_role_id`,
        `composers`.`performing_rights_organization_id`,
        `composers`.`first_name`,
        `composers`.`last_name`,
        `composers`.`address_1`,
        `composers`.`address_2`,
        `composers`.`city`,
        `composers`.`state`,
        `composers`.`zip`,
        `composers`.`cae`,
        `composers`.`email`,
        `composers`.`phone`,
        `composers`.`agreed_to_terms_at`,
        `composers`.`name_prefix`,
        `composers`.`name_suffix`,
        `composers`.`alternate_email`,
        `composers`.`alternate_phone`,
        `composers`.`middle_name`,
        `composers`.`dob`,
        `composers`.`nosocial`,
        `composers`.`terminated_at`,
        `composers`.`provider_identifier`,
        `composers`.`perform_live`,
        `composers`.`sync_opted_in`,
        `composers`.`sync_opted_updated_at`,
        `composers`.`updated_at`
      FROM
        `composers`
      WHERE
        `composers`.`id` in #{joined_ids_for_query_statement}
    SQL
  end

  def id_query_statement
    <<-SQL.strip_heredoc
      SELECT
        DISTINCT(`composers`.`id`)
      FROM
        `composers`
      LEFT OUTER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      WHERE
        `publishing_composers`.`id` IS NULL
    SQL
  end

  def map_insertion_row(row)
    composer_id,
      person_id,
      account_id,
      lod_id,
      publisher_id,
      publishing_role_id,
      performing_rights_organization_id,
      first_name,
      last_name,
      address_1,
      address_2,
      city,
      state,
      zip,
      cae,
      email,
      phone,
      agreed_to_terms_at,
      name_prefix,
      name_suffix,
      alternate_email,
      alternate_phone,
      middle_name,
      dob,
      nosocial,
      terminated_at,
      provider_identifier,
      perform_live,
      sync_opted_in,
      sync_opted_updated_at,
      updated_at = row

    country_id = nil
    created_at = time_now
    updated_at = time_now
    is_primary_composer = true
    is_unknown = false
    legacy_composer_id = composer_id
    legacy_cowriter_id = nil

    [
      person_id,
      account_id,
      lod_id,
      publisher_id,
      country_id,
      publishing_role_id,
      performing_rights_organization_id,
      first_name,
      last_name,
      address_1,
      address_2,
      city,
      state,
      zip,
      cae,
      email,
      phone,
      agreed_to_terms_at,
      name_prefix,
      name_suffix,
      alternate_email,
      alternate_phone,
      middle_name,
      dob,
      nosocial,
      terminated_at,
      provider_identifier,
      perform_live,
      sync_opted_in,
      sync_opted_updated_at,
      created_at,
      updated_at,
      is_primary_composer,
      is_unknown,
      legacy_composer_id,
      legacy_cowriter_id
    ]
  end

  def insertion_statement
    <<-SQL.strip_heredoc
      INSERT INTO publishing_composers (
        person_id,
        account_id,
        lod_id,
        publisher_id,
        country_id,
        publishing_role_id,
        performing_rights_organization_id,
        first_name,
        last_name,
        address_1,
        address_2,
        city,
        state,
        zip,
        cae,
        email,
        phone,
        agreed_to_terms_at,
        name_prefix,
        name_suffix,
        alternate_email,
        alternate_phone,
        middle_name,
        dob,
        nosocial,
        terminated_at,
        provider_identifier,
        perform_live,
        sync_opted_in,
        sync_opted_updated_at,
        created_at,
        updated_at,
        is_primary_composer,
        is_unknown,
        legacy_composer_id,
        legacy_cowriter_id
      ) VALUES #{insertion_values}
    SQL
  end
end
