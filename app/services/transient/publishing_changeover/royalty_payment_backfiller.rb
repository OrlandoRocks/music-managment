class Transient::PublishingChangeover::RoyaltyPaymentBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `royalty_payments`.`id`
      FROM
        `royalty_payments`
      WHERE
        `royalty_payments`.`publishing_composer_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `royalty_payments`
      INNER JOIN
        `composers` ON `composers`.`id` = `royalty_payments`.`composer_id`
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      SET
        `royalty_payments`.`publishing_composer_id` = `publishing_composers`.`id`
      WHERE
        `royalty_payments`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
