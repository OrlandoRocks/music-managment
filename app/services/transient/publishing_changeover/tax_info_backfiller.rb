class Transient::PublishingChangeover::TaxInfoBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `tax_info`.`id`
      FROM
        `tax_info`
      WHERE
        `tax_info`.`publishing_composer_id` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `tax_info`
      INNER JOIN
        `composers` ON `composers`.`id` = `tax_info`.`composer_id`
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      SET
        `tax_info`.`publishing_composer_id` = `publishing_composers`.`id`
      WHERE
        `tax_info`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
