class Transient::PublishingChangeover::NtcSongPublishingCompositionBackfiller < Transient::PublishingChangeover::PublishingCompositionBackfillerBase
  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `publishing_compositions`
      INNER JOIN
        `non_tunecore_songs` ON `non_tunecore_songs`.`publishing_composition_id` = `publishing_compositions`.`id`
      INNER JOIN
        `non_tunecore_albums` ON `non_tunecore_albums`.`id` = `non_tunecore_songs`.`non_tunecore_album_id`
      INNER JOIN
        `accounts` ON `accounts`.`id` = `non_tunecore_albums`.`account_id`
      SET
        `publishing_compositions`.`account_id` = `accounts`.`id`
      WHERE
        `publishing_compositions`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
