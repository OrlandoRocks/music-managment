class Transient::PublishingChangeover::UnpaidPurchasesBackfiller < Transient::PublishingChangeover::BackfillerBase
  def query_statement
    <<-SQL.strip_heredoc
      SELECT
        `purchases`.`id`
      FROM
        `purchases`
      WHERE
        `purchases`.`related_type` = 'Composer'
        AND
        `purchases`.`paid_at` IS NULL
    SQL
  end

  def update_statement
    <<-SQL.strip_heredoc
      UPDATE
        `purchases`
      INNER JOIN
        `composers` ON `composers`.`id` = `purchases`.`related_id` AND `purchases`.`related_type` = 'Composer'
      INNER JOIN
        `publishing_composers` ON `publishing_composers`.`legacy_composer_id` = `composers`.`id`
      SET
        `purchases`.`related_id` = `publishing_composers`.`id`,
        `purchases`.`related_type` = 'PublishingComposer'
      WHERE
        `purchases`.`id` IN #{backfillable_record_ids}
    SQL
  end
end
