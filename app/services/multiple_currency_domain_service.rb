class MultipleCurrencyDomainService
  def initialize(user)
    @user = user
  end

  def non_native_currency_site?
    @user.country_website.non_native_currency_site?
  end

  def distribution_language_code_select(code)
    code unless non_native_currency_site?
  end
end
