class BulkAlbumsForUsers
  def self.execute(user_ids)
    formatted_user_ids = user_id_array(user_ids)
    collect_albums(formatted_user_ids)
  end

  def self.user_id_array(user_ids)
    if user_ids.include?(",")
      user_ids.split(/,/).map(&:strip)
    else
      user_ids.split(" ")
    end
  end

  def self.collect_albums(user_ids)
    person_conditions = Person.arel_table[:id].in(user_ids)
    Album.joins(:person).where(person_conditions)
  end
end
