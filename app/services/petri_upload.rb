class PetriUpload
  def self.upload(distributable, converted_album, queue)
    if ::PetriUpload::S3Bucket.upload(distributable, converted_album.to_yaml)
      ::PetriUpload::Gen2.upload(distributable, converted_album, queue)
    else
      s3_error
    end
  end

  def initialize(distributable, converted_album, _queue = nil)
    @distributable = distributable
    @converted_album = converted_album
  end

  def object_key
    @distributable.respond_to?(:distribution_id) ? "#{@distributable.distribution_id}_#{@distributable.id}" : @distributable.id
  end

  def s3_error
    error_msg = "#{@distributable.class.name.titleize} #{@distributable.id} failed to queue (couldn't upload to S3)"
    Rails.logger.error error_msg
    raise error_msg
  end
end
