class S3LogService
  attr_reader :file_name, :headers, :data, :s3_file, :key

  class NoFileNameProvidedError < StandardError; end

  class NoDataProvidedError < StandardError; end

  class NoBucketFoundError < StandardError; end

  class NoBucketPathError < StandardError; end

  class InvalidFileError < StandardError; end

  def self.write_and_upload(options)
    new(options).write_and_upload
  end

  def self.upload_only(options)
    new(options).upload_only
  end

  def initialize(options)
    @file_name  = options.fetch(:file_name) { raise NoFileNameProvidedError }
    # @data can be a TempFile/File OR a String/stringable object.  If File, use #upload_only
    @data       = options.fetch(:data) { raise NoDataProvidedError }

    bucket      = options.fetch(:bucket_name) { raise NoBucketFoundError }
    bucket_path = options.fetch(:bucket_path) { raise NoBucketPathError }

    @key = set_s3_key(bucket_path)
    credentials = Aws::Credentials.new(AWS_ACCESS_KEY, AWS_SECRET_KEY)
    client      = Aws::S3::Client.new(
      region: ENV.fetch("AWS_DEFAULT_REGION", "us-east-1"),
      credentials: credentials
    )

    @s3_file = Aws::S3::Object.new(bucket, key, client: client)
  end

  # For the uninitiated, we create, write, reset the pointer to the beginning,
  # send to s3, and close and delete the temp file.
  def write_and_upload
    file = Tempfile.new(file_name)
    file.write(data)
    file.rewind
    s3_file.upload_file(file.path)
    file.unlink
    Rails.logger.info "Uploaded file to S3 - #{key}"
  end

  # option for directly uploading files handled outside the scope of this service
  # data must be a file
  def upload_only
    raise InvalidFileError unless data.respond_to?(:path)

    s3_file.upload_file(data.path)
    Rails.logger.info "Uploaded file to S3 - #{key}"
  end

  def set_s3_key(bucket_path)
    "#{bucket_path}/#{file_name}"
  end
end
