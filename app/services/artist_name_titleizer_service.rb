class ArtistNameTitleizerService
  def self.artist_names(artist_names)
    artist_names.select(&:present?).to_sentence(last_word_connector: ", & ", two_words_connector: " & ")
  end
end
