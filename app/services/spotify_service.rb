# frozen_string_literal: true

class SpotifyService
  INVALID_SPOTIFY_ARTIST_NAME = "Invalid Spotify artist name"
  SPOTIFY_ARTIST_ROLES = %w[primary_artist featuring remixer].freeze

  def self.batch_uri_retrieval(date = 1.week.ago)
    new.tap do |svc|
      albs = svc.find_unlinked_spotify_distributions(date)
      svc.search_by_upc(albs)
    end
  end

  def self.get_one_album_tracks(album_spotify_id)
    new.tap { |svc| svc.get_tracks_for_album(album_spotify_id) }
  end

  def self.get_uri_by_albums(albums_array)
    new.search_by_upc(albums_array)
  end

  attr_accessor :spot_client

  def initialize
    @spot_client = Spotify::ApiClient.new
  end

  def search_by_upc(albums_array)
    return if albums_array.blank?

    albums_array.map do |alb|
      resp = @spot_client.find_by_upc(alb.upc.number)
      if resp.present?
        alb.set_external_id_for("spotify", resp["id"])
      elsif date_delivered(alb) && date_delivered(alb) < 2.weeks.ago
        alb.set_external_id_for("spotify", "unavailable")
      end
    end
  end

  def get_tracks_for_album(spotify_album_uri)
    return if spotify_album_uri.eql?("unavailable")

    tracks = @spot_client.get_album_tracks(spotify_album_uri)
    return if tracks.blank?

    uri_isrc_hash  = extract_track_info(tracks)
    unlinked_songs = unlinked_song_ids(uri_isrc_hash.keys)
    format_and_insert(uri_isrc_hash, unlinked_songs) if unlinked_songs.present?
  end

  def multi_album_lookup(spotify_uris = [])
    @spot_client.get_albums(spotify_uris)
  end

  def unlinked_song_ids(isrc_array = [])
    Song
      .joins("LEFT OUTER JOIN external_service_ids esi ON esi.linkable_id = songs.id and esi.linkable_type = 'Song' and esi.service_name = 'spotify'")
      .by_isrc(isrc_array)
      .where("esi.id is null")
      .select_id_isrc
  end

  def extract_track_info(spotify_tracks)
    spotify_tracks.each_with_object({}) do |item, hash|
      isrc = item["external_ids"]["isrc"]
      uri  = item.fetch("id", nil)
      hash[isrc] = uri
    end
  end

  def set_track_uris(_track_hash, album_uri)
    songs = Album.find_by_service_id(album_uri).first.songs
    songs.each do |s|
      s.set_external_id_for("spotify", track_objects[s.track_num])
    end
  end

  def format_and_insert(track_hash, tc_songs_data)
    statement = +"insert into external_service_ids (linkable_id, identifier, service_name, " \
                +"linkable_type) values "
    tc_songs_data.each do |item|
      statement << "(#{item.id}, '#{track_hash[item.resolved_isrc]}', 'spotify', 'Song'), "
    end

    ActiveRecord::Base.connection.execute(statement.chomp(", "))
  end

  def date_delivered(album)
    album.distributions
         .where(converter_class: "MusicStores::Spotify::Converter", state: "delivered")
         .order(updated_at: :desc).limit(1)
         .pluck(:updated_at).pop
  end

  def find_unlinked_spotify_distributions(date = 1.week.ago)
    album_ids = DistributorAPI::Album
                .delivered_albums({ store: Store::SPOTIFY_STORE_ID, from: date })[0]
                .http_body["albums"]

    Album
      .joins("LEFT OUTER JOIN external_service_ids ei ON ei.linkable_id = albums.id AND ei.linkable_type = 'Album' AND ei.service_name = 'spotify'")
      .joins(:distributions, :upcs)
      .where(upcs: { upc_type: "tunecore" })
      .where(Distribution.arel_table[:updated_at].gt(date))
      .where(takedown_at: nil)
      .where(ei: { linkable_id: nil })
      .where(id: album_ids)
  end

  def self.backfill_artist_ids(limit = 1000)
    new.tap do |svc|
      albs = svc.find_unlinked_artists_albums(limit)
      svc.update_artist_ids(albs) if albs.present?
    end
  end

  def update_artist_ids(albums)
    albums.each do |alb|
      album_esi = alb.spotify_external_service_id
      next if album_esi.blank?

      spotify_artists = @spot_client.find_artists_by_album_uri(album_esi.identifier)

      next if spotify_artists.blank?

      tc_creatives = get_creatives_by_artist_name_and_account_for_spotify(alb)
      tc_creatives.each do |cr|
        tc_artist_name = cr.name.downcase
        if spotify_artists.key? tc_artist_name
          Rails.logger.info("saving #{spotify_artists[tc_artist_name]} as spotify_id for artist #{tc_artist_name}")
          cr.set_external_id_for("spotify", spotify_artists[tc_artist_name])
        else
          Rails.logger.error("Name mismatch for Spotify Album ID: #{alb.id} TC_Artist: #{tc_artist_name} Spotify_Artists: #{spotify_artists}")
        end
      end
    end
    Rails.logger.info("Job Complete: #{albums.count} albums processed via SpotifyService#backfill_artist_ids")
  end

  def find_unlinked_artists_albums(limit = 1000)
    Album
      .finalized
      .joins(:salepoints)
      .joins(:creatives)
      .joins(:spotify_external_service_id)
      .joins("LEFT OUTER JOIN external_service_ids sei ON sei.linkable_id = creatives.id AND sei.linkable_type = 'Creative' AND sei.service_name = 'spotify'")
      .where("sei.linkable_id is null OR (sei.state = 'new_artist' OR sei.identifier is null)")
      .where(salepoints: { store_id: Store::SPOTIFY_STORE_ID, takedown_at: nil })
      .order(created_at: :desc)
      .limit(limit)
      .uniq
  end

  def get_creatives_by_artist_name_and_account_for_spotify(album)
    artist_names = album.creatives.joins(:artist).pluck("artists.name")
    song_ids = album.songs.pluck(:id)

    Creative
      .where(person_id: album.person_id)
      .joins(:artist)
      .merge(creative_left_join_song_roles)
      .merge(creative_left_join_albums)
      .where(
        "(creatives.role in (?)
        AND creatives.creativeable_type = 'Album'
        AND creatives.creativeable_id = #{album.id}
        AND artists.name IN (?))
          OR
        (song_roles.role_type in (?)
        AND creatives.creativeable_type = 'Song'
        AND creatives.creativeable_id in (?)
        AND creatives.role = 'contributor')",
        SPOTIFY_ARTIST_ROLES, artist_names, SPOTIFY_ARTIST_ROLES, song_ids
      )
  end

  def creative_left_join_song_roles
    Creative.joins(
      "Left JOIN `creative_song_roles`
      ON `creative_song_roles`.`creative_id` = `creatives`.`id`
      LEFT JOIN `song_roles`
      ON `song_roles`.`id` = `creative_song_roles`.`song_role_id`
      LEFT JOIN `songs`
      ON `songs`.`id` = `creative_song_roles`.`song_id`"
    )
  end

  def creative_left_join_albums
    Creative.joins(
      "LEFT JOIN albums a1
      ON `creatives`.`creativeable_id` = `a1`.`id`"
    )
  end

  def get_album_creatives(album)
    album.creatives.where(role: SPOTIFY_ARTIST_ROLES).pluck(:id)
  end

  def get_songs_secondary_creatives(album)
    sp_roles = SPOTIFY_ARTIST_ROLES.map(&:inspect).join(", ")
    album.songs
         .joins(creative_song_roles: [:song_role, :creative])
         .where("song_roles.role_type IN (#{sp_roles}) OR creatives.role IN (#{sp_roles}) ")
         .pluck("DISTINCT creatives.id")
  end

  def get_songs_primary_creatives(album)
    album.songs
         .joins(:creatives)
         .where("creatives.role IN (#{SPOTIFY_ARTIST_ROLES.map(&:inspect).join(', ')})").pluck(" DISTINCT creatives.id")
  end
end
