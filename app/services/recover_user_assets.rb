require "open-uri"

class RecoverUserAssets
  RECOVER_ASSETS_PATH = "#{Rails.root}/tmp/recover_user_assets"
  attr_accessor :recover_asset, :person, :albums, :dir, :s3, :album_dir, :store_ids

  def initialize(recover_asset_id)
    @recover_asset = RecoverAsset.find(recover_asset_id)
    @person        = recover_asset.person
    @s3            = AWS::S3::Client.new
    @store_ids     = StoreDeliveryConfig.where(ddex_version: "382").pluck(:store_id)
    fetch_albums
  end

  def recover
    begin
      update_state("downloading_assets")
      setup_directory
      download_assets
      zip_assets
      upload_assets
      update_state("completed")
    rescue => e
      Rails.logger.warn("Error in recovering user assets for #{recover_asset.id}: #{e.message}")
      Airbrake.notify("Error in recovering user assets for", "#{recover_asset.id}: #{e}")
    ensure
      clean_up
    end
  end

  private

  def update_state(state)
    recover_asset.update(state: state)
  end

  def fetch_albums
    @albums ||= recover_albums_ids.present? ? person.albums.where(id: recover_albums_ids) : person.albums
  end

  def recover_albums_ids
    recover_asset.albums_ids
  end

  def download_assets
    albums.each do |album|
      setup_album_dir(album)
      recover_asset.include_assets.each { |type| send("download_#{type}", album) }
    end
  end

  def download_artwork(album)
    return unless album.artwork&.s3_asset

    get_asset(album.artwork.s3_asset.bucket, album.artwork.s3_asset.key)
  end

  def download_audio(album)
    return if album.songs.blank?

    album.songs.each do |song|
      next unless song.s3_asset

      get_asset(song.s3_asset.bucket, song.s3_asset.key)
    end
  end

  def download_metadata(album)
    begin
      distributions = album.distributions
                           .joins(:salepoints)
                           .where("salepoints.store_id IN (?) AND distributions.state = 'delivered'", store_ids)
      return if distributions.blank?

      distro = distributions.first
      filename = File.join(album_dir.path, "#{album.tunecore_upc_number}.xml")
      open(filename, "wb") do |file|
        file << open(distro.fetch_metadata).read
      end
    rescue => e
      Rails.logger.warn("Error downloading metadata: #{e.message}")
      Airbrake.notify("RecoverUserAssets s3 assets download failure metadata", e)
    end
  end

  def recover_assets_dir_path
    File.join(RECOVER_ASSETS_PATH, "/#{recover_asset.id}_#{person.id}")
  end

  def setup_directory
    dirname = FileUtils.mkdir_p(recover_assets_dir_path).first
    @dir = Dir.new(dirname)
  end

  def setup_album_dir(album)
    album_dir = File.join(dir.path, album.tunecore_upc_number)
    dirname = FileUtils.mkdir_p(album_dir).first
    @album_dir = Dir.new(dirname)
  end

  def get_asset(bucket = "tunecore.com", key)
    begin
      bucket = S3_CLIENT.buckets[bucket]
      filename = File.join(album_dir.path, key.split("/").last)
      local_dir = File.dirname(filename)
      FileUtils.mkdir_p(local_dir) unless File.exist?(local_dir)

      file = File.new(filename, "wb")
      bucket.objects[key].read { |chunk| file.write(chunk) }
    rescue => e
      Rails.logger.warn("Error downloading #{key}: #{e.message}")
      Airbrake.notify("RecoverUserAssets s3 assets download failure", e)
    ensure
      file.close
    end
  end

  def zipfile_name
    zipfile_name = File.join(dir, "/#{recover_asset.id}_#{person.id}.zip")
  end

  def zip_assets
    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      Dir["#{recover_assets_dir_path}/**/**"].each do |file|
        zipfile.add(file.sub(recover_assets_dir_path + "/", ""), file)
      end
    end
  end

  def upload_assets
    recover_asset.upload_zip(zipfile_name)
  end

  def clean_up
    FileUtils.remove_dir(dir.path, true)
  end

  def assets_included?(asset_type)
    recover_asset.include_assets.include?(asset_type)
  end
end
