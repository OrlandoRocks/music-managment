class ArtistNameUtils
  attr_reader :name

  def self.parse_for_comparison(name)
    new(name).parse_for_comparison
  end

  def self.normalize(name)
    new(name).normalize
  end

  def initialize(name)
    @name = name
  end

  # Normalize a name string from an external service for comparison with another name string
  def parse_for_comparison
    return if name.nil?

    @parse_for_comparison ||= transliterate(special_trim(normalize))
  end

  def normalize(str = name)
    return if str.nil?

    @normalize ||= str.mb_chars.unicode_normalize(:nfkd).wrapped_string.downcase.gsub(
      /\s+/,
      ""
    )
  end

  private

  def transliterate(str = name)
    @transliterate ||= I18n.transliterate(str)
  end

  def special_trim(str = name)
    @special_trim ||= str.downcase.gsub(/[\W\s]+/, "")
  end
end
