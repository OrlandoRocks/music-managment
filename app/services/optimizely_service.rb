module OptimizelyService
  include ERB::Util
  def record_optimizely_event(event_name, optimizely_end_user_id, optimizely_buckets, options = {})
    json = ActiveSupport::JSON.decode(CGI::unescape(optimizely_buckets))

    path = "/event?a=#{OPTIMIZELY_ACCOUNT_ID}"

    path += "&n=#{url_encode(event_name)}"
    path += "&u=#{url_encode(optimizely_end_user_id)}"

    json.each do |experiment_id, variation_id|
      path += "&x#{experiment_id}=#{variation_id}"
    end

    path += "&v=#{options[:v]}" if options[:v]

    begin
      res = Net::HTTP.get(OPTIMIZELY_URL, path)
      Rails.logger.info "Optimizely - #{path}"
    rescue StandardError => e
      Rails.logger.error e
    end
  end
end
