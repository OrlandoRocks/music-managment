class PetriUpload::Gen2 < PetriUpload
  attr_reader :distributable, :converted_album, :queue

  def self.upload(distributable, converted_album, queue)
    new(distributable, converted_album, queue).upload
  end

  def initialize(distributable, converted_album, queue)
    super
    @queue = get_queue(queue)
  end

  def upload
    distributable.update_attribute(:sqs_message_id, queue.send_message(message_body).message_id)
    Rails.logger.info "Queued message id #{distributable.sqs_message_id} for distribution #{distributable.id}"
    message_body
  end

  def get_queue(queue_name = nil)
    queue_name ||= PETRI_QUEUE || "petri-#{name_of_queue}"
    SQS_CLIENT.queues.named(queue_name)
  end

  def name_of_queue
    short_name = @distributable.store.short_name
    short_name = short_name.exclude?("iTunes") ? short_name : "iTunes"
    short_name = short_name.exclude?("Believe") ? short_name : "Believe"
  end

  def message_body
    {
      message_ver: "YAML.v1",
      bucket: PETRI_MESSAGE_BUCKET,
      key: object_key,
      retry_count: distributable.retry_count,
      distribution_type: distributable.distribution_type,
      provider: "TuneCoreUS"
    }.to_yaml
  end
end
