class PetriUpload::S3Bucket < PetriUpload
  def self.upload(distributable, converted_album, _song = nil)
    new(distributable, converted_album).upload
  end

  def upload
    bucket = S3_CLIENT.buckets[PETRI_MESSAGE_BUCKET]
    bucket = S3_CLIENT.buckets.create(PETRI_MESSAGE_BUCKET) unless bucket.exists?
    bucket.objects.create(object_key, @converted_album) ? true : false
  end
end
