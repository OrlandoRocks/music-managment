class PublishingAdministration::PublishingWorkMatcherService
  include ArelTableMethods
  include PublishingAdministrationHelper

  attr_reader :matched_publishing_compositions, :unmatched_works

  def self.match(account, works)
    new(account, works).tap(&:match)
  end

  def initialize(account, works)
    @account              = account
    @works                = works
    @matched_publishing_compositions = []
    @unmatched_works = []
  end

  def match
    works.each do |work|
      work_code     = work.work_code
      matched_comps = match_work_to_publishing_composition(work_code, downcased_name(work.title))

      if matched_comps.present?
        matched_comps.each do |_publishing_composition|
          matched_publishing_compositions.push([matched_comps.first, work])
        end
      else
        unmatched_works << work
      end
    end
  end

  private

  attr_reader :account, :works

  def account_publishing_compositions
    @account_publishing_compositions ||= (publishing_composition_songs_query + publishing_composition_ntc_songs_query).uniq
  end

  def publishing_composition_songs_query
    @publishing_composition_songs ||=
      PublishingComposition
      .joins(songs: { album: { person: { account: :publishing_composers } } })
      .left_joins(:muma_songs)
      .where(accounts: { id: account.id })
      .where(publishing_composer_t[:person_id].not_eq(nil))
      .preload(:muma_songs)
      .select(publishing_composition_t[Arel.star])
      .readonly(false)
  end

  def publishing_composition_ntc_songs_query
    @publishing_composition_ntc_songs ||=
      PublishingComposition
      .joins(non_tunecore_songs: { non_tunecore_album: :publishing_composer })
      .left_joins(:muma_songs)
      .where(publishing_composers: { account_id: account.id })
      .preload(:muma_songs)
      .select(publishing_composition_t[Arel.star])
      .readonly(false)
  end

  def downcased_name(name)
    return "" if name.nil?

    sanitize_name(name).downcase
  end

  def match_work_to_publishing_composition(work_code, work_title)
    account_publishing_compositions.select do |publishing_composition|
      matches_work_code?(publishing_composition, work_code) ||
        matches_work_name?(publishing_composition, work_title) ||
        matches_muma_song?(publishing_composition, work_title)
    end
  end

  def matches_work_code?(publishing_composition, work_code)
    publishing_composition.provider_identifier == work_code
  end

  def matches_work_name?(publishing_composition, work_title)
    publishing_composition.clean_name.downcase == work_title ||
      publishing_composition.clean_translated_name.downcase == work_title
  end

  def matches_muma_song?(publishing_composition, work_title)
    return false if publishing_composition.muma_songs.blank?

    publishing_composition.muma_songs.first&.title&.downcase == work_title
  end
end
