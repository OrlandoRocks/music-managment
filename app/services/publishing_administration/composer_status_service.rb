class PublishingAdministration::ComposerStatusService
  include ArelTableMethods

  def initialize(composer)
    @composer = composer
  end

  def needs_cae_number?
    composer.cae.blank?
  end

  def needs_pro?
    composer.performing_rights_organization.blank?
  end

  def needs_dob?
    composer.dob.blank? && dob_required?
  end

  def state
    return :terminated if composer.has_fully_terminated_composers?

    active? ? :active : :pending
  end

  def active?
    composer.account.present? && paid_for? && !needs_cae_number? && !needs_pro? && !needs_dob? && !composer.has_fully_terminated_composers? && composer.valid_letter_of_direction?
  end

  def paid_for?
    if composer.is_a?(PublishingComposer)
      return true if composer.legacy_composer && composer
                     .legacy_composer
                     .related_purchases
                     .where(related_type: "Composer", related_id: composer.legacy_composer.id, product_id: pub_admin_product_id)
                     .where.not(paid_at: nil)
                     .exists?

      return true if composer
                     .related_purchases
                     .where(related_type: "PublishingComposer", related_id: composer.id, product_id: pub_admin_product_id)
                     .where.not(paid_at: nil)
                     .exists?

      false
    else
      composer
        .related_purchases
        .where(related_type: "Composer", related_id: composer.id, product_id: pub_admin_product_id)
        .where.not(paid_at: nil)
        .exists?
    end
  end

  def pub_admin_product_in_cart?
    composer.related_purchases.exists?(
      person_id: composer.account.person_id,
      product_id: pub_admin_product_id,
      related_id: composer.id,
      related_type: related_type,
      invoice_id: nil,
      paid_at: nil
    )
  end

  def has_cae_number?
    composer.cae.present?
  end

  def cae_number
    composer.cae
  end

  private

  attr_reader :composer

  def related_type
    composer.is_a?(PublishingComposer) ? "PublishingComposer" : "Composer"
  end

  def dob_required?
    FeatureFlipper.show_feature?(:publishing_dob_required, composer.person)
  end

  def pub_admin_product_id
    @pub_admin_product_id ||=
      Product.find_products_for_country(
        composer.publishing_administrator.country_domain,
        :songwriter_service
      )&.first
  end
end
