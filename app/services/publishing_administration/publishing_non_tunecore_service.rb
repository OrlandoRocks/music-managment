class PublishingAdministration::PublishingNonTunecoreService
  def self.create_ntc_album_and_song(params)
    new(params).create_ntc_album_and_song
  end

  def initialize(params)
    @publishing_composer = params[:publishing_composer]
    @publishing_composition = params[:publishing_composition]
    @work = PublishingAdministration::ApiClientServices::GetPublishingWorkService.get_work(@publishing_composition.provider_identifier)
  end

  def create_ntc_album_and_song
    create_non_tunecore_album
    create_non_tunecore_song
  end

  private

  def create_non_tunecore_album
    @ntc_album = NonTunecoreAlbum.create(
      name: @publishing_composition.name,
      publishing_composer_id: @publishing_composer.id,
      orig_release_year: Date.today,
      account_id: @publishing_composer.account_id
    )
  end

  def create_non_tunecore_song
    NonTunecoreSong.create(
      name: @publishing_composition.name,
      non_tunecore_album_id: @ntc_album.id,
      publishing_composition_id: @publishing_composition.id,
      artist_id: find_or_create_artist.try(:id)
    )
  end

  def find_or_create_artist
    return unless @work["PerformingArtists"].try(:any?)

    Artist.where(name: @work["PerformingArtists"].first["Name"]).first_or_create
  end
end
