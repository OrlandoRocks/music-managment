class PublishingAdministration::PublishingWorkUpdateService
  attr_reader :publishing_composition, :work, :free_purchase

  def self.update(publishing_composition, work, account, free_purchase = false)
    new(publishing_composition, work, account, free_purchase).update
  end

  def initialize(publishing_composition, work, account, free_purchase = false)
    @publishing_composition = publishing_composition
    @work = work
    @account = account
    @free_purchase = free_purchase
  end

  def update
    return if work.writers.blank?

    update_publishing_composition_account
    compare_writers
    compare_splits
    update_publishing_composition
    update_publishing_composition_state
  end

  private

  def compare_writers
    PublishingAdministration::ApiClientServices::PrimaryPublishingComposerNormalizeService.reconcile_publishing_composers(writer_params)
    PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService.reconcile_cowriting_publishing_composers(writer_params)
  end

  def update_publishing_composition_account
    return if publishing_composition.account.present?

    publishing_composition.update(account_id: @account)
  end

  def compare_splits
    total_shares = work.writers.sum(&:share).to_d

    return unless total_shares == 100.to_d

    work.writers.each do |split|
      right_to_collect = split.right_to_collect
      writer = writers_for(right_to_collect).find_by(provider_identifier: split.writer_code)
      share_percentage = split.share.to_d

      if writer.present?
        update_split(writer, share_percentage)
      else
        add_split(right_to_collect, split.writer_code, share_percentage)
      end
    end
  end

  def update_split(writer, share_percentage)
    pub_split = publishing_composition.publishing_composition_splits.find_by(publishing_composition_splits: { publishing_composer_id: writer.id, right_to_collect: writer.is_primary_composer? })

    return unless pub_split.present? && pub_split.percent != share_percentage

    pub_split.update_attribute(:percent, share_percentage)
  end

  def add_split(right_to_collect, writer_code, share_percentage)
    writer = PublishingComposer.find_by(provider_identifier: writer_code)

    return unless writer.present? && (publishing_composition.total_split_pct + share_percentage <= 100)

    PublishingCompositionSplit.create!(
      publishing_composer_id: writer.id,
      publishing_composition_id: publishing_composition.id,
      percent: share_percentage,
      right_to_collect: right_to_collect,
    )
  end

  def update_publishing_composition
    return if work.should_not_update?

    return unless verified_dates_valid?

    publishing_composition.update(
      {
        name: work.title,
        verified_date: work.verified_date,
        provider_identifier: work.work_code
      }
    )
    publishing_composition.verified! if work.verified_date.present?

    return unless work.number_of_recordings.positive?

    PublishingAdministration::PublishingRecordingUpdateService.update(publishing_composition)
  end

  def verified_dates_valid?
    publishing_composition.verified_date.nil? ||
      work.verified_date.nil? ||
      publishing_composition.verified_date <= work.verified_date.try(:to_datetime)
  end

  def update_publishing_composition_state
    if work.is_deactivated? && !publishing_composition.terminated?
      publishing_composition.terminate!
    elsif !work.is_deactivated? && publishing_composition.terminated?
      publishing_composition.verified!
    elsif publishing_composition.non_tunecore_songs.any? && publishing_composition.pending_distribution?
      publishing_composition.submit_split!
    end
  end

  def writers_for(right_to_collect)
    PublishingComposer
      .joins(:publishing_composition_splits)
      .where(publishing_composition_splits: {
               publishing_composition_id: publishing_composition.id,
               right_to_collect: right_to_collect
             }).distinct
  end

  def writer_params
    publishing_composer_splits, cowriter_splits = work.writers.partition(&:right_to_collect)

    {
      account: publishing_composition.account,
      api_publishing_composers: publishing_composer_splits,
      api_cowriting_publishing_composers: cowriter_splits,
      publishing_composers: writers_for(true),
      cowriting_publishing_composers: writers_for(false),
      free_purchase: free_purchase
    }
  end
end
