class PublishingAdministration::CompositionMatcherService
  include ArelTableMethods

  def self.match(params)
    new(params).match
  end

  def initialize(params)
    @composer             = params[:composer]
    @current_composition  = params[:composition]
    @params               = { composer: @composer, composition: @current_composition }
  end

  def match
    find_existing_composition_that_matches_current_composition_and_update_provider_identifier
    @current_composition
  end

  private

  attr_reader :composer, :current_composition

  def compositions_by_name
    comp_songs      = composition_songs_by_name
    comp_ntc_songs  = composition_ntc_songs_by_name

    comp_ntc_songs.keys.each do |name|
      if comp_songs[name]
        comp_songs[name].concat(comp_ntc_songs[name])
      else
        comp_songs[name] = comp_ntc_songs[name]
      end
    end

    comp_songs
  end

  def composition_songs_by_name
    compositions_songs_query.group_by { |cs| cs.clean_composition_name.downcase }
  end

  def composition_ntc_songs_by_name
    compositions_ntc_songs_query.group_by { |ntcs| ntcs.clean_composition_name.downcase }
  end

  def compositions_songs_query
    @compositions ||= Composition
                      .joins(:publishing_splits)
                      .joins(songs: :album)
                      .where(album_t[:person_id].eq(composer.person_id))
                      .where(composition_t[:id].not_eq(current_composition.id))
                      .group(composition_t[:id])
  end

  def compositions_ntc_songs_query
    @ntc_compositions ||= Composition
                          .joins(:publishing_splits)
                          .joins(non_tunecore_songs: :non_tunecore_album)
                          .where(non_tunecore_album_t[:composer_id].eq(composer.id))
                          .where(composition_t[:id].not_eq(current_composition.id))
                          .group(composition_t[:id])
  end

  def current_composition_splits
    @current_composition_splits ||= parse_splits(current_composition)
  end

  def parse_splits(composition)
    composition.publishing_splits.each_with_object({}) do |ps, hash|
      key = ps.writer.full_name.downcase
      hash[key] = { percent: ps.percent, type: ps.writer_type }
    end
  end

  def splits_match?(other_comp)
    current_composition_splits == parse_splits(other_comp) &&
      other_comp.provider_identifier.present?
  end

  def find_existing_composition_that_matches_current_composition_and_update_provider_identifier
    current_name  = current_composition.clean_composition_name.downcase
    comps         = compositions_by_name[current_name]
    return if comps.blank?

    matching_comp = comps.find { |comp| splits_match?(comp) }

    @current_composition.update(provider_identifier: matching_comp.provider_identifier) if matching_comp
  end
end
