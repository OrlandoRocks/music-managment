class PublishingAdministration::CleanupSidekiqQueuesService
  def self.cleanup
    new.cleanup
  end

  def initialize
    @morgue = Sidekiq::DeadSet.new
  end

  def cleanup
    jobs = zombie_jobs
    kill(jobs)
  end

  private

  attr_reader :morgue

  def kill(jobs)
    jobs.each { |job| job.try(:delete) }
  end

  def zombie_jobs
    morgue.select { |job| job.klass.include?("PublishingAdministration") }
  end
end
