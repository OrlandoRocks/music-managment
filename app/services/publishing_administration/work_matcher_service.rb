class PublishingAdministration::WorkMatcherService
  include ArelTableMethods
  include PublishingAdministrationHelper

  attr_reader :matched_compositions, :unmatched_works

  def self.match(account, works)
    new(account, works).tap(&:match)
  end

  def initialize(account, works)
    @account              = account
    @works                = works
    @matched_compositions = []
    @unmatched_works      = []
  end

  def match
    works.each do |work|
      work_code     = work.work_code
      matched_comps = match_work_to_composition(work_code, downcased_name(work.title))

      if matched_comps.present?
        matched_comps.each do |_composition|
          matched_compositions.push([matched_comps.first, work])
        end
      else
        unmatched_works << work
      end
    end
  end

  private

  attr_reader :account, :works

  def account_compositions
    @account_compositions ||= (composition_songs_query + composition_ntc_songs_query).uniq
  end

  def composition_songs_query
    @composition_songs ||=
      Composition
      .joins(songs: { album: { person: { account: :composers } } })
      .where(accounts: { id: account.id })
      .where(composer_t[:person_id].not_eq(nil))
      .select(composition_t[Arel.star])
      .readonly(false)
  end

  def composition_ntc_songs_query
    @composition_ntc_songs ||=
      Composition
      .joins(non_tunecore_songs: { non_tunecore_album: :composer })
      .where(composers: { account_id: account.id })
      .select(composition_t[Arel.star])
      .readonly(false)
  end

  def downcased_name(name)
    return "" if name.nil?

    sanitize_name(name).downcase
  end

  def match_work_to_composition(work_code, work_title)
    account_compositions.select do |composition|
      matches_work_code?(composition, work_code) || matches_work_name?(composition, work_title)
    end
  end

  def matches_work_code?(composition, work_code)
    composition.provider_identifier == work_code
  end

  def matches_work_name?(composition, work_title)
    composition.clean_name.downcase == work_title ||
      composition.clean_translated_name.downcase == work_title ||
      MumaSong.exists?(composition_id: composition.id, title: work_title)
  end
end
