class PublishingAdministration::PostApprovalService
  def self.create(album_id)
    new(album_id).create
  end

  def initialize(album_id)
    @album = Album.find(album_id)
    @songs = Song.where(album_id: album_id, publishing_composition_id: nil)
  end

  def create
    songs.each do |song|
      PublishingAdministration::PublishingCompositionCreator.create(
        name: song.name,
        account: @album.person.account,
        song: song
      )
    end
  end

  private

  attr_reader :songs
end
