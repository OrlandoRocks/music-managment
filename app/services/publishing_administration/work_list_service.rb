class PublishingAdministration::WorkListService
  attr_reader :account, :free_purchase, :work_list, :unmatched_works, :matched_compositions

  def self.list(account, free_purchase = false)
    new(account, free_purchase).list
  end

  def initialize(account, free_purchase = false)
    @account = account
    @free_purchase = free_purchase
    @work_list = fetch_work_list
    @unmatched_works = work_matcher.unmatched_works
    @matched_compositions = work_matcher.matched_compositions
  end

  def list
    return unless work_list.valid?

    create_compositions_for_unmatched_works if unmatched_works.present?
    update_matched_compositions if matched_compositions.present?
    update_terminated_compositions_state
  end

  private

  def create_compositions_for_unmatched_works
    unmatched_works.each do |work|
      next if Composition.exists?(name: work.title, provider_identifier: work.work_code)

      writer_code = work.writers.find(&:right_to_collect)&.writer_code
      composer = account.composers.find_by(provider_identifier: writer_code)
      next if composer.blank?

      composition = Composition.where(name: work.title, provider_identifier: work.work_code).first_or_create
      params = { composer: composer, composition: composition }
      PublishingAdministration::NonTunecoreService.create_ntc_album_and_song(params)

      PublishingAdministration::WorkUpdateService.update(composition.reload, work, free_purchase)
    end
  end

  def update_matched_compositions
    matched_compositions.each do |composition, work|
      PublishingAdministration::WorkUpdateService.update(composition, work, free_purchase)
    end
  end

  def fetch_work_list
    PublishingAdministration::ApiClientServices::FetchWorkListService.fetch(account.provider_account_id)
  end

  def work_matcher
    @work_matcher ||=
      PublishingAdministration::WorkMatcherService.match(account, work_list.works)
  end

  def update_terminated_compositions_state
    PublishingAdministration::UpdateTerminatedCompositionsService.update(account)
  end
end
