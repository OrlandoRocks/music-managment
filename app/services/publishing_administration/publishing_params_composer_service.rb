class PublishingAdministration::PublishingParamsComposerService
  def self.compose(publishing_composer_id)
    new(publishing_composer_id).compose
  end

  def self.format_dob_params(params)
    enroll_params = params[:publishing_administration_enrollment_form]
    enroll_params[:dob_y] = enroll_params.delete("dob(1i)")
    enroll_params[:dob_m] = enroll_params.delete("dob(2i)")
    enroll_params[:dob_d] = enroll_params.delete("dob(3i)")
  end

  def initialize(publishing_composer_id)
    @publishing_composer = PublishingComposer.includes(:person, :publisher).find(publishing_composer_id)
    @person = publishing_composer.publishing_administrator
    @publisher = publishing_composer.publisher
  end

  def compose
    if publisher.present?
      publishing_composer_params.merge!(publisher_params)
    else
      publishing_composer_params
    end.with_indifferent_access
  end

  private

  attr_reader :publishing_composer, :publisher, :person

  def publishing_composer_params
    {
      id: publishing_composer.id,
      name_prefix: publishing_composer.name_prefix,
      first_name: publishing_composer.first_name,
      middle_name: publishing_composer.middle_name,
      last_name: publishing_composer.last_name,
      name_suffix: publishing_composer.name_suffix,
      dob: publishing_composer.dob,
      email: publishing_composer.current_email,
      person_id: publishing_composer.person_id || publishing_composer.account.person_id,
      composer_cae: publishing_composer.cae,
      composer_pro_id: publishing_composer.performing_rights_organization_id,
      publishing_role_id: publishing_composer.publishing_role_id
    }
  end

  def publisher_params
    {
      publisher_name: publisher.name,
      publisher_cae: publisher.cae,
      publisher_pro_id: publishing_composer.performing_rights_organization_id
    }
  end
end
