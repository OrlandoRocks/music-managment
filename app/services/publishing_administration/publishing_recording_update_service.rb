class PublishingAdministration::PublishingRecordingUpdateService
  attr_reader :publishing_composition, :recording_list

  def self.update(publishing_composition)
    new(publishing_composition).update
  end

  def initialize(publishing_composition)
    @publishing_composition = publishing_composition
    @recording_list = PublishingAdministration::ApiClientServices::FetchRecordingListService.fetch(publishing_composition)
  end

  def update
    return if publishing_composition.provider_identifier.blank? || !recording_list.valid?

    recording_list.recordings.each do |recording|
      next if publishing_composition.recordings.exists?(recording_code: recording.recording_id)

      publishing_composition.recordings.create!(
        recordable: publishing_composition.song,
        recording_code: recording.recording_id,
      )

      update_isrc_number(recording.isrc) if recording.isrc.present?
    end

    delete_extraneous_recordings
  end

  private

  def update_isrc_number(isrc_number)
    ntc_song = NonTunecoreSong.find_by(publishing_composition_id: publishing_composition.id)
    ntc_song.update(isrc: isrc_number) if ntc_song.present?
  end

  def delete_extraneous_recordings
    recording_ids = recording_list.recordings.map(&:recording_id)
    recordings_to_delete = publishing_composition.recordings.where.not(recording_code: recording_ids)

    recordings_to_delete.destroy_all if recordings_to_delete.present?
  end
end
