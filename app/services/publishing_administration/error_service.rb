class PublishingAdministration::ErrorService
  class ApiError < StandardError; end

  attr_reader :request, :response, :requestable

  def self.log_errors(request, response)
    new(request, response).log_errors
  end

  def initialize(request, response)
    @request     = request
    @response    = response
    @requestable = request.requestable
  end

  def log_errors
    if requestable.present?
      RightsAppError.create!(
        api_endpoint: response.api_endpoint,
        http_method: http_method,
        http_status_code: response.http_status,
        requestable: requestable,
        json_response: response.body
      )
    end
    raise_rights_app_error
  end

  def raise_rights_app_error
    message = response.messages.first

    raise ApiError,
          "Error from #{http_method}: #{request.class.name.demodulize}.\n
      Message: #{message}, Url: #{response.api_endpoint}"
  end

  private

  def http_method
    @http_method ||= request.request_method.to_s.upcase
  end
end
