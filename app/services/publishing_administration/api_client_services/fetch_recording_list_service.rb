class PublishingAdministration::ApiClientServices::FetchRecordingListService < PublishingAdministration::ApiClientServices::Base
  attr_reader :composition

  PAGE_SIZE = 100

  def self.fetch(composition)
    new(composition).tap(&:fetch)
  end

  def initialize(composition)
    super
    @composition = composition
    @recordings = []
  end

  def fetch
    recording_list
  end

  def recordings
    @recordings.flatten.compact
  end

  def valid?
    !@response.errors?
  end

  private

  def recording_list(page = 1)
    @response = send_get_recording_list_request(composition: composition, page: page)

    @recordings << @response.recordings if @response.body["Records"].present?
    recording_list(page + 1) if @response.total_count > (PAGE_SIZE * page)
  end
end
