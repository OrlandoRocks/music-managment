class PublishingAdministration::ApiClientServices::GetPublishingWorkService < PublishingAdministration::ApiClientServices::Base
  attr_reader :work_code, :api_client

  def self.get_work(work_code)
    new(work_code).get_work
  end

  def self.writers_with_right_to_collect(work_code)
    new(work_code).writers_with_right_to_collect
  end

  def initialize(work_code)
    super
    @work_code = work_code
  end

  def get_work
    send_get_work_request(work_code: work_code, publishing_changeover_enabled: true).body
  end

  def writers_with_right_to_collect
    writers = get_work["WriterSplits"]
    writers.select { |writer| writer["RightToCollect"] }
  end
end
