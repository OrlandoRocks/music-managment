class PublishingAdministration::ApiClientServices::WorkCreationService < PublishingAdministration::ApiClientServices::Base
  def self.post_work(params)
    new(params).post_work
  end

  def self.update_work(params)
    new(params).update_work
  end

  def initialize(params)
    super
    @composer           = params[:composer]
    @composition        = params[:composition]
    @publishing_splits  = PublishingSplit.where(composition_id: @composition.id)
    @account            = @composer&.account || @composition&.account
  end

  def post_work
    return if account_blocked? || @composition.ineligible?
    return if @composition.publishing_splits.pluck(:percent).any? { |p| p == 0.to_d }

    create_cowriters if unsent_cowriters.present?
    send_create_request

    if @response.already_exists?
      work_code = @response.existing_work_code

      @composition.update(provider_identifier: work_code) if work_code != @composition.provider_identifier
    else
      @composition.update(provider_identifier: @response.work_code)
    end

    @response.body
  end

  def update_work
    return if account_blocked? || @composition.ineligible?
    return if @composition.publishing_splits.pluck(:percent).any? { |p| p == 0.to_d }

    create_cowriters if unsent_cowriters.present?
    send_update_request

    @response.body
  end

  private

  def cowriters
    cowriter_ids = @publishing_splits.where(writer_type: "Cowriter").pluck(:writer_id)
    Cowriter.where(id: cowriter_ids)
  end

  def create_cowriters
    unsent_cowriters.each do |cowriter|
      PublishingAdministration::ApiClientServices::WriterService.create_or_update(cowriter)
    end
  end

  def send_create_request
    request_options = { composer: @composer, composition: @composition }

    @response = send_create_work_request(request_options)
  end

  def send_update_request
    e_tag = send_get_work_request(work_code: @composition.provider_identifier).e_tag

    request_options = { composition: @composition, e_tag: e_tag }

    @response = send_update_work_request(request_options)
  end

  def unsent_cowriters
    @unsent_cowriters ||= cowriters.where(provider_identifier: nil)
  end
end
