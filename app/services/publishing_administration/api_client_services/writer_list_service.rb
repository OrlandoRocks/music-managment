class PublishingAdministration::ApiClientServices::WriterListService < PublishingAdministration::ApiClientServices::Base
  PAGE_SIZE = 100

  attr_reader :account, :writers, :writer

  def self.get_list(params)
    new(params).get_list
  end

  def self.get_writer_code(params)
    new(params).get_writer_code
  end

  def self.writer_codes_list(account)
    new(account).writer_codes_list
  end

  def initialize(writer:, account: nil)
    super
    @writer = writer
    @account = account || @writer.account
    @writers = []
  end

  def get_list
    return if @account.provider_account_id.nil?

    list_of_writers
    @response.body
  end

  def get_writer_code
    get_list
    return if (writer.try(:is_a_cowriting_composer?) && writer.unknown?) || writers_by_name.count != 1

    writers_by_name.first["WriterCode"]
  end

  def writer_codes_list
    return if account.provider_account_id.nil?

    list_of_writers
    writers.map { |writer| writer["WriterCode"] }
  end

  private

  def list_of_writers(page = 1)
    options = { artist_account_code: @account.provider_account_id, page: page }
    @response = send_get_writer_list_request(options)

    @writers += @response.writers
    list_of_writers(page + 1) if @response.total_count > (PAGE_SIZE * page)
  end

  def writers_by_name
    writers.find_all do |w|
      writer.first_name.strip.casecmp(w["FirstName"]).zero? &&
        writer.last_name.strip.casecmp(w["LastName"]).zero?
    end
  end
end
