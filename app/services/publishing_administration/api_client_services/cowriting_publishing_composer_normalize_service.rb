class PublishingAdministration::ApiClientServices::CowritingPublishingComposerNormalizeService < PublishingAdministration::ApiClientServices::Base
  def self.reconcile_cowriting_publishing_composers(params)
    new(params).reconcile_cowriting_publishing_composers
  end

  def initialize(params)
    super
    @account = params[:account]
    @api_publishing_composers = params[:api_publishing_composers]
    @api_cowriting_publishing_composers = params[:api_cowriting_publishing_composers]
    @cowriting_publishing_composers = params[:cowriting_publishing_composers]
  end

  def reconcile_cowriting_publishing_composers
    compare_cowriting_publishing_composers_to_rights_app
    handle_remaining_cowriting_publishing_composers if other_cowriting_publishing_composers?
  end

  private

  def compare_cowriting_publishing_composers_to_rights_app
    @cowriting_publishing_composers.each do |cowriting_publishing_composer|
      next if cowriting_publishing_composer.provider_identifier.blank?

      response = send_get_writer_request(writer: cowriting_publishing_composer, publishing_changeover_enabled: true)

      if response.needs_update?(cowriting_publishing_composer)
        update_cowriting_publishing_composer(cowriting_publishing_composer, response)
      end
    end
  end

  def create_or_update_cowriting_publishing_composer(response)
    cowriting_publishing_composer = match_cowriting_publishing_composer(response)

    if cowriting_publishing_composer.present?
      update_cowriting_publishing_composer(cowriting_publishing_composer, response)
    else
      PublishingComposer.create!(
        first_name: response.first_name,
        last_name: response.last_name,
        account_id: account.id,
        person_id: account.person_id,
        provider_identifier: response.writer_code,
        is_unknown: response.unknown?,
        is_primary_composer: false
      )
    end
  end

  def handle_remaining_cowriting_publishing_composers
    missing_codes.each do |code|
      response = send_get_writer_request({ writer_code: code, account: @account, publishing_changeover_enabled: true })
      create_or_update_cowriting_publishing_composer(response)
    end
  end

  def match_cowriting_publishing_composer(response)
    cowriting_publishing_composer = PublishingComposer.find_by(provider_identifier: response.writer_code, is_primary_composer: false)

    return cowriting_publishing_composer if cowriting_publishing_composer.present?

    return account.cowriting_composers.where(is_unknown: true).first if response.unknown?

    account.cowriting_composers.find { |c| names_match?(c, response) }
  end

  def missing_codes
    codes           = @api_cowriting_publishing_composers.map(&:writer_code)
    existing_codes  = @cowriting_publishing_composers.map(&:provider_identifier).compact
    codes - existing_codes
  end

  def names_match?(cowriting_publishing_composer, response)
    cowriting_publishing_composer.first_name.present? &&
      cowriting_publishing_composer.last_name.present? &&
      cowriting_publishing_composer.first_name.strip.casecmp(response.first_name.strip).zero? &&
      cowriting_publishing_composer.last_name.strip.casecmp(response.last_name.strip).zero?
  end

  def other_cowriting_publishing_composers?
    @cowriting_publishing_composers.count < @api_cowriting_publishing_composers.count || !@cowriting_publishing_composers.any?(&:provider_identifier)
  end

  def update_cowriting_publishing_composer(cowriting_publishing_composer, response)
    writer_code = response.writer_code

    cowriting_publishing_composer.update(
      first_name: response.first_name,
      last_name: response.last_name,
      is_unknown: response.unknown?,
      provider_identifier: writer_code
    )
  end
end
