class PublishingAdministration::ApiClientServices::PublishingWriterService < PublishingAdministration::ApiClientServices::Base
  def self.create_or_update(publishing_composer)
    new(publishing_composer).create_or_update
  end

  def initialize(publishing_composer)
    super
    @publishing_composer = publishing_composer
    @account = publishing_composer.account
  end

  def create_or_update
    return if account_blocked?

    if publishing_composer.provider_identifier.present?
      fetch_sentric_writer
      create_sentric_writer if @response.writer_code.nil?
      update_sentric_writer if @response.needs_update?(publishing_composer)
    else
      create_sentric_writer
    end

    @response.body if @response
  end

  private

  attr_reader :account, :api_client, :publishing_composer

  def create_sentric_writer
    PublishingAdministration::ApiClientServices::WriterCodeService.create_or_update(publishing_composer)
  end

  def fetch_sentric_writer
    @response = send_get_writer_request(writer: publishing_composer, publishing_changeover_enabled: true)
  end

  def update_sentric_writer
    e_tag = @response.e_tag || fetch_sentric_writer.e_tag
    @response = send_update_writer_request(
      writer: publishing_composer,
      e_tag: e_tag,
      publishing_changeover_enabled: true
    )
  end
end
