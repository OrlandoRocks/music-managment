class PublishingAdministration::ApiClientServices::CowriterNormalizeService < PublishingAdministration::ApiClientServices::Base
  def self.reconcile_cowriters(params)
    new(params).reconcile_cowriters
  end

  def initialize(params)
    super
    @account        = params[:account]
    @api_composers  = params[:api_composers]
    @api_cowriters  = params[:api_cowriters]
    @cowriters      = params[:cowriters]
  end

  def reconcile_cowriters
    compare_cowriters_to_rights_app
    handle_remaining_cowriters if other_cowriters?
  end

  private

  def compare_cowriters_to_rights_app
    @cowriters.each do |cowriter|
      next if cowriter.provider_identifier.blank?

      response = send_get_writer_request(writer: cowriter)

      update_cowriter(cowriter, response) if response.needs_update?(cowriter)
    end
  end

  def composer_by_provider_identifier
    @composer ||= Composer.find_by(provider_identifier: @api_composers.first.writer_code)
  end

  def create_or_update_cowriter(response)
    cowriter = match_cowriter(response)

    if cowriter.present?
      update_cowriter(cowriter, response)
    else
      Cowriter.create!(
        first_name: response.first_name,
        last_name: response.last_name,
        composer_id: composer_by_provider_identifier.id,
        provider_identifier: response.writer_code,
        is_unknown: response.unknown?
      )
    end
  end

  def handle_remaining_cowriters
    missing_codes.each do |code|
      response = send_get_writer_request({ writer_code: code, account: @account })
      create_or_update_cowriter(response)
    end
  end

  def match_cowriter(response)
    cowriter = Cowriter.find_by(provider_identifier: response.writer_code)
    return cowriter if cowriter.present?

    if response.unknown?
      composer_by_provider_identifier.cowriters.where(is_unknown: true).first
    else
      composer_by_provider_identifier.cowriters.find { |c| names_match?(c, response) }
    end
  end

  def missing_codes
    codes           = @api_cowriters.map(&:writer_code)
    existing_codes  = @cowriters.map(&:provider_identifier).compact
    codes - existing_codes
  end

  def names_match?(cowriter, response)
    cowriter.first_name.present? &&
      cowriter.last_name.present? &&
      cowriter.first_name.strip.casecmp(response.first_name.strip).zero? &&
      cowriter.last_name.strip.casecmp(response.last_name.strip).zero?
  end

  def other_cowriters?
    @cowriters.count < @api_cowriters.count || !@cowriters.any?(&:provider_identifier)
  end

  def update_cowriter(cowriter, response)
    writer_code = response.writer_code

    cowriter.update(
      first_name: response.first_name,
      last_name: response.last_name,
      is_unknown: response.unknown?,
      provider_identifier: writer_code
    )
  end
end
