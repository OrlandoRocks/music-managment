class PublishingAdministration::ApiClientServices::FetchWorkListService < PublishingAdministration::ApiClientServices::Base
  PAGE_SIZE = 100

  def self.fetch(provider_account_id)
    new(provider_account_id).tap(&:fetch)
  end

  def self.work_codes(provider_account_id)
    new(provider_account_id).work_codes
  end

  def initialize(provider_account_id)
    super
    @provider_account_id = provider_account_id
    @works               = []
  end

  def fetch
    work_list
  end

  def works
    @works.flatten.compact
  end

  def work_codes
    work_list
    works.map(&:work_code)
  end

  def valid?
    works.present? && !@response.errors?
  end

  private

  attr_reader :provider_account_id, :api_client

  def work_list(page = 1)
    return if provider_account_id.blank?

    options = { artist_account_code: provider_account_id, page: page }
    @response = send_get_work_list_request(options)

    @works << @response.works
    work_list(page + 1) if @response.total_count > (PAGE_SIZE * page)
  end
end
