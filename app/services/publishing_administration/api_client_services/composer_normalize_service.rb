class PublishingAdministration::ApiClientServices::ComposerNormalizeService < PublishingAdministration::ApiClientServices::Base
  def self.reconcile_composers(params)
    new(params).reconcile_composers
  end

  def initialize(params)
    super
    @account        = params[:account]
    @api_composers  = params[:api_composers]
    @composers      = params[:composers]
    @free_purchase  = params[:free_purchase] || false
  end

  def reconcile_composers
    return if account_blocked?

    compare_composers_to_rights_app
    handle_remaining_composers if other_composers?
  end

  private

  def compare_composers_to_rights_app
    @composers.each do |composer|
      next if composer.provider_identifier.blank?

      response = get_writer(writer: composer)
      update_composer(composer, response) if response.needs_update?(composer)
    end
  end

  def create_or_update_composer(response)
    composer = match_composer(response)

    if composer.present?
      update_composer(composer, response)
    else
      params = { account: @account, api_response: response, free_purchase: @free_purchase }
      PublishingAdministration::CreateComposerService.create_composer(params)
    end
  end

  def get_writer(opts)
    send_get_writer_request(opts)
  end

  def handle_remaining_composers
    missing_codes.each do |code|
      opts      = { writer_code: code, account: @account }
      response  = get_writer(opts)
      create_or_update_composer(response)
    end
  end

  def match_composer(response)
    composer = Composer.find_by(provider_identifier: response.writer_code)
    composer || @account.composers.find { |c| names_match?(c, response) }
  end

  def missing_codes
    codes           = @api_composers.map(&:writer_code)
    existing_codes  = @composers.map(&:provider_identifier).compact
    codes - existing_codes
  end

  def names_match?(composer, response)
    composer.first_name.present? &&
      composer.last_name.present? &&
      composer.first_name.strip.casecmp(response.first_name.strip).zero? &&
      composer.last_name.strip.casecmp(response.last_name.strip).zero?
  end

  def other_composers?
    @composers.count < @api_composers.count || !@composers.any?(&:provider_identifier)
  end

  def performing_rights_org(society_id)
    PerformingRightsOrganization.find_by(provider_identifier: society_id)
  end

  def update_composer(composer, response)
    pro       = performing_rights_org(response.society_id)
    publisher = composer.publisher

    composer.update(
      first_name: response.first_name,
      middle_name: response.middle_name,
      last_name: response.last_name,
      cae: pro.present? ? response.cae : nil,
      performing_rights_organization_id: pro.try(:id),
      provider_identifier: response.writer_code
    )

    update_publisher(publisher, response) unless publisher.nil?
  end

  def update_publisher(publisher, response)
    publisher.update(
      name: response.publisher_name,
      cae: response.publisher_cae
    )
  end
end
