class PublishingAdministration::ApiClientServices::TerminatedComposerAccountService < PublishingAdministration::ApiClientServices::Base
  def self.post_terminated_composer(params)
    new(params).post_terminated_composer
  end

  def initialize(params)
    super
    @terminated_composer = params[:terminated_composer]
    @account = @terminated_composer.try(:account)
  end

  def post_terminated_composer
    return if account_blocked?

    return unless @terminated_composer.partially_terminated?

    create_artist_account
    save_artist_account if @response.artist_account_code.present?
  end

  private

  def create_artist_account
    @response = send_create_artist_account_request(request_options)
  end

  def request_options
    { account: @terminated_composer.account }
  end

  def save_artist_account
    @terminated_composer.update(alt_provider_acct_id: @response.artist_account_code)
  end
end
