class PublishingAdministration::ApiClientServices::PrimaryPublishingComposerNormalizeService < PublishingAdministration::ApiClientServices::Base
  def self.reconcile_publishing_composers(params)
    new(params).reconcile_publishing_composers
  end

  def initialize(params)
    super
    @account = params[:account]
    @api_publishing_composers  = params[:api_publishing_composers]
    @publishing_composers      = params[:publishing_composers]
    @free_purchase = params[:free_purchase] || false
  end

  def reconcile_publishing_composers
    return if account_blocked?

    compare_publishing_composers_to_rights_app
    handle_remaining_publishing_composers if other_publishing_composers?
  end

  private

  def compare_publishing_composers_to_rights_app
    @publishing_composers.each do |publishing_composer|
      next if publishing_composer.provider_identifier.blank?

      response = get_writer(writer: publishing_composer)
      update_publishing_composer(publishing_composer, response) if response.needs_update?(publishing_composer)
    end
  end

  def create_or_update_publishing_composer(response)
    publishing_composer = match_publishing_composer(response)

    return update_publishing_composer(publishing_composer, response) if publishing_composer.present?

    params = { account: @account, api_response: response, free_purchase: @free_purchase }

    PublishingAdministration::CreatePublishingComposerService.create_publishing_composer(params)
  end

  def get_writer(opts)
    send_get_writer_request(opts)
  end

  def handle_remaining_publishing_composers
    missing_codes.each do |code|
      opts      = { writer_code: code, account: @account, publishing_changeover_enabled: true }
      response  = get_writer(opts)
      create_or_update_publishing_composer(response)
    end
  end

  def match_publishing_composer(response)
    publishing_composer = PublishingComposer.find_by(provider_identifier: response.writer_code)
    publishing_composer || @account.publishing_composers.find { |c| names_match?(c, response) }
  end

  def missing_codes
    codes           = @api_publishing_composers.map(&:writer_code)
    existing_codes  = @publishing_composers.map(&:provider_identifier).compact
    codes - existing_codes
  end

  def names_match?(publishing_composer, response)
    publishing_composer.first_name.present? &&
      publishing_composer.last_name.present? &&
      publishing_composer.first_name.strip.casecmp(response.first_name.strip).zero? &&
      publishing_composer.last_name.strip.casecmp(response.last_name.strip).zero?
  end

  def other_publishing_composers?
    @publishing_composers.count < @api_publishing_composers.count || !@publishing_composers.any?(&:provider_identifier)
  end

  def performing_rights_org(society_id)
    PerformingRightsOrganization.find_by(provider_identifier: society_id)
  end

  def update_publishing_composer(publishing_composer, response)
    pro       = performing_rights_org(response.society_id)
    publisher = publishing_composer.publisher

    publishing_composer.update(
      first_name: response.first_name,
      middle_name: response.middle_name,
      last_name: response.last_name,
      cae: pro.present? ? response.cae : nil,
      performing_rights_organization_id: pro.try(:id),
      provider_identifier: response.writer_code
    )

    update_publisher(publisher, response) unless publisher.nil?
  end

  def update_publisher(publisher, response)
    publisher.update(
      name: response.publisher_name,
      cae: response.publisher_cae
    )
  end
end
