class PublishingAdministration::ApiClientServices::PublishingWorkCreationService < PublishingAdministration::ApiClientServices::Base
  def self.post_work(params)
    new(params).post_work
  end

  def self.update_work(params)
    new(params).update_work
  end

  def initialize(params)
    super
    @publishing_composer           = params[:publishing_composer]
    @publishing_composition        = params[:publishing_composition]
    @publishing_composition_splits = @publishing_composition.publishing_composition_splits
    @account                       = @publishing_composer&.account || @publishing_composition&.account
  end

  def post_work
    return if account_blocked? || @publishing_composition.ineligible?
    return if @publishing_composition_splits.pluck(:percent).any? { |p| p == 0.to_d }

    create_cowriters if unsent_cowriters.present?
    send_create_request

    if @response.already_exists? && @response.existing_work_code != @publishing_composition.provider_identifier
      if @response.existing_work_code.present?
        @publishing_composition.update(provider_identifier: @response.existing_work_code)
      end
    elsif @response.work_code.present?
      @publishing_composition.update(provider_identifier: @response.work_code)
    end

    @response.body
  end

  def update_work
    return if account_blocked? || @publishing_composition.ineligible?
    return if @publishing_composition_splits.pluck(:percent).any? { |p| p == 0.to_d }

    create_cowriters if unsent_cowriters.present?
    send_update_request

    @response.body
  end

  private

  def unsent_cowriters
    @unsent_cowriters ||= cowriters.where(provider_identifier: nil)
  end

  def cowriters
    cowriter_ids = @publishing_composition_splits.where(right_to_collect: false).pluck(:publishing_composer_id)
    PublishingComposer.where(id: cowriter_ids, is_primary_composer: false)
  end

  def create_cowriters
    unsent_cowriters.each do |cowriter|
      PublishingAdministration::ApiClientServices::PublishingWriterService.create_or_update(cowriter)
    end
  end

  def send_create_request
    request_options = { composer: @publishing_composer, composition: @publishing_composition }

    @response = send_create_work_request(request_options)
  end

  def send_update_request
    e_tag = send_get_work_request(
      work_code: @publishing_composition.provider_identifier,
      publishing_changeover_enabled: true
    ).e_tag

    request_options = { composition: @publishing_composition, e_tag: e_tag }

    @response = send_update_work_request(request_options)
  end
end
