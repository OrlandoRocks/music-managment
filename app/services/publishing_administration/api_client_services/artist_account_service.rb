class PublishingAdministration::ApiClientServices::ArtistAccountService < PublishingAdministration::ApiClientServices::Base
  def self.find_or_create_artist(params)
    new(params).find_or_create_artist
  end

  def initialize(params)
    super
    @account = params[:account]
  end

  def find_or_create_artist
    return if account_blocked?

    if @account.provider_account_id.present?
      get_artist_account
      create_artist_account if should_create_artist_account?
    else
      create_artist_account
      create_artist_account if @response.retry?
    end

    save_artist_account unless @account.provider_account_id == @response.artist_account_code

    @response.body
  end

  private

  def get_artist_account
    @response = send_get_artist_account_request(account: @account)
  end

  def create_artist_account
    @response = send_create_artist_account_request(account: @account)
  end

  def should_create_artist_account?
    @response.artist_account_code.nil? || @response.is_deleted
  end

  def save_artist_account
    @account.update(provider_account_id: @response.artist_account_code)
  end
end
