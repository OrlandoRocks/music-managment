class PublishingAdministration::ApiClientServices::WriterImporterService < PublishingAdministration::ApiClientServices::Base
  attr_reader :composer, :response

  def self.import(composer)
    new(composer).import
  end

  def initialize(composer)
    super
    @composer   = composer
    @account    = composer.account
  end

  def import
    return if account_blocked?

    create_writer_code if @composer.provider_identifier.blank?
    @response = send_get_writer_request(writer: composer)

    return unless response.needs_update?(composer)

    composer.update(composer_params)
    composer.create_or_update_publisher(publisher_params) if composer.publisher.present? || response.has_publisher?
  end

  private

  def composer_params
    {
      first_name: response.first_name,
      middle_name: response.middle_name,
      last_name: response.last_name,
      cae: response.cae,
      provider_identifier: response.writer_code,
      performing_rights_organization_id: pro_id
    }
  end

  def publisher_params
    {
      name: response.publisher_name,
      cae: response.publisher_cae,
      performing_rights_organization_id: pro_id
    }
  end

  def pro_id
    PerformingRightsOrganization.find_by(provider_identifier: response.society_id)&.id
  end

  def create_writer_code
    PublishingAdministration::ApiClientServices::WriterCodeService.create_or_update(composer)
  end
end
