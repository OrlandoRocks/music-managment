class PublishingAdministration::ApiClientServices::Base
  attr_reader :api_client, :account

  CREATE_ARTIST_ACCOUNT = "CreateArtistAccount".freeze
  CREATE_RECORDING = "CreateRecording".freeze
  CREATE_WORK = "CreateWork".freeze
  CREATE_WRITER = "CreateWriter".freeze
  GET_ARTIST_ACCOUNT = "GetArtistAccount".freeze
  GET_RECORDING_LIST = "GetRecordingList".freeze
  GET_SOCIETY_LIST = "GetSocietyList".freeze
  GET_WORK = "GetWork".freeze
  GET_WORK_LIST = "GetWorkList".freeze
  GET_WRITER = "GetWriter".freeze
  GET_WRITER_LIST = "GetWriterList".freeze
  UPDATE_WORK = "UpdateWork".freeze
  UPDATE_WRITER = "UpdateWriter".freeze

  def initialize(_params = {})
    @api_client = $rights_app_api_client
  end

  private

  def account_blocked?
    return false if account.blank?

    account.blocked?
  end

  def send_api_request(action, options)
    api_client.send_request(action, options)
  end

  def send_create_artist_account_request(options)
    send_api_request(CREATE_ARTIST_ACCOUNT, options)
  end

  def send_create_recording_request(options)
    send_api_request(CREATE_RECORDING, options)
  end

  def send_create_work_request(options)
    send_api_request(CREATE_WORK, options)
  end

  def send_create_writer_request(options)
    send_api_request(CREATE_WRITER, options)
  end

  def send_get_artist_account_request(options)
    send_api_request(GET_ARTIST_ACCOUNT, options)
  end

  def send_get_recording_list_request(options)
    send_api_request(GET_RECORDING_LIST, options)
  end

  def send_get_society_list_request(options)
    send_api_request(GET_SOCIETY_LIST, options)
  end

  def send_get_work_request(options)
    send_api_request(GET_WORK, options)
  end

  def send_get_work_list_request(options)
    send_api_request(GET_WORK_LIST, options)
  end

  def send_get_writer_request(options)
    send_api_request(GET_WRITER, options)
  end

  def send_get_writer_list_request(options)
    send_api_request(GET_WRITER_LIST, options)
  end

  def send_update_work_request(options)
    send_api_request(UPDATE_WORK, options)
  end

  def send_update_writer_request(options)
    send_api_request(UPDATE_WRITER, options)
  end
end
