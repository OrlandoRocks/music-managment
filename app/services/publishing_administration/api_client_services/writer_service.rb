class PublishingAdministration::ApiClientServices::WriterService < PublishingAdministration::ApiClientServices::Base
  def self.create_or_update(writer)
    new(writer).create_or_update
  end

  def initialize(writer)
    super
    @writer       = writer
    @composer     = writer.is_a?(Composer) ? writer : writer.composer
    @account      = composer.account
  end

  def create_or_update
    return if account_blocked?

    if writer.provider_identifier.present?
      fetch_sentric_writer
      create_sentric_writer if @response.writer_code.nil?
      update_sentric_writer if @response.needs_update?(writer)
    else
      create_sentric_writer
    end

    @response.body if @response
  end

  private

  attr_reader :account, :api_client, :composer, :writer

  def create_sentric_writer
    PublishingAdministration::ApiClientServices::WriterCodeService.create_or_update(writer)
  end

  def fetch_sentric_writer
    @response = send_get_writer_request(writer: writer)
  end

  def update_sentric_writer
    e_tag     = @response.e_tag || fetch_sentric_writer.e_tag
    @response = send_update_writer_request(writer: writer, e_tag: e_tag)
  end
end
