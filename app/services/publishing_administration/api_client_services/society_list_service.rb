class PublishingAdministration::ApiClientServices::SocietyListService < PublishingAdministration::ApiClientServices::Base
  PRO_NAMES_TO_SKIP = Set["AMCOS", "1Test", "2test"]

  PAGE_SIZE = 100
  SESAC_IDS = [191, 324].freeze

  def self.list
    new.list
  end

  def initialize
    super
    @societies = []
  end

  def list
    society_list
  end

  private

  def create_pro(society)
    return if PRO_NAMES_TO_SKIP.include?(society["name"])

    PerformingRightsOrganization.create(
      name: society["name"],
      provider_identifier: society["id"],
      is_deleted: society["isdeleted"],
      cicaccode: society["cicaccode"],
    )
  end

  def handle_sesac_pros(pro, society)
    other_pros = PerformingRightsOrganization.where("name LIKE '%SESAC%' AND id != #{pro.id}")
    other_pros.each { |other_pro| update_pro(other_pro, society) }
  end

  def society_list
    request_societies

    @societies.each do |society|
      pro = PerformingRightsOrganization.find_by(name: society["name"])
      if pro.present?
        update_pro(pro, society)
        handle_sesac_pros(pro, society) if SESAC_IDS.include?(pro.id)
      else
        create_pro(society)
      end
    end
  end

  def request_societies(page = 1)
    @response = send_get_society_list_request(page: page)

    @societies += @response.societies unless @response.errors?
    request_societies(page + 1) if @response.total_count > (PAGE_SIZE * page)
  end

  def update_pro(pro, society)
    return if pro.provider_identifier == society["id"]

    pro.update(provider_identifier: society["id"])
  end
end
