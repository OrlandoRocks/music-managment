class PublishingAdministration::ApiClientServices::PublishingRecordingService < PublishingAdministration::ApiClientServices::Base
  def self.post_recording(params)
    new(params).post_recording
  end

  def initialize(params)
    super
    @publishing_composition  = params[:publishing_composition]
    @publishing_composer     = params[:publishing_composer]
    @account = @publishing_composer&.account
  end

  def post_recording
    return if account_blocked? || @publishing_composition.ineligible?
    return unless song.present? || non_tunecore_song.present?

    create_work if @publishing_composition.provider_identifier.blank?

    create_recordings
  end

  private

  def create_recordings
    missing_recordings = @publishing_composition.recordings.where(recording_code: [nil, ""]).to_a

    if @publishing_composition.recordings.blank?
      missing_recordings << @publishing_composition.recordings.build(recordable: @publishing_composition.song)
    end

    return if missing_recordings.blank?

    missing_recordings.each do |recording|
      @e_tag = send_get_work_request(
        work_code: @publishing_composition.provider_identifier,
        publishing_changeover_enabled: true
      ).e_tag

      response = send_create_recording_request(request_options)
      recording.update(recording_code: response.recording_id) if response.recording_id.present?
    end
  end

  def create_work
    params = { publishing_composer: @publishing_composer, publishing_composition: @publishing_composition }
    PublishingAdministration::ApiClientServices::PublishingWorkCreationService.post_work(params)
  end

  def request_options
    {
      composition: @publishing_composition,
      e_tag: @e_tag,
      song: song,
      non_tunecore_song: non_tunecore_song
    }
  end

  def song
    @song ||= @publishing_composition.songs.first
  end

  def non_tunecore_song
    @non_tunecore_song ||= @publishing_composition.non_tunecore_songs.first
  end
end
