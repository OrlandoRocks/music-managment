class PublishingAdministration::ApiClientServices::RecordingService < PublishingAdministration::ApiClientServices::Base
  def self.post_recording(params)
    new(params).post_recording
  end

  def initialize(params)
    super
    @composition  = params[:composition]
    @composer     = params[:composer]
    @account      = @composer&.account
  end

  def post_recording
    return if account_blocked? || @composition.ineligible?
    return unless song.present? || non_tunecore_song.present?

    create_work if @composition.provider_identifier.blank?

    create_recordings
  end

  private

  def create_recordings
    missing_recordings = @composition.recordings.where(recording_code: [nil, ""]).to_a

    missing_recordings << @composition.recordings.build(recordable: @composition.song) if @composition.recordings.blank?

    return if missing_recordings.blank?

    missing_recordings.each do |recording|
      @e_tag = send_get_work_request(work_code: @composition.provider_identifier).e_tag

      response = send_create_recording_request(request_options)
      recording.update(recording_code: response.recording_id)
    end
  end

  def create_work
    params = { composer: @composer, composition: @composition }
    PublishingAdministration::ApiClientServices::WorkCreationService.post_work(params)
  end

  def request_options
    {
      composition: @composition,
      e_tag: @e_tag,
      song: song,
      non_tunecore_song: non_tunecore_song
    }
  end

  def song
    @song ||= @composition.songs.first
  end

  def non_tunecore_song
    @non_tunecore_song ||= @composition.non_tunecore_songs.first
  end
end
