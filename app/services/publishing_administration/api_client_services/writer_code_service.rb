class PublishingAdministration::ApiClientServices::WriterCodeService < PublishingAdministration::ApiClientServices::Base
  attr_reader :writer, :account

  def self.create_or_update(writer)
    new(writer).create_or_update
  end

  def initialize(writer)
    super
    @writer = writer
    @account = writer.account
  end

  def create_or_update
    return if account_blocked?

    if account.provider_account_id.nil?
      PublishingAdministration::ApiClientServices::ArtistAccountService.find_or_create_artist(account: account)
    end

    writer_code = get_writer_code_from_rights_app || create_new_writer_code
    writer.update(provider_identifier: writer_code)
  end

  private

  def get_writer_code_from_rights_app
    PublishingAdministration::ApiClientServices::WriterListService.get_writer_code(writer: writer)
  end

  def create_new_writer_code
    send_create_writer_request(writer)&.writer_code
  end
end
