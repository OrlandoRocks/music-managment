class PublishingAdministration::PublishingWorkListService
  attr_reader :account, :free_purchase, :work_list, :unmatched_works, :matched_publishing_compositions

  def self.list(account, free_purchase = false)
    new(account, free_purchase).list
  end

  def initialize(account, free_purchase = false)
    @account = account
    @free_purchase = free_purchase
    @work_list = fetch_work_list
    @unmatched_works = work_matcher.unmatched_works
    @matched_publishing_compositions = work_matcher.matched_publishing_compositions
  end

  def list
    return unless work_list.valid?

    create_compositions_for_unmatched_works if unmatched_works.present?
    update_matched_publishing_compositions if matched_publishing_compositions.present?
    update_terminated_compositions_state
  end

  private

  def create_compositions_for_unmatched_works
    unmatched_works.each do |work|
      next if PublishingComposition.exists?(name: work.title, provider_identifier: work.work_code)

      writer_code = work.writers.find(&:right_to_collect)&.writer_code
      publishing_composer = account.publishing_composers.find_by(provider_identifier: writer_code)
      next if publishing_composer.blank?

      publishing_composition = PublishingComposition.where(
        name: work.title,
        account: account,
        provider_identifier: work.work_code
      ).first_or_create

      params = { publishing_composer: publishing_composer, publishing_composition: publishing_composition }
      PublishingAdministration::PublishingNonTunecoreService.create_ntc_album_and_song(params)

      PublishingAdministration::PublishingWorkUpdateService.update(
        publishing_composition.reload,
        work,
        @account,
        free_purchase
      )
    end
  end

  def update_matched_publishing_compositions
    matched_publishing_compositions.each do |composition, work|
      PublishingAdministration::PublishingWorkUpdateService.update(composition, work, @account, free_purchase)
    end
  end

  def fetch_work_list
    PublishingAdministration::ApiClientServices::FetchWorkListService.fetch(account.provider_account_id)
  end

  def work_matcher
    @work_matcher ||=
      PublishingAdministration::PublishingWorkMatcherService.match(account, work_list.works)
  end

  def update_terminated_compositions_state
    PublishingAdministration::UpdateTerminatedPublishingCompositionsService.update(account)
  end
end
