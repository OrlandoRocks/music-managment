class PublishingAdministration::NtcBulkUploadService
  include ActionView::Helpers::NumberHelper

  attr_accessor :errors, :publishing_compositions

  HEADERS = {
    title: "Composition Title",
    artist_name: "Performing Artist",
    release_date: "Release Date",
    appears_on: "Appears On",
    record_label: "Record Label",
    isrc: "ISRC",
    percent: "Your Share %",
    cowriter_1_name: "Cowriter #1 (First and Last name)",
    cowriter_1_share: "Cowriter #1 Share",
    cowriter_2_name: "Cowriter #2 (First and Last name)",
    cowriter_2_share: "Cowriter #2 Share",
    cowriter_3_name: "Cowriter #3 (First and Last name)",
    cowriter_3_share: "Cowriter #3 Share",
    public_domain: "Is Public Domain"
  }

  MAXIMUM_COMPOSITIONS = 1000

  def initialize(person_id, publishing_composer_id, file)
    @person = Person.find_by(id: person_id)
    @publishing_composer = PublishingComposer.primary.find_by(id: publishing_composer_id)
    @file = file

    @errors = {}
    @publishing_compositions = []

    @isrc_pattern = Regexp.new(/\A[A-Z]{2}[A-Z0-9]{3}[0-9]{7}\z|\A\z/).freeze
  end

  def self.sample_upload_file
    CSV.generate(headers: true) do |csv|
      csv << get_headers.values
    end
  end

  def process
    csv = CSV.parse(@file, headers: true)

    return unless validate_entry_count(csv)

    @publishing_compositions = to_hashes(csv)
    @csv_titles ||= @publishing_compositions.pluck(:title).compact.map(&:downcase)
    @csv_isrcs ||= @publishing_compositions.map { |c| c[:isrc]&.upcase }.compact

    validate_publishing_compositions
    return if @errors.present?

    create_publishing_compositions
  end

  def create_publishing_compositions
    @publishing_compositions.each do |publishing_composition|
      PublishingAdministration::NtcCreationWorker.perform_async(publishing_composition_params(publishing_composition))
    end
  end

  private

  def to_hashes(csv)
    csv.map do |row|
      transform_keys(row.to_h)
    end
  end

  def transform_keys(hsh)
    new_hsh = {}

    self.class.get_headers.each do |key, display_name|
      new_hsh[key] = hsh[display_name]
    end

    new_hsh.with_indifferent_access
  end

  def validate_entry_count(csv)
    if csv.length > MAXIMUM_COMPOSITIONS
      set_max_exceeded_error
      return false
    elsif csv.length.zero?
      set_no_entries_error
      return false
    end

    true
  end

  def validate_publishing_compositions
    @publishing_compositions.each_with_index do |publishing_composition, index|
      validate_single_publishing_composition(publishing_composition, index + 1)

      return if @errors.present?
    end

    bulk_validate_publishing_compositions
  end

  def validate_single_publishing_composition(publishing_composition, line)
    return set_missing_composition_info_error(line) unless info_present?(publishing_composition)

    invalid_columns = get_invalid_columns(publishing_composition)
    set_invalid_composition_info_error(line, invalid_columns.join(", ")) if invalid_columns.present?
  end

  def info_present?(publishing_composition)
    publishing_composition[:title].present? &&
      publishing_composition[:artist_name].present? &&
      publishing_composition[:percent].present?
  end

  def get_invalid_columns(publishing_composition)
    invalid_columns = []
    invalid_columns.push("release_date") unless valid_release_date?(publishing_composition[:release_date])
    invalid_columns.push("isrc") unless valid_isrc?(publishing_composition)
    invalid_columns.push("your_share") unless valid_percent_value?(publishing_composition[:percent])
    invalid_columns.push("cowriter_share") unless valid_cowriter_shares?(publishing_composition)
    invalid_columns.push("total_share_sum") unless valid_share_allocation?(publishing_composition)
    invalid_columns.push("public_domain") unless valid_public_domain_value?(publishing_composition)
    invalid_columns
  end

  def valid_percent_value?(percent)
    percent.to_d.between?(1, 100)
  end

  def valid_cowriter_shares?(publishing_composition)
    return true if cowriter_params(publishing_composition).blank?

    cowriter_params(publishing_composition).all? do |cowriter|
      cowriter.values.all?(&:present?) && cowriter[:cowriter_share].to_d.between?(1, 100)
    end
  end

  def cowriter_params(publishing_composition)
    (1..3).map do |i|
      first_name, last_name = publishing_composition["cowriter_#{i}_name"].to_s.split(" ")
      share = publishing_composition["cowriter_#{i}_share"]

      next if first_name.blank? && share.blank?

      {
        first_name: first_name,
        last_name: last_name,
        cowriter_share: share
      }
    end.compact
  end

  def valid_release_date?(release_date)
    return true if release_date.blank?
    return true if release_date.respond_to?(:strftime)

    begin
      Date.parse(release_date)
      true
    rescue
      false
    end
  end

  def valid_share_allocation?(publishing_composition)
    percent = publishing_composition[:percent].to_d
    cowriter_percent = cowriter_params(publishing_composition).sum { |c| c[:cowriter_share].to_d }

    (cowriter_percent + percent) <= 100
  end

  def valid_public_domain_value?(publishing_composition)
    return true if publishing_composition[:public_domain].blank?

    ["true", "false"].include?(publishing_composition[:public_domain].strip.downcase)
  end

  def valid_isrc?(publishing_composition)
    return true if publishing_composition[:isrc].blank?

    @isrc_pattern.match?(publishing_composition[:isrc])
  end

  def bulk_validate_publishing_compositions
    isrc_use_is_unique? && title_use_is_unique?
  end

  def isrc_use_is_unique?
    if @csv_isrcs.count != @csv_isrcs.uniq.count
      set_duplicate_isrc_in_file_error(@csv_isrcs)
      return
    end

    if @publishing_composer.person_id.present?
      validate_isrc_use_in_distro_songs(@csv_isrcs)
      return if @errors.present?
    end

    validate_isrc_use_in_ntc_songs(@csv_isrcs)
    @errors.blank?
  end

  def validate_isrc_use_in_distro_songs(isrcs)
    songs = Song.joins(:album).where(
      "albums.person_id = ? AND (songs.tunecore_isrc IN (?) OR songs.optional_isrc IN (?))",
      @publishing_composer.person_id,
      isrcs,
      isrcs
    )

    set_duplicate_isrc_error(songs, @csv_isrcs) if songs.present?
  end

  def validate_isrc_use_in_ntc_songs(isrcs)
    ntc_songs = NonTunecoreSong.joins(:non_tunecore_album).where(
      non_tunecore_albums: { publishing_composer_id: @publishing_composer.id },
      non_tunecore_songs: { isrc: isrcs }
    )

    set_duplicate_isrc_error(ntc_songs, @csv_isrcs) if ntc_songs.present?
  end

  def title_use_is_unique?
    return set_duplicate_titles_in_file_error(@csv_titles) if @csv_titles.count != @csv_titles.uniq.count

    publishing_composition_titles = PublishingAdministration::PublishingCompositionsBuilder.build(
      { publishing_composer_id: @publishing_composer.id }
    )

    cleaned_titles =
      publishing_composition_titles.map { |publishing_composition|
        publishing_composition.clean_name.downcase
      }

    set_duplicate_title_error(cleaned_titles & @csv_titles, @csv_titles) if (cleaned_titles & @csv_titles).present?
  end

  def get_duplicate_indices(list, item)
    duplicates =
      list.each_with_index.group_by(&:first).reduce({}) do |result, (val, group)|
        next result if item.nil? && group.length == 1
        next result if item.present? && item != val

        result.merge val => group.map { |pair| pair[1] + 1 }
      end

    duplicates.values.flatten
  end

  def set_max_exceeded_error
    max_limit = number_with_delimiter(MAXIMUM_COMPOSITIONS)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.limit_exceeded_error",
      { max_limit: max_limit }
    )
  end

  def set_default_error
    @errors[:publishing_compositions] = I18n.t("compositions.bulk_compositions.upload_failed_error")
  end

  def set_no_entries_error
    @errors[:publishing_compositions] = I18n.t("compositions.bulk_compositions.upload_zero_rows_submitted_error")
  end

  def set_missing_composition_info_error(line)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_missing_info_error",
      { line: line }
    )
  end

  def set_invalid_composition_info_error(line, columns)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_invalid_info_error",
      { line: line, columns: columns }
    )
  end

  def set_duplicate_isrc_in_file_error(isrcs)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_duplicate_isrc_error",
      { lines: get_duplicate_indices(isrcs, nil).join(", ") }
    )
  end

  def set_duplicate_isrc_error(songs, isrcs)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_isrc_in_use_error",
      { isrc: songs[0][:isrc], lines: get_duplicate_indices(isrcs, songs[0][:isrc]).join(", ") }
    )
  end

  def set_duplicate_titles_in_file_error(titles)
    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_duplicate_title_error",
      { lines: get_duplicate_indices(titles, nil).join(", ") }
    )
  end

  def set_duplicate_title_error(titles_in_use, titles)
    proc_title = titles_in_use[0]
    proc_title = "#{proc_title[0..10]}..." if proc_title.length > 10

    @errors[:publishing_compositions] = I18n.t(
      "compositions.bulk_compositions.upload_title_in_use_error",
      { title: proc_title, lines: get_duplicate_indices(titles, titles_in_use[0]).join(", ") }
    )
  end

  def publishing_composition_params(publishing_composition)
    {
      title: publishing_composition[:title],
      artist_name: publishing_composition[:artist_name],
      album_name: publishing_composition[:appears_on],
      record_label: publishing_composition[:record_label],
      isrc_number: publishing_composition[:isrc],
      percent: publishing_composition[:percent],
      release_date: (Date.parse(publishing_composition[:release_date]) rescue nil),
      person_id: @person.id,
      publishing_composer_id: @publishing_composer.id,
      cowriter_params: cowriter_params(publishing_composition),
      public_domain: publishing_composition[:public_domain]
    }
  end

  def self.get_headers
    headers = HEADERS.clone
    headers.delete(:public_domain) unless FeatureFlipper.show_feature?(:public_domain, @person)
    headers
  end
end
