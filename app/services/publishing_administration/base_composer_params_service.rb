class PublishingAdministration::BaseComposerParamsService
  def self.composer_class
    Composer
  end

  def self.compose(composer_id)
    new(composer_id).compose
  end

  def initialize(composer_id)
    @composer  = self.class.composer_class.includes(:person, :publisher).find(composer_id)
    @person    = composer.publishing_administrator
    @publisher = composer.publisher
  end

  def compose
    if publisher.present?
      composer_params.merge!(publisher_params)
    else
      composer_params
    end.with_indifferent_access
  end

  def self.format_dob_params(params)
    enroll_params = params[:publishing_administration_enrollment_form]
    enroll_params[:dob_y] = enroll_params.delete("dob(1i)")
    enroll_params[:dob_m] = enroll_params.delete("dob(2i)")
    enroll_params[:dob_d] = enroll_params.delete("dob(3i)")
  end

  private

  attr_reader :composer, :publisher, :person

  def composer_params
    {
      id: composer.id,
      name_prefix: composer.name_prefix,
      first_name: composer.first_name,
      middle_name: composer.middle_name,
      last_name: composer.last_name,
      name_suffix: composer.name_suffix,
      dob: composer.dob,
      email: composer.current_email,
      person_id: composer.person_id || composer.account.person_id,
      composer_cae: composer.cae,
      composer_pro_id: composer.performing_rights_organization_id,
      publishing_role_id: composer.publishing_role_id
    }
  end

  def publisher_params
    {
      publisher_name: publisher.name,
      publisher_cae: publisher.cae,
      publisher_pro_id: composer.performing_rights_organization_id
    }
  end
end
