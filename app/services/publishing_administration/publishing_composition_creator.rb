class PublishingAdministration::PublishingCompositionCreator
  def self.create(args = {})
    new(args).create
  end

  def initialize(args = {})
    @name = args[:name]
    @account = args[:account]
    @song = args[:song]
  end

  def create
    @song.publishing_composition = PublishingComposition.create(name: @name, account: @account)

    @song.save

    @publishing_composition = @song.publishing_composition

    @publishing_composition.hide! if duplicate_publishing_composition_exists?

    @publishing_composition
  end

  private

  def duplicate_publishing_composition_exists?
    return false unless @publishing_composition.id

    all_songs = Song.where(
      "songs.tunecore_isrc = :composition_isrc
      OR
      songs.optional_isrc = :composition_isrc",
      composition_isrc: @publishing_composition.isrc
    ).to_a
    all_songs.concat(NonTunecoreSong.where(isrc: @publishing_composition.isrc).to_a)

    all_songs.each do |song|
      next if song.publishing_composition_id == @publishing_composition.id || song.publishing_composition_id.blank?

      song_publishing_composition = PublishingComposition.find_by(id: song.publishing_composition_id)
      return true unless song_publishing_composition.state == "hidden"
    end

    false
  end
end
