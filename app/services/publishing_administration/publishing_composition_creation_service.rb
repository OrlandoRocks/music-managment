class PublishingAdministration::PublishingCompositionCreationService
  def self.create(publishing_composer_id)
    new(publishing_composer_id).create
  end

  def initialize(publishing_composer_id)
    @publishing_composer = PublishingComposer.find(publishing_composer_id)
    @eligible_albums = []
    @eligible_songs = []

    return if publishing_composer.person_id.blank?

    @eligible_albums.concat(publishing_composer.person.albums.where(legal_review_state: "APPROVED"))
    @eligible_songs.concat(
      publishing_composer.person.songs.where(
        publishing_composition_id: nil,
        album_id: @eligible_albums.pluck(:id)
      )
    )
  end

  def create
    eligible_songs.each do |song|
      PublishingAdministration::PublishingCompositionCreator.create(
        name: song.name,
        account: publishing_composer.account,
        song: song
      )
    end
  end

  private

  attr_reader :publishing_composer, :eligible_songs, :all_songs
end
