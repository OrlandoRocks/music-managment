class PublishingAdministration::IngestionBlocker
  attr_reader :work_codes

  def self.multiple_writers_with_right_to_collect?(provider_account_id)
    new(provider_account_id).multiple_writers_with_right_to_collect?
  end

  def initialize(provider_account_id)
    @work_codes = PublishingAdministration::ApiClientServices::FetchWorkListService.work_codes(provider_account_id)
  end

  def multiple_writers_with_right_to_collect?
    work_codes.any? do |work_code|
      writers_with_right_to_collect = PublishingAdministration::ApiClientServices::GetWorkService.writers_with_right_to_collect(work_code)
      return true if writers_with_right_to_collect.count > 1
    end
    false
  end
end
