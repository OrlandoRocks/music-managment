class PublishingAdministration::CompositionUpdateService
  def self.update(composer_id)
    new(composer_id).update
  end

  def initialize(composer_id)
    @composer = Composer.find(composer_id)
    @compositions = composer.compositions
  end

  def update
    compositions.update_all(name: composer.name, cwr_name: composer.name)
  end

  private

  attr_reader :composer, :compositions
end
