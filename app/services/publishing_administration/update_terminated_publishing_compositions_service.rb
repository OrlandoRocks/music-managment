class PublishingAdministration::UpdateTerminatedPublishingCompositionsService
  include ArelTableMethods

  def self.update(account)
    new(account).update
  end

  def initialize(account)
    @account = account
  end

  def update
    terminated_composers.each do |terminated_composer|
      works = work_list(terminated_composer.alt_provider_acct_id).works
      next if works.blank?

      work_codes = works.map(&:work_code)
      publishing_compositions = PublishingComposition.where(provider_identifier: work_codes)
      publishing_compositions.each(&:terminate!)
    end
  end

  private

  attr_reader :account

  def terminated_composers
    @terminated_composers ||=
      account.publishing_composers
             .joins(:terminated_composer)
             .where(terminated_composer_t[:alt_provider_acct_id].not_eq(nil))
             .select(terminated_composer_t[:alt_provider_acct_id])
  end

  def work_list(provider_account_id)
    PublishingAdministration::ApiClientServices::FetchWorkListService.fetch(provider_account_id)
  end
end
