class PublishingAdministration::RoyaltyStatementUploadService
  def self.attach(royalty_payment_id, quarter, year)
    new(royalty_payment_id, quarter, year).update_royalty_payment
  end

  def initialize(royalty_payment_id, quarter, year)
    @royalty_payment = RoyaltyPayment.find(royalty_payment_id)
    @person_id = @royalty_payment.person_id
    @person = Person.find(@person_id)
    @quarter = quarter
    @year = year
  end

  def update_royalty_payment
    if @royalty_payment
      Rails.logger.info "updating royalty payment #{@royalty_payment.id}"
      s3 = Aws::S3::Resource.new
      bucket = s3.bucket("ftp.tunecore.com")
      file_path = "tcxfr/royalty_temp_#{@quarter}Q#{@year}/#{@person_id}_#{@royalty_payment.code}"

      has_pdf = bucket.object("#{file_path}.pdf").exists?
      if has_pdf
        pdf = bucket.object("#{file_path}.pdf")
        Rails.logger.info "#{@person_id}_#{@royalty_payment.code}.pdf found. Attaching to RoyaltyPayment."
        @royalty_payment.pdf_summary_file_name = "#{@person_id}_#{@royalty_payment.code}.pdf"
        @royalty_payment.pdf_summary_file_size = pdf.get.size
        @royalty_payment.pdf_summary_updated_at = Time.now
        move_file(pdf, @royalty_payment.download_pdf_filename, "pdf")
      else
        Rails.logger.info "#{@person_id}_#{@royalty_payment.code}.pdf not found"
      end

      has_csv = bucket.object("#{file_path}.csv").exists?
      if has_csv
        csv = bucket.object("#{file_path}.csv")
        Rails.logger.info "#{@person_id}_#{@royalty_payment.code}.csv found. Attaching to RoyaltyPayment."
        @royalty_payment.csv_summary_file_name = "#{@person_id}_#{@royalty_payment.code}.csv"
        @royalty_payment.csv_summary_file_size = csv.get.size
        @royalty_payment.csv_summary_file_updated_at = Time.now
        move_file(csv, @royalty_payment.download_csv_filename, "csv")
      else
        Rails.logger.info "#{@person_id}_#{@royalty_payment.code}.csv not found"
      end
      return true if @royalty_payment.save
    end
    false
  end

  def move_file(file, download_filename, file_type)
    options = {
      content_disposition: "#{file_type == 'pdf' ? 'inline' : 'attachment'}; filename=\"#{download_filename}\"",
      content_type: file.get.to_h[:metadata]["content-type"],
      acl: "authenticated-read",
      metadata_directive: "REPLACE"
    }

    new_key = "#{@royalty_payment.id}/#{@person_id}_#{@royalty_payment.code}.#{file_type}"
    file.move_to(
      "#{ROYALTY_REPORTS_BUCKET_NAME}/#{new_key}",
      options
    )
  end
end
