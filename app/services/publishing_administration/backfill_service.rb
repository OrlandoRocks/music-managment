class PublishingAdministration::BackfillService
  attr_reader :start_date, :end_date

  def self.backfill(params = {})
    new(params).backfill
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date   = params[:end_date]
  end

  def backfill
    backfill_writers
    backfill_compositions
  end

  def backfill_writers
    eligible_writers.each do |writer|
      PublishingAdministration::ApiClientServices::WriterService.create_or_update(writer)
    end
  end

  def backfill_compositions
    eligible_compositions.each do |composition|
      composer = Composer.find(composition.composer_id)
      PublishingAdministration::ApiClientServices::WorkCreationService.post_work(
        composer: composer,
        composition: composition
      )
    end
  end

  private

  def eligible_writers
    eligible_composers + eligible_cowriters
  end

  def eligible_composers
    @eligible_composers ||= PublishingAdministration::EligibleComposersBuilder.new(builder_params).composers
  end

  def eligible_cowriters
    @eligible_cowriters ||= PublishingAdministration::EligibleCowritersBuilder.new(builder_params).cowriters
  end

  def eligible_compositions
    @eligible_compositions ||= PublishingAdministration::EligibleCompositionsBuilder.new(builder_params).compositions
  end

  def builder_params
    { start_date: start_date, end_date: end_date }
  end
end
