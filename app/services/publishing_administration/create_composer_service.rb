class PublishingAdministration::CreateComposerService
  include PublishingAdministrationHelper

  def self.create_composer(params)
    new(params).create
  end

  def initialize(params)
    @account        = params[:account]
    @response       = params[:api_response]
    @free_purchase  = params[:free_purchase] || false
    @pro            = PerformingRightsOrganization.find_by(provider_identifier: response.society_id)
  end

  def create
    create_or_update_publisher
    create_composer
    create_composer_purchase
    create_purchase_invoice if @free_purchase

    return unless purchase.present? && invoice.present?

    purchase.update(
      invoice_id: invoice.id,
      paid_at: invoice.created_at
    )
  end

  private

  attr_accessor :account, :composer, :invoice, :pro, :publisher, :purchase, :response

  def composer_attributes
    {
      first_name: response.first_name,
      middle_name: response.middle_name,
      last_name: response.last_name,
      account_id: account.id,
      agreed_to_terms_at: pub_admin_purchased_at || "01/01/1900".to_datetime,
      cae: pro.present? ? response.cae : nil,
      dob: "01/01/1900".to_date,
      publisher_id: publisher.id,
      provider_identifier: response.writer_code,
      publishing_role_id: PublishingRole::ON_BEHALF_OF_OTHER_ID,
      performing_rights_organization_id: performing_rights_org_id
    }
  end

  def create_composer
    @composer = Composer.create(composer_attributes)
  end

  def create_composer_purchase
    @purchase = Purchase.create(
      person_id: account.person_id,
      product_id: pub_admin_product_id,
      cost_cents: @free_purchase ? 0 : pub_admin_product_cost_cents,
      related_id: composer.id,
      related_type: "Composer"
    )
  end

  def create_purchase_invoice
    @invoice = Invoice.create(
      person_id: purchase.person_id,
      final_settlement_amount_cents: purchase.cost_cents,
      currency: purchase.currency
    )
  end

  def create_or_update_publisher
    @publisher = Publisher.where(
      performing_rights_organization_id: pro.try(:id),
      cae: response.publisher_cae.presence,
      name: response.publisher_name
    ).first_or_create
  end

  def performing_rights_org_id
    (response.cae.present? && pro.present?) ? pro.id : nil
  end

  def pub_admin_product_id
    @pub_admin_prod_id ||= Product::PRODUCT_COUNTRY_MAP[account.person.country_domain][:songwriter_service].first
  end

  def pub_admin_product_cost_cents
    Product.find(pub_admin_product_id).price.to_i * 100
  end

  def pub_admin_purchased_at
    account.person.purchases.where(product_id: pub_admin_product_id).try(:first).try(:paid_at)
  end
end
