class PublishingAdministration::ComposerSyncOptInService
  attr_reader :composer

  def self.opt_in(options)
    new(options).tap(&:opt_in)
  end

  def self.opt_out(options)
    new(options).tap(&:opt_out)
  end

  def initialize(args = {})
    @composer = args[:composer]
  end

  def opt_in
    composer.update(sync_opted_in: true, sync_opted_updated_at: Time.current)
  end

  def opt_out
    composer.update(sync_opted_in: false, sync_opted_updated_at: Time.current)
  end
end
