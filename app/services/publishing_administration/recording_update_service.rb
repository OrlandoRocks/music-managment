class PublishingAdministration::RecordingUpdateService
  attr_reader :composition, :recording_list

  def self.update(composition)
    new(composition).update
  end

  def initialize(composition)
    @composition = composition
    @recording_list = PublishingAdministration::ApiClientServices::FetchRecordingListService.fetch(composition)
  end

  def update
    return if composition.provider_identifier.blank? || !recording_list.valid?

    recording_list.recordings.each do |recording|
      next if composition.recordings.exists?(recording_code: recording.recording_id)

      composition.recordings.create!(
        recordable: composition.song,
        recording_code: recording.recording_id,
      )

      update_isrc_number(recording.isrc) if recording.isrc.present?
    end

    delete_extraneous_recordings
  end

  private

  def update_isrc_number(isrc_number)
    ntc_song = NonTunecoreSong.find_by(composition_id: composition.id)
    ntc_song.update(isrc: isrc_number) if ntc_song.present?
  end

  def delete_extraneous_recordings
    recording_ids = recording_list.recordings.map(&:recording_id)
    recordings_to_delete = composition.recordings.where.not(recording_code: recording_ids)

    recordings_to_delete.destroy_all if recordings_to_delete.present?
  end
end
