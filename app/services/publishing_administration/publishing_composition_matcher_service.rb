class PublishingAdministration::PublishingCompositionMatcherService
  include ArelTableMethods

  def self.match(params)
    new(params).match
  end

  def initialize(params)
    @publishing_composer = params[:publishing_composer]
    @current_publishing_composition = params[:publishing_composition]
    @params = { publishing_composer: @publishing_composer, publishing_composition: @current_publishing_composition }
  end

  def match
    find_existing_publishing_composition_that_matches_current_publishing_composition_and_update_provider_identifier
    @current_publishing_composition
  end

  private

  attr_reader :publishing_composer, :current_publishing_composition

  def publishing_compositions_by_name
    comp_songs      = publishing_composition_songs_by_name
    comp_ntc_songs  = publishing_composition_ntc_songs_by_name

    comp_ntc_songs.keys.each do |name|
      if comp_songs[name]
        comp_songs[name].concat(comp_ntc_songs[name])
      else
        comp_songs[name] = comp_ntc_songs[name]
      end
    end

    comp_songs
  end

  def publishing_composition_songs_by_name
    publishing_compositions_songs_query.group_by { |cs| cs.clean_composition_name.downcase }
  end

  def publishing_composition_ntc_songs_by_name
    publishing_compositions_ntc_songs_query.group_by { |ntcs| ntcs.clean_composition_name.downcase }
  end

  def publishing_compositions_songs_query
    @publishing_compositions ||= PublishingComposition
                                 .joins(:publishing_composition_splits)
                                 .joins(songs: :album)
                                 .where(album_t[:person_id].eq(publishing_composer.person_id))
                                 .where(publishing_composition_t[:id].not_eq(current_publishing_composition.id))
                                 .group(publishing_composition_t[:id])
  end

  def publishing_compositions_ntc_songs_query
    @ntc_publishing_compositions ||= PublishingComposition
                                     .joins(:publishing_composition_splits)
                                     .joins(non_tunecore_songs: :non_tunecore_album)
                                     .where(non_tunecore_album_t[:publishing_composer_id].eq(publishing_composer.id))
                                     .where(publishing_composition_t[:id].not_eq(current_publishing_composition.id))
                                     .group(publishing_composition_t[:id])
  end

  def current_publishing_composition_splits
    @current_publishing_composition_splits ||= parse_splits(current_publishing_composition)
  end

  def parse_splits(publishing_composition)
    publishing_composition.publishing_composition_splits.each_with_object({}) do |ps, hash|
      key = ps.publishing_composer.full_name.downcase
      hash[key] = { percent: ps.percent, right_to_collect: ps.right_to_collect }
    end
  end

  def splits_match?(other_comp)
    current_publishing_composition_splits == parse_splits(other_comp) &&
      other_comp.provider_identifier.present?
  end

  def find_existing_publishing_composition_that_matches_current_publishing_composition_and_update_provider_identifier
    current_name  = current_publishing_composition.clean_composition_name.downcase
    comps         = publishing_compositions_by_name[current_name]
    return if comps.blank?

    matching_comp = comps.find { |comp| splits_match?(comp) }

    return unless matching_comp

    @current_publishing_composition.update(provider_identifier: matching_comp.provider_identifier)
  end
end
