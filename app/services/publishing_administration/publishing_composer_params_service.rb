class PublishingAdministration::PublishingComposerParamsService < PublishingAdministration::BaseComposerParamsService
  def self.composer_class
    PublishingComposer
  end

  def self.format_dob_params(params)
    enroll_params = params[:publishing_administration_publishing_enrollment_form]
    enroll_params[:dob_y] = enroll_params.delete("dob(1i)")
    enroll_params[:dob_m] = enroll_params.delete("dob(2i)")
    enroll_params[:dob_d] = enroll_params.delete("dob(3i)")
  end
end
