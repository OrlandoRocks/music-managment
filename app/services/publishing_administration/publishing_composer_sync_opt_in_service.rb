class PublishingAdministration::PublishingComposerSyncOptInService
  attr_reader :publishing_composer

  def self.opt_in(options)
    new(options).tap(&:opt_in)
  end

  def self.opt_out(options)
    new(options).tap(&:opt_out)
  end

  def initialize(args = {})
    @publishing_composer = args[:publishing_composer]
  end

  def opt_in
    publishing_composer.update(sync_opted_in: true, sync_opted_updated_at: Time.current)
  end

  def opt_out
    publishing_composer.update(sync_opted_in: false, sync_opted_updated_at: Time.current)
  end
end
