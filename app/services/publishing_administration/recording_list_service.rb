class PublishingAdministration::RecordingListService
  attr_reader :account, :songs

  def self.list(account)
    new(account).list
  end

  def initialize(account)
    @account = account
    @songs = songs_with_compositions.to_a.concat(non_tunecore_songs_with_compositions.to_a)
  end

  def list
    return if songs.blank?

    songs.each do |song|
      composition = song.composition
      next if composition&.provider_identifier.blank?

      PublishingAdministration::RecordingUpdateService.update(composition)
    end
  end

  private

  def songs_with_compositions
    Song
      .includes(:composition)
      .joins(album: { person: :account })
      .where(accounts: { id: account.id })
      .where.not(songs: { composition_id: nil })
  end

  def non_tunecore_songs_with_compositions
    NonTunecoreSong
      .includes(:composition)
      .joins(non_tunecore_album: { composer: :account })
      .where(accounts: { id: account.id })
      .where.not(non_tunecore_songs: { composition_id: nil })
      .readonly(false)
  end
end
