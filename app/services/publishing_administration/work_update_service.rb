class PublishingAdministration::WorkUpdateService
  attr_reader :composition, :work, :free_purchase

  def self.update(composition, work, free_purchase = false)
    new(composition, work, free_purchase).update
  end

  def initialize(composition, work, free_purchase = false)
    @composition = composition
    @work = work
    @free_purchase = free_purchase
  end

  def update
    return if work.writers.blank?

    compare_writers
    compare_splits
    update_composition
    update_composition_state
  end

  private

  def compare_writers
    PublishingAdministration::ApiClientServices::ComposerNormalizeService.reconcile_composers(writer_params)
    PublishingAdministration::ApiClientServices::CowriterNormalizeService.reconcile_cowriters(writer_params)
  end

  def compare_splits
    total_shares = work.writers.sum(&:share).to_d

    return unless total_shares == 100.to_d

    work.writers.each do |split|
      writer_type = split.right_to_collect ? "Composer" : "Cowriter"
      writer = writers_for(writer_type).find_by(provider_identifier: split.writer_code)
      share_percentage = split.share.to_d

      if writer.present?
        update_split(writer, share_percentage)
      else
        add_split(writer_type, split.writer_code, share_percentage)
      end
    end
  end

  def update_split(writer, share_percentage)
    pub_split = composition.publishing_splits.find_by(
      publishing_splits: {
        writer_id: writer.id,
        writer_type: writer.class.name
      }
    )

    return unless pub_split.present? && pub_split.percent != share_percentage

    pub_split.update_attribute(:percent, share_percentage)
  end

  def add_split(writer_type, writer_code, share_percentage)
    writer = writer_type.constantize.find_by(provider_identifier: writer_code)

    return unless writer.present? && (composition.total_split_pct + share_percentage <= 100)

    PublishingSplit.create!(
      composer_id: (writer_type == "Composer") ? writer.id : writer.composer_id,
      composition_id: composition.id,
      percent: share_percentage,
      writer: writer,
    )
  end

  def update_composition
    return unless composition.verified_date.nil? || composition.verified_date <= work.verified_date

    composition.update(
      {
        name: work.title,
        verified_date: work.verified_date,
        provider_identifier: work.work_code
      }
    )
    composition.verified! if work.verified_date.present?

    PublishingAdministration::RecordingUpdateService.update(composition) if work.number_of_recordings.positive?
  end

  def update_composition_state
    if work.is_deactivated? && !composition.terminated?
      composition.terminate!
    elsif !work.is_deactivated? && composition.terminated?
      composition.verified!
    elsif composition.non_tunecore_songs.any? && composition.pending_distribution?
      composition.submit_split!
    end
  end

  def writers_for(klass)
    klass
      .classify.constantize
      .joins(:publishing_splits)
      .where(publishing_splits: { composition_id: composition.id }).distinct
  end

  def writer_params
    composer_splits, cowriter_splits = work.writers.partition(&:right_to_collect)

    {
      account: composition.account,
      api_composers: composer_splits,
      api_cowriters: cowriter_splits,
      composers: writers_for("Composer"),
      cowriters: writers_for("Cowriter"),
      free_purchase: free_purchase
    }
  end
end
