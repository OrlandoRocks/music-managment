class PublishingAdministration::PublishingBackfillService
  attr_reader :start_date, :end_date

  def self.backfill(params = {})
    new(params).backfill
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end

  def backfill
    backfill_writers
    backfill_publishing_compositions
  end

  def backfill_writers
    eligible_writers.each do |writer|
      PublishingAdministration::ApiClientServices::PublishingWriterService.create_or_update(writer)
    end
  end

  def backfill_publishing_compositions
    eligible_publishing_compositions.each do |publishing_composition|
      publishing_composer = PublishingComposer.find(publishing_composition.publishing_composer.id)

      PublishingAdministration::ApiClientServices::PublishingWorkCreationService.post_work(
        publishing_composer: publishing_composer,
        publishing_composition: publishing_composition
      )
    end
  end

  private

  def eligible_writers
    eligible_primary_publishing_composers + eligible_cowriters
  end

  def eligible_primary_publishing_composers
    @eligible_publishing_composers ||= PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.new(builder_params).primary_publishing_composers
  end

  def eligible_cowriting_publishing_composers
    @eligible_cowriters ||= PublishingAdministration::EligibleCowritingPublishingComposersBuilder.new(builder_params).cowriting_publishing_composers
  end

  def eligible_publishing_compositions
    @eligible_publishing_compositions ||= PublishingAdministration::EligiblePublishingCompositionsBuilder.new(builder_params).publishing_compositions
  end

  def builder_params
    { start_date: start_date, end_date: end_date }
  end
end
