class PublishingAdministration::NonTunecoreService
  def self.create_ntc_album_and_song(params)
    new(params).create_ntc_album_and_song
  end

  def initialize(params)
    @composer    = params[:composer]
    @composition = params[:composition]
    @work        = PublishingAdministration::ApiClientServices::GetWorkService.get_work(@composition.provider_identifier)
  end

  def create_ntc_album_and_song
    create_non_tunecore_album
    create_non_tunecore_song
  end

  private

  def create_non_tunecore_album
    @ntc_album = NonTunecoreAlbum.create(
      name: @composition.name,
      composer_id: @composer.id,
      orig_release_year: Date.today,
      account_id: @composer.account_id
    )
  end

  def create_non_tunecore_song
    NonTunecoreSong.create(
      name: @composition.name,
      non_tunecore_album_id: @ntc_album.id,
      composition_id: @composition.id,
      artist_id: find_or_create_artist.try(:id)
    )
  end

  def find_or_create_artist
    return unless @work["PerformingArtists"].try(:any?)

    Artist.where(name: @work["PerformingArtists"].first["Name"]).first_or_create
  end
end
