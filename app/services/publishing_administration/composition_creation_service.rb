class PublishingAdministration::CompositionCreationService
  def self.create(composer_id)
    new(composer_id).create
  end

  def initialize(composer_id)
    @composer       = Composer.find(composer_id)
    @eligible_songs = []

    @eligible_songs.concat(composer.person.songs.where(songs: { composition_id: nil })) if composer.person_id.present?
  end

  def create
    eligible_songs.each do |song|
      song.composition = Composition.create(name: song.name)
      song.save
    end
  end

  private

  attr_reader :composer, :eligible_songs
end
