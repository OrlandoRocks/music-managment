class PublishingAdministration::PublishingCompositionUpdateService
  def self.update(publishing_composer_id)
    new(publishing_composer_id).update
  end

  def initialize(publishing_composer_id)
    @publishing_composer = PublishingComposer.find(publishing_composer_id)
    @publishing_compositions = publishing_composer.publishing_compositions
  end

  def update
    publishing_compositions.update_all(name: publishing_composer.name)
  end

  private

  attr_reader :publishing_composer, :publishing_compositions
end
