class TcSocialCancellationReporter
  PER_PAGE = 25
  attr_reader :report_form, :page, :report

  def self.build_paginated_report(opts)
    new(opts).tap(&:build_paginated_report)
  end

  def self.build_csv_report(opts)
    new(opts).tap(&:build_csv_report)
  end

  def initialize(opts)
    @report_form = opts[:report_form]
    @page          = opts[:page] || 1
    @cancellations = get_cancellations
  end

  def get_cancellations
    events = TcSocialCancellationEventQueryBuilder.build_query(report_form)
    events.map { |event| TcSocialCancellationReportItem.new(event) }
  end

  def build_paginated_report
    @report = @cancellations.paginate(page: page, per_page: PER_PAGE)
  end

  def build_csv_report
    @report =
      CSV.generate do |csv|
        csv << TcSocialCancellationReportItem.column_names
        @cancellations.each do |cancellation|
          csv << cancellation.to_h.values_at(*TcSocialCancellationReportItem.column_names)
        end
      end
  end
end
