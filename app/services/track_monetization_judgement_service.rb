class TrackMonetizationJudgementService
  UNBLOCKING_MESSAGE = "unblocking_distribution".freeze
  BLOCKING_MESSAGE = "blocking_distribution".freeze
  PENDING_MESSAGE = "pending_approval".freeze

  def self.judge(params)
    new(params).tap(&:judge)
  end

  attr_reader :eligible_track_monetizations, :ineligible_track_monetizations, :pending_track_monetizations,
              :current_user

  def initialize(params = {})
    @eligible_track_monetizations = params[:eligible_track_monetizations] || []
    @ineligible_track_monetizations = params[:ineligible_track_monetizations] || []
    @pending_track_monetizations = params[:pending_track_monetizations] || []
    @current_user = params[:current_user]
  end

  def judge
    set_eligible_track_monetizations_to_unblocked
    set_ineligible_track_monetizations_to_blocked
    set_pending_track_monetizations_to_pending
  end

  private

  def set_eligible_track_monetizations_to_unblocked
    eligible_track_monetizations.map do |track_monetization|
      track_monetization.unblock_and_qualify(
        {
          actor: self.class.name,
          message: UNBLOCKING_MESSAGE
        }
      )

      track_monetization.create_manual_approval(@current_user) if track_monetization.manual_approval.nil?
    end
  end

  def set_ineligible_track_monetizations_to_blocked
    ineligible_track_monetizations.map do |track_monetization|
      track_monetization.block_and_disqualify(
        {
          actor: self.class.name,
          message: BLOCKING_MESSAGE
        }
      )

      track_monetization.manual_approval&.destroy
    end
  end

  def set_pending_track_monetizations_to_pending
    pending_track_monetizations.map do |track_monetization|
      track_monetization.unblock_and_qualify({ actor: self.class.name, message: UNBLOCKING_MESSAGE })
      track_monetization.manual_approval&.destroy
      track_monetization.reload

      track_monetization.pend({ actor: self.class.name, message: PENDING_MESSAGE })
    end
  end
end
