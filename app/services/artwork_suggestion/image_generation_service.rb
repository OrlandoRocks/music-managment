class ArtworkSuggestion::ImageGenerationService
  def self.copy(source, target)
    image = MiniMagick::Image.open(source)
    image.write(target)
  end

  def self.convert(params)
    MiniMagick::Tool::Convert.new do |convert|
      convert << params[:source] if params[:source]
      convert.merge! params[:opts]
      convert << params[:target]
    end
  end

  def self.composite(base, overlay, destination, opts = {})
    base_image    = MiniMagick::Image.new(base)
    overlay_image = MiniMagick::Image.new(overlay)
    result        =
      base_image.composite(overlay_image) do |composite|
        opts.each do |opt, val|
          composite.send(opt, val)
        end
      end
    result.write destination
  end

  def self.identify(filename)
    image = MiniMagick::Image.open(filename)
    data = image.data
    {
      filename: data["name"],
      format: data["format"],
      width: data["geometry"]["width"].to_i,
      height: data["geometry"]["height"].to_i,
      class: data["class"],
      filesize: data["filesize"]
    }
  rescue
    raise "ImageMagick identify command returned invalid results"
  end
end
