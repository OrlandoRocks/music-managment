class ArtworkSuggestion::CoverSuggestionService
  attr_accessor :genre_id

  def self.suggest_for_genre(genre, album)
    new(genre, album).suggest_for_genre
  end

  attr_accessor :album,
                :genre,
                :background,
                :background_effect,
                :typeface,
                :typeface_effect,
                :layout_class,
                :cover

  def initialize(genre, album)
    @genre             = genre
    @album             = album
    @background        = genre.random_background_for
    @background_effect = BackgroundEffect.random
    @typeface          = genre.random_typeface_for
    @typeface_effect   = TypefaceEffect.random
    @layout_class      = ArtworkSuggestion::Layouts::LayoutManager.random

    build_cover
  end

  def build_cover
    @cover ||= Cover.new(
      background_id: background.id,
      background_effect_id: background_effect.id,
      artist: album.artist_name,
      artist_typeface_id: typeface.id,
      artist_typeface_pointsize: 72,
      artist_typeface_effect_id: typeface_effect.id,
      title: album.name,
      title_typeface_id: typeface.id,
      title_typeface_pointsize: 72,
      title_typeface_effect_id: typeface_effect.id,
      layout_class: layout_class.to_s
    )
  end

  def suggest_for_genre
    cover.artist_typeface        = typeface
    cover.artist_typeface_effect = typeface_effect
    cover.album                  = album
    cover.save!
    cover
  end
end
