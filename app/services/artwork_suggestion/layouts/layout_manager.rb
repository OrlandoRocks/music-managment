class ArtworkSuggestion::Layouts::LayoutManager
  def self.all_layouts
    [
      ArtworkSuggestion::Layouts::CentercenterByCentercenter,
      ArtworkSuggestion::Layouts::CentereastByCentereast,
      ArtworkSuggestion::Layouts::CenterwestByCenterwest,
      ArtworkSuggestion::Layouts::NorthcenterByNorthcenter,
      ArtworkSuggestion::Layouts::NorthcenterBySouthcenter,
      ArtworkSuggestion::Layouts::NortheastByNortheast,
      ArtworkSuggestion::Layouts::NortheastBySoutheast,
      ArtworkSuggestion::Layouts::NortheastBySouthwest,
      ArtworkSuggestion::Layouts::NorthwestByNorthwest,
      ArtworkSuggestion::Layouts::NorthwestBySoutheast,
      ArtworkSuggestion::Layouts::NorthwestBySouthwest,
      ArtworkSuggestion::Layouts::SouthcenterBySouthcenter,
      ArtworkSuggestion::Layouts::SoutheastBySoutheast,
      ArtworkSuggestion::Layouts::SouthwestBySouthwest
    ]
  end

  def self.random
    all_layouts[rand(all_layouts.size)]
  end
end
