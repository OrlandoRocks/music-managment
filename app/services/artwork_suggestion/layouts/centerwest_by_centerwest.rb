class ArtworkSuggestion::Layouts::CenterwestByCenterwest < ArtworkSuggestion::Layouts::BorderLayout
  ArtistGravity = "Southwest"
  TitleGravity = "Northwest"

  def artist_top
    artist_top_for_center
  end

  def artist_left
    artist_left_for_west
  end

  def title_top
    title_top_for_center
  end

  def title_left
    title_left_for_west
  end
end
