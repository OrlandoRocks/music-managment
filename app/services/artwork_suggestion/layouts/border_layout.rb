class ArtworkSuggestion::Layouts::BorderLayout
  Margin = 10

  MinWidthPercentage = 0.60
  MinHeightPercentage = 0.33
  # TODO: these constants copy the field lengths for artist and title, which
  # is unlikely to change, but this is not totally DRY
  MaxLengthForArtist = 120
  MaxLengthForTitle = 129
  def initialize(cover)
    @cover = cover
  end

  def artist_top
    Margin
  end

  def artist_left
    Margin
  end

  def title_top
    Margin
  end

  def title_left
    Margin
  end

  alias_method :artist_top_for_north, :artist_top
  alias_method :artist_left_for_west, :artist_left
  def artist_top_for_south
    @cover.render_size - @cover.rendered_title_height - @cover.rendered_artist_height
  end

  def artist_left_for_east
    @cover.render_size - @cover.rendered_artist_width - Margin
  end

  def artist_top_for_center
    (@cover.render_size / 2).to_i - @cover.rendered_artist_height
  end

  def artist_left_for_center
    ((@cover.render_size / 2) - (@cover.rendered_artist_width / 2)).to_i
  end

  def title_top_for_north
    artist_top + @cover.rendered_artist_height
  end
  alias_method :title_left_for_west, :title_left
  def title_top_for_south
    @cover.render_size - @cover.rendered_title_height
  end

  def title_left_for_east
    @cover.render_size - @cover.rendered_title_width - Margin
  end

  def title_top_for_center
    (@cover.render_size / 2).to_i
  end

  def title_left_for_center
    ((@cover.render_size / 2) - (@cover.rendered_title_width / 2)).to_i
  end

  def width_for_text_of_length(_length, _max_length)
    @cover.render_size
  end

  def height_for_text_of_length(_length, _max_length)
    @cover.render_size
  end

  def width_for_text_of_length(length, max_length)
    percentage_of_max_length = length * (100.0 / max_length) * 0.01
    min_width = @cover.render_size * MinWidthPercentage
    calc_width = @cover.render_size * percentage_of_max_length
    max_width = @cover.render_size - (Margin * 2)
    if calc_width < min_width
      min_width.to_i
    elsif calc_width > max_width
      max_width.to_i
    else
      calc_width.to_i
    end
  end

  def height_for_text_of_length(length, max_length)
    percentage_of_max_length = length * (100.0 / max_length) * 0.01
    min_height = @cover.render_size * MinHeightPercentage
    max_height = @cover.render_size * 0.5
    calc_height = @cover.render_size * percentage_of_max_length
    if calc_height < min_height
      min_height.to_i
    elsif calc_height > max_height
      max_height.to_i
    else
      calc_height.to_i
    end
  end
end
