class ArtworkSuggestion::Layouts::NorthcenterBySouthcenter < ArtworkSuggestion::Layouts::BorderLayout
  ArtistGravity = "South"
  TitleGravity = "North"

  def artist_top
    artist_top_for_north
  end

  def artist_left
    artist_left_for_center
  end

  def title_top
    title_top_for_south
  end

  def title_left
    title_left_for_center
  end
end
