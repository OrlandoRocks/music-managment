class DateParser
  US_DATE_REGEX = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ # DD/MM/YYYY
  DATE_OBJECT_REGEX = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/ # YYYY-MM-DD
  EUROPEAN_DATE_REGEX = /^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$/ # DD.MM.YYYY

  def self.parse(unparsed_date)
    raise "Type Error: unparsed_date must be a string" unless unparsed_date.is_a?(String)

    date_to_parse = unparsed_date.delete(" ")

    if US_DATE_REGEX.match(date_to_parse)
      Time.strptime(date_to_parse, "%m/%d/%Y").to_date
    elsif DATE_OBJECT_REGEX.match(date_to_parse)
      Time.strptime(date_to_parse, "%Y-%m-%d").to_date
    elsif EUROPEAN_DATE_REGEX.match(date_to_parse)
      Time.strptime(date_to_parse, "%d.%m.%Y").to_date
    else
      nil
    end
  end
end
