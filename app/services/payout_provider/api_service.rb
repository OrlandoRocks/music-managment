class PayoutProvider::ApiService
  class WrongEnvError < StandardError; end

  def initialize(person:, api_client:, program_id: nil)
    @api_client = api_client.new(person: person, program_id: program_id)
    @version = FeatureFlipper.show_feature?(:use_payoneer_v4_api, person) ? :v4 : :v2
  end

  def register_existing_user(options = {})
    api_client.send_request(:register_existing_users, options)
  end

  def register_new_user(options = {})
    response = api_client.send_request(:register_new_users, options)
    Payoneer::Responses::RegisterNewUser.new(response, version)
  end

  def get_status(options = {})
    api_client.send_request(:person_statuses, options)
  end

  def submit_payout(options = {})
    response = api_client.send_request(:submit_payouts, options)
    Payoneer::Responses::SubmitPayout.new(response)
  end

  def get_payout_details(options = {})
    api_client.send_request(:payout_details, options)
  end

  def cancel_payout(options = {})
    api_client.send_request(:cancel_payouts, options)
  end

  def payee_report(options = {})
    api_client.send_request(:payee_reports, options)
  end

  def payee_details(options = {})
    response = api_client.send_request(:payee_details, options)
    Payoneer::Responses::PayeeDetail.new(response)
  end

  def payee_status(options = {})
    response = api_client.send_request(:payee_status, options)
    Payoneer::Responses::PayeeStatus.new(response, version)
  end

  def payee_extended_details(options = {})
    response = api_client.send_request(:payee_extended_details, options)
    Payoneer::Responses::PayeeExtendedDetail.new(response, version)
  end

  def move_program(options = {})
    raise WrongEnvError, "Sorry, this is not available in production" if Rails.env.production?

    api_client.send_request(:move_program, options)
  end

  def adopt_payee(options = {})
    response = api_client.send_request(:adopt_payee, options)
    Payoneer::Responses::AdoptPayee.new(response, version)
  end

  private

  attr_reader :api_client, :version
end
