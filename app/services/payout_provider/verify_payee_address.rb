# frozen_string_literal: true

class PayoutProvider::VerifyPayeeAddress
  attr_accessor :person

  def initialize(person, provider_payee_details: nil)
    @person = person
    @provider_payee_details = provider_payee_details
  end

  def call
    # If the country is already locked, then don't update it.
    if person.address_locked?
      Airbrake.notify("Person's address was already locked when Payoneer KYC process completed")
      return
    end

    update_person_country! if verify_person_country
    update_person_street_address!
    lock_person_address
    send_email_notification
  end

  def provider_payee_details
    @provider_payee_details ||= fetch_provider_payee_details
  end

  def send_email_notification
    # TODO: Implement mailer
  end

  private

  def verify_person_country
    if provider_payee_country.nil?
      Airbrake.notify(
        "Payee's Payoneer reported country does not exist in TuneCore",
        country_code: provider_payee_country_code
      )
      false
    else
      true
    end
  end

  def update_person_country!
    ActiveRecord::Base.transaction do
      person.country_audit_source = country_audit_source_name

      if person.country != provider_payee_country
        person.update!(country: provider_payee_country_code)
      else
        person.save!
      end
    end
  end

  def update_person_street_address!
    person.update!(
      address1: provider_payee_details.address_line_1,
      address2: provider_payee_details.address_line_2,
      city: provider_payee_details.city,
      state: provider_payee_details.state,
      postal_code: provider_payee_details.zip_code
    )
  end

  ## Attributes

  def person_country
    @person_country ||= Country.search_by_iso_code(person[:country])
  end

  def country_audit_source_name
    @country_audit_source_name ||= CountryAuditSource::PAYONEER_KYC_NAME
  end

  def provider_payee_country_code
    @provider_payee_country_code ||= provider_payee_details.country
  end

  def provider_payee_country
    @provider_payee_country ||= Country.country_name(provider_payee_country_code)
  end

  ## Helpers

  def fetch_provider_payee_details
    PayoutProvider::ApiService
      .new(person: person, api_client: Payoneer::PayoutApiClientShim.fetch_for(person))
      .payee_extended_details(person: person)
  end

  def lock_person_address
    person.lock_address!(
      admin_person_id: 0,
      reason: FlagReason.payoneer_kyc_completed_address_lock
    )
  end
end
