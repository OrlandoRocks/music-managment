# frozen_string_literal: true

class PayoutProvider::VerifyPayeeComplianceInfo
  attr_accessor :person, :airbrake_messages, :previous_country

  include AfterCommitEverywhere

  LOCK_REASON             = "Verified by Payoneer KYC"
  AIRBRAKE_MSG            = {
    call_transaction_failed: "Issue with persisting Payee's Payoneer Compliance Info Data and Location",
    country_absent: "Payee's Payoneer reported country does not exist in TuneCore",
    address_locked: "Person's address was already locked when Payoneer KYC process completed"
  }.freeze

  COUNTRY_AUDIT_SOURCE_NAMES = {
    payoneer_kyc: CountryAuditSource::PAYONEER_KYC_NAME,
    payoneer_post_kyc: CountryAuditSource::PAYONEER_POST_KYC_NAME
  }

  PAYONEER_INVALID_STATE_CODE = "00"

  def initialize(person, country_audit_source: :payoneer_kyc, provider_payee_details: nil)
    @person = person
    @person.country_audit_source = COUNTRY_AUDIT_SOURCE_NAMES[country_audit_source]
    @previous_country = @person.country
    @provider_payee_details = provider_payee_details
    @airbrake_messages = []
  end

  def call
    ActiveRecord::Base.transaction(requires_new: true) do
      after_commit do
        send_transaction_success_messages
        person.audit_country if payee_and_provider_country_differ? || person.country_audit_source_changed?
      end
      after_rollback { send_transaction_failure_airbreak }

      update_person_country!
      update_person_street_address!
      update_customer_type!
      update_first_name!
      update_last_name!
      update_company_name!

      lock_person_address
      lock_person_company_name
      lock_person_first_name
      lock_person_last_name
      lock_person_customer_type
    end
  end

  def provider_payee_details
    @provider_payee_details ||= fetch_provider_payee_details
  end

  def send_email_notification
    # TODO: Implement mailer
  end

  def send_airbreak_messages
    Airbrake.notify(*airbrake_messages.pop) until airbrake_messages.empty?
  end

  def send_transaction_success_messages
    send_email_notification
    send_airbreak_messages
  end

  private

  def verify_person_country?
    return true if provider_payee_country?

    airbrake_messages << [airbreak_msg(:country_absent), { country_code: provider_payee_country_code }]
    false
  end

  def sanitize_address_state(payoneer_address_state)
    payoneer_address_state == PAYONEER_INVALID_STATE_CODE ? nil : payoneer_address_state
  end

  def update_person_country!
    return unless verify_person_country?
    return unless payee_and_provider_country_differ?

    airbrake_messages << [airbreak_msg(:address_locked), { person: person.attributes }] if person.address_locked?

    ActiveRecord::Base.transaction(requires_new: true) do
      person.override_address_lock = true
      if payee_and_provider_country_differ?
        person.update!(country: provider_payee_country_code)
      else
        person.save!
      end
    end
  end

  def update_person_street_address!
    provider_payee_details.tap do |details|
      person.override_address_lock = true
      person.update!(
        address1: details.address_line_1,
        address2: details.address_line_2,
        city: details.city,
        state: sanitize_address_state(details.state),
        postal_code: details.zip_code
      )
    end
  end

  def update_customer_type!
    return if payee_and_provider_customer_type_match?

    ActiveRecord::Base.transaction(requires_new: true) do
      person.update!(customer_type: provider_payee_details.tc_customer_type)
    end
  end

  def update_first_name!
    return unless person.individual?

    ComplianceInfoField.set_value_for(
      person: person,
      field_name: :first_name,
      field_value: provider_payee_details.first_name
    )
  end

  def update_last_name!
    return unless person.individual?

    ComplianceInfoField.set_value_for(
      person: person,
      field_name: :last_name,
      field_value: provider_payee_details.last_name
    )
  end

  def update_company_name!
    return unless person.business?

    ComplianceInfoField.set_value_for(
      person: person,
      field_name: :company_name,
      field_value: provider_payee_details.company_name
    )
  end

  ## Attributes

  def person_first_name
    @person_first_name ||= person.compliance_info_field.where(field_name: :first_name).take!
  end

  def person_last_name
    @person_last_name ||= person.compliance_info_field.where(field_name: :last_name).take!
  end

  def person_company_name
    @person_company_name ||= person.compliance_info_field.where(field_name: :company_name).take!
  end

  def person_country
    @person_country ||= Country.search_by_iso_code(person[:country])
  end

  def country_audit_source_name
    @country_audit_source_name ||= CountryAuditSource::PAYONEER_KYC_NAME
  end

  def provider_payee_country_code
    @provider_payee_country_code ||= provider_payee_details.country
  end

  def provider_payee_country
    @provider_payee_country ||= Country.country_name(provider_payee_country_code)
  end

  ## Helpers

  def provider_payee_country?
    provider_payee_country.present?
  end

  def payee_and_provider_country_differ?
    previous_country != provider_payee_country
  end

  def payee_and_provider_customer_type_match?
    return provider_payee_details.individual? if person.individual?

    person.business? && provider_payee_details.company?
  end

  def fetch_provider_payee_details
    PayoutProvider::ApiService
      .new(person: person, api_client: Payoneer::PayoutApiClientShim.fetch_for(person))
      .payee_extended_details(person: person)
  end

  def lock_person_address
    person.lock_address!(
      admin_person_id: 0,
      reason: FlagReason.payoneer_kyc_completed_address_lock
    )
  end

  def lock_person_first_name
    return unless person.individual?

    person.lock_first_name(admin_person_id: 0, reason: lock_reason)
  end

  def lock_person_last_name
    return unless person.individual?

    person.lock_last_name(admin_person_id: 0, reason: lock_reason)
  end

  def lock_person_company_name
    return unless person.business?

    person.lock_company_name(admin_person_id: 0, reason: lock_reason)
  end

  def lock_person_customer_type
    flag_options = {
      person: person,
      flag_attributes: Flag::CUSTOMER_TYPE_LOCKED
    }

    Person::FlagService.flag!(flag_options)
  end

  def airbreak_msg(key)
    AIRBRAKE_MSG[key]
  end

  def lock_reason
    LOCK_REASON
  end

  def send_transaction_failure_airbreak
    Airbrake.notify(
      airbreak_msg(:call_transaction_failed), person: person.attributes, payout_provider_details: provider_payee_details
    )
  end
end
