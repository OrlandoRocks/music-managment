class SessionEncryptionEngine
  KEY  = SECRETS["session_cookie_key"]
  SALT = SECRETS["session_salt"]

  def self.encrypt(data)
    with_cipherer(base64_encode: false) do |cipherer|
      cipherer.encrypt(data).encrypted_string
    end
  end

  def self.decrypt(data)
    with_cipherer(base64_encode: false) do |cipherer|
      cipherer.decrypt(data)
    end
  end

  def self.encrypt64(data)
    with_cipherer(base64_encode: true) do |cipherer|
      cipherer.encrypt(data).encrypted_string
    end
  end

  def self.decrypt64(data)
    with_cipherer(base64_encode: true) do |cipherer|
      cipherer.decrypt(data)
    end
  end

  def self.with_cipherer(base64_encode:)
    strategy = Crypto::Strategies::Aes128CbcStrategy.new(
      secret: KEY,
      salt: SALT,
      base64_encode: base64_encode
    )
    cipherer = Cipherer.new(strategy: strategy)

    yield cipherer
  rescue OpenSSL::Cipher::CipherError
    false
  end

  private_class_method :with_cipherer
end
