class PersonMarketingMetrics
  def initialize(person, albums = nil)
    @person = person
    @albums = albums || @person.albums
  end

  def any_credits_available?
    @any_credits_available ||= CreditUsage.all_available(@person).any?
  end

  def release_in_progress?
    @release_in_progress ||= @albums.detect { |album| !album.payment_applied? }.present?
  end

  def credit_in_last_invoice?
    @credit_in_last_invoice ||= @person.invoices.eager_load(purchases: :product).last.purchases.detect { |purchase|
      purchase.product.is_a_credit_product?
    }.present?
  end
end
