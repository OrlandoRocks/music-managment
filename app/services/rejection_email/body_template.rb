class RejectionEmail::BodyTemplate
  def self.rendered_body(review_audit)
    new(review_audit).rendered_body
  end

  def self.format_body_for_modal(review_audit)
    new(review_audit).format_body_for_modal
  end

  attr_reader :person, :rejection_email

  def initialize(review_audit)
    @review_audit    = review_audit
    @album           = @review_audit.album
    @person          = @album.person
    @country         = @person.country_website.id
    @album_name      = @album.name
    @artist_name     = @album.artist_name
    @person_name     = @person.name
    @upc             = @album.upc.number
    @email_note      = @review_audit.email_note
  end

  def rendered_body
    return "" if email_template_type.blank?

    @rejection_email = RejectionEmailMailer.send(email_template_type, @review_audit)
    @rejection_email.body.encoded.gsub(/<%=\s*(@[a-zA-Z_]+)\s*%>/) { |_s| instance_variable_get($1) }
  end

  def format_body_for_modal
    text = rendered_body.gsub("\n", "<br>")
    link = text.scan(/http[^<]+/).first
    link ? text.gsub(link, "<a href='#{link}'> #{link} </a>") : text
  end

  def email_template_type
    @email_template_type ||= @review_audit.review_reasons.first.try(:email_template_type)
  end
end
