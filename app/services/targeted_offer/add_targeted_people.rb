class TargetedOffer::AddTargetedPeople
  def self.add(targeted_offer_id, issuing_user_id)
    new(targeted_offer_id, issuing_user_id).add
  end

  attr_accessor :targeted_offer

  def initialize(targeted_offer_id, issuing_user_id)
    @targeted_offer      = TargetedOffer.find(targeted_offer_id)
    @issuing_user        = Person.find(issuing_user_id)
    @start_time          = Time.now
    @could_not_find_ids  = []
    @validation_fails    = {}
    @success_ids         = []
    @already_added_ids   = []
    set_in_progress_status
  end

  def add
    @existing_people = get_existing_targeted_people
    @person_list     = get_list_of_people_to_add
    process_person_list if @person_list.present?
    send_success_mailer
    remove_in_progress_status
  end

  def set_in_progress_status
    targeted_offer.update(job_status: TargetedOffer::CHANGE_ACTIVATION_PROCESSING_STATUS)
  end

  def get_existing_targeted_people
    targeted_offer.targeted_people.group_by(&:person_id)
  end

  def get_list_of_people_to_add
    targeted_offer.grab_list_from_s3("add")
  end

  def process_person_list
    @person_list.each do |person_id|
      begin
        Person.find(person_id)
        existing_tp = @existing_people[person_id.to_i]
        tp = targeted_offer.targeted_people.create(person_id: person_id) if existing_tp.blank?

        if existing_tp.present?
          @already_added_ids << person_id
        elsif tp.valid?
          @success_ids << person_id
        else
          @validation_fails[person_id] = tp.errors.full_messages
        end
      rescue ActiveRecord::RecordNotFound
        @could_not_find_ids << person_id
      end
    end
  end

  def send_success_mailer
    TargetedOfferMailer
      .add_people_job_status(
        targeted_offer,
        @could_not_find_ids,
        @validation_fails,
        @success_ids,
        @already_added_ids,
        @issuing_user,
        Time.now - @start_time
      ).deliver_later
  end

  def remove_in_progress_status
    targeted_offer.update(job_status: nil)
  end
end
