class TargetedOffer::RemoveTargetedPeople
  def self.remove(targeted_offer_id, issuing_user_id)
    targeted_offer = TargetedOffer.find(targeted_offer_id)
    issuing_user   = Person.find(issuing_user_id)

    start_time            = Time.now
    could_not_find_ids    = []
    could_not_destroy_ids = []
    success_ids           = []

    targeted_offer.update(job_status: TargetedOffer::REMOVE_PEOPLE_PROCESSING_STATUS)

    existing_targeted_people  = targeted_offer.targeted_people.all.group_by(&:person_id)
    person_list               = targeted_offer.grab_list_from_s3("remove")

    if person_list.present?
      person_list.each do |person_id|
        tp_to_destroy = existing_targeted_people[person_id.to_i]

        if tp_to_destroy.blank?
          could_not_find_ids << person_id
        elsif tp_to_destroy.first.destroy
          success_ids << person_id
        else
          could_not_destroy_ids << person_id
        end
      end
    end

    TargetedOfferMailer
      .remove_people_job_status(
        targeted_offer,
        could_not_find_ids,
        success_ids,
        could_not_destroy_ids,
        issuing_user,
        Time.now - start_time
      ).deliver_later

    targeted_offer.update(job_status: nil)
  end
end
