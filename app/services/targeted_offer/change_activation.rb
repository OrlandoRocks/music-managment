class TargetedOffer::ChangeActivation
  def self.change(targeted_offer_id, activate, issuing_user_id)
    targeted_offer = TargetedOffer.find(targeted_offer_id)
    issuing_user   = Person.find(issuing_user_id)

    start_time = Time.now
    targeted_offer.update(job_status: TargetedOffer::CHANGE_ACTIVATION_PROCESSING_STATUS)

    # Update the activate status.. Note that targeted offer model monitors when this change and potentially does a lengthy purchase update
    targeted_offer.set_activation(activate)

    TargetedOfferMailer.activation_job_status(targeted_offer, issuing_user, Time.now - start_time).deliver_later
    targeted_offer.update(job_status: nil)
  end
end
