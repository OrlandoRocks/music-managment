class ApiLogger::JobStatusService
  attr_reader :http_client

  def initialize
    @http_client = Zendesk::ApiClient.new
  end

  def check_status
    return if job_ids.blank?

    response = http_client.get(path)
    body = JSON.parse(response.body, object_class: OpenStruct)

    body.job_statuses.each do |job_status|
      api_logger = ApiLogger.find_by(job_id: job_status.id)

      api_logger.completed! if api_logger && job_status.status == "completed"
    end
  end

  private

  def job_ids
    @job_ids ||= ApiLogger.where(
      "state = ? AND created_at > ?", "processing",
      Time.now - 1.hour
    ).pluck(:job_id).compact
  end

  def path
    "/api/v2/job_statuses/show_many.json?ids=#{job_ids.join(',')}"
  end
end
