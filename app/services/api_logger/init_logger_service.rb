class ApiLogger::InitLoggerService
  attr_reader :api_logger

  def initialize(api_logger_id)
    @api_logger = ApiLogger.find(api_logger_id)
  end

  def call
    api_logger.processing!
    send_request
  end

  private

  def send_request
    check_previous_email_changes = fetch_and_update_to_zendesk
    return if check_previous_email_changes

    options = Zendesk::ApiClient::ENDPOINTS[:CREATE_OR_UPDATE]
    response = Zendesk::ApiClient.new.post(options[:endpoint], { users: [api_logger.request_body] })

    api_logger.error! unless response.success?

    update_api_logger(response, options)
  end

  def update_api_logger(response, options)
    api_logger.update(
      endpoint: options[:endpoint],
      headers: response.headers,
      response_body: response.body,
      http_method: options[:method],
      status_code: response.status,
      job_id: JSON.parse(response.body).dig("job_status", "id")
    )
  end

  def fetch_and_update_to_zendesk
    return false unless api_logger.request_body[:previous_email]

    url             = "/api/v2/users/search.json?query=email:#{api_logger.request_body[:previous_email]}"
    fetch_user      = Zendesk::ApiClient.new.get(url)
    parsed_resp     = JSON.parse(fetch_user.body)
    zendesk_user_id = parsed_resp.dig("users", 0, "id")

    return false if zendesk_user_id.blank?

    update_to_zendesk(zendesk_user_id)
  end

  def update_to_zendesk(zendesk_user_id)
    options = { endpoint: "/api/v2/users/#{zendesk_user_id}.json", method: "put" }
    response = Zendesk::ApiClient.new.put(
      options[:endpoint],
      { user: api_logger.request_body.except(:previous_email) }
    )

    response.success? ? api_logger.completed! : api_logger.error!

    update_api_logger(response, options)
  end
end
