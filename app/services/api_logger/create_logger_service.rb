class ApiLogger::CreateLoggerService
  def initialize(options = {})
    @options = options
  end

  def log
    api_logger = ApiLogger.create(@options)
    ApiLoggerWorker.perform_async(api_logger.id)
  end
end
