# frozen_string_literal: true

class CorporateEntityMappingService
  attr_reader :data_file_key, :append_configs

  LOG_FORMATTERS = {
    data_file_not_found: "File not Found %s",
    entity_already_exists: "Entity already exists for %s with corporate_entity_id: %s",
    country_not_found: "Country with iso code %s not found."
  }.freeze

  def initialize(data_file_key)
    @data_file_key = data_file_key
  end

  def map_to_countries
    return unless data_file_present?

    country_entity_mappings = CSV.open(Rails.root + "db/csv/" + data_file_key, { headers: :first_row, encoding: "UTF-8" })

    country_entity_mappings.each do |country_entity_map|
      country = Country.unscoped.find_by(iso_code: country_entity_map["country_iso"])
      corporate_entity_id = country_entity_map["corporate_entity_id"]
      next if entity_exists?(country, corporate_entity_id, country.iso_code)

      country.update(corporate_entity: all_corporate_entities[corporate_entity_id])
    rescue => e
      print_logs(:error, "ERROR OCCURED: #{e} (country: #{country_entity_map['country_iso']})")
    end
  end

  def map_to_payout_provider_configs
    return unless data_file_present?

    config_entity_mappings = CSV.open(Rails.root + "db/csv/" + data_file_key, { headers: :first_row, encoding: "UTF-8" })

    config_entity_mappings.each do |config_entitiy_map|
      payout_provider_config = PayoutProviderConfig.find_by(id: config_entitiy_map["payout_provider_config_id"])
      next if payout_provider_config.blank?

      corporate_entity_id = config_entitiy_map["corporate_entity_id"]
      next if entity_exists?(payout_provider_config, corporate_entity_id, payout_provider_config.name)

      payout_provider_config.update(corporate_entity: all_corporate_entities[corporate_entity_id])
      Rails.logger.info "payout_provider_config_id: #{payout_provider_config.id}, corporate_entity_id updated to: #{corporate_entity_id}"
    end

    Rails.logger.info "Payout Provider Config to Corporate Entity mapping complete"
    PayoutProviderConfig.all.each do |config|
      corporate_entity_message = config.corporate_entity.present? ? config.corporate_entity.id : "NULL"
      Rails.logger.info "Payout Provider Config: #{config.id} - Corporate Entity: #{corporate_entity_message}"
    end
  end

  def entity_exists?(subject, entity_id, log_entry)
    return false unless subject.corporate_entity_id == entity_id.to_i

    print_logs(:error, LOG_FORMATTERS[:entity_already_exists], [log_entry, entity_id])
    true
  end

  def data_file_present?
    return true if data_file_key.present?

    print_logs(:error, LOG_FORMATTERS[:data_file_not_found], [data_file_key])
    false
  end

  def print_logs(log_type, log_formatter, formatter_data = [])
    log_string = formatter_data.present? ? (log_formatter % formatter_data) : log_formatter
    Rails.logger.send(log_type, log_string)
  end

  def all_corporate_entities
    return @all_corporate_entities unless @all_corporate_entities.nil?

    @all_corporate_entities = CorporateEntity.all.index_by { |ce| ce.id.to_s }
  end
end
