module ReferrerService
  def record_referrer_event(invoice, person)
    return unless person.referred_by("raf")

    referrals = FriendReferralCampaign
                .where(
                  "campaign_code LIKE ? and active = true",
                  person.referral_token.split("_").first
                )
    referrals.each do |ref|
      next unless ref.campaign_code == person.referral_token.split("_").first

      transaction = FriendReferral
                    .record_event(
                      invoice,
                      person,
                      {
                        campaign_id: ref.campaign_id,
                        campaign_code: ref.campaign_code
                      }
                    )
      break
    end
  end
end
