module Sns
  module Notifier
    extend self

    TC_DISTRIBUTOR_METADATA_ONLY = "metadata_only"
    TC_DISTRIBUTOR_FULL_DELIVERY = "full_delivery"
    TC_DISTRIBUTOR_TAKEDOWN_DELIVERY = "takedown"
    TC_DISTRIBUTOR_UNTAKEDOWN_DELIVERY = "untakedown"
    TC_DISTRIBUTOR_DELIVERY_TYPES =
      [
        TC_DISTRIBUTOR_METADATA_ONLY,
        TC_DISTRIBUTOR_FULL_DELIVERY,
        TC_DISTRIBUTOR_TAKEDOWN_DELIVERY,
        TC_DISTRIBUTOR_UNTAKEDOWN_DELIVERY
      ]

    def perform(
      topic_arn:, album_ids_or_upcs:, store_ids:, song_ids: nil, person_id: nil,
      delivery_type: "full_delivery"
    )
      albums = find_albums(Array(album_ids_or_upcs))
      store_ids.map!(&:to_i).reject!(&:zero?)
      # original_store_ids = store_ids.dup
      # store_ids = add_dual_delivery_store_ids_to(store_ids: store_ids) if is_dual_delivery
      valid_store_ids = valid_store_ids(store_ids: store_ids).uniq.sort
      paused_store_ids = paused_stores_for(store_ids: valid_store_ids, topic_arn: topic_arn)
      unpaused_store_ids = valid_store_ids - paused_store_ids

      # if is_dual_delivery
      #   dual_track_monetization_store_ids, unpaused_store_ids =
      #     unpaused_store_ids.partition do |store_id|
      #     Store::TRACK_MONETIZATION_STORES.include?(store_id) &&
      #       original_store_ids.exclude?(store_id)
      #   end
      # end

      albums.each do |album|
        deliver_album(
          topic_arn: topic_arn, album: album, song_ids: song_ids, person_id: person_id,
          stores: stores_h(album: album, store_ids: unpaused_store_ids, delivery_type: tc_distributor_delivery_type(delivery_type)),
        )
        # next unless Array(dual_track_monetization_store_ids).any?

        # deliver_album_tracks(
        #   album: album, track_monetization_store_ids: dual_track_monetization_store_ids, delivery_type: tc_distributor_delivery_type(delivery_type)
        # )
      end if unpaused_store_ids.any?

      save_paused_store_message(album_ids: albums.pluck(:id).uniq, stores: paused_store_ids) if paused_store_ids.any?
    end

    def deliver_album(topic_arn:, album:, stores:, song_ids:, person_id:)
      Rails.logger.info(
        "SNS::Notifier publish to topic #{topic_arn} with album id: #{album.id}," \
          " stores: #{stores}, song_ids: #{song_ids}, person_id: #{person_id}"
      )
      topic(arn: topic_arn).publish(
        message: sns_message(
          album_id: album.id, stores: stores, song_ids: song_ids, person_id: person_id
        )
      )
    end

    # NOTE: handle track monetization deliveries
    # we use a dedicated state :start_without_album (vs :start) to prevent
    # track monetization delivery from triggering an album delivery again
    # def deliver_album_tracks(album:, track_monetization_store_ids:, delivery_type:)
    #   TrackMonetization.delivered_via_tc_distributor
    #                    .where(
    #                      song_id: album.songs.pluck(:id),
    #                      store_id: track_monetization_store_ids
    #                    ).find_each do |track_monetization|
    #                      track_monetization.update(
    #                        state: TrackMonetization::DELIVERED_TC_DISTRIBUTOR_STATE,
    #                        tc_distributor_state: TrackMonetization::START_WITHOUT_ALBUM_STATE,
    #                        delivery_type: delivery_type,
    #                        takedown_at: album.takedown_at
    #                      )
    #                    end
    # end

    def tc_distributor_delivery_type(delivery_type)
      @tc_distributor_delivery_type =
        if delivery_type.in?(TC_DISTRIBUTOR_DELIVERY_TYPES)
          delivery_type
        else
          TC_DISTRIBUTOR_METADATA_ONLY
        end
    end

    def stores_h(album:, store_ids:, delivery_type:)
      store_ids.map { |store_id|
        {
          id: store_id,
          delivery_type: delivery_type,
          delivered: album.delivered_by_tc_www_on?(store_id: store_id)
        }
      }
    end

    def valid_store_ids(store_ids:)
      store_ids - inactive_store_ids
    end

    def sns_client
      @sns_client ||=
        if ENV["SNS_ENDPOINT"]
          Aws::SNS::Resource.new(endpoint: ENV["SNS_ENDPOINT"])
        else
          Aws::SNS::Resource.new
        end
    end

    def topic(arn:)
      @topic ||= sns_client.topic(arn)
    end

    def find_albums(album_ids_or_upcs)
      BulkAlbumFinder.new(album_ids_or_upcs.join(",")).execute
    end

    def sns_message(album_id:, stores:, person_id:, song_ids: nil)
      {
        album_id: album_id,
        stores: stores,
        person_id: person_id,
      }.tap { |body|
        body[:song_ids] = song_ids if Array(song_ids).any?
      }.to_json
    end

    def save_paused_store_message(album_ids:, stores:)
      stores.each do |store|
        Delivery::SnsNotifierPausedWorker.perform_async(album_ids, store)
      end
    end

    def inactive_store_ids
      (Store.not_active.pluck(:id) + Store.is_not_used.pluck(:id) + Store::EXCLUDED_FROM_NOTIFICATION_IDS - Store::TRACK_MONETIZATION_STORES).uniq
    end

    def paused_stores_for(store_ids:, topic_arn:)
      # Bypass the paused stores for priority retries
      store_ids.select { |store_id|
        Store.find(store_id).store_delivery_config&.paused? &&
          topic_arn != ENV["SNS_RELEASE_UPDATE_URGENT_TOPIC"]
      }
    end

    # def add_dual_delivery_store_ids_to(store_ids:)
    #   store_ids.concat(
    #     Store::DUAL_DELIVERY_STORE_IDS
    #                    .slice(*dual_delivery_stores(store_ids: store_ids)).values
    #   )
    #   store_ids.uniq.sort
    # end

    # def dual_delivery_stores(store_ids:)
    #   store_ids & Store::DUAL_DELIVERY_STORE_IDS.keys
    # end
  end
end
