module Sns
  module TakedownNotifier
    extend self

    TC_DISTRIBUTOR_TAKEDOWN_DELIVERY = "takedown"
    TC_DISTRIBUTOR_DELIVERY_TYPES =
      [
        TC_DISTRIBUTOR_TAKEDOWN_DELIVERY
      ]

    SUSPENDED_STORE_IDS = [91]

    def perform(
      topic_arn:,
      album_ids_or_upcs:,
      store_ids:,
      delivery_type: TC_DISTRIBUTOR_TAKEDOWN_DELIVERY,
      **options
    )
      song_ids = options.fetch(:song_ids, nil)
      person_id = options.fetch(:person_id, nil)
      albums = find_albums(Array(album_ids_or_upcs))

      store_ids = store_ids.map { |store_id|
        Integer(store_id)
      }.reject(&:zero?) if store_ids.present?

      valid_store_ids = valid_store_ids(store_ids: store_ids).uniq.sort
      paused_store_ids = paused_stores_for(store_ids: valid_store_ids, topic_arn: topic_arn)
      unpaused_store_ids = valid_store_ids - paused_store_ids

      albums.each do |album|
        deliver_album(
          topic_arn: topic_arn,
          album: album,
          stores: stores_h(
            album: album,
            store_ids: unpaused_store_ids,
            delivery_type: tc_distributor_delivery_type(delivery_type)
          ),
          song_ids: song_ids,
          person_id: person_id
        )
      end if unpaused_store_ids.any?

      save_paused_store_message(album_ids: albums.pluck(:id).uniq, stores: paused_store_ids) if paused_store_ids.any?
    end

    def deliver_album(topic_arn:, album:, stores:, song_ids:, person_id:)
      Rails.logger.info(
        "SNS::Notifier publish to topic #{topic_arn} with album id: #{album.id}," \
          " stores: #{stores}, song_ids: #{song_ids}, person_id: #{person_id}"
      )
      topic(arn: topic_arn).publish(
        message: sns_message(
          album_id: album.id, stores: stores, song_ids: song_ids, person_id: person_id
        )
      )
    end

    def tc_distributor_delivery_type(delivery_type)
      if delivery_type != TC_DISTRIBUTOR_TAKEDOWN_DELIVERY
        raise "Delivery_type is not takendown. Will not be able to takedown."
      end

      @tc_distributor_delivery_type = TC_DISTRIBUTOR_TAKEDOWN_DELIVERY
    end

    def stores_h(album:, store_ids:, delivery_type:)
      store_ids.map { |store_id|
        {
          id: store_id,
          delivery_type: delivery_type,
          delivered: album.delivered_by_tc_www_on?(store_id: store_id)
        }
      }
    end

    def valid_store_ids(store_ids:)
      store_ids - inactive_store_ids
    end

    def sns_client
      @sns_client ||= Aws::SNS::Resource.new
    end

    def topic(arn:)
      @topic ||= sns_client.topic(arn)
    end

    def find_albums(album_ids_or_upcs)
      BulkAlbumFinder.new(album_ids_or_upcs.join(",")).execute
    end

    def sns_message(album_id:, stores:, person_id:, song_ids: nil)
      {
        album_id: album_id,
        stores: stores,
        person_id: person_id,
      }.tap { |body|
        body[:song_ids] = song_ids if Array(song_ids).any?
      }.to_json
    end

    def save_paused_store_message(album_ids:, stores:)
      stores.each do |store|
        Delivery::SnsNotifierPausedWorker.perform_async(album_ids, store)
      end
    end

    def inactive_store_ids
      (
        Store.not_active.pluck(:id) +
        Store.is_not_used.pluck(:id) +
        Store::EXCLUDED_FROM_NOTIFICATION_IDS -
        Store::TRACK_MONETIZATION_STORES
      ).uniq
    end

    def paused_stores_for(store_ids:, topic_arn:)
      # Bypass the paused stores for priority retries and suspended store
      store_ids.select { |store_id|
        Store.find(store_id).store_delivery_config&.paused? &&
          !allow_delivery?(topic_arn, store_id)
      }
    end

    def allow_delivery?(topic_arn, store_id)
      # if it is already takendown and user is priority retrying
      # then it will have sns SNS_RELEASE_UPDATE_URGENT_TOPIC
      # or if it is suspended store(we don't want to send feed for some duration)
      topic_arn == ENV["SNS_RELEASE_UPDATE_URGENT_TOPIC"] ||
        SUSPENDED_STORE_IDS.include?(store_id)
    end
  end
end
