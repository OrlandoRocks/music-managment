class RedistributionPackageChecker
  def self.check_redistribution_statuses
    RedistributionPackage.pending.each do |package|
      states = package.distributions.pluck(:state)
      if states.uniq == ["delivered"]
        package.update(state: "redelivered")
        # ArtistMappingMailer.redistribution_success_email(package).deliver
      elsif states.include?("error") && !states.include?("enqueued")
        package.update(state: "error")
      end
    end
  end
end
