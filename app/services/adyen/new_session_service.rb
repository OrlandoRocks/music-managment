# frozen_string_literal: true

class Adyen::NewSessionService
  include AdyenSecretsHelper

  attr_reader :invoice, :payment_method, :person

  def initialize(invoice, payment_method)
    @payment_method = payment_method
    @person = invoice.person
    @invoice = invoice
  end

  def update_adyen_transaction
    return if session.error_code

    AdyenTransaction.find_by(invoice: invoice).update(update_attrs)
  end

  def details
    return { session_id: "", session_data: "" } unless adyen_integration_enabled? && selected_country_affiliated_to_bi?
    return { session_id: "", session_data: "" } if session.error_code

    info = { session_id: session.id, session_data: session.session_data }
    merge_credentials(info)
  end

  def formatted_amount_in_original_currency
    return amount_formatter.formatted_amount unless amount_formatter.native_converter?

    Money.new(invoice.outstanding_amount_cents, person.currency).format
  end

  def formatted_amount_in_local_currency
    return unless amount_formatter.native_converter?

    amount_formatter.formatted_amount
  end

  private

  def amount_formatter
    @amount_formatter ||=
      Adyen::RequestAmountFormatter::Factory
      .for(
        payment_method: payment_method_name,
        original_amount: invoice.outstanding_amount_cents,
        original_currency: person.currency,
        country_code: country_code
      )
  end

  def update_attrs
    {
      session_id: session.id,
      amount_in_local_currency: session.amount_in_local_currency,
      adyen_payment_method_info: payment_method_info,
      adyen_merchant_config: adyen_merchant_config
    }.merge(local_currency_info)
  end

  def local_currency_info
    return {} unless amount_formatter.native_converter?

    {
      foreign_exchange_rate: amount_formatter.foreign_exchange_rate,
      local_currency: amount_formatter.currency
    }
  end

  def session
    @session ||= Adyen::PaymentsApiClient.new(invoice.corporate_entity_id).send_request(
      :new_session,
      payment_method: payment_method,
      invoice: invoice,
      amount_formatter: amount_formatter
    )
  end

  def payment_method_info
    AdyenPaymentMethodInfo.find_by(payment_method_name: payment_method_name)
  end

  def adyen_integration_enabled?
    person.enabled_adyen_integration?
  end

  def merge_credentials(session_info)
    session_info[:client_key] = adyen_client_key
    session_info[:environment] = adyen_env
    session_info
  end

  def adyen_env
    Rails.env.production? ? "live" : "test"
  end

  def country_code
    Country.find_by(name: person.country_name_untranslated).iso_code
  end

  def selected_country_affiliated_to_bi?
    Country.find_by(iso_code: country_code).corporate_entity.try(:name) == "BI Luxembourg"
  end

  # Adyen /PaymentMethods API returns "scheme" for CUP payments
  def payment_method_name
    return AdyenPaymentMethodInfo::CUP if payment_method == "scheme"

    payment_method
  end

  def adyen_merchant_config
    config_for_corporate_entity(invoice.corporate_entity_id)
  end

  def adyen_client_key
    adyen_merchant_config.client_key
  end
end
