# frozen_string_literal: true

class Adyen::RefreshAdyenTransactionService
  include AdyenError

  def initialize(adyen_transaction, redirect_result)
    @adyen_transaction = adyen_transaction
    @redirect_result = redirect_result
  end

  def self.call(*args)
    new(*args).refresh!
  end

  attr_accessor :adyen_transaction

  def refresh!
    write_transaction_log_to_s3(adyen_response)
    update_payment_details!
    raise AdyenTransactionError.new(response: adyen_response) unless adyen_response.payment_succeeded?
  end

  private

  attr_reader :redirect_result

  def adyen_response
    @adyen_response ||= Adyen::PaymentsApiClient.new(adyen_transaction.invoice.corporate_entity_id).send_request(
      :payment_detail,
      redirect_result: redirect_result
    )
  end

  def write_transaction_log_to_s3(adyen_response)
    Adyen::TransactionLogWorker.new.async.write_to_bucket(
      adyen_transaction.invoice.id,
      adyen_response
    )
  end

  def transaction_status
    adyen_response.result_code == AdyenTransaction::AUTHORISED ? "success" : "failure"
  end

  def update_payment_details!
    adyen_transaction.update!(
      psp_reference: adyen_response.psp_reference,
      result_code: adyen_response.result_code,
      merchant_reference: adyen_response.merchant_reference,
      status: transaction_status,
      error_message: adyen_response.error_message
    )

    Adyen::StoredPaymentMethodUpdateService.call!(
      adyen_transaction, adyen_response.recurring_reference
    ) if adyen_response.recurring_reference.present? && adyen_response.payment_succeeded?
  end
end
