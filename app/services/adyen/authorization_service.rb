# frozen_string_literal: true

class Adyen::AuthorizationService
  include AdyenSecretsHelper

  attr_accessor :notification_item

  def initialize(params)
    @notification_item = parse_notification_item(params)
  end

  def authorize!
    hmac_signature == base64_encoded_string
  end

  private

  def parse_notification_item(params)
    params["notificationItems"][0]["NotificationRequestItem"]
  end

  def hmac_signature
    notification_item["additionalData"]["hmacSignature"]
  end

  def hmac_key_hex
    Array(adyen_hmac_key).pack("H*")
  end

  def hmac_encoded_string
    OpenSSL::HMAC.digest(OpenSSL::Digest.new("sha256"), hmac_key_hex, authorization_string)
  end

  def base64_encoded_string
    Base64.strict_encode64 hmac_encoded_string
  end

  def authorization_string
    "#{psp_reference}:#{original_reference}:#{merchant_account_code}:#{merchant_reference}:"\
    "#{value}:#{currency}:#{event_code}:#{success}"
  end

  def psp_reference
    notification_item["pspReference"]
  end

  def original_reference
    notification_item["originalReference"]
  end

  def merchant_account_code
    notification_item["merchantAccountCode"]
  end

  def merchant_reference
    notification_item["merchantReference"]
  end

  def value
    notification_item["amount"]["value"]
  end

  def currency
    notification_item["amount"]["currency"]
  end

  def event_code
    notification_item["eventCode"]
  end

  def success
    notification_item["success"]
  end

  def adyen_hmac_key
    config_for_merchant_account(merchant_account_code).decrypted_hmac_key
  end
end
