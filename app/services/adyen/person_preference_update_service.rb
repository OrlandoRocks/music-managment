module Adyen
  class PersonPreferenceUpdateService
    def self.call!(*args)
      new(*args).update!
    end

    def initialize(person, stored_payment_method)
      @person = person
      @stored_payment_method = stored_payment_method
    end

    def update!
      return unless adyen_feature_flags_on?

      person_preference.blank? ? create_person_preference : update_person_preference
    end

    private

    attr_reader :person, :stored_payment_method

    def adyen_feature_flags_on?
      person.allow_adyen? && person.adyen_renewals_feature_on?
    end

    def person_preference
      @person_preference ||= person.person_preference
    end

    def create_person_preference
      PersonPreference.create!(
        person: person,
        preferred_adyen_payment_method_id: stored_payment_method.id,
        preferred_payment_type: AdyenTransaction::ADYEN_NAME
      )
    end

    def update_person_preference
      person_preference.update!(
        preferred_adyen_payment_method_id: stored_payment_method.id,
        preferred_payment_type: AdyenTransaction::ADYEN_NAME
      )
    end
  end
end
