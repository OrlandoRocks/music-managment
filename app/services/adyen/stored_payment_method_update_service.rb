module Adyen
  class StoredPaymentMethodUpdateService
    def self.call!(adyen_transaction, recurring_reference)
      new(adyen_transaction, recurring_reference).call!
    end

    def initialize(adyen_transaction, recurring_reference)
      @adyen_transaction = adyen_transaction
      @recurring_reference = recurring_reference
    end

    def call!
      adyen_transaction.update!(adyen_stored_payment_method: stored_payment_method)
      Adyen::PersonPreferenceUpdateService.call!(person, stored_payment_method)
    end

    private

    attr_reader :adyen_transaction, :recurring_reference

    def person
      @person ||= adyen_transaction.person
    end

    def stored_payment_method
      @stored_payment_method ||=
        AdyenStoredPaymentMethod.find_or_create_by!(
          person: person,
          country: adyen_transaction.country,
          corporate_entity: bi_corporate_entity,
          recurring_reference: recurring_reference,
          adyen_payment_method_info: adyen_transaction.adyen_payment_method_info
        )
    end

    def bi_corporate_entity
      CorporateEntity.find_by(name: CorporateEntity::BI_LUXEMBOURG_NAME)
    end
  end
end
