# frozen_string_literal: true

class Adyen::UpdateAdyenRefundTransactionService
  attr_reader :person, :adyen_transaction, :params

  def initialize(params)
    @params = params.with_indifferent_access
    @adyen_transaction = AdyenTransaction.find_by(psp_reference: psp_reference)
  end

  def update_transaction
    return "refused " unless adyen_transaction && adyen_integration_enabled?

    adyen_transaction.update(update_attrs)

    authorised? ? process_settlement : process_refund_failure

    "authorised"
  end

  private

  def update_attrs
    {
      reason: reason,
      result_code: result_code,
      status: status
    }
  end

  def process_settlement
    return if refund.success?

    refund.process_settlement!(adyen_transaction, adyen_transaction.amount)
    refund.update(status: "success")
  end

  def process_refund_failure
    refund.update(status: "error")
  end

  def refund
    adyen_transaction.invoice.refunds.last
  end

  def refund_params
    params["notificationItems"][0]["NotificationRequestItem"]
  end

  def psp_reference
    refund_params[:pspReference]
  end

  def reason
    refund_params[:reason]
  end

  def adyen_integration_enabled?
    adyen_transaction.person.enabled_adyen_integration?
  end

  def result_code
    authorised? ? AdyenTransaction::AUTHORISED : AdyenTransaction::REFUSED
  end

  def status
    authorised? ? AdyenTransaction::SUCCESS : AdyenTransaction::FAILURE
  end

  def authorised?
    refund_params[:success] == AdyenTransaction::ADYEN_TRUE
  end
end
