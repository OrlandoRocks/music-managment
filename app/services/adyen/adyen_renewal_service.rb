# frozen_string_literal: true

class Adyen::AdyenRenewalService
  attr_accessor :adyen_transaction

  def initialize(adyen_transaction, invoice)
    @adyen_transaction = adyen_transaction
    @invoice = invoice
  end

  def process_renewal
    write_transaction_log_to_s3(adyen_response)

    update_payment_details!
  end

  private

  attr_reader :invoice

  def adyen_response
    @adyen_response ||= Adyen::PaymentsApiClient.new(invoice.corporate_entity_id).send_request(
      :renewal,
      invoice: invoice
    )
  end

  def write_transaction_log_to_s3(adyen_response)
    Adyen::TransactionLogWorker.new.async.write_to_bucket(
      adyen_transaction.invoice.id,
      adyen_response
    )
  end

  def transaction_status
    adyen_response.result_code == AdyenTransaction::AUTHORISED ? "success" : "failure"
  end

  def update_payment_details!
    adyen_transaction.update!(
      psp_reference: adyen_response.psp_reference,
      result_code: adyen_response.result_code,
      merchant_reference: adyen_response.merchant_reference,
      status: transaction_status
    )
  end
end
