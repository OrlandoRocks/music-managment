# frozen_string_literal: true

class Adyen::RefundTransactionService
  include AdyenError
  include CurrencyHelper

  attr_reader :original_transaction, :refund_amount

  def initialize(original_transaction, refund_amount)
    @original_transaction = original_transaction
    @refund_amount = refund_amount
  end

  def call
    raise AdyenTransactionRefundError if refund_failed?

    AdyenTransaction.create(transaction_attributes)
  end

  private

  def refund_failed?
    refund_response.result_code != AdyenTransaction::RECEIVED
  end

  def refund_response
    @refund_response ||= Adyen::PaymentsApiClient.new(original_transaction.invoice.corporate_entity_id).send_request(
      :refund,
      adyen_transaction: original_transaction,
      converted_refund_amount: amount_formatter.amount,
      currency: amount_formatter.currency
    )
  end

  def transaction_attributes
    original_transaction.slice(
      :person_id,
      :invoice_id,
      :adyen_stored_payment_method_id,
      :foreign_exchange_rate_id,
      :country_id,
      :currency,
      :local_currency,
      :adyen_merchant_config_id,
      :adyen_payment_method_info_id
    ).tap do |attributes|
      attributes[:result_code] = refund_response.result_code
      attributes[:psp_reference] = refund_response.psp_reference
      attributes[:amount] = refund_amount
      attributes[:amount_in_local_currency] = amount_formatter.amount
      attributes[:related_transaction_id] = original_transaction.id
    end
  end

  def amount_formatter
    @amount_formatter ||=
      Adyen::RequestAmountFormatter::Factory.for(
        payment_method: original_transaction.payment_method_name,
        original_amount: refund_amount,
        original_currency: original_transaction.currency,
        foreign_exchange_rate: original_transaction.foreign_exchange_rate
      )
  end
end
