module Adyen
  class FetchRecurringDetailsService
    def self.call!(adyen_transaction)
      new(adyen_transaction).call!
    end

    def initialize(adyen_transaction)
      @adyen_transaction = adyen_transaction
    end

    def call!
      return if recurring_reference.blank? || psp_reference.blank?

      Adyen::StoredPaymentMethodUpdateService.call!(adyen_transaction, recurring_reference)
    end

    private

    attr_reader :adyen_transaction

    def psp_reference
      @psp_reference ||= adyen_transaction.psp_reference
    end

    def recurring_deatils
      @recurring_deatils ||= fetch_recurring_details
    end

    def fetch_recurring_details
      Adyen::RecurringApiClient.new(adyen_transaction.invoice.corporate_entity_id).send_request(
        :list_recurring_details,
        shopper_reference: adyen_transaction.adyen_customer_reference,
      )
    end

    def recurring_reference
      recurring_deatils
        .recurring_detail_reference_for_psp_reference(psp_reference)
    end
  end
end
