# frozen_string_literal: true

module Adyen
  module RequestAmountFormatter
    class Factory
      METHODS_WITH_CONVERSION_REQUIREMENT = Set[
        "dana",
        "gcash",
        "gopay_wallet",
        "momo_wallet",
        "paymaya_wallet",
        "ideal"
      ].freeze

      METHODS_WITHOUT_CONVERSION_REQUIREMENT = Set[
        "cup",
        "googlepay",
        "applepay"
      ].freeze

      def self.for(**args)
        new(**args).formatter
      end

      def initialize(payment_method:, **args)
        @payment_method = payment_method
        @args = args
      end

      def formatter
        @formatter ||= formatter_klass.new(**args)
      end

      private

      attr_reader :payment_method, :args

      def formatter_klass
        case
        when METHODS_WITH_CONVERSION_REQUIREMENT.include?(payment_method)
          NativeCurrencyFormatter
        when METHODS_WITHOUT_CONVERSION_REQUIREMENT.include?(payment_method)
          OriginalCurrencyFormatter
        else
          raise "Conversion requirement not specified for #{payment_method}"
        end
      end
    end
  end
end
