# frozen_string_literal: true

module Adyen
  module RequestAmountFormatter
    # Adyen APIs expect amounts to be submitted in minor units: the smallest
    # unit of a currency, depending on the number of decimals.
    #
    # For some currencies adyen has different number of decimals than that of
    # ISO 4217 standard.
    # see: https://docs.adyen.com/development-resources/currency-codes
    class CurrencyMinorUnitConverter < ::CurrencyMinorUnitConverter
      MINOR_UNIT_FRACTION_OVERRIDES = {
        "CLP" => 2,
        "CVE" => 0,
        "IDR" => 0,
        "ISK" => 0
      }.freeze

      private

      def source_currency_minor_unit_fractions
        MINOR_UNIT_FRACTION_OVERRIDES[source_currency.to_s] || super
      end

      def target_currency_minor_unit_fractions
        MINOR_UNIT_FRACTION_OVERRIDES[target_currency.to_s] || super
      end
    end
  end
end
