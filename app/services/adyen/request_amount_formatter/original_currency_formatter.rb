module Adyen
  module RequestAmountFormatter
    class OriginalCurrencyFormatter
      include MoneyFormatter

      def initialize(original_amount:, original_currency:, **_kwargs)
        @original_amount = original_amount
        @original_currency = original_currency
      end

      def amount
        Integer(original_amount)
      end

      def currency
        original_currency
      end

      def native_converter?
        false
      end

      private

      attr_reader :original_amount, :original_currency
    end
  end
end
