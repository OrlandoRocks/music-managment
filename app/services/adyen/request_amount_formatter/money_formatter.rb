module Adyen
  module RequestAmountFormatter
    module MoneyFormatter
      def formatted_amount
        format(amount, currency)
      end

      def formatted_original_amount
        format(original_amount, original_currency)
      end

      def format(amount, currency)
        Money.new(amount, currency).format
      end
    end
  end
end
