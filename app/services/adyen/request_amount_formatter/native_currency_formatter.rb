module Adyen
  module RequestAmountFormatter
    class NativeCurrencyFormatter
      include MoneyFormatter

      attr_reader :foreign_exchange_rate

      def initialize(original_amount:, **kwargs)
        @original_amount = original_amount
        @foreign_exchange_rate =
          kwargs.fetch(:foreign_exchange_rate) { get_exchange_rate(kwargs) }
      end

      def amount
        Integer(amount_in_minor_unit)
      end

      def currency
        native_currency
      end

      def native_converter?
        true
      end

      private

      attr_reader :original_amount

      def get_exchange_rate(args)
        NativeCurrencyExchangeRateFetcher.new(
          country_iso_code: args.fetch(:country_code),
          source_currency: args.fetch(:original_currency)
        ).foreign_exchange_rate
      end

      def original_currency
        foreign_exchange_rate.source_currency
      end

      def native_currency
        foreign_exchange_rate.target_currency
      end

      def converted_amount
        original_amount * foreign_exchange_rate.exchange_rate
      end

      def amount_in_minor_unit
        CurrencyMinorUnitConverter
          .new(original_currency, native_currency)
          .adjust_minor_unit_for(converted_amount)
      end
    end
  end
end
