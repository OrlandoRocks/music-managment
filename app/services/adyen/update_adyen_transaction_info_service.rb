# frozen_string_literal: true

class Adyen::UpdateAdyenTransactionInfoService
  attr_reader :country_code, :person, :adyen_transaction, :params

  def initialize(params)
    @params = params.with_indifferent_access
    @adyen_transaction = AdyenTransaction.find_by(session_id: checkout_session_id)
    @person = adyen_transaction&.person
    @country_code = adyen_transaction&.country&.iso_code
  end

  def update_transaction
    return "refused" unless adyen_integration_enabled? && selected_country_affiliated_to_bi?
    return "refused" if indonesian_wallets_feature_flag_disabled? && payment_method_is_not_cup?

    update_adyen_transaction if update_adyen_transaction?
    Adyen::StoredPaymentMethodUpdateService
      .call!(adyen_transaction, recurring_reference) if recurring_reference.present?

    "authorised"
  end

  private

  def psp_reference
    authorize_params[:pspReference]
  end

  def adyen_integration_enabled?
    adyen_transaction && person.enabled_adyen_integration?
  end

  def indonesia_country_code
    Country.find_by(name: "Indonesia").iso_code
  end

  def indonesian_wallets_feature_flag_disabled?
    country_code == indonesia_country_code && !person.enabled_indonesian_wallets?
  end

  def selected_country_affiliated_to_bi?
    Country.find_by(iso_code: country_code).corporate_entity.try(:name) == "BI Luxembourg"
  end

  def payment_method_is_not_cup?
    payment_method != "cup"
  end

  def currency
    authorize_params[:amount][:currency]
  end

  def result_code
    authorize_params[:success] == "true" ? "Authorised" : "Refused"
  end

  def reason
    authorize_params[:reason]
  end

  def status
    authorize_params[:success] == "true" ? "success" : "failure"
  end

  def checkout_session_id
    authorize_params[:additionalData][:checkoutSessionId]
  end

  def authorize_params
    params["notificationItems"][0]["NotificationRequestItem"]
  end

  def recurring_reference
    authorize_params[:additionalData][:"recurring.recurringDetailReference"]
  end

  def payment_method
    authorize_params[:paymentMethod]
  end

  def adyen_payment_method_info
    AdyenPaymentMethodInfo.find_by(payment_method_name: adyen_payment_method)
  end

  def adyen_payment_method
    applepay_variant || googlepay_variant || ideal_renewal || payment_method
  end

  def googlepay_variant
    return unless AdyenPaymentMethodInfo::GOOGLEPAY_VARIANTS.include?(payment_method)

    AdyenPaymentMethodInfo::GOOGLEPAY_PAYMENT_METHOD
  end

  def ideal_renewal
    return unless payment_method == AdyenPaymentMethodInfo::SEPADIRECTDEBIT

    AdyenPaymentMethodInfo::IDEAL_PAYMENT_METHOD
  end

  def applepay_variant
    return unless AdyenPaymentMethodInfo::APPLEPAY_VARIANTS.include?(payment_method)

    AdyenPaymentMethodInfo::APPLEPAY_PAYMENT_METHOD
  end

  def corporate_entity
    CorporateEntity.find_by(name: "BI Luxembourg")
  end

  def update_adyen_transaction
    adyen_transaction.update(
      psp_reference: psp_reference,
      result_code: result_code,
      currency: currency,
      status: status,
      reason: reason,
      adyen_payment_method_info: adyen_payment_method_info
    )
  end

  def update_adyen_transaction?
    adyen_transaction.psp_reference.blank?
  end
end
