# frozen_string_literal: true

class Adyen::PaymentMethodService
  attr_reader :country_code, :person

  def initialize(country_code, person)
    @country_code = country_code
    @person = person
  end

  def payment_methods
    return [
      {
        "brands" => ["cup"],
        "name" => "Credit Card",
        "type" => "scheme"
      }
    ] if indonesian_wallets_feature_flag_disabled?

    fetch_adyen_payment_methods
  end

  private

  def fetch_adyen_payment_methods
    return [] unless selected_country_affiliated_to_bi? && person.enabled_adyen_integration?

    adyen_response = Adyen::PaymentsApiClient.new(corporate_entity_id).send_request(
      :payment_method,
      country_code: country_code
    )
    adyen_response.payment_methods
  end

  def indonesian_wallets_feature_flag_disabled?
    country_code == indonesia_country_code && !person.enabled_indonesian_wallets?
  end

  def indonesia_country_code
    Country.find_by(name: "Indonesia").iso_code
  end

  def selected_country_affiliated_to_bi?
    Country.find_by(iso_code: country_code).corporate_entity.try(:name) == "BI Luxembourg"
  end

  def corporate_entity_id
    Country.find_by(iso_code: country_code).corporate_entity_id
  end
end
