# frozen_string_literal: true

class Adyen::UpdateAdyenRecurringContractService
  attr_reader :country_code, :person, :adyen_transaction, :params

  def initialize(params)
    @params = params.with_indifferent_access
    @adyen_transaction = AdyenTransaction.find_by(psp_reference: original_reference)
    @person = adyen_transaction&.person
    @country_code = adyen_transaction&.country&.iso_code
  end

  def update_transaction
    return "refused" unless adyen_transaction

    Adyen::StoredPaymentMethodUpdateService
      .call!(adyen_transaction, recurring_reference) if recurring_reference.present?

    "authorised"
  end

  private

  def recurring_contract_params
    params["notificationItems"][0]["NotificationRequestItem"]
  end

  def original_reference
    recurring_contract_params[:originalReference]
  end

  def recurring_reference
    recurring_contract_params[:pspReference]
  end
end
