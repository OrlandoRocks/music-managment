class TaxTokensCohortService
  def self.create(params)
    new(params).tap(&:create)
  end

  def initialize(options)
    @name = options[:name]
    @start_date = options[:start_date]
    @end_date = options[:end_date]
    @file = options[:file]
  end

  def create_tax_tokens_cohort
    TaxTokensCohort.create!(
      name: @name,
      start_date: @start_date,
      end_date: @end_date
    )
  end

  def create_tax_tokens(cohort)
    csv_file = CSV.parse(@file.read, headers: false)
    csv_file.each do |row|
      TaxToken.create(
        person_id: row.first,
        token: row.last,
        is_visible: true,
        tax_tokens_cohort_id: cohort.id
      )
    end
  end

  def create
    cohort = create_tax_tokens_cohort
    create_tax_tokens(cohort) if cohort
    send_emails(cohort)
  end

  private

  def send_emails(cohort)
    reminder_email_seconds = calculate_time_until_send(cohort.end_date.to_date)
    if cohort.start_date.to_date <= Date.today
      TaxTokensCohortsMailWorker.perform_async(cohort.id)
    else
      start_date_email_seconds = calculate_time_until_send(cohort.start_date.to_date)
      TaxTokensCohortsMailWorker.perform_in(start_date_email_seconds, cohort.id)
    end

    TaxTokensCohortsMailWorker.perform_in(reminder_email_seconds, cohort.id)
  end

  def calculate_time_until_send(date)
    daily_send_email_time = 10.hours
    number_of_days = date - Date.today
    send_email_time = number_of_days.days.from_now.beginning_of_day + daily_send_email_time
    send_email_time - Time.now
  end
end
