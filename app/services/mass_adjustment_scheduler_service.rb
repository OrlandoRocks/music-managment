class MassAdjustmentSchedulerService
  attr_accessor :mass_adjustment_batch

  def self.schedule(mass_adjustment_batch)
    new(mass_adjustment_batch).schedule
  end

  def initialize(mass_adjustment_batch)
    @mass_adjustment_batch = mass_adjustment_batch
  end

  def schedule
    mass_adjustment_batch.with_lock do
      if mass_adjustment_batch.status == MassAdjustmentBatch::APPROVED
        mass_adjustment_batch.update(status: MassAdjustmentBatch::SCHEDULED)
        process_entries_async
      end
    end
  end

  private

  def process_entries_async
    entries = mass_adjustment_batch.mass_adjustment_entries.unprocessed
    entries.each do |entry|
      MassBalanceAdjustment::BatchEntryWorker.perform_async(entry.id)
    end
  end
end
