class CreativeExternalServiceIdsService
  attr_reader :creative, :apple_artist_id, :spotify_artist_id

  def self.update(params)
    new(params).tap(&:update)
  end

  def initialize(args = {})
    @creative = args[:creative]
    @apple_artist_id = args[:apple_artist_id]
    @spotify_artist_id = args[:spotify_artist_id]
  end

  def update
    update_external_service_ids

    true
  end

  private

  def external_service_identifiers
    {
      spotify: spotify_artist_id,
      apple: apple_artist_id
    }
  end

  def update_external_service_ids
    external_service_identifiers.each do |key, value|
      if value.present?
        creative&.update_or_create_external_service_id(key, value, [creative])
      else
        creative.external_service_ids.by_service(key).destroy_all
      end
    end
  end
end
