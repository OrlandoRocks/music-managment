class LegalDocument::Docusign
  LOD_DOC_NO = "1".freeze

  def initialize(legal_document, documents_s3_bucket)
    @legal_document      = legal_document
    @documents_s3_bucket = documents_s3_bucket
    @api_client          = DocuSign::ApiClient.new.api_client
    @envelopes_api       = DocuSign_eSign::EnvelopesApi.new(@api_client)
  end

  def document_url
    unless legal_document.asset.exists?
      asset_file = Paperclip.io_adapters.for(File.open(docusign_document))
      asset_file.original_filename = "letter_of_direction.pdf"
      # ^^^ workaround paperclip's spoof detection false positives on Tempfile names

      legal_document.update(asset: asset_file)
    end
    documents_s3_bucket.objects[asset_path].url_for(:read, expires: 10.minutes).to_s
  end

  private

  attr_reader :legal_document, :envelopes_api, :documents_s3_bucket

  def docusign_document
    envelopes_api.get_document(DocuSign::ApiClient::ACCOUNT_ID, LOD_DOC_NO, legal_document.provider_identifier)
  end

  def asset_path
    legal_document.asset.path[1..-1]
  end
end
