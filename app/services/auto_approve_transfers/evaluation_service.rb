# frozen_string_literal: true

module AutoApproveTransfers
  class EvaluationService
    include ActiveModel::Model

    attr_accessor :transfer_metadatum, :transfer

    validates :transfer_metadatum, presence: { strict: true }
    validates :transfer,           presence: { strict: true }

    def initialize(transfer_metadatum)
      @transfer_metadatum = transfer_metadatum
      @transfer           = transfer_metadatum.trackable
      valid?
    end

    def call!
      return unless all_rules_pass?

      transfer_metadatum.auto_approve!
      enqueue_worker!
    end

    private

    # As we update the rules, we should eager_load the associations on
    # to the transfer being passed to the RuleService
    def all_rules_pass
      @all_rules_pass ||=
        AutoApproveTransfers::RulesService.new(transfer).all_rules_pass?
    end

    def all_rules_pass?
      all_rules_pass
    end

    def enqueue_worker!
      AutoApproveTransfers::WorkflowWorker.perform_at(base_enqueue_time.from_now, transfer_metadatum.id)
    end

    # This makes sure a the sidekiq job will never be scheduled
    # on a Saturday or Sunday
    def base_enqueue_time
      @base_enqueue_time ||=
        case scheduled_for_weekday
        when :monday, :tuesday, :wednesday, :thursday, :friday
          time_in.hours
        when :saturday
          time_in.hours + 48.hours
        when :sunday
          time_in.hours + 24.hours
        end
    end

    # AUTO_APPROVE_BASE_ENQUEUE_TIME exists mostly for testing purposes.
    # May or maynot be used by the QA autmoation team.
    def time_in
      @time_in ||= Float(ENV.fetch("AUTO_APPROVE_TRANSFER_ENQUEUE_TIME", "24.0"))
    end

    def scheduled_for_weekday
      @scheduled_for_weekday ||= Time.current.in(time_in.hours)
                                     .strftime("%A")
                                     .downcase
                                     .to_sym
    end
  end
end
