module AutoApproveTransfers
  class RulesService
    include Ruleable
    include ActiveModel::Model

    attr_accessor :transfer

    validates :transfer, presence: { strict: true }

    define_rules do
      # The request amount must be less than 100 of the website currency
      rule :currency_limit, ->(config, transfer) do
        if transfer.requested_withdrawal_amount < config.fetch(:max_amount).to_money(transfer.person.currency)
          return :ok
        end

        :fail
      end

      # The request must be made by a user whose account is has 2fa enabled
      # unless the user is also a VIP
      rule :enabled_2fa_unless_vip, ->(_config, transfer) do
        return :ok if transfer.person.vip
        return :ok if transfer.person.two_factor_auth&.active?

        :fail
      end

      # The request must be made by a user whose account is not marked as ‘Suspicious’
      # for any reason
      rule :not_currently_flagged, ->(_config, transfer) do
        return :fail if transfer.flagged?

        :ok
      end

      # The request must be made by a user whose account is not tied to a multiple-occupancy
      # Payoneer account unless the user is also a VIP
      rule :not_multiple_occupancy_payoneer_unless_vip, ->(_config, transfer) do
        return :ok if transfer.class != PayoutTransfer # Payoneer only

        person = transfer.person
        return :ok if person.vip?
        return :ok if
          PayoutProvider.count_of_active_providers_for_people_by(
            provider_identifier: person.payout_provider.provider_identifier
          ).one?

        :fail
      end

      # The request must be made by a user who has not had another request
      # approved within a 7-day window from their last successfully-processed
      # auto-approval
      rule :request_approved_within, ->(config, transfer) do
        return :fail unless transfer.is_a?(PayoutTransfer) || transfer.is_a?(PaypalTransfer)

        return :fail if transfer.class
                                .recently_approved_transactions(transfer.person_id, config.fetch(:window_in_days))
                                .any?

        :ok
      end

      # The request must be made by a user whose subdivision, city, and country from their
      # login IP at the time the request is made match the subdivision, city, and country
      # from at least 1 of their last 5 login IPs
      rule :transfer_login_match_previous, ->(config, transfer) do
        matching_logins = LoginEvent
                          .matching_previous_logins(
                            transfer.transfer_metadatum.login_event,
                            config.fetch(:number_of_previous_logins)
                          )

        return :ok if matching_logins.present?

        :fail
      end

      # The request must be made by a user who has had 3 successful withdrawals
      # over their lifetime as a TuneCore customer using the same payout method
      # and with paypal, they have the same paypal_address
      rule :withdrawals_using_same_payout_method, ->(config, transfer) do
        past_success_count = transfer.class
                                     .completed_and_successful(transfer.person_id, transfer.paypal_address)
                                     .count

        return :ok if past_success_count >= config.fetch(:successful_withdrawals)

        :fail
      end

      # The request must be made by a user whose account does not have any hold
      # on it. Hold = copyright claim, marked as suspicious, or locked
      rule :account_not_held, ->(_config, transfer) do
        person = transfer.person
        return :ok unless person.account_held?

        :fail
      end
    end

    def initialize(transfer)
      @transfer = transfer
    end

    def all_rules_pass?
      return false unless valid?

      rule_facts = evaluate_rules(against: transfer, category: :auto_approve_transfers)
      rule_facts.exclude?(:fail)
    end
  end
end
