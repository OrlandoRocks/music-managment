module YoutubeMonetizations
  class SyncYoutubeMonetizationChangesToBelieveApi
    def self.call(youtube_monetization)
      new.send(:call, youtube_monetization)
    end

    private

    def call(youtube_monetization)
      return unless conditions_met?(youtube_monetization)

      if DEFER_BELIEVE
        Believe::PersonWorker.perform_async(youtube_monetization.person.id)
      else
        Believe::PersonWorker.perform(youtube_monetization.person.id)
      end
    end

    def conditions_met?(youtube_monetization)
      [
        youtube_monetization.saved_change_to_effective_date?,
        youtube_monetization.saved_change_to_termination_date?
      ].any?
    end
  end
end
