class FileNameModifier
  attr_reader :filename

  def self.spatial_audio_filename(params)
    new(params).spatial_audio_filename
  end

  def initialize(params)
    @filename = params[:filename]
  end

  def spatial_audio_filename
    return @spatial_audio_filename if @spatial_audio_filename

    file_basename = File.basename(filename, ".*")
    file_extension = filename.gsub(file_basename, "")

    @spatial_audio_filename = [file_basename, "_spatial", file_extension].join("")
  end
end
