class TaxTokenService
  def initialize(person_id)
    @person_id = person_id
  end

  def remove_user_from_all_cohorts
    TaxToken.where(person_id: @person_id).update_all(is_visible: false)
  end
end
