class PersonSubscriptionStatusService
  def self.disable_access(subscription_status, current_user)
    instance = new(subscription_status, current_user)
    instance.disable_access
    instance
  end

  attr_reader :subscription_status, :current_user, :purchase
  attr_accessor :errors

  def initialize(subscription_status, current_user)
    @subscription_status = subscription_status
    @purchase = subscription_status.person.subscription_purchase_for(subscription_status)
    @errors = ActiveModel::Errors.new(self)
    @current_user = current_user
  end

  def disable_access
    subscription_event_result = create_subscription_event
    errors[:base] << ("error saving subscription event") unless subscription_event_result
    note_result = create_access_disabled_note
    errors[:base] << ("error saving note") unless note_result
    result = subscription_status.disable_access(current_user)
    errors[:base].concat(result.errors.full_messages) if result.errors
    self
  end

  private

  def create_access_disabled_note
    note = Note.new(
      subject: "Subscriptions",
      note: "TC Social Access Disabled",
      related: subscription_status.person,
      note_created_by_id: current_user.id
    )
    note.save
  end

  def create_subscription_event
    subscription_event = SubscriptionEvent.new(
      {
        subscription_purchase_id: purchase.id,
        person_subscription_status_id: subscription_status.id,
        event_type: "Access Disabled",
        subscription_type: subscription_status.subscription_type
      }
    )
    subscription_event.save
  end
end
