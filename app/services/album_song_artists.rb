class AlbumSongArtists
  include ArelTableMethods
  include ArelNamedMysqlFunctions
  include ArelSongContributorsJoins

  def self.song_contributors(album)
    new(album.songs).song_contributors
  end

  def initialize(songs)
    @songs = songs
  end

  def song_contributors
    contributors = base_query.where(where_conditions)
    contributors.select do |c|
      appears_on_all?(c)
    end
  end

  def base_query
    Creative.select(
      select_statement
    ).joins(
      artist_join
    ).joins(
      blacklisted_artist_join
    ).joins(
      creative_song_roles_join
    ).joins(
      song_roles_join
    ).group([creative_t[:artist_id], song_role_t[:role_type]])
  end

  def select_statement
    [
      artist_t[:id].as("artist_id"),
      artist_t[:name].as("artist_name"),
      blacklisted_artist_t[:active].as("blacklisted"),
      song_role_t[:role_type].as("role_types"),
      select_as_string(creative_song_role_t[:song_role_id]).as("role_ids"),
      group_concat_distinct(creative_t[:role]).as("credit"),
      group_concat_distinct(creative_t[:creativeable_id]).as("appears_on"),
      creative_t[:person_id].as("person_id")
    ]
  end

  def where_conditions
    creative_t[:creativeable_type].eq("Song")
                                  .and(creative_t[:creativeable_id].in(@songs.map(&:id)))
  end

  def appears_on_all?(contributor)
    tracks = contributor.appears_on.split(",").sort
    @songs.map { |s| s.id.to_s } == tracks
  end
end
