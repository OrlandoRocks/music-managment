class MetadataFetcher
  def self.fetch_metadata(distributable)
    new(distributable).send(:fetch_metadata)
  end

  def self.archived_metadata_url(distributable)
    new(distributable).send(:archived_metadata_url)
  end

  def self.metadata_key(distributable)
    new(distributable).send(:metadata_key)
  end

  def initialize(distributable)
    @distributable = distributable
  end

  private

  def fetch_metadata
    url = AwsWrapper::S3.read_url(
      bucket: PETRI_ARCHIVE_BUCKET,
      key: metadata_key
    )

    Rails.logger.warn("Error fetching metadata for distribution #{@distributable.id}") if url.nil?

    url
  end

  def archived_metadata_url
    return unless @distributable.state == "delivered"

    AwsWrapper::S3.read_url(
      bucket: PETRI_ARCHIVE_BUCKET,
      key: metadata_key,
      options: {
        expires: 10.minutes
      }
    )
  end

  def metadata_key
    converter_store = @distributable.converter_class.split("::")[1]

    folder_name =
      if converter_store == "Amazonod"
        @distributable.petri_bundle.album.id
      else
        @distributable.petri_bundle.album.upc
      end

    "#{folder_name}/#{@distributable.store.short_name}/#{@distributable.job_id}.xml"
  end
end
