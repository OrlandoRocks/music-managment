class BulkAlbumFinder
  attr_reader :album_identifiers, :messages

  def initialize(album_identifiers)
    @album_identifiers = album_identifiers
    @messages = {}
  end

  def execute
    find_albums
  end

  def find_albums
    @album_identifiers =
      if comma_separated?
        album_identifiers.split(/,/).collect(&:strip)
      else
        album_identifiers.split
      end
    collect_albums
  end

  def comma_separated?
    album_identifiers.include?(",")
  end

  def collect_albums
    album_identifiers.map do |id|
      album = find_album_by_id(id)
      album ||= find_album_by_upc(id)
      album ||= find_album_by_isrc(id)
      album || set_missing_album_message(id)
    end.compact
  end

  def find_album_by_id(id)
    Album.includes(:person, :salepoints).find_by(id: id)
  end

  def find_album_by_upc(id)
    upc = Upc.arel_table[:number].eq(id)
    result = Album.includes(:person, :salepoints).joins(:upcs).where(upc).readonly(false)
    result.first unless result.empty?
  end

  def find_album_by_isrc(id)
    return unless id

    Album.includes(:person, :salepoints).joins(:songs)
         .find_by("songs.tunecore_isrc = ? OR songs.optional_isrc = ?", id, id)
  end

  def set_missing_album_message(id)
    @messages[id] = "Album with ID of #{id} or UPC of #{id} or song ISRC of #{id} not found."
    nil
  end
end
