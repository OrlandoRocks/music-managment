class SongTitleizerService
  def self.titleize(song_name, featured_artists)
    Utilities::Titleizer.new(song_name, featured_artists).titleize
  end
end
