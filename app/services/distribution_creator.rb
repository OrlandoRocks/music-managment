class DistributionCreator
  def self.create(album, store_short_name, params = {})
    new(album, store_short_name, params).tap(&:create)
  end

  attr_reader :album, :distribution, :store_short_name, :salepoint, :converter_class, :delivery_type, :store

  def initialize(album, store_short_name, params = {})
    @album            = album
    @store_short_name = store_short_name
    @converter_class  = params[:converter_class] || Store::CONVERTER_MAP.fetch(store_short_name, "MusicStores::DDEX::Converter")
    @delivery_type    = params[:delivery_type] || "full_delivery"
    @state            = params[:state]
  end

  def petri_bundle
    @petri_bundle ||= PetriBundle.for(album)
  end

  def create
    load_salepoint
    return if @salepoint.ytsr_proxy_salepoint?

    load_distribution

    unless @distribution.salepoints.include?(@salepoint)
      Rails.logger.info("Adding salepoint for #{@salepoint.salepointable_type} #{@salepoint.salepointable_id} to #{@distribution.id}. Converter class #{@converter_class}.")
      @distribution.salepoints << @salepoint
    end
    @distribution.delivery_type = delivery_type
    @distribution.state = "new" unless distribution.new?
    @distribution.state = @state unless @state.nil?

    Rails.logger.info("Distribution ID #{@distribution.id} saved!")
    @distribution.save!

    petri_bundle.distributions << distribution
    start_distribution if ready_to_distribute?
  rescue => e
    Tunecore::Airbrake.notify(e)
  end

  private

  def load_distribution
    distributions = petri_bundle.distributions
                                .joins(:salepoints)
                                .where(
                                  distributions: {
                                    converter_class: @converter_class
                                  },
                                  salepoints: {
                                    store_id: salepoint.store_id
                                  }
                                ).readonly(false)

    @distribution = distributions.first_or_create!
  end

  def load_salepoint
    @store = Store.find_by(short_name: store_short_name)
    @salepoint = Salepoint.where(
      store_id: store.id,
      salepointable_id: album.id,
      salepointable_type: "Album"
    ).first_or_initialize

    return if @salepoint.persisted?

    @salepoint.price_policy_id = 17
    @salepoint.has_rights_assignment = true
    @salepoint.payment_applied = true
    @salepoint.save!
  end

  def ready_to_distribute?
    album.approved_for_distribution? &&
      album.finalized_at != nil &&
      distribution.converter_class != "MusicStores::YoutubeSr::Converter" &&
      distribution.valid_for_distribution?
  end

  def start_distribution
    @distribution.start(
      actor: "DistributionCreator#create",
      message: "Starting distribution"
    )
  end
end
