class OpsSupport::RemoveDuplicatedDistributionsSalepointsService
  PER_BATCH_COUNT = 10_000

  def initialize
    @tmp_table_name = "tmp_distributions_salepoints_duplicates";
  end

  def remove!
    log("Starting duplication removal for DistributionsSalepoints Table")
    setup_tmp_table

    duplicates = get_duplicates_populated

    total_combos_to_process = duplicates["total"]

    if total_combos_to_process.zero?
      log("No duplicates to process. Exiting.")
      delete_tmp_table
      return
    end

    batch = 0

    @records_deleted = 0
    @combos_deleted = 0

    log("Starting Cleaning. DuplicateCombos: #{total_combos_to_process}. Rows to purge: #{duplicates['sum']}")

    while total_combos_to_process.positive? do
      log("CombosToProcess:#{total_combos_to_process}")

      combos_deleted_in_batch = process_batch(batch)
      batch += 1
      total_combos_to_process -= combos_deleted_in_batch
    end

    delete_tmp_table
    log("Finished deleting duplicates. CombosDeleted:#{@combos_deleted}. RecordsDeleted:#{@records_deleted}.")
  end

  private

  def setup_tmp_table
    log("Creating temporary table for duplicates")

    # the COUNT(*) - 1 is to delete everything except 1 original record.
    tmp_table_create = <<-SQL
      CREATE TABLE IF NOT EXISTS #{@tmp_table_name} AS
      SELECT ds.*, COUNT(*) - 1 AS records_to_delete
      FROM distributions_salepoints ds
      GROUP BY ds.distribution_id, ds.salepoint_id
      HAVING COUNT(ds.distribution_id) > 1
      AND COUNT(ds.salepoint_id) > 1;
    SQL

    ActiveRecord::Base.connection.execute(tmp_table_create)

    log("Populated temporary table with duplicates.")
  end

  def delete_tmp_table
    ActiveRecord::Base.connection.execute("DROP TABLE IF EXISTS #{@tmp_table_name};")
  end

  def get_duplicates_populated
    duplicates_count_query = "SELECT COUNT(*) total, SUM(records_to_delete) sum FROM #{@tmp_table_name};"
    duplicates = ActiveRecord::Base.connection.select_one(duplicates_count_query)
  end

  def process_batch(batch)
    log("Processing Batch:#{batch}.")
    combos_deleted_in_batch = 0

    query = <<-SQL
      SELECT * FROM #{@tmp_table_name}
      LIMIT #{PER_BATCH_COUNT} OFFSET #{batch * PER_BATCH_COUNT}
    SQL

    ActiveRecord::Base.connection.select_all(query).each do |row|
      delete_combo(row)
      combos_deleted_in_batch += 1
    end

    log("Finished Batch:#{batch}. CombosProcessed:#{@combos_deleted}.")
    combos_deleted_in_batch
  end

  def delete_combo(row)
    distribution_id = row["distribution_id"]
    salepoint_id = row["salepoint_id"]
    amount_to_delete = row["records_to_delete"]

    log("Deleting duplicates of DistributionID:#{distribution_id} SalepointID:#{salepoint_id} Count:#{amount_to_delete}")

    mutation = <<-SQL
      DELETE FROM distributions_salepoints
      WHERE distribution_id = #{distribution_id}
      AND salepoint_id = #{salepoint_id}
      LIMIT #{amount_to_delete}
    SQL

    @records_deleted += amount_to_delete
    @combos_deleted += 1

    ActiveRecord::Base.connection.execute(mutation)

    log("Deleted duplicates of DistributionID:#{distribution_id} SalepointID:#{salepoint_id} Count:#{amount_to_delete}")
  end

  def log(msg)
    Rails.logger.info("#{Time.current}: #{msg}")
  end
end
