class StoreCreator
  class StoreExistsError < StandardError; end

  def self.create(attrs)
    new(attrs).tap(&:create)
  end

  def initialize(attrs)
    @id = attrs[:id]
    raise StoreExistsError.new("Store already exists.") if id && Store.find_by(id: id)

    build_attributes(attrs)
  end

  attr_reader :attributes, :id

  def create
    @store    = Store.new(attributes)
    @store.id = id if id

    return unless @store.save!

    @store.salepointable_stores.create!(salepointable_type: "Album")
    @store.salepointable_stores.create!(salepointable_type: "Single")
  end

  private

  def build_attributes(attrs)
    @attributes = {
      name: attrs[:name],
      abbrev: attrs[:abbrev],
      short_name: attrs[:short_name],
      position: attrs[:position]
    }.merge(default_attributes)
  end

  def default_attributes
    {
      needs_rights_assignment: false,
      is_active: false,
      base_price_policy_id: "3",
      is_free: false,
      in_use_flag: false,
      delivery_service: "petri"
    }
  end
end
