module Cart
  module Purchases
    module Money
      attr_reader :total_savings_cents
      attr_reader :total_cost_cents
      attr_reader :total_vat_cents

      def initialize
        @total_savings_cents = 0
        @total_cost_cents = 0
        @total_vat_cents = 0
      end

      def add_costs(purchase)
        @total_cost_cents += purchase.cost_cents if purchase.cost_cents.present?
        @total_savings_cents += purchase.discount_cents if purchase.discount_cents.present?
        @total_vat_cents += purchase.vat_amount_cents if purchase.vat_amount_cents.present?
      end

      def remove_costs(purchase)
        @total_cost_cents -= purchase.cost_cents if purchase.cost_cents.present?
        @total_savings_cents -= purchase.discount_cents if purchase.discount_cents.present?
        @total_vat_cents -= purchase.vat_amount_cents if purchase.vat_amount_cents.present?
      end

      def total_price_cents
        return 0 if total_cost_cents.zero?

        [(total_cost_cents - total_savings_cents + total_vat_cents), 0].max
      end

      def vat_exclusive_price_cents
        return 0 if total_cost_cents.zero?

        [(total_cost_cents - total_savings_cents), 0].max
      end

      def total_price
        (total_price_cents.to_f / 100)
      end

      def total_savings
        (total_savings_cents.to_f / 100)
      end

      def total_cost
        (total_cost_cents.to_f / 100)
      end

      def total_vat
        (total_vat_cents.to_f / 100)
      end

      def vat_exclusive_price
        (vat_exclusive_price_cents.to_f / 100)
      end
    end

    class Manager
      include Money

      attr_accessor :purchases
      attr_accessor :user
      attr_accessor :related_products
      attr_reader   :automators
      attr_reader   :salepoints
      attr_reader   :others
      attr_reader   :expander_total_cents
      attr_reader   :expander_vat_exclusive_total_cents
      attr_reader   :reports
      attr_reader   :preorders
      attr_reader   :currency

      def initialize(user, purchases, related_products = [])
        super()
        @user = user
        @others = [] # array of BundledPurchases
        @salepoints = [] # array of GroupedPurchases
        @automators = Cart::Purchases::GroupedPurchases.new
        @preorders = Cart::Purchases::GroupedPurchases.new
        @additional_artists = Cart::Purchases::GroupedPurchases.new
        @splits_collaborator = Cart::Purchases::GroupedPurchases.new
        @dolby_atmos_audios = Cart::Purchases::GroupedPurchases.new
        @expander_total_cents = 0
        @expander_vat_exclusive_total_cents = 0
        @reports = []
        @currency = user.person_balance.currency
        self.related_products = related_products
        self.purchases = purchases

        generate_data
      end

      def can_cover_with_balance?
        user.balance >= total_price
      end

      def expander_total
        (expander_total_cents.to_f / 100)
      end

      def expander_vat_exclusive_total
        (expander_vat_exclusive_total_cents.to_f / 100)
      end

      ## Private Methods
      private

      def generate_data
        # Loop over purchases to separate data
        # !! Assuming Salepoints, Subscriptions, and Booklets
        # !! dont have credit usages for them
        others = []
        sp = []
        booklets_hash = {}

        purchases.each do |purchase|
          add_costs(purchase)

          if unbundleable_additional_artist?(purchase) || unbundleable_splits_collaborator?(purchase)
            others << purchase
            next
          end

          case purchase.related_type
          when "Salepoint"
            sp << purchase
          when "SalepointSubscription"
            @automators.add_purchase(purchase.related_id, purchase)
          when "PreorderPurchase"
            @preorders.add_purchase(purchase.related_id, purchase)
          when "SalepointPreorderData"
            @preorders.add_purchase(purchase.related_id, purchase)
          when "Booklet"
            booklets_hash[purchase.related_id] = purchase
          when "ItunesUserReport"
            @reports << Cart::Purchases::ItunesUserReport.new(purchase)
          when Purchase::PLAN_ADDON
            @additional_artists.add_purchase(purchase.related_id, purchase) if purchase.additional_artist?
            @splits_collaborator.add_purchase(purchase.related_id, purchase) if purchase.splits_collaborator?
          when ImmersiveAudio.to_s
            @dolby_atmos_audios.add_purchase(purchase.related_id, purchase)
          else
            others << purchase
          end
        end

        # group all the salepoints by stores
        sp = sp.group_by { |p| p.related.store_id }
        sp.each do |_store_id, store_adds|
          group = Cart::Purchases::GroupedPurchases.new
          store_adds.each do |add_purchase|
            group.add_purchase(add_purchase.id, add_purchase)
          end
          @salepoints << group
          @expander_total_cents += group.total_price_cents
          @expander_vat_exclusive_total_cents += group.vat_exclusive_price_cents
        end

        # loop over all other purchases and grab any bundled items
        # that might exist
        others.each do |purchase|
          item = purchase
          item =
            if purchase.related.is_a?(CreditUsage)
              purchase.related.related
            else
              purchase.related
            end
          bundle = Cart::Purchases::BundledPurchases.new(purchase)

          if item.respond_to?(:booklet) && item.booklet.present? && booklets_hash[item.booklet.id]
            bundle.add_bundled_item(booklets_hash.delete(item.booklet.id))
          end

          if item.respond_to?(:salepoint_subscription) && item.salepoint_subscription.present? && @automators.has_purchase?(item.salepoint_subscription.id)
            bundle.add_bundled_item(@automators.remove_purchase(item.salepoint_subscription.id))
          end

          if item.respond_to?(:preorder_purchase) && item.preorder_purchase.present? && @preorders.has_purchase?(item.preorder_purchase.id)
            bundle.add_bundled_item(@preorders.remove_purchase(item.preorder_purchase.id))
          end

          if item.respond_to?(:salepoint_preorder_data) && item.salepoint_preorder_data.by_store("iTunesWW").present? && @preorders.has_purchase?(item.salepoint_preorder_data.by_store("iTunesWW").first.id)
            bundle.add_bundled_item(@preorders.remove_purchase(item.salepoint_preorder_data.by_store("iTunesWW").first.id))
          end

          if item.respond_to?(:is_plan?) && item.is_plan? && @additional_artists.present?
            @additional_artists.each do |k, _v|
              bundle.add_bundled_item(@additional_artists.remove_purchase(k))
            end
          end

          if item.respond_to?(:is_plan?) && item.is_plan? && @splits_collaborator.present?
            @splits_collaborator.each do |k, _v|
              bundle.add_bundled_item(@splits_collaborator.remove_purchase(k))
            end
          end

          # If user is not upgrading or buying a plan, bundle Additional Artists with the Distribution
          if item.respond_to?(:release?) && item.release? && @additional_artists.present? && !user.plan_in_cart?
            bundle_artists_to_release(bundle, item)
          end

          if item.respond_to?(:dolby_atmos_audios?) && item.dolby_atmos_audios? && @dolby_atmos_audios.present?
            item.spatial_audios.pluck(:id).each do |id|
              next unless @dolby_atmos_audios.has_purchase?(id)

              bundle.add_bundled_item(@dolby_atmos_audios.remove_purchase(id))
            end
          end

          @others << bundle
        end
      end

      def no_plan_or_release_carted?
        @no_plan_or_release_carted ||=
          purchases.none? { |p| p.plan_product_id? || p.free_with_plan? }
      end

      def unbundleable_additional_artist?(purchase)
        return false unless purchase.additional_artist_product_id?

        no_plan_or_release_carted?
      end

      def unbundleable_splits_collaborator?(purchase)
        return false unless purchase.splits_collaborator?

        purchases.none?(&:plan_product_id?)
      end

      def bundle_artists_to_release(bundle, item)
        finalized_at = item.finalized_at if bundle.purchase.paid?

        person = item.person
        additional_artist_count = Plans::AdditionalArtistService.calculate_new_artists(person, item)

        if person.active_artists(finalized_at).empty? && !@subtracted_one_additional_artist
          additional_artist_count -= 1
          @subtracted_one_additional_artist = true
        end

        additional_artist_count.times do
          key = @additional_artists.first_key
          break if key.nil?

          bundle.add_bundled_item(@additional_artists.remove_purchase(key))
        end
      end
    end # Manager

    #
    # - Grouped purchases don't work for credited item
    # - Assumes everything same class
    #
    class GroupedPurchases
      include Money

      attr_reader :purchases
      attr_reader :person

      def initialize
        super
        @purchases = {}
        @person = nil
      end

      def add_purchase(key, purchase)
        @purchases[key] = purchase
        @person ||= purchase.person

        add_costs(purchase)
      end

      def remove_purchase(key)
        purchase = @purchases.delete(key)
        remove_costs(purchase)
        purchase
      end

      def has_purchase?(key)
        purchases && purchases.has_key?(key)
      end

      def each
        @purchases.each do |k, v|
          yield k, v
        end
      end

      def empty?
        @purchases.empty?
      end

      def first_key
        return if empty?

        @purchases&.keys&.first
      end
    end # GroupedPurchases

    class BundledPurchases
      include Money

      attr_accessor :bundled_items
      attr_reader   :purchase
      attr_reader   :credited
      attr_reader   :person

      def initialize(purchase_item)
        super()
        @purchase = purchase_item
        @person   = purchase_item.person
        @credited = purchase_item.credited?
        @bundled_items = []

        add_costs(purchase_item)
      end

      def add_bundled_item(item)
        @bundled_items << item
        add_costs(item)
      end

      def credited?
        purchase.credited?
      end

      def purchased_item
        item = purchase.purchased_item
        item = item.related if credited?
        item
      end
    end # BundledPurchase

    class ItunesUserReport
      include Money

      ITEM_NAME = "iTunes User Report"

      def initialize(purchase_item)
        super()

        add_costs(purchase_item)
      end
    end
  end
end # end module
