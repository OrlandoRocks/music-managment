class Cart::Payment::Manager
  attr_reader   :purchases
  attr_reader   :strategies
  attr_reader   :ip_address
  attr_accessor :invoice
  attr_accessor :person
  attr_accessor :notice

  def initialize(person, purchases, ip_address)
    @strategies = []
    @purchases  = purchases
    @person     = person
    @ip_address = ip_address
    @successful = false
    @redirect_for_otp = false
  end

  def add_strategy(strategy)
    @strategies << strategy
  end

  def process!
    return if purchases.blank?
    return if purchases.any?(&:processing?)

    strats = strategies.clone
    balance = []
    strats.delete_if { |s| balance << s if s.is_a?(Cart::Payment::Strategies::Balance) }

    purchases.each(&:processing!)

    ActiveRecord::Base.transaction do
      self.invoice = Invoice.factory(person, purchases)
      raise "invoice not created!" if invoice.blank?

      if invoice.can_settle?
        invoice.settled!
      else
        # Use balance first
        (balance + strats).each do |strategy|
          strategy.process(self)
          break if invoice.settled?

          invoice.reload
        end
      end

      @redirect_for_otp = needs_otp_redirect?

      if raise_error_for_QA? # should be deleted after QA approval. this allows us to ensure there is an error
        raise "QA testing for refunds"
      end

      raise "invoice not settled!" unless invoice.settled? || @redirect_for_otp.present?

      @successful = true
    end
  rescue StandardError => e
    reset_purchase_invoice_ids_to_nil

    invoice_errors = invoice.valid? ? "none" : invoice.errors.full_messages.join(",")

    invoice_error_message = "\ninvoice errors: #{invoice_errors}"
    notice_message        = "\nnotice: #{notice}"
    log_error = "Transaction Failed: #{e.message} invoice: #{invoice.to_json}#{invoice_error_message}#{notice_message}"

    Airbrake.notify(
      e,
      {
        invoice: invoice,
        invoice_error: invoice_errors.to_s,
        notice: notice
      }
    )

    Rails.logger.error log_error
    Rails.logger.error e.backtrace

    options = {
      person: @person,
      invoice: invoice,
      message: log_error,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)

    (balance + strats).each(&:rollback_handler)
  ensure
    @person.reload
    unless @redirect_for_otp
      @successful ? purchases.each(&:processed!) : purchases.each(&:failed!)
    end
  end

  def successful?
    @successful
  end

  def reauthorize
    strategies.each do |strat|
      return strat.authorizer if strat.authorizer
    end

    nil
  end

  def needs_otp_redirect?
    strategies.any? { |s| s.try(:needs_otp_redirect?) }
  end

  def otp_redirect_url
    strategies.find { |s| s.respond_to?(:otp_redirect_url) }&.otp_redirect_url
  end

  private

  def reset_purchase_invoice_ids_to_nil
    purchases.map { |purchase| purchase.reload if purchase.class.exists?(purchase.id) }
  end

  def raise_error_for_QA? # should be deleted after QA approval. this allows us to ensure there is an error
    FeatureFlipper.show_feature?(:qa_test_braintree_refunds, @person)
  end
end
