# frozen_string_literal: true

require "./lib/slack_notifier"

class Cart::Payment::Strategies::Paypal
  AIRBRAKE_MESSAGE = "User erroneously charged by Paypal and automatically refunded, see GS-9709"
  SLACK_ALERT_MESSAGE = "A failed Paypal transaction was refunded. Transaction id: "

  attr_accessor :stored_paypal_account
  attr_accessor :txn
  attr_accessor :authorizer
  attr_accessor :person

  def initialize(stored_paypal_account, person)
    @stored_paypal_account = stored_paypal_account
    @person = person
    @txn = nil
  end

  def process(context)
    person = context.person
    invoice = context.invoice

    return if invoice.settled?

    self.txn = PaypalTransaction.process_reference_transaction(stored_paypal_account, invoice)
    return if txn.success?

    context.notice = txn.error_code_message
    context.notice ||= "An error occurred while processing your PayPal payment. Please contact customer support."
  end

  def rollback_handler
    return if txn.blank?

    # re-create the txn record that was rolled back
    self.txn = txn.dup
    txn.save

    # this metadata is present in rollback_handlers, and we know it is dubious
    method_metadata = {
      current_method_name: :rollback_handler,
      caller_method_path: caller&.first
    }

    if charged_by_paypal_and_refund_failed_transactions?
      refund_transaction
      log_options("refunding failed paypal transaction", method_metadata)
    else
      log_options("save successful", method_metadata)
    end

    error_handler
  end

  def log_options(message, method_metadata)
    options = {
      person: txn&.person,
      invoice: txn&.invoice,
      paypal_transaction_id: txn&.id,
      message: message
    }.merge!(method_metadata)

    InvoiceLogService.log(options)
  end

  def charged_by_paypal_and_refund_failed_transactions?
    charged_by_paypal? && refund_failed_transactions?
  end

  def charged_by_paypal?
    txn.completed? && txn.sale?
  end

  def refund_failed_transactions?
    FeatureFlipper.show_feature?(:refund_failed_paypal_transactions, txn.person)
  end

  def refund_transaction
    paypal_transaction = txn

    refunded_transaction = PayPalAPI.refund(paypal_transaction.payin_provider_config, paypal_transaction.transaction_id)
    PaypalTransaction.record_refunded_transaction(refunded_transaction, paypal_transaction)

    send_paypal_refund_airbrake(paypal_transaction.id)
  end

  private

  def error_handler
    case txn.raw_response
    when /L_ERRORCODE0=10201/
      stored_paypal_account.destroy(person, skip_validation: true)
      self.authorizer = :paypal
    end
  end

  def send_paypal_refund_airbrake(paypal_transaction_id)
    airbrake_options = {
      person: txn&.person,
      paypal_transaction: txn,
      caller_method_path: caller&.first
    }
    Tunecore::Airbrake.notify(AIRBRAKE_MESSAGE, airbrake_options)
    notify_in_slack(SLACK_ALERT_MESSAGE + paypal_transaction_id.to_s, "Paypal Transactions")
  end
end
