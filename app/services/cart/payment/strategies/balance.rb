class Cart::Payment::Strategies::Balance
  def process(context)
    person  = context.person
    balance = person.person_balance.balance
    invoice = context.invoice

    if balance < 0.01
      message = "balance less than a penny, balance: #{balance}"
      options = {
        person: person,
        invoice: invoice,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      return
    end
    if invoice.settled?
      message = "invoice already settled, balance: #{balance}"
      options = {
        person: person,
        invoice: invoice,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      return
    end
    royalty_purchase = process_vat(person, invoice, balance)
    amount = royalty_purchase.amount
    txn = PersonTransaction.create!(
      person: person,
      debit: Tunecore::Numbers.cents_to_decimal(amount),
      credit: 0,
      target: invoice,
      comment: "Payment for Invoice #{invoice.code}"
    )

    message = "balance: #{balance}, person_transacton_id: #{txn&.id}, can_settle: #{invoice&.can_settle?}"
    options = {
      person: person,
      invoice: invoice,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)

    invoice.settlement_received(txn, amount)
    royalty_purchase.generate_outbound_invoice(invoice.person_transaction_settlement)
    invoice.settled! if invoice.can_settle?
  end

  def rollback_handler
    nil
  end

  def authorizer
    nil
  end

  private

  def process_vat(person, invoice, balance)
    royalty_purchase = TcVat::RoyaltyPurchase.new(person, invoice, balance)
    royalty_purchase.process_vat!

    royalty_purchase
  end
end
