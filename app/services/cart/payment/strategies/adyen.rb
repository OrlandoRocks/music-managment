class Cart::Payment::Strategies::Adyen
  attr_accessor :redirect, :person, :invoice, :payment_method

  include Rails.application.routes.url_helpers

  def initialize(payment_method)
    @payment_method = payment_method
  end

  def process(context)
    self.person  = context.person
    self.invoice = context.invoice
    return if invoice.settled?

    AdyenTransaction.create_settlement(context.invoice)

    self.redirect = "#{dropin_cart_path}?invoice_id=#{invoice.id}&payment_method=#{payment_method}"
  end

  def needs_otp_redirect?
    true
  end

  def otp_redirect_url
    redirect
  end

  def rollback_handler
    log_rollback
  end

  def authorizer
    nil
  end

  private

  def log_rollback
    options = {
      person: person,
      invoice: invoice,
      message: "adyen transaction rolled back",
    }
    InvoiceLogService.log(options)
  end
end
