class Cart::Payment::Strategies::PaymentsOSCard
  attr_accessor :stored_card_id
  attr_accessor :txn

  def initialize(stored_card_id, cvv)
    @stored_card_id = stored_card_id
    @cvv            = cvv
    @txn            = nil
  end

  def process(context)
    person  = context.person
    invoice = context.invoice
    return if invoice.settled?

    stored_card = person.stored_credit_cards.find(stored_card_id)
    self.txn = PaymentsOS::PaymentsService.new(invoice, stored_card, @cvv).pay
    context.notice = I18n.t "payments_os.failed_transaction" if txn.failed?
  end

  def success?
    !txn&.failed?
  end

  def rollback_handler
    # Preserve the payments os transaction if present
    txn.save if txn.present?
    log_rollback
  end

  def otp_redirect_url
    txn&.otp_redirect_url
  end

  def needs_otp_redirect?
    true
  end

  def authorizer
    nil
  end

  private

  def log_rollback
    options = {
      person: txn&.person,
      invoice: txn&.invoice,
      payments_os_transaction_id: txn&.id,
      message: "payments os transaction rolled back",
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
  end
end
