# frozen_string_literal: true

require "./lib/slack_notifier"

class Cart::Payment::Strategies::CreditCard
  STATUS = {
    DECLINED: "declined",
    DUPLICATE: "duplicate",
    ERROR: "error",
    FRAUD: "fraud",
    SUCCESS: "success"
  }.freeze

  ACTION = {
    REFUND: "refund",
    SALE: "sale"
  }.freeze

  AIRBRAKE_MESSAGE = "User erroneously charged by Braintree and automatically refunded, see GS-7471"
  SLACK_ALERT_MESSAGE = "A failed Braintree transaction was refunded. Transaction id: "
  REFUND_REASON = "Rollback after error processing charge"

  attr_accessor :stored_card_id
  attr_accessor :txn
  attr_reader :three_d_secure_nonce

  def initialize(stored_card_id, three_d_secure_nonce = nil)
    @stored_card_id = stored_card_id
    @txn            = nil
    @three_d_secure_nonce = three_d_secure_nonce
  end

  def process(context)
    person  = context.person
    invoice = context.invoice

    if invoice.settled?
      message = "invoice already settled"
      options = {
        person: person,
        invoice: invoice,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      return
    end

    stored_card = person.stored_credit_cards.find(stored_card_id)
    amount      = invoice.outstanding_amount_cents

    self.txn = BraintreeTransaction.process_purchase_with_stored_card(
      invoice,
      stored_card,
      { payment_amount: amount, ip_address: context.ip_address, three_d_secure_nonce: three_d_secure_nonce }
    )

    if txn.success?
      message = "Transaction Successful."
      options = {
        person: person,
        invoice: invoice,
        braintree_transaction_id: txn&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      txn
    else
      message = I18n.t "payments_braintree.failed_transaction"
      options = {
        person: person,
        invoice: invoice,
        braintree_transaction_id: txn&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      context.notice = message
    end
  end

  def rollback_handler
    # re-create the txn record that was rolled back.
    return if txn.blank?

    # because of the rollback, txn is in a weird state so we need to dup it to
    # create a new object instead of trying to save the rolledback record
    #
    # Fixes https://tunecore.atlassian.net/browse/UP-112
    #
    # likely also fixes other credit card transaction issues & missing
    # braintree transaction issues
    # https://tunecore.atlassian.net/browse/UP-74
    # https://tunecore.atlassian.net/browse/UP-125
    self.txn = txn.dup
    txn.save

    if txn.status == STATUS[:FRAUD]
      message = "Attempted a potentially fraudulent credit card purchase. Status Code: #{txn.response_code}"
      options = {
        person: txn&.person,
        invoice: txn&.invoice,
        braintree_transaction_id: txn&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      Note.create(
        note: message,
        note_created_by_id: 0,
        related: txn.person,
        subject: "Possibly Fraudulent Purchase"
      )
    elsif charged_by_braintree_and_autorefund_failed_braintree_transactions?
      refund_braintree_transaction
      txn.status

      message = "rollback and refund"
      options = {
        person: txn&.person,
        invoice: txn&.invoice,
        braintree_transaction_id: txn&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
    else
      message = "save successful"
      options = {
        person: txn&.person,
        invoice: txn&.invoice,
        braintree_transaction_id: txn&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
    end
  end

  def authorizer
    nil
  end

  private

  def autorefund_failed_braintree_transactions?
    FeatureFlipper.show_feature?(:autorefund_failed_braintree_transactions, txn.person)
  end

  def charged_by_braintree?
    txn.status == STATUS[:SUCCESS] && txn.action == ACTION[:SALE]
  end

  def charged_by_braintree_and_autorefund_failed_braintree_transactions?
    charged_by_braintree? && autorefund_failed_braintree_transactions?
  end

  def refund_braintree_transaction
    # Sometimes txns here will not have an id. Sometimes txns do not have an invoice.
    braintree_transaction_id = txn.id || txn.invoice.braintree_transactions.first.id

    # process_refund method needs the following options for the refund txn to save:
    #   refund_reason, refund_category, refunded_by_id, ip_address
    BraintreeTransaction.process_refund(braintree_transaction_id, txn.amount, refund_reason: REFUND_REASON)
    mark_txn_as_refunded
    send_braintree_refund_airbrake(braintree_transaction_id)
  end

  def mark_txn_as_refunded
    txn.update(action: ACTION[:REFUND], status: STATUS[:ERROR])
  end

  def send_braintree_refund_airbrake(braintree_transaction_id)
    airbrake_options = {
      person: txn&.person,
      invoice: txn&.invoice,
      braintree_transaction: txn,
      caller_method_path: caller&.first
    }
    Tunecore::Airbrake.notify(AIRBRAKE_MESSAGE, airbrake_options)
    notify_in_slack(SLACK_ALERT_MESSAGE + braintree_transaction_id.to_s, "Braintree Transactions")
  end
end
