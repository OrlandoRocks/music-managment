class Cart::Payment::Strategies::Payu
  attr_accessor :redirect
  attr_accessor :person
  attr_accessor :invoice

  def process(context)
    self.person  = context.person
    self.invoice = context.invoice
    return if invoice.settled?

    self.redirect = PayuTransaction.setup(invoice)
  end

  def success?
    true
  end

  def needs_otp_redirect?
    true
  end

  def rollback_handler
    log_rollback
  end

  def otp_redirect_url
    redirect
  end

  def authorizer
    nil
  end

  private

  def log_rollback
    options = {
      person: person,
      invoice: invoice,
      message: "payu transaction rolled back",
    }
    InvoiceLogService.log(options)
  end
end
