class Cart::SocialCtaManager
  def self.manage(person, purchases)
    new(person, purchases).tap(&:manage)
  end

  attr_accessor :person, :purchases, :plan, :cta_type

  def initialize(person, purchases)
    @person         = person
    @purchases      = purchases || []
    @plan           = Social::PlanStatus.for(person).plan
    @social_products = Product.where(display_name: "tc_social_monthly")
  end

  def manage
    @cta_type = get_cta_type
  end

  def show_cta?
    if available_for_person?
      return false if in_cart?
      return false if previously_purchased?

      true
    else
      false
    end
  end

  def social_product
    @social_product ||= social_products_for_country_website.first
  end

  private

  def social_products_for_country_website
    @social_products.where(country_website_id: person.country_website_id)
  end

  def available_for_person?
    social_products_for_country_website.any?
  end

  def get_cta_type
    return "none" unless show_cta?

    (plan == "free" && person.tc_social_token) ? "upgrade" : "buy"
  end

  def in_cart?
    return false unless social_product

    purchases.map(&:product_id).include?(social_product.id)
  end

  def previously_purchased?
    %w[pro pro_expired pro_expired_free].include?(plan)
  end
end
