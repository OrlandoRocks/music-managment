class BulkAlbumApprovalService
  APPROVAL_STATES = ["STARTED REVIEW", "APPROVED"].map(&:freeze).freeze
  NOTE            = "Low Risk Auto-Approval".freeze

  attr_reader :albums, :person

  def self.approve(options)
    new(options).approve
  end

  def initialize(options)
    @albums = Album.find(options[:album_ids])
    @person = Person.find(options[:person_id])
  end

  def approve
    @albums.each do |album|
      ReviewAudit.transaction do
        APPROVAL_STATES.each do |state|
          ReviewAudit.create!(
            album: album,
            person: person,
            event: state,
            note: NOTE
          )
        end
      end
    end
    true
  rescue StandardError => e
    false
  end
end
