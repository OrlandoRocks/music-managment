module AssetsUploader
  class Transcoder
    attr_accessor :asset_url, :song, :orig_filename, :source_key, :source_bucket, :file_type_validation_enabled

    MP3_OUTPUT_FORMAT = {
      extension: "mp3"
    }

    CALLBACK_URL = "https://#{CALLBACK_API_URLS['US']}/api/transcoder/songs/set_streaming_asset".freeze

    MINIMUM_VALID_DURATION = 2001
    MAXIMUM_VALID_DURATION = Integer(ENV.fetch("MAXIMUM_VALID_DURATION", "10_800_000"), 10)

    DURATION_ERROR = "Duration is 2 seconds or less.".freeze
    MAX_DURATION_ERROR = "Duration is greater than 3 hours.".freeze
    IMMERSIVE_DURATION_ERROR = :immersive_duration_error
    STEREO_FILE_TYPE_ERROR = :stereo_file_type_error

    def initialize(args = {})
      @source_key = args[:source_key]
      @source_bucket = args[:source_bucket]
      @asset_url = args[:asset_url]
      parse_asset_url if asset_url
      @song = args[:song]
      @orig_filename = args[:orig_filename].presence || "#{timestamp}-#{File.basename(@source_key)}"
      @file_type_validation_enabled = args[:file_type_validation_enabled] == true
    end

    def upload
      copy_asset_to_original_location

      save_streaming_asset

      asset_details
    end

    def asset_details
      {
        uuid: SecureRandom.uuid,
        key: original_asset_key,
        bucket: BIGBOX_BUCKET_NAME,
        orig_filename: orig_filename,
        bit_rate: bit_rate,
        duration: duration,
        status: 0,
        created_at: timestamp,
        streaming_url: streaming_url
      }
    end

    def validate_asset
      total_duration = get_total_duration
      errors = {}
      errors[:validate_duration] = IMMERSIVE_DURATION_ERROR if invalid_immersive_duration?
      errors[:duration] = DURATION_ERROR if duration < MINIMUM_VALID_DURATION
      errors[:max_duration] = MAX_DURATION_ERROR if total_duration > MAXIMUM_VALID_DURATION &&
                                                    FeatureFlipper.show_feature?(:long_track_approval, @album.person)

      errors[:validate_file_type] = STEREO_FILE_TYPE_ERROR if file_type_validation_enabled && !valid_file_type

      return { errors: errors } if errors.present?

      asset_details
    end

    def get_total_duration
      @album = Album.find(song[:album_id])

      tracks = @album.songs.select { |track| track.id != song.id && track.duration }
      tracks.sum { |track| track.duration * 1000 } + duration
    end

    def copy_asset_to_original_location
      return if source_bucket == BIGBOX_BUCKET_NAME

      new_object = destination_bucket_object.object(original_asset_key)
      new_object.copy_from(source_bucket_object.object(source_key))
    end

    def save_streaming_asset
      # Clear the s3 streaming asset before creating a new one
      remove_old_s3_streaming_asset
      return save_original_asset_as_streaming_asset if codec_name == MP3_OUTPUT_FORMAT[:extension]

      transcode_asset_to_mp3
    end

    def streaming_url
      s3 = Aws::S3::Resource.new
      object = s3.bucket(BIGBOX_BUCKET_NAME).object(source_key)
      object.presigned_url(:get)
    end

    def remove_old_s3_streaming_asset
      song.update(s3_streaming_asset: nil)
    end

    def transcode_asset_to_mp3
      AssetsBackfill::MigrateNonFlac.new(
        {
          song: song,
          asset: s3_orig_asset,
          destination_asset_key: streaming_asset_key,
          output_format: MP3_OUTPUT_FORMAT,
          callback_url: CALLBACK_URL
        }
      ).run
    end

    def save_original_asset_as_streaming_asset
      upload_streaming_asset

      song.update!(
        {
          s3_streaming_asset: S3Asset.create(
            key: streaming_asset_key,
            bucket: BIGBOX_BUCKET_NAME
          )
        }
      )
    end

    def upload_streaming_asset
      new_object = destination_bucket_object.object(streaming_asset_key)
      new_object.copy_from(source_bucket_object.object(source_key))
    end

    def bit_rate
      ffprobe_metadata["format"]["bit_rate"]
    end

    def codec_name
      ffprobe_metadata["streams"][0]["codec_name"]
    end

    def duration
      (Float(ffprobe_metadata["streams"][0]["duration"]) * 1000).ceil
    end

    def original_asset_key
      "#{person_id}/#{orig_filename}"
    end

    def streaming_asset_key
      "#{person_id}/#{orig_filename}.mp3"
    end

    def ffprobe_metadata
      @ffprobe_metadata ||= s3_orig_asset.fetch_asset_metadata
    end

    def s3_orig_asset
      @s3_orig_asset ||= S3Asset.new(key: original_asset_key, bucket: BIGBOX_BUCKET_NAME)
    end

    def timestamp
      @timestamp ||= Time.current.strftime("%s%3N").to_i
    end

    def person_id
      @person_id ||= song.person.id
    end

    def invalid_immersive_duration?
      return false unless song.spatial_audio_asset

      response = ImmersiveAudio::ValidationService.new(
        s3_orig_asset,
        song.spatial_audio_asset.key,
        song.spatial_audio_asset.bucket,
        SpatialAudio::AUDIO_TYPE
      ).call
      !!response[:errors][:validate_duration]
    end

    def parse_asset_url
      @source_bucket = source_asset_uri.hostname
      @source_key = source_asset_uri.path[1..-1]
    end

    def source_asset_uri
      @source_asset_uri ||= URI.parse(asset_url)
    end

    def source_bucket_object
      Aws::S3::Bucket.new(source_bucket)
    end

    def destination_bucket_object
      Aws::S3::Bucket.new(BIGBOX_BUCKET_NAME)
    end

    def valid_file_type
      file_type_is_not_atmos?
    end

    def file_type_is_not_atmos?
      amtos_file_format = (media_metadata&.dig("media", "track") || [])[-1]&.dig("extra", "AdmProfile_Format")

      amtos_file_format.blank?
    end

    def media_metadata
      return @media_metadata if @media_metadata

      s3_object = Aws::S3::Resource.new.bucket(source_bucket).object(source_key)
      presigned_get_url = s3_object.presigned_url(:get, expires_in: 3600)
      output = `mediainfo --Output=JSON "#{presigned_get_url}"`
      @media_metadata = JSON.parse(output)
    end
  end
end
