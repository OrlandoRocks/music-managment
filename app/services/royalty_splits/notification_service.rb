# frozen_string_literal: true

class RoyaltySplits::NotificationService
  class << self
    include Rails.application.routes.url_helpers
    include ActiveSupport::NumberHelper
    include RoyaltySplitHelper
    include ActionView::RecordIdentifier
  end

  def self.added_to_split(recipient)
    royalty_split = recipient.royalty_split
    person = recipient.person
    I18n.with_locale person.locale do
      Notification.create(
        person: person,
        title: I18n.t("royalty_splits.notifications.you_were_added_title"),
        text: I18n.t(
          "royalty_splits.notifications.you_were_added_html",
          mah_name: royalty_split.owner.name,
          split_percent: number_to_percentage(recipient.percent, precision: 1, locale: person.bcp47_locale),
          split_title: royalty_split.title,
          applied_on: split_description(royalty_split),
        ),
        url: invited_royalty_splits_path(anchor: dom_id(royalty_split)),
        link_text: I18n.t("royalty_splits.notifications.view_invited_split"),
      )
    end
  end

  def self.percent_changed(recipient, from:)
    royalty_split = recipient.royalty_split
    person = recipient.person
    I18n.with_locale person.locale do
      Notification.create(
        person: person,
        title: I18n.t("royalty_splits.notifications.percent_changed_title"),
        text: I18n.t(
          "royalty_splits.notifications.percent_changed_html",
          split_title: royalty_split.title,
          from_percent: number_to_percentage(from, precision: 1, locale: person.bcp47_locale),
          to_percent: number_to_percentage(recipient.percent, precision: 1, locale: person.bcp47_locale),
        ),
        url: invited_royalty_splits_path(anchor: dom_id(royalty_split)),
        link_text: I18n.t("royalty_splits.notifications.view_invited_split"),
      )
    end
  end

  def self.removed_from_split(person:, title:)
    # NOTE: The recipient and maybe the royalty_split are deleted at this point
    I18n.with_locale person.locale do
      Notification.create(
        person: person,
        title: I18n.t("royalty_splits.notifications.you_were_removed_title"),
        text: I18n.t(
          "royalty_splits.notifications.you_were_removed_html",
          split_title: title,
        ),
      )
    end
  end

  def self.songs_added_to_split(split:, new_songs:)
    split.not_owner_recipients.map do |recipient|
      next unless recipient.person?

      I18n.with_locale recipient.person.locale do
        Notification.create(
          person: recipient.person,
          title: I18n.t("royalty_splits.notifications.new_tracks_added"),
          text: I18n.t(
            "royalty_splits.notifications.your_accepted_split_has_been_applied",
            split_title: split.title,
            release_name: new_songs.map(&:album).first.name,
          ),
          url: invited_royalty_splits_path(anchor: dom_id(split)),
          link_text: I18n.t("royalty_splits.notifications.view_invited_split"),
        )
      end
    end
  end
end
