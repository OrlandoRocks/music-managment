class RoyaltySplits::EventService
  thread_cattr_accessor :current_admin_or_user_id, instance_accessor: false

  # Recipient
  def self.recipient_created(recipient)
    return if recipient.owner?

    # TODO: Send onboarding email
    RoyaltySplitsMailerWorker.perform_async(:new_split_invite, { recipient_id: recipient.id, locale: I18n.locale })

    return unless recipient.person?

    RoyaltySplits::NotificationService.added_to_split(recipient)
  end

  def self.recipient_deactivated(recipient)
  end

  def self.recipient_changed_percent(recipient, from:)
    return if recipient.owner?
    return unless recipient.person?

    RoyaltySplits::NotificationService.percent_changed(recipient, from: from)
    RoyaltySplitsMailerWorker.perform_async(:split_percent_changed, { recipient_id: recipient.id, from: from })
  end

  def self.recipient_destroyed(recipient)
    return if recipient.owner?
    return unless recipient.person?

    person = recipient.person
    split = recipient.royalty_split
    split_title = split.title
    songs = split.songs
    RoyaltySplits::NotificationService.removed_from_split(
      person: person,
      title: split_title
    )
    RoyaltySplitsMailerWorker.perform_async(
      :removed_from_split,
      {
        owner_id: split.owner_id,
        song_ids: songs.map(&:id),
        person_id: person.id,
        title: split_title
      }
    )
  end

  # Song
  def self.song_created(royalty_split_song)
  end

  def self.song_destroyed(royalty_split_song)
  end

  # Split
  def self.split_created(royalty_split)
  end

  def self.split_deactivated(royalty_split)
  end

  def self.split_changed_title(royalty_split)
  end

  def self.split_destroyed(royalty_split)
  end
end
