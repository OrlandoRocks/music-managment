class TaxFormCheckService
  include ArelTableMethods
  include ArelJoinMethods
  include ArelNamedMysqlFunctions

  TAXABLE_THRESHOLD = 5.00

  attr_reader :person, :person_id, :taxable_person

  def self.needs_tax_form?(person_id)
    new(person_id).needs_tax_form?
  end

  def self.check_api?(person_id)
    new(person_id).check_api?
  end

  def initialize(person_id)
    @person_id = person_id
    @taxable_person = person_tax_query.first
  end

  def needs_tax_form?
    !Payoneer::FeatureService.payoneer_exclude_tax_check?(Person.find(@person_id)) &&
      from_applicable_country? && meets_threshold? && !active_form?
  end

  def from_applicable_country?
    Country::UNITED_STATES_AND_TERRITORIES.include?(taxable_person.country_code)
  end

  def meets_threshold?
    fiscal_years_earnings.any? { |year| year.earnings >= TAXABLE_THRESHOLD }
  end

  def active_form?
    taxable_person.active_tax_form.nil? ? check_api? : true
  end

  def check_api?
    return false if taxable_person.payee_id.nil?

    tax_service = Payoneer::TaxApiService.new(tax_api_params)
    tax_service.save_tax_form if tax_service.has_active_forms?

    if Payoneer::FeatureService.secondary_taxform_submission?(person_id)
      secondary_tax_service = Payoneer::TaxApiService.new(
        tax_api_params.merge!(program_id: PayoutProviderConfig::SECONDARY_TAX_FORM_PROGRAM_ID)
      )
      secondary_tax_service.save_tax_form if secondary_tax_service.has_active_forms?
    end
    tax_service.has_active_forms? || (secondary_tax_service && secondary_tax_service.has_active_forms?)
  end

  def person_tax_query
    Person.select(
      tax_select
    ).joins(
      taxable_earnings_outer_join
    ).joins(
      tax_forms_outer_join
    ).joins(
      payout_providers_outer_join
    ).where(
      person_t[:id].eq(person_id)
    ).limit(1)
  end

  def tax_select
    [
      person_t[:id].as("person_id"),
      person_t[:name],
      person_t[:country].as("country_code"),
      person_t[:country_website_id].as("website_code"),
      ytd_earnings.as("earnings"),
      tax_form_t[:id].as("active_tax_form"),
      payout_provider_t[:client_payee_id].as("payee_id")
    ]
  end

  def ytd_earnings
    select_sum(taxable_earning_t[:credit])
  end

  def taxable_earnings_outer_join
    person_t.join(
      taxable_earning_t, Arel::Nodes::OuterJoin
    ).on(
      person_t[:id].eq(taxable_earning_t[:person_id])
        .and(select_year(taxable_earning_t[:created_at]).eq(Date.today.year))
    ).join_sources
  end

  def tax_forms_outer_join
    person_t.join(
      tax_form_t, Arel::Nodes::OuterJoin
    ).on(
      person_t[:id].eq(tax_form_t[:person_id])
        .and(tax_form_t[:expires_at].gteq(Date.today))
    ).join_sources
  end

  def payout_providers_outer_join
    person_t.join(
      payout_provider_t, Arel::Nodes::OuterJoin
    ).on(
      person_t[:id].eq(payout_provider_t[:person_id])
        .and(payout_provider_t[:active_provider].eq(true))
    ).join_sources
  end

  def tax_api_params
    { PartnerPayeeID: taxable_person.payee_id }
  end

  def fiscal_years_earnings
    max_tax_years_ago = Date.today.year - 2

    TaxableEarning.select(
      ytd_earnings.as("earnings")
    ).where(
      select_year(taxable_earning_t[:created_at]).gteq(max_tax_years_ago)
    ).where(
      taxable_earning_t[:person_id].eq(person_id)
    ).group(
      select_year(taxable_earning_t[:created_at])
    )
  end
end
