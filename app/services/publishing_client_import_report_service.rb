class PublishingClientImportReportService
  include ZipFolder
  attr_reader :person, :options

  def initialize(options)
    @person = Person.find_by(id: options[:person_id])
    @options = options
  end

  def process
    folder, zip_filename = PublishingChangeoverReport.client_import_csv(options)

    return unless folder

    zip_folder(folder, zip_filename)
    person.notes.create(
      subject: "Publishing Client Import Download",
      note: "Downloaded #{zip_filename}",
      note_created_by: person
    )
    AdminNotifier.client_import_report(person, folder, zip_filename).deliver
    remove_dir(folder)
  end

  private

  def zip_folder(folder, zip_filename)
    zipfile_name = File.join(folder, "/#{zip_filename}.zip")
    compress_folder(folder, zipfile_name)
  end

  def remove_dir(folder)
    FileUtils.remove_dir(folder)
  end
end
