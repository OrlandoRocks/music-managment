class SalepointService
  include SalepointSystem

  def self.call(person, album, salepoint_params = {}, apple_music_opt_in = nil)
    new(person, album, salepoint_params, apple_music_opt_in).call
  end

  attr_reader :person,
              :album,
              :salepoints,
              :apple_music,
              :itunes_ww,
              :itunes_us,
              :itunes_ww_salepoints,
              :itunes_us_salepoints,
              :is_itunes_upgrade,
              :aod_just_added

  def initialize(person, album, salepoint_params = {}, apple_music_opt_in = nil)
    @person         = person
    @album          = album
    @salepoints     = transform_salepoints(salepoint_params)
    @apple_music    = apple_music_opt_in.present?
    @aod_just_added = amazon_on_demand_new?
    setup_itunes_variables
  end

  def call
    Salepoint.transaction do
      set_album_salepoints

      salepoints.each do |salepoint|
        salepoint.salepointable = album
        handle_itunes_upgrade(salepoint)
        salepoint.save!
      end

      album.apple_music = apple_music
      album.save!
    end
    true
  rescue => e
    Tunecore::Airbrake.notify("SalepointService Error: #{e}")
    false
  end

  def amazon_on_demand
    @amazon_on_demand ||= Store.find_by(short_name: "AmazonOD")
  end

  def fb_reels
    @fb_reels ||= Store.find(Store::FB_REELS_STORE_ID)
  end

  def amazon_on_demand_new?
    @salepoints.any? { |sp| sp.store_id == amazon_on_demand.id } && !@album.has_aod
  end

  def aod_salepoint
    @album.salepoints.where(store: amazon_on_demand).first
  end

  private

  def set_album_salepoints
    new_salepoints, existing_salepoints  = salepoints.partition(&:new_record?)
    album.salepoints                     = existing_salepoints
    album.salepoints << new_salepoints # a bit round-about, but necessary for how error handling currently works (raises error on album at later point)
  end

  def setup_itunes_variables
    @itunes_ww            = Store.find_by(short_name: "iTunesWW")
    @itunes_us            = Store.find_by(short_name: "iTunesUS")
    @itunes_ww_salepoints = salepoints.select       { |sp| sp.store_id == itunes_ww.id }
    @itunes_us_salepoints = album.salepoints.select { |sp| sp.store_id == itunes_us.id }
    @is_itunes_upgrade    = upgrading_to_itunes_ww?
  end

  def handle_itunes_upgrade(salepoint)
    use_us_variable_pricing(salepoint) if is_itunes_ww_salepoint?(salepoint) && is_itunes_upgrade
  end

  def is_itunes_ww_salepoint?(salepoint)
    salepoint.store_id == itunes_ww.id
  end

  def upgrading_to_itunes_ww?
    itunes_ww_salepoints.present? && itunes_ww_salepoints.first.new_record? && itunes_us_salepoints.present?
  end

  def use_us_variable_pricing(salepoint)
    salepoint.variable_price       = itunes_us_salepoints.first.variable_price
    salepoint.track_variable_price = itunes_us_salepoints.first.track_variable_price
  end

  # For popup
  def non_tiktok_salepoints?
    @album.salepoints.where.not(
      salepoints: { store_id: SpecializedReleaseable::TIKTOK_RELEASE_STORES }
    ).limit(1).count.positive?
  end
end
