# frozen_string_literal: true

class VatFailureAnalyzeService
  attr_reader :start_date, :end_date

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def succeeded_calls_count
    @succeeded_calls_count ||= data.succeeded_calls_count
  end

  def failed_calls_count
    @failed_calls_count ||= data.failed_calls_count
  end

  def failure_handled_calls_count
    @failure_handled_calls_count ||= data.failure_handled_calls_count
  end

  def total_calls_count
    @total_calls_count ||= data.total_calls_count
  end

  def failure_handled_calls_ratio
    ratio_wrt_total(failure_handled_calls_count)
  end

  def failed_calls_ratio
    ratio_wrt_total(failed_calls_count)
  end

  private

  def data
    @data ||=
      PurchaseTaxInformation.where(
        tax_type: PurchaseTaxInformation::VAT_TAX,
        created_at: start_date.beginning_of_day..end_date.end_of_day
      ).select(fields_to_select).take
  end

  def fields_to_select
    [
      succeeded_calls_count_fetch_query,
      failure_handled_calls_count_fetch_query,
      failed_calls_count_fetch_query,
      total_calls_count_fetch_query
    ]
  end

  def succeeded_calls_count_fetch_query
    "COALESCE(
      SUM(
        CASE
        WHEN error_code IS NULL
          THEN 1
          ELSE 0
        END
      ), 0) AS succeeded_calls_count"
  end

  # Details about failsafe method used will be stored in the note column and
  # original error code will be stored in error_code column.
  # But in case we used cache to calculate VAT, the note column will be NULL and
  # error_code will be 'Vat Service Down - Used cache to calculate VAT'.
  def failure_handled_calls_count_fetch_query
    "COALESCE(
      SUM(
        CASE
        WHEN (error_code = '#{TcVat::FailSafe::Calculator::VAT_SERVICE_ERROR}')
            OR (error_code IS NOT NULL AND note IS NOT NULL)
          THEN 1
          ELSE 0
        END
      ), 0) AS failure_handled_calls_count"
  end

  def failed_calls_count_fetch_query
    "COALESCE(
      SUM(
        CASE
        WHEN error_code != '#{TcVat::FailSafe::Calculator::VAT_SERVICE_ERROR}'
             AND error_code IS NOT NULL AND note IS NULL
          THEN 1
          ELSE 0
        END
      ), 0) AS failed_calls_count"
  end

  def total_calls_count_fetch_query
    "COUNT(*) AS total_calls_count"
  end

  def ratio_wrt_total(count)
    return 0 if total_calls_count.zero?

    count.fdiv(total_calls_count).round(4)
  end
end
