# frozen_string_literal: true

class Transfers::ApproveService
  include ActiveModel::Model

  ALLOWED_ACTIONS = %w[approve reject]
  ALLOWED_PROVIDERS = %w[paypal payoneer]

  attr_accessor :action, :transfer_ids, :person, :provider, :referrer_url

  validates :action, :transfer_ids, :person, :provider, presence: { strict: true }
  validates :provider, inclusion: { in: ALLOWED_PROVIDERS }, strict: true
  validates :action, inclusion: { in: ALLOWED_ACTIONS }, strict: true

  def initialize(options)
    @action = options[:action]
    @transfer_ids = options[:transfer_ids]
    @person = options[:person]
    @provider = options[:provider]
    @referrer_url = options[:referrer_url]
  end

  def self.call(options)
    new(options).send(:call)
  end

  private

  def call
    valid?
    send(action)
  end

  ALLOWED_ACTIONS.each do |axn|
    define_method axn do
      errors = send("#{axn}_#{provider}_transfer")
      { success: errors.blank?, errors: errors }
    end
  end

  # TODO: remove PaypalTransfer.do_mass_pay, replace with Transfers::ApproveService.call
  def approve_paypal_transfer
    paypal_transfers = PaypalTransfer.find(transfer_ids)
    if FeatureFlipper.show_feature?(:new_paypal_mass_pay_handler)
      Admin::PaypalMassPayHandler.call(person, paypal_transfers)
    else
      PaypalTransfer.do_mass_pay(paypal_transfers, person.id)
    end
  end

  def reject_paypal_transfer
    paypal_transfers = PaypalTransfer.find(transfer_ids)
    errors = []
    paypal_transfers.each do |transfer|
      errors << transfer.id unless transfer.cancel
    end
    errors
  end

  def approve_payoneer_transfer
    Payoneer::BatchActionsService.send_actions(payoneer_approval_params)
  end
  alias_method :reject_payoneer_transfer, :approve_payoneer_transfer

  def payoneer_approval_params
    { action: action, transfer_ids: transfer_ids, person: person, referrer_url: referrer_url }
  end
end
