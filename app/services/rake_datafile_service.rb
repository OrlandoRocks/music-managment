# frozen_string_literal: true

class RakeDatafileService
  RAKE_S3_BUCKET = "rake-data"
  UPLOAD_ACTION = "upload"
  DOWNLOAD_ACTION = "download"

  attr_accessor :temporary,
                :s3_instance,
                :s3_bucket,
                :file_path,
                :local_file,
                :file_key,
                :action,
                :action_status,
                :overwrite,
                :binary_mode

  def initialize(args = {})
    @s3_instance        = AWS::S3.new
    @s3_bucket          = @s3_instance.buckets[args.fetch(:bucket_name, RAKE_S3_BUCKET)]
    @file_path          = args[:file_path]
    @action             = args[:action]
    @file_key           = args[:file_key]
    @overwrite          = args[:overwrite]
    @environment_folder = args[:environment_folder]
    @temporary          = args[:temporary]
    @binary_mode        = args[:binary_mode]
    send(action)
  end

  def self.download(remote_file_key, download_opts = {})
    service = new(
      file_key: remote_file_key,
      action: DOWNLOAD_ACTION,
      **download_opts
    )
  end

  def self.upload(local_file, upload_opts = {})
    object = new(
      file_key: File.basename(local_file),
      file_path: local_file,
      action: UPLOAD_ACTION,
      overwrite: upload_opts[:overwrite],
      environment_folder: upload_opts[:environment_folder]
    )
  end

  def self.csv(remote_file_key, options = { headers: :first_row })
    download_options = { environment_folder: options.delete(:environment_folder) }
    CSV.open(
      download(remote_file_key, download_options).local_file,
      **options
    )
  end

  def upload
    remote_file = get_remote_file
    return upload_overwrite(remote_file) if file_already_exists?

    remote_file.write(file: @file_path)
    @action_status = true
    remote_file
  end

  def upload_overwrite(remote_file)
    print "File Already Exists. Do you want to overwrite? [y/N]: " if @overwrite.blank?
    @overwrite ||= STDIN.gets.chomp.strip
    if @overwrite.casecmp?("y")
      remote_file.write(file: @file_path)
      @action_status = remote_file.exists?
    else
      puts "File Already Exists. Upload terminated.."
      @action_status = false
    end
  end

  def download
    @local_file = generate_local_file
    get_remote_file.read do |chunk|
      @local_file.write(chunk)
    end
    @local_file.tap(&:rewind)
  end

  private

  def file_already_exists?
    get_remote_file.exists?
  end

  def generate_local_file
    filename = Rails.root.join("tmp", environment_folder, @file_key)
    FileUtils.mkdir_p(File.dirname(filename))
    return File.open(filename, binary_mode ? "wb" : "w") unless @temporary

    local_file_extname = File.extname(@file_key)
    local_file_basename = File.basename(@file_key, local_file_extname)
    Tempfile.new([local_file_basename, local_file_extname], binmode: binary_mode)
  end

  def get_remote_file
    @s3_bucket.objects[
      File.join(environment_folder, @file_key)
    ]
  end

  def environment_folder
    @environment_folder ||=
      if ENV["RAILS_ENV"] == "test"
        "development"
      else
        ENV["RAILS_ENV"]
      end
  end
end
