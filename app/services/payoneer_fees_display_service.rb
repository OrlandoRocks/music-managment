# frozen_string_literal: true

class PayoneerFeesDisplayService
  include CurrencyHelper
  include RegionHelper

  attr_reader :person, :domain

  def initialize(person)
    @person = person
    @domain = person.country_domain
  end

  def ach_fee_display
    amount = PayoneerAchFee.find_by(target_country_code: domain).transfer_fees * 100
    ach_amount_cents_compiler(amount)
  end

  def paypal_fee_display
    payout_type_compiler(programs(PayoutProgram::PAYPAL))
  end

  def cc_fee_display
    payout_type_compiler(programs(PayoutProgram::PAYONEER))
  end

  def paper_check_fee_display
    payout_type_compiler(programs(PayoutProgram::CHECK))
  end

  def ach_min_withdraw_display
    amount = PayoneerAchFee.find_by(target_country_code: domain).transfer_fees * 100
    ach_amount_cents_compiler(amount + 100)
  end

  def cc_min_withdraw_display
    amount_cents_compiler(programs(PayoutProgram::PAYONEER))
  end

  def paper_check_min_withdraw_display
    amount_cents_compiler(programs(PayoutProgram::CHECK))
  end

  def payout_type_compiler(programs)
    display = ""
    programs.each do |program|
      display += localized_fee_display(program) + "*" if display_localized_fee?
      display += "<br>" if display_localized_fee?
      display += fee_display(program)
      display += "<br>"
    end

    display.html_safe
  end

  def amount_cents_compiler(programs)
    display = ""
    programs.each do |program|
      money = Money.new(program.amount_cents + 100, site_currency)
      balance_formatted_money = balance_to_currency(
        money,
        {
          currency: user_currency,
          iso_code: domain
        }
      ) if display_localized_fee?
      formatted_money = format_to_currency(money)

      display += amount_cents_display(balance_formatted_money, user_currency) + "*" if display_localized_fee?
      display += "<br>" if display_localized_fee?
      display += amount_cents_display(formatted_money, site_currency)
      display += "<br>"
    end

    display.html_safe
  end

  def ach_amount_cents_compiler(amount)
    balance_formatted_money = balance_to_currency(
      Money.new(amount, site_currency),
      { currency: user_currency, iso_code: domain }
    ) if display_localized_fee?

    display = ""
    display += amount_cents_display(balance_formatted_money, user_currency) + "*" if display_localized_fee?
    display += "<br>" if display_localized_fee?
    display += amount_cents_display(format_to_currency(Money.new(amount, site_currency)), site_currency)
    display += "<br>"

    display.html_safe
  end

  def fee_display(program)
    return per_transaction_display(
      format_to_currency(Money.new(program.amount_cents, site_currency)),
      site_currency
    ) if program.amount_cents?

    percentage_display(
      program.percentage,
      format_to_currency(Money.new(program.max_fee, site_currency))
    ) if program.percentage?
  end

  def localized_fee_display(program)
    return per_transaction_display(
      balance_to_currency(
        Money.new(program.amount_cents, site_currency),
        { currency: user_currency, iso_code: domain }
      ),
      user_currency
    ) if program.amount_cents?

    percentage_display(
      program.percentage,
      balance_to_currency(
        Money.new(program.max_fee, site_currency),
        { currency: user_currency, iso_code: domain }
      )
    ) if program.percentage?
  end

  def per_transaction_display(amount, currency)
    I18n.with_locale(domain) {
      I18n.t(
        "payoneer_fees.index.per_transaction_amount",
        amount: amount,
        currency: currency
      )
    }
  end

  def percentage_display(percentage, max_fee)
    I18n.with_locale(domain) {
      I18n.t(
        "payoneer_fees.index.percent_up_to",
        money: max_fee,
        percentage: percentage * 100
      )
    }
  end

  def amount_cents_display(amount, currency)
    "#{amount} #{currency}"
  end

  def display_localized_fee?
    return @display_localized_fee unless @display_localized_fee.nil?

    @display_localized_fee = person.country_website.non_native_currency_site?
  end

  def user_currency
    return @user_currency unless @user_currency.nil?

    @user_currency = person.user_currency
  end

  def site_currency
    return @site_currency unless @site_currency.nil?

    @site_currency = person.site_currency
  end

  def programs(payout_type)
    return @programs.where(payout_type: payout_type) unless @programs.nil?

    # we will remove the inside_region where statement when we get more sophisticated
    @programs = PayoutProgram
                .where(currency: person.site_currency)
                .where(inside_region: true)
    @programs.where(payout_type: payout_type)
  end
end
