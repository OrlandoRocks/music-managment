# frozen_string_literal: true

class TaxReports::RevenueReportService
  def initialize(person_id, year)
    @person_id = person_id
    @year      = DateTime.strptime(String(year), "%Y").all_year
  end

  def generate_revenue_stats
    append_distribution_meta({})
      .then { |chain| append_publishing_meta(chain) }
  end

  private

  def append_publishing_meta(chain)
    chain.merge(
      {
        publishing: {
          earnings: revenue_stream_grouped_earnings.dig(publishing_revenue_stream_id, :earnings),
          withholdings: revenue_stream_grouped_earnings.dig(publishing_revenue_stream_id, :withholdings)
        }
      }
    )
  end

  def append_distribution_meta(chain)
    earnings = [
      revenue_stream_grouped_earnings.dig(distribution_revenue_stream_id, :earnings),
      revenue_stream_grouped_earnings.dig(rollover_revenue_stream_id, :earnings)
    ].sum

    withholdings = [
      revenue_stream_grouped_earnings.dig(distribution_revenue_stream_id, :withholdings),
      revenue_stream_grouped_earnings.dig(rollover_revenue_stream_id, :withholdings)
    ].sum

    chain.merge(
      {
        distribution: {
          earnings: earnings,
          withholdings: withholdings,
        }
      }
    )
  end

  def calculate_person_transaction_based_earnings(transactions, mapping)
    mapping_transaction_ids = transactions
                              .where(target_type: mapping.person_transaction_type)

    earnings = mapping_transaction_ids.sum(:credit)
    withholdings = IRSTaxWithholding
                   .where(withheld_from_person_transaction_id: mapping_transaction_ids.pluck("person_transactions.id"))
                   .sum(:withheld_amount)

    {
      earnings: earnings,
      withholdings: withholdings,
      revenue_stream_id: mapping.revenue_stream_id
    }
  end

  def calculate_balance_adjustment_based_earnings(transactions, mapping)
    mapping_transaction_ids = transactions
                              .where(target_type: "BalanceAdjustment")
                              .joins(<<-SQL)
                                LEFT JOIN balance_adjustments
                                  ON person_transactions.target_id = balance_adjustments.id
                              SQL
                              .where(balance_adjustments: { category: mapping.balance_adjustment_category })

    earnings = mapping_transaction_ids.sum("person_transactions.credit")
    withholdings = IRSTaxWithholding
                   .where(withheld_from_person_transaction_id: mapping_transaction_ids.pluck("person_transactions.id"))
                   .sum(:withheld_amount)

    { earnings: earnings, withholdings: withholdings, revenue_stream_id: mapping.revenue_stream_id }
  end

  def calculate_earnings_for_mapping(transactions, mapping)
    if mapping.balance_adjustment_category.present?
      calculate_balance_adjustment_based_earnings(transactions, mapping)
    else
      calculate_person_transaction_based_earnings(transactions, mapping)
    end
  end

  def revenue_stream_grouped_earnings
    return @revenue_stream_grouped_earnings if @revenue_stream_grouped_earnings

    transactions = PersonTransaction.where(person_id: @person_id, created_at: @year)
    @revenue_stream_grouped_earnings =
      RevenueStreamsTransactionMapping
      .all
      .map { |mapping| calculate_earnings_for_mapping(transactions, mapping) }
      .group_by { |calculated_earnings| calculated_earnings[:revenue_stream_id] }
      .transform_values do |earnings|
        { earnings: earnings.pluck(:earnings).sum, withholdings: earnings.pluck(:withholdings).sum }
      end
  end

  def publishing_revenue_stream_id
    @publishing_revenue_stream_id ||= RevenueStream.publishing.id
  end

  def distribution_revenue_stream_id
    @distribution_revenue_stream_id ||= RevenueStream.distribution.id
  end

  def rollover_revenue_stream_id
    @rollover_revenue_stream_id ||= RevenueStream.taxable_earnings_rollover.id
  end
end
