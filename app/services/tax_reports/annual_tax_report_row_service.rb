# frozen_string_literal: true

class TaxReports::AnnualTaxReportRowService
  ACCEPTED_FORM_TYPES = ["W9 - Individual", "W9 - Entity"].freeze
  EARNINGS_MIN_THRESHOLD = 5.0
  CANADIAN_ADDRESS_TYPE = "C"
  OTHER_ADDRESS_TYPE = "O"
  EMPTY_COLUMN = ""
  DEFAULT_SSN = "000000000"
  REDIS_DATA_KEY = :annual_1099_report_gen_thread
  REDIS_RECORDS_KEY = :annual_1099_report_processed_users
  REPORT_HEADERS = [
    "RcpTIN",
    "Last Name/Company",
    "First Name",
    "Name Line 2",
    "Address Type",
    "Address Deliv/Street",
    "Address Apt/Suite",
    "City",
    "State",
    "Zip",
    "Country",
    "Rcp Account",
    "Rcp Email",
    "2nd TIN Notice",
    "FATCA Checkbox",
    "Box 1 Amount",
    "Box 2 Amount",
    "Box 3 Amount",
    "Box 4 Amount",
    "Box 5 Amount",
    "Box 6 Amount",
    "Box 7 Amount",
    "Box 8 Amount",
    "Box 9 Amount",
    "Box 10 Amount",
    "Box 11 Amount",
    "Box 12 Amount",
    "Box 13 Amount",
    "Box 14 Amount",
    "Box 15 Amount",
    "Box 16 Amount",
    "Box 17 Amount",
    "Opt Rcp Text Line 1",
    "Opt Rcp Text Line 2",
    "Form Category",
    "Form Source",
    "Batch ID",
    "Tax State",
    "Currency"
  ]

  attr_accessor :kind, :record, :person, :year

  def initialize(kind:, data:, year:)
    @year = year
    @kind = kind
    @record = person_record? ? Person.find_by(id: data) : data.with_indifferent_access
    @person = person_record? ? @record : find_user_by_payee_id
  end

  def generate_user_csv_row
    return if record.blank?
    return if person.blank?
    return unless valid_record?

    reportable_amount(person.id)
      .filter { |_, revenue_data| revenue_data[:earnings] > EARNINGS_MIN_THRESHOLD }
      .map { |_, revenue_data| generate_output_row(revenue_data[:earnings], revenue_data[:withholdings]) }
      .each { |processed_record| add_to_cache(processed_record) }
  end

  private

  def valid_record?
    return true if person_record?
    return true if ACCEPTED_FORM_TYPES.include?(record["Tax Form Type"]&.strip)

    false
  end

  def find_user_by_payee_id
    @find_user_by_payee_id ||= PayoutProvider
                               .find_by(client_payee_id: record["Payee ID"])&.person
  end

  def append_taxpayer_id_number(chain)
    if person_record?
      chain.append(DEFAULT_SSN)
    else
      chain.append(record["Taxpayer ID Number"].presence || DEFAULT_SSN)
    end
  end

  def append_person_name(chain)
    if person_record?
      chain.append(record.name)
    else
      chain.append(record["Name"])
    end
  end

  def append_empty_column(chain, count = 1)
    chain + [EMPTY_COLUMN] * count
  end

  def append_address_type(chain)
    if person_record?
      append_person_address_type(chain)
    else
      append_record_address_type(chain)
    end
  end

  def append_person_address_type(chain)
    if Country::UNITED_STATES_AND_TERRITORIES.include?(record[:country])
      append_empty_column(chain)
    elsif record[:country].eql?("CA")
      chain.append(CANADIAN_ADDRESS_TYPE)
    else
      chain.append(OTHER_ADDRESS_TYPE)
    end
  end

  def append_record_address_type(chain)
    if Country::UNITED_STATES_AND_TERRITORIES.include?(record["Country"])
      append_empty_column(chain)
    elsif record["Country"].eql?("CA")
      chain.append(CANADIAN_ADDRESS_TYPE)
    else
      chain.append(OTHER_ADDRESS_TYPE)
    end
  end

  def append_address(chain)
    if person_record?
      chain.append(String(record.address1).concat(String(record.address2)))
    else
      chain.append(record["Address"])
    end
  end

  def append_city(chain)
    if person_record?
      chain.append(record.city)
    else
      chain.append(record["City"])
    end
  end

  def append_state(chain)
    if person_record?
      chain.append(record.state)
    else
      chain.append(record["State"])
    end
  end

  def append_postal_code(chain)
    if person_record?
      chain.append(record.postal_code)
    else
      chain.append(record["Zip"])
    end
  end

  def append_country(chain)
    if person_record?
      chain.append(record.country)
    else
      chain.append(Country.find_by(iso_code: record["Country"])&.name)
    end
  end

  def append_person_id(chain)
    if person_record?
      chain.append(record.id)
    else
      chain.append(person.id)
    end
  end

  def append_person_email(chain)
    if person_record?
      chain.append(record.email)
    else
      chain.append(person.email)
    end
  end

  def append_currency(chain)
    if person_record?
      chain.append(String(record.country_website&.currency))
    else
      chain.append(String(person.country_website&.currency))
    end
  end

  def append_address_meta(partial_output)
    partial_output
      .then { |chain| append_address_type(chain) }
      .then { |chain| append_address(chain) }
      .then { |chain| append_empty_column(chain) }
      .then { |chain| append_city(chain) }
      .then { |chain| append_state(chain) }
      .then { |chain| append_postal_code(chain) }
      .then { |chain| append_country(chain) }
  end

  def generate_output_row(earnings, withholdings)
    append_taxpayer_id_number([])
      .then { |chain| append_person_name(chain) }
      .then { |chain| append_empty_column(chain, 2) }
      .then { |chain| append_address_meta(chain) }
      .then { |chain| append_person_id(chain) }
      .then { |chain| append_person_email(chain) }
      .then { |chain| append_empty_column(chain, 3) }
      .then { |chain| chain.append(earnings) }
      .then { |chain| append_empty_column(chain) }
      .then { |chain| chain.append(withholdings) }
      .then { |chain| append_empty_column(chain, 19) }
      .then { |chain| append_currency(chain) }
  end

  def reportable_amount(person_id)
    TaxReports::RevenueReportService
      .new(person_id, year)
      .generate_revenue_stats
  end

  def person_record?
    kind.eql?(:person_id)
  end

  def add_to_cache(value)
    Redis.new(url: ENV["REDIS_URL"], timeout: 20)
         .tap { |redis_conn| redis_conn.sadd(REDIS_DATA_KEY, CSV.generate_line(value)) }
         .tap { |redis_conn| redis_conn.sadd(REDIS_RECORDS_KEY, value[11]) }
  end
end
