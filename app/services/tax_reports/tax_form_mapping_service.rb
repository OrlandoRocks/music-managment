class TaxReports::TaxFormMappingService
  attr_accessor :kind, :mapping_definition, :person_id

  def initialize(person_id, kind: :default, mapping_definition: [])
    @kind               = kind
    @person_id          = person_id
    @mapping_definition = mapping_definition
  end

  def self.call!(person_id, kind: :default, mapping_definition: [])
    new(
      person_id,
      kind: kind,
      mapping_definition: mapping_definition
    ).call!
  end

  def call!
    return :cannot_override_usermap if default_mapping? && user_mapped?
    return :no_such_person          if person.blank?

    map_tax_forms_to_revenue_streams!
  end

  private

  def person
    @person ||=
      Person.find_by(id: person_id)
  end

  def map_tax_forms_to_revenue_streams!
    definitions.map do |definition|
      tax_form_revenue_stream =
        TaxFormRevenueStream.find_or_initialize_by(
          revenue_stream: definition[:revenue_stream],
          person_id: person_id,
        )

      tax_form_revenue_stream.user_mapped_at = user_mapped_at
      tax_form_revenue_stream.tax_form       = definition[:tax_form]
      tax_form_revenue_stream.save!
    end

    case kind
    when :user then :user_mapped_successfully
    when :default then :default_mapped_successfully
    end
  end

  def user_mapped_at
    return unless user_mapping?

    Time.now
  end

  def definitions
    mapping_definition.presence || default_definitions
  end

  def user_mapped?
    TaxForm
      .where(person_id: person_id)
      .revenue_stream_mapping_by(:user)
      .exists?
  end

  def default_definitions
    @default_definitions ||=
      [
        {
          revenue_stream: RevenueStream.distribution,
          tax_form: person_default_tax_form
        },
        {
          revenue_stream: RevenueStream.publishing,
          tax_form: person_default_tax_form
        }
      ]
  end

  def person_default_tax_form
    @person_default_tax_form ||=
      TaxForm
      .where(person_id: person_id)
      .current
      .w9
      .find_by(payout_provider_config_id: PayoutProviderConfig.default_us_tax_program_configs.ids)
  end

  def default_mapping?
    kind == :default
  end

  def user_mapping?
    kind == :user
  end
end
