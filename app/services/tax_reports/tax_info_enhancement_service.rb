# frozen_string_literal: true

class TaxReports::TaxInfoEnhancementService
  def finalize_csv
    write_to_local_file
      .then { |localfile| write_to_s3(localfile) }
      .then { cleanup_redis }
  end

  private

  def cleanup_redis
    Redis.new(url: ENV["REDIS_URL"], timeout: 20)
         .tap { |redis_conn| redis_conn.del(TaxReports::AnnualTaxReportRowService::REDIS_DATA_KEY) }
         .tap { |redis_conn| redis_conn.del(TaxReports::AnnualTaxReportRowService::REDIS_RECORDS_KEY) }
  end

  def write_to_s3(local_file)
    RakeDatafileService.upload(local_file.path, overwrite: "y")
  end

  def fetch_processed_records
    Redis.new(url: ENV["REDIS_URL"], timeout: 20)
         .tap { |redis_conn| redis_conn.smembers(TaxReports::AnnualTaxReportRowService::REDIS_DATA_KEY) }
  end

  def write_to_local_file
    Tempfile
      .new([report_filename, ".csv"], binmode: "w")
      .tap { |tmpfile| tmpfile.puts(generate_headers) }
      .tap { |tmpfile| fetch_processed_records.map { |csv_record| tmpfile.puts(csv_record) } }
      .tap(&:rewind)
  end

  def generate_headers
    CSV.generate_line(TaxReports::AnnualTaxReportRowService::REPORT_HEADERS)
  end

  def report_filename
    format(
      "1099_FINAL_REPORT_%{generated_env}_%{generated_at}",
      {
        generated_at: DateTime.current.strftime("%m_%d_%Y"),
        generated_env: Rails.env
      }
    )
  end
end
