# frozen_string_literal: true

# Validate a creative's Spotify External Service ID.
# Valid if Spotify's name matches our name (after name is parsed).
#
# Used:
# Client-side, on artist modal form input:
#  - Filters 'search results' and 'manual input' response
#
# Server-side, synchronous, during album POST:
#  - Persisted artist name is compared with Apple record, which is now fetched by identifier
#    that was on the submitted Creative hash
class Spotify::ArtistIdValidator
  class ArtistIdValidatorError < StandardError; end

  INVALID_SPOTIFY_ARTIST_NAME = "Invalid Spotify artist name."

  def self.valid?(options)
    new(options).valid?
  end

  attr_reader :id, :name, :spotify_client, :spotify_name

  def initialize(options)
    @id             = options[:id]
    @name           = options[:name]
    @spotify_name   = options[:spotify_name]
    @spotify_client = Spotify::ApiClient.new
    validate_configuration
  end

  def valid?
    @spotify_name ||= set_spotify_name
    return false unless @spotify_name

    artist_name   = ArtistNameUtils.new(name)
    response_name = ArtistNameUtils.new(spotify_name)

    if artist_name.normalize.present? && response_name.normalize.present?
      artist_name.parse_for_comparison == response_name.parse_for_comparison
    else
      # Airbrake.notify(INVALID_SPOTIFY_ARTIST_NAME, { artist_name: artist_name,
      #                                                spotify_artist_name: response_name })
      false
    end
  end

  private

  def validate_configuration
    return unless id.nil? && spotify_name.nil?

    raise ArtistIdValidatorError, "An id or spotify_name is required for name comparison."
  end

  def set_spotify_name
    response = spotify_client.get_artist_by_id(id)

    return unless response.success?

    JSON.parse(response.body)["name"]
  end
end
