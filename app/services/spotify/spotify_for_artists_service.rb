# frozen_string_literal: true

class Spotify::SpotifyForArtistsService
  SPOTIFY_AUTH_ENDPOINT = "https://accounts.spotify.com/authorize"
  SPOTIFY_TOKEN_ENPOINT = "https://accounts.spotify.com/api/token"
  SPOTIFY_FOR_ARTISTS_ENDPOINT = "https://creator.wg.spotify.com/s4a-onboarding/v0/external/request"
  SPOTIFY_FOR_ARTISTS_USER_URL = "https://artists.spotify.com/c/distributor/invite/invalid"

  attr_reader :path_with_params,
              :encoded_params,
              :http,
              :redirect_path,
              :encodeded_scopes,
              :state,
              :uri,
              :url,
              :options,
              :auth_token,
              :error,
              :current_user,
              :code,
              :error,
              :artist_uri,
              :album_uris

  def self.authenticate_spotify_user
    new.auth_path_with_params
  end

  def self.request_spotify_for_artists_access(options)
    new(options).onboard_spotify_for_artists
  end

  def initialize(params = {})
    @params = params
    params.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end if params
  end

  def request_auth_token
    uri = URI.parse(SPOTIFY_TOKEN_ENPOINT)
    http = Net::HTTP.new(uri.host, uri.port)
    http.set_debug_output($stdout)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri)
    request.basic_auth(SPOTIFY_API[:client_id], SPOTIFY_API[:client_secret])
    request.set_form_data(token_request_body)
    http.request(request)
  end

  def request_spotify_for_artists_access
    uri = URI.parse(SPOTIFY_FOR_ARTISTS_ENDPOINT)
    http = Net::HTTP.new(uri.host, uri.port)
    http.set_debug_output($stdout)
    http.use_ssl = true
    body = spotify_for_artist_body
    headers = spotify_for_artists_header
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.body = body
    http.request(request)
  end

  def spotify_for_artist_body
    {
      artistUri: "spotify:artist:" + artist_uri,
      albumUris: album_uris,
      clientSecret: ENV["SPOTIFY_FOR_ARTIST_SECRET"]
    }.to_json
  end

  def spotify_for_artists_header
    {
      "Authorization" => "Bearer " + auth_token,
      "Content-Type" => "application/json"
    }
  end

  def success_response
    @response["url"]
  end

  def log_error
    Airbrake.notify(
      "onboard_spotify_for_artists failure",
      @response.merge(@params).merge(current_user: current_user)
    )
  end

  def error_response
    log_error
    SPOTIFY_FOR_ARTISTS_USER_URL
  end

  def log_response_message
    Airbrake.notify(
      "onboard_spotify_for_artists message response for user #{current_user.id}",
      @response.merge(@params).merge(current_user: current_user)
    )
  end

  def success?
    @response["url"].present?
  end

  def onboard_spotify_for_artists
    @response ||= JSON.parse(request_spotify_for_artists_access.body)
    log_response_message if @response["message"]
    if success?
      success_response
    else
      error_response
    end
  end

  def uri
    @uri ||= URI.parse(url)
  end

  def spotify_artist_uri
    @spotify_artist_uri ||= JSON.parse(request_user_profile.body)["uri"].split(":").last
  end

  def auth_token
    @auth_token ||= JSON.parse(request_auth_token.body)["access_token"]
  end

  def redirect_path
    @redirect_path ||= URI.parse(ENV["SPOTIFY_REDIRECT_URL"])
  end

  def auth_path_with_params
    [SPOTIFY_AUTH_ENDPOINT, encoded_params].join("?")
  end

  def encodeded_scopes
    @encodeded_scopes ||= URI.escape("user-read-private user-read-email", Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
  end

  def encoded_params
    @encoded_params ||= URI.encode_www_form(query_params)
  end

  def token_request_body
    {
      "grant_type" => "authorization_code",
      "code" => code,
      "redirect_uri" => redirect_path
    }
  end

  def query_params
    {
      client_id: SPOTIFY_API[:client_id],
      redirect_uri: redirect_path,
      scope: encodeded_scopes,
      response_type: "code"
    }
  end

  def album_uris
    albums = ServiceForArtists::QueryBuilder.new(current_user.id, "spotify")
                                            .albums_with_service_ids.where(name: artist_name)
    @album_uris ||= albums.map { |album| "spotify:album:" + album.identifier }
  end

  def artist_name
    @artist_name ||= ExternalServiceId.where(identifier: artist_uri).first.linkable.artist.try(:name)
  end
end
