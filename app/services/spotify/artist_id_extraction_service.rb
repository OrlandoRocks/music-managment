class Spotify::ArtistIdExtractionService
  SPOTIFY_IDENTIFIER = /^https:\/\/open.spotify.com\/artist\/(\w+)(?:\??|(?:[?]\w+=\w+)(?:&\w+=\w+)*)*$|^spotify:artist:(\w+)$/

  def self.extract_artist_id(artist_id_url)
    new(artist_id_url).extract_artist_id
  end

  def initialize(artist_id_url)
    @identifier = artist_id_url || ""
  end

  def extract_artist_id
    identifier[SPOTIFY_IDENTIFIER, 1] || identifier[SPOTIFY_IDENTIFIER, 2]
  end

  private

  attr_reader :identifier
end
