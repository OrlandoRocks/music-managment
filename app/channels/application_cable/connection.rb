module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :cable_auth_token

    def connect
      authenticate_user
    end

    private

    def authenticate_user
      return reject_unauthorized_connection unless request.params.keys.sort == %w[cable_auth_token email]

      person = Person.find_by(email: request.params["email"])

      if person.blank? || !CableAuthService.validate_token(person, request.params["cable_auth_token"])
        return reject_unauthorized_connection
      end

      @cable_auth_token = person.cable_auth_token&.token
    end
  end
end
