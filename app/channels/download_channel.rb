# frozen_string_literal: true

class DownloadChannel < ApplicationCable::Channel
  def subscribed
    stream_from "downloads-channel-#{params[:cable_auth_token]}"
  end

  def fetch_report(data)
    report_csv = PayoutTransferReportService.generate_report_csv(
      data.dig("params", "payout_provider"),
      report_query_params(data["params"])
    )

    stream_report_file(report_csv, data)
  end

  def unsubscribed; end

  private

  def report_query_params(raw_params)
    case raw_params["payout_provider"]
    when "paypal", "check"
      paypal_report_query_params(raw_params)
    when "payoneer"
      payoneer_report_query_params(raw_params)
    else
      {}
    end
  end

  def payoneer_report_query_params(raw_params)
    raw_params.with_indifferent_access.slice(
      :tunecore_status,
      :provider_status,
      :filter_type,
      :filter_amount,
      :withdrawal_method,
      :person_name,
      :currency,
      :start_date,
      :end_date,
      :rollback,
      :auto_approval_eligible
    )
  end

  def paypal_report_query_params(raw_params)
    raw_params.with_indifferent_access.slice(
      :keyword,
      :type,
      :threshold_status,
      :status
    )
  end

  def stream_report_file(report_csv, data)
    ActionCable.server.broadcast(
      "downloads-channel-#{data['cable_auth_token']}",
      {
        file: {
          name: generate_report_file_name(data),
          type: "text/csv",
          size: report_csv.size,
          encoding: "utf-8",
          content: report_csv.read
        }
      }
    )
    report_csv.unlink
  end

  def generate_report_file_name(data)
    params_string = data["params"]
    params_string.reject! do |_query_key, query_value|
      query_value.blank?
    end
    "payout_transfers_#{params_string.to_query}.csv"
  end
end
