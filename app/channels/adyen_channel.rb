# frozen_string_literal: true

class AdyenChannel < ApplicationCable::Channel
  def subscribed
    stream_from "adyen-channel-#{params[:cable_auth_token]}"
  end

  def unsubscribed; end
end
