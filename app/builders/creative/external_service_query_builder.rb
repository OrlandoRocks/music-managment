class Creative::ExternalServiceQueryBuilder
  include ArelTableMethods
  def self.build(creative)
    new(creative).build
  end

  def initialize(creative)
    @creative = creative
  end

  def build
    Album.joins(:creatives, external_service_join)
         .where(creatives: { id: @creative.id })
  end

  def external_service_join
    join_on = external_service_id_t.create_on(
      external_service_id_t[:linkable_id].eq(album_t[:id])
      .and(external_service_id_t[:linkable_type].eq("Album"))
    )
    album_t.create_join(
      external_service_id_t,
      join_on,
      Arel::Nodes::OuterJoin
    )
  end
end
