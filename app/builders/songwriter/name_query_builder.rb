class Songwriter::NameQueryBuilder
  include ArelTableMethods
  include ArelNamedMysqlFunctions

  def self.build(person_id)
    new.build(person_id)
  end

  def build(person_id)
    Artist.joins(creatives: { creative_song_roles: { song: :album } })
          .where(albums: { person_id: person_id }, creative_song_roles: { song_role_id: SongRole::SONGWRITER_ROLE_ID })
          .select(artist_t[:name]).distinct
  end
end
