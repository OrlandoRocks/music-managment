class CreativeExternalServiceId::SearchQueryBuilder
  attr_accessor :params
  attr_reader :initial_query, :search_text, :search_query

  def self.build(params)
    new.build_query(params)
  end

  def build_query(params)
    @params = params

    valid_params? ? results : initial_query
  end

  private

  def initial_query
    @initial_query ||= (params[:initial_query] || Creative.all).joins(:artist)
  end

  def search_text
    @search_text ||= params[:search_text].to_s.strip
  end

  def search_query
    @search_query ||= "artist_name"
  end

  def results
    send("#{search_query}_query")
  end

  def valid_params?
    @params[:search_text].present?
  end

  def artist_name_query
    initial_query.where("lower(artists.name) like :search_text", search_text: "%#{search_text.to_s.downcase}%")
  end
end
