class Album::SearchQueryBuilder
  DEFAULT_JOINS = <<-SQL.strip_heredoc.freeze
    LEFT JOIN creatives ON creatives.creativeable_id = albums.id AND creatives.creativeable_type = 'Album'
    LEFT JOIN artists ON artists.id = creatives.artist_id
    LEFT JOIN upcs tunecore_upc ON tunecore_upc.upcable_id = albums.id AND tunecore_upc.upcable_type = 'Album' AND tunecore_upc.inactive = 0 AND tunecore_upc.upc_type = 'tunecore'
    LEFT JOIN upcs optional_upc ON optional_upc.upcable_id = albums.id AND optional_upc.upcable_type = 'Album' AND optional_upc.inactive = 0 AND optional_upc.upc_type = 'optional'
    LEFT JOIN upcs physical_upc ON physical_upc.upcable_id = albums.id AND physical_upc.upcable_type = 'Album' AND physical_upc.inactive = 0 AND physical_upc.upc_type = 'physical'
  SQL

  SELECT_STATEMENT = [
    Album.arel_table[Arel.star],
    "if(optional_upc.id IS NOT NULL, optional_upc.number, tunecore_upc.number) as active_upc".freeze,
    "GROUP_CONCAT(DISTINCT artists.name) as artist_names".freeze
  ].freeze

  INCLUDES_STATEMENT = [
    :tunecore_upcs, :optional_upcs, { creatives: :artist }
  ].freeze

  def self.build(search_field, search_text)
    new(search_field, search_text).build
  end

  def initialize(search_field, search_text)
    @search_field = search_field.to_sym
    @search_text = search_text
  end

  def build
    query = query_parameters[@search_field]

    Album.preload(:artwork)
         .includes(INCLUDES_STATEMENT)
         .select(SELECT_STATEMENT)
         .joins(query[:joins] || "", DEFAULT_JOINS)
         .where(query[:conditions])
         .group(Album.arel_table[:id])
         .order(query[:order] || Album.arel_table[:name])
  end

  private

  def query_parameters
    {
      id: {
        conditions: { albums: { id: @search_text } }
      },
      upc: {
        conditions: { all_upcs: { number: @search_text } },
        joins: "INNER JOIN upcs all_upcs ON all_upcs.upcable_id = albums.id AND all_upcs.upcable_type = 'Album'"
      },
      isrc: {
        conditions: Song.arel_table[:tunecore_isrc].eq(@search_text).or(Song.arel_table[:optional_isrc].eq(@search_text)),
        joins: :songs
      },
      name: {
        conditions: case_insensitive_search(Album, :name, :name),
        order: "albums.name ASC"
      },
      label: {
        conditions: case_insensitive_search(Label, :name, :label),
        joins: :label
      },
      artist: {
        conditions: case_insensitive_search(Artist, :name, :artist)
      },
      apple_id: {
        conditions: { apple_identifier: @search_text }
      }
    }
  end

  def case_insensitive_search(model, column, search_type)
    return unless @search_field == search_type

    conditions = [
      "#{column} like _utf8 ? COLLATE utf8_general_ci",
      @search_text.to_s.delete("%").tr("*", "%").to_s
    ]
    ["#{model.to_s.tableize}.id IN (?)", model.where(conditions).pluck(:id)]
  end
end
