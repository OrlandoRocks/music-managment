class Album::CreativeQueryBuilder
  include ArelTableMethods

  def self.build(person)
    new(person).build
  end

  def initialize(person)
    @person = person
  end

  def build
    @person.creatives
           .primary
           .select(selects)
           .distinct(:artist_id)
           .joins(:artist, :external_service_ids)
           .order(artist_t[:name])
  end

  def selects
    [
      external_service_id_t[:identifier].as("artist_id"),
      artist_t[:name].as("artist_name"),
      creative_t[:creativeable_type],
      creative_t[:creativeable_id],
      creative_t[:id].as("id")
    ]
  end
end
