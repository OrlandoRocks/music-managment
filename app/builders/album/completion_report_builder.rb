class Album::CompletionReportBuilder
  def self.build(album_id, person_id)
    new(album_id, person_id).build
  end

  attr_reader :person_id, :album_id, :song_data_reports

  def initialize(album_id, person_id)
    @song_data_reports = SongData::CompletionReportBuilder.build_for_album(album_id, person_id)
    @person_id         = person_id
    @album_id          = album_id
  end

  def build
    Album::CompletionReport.new(person_id, album_id, song_data_reports)
  end
end
