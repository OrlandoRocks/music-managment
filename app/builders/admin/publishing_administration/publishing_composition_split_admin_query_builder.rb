class Admin::PublishingAdministration::PublishingCompositionSplitAdminQueryBuilder
  def self.build(options = {})
    new(options).build
  end

  def initialize(options = {})
    @options = options
  end

  def build
    select_sql = "publishing_composition_splits.*, publishing_compositions.name as composition_name, publishing_compositions.id as comp_id, publishing_compositions.state as
                  composition_state, people.id as account_id, people.name as account_name, albums.id as release_id,
                  COALESCE(optional.number, tc.number) as release_upc"

    join_terms = "inner join publishing_compositions on publishing_composition_splits.publishing_composition_id = publishing_compositions.id inner join songs on publishing_compositions.id = songs.publishing_composition_id inner join albums on songs.album_id = albums.id
                  inner join people on albums.person_id = people.id left outer join upcs optional on optional.upcable_id = albums.id and optional.inactive = false and optional.upc_type = 'optional' and optional.upcable_type = 'Album'
                  inner join upcs tc on tc.upcable_id = albums.id and tc.inactive = false and tc.upc_type = 'tunecore' and tc.upcable_type = 'Album' "

    case @options[:query]
    when "PublishingComposition ID"
      condition = ["publishing_compositions.id = ?", @options[:search]]
    when "PublishingComposition Name"
      join_terms += "inner join ( select * from publishing_compositions
                     where name = '#{@options[:search].delete('%').sub('*', '%')}' ) c on publishing_composition_splits.publishing_composition_id = c.id"
    when "Account ID"
      condition = ["people.id = ?", @options[:search]]
    when "Account Name"
      condition = ["people.name like ?", @options[:search].delete("%").sub("*", "%").to_s]
    when "Account Email"
      condition = ["people.email = ?", @options[:search]]
    when "Release ID"
      condition = ["albums.id = ?", @options[:search]]
    when "Release Name"
      condition = ["albums.name like ?", @options[:search].delete("%").sub("*", "%").to_s]
    when "Release UPC"
      join_terms += "inner join ( select * from upcs where number = '#{@options[:search]}' ) u on u.upcable_id = albums.id and u.upcable_type = 'Album'"
    when "Song ISRC"
      condition = ["songs.tunecore_isrc = ? or songs.optional_isrc = ?", @options[:search], @options[:search]]
    end

    splits = PublishingCompositionSplit.select(select_sql).joins(join_terms).where(condition).limit(10).order(@options[:order])

    if @options[:per_page] == "All"
      splits
    else
      splits.paginate(page: @options[:page], per_page: @options[:per_page])
    end
  end
end
