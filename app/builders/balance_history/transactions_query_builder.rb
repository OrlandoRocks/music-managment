class BalanceHistory::TransactionsQueryBuilder
  DEFAULT_START_DATE = Date.new(2006, 5, 18)

  def self.build(params)
    new(params).build
  end

  attr_reader :person,
              :start_date,
              :end_date,
              :transactions,
              :transaction_ids,
              :transaction_types

  def initialize(params)
    @person            = params[:person]
    @start_date        = to_date(params[:start_date]) || DEFAULT_START_DATE
    @end_date          = to_date(params[:end_date])   || Date.tomorrow
    @transaction_ids   = params.fetch(:txn_ids, {})
    @transaction_types = params.fetch(:transaction_types, []).select(&:present?).map(&:classify)
  end

  def build
    @transactions ||= person.person_transactions
                            .select(select_statement)
                            .joins(payout_transfer_join)
                            .between_dates(start_date, end_date)
                            .by_transaction_ids_and_target_type(transaction_ids, transaction_types)
                            .group(person_transaction_table[:id])
                            .order(person_transaction_table[:created_at].desc,
                                   person_transaction_table[:id].desc)
  end

  private

  def to_date(date_string)
    Date.parse(date_string)
  rescue ArgumentError, TypeError
    nil
  end

  def select_statement
    [
      person_transaction_table[:id],
      person_transaction_table[:created_at],
      person_transaction_table[:target_type],
      person_transaction_table[:target_id],
      person_transaction_table[:currency],
      person_transaction_table[:comment],
      person_transaction_table[:credit],
      person_transaction_table[:debit],
      person_transaction_table[:previous_balance],
      payout_transfer_table[:client_reference_id].as("uuid"),
      payout_transfer_table[:tunecore_status],
      payout_transfer_table[:provider_status],
    ]
  end

  def payout_transfer_join
    join_on = person_transaction_table.create_on(
      payout_transfer_table[:id].eq(person_transaction_table[:target_id])
      .and(person_transaction_table[:target_type].eq("PayoutTransfer"))
    )

    person_transaction_table.create_join(payout_transfer_table, join_on, Arel::Nodes::OuterJoin)
  end

  def person_transaction_table
    PersonTransaction.arel_table
  end

  def payout_transfer_table
    PayoutTransfer.arel_table
  end
end
