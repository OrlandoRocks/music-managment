class BatchManager::DeliveryBatchIncompleteQueryBuilder
  include ArelTableMethods
  include ArelNamedMysqlFunctions
  START_DATE = Date.parse("2018-10-23")

  def self.build
    new.build
  end

  def build
    DeliveryBatch.select(select_statement).joins(
      :delivery_batch_items,
      store: :store_delivery_config
    ).where(delivery_batch_t[:count].eq(delivery_batch_t[:max_batch_size]))
                 .where(
                   delivery_batches: {
                     batch_complete_sent_at: nil,
                     currently_closing: false
                   },
                   delivery_batch_items: {
                     status: "present"
                   }
                 ).where(
                   delivery_batch_t[:active].eq(false).and(
                     delivery_batch_t[:created_at].gt(START_DATE)
                   ).or(
                     delivery_batch_t[:active].eq(true).and(
                       delivery_batch_item_t[:updated_at].gt(1.day.ago)
                     )
                   )
                 ).group(
                   delivery_batch_t[:id]
                 ).order(delivery_batch_t[:batch_id].desc)
  end

  private

  def select_statement
    [
      delivery_batch_t[:id],
      delivery_batch_t[:batch_id],
      store_t[:name].as("store_name"),
      store_delivery_config_t[:transfer_type],
      delivery_batch_t[:processed_count].as("completed_items"),
      delivery_batch_t[:created_at],
      delivery_batch_t[:max_batch_size]
    ]
  end
end
