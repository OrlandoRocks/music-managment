class BatchManager::DeliveryBatchInProgressQueryBuilder
  include ArelTableMethods
  START_DATE = Date.parse("2018-10-23")

  def self.build
    new.build
  end

  def build
    DeliveryBatch.includes(:store).where(delivery_batch_t[:created_at].gt(START_DATE)).where(
      delivery_batch_t[:processed_count].lt(delivery_batch_t[:max_batch_size]).or(
        delivery_batch_t[:active].eq(true)
      ).or(
        delivery_batch_t[:batch_complete_sent_at].eq(nil)
      )
    )
  end
end
