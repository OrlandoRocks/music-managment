class SongData::CompletionReportBuilder
  def self.build_for_album(album_id, person_id)
    Song.where(album_id: album_id).map do |song|
      song_data = SongDataBuilder.build(person_id, song)
      build(song_data)
    end
  end

  def self.build(song_data)
    SongData::CompletionReport.new(song_data)
  end
end
