class MyArtist::StoreReleaseQueryBuilder
  attr_reader :person_id, :artist_id, :service_name

  def self.build(person_id, artist_id, service_name)
    new(person_id, artist_id, service_name).build
  end

  def initialize(person_id, artist_id, service_name)
    @person_id = person_id
    @artist_id = artist_id
    @service_name = service_name
  end

  def build
    Creative.from("(#{album_creatives_query.to_sql} UNION #{song_creatives_query.to_sql}) AS creatives")
  end

  private

  def album_creatives_query
    Creative
      .joins(joins)
      .select(selection)
      .distinct("#{service_name}_id")
      .where(filter.merge(creatives: albums_creatives_filter))
  end

  def song_creatives_query
    Creative
      .joins(joins + [songs_join])
      .select(selection)
      .distinct(service_name_id)
      .where(filter.merge(creatives: songs_creatives_filter))
  end

  def joins
    [
      :artist,
      :person,
      albums_join,
      external_service_ids_join,
      genres_join
    ]
  end

  def filter
    {
      people: {
        id: person_id
      },
      artists: {
        id: artist_id
      },
      external_service_ids: {
        service_name: service_name
      }
    }
  end

  def albums_creatives_filter
    { creativeable_type: "Album" }
  end

  def songs_creatives_filter
    { creativeable_type: "Song" }
  end

  def service_name_id
    "#{service_name}_id"
  end

  def selection
    [
      external_service_ids[:identifier].as("#{service_name}_id"),
      creatives[Arel.star],
      albums[:finalized_at].as("release_date"),
      albums[:name].as("album_name"),
      genres[:name].as("primary_genre_name"),
      albums[:id].as("album_id")
    ]
  end

  def songs_join
    join_on = albums.create_on(
      albums[:id].eq(songs[:album_id])
    )

    albums.create_join(
      songs,
      join_on
    )
  end

  def albums_join
    join_on = creatives.create_on(
      albums[:id].eq(creatives[:creativeable_id]).and(creatives[:creativeable_type].eq("Album"))
    )

    creatives.create_join(
      albums,
      join_on
    )
  end

  def external_service_ids_join
    join_on = albums.create_on(
      albums[:id].eq(external_service_ids[:linkable_id]).and(
        external_service_ids[:linkable_type].eq("Album")
      )
    )

    albums.create_join(
      external_service_ids,
      join_on
    )
  end

  def genres_join
    join_on = albums.create_on(
      albums[:primary_genre_id].eq(genres[:id])
    )

    albums.create_join(
      genres,
      join_on
    )
  end

  def songs
    Song.arel_table
  end

  def people
    Person.arel_table
  end

  def albums
    Album.arel_table
  end

  def external_service_ids
    ExternalServiceId.arel_table
  end

  def genres
    Genre.arel_table
  end

  def creatives
    Creative.arel_table
  end
end
