class MyArtist::NameQueryBuilder
  include ArelTableMethods
  include ArelJoinMethods

  def self.build(person_id)
    new.build(person_id)
  end

  def build(person_id)
    MyArtist::CollectionQueryBuilder.build
                                    .joins(left_outer_join_creative_song_roles)
                                    .where(people: { id: person_id })
  end

  private

  def left_outer_join_creative_song_roles
    left_outer_join(creative_t, creative_song_role_t, creative_t[:id], creative_song_role_t[:creative_id])
  end
end
