class MyArtist::ReleasesQueryBuilder
  attr_reader :person_id, :artist_id

  def self.build(person_id, artist_id)
    new(person_id, artist_id).build
  end

  def initialize(person_id, artist_id)
    @person_id = person_id
    @artist_id = artist_id
  end

  def build
    Album.from("(#{album_query.to_sql} UNION #{song_query.to_sql}) AS albums")
  end

  private

  def album_query
    Album
      .joins(:creatives)
      .select(selection)
      .where(person_id: person_id, creatives: creatives_filter)
  end

  def song_query
    Song
      .joins(:creatives, :album)
      .select(selection)
      .where(albums: { person_id: person_id }, creatives: creatives_filter)
  end

  def creatives_filter
    { artist_id: artist_id, role: %w[featuring primary_artist] }
  end

  def selection
    [albums[Arel.star], creatives[:role].as("creative_role")]
  end

  def albums
    Album.arel_table
  end

  def creatives
    Creative.arel_table
  end
end
