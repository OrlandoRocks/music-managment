class MyArtist::CollectionQueryBuilder
  def self.build
    new.build
  end

  def build
    Creative.select(
      [
        "artists.name AS artist_name",
        "artists.id AS artist_id",
        external_service_id_artworks[Arel.star]
      ]
    ).distinct(:artist_name).joins(
      :person,
      :artist,
      external_service_ids_join,
      external_service_id_artworks_join
    ).order("artists.name")
  end

  private

  def external_service_id_artworks_join
    join_on = creatives.create_on(
      external_service_ids[:external_service_id_artwork_id].eq(external_service_id_artworks[:id])
    )
    creatives.create_join(
      external_service_id_artworks,
      join_on,
      Arel::Nodes::OuterJoin
    )
  end

  def external_service_ids_join
    join_on = creatives.create_on(
      external_service_ids[:linkable_id].eq(creatives[:id])
        .and(external_service_ids[:linkable_type].eq("Creative"))
    )
    creatives.create_join(
      external_service_ids,
      join_on,
      Arel::Nodes::OuterJoin
    )
  end

  def creatives
    Creative.arel_table
  end

  def people
    Person.arel_table
  end

  def external_service_ids
    ExternalServiceId.arel_table
  end

  def external_service_id_artworks
    ExternalServiceIdArtwork.arel_table
  end

  def artists
    Artist.arel_table
  end
end
