class PublishingAdministration::AllPublishingCompositionsBuilder < PublishingAdministration::PublishingCompositionsBuilder
  def initialize(search_params)
    super

    @publishing_composer_ids = search_params[:publishing_composer_ids]
  end

  private

  attr_reader :publishing_composer_ids

  def select_statement
    super.append(publishing_composer_split_ids)
  end

  def publishing_composer_split_ids
    <<-SQL
      (
        SELECT GROUP_CONCAT(
          DISTINCT publishing_composition_splits.id
        )
        FROM
          publishing_composition_splits
        INNER JOIN
          publishing_composers ON publishing_composers.id = publishing_composition_splits.publishing_composer_id
        WHERE publishing_composition_splits.publishing_composition_id = publishing_compositions.id
      )
      AS composer_split_ids
    SQL
  end

  def where_clause
    if publishing_compositions_name.present?
      publishing_composer_t[:id]
        .in(publishing_composer_ids)
        .and(
          publishing_composition_t[:state].not_eq("hidden")
        )
        .and(
          publishing_composition_t[:name].matches("%#{publishing_compositions_name}%")
        )
    else
      publishing_composer_t[:id]
        .in(publishing_composer_ids)
        .and(
          publishing_composition_t[:state].not_eq("hidden")
        )
    end
  end

  def hidden_where_clause
    if publishing_compositions_name.present?
      publishing_composer_t[:id]
        .in(publishing_composer_ids)
        .and(
          publishing_composition_t[:state].eq("hidden")
        )
        .and(
          publishing_composition_t[:name].matches("%#{publishing_compositions_name}%")
        )
    else
      publishing_composer_t[:id]
        .in(publishing_composer_ids)
        .and(
          publishing_composition_t[:state].eq("hidden")
        )
    end
  end
end
