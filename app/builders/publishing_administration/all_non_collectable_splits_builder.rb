class PublishingAdministration::AllNonCollectableSplitsBuilder < PublishingAdministration::NonCollectableSplitsBuilder
  def self.build(publishing_composer_ids, publishing_composition_id = nil)
    new(publishing_composer_ids, publishing_composition_id).build
  end

  def initialize(publishing_composer_ids, publishing_composition_id = nil)
    @publishing_composer_ids = publishing_composer_ids
    @publishing_composition_id = publishing_composition_id
  end

  def build
    PublishingCompositionSplit
      .non_collectable_known
      .where(publishing_composition_splits: { publishing_composer_id: publishing_composer_ids })
      .where(find_by_publishing_composition)
      .select(select_statement)
  end

  private

  attr_reader :publishing_composer_ids
end
