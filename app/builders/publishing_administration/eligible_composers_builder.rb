class PublishingAdministration::EligibleComposersBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.composers(params = {})
    new(params).composers
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date   = params[:end_date]
  end

  def composers
    Composer
      .purchased_pub_admin
      .joins(:account)
      .where(missing_attributes)
      .where(composer_cae_and_pro_are_both_empty_or_present)
      .where(filter_by_date)
      .readonly(false)
  end

  private

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    composer_t[:created_at].gteq(start_date)
                           .and(composer_t[:created_at].lteq(end_date))
                           .or(account_t[:created_at].gteq(start_date)
      .and(account_t[:created_at].lteq(end_date)))
  end

  def composer_cae_and_pro_are_both_empty_or_present
    <<-SQL
     (NULLIF(`composers`.`cae`, '') IS NULL
     AND NULLIF(`composers`.`performing_rights_organization_id`, '') IS NULL
     OR NULLIF(`composers`.`cae`, '') IS NOT NULL
     AND NULLIF(`composers`.`performing_rights_organization_id`, '') IS NOT NULL)
    SQL
  end

  def missing_attributes
    "(NULLIF(`accounts`.`provider_account_id`, '') IS NULL OR NULLIF(`composers`.`provider_identifier`, '') IS NULL)"
  end
end
