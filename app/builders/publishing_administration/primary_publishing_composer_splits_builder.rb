class PublishingAdministration::PrimaryPublishingComposerSplitsBuilder
  include ArelTableMethods

  def self.build(publishing_composition_id)
    new(publishing_composition_id).build
  end

  def initialize(publishing_composition_id)
    @publishing_composition_id = publishing_composition_id
  end

  def build
    PublishingCompositionSplit
      .joins(:publishing_composer)
      .where(publishing_composition_splits: { right_to_collect: true })
      .where(find_by_composition)
      .select(select_statement)
  end

  private

  attr_reader :publishing_composition_id

  def select_statement
    [
      publishing_composer_t[:id],
      publishing_composition_split_t[:id].as("id"),
      publishing_composition_split_t[:percent].as("composer_share"),
    ]
  end

  def find_by_composition
    { publishing_composition_splits: { publishing_composition_id: publishing_composition_id } }
  end
end
