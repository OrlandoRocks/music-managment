class PublishingAdministration::PublishingComposersBuilder
  def self.build(account_id, person_id = nil)
    publishing_composers = new(account_id).build

    return publishing_composers if person_id.blank?

    publishing_composers.where(person_id: person_id)
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def build
    PublishingComposer
      .unscoped
      .joins(:account)
      .where(accounts: { id: @account_id }, is_primary_composer: true)
      .group("publishing_composers.id")
      .order(sort_order)
  end

  private

  def sort_order
    <<-SQL
      CASE WHEN IFNULL(publishing_composers.person_id, -1) = accounts.person_id
        THEN 0
        ELSE 1
      END
    SQL
  end
end
