class PublishingAdministration::NonCollectableSplitsBuilder
  def self.build(publishing_composer_id, publishing_composition_id = nil)
    new(publishing_composer_id, publishing_composition_id).build
  end

  def initialize(publishing_composer_id, publishing_composition_id = nil)
    @publishing_composer_id = publishing_composer_id
    @primary_publishing_composer = PublishingComposer.find(publishing_composer_id)
    @publishing_composition_id = publishing_composition_id
    @publishing_composition_ids = primary_publishing_composer.publishing_compositions.pluck(:id)
  end

  def build
    PublishingCompositionSplit
      .non_collectable_known
      .where(publishing_composition_splits: { publishing_composition_id: publishing_composition_ids })
      .where(find_by_publishing_composition)
      .select(select_statement)
  end

  private

  attr_reader :publishing_composer_id,
              :publishing_composition_id,
              :primary_publishing_composer,
              :publishing_composition_ids

  def select_statement
    <<-SQL
      publishing_composers.first_name,
      publishing_composers.last_name,
      publishing_composers.is_unknown,
      publishing_composition_splits.id AS id,
      publishing_composition_splits.percent AS cowriter_share
    SQL
  end

  def find_by_publishing_composition
    return {} if publishing_composition_id.blank?

    { publishing_composition_splits: { publishing_composition_id: publishing_composition_id } }
  end
end
