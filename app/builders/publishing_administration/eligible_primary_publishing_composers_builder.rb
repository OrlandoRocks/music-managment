class PublishingAdministration::EligiblePrimaryPublishingComposersBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.primary_publishing_composers(params = {})
    new(params).primary_publishing_composers
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end

  def primary_publishing_composers
    PublishingComposer
      .primary
      .purchased_pub_admin
      .joins(:account)
      .where(missing_attributes)
      .where(publishing_composer_cae_and_pro_are_both_empty_or_present)
      .where(filter_by_date)
      .readonly(false)
  end

  private

  def missing_attributes
    <<-SQL
      (
        NULLIF(`accounts`.`provider_account_id`, '') IS NULL
        OR
        NULLIF(`publishing_composers`.`provider_identifier`, '') IS NULL
      )
    SQL
  end

  def publishing_composer_cae_and_pro_are_both_empty_or_present
    <<-SQL
      (
        NULLIF(`publishing_composers`.`cae`, '') IS NULL
        AND
        NULLIF(`publishing_composers`.`performing_rights_organization_id`, '') IS NULL
        OR
        NULLIF(`publishing_composers`.`cae`, '') IS NOT NULL
        AND
        NULLIF(`publishing_composers`.`performing_rights_organization_id`, '') IS NOT NULL
      )
    SQL
  end

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    publishing_composer_t[:created_at].gteq(start_date)
                                      .and(publishing_composer_t[:created_at].lteq(end_date))
                                      .or(account_t[:created_at].gteq(start_date).and(account_t[:created_at].lteq(end_date)))
  end
end
