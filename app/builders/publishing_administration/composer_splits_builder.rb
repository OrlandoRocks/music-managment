class PublishingAdministration::ComposerSplitsBuilder
  include ArelTableMethods

  def self.build(composition_id)
    new(composition_id).build
  end

  def initialize(composition_id)
    @composition_id = composition_id
  end

  def build
    PublishingSplit
      .joins(:composer)
      .where(publishing_splits: { writer_type: "Composer" })
      .where(find_by_composition)
      .select(select_statement)
  end

  private

  attr_reader :composition_id

  def select_statement
    [
      composer_t[:id],
      publishing_split_t[:id].as("id"),
      publishing_split_t[:percent].as("composer_share"),
    ]
  end

  def find_by_composition
    { publishing_splits: { composition_id: composition_id } }
  end
end
