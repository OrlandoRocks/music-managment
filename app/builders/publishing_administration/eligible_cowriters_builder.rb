class PublishingAdministration::EligibleCowritersBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.cowriters(params = {})
    new(params).cowriters
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date   = params[:end_date]
  end

  def cowriters
    Cowriter
      .where(composer_id: composer_ids_with_pub_admin)
      .where(cowriter_provider_identifier)
      .where(filter_by_date)
  end

  private

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    cowriter_t[:created_at].gteq(start_date)
                           .and(cowriter_t[:created_at].lteq(end_date))
  end

  def composer_ids_with_pub_admin
    Composer.purchased_pub_admin.pluck(:id)
  end

  def cowriter_provider_identifier
    "NULLIF(`cowriters`.`provider_identifier`, '') IS NULL"
  end
end
