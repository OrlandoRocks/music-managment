class PublishingAdministration::ComposersBuilder
  include ArelTableMethods

  def self.build(account_id, person_id = nil)
    composers = new(account_id).build

    if person_id.present?
      composers.select { |composer| composer.person_id == person_id }
    else
      composers
    end
  end

  def initialize(account_id)
    @account_id = account_id
  end

  def build
    Composer
      .unscoped
      .joins(:account)
      .where(accounts: { id: @account_id })
      .group(composer_t[:id])
      .order(sort_order)
  end

  private

  def sort_order
    <<-SQL
      CASE WHEN IFNULL(composers.person_id, -1) = accounts.person_id
        THEN 0
        ELSE 1
      END
    SQL
  end
end
