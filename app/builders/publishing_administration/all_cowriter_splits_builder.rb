class PublishingAdministration::AllCowriterSplitsBuilder < PublishingAdministration::CowriterSplitsBuilder
  def self.build(composer_ids, composition_id = nil)
    new(composer_ids, composition_id).build
  end

  def initialize(composer_ids, composition_id = nil)
    @composer_ids = composer_ids
    @composition_id = composition_id
  end

  def build
    PublishingSplit
      .joins(cowriter_join)
      .where(publishing_splits: { composer_id: composer_ids })
      .where(cowriters: { is_unknown: false })
      .where(find_by_composition)
      .select(select_statement)
  end

  private

  attr_reader :composer_ids
end
