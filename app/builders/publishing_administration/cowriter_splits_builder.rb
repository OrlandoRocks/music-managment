class PublishingAdministration::CowriterSplitsBuilder
  include ArelTableMethods

  def self.build(composer_id, composition_id = nil)
    new(composer_id, composition_id).build
  end

  def initialize(composer_id, composition_id = nil)
    @composer_id = composer_id
    @composition_id = composition_id
  end

  def build
    PublishingSplit
      .joins(cowriter_join)
      .where(publishing_splits: { composer_id: composer_id })
      .where(cowriters: { is_unknown: false })
      .where(find_by_composition)
      .select(select_statement)
  end

  private

  attr_reader :composer_id, :composition_id

  def select_statement
    [
      cowriter_t[:first_name],
      cowriter_t[:last_name],
      cowriter_t[:is_unknown],
      publishing_split_t[:id].as("id"),
      publishing_split_t[:percent].as("cowriter_share"),
    ]
  end

  def cowriter_join
    publishing_split_t.join(
      cowriter_t
    ).on(
      publishing_split_t[:writer_id].eq(cowriter_t[:id]).and(publishing_split_t[:writer_type].eq("Cowriter"))
    ).join_sources
  end

  def find_by_composition
    if composition_id.present?
      { publishing_splits: { composition_id: composition_id } }
    else
      {}
    end
  end
end
