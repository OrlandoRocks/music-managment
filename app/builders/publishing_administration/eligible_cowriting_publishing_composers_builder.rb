class PublishingAdministration::EligibleCowritingPublishingComposersBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.cowriting_publishing_composers(params = {})
    new(params).cowriting_publishing_composers
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end

  def cowriting_publishing_composers
    PublishingComposer
      .cowriting
      .where(account_id: publishing_composer_account_ids_with_pub_admin)
      .where(missing_provider_identifier)
      .where(filter_by_date)
  end

  private

  def publishing_composer_account_ids_with_pub_admin
    PublishingComposer.purchased_pub_admin.pluck(:account_id)
  end

  def missing_provider_identifier
    <<-SQL
      NULLIF(`publishing_composers`.`provider_identifier`, '') IS NULL
    SQL
  end

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    publishing_composer_t[:created_at].gteq(start_date)
                                      .and(publishing_composer_t[:created_at].lteq(end_date))
  end
end
