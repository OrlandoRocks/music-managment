class PublishingAdministration::CompositionsBuilder
  include ArelTableMethods

  def self.build(search_params)
    new(search_params).build
  end

  def initialize(search_params)
    @composer_id = search_params[:composer_id]
    @compositions_name = search_params[:composition_name]
    @hidden_compositions = search_params[:hidden]
  end

  # NOTE: this used to return an ActiveRecord::Relation, but now returns an
  #  array. If this becomes a requirement in the future, one path forward is
  #  to build a union between the two compositions_with_* methods and return
  #  that union. Best of luck.

  def build
    compositions_with_songs.to_a.concat(compositions_with_non_tunecore_songs.to_a)
  end

  private

  attr_reader :composer_id, :compositions_name

  def compositions_with_songs
    isrc_select = [
      song_t[:tunecore_isrc],
      song_t[:optional_isrc]
    ]
    conditions = @hidden_compositions.present? ? hidden_where_clause : where_clause
    Composition
      .select(select_statement(album_t) + isrc_select)
      .joins(
        publishing_splits_join,
        cowriters_join,
        recordings_join,
        unknown_split_join,
        songs: { album: { person: :composers } }
      )
      .where(conditions)
      .where(albums: { is_deleted: false, legal_review_state: ["APPROVED", "DO NOT REVIEW"] })
      .where.not(albums: { finalized_at: nil })
      .group(composition_t[:id])
  end

  def compositions_with_non_tunecore_songs
    isrc_select = non_tunecore_song_t[:isrc]
    performing_artist_select = artist_t[:name].as("performing_artist")
    release_date_select = non_tunecore_song_t[:release_date]

    conditions = @hidden_compositions.present? ? hidden_where_clause : where_clause

    Composition
      .select(
        select_statement(non_tunecore_album_t) +
        [isrc_select, performing_artist_select, release_date_select]
      )
      .joins(
        non_tunecore_songs_join,
        non_tunecore_albums_join,
        composers_join,
        publishing_splits_join,
        cowriters_join,
        recordings_join,
        artists_join,
        unknown_split_join,
        non_tunecore_songs: { non_tunecore_album: :composer }
      )
      .where(conditions)
      .group(composition_t[:id])
  end

  def share_percentage(writer_type)
    <<-SQL
      IFNULL((SELECT SUM(ps.percent)
      FROM publishing_splits AS ps
      WHERE ps.composition_id = compositions.id
        AND ps.writer_type = "#{writer_type}"), 0) AS #{writer_type.downcase}_share
    SQL
  end

  def cowriter_split_ids
    <<-SQL
      (SELECT GROUP_CONCAT(DISTINCT publishing_splits.id) FROM publishing_splits
        INNER JOIN cowriters ON cowriters.id = publishing_splits.writer_id
      WHERE publishing_splits.composition_id = compositions.id
        AND cowriters.is_unknown = FALSE) AS cowriter_split_ids
    SQL
  end

  def cowriters_join
    publishing_split_t.join(
      cowriter_t, Arel::Nodes::OuterJoin
    ).on(
      cowriter_t[:id].eq(publishing_split_t[:writer_id])
        .and(publishing_split_t[:writer_type].eq("Cowriter"))
        .and(publishing_split_t[:composition_id].eq(composition_t[:id]))
    ).join_sources
  end

  def publishing_splits_join
    composition_t.join(
      publishing_split_t, Arel::Nodes::OuterJoin
    ).on(
      composition_t[:id].eq(publishing_split_t[:composition_id])
    ).join_sources
  end

  def recordings_join
    composition_t.join(
      recording_t, Arel::Nodes::OuterJoin
    ).on(
      composition_t[:id].eq(recording_t[:composition_id])
    ).join_sources
  end

  def non_tunecore_songs_join
    composition_t.join(
      non_tunecore_song_t, Arel::Nodes::InnerJoin
    ).on(
      non_tunecore_song_t[:composition_id].eq(composition_t[:id])
    ).join_sources
  end

  def artists_join
    non_tunecore_song_t.join(
      artist_t, Arel::Nodes::OuterJoin
    ).on(
      non_tunecore_song_t[:artist_id].eq(artist_t[:id])
    ).join_sources
  end

  def unknown_split_join
    <<-SQL
     LEFT OUTER JOIN (
       SELECT ps1.percent, ps1.composition_id FROM publishing_splits ps1
         INNER JOIN cowriters c1 ON c1.id = ps1.writer_id
       WHERE c1.is_unknown = TRUE
     ) AS unknown_split ON unknown_split.composition_id = compositions.id
    SQL
  end

  def non_tunecore_albums_join
    non_tunecore_song_t.join(
      non_tunecore_album_t, Arel::Nodes::InnerJoin
    ).on(
      non_tunecore_album_t[:id].eq(non_tunecore_song_t[:non_tunecore_album_id])
    ).join_sources
  end

  def composers_join
    non_tunecore_album_t.join(
      composer_t, Arel::Nodes::InnerJoin
    ).on(
      composer_t[:id].eq(non_tunecore_album_t[:composer_id])
    ).join_sources
  end

  def select_statement(album_table)
    [
      composition_t[:id].as("id"),
      composition_t[:name].as("composition_title"),
      composition_t[:state].as("composition_state"),
      composition_t[:translated_name],
      composition_t[:name],
      composition_t[:provider_identifier],
      composition_t[:state_updated_on],
      composition_t[:verified_date],
      recording_t[:recording_code].as("recording_code"),
      album_table[:name].as("appears_on"),
      share_percentage("Composer"),
      share_percentage("Cowriter"),
      cowriter_split_ids,
      "unknown_split.percent AS unknown_split_percent",
    ]
  end

  def where_clause
    if compositions_name.present?
      composer_t[:id].eq(composer_id).and(composition_t[:state].not_eq("hidden")).and(composition_t[:name].matches("%#{compositions_name}%"))
    else
      composer_t[:id].eq(composer_id).and(composition_t[:state].not_eq("hidden"))
    end
  end

  def hidden_where_clause
    if compositions_name.present?
      composer_t[:id].eq(composer_id).and(composition_t[:state].eq("hidden")).and(composition_t[:name].matches("%#{compositions_name}%"))
    else
      composer_t[:id].eq(composer_id).and(composition_t[:state].eq("hidden"))
    end
  end
end
