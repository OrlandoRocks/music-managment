class PublishingAdministration::AllCompositionsBuilder < PublishingAdministration::CompositionsBuilder
  def initialize(search_params)
    super

    @composer_ids = search_params[:composer_ids]
  end

  private

  attr_reader :composer_ids

  def select_statement(album_table)
    super(album_table).append(composer_split_ids)
  end

  def composer_split_ids
    <<-SQL
      (SELECT GROUP_CONCAT(DISTINCT publishing_splits.id) FROM publishing_splits
        INNER JOIN composers ON composers.id = publishing_splits.writer_id
        AND publishing_splits.writer_type = 'Composer'
      WHERE publishing_splits.composition_id = compositions.id)
      AS composer_split_ids
    SQL
  end

  def where_clause
    if compositions_name.present?
      composer_t[:id].in(composer_ids).and(composition_t[:state].not_eq("hidden")).and(composition_t[:name].matches("%#{compositions_name}%"))
    else
      composer_t[:id].in(composer_ids).and(composition_t[:state].not_eq("hidden"))
    end
  end

  def hidden_where_clause
    if compositions_name.present?
      composer_t[:id].in(composer_ids).and(composition_t[:state].eq("hidden")).and(composition_t[:name].matches("%#{compositions_name}%"))
    else
      composer_t[:id].in(composer_ids).and(composition_t[:state].eq("hidden"))
    end
  end
end
