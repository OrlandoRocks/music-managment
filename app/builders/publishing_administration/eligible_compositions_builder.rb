class PublishingAdministration::EligibleCompositionsBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.compositions(params = {})
    new(params).compositions
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date   = params[:end_date]
  end

  def compositions
    Composition
      .joins(:publishing_splits, recordings_join)
      .where(publishing_splits: {
               writer_id: composer_ids_with_pub_admin,
               writer_type: "Composer"
             })
      .where(missing_provider_identifier_or_recording_code)
      .where(filter_by_date)
      .select([
                composition_t[Arel.star],
                publishing_split_t[:composer_id].as("composer_id")
              ])
  end

  private

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    composition_t[:created_at].gteq(start_date)
                              .and(composition_t[:created_at].lteq(end_date))
  end

  def composer_ids_with_pub_admin
    Composer.purchased_pub_admin.pluck(:id)
  end

  def missing_provider_identifier_or_recording_code
    <<-SQL
    (NULLIF(`compositions`.`provider_identifier`, '') IS NULL
     OR NULLIF(`recordings`.`recording_code`, '') IS NULL)
    SQL
  end

  def recordings_join
    composition_t.join(
      recording_t, Arel::Nodes::OuterJoin
    ).on(
      composition_t[:id].eq(recording_t[:composition_id])
    ).join_sources
  end
end
