class PublishingAdministration::EligiblePublishingCompositionsBuilder
  include ArelTableMethods

  attr_reader :start_date, :end_date

  def self.publishing_compositions(params = {})
    new(params).publishing_compositions
  end

  def initialize(params = {})
    @start_date = params[:start_date]
    @end_date   = params[:end_date]
  end

  def publishing_compositions
    PublishingComposition
      .joins(:publishing_composition_splits, recordings_join)
      .where(publishing_composition_splits: {
               publishing_composer_id: publishing_composer_ids_with_pub_admin,
               right_to_collect: true
             })
      .where(missing_provider_identifier_or_recording_code)
      .where(filter_by_date)
      .select([
                publishing_composition_t[Arel.star],
                publishing_composition_split_t[:publishing_composer_id].as("publishing_composer_id")
              ])
  end

  private

  def filter_by_date
    return {} if start_date.nil? || end_date.nil?

    publishing_composition_t[:created_at].gteq(start_date)
                                         .and(publishing_composition_t[:created_at].lteq(end_date))
  end

  def publishing_composer_ids_with_pub_admin
    PublishingComposer.purchased_pub_admin.pluck(:id)
  end

  def missing_provider_identifier_or_recording_code
    <<-SQL
      (
        NULLIF(`publishing_compositions`.`provider_identifier`, '') IS NULL
        OR
        NULLIF(`recordings`.`recording_code`, '') IS NULL
      )
    SQL
  end

  def recordings_join
    publishing_composition_t.join(
      recording_t, Arel::Nodes::OuterJoin
    ).on(
      publishing_composition_t[:id].eq(recording_t[:publishing_composition_id])
    ).join_sources
  end
end
