class PublishingAdministration::PublishingCompositionsBuilder
  include ArelTableMethods

  def self.build(search_params)
    new(search_params).build
  end

  def initialize(search_params)
    @publishing_composer_id = search_params[:publishing_composer_id]
    @publishing_compositions_name = search_params[:publishing_composition_name]
    @hidden_publishing_compositions = search_params[:hidden]
    @search_by = search_params[:search_by]
    @search_term = search_params[:search_term]
    @order_by = search_params[:order_by]
    @order_by_desc = search_params[:order_by_desc].to_s == "true"
  end

  def build
    publishing_compositions_query
  end

  private

  attr_reader :publishing_composer_id, :publishing_compositions_name

  def publishing_compositions_query
    conditions = @hidden_publishing_compositions.present? ? hidden_where_clause : where_clause

    PublishingComposition
      .select(select_statement)
      .joins(
        publishing_composition_splits_join,
        recordings_join,
        unknown_split_join,
        non_tunecore_songs_join,
        non_tunecore_albums_join,
        songs_join,
        albums_join,
        publishing_composers_join,
        artists_join
      )
      .where(conditions)
      .where(albums_conditions)
      .where(search_statement)
      .group(publishing_composition_t[:id])
      .order(order_by_statment)
  end

  def select_statement
    [
      publishing_composition_t[:id].as("id"),
      publishing_composition_t[:name].as("publishing_composition_title"),
      publishing_composition_t[:state].as("publishing_composition_state"),
      publishing_composition_t[:translated_name],
      publishing_composition_t[:name],
      publishing_composition_t[:provider_identifier],
      publishing_composition_t[:state_updated_on],
      publishing_composition_t[:verified_date],
      publishing_composition_t[:public_domain],
      recording_t[:recording_code].as("recording_code"),
      select_appears_on,
      select_share_percentage(true, "Composer"),
      select_share_percentage(false, "Cowriter"),
      select_cowriter_split_ids,
      share_submitted_date,
      "unknown_split.percent AS unknown_split_percent",
      song_t[:tunecore_isrc],
      song_t[:optional_isrc],
      non_tunecore_song_t[:isrc],
      artist_t[:name].as("performing_artist"),
      non_tunecore_song_t[:release_date],
      non_tunecore_album_t[:record_label].as("record_label"),
      select_is_ntc_song
    ]
  end

  def select_appears_on
    <<-SQL
      IFNULL(`albums`.`name`, non_tunecore_albums.name) AS appears_on
    SQL
  end

  def select_is_ntc_song
    <<-SQL
      CASE WHEN `songs`.`tunecore_isrc` IS NOT NULL THEN FALSE ELSE TRUE END AS is_ntc_song
    SQL
  end

  def select_share_percentage(right_to_collect, writer_type)
    <<-SQL
      IFNULL((SELECT SUM(ps.percent)
      FROM publishing_composition_splits AS ps
      WHERE ps.publishing_composition_id = publishing_compositions.id
        AND ps.right_to_collect = #{right_to_collect}), 0) AS #{writer_type.downcase}_share
    SQL
  end

  def select_cowriter_split_ids
    <<-SQL
      (SELECT GROUP_CONCAT(DISTINCT publishing_composition_splits.id) FROM publishing_composition_splits
        INNER JOIN publishing_composers ON publishing_composers.id = publishing_composition_splits.publishing_composer_id
      WHERE publishing_composition_splits.publishing_composition_id = publishing_compositions.id
        AND publishing_composition_splits.right_to_collect = FALSE) AS cowriter_split_ids
    SQL
  end

  def select_split_total
    <<-SQL
      IFNULL((SELECT SUM(ps.percent)
      FROM publishing_composition_splits AS ps
      WHERE ps.publishing_composition_id = publishing_compositions.id), 0)
    SQL
  end

  def share_submitted_date
    <<-SQL
      (SELECT publishing_composition_splits.updated_at FROM publishing_composition_splits
        INNER JOIN publishing_composers ON publishing_composers.id = publishing_composition_splits.publishing_composer_id
        WHERE publishing_composition_splits.publishing_composition_id = publishing_compositions.id
        ORDER BY publishing_composition_splits.updated_at DESC
        LIMIT 1
      ) AS share_submitted_date
    SQL
  end

  def share_submitted_date_no_alias
    <<-SQL
      (SELECT publishing_composition_splits.updated_at FROM publishing_composition_splits
        INNER JOIN publishing_composers ON publishing_composers.id = publishing_composition_splits.publishing_composer_id
        WHERE publishing_composition_splits.publishing_composition_id = publishing_compositions.id
        ORDER BY publishing_composition_splits.updated_at DESC
        LIMIT 1
      )
    SQL
  end

  def publishing_composition_splits_join
    publishing_composition_t.join(
      publishing_composition_split_t, Arel::Nodes::OuterJoin
    ).on(
      publishing_composition_t[:id].eq(publishing_composition_split_t[:publishing_composition_id])
    ).join_sources
  end

  def recordings_join
    publishing_composition_t.join(
      recording_t, Arel::Nodes::OuterJoin
    ).on(
      publishing_composition_t[:id].eq(recording_t[:publishing_composition_id])
    ).join_sources
  end

  def unknown_split_join
    <<-SQL
     LEFT OUTER JOIN (
       SELECT pcs1.percent, pcs1.publishing_composition_id FROM publishing_composition_splits pcs1
       INNER JOIN publishing_composers pc1 ON pc1.id = pcs1.publishing_composer_id
       WHERE pc1.is_unknown = TRUE
     ) AS unknown_split ON unknown_split.publishing_composition_id = publishing_compositions.id
    SQL
  end

  def non_tunecore_songs_join
    publishing_composition_t.join(
      non_tunecore_song_t, Arel::Nodes::OuterJoin
    ).on(
      non_tunecore_song_t[:publishing_composition_id].eq(publishing_composition_t[:id])
    ).join_sources
  end

  def non_tunecore_albums_join
    non_tunecore_song_t.join(
      non_tunecore_album_t, Arel::Nodes::OuterJoin
    ).on(
      non_tunecore_album_t[:id].eq(non_tunecore_song_t[:non_tunecore_album_id])
    ).join_sources
  end

  def songs_join
    publishing_composition_t.join(
      song_t, Arel::Nodes::OuterJoin
    ).on(
      song_t[:publishing_composition_id].eq(publishing_composition_t[:id])
    ).join_sources
  end

  def albums_join
    song_t.join(
      album_t, Arel::Nodes::OuterJoin
    ).on(
      album_t[:id].eq(song_t[:album_id])
    ).join_sources
  end

  def people_join
    <<-SQL
      LEFT OUTER JOIN `people` ON `people`.`id` = `albums`.`person_id`
    SQL
  end

  def publishing_composers_join
    <<-SQL
      LEFT OUTER JOIN `publishing_composers`#{' '}
        ON publishing_composers.account_id = publishing_compositions.account_id
    SQL
  end

  def artists_join
    non_tunecore_song_t.join(
      artist_t, Arel::Nodes::OuterJoin
    ).on(
      non_tunecore_song_t[:artist_id].eq(artist_t[:id])
    ).join_sources
  end

  def where_clause
    if publishing_compositions_name.present?
      publishing_composer_t[:id].eq(publishing_composer_id).and(publishing_composition_t[:state].not_eq("hidden")).and(publishing_composition_t[:name].matches("%#{publishing_compositions_name}%"))
    else
      publishing_composer_t[:id].eq(publishing_composer_id).and(publishing_composition_t[:state].not_eq("hidden"))
    end
  end

  def hidden_where_clause
    if publishing_compositions_name.present?
      publishing_composer_t[:id].eq(publishing_composer_id).and(publishing_composition_t[:state].eq("hidden")).and(publishing_composition_t[:name].matches("%#{publishing_compositions_name}%"))
    else
      publishing_composer_t[:id].eq(publishing_composer_id).and(publishing_composition_t[:state].eq("hidden"))
    end
  end

  def albums_conditions
    <<-SQL
    `publishing_compositions`.`is_unallocated_sum` = FALSE
    AND CASE WHEN (albums.id is not NULL)#{' '}
      THEN#{' '}
        `albums`.`is_deleted` = FALSE#{' '}
        AND `albums`.`legal_review_state` IN ('APPROVED', 'DO NOT REVIEW')#{' '}
        AND `albums`.`finalized_at` IS NOT NULL#{' '}
      ELSE TRUE
		END
    SQL
  end

  def search_statement
    default = <<-SQL
      TRUE
    SQL
    invalid_input = <<-SQL
      FALSE
    SQL

    return default if @search_term == ""

    case @search_by
    when "composition_id"
      term_i = @search_term.to_i
      return invalid_input if term_i < 1

      <<-SQL
        `publishing_compositions`.`id` = #{term_i}
      SQL
    when "composition_name"
      <<-SQL
        `publishing_compositions`.`name` LIKE '#{@search_term}%'
      SQL
    when "composition_name_contains"
      <<-SQL
        `publishing_compositions`.`name` LIKE '%#{@search_term}%'
      SQL
    when "isrc"
      <<-SQL
        optional_isrc LIKE '%#{@search_term}%' OR#{' '}
        tunecore_isrc LIKE '%#{@search_term}%' OR#{' '}
        isrc LIKE '%#{@search_term}%'
      SQL
    when "share_submitted_date"
      <<-SQL
        #{share_submitted_date_no_alias} LIKE '%#{@search_term}%'
      SQL
    when "total_share_eq"
      term_i = @search_term.to_i
      return invalid_input if @search_term.to_i.to_s != @search_term

      <<-SQL
        #{select_split_total} = #{term_i}
      SQL
    when "total_share_gt"
      term_i = @search_term.to_i
      return invalid_input if @search_term.to_i.to_s != @search_term

      <<-SQL
        #{select_split_total} > #{term_i}
      SQL
    when "total_share_lt"
      term_i = @search_term.to_i
      return invalid_input if @search_term.to_i.to_s != @search_term

      <<-SQL
        #{select_split_total} < #{term_i}
      SQL
    else
      default
    end
  end

  def order_by_statment
    case @order_by
    when "total_share"
      <<-SQL
        (`composer_share` + `cowriter_share`) #{@order_by_desc ? 'DESC' : 'ASC'}, composer_share DESC, publishing_composition_title ASC
      SQL
    when "isrc"
      <<-SQL
        IFNULL(`songs`.`optional_isrc`, IFNULL(`songs`.`tunecore_isrc`, `non_tunecore_songs`.`isrc`)) #{@order_by_desc ? 'DESC' : 'ASC'}, publishing_composition_title ASC
      SQL
    when "share_submitted_date"
      <<-SQL
        share_submitted_date #{@order_by_desc ? 'DESC' : 'ASC'}, publishing_composition_title ASC
      SQL
    when "composition_name"
      <<-SQL
        publishing_composition_title #{@order_by_desc ? 'DESC' : 'ASC'}
      SQL
    else
      <<-SQL
        id #{@order_by_desc ? 'DESC' : 'ASC'}#{' '}
      SQL
    end
  end
end
