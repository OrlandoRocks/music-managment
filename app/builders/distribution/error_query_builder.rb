class Distribution::ErrorQueryBuilder
  def self.build(params = {})
    new(params).build
  end

  attr_reader :start_date, :end_date, :filter_type, :states

  def initialize(params = {})
    @start_date  = params[:start_date]  || DateTime.now - 1
    @end_date    = params[:end_date]    || DateTime.now
    @filter_type = params[:filter_type] || "store"
    @states      = params[:states]      || ["error"]
  end

  def build
    send("filter_by_#{filter_type}").sort_by { |_, v| -v.size }
  end

  def filter_by_store
    query.group_by { |distribution| distribution[:store_id] }
  end

  def filter_by_error
    DistributionError.pluck(:message).each_with_object({}) do |error, obj|
      distributions = query.where(transition_table[:message].matches("%#{error}%"))
      obj[error]    = distributions if distributions.present?
    end
  end

  def filter_by_release
    query.group_by { |distribution| distribution[:album_name] }
  end

  def store_map
    @store_map ||= active_or_in_use_stores.each_with_object({}) { |store, stores| stores[store.id] = store.name }
  end

  private

  def active_or_in_use_stores
    Store.where(store_table[:in_use_flag].eq(true).or(store_table[:is_active].eq(true)))
  end

  def query
    @query ||= Distribution
               .select(select_statement)
               .joins(transitions)
               .joins(:salepoints)
               .joins(albums)
               .where(distributions: { state: states })
               .where(distribution_table[:updated_at].gteq(start_date).and(distribution_table[:updated_at].lt(end_date)))
               .where(salepoint_table[:salepointable_id].not_eq(nil))
               .where("transitions_2.id is NULL")
               .where(salepoint_table[:store_id].in(store_map.keys))
  end

  def select_statement
    <<-SQL.strip_heredoc
      DISTINCT (distributions.id) AS distribution_id,
      distributions.updated_at,
      distributions.state,
      salepoints.salepointable_id,
      salepoints.store_id AS store_id,
      transitions.message,
      albums.id AS album_id, albums.name AS album_name
    SQL
  end

  def transitions
    <<-SQL.strip_heredoc
      INNER JOIN transitions ON transitions.state_machine_id = distributions.id
      AND transitions.state_machine_type = 'Distribution'
      LEFT JOIN transitions transitions_2 ON (transitions_2.state_machine_id = distributions.id
      AND transitions.state_machine_type = 'Distribution'
      AND transitions_2.id > transitions.id)
    SQL
  end

  def albums
    <<-SQL.strip_heredoc
      INNER JOIN albums ON albums.id = salepoints.salepointable_id AND salepoints.salepointable_type = 'Album'
    SQL
  end

  def distribution_table
    Distribution.arel_table
  end

  def salepoint_table
    Salepoint.arel_table
  end

  def transition_table
    Transition.arel_table
  end

  def store_table
    Store.arel_table
  end
end
