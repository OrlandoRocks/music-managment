class Distribution::OrphanedSalepointQueryBuilder
  include ArelJoinMethods
  include ArelTableMethods

  attr_reader :album

  def self.build(album)
    new(album).build
  end

  def initialize(album)
    @album = album
  end

  def build
    distributions_salepoints_t = Arel::Table.new(:distributions_salepoints)

    album.salepoints.exclude_ytsr_proxy.payment_applied.has_store_delivery_config
         .includes(:store)
         .joins(
           left_outer_join(
             salepoint_t,
             distributions_salepoints_t,
             salepoint_t[:id],
             distributions_salepoints_t[:salepoint_id]
           )
         ).where(
           distributions_salepoints: {
             salepoint_id: nil
           }
         )
  end
end
