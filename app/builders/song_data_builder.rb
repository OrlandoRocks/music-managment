class SongDataBuilder
  def self.build(person_id, song)
    new(person_id: person_id, song: song).build
  end

  attr_reader :person_id, :album_id, :song

  def initialize(options = {})
    @song      = options[:song]
    @person_id = options[:person_id]
    @album_id  = @song.album_id
  end

  def build
    SongData.build(song, person_id, artist_names)
  end

  def artist_names
    @artist_names ||= MyArtist.all_artists_for(person_id).pluck("artists.name")
  end
end
