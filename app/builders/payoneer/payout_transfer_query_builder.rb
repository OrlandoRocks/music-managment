class Payoneer::PayoutTransferQueryBuilder
  include ArelTableMethods
  include ArelCommonJoins

  attr_reader :person_name,
              :withdrawal_method,
              :filter_type,
              :filter_amount,
              :tunecore_status,
              :provider_status,
              :currency,
              :start_date,
              :end_date,
              :rollback,
              :auto_approval_eligible

  def self.build(options)
    new(options).build
  end

  def initialize(options)
    @person_name = options[:person_name]
    @withdrawal_method = options[:withdrawal_method]
    @filter_type = options[:filter_type]
    @filter_amount = options[:filter_amount]
    @tunecore_status = options[:tunecore_status]
    @provider_status = options[:provider_status]
    @currency = options[:currency]
    @start_date = options[:start_date]
    @end_date = options[:end_date]
    @rollback = options[:rollback]
    @auto_approval_eligible = options[:auto_approval_eligible]
  end

  def build
    PayoutTransfer.joins(payout_provider: :person)
                  .by_tunecore_status(form_tunecore_status)
                  .by_provider_status(form_provider_status)
                  .where(name_filter)
                  .where(amount_filter)
                  .where(payout_type_filter)
                  .where(currency_filter)
                  .where(rollback_filter)
                  .where(date_range)
                  .then { |r| auto_approval_eligible_filter(r) }
  end

  private

  def date_range
    if start_date.blank? && end_date.blank?
      ""
    else
      start_val = start_date.try(:to_date) || Date.new(0)
      end_val = (end_date.try(:to_date) || Date.today) + 1.day
      {
        tunecore_processed_at: start_val..end_val,
        tunecore_status: [PayoutTransfer::APPROVED, PayoutTransfer::REJECTED]
      }
    end
  end

  def name_filter
    return "" if person_name.blank?

    person_t[:name].matches("%#{person_name}%")
  end

  def amount_filter
    if filter_type.present? && filter_amount.present?
      "amount_cents #{filter_type} #{parse_filter_amount}"
    else
      ""
    end
  end

  def payout_type_filter
    return "" if withdrawal_method.blank?

    { withdraw_method: withdrawal_method }
  end

  def currency_filter
    return "" if currency.blank?

    { currency: currency }
  end

  def rollback_filter
    { rolled_back: rollback.present? }
  end

  def auto_approval_eligible_filter(relation)
    case auto_approval_eligible
    when "yes"
      relation.with_auto_approval_eligible_transactions
    when "no"
      relation.without_auto_approval_eligible_transactions
    else
      relation
    end
  end

  def parse_filter_amount
    filter_amount.to_f * 100
  end

  def form_tunecore_status
    tunecore_status.presence || PayoutTransfer::TUNECORE_LIFECYCLE
  end

  def form_provider_status
    provider_status.presence || PayoutTransfer::PROVIDER_LIFECYCLE
  end
end
