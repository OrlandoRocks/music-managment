class ArtistRoleBuilder
  include ArelTableMethods
  def self.from_role_ids(role_ids)
    new(role_ids).from_role_ids
  end

  attr_reader :role_ids

  def initialize(role_ids)
    @role_ids = role_ids
  end

  def from_role_ids
    return [] if role_ids.blank?

    song_roles = SongRole.where(id: role_ids, user_defined: true)
    ddex_roles = DDEXRole.joins(ddex_song_roles: :song_role)
                         .where(song_role_t[:id].in(role_ids)
                   .and(ddex_role_t[:role_type].not_in(song_roles.map(&:role_type))))
    ddex_roles + song_roles
  end
end
