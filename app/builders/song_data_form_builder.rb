class SongDataFormBuilder
  def self.song_data_forms_for(album_id, person_id)
    Song.includes(:lyric, :s3_asset, album: :primary_genre).where(album_id: album_id).map do |song|
      SongDataForm.build(person_id: person_id, song: song, album_id: album_id)
    end
  end

  def self.new_song_form_for(album_id, person_id)
    SongDataForm.build(person_id: person_id, album_id: album_id)
  end
end
