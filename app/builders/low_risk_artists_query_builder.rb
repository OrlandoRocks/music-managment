class LowRiskArtistsQueryBuilder
  include ArelTableMethods
  include ArelCommonJoins

  def self.build
    new.build
  end

  def build
    Album
      .select(selects)
      .joins(:person, :low_risk_account, :songs)
      .joins(albums_to_artists_join)
      .joins(blacklisted_album_artist_join)
      .joins(songs_to_artists_outer_join)
      .joins(blacklisted_song_artist_join)
      .where(needs_review)
      .group(album_t[:id])
      .having(blacklisted_album_artist_t[:id].count.eq(0))
      .having(blacklisted_song_artist_t[:id].count.eq(0))
  end

  private

  def needs_review
    album_t[:legal_review_state].eq("NEEDS REVIEW")
  end

  def selects
    [
      album_t[:id],
      album_t[:name].as("album_title"),
      group_concat_distinct([album_artist_t[:name]]).as("album_artist_names"),
      group_concat_distinct([song_artist_t[:name]]).as("song_artist_names"),
      album_t[:finalized_at],
      person_t[:name].as("person_name"),
      person_t[:id].as("person_id")
    ]
  end

  def blacklisted_album_artist_join
    album_artist_t.create_join(
      blacklisted_album_artist_t,
      album_artist_t.create_on(
        blacklisted_album_artist_t[:artist_id].eq(album_artist_t[:id])
        .and(blacklisted_album_artist_t[:active].eq(true))
      ),
      Arel::Nodes::OuterJoin
    )
  end

  def blacklisted_song_artist_join
    song_artist_t.create_join(
      blacklisted_song_artist_t,
      song_artist_t.create_on(
        blacklisted_song_artist_t[:artist_id].eq(song_artist_t[:id])
        .and(blacklisted_song_artist_t[:active].eq(true))
      ),
      Arel::Nodes::OuterJoin
    )
  end

  def blacklisted_album_artist_t
    blacklisted_artist_t.alias("blacklisted_album_artist")
  end

  def blacklisted_song_artist_t
    blacklisted_artist_t.alias("blacklisted_song_artist")
  end
end
