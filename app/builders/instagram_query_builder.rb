class InstagramQueryBuilder
  include ArelTableMethods
  extend ArelTableMethods
  include ArelCommonJoins

  def self.eligible_albums
    new.eligible_albums
  end

  def self.eligible_needing_backfill
    new.eligible_needing_backfill
  end

  def self.eligible_by_person(person_id)
    new.by_person(person_id)
  end

  def build
    Album.select(
      select_statement
    ).joins(
      albums_petri_bundle_join
    ).joins(
      petri_bundle_distribution_join
    ).joins(
      album_song_count_join
    ).joins(
      album_track_mons_join
    ).group(
      album_t[:id]
    ).having(
      song_count_subquery[:count].eq(track_mons_count_subquery[:track_mons_count])
    )
  end

  def select_statement
    [
      album_t[Arel.star],
      song_count_subquery[:count].as("album_songs"),
      track_mons_count_subquery[:track_mons_count].as("monetized_tracks"),
    ]
  end

  def base_eligibility_conditions
    album_t[:legal_review_state].eq(
      "APPROVED"
    ).and(
      album_t[:payment_applied].eq(true)
    ).and(
      album_t[:finalized_at].not_eq(nil)
    ).and(
      album_t[:takedown_at].eq(nil)
    ).and(
      album_t[:album_type].not_eq("Ringtone")
    ).and(
      petri_bundle_t[:state].not_eq("dismissed")
    )
  end

  def eligible_albums
    build.where(base_eligibility_conditions)
  end

  def eligible_needing_backfill
    build.joins(albums_ig_salepoints_outer_join).where(base_eligibility_conditions).where(no_salepoint)
  end

  def by_person(person_id)
    eligible_albums.where(album_t[:person_id].eq(person_id))
  end

  def no_salepoint
    salepoint_t[:id].eq(nil)
  end

  def albums_ig_salepoints_outer_join
    album_t.join(
      salepoint_t, Arel::Nodes::OuterJoin
    ).on(
      album_t[:id].eq(salepoint_t[:salepointable_id]).and(salepoint_t[:salepointable_type].eq("Album").and(salepoint_t[:store_id].eq(Store::IG_STORE_ID)))
    ).join_sources
  end

  def album_song_count_join
    album_t.join(
      song_count_subquery
    ).on(
      song_count_subquery[:album_id].eq(album_t[:id])
    ).join_sources
  end

  def song_count_subquery
    song_t.project(
      song_t[:album_id],
      song_t[:id].count.as("count")
    ).group(song_t[:album_id]).as("songs_count")
  end

  def album_track_mons_join
    album_t.join(
      track_mons_count_subquery
    ).on(
      album_t[:id].eq(track_mons_count_subquery[:album_id])
    ).join_sources
  end

  def track_mons_count_subquery
    track_monetization_t.project(
      song_t[:album_id],
      track_monetization_t[:id].count.as("track_mons_count")
    ).join(
      song_t
    ).on(
      song_t[:id].eq(track_monetization_t[:song_id])
    ).where(
      track_monetization_t[:store_id].eq(Store::FBTRACKS_STORE_ID)
      .and(track_monetization_t[:eligibility_status].eq("eligible"))
      .and(track_monetization_t[:state].in(["delivered", "enqueued", "gathering_assets", "packaged"]))
      .and(track_monetization_t[:takedown_at].eq(nil))
    ).group(song_t[:album_id]).as("track_mons")
  end
end
