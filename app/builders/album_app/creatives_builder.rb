class AlbumApp::CreativesBuilder
  include ArelTableMethods

  def self.build(album_id)
    new(album_id).build
  end

  def initialize(album_id)
    @album_id = album_id
  end

  def build
    Creative
      .joins(:artist)
      .where(creatives: { creativeable_id: @album_id, creativeable_type: "Album" })
      .select(
        [
          creative_t[:id].as("creative_id"),
          artist_t[:name].as("artist_name"),
          creative_t[:role].as("role")
        ]
      )
  end
end
