class PublishingComposer::SearchQueryBuilder
  SEARCH_FIELD_OPTIONS = {
    Name: "name",
    Email: "email",
    PID: "person_id",
    SWID: "id"
  }.freeze

  def self.build(params)
    new.build_query(params)
  end

  def build_query(params)
    @params = params

    valid_params? ? results : initial_query
  end

  def initial_query
    PublishingComposer.is_paid.joins(:lod, :person).order("lods.last_status_at desc")
  end

  private

  def results
    @query = initial_query
    @search_text = @params[:search_text].to_s.strip

    send("#{@params[:search_field]}_query")
  end

  def valid_params?
    return false unless acceptable_search_field_present?
    return false if @params[:search_text].blank?

    true
  end

  def acceptable_search_field_present?
    SEARCH_FIELD_OPTIONS.value?(@params[:search_field].to_s)
  end

  def name_query
    query_string = "
      publishing_composers.first_name like :search_text or
      publishing_composers.middle_name like :search_text or
      publishing_composers.last_name like :search_text or
      concat(publishing_composers.first_name, ' ', publishing_composers.last_name) like :search_text or
      concat(publishing_composers.first_name, ' ', publishing_composers.middle_name, ' ', publishing_composers.last_name) like :search_text
    "

    @query.where(query_string, search_text: "%#{@search_text}%")
  end

  def email_query
    @query.where("publishing_composers.email like ?", "%#{@search_text}%")
  end

  def person_id_query
    @query.where("publishing_composers.person_id = ?", @search_text)
  end

  def id_query
    @query.where("publishing_composers.id = ?", @search_text)
  end
end
