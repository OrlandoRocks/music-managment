class TrackMonetization::SearchQueryBuilder
  SEARCH_FIELD_OPTIONS = {
    "Account ID" => "person_id",
    "Account Email" => "person_email",
    "Account Name" => "person_name",
    "Song Name" => "song_name",
    "Song ISRC" => "song_isrc",
    "Release UPC" => "album_upc",
    "Release Name" => "album_name",
    "Artist Name" => "artist_name"
  }.freeze

  PAGINATION_OPTIONS = [10, 25, 50, 100].freeze

  def self.build(params)
    new.build_query(params)
  end

  def build_query(params)
    @params = params
    @initial_query = build_initial_query

    valid_search_params? ? results : @initial_query
  end

  def build_initial_query
    TrackMonetization.includes(:song, album: [:upcs], person: [:artist])
                     .distinct
                     .paginate(page: @params[:page] || 1, per_page: (@params[:per_page] || PAGINATION_OPTIONS[0]).to_i)
                     .order(created_at: :desc)
                     .where(eligibility_status: eligibility_status, store_id: store_id)
  end

  private

  def eligibility_status
    @eligibility_status ||=
      @params[:eligibility_status].presence || TrackMonetization::ELIGIBILITY_STATUSES
  end

  def store_id
    @store_id ||= @params[:store_id] || Store::TRACK_MONETIZATION_STORES
  end

  def results
    @query = @initial_query
    @search_text = @params[:search_text].to_s.strip

    send("#{@params[:search_field]}_query")
  end

  def valid_search_params?
    return false unless acceptable_search_field_present?
    return false if @params[:search_text].blank?

    true
  end

  def acceptable_search_field_present?
    SEARCH_FIELD_OPTIONS.value?(@params[:search_field].to_s)
  end

  def person_id_query
    @query.joins(:person).where("people.id = ?", @search_text)
  end

  def person_email_query
    @query.joins(:person).where("people.email LIKE ?", "%#{@search_text}%")
  end

  def person_name_query
    @query.joins(:person).where("people.name LIKE ?", "%#{@search_text}%")
  end

  def song_name_query
    @query.joins(:song).where("songs.name LIKE ?", "%#{@search_text}%")
  end

  def song_isrc_query
    @query.joins(:song).where(
      "songs.tunecore_isrc LIKE :search_text OR songs.optional_isrc LIKE :search_text",
      search_text: "%#{@search_text}%"
    )
  end

  def album_upc_query
    @query.joins(album: [:upcs]).where("upcs.number LIKE ?", "%#{@search_text}%")
  end

  def album_name_query
    @query.joins(:album).where("albums.name LIKE ?", "%#{@search_text}%")
  end

  def artist_name_query
    @query.joins(person: [:artist]).where("artists.name LIKE ?", "%#{@search_text}%")
  end
end
