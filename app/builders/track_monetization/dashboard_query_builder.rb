class TrackMonetization::DashboardQueryBuilder
  def self.build(person, store_id)
    new.track_data_for(person, store_id)
  end

  def track_data_for(person, store_id)
    person.track_monetizations
          .where(store_id: store_id)
          .eligible_or_blocked
          .select(dashboard_columns)
          .joins(*table_joins)
          .joins(tunecore_upc_join, optional_upc_join)
          .joins(album_creative_join, album_artist_join)
          .joins(song_creative_join)
          .group("track_id")
  end

  private

  def table_joins
    { song: :album }
  end

  def album_creative_join
    join_on = album_table.create_on(
      album_creative_table[:creativeable_id].eq(album_table[:id])
        .and(album_creative_table[:creativeable_type].eq("Album"))
        .and(album_creative_table[:role].eq("primary_artist"))
    )

    album_table.create_join(album_creative_table, join_on, Arel::Nodes::OuterJoin)
  end

  def song_creative_join
    join_on = song_table.create_on(
      song_creative_table[:creativeable_id].eq(song_table[:id])
        .and(song_creative_table[:creativeable_type].eq("Song"))
        .and(song_creative_table[:role].eq("primary_artist"))
    )

    song_table.create_join(song_creative_table, join_on, Arel::Nodes::OuterJoin)
  end

  def album_artist_join
    join_on = album_creative_table.create_on(
      artist_table[:id].eq(album_creative_table[:artist_id])
    )

    album_creative_table.create_join(artist_table, join_on, Arel::Nodes::OuterJoin)
  end

  def tunecore_upc_join
    join_on = album_table.create_on(
      tc_upc_table[:upcable_id].eq(album_table[:id])
        .and(tc_upc_table[:upcable_type].eq("Album"))
        .and(tc_upc_table[:upc_type].eq("tunecore"))
        .and(tc_upc_table[:inactive].eq(false))
    )

    album_table.create_join(tc_upc_table, join_on)
  end

  def optional_upc_join
    join_on = album_table.create_on(
      opt_upc_table[:upcable_id].eq(album_table[:id])
        .and(opt_upc_table[:upcable_type].eq("Album"))
        .and(opt_upc_table[:upc_type].eq("optional"))
        .and(opt_upc_table[:inactive].eq(false))
    )

    album_table.create_join(opt_upc_table, join_on, Arel::Nodes::OuterJoin)
  end

  def dashboard_columns
    [
      track_table[:id].as("track_id"),
      track_table[:state],
      track_table[:tc_distributor_state],
      track_table[:eligibility_status],
      track_table[:takedown_at],
      Arel::Nodes::NamedFunction.new(
        "COALESCE",
        [
          Arel::Nodes::NamedFunction.new("NULLIF", [song_table[:optional_isrc], Arel::Nodes.build_quoted("")]),
          song_table[:tunecore_isrc]
        ],
        "isrc"
      ),
      song_table[:id].as("song_id"),
      song_table[:name].as("song_name"),
      Arel::Nodes::NamedFunction.new(
        "COALESCE",
        [artist_table[:name], Arel::Nodes.build_quoted(I18n.t(:various_artists))],
        "artist_name"
      ),
      album_table[:name].as("album_name"),
      Arel::Nodes::NamedFunction.new(
        "COALESCE",
        [opt_upc_table[:number], tc_upc_table[:number]],
        "upc"
      ),
      track_table[:created_at],
      track_table[:store_id]
    ]
  end

  def album_table
    Album.arel_table
  end

  def artist_table
    Artist.arel_table
  end

  def album_creative_table
    Creative.arel_table.alias("album_creative")
  end

  def song_creative_table
    Creative.arel_table.alias("song_creative")
  end

  def song_table
    Song.arel_table
  end

  def track_table
    TrackMonetization.arel_table
  end

  def tc_upc_table
    Upc.arel_table.alias("tunecore_upcs")
  end

  def opt_upc_table
    Upc.arel_table.alias("optional_upcs")
  end
end
