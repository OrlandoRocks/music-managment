class ServiceForArtists::QueryBuilder
  def initialize(person_id, service_name)
    validate_service(service_name)
    @person_id = person_id
    @service = service_name
  end

  def artists_with_service_ids
    scope = Artist
            .joins(creatives: :external_service_ids)

    scope = filter_blacklists(scope)

    scope
      .merge(creatives_filter)
      .where(external_service_ids_filter)
      .distinct
      .select(artist_selection)
  end

  def albums_with_service_ids
    scope = Artist
            .joins(albums: :external_service_ids)

    scope = filter_blacklists(scope)

    scope
      .merge(creatives_filter)
      .where(album_external_filter)
      .distinct
      .select(album_selection)
  end

  def artists_with_album_ids
    scope = Artist
            .joins(:albums)
            .joins(creatives: :external_service_ids)

    scope = filter_blacklists(scope)

    scope = albums_with_identifiers_filter(scope)

    scope
      .merge(creatives_filter)
      .where(external_service_ids_filter)
      .distinct
      .select(artist_selection)
  end

  private

  def creatives_filter
    base_scope = Creative.where(person_id: @person_id, creativeable_type: "Album")

    case @service
    when ExternalServiceId::APPLE_SERVICE
      base_scope.where(role: "primary_artist")
    when ExternalServiceId::SPOTIFY_SERVICE
      base_scope.primary_featuring_or_remixer
    when ExternalServiceId::AMAZON_SERVICE
      base_scope.primary_featuring_or_remixer
    end
  end

  def external_service_ids_filter
    [
      "external_service_ids.service_name = '#{@service}'",
      "external_service_ids.identifier IS NOT NULL",
      "external_service_ids.identifier <> 'unavailable'"
    ].join(" AND ")
  end

  def album_external_filter
    filter = external_service_ids_filter

    filter.concat(" AND external_service_ids.linkable_type = 'Album'")
  end

  def artist_selection
    "artists.name as artist_name, external_service_ids.identifier as identifier"
  end

  def album_selection
    "albums.name as album_name, external_service_ids.identifier as identifier"
  end

  def albums_with_identifiers_filter(scope)
    scope
      .joins(
        "INNER JOIN external_service_ids as al_ids
        ON al_ids.linkable_id = albums.id
        AND al_ids.linkable_type = 'Album'
        AND al_ids.service_name = '#{@service}'
        AND al_ids.identifier <> 'unavailable'
        AND al_ids.identifier IS NOT NULL"
      )
  end

  def validate_service(service_name)
    services = [
      ExternalServiceId::APPLE_SERVICE,
      ExternalServiceId::SPOTIFY_SERVICE,
      ExternalServiceId::AMAZON_SERVICE,
    ]

    raise "Unconfigured service passed to query builder!" unless services.include?(service_name)
  end

  def service_store_map
    {
      ExternalServiceId::SPOTIFY_SERVICE => Store.find_by(abbrev: "sp").id
    }
  end

  def filter_blacklists(scope)
    store_id = service_store_map[@service]

    scope = scope
            .joins("LEFT JOIN blacklisted_artists ba ON ba.artist_id = artists.id")

    filter = ["(ba.id IS NULL OR ba.active = false)"]

    if store_id.present?
      scope = scope
              .joins("LEFT JOIN artist_store_whitelists wl ON wl.artist_id = artists.id AND wl.store_id = #{store_id}")

      filter << "(wl.id IS NOT NULL)"
    end

    scope.where(filter.join(" || "))
  end
end
