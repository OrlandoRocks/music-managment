class PersonSerializer < ApplicationSerializer
  attributes :email, :plan, :balance

  delegate :plan, to: :object

  delegate :balance, to: :object
end
