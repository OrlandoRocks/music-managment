class Sift::BaseSerializer < FormattedKeySerializer
  attr_reader :object, :options

  def initialize(object, options = {})
    object[:key_formats] ||= []
    object[:key_formats] += default_sift_key_formats

    object[:remove_blanks] ||= true

    super object, options

    @object = object
    @options = options
  end

  def as_json
    pre_validate_instance

    json = ActiveSupport::HashWithIndifferentAccess.new(super)

    post_validate_output(json)

    json
  end

  private

  # Extend these for validations
  def required_fields
    nil
  end

  def required_models
    nil
  end

  def default_sift_key_formats
    [
      FormattedKeySerializer::KeyFormat.new(nil, "__custom", "%s"),
      default_case
    ].freeze
  end

  def default_case
    FormattedKeySerializer::KeyFormat.new(nil, nil, "$%s")
  end

  def pre_validate_instance
    return if required_models.blank?

    required_models.each do |field|
      next if instance_variable_defined?("@#{field}")

      raise Sift::InvalidObjectError.new("Serializer is missing the instance variable #{field}")
    end
  end

  def post_validate_output(json)
    return if required_fields.blank?

    required_fields.each do |field|
      next if json[field].present?

      raise Sift::SerializationError.new("Serialized Output is missing #{field}", json)
    end
  end
end
