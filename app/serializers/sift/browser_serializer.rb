# frozen_string_literal: true

class Sift::BrowserSerializer < Sift::BaseSerializer
  attr_reader :request, :cookies

  attributes :user_agent,
             :accept_language,
             :content_language

  def initialize(object, options = {})
    super object, options

    @request = object[:request] || {}
    @cookies = object[:cookies] || {}
  end

  def user_agent
    return if request.env.blank?

    request.env["HTTP_USER_AGENT"]
  end

  def accept_language
    return if request.env.blank?

    request.env["HTTP_ACCEPT_LANGUAGE"]
  end

  def content_language
    return if cookies.blank?

    cookies["tc-lang"]
  end
end
