class Sift::CreateAccountSerializer < Sift::EventBodySerializer
  attributes :user_id,
             :name,
             :user_email,
             :mobile_phone__custom,
             :corporate_entity__custom,
             :releases_active__custom,
             :releases_taken_down__custom,
             :session_id,
             :browser

  REQUIRED_FIELDS = [
    :$user_id,
    :$user_email,
    :$name
  ]
  REQUIRED_MODELS = [
    :person
  ]

  def initialize(object, options = {})
    super object, options
  end

  def mobile_phone__custom
    return unless mobile_number_valid?(person.mobile_number)

    person.mobile_number
  end

  private

  def required_fields
    REQUIRED_FIELDS
  end

  def required_models
    REQUIRED_MODELS
  end
end
