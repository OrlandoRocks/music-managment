class Sift::CreateOrderSerializer < Sift::EventBodySerializer
  attr_reader :finalize_form,
              :purchases,
              :payment_id,
              :use_balance

  attributes :user_id,
             :user_email,
             :order_id,
             :amount,
             :currency_code,
             :items,
             :payment_methods,
             :billing_address,
             :brand_name,
             :site_domain,
             :site_country,
             :is_first_time_buyer__custom,
             :releases_active__custom,
             :releases_taken_down__custom,
             :session_id,
             :browser

  REQUIRED_FIELDS = [
    :$user_id,
    :$order_id,
    :$amount,
    :$items
  ]
  REQUIRED_MODELS = [
    :person,
    :finalize_form,
    :purchases
  ]

  def initialize(object, options = {})
    super object, options

    @finalize_form = object[:finalize_form]
    @use_balance = @finalize_form.use_balance
    @payment_id = @finalize_form.payment_id
    @purchases = @finalize_form.purchases
  end

  def amount
    total_amount_cents * SIFT_CENTS_TO_MICRONS_MULTIPLYER
  end

  def payment_methods
    Sift::PaymentMethodSerializer.serialize_selected(
      person: person, payment_id: payment_id, use_balance: use_balance
    )
  end

  def brand_name
    "Tunecore".freeze
  end

  def site_domain
    return if request.blank?

    request.host
  end

  def site_country
    return if person.blank?

    person.country_website.country_iso
  end

  def items
    return [] if purchases.blank?

    Sift::PurchaseGroupSerializer.serialize_purchases(purchases)
  end

  # rubocop:disable Naming/PredicateName
  # meta-programming: sift expects is_first_time_buyer
  def is_first_time_buyer__custom
    Invoice.where(person_id: person.id).where.not(settled_at: nil).count.zero?
  end
  # rubocop:enable Naming/PredicateName

  private

  def required_fields
    REQUIRED_FIELDS
  end

  def required_models
    REQUIRED_MODELS
  end
end
