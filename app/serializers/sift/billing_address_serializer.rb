class Sift::BillingAddressSerializer < Sift::BaseSerializer
  attr_reader :billing_address_params, :credit_card, :compliance_info

  # rubocop:disable Naming/VariableNumber
  # meta-programming: sift expects $address_1 and $address_2
  attributes :name,
             :phone,
             :address_1,
             :address_2,
             :city,
             :region,
             :country,
             :zipcode
  # rubocop:enable Naming/VariableNumber

  def initialize(object, options = {})
    super object, options

    @credit_card = object[:credit_card] || {}
    @billing_address_params = object[:billing_address_params] || @credit_card || {}
    @compliance_info = @billing_address_params[:compliance_info] || {}
  end

  def name
    return [credit_card[:first_name], credit_card[:last_name]].join(" ") if credit_card.present?
    return if compliance_info[:first_name].blank? || compliance_info[:last_name].blank?

    [compliance_info[:first_name], compliance_info[:last_name]].join(" ")
  end

  def phone
    billing_address_params[:phone_number]
  end

  # rubocop:disable Naming/VariableNumber
  # meta-programming: sift expects $address_1 and $address_2
  def address_1
    billing_address_params[:address1]
  end

  def address_2
    billing_address_params[:address2]
  end
  # rubocop:enable Naming/VariableNumber

  def city
    billing_address_params[:city]
  end

  def region
    billing_address_params[:state]
  end

  def country
    billing_address_params[:country]
  end

  def zipcode
    billing_address_params[:zip] || billing_address_params[:postal_code]
  end
end
