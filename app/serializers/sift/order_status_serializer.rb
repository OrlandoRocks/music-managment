class Sift::OrderStatusSerializer < Sift::EventBodySerializer
  attr_reader :order_status

  attributes  :user_id,
              :order_id,
              :order_status

  REQUIRED_FIELDS = [
    :$user_id,
    :$order_id,
    :$order_status
  ]
  REQUIRED_MODELS = [
    :person,
    :order_status
  ]

  def initialize(object, options = {})
    super object, options

    @order_status = object[:order_status]
  end

  def order_id
    person&.person_sift_score&.sift_order_id
  end

  private

  def required_fields
    REQUIRED_FIELDS
  end

  def required_models
    REQUIRED_MODELS
  end
end
