class Sift::UpdateAccountSerializer < Sift::EventBodySerializer
  attr_reader :params,
              :update_type

  attributes :user_id,
             :name,
             :user_email,
             :changed_password,
             :payment_methods,
             :billing_address,
             :phone,
             :mobile_phone__custom,
             :corporate_entity__custom,
             :releases_active__custom,
             :releases_taken_down__custom,
             :session_id,
             :browser

  REQUIRED_FIELDS = [
    :$user_id
  ]
  REQUIRED_MODELS = [
    :person,
    :params,
    :update_type
  ]

  def initialize(object, options = {})
    super object, options

    @params = object[:params] || {}
    @update_type = object[:update_type]
  end

  def name
    return unless update_type == :contact_info

    params[:name]
  end

  def phone
    return unless update_type == :contact_info

    super
  end

  def user_email
    return unless update_type == :email

    super
  end

  def changed_password
    return unless update_type == :password

    true
  end

  def payment_methods
    return unless update_type == :payment_methods && person.present?

    Sift::PaymentMethodSerializer.serialize_all(person: person)
  end

  def billing_address
    return unless update_type == :contact_info && params.present?
    return if params[:billing_address].blank?

    Sift::BillingAddressSerializer.new({ billing_address_params: params[:billing_address] }).as_json
  end

  def mobile_phone__custom
    return unless update_type == :contact_info && params.present?
    return unless mobile_number_valid?(params[:mobile_number])

    params[:mobile_number]
  end

  private

  def required_fields
    REQUIRED_FIELDS
  end

  def required_models
    REQUIRED_MODELS
  end
end
