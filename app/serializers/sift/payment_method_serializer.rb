# frozen_string_literal: true

class Sift::PaymentMethodSerializer < Sift::BaseSerializer
  attr_reader :payment_method, :payment_method_type, :payment_id

  attributes :payment_type,
             :payment_gateway,
             :paypal_payer_email,
             :card_last4,
             :card_type__custom

  def self.serialize_all(person:)
    return if person.blank?

    payment_methods = []

    person.stored_credit_cards.active.each do |card|
      payment_methods << Sift::PaymentMethodSerializer.new(
        {
          payment_method_type: :credit_card,
          payment_method: card
        }
      ).as_json
    end

    person.stored_paypal_accounts.active.each do |paypal_account|
      payment_methods << Sift::PaymentMethodSerializer.new(
        {
          payment_method_type: :paypal,
          payment_method: paypal_account
        }
      ).as_json
    end

    payment_methods
  end

  def self.serialize_selected(person:, payment_id:, use_balance:)
    payment_methods = []

    payment_methods << serialize(person: person, payment_id: nil, use_balance: true) if use_balance
    payment_methods << serialize(
      person: person, payment_id: payment_id,
      use_balance: false
    ) if payment_id.present? || using_payu?(
      payment_id, person
    )

    payment_methods
  end

  def self.serialize(person:, payment_id:, use_balance:)
    return if person.blank?

    payment_method_type = payment_method_type?(person, payment_id, use_balance)

    payment_method = StoredCreditCard.find_by(id: payment_id) if payment_method_type == :credit_card
    payment_method = person.stored_paypal_accounts.active.first if payment_method_type == :paypal

    new({ payment_method_type: payment_method_type, payment_method: payment_method }).as_json
  end

  def self.payment_method_type?(person, payment_id, use_balance)
    if use_balance
      payment_method_type = :balance
    else
      payment_method_type = :credit_card if using_credit_card?(payment_id)
      payment_method_type = :adyen if using_adyen_gateway?(payment_id)
      payment_method_type = :payu if using_payu?(payment_id, person)
      payment_method_type = payment_id.to_sym if payment_id.present? && payment_method_type.blank?
    end

    payment_method_type
  end

  def self.using_adyen_gateway?(payment_id)
    return if payment_id.blank?

    payment_id.to_s.split("-")[0] == "adyen"
  end

  # payment_id for credit_cards can be a Integer or a String Integer
  def self.using_credit_card?(payment_id)
    payment_id.present? && payment_id.to_s.match("^[0-9]+$")
  end

  def self.using_payu?(payment_id, person)
    payment_id.blank? && CountryWebsite::PAYMENTS_OS_COUNTRIES.include?(person.country_domain)
  end

  def initialize(object, options = {})
    super(object, options)

    @person = object[:person]
    @payment_method_type = object[:payment_method_type]
    @payment_method = object[:payment_method]
    @payment_id = object[:payment_id]

    return if @payment_method.present?

    @payment_method = StoredCreditCard.find(Integer(@payment_id)) if @payment_method_type == :credit_card
    @payment_method = @person.stored_paypal_accounts.active.first if @payment_method_type == :paypal && @person.present?
  end

  def as_json
    return if payment_method_type.blank?

    super
  end

  def payment_type
    return "$store_credit" if payment_method_type == :balance
    return "$third_party_processor" if [:paypal, :adyen, :payu].include?(payment_method_type)

    "$credit_card"
  end

  def payment_gateway
    return if payment_method_type == :balance
    return "$#{payment_method_type}" if [:paypal, :adyen, :payu].include? payment_method_type

    "$braintree"
  end

  def paypal_payer_email
    return unless payment_method.present? && payment_method_type == :paypal

    payment_method.email
  end

  def card_last4
    return unless payment_method.present? && payment_method_type == :credit_card

    payment_method.last_four
  end

  def card_type__custom
    return unless payment_method.present? && payment_method_type == :credit_card

    payment_method.cc_type
  end
end
