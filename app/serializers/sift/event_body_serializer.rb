class Sift::EventBodySerializer < Sift::BaseSerializer
  attr_reader :person,
              :request,
              :cookies,
              :controller_action

  def self.serialize(serializer_class, object, options = {})
    return if serializer_class.blank?

    begin
      return serializer_class.new(object, options).as_json
    rescue Sift::InvalidObjectError => e
      log_error(
        e,
        "#{serializer_class} - The object provided is invalid for this serializer.",
        serializer_class,
        object,
        options
      )
    rescue Sift::SerializationError => e
      log_error(e, "#{serializer_class} - There was an error during serialization.", serializer_class, object, options)
    rescue => e
      log_error(e, "#{serializer_class} - An unknown error has occurred.", serializer_class, object, options)
    end

    {}
  end

  def self.create_account_body(person:, cookies:, request:)
    serialize(
      Sift::CreateAccountSerializer,
      {
        person: person,
        cookies: cookies,
        request: request
      }
    )
  end

  def self.order_status_body(person:, order_status:, cookies:, request:)
    return {} if order_status == "$canceled" &&
                 (person&.person_sift_score&.pending_v1_order? || person&.person_sift_score&.sift_order_id.blank?)

    serialize(
      Sift::OrderStatusSerializer,
      {
        person: person,
        order_status: order_status,
        cookies: cookies,
        request: request
      }
    )
  end

  def self.update_account_body(person:, params: {}, update_type:, cookies:, request:)
    serialize(
      Sift::UpdateAccountSerializer,
      {
        person: person,
        params: params,
        update_type: update_type,
        cookies: cookies,
        request: request
      }
    )
  end

  # rubocop:disable Lint/MissingCopEnableDirective
  # rubocop:disable Metrics/ParameterLists
  def self.transaction_bodies(person:, invoice_id:, finalize_form:, transaction_status:, cookies:, request:)
    serialize(
      Sift::TransactionSerializer,
      {
        person: person,
        finalize_form: finalize_form,
        invoice_id: invoice_id,
        transaction_status: transaction_status,
        cookies: cookies,
        request: request
      }
    )
  end
  # rubocop:enable Metrics/ParameterLists
  # rubocop:enable Lint/MissingCopEnableDirective

  def self.create_order_body(finalize_form:, cookies:, request:)
    serialize(
      Sift::CreateOrderSerializer,
      {
        person: finalize_form.person,
        finalize_form: finalize_form,
        cookies: cookies,
        request: request
      }
    )
  end

  def self.log_error(error, message, serializer_class, object, _options)
    error_options = {
      serializer: serializer_class.to_s,
      person_id: object[:person]&.id || object[:finalize_form]&.person&.id,
      error_message: error.message,
      stack_trace: error.backtrace.join("\n")
    }
    error_options.merge({ json: error.json }) if error.respond_to?(:json)

    Tunecore::Airbrake.notify(message, error_options)
  end

  private_class_method :log_error

  def initialize(object, options = {})
    super object, options

    @person = object[:person] || @finalize_form&.person
    @request = object[:request]
    @cookies = object[:cookies]
    @controller_action = request[:action]
  end

  def user_id
    return if person.blank?

    person.id.to_s
  end

  delegate :name, to: :person, allow_nil: true

  def user_email
    return if person.blank?

    person.email
  end

  def phone
    return if person.blank?

    person.phone_number
  end

  def order_id
    return if person.blank? || purchases.blank?

    purchase_hash = Digest::SHA1.hexdigest("purchases_#{purchases.pluck(:id).join('_')}")
    "v2-person_id-#{person.id}-#{purchase_hash}"
  end

  def currency_code
    return if purchases.blank?

    purchases.first.currency
  end

  def corporate_entity__custom
    return if person.blank?

    person.corporate_entity.try(:name)
  end

  def billing_address
    return unless using_credit?

    card = StoredCreditCard.find(payment_id)

    Sift::BillingAddressSerializer.new({ credit_card: card }).as_json
  end

  def releases_active__custom
    return 0 if person.blank?

    person.albums.distributed.approved.not_taken_down.count
  end

  def releases_taken_down__custom
    return 0 if person.blank?

    person.albums.distributed.taken_down.count
  end

  def session_id
    return if cookies.blank?

    cookies["tc-sift-session-id"]
  end

  def browser
    return unless cookies.present? && request.present?

    Sift::BrowserSerializer.new({ cookies: cookies, request: request }).as_json
  end

  private

  def total_amount_cents
    return 0 if purchases.blank?

    purchases.pluck(:cost_cents).sum +
      purchases.pluck(:vat_amount_cents).sum +
      purchases.pluck(:vat_amount_cents_in_eur).sum -
      purchases.pluck(:discount_cents).sum
  end

  def mobile_number_valid?(mobile_number)
    return true if mobile_number.blank?

    mobile_number.match(/^[-0-9+\(\)#\/ ]{0,25}$/).present?
  end

  def pending?
    transaction_status == "$pending"
  end

  def failure?
    transaction_status == "$failure"
  end

  def using_credit?
    Sift::PaymentMethodSerializer.using_credit_card?(payment_id)
  end
end
