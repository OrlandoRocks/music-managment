class Sift::TransactionSerializer < Sift::EventBodySerializer
  attr_accessor :selected_payment_strategy, :transaction_id

  attr_reader :transaction_status,
              :invoice_id,
              :finalize_form,
              :purchases,
              :payment_id,
              :use_payu,
              :use_adyen

  attributes :user_id,
             :user_email,
             :order_id,
             :transaction_id,
             :transaction_type,
             :amount,
             :currency_code,
             :payment_method,
             :billing_address,
             :transaction_status,
             :releases_active__custom,
             :releases_taken_down__custom,
             :session_id,
             :browser

  REQUIRED_FIELDS = [
    :$user_id,
    :$transaction_status,
    :$amount,
    :$currency_code,
    :$order_id,
    :$payment_method
  ]

  REQUIRED_MODELS = [
    :person,
    :purchases,
    :transaction_status
  ]

  def initialize(object, options = {})
    super object, options

    @finalize_form = object[:finalize_form]
    @transaction_status = object[:transaction_status]
    @selected_payment_strategy = nil

    if !pending? && one_time_payment?
      @invoice_id = object[:invoice_id]
      @purchases = Purchase.where(invoice_id: @invoice_id)
      @use_payu = using_payu?
      @use_adyen = using_adyen?
      @payment_id = "payu" if @use_payu
      @payment_id = "adyen" if @use_adyen
    else
      @use_balance = @finalize_form.use_balance
      @payment_id = @finalize_form.payment_id
      @purchases = @finalize_form.purchases
      @invoice_id = @purchases[0].invoice_id if !pending? && @purchases.present?
      @use_payu = using_payu?
      @use_adyen = using_adyen?
    end
  end

  def as_json
    bodies = []

    payment_strategies.each do |strategy|
      @selected_payment_strategy = strategy
      @transaction_id = order_id + "_#{strategy[:type]}_transaction"

      bodies << super
    end

    bodies
  end

  def amount
    return 0 unless selected_payment_strategy&.dig(:amount)&.positive?

    selected_payment_strategy[:amount] * SIFT_CENTS_TO_MICRONS_MULTIPLYER
  end

  def payment_method
    return if selected_payment_strategy.blank? || selected_payment_strategy[:type].blank?

    Sift::PaymentMethodSerializer.new(
      {
        person: person,
        payment_method_type: selected_payment_strategy[:type],
        payment_id: selected_payment_strategy[:payment_id]
      }
    ).as_json
  end

  def transaction_type
    "$sale".freeze
  end

  private

  def required_fields
    REQUIRED_FIELDS
  end

  def required_models
    REQUIRED_MODELS
  end

  def one_time_payment?
    finalize_form.blank? || using_adyen? || using_payu?
  end

  def using_adyen?
    return true if !pending? && controller_action == "adyen_finalize_after_redirect"

    payment_id.present? && Sift::PaymentMethodSerializer.using_adyen_gateway?(payment_id)
  end

  def using_payu?
    return true if !pending? && controller_action == "finalize_after_redirect"

    payment_id.blank? && CountryWebsite::PAYMENTS_OS_COUNTRIES.include?(person.country_domain)
  end

  # 5 Strategies
  # Balance, Credit, Paypal, PayU, Adyen
  # Balance is used IF finalize_form.use_balance is true
  #                 OR there exists an associated person transaction with the invoice after payment is processed
  # Credit is used if payment_id is a Number
  # Paypal is used if payment_id is "paypal"
  # PayU is used if payment_id is blank AND
  #                   person.country is in PaymentsOS Country Array
  #                 OR Controller_Action == "finalize_after_redirect"
  # Adyen is used if payment_id contains "adyen" OR Controller_Action == "adyen_finalize_after_redirect"
  def payment_strategies
    if pending?
      @person_balance_cents = @use_balance ? Integer(person.balance * 100) : 0
    elsif failure? && @use_balance
      @person_balance_cents = Integer(@person.balance * 100)
    else
      person_transaction = PersonTransaction.where(target_id: invoice_id, target_type: "Invoice").last
      @use_balance = person_transaction.present?
      @person_balance_cents = @use_balance ? Integer(person_transaction.debit * 100) : 0
    end

    add_strategies
  end

  def add_strategies
    strategies = []

    balance_applied = [total_amount_cents, @person_balance_cents].min
    amount_sans_balance = total_amount_cents - balance_applied

    strategies << { type: :balance, amount: balance_applied } if @use_balance
    strategies << { type: :credit_card, amount: amount_sans_balance, payment_id: payment_id } if using_credit?
    strategies << {
      type: :paypal,
      amount: amount_sans_balance,
      payment_id: payment_id
    } if ["paypal"].include?(payment_id)
    strategies << { type: :payu, amount: amount_sans_balance } if use_payu
    strategies << { type: :adyen, amount: amount_sans_balance } if use_adyen

    strategies
  end
end
