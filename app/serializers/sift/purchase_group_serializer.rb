class Sift::PurchaseGroupSerializer < Sift::BaseSerializer
  attr_reader :purchase_groups, :purchase_group_count, :index

  attributes :item_id,
             :product_title,
             :price,
             :currency_code,
             :quantity

  def self.serialize_purchases(purchases = [])
    return if purchases.blank?

    purchase_groups = purchases.group(
      [
        "product_id",
        "discount_cents",
        "cost_cents",
        "vat_amount_cents",
        "vat_amount_cents_in_eur"
      ]
    )
    purchase_group_counts = purchase_groups.count

    purchase_group_counts.map.with_index do |purchase_group_count, i|
      new({ purchase_groups: purchase_groups, purchase_group_count: purchase_group_count, index: i }).as_json
    end
  end

  def initialize(object, options = {})
    super object, options

    @purchase_groups = object[:purchase_groups]
    @purchase_group_count = object[:purchase_group_count]
    @index = object[:index]
  end

  def item_id
    purchase_group_count[0][0].to_s
  end

  def product_title
    purchase_groups[index].product.name
    if credit_usage?
      # The first Related is for CreditUsage the second is for Products
      purchase_groups[index].related.related.name
    else
      purchase_groups[index].product.name
    end
  end

  def credit_usage?
    purchase_groups[index]&.related_type == "CreditUsage"
  end

  def price
    (purchase_group_count[0].drop(2).sum - purchase_group_count[0][1]) * SIFT_CENTS_TO_MICRONS_MULTIPLYER
  end

  def currency_code
    purchase_groups[index].currency
  end

  def quantity
    purchase_group_count[1]
  end
end
