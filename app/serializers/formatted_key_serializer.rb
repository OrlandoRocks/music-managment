# Confluence Documentation: https://tunecore.atlassian.net/l/cp/9jwbCeEf
class FormattedKeySerializer < ApplicationSerializer
  attr_reader :key_formats, :remove_blanks

  def initialize(object, options = {})
    super object, options

    @key_formats = object[:key_formats] || []
    @remove_blanks = object[:remove_blanks] || false
  end

  def as_json
    json = super
    root = json.keys[0]

    json[root].each_with_object({}) do |kv, formatted_json|
      next if remove_blanks && kv[1].blank?

      formatted_json[format_key(kv[0])] = kv[1]
    end
  end

  private

  def format_key(key)
    return if key.blank?

    key_formats.each do |key_format|
      return sprintf(key_format.format_string, key).to_sym if key_format.prefix.nil? && key_format.suffix.nil?

      next unless can_format?(key, key_format)

      new_key = format_prefix(key, key_format.prefix)
      new_key = format_suffix(new_key, key_format.suffix)
      return sprintf(key_format.format_string, new_key).to_sym
    end

    key
  end

  def can_format?(key, key_format)
    (key_format.prefix.present? && key.to_s.starts_with?(key_format.prefix)) ||
      (key_format.suffix.present? && key.to_s.end_with?(key_format.suffix))
  end

  def format_prefix(key, prefix)
    return key if prefix.blank?

    key.to_s[prefix.length - 1..-1]
  end

  def format_suffix(key, suffix)
    return key if suffix.blank?

    key.to_s[0..-(suffix.length + 1)]
  end

  class KeyFormat
    attr_reader :prefix, :suffix, :format_string

    def initialize(prefix, suffix, format_string)
      @prefix = prefix
      @suffix = suffix
      @format_string = format_string
    end
  end
end
