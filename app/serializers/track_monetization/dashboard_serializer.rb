# frozen_string_literal: true

class TrackMonetization::DashboardSerializer < ApplicationSerializer
  NEW_TRACK           = "new"
  MONETIZED           = "monetized"
  NOT_MONETIZED       = "not_monetized"
  PROCESSING          = "processing"
  PROCESSING_TAKEDOWN = "processing_takedown"
  BLOCKED             = "blocked"

  LEGACY_FB_END_DATE = ENV["FB_FREEMIUM_DATE"] || "2021-06-14"
  LEGACY_YTSR_END_DATE = ENV.fetch("YTSR_DISCOVERY_DATE", 10.days.from_now)

  CAN_ENQUEUE = ::Distribution::Stateful::CAN_ENQUEUE.map(&:to_s).freeze

  attributes :id, :song, :isrc, :artist, :appears_on, :upc, :status, :discovery, :tc_distributor_state

  def id
    object.track_id
  end

  def song
    object.song_name
  end

  def artist
    object.artist_name
  end

  def appears_on
    object.album_name
  end

  delegate :isrc, :upc, :created_at, :store_id, to: :object

  def current_user
    context[:current_user]
  end

  def status
    return NEW_TRACK if object.state == "new" && !object.takedown_at
    return BLOCKED if object.state == "blocked"

    if object.state.in?(CAN_ENQUEUE)
      if error? || object.takedown_at.present?
        NOT_MONETIZED
      else
        MONETIZED
      end
    else
      object.takedown_at.present? ? PROCESSING_TAKEDOWN : PROCESSING
    end
  end

  def error?
    object.state == "error" || object.tc_distributor_state == "error"
  end

  def discovery
    # false if neither feature flag is on
    return false unless (FeatureFlipper.show_feature?(
      :freemium_flow,
      current_user
    ) || FeatureFlipper.show_feature?(
      :discovery_platforms,
      current_user
    ))

    return true if FeatureFlipper.show_feature?(
      :disable_legacy_youtube_flow,
      current_user
    ) && store_id == Store::YTSR_STORE_ID

    end_date = store_id == Store::FBTRACKS_STORE_ID ? LEGACY_FB_END_DATE : LEGACY_YTSR_END_DATE
    end_date <= created_at
  end
end
