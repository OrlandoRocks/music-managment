class SalepointSerializer < ApplicationSerializer
  attributes :store_id,
             :store_name,
             :price_code,
             :track_price_code,
             :available_for_download

  def store_name
    object.store.name
  end

  # sort this out for real when we cutover Amazon and iTunes
  def available_for_download
    true
  end

  # These are leftover from petri logic.
  # It was decided to not handle this logic anymore since Believe now
  # interacts directly with stores on our behalf now.
  # def excluded_countries(store)
  #   excludes = {
  #     ddex:   ["BQ", "CW", "SX", "SS"],
  #     google: ["BQ", "CW", "SX", "SS", "BL", "MF"],
  #     nokia:  ["BQ", "CW", "SX", "SS", "BL", "MF", "AN", "CS", "ZR"]
  #   }
  #   excludes[store]
  # end
end
