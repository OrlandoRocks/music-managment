class PersonTransactionSerializer < ActiveModel::Serializer
  attributes :date, :description, :debit, :credit, :invoice_id, :currency

  def date
    object.created_at.to_date.to_s
  end

  def description
    case object.target_type
    when "PersonIntake"
      "Sales Posted"
    when "YouTubeRoyaltyIntake"
      "YouTube Royalties Posted"
    else
      object.comment
    end
  end

  def debit
    object.debit.truncate(2).to_s
  end

  def credit
    object.credit.truncate(2).to_s
  end

  def invoice_id
    object.target_id
  end

  delegate :currency, to: :object
end
