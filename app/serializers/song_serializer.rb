class SongSerializer < ApplicationSerializer
  attributes :song_id,
             :number,
             :title,
             :artists,
             :sku,
             :isrc,
             :explicit_lyrics,
             :s3_key,
             :s3_bucket,
             :album_only,
             :free_song,
             :available_for_streaming,
             :asset_url,
             :orig_filename,
             :duration,
             :upload_timestamp

  def song_id
    object.id
  end

  def number
    object.track_num
  end

  def title
    object.name
  end

  def artists
    return [{ name: object.artist_name, role: "primary_artist" }] if object.creatives.empty?

    artist_array = []
    unless object.album.is_various?
      object.album.artists.each do |artist|
        artist_array << { name: artist.name, role: "primary_artist" }
      end
    end

    object.creatives.each do |creative|
      next if object.primary_artist_is_featured?(creative)

      artist_array << { name: creative.artist.name, role: creative.role }
    end

    artist_array
  end

  def explicit_lyrics
    object.parental_advisory?
  end

  def orig_filename
    object.upload.uploaded_filename
  end

  def upload_timestamp
    object.upload.created_at.to_i
  end

  def s3_key
    s3_key = nil
    if object.s3_asset && object.s3_asset.uuid
      s3_key = object.s3_asset.key
    elsif object.s3_orig_asset
      s3_key = object.s3_orig_asset.key
    elsif object.s3_flac_asset
      s3_key = object.s3_flac_asset.key
    end

    s3_key
  end

  def s3_bucket
    s3_bucket = nil
    if object.s3_asset && object.s3_asset.uuid
      s3_bucket = object.s3_asset.bucket
    elsif object.s3_orig_asset
      s3_bucket = object.s3_orig_asset.bucket
    elsif object.s3_flac_asset
      s3_bucket = object.s3_flac_asset.bucket
    end

    s3_bucket
  end

  def asset_url
    object.external_asset_url
  end

  def duration
    object.duration / 1000 if object.duration
  end
end
