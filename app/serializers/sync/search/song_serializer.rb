class Sync::Search::SongSerializer < ApplicationSerializer
  include Sync::SearchHelper

  attributes(
    :id,
    :name,
    :album_id,
    :created_on,
    :songstatus,
    :track_num,
    :tunecore_isrc,
    :optional_isrc,
    :s3_flac_asset_id,
    :s3_orig_asset_id,
    :last_errors,
    :parental_advisory,
    :price_policy_id,
    :upload_error_log,
    :purchase_id,
    :price_override,
    :external_asset_id,
    :s3_asset_id,
    :composition_id,
    :clean_version,
    :content_fingerprint,
    :language_code_id,
    :translated_name,
    :song_version,
    :previously_released_at,
    :made_popular_by,
    :cover_song,
    :artist_name,
    :mech_collect_share,
    :formatted_duration,
    :is_favorite,
    :artwork_url,
    :person_id,
    :album_takedown,
    :composer_names,
    :primary_genre
  )

  def formatted_duration
    convert_duration(object.duration)
  end

  def is_favorite
    if scope.present? && scope.current_user.sync_favorite_songs.include?(object.id)
      "1"
    else
      "0"
    end
  end

  def artwork_url
    if object.album.artwork && object.album.artwork.uploaded?
      sync_artwork_url(object.album.artwork.url(:small))
    else
      ""
    end
  end

  def person_id
    object.album.person.id
  end

  def album_takedown
    object.album.takedown_at.present?
  end

  def composer_names
    object.get_composer_names
  end

  def primary_genre
    object.primary_genre_name
  end
end
