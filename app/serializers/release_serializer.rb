class ReleaseSerializer < ApplicationSerializer
  attributes :tunecore_id,
             :copyright_name,
             :upc,
             :sku,
             :title,
             :copyright_pline,
             :copyright_cline,
             :release_date,
             :genres,
             :artwork_s3_key,
             :actual_release_date,
             :label_name,
             :explicit_lyrics,
             :liner_notes,
             :artists,
             :artwork_file,
             :street_date,
             :booklet_file,
             :delivery_type,
             :release_type,
             :takedown,
             :countries,
             :language,
             :customer_name,
             :customer_id,
             :artist_name,
             :various_artists,
             :excluded_countries

  has_many :songs
  has_many :salepoints

  def tunecore_id
    object.id
  end

  def upc
    object.upc.to_s
  end

  def title
    object.name
  end

  def copyright
    "#{object.orig_release_date.year} #{object.copyright_name}"
  end

  alias_method :copyright_pline, :copyright
  alias_method :copyright_cline, :copyright

  def copyright_year
    object.orig_release_date.year
  end

  def release_date
    object.orig_release_date.strftime("%Y-%m-%d")
  end

  def genres
    object.genres.map(&:name)
  end

  def artwork_s3_key
    object.artwork.s3_asset.key if object.artwork
  end

  def label_name
    object.label.try(:name) || object.artist_name
  end

  def explicit_lyrics
    object.parental_advisory
  end

  def artists
    return [{ name: object.artist_name, role: "primary_artist" }] if object.creatives.empty?

    artist_array = []
    object.creatives.each do |creative|
      artist_array << { name: creative.artist.name, role: creative.role }
    end
    artist_array
  end

  def artwork_file
    return {} unless object.artwork

    { asset: object.artwork.s3_asset.key, bucket: object.artwork.s3_asset.bucket }
  end

  def booklet_file
    if object.booklet
      {
        asset: object.booklet.s3_asset.key,
        bucket: object.booklet.s3_asset.bucket
      }
    else
      {}
    end
  end

  def delivery_type
    takedown ? "metadata_only" : "full_delivery"
  end

  def release_type
    object.class.name
  end

  def takedown
    object.takedown? || !!object.salepoints.detect(&:taken_down?)
  end

  def countries
    object.country_iso_codes # - excluded_countries(:ddex)
  end

  def language
    object.language.description
  end

  def customer_name
    object.person.name
  end

  def customer_id
    object.person_id
  end

  def actual_release_date
    object.sale_date.strftime("%Y-%m-%d")
  end

  alias_method :street_date, :actual_release_date

  def various_artists
    object.is_various?
  end

  # TODO: This probably isn't the right place for this
  # def excluded_countries(store)
  #   excludes = {
  #     ddex:   ["BQ", "CW", "SX", "SS"],
  #     google: ["BQ", "CW", "SX", "SS", "BL", "MF"],
  #     nokia:  ["BQ", "CW", "SX", "SS", "BL", "MF", "AN", "CS", "ZR"]
  #   }
  #   excludes[store]
  # end
end
