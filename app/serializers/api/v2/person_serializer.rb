class Api::V2::PersonSerializer < Api::V2::BaseSerializer
  attributes  :id, :name, :email, :country, :roles, :balance, :available_balance, :lock_reason, :status_updated_at

  has_many :people_flags, serializer: Api::V2::PeopleFlagsSerializer

  def roles
    object.roles.map do |role|
      {
        name: role.name,
        long_name: role.long_name,
        description: role.description,
        is_administrative: role.is_administrative
      }
    end
  end
end
