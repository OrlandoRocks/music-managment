class Api::V2::FlagSerializer < Api::V2::BaseSerializer
  attributes :id, :name
end
