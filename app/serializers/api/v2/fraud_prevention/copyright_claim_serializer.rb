class Api::V2::FraudPrevention::CopyrightClaimSerializer < Api::V2::BaseSerializer
  attributes :id,
             :hold_amount,
             :person_id,
             :person_name,
             :admin,
             :store,
             :internal_claim_id,
             :note,
             :created,
             :updated,
             :asset,
             :fraud_type,
             :asset_id,
             :store_id,
             :asset_type,
             :is_active,
             :album_id

  def created
    I18n.l(object.created_at, format: :slash_with_time)
  end

  def updated
    I18n.l(object.updated_at, format: :slash_with_time)
  end

  def person_name
    object.person.name
  end

  def note
    object.notes&.first&.note || ""
  end

  def asset
    if object.asset_type == "Album"
      object.asset.upc&.number
    else
      object.asset.isrc
    end
  end

  def admin
    Person.find_by(id: object.admin_id).name
  end

  def store
    object.store ? object.store.name : object.copyright_claimants.map(&:email).join(", ")
  end

  def asset_type
    object.asset_type == "Album" ? "UPC" : "ISRC"
  end

  # rubocop:disable Rails/DynamicFindBy
  def album_id
    if object.asset_type == "Album"
      Album.find_by_upc(asset).id
    else
      Song.by_isrc(asset).first&.album_id
    end
  end
end
