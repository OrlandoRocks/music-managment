class Api::V2::FlagReasonSerializer < Api::V2::BaseSerializer
  attributes :id, :reason
end
