class Api::V2::RewardSystem::RewardPersonSerializer < Api::V2::BaseSerializer
  attributes  :person_id,
              :name,
              :email,
              :currency,
              :country_iso_code,
              :status,
              :lifetime_earnings,
              :releases,
              :facebook_music_signup,
              :youtube_monetization,
              :albums,
              :publishing_customer,
              :tc_social_customer,
              :singles,
              :tiers,
              :points,
              :plan_id

  def person_id
    object.id
  end

  def lifetime_earnings
    object.lifetime_earning_amount
  end

  def releases
    {
      active: object.reward_eligible_releases.count,
      inactive: ineligible_releases.count
    }
  end

  def facebook_music_signup
    object.has_active_subscription_product_for?("FBTracks")
  end

  def youtube_monetization
    object.has_youtube_money?
  end

  def albums
    {
      active: object.reward_eligible_releases.full_albums.count,
      inactive: ineligible_releases.full_albums.count
    }
  end

  def publishing_customer
    object.purchased_pub_admin?
  end

  def tc_social_customer
    object.has_tc_social?
  end

  def singles
    {
      active: object.reward_eligible_releases.singles.count,
      inactive: ineligible_releases.singles.count
    }
  end

  def tiers
    object.tiers.order(hierarchy: :desc).pluck(:name)
  end

  def points
    object.person_points_balance&.balance
  end

  def plan_id
    object.plan.id if object.has_plan?
  end

  private

  def ineligible_releases
    object.albums.distributed.taken_down
  end
end
