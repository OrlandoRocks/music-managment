class Api::V2::RewardSystem::RewardSerializer < Api::V2::BaseSerializer
  attributes  :id, :name, :description, :link, :content_type, :points, :category, :is_active
  has_many :tiers

  def filter(keys)
    keys.delete(:tiers) unless opts[:include_associations]
    super(keys)
  end

  def opts
    scope || {}
  end
end
