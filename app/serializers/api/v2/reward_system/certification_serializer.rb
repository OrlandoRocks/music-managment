class Api::V2::RewardSystem::CertificationSerializer < Api::V2::BaseSerializer
  attributes  :id, :name, :description, :link, :badge_url, :points, :category, :is_active
  has_many :tiers
end
