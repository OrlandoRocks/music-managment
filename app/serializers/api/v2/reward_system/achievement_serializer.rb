class Api::V2::RewardSystem::AchievementSerializer < Api::V2::BaseSerializer
  attributes  :id, :name, :description, :link, :points, :category, :is_active
  has_many :tiers
end
