class Api::V2::RewardSystem::PersonPointsSerializer < Api::V2::BaseSerializer
  attributes :person_id, :points_balance

  def person_id
    object.id
  end

  def points_balance
    object.available_points
  end
end
