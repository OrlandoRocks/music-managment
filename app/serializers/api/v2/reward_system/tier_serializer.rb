class Api::V2::RewardSystem::TierSerializer < Api::V2::BaseSerializer
  attributes  :id,
              :name,
              :slug,
              :description,
              :badge_url,
              :hierarchy,
              :is_active,
              :is_sub_tier,
              :parent_id

  has_many :rewards
  has_many :achievements, except: [:tiers]
  has_many :certifications, except: [:tiers]

  def filter(keys)
    keys -= [:rewards, :achievements, :certifications] unless opts[:tier_associations]

    super(keys)
  end

  def opts
    scope || {}
  end
end
