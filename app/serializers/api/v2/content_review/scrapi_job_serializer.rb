class Api::V2::ContentReview::ScrapiJobSerializer < Api::V2::BaseSerializer
  attributes :id, :song_id, :status, :is_matched
end
