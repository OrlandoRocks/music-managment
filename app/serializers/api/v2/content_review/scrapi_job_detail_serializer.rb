class Api::V2::ContentReview::ScrapiJobDetailSerializer < Api::V2::BaseSerializer
  attributes :flagged_song_id,
             :artists,
             :flagged_song_name,
             :matched_song_name,
             :matched_song_album_name,
             :matched_album_label,
             :id

  def flagged_song_id
    object.scrapi_job.song_id
  end

  def flagged_song_name
    object.scrapi_job.song.name
  end

  def matched_song_name
    object.track_title
  end

  def matched_song_album_name
    object.album_title
  end

  def matched_album_label
    object.label
  end

  def artists
    object.artists.map { |artist| { name: artist } }
  end
end
