class Api::V2::ContentReview::SalepointSerializer < Api::V2::BaseSerializer
  attributes :id, :store, :distributions

  has_many :distributions, serializer: Api::V2::ContentReview::DistributionSerializer, root: false

  def store
    object.store.name
  end
end
