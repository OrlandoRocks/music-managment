class Api::V2::ContentReview::TrackMonetizationBlockerSerializer < Api::V2::BaseSerializer
  attributes :id, :store, :store_id

  def store
    object.store.name
  end
end
