class Api::V2::ContentReview::ExpandedAlbumFlaggedContentSerializer < Api::V2::ContentReview::AlbumFlaggedContentSerializer
  attributes :flagged_content

  def flagged_content
    matched_songs = []
    object.flagged_content.map { |flagged_song_params|
      flagged_song = Song.find(flagged_song_params["flagged_song_id"])
      Song.includes(album: :artists).where(id: flagged_song_params["matched_song_ids"]).each do |matched_song|
        matched_album = matched_song.album
        matched_songs << {
          matched_song_id: matched_song.id,
          matched_song_name: matched_song.name,
          matched_song_album_name: matched_album.name,
          matched_album_label: matched_album.label_name,
          matched_song_album_id: matched_song.album_id,
          flagged_song_name: flagged_song.name,
          flagged_song_id: flagged_song.id,
          artists: matched_album.artists.map { |artist| { name: artist.name } }
        }
      end
    }
    matched_songs.flatten
  end
end
