class Api::V2::ContentReview::DistributionSerializer < Api::V2::BaseSerializer
  attributes :id, :state
end
