class Api::V2::ContentReview::AlbumFlaggedContentSerializer < Api::V2::BaseSerializer
  attributes :id, :flagged_content
end
