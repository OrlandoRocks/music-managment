class Api::V2::ContentReview::AlbumListSerializer < Api::V2::BaseSerializer
  attributes :id, :name, :review_audits, :creatives, :is_various, :paid_at

  def creatives
    object.creatives.map do |creative|
      {
        id: creative.id,
        name: creative.name,
        role: creative.role,
      }
    end
  end

  def review_audits
    last_review_audit = object.last_review_audit
    return [] unless last_review_audit

    [
      {
        id: last_review_audit.id,
        created: last_review_audit.created_at,
        note: last_review_audit.note,
        event: last_review_audit.event,
        person: { name: last_review_audit.person.name }
      }
    ]
  end

  def paid_at
    purchase = object.credit_usage ? object.credit_usage.purchase : object.purchases.last
    purchase&.paid_at || ""
  end
end
