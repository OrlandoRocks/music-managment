class Api::V2::ContentReview::ReviewReasonSerializer < Api::V2::BaseSerializer
  attributes :id, :reason, :template_type
end
