class Api::V2::ContentReview::ReviewAuditSerializer < Api::V2::BaseSerializer
  attributes :id, :event, :note, :created, :person

  has_many :review_reasons, serializer: Api::V2::ContentReview::ReviewReasonSerializer, root: false

  def created
    I18n.l(object.created_at, format: :slash_with_time)
  end

  def person
    Api::V2::PersonSerializer.new(object.person, root: false)
  end
end
