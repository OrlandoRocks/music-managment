class Api::V2::ContentReview::TrackMonetizationSerializer < Api::V2::BaseSerializer
  attributes :id, :state, :store

  def store
    object.store.name
  end
end
