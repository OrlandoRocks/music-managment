class Api::V2::ContentReview::AlbumSerializer < Api::V2::BaseSerializer
  attributes :name,
             :album_type,
             :created_on,
             :tracks,
             :album_type,
             :artwork_url,
             :artwork_auto_generated,
             :is_various,
             :person_id,
             :id,
             :auto_format_on,
             :vip_status,
             :total_releases,
             :account_status,
             :account_created,
             :account_owner,
             :account_sift_score,
             :account_country,
             :account_domain,
             :account_lock_reason,
             :legal_review_state,
             :finalized,
             :account_email,
             :genres,
             :primary_genre,
             :secondary_genre,
             :countries,
             :label,
             :songs,
             :creatives,
             :metadata_language_description,
             :metadata_language_code_id,
             :primary_genre_id,
             :secondary_genre_id,
             :golive_date,
             :timed_release_timing_scenario,
             :salepoints,
             :allow_different_format,
             :purchases,
             :review_audits,
             :album_flagged_content,
             :has_active_ytm,
             :first_release,
             :paid_at,
             :primary_sub_genre,
             :secondary_sub_genre,
             :indian_sub_genres,
             :has_india_sub_genre,
             :tunecore_upc,
             :optional_upc,
             :explicit_content,
             :has_multiple_needs_review

  has_many :salepoints, serializer: Api::V2::ContentReview::SalepointSerializer, root: false
  has_many :purchases, serializer: Api::V2::ContentReview::PurchaseSerializer, root: false do
    object.purchases.order(created_at: :desc)
  end
  has_one :album_flagged_content, serializer: Api::V2::ContentReview::AlbumFlaggedContentSerializer, root: false

  def golive_date
    if object.golive_date
      object.golive_date.utc.iso8601
    else
      Album::DateTransformerService.convert_eastern_date_to_localtime(object.sale_date)
    end
  end

  def tracks
    object.songs.size
  end

  delegate :person, to: :object

  def artwork_url
    object.artwork&.url(:large)
  end

  def artwork_auto_generated
    object.artwork&.auto_generated
  end

  def auto_format_on
    object.allow_different_format
  end

  def vip_status
    person.vip
  end

  delegate :albums, to: :person, prefix: true

  def total_releases
    person_albums.size
  end

  def has_multiple_needs_review
    person_albums.needs_review.count > 1
  end

  def account_status
    person.status
  end

  def account_created
    I18n.l(person.created_on, format: :slash_with_time)
  end

  def account_owner
    person.name
  end

  def account_country
    person.country
  end

  def account_sift_score
    score = person.person_sift_score&.score

    return unless score

    (score.round(2) * 100).round
  end

  def account_domain
    person.country_domain
  end

  def account_lock_reason
    person.lock_reason
  end

  def legal_review_state
    object.legal_review_state.capitalize
  end

  def finalized
    return if object.finalized_at.nil?

    I18n.l(object.finalized_at, format: :slash_with_time)
  end

  def account_email
    person.email
  end

  def has_active_ytm
    object.person.has_active_ytm?
  end

  def first_release
    object.person.first_release?
  end

  def genres
    object.genres.map(&:name)
  end

  def primary_genre
    if object.primary_genre&.india_subgenre?
      get_indian_genre(object.primary_genre)
    elsif object.primary_genre&.subgenre?
      get_genre(object.primary_genre)
    elsif object.primary_genre.present?
      object.primary_genre&.name
    end
  end

  def secondary_genre
    if object.secondary_genre&.india_subgenre?
      get_indian_genre(object.secondary_genre)
    elsif object.secondary_genre&.subgenre?
      get_genre(object.secondary_genre)
    elsif object.secondary_genre.present?
      object.secondary_genre&.name
    end
  end

  def get_indian_genre(genre)
    if has_india_sub_genre
      Genre.india_parent_genre.name
    else
      genre.name
    end
  end

  def get_genre(genre)
    if has_latin_sub_genre(genre) || has_brazilian_sub_genre(genre) || has_african_sub_genre(genre)
      genre.parent_genre.name
    else
      genre.name
    end
  end

  delegate :metadata_language_code_id, to: :object

  def metadata_language_description
    object.metadata_language_code.try(:description)
  end

  def countries
    object.countries.map { |c| [c.name, c.iso_code] }
  end

  def label
    object.label&.name
  end

  def creatives
    object.creatives.map do |creative|
      Api::V2::ContentReview::CreativeSerializer.new(creative, root: false)
    end
  end

  def songs
    object.songs.map do |song|
      Api::V2::ContentReview::SongSerializer.new(song, root: false)
    end
  end

  def review_audits
    object.review_audits.order(created_at: :desc).map do |review_audit|
      Api::V2::ContentReview::ReviewAuditSerializer.new(review_audit, root: false)
    end
  end

  def paid_at
    purchase = object.credit_usage ? object.credit_usage.purchase : object.purchases.last
    (purchase && purchase.paid_at) ? purchase.paid_at.utc.iso8601 : ""
  end

  def has_india_sub_genre
    return unless person.india_user?

    true
  end

  def has_latin_sub_genre(genre)
    genre.latin_subgenre? && FeatureFlipper.show_feature?(:latin_genres, person)
  end

  def has_brazilian_sub_genre(genre)
    genre.brazilian_subgenre? && FeatureFlipper.show_feature?(:brazilian_genres, person)
  end

  def has_african_sub_genre(genre)
    genre.african_subgenre? && FeatureFlipper.show_feature?(:african_genres, person)
  end

  def primary_sub_genre
    return if object.primary_genre.blank?

    return if object.primary_genre&.india_subgenre? && !has_india_sub_genre

    object.primary_genre.name
  end

  def secondary_sub_genre
    return if object.secondary_genre.blank?

    return if object.secondary_genre&.india_subgenre? && !has_india_sub_genre

    object.secondary_genre.name
  end

  def indian_sub_genres
    return unless has_india_sub_genre

    Genre.india_subgenres.as_json(only: [:id, :name], root: false)
  end

  def tunecore_upc
    object.tunecore_upc&.number
  end

  def optional_upc
    object.optional_upc&.number
  end

  def explicit_content
    object.songs.any?(&:parental_advisory)
  end
end
