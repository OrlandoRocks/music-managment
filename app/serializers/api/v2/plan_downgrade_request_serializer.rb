class Api::V2::PlanDowngradeRequestSerializer < Api::V2::BaseSerializer
  attributes :id, :name, :email, :status, :current_plan, :plan_downgrade_requests

  has_many :plan_downgrade_requests

  def current_plan
    plan_history = PersonPlanHistory.where(person_id: id).last
    {
      name: object.person_plan.name,
      created_at: plan_history.plan_start_date,
      expires_at: plan_history.plan_end_date,
    } if object.person_plan
  end

  def plan_downgrade_requests
    plan_downgrade_requests = []
    object.plan_downgrade_requests.reverse.map do |plan_downgrade_request|
      plan_downgrade_requests << {
        id: plan_downgrade_request.id,
        status: plan_downgrade_request.status,
        date: plan_downgrade_request.created_at,
        name: plan_downgrade_request.requested_plan.name,
        reason_type: plan_downgrade_request.reason.downgrade_category.name,
        reason: plan_downgrade_request.reason.name,
        reason_detail: plan_downgrade_request&.downgrade_other_reason&.description
      }
    end

    plan_downgrade_requests
  end

  def reason
    {
      reason: object.reason.name
    }
  end
end
