class Api::V2::PeopleFlagsSerializer < Api::V2::BaseSerializer
  attributes :id, :created_at

  has_one :flag
  has_many :flag_transitions
end
