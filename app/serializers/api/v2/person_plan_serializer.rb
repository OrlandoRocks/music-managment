class Api::V2::PersonPlanSerializer < Api::V2::BaseSerializer
  attributes :plan_id, :name, :created_at, :person

  def person
    {
      id: object.person.id,
      name: object.person.name,
      email: object.person.email,
      status: object.person.status
    }
  end
end
