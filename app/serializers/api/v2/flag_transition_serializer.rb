class Api::V2::FlagTransitionSerializer < Api::V2::BaseSerializer
  attributes :id, :add_remove

  has_one :flag_reason
end
