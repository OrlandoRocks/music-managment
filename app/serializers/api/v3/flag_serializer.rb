class Api::V3::FlagSerializer < Api::V3::BaseSerializer
  attributes :id, :name
end
