class Api::V3::FlagTransitionSerializer < Api::V3::BaseSerializer
  attributes :id, :add_remove

  has_one :flag_reason
end
