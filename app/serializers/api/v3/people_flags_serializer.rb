class Api::V3::PeopleFlagsSerializer < Api::V3::BaseSerializer
  attributes :id, :created_at

  has_one :flag
  has_many :flag_transitions
end
