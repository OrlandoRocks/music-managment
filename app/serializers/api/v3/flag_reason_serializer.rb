class Api::V3::FlagReasonSerializer < Api::V3::BaseSerializer
  attributes :id, :reason
end
