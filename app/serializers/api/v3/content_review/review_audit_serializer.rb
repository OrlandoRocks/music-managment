class Api::V3::ContentReview::ReviewAuditSerializer < Api::V3::BaseSerializer
  attributes :id, :event, :note, :created, :person

  has_many :review_reasons, serializer: Api::V3::ContentReview::ReviewReasonSerializer, root: false

  def created
    I18n.l(object.created_at, format: :slash_with_time)
  end

  def person
    Api::V3::PersonSerializer.new(object.person, root: false)
  end
end
