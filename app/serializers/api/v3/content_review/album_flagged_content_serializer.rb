class Api::V3::ContentReview::AlbumFlaggedContentSerializer < Api::V3::BaseSerializer
  attributes :id, :flagged_content
end
