class Api::V3::ContentReview::TrackMonetizationSerializer < Api::V3::BaseSerializer
  attributes :id, :state, :store

  def store
    object.store.name
  end
end
