class Api::V3::ContentReview::TrackMonetizationBlockerSerializer < Api::V3::BaseSerializer
  attributes :id, :store, :store_id

  def store
    object.store.name
  end
end
