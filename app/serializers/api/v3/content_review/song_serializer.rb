class Api::V3::ContentReview::SongSerializer < Api::V3::BaseSerializer
  attributes :id,
             :name,
             :track_number,
             :translated_name,
             :parental_advisory,
             :instrumental,
             :lyrics,
             :language_code_id,
             :language_description,
             :primary_artists,
             :featuring_artists,
             :songwriters,
             :contributors,
             :track_monetizations,
             :asset_url,
             :isrc,
             :filename,
             :album_id,
             :scrapi_job,
             :duration_in_seconds,
             :orig_asset_url

  has_many :track_monetization_blockers,
           serializer: Api::V3::ContentReview::TrackMonetizationBlockerSerializer,
           root: false

  has_one :scrapi_job, serializer: Api::V3::ContentReview::ScrapiJobSerializer, root: false

  def track_number
    object.track_num
  end

  def uuid
    object.s3_asset.nil? ? nil : object.s3_asset.uuid
  end

  def lyrics
    object.lyric.try(:content)
  end

  def language_code_id
    object.lyric_language_code_id
  end

  def language_description
    object.lyric_language_code.try(:description)
  end

  def primary_artists
    song_artists.primary_artists.map do |creative|
      Api::V3::ContentReview::CreativeSerializer.new(creative, root: false)
    end
  end

  def featuring_artists
    song_artists.featurings.map do |creative|
      Api::V3::ContentReview::CreativeSerializer.new(creative, root: false)
    end
  end

  def songwriters
    object.songwriters.map do |creative|
      Api::V3::ContentReview::CreativeSerializer.new(creative, root: false)
    end
  end

  def contributors
    song_artists.contributors.map do |creative|
      Api::V3::ContentReview::CreativeSerializer.new(creative, root: false)
    end
  end

  def instrumental
    object.instrumental?
  end

  def track_monetizations
    object.track_monetizations.map do |track|
      Api::V3::ContentReview::TrackMonetizationSerializer.new(track, root: false)
    end
  end

  def asset_url
    return "" if object.s3_streaming_asset.nil? && object.s3_asset.nil?

    if object.s3_streaming_asset
      generate_asset_url(object.s3_streaming_asset)
    else
      generate_asset_url(
        object.s3_asset,
        true
      )
    end
  end

  def orig_asset_url
    return "" if object.s3_orig_asset.nil?

    generate_asset_url(object.s3_orig_asset)
  end

  def song_artists
    object.album.album? ? object : object.album
  end

  def filename
    object.original_filename
  end

  def generate_asset_url(asset, with_mp3_ext = false)
    generator.expires_in = 36_000
    generator.get(asset.bucket, "#{asset.key}#{with_mp3_ext ? '.mp3' : ''}")
  end

  def generator
    @generator ||= S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
  end
end
