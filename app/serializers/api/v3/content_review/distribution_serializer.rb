class Api::V3::ContentReview::DistributionSerializer < Api::V3::BaseSerializer
  attributes :id, :state
end
