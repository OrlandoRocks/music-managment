class Api::V3::ContentReview::ReviewReasonSerializer < Api::V3::BaseSerializer
  attributes :id, :reason, :template_type
end
