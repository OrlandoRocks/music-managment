class Api::V3::ContentReview::CreativeSerializer < Api::V3::BaseSerializer
  attributes :id, :name, :role, :creative_song_roles

  def creative_song_roles
    object.creative_song_roles.includes(:song_role).map do |csr|
      {
        id: csr.id,
        song_id: csr.song_id,
        creative_id: csr.creative_id,
        song_role_id: csr.song_role_id,
        song_role: {
          id: csr.song_role.id,
          role_type: csr.song_role.role_type,
          role_group: csr.song_role.role_group,
        }
      }
    end
  end
end
