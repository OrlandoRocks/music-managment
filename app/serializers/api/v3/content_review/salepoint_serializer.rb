class Api::V3::ContentReview::SalepointSerializer < Api::V3::BaseSerializer
  attributes :id, :store, :distributions

  has_many :distributions, serializer: Api::V3::ContentReview::DistributionSerializer, root: false

  def store
    object.store.name
  end
end
