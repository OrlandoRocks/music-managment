class Api::V3::ContentReview::PurchaseSerializer < Api::V3::BaseSerializer
  attributes :id, :paid_at

  def paid_at
    I18n.l(object.paid_at, format: :slash_with_time) if object.paid_at.present?
  end
end
