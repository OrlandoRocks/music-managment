class Api::V3::ContentReview::ScrapiJobSerializer < Api::V3::BaseSerializer
  attributes :id, :song_id, :status, :is_matched
end
