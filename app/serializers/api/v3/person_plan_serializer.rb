class Api::V3::PersonPlanSerializer < Api::V3::BaseSerializer
  attributes :plan_id, :name, :created_at, :expires_at, :person

  def person
    {
      id: object.person.id,
      name: object.person.name,
      email: object.person.email,
      status: object.person.status
    }
  end
end
