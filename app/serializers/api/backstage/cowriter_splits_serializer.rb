class Api::Backstage::CowriterSplitsSerializer < ApplicationSerializer
  attributes :id,
             :first_name,
             :full_name,
             :last_name,
             :uuid,
             :is_unknown,
             :cowriter_share,
             :errors

  def full_name
    "#{first_name} #{last_name}"
  end

  def uuid
    object.id
  end
end
