class Api::Backstage::SongCopyrightClaimSerializer < ApplicationSerializer
  attributes :id, :has_claimant, :claimant_has_songwriter_agreement,
             :claimant_has_pro_or_cmo, :claimant_has_tc_publishing

  alias_attribute :has_claimant, :claimant

  def claimant
    object.claimant.present?
  end
end
