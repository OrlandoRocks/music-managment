class Api::Backstage::SongArtistSerializer < ApplicationSerializer
  attributes :artist_name, :associated_to, :credit, :role_ids, :creative_id

  def role_ids
    object.role_ids.try(:split, ",") || []
  end
end
