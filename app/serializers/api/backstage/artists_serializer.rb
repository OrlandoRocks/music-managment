class Api::Backstage::ArtistsSerializer < ApplicationSerializer
  attributes :name

  delegate :name, to: :object
end
