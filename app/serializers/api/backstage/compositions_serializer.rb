class Api::Backstage::CompositionsSerializer < ApplicationSerializer
  attributes :id,
             :composition_title,
             :translated_name,
             :appears_on,
             :record_label,
             :cowriters,
             :composers,
             :cowriter_share,
             :status,
             :composer_share,
             :errors,
             :isrc,
             :submitted_at,
             :performing_artist,
             :release_date,
             :is_ntc_song,
             :unknown_split_percent,
             :public_domain

  COMPOSITION_STATES = Set["verified",
                           "submitted",
                           "resubmitted",
                           "terminated",
                           "ineligible",
                           "pending_distribution",
                           "draft"].freeze

  def composition_title
    object.try(:composition_title) || object.try(:publishing_composition_title)
  end

  def status
    return "submitted" if composition_state == "split_submitted"
    return "verified" if composition_state == "accepted"
    return composition_state if COMPOSITION_STATES.include?(composition_state)

    "shares_missing"
  end

  def record_label
    object.try(:record_label) || ""
  end

  def cowriters
    object.cowriter_split_ids.blank? ? [] : object.cowriter_split_ids.split(",").map(&:to_i)
  end

  def composers
    object.try(:composer_split_ids).blank? ? [] : object.composer_split_ids.split(",").map(&:to_i)
  end

  def isrc
    object.isrc.presence || object.try(:optional_isrc).presence || object.try(:tunecore_isrc).presence
  end

  def submitted_at
    object.share_submitted_date
  end

  def performing_artist
    object.try(:performing_artist) || ""
  end

  def release_date
    object[:release_date].present? ? object[:release_date].strftime("%m/%d/%Y") : ""
  end

  # If `isrc` is one of the object's attributes we can infer
  # this composition is associated to a `NonTunecoreSong`
  # because `isrc` is a column on the `NonTunecoreSong` model
  # and not on the `Song` model
  def is_ntc_song
    object.try(:is_ntc_song)
  end

  private

  def composition_state
    object.try(:composition_state) || object.try(:publishing_composition_state)
  end
end
