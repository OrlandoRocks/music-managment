class Api::Backstage::SongCoverSongMetadataSerializer < ApplicationSerializer
  attributes :id, :song_id, :cover_song, :licensed, :will_get_license
end
