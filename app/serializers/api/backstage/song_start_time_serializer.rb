class Api::Backstage::SongStartTimeSerializer < ApplicationSerializer
  attributes :id, :store_id, :start_time
end
