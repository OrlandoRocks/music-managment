class Api::Backstage::AlbumApp::AlbumSerializer < ApplicationSerializer
  attributes :album_type,
             :allow_different_format,
             :creatives,
             :finalized,
             :genre,
             :golive_date,
             :is_editable,
             :is_new,
             :is_previously_released,
             :is_various,
             :label_name,
             :language_code,
             :name,
             :orig_release_year,
             :primary_genre_id,
             :sale_date,
             :secondary_genre_id,
             :optional_upc_number,
             :id,
             :track_limit_reached,
             :timed_release_timing_scenario,
             :explicit,
             :clean_version,
             :recording_location,
             :optional_isrc,
             :parental_advisory,
             :is_dj_release,
             :selected_countries,
             :person_id,
             :artwork_url

  NO_CREATIVES = [].freeze

  def album_type
    object.class.to_s
  end

  def allow_different_format
    object.allow_different_format? || ""
  end

  def finalized
    object.finalized?
  end

  def genre
    object.primary_genre.try(:name) || ""
  end

  def golive_date
    live_date = object.golive_date || DateTime.now.change(hour: 0, min: 0)
    Album::DateTransformerService.convert_to_hash(live_date)
  end

  def is_editable
    object.is_editable?
  end

  def is_new
    object.id.nil?
  end

  def is_previously_released
    object.previously_released
  end

  def is_various
    object.is_various?
  end

  def label_name
    object.label.try(:name) || ""
  end

  def language_code
    object.language_code || I18n.locale.to_s[0..1]
  end

  def name
    object.name || ""
  end

  def orig_release_year
    object.orig_release_year || ""
  end

  def primary_genre_id
    object.primary_genre_id || ""
  end

  def sale_date
    object.sale_date || DateTime.now
  end

  def secondary_genre_id
    object.secondary_genre_id || ""
  end

  def optional_upc_number
    object.upc&.number || ""
  end

  def optional_isrc
    object.try(:optional_isrc) || ""
  end

  def track_limit_reached
    object.track_limit_reached?
  end

  def timed_release_timing_scenario
    object.timed_release_timing_scenario || Album::TIMED_RELEASE_RELATIVE_TIME
  end

  def explicit
    parental_advisory
  end

  def parental_advisory
    object.try(:parental_advisory) unless is_new
  end

  def clean_version
    object.try(:clean_version) unless is_new
  end

  def recording_location
    object.recording_location || ""
  end

  def selected_countries
    object.country_iso_codes
  end

  def creatives
    return NO_CREATIVES if object.id.nil?

    Album
      .includes(:creatives, [:external_service_ids])
      .find(object.id)
      .creatives
      .map do |creative|
        Api::Backstage::AlbumApp::CreativeSerializer.new(creative, root: false)
      end
  end

  def person_id
    object.person&.id
  end

  def artwork_url
    object.artwork&.url(:medium)
  end
end
