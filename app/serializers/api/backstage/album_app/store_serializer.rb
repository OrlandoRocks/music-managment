class Api::Backstage::AlbumApp::StoreSerializer < ApplicationSerializer
  include ActionView::Helpers::TranslationHelper
  include CustomTranslationHelper

  attributes :id, :name, :selected, :tagline, :custom_abbrev

  def salepoint_store_ids
    context[:salepoint_store_ids] || []
  end

  def selected
    salepoint_store_ids.include?(object.id)
  end

  def tagline
    case object.id
    when Store::IHEART_RADIO_STORE_ID, Store::NAPSTER_STORE_ID
      custom_t("salepoints.napster_iheart.iheart_and_napster")
    end
  end
end
