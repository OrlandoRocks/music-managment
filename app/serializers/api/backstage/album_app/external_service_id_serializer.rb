class Api::Backstage::AlbumApp::ExternalServiceIdSerializer < ApplicationSerializer
  attributes :identifier, :service_name, :state
end
