class Api::Backstage::AlbumApp::CreativeSerializer < ApplicationSerializer
  attributes :id, :name, :external_service_ids, :role

  private

  def name
    object.artist.name
  end

  def external_service_ids
    object.external_service_ids.map do |esid|
      Api::Backstage::AlbumApp::ExternalServiceIdSerializer.new(esid, root: false)
    end
  end
end
