# The album_app's song-related data dependencies, a subset of the #songs_app div dataset
class Api::Backstage::AlbumApp::SongsDataSerializer < ApplicationSerializer
  attributes :bigbox_url,
             :default_song,
             :song_roles,
             :songs,
             :songwriter_names,
             :start_time_stores,
             :tc_pid

  delegate :bigbox_url, to: :object
  delegate :default_song, to: :object
  delegate :song_roles, to: :object
  delegate :songs, to: :object
  delegate :songwriter_names, to: :object
  delegate :tc_pid, to: :object

  def start_time_stores
    Store::SONG_START_TIME_STORES
  end
end
