class Api::Backstage::SongCopyrightSerializer < ApplicationSerializer
  attributes :id, :composition, :recording
end
