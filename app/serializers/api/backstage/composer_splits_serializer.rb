class Api::Backstage::ComposerSplitsSerializer < ApplicationSerializer
  attributes :id, :uuid, :composer_share, :errors

  def full_name
    "#{first_name} #{last_name}"
  end

  def uuid
    object.id
  end
end
