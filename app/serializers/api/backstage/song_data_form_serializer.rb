class Api::Backstage::SongDataFormSerializer < ApplicationSerializer
  attributes :errors
  has_one :data, serializer: Api::Backstage::SongDataSerializer
end
