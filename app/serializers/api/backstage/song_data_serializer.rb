# See also songs_data_serializer
class Api::Backstage::SongDataSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :language_code_id,
             :translated_name,
             :version,
             :cover_song,
             :made_popular_by,
             :explicit,
             :clean_version,
             :optional_isrc,
             :lyrics,
             :asset_url,
             :spatial_audio_asset_url,
             :album_id,
             :artists,
             :track_number,
             :previously_released_at,
             :asset_filename,
             :atmos_asset_filename,
             :instrumental,
             :copyrights,
             :song_start_times,
             :bigbox_disabled

  has_many :artists, serializer: ::Api::Backstage::SongArtistSerializer, root: false
  has_many :song_start_times, serializer: ::Api::Backstage::SongStartTimeSerializer, root: false
  has_one :copyrights, serializer: ::Api::Backstage::SongCopyrightSerializer, root: false
  has_one :song_copyright_claims, serializer: ::Api::Backstage::SongCopyrightClaimSerializer, root: false
  has_one :cover_song_metadata, serializer: ::Api::Backstage::SongCoverSongMetadataSerializer, root: false
end
