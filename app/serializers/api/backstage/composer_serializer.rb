class Api::Backstage::ComposerSerializer < ApplicationSerializer
  attributes  :id,
              :account_composer,
              :cae,
              :first_name,
              :full_name,
              :last_name,
              :middle_name,
              :name,
              :has_tax_info,
              :prefix,
              :pro_id,
              :pro_name,
              :suffix,
              :status

  def account_composer
    object.person_id.present? && object.person_id == object.account.person_id
  end

  def cae
    object.cae || ""
  end

  def first_name
    object.first_name.strip
  end

  def full_name
    object.full_name.strip
  end

  def last_name
    object.last_name.strip
  end

  def middle_name
    (object.middle_name || "").strip
  end

  def name
    object.name.strip
  end

  def has_tax_info
    account_composer && object.tax_info.present?
  end

  def prefix
    object.name_prefix || ""
  end

  def pro_id
    object.performing_rights_organization_id || ""
  end

  def pro_name
    pro_id.present? ? PerformingRightsOrganization.find(pro_id).name : ""
  end

  def suffix
    object.name_suffix || ""
  end

  def status
    ::PublishingAdministration::ComposerStatusService.new(object).state
  end
end
