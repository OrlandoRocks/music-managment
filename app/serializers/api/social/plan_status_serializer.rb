module Api::Social
  class PlanStatusSerializer < Api::Social::BaseSerializer
    attributes :data

    def data
      country_id = Country.find_by(name: object.user.country)&.id
      {
        email: object.user.email,
        plan: object.plan,
        balance: object.user.balance,
        country_website: COUNTRY_URLS[object.user.country_domain],
        plan_expires_at: object.plan_expires_at.to_s,
        grace_period_ends_on: object.grace_period_ends_on.to_s,
        payment_channel: object.user.subscription_payment_channel_for("Social"),
        is_active_releases: object.user.distributed_albums?,
        country: country_id
      }
    end
  end
end
