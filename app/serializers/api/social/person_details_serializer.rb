module Api::Social
  class PersonDetailsSerializer < Api::Social::BaseSerializer
    attributes :data

    def data
      plan_status = ::Social::PlanStatus.for(object)
      country_id = Country.find_by(name: object.country)&.id
      {
        person_id: object.id,
        email: object.email,
        name: object.name,
        plan: plan_status.plan,
        balance: object.balance,
        country: country_id,
        country_website: COUNTRY_URLS[object.country_domain],
        plan_expires_at: plan_status.plan_expires_at.to_s,
        grace_period_ends_on: plan_status.grace_period_ends_on.to_s,
        payment_channel: object.subscription_payment_channel_for("Social")
      }
    end
  end
end
