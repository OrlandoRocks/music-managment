module Api::Social
  class BaseSerializer < ApplicationSerializer
    attributes :status

    def status
      "success"
    end
  end
end
