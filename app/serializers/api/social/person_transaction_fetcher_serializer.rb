class Api::Social::PersonTransactionFetcherSerializer < Api::Social::BaseSerializer
  attributes :data

  def data
    object.transactions.map { |transaction| PersonTransactionSerializer.new(transaction, root: false) }
  end
end
