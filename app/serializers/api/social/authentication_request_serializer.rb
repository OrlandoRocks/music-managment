module Api::Social
  class AuthenticationRequestSerializer < Api::Social::BaseSerializer
    attributes :data

    def data
      country_id = Country.find_by(name: object.user.country)&.id
      is_tfa_enabled = object.user.two_factor_auth.try(:active?) || false
      {
        email: object.user.email,
        is_tfa_enabled: is_tfa_enabled,
        plan: object.plan,
        plan_expires_at: object.plan_expires_at.to_s,
        grace_period_ends_on: object.grace_period_ends_on.to_s,
        balance: object.user.balance,
        first_name: object.user.first_name,
        last_name: object.user.last_name,
        person_id: object.user.id,
        payment_channel: object.user.subscription_payment_channel_for("Social"),
        country: country_id,
        customer_type: object.user.customer_type,
        corporate_entity: object.user.corporate_entity.try(:name)
      }
    end
  end
end
