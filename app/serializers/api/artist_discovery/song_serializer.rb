class Api::ArtistDiscovery::SongSerializer < ApplicationSerializer
  attributes :id,
             :title,
             :image,
             :artist_name,
             :song_url,
             :song_key,
             :spotify_url,
             :album_name

  def title
    object&.name
  end

  def image
    object.album.artwork.url(:medium) if object.album.artwork && object.album.artwork.s3_asset
  end

  def artist_name
    object.album.artist&.name
  end

  def song_url
    object.s3_asset.original_url if object.s3_asset && object.s3_asset.original_url
  end

  def song_key
    object.s3_asset&.key if object.s3_asset
  end

  def spotify_url
    identifier = ExternalServiceId.where(linkable: object, service_name: "spotify").last&.identifier
    "#{ENV.fetch('SPOTIFY_STORE_BASE_URL')}/embed/track/#{identifier}" if identifier.present?
  end

  def album_name
    object.album&.name
  end
end
