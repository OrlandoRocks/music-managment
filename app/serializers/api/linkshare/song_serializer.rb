class Api::Linkshare::SongSerializer < ApplicationSerializer
  attributes :id, :title, :image, :artist_name

  delegate :id, to: :object

  def title
    object&.name
  end

  def image
    object.album.artwork.url(:medium) if object.album.artwork && object.album.artwork.s3_asset
  end

  def artist_name
    object.album.artist&.name
  end
end
