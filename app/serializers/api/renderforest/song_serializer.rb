class Api::Renderforest::SongSerializer < ApplicationSerializer
  attributes :id, :title, :image, :artist_name, :song_url, :song_key

  def title
    object&.name
  end

  def image
    object.album.artwork.url(:medium) if object.album.artwork && object.album.artwork.s3_asset
  end

  def artist_name
    object.album.artist&.name
  end

  def song_url
    object.s3_asset.original_url(1.day) if object.s3_asset
  end

  def song_key
    object.s3_asset&.key if object.s3_asset
  end
end
