class Api::Renderforest::PurchaseHandlerSerializer < ApplicationSerializer
  attributes :data, :status

  def data
    { "paid_at" => object.paid_at, "purchase_id" => object.purchase_id }
  end

  def status
    "success"
  end
end
