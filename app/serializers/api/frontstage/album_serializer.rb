class Api::Frontstage::AlbumSerializer < ApplicationSerializer
  attributes :apple_id, :name, :upc

  def apple_id
    object.external_id_for("apple")
  end

  delegate :name, to: :object

  def upc
    object.upc.number
  end
end
