class Api::Frontstage::CreativeSerializer < ApplicationSerializer
  attributes :apple_id, :name, :albums

  has_many :albums, serializer: Api::Frontstage::AlbumSerializer

  def apple_id
    object.artist_id
  end

  def name
    object.artist_name
  end

  def albums
    Creative::ExternalServiceQueryBuilder.build(object)
  end
end
