class Api::External::CountryWebsiteLanguageSerializer < ApplicationSerializer
  attributes :country_website_id,
             :yml_languages,
             :selector_language,
             :selector_country,
             :selector_order,
             :redirect_country_website_id
end
