class Api::External::AlbumSerializer < ApplicationSerializer
  include ExternalServiceIdHelper

  attributes :album_id,
             :album_name,
             :album_s3_key,
             :album_artwork_url,
             :artist_apple_url,
             :artist_name,
             :artist_tcsocial_url,
             :artist_spotify_url,
             :album_upc,
             :release_date,
             :release_type

  has_many :songs, root: :tracks
  has_many :genres

  def album_id
    object.id
  end

  def album_name
    object.name
  end

  def album_artwork_url
    object.artwork.url(:medium)
  end

  def album_upc
    object.tunecore_upc.number
  end

  def artist_apple_url
    apple_id = object.external_service_ids.where(service_name: "Apple").first
    return unless apple_id

    url = apple_id.apple_url
    url += "?ls=1&app=itunes" unless object.apple_music

    url
  end

  def artist_spotify_url
    spotify_id = object.external_service_ids.where(service_name: "Spotify").first
    spotify_id.spotify_url if spotify_id
  end

  # TODO: When TC Social is ready, this needs to be added.
  def artist_tcsocial_url
  end

  def album_s3_key
    object.artwork.s3_asset.key
  end

  def artist_name
    object.artist.name
  end

  def release_date
    object.golive_date.to_s
  end

  def release_type
    object.album_type
  end

  private

  def album_urls
    @album_urls ||= build_album_links(object)
  end
end
