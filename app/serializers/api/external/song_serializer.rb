class Api::External::SongSerializer < ApplicationSerializer
  SECONDS_IN_AN_HOUR = 60 * 60

  attributes :asset_files_url, :asset_s3_key, :isrc, :song_name, :track_id

  def asset_files_url
    object.s3_url(SECONDS_IN_AN_HOUR)
  end

  def asset_s3_key
    object.s3_asset.key
  end

  def isrc
    object.tunecore_isrc
  end

  def song_name
    object.name
  end

  def track_id
    object.id
  end
end
