class Api::Webhooks::BaseSerializer < ApplicationSerializer
  attr_reader :request, :body, :remote_ip, :required_attributes

  attributes :body, :remote_ip

  self.root = false

  def initialize(object, options = {})
    super object, options

    @request = object[:request]

    raise ArgumentError.new("Expected a request object for #{self.class}, got {}") and return if @request.blank?

    @body = @request.body
    @remote_ip = @request.remote_ip
  end

  def attributes
    data = super

    return data if required_attributes.blank?

    invalid_attributes = []

    required_attributes.each do |key|
      invalid_attributes << key if data[key].blank?
    end

    if invalid_attributes.present?
      raise ArgumentError.new("Required Attributes #{invalid_attributes} for #{self.class} are blank")
    end

    data
  end

  def decode
    JSON.parse(to_json)
  end
end
