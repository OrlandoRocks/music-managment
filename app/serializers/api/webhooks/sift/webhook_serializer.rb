class Api::Webhooks::Sift::WebhookSerializer < Api::Webhooks::BaseSerializer
  attr_reader :person_id

  attributes :decision, :releases_active, :note_options, :person_id

  def self.decode(request:)
    new({ request: request }).decode
  end

  def initialize(object, options = {})
    super object, options

    @required_attributes = [:body, :releases_active, :note_options, :person_id]

    @body = body.present? ? JSON.parse(body) : {}
    @person_id = body.dig("entity", "id")
  end

  def decision
    body.dig("decision", "id")
  end

  def releases_active
    return 0 if person_id.blank?

    Person.find(person_id).albums.distributed.approved.not_taken_down.count
  end

  def note_options
    return {} if person_id.blank?

    {
      related_id: person_id,
      related_type: "Person",
      ip_address: remote_ip,
      note: "Sift Automated Decision Marked as Suspicious".freeze,
      subject: "Sift Identified - Will Not Distribute".freeze,
      note_created_by_id: 0
    }
  end
end
