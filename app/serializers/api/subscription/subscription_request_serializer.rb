class Api::Subscription::SubscriptionRequestSerializer < Api::Subscription::BaseSerializer
  attributes :data

  def data
    Api::Subscription::SubscriptionSerializer.new(
      object.subscription,
      serializer_options: { root: false }
    ).serializable_hash
  end
end
