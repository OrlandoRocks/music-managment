class Api::Subscription::PaymentOptionsSerializer < Api::Subscription::BaseSerializer
  attributes :data

  def data
    { payment_options: construct_plans }
  end

  private

  def construct_plans
    [construct_paypal, construct_credit_cards].flatten
  end

  def construct_paypal
    { paypal: { email: object.paypal.try(:email) } }
  end

  def construct_credit_cards
    {
      credit_cards: object.credit_cards.map { |cc| serialize_credit_card(cc) }
    }
  end

  def serialize_credit_card(card)
    {
      type: card.cc_type,
      expiration_month: card.expiration_month,
      expiration_year: card.expiration_year,
      last_four_digits: card.last_four,
    }
  end
end
