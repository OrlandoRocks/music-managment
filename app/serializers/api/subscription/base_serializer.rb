class Api::Subscription::BaseSerializer < ActiveModel::Serializer
  attributes :status

  def status
    "success"
  end
end
