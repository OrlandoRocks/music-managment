class Api::Subscription::PlansSerializer < Api::Subscription::BaseSerializer
  attributes :data

  def data
    { plans: product_data }
  end

  private

  def product_data
    object.plans.map do |product|
      {
        price: product.price,
        currency: product.currency,
        display_name: product.display_name,
        product_type: product.product_type,
        discount: product&.discount
      }
    end
  end
end
