class Api::Subscription::SubscriptionSerializer < ActiveModel::Serializer
  attributes :subscription, :user

  def subscription
    {
      payment_plan: object.payment_plan,
      payment_channel: object.payment_channel,
      effective_date: object.effective_date,
      termination_date: object.termination_date,
      canceled_at: object.canceled_at
    }
  end

  def user
    {
      plan: object.plan,
      plan_expires_at: object.plan_expires_at
    }
  end
end
