class Api::Subscription::PurchaseHandlerSerializer < Api::Subscription::BaseSerializer
  attributes :data

  def data
    if object.gtm_invoice_data
      {
        "plan" => object.plan,
        "plan_expires_at" => object.plan_expires_at,
        "gtm_invoice_data" => object.gtm_invoice_data
      }
    else
      { "plan" => object.plan, "plan_expires_at" => object.plan_expires_at }
    end
  end
end
