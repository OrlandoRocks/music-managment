module AwsWrapper
  module Sdb
    CONFIG = {
      # TODO: Clean up environment access by removing these constants from the
      # global namespace and placing them in a credentials file.
      max_retries: 10,
      ssl_verify_peer: false
    }.freeze
  end
end
