module Resolvers
  class AuthenticatedResolver < Resolvers::Base
    # Classes subclassing this are invisible to unauthenticated users.
    #
    # GraphiQL currently doesn't provide for passing headers to its initializing schema request,
    # therefore fields using this class aren't visible in the schema, and usage
    # results in a "Field <fieldName> doesn't exist on type 'Query'" error.
    #
    # https://graphql-ruby.org/authorization/visibility
    def self.visible?(context)
      return true if Rails.env.development?

      super && context[:current_user].present?
    end

    def resolve
      raise Resolvers::Base::SubclassResponsibilityError
    end
  end
end
