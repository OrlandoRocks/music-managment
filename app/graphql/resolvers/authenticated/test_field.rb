# frozen_string_literal: true

module Resolvers
  class Authenticated::TestField < AuthenticatedResolver
    def resolve
      "Authed OK"
    end
  end
end
