# frozen_string_literal: true

module Resolvers
  class Authenticated::Release < AuthenticatedResolver
    argument :id, Integer,
             required: true,
             description: "Release ID"

    def resolve(id:)
      Album.find(id)
    end
  end
end
