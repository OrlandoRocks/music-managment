# frozen_string_literal: true

module Resolvers
  class Public::TestField < PublicResolver
    def resolve
      "Public OK"
    end
  end
end
