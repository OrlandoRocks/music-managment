# frozen_string_literal: true

module Resolvers
  class PublicResolver < Resolvers::Base
    class PublicResolverError < StandardError; end

    def resolve
      raise Resolvers::Base::SubclassResponsibilityError
    end

    def current_user
      raise PublicResolverError, "No current user"
    end
  end
end
