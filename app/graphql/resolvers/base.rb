module Resolvers
  class Base < GraphQL::Schema::Resolver
    # When trying to use the base, public or authenticated classes directly
    class SubclassResponsibilityError < StandardError; end

    def resolve
      raise SubclassResponsibilityError
    end

    def current_user
      raise SubclassResponsibilityError
    end
  end
end
