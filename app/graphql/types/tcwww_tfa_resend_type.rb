# frozen_string_literal: true

module Types
  class TCWWWTfaResendType < Types::BaseObject
    description "A TFA token"

    field :tfa_session_token, String, null: true
    field :errors, [Types::ErrorType], null: false
  end
end
