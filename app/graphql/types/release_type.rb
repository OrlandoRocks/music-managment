# frozen_string_literal: true

module Types
  class ReleaseType < Types::BaseObject
    description "A album, single or ringtone"

    field :name, String, null: false
    field :person_id, Integer, null: false

    def self.authorized?(object, context)
      super && (object.person_id == context[:current_user][:id])
    end
  end
end
