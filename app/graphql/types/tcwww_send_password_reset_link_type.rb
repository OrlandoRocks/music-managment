# frozen_string_literal: true

module Types
  class TCWWWSendPasswordResetLinkType < Types::BaseObject
    description "Sends a Password Reset Link to the User"

    field :errors, [Types::ErrorType], null: false
  end
end
