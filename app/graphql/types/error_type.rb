# frozen_string_literal: true

module Types
  class ErrorType < Types::BaseObject
    description "An error"

    field :message, String, null: true
  end
end
