# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :pub, String,
          null: false,
          resolver: Resolvers::Public::TestField,
          description: "A field visible to all users"

    field :auth, String,
          null: false,
          resolver: Resolvers::Authenticated::TestField,
          description: "A field invisible to unauthenticated users"

    field :release, type: Types::ReleaseType,
                    null: true,
                    resolver: Resolvers::Authenticated::Release,
                    description: "A release"
  end
end
