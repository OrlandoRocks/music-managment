# frozen_string_literal: true

module Types
  class TCWWWAuthenticateType < Types::BaseObject
    description "A JWT API token and nonce, or a TFA token"

    field :jwt, String, null: true
    field :nonce, String, null: true
    field :tfa_required, Boolean, null: false
    field :tfa_session_token, String, null: true
    field :errors, [Types::ErrorType], null: false
  end
end
