# frozen_string_literal: true

module Types
  class TCWWWTfAuthenticateType < Types::BaseObject
    description "A JWT API token and nonce"

    field :jwt, String, null: true
    field :nonce, String, null: true
    field :errors, [Types::ErrorType], null: false
  end
end
