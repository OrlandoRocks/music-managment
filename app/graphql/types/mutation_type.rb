module Types
  class MutationType < Types::BaseObject
    field :pw_authenticate, mutation: Mutations::Public::PwAuthenticate
    field :tf_authenticate, mutation: Mutations::Public::TfAuthenticate
    field :tfa_resend, mutation: Mutations::Public::TfaResend
    field :send_password_reset_link, mutation: Mutations::Public::SendPasswordResetLink
  end
end
