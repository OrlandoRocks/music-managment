# frozen_string_literal: true

module Mutations
  class Public::SendPasswordResetLink < PublicMutation
    NO_ERRORS = [].freeze

    argument :email, String, required: true

    type Types::TCWWWSendPasswordResetLinkType

    def resolve(email:)
      person = Person.find_by(email: email)
      return success unless person

      person.unlock_account if person.account_locked?

      url = "#{ENV['V2_BASE_URL']}/reset-password"

      if PasswordResetService.reset(person.email, :user, url: url)
        success
      else
        failure
      end
    end

    private

    def success
      {
        errors: NO_ERRORS
      }
    end

    def failure
      {
        errors: [GraphQL::ExecutionError.new("Something went wrong")]
      }
    end
  end
end
