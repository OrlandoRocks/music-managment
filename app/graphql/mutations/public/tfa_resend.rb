# frozen_string_literal: true

module Mutations
  class Public::TfaResend < PublicMutation
    argument :tfa_session_token, String, required: true

    type Types::TCWWWTfaResendType

    def resolve(tfa_session_token:)
      result = V2::Authentication::TfaResendService.call(params: { tfa_session_token: tfa_session_token })

      return success(result) if success?(result)

      failure
    end

    private

    def success?(result)
      return false if result.nil?

      result[:tfa_session_token].present?
    end

    def success(result)
      {
        tfa_session_token: result[:tfa_session_token],
        errors: Mutations::Base::NO_ERRORS
      }
    end

    def failure
      {
        tfa_session_token: nil,
        errors: [GraphQL::ExecutionError.new("Unauthorized", extensions: { code: "FORBIDDEN" })]
      }
    end
  end
end
