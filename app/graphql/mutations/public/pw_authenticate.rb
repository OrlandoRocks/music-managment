# frozen_string_literal: true

module Mutations
  class Public::PwAuthenticate < PublicMutation
    argument :email, String, required: true
    argument :password, String, required: true

    type Types::TCWWWAuthenticateType

    def resolve(email:, password:)
      result = V2::Authentication::AuthenticationService.call(
        params: { email: email, password: password },
        cookies: context[:cookies],
        request: context[:request]
      )

      return result if success?(result)

      failure(result)
    end

    private

    def success?(result)
      result[:errors].empty?
    end

    def failure(result)
      result.merge(
        {
          errors: [GraphQL::ExecutionError.new("Unauthorized", extensions: { code: "FORBIDDEN" })]
        }
      )
    end
  end
end
