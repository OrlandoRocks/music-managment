# frozen_string_literal: true

module Mutations
  class Public::TfAuthenticate < PublicMutation
    argument :auth_code, String, required: true
    argument :tfa_session_token, String, required: true

    type Types::TCWWWTfAuthenticateType

    def resolve(auth_code:, tfa_session_token:)
      result = V2::Authentication::AuthenticationService.call(
        params: { auth_code: auth_code, tfa_session_token: tfa_session_token },
        cookies: context[:cookies],
        request: context[:request]
      )

      return result if success?(result)

      failure(result)
    end

    private

    def success?(result)
      result[:errors].empty?
    end

    def failure(result)
      result.merge(
        {
          errors: [GraphQL::ExecutionError.new("Unauthorized", extensions: { code: "FORBIDDEN" })]
        }
      )
    end
  end
end
