# frozen_string_literal: true

module Mutations
  class Authenticated::TestField < AuthenticatedMutation
    def resolve
      "Authed OK"
    end
  end
end
