# frozen_string_literal: true

module Mutations
  class Base < GraphQL::Schema::RelayClassicMutation
    # When trying to use the base, public or authenticated classes directly
    class SubclassResponsibilityError < StandardError; end

    NO_ERRORS = [].freeze

    argument_class Types::BaseArgument
    field_class Types::BaseField
    input_object_class Types::BaseInputObject
    object_class Types::BaseObject

    def resolve
      raise SubclassResponsibilityError
    end

    def current_user
      raise SubclassResponsibilityError
    end
  end
end
