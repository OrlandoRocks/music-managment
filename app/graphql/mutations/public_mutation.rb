# frozen_string_literal: true

module Mutations
  class PublicMutation < Mutations::Base
    class PublicMutationError < StandardError; end

    def resolve
      raise Mutations::Base::SubclassResponsibilityError
    end
  end
end
