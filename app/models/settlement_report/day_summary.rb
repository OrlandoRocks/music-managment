# frozen_string_literal: true

class SettlementReport::DaySummary
  extend MoneyField
  def initialize(fake_invoice, fake_settlements, currency)
    self.currency = currency
    self.day = fake_invoice.day.to_date
    self.people_count = fake_invoice.people_count.to_i
    self.total_cents = fake_invoice.total_cents.to_i
    self.paypal_amount_cents = fake_settlements["PaypalTransaction"].settlement_cents.to_i rescue 0
    self.tunecore_amount_cents = fake_settlements["PersonTransaction"].settlement_cents.to_i rescue 0
    self.adyen_amount_cents = fake_settlements["AdyenTransaction"].settlement_cents.to_i rescue 0
    set_credit_credit_card_amount(fake_settlements)
  end

  def self.report(country_website, day = nil)
    start_time = Time.parse(day).beginning_of_month rescue raise(ActiveRecord::RecordNotFound, "unable to generate report for #{month.inspect}")
    end_time = start_time.next_month - 1
    report_conditions = ["invoices.settled_at between ? and ? and invoices.currency = ? and p.country_website_id = ?", start_time, end_time, country_website.currency, country_website.id]

    fake_invoices = Invoice
                    .select(%q|DATE_FORMAT(invoices.settled_at, '%Y-%m-%d') as day,
              count(distinct invoices.person_id) as people_count,
              sum(invoices.final_settlement_amount_cents) as total_cents|)
                    .joins("inner join people p on p.id = invoices.person_id")
                    .group("day")
                    .where(report_conditions)
                    .order("day ASC")

    fake_settlements = InvoiceSettlement
                       .select(%q|DATE_FORMAT(invoices.settled_at, '%Y-%m-%d') as day,
              sum(invoice_settlements.settlement_amount_cents) as settlement_cents,
              invoice_settlements.source_type as settlement_type|)
                       .joins("inner join invoices on invoice_settlements.invoice_id = invoices.id", "inner join people p on p.id = invoices.person_id")
                       .where(report_conditions)
                       .group("day, settlement_type")
                       .order("day")
                       .group_by(&:day)

    fake_invoices.collect { |fake_invoice| new(fake_invoice, Array(fake_settlements[fake_invoice.day]).index_by(&:settlement_type), country_website.currency) }
  end

  def self.csv_headers
    [
      "Day",
      "Distinct Customers",
      "Paid with Paypal",
      "Paid with Balance",
      "Paid with Credit Card",
      "Paid with Adyen",
      "Settlements Total"
    ]
  end

  def to_csv_a
    [
      day.strftime("%Y-%m-%d"),
      people_count,
      paypal_amount,
      tunecore_amount,
      credit_card_amount,
      adyen_amount,
      total
    ]
  end

  attr_reader :day, :people_count, :total_cents, :paypal_amount_cents,
              :tunecore_amount_cents, :braintree_amount_cents, :payments_os_amount_cents,
              :credit_card_amount_cents, :adyen_amount_cents, :currency

  money_reader :total, :paypal_amount, :tunecore_amount, :braintree_amount,
               :payments_os_amount, :credit_card_amount, :adyen_amount

  def set_credit_credit_card_amount(fake_settlements)
    self.braintree_amount_cents = fake_settlements["BraintreeTransaction"].settlement_cents.to_i rescue 0
    self.payments_os_amount_cents = fake_settlements["PaymentsOSTransaction"].settlement_cents.to_i rescue 0
    self.credit_card_amount_cents = braintree_amount_cents + payments_os_amount_cents
  end

  def discrepancy?
    total_cents != (paypal_amount_cents + tunecore_amount_cents + credit_card_amount_cents + adyen_amount_cents)
  end

  protected

  attr_writer :day, :people_count, :total_cents, :paypal_amount_cents,
              :tunecore_amount_cents, :braintree_amount_cents, :credit_card_amount_cents,
              :payments_os_amount_cents, :adyen_amount_cents, :currency
end
