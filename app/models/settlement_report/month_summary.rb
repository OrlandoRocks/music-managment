# frozen_string_literal: true

class SettlementReport::MonthSummary
  extend MoneyField

  def initialize(fake_invoice, fake_settlements, currency)
    self.currency = currency
    self.month = fake_invoice.month.to_date
    self.people_count = fake_invoice.people_count.to_i
    self.paypal_amount_cents = fake_settlements["PaypalTransaction"].settlement_cents.to_i rescue 0
    self.tunecore_amount_cents = fake_settlements["PersonTransaction"].settlement_cents.to_i rescue 0
    self.adyen_amount_cents = fake_settlements["AdyenTransaction"].settlement_cents.to_i rescue 0
    set_credit_credit_card_amount(fake_settlements)
    self.total_cents = paypal_amount_cents + tunecore_amount_cents + credit_card_amount_cents + adyen_amount_cents
  end

  def self.report(country_website, month = nil)
    report_conditions = ["invoices.settled_at is not null and invoices.currency = ? and p.country_website_id = ?", country_website.currency, country_website.id]
    if month.present?
      start_time = month.to_time.beginning_of_month rescue raise(ActiveRecord::RecordNotFound, "unable to generate report for #{month.inspect}")
      end_time = start_time.next_month - 1
      report_conditions = ["p.country = ? and invoices.settled_at between ? and ?", country_website.country, start_time, end_time]
    end

    fake_invoices = Invoice
                    .select(%q|DATE_FORMAT(invoices.settled_at, '%Y-%m-1') as month,
              count(distinct invoices.person_id) as people_count,
              sum(invoices.final_settlement_amount_cents) as total_cents|)
                    .joins("inner join people p on p.id = invoices.person_id")
                    .group("month")
                    .where(report_conditions)
                    .order("month DESC")

    fake_settlements = InvoiceSettlement
                       .select(%q|DATE_FORMAT(invoices.settled_at, '%Y-%m-1') as month,
              sum(invoice_settlements.settlement_amount_cents) as settlement_cents,
              invoice_settlements.source_type as settlement_type|)
                       .joins("inner join invoices on invoice_settlements.invoice_id = invoices.id", "inner join people p on p.id = invoices.person_id")
                       .where(report_conditions)
                       .group("month, settlement_type")
                       .order("month")
                       .group_by(&:month)

    rv =
      fake_invoices.collect do |fake_invoice|
        new(fake_invoice, Array(fake_settlements[fake_invoice.month]).index_by(&:settlement_type), country_website.currency)
      end

    month.blank? ? rv : rv.first
  end

  def self.csv_headers
    [
      "Month",
      "Distinct Customers",
      "Paid with Paypal",
      "Paid with Balance",
      "Paid with Credit Card",
      "Paid with Adyen",
      "Settlements Total"
    ]
  end

  def to_csv_a
    [
      month.strftime("%Y-%m"),
      people_count,
      paypal_amount,
      tunecore_amount,
      credit_card_amount,
      adyen_amount,
      total
    ]
  end

  attr_reader :month, :people_count, :total_cents, :paypal_amount_cents,
              :tunecore_amount_cents, :credit_card_amount_cents, :braintree_amount_cents,
              :payments_os_amount_cents, :adyen_amount_cents, :currency

  money_reader :total, :paypal_amount, :tunecore_amount, :braintree_amount,
               :payments_os_amount, :credit_card_amount, :adyen_amount

  def set_credit_credit_card_amount(fake_settlements)
    self.braintree_amount_cents = fake_settlements["BraintreeTransaction"].settlement_cents.to_i rescue 0
    self.payments_os_amount_cents = fake_settlements["PaymentsOSTransaction"].settlement_cents.to_i rescue 0
    self.credit_card_amount_cents = braintree_amount_cents + payments_os_amount_cents
  end

  def to_param
    month.to_formatted_s(:db)
  end

  def discrepancy?
    total_cents != (paypal_amount_cents + tunecore_amount_cents + credit_card_amount_cents + adyen_amount_cents)
  end

  protected

  attr_writer :month, :people_count, :total_cents, :paypal_amount_cents,
              :tunecore_amount_cents, :braintree_amount_cents, :credit_card_amount_cents,
              :payments_os_amount_cents, :currency, :adyen_amount_cents
end
