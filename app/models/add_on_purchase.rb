class AddOnPurchase < ApplicationRecord
  belongs_to :person
  belongs_to :product

  validates :person, presence: true
  validates :product, presence: true
end
