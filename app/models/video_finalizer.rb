class VideoFinalizer
  attr_reader :video

  def self.factory(video)
    new(video)
  end

  def initialize(video)
    @video = video
  end

  def already_finalized?
    video.finalized?
  end

  def permit_metadata_changes?
    !video.finalized?
  end

  def has_accepted_format?
    video.accepted_format?
  end

  def needs_accepted_format?
    !has_accepted_format?
  end

  def permit_format_change?
    # needs to be implemented (when can this be changed? or seem to be changed by the user, we aren't actually storing the results, only that they complied with our guidelines...)
    true
  end

  def steps_required
    [has_accepted_format?].count(&:!)
  end

  def has_required_elements?
    has_accepted_format?
  end

  def waiting_for_media_delivery?
    has_required_elements? && !media_delivered? && already_paid?
  end

  def media_delivered?
    video.metadata_picked_up?
  end

  def should_pay?
    !already_paid? && has_required_elements?
  end

  def already_paid?
    Inventory.used?(video) # an outstanding purchase will spring into existence if needed
  end

  def should_finalize?
    has_required_elements? && !already_finalized?
  end

  def stuck_in_invoice?
    !!current_invoice
  end

  def current_invoice
    (outstanding_purchase or return).invoice
  end

  def ready_to_purchase?
    (outstanding_purchase or return false).ready_to_purchase?(video)
  end

  def outstanding_purchase?
    !!outstanding_purchase
  end

  def outstanding_purchase
    @outstanding_purchase ||= (Product.purchase(video.person, video) || nil)
    (@outstanding_purchase == :nil) ? nil : @outstanding_purchase
  end
end
