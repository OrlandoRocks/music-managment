class TransferMetadatum < ApplicationRecord
  belongs_to :trackable, polymorphic: true
  belongs_to :login_event

  after_create :run_auto_approve_transfer_services!

  def auto_approve!
    update!(auto_approved: true)
  end

  def complete_auto_approval!
    update!(auto_approved_at: Time.current)
  end

  def roll_back_approval!
    update!(auto_approved: false)
  end

  private

  def run_auto_approve_transfer_services!
    return unless FeatureFlipper.show_feature?(:auto_approved_withdrawals, trackable.person.id)

    AutoApproveTransfers::EvaluationService.new(self).call!
  end
end
