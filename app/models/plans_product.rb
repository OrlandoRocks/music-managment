# frozen_string_literal: true

class PlansProduct < ApplicationRecord
  belongs_to :plan
  belongs_to :product
end
