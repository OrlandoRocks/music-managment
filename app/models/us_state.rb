# 2010-07-12 -- DCD -- added a STATES_AND_TERRITORIES array for forms, so we don't have to hit the DB to load states. Also, this array does not include the states
# 'Other', 'Federated States of Micronesia', 'Palau', 'Marsahall Islands', and 'Northern Mariana Islands' which the DB does contain.

# This model is will be used primarly to fetch all country records for name lookup
class UsState < ApplicationRecord
  has_many :tour_dates
  has_many :product_tax_rates, as: :locale

  TERRITORIES =
    [
      ["AMERICAN SAMOA", "AS"],
      ["GUAM", "GU"],
      ["PUERTO RICO", "PR"],
      ["VIRGIN ISLANDS", "VI"]
    ]

  STATES =
    [
      ["ALABAMA", "AL"],
      ["ALASKA", "AK"],
      ["ARIZONA", "AZ"],
      ["ARKANSAS", "AR"],
      ["CALIFORNIA", "CA"],
      ["COLORADO", "CO"],
      ["CONNECTICUT", "CT"],
      ["DELAWARE", "DE"],
      ["DISTRICT OF COLUMBIA", "DC"],
      ["FLORIDA", "FL"],
      ["GEORGIA", "GA"],
      ["HAWAII", "HI"],
      ["IDAHO", "ID"],
      ["ILLINOIS", "IL"],
      ["INDIANA", "IN"],
      ["IOWA", "IA"],
      ["KANSAS", "KS"],
      ["KENTUCKY", "KY"],
      ["LOUISIANA", "LA"],
      ["MAINE", "ME"],
      ["MARYLAND", "MD"],
      ["MASSACHUSETTS", "MA"],
      ["MICHIGAN", "MI"],
      ["MINNESOTA", "MN"],
      ["MISSISSIPPI", "MS"],
      ["MISSOURI", "MO"],
      ["MONTANA", "MT"],
      ["NEBRASKA", "NE"],
      ["NEVADA", "NV"],
      ["NEW HAMPSHIRE", "NH"],
      ["NEW JERSEY", "NJ"],
      ["NEW MEXICO", "NM"],
      ["NEW YORK", "NY"],
      ["NORTH CAROLINA", "NC"],
      ["NORTH DAKOTA", "ND"],
      ["OHIO", "OH"],
      ["OKLAHOMA", "OK"],
      ["OREGON", "OR"],
      ["PENNSYLVANIA", "PA"],
      ["RHODE ISLAND", "RI"],
      ["SOUTH CAROLINA", "SC"],
      ["SOUTH DAKOTA", "SD"],
      ["TENNESSEE", "TN"],
      ["TEXAS", "TX"],
      ["UTAH", "UT"],
      ["VERMONT", "VT"],
      ["VIRGINIA", "VA"],
      ["WASHINGTON", "WA"],
      ["WEST VIRGINIA", "WV"],
      ["WISCONSIN", "WI"],
      ["WYOMING", "WY"]
    ]
  STATES_AND_TERRITORIES = STATES + [["--------------", ""]] + TERRITORIES

  scope :by_name_or_abbr,
        ->(key) {
          where(name: key).or(where(abbreviation: key))
        }
end
