class ProductTaxRate < ApplicationRecord
  belongs_to :locale, polymorphic: true
end
