class RoyaltySubStore < ApplicationRecord
  belongs_to :royalty_store
  has_many :user_store_royalty_rates, dependent: :destroy

  validates :name, uniqueness: true, presence: true
  validates :royalty_store, presence: true
  validates :royalty_rate, presence: true
  validates :tarif_rate, presence: true
  validates :sales_period_type, presence: true
  validates :royalty_source, presence: true
end
