# frozen_string_literal: true

class RoyaltySplitSong < ApplicationRecord
  include RoyaltySplitLoggable

  belongs_to :royalty_split, inverse_of: :royalty_split_songs
  belongs_to :song, inverse_of: :royalty_split_song

  validates :song, presence: true, uniqueness: true
  validates :royalty_split, presence: true

  delegate :recipients, to: :royalty_split
  delegate :album, to: :song
  delegate :owner, to: :royalty_split
end
