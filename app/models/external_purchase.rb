class ExternalPurchase < ActiveRecord::Base
  belongs_to  :external_product
  belongs_to  :person

  has_many :external_purchase_items
  has_many :songs, through: :external_purchase_items, source: :related_item, source_type: "Song"

  validates :external_product, presence: true
  validates :person, presence: true
  validates :currency, presence: true

  scope :unpaid, -> { where(paid_at: nil) }
  scope :paid, -> { where.not(paid_at: nil) }

  before_save :set_currency

  private

  def set_currency
    self.currency = person.currency
  end
end
