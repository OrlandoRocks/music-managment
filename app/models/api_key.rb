class ApiKey < ApplicationRecord
  validates :key, uniqueness: true
  validates :partner_name, uniqueness: true
end
