# frozen_string_literal: true

class CopyrightStateTransition < ApplicationRecord
  belongs_to :person
  belongs_to :copyright_status

  enum state: {
    approved: "approved",
    pending: "pending",
    rejected: "rejected"
  }
end
