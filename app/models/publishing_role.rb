class PublishingRole < ApplicationRecord
  ON_BEHALF_OF_OTHER_ID = 4
  ON_BEHALF_OF_SELF_ID  = 5

  validates :title, presence: true

  scope :on_behalf_of_self,    -> { where(title: "self") }
  scope :on_behalf_of_another, -> { where(publishing_role_t[:title].not_eq("self")) }

  def self.publishing_role_t
    arel_table
  end

  def on_behalf_of_self?
    title == "self"
  end
end
