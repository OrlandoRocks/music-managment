class Album < ApplicationRecord
  include Tunecore::Lockable
  include Tunecore::Creativeable
  include Tunecore::Genreable
  include Tunecore::DistributableChecklist
  include Tunecore::Salepointable
  include Tunecore::Albums::Artists
  include Tunecore::Albums::Creatives
  include Tunecore::Albums::Dates
  include Tunecore::Albums::Petri
  include Tunecore::Albums::Songs
  include Tunecore::Albums::Stores
  include Tunecore::Albums::State
  include Tunecore::Albums::Streaming
  include Tunecore::Albums::Takedown
  extend  Tunecore::Albums::Discography
  include Upcable
  include Linkable
  include ArelTableMethods
  include Jsonable
  prepend SalepointsWithInventoryChecks
  include SpecializedReleaseable
  include AlbumMethods

  self.inheritance_column = "album_type"

  has_paper_trail on: [:update], if: proc { |a| a.ready_for_distribution? }

  attr_accessor :album_name, :cached_renewal

  SUBSCRIPTION_SUPPORTED_TYPES = ["Album", "Single"]
  PREORDER_SUPPORTED_TYPES = ["Album", "Single"]
  LEGACY_LANGUAGE_CODE_MAP = {
    "en-US" => "en",
    "nl-NL" => "nl",
    "fi-FI" => "fi",
    "it-IT" => "it",
    "ja-JP" => "ja",
    "no-NO" => "no"
  }
  TIMED_RELEASE_RELATIVE_TIME = "relative_time".freeze
  TIMED_RELEASE_ABSOLUTE_TIME = "absolute_time".freeze
  TIMED_RELEASE_TYPES = [TIMED_RELEASE_ABSOLUTE_TIME, TIMED_RELEASE_RELATIVE_TIME].freeze
  MAX_TRACK_LIMIT = 100
  MIN_DAYS_UNTIL_PREORDER = 10
  OPTIMISTIC_APPROVAL_STATUSES = ["APPROVED", "NEEDS REVIEW"].map(&:freeze).freeze
  SONGWRITER = "songwriter".freeze
  UNFINALIZED_RINGTONE_ATTRIBUTES = { finalized_at: nil }.freeze
  UNFINALIZED_ATTRIBUTES = {
    finalized_at: nil,
    created_with: SONGWRITER,
    created_with_songwriter: true
  }.freeze

  belongs_to :person
  belongs_to :label
  belongs_to :low_risk_account, foreign_key: :person_id, primary_key: :person_id
  belongs_to :metadata_language_code, class_name: "LanguageCode"

  has_one :booklet
  has_one :salepoint_subscription, dependent: :destroy
  has_one :credit_usage, as: :related, dependent: :destroy
  has_one :plan_credit_usage, as: :related, dependent: :destroy
  has_one :inventory_usage, as: :related, dependent: :destroy
  has_one :preorder_purchase, dependent: :destroy
  has_one :album_itunes_status
  has_one :album_flagged_content
  has_one :non_tunecore_album
  has_one :copyright_status, as: :related
  has_one :distribution_api_album
  has_one :distribution_api_service, through: :distribution_api_album

  has_many :copyright_documents, as: :related, dependent: :destroy
  has_many :copyright_state_transitions, through: :copyright_status
  has_many :album_intakes
  has_many :annual_renewals
  has_many :creatives, -> { order "creatives.id" }, as: :creativeable
  has_many :artists,
           -> { where("creatives.role = 'primary_artist'").order("creatives.id") },
           through: :creatives
  has_many :related_purchases, as: :related, class_name: "Purchase"
  has_many :salepoints, -> { distinct }, as: :salepointable, dependent: :destroy
  has_many :sales_records, as: :related
  has_many :stores, -> { select("stores.*").distinct }, through: :salepoints
  has_many :notes, -> { order "id DESC" }, as: :related
  has_many :purchases, as: :related
  has_many :non_tunecore_songs, through: :non_tunecore_album
  has_many :copyrights, as: :copyrightable, dependent: :destroy
  has_many :song_copyrights, through: :songs, source: :copyrights
  has_many :distribution_api_songs, through: :songs
  has_many :royalty_split_songs, through: :songs
  has_many :royalty_splits, -> { distinct }, through: :royalty_split_songs, inverse_of: :albums
  has_many :royalty_split_recipients, -> { distinct }, through: :royalty_splits, source: :recipients, inverse_of: :albums
  has_many :spatial_audios, through: :songs

  has_many :videos
  has_many :urls
  has_many :covers
  has_many :review_audits
  has_many :album_countries
  has_many :salepoint_preorder_data, through: :preorder_purchase, dependent: :destroy
  has_many :youtubesr_salepoints,
           -> { where "store_id = 48" },
           class_name: "Salepoint",
           as: :salepointable
  has_many :petri_bundles
  has_many :believe_errors
  has_many :distributions, through: :petri_bundles
  has_many :track_monetizations, through: :songs
  has_one :petri_bundle, -> { where.not(state: "dismissed") }

  has_many :renewal_items, as: :related
  has_many :renewals, through: :renewal_items
  has_many :renewal_histories, through: :renewals
  has_many :scrapi_jobs, through: :songs

  has_many :invoice_logs, primary_key: "id", foreign_key: "purchase_no_purchase_album_id", class_name: "InvoiceLog"
  has_many :copyright_claims, as: :asset
  has_one :spotify_external_service_id,
          -> {
            where(service_name: "spotify")
              .where.not(identifier: ["unavailable", nil])
          },
          as: :linkable,
          class_name: "ExternalServiceId"

  has_one :last_review_audit,
          -> {
            left_outer_joins(:album)
              .where("review_audits.event = albums.legal_review_state")
              .order(created_at: :desc)
              .includes(:person)
          },
          class_name: "ReviewAudit"

  scope :deleted, -> { where(is_deleted: true) }
  scope :not_deleted, -> { where(is_deleted: false) }
  scope :have_been_live, -> { where("known_live_on IS NOT NULL") }
  scope :published, -> { have_been_live.not_deleted }
  scope :unpublished, -> { where(known_live_on: nil) }
  scope :finalized, -> { where("albums.finalized_at IS NOT NULL") }
  scope :distributed, -> { finalized.not_deleted }
  scope :undistributed, -> { where(finalized_at: nil) }
  scope :full_albums, -> { where(album_type: "Album") }
  scope :singles, -> { where(album_type: "Single") }
  scope :ringtones, -> { where(album_type: "Ringtone") }
  scope :not_ringtones, -> { where("album_type != 'Ringtone'") }
  scope :by_date_desc, -> { order("created_at DESC") }
  scope :by_id_desc, -> { order("id DESC") }
  scope :by_finalized_asc, -> { order("albums.finalized_at ASC, albums.id DESC") }
  scope :by_finalized_desc, -> { order("albums.finalized_at DESC, albums.id DESC") }
  scope :not_taken_down, -> { where(albums: { takedown_at: nil }) }
  scope :taken_down, -> { where("takedown_at IS NOT NULL") }
  scope :approved, -> { where(legal_review_state: "APPROVED") }
  scope :approved_or_pending_approval, -> { where(legal_review_state: OPTIMISTIC_APPROVAL_STATUSES) }
  scope :paid, -> { where(payment_applied: true) }
  scope :spotify_or_apple,
        -> {
          includes(:external_service_ids).where(external_service_ids: { service_name: ["Apple", "Spotify"] })
        }

  scope :live_releases,
        -> {
          select(
            "
           albums.*,
           (
            SELECT max(expires_at)
            FROM renewal_history rh
            INNER JOIN renewals r ON r.id = rh.renewal_id
            INNER JOIN renewal_items ri ON ri.renewal_id = r.id
            WHERE ri.related_type = 'Album'
            AND ri.related_id = albums.id
            AND r.takedown_at IS null
            AND rh.expires_at > current_timestamp
          ) renewal_date"
          )
            .from("albums")
            .where("albums.takedown_at IS NULL AND albums.payment_applied = 1")
            .order("renewal_date ASC")
        }

  scope :exclude_genre,
        ->(genre) {
          where("albums.primary_genre_id != :genre_id AND (albums.secondary_genre_id != :genre_id OR albums.secondary_genre_id IS NULL)", genre_id: Genre.find_by(name: genre).id)
        }

  scope :for_amazon_takedown,
        ->(start, finish) {
          joins(
            "INNER JOIN salepoints spts ON spts.salepointable_id = albums.id AND spts.salepointable_type = 'Album' AND spts.store_id = #{Store::AOD_STORE_ID}
      INNER JOIN upcs tunecore_upc ON tunecore_upc.upcable_id = albums.id AND tunecore_upc.upcable_type = 'Album' AND tunecore_upc.inactive = 0 AND tunecore_upc.upc_type = 'tunecore'
      LEFT JOIN upcs optional_upc ON optional_upc.upcable_id = albums.id AND optional_upc.upcable_type = 'Album' AND optional_upc.inactive = 0 AND optional_upc.upc_type = 'optional'"
          )
            .where("spts.takedown_at BETWEEN ? AND  ?", start, finish)
            .includes(:optional_upcs, :tunecore_upcs, { creatives: :artist }, :salepoints)
            .order("spts.takedown_at ASC")
        }

  scope :for_takedown_by_day,
        ->(expire_date, monthly) {
          select("albums.*, rh.expires")
            .from("renewals r")
            .joins(<<-SQL.strip_heredoc)
        INNER JOIN (SELECT MAX(expires_at) AS expires, renewal_id FROM renewal_history
        GROUP BY renewal_id) rh ON rh.renewal_id=r.id
        INNER JOIN renewal_items ri ON ri.renewal_id = r.id
        INNER JOIN product_items pi on pi.id = r.item_to_renew_id and r.item_to_renew_type = 'ProductItem'
        INNER JOIN albums ON albums.id = ri.related_id AND ri.related_type = 'Album'
        LEFT JOIN person_plans on person_plans.person_id = albums.person_id
            SQL
            .where("r.takedown_at IS NULL AND
              (DATE_FORMAT(rh.expires, '%Y-%m-%d') > '2020-01-01' and DATE_FORMAT(rh.expires, '%Y-%m-%d') <= ?
                OR
              (r.canceled_at IS NOT NULL AND DATE(rh.expires) < CURDATE() ) )
              #{'AND pi.renewal_interval = \'month\'' if monthly}",
                   expire_date)
            .where(person_plans: { id: nil })
            .includes(:tunecore_upcs, :salepoints, :upcs, :optional_upcs)
        }

  scope :needs_or_has_been_reviewed, -> { where.not(legal_review_state: "DO NOT REVIEW") }
  scope :finalized_at_within,
        ->(start_time, end_time) {
          where("? <= albums.finalized_at AND albums.finalized_at < ?", start_time, end_time)
        }
  scope :review_audited_within,
        ->(start_time, end_time) {
          joins("INNER JOIN ( SELECT MAX(created_at) AS created, album_id FROM review_audits GROUP BY album_id) ra ON ra.album_id = albums.id")
            .where("ra.created > ? AND ra.created < ?", start_time, end_time)
        }

  scope :missing_salepoint,
        ->(sp_store) {
          select("DISTINCT(albums.id)")
            .joins(<<-SQL.strip_heredoc)
        LEFT OUTER JOIN salepoints sp ON albums.id = sp.salepointable_id
          AND sp.salepointable_type = 'Album'
          AND #{sanitize_sql_array(["store_id = '%s'", sp_store.id])}
        INNER JOIN salepointable_stores sps ON albums.album_type = sps.salepointable_type
          AND #{sanitize_sql_array(["sps.store_id = '%s'", sp_store.id])}
            SQL
            .where("sp.id IS NULL")
        }

  scope :salepoints_not_bought,
        ->(sp_store) {
          select("DISTINCT(albums.id)")
            .joins(<<-SQL.strip_heredoc)
        LEFT OUTER JOIN salepoints sp ON albums.id = sp.salepointable_id
          AND sp.salepointable_type = 'Album'
          AND #{sanitize_sql_array(["store_id = '%s'", sp_store.id])}
        LEFT OUTER JOIN purchases p ON sp.id = p.related_id
          AND p.related_type = 'Salepoint'
        INNER JOIN salepointable_stores sps ON albums.album_type = sps.salepointable_type
          AND #{sanitize_sql_array(["sps.store_id = '%s'", sp_store.id])}
            SQL
            .where("p.id IS NULL AND ((sp.payment_applied = 0 AND sp.finalized_at IS NULL) OR sp.id IS NULL)")
        }

  scope :genre_filter, ->(genre_id) { where("primary_genre_id != ?", genre_id) }
  scope :not_rejected, -> { where("legal_review_state != 'REJECTED'") }

  scope :ready_to_renew,
        ->(end_date) {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN renewal_items ri ON ri.related_id = albums.id AND ri.related_type = 'Album'
      INNER JOIN renewals on renewals.id = ri.renewal_id
      LEFT JOIN (SELECT MAX(expires_at) AS expires, renewal_id FROM renewal_history GROUP BY renewal_id) h ON renewals.id=h.renewal_id
      LEFT JOIN petri_bundles ON albums.id = petri_bundles.album_id
      LEFT JOIN distributions ON petri_bundles.id = distributions.petri_bundle_id
        AND distributions.state NOT IN ('new', 'pending_approval', 'blocked')
          SQL
            .where(
              "expires >= ?
        AND expires <= ?
        AND renewals.canceled_at IS NULL
        AND renewals.takedown_at IS NULL
        AND petri_bundles.id IS NULL
        AND distributions.id IS NULL
        AND albums.legal_review_state = ?",
              end_date - 1.day,
              end_date,
              "REJECTED"
            )
        }

  scope :needs_review,
        -> {
          joins("LEFT OUTER JOIN review_audits ON albums.id = review_audits.album_id")
            .where(
              takedown_at: nil,
              legal_review_state: "NEEDS REVIEW"
            )
            .where.not(finalized_at: nil)
            .distinct
        }

  scope :distributed_albums,
        -> {
          joins(:distributions)
            .where(takedown_at: nil)
            .where(distributions: { state: "delivered" })
        }

  scope :distributed_albums_via_distributor,
        -> {
          joins(:distributions)
            .where(takedown_at: nil)
            .where(distributions: { state: "delivered_via_tc_distributor" })
        }

  scope :distributed_albums_via_www_or_distributor,
        -> {
          distributed_albums.or(
            distributed_albums_via_distributor
          )
        }

  scope :crt_list_view_query,
        ->(sort_by, sort_order, legal_review_state, limit: 100) {
          needs_or_has_been_reviewed
            .not_taken_down
            .finalized_at_within(1.year.ago.localtime, Time.current)
            .limit(limit)
            .includes(
              { creatives: :artist },
              { credit_usage: :purchase },
              :purchases,
              :last_review_audit
            ).sort_albums(sort_by, sort_order, legal_review_state)
        }

  scope :search_by_album_id,
        ->(album_id) {
          where(albums: { id: album_id })
        }

  scope :search_by_album_name,
        ->(album_name) {
          where("albums.name LIKE ?", "%#{album_name}%")
        }

  scope :search_by_artist_name,
        ->(artist_name) {
          joins({ creatives: :artist })
            .where("lower(artists.name) LIKE ?", "%#{artist_name.downcase}%").distinct
        }

  scope :search_by_person_id,
        ->(person_id) {
          joins(:person).where(people: { id: person_id })
        }

  scope :search_by_person_name,
        ->(person_name) {
          joins(:person).where("people.name LIKE ?", "%#{person_name}%")
        }

  scope :search_by_legal_review_state,
        ->(legal_review_state) {
          where(legal_review_state: legal_review_state)
        }

  scope :search_by_language_code,
        ->(language_code) {
          where(language_code: language_code)
        }

  scope :search_by_country_website_id,
        ->(country_website_id) {
          joins(:person).where(people: { country_website_id: country_website_id })
        }

  scope :search_by_paid_at_date,
        ->(start_date:, end_date:) {
          left_outer_joins(last_review_audit: :album)
            .left_outer_joins(:purchases)
            .where(purchases: { paid_at: start_date.to_date..end_date.to_date })
            .distinct
        }

  scope :search_by_last_review_date,
        ->(start_date:, end_date:) {
          left_outer_joins(last_review_audit: :album)
            .where(review_audits: { created_at: start_date.to_date..end_date.to_date })
            .distinct
        }

  scope :search_by_release_date,
        ->(start_date:, end_date:) {
          where(golive_date: start_date.to_date..end_date.to_date)
        }

  # Added to support sorting for ReTool
  scope :sort_albums,
        ->(sort_by, sort_order, legal_review_state) {
          if (sort_by == "paid_at")
            left_outer_joins({ credit_usage: :purchase }, :purchases)
              .order("CASE WHEN purchases.paid_at IS NOT NULL THEN purchases.paid_at ELSE purchases_albums.paid_at END #{sort_order}")
          elsif legal_review_state == "NEEDS REVIEW"
            if (["review_audits.created_at", "people.name", "review_audits.event", "review_audits.note"].include? sort_by)
              order("albums.finalized_at asc")
            else
              order("#{sort_by} #{sort_order}")
            end
          elsif ["review_audits.created_at", "people.name", "review_audits.event", "review_audits.note"].include? sort_by
            joins("INNER JOIN `review_audits` ON `review_audits`.`album_id` = `albums`.`id` AND `review_audits`.`event` = `albums`.`legal_review_state` INNER JOIN people ON `people`.`id` =  `review_audits`.`person_id`")
              .where("review_audits.id = (SELECT MAX(ra.id) FROM review_audits ra WHERE ra.album_id = albums.id AND ra.event = albums.legal_review_state)")
              .order("#{sort_by} #{sort_order}")
          else
            order("#{sort_by} #{sort_order}")
          end
        }
  scope :with_active_creatives, -> do
    # this is the inverse of the scope on Creative, #active
    joins(creatives: :artist)
      .where(albums: { takedown_at: nil, payment_applied: true },
             creatives: { role: Creative::PRIMARY_ARTIST_ROLE })
  end

  scope :finalized_store_salepoints, ->(store_short_name) {
    paid
      .not_taken_down
      .finalized
      .joins(salepoints: :store)
      .where(salepoints: { takedown_at: nil })
      .where.not(salepoints: { finalized_at: nil })
      .where(salepoints: { stores: { short_name: store_short_name } })
  }
  scope :in_cart, -> do
    not_taken_down
      .not_deleted
      .joins("LEFT OUTER JOIN purchases p1 ON albums.id = p1.related_id AND p1.related_type = 'Album'")
      .joins(
        "LEFT OUTER JOIN credit_usages ON albums.id = credit_usages.related_id " \
        "AND credit_usages.related_type = 'Album'"
      )
      .joins("LEFT OUTER JOIN purchases p2 ON credit_usages.id = p2.related_id AND p2.related_type = 'CreditUsage'")
      .where("p1.id IS NOT NULL OR p2.id IS NOT NULL")
      .where("p1.paid_at IS NULL OR p1.paid_at = ''")
      .where("p2.paid_at IS NULL OR p2.paid_at = ''")
  end

  enum created_with: { songwriter: "songwriter", songwriter_relaxed: "songwriter_relaxed" }

  validates :name, presence: true
  validates :person, presence: true
  validates :name, length: { maximum: 129 }
  validates :known_live_on, presence: { if: :takedown? }
  validates :renewal_due_on, presence: { if: :takedown? }
  validates :sale_date, presence: true
  validates :timed_release_timing_scenario, allow_blank: true, inclusion: { in: TIMED_RELEASE_TYPES }
  validate :valid_orig_release_year?
  validates :language_code, presence: { message: I18n.t("models.album.choose_language") }
  validates :primary_genre_id, presence: { message: I18n.t("models.album.choose_genre") }
  validate :check_for_album_creatives
  validate :check_for_missing_tracks, if: proc { |record| record.album_type == "Album" }
  validate :check_for_various_artists
  validate :acceptable_album_review_events
  validate :same_language_codes_on_songs, if: proc { language_code_changed? }
  validates :album_type, inclusion: { in: %w[Album Single Ringtone] }
  validate :valid_golive_date, unless: proc { |record| record.is_a?(Ringtone) }
  validate :not_india_parent_genre
  validates_with Utf8mb3Validator, fields: [:name]

  accepts_nested_attributes_for :salepoints

  before_validation :move_creatives
  before_validation :titleize_name
  before_validation :titleize_songs
  before_validation :add_missing_golive_date, unless: proc { |record| record.is_a?(Ringtone) }

  before_save :set_default_auto_format, if: proc { language_code_changed? }
  before_save :set_default_orig_release_year
  before_save :mark_as_do_not_review, if: proc { |record| !record.finalized? }
  before_save :update_composition_submission
  after_create :create_copyright_status, if: proc { |record| record.person.india_user? }

  after_save :retry_streaming_distro, if: proc { |record| record.finalized? && record.finalized_at_changed? }
  after_save :send_apple_music_update, if: proc { apple_music_changed? && is_live? && !takedown? }
  after_save :clear_songs_previously_released_at, if: proc { changed.include?("orig_release_year") }

  after_commit :run_after_commit_callbacks

  # Called before add to cart processes
  #
  # For an album we want to change single track albums to singles
  #
  # Returns true/false whether or not nullify the product being added to cart
  #

  def self.facebook_monetizable
    paid.approved.distributed.not_taken_down.not_ringtones.exclude_genre("Classical")
  end

  def self.youtube_monetizable
    paid.approved.distributed.not_taken_down.not_ringtones
  end

  def self.send_chain(methods)
    Array(methods).inject(self) do |album_class, method_with_args|
      album_class.send(*method_with_args)
    end
  end

  def self.fetch_albums_ids_for_renewal_takedown
    # Legacy non-plan users have monthly and yearly album subscriptions
    (monthly_renewal_takedowns_ids + annual_renewal_takedowns_ids).pluck(:id).uniq
  end

  def self.monthly_renewal_takedowns_ids
    monthly_date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN_MONTHLY + 3).days
    expired_albums_to_takedown(monthly_date, true)
  end

  def self.annual_renewal_takedowns_ids
    annual_date = Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days
    expired_albums_to_takedown(annual_date, false)
  end

  def facebook_monetizable?
    self.class.facebook_monetizable.exists?(id: id)
  end

  def facebook_tracks
    track_monetizations.where(store_id: Store.find_by(short_name: "FBTracks").id)
  end

  def all_songs_on_facebook?
    songs.count == facebook_tracks.can_be_delivered.count
  end

  def fb_reels_salepoint?
    salepoints.pluck(:store_id).include? Store::FB_REELS_STORE_ID
  end

  def instagrammable?
    facebook_monetizable? && all_songs_on_facebook?
  end

  def youtube_monetizeable?
    !person.ytm_blocked? && !youtube_music_takedown? && (person.has_ytsr? || has_proxy_ytsr_store?)
  end

  def youtube_music_takedown?
    takedown? || salepoints.find_by(store_id: Store::GOOGLE_STORE_ID)&.taken_down?
  end

  def created_with_songwriter?
    songwriter? || songwriter_relaxed?
  end

  def skip_songwriter_validations?
    !songwriter?
  end

  def add_to_cart_pre_proc
    updated = change_single_track_album_to_single
    add_related_to_cart
    updated
  end

  def remove_from_cart_pre_proc
    remove_related_from_cart
  end

  def paid_post_proc
    update(payment_applied: true, steps_required: 0)
    finalize

    petri_bundles.create unless petri_bundles.exists?

    return unless person.has_active_ytm? && !ringtone?

    salepoint = Salepoint.add_youtubesr_store(self, person.youtube_monetization.purchase)
    Rails.logger.info "Failed to add YouTube Sound Recording Revenue Salepoint for album: #{id}" unless salepoint
  end

  #
  #  The portion of the album name that
  #  the user is allowed to modify
  #
  def album_name=(value)
    @album_name = value
    self.name = value
  end

  def album_name
    parts =
      if featured_artists.size.positive?
        # We are currently only interested in breaking apart the (feat. artist) for apple
        name.split(/\s\(f(?:ea)?t(?:(?:\w)+|(?:\s)|(?:\.)?)\s?/i)
      else
        name.split(" - Single")
      end
    parts.size.positive? ? parts.first : ""
  end

  def total_track_length_in_seconds
    songs.map(&:duration_in_seconds).compact.sum
  end

  def scrapi_jobs_stats
    songs_scrapi_jobs = Song.includes(:scrapi_job).where(album_id: id).map(&:scrapi_job).compact
    completed = songs_scrapi_jobs.count { |scrapi_job| scrapi_job.status == "done" }
    { total: songs.count, completed: completed }
  end

  def is_various=(value)
    super
  end

  def label_name
    if label
      label.name
    else
      ""
    end
  end

  def label_name=(arg)
    self.label = arg.blank? ? nil : Label.find_or_create_by(name: arg)

    include_label_name_errors
  end

  def set_default_label_name
    self.label_name =
      if label
        label.name
      elsif artist_name.length <= 120
        artist_name
      else
        singular_primary_artist_name
      end
    true
  end

  def release_date_changes(current_user, field, label)
    return unless changed.include?(field)

    current_time = Time.current.strftime("%Y-%m-%d at %H:%M:%S")
    instance_variable_set("@#{field}_note", "Release date changed on #{current_time} by #{current_user.name}, Original #{label}: #{changes[field][0]}")
  end

  def add_note_for_release_date_change(current_user, field)
    return unless instance_variable_get("@#{field}_note")

    notes.create!(
      note_created_by_id: current_user.id,
      subject: "Release date changed".freeze,
      note: instance_variable_get("@#{field}_note")
    )
  end

  def language_code_legacy_support=(code)
    self.language_code = LEGACY_LANGUAGE_CODE_MAP[code] || code
  end

  def language_code_legacy_support
    LEGACY_LANGUAGE_CODE_MAP[language_code] || language_code
  end

  def language
    LanguageCode.available_album_languages.find { |l| l.code == language_code_legacy_support }
  end

  #
  # Changes a renewal to specified interval and duration.. Defaults to change to a yearly renewal
  # returns true if the renewal was converted, and false/nil if it was not
  #
  def change_renewal_interval(interval = "year", duration = "1")
    Renewal.find(renewal.id).change_album_renewal_length(album_type, interval, duration)
  end

  #
  # Changes a single track album to a single album type:
  #
  def change_single_track_album_to_single
    # Album and one track? and track < 10 minutes
    return unless convert_to_single?

    song = songs.first

    return unless song.duration && song.duration < 10.minutes

    self.album_type = "Single"
    self.is_various = false
    move_creatives_to_album
    save

    # We have to reload the object as a single due to the workings of the
    # titlizer class
    self_as_single      = Single.find(id)
    self_as_single.name = song.name
    self_as_single.save

    Note.create(
      note: "Converted Album ID: #{id} to single",
      subject: "Single Track Album Conversion",
      related: person,
      note_created_by_id: 0
    )
  end

  def convert_to_single?
    instance_of?(Album) and songs.count == 1
  end

  def update_review_status(new_review)
    (new_review == "false") ? mark_as_last_review_audit_state! : mark_as_needs_review!
  end

  # Called after the album has been purchased
  # Finalizes the album and the salepoints, marks the album as ready for review
  # Once the album has been reviewed it can then be send to PETRI for distribution
  def distribute!
    # update the estimated golive_date
    update_attribute(:golive_date, Time.now + 6.weeks) if golive_date.nil? && !ringtone?

    # if we are distribution an album with preorder data that has been filled
    # but not paid, reset the variable pricing for the itunes salepoint
    if preorder_purchase && !preorder_purchase.paid_at? && itunes_ww_salepoint && itunes_ww_salepoint.salepoint_preorder_data
      itunes_ww_salepoint.salepoint_preorder_data.set_track_price(0.99)
      itunes_ww_salepoint.set_variable_price(9.99)
    end

    # if we are distributing the album and the
    # booklet has not yet been paid for, delete the
    # assocation so petri does not attach to the distribution
    return unless booklet && !booklet.paid?

    booklet.destroy
    reload
  end

  def discovery_platforms
    stores.where(id: SpecializedReleaseable::FREEMIUM_STORES)
  end

  def finalize(params = { message: "Auto-finalized", person: person })
    if should_finalize? && already_paid?
      update_attribute(:finalized_at, Time.now)
      salepoints.each(&:finalize)
      process_missing_automator_salepoints
      begin
        if finalized?
          Note.create(related: self, note_created_by: params[:person] || person, subject: "Finalized", note: params[:message])
        end
      rescue
      end
    end
    transcode_assets
    finalized?
  end

  def transcode_assets
    AssetsBackfill::Base.new(album_ids: [id]).run if FeatureFlipper.show_feature?(:asset_transcoding) && finalized?
  end

  def unfinalize_attributes
    return UNFINALIZED_RINGTONE_ATTRIBUTES if ringtone?

    UNFINALIZED_ATTRIBUTES
  end

  def unfinalize(params = { message: "Auto-unfinalized due to rejection", person: person })
    update(unfinalize_attributes)
    salepoints.each(&:unfinalize)
    begin
      unless finalized?
        Note.create(related: self, note_created_by: params[:person] || person, subject: "Unfinalized", note: params[:message])
      end
    rescue
    end

    !finalized?
  end

  def flag_duplicate_content
    duplicated_content = all_duplicate_content
    return unless finalized? && !duplicated_content.empty?

    album_flagged_content.nil? ? AlbumFlaggedContent.create(album: self, flagged_content: duplicated_content) : album_flagged_content.update(flagged_content: duplicated_content)
    save
  end

  def all_duplicate_content
    duplicate_songs = []
    songs.select { |song| song.content_fingerprint.present? }.each do |song|
      matched_song_ids = Song.where(Song.arel_table[:content_fingerprint].eq(song.content_fingerprint).and(Song.arel_table[:id].not_eq(song.id))).pluck(:id)
      duplicate_songs << { flagged_song_id: song.id, matched_song_ids: matched_song_ids } unless matched_song_ids.empty?
    end
    duplicate_songs
  end

  def flagged_content_data
    flagged_content = []
    unless album_flagged_content.nil?
      album_flagged_content.flagged_content.each do |flagged_song_params|
        flagged_song = Song.find(flagged_song_params["flagged_song_id"])
        flagged_content << {
          "flagged_song" => {
            name: flagged_song.name,
            song_id: flagged_song.id
          },
          "matched_songs" => flagged_song_params["matched_song_ids"].map do |song_id|
                               matched_song = Song.find(song_id)
                               {
                                 name: matched_song.name,
                                 content_fingerprint: matched_song.content_fingerprint,
                                 album_id: matched_song.album_id
                               }
                             end
        }
      end
    end
    flagged_content
  end

  def flip_format_flag
    self.allow_different_format =
      (has_non_formattable_language_code? || !allow_different_format)
    save
  end

  def start_review(args)
    if review_audits.find_by("event = 'STARTED REVIEW' AND created_at > '#{30.minutes.ago.localtime.strftime('%Y-%m-%d %H:%M:%S')}' and person_id =#{args[:person_id]}").nil?
      ReviewAudit.create(event: "STARTED REVIEW", person_id: args[:person_id], album_id: id)
    end
    reload
  end

  def salepoints_to_distribute(to_distribute_salepoints)
    # if there's an itunes salepoint in this purchase we need to include all the itunes
    # salepoints that are already attached to the album, so that itunes includes all the correct
    # territories
    (to_distribute_salepoints + itunes_salepoints).compact.uniq
  end

  def countries=(included)
    AlbumCountry::RelationTypeService.update(self, included)
  end

  def countries
    AlbumCountry::RelationTypeService.included(self)
  end

  def excluded_countries
    AlbumCountry::RelationTypeService.excluded(self)
  end

  def country_iso_codes
    countries.map(&:iso_code)
  end

  # only used in petri
  def all_country_iso_codes
    Country.all.map(&:iso_code)
  end

  # Checks to see if the album has an outstanding/unpaid renewal.
  def has_unpaid_renewal?
    Renewal.unpaid_renewal?(self)
  end

  def renewal
    @cached_renewal || Renewal.renewal_for(self)
  end

  def renewal_without_info
    Renewal.renewal_for_without_info(self)
  end

  def undelete
    update_attribute :is_deleted, false
    update_attribute :deleted_date, nil
  end

  def has_booklet?
    booklet != nil
  end

  def has_salepoint_subscription?
    salepoint_subscription.present?
  end

  def streaming_distribution
    distributions.where(converter_class: "MusicStores::Streaming::Converter", state: "delivered").last
  end

  def retry_streaming_distro(reason = "Retrying streaming distribution")
    return true unless streaming_distribution
    return true unless approved_for_distribution?

    if streaming_distribution.state == "enqueued"
      streaming_distribution.state = "error"
    end # sometimes distros don't like to go from enqueued -> enqueued
    streaming_distribution.retry(actor: "System", message: reason)
    true
  end

  def send_to_bigbox(prefix)
    begin
      song_filenames = []
      songs.each do |song|
        if song.s3_asset
          key = song.s3_asset.key
          bucket_name = song.s3_asset.bucket
        else
          key = song.s3_orig_asset.key
          bucket_name = FLAC_BUCKET_NAME
        end
        filename = song.download_filename
        song_filenames << { bucket_name: bucket_name, key: key, filename: filename }
      end

      threads = []
      song_filenames.each do |song_filename|
        threads << Thread.new { S3Asset.set_download_filename(song_filename[:bucket_name], song_filename[:key], song_filename[:filename]) }
      end

      urls = []
      songs.each do |song|
        urls << song.s3_url(3600)
      end
      urls << artwork.s3_url(3600)

      threads.each(&:join)
      BigboxWorker.perform_async(BIGBOX_JOB_QUEUE, "zip", prefix, urls, AWS_ACCESS_KEY, AWS_SECRET_KEY, package_name: name)
      logger.info("Enqueued bigbox import for album #{id}")
      true
    rescue StandardError => e
      Rails.logger.error("Couldn't send album #{id} to bigbox:\n#{e.message}\n#{e.backtrace}")
      false
    end
  end

  def status
    if state == "taken down"
      "down"
    elsif legal_review_state == "REJECTED"
      "rejected"
    elsif state == "live" or (
           state == "ready to go" &&
           finalized? &&
           salepoints.where(payment_applied: false).any?
         )

      "live"
    elsif state == "sent"
      "sent"
    elsif state == "ready to go"
      "ready"
    else
      "incomplete"
    end
  end

  def alerts
    alerts = []
    if renewal && renewal.expires_at && renewal.expires_at - 4.weeks <= Time.now && !renewal.taken_down? && renewal.canceled_at.nil?
      alerts.push("renewal")
    end
    alerts.push("canceled") if renewal != nil && !renewal.canceled_at.nil?
    if !salepoints.includes(:inventory_usages, :store).select { |sp| !sp.payment_applied? }.empty? and finalized?
      alerts.push("unpaid")
    end
    if (person.paid_for_composer? || person.paid_for_publishing_composer?) && finalized? && has_publishing_splits == false && legal_review_state != "REJECTED" && state != "taken down"
      alerts.push("splits")
    end
    alerts
  end

  # 11/11/2011 LA
  def templates_required?
    salepoints.detect(&:requires_template?)
  end

  # 2011-9-29 AK
  # FIXME: Need to adjust this method for my_compositions
  # This method is no longer accurate since it assumes if there's one split, then
  # the album split is selected
  # However, if we change this method, then user will be able to fix all the split,
  # causing resubmission. We should only allow this for the split that is missing
  def has_publishing_splits
    if songs.detect { |song| song.get_publishing_split != nil }
      true
    else
      false
    end
  end

  # FIXME: fix me too
  def missing_splits?
    songs.any? { |s| s.get_publishing_split.nil? }
  end

  def creatives=(values)
    values.reject! do |creative_data|
      creative_data["name"].blank? ||
        ((instance_of?(Album) || instance_of?(Single)) &&
          creative_data["role"] != "primary_artist")
    end

    no_main_artist = values.none? { |cd| cd["role"] == "primary_artist" }

    if no_main_artist
      errors.add(:creatives, I18n.t("models.album.requires_main_artist").to_s) unless is_various?

      return @creatives_missing = true
    end

    new_values =
      values
      .partition { |creative_data| creative_data["role"] == "primary_artist" }.flatten
      .uniq { |creative_data| creative_data["name"].strip }

    check_for_duplicate_creatives_on_songs(new_values)

    if persisted?
      remove_creatives(new_values)
      update_existing_creatives(new_values)
    end

    new_creatives = creatives + new_creatives(new_values)

    new_creatives.each do |new_c|
      if new_c.role == "primary_artist" && check_for_duplicate_with_songs(new_c)
        errors.add(:creatives, I18n.t("songs_app.form_field_warnings.artist_name.duplicate_artist_name"))
        new_creatives.delete(new_c)
      end
    end

    super new_creatives
  end

  def check_for_album_creatives
    return unless !is_various? && creatives.empty? && instance_of?(Album)

    errors.add(:creatives, I18n.t("models.album.requires_main_artist").to_s)
  end

  def check_for_various_artists
    return unless !is_various? && creatives.any?

    creatives.each do |creative|
      if creative.name&.downcase&.match?(/various artist(s)?/)
        errors.add(:creatives, I18n.t("models.album.cannot_be_various_artists"))
      end
    end
  end

  def check_for_duplicate_creatives_on_songs(album_creatives)
    album_creatives.each do |album_creative|
      songs.each do |song|
        song.creatives.detect do |song_creative|
          if song_creative.name == album_creative["name"] && song_creative.role == album_creative["role"]
            song_creative.destroy
          end
        end
      end
    end
  end

  def check_for_missing_tracks
    songs.each_with_index do |song, index|
      errors.add(:songs, I18n.t("models.album.missing_track").to_s) && return unless song.track_num == index + 1
    end
  end

  def acceptable_album_review_events
    return if Album.acceptable_review_states.include?(legal_review_state)

    errors.add(
      :event,
      "is unacceptable. Must be one of the following values: #{Album.acceptable_review_states.join(', ')}"
    )
  end

  def move_creatives
    # Checking if we are changing to a various artist album
    if is_various? && creatives.present?
      songs.each do |song|
        creatives.each do |creative|
          Creative.create_role(song, creative.name, creative.role)
        end
        song.save
      end
      creatives.destroy_all
    end
    true
  end

  def all_songs_are_free?
    songs.detect { |s| !s.free_song }.nil?
  end

  def last_review_state
    review_audits.where.not(event: ReviewAudit::STARTED_REVIEW).last&.event
  end

  def mark_as_last_review_audit_state
    update_attribute :legal_review_state, last_review_state unless review_audits.empty?
  end

  def mark_as_last_review_audit_state!
    return if review_audits.empty?

    self.legal_review_state = last_review_state
    save!
  end

  def mark_as_needs_review
    update_attribute :legal_review_state, "NEEDS REVIEW"
  end

  def mark_as_needs_review!
    self.legal_review_state = "NEEDS REVIEW"
    save!
  end

  def mark_as_do_not_review
    self.legal_review_state = "DO NOT REVIEW"
    true
  end

  def self.expired_albums_to_takedown(expire_date, monthly)
    Album.for_takedown_by_day(expire_date, monthly)
         .approved
         .distributed_albums_via_www_or_distributor
         .distinct
  end

  def self.acceptable_review_states
    ["FLAGGED", "REJECTED", "APPROVED", "NEEDS REVIEW", "DO NOT REVIEW", "NOT SURE", "SKIPPED"]
  end

  # Checks if a release has been flagged as dismissed by querying Simple DB
  # To dismiss a release, go to portal.tunecore.com/fraud/show/album-distribution-report
  #
  # For a code reference to the application that handles marking albums as
  # dismissed see the fraud app.
  # https://github.com/tunecore/CoreData/blob/master/apps/fraud/views.py#L117
  def dismissed?
    attributes = AwsWrapper::SimpleDb.get_attributes(
      domain: BIGBOX_ASSETS_SDB_DOMAIN,
      item: upc.to_s
    )

    attributes["state"] == ["dismissed"]
  end

  def has_sales?
    !sales_records.first.nil? || !SalesRecord.where("related_id in (?) AND related_type = 'Song'", songs.collect(&:id)).first.nil?
  end

  def days_til_expiration
    renewal.nil? ? nil : (renewal.expires_at.to_date - Time.now.to_date).to_i
  end

  def itunes_ww_salepoint
    salepoints.where(store_id: Store.find_by(short_name: "iTunesWW").id).last
  end

  # 2012-12-10 AK For use with the country selector
  def has_itunes_ww_salepoint?
    !!itunes_ww_salepoint
  end

  #
  # Determines if the album type is a valid type
  # for salepoint subscriptions
  #
  def supports_subscriptions
    (type_supports_subscription? && !takedown?)
  end

  def type_supports_subscription?
    SUBSCRIPTION_SUPPORTED_TYPES.include?(album_type)
  end

  def add_related_to_cart
    if salepoint_subscription and salepoint_subscription.purchase.blank? and !salepoint_subscription.finalized?
      Product.add_to_cart(person, salepoint_subscription)
    end
    Album::DolbyAtmosCartService.call(person, self)

    Product.add_to_cart(person, booklet) if booklet and booklet.purchase.blank? and !booklet.paid?

    unless preorder_purchase.present? &&
           !preorder_purchase.paid_at? &&
           preorder_purchase.purchase.blank? &&
           preorder_purchase.enabled_preorder_data_count.positive?

      return
    end

    product = find_correct_preorder_product
    Product.add_to_cart(person, preorder_purchase, product)
  end

  def remove_related_from_cart
    salepoint_subscription.purchase.destroy if salepoint_subscription and salepoint_subscription.purchase

    booklet.purchase.destroy if booklet and booklet.purchase

    preorder_purchase.purchase.destroy if preorder_purchase.present? && preorder_purchase.purchase

    spatial_audios.each do |spatial_audio|
      spatial_audio.purchase.destroy if spatial_audio.purchase
    end
  end

  def has_automator?
    salepoint_subscription.present?
  end

  def add_automator
    create_salepoint_subscription(is_active: true) if (salepoint_subscription.blank? && supports_subscriptions)
  end

  def supports_preorder?
    type_supports_preorder? && !payment_applied? && stores.any? { |store| PreorderPurchase::STORE_NAME_TO_ENABLE_FLAG.key?(store.short_name) }
  end

  def type_supports_preorder?
    PREORDER_SUPPORTED_TYPES.include?(album_type)
  end

  def minimum_price_by_track_size
    track_size = songs.count

    if track_size > 10
      4.99
    elsif track_size > 7
      3.99
    elsif track_size > 4
      2.99
    elsif track_size > 2
      1.99
    end
  end

  def find_correct_preorder_product
    Product.get_preorder_product_by_country_website_id(person.country_website.id)
  end

  def preorder_date_expired?
    salepoint_preorder_data.present? && salepoint_preorder_data.any? { |spd| spd.start_date && spd.start_date > (sale_date - 1.day) }
  end

  def sale_date_supports_preorder?
    (sale_date > (Date.today + 11.days))
  end

  def approved_for_distribution?
    (approved? || takedown_at?) &&
      ["APPROVED", "DO NOT REVIEW"].include?(style_review_state)
  end

  def as_json(options = {})
    new_attributes = {
      artist_name: artist_name,
      artwork_url: {
        small_url: artwork.try(:url, :small),
        medium_url: artwork.try(:url, :medium),
        large_url: artwork.try(:url, :large)
      }
    }

    merge_attributes_as_json(new_attributes, options)
  end

  def album?
    album_type == "Album"
  end

  def single?
    album_type == "Single"
  end

  def ringtone?
    album_type == "Ringtone"
  end

  def release?
    album? || single? || ringtone?
  end

  def live_itunes_preorder
    apple_identifier && preorder_purchase.present? && preorder_purchase.paid_at? && preorder_purchase.itunes_enabled && Date.today >= itunes_ww_salepoint.salepoint_preorder_data.start_date
  end

  def includes_automated_store?(stores)
    stores.any? { |store| automated_store?(store) }
  end

  def automated_store?(store)
    salepoint_subscription.present? && is_salepoint_effective?(store)
  end

  def has_been_delivered?(store)
    # If the album as been setup for automator delivery before the store launched, it should
    # already have been delivered to that store.
    salepoint_subscription.effective <= store.launched_at
  end

  def retriable_distributions_by_stores(selected_store_ids)
    distros = []
    itunes_distros = []
    extra_condition = ["salepoints.store_id IN (?)", selected_store_ids] unless selected_store_ids == :all

    salepoints = self.salepoints
                     .includes(distributions: :petri_bundle)
                     .where.not(petri_bundles: { state: ["dimissed", "blocked"] })
                     .where(extra_condition)

    salepoints.each do |salepoint|
      distribution = salepoint.distributions.latest
      distros << distribution
      itunes_distros << distribution if distribution.converter_class[/Itunes/]
    end

    return distros, itunes_distros
  end

  def allow_itunes_streaming
    apple_music
  end

  def self.content_review_list_query(select_string, includes_string, options = {})
    review_state_condition = (options[:review_state] == "ALL") ? "" : ["legal_review_state = ?", options[:review_state]]
    language_code_condition = (options[:language_code] == "ALL") ? "" : ["albums.language_code = ?", options[:language_code]]
    if options[:date_filter_type] == "audit"
      order_by = "created asc"
      time_filter = :review_audited_within
    else
      order_by = "finalized_at asc"
      time_filter = :finalized_at_within
    end

    Album.select(select_string)
         .needs_or_has_been_reviewed
         .send(time_filter, options[:start_time], options[:end_time])
         .where(review_state_condition)
         .where(language_code_condition)
         .where(takedown_at: nil)
         .limit(options[:limit])
         .includes(includes_string)
         .order(order_by)
  end

  def self.takedown_unsent_and_ready_to_renew(end_date)
    ready_to_renew(end_date)
      .readonly(false)
      .each do |album|
        Rails.logger.info "Takedown album: #{album.id}"
        opts = {
          user_id: 0,
          subject: "Automated Takedown",
          note: "Release was taken down via batch:build_and_run task"
        }
        album.takedown!(opts)
      end
  end

  def last_purchased_album_of_type?
    return false if purchases.empty?

    Album.find_by_sql(
      [
        "select * from
                            (select albums.*, purchases.paid_at from purchases
                               inner join albums on albums.id = purchases.related_id and purchases.related_type = 'Album'
                               where purchases.person_id = ? and albums.album_type = ?

                             UNION

                             select albums.*, purchases.paid_at from purchases
                               inner join credit_usages on purchases.related_type = 'CreditUsage' and credit_usages.id = purchases.related_id
                               inner join albums on credit_usages.related_id = albums.id and credit_usages.related_type = 'Album'
                               where purchases.person_id = ? and albums.album_type = ?) a1

                             where paid_at > ?",
        person_id,
        album_type,
        person_id,
        album_type,
        purchases.first.paid_at
      ]
    ).blank?
  end

  def eager_song_notes
    Note.select("songs.track_num, notes.*").joins("INNER JOIN songs ON notes.related_id = songs.id").where("related_id IN (?) AND related_type= 'Song'", songs.map(&:id))
  end

  def eager_song_creatives
    Creative.where("creativeable_id IN (?) AND creativeable_type = 'Song'", songs.map(&:id))
  end

  def self.fetch_by_store_and_distribution_date(store_ids, delivered_date)
    d_table = Distribution.arel_table
    updated_at_date = Arel::Nodes::NamedFunction.new("DATE", [d_table[:updated_at]])
    sp_table  = Salepoint.arel_table
    a_table   = Album.arel_table

    albums = Album.select(a_table[Arel.star]).joins(salepoints: :distributions)
                  .where(d_table[:state].eq("delivered"))
                  .where(updated_at_date.eq(delivered_date))
                  .where(sp_table[:store_id].in(store_ids))
                  .where(a_table[:takedown_at].eq(nil))
  end

  def approved?
    legal_review_state == "APPROVED"
  end

  def has_ever_been_approved?
    review_audits.where(event: "APPROVED").exists?
  end

  def has_been_distributed?
    !salepoints.joins(:distributions, :store).where(Store.arel_table[:is_active].eq(true).and(Distribution.arel_table[:state].eq("delivered"))).empty?
  end

  def delivered_by_tc_www_on?(store_id:)
    !!salepoints.joins(:distributions, :store)&.find_by(store_id: store_id)&.delivered_by_tc_www?
  end

  def get_finalized_store_ids
    salepoints.where(Salepoint.arel_table[:finalized_at].not_eq(nil)).pluck(:store_id)
  end

  def ready_for_distribution?
    finalized? && payment_applied && approved_for_distribution?
  end

  def create_purchase_with_credit
    credit_usage = CreditUsage.for(person, self)
    return unless credit_usage

    purchase = Product.add_to_cart(person, self)
    credit_usage.destroy if purchase.nil?
    purchase
  end

  def cancel_renewal
    renewal = Renewal.renewal_for(self)
    renewal.cancel! unless renewal.nil?
  end

  def reset_renewal
    return if takedown?

    renewal = Renewal.renewal_for(self)
    renewal.keep! unless renewal.nil?
  end

  def source
    self[:source] || "customer"
  end

  def under_review?
    legal_review_state == "NEEDS REVIEW"
  end

  def self.find_by_upc(upc)
    Upc.includes(:upcable).where(number: upc, inactive: false).try(:last).try(:upcable)
  end

  def valid_golive_date
    return if is_datetime_type(golive_date)

    errors.add(:golive_date, I18n.t("models.album.album_timed_release_fields.error_message"))
  end

  def itunes_salepoints
    salepoints.eager_load(:store).select { |s| s.store.short_name.include?("iTunes") }
  end

  def has_itunes_salepoints?
    itunes_salepoints.any?
  end

  def not_india_parent_genre
    if primary_genre&.india_parent_genre?
      errors.add(:primary_genre_id, I18n.t("models.album.no_select_india_parent_genre"))
    end

    return unless secondary_genre&.india_parent_genre?

    errors.add(:secondary_genre_id, I18n.t("models.album.no_select_india_parent_genre"))
  end

  def get_tracks_for(service_name)
    tracks = external_ids_for(Song, service_name).where(songs: { album_id: id })
    if tracks.present?
      tracks
    else
      SpotifyUriWorker.perform_async(:get_one_album_tracks, external_id_for("spotify"))
      nil
    end
  end

  def low_risk_account?
    !!low_risk_account
  end

  def same_language_codes_on_songs
    same_language_code_as_album = songs.all? { |s| s.metadata_language_code.try(:code) == language_code }
    return if same_language_code_as_album && songs.present?

    new_language_code = LanguageCode.where(code: language_code).first
    return if new_language_code.nil?

    songs.each do |s|
      s.update(metadata_language_code_id: new_language_code.id)
    end
  end

  def timed_release_absolute_time?
    timed_release_timing_scenario == "absolute_time"
  end

  def timed_release_relative_time?
    timed_release_timing_scenario == "relative_time"
  end

  def track_limit_reached?
    songs.count > MAX_TRACK_LIMIT && !person.feature_enabled?(:hundred_tracks_allowed)
  end

  def create_note(opts)
    user_id, ip_address, subject, note = opts[:user_id], opts[:ip_address], opts[:subject], opts[:note]
    Note.create(
      related: self,
      note_created_by_id: user_id,
      ip_address: ip_address,
      subject: subject,
      note: note
    )
  end

  def process_missing_automator_salepoints
    return unless salepoint_subscription && salepoint_subscription.finalized?

    stores = Store.where(["launched_at > ?", salepoint_subscription.effective])

    orphaned_sp_stores = stores.orphaned_salepoint_stores_for_album(self)

    salepoint_subscription.add_to_subscription(orphaned_sp_stores, true) if orphaned_sp_stores.any?
  end

  def has_active_salepoint_for?(store_short_name)
    return false if (!finalized? || takedown?)

    store = Store.find_by(short_name: store_short_name)

    salepoints
      .where(store_id: store.id, takedown_at: nil)
      .where.not(finalized_at: nil)
  end

  def lowest_audio_quality
    songs_with_assets = songs.joins(s3_asset: :s3_detail)

    return "" if songs_with_assets.blank?

    file_types = S3Detail.file_types_for_songs(songs_with_assets)

    file_types.max do |file_type_a, file_type_b|
      return file_type_b if file_type_b == S3Detail::FILE_TYPE_ORDER.keys.last

      S3Detail::FILE_TYPE_ORDER.fetch(file_type_a, 0) <=> S3Detail::FILE_TYPE_ORDER.fetch(file_type_b, 0)
    end
  end

  def has_minimum_number_of_songs?
    if number_of_tracks_allowed == 1
      songs.count == 1
    else
      songs.count > 1
    end
  end

  private

  def include_label_name_errors
    return unless label&.errors&.messages&.fetch(:name, nil)&.any?

    errors[:label_name].push(*label.errors.messages[:name])
  end

  def new_creatives(album_creatives)
    new_creatives =
      album_creatives.select do |alb_creative|
        alb_creative["id"].blank?
      end

    new_creatives.map do |creative|
      Creative.new(
        role: creative["role"],
        artist_id: Artist.create_by_nontitleized_name(creative["name"]).id
      )
    end
  end

  def remove_creatives(album_creatives)
    album_creative_ids = album_creatives.map { |alb_creative| alb_creative["id"] }.compact

    creative_roles_to_remove = (instance_of?(Single) || instance_of?(Album)) ? %w[primary_artist] : %w[primary_artist featuring]

    creatives.destroy(
      creatives.where.not(
        id: album_creative_ids
      ).where(
        role: creative_roles_to_remove
      )
    )
  end

  def update_existing_creatives(album_creatives)
    album_creatives.each do |alb_creative|
      next if alb_creative["id"].blank?

      creative = Creative.find(alb_creative["id"])
      artist = creative.artist

      if alb_creative["role"] == "primary_artist" && check_for_duplicate_with_songs(alb_creative)
        errors.add(:creatives, I18n.t("songs_app.form_field_warnings.artist_name.duplicate_artist_name"))
        return
      end

      if alb_creative["name"] != artist.name
        creative.artist = Artist.create_by_nontitleized_name(alb_creative["name"])
        creative.save
      end
    end
  end

  def check_for_duplicate_with_songs(album_creative)
    name = album_creative["name"] || album_creative&.name
    if is_a?(Single) || is_a?(Ringtone)
      creatives.includes(:artist).joins(:artist).where("creatives.role = 'featuring' AND lower(artists.name) = lower(?)", name).any?
    elsif is_a?(Album)
      songs.includes(creatives: :artist).joins(creatives: :artist).where("creatives.role = 'featuring' AND lower(artists.name) = lower(?)", name).any?
    end
  end

  def update_composition_submission
    Composition.update_pending_submissions(self) if payment_applied_changed? && payment_applied?
    Composition.update_details_post_finalized(self) if finalized_at_changed? && finalized?
  end

  def is_salepoint_effective?(store)
    salepoint_subscription.effective.present? && salepoint_subscription.is_active && store.launched_at.present? && salepoint_subscription.effective <= store.launched_at
  end

  def run_after_commit_callbacks
    CrtCache::Invalidator.invalidate_cache_if_changed(self)
    Album::AfterCommitCallbacks.after_commit(self)
  end

  def send_apple_music_update
    Rails.logger.info "Sending apple music update for Album #{id}"
    begin
      salepoint = itunes_ww_salepoint
      if salepoint
        distribution = salepoint.distributions.uniq.last
      else
        Rails.logger.error "Apple Music Update Error: No iTunesWW salepoint found for Album #{id}"
        return
      end

      if distribution
        distribution.delivery_type = "metadata_only"
        distribution.update_attribute(:state, "new")
        distribution.retry actor: "User: #{person.name}", message: "Sending iTunes metadata update for apple music opt #{apple_music ? 'in' : 'out'}"
      else
        Rails.logger.error "Apple Music Update Error: No iTunes distribution found for Album #{id}"
      end
    rescue => e
      Rails.logger.error "Apple Music Update Error - Album #{id}: #{e.message}"
    end
  end

  def add_missing_golive_date
    return unless golive_date.blank? && sale_date.present?

    self.golive_date = Album::DateTransformerService.convert_eastern_date_to_localtime(sale_date)
    self.timed_release_timing_scenario = "relative_time"
  end

  def create_copyright_status
    create_copyright_status!(status: "pending")
  end
end
