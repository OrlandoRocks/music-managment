class Reward < ApplicationRecord
  has_many :tier_rewards, dependent: :destroy
  has_many :tiers, through: :tier_rewards

  validates :name, :content_type, :category, :points, presence: true
  validates :is_active, inclusion: { in: [true, false] }

  scope :is_active, -> { where(is_active: true) }

  def redeem!(person)
    person_rewards = person.rewards

    raise "Reward not allowed" unless person_rewards.ids.include?(id)
    raise "Insufficient balance" if person.available_points < points

    person.person_points_transactions.create!(
      {
        target: self,
        debit_points: points
      }
    )
  end
end
