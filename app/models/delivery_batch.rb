class DeliveryBatch < ApplicationRecord
  has_many :delivery_batch_items
  has_many :delivery_manifest_items, through: :delivery_batch_items
  has_many :distributions, through: :delivery_batch_items, source: :deliverable, source_type: "Distribution"
  has_many :track_monetizations, through: :delivery_batch_items, source: :deliverable, source_type: "TrackMonetization"
  belongs_to :store

  before_create :set_batch_id
  before_create :set_max_size

  validates :count, numericality: { less_than_or_equal_to: 100 }, presence: true

  scope :active, -> { where(active: true) }

  alias_attribute :joined_count, :count

  def increment_joined_count
    with_lock do
      DeliveryBatch.where(id: id).update_all(count: count + 1)
      DeliveryBatch.where(id: id)
                   .where(DeliveryBatch.arel_table[:count].gteq(max_batch_size))
                   .update_all(active: false)
    end
    reload
  end

  def increment_processed_count
    with_lock do
      DeliveryBatch.where(id: id)
                   .update_all(processed_count: processed_count + 1)
      DeliveryBatch.where(id: id)
                   .where(DeliveryBatch.arel_table[:processed_count].gteq(count))
                   .where(count: max_batch_size)
                   .update_all(active: false)
    end
    reload
  end

  def decrement_joined_count
    with_lock do
      DeliveryBatch.where(id: id)
                   .where(DeliveryBatch.arel_table[:count].gt(0))
                   .update_all(count: count - 1)
      DeliveryBatch.where(id: id)
                   .where(DeliveryBatch.arel_table[:count].lt(max_batch_size))
                   .update_all(active: true)
    end
    reload
  end

  def deactivate
    update(active: false)
  end

  def activate
    update(active: true)
  end

  def batch_full?
    joined_count >= max_batch_size
  end

  def batch_complete?
    processed_count >= max_batch_size
  end

  def set_batch_id
    self.batch_id = timestamp
  end

  def set_max_size
    self.max_batch_size = max
  end

  def max
    StoreDeliveryConfig.where(store_id: store_id).first.batch_size || 100
  end

  def timestamp
    Time.now.strftime("%Y%m%d%H%M%S%L")
  end
end
