# = Description
#
#
# = Usage
#
#
# = Change Log
# [2010-09-08 -- CH]
# Created Class

class CmsAsset < ApplicationRecord
  has_one :created_by, class_name: "Person", foreign_key: "id", primary_key: "created_by_id"

  CATEGORIES = ["Ad", "Dashboard", "General"]

  attr_accessor :temp_file

  validates :file_type, :file_size, :asset_name, :asset_category, presence: true
  validates :temp_file, presence: { on: :create }

  # before_update :process_replacement_file
  before_create :set_skip_replace
  after_create  :save_temp_file
  after_update  :replace_file
  after_destroy :delete_asset_file

  default_scope -> { order("asset_category, asset_name") }

  def uploaded_file=(file_field)
    self.temp_file  = file_field
    self.file_type  = temp_file.content_type.chomp
    self.file_size  = temp_file.size
  end

  def image?
    file_type.include?("image")
  end

  def relative_url
    "#{ASSET_RELATIVE_PATH}#{file_name}"
  end

  private

  def save_temp_file
    original_name   = temp_file.original_filename
    extension       = original_name.slice!(original_name.rindex("."), original_name.length)
    self.file_name  = "#{asset_category.downcase}_#{id}#{extension}"
    self.file_path  = "#{ASSET_FULL_PATH}#{file_name}"
    save
    File.open(file_path, "wb") { |f| f.write(temp_file.read) }
  end

  def set_skip_replace
    @skip_replace = true
  end

  def replace_file
    return unless !@skip_replace && !temp_file.nil?

    delete_asset_file
    File.open(file_path, "wb") { |f| f.write(temp_file.read) }
  end

  def delete_asset_file
    FileUtils.rm(file_path) if File.file?(file_path)
  end
end
