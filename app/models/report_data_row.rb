class ReportDataRow
  attr_accessor :data_type,
                :row_title,
                :show_title,
                :show_average,
                :show_total,
                :cells,
                :total,
                :average,
                :total_data_type,
                :average_data_type,
                :highlight,
                :skip_sum_and_average,
                :preorder_type

  def initialize(options)
    self.cells = []
    set_row_attributes(options)
  end

  def add_cell(value, data_type)
    cells << ReportDataCell.new(value, data_type)
  end

  def compute_total_and_average
    self.total    = cells.inject(0) { |acc, elem| acc + elem.value.to_i }
    self.average  = cells.length.positive? ? total.to_f / cells.length : 0.0
  end

  def flatten_data
    self.cells = cells.flatten
  end

  def set_row_attributes(options)
    self.data_type    = options[:data_type]
    self.row_title    = options[:row_title]
    self.show_title   = options[:show_title].nil? || options[:show_title] == true
    self.show_total   = options[:show_total].nil? || options[:show_total] == true
    self.show_average = options[:show_average].nil? || options[:show_average] == true
    self.highlight    = options[:highlight] != nil || options[:highlight] == true
    self.skip_sum_and_average = options[:skip_sum_and_average] || false
    set_total_and_average_data_types
  end

  def set_total_and_average_data_types
    return if skip_sum_and_average

    case data_type
    when ReportDataCell::DECIMAL
      self.average_data_type = ReportDataCell::DECIMAL
      self.total_data_type = ReportDataCell::DECIMAL
    when ReportDataCell::INTEGER
      self.average_data_type = ReportDataCell::DECIMAL
      self.total_data_type = ReportDataCell::INTEGER
    when ReportDataCell::CURRENCY
      self.average_data_type = ReportDataCell::CURRENCY
      self.total_data_type = ReportDataCell::CURRENCY
    end
  end

  def highlight?
    highlight
  end

  def show_total?
    show_total
  end

  def show_average?
    show_average
  end
end
