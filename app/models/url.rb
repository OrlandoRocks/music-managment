# == Schema Information
# Schema version: 404
#
# Table name: urls
#
#  id         :integer(11)     not null, primary key
#  store_id   :integer(11)
#  album_id   :integer(11)
#  link       :string(255)
#  created_on :datetime
#

class Url < ApplicationRecord
  belongs_to :album
end
