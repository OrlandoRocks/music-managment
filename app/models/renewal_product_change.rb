class RenewalProductChange < ApplicationRecord
  #################
  # Associations #
  #################
  belongs_to :renewal
  belongs_to :old_item_to_renew, polymorphic: true # references the product or product_item to use for renewal logic
  belongs_to :new_item_to_renew, polymorphic: true # references the product or product_item to use for renewal logic

  #################
  # Attributes #
  #################

  #################
  # Validations #
  #################
  before_validation :assign_old_item_to_renew, if: :has_renewal?
  before_validation :assign_new_item_duration, if: :has_new_item?

  validates :renewal_id,
            :new_item_to_renew_id,
            :old_item_to_renew_id,
            :old_interval,
            :old_duration,
            :new_interval,
            :new_duration,
            presence: true

  validate :product_changed, on: :create

  after_create :update_renewal

  private

  def has_renewal?
    !renewal_id.nil?
  end

  def has_new_item?
    !new_item_to_renew_id.nil?
  end

  def assign_old_item_to_renew
    self.old_item_to_renew = renewal.item_to_renew
    self.old_interval = renewal.item_to_renew.renewal_interval
    self.old_duration = renewal.item_to_renew.renewal_duration
  end

  def assign_new_item_duration
    self.new_interval = new_item_to_renew.renewal_interval
    self.new_duration = new_item_to_renew.renewal_duration
  end

  def product_changed
    errors.add(
      :new_item_to_renew,
      I18n.t("models.product.must_be_a_different_product_than_before")
    ) if old_item_to_renew == new_item_to_renew
  end

  def update_renewal
    renewal.update(item_to_renew: new_item_to_renew)
  end
end
