class MumaSongSociety < ApplicationRecord
  belongs_to :muma_song, foreign_key: "song_code", primary_key: "code"
end
