class AppliedIneligibilityRule < ApplicationRecord
  belongs_to :ineligibility_rule
  belongs_to :vetted_item, polymorphic: true
end
