class PersonTransaction < ApplicationRecord
  belongs_to :person
  belongs_to :target, polymorphic: true

  # This exists for the inverse relationship to work.
  # This always return `nil`.
  has_one :irs_tax_withholding, dependent: :destroy,
                                class_name: "IRSTaxWithholding",
                                inverse_of: :withheld_from_person_transaction,
                                foreign_key: :withheld_from_person_transaction_id

  before_validation :set_currency
  validates :currency, presence: true
  validate :currency_matches_person_balance

  before_create :lock_person_balance
  before_create :set_previous_balance
  after_create :update_balance

  # This is being used in the filters for transactions page history
  # first element in the array is what we display to the customer, the 2nd is how we store it in the
  # database, the 3rd is an array of which country has it active. For balance adjustment categories,
  # the 3rd element is the category name (with spaces), and the 4th is the country array
  PAYOUT_TRANSACTION = "PayoutTransfer".freeze
  PAGINATION_LIMIT = 10
  ADMIN_PAGINATION_LIMIT = 100
  TRANSACTION_TYPES = [
    ["Music Sales", "PersonIntake", %w[US CA]],
    ["Purchases", "Invoice", %w[US CA]], # this should also include the batch transaction
    ["Check Withdrawal", "CheckTransfer", ["US"]],
    ["Electronic Funds Transfer", "EftBatchTransaction", ["US"]],
    ["Paypal Withdrawal", "PaypalTransfer", %w[US CA]],
    ["Misc. Adjustments", "BalanceAdjustment", %w[US CA]],
    ["Friend Referral", "FriendReferral", ["US"]],
    ["YouTube Royalties", "YouTubeRoyaltyIntake", %w[US CA]],
    ["Refunds", "Refund", %w[US CA]],
    ["Credit Note Invoice", "OutboundRefund", %w[US CA]],
    ["Split Royalties", "SplitRoyaltyIntake", %w[US CA]]
  ].freeze

  BALANCE_ADJUSTMENT_TYPE = "BalanceAdjustment"

  IRS_TAXABLE_TRANSACTION_TYPES = [
    "TaxableEarningsRollover", # specific for the 2021-2022 blocked income tax rollovers
    "PersonIntake",
    "YouTubeRoyaltyIntake",
    "YouTube Royalties"
  ].freeze

  BALANCE_ADJUSTMENT_CATEGORIES = [
    [
      "Publishing Royalties",
      "PublishingRoyalty",
      "Songwriter Royalty",
      %w[US CA]
    ]
  ].freeze

  VAT_TRANSACTION_TYPES = [
    ["Royalty Invoice", "OutboundInvoice", %w[US CA]],
    ["VAT Adjustments", "VatTaxAdjustment", %w[US CA]]
  ].freeze

  PAYOUT_TRANSACTION_TYPE = [["Payoneer Withdrawals", "PayoutTransfer", %w[US CA]]].freeze

  scope :with_no_irs_tax_withholdings, -> do
    left_joins(:irs_tax_withholding)
      .where(irs_tax_withholdings: { withheld_from_person_transaction_id: nil })
  end

  scope :excluded_transactions, ->(field, ids) { where(arel_table[field].not_in(ids)) }

  scope :for_payouts, ->(payouts) { for_targets(payouts, PayoutTransfer.name) }

  scope :by_transaction_ids_and_target_type,
        ->(transaction_ids, target_types) {
          transaction_ids_hash = transaction_ids.present? ? { id: transaction_ids } : {}
          by_target_type(target_types).where(transaction_ids_hash)
        }

  scope :by_target_type,
        ->(*target_types) {
          target_types.compact.flatten.empty? ? for_all_targets : for_specific_targets(target_types)
        }

  scope :positive_credit, -> { where.not(credit: -Float::INFINITY..0) }
  scope :for_person, ->(person) { where(person_id: person.id) }

  scope :for_all_targets, -> { where("target_type IS NOT NULL") }

  scope :for_specific_targets,
        ->(types) {
          joins("LEFT OUTER JOIN balance_adjustments ON balance_adjustments.id = person_transactions.target_id")
            .where("person_transactions.target_type IN (?) OR (person_transactions.target_type = 'BalanceAdjustment' AND balance_adjustments.category IN (?))", *types, *types)
        }

  scope :for_all_us_taxable_targets, -> do
    joins("LEFT OUTER JOIN balance_adjustments ON balance_adjustments.id = person_transactions.target_id")
      .where(person_transactions: { target_type: IRS_TAXABLE_TRANSACTION_TYPES }).or(for_us_taxable_balance_adjustment_target)
  end

  scope :for_us_taxable_distribution_targets, -> { where(target_type: PersonTransaction::IRS_TAXABLE_TRANSACTION_TYPES) }

  scope :for_us_taxable_balance_adjustment_target, -> do
    joins("LEFT OUTER JOIN balance_adjustments ON balance_adjustments.id = person_transactions.target_id")
      .merge(BalanceAdjustment.us_taxable)
  end

  scope :for_revenue_stream_target, ->(revenue_stream_code) do
    case revenue_stream_code
    when :distribution
      for_us_taxable_distribution_targets
    when :publishing
      for_us_taxable_balance_adjustment_target
    end
  end

  scope :between_dates,
        ->(start_date, end_date) {
          where(person_transactions: { created_at: start_date..end_date })
        }

  scope :between_dates_by_month,
        ->(begin_date, end_date) {
          if begin_date && end_date
            where(
              "DATE(person_transactions.created_at) between DATE(?) AND LAST_DAY(?)",
              begin_date,
              end_date
            )
          else
            where({})
          end
        }

  scope :date_range_by_user,
        ->(user) {
          select(:created_at)
            .where(person_id: user.id)
            .group("YEAR(person_transactions.created_at), MONTH(person_transactions.created_at)")
            .order("DATE(person_transactions.created_at) ASC")
        }

  scope :for_current_year, -> {
    where(created_at: DateTime.current.beginning_of_year..DateTime.current.end_of_year)
  }

  scope :with_splits_information,
        ->(user_id, transactions) {
          select(
            "
              pt.id AS transaction_id,
              sum(sd.amount) as amount,
              si.currency as currency,
              sd.song_id as song_id,
              si.person_id as person_id,
              sd.royalty_split_id as royalty_split_id,
              sd.royalty_split_title as royalty_split_title
            "
          ).from("person_transactions pt")
            .joins("
            INNER JOIN royalty_split_intakes si ON si.id = pt.target_id
            INNER JOIN royalty_split_details sd ON sd.royalty_split_intake_id = si.id")
            .where(
              pt: {
                person_id: user_id,
                id: transactions.pluck(:id)
              }
            ).group(
              "pt.id, sd.royalty_split_id"
            ).order("amount DESC")
        }

  scope :with_sales_record_information,
        ->(user_id, transactions) {
          select(
            "
      pt.created_at,
      pt.id AS transaction_id,
      s.id AS store_id,
      s.name AS store_name,
      ss.id AS sip_store_id,
      ss.display_group_id AS store_display_group,
      srm.country AS country,
      srm.period_type AS period_type,
      srm.period_year AS period_year,
      srm.period_interval AS period_interval,
      srm.amount_currency AS currency,
      TRUNCATE(sum(srs.download_amount + srs.stream_amount),2) AS store_amount,
      DATE_FORMAT(srm.period_sort,'%b %Y') AS period_sort
    "
          )
            .from("person_transactions pt")
            .sales_record_joins
            .where(pt: {
                     person_id: user_id,
                     id: transactions.pluck("person_transactions.id")
                   }).sales_record_groups
        }

  scope :sales_record_joins,
        -> {
          joins(
            "
      INNER JOIN person_intakes pi
              ON pi.id = pt.target_id
             AND pt.target_type = 'PersonIntake'
      INNER JOIN person_intake_sales_record_masters pisrm
              ON pisrm.person_intake_id = pi.id
      INNER JOIN sales_record_masters srm
              ON srm.id = pisrm.sales_record_master_id
      INNER JOIN sales_record_summaries srs
              ON srs.person_id = pt.person_id
             AND srs.sales_record_master_id = pisrm.sales_record_master_id
             AND (
                  srs.person_intake_id is null
               OR srs.person_intake_id = pisrm.person_intake_id
             )
      INNER JOIN sip_stores ss ON ss.id = srm.sip_store_id
      INNER JOIN stores s ON s.id = ss.store_id
    "
          )
        }

  scope :sales_record_groups,
        -> {
          group(
            "
      pt.id,
      ss.display_group_id,
      srm.country,
      srm.period_type,
      srm.period_year,
      srm.period_interval,
      ss.id
    "
          )
        }

  scope :paginate_for_balance_history, ->(num_pages) { limit(num_pages * PAGINATION_LIMIT) }

  scope :pending_payout_transactions,
        -> {
          where(target_type: "PayoutTransfer").where(
            payout_transfers: {
              tunecore_status: [PayoutTransfer::SUBMITTED, PayoutTransfer::APPROVED],
              provider_status: PayoutTransfer::PENDING_PROVIDER_STATUSES
            }
          )
        }

  scope :irs_tax_withholding_transactions, -> { where(target_type: "IRSTaxWithholding") }
  scope :non_rollover_transactions, -> { where.not(target_type: "TaxableEarningsRollover") }

  def self.transaction_months(person)
    connection
      .select_values("
        SELECT distinct DATE_FORMAT(created_at,'%Y-%m-1') AS creation_month
          FROM person_transactions
         WHERE person_id = #{person.id}
         ORDER BY created_at DESC
      ")
      .collect(&:to_date)
  end

  def self.user_has_transactions?(person)
    person.person_transactions.exists?
  end

  def self.transaction_types_by_user(person)
    if Payoneer::FeatureService.payoneer_payout_enabled?(person)
      TRANSACTION_TYPES + PAYOUT_TRANSACTION_TYPE
    else
      TRANSACTION_TYPES
    end.select do |_, _, country_list|
      country_list.include?(person.country_domain)
    end
  end

  def self.generate_csv(transactions, sales)
    PersonTransactionExporter.export(transactions, sales)
  end

  def self.payout_transfer
    PayoutTransfer.arel_table
  end

  def self.process_auto_refund!(refund, amount_cents, _)
    PersonTransaction.create!(
      person: refund.person,
      debit: 0,
      credit: Tunecore::Numbers.cents_to_decimal(amount_cents),
      target: refund,
      comment: "Refund for Invoice #{refund.invoice.code}"
    )
  end

  def updated_balance
    raise ArgumentError, "cannot get updated balance until the transaction has been saved" if new_record?

    (previous_balance - debit + credit)
  end

  def debit?
    [debit, 0].compact.max.positive?
  end

  def credit?
    [credit, 0].compact.max.positive?
  end

  private

  def lock_person_balance
    @person_balance = PersonBalance.find_and_lock_balance(person)
  end

  def set_previous_balance
    self.previous_balance = @person_balance.balance
  end

  def update_balance
    @person_balance.update_balance(credit - debit)
  end

  def set_currency
    self.currency = person.currency
  end

  def currency_matches_person_balance
    return if currency == person.person_balance.currency

    errors.add(
      :base,
      I18n.t("models.billing.currency_does_not_match_person_balance_currency")
    )
  end
end
