class ItunesLinkNotification < Notification
  attr_accessor :release_type

  def setup_system_notification
    @release_type = notification_item.class.name.downcase if notification_item

    set_url
    set_title
    set_text
    set_link_text
    set_image_url
    auto_archive
  end

  def self.create_notifications(options = {})
    run_date = options[:run_date].present? ? Date.parse(options[:run_date]) : Date.today

    # Get the distinct album_ids
    # albums that have a sale date in the past get a notification when the known live is set
    # albums that have a sale date in the future get a notification on the sale live date if its known live and the apple id is set
    sql = %Q{
     (SELECT distinct albums.id
     FROM albums
     LEFT OUTER JOIN notifications on notifications.notification_item_type = 'Album' and notification_item_id = albums.id and notifications.type = '#{name}'
     WHERE albums.sale_date < ?
     and known_live_on = ?
     and apple_identifier is not null
     and albums.takedown_at is null
     and notifications.id is null
     and albums.album_type != 'Ringtone')

     UNION

     (SELECT distinct albums.id
      FROM albums
      LEFT OUTER JOIN notifications on notifications.notification_item_type = 'Album' and notification_item_id = albums.id and notifications.type = '#{name}'
      WHERE albums.sale_date = ?
      AND albums.known_live_on is not null
      and albums.apple_identifier is not null
      and albums.takedown_at is null
      and notifications.id is null
      and albums.album_type != 'Ringtone')}

    sql = ActiveRecord::Base.send(:sanitize_sql_array, [sql, run_date, run_date, run_date])
    results = ActiveRecord::Base.connection.execute(sql)
    album_ids = []
    results.each do |row|
      album_ids << row[0]
    end

    albums = Album.where("id in (?)", album_ids).includes(:person).all
    Rails.logger.info "Creating itunes live notifications for #{albums.size}"

    albums.each do |album|
      Rails.logger.info "Creating a release notification for person=#{album.person_id}, album=#{album.id}"
      ItunesLinkNotification.create(
        notification_item: album,
        person: album.person
      )
    end
  end

  private

  def auto_archive
  end

  def set_url
    self.url = "/albums/#{notification_item_id}"
  end

  def set_title
    self.title = "iTunes link available"
  end

  def set_text
    self.text = "Your album #{notification_item.name} is available in iTunes!"
  end

  def set_image_url
    self.image_url = notification_item.artwork.artwork.url(:small) if notification_item and notification_item.artwork
  end

  def set_link_text
    self.link_text = "Click here to see your iTunes link"
  end
end
