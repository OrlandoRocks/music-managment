class ExternalPurchaseItem < ActiveRecord::Base
  belongs_to :external_purchase
  belongs_to :related_item, polymorphic: true

  validates :related_item_id,
            uniqueness: { scope: [:related_item_type] }
end
