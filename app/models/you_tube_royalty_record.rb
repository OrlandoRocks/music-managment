class YouTubeRoyaltyRecord < ApplicationRecord
  include DatabaseStatementSettable

  belongs_to :person
  belongs_to :song

  def self.date_range_of_royalties(person)
    record = YouTubeRoyaltyRecord.where("person_id = ?", person.id).select("min(sales_period_start) as min_start_date, max(sales_period_start) as max_start_date").first
    return if record.nil?

    [record.min_start_date, record.max_start_date]
  end

  def self.months_with_royalties(person)
    YouTubeRoyaltyRecord.where("person_id=?", person.id).order("sales_period_start desc").select("distinct sales_period_start as sales_period").map(&:sales_period)
  end
end
