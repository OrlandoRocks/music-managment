class AlbumItunesStatus < ApplicationRecord
  validates :album, presence: true
  belongs_to :album

  def update_failure
    update(
      content_review_status: "Failed to retrieve state",
      itunes_connect_status: nil,
      stores_list: nil,
      itunes_created_at: nil,
      content_state_status: nil
    )
  end

  def update_success_xml(doc_xml)
    content_status_info = doc_xml.xpath("//content_status_info").first
    content_review_status = content_status_info.attributes["content_review_status"].value
    itunes_connect_status = content_status_info.attributes["itunes_connect_status"].value

    upload_status_info = doc_xml.xpath("//upload_status_info").first
    itunes_created_at = upload_status_info.attributes["created"].value
    content_state_status = upload_status_info.attributes["status"].value

    store_status = doc_xml.xpath("//store_status").first
    stores_list = store_status.attributes["on_store"].value if store_status.present?

    update(
      content_review_status: content_review_status,
      itunes_connect_status: itunes_connect_status,
      stores_list: stores_list,
      itunes_created_at: itunes_created_at,
      content_state_status: content_state_status
    )
  end
end
