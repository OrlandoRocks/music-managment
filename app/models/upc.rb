# frozen_string_literal: true

class Upc < ApplicationRecord
  class PhysicalUpcLimit < StandardError; end
  include UpcChecksumable
  belongs_to :upcable, polymorphic: true

  OPTIONAL_TYPE = "optional"
  PHYSICAL_TYPE = "physical"
  TUNECORE_TYPE = "tunecore"

  TUNECORE_PREFIX = "8597"

  scope :optional, -> { where(upc_type: OPTIONAL_TYPE) }
  scope :physical, -> { where(upc_type: PHYSICAL_TYPE) }
  scope :tunecore, -> { where(upc_type: TUNECORE_TYPE) }

  validates :number, presence: { if: :validate_number? }
  validates :number, uniqueness: { if: :validate_number? }
  validates :number, numericality: { greater_than: 0, if: :validate_number? }
  validates       :number, length: { within: 12..13, if: :optional_upc? }
  validates       :number, length: { is: 12, if: :physical_upc? }

  validate :checksum_matches                    # make sure that checksum is good
  validate :tunecore_prefix_use                 # make sure that only Tunecore UPCs can start with TuneCore's prefix
  validate :physical_prefix_use                 # make sure that only Physical UPCs can start with the Physical prefix

  after_create :set_number, if: :tunecore_upc?  # Tunecore UPCs are set based on row ID, thus we use after_create
  after_save :deactivate_other_upcs             # We only want 1 Tunecore UPC and 1 Optional UPC active at any given time

  def validate_number?
    !tunecore_upc?
  end

  def checksum_matches
    return unless validate_number? && number_length? && !checksum_matches?(number)

    errors.add(:number, I18n.t("models.media.is_not_valid"))
  end

  def tunecore_prefix_use
    return unless validate_number? && has_tunecore_prefix? && !tunecore_upc?

    errors.add(:number, I18n.t("models.media.cannot_start_with") + " #{TUNECORE_PREFIX}")
  end

  def physical_prefix_use
    return unless validate_number? && has_physical_prefix? && !physical_upc?

    errors.add(:number, I18n.t("models.media.cannot_start_with") + " #{PHYSICAL_UPC_PREFIXES}")
  end

  def set_number
    return unless number.blank? && !optional_upc?

    self.number = generate_upc
    Tunecore::Airbrake.notify("Unable to save UPC: #{errors.full_messages}") unless save
  end

  def a_table
    self.class.arel_table
  end

  def deactivate_other_upcs
    return unless number && active?

    upcable.upcs
           .where(a_table[:id].not_eq(id)
      .and(a_table[:upc_type].eq(upc_type)))
           .update_all(inactive: true)
  end

  def active?
    !inactive?
  end

  def tunecore_upc?
    upc_type == "tunecore"
  end

  def optional_upc?
    upc_type == "optional"
  end

  def physical_upc?
    upc_type == "physical"
  end

  def to_s
    number
  end

  def generate_upc
    upc_base = tunecore_upc_base
    upc_base.to_s + checksum(upc_base).to_s
  end

  def tunecore_upc_base
    TUNECORE_PREFIX.to_s + ("%07d" % id)
  end

  def number_length?
    number && number.length > 11 && number.length < 14
  end

  # TODO: This method will need to be modified to handle multiple prefixes if we ever go over 10MM products
  def has_tunecore_prefix?
    number =~ /^#{TUNECORE_PREFIX}/o
  end

  def has_physical_prefix?
    PHYSICAL_UPC_PREFIXES.any? { |prefix| number.starts_with?(prefix) }
  end
end
