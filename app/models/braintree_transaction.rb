# frozen_string_literal: true

class BraintreeTransaction < ApplicationRecord
  include TransactionResponse
  include BraintreePaymentSplit

  belongs_to :person
  belongs_to :stored_credit_card
  belongs_to :country_website
  has_many :refunds, -> { where(action: "refund", status: "success") }, class_name: "BraintreeTransaction", foreign_key: "related_transaction_id"
  has_many :refund_transactions, class_name: "BraintreeTransaction", foreign_key: "related_transaction_id"
  belongs_to :related_transaction, class_name: "BraintreeTransaction"
  belongs_to :refunded_by, class_name: "Person"
  belongs_to :invoice
  belongs_to :payin_provider_config

  has_many :invoice_logs

  validates :transaction_id, uniqueness: { unless: :skip_transaction_id_uniqueness? }
  validates :refund_reason, :refund_category, presence: { unless: :skip_refund_validations? }
  validates :person, presence: true

  after_commit :update_three_d_secure_information, if: -> { person.three_d_secure_enabled? && !renewal? }

  attr_accessor :gateway_response, :response_error_codes

  STATUSES = %w[success declined duplicate error fraud].freeze

  RESPONSE_CODE_STATUSES = { "10" => "success", "20" => "declined", "30" => "error" }.freeze

  REFUND = "refund"

  SUCCESS = "success"

  def self.process(query_string, person_id, ip_address)
    response = ActiveMerchant::Billing::Integrations::Braintree::Return.new(query_string, api_key: BRAINTREE[:key])

    txn = BraintreeTransaction.new
    if response.valid?

      txn.invoice_id = response.order_id
      txn.action = "sale"

      # nil is a more appropriate transaction_id for our purposes than 0 on failure
      txn.transaction_id = response.transaction_id unless response.transaction_id == "0"
      txn.amount = response.amount
      txn.response_code = response.response_code
      txn.gateway_response = response
      txn.ip_address = ip_address
      txn.person_id = person_id
      txn.avs_response = response.avs_result
      txn.cvv_response = response.cvv_result
      # ---- internationalization fields (3)
      txn.country_website = !Person.find_by(id: person_id).nil? ? Person.find_by(id: person_id).country_website : nil
      txn.currency = response.currency
      txn.processor_id = response.processor_id
      # ----
      txn.set_status(query_string)
      txn.detect_duplicates
      # this was abstracted into a :before_create filter but we're only doing this operation in the process method
      # so it was more expressive to put it here and only run the option during processing.
      if txn.status == "success" && response.store?
        scc = StoredCreditCard.where("customer_vault_id = ? and person_id = ?", response.customer_id, person_id).first
        txn.stored_credit_card = !scc.nil? ? scc : StoredCreditCard.create(person_id: txn.person_id, customer_vault_id: response.customer_id)

        if response.mark_as_preferred? && !txn.stored_credit_card.nil?
          preference = PersonPreference.where(person_id: person_id).first
          if preference.nil?
            PersonPreference.create(person_id: person_id, preferred_credit_card: txn.stored_credit_card)
          elsif preference.preferred_credit_card_id.nil?
            preference.update_attribute(:preferred_credit_card_id, txn.stored_credit_card.id)
          end
        end
      end

      txn.save

      txn.log_transaction(response.response_code)
      txn.settle_invoice
    else # toss requests with invalid hashes completely
      txn.log_transaction("", invalid: true, ip_address: ip_address, person_id: person_id)
      txn.status = "invalid"
    end

    txn
  end

  def self.process_refund(id, refund_amount, options = {})
    original = find(id)
    gateway = ActiveMerchant::Billing::BraintreeGateway.new(password: BRAINTREE[:password], login: BRAINTREE[:username])

    # ensure that we're submitting a refund that does not exceed the original transaction
    amount_available_to_refund = original.amount - (original.refunded_amount || 0)
    final_amount_to_refund = (amount_available_to_refund.to_money > refund_amount.to_money) ? refund_amount : amount_available_to_refund

    txn = BraintreeTransaction.new
    txn.invoice_id = original.invoice_id
    txn.action = "refund"
    txn.related_transaction_id = original.id
    txn.amount = final_amount_to_refund
    txn.ip_address = options[:ip_address]
    txn.refunded_by_id = options[:refunded_by_id]
    txn.refund_reason = options[:refund_reason]
    txn.refund_category = options[:refund_category]
    txn.person_id = original.person_id
    txn.stored_credit_card_id = original.stored_credit_card_id
    txn.country_website = original.country_website
    txn.currency = original.currency
    txn.payin_provider_config = original.payin_provider_config

    if txn.stored_credit_card.bt_token.present?
      elapsed_time = txn.process_braintree_blue_refund(original, final_amount_to_refund)
    else
      start_time = Time.now
      response = gateway.refund(final_amount_to_refund.to_money, original.transaction_id, currency: original.currency, processor_id: original.processor_id)
      elapsed_time = Time.now - start_time
      txn.raw_response = response.to_yaml

      # nil is a more appropriate transaction_id for our purposes than 0 on failure
      txn.transaction_id = response.params["transactionid"] unless response.params["transactionid"] == "0"
      txn.response_code = response.params["response_code"]
      txn.processor_id = response.params["processor_id"]
      txn.set_status(response.to_yaml)

      BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice.id, response)
    end

    txn.detect_duplicates
    txn.save

    txn.log_transaction(txn.response_code, elapsed_time: elapsed_time)
    original.update_refunded_amount!

    txn
  end

  #
  # process_with_stored_card makes purchases with a user's stored_credit_card via Braintree's direct post API
  #
  def self.process_purchase_with_stored_card(invoice, stored_credit_card, options = {})
    payment_amount = BigDecimal(options[:payment_amount]) / 100
    country_website = stored_credit_card.person.country_website

    txn = BraintreeTransaction.new
    txn.invoice = invoice
    txn.action = "sale"
    txn.country_website = country_website
    txn.currency = country_website.currency
    txn.person_id = stored_credit_card.person_id
    txn.stored_credit_card = stored_credit_card
    txn.amount = payment_amount
    txn.ip_address = options[:ip_address]
    txn.payin_provider_config = txn.braintree_payment_splits_config(txn.person, { is_renewal: txn.renewal? })

    if stored_credit_card.bt_token.present?
      elapsed_time = txn.process_braintree_blue_transaction(payment_amount, options)
    else
      gateway = ActiveMerchant::Billing::BraintreeGateway.new(password: BRAINTREE[:password], login: BRAINTREE[:username])
      start_time = Time.now
      # note that options[:payment_amount] is an object of the Money class, which has both the amount and currency within it.
      response = gateway.purchase(
        options[:payment_amount],
        stored_credit_card.customer_vault_id,
        order_id: invoice.id,
        processor_id: country_website.braintree_processor,
        currency: country_website.currency
      )
      elapsed_time = Time.now - start_time

      BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice.id, response)
      # nil is a more appropriate transaction_id for our purposes than 0 on failure
      txn.transaction_id = response.params["transactionid"] unless response.params["transactionid"] == "0"
      txn.gateway_response = response
      status = response.params["response_code"]
      txn.response_code = status
      txn.avs_response = response.avs_result["code"]
      txn.cvv_response = response.cvv_result["code"]
      # ---- internationalization fields (3)
      txn.currency = response.params["currency"]
      txn.processor_id = response.params["processor_id"]
      # ----

      txn.set_status(response.to_yaml)
    end

    txn.detect_duplicates
    txn.save
    txn.log_transaction(status, elapsed_time: elapsed_time)

    begin
      txn.settle_invoice
    rescue => e
      Airbrake.notify(e)
      Rails.logger.error "BraintreeTransaction#process_purchase_with_stored_card Failed: #{e.message}"
      Rails.logger.error e.backtrace
    end

    txn
  end

  def self.process_batch_response_row(txn_id, response, avs, cvv, amount, batch_transaction_id, invoice_id, person_id, processor_id)
    batch_transaction = BatchTransaction.find(batch_transaction_id.to_i)
    credit_card_id = !batch_transaction.nil? ? batch_transaction.stored_credit_card_id : nil
    country_website_id = !batch_transaction.nil? ? batch_transaction.stored_credit_card.person.country_website_id : nil
    currency = !batch_transaction.nil? ? batch_transaction.currency : nil
    payin_provider_config = !batch_transaction.nil? ? batch_transaction.stored_credit_card.payin_provider_config : nil

    txn = BraintreeTransaction.new(
      country_website_id: country_website_id,
      transaction_id: (txn_id == "0") ? nil : txn_id,
      response_code: response,
      invoice_id: invoice_id,
      avs_response: avs,
      cvv_response: cvv,
      stored_credit_card_id: credit_card_id,
      action: "sale",
      amount: amount,
      person_id: person_id,
      ip_address: "batch",
      processor_id: processor_id,
      currency: currency,
      payin_provider_config: payin_provider_config
    )
    txn.set_status(response.to_yaml) # these two lines should be combined into a before_create
    txn.detect_duplicates #
    txn.save
    txn.log_transaction(response) # this should probably be refactored into an after_create
    txn.settle_invoice # this should be an after_create

    BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice_id, response)
    txn
  end

  def self.search_transactions(conditions, options = {})
    select("bt.id, bt.transaction_id, bt.created_at, bt.status, bt.action, bt.amount, bt.refunded_amount, p.name, bt.person_id, bt.invoice_id, bt.currency, bt.country_website_id")
      .joins("bt INNER JOIN people p ON p.id=bt.person_id")
      .where(conditions)
      .order(options[:sort] || "bt.created_at DESC")
      .paginate(page: options[:page], per_page: (options[:per_page] || 10).to_i)
  end

  def self.process_auto_refund!(refund, amount, invoice_settlement)
    admin_login_ip = refund.refunded_by.last_logged_in_ip
    options = {
      refunded_by_id: refund.refunded_by_id,
      refund_reason: refund.refund_reason&.reason,
      refund_category: refund.label,
      ip_address: admin_login_ip
    }

    # Amount should be in dollars.
    amount /= 100.0

    transaction = process_refund(invoice_settlement.source_id, amount, options)

    raise AutoRefunds::Utils::SettlementFailed.new(transaction.build_refund_error_message) unless transaction.success?

    transaction
  end

  def sale?
    action == "sale"
  end

  def success?
    status == "success"
  end

  def duplicate?
    status == "duplicate"
  end

  def error?
    status == "error"
  end

  #
  # update_refunds! updates transaction with the total amount refunded
  # This keeps a cached total of all refunds in the original record (:refunded_amount).
  # Otherwise, we'd have to sum up and call the database every time we want to count/total refunds.
  # CH - 2009/9/23 - Created method
  #
  def update_refunded_amount!
    update_attribute(:refunded_amount, refunds.sum(:amount))
  end

  def refunded?
    !refunded_amount.nil? && refunded_amount.positive?
  end

  #
  # totally_refunded deals with edge cases where a transaction is refunded twice for smaller than original payment amounts.
  # If we just checked for refunds.nil? we wouldn't know if there was still an amount left
  # on the original transaction
  # 2009-9-22 - CH - created method
  #
  def totally_refunded?
    !refunded_amount.nil? && refunded_amount >= amount
  end

  def can_refund?
    # This transaction has not been refunded, it was a sale, and the status was either successful or duplicate
    !totally_refunded? && sale? && (success? || duplicate?)
  end

  def payment_amount_cents
    amount * 100
  end

  def log_transaction(response_code, options = {})
    logged_action = "Braintree Transaction |"
    if options[:invalid].nil?
      logged_action += " ip=#{ip_address} " if ip_address
      logged_action += " transaction_type=#{action} "
      logged_action += " amount=#{amount} "
      logged_action += " currency=#{currency} "
      logged_action += " processor_id=#{processor_id} "
      logged_action += " response_code=#{response_code} "
      logged_action += " person_id=#{person_id} "
      logged_action += " invoice_id=#{invoice_id}" unless invoice_id.nil?
      logged_action += " stored_credit_card_id=#{stored_credit_card_id} " unless stored_credit_card_id.nil?
      logged_action += " card_type=#{stored_credit_card.cc_type} " if stored_credit_card
      logged_action += " elapsed_time=#{options[:elapsed_time]}" if options[:elapsed_time]

      logged_action += " transaction_id=#{transaction_id}"
      logged_action += " status=#{status}"
    else
      logged_action += " response_code=-1 "
      logged_action += " ip=#{options[:ip_address]} "
      logged_action += " person_id=#{options[:person_id]} "
    end
    logger.info logged_action
  end

  # Translate the status code from the gateway to one of our statuses (success,fraud,declined,error,duplicate)
  def set_status(raw_response)
    if errors.empty?
      case response_code.to_i
      when 100
        self.status = "success"
      when 250..253
        self.status = "fraud"
        Note.create(note: "Attempted a potentially fraudulent credit card purchase. Status Code: #{response_code}", note_created_by_id: 0, related: person, subject: "Possibly Fraudulent Purchase")
      when 200..299
        self.status = "declined"
      when 400
        # Braintree sometimes sends a response with 400 and a raw_response with NO+CHECKING+ACCT which means that the credit card
        # is not currently active. The card could be brand new or recently reissued as a replacement for an existing account
        self.status =
          if raw_response.include?("NO+CHECKING+ACCT")
            "declined"
          else
            "duplicate"
          end
      else
        # Braintree sometimes sends the AVS and CVV rejection as a 300 error code so it's necessary to mark the transaction declined
        # after checking the raw_response
        self.status =
          if raw_response.match?(/(AVS.*REJECTED|CVV.*REJECTED|CVV.*must.*be)/)
            "declined"
          else
            "error"
          end
      end
    else
      self.status = "error"
    end
  end

  def settle_invoice
    return unless success? && sale? && !invoice.settled?

    invoice.settlement_received(self, payment_amount_cents)
    invoice.settled! if invoice.can_settle?
  end

  # Mark transactions as duplicate if the invoice has already been paid
  def detect_duplicates
    if status == "success" &&
       action == "sale" &&
       BraintreeTransaction.exists?(
         person_id: person_id,
         invoice_id: invoice_id,
         status: "success",
         action: "sale"
       )

      self.status = "duplicate"
    end
  end

  def process_braintree_blue_transaction(payment_amount, options = {})
    start_time = Time.now

    result =
      if options[:three_d_secure_nonce].present? && stored_credit_card.require_three_d_secure_transaction? && !renewal?
        create_3ds_sale_transaction(payment_amount, options)
      else
        create_sale_transaction(payment_amount)
      end

    elapsed_time = Time.now - start_time

    self.transaction_id = result.try(:transaction).try(:id)
    self.response_code = result.try(:transaction).try(:processor_response_code) || "3000"
    self.status = blue_status

    BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice.id, result)
    begin
      TransactionResponse.process_transaction_params(self, result)
    rescue => e
      Airbrake.notify(
        "Additional Braintree transaction response processing failed",
        {
          error: e,
          sale_result: result.to_yaml,
          transaction_id: id
        }
      )
    end
    elapsed_time
  end

  def create_sale_transaction(payment_amount)
    transaction_data = {
      amount: payment_amount,
      payment_method_token: stored_credit_card.bt_token,
      merchant_account_id: person.braintree_config_by_corporate_entity.merchant_account_id,
      options: { submit_for_settlement: true },
      order_id: invoice_id
    }

    transaction_data[:transaction_source] = "unscheduled" if person.payment_splits_renewal_enabled? && renewal?

    stored_credit_card.config_gateway_service.sale(transaction_data)
  end

  def create_3ds_sale_transaction(payment_amount, options)
    stored_credit_card.config_gateway_service.sale(
      amount: payment_amount,
      payment_method_nonce: options[:three_d_secure_nonce],
      merchant_account_id: person.braintree_config_by_corporate_entity.merchant_account_id,
      options: { submit_for_settlement: true },
      order_id: invoice_id
    )
  end

  def process_braintree_blue_refund(original, refund_amount)
    start_time = Time.now
    result = original
             .stored_credit_card
             .config_gateway_service
             .refund(original.transaction_id, refund_amount.to_s)
    elapsed_time = Time.now - start_time

    if result.success?
      self.transaction_id = result.transaction.id
      self.response_code = result.transaction.processor_response_code
      self.status = blue_status
    elsif original_submitted_for_settlement?(original) && original.amount == refund_amount
      elapsed_time += void_blue_transaction(original.transaction_id)
    else
      self.response_code = "3000"
      self.status = "error"
    end

    self.response_error_codes = result.errors.map(&:code) if result.is_a?(Braintree::ErrorResult)

    BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice.id, result)
    begin
      TransactionResponse.process_transaction_params(self, result)
    rescue => e
      Airbrake.notify(
        "Additional Braintree transaction response processing failed",
        {
          error: e,
          sale_result: result.to_yaml,
          transaction_id: id
        }
      )
    end
    elapsed_time
  end

  def blue_status
    if errors.empty?
      if response_code == "2016"
        "duplicate"
      elsif %w[2012 2013 2014].include? response_code
        note = Note.create(note: "Made a fraudulent credit card purchase. Status Code: #{response_code}", note_created_by_id: 0, related: person, subject: "Marked as Suspicious")
        person.mark_as_suspicious!(note)
        "fraud"
      elsif !response_code
        self.response_code = 3000
        "error"
      else
        RESPONSE_CODE_STATUSES[response_code[0..1]]
      end
    else
      "error"
    end
  end

  def braintree_gateway_transaction(original)
    return @braintree_gateway_transaction unless @braintree_gateway_transaction.nil?

    @braintree_gateway_transaction =
      original
      .stored_credit_card
      .config_gateway_service
      .find_transaction(original.transaction_id)
  end

  def original_submitted_for_settlement?(original)
    braintree_gateway_transaction(original).status == "submitted_for_settlement"
  end

  def build_refund_error_message
    response_error_codes.map { |error_code| error_code_message(error_code) }.join(" ")
  end

  # Value of ip_address field is either 'renewal' or 'batch' if it's a renewal transaction.
  def renewal?
    ip_address == "renewal" || ip_address == "batch"
  end

  private

  def error_code_message(error_code)
    case error_code
    when "91506"
      "Cannot issue partial refunds to credit cards if the invoice is settled on the same day."
    when "915200"
      "This refund cannot be completed because the processor has declined the refund."
    else
      response_message
    end
  end

  # Braintree does not set the transaction_id unless the transaction was successful, so we only check if it is unique when it has a value
  def skip_transaction_id_uniqueness?
    transaction_id.nil?
  end

  # if there is no related transaction, this is not a refund and we don't need to validate refund fields
  def skip_refund_validations?
    related_transaction_id.nil?
  end

  def void_blue_transaction(original_transaction_id)
    original = BraintreeTransaction.find_by(transaction_id: original_transaction_id)
    start_time = Time.now
    result = original
             .stored_credit_card
             .config_gateway_service
             .void(original_transaction_id)
    elapsed_time = Time.now - start_time

    self.response_code = result.try(:transaction).try(:processor_response_code) || "3000"
    self.status = blue_status

    elapsed_time
  end

  def update_three_d_secure_information
    return if transaction_id.nil? || liability_shifted.present?

    ThreeDSecureInfoWorker.perform_async(id)
  end
end
