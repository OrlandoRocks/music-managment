require "S3"
class Booklet < ApplicationRecord
  belongs_to :album
  belongs_to :s3_asset

  has_one :purchase, as: :related
  has_one :person, through: :album

  delegate :person_id, to: :album

  before_destroy :check_if_used

  def s3_key
    "booklet/#{album.id}.pdf"
  end

  def paid?
    paid = paid_at.present?
    paid ||= purchase.paid? if purchase
    paid ||= Inventory.used?(self)
    paid
  end

  def check_if_used
    raise RuntimeError, "This booklet has been paid for--it cannot be destroyed" if Inventory.used?(self)
  end

  # needs to perform a test to see if the file has been uploaded and placed on S3
  def uploaded?
    s3_asset != nil
  end

  def url
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = 300
    generator.get(BOOKLET_BUCKET_NAME, s3_asset.key)
  end

  def can_distribute?
    if Purchase.currently_in_cart?(person, album)
      true
    else
      false
    end
  end

  def replace_booklet(booklet_file)
    asset = s3_asset

    unless asset.bucket == BOOKLET_BUCKET_NAME
      raise "You cannot replace a booklet in a different environment it was created in"
    end

    begin
      asset.filename = booklet_file.path
      asset.put!
      asset.save
    rescue
      return false
    end

    true
  end

  def paid_post_proc
    update({ paid_at: current_time_from_proper_timezone })
  end
end
