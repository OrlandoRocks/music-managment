class ReferralDatum < ApplicationRecord
  @@allowed_parameters = [
    :ref,
    :jt,
    :cmp,
    :tkn,
    :utm_source,
    :utm_medium,
    :utm_term,
    :utm_content,
    :utm_campaign,
    :fbuy_ref_code
  ]
  belongs_to :person

  def self.allowed_parameters
    @@allowed_parameters
  end
end
