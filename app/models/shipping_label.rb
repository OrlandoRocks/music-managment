# == Schema Information
# Schema version: 404
#
# Table name: shipping_labels
#
#  id                    :integer(11)     not null, primary key
#  video_id              :integer(11)
#  price_policy_id       :integer(11)
#  weight                :float
#  service_type          :string(255)
#  label                 :binary
#  address1              :string(255)
#  address2              :string(255)
#  city                  :string(255)
#  state                 :string(255)
#  zip                   :string(255)
#  tracking_number       :string(255)
#  fedex_cost_cents      :integer(11)
#  purchase_id           :integer(11)
#  printed               :boolean(1)
#  shipping_service_type :string(30)
#  global_flag           :boolean(1)

class ShippingLabel < ApplicationRecord
  belongs_to :video

  before_validation :set_weight, :trim_spaces

  with_options if: :not_global? do |ng|
    ng.validates_presence_of :address1, :city, :state, :zip, :weight
    ng.validates_length_of :state, is: 2
    ng.validates_numericality_of :zip, if: proc { |record| record.zip.present? }
    ng.validates_length_of :zip, is: 5, if: proc { |record| record.zip.present? }
  end

  def trim_spaces
    self.address1 = address1.strip if address1
    self.address2 = address2.strip if address2
    self.city = city.strip if city
    self.state = state.strip.upcase if state
    self.zip = zip.to_s.strip if zip
  end

  def tracking_number?
    tracking_number.present?
  end

  def global_flag=(arg)
    self[:global_flag] = arg
  end

  def not_global?
    !global_flag
  end

  def fedex_label_url
    "/videos/#{video.id}/fedex_label"
  end

  # Price to display on the website
  def fedex_price
    return unless fedex_price_cents

    (fedex_price_cents.to_f / 100.00)
  end

  # Gets the price of the shipment from FedEx and saves it in video_cost_cents
  def fedex_price_cents
    return fedex_cost_cents unless fedex_cost_cents.nil?

    fedex_api = Fedex.label_for_video(video)
    price_cents = fedex_api.discount_price * 100
    self.fedex_cost_cents = price_cents
    save!
    price_cents.to_i
  rescue => e
    logger.error "*** Couldn't get price from fedex!"
    nil
  end

  def ship_via
    shipping_service_type
  end

  def ship_via=(carrier)
    # interface to the shipping_service_type data
    # raise "stop"
    self.shipping_service_type =
      case carrier
      when "fedex"
        "fedex"
      when "customer" || "customer_arranged"
        "customer_arranged"
      else
        "customer_arranged"
      end
  end

  # Generates a new fedex label
  def generate_fedex_label
    fedex_api = Fedex.label_for_video(video)
    label = fedex_api.label
    return false if label.nil?

    image = []
    label.image.each_line { |line| image << line }
    price_cents = fedex_api.discount_price * 100
    self.attributes = {
      tracking_number: label.tracking_number,
      label: image.join,
      fedex_cost_cents: price_cents
    }
    save!
    fedex_api
  end

  private

  def set_weight
    self.weight = 2.0
    self.service_type = "ground_service"
  end
end
