class InvoiceLog < ApplicationRecord
  belongs_to :person
  belongs_to :invoice
  belongs_to :purchase
  belongs_to :batch_transaction
  belongs_to :payment_batch
  belongs_to :braintree_transaction
  belongs_to :paypal_transaction
  belongs_to :stored_paypal_account
  belongs_to :renewal

  belongs_to :foreign_exchange_pegged_rate,
             primary_key: "id",
             foreign_key: "invoice_foreign_exchange_pegged_rate_id",
             class_name: "ForeignExchangePeggedRate"

  belongs_to :product, primary_key: "id", foreign_key: "purchase_product_id", class_name: "Product"
  belongs_to :salepoint, primary_key: "id", foreign_key: "purchase_salepoint_id", class_name: "Salepoint"
  belongs_to :targeted_product,
             primary_key: "id",
             foreign_key: "purchase_targeted_product_id",
             class_name: "TargetedProduct"
  belongs_to :no_purchase_album,
             primary_key: "id",
             foreign_key: "purchase_no_purchase_album_id",
             class_name: "Album"

  before_save :truncate_strings

  def truncate_strings
    max_char_length = 255
    self.message               = message[0, max_char_length] if message.present?
    self.caller_method_path    = caller_method_path[0, max_char_length] if caller_method_path.present?
    self.current_method_name   = current_method_name[0, max_char_length] if current_method_name.present?
    self.purchase_related_type = purchase_related_type[0, max_char_length] if purchase_related_type.present?
    self.purchase_status       = purchase_status[0, max_char_length] if purchase_status.present?
  end
end
