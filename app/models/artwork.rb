class Artwork < ApplicationRecord
  include S3Downloadable

  attr_accessor :unprocessed_file

  MINIMUM_ARTWORK_HEIGHT_PX = 1600
  MAXIMUM_ARTWORK_HEIGHT_PX = 3000
  MAXIMUM_ARTWORK_FILE_SIZE_BIT = 10_000_000 # 10MB
  MAXIMUM_ARTWORK_SQUARE_DIMENSIONS_DELTA_PX = 20 # allow a difference of 20px between height and width
  ARTWORK_VALID_CONTENT_TYPES = ["JPG", "JPEG", "PNG", "GIF"].freeze

  ORIGINAL_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/artv1/:filename",
    bucket: ORIG_BUCKET_NAME,
    s3_credentials: {
      access_key_id: AWS_ACCESS_KEY,
      secret_access_key: AWS_SECRET_KEY
    },
    s3_permissions: :public_read
  }.freeze

  ARTWORK_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/artwork/:style/:album_id_hash/:filename",
    bucket: SCALED_ARTWORK_BUCKET_NAME,
    s3_credentials: {
      access_key_id: ASSETS_AWS_ACCESS_KEY,
      secret_access_key: ASSETS_AWS_SECRET_KEY
    },
    s3_permissions: :public_read,
    s3_protocol: :https,
    url: ":s3_path_url",
    s3_host_alias: SCALED_ARTWORK_BUCKET_NAME
  }.freeze

  belongs_to :album

  # Stored in tunecore.com bucket in aws account that petri uses
  has_attached_file :original, ORIGINAL_STORAGE_OPTIONS

  # Stored in new aws account under pub.tunecore.com bucket
  has_attached_file :artwork,
                    {
                      styles: {
                        large: {
                          geometry: "600x600",
                          format: :jpg
                        },
                        medium: {
                          geometry: "400x400",
                          format: :jpg
                        },
                        small: {
                          geometry: "100x100",
                          format: :jpg
                        }
                      }
                    }.merge(ARTWORK_STORAGE_OPTIONS)

  delegate :url, to: :artwork

  # NOTE: Paperclip 1/2: default validations are triggered on
  #   converted .tiff file (not file submitted by user through the form)
  validates_with AttachmentPresenceValidator, attributes: :artwork
  validates_with AttachmentContentTypeValidator,
                 attributes: :artwork,
                 content_type: ["image/jpeg", "image/gif", "image/png", "image/tiff"]
  # deactivated because reencoded square .tiff file can be > 10MB
  # validates_with AttachmentSizeValidator, attributes: :artwork,
  #   less_than_or_equal_to: 10.megabytes
  validates_with CustomAttachmentDimensionsValidator,
                 attributes: :artwork,
                 dimensions: {
                   min_width: MINIMUM_ARTWORK_HEIGHT_PX,
                   max_width: MAXIMUM_ARTWORK_HEIGHT_PX
                 },
                 shape: "square"

  # NOTE: Paperclip 2/2: custom validations are triggered on user submitted file (unprocessed_file)
  validate :validate_file_size, :validate_content_type, :validate_artwork_colorspace, :validate_dimensions

  def validate_file_size
    return true unless unprocessed_file

    if unprocessed_file.size > MAXIMUM_ARTWORK_FILE_SIZE_BIT
      self.last_errors = I18n.t("model.artwork.invalid_file_size")
      errors.add(:artwork, I18n.t("model.artwork.invalid_file_size"))
    end
    true
  end

  def validate_content_type
    return true unless unprocessed_file

    return if valid_content_type?

    self.last_errors = I18n.t("model.artwork.invalid_content_type")
    errors.add(:artwork, I18n.t("model.artwork.invalid_content_type"))
  end

  def validate_artwork_colorspace
    # NOTE: looks like the validation is done on the processed .tiff file
    return if artwork.blank?

    filepath = File.join(Rails.root, UPLOAD_ARTWORK_PATH, artwork.original_filename)
    image = MiniMagick::Image.open(filepath) rescue nil
    # NOTE: MiniMagick gotcha, image.colorspace refers to a different image
    #       property representing the image class and colorspace and this can
    #       sometimes differ (for reasons unknown) from the raw image colorspace
    return unless image && image.info("%[colorspace]") !~ /rgb/i

    self.last_errors = I18n.t("model.artwork.invalid_colorspace")
    errors.add(:artwork, I18n.t("model.artwork.invalid_colorspace"))
  end

  def validate_dimensions
    return if valid_dimensions?

    last_errors_invalid_min_dimensions unless valid_min_dimensions?
    last_errors_invalid_max_dimensions unless valid_max_dimensions?
    last_errors_invalid_square_dimensions unless valid_square_dimensions?
    self.artwork = nil
    self.uploaded = false
    self.height = nil
    self.width = nil
    save validate: false
  end

  def valid_content_type?
    ARTWORK_VALID_CONTENT_TYPES.include?(unprocessed_file.type)
  end

  def valid_min_dimensions?
    return true unless unprocessed_file

    Array(unprocessed_file.dimensions).min.to_i >= MINIMUM_ARTWORK_HEIGHT_PX
  end

  def valid_max_dimensions?
    return true unless unprocessed_file

    Array(unprocessed_file.dimensions).max.to_i <= MAXIMUM_ARTWORK_HEIGHT_PX
  end

  def valid_square_dimensions?
    return true unless unprocessed_file

    dimensions_ar = Array(unprocessed_file.dimensions)
    (dimensions_ar.max.to_i - dimensions_ar.min.to_i) <= MAXIMUM_ARTWORK_SQUARE_DIMENSIONS_DELTA_PX
  end

  def valid_dimensions?
    valid_min_dimensions? && valid_max_dimensions? && valid_square_dimensions?
  end

  def last_errors_invalid_min_dimensions
    logger.debug("the cover image was too small")
    self.last_errors =
      I18n.t(
        "model.artwork.dimensions_error_too_small",
        min_width: MINIMUM_ARTWORK_HEIGHT_PX,
        min_height: MINIMUM_ARTWORK_HEIGHT_PX
      )
  end

  def last_errors_invalid_max_dimensions
    logger.debug("the cover image was too large")
    self.last_errors =
      I18n.t(
        "model.artwork.dimensions_error_too_large",
        max_width: MAXIMUM_ARTWORK_HEIGHT_PX,
        max_height: MAXIMUM_ARTWORK_HEIGHT_PX
      )
  end

  def last_errors_invalid_square_dimensions
    logger.debug("the cover image must be a square")
    self.last_errors =
      I18n.t("model.artwork.dimensions_error_square")
  end

  before_save :set_uploaded
  after_save  :cleanup_temp_images

  def cleanup_temp_images
    begin
      logger.debug("Deleting temporary file: #{square_tiff_path}")
      File.delete(square_tiff_path)
    rescue => e
      logger.debug("Could not delete #{square_tiff_path}: #{e}")
    end
    true
  end

  def set_uploaded
    return if last_errors.present?

    self.uploaded = true

    return unless Array(unprocessed_file&.dimensions).any?

    self.height = unprocessed_file.dimensions.min
    self.width  = unprocessed_file.dimensions.min
  end

  def artwork_file_name
    "#{album_id}.tiff"
  end

  def artwork_file_name=(name)
  end

  def original_file_name
    artwork_file_name
  end

  def original_file_name=(name)
  end

  def assign_artwork(file, auto_generated = false)
    self.last_errors = nil
    self.auto_generated = auto_generated
    file.rewind
    image = MiniMagick::Image.open(file.path)
    # NOTE: Image file here is the one provided by the user
    # It can be anything as no check is done prior to this one
    @unprocessed_file = image

    return unless valid_dimensions?

    make_square_tiff(file)
    assign_square_tiff_file
  end

  def copy_image_file_from_s3(s3_artwork_keys)
    filename = "#{Rails.root}/tmp/artwork_api/#{album_id}"
    FileUtils.mkdir_p(File.dirname(filename))
    bucket = S3_CLIENT.buckets[s3_artwork_keys[:bucket]]
    artwork_object = bucket.objects[s3_artwork_keys[:key]]
    File.open(filename, "wb") do |file|
      artwork_object.read do |chunk|
        file.write(chunk)
      end
    end

    artwork_image = File.open(filename)
    filename = s3_artwork_keys[:key].split("/").last
    assign_artwork ActionDispatch::Http::UploadedFile.new(
      tempfile: artwork_image,
      filename: filename,
      type: "image/jpeg"
    )
    artwork_image.close
  end

  #
  # For legacy compatible after paperclip switch.  Petri relies on this model having an s3_asset
  # that responds to key.
  #
  def s3_asset
    S3Asset.new(key: original.path(:original).gsub(/^\//, ""), bucket: original.options[:bucket]) unless new_record?
  end

  def square_tiff_path
    File.join(Rails.root, UPLOAD_ARTWORK_PATH, "#{album_id}.tiff")
  end

  def make_square_tiff(file)
    image = MiniMagick::Image.open(file.path)
    square_side_length = unprocessed_file&.dimensions&.min

    image.combine_options do |b|
      b.gravity "center"
      b.crop "#{square_side_length}x#{square_side_length}+0+0"
    end

    image.format "tiff"

    # prevent colorspace conversion when source image is gray scale but rgb
    # NOTE: MiniMagick gotcha, image.colorspace refers to a different image
    #       property representing the image class and colorspace and this can
    #       sometimes differ (for reasons unknown) from the raw image colorspace
    image.colorspace image.info("%[colorspace]")
    image.type "truecolor"

    image.write(square_tiff_path)
  end

  def assign_square_tiff_file
    self.original = File.open(square_tiff_path)
    self.artwork  = File.open(square_tiff_path)
  end

  # used to determine if the artwork is over 1600x1600
  # do we need to check dpi???
  def high_quality?
    (width.blank? || height.blank?) ? false : width >= 1600 && height >= 1600
  end

  #
  #  The default url to show if the requested file could not be found
  #
  def self.default_url(size = :original)
    case size
    when :large
      "/images/albums/noartwork_small.jpg"
    when :medium
      "/version/tunecore_neo/images/backgrounds/400_dummy_album.gif"
    when :small
      "/images/albums/noartwork_small.jpg"
    else
      "/images/albums/noartwork_small.jpg"
    end
  end
end
