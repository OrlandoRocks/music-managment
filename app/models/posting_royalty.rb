class PostingRoyalty < ApplicationRecord
  RAKE_USER = "rake_user".freeze
  PENDING = "pending".freeze
  APPROVED = "approved".freeze
  SUBMITTED = "submitted".freeze

  enum state: {
    pending: PENDING,
    submitted: SUBMITTED,
    approved: APPROVED
  }
end
