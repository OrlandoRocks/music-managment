class Cowriter < ApplicationRecord
  include PrimaryComposerable

  belongs_to :composer
  belongs_to :performing_rights_organization

  has_many :rights_app_errors, as: :requestable
  has_many :publishing_splits, as: :writer

  validates :composer, presence: true
  validates :first_name, presence: true, exclusion: { in: %w[unknown] }, unless: :unknown?
  validates :last_name, presence: true, exclusion: { in: %w[unknown] }, unless: :unknown?

  delegate :account, to: :composer

  def full_name
    unknown? ? "" : "#{first_name} #{last_name}"
  end

  def unknown?
    is_unknown
  end
end
