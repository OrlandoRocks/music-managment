class TierAchievement < ApplicationRecord
  belongs_to :tier
  belongs_to :achievement

  validates :tier_id, uniqueness: { scope: :achievement_id }
end
