# frozen_string_literal: true

class AdyenPaymentMethodInfo < ApplicationRecord
  CUP = "cup"

  IDEAL_PAYMENT_METHOD = "ideal"

  GOOGLEPAY_PAYMENT_METHOD = "googlepay"

  GOOGLEPAY_VARIANTS = ["amex_googlepay", "discover_googlepay", "mc_googlepay", "visa_googlepay"]

  SEPADIRECTDEBIT = "sepadirectdebit"

  APPLEPAY_PAYMENT_METHOD = "applepay"

  APPLEPAY_VARIANTS = Set[
  "amex_applepay",
  "discover_applepay",
  "mc_applepay",
  "visa_applepay"
  ].freeze

  PAYMENT_METHOD_DISPLAY_NAMES = {
    cup: "UnionPay",
    paymaya_wallet: "Maya Wallet",
    gcash: "GCash",
    momo_wallet: "Momo Wallet",
    dana: "DANA",
    gopay_wallet: "GoPay Wallet",
    ideal: "iDEAL",
    googlepay: "Google Pay",
    applepay: "Apple Pay"
  }.with_indifferent_access.freeze

  def payment_method_display_name
    PAYMENT_METHOD_DISPLAY_NAMES.fetch(payment_method_name)
  end
end
