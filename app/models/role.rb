# frozen_string_literal: true

# == Schema Information
# Schema version: 404
#
# Table name: roles
#
#  id   :integer(11)     not null, primary key
#  name :string(255)
#

class Role < ApplicationRecord
  has_and_belongs_to_many :people
  has_and_belongs_to_many :rights
  has_and_belongs_to_many :client_applications
  has_and_belongs_to_many :review_reasons

  scope :administrative,
        -> {
          where("long_name IS NOT NULL").order(
            "is_administrative DESC",
            "id ASC",
            "long_name ASC"
          )
        }

  CURATED_ARTIST = "Curated Artist"
  REFUNDS = "Refunds"
  ISSUES_REFUNDS_ONLY = "Issues Refunds Only"
end
