class LanguageCode < ApplicationRecord
  include Translatable

  has_many :people

  attr_translate :description

  ENGLISH = 1
  GERMAN  = 3
  FRANCE  = 4
  ITALY   = 5
  FEATURE_FLAG_LANGUAGES = [
    # add lowercase string of language here if needed
  ].freeze

  def self.available_album_languages
    @@available_album_languages ||= LanguageCode.all
  end
end
