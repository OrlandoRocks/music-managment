# == Schema Information
# Schema version: 404
#
# Table name: price_policies
#
#  id               :integer(11)     not null, primary key
#  short_code       :string(20)      default(""), not null
#  price_calculator :string(50)      default(""), not null
#  base_price_cents :integer(11)     default(0), not null
#  description      :string(100)     default(""), not null
#

# PricePolicy is a record in the database that represents:
# a) a base price for 'something'
# b) a reference to a strategy class (in PriceCalcultor:: namespace) for computing total prices
#
# Note that the PricePolicy isn't directly attached to a *Purchase for the item in question because
# TuneCore's stance is that price policies are in effect when the item is created... not when it's
# purchased.
#
# Take albums as an example.  Any time you change the base price for an album you need to create
# a new price_policy for that price, leaving the old one in place for any existing albums.
# See the 'album_price_increase2.rb' migration to see how that's done.
#
# Also, if you decide that from now on, you're going to offer one free store, that's also new default
# price_policy for albums even if you're not changing the 'base' price.
#
# The reason is that you have to use a different pricing strategy class (one that gives away a free store)
# while still preserving the previous pricing strategy reference for existing albums.
#
# Note: I probably shouldn't have called the price_calculator for albums 'current_album' - because maybe
# it won't be all that 'current' in the future.  If you have to create a new album pricing strategy please
# take the time to correct that misnomer :-)

class PricePolicy < ApplicationRecord
  extend MoneyField
  money_reader :base_price

  class << self
    def [](arg)
      case arg
      when String, Symbol
        all_by_short_code[arg.to_s] or raise ArgumentError, "Unable to locate PricePolicy using #{arg.inspect}"
      when Integer
        all_by_id[arg] or raise ArgumentError, "Unable to locate PricePolicy using #{arg.inspect}"
      when ActiveRecord::Base
        unless arg.respond_to? :price_policy_id
          raise ArgumentError,
                "Unable to locate PricePolicy using instance of #{arg.class.name}, it has no price_policy_id"
        end

        all_by_id[arg.price_policy_id]
      else
        raise ArgumentError, "Unable to locate PricePolicy using instance of #{arg.class.name}"
      end
    end

    # NOTE - ONLY TO BE USED IN MIGRATIONS!
    def refreshed
      @all = @all_by_short_code = @all_by_id = nil
      self
    end

    protected

    def all_by_short_code
      @all_by_short_code ||= all.index_by(&:short_code)
    end

    def all_by_id
      @all_by_id ||= all.index_by(&:id)
    end
  end

  validates :short_code, uniqueness: true

  def calculator_class
    Tunecore::PriceCalculator.class_of(price_calculator)
  end
end
