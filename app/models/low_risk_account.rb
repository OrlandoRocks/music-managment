class LowRiskAccount < ApplicationRecord
  belongs_to :person
  has_many :albums, foreign_key: :person_id, primary_key: :person_id
end
