class SongLibraryUpload < ApplicationRecord
  class SongLibraryAssetUploader < Shrine
    def generate_location(io, record: nil, derivative: nil, **)
      return super unless record&.person&.id

      "#{record.person.id}/#{super}"
    end
  end

  # NOTE: song_library_asset is tied to the column song_library_asset_data in
  # the database schema. This cannot change without that column changing as
  # well.
  include SongLibraryAssetUploader::Attachment[:song_library_asset]

  include MagicNumberable

  validates :person, :name, :artist_name, :genre, presence: true

  validate :file_is_a_true_mp3

  has_many :soundout_reports, as: :track

  belongs_to :person
  belongs_to :genre

  after_commit :set_duration, if: proc { saved_change_to_song_library_asset_data? }

  def set_duration
    begin
      song_library_asset.download do |file|
        Mp3Info.new(file).length.tap do |duration|
          update_columns(duration: duration)
        end
      end
    rescue => e
      Airbrake.notify("SongLibraryUpload#set_duration failed", e)
    end
  end

  def streaming_url(expires_in = 3600)
    song_library_asset&.url(expires_in: expires_in)
  end

  def soundout_url(expires_in = 259_200)
    streaming_url(expires_in)
  end

  def longer_than_90s
    duration ? duration >= 90 : nil
  end

  private

  def file_is_a_true_mp3
    return unless song_library_asset && !valid_and_true_mp3?(song_library_asset.download.path)

    errors.add(:song_library_asset, "invalid mp3 file")
  end
end
