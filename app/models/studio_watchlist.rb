class StudioWatchlist < ApplicationRecord
  scope :active, -> { where(is_active: true) }

  def self.watchlisted?(string)
    watchlisted_trie.has_key?(string)
  end

  def self.watchlisted_trie
    @watchlisted_trie ||= create_watchlisted_trie
  end

  def self.create_watchlisted_trie
    trie = Triez.new
    active.pluck(:text_to_compare).compact.each do |text_to_compare|
      trie << text_to_compare
    end
    trie
  end
  private_class_method :create_watchlisted_trie
end
