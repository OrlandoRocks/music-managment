# == Schema Information
# Schema version: 404
#
# Table name: rights
#
#  id         :integer(11)     not null, primary key
#  controller :string(255)
#  action     :string(255)
#  name       :string(255)
#

class Right < ApplicationRecord
  has_and_belongs_to_many :roles
end
