class TerminatedComposer < ApplicationRecord
  belongs_to :composer
  validates :termination_type, presence: true
  validate :presence_of_composer
  validates :termination_type, inclusion: { in: %w[fully partially] }

  belongs_to :publishing_composer
  belongs_to :publishing_termination_reason

  scope :fully_terminated,      -> { where(termination_type: "fully") }
  scope :partially_terminated,  -> { where(termination_type: "partially") }

  before_destroy :reactivate_composer_pub_admin_status
  after_save :create_alt_provider_acct_id, if: :partially_terminated?
  after_save :reactivate_composer_pub_admin_status, if: :partially_terminated?
  after_save :update_composer_pub_admin_status, if: :fully_terminated?

  def account
    (publishing_composer || composer).account
  end

  def fully_terminated?
    termination_type == "fully"
  end

  def partially_terminated?
    termination_type == "partially"
  end

  def terminated_compositions?
    compositions = PublishingAdministration::CompositionsBuilder.build({ composer_id: composer.id })
    compositions.any? { |c| c.composition_state == "terminated" }
  end

  def terminated_publishing_compositions?
    publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({ publishing_composer_id: publishing_composer.id })
    publishing_compositions.any? { |c| c.publishing_composition_state == "terminated" }
  end

  private

  def presence_of_composer
    return if composer_id.present? || publishing_composer_id.present?

    errors.add(:base, "A composer or publishing_composer must be present")
  end

  def create_alt_provider_acct_id
    PublishingAdministration::TerminatedAccountWorker.perform_async(id) if alt_provider_acct_id.nil?
  end

  def reactivate_composer_pub_admin_status
    if publishing_composer
      publishing_composer.reactivate_pub_admin
    else
      composer.reactivate_pub_admin
    end
  end

  def update_composer_pub_admin_status
    if publishing_composer
      publishing_composer.terminate_pub_admin
    else
      composer.terminate_pub_admin
    end
  end
end
