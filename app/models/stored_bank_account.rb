class StoredBankAccount < ApplicationRecord
  include ActiveMerchant::Billing
  include Logging
  include Braintree::BankAccount

  ### ASSOCIATIONS ###
  belongs_to  :person
  has_many    :eft_batch_transactions
  has_many    :braintree_vault_transactions, as: :related

  ### STATIC VARIABLES ###
  STATUSES = ["valid", "processing_error", "retrieval_error", "destroyed", "destruction_error"]
  SBA = "Stored Bank Account"

  ### ACCESSOR ATTRIBUTES ###
  attr_readonly :customer_vault_id, :person_id
  attr_accessor :meta_data, :last_four_account_confirmation, :ip_address, :bt_token

  ### VALIDATIONS AND CALLBACKS ###

  # ------------- #
  validates :customer_vault_id, :person_id, presence: true
  # ------------- #
  after_create :mark_previous_account_as_deleted

  ### NAMED SCOPES ###
  scope :active, -> { where(deleted_at: nil).order("created_at DESC").limit(1) }
  scope :archived,
        ->(is_archived) {
          where(
            if is_archived == "Only Deleted"
              "stored_bank_accounts.deleted_at IS NOT NULL"
            else
              "stored_bank_accounts.deleted_at IS NULL"
            end
          )
        }

  ### CONDITIONALS ###

  ### CLASS METHODS ###
  def self.current
    active.first
  end

  def self.search(params)
    search = all

    if params
      search = search.joins(:person).where(
        "people.status = ?",
        params[:person_status]
      ) if params[:person_status].present?
      search = search.joins(:person).where(
        "people.email like ?",
        "%#{params[:person_email]}%"
      ) if params[:person_email].present?
      search = search.where("stored_bank_accounts.person_id = ?", params[:person_id]) if params[:person_id].present?
      search = search.joins(:person).where(
        "people.name like ?",
        "%#{params[:person_name]}%"
      ) if params[:person_name].present?

      search = search.archived(params[:archived]) if params[:archived].present?
      search = search.where("stored_bank_accounts.id = ?", params[:id]) if params[:id].present?

      search = search.where(
        "stored_bank_accounts.created_at >= ?",
        DateTime.strptime(params[:created_at_min], "%m/%d/%Y")
      ) if params[:created_at_min].present?
      search = search.where(
        "stored_bank_accounts.created_at <= ?",
        DateTime.strptime(params[:created_at_max], "%m/%d/%Y")
      ) if params[:created_at_max].present?
    end

    Tunecore::Search.new(params, search)
  end

  ### OBJECT METHODS ###

  def last_four_account_padded
    "xxxxx#{last_four_account}"
  end

  def last_four_routing_padded
    "xxxxx#{last_four_routing}"
  end

  def destroy(ip_address)
    update(deleted_at: Time.now)
    delete_braintree_record(ip_address)
  end

  def pending_transaction_total
    eft_batch_transactions.find_all { |ebt|
      ebt.status == "pending_approval" || ebt.status == "waiting_for_batch"
    }.sum(&:amount).to_f
  end

  def processing_transaction_total
    eft_batch_transactions.find_all { |ebt|
      ebt.status == "processing_in_batch" || ebt.status == "sent_to_bank"
    }.sum(&:amount).to_f
  end

  def failed_transaction_total
    eft_batch_transactions.find_all { |ebt|
      ebt.status == "canceled" || ebt.status == "rejected" || ebt.status == "error" || ebt.status == "failure"
    }.sum(&:amount).to_f
  end

  def successful_transaction_total
    eft_batch_transactions.find_all { |ebt| ebt.status == "success" }.sum(&:amount).to_f
  end

  def last_successful_eft_withdrawal
    eft_batch_transactions.by_status("success").max
  end

  private

  ### VALIDATION METHODS ###

  ### CALLBACK METHODS ###

  def mark_previous_account_as_deleted
    old_account = StoredBankAccount.find_by("person_id = ? AND id != ? AND deleted_at IS NULL", person_id, id)
    old_account.destroy(ip_address) unless old_account.nil?
  end

  ### OTHER PRIVATE METHODS ###
end
