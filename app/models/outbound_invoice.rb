class OutboundInvoice < ApplicationRecord
  include StripEmojiMethods
  ROYALTY_INVOICE = "Royalty Invoice".freeze

  belongs_to :person
  belongs_to :related, polymorphic: true
  belongs_to :vat_tax_adjustment

  has_one :invoice_static_customer_address, as: :related, dependent: :destroy
  has_one :invoice_static_corporate_entity, as: :related, dependent: :destroy
  has_one :invoice_static_customer_info, as: :related, dependent: :destroy
  has_one :person_transaction, as: :target, dependent: :destroy

  has_many :outbound_refunds, dependent: :destroy
  has_many :refund_vat_tax_adjustments, through: :outbound_refunds, source: :vat_tax_adjustment

  validates :person_id,
            :related_id,
            :related_type,
            :user_invoice_prefix,
            :invoice_date,
            presence: true

  validates :invoice_sequence, uniqueness: { scope: :user_invoice_prefix, allow_nil: true }

  delegate :customer_type, to: :vat_tax_adjustment

  after_create :create_royalty_invoice_transaction
  after_save :create_static_content
  after_commit :generate_invoice_number

  def prefixed_invoice_number
    return if invoice_sequence.blank?

    format(%(#{user_invoice_prefix}%05d), invoice_sequence)
  end

  def create_static_content
    transaction do
      build_invoice_static_customer_address(customer_address_attributes).save!
      build_invoice_static_corporate_entity(person.corporate_entity.attributes.except("id")).save!
      build_invoice_static_customer_info(customer_info_attributes).save!
    end
  end

  def generate_invoice_number
    return if invoice_sequence.present?

    GenerateOutboundInvoiceSequenceWorker.perform_async(id)
  end

  def vat_amount_remaining
    total_vat_amount - debited_vat_amount
  end

  def total_vat_amount
    return 0 if vat_tax_adjustment.blank?

    Integer(vat_tax_adjustment.amount)
  end

  def debited_vat_amount
    return 0 if refund_vat_tax_adjustments.blank?

    Integer(refund_vat_tax_adjustments.sum(:amount))
  end

  private

  def customer_address_attributes
    emoji_stripped_address_attributes
      .merge(country: person.country_name_untranslated,
             zip: person.foreign_postal_code)
  end

  def customer_info_attributes
    {
      name: strip_emoji(person.compliant_contact_full_name),
      person_currency: person.currency,
      customer_type: fetch_customer_type,
      vat_registration_number: person.vat_information&.vat_registration_number
    }.merge(
      customer_company_info_attributes
    ).tap do |h|
      h[:name] = person.compliant_contact_company_name if person.business?
    end
  end

  def customer_company_info_attributes
    return {} unless person&.business? && person&.company_information.present?

    person
      .company_information
      .slice(*CompanyInformation::FIELDS)
      .symbolize_keys
  end

  def create_royalty_invoice_transaction
    create_person_transaction(
      person_id: person_id,
      debit: 0,
      credit: 0,
      comment: ROYALTY_INVOICE,
      currency: currency
    )
  end

  def fetch_customer_type
    return person.vat_information.customer_type if person.vat_information&.status_valid?

    person.customer_type
  end
end
