class SubscriptionPurchase < ApplicationRecord
  belongs_to :subscription_product
  has_one :subscription_event
  has_one :payment_channel_receipt
  belongs_to :person
  has_one :purchase, as: :related

  validates :person, presence: true
  validates :subscription_product, presence: true

  before_destroy :cleanup_purchase

  scope :by_product_name,
        ->(product_name) {
          joins(:subscription_product).where(
            "subscription_products.product_name = ?", product_name
          )
        }

  def cleanup_purchase
    purchase.delete if purchase && purchase.paid_at.nil?
  end

  def expires?
    subscription_product.term_length.positive?
  end

  def find_related_person_subscription_status(subscription_type)
    person.subscription_status_for(subscription_type)
  end

  def name
    subscription_product.product_name
  end

  def next_termination_date(date_time)
    date_time + subscription_product.term_length.month if expires?
  end

  def paid_post_proc
    SubscriptionEvent.create_purchase_event(self)
  end

  def get_termination_date
    expires? ? self[:termination_date].to_date : Date.today + 1.year
  end
end
