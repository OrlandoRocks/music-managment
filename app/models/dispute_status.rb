# frozen_string_literal: true

class DisputeStatus < ApplicationRecord
  BRAINTREE = "braintree"
  PAYPAL = "paypal"
  PAYU = "payu"

  SOURCE_TYPE_NAMES = { PAYPAL => "PayPal", BRAINTREE => "Braintree", PAYU => "PayU" }

  REFUNDABLE_DISPUTE_STATUSES = %w[
    accepted
    resolved
    refunded
    lost
    expired
  ].freeze

  enum source_type: {
    braintree: BRAINTREE,
    paypal: PAYPAL,
    payu: PAYU
  }

  def refundable?
    REFUNDABLE_DISPUTE_STATUSES.include? status.downcase
  end
end
