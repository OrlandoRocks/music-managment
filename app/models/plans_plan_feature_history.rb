# frozen_string_literal: true

class PlansPlanFeatureHistory < ApplicationRecord
  belongs_to :plan
  belongs_to :plan_feature

  validates :plan_feature, :plan, :change_type, presence: true

  enum change_type: { add: "add", remove: "remove" }

  def self.create_history!(plans_plan_feature, type)
    PlansPlanFeatureHistory.create!(
      plan: plans_plan_feature.plan,
      plan_feature: plans_plan_feature.plan_feature,
      change_type: type
    )
  end
end
