class SalesReport
  attr_accessor :person, :month, :year

  CSV_OPTIONS = {
    col_sep: ",",
    row_sep: "\r\n",
    force_quotes: true
  }

  def initialize(person, options = {})
    @person = person
    @options = options

    # Setup Report Classes
    @store_sales_report = StoreSalesReport.new
    @song_sales_report = SongSalesReport.new
    @album_sales_report = AlbumSalesReport.new
    @month_sales_report = MonthSalesReport.new(person, options)

    @net_earnings = 0 # cents
    @albums_sold = 0
    @songs_sold = 0
    @songs_streamed = 0

    prepare_options
    filter
    process_totals
  end

  def sales_records
    @cached_sales_records ||= @sales_records.includes(:store_intake)
  end

  def month_name
    Date::MONTHNAMES[month] if month
  end

  #
  # The available years for query
  # hint: Records begin in 2006
  #
  def self.all_years(customer_id)
    ReportingMonth
      .select("report_year")
      .joins("INNER JOIN sales_records ON sales_records.reporting_month_id = reporting_months.id")
      .where("sales_records.person_id = :person_id", { person_id: customer_id })
      .group("report_year")
      .map { |result| result["report_year"] }
  end

  #
  #  Produces a list of sales reports for a given year
  #  grouped by month
  #
  def self.find(person, opts)
    if opts[:month].blank?
      group_by_month(person, opts)
    else
      [new(person, opts)]
    end
  end

  def self.group_by_month(person, options)
    new_opts = options.clone
    months.map do |month|
      new_opts[:month] = month
      new(person, new_opts)
    end
  end

  def self.months
    (1..12).to_a
  end

  def months
    if @month
      [@month]
    else
      SalesReport.months
    end
  end

  #
  #  Overall Numbers
  #
  attr_reader :albums_sold

  attr_reader :songs_sold

  attr_reader :songs_streamed

  # note: uses PersonIntake model because it contains the
  #       number used when paying the customer
  def net_earnings
    @total_cents_net_earnings ||= @person_intakes.sum(:usd_payout_cents)
    Money.new(@total_cents_net_earnings)
  end

  def albums
    person.albums.includes(:songs).where("id IN (:album_ids)", { album_ids: album_ids })
  end

  def stores
    Store.where("id IN (:store_ids)", { store_ids: store_ids })
  end

  def has_total_fees?
    total_fees != Money.new(0)
  end

  def total_fees
    fees = (net_earnings_by_sales_records - net_earnings)
    total_cents = fees.cents * -1
    Money.new(total_cents)
  end

  def net_earnings_by_sales_records
    Money.new(@net_earnings)
  end

  #
  #  Specific song numbers
  #
  def song_albums_sold(song_id)
    @song_sales_report.albums_sold(song_id)
  end

  def song_songs_sold(song_id)
    @song_sales_report.songs_sold(song_id)
  end

  def song_songs_streamed(song_id)
    @song_sales_report.songs_streamed(song_id)
  end

  def song_net_earnings(song_id)
    @song_sales_report.net_earnings(song_id)
  end

  def song_sum_of_net_earnings
    result = Money.new(0)
    song_ids.each do |song_id|
      result += song_net_earnings(song_id)
    end
    result
  end

  #
  #  Specific Store Numbers
  #
  def store_albums_sold(store_id)
    @store_sales_report.albums_sold(store_id)
  end

  def store_songs_sold(store_id)
    @store_sales_report.songs_sold(store_id)
  end

  def store_songs_streamed(store_id)
    @store_sales_report.songs_streamed(store_id)
  end

  def store_net_sales(store_id)
    @store_sales_report.net_sales(store_id)
  end

  def store_currency(store_id)
    @store_sales_report.currency(store_id)
  end

  def store_net_earnings(store_id)
    @store_sales_report.net_earnings(store_id)
  end

  #
  #  Specific album numbers
  #
  def album_albums_sold(album_id)
    @album_sales_report.albums_sold(album_id)
  end

  def album_songs_sold(album_id)
    @album_sales_report.songs_sold(album_id)
  end

  def album_songs_streamed(album_id)
    @album_sales_report.songs_streamed(album_id)
  end

  # total sales for the album as a unit
  def album_net_sales(album_id)
    @album_sales_report.net_sales(album_id)
  end

  # total sales for the album sales, song sales, and song streams
  def album_net_earnings(album_id)
    @album_sales_report.net_earnings(album_id)
  end

  #
  #  Month Numbers
  #
  def month_albums_sold(month_num)
    @month_sales_report.albums_sold(month_num)
  end

  def month_songs_sold(month_num)
    @month_sales_report.songs_sold(month_num)
  end

  def month_songs_streamed(month_num)
    @month_sales_report.songs_streamed(month_num)
  end

  def month_net_earnings(month_num)
    # @month_sales_report.net_earnings(month_num)
    Money.new(PersonIntake.year(@year).month(month_num).person(@person.id).sum(:usd_payout_cents))
  end

  def sub_total_of_albums
    total = Money.new(0)
    album_ids.each do |album_id|
      total += album_net_earnings(album_id)
    end
    total
  end

  protected

  #
  #  Extract instance variables out of options
  #
  def prepare_options
    @year = @options[:year]
    @month = @options[:month]
    @album_id = @options[:album_id]
  end

  #
  #  Apply named scoping filter to sales records
  #
  def filter
    @person_intakes = person.person_intakes.year(@year).month(@month)
    @sales_records = person.sales_records.year(@year).month(@month).album(@album_id)
  end

  def store_ids
    sales_records.select { |record| record.store_id.present? }.map(&:store_id).uniq
  end

  def album_ids
    sales_records.map(&:album_id).uniq
  end

  def song_ids
    sales_records.select { |record| record.song_id.present? }.map(&:song_id).uniq
  end

  def process_totals
    sales_records.each do |record|
      @song_sales_report.add_record(record)
      @album_sales_report.add_record(record)
      @store_sales_report.add_record(record)
      @month_sales_report.add_record(record)

      @net_earnings += record.usd_total_cents
      @songs_sold += record.units_sold if record.song_id.present? && record.sale_type == "dl"
      @songs_streamed += record.units_sold if record.song_id.present? && record.sale_type == "ss"
      @albums_sold += record.units_sold if record.song_id.blank? && record.album_id.present?
    end
  end
end
