# frozen_string_literal: true

class TaxFormType < ApplicationRecord
  W9_INDIVIDUAL = "W9 - Individual"
  W9_ENTITY = "W9 - Entity"
  W9_TYPES = [W9_INDIVIDUAL, W9_ENTITY]

  W8_TYPES = ["W8BEN", "W8BEN-E", "W8ECI - Individual", "W8ECI - Entity", "W8233"]

  has_many :tax_forms, dependent: :destroy

  scope :w9_types, -> { where(kind: W9_TYPES) }
  scope :w8_types, -> { where(kind: W8_TYPES) }
  scope :us_types, -> { where(kind: W8_TYPES + W9_TYPES) }

  # Marks this model as readonly
  def readonly?
    true
  end

  def w9?
    W9_TYPES.include?(kind)
  end

  def w8?
    W8_TYPES.include?(kind)
  end

  def us?
    (W8_TYPES + W9_TYPES).include?(kind)
  end
end
