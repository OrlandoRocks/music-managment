class BalanceAdjustmentCategory < ApplicationRecord
  US_TAXABLE_CATEGORIES = ["Songwriter Royalty", "Tidal DAP", "Facebook"]

  validates :name, uniqueness: true

  scope :us_taxable, -> { where(name: US_TAXABLE_CATEGORIES) }
end
