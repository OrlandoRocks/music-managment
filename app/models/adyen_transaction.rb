# frozen_string_literal: true

class AdyenTransaction < ApplicationRecord
  belongs_to :person
  belongs_to :invoice
  belongs_to :country
  belongs_to :adyen_stored_payment_method
  belongs_to :foreign_exchange_rate
  belongs_to :adyen_payment_method_info
  belongs_to :payment_method_info, class_name: "AdyenPaymentMethodInfo"
  belongs_to :adyen_merchant_config

  delegate :payment_method_name,
           :payment_method_display_name,
           to: :adyen_payment_method_info, allow_nil: true

  delegate :adyen_customer_reference, to: :person

  AUTHORISED = "Authorised"
  ADYEN_NAME = "Adyen"
  RECEIVED = "received"
  REFUSED = "Refused"
  ADYEN_TRUE = "true"
  SUCCESS = "success"
  FAILURE = "failure"
  AUTHORISATION = "AUTHORISATION"
  RECURRING_CONTRACT = "RECURRING_CONTRACT"
  REFUND = "REFUND"

  after_commit :settle_invoice!, if: :marked_successful?
  after_commit :fetch_recurring_information, if: :fetch_recurring_information?

  def marked_successful?
    result_code_changed_to_authorised? && !refund_transaction?
  end

  def result_code_changed_to_authorised?
    saved_change_to_result_code? && result_code == AUTHORISED
  end

  def settle_invoice!
    invoice.settlement_received(self, invoice.outstanding_amount_cents)
    invoice.settled! if invoice.can_settle?
    update(status: "success")
    broadcast_authorization
  end

  def refund_transaction?
    related_transaction_id.present?
  end

  def self.create_settlement(invoice)
    country = Country.find_by(name: invoice.person.country_name_untranslated)
    create!(
      invoice: invoice,
      country: country,
      person: invoice.person,
      amount: invoice.outstanding_amount_cents,
      amount_in_local_currency: 0,
      currency: invoice.currency,
    )
  end

  def self.process_auto_refund!(_refund, amount, invoice_settlement)
    refund_service = Adyen::RefundTransactionService.new(invoice_settlement.source, amount)
    refund_service.call
  end

  delegate :process_renewal, to: :renewal_service

  def broadcast_authorization
    ActionCable.server.broadcast(
      adyen_channel,
      {
        invoice_id: invoice_id,
        session_id: session_id,
        result_code: result_code,
        redirect_url: invoice_success_redirect_url
      }
    )
  end

  def broadcast_unauthorization
    ActionCable.server.broadcast(
      adyen_channel,
      {
        invoice_id: invoice_id,
        session_id: session_id,
        result_code: "unauthorised",
        redirect_url: Rails.application.routes.url_helpers.cart_path(invoice_id: invoice_id, failed: "true")
      }
    )
  end

  def adyen_channel
    CableAuthService.refresh_token(person) if person.cable_auth_token.nil?
    "adyen-channel-#{person.cable_auth_token.token}"
  end

  def invoice_success_redirect_url
    Rails.application.routes.url_helpers.adyen_finalize_after_redirect_cart_path(invoice_id: invoice_id)
  end

  private

  def fetch_recurring_information?
    marked_successful? && psp_reference.present? && !renewal_transaction?
  end

  def renewal_transaction?
    ip_address == "batch"
  end

  def renewal_service
    Adyen::AdyenRenewalService.new(self, invoice)
  end

  def fetch_recurring_information
    Adyen::RecurringDetailsUpdateWorker.perform_at(1.minute.from_now, id)
  end
end
