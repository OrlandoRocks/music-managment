class OauthToken < ApplicationRecord
  belongs_to :client_application
  belongs_to :user, class_name: "Person"
  validates :token, uniqueness: true
  validates :client_application, :token, :secret, presence: true

  before_validation :generate_keys, on: :create

  attr_accessor :scope

  def invalidated?
    invalidated_at != nil
  end

  def invalidate!
    update_attribute(:invalidated_at, Time.now)
  end

  def authorized?
    authorized_at != nil && !invalidated?
  end

  def to_query
    "oauth_token=#{token}&oauth_token_secret=#{secret}"
  end

  protected

  def generate_keys
    oauth_token = client_application.oauth_server.generate_credentials
    self.token = oauth_token[0][0, 20]
    self.secret = oauth_token[1][0, 40]
  end
end
