class TypefaceGenre < ApplicationRecord
  belongs_to :typeface
  belongs_to :genre

  validates :typeface_id, :genre_id, presence: true
end
