class MyArtistMapping
  def initialize(external_service_id, redistribution_package)
    @external_service_id = external_service_id
    @redistribution_package = redistribution_package
  end

  delegate :identifier, to: :external_service_id

  def state
    state_map = {
      nil => "manage",
      "processing" => "processing",
      "requested" => "processing",
      "matched" => { "pending" => "processing" },
      "did_not_match" => "error",
      "error" => "error"
    }

    current_state = state_map[external_service_id.state]
    case current_state
    when Hash
      current_state[redistribution_package.try(:state)]
    when String
      current_state
    else nil
    end || "manage"
  end

  delegate :service_name, to: :external_service_id

  def as_json(*_)
    json = {
      identifier: identifier,
      state: state,
      service_name: service_name
    }
    json[:processed_at] = I18n.l(external_service_id.updated_at, format: :country_date) if state == "processing"
    json[:error] = error if state == "error"
    json
  end

  def error
    external_service_id.state
  end

  private

  attr_reader :external_service_id, :redistribution_package
end
