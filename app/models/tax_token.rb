class TaxToken < ApplicationRecord
  belongs_to :person
  belongs_to :tax_tokens_cohort
  validates :token, :person_id, presence: true

  scope :visible, -> { where(is_visible: true) }
  scope :incomplete, -> { where(tax_form_type: nil) }

  TAX_FORM_TYPES = {
    "1" => "W8BEN",
    "2" => "W9 - Individual",
    "3" => "W9 - Entity",
    "4" => "W8BEN-E",
    "5" => "W8ECI - Individual",
    "6" => "W8ECI - Entity",
    "7" => "W8233"
  }.freeze

  def self.for(person)
    where(person_id: person.id, is_visible: true).last
  end

  def tax_form
    TAX_FORM_TYPES[tax_form_type]
  end

  def form_submitted?
    tax_form_type.present?
  end

  def cohort_end_date
    tax_tokens_cohort.try(:end_date)
  end
end
