class SalesRecordSummary < ApplicationRecord
  belongs_to :sales_record_master
  belongs_to :person
  belongs_to :person_intake
  belongs_to :related, polymorphic: true
end
