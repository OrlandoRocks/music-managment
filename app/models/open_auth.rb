# = Description
# This model is used to store user's OAuth access token and secret. The table name is 'oauth_providers_people'
# and OpenAuth class name is used for simplicity.
#
# Currently there's only 1 OAuth provider- Souncloud with a provider_id of 1. See oauth_providers table
#
# A user can have many oauth_proivder_people records if they had granted us access to their Soundcloud account,
# but later revoked it, then re-grant us access. However, they can only have 1 active Soundcloud access token at a time,
# so please disable any previous active token before creating a new record.
#
# = Change Log
# 2010-07-13 -- ED -- created

class OpenAuth < ApplicationRecord
  self.table_name = "oauth_providers_people"

  belongs_to  :person
  belongs_to  :oauth_provider

  validates   :person, :oauth_provider, :access_token, :access_token_secret, presence: true
  validates :oauth_provider_id,
            uniqueness: {
              scope: [:person_id, :is_active],
              if: :is_active?,
              message: "An active token already exists"
            }

  def is_active?
    (is_active == "Y") ? true : false
  end

  def disable!
    self.is_active = "N"
    save
  end
end
