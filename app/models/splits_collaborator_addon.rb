# frozen_string_literal: true

class SplitsCollaboratorAddon < PlanAddon
  default_scope { splits_collaborator }

  scope :splits_collaborator, -> { where(addon_type: PlanAddon::SPLITS_COLLABORATOR) }

  validate :active_collaborator_exists, if: :active_collaborator?

  def active_collaborator_exists
    errors.add(:active_collaborator_exists, "This person already has an active collaborator")
  end

  def active_collaborator?
    SplitsCollaboratorAddon.active.exists?(person: person)
  end
end
