class DistributionSong < ApplicationRecord
  include Distribution::Distributable

  validates :distribution, :salepoint_song, presence: true
  validates :salepoint_song_id, uniqueness: true

  belongs_to :distribution
  belongs_to :salepoint_song
  has_one :salepoint, through: :salepoint_song
  has_one :store, through: :salepoint

  after_create :block_distribution_song, if: :ytm_blocked_song

  def distribution_type
    "Track"
  end

  delegate :converter_class, to: :distribution

  def converter_class=(name)
    distribution.update_attribute(:converter_class, name) if distribution.converter_class.blank?
  end

  delegate :petri_bundle, to: :distribution

  delegate :album, to: :petri_bundle

  def store
    super || salepoint_song.salepoint.try(:store)
  end

  def taken_down?
    salepoint_song.takedown_at?
  end

  def approved_for_distribution?
    if petri_bundle && petri_bundle.album && !petri_bundle.album.approved_for_distribution?
      false
    else
      true
    end
  end

  def job_id=(job_id)
    self.sqs_message_id = job_id
  end

  def job_id
    self[:sqs_message_id]
  end

  def self.active_distributions_by_state(state, date = nil)
    last_updated_date = date || Date.today.strftime
    DistributionSong.select(ds_table[Arel.star])
                    .joins(salepoint_song: { song: { album: :petri_bundles } })
                    .where(ds_table[:state].eq(state))
                    .where(ds_table[:updated_at].lt(last_updated_date))
                    .where(active_ytsr_release)
  end

  def ytm_blocked_song
    YtmBlockedSong.find_by(song_id: salepoint_song&.song_id)
  end

  def block_distribution_song
    block({ message: "blocking_distribution" })
  end

  class << self
    private

    def ds_table
      DistributionSong.arel_table
    end

    def sp_song_table
      SalepointSong.arel_table
    end

    def song_table
      Song.arel_table
    end

    def petri_bundle_table
      PetriBundle.arel_table
    end

    def album_table
      Album.arel_table
    end

    def active_ytsr_release
      album_table[:legal_review_state].in(["APPROVED", "DO NOT REVIEW"])
                                      .and(album_table[:style_review_state].in(["APPROVED", "DO NOT REVIEW"]))
                                      .and(sp_song_table[:state].eq("approved"))
                                      .and(petri_bundle_table[:state].not_eq("dismissed"))
    end
  end
end
