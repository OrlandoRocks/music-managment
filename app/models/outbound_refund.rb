# frozen_string_literal: true

class OutboundRefund < ApplicationRecord
  extend MoneyField

  CR_ROYALTY_INVOICE = "Credit Note Invoice for Royalty Invoice"

  belongs_to :refund
  belongs_to :outbound_invoice
  belongs_to :vat_tax_adjustment
  belongs_to :refund_settlement

  has_one :person_transaction, as: :target, dependent: :destroy

  validates :refund,
            :outbound_invoice,
            :refund_settlement,
            presence: true

  validates :invoice_sequence,
            uniqueness: {
              scope: :user_invoice_prefix,
              allow_nil: true
            }

  after_create :create_person_transaction_entry
  after_commit :generate_invoice_number

  delegate :person,
           :invoice_static_corporate_entity,
           to: :outbound_invoice

  money_reader :base_amount, :tax_amount, :total_amount

  def credit_note_invoice_number
    return if invoice_sequence.blank?

    format(%(#{user_invoice_prefix}%05d), invoice_sequence)
  end

  def total_amount_cents
    refund_settlement.settlement_amount_cents
  end

  def tax_amount_cents
    return 0 if vat_tax_adjustment.blank?

    Integer(vat_tax_adjustment.amount)
  end

  def base_amount_cents
    total_amount_cents - tax_amount_cents
  end

  def vat_cents_in_euro
    return 0 if vat_tax_adjustment.nil?

    Integer(vat_tax_adjustment.vat_amount_in_eur)
  end

  def vat_amount_in_euro
    Money.new(vat_cents_in_euro, CurrencyCodeType::EUR)
  end

  def tax_rate
    return 0 if vat_tax_adjustment.blank?

    vat_tax_adjustment.tax_rate
  end

  def tax_type
    return Purchase::VAT if vat_tax_adjustment.blank?

    vat_tax_adjustment.tax_type
  end

  private

  def generate_invoice_number
    return if invoice_sequence.present?

    GenerateOutboundInvoiceSequenceWorker.perform_async(id, self.class.name)
  end

  def create_person_transaction_entry
    create_person_transaction(
      person_id: refund.person.id,
      debit: 0,
      credit: 0,
      comment: CR_ROYALTY_INVOICE,
      currency: currency
    )
  end
end
