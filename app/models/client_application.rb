require "oauth"

class ClientApplication < ApplicationRecord
  belongs_to :user, class_name: "Person" # User who created this application
  has_many :tokens, class_name: "OauthToken"
  has_and_belongs_to_many :roles

  validates :name, :url, :key, :secret, presence: true
  validates :key, uniqueness: true

  before_validation :generate_keys, on: :create

  validates :url, format: { with: /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@\-\/]))?/i }
  validates :support_url,
            format: {
              with: /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@\-\/]))?/i,
              allow_blank: true
            }
  validates :callback_url,
            format: {
              with: /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@\-\/]))?/i,
              allow_blank: true
            }

  attr_accessor :token_callback_url

  scope :tc_social, -> { find_by(name: "tc_social") }

  SOCIAL_ALLOWED_REGIONS = ["US", "UK", "AU", "CA"].freeze

  # Overriding url based on the environment to prevent CRT app from breaking after a new db dump
  def url
    if name == "content_review"
      CRT_URL
    else
      self[:url]
    end
  end

  # Overriding callback_url based on the environment to prevent CRT app from breaking after a new db dump
  def callback_url
    if name == "content_review"
      "#{CRT_URL}/login/authenticated"
    else
      self[:callback_url]
    end
  end

  def self.find_token(token_key)
    token = OauthToken.includes(:client_application).find_by(token: token_key)
    if token && token.authorized?
      token
    else
      nil
    end
  end

  def self.verify_request(request, options = {}, &block)
    begin
      signature = OAuth::Signature.build(request, options, &block)
      return false unless OauthNonce.remember(signature.request.nonce, signature.request.timestamp)

      signature.verify
    rescue OAuth::Signature::UnknownSignatureMethod => e
      logger.info "ERROR" + e.to_s
      false
    end
  end

  def oauth_server
    @oauth_server ||= OAuth::Server.new("http://your.site")
  end

  def credentials
    @oauth_client ||= OAuth::Consumer.new(key, secret)
  end

  def create_request_token(_options = {})
    RequestToken.create client_application: self, callback_url: token_callback_url
  end

  def authorized_by_role?
    authorized_by_role
  end

  def skip_auth_form?
    ["tc_social"].include?(name)
  end

  protected

  def generate_keys
    oauth_client = oauth_server.generate_consumer_credentials
    self.key = oauth_client.key[0, 20]
    self.secret = oauth_client.secret[0, 40]
  end
end
