class PublishingChangeoverReport
  DEFAULT_MAX_COMPOSERS = 2
  DEFAULT_MAX_CLIENTS = 2
  DEFAULT_MAX_PUBLISHERS = 3
  TC_BMI_CAE = "642005099"
  TC_ASCAP_CAE = "641638348"
  TC_SESAC_CAE = "641080872"

  CATALOG_RECIPIENTS = ["songwriters@tunecore.com", "sentric_catalog_deliveries@tunecore.com"]

  TEMP_DIR = "#{Rails.root}/tmp/publishing_reports"
  TEMP_DIR_CLIENT_IMPORT = "#{Rails.root}/tmp/client_imports"

  def self.catalogs_by_date_range(start_date, end_date = nil)
    report_filenames = []
    summary_report_lines = []
    total_songs = 0
    total_publishing_composers = 0

    songs = get_submitted_songs(start_date: start_date, end_date: end_date)
    person_ids = songs.collect(&:album).collect(&:person_id).flatten.uniq

    unallocated_sums_splits = PublishingCompositionSplit.joins([publishing_composer: :person], :publishing_composition).where(publishing_composers: { person_id: person_ids }, publishing_compositions: { is_unallocated_sum: true }).includes(:publishing_composition, [publishing_composer: :person])

    songs.group_by { |s| s.album.person }.each do |person, person_songs|
      Rails.logger.info("Generating catalog for person id #{person.id}")

      hash_of_songs = collect_catalog_data(person_songs)
      u_sums_split = unallocated_sums_splits.detect { |split| split.publishing_composer.person_id == person.id }
      hash_of_songs << collect_u_sums_publishing_composition_data(u_sums_split) if u_sums_split

      xls_name = "#{person.id}_catalog_#{start_date.strftime('%Y_%m_%d')}"
      xls_name << "_to_#{end_date.strftime('%Y_%m_%d')}" if end_date
      sheet_name = "#{person.id}_#{start_date.strftime('%Y_%m_%d')}"
      filename = generate_catalog_xls(hash_of_songs, xls_name, { sheet_name: sheet_name })

      summary_report_lines << generate_catalog_summary_report_line(person.id, start_date)

      report_filenames << filename
      total_songs += hash_of_songs.count
      total_publishing_composers += 1
      Rails.logger.info("Successfully generated excel #{filename}")
    end

    report_filenames << write_catalog_summary_report(summary_report_lines, start_date)

    summary = { total_songs: total_songs, total_publishing_composers: total_publishing_composers }
    return report_filenames, summary
  end

  def self.write_catalog_summary_report(lines, date)
    create_dir(TEMP_DIR)
    file = File.join(TEMP_DIR, "#{date.strftime('%Y_%m_%d')}_summary.csv")

    CSV.open(file, "wb") do |report|
      lines.each { |line| report << line }
    end

    file
  end

  def self.generate_catalog_summary_report_line(person_id, date)
    publishing_composer = PublishingComposer.where(person_id: person_id).first
    intentional_blank = ""

    [
      date.strftime("%Y_%m_%d"),
      publishing_composer.first_name + " " + publishing_composer.last_name,
      person_id,
      intentional_blank,
      publishing_composer.cae
    ]
  end

  def self.get_submitted_songs(options = {})
    person_id = options[:person_id]
    start_date = options[:start_date]
    end_date = options[:end_date] || start_date

    conditions =
      if person_id
        ["publishing_composition_splits.percent > 0 and publishing_compositions.state = 'split_submitted' and publishing_composers.person_id = ?", person_id]
      elsif start_date || end_date
        ["publishing_composition_splits.percent > 0 and publishing_compositions.state = 'split_submitted' and publishing_compositions.state_updated_on >= ? and publishing_compositions.state_updated_on <= ?", start_date.to_date, end_date.to_date]
      end

    Song.joins(publishing_composition: [publishing_composition_splits: :publishing_composer]).includes(publishing_composition: [publishing_composition_splits: :publishing_composer], album: [person: [:artist, :label]]).where(conditions).order("person_id")
  end

  def self.client_import(options = {})
    xls_name, publishing_composers = query_new_client_data(options)
    updated_publishing_composers = publishing_composers_with_new_tax_submission = []
    publishing_composers_with_new_yt_preference = publishing_composers_with_updated_yt_preference = []
    unless options[:person_ids]
      updated_publishing_composers = query_updated_client_data(options)
      publishing_composers_with_new_tax_submission = query_new_tax_submissions(options[:start_date], options[:end_date])
      publishing_composers_with_new_yt_preference = query_new_youtube_preference(options[:start_date], options[:end_date])
      publishing_composers_with_updated_yt_preference = query_updated_youtube_preference(options)
      publishing_composers_with_updated_cae = query_updated_cae(options)
    end
    title, cae_title = client_import_titles
    dir = prepare_dir(xls_name)
    filename = "#{dir}/#{xls_name}.xls"

    File.delete(filename) if File.exist?(filename)

    SimpleXlsx::Serializer.new(filename) do |doc|
      add_sheet(doc, xls_name, title, publishing_composers, "client")
      add_sheet(doc, "New Tax Info", title, publishing_composers_with_new_tax_submission, "client")
      add_sheet(doc, "Tax Info Updates", title, updated_publishing_composers, "client")
      add_sheet(doc, "New YT Preference", title, publishing_composers_with_new_yt_preference, "client")
      add_sheet(doc, "YT Preference Updates", title, publishing_composers_with_updated_yt_preference, "client")
      add_sheet(doc, "CAE Updates", cae_title, publishing_composers_with_updated_cae, "cae")
    end
    filename
  end

  def self.client_import_csv(options = {})
    xls_name, publishing_composers = query_new_client_data(options)
    updated_publishing_composers = publishing_composers_with_new_tax_submission = []
    publishing_composers_with_new_yt_preference = publishing_composers_with_updated_yt_preference = []
    unless options[:person_ids]
      updated_publishing_composers = query_updated_client_data(options)
      publishing_composers_with_new_tax_submission = query_new_tax_submissions(options[:start_date], options[:end_date])
      publishing_composers_with_new_yt_preference = query_new_youtube_preference(options[:start_date], options[:end_date])
      publishing_composers_with_updated_yt_preference = query_updated_youtube_preference(options)
      publishing_composers_with_updated_cae = query_updated_cae(options)
    end
    title, cae_title = client_import_titles
    folder_name = "#{xls_name}#{Time.now.strftime('%y_%m_%d_%H_%M_%S')}"
    path = File.join(TEMP_DIR_CLIENT_IMPORT, folder_name)
    FileUtils.mkdir_p(path) unless File.exist?(path)
    create_csv(path, xls_name, title, publishing_composers, "client")
    create_csv(path, "New Tax Info", title, publishing_composers_with_new_tax_submission, "client")
    create_csv(path, "Tax Info Updates", title, updated_publishing_composers, "client")
    create_csv(path, "New YT Preference", title, publishing_composers_with_new_yt_preference, "client")
    create_csv(path, "YT Preference Updates", title, publishing_composers_with_updated_yt_preference, "client")
    create_csv(path, "CAE Updates", cae_title, publishing_composers_with_updated_cae, "cae")
    [path, folder_name]
  end

  def self.create_csv(path, filename, title, publishing_composers, data_type)
    file_path = File.join(path, "#{filename}.csv")

    CSV.open(file_path, "w") do |csv|
      csv << title
      publishing_composers.each do |publishing_composer|
        case data_type
        when "client"
          data = collect_client_data(publishing_composer)
        when "cae"
          data = collect_cae_data(publishing_composer)
        end
        csv << data
      end
    end
  end

  def self.add_sheet(doc, name, title, publishing_composers, data_type)
    doc.add_sheet(name) do |sheet|
      sheet.add_row(title)
      publishing_composers.each do |publishing_composer|
        case data_type
        when "client"
          data = collect_client_data(publishing_composer)
        when "cae"
          data = collect_cae_data(publishing_composer)
        end
        sheet.add_row(data)
      end
    end
  end

  def self.cleanup_temp_files
    begin
      # find folders older than 5 days
      old_folders = `find #{TEMP_DIR} -maxdepth 1 -mtime +5`.split("\n")
      old_folders.each { |f| FileUtils.rm_rf(f) }
      Rails.logger.info "Removed folder older than 5 days in #{TEMP_DIR}"
    rescue => e
      Rails.logger.warn "Failed to remove tmp publishing_reports folder that's older than 5 days: #{e.message}"
    end
  end

  # Put temp files into its own directory for easier cleanup, since xlsx generator creates lots of temporary xml files
  def self.prepare_dir(dir_name)
    dir = [TEMP_DIR, dir_name].join("/")
    begin
      create_dir(dir)
    rescue
      Rails.logger.error "Failed to create #{dir}"
    end
  end

  def self.create_dir(dir)
    FileUtils.mkdir_p(dir) unless File.exist?(dir)

    dir
  end

  def self.collect_client_data(publishing_composer)
    tax_percent, tax_id, tax_form = collect_tax_info(publishing_composer)
    youtube_channel, mcn_date, whitelist = collect_youtube_preference(publishing_composer)
    [
      nil,
      collect_name(publishing_composer),
      collect_address(publishing_composer),
      collect_phone(publishing_composer),
      collect_contract_date(publishing_composer),
      nil,
      "TUNE",
      collect_held(publishing_composer),
      nil,
      nil,
      collect_client_notes(publishing_composer),
      nil,
      nil,
      nil,
      collect_email(publishing_composer),
      nil,
      nil,
      nil,
      nil,
      nil,
      nil,
      nil,
      tax_percent,
      nil,
      nil,
      tax_id,
      nil,
      nil,
      nil,
      nil,
      nil,
      publishing_composer&.person&.name,
      nil,
      nil,
      publishing_composer.person_id,
      nil,
      tax_form,
      youtube_channel,
      mcn_date,
      (whitelist ? "Yes" : "No" if whitelist),
      suspicious?(publishing_composer)
    ]
  end

  def self.collect_cae_data(publishing_composer)
    [publishing_composer.person_id, collect_name(publishing_composer), publishing_composer.cae]
  end

  def self.collect_name(publishing_composer)
    publishing_composer.tax_name
  end

  def self.collect_address(publishing_composer)
    publishing_composer.tax_info ? publishing_composer.tax_info.mailing_address : ""
  end

  def self.collect_held(publishing_composer)
    if publishing_composer.submitted_tax_id? or publishing_composer.completed_w8ben?
      "N"
    else
      "Y"
    end
  end

  def self.collect_client_notes(publishing_composer)
    notes = ""
    notes << "(CAE/IPI# #{publishing_composer.cae})" if publishing_composer.cae.present?
    notes << "\nPublisher affiliation: #{publishing_composer.publisher.name if publishing_composer.publisher}"

    notes
  end

  def self.collect_phone(publishing_composer)
    publishing_composer.phone.presence || publishing_composer.account&.person&.phone_number
  end

  def self.collect_contract_date(publishing_composer)
    publishing_composer.paid_at.to_date if publishing_composer.paid_at
  end

  def self.collect_email(publishing_composer)
    publishing_composer.email.presence || publishing_composer.account&.person&.email
  end

  def self.collect_tax_info(publishing_composer)
    tax_percent = nil
    tax_id = nil
    tax_form = nil
    if publishing_composer.completed_w8ben?
      tax_percent = 30
      tax_form = "W8BEN"
      tax_id =
        publishing_composer.tax_id || "W8BEN"
    elsif publishing_composer.completed_w9?
      tax_id = publishing_composer.tax_id
      tax_form = "W9"
    end
    return tax_percent, tax_id, tax_form
  end

  def self.collect_youtube_preference(publishing_composer)
    yt_pref = publishing_composer&.person&.youtube_preference
    # youtube_channel, mcn_date, whitelist
    return yt_pref.channel_id, (yt_pref.mcn ? yt_pref.mcn_agreement.to_date : nil), yt_pref.whitelist if yt_pref
  end

  def self.collect_catalog_data(songs_with_split)
    songs = []
    songs_with_split.each do |song|
      song_hash = {}
      song_hash["publishing_composers"] = []
      song_hash["publishing_composition"] = song.publishing_composition.attributes
      song_hash["song"] = song.attributes.merge("isrc" => song.isrc, "artist_name" => song.artist_name)
      # have to add to attributes since non-attribute method is not available when calling attributes
      song_hash["album"] = song.album.attributes.merge("label_name" => song.album.label_name, "upc" => song.album.upc, "artist_name" => song.album.artist_name)
      person = song.album.person
      song_hash["person"] = person.attributes.merge("artist_name" => person.artist_name, "suspicious" => person.suspicious?)
      song_hash["publishers"] = []
      publishing_composers_count = 0
      publishers_count = 0

      song.publishing_composition.publishing_composition_splits.each do |split|
        if publishing_composer = split.publishing_composer
          publisher = publishing_composer.publisher ? publishing_composer.publisher.attributes : {}
          song_hash["publishing_composers"] << publishing_composer.attributes.merge(split.attributes).merge("publisher" => publisher).merge("tax_name" => publishing_composer.tax_name)
          publishing_composers_count += 1
        end

        if split.publishing_composer.publisher
          song_hash["publishers"] << split.publishing_composer.publisher.attributes
          publishers_count += 1
        end
      end
      song_hash["publishing_composers_count"] = publishing_composers_count
      song_hash["publishers_count"] = publishers_count
      songs << song_hash
    end

    songs
  end

  def self.collect_u_sums_publishing_composition_data(unallocated_sums_split)
    split = unallocated_sums_split
    song = {}
    song["is_unallocated_sum"] = true
    song["publishing_composition"] = split.publishing_composition.attributes

    publisher = split.publishing_composer.publisher ? split.publishing_composer.publisher.attributes : {}
    song["publishing_composers"] = [split.publishing_composer.attributes.merge(split.attributes).merge("publisher" => publisher).merge("tax_name" => split.publishing_composer.tax_name)]

    song
  end

  # outputting the catalog data to excel and return the filename of it
  def self.generate_catalog_xls(songs, xls_name, options = {})
    dir = prepare_dir(xls_name)
    filename = "#{dir}/#{xls_name}.xls"
    sheet_name = options[:sheet_name] || xls_name

    File.delete(filename) if File.exist?(filename)

    SimpleXlsx::Serializer.new(filename) do |doc|
      doc.add_sheet(sheet_name) do |sheet|
        if songs.empty?
          sheet.add_row("THERE IS NO DATA FOR THIS PERSON. IT COULD BE DUE TO THIS PERSON ENTERING 0% SPLIT ON A SINGLE.")
        else
          max_publishing_composers = max_clients = DEFAULT_MAX_COMPOSERS
          max_publishers = DEFAULT_MAX_PUBLISHERS

          publishing_composers_title = collect_publishing_composers_title(max_publishing_composers)
          clients_title = collect_clients_title(max_clients)
          publishers_title = collect_publishers_title(max_publishers)
          territories_title = collect_territories_title
          album_title = [
            "Recording 1 Recorded? (Y/N)",
            "Recording 1 Label Name",
            "Recording 1 #{I18n.t(:catalog).capitalize} Number",
            "Custom Text Field 1",
            "Recording 1 Display Artist",
            "Recording 1 Album Title",
            "Recording 1 Release Date (Format CCYYMMDD)",
            "Recording 1 ISRC",
            "Custom Text Field 2"
          ]
          sheet.add_row(["Song Title"] + territories_title + publishing_composers_title + clients_title + publishers_title + album_title + ["Custom Text Field 4", "Suspicious", "Email"])

          songs.each do |song|
            # in very rare scenario(due to missing splits information, publishing_composers would be missing)
            if song["publishing_composers"].blank?
              sheet.add_row([song_name(song), "COMPOSERS DATA IS MISSING FOR THIS SONG. IT COULD BE DUE TO MISSING SPLIT INFORMATION"])
            else
              publishing_composition_id = song["publishing_composition"]["id"]

              if song["is_unallocated_sum"]
                # since unallocated sums has quite a bit of empty fields, I had to fill some arrays with nils
                publishing_composers = collect_u_sums_publishing_composers_data(song["publishing_composers"], max_publishing_composers)
                clients = collect_clients_data(song["publishing_composers"], max_clients)
                publishers = [].fill(nil, 0..23)
                territories = [nil]
                album = [].fill(nil, 0..8)
                sheet.add_row([u_sums_song_name(song), territories + publishing_composers + clients + publishers + album, publishing_composition_id, nil].flatten)
              else
                publishing_composers = collect_publishing_composers_data(song["publishing_composers"], max_publishing_composers)
                clients = collect_clients_data(song["publishing_composers"], max_clients)
                publishers = collect_publishers_data(song["publishing_composers"], max_publishers)
                territories = collect_territories_data
                album = collect_album_data(song, publishers)
                suspicious = collect_suspicious_data(song["person"])
                email = song["person"]["email"]
                sheet.add_row([song_name(song), territories + publishing_composers + clients + publishers + album, publishing_composition_id, suspicious, email].flatten)
              end
            end
          end
        end
      end
    end

    filename
  end

  def self.collect_publishing_composers_title(max_publishing_composers)
    publishing_composers_title = []
    (1..max_publishing_composers).each do |n|
      publishing_composer = "PublishingComposer #{n}"
      publishing_composers_title.concat [
        "#{publishing_composer} First Name",
        "#{publishing_composer} Surname",
        "#{publishing_composer} Controlled",
        "#{publishing_composer} CAE No",
        "#{publishing_composer} Capacity",
        "#{publishing_composer} Share",
        "#{publishing_composer} Affiliation",
        "#{publishing_composer} Linked Publisher"
      ]
    end

    publishing_composers_title
  end

  def self.collect_clients_title(max_clients)
    clients_title = []
    (1..max_clients).each do |n|
      clients_title += ["Client #{n} Name", "Client #{n} Share"]
    end

    clients_title
  end

  def self.collect_publishers_title(max_publishers)
    publishers_title = []
    (1..max_publishers).each do |n|
      publisher = "Publisher #{n}"
      publishers_title.concat [
        "#{publisher} Name",
        "#{publisher} Controlled",
        "#{publisher} CAE No",
        "#{publisher} Capacity",
        "#{publisher} PO Share",
        "#{publisher} PC Share",
        "#{publisher} MO Share",
        "#{publisher} MC Share"
      ]
    end

    publishers_title
  end

  def self.collect_territories_title
    ["Territory 1 Name"]
  end

  def self.collect_publishing_composers_data(publishing_composers, max_publishing_composers)
    combined_publishing_composers = []
    max_index = 8
    (0...max_publishing_composers).each do |n|
      if publishing_composers[n]
        # return publishing_composers data as array
        publishing_composer = publishing_composers[n]
        combined_publishing_composers += [
          publishing_composer["first_name"].strip.to_s.capitalize,
          publishing_composer["last_name"].strip.to_s.capitalize,
          "Y",
          publishing_composer["cae"],
          "CA",
          publishing_composer["percent"],
          songwriter_affiliation(publishing_composer),
          linked_publisher(publishing_composer)
        ]
      elsif publishing_composers[n - 1] && publishing_composers[n - 1]["percent"] < 100
        # if there is no publishing_composers[n] and publishing_composers[n-1] is less than 100%, enter Unknown Writer
        share = (100 - publishing_composers[n - 1]["percent"].to_f)
        combined_publishing_composers += ["Unknown Writer", nil, "N", nil, "CA", share, nil, "Unknown Publisher"]
      else
        combined_publishing_composers += [].fill(nil, 0...max_index)
      end
    end

    combined_publishing_composers
  end

  def self.collect_u_sums_publishing_composers_data(_publishing_composers, _max_publishing_composers)
    combined_publishing_composers = [nil, "Unallocated Sums", nil, nil, "CA", nil, nil, nil]
    combined_publishing_composers += [].fill(nil, 0...8)
    combined_publishing_composers
  end

  def self.collect_clients_data(publishing_composers, max_clients)
    combined_clients = []
    (0...max_clients).each do |n|
      if publishing_composers[n]
        # return publishing_composers data as array
        publishing_composer = publishing_composers[n]
        combined_clients += [publishing_composer["tax_name"], 100]
      else
        combined_clients += [client_2_name(publishing_composers[0]), 100]
      end
    end

    combined_clients
  end

  def self.collect_publishers_data(publishing_composers, max_publishers)
    publishers = process_publishers_logic(publishing_composers)
    combined_publishers = []
    max_index = 7
    (0...max_publishers).each do |n|
      if publishers[n]
        publisher = publishers[n]
        combined_publishers += [
          publisher["name"],
          publisher["controlled"],
          publisher["cae"],
          publisher["capacity"],
          publisher["po_share"],
          publisher["pc_share"],
          publisher["mo_share"],
          publisher["mc_share"]
        ]
      else
        combined_publishers += [].fill(nil, 0..max_index)
      end
    end

    combined_publishers
  end

  def self.collect_territories_data
    ["WORL"]
  end

  def self.collect_album_data(song_hash, publishers)
    album = song_hash["album"]
    person = song_hash["person"]
    song = song_hash["song"]
    # ['Recorded?', 'Record Label(CWR)', 'Catalog Number(CWR)', 'Custom Field 1', 'Artist(CWR)', 'Album Title(CWR)', 'Release Date(CWR)', 'ISRC Code', 'Custom Field 2']
    ["Y", album["label_name"], nil, custom_field_1(publishers, song_hash["publishing_composers"]), artist_name(song, album), album["name"], release_date(album), isrc_code(song), (album["upc"]).to_s]
  end

  def self.collect_suspicious_data(person)
    person["suspicious"] ? "Yes" : "No"
  end

  def self.linked_publisher(publishing_composer)
    if publishing_composer["publisher"].blank?
      case songwriter_affiliation(publishing_composer)
      when "", nil
        "TUNECORE DIGITAL MUSIC"
      when "BMI"
        "TUNECORE DIGITAL MUSIC"
      when "ASCAP"
        "TuneCore Publishing"
      when "SESAC"
        "TuneCore Songs"
      else
        "TUNECORE DIGITAL MUSIC"
      end
    else
      publishing_composer["publisher"]["name"]
    end
  end

  def self.songwriter_affiliation(publishing_composer)
    return unless publishing_composer
  end

  def self.publisher_affiliation(publishing_composer)
    publishing_composer["publisher"]["performing_rights_organization"]["name"]
  end

  # contains the name of the administering publisher
  def self.client_2_name(publishing_composer)
    # if there is publisher affiliation, the administrating publisher would be based on the publisher affiliation
    affiliation =
      publisher_affiliation(publishing_composer).presence || songwriter_affiliation(publishing_composer)

    case affiliation
    when "BMI", "", nil
      "TuneCore Digital Music"
    when "ASCAP"
      "TuneCore Publishing"
    when "SESAC"
      "TuneCore Songs"
    else
      # For all other affiliation, it defaults to BMI
      "TuneCore Digital Music"
    end
  end

  def self.non_registered_publisher_affiliation(songwriter_affiliation)
    # we only recognize BMI, ASCAP, SESAC and SOCAN as the valid
    # publisher affiliation if the publishing_composer is not registered
    # with a publisher affiliation but has a songwriter affiliation
    if ["BMI", "ASCAP", "SESAC", "SOCAN"].include?(songwriter_affiliation)
      songwriter_affiliation
    else
      "BMI" # default to BMI for foreign society
    end
  end

  def self.custom_field_1(publishers, publishing_composers)
    affiliation =
      publisher_affiliation(publishing_composers[0]).presence || non_registered_publisher_affiliation(songwriter_affiliation(publishing_composers[0]))
    custom_field_1 = publishers[0]
    custom_field_1 += " (#{affiliation})"
    custom_field_1 += " Admin By TuneCore"
    custom_field_1.upcase
  end

  def self.isrc_code(song)
    song["isrc"] || song["id"]
  end

  def self.release_date(album)
    return if album["sale_date"].blank?

    album["sale_date"].strftime("%Y/%m/%d")
  end

  def self.song_name(song)
    song["song"]["name"].to_s.upcase
  end

  def self.u_sums_song_name(song)
    # Even though the publishing_composition name already contains the correct name
    # depending on publisher's status, we are re-checking it here in case
    # a publisher is removed after it was created
    song_name =
      if publisher_affiliation(song["publishing_composers"][0]).blank?
        song["publishing_composition"]["name"]
      else
        song["publishing_composers"][0]["publisher"]["name"]
      end

    song_name.upcase.to_s
  end

  def self.artist_name(song, album)
    return song["artist_name"] if song["artist_name"].present?

    album["artist_name"]
  end

  def self.suspicious?(publishing_composer)
    publishing_composer.account&.person&.suspicious? ? "Yes" : "No"
  end

  class << self
    private

    def process_publishers_logic(publishing_composers)
      publishing_composer = publishing_composers[0]
      writer_affiliation = songwriter_affiliation(publishing_composer)
      publisher_affiliation = publisher_affiliation(publishing_composer)

      publishers = []
      remaining_share = (100 - publishing_composer["percent"].to_f)

      if publisher_affiliation.blank?
        # when no publisher affiliation, use writer affiliation to determine the correct tunecore publisher name and cae#
        tc_publisher_name = tunecore_publisher_name(writer_affiliation)
        tc_cae = tunecore_cae(writer_affiliation)

        publishers[0] = {
          "name" => tc_publisher_name,
          "controlled" => "Y",
          "cae" => tc_cae,
          "capacity" => "OP",
          "po_share" => calculate_performance_share(publishing_composer["percent"]),
          "pc_share" => calculate_performance_share(publishing_composer["percent"]),
          "mo_share" => publishing_composer["percent"],
          "mc_share" => publishing_composer["percent"]
        }
      else
        tc_publisher_name = tunecore_publisher_name(publisher_affiliation)
        tc_cae = tunecore_cae(publisher_affiliation)

        publishers[0] = {
          "name" => publishing_composer["publisher"]["name"],
          "controlled" => "Y",
          "cae" => publishing_composer["publisher"]["cae"],
          "capacity" => "OP",
          "po_share" => calculate_performance_share(publishing_composer["percent"]),
          "pc_share" => 0,
          "mo_share" => publishing_composer["percent"],
          "mc_share" => 0
        }
        publishers[1] = {
          "name" => tc_publisher_name,
          "controlled" => "Y",
          "cae" => tc_cae,
          "capacity" => "AM",
          "po_share" => 0,
          "pc_share" => calculate_performance_share(publishing_composer["percent"]),
          "mo_share" => 0,
          "mc_share" => publishing_composer["percent"]
        }
      end

      if remaining_share.positive?
        publishers << {
          "name" => "Unknown Publisher",
          "controlled" => "N",
          "cae" => nil,
          "capacity" => "OP",
          "po_share" => calculate_performance_share(remaining_share),
          "pc_share" => calculate_performance_share(remaining_share),
          "mo_share" => remaining_share,
          "mc_share" => remaining_share
        }
      end

      publishers
    end

    def tunecore_publisher_name(pro_affiliation)
      case pro_affiliation
      when "BMI"
        "Tunecore Digital Music"
      when "ASCAP"
        "Tunecore Publishing"
      when "SESAC"
        "Tunecore Songs"
      else
        "Tunecore Digital Music"
      end
    end

    def tunecore_cae(pro_affiliation)
      case pro_affiliation
      when "BMI"
        TC_BMI_CAE
      when "ASCAP"
        TC_ASCAP_CAE
      when "SESAC"
        TC_SESAC_CAE
      else
        TC_BMI_CAE
      end
    end

    def calculate_performance_share(share)
      share.to_f / 2
    end

    def query_new_client_data(options)
      if options[:start_date]
        start_date = Date.strptime(options[:start_date], "%m/%d/%Y")
        begin_date_time = start_date.beginning_of_day
        end_date_time = start_date.end_of_day
      end

      if options[:end_date]
        end_date = Date.strptime(options[:end_date], "%m/%d/%Y")
        end_date_time = end_date.end_of_day
      end

      if options[:person_ids]
        joins = nil
        conditions = ["publishing_composers.person_id in (?)", options[:person_ids]]
        xls_name = "client_import_by_accounts_#{Time.now.strftime('%Y_%m_%d')}"
      elsif start_date
        joins = [:related_purchases]
        conditions = ["paid_at between ? and ?", begin_date_time, end_date_time]
        xls_name = "client_import_#{start_date.strftime('%Y_%m_%d')}"
        xls_name << "_to_#{end_date.strftime('%Y_%m_%d')}" if end_date
      else
        joins = nil
        conditions = nil
        xls_name = "client_import_#{Time.now.strftime('%Y_%m_%d')}"
      end

      return xls_name, PublishingComposer.joins(joins).where(conditions).includes(:publisher, :person)
    end

    def query_updated_client_data(options)
      if options[:start_date]
        start_date = Date.strptime(options[:start_date], "%m/%d/%Y")
        begin_date_time = start_date.beginning_of_day
        end_date_time = start_date.end_of_day
      end

      if options[:end_date]
        end_date = Date.strptime(options[:end_date], "%m/%d/%Y")
        end_date_time = end_date.end_of_day
      end

      joins = [:related_purchases, [tax_info: :versions]]
      conditions =
        if start_date
          # we want to find customer that updated their tax information after they paid and paid_at < the begin_date_time because
          # anyone who has paid between the begin_date_time and end_date_time are included in the query_new_client_data
          ["paid_at is not null and paid_at < ? and versions.created_at between ? and ? and versions.event = 'Update'", begin_date_time, begin_date_time, end_date_time]
        else
          ["paid_at is not null and versions.event = 'Update'"]
        end

      PublishingComposer.select("distinct publishing_composers.*").joins(joins).where(conditions).includes(:publisher, :person)
    end

    def query_new_tax_submissions(start_date, end_date = nil)
      start_date = Date.strptime(start_date, "%m/%d/%Y")
      begin_date_time = start_date.beginning_of_day
      end_date_time =
        if end_date
          Date.strptime(end_date, "%m/%d/%Y").end_of_day
        else
          start_date.end_of_day
        end

      joins = [:related_purchases, :tax_info]
      conditions = ["paid_at is not null and paid_at < ? and tax_info.created_at between ? and ?", begin_date_time, begin_date_time, end_date_time]

      PublishingComposer.select("distinct publishing_composers.*").joins(joins).where(conditions).includes(:publisher, :person)
    end

    def query_new_youtube_preference(start_date, end_date = nil)
      start_date = Date.strptime(start_date, "%m/%d/%Y")
      begin_date_time = start_date.beginning_of_day

      end_date_time =
        if end_date
          Date.strptime(end_date, "%m/%d/%Y").end_of_day
        else
          start_date.end_of_day
        end

      joins = [:related_purchases, [person: :youtube_preference]]
      conditions = ["paid_at is not null and paid_at < ? and youtube_preferences.created_at between ? and ?", begin_date_time, begin_date_time, end_date_time]

      PublishingComposer.select("distinct publishing_composers.*").joins(joins).where(conditions).includes(:publisher, :person)
    end

    def query_updated_youtube_preference(options)
      if options[:start_date]
        start_date = Date.strptime(options[:start_date], "%m/%d/%Y")
        begin_date_time = start_date.beginning_of_day
        end_date_time = start_date.end_of_day
      end

      if options[:end_date]
        end_date = Date.strptime(options[:end_date], "%m/%d/%Y")
        end_date_time = end_date.end_of_day
      end

      joins = [:related_purchases, [person: :youtube_preference]]
      conditions =
        if start_date
          ["paid_at is not null and paid_at < ? and youtube_preferences.updated_at between ? and ?", begin_date_time, begin_date_time, end_date_time]
        else
          ["paid_at is not null and youtube_preferences is not null"]
        end

      PublishingComposer.select("distinct publishing_composers.*").joins(joins).where(conditions).includes(:publisher, :person)
    end

    def query_updated_cae(options)
      if options[:start_date]
        start_date = Date.strptime(options[:start_date], "%m/%d/%Y")
        begin_date_time = start_date.beginning_of_day
        end_date_time = start_date.end_of_day
      end

      if options[:end_date]
        end_date = Date.strptime(options[:end_date], "%m/%d/%Y")
        end_date_time = end_date.end_of_day
      end

      joins = [:related_purchases, :versions]
      conditions =
        if start_date
          ["paid_at is not null and paid_at < ? and versions.created_at between ? and ? and versions.event = 'Update'", begin_date_time, begin_date_time, end_date_time]
        else
          ["paid_at is not null and versions.event = 'Update'"]
        end

      PublishingComposer.select("distinct publishing_composers.*").joins(joins).where(conditions).includes(:publisher, :person)
    end

    def client_import_titles
      [
        [
          "Code",
          "Name",
          "Address",
          "Telephone No",
          "Contract Date",
          "Option Date",
          "Company Code",
          "Held",
          "Client Cross Method",
          "Client Crossed To",
          "Notes",
          "Frequency",
          "Fax No",
          "Territory Code",
          "Email Address",
          "Currency Code",
          "Minimum Payment Amount",
          "Accounting Days",
          "Admin Deal",
          "Admin Deal Net Terms",
          "Admin Deal Administered",
          "Admin Deal Net Remainder",
          "Tax Percentage",
          "Tax Type",
          "Tax Number",
          "Tax Execption No or Social Security No",
          "Expiry Date",
          "Controlled",
          "Email Documents",
          "Admin Deal Adminstrating",
          "Include Royalty Transfer File",
          "Contact Name",
          "Email CC",
          "Email BCC",
          "Accounting System Number",
          "Site ID",
          "Tax Form",
          "YouTube Channel",
          "MCN Date",
          "Whitelist",
          "Suspicious"
        ],
        ["TC Account ID", "Writer Name", "PRO", "CAE number"]
      ]
    end
  end
end
