class FeatureBlacklist < ApplicationRecord
  validates :feature, :person_id, presence: true
end
