class CoverSongMetadata < ApplicationRecord
  has_paper_trail

  belongs_to :song
  validates :song, presence: true
end
