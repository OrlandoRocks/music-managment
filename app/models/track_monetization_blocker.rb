class TrackMonetizationBlocker < ActiveRecord::Base
  belongs_to  :song
  belongs_to  :store

  validates   :song, presence: true
  validates   :store, presence: true
  validates   :song_id,
              uniqueness: {
                scope: :store_id,
                message: "there is already a track_monetization_blocker with this song and store"
              }

  after_create :block_track_monetization
  after_destroy :unblock_track_monetization

  def block_track_monetization
    track_monetization&.block_and_disqualify(
      {
        actor: "TrackMonetizationBlocker",
        message: "blocking_distribution"
      }
    ) unless track_monetization&.ineligible?
  end

  private

  def unblock_track_monetization
    track_monetization&.unblock_and_qualify(
      {
        actor: "TrackMonetizationBlocker",
        message: "unblocking_distribution"
      }
    ) unless track_monetization&.eligible?
  end

  def track_monetization
    @track_monetization ||= TrackMonetization.find_by(song_id: song_id, store_id: store_id)
  end
end
