# == Schema Information
# Schema version: 404
#
# Table name: uploads
#
#  id                   :integer(11)     not null, primary key
#  song_id              :integer(11)
#  original_filename    :string(255)
#  uploaded_filename    :string(255)
#  converted_filename   :string(255)
#  filetype             :string(32)
#  bitrate              :integer(4)
#  is_probably_lossless :boolean(1)
#  created_at           :datetime
#

class Upload < ApplicationRecord
  belongs_to :song

  validates :original_filename, :uploaded_filename, :bitrate, presence: true
  validates :uploaded_filename, uniqueness: true
  validate :validate_file
  validates_with Utf8mb3Validator, fields: [:original_filename]

  after_save :destroy_if_song_blank

  def destroy_if_song_blank
    destroy if song_id.nil?
  end

  def self.verify_filesystem_matches_db
    Dir.chdir(File.join(Rails.root, UPLOAD_DIR))

    verification_errors = []

    Dir.glob("*.{mp3,m4a}").each do |filename|
      verification_errors << filename + " in filesystem, but not in DB" unless find_by(uploaded_filename: filename)
    end

    all.find_each do |upload|
      unless Dir.glob(upload.uploaded_filename)
        verification_errors << upload.uploaded_filename + " in DB, but not in filesystem"
      end
    end

    verification_errors
  end

  def file=(incoming_file)
    self.bitrate = 0
    @temp_file = incoming_file
    return false if incoming_file == ""

    # XXX:  Check for SQL injection
    self.original_filename = File.basename(incoming_file.original_filename)
    self.filetype = guess_filetype(original_filename)
    self.uploaded_filename = uuid_filename(filetype)
    case filetype
    when "mp3"
      begin
        mp3info = Mp3Info.new(@temp_file.path)
        self.bitrate = mp3info.bitrate
      rescue
        errors.add(:bitrate, I18n.t("models.media.your_file_is_corrupt_and_cannot_be_parsed"))
        logger.warn("Problem with file #{@temp_file.path}: #{$!}\n#{$!.backtrace}")
      end
    when "m4a"
      begin
        m4ainfo = MP4Info.new(@temp_file)
        self.bitrate = m4ainfo.BITRATE + 10 # MP4Info underestimates the bitrate slightly
        logger.debug("M4A bitrate: #{bitrate}")
        self.is_probably_lossless = true if bitrate >= 500
      rescue
        errors.add(:bitrate, I18n.t("models.media.your_file_is_corrupt_and_cannot_be_parsed"))
        logger.warn("Problem with file #{@temp_file.path}: #{$!}\n#{$!.backtrace}")
      end
    end
  end

  def validate_file
    return unless @temp_file

    case filetype
    when "mp3"
      begin
        mp3info = Mp3Info.new(@temp_file.path)
        self.bitrate = mp3info.bitrate
        logger.debug("MP3 bitrate: #{mp3info.bitrate}")
        errors.add(:bitrate, I18n.t("models.media.must_be_320_kbps")) unless bitrate > 256
      rescue
        errors.add(:filetype, I18n.t("models.media.your_file_is_corrupt_and_cannot_be_parsed"))
        logger.warn("Problem with file #{@temp_file.path}: #{$!}\n#{$!.backtrace}")
        # Ignore errors
        # XXX:  Eventually, logging / alerts
      end
    when "m4a"
      begin
        logger.debug("M4A bitrate: #{bitrate}")
        errors.add(:bitrate, I18n.t("models.media.must_be_320_kbps_or_greater")) unless bitrate >= 256
      rescue
        errors.add(:filetype, I18n.t("models.media.this_song_cannot_be_decoded"))
        logger.warn("Problem with file #{@temp_file.path}: #{$!}\n#{$!.backtrace}")
        # Ignore errors.
        # XXX: Eventually, logging / alerts
      end
    else
      errors.add(:filetype, I18n.t("models.media.must_be_mp3_or_aac"))
    end
  end

  def move_upload_to_s3
    begin
      reload
      if @temp_file
        logger.debug("Moving song upload from #{@temp_file.local_path} to #{song.staging_filename}")
        song.album.make_staging_directories
        FileUtils.copy(@temp_file.local_path, song.staging_filename)
      end
      logger.debug("Putting upload onto S3 on song ##{song.id}...")
      song.send_orig_to_s3
      logger.debug("Done sending upload to S3")
      FileUtils.rm_rf song.staging_filename if song.s3_orig_asset.exists? && File.exist?(song.staging_filename)
      logger.debug("Removing local copy after successfully sending to S3.")
    rescue => e
      logger.error("Error uploading song to s3: #{e.message}\n#{e.backtrace}")
      destroy
      errors.add(:filetype, I18n.t("models.media.error_uploading_song_to_s3"))
    end
  end

  def copy_to_upload_dir  # no longer called
    return unless @temp_file

    logger.debug(
      "Copying upload from temp to upload dir: #{File.join(
        Rails.root,
        UPLOAD_DIR,
        uploaded_filename
      )}"
    )
    FileUtils.mkdir_p(File.join(Rails.root, UPLOAD_DIR)) unless File.exist?(File.join(Rails.root, UPLOAD_DIR))
    FileUtils.copy(@temp_file.local_path, File.join(Rails.root, UPLOAD_DIR, uploaded_filename))
  end

  private

  def uuid_filename(suffix)
    "#{UUIDTools::UUID.random_create}.#{suffix}"
  end

  def guess_filetype(filename)
    case filename.split(".").last
    when "mp3"
      "mp3"
    when "m4a", "m4p", "aac"
      "m4a"
    else
      nil
    end
  end
end
