# = Description
# Advertisements give internal administrators the ability to create adx
# to be displayed on the Album, Single, or Ringtone and Dashboard pages.  These
# ads have three different display categories (wide, thumbnail, and sidebar).
#
# = Usage
# Advertisement.for_person will find the first applicable for the person and category.
#
# = Change Log
# [2010-04-22 -- CH]
# Created Model

class Advertisement < ApplicationRecord
  include Translatable
  attr_translate :name
  attr_translate :sidebar_title
  attr_translate :wide_title
  attr_translate :sidebar_copy
  attr_translate :wide_copy
  attr_translate :display_price_modifier
  attr_translate :display_price_notes
  attr_translate :image_caption
  attr_translate :thumbnail_link_text
  attr_translate :sidebar_link_text
  attr_translate :wide_link_text

  has_many  :targeted_advertisements
  has_one   :created_by, class_name: "Person", foreign_key: "id", primary_key: "created_by_id"

  ######################
  # Category Constants #
  ######################
  CATEGORY_DASHBOARD = "Dashboard"
  CATEGORY_ALBUM = "Album"
  CATEGORY_SINGLE = "Single"
  CATEGORY_RINGTONE = "Ringtone"
  CATEGORIES = ["Dashboard", "Album", "Single", "Ringtone"]

  #####################
  # Display Constants #
  #####################
  DISPLAY_TYPE_IMAGE = "image"
  DISPLAY_TYPE_COPY = "copy"
  DISPLAY_SIZE_WIDE = "wide"
  DISPLAY_SIZE_THUMBNAIL = "thumbnail"
  DISPLAY_SIZE_SIDEBAR = "sidebar"

  ################
  # Named Scopes #
  ################
  default_scope { order "advertisements.sort_order, advertisements.created_at DESC" }
  scope :active, -> { where status: "Active" }

  # expiration_date
  attr_accessor :display_size, :display_type, :expiration_date

  # returns either the ads for this individual or the
  # the ads targeted to the offer this individual
  # person => targets the ads to the passed in individual
  # category => 'Dashboard', 'Album', 'Single', 'Ringtone'
  # pass in the following options
  # :display_size => 'thumbnail', 'sidebar', 'wide' | default => 'sidebar'
  # :display_type => 'image', 'copy' | default => 'copy'
  # :number_of_ads => number of ads to return | default => 1
  # :overridden_targeted_offers => array of targeted offers to select ads from.  This is a
  #  performance enhancement because loading individual multiple ads on one page will cause a reload of a person's
  #  targeted offers each call to Advertisement.for_person
  # person with a plan is shown no ads as there aren't any relevant ones at this point in time.
  def self.for_person(person, category, options = {})
    return [] if person.has_plan?

    targeted_offers = options[:overriden_targeted_offers] || TargetedOffer.for_person(person)
    ads = find_ad(targeted_offers, category, person, options[:number_of_ads] || 1)
    set_display_attributes(ads, person, options) unless ads.empty?
    ads
  end

  def set_display_attributes(person, options)
    self.display_size = options[:display_size] || DISPLAY_SIZE_SIDEBAR
    self.display_type = options[:display_type] || DISPLAY_TYPE_COPY
    return if targeted_offer_id.nil?

    to = TargetedOffer.find(targeted_offer_id)
    self.expiration_date = to.expiration_date_for_person(person)
  end

  def targeted_offer_id
    attributes["targeted_offer_id"]
  end

  def active?
    status == "Active"
  end

  def self.set_display_attributes(ads, person, options)
    ads.each do |ad|
      ad.set_display_attributes(person, options)
    end
  end

  def self.find_ad(offers, category, person, number_of_ads = 1)
    if offers.blank?
      ad = active.where("category = ? and is_default = 1 and country_website_id = ?", category, person.country_website_id).order("sort_order, advertisements.created_at DESC").limit(number_of_ads)
    else
      offer_ids = []
      offers.map { |o| offer_ids << [o.id] }
      ad = active
           .select("advertisements.*, targeted_advertisements.targeted_offer_id")
           .joins("left join targeted_advertisements on advertisement_id=advertisements.id")
           .where("(advertisements.category = :category and advertisements.country_website_id = :country_website_id) AND (targeted_offer_id IN (:offer_ids) or (targeted_offer_id IS NULL and is_default=1))", { category: category, country_website_id: person.country_website_id, offer_ids: offer_ids.flatten.uniq })
           .order("targeted_offer_id DESC, sort_order, targeted_advertisements.created_at DESC")
           .limit(number_of_ads)

      if ad.nil?
        ad = active.where("category = ? and is_default = 1", category).order("sort_order, advertisements.created_at DESC").limit(number_of_ads)
      end
    end

    ad
  end
end
