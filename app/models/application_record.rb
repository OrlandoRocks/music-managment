class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # enable strong parameters for all ApplicationRecord-derived models
  include ActiveModel::ForbiddenAttributesProtection
  include ApplicationRecordMethods
end
