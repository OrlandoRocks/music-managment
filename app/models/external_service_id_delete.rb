class ExternalServiceIdDelete < ApplicationRecord
  validates :artist_name, :identifier, :service_name, :person_id, presence: true
  belongs_to :person
end
