class SongCopyrightClaim < ApplicationRecord
  belongs_to :song
  belongs_to :person

  alias_attribute :claimant_id, :person_id
  alias_attribute :claimant, :person

  validates :song, presence: true
end
