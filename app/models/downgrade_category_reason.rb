class DowngradeCategoryReason < ApplicationRecord
  belongs_to :downgrade_category
  has_many :plan_downgrade_requests, dependent: :destroy
end
