class SalesRecordMasterNew < ApplicationRecord
  self.table_name = "sales_record_masters_new"

  belongs_to :sip_store
  belongs_to :country_website
end
