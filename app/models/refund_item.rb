# frozen_string_literal: true

class RefundItem < ApplicationRecord
  belongs_to :refund
  belongs_to :purchase

  before_validation :set_currency, if: -> { refund.present? }

  validates :refund, :purchase, presence: true

  validate :total_amount_less_than_refundable_amount, if: -> { purchase.present? }

  before_save :calculate_vat_cents_in_euro, if: :will_save_change_to_tax_amount_cents?

  after_save :recalculate_refund

  delegate :euro_exchange_rate, to: :refund, allow_nil: true

  def total_amount_cents
    base_amount_cents + tax_amount_cents
  end

  private

  def set_currency
    self.currency = refund.currency
  end

  def total_amount_less_than_refundable_amount
    return if (total_amount_cents <= purchase.refundable_amount_cents) || refund.backfill?

    errors.add(
      :base, "Requested amount is greater than "\
              "refundable amount for purchase_id #{purchase.id}"
    )
  end

  def calculate_vat_cents_in_euro
    self.vat_cents_in_euro = (tax_amount_cents * euro_exchange_rate).round
  end

  def recalculate_refund
    refund.reload
    refund.update!(
      base_amount_cents: refund.refund_items.sum(&:base_amount_cents),
      tax_amount_cents: refund.refund_items.sum(&:tax_amount_cents),
      vat_cents_in_euro: refund.refund_items.sum(&:vat_cents_in_euro),
      total_amount_cents: refund.refund_items.sum(&:total_amount_cents)
    )
  end
end
