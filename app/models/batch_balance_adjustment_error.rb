class BatchBalanceAdjustmentError < ApplicationRecord
  belongs_to :batch_balance_adjustment_detail
  belongs_to :batch_balance_adjustment
end
