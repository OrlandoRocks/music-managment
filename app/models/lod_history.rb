class LodHistory < ApplicationRecord
  self.table_name = "lod_history"

  belongs_to :lod
end
