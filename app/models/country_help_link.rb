class CountryHelpLink < ApplicationRecord
  def self.help_domains
    {
      "US" => "http://help.tunecore.com",
      "CA" => "http://ca.help.tunecore.com",
      "UK" => "http://uk.help.tunecore.com",
      "AU" => "http://au.help.tunecore.com",
      "DE" => "http://de.help.tunecore.com",
      "FR" => "http://fr.help.tunecore.com",
      "IT" => "http://it.help.tunecore.com"
    }
  end

  def self.help_link_path
    "/app/answers/detail/a_id/"
  end

  def self.get_domain_help_link(country, meta_id = nil)
    return help_domains[country] if meta_id.nil?

    help_link = get_help_link_map.detect { |c| c.meta_id == meta_id }
    help_id = help_link.try("#{country.downcase}_id".to_sym) || help_link.try(:us_id)

    return help_domains[country] if help_id.nil?

    help_link =  help_domains[country]
    help_link += help_link_path + help_id.to_s if help_id
    help_link
  end

  def self.get_help_link_map
    @help_link_map ||= all
  end
end
