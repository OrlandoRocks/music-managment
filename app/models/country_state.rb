class CountryState < ApplicationRecord
  belongs_to :country
  belongs_to :gst_config, optional: true
  has_many :country_state_cities,
           primary_key: :id,
           foreign_key: :state_id,
           inverse_of: :country_state,
           dependent: :destroy
  has_many :people

  def self.get_gst_config(state)
    return if state.blank?

    CountryState.find_by("lower(name) = ?", state.strip.downcase)&.gst_config
  end
end
