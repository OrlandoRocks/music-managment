class PersonCountry < ApplicationRecord
  enum source: {
    payoneer: "payoneer"
  }

  belongs_to :person
  belongs_to :country

  validates :person_id, presence: true
  validates :country_id, presence: true
  validates :source, presence: true

  validate :country_source_cannot_be_duplicated

  def self.last_country_recorded(source, person_id)
    where(source: source, person_id: person_id).order(id: :desc).first&.country_id
  end

  def self.record_country(source, person_id, country_id)
    return if last_country_recorded(source, person_id) == country_id

    PersonCountry.create(source: source, person_id: person_id, country_id: country_id)
  end

  private

  def country_source_cannot_be_duplicated
    return unless PersonCountry.last_country_recorded(source, person_id) == country_id

    errors.add(:country, "is already the latest from this source")
  end
end
