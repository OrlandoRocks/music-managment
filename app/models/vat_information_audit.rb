class VatInformationAudit < ApplicationRecord
  belongs_to :person

  scope :open, -> { where(end_date: nil) }

  def self.audit_change!(person, opts)
    person.vat_information_audits.create(opts.merge(end_date: Time.zone.now))
  end
end
