class YoutubeMonetization < ApplicationRecord
  belongs_to :person
  has_one    :purchase, as: :related
  has_many   :songs, through: :person
  has_many   :invoice_logs

  validates :person, presence: true
  validate  :agreed_to_terms_or_pub_opt_in

  scope :active, -> { where("effective_date is not null and (termination_date is null or termination_date > ?)", Time.now) }

  after_commit :sync_youtube_monetization_changes_to_believe_api

  def after_successful_purchase
    update_attribute(:effective_date, Time.now)

    logger.info "Sidekiq Job initiated TrackMonetization::SubscriptionWorker AfterPurchase: #{{ person_id: person.id, service: 'YTTracks' }}"
    TrackMonetization::SubscriptionWorker.perform_async(person_id: person.id, service: "YTTracks")
  end

  def monetize_or_mark_ineligible(songs_and_actions)
    songs_to_monetize, songs_to_mark_ineligible = [], []

    songs_and_actions.each do |song, action|
      case action
      when "monetize"
        songs_to_monetize << song
      when "ineligible"
        songs_to_mark_ineligible << song
      end
    end

    monetize_songs(songs_to_monetize) unless songs_to_monetize.empty?
    mark_as_ineligible(songs_to_mark_ineligible) unless songs_to_mark_ineligible.empty?
  end

  def mark_as_ineligible(song_ids)
    ensure_ownership(song_ids)
    ineligible_songs = []
    song_ids.each do |song_id|
      ineligible_songs << YtmIneligibleSong.new(song_id: song_id)
    end

    ineligible_songs.each(&:save).group_by(&:persisted?)
  end

  def mark_as_eligible(song_ids)
    ensure_ownership(song_ids)
    YtmIneligibleSong.where("song_id in (?)", song_ids).delete_all
  end

  def block_songs(song_ids)
    ensure_ownership(song_ids)

    blocked_songs = song_ids.map { |sid| YtmBlockedSong.new(song_id: sid) }

    blocked_songs.each(&:save).group_by(&:persisted?)
  end

  def unblock_songs(song_ids)
    ensure_ownership(song_ids)
    YtmBlockedSong.where(song: song_ids).destroy_all
  end

  def monetize_songs(song_ids)
    ensure_ownership(song_ids)
    store = Store.find_by(abbrev: "ytsr")
    songs = Song.find(song_ids)
    songs_by_album = songs.group_by(&:album_id)

    errored_songs = []

    songs_by_album.each do |album_id, songs|
      album = Album.find(album_id)
      salepoint = album.salepoints_by_store(store).first
      raise "Could not find YouTube store for album #{album.name}." if salepoint.nil?

      songs.each do |song|
        next if song.salepoint_songs.present? && song.salepoint_songs.any? { |ss| ss.salepoint.store_id == store.id }

        begin
          SalepointSong.create!(state: "approved", salepoint_id: salepoint.id, song_id: song.id)
        rescue
          errored_songs << song.name
        end
      end
    end

    return if errored_songs.blank?

    raise "#{errored_songs.join(', ')} may have not been updated correctly. Verify the state of the tracks"
  end

  def create_youtube_purchase(product)
    Product.add_to_cart(person, self, product) unless purchase
  end

  def self.has_youtube_monetization?(person)
    person.youtube_monetization.present?
  end

  def self.has_active_youtube_monetization?(person)
    person.youtube_monetization.present? && !person.youtube_monetization.termination_date?
  end

  def terminated?
    termination_date? && termination_date < DateTime.current
  end

  def self.find_youtube_product(person)
    product_id = Product.find_products_for_country(person.country_domain, :ytm)
    Product.find(product_id)
  end

  def name
    "YouTube Sound Recording Revenue"
  end

  def self.vetting_status(person)
    you_tube_store = Store.find_by(abbrev: "ytsr")
    ytsr_salepoints = person.live_releases.map { |a| a.salepoints.where(store_id: you_tube_store.id) }.flatten.group_by(&:status)
    statuses = ytsr_salepoints.keys

    if statuses.include?("new") && statuses.include?("complete")
      "some"
    elsif statuses.include?("new")
      "none"
    else
      "all"
    end
  end

  private

  def agreed_to_terms_or_pub_opt_in
    return unless (pub_opted_in.nil? && agreed_to_terms_at.nil?)

    errors.add(:base, I18n.t("models.youtube_money.must_agree_to_terms"))
  end

  def ensure_ownership(song_ids)
    raise "You cannot mark songs you don't own" unless all_song_ids_belong_to_person?(song_ids)
  end

  def all_song_ids_belong_to_person?(song_ids)
    (song_ids.map(&:to_i) - songs.pluck(:id)).empty?
  end

  def sync_youtube_monetization_changes_to_believe_api
    YoutubeMonetizations::SyncYoutubeMonetizationChangesToBelieveApi.call(self)
  end
end
