require Rails.root.join("lib", "paperclip", "validators", "dimensions_validator")

class ExternalServiceIdArtwork < ApplicationRecord
  has_many :external_service_ids

  S3_CREDS = {
    s3_credentials: {
      bucket: SCALED_ARTWORK_BUCKET_NAME,
      access_key_id: ASSETS_AWS_ACCESS_KEY,
      secret_access_key: ASSETS_AWS_SECRET_KEY
    }
  }

  has_attached_file :asset,
                    EXTERNAL_SERVICE_ID_ARTWORK_CONFIG.merge(S3_CREDS)
  validates_attachment :asset,
                       content_type: { content_type: ["image/jpeg", "image/png"] }
  validates_attachment :asset, dimensions: { min: { height: 2400, width: 2400 }, shape: "square" }

  before_save :format_file_name, if: :asset_file_name

  def format_file_name
    asset_file_name = asset_file_name.parameterize.underscore
  end
end
