class Creative < ApplicationRecord
  include Linkable
  include CreativeMethods

  belongs_to :creativeable, polymorphic: true
  belongs_to :person
  belongs_to :artist
  has_many :external_service_id, as: :linkable
  has_many :creative_song_roles, dependent: :destroy
  has_many :song_roles, through: :creative_song_roles

  PRIMARY_ARTIST_ROLE = "primary_artist".freeze

  scope :primary, -> { where(role: "primary_artist") }
  scope :secondary,
        -> {
          where("role NOT IN (:roles)", roles: roles.map(&:underscore))
        }

  scope :primary_featuring_or_remixer,
        -> {
          joins(
            "
      LEFT JOIN creative_song_roles ON creative_song_roles.creative_id = creatives.id
      LEFT JOIN song_roles ON song_roles.id = creative_song_roles.song_role_id
    "
          ).where("song_roles.role_type = 'remixer' OR creatives.role IN ('primary_artist', 'featuring')")
        }

  scope :without_youtube_oac,
        -> {
          joins(
            "
      LEFT JOIN external_service_ids esi ON esi.linkable_type = 'Creative'
        AND esi.linkable_id = creatives.id
        AND esi.service_name = 'youtube_channel_id'
    "
          ).where("esi.id IS NULL")
        }

  scope :by_current_user, ->(current_user) { where(person_id: current_user.id) }

  scope :by_distinct_artist,
        -> {
          joins(:artist).distinct
        }

  scope :person_artists, ->(person_id, artist_id) { where(person_id: person_id, artist_id: artist_id) }

  scope :person_by_artist_name,
        ->(person_id, artist_name) {
          joins(:artist).where(
            person_id:
                                                person_id,
            artists: {
              name: artist_name
            }
          )
        }

  scope :active, -> {
    joins(
      "inner join albums on creatives.creativeable_type = 'Album'" \
                                    " and creatives.creativeable_id = albums.id"
    )
      .joins(:artist)
      .where(albums: { takedown_at: nil, payment_applied: true },
             creatives: { role: Creative::PRIMARY_ARTIST_ROLE })
  }

  before_create :store_person

  # validate roles
  validate do |record|
    unless record.is_role_valid?
      record.errors.add(
        :role,
        I18n.t(
          "models.community.must_be_within_list",
          roles: record.class.roles.map(&:titleize).join(", ")
        )
      )
    end
  end

  validates :artist, presence: true
  validates :role, presence: true

  def self.roles
    primary_roles | secondary_roles
  end

  def self.primary_roles
    ["PrimaryArtist"]
  end

  # non-primary roles
  def self.secondary_roles
    collaboration_roles | classical_roles
  end

  def self.single_roles
    ["Featuring", "PrimaryArtist"]
  end

  def self.album_roles
    [["Featured Artist", "featuring"], ["Main Artist", "primary_artist"]]
  end

  def self.collaboration_roles
    ["Featuring", "With", "Contributor"]
  end

  def self.classical_roles
    ["Composer", "FeaturedSoloist", "Orchestra", "Choir", "Ensemble", "Conductor"]
  end

  def self.person_unique_artist_names_no_oac(person)
    by_current_user(person)
      .without_youtube_oac
      .by_distinct_artist
      .order("LOWER(artists.name)")
      .pluck("artists.name")
  end

  def apple_artist_id
    @apple_artist_id ||= get_apple_artist_id
  end

  def spotify_artist_id
    @spotify_artist_id ||= get_spotify_artist_id
  end

  def name=(artist_name)
    raise "Artist name is blank!" if artist_name.blank?

    self.artist = Artist.find_or_initialize_by(name: artist_name)
  end

  def titleize_artist
    return unless artist

    new_artist =
      if creativeable.allow_different_format?
        Artist.create_by_nontitleized_name(name)
      else
        titleized_name = Utilities::Titleizer.titleize(name)
        Artist.find_or_create_by(name: titleized_name)
      end
    self.artist = new_artist
  end

  # wrapper to signify change to self
  def titleize_artist!
    titleize_artist
  end

  def role_for_petri
    role.chomp("_artist")
  end

  def is_role_valid?
    self.class.roles.collect(&:underscore).include?(role)
  end

  def scrubbed_artist_name
    artist.scrub_artist_name(artist.name)
  end

  def is_primary?
    self.class.primary_roles.map(&:underscore).include?(role)
  end

  def self.create_role(creativeable, artist_name, role)
    unless creativeable.respond_to?(:allow_different_format?)
      raise "creativeable must respond to the :allow_different_format? call"
    end

    artist = Artist.create_by_nontitleized_name(artist_name)

    creativeable.creatives << Creative.new(artist: artist, role: role) unless creativeable.creatives.detect { |x|
                                                                                x.artist == artist && x.role == role
                                                                              }
  end

  # TODO destroy this, create active record associations for these instead
  def album
    case creativeable_type
    when "Album"
      creativeable
    when "Song"
      creativeable.try(:album)
    end
  end

  delegate :blacklisted?, to: :artist

  def get_apple_artist_id
    user = person || album&.person
    new_feature_flipper = FeatureFlipper.show_feature?(:new_apple_artist_id_creation, user)

    creative_external_service_id = external_service_id.apple_service.first
    return creative_external_service_id.get_identifier(new_feature_flipper) if creative_external_service_id.present?

    return unless album

    esi = ExternalServiceId.apple_service.by_creatives(
      album.creatives.where(role: "primary_artist", artist_id: artist.id).pluck(:id)
    ).first

    esi&.get_identifier(new_feature_flipper)
  end

  def get_spotify_artist_id
    return if artist_id.blank?

    ExternalServiceId.identifier_for_artist(self, "spotify")
  end

  def unique_main_featuring_per_album
    if creativeable_type == "Song"
      s = Song.find(creativeable_id)
      if Creative.exists?(
        creativeable_type: "Album",
        creativeable_id: s.album_id,
        artist_id: artist_id,
        role: "primary_artist"
      )

        errors.add(:base, I18n.t("songs_app.form_field_warnings.artist_name.duplicate_artist_name"))
      end
    end

    return unless creativeable_type == "Album"

    a = Album.find(creativeable_id)
    if Creative.exists?(
      creativeable_type: "Song",
      creativeable_id: a.songs.pluck(:id),
      artist_id: artist_id,
      role: "featuring"
    )

      errors.add(:base, I18n.t("songs_app.form_field_warnings.artist_name.duplicate_artist_name"))
    end
  end

  def existing_external_id_on_acct(service)
    person.creatives.joins(:external_service_ids, :artist)
          .where(artists: { name: name })
          .where(external_service_ids: { service_name: service })
          .where.not(creatives: { id: id })
          .select("external_service_ids.identifier")
          .first
      &.identifier
  end

  private

  def store_person
    self.person_id = album.try(:person_id)
  end
end
