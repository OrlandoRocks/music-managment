# frozen_string_literal: true

class PaymentsOSTransaction < ActiveRecord::Base
  belongs_to :person
  belongs_to :stored_credit_card
  belongs_to :invoice
  delegate :country_website, to: :person, allow_nil: true
  belongs_to :original_transaction, class_name: "PaymentsOSTransaction", primary_key: :id, optional: true
  has_many :refunds, class_name: "PaymentsOSTransaction", primary_key: :id, foreign_key: :original_transaction_id
  has_one :foreign_exchange_pegged_rate, through: :invoice
  has_many :payu_invoice_upload_responses

  attr_accessor :otp_redirect_url

  alias_attribute :transaction_id, :reconciliation_id
  alias_attribute :status, :charge_status

  SALE = "sale"
  REFUND = "refund"

  CREDITED = "Credited"
  SUCCEEDED = "Succeed"
  FAILED = "Failed"
  VOIDED = "Voided"
  PENDING = "Pending"
  INITIALIZED = "Initialized"
  REFUNDED = "Refunded"
  CAPTURED = "Captured"

  PAYMENT_STATUSES = [INITIALIZED, CREDITED].freeze
  CHARGE_STATUSES  = [SUCCEEDED, FAILED].freeze
  REFUND_STATUSES  = [PENDING, SUCCEEDED, FAILED].freeze
  TRANSACTION_STATUSES = PAYMENT_STATUSES | CHARGE_STATUSES | REFUND_STATUSES

  ADHOC_PAYMENT_CYCLE      = "ADHOC"
  CONSENT_TRANSACTION_TYPE = "consent_transaction"

  scope :user_transactions, ->(person) { where(person_id: person.id) }
  scope :successful_refunds, -> { where(action: REFUND).where.not(refund_status: FAILED) }

  scope :action_filter,
        ->(search_params) {
          if search_params[:action].present? && !search_params[:action].casecmp("all").zero?
            where(action: search_params[:action])
          end
        }

  scope :min_amount_filter,
        ->(search_params) {
          where(amount: search_params[:mina].to_i..Float::INFINITY) if search_params[:mina].present?
        }

  scope :max_amount_filter,
        ->(search_params) {
          where(amount: 0..search_params[:maxa].to_i) if search_params[:maxa].present? && !search_params[:maxa].zero?
        }

  scope :transaction_id_filter,
        ->(search_params) {
          if search_params[:transaction_id].present?
            where(payment_id: search_params[:transaction_id].strip)
              .or(where(charge_id: search_params[:transaction_id].strip))
              .or(where(refund_id: search_params[:transaction_id].strip))
          end
        }

  scope :from_date_filter,
        ->(search_params) {
          where(arel_table[:created_at].gteq(search_params[:from_date])) if search_params[:from_date].present?
        }

  scope :to_date_filter,
        ->(search_params) {
          where(arel_table[:created_at].lteq(search_params[:to_date])) if search_params[:to_date].present?
        }

  scope :status_filter,
        ->(search_params) {
          if [INITIALIZED, CREDITED, SUCCEEDED, FAILED, VOIDED, PENDING].include?(search_params[:payments_os_status])
            where(payment_status: search_params[:payments_os_status])
              .or(where(refund_status: search_params[:payments_os_status]))
          end
        }

  scope :person_filter,
        ->(search_params) {
          if search_params[:person_search].present?
            people = Person.arel_table
            people_join = arel_table.join(people).on(arel_table[:person_id].eq(people[:id])).join_sources
            sanitized_like_query = "%#{sanitize_sql_like(search_params[:person_search])}%"

            joins(people_join).where(people[:email].eq(search_params[:person_search]))
                              .or(joins(people_join).where(people[:name].matches(sanitized_like_query)))
          end
        }

  scope :search_transaction,
        ->(search_params) {
          action_filter(search_params)
            .transaction_id_filter(search_params)
            .from_date_filter(search_params)
            .to_date_filter(search_params)
            .person_filter(search_params)
            .max_amount_filter(search_params)
            .min_amount_filter(search_params)
            .status_filter(search_params)
            .order(created_at: :desc)
        }

  scope :for_country_website,
        ->(country_website_id) {
          joins(:person)
            .where(people: { country_website_id: country_website_id })
        }

  scope :for_country_states_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          for_country_website(country_website_id)
            .joins("left join country_states on lower(people.state) = lower(country_states.name) and people.country = '#{Country::INDIA_ISO}'")
        }

  scope :for_gst_configs_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          for_country_states_by_country_website(country_website_id)
            .joins("left join gst_configs on country_states.gst_config_id = gst_configs.id")
        }

  scope :for_gst_info_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          # we're calling for_gst_configs_by_country_website here because, while it's not strictly necessary to get gst_configs
          # to get gst_infos, they go hand in hand, and it is necessary to get people
          # since we need people for both this and for_gst_configs_by_country_website independently, in the interest of
          # avoiding duplicate joins we're including them in the same scope
          for_gst_configs_by_country_website(country_website_id)
            .joins("left join (select max(id) as gst_id, person_id from gst_infos group by person_id) gst on people.id = gst.person_id")
            .joins("left join gst_infos on gst.gst_id = gst_infos.id")
        }

  scope :for_refunds,
        -> {
          where(action: "refund")
            .where(refund_status: "succeed")
        }

  scope :within_month_of_date,
        ->(date) {
          where(updated_at: (date - 1.month)..date)
        }

  def transaction_id
    (action == SALE) ? payment_id : refund_id
  end

  def refunded?
    refunds.successful_refunds.any?
  end

  def refund_transaction?
    action == REFUND || original_transaction_id.present?
  end

  def transaction_payu_id
    charge_response = JSON.parse(charge_raw_response)
    charge_response.dig("table", "provider_data", "table", "external_id")
  end

  def refunded_amount
    refunds.successful_refunds.pluck(:amount).sum
  end

  def payment_succeeded?
    charge_status == PaymentsOSTransaction::SUCCEEDED &&
      payment_status == PaymentsOSTransaction::CREDITED
  end

  def refund_modifiable?
    action == REFUND && refund_status == PaymentsOSTransaction::PENDING
  end

  def can_refund?
    payment_succeeded? && refunded_amount < amount && !refund_transaction?
  end

  def failed?
    payment_status == PaymentsOSTransaction::FAILED || charge_status == PaymentsOSTransaction::FAILED
  end

  def success?
    charge_status == SUCCEEDED
  end

  def payment_completed?
    failed? || payment_succeeded?
  end

  def succeeded
    update(
      payment_status: PaymentsOSTransaction::CREDITED,
      charge_status: PaymentsOSTransaction::SUCCEEDED
    )
    invoice.settlement_received(self, amount)
    invoice.settled! if invoice.can_settle?
  end

  def failed
    update(
      payment_status: PaymentsOSTransaction::VOIDED,
      charge_status: PaymentsOSTransaction::FAILED
    )
  end

  def gst_config
    stored_credit_card.country_state&.gst_config
  end

  def self.process_auto_refund!(refund, amount, invoice_settlement)
    # The amount should be in dollars for refund
    amount /= 100.0

    refund_reason = refund.refund_reason
    transaction = PaymentsOS::RefundService.new(invoice_settlement.source, false).refund(amount, refund.label, refund_reason)

    if transaction.failed?
      raise AutoRefunds::Utils::SettlementFailed.new("Payment OS Refund Transaction Failed: #{transaction.refund_raw_response}")
    end

    transaction
  end
end
