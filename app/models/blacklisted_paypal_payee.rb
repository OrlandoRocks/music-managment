class BlacklistedPaypalPayee < ApplicationRecord
  validates :payee, uniqueness: true
end
