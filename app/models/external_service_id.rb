# frozen_string_literal: true

class ExternalServiceId < ApplicationRecord
  include ActiveRecordPolyfill

  SPOTIFY_SERVICE = "spotify"
  APPLE_SERVICE   = "apple"
  ITUNES_SERVICE  = "itunes"
  AMAZON_SERVICE  = "amazon"
  YOUTUBE_AUTH    = "youtube_authorization"
  YOUTUBE_CHANNEL = "youtube_channel_id"
  YOUTUBE_SERVICE = "youtube"

  NEW_ARTIST = "new_artist"

  NEW                       = "NEW"
  UNAVAILABLE               = "unavailable"
  INVALID_APPLE_IDENTIFIERS = %w[NEW UNAVAILABLE].freeze

  belongs_to :linkable, polymorphic: true
  belongs_to :artwork, class_name: "ExternalServiceIdArtwork", foreign_key: :external_service_id_artwork_id

  validates :linkable_id, :linkable_type, :service_name, presence: true
  validates :identifier, presence: true, if: proc { |s| s.linkable_type != "Creative" }
  validates :state,
            inclusion: {
              in: %w[processing requested matched did_not_match error new_artist],
              message: "%{value} is not a valid state"
            },
            allow_nil: true

  scope :spotify,         -> { where(service_name: SPOTIFY_SERVICE) }
  scope :by_type,         ->(linkable_type) { where(linkable_type: linkable_type) }
  scope :by_service,      ->(service_name) { where(service_name: service_name) }
  scope :by_creative_ids, ->(creative_ids) { joins(creative_join).where(creatives: { id: creative_ids }) }
  scope :poly_inner_join, ->(type) { joins("inner join #{type} on #{type}.id = external_service_ids.linkable_id and external_service_ids.linkable_type = '#{type.classify}'") }
  scope :select_names,    ->(type) { select("#{type}.name, external_service_ids.identifier") }
  scope :apple_service, -> { where(service_name: "apple") }
  scope :by_creatives, ->(creative_ids) { where(linkable_type: "Creative", linkable_id: creative_ids) }

  scope :artist_ids_for,
        ->(person_id, artist_id, service_name) {
          by_service(service_name).joins(creative_join)
                                  .where(creatives: {
                                           artist_id: artist_id,
                                           person_id: person_id
                                         })
        }

  scope :artist_ids_for_creative,
        ->(person_id, artist_id) {
          joins(creative_join)
            .where(creatives: {
                     artist_id: artist_id,
                     person_id: person_id
                   }).select("DISTINCT(service_name), external_service_ids.*")
        }

  scope :created_zendesk_tickets,
        -> {
          where(
            identifier: nil,
            linkable_type: "Creative",
            state: "processing"
          )
        }

  scope :by_artist_person_match,
        ->(creative) {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN creatives ON external_service_ids.linkable_id = creatives.id
      INNER JOIN artists ON artists.id = creatives.artist_id
          SQL
            .where(artists: { name: creative.artist.name })
            .where(creatives: { person_id: creative.person_id })
        }

  scope :by_artist_name_and_person_id,
        ->(artist_name, person_id) {
          joins(
            "INNER JOIN creatives ON external_service_ids.linkable_id = creatives.id
            INNER JOIN artists ON artists.id = creatives.artist_id"
          )
            .where(artists: { name: artist_name }, creatives: { person_id: person_id })
        }

  scope :artist_ids_for_albums_by_person_id_and_date,
        ->(person_id, date) {
          joins(creative_join)
            .where(creatives: {
                     person_id: person_id,
                     creativeable_type: "Album"
                   })
            .where("DATE(external_service_ids.updated_at) = ?", date)
            .where.not(identifier: nil)
        }

  scope :album_ids_by_person_id_and_date,
        ->(person_id, date) {
          joins(album_join)
            .where(albums: {
                     person_id: person_id
                   })
            .where("DATE(external_service_ids.updated_at) = ?", date)
            .where.not(identifier: nil)
        }

  def self.ids_for(type, service_name)
    t_name = type.table_name
    poly_inner_join(t_name).by_service(service_name).select_names(t_name)
  end

  def self.new_artist_id_for_service(service_name, person_id, artist_id)
    ExternalServiceId.new(linkable: Creative.where(person_id: person_id, artist_id: artist_id).first, service_name: service_name)
  end

  def requested?
    state == "requested"
  end

  def apple_url
    artist_name = linkable.name.parameterize
    "#{ENV['APPLE_ARTIST_URL_BASE']}/#{artist_name}/#{identifier}"
  end

  def spotify_url
    "#{ENV['SPOTIFY_ARTIST_URL_BASE']}/#{identifier}"
  end

  def self.identifier_for_artist(creative, service_name)
    esi = by_service(service_name)
          .where(linkable: creative).select(:identifier, :state).first

    esi&.get_identifier
  end

  def new_artist_id?
    state == "new_artist"
  end

  def get_identifier(check_new_artist = true)
    return if blank?

    return NEW if check_new_artist && new_artist_id?

    identifier if identifier != "unavailable"
  end

  def freeze_new_artist_editing?
    identifier && identifier != "unavailable"
  end

  def self.creative_join
    <<-SQL.strip_heredoc
      INNER JOIN creatives ON creatives.id = external_service_ids.linkable_id
      AND external_service_ids.linkable_type = 'Creative'
    SQL
  end

  def self.album_join
    <<-SQL.strip_heredoc
      INNER JOIN albums ON albums.id = external_service_ids.linkable_id
      AND external_service_ids.linkable_type = 'Album'
    SQL
  end
end
