# == Schema Information
# Schema version: 404
#
# Table name: invoice_items
#
#  id                       :integer(11)     not null, primary key
#  invoice_id               :integer(11)     default(0), not null
#  target_id                :integer(11)     default(0), not null
#  target_type              :string(30)      default(""), not null
#  cost_at_settlement_cents :integer(11)
#

class InvoiceItem < ApplicationRecord
  belongs_to :invoice
  belongs_to :target, polymorphic: true

  before_validation :set_currency
  validates :currency, presence: true

  private

  def set_currency
    self.currency = invoice.person.currency
  end
end
