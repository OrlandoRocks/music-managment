# frozen_string_literal: true

class ReviewAudit < ApplicationRecord
  attr_accessor :email_note

  has_and_belongs_to_many :review_reasons, join_table: :review_audits_review_reasons
  belongs_to :person
  belongs_to :album

  validates :person, :album, :event, presence: true
  validates :note, presence: { if: proc { |review_audit| review_audit.event == "FLAGGED" || review_audit.event == "NOT SURE" } }
  validates :note, length: { maximum: 255, allow_nil: true }

  # Only Content Review Manager can override a release state
  validate :authority_to_override_state
  validate :presence_of_reasons, if: proc { |review_audit| review_audit.event == "FLAGGED" || review_audit.event == "REJECTED" }

  # Reasons are assigned to roles. Verify if user has the appropriate roles to select these reasons
  validate :authority_to_choose_these_reasons
  validate :acceptable_events

  # Make sure album is valid before creating review_audit record since we need to update album's legal_review_state column
  validate :album_is_valid

  before_save :run_before_save_callbacks

  after_create :update_album_review_state

  # Notify customer if their release has been rejected for fraudulent or suspicious reasons
  after_create :notify_customer, if: proc { |review_audit| review_audit.event == "REJECTED" }
  after_create :unfinalize_if_rejected
  after_commit :notify_distribution_api_services, if: proc { |review_audit| [APPROVED, REJECTED].include?(review_audit.event) }, on: :create

  after_commit if: :approved? do
    if transaction_include_any_action?([:create])
      create_publishing_compositions
      check_tier_eligibility
    end
    if transaction_include_any_action?([:create, :update])
      create_youtube_monetizations
      create_facebook_monetizations
    end
    verify_album_transcoding
    start_distribution
    send_sns_notification
  end

  # The following are acceptable events for review_audits table event ENUM column
  ACCEPTABLE_EVENTS = ["STARTED REVIEW", "APPROVED", "FLAGGED", "REJECTED", "SKIPPED", "NOT SURE"]

  APPROVED = "APPROVED"
  FLAGGED = "FLAGGED"
  NEEDS_REVIEW = "NEEDS REVIEW"
  NOT_SURE = "NOT SURE"
  REJECTED = "REJECTED"
  SKIPPED = "SKIPPED"
  STARTED_REVIEW = "STARTED REVIEW"

  def approved?
    event == APPROVED
  end

  def acceptable_events
    return if ACCEPTABLE_EVENTS.include?(event)

    errors.add(:event, I18n.t("models.distribution.is_unacceptable_must_be_one_of_the_following_values", values: ACCEPTABLE_EVENTS.join(", ")))
  end

  def authority_to_override_state
    # Deny if user isn't a content review manager
    return unless album && !album.under_review? && person && !person.has_role?("Content Review Manager")

    errors.add(:base, I18n.t("models.distribution.you_dont_have_permission_to_mark_this_release"))
  end

  def album_is_valid
    return if album.valid?

    errors.add(:base, I18n.t("models.distribution.album_error", errors: album.errors.full_messages.each { |msg| msg }.join(". ")))
  end

  def presence_of_reasons
    errors.add(:review_reasons, I18n.t("models.distribution.cant_be_blank")) if review_reasons.empty?
  end

  # Reasons are assigned to roles. Verify if user has the appropriate roles to select these reasons
  def authority_to_choose_these_reasons
    return unless (event == "FLAGGED" || event == "REJECTED" || event == "NOT SURE") && (review_reasons && person)

    review_reasons.each do |reason|
      unless reason.roles.any? { |r| person.roles.include?(r) }
        errors.add(:base, I18n.t("models.distribution.you_dont_have_permission_to_mark_this_release", event: event.downcase, reason: reason.reason))
      end
      unless (reason.reason_type == event)
        errors.add(:event, I18n.t("models.distribution.reason_is_invalid_event", event: event, reason: reason.reason))
      end
    end
  end

  def run_before_save_callbacks
    CrtCache::Invalidator.invalidate_cache_if_changed(self)
  end

  def update_album_review_state
    return unless Album.acceptable_review_states.include?(event)

    album.legal_review_state = event
    album.save
  end

  def start_distribution
    Rails.logger.info("Sending album ID #{album.id} to Petri...")
    # DistributionCreator.create(album, "BelieveLiv", converter_class: "MusicStores::Believe::Converter", delivery_type: "full_delivery" )
    # DistributionCreator.create(album, "Streaming",  converter_class: "MusicStores::Streaming::Converter", delivery_type: "full_delivery")

    Distribution::OrphanedSalepointQueryBuilder.new(album).build.each do |salepoint|
      DistributionCreator.create(album, salepoint.store.short_name)
    end

    statuses = ["new", "pending_approval"]
    PetriBundle.for(album).distributions.select { |distribution|
      statuses.include?(distribution.state) &&
        distribution.converter_class != "MusicStores::YoutubeSr::Converter" &&
        distribution.valid_for_distribution?
    }.each do |distribution|
      distribution.start(
        actor: "Content Review by #{person.name}",
        message: "Distribution enqueued for delivery to #{distribution.salepoints.map(&:store).map(&:name).join(', ')}"
      )
    end
  end

  def send_sns_notification
    return unless FeatureFlipper.show_feature?(:use_sns, album.person)

    Sns::Notifier.perform(
      topic_arn: ENV["SNS_RELEASE_NEW_TOPIC"],
      album_ids_or_upcs: [album.id],
      store_ids: album.salepoints.pluck(:store_id) - Store::TRACK_MONETIZATION_STORES,
      person_id: person.id
    )
  end

  def create_youtube_monetizations
    return unless album.youtube_monetizeable?

    store = Store.find_by(id: Store::YTSR_STORE_ID)
    logger.info "Sidekiq Job initiated TrackMonetization::CreationService review_audit: #{album.id} -> #{store.id}"
    TrackMonetization::YoutubePostApprovalWorker.perform_in(3.hours.from_now, album.id, store.id)
  end

  def create_facebook_monetizations
    if album.fb_reels_salepoint? && FeatureFlipper.show_feature?(:freemium_flow, album.person)
      monetize_tracks_on_fb
    elsif album.person.has_active_subscription_product_for?("FBTracks")
      store = Store.find_by(short_name: "FBTracks")
      logger.info "Sidekiq Job initiated TrackMonetization::PostApprovalWorker review_audit: #{album.id} -> #{store.id}"
      TrackMonetization::PostApprovalWorker.perform_async(album.id, store.id)
    end
  end

  def check_tier_eligibility
    RewardSystem::Events.release_approved(album.person.id) if album.eligible_for_tier_check?
  end

  def create_publishing_compositions
    PublishingAdministration::PostApprovalWorker.perform_async(album.id) if create_publishing_compositions_permitted?
  end

  def notify_customer
    RejectionEmailService.new(self).notify
  end

  def notify_distribution_api_services
    return unless album&.distribution_api_service&.bytedance?

    Bytedance::ApiClientWorker.perform_async(Bytedance::Requests::ContentReview, id, album.id)
  end

  def unfinalize_if_rejected
    return unless event == "REJECTED" && review_reasons.any?(&:should_unfinalize)

    album.unfinalize(message: "Auto-unfinalized due to rejection", person: person)
  end

  def verify_album_transcoding
    return unless album.songs.any? { |song| !song.duration_in_seconds || !song.s3_flac_asset_id }

    Airbrake.notify("[GS-9206]: Album #{album.id} missing Transcoded Flac Assets, attempting subsequent attempt")
    album.transcode_assets
  end

  private

  def create_publishing_compositions_permitted?
    album.person&.publishing_composers&.any? && album.person.publishing_composers.primary.all?(&:is_paid) &&
      !album.person.publishing_composers.primary.first.try(:has_fully_terminated_composers?)
  end

  def monetize_tracks_on_fb
    TrackMonetization::FbPostApprovalWorker.perform_async(album.id, Store::FBTRACKS_STORE_ID)
  end
end
