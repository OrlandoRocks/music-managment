class SoundoutReport < ApplicationRecord
  include Jsonable

  belongs_to :track, polymorphic: true
  belongs_to :soundout_product
  belongs_to :person

  has_one :soundout_report_data
  has_one :purchase, as: :related

  validate :require_90_second_song, on: :create
  validate :is_album_distributed_to_streaming?, on: :create
  validates :person, :track, presence: true

  scope :requested, -> { where(status: "requested") }
  scope :received, -> { where(status: "received") }

  delegate :url,                          to: :soundout_report_data, allow_nil: true
  delegate :artists_mentioned,            to: :soundout_report_data, allow_nil: true
  delegate :genre_positioning,            to: :soundout_report_data, allow_nil: true
  delegate :has?,                         to: :soundout_report_data, allow_nil: true
  delegate :report_data,                  to: :soundout_report_data, allow_nil: true
  delegate :report_data_hash,             to: :soundout_report_data, allow_nil: true
  delegate :market_potential,             to: :soundout_report_data, allow_nil: true
  delegate :market_potential,             to: :soundout_report_data, allow_nil: true
  delegate :market_potential_genre,       to: :soundout_report_data, allow_nil: true
  delegate :market_pot_age_groups,        to: :soundout_report_data, allow_nil: true
  delegate :market_pot_age_genre_groups,  to: :soundout_report_data, allow_nil: true
  delegate :track_rating,                 to: :soundout_report_data, allow_nil: true
  delegate :passion_rating,               to: :soundout_report_data, allow_nil: true
  delegate :in_genre_class,               to: :soundout_report_data, allow_nil: true
  delegate :rating_distribution_data,     to: :soundout_report_data, allow_nil: true
  delegate :sample_age_groups,            to: :soundout_report_data, allow_nil: true
  delegate :sample_group_male_percent,    to: :soundout_report_data, allow_nil: true
  delegate :sample_group_female_percent,  to: :soundout_report_data, allow_nil: true
  delegate :search_and_filter_reviews,    to: :soundout_report_data, allow_nil: true
  delegate :song_analysis,                to: :soundout_report_data, allow_nil: true
  delegate :track_positioning,            to: :soundout_report_data, allow_nil: true
  delegate :track_appeal_preference,      to: :soundout_report_data, allow_nil: true
  delegate :track_appeal_no_preference,   to: :soundout_report_data, allow_nil: true
  delegate :track_positioning_major_releases, to: :soundout_report_data, allow_nil: true
  delegate :word_cloud, to: :soundout_report_data, allow_nil: true

  def paid?
    purchase and purchase.paid_at.present?
  end

  def paid_post_proc
    update({ status: "paid" })

    if track.is_a?(Song)
      Soundout::Transcoder.new(soundout_report: self).run
    else
      Soundout::OrderWorker.perform_async(id)
    end
  end

  def name
    "TuneCore Fan Reviews: #{track.name}"
  end

  def canceled?
    canceled_at.present?
  end

  def errored?
    status == "error"
  end

  def requested?
    status == "requested"
  end

  def received?
    status == "received"
  end

  def available?
    status == "available"
  end

  def mark_as_available
    update_attribute(:status, "available")
  end

  #
  # Marks the report as canceled or not canceled
  #
  def toggle_cancel!
    if canceled?
      update!(canceled_at: nil)
    else
      update!(canceled_at: Time.now)
    end
  end

  #
  # Processes an soundout product order
  #
  def process_order(soundout_id)
    if soundout_id && paid?
      Rails.logger.info("Successfully processed order of soundout_report: #{id}")
      update(
        {
          status: "requested",
          soundout_id: soundout_id,
          report_requested_tmsp: Time.now
        }
      )
    else
      Rails.logger.info("Error processing order of soundout_report: #{id}")
      update_attribute(:status, "error")
    end

    soundout_id
  end

  #
  # Processes a report being fetched
  #
  def process_report_fetch(success, report_data)
    self.retrieval_attempts ||= 0
    update_attribute(:retrieval_attempts, retrieval_attempts + 1)

    send_customer_communications = false

    transaction do
      if success
        send_customer_communications = true if !received? && !canceled?

        Rails.logger.info("Successfully downloaded data for report #{id}")
        self.report_data          = report_data
        self.report_received_tmsp = Time.now

        # Make sure we could parse the data out
        if market_potential.present?
          self.status = "received"
          Rails.logger.info("Soundout report process fetch: success")
        else
          self.status = "error"
          Rails.logger.error("Soundout report process fetch: could not parse soundout json or json incomplete")
        end
      else
        self.status      = "error"
        self.report_data = report_data
        Rails.logger.error("Report #{id} (SoundOut ReportId: #{soundout_id}) not available for #{self.retrieval_attempts} days after expected delivery!")
      end

      save!

      # Send out customer communications if its now received and it wasn't already received or canceled
      if received? && send_customer_communications
        SoundoutReportNotification.create(notification_item: self)
        SoundoutMailer.report_available(self).deliver
      end
    end

    report_data
  end

  #
  # Create or update the report data model with the json
  #
  def report_data=(json)
    if soundout_report_data.blank?
      build_soundout_report_data(report_data: json)
      soundout_report_data.save unless new_record?
    else
      soundout_report_data.report_data = json
      soundout_report_data.save
    end
  end

  #
  # Requires the song to have a bigbox asset with a duration of 90+ seconds
  #
  def require_90_second_song
    if track.is_a?(Song) && track.s3_asset.blank?
      errors.add(:base, I18n.t("models.reporting.cannot_determine_song_duration", track_name: track.name))
    elsif (track.is_a?(Song) && (track.duration.blank? || !track.longer_than_90s)) || (track.is_a?(SongLibraryUpload) && !track.longer_than_90s)
      errors.add(:base, I18n.t("models.reporting.must_be_90_or_more_seconds_long", track_name: track.name))
    end
  end

  #
  # Requires the album to be finalized and contain "streaming" distribution to be delivered
  #
  def is_album_distributed_to_streaming?
    return unless track.is_a?(Song) && track.album && !track.album.has_been_distributed?

    errors.add(:base, "The album for the song #{track.name} must be distributed")
  end

  def as_json(options = {})
    new_attributes = {
      artist_name: track.artist_name,
      song_name: track.name,
      report_type: soundout_product.report_type
    }

    if soundout_report_data
      new_attributes[:market_potential] = soundout_report_data.market_potential
      new_attributes[:track_rating] = soundout_report_data.track_rating
    end

    new_attributes[:artwork_url] =
      if track.is_a?(Song)
        {
          small_url: track.album.artwork.url(:small),
          medium_url: track.album.artwork.url(:medium),
          large_url: track.album.artwork.url(:large)
        }
      else
        { small_url: nil, medium_url: nil, large_url: nil }
      end

    merge_attributes_as_json(new_attributes, options)
  end
end
