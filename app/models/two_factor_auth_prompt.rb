class TwoFactorAuthPrompt < ApplicationRecord
  belongs_to :person
  has_one :two_factor_auth, through: :person

  PROMPT_TIMEOUT = 1.hour.freeze
  PROMPT_DISMISS_LENGTH = 30.days.freeze

  def active?
    return false if two_factor_auth.try(:active?)

    prompt_at.nil? || prompt_at <= PROMPT_TIMEOUT.ago
  end

  def dismiss
    update(
      dismissed_count: dismissed_count + 1,
      prompt_at: PROMPT_DISMISS_LENGTH.from_now
    )
  end

  def never_prompted?
    prompt_at.nil?
  end
end
