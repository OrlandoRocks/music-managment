class Background < ApplicationRecord
  has_many :covers
  has_many :background_genres
  has_many :genres, through: :background_genres

  validates :name, :url, presence: true

  SIZES = {
    t100: "100x100",
    t300: "300x300",
    t1600: "1600x1600"
  }

  BACKGROUND_ASSET_PATH = File.join(Rails.root, "public", "cover-images")

  def full_name
    name + ".jpg"
  end

  def rendered_path(size)
    size = :t300 unless SIZES.include?(size)
    File.join(BACKGROUND_ASSET_PATH, SIZES[size])
  end

  def rendered_background_filename(size)
    File.join(rendered_path(size), full_name)
  end

  def rendered_at(size)
    rendered_background_filename(size)
  end
end
