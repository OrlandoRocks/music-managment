# = Description
# The ReportDataTable takes the returned recordset from the purchase_report and transforms it into
# an array of PurchaseReportData objects which can be iterated through and accessed in views and partials.
#
# = Usage
#
#
# = Change Log
# [2010-02-06 -- CH]
# Created Class
#
# [2010-03-15 -- CH]
# Added data_field_name option to provide a different way of getting data out of raw_data.  Allows selection of headers other than resolution.

class ReportDataTable
  require "logger"
  attr_accessor :rows, :headers, :raw_data, :fields, :raw_headers
  attr_accessor :row_title_included, :include_total_and_avg, :data_field_name
  attr_accessor :headers_excluded, :data_is_flat

  def initialize(options)
    self.rows                   = []
    self.raw_headers            = options[:headers]
    self.raw_data               = options[:raw_data]
    self.fields                 = options[:fields]
    self.row_title_included     = options[:include_row_title] || false
    self.include_total_and_avg  = options[:include_total_and_avg] || false
    self.data_field_name        = options[:data_field_name] || "resolution"
    self.data_is_flat           = options[:data_is_flat] || false

    add_header_row

    if data_is_flat == false
      add_rows
    else
      add_flat_rows
    end
  end

  def row_title_included?
    row_title_included
  end

  def include_total_and_avg?
    include_total_and_avg
  end

  private

  def add_header_row
    self.headers = raw_headers
    add_header_padding
    add_total_and_avg_headers
    self.headers = headers.flatten
  end

  def add_flat_rows
    title_field = fields.select { |f| f[:is_title] && f[:is_title] == true }
    title_field = title_field[0] unless title_field.empty?
    data_fields = fields.select { |f| !f[:is_title] || (f[:is_title] && f[:is_title] == false) }

    raw_data.each do |flat_data|
      if title_field
        row_title   = flat_data.send(title_field[:field_name])
        show_title  = true
      else
        row_title   = nil
        show_title  = false
      end
      row = ReportDataRow.new(
        data_type: nil,
        row_title: row_title,
        show_average: false,
        show_title: show_title,
        show_total: false,
        highlight: false
      )

      data_fields.each do |df|
        row.add_cell(flat_data.send(df[:field_name]), df[:data_type])
      end

      rows << row
    end
  end

  def add_row(row_title, data, field)
    row = ReportDataRow.new(
      data_type: field[:data_type],
      row_title: row_title,
      show_average: field[:show_average],
      show_title: row_title_included,
      show_total: field[:show_total],
      highlight: field[:highlight]
    )

    raw_headers.each do |header|
      row_data = data.find { |d| d.send(data_field_name) == header }
      if row_data.blank?
        row.add_cell(0, field[:data_type])
      else
        row.add_cell(row_data.send(field[:field_name]), field[:data_type])
      end
    end

    row.compute_total_and_average
    row.flatten_data

    rows << row
  end

  def add_rows
    if raw_data.is_a?(Hash)
      raw_data.each do |id, data|
        add_row(id, data, fields.first)
      end
    else
      fields.each do |field|
        add_row(field[:title], raw_data, field)
      end
    end
  end

  def add_footer
    data << row
  end

  def add_row_title(row_title, row)
    row = [row_title] + row if row_title_included
  end

  def add_header_padding
    self.headers = [""] + headers if row_title_included
  end

  def add_total_and_avg_headers
    self.headers = headers << ["Total", "Avg"] if include_total_and_avg
  end
end
