# frozen_string_literal: true

class PlanAddon < ApplicationRecord
  belongs_to :person
  has_many :purchases, as: :related, dependent: :destroy

  validates :person, :addon_type, presence: true

  ADDITIONAL_ARTIST = "additional_artist"
  AUTOMATOR = "store_automator"
  SPLITS_COLLABORATOR = "splits_collaborator"

  scope :additional_artists, -> { where(addon_type: ADDITIONAL_ARTIST) }
  scope :active, -> { where(canceled_at: nil) }
  scope :paid, -> { where.not(paid_at: nil) }
  scope :unpaid, -> { where(paid_at: nil) }
  scope :purchase_paid, -> {
    joins(:purchases)
      .where(<<~SQL)
        purchases.created_at = (
          SELECT MAX(purchases.created_at)
          FROM purchases
          WHERE purchases.related_id = plan_addons.id
            AND purchases.related_type = 'PlanAddon'
        )
      SQL
      .where.not(purchases: { paid_at: nil })
  }
  scope :purchase_unpaid, -> {
    joins(:purchases)
      .where(<<~SQL)
        purchases.created_at = (
          SELECT MAX(purchases.created_at)
          FROM purchases
          WHERE purchases.related_id = plan_addons.id
            AND purchases.related_type = 'PlanAddon'
        )
      SQL
      .where(purchases: { paid_at: nil })
  }

  enum addon_type: {
    additional_artist: ADDITIONAL_ARTIST,
    automator: AUTOMATOR,
    splits_collaborator: SPLITS_COLLABORATOR
  }

  def additional_artist?
    addon_type == ADDITIONAL_ARTIST
  end

  def renew!(purchase)
    # TODO: ECOM-725 🏂 Cannot complete a purchase without this scaffolding
    # want to ensure renewal date matches plan renewal date
    # create_renewal_history
  end

  def mark_as_paid
    update!(paid_at: Time.current)
  end

  def destroy_unpaid
    transaction do
      destroy_unpaid_purchases
      # looking at paid_at seems the most efficient way to determine if a
      # plan_addon is associated with paid purchases, but if it later
      # turns out that there are discrepancies, we may need to look at
      # purchases.length > 0 instead
      destroy if paid_at.nil?
    end
  end

  private

  def destroy_unpaid_purchases
    purchases.unpaid.not_in_invoice.destroy_all
  end
end
