class EftBatch < ApplicationRecord
  has_many :eft_batch_transactions
  belongs_to :country_website

  before_validation :assign_country_website_id, on: :create
  before_validation :set_currency, on: :create

  validates :currency, :country_website_id, presence: true

  STATUSES = ["complete", "uploaded", "new", "upload_confirmed"]

  WITHDRAWAL_BATCH_RECIPIENTS = ["report_eft@tunecore.com"]

  # SELECT_FOR_SUMMARY = "eft_batches.id, eft_batches.created_at,
  #   COUNT(ebt.id) AS total_count,
  #   SUM(ebt.amount) AS total_amount,
  #   SUM(CASE WHEN ebt.status = 'error' THEN ebt.amount END) AS error_amount,
  #   SUM(CASE WHEN ebt.status = 'failure' THEN ebt.amount END) AS failure_amount,
  #   SUM(CASE WHEN ebt.status = 'success' THEN ebt.amount END) AS success_amount,
  #   SUM(CASE WHEN ebt.status IN ('sent_to_bank','processing_in_batch') THEN ebt.amount END) AS processing_amount,
  #   COUNT(CASE WHEN ebt.status = 'error' THEN ebt.id END) AS error_count,
  #   COUNT(CASE WHEN ebt.status = 'failure' THEN ebt.id END) AS failure_count,
  #   COUNT(CASE WHEN ebt.status = 'success' THEN ebt.id END) AS success_count,
  #   COUNT(CASE WHEN ebt.status IN ('sent_to_bank','processing_in_batch') THEN ebt.id END) AS processing_count"

  SELECT_FOR_SUMMARY = "
    COUNT(ebt.id) AS total_count,
    COUNT(CASE WHEN ebt.status = 'failure' THEN ebt.id END) AS failure_count,
    COUNT(CASE WHEN ebt.status = 'success' THEN ebt.id END) AS success_count,
    COUNT(CASE WHEN ebt.status IN ('sent_to_bank','processing_in_batch') THEN ebt.id END) AS processing_count"

  scope :summary,
        -> {
          select(SELECT_FOR_SUMMARY)
            .joins("LEFT JOIN eft_batch_transactions ebt ON eft_batches.id = ebt.eft_batch_id")
            .group("eft_batches.id")
        }

  scope :uploaded, -> { where(status: "uploaded") }
  scope :unprocessed, -> { where(response_processed_at: nil) }
  scope :sent_last_30_days,
        -> {
          where("batch_sent_at between ? and ?", Date.today - 30, Date.today)
        }

  attr_accessor :total_count, :failure_count, :success_count, :processing_count

  def self.upload_confirmed_batch_dates
    where(status: "upload_confirmed").map(&:batch_sent_at).compact.map(&:to_date).uniq
  end

  def completed?
    (eft_batch_transactions.reload.collect(&:status) & EftBatchTransaction::PROCESSING_STATUSES).empty?
  end

  private

  def assign_country_website_id
    self.country_website_id = 1 # hardcoding to 1 since we are currently only using EFT for US Customers
  end

  def set_currency
    self.currency = country_website.currency # hardcoding to 'USD' since we are currently only using EFT for US Customers
  end
end
