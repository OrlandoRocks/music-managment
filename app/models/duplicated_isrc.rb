# == Schema Information
# Schema version: 404
#
# Table name: duplicated_isrcs
#
#  id            :integer(11)     not null, primary key
#  created_at    :datetime        not null
#  song_id       :integer(11)     default(0), not null
#  tunecore_isrc :string(12)      default(""), not null
#  corrected_at  :datetime
#  adjusted_isrc :string(12)
#  tunecore_upc  :string(32)
#  optional_upc  :string(32)
#

class DuplicatedIsrc < ApplicationRecord
  belongs_to :song
  has_many :apple_identifiers

  def corrected?
    corrected_at.present?
  end

  def store_apple_id(apple_id)
    self.class.transaction do
      return if has_apple_id? apple_id

      new_identifier = apple_identifiers.build(apple_id: apple_id)
      new_identifier.save!
    end
  end

  def has_apple_id?(possible)
    apple_ids.include? possible
  end

  def apple_ids
    apple_identifiers.collect(&:apple_id)
  end

  def matches_disambiguation?(disambig_string)
    return has_apple_id?(disambig_string) if apple_disambiguation?(disambig_string)

    disrc, dupc = disambig_string.split(disambiguation_separator)
    (
      disrc == tunecore_isrc &&
      (dupc.gsub(/^0+/, "") == tunecore_upc.gsub(/^0+/, "") || dupc.gsub(/^0+/, "") == optional_upc.gsub(/^0+/, ""))
    )
  end

  def self.apple_disambiguation?(disambig_string)
    !disambig_string.include? disambiguation_separator
  end

  def self.disambiguation_separator
    "|"
  end

  delegate :apple_disambiguation?, :disambiguation_separator, to: self

  # a console-only method for generating the list of duplicates.
  # safe to run repeatedly as it will exclude any duplicates already
  # recorded.
  def self.populate
    transaction do
      with_context do
        affected_ids = affected_song_ids

        affected_songs = Song.where(id: affected_ids - recorded_song_ids).order("tunecore_isrc, id")
        # build up an album cache
        Album.find(affected_songs.collect(&:album_id).uniq)

        affected_songs.each do |song|
          songalbum = song.album or raise "ERROR: song has no album #{song.id}"
          create! song_id: song.id,
                  tunecore_isrc: song.tunecore_isrc,
                  tunecore_upc: songalbum.tunecore_upc,
                  optional_upc: songalbum.optional_upc
        end
        affected_songs.length
      end
    end
  end

  def self.affected_song_ids
    Song.select("id").where("tunecore_isrc is not null and tunecore_isrc in (?)", isrc_duplicates_in_songs).map(&:id)
  end

  # return just the list of problem tunecore_isrc values
  def self.recorded_isrcs
    select("tunecore_isrc").map(&:tunecore_isrc).uniq
  end

  def self.recorded_song_ids
    all.map(&:song_id)
  end

  def self.uncorrected_song_ids
    select("song_id").where("corrected_at is null").map(&:song_id)
  end

  def self.corrected_song_ids
    select("song_id").where("corrected_at is not null").map(&:song_id)
  end

  def self.isrc_duplicates_in_songs
    Song.select("tunecore_isrc").where("tunecore_isrc is not null").group("tunecore_isrc having count(*) > 1").map(&:tunecore_isrc)
  end

  def self.correct_unpublished
    transaction do
      with_context do
        records = where("corrected_at is null")

        # build up an album cache
        Album.find(Song.find(records.collect(&:song_id).uniq).collect(&:album_id).uniq)
        # these album ids are for non-finalized, yet previously finalized albums
        dangerous_album_ids = Album.select("id").where("finalized_at is null").joins("inner join bundles on bundles.album_id = albums.id").map(&:id).uniq

        dangerous_album_ids |= Album.select("id").where("salepoints.finalized_at is not null and albums.finalized_at is null").joins("inner join salepoints on salepoints.album_id = albums.id").map(&:id).uniq

        work_items = records.group_by(&:tunecore_isrc)
        raise "strange, not everything is a pair" unless work_items.all? { |_k, v| v.length == 2 }

        work_items.each do |_isrc, dups|
          dups.each do |dup|
            next if dup.song.album.finalized? || dup.corrected?

            if dangerous_album_ids.include? dup.song.album.id
              puts "song #{dup.song.id} - album not finalized, but it's been published! #{dup.song.album.id}"
              next
            end
            old_isrc = dup.song.tunecore_isrc
            dup.song.send(:make_tunecore_isrc, true)
            raise "not finalized but no change!!!" if old_isrc == dup.song.tunecore_isrc

            dup.adjusted_isrc = dup.song.tunecore_isrc
            now = Time.now
            raise "I couldn't fix them both!" unless dups.all? { |x| x.update corrected_at: now }
          end
        end
        nil
      end
    end
  end

  def self.populate_apple_data(from_file)
    apple_structs = load_apple_data(from_file)
    by_isrc = apple_structs.group_by { |x| x[:isrc] }

    work_items = includes(:apple_identifiers)

    related_songs = Song.find(work_items.collect(&:song_id).uniq).index_by(&:id)
    related_albums = Album.find(related_songs.values.collect(&:album_id).uniq).index_by(&:id)
    work_items.each do |work_item|
      # maybe apple knows it by optional isrc, grab via that too
      candidates = by_isrc[work_item.tunecore_isrc] || by_isrc[related_songs[work_item.song_id].optional_isrc]
      if candidates.blank?
        puts "skipping duplicated_isrcs row for song id that apple doesn't seem to know about: #{work_item.song_id}"
        next
      end

      puts "#{work_item.id} has more than 2 candidates - #{candidates.inspect}" if candidates.length > 2

      candidates.each do |candidate|
        next unless candidate[:upc] == work_item.tunecore_upc || candidate[:upc] == work_item.optional_upc ||
                    candidate[:upc].gsub(/^0+/, "") == work_item.tunecore_upc.gsub(/^0+/, "") ||
                    candidate[:upc].gsub(/^0+/, "") == work_item.optional_upc.gsub(/^0+/, "")

        puts "setting apple id for duplicated_isrc #{work_item.id}"
        work_item.store_apple_id(candidate[:apple_id])
      end
    end
    puts "done, but the ruby VM may take some time to finish, this was hard work :-)"
    nil
  end

  def self.load_apple_data(from_file)
    puts "reading input file #{from_file} - this will take a while"
    return_value = []
    oline = []
    capture = false
    IO.foreach(from_file) do |line|
      if capture
        oline << line.strip
        capture = false
      end
      case line
      when /^<tr/
        next if oline.empty?
        raise "Malformed input, expecting 10 columns but got #{oline.length}" if oline.length != 10
        if oline[1] != oline[2]
          raise "Malformed input, expecting isrc and vendor_id to match but got #{oline[1]}/#{oline[2]}"
        end

        if oline[7] != oline[8] && (/#{oline[7]}$/ =~ oline[8]).nil?
          puts "Malformed input, expecting upc and vendor_id to match but got #{oline[7]}/#{oline[8]}"
        end
        #### STORE IT
        return_value << { apple_id: oline[0], isrc: oline[2], upc: oline[8] }
        if (return_value.size % 50).zero?
          GC.start
          STDERR << "."
        end
        oline = []
      when /^<td/
        capture = true
        next
      end
    end
    puts ""
    puts "finished reading input file"
    return_value
  end
end
