# = Description
# RelatedProduct relates internal products to other products so they can be presented at the time of checkout
# for addition to the cart.
#
# = Usage
# NOTE: related_for_purchases and list_of_related should be accessed from the Product model because they return a list of Products in an array
#
# = Change Log
# [2010-05-17 -- CH]
# Created Model

class RelatedProduct < ApplicationRecord
  belongs_to :product
  belongs_to :related_product, class_name: "Product"
  belongs_to :created_by, class_name: ""

  validates :product, :related_product, presence: true

  def self.related_for_purchases(person, purchases, number_of_products_to_display)
    product_ids = []
    purchases.map { |pu| product_ids << [pu.product_id] }
    list_of_related(person, product_ids.flatten.uniq, number_of_products_to_display)
  end

  def self.list_of_related(person, product_list, number_of_products_to_display)
    related_products = where(
      "product_id IN (:product_list) and related_product_id NOT IN (:product_list) and start_date <= NOW() AND NOW() <= expiration_date",
      { product_list: product_list }
    )
                       .order("product_id, sort_order")
                       .includes(:related_product)
                       .group("related_product_id")

    display_list = []
    related_products.each_with_index do |rp, index|
      display_list << rp.product_for_display(person)
      break if (index + 1) == number_of_products_to_display
    end
    display_list
  end

  def active?
    (start_date >= Time.now && Time.now <= expiration_date)
  end

  # returns the product for upsell and inclusion in the returned product list
  def product_for_display(person)
    related = related_product.display(person)

    related.display_name = display_name if display_name.present?

    related.description = description if description.present?

    related
  end
end
