class PaperAgreement < ApplicationRecord
  belongs_to :related, polymorphic: true
end
