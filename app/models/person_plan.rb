# frozen_string_literal: true

class PersonPlan < ApplicationRecord
  validates :person, :plan, presence: true
  validates :person, uniqueness: true

  belongs_to :person
  belongs_to :plan

  has_many :renewal_items, as: :related, dependent: :destroy
  has_many :renewals, through: :renewal_items
  has_many :renewal_histories, through: :renewals

  delegate :name, to: :plan
  delegate :plan_renewals, to: :person

  def renewal_expires_at
    renewal = plan_renewals.last
    # checking for presence to account for before person_plan is paid and renewal created
    renewal.present? ? renewal.expires_at : nil
  end

  def expired?
    return false if renewal_histories.empty?

    renewal_histories.last.expires_at <= Time.current.beginning_of_day
  end

  def renew!(purchase)
    plan_product_item = ProductItem.find_by(product: purchase.product)
    find_or_create_renewal_record(plan_product_item, purchase)
  end

  def active_plan_on_datetime(datetime)
    PersonPlanHistory.where(created_at: Time.at(0).to_datetime..datetime, person_id: person.id).last&.plan
  end

  def self.plan_eligible_for_user?(person, plan)
    return true if person.plan.nil?

    person.plan.upgrade_eligible?(plan.id)
  end

  def self.downgrade?(person, plan)
    return false if person.plan.nil?

    !person.plan.upgrade_eligible?(plan.id) && person.plan != plan
  end

  private

  def find_or_create_renewal_record(item, purchase)
    ApplicationRecord.transaction do
      renewal = Renewal.find_or_initialize_by(
        person: purchase.person,
        item_to_renew_id: item.id,
        item_to_renew_type: "ProductItem",
        canceled_at: nil,
        takedown_at: nil
      )

      # if the id is present, this is being called from the renewal flow for the same product_item
      # if the id is not present, this is an upgrade, and we need to cancel the renewals for the lower plans before
      # saving the upgrade_renewal
      return if renewal.id.present?

      cancel_existing_renewals!

      renewal.update(purchase: purchase, renewed_items: self)
    end
  end

  def cancel_existing_renewals!
    renewals.each(&:cancel!)
  end
end
