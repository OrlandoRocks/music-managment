class PersonAnalytic < ApplicationRecord
  belongs_to :person
  validates :person, :metric_name, presence: true
  validates :person_id, uniqueness: { scope: :metric_name, message: "should only have 1 unique metric_name per person" }
  validate :metric_name_is_available

  METRIC_NAMES = ["first_distribution_invoice_id", "first_invoice_id", "publishing_invoice_id", "first_artist_service_invoice_id", "first_non_distro_invoice_id", "first_distro_add_on_invoice_id"]

  def self.calculate_metric_if_empty(metric_name, person_id)
    return if PersonAnalytic.find_by(metric_name: metric_name, person_id: person_id).present?

    send("calculate_#{metric_name}", person_id)
  end

  def self.calculate_first_invoice_id(person_id)
    first_invoice = Invoice.where("settled_at is not null and person_id = ?", person_id).order("settled_at ASC").limit(1)
    return unless first_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "first_invoice_id", metric_value: first_invoice.first.id })
  end

  def self.calculate_publishing_invoice_id(person_id)
    publishing_invoice = Invoice.joins(purchases: :product).where("settled_at is not null and invoices.person_id = ? and products.product_family = 'Publishing'", person_id).order("settled_at DESC").limit(1)
    return unless publishing_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "publishing_invoice_id", metric_value: publishing_invoice.first.id })
  end

  def self.calculate_first_distribution_invoice_id(person_id)
    first_distribution_invoice = Invoice.joins(purchases: :product).where("settled_at is not null and invoices.person_id = ? and products.product_family = 'Distribution'", person_id).order("settled_at ASC").limit(1)
    return unless first_distribution_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "first_distribution_invoice_id", metric_value: first_distribution_invoice.first.id })
  end

  def self.calculate_first_artist_service_invoice_id(person_id)
    first_artist_service_invoice = Invoice.joins(purchases: :product).where("settled_at is not null and invoices.person_id = ? and products.product_family = 'Artist Services (post-distribution)'", person_id).order("settled_at ASC").limit(1)
    return unless first_artist_service_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "first_artist_service_invoice_id", metric_value: first_artist_service_invoice.first.id })
  end

  def self.calculate_first_non_distro_invoice_id(person_id)
    first_non_distro_invoice = Invoice.joins(purchases: :product).where("settled_at is not null and invoices.person_id = ? and products.product_family = 'Artist Services (non-distribution)'", person_id).order("settled_at ASC").limit(1)
    return unless first_non_distro_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "first_non_distro_invoice_id", metric_value: first_non_distro_invoice.first.id })
  end

  def self.calculate_first_distro_add_on_invoice_id(person_id)
    first_distro_add_on_invoice = Invoice.joins(purchases: :product).where("settled_at is not null and invoices.person_id = ? and products.product_family = 'Distribution Add-Ons'", person_id).order("settled_at ASC").limit(1)
    return unless first_distro_add_on_invoice.length.positive?

    PersonAnalytic.create({ person_id: person_id, metric_name: "first_distro_add_on_invoice_id", metric_value: first_distro_add_on_invoice.first.id })
  end

  def self.update_analytics_for_invoice(invoice_id)
    invoice = Invoice.find(invoice_id)
    METRIC_NAMES.each { |metric| calculate_metric_if_empty(metric, invoice.person_id) }
  end

  private

  def metric_name_is_available
    errors.add(:metric_name, "Not a valid metric name") unless METRIC_NAMES.include?(metric_name)
  end
end
