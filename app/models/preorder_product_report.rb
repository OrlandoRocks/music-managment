class PreorderProductReport < ReportBase
  attr_accessor :product, :country_website_id, :preorder_type

  def initialize(options)
    self.country_website_id = options[:country_website_id]
    self.product = preorder_product_for_country
    self.preorder_type = options[:preorder_type]
    if !preorder_type
      self.fields = [{ title: "Total Purchases", field_name: :total_purchased, data_type: "integer" }]
    else
      self.fields = [
        { title: "Total Purchases", field_name: :total_purchased, data_type: "integer" },
        { title: "Total Spent", field_name: :total_cost, data_type: "currency" },
        { title: "Total Discounts", field_name: :total_discounts, data_type: "currency" },
        { title: "Average Cost", field_name: :average_cost, data_type: "currency", show_total: false },
        { title: "Average Discount", field_name: :average_discount, data_type: "currency", show_total: false },
        { title: "Average Paid", field_name: :average_paid, data_type: "currency", show_total: false }
      ]
    end
    super
  end

  def stored_product_for(_id)
    product
  end

  protected

  def build_condition_array
    make_date_conditions
    ["pu.paid_at >= ? and pu.paid_at <= ? and products.id = ? and country_website_id = ?", start_date, end_date, product.id, country_website_id]
  end

  def retrieve_data
    Product.select(select_string).joins(join_string).where(build_condition_array).order(order_string).group(group_by_string).having(having_string)
  end

  def preorder_product_for_country
    Product.find_by!(
      display_name: "preorder",
      status: "Active",
      country_website_id: country_website_id
    )
  end

  def select_string
    %Q(products.id,
    products.name,
    count(*) as total_purchased,
    TRUNCATE(sum(cost_cents)/100,2) as total_cost,
    TRUNCATE(sum(discount_cents)/100,2) as total_discounts,
    TRUNCATE((sum(cost_cents) - sum(discount_cents))/100, 2) as cost_after_discount,
    TRUNCATE(AVG(cost_cents)/100,2) as average_cost,
    TRUNCATE(AVG(discount_cents)/100,2) as average_discount,
    TRUNCATE(AVG(cost_cents - discount_cents)/100,2) as average_paid,
    DATE_FORMAT(pu.paid_at, '#{resolution_date_format}') as `resolution`,
    CASE
      WHEN preorder_purchases.itunes_enabled = true AND preorder_purchases.google_enabled = true
        THEN 'iTunes and Google'
      WHEN preorder_purchases.itunes_enabled = true AND preorder_purchases.google_enabled = false
        THEN 'iTunes'
      WHEN preorder_purchases.itunes_enabled = false AND preorder_purchases.google_enabled = true
        THEN 'Google'
    END as preorder_product_name
    )
  end

  def join_string
    "INNER JOIN purchases pu FORCE INDEX (`index_purchases_on_paid_at`) on pu.product_id=products.id
     INNER JOIN preorder_purchases on preorder_purchases.id=pu.related_id AND pu.related_type='PreorderPurchase'"
  end

  def order_string
    ""
  end

  def group_by_string
    "DATE_FORMAT(pu.paid_at, '#{group_by_date_format}'), itunes_enabled, google_enabled"
  end

  def having_string
    ["preorder_product_name = ?", preorder_type] if preorder_type
  end

  def prepare_data
    self.raw_data = raw_data.group_by(&:preorder_product_name) unless preorder_type
    self.data_table = ReportDataTable.new(
      {
        headers: headers,
        raw_data: raw_data,
        fields: fields,
        include_row_title: true,
        include_total_and_avg: include_total_and_avg,
        data_field_name: data_field_name,
        data_is_flat: data_is_flat
      }
    )
  end
end
