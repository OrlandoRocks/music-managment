# == Schema Information
# Schema version: 404
#
# Table name: genres
#
#  id        :integer(11)     not null, primary key
#  name      :string(128)     default(""), not null
#  parent_id :integer(3)      default(0), not null
#  is_active :boolean(1)      default(TRUE), not null
#

class Genre < ApplicationRecord
  include Translatable
  extend ArelTableMethods
  attr_translate :name

  COUNTRY_GENRE_NAMES = ["Indian"].map(&:freeze).freeze

  NO_DISPLAY_INDIA_GENRES = [
    "Classical",
    "Classical / Instrumental",
    "Classical / Vocal",
    "Carnatic Classical",
    "Carnatic Classical Instrumental",
    "Hindustani Classical",
    "Hindustani Classical Instrumental"
  ].freeze

  INSTRUMENTAL_GENRE_ID = 38
  INDIAN_GENRE_ID = 40
  LATIN_GENRE_ID = 17
  BRAZILIAN_GENRE_ID = 102
  AFRICAN_GENRE_ID = 125
  AMBIENT_GENRE_ID = 101
  LATIN_SUBGENRE_IDS = (103..114).to_a.freeze
  BRAZILIAN_SUBGENRE_IDS = [102, 115..124].freeze
  AFRICAN_SUBGENRE_IDS = [AFRICAN_GENRE_ID, 126..134].to_a.freeze
  INDIAN_GENRE_NAME = "Indian".freeze
  LATIN_GENRE_NAME = "Latin".freeze
  BRAZILIAN_GENRE_NAME = "Brazilian".freeze
  AFRICAN_GENRE_NAME = "African".freeze

  has_many :background_genres
  has_many :backgrounds, through: :background_genres

  has_many :typeface_genres
  has_many :typefaces, through: :typeface_genres

  validates :name, presence: true

  scope :active, -> { where(is_active: true) }
  scope :by_name, -> { order(genre_t[:name].asc) }
  scope :global, -> { global_genres }
  scope :display_latin_subgenres, -> { where(id: LATIN_SUBGENRE_IDS) }
  scope :display_brazilian_subgenres, -> { where(id: BRAZILIAN_SUBGENRE_IDS) }
  scope :display_ambient_genre, -> { where(id: AMBIENT_GENRE_ID) }
  scope :display_african_subgenres, -> { where(id: AFRICAN_SUBGENRE_IDS) }
  scope :display_without_no_display_india, -> {
    where.not(name: Genre::NO_DISPLAY_INDIA_GENRES)
         .where("is_active IS TRUE OR (parent_id = 40 AND is_active IS FALSE)")
  }

  @@spoken_word_genre = nil
  @@classical_genre = nil

  def to_s
    name || ""
  end

  def random_background_for
    collection = Background.all
    collection[rand(collection.size)]
  end

  def random_typeface_for
    collection = Typeface.all
    collection[rand(collection.size)]
  end

  def self.display_genres(person)
    genres = display_without_no_display_india
    genres = genres.or(display_latin_subgenres) if FeatureFlipper.show_feature?(:latin_genres, person)
    genres = genres.or(display_brazilian_subgenres) if FeatureFlipper.show_feature?(:brazilian_genres, person)
    genres = genres.or(display_ambient_genre) if FeatureFlipper.show_feature?(:ambient_genre, person)
    genres = genres.or(display_african_subgenres) if FeatureFlipper.show_feature?(:african_genres, person)
    genres
  end

  def self.global_genres
    country_parent_genres = where(name: COUNTRY_GENRE_NAMES)

    where.not(parent_id: country_parent_genres, id: country_parent_genres)
  end

  def self.india_subgenres
    find_by(name: INDIAN_GENRE_NAME).subgenres
                                    .where.not(name: NO_DISPLAY_INDIA_GENRES)
  end

  def self.latin_subgenres
    find_by(name: LATIN_GENRE_NAME).subgenres
  end

  def self.brazilian_subgenres
    find_by(name: BRAZILIAN_GENRE_NAME).subgenres
  end

  def self.african_subgenres
    find_by(name: AFRICAN_GENRE_NAME).subgenres
  end

  def self.available_album_genres
    active.global.by_name
  end

  def self.available_ringtone_genres
    active.global.by_name.where(self[:id].not_in(Ringtone::RESTRICTED_GENRES))
  end

  def self.[](attr)
    arel_table[attr.to_sym]
  end

  def self.spoken_word_genre
    @@spoken_word_genre ||= find_by(name: "Spoken Word")
  end

  def self.classical_genre
    @@classical_genre ||= find_by(name: "Classical")
  end

  def escapeHTML
    CGI.escapeHTML(name)
  end

  def subgenre?
    parent_id?
  end

  def subgenres
    Genre.where(parent_id: id)
  end

  def parent_id?
    parent_id != 0
  end

  def parent_genre
    Genre.find_by(id: parent_id)
  end

  def india_parent_genre?
    name == INDIAN_GENRE_NAME
  end

  def latin_parent_genre?
    name == LATIN_GENRE_NAME
  end

  def brazilian_parent_genre?
    name == BRAZILIAN_GENRE_NAME
  end

  def african_parent_genre?
    name == AFRICAN_GENRE_NAME
  end

  def india_subgenre?
    parent_genre == Genre.india_parent_genre
  end

  def latin_subgenre?
    parent_genre == Genre.latin_parent_genre
  end

  def brazilian_subgenre?
    parent_genre == Genre.brazilian_parent_genre
  end

  def african_subgenre?
    parent_genre == Genre.african_parent_genre
  end

  def self.parent_genres
    where(parent_id: [nil, 0])
  end

  def self.india_parent_genre
    @@india_genre ||= find_by(name: INDIAN_GENRE_NAME)
  end

  def self.latin_parent_genre
    @@latin_genre ||= find_by(name: LATIN_GENRE_NAME)
  end

  def self.brazilian_parent_genre
    @@brazilian_genre ||= find_by(name: BRAZILIAN_GENRE_NAME)
  end

  def self.african_parent_genre
    @@african_genre ||= find_by(name: AFRICAN_GENRE_NAME)
  end
end
