class PeopleFlag < ApplicationRecord
  belongs_to :person
  belongs_to :flag
  belongs_to :flag_reason, optional: true

  has_many :flag_transitions,
           ->(
                   record
             ) do
             joins(:flag)
               .where(flag_transitions: {
                        person_id: record.person.id,
                        flag_id: record.flag_id
                      })
           end,
           foreign_key: :person_id,
           primary_key: :person_id

  validates :flag_id, uniqueness: { scope: %i[person_id flag_reason_id] }
  validates_with FlagReasonValidator

  def self.latest_for_flag(flag)
    where(flag: flag)
      .order(created_at: :desc)
      .take
  end
end
