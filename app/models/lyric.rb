class Lyric < ApplicationRecord
  belongs_to :song

  validates :content, length: { maximum: 10_000 }
  validates_with Utf8mb3Validator, fields: [:content]

  before_save :capitalize_content

  def capitalize_content
    return if content.blank?

    content.gsub!(/^(\s*)\w/, &:upcase)
  end
end
