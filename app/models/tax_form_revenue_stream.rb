class TaxFormRevenueStream < ApplicationRecord
  belongs_to :tax_form
  belongs_to :revenue_stream
  belongs_to :person

  scope :user_mapped, -> { where.not(user_mapped_at: nil) }
  scope :distribution, -> { joins(:revenue_stream).merge(RevenueStream.with_distribution) }
  scope :publishing, -> { joins(:revenue_stream).merge(RevenueStream.with_publishing) }
end
