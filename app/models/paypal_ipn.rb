class PaypalIpn < ApplicationRecord
  extend MoneyField
  money_reader :payment_amount

  belongs_to :person
  belongs_to :invoice
  after_create :log_transaction

  # FIXMETHS - val_uniqueness_of on source (both id and type)

  def log_transaction
    begin
      logged_action = "PayPal Transaction |"
      logged_action += " transaction_id=#{paypal_txn_id} " if paypal_txn_id
      logged_action += " amount=#{payment_amount} " if payment_amount
      logged_action += " person_id=#{person_id} " if person_id
      logged_action += " status=#{payment_status} " if payment_status
      logged_action += " response_code=100 " if payment_status == "Completed"
      logger.info logged_action
      logged_action
    rescue
    end
  end

  def self.search_transactions(conditions, options = {})
    select("pp.id, pp.invoice_id, pp.payment_status, pp.paypal_txn_id, pp.paypal_item_number, pp.payment_amount_cents, pp.updated_at, p.name, pp.person_id")
      .joins("pp INNER JOIN people p ON pp.person_id=p.id")
      .where(conditions)
      .order(options[:sort] || "updated_at DESC")
      .paginate(page: options[:page], per_page: (options[:per_page] || 10).to_i)
  end
end
