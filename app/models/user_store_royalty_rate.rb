class UserStoreRoyaltyRate < ApplicationRecord
  belongs_to :royalty_sub_store
  belongs_to :person

  validates :person, presence: true
  validates :royalty_sub_store, presence: true
  validates :royalty_rate, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 1 }
end
