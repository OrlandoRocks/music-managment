require "S3"

class CopyrightDocument < ApplicationRecord
  VALID_CONTENT_TYPES = [
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/pdf",
    "image/jpeg"
  ].freeze

  belongs_to :related, polymorphic: true
  belongs_to :s3_asset

  def s3_key(filename)
    "indiapublishing/#{Rails.env}/#{related.person.id}/#{related.id}/#{filename}"
  end

  def self.upload_document(album, document)
    document_uploaded = true
    begin
      ActiveRecord::Base.transaction do
        copyright_document = CopyrightDocument.create!(related: album)
        asset = S3Asset.new(
          bucket: COPYRIGHT_DOCUMENTS_BUCKET_NAME,
          key: copyright_document.s3_key(document.original_filename),
          filename: document.path
        )
        asset.put!(file: document.tempfile)
        asset.save!
        copyright_document.s3_asset = asset
        copyright_document.save!
      end
    rescue StandardError => e
      document_uploaded = false
      message = "Error uploading copyright_document -> #{album.id}: #{e}"
      Tunecore::Airbrake.notify(message)
      Rails.logger.info message
    end
    document_uploaded
  end

  def url
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = 3600
    generator.get(COPYRIGHT_DOCUMENTS_BUCKET_NAME, s3_asset.key)
  end
end
