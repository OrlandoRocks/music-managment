class TcReporterAudit < ApplicationRecord
  serialize :api_params, QuirkyJson
  serialize :api_response, QuirkyJson

  belongs_to :person

  validates :api_params, :api_response, :person, :report, presence: true
end
