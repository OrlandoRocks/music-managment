# == Schema Information
# Schema version: 404
#
# Table name: serials
#
#  id         :integer(11)     not null, primary key
#  number     :string(128)     default(""), not null
#  taken      :boolean(1)      not null
#  person_id  :integer(11)
#  claimed_at :datetime
#  created_at :datetime        not null
#  os         :string(15)      default("")
#  program    :string(128)
#

# stores serial numbers which can be redeemed by customers for free stuff from other companies
class Serial < ApplicationRecord
  belongs_to :person
  validates :program, presence: true
  validates :number, uniqueness: { scope: [:program] }
  validates(
    :person_id,
    uniqueness: {
      scope: [:program],
      message: "Only one serial number per account",
      if: :person_id
    }
  )

  # which programs have operating system specific serial numbers?
  def self.os_specific?(program)
    %w[t_racks].include? program
  end

  def os_specific?
    self.class.os_specific? program
  end

  validates_each :os do |record, attr, value|
    if record.os_specific?
      record.errors.add attr, "invalid os type" unless %w[mac windows].include? value
    elsif value.present?
      record.errors.add attr, "should not have os type"
    end
  end

  def assign_to_person(person)
    self.person = person
    self.taken = true
    self.claimed_at = Time.now
  end

  def self.remaining(program, os = nil)
    if os_specific? program
      raise "missing os" unless os

      conditions = ["program = ? and taken = false and os = ?", program, os]
    else
      conditions = ["program = ? and taken = false", program]
    end
    where(conditions).count
  end

  def self.get_free(program, os = nil)
    if os_specific? program
      raise "missing os" unless os

      find_by(program: program, taken: false, os: os)
    else
      find_by(program: program, taken: false)
    end
  end

  def display_program
    program.titleize
  end

  # os specific methods
  def mac?
    "mac" == os
  end

  def windows?
    "windows" == os
  end

  def os_name
    mac? ? "Mac" : "PC"
  end

  def wrong_os_name
    mac? ? "PC" : "Macintosh"
  end
end
