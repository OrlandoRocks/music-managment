# frozen_string_literal: true

class Refund < ApplicationRecord
  extend MoneyField

  LEVELS = [
    INVOICE = "invoice",
    LINE_ITEM = "line_item"
  ].freeze

  TYPES = [
    FIXED_AMOUNT = "fixed_amount",
    FULL_REFUND = "full_refund",
    PERCENTAGE = "percentage"
  ].freeze

  ADMIN_ROLES = [Role::REFUNDS, Role::ISSUES_REFUNDS_ONLY].freeze

  enum status: {
    success: "success",
    error: "error",
    pending: "pending"
  }

  belongs_to :invoice
  belongs_to :corporate_entity
  belongs_to :refunded_by, class_name: "Person"
  belongs_to :refund_reason

  has_many :refund_items, dependent: :destroy
  has_many :refund_settlements, dependent: :destroy

  has_one :outbound_refund, dependent: :destroy
  has_one :dispute, dependent: :nullify

  delegate :person,
           :invoice_static_corporate_entity,
           :tax_type,
           :tax_rate,
           :foreign_exchange_rate,
           :pegged_rate, to: :invoice, allow_nil: true

  delegate :backfill?, to: :refund_reason
  delegate :reason, to: :refund_reason, allow_nil: true

  before_validation :set_corporate_entity_and_currency, if: -> { invoice.present? }

  validates :currency, :corporate_entity, :invoice, :refunded_by, presence: true
  validates :invoice_sequence,
            uniqueness: {
              scope: :corporate_entity_id,
              allow_nil: true
            },
            if: -> { corporate_entity.present? }

  validate :refundable_invoice, if: -> { invoice.present? && refund_items.blank? }
  validate :admin_has_refund_role

  after_save_commit :generate_invoice_sequence, if: :generate_invoice_sequence?

  money_reader :base_amount, :tax_amount, :total_amount

  scope :non_backfill, -> {
    joins(:refund_reason).where.not("reason = ?", RefundReason::BACKFILL)
  }

  def self.last_generated_invoice_sequence(corporate_entity)
    where(corporate_entity: corporate_entity)
      .order(invoice_sequence: :desc)
      .first&.invoice_sequence || 0
  end

  def credit_note_invoice_number
    return if invoice_sequence.blank? || credit_note_invoice_prefix.blank?

    format("#{credit_note_invoice_prefix}%05d", invoice_sequence)
  end

  def settled_amount
    refund_settlements.sum(&:settlement_amount_cents)
  end

  def settled?
    refund_settlements.any? && settled_amount >= total_amount_cents
  end

  def process_settlement!(transaction, amount)
    refund_settlements.create!(
      settlement_amount_cents: amount,
      source: transaction,
      currency: currency
    )
  end

  def label
    "Refund for the invoice #{invoice.id}"
  end

  def vat_amount_in_euro
    Money.new(vat_cents_in_euro, CurrencyCodeType::EUR)
  end

  def euro_exchange_rate
    return 1 if currency == CurrencyCodeType::EUR
    return foreign_exchange_rate.exchange_rate if foreign_exchange_rate.present?

    ForeignExchangeRate
      .latest_by_currency(source: currency, target: CurrencyCodeType::EUR)
      .exchange_rate
  end

  def show_credit_note_invoice?
    success? &&
      !invoice.has_incomplete_static_data? &&
      credit_note_invoice_number.present?
  end

  private

  def set_corporate_entity_and_currency
    self.corporate_entity = invoice.corporate_entity || person.corporate_entity
    self.currency = invoice.currency
  end

  def admin_has_refund_role
    return if refunded_by&.refund_admin?

    errors.add(:base, "Admin doesn't have permission to refund")
  end

  def refundable_invoice
    return if invoice.refundable_amount_cents.positive? || backfill?

    errors.add(:base, "Refundable amount is 0 for invoice_id #{invoice.id}")
  end

  def generate_invoice_sequence
    GenerateCreditNoteInvoiceSequenceWorker.perform_async(id)
  end

  def generate_invoice_sequence?
    settled? && success? && invoice_sequence.blank? && !backfill?
  end

  def credit_note_invoice_prefix
    return if invoice_static_corporate_entity.blank?

    invoice_static_corporate_entity.credit_note_inbound_prefix || credit_note_invoice_prefix_fallback
  end

  def credit_note_invoice_prefix_fallback
    return CorporateEntity::TC_CREDIT_NOTE_INVOICE_PREFIX if invoice_static_corporate_entity.tunecore_us?

    CorporateEntity::BI_CREDIT_NOTE_INVOICE_PREFIX
  end
end
