class Artist < ApplicationRecord
  include Linkable
  include Scrubber
  has_many :creatives
  has_many :albums, through: :creatives, source: :creativeable, source_type: "Album"
  has_many :videos, through: :creatives, source: :creativeable, source_type: "Video"
  has_many :songs, through: :creatives, source: :creativeable, source_type: "Song"
  has_one :person
  has_one :blacklisted_artist, dependent: :destroy
  has_many :workers
  has_many :artist_store_whitelists, dependent: :destroy

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: true, on: :create }

  before_validation :strip_name, on: :create
  before_save :truncate_name
  before_save :update_scrubbed_name

  scope :active_blacklist, -> { joins(:blacklisted_artist).where(blacklisted_artists: { active: true }) }

  scope :inactive_blacklist, -> { joins(:blacklisted_artist).where(blacklisted_artists: { active: false }) }

  scope :for_display, -> { select("artists.name, group_concat(artists.id) as ids").group(:name).order(name: :asc) }

  def strip_name
    self.name = name.strip if name.present?
  end

  def truncate_name
    self.name = name.slice(0, 120)
  end

  def self.bulk_find_or_create(artist_names)
    existing  = where(name: artist_names)
    new_names = (artist_names - existing.map(&:name)).map { |n| [n] }

    Artist.import [:name], new_names, validate: true if new_names.present?

    existing.reload
  end

  def blacklisted?
    !!blacklisted_artist.try(:active?)
  end

  def name=(arg)
    if @new_record == true
      super(arg)
    else
      raise "
        Sorry, you can't change an artist's name, even for
        capitalization reasons. You have to create a new artist record
        and update the class/object's artist_id instead.
      "
    end
  end

  def blacklist!
    return true if blacklisted?

    blacklisted_artist ? blacklisted_artist.toggle!(:active) : create_blacklisted_artist
  end

  def remove_from_blacklist
    return true if blacklisted_artist.nil?

    blacklisted_artist.update_attribute(:active, false)
  end

  def blacklisted?
    !!blacklisted_artist.try(:active?)
  end

  # This method finds or creates a NON-TITLEIZED artist
  def self.create_by_nontitleized_name(value)
    Artist.find_or_create_by(name: value)
  end

  # This method finds or creates a TITLEIZED artist, unless it finds a non-titleized version that matches the input
  def self.create_by_titleized_name(value)
    Artist.find_by(name: value) || Artist.find_or_create_by(name: Utilities::Titleizer.titleize(value))
  end
end
