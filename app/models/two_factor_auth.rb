class TwoFactorAuth < ApplicationRecord
  belongs_to :person
  has_many :two_factor_auth_events

  # NOTE: this callback can be removed once Authy is debugged.
  before_validation :force_sms_auth, if: :uses_app_notification?

  validates :person, presence: true
  # NOTE: this validation can stay once the app is enabled, and we can just modify the constant.
  validate :has_active_notification_method

  CALL = "call"
  SMS = "sms"

  ACTIVE_NOTIFICATION_METHODS = [CALL, SMS].map(&:freeze).freeze
  AUTHENTICATION_WINDOW = 2.minutes.freeze

  def last_event
    two_factor_auth_events.last
  end

  def active?
    return false if activated_at.blank?

    disabled_at.blank? || activated_at > disabled_at
  end

  def within_authentication_window?
    last_verified_at.present? && last_verified_at >= AUTHENTICATION_WINDOW.ago
  end

  def disable!
    update_attribute(:disabled_at, Time.now)
  end

  def auth_success!
    time = Time.now
    params = { last_verified_at: time }
    params[:activated_at] = time if activated_at.nil? || reactivating_after_disabled
    update(params)
  end

  def auth_failure!
    update(last_failed_at: Time.now)
  end

  def alternate_method
    (notification_method == "sms") ? "call" : "sms"
  end

  def uses_app_notification?
    notification_method == "app"
  end

  def force_sms_auth
    self.notification_method = "sms"
  end

  def has_active_notification_method
    notification_method.in?(ACTIVE_NOTIFICATION_METHODS)
  end

  def save_previous_preferences
    update(
      previous_notification_method: notification_method,
    )
  end

  def restore_previous_preferences
    update(
      notification_method: previous_notification_method,
    ) if can_restore_preferences?
  end

  private

  def can_restore_preferences?
    !previous_preferences_are_nil? && !previous_preferences_are_identical?
  end

  def previous_preferences_are_nil?
    (previous_preferences - [nil]).empty?
  end

  def previous_preferences_are_identical?
    (current_preferences & previous_preferences) == current_preferences
  end

  def current_preferences
    [notification_method]
  end

  def previous_preferences
    [previous_notification_method]
  end

  def reactivating_after_disabled
    disabled_at? && Time.now > disabled_at
  end
end
