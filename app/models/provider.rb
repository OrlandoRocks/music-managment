class Provider < TrendsBase
  self.table_name = "providers"

  STREAMING = ["spotify"]
  DOWNLOAD  = ["itunes", "amazon"]

  validates :name, presence: true

  scope :itunes, -> { where(name: "itunes") }
  scope :amazon, -> { where(name: "amazon") }
  scope :spotify, -> { where(name: "spotify") }

  scope :streaming, -> { where(name: STREAMING) }
  scope :download, -> { where(name: DOWNLOAD) }
end
