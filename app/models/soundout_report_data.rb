class SoundoutReportData < ApplicationRecord
  extend Memoist

  self.primary_key = "soundout_report_id"

  GENDER  = "gender"
  RATING  = "score"
  AGE     = "age"
  COMMENT = "comment"
  GENDER_MAP = ActiveSupport::HashWithIndifferentAccess.new({ 1 => "Male", 2 => "Female" })

  belongs_to :soundout_report
  validates :soundout_report, presence: true
  validates :report_data, presence: true

  #
  # Returns an enumerator of the artists mentioned
  #
  def artists_mentioned
    if report_data_hash["data"] and report_data_hash["data"]["ArtistsMentioned"]
      Enumerator.new do |yielder|
        report_data_hash["data"]["ArtistsMentioned"].each do |artist_hash|
          yielder << [CGI.unescape(artist_hash.first[0]), artist_hash.first[1]]
        end
      end
    else
      Enumerator.new { |yielder| yielder << [] }
    end
  end

  #
  # Returns whether or not the report data has the specified key
  #
  def has?(k)
    report_data_hash["data"][k.to_s].present? if report_data_hash["data"]
  end

  #
  # Return an enumerator of the market potential age groups
  #
  def market_pot_age_groups
    return unless report_data_hash["data"] and report_data_hash["data"]["MktPotAge"]

    Enumerator.new do |yielder|
      report_data_hash["data"]["MktPotAge"].each do |age_hash|
        yielder << age_hash.first
      end
    end
  end

  #
  # Return an enumerator of the market potential in genre groups
  #
  def market_pot_age_genre_groups
    return unless report_data_hash["data"] and report_data_hash["data"]["MktPotAgeGenre"]

    Enumerator.new do |yielder|
      report_data_hash["data"]["MktPotAgeGenre"].each do |age_hash|
        yielder << age_hash.first
      end
    end
  end

  #
  # Return a normalized hash
  #
  def track_positioning
    return unless report_data_hash["data"] and report_data_hash["data"]["PositioningThisTrack"]

    report_data_hash["data"]["PositioningThisTrack"]
  end

  #
  # Returns a normalized hash
  #
  def track_positioning_major_releases
    report_data_hash["data"]["PositioningLabelOther"] if report_data_hash["data"]
  end

  #
  # Return a normalized hash
  #
  def genre_positioning
    report_data_hash["data"]["PositioningGenreOther"]
  end

  #
  # Returns an array of just the just a hash of the rating distribtion data
  #
  def rating_distribution_data
    data = []
    report_data_hash["data"]["RatingDistribution"].each do |rating|
      data << if rating.is_a? Array
                rating.first
              else
                rating.first[1]
              end
    end if report_data_hash["data"] and report_data_hash["data"]["RatingDistribution"]
    data
  end

  def report_data_hash
    begin
      JSON.parse(report_data)
    rescue
      Rails.logger.info("Error parsing soundout json")
    end
  end
  memoize :report_data_hash

  def report_data=(json_string)
    begin
      json = JSON.parse(json_string) if json_string

      if json
        self.url = json["status"]["url"] if json["status"].present?

        # If this json has a data section
        if json["data"]
          self.market_potential       = json["data"]["MktPot"].to_i
          self.market_potential_genre = json["data"]["MktPotGenre"].to_i
          self.track_rating           = json["data"]["TrackRating"].to_f
          self.passion_rating         = json["data"]["PassionRating"].to_f
          self.in_genre_class         = json["data"]["InGenreClass"]
        end

        # Normalize the comments encoding if there is a reviews section
        json["reviews"].each do |id, review|
          next if id == "total"

          review[COMMENT] = Rack::Utils.unescape(review[COMMENT])
        end if json["reviews"]

        json_string = json.to_json
      end
    rescue JSON::ParserError
      Rails.logger.info("Error parsing soundout json")
    end

    self[:report_data] = json_string
  end

  #
  # Returns the percent of the weight that is male
  #
  def sample_group_male_percent
    (report_data_hash["data"]["SampleGroupGender"].find { |v| v.has_key?("Male") }["Male"].to_f / sample_group_weighted_total) * 100
  end

  #
  # Returns the percent of the weight that is female
  #
  def sample_group_female_percent
    (report_data_hash["data"]["SampleGroupGender"].find { |v| v.has_key?("Female") }["Female"].to_f / sample_group_weighted_total) * 100
  end

  #
  # Returns an enumertor of the sample age groups and its percent unrounded
  #
  def sample_age_groups
    return unless report_data_hash["data"] and report_data_hash["data"]["SampleGroupAge"]

    Enumerator.new do |yielder|
      report_data_hash["data"]["SampleGroupAge"].each do |age_hash|
        yielder << [age_hash.first[0], (age_hash.first[1].to_f / sample_group_weighted_total) * 100]
      end
    end
  end

  #
  # Filters and searches reviews from the json report data
  #
  # O(n) + O(nlogn)
  #
  def search_and_filter_reviews(options = {})
    options[:page]     ||= 1
    options[:per_page] ||= 50

    filtered = []

    begin
      json = JSON.parse(report_data)

      if json and json["reviews"]

        json["reviews"].each do |id, review|
          next if id == "total"

          # Filter before doing the more expensive string search
          # If any filter rule fails continue to the next review
          next if options[:rating_gte] && (review[RATING].to_i < options[:rating_gte].to_i)

          next if options[:rating_lte] && (review[RATING].to_i > options[:rating_lte].to_i)

          next if review[AGE] and options[:age_gte] && (review[AGE].to_i < options[:age_gte].to_i)

          next if review[AGE] and options[:age_lte] && (review[AGE].to_i > options[:age_lte].to_i)

          if review[GENDER] and options[:gender] and options[:gender] != "ALL" && (review[GENDER].to_i != ((options[:gender] == "Male") ? 1 : 2))
            next
          end

          next if options[:search] && !review[COMMENT].downcase.include?(options[:search].downcase)

          filtered << review
        end

        # Sort if necessary
        if options[:order]

          case options[:order]
          when "Rating ASC"
            filtered.sort! { |a, b| a[RATING].to_i <=> b[RATING].to_i }
          when "Rating DESC"
            filtered.sort! { |a, b| b[RATING].to_i <=> a[RATING].to_i }
          when "Age ASC"
            filtered.sort! { |a, b| a[AGE].to_i <=> b[AGE].to_i }
          when "Age DESC"
            filtered.sort! { |a, b| b[AGE].to_i <=> a[AGE].to_i }
          when "Gender ASC"
            filtered.sort! { |a, b| a[GENDER].to_i <=> b[GENDER].to_i }
          when "Gender DESC"
            filtered.sort! { |a, b| b[GENDER].to_i <=> a[GENDER].to_i }
          end
        end

        # Do paging
        start_index = (options[:page].to_i - 1) * options[:per_page].to_i
        return filtered[start_index, options[:per_page].to_i], filtered.size
      end
    rescue JSON::ParserError
      return [], 0
    end
  end

  #
  # Returns a normalized hash of song analysis data
  #
  def song_analysis
    data = {}
    report_data_hash["data"]["SongAnalysis"].each do |v|
      data[v.first[0]] = v.first[1]
    end
    data
  end

  def track_appeal_preference
    return unless report_data_hash and report_data_hash["data"] and report_data_hash["data"]["TrackAppeal"]

    report_data_hash["data"]["TrackAppeal"].find { |v| v.has_key?("Preference") }["Preference"]
  end

  def track_appeal_no_preference
    return unless report_data_hash and report_data_hash["data"] and report_data_hash["data"]["TrackAppeal"]

    report_data_hash["data"]["TrackAppeal"].find { |v| v.has_key?("NoPreference") }["NoPreference"]
  end

  #
  # Returns a normalized hash of wordcloud data
  #
  def word_cloud
    data = {}
    report_data_hash["data"]["WordCloud"].each do |v|
      data[v.first[0]] = v.first[1]
    end if report_data_hash["data"] and report_data_hash["data"]["WordCloud"]
    data
  end

  private

  #
  # Memoizes the total weight of the sample group
  #
  def sample_group_weighted_total
    total = 0.0
    report_data_hash["data"]["SampleGroupGender"].each do |gender_hash|
      total += gender_hash.first[1].to_f
    end if report_data_hash["data"] and report_data_hash["data"]["SampleGroupGender"]
    total
  end
  memoize :sample_group_weighted_total
end
