# == Schema Information
# Schema version: 404
#
# Table name: store_intakes
#
#  id                  :integer(11)     not null, primary key
#  ruby_type           :string(20)      default(""), not null
#  store_id            :integer(11)     default(0), not null
#  reporting_month_id  :integer(11)     default(0), not null
#  local_currency      :string(3)       default(""), not null
#  local_total_cents   :integer(11)     default(0), not null
#  exchange_symbol     :string(6)
#  exchange_rate_fixed :integer(19)
#  exchange_rate_scale :integer(11)
#  usd_actual_cents    :integer(11)     default(0), not null
#  usd_total_cents     :integer(11)     default(0), not null
#  usd_payout_cents    :integer(11)     default(0), not null
#  comment             :text
#  unique_id           :string(30)
#

class StoreIntakeItunesAu < StoreIntake
  # iTunes Australia withholds 5% from all sales despite reporting the full
  # amount in the statement - we fix the value here.
  def self.calculate_payout_cents(lcents, exchange_symbol, exchange_rate)
    calculate_usd_cents(LongDecimal(lcents) * LongDecimal("0.95"), exchange_symbol, exchange_rate)
  end

  def payout_comment
    "5% of net sales held back for Australian/NZ copyright tariff."
  end
end
