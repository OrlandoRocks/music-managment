class TrendDataDetail < TrendsBase
  self.table_name = "trend_data_detail"

  belongs_to :album
  belongs_to :import_log
  belongs_to :song
  belongs_to :trans_type
  belongs_to :artist
  belongs_to :person
  belongs_to :provider
  belongs_to :trend_data_summary

  validates :provider, :customer_price, :royalty_price, :trans_type, :qty, :album, :sale_date, presence: true
end
