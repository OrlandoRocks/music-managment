class VariablePrice < ApplicationRecord
  belongs_to :store
  has_many :salepoints
  has_many :variable_prices_salepointable_types, dependent: :destroy
  has_many :variable_prices_stores, dependent: :destroy
  has_many :country_variable_prices

  #
  #  Variable Prices are offered different
  #  lists per salepointable type.
  #
  scope :by_salepointable_type,
        ->(type) {
          includes(:variable_prices_salepointable_types)
            .where(variable_prices_salepointable_types: { salepointable_type: type })
            .order("variable_prices.position ASC")
        }

  scope :user_editable, -> { where(user_editable: true) }

  def price_by_country_website(country_website)
    country_variable_prices.find { |cvp|
      country_website.id == cvp.country_website.id
    }.try(:wholesale_price) || price
  end
end
