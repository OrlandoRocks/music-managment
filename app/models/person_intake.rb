class PersonIntake < ApplicationRecord
  belongs_to  :person
  has_many    :album_intakes, before_add: [:adding_album_intake]

  has_one     :person_transaction, as: :target
  has_and_belongs_to_many :sales_record_masters, join_table: "person_intake_sales_record_masters"

  delegate :report_month, :report_year, to: "(reporting_month or return nil)"

  scope :year,
        ->(year) {
          includes(:reporting_months)
            .where("reporting_months.report_year = :year", year: year) if year
        }
  scope :month,
        ->(month) {
          includes(:reporting_month)
            .where("reporting_months.report_month = :month", month: month) if month
        }
  scope :person,
        ->(person_id) {
          where(person_id: person_id) if person_id
        }

  scope :with_posting_id,
        ->(posting_id) {
          joins(:sales_record_masters)
            .joins("LEFT JOIN outbound_invoices ON person_intakes.id = outbound_invoices.related_id
            and outbound_invoices.related_type = 'PersonIntake'")
            .joins("LEFT JOIN vat_tax_adjustments ON
            person_intakes.id = vat_tax_adjustments.related_id
            AND vat_tax_adjustments.related_type = 'PersonIntake'")
            .where(sales_record_masters: { posting_id: posting_id }, outbound_invoices: { id: nil })
            .order("person_intakes.id")
            .distinct
            .select('person_intakes.id AS id,
             person_intakes.person_id AS person_id,
             vat_tax_adjustments.id AS vat_tax_adjustment_id')
        }

  def self.any_for?(person)
    !where("person_id = ?", person.id).first.nil?
  end

  def compute_payout
    self.usd_payout_cents = store_intake.calculate_payout_cents(local_total_cents)
    store_intake.payout_computed(usd_payout_cents)
  end

  def adding_album_intake(album_intake)
    album_intake.attributes = {
      person_id: person_id,
      reporting_month: reporting_month
    }
  end

  def sales_record_added(sales_record)
    self.local_total_cents += sales_record.local_total_cents
    self.usd_total_cents += sales_record.usd_total_cents
  end

  def summarized?
    sales_record_masters.summarized(false).count.zero?
  end
end
