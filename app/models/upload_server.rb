# == Schema Information
# Schema version: 404
#
# Table name: upload_servers
#
#  id              :integer(11)     not null, primary key
#  url             :string(255)
#  status          :string(255)     default("active")
#  failed_attempts :integer(11)     default(0)
#

require "net/http"
require "uri"

# Used to keep track of our upload servers.
class UploadServer < ApplicationRecord
  # only allow valid hostnames. reject URLs.
  validates :url, format: { with: /\A[A-Za-z0-9.-]*\Z/, message: "Invalid hostname" }

  # Returns a random server URL from the pool
  def self.find_random
    max_attempts = 3
    @server = order("RAND()").where("status = 'active' ").first
    # check to make sure that the server is responding to us
    n = 0
    while @server != nil && !@server.is_valid?
      n += 1
      @server = order("RAND()").first
      raise "Couldn't find a valid upload server after #{max_attempts}" if n >= max_attempts
    end
    @server
  end

  def upload_server_path
    "http://#{url}/files/upload"
  end

  # Pings the upload server to make sure it is ready to accept connections. Parameter is the base URL of the upload server.
  def is_valid?
    http = Net::HTTP.new(url)
    http.open_timeout = 2
    http.read_timeout = 2
    begin
      @res_body = http.get("/files/ping").body
    rescue Timeout::Error
      return false
    end

    if @res_body[/UPLOAD SERVER READY/]
      # if the server was previously down, reset it's failed attempts log to 0
      if failed_attempts.positive?
        self.failed_attempts = 0
        self.status = "active"
        save
      end
      true
    else
      # increment the server's failed attempts log, and de-activate it if it's been down for too long
      self.failed_attempts += 1
      self.status = "inactive" if self.failed_attempts >= 3
      save
      false
    end
  end
end
