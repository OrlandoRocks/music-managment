class Person::VatCompliance
  attr_accessor :person, :customer_type

  def initialize(person)
    @person        = person
    @customer_type = person.customer_type
  end

  def incomplete?
    return true unless customer_type?
    return true unless valid_compliance_info_fields?
    return true unless valid?

    false
  end

  def complete?
    !incomplete?
  end

  private

  def customer_type?
    customer_type.present?
  end

  def valid_compliance_info_fields?
    case customer_type.to_sym
    when :individual
      person.compliance_first_name.present? &&
        person.compliance_last_name.present?
    when :business
      person.compliance_company_name.present?
    else
      false
    end
  end

  def valid?
    person.valid?
  end
end
