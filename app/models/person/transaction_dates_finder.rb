class Person::TransactionDatesFinder
  def initialize(person, date)
    @person     = person
    @start_date = date ? Date.parse(date[:start_date]) : date_range.first
    @end_date   = date ? Date.parse(date[:end_date]) : date_range.last
  end

  attr_reader :start_date
  attr_reader :end_date

  def any?
    PersonTransaction.user_has_transactions?(@person)
  end

  def date_range
    @date_range ||= PersonTransaction.date_range_by_user(@person).map(&:created_at).map(&:to_date)
  end

  def full_range
    Utilities::FormDateRange.create_range(date_range.first, date_range.last)
  end
end
