# frozen_string_literal: true

class Plan < ApplicationRecord
  validates :name, presence: true

  has_many :person_plans, dependent: :destroy
  has_many :plans_products, dependent: :destroy
  has_many :products, through: :plans_products
  has_many :plans_stores, dependent: :destroy
  has_many :stores, through: :plans_stores
  has_many :plans_plan_features, dependent: :destroy
  has_many :plan_features, through: :plans_plan_features

  NEW_ARTIST = 1
  RISING_ARTIST = 2
  BREAKOUT_ARTIST = 3
  PROFESSIONAL = 4
  LEGEND = 5

  FORCED_DOWNGRADE_PLAN_ID = NEW_ARTIST

  PLAN_UPGRADE_AVAILABILITY = {
    LEGEND => [],
    PROFESSIONAL => [5],
    BREAKOUT_ARTIST => [4, 5],
    RISING_ARTIST => [3, 4, 5],
    NEW_ARTIST => [2, 3, 4, 5]
  }.freeze

  ALL_PLAN_IDS = Plan.all.pluck(:id)
  FREE_PLAN_IDS = [NEW_ARTIST]

  scope :purchasable_by_user, -> {
    where.not(id: LEGEND)
  }

  scope :upgradable_by_user, -> do
    where.not(id: [LEGEND, PROFESSIONAL])
  end

  # Some accounts should NEVER see any Plan logic. We do not want
  # block_flag to default to anything to prevent future mishaps.
  def self.enabled?(user, block_flag)
    return false if block_flag

    FeatureFlipper.show_feature?(:plans_pricing, user)
  end

  # will prob replace this with can_distribute?(person, type) -> might put it on person instead too, although it
  # sorta feels liek the responsibility of the plan
  #
  def credit_available_for?(_person, _type)
    true
  end

  def store_available_for?(store)
    stores.exists?(id: store)
  end

  def eligible_plan_ids(plan_id)
    PLAN_UPGRADE_AVAILABILITY[plan_id]
  end

  def lower_plan_ids
    ALL_PLAN_IDS - [id] - eligible_plan_ids(id)
  end

  def downgrade_eligible?(downgrade_plan_id)
    lower_plan_ids.include?(downgrade_plan_id)
  end

  def upgrade_eligible?(next_plan_id)
    id != next_plan_id && eligible_plan_ids(id).include?(next_plan_id)
  end

  def product_by_person(person)
    products.find_by(country_website_id: person.country_website_id)
  end

  def next_plan
    Plan.find(PLAN_UPGRADE_AVAILABILITY[id].first)
  end

  def plan_features_h
    plan_features
      .pluck(:feature)
      .index_with { |_f| true }
  end

  def free_plan?
    FREE_PLAN_IDS.include?(id)
  end
end
