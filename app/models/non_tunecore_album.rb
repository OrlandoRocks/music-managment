class NonTunecoreAlbum < ApplicationRecord
  belongs_to :album
  belongs_to :composer
  belongs_to :publishing_composer
  belongs_to :account

  has_one :person, through: :composer

  has_many :non_tunecore_songs

  validates :name, presence: true
  validate :has_composer_or_publishing_composer

  private

  def has_composer_or_publishing_composer
    return unless composer_id.blank? && publishing_composer_id.blank?

    errors.add(:base, "Must have either a composer or a publishing_composer")
  end
end
