class ProductSalesOverviewReport < ReportBase
  def initialize(options)
    self.fields = [
      { field_name: :cost_after_discount, title: "Total Sales", data_type: "currency", highlight: true },
      { field_name: :average_transaction, title: "Average Transaction", show_total: false, data_type: "currency" }
    ]
    self.report_type = UNGROUPED
    super
  end

  protected

  def retrieve_data
    Purchase.select(select_string).joins(join_string).where(build_condition_array).order(order_string).group(group_by_string)
  end

  def build_condition_array
    make_date_conditions
    ["paid_at >= ? and paid_at <= ? and country_website_id = ?", start_date, end_date, country_website_id]
  end

  def select_string
    %Q(count(*) as total_purchased,
     TRUNCATE(sum(cost_cents)/100,2) as total_cost,
     TRUNCATE(sum(discount_cents)/100,2) as total_discounts,
     TRUNCATE((sum(cost_cents) - sum(discount_cents))/100, 2) as cost_after_discount,
     TRUNCATE(AVG(cost_cents - discount_cents)/100,2) as average_transaction,
     DATE_FORMAT(paid_at, '#{resolution_date_format}') as `resolution`)
  end

  def order_string
    "paid_at"
  end

  def group_by_string
    "DATE_FORMAT(paid_at, '#{group_by_date_format}')"
  end

  def join_string
    "inner join people p ON p.id = purchases.person_id"
  end
end
