class Setting < ApplicationRecord
  belongs_to :settable, polymorphic: true
  belongs_to :option

  def value
    attributes["value"] || option.default_value
  end
end
