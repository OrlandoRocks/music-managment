class EftBatchTransactionHistory < ApplicationRecord
  self.table_name = "eft_batch_transaction_history"

  default_scope { order(created_at: :asc) }

  belongs_to :eft_batch_transaction
  belongs_to :person_transaction
  belongs_to :posted_by, class_name: "Person"
  STATUSES = [
    "requested",
    "debit_amount",
    "debit_service_fee",
    "canceled",
    "rejected",
    "rollback_debit",
    "rollback_service_fee",
    "approved",
    "removed_approval",
    "processing",
    "error",
    "sent_to_bank",
    "success",
    "failure",
    "debit_failure_fee"
  ]

  before_validation :set_currency, on: :create, if: :has_amount?

  validates :currency, presence: { if: :has_amount? }
  validates :currency, inclusion: { in: ["USD"], if: :has_amount? }

  ### NAMED SCOPES ###
  scope :by_date, ->(date) { where("DATE(created_at) = ?", date) }
  scope :by_status, ->(status) { where(status: status) }

  private

  def has_amount?
    !amount.nil?
  end

  def set_currency
    self.currency = eft_batch_transaction.currency
  end
end
