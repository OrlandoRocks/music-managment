# frozen_string_literal: true

class PayuTransaction < ApplicationRecord
  include PayuError

  belongs_to :person
  belongs_to :invoice

  has_many :refunds, -> {
    where(action: REFUND)
  }, class_name: "PayuTransaction",
     foreign_key: :original_transaction_id,
     inverse_of: :original_transaction

  belongs_to :original_transaction, -> {
    where(action: SALE, status: true)
  }, class_name: "PayuTransaction",
     foreign_key: :original_transaction_id,
     inverse_of: :refunds,
     optional: true

  has_many :payu_webhook_logs, dependent: :destroy

  validates :person, :amount, presence: true
  validates :invoice, presence: true
  delegate :country_website, to: :person, allow_nil: true

  after_update :settle_invoice!, if: -> { marked_successful? && sale? }

  # CREDITED = "Credited"
  SUCCEEDED = "Succeed"
  FAILED = "Failed"
  # VOIDED = "Voided"
  PENDING = "Pending"
  # INITIALIZED = "Initialized"
  # REFUNDED = "Refunded"
  # CAPTURED = "Captured"
  SALE = "sale"
  REFUND = "refund"

  enum action: {
    sale: SALE,
    refund: REFUND
  }

  scope :action_filter,
        ->(search_params) {
          if search_params[:action].present?
            search_action = search_params[:action].downcase.strip
            where(action: search_action) if actions.values.map(&:downcase).include? search_action
          end
        }

  scope :min_amount_filter,
        ->(search_params) {
          where(amount: search_params[:mina]..Float::INFINITY) if search_params[:mina].present? && search_params[:mina]
        }

  scope :max_amount_filter,
        ->(search_params) {
          where(
            amount: 0..search_params[:maxa],
          ) if search_params[:maxa].present? && !search_params[:maxa].zero?
        }

  scope :transaction_id_filter,
        ->(search_params) {
          if search_params[:transaction_id].present?
            where(payu_id: search_params[:transaction_id].strip)
              .or(where(refund_request_id: search_params[:transaction_id].strip))
            # PAYU_TODO
            # .or(where(charge_id: search_params[:transaction_id].strip))
          end
        }

  scope :from_date_filter,
        ->(search_params) {
          where(arel_table[:created_at].gteq(search_params[:from_date])) if search_params[:from_date].present?
        }

  scope :to_date_filter,
        ->(search_params) {
          where(arel_table[:created_at].lteq(search_params[:to_date])) if search_params[:to_date].present?
        }

  scope :status_filter,
        ->(search_params) {
          if search_params[:payments_os_status].present?
            case search_params[:payments_os_status]
            when SUCCEEDED
              where(status: 1)
            when FAILED, PENDING
              where(status: [0, nil])
            else
              none
            end
          end
        }

  scope :person_filter,
        ->(search_params) {
          if search_params[:person_search].present?
            people = Person.arel_table
            people_join = arel_table.join(people).on(arel_table[:person_id].eq(people[:id])).join_sources
            sanitized_like_query = "%#{sanitize_sql_like(search_params[:person_search])}%"

            joins(people_join).where(people[:email].eq(search_params[:person_search]))
                              .or(joins(people_join).where(people[:name].matches(sanitized_like_query)))
          end
        }

  scope :search_transaction,
        ->(search_params) {
          action_filter(search_params)
            .transaction_id_filter(search_params)
            .from_date_filter(search_params)
            .to_date_filter(search_params)
            .person_filter(search_params)
            .max_amount_filter(search_params)
            .min_amount_filter(search_params)
            .status_filter(search_params)
            .order(created_at: :desc)
        }

  scope :user_transactions, ->(person) { where(person_id: person.id) }

  def self.process_auto_refund!(refund, amount, invoice_settlement)
    transaction = Payu::RefundService
                  .new(refund, amount, invoice_settlement.source)
                  .process!
  rescue PayuRefundTransactionError => e
    raise AutoRefunds::Utils::SettlementFailed.new(e.message)
  else
    transaction
  end

  def self.setup(invoice)
    create!(invoice: invoice, person: invoice.person, amount: invoice.outstanding_amount_in_local_currency)
    Payu::PaymentsApiClient.new.send_request(:new_payment, invoice: invoice).redirect
  end

  def failed!
    update(status: false)
  end

  def refresh!
    payu_response = payu_verify_payment
    log_response_to_s3(payu_response)
    update_payu_txn!(payu_response)

    unless payu_response.payment_succeeded?
      failed!
      raise PayuTransactionError, "PayU transaction failed" # this needs to be PhraseApp key
    end

    succeeded!
    self
  end

  def succeeded!
    update(status: true)
  end

  def success?
    status == true
  end

  private

  def log_response_to_s3(payu_response)
    PayuTransactionLogWorker.new.async.write_to_bucket(invoice.id, payu_response)
  end

  def marked_successful?
    saved_change_to_status? && status == true
  end

  def payu_verify_payment
    Payu::ApiClient.new.send_request(
      :verify_payment,
      invoice_id: invoice.id
    )
  end

  def settle_invoice!
    # we created the payu transaction based off outstanding_amount_cents in the local currency.
    invoice.settlement_received(self, invoice.outstanding_amount_cents)
    # TODO: autorefund handling for failed PayU invoices
    invoice.settled! if invoice.can_settle?
  end

  def update_payu_txn!(payu_response)
    update(
      payu_id: payu_response.transaction_id,
      payment_method: payu_response.payment_method,
      error_code: payu_response.error_code
    )
  end
end
