module ArelSongContributorsJoins
  def artist_join
    creative_t.join(artist_t).on(
      artist_t[:id].eq(creative_t[:artist_id])
    ).join_sources
  end

  def blacklisted_artist_join
    creative_t.join(blacklisted_artist_t, Arel::Nodes::OuterJoin).on(
      blacklisted_artist_t[:artist_id].eq(creative_t[:artist_id])
    ).join_sources
  end

  def creative_song_roles_join
    creative_t.join(
      creative_song_role_t, Arel::Nodes::OuterJoin
    ).on(
      creative_t[:id].eq(creative_song_role_t[:creative_id])
    ).join_sources
  end

  def song_roles_join
    creative_song_role_t.join(
      song_role_t, Arel::Nodes::OuterJoin
    ).on(
      creative_song_role_t[:song_role_id].eq(song_role_t[:id])
    ).join_sources
  end
end
