module ProviderSecretsEncryptable
  extend ActiveSupport::Concern

  def decrypted_password
    decrypted_secrets["password"]
  end

  def decrypted_hmac_key
    decrypted_secrets["hmac_key"]
  end

  private

  def decrypted_secrets
    @decrypted_secrets ||= JSON.parse(cipher_datum.decrypt)
  end

  def encrypt_attributes
    CipherDatum.encrypt(encrypt_text, self)
  end

  def encrypt_text
    JSON.generate(encryptable_attributes)
  end
end
