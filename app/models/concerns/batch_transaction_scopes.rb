module BatchTransactionScopes
  extend ActiveSupport::Concern

  included do
    scope :waiting_for_batch,
          ->(country_website) {
            includes(:person).where(
              invoices: {
                batch_status: "waiting_for_batch",
                settled_at: nil
              },
              people: {
                country_website_id: country_website.id
              }
            )
          }

    scope :unsettled_visible_to_customer,
          -> {
            where(batch_status: "visible_to_customer", settled_at: nil)
          }

    scope :batch_transaction_eligible,
          ->(date) {
            joins(<<-SQL.strip_heredoc)
            LEFT OUTER JOIN (
              SELECT
                invoice_id, count(*) AS count, max(created_at) AS batch_transactions_created_at
              FROM
                batch_transactions
              GROUP BY
                invoice_id
                )
              AS batch_transactions ON invoices.id = batch_transactions.invoice_id
            SQL
              .where("DATE(batch_transactions_created_at) BETWEEN DATE(:forty_two_days_ago) AND DATE(:twenty_days_ago)",
                     forty_two_days_ago: date - 42.days, twenty_days_ago: date - 20.days)
              .where("batch_transactions.count >= 1")
          }

    scope :extending_renewals_due_on,
          ->(date) {
            joins(<<-SQL.strip_heredoc)
      INNER JOIN purchases ON purchases.invoice_id = invoices.id and related_type = 'Renewal'
      INNER JOIN renewal_history rh1 ON rh1.purchase_id = purchases.id
      INNER JOIN renewal_history rh2 ON rh2.renewal_id = rh1.renewal_id
            SQL
              .where("DATE(rh1.starts_at) = DATE(:date) AND DATE(rh2.expires_at) = DATE(:date)", date: date)
          }
  end
end
