require "active_support/concern"

module Linkable
  extend ActiveSupport::Concern

  included do
    has_many :external_service_ids, as: :linkable
  end

  def external_ids_for(type, service_name)
    ExternalServiceId.ids_for(type, service_name)
  end

  def external_id_for(service_name)
    external_service_ids.where(service_name: service_name).first.try(:identifier)
  end

  def update_or_create_external_service_id(service_name, identifier, creatives)
    creatives.each do |c|
      external_service_id = c.external_service_ids.where(service_name: service_name, linkable_type: "Creative", linkable_id: c.id).try(:first)
      if external_service_id.present?
        external_service_id.update(identifier: identifier, state: "matched")
      else
        ExternalServiceId.create(service_name: service_name, linkable: c, identifier: identifier, state: "matched")
      end
    end
  end

  def set_external_id_for(service_name, identifier, state = nil)
    external_service_id = ExternalServiceId
                          .find_by(service_name: service_name,
                                   linkable_id: id,
                                   linkable_type: linkable_type)

    if external_service_id.present?
      external_service_id.update(
        identifier: identifier,
        state: state
      )
      Rails.logger.info(
        "ExternalServiceId updated ID for #{id}, identifier: #{identifier}, " \
                                "service_name: #{service_name}, state: #{state}"
      )
    else
      ExternalServiceId.create(
        service_name: service_name,
        identifier: identifier,
        state: state,
        linkable: self
      )
      Rails.logger.info(
        "ExternalServiceId created for #{id}, identifier: #{identifier}, " \
                                "service_name: #{service_name}, state: #{state}"
      )
    end
  end

  def linkable_type
    if instance_of?(Single) || instance_of?(Ringtone)
      "Album"
    else
      self.class.name
    end
  end
end
