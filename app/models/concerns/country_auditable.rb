module CountryAuditable
  extend ActiveSupport::Concern

  included do
    attr_accessor :country_audit_source

    has_many :country_audits

    after_commit :audit_country, if: :country_auditable?
  end

  def active_audit
    country_audits.last
  end

  def country_audit_source_changed?
    country_audit_source.present? && active_audit.present? && active_audit.country_audit_source.name != country_audit_source
  end

  def country_auditable?
    id_previously_changed? || country_previously_changed? || country_audit_source_changed?
  end

  def audit_country
    raise "missing country_audit_source" if country_audit_source.blank?

    country_audits.create(
      country: Country.search_by_iso_code(self[:country]),
      country_audit_source: CountryAuditSource.find_by(name: country_audit_source),
    )
  end
end
