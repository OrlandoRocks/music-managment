# frozen_string_literal: true

module AdyenError
  class AdyenTransactionError < StandardError
    attr_reader :response

    def initialize(msg: "Adyen Transaction Failed", response:)
      @response = response
      super(msg)
    end

    def message
      "#{msg}, response: #{response.inspect}"
    end
  end

  class AdyenUnauthorisedTransactionError < StandardError; end

  class AdyenTransactionRefundError < StandardError; end
end
