module ApplicationRecordMethods
  extend ActiveSupport::Concern

  included do
    def tc_instance_of?(klass)
      is_a?(klass) || is_a?("ReadOnlyReplica::#{klass}".constantize)
    end

    def class_name
      self.class.name.match(/ReadOnlyReplica::(.*)/)&.captures&.first || self.class.name
    end

    def base_class_name
      self.class.base_class.name.match(/ReadOnlyReplica::(.*)/)&.captures&.first || self.class.base_class.name
    end
  end
end
