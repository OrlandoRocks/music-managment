module SongMethods
  extend ActiveSupport::Concern

  included do
    belongs_to :album, inverse_of: :songs
  end
end
