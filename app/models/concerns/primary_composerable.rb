module PrimaryComposerable
  extend ActiveSupport::Concern

  def is_a_primary_composer?
    return true if instance_of?(Composer)
    return false if instance_of?(Cowriter)
    return true if instance_of?(PublishingComposer) && is_primary_composer?
    return false if instance_of?(PublishingComposer) && !is_primary_composer?

    false
  end

  def is_a_cowriting_composer?
    return false if instance_of?(Composer)
    return true if instance_of?(Cowriter)
    return false if instance_of?(PublishingComposer) && is_primary_composer?
    return true if instance_of?(PublishingComposer) && !is_primary_composer?

    false
  end
end
