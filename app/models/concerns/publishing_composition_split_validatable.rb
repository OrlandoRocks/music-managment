module PublishingCompositionSplitValidatable
  extend ActiveSupport::Concern

  private

  def validate_split_percentage_maximum(percentage_value)
    errors.add(
      :percent,
      I18n.t("model.publishing_composition_split.errors.total_splits_do_not_exceed_100_percent_error")
    ) if percentage_value.to_d > 100.to_d
  end

  def validate_split_percent_range(percent)
    errors.add(
      :percent,
      I18n.t("model.publishing_composition_split.errors.invalid_split_percent_error")
    ) if (percent.to_d < 0.to_d || percent.to_d > 100.to_d)
  end
end
