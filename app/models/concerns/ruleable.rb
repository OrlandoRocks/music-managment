# frozen_string_literal: true

# A concern that can be grafted on to any object instance
# that can be tied to configurable logic
module Ruleable
  extend ActiveSupport::Concern

  # attaches RulesEngine
  included do
    attach_engine!
  end

  # Maps method to the evaluation of a single rule for debugging purposes
  def evaluate_rule(enabled: true, name:, category:, against:)
    self.class.evaluate_rule(
      enabled: enabled,
      name: name,
      category: category,
      against: against
    )
  end

  # Maps method to all active business rules configured in the DB
  def evaluate_rules(against:, category: nil)
    self.class
        .evaluate_rules(against: against, category: category)
  end

  def evaluation_strategy(business_object, category = :no_category)
    self.class
        .evaluation_strategy(business_object, category)
  end

  def enabled_and_registered_rule_names(business_object, category = :no_category)
    self.class
        .evaluation_strategy(business_object, category)
        .map(&:first)
        .map(&:to_sym)
  end

  def self.included(klass)
    klass.extend(ClassMethods)
  end

  # @@engine[self] --- `self` Will always be the name of the class to which the module is attached and the
  # registry will be unique to that class.
  module ClassMethods
    class RuleableError < StandardError; end
    # Allows for the Engine Registry to be extended with more rules
    # in the context of the class `Rulable` being included on

    def attach_engine!
      @@engine ||= {}
      @@engine[self] =
        RulesEngine::RuleRegistry.define_rules do
          # Without this we wouldn't be able to express arrays/lists
          rule :list_of, ->(*values) { values }
        end
    end

    def define_rules(&block)
      extended_registry = @@engine[self].extend(&block).registry
      @@engine[self].registry.merge!(extended_registry)
    end

    # Evaluation of all active business rules configured in the DB
    def evaluate_rules(against:, category:)
      business_object = against
      expressions     = expressions(business_object, category)

      raise RuleableError, "No Rules found" if expressions.blank?

      @@engine[self].evaluate([:list_of, *expressions])
    end

    # Evaluation of a single rule for debugging purposes
    def evaluate_rule(enabled: true, name:, category:, against:)
      business_object = against
      rule            = EngineRule.find_by(name: name, category: category, enabled: enabled)

      raise RuleableError, "No Rule found" if rule.blank?

      @@engine[self].evaluate([rule.name, rule.config, business_object])
    end

    def evaluation_strategy(business_object, category)
      expressions(business_object, category)
    end

    def engine
      @@engine[self]
    end

    private

    # Converts business rules into s-expressions to be evaluated by rules engine
    def expressions(business_object, category = :no_category)
      active_rules = EngineRule.fetch_active_rules(category)
      active_rules.map do |rule|
        [rule.name, rule.config, business_object]
      end
    end
  end
end
