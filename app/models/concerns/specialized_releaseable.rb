require "active_support/concern"

module SpecializedReleaseable
  extend ActiveSupport::Concern

  SPECIALIZED_RELEASE_TYPE_TO_STORE = {
    "discovery_platforms" => :discovery_platforms
  }.freeze

  def is_general_discovery_release?(current_user = nil)
    @general_discovery_release ||= salepoints.discovery(current_user).any?
  end

  def only_discovery_release?(current_user)
    (salepoints
      .exclude_believe
      .pluck(:store_id) -
      Store.discovery_platforms(current_user).pluck(:id)
    )
      .empty?
  end

  def strictly_discovery_release?(current_user = nil)
    @strictly_freemium_release ||= is_general_discovery_release?(current_user) &&
                                   only_discovery_release?(current_user)
  end

  def strictly_facebook_release?
    @strictly_facebook_release ||= is_general_discovery_release? &&
                                   (salepoints.facebook_reels.any? && salepoints.exclude_believe.count == 1)
  end
end
