module Taxable
  extend ActiveSupport::Concern

  # Returns true if the person's total taxable earnings are equal to or surpass
  # the taxable threshold.
  def met_taxable_threshold?(revenue_streams: :all)
    revenue_streams = revenue_streams == :all ? RevenueStream::CODES : Array.wrap(revenue_streams)

    revenue_data =
      TaxReports::RevenueReportService.new(id, Date.current.year).generate_revenue_stats

    # Check if all specified revenue streams have reached the taxable threshold
    revenue_streams.all? do |revenue_stream|
      revenue_data[revenue_stream.to_sym][:earnings] >= TaxFormCheckService::TAXABLE_THRESHOLD
    end
  end

  # @deprecated
  # Always returns false. This needs to be removed along with the methods that call it.
  def tax_blocked?
    false
  end
  deprecate tax_blocked?: "tax blocking feature", deprecator: ActiveSupport::Deprecation.new(nil, "tc-www")

  # Returns true if the person has at least one tax dist or pub tax form.
  def valid_tax_forms?
    dist_or_pub_tax_forms.any?
  end

  # Returns the submitted_at time for the last dist or pub tax form submitted.
  def last_dist_or_pub_tax_form_submitted_at
    dist_or_pub_tax_forms.order(:submitted_at).pluck(:submitted_at).last
  end

  # Returns true if the person had already made a withdrawal after their most recent tax form
  # was submitted.
  def withdrawals_since_last_tax_form_submitted?
    start_date = last_dist_or_pub_tax_form_submitted_at

    paypal_transfers.completed_and_successful.exists?(created_at: start_date..) ||
      payout_transfers.completed_and_successful.exists?(created_at: start_date..) ||
      check_transfers.exists?(created_at: start_date..)
  end

  private

  def dist_or_pub_tax_forms
    tax_forms
      .current
      .joins(:revenue_streams)
      .merge(RevenueStream.with_distribution)
      .merge(RevenueStream.with_publishing)
  end
end
