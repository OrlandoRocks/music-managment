module Jsonable
  extend ActiveSupport::Concern

  def merge_attributes_as_json(new_attributes, options)
    merged_attributes = attributes.symbolize_keys
                                  .merge(new_attributes)
                                  .with_indifferent_access

    merged_attributes = merged_attributes.slice(*options[:only]) if options[:only].present?

    if include_root?(options)
      { root_name => merged_attributes.as_json }
    else
      merged_attributes.as_json
    end
  end

  private

  def root_name
    self.class.name.underscore.to_sym
  end

  def include_root?(options)
    if options.key?(:root)
      options[:root]
    else
      ActiveRecord::Base.include_root_in_json
    end
  end
end
