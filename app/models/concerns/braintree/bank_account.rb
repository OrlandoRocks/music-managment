module Braintree
  module BankAccount
    extend ActiveSupport::Concern

    def delete_braintree_record(ip_address)
      start_time = Time.current
      response = Braintree::Configuration.gateway.payment_method.delete(bt_token)
      self.status =
        if response.success? || response.message.match(/(Invalid Customer Vault Id)/)
          "destroyed"
        else
          "destruction_error"
        end
    rescue => e
      Airbrake.notify(e)
      self.status = "destruction_error"
    ensure
      save
      BraintreeVaultTransaction.log_deletion(
        self,
        ip_address,
        response,
        seconds_since(start_time)
      )
    end

    module ClassMethods
      def create_with_braintree(person, options = {})
        unless person.braintree_customer_id
          result = Braintree::Configuration.gateway.customer.create(
            {
              first_name: person.first_name,
              last_name: person.last_name,
            }
          )
          person.update(
            {
              braintree_customer_id: result.customer.id
            }
          )
        end
        result = Braintree::Configuration.gateway.payment_method.create(
          customer_id: person.braintree_customer_id,
          payment_method_nonce: options[:braintree_payment_nonce],
          options: {
            us_bank_account_verification_method: :network_check
          },
          billing_address: {
            street_address: options[:address1],
            extended_address: options[:address2],
            locality: options[:city],
            region: options[:state],
            postal_code: options[:zip],
            country_code_alpha2: options[:country],
            first_name: options[:checkname].split.first,
            last_name: options[:checkname].split.last,
          }
        )
        handle_braintree_result(result, person, nil, options)
      end

      def handle_braintree_result(result, person, bank_account = nil, options = {})
        if result.success?
          payment_method = result.payment_method
          bank_account = create_or_update_account(payment_method, person, bank_account, options)
          response_code = payment_method.verifications.first&.processor_response_code
          added_or_updated = bank_account.present? ? "updated" : "added"
          btvt = BraintreeVaultTransaction.create(
            person: person,
            related: bank_account,
            response_code: response_code || "success",
            ip_address: options[:ip_address],
            raw_response: payment_method.to_yaml
          )
          note = <<-NOTE
            User #{added_or_updated} a bank account,#{' '}
            ******#{bank_account.last_four_account}. (ID: #{bank_account.id}"
          NOTE

          Note.create(
            related: person,
            note_created_by: person,
            ip_address: options[:ip_address],
            subject: "Bank Account #{added_or_updated}",
            note: note
          )
          [true, btvt]
        else
          btvt = BraintreeVaultTransaction.create(
            person: person,
            response_code: "errored",
            ip_address: options[:ip_address],
            raw_response: result.to_yaml
          )
          [false, btvt]
        end
      end

      def create_or_update_account(payment_method, person, bank_account, options = {})
        params = {
          account_type: payment_method.account_type,
          address1: options[:address1],
          address2: options[:address2],
          bank_name: payment_method.bank_name,
          bt_token: payment_method.token,
          city: options[:city],
          country: options[:country],
          customer_vault_id: 0,
          ip_address: options[:ip_address],
          last_four_account: payment_method.last_4,
          last_four_routing: payment_method.routing_number[-4..-1],
          name: payment_method.account_holder_name,
          person: person,
          phone: options[:phone],
          state: options[:state],
          zip: options[:zip],
          status: payment_method.verified ? "valid" : "processing_error"
        }
        if bank_account
          bank_account.update(params)
        else
          bank_account = create(params)
        end
        bank_account
      end
    end
  end
end
