module PlanPurchaseMethods
  extend ActiveSupport::Concern

  def all_free_plan_purchases?
    purchases.all? do |purchase|
      purchase.cost_cents.zero? && purchase.credited? && purchase.related&.plan_credit
    end
  end

  def self.included(klass)
    klass.extend(ClassMethods)
  end

  module ClassMethods
    def additional_artist_plan_renewal_fails?(person, purchases)
      purchases.any?(&:additional_artist_purchaseable_plan_renewal?) &&
        purchases_sum_exceeds_person_balance(person, purchases)
    end

    def purchases_sum_exceeds_person_balance(person, purchases)
      Integer(purchases.sum(&:purchase_total_cents)) > Integer(person.person_balance.balance * 100)
    end
  end
end
