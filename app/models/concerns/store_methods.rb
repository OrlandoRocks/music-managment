module StoreMethods
  extend ActiveSupport::Concern

  included do
    has_many :sip_stores
  end
end
