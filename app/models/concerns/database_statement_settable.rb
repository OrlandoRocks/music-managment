module DatabaseStatementSettable
  extend ActiveSupport::Concern

  included do
    # Executes the relation with a temporary database timeout.
    #
    # Raises ActiveRecord::StatementInvalid exception with the message if max
    # statement time is exceeded:
    # "Mysql2::Error: Query execution was interrupted (max_statement_time exceeded)"
    #
    # This implementation is specific to MariaDB.
    scope :with_max_statement_time, ->(time) do
      find_by_sql("SET STATEMENT max_statement_time=#{time} FOR #{to_sql}")
    end
  end
end
