require "active_support/concern"

module ActiveRecordPolyfill
  extend ActiveSupport::Concern

  included do
    def self.find_or_create_by(attributes)
      where(attributes).first_or_create(attributes)
    end
  end
end
