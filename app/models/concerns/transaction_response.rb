module TransactionResponse
  extend ActiveSupport::Concern

  AVS_RESPONSE_DICT = {
    X: "Exact match, 9-character ZIP",
    Y: "Exact match, 5-character numeric ZIP",
    D: "Exact match, 5-character numeric ZIP",
    M: "Exact match, 5-character numeric ZIP",
    A: "Address match only",
    W: "9-character numeric ZIP match only",
    Z: "5-character ZIP match only",
    P: "5-character ZIP match only",
    L: "5-character ZIP match only",
    N: "No address or ZIP match",
    C: "No address or ZIP match",
    U: "Address unavailable",
    G: "Non-U.S. Issuer, does not participate I Non-U.S. Issuer, does not participate R Issuer system unavailable",
    E: "Not a mail/phone order",
    S: "Service not supported",
    0 => "AVS Not Available",
    O: "AVS Not Available",
    B: "AVS Not Available",
    I: "Postal Code not provided"
  }

  CVV_RESPONSE_DICT = {
    M: "CVV2/CVC2 Match",
    N: "CVV2/CVC2 No Match",
    P: "Not Processed",
    S: "Merchant has indicated that CVV2/CVC2 is not present on card",
    U: "Issue is not certified and/or has not provided Visa encryption keys",
    I: "CVV not provided",
    A: "CVV not applicable",
    B: "CVV skipped"
  }

  def self.process_transaction_params(transaction, response)
    # message doesn't exist in success response
    transaction.response_message = response.message if response.try(:message)
    transaction.transaction_status = response.transaction.status if response.try(:transaction).try(:status)

    response_txn = response.try(:transaction)
    # 3000 level error response object doesn't have response.transaction
    return unless response_txn

    transaction.processor_response_text       = response_txn.processor_response_text
    transaction.processor_response_type       = response_txn.processor_response_type
    transaction.additional_processor_response = response_txn.additional_processor_response
    transaction.network_response_text         = response_txn.network_response_text

    transaction.avs_response_code             = response_txn.avs_error_response_code
    transaction.avs_postal_response           = response_txn.avs_postal_code_response_code
    transaction.avs_street_address_response   = response_txn.avs_street_address_response_code
    transaction.cvv_response_code             = response_txn.cvv_response_code

    transaction.avs_message = parse_avs_message(transaction.avs_response_code) if transaction.avs_response_code
    if transaction.avs_postal_response
      transaction.avs_postal_message = parse_avs_message(transaction.avs_postal_response)
    end
    if transaction.avs_street_address_response
      transaction.avs_street_address_message = parse_avs_message(transaction.avs_street_address_response)
    end
    transaction.cvv_message = parse_cvv_message(transaction.cvv_response_code) if transaction.cvv_response_code
  end

  def self.parse_avs_message(key)
    AVS_RESPONSE_DICT[key&.to_sym]
  end

  def self.parse_cvv_message(key)
    CVV_RESPONSE_DICT[key&.to_sym]
  end
end
