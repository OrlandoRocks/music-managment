module BraintreePaymentSplit
  extend ActiveSupport::Concern

  # should return TC merchant if none of the payment splits feature flags are enabled
  # should return TC merchant if payment_splits_renewal is disabled and money_in_bi_transfer is enabled
  # should return BI merchant if both payment splits feature flags are enabled and a renewal transaction

  def braintree_payment_splits_config(person, options)
    return person.braintree_config_by_corporate_entity unless options[:is_renewal]
    return person.braintree_config_by_corporate_entity if person.payment_splits_renewal_enabled? && options[:is_renewal]

    tunecore_merchant(person)
  end

  private

  def tunecore_merchant(person)
    PayinProviderConfig.joins(:corporate_entity).find_by(
      corporate_entities:
      {
        name: CorporateEntity::TUNECORE_US_NAME
      },
      payin_provider_configs: {
        name: PayinProviderConfig::BRAINTREE_NAME,
        currency: person.site_currency
      }
    )
  end
end
