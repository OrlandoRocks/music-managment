require "active_support/concern"

module MagicNumberable
  extend ActiveSupport::Concern

  def valid_and_true_mp3?(mp3_file_path)
    MagicNumber.is_real?(mp3_file_path, extension: "mp3")
  end
end
