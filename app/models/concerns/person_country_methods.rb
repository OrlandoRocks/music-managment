module PersonCountryMethods
  extend ActiveSupport::Concern

  # As mentioned in the TODO comment on Person#country, we don't have
  # an ActiveRecord relationship between the Person and Country models.
  #
  # The Person#country method also doesn't always return a Country
  # object.
  #
  # That is why we need this method.
  def person_country_phone_code
    country = Country.find_by(name: self.country)
    return if country.nil?

    phone_codes = country.country_phone_codes
    phone_codes.count.zero? ? nil : phone_codes.first.code
  end
end
