module RefundMethods
  extend ActiveSupport::Concern

  def refunded_amount_cents
    refunds.success.sum(&:total_amount_cents)
  end

  def refundable_amount_cents
    return 0 unless settled?

    final_settlement_amount_cents - refunded_amount_cents
  end

  def refundable_purchases
    purchases.order(id: :asc).select(&:refundable?)
  end

  def refunds_with_credit_note_invoice
    refunds.non_backfill.order(id: :desc).select(&:show_credit_note_invoice?)
  end
end
