require "active_support/concern"

module LoginTrackable
  extend ActiveSupport::Concern

  included do
    has_many     :login_tracks, as: :trackable
    after_create :create_login_track
  end

  def create_login_track
    login_tracks.create(login_event: person.last_login_event) if person.last_login_event
  end
end
