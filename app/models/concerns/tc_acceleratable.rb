# frozen_string_literal: true

module TcAcceleratable
  extend ActiveSupport::Concern

  def tc_accelerator_opted_in?
    !tc_accelerator_opted_out? && distributed?
  end

  def tc_accelerator_opted_out?
    Person::FlagService.person_flag_exists?({ person: self, flag_attributes: Flag::TC_ACCELERATOR_OPTED_OUT })
  end

  def tc_accelerator_do_not_notify?
    Person::FlagService.person_flag_exists?({ person: self, flag_attributes: Flag::TC_ACCELERATOR_DO_NOT_NOTIFY })
  end

  def show_accelerator_cta?
    FeatureFlipper.show_feature?(:tc_accelerator, self) && distributed?
  end

  def show_accelerator_modal?(invoice = nil)
    FeatureFlipper.show_feature?(:tc_accelerator, self) &&
      !tc_accelerator_do_not_notify? &&
      invoice.includes_distribution_to_store?(:any)
  end
end
