module ContentFingerprintable
  def create_and_store_content_fingerprint!
    return unless song_file

    begin
      # This uses ETAG from S3 Object instead of downloading and computing hash.
      # Streaming or writing files from S3 were allocating too much memory for larger files
      # which is due to the presence of http_right_connection gem
      # The ETAG will no longer be MD5 if the song upload process is changed to use multipart upload.
      s3_etag = S3_CLIENT.buckets[song_file.bucket].objects[song_file.key].etag
      parsed_etag = JSON.parse(s3_etag)
      self.content_fingerprint = parsed_etag
      save!
    rescue => e
      Airbrake.notify("Content Fingerprint Generation Error for #{class_name}", e)
    end
  end
end
