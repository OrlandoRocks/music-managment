# frozen_string_literal: true

module InvoicePurchaseMethods
  extend ActiveSupport::Concern

  def credit_purchases?
    @credit_purchases ||=
      purchases.exists?(product_id: Product::CREDIT_PRODUCT_IDS)
  end

  def purchased_publishing_before?
    return false if settled_at.blank?

    person
      .invoices
      .joins(:purchases)
      .where("invoices.settled_at < ?", settled_at)
      .where(purchases: { related_type: "Composer" })
      .where.not("invoices.id = ?", id)
      .present?
  end

  def purchased_credits_before?
    @purchased_credits_before ||=
      begin
        settled_before_date = settled_at || Time.current

        person
          .invoices
          .settled_at_before(settled_before_date)
          .including_products(Product::CREDIT_PRODUCT_IDS)
          .where.not(invoices: { id: id })
          .exists?
      end
  end

  def paid_distribution?
    purchases.any? { |purchase| purchase.paid_release? || purchase.paid_plan? }
  end

  def ytm_purchase?
    purchases.any? { |p| p.related_type == "YoutubeMonetization" }
  end

  def publishing_purchase?
    purchases.any? { |p| p.related_type == "Composer" }
  end

  def album_or_credit_purchase?
    purchases.where(product_id: Product.distribution_product_ids + Product.paid_plan_products).present?
  end

  def first_purchase?
    return false if settled_at.blank?

    person.invoices.where("invoices.settled_at < ? AND invoices.id != ?", settled_at, id).blank?
  end

  def includes_distribution_to_store?(store_short_name = :any)
    # modified method to accept :any - method will return true if the invoice involves
    # distribution to any store, not just a specific store or array of stores
    ["Album", "CreditUsage", "Salepoint"].each do |via|
      distro_purchases = purchases.with_store_salepoints(via)
      result =
        if store_short_name == :any
          distro_purchases.exists?
        else
          distro_purchases.exists?({ stores: { short_name: store_short_name } })
        end
      return result if result
    end
    false
  end

  def purchased_publishing_recently?(time)
    person.invoices.joins("inner join purchases on invoices.id = purchases.invoice_id")
          .where([
                   "purchases.related_type = 'Composer' and invoices.settled_at >= ? and invoices.settled_at < ?",
                   settled_at - time,
                   settled_at
                 ]).present?
  end

  def purchased_first_distro_recently?(time)
    return false if first_distro_person_analytic.blank?

    i = Invoice.find(first_distro_person_analytic.metric_value)
    i.settled_at >= Time.now - time && i.settled_at < settled_at
  end
end
