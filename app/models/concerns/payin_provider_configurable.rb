module PayinProviderConfigurable
  extend ActiveSupport::Concern

  def braintree_config_by_corporate_entity
    return @braintree_config_by_corporate_entity unless @braintree_config_by_corporate_entity.nil?

    config_corporate_entity =
      if FeatureFlipper.show_feature?(:money_in_bi_transfer, self)
        corporate_entity
      else
        CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      end

    @braintree_config_by_corporate_entity = PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: config_corporate_entity,
      currency: site_currency
    )
  end

  def config_gateway_service
    return @config_gateway_service unless @config_gateway_service.nil?

    @config_gateway_service = Braintree::ConfigGatewayService.new(braintree_config_by_corporate_entity)
  end

  def config_gateway_service_by_country(country_code)
    return @config_gateway_service_by_country unless @config_gateway_service_by_country.nil?

    country = Country.find_by(iso_code: country_code)
    config = PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: country.corporate_entity,
      currency: site_currency
    )
    @config_gateway_service_by_country = Braintree::ConfigGatewayService.new(config)
  end

  def customer_in_config_gateway?
    config_gateway_service.customer_in_config_gateway?(braintree_customer_id)
  end
end
