require "active_support/concern"

module Upcable
  extend ActiveSupport::Concern

  included do
    has_many :upcs, as: :upcable, dependent: :destroy
    has_many :optional_upcs, -> { where(inactive: false, upc_type: "optional") }, class_name: "Upc", as: :upcable
    has_many :tunecore_upcs, -> { where(inactive: false, upc_type: "tunecore") }, class_name: "Upc", as: :upcable
    has_one  :physical_upc, -> { where(inactive: false, upc_type: "physical") }, class_name: "Upc", as: :upcable

    after_save :make_tunecore_upc, if: :needs_tunecore_upc?
    after_save :make_optional_upc, if: :needs_optional_upc?

    validate :optional_upc_is_valid?
  end

  module ClassMethods
    def find_upc(arg)
      upc = Upc.where(number: arg, upcable_type: name).first
      find(upc.upcable_id)
    end
  end

  def upc
    optional_upc || tunecore_upc
  end

  def tunecore_upc
    tunecore_upcs.first
  end

  def optional_upc
    optional_upcs.first
  end

  def has_optional_upc?
    optional_upc.present?
  end

  def optional_upc_number=(number)
    return if number.blank?

    @optional_upc_number = number.strip
  end

  def optional_upc_number
    @optional_upc_number || (optional_upc.number if optional_upc)
  end

  def tunecore_upc_number
    @tunecore_upc_number || (tunecore_upc.number if tunecore_upc)
  end

  def remove_optional_upcs
    optional_upcs.destroy_all unless optional_upcs.empty? || has_possibly_been_finalized?
  end

  def remove_physical_upc
    physical_upc.destroy unless physical_upc.blank? || has_possibly_been_finalized?
  end

  def assign_new_tunecore_upc!
    tunecore_upcs.create(upc_type: "tunecore")
  end

  def make_tunecore_upc
    tunecore_upcs.create(upc_type: "tunecore")
    true
  end

  def make_optional_upc
    optional_upcs.create(upc_type: "optional", number: @optional_upc_number)
    true
  end

  def needs_tunecore_upc?
    !tunecore_upc
  end

  def needs_optional_upc?
    !(@optional_upc_number.blank? || optional_upcs.find_by(number: @optional_upc_number))
  end

  def needs_physical_upc?
    stores.pluck(:abbrev).include?("aod_us") && !physical_upc
  end

  def optional_upc_is_valid?
    if @optional_upc_number.present? && !optional_upcs.detect { |upc| upc.number == @optional_upc_number }
      new_upc = optional_upcs.new(number: @optional_upc_number, upc_type: "optional")
      new_upc.valid?
      new_upc.errors.each do |_attr, message|
        errors.add(:optional_upc_number, message.titleize)
      end
    else
      true
    end
  end
end
