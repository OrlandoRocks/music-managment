module SalesRecordMethods
  extend ActiveSupport::Concern

  included do
    belongs_to :sales_record_master
    belongs_to :person
    belongs_to :person_intake
    belongs_to :related, polymorphic: true
  end

  class_methods do
    # returns the months a user has sales for grouped by sales period
    def months_with_sales(person)
      select("srm.period_sort as sales_period")
        .where(sr: { person_id: person })
        .from("sales_record_summaries sr")
        .joins("inner join sales_record_masters srm ON srm.id = sr.sales_record_master_id")
        .group("srm.period_sort")
        .order("sales_period DESC")
    end

    # returns the months a user has sales for grouped by reporting period
    def reporting_months_with_sales(person)
      select("srm.created_at as reporting_period")
        .where(sr: { person_id: person })
        .from("sales_record_summaries sr")
        .joins("inner join sales_record_masters srm ON srm.id = sr.sales_record_master_id")
        .group("MONTH(srm.created_at), YEAR(srm.created_at)")
        .order("srm.created_at DESC")
    end

    # Set the select fields that are used in the sale report downloads
    def export_information_select(sales_record_alias)
      <<-SQL.strip_heredoc
        #{sales_record_alias}.id,
          CASE
            WHEN a.id IS NOT NULL THEN a.id
            ELSE album_sales.id
          END AS album_id,
          CASE
            WHEN a.id IS NOT NULL THEN a.name
            WHEN album_sales.id IS NOT NULL THEN album_sales.name
            WHEN v.id IS NOT NULL THEN v.name
          END AS release_title,
          CASE
            WHEN a.is_various = 1 THEN 'Various Artists'
            WHEN artists.id IS NOT NULL THEN GROUP_CONCAT(DISTINCT artists.name separator ' & ')
            WHEN aa.id IS NOT NULL THEN GROUP_CONCAT(DISTINCT aa.name separator ' & ')
            WHEN aas.id IS NOT NULL THEN GROUP_CONCAT(DISTINCT aas.name separator ' & ')
          END AS artist_name,
          #{sales_record_alias}.related_type AS release_type,
          s.name AS song_title,srm.period_sort,
          #{sales_record_alias}.quantity,
          #{sales_record_alias}.revenue_per_unit,
          #{sales_record_alias}.revenue_total AS revenue_total,
          srm.revenue_currency AS revenue_currency,
          srm.amount_currency AS amount_currency,
          TRUNCATE(srm.exchange_rate,2) AS exchange_rate,
          #{sales_record_alias}.amount AS amount,
          ssdg.name AS store_name,
          srm.country,
          CASE
            WHEN u.id IS NOT NULL THEN u.number
            ELSE uas.number
          END AS upc,
          CASE
            WHEN u_optional_upc.id IS NOT NULL THEN u_optional_upc.number
            ELSE uas_optional_upc.number
          END AS optional_upc,
          date_format(srm.created_at, '%Y-%m-%d') AS posted_date,
          #{sales_record_alias}.distribution_type,
          CASE
            WHEN l.id IS NOT NULL THEN l.name
            WHEN label_albums.id IS NOT NULL THEN label_albums.name
            WHEN label_videos.id IS NOT NULL THEN label_videos.name
          END AS label_name,
          s.tunecore_isrc AS isrc,
          s.optional_isrc AS optional_isrc
      SQL
    end

    # Set the joins that are used in the sale report downloads
    def export_information_join(sales_record_alias)
      <<-SQL.strip_heredoc
        INNER JOIN sales_record_masters srm ON srm.id = #{sales_record_alias}.sales_record_master_id
        INNER JOIN sip_stores ss ON ss.id = srm.sip_store_id
        INNER JOIN sip_stores_display_groups ssdg ON ssdg.id = ss.display_group_id
        LEFT JOIN videos v ON v.id = #{sales_record_alias}.related_id
          AND #{sales_record_alias}.related_type = 'Video'
        LEFT JOIN songs s ON s.id = #{sales_record_alias}.related_id
          AND #{sales_record_alias}.related_type = 'Song'
        LEFT JOIN albums album_sales ON album_sales.id = #{sales_record_alias}.related_id
          AND #{sales_record_alias}.related_type = 'Album'
        LEFT JOIN albums a ON a.id = s.album_id
        LEFT JOIN upcs u ON u.upcable_id = a.id
          AND u.upcable_type = 'Album'
          AND u.inactive = 0
          AND u.upc_type = 'tunecore'
        LEFT JOIN upcs uas ON uas.upcable_id = album_sales.id
          AND uas.upcable_type = 'Album'
          AND uas.inactive = 0
          AND uas.upc_type = 'tunecore'
        LEFT JOIN upcs u_optional_upc ON u_optional_upc.upcable_id = a.id
          AND u_optional_upc.upcable_type = 'Album'
          AND u_optional_upc.inactive = 0
          AND u_optional_upc.upc_type = 'optional'
        LEFT JOIN upcs uas_optional_upc ON uas_optional_upc.upcable_id = album_sales.id
          AND uas_optional_upc.upcable_type = 'Album'
          AND uas_optional_upc.inactive = 0
          AND uas_optional_upc.upc_type = 'optional'
        LEFT JOIN labels l ON l.id = a.label_id
        LEFT JOIN labels label_albums ON label_albums.id = album_sales.label_id
        LEFT JOIN labels label_videos ON label_videos.id = v.label_id
        LEFT JOIN creatives c ON c.creativeable_id = a.id
          AND c.creativeable_type = 'Album'
          AND c.role = 'primary_artist'
        LEFT JOIN artists ON artists.id = c.artist_id
        LEFT JOIN creatives cas ON cas.creativeable_id = album_sales.id
          AND cas.creativeable_type = 'Album'
          AND cas.role = 'primary_artist'
        LEFT JOIN artists aas ON aas.id = cas.artist_id
        LEFT JOIN creatives cc ON cc.creativeable_id = s.id
          AND cc.creativeable_type = 'Song'
          AND cc.role = 'primary_artist'
        LEFT JOIN artists aa ON aa.id = cc.artist_id
      SQL
    end
  end

  class_methods do
    def album_query(person, release)
      select(
        "a.name as album_name,
        a.album_type as release_type,
        g.name as genre,
        ai.name as artist_name,
        l.name as label_name,
        u.number as upc_number,
        uo.number as optional_upc_number"
      )
        .from("albums a")
        .joins(
          "left join genres g ON g.id = a.primary_genre_id
          left join labels l On l.id = a.label_id
          left join upcs u ON upcable_id = a.id and u.upcable_type = 'Album' and u.upc_type = 'tunecore'
          left join upcs uo
            ON uo.upcable_id = a.id and uo.upcable_type = 'Album' and uo.upc_type = 'optional' and uo.inactive = 0
          left join creatives c ON c.creativeable_id = a.id and creativeable_type = 'Album'
          left join artists ai ON ai.id = c.artist_id"
        )
        .where("a.person_id = ? and a.id = ?", person.id, release.id)
        .order("a.name")
        .first
    end

    def single_query(person, release)
      select(
        "a.name as album_name,
        a.album_type as release_type,
        g.name as genre,
        ai.name as artist_name,
        l.name as label_name,
        u.number as upc_number,
        uo.number as optional_upc_number"
      )
        .from("#{table_name} sr")
        .joins(
          "left join songs s ON s.id = sr.related_id
          left join albums a ON a.id = s.album_id
          left join genres g ON g.id = a.primary_genre_id
          left join labels l ON l.id = a.label_id
          left join upcs u ON u.upcable_id = a.id and u.upcable_type = 'Album' and u.upc_type = 'tunecore'
          left join upcs uo
            ON uo.upcable_id = a.id and uo.upcable_type = 'Album' and uo.upc_type = 'optional' and uo.inactive = 0
          left join creatives c
            ON c.creativeable_id = a.id and creativeable_type = 'Album' and c.role = 'primary_artist'
          left join artists ai ON ai.id = c.artist_id"
        )
        .where("sr.person_id = ? and a.id = ? and a.album_type = ?", person.id, release.id, release.class_name)
        .order("a.name")
        .first
    end

    def ringtone_query(person, release)
      select(
        "a.name as name,
        a.album_type as release_type,
        l.name as label_name,
        u.number as upc_number,
        uo.number as optional_upc_number"
      )
        .from("#{table_name} sr")
        .joins(
          "left join songs ss ON ss.id = sr.related_id
          left join albums a ON a.id = ss.album_id
          left join upcs u
            ON u.upcable_id = a.id and u.upc_type = 'tunecore'
          left join upcs uo ON uo.upcable_id = a.id and uo.upc_type = 'optional' and uo.inactive = 0
          left join labels l ON l.id = a.label_id"
        )
        .where("sr.person_id = ? and a.id = ?", person.id, release.id)
        .order("a.name")
        .first
    end

    def video_query(person, release)
      select(
        "*,
        v.name as name,
        l.name as label_name,
        u.number as upc,
        'Video' as release_type"
      )
        .from("#{table_name} sr")
        .joins(
          "left join videos v ON v.id = sr.related_id and sr.related_type = 'Video'
        left join upcs u ON u.upcable_id = v.id AND u.upcable_type = 'Video' and u.upc_type = 'tunecore'
        left join labels l ON l.id = v.label_id"
        )
        .where("sr.person_id = ? and v.id = ?", person.id, release.id)
        .order("v.name")
        .first
    end

    # returns the details of a release to be displayed on the individual release
    # details page(label, artist name, upc etc)
    def get_album_release_details(person, release, options = {})
      # clean this up
      case options[:related_type]
      when "Album"
        album_query(person, release)
      when "Single"
        single_query(person, release)
      when "Ringtone"
        ringtone_query(person, release)
      when "Video"
        video_query(person, release)
      end
    end
  end
end
