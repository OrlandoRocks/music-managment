module PriorityArtistable
  extend ActiveSupport::Concern

  def valid_priority_artist
    return if priority_artist.blank?

    priority_artist.validate_or_reset_artist
  end
end
