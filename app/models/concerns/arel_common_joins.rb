module ArelCommonJoins
  include ArelTableMethods
  include ArelNamedMysqlFunctions

  def albums_petri_bundle_join
    album_t.join(
      petri_bundle_t
    ).on(
      album_t[:id].eq(petri_bundle_t[:album_id])
    ).join_sources
  end

  def petri_bundle_distribution_join
    petri_bundle_t.join(
      distribution_t
    ).on(
      petri_bundle_t[:id].eq(distribution_t[:petri_bundle_id])
    ).join_sources
  end

  def albums_to_songs
    album_t.join(song_t).on(
      album_t[:id].eq(song_t[:album_id])
    ).join_sources
  end

  def songs_yttracks_outer_join
    song_t.join(
      track_monetization_t, Arel::Nodes::OuterJoin
    ).on(
      song_t[:id].eq(track_monetization_t[:song_id]).and(track_monetization_t[:store_id].eq(Store::YTSR_STORE_ID))
    ).join_sources
  end

  def songs_fbtracks_outer_join
    song_t.join(
      track_monetization_t, Arel::Nodes::OuterJoin
    ).on(
      song_t[:id].eq(track_monetization_t[:song_id]).and(track_monetization_t[:store_id].eq(Store::FBTRACKS_STORE_ID))
    ).join_sources
  end

  def creative_external_service_ids
    creative_t.join(external_service_id_t).on(
      external_service_id_t[:linkable_id].eq(creative_t[:id])
      .and(external_service_id_t[:linkable_type].eq("Creative"))
    ).join_sources
  end

  def artist_external_service_ids
    external_service_id_t.join(creative_external_service_ids).join(artist_t, Arel::Nodes::InnerJoin).on(
      artist_t[:id].eq(creative_t[:artist_id])
    ).join_sources
  end

  def blacklisted_artists_join
    artist_t.join(blacklisted_artist_t, Arel::Nodes::OuterJoin).on(
      blacklisted_artist_t[:artist_id].eq(artist_t[:id])
    ).join_sources
  end

  def albums_to_artists_join
    album_t.join(album_creative_t).on(
      album_t[:id].eq(album_creative_t[:creativeable_id])
      .and(album_creative_t[:creativeable_type].eq("Album"))
    ).join(album_artist_t).on(
      album_creative_t[:artist_id].eq(album_artist_t[:id])
    ).join_sources
  end

  def songs_to_artists_outer_join
    song_t.join(song_creative_t, Arel::Nodes::OuterJoin).on(
      song_t[:id].eq(song_creative_t[:creativeable_id])
      .and(song_creative_t[:creativeable_type].eq("Song"))
    ).join(song_artist_t, Arel::Nodes::OuterJoin).on(
      song_creative_t[:artist_id].eq(song_artist_t[:id])
    ).join_sources
  end

  def artists_creatives_outer_join
    artist_t.join(creative_t, Arel::Nodes::OuterJoin).on(
      creative_t[:artist_id].eq(artist_t[:id])
    ).join_sources
  end

  def creatives_albums_outer_join
    creative_t.join(album_t, Arel::Nodes::OuterJoin).on(
      creative_t[:creativeable_id].eq(album_t[:id])
    ).join_sources
  end

  def albums_salepoints_outer_join
    album_t.join(salepoint_t, Arel::Nodes::OuterJoin).on(
      salepoint_t[:salepointable_id].eq(album_t[:id])
      .and(salepoint_t[:salepointable_type].eq("Album"))
    ).join_sources
  end

  def salepoints_distributions_outer_join
    "LEFT OUTER JOIN distributions_salepoints dsp on dsp.salepoint_id = salepoints.id LEFT OUTER JOIN distributions on distributions.id = dsp.distribution_id"
  end

  def album_creative_t
    creative_t.alias("album_creative_t")
  end

  def album_artist_t
    artist_t.alias("album_artist_t")
  end

  def song_creative_t
    creative_t.alias("song_creative_t")
  end

  def song_artist_t
    artist_t.alias("song_artist_t")
  end
end
