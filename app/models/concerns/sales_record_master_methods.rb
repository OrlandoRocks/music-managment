module SalesRecordMasterMethods
  extend ActiveSupport::Concern

  included do
    scope :earliest_and_latest_sales_record_master, ->(user) {
      select("MIN(sales_record_masters.period_sort) AS earliest, MAX(sales_record_masters.period_sort) AS latest")
        .from("sales_record_masters")
        .joins(
          "INNER JOIN sales_record_summaries ON sales_record_summaries.sales_record_master_id = sales_record_masters.id"
        )
        .where(sales_record_summaries: { person_id: user.id })
    }
  end
end
