module InvoiceRenewable
  extend ActiveSupport::Concern

  def contains_album_renewals?
    purchases
      .preload(related: :renewal_items)
      .where(purchases: { related_type: "Renewal" })
      .map(&:renewal)
      .compact
      .map(&:renewal_items)
      .map(&:first)
      .map(&:related_type)
      .include?("Album")
  end
end
