module CreativeMethods
  extend ActiveSupport::Concern

  included do
    belongs_to :artist, autosave: true

    def name
      artist.name rescue nil
    end
  end
end
