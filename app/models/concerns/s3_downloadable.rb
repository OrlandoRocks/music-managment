module S3Downloadable
  def s3_url(expires_in, check_flac = false)
    asset = s3_streaming_asset || (check_flac && s3_flac_asset) || s3_asset || s3_orig_asset

    generate_s3_url(expires_in: expires_in, asset: asset)
  end

  def spatial_audio_url(expires_in, _check_flac = false)
    generate_s3_url(expires_in: expires_in, asset: spatial_audio_asset)
  end

  def s3_original_url(expires_in, check_flac = false)
    asset = s3_orig_asset || s3_asset || (check_flac && s3_flac_asset) || s3_streaming_asset
    generate_s3_url(expires_in: expires_in, asset: asset)
  end

  private

  def generate_s3_url(expires_in:, asset: nil)
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = expires_in
    generator.get(asset.bucket, asset.key) if asset
  end
end
