module Processable
  extend ActiveSupport::Concern

  included do
    enum status: {
      unprocessed: "new",
      processing: "processing",
      processed: "processed",
      failed: "failed",
    }
  end
end
