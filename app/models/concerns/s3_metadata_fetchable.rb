# frozen_string_literal: true

module S3MetadataFetchable
  class FFProbeError < RuntimeError
  end

  FFPROBE_URI = ENV["FFPROBE_URI"] || "https://ud7rdcvjk9.execute-api.us-east-1.amazonaws.com/qa/ffprobe"

  def set_asset_metadata!
    s3d = s3_detail || S3Detail.new(s3_asset_id: id)
    metadata = fetch_asset_metadata
    s3d.metadata_json = metadata
    s3d.file_size = metadata["format"]["size"].to_i
    s3d.file_type = metadata["format"]["format_name"]
    s3d.save!
  end

  def fetch_asset_metadata
    resp = HTTParty.post(FFPROBE_URI, body: ffprobe_body, headers: ffprobe_headers)
    raise FFProbeError.new("Call to FFProbe Failed -- #{resp.body}") if resp.code != 200

    JSON.parse resp.body
  end

  def ffprobe_body
    {
      source: {
        bucket: bucket,
        key: key
      }
    }.to_json
  end

  def ffprobe_headers
    {
      "Content-Type": "application/json"
    }
  end

  def backfill_album_metadata(album_id)
    GetMetadataPriorityWorker.perform_async(album_id)
  end
end
