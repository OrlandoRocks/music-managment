require "memoist"
module Planable
  extend ActiveSupport::Concern
  extend Memoist

  def can_do?(feature, include_carted_plan: false)
    return Plans::CanDoService.can_do?(plan, feature) unless include_carted_plan

    Plans::CanDoService.can_do?(plan, feature) || Plans::CanDoService.can_do?(cart_plan, feature)
  end

  def feature_gating_can_do?(feature, include_carted_plan: false)
    return true if !FeatureFlipper.show_feature?(:plans_pricing, self) || has_admin_access_to_feature?(feature)

    can_do?(feature, include_carted_plan: include_carted_plan)
  end

  def can_use_store_expander?
    return true unless FeatureFlipper.show_feature?(:plans_pricing, self)
    return false unless has_plan?

    can_do?(:store_expander)
  end

  def eligible_for_free_automator_product?
    return false unless has_plan? || owns_or_carted_plan?

    FeatureFlipper.show_feature?(:plans_pricing, self) && can_do?(:store_automator, include_carted_plan: true)
  end

  def can_toggle_automator?
    return false unless has_plan? || owns_or_carted_plan?

    FeatureFlipper.show_feature?(:plans_pricing, self) && can_do?(:store_automator)
  end

  def active_releases?
    purchased_distribution_credits_before? || distributed?
  end

  def active_additional_artists?
    additional_artists.paid.active.count.positive?
  end

  def no_plan_and_active_releases?
    legacy_user? && active_releases?
  end

  def plan_user?
    person_plan.present?
  end

  def has_plan?
    plan.present?
  end

  def owns_or_carted_plan?
    has_plan? || plan_in_cart?
  end

  def legacy_user?
    !has_plan?
  end

  def current_plan?(plan_id)
    has_plan? && plan.id == plan_id
  end

  def plan_in_cart?
    purchases.unpaid.not_in_invoice.exists?(product_id: Product::PLAN_PRODUCT_IDS)
  end

  def number_of_plans_in_cart
    purchases.unpaid.not_in_invoice.where(product_id: Product::PLAN_PRODUCT_IDS).count
  end

  def cart_plan
    return if cart_plan_purchase.blank?

    cart_plan_purchase.product.plan
  end

  def cart_plan_purchase
    return unless plan_in_cart?

    purchases.unpaid.not_in_invoice.find(&:is_plan?)
  end

  def most_recent_plan_purchase
    purchases
      .paid
      .order(paid_at: :desc)
      .find_by(product_id: Product::PLAN_PRODUCT_IDS)
  end

  def has_admin_access_to_feature?(feature)
    return false unless (feature.in? PlanFeature::ADMIN_ACCESSIBLE_FEATURES) && takeover_admin_id

    takeover_admin = Person.find(takeover_admin_id)
    takeover_admin.is_trends_and_sales_admin?
  end

  def oldest_primary_artist
    creatives
      .active
      .order(id: :desc)
      .last
      &.artist
  end

  def user_or_cart_plan?(plan_id)
    [cart_plan&.id, plan&.id].include?(plan_id)
  end

  def active_splits_collaborator?
    splits_collaborator_addons.active.paid.exists?
  end

  def purchased_create_splits?
    # NOTE: We use "splits_collaborator" feature for access to creating splits,
    # although "collaborators" might better describe users that have purchased
    # the splits collaborator addon (see: can_accept_splits?)

    has_plan? && can_do?(:splits_collaborator)
  end
  memoize :purchased_create_splits?

  def can_create_splits?
    return false unless show_splits_based_on_feature_flag?
    return false if flagged_suspicious?
    return false unless has_tax_forms_for_splits?

    purchased_create_splits?
  end
  memoize :can_create_splits?

  def can_accept_splits?
    return false unless show_invited_splits_based_on_feature_flag?
    return false if flagged_suspicious?
    return false unless has_tax_forms_for_splits?

    # NOTE: The only users that can accept splits but not create splits,
    # are the users with an active splits_collaborator product subscription.
    purchased_create_splits? || active_splits_collaborator?
  end
  # TODO: Remove alias after splits is launched/MVP features merged
  memoize :can_accept_splits?

  def has_unaccepted_split_invites?
    royalty_split_recipients.pending_invites.exists?
  end

  def has_split_invites?
    royalty_split_recipients.collaborators.exists?
  end

  def has_tax_forms_for_splits?
    return true unless from_united_states_and_territories?

    tax_forms.current_active_w9.exists?
  end

  def show_splits_based_on_feature_flag?
    return true if FeatureFlipper.show_feature?(:royalty_splits, self)
    return plan&.id == Plan::PROFESSIONAL && has_tax_forms_for_splits? if FeatureFlipper.show_feature?(
      :royalty_splits_beta, self
    )

    false
  end
  memoize :show_splits_based_on_feature_flag?

  def show_invited_splits_based_on_feature_flag?
    return true if FeatureFlipper.show_feature?(:royalty_splits, self)
    return plan&.id == Plan::PROFESSIONAL || has_split_invites? if FeatureFlipper.show_feature?(
      :royalty_splits_beta, self
    )

    false
  end
  memoize :show_invited_splits_based_on_feature_flag?

  def add_splits_collaborator_to_cart!
    ApplicationRecord.transaction(requires_new: true) do
      splits_collaborator = SplitsCollaboratorAddon.find_or_create_by!(person: self, canceled_at: nil, paid_at: nil)
      product = Product.find_by(
        country_website: country_website,
        display_name: Product::SPLITS_COLLABORATOR_DISPLAY_NAME
      )
      Product.add_to_cart(self, splits_collaborator, product)
    end
  end
end
