module StripEmojiMethods
  def strip_emoji(string)
    four_byte_chars = Utf8ParserService.new(string, "").four_byte_chars
    four_byte_chars.uniq.each { |a| string.gsub!(a, "") }
    string
  end

  def emoji_stripped_address_attributes
    address_attributes = person.attributes.slice("address1", "address2", "city", "state")
    address_attributes.each { |k, v| address_attributes[k] = strip_emoji(v) }
  end
end
