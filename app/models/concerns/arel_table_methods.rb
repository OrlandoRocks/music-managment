module ArelTableMethods
  ActiveRecord::Base.connection.tables.each do |table|
    define_method("#{table.singularize.tr('/', '_')}_t") do
      table.classify.constantize.arel_table if defined?(table.classify.constantize)
    end
  end
end
