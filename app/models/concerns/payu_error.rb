# frozen_string_literal: true

module PayuError
  class PayuTransactionError < StandardError; end

  class PayuRefundTransactionError < StandardError
    ERROR_MESSAGE_PREFIX = "PayU transaction failed"

    attr_reader :error_code

    def initialize(error_code = nil, msg = nil)
      super(formatted_msg(msg))
      @error_code = error_code
    end

    private

    def formatted_msg(msg)
      [ERROR_MESSAGE_PREFIX, msg].reject(&:blank?).join(": ")
    end
  end
end
