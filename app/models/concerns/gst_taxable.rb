require "active_support/concern"

module GstTaxable
  extend ActiveSupport::Concern

  def tax_amount(gst_config, total_amount)
    OpenStruct.new(tax_split(gst_config, total_amount)) if gst_config
  end

  def base_amount(gst_config, total_amount)
    return total_amount unless gst_config

    total_tax =
      tax_split(gst_config, total_amount)
      .reduce(0) { |total, (_type, value)| total + value }
    total_amount - total_tax
  end

  def updated_gst_calc_date
    @updated_gst_calc_date ||= ENV["UPDATED_GST_CALC_DATE"]
  end

  def date_of_new_gst_calc?
    paid_date =
      if is_a?(Invoice)
        settled_at
      elsif is_a?(Purchase)
        paid_at
      end

    updated_gst_calc_date.present? && paid_date.present? && paid_date > updated_gst_calc_date.to_date
  end

  private

  def tax_split(gst_config, total)
    return new_gst_calculation(gst_config, total) if date_of_new_gst_calc?

    old_gst_calculation(gst_config, total)
  end

  def new_gst_calculation(gst_config, total)
    return { igst: total - (total / (1 + (gst_config.igst / 100))) } if gst_config.igst?

    cgst = gst_config.cgst
    sgst = gst_config.sgst

    gst_dividend = (1 + (cgst / 100) + (sgst / 100))
    total_tax_amount = total - (total / gst_dividend)

    total_gst_percentage = cgst + sgst
    percent_cgst = cgst / total_gst_percentage
    percent_sgst = sgst / total_gst_percentage

    cgst_amount = total_tax_amount * percent_cgst
    sgst_amount = total_tax_amount * percent_sgst

    {
      cgst: cgst_amount,
      sgst: sgst_amount,
    }
  end

  def old_gst_calculation(gst_config, total)
    return { igst: total * (gst_config.igst / 100) } if gst_config.igst?

    {
      cgst: total * (gst_config.cgst / 100),
      sgst: total * (gst_config.sgst / 100),
    }
  end
end
