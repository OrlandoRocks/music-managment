module TransferMethods
  extend ActiveSupport::Concern

  def dollar_amount
    Money.new(send(amount_attribute_name), person.currency)
  end
  alias_method :requested_withdrawal_amount, :dollar_amount

  def flagged?
    case self.class.to_s
    when "PaypalTransfer"
      (person.status != "Active") || suspicious?("paypal_address")
    when "PayoutTransfer"
      person.suspicious? || withdraw_method_changed
    end
  end

  private

  def amount_attribute_name
    instance_of?(PaypalTransfer) ? :payment_cents : :amount_cents
  end
end
