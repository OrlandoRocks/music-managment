module SipStoreMethods
  extend ActiveSupport::Concern

  TIK_TOK_SIP_STORE_ID = 474
  TENCENT_MUCOIN_SIP_STORE_ID = 479

  included do
    has_many :sales_record_masters
    belongs_to :display_group, class_name: "SipStoresDisplayGroup"
    belongs_to :store

    scope :earliest_and_latest_sales_record_master, ->(user) {
      select("MIN(sales_record_masters.period_sort) AS earliest, MAX(sales_record_masters.period_sort) AS latest")
        .from("sales_record_masters")
        .joins(
          "INNER JOIN sales_record_summaries ON sales_record_summaries.sales_record_master_id = sales_record_masters.id"
        )
        .where(sales_record_summaries: { person_id: user.id })
    }

    scope :find_sales_record_master_by_group_and_date, ->(display_group_id, month) {
      joins(<<-SQL.strip_heredoc)
        INNER JOIN sales_record_masters srm ON srm.sip_store_id = sip_stores.id
        INNER JOIN sip_stores_display_groups ssgd ON ssgd.id = sip_stores.display_group_id
      SQL
        .where(<<-SQL.strip_heredoc, display_group_id, month, month)
        display_group_id = ?
          AND ssgd.group_on_status = TRUE
          AND MONTH(srm.period_sort) = MONTH(?)
          AND YEAR(srm.period_sort) = YEAR(?)
        SQL
        .limit(1)
    }

    def sales_for_month?(month)
      sales_record_masters.exists?(["MONTH(period_sort) = MONTH(?) AND YEAR(period_sort) = YEAR(?)", month, month])
    end

    def sales_for_month_in_group?(month, display_group_id)
      if SipStore.find_sales_record_master_by_group_and_date(display_group_id, month).empty?
        false
      else
        true
      end
    end
  end

  class_methods do
    def find_sip_stores_for_person_grouped_by_display(person_id)
      sip_store_display_groups = select("ssdg.name as store_name, ss.display_group_id")
                                 .from("sip_stores ss")
                                 .joins("left join sales_record_masters srm ON srm.sip_store_id = ss.id
                left join sales_record_summaries sr ON sr.sales_record_master_id = srm.id
                left join stores s ON s.id = ss.store_id
                left join sip_stores_display_groups ssdg ON ssdg.id = ss.display_group_id")
                                 .where(sr: { person_id: person_id })
                                 .group("ss.display_group_id")
                                 .order("ssdg.sort_order asc")

      adjust_data_for_updated_stores(sip_store_display_groups)
    end

    def adjust_data_for_updated_stores(sip_store_display_groups)
      sip_store_display_groups.map do |display_group|
        display_group.store_name = "Tidal" if display_group.store_name == "WiMP"
      end

      sip_store_display_groups
    end
  end
end
