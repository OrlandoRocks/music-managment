module UpcChecksumable
  extend ActiveSupport::Concern

  # calculate checksum for UPC-A standard
  # as described at http://en.wikipedia.org/wiki/Universal_Product_Code#Check_digit_calculation
  def checksum(value)
    if ean?(value)
      characters = value[0, 12].chars
      raise "Cannot calculate checksum on invalid number" if characters.size != 12
    else # if upc?
      characters = value[0, 11].chars
      raise "Cannot calculate checksum on invalid number" if characters.size != 11
    end

    # split digits into odd and even buckets by position
    odds = []
    evens = []
    while !characters.empty? do
      odds << characters.shift.to_i
      evens << characters.shift.to_i unless characters.empty?
    end

    if ean?(value)
      # multiply each even (odd counting from the back) digits by 3 and add the sum to the sum of odd digits
      evens.collect! { |x| x * 3 }
      summed_values = odds.sum + evens.sum
    else # if upc?
      # multiply sum of odd digits by 3 and add sum of even digits
      summed_values = (odds.sum * 3) + evens.sum
    end

    # find one digit integer that when added to summed_values would create a multiple of 10
    (summed_values % 10).zero? ? 0 : 10 - (summed_values % 10)
  end

  def checksum_matches?(number)
    number.last == checksum(number).to_s
  end

  # Sometimes we get EANs instead of UPCs
  def upc?(number)
    number && number.length == 12
  end

  def ean?(number)
    number && number.length == 13
  end

  def checksummed(code)
    code.to_s + checksum(code.to_s).to_s
  end
end
