module CountriesAggregatable
  extend ActiveSupport::Concern

  def countries_from_sources
    [
      self_identified_country,
      logged_in_ip_countries,
      two_factor_auth_country,
      credit_card_countries
    ].flatten.compact.uniq
  end

  def countries_from_non_self_sources
    return locked_country if address_locked?

    [
      logged_in_ip_countries,
      two_factor_auth_country,
      credit_card_countries
    ].flatten.compact.uniq
  end

  def has_no_countries?
    countries_from_sources.empty?
  end

  def has_multiple_countries?
    countries_from_sources.map do |country|
      country.corporate_entity_id if country.present?
    end.flatten.compact.uniq.length > 1
  end

  def has_invalid_country?
    self_identified_country.blank?
  end

  def has_self_in_atleast_one_source?
    countries_from_non_self_sources.any? do |country|
      self_identified_country.include?(country)
    end
  end

  def self_identified_country
    return if self[:country].blank?

    Country.find_by_name_or_iso_code(self[:country])
  end

  def logged_in_ip_countries
    person_login_countries = login_events
                             .order(created_at: :desc)
                             .where.not(country: nil)
                             .limit(5)
                             .compact
                             .map(&:country)
                             .uniq
    return [] if person_login_countries.blank?

    person_login_countries.map! { |country| country == "China" ? "Mainland China" : country }
    Country.find_by_name_or_iso_code(person_login_countries)
  end

  def two_factor_auth_country
    return if two_factor_auth.blank?

    tfa_country_phone_code = two_factor_auth&.country_code
    Country.find_by_country_phone_code(tfa_country_phone_code)
  end

  def credit_card_countries
    stored_cc_countries = stored_credit_cards.pluck(:country).uniq
    return [] if stored_cc_countries.blank?

    Country.find_by_name_or_iso_code(stored_cc_countries)
  end

  def kyc_country
    Country.find_by_name_or_iso_code(country)
  end

  def locked_country
    AddressLockedCountry::Finder.call(self)
  end
end
