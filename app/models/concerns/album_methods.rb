module AlbumMethods
  extend ActiveSupport::Concern
  include Tunecore::Creativeable

  included do
    has_one :artwork
    has_many :songs, -> { order "songs.track_num asc" }, inverse_of: :album
    has_many :spatial_audios, through: :songs
  end

  def dolby_atmos_audios?
    songs.joins(:spatial_audio).exists?
  end
end
