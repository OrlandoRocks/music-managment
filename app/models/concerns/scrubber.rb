module Scrubber
  extend ActiveSupport::Concern

  REPLACE_SPECIAL_CHARACTERS = {
    "/" => "",
    "`" => "",
    '\\' => "",
    "," => "",
    "." => "",
    "_" => "",
    "@" => "",
    "{" => "",
    "}" => "",
    "?" => "",
    "[" => "",
    "]" => "",
    "|" => "",
    "+" => "",
    "-" => "",
    "=" => "",
    "<" => "",
    ">" => "",
    "(" => "",
    ")" => "",
    ":" => "",
    ";" => "",
    "^" => "",
    "!" => "",
    "~" => "",
    "#" => "",
    "$" => "",
    "%" => "",
    "*" => "",
    "&" => "and",
    "\'" => "",
    '"' => "",
  }

  def scrub_artist_name(artist_name)
    artist_name
      .gsub(
        Regexp.new(
          Artist::REPLACE_SPECIAL_CHARACTERS.keys.map { |char| Regexp.escape(char) }.join("|")
        ),
        Artist::REPLACE_SPECIAL_CHARACTERS
      ).strip.downcase
  end

  private

  def update_scrubbed_name
    self.scrubbed_name = scrub_artist_name(name) if name_changed?
  end
end
