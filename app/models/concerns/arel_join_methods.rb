module ArelJoinMethods
  def left_outer_join(from_table, to_table, column1, column2)
    from_table.join(
      to_table, Arel::Nodes::OuterJoin
    ).on(
      column1.eq(column2)
    ).join_sources
  end
end
