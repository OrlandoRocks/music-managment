module ArelNamedMysqlFunctions
  def coalesce(*args)
    Arel::Nodes::NamedFunction.new("COALESCE", [args])
  end

  def distinct(*args)
    Arel::Nodes::NamedFunction.new("DISTINCT", args)
  end

  def group_concat(*args)
    Arel::Nodes::NamedFunction.new("GROUP_CONCAT", args)
  end

  def group_concat_distinct(*args)
    group_concat([distinct(args)])
  end

  def if_null(col, default)
    Arel::Nodes::NamedFunction.new("IFNULL", [col, default])
  end

  def null_if(col, default)
    Arel::Nodes::NamedFunction.new("NULLIF", [col, Arel::Nodes.build_quoted(default)])
  end

  def select_as_string(int_attr)
    Arel::Nodes::NamedFunction.new("CAST", [int_attr.as("CHAR")])
  end

  def select_count(*args)
    Arel::Nodes::NamedFunction.new("COUNT", [args])
  end

  def select_sum(*args)
    Arel::Nodes::NamedFunction.new("SUM", [args])
  end

  def select_year(*args)
    Arel::Nodes::NamedFunction.new("YEAR", [args])
  end
end
