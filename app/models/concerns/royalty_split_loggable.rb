# frozen_string_literal: true

module RoyaltySplitLoggable
  extend ActiveSupport::Concern

  included do
    after_commit :was_created_royalty_split_loggable_events, on: :create
    after_commit :was_changed_royalty_split_loggable_events, on: :update
    after_commit :was_destroyed_royalty_split_loggable_events, on: :destroy
  end

  def was_created_royalty_split_loggable_events
    case class_name
    when "RoyaltySplit"
      RoyaltySplits::EventService.split_created self
    when "RoyaltySplitRecipient"
      RoyaltySplits::EventService.recipient_created self
    when "RoyaltySplitSong"
      RoyaltySplits::EventService.song_created self
    end
  end

  def was_destroyed_royalty_split_loggable_events
    case class_name
    when "RoyaltySplit"
      RoyaltySplits::EventService.split_destroyed self
    when "RoyaltySplitRecipient"
      RoyaltySplits::EventService.recipient_destroyed self
    when "RoyaltySplitSong"
      RoyaltySplits::EventService.song_destroyed self
    end
  end

  def was_changed_royalty_split_loggable_events
  end
end
