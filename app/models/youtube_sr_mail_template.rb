class YoutubeSrMailTemplate < ApplicationRecord
  serialize :custom_fields
  belongs_to :country_website

  DEFAULT_HEADER_FIELDS = [:count, :isrc, :person_name, :person_email]
  EVALUATABLE_ATTRIBUTES = Set.new([:header, :subject])

  validates :template_name, uniqueness: { scope: :country_website_id, message: "should be unique to the country" }

  def self.types_for_select
    where("body != '' OR body IS NOT NULL AND custom_fields IS NOT NULL")
      .group(:template_name).order(:id).map { |pairs| [pairs.select_label, pairs.template_name.to_sym] }
  end

  def self.return_template(template_name)
    where(template_name: template_name, country_website_id: CountryWebsite::UNITED_STATES).first
  end

  def self.template_for_country_website_id(template_name, country_website_id)
    find_by(template_name: template_name, country_website_id: country_website_id) ||
      find_by(template_name: template_name, country_website_id: CountryWebsite::UNITED_STATES)
  end

  def evaluate_attribute(attribute, data)
    return "" unless EVALUATABLE_ATTRIBUTES.include?(attribute)

    song = data.first[:song]
    data_count = data.count
    isrc_text = (data_count <= 1) ? song.isrc.to_s : "Multiple ISRCs"
    params = {
      count: data_count.to_s,
      isrc: isrc_text,
      person_name: song.person_name.to_s,
      person_email: song.person_email.to_s
    }

    Mustache.render(send(attribute), params)
  end
end
