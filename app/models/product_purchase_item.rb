# = Description
# This class is just to make a product purchaseable.  It does nothing other than enable that.
#
# = Change Log
# [2009-12-30 -- CH]
# Created Model

class ProductPurchaseItem < ApplicationRecord
  belongs_to :purchase
  belongs_to :purchaseable, polymorphic: true
  validates :purchaseable_id, uniqueness: { scope: [:purchase_id, :purchaseable_type] }

  def purchase_item_related_exists?
    purchaseable_type.constantize.find_by(id: purchaseable_id) ? true : false
  end
end
