# Takes a distribution transition object and turns the message in to something meaningful that Customer Service can act on
class DistributionErrorDecoder
  # Decodes the error message from a transition
  def self.decode_transition(transition)
    case transition.message
    when /Command/
      "Encoding error - Replace existing audio with an acceptable format"
    when /undefined method `key' for nil:NilClass/
      "Missing assets error - Missing audio or artwork assets"
    when /undefined method `name' for nil:NilClass/
      "Missing assets error - Missing audio or artwork assets"
    else
      "#{transition.message[0..100]} - Escalate with tech department"
    end
  end

  # Finds the last errored distribution for album associated with the bundle and decodes the message
  def self.decode_bundle(petri_bundle)
    # find the the last bundle that's in an errored state for this album
    bundles = PetriBundle.where("state = 'error' and album_id = ?", petri_bundle.album_id)
    bundle = bundles.last
    # find the last errored distribution
    distributions = bundle.distributions.select { |d| d.state == "error" }
    distribution = distributions.select { |d| d.state == "error" }

    # bundle doesn't have any distributions!
    return "No distributions for the last bundle.  Create a new bundle." if distributions.size < 1

    # whats the error mesg?
    decode_transition(distribution.last.transitions.last)
  rescue => e
    "Couldn't decode distribution #{distribution.id}: #{e.message}"
  end

  # finds the last error for a distribution
  def self.decode(distribution)
    decode_transition(distribution.last.transitions.last)
  end
end
