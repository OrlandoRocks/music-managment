# frozen_string_literal: true

class LatestInEachAdyenPaymentMethodView < ApplicationRecord
  self.table_name = "latest_in_each_adyen_payment_methods"

  belongs_to :person
  belongs_to :adyen_payment_method_info

  delegate :payment_method_name,
           :payment_method_display_name,
           to: :adyen_payment_method_info, allow_nil: true

  def readonly?
    true
  end
end
