class PetriBundle < ApplicationRecord
  class BundleNotReady < StandardError; end

  class DuplicateSalepointError < StandardError; end
  include Tunecore::StateMachine
  include Tunecore::LockableLock

  belongs_to :album
  has_many :distributions, -> { where.not(distributions: { state: %w[dismissed blocked], converter_class: "MusicStores::YoutubeSr::Converter" }) }
  has_many :transitions, as: :state_machine, dependent: :destroy
  has_many :historical_distributions, class_name: "Distribution", dependent: :destroy

  acts_as_state_machine initial: :new

  state :new,         exit: :obtain_lock
  state :processing
  state :error
  state :complete,    after: :release_lock
  state :dismissed,   after: :release_lock

  event(:processing)  { transitions from: [:new, :processing, :complete, :error], to: :processing }
  event(:error)       { transitions from: [:new, :processing, :error],            to: :error      }
  event(:complete)    { transitions from: [:processing, :error],                  to: :complete   }
  event(:dismissed)   { transitions from: [:error, :processing, :new, :complete], to: :dismissed  }

  scope :active, -> { where("state <> 'dismissed'").order(id: :desc) }

  def self.recreate!(album, params = {})
    new_bundle = nil
    transaction do
      old_bundles = where(album_id: album.id).active.lock
      distribution_song_hash = Hash.new([])
      old_bundles.each do |old_bundle|
        old_bundle.distributions.each do |distro|
          if distro.distribution_songs.present?
            distribution_song_hash[distro.converter_class] += distro.distribution_songs
          end
        end
        old_bundle.dismiss(params)
      end
      new_bundle = album.petri_bundles.create!
      new_bundle.do_bundle!
      new_bundle.reassociate_distribution_songs(distribution_song_hash) if distribution_song_hash.present?
    end

    new_bundle.reload
  end

  def dismiss(params)
    audit(params) do
      dismissed!
    end
  end

  def dismissed?
    state == "dismissed"
  end

  def complete?
    state == "complete"
  end

  def add_salepoints(salepoints, params = {})
    salepoints = Array(salepoints)
    dist = []
    salepoints.each do |salepoint|
      next if salepoint.ytsr_proxy_salepoint?

      params = {
        converter_class: salepoint.store.converter_class,
        delivery_type: params[:delivery_type]
      }

      state = salepoint.distributions.latest.try(:state)
      params[:state] = state if preserve_state?(state)

      created = DistributionCreator.create(album, salepoint.store.short_name, params)
      dist << created.distribution
    end

    dist
  rescue => e
    audit(
      actor: params[:actor],
      message: "Failed to create distribution for album #{album.id}: #{e.message}",
      backtrace: e.backtrace
    ) { error! }
  end

  def preserve_state?(distribution_state)
    ["dismissed", "blocked", "pending_approval"].include?(distribution_state)
  end

  def next_state
    complete = true
    error = false
    converters = ["MusicStores::Streaming::Converter", "MusicStores::Youtube::Converter"]

    distributions.each do |distro|
      next if converters.include?(distro.converter_class)

      complete  = false unless distro.delivered?
      error     = true if distro.error?
    end

    if error
      "error"
    elsif complete
      "complete"
    else
      "processing"
    end
  end

  def distribution_updated(params)
    distributions.reload
    audit(params) do
      send("#{next_state}!")
    end
  end

  def self.for(album)
    album.petri_bundle
  end

  def obtain_lock
    return if dismissed?

    if album.locked_by.nil?
      album.obtain_lock(self)
    elsif album.locked_by_id != id
      unless album.release_lock
        destroy
        raise RuntimeError, "Bundling process unable to release lock on album ##{album.id} held by entity #{album.locked_by.class.name}.  Deleting bundle!"
      end
      album.obtain_lock(self)
    end
  end

  delegate :release_lock, to: :album

  def okay_to_release?(_lockable)
    ["complete", "dismissed"].include?(state)
  end

  def do_bundle!
    message =
      if album.locked? && !album.locked_by.okay_to_release?(album)
        "The album is locked by other petri bundle or process"
      elsif !album.payment_applied?
        "The album has not been paid for"
      elsif !album.has_required_elements?
        "The album does not have the required elements"
      end

    transaction do
      bundle_salepoints = album.salepoints.reject { |sp| !sp.payment_applied? || !sp.store.in_use_flag }
      if bundle_salepoints.empty?
        message = "There are no salepoints to add"
      else
        add_salepoints(bundle_salepoints)
      end
    end

    raise BundleNotReady, message if message
  end

  def reassociate_distribution_songs(distribution_song_hash)
    distribution_song_hash.each do |converter_class, distribution_songs|
      if (distribution = distributions.detect { |d| d.converter_class == converter_class }).present?
        distribution_songs.each { |ds| ds.update(distribution_id: distribution.id) }
      else
        audit(actor: params[:actor], message: "Could not find #{converter_class} distribution for album<#{album.id}> with distribution_songs") { error! }
      end
    end
  end
end
