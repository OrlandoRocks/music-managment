class PlanFeature < ApplicationRecord
  has_many :plans_plan_features, dependent: :destroy
  has_many :plans, through: :plans_plan_features

  validates :feature, presence: true, uniqueness: true

  ADMIN_ACCESSIBLE_FEATURES = [:trend_reports, :premium_sales_report]
end
