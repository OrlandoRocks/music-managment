class Tier < ApplicationRecord
  # Maps its rows by name to the corresponding class
  ELIGIBILITY_HANDLERS = {
    new_artist: RewardSystem::Tiers::NewArtist,
    rising_artist: RewardSystem::Tiers::RisingArtist,
    breakout_artist: RewardSystem::Tiers::BreakoutArtist,
    professional: RewardSystem::Tiers::ProfessionalArtist,
    artist_accelerator: RewardSystem::Tiers::ArtistAccelerator,
    legend: RewardSystem::Tiers::Legend,
  }

  belongs_to :parent, class_name: "Tier", optional: true

  has_many :tier_people, dependent: :destroy
  has_many :people, through: :tier_people
  has_many :tier_rewards, dependent: :destroy
  has_many :rewards, through: :tier_rewards
  has_many :tier_thresholds, dependent: :destroy
  has_many :tier_certifications, dependent: :destroy
  has_many :certifications, through: :tier_certifications
  has_many :tier_achievements, dependent: :destroy
  has_many :achievements, through: :tier_achievements

  validates :name, :hierarchy, :slug, presence: true, uniqueness: true
  validates :is_active, inclusion: { in: [true, false] }
  validates :parent, presence: true, if: -> { is_sub_tier? }
  validate :validate_parent

  before_validation :nullify_parent_id
  before_validation :assign_hierarchy, on: :create
  before_validation :generate_slug, on: :create

  scope :active, -> { where(is_active: true) }
  scope :parents_only, -> { where(is_sub_tier: false) }

  def tier_key
    name.parameterize.underscore
  end

  def handler_service
    fallback_class = RewardSystem::Tiers::Fallback

    handler_class = ELIGIBILITY_HANDLERS[tier_key.to_sym]

    if handler_class.blank?
      Airbrake.notify("Handler class not implemented for Tier", { tier: self })
      fallback_class.new(self)
    else
      handler_class.new(self)
    end
  end

  def nullify_parent_id
    self.parent_id = nil unless is_sub_tier?
  end

  def assign_hierarchy
    self.hierarchy = next_in_hierarchy if hierarchy.blank?
  end

  def generate_slug
    self.slug = name.to_s.strip.parameterize.underscore
  end

  private

  def next_in_hierarchy
    tiers = Tier.all

    tiers =
      if is_sub_tier?
        tiers.where("parent_id = :parent_id OR id = :parent_id", parent_id: parent_id)
      else
        tiers.where("parent_id IS NULL OR parent_id = 0")
      end

    last_tier = tiers.order(hierarchy: :asc).last
    last_used_hierarchy = last_tier&.hierarchy.to_f

    next_in_order =
      if is_sub_tier?
        last_used_hierarchy + 0.1
      else
        last_used_hierarchy + 1
      end

    next_in_order.round(2)
  end

  def validate_parent
    return unless is_sub_tier?

    parent_tier = Tier.find_by(id: parent_id)

    if parent_tier.blank?
      errors.add(:parent, "Invalid parent tier id. Parent not assigned.")
      errors.add(:hierarchy, "Invalid parent tier id. Hierarchy not assigned.")
      return
    end

    errors.add(:parent, "cannot be a sub tier") if parent_tier.is_sub_tier?
  end
end
