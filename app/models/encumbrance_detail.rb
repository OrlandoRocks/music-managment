class EncumbranceDetail < ApplicationRecord
  belongs_to :encumbrance_summary

  validates :encumbrance_summary_id, :transaction_date, :transaction_amount, presence: true
  validates :transaction_amount, numericality: true
end
