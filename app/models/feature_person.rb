class FeaturePerson < ApplicationRecord
  self.table_name = "features_people"

  belongs_to :person
  belongs_to :feature
end
