# frozen_string_literal: true

class KnowledgebaseLink < ApplicationRecord
  validates :article_slug, presence: true

  PLAN_LEARN_MORE_ARTICLE_ID = "4418252129044"
  ACCELERATOR_FAQ_ARTICLE_ID = "13259365372820"
  DOLBY_ATMOS_ARTICLE_ID = "10511913953812"
  COVER_SONG_ARTICLE_ID = "115006695208"
end
