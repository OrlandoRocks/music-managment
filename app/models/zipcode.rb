# == Schema Information
# Schema version: 404
#
# Table name: zipcodes
#

class Zipcode < TrendsBase
  self.table_name = "zipcodes"

  validates :state,
            length: {
              maximum: 2,
              message: "Please use the state's two letter code"
            }

  def city_and_state
    city_mixed_case + ", " + state
  end
end
