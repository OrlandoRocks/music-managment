require "aws-sdk"

class CmsSet < ApplicationRecord
  include Jsonable

  # For ever new callout_type there needs to be 'None' set.
  CALLOUT_TYPE = ["login", "interstitial", "dashboard"]

  validates :callout_type, :callout_name, presence: true
  validates :callout_name, uniqueness: { scope: [:callout_type, :country_website_id, :country_website_language_id] }
  validate :timestamp_is_not_in_the_past, on: :create

  has_many :cms_set_attributes, dependent: :destroy
  belongs_to :country_website
  belongs_to :country_website_language

  def self.active_set(callout_type, country, country_website_language_id)
    CmsSetFinder.active_set(callout_type, country, country_website_language_id)
  end

  def create_attributes(attributes)
    attributes.reject! { |_name, value| value.blank? }
    attributes.each do |name, value|
      if CmsSetAttribute::UPLOADED_IMAGE_TYPES.include?(name)
        cms_set_attributes.new(attr_name: name, cms_set: self, name.to_sym => value)
      elsif CmsSetAttribute::TEXT_FIELD_ATTRIBUTE_NAMES.include?(name)
        cms_set_attributes.new(attr_name: name, attr_text: value, cms_set: self)
      else
        cms_set_attributes.new(attr_name: name, attr_value: value, cms_set: self)
      end
    end
  end

  def update_cms_set_attributes(attributes)
    new_attributes = {}
    existing_attributes = {}

    cms_set_attributes.each do |attr|
      existing_attributes[attr.attr_name] = attr.id
    end

    attributes.each do |name, value|
      if existing_attributes.include?(name)
        if value.blank?
          CmsSetAttribute.find(existing_attributes[name]).destroy
        elsif CmsSetAttribute::UPLOADED_IMAGE_TYPES.include?(name)
          CmsSetAttribute.find(existing_attributes[name]).update!(name.to_sym => value)
        elsif CmsSetAttribute::TEXT_FIELD_ATTRIBUTE_NAMES.include?(name)
          CmsSetAttribute.find(existing_attributes[name]).update(attr_text: value)
        else
          CmsSetAttribute.find(existing_attributes[name]).update(attr_value: value)
        end
      else
        new_attributes[name] = value
      end
    end

    create_attributes(new_attributes) unless new_attributes.empty?
  end

  def activate
    update(start_tmsp: Time.now)
  end

  def deactivate
    update(callout_name: "None", start_tmsp: Time.now)
  end

  def as_json(options = {})
    new_attributes = {}

    cms_set_attributes.each do |cms_set_attr|
      new_attributes[cms_set_attr.attr_name] =
        if CmsSetAttribute::UPLOADED_IMAGE_TYPES.include?(cms_set_attr.attr_name)
          cms_set_attr.send(cms_set_attr.attr_name)
        elsif CmsSetAttribute::TEXT_FIELD_ATTRIBUTE_NAMES.include?(cms_set_attr.attr_name)
          cms_set_attr.attr_text
        else
          cms_set_attr.attr_value
        end
    end

    merge_attributes_as_json(new_attributes, options)
  end

  private

  def timestamp_is_not_in_the_past
    errors.add(:start_tmsp, "cannot be in the past") if !start_tmsp.nil? && (start_tmsp < Time.now)
  end
end
