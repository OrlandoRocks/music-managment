# frozen_string_literal: true

class InvoiceStaticCustomerAddress < ApplicationRecord
  FIELDS = Set[
    "address1",
    "address2",
    "city",
    "state",
    "zip",
    "country"
  ].freeze

  belongs_to :related, polymorphic: true

  validates :related_id, :related_type, presence: true

  def luxembourg_customer?
    country == "Luxembourg"
  end

  def eu_country?
    country_obj = Country.find_by(name: country)
    return false if country_obj.nil?

    Country::EU_COUNTRIES.include?(country_obj.iso_code)
  end
end
