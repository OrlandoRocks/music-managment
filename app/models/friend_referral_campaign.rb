class FriendReferralCampaign < ApplicationRecord
  belongs_to :person

  validates :person_id, uniqueness: { scope: :campaign_id }

  class << self
    attr_accessor :current_campaign_id
  end

  @current_campaign_id = GA_DEFAULT_CAMPAIGN_ID

  # # get overall stats for the person
  def self.get_stats(person)
    Tunecore::FriendReferral::Kickback.get_ambassador_stats(person)
  end

  def self.signup(person)
    kickback = Tunecore::FriendReferral::Kickback.signup(person)
    if kickback.errors.empty?
      campaign_url = kickback.campaign_url(current_campaign_id)
      campaign_code = kickback.campaign_code(current_campaign_id)
      campaign = create(
        person: person,
        campaign_id: current_campaign_id,
        campaign_url: campaign_url,
        campaign_code: campaign_code
      )
    else
      campaign = new
      campaign.errors.add(:base, kickback.errors)
    end

    campaign
  end

  def self.get_current_campaign_details(person)
    campaign = find_by(person_id: person.id, campaign_id: current_campaign_id)
    campaign ||= signup(person)

    campaign
  end

  # get campaign stats
  def get_stats
    stats = Tunecore::FriendReferral::Kickback.get_ambassador_stats(person)
    stats.campaign_stats(self.class.current_campaign_id)
  end
end
