class CreativeSongRole < ApplicationRecord
  belongs_to :song_role
  belongs_to :creative
  belongs_to :song
  has_one :artist, through: :creative
end
