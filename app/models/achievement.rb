class Achievement < ApplicationRecord
  has_many :tier_achievements, dependent: :destroy
  has_many :tiers, through: :tier_achievements

  validates :name, :points, :category, presence: true
  validates :is_active, inclusion: { in: [true, false] }
end
