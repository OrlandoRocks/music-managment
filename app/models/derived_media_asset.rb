# 2009-08-19 -- ED -- this model will be handling converted media assets such as band photos
# =>               -- resized to 160x160 thumbnail

class DerivedMediaAsset < ApplicationRecord
  belongs_to :media_asset
  belongs_to :s3_asset

  validates :media_asset, :size, :mime_type, presence: true
  validates :s3_asset, presence: true
  validates   :width, numericality: { only_integer: true, less_than: 65_535, allow_nil: true }
  validates   :height, numericality: { only_integer: true, less_than: 65_535, allow_nil: true }
  validates   :bit_depth, numericality: { only_integer: true, less_than: 255, allow_nil: true }
  validates   :size, numericality: { only_integer: true, less_than: 4_294_967_295, allow_nil: false }

  # validate mime_type to fall in the following categories
  # image/jpeg, image/png, image/tiff, image/gif and image/bmp
  validates :mime_type,
            inclusion: {
              in: %w[image/jpeg image/png image/tiff image/gif image/bmp],
              message: "extension {{value}} is not included in the list"
            }

  before_update  :remove_assets
  before_destroy :destroy_assets

  private

  def remove_assets
    # we're only working with s3 asset for band photos
    # we should also remove local asset in the future
    begin
      s3_asset.delete_key! if s3_asset
    rescue => e
      logger.error("removing derived media assets from local and/or s3: ")
    end
  end

  def destroy_assets
    begin
      s3_asset.destroy! if s3_asset # we're only working with s3 asset for band photos
    rescue => e
      logger.error("destroying s3 asset in DerivedMediaAsset model ")
    end
  end
end
