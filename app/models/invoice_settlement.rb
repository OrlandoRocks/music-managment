# == Schema Information
# Schema version: 404
#
# Table name: invoice_settlements
#
#  id                      :integer(11)     not null, primary key
#  invoice_id              :integer(11)     default(0), not null
#  created_at              :datetime        not null
#  settlement_amount_cents :integer(11)     default(0), not null
#  source_id               :integer(11)     default(0), not null
#  source_type             :string(50)      default(""), not null
#

class InvoiceSettlement < ApplicationRecord
  extend MoneyField

  money_reader :settlement_amount

  belongs_to :invoice

  # note - going to make :source be something a bit more explicit
  # and *perhaps* make it so that you can 'comp' a value for the invoice
  # which would mean an admin could partially or completely settle the
  # invoice, while preserving the flow of cash...
  belongs_to :source, polymorphic: true
  has_one :vat_tax_adjustment, as: :related
  has_one :outbound_invoice, as: :related

  has_many :refunds, through: :invoice
  has_many :refund_settlements, through: :refunds, source: :refund_settlements
  has_many :disputes, ->(settlement) {
    where(source: settlement.source)
  }, through: :invoice

  before_validation :set_currency
  before_destroy :never_destroy

  validates :currency, presence: true
  # ensure the same source can't be used more than once
  validates :source_id, uniqueness: { scope: :source_type }

  def settled_by_payments_os?
    source.instance_of?(PaymentsOSTransaction)
  end

  def settled_by_balance?
    source.instance_of?(PersonTransaction)
  end

  def never_destroy
    raise RuntimeError, "You cannot destroy settlements or invoices with settlements"
  end

  def historic_refund_settlements
    refund_settlements.where(source_type: source_type)
  end

  def refunded_amount
    historic_refund_settlements.sum(&:settlement_amount_cents) + refunded_disputes.sum(:amount_cents)
  end

  def max_refund_amount
    settlement_amount_cents - refunded_amount
  end

  def can_refund?
    max_refund_amount.positive?
  end

  private

  def set_currency
    self.currency = invoice.person.currency
  end

  def refunded_disputes
    disputes.refunded
  end
end
