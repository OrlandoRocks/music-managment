class YtmBlockedSong < ActiveRecord::Base
  belongs_to  :song
  validates   :song, presence: true
  validates   :song_id, uniqueness: true

  after_create :takedown_salepoint_song, if: :sent_for_monetization?
  after_create :reject_salepoint_song, if: :salepoint_song
  after_destroy :remove_salepoint_song_takedown, if: :sent_for_monetization?

  def salepoint_song
    @salepoint_song ||= SalepointSong.joins(:salepoint)
                                     .find_by(song_id: song&.id, salepoints: { store_id: Store::YTSR_STORE_ID })
  end

  private

  def sent_for_monetization?
    song&.distribution_songs&.present?
  end

  def takedown_salepoint_song
    SalepointSong.takedown(song)
  end

  def reject_salepoint_song
    SalepointSong.update_states(rejected_salepoint_song_hash)
  end

  def remove_salepoint_song_takedown
    SalepointSong.remove_takedown(song)
  end

  def rejected_salepoint_song_hash
    {
      salepoint_song.id.to_s => {
        "state" => "rejected", "reason_for_rejection" => "Mass blocked"
      }
    }
  end
end
