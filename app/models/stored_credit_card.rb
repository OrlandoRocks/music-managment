#
#  stored_credit_card.rb
#
#  The StoredCreditCard model keeps track of an individual's different stored credit cards. As of 8/14/09, we're
#  only storing customer account data in the Vault at Braintree (www.braintreepaymentsolutions.com).
#  This model is responsible for retrieving the meta data and storing it for the customer's review.
#
#  2009-08-14 CH -- created model
#  2009-08-21 CH -- renamed model from BankAccount to StoredCreditCard
#  2010-01-05 DCD -- added has_many batch_transactions relationship

class StoredCreditCard < ApplicationRecord
  include ActiveMerchant::Billing
  scope :active, -> { where(deleted_at: nil) }
  scope :not_expired, -> { where("expiration_year > :year OR ( expiration_month > :month AND expiration_year = :year)", year: Date.today.year, month: Date.today.month) }

  scope :expired, -> { where("expiration_year < :year OR ( expiration_month < :month AND expiration_year = :year)", year: Date.today.year, month: Date.today.month) }

  scope :braintree_cards, -> { where.not(bt_token: nil) }
  scope :payments_os_cards, -> { where.not(payments_os_token: nil) }

  belongs_to :person
  belongs_to :country_state, foreign_key: :state_id, primary_key: :id, optional: true
  belongs_to :country_state_city, foreign_key: :state_city_id, primary_key: :id, optional: true
  belongs_to :payin_provider_config

  has_many  :braintree_vault_transactions, as: :related
  has_many  :batch_transactions
  has_many  :braintree_transactions

  delegate :person_preference, to: :person, allow_nil: true

  validates :customer_vault_id, :person_id, presence: true
  validates :country_state, :country_state_city, presence: { on: :create, if: :is_payments_os_card? }

  attr_accessor :meta_data

  def destroy(ip_address, person, options = {})
    options[:skip_validation] ||= false
    transaction do
      # Mark it as deleted
      self.deleted_at = Time.now
      save!

      # Let person_preference fix itself and validate itself, throws an exception if invalid
      if person_preference.try(:persisted?)
        person_preference.delete_stored_credit_card_proc!(self, ip_address, person, options)
      end

      begin
        result = config_gateway_service.delete_payment_method(bt_token)
      rescue
        failed = "error"
      end

      BraintreeVaultTransaction.create(
        person: person,
        response_code: result ? "success" : failed,
        ip_address: ip_address,
        related: self,
        raw_response: result
      )

      save!

      true
    end
  rescue ActiveRecord::RecordInvalid => e
    false
  end

  def expired?
    expires_by_date?(Date.today)
  end

  def expires_before_date?(before_date)
    expires_by_date?(before_date)
  end

  def deleted?
    deleted_at != nil
  end

  def is_payments_os_card?
    payments_os_token.present?
  end

  def is_braintree_card?
    bt_token.present?
  end

  def padded_cc_number
    if cc_type != "American Express" && cc_type != "Diner's Club"
      "************#{last_four}"
    else
      (cc_type == "American Express") ? "**********#{last_four}" : "***********#{last_four}"
    end
  end

  def self.create_with_payments_os(person, options = {})
    if FeatureFlipper.show_feature?(:enable_gst, person)
      gst = GstInfo.find_or_create_by(person_id: person.id, gstin: options[:gstin]) if options[:gstin].present?
      return [:gst_error, gst] if gst.present? && gst.errors.present?
    end

    payos_customer, result = PaymentsOS::CustomerService.new(person).find_or_create_customer
    return [:payos_customer_error, result] if payos_customer.blank?

    status, response = PaymentsOS::CardService.new(person.reload).store_payment_method(options)

    person.set_payment_preference(response, person.pay_with_balance?) if status == :ok

    [status, response]
  end

  def self.create_with_braintree(person, options = {})
    if person.braintree_customer_id.blank? || !person.customer_in_config_gateway?
      customer_result = person.config_gateway_service.create_customer(
        first_name: person.name.split[0],
        last_name: person.name.split[-1]
      )
      person.update(braintree_customer_id: customer_result.customer.id)
    end

    result = person.config_gateway_service.create_payment_method(
      customer_id: person.braintree_customer_id,
      payment_method_nonce: braintree_payment_nonce(options),
      billing_address:
        {
          street_address: options[:address1],
          extended_address: options[:address2],
          locality: options[:city],
          region: options[:state],
          postal_code: options[:zip],
          country_code_alpha2: options[:country],
          first_name: options[:firstname],
          last_name: options[:lastname]
        },
      options: {
        verify_card: true,
        verification_merchant_account_id: person.braintree_config_by_corporate_entity.merchant_account_id
      }
    )
    StoredCreditCard.handle_braintree_result(result, person, nil, options)
  end

  def update_social_card_with_braintree(options = {})
    result = config_gateway_service.update_payment_method(
      bt_token,
      cardholder_name: "#{options[:firstname]} #{options[:lastname]}",
      expiration_date: options[:ccexp],
      cvv: options[:cvv],
      billing_address: {
        street_address: options[:address1],
        extended_address: options[:address2],
        locality: options[:city],
        region: options[:state],
        postal_code: options[:zip],
        country_code_alpha2: options[:country],
        first_name: options[:firstname],
        last_name: options[:lastname],
        options: {
          update_existing: true
        }
      }
    )
    StoredCreditCard.handle_braintree_result(result, person, self, options)
  end

  def update_with_braintree(options = {})
    result = config_gateway_service.update_payment_method(
      bt_token,
      payment_method_nonce: StoredCreditCard.braintree_payment_nonce(options),
      cardholder_name: "#{options[:firstname]} #{options[:lastname]}",
      expiration_date: options[:ccexp],
      billing_address: {
        street_address: options[:address1],
        extended_address: options[:address2],
        locality: options[:city],
        region: options[:state],
        postal_code: options[:zip],
        country_code_alpha2: options[:country],
        first_name: options[:firstname],
        last_name: options[:lastname],
        options: {
          update_existing: true
        }
      }
    )
    StoredCreditCard.handle_braintree_result(result, person, self, options)
  end

  def days_until_credit_card_expires
    return if expired?

    end_date = DateTime.new(expiration_year, expiration_month, 1).end_of_month
    (end_date - DateTime.current).to_f.floor
  end

  def self.handle_braintree_result(result, person, stored_credit_card, options = {})
    if result.success?
      payment_method = result.payment_method
      credit_card = create_or_update_card(payment_method, person, stored_credit_card, options)
      btvt = BraintreeVaultTransaction.create(person: person, related: credit_card, response_code: "success", ip_address: options[:ip_address], raw_response: payment_method.to_yaml)
      if options[:merchant_defined_field_5].to_i == 1
        person.set_payment_preference(credit_card, options[:merchant_defined_field_10].to_i == 1)
      end
      note = "User #{stored_credit_card.present? ? 'updated' : 'added'} a credit card, ************#{credit_card.last_four}. (ID: #{credit_card.id}) Preferred payment method was set to this credit card"
      Note.create(related: person, note_created_by: person, ip_address: options[:ip_address], subject: "Payment Preferences Changed", note: note)
      [:ok, btvt]
    else
      btvt = BraintreeVaultTransaction.create(person: person, response_code: "errored", ip_address: options[:ip_address], raw_response: result.to_yaml)
      [:braintree_error, btvt]
    end
  end

  def self.create_or_update_card(payment_method, person, stored_credit_card, options = {})
    if stored_credit_card
      stored_credit_card.update(
        expiration_month: payment_method.expiration_month,
        expiration_year: payment_method.expiration_year,
        bt_token: payment_method.token,
        cc_type: payment_method.card_type,
        last_four: payment_method.last_4,
        company: options[:company],
        first_name: options[:firstname],
        last_name: options[:lastname],
        address1: options[:address1],
        address2: options[:address2],
        city: options[:city],
        state: options[:state],
        country: options[:country],
        zip: options[:zip],
        cvv: true,
        payin_provider_config: options[:payin_provider_config]
      )
      stored_credit_card
    else
      create(
        person: person,
        customer_vault_id: 0,
        bt_token: payment_method.token,
        cc_type: payment_method.card_type,
        last_four: payment_method.last_4,
        expiration_month: payment_method.expiration_month,
        expiration_year: payment_method.expiration_year,
        status: "current",
        company: options[:company],
        first_name: options[:firstname],
        last_name: options[:lastname],
        address1: options[:address1],
        address2: options[:address2],
        city: options[:city],
        state: options[:state],
        country: options[:country],
        zip: options[:zip],
        cvv: true,
        payin_provider_config: options[:payin_provider_config]
      )
    end
  end

  def billing_info
    return self if address1.present?

    payment_method = config_gateway_service.find_payment_method(bt_token)
    info = payment_method.billing_address
    { stored_credit_card: attributes.merge(billing_params(info)) }
  rescue StandardError
    self
  end

  def mark_as_failed?
    most_recent_braintree_transaction = braintree_transactions.order(updated_at: :desc).first
    return false if most_recent_braintree_transaction.nil?

    most_recent_braintree_transaction.status == "declined" && updated_at <= most_recent_braintree_transaction.created_at
  end

  def tunecore_bt_payin_provider_config
    PayinProviderConfig.find_by(
      name: PayinProviderConfig::BRAINTREE_NAME,
      corporate_entity: CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME),
      currency: person.site_currency
    )
  end

  # this ternary is necessary until the backfill is run, once it is run we may remove this code
  # https://tunecore.atlassian.net/browse/ECOM-215
  def present_payin_provider_config
    return tunecore_bt_payin_provider_config if payin_provider_config.blank?

    payin_provider_config
  end

  def config_gateway_service
    return @config_gateway_service unless @config_gateway_service.nil?

    @config_gateway_service = Braintree::ConfigGatewayService.new(present_payin_provider_config)
  end

  def create_payment_method_nonce
    result = config_gateway_service.gateway.payment_method_nonce.create(bt_token)
    result.payment_method_nonce
  end

  def require_three_d_secure_transaction?
    return false unless person.three_d_secure_enabled?

    located_in_eea? && person.affiliated_to_bi?
  end

  def billing_address
    {
      street_address: address1,
      extended_address: address2,
      locality: city,
      region: state,
      postal_code: zip,
      country_code_alpha2: country,
      first_name: person.first_name,
      last_name: person.last_name,
    }
  end

  def self.braintree_payment_nonce(options)
    return options[:three_d_secure_nonce] if options[:three_d_secure_nonce].present?

    options[:braintree_payment_nonce]
  end

  # check if CC was issued in European Economic Area(EEA)
  def located_in_eea?
    Country::EEA_COUNTRIES.include?(country)
  end

  private

  def expires_by_date?(before_date)
    if !expiration_year.nil? && !expiration_month.nil?
      before_date.year > expiration_year || (before_date.month > expiration_month && before_date.year == expiration_year)
    else
      true
    end
  end

  def billing_params(info)
    {
      "first_name" => info.first_name,
      "last_name" => info.last_name,
      "address1" => info.street_address,
      "address2" => info.extended_address,
      "city" => info.locality,
      "state" => info.region,
      "zip" => info.postal_code,
      "country" => info.country_code_alpha2
    }
  end
end
