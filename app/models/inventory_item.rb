class InventoryItem
  attr_accessor :item, :inventory

  #
  #  Takes collection of InventoryItem objects
  #  and performs a distribution for each
  #
  def self.distribute!(items)
    items.each(&:distribute!)
  end

  #
  #  Takes:
  #  * person: Person
  #  * options:
  #      :item_to_use => SomeModel.new
  #      :product_to_use => Product.new
  #
  #
  #  returns: InventoryItem
  #
  def self.create(person, options)
    inventory = Inventory.use_single_item!(person, options)

    opts = {
      item: options[:item_to_use],
      inventory: inventory
    }

    InventoryItem.new(opts)
  end

  def initialize(options)
    @item = options[:item]
    @inventory = options[:inventory]
  end

  def distribute!
    distributor = InventoryItemDistributor.new(self)
    distributor.distribute!
  end
end
