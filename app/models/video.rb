class Video < ApplicationRecord
  include Tunecore::Lockable
  include Tunecore::Creativeable
  include Tunecore::Salepointable
  include Tunecore::Albums::Stores
  include Upcable

  self.inheritance_column = "video_type"

  attr_accessor :artwork  # reserved for future use
  attr_accessor :state # reserved for future use
  attr_accessor :length_min # for legacy video radio buttons

  delegate :steps_required, :should_finalize?, to: :finalizer

  # ================
  # = Associations =
  # ================
  belongs_to :person
  belongs_to :label
  belongs_to :album
  has_many :salepoints, as: :salepointable, dependent: :destroy
  has_many :stores, -> { select("stores.*").distinct }, through: :salepoints
  has_many :creatives, -> { order "creatives.id" }, as: :creativeable
  has_many :artists, -> { where("creatives.role = 'primary_artist'").order("creatives.id") }, through: :creatives
  has_one  :purchase, as: :related
  has_many :sales_records, as: :related

  # ===============
  # = Validations =
  # ===============
  validates :tunecore_isrc, uniqueness: true
  validates :optional_isrc, uniqueness: { if: :has_optional_isrc? }

  # =============
  # = Callbacks =
  # =============
  after_validation :copy_upc_errors_to_optional_upc
  after_create :make_tunecore_isrc, :add_default_salepoints

  # ============
  # =  Scopes  =
  # ============
  scope :music_videos,
        -> {
          includes(creatives: :artist).where(is_deleted: false, video_type: "MusicVideo")
        }

  scope :feature_films,
        -> {
          includes(creatives: :artist).where(is_deleted: false, video_type: "FeatureFilm")
        }

  def copy_upc_errors_to_optional_upc
    return unless errors.include?(:upcs)

    upcs.each do |single_upc|
      next if single_upc.valid?

      single_upc.errors.each do |_attr, msg|
        errors.add(:optional_upc, msg.to_s)
      end
    end
  end

  def sale_date=(date)
    begin
      super(date)
    rescue
      self.sale_date = Tunecore::StringDateParser.to_date(date)
    end
  end

  def orig_release_year=(date)
    begin
      super(date)
    rescue
      self.orig_release_year = Tunecore::StringDateParser.to_date(date)
    end
  end

  def sale_date_to_s
    Tunecore::StringDateParser.to_s(sale_date)
  end

  def orig_release_year_to_s
    Tunecore::StringDateParser.to_s(orig_release_year)
  end

  def make_tunecore_isrc
    isrc_year = Date.today.year.to_s[-2, 2]

    if id >= 100_000
      raise "gotcha sucka!  TC video id is now over 100,000 videos. You need to figure out new ISRC codes"
    end

    last_five_digits = sprintf("%05d", id)
    update_attribute(:tunecore_isrc, ISRC_VIDEO_PREFIX + isrc_year + last_five_digits)
  end

  # The 5 itunes stores are the only options for video distribution and they are required, this methods adds them
  # Added default variable price id of 33 ($1.29 track front) so Salepoint record validates and saves
  def add_default_salepoints
    salepoints << Salepoint.create(store_id: 1, variable_price_id: 33)
    salepoints << Salepoint.create(store_id: 2, variable_price_id: 33)
    salepoints << Salepoint.create(store_id: 3, variable_price_id: 33)
    salepoints << Salepoint.create(store_id: 4, variable_price_id: 33)
    salepoints << Salepoint.create(store_id: 5, variable_price_id: 33)
  end

  def name=(arg)
    super(allow_different_format? ? arg : Utilities::Titleizer.titleize(arg))
  end

  # Video number displayed to the end users.  Looks better than you're video number 4
  def code
    "XV" + sprintf("%06d", id)
  end

  def has_optional_isrc?
    optional_isrc.present?
  end

  # used in the video interface to allow users to view their info fields without being able to edit them
  def editable?
    !metadata_picked_up? && !locked?
  end

  def mark_as_deleted
    Purchase.destroy_by_purchase_item(person, self)
    update_attribute :is_deleted, true
  end

  def selected_artist_name
    @selected_artist_name ||= artist.nil? ? "" : artist.name
  end

  attr_writer :selected_artist_name

  def video_length_in_minutes
    if length.blank?
      0
    else
      length.to_i / 60
    end
  end

  def is_hd_feature?
    # returns true (HD), false (SD), or nil (not a feature)
    case definition
    when "HD"
      true
    when "SD"
      false
    else
      nil
    end
  end

  # method to display the time string in the user interface
  def duration
    "#{length_in_minutes}'  #{length_sec}\""
  end

  def label_name
    label.name if label
  end

  def label_name=(arg)
    self.label = arg.blank? ? nil : Label.find_or_create_by(name: arg)
  end

  def finalized?
    Inventory.used?(self)
  end

  def finalizer(force = false)
    @finalizer = nil if force
    @finalizer ||= VideoFinalizer.factory(self)
  end

  def fedex?
    false
    # we no longer have fedex label requirements, but this is
    # still here for some product pricing stuff
  end

  def has_sales?
    !sales_records.first.nil?
  end

  def self.inherited(child)
    child.instance_eval do
      def model_name
        Video.model_name
      end
    end
    super
  end

  private

  def presence_of_accepted_format
    return if (accepted_format == true)

    errors.add(:base, "You must confirm that you understand TuneCore's policy by #{t(:ele_check)}ing the box below")
  end
end
