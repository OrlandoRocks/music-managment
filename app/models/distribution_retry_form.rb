class DistributionRetryForm < FormObject
  attr_accessor(
    :retriable_distributions,
    :ip_address,
    :album
  )

  define_model_callbacks :save, only: [:after, :before]
  after_save :create_note

  validates :retriable_distributions, :ip_address, :album, presence: true

  def save
    Rails.logger.info("MASSRETRIES: album_id: #{album&.id} DistributionRetryForm.save before callbacks valid? = #{valid?}")
    Rails.logger.info("MASSRETRIES: album_id: #{album&.id} before callbacks DistributionRetryForm.errors = #{errors.messages}")

    return false unless valid?

    run_callbacks :save do
      retriable_distributions.map(&:retry)
    end

    Rails.logger.info("MASSRETRIES: DistributionRetryForm.save after callbacks valid? = #{retriable_distributions}")
    Rails.logger.info("MASSRETRIES: DistributionRetryForm.errors after callbacks = #{errors.messages}")
  rescue ActiveRecord::RecordNotFound => e
    errors.add(:base, e.message.to_s)
    false
  end

  private

  def create_note
    Rails.logger.info("MASSRETRIES: album_id: #{album.id} Note Create Start DistributionRetryForm.errors = #{errors.messages}")
    Note.create(
      related: album,
      note_created_by: retriable_distributions.first.person,
      ip_address: ip_address,
      subject: "Distributions Retried",
      note: "The following distributions were retried: #{converter_class_names}"
    )

    Rails.logger.info("MASSRETRIES: album_id: #{album.id} Note Create End DistributionRetryForm.errors = #{errors.messages}")
  end

  def converter_class_names
    retriable_distributions.map(&:delivery).map(&:converter_class).map { |cc| cc.split("::")[1] }.join(", ")
  end

  def store_ids_for(distribution)
    distribution.salepoints.map(&:store_id).uniq
  end

  def action_type_for(distribution)
    if distribution.delivery_type == "full_delivery"
      "INSERT"
    else
      "UPDATE"
    end
  end
end
