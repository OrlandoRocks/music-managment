# frozen_string_literal: true

class RevenueStreamsTransactionMapping < ApplicationRecord
  belongs_to :revenue_stream
end
