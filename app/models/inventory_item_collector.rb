#
#  A Class to take in options for
#  Inventory.use! and transform them into
#  an object w/ methods and variables
#
class InventoryItemCollector
  attr_accessor :inventory_items

  def self.collect(person, options)
    collector = InventoryItemCollector.new(person, options)
    collector.inventory_items
  end

  #
  #  options
  #  * :item_to_use => Some model
  #  * :product_to_use => Some Product
  #  * :items_to_use [{:item_to_use => item_to_use, :product => product}, {:item_to_use => item_to_use, :product => product}]
  #
  def initialize(person, options)
    @inventory_items = []
    @options = options
    @person = person

    set_inventory_items
    remove_duplicate_items
  end

  protected

  def set_inventory_items
    if given_multiple_items?
      create_multiple_inventories
    else
      @inventory_items << InventoryItem.create(@person, @options)
    end
  end

  def given_multiple_items?
    !!(@options[:items_to_use])
  end

  def create_multiple_inventories
    items_to_use = @options[:items_to_use].flatten
    items_to_use.each do |options|
      begin
        @inventory_items << InventoryItem.create(@person, options)
      rescue
        # silently fail so the process can continue
      end
    end
  end

  def remove_duplicate_items
    # ensure that the array is uniq and w/o nils
    @inventory_items = @inventory_items.compact.uniq
  end
end
