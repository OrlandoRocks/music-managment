class SyncFavoriteSong < ApplicationRecord
  belongs_to :person
  belongs_to :song

  validates :person, :song, presence: true

  validates :song_id, uniqueness: { scope: :person_id }
end
