# This model grabs aggregate data and is only used for display
# purposes.  As such, we encapsulate the underlying ActiveRecord
# objects so that it doesn't allow access to misleading information.
class SalesRecordByStore
  def self.all_for(person, album, reporting_months)
    reporting_months = Array(reporting_months)

    # It's not as ugly as it looks, we're just summing units and cents for sales_records
    # as well as pulling in the exchange/store info from store_intakes
    records = SalesRecord
              .select(%q|store_intakes.store_id as si_store_id,
              sales_records.album_id,
              sales_records.song_id,
              sales_records.substore,
              sales_records.country_of_sale,
              sales_records.sale_type,
              store_intakes.exchange_symbol as si_exchange_symbol,
              store_intakes.exchange_rate_fixed as si_exchange_rate_fixed,
              store_intakes.exchange_rate_scale as si_exchange_rate_scale,
              store_intakes.local_currency as si_local_currency,
              min(sales_records.statement_start_date) as statement_start_date,
              max(sales_records.statement_end_date) as statement_end_date,
              sum(sales_records.units_sold) as units_sold,
              sales_records.local_unit_cents as local_unit_cents,
              sum(sales_records.local_total_cents) as local_total_cents,
              sum(sales_records.usd_total_cents) as usd_total_cents,
              count(distinct store_intakes.exchange_rate_fixed, store_intakes.exchange_rate_scale) as si_exchange_rates|)
              .from("sales_records, store_intakes")
              .group("store_intakes.store_id, sales_records.album_id, sales_records.song_id, sales_records.sale_type, store_intakes.local_currency, sales_records.substore, sales_records.country_of_sale, sales_records.local_unit_cents")
              .order("sales_records.substore, sales_records.country_of_sale ASC")
              .where(%q|sales_records.store_intake_id = store_intakes.id and
              sales_records.person_id = ? and sales_records.album_id = ? and
              sales_records.reporting_month_id in (?)|,
                     person.id,
                     album.id,
                     reporting_months.collect(&:id))

    records.collect do |record|
      SalesRecordByStore.new(record)
    end
  end

  attr_reader :reporting_month, :person

  delegate :album_id,
           :song_id,
           :sale_type,
           :units_sold,
           :songs_sold,
           :songs_streamed,
           :albums_sold,
           :is_album_sale?,
           :is_song_sale?,
           :statement_start_date,
           :statement_end_date,
           :units_sold,
           :local_unit_cents,
           :local_total_cents,
           :country_of_sale,
           :substore,
           :net_sales_usd,
           :net_sales_local,
           to: :record

  def initialize(record)
    self.record = record
  end

  def store_id
    record[:si_store_id].to_i
  end

  def local_currency
    record[:si_local_currency]
  end

  def exchange_symbol
    record[:si_exchange_symbol]
  end

  def exchange_rate
    return if record[:si_exchange_rate_fixed].blank? || record[:si_exchange_rate_scale].blank?

    LongDecimal(record[:si_exchange_rate_fixed], record[:si_exchange_rate_scale].to_i)
  end

  def exchange_rate_count
    record[:si_exchange_rates].to_i
  end

  def has_multiple_exchange_rates?
    exchange_rate_count > 1
  end

  def has_sales?
    return false if data.nil?

    songs_sold != 0 || songs_streamed != 0 || albums_sold != 0
  end

  protected

  attr_writer :reporting_month, :person
  attr_accessor :record
end
