class TargetedProductStore < ApplicationRecord
  belongs_to :targeted_product
  belongs_to :store

  TARGETED_PRODUCT_STORES_MAP = {
    facebook: Store::FB_REELS_STORE_ID,
    discovery_platforms: Store.discovery_platforms.pluck(:id)
  }.with_indifferent_access.freeze

  def self.active_offer?(country_website_id, stores_key)
    TargetedProductStore
      .joins(targeted_product: [:targeted_offer])
      .where(store_id: TARGETED_PRODUCT_STORES_MAP[stores_key])
      .where(targeted_offers: {
               country_website_id: country_website_id,
               status: TargetedOffer::ACTIVE_STATUS
             })
      .where(TargetedOffer.arel_table[:expiration_date].gt(Time.now))
      .exists?
  end
end
