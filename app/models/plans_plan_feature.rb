class PlansPlanFeature < ApplicationRecord
  belongs_to :plan
  belongs_to :plan_feature

  validates :plan_feature, :plan, presence: true
  validates :plan_feature, uniqueness: { scope: :plan }

  after_create { PlansPlanFeatureHistory.create_history!(self, :add) }
  after_destroy { PlansPlanFeatureHistory.create_history!(self, :remove) }
end
