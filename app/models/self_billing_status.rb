# frozen_string_literal: true

class SelfBillingStatus < ApplicationRecord
  belongs_to :person

  validates :status, presence: { message: "self_billing.errors.required" }

  enum status: { accepted: "accepted", declined: "declined" }
end
