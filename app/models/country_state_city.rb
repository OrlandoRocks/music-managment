class CountryStateCity < ApplicationRecord
  belongs_to :country_state, primary_key: :id, foreign_key: :state_id, inverse_of: :country_state_cities
  has_many :people

  scope :for_state,
        ->(state_id) {
          where(state_id: state_id)
            .order(:name)
        }

  scope :auto_complete_query_filter,
        ->(query) {
          query ? where("name like ?", "#{sanitize_sql_like(query)}%") : none
        }

  scope :exact_city_name,
        ->(city_name) {
          where(name: city_name)
        }
end
