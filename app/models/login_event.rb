class LoginEvent < ApplicationRecord
  belongs_to :person
  has_many   :login_tracks

  validates :person, presence: true

  after_commit :geocode, if: :ip_address?, on: :create

  scope :matching_previous_logins, ->(login_event, number_of_previous_logins = 5) do
    where(
      id: where(person_id: login_event.person_id)
          .where.not(id: login_event.id)
          .order(created_at: :desc)
          .limit(Integer(number_of_previous_logins.to_s, 10)).ids,
      subdivision: login_event.subdivision,
      city: login_event.city,
      country: login_event.country
    )
  end

  private

  def geocode
    LoginEventGeocodeWorker.perform_async(id)
  end
end
