class PersonProfileSurveyInfo < ApplicationRecord
  belongs_to :person

  def self.get_survey_info(person)
    find_or_create_by(person_id: person.id)
  end

  def show_interstitial?
    FeatureFlipper.show_feature?(:account_profile_survey, person) && (last_visited_at.blank? || Time.now > (last_visited_at + 30.days)) && segment_question_response.blank?
  end

  def already_viewed?
    last_visited_at.present?
  end

  def set_viewed_status(viewed)
    update({ last_visited_at: viewed ? Time.now : nil })
  end

  def get_survey_token
    return survey_token if survey_token.present?

    new_token = SecureRandom.urlsafe_base64(16)
    update({ survey_token: new_token })
    new_token
  end

  def set_segment_response(response)
    update({ segment_question_response: response })
  end

  def restart_survey
    update({ survey_token: nil, segment_question_response: nil })
  end
end
