class YouTubeIneligibleKeyword < ApplicationRecord
  scope :active, -> { where(active: true) }

  def self.keyword_present?(name)
    active.each { |word| return word.keyword if name.include?(word.keyword) }

    false
  end
end
