# frozen_string_literal: true

class PayinProviderConfig < ActiveRecord::Base
  include ProviderSecretsEncryptable
  BRAINTREE_NAME = "Braintree"
  PAYPAL_NAME = "PayPal"

  before_create :encrypt_attributes

  has_one :cipher_datum, as: :cipherable
  belongs_to :corporate_entity

  # we don't expect to use these associations, but we're creating them just for gigs
  # and cause we just want to correspond to the belongs_to on these models
  has_many :stored_paypal_accounts
  has_many :stored_credit_cards
  has_many :paypal_transactions
  has_many :braintree_transactions

  delegate :affiliated_to_bi?, to: :corporate_entity

  default_scope { by_env }
  scope :by_env,
        -> {
          where(sandbox: !Rails.env.production?)
        }

  # these are temp methods for create which we convert to a JSON
  # string and encrypt on the 'secrets' attribute to save to the db
  attr_accessor :password, :signature, :private_key

  def decrypted_signature
    decrypted_secrets["signature"]
  end

  def decrypted_private_key
    decrypted_secrets["private_key"]
  end

  def encryptable_attributes
    { private_key: private_key, signature: signature, password: password }
  end
end
