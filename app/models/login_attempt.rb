class LoginAttempt < ApplicationRecord
  mattr_accessor :threshold, :ban_threshold
  self.threshold = 5
  # Temp change to deal with customer being locked out - Mike Steinfeld
  self.ban_threshold = 300

  def self.record_failed_attempt(ip_address)
    login_attempt = find_by("ip_address = INET_ATON(?)", ip_address)

    if login_attempt
      # Update the counts
      login_attempt.all_time_count         += 1
      login_attempt.since_last_login_count += 1

      # Reset to 1 if we got to 300 in greater than 45 days
      if login_attempt.all_time_count == 300 && (Time.now - login_attempt.created_at > 45.days)
        login_attempt.all_time_count = 1
        login_attempt.created_at = Time.now
      end

      login_attempt.save
    else
      connection.execute(
        "INSERT INTO login_attempts (ip_address, all_time_count, since_last_login_count, updated_at, created_at)
        VALUES(INET_ATON('#{ip_address}'),1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)"
      )
    end
  end

  def self.reset_since_last_login_count(ip_address)
    login_attempt = find_by("ip_address = INET_ATON(?)", ip_address)
    login_attempt.update_attribute(:since_last_login_count, 0) if login_attempt
  end

  def self.ban_threshold_reached?(ip_address)
    find_by("ip_address = INET_ATON(?) AND all_time_count > ?", ip_address, ban_threshold)
  end
end
