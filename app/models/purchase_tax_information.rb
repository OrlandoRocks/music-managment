# frozen_string_literal: true

class PurchaseTaxInformation < ApplicationRecord
  self.table_name = "purchase_tax_information"
  BUSINESS = "Business"

  VAT_TAX = "VAT"

  belongs_to :purchase

  scope :succeeded, -> { where(error_code: nil) }

  # Details about failsafe method used will be stored in the note column and
  # original error code will be stored in error_code column.
  # But in case we used cache to calculate VAT, the note column will be NULL and
  # error_code will be 'Vat Service Down - Used cache to calculate VAT'.
  scope :failure_handled,
        -> {
          where(
            "error_code = ? OR (error_code IS NOT NULL AND note IS NOT NULL)",
            TcVat::FailSafe::Calculator::VAT_SERVICE_ERROR
          )
        }

  scope :failed,
        -> {
          where(
            "error_code != ? AND error_code IS NOT NULL AND note IS NULL",
            TcVat::FailSafe::Calculator::VAT_SERVICE_ERROR
          )
        }

  def business_transaction?
    customer_type == BUSINESS
  end

  def self.historic_record(person_id, vat_number, member_country)
    record = joins(:purchase)
             .where(vat_registration_number: vat_number)
             .where(purchases: { person_id: person_id })
             .where(customer_location: member_country)
             .order(id: :desc)
             .first
    return if record.nil? || !record.business_transaction?

    {
      id: record.id,
      valid: record.business_transaction?,
      trader_name: record.trader_name
    }
  end
end
