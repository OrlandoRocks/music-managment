class TypefaceEffect < ApplicationRecord
  class TypefaceEffectError; end
  has_many :artist_covers, class_name: "Cover", foreign_key: :artist_typeface_effect_id
  has_many :title_covers, class_name: "Cover", foreign_key: :title_typeface_effect_id

  FONT_PATH = File.join(Rails.root.to_s, "public", "fonts", "artwork_suggestion")

  validates :name, presence: true

  def self.random
    all = self.all
    if all.empty?
      nil
    else
      all[rand(all.size)]
    end
  end

  def render_text_for(cover, render_type)
    render_type = render_type.to_s
    target_raw     = File.join("Cover::#{render_type.upcase}_IMAGES_DIR".constantize, "/raw_#{cover.id_dot_png}")
    target_trimmed = File.join("Cover::#{render_type.upcase}_IMAGES_DIR".constantize, cover.id_dot_png)

    text_for_render = cover.send("#{render_type}_text_for_render").gsub(/"/, '\\\\"')
    font_name       = cover.send("#{render_type}_typeface").name
    pointsize       = cover.send("#{render_type}_typeface_pointsize_for_render")
    render(cover, render_type, text_for_render, font_name, pointsize, target_raw, target_trimmed)
  end

  def render(cover, render_type, text, typeface_name, _typeface_pointsize, target_raw, target_trimmed)
    gravity       = set_gravity(render_type, cover.layout_class)
    width, height = set_size(render_type, cover, text)
    create_target_raw(target_raw)
    convert_image_files(target_raw, target_trimmed, gravity, width, height, typeface_name, text)

    raise TypefaceEffectError,
          %Q(
      Failed to render text for #{render_type}
      Text: #{text}
      Typeface: #{typeface_name}
      Effect: #{name}
    ) unless File.exist?(target_trimmed)
  end

  private

  def escape_special_shell_chars(text)
    text.gsub(/\$/, '\$')
  end

  def set_gravity(render_type, layout_class)
    gravity_constant = "#{render_type.capitalize}Gravity"
    "#{layout_class}::#{gravity_constant}".constantize
  end

  def set_size(render_type, cover, text)
    max_length = "ArtworkSuggestion::Layouts::BorderLayout::MaxLengthFor#{render_type.capitalize}".constantize
    width      = cover.layout.width_for_text_of_length(text.size, max_length)
    height     = cover.layout.height_for_text_of_length(text.size, max_length)
    [width, height]
  end

  def create_target_raw(file)
    file = File.new(file, "w")
    file.close
  end

  def convert_image_files(target_raw, target_trimmed, gravity, width, height, typeface_name, text)
    begin
      ArtworkSuggestion::ImageGenerationService.convert(
        conversion_params(
          target_raw,
          gravity,
          width,
          height,
          typeface_name,
          text,
          ["-stroke", "black"]
        )
      )
    rescue
      ArtworkSuggestion::ImageGenerationService.convert(
        conversion_params(
          target_raw,
          gravity,
          width,
          height,
          typeface_name,
          text
        )
      )
    end

    ArtworkSuggestion::ImageGenerationService.convert(
      {
        source: target_raw,
        target: target_trimmed,
        opts: ["-trim"]
      }
    )
  end

  def conversion_params(target, gravity, width, height, font, text, stroke = nil)
    opts = self.class.opts(gravity, width, height, font, escape_special_shell_chars(text))
    opts = stroke.concat(opts) if stroke
    {
      target: target,
      opts: opts
    }
  end

  def self.opts(gravity, width, height, font, text)
    [
      "-gravity",
      gravity,
      "-size",
      "#{width}X#{height}",
      "-background",
      "transparent",
      "-fill",
      "white",
      "-font",
      File.join(
        FONT_PATH,
        "#{font}.ttf"
      ).to_s,
      "caption:#{text}"
    ]
  end
end
