class BackgroundGenre < ApplicationRecord
  belongs_to :background
  belongs_to :genre

  validates :genre_id, :background_id, presence: true
end
