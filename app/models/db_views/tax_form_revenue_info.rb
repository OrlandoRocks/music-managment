class DBViews::TaxFormRevenueInfo < ApplicationRecord
  self.table_name = :tax_form_revenue_types
  def read_only?
    true
  end
end
