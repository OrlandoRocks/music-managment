# frozen_string_literal: true

class RefundReason < ApplicationRecord
  validates :reason, presence: true

  scope :visible, -> { where(visible: true) }

  BACKFILL = "Backfill"

  def backfill?
    reason == BACKFILL
  end
end
