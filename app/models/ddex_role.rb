class DDEXRole < ApplicationRecord
  has_many :ddex_song_roles
  has_many :song_roles, through: :ddex_song_roles
end
