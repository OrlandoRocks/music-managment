# = Description
#
#
# = Usage
#
#
# = Change Log
# [2010-04-22 -- CH]
# Created Model

class TargetedPerson < ApplicationRecord
  belongs_to  :targeted_offer
  belongs_to  :person

  after_create  :add_to_pop_count, :add_targeted_product_to_purchases
  after_destroy :remove_targeted_product_from_purchases

  validates :person, :targeted_offer, presence: true
  validates :person_id, uniqueness: { scope: :targeted_offer_id }
  validate :targeted_offer_has_products
  validate :person_is_not_part_of_another_population, on: :create
  validate :person_country_website_same_as_targeted_offer

  EXPORT_FILE_HEADERS = ["Person ID", "Person Name", "Person Email"]

  # over
  def destroy
    return if has_used?

    remove_from_pop_count
    super()
  end

  def has_used?
    usage_count.positive?
  end

  def to_csv(csv = nil, options = {})
    options[:include_headers] ||= false

    csv_was_blank = csv.blank?
    csv = CSV.new(response = "", row_sep: "\r\n") if csv.blank?

    csv << EXPORT_FILE_HEADERS if options[:include_headers]

    csv << [
      person_id,
      person.name,
      person.email
    ]

    # Return the response if we are not using this function to build up a larger csv file
    response if csv_was_blank
  end

  def use!
    increment!(:usage_count, 1)
  end

  private

  def add_to_pop_count
    targeted_offer.add_to_pop_count(1)
  end

  def remove_from_pop_count
    targeted_offer.remove_from_pop_count(1)
  end

  # If this individual is not already part of an active TargetedOffer for a similar product
  # they can be added.
  def person_is_not_part_of_another_population
    unless !targeted_offer.products.all.empty? and
           !(
             TargetedOffer.active_offers_for_person_and_products(person, targeted_offer.products.all) - [targeted_offer]
           ).empty?

      return
    end

    errors.add(:person, I18n.t("models.promotion.is_already_part_of_another_targeted_offer_with_the_same_product_or_products"))
  end

  def person_country_website_same_as_targeted_offer
    unless person and targeted_offer && (person.country_website_id.to_i != targeted_offer.country_website_id.to_i)
      return
    end

    errors.add(:person, I18n.t("models.promotion.must_have_the_same_country_website_as_the_targeted_offer"))
  end

  def targeted_offer_has_products
    return unless targeted_offer and targeted_offer.products.blank?

    errors.add(:targeted_offer, I18n.t("models.promotion.must_have_products_added_before_adding_people"))
  end

  #
  # Adds this targeted product to purchases that would be affected and recalculates
  #
  def add_targeted_product_to_purchases
    return unless targeted_offer.status == "Active"

    product_ids = targeted_offer.targeted_products.collect(&:product_id)

    # Loop through this persons purchases that are affected
    person.purchases.unpaid.where(["price_adjustment_histories_count = 0 and product_id in (:product_ids)", { product_ids: product_ids }]).find_each(&:recalculate)
  end

  #
  # Removes this targeted products from purchases that are affected and recalculates
  #
  def remove_targeted_product_from_purchases
    return unless targeted_offer.status == "Active"

    product_ids = targeted_offer.targeted_products.collect(&:product_id)

    # Loop through this persons purchases that are affected
    person.purchases.unpaid.where(["price_adjustment_histories_count = 0 and product_id in (:product_ids)", { product_ids: product_ids }]).find_each do |purchase|
      purchase.product.set_current_targeted_product(nil)
      purchase.recalculate
    end
  end
end
