# frozen_string_literal: true

class PlansStore < ApplicationRecord
  belongs_to :plan
  belongs_to :store
end
