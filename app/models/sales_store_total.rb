# This model grabs aggregate data and is only used for display
# purposes.  As such, we encapsulate the underlying ActiveRecord
# objects so that it doesn't allow access to misleading information.
class SalesStoreTotal
  def self.all_for(person, reporting_months)
    reporting_months = Array(reporting_months).index_by(&:id)

    person_intakes = PersonIntake.includes(:store_intake, :album_intakes, :store).where(
      [
        "person_intakes.person_id = ? and person_intakes.reporting_month_id in (?)",
        person.id,
        reporting_months.keys
      ]
    ).order("stores.position ASC, person_intakes.id ASC")

    # we get min/max here because we can't assume that all start/end dates for
    # an intake are the same.
    statement_ranges =
      SalesRecord.select(
        %q|store_intake_id,
                                                       min(statement_start_date) as statement_start_date,
                                                       max(statement_end_date) as statement_end_date|
      ).group("store_intake_id").where("store_intake_id in (?)", person_intakes.collect(&:store_intake_id)).index_by(&:store_intake_id)

    person_intakes.collect do |pi|
      new(
        reporting_months[pi.reporting_month_id],
        person,
        pi.store,
        pi.store_intake,
        pi,
        pi.album_intakes,
        statement_ranges[pi.store_intake_id]
      )
    end
  end

  attr_reader :reporting_month, :store

  delegate :local_currency,
           :exchange_rate,
           :exchange_symbol,
           to: :store_intake

  delegate :statement_start_date, :statement_end_date, to: :statement_range

  def initialize(reporting_month, person, store, store_intake, person_intake, album_intakes, statement_range)
    self.reporting_month = reporting_month
    self.person = person
    self.store = store
    self.store_intake = store_intake
    self.person_intake = person_intake
    self.album_intakes = album_intakes
    self.statement_range = statement_range
  end

  def songs_sold
    album_intakes.to_a.sum(&:songs_sold)
  end

  def songs_streamed
    album_intakes.to_a.sum(&:songs_streamed)
  end

  def albums_sold
    album_intakes.to_a.sum(&:albums_sold)
  end

  def net_sales_local
    person_intake.local_total_cents
  end

  def net_earnings_usd
    person_intake.usd_payout_cents
  end

  delegate :comment, to: :person_intake

  def has_multiple_exchange_rates?
    false
  end

  protected

  attr_writer :reporting_month, :store
  attr_accessor :person, :store_intake, :person_intake, :album_intakes, :statement_range
end
