# == Schema Information
# Schema version: 404
#
# Table name: download_trackings
#
#  id            :integer(11)     not null, primary key
#  person_id     :integer(11)     not null
#  downloaded_at :datetime        not null
#  promotion     :string(64)      not null
#

class DownloadTracking < ApplicationRecord
end
