class TrendDataSummary < TrendsBase
  self.table_name = "trend_data_summary"

  belongs_to :provider
  belongs_to :person
  belongs_to :album
  belongs_to :song
  belongs_to :trans_type

  has_one :trend_data_detail

  validates :provider, :date, :qty, presence: true

  def self.top_downloaded_releases(user, providers, start_date, end_date, options = {})
    top_releases(user, providers, start_date, end_date, options, [TransType.album_download.id, TransType.song_download.id])
  end

  def self.top_streamed_releases(user, providers, start_date, end_date, options = {})
    top_releases(user, providers, start_date, end_date, options, [TransType.stream.id])
  end

  def self.top_downloaded_songs(user, providers, start_date, end_date, options = {})
    top_songs(user, providers, start_date, end_date, options, [TransType.song_download.id])
  end

  def self.top_streamed_songs(user, providers, start_date, end_date, options = {})
    top_songs(user, providers, start_date, end_date, options, [TransType.stream.id])
  end

  def self.top_ringtones(user, providers, start_date, end_date, options = {})
    start_date = start_date.to_s
    end_date   = end_date.to_s
    sql = %Q{
      SELECT tds.song_id as id, tds.album_id as album_id, sum(tds.qty) as ringtone_qty,
        (select album_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as album_name,
        (select album_type from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_type from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as album_type,
        (select artist_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select artist_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as artist_name,
        (select song_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select song_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as name
      from #{table_name} tds
      WHERE tds.trans_type_id IN (#{TransType.ringtone.id}) and tds.person_id = ? AND tds.date between ? AND ? AND tds.provider_id IN (?)
    }

    sql += "AND tds.album_id = ?" if options[:album]

    sql += %Q(
      GROUP BY tds.song_id
      ORDER BY ringtone_qty DESC, name ASC
    )

    sql_array = [sql, user.id, start_date, end_date, providers]
    sql_array << options[:album].id if options[:album]

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end

    results
  end

  def self.top_downloaded_countries(user, providers, start_date, end_date, options = {})
    top_countries(user, providers, start_date, end_date, options, [TransType.album_download.id, TransType.song_download.id])
  end

  def self.top_streamed_countries(user, providers, start_date, end_date, options = {})
    top_countries(user, providers, start_date, end_date, options, [TransType.stream.id])
  end

  def self.last_trend_date(user, providers)
    sql = %Q{
      SELECT provider_id, max(date) as last_trend_date
      FROM #{table_name}
      where person_id = ? AND provider_id in (?)
      group by provider_id
    }

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, user.id, providers])).each(as: :hash) do |result|
      result = ActiveSupport::HashWithIndifferentAccess.new(result)
      results << result
    end

    results
  end

  def self.last_us_import
    last_import_table = Arel::Table.new(:last_import)
    query = last_import_table.project(:provider_id, :trend_date)
                             .where(last_import_table[:country_code].eq("US").or(last_import_table[:country_code].eq("all")))
                             .group(:provider_id)

    connection.select_all(query.to_sql)
  end

  def self.last_trend_import
    sql = %Q{
      SELECT max(created_at) as last_trend_import
      from #{table_name}
    }

    results = connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql])).each(as: :hash).first
    results["last_trend_import"]
  end

  def self.ringtones_sold?(person, providers, start_date, end_date)
    sql = %Q{
      SELECT max(id) as last_ringtone_trend_id
      FROM #{table_name} WHERE trans_type_id = #{TransType.ringtone.id} AND person_id = ? AND provider_id in (?) AND date between ? AND ?
    }

    sql_array = [sql, person.id, providers, start_date, end_date]

    result = connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, person.id, providers, start_date, end_date])).each(as: :hash).first
    result["last_ringtone_trend_id"].present?
  end

  def self.trend_data_counts_by_day(person, providers, start_date, end_date, options = {})
    start_date = start_date.to_s
    end_date   = end_date.to_s
    sql = %Q{
      SELECT trans_type_id, date as sale_date, sum(qty) as quantity, provider_id
      FROM #{table_name} WHERE person_id = ? AND provider_id IN (?) AND date BETWEEN ? AND ?
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "album_id = ?" if options[:album]

    song_album_query << "song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        AND #{song_album_query.join(' AND ')}
      )
    end

    sql += %Q(
      group by trans_type_id, sale_date
      ORDER BY trans_type_id ASC, date ASC
    )

    sql_array = [sql, person.id, providers, start_date, end_date]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = []
    connection.execute(send(:sanitize_sql_array, sql_array)).each(as: :hash) do |hash|
      results << ActiveSupport::HashWithIndifferentAccess.new(hash)
    end

    results
  end

  def self.all_trend_data(person, providers, start_date, end_date, options = {})
    start_date = start_date.to_s
    end_date   = end_date.to_s
    # FIXME: There is a bug on these two lines when there exists two zip units
    # that have same zipcode and are marked as primary_record: 'P',
    # this exists in the seed data currently.
    #
    # if(tdd.country_code = 'US', (select city_mixed_case from zipcodes where zipcode = tdd.zip_code and primary_record = 'P'), NULL) as city,
    # if(tdd.country_code = 'US', (select state from zipcodes where zipcode = tdd.zip_code and primary_record = 'P'), NULL) as state,
    #
    # `Mysql2::Error: Subquery returns more than 1 row`
    #
    sql = %Q{
      select tds.date as sale_date, tds.provider_id,
        tdd.album_name,
        tdd.album_type,
        tdd.song_name,
        tdd.tunecore_isrc,
        tdd.optional_isrc,
        tdd.upc as album_upc,
        tdd.artist_name,
        (select name from #{TUNECORE_DB}.countries where iso_code = tdd.country_code) as country_name,
        if(tdd.country_code = 'US', (select city_mixed_case from zipcodes where zipcode = tdd.zip_code and primary_record = 'P'), NULL) as city,
        if(tdd.country_code = 'US', (select state from zipcodes where zipcode = tdd.zip_code and primary_record = 'P'), NULL) as state,
        tdd.zip_code as zip,
        tdd.qty as quantity,
        (select name from trans_types where id = tdd.trans_type_id) as trans_type_name
      from #{table_name} tds
      inner join
      (
        select * from trend_data_detail where person_id = ? AND provider_id in (?) AND sale_date between ? AND ?
        UNION ALL
        select * from trend_data_detail_two  where person_id = ? AND provider_id in (?) AND sale_date between ? AND ?
      ) tdd on trend_data_summary_id = tds.id
        where tds.person_id = ? AND tds.provider_id in (?) AND tds.date between ? AND ?
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        AND #{song_album_query.join(' AND ')}
      )
    end

    sql_array = [sql, person.id, providers, start_date, end_date, person.id, providers, start_date, end_date, person.id, providers, start_date, end_date]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    data  = find_by_sql(sql_array)
    count = all_trend_data_count(person, providers, start_date, end_date, options)

    return data, count
  end

  def self.all_trend_data_count(person, providers, start_date, end_date, options = {})
    start_date = start_date.to_s
    end_date   = end_date.to_s
    sql = %Q{
      SELECT sum(qty), provider_id
      FROM #{table_name} where person_id = ? AND provider_id IN (?) AND date between ? and ?
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "album_id = ?" if options[:album]

    song_album_query << "song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        AND #{song_album_query.join(' AND ')}
      )
    end

    sql_array = [sql, person.id, providers, start_date, end_date]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = connection.execute(send(:sanitize_sql_array, sql_array)).first
    results[0].to_i if results
  end

  def self.top_us_markets(user, start_date, end_date, options = {})
    start_date = start_date.to_s
    end_date   = end_date.to_s
    tds_ids = get_tds_ids(user, start_date, end_date, options, [1, 3])
    return [] if tds_ids.empty?

    sql_condition = "where trend_data_summary_id IN ( #{tds_ids.join(',')}) AND cbsa != '' and cbsa is not null and country_code = 'US'"
    sql = %Q{
      select g.cbsa, sum(g.qty) as qty, g.date,
        (select cbsa_name from zipcodes where cbsa = g.cbsa AND cbsa_name is not null and cbsa_name != "" and primary_record = 'P' limit 1) as cbsa_name,
        (select COALESCE(cbsa_short_name, cbsa_name) from zipcodes where cbsa = g.cbsa and primary_record = 'P' limit 1) as cbsa_short_name,
        (select state from zipcodes where cbsa = g.cbsa and primary_record = 'P' limit 1) as state
      from (
        select * from geo
        #{sql_condition}
        UNION ALL
        select * from geo_two
        #{sql_condition}
        ) g
      GROUP BY g.cbsa
      ORDER BY qty DESC, cbsa_name ASC, state ASC;
    }

    results = []
    connection.execute(sql).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  def self.top_releases(user, providers, start_date, end_date, _options, trans_types)
    start_date = start_date.to_s
    end_date   = end_date.to_s
    stream = trans_types.include? TransType.stream.id
    sql = %Q{
      SELECT tds.album_id as id,
	           tds.date, tds.provider_id, trans_type_id,
	           (select album_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as name,
             (select album_type from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_type from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as album_type,
             (select artist_name from trend_data_detail where trend_data_summary_id = tds.id  and person_id = #{user.id} UNION ALL select artist_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as artist_name,
    }
    if !stream
      sql += %Q{
        sum( if( tds.trans_type_id = #{TransType.album_download.id}, tds.qty,  0 )) as album_qty,
        sum( if( tds.trans_type_id = #{TransType.song_download.id}, tds.qty, 0)) as song_qty,
        sum( if( tds.trans_type_id = #{TransType.album_download.id}, tds.qty, if( tds.trans_type_id = #{TransType.song_download.id}, .1*tds.qty, 0))) as rank
      }
    else
      sql += %Q{
        sum(tds.qty) as stream_qty,
        sum(tds.qty) as rank
      }
    end

    sql += %Q{
      from #{table_name} tds
      WHERE tds.person_id = ? AND tds.provider_id IN (?) AND tds.date between ? AND ? AND tds.trans_type_id in (?)
      GROUP BY tds.album_id
      ORDER BY rank DESC, name ASC
    }

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, user.id, providers, start_date, end_date, trans_types])).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end

    results
  end

  def self.top_songs(user, providers, start_date, end_date, options, trans_types)
    start_date = start_date.to_s
    end_date   = end_date.to_s
    sql = %Q{
      SELECT tds.song_id as id, tds.album_id as album_id, tds.provider_id, sum(tds.qty) as qty, tds.trans_type_id,
        (select album_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as album_name,
        (select album_type from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select album_type from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as album_type,
        (select artist_name from trend_data_detail where trend_data_summary_id = tds.id and person_id = #{user.id} UNION ALL select artist_name from trend_data_detail_two where trend_data_summary_id = tds.id and person_id = #{user.id} limit 1) as artist_name,
        (select song_name from trend_data_detail where trend_data_summary_id = tds.id UNION ALL select song_name from trend_data_detail_two where trend_data_summary_id = tds.id limit 1) as name
      from #{table_name} tds
      WHERE tds.trans_type_id IN (?) and tds.person_id = ? AND tds.date between ? AND ? AND tds.provider_id IN (?)
    }

    sql += "AND tds.album_id = ?" if options[:album]

    sql += %Q(
      GROUP BY tds.song_id
      ORDER BY qty DESC, name ASC
    )

    sql_array = [sql, trans_types, user.id, start_date, end_date, providers]
    sql_array << options[:album].id if options[:album]
    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  def self.top_countries(user, providers, start_date, end_date, options, trans_types)
    start_date = start_date.to_s
    end_date   = end_date.to_s
    tds_ids = get_tds_ids(user, start_date, end_date, options, providers, trans_types)
    return [] if tds_ids.empty?

    sql_condition = "where trend_data_summary_id IN ( #{tds_ids.join(',')}) AND country_code is not null"
    sql = %Q{
      select g.country_code as iso_code, sum(g.qty) as qty, tds.provider_id,
      (select name from #{TUNECORE_DB}.countries where iso_code = g.country_code) as name
      from (
        select * from geo
        #{sql_condition}
        UNION ALL
        select * from geo_two
        #{sql_condition}
        ) g
      INNER JOIN trend_data_summary tds on tds.id = g.trend_data_summary_id
      GROUP BY g.country_code
      ORDER BY qty DESC, name ASC, g.country_code ASC;
    }

    results = []
    connection.execute(sql).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end

    results
  end

  def self.get_tds_ids(user, start_date, end_date, options, providers = nil, trans_types = nil)
    sql = %Q(
      select id
      from #{table_name} tds
      where tds.person_id = ? and tds.date between ? AND ?
    )

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tds.provider_id IN (?)" if providers
    song_album_query << "tds.trans_type_id IN (?)" if trans_types
    song_album_query << "tds.album_id = ?" if options[:album]
    song_album_query << "tds.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        AND #{song_album_query.join(' AND ')}
      )
    end

    sql_array = [sql, user.id, start_date.to_s, end_date.to_s]
    sql_array << providers if providers
    sql_array << trans_types if trans_types
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = []
    connection.select_all(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).collect { |v| v["id"] }
  end
end
