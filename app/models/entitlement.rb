# =Description
# The Entitlement model holds a reference to the EntitlementRightsGroup.
# An Entitlement does not exists without a related Renewal. The Renewal
# ties the Entitlement to the Person model through RenewalItem -> Renewal.
#
# =Changelog
# [2010-11-10 -- CH]
# Created Class
#
# [2010-01-06 -- CH]
# Removed Tunecore:Purchaseable methods

class Entitlement < ApplicationRecord
  #################
  # Associations #
  #################

  belongs_to :entitlement_rights_group
  has_many :entitlement_rights, through: :entitlement_rights_group

  has_many :renewal_items, foreign_key: :related_id
  has_many :renewals, through: :renewal_items

  delegate :name, to: :entitlement_rights_group

  ################
  # Validations  #
  ################
  # validates_presence_of :person_id

  #################
  # Named Scopes #
  #################

  scope :with_history,
        -> {
          joins(
            %{
      INNER JOIN renewal_items ri ON (ri.related_id=entitlements.id AND ri.related_type='Entitlement')
      INNER JOIN renewals r ON ri.renewal_id=r.id
      INNER JOIN renewal_history rh ON rh.renewal_id=r.id
    }
          ).select("entitlements.*, MIN(rh.starts_at) as starts_at, MAX(rh.expires_at) as expires_at").group("entitlements.id")
        }

  # In order to tell whether or not an entitlement is active,
  # we must join through renewal_items to renewals and then to renewal_history
  # then get the maximum expiration date, minimum start date
  def self.active
    where("expires_at >= NOW()").with_history
  end

  # In order to tell whether or not an entitlement has expired,
  # we must join through renewal_items to renewals and then to renewal_history
  # then get the maximum expiration date, minimum start date
  def self.expired
    where("expires_at < NOW()").with_history
  end

  # The entitlements table itself does not have a person_id, so
  # it has to be gotten through the renewals table
  def self.by_person_id(person_id)
    where("r.person_id=?", person_id).with_history
  end

  def self.active_for_person(person)
    active.by_person_id(person.id).select("entitlements.*, MIN(rh.starts_at) as starts_at, MAX(rh.expires_at) as expires_at")
  end

  def self.expired_for_person(person)
    expired.by_person_id(person.id).select("entitlements.*, MIN(rh.starts_at) as starts_at, MAX(rh.expires_at) as expires_at")
  end

  #
  #  has_active_access_rights? checks to see if there is an active entitlement with a specific set of access
  #  rights. This is used when a controller action requires a check to see if the user has rights at the given time.
  #  If a user's subscription has expired, this method will return false,
  #  use the load_by_access_rights to load whether or not the user ever had access.
  #
  def self.person_has_active_access_rights?(person, controller, action)
    found = Entitlement.select("e.id, e.entitlement_rights_group_id, r.canceled_at, MIN(rh.starts_at) as starts_at, MAX(rh.expires_at) as expires_at")
                       .joins(%{
                          e INNER JOIN renewal_items ri on (ri.related_id=e.id and ri.related_type='Entitlement')
                          INNER JOIN renewals r on r.id=ri.renewal_id
                          INNER JOIN renewal_history rh on rh.renewal_id=r.id
                          INNER JOIN entitlement_rights er on er.entitlement_rights_group_id=e.entitlement_rights_group_id
                        })
                       .where(["r.person_id = :person_id AND (rh.starts_at <= :todays_date AND rh.expires_at >= :todays_date) AND er.controller = :controller AND er.action = :action", { person_id: person.id, todays_date: Time.new, controller: controller, action: action }])
                       .group("r.id")
    found.present?
  end

  #
  # loads all entitlements from the associated permissions. This allows any subscription
  # that provides access to a specific set of dated records (e.g. trend reports) to maintain
  # access to each set of dates for that subscription
  #
  def self.load_access_entitlements_for_person(person, controller, action)
    Entitlement.select(
      %{e.id, e.entitlement_rights_group_id, r.canceled_at, MIN(rh.starts_at) as starts_at, MAX(rh.expires_at) as expires_at,
                     if(pi.first_renewal_interval IS NULL,p.first_renewal_interval,pi.first_renewal_interval) as first_renewal_interval,
                     if(pi.first_renewal_duration IS NULL,p.first_renewal_duration,pi.first_renewal_duration) as first_renewal_duration,
                     if(pi.renewal_interval IS NULL,p.renewal_interval,pi.renewal_interval) as renewal_interval,
                     if(pi.renewal_duration IS NULL,p.renewal_duration,pi.renewal_duration) as renewal_duration,
                     if(pi.renewal_product_id IS NULL,p.renewal_product_id,pi.renewal_product_id) as renewal_product_id,
                     pi.does_not_renew}
    )
               .joins(%{e INNER JOIN renewal_items ri on (ri.related_id=e.id and ri.related_type='Entitlement')
                     INNER JOIN renewals r on r.id=ri.renewal_id
                     INNER JOIN renewal_history rh on rh.renewal_id=r.id
                     INNER JOIN entitlement_rights er on er.entitlement_rights_group_id=e.entitlement_rights_group_id
                     LEFT JOIN product_items pi on (r.item_to_renew_id=pi.id and r.item_to_renew_type='ProductItem')
                     LEFT JOIN products p on (r.item_to_renew_id=p.id and r.item_to_renew_type='Product')})
               .where(["r.person_id = :person_id AND er.controller = :controller AND er.action = :action", { person_id: person.id, controller: controller, action: action }])
               .order("rh.expires_at")
               .group("r.id")
  end

  def active?
    Time.now < expires_at
  end

  def canceled?
    !!canceled_at
  end

  def canceled_at
    result = attributes["canceled_at"]
    DateTime.parse(result) if result
  end

  # value filled by scopes in select clause
  def starts_at
    result = attributes["starts_at"]
    DateTime.parse(result) if result
  end

  # value filled by scopes in select clause
  def expires_at
    result = attributes["expires_at"]
    DateTime.parse(result) if result
  end

  def renewal_ids
    renewals.collect(&:id)
  end

  def history
    params = { renewal_ids: renewal_ids }
    RenewalHistory.where("renewal_id IN (:renewal_ids)", params)
  end
end
