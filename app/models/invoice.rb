class Invoice < ApplicationRecord
  include Tunecore::LockableLock
  include Tunecore::AdminReports::PurchaseReportDataFields
  include GstTaxable
  include StripEmojiMethods
  include BatchTransactionScopes
  include PlanPurchaseMethods
  include RefundMethods
  include InvoiceRenewable
  include InvoicePurchaseMethods
  extend MoneyField
  extend Logging
  money_reader :invoice_amount,
               :settlement_amount,
               :final_settlement_amount,
               :outstanding_amount,
               :vat_amount

  has_many :purchases
  has_many :invoice_settlements, dependent: :destroy
  has_many :braintree_transactions
  has_many :paypal_ipns
  has_many :paypal_transactions
  has_many :batch_transactions
  has_many :invoice_logs
  has_many :payments_os_transactions, class_name: "PaymentsOSTransaction"
  has_many :stored_credit_cards, through: :payments_os_transactions
  has_many :refunds
  has_many :payu_transactions
  has_many :disputes

  belongs_to :gst_config, optional: true
  belongs_to :gst_info, optional: true
  belongs_to :foreign_exchange_pegged_rate
  belongs_to :person
  belongs_to :foreign_exchange_rate
  belongs_to :corporate_entity

  has_one :invoice_static_customer_address, as: :related, dependent: :destroy
  has_one :invoice_static_corporate_entity, as: :related, dependent: :destroy
  has_one :invoice_static_customer_info, as: :related, dependent: :destroy

  before_validation :set_currency
  before_validation :set_corporate_entity
  validates :currency, :corporate_entity, presence: true
  after_destroy :detach_purchases

  after_save_commit :generate_static_content_and_sequence, if: :saved_change_to_settled_at?

  # db level validation is also inplace to validate uniqueness of invoice_sequence with corporate_entity_id which allows nil values
  validates :invoice_sequence, uniqueness: { scope: :corporate_entity_id, allow_nil: true }, if: :corporate_entity_id

  # number of decimal places used to display invoice (e.g I000023, I123456)
  # currently 6, will need to be bumped to 7 as we approach limit 999,999 (272,000 Invoices on 3/10/2010)
  DECIMALS_IN_INVOICE_NUMBER = 6
  BI_INVOICE_PREFIX = "BI-INV".freeze
  TC_INVOICE_PREFIX = "TC-INV".freeze
  EUR = "EUR".freeze

  scope :for_country_website, ->(country_website_id) do
                                joins(:person)
                                  .where(people: { country_website_id: country_website_id })
                              end

  scope :paid, -> { where("settled_at IS NOT NULL") }

  scope :settled_at_before, ->(date) { where("settled_at < ?", date) }

  scope :settled_at_present, -> { where.not(settled_at: nil) }

  scope :excluding_products, ->(product_ids) do
    joins(:purchases).where.not(purchases: { product_id: product_ids })
  end

  scope :including_products, ->(product_ids) do
    joins(:purchases).where(purchases: { product_id: product_ids })
  end

  scope :settled_only_with, ->(transaction_types) do
                              all_transaction_types = ["PersonTransaction", "BraintreeTransaction", "PaypalTransaction"]
                              other_transaction_types = all_transaction_types - transaction_types

                              join_query = []
                              conditions = []

                              transaction_types.each_with_index { |tt, i|
                                join_query << "inner join invoice_settlements as invoice_settlements#{i} on invoice_settlements#{i}.invoice_id = invoices.id and invoice_settlements#{i}.source_type = '#{tt}'"
                              }

                              other_transaction_types.each_with_index { |tt, i|
                                join_query << "left join invoice_settlements as invoice_settlements#{i + transaction_types.count} on invoice_settlements#{i + transaction_types.count}.invoice_id = invoices.id and invoice_settlements#{i + transaction_types.count}.source_type = '#{tt}'"
                                conditions << "invoice_settlements#{i + transaction_types.count}.id is null"
                              }

                              joins(join_query.join(" "))
                                .where(conditions.join(" AND "))
                            end

  scope :for_gst_by_country_website, ->(country_website_id = CountryWebsite::INDIA) do
                                       for_country_website(country_website_id)
                                         .joins("left join (select max(id) as gst_id, person_id from gst_infos group by person_id) gst on people.id = gst.person_id")
                                         .joins("left join gst_infos on gst.gst_id = gst_infos.id")
                                     end

  scope :within_month_of_date, ->(date) do
                                 where(settled_at: (date - 1.month)..date)
                               end

  scope :where_settled, -> do
                          where.not(final_settlement_amount_cents: nil)
                        end

  scope :not_sent, -> do
                     where(invoice_sent_at: nil)
                   end

  scope :with_invoice_sequence, -> { where.not(invoice_sequence: nil) }

  delegate :country_website,    to: :person, allow_nil: true
  delegate :country_website_id, to: :person, allow_nil: true

  def self.factory(person, purchases, batch_status = "visible_to_customer")
    purchases = [purchases].flatten
    return false if purchases.blank?

    ActiveRecord::Base.transaction do
      create!(
        person: person,
        batch_status: batch_status,
        foreign_exchange_pegged_rate_id: ForeignExchangePeggedRateService.current_pegged_rate_id(person)
      ).tap do |invoice|
        purchases.each do |purchase|
          purchase.attach_to_invoice(invoice)
        end
      end
    end
  rescue
    message = "invoice.factory failed"
    options = {
      person_id: person&.id,
      purchase_id: purchases.map(&:id),
      batch_status: batch_status
    }
    Airbrake.notify(message, options)
  end

  def settlement_received(source_purchase, amount_in_cents)
    settlement = InvoiceSettlement.new(
      invoice: self,
      source: source_purchase,
      settlement_amount_cents: amount_in_cents
    )
    set_gst_config_id(settlement)

    # okay, we create the object outside of the invoice_settlements association
    # so that any duplicate :source objects are caught by the InvoiceSettlement
    # validation *and* we don't pollute invoice_settlements with an invalid record
    # (which would have been there using build or create)
    return unless settlement.save

    # it was saved, reload the settlements
    invoice_settlements.reload
  end

  def settled!(t = Time.now)
    raise ArgumentError, "invoice already settled" if settled?
    raise ArgumentError, "your settlements do not match costs" unless can_settle?

    # Refer https://api.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters/DatabaseStatements.html#method-i-transaction
    # to better understand significance of `requires_new`
    #
    # mark the settlement event off in its own transaction
    # final_settlement_amount_cents includes VAT
    transaction(requires_new: true) do
      Purchase.process_purchases_for_invoice(self)

      # Add VAT amount in Euro for invoice and purchases
      TcVat::VatEuroConverter.new(invoice: self).convert_to_euro! unless currency == EUR

      update!(
        settled_at: t,
        final_settlement_amount_cents: settlement_amount_cents,
        batch_status: "visible_to_customer",
        vat_amount_cents: invoice_vat_amount_cents
      )
    end
  end

  #
  # Used to prevent an invoice from being destroyed if it has any settlements at all.
  # I've exposed the ability to destory an invoice to admins if it only has Balance payment
  # settlements.  The signature didn't change, so customer facing logic still remains.
  # 2009-11-6 -- CH -- edited method
  #
  def can_destroy?(is_admin = false)
    if !is_admin
      (!settled? || !has_settlements?) && is_visible_to_customer?
    else
      can_be_credited_and_destroyed? && is_visible_to_customer?
    end
  end

  def can_be_credited_and_destroyed?
    !settled? || (!has_settlements? || invoice_settlements.select { |s| s.source_type != "PersonTransaction" }.empty?)
  end

  def is_visible_to_customer?
    batch_status == "visible_to_customer"
  end

  # creates an array of items in the correct form for use by the paypal api
  def items_for_paypal
    # currently only passes one item for an invoice
    return [{ name: "Invoice ID:#{id}", qty: 1, amt: outstanding_amount_cents.to_f / 100 }] if has_settlements?

    purchases.select(&:cents_greater_than_zero?).map(&:item_for_paypal)
  end

  #
  # Does just what it says, credits and removes invoice settlements.
  # This function gives us the ability to
  # to destroy invoices that have settlements paid by a Balance Payment.
  # It was added because some users partially pay for an invoice and then
  # can't edit their albums while it's in invoice.
  # 2009-11-06 -- CH -- added method
  #
  def credit_and_remove_settlements!
    settlements = invoice_settlements
    unless can_be_credited_and_destroyed?
      raise RuntimeError, "Only invoices partially paid with a TuneCore balance can be credited."
    end

    balance_to_repay = settlements.sum(:settlement_amount_cents)
    person_to_credit = person

    return if balance_to_repay.zero?

    PersonTransaction.create!(
      person: person_to_credit,
      debit: 0,
      credit: Tunecore::Numbers.cents_to_decimal(balance_to_repay),
      target: person_to_credit,
      comment: "Refunded partial payment for invoice #{code}. Original invoice date: #{created_at.strftime('%B %d, %Y')}"
    )

    revert_vat!
    outbound_invoice&.destroy

    InvoiceSettlement.where("invoice_id=?", id).delete_all
  end

  def revert_vat!
    return if vat_tax_adjustment.blank?

    transaction do
      create_debit_transaction_for_vat(vat_tax_adjustment)
      vat_tax_adjustment.destroy
    end
  end

  def has_settlements?
    invoice_settlements.exists?
  end

  def vat_tax_adjustment
    invoice_settlements.joins(:vat_tax_adjustment).first&.vat_tax_adjustment
  end

  def outbound_invoice
    invoice_settlements.joins(:outbound_invoice).first&.outbound_invoice
  end

  def code
    "I" + sprintf("%#{DECIMALS_IN_INVOICE_NUMBER}d", id)
  end

  def batch_attempts
    batch_transactions.count
  end

  def invoice_amount_cents
    purchases.sum(&:purchase_total_cents)
  end

  def invoice_vat_amount_cents
    purchases.sum(:vat_amount_cents)
  end

  def amount
    Tunecore::Numbers.cents_to_decimal(purchases.sum(&:purchase_total_cents))
  end

  def settlement_amount_cents
    invoice_settlements.sum(&:settlement_amount_cents)
  end

  def outstanding_amount_cents
    [0, invoice_amount_cents - settlement_amount_cents].max
  end

  def outstanding_amount_in_local_currency
    pegged_rate.present? ? (outstanding_amount_cents * pegged_rate).to_i : outstanding_amount_cents
  end

  def gst_tax_amount(amount = final_settlement_amount)
    tax_amount(gst_config, amount)
  end

  def final_settlement_amount_without_tax(amount = final_settlement_amount)
    base_amount(gst_config, amount)
  end

  def can_settle?
    !settled? && (settlement_amount_cents >= invoice_amount_cents)
  end

  def settled?
    settled_at.present?
  end

  def okay_to_release?(_lockable)
    settled?
  end

  def remove_batch_status
    update!(batch_status: "visible_to_customer")
  end

  def status_text
    case
    when settled?
      "settled"
    when has_settlements?
      "partial"
    when can_destroy?
      "pending"
    else
      "strange!"
    end
  end

  def person_transaction_settlement
    invoice_settlements.find_by(source_type: PersonTransaction.to_s)
  end

  def paypal_item_string
    "#{person_id}::#{id}"
  end

  def release_purchases
    credit_usages = purchases.select { |p| p.related_type == "CreditUsage" }.map(&:related)
    release_types = %w[Album Single Ringtone].freeze
    (credit_usages + purchases).select { |p| release_types.include?(p.related_type) }.map(&:related)
  end

  def contains_distribution_product?
    # If we have more non-distribution product, we would then get a list of products instead of using this exclusion
    if purchases.any? { |p| p.related_type == "Album" || p.related_type == "CreditUsage" }
      true
    else
      false
    end
  end

  def self.add_renewal_purchases_to_invoices(person, person_purchases)
    balance_purchases      = []
    other_purchases        = []
    balance_purchase_total = 0
    invoices               = []

    log("Payment Batch", "Creating invoices for person", person_id: person.id, purchases: person_purchases.collect(&:id).join(","))

    if person_purchases.empty?
      log("Payment Batch", "No invoices created for person", person_id: person.id)
    else
      # Sort by priority of renewal
      purchases = Purchase.prioritize_purchases(person_purchases)

      # Add as much as we can to balance purchases
      purchases.each do |purchase|
        break unless balance_purchase_total + purchase.cost_cents < (person.person_balance.balance * 100)

        balance_purchase_total += purchase.cost_cents

        balance_purchases << purchase
      end unless additional_artist_plan_renewal_fails?(person, purchases)

      # Get the other purchases
      other_purchases = person_purchases - balance_purchases

      # add a rescue here in case a person selects Pay Now for their invoice at the very second we try to process it
      begin
        unless balance_purchases.empty?
          balance_invoice       = Invoice.factory(person, balance_purchases, "waiting_for_batch")
        end
        unless other_purchases.empty?
          other_payment_invoice = Invoice.factory(person, other_purchases,   "waiting_for_batch")
        end

        invoices << balance_invoice       if balance_invoice.present?
        invoices << other_payment_invoice if other_payment_invoice.present?
      rescue
        message = "Payment Batch | comment = ERROR - Invoice creation errored out person_id=#{person&.id}"
        options = {
          person: person,
          message: message,
          current_method_name: __callee__,
          caller_method_path: caller&.first
        }
        Airbrake.notify(
          message,
          {
            person_id: person.id,
            current_method_name: __callee__
          }
        )
        InvoiceLogService.log(options)
        logged = message
      end

      # log invoice payment transaction for batch (if it didn't error out)
      if balance_invoice
        log("Payment Batch", "Created balance invoice",       person_id: person.id, invoice_id: balance_invoice.id,       amount: balance_invoice.amount,       purchases: balance_purchases.collect(&:id).join(","))
      end
      if other_payment_invoice
        log("Payment Batch", "Created other payment invoice", person_id: person.id, invoice_id: other_payment_invoice.id, amount: other_payment_invoice.amount, purchases: other_purchases.collect(&:id).join(","))
      end
    end

    invoices
  end

  def self.ids_by_settlement(settled = true, person_id = false)
    id_name = person_id ? "distinct(person_id)" : "id"
    connection.select_values("select #{id_name} from invoices where settled_at is #{'not' if settled} null").map(&:to_i)
  end

  def self.settled_ids
    ids_by_settlement true
  end

  def self.unsettled_ids
    ids_by_settlement false
  end

  def self.ids_of_people_with_settled_invoices
    ids_by_settlement(true, true)
  end

  def self.ids_of_people_with_unsettled_invoices
    ids_by_settlement(false, true)
  end

  def self.count_people_with_settled_invoices
    ids_of_people_with_settled_invoices.size
  end

  def self.count_people_with_unsettled_invoices
    ids_of_people_with_unsettled_invoices.size
  end

  # returns true for customers that have previously paid for an invoice
  # at the time the method is called
  def self.previous_customer?(person)
    where("person_id = ? AND settled_at IS NOT NULL", person.id).count >= 1
  end

  def set_currency
    self.currency = person.currency
  end

  def check_and_set_first_time_distribution_or_credit
    if first_distro_person_analytic.blank?
      !!set_person_analytic_if_first_time_distro
    else
      first_distro_person_analytic.metric_value == id
    end
  end

  def send_refer_friend_purchase_notification(new_customer)
    ReferAFriendNotificationWorker.perform_async(id, new_customer)
  end

  def has_incomplete_static_data?
    invoice_static_corporate_entity.blank? ||
      invoice_static_customer_info.blank? ||
      invoice_static_customer_address.blank?
  end

  def set_person_analytic_if_first_time_distro
    return false if settled_at.blank?
    return false unless album_or_credit_purchase?
    return false if first_distro_person_analytic.present?

    # Marketing wants to consider paid Plan purchases as an "initial distribution purchase"
    # even if the user is not distributing a release during this initial purchase.

    person.create_analytic_by_metric_name_and_value("first_distribution_invoice_id", id) if paid_distribution?
  end

  def payment_methods_used
    methods = []
    invoice_settlements.each do |i|
      methods.push("TuneCore balance") if i.source_type == "PersonTransaction"
      methods.push("credit card") if i.source_type == "BraintreeTransaction"
      methods.push("Paypal") if i.source_type == "PaypalTransaction"
      methods.push(i.source.payment_method_display_name) if i.source_type == "AdyenTransaction"
    end
    methods
  end

  def recently_purchased_first_distro_and_publishing?(is_first_time_dist_customer = nil, songwriter_product_purchase = nil)
    songwriter_product_purchase = publishing_purchase? if songwriter_product_purchase.nil?
    is_first_time_dist_customer = check_and_set_first_time_distribution_or_credit if is_first_time_dist_customer.nil?

    if is_first_time_dist_customer && songwriter_product_purchase
      true
    elsif is_first_time_dist_customer
      purchased_publishing_recently?(1.day)
    elsif songwriter_product_purchase
      purchased_first_distro_recently?(1.day)
    else
      false
    end
  end

  def artist_services_product_names
    artist_services_product_ids = []

    Product::PRODUCT_COUNTRY_MAP.each_value do |country|
      artist_services_product_ids += [country[:ytm], country[:mastered_track], country[:soundout], country[:facebook]].flatten
    end

    purchases.where(product_id: artist_services_product_ids).includes(:product).map { |p| { product_name: p.product.name, purchase_id: p.id } }
  end

  def update_person_analytics
    PersonAnalyticWorker.perform_async(id)
  end

  def self.analytics_by_date(start_date, end_date)
    where(
      "DATE(settled_at) >= ? AND DATE(settled_at) <= ?",
      start_date,
      end_date
    )
      .uniq(&:person_id)
  end

  def self.sales_amounts(for_date)
    joins(:person)
      .select("people.country_website_id, truncate(sum(final_settlement_amount_cents)/100,2) total_sales, truncate(avg(final_settlement_amount_cents)/100,2) average_sales")
      .where(settled_at: for_date)
      .group("people.country_website_id")
      .order("people.country_website_id")
  end

  def self.renewal_customers(for_date)
    joins(purchases: { product: :created_by })
      .select("people.country_website_id, count(distinct people.id) num_customers")
      .where(settled_at: for_date)
      .where(products: { product_family: "Renewal" })
      .group("people.country_website_id")
      .order("people.country_website_id")
  end

  def self.returning_dist(for_date)
    joins(<<-SQL.strip_heredoc)
      INNER JOIN purchases ON invoices.id = purchases.invoice_id
      INNER JOIN products ON purchases.product_id = products.id
      INNER JOIN people ON purchases.person_id = people.id
      INNER JOIN person_analytics ON person_analytics.person_id = people.id
        AND person_analytics.metric_name = 'first_distribution_invoice_id'
      INNER JOIN invoices invoices_2 ON invoices_2.id = person_analytics.metric_value
      LEFT JOIN person_analytics person_analytics_2 ON person_analytics_2.person_id = people.id
        AND person_analytics_2.metric_name = 'publishing_invoice_id'
      LEFT JOIN invoices invoices_3 ON invoices_3.id = person_analytics_2.metric_value
    SQL
      .select("people.country_website_id, count(distinct people.id) num_customers")
      .where(
        "products.product_family = ?
        AND date(invoices.settled_at) = ?
        AND (
          date(invoices_2.settled_at) < ?
            OR date(coalesce(invoices_3.settled_at, curdate() + INTERVAL 1 DAY)) < ?
        )",
        "Distribution",
        for_date,
        for_date,
        for_date
      )
      .group("people.country_website_id")
      .order("people.country_website_id")
  end

  def self.product_sales(for_date)
    joins(purchases: :product)
      .select(<<-SQL.strip_heredoc)
      products.id,
      products.country_website_id,
      products.name,
      products.product_family,
      products.country_website_id,
      COUNT(1) product_count,
      truncate(sum(purchases.cost_cents)/100,2) product_amount
    SQL
      .where(settled_at: for_date)
      .group("1, 2, 3, 4, 5")
  end

  def associated_product_types
    credit_usage_ids = purchases.where(related_type: CreditUsage).pluck(:related_id)
    credit_used_product_types = CreditUsage.where(id: credit_usage_ids).pluck(:applies_to_type)
    purchased_product_types = Product.where(id: purchases.pluck(:product_id)).pluck(:applies_to_product)

    [credit_used_product_types + purchased_product_types].flatten
  end

  def associated_album_ids
    credit_usage_ids = []
    purchased_album_ids = []

    purchases.each do |p|
      case p.related_type
      when "Album"
        purchased_album_ids << p.related_id
      when "CreditUsage"
        credit_usage_ids << p.related_id
      end
    end

    credit_album_ids = CreditUsage
                       .where(id: credit_usage_ids, related_type: "Album")
                       .pluck(:related_id)

    [credit_album_ids + purchased_album_ids].flatten
  end

  def distributed_item_before?(item, exclusion)
    items_purchased = []
    if item == Album
      items_purchased = person.albums.full_albums.finalized.pluck(:id)
    elsif item == Single
      items_purchased = person.singles.finalized.pluck(:id)
    elsif item == Ringtone
      items_purchased = person.ringtones.finalized.pluck(:id)
    end

    (items_purchased - exclusion).present?
  end

  def event_ids_for_purchase(is_first_purchase)
    event_ids = []
    product_types = associated_product_types
    album_ids = associated_album_ids

    # A generic distribution event for first purchases.
    event_ids << FIRST_DISTRIBUTION_EVENT_ID if is_first_purchase && !credit_purchases?

    if product_types.include?("Album")
      event_ids << if distributed_item_before?(Album, album_ids)
                     RETURN_PURCHASE_ALBUM_DISTRIBUTION_EVENT
                   else
                     FIRST_PURCHASE_ALBUM_DISTRIBUTION_EVENT
                   end
    end

    if product_types.include?("Single")
      event_ids << if distributed_item_before?(Single, album_ids)
                     RETURN_PURCHASE_SINGLE_DISTRIBUTION_EVENT
                   else
                     FIRST_PURCHASE_SINGLE_DISTRIBUTION_EVENT
                   end
    end

    if product_types.include?("Ringtone")
      event_ids << if distributed_item_before?(Ringtone, album_ids)
                     RETURN_PURCHASE_RINGTONE_DISTRIBUTION_EVENT
                   else
                     FIRST_PURCHASE_RINGTONE_DISTRIBUTION_EVENT
                   end
    end

    if credit_purchases?
      event_ids << if purchased_credits_before?
                     RETURN_PURCHASE_DISTRIBUTION_CREDITS_EVENT
                   else
                     FIRST_PURCHASE_DISTRIBUTION_CREDITS_EVENT
                   end
    end

    event_ids
  end

  def pegged_rate
    foreign_exchange_pegged_rate&.pegged_rate
  end

  def invoice_number
    return if self[:invoice_sequence].nil? || invoice_static_corporate_entity.nil?

    format(%(#{invoice_static_corporate_entity.inbound_invoice_prefix}%05d), self[:invoice_sequence])
  end

  def generate_static_content_and_sequence
    return unless settled?

    transaction do
      set_corporate_entity
      create_static_content
      generate_invoice_number
    end
  end

  def generate_invoice_number
    return unless FeatureFlipper.show_feature?(:vat_tax, person)

    return if invoice_sequence.present? || all_free_plan_purchases?

    GenerateInboundInvoiceSequenceWorker.perform_async(id)
  end

  def self.last_generated_invoice_number(corporate_entity)
    where(corporate_entity: corporate_entity)
      .order(invoice_sequence: :desc)
      .first
        &.[](:invoice_sequence)
        &.to_i
  end

  def customer_type
    purchases.first&.purchase_tax_information&.customer_type&.downcase
  end

  def business_invoice?
    purchases.first&.purchase_tax_information&.business_transaction?
  end

  def other_plan_purchases?
    purchases.plans.any? { |purchase|
      !purchase.active_plan? && purchase.person.historic_plan_purchases.count > 1
    }
  end

  def create_static_content
    person.reload

    build_invoice_static_customer_address(customer_address_attributes).save!
    build_invoice_static_corporate_entity(person.corporate_entity&.attributes&.except("id")).save!
    build_invoice_static_customer_info(customer_info_attributes).save!
  end

  def tax_type
    return Purchase::VAT if invoice_static_corporate_entity&.name == CorporateEntity::BI_LUXEMBOURG_NAME

    purchases.first&.purchase_tax_information&.tax_type
  end

  def vat_applicable?
    tax_type == Purchase::VAT
  end

  def tax_rate
    purchases.first.purchase_tax_information&.tax_rate
  end

  protected

  def detach_purchases
    message = "after destroy, invoices"
    options = {
      invoice: self,
      person: person,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
    Purchase.detach_purchases(self)
  end

  private

  def min_paid_at
    @min_paid_at ||= purchases.min_by(&:paid_at).paid_at
  end

  def set_gst_config_id(settlement)
    return unless FeatureFlipper.show_feature?(:enable_gst, person)

    return set_gst_by_payments_os(settlement) unless FeatureFlipper.show_feature?(:bi_transfer, person)
    return unless needs_gst?

    update(gst_config: person.person_gst_config)
  end

  def set_gst_by_payments_os(settlement)
    # we have to update gst_config twice if paying partially with balance and partially with payments_os card
    # this is because we process balance first, but want to save the gst config of the cc if it's available
    # this will mean potentially duplicate, superfluous updates, but that's the world we live in.
    return unless gst_config.blank? || settlement.settled_by_payments_os?

    if settlement.settled_by_payments_os? && settlement.source.gst_config.present?
      update(gst_config: settlement.source.gst_config)
    else
      update(gst_config: person.person_gst_config)
    end
  end

  def customer_address_attributes
    emoji_stripped_address_attributes
      .merge(country: person.country_name_untranslated,
             zip: person.foreign_postal_code)
  end

  def customer_info_attributes
    {
      name: strip_emoji(person.compliant_contact_full_name),
      person_currency: person.currency,
      customer_type: customer_type,
    }.tap do |h|
      h[:vat_registration_number] = person.vat_registration_number if business_invoice?
      h[:name] = person.compliant_contact_company_name if person.business?
    end
  end

  def set_corporate_entity
    return if corporate_entity.present?

    self.corporate_entity_id = person.corporate_entity_id
  end

  def needs_gst?
    return false if gst_config.present?

    FeatureFlipper.show_feature?(:bi_transfer, person) ? person.india_country_and_domain? : person.india_user?
  end

  def create_debit_transaction_for_vat(vat_tax_adjustment)
    PersonTransaction.create!(
      person: person,
      debit: Tunecore::Numbers.cents_to_decimal(vat_tax_adjustment.amount),
      credit: 0,
      target: person,
      comment: "VAT Posting Reverted"
    )
  end

  def first_distro_person_analytic
    @first_distro_person_analytic ||= person.analytic_by_metric("first_distribution_invoice_id")
  end
end
