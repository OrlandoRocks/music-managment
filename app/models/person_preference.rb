class PersonPreference < ApplicationRecord
  belongs_to :person
  belongs_to :preferred_credit_card,    class_name: "StoredCreditCard"
  belongs_to :preferred_paypal_account, class_name: "StoredPaypalAccount"
  belongs_to :preferred_adyen_payment_method, class_name: "AdyenStoredPaymentMethod"

  validates :person_id, presence: true

  # Use validate for a more descriptive error
  validate :must_have_stored_paypal_account, if: :pay_by_paypal?
  validate :must_have_cc,                    if: :pay_by_credit_card?
  validate :must_have_adyen_stored_payment_method, if: :pay_by_adyen?

  validate :cc_must_not_be_expired, if: :pay_by_credit_card?
  validate :must_have_another_payment_method_if_not_using_tunecore_balance
  validate :preferred_accounts_must_belong_to_person

  #
  # Called when a stored credit card is deleted
  #
  def delete_stored_credit_card_proc!(stored_credit_card, ip_address, person, options = {})
    options[:skip_validation] ||= false

    # Update preferred credit card
    if preferred_credit_card.nil? || preferred_credit_card.id == stored_credit_card.id
      self.preferred_credit_card = self.person.stored_credit_cards.active.not_expired.find_by("id != ?", stored_credit_card.id)
      Note.create(
        related: self.person,
        note_created_by: person,
        ip_address: ip_address,
        subject: "Payment Preferences Changed",
        note: "Preferred credit card was set to
                  ************#{preferred_credit_card.last_four}"
      ) if preferred_credit_card
    end

    # Is it still nil? Can we change it to paypal?
    if preferred_credit_card.nil? and self.person.stored_paypal_accounts.active.size.positive?
      self.preferred_payment_type = "PayPal"
      self.preferred_paypal_account_id = self.person.stored_paypal_accounts.active.first.id

      Note.create(
        related: self.person,
        note_created_by: person,
        ip_address: ip_address,
        subject: "Payment Preferences Changed",
        note: "Preferred payment method was set to PayPal Account:
                  #{preferred_paypal_account_id}"
      )
    end

    # if we pass in to skip validation or  were deleting an expired card
    if options[:skip_validation] || stored_credit_card.expired?
      save(validate: false)
    else
      save!
    end
  end

  def delete_stored_paypal_account_proc!(_stored_paypal_account_id, person, options = {})
    options[:skip_validation] ||= false

    self.preferred_paypal_account_id = nil

    # If we have credit cards then change the preferred type to credit card and the first card
    unless self.person.stored_credit_cards.active.not_expired.empty?
      self.preferred_payment_type = "CreditCard"
      self.preferred_credit_card  = person.stored_credit_cards.active.not_expired.first
      Note.create(
        related: self.person,
        note_created_by: person,
        ip_address: "N/A",
        subject: "Payment Preferences Changed",
        note: "Preferred payment method was set to credit card
                  ************#{preferred_credit_card.last_four}"
      )
    end

    if options[:skip_validation]
      save(validate: false)
    else
      save!
    end
  end

  private

  def must_have_cc
    return unless preferred_credit_card_id.nil? && pay_by_credit_card?

    errors.add(:base, I18n.t("models.community.you_must_select_a_valid_credit_card_if_you_choose_to_pay_with_credit_card"))
  end

  def must_have_stored_paypal_account
    return unless preferred_paypal_account_id.nil?

    errors.add(:base, I18n.t("models.community.you_must_add_a_paypal_account_if_you_choose_pay_with_paypal"))
  end

  def must_have_adyen_stored_payment_method
    return unless preferred_adyen_payment_method_id.nil? && pay_by_adyen?

    errors.add(:base, I18n.t("models.community.you_must_have_a_valid_adyen_payment_method_if_you_choose_to_pay_with_adyen_payment_method"))
  end

  def cc_must_not_be_expired
    return unless !preferred_credit_card_id.nil? && preferred_credit_card.expired?

    errors.add(
      :base,
      I18n.t("models.community.the_credit_card_you_have_selected_has_expired_please_select_a_valid_credit_card")
    )
  end

  def must_have_another_payment_method_if_not_using_tunecore_balance
    return unless no_balance? &&
                  preferred_credit_card_id.nil? &&
                  preferred_paypal_account_id.nil? &&
                  preferred_adyen_payment_method_id.nil?

    errors.add(
      :base,
      I18n.t("models.community.if_you_do_not_want_to_pay_with_your_tunecore_balance_please_add_another_payment_method")
    )
  end

  def balance?
    pay_with_balance == 1
  end

  def no_balance?
    pay_with_balance != 1
  end

  def pay_by_credit_card?
    preferred_payment_type == "CreditCard"
  end

  def pay_by_paypal?
    preferred_payment_type == "PayPal"
  end

  def pay_by_adyen?
    preferred_payment_type == "Adyen"
  end

  def preferred_accounts_must_belong_to_person
    errors.add(:preferred_credit_card) if preferred_credit_card and preferred_credit_card.person_id != person_id

    return unless preferred_paypal_account and preferred_paypal_account.person_id != person_id

    errors.add(:preferred_paypal_account)
  end
end
