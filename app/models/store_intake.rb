# == Schema Information
# Schema version: 404
#
# Table name: store_intakes
#
#  id                  :integer(11)     not null, primary key
#  ruby_type           :string(20)      default(""), not null
#  store_id            :integer(11)     default(0), not null
#  reporting_month_id  :integer(11)     default(0), not null
#  local_currency      :string(3)       default(""), not null
#  local_total_cents   :integer(11)     default(0), not null
#  exchange_symbol     :string(6)
#  exchange_rate_fixed :integer(19)
#  exchange_rate_scale :integer(11)
#  usd_actual_cents    :integer(11)     default(0), not null
#  usd_total_cents     :integer(11)     default(0), not null
#  usd_payout_cents    :integer(11)     default(0), not null
#  comment             :text
#  unique_id           :string(30)
#

class StoreIntake < ApplicationRecord
  self.inheritance_column = "ruby_type"

  belongs_to  :reporting_month
  belongs_to  :store
  has_many    :person_intakes, before_add: [:adding_person_intake]

  validates :unique_id, uniqueness: true

  validates_associated :person_intakes
  validates_each :usd_payout_cents do |record, attr_name, value|
    if record.usd_actual_cents < value
      record.errors.add(attr_name, I18n.t("models.royalties.payout_exceeds_actual_deposit", value: value, usd_actual_cents: record.usd_actual_cents, store_name: record.store.short_name, local_currency: record.local_currency))
    end

    if (record.usd_actual_cents - value) > maximum_payout_discrepancy(
      record.local_total_cents,
      record.exchange_symbol,
      record.exchange_rate,
      record.person_intakes.length
    )

      record.errors.add(attr_name, I18n.t("models.royalties.payout_discrepancy", value: value, usd_actual_cents: record.usd_actual_cents, store_name: record.store.short_name, local_currency: record.local_currency))
    end
  end

  def self.class_for(store_short_name)
    return StoreIntakeItunesAu if ["iTunesAU", "iTunesNZ"].include?(store_short_name)

    StoreIntakeDefault
  end

  def self.calculate_usd_cents(lcents, exchange_symbol, exchange_rate)
    case exchange_symbol
    when nil
      lcents
    when /USD.../
      (LongDecimal(lcents) / exchange_rate).round_to_scale(0, LongDecimal::ROUND_DOWN).int_val
    when /...USD/
      (LongDecimal(lcents) * exchange_rate).round_to_scale(0, LongDecimal::ROUND_DOWN).int_val
    end
  end

  def self.calculate_payout_cents(lcents, exchange_symbol, exchange_rate)
    calculate_usd_cents(lcents, exchange_symbol, exchange_rate)
  end

  # return the maximum discrepancy permitted between an actual deposit and the
  # sum of all payouts.
  def self.maximum_payout_discrepancy(_lcents, exchange_symbol, _exchange_rate, num_ops)
    return 0 if exchange_symbol.nil? # it's USD all the way, no tolerance

    num_ops
  end

  def payout_computed(pcents)
    self.usd_payout_cents += pcents
  end

  def calculate_usd_cents(lcents)
    self.class.calculate_usd_cents(lcents, exchange_symbol, exchange_rate)
  end

  def calculate_payout_cents(lcents)
    self.class.calculate_payout_cents(lcents, exchange_symbol, exchange_rate)
  end

  def exchange_rate=(rate)
    if rate.nil?
      self.attributes = { exchange_rate_fixed: nil, exchange_rate_scale: nil }
    else
      rate_ld = LongDecimal(rate)
      self.attributes = { exchange_rate_fixed: rate_ld.int_val, exchange_rate_scale: rate_ld.scale }
    end
  end

  def exchange_rate
    return if exchange_rate_fixed.blank? || exchange_rate_scale.blank?

    LongDecimal(exchange_rate_fixed, exchange_rate_scale)
  end

  def adding_person_intake(person_intake)
    person_intake.attributes = {
      store: store,
      reporting_month: reporting_month
    }
  end

  def has_multiple_exchange_rates?
    false
  end

  def payout_comment
    nil
  end
end
