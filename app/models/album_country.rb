class AlbumCountry < ApplicationRecord
  validates :relation_type, inclusion: { in: ["include", "exclude"] }
  validate :cannot_include_sanctioned_countries
  belongs_to :album
  belongs_to :country

  after_commit :invalidate_cache

  def invalidate_cache
    CrtCache::Invalidator.invalidate_cache_if_changed(self)
  end

  def cannot_include_sanctioned_countries
    iso_code = Country.unscoped.find(country_id).iso_code
    return unless relation_type == "include" && Country.unscoped.sanctioned_iso_codes.include?(iso_code)

    errors.add(:country, I18n.t("models.album_country.sanctioned"))
  end
end
