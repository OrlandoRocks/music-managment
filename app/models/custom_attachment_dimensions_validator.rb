class CustomAttachmentDimensionsValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.queued_for_write[:original]

    begin
      dimensions = Paperclip::Geometry.from_file(value.queued_for_write[:original].path)
      validate_attachment_dimensions(record, attribute, dimensions)
      validate_attachment_shape(record, attribute, dimensions)
    rescue Paperclip::Errors::NotIdentifiedByImageMagickError
      Paperclip.log("cannot validate dimensions on #{attribute}")
    end
  end

  def validate_attachment_dimensions(record, attribute, dimensions)
    error_messages = []
    error_messages << dimensions_error_to_small unless valid_dimension_min?(dimensions)
    error_messages << dimensions_error_to_large unless valid_dimension_max?(dimensions)
    record.errors.add(attribute.to_sym, error_messages.join(" ")) if error_messages.any?
  end

  def validate_attachment_shape(record, attribute, dimensions)
    record.errors.add(attribute.to_sym, dimensions_error_square) unless valid_square?(dimensions)
  end

  def valid_dimension_min?(dimensions)
    shorter_side = [dimensions.height, dimensions.width].min
    shorter_side >= min_width
  end

  def valid_dimension_max?(dimensions)
    longer_side = [dimensions.height, dimensions.width].max
    longer_side <= max_width
  end

  def valid_square?(dimensions)
    (shape == "square") && (dimensions.height == dimensions.width)
  end

  def min_width
    options.dig(:dimensions, :min_width).to_f
  end

  def max_width
    options.dig(:dimensions, :max_width).to_f
  end

  def shape
    options[:shape].presence || "square"
  end

  def dimensions_error_to_small
    I18n.t(
      "model.artwork.dimensions_error_too_small",
      min_width: min_width,
      min_height: min_width
    )
  end

  def dimensions_error_to_large
    I18n.t(
      "model.artwork.dimensions_error_too_large",
      max_width: max_width,
      max_height: max_width
    )
  end

  def dimensions_error_square
    I18n.t("model.artwork.dimensions_error_square")
  end
end
