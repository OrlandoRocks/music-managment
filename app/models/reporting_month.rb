# == Schema Information
# Schema version: 404
#
# Table name: reporting_months
#
#  id           :integer(11)     not null, primary key
#  report_year  :integer(11)     default(0), not null
#  report_month :integer(11)     default(0), not null
#  report_date  :date            not null
#

class ReportingMonth < ApplicationRecord
  has_many :store_intakes
  has_many :person_intakes
  has_many :album_intakes

  validates :report_date, uniqueness: true
  validates_associated :store_intakes

  scope :latest, -> { order "report_date desc" }

  scope :between_dates,
        ->(start_date, end_date) {
          where("DATE(report_date) BETWEEN DATE(?) and DATE(?)", start_date, end_date)
        }

  def self.find_or_create_for_date(date)
    sanitized = Date.civil(date.year, date.month, 1)
    rval = ReportingMonth.find_by(report_date: sanitized)
    if rval.nil?
      rval = ReportingMonth.create(
        report_date: date,
        report_month: date.month,
        report_year: date.year
      )
    end
    rval
  end

  def self.all_years(person)
    self
      .select("reporting_months.report_year")
      .joins("left join person_intakes on person_intakes.reporting_month_id_temp = reporting_months.id")
      .where("person_intakes.person_id = ?", person.id)
      .group("reporting_months.report_year")
      .order("report_year ASC")
      .collect(&:report_year)
  end

  def self.all_for_person(person)
    self
      .select("distinct reporting_months.*")
      .joins("left join person_intakes on person_intakes.reporting_month_id_temp = reporting_months.id")
      .where("person_intakes.person_id = ?", person.id)
      .order("report_date ASC")
  end

  def self.all_for_year(person, year = nil)
    year ||= Date.today.year
    self
      .select("reporting_months.*")
      .joins("left join person_intakes on person_intakes.reporting_month_id_temp = reporting_months.id")
      .where("person_intakes.person_id = ? and reporting_months.report_year = ?", person.id, year)
      .group("reporting_months.id")
      .order("report_date DESC")
  end

  def in_english
    report_date.strftime("%B %Y")
  end

  def short_form
    report_date.strftime("%Y-%m")
  end

  def filename_fragment
    report_date.strftime("%Y%m")
  end
end
