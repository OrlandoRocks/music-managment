class CopyrightClaim < ApplicationRecord
  belongs_to :person
  belongs_to :admin, class_name: "Person"
  belongs_to :store
  belongs_to :asset, polymorphic: true
  has_many :notes, -> { order "id DESC" }, as: :related
  has_many :copyright_claimants, dependent: :destroy
  scope :active, -> { where(is_active: true) }

  scope :search_by_internal_claim_id,
        ->(internal_claim_id) {
          where(
            "copyright_claims.is_active = true and lower(copyright_claims.internal_claim_id) LIKE ?",
            "%#{internal_claim_id.downcase}%"
          )
        }

  scope :search_by_account,
        ->(account) {
          joins(:person)
            .where(
              "copyright_claims.is_active = true and lower(people.name) LIKE ?",
              "%#{account.downcase}%"
            )
        }
  scope :search_by_admin,
        ->(admin) {
          joins(:admin)
            .where(
              "copyright_claims.is_active = true and lower(people.name) LIKE ?",
              "%#{admin.downcase}%"
            )
        }
  scope :search_by_store,
        ->(name) {
          joins(:store).where(is_active: true, stores: { name: name })
        }

  attr_accessor :compute_hold_amount

  validate :validate_song_asset, on: :create

  after_create :deactivate_related_claims!, if: :related_claims?
  after_commit :calculate_hold_amount!, if: :compute_hold_amount

  def deactivate!
    update(is_active: false)
  end

  def calculate_hold_amount!
    return if saved_change_to_hold_amount?

    asset_hold_amount =
      Float(
        LifetimeEarningsFetcher.fetch(
          asset_id,
          asset_type
        ).first[:lifetime_earnings]
      )
    return if asset_hold_amount == hold_amount

    update(hold_amount: asset_hold_amount)
  end

  def deactivate_related_claims!
    related_claims.map(&:deactivate!)
  end

  def self.search_by_asset(asset)
    return [] if asset.is_a? String

    asset.copyright_claims.active
  end

  private

  def validate_song_asset
    errors.add(
      :base,
      "Claim cannot be processed - Already held at release"
    ) if asset.instance_of?(Song) && CopyrightClaim.active.exists?(asset: asset.album)
  end

  def related_claims
    related_claims = CopyrightClaim.active.where(asset: asset).where.not(id: id)
    related_claims += CopyrightClaim.active.where(asset: asset.songs) if asset.is_a?(Album)

    related_claims
  end

  def related_claims?
    related_claims.present?
  end
end
