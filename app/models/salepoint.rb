# frozen_string_literal: true

class Salepoint < ApplicationRecord
  include Tunecore::Salepoints::VariablePricing
  class DestroyAfterUsed < StandardError
  end

  ALBUM_SALEPOINTABLE_TYPE = "Album"

  belongs_to  :salepointable, polymorphic: true
  belongs_to  :album,
              -> { where(salepoints: { salepointable_type: ALBUM_SALEPOINTABLE_TYPE }) },
              foreign_key: "salepointable_id"
  belongs_to  :variable_price
  belongs_to  :store
  has_many    :inventory_usages, as: :related
  has_one     :purchase, as: :related
  has_one     :salepoint_preorder_data, dependent: :destroy
  has_many    :salepoint_songs, dependent: :destroy

  has_many :distributions_salepoints, class_name: "::DistributionSalepoint"
  has_many :distributions, through: :distributions_salepoints, class_name: "::Distribution" do
    def latest
      order(id: :desc).limit(1).first
    end
  end

  has_many :invoice_logs, primary_key: "id", foreign_key: "purchase_salepoint_id", class_name: "InvoiceLog"

  scope :active, -> { includes(:store).where(stores: { is_active: true }) }
  scope :inactive, -> { includes(:store).where(stores: { is_active: false }) }
  scope :is_used, -> { includes(:store).where(stores: { in_use_flag: true }) }
  scope :facebook_reels, -> { where(store_id: Store::FB_REELS_STORE_ID) }
  scope :exclude_believe, -> { where.not(store_id: Store::BELIEVE_STORE_IDS) }
  scope :discovery, ->(person) { where(store: Store.discovery_platforms(person)) }
  scope :exclude_discovery, ->(person) { where(store: Store.exclude_discovery(person)) }
  scope :exclude_ytsr_proxy, -> { where.not(store_id: Store::YTSR_PROXY_ID) }
  scope :payment_applied, -> { where(payment_applied: true) }
  scope :has_store_delivery_config, -> { joins("INNER JOIN store_delivery_configs ON store_delivery_configs.store_id = salepoints.store_id") }

  validates :store_id, uniqueness: { scope: :salepointable_id }
  before_validation :set_implicit_default_variable_price
  before_validation :set_implicit_track_variable_price

  validate :store_is_within_salepointable_stores
  validate :validate_extra_rights_confirmation
  validate :validate_variable_price_selection
  validate :artwork_size_for_amazon_on_demand
  validate :valid_variable_price

  before_update :save_changes
  after_destroy :remove_related_purchase
  after_destroy(proc { |sp| log_google_destroyer(sp) if sp.store_id == 28 })
  after_commit :allow_salepoint_in_believe_api

  belongs_to :track_variable_price, class_name: "VariablePrice"

  attr_accessor :previously_changed

  def self.create_batch_salepoints(sp_store, salepointables)
    sp_values = []
    salepointable_ids = []

    related_type = related_type = salepointables.first.class.base_class.name if salepointables.present?
    variable_price_id = sp_store.default_variable_price.present? ? sp_store.default_variable_price.id : (Store.get_implicit_default_variable_price(sp_store, related_type).try(:id) || "NULL")
    track_variable_price_id = (Store.get_implicit_track_variable_price(sp_store).try(:id) || "NULL")

    salepointables.each do |salepointable|
      if salepointable.salepoints_by_store(sp_store).blank?
        sp_values << "(#{sp_store.id}, 1, #{variable_price_id}, #{salepointable.id}, '#{related_type}', 'new', #{track_variable_price_id})"
      end
      salepointable_ids << salepointable.id
    end

    if sp_values.present?
      sql = "INSERT INTO salepoints (`store_id`, `has_rights_assignment`, `variable_price_id`, `salepointable_id`, `salepointable_type`, `status`, `track_variable_price_id`) VALUES #{sp_values.join(', ')}"
      ActiveRecord::Base.connection.execute sql
    end

    salepointable_ids
  end

  def self.create_with_default_store_var(store)
    return unless store

    salepoint_attrs = { store: store }
    salepoint_attrs[:has_rights_assignment] = 1 if store.needs_rights_assignment?
    salepoint_attrs[:variable_price_id]     = store.default_variable_price.id if store.variable_pricing_required?
    create(salepoint_attrs)
  end

  def self.add_youtubesr_store(album, ytm_purchase)
    ytsr_store = Store.find_by(abbrev: "ytsr")
    sr_salepoints, error_stores = album.add_stores([ytsr_store])

    if sr_salepoints.length == 1 && error_stores.length.zero?
      options = { item_to_use: sr_salepoints.first, purchase: ytm_purchase }
      Inventory.use!(album.person, options)
      sr_salepoints.first
    else
      false
    end
  end

  def self.salepoints_to_add_to_cart(album)
    album
      .salepoints
      .includes(:inventory_usages, :store)
      .select { |sp| !sp.payment_applied? && sp.store.is_active }
  end

  def self.send_sns_notification(album_store_id_hash)
    return if ENV.fetch("SNS_RELEASE_NEW_TOPIC", "").blank?

    album_store_id_hash.each do |album_id, store_ids|
      album = Album.find(album_id)
      next unless album.approved_for_distribution?

      Sns::Notifier.perform(
        topic_arn: ENV.fetch("SNS_RELEASE_NEW_TOPIC"),
        album_ids_or_upcs: [album_id],
        store_ids: store_ids.presence || album.salepoints.pluck(:store_id),
        delivery_type: store_ids.blank? ? "none" : "full_delivery"
      )
    end
  end

  def delivered_by_tc_www?
    !!distributions&.map(&:has_been_delivered_by_tc_www?)&.any?
  end

  def finalize
    return unless payment_applied?

    if is_itunes_salepoint?
      create_apple_artists
      verify_apple_curated_artist if FeatureFlipper.show_feature?(:curated_artists_flag, salepointable.person)
    end

    monetize_tracks

    update_attribute(:finalized_at, Time.now)
  end

  def unfinalize
    update_attribute(:finalized_at, nil)
  end

  def finalized?
    finalized_at.present?
  end

  def save_changes
    @previously_changed = changes
  end

  delegate :name, to: :store, prefix: true

  def payment_applied?
    payment_applied
  end

  def monetize_tracks
    monetize_yt_tracks if post_purchase_ytsr_proxy_salepoint?
    monetize_fb_tracks if post_purchase_fb_salepoint?
  end

  def post_purchase_ytsr_proxy_salepoint?
    # returns true if store_id 105 is added to release after the initial album purchase is made
    ytsr_proxy_salepoint? && salepointable.approved? && !taken_down?
  end

  def post_purchase_fb_salepoint?
    fb_salepoint? && salepointable.approved? && !taken_down?
  end

  def is_disabled?
    !!(payment_applied? || finalized?)
  end

  def destroy
    raise DestroyAfterUsed, "Attempt to destroy a used salepoint #{inspect}" if Inventory.used?(self)

    super unless payment_applied
  end

  def monetize_yt_tracks
    TrackMonetization::YoutubePostApprovalWorker.perform_async(salepointable.id, Store::YTSR_STORE_ID)
  end

  def monetize_fb_tracks
    TrackMonetization::FbPostApprovalWorker.perform_async(salepointable.id, Store::FBTRACKS_STORE_ID)
  end

  # raise an error if anthing other than amazon on demand tries to set a template id
  def template_id=(value)
    raise "templates cannot be set for stores that don't require a template" unless requires_template?

    self[:template_id] = value
  end

  # finds stores that need a template
  def requires_template?
    store.abbrev == "aod_us"
  end

  def has_chosen_template?
    return true unless requires_template?

    template_id.present?
  end

  # this is currently a stub method that says "no" if the salepoint has been finalized.
  # Soon we are going to be allowing metadata updates after the salepoint has been paid for
  def can_update_metadata?
    !finalized?
  end

  # legacy album_id support
  def album_id
    salepointable_id if salepointable_type == "Album"
  end

  def taken_down?
    !!takedown_at
  end

  def takedown!(album_takedown = false)
    update_attribute(:takedown_at, Time.now)
    salepointable.try(:ensure_takedown_genre) unless album_takedown
    salepoint_songs.each { |ss| ss.update_attribute(:takedown_at, Time.now) } if salepoint_songs.present?

    if store.short_name.exclude?("iTunes") || (store.short_name.include?("iTunes") && !album_takedown)
      send_metadata_update("issuing a takedown distribution")
    end

    takedown_tracks if Store::TRACK_MONETIZATION_STORE_MAP[store_id]
    remove_related_purchase
    true
  end

  def takedown_tracks
    monetizations = TrackMonetization.joins(:song)
                                     .where(songs: { album_id: salepointable.id },
                                            store_id: Store::TRACK_MONETIZATION_STORE_MAP[store_id],
                                            takedown_at: nil)

    return unless monetizations.any?

    takedown_params = {
      takedown_source: "#{store.name} album level takedown",
      track_mons: monetizations
    }.with_indifferent_access

    TrackMonetization::TakedownService.takedown(takedown_params)
  end

  def remove_takedown
    update_attribute(:takedown_at, nil)

    redeliver_untakedown if store.short_name.exclude?("iTunes")
  end

  def has_delivered_distribution?
    distributions.latest.try(:has_been_delivered?)
  end

  def blocked?
    distributions.blank? ? false : distributions.latest.blocked?
  end

  def block!(opts)
    if has_delivered_distribution? || (distributions.blank? ? true : !distributions.latest.new?)
      false
    else
      distributions.latest.block(opts)
      true
    end
  end

  def unblock!(opts)
    if has_delivered_distribution? || distributions.blank? || !distributions.latest.blocked?
      false
    else
      distributions.latest.unblock(opts)
      true
    end
  end

  def send_metadata_update(message = "")
    return false unless store.in_use? &&
                        distributions.latest.try(:supports_takedown?) &&
                        (taken_down? || has_delivered_distribution?)

    distributions.latest.update(delivery_type: "metadata_only", state: "new")
    distributions.latest.retry(actor: "Salepoint#send_metadata_update", message: message)
    true
  end

  def redeliver_untakedown
    previous_delivery = distributions.latest
    return false unless store.in_use? &&
                        previous_delivery.try(:supports_takedown?)

    previous_delivery.update(delivery_type: "full_delivery", state: "new")
    previous_delivery.retry(actor: "Salepoint#redeliver_untakedown", message: "issuing a reactivation distribution")
    true
  end

  def preorder_data
    unless salepoint_preorder_data &&
           salepoint_preorder_data.is_enabled? &&
           salepoint_preorder_data.preorder_purchase.paid_at?

      return
    end

    salepoint_preorder_data.preorder_data
  end

  def set_variable_price(price)
    variable_prices_store = store.variable_prices_stores.joins(:variable_price).where("variable_prices.price = ?", price).first

    update(variable_price_id: variable_prices_store.variable_price_id)
  end

  def unpurchasable_salepoint?
    Store.not_active.where(id: store_id).present?
  end

  def setup_distribution
    update_post_purchase unless payment_applied
    PetriBundle.for(salepointable).add_salepoints([self], actor: "Salepoint#setup_distribution")
  end

  def paid_post_proc
    update_post_purchase
    create_audit_for_reviewable_stores
  end

  def update_post_purchase
    update_attribute(:payment_applied, true)
    finalize if salepointable.finalized?
  end

  def create_audit_for_reviewable_stores
    return unless store.reviewable && salepointable_type == "Album"

    salepointable.legal_review_state = "NEEDS REVIEW"
    Rails.logger.info("Marking album as 'NEEDS REVIEW' because the store was flagged as reviewable album_id=#{album_id}")
    salepointable.save
  end

  def ytsr_proxy_salepoint?
    Store::YTSR_PROXY_ID == store_id
  end

  def fb_salepoint?
    Store::FB_REELS_STORE_ID == store_id
  end

  def self.get_missing_salepoint_query
    <<~SQL.squish
      SELECT
        `salepoints`.id
      FROM
        `salepoints`
      WHERE
        `salepoints`.`store_id` = :source_store_id
        AND `salepoints`.`salepointable_type` = 'Album'
        AND `salepoints`.`salepointable_id` NOT IN (
          SELECT
            sp.salepointable_id
          FROM
            salepoints sp
          WHERE
            sp.store_id = :destination_store_id
            AND sp.salepointable_type = 'Album'
        )
    SQL
  end

  def send_sns_notification_takedown(user)
    return unless FeatureFlipper.show_feature?(:use_sns, user) &&
                  ENV.fetch("SNS_RELEASE_TAKEDOWN_URGENT_TOPIC", nil).present?

    Sns::Notifier.perform(
      topic_arn: ENV.fetch("SNS_RELEASE_TAKEDOWN_URGENT_TOPIC"),
      album_ids_or_upcs: [salepointable_id],
      store_ids: [store_id],
      delivery_type: "takedown",
      person_id: user.id
    )
  end

  protected

  #
  #  Make sure that the salepoint's store is within the list of
  #  available stores for the salepointable object
  #
  def store_is_within_salepointable_stores
    return false unless salepointable

    return if salepointable.available_store?(store)

    errors.add(:store, I18n.t("model.salepoint.store_error", salepoint: salepointable_type.pluralize).to_s)
  end

  def validate_extra_rights_confirmation
    return unless store.needs_rights_assignment?

    return if has_rights_assignment?

    errors.add :distribution, I18n.t("model.salepoint.extra_rights_confirmation_error", store_name: store.name).to_s
  end

  def artwork_size_for_amazon_on_demand
    return if store.abbreviation != "aod_us" || album_id.blank?

    album = salepointable
    return unless album.has_artwork? && !album.artwork.high_quality? && !finalized?

    errors.add :distribution, I18n.t("model.salepoint.amazon_on_demand_error", store_name: store.name).to_s
  end

  private

  def remove_related_purchase
    Purchase.destroy_by_purchase_item(salepointable.person, self)
  end

  def allow_salepoint_in_believe_api
    Salepoints::AfterCommitAllowSalepointInBelieveApi.call(self)
  end

  def verify_apple_curated_artist
    Apple::CuratedArtistServiceWorker.perform_async(salepointable.id)
  end

  def create_apple_artists
    Apple::CreateAppleArtistsServiceWorker.perform_async(salepointable.id)
  end

  def is_itunes_salepoint?
    Store::ITUNES_STORE_IDS.include? store_id
  end

  def log_google_destroyer(sp)
    state = finalized_at.present? ? "Finalized" : "Unfinalized"
    Rails.logger.info "#{state} Google Salepoint destroyed"
    Rails.logger.info sp.inspect
    Rails.logger.info caller.select { |line| line.include?("tc-www") }.join("\n")
  end
end
