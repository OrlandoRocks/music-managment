class DistributionBatch < ApplicationRecord
  has_many :distribution_batch_items
  has_many :distributions, through: :distribution_batch_items

  before_create :set_batch_id
  validates :count, numericality: { less_than_or_equal_to: 100 }, presence: true

  MAX_BATCH_SIZE = 100

  def increment_count
    update(count: count + 1)
    deactivate if batch_full?
    true
  end

  def decrement_count
    update(count: count - 1) if count.positive?
    activate if !batch_full? && !active
    true
  end

  def deactivate
    update(active: false)
  end

  def activate
    update(active: true)
  end

  def batch_full?
    max = ENV["DIST_BATCH"] || MAX_BATCH_SIZE
    count >= max.to_i
  end

  def set_batch_id
    self.batch_id = timestamp
  end

  def timestamp
    Time.now.strftime("%Y%m%d%H%M%S%L")
  end
end
