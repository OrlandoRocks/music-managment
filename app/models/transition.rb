class Transition < ApplicationRecord
  belongs_to :state_machine, polymorphic: true
end
