# frozen_string_literal: true

class FlagReason < ApplicationRecord
  belongs_to :flag

  has_many :people_flags, dependent: :nullify
  has_many :flag_transitions, dependent: :nullify

  # withdrawal
  INCORRECT_TAX_FORM = "incorrect_tax_form_submission"
  SELF_IDENTIFIED_ADDRESS_LOCK = "self_identified_address_lock"

  # address_locked
  PAYONEER_KYC_COMPLETED = "payoneer_kyc_completed"
  SELF_IDENTIFIED_ADDRESS_LOCKED = "self_identified_address_locked"

  # suspicious
  SIFT_IDENTIFIED = "Sift Identified"

  scope :suspicious_flag_reasons, -> { where(flag: Flag.suspicious_flag) }
  scope :address_lock_flag_reasons, -> {
    where(
      flag: Flag.find_by(Flag::ADDRESS_LOCKED)
    )
  }

  def self.payoneer_kyc_completed_address_lock
    address_lock_flag_reasons.find_by(reason: PAYONEER_KYC_COMPLETED)
  end

  def self.sift_identified_suspicious
    find_by(reason: SIFT_IDENTIFIED)
  end
end
