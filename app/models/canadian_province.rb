#
#  CanadianProvince
#
#  Data Abstraction of the Canadian Provinces.
#  Follows the pattern established by UsStates but does
#  not have data in the database.
#
class CanadianProvince
  attr_accessor :name, :abbreviation

  PROVINCES = [
    { name: "Ontario", abbreviation: "ON" },
    { name: "Quebec", abbreviation: "QC" },
    { name: "Nova Scotia", abbreviation: "NS" },
    { name: "New Brunswick", abbreviation: "NB" },
    { name: "Manitoba", abbreviation: "MB" },
    { name: "British Columbia", abbreviation: "BC" },
    { name: "Prince Edward Island", abbreviation: "PE" },
    { name: "Saskatchewan", abbreviation: "SK" },
    { name: "Alberta", abbreviation: "AB" },
    { name: "Newfoundland and Labrador", abbreviation: "NL" },
    { name: "Northwest Territories", abbreviation: "NT" },
    { name: "Yukon", abbreviation: "YT" },
    { name: "Nunavut", abbreviation: "NU" }
  ]

  PROVINCES_ARRAY = [
    ["ALBERTA", "AB"],
    ["BRITISH COLUMBIA", "BC"],
    ["MANITOBA", "MB"],
    ["NEW BRUNSWICK", "NB"],
    ["NEWFOUNDLAND AND LABRADOR", "NL"],
    ["NORTHWEST TERRITORIES", "NT"],
    ["NOVA SCOTIA", "NS"],
    ["NUNAVUT", "NU"],
    ["ONTARIO", "ON"],
    ["PRINCE EDWARD ISLAND", "PE"],
    ["QUEBEC", "QC"],
    ["SASKATCHEWAN", "SK"],
    ["YUKON", "YT"]
  ]

  def initialize(options = {})
    options ||= {}

    @name = options[:name]
    @abbreviation = options[:abbreviation]
  end

  #
  #  Returns all of the provinces
  #
  def self.all
    PROVINCES.map do |province|
      CanadianProvince.new(province)
    end
  end

  #
  #  Finds a province by name
  #
  def self.find_by_name(name)
    hash = PROVINCES.find { |province| province[:name] == name }
    hash ? CanadianProvince.new(hash) : nil
  end

  #
  #  Finds a province by abbreviation
  #
  def self.find_by_abbreviation(abbreviation)
    hash = PROVINCES.find { |province| province[:abbreviation] == abbreviation }
    hash ? CanadianProvince.new(hash) : nil
  end
end
