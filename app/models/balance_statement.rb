class BalanceStatement
  attr_accessor :start_date, :end_date, :previous_balance, :current_balance, :withdrawals, :deposits
  attr_accessor :other_debits, :other_credits, :pending_paypal_withdrawals, :pending_check_withdrawals

  def self.money_fields
    [
      :previous_balance,
      :withdrawals,
      :deposits,
      :other_debits,
      :other_credits,
      :pending_paypal_withdrawals,
      :pending_check_withdrawals,
      :current_balance
    ]
  end

  def self.generate(start_date, end_date)
    raise "start date must be before end" unless start_date < end_date

    end_date = Date.today - 1 unless end_date < Date.today

    statement = new
    statement.start_date = start_date
    statement.end_date = end_date
    transaction_dates_string = "'#{statement.start_date.to_s :db}' <= DATE(created_at) and DATE(created_at) <= '#{statement.end_date.to_s :db}'"

    PersonTransaction.transaction do
      # sum of all tunecore balances right now
      now_balance = (PersonTransaction.connection.select_value "select sum(balance) from person_balances").to_f

      # difference between now and statement date
      offset_cents = (PersonTransaction.connection.select_value "select sum((debit) + (-1 * credit)) from person_transactions WHERE created_at > '#{end_date.to_s :db}'" || 0).to_f

      # sum of all tunecore balances at the end of this statement
      current_balance = now_balance + offset_cents

      # difference between statement date and start of period
      period_offset_cents = (PersonTransaction.connection.select_value "select sum((debit) + (-1 * credit)) from person_transactions WHERE #{transaction_dates_string}" || 0).to_f

      # sum of all tunecore balances at the beginning of this statement
      previous_balance = current_balance + period_offset_cents

      # sums of different types of activity over the period of this statement
      withdrawal_cents = (PersonTransaction.connection.select_value "select sum(debit) from person_transactions WHERE #{transaction_dates_string} AND target_type in ('CheckTransfer','PaypalTransfer')" || 0).to_f
      deposit_cents = (PersonTransaction.connection.select_value "select sum(credit) from person_transactions WHERE #{transaction_dates_string} AND target_type in ('PersonIntake')" || 0).to_f
      other_debit_cents = (PersonTransaction.connection.select_value "select sum(debit) from person_transactions WHERE #{transaction_dates_string} AND (target_type is null or target_type not in ('CheckTransfer','PaypalTransfer','PersonIntake'))" || 0).to_f
      other_credit_cents = (PersonTransaction.connection.select_value "select sum(credit) from person_transactions WHERE #{transaction_dates_string} AND (target_type is null or target_type not in ('CheckTransfer','PaypalTransfer','PersonIntake'))" || 0).to_f

      # outstanding withdrawals from this statement period
      # NOTE: the value of these withdrawals is removed from the customer's person balance as soon as they request it, the funds are sent later
      pending_paypal_cents = (PersonTransaction.connection.select_value "select sum(payment_cents) from paypal_transfers WHERE #{transaction_dates_string} AND transfer_status in ('processing','pending')" || 0).to_f
      pending_check_cents = (PersonTransaction.connection.select_value "select sum(payment_cents) from check_transfers WHERE #{transaction_dates_string} AND transfer_status in ('processing','pending')" || 0).to_f

      # not the prettiest code in the world
      statement.previous_balance = previous_balance / 100
      statement.current_balance = current_balance / 100
      statement.withdrawals = withdrawal_cents / 100
      statement.deposits = deposit_cents / 100
      statement.other_debits = other_debit_cents / 100
      statement.other_credits = other_credit_cents / 100
      statement.pending_paypal_withdrawals = pending_paypal_cents / 100
      statement.pending_check_withdrawals = pending_check_cents / 100
    end

    statement
  end
end
