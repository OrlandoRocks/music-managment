class Country < ApplicationRecord
  include Translatable

  # SANCTIONED_COUNTRIES only exists for backward compatibility
  # with old migrations. Use `sanctioned` scope on this model, for new development.
  SANCTIONED_COUNTRIES = [
    "BI",
    "BY",
    "CD",
    "CF",
    "CU",
    "IQ",
    "IR",
    "KP",
    "LY",
    "MM",
    "RS",
    "SD",
    "SO",
    "SS",
    "SY",
    "YE",
    "ZW"
  ].map(&:freeze).freeze

  CURRENT_SANCTIONED_COUNTRIES_ISO_CODES = Country
                                           .unscoped
                                           .where(is_sanctioned: true)
                                           .pluck(:iso_code)
                                           .map(&:freeze)
                                           .freeze

  US_ONLY              = ["US"].freeze

  GB_ONLY              = ["GB"].freeze

  EU_GB_ONLY           = [
    "BE",
    "DK",
    "DE",
    "ES",
    "FI",
    "FR",
    "GR",
    "IE",
    "IT",
    "LU",
    "NL",
    "NO",
    "AT",
    "PT",
    "CH",
    "SE",
    "GB",
    "BG",
    "CY",
    "CZ",
    "EE",
    "HU",
    "LV",
    "LT",
    "MT",
    "PL",
    "RO",
    "SK",
    "SI"
  ].freeze

  NON_EU_GB_ONLY = [
    "AL",
    "AD",
    "MC",
    "RS",
    "BE",
    "HR",
    "DK",
    "DE",
    "ES",
    "FI",
    "FR",
    "GR",
    "IE",
    "IT",
    "LU",
    "NL",
    "NO",
    "AT",
    "PT",
    "CH",
    "SE",
    "GB",
    "BG",
    "CY",
    "CZ",
    "EE",
    "HU",
    "LV",
    "LT",
    "MT",
    "PL",
    "RO",
    "SK",
    "SI",
    "IS",
    "LI",
    "MK",
    "MD",
    "ME",
    "BY",
    "VA",
    "JE"
  ].freeze

  EU_COUNTRIES = Set["AT",
                     "BE",
                     "BG",
                     "HR",
                     "CY",
                     "CZ",
                     "DK",
                     "EE",
                     "FI",
                     "FR",
                     "DE",
                     "GR",
                     "HU",
                     "IE",
                     "IT",
                     "LV",
                     "LT",
                     "LU",
                     "MT",
                     "NL",
                     "PL",
                     "PT",
                     "RO",
                     "SK",
                     "SI",
                     "ES",
                     "SE"].freeze

  EEA_COUNTRIES = Set["AT",
                      "BE",
                      "BG",
                      "HR",
                      "CY",
                      "CZ",
                      "DK",
                      "EE",
                      "FI",
                      "FR",
                      "DE",
                      "GB",
                      "GR",
                      "HU",
                      "IE",
                      "IS",
                      "IT",
                      "LI",
                      "LV",
                      "LT",
                      "LU",
                      "MT",
                      "NL",
                      "NO",
                      "PL",
                      "PT",
                      "RO",
                      "SK",
                      "SI",
                      "ES",
                      "SE"].freeze

  INDIA_ISO = "IN".freeze
  INDIA_NAME = "India".freeze
  INDIA_NAME_AND_ISO = [INDIA_ISO.downcase.freeze, INDIA_NAME.downcase.freeze].freeze

  LUXEMBOURG_ISO = "LU".freeze

  UNITED_STATES_AND_TERRITORIES = [
    "US",
    "AS",
    "GU",
    "PR",
    "UM",
    "VI"
  ].map(&:freeze).freeze
  # TODO probably don't need this after GS-8084 is implemented
  UNITED_STATES_AND_TERRITORIES_COUNTRY_NAMES = [
    "United States",
    "Guam",
    "Puerto Rico",
    "American Samoa",
    "United States Minor Outlying Islands",
    "Virgin Islands, U.S.",
    "Virgin Islands, United States"
  ].map(&:freeze).freeze

  COUNTRIES_MAP = {
    "XK" => "RS"
  }

  SOUTH_EAST_ASIAN_COUNTRY_CURRENCY_MAP = {
    "ID" => "IDR".freeze,
    "MY" => "MYR".freeze,
    "PH" => "PHP".freeze,
    "TH" => "THB".freeze,
    "VN" => "VND".freeze
  }.freeze

  NATIVE_CURRENCY_MAP = {
    # "COUNTRY_ISO_CODE" => "CURRENCY_ISO_CODE"
    "NL" => "EUR".freeze
  }.merge(
    SOUTH_EAST_ASIAN_COUNTRY_CURRENCY_MAP
  ).freeze

  UNFORMATTED_CURRENCIES = ["IDR"].map(&:freeze).freeze

  has_many :country_phone_codes
  has_many   :tour_dates
  has_many   :product_tax_rates, as: :locale
  has_one    :country_website
  has_many   :foreign_exchange_pegged_rates
  has_many   :country_states, inverse_of: :country
  has_many   :country_state_cities, through: :country_states
  has_many   :country_audits # not expecting to use this has_many, but adding as a formality
  belongs_to :payoneer_ach_fee, primary_key: "target_country_code", foreign_key: "iso_code", class_name: "PayoneerAchFee"
  belongs_to :corporate_entity

  validates :name, :iso_code, :region, :tc_region, :iso_code_3, presence: true
  validates :name, :iso_code, :iso_code_3, uniqueness: true

  attr_translate :name

  scope :by_album_country, ->(album_countries) { where(id: album_countries.pluck(:country_id)) }

  default_scope { unsanctioned }
  scope :unsanctioned,     -> { where(is_sanctioned: false) }
  scope :sanctioned,       -> { unscoped.where(is_sanctioned: true) }
  scope :us_only,          -> { unsanctioned.where(iso_code: US_ONLY) }
  scope :gb_only,          -> { unsanctioned.where(iso_code: GB_ONLY) }
  scope :eu_gb_only,       -> { unsanctioned.where(iso_code: EU_GB_ONLY) }
  scope :non_eu_gb_only,   -> { unsanctioned.where(iso_code: NON_EU_GB_ONLY) }
  scope :eu,               -> { unsanctioned.where(iso_code: EU_COUNTRIES) }

  scope :find_by_name_or_iso_code,
        ->(search_key) {
          where(iso_code: search_key).or(where(name: search_key))
        }

  scope :find_by_country_phone_code,
        ->(search_phone_code) {
          joins(:country_phone_codes).where(country_phone_codes: { code: search_phone_code })
        }

  COUNTRIES = Country.unsanctioned.order(:name).map { |c| [c.name, c.iso_code] }

  def self.available_countries
    Country.unsanctioned.order(:name)
  end

  def self.search_by_iso_code(code)
    return find_by(iso_code: "GB") if code == "UK"

    find_by(iso_code: code)
  end

  def self.find_countries_with_sales_for_person(person_id)
    Country.select("distinct c.id, c.name, c.iso_code").from("countries c").joins("inner join sales_record_masters srm on srm.country = c.iso_code inner join sales_record_summaries sr on sr.sales_record_master_id = srm.id").where("sr.person_id = :person_id", person_id: person_id).order("c.name asc")
  end

  def self.country_name(country_code, raw = false)
    # account for legacy record with non iso country code
    country_code = LEGACY_NEW_COUNTRY_CODE_MAP[country_code] if LEGACY_NEW_COUNTRY_CODE_MAP.key?(country_code)
    country = Country.find_by(iso_code: country_code)
    return country&.name_raw if raw

    country&.name
  end

  def self.country_iso_code(country_name)
    Country.find_by(name: country_name)&.iso_code
  end

  def self.sanctioned_iso_codes
    unscoped.sanctioned.pluck(:iso_code)
  end

  def current_pegged_rate
    foreign_exchange_pegged_rates.order(created_at: :desc).first
  end

  def current_exchange_rate
    ForeignExchangeRate.current_exchange_rate(id)
  end

  def eu?
    EU_COUNTRIES.include?(iso_code)
  end

  def us_or_territories?
    UNITED_STATES_AND_TERRITORIES.include?(iso_code)
  end
end
