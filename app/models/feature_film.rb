#
# = Description
#
# The Feature Film model extends from Video and enables creation of feature films
#  Differences from video:  Duration is not important, Definition (HD or SD) is
#
# Subclass of Video using single-table inheritance
#
#
# = Usage
#
#
# = Change Log
#
# [2012-04-20 -- GFB]
# Creation
#
class FeatureFilm < Video
  include Tunecore::Lockable
  include Tunecore::Creativeable
  self.inheritance_column = "video_type"

  validate :presence_of_accepted_format
  validate :presence_of_definition
  validates :name, :person_id, :director, presence: true

  def initialize(options = {})
    options ||= {}

    super
  end

  private

  def presence_of_accepted_format
    return if (accepted_format == true)

    errors.add(:base, I18n.t("models.media.you_must_confirm_that_you_understand_tunecores_policy"))
  end

  def presence_of_definition
    errors.add(:base, I18n.t("models.media.you_must_select_hd_or_sd")) unless definition
  end
end
