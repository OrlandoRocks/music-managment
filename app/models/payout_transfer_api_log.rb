# frozen_string_literal: true

class PayoutTransferApiLog < ApplicationRecord
  belongs_to :payout_transfer
  RESPONSE_KIND = "response"
  REQUEST_KIND  = "request"

  scope :responses, -> { where(kind: RESPONSE_KIND) }
  scope :requests, -> { where(kind: REQUEST_KIND) }

  def parse_body
    return if kind.eql?(REQUEST_KIND)

    Oj.load(body).with_indifferent_access
  end
end
