class PersonPointsBalance < ApplicationRecord
  belongs_to :person

  validates :balance, presence: true
  validates :person_id, uniqueness: true

  def update_balance(amount)
    update(balance: balance + amount)
  end
end
