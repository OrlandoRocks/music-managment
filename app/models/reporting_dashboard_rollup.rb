class ReportingDashboardRollup < ApplicationRecord
  belongs_to :reporting_date
  belongs_to :country_website

  default_scope { order("report_date ASC") }

  def self.calculate_daily_values(for_date = Date.today)
    rep_date = ReportingDate.find_by(reporting_date: for_date)

    CountryWebsite.all.find_each do |country_website|
      row = where(report_date: for_date, country_website_id: country_website.id).first_or_create
      row.update(
        {
          reporting_date: rep_date,
          report_date: for_date,
          country_website_id: country_website.id,
          total_sales: Invoice.sales_amounts(for_date).find { |s| s["country_website_id"] == country_website.id }.try(:total_sales).try(:to_f) || 0.00,
          average_sales: Invoice.sales_amounts(for_date).find { |s| s["country_website_id"] == country_website.id }.try(:average_sales).try(:to_f) || 0.00,
          new_dist_first_purchase: Person.new_dist_customers(for_date).find { |d| d.country_website_id == country_website.id && d.purchase_type == "first_dist_and_new" }.try(:num_customers) || 0,
          new_dist_not_first_purchase: Person.new_dist_customers(for_date).find { |d| d.country_website_id == country_website.id && d.purchase_type == "first_dist_only" }.try(:num_customers) || 0,
          returning_dist: Invoice.returning_dist(for_date).find { |d| d["country_website_id"] == country_website.id }.try(:num_customers) || 0,
          renewals: Invoice.renewal_customers(for_date).find { |d| d["country_website_id"] == country_website.id }.try(:num_customers) || 0,
          pub_first_purchase: Person.pub_customers(for_date).find { |p| p.country_website_id == country_website.id && p.purchase_type == "pub_and_new" }.try(:num_customers) || 0,
          pub_not_first_purchase: Person.pub_customers(for_date).find { |p| p.country_website_id == country_website.id && p.purchase_type == "pub_only" }.try(:num_customers) || 0
        }
      )
    end
  end

  def self.get_daily_values(start_date, end_date, country)
    get_data("day_display", "reporting_date", start_date, end_date, country)
  end

  def self.get_weekly_values(start_date, end_date, country)
    get_data("week_display", "week_year", start_date, end_date, country)
  end

  def self.get_monthly_values(start_date, end_date, country)
    get_data("month_display", "month_year", start_date, end_date, country)
  end

  def self.get_data(display_field, group_by_field, start_date, end_date, country)
    find_by_sql(["select dates.#{display_field} rep_date, sum(coalesce(total_sales, 0)) total_sales, avg(coalesce(average_sales, 0)) average_sales, sum(coalesce(new_dist_first_purchase, 0)) new_dist_first_purchase, sum(coalesce(new_dist_not_first_purchase, 0)) new_dist_not_first_purchase, sum(coalesce(returning_dist, 0)) returning_dist, sum(coalesce(renewals, 0)) renewals, sum(coalesce(pub_first_purchase, 0)) pub_first_purchase, sum(coalesce(pub_not_first_purchase, 0)) pub_not_first_purchase, max(rollups.updated_at) updated_at from reporting_dates dates left join reporting_dashboard_rollups rollups on dates.id = rollups.reporting_date_id where dates.reporting_date >= ? and dates.reporting_date <= ? and country_website_id = ? group by dates.#{group_by_field}", start_date, end_date, country])
  end
end
