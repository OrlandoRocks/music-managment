class SyncLicenseProduction < ApplicationRecord
  belongs_to    :person
  has_many      :sync_license_requests

  validates :person, presence: true
end
