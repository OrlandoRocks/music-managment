class CreditUsage < ApplicationRecord
  #
  # constants
  #

  VALID_RELATED = ["Album", "Single", "Ringtone"]

  #
  # associations
  #

  belongs_to :related, polymorphic: true
  belongs_to :person

  belongs_to  :album,
              -> { where(credit_usages: { related_type: "Album" }) },
              foreign_key: "related_id",
              inverse_of: "credit_usage"

  has_one :purchase, as: :related
  has_one :plan_credit_usage_purchase, dependent: :nullify

  #
  # validations
  #

  validates :related, presence: true
  validates :person, presence: true
  validates :applies_to_type, presence: true

  validates :related_id, uniqueness: { scope: [:related_type] }

  validate :validate_type_of_related
  validate :ensure_credit_available
  validate :validate_applies_to_type

  #
  # scopes
  #

  scope :for_type, ->(type) { where(applies_to_type: type) }

  scope :for_all_types,
        -> {
          select("applies_to_type as related_type, COUNT(*) as temp_used")
            .group(:applies_to_type)
        }

  scope :not_finalized, -> { where(finalized_at: nil) }

  scope :distibution_credits, -> { where(plan_credit: false) }

  #
  # class methods
  #

  # creates a credit usage for creditable if able,
  # otherwise returns nil
  def self.for(person, creditable)
    return plan_credit_for(person, creditable) if person.owns_or_carted_plan?
    return if Product.free_release?(creditable)

    credit = nil

    current = credit_usage_for(person, creditable)
    if current
      credit = current unless current.finalized?
    elsif credit_available_for?(person, creditable.class.name) && !Inventory.used?(creditable)
      credit = create_credit(person, creditable)
    end

    credit
  end

  def self.plan_credit_for(person, creditable)
    credit = nil
    plan = person.plan_in_cart? ? person.cart_plan : person.plan

    current = credit_usage_for(person, creditable)
    if current
      credit = current unless current.finalized?
    # maybe person.credit_available_for? then we don't have to chain and call person in the argument as well
    # but the responsibility really feels like the plan. would like input on this.
    elsif plan.credit_available_for?(person, creditable.class.name) && creditable?(creditable.class.name)
      credit = create_credit(person, creditable, :plan)
    end

    credit
  end

  def self.create_credit(person, creditable, type = :default)
    case type
    when :plan
      credit = PlanCreditUsage.new(
        person: person,
        related: creditable,
        applies_to_type: creditable.class.name
      )
      credit.save ? credit : nil
    when :default
      credit = person.credit_usages.build(related: creditable, applies_to_type: creditable.class.name)
      credit.save ? credit : nil
    end
  end

  def self.credit_usage_for(person, creditable)
    usage = nil
    if creditable?(creditable.class.name)

      type = creditable.class.base_class.name
      usage = person.credit_usages.find_by("related_id = ? and related_type = ?", creditable.id, type)
    end
    usage
  end

  def self.credit_available_for?(person, type)
    number_credits_available(person, type).positive?
  end

  def self.number_credits_available(person, type)
    return 0 unless creditable?(type)

    total_inventories_avail = Inventory.total_left(person, type)
    accounted_for = person.credit_usages.not_finalized.for_type(type).count
    ((total_inventories_avail - accounted_for) <= 0) ? 0 : (total_inventories_avail - accounted_for)
  end

  def self.all_available(person)
    available_inventories = Inventory.all_available(person)
    current_used = person.credit_usages.not_finalized.for_all_types

    used = {}
    current_used.each do |u|
      used[u.related_type] = u.temp_used
    end

    available_inventories.select do |i|
      i[:total_left] =
        used.has_key?(i.inventory_type) ? (i[:total_left].to_i - used[i.inventory_type].to_i) : i[:total_left].to_i
      (i[:total_left]).positive?
    end
  end

  def self.creditable?(type)
    VALID_RELATED.include?(type)
  end

  #
  # public methods
  #

  def finalized?
    finalized_at?
  end

  #
  # Only want to use the related object and not the purchase or
  # product information when processing Inventory.use!. This will
  # allow for Inventory.use! to find the available credit
  #
  def inventory_use_options
    { item_to_use: related }
  end

  #
  # Callback made once related purchase marked as paid
  #
  def paid_post_proc
    # update attributes for for album purchases
    related.paid_post_proc

    update({ finalized_at: current_time_from_proper_timezone })
  end

  #
  # callback for when removing a credit usage from cart
  #
  def remove_from_cart_pre_proc
    related.remove_from_cart_pre_proc if related.respond_to?(:remove_from_cart_pre_proc)
  end

  #
  # callback for when adding credit usage to cart
  #
  def add_to_cart_pre_proc
    related.add_to_cart_pre_proc if related.respond_to?(:add_to_cart_pre_proc)
  end

  def can_distribute?
    return related.can_distribute? if related.respond_to?(:can_distribute?)

    true
  end

  delegate :name, to: :related

  def album_type
    related_type
  end

  #
  # private methods
  #

  private

  def validate_type_of_related
    errors.add(:related, "does not support credit usages") if related and !CreditUsage.creditable?(related.class.name)
  end

  #
  # Ensure we at least have one credit for the related class type available
  # that has not been accounted for by other credit usages
  #
  def ensure_credit_available
    return unless new_record? && !CreditUsage.credit_available_for?(person, related.class.name)

    errors.add(:base, "No credits available")
  end

  def validate_applies_to_type
    errors.add(:applies_to_type, "is not compatible with related item") if related.class.name != applies_to_type
  end
end
