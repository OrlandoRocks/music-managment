# frozen_string_literal: true

class CopyrightStatus < ApplicationRecord
  belongs_to :related, polymorphic: true
  has_many :copyright_state_transitions

  validates :status, presence: true

  enum status: {
    approved: "approved",
    pending: "pending",
    rejected: "rejected"
  }
end
