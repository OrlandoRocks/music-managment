# = Description
# The inventory model is used to store each customers purchased inventory, both TuneCore and 3rd Party.
# When a user purchases a product that contains consumable items (Album Distributions, Ringtone Distributions, etc.)
# those items are added to this table based on the product_item_rules set for the product being purchased.
#
#
# = Usage
# Inventory.use!(person, product, product_to_use)
# Simply pass in the person this applies to
#
# Inventory.used?(item)
#
# Inventory.process_product (person, inventory_type)
# Inventory.total_left (person, inventory_type)
# Inventory.count (person, inventory_type)
# Inventory.all_unused_inventory(person)
#
# = Change Log
# [2009/12/16 - CH]
# Created Model
#
# [2010-09-24 -- CH]
# Added can_use? method and pointed Inventory.use_single_item to can_use? instead of used?

class Inventory < ApplicationRecord
  #################
  # Associations #
  #################
  belongs_to :person
  belongs_to :purchase
  belongs_to :product_item
  belongs_to :parent, class_name: "Inventory"

  has_many :sub_items, class_name: "Inventory", foreign_key: "parent_id"
  has_many :inventory_usages
  has_many :inventory_adjustments

  ##########################
  # Class level variables #
  ##########################
  ARRAY_RETRIEVAL_METHODS   = { "Salepoint" => "salepoints", "Booklet" => "booklet", "Song" => "songs", "Artwork" => "artwork" }
  MAIN_INVENTORY_TYPES      = ["Album", "Single", "Ringtone"]
  SINGLE_USE_CLASSES        = [Album, Single, Ringtone, Salepoint, Song, Booklet, MusicVideo, FeatureFilm, Video]
  MULTI_USE_CLASSES         = []

  attr_accessor :item_to_use, :items_to_ditribute, :distribute_immediately

  ################
  # Validations #
  ################
  validates :product_item, presence: true
  validates :purchase, presence: true
  validates :title, presence: true

  scope :remaining_for_person,
        ->(person_id) {
          where(person_id: person_id, parent_id: nil).where("quantity > 1 OR (quantity = 1 AND quantity_used = 0)")
        }

  #########################
  # Public Class Methods #
  #########################

  # person
  #
  # options
  # * :item_to_use
  # * :product_to_use
  #
  # * :items_to_use [{:item_to_use => item_to_use, :product => product}, {:item_to_use => item_to_use, :product => product}]
  # Inventory.use!(current_user, {:item_to_use => album})
  def self.use!(person, options)
    # transform into InventoryItem objects
    items = InventoryItemCollector.collect(person, options)
    invoke_distribution = !(options[:distribute] == false)

    if invoke_distribution
      InventoryItem.distribute!(items)
    else
      items
    end
  end

  #
  #  Inventory#total_left
  #  person:  a Person object
  #  inventory_type:  a String representation of the class wanted
  #
  def self.total_left(person, inventory_type)
    params = { person_id: person.id, inventory_type: inventory_type }
    result = find_by_sql(
      [
        "
        SELECT (SUM(quantity) - SUM(quantity_used) + SUM(quantity_adjustment)) as total_left
        FROM inventories
        WHERE person_id = :person_id
        AND inventory_type = :inventory_type
        AND (parent_id IS NULL or parent_id = 0)
        AND (expires_at >= NOW() or expires_at IS NULL)
      ",
        params
      ]
    )
    result.first.total_left.to_i
  end

  def self.has_inventory_for?(person, inventory_type)
    total_left(person, inventory_type).positive?
  end

  # Contains the logic that returns all available and used inventory for an individual
  # excludes child inventory records
  def self.all_available(person)
    params = { person_id: person.id, product_type: Product::AD_HOC }
    select("title, inventory_type, SUM(quantity) - SUM(quantity_used) + SUM(quantity_adjustment) as total_left")
      .where([
               "
        person_id = :person_id
        AND (parent_id = 0 OR parent_id IS NULL)
        AND products.product_type <> :product_type
      ",
               params
             ])
      .joins("
        INNER JOIN product_items ON product_items.id = inventories.product_item_id
        INNER JOIN products ON product_items.product_id = products.id
      ")
      .group("inventory_type, title")
      .order("inventory_type")
      .having("total_left > 0")
  end

  def quantity_remaining
    quantity - quantity_used + quantity_adjustment
  end

  def self.quantity_remaining_by_type(person_id, inventory_type)
    inventories = Inventory.where({ person_id: person_id, parent_id: nil, inventory_type: inventory_type })
    inventories.sum(&:quantity_remaining)
  end

  def self.used_by_purchase(person, purchase)
  end

  def self.used_by_type(person, inventory_type)
  end

  def self.used?(item)
    usage(item).present?
  end

  def self.usage(item)
    item_type = item.class.base_class.name
    InventoryUsage.where(related_type: item_type, related_id: item.id).first
  end

  def self.original_product_item_from_used(item)
    item_type = item.class.base_class.name
    ProductItem.joins("inner join inventories i on i.product_item_id=product_items.id INNER JOIN inventory_usages iu on iu.inventory_id=i.id").where("related_type = :related_type AND related_id = :related_id", { related_type: item_type, related_id: item.id }).first
  end

  def self.is_multi_use?(item)
    MULTI_USE_CLASSES.include?(item.class)
  end

  def self.distribution_credits(person)
    product_ids = Product.find_products_for_country(person.country_domain, :distribution_credit)
    Inventory.where("product_id IN (?) and person_id=? AND parent_id IS NULL", product_ids, person.id).joins(:product_item)
  end

  ############################
  # Public Instance Methods #
  ############################

  #
  #  if all sub items do not match,
  #  return false
  #
  def sub_items_match?
    sub_items.each do |sub|
      return false unless sub_item_match?(sub)
    end
    true
  end

  #
  #  does the given sub item match the
  #  existing parent object
  #
  def sub_item_match?(sub_item)
    sub_item.is_unlimited? || sub_item.quantity_is_within_inventory_limit?
  end

  #
  #  quantity: the limit of available inventory
  #  related_items_count: total inventory used
  #
  def quantity_is_within_inventory_limit?
    number_of_items_used <= limit
  end

  #
  #  quantity: represents the limit of the amount of items that can
  #  be used for this inventory
  #
  def limit
    quantity
  end

  #
  #  related items count: represents the amount of actual items used for
  #  this inventory
  #
  def number_of_items_used
    related_items_count
  end

  def is_unlimited?
    !!unlimited
  end

  def process_usage
    transaction do
      # associate top-level inventory item
      InventoryUsage.create(inventory: self, related: item_to_use)
      update_usage_count(quantity_used + 1)
      if sub_items.present?
        sub_items.each do |sub|
          sub.item_to_use = item_to_use
          related_items = sub.related_items_for_child_inventory
          if related_items.respond_to?(:each)
            related_items.each do |related_item|
              sub.inventory_usages.create(related: related_item) if related_item
            end
          elsif related_items
            sub.inventory_usages.create(related: related_items)
          end
        end
      end
    end
  end

  def related_items_for_child_inventory
    case inventory_type
    when "Salepoint"
      item_to_use.salepoints
    when "Song"
      item_to_use.songs
    when "Artwork"
      item_to_use.artwork
    when "ShippingLabel"
      item_to_use.shipping_label
    end
  end

  def needs_parents_item_to_use?
    !item_to_use && !!parent
  end

  def related_items_count
    self.item_to_use = parent.item_to_use if needs_parents_item_to_use?

    begin
      case inventory_type
      when "Salepoint"
        item_to_use.salepoints.count
      when "Song"
        item_to_use.songs.count
      when "Artwork"
        item_to_use.artwork.count
      when "Booklet"
        item_to_use.booklet.count
      when "ShippingLabel"
        item_to_use.shipping_label.count
      else
        0
      end
    rescue NoMethodError
      0
    end
  end

  private

  ##########################
  # Private Class Methods #
  ##########################
  # For multi-use classes, we don't care if
  # they've been used once or 100 times,
  # but we do need to prevent most classes from being redistributed in
  # any way (i.e. albums, singles, ringtones)
  def self.can_use?(item)
    if MULTI_USE_CLASSES.include?(item.class)
      true
    else
      usage(item).blank?
    end
  end

  def self.use_single_item!(person, options = {})
    item = options[:item_to_use]
    product_to_use = options[:product_to_use]
    purchase_to_use = options[:purchase]

    raise "This #{item.class.name} has already been used." unless can_use?(item)

    inventory = find_best_inventory_match(person, item, product_to_use, purchase_to_use)

    raise "No Inventory was found matching this #{item.class.name}." if inventory.nil?

    inventory.item_to_use = item
    inventory.process_usage
    inventory
  end

  def self.match_expiring_inventory_sub_items(inventories, item)
    expiring_inventories = inventories.select { |i| i.expires_at != nil }
    unless expiring_inventories.empty?
      expiring_inventories.each do |inventory|
        inventory.item_to_use = item
        return inventory if inventory.sub_items_match?
      end
    end
    nil
  end

  def self.match_non_expiring_inventory_sub_items(inventories, item)
    inventories = inventories.select { |i| i.expires_at.nil? }
    unless inventories.empty?
      inventories.each do |inventory|
        inventory.item_to_use = item
        return inventory if inventory.sub_items_match?
      end
    end
    nil
  end

  def self.match_sub_items(inventories, item)
    inventory_to_use = match_expiring_inventory_sub_items(inventories, item)
    inventory_to_use = match_non_expiring_inventory_sub_items(inventories, item) if inventory_to_use.nil?
    inventory_to_use
  end

  def self.find_best_inventory_match(person, item, product_to_use = nil, purchase_to_use = nil)
    # class_name = (item.class.name == "Video")? item.video_type : item.class.name  # this is a bit of a hack but it makes the videos finalization work - will revisit when we do the renewals
    class_name = item.class.name

    if purchase_to_use != nil
      inventories = person.inventories
                          .where([
                                   "purchase_id = ?
                                    and inventory_type = ?
                                    and (((quantity+quantity_adjustment) > quantity_used) or unlimited=1) and (expires_at IS NULL or expires_at >= NOW())
                                    and (parent_id=0 or parent_id IS NULL)",
                                   purchase_to_use.id,
                                   class_name
                                 ])
                          .order("expires_at, created_at")

    elsif product_to_use != nil && purchase_to_use.nil?
      inventories = person.inventories
                          .where(["inventory_type = ? and (((quantity+quantity_adjustment) > quantity_used) or unlimited=1) and (expires_at IS NULL or expires_at >= NOW()) and (parent_id=0 or parent_id IS NULL) and pi.product_id=?", class_name, product_to_use.id])
                          .joins("inner join product_items pi on pi.id=inventories.product_item_id")
                          .order("expires_at, created_at")
    else
      inventories = person.inventories
                          .where(["inventory_type = ? and (((quantity+quantity_adjustment) > quantity_used) or unlimited=1) and (expires_at IS NULL or expires_at >= NOW()) and (parent_id=0 or parent_id IS NULL)", class_name])
                          .order("expires_at, created_at")
    end

    match_sub_items(inventories, item)
  end

  #############################
  # Private Instance Methods #
  #############################
  def update_usage_count(new_count = nil)
    if new_count.nil?
      update_attribute(:quantity_used, inventory_usages.count)
    else
      update_attribute(:quantity_used, new_count)
    end
  end
end
