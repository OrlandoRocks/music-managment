class Copyright < ApplicationRecord
  belongs_to :copyrightable, polymorphic: true
  belongs_to :person

  attr_accessor :skip_composition_and_recording_validation

  validates :composition, presence: true, unless: :skip_composition_and_recording_validation
  validates :copyrightable, presence: true
  validates :recording, presence: true, unless: :skip_composition_and_recording_validation

  # Prepare for safe migration that removes these columns
  self.ignored_columns = %w[holder_name ownership year]

  # If a user CLAIMS to own 100% of a song,
  # then their person.id gets stored as a "claimant"
  alias_attribute :claimant_id, :person_id
  alias_attribute :claimant, :person
end
