class DistributionApiService < ApplicationRecord
  before_create :set_uuid

  BYTEDANCE = "Bytedance".freeze

  def set_uuid
    self.uuid = SecureRandom.uuid
  end

  def bytedance?
    name == BYTEDANCE
  end
end
