class Notification < ApplicationRecord
  belongs_to :person
  belongs_to :notification_item, polymorphic: true
  belongs_to :targeted_notification

  validates :effective_created_at, presence: true
  validates :targeted_notification_id, uniqueness: { scope: :person_id, allow_nil: true }

  before_validation :set_effective_created_at, on: :create
  before_validation :setup_system_notification, on: :create, if: -> { respond_to? :setup_system_notification }

  #
  # Uses image_url from targeted notification if image_url blank to save db space
  #
  def image_url
    ret = self[:image_url]

    if ret.blank? && targeted_notification.present? && targeted_notification.notification_icon.present?
      ret = targeted_notification.notification_icon.file.url(:thumb)
    end

    ret
  end

  #
  # Uses link_text from targeted notification if link_text blank to save db space
  #
  def link_text
    ret = self[:link_text]

    ret = targeted_notification.link_text if ret.blank? && targeted_notification.present?

    ret
  end

  def mark_actioned
    self.first_clicked = DateTime.now if first_clicked.blank?
    self.last_clicked  = DateTime.now
    self.click_count = 0 if click_count.blank?
    self.click_count += 1
    save
  end

  def mark_archived
    self.first_archived = DateTime.now if first_archived.blank?
    save
  end

  def set_effective_created_at
    self.effective_created_at = DateTime.now if effective_created_at.blank?
  end

  #
  # Use the title from the targeted notification if title is blank saving db space
  #
  def title
    ret = self[:title]

    ret = targeted_notification.title if ret.blank? && targeted_notification.present?

    ret
  end

  #
  # Use the text from the targeted notification if text is blank saving db space
  #
  def text
    ret = self[:text]

    ret = targeted_notification.text if ret.blank? && targeted_notification.present?

    ret
  end

  #
  # Use url from the targeted notification if url is blank saving db space
  #
  def url
    ret = self[:url]

    ret = targeted_notification.url if ret.blank? && targeted_notification.present?

    ret
  end

  def self.archived(archived)
    if archived
      where("first_archived is not null")
    else
      where("first_archived is null")
    end
  end

  def self.read(read)
    case read
    when "1"
      where("first_clicked is not null")
    when "0"
      where("first_clicked is null")
    else
      all
    end
  end

  def self.range(start_date, end_date)
    if start_date && end_date
      start_date = Date.parse(start_date)
      end_date = 1.month.from_now(Date.parse(end_date)) - 1.day
      where(effective_created_at: (start_date..end_date))
    else
      all
    end
  end

  #
  # Returns an unsaved notification for a person created from a targeted notification
  #
  def self.build_from_targeted_notification(targeted_notification, person_id)
    notification = Notification.new(
      targeted_notification_id: targeted_notification.id,
      effective_created_at: targeted_notification.created_at
    )

    # Apply Templates

    notification.person_id = person_id
    notification
  end

  #
  # Marks a collection of notifications as seen
  #
  def self.mark_seen(person, notifications)
    person.notifications.seen(false).where("id in (?)", notifications.map(&:id)).update_all("first_seen = NOW()")
  end

  def self.seen(seen)
    if seen
      where("first_seen is not null")
    else
      where("first_seen is null")
    end
  end

  #
  # Unseen notifications + any targeted global notifications that
  # have no notification associated with it
  #
  def self.unseen_notification_count(person)
    person.notifications.seen(false).count + TargetedNotification.new_global_notifications(person).count
  end

  def self.all_seen?(person)
    unseen_notification_count(person).zero?
  end

  #
  # Returns unarchived notifications, instantiating any new global notifications
  #
  def self.unarchived_notifications(person, options = {})
    options[:limit] ||= 100

    TargetedNotification.new_global_notifications(person).each do |global|
      notification = Notification.build_from_targeted_notification(global, person.id)
      notification.save! if notification
    end

    person.notifications.archived(false).includes(:targeted_notification).limit(options[:limit])
  end
end
