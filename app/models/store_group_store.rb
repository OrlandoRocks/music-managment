#
# = Description
#
# = Usage
#
# = History
#
#
class StoreGroupStore < ApplicationRecord
  belongs_to :store_group
  belongs_to :store
end
