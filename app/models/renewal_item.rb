# = Description
# RenewalItem supports the renewal table and references the items that are
# renewed according to the rules set in the renewal and ProductItem models.
#
# = Usage
#
#
# = Change Log
# [2010-01-08 -- CH]
# Created Model
class RenewalItem < ApplicationRecord
  belongs_to :related, polymorphic: true
  belongs_to :renewal

  ################
  # Validations #
  ################
  # validates_presence_of :related, :renewal
  #

  def is_subscription?
    !(related.is_a?(Album) || related.is_a?(Video))
  rescue NameError => e
    Rails.logger.error("\n#{e.class} (#{e.message})")
  end
end
