class SyncLicenseOption < ApplicationRecord
  belongs_to :sync_license_request

  validates :sync_license_request, :number, presence: true
  validates :number, numericality: { only_interger: true }
end
