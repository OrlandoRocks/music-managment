class NotificationIcon < ApplicationRecord
  has_attached_file(
    :file,
    styles: {
      thumb: {
        geometry: "50x50", format: :jpg
      }
    },
    default_url: "/images/missing.png",
    storage: :s3,
    path: "/:id/:style/:filename",
    bucket: NOTIFICATION_ICONS_BUCKET_NAME,
    s3_credentials: {
      access_key_id: AWS_ACCESS_KEY,
      secret_access_key: AWS_SECRET_KEY
    },
    s3_permissions: :public_read
  )

  validates_attachment(
    :file,
    presence: true,
    content_type: {
      content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]
    }
  )
end
