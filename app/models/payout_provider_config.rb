# frozen_string_literal: true

class PayoutProviderConfig < ActiveRecord::Base
  include ProviderSecretsEncryptable

  PAYONEER_NAME = "Payoneer"
  PAYPAL_NAME   = "PayPal"

  TC_US_PROGRAM_ID = "100084980"
  BI_US_PROGRAM_ID = "100114150"
  TC_UK_PROGRAM_ID = "100094990"
  BI_UK_PROGRAM_ID = "100148850"
  TC_EU_PROGRAM_ID = "100094970"
  BI_EU_PROGRAM_ID = "100148830"
  TC_AU_PROGRAM_ID = "100095710"
  BI_AU_PROGRAM_ID = "100148810"
  TC_CA_PROGRAM_ID = "100095690"
  BI_CA_PROGRAM_ID = "100148790"

  # A special payoneer program that accepts a secondary tax-form for
  # separate taxation of distribution and publishing revenues streams.
  SECONDARY_TAX_FORM_PROGRAM_ID = "100165480"

  PROGRAM_IDS = [
    TC_US_PROGRAM_ID,
    BI_US_PROGRAM_ID,
    TC_UK_PROGRAM_ID,
    BI_UK_PROGRAM_ID,
    TC_EU_PROGRAM_ID,
    BI_EU_PROGRAM_ID,
    TC_AU_PROGRAM_ID,
    BI_AU_PROGRAM_ID,
    TC_CA_PROGRAM_ID,
    BI_CA_PROGRAM_ID
  ].freeze

  DEFAULT_US_TAX_PROGRAMS = Set[
    TC_US_PROGRAM_ID,
    TC_UK_PROGRAM_ID,
    TC_EU_PROGRAM_ID,
    TC_AU_PROGRAM_ID,
    TC_CA_PROGRAM_ID,
  ].freeze

  before_create :encrypt_attributes

  has_one :cipher_datum, as: :cipherable
  has_many :payout_providers
  has_many :tax_forms, dependent: :nullify
  belongs_to :corporate_entity

  # this is a temp method for create which we convert to JSON and encrypt
  # on the cipher_datum's 'secrets' attribute to save to the db
  attr_accessor :password

  scope :by_env, -> { where.not(sandbox: Rails.env.production?) }
  scope :by_program, ->(program_id) { where(program_id: program_id) }
  scope :default_us_tax_program_configs, -> { by_env.by_program(DEFAULT_US_TAX_PROGRAMS) }

  def self.by_currency(currency)
    find_by(currency: currency, sandbox: !Rails.env.production?)
  end

  def self.secondary_tax_form_config
    by_env.by_program(SECONDARY_TAX_FORM_PROGRAM_ID).take
  end

  private

  # This is done to ensure the JSON we encrypt is long enough since
  # Cipherer does not work well with very short strings
  #
  # This strategy also makes it more stronger so it is harder to hack.
  def encryptable_attributes
    {
      name: name,
      username: username,
      password: password
    }
  end
end
