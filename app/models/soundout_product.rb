class SoundoutProduct < ApplicationRecord
  include Jsonable

  belongs_to :product
  has_many :soundout_reports

  STARTER  = "starter"
  ENHANCED = "enhanced"
  PREMIUM  = "premium"

  #
  # Call soundout fetch report and process
  #
  # Returns: The report data
  #
  def fetch_report(soundout_report)
    success, report_data = Soundout.fetch_report(soundout_report.soundout_id)

    soundout_report.process_report_fetch(success, report_data)
    return success, report_data
  end

  #
  # Call soundout processing library
  # Sets the purchases status and soundout_id
  #
  # Returns: The soundout identifier for the report
  #
  def order_report(soundout_report)
    soundout_id = nil

    if soundout_report.paid?
      soundout_id = Soundout.order_report(soundout_report)

      # Let the report purchase process the order
      soundout_report.process_order(soundout_id)
    end

    soundout_id
  end

  #
  # Find an soundout product for a purchase.  Requires join to products
  # for country_website
  #
  def self.find_for_purchase(type, country_website)
    joins(:product)
      .where("report_type = :type and country_website_id = :cw_id", { type: type, cw_id: country_website.id }).first
  end

  # Method to purchase multiple soundout products, will return errors if they exist
  def self.purchase(person, purchases, assets)
    transaction do
      errored_report_purchases = []
      is_errored_purchases = false
      purchases.each do |song_id, report_type|
        asset_type = assets[song_id]
        report = SoundoutProduct.find_for_purchase(report_type, person.country_website)
        product = Product.find(report.product_id)
        create_params = {
          soundout_product_id: report.id,
          person_id: person.id,
          track_id: song_id,
          status: "new"
        }
        create_params[:track_type] = asset_type == "song_library" ? "SongLibraryUpload" : "Song"
        report_purchase = SoundoutReport.create(create_params)
        errored_report_purchases << report_purchase and next unless report_purchase.valid?

        purchase = Product.add_to_cart(person, report_purchase, product)

        # Check if purchase is valid?
        is_errored_purchases = !purchase.valid?
      end

      if errored_report_purchases.present?
        { error_type: :invalid_report_purchases, soundout_reports: errored_report_purchases }
      elsif is_errored_purchases
        { error_type: :invalid_purchases }
      else
        { error_type: false }
      end
    end
  end

  def self.soundout_products_detail(country_website_id, person = nil)
    soundout_products = SoundoutProduct
                        .includes(:product)
                        .where("products.country_website_id = ?", country_website_id)
                        .references(:product)

    soundout_products.as_json(person: person, only: [:id, :report_type, :display_name, :price, :currency], root: false)
  end

  def as_json(options = {})
    person = options.delete(:person)

    new_attributes = {
      "price" => adjusted_product_price(person: person),
      "currency" => product.currency
    }

    merge_attributes_as_json(new_attributes, options)
  end

  private

  def adjusted_product_price(person: nil)
    if person && product
      product.set_targeted_product(TargetedProduct.targeted_product_for_active_offer(person, product))
      product.set_adjusted_price(product.calculate_price(product))
    else
      product&.price
    end
  end
end
