class SyncMembershipRequest < ApplicationRecord
  belongs_to :person

  validates :email, presence: true
  validates :email, uniqueness: { message: "already in use" }
  validates :email, confirmation: { message: "should match confirmation" }
  validates :email,
            format: {
              with: /\A([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})\z/,
              if: proc { |record| record.email.present? }
            }

  validates  :name, presence: true
  validates  :company, presence: true
  validates  :status, presence: true
  validates :status, inclusion: { in: %w[pending approved declined] }

  before_validation :set_default_status, on: :create
  validates  :phone_number, presence: true
  validates  :country, presence: true

  #
  # Marks the sync request as approved
  #
  def approve
    str = email + Time.now.to_i.to_s + rand.to_s
    invite_code = Digest::SHA1.hexdigest("3v3ry0n310v3sTC--#{str}--")[0..39]
    update({ status: "approved", request_code: invite_code })
  end

  def approved?
    status == "approved"
  end

  #
  # Completes a sync mmembership request
  #
  def complete_registration(person_params)
    person = nil
    transaction do
      person = Person.new(person_params)
      person.features << Feature.where("name = ?", "sync_licensing").first
      person.is_verified = false

      # Copy info from the request
      person.email              = email
      person.name               = name
      person.business_name      = company
      person.country            = country
      person.phone_number       = phone_number
      person.invite_code        = nil
      person.invite_expiry      = nil
      person.is_verified        = true

      person.save!

      # Link the request to the new person
      self.person_id = person.id

      # Save the request
      save!
    end

    person
  rescue StandardError => e
    Rails.logger.info("SyncMembershipRequest.complete_registration: #{e}")
    person
  end

  #
  # Marks the sync request as declined
  #
  def decline
    update_attribute(:status, "declined")
  end

  def declined?
    status == "declined"
  end

  def pending?
    status == "pending"
  end

  def created_at_time
    created_at.in_time_zone("America/Los_Angeles")
  end

  delegate :zone, to: :created_at_time, prefix: true

  private

  def set_default_status
    self.status = "pending" if status.blank?
  end
end
