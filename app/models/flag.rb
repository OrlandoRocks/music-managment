# frozen_string_literal: true

class Flag < ApplicationRecord
  has_many :flag_reasons, dependent: :destroy

  WITHDRAWAL = {
    category: "withdrawal",
    name: "blocked"
  }.freeze

  ADDRESS_LOCKED = {
    category: "person_location",
    name: "address_locked"
  }.freeze

  BLOCKED_FROM_DISTRIBUTION = {
    category: "distribution",
    name: "blocked_from_distribution"
  }.freeze

  FIRST_NAME_LOCKED = {
    category: "identity_individual",
    name: "first_name_locked"
  }.freeze

  LAST_NAME_LOCKED = {
    category: "identity_individual",
    name: "last_name_locked"
  }.freeze

  COMPANY_NAME_LOCKED = {
    category: "identity_business",
    name: "company_name_locked"
  }.freeze

  CUSTOMER_TYPE_LOCKED = {
    category: "identity_type",
    name: "customer_type_locked"
  }.freeze

  SUSPICIOUS = {
    category: "account",
    name: "suspicious"
  }.freeze

  AUTO_RENEWAL_EMAIL_SENT = {
    category: "vat_compliance",
    name: "auto_renewal_email_sent"
  }.freeze

  BLOCKED_FROM_YT_MONEY = {
    category: "distribution",
    name: "blocked_from_yt_money"
  }.freeze

  BLOCKED_FROM_PLANS = {
    category: "plans",
    name: "blocked_from_plans"
  }.freeze

  SELF_IDENTIFIED_ADDRESS_LOCKED = {
    category: "person_location",
    name: "self_identified_address_locked"
  }.freeze

  TC_ACCELERATOR_OPTED_OUT = {
    category: "distribution",
    name: "tc_accelerator_opt_out"
  }.freeze

  TC_ACCELERATOR_DO_NOT_NOTIFY = {
    category: "distribution",
    name: "tc_accelerator_do_not_notify"
  }.freeze

  FLAGS_WITH_MANDATORY_FLAG_REASON = [
    ADDRESS_LOCKED
  ].freeze

  has_many :flag_reasons, dependent: :destroy
  has_many :flag_transitions

  scope :suspicious_flag, -> { find_by(SUSPICIOUS) }
  scope :blocked_from_distribution_flag, -> { find_by(BLOCKED_FROM_DISTRIBUTION) }

  scope :with_mandatory_flag_reason, -> {
    where(*FLAGS_WITH_MANDATORY_FLAG_REASON)
  }
end
