class PayoutServiceFee < ApplicationRecord
  has_many :eft_batch_transactions
  belongs_to :country_website

  TYPES = ["eft_service", "eft_failure", "check_service"]
  PAYOUT_FEE_RECIPIENTS = ["report_eft@tunecore.com"]

  validates :amount, :fee_type, :currency, :country_website_id, presence: true
  validates :amount, numericality: { greater_than: 0 }
  validates :fee_type, inclusion: { in: PayoutServiceFee::TYPES }
  after_create :mark_old_fees_as_archived
  after_create :notify_of_fee_change

  scope :fee_type, ->(fee_type) { where(fee_type: fee_type) }
  scope :for_country_website, ->(country_website_id) { where(country_website_id: country_website_id) }
  scope :most_recent, -> { order("created_at DESC").limit(1) }
  scope :active, -> { where(archived_at: nil) }

  def self.current(fee_type, country_website_id = 1)
    (self.fee_type(fee_type).for_country_website(country_website_id).active.most_recent.first ||
    self.fee_type(fee_type).for_country_website(country_website_id).most_recent.first)
  end

  private

  def mark_old_fees_as_archived
    old_fees_of_the_same_type = PayoutServiceFee.where(
      [
        "archived_at IS NULL AND fee_type = ? AND id != ?",
        fee_type,
        id
      ]
    )
    old_fees_of_the_same_type.each do |old_fee|
      old_fee.update(archived_at: Time.now)
    end
  end

  def notify_of_fee_change
    EftNotifier.payout_fee_change_notice(id).deliver
  end
end
