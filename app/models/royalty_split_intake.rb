class RoyaltySplitIntake < ApplicationRecord
  belongs_to :person
  has_many :royalty_split_details, dependent: :destroy
  has_one :person_transaction, as: :target, dependent: :destroy
end
