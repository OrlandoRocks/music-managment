class TaxWithholdings::IRSPayablesReport
  extend Memoist

  attr_accessor :irs_transaction

  def initialize(irs_transaction: nil)
    @irs_transaction = irs_transaction if irs_transaction.present?
  end

  def filename
    report_end_date =
      irs_tax_withholdings
      .order("irs_tax_withholdings.created_at ASC")
      .pluck(:created_at)
      .last
      .to_date

    "irs_payables_report_#{Rails.env}_#{irs_transaction.id}_" +
      "#{report_end_date.year}w#{report_end_date.cweek}.csv"
  end
  memoize :filename

  def generate(async: true)
    TaxWithholdings::Reporting::IRSPayablesReportWorker.tap do |worker|
      async ? worker.perform_async(irs_transaction.id) : worker.new.perform(irs_transaction.id)
    end
  end

  def fetch
    RakeDatafileService.download(filename).local_file.tap do |file|
      CSV.read(
        file,
        headers: true,
        liberal_parsing: true
      )
    end
  end

  private

  def irs_tax_withholdings
    IRSTaxWithholding
      .for_irs_transaction(irs_transaction.id)
      .with_people_and_transactions
  end
end
