class EncumbranceSummary < ApplicationRecord
  has_many :encumbrance_details
  belongs_to :person

  scope :outstanding, -> { where("outstanding_amount > 0") }

  validates :person_id, :reference_source, :total_amount, :fee_amount, :outstanding_amount, presence: true
  validates :person_id, :total_amount, :fee_amount, :outstanding_amount, numericality: { greater_than_or_equal_to: 0 }

  after_create :create_initial_detail

  def self.encumbrance_amount_for_person(person)
    person.encumbrance_summaries.sum(:outstanding_amount)
  end

  private

  def create_initial_detail
    EncumbranceDetail.create(
      {
        encumbrance_summary_id: id,
        transaction_date: Date.today,
        transaction_amount: total_amount
      }
    )
  end
end
