class PayoneerAchFee < ActiveRecord::Base
  has_one :country, primary_key: "target_country_code", foreign_key: "iso_code", class_name: "Country"

  validates :source_currency, presence: true

  def self.get_fee_cents(country_code, _person, _payee_details)
    ach_fees = PayoneerAchFee.find_by(
      target_country_code: country_code
      # source_currency: person.site_currency,
      # target_currency: payee_details.withdrawal_currency
    )
    # commenting out source currency and target currency because they currently only have the potential
    # to make the user's fee 0.
    # There will be one fee for each country code, and for now that is all we will apply for ach withdraws
    # we may wish to make this more sophisticated down the line, in which case
    # we will need to include source and target country
    ach_fees ? (ach_fees.transfer_fees * 100) : 0
  end
end
