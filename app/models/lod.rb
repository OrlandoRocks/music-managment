require "S3"

# Lod means Letter of Directions
class Lod < ApplicationRecord
  include ERB::Util
  LODS_ASEETS_DIR = "#{Rails.root}/tmp/lods"

  TEMPLATE_ID = "a_1016785_d2929736495a445db458d31099f20f27".freeze
  STATUSES = [
    :scheduled_to_send,
    :sent_to_customer,
    :signed_by_customer,
    :resent_to_customer,
    :sent_to_pro,
    :confirmed_by_pro,
    :expired
  ].freeze

  STATUSES_TEXT = {
    scheduled_to_send: "Scheduled".freeze,
    sent_to_customer: "Sent".freeze,
    signed_by_customer: "Signed".freeze,
    resent_to_customer: "Resent".freeze,
    sent_to_pro: "PRO Sent".freeze,
    confirmed_by_pro: "PRO Confirmed".freeze,
    expired: "Expired".freeze
  }.freeze.with_indifferent_access

  EXPIRED_DAY = 46

  has_one :publishing_composer
  has_one :composer
  has_one :publishing_composer
  has_one :paper_agreement, as: :related

  has_many :history, class_name: "LodHistory"

  belongs_to :s3_asset

  after_save :create_history

  default_scope { select "lods.*, DATEDIFF(CURDATE(), last_status_at) as days_since_last_status" }
  scope :sent_to_customer, -> { where(last_status: "sent_to_customer") }
  scope :expired, -> { sent_to_customer.where("DATEDIFF(CURDATE(), last_status_at) >= ?", EXPIRED_DAY) }

  def composer_record
    publishing_composer || composer
  end

  def update_document_status(rs_document_source)
    success = false
    state = rs_document_source["state"]
    case state
    when "signed"
      if sent_to_customer?
        update_attribute(:raw_response, rs_document_source)
        signed_date = extract_signed_date(rs_document_source)
        mark_as_signed_by_customer(signed_date)
        success = true
      end
    end
    success
  end

  def doc_date
    I18n.l(Time.now, format: :country_date)
  end

  def doc_recipient_name
    composer_record.full_name
  end

  def doc_publisher_entity
    composer_record.publisher_admin_name
  end

  def doc_publisher_name
    composer_record.publisher.name_with_pro
  end

  def doc_subject
    "#{doc_publisher_name} - Letter of Direction"
  end

  def doc_callback_location
    ""
  end

  def mark_as_scheduled_to_send(datetime = nil)
    mark_last_status "scheduled_to_send", datetime
  end

  def mark_as_sent_to_customer(datetime = nil)
    mark_last_status "sent_to_customer", datetime
  end

  def mark_as_signed_by_customer(datetime = nil)
    mark_last_status "signed_by_customer", datetime
  end

  def mark_as_expired(datetime = nil)
    mark_last_status "expired", datetime
  end

  def scheduled_or_sent?
    !last_status.nil?
  end

  def sent_to_customer?
    ["sent_to_customer", "resent_to_customer"].include?(last_status)
  end

  def signed_by_customer?
    !["scheduled_to_send", "sent_to_customer", "resent_to_customer"].include?(last_status)
  end

  def reset_status
    update_attribute(:last_status, nil)
  end

  def status_text
    return unless STATUSES_TEXT.key?(last_status)

    STATUSES_TEXT[last_status]
  end

  def days_since_sent
    # This is being populated by default_scope. Beware of the usage because there's a bug in Rails < 3 in which
    # the default_scope is not being used when the model is eager loaded, like :include => :lod
    return unless sent_to_customer?

    # TODO: if this attribute is not available, then maybe we have to calculate it here
    attributes["days_since_last_status"].to_i || nil
  end

  def upload_asset(_file)
    asset = S3Asset.new(bucket: SIGNATURE_BUCKET_NAME, key: s3_key, filename: filename)
    asset.put!(file: open(temp_filename))
    asset.save!
    self.s3_asset = asset
    save!
  end

  def s3_key
    "lods/#{Rails.env}/#{id}/#{filename}"
  end

  def filename
    "TuneCore_Publishing_Letter_of_Direction-signed.pdf"
  end

  def temp_filename
    LODS_ASEETS_DIR + "/#{id}/TuneCore_Publishing_Letter_of_Direction-signed.pdf"
  end

  def url
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = 3600
    generator.get(SIGNATURE_BUCKET_NAME, s3_asset.key)
  end

  private

  def create_history
    return if last_status.nil?

    LodHistory.create(
      status: last_status, status_at: last_status_at, lod_id: id
    )
  end

  def mark_last_status(status, status_at)
    # RS Date is in PDT, need to convert it back to local timezone
    datetime =
      if status_at.nil?
        Time.now
      elsif status_at.kind_of?(String)
        Time.parse(status_at)
      else
        status_at
      end

    update(
      last_status: status,
      last_status_at: datetime.localtime
    )
  end

  def mark_history(status, status_at, notes = nil)
    LodHistory.create(
      status: status,
      status_at: status_at,
      notes: notes,
      lod_id: id
    )
  end

  def extract_signed_date(rs_document_source)
    # TODO: Probably can extract a RSDocument model that would provide method to access basic metadata
    rs_document_source["recipients"].detect { |a| a["document_role_id"] == "signer_A" }["completed_at"]
  end

  def composer_record
    publishing_composer || composer
  end
end
