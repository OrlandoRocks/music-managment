# frozen_string_literal: true

class PersonEarningsTaxMetadatum < ApplicationRecord
  TAXFORM_ATTRIBUTES = [
    "dist_taxform_id",
    "pub_taxform_id",
    "total_distribution_earnings",
    "total_publishing_earnings"
  ].freeze

  belongs_to :person
  belongs_to :dist_taxform, class_name: "TaxForm"
  belongs_to :pub_taxform,  class_name: "TaxForm"

  validates :person, presence: true

  after_save :update_tax_eligibility, if: :update_tax_eligibility?
  after_save :notify_user_of_tax_block, if: :notify_user_of_tax_block?

  scope :taxform_submitted, -> do
    where.not(dist_taxform: nil).or(where.not(pub_taxform: nil))
  end

  scope :taxform_not_submitted, -> do
    where(dist_taxform: nil, pub_taxform: nil)
  end

  def taxforms?
    dist_taxform.present? ||
      pub_taxform.present?
  end

  def tax_block!
    update!(tax_blocked: true)
  end

  def tax_unblock!
    update!(tax_blocked: false)
  end

  def total_earnings
    [
      total_publishing_earnings,
      total_distribution_earnings,
    ].sum
  end

  def total_taxable_earnings
    [
      total_taxable_publishing_earnings,
      total_taxable_distribution_earnings
    ].sum
  end

  def update_tax_eligibility
    TaxBlocking::UpdatePersonTaxblockStatusService.call(person)
  end

  def update_tax_eligibility?
    FeatureFlipper.show_feature?(:advanced_tax_blocking) &&
      tax_year.eql?(DateTime.current.year) &&
      TAXFORM_ATTRIBUTES.any? { |attr| saved_changes.key?(attr) }
  end

  def notify_user_of_tax_block?
    FeatureFlipper.show_feature?(:advanced_tax_blocking) &&
      tax_year.eql?(DateTime.current.year) &&
      saved_change_to_attribute?(:tax_blocked, to: true)
  end

  def notify_user_of_tax_block
    person.notifications.create(
      text: I18n.t("notifications.tax_form_request_body"),
      title: I18n.t("notifications.tax_form_request_title"),
      url: Rails.application.routes.url_helpers.account_settings_path(
        tab: person.payoneer_account? ? "taxpayer_id" : "payoneer_payout"
      )
    )
  end
end
