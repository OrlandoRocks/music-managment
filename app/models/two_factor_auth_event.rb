class TwoFactorAuthEvent < ApplicationRecord
  self.inheritance_column = nil
  belongs_to :two_factor_auth

  delegate :person, to: :two_factor_auth
  timeout = ENV["TFA_LOGIN_TIMEOUT_IN_SEC"].to_i
  scope :recent_auth_events, -> { where(created_at: timeout.seconds.ago..Time.current, action: "complete") }
  scope :signup_attempts_in_last_day,
        -> {
          where(
            created_at: 1.day.ago..Time.current,
            action: "authentication",
            page: "enrollments",
            type: "authentication"
          )
        }
  scope :resend_attempts_in_last_day,
        -> {
          where(
            created_at: 1.day.ago..Time.current,
            action: "back_stage_code_resend",
            page: "account_settings",
            type: "authentication"
          )
        }
end
