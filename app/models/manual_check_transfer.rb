# Represent transfers that are to be carried out manually and accounted for manually.

class ManualCheckTransfer < CheckTransfer
  attr_accessor :other_state,
                :confirm_password,
                :payment_amount_main_currency,
                :payment_amount_fractional_currency

  scope :pending, -> { includes(:person).where(transfer_status: "pending") }
  scope :processing, -> { includes(:person).where(transfer_status: "processing") }
  scope :completed, -> { includes(:person).where(transfer_status: "completed") }
  scope :failed, -> { includes(:person).where(transfer_status: "failed") }
  scope :cancelled, -> { includes(:person).where(transfer_status: "cancelled") }

  # Can this manual check transfer be converted into an EzpayTransder?

  PERMITTED_SCOPES = [:pending, :processing, :completed, :failed, :cancelled].freeze

  def self.send_scope(scope_type = "pending")
    unless PERMITTED_SCOPES.include?(scope_type.to_s.to_sym)
      raise NoMethodError.new("'#{scope_type}' is not a defined scope")
    end

    send(scope_type)
  end

  def convertible?
    id != nil
  end
end
