class IneligibilityRule < ApplicationRecord
  belongs_to :store

  has_many :applied_ineligibility_rules

  OPERATOR_METHODS = {
    contains: :include?,
    equal: :==,
    not_equal: :!=
  }.with_indifferent_access

  validates :operator, inclusion: { in: OPERATOR_METHODS.keys }

  def matches?(item)
    method = OPERATOR_METHODS[operator]
    item.send(property).to_s.downcase.send(method, value.to_s.downcase)
  end
end
