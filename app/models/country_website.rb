# encoding: utf-8
# frozen_string_literal: true

class CountryWebsite < ApplicationRecord
  scope :with_language_translations, -> { where(id: [UNITED_STATES, GERMANY, FRANCE, ITALY]) }

  # Country Website Constants
  UNITED_STATES = 1
  CANADA        = 2
  UK            = 3
  AUSTRALIA     = 4
  GERMANY       = 5
  FRANCE        = 6
  ITALY         = 7
  INDIA         = 8

  ISO_CODE_MAP = {
    "US" => UNITED_STATES,
    "CA" => CANADA,
    "UK" => UK,
    "AU" => AUSTRALIA,
    "DE" => GERMANY,
    "FR" => FRANCE,
    "IT" => ITALY,
    "IN" => INDIA
  }

  COUNTRY_WEBSITE_ID_TO_LANGUAGE_COUNTRY_MAP = {
    UNITED_STATES => "United States",
    CANADA => "Canada",
    GERMANY => "Deutschland",
    AUSTRALIA => "Australia",
    UK => "United Kingdom",
    INDIA => "India",
    FRANCE => "France",
    ITALY => "Italia",
  }

  COUNTRY_WEBSITE_ID_MAP = {
    "DEFAULT" => UNITED_STATES,
    "US" => UNITED_STATES,
    "CA" => CANADA,
    "GB" => UK,
    "AU" => AUSTRALIA,
    "NZ" => AUSTRALIA,
    "DE" => GERMANY,
    "AT" => GERMANY,
    "FR" => FRANCE,
    "DZ" => FRANCE,
    "BJ" => FRANCE,
    "BF" => FRANCE,
    "BI" => FRANCE,
    "BL" => FRANCE,
    "CD" => FRANCE,
    "CF" => FRANCE,
    "CG" => FRANCE,
    "CI" => FRANCE,
    "CM" => FRANCE,
    "DJ" => FRANCE,
    "GA" => FRANCE,
    "GF" => FRANCE,
    "GG" => FRANCE,
    "GN" => FRANCE,
    "GP" => FRANCE,
    "GQ" => FRANCE,
    "JE" => FRANCE,
    "KM" => FRANCE,
    "MA" => FRANCE,
    "MC" => FRANCE,
    "MG" => FRANCE,
    "ML" => FRANCE,
    "MQ" => FRANCE,
    "MU" => FRANCE,
    "NE" => FRANCE,
    "RE" => FRANCE,
    "RW" => FRANCE,
    "PF" => FRANCE,
    "PM" => FRANCE,
    "SC" => FRANCE,
    "SN" => FRANCE,
    "SX" => FRANCE,
    "TD" => FRANCE,
    "TG" => FRANCE,
    "TN" => FRANCE,
    "WF" => FRANCE,
    "IT" => ITALY,
    "IN" => INDIA
  }.freeze

  US_DOMAIN = "http://www.tunecore.com"
  CA_DOMAIN = "http://www.tunecore.ca"
  UK_DOMAIN = "http://www.tunecore.co.uk"
  AU_DOMAIN = "http://www.tunecore.com.au"
  DE_DOMAIN = "http://www.tunecore.de"
  FR_DOMAIN = "http://www.tunecore.fr"
  IT_DOMAIN = "http://www.tunecore.it"
  IN_DOMAIN = "http://www.tunecore.in"

  LANGUAGE_CODE_MAP = {
    UNITED_STATES => LanguageCode::ENGLISH,
    CANADA => LanguageCode::ENGLISH,
    UK => LanguageCode::ENGLISH,
    AUSTRALIA => LanguageCode::ENGLISH,
    GERMANY => LanguageCode::GERMAN,
    FRANCE => LanguageCode::FRANCE,
    ITALY => LanguageCode::ITALY,
    INDIA => LanguageCode::ENGLISH
  }.freeze

  # For GST purposes, we need country_state and country_state_city for India,
  # in the future we may want additional countries to have country_state and country_state_city
  COUNTRY_STATE_COUNTRIES = ["IN"].map(&:freeze).freeze
  COOKIE_WARNING_COUNTRIES = ["UK", "DE", "FR", "IT", "IN"].freeze

  PAYMENTS_OS_COUNTRIES = ["IN"].freeze

  VAT_CURRENCIES    = ["GBP", "AUD", "CAD", "USD", "INR"].freeze
  EURO_ABBREVIATION = "EUR"

  has_many :people
  has_many :payment_batches
  has_many :products
  has_many :payout_service_fees
  has_many :braintree_transactions
  has_many :eft_batch_transactions
  has_many :eft_batches
  has_many :targeted_offers
  has_many :paypal_transactions
  has_many :stored_paypal_accounts
  has_many :sales_record_masters
  has_many :country_variable_prices
  has_many :cms_sets
  has_many :country_website_languages

  belongs_to :country_record, class_name: "Vendor", foreign_key: "country_id"

  has_and_belongs_to_many :stores

  has_many :disabled_features_country_websites
  has_many :disabled_features,
           through: :disabled_features_country_websites,
           source: :feature

  has_many :foreign_exchange_pegged_rates

  validates :name, :currency, :country, presence: true
  validates :currency, length: { is: 3 }
  validates :name, uniqueness: true

  def self.country_id_for(country)
    COUNTRY_WEBSITE_ID_MAP.fetch(country, COUNTRY_WEBSITE_ID_MAP["DEFAULT"])
  end

  def self.find_for_country_iso_code(country_iso_code)
    find(country_id_for(country_iso_code))
  end

  def is_eur?
    currency == "EUR"
  end

  def india?
    id == CountryWebsite::INDIA
  end

  def self.show_cookie_warning?(country)
    COOKIE_WARNING_COUNTRIES.include?(country)
  end

  # whenever you add a country, you will need to edit those two files with a new processor for that country
  def braintree_processor
    BRAINTREE[id]
  end

  # whenever you add a country, you will need to edit those two files with a new processor for that country
  def paypal_checkout_credentials
    StoredPaypalAccount.checkout_credentials(self)
  end
  deprecate :paypal_checkout_credentials

  def paypal_withdrawal_account
    PaypalTransfer.withdrawal_account(self)
  end

  def url
    COUNTRY_URLS[country]
  end

  def web_url
    WEB_ONLY_URLS[country]
  end

  def feature_enabled?(value)
    # TODO: Should raise exception when value is not a valid feature's symbol
    !disabled_features.find_feature(value)
  end

  def full_country_name
    include_country
    if @country
      @country.name
    else
      (country == "UK") ? "United Kingdom" : "United States"
    end
  end

  def country_iso
    include_country
    if @country
      @country.iso_code
    else
      (country == "UK") ? "GB" : "US"
    end
  end

  def locale
    COUNTRY_LOCALE_MAP[country]
  end

  def bcp47_locale
    BCP_47_LOCALE_MAP.fetch(locale.downcase, BCP_47_LOCALE_MAP[USA_LOCALE])
  end

  def include_country
    @country = Country.find_by(iso_code: country)
  end

  def current_pegged_rate
    foreign_exchange_pegged_rates.order(created_at: :desc).first
  end

  def current_exchange_rate
    ForeignExchangeRate.current_exchange_rate(country_id)
  end

  def non_native_currency_site?
    foreign_exchange_pegged_rates.exists?
  end

  def site_currency
    currency
  end

  def user_currency
    foreign_exchange_pegged_rates.last&.currency || currency
  end

  def country_wesbite_currency_symbol
    MONEY_CONFIG[user_currency][:unit]
  end

  def has_reseller?
    id == INDIA
  end
end
