class Purchase < ApplicationRecord
  include Processable
  include Tunecore::AdminReports::PurchaseReportDataFields
  include GstTaxable
  extend MoneyField

  REMOVE_RELATED_CLASS = [
    Salepoint,
    SalepointSubscription,
    CreditUsage,
    SoundoutReport,
    YoutubeMonetization,
    SubscriptionPurchase,
    PlanAddon
  ]
  ALBUM = "Album".freeze
  CREDIT_USAGE = "CreditUsage".freeze
  PLAN_ADDON = "PlanAddon".freeze
  PRODUCT = "Product".freeze
  SALEPOINT_SUBSCRIPTION = "SalepointSubscription".freeze
  IMMERSIVE_AUDIO = "ImmersiveAudio".freeze
  VAT = "VAT".freeze
  PRORATION = "proration".freeze

  PLAN_PRORATION = "plan proration".freeze
  PRE_PLAN_PRORATION = "pre-plan release proration".freeze
  NON_PLAN_DISCOUNT = "non-plan discount".freeze
  NO_DISCOUNT = "no discount".freeze

  STATUS_FAILED = "failed".freeze

  VALID_DISCOUNT_REASONS = {
    plan_proration: PLAN_PRORATION,
    pre_plan_proration: PRE_PLAN_PRORATION,
    non_plan_discount: NON_PLAN_DISCOUNT,
    no_discount: NO_DISCOUNT
  }
  DEFAULT_DISCOUNT_REASON = VALID_DISCOUNT_REASONS[:no_discount]

  enum discount_reason: VALID_DISCOUNT_REASONS
  money_reader :cost, :discount, :purchase_total, :vat_amount

  belongs_to  :invoice
  belongs_to  :product
  belongs_to  :related, polymorphic: true
  belongs_to  :album,
              -> { where(purchases: { related_type: "Album" }) },
              foreign_key: "related_id"
  belongs_to  :renewal,
              -> { where(purchases: { related_type: "Renewal" }) },
              foreign_key: "related_id"
  belongs_to  :credit_usage,
              -> { where(purchases: { related_type: "CreditUsage" }) },
              foreign_key: "related_id"
  belongs_to  :salepoint,
              -> { where(purchases: { related_type: "Salepoint" }) },
              foreign_key: "related_id"
  belongs_to  :immersive_audio,
              -> { where(purchases: { related_type: "ImmersiveAudio" }) },
              foreign_key: "related_id"
  delegate :album, to: :credit_usage, prefix: true
  belongs_to  :person
  belongs_to  :targeted_product

  has_one     :cert
  has_one :purchase_tax_information, dependent: :destroy

  has_many :price_adjustment_histories, -> { order "created_at DESC" }, dependent: :destroy
  has_many :invoice_logs
  has_many :refund_items, dependent: :destroy
  has_many :plans, through: :product

  before_validation :set_currency
  before_validation :set_no_discount_reason
  # you must include a 'related' object
  validates :related_id, :related_type, presence: true
  validates :product, presence: true
  validates :person, presence: true
  validates :currency, presence: true
  validates :discount_cents, presence: true
  validates :discount_reason, presence: true
  validates :cost_cents, presence: { message: "purchases.errors.issue_adding_to_cart" }

  validate :validate_related_id
  validate :validate_country_for_product_matches_person
  validate :no_cert_and_targeted_product

  # we should only have one 'enlistable' purchase open at any time
  # for a given 'related_to' if that related_to is not a generic product

  # added PlanAddon as the renewal of a PlanAddon is tied to the renewal of a Plan
  # we don't want to keep track of a separate renewal for PlanAddons and we don't
  # want renewals of PlanAddons and Plans to be on separate invoices
  validates :related_id,
            uniqueness: {
              scope: [:related_type],
              if: proc { |model| !%w[Product Renewal PlanAddon].include?(model.related_type) }
            }

  # delegate :tc_approved!, :tc_approved?, :to => :related

  scope :unpaid, -> { where(paid_at: nil) }
  scope :paid, -> { where.not(paid_at: nil) }
  scope :not_in_invoice, -> { where(invoice_id: nil) }
  scope :failed, -> { where(status: STATUS_FAILED) }

  scope :old_trend_reports_for_deletion,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN (
        SELECT MAX(expires_at) AS expires,
               renewal_history.renewal_id,
               renewal_items.related_type
        FROM renewal_history
        INNER JOIN renewal_items ON (
          renewal_items.renewal_id = renewal_history.renewal_id
          AND renewal_items.related_type = 'Entitlement'
        )
      )
      GROUP BY renewal_history.renewal_id) rh
      ON (purchases.related_id=rh.renewal_id AND purchases.related_type = 'Renewal')
          SQL
            .where(["ADDDATE(expires, ?) < NOW()", Renewal::UNPAID_TREND_REPORTS_GRACE_DAYS_BEFORE_DELETION.days])
            .unpaid
        }

  scope :deleted_albums,
        -> {
          joins("INNER JOIN albums ON (purchases.related_id = albums.id AND purchases.related_type = 'Album')")
            .where(albums: { is_deleted: true })
            .unpaid
        }

  scope :active_additional_artists,
        -> {
          joins("INNER JOIN plan_addons ON (purchases.related_id = plan_addons.id AND purchases.related_type = 'PlanAddon')")
            .where(plan_addons: { addon_type: PlanAddon::ADDITIONAL_ARTIST, canceled_at: nil })
        }

  scope :deleted_albums_attached_to_credits,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN credit_usages ON (
        purchases.related_id = credit_usages.id
        AND purchases.person_id = credit_usages.person_id
      )
      INNER JOIN albums ON (
        credit_usages.related_id=albums.id
        AND credit_usages.related_type = 'Album'
      )
          SQL
            .where(albums: { is_deleted: true })
            .unpaid
        }
  scope :plan_purchases, -> { where(product_id: Product::PLAN_PRODUCT_IDS) }

  scope :plans, -> { where(product_id: Product::PLAN_PRODUCT_IDS) }

  scope :plan_related, -> {
    where(product_id: Product::ADDITIONAL_ARTIST_PRODUCT_IDS + Product::PLAN_PRODUCT_IDS)
  }

  scope :current_plan, ->(invoice_id) { plans.paid.where(invoice_id: invoice_id) }

  scope :automator, -> { where(related_type: SalepointSubscription.name) }

  scope :with_store_salepoints, ->(via = "Album") do
    case via
    when "CreditUsage"
      paid.joins(credit_usage: { album: { salepoints: :store } })
    when "Salepoint"
      paid.joins(salepoint: :store)
    else
      paid.joins(album: { salepoints: :store })
    end
  end

  after_commit :sync_believe_person

  def self.related_type_filter(type)
    widget_ids = Product.where(applies_to_product: "Widget").map(&:id)
    if type.is_a?(Array)
      where("related_type in (?) and product_id not in (?)", type, widget_ids)
    else
      where("related_type = ? and product_id not in (?)", type, widget_ids)
    end
  end

  def self.related_type_exlude(types)
    where("related_type not in (?)", types)
  end

  def validate_related_id
    return unless related && !related.is_a?(Product)

    related_person_id =
      case related
      when Composer, PublishingComposer
        related.publishing_administrator.id
      when Salepoint
        related.salepointable.person_id
      when PreorderPurchase
        related.album.person_id
      when SalepointPreorderData
        related.salepoint.salepointable.person_id
      when ImmersiveAudio
        related.person.id
      else
        related.person_id
      end

    return if related_person_id == person_id

    errors.add(:base, I18n.t("models.billing.the_person_id_for_the_related_and_the_purchase_must_be_the_same"))
  end

  # Caution!! Creates a single sql statement to
  # create a batch a purchases. With this, validations will
  # not be ran on records created.
  #
  # Assumes items all of same class
  #
  # This method should be re-evaluated if purchase create process ever
  # changes. (i.e. new validations or pricing)
  def self.create_batch_purchases(person, items)
    carted_ids = []
    cart_up_values = []

    product = items.blank? ? nil : Product.find_ad_hoc_product(items.first, person)
    if product
      purchase = Purchase.new(person: person, product: product, related: items.first)
      price_calc = purchase.price_calculator(true)

      purchase_cents = price_calc.purchase_cost_cents
      discount_cents, discount_reason = price_calc.purchase_discount_cents_and_reason

      targeted_product = TargetedProduct.targeted_product_for_active_offer(person, product)
      targeted_product_id = targeted_product ? targeted_product.id : "NULL"

      currency = person.currency

      related_type = items.first.class.base_class.name if items.present?

      items.each do |item|
        cart_up_values << "(#{person.id}, #{product.id}, #{item.id}, '#{related_type}', #{purchase_cents}, #{discount_cents},  '#{discount_reason}', #{targeted_product_id}, '#{currency}', NULL, CURRENT_TIMESTAMP)"
        carted_ids << item.id
      end

      if cart_up_values.present?
        sql = "INSERT INTO purchases (`person_id`, `product_id`, `related_id`, `related_type`, `cost_cents`, `discount_cents`, `discount_reason`, `targeted_product_id`, `currency`, `paid_at`, `created_at`) VALUES #{cart_up_values.join(', ')}"
        ActiveRecord::Base.connection.execute sql
      end
    end

    # update vat amount
    TcVat::BulkVatCalculator.new(person).calculate!(
      person.purchases.unpaid.where(related_id: carted_ids).pluck(:id)
    )
    carted_ids
  end

  def self.purchase_for(person, item, paid = true)
    item_type = item.class.base_class.name

    if paid == true
      Purchase.paid.where(
        [
          "person_id = ? " \
          "AND purchases.related_id = ? " \
          "AND purchases.related_type = ?",
          person.id,
          item.id,
          item_type
        ]
      ).first
    else
      Purchase.unpaid.where(
        [
          "person_id = ? " \
          "AND purchases.related_id = ? " \
          "AND purchases.related_type = ?",
          person.id,
          item.id,
          item_type
        ]
      ).first
    end
  end

  def self.process_purchases_for_invoice(invoice)
    mark_as_paid_for_invoice(invoice)
    purchases = invoice.purchases
                       .paid
                       .includes(:related, product: { product_items: :product_item_rules })

    # update all the targeted people for this person
    # that are being used in the collection of purchases
    Purchase.mark_targeted_offers_as_used(invoice)

    Product.process_purchase(invoice.person, purchases)
    message = "update all the targeted people for this person that are being used in the collection of purchases"
    options = {
      person: invoice&.person,
      invoice: invoice,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
  end

  def self.cart_items_count(person)
    unpaid
      .joins("LEFT JOIN invoices i ON purchases.invoice_id = i.id LEFT JOIN invoice_settlements ivs ON ivs.invoice_id = i.id")
      .where(person_id: person.id)
      .where("purchases.invoice_id IS NULL OR (i.batch_status = 'visible_to_customer' AND ivs.id IS NULL)")
      .count
  end

  def self.cart_item_in_cart?(person, item, type)
    Purchase.where("person_id = ? AND purchases.related_id = ? AND purchases.related_type = ?", person.id, item.id, type).first
  end

  # Determines if an item is currently in cart and not
  # partially paid
  def self.currently_in_cart?(person, item)
    type = item.class.base_class.name
    carted = person.purchases.unpaid.not_in_invoice.where("related_id = ? AND related_type = ?", item.id, type).first.present?

    # Check if item has a credit usage associated with it
    # and if it's in cart
    unless carted
      credit = CreditUsage.credit_usage_for(person, item)
      carted = (credit.purchase && !credit.purchase.paid?) if credit
    end

    carted
  end

  # 2010/03/30 - Replaces outstanding_of
  def self.recalculate_or_create(person, product, related_item)
    purchase = Purchase.purchase_for(person, related_item, false)

    if purchase.nil?
      purchase = Purchase.create(person: person, product: product, related: related_item)
    else
      purchase.product = product
    end

    purchase.recalculate
    purchase
  end

  def self.destroy_by_purchase_item(person, item)
    credit = CreditUsage.credit_usage_for(person, item)

    # if there is both an existing credit_usage and album purchase, they both need to be destroyed
    # This scenario is a result of a carted freemium album followed by the addition of paid stores prior to completing checkout
    if credit && item.class.base_class.name == "Album"
      purchases = Purchase.where(person: person, related: [item, credit]).unpaid
      purchases.each(&:destroy)
    else
      item = credit if credit
      purchase = Purchase.where(person_id: person.id, related: item).unpaid.first
      purchase.destroy if purchase
    end
  end

  def self.detach_purchases(invoice)
    Purchase.where("invoice_id = ?", invoice.id).update_all(invoice_id: nil, status: "failed")
  end

  #
  # Orders purchases by priority of items.  This is used
  # to optimize the usage of a person's balance when renewing
  #
  def self.prioritize_purchases(purchases)
    purchases.sort do |purchase1, purchase2|
      if purchase1.product.applies_to_product == "Album"
        -1
      elsif purchase2.product.applies_to_product == "Album"
        1
      elsif purchase1.product.applies_to_product == "Single"
        -1
      elsif purchase2.product.applies_to_product == "Single"
        1
      elsif purchase1.product.applies_to_product == "Ringtone"
        -1
      elsif purchase2.product.applies_to_product == "Ringtone"
        1
      else
        0
      end
    end
  end

  def self.open_composer_purchase(person)
    songwriter_service_product = Product::PRODUCT_COUNTRY_MAP[person.country_domain][:songwriter_service]&.first
    return unless songwriter_service_product

    person.purchases.where("paid_at IS NULL and product_id = #{songwriter_service_product}").first
  end

  def destroy
    remove_related = false

    raise "Cannot delete a purchased product" unless unpaid?

    related.remove_from_cart_pre_proc if related && related.respond_to?(:remove_from_cart_pre_proc)

    remove_related = true if REMOVE_RELATED_CLASS.include?(related.class)

    cert.disassociate if cert

    destroyed_result = super

    related.destroy if remove_related

    destroyed_result
  end

  #  Create an item to be used in Inventory.use!
  #  for this purchase
  #
  def item_to_use
    if related.respond_to?(:inventory_use_options)
      related.inventory_use_options
    elsif use_in_inventory?
      {
        product: product,
        purchase: self,
        item_to_use: related
      }
    else
      nil # must return nil, not result of boolean expression
    end
  end

  #
  #  determine if
  #
  def use_in_inventory?
    !["Renewal", "Product", "SalepointSubscription"].include?(related_type)
  end

  def renewal
    related.is_a?(Renewal) ? related : nil
  end

  def recalculate
    return self unless price_adjustment_histories_count.zero?

    price_calculator(true) # clear it out to start fresh

    if cert.blank?
      product.set_targeted_product(TargetedProduct.targeted_product_for_active_offer(person, product, related))
      targeted_product = product.current_targeted_product
    end
    temp_cost_cents = price_calculator.purchase_cost_cents
    purchase_discount_cents, purchase_discount_reason = price_calculator.purchase_discount_cents_and_reason
    unless temp_cost_cents
      Rails.logger.info("Unable to add product #{product.id} for person #{person.id} due to nil cost_cents")
    end
    assign_attributes(
      cost_cents: temp_cost_cents,
      discount_cents: purchase_discount_cents,
      discount_reason: purchase_discount_reason,
      targeted_product: product.current_targeted_product
    )
    TcVat::Calculator.new(self).perform!(temp_cost_cents, purchase_discount_cents)
  end

  def attach_to_invoice(invoice_to_attach_to)
    raise ArgumentError, "purchase #{id} is already attached to invoice: #{invoice_id}" if invoice_id?

    self.invoice_id = invoice_to_attach_to.id
    save!
  end

  # is there an attached invoice object?
  def invoice?
    !!(invoice_id? && invoice)
  end

  def paid?
    !unpaid?
  end

  def unpaid?
    paid_at.nil? || paid_at == ""
  end

  def failed?
    status == STATUS_FAILED
  end

  def allow_new_cert?
    !paid? && price_calculator.allow_new_cert?
  end

  # returns the price calculator (it's a pricing strategy) for this instance
  def price_calculator(force = false)
    @price_calculator = Tunecore::PriceCalculator.of(self, related) if @price_calculator.nil? || force
    @price_calculator
  end

  def purchase_total_cents
    [cost_cents.to_i - discount_cents.to_i + vat_amount_cents.to_i, 0].max
  end

  def sub_total_excl_vat
    [cost_cents.to_i - discount_cents.to_i, 0].max
  end

  def discount_details
    price_calculator.purchase_discount_details
  end

  delegate :has_discounts?, to: :price_calculator

  # Returns the item purchased
  def purchased_item
    related
  end

  # returns the name of the product in question, e.g. if it is an album, it returns the title
  # with the addition of renewals and entitlements, not everything has the same path to a name
  def purchased_name
    case related.class.name
    when "Album", "Single", "Ringtone", "MusicVideo", "FeatureFilm" then related.name
    when "Salepoint" then "Added Store: " + related.store.name
    when "Renewal"
      case renewal.renewal_items.first.related_type
      when "Album"        then renewal.renewal_items.first.related.name
      when "Entitlement"  then renewal.renewal_items.first.related.entitlement_rights_group.name
      when "MusicVideo"        then renewal.renewal_items.first.related_type
      when "FeatureFilm" then renewal.renewal_items.first.related_type

      else nil
      end
    else nil
    end
  end

  # returns the artist of the product in question, e.g. if it is an album, it returns the artist that made that album
  # with the addition of renewals and entitlements, not everything has the same path to an artist, and not every item has an artist
  def purchased_artist
    case purchased_item.class.name
    when "Album", "Single", "Ringtone", "MusicVideo" then purchased_item.artist_name
    when "FeatureFilm" then purchased_item.director
    when "Renewal"
      case renewal.renewal_items.first.related_type
      when "Album", "MusicVideo" then renewal.renewal_items.first.related.artist_name
      when "FeatureFilm" then renewal.renewal_items.first.related.director
      else nil
      end
    else nil
    end
  end

  def adjust_price(new_price, admin_note, created_by)
    check_for_errors_on_price_adjustment(new_price, admin_note)

    return unless errors.empty?

    new_price         = 0 if new_price.negative?
    original_price    = (cost_cents / 100.00)
    original_discount = (discount_cents / 100.00)
    new_discount      = 0

    # Keep a true log of the price adjustment happening.
    PriceAdjustmentHistory.create!(purchase: self, admin: created_by, note: admin_note, original_cost_cents: cost_cents, original_discount_cents: discount_cents, new_cost_cents: new_price);

    update(
      cost_cents: new_price,
      discount_cents: new_discount,
      price_adjustment_histories_count: price_adjustment_histories_count + 1,
      vat_amount_cents: adjustment_vat(new_price, new_discount)
    )

    Note.create(note_created_by: created_by, related: person, subject: "Price Adjustment - Purchase: #{id}", note: admin_note + "\n#{create_adjustment_note_text(new_price, original_price, original_discount)}")
  end

  def adjustment_vat(new_price, new_discount)
    tax_rate = purchase_tax_information&.tax_rate.to_f

    ((new_price - new_discount) * tax_rate / 100.0).round
  end

  def self.mark_targeted_offers_as_used(invoice)
    targeted_offer_ids = invoice.purchases.map { |p| p.targeted_product.try(:targeted_offer_id) }.compact

    return unless targeted_offer_ids.any?

    targeted_people = invoice.person.targeted_people.where("targeted_offer_id in (?)", targeted_offer_ids)
    targeted_people.each(&:use!)
  end

  def credited?
    related_type == CREDIT_USAGE
  end

  def immersive_audio?
    related_type == IMMERSIVE_AUDIO
  end

  def store_automator?
    related_type == SALEPOINT_SUBSCRIPTION
  end

  def purchased_release
    return if !credited? && !related.try(:release?)
    return related.related if credited?

    related if related.try(:release?)
  end

  def paid_release?
    release = purchased_release
    return false unless release

    !Product.free_release?(related)
  end

  def self.releases_in_cart(person)
    person
      .albums
      .in_cart
  end

  def self.albums_in_cart(person)
    releases_in_cart(person).where(["album_type != ?", "Ringtone"])
  end

  def is_social?
    related_type == "SubscriptionPurchase" && related.name == "Social"
  end

  def gst_tax_amount
    tax_amount(invoice.gst_config, purchase_total)
  end

  def purchase_total_without_tax
    base_amount(invoice.gst_config, purchase_total)
  end

  def price_before_gst(price)
    gst_tax_amount.to_h.each_value do |v|
      price -= v
    end
    price
  end

  def cents_greater_than_zero?
    purchase_total_cents.positive?
  end

  def item_for_paypal
    {
      qty: 1,
      amt: (cost_cents.to_f - discount_cents.to_f + vat_amount_cents.to_f) / 100,
      name: product.display_name
    }
  end

  def has_vat_info?
    purchase_tax_information.present?
  end

  def vat_applicable?
    purchase_tax_information&.tax_type == VAT
  end

  def need_sns_notification?
    ["Salepoint", "SalepointSubscription"].include?(related_type)
  end

  def related_album_id
    case related.class.name
    when "Salepoint" then related.salepointable_id
    when "SalepointSubscription" then related.album_id
    end
  end

  def related_store_id
    case related.class.name
    when "Salepoint" then related.store_id
    else nil
    end
  end

  def self.mark_as_paid_for_invoice(invoice)
    where(invoice: invoice).update_all(paid_at: Time.current)
  end

  def refunded_amount_cents
    refund_items.joins(:refund)
                .where(refunds: { status: "success" })
                .sum(&:total_amount_cents)
  end

  def refundable_amount_cents
    purchase_total_cents - refunded_amount_cents
  end

  def refundable?
    refundable_amount_cents.positive?
  end

  def plan_addon?
    related_type == PLAN_ADDON
  end

  def plan_credit?
    related.try(:plan_credit) == true
  end

  def plan_renewal?
    renewal? && product.try(:plan).present?
  end

  def additional_artist_purchaseable_plan_renewal?
    renewal? && product.try(:plan).present? && Plans::CanDoService.can_do?(product.plan, :buy_additional_artists)
  end

  delegate :splits_collaborator?,
           :additional_artist?,
           :is_plan?,
           :plan_related?,
           :is_free_automator_product?,
           :paid_plan?,
           to: :product

  def plan
    return if product.plan.blank?

    product.plan.first
  end

  def free_with_plan?
    credited? && plan_credit?
  end

  def plan_upgrade?
    person.has_plan? && is_plan? && !plan_renewal?
  end

  def discount_cents_positive?
    discount_cents.present? && discount_cents.positive?
  end

  def additional_artist_product_id?
    product_id.in?(Product::ADDITIONAL_ARTIST_PRODUCT_IDS)
  end

  def plan_product_id?
    product_id.in?(Product::PLAN_PRODUCT_IDS)
  end

  def plan_related_product_id?
    product_id.in?(Product::PLAN_RELATED_PRODUCT_IDS)
  end

  def active_plan?
    product.plan == person.plan
  end

  def renewal?
    related_type == Renewal.name
  end

  def proration_discounted?
    discount_reason&.include?(PRORATION) || false
  end

  private

  def set_currency
    self.currency = person.currency
  end

  def set_no_discount_reason
    self.discount_reason = NO_DISCOUNT unless discount_cents_positive?
  end

  def create_adjustment_note_text(new_price, original_price, original_discount)
    distribution_name = (related_type == "Album") ? "\nDistribution: #{related.name}" : ""
    applies = (product.applies_to_product != "None") ? " - " + product.applies_to_product : ""
    "#{distribution_name}\nProduct: #{product.name} #{applies}\nNew Price: #{new_price / 100.00}\nOriginal Price: #{original_price}\nOriginal Discount: #{original_discount}"
  end

  def check_for_errors_on_price_adjustment(new_price, admin_note)
    errors.add(:base, I18n.t("models.billing.enter_a_valid_price")) if new_price.nil?

    errors.add(:base, I18n.t("models.billing.admin_not_required")) if admin_note.blank?

    errors.add(:base, I18n.t("models.billing.you_cannot_adjust_the_price")) if paid?
  end

  def validate_country_for_product_matches_person
    return unless product && person.country_website_id != product.country_website_id

    errors.add(:base, I18n.t("models.billing.the_product_you_tried_to_purchase"))
  end

  def no_cert_and_targeted_product
    return unless cert && targeted_product

    errors.add(:base, I18n.t("models.billing.you_cannot_apply_a_targeted_offer_and_a_promotional_code_at_the_same_time"))
  end

  def sync_believe_person
    return unless related_type == "Composer" && paid_at && related

    if DEFER_BELIEVE
      Believe::PersonWorker.perform_async(related.publishing_administrator.id)
    else
      Believe::PersonWorker.perform(related.publishing_administrator.id)
    end
  end
end
