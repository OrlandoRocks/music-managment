class MetaTag < ApplicationRecord
  belongs_to :country_website
  belongs_to :person

  validates :page_name, :name, :country_website_id, :person_id, presence: true

  def self.meta_tags
    ["description", "keywords"]
  end

  def self.valid_tags
    meta_tags + ["canonical", "title"]
  end

  def self.distinct_page_names
    pluck(:page_name).uniq
  end
end
