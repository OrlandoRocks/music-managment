class ReportingDate < ApplicationRecord
  has_one  :reporting_dashboard_rollup
  has_many :reporting_dashboard_product
end
