class TransactionErrorAdjustment < ApplicationRecord
  belongs_to :person, inverse_of: :nullify
end
