class SoundoutReportNotification < Notification
  validates :notification_item, presence: true
  validates :notification_item_type, inclusion: { in: %w[SoundoutReport] }

  def setup_system_notification
    set_url
    set_title
    set_text
    set_link_text
    set_image_url

    self.person = notification_item.person if notification_item
  end

  private

  def set_url
    self.url = "/fanreviews/#{notification_item_id}/market_potential"
  end

  def set_title
    self.title = "Fan Reviews Report Available"
  end

  def set_text
    self.text = "Your TuneCore Fan Reviews report for #{notification_item.track.name} is now available! Read what others have to say about your song."
  end

  def set_image_url
    self.image_url = nil
  end

  def set_link_text
    self.link_text = "See your report."
  end
end
