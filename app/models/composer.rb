class Composer < ApplicationRecord
  include Tunecore::Publishing::SummaryReport
  include PublishingAdministrationHelper
  include PrimaryComposerable

  has_paper_trail on: [:update], only: [:cae]

  # These are the PROs for songwriters
  SONGWRITER_PROS = %w[
    ABRAMUS
    AEPI
    AKM
    APRA
    ASCAP
    BMI
    BUMA
    GEMA
    IMRO
    JACAP
    JASRAC
    KODA
    PRS
    RAO
    SABAM
    SACEM
    SACM
    SADAIC
    SAMRO
    SESAC
    SGAE
    SIAE
    SOCAN
    SPA
    STIM
    SUISA
    TONO
  ].freeze

  PUBLISHER_ADMIN = {
    BMI: "TuneCore Digital Music (BMI)",
    ASCAP: "TuneCore Publishing (ASCAP)",
    SESAC: "TuneCore Songs (SESAC)"
  }.with_indifferent_access.freeze

  belongs_to :account
  belongs_to :person
  belongs_to :publisher
  belongs_to :publishing_role
  belongs_to :performing_rights_organization
  has_many :publishing_splits
  has_many :cowriters
  has_many :compositions, through: :publishing_splits
  has_many :notes, -> { order(created_at: :desc) }, as: :related
  has_many :related_purchases, as: :related, class_name: "Purchase"
  has_many :rights_app_errors, as: :requestable
  has_many :royalty_payments
  has_many :non_tunecore_albums
  has_many :non_tunecore_songs, through: :non_tunecore_albums
  has_many :legal_documents, as: :subject, class_name: "LegalDocument"
  has_one :terminated_composer

  belongs_to :lod
  has_one :tax_info, dependent: :destroy
  has_one :terminated_composer
  has_one :paper_agreement, as: :related

  before_validation :clean_cae, if: proc { |record| record.cae.present? }

  validates :first_name, :last_name, presence: true

  # Terms and Conditions check
  # TODO: super confusing :(
  # rubocop:disable Style/NumericPredicate
  validate do |record|
    if has_rights_app?
      true
    elsif ((record.agreed_to_terms == 0) || !record.agreed_to_terms)
      if new_record?
        record.errors.add("agreed_to_terms", I18n.t("models.publishing.please_agree_to_the_terms_and_conditions"))
      end
    end
  end
  # rubocop:enable Style/NumericPredicate

  validates :first_name,  length: { maximum: 45 }
  validates :last_name,   length: { maximum: 45 }
  validates :middle_name, length: { maximum: 100, allow_nil: true }
  validates :cae,         length: { in: 9..11, allow_blank: true, unless: :skip_cae_validation }

  validates :email,
            format: {
              with: /\A([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})\z/,
              if: proc { |record| record.email.present? }
            }
  validates :alternate_email,
            format: {
              with: /\A([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})\z/,
              if: proc { |record| record.alternate_email.present? }
            }

  validates :cae, numericality: { allow_blank: true }

  validate :does_not_have_fully_terminated_composers, on: :save
  validates_with Utf8mb3Validator, fields: %i[first_name last_name]

  attr_accessor :agreed_to_terms, :skip_cae_validation

  accepts_nested_attributes_for :publisher

  after_create :create_unallocated_sum_composition
  after_create :assign_publishing_role, unless: :has_rights_app?

  scope :albums_with_splits_count,
        -> {
          select("composers.*, COUNT(distinct albums.id) AS albums_count")
            .joins(publishing_splits: [composition: [songs: :album]])
            .order("composers.id")
            .group("composers.id")
        }

  scope :albums_finalized_count,
        -> {
          select("composers.*, count(distinct albums.id) as albums_count")
            .joins(person: :albums)
            .where("albums.finalized_at IS NOT NULL AND albums.takedown_at IS NULL AND albums.deleted_date IS NULL")
            .order("composers.id")
            .group("composers.id")
        }

  scope :splits_last_updated_at,
        -> {
          select("composers.*, max(publishing_splits.updated_at)  as last_updated_at")
            .joins("STRAIGHT_JOIN publishing_splits on publishing_splits.composer_id = composers.id STRAIGHT_JOIN compositions ON compositions.id = publishing_splits.composition_id")
            .where(compositions: { is_unallocated_sum: 0 })
            .order("composers.id")
            .group("composers.id")
        }
  scope :is_paid, -> { joins(:related_purchases).where.not(purchases: { paid_at: nil }) }
  scope :with_no_provider_identifier, -> { joins(:account).where(accounts: { provider_account_id: nil }) }
  scope :purchased_pub_admin, -> { is_paid.where(purchases: { related_type: "Composer", product_id: Product.publishing_admin_product_ids }) }
  scope :with_lods, -> { joins("left outer join lods on composers.lod_id = lods.id").includes(:lod) }

  default_scope { includes(:tax_info) }

  delegate :completed_w9?, :tax_id, :submitted_tax_id?, :completed_w8ben?, to: :tax_info, allow_nil: true

  def self.with_no_unallocated_sum_composition
    Composer.find_by_sql(
      "SELECT composers.* FROM `composers`
      LEFT OUTER JOIN
      (select publishing_splits.composer_id, compositions.name, compositions.is_unallocated_sum
      from publishing_splits INNER JOIN compositions
      ON publishing_splits.composition_id = compositions.id
      where is_unallocated_sum = 1) compositions_splits
      ON composers.id = compositions_splits.composer_id
      where compositions_splits.composer_id is NULL"
    )
  end

  def self.songwriter_pros
    SONGWRITER_PROS
  end

  def clean_cae
    # Ignore first character (can be any alphanumeric character)
    # Clean (remove space, dash, symbols, alpha characters) after the first character
    cae.strip!
    self.cae = (cae[0..0] + cae[1..cae.size].gsub(/[^0-9]/, "")) if cae.present? && cae.size > 1
  end

  def full_name_affixed
    [name_prefix, first_name, middle_name, last_name, name_suffix]
      .compact.map(&:strip).reject(&:empty?).join(" ")
  end

  def full_name
    middle_name.present? ? "#{first_name} #{middle_name} #{last_name}" : name
  end

  def name
    "#{first_name} #{last_name}"
  end

  def understandable_error_messages
    # Overriding errors method so attributes name will make sense for customers
    list_of_messages = []
    errors.each do |attr, msg|
      list_of_messages << case attr.to_s
                          when "dob"
                            I18n.t("model.composer.date_of_birth", msg: msg)
                          when "phone"
                            I18n.t("model.composer.phone_number", msg: msg)
                          when "alternate_phone"
                            I18n.t("model.composer.mobile_number", msg: msg)
                          when "agreed_to_terms"
                            msg.to_s
                          when "cae"
                            I18n.t("model.composer.songwriter_cae_ipn_number", msg: msg)
                          when "performing_rights_organization_id"
                            I18n.t("model.composer.songwriter_affiliation", msg: msg)
                          else
                            "#{attr.to_s.humanize} #{msg}"
                          end
    end
    list_of_messages
  end

  def is_paid
    related_purchases.any?(&:paid_at)
  end

  def paid_at
    @paid_purchase ||= related_purchases.detect { |p| p.paid_at != nil }
    @paid_purchase ? @paid_purchase.paid_at : nil
  end

  def to_s
    full_name
  end

  def selected_splits?
    Composer.selected_splits_count(self).positive?
  end

  def albums_with_splits_count
    composers = Composer.albums_with_splits_count.where(id: id)
    composers.blank? ? 0 : composers[0].albums_count
  end

  def albums_finalized_count
    composers = Composer.albums_finalized_count.where(id: id)
    composers.blank? ? 0 : composers[0].albums_count
  end

  def splits_last_updated_at
    composers = Composer.splits_last_updated_at.where(id: id)
    composers.blank? ? nil : Time.parse(composers[0].last_updated_at)
  end

  def lod_status
    return unless lod

    { status: lod.status_text, date: lod.last_status_at }
  end

  def publishing_administrator
    account.try(:person) || person
  end

  def publisher_admin_name
    PUBLISHER_ADMIN[publisher.performing_rights_organization.name] || PUBLISHER_ADMIN["BMI"]
  end

  def publisher_entity_name
    return "" unless has_publisher?

    publisher.name_with_pro
  end

  def has_publisher?
    publisher.present? && publisher.name.present?
  end

  def pro
    performing_rights_organization
  end

  def has_pro_songwriter_affiliation?
    pro.present?
  end

  def does_not_have_fully_terminated_composers
    errors.add(:base, "cannot edit terminated composers") if has_fully_terminated_composers?
  end

  def has_fully_terminated_composers?
    terminated_composer.present? && terminated_composer.fully_terminated?
  end

  def account_composer
    Composer.where(person_id: publishing_administrator.id).first
  end

  def process_paid
    send_bmi_registration_reminder

    return unless has_rights_app?

    send_to_publishing_administration
    PublishingAdministration::CompositionCreationWorker.perform_async(id)
  end

  def royalty_pdf_link
    royalty_payments.last.pdf_summary.expiring_url(600) if royalty_payments.present?
  end

  def royalty_csv_link
    royalty_payments.last.csv_summary.expiring_url(600) if royalty_payments.present?
  end

  def current_email
    email.presence || publishing_administrator.email
  end

  def tax_name
    if tax_info
      tax_info.is_entity? ? tax_info.entity_name.to_s : tax_info.name.to_s
    else
      name.to_s
    end
  end

  def self.missing_splits?(person)
    return false unless person.paid_for_composer?

    Composer.find_by_sql(
      [
        "select composers.id from composers
                          inner join people on composers.person_id = people.id
                          inner join albums on people.id = albums.person_id
                          inner join songs on albums.id = songs.album_id
                          left join compositions on songs.composition_id = compositions.id
                          where people.id = ? and (albums.is_deleted = false and albums.takedown_at is null and
                                (compositions.id is null or COALESCE(compositions.state,'') IN ('new','')))
                          limit 1",
        person.id
      ]
    ).present?
  end

  def terminate_pub_admin
    update_columns(terminated_at: Date.today)
  end

  def reactivate_pub_admin
    update_columns(terminated_at: nil)
  end

  def has_rights_app?
    rights_app_enabled?(publishing_administrator) if publishing_administrator.present?
  end

  def self.active_composers(person)
    return unless person.account.present? && person.account.composers.any?

    person.account.composers.select(&:is_active?)
  end

  def format_paid_at
    paid_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def format_provider_identifier
    provider_identifier || ""
  end

  def format_registration_date
    agreed_to_terms_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def format_termination_date
    date = terminated_composer.try(:effective_date) || terminated_at
    date.try(:to_date).try(:strftime, "%m/%d/%Y")
  end

  def format_sync_opted_updated_at
    sync_opted_updated_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def find_or_create_terminated_composer(termination_type)
    terminated_composer || TerminatedComposer.new(composer_id: id, termination_type: termination_type)
  end

  def self.selected_splits_count(composer)
    joins(:compositions)
      .where(id: composer.id)
      .where(compositions: { is_unallocated_sum: false })
      .count
  end

  def letter_of_direction
    legal_documents.where(name: DocumentTemplate::LOD).last
  end

  def valid_letter_of_direction?
    lod = letter_of_direction
    lod ? lod.signed_at.present? : true
  end

  def create_or_update_publisher(publisher_params)
    if publisher.present?
      has_publisher_values = publisher_params.values.any?(&:present?)
      has_publisher_values ? publisher.update(publisher_params) : publisher.destroy
    else
      publisher = Publisher.new(publisher_params)
      update(publisher: publisher)
    end
  end

  def is_active?
    PublishingAdministration::ComposerStatusService.new(self).active?
  end

  def paid_for?
    PublishingAdministration::ComposerStatusService.new(self).paid_for?
  end

  private

  def create_unallocated_sum_composition
    begin
      publishing_splits.create!(percent: 100, composition: Composition.create!(name: unallocated_sums_composition_name, is_unallocated_sum: true))
    rescue StandardError => e
      logger.error "Error in creating unallocated sums composition: #{e.message}"
    end
  end

  def unallocated_sums_composition_name
    if publisher
      publisher.name
    else
      name
    end
  end

  def send_bmi_registration_reminder
    return if has_pro_songwriter_affiliation?

    person = publishing_administrator

    MailerWorker.perform_async(
      "PublishingNotifier",
      :bmi_registration_reminder,
      id
    )

    Note.create(
      note: "BMI Registration Reminder sent to #{name}(#{person.email})",
      note_created_by_id: 0,
      related: person,
      subject: "Publishing Email"
    )
  end

  def send_to_publishing_administration
    return unless paid_for?

    if account.provider_account_id.nil?
      PublishingAdministration::ArtistAccountWriterWorker.perform_async(id)
    else
      PublishingAdministration::WriterCreationWorker.perform_async(id)
    end
  end

  def assign_publishing_role
    update(publishing_role_id: PublishingRole::ON_BEHALF_OF_SELF_ID) if publishing_role.nil?
  end
end
