class ExternalServicesPerson < ApplicationRecord
  #
  # Associations
  #
  attr_accessor :social_login

  belongs_to    :person
  belongs_to    :external_service

  #
  # Validations
  #

  validates   :person, presence: true
  validates   :external_service, presence: true
  validates   :account_id, presence: { unless: :social_login }
  validate :external_service_valid

  private

  def external_service_valid
    begin
      ExternalService.find(external_service_id)
    rescue ActiveRecord::RecordNotFound
      false
    end
  end
end
