class ReleaseSubscription
  attr_accessor :album_id, :subscription_action

  def initialize(params = {})
    @album_id = params[:album_id]
    @subscription_action = params[:subscription_action]
    @options = params[:options]
  end

  def process_action
    return false unless is_valid?

    send("process_#{subscription_action}".to_sym)
  end

  def is_valid?
    ["takedown", "cancel"].include?(@subscription_action)
  end

  private

  def process_cancel
    album = Album.find(@album_id)
    album.cancel_renewal
  rescue ActiveRecord::RecordNotSaved
    false
  end

  def process_takedown
    album = Album.find(@album_id)
    album.takedown!(@options)
  end
end
