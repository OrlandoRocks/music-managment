# frozen_string_literal: true

class IRSTaxWithholding < ApplicationRecord
  belongs_to :irs_transaction
  belongs_to :withheld_from_person_transaction,
             class_name: "PersonTransaction"
  belongs_to :foreign_exchange_rate

  validates :withheld_amount, numericality: { greater_than: 0.0 }

  scope :unreported, -> { where(irs_transaction_id: nil) }
  scope :for_irs_transaction, ->(irs_txn_id) { where(irs_transaction_id: irs_txn_id) }
  scope :with_people_and_transactions, -> { joins(withheld_from_person_transaction: :person) }
end
