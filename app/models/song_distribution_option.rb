class SongDistributionOption < ApplicationRecord
  belongs_to :song

  has_paper_trail

  OPTIONS = {
    available_for_streaming: true,
    free_song: false,
    album_only: false
  }

  VALUES = ["1", "true", true]

  def self.all_options
    OPTIONS
  end
end
