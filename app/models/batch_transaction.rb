class BatchTransaction < ApplicationRecord
  include Logging
  include BraintreePaymentSplit
  include AdyenSecretsHelper

  belongs_to :invoice
  belongs_to :payment_batch
  belongs_to :stored_credit_card
  belongs_to :matching, polymorphic: true
  has_one :person_transaction, as: :target

  has_many :invoice_logs

  validates :invoice_id, :payment_batch_id, :amount, :currency, presence: true

  scope :successful_braintree_transactions,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN braintree_transactions ON matching_type = 'BraintreeTransaction'
        AND matching_id = braintree_transactions.id
          SQL
            .where(braintree_transactions: { status: "success" })
        }

  scope :successful_payments_os_transactions,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN payments_os_transactions ON matching_type = 'PaymentsOSTransaction'
        AND matching_id = payments_os_transactions.id
          SQL
            .where(payments_os_transactions: { charge_status: "Succeed" })
        }

  scope :successful_paypal_transactions,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN paypal_transactions ON matching_type = 'PaypalTransaction'
        AND matching_id=paypal_transactions.id
          SQL
            .where(paypal_transactions: { status: "Completed" })
        }
  scope :successful_adyen_transactions,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN adyen_transactions ON matching_type = 'AdyenTransaction'
       AND matching_id=adyen_transactions.id
          SQL
            .where(adyen_transactions: { result_code: "Authorised" })
        }

  def processed?
    processed == "processed"
  end

  def process
    return if processed?

    if invoice.settled?
      mark_already_processed!
      return
    end

    person = invoice.person

    payment_options = build_payment_options

    payment_options.each do |option|
      break if reload.processed?

      payment_method_type, instrument = option

      case payment_method_type
      when :balance
        # lock the person balance to prevent parallel runs reading the same starting balance
        person.person_balance.with_lock do
          process_with_balance
        end
      when :paypal
        process_with_paypal(instrument)
      when :card
        process_with_card(instrument)
      when :adyen
        process_adyen_payment(instrument)
      else
        mark_failure(person, invoice)
      end
    end

    if person.plan_user?
      Plans::CancelAdditionalArtistService.cancel_unrenewed_artists(invoice.person, invoice.purchases)
    end

    invoice.update(batch_status: "visible_to_customer")
  end

  def build_payment_options
    # return payment options with a [type, instrument] tuple-ish array.
    person = invoice.person
    payment_options = []

    payment_options << [:balance] if person.renew_with_balance? && person.person_balance.reload.balance >= amount

    if person.renew_with_paypal? && person.preferred_paypal_account.present?
      payment_options << [:paypal, person.preferred_paypal_account]
    end
    if person.renew_with_credit_card? && person.preferred_credit_card.present?
      payment_options << [:card, person.preferred_credit_card]
    end

    if person.renew_with_adyen? && person.adyen_renewals_feature_on? && person.preferred_adyen_payment_method.present?
      payment_options << [:adyen, person.preferred_adyen_payment_method]
    end

    other_paypal_accounts = person.stored_paypal_accounts.active.map { |spa| [:paypal, spa] }
    payment_options = payment_options.union(other_paypal_accounts) unless other_paypal_accounts.empty?

    other_stored_cards = person.stored_credit_cards.active.not_expired.order("updated_at desc, id desc").map { |scc| [:card, scc] }
    payment_options = payment_options.union(other_stored_cards) unless other_stored_cards.empty?

    payment_options << [:failure_marker]

    unless FeatureFlipper.show_feature?(:try_multiple_payment_methods, person.id)
      payment_options = [payment_options.first]
    end
    payment_options
  end

  def self.search(params)
    search = all

    if params
      search = search.where("invoice_id = ?", params[:invoice_id]) if params[:invoice_id].present?
      search = search.where("matching_type = ?", params[:matching_type]) if params[:matching_type].present?
    end

    Tunecore::Search.new(params, search)
  end

  def failed!
    update(processed: "cannot_process", matching: nil)
  end

  private

  def processed!(matching)
    update(processed: "processed", matching: matching)
  end

  def mark_failure(person, invoice)
    invoice.update(batch_status: "visible_to_customer")
    failed!
    comment = "Add not processed transaction"
    options = {
      person: person,
      invoice: invoice,
      payment_batch_id: payment_batch_id,
      batch_transaction_id: id,
      message: comment,
      current_method_name: "process",
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
    log(
      "Payment Batch",
      comment,
      payment_batch_id: payment_batch_id,
      invoice_id: invoice.id,
      person_id: person.id,
      batch_transaction_id: id
    )
  end

  def process_with_card(card)
    if card.is_payments_os_card?
      process_with_payments_os(card)
    else
      process_with_braintree_credit_card(card)
    end
  end

  def process_with_balance
    # Never allow a transaction to be processed twice
    return if processed?

    invoice = self.invoice
    person = invoice.person

    # Process outbound VAT
    balance = person.person_balance

    return if balance.balance < amount

    royalty_purchase = process_vat(person, invoice, balance.balance)
    self.amount = Tunecore::Numbers.cents_to_decimal(royalty_purchase.amount) if royalty_purchase.present?

    # Create the person transaction
    person_transaction = PersonTransaction.create(
      person: person,
      debit: amount,
      credit: 0,
      target: self,
      comment: "Invoice Payment"
    )
    comment = "Pay with balance"
    message = "comment: #{comment}, person_transaction_id: #{person_transaction.id}, amount: #{amount}"
    options = {
      person: person,
      invoice: invoice,
      payment_batch_id: payment_batch_id,
      batch_transaction_id: id,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)

    log(
      "Payment Batch",
      comment,
      batch: payment_batch_id,
      invoice: invoice.id,
      person: person.id,
      person_transaction: person_transaction.id,
      amount: amount
    )

    # Try to settle the invoice
    settlement = invoice.settlement_received(person_transaction, Tunecore::Numbers.decimal_to_cents(amount))
    invoice.settled!

    if invoice.settled?
      # Update the transaction
      processed!(person_transaction)

      comment = "Thank you email sent for balance payment"
      message = "comment: #{comment}, person_transaction_id: #{person_transaction.id}, amount: #{amount}"
      options = {
        person: person,
        invoice: invoice,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)

      log(
        "Payment Batch",
        comment,
        batch: payment_batch_id,
        invoice: invoice.id,
        person: person.id,
        person_transaction: person_transaction.id,
        amount: amount,
        email: person.email
      )
      # Send a thank you email
      BatchNotifier.renewal_payment_thank_you(invoice.person, invoice, invoice.person.domain).deliver

      royalty_purchase.generate_outbound_invoice(invoice.person_transaction_settlement) if royalty_purchase.present?
    else
      comment = "Balance transaction failed"
      message = "comment: #{comment}, person_transaction_id: #{person_transaction.id}, amount: #{amount}"
      options = {
        person: person,
        invoice: invoice,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      log_params = {
        batch: payment_batch_id,
        invoice: invoice.id,
        person: person.id,
        amount: amount
      }
      log(
        "Payment Batch",
        comment,
        log_params
      )
      Airbrake.notify(message, log_params)
    end
  end

  def process_vat(person, invoice, balance)
    return unless person.vat_applicable?

    royalty_purchase = TcVat::RoyaltyPurchase.new(person, invoice, balance)
    royalty_purchase.process_vat!

    royalty_purchase
  end

  def process_with_braintree_credit_card(card)
    # Never allow a transaction to be processed twice
    return if processed?

    begin
      bt_transaction = process_braintree_blue_payment(card)

      comment = "Braintree Transaction"
      bt_amount = bt_transaction.amount
      bt_currency = bt_transaction.currency
      bt_status = bt_transaction.status
      message = "comment: #{comment}, amount: #{bt_amount}, currency: #{bt_currency}, status: #{bt_status}"
      options = {
        person: invoice.person,
        invoice: invoice,
        braintree_transaction_id: bt_transaction.id,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)

      log(
        "Payment Batch",
        comment,
        batch: payment_batch_id,
        invoice: invoice.id,
        person: invoice.person,
        braintree: bt_transaction.id,
        amount: bt_transaction.amount,
        currency: bt_transaction.currency,
        status: bt_transaction.status
      )

      if bt_transaction.invoice&.settled?
        process_successful_credit_card(bt_transaction)
        processed!(bt_transaction)
      else
        # using different amounts for the messages here because the existing logs did it this way
        # above we use bt_transaction amount, here we use batch_transaction amount
        comment = "Braintree transaction failed"
        bt_currency = bt_transaction.currency
        bt_status = bt_transaction.status
        message = "comment: #{comment}, amount: #{amount}, currency: #{bt_currency}, status: #{bt_status}"
        options = {
          person: invoice.person,
          invoice: invoice,
          braintree_transaction_id: bt_transaction.id,
          payment_batch_id: payment_batch_id,
          batch_transaction_id: id,
          message: message,
          current_method_name: __callee__,
          caller_method_path: caller&.first
        }
        InvoiceLogService.log(options)
        log_params = {
          batch: payment_batch.id,
          invoice: invoice.id,
          person: invoice.person_id,
          amount: amount,
          braintree_transaction: bt_transaction.id
        }
        log(
          "Payment Batch",
          comment,
          log_params
        )
        Airbrake.notify(message, log_params)
      end
    rescue StandardError => e
      Braintree::RefundFailedTransactionService.refund_if_charged_and_failed(bt_transaction) if bt_transaction

      # excluding amount, currency, and status in message here because existing logs do
      comment = "Braintree Exception"
      message = "comment: #{comment}, exception: #{e}"
      options = {
        person: invoice&.person,
        invoice: invoice,
        braintree_transaction_id: bt_transaction&.id,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      log_params = {
        batch: payment_batch_id,
        batch_transaction_id: id,
        invoice_id: invoice.id,
        person_id: invoice.person_id,
        exception: e.to_s
      }
      log(
        "Payment Batch",
        comment,
        log_params
      )

      Airbrake.notify(message, log_params)
    end
  end

  def process_adyen_payment(adyen_stored_payment_method)
    return if processed?

    ActiveRecord::Base.transaction do
      person = invoice.person
      txn = AdyenTransaction.new(
        person: person,
        invoice: invoice,
        amount: amount,
        country: country(invoice.person),
        currency: person.currency,
        amount_in_local_currency: 0,
        ip_address: "batch",
        adyen_stored_payment_method: adyen_stored_payment_method,
        adyen_payment_method_info_id: adyen_stored_payment_method.adyen_payment_method_info_id,
        adyen_merchant_config: config_for_corporate_entity(invoice.corporate_entity_id)
      )

      txn.process_renewal
      txn.settle_invoice! if txn.marked_successful?
      txn.save

      comment = "Adyen Transaction"
      adyen_amount = txn.amount
      adyen_currency = txn.currency
      adyen_status = txn.result_code
      message = "comment: #{comment}, amount: #{adyen_amount}, currency: #{adyen_currency}, status: #{adyen_status}"
      options = {
        person: invoice.person,
        invoice: invoice,
        adyen_transaction_id: txn.id,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      log(
        "Payment Batch",
        comment,
        batch: payment_batch_id,
        invoice: invoice.id,
        person: invoice.person,
        adyen_transaction_id: txn.id,
        amount: txn.amount,
        currency: txn.currency,
        status: txn.result_code
      )

      if txn.invoice&.settled?
        process_successful_adyen_transaction(txn)
        processed!(txn)
      else
        notify_adyen_transaction_failure(invoice, txn)
      end
    end
  end

  def process_with_payments_os(card)
    return if processed?

    begin
      amount_in_local_currency = invoice.outstanding_amount_in_local_currency
      payments_service = PaymentsOS::PaymentsService.new(invoice, card, amount_in_local_currency)
      payments_os_transaction = payments_service.pay(true)

      if payments_os_transaction.charge_status == PaymentsOSTransaction::SUCCEEDED
        payments_os_transaction.succeeded
        invoice.purchases.each(&:processed!)
        processed!(payments_os_transaction)
        process_successful_credit_card(payments_os_transaction)
      else
        log("Payment Batch", "PaymentsOS transaction error for the id ", { id: payments_os_transaction.id })
        Airbrake.notify("PaymentsOS transaction error for the id", { id: payments_os_transaction.id })
      end
    rescue StandardError => e
      log_params = {
        batch: payment_batch_id,
        batch_transaction_id: id,
        invoice_id: invoice.id,
        person_id: invoice.person_id,
        exception: e.to_s
      }
      log("Payment Batch", "PaymentsOS Exception", log_params)
      Airbrake.notify("PaymentsOS Exception for Payment Batch", log_params)
    end
  end

  def process_with_paypal(account)
    # Never allow a transaction to be processed twice
    return if processed?

    invoice = self.invoice
    person = invoice.person
    account = person.preferred_paypal_account if account.nil?

    # Pay with paypal
    begin
      paypal_transaction = account.pay(invoice)

      # this allows us to ensure there is an error during testing
      raise "QA testing for Paypal refunds" if raise_paypal_error_for_QA?
    rescue StandardError => e
      Paypal::RefundFailedTransactionService.refund_if_charged_and_failed(paypal_transaction) if paypal_transaction

      comment = "Paypal Exception"
      message = "comment: #{comment}, exception: #{e}"
      options = {
        person: person,
        invoice: invoice,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        stored_paypal_account_id: account.id,
        paypal_transaction_id: paypal_transaction&.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)
      log_params = {
        batch: payment_batch_id,
        batch_transaction_id: id,
        invoice_id: invoice.id,
        person_id: invoice.person_id,
        exception: e.to_s
      }
      log(
        "Payment Batch",
        comment,
        log_params
      )
      Airbrake.notify(message, log_params)
      return
    end

    comment = "Pay with Paypal"

    message = "comment: #{comment}"
    options = {
      person: person,
      invoice: invoice,
      payment_batch_id: payment_batch_id,
      batch_transaction_id: id,
      stored_paypal_account_id: account.id,
      paypal_transaction_id: paypal_transaction&.id,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)

    log(
      "Payment Batch",
      comment,
      batch: payment_batch.id,
      invoice: invoice.id,
      person: person.id,
      paypal_transaction: paypal_transaction.id,
      stored_paypal_account: account.id,
      amount: amount
    )

    if invoice.settled? && paypal_transaction.success?
      processed!(paypal_transaction)

      comment = "Thank you email sent for paypal payment"
      message = "comment: #{comment}, amount: #{paypal_transaction.amount}, email: #{person.email}"

      options = {
        person: person,
        invoice: invoice,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        stored_paypal_account_id: account.id,
        paypal_transaction_id: paypal_transaction.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)

      log(
        "Payment Batch",
        comment,
        batch: payment_batch_id,
        invoice: invoice.id,
        person: person.id,
        paypal_transaction: paypal_transaction.id,
        amount: paypal_transaction.amount,
        email: person.email
      )
      BatchNotifier.renewal_payment_thank_you(invoice.person, invoice, invoice.person.domain).deliver

    else
      Paypal::RefundFailedTransactionService.refund_if_charged_and_failed(paypal_transaction) if paypal_transaction

      comment = "Paypal transaction failed"
      message = "comment: #{comment}, amount: #{paypal_transaction.amount}, email: #{person.email}"

      options = {
        person: person,
        invoice: invoice,
        payment_batch_id: payment_batch_id,
        batch_transaction_id: id,
        stored_paypal_account_id: account.id,
        paypal_transaction_id: paypal_transaction.id,
        message: message,
        current_method_name: __callee__,
        caller_method_path: caller&.first
      }
      InvoiceLogService.log(options)

      log_params = {
        batch: payment_batch.id,
        invoice: invoice.id,
        person: person.id,
        amount: amount,
        stored_paypal_account: person.preferred_paypal_account.id
      }
      log(
        "Payment Batch",
        comment,
        log_params
      )
      Airbrake.notify(message, log_params)

      log(
        "Payment Batch",
        "Notification for renewal sent to customer - paypal failure",
        batch: payment_batch.id,
        invoice: invoice.id,
        person: invoice.person.id,
        amount: invoice.amount,
        email: invoice.person.email,
        stored_paypal_account: person.preferred_paypal_account.id
      )
    end
  end

  def process_braintree_blue_payment(card)
    payin_provider_config = braintree_payment_splits_config(card.person, { is_renewal: true })

    transaction_data = {
      amount: amount,
      payment_method_token: card.bt_token,
      merchant_account_id: payin_provider_config.merchant_account_id,
      options: { submit_for_settlement: true },
      order_id: invoice_id
    }

    transaction_data[:transaction_source] = "recurring" if FeatureFlipper.show_feature?(:send_recurring_indicator)
    transaction_data[:transaction_source] = "unscheduled" if card.person.payment_splits_renewal_enabled?

    result = card.config_gateway_service.sale(transaction_data)

    txn = BraintreeTransaction.new(
      country_website: card.person.country_website,
      transaction_id: result.transaction.try(:id),
      response_code: result.transaction.try(:processor_response_code),
      invoice_id: invoice_id,
      stored_credit_card: card,
      action: "sale",
      amount: amount,
      person_id: invoice.person_id,
      ip_address: "batch",
      currency: result.transaction.try(:currency_iso_code) || currency,
      payin_provider_config: payin_provider_config
    )

    txn.status = txn.blue_status
    txn.detect_duplicates
    BraintreeTransactionLogWorker.new.async.write_to_bucket(invoice.id, result)
    begin
      TransactionResponse.process_transaction_params(txn, result)
    rescue => e
      Airbrake.notify(
        "Additional Braintree transaction response processing failed",
        {
          error: e,
          sale_result: result.to_yaml,
          transaction_id: txn.id
        }
      )
    end
    txn.save
    txn.log_transaction(txn.status) # this should probably be refactored into an after_create
    txn.settle_invoice # this should be an after_create
    txn
  end

  def process_successful_credit_card(transaction)
    BatchNotifier.renewal_payment_thank_you(
      transaction.person,
      transaction.invoice,
      transaction.person.domain
    ).deliver
    log(
      "Payment Batch",
      "Thank you email sent for credit card payment",
      batch: payment_batch_id,
      invoice: transaction.invoice_id,
      person: transaction.person_id,
      batch_transaction: id,
      amount: transaction.amount,
      currency: transaction.currency,
      email: transaction.person.email
    )
  end

  def process_successful_adyen_transaction(transaction)
    BatchNotifier.renewal_payment_thank_you(
      transaction.person,
      transaction.invoice,
      transaction.person.domain
    ).deliver
    log(
      "Payment Batch",
      "Thank you email sent for adyen payment",
      batch: payment_batch_id,
      invoice: transaction.invoice_id,
      person: transaction.person_id,
      batch_transaction: id,
      amount: transaction.amount,
      currency: transaction.currency,
      email: transaction.person.email
    )
  end

  def mark_already_processed!
    matching = invoice.invoice_settlements.first.source
    processed!(matching)

    Airbrake.notify("Marked a duplicate batch transaction as processed", { id: id, invoice_id: invoice.id })
  end

  def notify_adyen_transaction_failure(invoice, txn)
    # using different amounts for the messages here because the existing logs did it this way
    # above we use adyen_transaction amount, here we use batch_transaction amount
    comment = "Adyen transaction failed"
    currency = txn.local_currency
    status = txn.result_code
    message = "comment: #{comment}, amount: #{amount}, currency: #{currency}, status: #{status}"
    options = {
      person: invoice.person,
      invoice: invoice,
      adyen_transaction_id: txn.id,
      payment_batch_id: payment_batch_id,
      batch_transaction_id: id,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
    log_params = {
      batch: payment_batch.id,
      invoice: invoice.id,
      person: invoice.person_id,
      amount: amount,
      adyen_transaction: txn.id
    }
    log(
      "Payment Batch",
      comment,
      log_params
    )
  end

  def country(person)
    Country.find_by(name: person.country)
  end

  def raise_paypal_error_for_QA? # should be disabled after QA approval
    FeatureFlipper.show_feature?(:qa_test_paypal_refunds, @person)
  end
end
