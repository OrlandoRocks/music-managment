class PerformingRightsOrganization < ApplicationRecord
  validates :name, :provider_identifier, presence: true

  def self.eligible_publishing_pro_ids
    where(name: Composer::PUBLISHER_ADMIN.keys).pluck(:id)
  end
end
