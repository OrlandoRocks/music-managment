# frozen_string_literal: true

class MassAdjustmentBatch < ApplicationRecord
  NEW = "new"
  INGESTED = "ingested"
  CONFIGURED = "configured"
  CANCELLED = "cancelled"
  APPROVED = "approved"
  DECLINED = "declined"
  SCHEDULED = "processing"
  COMPLETED = "completed"
  COMPLETED_WITH_ERROR = "completed_with_error"

  STATUS = [NEW, CONFIGURED, CANCELLED, APPROVED, DECLINED, SCHEDULED, COMPLETED, COMPLETED_WITH_ERROR]

  belongs_to :created_by, class_name: "Person"
  belongs_to :balance_adjustment_category
  has_many :mass_adjustment_entries
  has_many :unprocessed_mass_adjustment_entries, -> { where(status: nil) }, class_name: "MassAdjustmentEntry"

  after_save :withhold_taxes, if: proc { withhold_taxes? }

  scope :incomplete,
        -> {
          joins(:mass_adjustment_entries)
            .where.not(mass_adjustment_batches: { status: MassAdjustmentBatch::COMPLETED })
            .distinct
        }

  def self.latest
    order("created_at").last
  end

  def self.batch_in_flight?
    return false unless latest

    (latest.status == DECLINED || latest.status == CANCELLED) ? false : !latest.processing_completed?
  end

  def self.process_batches_as_completed
    MassAdjustmentBatch.incomplete.each do |batch|
      status =
        if batch.processing_completed_with_error?
          MassAdjustmentBatch::COMPLETED_WITH_ERROR
        elsif batch.processing_completed?
          MassAdjustmentBatch::COMPLETED
        end

      batch.update(status: status) if status && batch.status != status
    end
  end

  def processing_completed_with_error?
    processing_completed? && errored_entries.present?
  end

  def processing_completed?
    mass_adjustment_entries.present? && unprocessed_mass_adjustment_entries.count.zero?
  end

  def needs_negative_values_processed?
    unprocessed_mass_adjustment_entries.present? && unprocessed_mass_adjustment_entries == (mass_adjustment_entries - mass_adjustment_entries.where(balance_turns_negative: false))
  end

  def errored_entries
    mass_adjustment_entries.select(&:error?)
  end

  def withhold_taxes?
    FeatureFlipper.show_feature?(:us_tax_withholding) &&
      saved_change_to_attribute?("status", to: MassAdjustmentBatch::COMPLETED)
  end

  def withhold_taxes
    TaxWithholdings::MassAdjustmentsBatchWorker.perform_async
  end
end
