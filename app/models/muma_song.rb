class MumaSong < ApplicationRecord
  self.primary_key = "code"

  belongs_to :composition
  belongs_to :publishing_composition

  has_many :muma_song_ips,          foreign_key: "song_code", primary_key: "code"
  has_many :muma_song_societies,    foreign_key: "song_code", primary_key: "code"

  has_many :publishers, -> { where "ip_c_or_p = 'P'" }, foreign_key: "song_code", primary_key: "code", class_name: "MumaSongIp"
  has_many :composers, -> { where "ip_c_or_p = 'C'" }, foreign_key: "song_code", primary_key: "code", class_name: "MumaSongIp"

  def tc_song_and_album
    Song.eager_load(album: [:primary_genre, :secondary_genre, :upcs, { person: :composers }])
        .find_by(
          "composition_id = ?
        AND (tunecore_isrc = REPLACE(?,'-','')
        OR optional_isrc = REPLACE(?,'-',''))
        AND upcs.number = ?",
          composition_id,
          isrc,
          isrc,
          album_upc.to_i
        )
  end

  #
  # Adds entries in sales_by_year table for muma songs
  # that are not in there
  #
  def self.insert_itunes_sales_by_year_for_new_muma_songs
    sql = %Q{
       insert into sales_by_year ( year, store_id, related_type, related_id, qty, amount )
        select srm.period_year, sip_stores.store_id, sr.related_type, sr.related_id, sum(sr.quantity), sum(sr.amount)
        from ( select songs.id as `related_id`, "Song" as related_type from muma_songs
        inner join songs on songs.composition_id = muma_songs.composition_id
        left outer join sales_by_year on sales_by_year.related_id = songs.id and sales_by_year.related_type = "Song"
        where sales_by_year.id is null) as songs_not_in_summary
        inner join sales_records sr on sr.related_id = songs_not_in_summary.related_id and sr.related_type = songs_not_in_summary.related_type
        inner join sales_record_masters srm on srm.id = sr.sales_record_master_id
        inner join sip_stores on sip_stores.id = srm.sip_store_id
        where sip_stores.store_id = ? and srm.period_year < ?
        group by srm.period_year, sip_stores.store_id, sr.related_type, sr.related_id}

    sql_array = [sql, Store.where("abbrev = ?", "ww").first, Date.today.year]
    sql = ActiveRecord::Base.send(:sanitize_sql_array, sql_array)

    ActiveRecord::Base.connection.execute(sql)
  end
end
