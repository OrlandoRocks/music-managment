class MusicVideo < Video
  include Tunecore::Lockable
  include Tunecore::Creativeable
  self.inheritance_column = "video_type"

  # ===============
  # = Validations =
  # ===============
  validates :name, :length, :person_id, :artist, presence: true
  validate :presence_of_accepted_format
  validates :length,
            inclusion: {
              in: 30..1800,
              message: "of your video must be longer than 30 seconds and shorter than 30 minutes"
            }
  validates :length, numericality: true

  # =============
  # = Callbacks =
  # =============
  before_validation :validation_set_artist, :set_video_length

  def validation_set_artist
    if @selected_artist_name
      self.artist = @selected_artist_name.blank? ? nil : Artist.create_by_nontitleized_name(@selected_artist_name)
      @selected_artist_name = nil
    else
      logger.debug "Artist name is not selected.  Should include an exception?"
    end
  end

  # generates the database length column in the before_validation call
  def set_video_length
    # self.length = (@min.to_i * 60) + @sec.to_i
    self.length = (length_min.to_i * 60) + length_sec.to_i
    @min = nil
    @sec = nil
  end

  def length_min
    if length.blank? && @min.nil?
      nil
    else
      (@min = length.to_i / 60) if @min.nil?
      @min
    end
  end

  def video_length_in_minutes
    if length.blank?
      0
    else
      length.to_i / 60
    end
  end

  def length_min=(min)
    @min = min.to_i
  end

  def length_sec
    if length.blank? && @sec.nil?
      nil
    else
      (@sec = length.to_i.modulo(60)) if @sec.nil?
      @sec
    end
  end

  def length_sec=(sec)
    @sec = sec.to_i
  end
end
