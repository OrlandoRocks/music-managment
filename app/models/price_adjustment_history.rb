class PriceAdjustmentHistory < ApplicationRecord
  ###
  # Belongs To Relationships
  ###
  belongs_to :admin, class_name: "Person"
  belongs_to :purchase, counter_cache: true

  ###
  # Validations
  ###
  validates :admin,
            :purchase,
            :note,
            :original_cost_cents,
            :original_discount_cents,
            :new_cost_cents,
            :adjustment_cents,
            presence: true

  ###
  # Callbacks
  ###
  before_validation :calculate_adjustement_cents

  #
  # Calculates the delta between the what they were gonna pay with what they have to pay now
  #
  def calculate_adjustement_cents
    if original_cost_cents and original_discount_cents and new_cost_cents
      self.adjustment_cents = ((original_cost_cents - original_discount_cents) - new_cost_cents).abs
    end
    true
  end
end
