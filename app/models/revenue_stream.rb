class RevenueStream < ApplicationRecord
  has_many :revenue_streams_transaction_mappings, dependent: :nullify
  has_one :tax_form_revenue_stream, dependent: :nullify
  has_one :tax_form, through: :tax_form_revenue_streams

  CODES = [:distribution, :publishing].freeze

  scope :with_distribution, -> { where(code: :distribution) }
  scope :with_publishing, -> { where(code: :publishing) }

  def self.distribution
    find_by(code: :distribution)
  end

  def self.publishing
    find_by(code: :publishing)
  end

  def self.taxable_earnings_rollover
    find_by(code: :taxable_earnings_rollover)
  end
end
