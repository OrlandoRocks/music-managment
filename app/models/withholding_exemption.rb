# frozen_string_literal: true

class WithholdingExemption < ApplicationRecord
  belongs_to :withholding_exemption_reason
  belongs_to :person

  has_paper_trail

  scope :active, -> do
    if progressive_withholding_exemption_enabled?
      where(ended_at: nil).or(WithholdingExemption.where(revoked_at: nil))
    else
      where(ended_at: nil)
    end
  end

  # Needs to ignore callbacks to make readonly?
  def revoke!
    time_stamp = Time.now.utc.beginning_of_day

    # rubocop:disable Rails/SkipsModelValidations
    transaction do
      update_column(:revoked_at, time_stamp)
      update_column(:ended_at, time_stamp)
    end
    # rubocop:enable Rails/SkipsModelValidations
  end

  def revoked?
    revoked_at.present? && ended?
  end

  def ended?
    ended_at.present?
  end

  def active?
    if self.class.progressive_withholding_exemption_enabled?
      started_at.present? && (!ended? || !revoked?)
    else
      started_at.present? && !ended?
    end
  end

  # Once revoked, we should no longer update this record.
  def readonly?
    revoked?
  end

  def self.progressive_withholding_exemption_enabled?
    FeatureFlipper.show_feature?(:progressive_withholding_exemption)
  end
end
