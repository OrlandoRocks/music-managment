module CashAdvances
  class Transport
    def initialize
      @conn =
        Faraday.new(url: CASH_ADVANCES_CONFIG["tc_service_host"]) do |faraday|
          faraday.request :url_encoded
          faraday.response :logger
          faraday.adapter  Faraday.default_adapter
          faraday.options[:timeout] = 60 * 5
        end
    end

    def post(person)
      @conn.post CASH_ADVANCES_CONFIG["tc_service_endpoint"],
                 { email: person.email, name: person.name, user_id: person.id }
    end
  end
end
