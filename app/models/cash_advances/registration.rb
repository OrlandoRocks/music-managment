module CashAdvances
  class Registration
    def self.register(person, transport = CashAdvances::Transport.new)
      res = transport.post(person)

      JSON.parse(res.body)["token"] if res.body.present?
    end
  end
end
