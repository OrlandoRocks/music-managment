#
# = Description
#
# = Usage
#
# = History
#
#
class StoreGroup < ApplicationRecord
  has_many :store_group_stores
  has_many :stores, through: :store_group_stores

  def abbreviation
    stores.first.abbrev
  end

  def is_itunes?
    key == "itunes"
  end
end
