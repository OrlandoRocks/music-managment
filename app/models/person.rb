class Person < ApplicationRecord
  include Tunecore::LockableLock
  include Tunecore::People::YoutubeMonetization
  include Tunecore::People::SubscriptionService
  include Tunecore::People::PreferenceManageable
  include Tunecore::People::PeopleFlagsWithdrawalBlock
  include Tunecore::People::FirstNameLockFlag
  include Tunecore::People::LastNameLockFlag
  include Tunecore::People::CompanyNameLockFlag
  include Tunecore::People::AddressLockFlag
  include Tunecore::People::AddressValidatable
  include Tunecore::People::AutoRenewalEmailFlaggable
  include Tunecore::People::ComplianceInfo
  include Tunecore::People::RewardSystem
  include Tunecore::People::VatCompliance
  include Tunecore::People::TaxWithholdable
  include Tunecore::CrtReviewable
  include ActiveModel::Serialization
  include PublishingAdministrationHelper
  include ArelCommonJoins
  include CountriesAggregatable
  include CountryAuditable
  include PayinProviderConfigurable
  include Taxable
  include PaymentHelper
  include PersonCountryMethods
  include Planable
  include PriorityArtistable
  include TcAcceleratable

  has_paper_trail on: [:update], if: ->(person) { person.email_changed? }

  attr_accessor :conversion_rate
  attr_accessor :promo_ref
  attr_accessor :first_login
  attr_accessor :new_password
  attr_accessor :password_confirmation
  attr_accessor :email_confirmation
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :widget_status
  attr_accessor :original_password
  attr_accessor :original_email
  attr_accessor :per_person_revenue
  attr_accessor :from_password_reset_form
  attr_accessor :takeover_admin_id
  attr_accessor :prefixless_mobile_number

  self.ignored_columns = %w[gain_employee_id]

  # ================
  # = Associations =
  # ================
  belongs_to :artist
  belongs_to :label
  belongs_to :us_zip_code
  # belongs_to :canada_postal_code
  belongs_to :country_website
  belongs_to :language_code
  belongs_to :corporate_entity
  belongs_to :country_state
  belongs_to :country_state_city
  belongs_to :country_website_language

  has_one :cable_auth_token, dependent: :destroy
  has_one :person_balance
  has_one :person_preference
  has_one :person_sift_score
  has_one :youtube_preference, dependent: :destroy
  has_one :deleted_account
  has_many :deleted_accounts, foreign_key: :admin_id
  has_many :withholding_exemptions
  has_one :payments_os_customer, class_name: PaymentsOSCustomer.to_s
  has_one :lifetime_earning, dependent: :destroy
  has_one :priority_artist
  has_one :priority_artist_artist, through: :priority_artist, source: :artist

  has_one :suspicious_flag,
          -> {
            joins(:flag).where(flag: Flag.suspicious_flag)
          },
          class_name: "PeopleFlag"

  has_one :blocked_from_distribution_flag,
          -> {
            joins(:flag).where(flag: Flag.blocked_from_distribution_flag)
          },
          class_name: "PeopleFlag"

  has_one :person_points_balance
  has_many :person_points_transactions

  has_many :gst_infos
  has_many :person_subscription_statuses
  has_many :legal_documents
  has_many :youtube_monetizations

  has_many :certs
  has_many :check_transfers
  has_many :paypal_transfers
  has_many :invoices
  has_many :albums do
    def live_on_itunes_us
      order(id: :desc).select(&:is_live_on_itunes_us?)
    end

    def with_outstanding_renewal_purchases
      order(:id).select(&:has_unpaid_renewal?)
    end

    def streamable
      where(can_stream_cache: true).includes(songs: { creatives: :artist }).order(id: :desc)
    end
  end
  has_many :singles
  has_many :ringtones
  has_many :songs, through: :albums
  has_many :invoices
  has_many :person_transactions
  has_many :serials
  has_many :videos
  has_many :feature_films
  has_many :music_videos
  has_many :notifications, -> { order "notifications.effective_created_at DESC" }
  has_many :purchases
  has_many :sales_records
  has_many :album_intakes
  has_many :salepoints, -> { includes(:store) }, through: :albums
  has_many :salepoint_songs, through: :salepoints
  has_many :soundout_reports
  has_many :sync_license_requests
  has_many :sync_license_productions
  has_many :sync_favorite_songs, -> { order :song_id }
  has_many :song_copyrights, through: :songs, source: :copyrights
  has_many :people_flags
  has_many :flags, through: :people_flags
  has_many :flag_transitions
  has_many :copyright_claims
  has_many :two_factor_auth_events, through: :two_factor_auth
  has_many :plan_downgrade_requests
  has_many :stored_credit_cards
  has_many :stored_paypal_accounts
  has_many :stored_bank_accounts
  has_many :adyen_stored_payment_methods
  has_many :latest_in_each_adyen_payment_methods, class_name: "LatestInEachAdyenPaymentMethodView"
  has_many :paypal_transactions
  has_many :targeted_people
  # #do not use this association to get a person's Offers, since it doesn't take expiration, etc. into account.
  # For that use: TargetedOffer.for_person(person)
  # this is pull all past and present offers.
  has_many :targeted_offers, through: :targeted_people
  has_many :braintree_transactions
  has_many :adyen_transactions
  has_many :braintree_vault_transactions
  has_many :notes, -> { order "created_at DESC" }, as: :related
  has_many :person_intakes
  has_many :tour_dates, -> { order "event_date_at asc" } do
    def upcomming
      where("event_date_at >= ?", Time.current.to_date).order(event_date_at: :asc)
    end
  end
  has_many :band_photos
  has_many :balance_adjustments
  has_many :balance_adjustments, foreign_key: "posted_by_id"

  has_and_belongs_to_many :roles

  has_many :renewals
  has_many :plan_renewals, -> { plans }, class_name: "Renewal", foreign_key: :person_id
  has_many :inventories

  has_many :open_auths
  has_many :active_oauths, -> { where is_active: "Y" }, class_name: "OpenAuth", source: :open_auths

  # OAUTH provider associations(2011-02-23 AK)
  has_many :client_applications, foreign_key: :user_id
  has_many :tokens, -> {
                      order("authorized_at desc").includes(:client_application)
                    }, class_name: "OauthToken", foreign_key: :user_id

  has_many :review_audits

  has_many :feature_people
  has_many :features, through: :feature_people

  # Publishing (2011-07-14 AK)
  has_many :composers
  has_many :publishing_splits, through: :composers
  has_many :compositions, through: :publishing_splits
  has_many :publishing_composers
  has_many :publishing_composition_splits, through: :publishing_composers
  has_many :publishing_compositions, through: :publishing_composition_splits
  has_many :royalty_payments

  has_many :friend_referral_campaigns
  has_many :referral_data
  has_many :external_services_people
  has_many :external_services, through: :external_services_people
  has_many :salepoint_subscriptions, through: :albums
  has_many :credit_usages
  has_many :mastered_tracks

  has_many :you_tube_royalty_records
  has_many :you_tube_royalty_intakes
  has_many :person_analytics
  has_one  :person_profile_survey_info
  has_many :person_service_opt_ins
  has_many :login_events
  has_many :apple_artist_ids
  has_many :song_library_uploads
  has_many :tunecore_trackings
  has_many :tax_tokens

  has_many :tcda_earnings_period_records, class_name: "DirectAdvance::EarningsPeriodRecord"
  has_many :tcda_eligibility_records, class_name: "DirectAdvance::EligibilityRecord"

  has_many :encumbrance_summaries

  has_many :creatives
  has_many :artists, -> { distinct }, through: :creatives
  has_one  :two_factor_auth
  has_one  :two_factor_auth_prompt
  has_many :two_factor_auth_events, through: :two_factor_auth

  has_many :payout_providers
  has_one :payout_provider, -> { where(active_provider: true) }, class_name: PayoutProvider.to_s.freeze
  has_one :payout_provider_config, through: :payout_provider
  has_many :payout_withdrawal_types, through: :payout_provider
  has_many :payout_transfers, through: :payout_providers
  has_many :track_monetizations

  has_one :account # TODO: Remove account and change to publishing_account
  has_many :accounts
  has_many :taxable_earnings
  has_many :tax_forms
  has_many :current_tax_forms_across_programs, -> { current.active }, class_name: "TaxForm", inverse_of: :person
  has_many :recover_assets, dependent: :destroy
  has_one :vat_information, dependent: :destroy
  has_many :vat_information_audits
  has_many :compliance_info_fields
  has_many :country_audits
  has_one :self_billing_status, dependent: :destroy
  has_many :outbound_invoices
  has_many :outbound_refunds, through: :outbound_invoices
  has_one :company_information, dependent: :destroy

  has_many :tier_people, dependent: :destroy
  has_many :tiers, through: :tier_people

  has_many :tier_people, dependent: :destroy
  has_many :tiers, through: :tier_people

  has_many :tier_people, dependent: :destroy
  has_many :tiers, through: :tier_people
  has_many :person_earnings_tax_metadata

  has_many :payu_transactions
  has_many :email_change_requests

  has_one :person_plan
  has_one :plan, through: :person_plan
  has_many :plans_stores, through: :plan
  has_many :plan_addons
  has_many :stores_from_plan, through: :plans_stores, source: :store
  has_many :manual_approvals, dependent: :destroy, foreign_key: "created_by_id"
  has_many :additional_artists
  has_many :splits_collaborator_addons
  has_many :survey_responses, dependent: :destroy
  has_many :surveys, through: :survey_responses
  has_many :tax_form_revenue_streams, dependent: :destroy
  has_many :revenue_streams, through: :tax_form_revenue_streams

  has_many :owned_royalty_splits, foreign_key: "owner_id", class_name: "RoyaltySplit", inverse_of: :owner
  has_many :royalty_split_recipients, inverse_of: :person
  has_many :royalty_splits, through: :royalty_split_recipients, class_name: "RoyaltySplit", source: :royalty_split

  accepts_nested_attributes_for :referral_data
  accepts_nested_attributes_for :vat_information
  accepts_nested_attributes_for :self_billing_status
  accepts_nested_attributes_for :compliance_info_fields

  delegate :preferred_credit_card, to: :person_preference, allow_nil: true
  delegate :preferred_adyen_payment_method, to: :person_preference, allow_nil: true
  delegate :currency, to: :country_website

  delegate :active_plan_on_datetime, to: :person_plan

  delegate :company_name, to: :vat_information, prefix: "vat", allow_nil: true
  delegate :vat_registration_number, to: :vat_information, allow_nil: true

  scope :collaborators, -> {
                          joins(:royalty_split).where("royalty_splits.owner_id != royalty_split_recipients.person_id")
                        }
  scope :with_total_albums,
        ->(album_count) {
          joins(:albums)
            .group(:id)
            .having("COUNT(albums.id) = ?", album_count)
        }

  # this named scoped returns an array of people that have renewals that expire within a set period of time
  # it can accept 0,1, or 2 varables passed in as strings that are be converted to dates
  # Person.preference_reminders returns people to notify with expirations between Now and 6 weeks from now (6 was the initial setting, this may change)
  # Person.preference_reminders('2010-01-01') returns people to notify with expirations between 1/1/2010 and 6 weeks from 1/1/2010
  # Person.preference_reminders('2010-01-01','2010-02-01') returns people to notify with expirations between 1/1/2010 and 2/1/2010

  # May not need
  scope :preference_reminders_old,
        ->(*dates) {
          joins(
            "INNER JOIN renewals r ON r.person_id=people.id
          LEFT JOIN (SELECT MAX(expires_at) AS expires, renewal_id, created_at FROM renewal_history group by renewal_id) h ON r.id=h.renewal_id
          LEFT JOIN (SELECT * FROM purchases where paid_at IS NULL) pu ON r.id=pu.related_id AND pu.related_type='Renewal'
          LEFT JOIN person_preferences pp ON pp.person_id=people.id
          LEFT JOIN stored_credit_cards scc ON scc.id=pp.preferred_credit_card_id"
          )
            .where("h.expires >= ?
              AND h.expires <= ?
              AND ADDDATE(h.created_at, ?) < ?
              AND r.canceled_at IS NULL AND r.takedown_at IS NULL
              #{'AND (
                  pp.id IS NULL
                  OR pp.do_not_autorenew = 1
                  OR (pp.preferred_payment_type = "CreditCard" AND pp.preferred_credit_card_id IS NULL)
                  OR ((pp.preferred_payment_type = "CreditCard" AND pp.preferred_credit_card_id IS NOT NULL)
                    AND (scc.expiration_year < YEAR(h.expires) OR (scc.expiration_year = YEAR(h.expires)
                    AND scc.expiration_month <= MONTH(h.expires))))
                  OR (pp.preferred_payment_type = "PayPal" AND pp.preferred_paypal_account_id IS NULL)
                        )' if Date.parse(dates.first) >= Date.today}",
                   (dates.first ? Date.parse(dates.first) : Time.now),
                   if dates.second
                     Date.parse(dates.second)
                   else
                     ((dates.first ? Date.parse(dates.first) : Time.now) + Renewal::REMINDERS_LENGTH.weeks)
                   end,
                   Renewal::REMINDERS_NOTICE_GRACE_DAYS,
                   (dates.first ? Date.parse(dates.first) : Time.now))
            .group("people.id")
        }

  scope :preference_reminders,
        ->(monthly, date) {
          joins(
            "INNER JOIN renewals r on r.person_id=people.id
          INNER JOIN product_items pi on pi.id = r.item_to_renew_id and r.item_to_renew_type = 'ProductItem'
          LEFT JOIN (SELECT MAX(expires_at) as expires, renewal_id, created_at FROM renewal_history group by renewal_id) h on r.id=h.renewal_id
          LEFT JOIN (SELECT * from purchases where paid_at IS NULL) pu on r.id=pu.related_id and pu.related_type='Renewal'
          LEFT JOIN person_preferences pp ON pp.person_id=people.id
          LEFT JOIN stored_credit_cards scc ON scc.id=pp.preferred_credit_card_id"
          )
            .where("h.expires >= ?
                      AND h.expires <= ?
                      AND ADDDATE(h.created_at, ?) < ?
                      AND r.canceled_at IS NULL AND r.takedown_at IS NULL
                      AND pi.renewal_interval = ?
                #{'AND (
                  pp.id IS NULL
                  OR pp.do_not_autorenew = 1
                  OR (pp.preferred_payment_type = "CreditCard" AND pp.preferred_credit_card_id IS NULL)
                  OR ((pp.preferred_payment_type = "CreditCard" AND pp.preferred_credit_card_id IS NOT NULL)
                      AND (scc.expiration_year < YEAR(h.expires) OR (scc.expiration_year = YEAR(h.expires)
                      AND scc.expiration_month <= MONTH(h.expires))))
                  OR (pp.preferred_payment_type = "PayPal" AND pp.preferred_paypal_account_id IS NULL)
                          )' if Date.parse(date) >= Date.today}",
                   Date.parse(date),
                   Date.parse(date),
                   Renewal::REMINDERS_NOTICE_GRACE_DAYS,
                   Date.parse(date),
                   monthly ? "month" : "year")
            .group("people.id")
        }

  # this is mostly the same as the query above, used for logging total renewals for comparison to how many people we need to contact.
  scope :with_renewals_in_period_old,
        ->(*dates) {
          joins(
            "inner join renewals r on r.person_id=people.id
                LEFT JOIN person_preferences pp on pp.person_id = people.id
                left join (SELECT MAX(expires_at) as expires, renewal_id, created_at FROM renewal_history group by renewal_id) h on r.id=h.renewal_id
               left join (SELECT * from purchases where paid_at IS NULL) pu on r.id=pu.related_id and pu.related_type='Renewal'"
          )
            .where("(h.expires >= ? AND h.expires <= ? AND ADDDATE(h.created_at, ?) < ?)
              AND r.canceled_at IS NULL AND r.takedown_at IS NULL",
                   (dates.first ? Date.parse(dates.first) : Time.now),
                   if dates.second
                     Date.parse(dates.second)
                   else
                     ((dates.first ? Date.parse(dates.first) : Time.now) + Renewal::REMINDERS_LENGTH.weeks)
                   end,
                   Renewal::REMINDERS_NOTICE_GRACE_DAYS,
                   (dates.first ? Date.parse(dates.first) : Time.now))
            .group("people.id")
        }

  scope :with_renewals_for_day,
        ->(monthly, date) {
          joins(
            "INNER JOIN renewals r ON r.person_id=people.id
          INNER JOIN product_items pi ON pi.id = r.item_to_renew_id AND r.item_to_renew_type = 'ProductItem'
          LEFT JOIN person_preferences pp ON pp.person_id = people.id
          LEFT JOIN (SELECT MAX(expires_at) AS expires, renewal_id, created_at FROM renewal_history group by renewal_id) h ON r.id=h.renewal_id
          LEFT JOIN purchases pu ON r.id=pu.related_id and pu.related_type='Renewal' AND pu.paid_at IS NULL"
          )
            .where("(h.expires = :date AND ADDDATE(h.created_at, :grace_days) < :date) AND pi.renewal_interval = :interval
                      AND r.canceled_at IS NULL AND r.takedown_at IS NULL",
                   date: date,
                   grace_days: Renewal::REMINDERS_NOTICE_GRACE_DAYS,
                   interval: monthly ? "month" : "year")
            .group("people.id")
        }

  # scope so you can use named_scopes on an individual person, e.g. Person.preference_reminders.for_person(1)
  scope :for_person, ->(person_id) { where(id: person_id) }

  scope :with_no_taxform_associations, -> do
    left_joins(:tax_forms).where(tax_forms: { person_id: nil })
  end

  scope :with_no_valid_taxforms, -> do
    joins(:tax_forms).where(tax_forms: { expires_at: Time.at(0).to_datetime..DateTime.current })
  end

  scope :with_no_lifetime_earnings_association, -> do
    left_joins(:lifetime_earning)
      .where(lifetime_earnings: { person_id: nil })
  end

  scope :with_no_valid_taxforms_or_associations, -> do
    left_joins(:tax_forms)
      .where(tax_forms: { person_id: nil })
      .or(
        left_joins(:tax_forms)
        .where.not(tax_forms: { tax_form_type_id: TaxFormType.w9_types.ids })
      )
  end

  scope :with_no_lifetime_earning, -> do
    joins(:lifetime_earning)
      .where(lifetime_earnings: {
               person_intake: (-Float::INFINITY..0),
               youtube_intake: (-Float::INFINITY..0)
             })
  end

  scope :with_lifetime_earnings, -> do
    joins(:lifetime_earning).merge(
      LifetimeEarning.where("person_intake > 0")
        .or(LifetimeEarning.where("youtube_intake > 0"))
    )
  end

  scope :with_renewals_expiring_between,
        ->(date1, date2) {
          select("distinct people.*")
            .joins("inner join renewals on renewals.person_id = people.id",
                   "inner join renewal_history on renewal_history.renewal_id = renewals.id")
            .where("Date(?) <= Date(renewal_history.expires_at) and Date(renewal_history.expires_at) <= Date(?)", date1, date2)
        }

  # ABOVE THIS INE WAS GREPPED

  scope :that_extended_renewals_due_on_paid_before,
        ->(date, paid_before_time) {
          select("distinct people.*")
            .joins("inner join renewals on renewals.person_id = people.id",
                   "inner join renewal_history rh1 on rh1.renewal_id = renewals.id",
                   "inner join renewal_history rh2 on rh2.renewal_id = renewals.id",
                   "inner join purchases on rh2.purchase_id = purchases.id")
            .where("Date(rh1.expires_at) = Date(:date) and Date(rh2.starts_at) = Date(:date) and paid_at is NOT NULL and paid_at < :paid_before_time", date: date, paid_before_time: paid_before_time)
        }

  scope :that_extended_renewals_due_on_paid_after,
        ->(date, paid_after_time) {
          select("distinct people.*")
            .joins("inner join renewals on renewals.person_id = people.id",
                   "inner join renewal_history rh1 on rh1.renewal_id = renewals.id",
                   "inner join renewal_history rh2 on rh2.renewal_id = renewals.id",
                   "inner join purchases on rh2.purchase_id = purchases.id")
            .where("Date(rh1.expires_at) = Date(:date) and Date(rh2.starts_at) = Date(:date) and paid_at is NOT NULL and paid_at >= :paid_after_time", date: date, paid_after_time: paid_after_time)
        }

  scope :with_renewals_for_album_type,
        ->(album_type) {
          select("distinct people.*")
            .joins("inner join renewals on renewals.person_id = people.id",
                   "inner join renewal_items on renewal_items.renewal_id = renewals.id",
                   "inner join albums on albums.id = renewal_items.related_id")
            .where("renewal_items.related_type = 'Album' and album_type = ?", album_type)
        }

  scope :with_renewals_due_on_but_not_renewed,
        ->(date) {
          select("distinct people.*")
            .joins("inner join renewals on renewals.person_id = people.id",
                   "inner join renewal_history rh1 on rh1.renewal_id = renewals.id",
                   "left join ( select renewal_history.* from renewal_history where renewal_history.starts_at = '#{date.to_s(:db)}'  ) rh2 on rh2.renewal_id = renewals.id",
                   "inner join purchases on rh1.purchase_id = purchases.id")
            .where("Date(rh1.expires_at) = Date(?) and rh2.id is null", date)
        }

  scope :with_canceled_renewals,
        ->(canceled) {
          select("distinct people.*")
            .joins(:renewals)
            .where(canceled ? "renewals.canceled_at is NOT NULL" : "renewals.canceled_at is NULL")
        }

  scope :with_takendown_renewals,
        ->(takendown) {
          select("distinct people.*")
            .joins(:renewals)
            .where(takendown ? "renewals.takedown_at is NOT NULL" : "renewals.takedown_at is NULL")
        }

  scope :for_country_website,
        ->(country_website_id) {
          where(country_website_id: country_website_id)
        }

  # TODO: Replace the where method call with the following once Person is associated with Country.
  # joins(:country).merge(Country.eu)
  scope :in_eu, -> { where(country: Country::EU_COUNTRIES) }

  scope :with_positive_balance, -> { joins(:person_balance).where("person_balances.balance > 0") }

  # user with a positive balance and no login events in the past 3 years and currently not marked 'dormant'
  scope :needs_marked_dormant,
        -> {
          with_positive_balance
            .where(dormant: false)
            .where("created_on < ?", Date.current - 3.years)
            .where("recent_login is null or recent_login < ?", Date.current - 3.years)
        }

  scope :for_united_states_and_territories,
        -> {
          where(country: Country::UNITED_STATES_AND_TERRITORIES)
        }

  scope :for_not_locked_accounts,
        -> {
          where.not(status: LOCKED)
        }

  scope :with_flag, ->(flag) do
    joins(people_flags: [:flag]).where(flags: flag)
  end

  scope :without_flag, ->(flag) do
    left_joins(people_flags: :flag)
      .where.not(people_flags: { flags: { id: flag.id } })
      .or(Person.left_joins(people_flags: :flag).where(people_flags: { flags: { id: nil } }))
  end

  scope :with_active_payout_providers, ->(provider_name: PayoutProvider::PAYONEER) do
    joins(:payout_providers).merge(PayoutProvider.active).where(
      payout_providers: { name: provider_name }
    )
  end

  scope :without_active_payout_providers, ->(provider_name: PayoutProvider::PAYONEER) do
    left_joins(:payout_providers).merge(
      PayoutProvider
        .not_active
        .or(PayoutProvider.where.not(name: provider_name))
        .or(PayoutProvider.where(person_id: nil))
    )
  end

  # Callbacks
  #
  before_validation :set_created_on, on: :create
  before_validation :email_downcase
  before_validation :assign_country_website_on_create, on: :create

  #
  # Validations
  #
  validates :email, presence: true
  validate :validate_email_uniqueness
  # TODO: change message when new translation available
  validates :email, confirmation: { message: I18n.t("models.person.should_match_confirmation") }
  validates :email,
            format: {
              with: /\A([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})\z/,
              if: proc { |record| record.email.present? }
            }

  PASSWORD_REGEX = /\A(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}\z/
  PASSWORD_REGEX_JAVASCRIPT = "(^(?=.*[A-Za-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$)".freeze
  OUTBOUND_PREFIX_TAG = "RI".freeze
  CR_OUTBOUND_PREFIX_TAG = "RI-CR".freeze
  EUR = "EUR".freeze

  validates :password, presence: true
  validates :password,
            format: {
              with: PASSWORD_REGEX,
              message: :password_requirements,
              if: :validate_password?
            }
  validates :password_confirmation, presence: { if: :validate_password? }
  validates :password, confirmation: { if: :validate_password?, message: :passwords_do_not_match }
  validate :password_not_restricted, if: :validate_password?
  validates :name, presence: { allow_blank: false }
  validates :name, length: { within: 5..80, message: :is_too_short_or_too_long, allow_blank: true }

  validates :country_website_id, presence: true

  # Terms and Conditions check - only on create because of mass password reset December 2015
  validate :accepted_terms_and_conditions_on_not_null, on: :create

  validates :apple_role, presence: { if: -> { apple_id.present? } }
  validates :apple_id, presence: { if: -> { apple_role.present? } }
  validate :country_not_japan, on: :create
  validates_with Utf8mb3Validator, fields: [:name]

  validates :country, presence: { on: :create }

  # Note: the two callbacks below add restrictions to updating the country_website_id
  validate :validate_country_website_updater_policy
  validate :valid_phone_number?, on: [:create], if: :india_user?
  before_save :invoke_country_website_updater_update
  # end note

  before_save :crypt_password
  before_save :standardize_legacy_country_code
  before_save :update_corporate_entity, if: :will_save_change_to_country?
  before_save :set_is_business
  before_create :set_migrated_to_bigbox
  after_create :add_to_targeted_offer
  after_create :initialize_person_balance
  after_create :convert_royalty_split_recipients_emails_to_people
  before_update :set_number_blank_if_plus
  before_update :update_country_state, if: :country_state_user_changing_location?
  after_update :create_profile_modification_note
  after_save :set_new_password_to_false
  after_save :remove_vat_info_and_recalculate, if: :saved_change_to_country?
  after_save :evaluate_vat_acceptance, if: :address_fields_changed?
  after_save :destroy_self_billing_status, if: :saved_change_to_corporate_entity_id?
  after_save :destroy_company_information, if: :saved_change_to_country?
  after_commit :run_after_commit_callbacks
  after_commit :migrate_referral_data, if: :referral?, on: :create

  delegate :client_payee_id, to: :payout_provider, allow_nil: true

  REASONS_FOR_DEACTIVATION = [
    "Credit Card Fraud",
    "Credit Card Fraud - Will Not Distribute",
    "Fraudulent Sales",
    "Fraudulent Sales - Will Not Distribute",
    "No Rights",
    "No Rights - Will Not Distribute",
    "PayPal Fraud",
    "PayPal Fraud - Will Not Distribute",
    "Sift Identified",
    "Sift Identified - Will Not Distribute",
    "Store End",
    "Store End - Will Not Distribute",
    "Suspicious Country IP",
    "Hacked/Ownership Dispute",
    "Hacked/Ownership Dispute - Will Not Distribute",
    "Video Fraud",
    "Video Fraud - Will Not Distribute",
    "Other",
    "Other - Will Not Distribute"
  ]

  ACTIVE = "Active".freeze
  INACTIVE = "Inactive".freeze
  SUSPICIOUS = "Suspicious".freeze
  LOCKED = "Locked".freeze

  STATUSES = [ACTIVE, INACTIVE, SUSPICIOUS, LOCKED].freeze

  enum customer_type: { business: "business", individual: "individual" }

  def potentially_monetizable_songs
    albums.facebook_monetizable
          .joins(
            albums_to_songs
          ).joins(
            songs_fbtracks_outer_join
          ).where(
            track_monetization_t[:id].eq(nil)
          ).select(
            song_t[Arel.star]
          )
  end

  def instagrammable_albums
    InstagramQueryBuilder.eligible_albums_by_person(id)
  end

  def send_instagrammable_albums
    InstagramBackfillService.backfill_by_person(id)
  end

  def grandfathered_pubadmin_user?
    return false unless youtube_monetization

    youtube_monetization.pub_opted_in
  end

  def has_never_distributed?
    !albums.distributed.not_taken_down.exists?
  end

  def distributed?
    !has_never_distributed?
  end

  def has_youtube_money?
    youtube_monetization.present?
  end

  def set_created_on
    self.created_on = Time.now
  end

  def has_automator?
    salepoint_subscriptions.active_and_album_approved_or_pending.any?
  end

  def set_new_password_to_false
    @new_password = false
  end

  def self.current_user_from_paper_trail
    Person.find_by(id: PaperTrail.request.whodunnit)
  end

  def self.remote_ip_from_paper_trail
    PaperTrail.request.controller_info[:ip]
  end

  def create_profile_modification_note
    notify_email_changes_airbrake if saved_changes.key?("email")
    return unless saved_changes? && Person.current_user_from_paper_trail.present?

    notes.create(
      related: self,
      note_created_by: Person.current_user_from_paper_trail,
      subject: "Profile Modified",
      ip_address: Person.remote_ip_from_paper_trail,
      note: Note.generate_note_from_attrs(saved_changes)
    )
  end

  #
  #  Accepted terms and conditions:
  #  -- getter and setter to perform
  #  -- boolean transformation from date
  #
  def accepted_terms_and_conditions
    !!accepted_terms_and_conditions_on
  end

  def accepted_terms_and_conditions?
    accepted_terms_and_conditions
  end

  def accepted_terms_and_conditions=(value)
    # value is zero, nil or false
    # should trigger empty of accepted_terms_and_conditions_on
    if value == "0" || !value
      self.accepted_terms_and_conditions_on = nil
    else
      self.accepted_terms_and_conditions_on ||= Time.now
    end
  end

  #
  #  Postal Code Methods
  #
  def postal_code
    if is_us? && us_zip_code
      @us_postal_code ||= us_zip_code.code
    else
      foreign_postal_code
    end
  end

  def postal_code=(code)
    # Don't perform Country check here.
    # can't be certain that the country is set already
    self.us_postal_code = code
    self.foreign_postal_code = code
  end

  delegate :locale, to: :country_website
  delegate :bcp47_locale, to: :country_website

  def fallback_locale
    return locale unless country_website.country == "US"

    USA_FALLBACK_LOCALE
  end

  # these metaprogramming method definitions are troublesome
  # They also seem to come from a time when country_website == country
  CountryWebsite::ISO_CODE_MAP.each do |iso_code, id|
    define_method("is_#{iso_code.downcase}?".to_sym) { country_website_id == id }
  end

  def format_canadian_postal_code
    # remove any spaces from the Canadian postal code
    self.foreign_postal_code = foreign_postal_code.delete(" ") unless foreign_postal_code.nil?
  end

  # used only to determine if we need to validate the CA postal code
  # we can't use is_ca? since, a ca user could eventually move to another country and want to change their address
  def validate_ca_postal_code?
    country == "Canada" && (new_record? || country_changed?)
  end

  def validate_ca_postal_code
    return unless CanadaPostalCode.find_by(code: foreign_postal_code).nil?

    errors.add(:postal_code, I18n.t("models.person.is_not_valid_canadian_postal_code"))
  end

  # TODO investigate using actual iso codes (Country::UNITED_STATES_AND_TERRITORIES) here in gs-8084
  def from_united_states_and_territories?
    self[:country].present? && Country::UNITED_STATES_AND_TERRITORIES.include?(self[:country])
  end

  def enough_for_eft_withdrawal?
    balance >= 2.76
  end

  def can_withdraw?
    balance >= 0.01
  end

  def legacy_payout_enabled?
    payout_provider.disabled?
  end

  #
  #  Sales Reporting
  #
  # provides sales reports grouped by year
  def sales_report(options = {})
    SalesReport.new(self, options)
  end

  # all stores in which the user has sales
  def stores
    salepoints.map(&:store).uniq.sort! { |a, b| a.position <=> b.position }
  end

  def total_cents_earned
    sales_records.inject(0) { |acc, elem| acc + elem.usd_total_cents }
  end

  def country_domain
    country_website.country
  end

  def initialize_person_balance
    PersonBalance.create(person: self, balance: 0, currency: currency)
  end

  def artist_name=(name)
    self.artist =
      if name.present?
        Artist.find_or_create_by(name: name)
      else
        nil
      end
  end

  # convenience for views
  def to_s
    name
  end

  def cart_items_count
    Purchase.cart_items_count(self)
  end

  # This is currently a shunt as we move from a boolean on/off admin to a role based system.
  # We just check to see if the admin has the role of admin
  def is_administrator
    roles.exists?(name: "Admin")
  end

  alias_method :is_admin?, :is_administrator
  alias_method :is_administrator?, :is_administrator

  def tfa_admin?
    has_role?("tfa_admin")
  end

  def is_download_assets_admin?
    has_role?("Download Assets")
  end

  def is_mass_download_assets_admin?
    has_role?("Mass Download Assets")
  end

  def is_publishing_manager?
    has_role?("Publishing Manager")
  end

  def is_reward_admin?
    has_role?("Reward Admin")
  end

  def is_trends_and_sales_admin?
    has_role?("Sales And Trends")
  end

  def refund_admin?
    roles.where(name: Refund::ADMIN_ROLES).any?
  end

  def flagged_suspicious?
    suspicious? || Person::FlagService.person_flag_exists?({ person: self, flag_attributes: Flag::SUSPICIOUS })
  end

  #
  #  Authenticate user by email, password
  #
  #  example:
  #  Person.authenticate("customer@tunecore.com", "testpass", 10)
  #
  #  failure: return nil
  #  success: return #<Person>
  #
  def self.authenticate(params)
    return unless person = Person.readonly.where(email: params[:email], deleted: false).first

    person.authenticate!(params[:password])
  end

  # is_permitted_to? allows you to detect whether or not the user in question can perform a specific action
  # as it's defined in the rights table.  the controller and action must be an exact match.
  # By default, it also checks to see if the user has administrative permission as well.
  # 2009-9-28 - CH - created method
  def is_permitted_to?(controller, action, require_admin = true)
    (require_admin == false or is_administrator) &&

      roles
        .includes(:rights)
        .find_by(rights: { action: action, controller: controller })
        .present?
  end

  # has_role? allows you to detect whether or not the user in question has a role with a specific name
  # By default, it also checks to see if the user has administrative permission as well.
  # 2009-9-28 - CH - created method
  def has_role?(role_name, require_admin = true)
    (!require_admin || is_administrator) && roles.exists?(name: role_name)
  end

  def has_active_access_rights?(controller, action)
    Entitlement.person_has_active_access_rights?(self, controller, action)
  end

  def access_right_entitlements(controller, action)
    Entitlement.load_access_entitlements_for_person(self, controller, action)
  end

  def has_distribution_credits_available?
    CreditUsage.credit_available_for?(self, "Album") ||
      CreditUsage.credit_available_for?(self, "Single") ||
      CreditUsage.credit_available_for?(self, "Ringtone")
  end

  #
  #  Test the given password against the user's account
  #  and perform logging and access control
  #
  def authenticate!(pass)
    result = true
    @readonly = false

    # make sure password is correct
    unless is_password_correct?(pass)
      increment_failed_login_count!
      result = false
    end

    # make sure account is not locked
    if account_locked?
      log_failed_locked_login_attempt!
      result = false
    end

    if result
      # Check and mark if this is the first time the user is logging in
      if recent_login.blank?
        self.first_login = true
        ClearFailedLoginAttemptsService.clear(id)
      else
        clear_failed_login_attempts!
      end

      self
    else
      nil
    end
  end

  def log_failed_locked_login_attempt!
    update_attribute(:recent_login_failure, Time.now)
  end

  def clear_failed_login_attempts!
    ClearFailedLoginAttemptsWorker.perform_async(id)
  end

  # Check the given password against the
  # salted stored password
  def is_password_correct?(pass)
    salted_pass = self.class.salt_my_password(salt, self.class.upgraded_hash(pass), self)

    ActiveSupport::SecurityUtils.secure_compare(password, salted_pass)
  end

  def increment_failed_login_count!
    self.login_attempts += 1
    lock_account if login_attempts >= 10
    save
  end

  def increment_invalid_redemption_count
    self.redemption_attempts += 1
    lock_redemption if redemption_attempts >= 10
    save
  end

  # returns true if the account is locked
  def account_locked?
    return true unless active?
    return false unless account_locked_until

    # unlock account when time has exceeded account_locked_until
    if account_should_be_unlocked?
      unlock_account
      false
    else
      true
    end
  end

  def redemption_locked?
    return false unless redemption_locked_until
    return true if Time.now <= redemption_locked_until

    unlock_redemption if Time.now > redemption_locked_until
    false
  end

  def account_should_be_unlocked?
    return false unless account_locked_until

    login_attempts >= 10 && Time.now > account_locked_until
  end

  def self.account_locked?(email)
    return true unless email
    return false unless person = Person.find_by(email: email)

    person.account_locked?
  end

  # locks the account until a specified time
  def lock_account
    # make account_locked_until stick even if the user keeps trying to login
    # we don't want the account to try and double-lock
    self.account_locked_until = 4.hours.from_now
  end

  def lock_redemption
    self.redemption_locked_until = 4.hours.from_now
  end

  # unlocks the account
  def unlock_account
    update(account_locked_until: nil, login_attempts: 0)
    save(validate: false)
  end

  def unlock_redemption
    update(redemption_locked_until: nil, redemption_attempts: 0)
    save(validate: false)
  end

  def active?
    status != "Locked"
  end

  def deleted?
    deleted == 1
  end

  def lock!(reason, takedown)
    self.status = "Locked"
    self.status_updated_at = Time.now
    self.lock_reason = reason
    save(validate: false)

    return unless takedown

    albums.distributed.not_taken_down.each(&:takedown!)
    singles.distributed.not_taken_down.each(&:takedown!)
    ringtones.distributed.not_taken_down.each(&:takedown!)
  end

  def activate!
    self.status = "Active"
    self.status_updated_at = Time.now
    self.lock_reason = nil
    save(validate: false)
  end

  def mark_as_suspicious!(_note, reason = nil)
    self.status = SUSPICIOUS
    self.status_updated_at = Time.now
    self.lock_reason = reason
    save(validate: false)
    # deliver notification to fraud_report@tunecore.com
    # AdminNotifier.suspicious_activity(self, note).deliver
  end

  def first_upcoming_renewal_expires_at
    renewals.first_expiration_after_date(Time.now.to_date)
            .order("expires_at ASC").pluck(:expires_at).first
  end

  #
  # Checks to see if all legacy songs are migrated to bigbox
  #
  def check_and_set_migrated_to_bigbox
    return unless songs.where("songs.s3_asset_id is null and songs.s3_orig_asset_id is not null").count.zero?

    update_attribute(:migrated_to_bigbox, true)
  end

  #
  # Creates bigbox jobs for all songs that have only a legacy asset
  #
  def migrate_to_bigbox
    return 0 if migrated_to_bigbox?

    sent_songs = 0

    # Send all albums songs to bigbox
    albums.each do |album|
      album.songs.each do |s|
        # Send songs that have an original asset but no s3_asset
        if s.s3_asset.blank? and s.s3_orig_asset
          s.send_to_bigbox(id)
          sent_songs += 1
        end
      end
    end

    sent_songs
  end

  def remove_vat_info_and_recalculate
    vat_information.invalidate! if vat_information
    recalculate_unpaid_purchases
  end

  def destroy_self_billing_status
    self_billing_status&.destroy if corporate_entity.name == CorporateEntity::TUNECORE_US_NAME
  end

  def recalculate_unpaid_purchases
    TcVat::BulkVatCalculator.new(self).calculate!(
      purchases.unpaid.pluck(:id)
    )
  end

  def remove_suspicion!
    self.status = "Active"
    self.status_updated_at = Time.now
    self.lock_reason = nil
    save(validate: false)
    Person::FlagService.unflag!({ person: self, flag_attributes: Flag::BLOCKED_FROM_DISTRIBUTION })
  end

  def suspicious?
    status == "Suspicious"
  end

  def mark_as_deleted(_note, reason = nil)
    if deleted.zero? && !purchased_before?
      self.email = email + ".DELETED" unless (email.last(8) == ".DELETED")
      self.deleted = 1
      lock!(reason, false)
    else
      errors.add(:base, "User has already been deleted.") if deleted != 0
      errors.add(:base, "User that has purchased before can't be deleted.") if purchased_before?
    end
  end

  def remove_deletion!
    self.email = email.sub(/\.DELETED$/, "") if email.last(8) == ".DELETED"
    self.deleted = 0
    activate!
  end

  def self.hashed(str)
    Digest::SHA1.hexdigest("#{SECRETS['hash_prefix']}--#{str}--")[0..39]
  end

  def self.upgraded_hash(str)
    Digest::SHA2.hexdigest("#{SECRETS['upgraded_hash_prefix']}--#{str}--")
  end

  def self.salt_my_password(salt, hashed_password, _person)
    upgraded_hash(salt + hashed_password)
  end

  def salt_my_password(salt, hashed_password)
    self.class.salt_my_password(salt, hashed_password, self)
  end

  # Has enough in balance to withdraw by check
  def enough_for_check_withdrawal?
    person_balance.balance >= 103.00
  end

  # Represent balance_cents as Money
  def balance
    person_balance.balance.truncate(2)
  end

  def balance_cents
    Tunecore::Numbers.decimal_to_cents(person_balance.balance.truncate(2))
  end

  def available_balance
    balance - hold_amount.to_f
  end

  def available_balance_cents
    balance_cents - hold_amount_cents
  end

  def hold_amount
    copyright_claims.active.sum(:hold_amount).truncate(2)
  end

  def hold_amount_cents
    Tunecore::Numbers.decimal_to_cents(hold_amount.to_f)
  end

  def account_held?
    copyright_claims.active.any? || account_locked? || suspicious?
  end

  #### PERSON PREFERENCES HELPER METHODS #####

  def preferences_set?
    person_preference.present?
  end

  def autorenew?
    # autorenew if their preferences are not set, or they have preferences set to renew (default)
    !preferences_set? || person_preference.do_not_autorenew.zero?
  end

  def pay_with_balance?
    !preferences_set? || (preferences_set? && person_preference.pay_with_balance == 1)
  end

  def preferred_paypal_account?
    preferences_set? && person_preference.preferred_paypal_account.present?
  end

  def preferred_adyen_payment_method?
    preferences_set? && person_preference.preferred_adyen_payment_method.present?
  end

  def preferred_credit_card?
    preferences_set? && person_preference.preferred_credit_card.present? && !person_preference.preferred_credit_card.expired?
  end

  def preferred_credit_card_expired?
    autorenew? && preferences_set? && person_preference.preferred_payment_type == "CreditCard" && person_preference.preferred_credit_card_id.present? && preferred_credit_card.expired?
  end

  # All renewal methods go through the following 3 methods
  def renew_with_balance?
    # If the person has not set a preference, we pay with balance first
    autorenew? && pay_with_balance?
  end

  def renew_with_credit_card?
    autorenew? && preferred_credit_card? && person_preference.preferred_payment_type == "CreditCard"
  end

  def renew_with_paypal?
    autorenew? && preferred_paypal_account? && person_preference.preferred_payment_type == "PayPal"
  end

  def renew_with_adyen?
    autorenew? && preferred_adyen_payment_method? && person_preference.preferred_payment_type == "Adyen"
  end

  def can_autorenew?
    renew_with_balance? || renew_with_credit_card? || renew_with_paypal?
  end

  def has_upcoming_autorenewal?
    Person::AutoRenewalPeopleService.new.call.exists?(id: id)
  end

  def has_upcoming_tc_social_renewal?
    Person::ActiveSocialSubscriptionPeopleService.new.call.exists?(id: id)
  end

  def preferred_credit_card_id
    person_preference.preferred_credit_card_id if preferred_credit_card?
  end

  def preferred_paypal_account
    person_preference.preferred_paypal_account if preferred_paypal_account?
  end

  def prefered_adyen_payment_method
    person_preference.preferred_adyen_payment_method if preferred_adyen_payment_method?
  end

  def preferred_payment_account
    if renew_with_paypal?
      preferred_paypal_account
    else
      (renew_with_credit_card? ? person_preference.preferred_credit_card : nil)
    end
  end

  def allow_paypal?
    CountryWebsiteService.paypal_available?(self)
  end

  def allow_adyen?
    FeatureFlipper.show_feature?(:adyen_integration, self)
  end

  #### END PERSON PREFERENCES HELPER METHODS #####

  # tested in invoice_spec
  def purchased_before?
    Invoice.previous_customer?(self)
  end

  def purchased_distribution_before?
    @purchased_distribution_before ||=
      invoices
      .settled_at_present
      .excluding_products(Product.distribution_product_ids)
      .exists?
  end

  def purchased_distribution_credits_before?
    @purchased_distribution_credits_before ||=
      invoices
      .settled_at_present
      .including_products(Product::CREDIT_PRODUCT_IDS)
      .exists?
  end

  def paid_invoice_count
    invoices.paid.size
  end

  def has_stored_bank_account?
    !stored_bank_accounts.empty? && stored_bank_accounts.collect(&:deleted_at).include?(nil)
  end

  def purchased_pub_admin?
    publishing_composers&.purchased_pub_admin&.any? || composers&.purchased_pub_admin&.any?
  end

  def eft_batch_transactions
    if stored_bank_accounts.empty?
      EftBatchTransaction.where("stored_bank_account_id = 0")
    else
      EftBatchTransaction.where(stored_bank_account_id: stored_bank_accounts.collect(&:id))
    end
  end

  def current_eft_batch_transaction
    !stored_bank_accounts.empty? && eft_batch_transactions.last
  end

  def pending_eft_batch_transactions
    if !stored_bank_accounts.empty? && !eft_batch_transactions.empty?
      eft_batch_transactions.select(&:processing?)
    else
      []
    end
  end

  def current_paypal_transfer
    paypal_transfers.last
  end

  def current_check_transfer
    check_transfers.last
  end

  def pending_paypal_transfers
    if paypal_transfers.empty?
      []
    else
      paypal_transfers.select { |transfer| transfer.transfer_status == "pending" }
    end
  end

  def pending_check_transfers
    if check_transfers.empty?
      []
    else
      check_transfers.select { |transfer| transfer.transfer_status == "pending" }
    end
  end

  def has_pending_eft_batch_transaction?
    !stored_bank_accounts.empty? && !(eft_batch_transactions.collect(&:status) & EftBatchTransaction::PROCESSING_STATUSES).empty?
  end

  def has_pending_paypal_transfer?
    paypal_transfers.collect(&:transfer_status).include?("pending")
  end

  def has_pending_check_transfer?
    check_transfers.collect(&:transfer_status).include?("pending")
  end

  def has_pending_payout?
    has_pending_eft_batch_transaction? || has_pending_paypal_transfer? || has_pending_check_transfer?
  end

  def domain
    country_website.url
  end

  def password=(new_pass)
    return if new_pass.blank?

    super(new_pass)
    @new_password = true
  end

  def change_password(pass, confirm = nil)
    self.password = pass
    self.password_confirmation = confirm.nil? ? pass : confirm
    @new_password = true
  end

  def clear_invite
    clear_invite_fields
    save
  end

  def confirm_invite(key)
    return false unless key
    return false if is_verified == 1
    return false unless valid_invite?(key)

    self.is_verified = 1
    clear_invite_fields
    save
    true
  end

  def mark_as_verified
    self.is_verified = true
    save
  end

  def valid_invite?(key)
    valid = invite_active? && invite_code == key
    Rails.logger.info("Invalid invite code used for Person ID #{id}: (#{key})") unless valid
    valid
  end

  def invite_active?
    Rails.logger.info("Checking active invite for Person ID #{id}.")

    active = !!invite_code
    Rails.logger.info("Invite invalid because does not exist.") and return active unless active

    active = !!invite_expiry
    Rails.logger.info("Invite invalid because expiry does not exist.") and return active unless active

    active = Time.now < invite_expiry
    Rails.logger.info("Invite invalid because expiry has past.") and return active unless active

    active
  end

  def generate_invite_code(hours = 72)
    self.invite_code = SecureRandom.hex
    self.invite_expiry = Time.now + hours.hours
    if save
      invite_code
    else
      Rails.logger.error("Unable to generate invite code for person_id #{id}: #{errors.full_messages.join(',')}")
      nil
    end
  end

  def clear_invite_fields
    self.invite_code = nil
    self.invite_expiry = nil
  end

  def artist_name
    artist.name if artist
  end

  def first_album
    albums.first.name if albums
  end

  def first_distribution?(album)
    if album && album.is_a?(Album)
      distributed_albums = albums
                           .distributed
                           .where.not(id: album.id)
                           .where("albums.finalized_at < ?", album.finalized_at)
      return distributed_albums.empty?
    end
    false
  end

  def label_name
    if attributes["label_name"].present?
      attributes["label_name"]
    elsif label.present?
      label.name
    else
      nil
    end
  end

  def label_name=(arg)
    self.label = Label.find_or_create_by(name: arg)
  end

  # Re-implementation of LoginEngine's method, which is no longer being used.
  def set_delete_after
    delete_day = AccountSystem.config[:delayed_delete_days]
    update_attribute(:delete_after, delete_day.days.from_now) if delete_day
  end

  def self.count_with_albums
    connection.select_values("select count(distinct(person_id)) from albums where is_deleted = 0").first.to_i
  end

  def self.leads_for(country_names)
    if country_names.empty?
      []
    else
      Person.includes(:invoices, :person_balance, :person_transactions).where(
        country_website_id: 1,
        country: country_names, invoices: { person_id: nil }, person_balances: { balance: 0 }, person_transactions: { person_id: nil }
      )
    end
  end

  def serial(program)
    Serial.find_by(person_id: self, program: program)
  end

  def is_video_customer?
    videos.size.positive? ? true : false
  end

  def months_with_reports
    (PersonTransaction.transaction_months(self) + ReportingMonth.all_for_person(self).map(&:report_date)).uniq.sort.reverse
  end

  def okay_to_release?(_lockable)
    true
  end

  def subscribe
    return "#{name} is already subscribed" if is_opted_in?

    update_attribute :is_opted_in, true
    "Subscribed #{name}"
  end

  def unsubscribe
    return "#{name} is already unsubscribed" unless is_opted_in?

    update_attribute :is_opted_in, false
    "Unsubscribed #{name}"
  end

  def has_albums_eligible_for_early_renewal?
    raise "this has been turned off, but being called from somewhere..."
    albums_eligible_for_early_renewal.size.positive?
  end

  def albums_eligible_for_early_renewal
    start_date = Date.new((Date.today >> 3).year, (Date.today >> 3).month, 1)
    end_date = Date.new((Date.today >> 12).year, (Date.today >> 12).month, 1)
    albums.select { |album|
      album.renewal_due_on >= start_date && album.renewal_due_on < end_date && !album.has_unpaid_renewal? && album.takedown_at.blank? && !album.is_deleted? && !album.locked?
    }
  end

  # We're wrapping the interstitial_shown_on as to get around migrating the db (we're just reusing)
  def beta_user?
    interstitial_shown_on.present?
  end

  def first_initial_and_last_name
    "#{first_name.first}. #{last_name}"
  end

  def first_name
    @first_name || first_name_from_name
  end

  def last_name
    @last_name || last_name_from_name
  end

  def artists_for_tour_date
    # return artists from album, single, & ringtone that are not taken down and not in live
    # processing, or sent state. ie all artists associated with an album, single, & ringtone
    # with streamable songs
    albums.collect(&:creatives).flatten.collect(&:artist).uniq
  end

  # Used to control what dashboard page version is displayed
  def dashboard_state(cached_albums = nil)
    ## CUSTOMER STATES
    #
    # has not verified email -- level 0 (taken care of by another controller--the user cannot do anything and are directed to a verification page)
    # virgin -- level 1 -- Users has not created an album at all
    # unfinalized -- level 2 -- User has created one or more albums, but none of them are able to be published (finalized)
    # should_finalize -- level 3 -- User has created one or more albums, and at least one of them is ready to be published (finalized)
    # published -- level 4 -- User has paid for delivery but we have no sales data/don't know that it is live in a store -- Currently unused
    # selling -- level 5 -- We have recieved confirmation from and iTunes Comprehensive or sales data that an album is live
    #
    albums = cached_albums || self.albums

    if albums.not_deleted.limit(1).pluck(:id).blank? &&
       legacy_user?

      "virgin"
    elsif has_finalized_release?
      if has_live_release?
        "selling"
      else
        "no_longer_live"
      end
    elsif albums
          .includes(:artwork, [{ songs: :upload }], :salepoints, :creatives)
          .not_deleted
          .any?(&:should_finalize?)

      "should_finalize"
    else
      "unfinalized"
    end
  end

  def permissable_content_review_reasons
    reasons = ReviewReason.includes(:roles).all
    permissable_reasons = []
    reasons.each do |reason|
      permissable_reasons << reason if reason.roles.any? { |r| roles.include?(r) }
    end
    permissable_reasons
  end

  def enable_auto_renewal(note)
    set_auto_renewal(true, note)
  end

  def disable_auto_renewal(note)
    set_auto_renewal(false, note)
  end

  def feature_enabled?(symbol)
    features.find_by(name: symbol.to_s).present?
  end

  # Returns an ItemCollection of the person's Bigbox assets
  # The ItemCollection is loaded lazily, so this method should return very quickly.
  # The items will not be loaded from SDB until you actually try to use them.
  #
  # If you want a pre-loaded list of assets use Person#bigbox_assets_hashes()
  def bigbox_assets
    sdb = AWS::SimpleDB.new(
      {
        access_key_id: AWS_ACCESS_KEY,
        secret_access_key: AWS_SECRET_KEY,
        max_retries: 10,
        ssl_verify_peer: false
      }
    )
    domain = sdb.domains.create(BIGBOX_ASSETS_SDB_DOMAIN)

    query = "locker_name = '#{id}'  "

    domain.items.select("*").where(query)
  end

  def bigbox_assets_hashes
    item_collection = bigbox_assets
    items = []
    item_collection.each do |item|
      item_hash = item.attributes
      item_hash["uuid"] = [item.name]
      items << item_hash
    end
    items
  end

  def paid_for_composer?
    if account
      account.composers.any?(&:is_paid)
    else
      composers.any?(&:is_paid)
    end
  end

  def paid_for_publishing_composer?
    if account
      account.publishing_composers.any?(&:is_paid)
    else
      publishing_composers.any?(&:is_paid)
    end
  end

  def paid_for_composer_on
    composer_product_ids = Product.where(display_name: "pub_admin").map(&:id)
    purchases.paid.find { |p| composer_product_ids.include?(p.product.id) }.try(:paid_at)
  end

  def paid_for_publishing_composer_on
    paid_for_composer_on
  end

  def paid_for_facebook_track_monetization?
    facebook_product_ids = Product.where(display_name: "fb_track_monetization").map(&:id)
    purchases.paid.find { |p| facebook_product_ids.include?(p.product.id) }.try(:paid_at)
  end

  # return true if person has selected any splits
  def selected_splits?
    composers.any?(&:selected_splits?)
  end

  # returns true if person has distributions that still need split selection
  def missing_splits?
    songs.any? { |s| s.composition_id.nil? }
  end

  # return true if person has at least one finalized release
  def has_finalized_release?
    !albums.finalized.limit(1).pluck(:id).empty?
  end

  def has_live_release?
    albums.finalized.not_taken_down.length.positive?
  end

  def convert_to_canadian
    if country_website_id == 1
      # remove all of the removable items that are tied to currency/country
      # these are all safe-destroys, in that they check if they are allowed to be destroyed
      # e.g., a paid purchase or a used targeted offer can not be deleted
      offers = TargetedOffer.for_person(self) # storing this so we can add the person back in to the offer if we don't convert them.
      remove_from_all_targeted_offers
      remove_unpaid_invoices
      remove_unpaid_purchases
      # then verify that the person has no more country specific items, and change their country_website and balance currency
      if !has_country_specific_items? && person_balance.balance == 0.00
        update_attribute(:country_website_id, 2) # note this skips validation, so ignores invalid postal codes, etc.
        person_balance.update(currency: "CAD")
      else
        # if we don't convert them to CA, put them back in their targeted offers
        offers.each do |offer|
          if TargetedOffer.find_by(id: offer.id) && !targeted_people.reload.collect(&:id).include?(offer.id) # make sure the person isn't still in the offer
            TargetedOffer.find_by(id: offer.id).add_person(self)
          end
        end
        false
      end
    else
      false
    end
  end

  def has_country_specific_items?
    !invoices.reload.first.nil? || !purchases.reload.first.nil? || !targeted_people.reload.first.nil? || !braintree_transactions.first.nil? ||
      !paypal_transactions.first.nil? || !person_transactions.first.nil? || !stored_bank_accounts.first.nil? || !stored_paypal_accounts.first.nil? ||
      !person_intakes.first.nil? || !inventories.first.nil? || !check_transfers.first.nil? || !paypal_transfers.first.nil? ||
      !certs.first.nil? || !balance_adjustments.first.nil?
  end

  def missing_on_sale_stores?(stores_on_sale)
    missing_on_sale_stores(stores_on_sale).any?
  end

  def missing_on_sale_stores(stores_on_sale)
    stores_on_sale.each_with_object([]) do |store, missing_stores|
      missing_stores << store if missing_store?(store)
    end
  end

  def has_store(store_name)
    albums.each do |album|
      next unless album.finalized? && album.status != "down"

      album.stores.each do |store|
        return true if store.name == store_name
      end
    end
    false
  end

  # this returns all of a person's notes for their related items
  # (currently: Person Notes, Album Notes, Song Notes, and EFT Batch transaction Notes)
  def all_notes
    temp_array = [] # start with empty array

    # add existing person notes and related album, song, eft batch transaction notes
    # temporarily adding limit here to fix performance issues
    if notes
      (temp_array = Note.includes(:note_created_by, :related)
        .where(["related_type = 'Person' AND related_id = ?", id])
        .order("id DESC").limit(2000))
    end
    unless eft_batch_transactions.nil?
      (temp_array += Note.includes(:note_created_by, :related)
        .where(["related_type = 'EftBatchTransaction' AND related_id IN (?)", eft_batch_transactions.collect(&:id)])
        .order("id DESC").limit(2000))
    end
    if albums.present?
      (temp_array += Note.includes(:note_created_by, :related)
        .where(["related_type = 'Album' AND related_id IN (?)", albums.collect(&:id)])
        .order("id DESC").limit(2000))
    end
    unless songs.nil?
      (temp_array += Note.includes(:note_created_by, :related)
        .where(["related_type = 'Song' AND related_id IN (?)", songs.collect(&:id)])
        .order("id DESC").limit(2000))
    end

    # sort descending by id to show latest first
    temp_array.sort! { |a, b| b.id <=> a.id }

    temp_array
  end

  def display_recent_notes_warning?
    display_only_recent_notes = true if notes && Note.where(related_id: id).count > 2000
    if !display_only_recent_notes && eft_batch_transactions.present? && Note.where(
      ["related_type = 'EftBatchTransaction' AND related_id IN (?)", eft_batch_transactions.collect(&:id)]
    ).count > 2000

      display_only_recent_notes = true
    end
    if !display_only_recent_notes && albums.present? && Note.where(
      ["related_type = 'Album' AND related_id IN (?)", albums.collect(&:id)]
    ).count > 2000

      display_only_recent_notes = true
    end
    if !display_only_recent_notes && songs.present? && Note.where(
      ["related_type = 'Song' AND related_id IN (?)", songs.collect(&:id)]
    ).count > 2000

      display_only_recent_notes = true
    end

    display_only_recent_notes
  end

  def is_ambassador?
    friend_referral_campaigns.exists?(active: true)
  end

  def created_at_time
    created_on.in_time_zone("America/Los_Angeles")
  end

  def email_uniqueness_validated?
    @email_uniqueness_validated
  end

  def self.validate_user(id, email)
    p = Person.find_by(id: id, email: email)
    raise ActiveRecord::RecordNotFound unless p

    p.is_verified?
  end

  def analytic_by_metric(metric_type)
    person_analytics.where(metric_name: metric_type).first
  end

  def create_analytic_by_metric_name_and_value(metric_name, metric_value)
    person_analytics.create(metric_name: metric_name, metric_value: metric_value)
  end

  def record_login(ip_address)
    update(recent_login: Time.current, last_logged_in_ip: ip_address)
  end

  def skip_login_event_logging?
    skip_for_emails = ENV.fetch("SKIP_LOGIN_EVENT_LOGGING_FOR_USERS", "")
    skip_for_emails.split(",").include?(email)
  end

  def create_logged_in_event(ip_address, user_agent)
    return if skip_login_event_logging?

    login_events.create(ip_address: ip_address, user_agent: user_agent)
  end

  def default_language
    (country_website_id == 5) ? "de" : "en"
  end

  def referred_by(referral_string)
    (referral_type == referral_string || !!referral_data.detect { |rd|
       rd.key == "ref" && rd.value == referral_string
     }) || false
  end

  def join_token
    referral_data.detect { |rd| rd.key == "jt" }.try(:value)
  end

  def referred_by_friend?
    !friend_referral_code.nil?
  end

  def friend_referral_code
    referral_data.where(key: ReferAFriend::PurchaseHandler.referral_code_key).first.try(:value)
  end

  def has_join_token?
    !!join_token
  end

  def has_tc_social?
    has_active_subscription_product_for?("Social") ||
      !!tc_social_token
  end

  def tc_social_token
    tc_social_tokens.find_by(type: "Oauth2Token").try(:token)
  end

  # TODO we should have an active record relationship between person and country
  # this is awful, but everywhere. We need to develop a strategy
  # for rolling out an active record relationship to replace this
  def country
    if self[:country] && self[:country].length == 2
      Country.country_name(self[:country])
    else
      self[:country]
    end
  end

  def country_name
    # This should eventually replace the above Person#country method,
    # and hopefully lead us towards letting Person#country just be a column
    if self[:country] && self[:country].length == 2
      Country.country_name(self[:country])
    else
      self[:country]
    end
  end

  def country_sanctioned?
    self[:country].in?(Country::CURRENT_SANCTIONED_COUNTRIES_ISO_CODES)
  end

  def country_iso_code
    return self[:country] if self[:country].to_s.length == 2

    Country.country_iso_code(self[:country])
  end

  delegate :live_releases, to: :albums

  def distributed_albums?
    albums.distributed_albums.exists?
  end

  def new_user_referred_by_friend?
    id.nil? && referral_data.select { |x| x.key == "fbuy_ref_code" }.present?
  end

  def has_encumbrance?
    encumbrance_amount.positive?
  end

  def encumbrance_amount
    @encumbrance_amount ||= EncumbranceSummary.encumbrance_amount_for_person(self)
  end

  def last_login_event
    login_events.last
  end

  def last_logged_in_at
    last_login_event&.created_at
  end

  def releases_eligible_for_youtube_art_tracks_store?
    youtube_art_tracks_store_id = Store.find_by(short_name: "Google").id
    yt_salepoints = Album.joins(:salepoints).where(salepoints: { store_id: youtube_art_tracks_store_id })

    albums.includes(:salepoints)
          .where(payment_applied: true, takedown_at: nil, is_deleted: false)
          .where(legal_review_state: ["APPROVED", "DO NOT REVIEW"])
          .where.not(album_type: "Ringtone")
          .where.not(id: yt_salepoints)
          .any?
  end

  def imported_from_stem?
    referral.to_s.casecmp?("stem")
  end

  def no_payment_on_file?
    stored_credit_cards.blank? && stored_paypal_accounts.blank?
  end

  def stem_user_without_payment?
    imported_from_stem? && no_payment_on_file?
  end

  def stem_free_trial_days_left
    return 0 unless imported_from_stem?

    MassIngestion::Stem.free_trial_days_left
  end

  def blocked_for_stem?
    stem_user_without_payment? && !feature_enabled?(:disable_stem_modal)
  end

  def prefers_cc?
    person_preference&.preferred_payment_type == "CreditCard" || false
  end

  def cc_expiring?
    preferred_credit_card&.expires_before_date?(Date.today + 45.days) || false
  end

  def active_distributions?
    albums.where(legal_review_state: ReviewAudit::APPROVED, takedown_at: nil).any?
  end

  def check_credit_card_expiration_date
    return unless prefers_cc?
    return unless cc_expiring?
    return unless active_distributions?

    preferred_credit_card&.days_until_credit_card_expires
  end

  def post_update_password(password, remote_ip, user_agent)
    unlock_account if account_locked?
    PersonNotifier.change_password(self, password).deliver
    create_logged_in_event(remote_ip, user_agent)
    clear_invite
  end

  def update_phone_number(phone_number: nil, mobile_number: nil)
    self.phone_number = phone_number
    self.mobile_number = mobile_number
    return update(phone_number: self.phone_number, mobile_number: self.mobile_number) unless india_user?

    update(phone_number: self.phone_number, mobile_number: self.mobile_number) if valid_phone_number?
  end

  def valid_phone_number?
    valid = true
    set_number_blank_if_plus
    valid = false unless valid_phone_number_formats?

    if phone_number.blank? && mobile_number.blank?
      add_blank_telephone_error
      valid = false
    end
    valid = false if invalid_phone_number_lengths?

    valid
  end

  def india_user?
    country_website_id == CountryWebsite::INDIA
  end

  def india_country_and_domain?
    india_user? && Country::INDIA_NAME_AND_ISO.include?(self[:country].downcase)
  end

  def legacy_paypal_enabled?
    payoneer_enabled = Payoneer::FeatureService.payoneer_payout_enabled?(self)
    redesigned_onboarding_enabled = Payoneer::FeatureService.payoneer_redesigned_onboarding?(self)
    payoneer_enabled && !redesigned_onboarding_enabled
  end

  def latest_gst_info
    gst_infos.order(:created_at).last
  end

  def german_user?
    country_website_id == CountryWebsite::GERMANY
  end

  delegate :user_currency, :country_wesbite_currency_symbol, to: :country_website

  def country_name_untranslated
    # In some cases, instead of the ISO code, the country name itself will be present in this column.
    # Bypassing the country fetch using ISO code for the above case.
    return self[:country] if self[:country] && self[:country].length != 2

    Country.country_name(self[:country], true)
  end

  def country_region
    Country.find_by(name: country_name_untranslated)&.region
  end

  delegate :site_currency, to: :country_website

  def tfa_method(alternate_method = false)
    alternate_method ? two_factor_auth.alternate_method : two_factor_auth.notification_method
  end

  def update_corporate_entity
    return if self[:country].blank?

    country = Country.find_by(name: country_name_untranslated)
    self.corporate_entity =
      if country.present?
        country.corporate_entity
      else
        CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
      end
  end

  def first_release?
    albums.finalized.one?
  end

  def active_country_audit
    country_audits.last
  end

  def in_eu?
    Country::EU_COUNTRIES.include?(self[:country])
  end

  def affiliated_to_bi?
    corporate_entity.try(:name) == "BI Luxembourg"
  end

  def required_address_info?
    return true if from_united_states_and_territories?

    [
      address1,
      city,
      country,
      foreign_postal_code
    ].all?(&:present?)
  end

  # indian users should all have states, but we have a few that don't. This is the one time work around for that.
  # We should fix the users that don't have states
  def person_gst_config
    country_state = CountryState.find_by(name: state)
    return country_state.gst_config if country_state.present?

    payments_os_card = stored_credit_cards.payments_os_cards.last
    payments_os_card.country_state.gst_config if payments_os_card.present?
  end

  def suspicious_email_sent!
    $redis.setex(suspicious_email_key, 600, true)
  end

  def suspicious_email_sent_recently?
    $redis.get(suspicious_email_key).present?
  end

  def suspicious_email_key
    "user_#{id}:SUS_EMAIL_SENT"
  end

  def has_store_salepoints?(store_short_name)
    albums.joins(salepoints: :store).where(salepoints: { stores: { short_name: store_short_name } }).limit(1).count.positive?
  end

  def store_salepoints_for_finalized_albums(store_short_name)
    albums.finalized_store_salepoints(store_short_name)
  end

  def active_store_salepoints?(store_short_name)
    store_salepoints_for_finalized_albums(store_short_name)
      .approved_or_pending_approval
      .exists?
  end

  def paid_unfinalized_store_salepoints?(store_short_name)
    store_salepoints_for_finalized_albums(store_short_name)
      .joins(salepoints: :inventory_usages)
      .exists?
  end

  def store_distributions(store_short_name, has_n_distributions = nil)
    albums.approved
          .joins(salepoints: [:store, :distributions])
          .where(salepoints: { stores: { short_name: store_short_name } })
          .where(distributions: { state: "delivered" })
          .limit(has_n_distributions || 1)
  end

  def has_store_distributions?(store_short_name, has_n_distributions = nil)
    count = store_distributions(store_short_name, has_n_distributions).count

    if has_n_distributions
      count >= has_n_distributions
    else
      count.positive?
    end
  end

  def outbound_invoice_prefix
    "#{OUTBOUND_PREFIX_TAG}-#{'%07d' % id}"
  end

  def cr_outbound_invoice_prefix
    "#{CR_OUTBOUND_PREFIX_TAG}-#{'%07d' % id}"
  end

  def eur_fx_rate
    ForeignExchangeRate.latest_by_currency(source: currency, target: EUR)
  end

  def vat_applicable?
    vat_feature_on? && affiliated_to_bi?
  end

  def vat_feature_on?
    FeatureFlipper.show_feature?(:vat_tax, self)
  end

  def new_refunds_feature_on?
    FeatureFlipper.show_feature?(:new_refunds, self)
  end

  def chargebacks_feature_on?
    FeatureFlipper.show_feature?(:chargebacks, self)
  end

  def adyen_renewals_feature_on?
    FeatureFlipper.show_feature?(:adyen_renewals, self)
  end

  def update_address_with_country_audit(country_audit_source, address_params)
    return unless FeatureFlipper.show_feature?(:bi_transfer, self)
    return if address_params.blank?

    merged_address_params = address_params.merge(
      {
        require_address: true,
        country_audit_source: country_audit_source
      }
    )

    update(merged_address_params)
  end

  def compliant_contact_full_name
    first_name = compliance_contact_hash[:first_name][:field_value]
    last_name = compliance_contact_hash[:last_name][:field_value]
    full_name = %(#{first_name} #{last_name})
    return full_name if full_name.present?

    name
  end

  # Use person name if the compliance_info_fields for the company name is empty
  def compliant_contact_company_name
    compliance_contact_hash[:company_name][:field_value] || name
  end

  def country_state_user_changing_location?
    country_state_domain? && location_changed?
  end

  def country_state_country?
    CountryWebsite::COUNTRY_STATE_COUNTRIES.include?(self[:country])
  end

  def country_state_domain?
    CountryWebsite::COUNTRY_STATE_COUNTRIES.include?(country_website.country)
  end

  def location_changed?
    city_changed? ||
      state_changed? ||
      country_changed? ||
      country_state_id_changed? ||
      country_state_city_id_changed?
  end

  def has_ytsr?
    has_active_subscription_product_for?("YTTracks") || has_active_ytm?
  end

  def update_country_state
    Person::CountryAndStateUpdaterService.new(self)
  end

  def recalculate_lifetime_earning
    earning_record = lifetime_earning || create_lifetime_earning
    earning_record.recalculate!
  end

  def can_use_language_selector?
    is_us? || is_ca?
  end

  def lifetime_earning_amount
    recalculate_lifetime_earning if lifetime_earning.blank?

    lifetime_earning.total_intake
  end

  def reward_eligible_releases
    albums.distributed.approved.not_taken_down
  end

  def non_self_country_details
    countries_from_non_self_sources.map do |country|
      {
        iso_code: country.iso_code,
        name: country.name_raw,
        affiliated_to_bi: country.corporate_entity.affiliated_to_bi?,
        corporate_entity_id: country.corporate_entity_id,
        is_eu: country.eu?
      }
    end
  end

  def vat_and_bi_enabled?
    vat_feature_on? && bi_transfer_feature_on?
  end

  def bi_transfer_feature_on?
    FeatureFlipper.show_feature?(:bi_transfer, self)
  end

  def person_country_vat_rates
    self_countries = countries_from_non_self_sources.pluck(:name)
    country_list = JSON.parse($redis.get("vat_applicable_countries_inbound") || "{}")
    return [] if country_list.empty?

    country_list["individual"].select { |c| self_countries.include?(c["country"]) }
  end

  def sharing_vat_number?
    return false if vat_registration_number.blank?

    VatInformation
      .where(vat_registration_number: vat_registration_number)
      .where.not(person_id: id)
      .any?
  end

  def self_billing_accepted?
    return self_billing_status.accepted? if self_billing_status

    false
  end

  def self_billing_needed?
    !self_billing_accepted?
  end

  def relevant_tier
    tiers.order(hierarchy: :desc).first
  end

  def display_rewards_program?
    current_language_unsupported_for_rewards = (I18n.t("rewards_program.rewards_program_name") == "N/A")
    return false if current_language_unsupported_for_rewards
    return false if tiers.empty?
    return false unless FeatureFlipper.show_feature?(:tier_promo, self)

    ["Suspicious", "Active"].include?(status)
  end

  def optimizely_failed_renewal
    return unless country_website_id == CountryWebsite::UNITED_STATES

    renewals.expired_live_and_not_canceled.first
  end

  def sanitize!
    self.class.select(self.class.column_names - ["password", "salt"])
        .find_by(id: id)
  end

  def last_generated_outbound_sequence
    outbound_invoices.where(user_invoice_prefix: outbound_invoice_prefix)
                     .order(invoice_sequence: :desc)
                     .first
                     &.invoice_sequence
                     .to_i
  end

  def last_generated_cr_outbound_sequence
    outbound_refunds.where(user_invoice_prefix: cr_outbound_invoice_prefix)
                    .order(invoice_sequence: :desc)
                    .first
                    &.invoice_sequence
                    .to_i
  end

  def paypal_payin_provider_config
    PayinProviderConfig.find_by!(
      name: PayinProviderConfig::PAYPAL_NAME, corporate_entity: payin_corporate_entity,
      currency: country_website.currency
    )
  end

  def payin_corporate_entity
    if FeatureFlipper.show_feature?(:money_in_bi_transfer, id)
      corporate_entity
    else
      CorporateEntity.find_by(name: CorporateEntity::TUNECORE_US_NAME)
    end
  end

  def evaluate_vat_acceptance
    return if (vat_registration_number.blank? || VatAssesmentService.new(self, vat_registration_number).acceptable?)

    remove_vat_info_and_recalculate
  end

  def current_tax_year_total_earnings
    tax_metadata = person_earnings_tax_metadata.find_by(tax_year: Date.current.year)
    return 0 if tax_metadata.blank?

    tax_metadata.total_earnings
  end

  def accurate_current_tax_year_total_earnings
    us_date_ranges =
      TaxWithholdings::TransactionRangeFinderService
      .new(id, Date.current.year)
      .call
      .select { |dates| dates[:country] == "US" }
      .pluck(:range)

    person_transactions
      .where(created_at: us_date_ranges)
      .for_all_us_taxable_targets
      .sum(:credit)
  end

  def active_artists(finalized_at = nil)
    active_creatives = creatives.distinct.active

    if finalized_at.present?
      active_creatives = active_creatives.where(
        albums: {
          finalized_at: (Time.at(0).to_datetime...finalized_at)
        }
      )
    end

    active_creatives.pluck("artists.name")
  end

  def payout_provider_identifier
    payout_provider&.provider_identifier || payout_providers.first&.provider_identifier
  end

  def linked_tunecore_accounts(active: false)
    return [] if payout_provider_identifier.nil?

    relation = Person.left_joins(:payout_providers).where(
      payout_providers: {
        provider_identifier: payout_provider_identifier
      }
    ).where.not(id: id)
    relation = relation.merge(PayoutProvider.active) if active
    relation
  end

  def tax_withheld?
    person_transactions.irs_tax_withholding_transactions.exists?
  end

  def payoneer_account?
    payout_provider &&
      (payout_provider.pending? || payout_provider.onboarding? || payout_provider.approved?)
  end

  def three_d_secure_enabled?
    FeatureFlipper.show_feature?(:money_in_bi_transfer, self)
  end

  def payment_splits_renewal_enabled?
    FeatureFlipper.show_feature?(:payment_splits_renewal, self)
  end

  def historic_plan_purchases
    purchases.plans.where.not(invoice: nil)
  end

  def enabled_indonesian_wallets?
    FeatureFlipper.show_feature?(:indonesian_wallets, self)
  end

  def enabled_adyen_integration?
    FeatureFlipper.show_feature?(:adyen_integration, self)
  end

  def adyen_customer_reference
    "PERSON-#{id}"
  end

  def clear_cart!
    purchases.unpaid.not_in_invoice.each(&:destroy)
  end

  def dolby_atmos_product
    @dolby_atmos_product ||=
      Product.find_by(country_website: country_website, display_name: Product::DOLBY_ATMOS_DISPLAY_NAME)
  end

  def primary_tax_form
    current_tax_forms_across_programs.where(payout_provider_config: payout_provider_config).take
  end

  def secondary_tax_form
    current_tax_forms_across_programs.secondary.take
  end

  def user_mapped_distribution_tax_form
    tax_form_revenue_streams.user_mapped.distribution.first&.tax_form
  end

  def user_mapped_publishing_tax_form
    tax_form_revenue_streams.user_mapped.publishing.first&.tax_form
  end

  def default_revenue_streams?
    return false if tax_form_revenue_streams.empty?

    tax_form_revenue_streams.distribution.last.user_mapped_at.nil? &&
      tax_form_revenue_streams.publishing.last.user_mapped_at.nil?
  end

  protected

  def tc_social_tokens
    tokens.joins(:client_application)
          .where(ClientApplication.arel_table[:name].eq("tc_social"))
  end

  # following 3 methods used by the convert_to_canadian_method
  def remove_from_all_targeted_offers
    targeted_people.all.find_each(&:destroy)
  end

  def remove_unpaid_invoices
    invoices.each { |invoice| invoice.destroy if invoice.can_destroy? }
  end

  def remove_unpaid_purchases
    purchases.each { |purchase| purchase.destroy unless purchase.paid? }
  end

  def standardize_legacy_country_code
    return unless LEGACY_NEW_COUNTRY_CODE_MAP.key?(self[:country])

    self.country = LEGACY_NEW_COUNTRY_CODE_MAP[self[:country]]
  end

  def assign_country_website_on_create
    self.country_website_id = CountryWebsite.country_id_for(self[:country])
  end

  def set_migrated_to_bigbox
    self.migrated_to_bigbox = true
  end

  def us_postal_code=(zip_code)
    self.us_zip_code = UsZipCode.find_by(code: zip_code)
    @us_postal_code = zip_code
  end

  def ensure_postal_codes_are_clean
    if is_us?
      self.foreign_postal_code = nil
    else
      self.us_zip_code = nil
    end

    true # return true so the validation does not fail
  end

  def first_name_from_name
    (name.split(" ", 0).first if name)
  end

  def last_name_from_name
    (name.split(" ", 0).last if name)
  end

  def add_to_targeted_offer
    TargetedOffer.add_person_with_join_token(self, join_token) if has_join_token?
  end

  def has_active_targeted_offer?
    !TargetedOffer.for_person(self).empty?
  end

  def validate_password?
    @new_password
  end

  def run_after_commit_callbacks
    Person::AfterCommitCallbacks.after_commit(self)
  end

  def crypt_password
    return unless validate_password?

    self["salt"] = self.class.random_salt
    self["password"] = self.class.salt_my_password(salt, self.class.upgraded_hash(password), self)
  end

  def password_not_restricted
    return unless PasswordRestrictionService.restricted?(password)

    errors.add(:password, I18n.t("models.person.password_not_secure"))
  end

  def password_length
    errors.add(:password, I18n.t("models.person.password_too_short")) if password.length < 8
  end

  # Must be lowercase to pass validation
  def email_downcase
    email.downcase!
    email_confirmation.downcase! if email_confirmation
  end

  def set_auto_renewal(enable, note_params = {})
    if person_preference
      person_preference.do_not_autorenew = !enable
    else
      build_person_preference(do_not_autorenew: !enable)
    end

    # Archive stored payment forms
    stored_credit_cards.active.each do |active_card|
      # Skip validation because we are removing all cards
      active_card.destroy(note_params[:ip_address], note_params[:note_created_by], skip_validation: true)
    end

    stored_paypal_accounts.active.each do |active_pp_account|
      active_pp_account.destroy(note_params[:note_created_by], skip_validation: true)
    end

    begin
      if note_params[:note].present? and person_preference.save(validate: false)
        Note.create(
          note_params.merge(
            {
              related: self,
              subject: enable ? "Enabling Auto-Renew" : "Disabling Auto-Renew"
            }
          )
        )
      else
        errors.add(:base, I18n.t("models.person.note_required_for_changing_auto_renewal"))
      end
    rescue ActiveRecord::RecordInvalid
      errors.add(:base, I18n.t("models.person.failed_to_update_auto_renewal"))
    end
  end

  def self.new_dist_customers(for_date)
    joins(<<-SQL.strip_heredoc)
      INNER JOIN person_analytics ON people.id = person_analytics.person_id
        AND person_analytics.metric_name = "first_distribution_invoice_id"
      INNER JOIN invoices ON invoices.id = person_analytics.metric_value
      INNER JOIN person_analytics person_analytics_2 ON people.id = person_analytics_2.person_id
        AND person_analytics_2.metric_name = "first_invoice_id"
      INNER JOIN invoices invoices_2 ON invoices_2.id = person_analytics_2.metric_value
      LEFT JOIN person_analytics person_analytics_3 ON people.id = person_analytics_3.person_id
        AND person_analytics_3.metric_name = "publishing_invoice_id"
      LEFT JOIN invoices invoices_3 ON invoices_3.id = person_analytics_3.metric_value
    SQL
      .select(<<-SQL.strip_heredoc)
      people.country_website_id,
      CASE
        WHEN (invoices_3.settled_at IS NULL OR invoices_3.settled_at >= invoices.settled_at)
          AND invoices_2.settled_at = invoices.settled_at THEN 'first_dist_and_new'
        WHEN (invoices_3.settled_at IS NULL OR invoices_3.settled_at >= invoices.settled_at)
          AND invoices_2.settled_at < invoices.settled_at THEN 'first_dist_only'
      END
      purchase_type,
      COUNT(distinct people.id) num_customers
    SQL
      .where("date(invoices.settled_at) = ?", for_date)
      .group("1, 2")
  end

  def self.pub_customers(for_date)
    joins(<<-SQL.strip_heredoc)
      INNER JOIN person_analytics ON people.id = person_analytics.person_id
        AND person_analytics.metric_name = "publishing_invoice_id"
      INNER JOIN invoices ON invoices.id = person_analytics.metric_value
      LEFT JOIN person_analytics person_analytics_2 ON people.id = person_analytics_2.person_id
        AND person_analytics_2.metric_name = "first_distribution_invoice_id"
      LEFT JOIN invoices invoices_2 ON invoices_2.id = person_analytics_2.metric_value
    SQL
      .select(<<-SQL.strip_heredoc)
      people.country_website_id,
      CASE
        WHEN (invoices_2.settled_at IS NULL OR invoices_2.settled_at >= invoices.settled_at) THEN 'pub_and_new'
        ELSE 'pub_only'
      END
      purchase_type,
      COUNT(distinct people.id) num_customers
    SQL
      .where("date(invoices.settled_at) = ?", for_date)
      .group("1, 2")
  end

  alias_attribute :override_ineligibility?, :override_advance_ineligibility

  def set_is_business
    self.is_business = customer_type == "business"
  end

  private

  def self.random_salt
    SecureRandom.hex(32)
  end

  def missing_store?(store)
    albums.each do |album|
      return true unless album.stores.include?(store)
    end
    false
  end

  def convert_royalty_split_recipients_emails_to_people
    RoyaltySplitRecipient.where(email: email).find_each do |recipient|
      recipient.convert_existing_email_to_person
      recipient.save!
    end
  end

  def validate_email_uniqueness
    conditions = []
    conditions =
      if (new_record? || destroyed?)
        ["email = ?", email]
      else
        ["email = ? and id != ?", email, id]
      end

    if Person.find_by(conditions).present?
      errors.add(:email, I18n.t("models.person.already_in_use"))
      @email_uniqueness_validated = false
    else
      @email_uniqueness_validated = true
    end
  end

  def validate_country_website_updater_policy
    return unless persisted? && country_website_id_changed?

    policy_result = CountryWebsiteUpdater::Policy.can_update?(person: self)

    # RAILS_5_UPGRADE: when at 5.2, change this statement to read:
    # errors.merge!(policy_result.errors)
    return unless policy_result.errors.any?

    errors.add(
      :country_website_policy,
      policy_result.errors.full_messages.join(" ")
    )
  end

  def invoke_country_website_updater_update
    return unless errors.empty? && persisted? && country_website_id_changed?

    begin
      CountryWebsiteUpdater::Update.update(person: self)
    rescue => e
      errors.add(:country_website_updater, e.message)
      throw(:abort)
    end
  end

  def country_not_japan
    errors.add(:country, I18n.t("models.person.cannot_be_japan")) if country == "Japan"
  end

  def accepted_terms_and_conditions_on_not_null
    return unless self.accepted_terms_and_conditions_on.nil?

    errors.add("accepted_terms_and_conditions", I18n.t("models.person.accepted"))
  end

  def migrate_referral_data
    return if referral.try(:downcase) == "stem"

    # INSERT INTO referral_data_tmp(person_id, referral, referral_type, referral_campaign, created_at, updated_at)
    # VALUES(person["id"], referral, referral_type, referral_campaign, dt, dt)
    begin
      dt = DateTime.now

      insert_statement = "INSERT INTO referral_data_tmp(person_id"
      insert_statement.concat(", referral") if referral
      insert_statement.concat(", referral_type") if referral_type
      insert_statement.concat(", referral_campaign") if referral_campaign
      insert_statement.concat(", created_at, updated_at) VALUES (#{id}")
      insert_statement.concat(", \"#{referral}\"") if referral
      insert_statement.concat(", \"#{referral_type}\"") if referral_type
      insert_statement.concat(", \"#{referral_campaign}\"") if referral_campaign
      insert_statement.concat(", '#{dt}', '#{dt}')")

      ActiveRecord::Base.connection.execute(insert_statement)
      update(referral: nil, referral_type: nil, referral_campaign: nil)
    rescue
      Tunecore::Airbrake.notify("Insert into referral_data_tmp for person_id #{id} failed")
    end
  end

  def referral?
    referral.present? || referral_type.present? || referral_campaign.present?
  end

  def set_number_blank_if_plus
    self.phone_number = "" if phone_number == "+"
    self.mobile_number = "" if mobile_number == "+"
  end

  def valid_phone_number_formats?
    valid_phone_number = numbers_with_plus?(phone_number, :phone_number)
    valid_mobile_number = numbers_with_plus?(mobile_number, :mobile_number)
    valid_phone_number && valid_mobile_number
  end

  def numbers_with_plus?(telephone_number, type)
    return true if telephone_number.blank?

    data_matches = !!/^\+\d*$/.match(telephone_number)
    errors.add(type, I18n.t("people.errors.phone_must_have_plus")) unless data_matches
    data_matches
  end

  def invalid_phone_number_lengths?
    invalid_phone_number = present_and_incorrect_length?(phone_number, :phone_number)
    invalid_prefixless_mobile_number = present_and_incorrect_length?(
      prefixless_mobile_number, :mobile_number,
      :prefixless
    )
    invalid_mobile_number = present_and_incorrect_length?(mobile_number, :mobile_number)
    invalid_phone_number || invalid_mobile_number || invalid_prefixless_mobile_number
  end

  def present_and_incorrect_length?(telephone_number, type, options = {})
    if options == :prefixless && telephone_number.present? && telephone_number.length != 10 # for India, phone numbers are 10 digits
      errors.add(type, I18n.t("people.errors.must_be_valid_length"))
      return true
    end

    if options != :prefixless && telephone_number.present? && !telephone_number.length.between?(12, 20)
      errors.add(type, I18n.t("people.errors.must_be_valid_length"))
      return true
    end
    false
  end

  def add_blank_telephone_error
    errors.add(:mobile_number, I18n.t("people.errors.or_phone_number_must_be_present"))
    errors.add(:phone_number, I18n.t("people.errors.or_mobile_number_must_be_present"))
  end

  def notify_email_changes_airbrake
    Airbrake.notify("Email changed for user #{id} by user #{Person.current_user_from_paper_trail&.id}: #{saved_changes}")
  end

  def compliance_contact_hash
    @compliance_contact_hash ||= ComplianceInfoFields::MetaDataService.display(self)
  end

  def address_fields_changed?
    changed_fields_set = saved_changes.keys.map(&:to_sym).to_set

    (changed_fields_set & VatAssesmentAddressBuilder::FIELDS).present?
  end

  def destroy_company_information
    company_information&.destroy
  end

  def first_distro_person_analytic?
    analytic_by_metric("first_distribution_invoice_id")
  end
end
