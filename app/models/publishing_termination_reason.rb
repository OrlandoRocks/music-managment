class PublishingTerminationReason < ApplicationRecord
  validates :reason, presence: true
  has_many :terminated_composers
end
