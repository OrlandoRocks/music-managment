class GstInfo < ApplicationRecord
  belongs_to :person
  validates :gstin,
            length: { is: 15 },
            format: { with: /\A[a-zA-Z0-9]*\z/, message: "must be alphanumeric" }
end
