class PayoutTransfer < ApplicationRecord
  include ActiveModel::ForbiddenAttributesProtection
  include TransferMethods

  SUBMITTED = "submitted".freeze
  APPROVED  = "approved".freeze
  REJECTED  = "rejected".freeze
  CANCELLED = "cancelled".freeze

  NONE = "not submitted".freeze
  REQUESTED          = "requested".freeze
  PENDING            = "pending".freeze
  DECLINED           = "declined".freeze
  COMPLETED          = "completed".freeze
  ERRORED = "errored".freeze

  PAYPAL       = "PAYPAL".freeze
  PAYONEER     = "PAYONEER".freeze
  BANK         = "BANK".freeze
  ACCOUNT      = "ACCOUNT".freeze
  PAPER_CHECK  = "PAPER_CHECK".freeze
  PREPAID_CARD = "PREPAID_CARD".freeze
  PAYONEER_PARTNER = "PAYONEER_PARTNER".freeze

  WITHDRAWAL_OPTIONS = [PAYPAL, ACCOUNT, BANK, PAPER_CHECK, PREPAID_CARD, PAYONEER_PARTNER]

  TUNECORE_LIFECYCLE = [
    SUBMITTED, REJECTED, APPROVED
  ].freeze

  PROVIDER_LIFECYCLE = [
    NONE,
    REQUESTED,
    PENDING,
    DECLINED,
    COMPLETED,
    ERRORED,
    CANCELLED
  ].freeze

  PENDING_PROVIDER_STATUSES = [NONE, REQUESTED, PENDING]

  PAYOUT_METHODS = {
    ACCOUNT: "Payoneer",
    BANK: "Bank Account",
    PREPAID_CARD: "Payoneer Card",
    PAYPAL: "Payoneer: PayPal",
    PAPER_CHECK: "Payoneer: Check",
    PAYONEER_PARTNER: "Payoneer Partner"
  }.with_indifferent_access.freeze

  PAYONEER_PAYPAL_NET_TRANSFER_LIMIT_IN_CENTS = 200
  PAYONEER_NON_PAYPAL_NET_TRANSFER_LIMIT_IN_CENTS = 100

  belongs_to :payout_provider
  belongs_to :payout_program
  belongs_to :tunecore_processor, class_name: "Person"

  has_many :payout_transactions, as: :target
  has_one :person, through: :payout_provider
  has_one :person_balance, through: :person
  has_one :vat_tax_adjustment, as: :related
  has_one :outbound_invoice, as: :related
  has_one :transfer_metadatum, as: :trackable
  has_many :payout_transfer_api_logs

  before_create :generate_client_reference_id
  before_create :check_if_withdraw_method_changed
  after_create :generate_metadatum

  # Status scopes
  scope :pending, -> { by_tunecore_status(SUBMITTED).by_provider_status(NONE) }
  scope :submitted, -> { by_tunecore_status(APPROVED).by_provider_status([REQUESTED, PENDING]) }
  scope :completed_and_successful, ->(person_id = nil, _email = nil) do
    query = by_tunecore_status(APPROVED).by_provider_status(COMPLETED)
    query = query.where(person_id: person_id) if person_id.present?
    query
  end
  scope :by_tunecore_status, ->(status) { where(tunecore_status: status) }
  scope :by_provider_status, ->(status) { where(provider_status: status) }

  # Auto-approval scopes
  scope :with_auto_approval_eligible_transactions, -> { joins(:transfer_metadatum) }
  scope :without_auto_approval_eligible_transactions, -> do
    left_joins(:transfer_metadatum).where(transfer_metadata: { trackable_id: nil })
  end

  scope :rolled_back, -> { where(rolled_back: true) }
  scope :recently_approved_transactions, ->(person_id, within_days) {
    completed_and_successful(person_id, _email = nil).where(
      payout_transfers: {
        updated_at: DateTime.now.ago(Integer(within_days).days).beginning_of_day..DateTime.now
      }
    )
  }

  delegate :auto_approved?, to: :transfer_metadatum, allow_nil: true

  validates :tunecore_status, inclusion: { in: TUNECORE_LIFECYCLE }
  validates :provider_status, inclusion: { in: PROVIDER_LIFECYCLE }

  has_paper_trail

  TUNECORE_LIFECYCLE.each do |ts|
    define_method "#{ts}?".to_sym do
      tunecore_status == ts
    end
  end

  PROVIDER_LIFECYCLE.each do |ps|
    define_method "#{ps}?".to_sym do
      provider_status == ps
    end
  end

  def fee_dollar_amount
    Money.new(fee_cents, person.currency)
  end

  def to_param
    client_reference_id
  end

  def mark_provider_as(status)
    return false unless PROVIDER_LIFECYCLE.include?(status)

    update(provider_status: status)
  end

  def can_be_rejected?
    submitted? && provider_status == PayoutTransfer::NONE
  end

  def auto_approveable?
    provider_status == "not submitted" &&
      tunecore_status == "submitted"
  end

  def amount_cents_as_decimal
    Money.new(amount_cents).to_d
  end

  def build_payout_transaction(comment_string)
    payout_transactions.build(
      person_id: person.id,
      credit: 0,
      debit: amount_cents_as_decimal,
      currency: person.currency,
      comment: comment_string,
      previous_balance: person_balance.balance
    )
  end

  def in_provider_lifecycle?(status = provider_status)
    in_lifecycle(status, PROVIDER_LIFECYCLE)
  end

  def in_tunecore_lifecycle?(status = tunecore_status)
    in_lifecycle(status, TUNECORE_LIFECYCLE)
  end

  def in_lifecycle(status, lifecycle)
    ([status] & lifecycle).present?
  end

  def requested_amount
    Money.new(amount_cents + (fee_cents || 0), person.currency)
  end

  def fee_amount
    Money.new(fee_cents, person.currency)
  end

  def amount_to_transfer
    # For Paypal transfer, the contract between payoneer and tunecore is to withhold the fee amount
    tax_exclusive_amount = (withdraw_method == "PAYPAL") ? dollar_amount : requested_amount

    tax_exclusive_amount + tax_amount
  end

  def tax_amount
    Money.new(vat_tax_adjustment&.amount, person.currency)
  end

  def unsuccessful?
    rejected? || declined? || cancelled?
  end

  def response
    return if raw_response.blank?

    JSON.parse(raw_response).with_indifferent_access
  end

  def raw_api_response
    payout_transfer_api_logs.responses.last.body if payout_transfer_api_logs.responses.any?
  end

  def raw_api_request
    payout_transfer_api_logs.requests.last&.body
  end

  def api_request_url
    payout_transfer_api_logs.requests.last&.url
  end

  def create_outbound_invoice
    return unless person.vat_applicable?

    build_outbound_invoice(
      person_id: person.id,
      user_invoice_prefix: person.outbound_invoice_prefix,
      vat_tax_adjustment_id: vat_tax_adjustment&.id,
      invoice_date: Date.today,
      currency: person.currency
    ).save
  end

  def auto_approval_eligible?
    transfer_metadatum.present?
  end

  # Needed to implement the same interface as PaypalTransfer
  def paypal_address
    nil
  end

  private

  def generate_client_reference_id
    self.client_reference_id = SecureRandom.uuid
  end

  def check_if_withdraw_method_changed
    last_withdraw_method = PayoutTransfer.where(payout_provider_id: payout_provider)
                                         .order("created_at DESC")
                                         .pluck(:withdraw_method).first
    self.withdraw_method_changed = last_withdraw_method != withdraw_method
    true
  end

  def generate_metadatum
    return unless FeatureFlipper.show_feature?(:auto_approved_withdrawals, person.id)

    create_transfer_metadatum(
      login_event: LoginEvent.where(person_id: person.id).last
    )
  end
end
