class MarketingEventTrigger < ApplicationRecord
  belongs_to :related, polymorphic: true
  belongs_to :hubspot_event

  def hubspot_event
    HubspotEvent.find(provider_id)
  end

  def event_name
    "#{product.name.upcase.tr(' ', '_')}_#{event_trigger}"
  end
end
