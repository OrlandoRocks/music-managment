class SyncLicenseRequest < ApplicationRecord
  #
  # Constants
  #

  NUMBER_ASSET_DOWNLOADS = 3
  STATUSES = { new: "new", quote_sent: "quote sent", revised: "revised", licensed: "licensed", expired: "expired" }
  MAX_SECONDS = 60
  UPLOAD_TYPES = [
    "text/plain",
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/pdf",
    "image/jpeg",
    "image/png",
    "image/gif",
    "image/tiff"
  ]

  #
  # Changing the storage formats will affect the authenticated_url functionality. Ensure
  # behavior is as expected
  #

  REQUEST_DOC_OPTIONS =
    if Rails.env.production? || Rails.env.staging? || Rails.env.test?

      {
        storage: :s3,
        path: "/synch/:id/:filename",
        bucket: SYNC_REQUEST_BUCKET_NAME,
        s3_credentials: { access_key_id: AWS_ACCESS_KEY, secret_access_key: AWS_SECRET_KEY },
        s3_permissions: "authenticated-read",
      }

    else

      {
        path: ":rails_root/public/synch/:id/:filename",
        url: "/synch/:id/:filename"
      }

    end

  #
  # Associations
  #

  belongs_to          :song
  belongs_to          :sync_license_production
  belongs_to          :person

  has_many            :sync_license_options, -> { order "number ASC" }

  has_attached_file   :request_document, REQUEST_DOC_OPTIONS

  #
  # Delegates
  #

  delegate :composition, to: :song

  #
  # Validations
  #

  validates             :person, presence: true
  validates             :song, presence: true
  validates             :status, presence: true
  validates            :status,            inclusion: { in: STATUSES.values }
  validates            :master_use,        inclusion: { in: [true, false] }
  validates         :duration_min,      numericality: { only_integer: true, allow_blank: true, message: "is invalid" }
  validates         :duration_sec,      numericality: { only_integer: true, less_than: MAX_SECONDS, allow_blank: true, message: "is invalid" }
  validates         :duration,          numericality: { only_integer: true, allow_blank: true }
  validates_attachment_content_type :request_document,  content_type: UPLOAD_TYPES, allow_blank: true
  validates_attachment_size         :request_document,  less_than: 10.megabytes
  validate                          :muma_song_present

  #
  # Callbacks
  #

  before_validation :set_default_status, on: :create
  before_update     :set_ticketbooth_url

  #
  # Attributes
  #

  accepts_nested_attributes_for :sync_license_production,   reject_if: :all_blank
  accepts_nested_attributes_for :sync_license_options,      reject_if: :all_blank

  #
  # expires in given in seconds
  #
  def authenticated_url(expires_in = 10)
    if request_document.options[:storage] == :s3
      request_document.expiring_url(expires_in, request_document.default_style)
    else
      request_document.url(:original, false)
    end
  end

  def downloadable?
    request_document.exists?
  end

  def duration_min
    if @duration_min
      @duration_min

    else
      duration ? (duration / 60) : nil
    end
  end

  def duration_min=(val)
    int_val = 0

    if val.present?
      begin
        int_val = Integer(val)
        @duration_min = nil
      rescue ArgumentError
        @duration_min = val
      end
    end

    self.duration =
      if duration
        (duration % 60) + (60 * int_val)
      else
        60 * int_val
      end
  end

  def duration_sec
    if @duration_sec
      @duration_sec

    else
      duration ? (duration % 60) : nil
    end
  end

  def duration_sec=(val)
    int_val = 0

    if val.present?
      begin
        int_val = Integer(val)

        if int_val >= MAX_SECONDS
          @duration_sec = int_val
          int_val = duration ? duration % 60 : 0
        else
          @duration_sec = nil
        end
      rescue ArgumentError
        @duration_sec = val
      end
    end

    self.duration =
      if duration
        duration - (duration % 60) + int_val
      else
        int_val
      end
  end

  def muma_song
    song.muma_songs.first
  end

  delegate :title, to: :muma_song

  delegate :mech_collect_share, to: :muma_song

  def new?
    status == STATUSES[:new]
  end

  def quote_sent
    update_attribute(:status, STATUSES[:quote_sent])
  end

  def quote_sent?
    status == STATUSES[:quote_sent]
  end

  def revised
    update_attribute(:status, STATUSES[:revised])
  end

  def revised?
    status == STATUSES[:revised]
  end

  def licensed
    update_attribute(:status, STATUSES[:licensed])
  end

  def licensed?
    status == STATUSES[:licensed]
  end

  def expired
    update_attribute(:status, STATUSES[:expired])
  end

  def expired?
    status == STATUSES[:expired]
  end

  private

  #
  # Needed for validates_numericality_of for virtual attribute
  #
  def duration_min_before_type_cast
    duration_min
  end

  #
  # Needed for validates_numericality_of for virtual attribute
  #
  def duration_sec_before_type_cast
    duration_sec
  end

  def muma_song_present
    errors.add(:song, I18n.t("models.sync_licensing.is_not_a_eligible_for_synch_licensing")) if song.muma_songs.blank?
  end

  def set_default_status
    self.status = STATUSES[:new] if status.blank?
  end

  def set_ticketbooth_url
    # url to download the asset
    return unless status == STATUSES[:licensed] && valid? && (ticketbooth_url.blank? && song.s3_asset)

    restricted_url = song.s3_asset.create_restricted_url(NUMBER_ASSET_DOWNLOADS)
    self.ticketbooth_url = restricted_url
  end
end
