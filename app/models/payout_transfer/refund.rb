class PayoutTransfer::Refund
  include ActiveModel::Validations

  def self.refund(params)
    new(params).tap(&:refund)
  end

  validates :payout_transfer, :person, :amount, :currency, presence: true

  attr_reader :payout_transfer, :person, :amount, :person_balance, :currency

  def initialize(params)
    @payout_transfer    = params[:payout_transfer]
    @amount             = @payout_transfer.try(:requested_amount).try(:to_d)
    @person             = @payout_transfer.try(:person)
    @currency           = @payout_transfer.try(:currency)
    @person_balance     = @person.try(:person_balance)
  end

  def refund
    return false unless valid?

    build_transaction

    ActiveRecord::Base.transaction do
      payout_transfer.save!
      person_balance.update_balance(amount)
    end
  end

  private

  def build_transaction
    payout_transfer.update(rolled_back: true)

    payout_transfer.payout_transactions.build(
      person: person,
      credit: amount,
      currency: currency,
      target: payout_transfer,
      previous_balance: person_balance.balance,
      comment: "Rollback of #{payout_transfer.payout_provider.name} Payout Transaction"
    )
  end
end
