class FeatureFlipper
  ALBUM_APP_FEATURES = %i[plans_pricing].freeze

  def self.all_features(country_website = nil)
    feature_names = country_website.blank? ? $rollout.features : country_specific_rollout(country_website).features
    feature_names.uniq.each_with_object([]) do |name, feature_details|
      feature_details << get_feature_details(name, country_website)
    end
  end

  def self.add_feature(feature, country_website = nil)
    return if feature.blank?

    if country_website.blank?
      $rollout.deactivate(parse_feature_name(feature))
    else
      country_specific_rollout(country_website)&.deactivate(parse_feature_name(feature))
    end
  end

  def self.update_feature(feature, percentage, users, country_website = nil)
    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    rollout_instance.activate_percentage(feature, percentage.to_i)

    deactivate_users(feature, country_website)
    activate_users(feature, users, country_website)
  end

  def self.show_feature?(feature, user_id_or_user = nil)
    user =
      if user_id_or_user.is_a? Person
        user_id_or_user
      elsif user_id_or_user.present?
        Person.find_by(id: user_id_or_user)
      end
    if feature_has_country_specific_variant?(feature, user)
      show_feature_for_country_website?(feature, user)
    else
      $rollout.active?(feature) || $rollout.active?(feature, user)
    end
  rescue => e
    Rails.logger.error("Error Occured When Checking Feature: #{e}")
    false
  end

  # show_website_feature? is for when we don't have a current_user
  def self.show_website_feature?(feature, country_website)
    country_specific_rollout = country_specific_rollout_by_country(country_website)
    return country_specific_rollout&.active?(feature) if country_specific_rollout.features.include?(feature)

    $rollout.active?(feature)
  end

  def self.get_features(feature_array, user = nil)
    all_features
      .select { |f| feature_array.include?(f.name) }
      .map { |f| [f.name.to_s, FeatureFlipper.show_feature?(f.name, user)] }
      .to_h
  end

  def self.deactivate_users(feature, country_website = nil)
    details = get_feature_details(feature, country_website)
    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    valid_users = Person.where(id: details.users.to_a)
    invalid_user_ids = details.users.to_a - valid_users.pluck(:id)
    remove_invalid_users(feature, rollout_instance, invalid_user_ids)
    valid_users.find_each do |user|
      rollout_instance.deactivate_user(feature, user)
    end
  end

  def self.remove_feature(feature, country_website = nil)
    return unless feature

    feature = feature.gsub(/\s+/, "_").downcase.to_sym

    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    rollout_instance.delete(feature)
  end

  def self.get_users(feature, country_website = nil)
    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    rollout_instance.get(feature.to_sym).users
  end

  def self.add_by_user_ids(feature, user_ids, country_website = nil)
    return unless user_ids.present? && user_ids.kind_of?(Array)

    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    rollout_instance.activate_users(feature.to_sym, user_ids)
  end

  private

  def self.activate_users(feature, users, country_website = nil)
    rollout_instance = country_website.present? ? country_specific_rollout(country_website) : $rollout
    Person.where(id: users.to_s.split(",").select(&:present?)).find_each do |person|
      rollout_instance.activate_user(feature, person) if person.present?
    end
  end

  # PRIVATE CLASS METHODS
  def self.remove_invalid_users(feature, rollout_obj, invalid_user_ids)
    invalid_user_ids.each do |invalid_user_id|
      person = Person.new(id: invalid_user_id)
      rollout_obj.deactivate_user(feature, person)
    end
  end

  def self.parse_feature_name(feature_name)
    feature_name.gsub(/\s+/, "_").downcase.to_sym
  end

  def self.country_specific_rollout(country_website)
    $country_rollouts[country_website.country]
  end

  def self.country_specific_rollout_by_country(country)
    $country_rollouts[country]
  end

  def self.feature_has_country_specific_variant?(feature, user)
    user.present? &&
      user.country_website.present? &&
      country_specific_rollout(user.country_website).features.include?(feature)
  end

  def self.show_feature_for_country_website?(feature, user)
    user.present? && (
      country_specific_rollout(user&.country_website)&.active?(feature) ||
      country_specific_rollout(user&.country_website)&.active?(feature, user)
    )
  end

  def self.get_feature_details(feature, country_website)
    if country_website.present? && country_specific_rollout(country_website).exists?(feature)
      country_specific_rollout(country_website).get(feature)
    else
      $rollout.get(feature)
    end
  end

  private_class_method :country_specific_rollout,
                       :show_feature_for_country_website?,
                       :feature_has_country_specific_variant?,
                       :parse_feature_name,
                       :get_feature_details,
                       :remove_invalid_users
end
