# 2009-08-20 -- ED -- this class inherits from MediaAsset model and will ll be used to store band photos
# 2009-08-27 -- ED -- added validation so user can create only 1 band photo
# 2009-08-31 -- ED -- fixed issue when user edits their band photo with a non-image it'll set the file size and mime type
# 2009-09-01 -- ED -- crops original image to a square and thumbnail generation is based off the square copy
# 2009-09-02 -- ED -- fixed undefined ActionController::TestUploadedFile class
# =>               -- renamed uploaded_file to file
# 2009-10-01 -- ED -- fixed band photo upload in IE 7
class BandPhoto < MediaAsset
  # KEY_PREFIX is used for organizing files saved to S3
  ORIGINAL_KEY_PREFIX = "band_photos/original".freeze
  THUMBNAIL_160X160_KEY_PREFIX = "band_photos/160x160".freeze

  attr_accessor :file

  before_validation :set_meta_data

  validates :file, presence: true
  validates :width,
            inclusion: {
              in: 100..4032,
              allow_nil: false,
              message: I18n.t("model.band_photo.width_error_msg")
            }
  validates :height,
            inclusion: {
              in: 100..3160,
              allow_nil: false,
              message: I18n.t("model.band_photo.height_error_msg")
            }
  validates :bit_depth,
            inclusion: {
              in: 1..32,
              allow_nil: true,
              message: I18n.t("model.band_photo.bit_depth_error_msg")
            }
  validates :size,
            inclusion: {
              in: 1024..5_242_880,
              allow_nil: false,
              message: I18n.t("model.band_photo.file_size_error_msg")
            }

  validate :one_band_photo_per_person, on: :create

  before_create :create_assets # create a blank asset record so we can save s3_asset_id or loca_asset_id into the db
  after_commit :do_required_tasks_with_original_file, on: [:create, :update]
  # doing this after commit because filename generation uses self.id see filename_seed method

  def url(size = :original)
    case size
    when :thumb
      s3_thumbnail_path
    else
      s3_original_path
    end
  end

  def s3_thumbnail_path
    "http://#{derived_media_assets.first.s3_asset.bucket}.s3.amazonaws.com/#{derived_media_assets.first.s3_asset.key}?#{Time.now.strftime('%H%M%S')}"
  rescue => e
    logger.error("s3 thumbnail path: #{e.message}")
    ""
  end

  def s3_original_path
    "http://#{s3_asset.bucket}.s3.amazonaws.com/#{s3_asset.key}?#{Time.now.strftime('%H%M%S')}"
  rescue => e
    logger.error("s3 thumbnail path: #{e.message}")
    ""
  end

  private

  def set_meta_data
    return if @file.blank?

    image = MiniMagick::Image.open(@file.path)

    self.size = image.size
    self.mime_type = image.mime_type
    self.width = image.width
    self.height = image.height
  end

  def one_band_photo_per_person
    if BandPhoto.find_by(person_id: person_id)
      errors.add(:base, I18n.t("models.media.sorry_you_can_only_upload_one_band_photo"))
    end
    true
  end

  def create_assets
    # we're only creating s3 asset since we wont be serving band photos from our app server
    self.s3_asset = S3Asset.create
  end

  def do_required_tasks_with_original_file
    process_list = []
    # update local or s3 asset
    process_list << update_assets
    # make a temp copy of original image so resizing can work
    process_list << make_copies_to(temp: true, original: true)
    # generate a cropped square version and resizing will be based off it
    process_list << generate_square
    # generate resized thumbnail to 160x160
    process_list << generate_thumbnails
    # create db records for thumbnails
    process_list << save_derived_media_assets
    # make copies to S3 so it'll be publicly available
    process_list << make_copies_to(s3: true, original: true)
    process_list << make_copies_to(s3: true, thumbnail_160x160: true)
    # clean up any temp images here
    process_list << cleanup_temp_images
    if process_list.any? { |process| process == false }
      logger.error "Could not process band photo upload"
      destroy
      false
    else
      logger.info "Successfully processed band photo upload"
      true
    end
  rescue => e
    logger.error "Do Required Tasks With Original File: #{e.message}"
    destroy
    false
  end

  def update_assets(options = { s3: true })
    if options[:s3]
      # s3_asset always exist upon band photo creation via before_create call back method
      # so we're simply just updating the record here
      s3_asset.bucket = ASSET_BUCKET_NAME
      s3_asset.key = "#{ORIGINAL_KEY_PREFIX}/#{filename_seed}.jpg"
      if s3_asset.save
        logger.info "Successfully updated band photo's s3 asset"
        true
      else
        logger.error "Could not update band photo's s3 asset"
        false
      end
    end
    # if options[:local]
    # end
  rescue => e
    logger.error "Saving S3 Asset: #{e.message}"
    raise "Problem saving S3 Asset"
  end

  # make copies of images to local, temp, or s3
  def make_copies_to(options = {})
    if options[:s3]
      if options[:original]
        logger.info "About to make a copy of the original band photo to s3"
        s3_asset.filename = temp_file_path(original: true)
        if s3_asset.put!(access: "public-read")
          logger.info "Successfully made a copy of the original band photo to s3"
          true
        else
          logger.error "Could not make a copy of the original band photo to s3"
          false
        end
      elsif options[:thumbnail_160x160]
        logger.info "About to make a copy of the 160x160 thumbnail band photo to s3"
        thumbnail_s3_asset = derived_media_assets.first.s3_asset
        thumbnail_s3_asset.filename = temp_file_path(thumbnail_160x160: true)
        if thumbnail_s3_asset.put!(access: "public-read")
          logger.info "Successfully made a copy of the band photo thumbnail to s3"
          true
        else
          logger.error "Could not make a copy of the band photo thumbnail to s3"
          false
        end
      end
    elsif options[:temp]
      if options[:original]
        logger.info "About to make a copy of the original band photo to local temp"
        File.open(temp_file_path(original: true), "wb") do |f|
          if f.write(@file.read)
            logger.info "Successfully made a temp copy of the original band photo"
            return true
          else
            logger.error "Could not make a copy of the original band photo"
            return false
          end
        end
      end
    end
  rescue => e
    logger.error "Making copies of band photo to temp, S3, or local: #{e.message}"
    raise "Problem making copies of band photo"
  end

  def generate_square
    # Create a square copy
    image = MiniMagick::Image.open(temp_file_path(original: true))

    dimension = image.dimensions.min

    image.combine_options do |b|
      b.gravity "center"
      b.crop "#{dimension}x#{dimension}+0+0"
    end

    image.write(temp_file_path(square: true))
  end

  # Generates thumbnail for band photo and copies it to S3
  def generate_thumbnails(options = { thumbnail_160x160: true })
    return unless options[:thumbnail_160x160]

    image = MiniMagick::Image.open(temp_file_path(square: true))

    image.resize("160x160")

    image.write(temp_file_path(thumbnail_160x160: true))
  end

  def save_derived_media_assets(options = { thumbnail_160x160: true })
    if options[:thumbnail_160x160]
      logger.info "saving band photo's derived media asset for 160x160 thumbnail"
      # check if derived_media_asset exists
      derived_media_assets << DerivedMediaAsset.new if derived_media_assets.empty?
      thumbnail_derived_asset = derived_media_assets.first
      # as of 8/25/2009, band photo will have only 1 derived media asset -> 160x160 image
      image = MiniMagick::Image.open(@file.path)

      thumbnail_derived_asset.width = image.width
      thumbnail_derived_asset.height = image.height

      thumbnail_derived_asset.size = File.new(temp_file_path(thumbnail_160x160: true)).size
      thumbnail_derived_asset.mime_type = image.mime_type

      if thumbnail_derived_asset.s3_asset.nil?
        thumbnail_derived_asset.s3_asset = S3Asset.new
      end # we might be editing a band photo

      thumbnail_s3_asset = thumbnail_derived_asset.s3_asset
      thumbnail_s3_asset.bucket = ASSET_BUCKET_NAME
      thumbnail_s3_asset.key = "#{THUMBNAIL_160X160_KEY_PREFIX}/#{filename_seed}.jpg"
      if thumbnail_s3_asset.save && thumbnail_derived_asset.save
        logger.info "Succesffully saved band photo's derived media asset"
        true
      else
        logger.error "Could not save band photo's derived media asset"
        false
      end
    end
    # if options[:thumbnail_100x100]
  rescue => e
    # uh-oh we have an issue saving the derived media asset
    logger.error "Saving band photo's derived media asset: #{e.message}"
    raise "Problem saving band photo's derived media asset"
  end

  def cleanup_temp_images
    File.delete(temp_file_path(thumbnail_160x160: true))
    logger.debug("removing band photo temp " + temp_file_path(thumbnail_160x160: true))
    File.delete(temp_file_path(original: true))
    logger.debug("removing band photo temp " + temp_file_path(original: true))
    File.delete(temp_file_path(square: true))
    logger.debug("removing band photo temp " + temp_file_path(square: true))
    true
  rescue => e
    logger.error "Cleaning temp band photos: #{e.message}"
    raise "Problem cleaning temp band photos"
  end

  def web_path(options = {})
    return unless options[:s3]

    if options[:thumbnail_160x160]
      "http://#{ASSET_BUCKET_NAME}.s3.amazonaws.com/#{THUMBNAIL_160X160_KEY_PREFIX}/#{filename_seed}.jpg"
    elsif options[:original]
      "http://#{ASSET_BUCKET_NAME}.s3.amazonaws.com/#{ORIGINAL_KEY_PREFIX}/#{filename_seed}.jpg"
    end
  end

  def temp_file_path(options = { thumbnail_160x160: true })
    mime_type.slice!("image/")
    if options[:thumbnail_160x160]
      # make sure the folder exists
      File.join(Rails.root, UPLOAD_ARTWORK_PATH, "thumbnail_160x160_#{filename_seed}.#{mime_type}")
    elsif options[:original]
      # make sure the folder exists
      File.join(Rails.root, UPLOAD_ARTWORK_PATH, "original_#{filename_seed}.#{mime_type}")
    elsif options[:square]
      File.join(Rails.root, UPLOAD_ARTWORK_PATH, "square_#{filename_seed}.#{mime_type}")
    end
  end

  def filename_seed
    id.to_s unless id.nil?
  end
end
