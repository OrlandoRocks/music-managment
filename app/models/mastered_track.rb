class MasteredTrack < ApplicationRecord
  belongs_to :person
  has_one    :purchase, as: :related

  validates :name, :person, presence: true
  before_destroy :do_not_destroy_paid_records

  scope :unpaid, -> { where(paid_at: nil) }
  scope :paid, -> { where "paid_at is not null" }

  def self.add_items(person, mastered_tracks_param)
    mastered_tracks = []
    mastered_tracks_param
    mastered_tracks_param.each do |param|
      attributes = param.merge!(person_id: person.id)
      mastered_tracks << create(attributes)
    end

    mastered_tracks
  end

  def self.remove_items_by_claim_ids(person, claim_ids)
    # FIXME: Error Handling
    mastered_tracks = MasteredTrack.unpaid.where(claim_id: claim_ids, person_id: person.id)
    removed_mastered_tracks = []
    mastered_tracks.each do |mastered_track|
      # TODO: guard against paid mastered track?
      removed_mastered_tracks << mastered_track.destroy
    end

    removed_mastered_tracks
  end

  def self.purchase_track(person, mastered_track_ids)
    mastered_tracks = MasteredTrack.where(id: mastered_track_ids, person_id: person.id)
    product = Product.find(Product.find_products_for_country(person.country_domain, :mastered_track))
    mastered_tracks.each do |mt|
      mt.purchase_track(product)
    end
    # FIXME: Error Handling
  end

  def self.find_claims(invoice_id)
    invoice = Invoice.find(invoice_id)
    purchases = invoice.purchases.where(related_type: "MasteredTrack").preload(:related)

    claims = []
    purchases.each { |p| claims << p.related.claim_id }
    claims
  end

  def self.retrieve_download_link(person_id, landr_track_id, link, job_id, _host)
    mastered_track = MasteredTrack.where(person_id: person_id, job_id: job_id).where("download_link is null").first
    raise ActiveRecord::RecordNotFound unless mastered_track

    mastered_track.update(download_link: link, landr_track_id: landr_track_id)

    mastered_track
  end

  def self.invoice_downloads_complete?(invoice_id)
    MasteredTrack.joins("inner join purchases on mastered_tracks.id = purchases.related_id and purchases.related_type = 'MasteredTrack'")
                 .where(["purchases.invoice_id = ? and mastered_tracks.download_link is null", invoice_id]).blank?
  end

  def self.unpaid_tracks?(current_user)
    MasteredTrack.where(paid_at: nil, person_id: current_user.id).present?
  end

  def purchase_track(product)
    purchase = Product.add_to_cart(person, self, product)
  end

  def paid_post_proc
    update({ paid_at: purchase.paid_at })
  end

  private

  def do_not_destroy_paid_records
    throw(:abort) if paid_at.present?
  end
end
