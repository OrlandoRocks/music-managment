# = HISTORY
# The DataReport and associated DataRecord models were created to roll-up data reports
# because a number of reports the business needed to view were incredibly database
# intensive, causing a huge number of queries to be called everytime the report was viewed.
#
# = GOAL
# Create a DataReport template that would handle the administrative tasks associated
# with the calculation and storage of reports.  The structure of the DataRecords is such that
# that the DataReport can store any number of columns.
#
# = USAGE
# To create a new data report a new report (descending from DataReport) should be created
# in /lib/tunecore/reports/[optional subdirectory].
#
# The only required override in the subclassed model is the build_records method (seen below)
#
#   def build_records(resolution, start_date, stop_date)
#    some awesome code that calculates some information based on the start and stop dates.
#    This could be a sql query, a call to a web service, ruby code, etc.
#    and then calls the create_record(resolution, start_date, data) method
#   end
#
# An important concept when creating a new report is the report's resolution.  Currently the
# DataReport template can deal with the resolutions of :year, :quarter, :month, :week (commercial)
# :day or :live.  The resolution identifies the time period or periods that you want to keep track
# and passes the correct dates into the build_records method when the your_report.record method is called
#
# MORE TO COME....
#
#
# = Change Log
# [2009-12-02 -- MJL]
# Adding the initial (and long overdue) description of the DataReport class
#
# [2009-12-03 -- MJL]
# Fixing how we identify quarters in date_range_quarter. We were accidently capturing too small of a range
# because the returned date range (which has an extra day at the for sql between statements) was being
# used to calculate which dates fall in that range.
#
# [2009-12-04 -- MJL]
# Refactoring the date_range methods into the Utilities::DateRange class
#
# [2010-01-08 -- MJL]
# Fixing a call to the deprecated method resolution_identifier for DataReport.export
class DataReport < ApplicationRecord
  has_many :data_records
  validates :resolution, :name, :start_on, presence: true
  validate :validate_resolution

  # Calling this method creates and/or updates the
  def record(date = Date.today)
    if should_update_live? || should_update_resolutions?(date)
      if should_update_live?
        update_attribute(:live_update_at, Time.now)
        record_live
      end

      if should_update_resolutions?(date)
        # update_attribute(:live_update_at, Time.now)
        # if a report has an updateable past we need to reset the last calculated so that it will regenerate the entire cycle
        # else, we need to reset the last_calculated_on to the current date so that it will regenerate today and beyond
        if updateable_past?
          update_attribute(:last_calculated_on, nil)
        elsif last_calculated_on.present?
          update_attribute(:last_calculated_on, Date.today)
        end

        # In the initial sweep, we set the last calculated on is blank, if so, we need to set it to the start date and generate all records
        update_attribute(:last_calculated_on, start_on) if last_calculated_on.blank?

        # set the last_calculated_on date to today if it is greater than today
        update_attribute(:last_calculated_on, Date.today) if last_calculated_on > Date.today

        while last_calculated_on <= date do
          # Check that the report includes the resolution and that a record hasn't been created for it.
          record_day(last_calculated_on) if should_update?(:day, last_calculated_on)
          record_week(last_calculated_on) if should_update?(:week, last_calculated_on)
          record_month(last_calculated_on) if should_update?(:month, last_calculated_on)
          record_quarter(last_calculated_on) if should_update?(:quarter, last_calculated_on)
          record_year(last_calculated_on) if should_update?(:year, last_calculated_on)

          # Why the hell won't this work without a db commit....?
          update_attribute(:last_calculated_on, last_calculated_on + 1)
        end
        update_attribute(:live_update_at, Time.now)
      end
      true
    else
      false
    end
  end

  def should_update?(resolution, date)
    far_enough_in_the_future =
      case resolution
      when :live    then true
      when :day     then true # Date.today >= date || resolution_identifier(:day, date) == resolution_identifier(:day, Date.today)
      when :week    then date.cwday == 1 || Utilities::DateRange.new(:week, date).identifier == Utilities::DateRange.new(:week, Date.today).identifier
      when :month   then date.mday == 1 || Utilities::DateRange.new(:month, date).identifier == Utilities::DateRange.new(:month, Date.today).identifier
      when :quarter then date.mday == 1 && (date.month == 1 || date.month == 4 || date.month == 7 || date.month == 10) || Utilities::DateRange.new(:quarter, date).identifier == Utilities::DateRange.new(:quarter, Date.today).identifier
      when :year    then date.month == 1 && date.mday == 1 || Utilities::DateRange.new(:year, date).identifier == Utilities::DateRange.new(:year, Date.today).identifier
      end

    (resolution_includes?(resolution) && far_enough_in_the_future) || (resolution_includes?(resolution) && updateable_past? && far_enough_in_the_future)
  end

  def should_update_live?
    return false unless resolution_includes?(:live)

    live_update_at.blank? || (live_update_at < Time.now - (live_update_limit * 60)) # update_limit is in minutes
  end

  def should_update_resolutions?(date = Date.today)
    return false if (resolution.size == 1 && resolution_includes?(:live))
    return true if last_calculated_on.blank?

    (last_calculated_on < date) || (live_update_at < Time.now - (live_update_limit * 60)) || live_update_at.blank?
  end

  def has_recorded?(symbol)
    !!data_records.find_by("data_report_id = ? AND resolution_identifier = ?", id, symbol.to_s)
  end

  #  Methods Dealing with Resolutions and Resolution Identifiers
  # def resolution_identifier(resolution, date = Date.today)
  #  case resolution
  #  when :live          : :live
  #  when :day           : Utilities::DateRange.new(:day, date).identifier
  #  when :week          : Utilities::DateRange.new(:week, date).identifier
  #  when :month         : date_range_month(date)[2]
  #  when :quarter       : date_range_quarter(date)[2]
  #  when :year          : date_range_year(date)[2]
  #  else
  #    raise "Issue Identifying Report Resolution"
  #  end
  # end

  def resolution_includes?(x)
    resolution.include?(x)
  end

  # Pass in an array of resolution identifier symbols requires +[:live, :day, :week, :month, :quarter, :year]+
  def resolution=(value)
    super(value.join(" ")) if value.instance_of?(Array) && value.all? { |x| x.instance_of?(Symbol) }
  end

  # Returns an array of symbols identifying the resolution identifiers
  def resolution
    # Resolution is stored in the database as a string, convert the string to an array of symbols
    if super
      super.split(" ").collect(&:intern)
    else
      []
    end
  end

  # The data parameter must come in as an array of arrays [[measure,  description], [etc, etc]]
  def create_record(resolution, date, data)
    unless resolution_includes?(resolution)
      raise RuntimeError, "The resolution supplied is not in this reports resolution list"
    end

    identifier = Utilities::DateRange.new(resolution, date).identifier.to_s # resolution_identifier(resolution, date).to_s

    # if the report has an updatable past, then we want to find the previous resolution identifiers and update them.
    if has_recorded?(identifier) # (updateable_past? || resolution_includes?(:live)) &&
      records_to_update = data_records.where("resolution_identifier = ?", identifier)

      # cycle through the data and find the unit_of_measure and facet and update the measure
      data.each do |record|
        row_to_update = records_to_update.detect { |x| x.description == record[1] }

        if row_to_update.nil?

          # duplicate code needs to be refactored.
          date_recorded =
            case resolution
            when :day   then date.to_s
            when :month then date.strftime("%Y-%m")
            else
              nil
            end

          data_records.create(
            resolution_unit: resolution.to_s,
            resolution_identifier: identifier,
            resolution_display: date_recorded,
            measure: record[0],
            description: record[1]
          )
        else
          row_to_update.update_attribute(:measure, record[0])
        end
      end
    else
      data.each do |record|
        date_recorded =
          case resolution
          when :day   then date.to_s
          when :month then date.strftime("%Y-%m")
          else
            nil
          end

        data_records.create(
          resolution_unit: resolution.to_s,
          resolution_identifier: identifier,
          resolution_display: date_recorded,
          measure: record[0],
          description: record[1]
        )
      end
    end
  end

  # This method deletes all of the stored data_records and
  # causes the report to be completely regenerated from its
  # start_on date
  def force_regeneration!(date = Date.today)
    DataRecord.where("data_report_id = ?", id).destroy_all

    self.last_calculated_on = nil
    self.live_update_at = nil
    save!
    record(date)
  end

  # this needs to move into the data_report model itself
  def export(start, stop, resolution)
    resolutions_to_return = []

    until start > stop do
      resolutions_to_return << Utilities::DateRange.new(resolution, start).identifier

      case resolution
      when :live  then start += 1
      when :day   then start += 1
      when :week  then start += 7
      when :month then start = start >> 1
      else
        raise "The export doesn't handle this resolution"
      end
    end

    # return resolutions_to_return
    data_to_export = []

    resolutions_to_return.each do |res|
      data_to_export << data_records.where("resolution_identifier = ?", res.to_s).sort
    end
    data_to_export
  end

  # =====================
  # = Protected Methods =
  # =====================

  protected

  def data(resolution, go_back = 7, columns = [])
    # raise an error if the requested resolution isn't defined in this report
    raise "The resolution :#{resolution} is not recorded for this report" unless resolution_includes?(resolution)

    # if there are no columns specified return all columns, else return selected columns
    if columns.blank?
      return_all_columns_for_resolution(resolution, go_back)
    else
      return_identified_columns_for_resolution(resolution, go_back, columns)
    end
  end

  # This method must be overridden by the class that inherits from DataReport
  def build_records(_resolution, _start, _stop)
    raise RuntimeError, "build_records must be overridden by the inheriting class"
  end

  # Private Methods

  private

  def resolutions_are_all_valid_symbols?
    symbols = [:live, :day, :week, :month, :quarter, :year]
    resolution.all? { |x| symbols.include?(x) }
  end

  # Makes sure that all of the resolutions supplied by the user
  # are in the resolution list that we expect
  def validate_resolution
    return if resolutions_are_all_valid_symbols?

    errors.add(:resolution, I18n.t("models.reporting.resolutions_must_be_of_type", types: "[:live, :day, :week, :month, :quarter, :year]"))
  end

  def return_all_columns_for_resolution(resolution, go_back)
    results = []
    0.upto(go_back - 1) do |x|
      identifier = Utilities::DateRange.new(resolution, Date.today - x).identifier.to_s
      records = data_records.where(resolution_identifier: identifier)
      results << DataRow.new(Array.new(records)) unless records.empty?
    end
    results
  end

  def return_identified_columns_for_resolution(resolution, go_back, columns)
    results = []
    0.upto(go_back - 1) do |x|
      identifier = Utilities::DateRange.new(resolution, Date.today - x).identifier.to_s
      records = data_records.where(resolution_identifier: identifier)

      # setting up a loop to control the return order
      ordered_records = []
      columns.each do |column|
        ordered_records << records.detect { |record| record.description == column }
      end
      ordered_records.compact # get rid of any nil values

      results << DataRow.new(Array.new(ordered_records)) unless ordered_records.empty?
    end
    results
  end

  # Record Methods that call the build_record method

  def record_live
    return false unless resolution_includes?(:live)

    date_range = Utilities::DateRange.new(:month, Date.today)
    build_records(:live, date_range.start, date_range.stop)
  end

  def record_day(date)
    return false unless resolution_includes?(:day)

    date_range = Utilities::DateRange.new(:day, date)
    build_records(:day, date_range.start, date_range.stop)
  end

  def record_week(date)
    return false unless resolution_includes?(:week)

    date_range = Utilities::DateRange.new(:week, date)
    build_records(:week, date_range.start, date_range.stop)
  end

  def record_month(date)
    return false unless resolution_includes?(:month)

    date_range = Utilities::DateRange.new(:month, date)
    build_records(:month, date_range.start, date_range.stop)
  end

  def record_quarter(date)
    return false unless resolution_includes?(:quarter)

    date_range = Utilities::DateRange.new(:quarter, date)
    build_records(:quarter, date_range.start, date_range.stop)
  end

  def record_year(date)
    return false unless resolution_includes?(:year)

    date_range = Utilities::DateRange.new(:year, date)
    build_records(:year, date_range.start, date_range.stop)
  end
end
