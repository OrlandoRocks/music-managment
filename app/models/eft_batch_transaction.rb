class EftBatchTransaction < ApplicationRecord
  include Logging
  include Tunecore::Flagging::Suspicious
  include LoginTrackable
  extend DateFunctions

  ### ASSOCIATIONS ###
  belongs_to  :eft_batch
  belongs_to  :eft_query
  belongs_to  :stored_bank_account
  belongs_to  :payout_service_fee
  belongs_to  :country_website
  belongs_to  :failure_fee, class_name: "PayoutServiceFee"
  has_many    :eft_batch_transaction_history
  has_many    :notes, as: :related

  ### STATIC VARIABLES ###
  STATUSES = ["pending_approval", "waiting_for_batch", "canceled", "rejected", "processing_in_batch", "sent_to_bank", "error", "success", "failure"]
  FAILED_STATUSES = ["canceled", "rejected", "error", "failure"]
  PROCESSING_STATUSES = ["pending_approval", "waiting_for_batch", "sent_to_bank", "processing_in_batch"]
  CANCELABLE_STATUSES = ["pending_approval", "waiting_for_batch"]
  EFT = "EFT Batch"
  MAX_WITHDRAWAL = 250_000
  EFT_LIMIT_FOR_AUTO_APPROVAL = 0
  EFT_WITHDRAWAL_LOWER_LIMIT = 0 # this needs to be greater than the service fee
  ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_LOWER = 7
  ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_UPPER = 10
  ADMIN_EMAILS = ["report_eft@tunecore.com"]
  APPROVAL_START_TIME = "13:00:00" # 1PM cut off time for Customer Care EFT approvals. We use this for the approval summary page
  KNOWN_ERROR_RESPONSE_CODE = ["200", "300", "400"]

  ### ACCESSOR ATTRIBUTES ###
  attr_accessor :amount_from_form,
                :amount_from_form_main_currency,
                :amount_from_form_fractional_currency,
                :customer_eft_page,
                :total,
                :confirm_page,
                :password_entered,
                :from_form

  ### VALIDATIONS AND CALLBACKS ###
  before_validation :clean_amount_from_form,             on: :create, if: :customer_eft_page?
  before_validation :round_amount,                       on: :create
  before_validation :set_currency,                       on: :create
  before_validation :assign_country_website,             on: :create
  before_validation :assign_current_service_fee,         on: :create
  before_validation :calculate_amount_minus_service_fee, on: :create
  before_validation :assign_default_status,              on: :create

  # ------------- #
  validates :stored_bank_account_id, :payout_service_fee_id, :amount, :country_website_id, :currency, presence: true
  validates :amount, numericality: { greater_than: EFT_WITHDRAWAL_LOWER_LIMIT, message: " must be greater than service fee" }
  validates :status, inclusion: { in: STATUSES }

  validates :currency, inclusion: { in: ["USD"], message: "is not a valid currency for Bank Withdrawals" } # only currently provide EFT's in USD
  validates :country_website_id, inclusion: { in: [1], message: "is not a valid country for Bank Withdrawals" } # only currently provide EFT's for US Customers

  validate :overdraft_protection_check, on: :create
  validate :verify_password, unless: :confirm_page, on: :create

  validates :amount,
            numericality: {
              less_than_or_equal_to: (MAX_WITHDRAWAL - (!PayoutServiceFee.current("eft_service").nil? ? PayoutServiceFee.current("eft_service").amount : 2.75)),
              message: "must be less than the max withdrawal amount of $#{MAX_WITHDRAWAL}",
              on: :create
            }
  # ------------- #
  after_validation :display_cleaned_amount_on_form,        on: :create, if: :customer_eft_page?
  after_validation :assign_initial_status_based_on_amount, on: :create
  after_validation :flag_suspicious_locked_accounts,       on: :create
  after_create :add_create_to_history
  after_create :generate_order_id
  after_create :debit_amount
  after_create :debit_service_fee
  after_create :send_confirmation_email

  ### NAMED SCOPES ###
  scope :waiting, -> { where(status: "waiting_for_batch") }
  scope :pending_approval, -> { where(status: "pending_approval") }
  scope :can_be_canceled, -> { where(status: ["pending_approval", "waiting_for_batch"]) }
  scope :by_date, ->(date) { where("DATE(eft_batch_transactions.created_at) = ?", date) }
  scope :by_status, ->(status) { where(status: status) }
  scope :with_history_status,
        ->(history_status) {
          joins(:eft_batch_transaction_history)
            .where(eft_batch_transaction_history: { status: history_status })
        }
  scope :approval_summary,
        -> {
          select(<<-SQL.strip_heredoc)
      MAKEDATE(year_of_eft, day_of_year) as date_of_request, year_of_eft, day_of_year, COUNT(DISTINCT id) AS total_count,
      SUM(CASE WHEN t.h_status = 'requested' THEN t.amount END) AS total_amount,
      COUNT(DISTINCT CASE WHEN t.h_status = 'approved' THEN id END) AS approved_count,
      SUM(DISTINCT CASE WHEN t.h_status = 'approved' THEN t.amount END) AS approved_amount,
      COUNT(DISTINCT CASE WHEN t.h_status = 'removed_approval' THEN id END) AS removed_approval_count,
      SUM(DISTINCT CASE WHEN t.h_status = 'removed_approval' THEN t.amount END) AS removed_approval_amount,
      COUNT(DISTINCT CASE WHEN t.h_status = 'canceled' THEN id END) AS canceled_count,
      SUM(DISTINCT CASE WHEN t.h_status = 'canceled' THEN t.amount END) AS canceled_amount,
      COUNT(DISTINCT CASE WHEN t.h_status = 'rejected' THEN id END) AS rejected_count,
      SUM(DISTINCT CASE WHEN t.h_status = 'rejected' THEN t.amount END) AS rejected_amount,
      COUNT(DISTINCT CASE WHEN t.status = 'pending_approval' THEN id END) AS pending_count,
      SUM(DISTINCT CASE WHEN t.status = 'pending_approval' THEN t.amount END) AS pending_amount
          SQL
            .from("(SELECT ebt.id, ebt.amount,ebt.status, h.status AS h_status, ebt.created_at AS created_datetime,
             if(HOUR(ebt.created_at) > 13, DAYOFYEAR(ebt.created_at) + 1, DAYOFYEAR(ebt.created_at)) as day_of_year, YEAR(ebt.created_at) as year_of_eft
             FROM eft_batch_transactions ebt JOIN eft_batch_transaction_history h ON ebt.id = h.eft_batch_transaction_id) AS t")
            .group("year_of_eft, day_of_year")
            .limit(20)
            .order("day_of_year DESC")
        }

  ### CONDITIONALS ###
  def customer_eft_page?
    customer_eft_page != nil
  end

  def processing?
    PROCESSING_STATUSES.include?(status)
  end

  def success?
    status == "success"
  end

  def failed?
    FAILED_STATUSES.include?(status)
  end

  def already_debited_amount?
    eft_batch_transaction_history.reload.collect(&:status).include?("debit_service_fee")
  end

  def already_debited_service_fee?
    eft_batch_transaction_history.reload.collect(&:status).include?("debit_service_fee")
  end

  def already_debited_failure_fee?
    eft_batch_transaction_history.reload.collect(&:status).include?("debit_failure_fee")
  end

  def rolled_back?
    eft_batch_transaction_history.reload.collect(&:status).include?("rollback_debit")
  end

  def rolled_back_service_fee?
    eft_batch_transaction_history.reload.collect(&:status).include?("rollback_service_fee")
  end

  def can_be_canceled?
    CANCELABLE_STATUSES.include?(status)
  end

  def can_be_approved?
    status == "pending_approval"
  end

  def can_be_unapproved?
    status == "waiting_for_batch"
  end

  def can_be_errored_or_sent_to_bank?
    status == "processing_in_batch"
  end

  def waiting_for_bank_response?
    status == "sent_to_bank"
  end

  def can_be_updated_by_bank?
    waiting_for_bank_response? || success?
  end

  def flag_new_bank_account?
    return false unless status == "pending_approval"
    return true  if stored_bank_account.successful_transaction_total.zero?

    if stored_bank_account.last_successful_eft_withdrawal.present?
      stored_bank_account.updated_at > stored_bank_account.last_successful_eft_withdrawal.created_at
    else
      true
    end
  end

  ### CLASS METHODS ###
  def self.summary_grouped_by_status
    EftBatchTransaction.select("status, COUNT(amount) as total_count, SUM(amount) as total_amount").group("status").order("total_count DESC")
  end

  def self.search(params)
    search = all

    if params
      if params[:person_status].present?
        search = search.joins({ stored_bank_account: :person }).where("people.status = ?", params[:person_status])
      end
      if params[:person_email].present?
        search = search.joins({ stored_bank_account: :person }).where("people.email like ?", "%#{params[:person_email]}%")
      end
      if params[:person_name].present?
        search = search.joins({ stored_bank_account: :person }).where("people.name like ?", "%#{params[:person_name]}%")
      end
      if params[:artist_name].present?
        search = search.joins({ stored_bank_account: { person: :artist } }).where("artists.name like ?", "%#{params[:artist_name]}%")
      end

      search = search.where("eft_batch_transactions.id = ?", params[:id]) if params[:id].present?
      if params[:eft_batch_id].present?
        search = search.where("eft_batch_transactions.eft_batch_id = ?", params[:eft_batch_id])
      end
      if params[:eft_query_id].present?
        search = search.where("eft_batch_transactions.eft_query_id = ?", params[:eft_query_id])
      end
      search = search.where("eft_batch_transactions.order_id = ?", params[:order_id]) if params[:order_id].present?
      search = search.where("eft_batch_transactions.status = ?", params[:eft_status]) if params[:eft_status].present?

      search = search.where("eft_batch_transactions.amount >= ?", params[:amount_min]) if params[:amount_min].present?
      search = search.where("eft_batch_transactions.amount <= ?", params[:amount_max]) if params[:amount_max].present?

      if params[:created_at_min].present?
        search = search.where("eft_batch_transactions.created_at >= ?", DateTime.strptime(params[:created_at_min], "%m/%d/%Y"))
      end
      if params[:created_at_max].present?
        search = search.where("eft_batch_transactions.created_at <= ?", DateTime.strptime(params[:created_at_max], "%m/%d/%Y"))
      end
    end

    Tunecore::Search.new(params, search)
  end

  ### OBJECT METHODS ###
  delegate :person, to: :stored_bank_account

  def service_fee
    payout_service_fee.amount
  end

  delegate :amount, to: :failure_fee, prefix: true

  def expected_deposit_date(num_of_days = ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_UPPER)
    temp_date = created_at
    num_of_days.times do
      while (temp_date.wday == 5 || temp_date.wday == 6)
        temp_date += 1.day
      end
      temp_date += 1.day
    end
    temp_date
  end

  # Customer cancels withdrawal before it gets processed
  # THIS HAS BEEN REMOVED FROM THE EFT RELEASE - KEEPING IT HERE IN CASE WE EVER NEED TO IMPLEMENT IT
  def cancel
    return unless can_be_canceled?

    update(status: "canceled")
    log(EFT, "Bank withdrawal canceled by customer", person_id: person.id, eft_batch_transaction_id: id, amount: amount, service_fee: service_fee, currency: currency)
    EftBatchTransactionHistory.create(status: "canceled", eft_batch_transaction: self, comment: "Customer canceled Electronic Funds Transfer request")
    rollback_debit
    rollback_service_fee
    EftNotifier.failure_notice(id).deliver
    EftBatchTransactionHistory.create(status: "email_sent", eft_batch_transaction: self, comment: "Cancelation email sent to customer")
    self
  end

  # TC Admin approves a 'pending_approval' withdrawal
  def approve(admin_user)
    return unless can_be_approved?

    update(status: "waiting_for_batch")
    log(EFT, "Bank withdrawal approved by TCAdmin", person_id: person.id, eft_batch_transaction_id: id, amount: amount, service_fee: service_fee, currency: currency, posted_by_name: admin_user.name)
    EftBatchTransactionHistory.create(
      status: "approved",
      eft_batch_transaction_id: id,
      posted_by_id: admin_user.id,
      posted_by_name: admin_user.name,
      comment: "TC Admin approved this transaction"
    )
    self
  end

  # TC Admin removes an approval withdrawal
  def remove_approval(admin_user)
    return unless can_be_unapproved?

    update(status: "pending_approval")
    log(EFT, "Bank withdrawal changed back to pending by TCAdmin", person_id: person.id, eft_batch_transaction_id: id, amount: amount, service_fee: service_fee, currency: currency, posted_by_name: admin_user.name)
    EftBatchTransactionHistory.create(status: "removed_approval", eft_batch_transaction: self, posted_by_id: admin_user.id, posted_by_name: admin_user.name, comment: "TC Admin changed this transaction back to pending")
    self
  end

  # TC Admin rejects a 'pending_approval' withdrawal
  def reject(admin_user, message = nil, admin_note = nil, validate = true)
    return unless can_be_canceled?

    if validate
      update(status: "rejected", email_message: message)
    else
      self.status = "rejected"
      save(validate: false)
    end
    log(EFT, "Bank withdrawal rejected by TCAdmin", person_id: person.id, eft_batch_transaction_id: id, amount: amount, service_fee: service_fee, currency: currency, posted_by_name: admin_user.name)
    EftBatchTransactionHistory.create(
      status: "rejected",
      eft_batch_transaction_id: id,
      posted_by_id: admin_user.id,
      posted_by_name: admin_user.name,
      comment: "TC Admin Rejected this transaction"
    )
    rollback_debit
    rollback_service_fee
    unless admin_note.nil?
      Note.create(related: self, subject: "Electronic Funds Transfer Rejected by Admin", note_created_by: admin_user, note: admin_note)
    end
    EftNotifier.failure_notice(id, message).deliver
    EftBatchTransactionHistory.create(
      status: "email_sent",
      eft_batch_transaction_id: id,
      posted_by_id: admin_user.id,
      posted_by_name: admin_user.name,
      comment: "Rejection Email sent to customer"
    )
    self
  end

  # Errors out during Braintree's initial check
  def error(transaction_id, response_code, response_text)
    if can_be_errored_or_sent_to_bank?
      update(status: "error", transaction_id: transaction_id, response_code: response_code, response_text: response_text)
      log(
        EFT,
        "Bank withdrawal errored on initial Braintree inspection",
        person_id: person.id,
        eft_batch_transaction_id: id,
        transaction_id: transaction_id,
        response_code: response_code,
        response_text: response_text,
        amount: amount,
        service_fee: service_fee,
        currency: currency
      )
      EftBatchTransactionHistory.create(
        status: "error",
        eft_batch_transaction_id: id,
        comment: "Bank withdrawal errored on initial Braintree inspection"
      )
      rollback_debit
      rollback_service_fee
      EftNotifier.failure_notice(id).deliver
      EftBatchTransactionHistory.create(
        status: "email_sent",
        eft_batch_transaction_id: id,
        comment: "Error email sent to customer"
      )
    end
    self
  end

  # Passes Braintree's initial check
  def sent_to_bank(transaction_id)
    return unless can_be_errored_or_sent_to_bank?

    update(status: "sent_to_bank", transaction_id: transaction_id)
    log(EFT, "Bank withdrawal successfully sent to bank by Braintree", person_id: person.id, eft_batch_transaction_id: id, transaction_id: transaction_id, amount: amount, service_fee: service_fee, currency: currency)
    EftBatchTransactionHistory.create(
      status: "sent_to_bank",
      eft_batch_transaction_id: id,
      comment: "Bank withdrawal successfully sent to bank by Braintree"
    )
    self
  end

  # Transaction is successfully deposited in the bank
  def success(query_response_code, query_response_text, eft_query)
    return unless waiting_for_bank_response?

    update(status: "success", response_code: query_response_code, response_text: query_response_text, eft_query: eft_query)
    log(EFT, "Bank withdrawal successfully deposited in customer's account", person_id: person.id, eft_batch_transaction_id: id, batch_id: eft_batch_id, amount: amount, service_fee: service_fee, currency: currency)
    EftBatchTransactionHistory.create(
      status: "success",
      eft_batch_transaction_id: id,
      comment: "Bank withdrawal successfully deposited in customer's bank account"
    )
    EftNotifier.success_notice(id).deliver
    EftBatchTransactionHistory.create(
      status: "email_sent",
      eft_batch_transaction_id: id,
      comment: "Success Email sent to customer"
    )
    self
  end

  # Transaction fails during bank deposit phase
  def failure(query_response_code, query_response_text, eft_query)
    if can_be_updated_by_bank?
      failure_fee = PayoutServiceFee.current("eft_failure")
      update(status: "failure", response_code: query_response_code, response_text: query_response_text, eft_query: eft_query, failure_fee: failure_fee)
      log(EFT, "Bank withdrawal failed to deposit in customer's bank account", person_id: person.id, eft_batch_transaction_id: id, batch_id: eft_batch_id, amount: amount, currency: currency, service_fee: service_fee)
      EftBatchTransactionHistory.create(
        status: "failure",
        eft_batch_transaction_id: id,
        comment: "Bank withdrawal failed to deposit in customer's bank account"
      )
      rollback_debit
      debit_failure_fee
      EftNotifier.failure_notice(id).deliver
      EftBatchTransactionHistory.create(
        status: "email_sent",
        eft_batch_transaction_id: id,
        comment: "Failure email sent to customer"
      )
    end
    self
  end

  def set_suspicious_flags
    self.suspicious_flags = Tunecore::Flagging::StoredBankAccountFlagger.new(self).set_flags
  end

  private

  ### VALIDATION METHODS ###
  def overdraft_protection_check
    return unless !amount.nil? && !stored_bank_account.nil? && !payout_service_fee.nil?

    balance = BigDecimal(stored_bank_account.person.person_balance.balance.to_s)
    withdrawal_amount = BigDecimal((amount + service_fee).to_s)

    return unless withdrawal_amount > balance

    errors.add(
      :base,
      I18n.t("models.billing.withdrawal_amount_must_be_less_than_or_equal_to_your_current_account_balance")
    )
  end

  def verify_password
    return unless person.authenticate!(password_entered).nil?

    errors.add(:base, I18n.t("models.billing.sorry_that_password_is_incorrect"))
  end

  def person_has_no_eft_transactions_outstanding
    return if stored_bank_account.nil?

    person = stored_bank_account.person

    # check if the person has any transactions that have a processing status
    return if (person.eft_batch_transactions.map(&:status) & PROCESSING_STATUSES).empty?

    errors.add(:base, I18n.t("models.billing.you_may_only_have_one_bank_withdrawal_processing_at_a_time_please_try_again_once_your_withdrawal_has_completed"))
  end

  ### CALLBACK METHODS ###

  def clean_amount_from_form
    self.amount = amount_from_form.to_s.gsub(/(\$|,)/, "").to_f unless amount_from_form.nil?
  end

  def round_amount
    self.amount = (amount * 100).round.to_f / 100 unless amount.nil?
  end

  def assign_country_website
    self.country_website = person.country_website
  end

  def set_currency
    self.currency = person.currency
  end

  def assign_current_service_fee
    # since there won't be an eft service fee for other countries, don't pass
    # in the country_website_id and let it default to 1
    self.payout_service_fee = PayoutServiceFee.current("eft_service")
  end

  def calculate_amount_minus_service_fee
    self.amount = ((amount - service_fee) * 100).round.to_f / 100
  end

  def assign_default_status
    status = "pending_approval"
  end

  # transactions with a 'waiting_for_batch' status will be sent to the bank, 'pending_approval' transactions need to be manually approved by a TC Admin
  # status defaults to pending_approval in DB if none provided
  def assign_initial_status_based_on_amount
    self.status = ((amount < EFT_LIMIT_FOR_AUTO_APPROVAL) ? "waiting_for_batch" : "pending_approval")
  end

  def flag_suspicious_locked_accounts
    return if stored_bank_account.nil?

    person = stored_bank_account.person
    self.status = "pending_approval" if person.status == "Locked" || person.status == "Suspicious"
  end

  def display_cleaned_amount_on_form
    self.amount_from_form = amount + service_fee
  end

  def add_create_to_history
    EftBatchTransactionHistory.create(
      status: "requested",
      eft_batch_transaction_id: id,
      comment: "Electronic Funds Transfer Transaction requested successfully"
    )
    log(EFT, "Electronic Funds Transfer Transaction Requested", eft_batch_transaction_id: id)
  end

  def generate_order_id
    self.order_id = "eft#{id}"
  end

  def send_confirmation_email
    EftNotifier.confirmation(id).deliver
    EftBatchTransactionHistory.create(status: "email_sent", eft_batch_transaction_id: id, comment: "Withdrawal Request Confirmation Email Sent to Customer")
  end

  # made this private, since it should only be called by the after_create callback
  def debit_amount
    if !already_debited_amount?
      person_id = stored_bank_account.person_id
      transaction = PersonTransaction.create(
        person_id: person_id,
        debit: amount,
        currency: currency,
        target: self,
        comment: "Electronic Funds Transfer"
      )
      EftBatchTransactionHistory.create(status: "debit_amount", person_transaction_id: transaction.id, eft_batch_transaction_id: id, amount: -amount, comment: "Debit Electronic Funds Transfer")
      log(EFT, "Debit Amount", eft_batch_transaction_id: id, amount: amount, currency: currency)
    else
      message = log(EFT, "Attempted to debit amount twice", eft_batch_transaction_id: id, amount: amount)
      EftNotifier.admin_error_notice(message, id).deliver
    end
  end

  # made this private, since it should only be called by the after_create callback
  def debit_service_fee
    if !already_debited_service_fee?
      person_id = stored_bank_account.person_id
      transaction = PersonTransaction.create(
        person_id: person_id,
        debit: service_fee,
        currency: currency,
        target: self,
        comment: "Electronic Funds Transfer Service Fee"
      )
      EftBatchTransactionHistory.create(status: "debit_service_fee", person_transaction_id: transaction.id, eft_batch_transaction_id: id, amount: -service_fee, comment: "Debit Electronic Funds Transfer Service Fee")
      log(EFT, "Debit Service Fee", eft_batch_transaction_id: id, amount: service_fee)
    else
      message = log(EFT, "Attempted to debit service fee twice", eft_batch_transaction_id: id, amount: amount)
      EftNotifier.admin_error_notice(message, id).deliver
    end
  end

  ### OTHER PRIVATE METHODS ###

  # made this private, since it should only be called by the after_create callback
  def rollback_debit
    if !rolled_back?
      person_id = stored_bank_account.person_id
      transaction = PersonTransaction.create(
        person_id: person_id,
        credit: amount,
        currency: currency,
        target: self,
        comment: "Rollback of Electronic Funds Transfer"
      )
      EftBatchTransactionHistory.create(
        status: "rollback_debit",
        person_transaction_id: transaction.id,
        eft_batch_transaction_id: id,
        amount: amount,
        comment: "Rollback of Electronic Funds Transfer"
      )
      log(EFT, "Rollback Debit", eft_batch_transaction_id: id, amount: amount, currency: currency)
    else
      message = log(EFT, "Attempted to rollback twice", eft_batch_transaction_id: id, amount: amount, currency: currency)
      EftNotifier.admin_error_notice(message, id).deliver
    end
  end

  def rollback_service_fee
    if !rolled_back_service_fee?
      person_id = stored_bank_account.person_id
      transaction = PersonTransaction.create(
        person_id: person_id,
        credit: service_fee,
        currency: currency,
        target: self,
        comment: "Rollback of Electronic Funds Transfer Service Fee"
      )
      EftBatchTransactionHistory.create(status: "rollback_service_fee", person_transaction_id: transaction.id, eft_batch_transaction_id: id, amount: service_fee, currency: currency, comment: "Rollback of Electronic Funds Transfer Service Fee")
      log(EFT, "Rollback Service Fee", eft_batch_transaction_id: id, amount: service_fee, currency: currency)
    else
      message = log(EFT, "Attempted to rollback service fee twice", eft_batch_transaction_id: id, amount: service_fee, currency: currency)
      EftNotifier.admin_error_notice(message, id).deliver
    end
  end

  def debit_failure_fee
    if !already_debited_failure_fee?
      person_id = stored_bank_account.person_id
      transaction = PersonTransaction.create(
        person_id: person_id,
        debit: failure_fee_amount,
        currency: currency,
        target: self,
        comment: "Electronic Funds Transfer Failure Fee"
      )
      EftBatchTransactionHistory.create(status: "debit_failure_fee", person_transaction_id: transaction.id, eft_batch_transaction_id: id, amount: -failure_fee_amount, comment: "Electronic Funds Transfer Failure Fee")
      log(EFT, "Debit Failure Fee", eft_batch_transaction_id: id, amount: failure_fee_amount, currency: currency)
    else
      message = log(EFT, "Attempted to debit Failure Fee twice", eft_batch_transaction_id: id, amount: failure_fee_amount, currency: currency)
      EftNotifier.admin_error_notice(message, id).deliver
    end
  end
end
