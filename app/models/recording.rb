class Recording < ActiveRecord::Base
  belongs_to :composition
  belongs_to :publishing_composition
  belongs_to :recordable, polymorphic: true

  validate :composition_present?
  validates :isrc, uniqueness: { allow_blank: true }

  private

  def composition_present?
    errors.add(
      :base,
      "A composition or publishing_composition must be present"
    ) unless composition || publishing_composition
  end
end
