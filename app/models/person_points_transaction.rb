class PersonPointsTransaction < ApplicationRecord
  CREDIT_SOURCE = [Achievement, Certification]
  DEBIT_SOURCE = [Reward]

  belongs_to :person
  belongs_to :target, polymorphic: true

  validates :person, :target, presence: true
  validate :valid_credit_or_debit
  validate :target_credit_debit
  validate :points_not_changed

  after_create :update_balance

  def is_credit?
    credit_points.present? && credit_points.positive?
  end

  def is_debit?
    debit_points.present? && debit_points.positive?
  end

  def update_balance
    person.points_balance.update_balance(credit_points - debit_points)
  end

  private

  def points_not_changed
    return unless (saved_changes.keys & ["credit_points", "debit_points"]).present? && persisted?

    errors.add(:transaction, "cannot update credit or debit points!")
  end

  def valid_credit_or_debit
    if is_credit? && is_debit?
      errors.add(:transaction, "cannot have both credit & debit")
      return
    end

    errors.add(:transaction, "cannot be empty on both credit & debit") if !is_credit? && !is_debit?
  end

  def target_credit_debit
    if is_debit? && !DEBIT_SOURCE.include?(target.class)
      errors.add(:target, "outside of allowed debit classes - #{DEBIT_SOURCE.map(&:to_s)}")
      return
    end

    return unless is_credit? && !CREDIT_SOURCE.include?(target.class)

    errors.add(:target, "outside of allowed credit classes - #{CREDIT_SOURCE.map(&:to_s)}")
  end
end
