class ArtistStoreWhitelist < ApplicationRecord
  belongs_to :artist
  belongs_to :store

  validates :artist_id, uniqueness: { scope: :store_id }
end
