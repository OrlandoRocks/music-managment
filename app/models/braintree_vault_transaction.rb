#
#  = Description
#  The braintree_vault_transaction model is the wrapper for the stored_credit_card transaction model
#  This model processes raw braintree_vault transactions and stores the failures and successes of any
#  requests made to braintree to store, update, or delete a user's stored credit cards.
#  Any new stored credit cards that are added when a purchase is made have transactions that
#  stored in the braintree_transaction model (the model that controls payments)
#
#  = Change Log
#  *2009-08-25 - CH - Created the model

#  *2010-06-22 - DCD - Refactored self.process method to make it cleaner and to generalize it for both StoredCreditCards and StoredBankAccounts

class BraintreeVaultTransaction < ApplicationRecord
  belongs_to :person
  belongs_to :related, polymorphic: true

  #
  # The process method takes the raw query_string from a braintree
  # transaction and stores the raw data and creates the
  # stored_credit_card if the request was successful.
  # All request to create, update, or delete a stored credit card
  # should be passed through this method.
  #
  # Braintree will only return 100 or 300 as response codes when doing vault actions
  #

  def self.process(query_type, query_string, person_id, ip_address)
    stored_item, invalid, message, related_id, related_type, transaction_type = nil # initialize variables
    response = ActiveMerchant::Billing::Integrations::Braintree::Return.new(query_string, api_key: BRAINTREE[:key])

    if response.valid?
      response_code = response.response_code
      message = response.message
      if response_code == 100 # successful transaction
        transaction_type = transaction_type_for_log(query_type, response.customer_id) # returns "Addition" or "Update"
        stored_item = process_related_stored_item(query_type, response.customer_id, person_id, ip_address) # create/update related StoredBankAccount or StoredCreditCard
        related_id = stored_item.id
        related_type = query_type

        # if a creating/updating a credit card, and the person selects to use this CC as their preferred card, then update their preferences
        if (query_type == "StoredCreditCard" && response.mark_as_preferred?)
          update_preferences(person_id, response.renew_with_balance?, stored_item)
        end
      end
      vault_transaction = create(
        person_id: person_id,
        response_code: response_code,
        raw_response: query_string,
        ip_address: ip_address,
        related_id: related_id,
        related_type: related_type
      )
    else
      # the controller uses returned attributes to process, so we need to assign attributes to a vault_transaction we don't save
      vault_transaction = new(response_code: 300, raw_response: "Invalid Request")
      invalid = true
    end

    log_transaction(
      ip_address,
      person_id,
      transaction_type: transaction_type,
      response_code: vault_transaction.response_code,
      related_id: related_id,
      related_type: related_type,
      invalid: invalid,
      message: message
    )

    vault_transaction
  end

  #
  # Unlike the process method, all CC deletions go through the StoredCreditCard model
  # using the Direct Post method at Braintree.
  # This log deletion method simply creates the correct BraintreeVaultTransaction record
  # and logs the correct information in the Rails log.
  #
  def self.log_deletion(stored_item, ip_address, response, elapsed_time)
    response_code = response.params["response_code"] rescue nil
    response_message = response.message rescue nil
    BraintreeVaultTransaction.create(
      ip_address: ip_address,
      person_id: stored_item.person_id,
      raw_response: response_message,
      related_id: stored_item.id,
      related_type: stored_item.class.to_s,
      response_code: response_code,
    )
    log_transaction(
      ip_address,
      stored_item.person_id,
      {
        elapsed_time: elapsed_time,
        message: response_message,
        related_id: stored_item.id,
        related_type: stored_item.class.to_s,
        response_code: response_code,
        transaction_type: "Deletion",
      }
    )
  end

  def self.process_related_stored_item(query_type, customer_vault_id, person_id, ip_address)
    case query_type
    when "StoredCreditCard"
      stored_item = StoredCreditCard.find_by(customer_vault_id: customer_vault_id)
      stored_item.nil? ? StoredCreditCard.create(customer_vault_id: customer_vault_id, person_id: person_id) : stored_item.update_vault_data(true)
    when "StoredBankAccount"
      StoredBankAccount.create(customer_vault_id: customer_vault_id, person_id: person_id, ip_address: ip_address)
    end
  end

  def self.transaction_type_for_log(query_type, customer_vault_id)
    case query_type
    when "StoredBankAccount"
      "Addition"
    when "StoredCreditCard"
      StoredCreditCard.find_by(customer_vault_id: customer_vault_id).nil? ? "Addition" : "Update"
    end
  end

  def self.update_preferences(person_id, pay_with_balance, stored_credit_card)
    person_preference = PersonPreference.where(person_id: person_id).first
    if person_preference.nil?
      PersonPreference.create(person_id: person_id, pay_with_balance: pay_with_balance, preferred_credit_card: stored_credit_card, preferred_payment_type: "CreditCard")
    else
      person_preference.update(pay_with_balance: pay_with_balance, preferred_credit_card: stored_credit_card, preferred_payment_type: "CreditCard")
    end
  end

  #
  # log_transaction is the abstracted logger mechanism for the BriantreeVaultTransaction
  # it simply accepts the options outlined below and keeps them logging in a standard format
  #
  def self.log_transaction(ip_address, person_id, options = {})
    logged_action = "Braintree Vault Transaction |"
    if options[:invalid].nil?
      logged_action += " ip=#{ip_address} "
      logged_action += " transaction_type=#{options[:transaction_type]} " if options[:transaction_type]
      logged_action += " response_code=#{options[:response_code]} " if options[:response_code]
      logged_action += " person_id=#{person_id} "
      logged_action += " related_type=#{options[:related_type]}" if options[:related_type]
      logged_action += " related_id=#{options[:related_id]}" if options[:related_id]
      logged_action += ' message="' + options[:message] + '"' if options[:message]
      logged_action += " elapsed_time=#{options[:elapsed_time]}" if options[:elapsed_time]
    else
      logged_action += " response_code=-1 "
      logged_action += " ip=#{ip_address} "
      logged_action += " person_id=#{person_id} "
    end

    logger.info logged_action
  end
end
