# = Description
# This used to be the Products model, but we reclaimed the namespace for the product management models.
# This table holds products the business wants to promote.  Typically, the records in this table represent
# products TuneCore sells outside of our ecommerce system through e-Junkie.
#
#
# = Change Log
# [2009-12-17 -- CH]
# Renamed model from Product to Promotion

class PromotionalProduct < ApplicationRecord
  validates :title, :body, :start_at, :finish_at, :short_title, presence: true
  validates :url_slug, uniqueness: true

  acts_as_list scope: "online"

  before_validation :set_start_and_finish
  after_save :save_files

  def set_start_and_finish
    self.start_at = Time.now if start_at.nil?
    self.finish_at = Time.now if finish_at.nil?
  end

  def save_files
    if @file_mini.present?
      logger.debug("About to save the mini file")
      file = @file_mini
      file.rewind
      File.open(File.join(Rails.root, PRODUCT_MINI_IMAGES_PATH, mini_name), "wb") { |f| f.write(file.read) }
    end

    return if @file_full.blank?

    logger.debug("About to save the large file")
    file = @file_full
    file.rewind
    File.open(File.join(Rails.root, PRODUCT_FULL_IMAGES_PATH, full_name), "wb") { |f| f.write(file.read) }
  end

  def full_image=(file)
    @file_full = file
  end

  def full_image_web_path
    if FileTest.exist?(File.join(Rails.root, PRODUCT_FULL_IMAGES_PATH, full_name))
      PRODUCT_FULL_IMAGES_WEBPATH + full_name
    else
      PromotionalProduct.no_full_promo_path
    end
  end

  def full_name
    "product_full_#{id}.jpg"
  end

  def mini_image=(file)
    @file_mini = file
  end

  def mini_image_web_path
    if FileTest.exist?(File.join(Rails.root, PRODUCT_MINI_IMAGES_PATH, mini_name))
      PRODUCT_MINI_IMAGES_WEBPATH + mini_name
    else
      PromotionalProduct.no_mini_promo_path
    end
  end

  def mini_name
    "product_mini_#{id}.jpg"
  end

  def self.no_mini_promo_path
    "/images/promo_mini.jpg"
  end

  def self.no_full_promo_path
    "/images/promo_full.jpg"
  end

  def self.top_products
    # return the array of 3 products
    list = [46, 47, 43]
    products = []

    list.each do |id|
      products << PromotionalProduct.find(id)
    end
    products
  rescue
    PromotionalProduct.where("online = true and finish > ?", Date.today).order("start desc").limit(3)
  end

  # extracts the price from the body of the product
  def price
    body[/[0-9]{1,}.[0-9]{2}/]
  end
end
