# frozen_string_literal: true

class PayuWebhookLog < ApplicationRecord
  validates :payu_transaction, :payu_transaction_created_at, presence: true
  validates :success, inclusion: { in: [true, false] }

  belongs_to :payu_transaction

  before_validation :assign_payu_transaction_created_at

  private

  def assign_payu_transaction_created_at
    self.payu_transaction_created_at = payu_transaction.created_at
  end
end
