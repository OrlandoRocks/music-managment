class RightsAppError < ApplicationRecord
  ACCOUNT_BASED_REQUESTS      = %(ArtistAccount WriterList WorkList).freeze
  COMPOSITION_BASED_REQUESTS  = %(Recording RecordingList Work).freeze
  WRITER_BASED_REQUESTS       = %(Writer).freeze

  belongs_to :requestable, polymorphic: true

  validates :http_method, inclusion: { in: %w[DELETE GET PATCH POST PUT] }
  validates :requestable_type,
            inclusion: {
              in: %w[
                Account
                Composer
                Cowriter
                Composition
                PublishingComposer
                PublishingComposition
              ]
            }

  serialize :json_request, QuirkyJson
  serialize :json_response, QuirkyJson

  scope :over_sixty_days_old, -> { where("DATE(created_at) < ?", Date.today - 60.days) }

  before_save :upcase_http_method

  private

  def upcase_http_method
    http_method.upcase!
  end
end
