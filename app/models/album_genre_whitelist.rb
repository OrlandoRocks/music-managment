class AlbumGenreWhitelist < ApplicationRecord
  belongs_to :album
  belongs_to :genre
  has_many :genre_permissions, dependent: :destroy
end
