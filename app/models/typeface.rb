class Typeface < ApplicationRecord
  has_many :covers, foreign_key: :artist_typeface_id
  has_many :covers, foreign_key: :title_typeface_id

  has_many :typeface_genres
  has_many :genres, through: :typeface_genres

  validates :name, :description, presence: true
end
