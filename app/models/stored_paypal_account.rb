class StoredPaypalAccount < ApplicationRecord
  ### REQUIRES/INCLUDES ###

  ### ASSOCIATIONS ###
  belongs_to  :person
  belongs_to  :paypal_transaction
  belongs_to  :country_website
  belongs_to  :payin_provider_config
  has_many    :paypal_transactions, foreign_key: "referenced_paypal_account_id"
  has_many    :invoice_logs

  delegate :person_preference, to: :person, allow_nil: true

  ### STATIC VARIABLES ###

  ### ACCESSOR ATTRIBUTES ###

  ### VALIDATIONS AND CALLBACKS ###
  before_validation :set_country_website, on: :create
  # ------------- #
  # validation
  validates :person_id, :country_website_id, presence: true
  # ------------- #
  # after_validation

  before_create :mark_previous_account_as_deleted
  after_create  :update_person_preferences

  ### NAMED SCOPES ###
  scope :active, -> { where(archived_at: nil) }

  ### CONDITIONALS ###

  ### CLASS METHODS ###
  def self.currently_active_for_person(person)
    where("person_id = ? and archived_at is null", person.id).first
  end

  ### OBJECT METHODS ###

  def destroy(person, options = {})
    options[:skip_validation] ||= false

    transaction do
      update(archived_at: Time.now)
      person_preference.delete_stored_paypal_account_proc!(id, person, options)
      true
    end
  rescue ActiveRecord::RecordInvalid => e
    false
  end

  def payin_provider_config
    self[:payin_provider_config] || person.paypal_payin_provider_config
  end

  def pay(invoice)
    PaypalTransaction.process_reference_transaction(self, invoice)
  end

  def successful_transaction_total
    paypal_transactions.find_all { |pt| pt.status == "Completed" && pt.action = "Sale" }.sum(&:amount).to_f
  end

  def failure_transaction_total
    paypal_transactions.find_all { |pt| pt.status == "Failed" }.sum(&:amount).to_f
  end

  def self.checkout_credentials(country_website)
    PAYPAL_CREDS[country_website.currency] || PAYPAL_CREDS["DEFAULT"]
  end
  deprecate :checkout_credentials

  def self.process_authorization(options)
    person, email, _ip, token, payerid, amount, currency, pay_with_balance, payin_config = options
                                                                                           .values_at(
                                                                                             :person,
                                                                                             :email,
                                                                                             :_ip,
                                                                                             :token,
                                                                                             :payerid,
                                                                                             :amount,
                                                                                             :currency,
                                                                                             :pay_with_balance,
                                                                                             :payin_config
                                                                                           )
    country_website = person.country_website
    transaction = PayPalAPI.process(payin_config, token, payerid, "Authorization", amount, currency)

    account = nil

    if transaction.response["ACK"].to_s != "Failure"
      account_options = {
        person: person,
        country_website: country_website,
        transaction: transaction,
        email: email,
        pay_with_balance: pay_with_balance,
        credentials: payin_config
      }
      account = store_account(account_options)
    end

    [transaction.response["ACK"].to_s, account]
  end

  def self.store_account(opts)
    person, transaction = opts.values_at(:person, :transaction)
    account = create(
      person_id: person.id,
      country_website: opts[:country_website],
      billing_agreement_id: transaction.response["BILLINGAGREEMENTID"].to_s,
      email: opts[:email],
      payin_provider_config: opts[:credentials]
    )
    PayPalAPI.void(opts[:credentials], transaction.response["PAYMENTINFO_0_TRANSACTIONID"].to_s)

    person.person_preference.update_attribute(:preferred_payment_type, "PayPal")
    if opts[:pay_with_balance] == 1
      person.person_preference.update_attribute(:pay_with_balance, true)
    else
      person.person_preference.update_attribute(:pay_with_balance, false)
    end

    account
  end

  private

  ### VALIDATION METHODS ###

  def set_country_website
    self.country_website = paypal_transaction.country_website if paypal_transaction
  end

  ### CALLBACK METHODS ###
  def mark_previous_account_as_deleted
    previous_stored_paypal_account = StoredPaypalAccount.find_by("archived_at IS NULL AND person_id = ?", person_id)
    previous_stored_paypal_account.update_attribute(:archived_at, Time.now) if previous_stored_paypal_account
  end

  def update_person_preferences
    person.create_person_preference(preferred_payment_type: "PayPal") if person.person_preference.nil?
    person.person_preference.update_attribute(:preferred_paypal_account_id, id)
  end
  ### OTHER PRIVATE METHODS ###
end
