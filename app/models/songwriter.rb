class Songwriter < ApplicationRecord
  belongs_to :person
  has_and_belongs_to_many :songs
  validates :first_name, :last_name, :person_id, presence: true

  def full_name
    middle_name.present? ? "#{first_name} #{middle_name} #{last_name}" : "#{first_name} #{last_name}"
  end
end
