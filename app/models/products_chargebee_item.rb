class ProductsChargebeeItem < ApplicationRecord
  validates :product, :chargebee_item_id, presence: true

  belongs_to :product

  # TODO replace this with call to redis cache of chargebee items
  def chargebee_item
    ChargeBee::Item.retrieve(chargebee_item_id)
  end
end
