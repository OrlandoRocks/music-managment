# = Description
# The ReportDataCell class supports ReportDataRow and makes up the cells of each instantianted row, which in turn make up the rows in ReportDataTable.
#
# = Usage
# This class is utilized exclusively by ReportDataRow and should not be called directly.
# * The ReportDataCell has two attributes, :value and :data_type.
# * This allows the data_type to be specified which can then be used by the view layer to display it according to type. This currently works in conjunction with
# the reports_shared _report_cell partials.
#
# = Change Log
# [2010-10-06 -- CH]
# Created Class

class ReportDataCell
  DECIMAL   = "decimal"
  CURRENCY  = "currency"
  INTEGER   = "integer"
  STRING    = "string"

  attr_accessor :value, :data_type

  def initialize(value, data_type)
    self.value = value
    set_data_type(data_type)
  end

  private

  def set_data_type(data_type)
    self.data_type =
      if data_type.nil?
        STRING
      else
        data_type
      end
  end
end
