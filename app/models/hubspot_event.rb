class HubspotEvent < ApplicationRecord
  has_many :marketing_event_triggers
  has_many :related, through: :marketing_event_triggers
end
