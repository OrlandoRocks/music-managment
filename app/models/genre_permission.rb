class GenrePermission < ApplicationRecord
  belongs_to :store
  belongs_to :country
  belongs_to :album_genre_whitelist
end
