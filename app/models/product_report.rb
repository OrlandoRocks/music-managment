# = Description
# The Product report encapsulates the logic and data retrieval necessary to produce reports for the
# reporting dashboard. Returns a collection of purchase data with acompanying date and calculated data
#
#
# = Usage
# ProductReport.new({
#                   :report_type => [ProductReport::GROUPED,ProductReport::CSV,ProductReport::UNGROUPED],
#                   :resolution => [ProductReport::DAILY,ProductReport::WEEKLY,ProductReport::MONTHLY],
#                   :range => See Purchase Report Base for range arrays,
#                   :grouped_by => [ProductReport::GROUPED_BY_PRODUCT, ProductReport::GROUPED_BY_RESOLUTION],
#                   :resolution_headers => Optional Array of Headers to Force data set into
#                  })
#
# = Change Log
# [2010-02-02 -- CH]
# Created

class ProductReport < ReportBase
  attr_accessor :product_id, :products, :country_website_id

  def initialize(options)
    self.products = Product.all
    self.product_id = options[:product_id]
    self.country_website_id = options[:country_website_id]
    if !product_id
      self.fields = [{ title: "Total Purchases", field_name: :total_purchased, data_type: "integer" }]
    else
      self.fields = [
        { title: "Total Purchases", field_name: :total_purchased, data_type: "integer" },
        { title: "Total Spent", field_name: :total_cost, data_type: "currency" },
        { title: "Total Discounts", field_name: :total_discounts, data_type: "currency" },
        { title: "Average Cost", field_name: :average_cost, data_type: "currency", show_total: false },
        { title: "Average Discount", field_name: :average_discount, data_type: "currency", show_total: false },
        { title: "Average Paid", field_name: :average_paid, data_type: "currency", show_total: false }
      ]
    end
    super
  end

  def stored_product_name(id)
    stored_product_for(id)&.name
  end

  def stored_product_for(id)
    products.find { |p| p.id == id.to_i }
  end

  protected

  def build_condition_array
    make_date_conditions
    if product_id.nil?
      ["paid_at >= ? and paid_at <= ? and country_website_id = ? and products.id not in (?)", start_date, end_date, country_website_id, preorder_product_ids]
    else
      ["paid_at >= ? and paid_at <= ? and products.id = ? and country_website_id = ? and products.id not in (?)", start_date, end_date, product_id, country_website_id, preorder_product_ids]
    end
  end

  def retrieve_data
    Product.select(select_string).joins(join_string).where(build_condition_array).order(order_string).group(group_by_string)
  end

  def select_string
    %Q(products.id,
    products.name,
    count(*) as total_purchased,
    TRUNCATE(sum(cost_cents)/100,2) as total_cost,
    TRUNCATE(sum(discount_cents)/100,2) as total_discounts,
    TRUNCATE((sum(cost_cents) - sum(discount_cents))/100, 2) as cost_after_discount,
    TRUNCATE(AVG(cost_cents)/100,2) as average_cost,
    TRUNCATE(AVG(discount_cents)/100,2) as average_discount,
    TRUNCATE(AVG(cost_cents - discount_cents)/100,2) as average_paid,
    DATE_FORMAT(pu.paid_at, '#{resolution_date_format}') as `resolution`)
  end

  def join_string
    "INNER JOIN purchases pu FORCE INDEX (`index_purchases_on_paid_at`) on pu.product_id=products.id"
  end

  def order_string
    "products.name, pu.paid_at"
  end

  def group_by_string
    "DATE_FORMAT(pu.paid_at, '#{group_by_date_format}'), products.id"
  end

  def preorder_product_ids
    Product
      .where(
        display_name: "preorder",
        status: "Active"
      )
      .pluck(:id)
  end
end
