class Content  # not activerecord
  # Added this method to allow the creation of content in conjunction
  # with the targeted_offer model.
  def self.create_cms_files(page_name, _options = {})
    content = Content.new(page_name.to_s)
    content.create_file_from_tunecore_file unless File.exist?(content.original_filepath)
  end

  # Added this method to remove any outstanding files that are no longer needed in conjunction
  # with targeted_offers
  def self.delete_cms_files(page_name, _options = {})
    content = Content.new(page_name.to_s)
    content.remove_cms_file
  end

  def initialize(action_name, _controller_name = "index")
    @incoming_file = action_name
    action_arr = action_name.split("__")
    action_arr.uniq!

    if action_arr[0] == "preview" && action_arr.length == 3
      @preview = true
      @partial_name = action_arr[1]
    elsif action_arr.length == 2
      @preview = false
      @partial_name = action_arr[0]
    else
      @preview = false
      @partial_name = false
    end
  end

  def preview?
    if @preview
      true
    else
      false
    end
  end

  attr_reader :incoming_file

  attr_reader :action_name

  def preview_name(name = @partial_name)
    "preview__#{name}__tunecore_neo"
  end

  def partial_name(name = @partial_name)
    "#{name}__tunecore_neo"
  end

  def partial_or_homepage(name = @partial_name)
    if name.include?("homepage")
      "index"
    else
      name
    end
  end

  def domain
    case Rails.env
    when "development" then "web.tunecore.local"
    when "staging"     then "qa1.tunecore.com"
    else "www.tunecore.com"
    end
  end

  def preview_filename(name = @partial_name)
    "_preview__#{name}__tunecore_neo.erb"
  end

  def original_filename(name = @partial_name)
    "_#{name}__tunecore_neo.erb"
  end

  def original_filepath(original_file = original_filename)
    "#{CONTENT_DIR_REL_PATH}#{original_file}"
  end

  def default_path
    "#{CONTENT_DIR_REL_PATH}_#{@partial_name}__tunecore_neo.erb"
  end

  def incoming_path
    "#{CONTENT_DIR_REL_PATH}_#{incoming_file}.erb"
  end

  def preview_filepath(preview_file = preview_filename)
    "#{CONTENT_DIR_REL_PATH}#{preview_file}"
  end

  def preview(name = preview_name)
    "#{CONTENT_DIR_VIEW_PATH}#{name}"
  end

  def original(name = partial_name)
    "#{CONTENT_DIR_VIEW_PATH}#{name}"
  end

  def preview_file_exists?
    File.file?(preview_filepath)
  end

  def create_file_from_tunecore_file(path = incoming_path)
    begin
      if File.file?(default_path)
        FileUtils.cp(default_path, path)
        CMS_LOG.debug("Copied new file from tunecore_neo default to #{path}")
      else
        File.open(path, "w") { |f| f.puts("Blank file, please replace") }
        CMS_LOG.debug("Created new blank file at #{path}")
      end

      true
    rescue
      CMS_LOG.debug("Exception: #{err}")
      false
    end
  end

  def remove_cms_file
    FileUtils.rm(incoming_path) if File.file?(incoming_path)
  end
end
