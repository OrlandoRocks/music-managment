# frozen_string_literal: true

class CableAuthToken < ApplicationRecord
  CABLE_TOKEN_VALIDITY = 12.hours

  belongs_to :person

  def expired?
    expires_at < DateTime.now
  end
end
