class PreorderPurchase < ApplicationRecord
  STORE_NAME_TO_ENABLE_FLAG = { "iTunesWW" => :itunes_enabled, "Google" => :google_enabled }

  belongs_to :album
  belongs_to :preorder_product
  has_one  :purchase, as: :related
  has_many :salepoint_preorder_data, class_name: "SalepointPreorderData", dependent: :destroy

  scope :paid, -> { where("paid_at IS NOT NULL") }

  before_validation :create_enable_salepoint_preorder_data
  validates :album_id, presence: true
  validates :album_id, uniqueness: true
  validate :type_of_album
  validate :validate_not_paid_for
  before_create :set_preorder_product
  after_save :recalculate_purchase_price

  def enabled_preorder_data_count
    count = 0
    count += 1 if itunes_enabled
    count += 1 if google_enabled
    count
  end

  def valid_preorder_data?
    salepoint_preorder_data.all?(&:valid_preorder_data?)
  end

  def enabled?
    itunes_enabled || google_enabled
  end

  def update_enabled_states(store)
    case store
    when "all"
      update(itunes_enabled: true, google_enabled: true)
    when "itunes"
      update(itunes_enabled: true, google_enabled: false)
    when "google"
      update(google_enabled: true, itunes_enabled: false)
    when "none"
      update(itunes_enabled: false, google_enabled: false)
    else
      raise "Invalid store"
    end
    set_preorder_product
  end

  def preorder_data_errors
    messages = []
    salepoint_preorder_data.each { |spd| messages += spd.preorder_data_errors if spd.is_enabled? }
    messages.uniq.to_sentence if messages.present?
  end

  def product
    preorder_product
  end

  def update_preorder_data(itunes_data, all_data)
    itunes_ww_salepoint = album.itunes_ww_salepoint
    itunes_salepoint_preorder_data = itunes_ww_salepoint.salepoint_preorder_data if itunes_ww_salepoint

    return if paid_at?

    start_date =
      if all_data[:start_date]
        Tunecore::DateTranslator.parse_date(
          all_data[:start_date],
          "country_date",
          :en
        )
      else
        nil
      end
    salepoint_preorder_data.each { |spd|
      spd.update(start_date: start_date, preview_songs: all_data[:preview_songs])
    }

    return unless itunes_ww_salepoint && itunes_salepoint_preorder_data

    itunes_salepoint_preorder_data.update_grat_tracks(itunes_data[:gratification_tracks])
    itunes_salepoint_preorder_data.set_track_price(itunes_data[:track_price]) if itunes_data[:track_price]
    itunes_salepoint_preorder_data.set_preorder_price_tier(itunes_data[:album_price]) if itunes_data[:album_price]
    itunes_salepoint_preorder_data.set_salepoint_album_tier
  end

  def name
    if itunes_enabled && google_enabled
      "iTunes and Google Preorder"
    elsif itunes_enabled
      "iTunes Preorder"
    elsif google_enabled
      "Google Preorder"
    end
  end

  def paid_post_proc
    update(paid_at: Time.now)
  end

  def admin_update(all, itunes = {})
    salepoint_preorder_data.each { |spd| spd.admin_update(all, itunes) }
  end

  delegate :can_distribute?, to: :album, prefix: true

  private

  def type_of_album
    errors.add(:album, I18n.t("models.product.is_not_a_supported_type")) if album && !album.type_supports_preorder?
  end

  def validate_not_paid_for
    return unless !paid_at_changed? && paid_at?

    errors.add(:base, I18n.t("models.product.cannot_update_a_paid_for_preorder_purchase"))
  end

  def create_enable_salepoint_preorder_data
    stores = []
    if itunes_enabled_changed? && itunes_enabled && salepoint_preorder_data.by_store("iTunesWW").blank?
      stores << Store.find_by(short_name: "iTunesWW")
    end
    if google_enabled_changed? && google_enabled && salepoint_preorder_data.by_store("Google").blank?
      stores << Store.find_by(short_name: "Google")
    end
    stores.each { |store|
      SalepointPreorderData.create!(preorder_purchase: self, salepoint: album.salepoints_by_store(store).first)
    }
  end

  def recalculate_purchase_price
    return unless purchase && purchase.paid_at.blank?

    if !google_enabled && !itunes_enabled
      purchase.destroy
    elsif google_enabled_changed? || itunes_enabled_changed?
      purchase.recalculate
    end
  end

  def set_preorder_product
    self.preorder_product_id = PreorderProduct.where(product_name: name).first.id if enabled?
  end
end
