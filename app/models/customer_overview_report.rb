# = Description
# The CustomerOverviewReport gets the number of new and returning customers based on the resolution
# requested.
#
# = Usage
#  CustomerOverviewReport.new({
#                               :report_type  => ProductReport::GROUPED,
#                               :resolution   => @resolution,
#                               :range        => @range,
#                               :grouped_by   => ProductReport::GROUPED_BY_INTERVAL
#                             })
#
# = Change Log
# [2010-02-06 -- CH]
# Created Model

class CustomerOverviewReport < ReportBase
  def initialize(options)
    self.fields = [
      { field_name: :new_customers, title: "Distribution: New Customers", data_type: "integer" },
      { field_name: :returning_customers, title: "Distribution: Returning Customers", data_type: "integer" },
      { field_name: :free_stores, title: "Distribution: Free Store Adds", data_type: "integer" },
      { field_name: :renewal_customers, title: "Distribution: Renewal Customers", data_type: "integer" },
      { field_name: :pub_customers, title: "Pub Admin: Total Customers", data_type: "integer" }
    ]
    self.report_type = UNGROUPED
    super
  end

  protected

  def retrieve_data
    make_date_conditions
    Invoice.find_by_sql([sql_string, { start_date: start_date, end_date: end_date }])
  end

  def sql_string
    %Q|SELECT
    resolution,
    sum(new_customers) as new_customers,
    sum(returning_customers) as returning_customers,
    sum(renewal_customers) as renewal_customers,
    sum(pub_customers) as pub_customers,
    sum(free_stores) as free_stores
    FROM
        (SELECT
          DATE_FORMAT(i.settled_at,'#{resolution_date_format}') as `resolution`,
          '0' as returning_customers,
           COUNT(DISTINCT p.id) AS new_customers,
           '0' as renewal_customers,
           '0' as pub_customers,
           '0' AS free_stores
           FROM invoices i
           INNER JOIN people p ON p.id=i.person_id
           INNER JOIN purchases pu ON pu.invoice_id = i.id AND pu.product_id NOT IN (1,2,3,33,65,66,67,68,86,88,101,102,149,150)
           LEFT JOIN invoices i2 ON i.person_id=i2.person_id AND i.id<>i2.id AND i.settled_at > i2.settled_at
           WHERE (i.settled_at IS NOT NULL AND i2.settled_at IS NULL AND p.country_website_id = '#{country_website_id}')
           AND i.settled_at >= :start_date AND i.settled_at <= :end_date
           GROUP BY DATE_FORMAT(i.settled_at,'#{group_by_date_format}')
       UNION ALL
    	  SELECT
          DATE_FORMAT(i.settled_at,'#{resolution_date_format}') as `resolution`,
          COUNT(DISTINCT p.id) as returning_customers,
          '0'  AS new_customers,
          '0' as renewal_customers,
           '0' as pub_customers,
           '0' AS free_stores
           FROM invoices i
           INNER JOIN people p ON p.id=i.person_id
           INNER JOIN purchases pu ON pu.invoice_id = i.id AND pu.product_id NOT IN (1,2,3,33,65,66,67,68,86,88,101,102,149,150) AND pu.cost_cents > 0
           LEFT JOIN invoices i2 ON i.person_id=i2.person_id AND i.id<>i2.id AND i.settled_at > i2.settled_at
           WHERE (i.settled_at IS NOT NULL AND i2.settled_at IS NOT NULL AND p.country_website_id = '#{country_website_id}')
           AND i.settled_at >= :start_date AND i.settled_at <= :end_date
           GROUP BY DATE_FORMAT(i.settled_at,'#{group_by_date_format}')
       UNION ALL
          SELECT
           DATE_FORMAT(i.settled_at,'#{resolution_date_format}') as `resolution`,
           '0' AS returning_customers,
           '0' AS new_customers,
           COUNT(DISTINCT i.person_id) as renewal_customers,
           '0' as pub_customers,
           '0' AS free_stores
           FROM invoices i
           INNER JOIN people p ON p.id=i.person_id
           INNER JOIN purchases pu ON pu.invoice_id = i.id AND pu.product_id IN (1,2,3,33,66,67,68,86,101,102)
           WHERE (i.settled_at IS NOT NULL AND p.country_website_id = '#{country_website_id}')
           AND i.settled_at >= :start_date AND i.settled_at <= :end_date
           GROUP BY DATE_FORMAT(i.settled_at,'#{group_by_date_format}')
       UNION ALL
          SELECT
           DATE_FORMAT(i.settled_at,'#{resolution_date_format}') as `resolution`,
           '0' AS returning_customers,
           '0' AS new_customers,
           '0' AS renewal_customers,
           COUNT(DISTINCT i.person_id) as pub_customers,
           '0' AS free_stores
           FROM invoices i
           INNER JOIN people p ON p.id=i.person_id
           INNER JOIN purchases pu ON pu.invoice_id = i.id AND pu.product_id IN (65,88)
           WHERE (i.settled_at IS NOT NULL AND p.country_website_id = '#{country_website_id}')
           AND i.settled_at >= :start_date AND i.settled_at <= :end_date
           GROUP BY DATE_FORMAT(i.settled_at,'#{group_by_date_format}')
        UNION ALL
            SELECT
            DATE_FORMAT(i.settled_at,'#{resolution_date_format}') as `resolution`,
            '0' AS returning_customers,
            '0' AS new_customers,
            '0' AS renewal_customers,
            '0' AS pub_customers,
            COUNT(DISTINCT i.person_id) AS free_stores
            FROM invoices i
            INNER JOIN people p ON p.id=i.person_id
            INNER JOIN purchases pu ON pu.invoice_id = i.id AND pu.product_id IN (9,85) AND pu.cost_cents = 0
            WHERE (i.settled_at IS NOT NULL AND p.country_website_id = '#{country_website_id}')
            AND i.settled_at >= :start_date AND i.settled_at <= :end_date
            GROUP BY DATE_FORMAT(i.settled_at,'#{group_by_date_format}')
            ) customers
           group by `resolution`|
  end
end
