class PayoutProvider < ApplicationRecord
  ONBOARDING  = "onboarding".freeze
  PENDING     = "pending".freeze
  APPROVED    = "approved".freeze
  DECLINED    = "declined".freeze

  ACTIVE      = "active".freeze
  DISABLED    = "disabled".freeze

  PAYONEER = "payoneer".freeze

  ACH_REGION_KEY    = "payoneer_ach".freeze
  PAYPAL_REGION_KEY = "payoneer_paypal".freeze
  CHECK_REGION_KEY  = "payoneer_check".freeze
  CC_REGION_KEY     = "payoneer_cc".freeze

  PROVIDER_STATUSES = [ONBOARDING, PENDING, APPROVED, DECLINED].freeze
  TUNECORE_STATUSES = [ACTIVE, DISABLED].freeze

  USD_CURRENCY = "USD".freeze
  NON_USD_CURRENCY = "NUD".freeze
  WITHDRAWAL_CURRENCIES = [USD_CURRENCY, NON_USD_CURRENCY].freeze

  belongs_to :person
  belongs_to :payout_provider_config
  has_one :corporate_entity, through: :payout_provider_config

  has_one :current_withdrawal_type, -> { where enabled: true }, class_name: "PayoutWithdrawalType"
  has_one :payout_program, through: :current_withdrawal_type

  has_many :payout_withdrawal_types
  has_many :payout_transfers

  validates :provider_status, inclusion: { in: PROVIDER_STATUSES }
  validates :active_provider, inclusion: { in: [true, false] }
  validate :max_one_active_payout_provider

  before_create :generate_client_payee_id

  scope :active, -> do
    where(
      active_provider: true,
      tunecore_status: ACTIVE
    )
  end

  scope :not_active, -> do
    where.not(active_provider: true).or(
      where.not(tunecore_status: ACTIVE)
    )
  end

  scope :active_by, ->(provider_identifier:, name: PAYONEER) do
    active.where(
      provider_identifier: provider_identifier,
      name: name
    )
  end

  scope :active_providers_for_people_by, ->(**keyword_args) do
    active_by(**keyword_args).joins(:person).group("people.id")
  end

  scope :all_except, ->(id) { where.not(id: id) }

  def self.count_of_active_providers_for_people_by(**keyword_args)
    active_providers_for_people_by(**keyword_args).length
  end

  def can_use_payoneer?
    approved? && active?
  end

  # Creates boolean methods for `provider_status`, such as `approved?`
  PROVIDER_STATUSES.each do |ps|
    define_method "#{ps}?".to_sym do
      provider_status == ps
    end
  end

  # Creates `active?` and `disabled?` methods, which delegate to `tunecore_status`
  TUNECORE_STATUSES.each do |ts|
    define_method "#{ts}?".to_sym do
      tunecore_status == ts
    end
  end

  def other_linked_tunecore_accounts
    return [] if provider_identifier.blank?

    @other_linked_tunecore_accounts ||= self.class.where(
      name: name,
      provider_identifier: provider_identifier
    ).where.not(id: id)
  end

  def has_made_successful_transfer?
    payout_transfers.exists?(tunecore_status: PayoutTransfer::APPROVED)
  end

  def disassociate_account
    update(provider_status: DECLINED, active_provider: false)
  end

  def needs_resubmission?
    [ONBOARDING].include?(provider_status)
  end

  private

  def max_one_active_payout_provider
    return unless active_provider_changed?(to: true)
    return if PayoutProvider.all_except(id).where(person_id: person_id, active_provider: true).none?

    errors.add(:base, "Another active provider already exists")
  end

  def generate_client_payee_id
    self.client_payee_id = SecureRandom.uuid
  end
end
