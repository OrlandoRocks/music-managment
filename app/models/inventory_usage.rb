#  = Description
#  InventoryUsage ties models throughout the system back to a customer's purchased inventory. If a user has purchased
#  the distribution of an Album the reference to the distributed album will be stored in this model as a related_type with
#  related_id.
#
#  = Usage
#  To use inventory, you should call Inventory.use(person, item_to_use). The Inventory model will determine the correct usage for the passed in item.
#
#  = Change Log
#  * 2009-12-16 -- CH -- Created model

class InventoryUsage < ApplicationRecord
  include Tunecore::AdminReports::DistributionReportDataFields
  belongs_to :inventory
  belongs_to :related, polymorphic: true
  belongs_to  :salepoint,
              -> { where(inventory_usages: { related_type: "Salepoint" }) },
              foreign_key: "related_id",
              inverse_of: "inventory_usages"
end
