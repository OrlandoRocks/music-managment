# = Description
#
# ProductItemRule defines the pricing logic for each ProductItem. As of 2009/12/21, there are two types of rules, Entitlement and Inventory
# Entitlement provides the logic to give a customer access to a specific part of the system, Inventory provides the pricing and inventory usage logic
# for product customers can consume.
#
# = Usage (as of 2009/12/21)
# <b>Entitlement Rule</b>
# * entitlement_rights_group_id - refers to the rights the user gets when they purchase the product.
#
# <b>Inventory Rule</b>
# * rule - The rule setting tells the system how to process the purchase and establish a price, the following rules apply:
#   1. price_for_each - simple multiplication rule (price * number_of_items)
#   2. total_included - determines how many of the defined inventory item are included in the product (i.e. 6 songs, 10 Salepoints)
#   3. price_above_included (sub to total_included) - sets the price for any item over the initially included inventory item
#   4. price_for_each_above_included (sub to total_included) - sets an individual price for any inventory item over the total_included. Only price_above_included or price_for_each_about_included should be used at the same time.
#   5. range - sets a pricing rule for a range of values - (i.e. 36 Minute Video vs 120 Minute Video)
#   6. true_false - If a condition is met, one price is used instead of another
#
# <b>Pricing_only Rule</b>
# Only used to calculate a price.  Does not add an inventory or entitlement record to support the purchase.
#
# * inventory_type - refers to the model name being purchased - This applies directly to the Inventory and InventoryUsage models
# * quantity - what initial quantity should be applied to the users account. 0 translates to unlimited in the Inventory table when the total_included rule is used
# * model_to_check - the model name to apply the method_to_check against.
# * method_to_check - the method called against the model_to_check.
# * total_included - the number of items included in the price - used only when the rule is set to 'total_included'
# * true_false - boolean value to check.
# * minimum - bottom value of range rule.
# * maximum - top value of a range rule.
#
#
# Rules can be chained together by using the parent_id.  This is set in the BaseItemOption model.
#
#
# = Change Log
# [2009-12-21 -- CH]
# Created Model

class ProductItemRule < ApplicationRecord
  #################
  # Associations #
  #################
  belongs_to  :product_item
  has_many    :sub_rules, -> { where is_active: true }, class_name: "ProductItemRule", foreign_key: "parent_id"

  ############
  # Filters #
  ############

  ################
  # Validations #
  ################
  validates :product_item, presence: true

  #################
  # Named Scopes #
  #################
  scope :active, -> { where(is_active: true) }

  # white list for table stored methods and models...just in case our db values are changed erroneously
  MODEL_WHITE_LIST = ["Album", "Single", "Ringtone", "self", "Self", "Entitlement", "MusicVideo", "FeatureFilm", "Salepoint", "Composer", "SoundoutProduct", "PreorderPurchase"]
  METHOD_WHITE_LIST = ["songs.count", "salepoints.count", "video_length_in_minutes", "fedex?", "salepoints.total_ready_to_go", "is_hd_feature?", "booklet.count", "store.is_free?", "store.on_sale?", "store.on_flash_sale?", "enabled_preorder_data_count"]

  attr_accessor :model_to_price

  def self.top_level_rules(product_item)
    ProductItemRule
      .active
      .where(product_item_id: product_item.id)
      .where.not(rule: %w[price_for_each_above_included price_above_included])
  end

  # rubocop:disable Style/ClassEqualityComparison
  def calculate_price(model, _product)
    self.model_to_price = model
    if model_to_price.class.name == inventory_type
      # this is the top-level model, return the price
      price
    else
      case rule
      when "price_for_each"
        calculate_price_for_each
      when "total_included"
        calculate_total_included
      when "range"
        calculate_range
      when "true_false"
        calculate_true_false
      else
        0.00
      end
    end
  end
  # rubocop:enable Style/ClassEqualityComparison

  # Once a product is purchased,
  #
  # options
  # * :person              => Person these rules will apply to
  # * :product             => Product the rules come from
  # * :purchase            => Purchase used to buy the product
  # * :renewal(optional)   => Renewal to apply to all available rules
  # * :do_not_renew(optional) => Do not apply any renewals to any rules
  def self.process_product_rules!(options)
    product   = options[:product]
    person    = options[:person]
    purchase  = options[:purchase]
    renewal   = options[:renewal] || nil

    rules_to_process = product.product_item_rules.includes(:product_item).where.not(rule_type: "price_only")
    parent_rules = rules_to_process.select { |r| r.parent_id.nil? || r.parent_id.zero? }

    parent_rules.each do |parent_rule|
      parent_item = parent_rule.process!({ person: person, product: product, purchase: purchase, title: parent_rule.product_item.name })
      parent_rule.process_renewal!(parent_item, { renewal: renewal, person: person, purchase: purchase })

      child_rules = rules_to_process.select { |r| r.parent_id == parent_rule.id }
      next if child_rules.blank?

      child_rules.each do |rule|
        item = rule.process!({ person: person, product: product, purchase: purchase, parent_id: parent_item.id })
        rule.process_renewal!(item, { renewal: renewal, person: person, purchase: purchase })
      end
    end
  end

  def process!(options)
    case rule_type
    when "inventory"
      add_inventory(options)
    when "entitlement"
      add_entitlement(options)
    end
  end

  #
  #  item        => Item to be added to the renewal
  #
  #  options
  #  :renewal    =>  if this entitlement should be attached to a particular renewal, pass in the renewal,
  #                  otherwise, pass in the options listed below to create a new renewal.
  #  :person     => Person this entitlement / renewal is tied to
  #  :purchase   => Original purchase responsible for this renewal
  def process_renewal!(item, options = {})
    return unless item && item.instance_of?(Entitlement)

    if options[:renewal].nil?
      Renewal.create(
        purchase: options[:purchase],
        renewed_items: item,
        person: options[:person],
        item_to_renew_id: product_item_id,
        item_to_renew_type: "ProductItem"
      )
    else
      options[:renewal].add_item(item)
    end
  end

  protected

  #
  #  options
  #  * :person             => customer the inventory should be applied to
  #  * :purchase           => original purchase that added the inventory
  #  * :parent(optional)   => parent inventory item, extrapolated from the product_item_rule parent and children
  #
  def add_inventory(options)
    Inventory.create(
      person: options[:person],
      product_item_id: product_item_id,
      purchase: options[:purchase],
      title: options[:title] || inventory_type,
      quantity: quantity,
      unlimited: unlimited,
      parent_id: options[:parent_id],
      inventory_type: inventory_type
    )
  end

  def add_entitlement(_options)
    Entitlement.create(entitlement_rights_group_id: entitlement_rights_group_id)
  end

  def calculate_price_for_each
    (method_to_check_result * price).to_f
  end

  # includes the calculation of price_above_included and price_for_each_above_included
  def calculate_total_included
    method_result = method_to_check_result
    if quantity >= method_result
      price
    else
      return_price = price
      sub_rules = self.sub_rules.where(parent_id: id)
      sub_rules.each do |sub_rule|
        return_price +=
          if sub_rule.rule_type == "price_above_included"
            sub_rule.price
          else
            (sub_rule.price * (method_result - quantity))
          end
      end
      return_price
    end
  end

  def calculate_range
    method_result = method_to_check_result
    if method_result >= minimum && method_result <= maximum
      price
    else
      0.00
    end
  end

  def calculate_true_false
    method_result = method_to_check_result
    if (method_result != nil or method_result != 0) && method_result == true_false
      price
    else
      0.00
    end
  end

  def method_to_check_result
    if MODEL_WHITE_LIST.include?(model_to_check) && METHOD_WHITE_LIST.include?(method_to_check)
      methods = method_to_check.split(".") # this handle stored syntax like salepoints.count
      if methods.length == 2
        model_to_price.send(methods.first).send(methods.last)
      else
        model_to_price.send(methods.first)
      end
    else
      0
    end
  end
end
