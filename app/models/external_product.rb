class ExternalProduct < ActiveRecord::Base
  has_many   :external_purchases
  belongs_to :person
  belongs_to :external_service

  validates :person, presence: true
  validates :external_service, presence: true
  validates :name, presence: true
  validates :status, presence: true

  scope :active, -> { where(status: "active") }
  scope :inactive, -> { where(status: "inactive") }

  enum status: { inactive: 0, active: 1 }
end
