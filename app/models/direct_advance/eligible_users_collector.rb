class DirectAdvance::EligibleUsersCollector
  def self.collect(end_date, test_run = false)
    new(end_date, test_run).tap(&:collect)
  end

  attr_accessor :end_date, :test_run, :rerun, :last_person_id, :us_eligible_users, :non_us_eligible_users

  def initialize(end_date, options = {})
    @end_date                = end_date
    @test_run                = options[:test_run]
    @rerun                   = options[:eligibility_rerun]
    @last_person_id          = options[:last_operated_person]
    @us_eligible_users       = []
    @non_us_eligible_users   = []
  end

  def collect
    starting_users.each do |user|
      ActiveRecord::Base.connection_pool.with_connection do
        user_id, country    = user
        user_earning_report = user_earnings(user_id)
        check_eligibility(user_id, country, user_earning_report)
      end
    end
  end

  def eligible_users
    us_eligible_users + non_us_eligible_users
  end

  def starting_users
    ActiveRecord::Base.connection.execute(initial_users_query).entries
    # TODO next person to touch this, please refactor to active record
  end

  def user_earnings(user_id)
    total = total_prev_earnings(user_id)
    intakes = monthly_earnings(user_id)
    create_array_for_calculator(total, intakes, user_id)
  end

  def check_eligibility(user_id, country, user_earning_report)
    return unless user_earning_report

    calc = DirectAdvance::EarningsReportCalculator.calculate(user_earning_report, end_date)
    add_to_eligible_users(user_id, country) if calc.user_is_eligible
  end

  def add_to_eligible_users(user_id, country)
    collection = ((country == "US") ? us_eligible_users : non_us_eligible_users)
    collection << user_id
  end

  # This query returns all users that have had a distribution with TuneCore for
  # at least 14 months and are US website users (although their country location may not
  # be US). We then further inspect these users to see if they meet
  # the other minimum requirements in terms of their earnings amounts and timelines.
  def initial_users_query
    test_run ? test_query : prod_query
    # TODO next person to touch this, please refactor to active record
  end

  def prod_query
    <<~SQL
      select person_id, country from
        (
          select a.person_id, pe.country, min(iu.created_at) as first_distro_date from renewal_history rh
          inner join purchases p on p.id = rh.purchase_id
          inner join renewals r on r.id = rh.renewal_id
          inner join renewal_items ri on r.id = ri.renewal_id
          left join albums a on ri.related_id = a.id
          inner join people pe on pe.id = a.person_id
          inner join inventory_usages iu on iu.related_id = a.id and iu.related_type = 'Album'
          where rh.purchase_id is not Null
          and pe.status != 'Locked'
          and (pe.lock_reason is null or pe.lock_reason != 'No Rights')
          and pe.country_website_id = 1
          and date(rh.expires_at) >= (date_sub(curdate(),Interval 45 Day))
          and date(rh.created_at) <= curdate()
          and (r.takedown_at is Null or r.takedown_at  >= curdate())
          and a.finalized_at is not null
          #{rerun_statement}
          group by a.person_id
        ) as active_users
      where first_distro_date <= date_sub(DATE('#{end_date}'),interval 12 MONTH)
    SQL
    # TODO next person to touch this, please refactor to active record
  end

  def test_query
    latest_run_date = find_latest_run_date
    <<~SQL
      select person_id, country
      from direct_advance_eligibility_records
      join people on people.id = direct_advance_eligibility_records.person_id
      where report_run_on = DATE('#{latest_run_date}') and direct_advance_eligibility_records.eligible = 1
      limit 25
    SQL
    # TODO next person to touch this, please refactor to active record
  end

  def find_latest_run_date
    latest_run_date_obj = DirectAdvance::EligibilityRecord
                          .select(:report_run_on)
                          .where("report_run_on <= ?", end_date)
                          .order(report_run_on: :desc)
                          .first
    latest_run_date_obj.report_run_on
  end

  def rerun_statement
    return "" unless rerun

    "and a.person_id > #{last_person_id}"
  end

  # These are the follow-up queries we run on the users returned by the initial_users_query above.
  # It collects earnings sums for various periods to be analyzed by the DirectAdvance::EarningsReportCalculator.
  # Twelve of the periods are monthly intervals and the last period is the sum of all earnings accrued
  # more than 12 months prior to the end_date passed in.
  def monthly_earnings(person_id)
    PersonIntake
      .where(monthly_where_clause, person_id)
      .group(monthly_group_clause)
      .order(created_at: :desc)
      .select(monthly_select_clause)
  end

  def monthly_where_clause
    first_of_month_one_year_ago = "str_to_date(concat('1-',month(date_sub(CURDATE(), interval 12 month)), '-', year(date_sub(CURDATE(), interval 12 month))), '%d-%m-%Y')"
    first_of_this_month         = "str_to_date(concat('1-',month(CURDATE()), '-', year(CURDATE())), '%d-%m-%Y')"
    "created_at >= #{first_of_month_one_year_ago} and created_at < #{first_of_this_month} and person_id = ?"
  end

  def monthly_group_clause
    "person_id, month(created_at)"
  end

  def monthly_select_clause
    "sum(amount) as amount, month(created_at) as month, year(created_at) as year"
  end

  def total_prev_earnings(person_id)
    PersonIntake
      .where(total_where_clause, person_id)
      .sum(:amount)
  end

  def total_where_clause
    first_of_month_one_year_ago = "str_to_date(concat('1-',month(date_sub(CURDATE(), interval 12 month)), '-', year(date_sub(CURDATE(), interval 12 month))), '%d-%m-%Y')"
    "date(created_at) < #{first_of_month_one_year_ago} and person_id = ?"
  end

  # This method is necessary because this used to work differently. It used to select 12 different periods,
  # based on the current date, each one month back (e.g. 2/11-3/11, 1/11-2/10, 12/11-1/10, 11/11-12/10, etc)
  # for "month periods" with no earnings it selected nil
  # since we are now only selecting months with earnings (by grouping by actual months, from the first of the month to the last of the month),
  # we need to insert 0 for no-earnings-months because the rest of the code expects it
  #
  # probably wise to do a greater rewrite of this entire process at some point, but to fix the issue of the moving target--
  # different monthly periods depending on the date that the report is run-- this will suffice
  def create_array_for_calculator(total, intakes, user_id)
    base_date = Date.current - 1.month
    result = [user_id]
    intakes_index = 0
    12.times do
      intake = intakes[intakes_index]
      # the safety operators are for when there are no intakes or when the loop has gone through all the intakes
      if intake&.month == base_date.month && intake&.year == base_date.year
        result << intake.amount
        intakes_index += 1
      else
        result << 0.0
      end
      base_date -= 1.month
    end
    result << total
    result
  end
end
