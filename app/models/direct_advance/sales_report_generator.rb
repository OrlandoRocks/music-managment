class DirectAdvance::SalesReportGenerator
  LOG_PREFIX = "DirectAdvance SalesReportGenerator Log Message:".freeze

  def self.generate(user_id)
    new(user_id).tap(&:generate)
  end

  attr_accessor :user_id, :file_name, :headers, :data

  def initialize(user_id)
    @user_id       = user_id
    @file_name     = "#{user_id}.csv"
  end

  def generate
    start_time = Time.current
    connection = ReadOnlyReplica::SalesRecordSummary.connection
    sales_records = connection.exec_query(sales_reports_for_user)
    you_tube_records =
      if FeatureFlipper.show_feature?(:add_youtube_to_lyric_report)
        connection.exec_query(you_tube_reports_for_user).to_a
      else
        []
      end
    time_to_complete = (Time.current - start_time).seconds
    Rails.logger.info "#{LOG_PREFIX} #{time_to_complete} seconds to complete query"

    build_report(sales_records, you_tube_records)
  end

  private

  def build_report(sales_results, you_tube_results)
    Rails.logger.info "#{LOG_PREFIX} Building sales report for #{user_id}"

    @headers = %w[
      sales_period
      posted_date
      store
      country
      artists
      label
      album_type
      album_id
      album_name
      upc
      release_date
      song_id
      song_name
      track_num
      tunecore_isrc
      optional_isrc
      distribution_type
      units_sold
      rev_per_unit
      amount_earned
    ]

    @data = add_sales_records(sales_results) + add_you_tube_records(you_tube_results)
  end

  def add_sales_records(sales_records)
    result = []
    sales_records.each do |r|
      result << [
        r["period_sort"],
        r["posted_date"],
        r["store_name"],
        r["country"],
        r["artist_name"],
        r["label_name"],
        r["release_type"],
        r["album_id"],
        r["release_title"],
        r["upc"],
        r["release_date"],
        r["song_id"],
        r["song_title"],
        r["sequence"],
        r["isrc"],
        r["optional_isrc"],
        r["distribution_type"],
        r["quantity"],
        r["revenue_per_unit"].to_f,
        r["amount"].to_f,
      ]
    end
    result
  end

  def add_you_tube_records(you_tube_records)
    result = []
    you_tube_records.each do |r|
      result << [
        r["period_sort"],
        r["posted_date"],
        r["store_name"],
        r["country"],
        r["artist_name"],
        r["label_name"],
        r["release_type"],
        r["album_id"],
        r["release_title"],
        r["upc"],
        r["release_date"],
        r["song_id"],
        r["song_title"],
        r["sequence"],
        r["isrc"],
        r["optional_isrc"],
        r["distribution_type"],
        r["quantity"],
        r["revenue_per_unit"].to_f,
        r["amount"].to_f,
      ]
    end
    result
  end

  def you_tube_reports_for_user
    "
      SELECT
        sales_period_start as period_sort,
        date_format(you_tube_royalty_records.created_at, '%Y-%m-%d') as posted_date,
        'Youtube Royalties' as store_name,
        'US' as country,
        artist_name,
        label_name,
        a.album_type as release_type,
        a.id as album_id,
        album_name as release_title,
        case when u.id is not null then u.number else uas.number end as upc,
        a.sale_date as release_date,
        song_id,
        song_name as song_title,
        s.track_num as sequence,
        s.tunecore_isrc as isrc,
        s.optional_isrc as optional_isrc,
        'Streaming' as distribution_type,
        total_views as quantity,
        net_revenue/total_views as revenue_per_unit,
        net_revenue as amount
      FROM you_tube_royalty_records
      LEFT JOIN songs s on s.id = you_tube_royalty_records.song_id
      LEFT JOIN albums a on a.id = s.album_id
      LEFT JOIN upcs u on u.upcable_id = a.id and u.upcable_type = 'Album' and u.inactive = 0 and u.tunecore_upc = 1
      LEFT JOIN upcs uas on uas.upcable_id = a.id
        and uas.upcable_type = 'Album'
        and uas.inactive = 0
        and uas.tunecore_upc = 1
      WHERE you_tube_royalty_records.person_id = (#{user_id})
      AND you_tube_royalty_records.created_at >= DATE_SUB(curdate(), INTERVAL 12 MONTH)
    "
  end

  def sales_reports_for_user
    sales_report_query_for_user("sales_record_summaries")
  end

  def sales_report_query_for_user(table_alias)
    <<-SQL.strip_heredoc
      SELECT
          srm.period_sort,
          date_format(srm.created_at, '%Y-%m-%d') AS posted_date,
          ssdg.name as store_name,
          srm.country,
          case when a.is_various = 1 then 'Various Artists' when artists.id is not null then artists.name when aa.id is not null then aa.name
            when aas.id is not null then aas.name end as artist_name,
          case when l.id is not null then l.name when label_albums.id is not null then label_albums.name when label_videos.id is not null then label_videos.name end as label_name,
          sr.related_type as release_type,
          case when a.id is not null then a.id else album_sales.id end as album_id,
          case when a.id is not null then a.name when album_sales.id is not null then album_sales.name when v.id is not null then v.name end as release_title,
          case when u.id is not null then u.number else uas.number end as upc,
          a.sale_date as release_date,
          s.id as song_id,
          s.name as song_title,
          s.track_num as sequence,
          s.tunecore_isrc as isrc,
          s.optional_isrc as optional_isrc,
          case#{' '}
            when sr.downloads_sold > 0 and sr.streams_sold > 0
            then 'Both Downloads and Streams'
            when sr.downloads_sold > 0 and sr.streams_sold = 0
            then 'Download'
            when sr.downloads_sold = 0 and sr.streams_sold > 0
            then 'Streaming'
            end as distribution_type,
          sr.downloads_sold + sr.streams_sold as quantity,
          ( sr.download_amount + sr.stream_amount ) / (sr.downloads_sold + sr.streams_sold) as revenue_per_unit,
          sr.download_amount + sr.stream_amount as amount
      FROM #{table_alias} sr
      inner join sales_record_masters srm FORCE INDEX FOR JOIN (id) on srm.id = sr.sales_record_master_id
      inner join sip_stores ss on ss.id = srm.sip_store_id
      inner join sip_stores_display_groups ssdg on ssdg.id = ss.display_group_id
      left join videos v on v.id = sr.related_id and sr.related_type = 'Video'
      left join songs s on s.id = sr.related_id and sr.related_type = 'Song'
      left join albums album_sales on album_sales.id = sr.related_id and sr.related_type = 'Album'
      left join albums a on a.id = s.album_id
      left join upcs u on u.upcable_id = a.id and u.upcable_type = 'Album' and u.inactive = 0 and u.tunecore_upc = 1
      left join upcs uas on uas.upcable_id = album_sales.id and uas.upcable_type = 'Album' and uas.inactive = 0 and uas.tunecore_upc = 1
      left join upcs u_optional_upc on u_optional_upc.upcable_id = a.id and u_optional_upc.upcable_type = 'Album' and u_optional_upc.inactive = 0 and u_optional_upc.tunecore_upc = 0
      left join upcs uas_optional_upc on uas_optional_upc.upcable_id = album_sales.id and uas_optional_upc.upcable_type = 'Album' and uas_optional_upc.inactive = 0 and uas_optional_upc.tunecore_upc = 0
      left join labels l on l.id = a.label_id
      left join labels label_albums on label_albums.id = album_sales.label_id
      left join labels label_videos on label_videos.id = v.label_id
      left join creatives c on c.creativeable_id = a.id and c.creativeable_type = 'Album' and c.role = 'primary_artist'
      left join artists on artists.id = c.artist_id
      left join creatives cas on cas.creativeable_id = album_sales.id and cas.creativeable_type = 'Album' and cas.role = 'primary_artist'
      left join artists aas on aas.id = cas.artist_id
      left join creatives cc on cc.creativeable_id = s.id and cc.creativeable_type = 'Song' and cc.role = 'primary_artist'
      left join artists aa on aa.id = cc.artist_id
      WHERE sr.person_id = (#{user_id}) and srm.created_at >= DATE_SUB(curdate(), INTERVAL 12 MONTH)
      group by sr.person_id, sr.sales_record_master_id, sr.related_id, sr.related_type, sr.person_intake_id
    SQL
  end
end
