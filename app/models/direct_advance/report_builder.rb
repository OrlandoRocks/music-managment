class DirectAdvance::ReportBuilder
  def self.build(person, report_run_date)
    new(person, report_run_date).build
  end

  attr_accessor :person, :report_run_date, :eligibility_record, :earnings_period_records, :report

  def initialize(person, report_run_date)
    @person          = person
    @report_run_date = Date.parse(report_run_date.to_s)
    @report          = report_base
  end

  def build
    add_eligibility_record
    add_earnings_period_records
    report
  end

  private

  def add_earnings_period_records
    earnings_period_records.each do |record|
      report[:earnings] << [record.period, period_start_date(record), record.amount]
    end
  end

  def add_eligibility_record
    report[:eligible] = eligibility_record.try(:eligible) || false
  end

  def report_base
    { person: person, report_run_on: report_run_date, eligibility: nil, earnings: [] }
  end

  def period_start_date(record)
    months_before_report_run_date = numerical_periods.index(period_to_num(record.period))
    report_run_date.at_beginning_of_month.last_month - months_before_report_run_date.months
  end

  def numerical_periods
    @numerical_periods ||= earnings_period_records.map { |r| period_to_num(r.period) }.sort
  end

  def period_to_num(period)
    period.gsub("month_", "").gsub("all_prev_months", "999").to_i
  end

  def earnings_period_records
    DirectAdvance::EarningsPeriodRecord.where(person_id: person.id, report_run_on: report_run_date)
  end

  def eligibility_record
    DirectAdvance::EligibilityRecord.where(person_id: person.id, report_run_on: report_run_date).try(:first)
  end
end
