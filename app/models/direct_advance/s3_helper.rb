class DirectAdvance::S3Helper
  LOG_PREFIX = "DirectAdvance S3Helper Log Message:".freeze

  attr_accessor :s3_bucket, :path

  def initialize(test_run = false)
    @s3_bucket        = "lyric-reports"
    @path             = FeatureFlipper.show_feature?(:lyric_s3_bucket_rename) ? Rails.env : select_bucket_path(test_run)
  end

  def upload_eligible_user_reports(collector, end_date)
    Rails.logger.info "#{LOG_PREFIX} Uploading eligibility reports to S3"
    eligible_user_reports = {
      "eligible_users_ALL" => collector.eligible_users,
      "eligible_US_users" => collector.us_eligible_users,
      "eligible_non_US_users" => collector.non_us_eligible_users
    }

    eligible_user_reports.each do |file_name_prefix, user_ids|
      file_name = "#{file_name_prefix}_#{end_date}.csv"
      write_and_upload(file_name, user_ids)
    end

    Rails.logger.info "#{LOG_PREFIX} Finished uploading eligibility reports to S3"
  end

  def upload_comparison_report(comparison_name, user_segment, user_ids, end_date)
    Rails.logger.info "#{LOG_PREFIX} Uploading comparison reports to S3"
    file_name = "#{comparison_name}_#{user_segment}_#{end_date}.csv"
    write_and_upload(file_name, user_ids)
  end

  def most_recent_eligible_users_report
    report = get_eligible_users_report("all")
    extract_user_ids_from_report(report)
  end

  def previous_report_for_segment_and_date(segment, end_date)
    report = get_eligible_users_report(segment, end_date)
    extract_user_ids_from_report(report)
  end

  def upload_to_s3(file_name, data, headers = nil)
    options = {
      file_name: file_name,
      headers: headers,
      data: data,
      bucket_name: s3_bucket,
      bucket_path: path
    }
    S3StreamingService.write_and_upload(options)
  end

  def get_last_report_and_date(user_segment)
    last_report_and_date(user_segment)
  end

  private

  def write_and_upload(file_name, user_ids)
    options = {
      file_name: file_name,
      data: user_ids,
      bucket_name: s3_bucket,
      bucket_path: path
    }
    S3StreamingService.write_and_upload(options)
  end

  def extract_user_ids_from_report(report)
    if report
      report.read.delete("\n").split(",").map(&:to_i)
    else
      []
    end
  end

  def get_eligible_users_report(user_segment, end_date_for_exclusion = nil)
    file_name_prefix, eligible_users_reports = get_user_reports(user_segment)

    if eligible_users_reports && eligible_users_reports.any?
      select_report(eligible_users_reports, file_name_prefix, end_date_for_exclusion)
    else
      Rails.logger.info "#{LOG_PREFIX} TCDA: Cannot locate eligible users report. Searched in: #{file_path_prefix}"
    end
  end

  def get_user_reports(user_segment)
    file_name_prefix       = get_file_name_prefix(user_segment)
    file_path_prefix       = "#{path}/#{file_name_prefix}"
    bucket                 = S3_CLIENT.buckets[s3_bucket]
    eligible_users_reports = bucket.objects.with_prefix(file_path_prefix)
    [file_name_prefix, eligible_users_reports]
  end

  def get_file_name_prefix(user_segment)
    {
      "all" => "eligible_users_ALL_",
      "US" => "eligible_US_users_",
      "non_US" => "eligible_non_US_users_"
    }[user_segment]
  end

  def select_report(eligible_users_reports, file_name_prefix, end_date_for_exclusion = nil)
    ordered_reports = eligible_users_reports.sort_by { |report| strip_date(report, file_name_prefix) }

    if end_date_for_exclusion.present?
      get_last_report_before_end_date(ordered_reports, file_name_prefix, end_date_for_exclusion)
    else
      ordered_reports.last
    end
  end

  def last_report_and_date(user_segment)
    file_name_prefix, eligible_users_reports = get_user_reports(user_segment)

    ordered_reports = eligible_users_reports.sort_by { |report| strip_date(report, file_name_prefix) }
    latest_report = ordered_reports.last

    latest_user_ids = extract_user_ids_from_report(latest_report)
    latest_date = strip_date(latest_report, file_name_prefix)

    { users: latest_user_ids, date: latest_date }
  end

  def strip_date(report, file_name_prefix)
    report.key.gsub(file_name_prefix, "").gsub(".csv", "").try(:to_date)
  end

  def get_last_report_before_end_date(ordered_reports, file_name_prefix, end_date_for_exclusion)
    cutoff_date = end_date_for_exclusion.to_date
    ordered_reports.partition { |r| strip_date(r, file_name_prefix) < cutoff_date }.first.last
  end

  def select_bucket_path(test_run)
    if Rails.env.production? && !test_run
      "automated_runs"
    else
      "test_runs"
    end
  end
end
