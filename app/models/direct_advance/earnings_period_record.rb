class DirectAdvance::EarningsPeriodRecord < ApplicationRecord
  belongs_to            :person
  validates :person_id, :amount, :period, :report_run_on, presence: true
  validates             :report_run_on, uniqueness: { scope: [:person_id, :period] }
  validate              :report_run_on_date

  def report_run_on_date
    errors.add(:report_run_on, " must be a Date") unless report_run_on.instance_of?(Date)
  end
end
