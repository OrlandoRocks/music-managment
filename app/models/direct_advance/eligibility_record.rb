class DirectAdvance::EligibilityRecord < ApplicationRecord
  belongs_to :person
  validates :person_id, :report_run_on, presence: true
  validates :eligible, inclusion: { in: [true, false] }
  validates               :report_run_on, uniqueness: { scope: :person_id }
  validate                :report_run_on_date

  def report_run_on_date
    errors.add(:report_run_on, " must be a Date") unless report_run_on.instance_of?(Date)
  end
end
