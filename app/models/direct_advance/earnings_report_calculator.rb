class DirectAdvance::EarningsReportCalculator
  LOG_PREFIX = "DirectAdvance EarningsReportCalculator Log Message:".freeze

  MIN_EARNINGS_REQUIREMENT = 870

  def self.calculate(user_earning_report, end_date)
    new(user_earning_report, end_date).tap(&:calculate)
  end

  attr_accessor :report, :end_date, :user_id, :monthly_earnings, :prev_earnings_sum, :user_is_eligible

  def initialize(user_earning_report, end_date)
    @report            = user_earning_report
    @end_date          = end_date
    @user_id           = report[0]
    @monthly_earnings  = report[1..12]
    @prev_earnings_sum = report[13].to_f
  end

  def calculate
    @user_is_eligible = all_checks_pass?
    save_monthly_earnings
    save_previous_earnings
    save_eligibility
  end

  def all_checks_pass?
    no_zero_earnings_months? &&
      min_annual_earnings_met? &&
      avg_monthly_earnings_met?
  end

  def no_zero_earnings_months?
    monthly_earnings.detect { |me| me <= 0 }.blank?
  end

  def min_annual_earnings_met?
    monthly_earnings.sum > MIN_EARNINGS_REQUIREMENT
  end

  def avg_monthly_earnings_met?
    max_avg = (monthly_earnings.sum / monthly_earnings.count) * 1.3
    months_within_max_avg = monthly_earnings.select { |me| me <= max_avg }
    new_adj_avg = (months_within_max_avg.sum / months_within_max_avg.count)
    new_adj_avg >= 92
  end

  def save_monthly_earnings
    monthly_earnings.each_with_index do |earnings, index|
      period = "month_" + (index + 1).to_s
      Rails.logger.info "#{LOG_PREFIX} Saving earnings period record - Period: #{period} UserID: #{user_id}"
      DirectAdvance::EarningsPeriodRecord.create(
        person_id: user_id,
        amount: earnings,
        period: period,
        report_run_on: end_date
      )
    end
  end

  def save_previous_earnings
    Rails.logger.info "#{LOG_PREFIX} Saving previous earnings period record - UserID: #{user_id}"
    DirectAdvance::EarningsPeriodRecord.create(
      person_id: user_id,
      amount: prev_earnings_sum,
      period: "all_prev_months",
      report_run_on: end_date
    )
  end

  def save_eligibility
    Rails.logger.info "#{LOG_PREFIX} Saving eligibility record - UserID: #{user_id}"
    DirectAdvance::EligibilityRecord.create(person_id: user_id, eligible: user_is_eligible, report_run_on: end_date)
  end
end
