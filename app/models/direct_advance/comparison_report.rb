class DirectAdvance::ComparisonReport
  attr_accessor :user_segment,
                :new_user_ids,
                :old_user_ids,
                :end_date,
                :s3_helper,
                :dropped_accounts,
                :added_accounts,
                :test_run

  def self.run_for(user_segment, new_user_ids, end_date, test_run = false)
    new(user_segment, new_user_ids, end_date, test_run).run
  end

  def initialize(user_segment, new_user_ids, end_date, test_run = false)
    @user_segment = user_segment
    @new_user_ids = new_user_ids
    @end_date     = end_date
    @s3_helper    = DirectAdvance::S3Helper.new(test_run)
    @test_run     = test_run
  end

  def run
    get_user_ids_from_last_run
    create_comparison_reports
  end

  private

  def get_user_ids_from_last_run
    @old_user_ids = s3_helper.previous_report_for_segment_and_date(user_segment, end_date)
  end

  def create_comparison_reports
    @dropped_accounts = old_user_ids - new_user_ids
    @added_accounts   = new_user_ids - old_user_ids

    s3_helper.upload_comparison_report("dropped_accounts", user_segment, dropped_accounts, end_date)
    s3_helper.upload_comparison_report("added_accounts", user_segment, added_accounts, end_date)
  end
end
