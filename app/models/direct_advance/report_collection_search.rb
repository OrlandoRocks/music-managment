class DirectAdvance::ReportCollectionSearch
  def self.search(query)
    new(query).tap(&:search)
  end

  attr_accessor :query, :person, :eligible, :latest_report, :historical_reports

  def initialize(query)
    @query = query.to_s
  end

  def search
    run_person_search
    return if person.blank?

    get_reports
    @eligible = check_eligibility
  end

  def search_field
    Regexp.new(EMAIL_REGEXP).match(query).present? ? "user_email" : "user_id"
  end

  private

  def check_eligibility
    return true  if person.override_ineligibility?
    return false if person.nil? || person.suspicious?

    @latest_report.present? && @latest_report[:eligible]
  end

  def run_person_search
    conditions =
      case search_field
      when "user_id"    then person_table[:id].eq(query)
      when "user_email" then person_table[:email].eq(query)
      end

    @person = Person.where(conditions).try(:first)
  end

  def get_reports
    reports =
      report_run_dates_for_person.map do |date|
        DirectAdvance::ReportBuilder.build(person, date)
      end
    @latest_report, *@historical_reports = reports
  end

  def report_run_dates_for_person
    DirectAdvance::EligibilityRecord.where(person_id: person.id).order(report_run_on: :desc).map(&:report_run_on)
  end

  def person_table
    Person.arel_table
  end
end
