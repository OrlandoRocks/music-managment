class DirectAdvance::Manager
  LOG_PREFIX = "DirectAdvance Manager Log Message:".freeze

  DEFAULT_OPTIONS = {
    create_eligibility_reports: true,
    create_sales_reports: true,
    create_comparison_reports: true,
    set_feature_flags: true,
    test_run: false,
    eligibility_rerun: false,
    sales_rerun: false,
    comparison_rerun: false,
    last_operated_person: 0
  }

  attr_accessor :end_date, :s3_helper, :eligible_users, :us_eligible_users, :non_us_eligible_users, :options

  def self.manage(end_date, options = {})
    new(end_date, options).tap(&:manage)
  end

  def initialize(end_date, options = {})
    @end_date  = end_date
    @options   = DEFAULT_OPTIONS.merge(options)
    @s3_helper = DirectAdvance::S3Helper.new(options[:test_run])
    verify_valid_date
  end

  def manage
    create_eligibility_reports  if options[:create_eligibility_reports]
    create_sales_reports        if options[:create_sales_reports]
    create_comparison_reports   if options[:create_comparison_reports]
    set_feature_flags           if options[:set_feature_flags]
  end

  private

  def create_eligibility_reports
    Rails.logger.info "#{LOG_PREFIX} Starting Eligibility reports"
    collector = DirectAdvance::EligibleUsersCollector.collect(end_date, options)
    Rails.logger.info "#{LOG_PREFIX} Finished Eligibility reports"

    @eligible_users        = collector.eligible_users
    @us_eligible_users     = collector.us_eligible_users
    @non_us_eligible_users = collector.non_us_eligible_users
    s3_helper.upload_eligible_user_reports(collector, end_date)
  end

  def create_sales_reports
    Rails.logger.info "#{LOG_PREFIX} Starting Sales reports"

    users_to_process = all_eligible_users - accounts_to_skip

    users_to_process.each_with_index do |user_id, index|
      next if options[:sales_rerun] && user_id < options[:last_operated_person]

      ActiveRecord::Base.connection_pool.with_connection do
        Rails.logger.info "#{LOG_PREFIX} Generating TCDA Sales Report ##{index + 1} of ##{users_to_process.count}"

        generator = DirectAdvance::SalesReportGenerator.generate(user_id)

        Rails.logger.info "#{LOG_PREFIX} Uploading TCDA Sales Report ##{index + 1} of ##{users_to_process.count}"

        s3_helper.upload_to_s3(generator.file_name, generator.data, generator.headers)
      end
    end

    Rails.logger.info "#{LOG_PREFIX} Finished Sales reports"
  end

  def create_comparison_reports
    return create_comparison_rerun if options[:comparison_rerun]

    unless us_eligible_users && non_us_eligible_users
      raise "#{LOG_PREFIX} TCDA: Cannot run comparison report without running eligibility report"
    end

    Rails.logger.info "#{LOG_PREFIX} Starting Comparison reports"

    DirectAdvance::ComparisonReport.run_for("US", us_eligible_users, end_date, options[:test_run])
    DirectAdvance::ComparisonReport.run_for("non_US", non_us_eligible_users, end_date, options[:test_run])

    Rails.logger.info "#{LOG_PREFIX} Finished Comparison reports"
  end

  def create_comparison_rerun
    us_eligible_users = s3_helper.get_last_report_and_date("US")
    non_us_eligible_users = s3_helper.get_last_report_and_date("non_US")

    Rails.logger.info "#{LOG_PREFIX} Starting Comparison reports"
    DirectAdvance::ComparisonReport.run_for(
      "US",
      us_eligible_users[:users],
      us_eligible_users[:date],
      options[:test_run]
    )
    DirectAdvance::ComparisonReport.run_for(
      "non_US",
      non_us_eligible_users[:users],
      us_eligible_users[:date],
      options[:test_run]
    )
    Rails.logger.info "#{LOG_PREFIX} Finished Comparison reports"
  end

  def set_feature_flags
    return if options[:test_run]

    Rails.logger.info "#{LOG_PREFIX} Updating FeatureFlag for eligible users"

    eligible_user_ids_string = all_eligible_users.join(", ")
    FeatureFlipper.update_feature("lyric_advance", 0, eligible_user_ids_string)

    Rails.logger.info "#{LOG_PREFIX} Successfully updated FeatureFlag for eligible users"
  end

  def all_eligible_users
    @eligible_users ||= s3_helper.most_recent_eligible_users_report
  end

  def verify_valid_date
    begin
      Date.strptime(end_date.to_s).to_s
    rescue
      raise "#{LOG_PREFIX} TCDA: Must supply valid end date in format: (YYYY-MM-DD)"
    end
  end

  def accounts_to_skip
    ["64516"] # accounts that are too large to properly process
  end
end
