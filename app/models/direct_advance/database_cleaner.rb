class DirectAdvance::DatabaseCleaner
  def self.clean
    DirectAdvance::EarningsPeriodRecord.where("report_run_on < ?", 1.year.ago).delete_all
  end
end
