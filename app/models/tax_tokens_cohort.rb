class TaxTokensCohort < ApplicationRecord
  has_many :tax_tokens, dependent: :destroy

  scope :active, -> { joins(:tax_tokens).where("tax_tokens.is_visible = ?", true).uniq }

  def is_blocking?
    Date.today >= end_date.to_date
  end

  def remove_all_tax_tokens
    tax_tokens.update_all(is_visible: false)
  end
end
