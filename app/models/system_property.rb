# == Schema Information
# Schema version: 404
#
# Table name: system_properties
#
#  id          :integer(11)     not null, primary key
#  name        :string(255)
#  value       :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class SystemProperty < ApplicationRecord
  def self.set(name, value, description = nil)
    sp = SystemProperty.find_or_create_by(name: name.to_s)
    sp.value = value
    sp.description = description unless description.nil?
    sp.save
    sp.value
  end

  def self.get(name)
    record = SystemProperty.find_by(name: name.to_s)
    record.value unless record.nil?
  end
end
