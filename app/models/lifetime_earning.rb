class LifetimeEarning < ApplicationRecord
  belongs_to :person

  def recalculate!
    intake_amount = PersonIntake.where(person_id: person_id).sum(:amount)
    yt_intake_amount = YouTubeRoyaltyIntake.where(person_id: person_id).sum(:amount)

    update(person_intake: intake_amount, youtube_intake: yt_intake_amount)
  end

  def total_intake
    person_intake.to_f + youtube_intake.to_f
  end
end
