# frozen_string_literal: true

class PersonSubscriptionStatus < ApplicationRecord
  belongs_to :person
  has_many :subscription_events
  has_many :subscription_purchases, through: :subscription_events
  has_many :subscription_products, -> { distinct }, through: :subscription_purchases

  validates :person, :subscription_type, presence: true

  ALLOWED_ADMIN_ROLES = ["Subscription Admin"].map(&:freeze).freeze
  FB_TRACKS = "FBTracks"
  YTSR = "YTTracks"

  scope :active, -> do
    where
      .not(effective_date: nil)
      .where("termination_date IS NULL OR termination_date > NOW()")
  end

  scope :with_termination_date, -> do
    where.not(termination_date: nil)
  end

  scope :social, -> { where(subscription_type: "Social") }

  def is_active?
    effective_date.present? && (termination_date.nil? || termination_date > Time.now)
  end

  def expires?
    termination_date.present?
  end

  def canceled?
    canceled_at.present?
  end

  def terminated?
    return false if termination_date.blank?

    DateTime.current > termination_date
  end

  def can_disable_access?
    canceled? && !terminated?
  end

  def next_purchase_event_type
    canceled_at.nil? ? "Renewal" : "Purchase"
  end

  def next_purchase_effective_start_date
    expires? ? [termination_date, DateTime.now].max : effective_date
  end

  def latest_payment_channel
    if subscription_events.present?
      subscription_events.order("created_at DESC").first.subscription_purchase.payment_channel
    else
      "None (Free Pro Subscription)"
    end
  end

  def within_grace_period?
    return false unless expires?

    Date.today.between?(termination_date.to_date, grace_period_ends_on)
  end

  def grace_period_ends_on
    date = expires? ? termination_date.to_date : Date.today
    date + grace_period_length.to_i.days
  end

  def grace_period_ends_today?
    return false unless expires?

    grace_period_ends_on == Date.today
  end

  def days_until_grace_period_ends
    (grace_period_ends_on - Date.today).to_i
  end

  def method_missing(method, *arguments, &block)
    if method.to_s =~ /^grace_period_ends_in_(.*)_days\?$/
      days_until_grace_period_ends == $1.to_i
    else
      super
    end
  end

  def product_type
    event = subscription_events.eager_load(subscription_purchase: :subscription_product).last
    event
      .try(:subscription_purchase)
      .try(:subscription_product)
      .try(:product_type)
  end

  def self.create_subscription_by_email(email, termination_date, type)
    user = Person.find_by(email: email)
    if user.present?
      new_subscription = PersonSubscriptionStatus.new(person: user, effective_date: DateTime.now, termination_date: termination_date, subscription_type: type, grace_period_length: DEFAULT_SUBSCRIPTION_GRACE_PERIOD)
      new_subscription.save
    else
      false
    end
  end

  def cancel
    update(canceled_at: DateTime.now)
  end

  def disable_access(current_user)
    disable_access_error_handling(current_user)
    update(termination_date: DateTime.now, grace_period_length: 0) if errors.blank?
    self
  end

  def disable_access_error_handling(current_user)
    errors.add(:disable_access, "status cannot be updated") unless can_disable_access?
    errors.add(:admin, "has inappropriate role") unless ALLOWED_ADMIN_ROLES.any? { |role| current_user.has_role?(role) }
  end

  def get_termination_date
    expires? ? self[:termination_date].to_date : Date.today + 1.year
  end
end
