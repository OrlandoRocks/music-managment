# This model grabs aggregate data and is only used for display
# purposes.  As such, we encapsulate the underlying ActiveRecord
# objects so that it doesn't allow access to misleading information.
class SalesMonthTotal
  # grab the person_intakes (for their payout_cents) and associated
  # album_intakes (for their units sold etc) for a given person and
  # a given list of reporting_months.
  # return as a list of SalesMonthTotal objects
  def self.all_for(person, reporting_months)
    reporting_months = Array(reporting_months)

    ai_records = ReportingMonth
                 .select(
                   %q|reporting_months.id,
        sum(album_intakes.songs_sold) as songs_sold,
        sum(album_intakes.songs_streamed) as songs_streamed,
        sum(album_intakes.albums_sold) as albums_sold|
                 )
                 .from("reporting_months, album_intakes")
                 .group("reporting_months.id")
                 .where(
                   %q|album_intakes.reporting_month_id = reporting_months.id and album_intakes.person_id = ? and reporting_months.id in (?)|,
                   person.id,
                   reporting_months.collect(&:id)
                 )

    pi_records = ReportingMonth
                 .select(%q|reporting_months.id, sum(person_intakes.usd_payout_cents) as usd_payout_cents|)
                 .from("reporting_months, person_intakes")
                 .group("reporting_months.id")
                 .where(
                   %q|person_intakes.reporting_month_id = reporting_months.id and person_intakes.person_id = ? and reporting_months.id in (?)|,
                   person.id,
                   reporting_months.collect(&:id)
                 )

    ai_records_by_id = ai_records.index_by(&:id)
    pi_records_by_id = pi_records.index_by(&:id)

    reporting_months.reverse.collect do |rmonth|
      SalesMonthTotal.new(person, rmonth, ai_records_by_id[rmonth.id], pi_records_by_id[rmonth.id])
    end
  end

  attr_reader :reporting_month, :person

  def initialize(person, reporting_month, ai_record, pi_record)
    self.person = person
    self.reporting_month = reporting_month
    self.ai_record = ai_record
    self.pi_record = pi_record
  end

  def songs_sold
    ai_record ? ai_record[:songs_sold].to_i : 0
  end

  def songs_streamed
    ai_record ? ai_record[:songs_streamed].to_i : 0
  end

  def albums_sold
    ai_record ? ai_record[:albums_sold].to_i : 0
  end

  def net_earnings_usd
    pi_record ? pi_record[:usd_payout_cents].to_i : 0
  end

  def has_sales?
    return false if ai_record.nil?

    songs_sold != 0 || songs_streamed != 0 || albums_sold != 0
  end

  protected

  attr_writer :reporting_month, :person
  attr_accessor :ai_record, :pi_record
end
