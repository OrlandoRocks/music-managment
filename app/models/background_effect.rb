class BackgroundEffect < ApplicationRecord
  BACKGROUND_EFFECT_ASSET_PATH = File.join(Rails.root, "public", "effect-assets")

  has_many :covers
  validates :name, presence: true

  BLUR_VALUES = {
    "1" => "5,5",
    "2" => "10,10",
    "3" => "50,50"
  }.freeze

  GLOW_VALUES = {
    "1" => "1",
    "2" => "3",
    "3" => "5"
  }.freeze

  VIDEO_VALUES = {
    "1" => "30",
    "2" => "30",
    "3" => "50"
  }.freeze

  def self.random
    one_or_two = rand(2)
    if one_or_two == 1
      find_by(name: "None")
    else
      all = self.all
      if all.empty?
        nil
      else
        all[rand(all.size)]
      end
    end
  end

  def render(source, target)
    case name
    when "None"
      ArtworkSuggestion::ImageGenerationService.copy(source, target)
    when "Antique"
      texture = BACKGROUND_EFFECT_ASSET_PATH + "/antique_0#{level}.jpg"
      ArtworkSuggestion::ImageGenerationService.convert(
        {
          source: source,
          target: target,
          opts: self.class.antique_opts
        }
      )
      if File.exist?(texture)
        ArtworkSuggestion::ImageGenerationService.composite(
          target,
          texture,
          target,
          { gravity: "center" }
        )
      end
    when "Blur"
      if level
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.blur_opts(level.to_s)
        )
      end
    when "Black_and_white"
      case level
      when 1
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.black_and_white_opts(level.to_s)
        )
      when 2
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.black_and_white_opts(level.to_s)
        )
      when 3
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.black_and_white_opts(level.to_s)
        )
      end
    when "Halftone"
      ArtworkSuggestion::ImageGenerationService.convert(
        source: source,
        target: target,
        opts: self.class.halftone_opts
      )
    when "Gray_halftone"
      ArtworkSuggestion::ImageGenerationService.convert(
        source: source,
        target: target,
        opts: self.class.grey_halftone_opts
      )
    when "Obama"
      ArtworkSuggestion::ImageGenerationService.convert(
        source: source,
        target: target,
        opts: self.class.obama_opts
      )
    when "Glow"
      if level
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.glow_opts(level.to_s)
        )
      end
    when "Video"
      case level
      when 1
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.video_opts(level.to_s)
        )
      when 2
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.video_opts(level.to_s)
        )
      when 3
        ArtworkSuggestion::ImageGenerationService.convert(
          source: source,
          target: target,
          opts: self.class.video_opts(level.to_s)
        )
      end
      if level
        texture = BACKGROUND_EFFECT_ASSET_PATH + "/video_texture_#{level}.jpg"
        ArtworkSuggestion::ImageGenerationService.composite(target, texture, target)
      end
    when "Fail"
      ArtworkSuggestion::ImageGenerationService.convert(
        source: source,
        target: target,
        opts: self.class.fail_opts
      )
    else
      raise "Tried to use an invalid background effect"
    end

    # Raise an exception if we failed to produce output
    raise %Q(
      Failed to render background
      Background effect: #{name}
      Level: #{level.nil? ? 'None' : level.to_s}
      Source: #{source}
      Target: #{target}
      ) unless File.exist?(target)
  end

  def self.video_opts(level)
    {
      "1" => [
        "-evaluate",
        "add",
        VIDEO_VALUES[level]
      ],
      "2" => [
        "-evaluate",
        "add",
        VIDEO_VALUES[level],
        "-scale",
        "10%",
        "-scale",
        "1000%"
      ],
      "3" => [
        "-evaluate",
        "add",
        VIDEO_VALUES[level],
        "-scale",
        "10%",
        "-scale",
        "1000%"
      ]
    }[level]
  end

  def self.glow_opts(level)
    [
      "+clone",
      "-evaluate",
      "multiply",
      GLOW_VALUES[level],
      "-blur",
      "10,1
      0",
      "-compose",
      "plus",
      "-composite"
    ]
  end

  def self.black_and_white_opts(level)
    {
      "1" => ["-colorspace", "gray", "-normalize"],
      "2" => [
        "+dither",
        "-blur",
        "5,5",
        "-colors",
        "6",
        "-colorspace",
        "gray",
        "-normalize"
      ],
      "3" => [
        "+dither",
        "-blur",
        "5,5",
        "-colors",
        "2",
        "-colorspace",
        "gray",
        "-normalize"
      ]
    }.freeze[level]
  end

  def self.obama_opts
    [
      "-blur",
      "5,5",
      "-evaluate",
      "multiply",
      "2",
      "+dither",
      "-posterize",
      "4",
      "-colors",
      "4"
    ].freeze
  end

  def self.antique_opts
    ["-sepia-tone", "85%"].freeze
  end

  def self.blur_opts(level)
    ["-blur", BLUR_VALUES[level]]
  end

  def self.halftone_opts
    ["-ordered-dither", "h8x8o"].freeze
  end

  def self.grey_halftone_opts
    ["-ordered-dither", "h8x8o", "-colorspace", "gray"].freeze
  end

  def self.fail_opts
    ["-ordered-dither", "h8x8o"].freeze
  end
end
