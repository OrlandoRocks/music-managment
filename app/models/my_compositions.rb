class MyCompositions
  attr_accessor :person

  def initialize(person)
    @person = person
  end

  def search(options = {})
    ar = ActiveRecord::Base
    sort_order = collect_sort_order(options[:order] || ["status asc", "compositions.name asc", "publishing_splits.percent asc"])
    page = options[:page] || 1
    per_page = (options[:per_page] || 25).to_i
    # FIXME: Probably not correct
    compositions = options[:missing_splits_only] ? not_submitted_sql_condition.to_s : "(#{not_submitted_sql_condition} OR #{submitted_sql_condition})"

    conditions = [
      ar.send(
        :sanitize_sql_array,
        [
          "albums.person_id = ? and #{compositions}",
          @person.id
        ]
      )
    ]

    keyword_conditions = []
    if kw = options[:keyword]
      %w[compositions.name albums.name publishing_splits.percent compositions.state].each do |col|
        if col == "compositions.state"
          state = Composition.convert_to_state(kw)
          keyword_conditions << ar.send(:sanitize_sql_array, ["#{col} LIKE ?", "%#{state}%"]) if state.present?
        else
          keyword_conditions << ar.send(:sanitize_sql_array, ["#{col} LIKE ?", "%#{kw}%"])
        end
      end
    end

    filters_conditions = []
    if filters = options[:filters]
      if filters[:statuses]
        filters_conditions << ar.send(:sanitize_sql_array, ["COALESCE(compositions.state,'new') IN (?)", filters[:statuses]])
      end
      if filters[:splits]
        filters_conditions << ar.send(:sanitize_sql_array, ["publishing_splits.percent IN (?)", filters[:splits]])
      end
      if filters[:album_ids]
        filters_conditions << ar.send(:sanitize_sql_array, ["albums.id IN (?)", filters[:album_ids]])
      end
    end

    conditions_string = conditions.join(" AND ")
    conditions_string << " AND (#{keyword_conditions.join(' OR ')})" unless keyword_conditions.empty?
    conditions_string << " AND #{filters_conditions.join(' AND ')}" unless filters_conditions.empty?

    Song.joins(:album, composition: :publishing_splits).where(conditions_string).paginate(
      page: page,
      per_page: per_page
    ).order(sort_order)
  end

  def albums
    releases.select { |r| r.album_type == "Album" }
  end

  def singles
    releases.select { |r| r.album_type == "Single" }
  end

  def ringtones
    releases.select { |r| r.album_type == "Ringtone" }
  end

  def releases
    @releases ||= Album.find_by_sql(
      "select distinct albums.*
      from songs
      inner join albums
      on songs.album_id = albums.id
      left join compositions
      on songs.composition_id = compositions.id
      where albums.person_id = #{@person.id}
      and
      (
      #{not_submitted_sql_condition}
      or
      #{submitted_sql_condition}
      )
      order by albums.album_type, albums.name"
    )
  end

  def statuses_and_counts
    @statuses_and_counts ||= Composition.find_by_sql(
      "select IF(compositions.state IS NULL, 'new', compositions.state) as state, count(songs.id) as count
      from songs
      inner join albums
      on songs.album_id = albums.id
      left join compositions
      on songs.composition_id = compositions.id
      where albums.person_id = #{@person.id}
      and
      (
      #{not_submitted_sql_condition}
      or
      #{submitted_sql_condition}
      )
      group by IF(compositions.state IS NULL, 'new', compositions.state)
      order by IF(compositions.state IS NULL, 'new', compositions.state)"
    )
  end

  def statuses
    states = statuses_and_counts.collect(&:state)

    statuses = []
    states.each do |state|
      statuses << [Composition.convert_to_status(state), state]
    end

    statuses
  end

  def stats
    stats = []
    total = 0
    statuses_and_counts.each do |s|
      stats << { status: Composition.convert_to_status(s.state), count: s.count.to_i, state: s.state }
      total += s.count.to_i
    end
    stats << { status: "Total Compositions", count: total, state: "total" }
  end

  def splits_and_counts
    @splits_and_counts ||= PublishingSplit.find_by_sql(
      "select percent, count(compositions.id) as count
      from compositions
      inner join songs
      on songs.composition_id = compositions.id
      inner join albums
      on songs.album_id = albums.id
      inner join composers
      on albums.person_id = composers.person_id
      inner join publishing_splits ps
      on ps.composition_id = compositions.id
      and ps.composer_id = composers.id
      where albums.person_id = #{@person.id}
      and #{submitted_sql_condition}
      group by percent
      order by percent"
    )
  end

  def splits
    splits = []
    splits_and_counts.each do |s|
      splits << [s.percent.to_s, s.count]
    end

    splits
  end

  def compositions_in_dispute
    Composition.joins(songs: :album).where(
      {
        state: Composition.convert_to_state("dispute"),
        albums: { person_id: @person.id }
      }
    ).includes(:muma_songs)
  end

  private

  def not_submitted_sql_condition
    "(albums.is_deleted = false and albums.takedown_at is null and (compositions.id is null or COALESCE(compositions.state,'') IN ('new','')))"
  end

  def submitted_sql_condition
    "(compositions.id is not null and COALESCE(compositions.state,'') NOT IN ('new','')) and albums.is_deleted = false"
  end

  def collect_sort_order(sort_array)
    return sort_array unless sort_array.kind_of?(Array)

    sort_order = []
    statuses = [
      "Dispute",
      "Split missing",
      "Not distributed yet",
      "Split submitted",
      "Sent for registration",
      "Registered",
      "Not controlled"
    ]
    sort_array.each do |sort|
      if sort =~ /status\s(desc|asc)/
        state_asc = statuses.collect { |a| "'#{Composition.convert_to_state(a)}'" }

        sort_order << if $1 == "asc"
                        "FIELD(COALESCE(state,'new'),#{state_asc.join(',')})"
                      else
                        "FIELD(COALESCE(state,'new'),#{state_asc.reverse.join(',')})"
                      end
      else

        sort_order << sort
      end
    end

    sort_order.join(",")
  end
end
