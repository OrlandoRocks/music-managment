class RedistributionPackage < ApplicationRecord
  include ActiveRecordPolyfill

  STORE_CONVERTER_CLASS_MAP = {
    "apple" => "MusicStores::Itunes::Converter"
  }

  belongs_to :person
  belongs_to :artist

  scope :pending, -> { where(state: "pending") }

  def distributions
    Distribution.for_store_creatives(person_id, artist_id, converter_class).readonly(false)
  end

  def pending?
    state == "pending"
  end

  def self.for(person_id, artist_id, store_name)
    where(person_id: person_id, artist_id: artist_id, converter_class: STORE_CONVERTER_CLASS_MAP[store_name]).first
  end
end
