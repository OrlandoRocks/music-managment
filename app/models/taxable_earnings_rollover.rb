class TaxableEarningsRollover < PersonEarningsTaxMetadatum
  # This exists mainly as an alias for PersonTransaction target_type

  has_many :person_transactions, dependent: :nullify

  def distribution_earnings?
    total_distribution_earnings > 0.0
  end

  def publishing_earnings?
    total_publishing_earnings > 0.0
  end

  def total_taxable_earnings
    [
      total_taxable_publishing_earnings,
      total_taxable_distribution_earnings,
    ].sum
  end
end
