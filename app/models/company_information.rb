# frozen_string_literal: true

class CompanyInformation < ApplicationRecord
  FIELDS = Set[
    "enterprise_number",
    "company_registry_data",
    "place_of_legal_seat",
    "registered_share_capital"
  ].freeze

  FIELD_FORMAT = %q[(^[\w\s().\/_\-@#`,*]*$)]

  belongs_to :person

  validates :enterprise_number,
            :company_registry_data,
            :place_of_legal_seat,
            format: {
              with: Regexp.new(FIELD_FORMAT),
              message: "is_invalid"
            },
            allow_blank: true
  validate :customer_type_should_be_buisness
  validate :country_should_not_be_us_and_territories

  private

  def customer_type_should_be_buisness
    errors.add(:customer_type, "should_be_business") unless person&.business?
  end

  def country_should_not_be_us_and_territories
    errors.add(:country, "should_not_be_us") if person&.from_united_states_and_territories?
  end
end
