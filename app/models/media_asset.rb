# 2009-08-19 -- ED -- this model will be handling all types of user file upload: images, audio, pdf, docs, etc.
# 2009-10-5 -- ED -- added mime_type image/pjpeg & image/x-png for IE7 upload

class MediaAsset < ApplicationRecord
  belongs_to :person
  belongs_to :s3_asset
  has_many   :derived_media_assets, dependent: :destroy

  validates :person, :type, presence: true
  validates   :description, length: { maximum: 500, allow_nil: true }
  validates   :width, numericality: { only_integer: true, less_than: 65_535, allow_nil: true }
  validates   :height, numericality: { only_integer: true, less_than: 65_535, allow_nil: true }
  validates   :bit_depth, numericality: { only_integer: true, less_than: 255, allow_nil: true }
  validates   :size, numericality: { only_integer: true, less_than: 4_294_967_295, greater_than: 0, allow_nil: false, message: I18n.t("errors.messages.invalid") }
  validates :mime_type,
            inclusion: {
              in: %w[image/jpeg image/pjpeg image/png image/x-png image/tiff image/gif image/bmp],
              message: I18n.t("model.band_photo.image_format")
            }

  after_validation :human_readable_messages

  before_update  :remove_assets
  before_destroy :destroy_assets

  private

  # Trying to make the error message more 'human readable' ie is to change from
  # 'Mime type format should be JPEG, PNG, TIFF, GIFF, or BMP' to
  # 'Format should be JPEG, PNG, TIFF, GIFF, or BMP'
  def human_readable_messages
    return if errors[:mime_type].blank?

    # add readable message
    errors.add(:image_format, errors[:mime_type])

    # remove non-readable message
    errors.delete(:mime_type)
  end

  def remove_assets
    # we're only working with s3 asset for band photos
    # we should also remove local asset in the future
    begin
      s3_asset.delete_key! if s3_asset
    rescue => e
      logger.error("removing media assets from local and/or s3: ")
    end
  end

  def destroy_assets
    begin
      s3_asset.destroy! if s3_asset # we're only working with s3 asset for band photos
    rescue => e
      logger.error("destroying s3 asset in MediaAsset model ")
    end
  end
end
