class AffiliateReport < ApplicationRecord
  serialize :data, QuirkyJson
  belongs_to :person
  belongs_to :api_key

  validates :person, :person_email, :api_key, presence: true
end
