# = Description
# Creates distribution report based on the inventory_usages table
#
# = Usage
#
#
# = Change Log
# [2010-03-15 -- CH]
# Created Class

class DistributionReport < ReportBase
  def initialize(options)
    self.fields = [{ field_name: :total_distributions, data_type: "integer", show_average: false }]
    @country = options[:country]
    options = options.merge({ grouped_by: GROUPED_BY_RESOLUTION, report_type: GROUPED })
    super
  end

  protected

  def retrieve_data
    InventoryUsage.select(select_string).where(build_condition_array).joins(join_string).order(order_string).group(group_by_string)
  end

  def build_condition_array
    make_date_conditions
    ["c.country='#{@country}' and related_type='Album' and inventory_usages.created_at >= ? and inventory_usages.created_at <= ?", start_date, end_date]
  end

  def select_string
    %Q|DATE_FORMAT(inventory_usages.created_at, '#{resolution_date_format}') as `resolution`,
       a.album_type,
       count(*) as total_distributions|
  end

  def join_string
    [
      "inner join albums a on a.id=inventory_usages.related_id",
      "inner join people p on p.id=a.person_id",
      "inner join country_websites c on c.id=p.country_website_id"
    ]
  end

  def order_string
    "inventory_usages.created_at DESC, a.album_type"
  end

  def group_by_string
    "DATE_FORMAT(inventory_usages.created_at, '#{group_by_date_format}'), a.album_type"
  end
end
