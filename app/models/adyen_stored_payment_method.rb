# frozen_string_literal: true

class AdyenStoredPaymentMethod < ApplicationRecord
  belongs_to :person
  belongs_to :country
  belongs_to :corporate_entity
  belongs_to :adyen_payment_method_info
  has_many :adyen_transactions, dependent: :destroy

  validates :recurring_reference, presence: true

  delegate :payment_method_name,
           :payment_method_display_name,
           to: :adyen_payment_method_info, allow_nil: true
end
