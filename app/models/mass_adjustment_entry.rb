class MassAdjustmentEntry < ApplicationRecord
  belongs_to :mass_adjustment_batch
  belongs_to :person

  SUCCESS = "success"
  CANCELLED = "cancelled"
  ERROR = "error"

  STATUS = [SUCCESS, ERROR]

  scope :unprocessed, -> { where(status: nil) }
  scope :successful_entries, -> { where(status: MassAdjustmentEntry::SUCCESS) }
  scope :errored_entries, -> { where(status: MassAdjustmentEntry::ERROR) }
  scope :cancelled_entries, -> { where(status: MassAdjustmentEntry::CANCELLED) }

  def account_goes_negative?
    person && (person.balance + amount).negative?
  end

  def success?
    status == SUCCESS
  end

  def error?
    status == ERROR
  end
end
