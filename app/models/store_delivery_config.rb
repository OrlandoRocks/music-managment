class StoreDeliveryConfig < ApplicationRecord
  belongs_to :store

  scope :by_store_name,
        ->(store_short_name) {
          joins(:store).where(Store.arel_table[:short_name].eq(store_short_name))
        }

  scope :paused_stores,
        ->(store_ids) {
          where(paused: true, store_id: store_ids)
        }

  def self.for(store_short_name)
    by_store_name(store_short_name).first
  end

  def pause!
    update(paused: true)
    DistributorAPI::Store.pause(store_id)
  end

  def unpause!
    update(paused: false)
    DistributorAPI::Store.unpause(store_id)
  end

  def unpaused?
    !paused?
  end

  def queue
    paused? ? "delivery-pause" : delivery_queue
  end
end
