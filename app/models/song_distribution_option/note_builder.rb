class SongDistributionOption::NoteBuilder
  def initialize(args)
    @option      = args[:option]
    @user        = args[:user]
    @song        = args[:song]
    @user_input  = args[:user_input] || ""
  end

  attr_reader :option

  def note
    Note.new(
      related: @song,
      note_created_by: @user,
      subject: subject,
      note: note_text
    )
  end

  private

  def subject
    "#{option.to_s.humanize} #{removed_or_added?} track #{@song.track_num}."
  end

  def note_text
    text = ""
    text += option_specific_text[option.to_s] if option_specific_text[option.to_s]
    text += "User Reason: #{@user_input}" if @user_input.present?
    text
  end

  def removed_or_added?
    @song.send("#{option}?") ? "removed from" : "added to"
  end

  # not crazy about these 2 below methods since they contain specialized information.
  # consider refactoring.

  def option_specific_text
    specific ||= {}
    specific["album_only"] ||= "Stores affected are #{album_only_store_names}. When ready please send metadata update to those stores. "
    specific
  end

  def album_only_store_names
    Store.stores_supporting_album_only.map(&:name).join(", ")
  end
end
