class FlagTransition < ApplicationRecord
  belongs_to :person
  belongs_to :admin, class_name: "Person"
  belongs_to :flag_reason, optional: true
  belongs_to :flag, optional: true

  has_many :notes, -> { order(created_at: :desc) }, as: :related

  validates_with FlagReasonValidator

  def self.create_trans(opts = {})
    create(
      updated_by: opts[:updated_by],
      comment: opts[:comment],
      add_remove: opts[:add_remove],
      flag_id: opts[:flag_id],
      person_id: opts[:person_id],
    )
  end
end
