class SongLifetimeEarning < ApplicationRecord
  self.primary_key = "song_id"

  belongs_to :song
end
