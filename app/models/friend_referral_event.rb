class FriendReferralEvent < ApplicationRecord
  serialize :raw_request
  serialize :raw_response
end
