class TierReward < ApplicationRecord
  belongs_to :tier
  belongs_to :reward

  validates :tier_id, uniqueness: { scope: :reward_id }
end
