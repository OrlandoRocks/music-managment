class YtmTracks
  STATE_CONVERSIONS = {
    "not_sent" => "IS NULL",
    "processing" => "= 'new'",
    "sent" => "= 'approved'",
    "claim_removed" => "= 'rejected'"
  }

  def initialize(person)
    @person = person
  end

  def unsent_songs_search
    results = search(status: "eligible_not_sent", per_page: 1_000_000)
    song_list =
      results.first.each_with_object([]) do |song, songs|
        songs << { "id" => song.id, "name" => song.name, "artist_names" => song.creatives.map(&:name), "album_name" => song.album_name }
      end

    [song_list, results.second]
  end

  def unsent_songs
    unsent_songs_search.first
  end

  def search(options = {})
    ar = ActiveRecord::Base
    select_sql = "songs.*, albums.name AS album_name, salepoint_songs.state AS state, ytm_ineligible_songs.id AS ytm, ytm_blocked_songs.id AS blocker,
    CONCAT_WS(', ', group_concat(prim_artists.name SEPARATOR ', '), group_concat(feat_artists.name SEPARATOR ', ')) AS artist_names, COALESCE(IF(optional_isrc = '', null, optional_isrc), tunecore_isrc) as track_isrc, COALESCE(optional.number, tc.number) as release_upc,
    CASE
      WHEN (salepoint_songs.takedown_at IS NOT NULL OR salepoints.takedown_at IS NOT NULL) THEN 6
      WHEN ytm_ineligible_songs.id IS NOT NULL THEN 4
      WHEN salepoint_songs.state IS NULL THEN 1
      WHEN salepoint_songs.state = 'new' THEN 2
      WHEN salepoint_songs.state = 'approved' THEN 3
      WHEN salepoint_songs.state = 'rejected' THEN 5
      ELSE 7
    END as status, salepoint_songs.takedown_at AS song_takedown_at, salepoints.takedown_at AS salepoint_takedown_at"

    join_sql = ar.send(
      :sanitize_sql_array,
      [
        "INNER JOIN albums ON songs.album_id = albums.id
      LEFT OUTER JOIN creatives feat_creatives ON feat_creatives.creativeable_id = songs.id
      AND feat_creatives.creativeable_type = 'Song' AND feat_creatives.role = 'primary_artist'
      LEFT OUTER JOIN artists feat_artists ON feat_creatives.artist_id = feat_artists.id
      LEFT OUTER JOIN creatives prim_creatives ON prim_creatives.creativeable_id = albums.id
      AND prim_creatives.creativeable_type = 'Album' AND prim_creatives.role = 'primary_artist'
      LEFT OUTER JOIN artists prim_artists ON prim_creatives.artist_id = prim_artists.id
      LEFT OUTER JOIN salepoint_songs ON salepoint_songs.song_id = songs.id
      INNER JOIN salepoints ON albums.id = salepoints.salepointable_id AND salepoints.store_id = ?
      AND salepoints.salepointable_type = 'Album'
      AND salepoints.status = 'complete'
      LEFT OUTER JOIN ytm_ineligible_songs ON ytm_ineligible_songs.song_id = songs.id
      LEFT OUTER JOIN ytm_blocked_songs ON ytm_blocked_songs.song_id = songs.id
      LEFT OUTER JOIN upcs optional ON optional.upcable_id = albums.id AND optional.inactive = false AND optional.upc_type = 'optional' AND optional.upcable_type = 'Album'
      INNER JOIN upcs tc ON tc.upcable_id = albums.id AND tc.inactive = false AND tc.upc_type = 'tunecore' AND tc.upcable_type = 'Album'",
        YtmTracks.store.id
      ]
    )

    conditions = ar.send(:sanitize_sql_array, ["albums.person_id = ? AND #{YtmTracks.default_conditions(true)}", @person.id])

    if search = options[:search]
      conditions += " AND " + ar.send(
        :sanitize_sql_array,
        [
          "(albums.name LIKE ? OR songs.name LIKE ?
        OR prim_artists.name LIKE ? OR feat_artists.name LIKE ?)",
          "%#{search}%",
          "%#{search}%",
          "%#{search}%",
          "%#{search}%"
        ]
      )
    end

    if options[:status]
      if STATE_CONVERSIONS.key?(options[:status])
        conditions += " AND salepoint_songs.state #{STATE_CONVERSIONS[options[:status]]}"
      elsif options[:status] == "marked_ineligible"
        conditions += " AND ytm_ineligible_songs.id IS NOT NULL"
      elsif options[:status] == "taken_down"
        conditions += " AND (salepoint_songs.takedown_at IS NOT NULL OR salepoints.takedown_at IS NOT NULL)"
      elsif options[:status] == "eligible_not_sent"
        conditions += " AND (salepoint_songs.id IS NULL AND ytm_ineligible_songs.id IS NULL)"
      elsif options[:status] == "sent_or_processing"
        conditions += " AND (salepoint_songs.state = 'approved' OR salepoint_songs.state = 'new')"
      end
    end

    conditions += " AND " + ar.send(:sanitize_sql_array, ["albums.id IN (?)", options[:releases]]) if options[:releases]

    if options[:artists]
      conditions += " AND " + ar.send(
        :sanitize_sql_array,
        [
          "(prim_artists.id IN (?) OR feat_artists.id IN (?))",
          options[:artists],
          options[:artists]
        ]
      )
    end

    order = pick_order(options[:sort], options[:direction])

    page = options[:page] || 1
    per_page = options[:per_page] || 25

    @songs = Song.select(select_sql)
                 .joins(join_sql)
                 .where(conditions)
                 .preload(:ytm_ineligible_song,
                          :ytm_blocked_song,
                          creatives: :artist,
                          salepoint_songs: :salepoint,
                          album: [{ creatives: :artist }])
                 .group("songs.id")
                 .order(order)
                 .paginate(page: page, per_page: per_page)

    [@songs, albums_without_salepoints?]
  end

  def albums
    @albums ||= @person.albums.joins(
      "inner join songs on songs.album_id = albums.id
      left outer join salepoint_songs on songs.id = salepoint_songs.song_id"
    ).where(YtmTracks.default_conditions).distinct
  end

  def full_albums
    albums.select { |a| a.instance_of?(Album) }
  end

  def singles
    albums.select { |a| a.instance_of?(Single) }
  end

  def artists
    @artists ||= Artist.select("DISTINCT artists.name, artists.id")
                       .joins("INNER JOIN creatives ON creatives.artist_id = artists.id
      AND creatives.creativeable_type = 'Album'
      INNER JOIN albums ON creatives.creativeable_id = albums.id AND creatives.role = 'primary_artist'
      inner join songs on albums.id = songs.album_id
      left outer join salepoint_songs on songs.id = salepoint_songs.song_id")
                       .where("albums.person_id = ? and #{YtmTracks.default_conditions}", @person.id)
  end

  def monetized_songs
    @monetized_songs ||= @person.songs.joins(:song_salepoints).where(
      "#{YtmTracks.default_conditions(true)} and salepoints.store_id = ?
      and salepoint_songs.state = 'approved' and salepoint_songs.takedown_at is null",
      YtmTracks.store.id
    )
  end

  def total_songs
    conditions = YtmTracks.default_conditions.dup
    conditions.slice!(" OR salepoint_songs.id IS NOT NULL")
    @total_songs ||= @person.songs.where(conditions)
  end

  def ineligible_songs
    conditions = YtmTracks.default_conditions.dup
    conditions.slice!(" OR salepoint_songs.id IS NOT NULL")
    @ineligible_songs ||= @person.songs.joins(:ytm_ineligible_song).where(conditions)
  end

  def blocked_songs
    conditions = YtmTracks.default_conditions.dup
    conditions.slice!(" OR salepoint_songs.id IS NOT NULL")
    @ineligible_songs ||= @person.songs.joins(:ytm_blocked_song).where(conditions)
  end

  def self.default_conditions(include_all_monetized = false)
    conditions = "((albums.takedown_at IS NULL AND albums.is_deleted = 0 AND albums.finalized_at is not null)
      OR salepoint_songs.id IS NOT NULL) AND albums.payment_applied = 1 AND albums.album_type != 'Ringtone'"

    conditions << (include_all_monetized ? YtmTracks.all_monetized_conditions : YtmTracks.live_album_conditions)
    conditions
  end

  def self.all_monetized_conditions
    "AND ((albums.legal_review_state = 'APPROVED' OR albums.legal_review_state = 'DO NOT REVIEW') OR (albums.legal_review_state = 'NEEDS REVIEW' AND salepoint_songs.state = 'approved'))"
  end

  def self.live_album_conditions
    " AND (albums.legal_review_state = 'APPROVED' OR albums.legal_review_state = 'DO NOT REVIEW') "
  end

  def self.store
    @store ||= Store.find_by(abbrev: "ytsr")
  end

  def self.songs_for_album(album)
    select_sql = "songs.*, salepoint_songs.state AS state, ytm_ineligible_songs.id AS ytm, ytm_blocked_songs.id AS blocker,
      salepoint_songs.takedown_at AS song_takedown_at, salepoints.takedown_at AS salepoint_takedown_at"
    join_sql = "LEFT OUTER JOIN salepoint_songs ON salepoint_songs.song_id = songs.id
      INNER JOIN albums on songs.album_id = albums.id
      INNER JOIN salepoints ON songs.album_id = salepoints.salepointable_id
      AND salepoints.store_id = #{YtmTracks.store.id} AND salepoints.salepointable_type = 'Album'
      LEFT OUTER JOIN ytm_ineligible_songs ON ytm_ineligible_songs.song_id = songs.id
      LEFT OUTER JOIN ytm_blocked_songs ON ytm_blocked_songs.song_id = songs.id"

    album.songs.select(select_sql)
         .joins(join_sql)
         .preload(:ytm_ineligible_song, :ytm_blocked_song, salepoint_songs: :salepoint)
         .where(YtmTracks.default_conditions)
  end

  def has_unactioned_songs?
    return false unless @person.has_active_ytm?

    conditions = YtmTracks.default_conditions.dup
    conditions.slice!(" OR salepoint_songs.id IS NOT NULL")
    conditions += " and salepoint_songs.id is null and ytm_ineligible_songs.id is null and ytm_blocked_songs.id is null"
    @person.songs.joins(
      "left join salepoint_songs on songs.id = salepoint_songs.song_id
                         left join ytm_ineligible_songs on songs.id = ytm_ineligible_songs.song_id
                         left join ytm_blocked_songs on ytm_blocked_songs.song_id = songs.id"
    )
           .where(conditions).limit(1).present?
  end

  private

  def pick_order(order, direction)
    orders = ["status", "artist_names", "album_name", "songs.track_num", "songs.name", "track_isrc", "release_upc"]
    if order.nil? || !orders.include?(order)
      return ["status asc", "artist_names asc", "album_name asc", "songs.track_num asc"]
    end

    order_arr = []
    order_arr << orders.delete(order) + " #{direction}"
    order_arr + orders.map { |order| order + " asc" }
  end

  def albums_without_salepoints?
    albums.joins(
      "LEFT OUTER JOIN salepoints ON albums.id = salepoints.salepointable_id
      AND salepoints.salepointable_type = 'Album' AND salepoints.store_id = #{YtmTracks.store.id}"
    ).where("salepoints.id IS NULL").limit(1).present?
  end
end
