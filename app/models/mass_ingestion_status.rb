class MassIngestionStatus < ApplicationRecord
  enum status: {
    successful: :successful,
    failed: :failed,
    skipped: :skipped
  }
end
