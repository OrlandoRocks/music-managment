class ForeignExchangePeggedRate < ApplicationRecord
  # shouldn't use the has_many's much, but including them anyway
  has_many :invoices
  has_many :invoice_logs,
           primary_key: "id",
           foreign_key: "invoice_foreign_exchange_pegged_rate_id",
           class_name: "InvoiceLog"

  belongs_to :country
  belongs_to :country_website
  belongs_to :updated_admin, class_name: Person.to_s, primary_key: :id, foreign_key: :updated_by

  scope :for_iso_code,
        ->(iso_code) {
          joins(:country_website)
            .where(country_websites: { country: iso_code })
            .order(created_at: :desc)
        }

  scope :for_currency,
        ->(currency_iso_code) {
          where(currency: currency_iso_code)
            .order(created_at: :desc)
        }

  scope :for_country_website_id,
        ->(country_website_id) {
          where(country_website_id: country_website_id)
            .limit(1)
        }

  scope :for_currency,
        ->(currency_iso) {
          where(currency: currency_iso).order(created_at: :desc)
        }

  validates :pegged_rate, :country_id, :currency, presence: true

  before_create :update_previous_rate
  validate :is_duplicate?, on: :create

  def self.previous_rate(currency: nil, country_id: nil)
    return if currency.blank? && country_id.blank?

    if currency.present?
      pegged_rates_history = where(currency: currency)
    elsif country_id.present?
      pegged_rates_history = where(country_id: country_id)
    end
    pegged_rates_history.order(created_at: :desc).first
  end

  def update_previous_rate
    prev_rate = ForeignExchangePeggedRate.previous_rate(currency: currency, country_id: country_id)
    return false unless prev_rate

    prev_rate.update(updated_at: created_at)
  end

  def is_duplicate?
    prev_rate = ForeignExchangePeggedRate.previous_rate(currency: currency, country_id: country_id)
    if prev_rate.present? && pegged_rate == prev_rate.pegged_rate
      errors[:base] << "You cannot create a new entry with the same pegged rate"
      return false
    end

    true
  end

  def self.current_rate(iso: nil, currency: nil)
    if iso.present?
      for_iso_code(iso).first
    elsif currency.present?
      for_currency(currency).first
    end
  end

  def self.pegged_currencies
    distinct(:currency).pluck(:currency)
  end

  def self.non_native_currency_site?(country_website_id:)
    for_country_website_id(country_website_id).exists?
  end
end
