# 2009-08-10 -- ED -- added validation for maximumo of 50 tour dates creation per person
# 2009-09-17 -- ED -- added validation for event date

class TourDate < ApplicationRecord
  DATE_FORMAT = "%B %d, %Y"

  belongs_to :person
  belongs_to :artist
  belongs_to :country
  belongs_to :us_state

  validates :person, :artist, :country, :city, :venue, :event_date_at, presence: true
  validates :us_state, presence: { if: :state_required? }
  validates   :venue, length: { maximum: 50, message: "exceeded 50 characters", allow_nil: true }
  validates   :city, length: { maximum: 50, message: "exceeded 50 characters", allow_nil: true }
  validates   :information, length: { maximum: 500, message: "exceeded 500 characters", allow_nil: true }

  validate :maximum_creations_per_person, if: proc { |tour_date| tour_date.person }
  validate :upcomming_tour_date, if: proc { |tour_date| tour_date.event_date_at }

  before_save :set_us_state_to_other, if: proc { |tour_date| !tour_date.state_required? }

  def state_required?
    country && country.name == "United States"
  end

  def event_date_at=(value)
    begin
      super(value)
    rescue
      self.event_date_at = Tunecore::StringDateParser.to_date(date)
    end
  end

  def event_date_at_to_s
    Tunecore::StringDateParser.to_s(event_date_at)
  end

  private

  def maximum_creations_per_person
    errors.add("You", I18n.t("models.promotion.cannot_create_more_than_50_tour_dates")) if person.tour_dates.size >= 50
  end

  def upcomming_tour_date
    return if event_date_at.to_date >= Time.now.to_date

    errors.add("Date", I18n.t("models.promotion.cannot_be_older_than_today"))
  end

  def set_us_state_to_other
    other_state = UsState.find_by(name: "OTHER")
    self.us_state = other_state if other_state
  end
end
