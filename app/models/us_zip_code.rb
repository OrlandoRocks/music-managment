# == Schema Information
# Schema version: 404
#
# Table name: us_zip_codes
#
#  code  :string(5)       not null
#  city  :string(40)      not null
#  state :string(2)       not null
#  id    :integer(11)     not null, primary key
#

class UsZipCode < ApplicationRecord
  has_many :trend_report_sales

  validates :state,
            length: {
              maximum: 2,
              message: "Please use the state's two letter code"
            }

  def city_and_state
    city + ", " + state
  end
end
