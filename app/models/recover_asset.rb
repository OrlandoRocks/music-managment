require "S3"

class RecoverAsset < ApplicationRecord
  self.table_name = "studio_recover_assets"

  serialize :custom_fields, Hash
  belongs_to :person
  belongs_to :admin, class_name: "Person"
  belongs_to :s3_asset

  enum state: {
    enqueued: "enqueued",
    downloading_assets: "downloading_assets",
    completed: "completed"
  }

  def s3_key
    "#{Rails.env}/#{person_id}/#{filename}"
  end

  def upload_zip(zip_file)
    asset = S3Asset.new(
      bucket: RECOVERY_ASSETS_BUCKET_NAME,
      key: s3_key,
      filename: filename,
      filetype: "application/zip"
    )
    asset.put!(file: open(zip_file))
    asset.save!
    self.s3_asset = asset
    save!
  end

  def filename
    "#{id}_#{person_id}.zip"
  end

  def url
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = 3600
    generator.get(RECOVERY_ASSETS_BUCKET_NAME, s3_asset.key)
  end

  def albums_ids
    custom_fields[:albums_ids]
  end

  def include_assets
    custom_fields[:include_assets]
  end
end
