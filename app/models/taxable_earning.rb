class TaxableEarning < ApplicationRecord
  extend ArelTableMethods
  extend ArelNamedMysqlFunctions
  include ArelNamedMysqlFunctions

  belongs_to :person

  scope :year_to_date, -> { where(select_year(taxable_earning_t[:created_at]).eq(Time.now.year)) }
end
