class BulkAddStoresByAlbums
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  validates :store_id, :album_ids_or_upcs, :store, :albums, presence: true

  attr_accessor :store_id, :album_ids_or_upcs
  attr_reader :store, :albums, :bulk_album_adder

  def initialize(attributes = nil)
    set_attributes(attributes) if attributes
  end

  def persisted?
    false
  end

  def save
    set_bulk_album_adder
    bulk_add_albums_to_stores
  end

  private

  def set_attributes(attributes)
    @store_id = attributes[:store_id]
    @album_ids_or_upcs = attributes[:album_ids_or_upcs]
    set_store
    set_albums
  end

  def bulk_add_albums_to_stores
    bulk_album_adder.add
    errors.add(:service_message, bulk_album_adder.messages)
  end

  def set_store
    @store = Store.find_by(id: store_id)
  end

  def set_albums
    bulk_album_finder = BulkAlbumFinder.new(album_ids_or_upcs)
    @albums = bulk_album_finder.execute
    errors.add(:service_message, bulk_album_finder.messages)
  end

  def set_bulk_album_adder
    @bulk_album_adder = BulkAddAlbumsToStores.new(store, albums)
  end
end
