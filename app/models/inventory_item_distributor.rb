class InventoryItemDistributor
  #
  #  Takes an InventoryItem
  #
  def initialize(inventory_item)
    @item = inventory_item.item
    @inventory = inventory_item.inventory
  end

  def distribute!
    if distributable_classes.include?(@item.class)
      @item.distribute!
      if !Inventory.is_multi_use?(@item) && !Renewal.renewal_exists_for?(@item)
        create_renewal_for_distributed_item
      elsif Inventory.is_multi_use?(@item)
        create_renewal_for_distributed_item
      end
    elsif @item.instance_of?(Salepoint)
      Delivery::SalepointWorker.perform_async(@item.id)
    end
  end

  protected

  def distributable_classes
    [Album, Single, Ringtone]
  end

  def create_renewal_for_distributed_item
    case renewal_level
    when "Item"
      create_product_item_renewal
    when "Product"
      create_product_renewal
    else
      raise "Unknown type of Renewal to create in Inventory Distrubutor"
    end
  end

  def create_product_item_renewal
    if @inventory.purchase.nil?
      Rails.logger.info("create_product_item_renewal nil purchase: inventory #{@inventory.id}, album #{@item.id}")
    end

    Renewal.create(
      purchase: @inventory.purchase,
      renewed_items: @item,
      person: @inventory.person,
      item_to_renew_id: product_item.id,
      item_to_renew_type: "ProductItem"
    )
  end

  def renewal_level
    product = product_item.product
    # puts product.renewal_level
    product.renewal_level
  end

  def create_product_renewal
    if @inventory.purchase.nil?
      Rails.logger.info("create_product_renewal nil purchase: inventory #{@inventory.id}, album #{@item.id}")
    end
    Renewal.create(
      purchase: @inventory.purchase,
      renewed_items: @item,
      person: @inventory.person,
      item_to_renew_id: product_item.product_id,
      item_to_renew_type: "Product"
    )
  end

  def product_item
    ProductItem.where("id = ?", @inventory.product_item_id).includes(:product).first
  end
end
