class StoreSalesReport
  def initialize
    @totals = []
  end

  #
  #  Specific Store Numbers
  #
  def albums_sold(store_id)
    current = @totals[store_id]
    current ? current[:albums_sold] : 0
  end

  def songs_sold(store_id)
    current = @totals[store_id]
    current ? current[:songs_sold] : 0
  end

  def songs_streamed(store_id)
    current = @totals[store_id]
    current ? current[:songs_streamed] : 0
  end

  def currency(store_id)
    current = @totals[store_id]
    current ? current[:currency] : "USD"
  end

  def net_sales(store_id)
    current = @totals[store_id]
    if current
      cents = current[:net_sales]
      currency = current[:currency]
      Money.new(cents, currency)
    else
      Money.new(0)
    end
  end

  def net_earnings(store_id)
    current = @totals[store_id]
    cents = current ? current[:net_earnings] : 0
    Money.new(cents)
  end

  def add_record(record)
    return unless record.store_intake

    store_id = record.store_intake.store_id
    setup_store_totals(store_id)

    current = @totals[store_id]
    current[:albums_sold] += record.units_sold if record.song_id.blank?
    current[:songs_sold] += record.units_sold if record.song_id.present? && record.sale_type == "dl"
    current[:songs_streamed] += record.units_sold if record.song_id.present? && record.sale_type != "dl"
    current[:net_sales] += record.local_total_cents || 0
    current[:net_earnings] += record.usd_total_cents || 0
    current[:currency] ||= record.local_currency
  end

  def setup_store_totals(store_id)
    return if @totals[store_id]

    @totals[store_id] = {
      albums_sold: 0,
      songs_sold: 0,
      songs_streamed: 0,
      net_sales: 0,
      net_earnings: 0,
      currnecy: nil
    }
  end
end
