class CountryPhoneCode < ApplicationRecord
  TOLL_FRAUD_COUNTRIES = [
    "Afghanistan",
    "Albania",
    "Algeria",
    "Azerbaijan",
    "Cameroon",
    "Croatia",
    "Dominican Republic",
    "Ecuador",
    "Ethiopia",
    "Guinea",
    "Indonesia",
    "Iran",
    "Kenya",
    "Laos",
    "Morocco",
    "Nigeria",
    "Philippines",
    "Sri Lanka",
    "Taiwan",
    "Tanzania",
    "Tajikistan",
    "Turkey",
    "Ukraine",
    "Vietnam"
  ].freeze

  belongs_to :country

  scope :for_iso_code,
        ->(iso_code) {
          joins(:country)
            .where(countries: { iso_code: iso_code })
        }

  def self.toll_fraud_phone_codes
    codes_with_country_names.where(name: TOLL_FRAUD_COUNTRIES)
  end

  def self.codes_with_country_names
    Country
      .joins(:country_phone_codes)
      .select(["countries.name", "country_phone_codes.code"])
  end

  def self.find_for_iso_code(iso_code)
    for_iso_code(iso_code).first
  end
end
