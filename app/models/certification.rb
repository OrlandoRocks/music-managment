class Certification < ApplicationRecord
  has_many :tier_certifications, dependent: :destroy
  has_many :tiers, through: :tier_certifications

  validates :name, :points, :category, presence: true
  validates :is_active, inclusion: { in: [true, false] }
end
