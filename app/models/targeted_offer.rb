class TargetedOffer < ApplicationRecord
  ###
  #
  # Has Many relationships
  #
  ###
  has_many  :targeted_offer_exclusions, dependent: :destroy
  has_many  :exclude_targeted_offers,   through: :targeted_offer_exclusions

  has_many  :excluded_in_targeted_offer_exclusions, dependent: :destroy, class_name: "TargetedOfferExclusion", foreign_key: "exclude_targeted_offer_id"

  has_many  :targeted_people,           dependent: :destroy
  has_many  :targeted_products,         dependent: :destroy
  has_many  :targeted_product_stores,   through: :targeted_products, dependent: :destroy
  has_many  :products,                  through: :targeted_products
  has_many  :purchases,                 through: :targeted_products
  has_many  :targeted_advertisements,   dependent: :destroy
  has_one   :created_by, class_name: "Person", foreign_key: "id", primary_key: "created_by_id"

  ###
  #
  # Belongs to relationships
  #
  ###
  belongs_to :country_website

  ###
  #
  # Virtual Attributes
  #
  ###
  attr_accessor :population_params, :population_where_sql, :population_having_sql, :customers_to_add, :activation_changed

  ###
  #
  # Constants
  #
  ###
  ACTIVE_CONDITIONS = %Q|targeted_offers.status='Active' AND ((targeted_offers.usage_limit > usage_count) OR targeted_offers.usage_limit = 0) AND
                      ((date_constraint = 'expiration' AND CURDATE() >= DATE(targeted_offers.start_date) AND CURDATE() <= DATE(targeted_offers.expiration_date)) OR
                      (date_constraint = 'join plus' AND CURDATE() <= DATE_ADD( DATE(:created_on), INTERVAL targeted_offers.join_plus_duration DAY)) OR
                      (date_constraint = 'permanent'))|

  PRODUCT_SHOW_TYPES = [["Show Only Selected Products", "show_only_selected"], ["Adjust Default Product Prices", "price_override"]]
  DATE_CONSTRAINT_TYPES = [["Same for all Customers", "expiration"], ["Expires after Customer Joins", "join plus"], ["Never Expires for the Customer", "permanent"]]

  # Statuses (formerly known as Delayed Job statuses)
  ADD_PEOPLE_SCHEDULED_STATUS         = "add people scheduled"
  ADD_PEOPLE_PROCESSING_STATUS        = "add people processing"
  REMOVE_PEOPLE_SCHEDULED_STATUS      = "remove people scheduled"
  REMOVE_PEOPLE_PROCESSING_STATUS     = "remove people processing"
  CHANGE_ACTIVATION_SCHEDULED_STATUS  = "change activation scheduled"
  CHANGE_ACTIVATION_PROCESSING_STATUS = "change activation processing"

  ACTIVE_STATUS = "Active".freeze

  ###
  #
  # Validations
  #
  ###
  validates :name, :country_website_id, :created_by_id, :status, :offer_type, :start_date, :expiration_date, :date_constraint, :product_show_type, presence: true
  validates :join_plus_duration, presence: { if: :join_plus? }
  validates :join_plus_duration, numericality: { if: :join_plus? }

  validate :targeted_people_belong_to_same_country_website
  validate :targeted_products_belong_to_same_country_website

  validates :join_token, uniqueness: true, if: :offer_type_new?, allow_nil: true

  enum offer_type: { new: "new", existing: "existing", country_targeted: "country_targeted" }, _prefix: true

  scope :status_active, -> { where(status: "Active") }
  scope :active, -> { status_active.where("now() >= start_date && now() <= expiration_date") }

  def targeted_people_belong_to_same_country_website
    if TargetedPerson
       .joins(:person)
       .where("targeted_offer_id = ? AND people.country_website_id != ?", id, country_website_id)
       .count.positive?

      errors.add(:country_website, I18n.t("models.promotion.must_be_the_same_as_all_people_already_targeted"))
    end
  end

  def targeted_products_belong_to_same_country_website
    if TargetedProduct
       .joins(:product)
       .where("targeted_offer_id = ? AND products.country_website_id != ?", id, country_website_id)
       .count.positive?

      errors.add(:country_website, I18n.t("models.promotion.cannot_be_the_same_as_the_targeted_offer"))
    end
  end

  ###
  #
  # Callbacks
  #
  ###
  before_create :create_join_token
  before_update :check_activation_change
  after_update  :process_activation_change, if: -> { activation_changed }
  after_save    :manage_cms_files

  ###
  #
  # Class Methods
  #
  ###
  def self.active_offers_for_person_and_products(person, products_to_find)
    product_ids = []
    products_to_find.map { |p| product_ids << [p.id] }
    params = { person_id: person.id, product_ids: product_ids.flatten.uniq, created_on: person.created_on }

    select("DISTINCT targeted_offers.*")
      .joins("inner join targeted_products tp on tp.targeted_offer_id=targeted_offers.id
           inner join targeted_people tpe on tpe.targeted_offer_id=targeted_offers.id")
      .where("#{ACTIVE_CONDITIONS}
          AND tpe.person_id = :person_id AND tp.product_id IN (:product_ids)",
             params)
  end

  def self.add_person_with_join_token(person, token)
    targeted_offer = find_active_offer_by_token(token)
    targeted_person = targeted_offer.add_person(person) if targeted_offer

    targeted_person
  end

  def self.find_active_offer_by_token(token)
    where(join_token: token, status: "Active").first
  end

  # This query pulls the applicable targeted offer for the customer and item provided.
  # Doing this in one query eliminates several round trips and comparative logic necessary if you pull a customer's
  # complete list of offers and test for validity in a loop or other mechanism.
  def self.find_offer_by_person_and_item(person, item, product_type)
    params = {
      person_id: person.id,
      applies_to_product: item.class.name,
      created_on: person.created_on,
      product_type: product_type,
      country_website_id: person.country_website_id
    }

    targeted_product_offer = TargetedProductStoreService.fetch_offer(params, item)
    return targeted_product_offer if targeted_product_offer.present?

    joins(
      "inner join targeted_products tp on tp.targeted_offer_id=targeted_offers.id
          inner join targeted_people tpe on tpe.targeted_offer_id=targeted_offers.id
          inner join products p on p.id=tp.product_id"
    )
      .where(["#{ACTIVE_CONDITIONS} AND tpe.person_id = :person_id AND p.applies_to_product = :applies_to_product and p.product_type = :product_type", params])
      .where(
        "NOT EXISTS (:active_targeted_product_store_offer)",
        active_targeted_product_store_offer: TargetedProductStore
          .joins(targeted_product: [:targeted_offer])
          .select("1")
          .where("tp.id = targeted_product_stores.targeted_product_id")
          .where(targeted_offers: { status: TargetedOffer::ACTIVE_STATUS })
      )
      .first
  end

  def targeted_offer_stores_ids
    targeted_product_stores.pluck(:store_id).uniq
  end

  def self.for_person(person, complete_history = false)
    find_offers_by_person(person, complete_history)
  end

  def self.for_person_and_item(person, item, product_type = Product::AD_HOC)
    find_offer_by_person_and_item(person, item, product_type)
  end

  def self.offer_data_by_join_token(token)
    offer = find_active_offer_by_token(token)
    data = { targeted_offer: offer, error_message: nil }

    if offer && offer.inactive?
      data[:error_message] = "The offer you're looking for expired on #{offer.expiration_date.strftime('%B %d, %Y')}."
    elsif offer && offer.population_cap_exceeded?
      data[:error_message] = "Sorry. All spaces for this special offer have already been taken."
    end

    data
  end

  def self.find_offers_by_person(person, complete_history)
    if complete_history == false
      params = { person_id: person.id, created_on: person.created_on }
      joins("INNER JOIN targeted_people tpe ON tpe.targeted_offer_id=targeted_offers.id")
        .where("#{ACTIVE_CONDITIONS} AND tpe.person_id = :person_id", params)
        .order(expiration_date: :desc)
    else
      params = { person_id: person.id }
      joins("INNER JOIN targeted_people tpe ON tpe.targeted_offer_id=targeted_offers.id")
        .where("tpe.person_id = :person_id", params)
        .order(expiration_date: :desc)
    end
  end

  ###
  #
  # Instance Methods
  #
  ###
  def used_by?(person)
    targeted_person = targeted_people.where("person_id = ?", person).first
    (!targeted_person.nil? && targeted_person.has_used?)
  end

  def expiration_date_for_person(person)
    case date_constraint
    when "join plus"
      (person.created_on + join_plus_duration.days)
    when "expiration"
      expiration_date
    when "permanent"
      nil
    end
  end

  #
  # Return whether or not this targeted offer can be destroyed
  #
  def can_destroy?
    # Can never delete a targeted offer that has be used, can never delete an active offer
    status != "Active" && targeted_people.where("usage_count > 0").none?
  end

  #
  # Clones targeted offer excluding population criteria
  #
  def dup
    # Shallow clone
    @new = super()
    @new.population_criteria_count = 0
    @new.join_token                = nil
    @new.targeted_population_count = 0
    @new.status                    = "New"
    @new.name                      = "#{@new.name} - cloned"
    @new.job_status = nil

    # Clone products
    targeted_products.each do |tp|
      new_tp                 = tp.dup
      new_tp.targeted_offer  = @new
      @new.targeted_products << new_tp
    end

    # Clone Advertisements
    targeted_advertisements.each do |ta|
      new_ta                       = ta.dup
      new_ta.targeted_offer        = @new
      @new.targeted_advertisements << new_ta
    end

    @new.created_at = Time.now
    @new.updated_at = Time.now

    @new.save!
    @new
  end

  #
  # Validates the destroy of a targeted offer
  #
  def destroy
    super() if can_destroy?
  end

  # The product_show_type attribute governs what products are listed for the targeted offer
  # * price_override      = list all defaults for the passed in item, adjust price according to pricing adjustment rules
  # * show_only_selected  = list only the products selected and adjust the prices according to price adjustment rules
  # * add_to_defaults     = include non-default products along with the default products offered, adjust prices according to pricing rules
  def product_list(item, country_website_id, product_type)
    product_list = []
    case product_show_type
    when "price_override"
      default_products = Product.default_products_for(item, country_website_id, product_type)
      tp = targeted_products.includes(:targeted_offer)

      default_products.each do |product|
        t_product = tp.find { |tar| tar.product_id == product.id }
        product.set_targeted_product(t_product) if t_product
        product.set_adjusted_price(product.calculate_price(item))
        product_list << product
      end

    when "show_only_selected"
      params = { product_type: product_type, applies_to_product: item.class.name, country_website_id: country_website_id }
      t_products = targeted_products.includes(:product, :targeted_offer).where(" applies_to_product = :applies_to_product AND products.status='Active' AND products.country_website_id = :country_website_id", params).where({ products: { product_type: params[:product_type] } }).order("targeted_products.sort_order, products.sort_order")

      t_products.each do |tp|
        tp.product.set_targeted_product(tp)
        tp.product.set_adjusted_price(tp.product.calculate_price(item))
        product_list << tp.product
      end
    end

    product_list
  end

  def ready_to_activate?
    (new? || inactive?) &&
      targeted_products.present? &&
      (!offer_type_existing? || population_criteria_ready?)
  end

  def new?
    status == "New"
  end

  def inactive?
    status == "Inactive"
  end

  def active?
    status == "Active" && between_start_and_expiration?
  end

  def active_for_person?(person)
    status == "Active" && !expired?(person)
  end

  def add_person(person)
    return unless active? && !population_cap_exceeded?

    # TO-DO We need to alert when a targeted offer failed to be tied to a person but not raise an exception
    # since this affect the sign-up flow
    TargetedPerson.create(targeted_offer: self, person: person)
  end

  def remove_person(person)
    targeted_person = TargetedPerson.where(targeted_offer_id: id, person_id: person.id).first
    targeted_person.destroy if targeted_person && !targeted_person.has_used?
  end

  def export_people_to_csv(csv = nil)
    csv = CSV.new(response = "", row_sep: "\r\n") if csv.blank?
    csv << TargetedPerson::EXPORT_FILE_HEADERS

    targeted_people.includes(:person).find_each do |targeted_person|
      targeted_person.to_csv(csv, include_headers: false)
    end

    response
  end

  def limited_use?
    !usage_limit.zero?
  end

  def single_use?
    usage_limit == 1
  end

  def person_has_used_limit?(person)
    targeted_people = self.targeted_people.where(person_id: person.id)
    if targeted_people.blank?
      return true
    end # return true if the person is not a valid person in this targeted offer to avoid false positive
    return false unless limited_use?

    targeted_people.detect { |tp| tp.usage_count >= usage_limit }.present?
  end

  def join_plus?
    date_constraint == "join plus"
  end

  def cms_page_title
    if include_sidebar_cms?
      "Targeted Offer #{id}"
    else
      "N/A"
    end
  end

  def scheduled_or_processing?
    job_status.present?
  end

  def schedule_add_people_job(user)
    if !scheduled_or_processing?
      update_attribute(:job_status, ADD_PEOPLE_SCHEDULED_STATUS)
      TargetedOffer::AddTargetedPeopleWorker.perform_async(id, user.id)
      true
    else
      false
    end
  end

  def schedule_change_activation_job(activate, user)
    if !scheduled_or_processing?
      update_attribute(:job_status, CHANGE_ACTIVATION_SCHEDULED_STATUS)
      TargetedOffer::ChangeActivationWorker.perform_async(id, activate, user.id)
      true
    else
      false
    end
  end

  def schedule_remove_people_job(user)
    if !scheduled_or_processing?
      update_attribute(:job_status, REMOVE_PEOPLE_SCHEDULED_STATUS)
      TargetedOffer::RemoveTargetedPeopleWorker.perform_async(id, user.id)
      true
    else
      false
    end
  end

  def sidebar_cms_page_name
    "targeted_offer_#{id}" if include_sidebar_cms?
  end

  ###
  # Population Builder Functions
  ###

  def add_people_from_criteria(rebuild = false)
    exclude_current_people_sql = ""

    if !offer_type_new? && status != "Active"
      number_of_customers = customers_to_add

      # If we're rebuilding trash the current people
      if rebuild
        TargetedPerson.where(targeted_offer_id: id, usage_count: 0).delete_all
      elsif targeted_people.size.positive?
        exclude_current_people_sql = " and p.id not in (#{targeted_people.collect(&:person_id).join(',')})"
      end

      build_population_criteria

      if number_of_customers == "all"
        limit_sql = ""
        mod_sql = ""
      else
        limit_sql = " LIMIT #{number_of_customers.to_i}"

        # randomize the selection of individuals
        mod_sql = " AND p.id % 2 = 0 " if population_criteria_count / 2 > number_of_customers.to_i
      end

      sql = %Q|INSERT INTO targeted_people (targeted_offer_id, person_id, created_at)
                    (SELECT #{id}, id, NOW() FROM (
                    select DISTINCT p.id, p.name, p.country, p.zip, p.referral, p.created_on, MAX(paid_at) as last_purchase_date
                    FROM people p
                    LEFT JOIN purchases pu on pu.person_id=p.id
                    #{population_where_sql} #{exclude_current_people_sql} #{mod_sql} GROUP BY p.id
                    #{population_having_sql}#{limit_sql}) p)|

      # In order to optimize the insert of so many records into a population
      # we need to issue an insert statement directly to the db.  And in order to
      # use Rails sanitization technique, we have to skirt the protected status of the
      # sanitize_sql_array method and to do that, we use the send method.
      sanitized_sql = TargetedOffer.send(:sanitize_sql_array, [sql, population_params])

      self.class.connection.execute(sanitized_sql)
      count = self.class.connection.execute("select count(*) from targeted_people where targeted_offer_id=#{id}").first[0]

      update_attribute(:targeted_population_count, count)

      true
    else
      false
    end
  end

  def add_to_pop_count(num)
    pop_count = reload.targeted_population_count || 0 # need to reload count in case it gets cached
    update_attribute(:targeted_population_count, pop_count + num)
  end

  def population_cap_exceeded?
    population_cap != nil && (targeted_people.count >= population_cap)
  end

  def population_criteria
    {
      country_website_id: country_website_id,
      country: country,
      zip: zip,
      join_date_from: created_on_start,
      join_date_to: created_on_end,
      referral: referral,
      has_never_made_a_purchase: has_never_made_a_purchase?,
      last_purchase_made_from: last_purchase_made_start,
      last_purchase_made_to: last_purchase_made_end
    }
  end

  def population_criteria_ready?
    ready = false
    population_criteria.each do |_att, val|
      return true if !val.nil? && val.present?
    end
  end

  def remove_from_pop_count(num)
    pop_count = reload.targeted_population_count || 0 # need to reload count in case it gets cached
    new_count = ((pop_count - num) >= 0) ? (pop_count - num) : 0 # ensure there are popcount is not negative
    update_attribute(:targeted_population_count, new_count)
  end

  def set_population_count_from_criteria
    return if offer_type_new?

    build_population_criteria

    pop_count = Person.count_by_sql(
      [
        %Q|SELECT count(*) FROM (
                                      select DISTINCT p.id, p.name, p.country, p.zip, p.referral, p.created_on, MAX(pu.paid_at) as last_purchase_date
                                      FROM people p
                                      LEFT JOIN purchases pu on pu.person_id=p.id
                                      #{population_where_sql} GROUP BY p.id
                                      #{population_having_sql}) p|,
        population_params
      ]
    )

    update_attribute(:population_criteria_count, pop_count)
  end

  def update_purchases
    person_ids = targeted_people.collect(&:person_id)

    targeted_products.each do |tp|
      purchases = Purchase.includes(:person, :related, :product).unpaid.where(["price_adjustment_histories_count = 0 and product_id = :product_id and person_id in(:person_ids)", { product_id: tp.product_id, person_ids: person_ids }])
      purchases.each(&:recalculate)
    end
  end

  def set_activation(activate = true)
    self.status =
      if activate
        "Active"
      else
        "Inactive"
      end
    save
  end

  def upload_csv(csv, filename)
    bucket = S3_CLIENT.buckets[TARGETED_OFFER_BUCKET_NAME]
    bucket = S3_CLIENT.buckets.create(TARGETED_OFFER_BUCKET_NAME) if bucket.blank?
    bucket.objects.create(filename, csv)
  end

  def add_csv_filename
    csv_filename("add")
  end

  def remove_csv_filename
    csv_filename("remove")
  end

  def csv_filename(identifier)
    "#{id}_#{identifier}_targeted_offer.csv"
  end

  def grab_list_from_s3(identifier)
    AwsWrapper::S3.read(
      bucket: TARGETED_OFFER_BUCKET_NAME,
      key: csv_filename(identifier)
    ).split("\n")
  end

  private

  #
  # Checks for a change in activation status and sets a flag so after update
  # can update the purchases
  #
  def check_activation_change
    # Did status change?
    return unless changes["status"]

    # Flag that the activation changes so after update can update purchases after the targeted offer has been saved
    self.activation_changed = (changes["status"][1] == "Active" || changes["status"][0] == "Active")
  end

  def create_join_token
    if offer_type_new?
      self.join_token = Digest::SHA1.hexdigest(Time.now.to_i.to_s + rand.to_s)[0..15] if join_token.blank?
    else
      self.join_token = nil
    end
  end

  def manage_cms_files
    if include_sidebar_cms?
      Content.create_cms_files("targeted_offer_#{id}")
    else
      Content.delete_cms_files("targeted_offer_#{id}")
    end
  end

  #
  # Called after update to update purchases if the activation status changed
  #
  def process_activation_change
    # We must call update purchases after the update so Purchase.recalculate
    # see's the targeted offer in the new activation state
    update_purchases if activation_changed
  end

  def build_population_criteria
    self.population_params = {}
    build_where_criteria_string
    build_having_criteria_string
  end

  def expired?(person)
    case date_constraint
    when "expiration"
      !between_start_and_expiration?
    when "permanent"
      false
    when "join plus"
      Time.now >= (person.created_on + join_plus_duration.days)
    end
  end

  def between_start_and_expiration?
    Time.now.to_date >= start_date.to_date && Time.now.to_date <= expiration_date.to_date
  end

  def build_having_criteria_string
    self.population_having_sql = assemble_sql([create_purchase_sql(false)])
    self.population_having_sql = "HAVING " + population_having_sql if population_having_sql.present?
  end

  def build_where_criteria_string
    self.population_where_sql = assemble_sql(["p.status = 'Active'", create_targeted_offers_sql, create_created_on_sql, create_general_demographics_sql, create_targeted_offer_exclusion_sql])
    self.population_where_sql = "WHERE (#{population_where_sql})" if population_where_sql.present?
  end

  def similar_active_permanent_and_expiration_offers
    params = { product_ids: targeted_products.collect(&:product_id) }
    TargetedOffer.select("DISTINCT targeted_offers.*").joins("inner join targeted_products tp on tp.targeted_offer_id=targeted_offers.id").where(
      [
        "targeted_offers.status='Active' AND
                             ((date_constraint = 'expiration' and CURDATE() >= DATE(targeted_offers.start_date) and CURDATE() <= DATE(targeted_offers.expiration_date)) OR (date_constraint = 'permanent'))
                              AND tp.product_id IN (:product_ids) AND targeted_offers.id <> #{id}",
        params
      ]
    )
  end

  def similar_active_join_plus_offers
    params = { product_ids: targeted_products.collect(&:product_id) }
    TargetedOffer.select("DISTINCT targeted_offers.*").joins("inner join targeted_products tp on tp.targeted_offer_id=targeted_offers.id").where("tp.product_id IN (:product_ids) AND targeted_offers.status='Active' AND date_constraint='join plus' AND targeted_offers.id <> #{id}", params)
  end

  def create_purchase_sql(_is_for_where = false)
    case purchase_criteria_type
    when "never_made"
      "last_purchase_date IS NULL"
    when "has_made"
      "last_purchase_date IS NOT NULL"
    when "date_range"
      if last_purchase_made_start != nil && last_purchase_made_end != nil
        self.population_params = population_params.merge({ last_purchase_made_start: last_purchase_made_start, last_purchase_made_end: last_purchase_made_end })
        "last_purchase_date >= :last_purchase_made_start AND last_purchase_date <= :last_purchase_made_end"
      elsif last_purchase_made_start != nil && last_purchase_made_start.nil?
        self.population_params = population_params.merge({ last_purchase_made_start: last_purchase_made_start })
        "last_purchase_date >= :last_purchase_made_start"
      elsif last_purchase_made_start.nil? && last_purchase_made_start != nil
        self.population_params = population_params.merge({ last_purchase_made_end: last_purchase_made_end })
        "last_purchase_date <= :last_purchase_made_end"
      else
        ""
      end
    when "n/a"
      ""
    end
  end

  def create_created_on_sql
    if created_on_start != nil && created_on_end != nil
      self.population_params = population_params.merge(
        {
          created_on_start: created_on_start,
          created_on_end: created_on_end
        }
      )
      "DATE(created_on) >= DATE(:created_on_start) AND created_on <= DATE(:created_on_end)"
    elsif created_on_start != nil && created_on_end.nil?
      self.population_params = population_params.merge({ created_on_start: created_on_start })
      "DATE(created_on) >= DATE(:created_on_start)"
    elsif created_on_start.nil? && created_on_end != nil
      self.population_params = population_params.merge({ created_on_end: created_on_end })
      "DATE(created_on) <= DATE(:created_on_end)"
    elsif created_on_start.nil? && created_on_end.nil?
      ""
    end
  end

  def create_general_demographics_sql
    return_sql = ""
    crit = { country_website_id: country_website_id, country: country, zip: zip, referral: referral }
    crit.each do |c|
      if c[1] != nil && c[1].present? && c[1] != "all"
        return_sql += "p.#{c[0]} = :#{c[0]} AND "
        self.population_params = population_params.merge({ c[0].to_sym => c[1] })
      end
    end

    remove_and_from_sql_end(return_sql)
  end

  def create_targeted_offers_sql
    other_offers     = similar_active_permanent_and_expiration_offers
    join_plus_offers = similar_active_join_plus_offers

    other_offers_sql     = "( p.id not in ( select tp2.person_id from targeted_people tp2 where tp2.targeted_offer_id in (:other_offers) ) )"
    join_plus_offers_sql = "( p.id not in ( select tp2.person_id from people p2 inner join targeted_people tp2 on tp2.person_id = p2.id inner join targeted_offers t2 on tp2.targeted_offer_id = t2.id where t2.id in (:join_plus_offers) and CURDATE() <= DATE(DATE_ADD(p2.created_on, INTERVAL t2.join_plus_duration DAY)) ) )"

    # Exclude people who are in other simlar active permanent and expiration offers and
    # Exclude people who are in active join plus offers within the current interval
    if !other_offers.empty? && !join_plus_offers.empty?
      self.population_params = population_params.merge({ other_offers: other_offers.map(&:id), join_plus_offers: join_plus_offers.map(&:id) })
      %Q|( #{other_offers_sql} and #{join_plus_offers_sql} )|

    elsif !other_offers.empty?
      self.population_params = population_params.merge({ other_offers: other_offers.map(&:id) })
      %Q|( #{other_offers_sql} )|
    elsif !join_plus_offers.empty?
      self.population_params = population_params.merge({ join_plus_offers: join_plus_offers.map(&:id) })
      %Q|( #{join_plus_offers_sql} )|
    end
  end

  #
  # Excludes targeted offers that are set up to be excluded
  #
  def create_targeted_offer_exclusion_sql
    return if targeted_offer_exclusions.blank?

    self.population_params = population_params.merge(
      { excluded_targeted_offer_ids: targeted_offer_exclusions.map(&:exclude_targeted_offer_id) }
    )

    "( p.id not in "\
    "( select tp2.person_id from targeted_people tp2 where tp2.targeted_offer_id in (:excluded_targeted_offer_ids) ))"
  end

  def assemble_sql(sql_array)
    assembled_sql = ""
    sql_array.each do |sql|
      assembled_sql += "#{sql} AND " if sql.present?
    end

    remove_and_from_sql_end(assembled_sql)
  end

  def remove_and_from_sql_end(sql)
    if sql.present?
      trim_length = (sql.length - 6)
      sql.slice(0..trim_length)
    else
      ""
    end
  end

  def get_inventory_types
    product_ids = []
    inventory_types = []
    targeted_products.map { |tp| product_ids << [tp.product_id] }

    product_rules = ProductItemRule.active.joins("inner join product_items pit on pit.id=product_item_rules.product_item_id").where("(parent_id IS NULL OR parent_id = 0) AND product_id IN (?)", product_ids.flatten.uniq)
    product_rules.map { |pr| inventory_types << [pr.inventory_type] }
    inventory_types.flatten.uniq
  end
end
