class Ringtone < Album
  include Tunecore::Albums::OneSong
  include Tunecore::Albums::MultipleCreatives

  RESTRICTED_GENRES = [
    9,      # Folk
    28,     # Americana
    31,     # Heavy Metal
    33,     # K-Pop
    35,     # Big Band
    36,     # Fitness & Workout
    37,     # High Classical
    38,     # Instrumental
    39      # Karaoke
  ]

  after_save :move_creatives_to_album

  validate :check_for_various_artists
  validates :language_code, presence: { message: I18n.t("models.album.choose_language") }

  self.inheritance_column = "album_type"

  # RAILS_5_UPGRADE: it is not best practice to override the iniitalize method of
  #  an AR object. Take a look at the song= definition in
  #  lib/tunecore/albums/one_song.rb and see if this can be replaced with a
  #  validation that a RingTone only has one song.
  def initialize(options = {})
    options ||= {}

    @song = options[:song] || Song.new
    super
    self.song = @song
  end

  def check_for_various_artists
    return unless creatives.any?

    creatives.each do |creative|
      if creative.name&.downcase&.match?(/various artist(s)?/)
        errors.add(:creatives, I18n.t("models.ringtone.cannot_be_various_artists"))
      end
    end
  end

  def ensure_takedown_genre
    pop_genre = Genre.available_ringtone_genres.find_by(name: "Pop")

    return if pop_genre.blank?

    self.primary_genre = pop_genre    if RESTRICTED_GENRES.include?(primary_genre_id)
    self.secondary_genre = pop_genre  if RESTRICTED_GENRES.include?(secondary_genre_id)
    save
  end

  private

  # This method only exists to override the valid_golive_date validation in Album (parent class), to just let it always
  # pass, since Ringtones do not have the timed release feature that requires a golive_date or timed_release_timing_scenario
  def valid_golive_date
    true
  end
end
