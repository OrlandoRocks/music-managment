# == Schema Information
# Schema version: 404
#
# Table name: trend_data
#
#  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
#  `provider_id` tinyint(3) unsigned NOT NULL COMMENT 'Foreign Key from providers table',
#  `sale_date` date NOT NULL COMMENT 'Date of sale',
#  `country_id` smallint(5) unsigned NOT NULL COMMENT 'Foreign Key from countries table',
#  `royalty_currency_id` char(3) NOT NULL DEFAULT '' COMMENT 'iso code of the currency',
#  `exchange_rate_applied` decimal(14,6) NOT NULL COMMENT 'Exchange rate applied when converted to USD',
#  `date_added` timestamp NULL DEFAULT NULL COMMENT 'Date data was added',
#

class TrendData < TrendsBase
  self.table_name = "trend_data"

  belongs_to :provider
  belongs_to :country

  has_many :trend_data_details

  validates :provider, :sale_date, :country, :royalty_currency_id, :exchange_rate_applied, :date_added, presence: true

  #
  # Returns all trend data for person in date range
  #  album - optional
  #  song  - optional
  #
  def self.all_trend_data(person, start_date, end_date, options = {})
    sql = %Q{
      SELECT td.*,
              a.name as album_name,
              a.album_type as album_type,
              s.name as song_name,
              s.tunecore_isrc as tunecore_isrc,
              s.optional_isrc as optional_isrc,
              if( a.optional_upc is NULL, a.tunecore_upc, a.optional_upc ) as album_upc,
              a.artist_name as artist_name,
              a.label_name as label_name,
              c.name as country_name,
              if(td.country_id = #{Country.where(iso_code: 'US').first.id}, zc.city_mixed_case, NULL) as city,
              if(td.country_id = #{Country.where(iso_code: 'US').first.id}, zc.state, NULL) as state,
              z.zip as zip,
              tdd.qty as quantity,
              trans_types.name as trans_type_name
      FROM (select trend_data.*  from trend_data where sale_date between ? and ? ) td

              INNER JOIN ( SELECT albums.id, albums.name, albums.album_type, l.name as label_name,
                            tunecore_upc.number as tunecore_upc,
                            optional_upc.number as optional_upc,
                            if( albums.is_various = 1 , "Various Artists", GROUP_CONCAT( ar.name SEPARATOR " & " )) as artist_name
                           FROM #{TUNECORE_DB}.albums
                           LEFT JOIN #{TUNECORE_DB}.creatives cr on cr.creativeable_id = albums.id and cr.creativeable_type = 'Album' and cr.role = 'primary_artist'
                           LEFT JOIN #{TUNECORE_DB}.artists ar on ar.id = cr.artist_id
                           LEFT JOIN #{TUNECORE_DB}.upcs as tunecore_upc on tunecore_upc.upcable_type = 'Album' and tunecore_upc.upcable_id = albums.id and tunecore_upc.inactive = 0 and tunecore_upc.upc_type = 'tunecore'
                           LEFT JOIN #{TUNECORE_DB}.upcs as optional_upc on optional_upc.upcable_type = 'Album' and optional_upc.upcable_id = albums.id and optional_upc.inactive = 0 and optional_upc.upc_type = 'optional'
                           LEFT JOIN #{TUNECORE_DB}.labels l on albums.label_id = l.id
                           WHERE albums.person_id = ?
                           GROUP BY albums.id ) a
              INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
              INNER JOIN trans_types on trans_types.id = tdd.trans_type_id
              INNER JOIN #{TUNECORE_DB}.countries c on c.id = td.country_id
              LEFT JOIN #{TUNECORE_DB}.songs s on s.id = tdd.song_id
              LEFT JOIN zips z on z.id = tdd.zip_id
              LEFT JOIN zipcodes zc on zc.zipcode = z.zip5 and zc.primary_record='P'
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        WHERE #{song_album_query.join(' AND ')}
      )
    end

    sql_array = [sql, start_date, end_date, person.id]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    data  = TrendData.find_by_sql(sql_array)
    count = TrendData.all_trend_data_count(person, start_date, end_date, options)

    return data, count
  end

  #
  # Returns count of trend data for person in time period
  #  album - optional
  #  song  - optional
  #
  def self.all_trend_data_count(person, start_date, end_date, options = {})
    conditions_array = [
      "a.person_id = ? and trend_data.sale_date >= ? and trend_data.sale_date <= ? #{if options[:album]
                                                                                       'and a.id = ?'
                                                                                     end} #{if options[:song]
                                                                                              'and s.id = ?'
                                                                                            end}",
      person.id,
      start_date,
      end_date
    ]
    conditions_array << options[:album].id if options[:album]
    conditions_array << options[:song].id if options[:song]

    sql = %Q{
      SELECT count(distinct tdd.id)
      FROM (select trend_data.id from trend_data where sale_date between ? and ? ) td
      INNER JOIN ( select albums.id from #{TUNECORE_DB}.albums where albums.person_id = ? ) a
      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        WHERE #{song_album_query.join(' AND ')}
      )
    end

    sql_array = [sql, start_date, end_date, person.id]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = TrendData.connection.execute(send(:sanitize_sql_array, sql_array)).first
    results[0].to_i if results
  end

  def self.last_trend_date(user)
    sql = %Q{
      SELECT max(td.sale_date) as last_trend_date
      FROM (select albums.id from #{TUNECORE_DB}.albums where person_id = ? ) a
      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id
      INNER JOIN trend_data td on td.id = tdd.trend_data_id}

    results = connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, user.id])).each(as: :hash)[0]
    results["last_trend_date"]
  end

  #
  # Returns top releases, with units sold/streamed
  #
  def self.top_releases(user, start_date, end_date, _options = {})
    sql = %Q{
      SELECT a.id,
             a.name,
             a.album_type,
             a.artist_name as artist_name,
             sum( if( tdd.trans_type_id = #{TransType.album_download.id}, tdd.qty,  0 )) as album_qty,
             sum( if( tdd.trans_type_id = #{TransType.song_download.id}, tdd.qty, 0)) as song_qty,
             sum( if( tdd.trans_type_id = #{TransType.stream.id}, tdd.qty, 0)) as stream_qty,
             sum( if( tdd.trans_type_id = #{TransType.album_download.id}, tdd.qty, if( tdd.trans_type_id = #{TransType.song_download.id}, .1*tdd.qty, 0))) as rank

      FROM (SELECT trend_data.id from trend_data where sale_date between ? and ? ) td

      INNER JOIN ( SELECT albums.id, albums.name, albums.album_type, if( albums.is_various = 1 , "Various Artists", GROUP_CONCAT( ar.name SEPARATOR " & " )) as artist_name
                   FROM #{TUNECORE_DB}.albums
                   LEFT JOIN #{TUNECORE_DB}.creatives cr on cr.creativeable_id = albums.id and cr.creativeable_type = 'Album' and cr.role = 'primary_artist'
                   LEFT JOIN #{TUNECORE_DB}.artists ar on ar.id = cr.artist_id
                   WHERE albums.person_id = ?
                   GROUP BY albums.id ) a

      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
      WHERE tdd.trans_type_id IN (#{TransType.album_download.id}, #{TransType.song_download.id}, #{TransType.stream.id})
      GROUP BY a.id
    }

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, start_date, end_date, user.id])).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end

    results.sort { |a, b| (b["rank"].to_f == a["rank"].to_f) ? a["name"] <=> b["name"] : b["rank"].to_f <=> a["rank"].to_f }
  end

  #
  # Returns top songs
  #   album - optional
  #
  def self.top_songs(user, start_date, end_date, options = {})
    sql = %Q{
        SELECT songs.id, songs.name, a.id as album_id, a.name as album_name, a.album_type, a.artist_name, sum(tdd.qty) as qty
        FROM (select trend_data.id from trend_data where sale_date between ? and ? ) td
        INNER JOIN ( SELECT albums.id, albums.name, albums.album_type, if( albums.is_various = 1 , "Various Artists", GROUP_CONCAT( ar.name SEPARATOR " & " )) as artist_name
                     FROM #{TUNECORE_DB}.albums
                     LEFT JOIN #{TUNECORE_DB}.creatives cr on cr.creativeable_id = albums.id and cr.creativeable_type = 'Album' and cr.role = 'primary_artist'
                     LEFT JOIN #{TUNECORE_DB}.artists ar on ar.id = cr.artist_id
                     WHERE albums.person_id = ?
                     GROUP BY albums.id, albums.name, albums.album_type ) a
        INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
        INNER JOIN #{TUNECORE_DB}.songs on songs.id = tdd.song_id
        WHERE tdd.trans_type_id IN (#{TransType.album_download.id}, #{TransType.song_download.id}, #{TransType.stream.id})
        }

    sql += "AND a.id = ?" if options[:album]

    sql += %Q(
      GROUP BY songs.id, songs.name
      ORDER BY qty DESC, songs.name ASC
    )

    sql_array = [sql, start_date, end_date, user.id]
    sql_array << options[:album].id if options[:album]

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  #
  # Returns top countries
  #  album - optional
  #  song - optional
  #
  def self.top_countries(user, start_date, end_date, options = {})
    sql = %Q{
      SELECT countries.id, countries.name, countries.iso_code, sum(tdd.qty) as qty
      FROM (select trend_data.id, trend_data.country_id  from trend_data where sale_date between ? and ? ) td
          INNER JOIN ( select albums.id, albums.name, albums.album_type from #{TUNECORE_DB}.albums where albums.person_id = ? ) a
          INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
          INNER JOIN #{TUNECORE_DB}.countries on countries.id = td.country_id
        }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        WHERE #{song_album_query.join(' AND ')}
      )
    end

    sql += %Q(
        GROUP BY countries.id, countries.name, countries.iso_code
        ORDER BY qty DESC, countries.name ASC, countries.iso_code ASC;)

    sql_array = [sql, start_date, end_date, user.id]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  #
  # Returns top ringtones
  #
  def self.top_ringtones(user, start_date, end_date, _options = {})
    sql = %Q{
      SELECT a.id,
             a.name,
             a.album_type,
             a.artist_name as artist_name,
             sum( tdd.qty ) as ringtone_qty

      FROM (SELECT trend_data.id from trend_data where sale_date between ? and ? ) td
      INNER JOIN ( SELECT albums.id, albums.name, albums.album_type, if( albums.is_various = 1 , "Various Artists", GROUP_CONCAT( ar.name SEPARATOR " & " )) as artist_name
                   FROM #{TUNECORE_DB}.albums
                   LEFT JOIN #{TUNECORE_DB}.creatives cr on cr.creativeable_id = albums.id and cr.creativeable_type = 'Album' and cr.role = 'primary_artist'
                   LEFT JOIN #{TUNECORE_DB}.artists ar on ar.id = cr.artist_id
                   WHERE albums.person_id = ?
                   GROUP BY albums.id, albums.name, albums.album_type ) a

      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
      WHERE tdd.trans_type_id = #{TransType.ringtone.id}
      GROUP BY a.id, a.name
      ORDER BY ringtone_qty DESC, a.name ASC
    }

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, start_date, end_date, user.id])).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  #
  # Returns top us markets with units sold/streamed
  #   album - optional
  #   song  - optional
  #
  def self.top_us_markets(user, start_date, end_date, options = {})
    sql = %Q{
      SELECT zipcodes.cbsa_short_name, zipcodes.cbsa_name, sum(tdd.qty) as qty
      FROM (select trend_data.id from trend_data where sale_date between ? and ?) td
      INNER JOIN ( select albums.id, albums.name, albums.album_type from #{TUNECORE_DB}.albums where albums.person_id = ? ) a
      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
      INNER JOIN zips on zips.id = tdd.zip_id
      INNER JOIN zipcodes on zips.zip5 = zipcodes.zipcode and zipcodes.primary_record = 'P'
      WHERE zipcodes.cbsa_short_name is not null and zipcodes.cbsa_short_name != ""
    }

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        AND #{song_album_query.join(' AND ')}
      )
    end

    sql += %Q(
      GROUP BY zipcodes.cbsa
      ORDER BY qty DESC, zipcodes.city ASC, zipcodes.state ASC;
    )

    sql_array = [sql, start_date, end_date, user.id]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = []
    connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array)).each(as: :hash) do |result|
      results << ActiveSupport::HashWithIndifferentAccess.new(result)
    end
    results
  end

  #
  # Returns quantity counts of trend data for person in date range grouped by sale date
  #   album  - optional
  #   song   - optional
  #
  def self.trend_data_counts_by_day(person, start_date, end_date, options = {})
    sql = %Q{
      SELECT tdd.trans_type_id, td.sale_date,  sum(tdd.qty) as quantity
      FROM (select trend_data.id, trend_data.sale_date from trend_data where sale_date between ? and ? ) td
      INNER JOIN ( select albums.id from #{TUNECORE_DB}.albums where albums.person_id = ? ) a
      INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id}

    # Add song album where criteria
    song_album_query = []
    song_album_query << "tdd.album_id = ?" if options[:album]

    song_album_query << "tdd.song_id = ?" if options[:song]

    unless song_album_query.empty?
      sql += %Q(
        WHERE #{song_album_query.join(' AND ')}
      )
    end

    sql += %Q(
      group by tdd.trans_type_id, td.sale_date
      ORDER BY tdd.trans_type_id ASC, td.sale_date ASC
    )

    sql_array = [sql, start_date, end_date, person.id]
    sql_array << options[:album].id if options[:album]
    sql_array << options[:song].id if options[:song]

    results = []
    connection.execute(send(:sanitize_sql_array, sql_array)).each(as: :hash) do |hash|
      results << ActiveSupport::HashWithIndifferentAccess.new(hash)
    end

    results
  end

  def self.last_trend_import
    sql = %Q{
      SELECT max(date_added) as last_trend_import
      FROM trend_data
    }

    results = connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql])).each(as: :hash)[0]
    results["last_trend_import"]
  end

  def self.ringtones_sold?(person, start_date, end_date)
    sql = %Q{
       SELECT max(trend_data_id) as last_ringtone_trend_id
       FROM ( select albums.id from #{TUNECORE_DB}.albums where albums.person_id = ? and albums.album_type = "Ringtone" ) a
       INNER JOIN ( select trend_data.id from trend_data where sale_date between ? and ? ) td
       INNER JOIN trend_data_detail tdd on tdd.album_id = a.id and td.id = tdd.trend_data_id
    }

    sql_array = [sql, person.id, start_date, end_date]

    result = connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, [sql, person.id, start_date, end_date])).each(as: :hash)[0]
    result["last_ringtone_trend_id"].present?
  end
end
