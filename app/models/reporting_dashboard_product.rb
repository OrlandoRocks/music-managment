class ReportingDashboardProduct < ApplicationRecord
  belongs_to :reporting_date
  belongs_to :product
  belongs_to :country_website

  default_scope { order("report_date ASC") }

  def self.calculate_daily_values(for_date = Date.today)
    rep_date = ReportingDate.find_by(reporting_date: for_date)

    Product.all.find_each do |product|
      row = where(report_date: for_date, product_id: product.id).first_or_create
      row.update(
        {
          reporting_date: rep_date,
          report_date: for_date,
          country_website_id: product.country_website_id,
          product_id: product.id,
          product_name: product.name,
          product_family: product.product_family,
          product_count: Invoice.product_sales(for_date).find { |s| s.id == product.id }.try(:product_count) || 0,
          product_amount: Invoice.product_sales(for_date).find { |s| s.id == product.id }.try(:product_amount).try(:to_f) || 0.00
        }
      )
    end
  end

  def self.get_daily_values(start_date, end_date, country)
    get_data("day_display", "reporting_date", start_date, end_date, country)
  end

  def self.get_weekly_values(start_date, end_date, country)
    get_data("week_display", "week_year", start_date, end_date, country)
  end

  def self.get_monthly_values(start_date, end_date, country)
    get_data("month_display", "month_year", start_date, end_date, country)
  end

  def self.get_data(display_field, group_by_field, start_date, end_date, country)
    find_by_sql(["select rd.#{display_field}, product_id, product_name, product_family, sum(coalesce(product_count, 0)) product_count, sum(coalesce(product_amount, 0)) product_amount from reporting_dates rd left join reporting_dashboard_products rdp on rd.id = rdp.reporting_date_id where rdp.country_website_id = ? and rd.reporting_date >= ? and rd.reporting_date <= ? group by rd.#{group_by_field}, product_id, product_name, product_family", country, start_date, end_date])
  end
end
