class ScrapiJobDetail < ApplicationRecord
  serialize :artists, Array
  serialize :genres, Array

  belongs_to :scrapi_job
  has_one :song, through: :scrapi_job
end
