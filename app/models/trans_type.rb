class TransType < TrendsBase
  self.table_name = "trans_types"

  validates :name, presence: true

  @@album_download = nil
  @@song_download  = nil
  @@stream         = nil
  @@ringtone       = nil

  def self.album_download
    @@album_download ||= TransType.find_by(name: "Album")
  end

  def self.song_download
    @@song_download ||= TransType.find_by(name: "Song")
  end

  def self.stream
    @@stream ||= TransType.find_by(name: "Stream")
  end

  def self.ringtone
    @@ringtone ||= TransType.find_by(name: "Ringtone")
  end
end
