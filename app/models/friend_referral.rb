class FriendReferral < ApplicationRecord
  belongs_to :referrer, class_name: "Person"
  belongs_to :referee, class_name: "Person"
  belongs_to :posting, polymorphic: true
  belongs_to :invoice

  scope :by_date, ->(date) { where("updated_at > ?", date) }
  scope :pending_commissions, -> { where(status: %w[purchase pending]) }
  scope :by_user, ->(person_id) { where(referrer_id: person_id) }
  scope :not_posted, -> { where("status NOT IN ('invalid','denied','posted')") }

  FIRST_REFERRAL_BONUS_AMOUNT = 20

  def self.paid_referrals
    find_by_sql(
      "select referrer.id as 'referrer_id', referrer.name as 'referrer_name', referrer.email as 'referrer_email', referrer.created_on as 'referrer_created_on',
      referee.id as 'referee_id', referee.name as 'referee_name', referee.email as 'referee_email', referee.created_on as 'referee_created_on',
      fr.invoice_id, fr.commission, fr.balance, fr.created_at, purchases.cost_cents as 'purchase_cost_cents', purchases.discount_cents as 'puchase_discount_cents',
      purchases.currency as 'purchase_currency', purchases.paid_at as 'purchase_paid_at'
      from friend_referrals fr
      inner join people referrer
      on fr.referrer_id = referrer.id
      inner join people referee
      on fr.referee_id = referee.id
      inner join invoices
      on invoices.id = fr.invoice_id
      inner join purchases
      on purchases.invoice_id = invoices.id
      where fr.commission > 0
      order by created_at desc"
    )
  end

  def self.referees(referrer)
    select("people.name, created_at").where(referrer_id: referrer.id, status: %w[purchase pending approved denied posted]).includes(:referee)
  end

  def self.record_event(invoice, referee, campaign_hash, ip_address = nil)
    if is_qualifying_purchase?(invoice, referee)
      kickback = Tunecore::FriendReferral::Kickback.record_event(invoice, referee, campaign_hash, ip_address)
      referrer = check_referrer(kickback.referrer_email, kickback.referrer_id)

      if referrer
        status = "purchase"
      else
        status = "error"
        error_message = "Referrer not found"
      end

      first_referral = (kickback.referrer_unique_referrals == 1) ? true : false

      create(
        referrer: referrer,
        referee: referee,
        commission: kickback.referrer_commission,
        balance: kickback.referrer_balance,
        invoice: invoice,
        raw_response: kickback.inspect,
        status: status,
        error_message: error_message,
        campaign_id: campaign_hash[:campaign_id],
        first_referral: first_referral
      )
    else
      campaign = FriendReferralCampaign.find_by(campaign_code: campaign_hash[:campaign_code])

      error_message = "non-qualifying purchase"
      person_id = campaign ? campaign.person_id : nil

      error_message << "; campaign_code of '#{campaign_hash[:campaign_code]}' not found in the system" unless campaign

      create(
        referrer_id: person_id,
        referee_id: referee.id,
        invoice: invoice,
        campaign_id: campaign_hash[:campaign_id],
        status: "invalid",
        error_message: error_message
      )
    end
  end

  def self.batch_process_commissions(start_date = nil, person_id = nil)
    referrals =
      if person_id
        not_posted.by_user(person_id)
      else
        start_date ? pending_commissions.by_date(start_date) : pending_commissions
      end

    processed_referrals = []
    referrals.each do |referral|
      page = 1
      loop do
        commissions = Tunecore::FriendReferral::Kickback.get_commissions(referral.referrer, page)
        page += 1
        break if (commissions.commission(referral.invoice_id) || commissions.commissions.empty?)
      end

      status = commissions.get_status(referral.invoice_id)
      amount = commissions.get_amount(referral.invoice_id)

      case status
      when "approved"
        referral.update_approved_commission(amount)
        referral.post_commission!
      when "denied"
        referral.update(status: "denied", error_message: "Commission is denied via getAmbassador")
      when "pending"
        referral.update(status: "pending", error_message: nil)
      else
        referral.update(status: "error", error_message: "Could not retrieve status from getAmbassador", raw_response: commissions.inspect)
      end

      processed_referrals << referral
    end

    processed_referrals
  end

  def post_commission!(comment = nil)
    return false if status != "approved"
    return unless referrer

    comment = "Friend Referral Payment for #{referee.name}"
    adjustment, message = post_adjustment(referrer, commission, comment)
    if adjustment
      update(status: "posted", posting: adjustment)
    else
      update(status: "posting_failed", error_message: message)
    end
  end

  def update_approved_commission(amount)
    update(status: "approved", commission: amount, balance: balance + amount)
    adjust_for_first_referral_bonus if first_referral
  end

  private

  def self.check_referrer(email, id)
    Person.find_by(email: email, id: id)
  end

  def self.is_qualifying_purchase?(invoice, person)
    valid_product_ids = [
      Product.find_products_for_country(person.country_domain, :distribution),
      Product.find_products_for_country(person.country_domain, :songwriter_service),
      Product.find_products_for_country(person.country_domain, :video_distribution)
    ].flatten

    purchased_valid_products_before = Invoice
                                      .paid
                                      .joins(:purchases)
                                      .where(person_id: invoice.person_id)
                                      .where(purchases: { product_id: valid_product_ids })
                                      .where.not(id: invoice.id)
                                      .present?

    purchasing_valid_products = invoice.purchases.any? { |p| valid_product_ids.include?(p.product_id) }

    if !purchased_valid_products_before && purchasing_valid_products
      true
    else
      false
    end
  end

  def post_adjustment(person, commission, comment = nil)
    comment ||= "Friend Referral Payment"
    person_transaction = nil
    begin
      person_transaction = PersonTransaction.create!(
        person: person,
        credit: commission,
        target: self,
        comment: comment
      )
    rescue StandardError => e
      return person_transaction, e.message
    end

    person_transaction
  end

  def adjust_for_first_referral_bonus
    bonus =  FIRST_REFERRAL_BONUS_AMOUNT
    kickback = Tunecore::FriendReferral::Kickback.add_balance(referrer, bonus)

    self.raw_response = "#{raw_response}\n#{kickback.inspect}"
    if (kickback.balance["new"].to_f - kickback.balance["old"].to_f) == bonus
      self.commission = commission + bonus
      # this balance could be different from what's returned from ambassador, since we adjust for bonus
      self.balance = balance + bonus
    else
      self.error_message = "Failed to add bonus"
      self.status = "error"
    end

    save
  end
end
