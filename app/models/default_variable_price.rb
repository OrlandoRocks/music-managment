class DefaultVariablePrice < ApplicationRecord
  belongs_to :variable_prices_store, foreign_key: "variable_price_store_id"
  belongs_to :store

  def variable_price
    variable_prices_store.variable_price if variable_prices_store
  end
end
