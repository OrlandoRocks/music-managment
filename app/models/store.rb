class Store < ApplicationRecord
  extend ArelTableMethods
  include ActionView::Helpers
  include CustomTranslationHelper

  # Prepare for safe migration that removes this column
  self.ignored_columns = %w[description]

  include Tunecore::DefaultVariablePriceMethods
  CONVERTER_MAP = {
    "Google" => "MusicStores::Google::Converter",
    "iTunesUS" => "MusicStores::Itunes::Converter",
    "iTunesAU" => "MusicStores::Itunes::Converter",
    "iTunesCA" => "MusicStores::Itunes::Converter",
    "iTunesEU" => "MusicStores::Itunes::Converter",
    "iTunesJP" => "MusicStores::Itunes::Converter",
    "iTunesUK" => "MusicStores::Itunes::Converter",
    "iTunesMX" => "MusicStores::Itunes::Converter",
    "iTunesLA" => "MusicStores::Itunes::Converter",
    "iTunesMatc" => "MusicStores::Itunes::Converter",
    "iTunesPA" => "MusicStores::Itunes::Converter",
    "iTunesWW" => "MusicStores::Itunes::Converter",
    "Amazon" => "MusicStores::Amazon::Converter",
    "RhapsodyRH" => "MusicStores::Rhapsody::Converter",
    "GroupieTun" => "MusicStores::GroupieTunes::Converter",
    "Streaming" => "MusicStores::Streaming::Converter",
    "AmazonOD" => "MusicStores::Amazonod::Converter",
    "Zune" => "MusicStores::Zune::Converter",
    "Medianet" => "MusicStores::Medianet::Converter",
    "Cur" => "MusicStores::Medianet::Converter",
    "Spotify" => "MusicStores::Spotify::Converter",
    "Deezer" => "MusicStores::Deezer::Converter",
    "Muve" => "MusicStores::Muve::Converter",
    "Slacker" => "MusicStores::Slacker::Converter",
    "Wimp" => "MusicStores::Wimp::Converter",
    "Gracenote" => "MusicStores::Gracenote::Converter",
    "Shazam" => "MusicStores::Shazam::Converter",
    "7Digital" => "MusicStores::SevenDigital::Converter",
    "YTMusic" => "MusicStores::YoutubeMusic::Converter",
    "Jbhifi" => "MusicStores::Jbhifi::Converter",
    "YoutubeSR" => "MusicStores::YoutubeSr::Converter",
    "KKBox" => "MusicStores::Kkbox::Converter",
    "Akazoo" => "MusicStores::Akazoo::Converter",
    "Anghami" => "MusicStores::Anghami::Converter",
    "Facebook" => "MusicStores::Facebook::Converter",
    "Spinlet" => "MusicStores::Spinlet::Converter",
    "Neurotic" => "MusicStores::NeuroticMedia::Converter",
    "SimfyAF" => "MusicStores::SimfyAfrica::Converter",
    "TouchTunes" => "DistributionSystem::DDEX::Converter",
    "FBTracks" => "DistributionSystem::TrackMonetization::Converter",
    "SCblock" => "DistributionSystem::TrackMonetization::Converter"
  }.freeze

  STORE_INFO_LINKS = {
    "ww" => "itunes-worldwide-description",
    "us" => "itunes-worldwide-description",
    "az" => "amazon-music-description",
    "sp" => "spotify-description",
    "mn" => "medianet-description",
    "em" => "emusic-description",
    "zn" => "groove-description",
    "rh" => "napster-description",
    "mix" => "mixradio-description",
    "tp" => "iheart-radio-description",
    "gp" => "vervelife-description",
    "aod_us" => "amazon-on-demand-description",
    "gm" => "google-play-description",
    "sma" => "simfy-africa-description",
    "dz" => "deezer-description",
    "mv" => "muve-music-description",
    "rd" => "rdio-description",
    "wi" => "tidal-description",
    "su" => "sony-music-description",
    "gn" => "gracenote-description",
    "sz" => "shazaam-description",
    "jk" => "juke-description",
    "sd" => "7digital-description",
    "jb" => "jb-hifi-description",
    "sk" => "slacker-description",
    "bm" => "bloom-description",
    "gvr" => "guvera-description",
    "akz" => "akazoo-description",
    "ang" => "anghami-description",
    "kkb" => "kkbox-description",
    "rvb" => "revibe-description",
    "spn" => "spinlet-description",
    "nrm" => "neurotic-media-description",
    "target" => "targetmusic-description",
    "yandex" => "yandex-description",
    "claro" => "claromsica-description",
    "ytm" => "youtube-art-tracks-description",
    "ringtone" => "ringtone-description",
    "playme" => "play-me-description",
    "zvooq" => "zvooq-description",
    "saavn" => "saavn-description",
    "nmusic" => "nmusic-description",
    "qsic" => "qsic-description",
    "cur" => "cur-description",
    "mload" => "musicload-description",
    "dmusic" => "kuack-description",
    "pndr" => "pandora-description",
    "sndxch" => "sound-exchange-description", # todo : needs to be updated!-description"
    "boom" => "boomplay-description",
    "ttunes" => "touchtunes-description",
    "fbt" => "facebook-tracks-description",
    "island" => "music-island-description",
    "tnct" => "tencent-description",
    "byda" => "bytedance-description",
    "uma" => "uma-description",
    "net" => "netease-description",
    "joox" => "joox-description",
    "tim" => "tim-description",
    "hung" => "hung-description",
    "zed" => "zed-description",
    "gaana" => "gaana-description",
    "go-yt" => "go-yt-description",
    "wkhg" => "wyhg-description",
    "mtn" => "mtn-description",
    "qob" => "qob-description",
    "scld" => "scld-description",
    "reels" => "reels-description",
    "wsng" => "wsng-description",
    "pel" => "pel-description",
    "dou" => "dou-description",
    "luna" => "luna-description"
  }.freeze

  TC_DISTRIBUTOR = "tc_distributor"
  TUNECORE = "tunecore".freeze
  BATCH_RETRY_DAILY_LIMIT_DEFAULT = 5000

  DOWNLOADS = "Downloads".freeze
  STREAMING = "Streaming".freeze
  CLOUD     = "Cloud".freeze
  PHYSICAL  = "Physical".freeze
  DISCOVERY = "Discovery".freeze
  CURATED   = "Curated".freeze
  FREE      = "Free".freeze
  SOCIAL    = "Social".freeze

  AOD_STORE_ID            = 19
  BELIEVE_STORE_ID        = 75
  FBTRACKS_STORE_ID       = 83
  FB_REELS_STORE_ID       = 100
  GOOGLE_STORE_ID         = 28
  IG_STORE_ID             = 84
  IHEART_RADIO_STORE_ID   = 25
  ITUNES_WW_ID            = 36
  NAPSTER_STORE_ID        = 7
  QOBUZ_STORE_ID          = 96
  SOUNDCLOUD_BLOCK_ID     = 99
  TENCENT_MUCOIN_STORE_ID = 86
  TIK_TOK_STORE_ID        = 87
  YTSR_PROXY_ID           = 105
  YTSR_STORE_ID           = 48
  SPOTIFY_STORE_ID        = 26
  SHAZAM_STORE_ID         = 42
  SIMFY_AFRICA_STORE_ID   = 31
  AYOBA_STORE_ID          = 64

  TRACK_MONETIZATION_STORE_MAP = {
    GOOGLE_STORE_ID => YTSR_STORE_ID,
    FB_REELS_STORE_ID => FBTRACKS_STORE_ID
  }

  BELIEVE_STORE_IDS = [73, 75].freeze
  FREEMIUM_STORES = [FB_REELS_STORE_ID].freeze
  BELIEVE_AND_FREEMIUM = FREEMIUM_STORES + BELIEVE_STORE_IDS

  DEPRECATED_ITUNES_STORES      = [1, 2, 3, 4, 5, 6, 22, 29, 30, 34].freeze
  DISCOVERY_PLATFORMS           = Store.where(discovery_platform: true).pluck(:id)
  DUAL_DELIVERY_STORE_IDS       = {
    FBTRACKS_STORE_ID => FB_REELS_STORE_ID,
    FB_REELS_STORE_ID => FBTRACKS_STORE_ID,
    YTSR_STORE_ID => GOOGLE_STORE_ID,
    GOOGLE_STORE_ID => YTSR_STORE_ID
  }.freeze
  EXCLUDED_FROM_MASS_RETRY      = [48, 73, 83].freeze
  FACEBOOK_AND_YOUTUBE_STORES   = [GOOGLE_STORE_ID, FB_REELS_STORE_ID]
  FREE_TO_ADD_STORES            = ["ig"].map(&:freeze).freeze
  ITUNES_STORE_IDS              = [1, 2, 3, 4, 5, 6, 22, 29, 30, 34, 36].freeze
  MASS_RETRY_SONG_STORES        = [48, 83].freeze
  MASS_TAKEDOWN_REJECTED_STORES = ["st", "gsd", "ytsr", "fbt", "ig"].map(&:freeze).freeze
  SONG_START_TIME_STORES        = [TIK_TOK_STORE_ID].freeze
  TRACK_MONETIZATION_STORES     = [48, 83].freeze

  BELIEVE_STORE_ABBREV = "bl".freeze
  GOOGLE_PLAY_ABBREV   = "gm".freeze
  YTM_STORE_ABBREV     = "ytm".freeze
  YTSR_STORE_ABBREV    = "yttm".freeze

  FACEBOOK_COUNTRY_EXCLUSIONS = ["XK"]

  STORE_DISTRO_TYPES = {
    "ww" => [DOWNLOADS, CLOUD],            # iTunes Worldwide
    "us" => [DOWNLOADS],                   # iTunes
    "au" => [DOWNLOADS],                   # iTunes Australia/N.Z.
    "ca" => [DOWNLOADS],                   # iTunes Canada
    "eu" => [DOWNLOADS],                   # iTunes UK/European Union
    "jp" => [DOWNLOADS],                   # iTunes Japan
    "mx" => [DOWNLOADS],                   # iTunes Mexico
    "la" => [DOWNLOADS],                   # iTunes Latin America (incl. Brazil)
    "pa" => [DOWNLOADS],                   # iTunes Asia
    "az" => [DOWNLOADS, CLOUD, STREAMING], # Amazon Music
    "sp" => [STREAMING],                   # Spotify
    "mn" => [DOWNLOADS, STREAMING],        # MediaNet
    "em" => [DOWNLOADS],                   # eMusic
    "zn" => [DOWNLOADS, STREAMING],        # Zune
    "rh" => [STREAMING],                   # Rhapsody
    "no" => [DOWNLOADS, STREAMING],        # Nokia
    "tp" => [STREAMING],                   # iHeartRadio
    "gp" => [DOWNLOADS, STREAMING],        # VerveLife
    "aod_us" => [PHYSICAL, DOWNLOADS], # Amazon On Demand
    "gm" => [DOWNLOADS, STREAMING], # Google Play
    "ringtone" => [DOWNLOADS], # iTunes Ringtone Store
    "sma" => [STREAMING, DOWNLOADS], # simfy africa
    "dz" => [DOWNLOADS, STREAMING],        # Deezer
    "mv" => [DOWNLOADS, STREAMING],        # Muve Music
    "rd" => [DOWNLOADS, STREAMING],        # Rdio
    "wi" => [DOWNLOADS, STREAMING],        # WiMP
    "su" => [STREAMING],                   # Sony Music Unlimited
    "gn" => [DISCOVERY],                   # Gracenote
    "sz" => [DISCOVERY],                   # Shazam
    "jk" => [DOWNLOADS, STREAMING],        # Juke
    "sd" => [DOWNLOADS, STREAMING],        # 7digital
    "jb" => [STREAMING],                   # JB HiFi
    "sk" => [STREAMING],                   # Slacker
    "ytm" => [STREAMING], # YouTube Music
    "bm" => [STREAMING], # Bloom
    "akz" => [DOWNLOADS, STREAMING],        # Akazoo
    "ang" => [DOWNLOADS, STREAMING],        # Anghami
    "kkb" => [DOWNLOADS, STREAMING],        # KKBox
    "rvb" => [STREAMING],                   # Revibe
    "spn" => [DOWNLOADS, STREAMING],        # Spinlet
    "nrm" => [DOWNLOADS, STREAMING],        # Neurotic Media
    "target" => [DOWNLOADS, STREAMING],        # TargetMusic
    "yandex" => [STREAMING],                   # Yandex
    "claro" => [DOWNLOADS, STREAMING], # Claromúsica
    "playme" => [DOWNLOADS, STREAMING], # Play.me
    "zvooq" => [STREAMING],                   # Zvooq
    "saavn" => [STREAMING],                   # saavn
    "nmusic" => [STREAMING], # NMusic
    "qsic" => [STREAMING], # Q.SIC
    "cur" => [STREAMING], # Cur
    "mload" => [DOWNLOADS], # Musicload
    "dmusic" => [STREAMING], # D’Music/Digicel
    "pndr" => [STREAMING, CURATED],          # Pandora
    "boom" => [DOWNLOADS, STREAMING],        # Boom
    "sndxch" => [DISCOVERY],                   # Sound Exchange
    "ttunes" => [DOWNLOADS, STREAMING],        # TouchTunes
    "island" => [DOWNLOADS, STREAMING],        # Music Island
    "tnct" => [DOWNLOADS, STREAMING],        # Tencent
    "byda" => [DOWNLOADS, STREAMING],        # Bytedance
    "uma" => [STREAMING],                   # UMA-BOOM
    "net" => [STREAMING],                   # NetEase
    "joox" => [STREAMING], # Joox
    "tim" => [STREAMING], # TIM
    "hung" => [STREAMING], # Hungama
    "gaana" => [DOWNLOADS, STREAMING], # Gaana
    "zed" => [STREAMING], # Zed
    "mtn" => [SOCIAL, STREAMING],    # Ayoba / MusicTime
    "qob" => [DOWNLOADS, STREAMING], # Qobuz
    "scld" => [STREAMING], # Sound Cloud
    "reels" => [FREE, SOCIAL, STREAMING], # FB Freemium
    "pel" => [STREAMING, DISCOVERY], # Peloton
    "dou" => [SOCIAL, STREAMING], # Douyin
    "luna" => [STREAMING, DISCOVERY], # Luna / Qishui Yinyue
  }.freeze

  INDIVIDUAL_TAKEDOWN_STORES = ["YoutubeSR", "FBTracks"]
  UNTRANSLATED_LOCALES = %w[de fr it in]

  EXCLUDED_FROM_NOTIFICATION_IDS = [18] # 18:Streaming

  SPOTIFY = "Spotify".freeze

  has_many :salepoints, dependent: :destroy
  has_many :distributions, through: :salepoints
  has_many :salepoint_songs, through: :salepoints
  has_many :distribution_songs, through: :salepoint_songs
  has_many :salepointables, through: :salepoints
  has_many :distributions, through: :salepoints
  has_many :variable_prices_stores, -> { where is_active: true }
  has_many :variable_prices, through: :variable_prices_stores
  has_many :track_monetizations
  has_many :track_monetization_blockers
  has_many :delivery_batches
  has_many :targeted_product_stores, dependent: :destroy
  has_one :store_group_store
  has_one :store_group, through: :store_group_store
  has_one :store_delivery_config
  has_and_belongs_to_many :country_websites

  has_many :salepointable_stores
  has_many :ineligibility_rules
  has_many :sip_stores
  has_many :copyright_claims
  has_many :plans_stores, dependent: :destroy
  has_many :plans, through: :plans_stores

  delegate :paused,
           :paused?,
           :unpaused,
           :unpaused?,
           :use_manifest,
           :distributable_class,
           :delivery_queue,
           to: :store_delivery_config

  scope :is_active, -> { where(is_active: true) }
  scope :not_active, -> { where(is_active: false) }
  scope :not_launched, -> { where(launched_at: nil) }
  scope :is_used, -> { where(in_use_flag: true) }
  scope :active_for_purchase, -> { is_active.where.not(id: DEPRECATED_ITUNES_STORES) }
  scope :is_not_used, -> { where(in_use_flag: false) }
  scope :is_unpaused,
        -> {
          joins(:store_delivery_config)
            .where(store_delivery_configs: { paused: false })
        }

  scope :salepointable_type,
        ->(salepointable_type) {
          select("DISTINCT stores.*")
            .joins(:salepointable_stores)
            .where(salepointable_stores: { salepointable_type: salepointable_type })
        }

  scope :used_stores_without_itunes, -> { Store.is_used.where(store_t[:id].not_in(ITUNES_STORE_IDS)) }
  scope :mass_retry_album_stores, -> { used_stores_without_itunes.where(store_t[:id].not_in(EXCLUDED_FROM_MASS_RETRY)) }
  scope :mass_retry_unpaused_album_stores, -> { mass_retry_album_stores.is_unpaused }
  scope :mass_retry_song_stores, -> { where(id: MASS_RETRY_SONG_STORES) }
  scope :track_monetization_stores, -> { where(id: TRACK_MONETIZATION_STORES) }
  scope :song_start_time_stores, -> { where(id: SONG_START_TIME_STORES) }
  scope :orphaned_salepoint_stores_for_album,
        ->(album) {
          joins(
            "LEFT JOIN salepoints ON salepoints.store_id = stores.id
                   AND salepoints.salepointable_id = #{album.id}
                   AND salepoints.salepointable_type = 'Album'"
          )
            .where(salepoints: { salepointable_id: nil })
        }
  # scope :discovery, -> { where(discovery_platform: true) }
  # scope :exclude_discovery, -> { where(discovery_platform: false) }
  scope :exclude_believe, -> { where.not(id: Store::BELIEVE_STORE_IDS) }
  scope :facebook, ->(_person = nil) { where(id: FB_REELS_STORE_ID) }

  validates :name, :short_name, uniqueness: true
  validates :abbrev, uniqueness: { scope: :is_active }
  validates :name, :short_name, :abbrev, presence: true

  def self.stores_on_sale
    where("on_sale = ?", true)
  end

  def self.stores_on_flash_sale
    where("on_flash_sale = ?", true)
  end

  delegate :queue, to: :store_delivery_config

  def unpaused
    !paused
  end

  # Breaks country codes out for the salepoint based on its abreviation
  def countries
    case abbrev
    when "eu"
      ["BE", "DK", "DE", "ES", "FI", "FR", "GR", "IE", "IT", "LU", "NL", "NO", "AT", "PT", "CH", "SE"]
    when "au"
      ["AU", "NZ"]
    when "az-us"
      ["US"]
    when "az"
      ["US", "GB", "DE", "AT", "CH", "FR", "JP"]
    when "np"
      ["WWD"]
    when "ww"
      Country::COUNTRIES.collect { |c| c[1] } << "WW"
    else
      [abbrev.upcase]
    end
  end

  def abbreviation
    abbrev
  end

  def self.ITUNES_US
    @@itunes_us ||= find(1)
  end

  def has_variable_pricing?
    default_variable_price.present? && variable_prices.any?
  end

  def variable_pricing_required?
    name.exclude?("Google") && has_variable_pricing?
  end

  def launch(_user)
    ret_val = false

    if launched_at.blank?
      self.launched_at = Time.now
      self.in_use_flag = true
      BulkStoreLaunchEmailWorker.perform_async(id)
      ret_val = save
    end

    ret_val
  end

  def deliver_through_petri?
    delivery_service == "petri"
  end

  def deliver_through_tunecore?
    delivery_service == "tunecore"
  end
  alias_method :deliver_through_sidekiq?, :deliver_through_tunecore?

  def group
    case short_name
    when "Google"
      "google"
    when "iTunesAU", "iTunesCA", "iTunesEU", "iTunesJP", "iTunesUK", "iTunesMX"
      "itunes"
    when "RhapsodyRH"
      "rhapsody"
    when "Medianet"
      "medianet"
    when "Napster"
      "napster"
    when "eMusic"
      "emusic"
    when "Connect"
      "sony_connect"
    when "GroupieTun"
      "vervelife"
    when "Amazon"
      "amazon_mp3"
    when "SNOCAP"
      "snocap"
    when "Lala"
      "lala"
    when "Amiestreet"
      "amiestreet"
    when "Streaming"
      "streaming"
    when "AmazonOD"
      "amazon_on_demand"
    when "Limewire"
      "limewire"
    when "MixRadio"
      "nokia"
    when "Myspace"
      "myspace"
    when "Zune"
      "zune"
    when "Thumbplay"
      "iheartradio"
    when "Spotify"
      "spotify"
    when "Youtube"
      "youtube"
    end
  end

  def position
    self[:position] || id
  end

  def description
    custom_t("store_descriptions.#{short_name}")
  end

  def converter_class
    self.class::CONVERTER_MAP[short_name] || "MusicStores::DDEX::Converter"
  end

  def in_use?
    in_use_flag
  end

  def number_of_paused_distributions_key
    "number_of_distros_paused_in_#{short_name}"
  end

  def number_of_paused_distributions
    Rails.cache.read(number_of_paused_distributions_key) || 0
  end

  def self.stores_supporting_album_only
    converter_classes = []
    ObjectSpace.each_object(DistributionSystem::Converter) { |c| converter_classes << c }
    album_only_store_names =
      converter_classes.select(&:supports_album_only?).collect { |k| k.class.to_s.split("::")[1] }
    album_only_stores = Store.all.select { |s| album_only_store_names.any? { |n| n.match(/#{s.short_name}/) } }
  end

  def self.mass_takedown_stores
    used_stores_without_itunes.reject { |store| MASS_TAKEDOWN_REJECTED_STORES.include?(store.abbrev) }
  end

  def self.variable_price_by_type(type, store)
    store.variable_prices.by_salepointable_type(type).first
  end

  def self.is_google?(name)
    name.include?("Google")
  end

  def self.get_implicit_track_variable_price(store)
    return if is_google?(store.name)

    store.default_variable_price.nil? ? variable_price_by_type("Song", store) : nil
  end

  def self.get_implicit_default_variable_price(store, type)
    return if is_google?(store.name)

    if store.variable_prices.size == 1
      store.variable_prices.first
    elsif store.default_variable_price.nil?
      variable_price_by_type(type, store) || variable_price_by_type("Album", store)
    end
  end

  def is_ytsr_store?
    id == YTSR_STORE_ID
  end

  def self.is_google_store?(abbrev)
    abbrev == GOOGLE_PLAY_ABBREV
  end

  def self.consolidate_google_ytm_store?
    FeatureFlipper.show_feature?(:consolidate_google_and_youtube)
  end

  def self.use_google_yt_abbrev?(abbrev)
    (is_google_store?(abbrev) && consolidate_google_ytm_store?) ||
      abbrev == YTSR_STORE_ABBREV
  end

  def self.get_custom_abbrev(abbrev)
    return "go-yt" if use_google_yt_abbrev?(abbrev)

    abbrev
  end

  # per product reqs scope discovery platforms to feature flag for launching to subset of users
  # TODO: Remove Flag once 100% and use a scope for discovery_platform column status
  def self.discovery_platforms(user = nil)
    if FeatureFlipper.show_feature?(:discovery_platforms, user)
      where(discovery_platform: true)
    else
      where(id: FB_REELS_STORE_ID)
    end
  end

  def self.exclude_discovery(user = nil)
    if FeatureFlipper.show_feature?(:discovery_platforms, user)
      where(discovery_platform: false)
    else
      where.not(id: FB_REELS_STORE_ID)
    end
  end

  def self.freemium_upsell_stores(user = nil)
    excluded_store_ids = Store::DEPRECATED_ITUNES_STORES + [Store::AOD_STORE_ID]
    active_for_purchase
      .exclude_discovery(user)
      .where
      .not(id: excluded_store_ids)
  end

  def name
    return "YouTube Music/Google Play" if Store.is_google_store?(abbrev) && Store.consolidate_google_ytm_store?

    super
  end

  def self.hide_ytm_store?(abbrev)
    abbrev == YTM_STORE_ABBREV && consolidate_google_ytm_store?
  end

  def self.hide_believe_store?(abbrev)
    abbrev == BELIEVE_STORE_ABBREV
  end

  def delivered_by_tc_distributor?
    delivery_service.include?(TC_DISTRIBUTOR)
  end

  def delivered_by_tunecore?
    delivery_service.include?(TUNECORE)
  end

  def freemium?
    FREEMIUM_STORES.include? id
  end

  def discovery_platform?
    discovery_platform
  end

  def custom_abbrev
    self.class.get_custom_abbrev(abbrev)
  end
end
