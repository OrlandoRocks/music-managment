class Cover < ApplicationRecord
  COVER_ART_TMP_DIR     = "cover_art_tmp".freeze
  IMAGES_DIR            = File.join(Rails.root.to_s, "public", COVER_ART_TMP_DIR).freeze
  BACKGROUND_IMAGES_DIR = IMAGES_DIR + "/backgrounds".freeze
  ARTIST_IMAGES_DIR     = IMAGES_DIR + "/artists".freeze
  TITLE_IMAGES_DIR      = IMAGES_DIR + "/titles".freeze
  COVER_IMAGES_DIR      = IMAGES_DIR + "/covers".freeze
  DEFAULT_BACKGROUND    = "/cover-images/600x600/abstract_blue.jpg".freeze
  RESOLUTIONS           = [300, 600, 1600].freeze

  belongs_to :album
  belongs_to :background
  belongs_to :background_effect

  belongs_to :artist_typeface,
             class_name: "Typeface"
  belongs_to :artist_typeface_effect,
             class_name: "TypefaceEffect"
  belongs_to :title_typeface,
             class_name: "Typeface"
  belongs_to :title_typeface_effect,
             class_name: "TypefaceEffect"

  validates :artist,
            :artist_typeface_pointsize,
            :artist_typeface_id,
            :title,
            :title_typeface_pointsize,
            :title_typeface_id,
            presence: true

  attr_accessor :artist_wrap_count
  attr_accessor :title_wrap_count

  before_create :assign_s3_file_name
  after_destroy :delete_images

  def render_size
    @render_size_value ||= 600
  end

  def render_size=(value)
    @render_size_value = value
  end

  def dimensions
    "#{render_size}x#{render_size}"
  end

  def rendered_background_filename
    "#{BACKGROUND_IMAGES_DIR}/#{id_dot_png}"
  end

  def rendered_title_filename
    "#{TITLE_IMAGES_DIR}/#{id_dot_png}"
  end

  def rendered_artist_filename
    "#{ARTIST_IMAGES_DIR}/#{id_dot_png}"
  end

  def rendered_cover_filename
    "#{COVER_IMAGES_DIR}/#{local_file_name}"
  end

  def s3_cover_path
    "#{bucket(render_size).url}#{s3_file_name}"
  end

  def local_cover_path
    "/#{COVER_ART_TMP_DIR}/covers/#{local_file_name}"
  end

  def local_file_name
    "#{render_size}_#{id_dot_png}"
  end

  def rendered_cover_descriptive_filename
    background_effect ? "#{COVER_IMAGES_DIR}/#{id}#{background_effect.name}#{background_effect.level}.png" : rendered_cover_filename
  end

  def background_url
    my_background = background ? background.url : DEFAULT_BACKGROUND
    # if we're doing a different size, switch the background location
    (render_size != 600) ? my_background.gsub("600", render_size.to_s) : my_background
  end

  def render_background
    background_effect.render("#{Rails.root}/public#{background_url}", "#{BACKGROUND_IMAGES_DIR}/#{id_dot_png}")
  end

  def render_cover
    target_folder = COVER_IMAGES_DIR
    file          = "/#{id_dot_png}"
    destination   = "#{target_folder}#{file}"

    raise "Missing #{rendered_artist_filename}"     unless artist_file_rendered
    raise "Missing #{rendered_background_filename}" unless background_file_rendered

    raise "Artist left is < 0" if artist_left.negative?
    raise "Title left is < 0"  if title_left.negative?

    compose_artist_name(destination)
    compose_album_title(destination)
    update_rendered_file_permissions
  end

  def render_title_text
    title_typeface_effect.render_text_for(self, :title)
  end

  def render_artist_text
    artist_typeface_effect.render_text_for(self, :artist)
  end

  def artist_typeface_pointsize_for_render
    artist_typeface_pointsize * (render_size.to_f / 600.to_f)
  end

  def title_typeface_pointsize_for_render
    title_typeface_pointsize * (render_size.to_f / 600.to_f)
  end

  def artist_text_for_render
    if artist_wrap_count.to_i.zero?
      artist
    else
      words = artist.split(" ") # split the words up into an array to make them easier to deal with
      array_index_for_newline = (artist_wrap_count + 1) * -1 # Our index needs to be incremented by one and made negative
      words.insert(array_index_for_newline, "\n") # The array index here is the position for the insertion of the new line.
      words.join(" ").gsub(" \n ", "\n") # put the string back together and remove the spaces around newlines
    end
  end

  def title_text_for_render
    if title_wrap_count.to_i.zero?
      title
    else
      words = title.split(" ") # split the words up into an array to make them easier to deal with
      array_index_for_newline = (title_wrap_count + 1) * -1 # Our index needs to be incremented by one and made negative
      words.insert(array_index_for_newline, "\n") # The array index here is the position for the insertion of the new line.
      words.join(" ").gsub(" \n ", "\n") # put the string back together and remove the spaces around newlines
    end
  end

  def rendered_artist_height
    height = ArtworkSuggestion::ImageGenerationService.identify(rendered_artist_filename)[:height]
    logger.info("Read artist height as: #{height}")
    height
  end

  def rendered_artist_width
    width = ArtworkSuggestion::ImageGenerationService.identify(rendered_artist_filename)[:width]
    logger.info("Read artist width as: #{width}")
    width
  end

  def rendered_title_height
    height = ArtworkSuggestion::ImageGenerationService.identify(rendered_title_filename)[:height]
    logger.info("Read title height as: #{height}")
    height
  end

  def rendered_title_width
    width = ArtworkSuggestion::ImageGenerationService.identify(rendered_title_filename)[:width]
    logger.info("Read title width as: #{width}")
    width
  end

  def render(size = 1600)
    self.render_size = size
    render_background
    render_artist_text
    render_title_text
    render_cover
    upload_cover_to_s3(rendered_cover_filename, s3_file_name, size)
  end

  def bucket(size)
    S3_CLIENT.buckets["assets.tunecore.com/artwork_suggestions/#{size}"]
  end

  def upload_cover_to_s3(src_file, dest_file, size = 1600)
    bucket(size).objects.create(dest_file, File.open(src_file, "r"), acl: :public_read)
  end

  def artist_font_examples
    Typeface.all.map { |font| render_text_for("artist", artist_text_for_render, font, artist_typeface_pointsize, artist_typeface_effect, font.name) }
  end

  def id_dot_png
    "#{id}.png"
  end

  def layout
    layout_class.constantize.new(self)
  end

  delegate :artist_top, to: :layout

  delegate :artist_left, to: :layout

  delegate :title_top, to: :layout

  delegate :title_left, to: :layout

  def generate_artwork
    render
    local_cover_path
  end

  def delete_images(resolutions = RESOLUTIONS)
    delete_local_images(resolutions)
    delete_s3_images(resolutions)
  end

  def delete_local_images(resolutions = RESOLUTIONS)
    resolutions.each do |size|
      self.render_size = size
      File.unlink rendered_artist_filename rescue nil
      File.unlink rendered_title_filename rescue nil
      File.unlink rendered_cover_filename rescue nil
    end
  end

  def delete_s3_images(resolutions = RESOLUTIONS)
    resolutions.each do |size|
      bucket(size).objects[s3_file_name].delete rescue nil
    end
  end

  private

  def assign_s3_file_name
    self.s3_file_name = "#{SecureRandom.uuid}.png"
  end

  def compose_artist_name(destination)
    ArtworkSuggestion::ImageGenerationService.composite(
      rendered_background_filename,
      rendered_artist_filename,
      destination,
      { geometry: "+#{artist_left}+#{artist_top}" }
    )
  end

  def compose_album_title(destination)
    ArtworkSuggestion::ImageGenerationService.composite(
      destination,
      rendered_title_filename,
      rendered_cover_filename,
      { geometry: "+#{title_left}+#{title_top}" }
    )
  end

  def update_rendered_file_permissions
    File.chmod(0755, "#{COVER_IMAGES_DIR}/#{local_file_name}")
  end

  def artist_file_rendered
    File.exist?(rendered_artist_filename)
  end

  def background_file_rendered
    File.exist?(rendered_background_filename)
  end
end
