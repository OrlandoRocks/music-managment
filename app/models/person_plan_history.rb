# frozen_string_literal: true

class PersonPlanHistory < ApplicationRecord
  validates :person, :plan, :plan_start_date, :plan_end_date, :change_type, presence: true
  validates :purchase, presence: true, unless: -> { DOWNGRADES.include?(change_type) }

  belongs_to :person
  belongs_to :plan
  belongs_to :purchase

  INITIAL_PURCHASE = "initial_purchase"
  USER_UPGRADE = "user_upgrade"
  REQUESTED_DOWNGRADE = "requested_downgrade"
  FORCED_DOWNGRADE = "forced_downgrade"
  RENEWAL = "renewal"

  DOWNGRADES = [REQUESTED_DOWNGRADE, FORCED_DOWNGRADE].freeze
  DEFAULT_DOWNGRADE = FORCED_DOWNGRADE

  VALID_CHANGE_TYPES = {
    initial_purchase: INITIAL_PURCHASE,
    user_upgrade: USER_UPGRADE,
    forced_downgrade: FORCED_DOWNGRADE,
    requested_downgrade: REQUESTED_DOWNGRADE,
    renewal: RENEWAL
  }

  enum change_type: VALID_CHANGE_TYPES

  enum discount_reason: Purchase::VALID_DISCOUNT_REASONS
end
