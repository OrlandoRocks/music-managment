class SongSalesReport
  def initialize
    @totals = []
  end

  def add_record(record)
    return unless song_id = record.song_id

    setup_song_totals(song_id)
    current = @totals[song_id]
    current[:songs_sold] += record.units_sold if record.sale_type == "dl"
    current[:songs_streamed] += record.units_sold if record.sale_type != "dl"
    current[:net_earnings] += record.usd_total_cents
  end

  def setup_song_totals(song_id)
    return if @totals[song_id]

    @totals[song_id] = {
      albums_sold: 0,
      songs_streamed: 0,
      songs_sold: 0,
      net_earnings: 0
    }
  end

  #
  #  Specific song numbers
  #
  def albums_sold(song_id)
    current = @totals[song_id]
    current ? current[:albums_sold] : 0
  end

  def songs_sold(song_id)
    current = @totals[song_id]
    current ? current[:songs_sold] : 0
  end

  def songs_streamed(song_id)
    current = @totals[song_id]
    current ? current[:songs_streamed] : 0
  end

  def net_earnings(song_id)
    current = @totals[song_id]
    cents = current ? current[:net_earnings] : 0
    Money.new(cents)
  end
end
