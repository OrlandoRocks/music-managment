# COMMENTS MOVED TO WIKI

class Renewal < ApplicationRecord
  #################
  # Associations #
  #################
  has_many :renewal_history
  has_many :history, class_name: "RenewalHistory"
  has_many :renewal_product_changes # , :order => "created_at ASC"
  has_many :renewal_items
  has_many :items, class_name: "RenewalItem"
  has_many :purchases, as: :related
  has_many :invoice_logs
  belongs_to :item_to_renew, polymorphic: true # references the product or product_item to use for renewal logic
  belongs_to :renewal_product_item,
             -> { joins(:renewals).where(renewals: { item_to_renew_type: "ProductItem" }) },
             foreign_key: :item_to_renew_id,
             class_name: :ProductItem,
             inverse_of: :renewals
  belongs_to :person

  ################
  # Validations #
  ################
  validates :item_to_renew, presence: true

  ###############
  # Delegations #
  ###############
  delegate :renewal_duration,
           :renewal_interval,
           :first_renewal_interval,
           :first_renewal_duration,
           :name,
           :is_plan?,
           :plan_related?,
           to: :item_to_renew

  PRODUCT_ITEM_RENEWAL_TYPE = "ProductItem".freeze

  #################
  # Named Scopes #
  #################
  RENEWAL_INFORMATION_SELECT = <<-SQL.strip_heredoc.freeze
    renewals.*, min(h.starts_at) AS starts_at, MAX(h.expires_at) AS expires_at,
      IF(pi.first_renewal_interval IS NULL,p.first_renewal_interval,pi.first_renewal_interval) AS first_renewal_interval,
      IF(pi.first_renewal_duration IS NULL,p.first_renewal_duration,pi.first_renewal_duration) AS first_renewal_duration,
      IF(pi.renewal_interval IS NULL,p.renewal_interval,pi.renewal_interval) AS renewal_interval,
      IF(pi.renewal_duration IS NULL,p.renewal_duration,pi.renewal_duration) AS renewal_duration,
      IF(pi.renewal_product_id IS NULL,p.renewal_product_id,pi.renewal_product_id) AS renewal_product_id,
      pi.does_not_renew, pi.renewal_type
  SQL

  RENEWAL_INFORMATION_JOIN = <<-SQL.strip_heredoc.freeze
    LEFT JOIN renewal_history h ON renewals.id=h.renewal_id
    LEFT JOIN product_items pi ON (renewals.item_to_renew_id=pi.id AND renewals.item_to_renew_type='ProductItem')
    LEFT JOIN products p ON (renewals.item_to_renew_id=p.id AND renewals.item_to_renew_type='Product')
  SQL

  scope :with_dates,
        -> {
          select("renewals.*, min(renewal_history.starts_at) as starts_at, max(renewal_history.expires_at) as expires_at")
            .joins("left join renewal_history on renewals.id=renewal_history.renewal_id")
            .group("renewals.id")
        }

  scope :by_person_id,
        ->(person_id) {
          where(person_id: person_id)
        }

  scope :active,
        -> {
          joins(:renewal_history)
            .order("renewals.created_at desc")
            .where("expires_at >= ?", Time.now)
        }

  scope :expired,
        -> {
          joins(:renewal_history)
            .order("created_at desc")
            .where("expires_at < ?", Time.now)
        }

  scope :unordered_expired,
        -> {
          joins(:renewal_history)
            .where("renewal_history.created_at = (SELECT MAX(created_at) FROM renewal_history WHERE renewal_history.renewal_id = renewals.id)")
            .where("expires_at < ?", Time.now)
        }

  scope :duplicate_active_plan_renewals,
        -> {
          plans
            .canceled(false)
            .group(:person_id)
            .having("COUNT(*) > 1")
        }

  REMINDERS_LENGTH = 5
  REMINDERS_NOTICE_GRACE_DAYS = 10
  UNPAID_TREND_REPORTS_GRACE_DAYS_BEFORE_DELETION = 14
  GRACE_DAYS_BEFORE_TAKEDOWN = 42
  GRACE_DAYS_BEFORE_TAKEDOWN_MONTHLY = 2
  DAYS_IN_RENEWAL_PERIOD = 28
  # this named scoped returns an array of renewals that expire within a set period of time
  # it can accept 0,1,or 2 varables passed in as strings that can be converted to dates
  # Renewals.expires_within_dates returns renewals that expire between Now and 6 weeks from now (6 was the initial setting, this may change)
  # Renewals.expires_within_dates('2010-01-01') returns renewals that expire between 1/1/2010 and 6 weeks from 1/1/2010
  # Renewals.expires_within_dates('2010-01-01','2010-02-01') returns renewals that expire between 1/1/2010 and 2/1/2010
  scope :expires_within_dates,
        ->(*dates) {
          with_expiry_date
            .where(
              "expires >= ? AND expires <= ? AND canceled_at IS NULL AND takedown_at IS NULL",
              dates.first || Time.now,
              dates.second || (dates.first || Time.now) + REMINDERS_LENGTH.weeks
            )
        }

  scope :expires_past_date,
        ->(date) do
          with_expiry_date
            .where("expires <= ?", date)
            .where(canceled_at: nil, takedown_at: nil)
        end

  scope :with_expiry_date,
        -> {
          joins(
            "LEFT JOIN (SELECT MAX(expires_at) AS expires, renewal_id FROM renewal_history GROUP BY renewal_id) rh ON rh.renewal_id=renewals.id"
          )
        }

  scope :expiring_within,
        ->(start_date, end_date) {
          with_expiry_date
            .where(canceled_at: nil, takedown_at: nil)
            .where("expires >= ? AND expires < ?", start_date, end_date)
            .eager_load(person: [:person_preference])
        }

  scope :expires_on_date,
        ->(monthly, date) {
          joins(<<-SQL.strip_heredoc)
      LEFT JOIN (SELECT MAX(expires_at) AS expires, renewal_id FROM renewal_history group by renewal_id) rh
        ON rh.renewal_id=renewals.id
      INNER JOIN product_items pi ON pi.id = renewals.item_to_renew_id
      AND renewals.item_to_renew_type = 'ProductItem'
          SQL
            .where(
              "expires = ? AND pi.renewal_interval = ? AND canceled_at IS NULL AND takedown_at IS NULL",
              date,
              monthly ? "month" : "year"
            )
        }

  # this named scope returns the renewal information from either the product or product_item level
  # depending on what was set when the renewal was created.
  # With this named_scope, you can call renewal.first_renewal_interval, etc even though this information is stored in
  # separate tables
  scope :with_renewal_information,
        -> {
          select(RENEWAL_INFORMATION_SELECT)
            .joins(RENEWAL_INFORMATION_JOIN)
            .group("renewals.id")
        }

  scope :canceled,
        ->(canceled) {
          where(canceled ? "renewals.canceled_at is NOT NULL" : "renewals.canceled_at is NULL")
        }

  scope :takendown,
        ->(takedown) {
          where(takedown ? "renewals.takedown_at is NOT NULL" : "renewals.takedown_at is NULL")
        }

  scope :expires_between,
        ->(date1, date2) {
          joins("INNER JOIN renewal_history AS rh1 ON rh1.renewal_id = renewals.id")
            .where("DATE(?) <= DATE(rh1.expires_at) AND DATE(rh1.expires_at) <= DATE(?)", date1, date2)
        }

  scope :first_expiration_after_date,
        ->(date) {
          joins(:renewal_history).where(
            %[
      renewal_history.expires_at = (
        SELECT MIN(renewal_history.expires_at)
        FROM renewal_history
        WHERE renewal_history.renewal_id = renewals.id
          AND DATE(renewal_history.expires_at) >= DATE(?)
      )
    ],
            date
          )
        }

  scope :with_purchase_extending_due_date_paid_before,
        ->(due_date, paid_before_time) {
          with_renewal_histories
            .where(<<-SQL.strip_heredoc, due_date: due_date, paid_before_time: paid_before_time)
        DATE(rh1.expires_at) = DATE(:due_date)
          AND DATE(rh2.starts_at) = DATE(:due_date)
          AND paid_at IS NOT NULL
          AND paid_at < :paid_before_time
            SQL
        }

  scope :with_purchase_extending_due_date_paid_after,
        ->(due_date, paid_after_time) {
          with_renewal_histories
            .where(<<-SQL.strip_heredoc, due_date: due_date, paid_after_time: paid_after_time)
        DATE(rh1.expires_at) = DATE(:due_date)
          AND DATE(rh2.starts_at) = DATE(:due_date)
          AND paid_at IS NOT NULL
          AND paid_at >= :paid_after_time
            SQL
        }

  scope :due_on_but_not_renewed,
        ->(date) {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN renewal_history AS rh1 ON rh1.renewal_id = renewals.id
      LEFT JOIN (
        SELECT renewal_history.* FROM renewal_history
          WHERE renewal_history.starts_at = '#{date.to_s(:db)}'
      ) rh2
      ON rh2.renewal_id = renewals.id
      INNER JOIN purchases on rh1.purchase_id = purchases.id
          SQL
            .where("Date(rh1.expires_at) = Date(?) AND rh2.id IS NULL", date)
        }

  scope :for_album_type,
        ->(album_type) {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN renewal_items ON renewal_items.renewal_id = renewals.id
      INNER JOIN albums ON albums.id = renewal_items.related_id
          SQL
            .where("renewal_items.related_type = 'Album' AND albums.album_type = ?", album_type)
        }

  scope :for_country_website,
        ->(country_website_id) {
          joins(:person).where(people: { country_website_id: country_website_id })
        }

  scope :with_renewal_histories,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN renewal_history AS rh1 ON rh1.renewal_id = renewals.id
      INNER JOIN renewal_history AS rh2 ON rh2.renewal_id = renewals.id
      INNER JOIN purchases ON rh2.purchase_id = purchases.id
          SQL
        }

  scope :expired_live_and_not_canceled, -> do
    unordered_expired
      .where(takedown_at: nil)
      .where(canceled_at: nil)
      .order(expires_at: :desc)
  end

  scope :plans, -> {
                  where(
                    item_to_renew: ProductItem::PLAN_PRODUCT_ITEM_IDS,
                    item_to_renew_type: PRODUCT_ITEM_RENEWAL_TYPE,
                  )
                }

  scope :in_cart, -> do
    joins(:purchases)
      .where(purchases: { paid_at: nil })
  end

  attr_accessor :purchase, :renewed_items, :product_item, :product

  validates :purchase, presence: { on: :create }

  ############
  # filters #
  ############
  after_create :create_renewal_history, :create_renewal_items

  # returns renewals that have not been taken down
  # unfortunately, it appears you can't chain a named_scope and add a having clause, so I had to recreate the query. CH
  def self.renewals_within_date_range_by_person(person, start_date, end_date)
    subscriptions = select(RENEWAL_INFORMATION_SELECT).joins(RENEWAL_INFORMATION_JOIN).group("renewals.id").where("person_id = ? AND takedown_at IS NULL", person.id).having("((expires_at >= ? AND expires_at <= ?) OR expires_at < ?)", start_date, end_date, Time.now).order("expires_at")
    subscriptions.select { |s| !s.is_subscription? }
  end

  # returns subscriptions that have not yet expired
  # unfortunately, it appears you can't chain a named_scope and add a having clause, so I had to recreate the query. CH
  def self.active_subscriptions_by_person(person)
    select(Renewal::RENEWAL_INFORMATION_SELECT)
      .joins(Renewal::RENEWAL_INFORMATION_JOIN)
      .joins(:renewal_items)
      .group("renewals.id")
      .where(
        renewals: {
          person_id: person.id
        },
        renewal_items: {
          related_type: %w[Album Video]
        }
      )
      .having("expires_at >= ?", Time.now)
      .order(:expires_at)
  end

  def self.due_on(date)
    expires_between(date, date)
  end

  def self.renewed_in_advance(batch)
    with_purchase_extending_due_date_paid_before(batch.batch_date.to_date, batch.batch_date)
  end

  def self.renewed_in(batch)
    with_purchase_extending_due_date_paid_after(batch.batch_date.to_date, batch.batch_date)
  end

  def self.not_renewed_in(batch)
    due_on_but_not_renewed(batch.batch_date.to_date)
  end

  def self.not_renewed_canceled(batch)
    not_renewed_in(batch).canceled(true)
  end

  def self.not_renewed_takendown(batch)
    not_renewed_in(batch).canceled(false).takendown(true)
  end

  def self.not_renewed_but_in_grace(batch)
    not_renewed_in(batch).canceled(false).takendown(false)
  end

  # returns subscriptions that have expired
  # unfortunately, it appears you can't chain a named_scope and add a having clause, so I had to recreate the query. CH
  def self.expired_subscriptions_by_person(person)
    subscriptions = select(RENEWAL_INFORMATION_SELECT)
                    .joins(RENEWAL_INFORMATION_JOIN)
                    .group("renewals.id")
                    .where("person_id = ?", person.id)
                    .having("expires_at < ?", Time.now)
                    .order("expires_at")

    subscriptions.select(&:is_subscription?)
  end

  #
  # Returns a product or product item for a renewal of the specified time
  #
  def self.product_or_product_item_for_type_and_duration(country_website_id, album_type, interval, duration)
    product = nil

    # We give precedence to product items
    product = ProductItem
              .joins(<<-SQL.strip_heredoc)
        INNER JOIN products renewal_products ON renewal_products.id = product_items.renewal_product_id
        INNER JOIN products ON products.id = product_items.product_id
              SQL
              .where(<<-SQL.strip_heredoc, album_type, country_website_id, interval, duration)
        renewal_products.product_type = 'Renewal'
          AND renewal_products.status = 'Active'
          AND renewal_products.applies_to_product = ?
          AND products.country_website_id = ?
          AND products.status = 'Active'
          AND products.product_type = 'Ad Hoc'
          AND product_items.renewal_interval= ?
          AND product_items.renewal_duration= ?
              SQL
              .limit(1)
              .first

    product ||= Product
                .joins("INNER JOIN products renewal_products ON renewal_products.id = products.renewal_product_id")
                .where(<<-SQL.strip_heredoc, album_type, country_website_id, interval, duration)
            renewal_products.product_type = 'Renewal'
              AND renewal_products.status = 'Active'
              AND renewal_products.applies_to_product = ?
              AND products.country_website_id = ?
              AND products.status = 'Active'
              AND products.renewal_interval= ?
              AND products.renewal_duration= ?
                SQL
                .limit(1)
                .first

    product
  end

  def self.renewal_for(related)
    related_type = related.class.base_class.name

    with_renewal_information
      .joins(:renewal_items)
      .where(renewal_items: { related_type: related_type, related_id: related.id })
      .readonly(false)
      .order(id: :desc)
      .first
  end

  def self.renewals_for(related)
    related_type = related.class.base_class.name

    with_renewal_information
      .joins("INNER JOIN renewal_items ri ON ri.renewal_id=renewals.id")
      .where("ri.related_type = ? AND ri.related_id = ?", related_type, related.id)
  end

  # 2011-11-28 AK
  # Similar to the self.renewal_for() method.
  # Takes an array of album objects and sets the 'renewal' attibute for each of the albums.
  # This is faster than calling Album.renewal() for each album because this method only makes 1 trip to the DB.
  def self.set_renewals_for_albums(albums)
    stime = Time.now
    related_type = "Album"
    album_ids = albums.collect(&:id)
    renewals = with_renewal_information
               .joins("inner join renewal_items ri on ri.renewal_id=renewals.id")
               .where("ri.related_type = ? and ri.related_id in (?)", related_type, album_ids)
               .order("id DESC")

    updated_albums = []
    albums.each do |album|
      renewal = renewals.detect { |r| album.id == r.item_to_renew_id }
      album.cached_renewal = renewal
    end
    Rails.logger.info("Renewal.set_renewals_for_albums completed in #{Time.now - stime}s for #{albums.size} albums")
    albums
  end

  def self.make_expires_at_cache_for_albums(albums, singles, ringtones)
    cache = {}

    albums_w_renewals = Album.includes(renewals: :history)
                             .where(
                               id: [albums, singles, ringtones].flatten
                             )
                             .order("albums.id DESC, renewal_history.expires_at DESC")
                             .references(renewals: :history)

    albums_w_renewals.each do |album|
      cache[album.id] = album.renewals.first.history.first.expires_at if album.renewals.any?
    end

    cache
  end

  def self.renewal_price_for(related)
    renewal = renewal_for(related)
    if renewal != nil
      renewal.price_to_renew_in_money
    else
      0.00.to_money
    end
  end

  def self.renewal_exists_for?(related)
    related_type = related.class.base_class.name

    RenewalItem.find_by("related_id = ? AND related_type = ?", related.id, related_type) != nil
  end

  def self.renewal_for_without_info(related)
    related_type = related.class.base_class.name

    includes(:renewal_items).find_by(renewal_items: { related_type: related_type, related_id: related.id })
  end

  def self.renewal_history_for(related)
    related_type = related.class.base_class.name

    RenewalHistory
      .where("ri.related_type = ? and ri.related_id = ?", related_type, related.id)
      .joins("inner join renewal_items ri on ri.renewal_id=renewal_history.renewal_id")
      .order("expires_at")
  end

  def self.adjust_expiration_date_for(related, new_date)
    renewal = renewal_for_without_info(related)
    history = renewal.history.last
    history.expires_at = new_date
    history.save!
  end

  # [2010-03-04 -- CH]
  # Pointed unpaid_renewal? to expires_within? to create
  # a range so customers can create
  def self.unpaid_renewal?(related)
    renewal = renewal_for(related)
    return false unless renewal

    renewal.expires_within?(Time.now, Time.now + REMINDERS_LENGTH.weeks) || renewal.expired_live_and_not_canceled?
  end

  # [2010-03-04 -- CH]
  # changed to reference a history threshold instead of an open purchase.
  def self.unpaid_renewals_for_person?(person, distributions_only = false, start_date = Time.now, end_date = Time.now + REMINDERS_LENGTH.weeks)
    if distributions_only
      join = "inner join renewals on renewals.id=renewal_history.renewal_id inner join renewal_items ri on ri.renewal_id=renewal_history.renewal_id"
      criteria = ["renewals.person_id = ? and renewals.takedown_at IS NULL and canceled_at IS NULL and ri.related_type='Album'", person.id]
    else
      join = "inner join renewals on renewals.id=renewal_history.renewal_id"
      criteria = ["renewals.person_id = ? and renewals.takedown_at IS NULL and canceled_at IS NULL", person.id]
    end

    renewals_due = RenewalHistory
                   .select("renewal_history.renewal_id, MAX(expires_at) as expires_at")
                   .joins(join)
                   .where(criteria)
                   .group("renewal_id")
                   .having("(expires_at >= ? and expires_at <= ?) OR expires_at <= ?", start_date, end_date, Time.now)
                   .first

    renewals_due != nil
  end

  def self.send_renewal_block_notification(person, renewals)
    BlockedRenewalsMailer.blocked_renewals(person: person, renewals: renewals).deliver
  end

  def self.renewals_blocked_for_person?(person:, renewals:, notify: false)
    return false unless person.country_sanctioned?

    send_renewal_block_notification(person, renewals) if notify

    true
  end

  def self.create_renewal_purchases(date = Date.today)
    grouped_renewals = load_renewals_by_person(date.to_date)
    return if grouped_renewals.blank?

    grouped_renewals.each do |person, renewals|
      # Entry point for downgrades related code
      renewals = handle_plan_downgrade_condition(person, renewals)
      next if person.person_preference&.do_not_autorenew?
      next if renewals_blocked_for_person?(person: person, renewals: renewals, notify: true)

      yield person, create_renewal_purchases_for_person(person, renewals) if block_given?
    rescue StandardError => e
      Airbrake.notify("grouped_renewals processing failed for person", e)
      Rails.logger.error("\n#{e.class} (#{e.message})")
      RenewalsBatchFailureService.log(date)
      next
    end
  end

  def self.create_renewal_purchases_for_person(person, renewals)
    created_purchases = []
    renewals.each do |renewal|
      product = renewal.item_to_renew.renewal_product
      related = renewal.renewal_items.first.related

      if skip_related_plan?(related, person, product)
        logger.info "Skipping renewal purchase for renewal #{renewal.id} because User has plan #{person.plan.id}"
      elsif skip_related_album?(related)
        logger.info "Skipping renewal purchase for renewal #{renewal.id} because album #{renewal.renewal_items.first.related.id} has never been approved or distributed."
      elsif add_additional_artists?(related, person)
        plan_and_artist_purchases = Plans::RenewAdditionalArtistsService
                                    .plan_and_additional_artist_purchases!(person, renewal, product)
        created_purchases.concat(plan_and_artist_purchases)
      else
        created_purchases.concat(renewal_batch_purchase(person, renewal, product))
      end
    end
    created_purchases
  end

  def self.renewal_batch_purchase(person, renewal, product)
    purchase = Product.add_to_cart(person, renewal, product)
    if purchase.invoice_id.nil?
      [purchase]
    else
      logger.info "Payment Batch | comment = Purchase skipped because it is in use person_id=#{person.id} renewal_id=#{renewal.id}"
      []
    end
  end

  def self.skip_related_plan?(related, person, product)
    !related.instance_of?(PersonPlan) && person.has_plan? && product.plan.blank?
  end

  def self.skip_related_album?(related)
    related.is_a?(Album) && !related.has_ever_been_approved? && !related.has_been_distributed?
  end

  def self.add_additional_artists?(related, person)
    related.instance_of?(PersonPlan) && person.has_plan? && person.active_artists.present?
  end

  def price_to_renew
    product = Product.find(item_to_renew.renewal_product_id)
    Purchase.new(product: product, person: person, related: self).price_calculator.net_cost
  end

  def price_to_renew_in_money
    product = Product.find(item_to_renew.renewal_product_id)
    price_to_renew.to_money(product.currency)
  end

  def expiring?
    expires_within?(Time.now, Time.now + REMINDERS_LENGTH.weeks) || expired?
  end

  def starts_at
    start_date = attributes["starts_at"] || history.order(created_at: :asc).first.try(:starts_at)
    if start_date != nil
      start_date.to_time
    else
      nil # would like this to be created_at but changing it broke a bunch of tests. Without time to inspect, leaving as nil
    end
  end

  def expires_at
    expiration_date = attributes["expires_at"] || history_with_latest_expiration.try(:expires_at)
    if expiration_date != nil && expiration_date.to_s != "0000-00-00 00:00:00"
      expiration_date.to_time
    else
      Time.now + 127.years
    end
  end

  def current_term_starts_at
    current_term_start_date = history_with_latest_expiration.try(:starts_at)
    if current_term_start_date != nil && current_term_start_date.to_s != "0000-00-00 00:00:00"
      current_term_start_date.to_time
    else
      nil
    end
  end

  def age_in_years
    age_to_return = attributes["age_in_years"] || ((DateTime.now - expires_at.to_datetime).to_f / 365).floor + 1
    if age_to_return >= 0
      age_to_return
    else
      0
    end
  end

  def open_purchase_exists
    open_purchase = attributes["open_purchase_exists"]
    unless open_purchase
      purchase = Purchase.unpaid.find_by(related_id: id, related_type: "Renewal")
      open_purchase = purchase.nil? ? 0 : 1
    end
    open_purchase == 1
  end

  def renewal_history_for_purchase(purchase)
    history.where(purchase_id: purchase).first
  end

  def unlimited?
    expires_at >= Time.now + 99.years
  end

  def mark_as_takedown!
    purchase = Purchase.where("related_id = ? and related_type = 'Renewal' and paid_at IS NULL", id).first
    message = purchase.nil? ? "no purchase available to log" : ""
    options = {
      person: person,
      purchase: purchase,
      renewal_id: id,
      invoice: purchase&.invoice,
      message: message,
      current_method_name: __callee__,
      caller_method_path: caller&.first
    }
    InvoiceLogService.log(options)
    Renewal.update(id, takedown_at: Time.now)
    # destroy an unpaid purchase tied to this renewal, if it exists, so a customer doesn't pay for something that was taken down
    purchase.destroy unless purchase.nil?
  end

  def remove_takedown!
    Renewal.update(id, takedown_at: nil)
  end

  def change_album_renewal_length(album_type = "Album", interval = "month", duration = 1)
    return unless ["Album", "Single", "Ringtone"].include?(album_type)

    new_renewal_product_item = Renewal.product_or_product_item_for_type_and_duration(person.country_website_id, album_type, interval, duration)

    return if new_renewal_product_item.blank?

    # make the change (this contains a callback to update the renewal if the change passes validation)
    change = RenewalProductChange.create(renewal: self, new_item_to_renew: new_renewal_product_item)
    change.valid? # returning true or false to top-level mthod call
  end

  # currently we are offering yearly and monthly renewals, this helper returns the opposite
  def opposite_renewal_interval
    (renewal_interval == "year") ? "month" : "year"
  end

  # currently we are offering yearly and monthly renewals, this helper returns the opposite price
  def opposite_price_to_renew
    Purchase.new(product: opposite_product, person: person, related: self).price_calculator.net_cost if opposite_product
  end

  def opposite_price_to_renew_in_money
    return unless opposite_product

    Purchase.new(product: opposite_product, person: person, related: self).price_calculator.net_cost.to_money(opposite_product.currency)
  end

  def opposite_product
    opposite_product_item = Renewal.product_or_product_item_for_type_and_duration(person.country_website_id, "Album", opposite_renewal_interval, 1)
    opposite_product = Product.find(opposite_product_item.renewal_product_id) if opposite_product_item.present?
  end

  # date that a
  def takedown_date
    if taken_down?
      takedown_at
    elsif canceled?
      expires_at
    elsif item_to_renew.renewal_interval == "month"
      expires_at + GRACE_DAYS_BEFORE_TAKEDOWN_MONTHLY.days
    else
      expires_at + GRACE_DAYS_BEFORE_TAKEDOWN.days
    end
  end

  def renew!(purchase)
    self.purchase = purchase
    create_renewal_history
    plan_renewal_sideffects(purchase)
  end

  def plan_renewal_sideffects(purchase)
    purchased_plan = purchase.product&.plan

    update_person_plan_expiry(purchase.person, purchased_plan) if purchased_plan.present?
  end

  def extend!(purchase)
    self.purchase = purchase
    extend_renewal
  end

  def add_item(item_to_add)
    self.renewed_items = item_to_add
    create_renewal_items
  end

  def canceled?
    canceled_at?
  end

  def taken_down?
    takedown_at?
  end

  def expired?
    expires_at <= DateTime.now
  end

  def expired_live_and_not_canceled?
    expired? && !taken_down? && !canceled?
  end

  # returns true if the renewal has already expired or is within the date range and has not been taken down
  def expires_within?(start_date, end_date)
    takedown_at.nil? && canceled_at.nil? && (expires_at >= start_date && expires_at <= end_date)
  end

  def cancel!
    self.canceled_at = Time.now
    save!
  end

  def keep!
    self.canceled_at = nil
    save!
  end

  def active?
    Time.now < expires_at
  end

  def unpaid?
    if canceled? || taken_down?
      false
    else
      unpaid = Purchase
               .unpaid
               .joins("left join invoices i on i.id = purchases.invoice_id")
               .find_by("related_type = 'Renewal' and related_id = ? AND (i.id IS NULL OR i.batch_status = 'visible_to_customer')", id)
      unpaid != nil
    end
  end

  def is_subscription?
    renewal_items.any?(&:is_subscription?)
  end

  def has_first_renewal_settings?
    first_renewal_interval.present? && first_renewal_duration.present?
  end

  def has_history?
    history.count > 1
  end

  def self.load_renewals_by_person(end_date, options = {})
    start_date = options[:start_date] || end_date
    expiring_within(start_date, end_date + 1)
      .group_by(&:person)
  end

  def update_person_plan_expiry(person, plan)
    PersonPlanCreationService.call(
      person_id: person.id,
      plan_id: plan.id,
      purchase_id: purchase.id,
      change_type: PersonPlanHistory::RENEWAL,
      expires_at: expires_at,
      starts_at: current_term_starts_at
    )
  end

  def plan_renewal?
    ProductItem::PLAN_PRODUCT_ITEM_IDS.include?(item_to_renew_id) && item_to_renew_type == Renewal::PRODUCT_ITEM_RENEWAL_TYPE
  end

  def release?
    renewal_items.first.related.kind_of?(Album)
  end

  def self.handle_plan_downgrade_condition(person, renewals)
    # checks for downgrade requests and lets unrelated renewals passed through unaltered
    active_downgrade_request =  person
                                .plan_downgrade_requests
                                .active
                                .last
    return renewals if active_downgrade_request.nil?

    Plans::RequestedDowngradeService.call(person, renewals, active_downgrade_request)
  end

  def discovery_platform_renewal?
    # TODO: convert this to a method that only looks where discovery platform is false
    # once investigation into deprecating TargetedOffer logic for discovery platform pricing is complete
    # https://tunecore.atlassian.net/browse/SC-3061
    release? &&
      !renewal_items
        .first
        .related
        .salepoints
        .joins(:store)
        .where.not(stores: { id: [Store::YTSR_PROXY_ID, Store::IG_STORE_ID, Store::BELIEVE_STORE_IDS].flatten })
        .exists?(stores: { discovery_platform: false })
  end

  private

  def create_renewal_items
    return if renewed_items.blank?

    if renewed_items.instance_of?(Array)
      renewed_items.each do |renewed_item|
        items.create(related: renewed_item)
      end
    else
      items.create(related: renewed_items)
    end
  end

  def create_renewal_history
    history.create(
      renewal: Renewal.with_renewal_information.find(id),
      purchase: purchase
    )
  end

  def extend_renewal
    RenewalHistory.add_extension(Renewal.with_renewal_information.find(id), purchase)
  end

  def history_with_latest_expiration
    history.order(expires_at: :desc).first
  end
end
