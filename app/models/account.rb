class Account < ApplicationRecord
  include PublishingAdministrationHelper

  has_paper_trail on: [:update], if: ->(account) { account.blocked_changed? }

  belongs_to :person
  has_many :composers

  has_many :publishing_compositions
  has_many :publishing_composers
  has_many :primary_composers,
           -> {
             where(is_primary_composer: true)
           },
           source: :publishing_composers,
           class_name: "PublishingComposer"
  has_many :cowriting_composers,
           -> {
             where(is_primary_composer: false)
           },
           source: :publishing_composers,
           class_name: "PublishingComposer"

  has_one :managing_composer, ->(account) { where(person_id: account.person_id) }, class_name: "Composer"
  has_one :managing_publishing_composer,
          ->(account) {
            where(is_primary_composer: true, person_id: account.person_id)
          },
          class_name: "PublishingComposer"

  has_many :rights_app_errors, as: :requestable
  has_many :songs, through: :person
  validates :account_type, inclusion: { in: %w[publishing_administration] }

  scope :publishing_administration, -> { where(account_type: "publishing_administration") }

  def artist_account_name
    publishing_composer = primary_composers.where(person_id: person_id).first
    return person.name if publishing_composer.blank?

    tax_info = publishing_composer.tax_info

    if tax_info.present?
      name = tax_info.is_entity ? tax_info.entity_name.presence : tax_info.name.presence
    end

    name || publishing_composer.full_name
  end

  def catalogue_code
    return default_catalogue if composer_pros.count > 1

    pro = composer_pros.first

    if pro.present?
      pro_name = pro.name.split(",").first.upcase
      RA_CATALOGUE_CODES[pro_name] || default_catalogue
    else
      default_catalogue
    end
  end

  def composer_pros
    @composer_pros ||= PerformingRightsOrganization.where(
      id: primary_composers.pluck(:performing_rights_organization_id)
    ).compact.uniq
  end

  def default_catalogue
    RA_CATALOGUE_CODES[:BMI]
  end

  def import_from_rights_app
    return unless publishing_composers.any?(&:is_paid)

    publishing_composers.select(&:is_paid).each do |publishing_composer|
      PublishingAdministration::PublishingWriterImporterWorker.perform_async(publishing_composer.id)
    end

    PublishingAdministration::PublishingWorkReconciliationWorker.perform_async(id, true)
  end
end
