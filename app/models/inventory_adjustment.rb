class InventoryAdjustment < ApplicationRecord
  belongs_to :inventory
  belongs_to :posted_by, class_name: "Person"
  belongs_to :related, class_name: "InventoryAdjustment"

  ADJUSTMENT_CATEGORIES = ["Other", "Refund", "Error"]

  before_validation :get_info_for_a_rollback, if: :rollback?

  validates :disclaimer, acceptance: { message: "must be agreed to" }
  validates :category, :posted_by_id, :posted_by_name, :admin_note, :amount, presence: true
  validates :amount, numericality: { only_integer: true, message: "must be an integer" }
  validate :must_refund_less_than_remaining_amount, if: :debit
  validate :must_be_non_zero

  after_create :update_inventory_count
  after_create :update_parent_of_rollback, if: :rollback?

  private

  def update_inventory_count
    inventory.update(quantity_adjustment: inventory.quantity_adjustment + amount)
  end

  def must_refund_less_than_remaining_amount
    return unless !amount.nil? && amount.negative? && (inventory.quantity_remaining + amount).negative?

    errors.add(
      :base,
      I18n.t(
        "models.product.you_cannot_debit_more_than_the_remaining_credit_balance",
        quantity: inventory.quantity_remaining
      )
    )
  end

  def must_be_non_zero
    errors.add(:base, I18n.t("models.product.amount_cannot_equal_0")) if !amount.nil? && amount.zero?
  end

  def debit
    !amount.nil? && amount.negative?
  end

  def rollback?
    rollback == 1
  end

  def get_info_for_a_rollback
    @parent = InventoryAdjustment.find(related_id)
    if !@parent.nil?
      self.amount = (@parent.amount * -1) # rollback gets the uses the opposite amount
      self.category = @parent.category
    else
      errors.add(:base, I18n.t("models.billing.balance_adjustment.system_error_could_not_find_original_transaction"))
    end
  end

  def update_parent_of_rollback
    @parent = InventoryAdjustment.find(related_id)
    @parent.update_attribute(:related_id, id)
  end
end
