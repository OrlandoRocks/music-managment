class RenewalsBatchFailure < ApplicationRecord
  validates :date, presence: true
  validates :resolved, inclusion: [true, false]

  scope :unresolved, -> { where(resolved: false) }
end
