class RefundSettlement < ApplicationRecord
  extend MoneyField

  belongs_to :refund
  belongs_to :source, polymorphic: true

  has_one :outbound_refund, dependent: :destroy

  validates :refund, :source_id, :source_type, presence: true

  money_reader :settlement_amount
end
