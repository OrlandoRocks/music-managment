class RoyaltyPayment < ApplicationRecord
  belongs_to :person
  belongs_to :composer
  belongs_to :publishing_composer
  belongs_to :posting, polymorphic: true

  HOLD_TYPES = {
    missing_tax: "Missing Tax",
    negative_balance: "Negative Balance",
    pending_dispute: "Pending Dispute",
    tax_levy: "Tax Levy",
    child_support_levy: "Child Support Levy",
    pending_signed_lod: "Pending Signed LOD"
  }

  def self.update_transaction(balance_adjustment, batch_detail, period_options)
    year, interval, type = period_options[:period_year], period_options[:period_interval], period_options[:period_type]

    # Note: GC - We are not ready to use composer information since there are royalty payments for
    # songwriter registered outside the system, however, we would still save them if available
    composer_id = Person.find(balance_adjustment.person_id)&.composers&.first&.id
    publishing_composer_id = Person.find_by(id: balance_adjustment.person_id)&.publishing_composers&.primary&.first&.id

    attributes = {
      composer_id: composer_id,
      publishing_composer_id: publishing_composer_id,
      code: batch_detail.code,
      person_id: balance_adjustment.person_id,
      period_year: year,
      period_interval: interval,
      period_type: type
    }

    royalty_payment = where(attributes).first
    royalty_payment ||= create attributes
    royalty_payment.amount = balance_adjustment.credit_amount
    royalty_payment.posting = balance_adjustment
    royalty_payment.hold_payment = batch_detail.payment_held?
    royalty_payment.hold_type = get_hold_type(batch_detail.hold_type)
    royalty_payment.save

    royalty_payment
  end

  def self.get_hold_type(hold_type)
    type = HOLD_TYPES.detect { |_k, v| v == hold_type }
    return type[0].to_s if type
  end

  def downloadable?(user)
    user == person || user.is_administrator?
  end

  def download_pdf_url(_style = nil, include_updated_timestamp = true)
    url = "/royalty_payments/#{id}/#{pdf_summary_file_name}"
    if (include_updated_timestamp && pdf_summary_updated_at)
      [url, pdf_summary_updated_at].compact.join(url.include?("?") ? "&" : "?")
    else
      url
    end
  end

  def download_csv_url(_style = nil, include_updated_timestamp = true)
    url = "/royalty_payments/#{id}/#{csv_summary_file_name}"
    if (include_updated_timestamp && csv_summary_file_updated_at)
      [url, csv_summary_file_updated_at].compact.join(url.include?("?") ? "&" : "?")
    else
      url
    end
  end

  def authenticated_url(format = "pdf", expires_in = 10)
    case format
    when "pdf"
      key = "#{id}/#{pdf_summary_file_name}"
    when "csv"
      key = "#{id}/#{csv_summary_file_name}"
    end

    s3 = Aws::S3::Resource.new
    object = s3.bucket(ROYALTY_REPORTS_BUCKET_NAME).object(key)
    object.presigned_url(:get, expires_in: expires_in)
  end

  def pdf_exists?
    pdf_summary_file_name.present?
  end

  def csv_exists?
    csv_summary_file_name.present?
  end

  def download_pdf_filename
    period_type_symbol =
      case period_type
      when "quarterly"
        "Q"
      when "monthly"
        "M"
      else
        ""
      end
    "#{person_id}_#{period_type_symbol}#{period_interval}_#{period_year}.pdf"
  end

  def download_csv_filename
    period_type_symbol =
      case period_type
      when "quarterly"
        "Q"
      when "monthly"
        "M"
      else
        ""
      end
    "#{person_id}_#{period_type_symbol}#{period_interval}_#{period_year}.csv"
  end

  def missing_tax_info?
    return true if hold_payment && hold_type == "missing_tax"
  end

  def negative_balance?
    return true if hold_payment && hold_type == "negative_balance"
  end

  def show_hold_type
    return HOLD_TYPES[hold_type.to_sym] if hold_type.present?
  end
end
