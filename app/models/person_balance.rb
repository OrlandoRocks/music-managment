class PersonBalance < ApplicationRecord
  belongs_to :person

  validates :person_id, uniqueness: true
  validates :currency, presence: true

  def self.find_and_lock_balance(person)
    balance = person.person_balance
    balance.lock!
    balance
  end

  def update_balance(amount)
    update_attribute(:balance, balance + amount)
  end
end
