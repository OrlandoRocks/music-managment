# frozen_string_literal: true

class VatTaxAdjustment < ApplicationRecord
  belongs_to :person
  belongs_to :related, polymorphic: true
  belongs_to :foreign_exchange_rate
  has_one :outbound_invoice
  has_many :person_transactions, as: :target

  validates :person_id,
            :related_id,
            :related_type,
            :amount,
            :tax_rate,
            presence: true

  delegate :person_balance, to: :person

  PAYOUT_TRANSACTION_COMMENT = "Payout Transaction - VAT"
  VAT_POSTED_COMMENT = "VAT Posted"
  VAT_REVERTED_COMMENT = "VAT Posting Reverted"
  VAT_DEBIT_COMMENT = "VAT Debit"

  def post_vat_transactions
    person_transactions.create(
      person_id: person_id,
      credit: amount_to_money,
      debit: 0,
      currency: person.currency,
      comment: VAT_POSTED_COMMENT,
      previous_balance: person_balance.balance
    )

    person_transactions.create(
      person_id: person_id,
      credit: 0,
      debit: amount_to_money,
      currency: person.currency,
      comment: PAYOUT_TRANSACTION_COMMENT,
      previous_balance: person_balance.balance
    )
  end

  def business_transaction?
    customer_type == VatInformation::BUSINESS
  end

  def self.historic_record(person_id, vat_number, _member_country)
    record = where(
      vat_registration_number: vat_number,
      person_id: person_id
    ).last
    return if record.nil? || !record.business_transaction?

    {
      id: record.id,
      valid: record.business_transaction?,
      trader_name: record.trader_name
    }
  end

  def vat_registered?
    customer_type == VatInformation::BUSINESS
  end

  private

  def amount_to_money
    Money.new(amount, person.currency)
  end
end
