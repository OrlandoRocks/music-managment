class PaymentBatch < ApplicationRecord
  include Logging

  belongs_to :country_website
  has_many :batch_transactions
  has_many :invoices, through: :batch_transactions

  has_many :credit_card_batch_transactions, -> { where matching_type: [BraintreeTransaction.to_s, PaymentsOSTransaction.to_s] }, class_name: BatchTransaction.to_s
  has_many :braintree_batch_transactions, -> { where matching_type: BraintreeTransaction.to_s }, class_name: BatchTransaction.to_s
  has_many :payments_os_batch_transactions, -> { where matching_type: PaymentsOSTransaction.to_s }, class_name: BatchTransaction.to_s
  has_many :paypal_batch_transactions, -> { where matching_type: PaypalTransaction.to_s }, class_name: BatchTransaction.to_s
  has_many :adyen_batch_transactions, -> { where matching_type: AdyenTransaction.to_s }, class_name: BatchTransaction.to_s
  has_many :balance_batch_transactions, -> { where matching_type: PersonTransaction.to_s }, class_name: BatchTransaction.to_s
  has_many :unprocessed_batch_transactions, -> { where matching_type: nil }, class_name: BatchTransaction.to_s
  has_many :pending_batch_transactions, -> { where processed: "pending" }, class_name: BatchTransaction.to_s
  has_many :finished_batch_transactions, -> { where.not processed: "pending" }, class_name: BatchTransaction.to_s

  has_many :invoice_logs

  after_initialize :initialize_batch_counts, if: :new_record?

  def batch_count
    batch_transactions.count
  end

  def balance_count
    balance_batch_transactions.count
  end
  alias_method :balance_count_total, :balance_count
  alias_method :balance_count_successful, :balance_count

  def balance_amount_successful
    balance_batch_transactions.sum(:amount)
  end

  def braintree_count
    braintree_batch_transactions.count
  end
  alias_method :braintree_count_total, :braintree_count
  alias_method :braintree_count_successful, :braintree_count

  def adyen_count
    adyen_batch_transactions.count
  end
  alias_method :adyen_count_total, :adyen_count
  alias_method :adyen_count_successful, :adyen_count

  def payments_os_count
    payments_os_batch_transactions.count
  end
  alias_method :payments_os_count_total, :payments_os_count
  alias_method :payments_os_count_successful, :payments_os_count

  def sent_back_count
    unprocessed_batch_transactions.count
  end

  def paypal_count
    paypal_batch_transactions.count
  end
  alias_method :paypal_count_total, :paypal_count
  alias_method :paypal_count_successful, :paypal_count

  def transactions_not_processed
    unprocessed_batch_transactions
  end

  def total_amount
    batch_transactions.sum(:amount)
  end

  def balance_amount
    balance_batch_transactions.sum(:amount)
  end

  def cannot_process_amount
    unprocessed_batch_transactions.sum(:amount)
  end

  def paypal_amount_total
    paypal_batch_transactions.sum(:amount)
  end
  alias_method :paypal_amount_successful, :paypal_amount_total

  def braintree_amount_total
    braintree_batch_transactions.sum(:amount)
  end
  alias_method :braintree_amount_successful, :braintree_amount_total

  def payments_os_amount_total
    payments_os_batch_transactions.sum(:amount)
  end
  alias_method :payments_os_amount_successful, :payments_os_amount_total

  def adyen_amount_total
    adyen_batch_transactions.sum(:amount)
  end
  alias_method :adyen_amount_successful, :adyen_amount_total

  def create_transactions_for_invoices(local_invoices)
    transactions   = []
    failure_total  = 0.0
    failure_count  = 0

    # create a batch transaction for each invoice
    local_invoices.each do |invoice|
      # check if invoice has been processed too many times
      if invoice.batch_attempts.to_i < BRAINTREE_BATCH[:run_limit].to_i
        next if invoice.settled?

        # Create the transaction
        if country_website_id == invoice.country_website_id
          person = invoice.person
          transaction = BatchTransaction.create(
            invoice: invoice,
            currency: invoice.currency,
            payment_batch_id: id,
            stored_credit_card_id: person.renew_with_credit_card? ? person.preferred_credit_card_id : nil,
            adyen_stored_payment_method_id: person.renew_with_adyen? ? person.preferred_adyen_payment_method.id : nil,
            stored_paypal_account_id: person.renew_with_paypal? ? person.preferred_paypal_account.id : nil,
            amount: invoice.amount
          )
          transactions << transaction

          message = "Create BatchTransaction"
          options = {
            person: person,
            invoice: invoice,
            payment_batch_id: id,
            batch_transaction_id: transaction&.id,
            message: message,
            current_method_name: __callee__,
            caller_method_path: caller&.first
          }
          InvoiceLogService.log(options)

          # update invoice to processing
          log(
            "Payment Batch",
            "Create BatchTransaction",
            currency: invoice.currency,
            country_website: country_website.name,
            batch: id,
            invoice_id: invoice.id,
            transaction: transaction.id,
            person: invoice.person.id,
            amount: transaction.amount
          )
          invoice.update(batch_status: "processing_in_batch")
          self.total_amount += invoice.amount
        else
          comment = "Error - Invoice being processed in wrong country payment_batch"
          message = "comment: #{comment}, invoice_country_website: #{invoice&.country_website&.name},
batch_country_website: #{self&.country_website&.name}"
          person = invoice&.person
          options = {
            person: person,
            invoice: invoice,
            payment_batch_id: id,
            message: message,
            current_method_name: __callee__,
            caller_method_path: caller&.first
          }
          InvoiceLogService.log(options)

          log_params = {
            batch_country_website: country_website.name,
            invoice_country_website: invoice.country_website.name,
            batch: id,
            invoice_id: invoice.id,
            person: invoice.person.id
          }
          log(
            "Payment Batch",
            comment,
            log_params
          )
          Airbrake.notify(message, log_params)
        end

      else
        person = invoice&.person
        comment = "Credit Card Failed more than limit"
        message = "comment: #{comment}, invoice_amount: #{invoice&.amount}, email_sent_to: #{person&.email}"
        options = {
          person: person,
          invoice: invoice,
          payment_batch_id: id,
          message: message,
          current_method_name: __callee__,
          caller_method_path: caller&.first
        }
        InvoiceLogService.log(options)

        log(
          "Payment Batch",
          comment,
          batch: id,
          invoice: invoice.id,
          person: invoice.person.id,
          amount: invoice.amount
        )

        BatchNotifier.batch_failure_notice(invoice).deliver unless invoice.purchases.empty?
        log_params = {
          batch: id,
          invoice: invoice.id,
          person: invoice.person_id,
          amount: invoice.amount,
          email: invoice.person.email
        }
        log(
          "Payment Batch",
          "Notification for renewal sent to customer - over failure limit",
          log_params
        )

        invoice.update(batch_status: "visible_to_customer")

        failure_total += invoice.amount
        failure_count += 1
        Airbrake.notify(message, log_params)
      end
    end

    update_attribute(:total_amount, self.total_amount)
    if failure_count.positive?
      log("Payment Batch", "Notification emails for multiple failures sent", batch: id, amount: failure_total, count: failure_count)
    end
    log("Payment Batch", "Update Batch Total Amount", batch: id, amount: self.total_amount)

    transactions
  end

  def initialize_batch_counts
    self.paypal_amount_total              = 0
    self.paypal_amount_successful         = 0
    self.balance_amount                   = 0
    self.braintree_amount_total           = 0
    self.braintree_amount_successful      = 0
    self.adyen_amount_total               = 0
    self.adyen_amount_successful          = 0
    self.cannot_process_amount            = 0
    self.total_amount                     = 0
    self.payments_os_amount_total         = 0
    self.payments_os_amount_successful    = 0
  end

  def process_transactions(transactions = nil)
    transactions = batch_transactions if transactions.blank?

    if transactions.present?
      # Process each transaction in the batch
      transactions.each do |transaction|
        transaction.process
      rescue => e
        invoice = transaction&.invoice
        transaction&.failed!
        invoice&.remove_batch_status
        person = invoice&.person
        error_message = e&.message
        category = "Payment Exception"
        message = "category: #{category}, error_message: #{error_message}"
        options = {
          person: person,
          invoice: invoice,
          payment_batch_id: id,
          batch_transaction_id: transaction&.id,
          message: message,
          current_method_name: __callee__,
          caller_method_path: caller&.first
        }
        InvoiceLogService.log(options)

        log_params = {
          batch: id,
          invoice: transaction.invoice_id,
          person: transaction.invoice.person_id
        }
        log(
          category,
          e.message,
          log_params
        )
        Airbrake.notify(message, log_params)
        next
      end
      true
    else
      false
    end
  end

  #
  # Returns the number of people that have X amount of invoices in the batch
  # and the sum of these invoices
  #
  def invoice_split_information(invoice_count = 1)
    sql = %{SELECT count(*), sum(total_gained) FROM (SELECT invoices.person_id, payment_batches.batch_date, count(invoices.id) as num_invoices, sum(final_settlement_amount_cents) as total_gained
                                                     FROM payment_batches inner join batch_transactions on payment_batch_id = payment_batches.id
                                                                           inner join invoices           on invoices.id      = batch_transactions.invoice_id
                                                     WHERE payment_batches.id = #{id} group by invoices.person_id having num_invoices = #{invoice_count}) t}

    ActiveRecord::Base.connection.execute(sql).first
  end

  def summary
    @_summary ||= ActiveRecord::Base.connection.exec_query(
      "SELECT
	      sum(purchases.cost_cents) AS amount,
	      products.applies_to_product AS renewal_type,
	      batch_transactions.processed AS status
      FROM
	      purchases
	      INNER JOIN batch_transactions ON purchases.invoice_id = batch_transactions.invoice_id
	      INNER JOIN products ON purchases.product_id = products.id
      WHERE
	      batch_transactions.payment_batch_id = #{id}
	      AND products.applies_to_product IN('Album', 'Ringtone', 'Single')
      GROUP BY
	      products.applies_to_product,
	      batch_transactions.processed;"
    )
  end

  def filter_sum(renewal_type = nil, status = nil)
    running_summary = summary
    running_summary = running_summary.select { |s| s["renewal_type"] == renewal_type } if renewal_type.present?
    running_summary = running_summary.select { |s| s["status"] == status } if status.present?
    running_summary.sum { |s| s["amount"] }
  end

  def complete_batch
    return if status == "complete"

    # Save totals
    update(
      balance_amount: balance_amount,
      braintree_amount_total: braintree_amount_total,
      braintree_amount_successful: braintree_amount_successful,
      paypal_amount_total: paypal_amount_total,
      paypal_amount_successful: paypal_amount_successful,
      adyen_amount_total: adyen_amount_total,
      adyen_amount_successful: adyen_amount_successful,
      payments_os_amount_total: payments_os_amount_total,
      payments_os_amount_successful: payments_os_amount_successful,
      status: "complete"
    )

    # Log if there are failed balance transactions
    if balance_count != balance_count_successful
      log("Payment Batch", "Not all of the balance transactions were successful", batch: id, amount: balance_amount_successful, count: balance_count_successful)
    end

    # Process the failed transactions here
    if !transactions_not_processed.empty?
      log("Payment Batch", "Begin transactions not processed emails", batch: id, transactions_not_processed: transactions_not_processed.size)
      transactions_not_processed.each do |transaction|
        # Send the failure email
        next unless transaction.invoice

        begin
          BatchNotifier.batch_failure_notice(transaction.invoice).deliver
          log("Payment Batch", "Notification for renewal sent to customer - cannot process", batch: id, invoice: transaction.invoice_id, person: transaction.invoice.person_id, amount: transaction.amount)
        rescue => e
          Airbrake.notify(
            e, {
              payment_batch_id: id,
              transaction_id: transaction.id
            }
          )
        end
      end

      BatchSummaryNotifier.not_processed_summary(transactions_not_processed).deliver
      log("Payment Batch", "Not Processed Notification totals", batch: id, count: transactions_not_processed.size, amount: cannot_process_amount)
    else
      log("Payment Batch", "No transactions were failed and returned to the customer", batch: id, amount: cannot_process_amount)
    end

    log("Payment Batch", "Pay with Balance totals",                          batch: id, amount: balance_amount,         count: balance_count)
    log("Payment Batch", "Pay with Paypal totals",                           batch: id, amount: paypal_amount_total,    amount_successful: paypal_amount_successful, total_count: paypal_count_total, success_count: paypal_count_successful)
    log("Payment Batch", "Pay with Credit Card totals - sent to braintree ", batch: id, amount: braintree_amount_total, total_count: braintree_count_total, amount_successful: braintree_amount_successful, success_count: braintree_count_successful)
    log("Payment Batch", "Pay with Adyen totals", batch: id, amount: adyen_amount_total, total_count: adyen_count_total, amount_successful: adyen_amount_successful, success_count: adyen_count_successful)

    log(
      "Payment Batch",
      "Pay with Credit Card totals - sent to payments os ",
      batch: id,
      amount: payments_os_amount_total,
      total_count: payments_os_count_total,
      amount_successful: payments_os_amount_successful,
      success_count: payments_os_count_successful
    )

    BatchSummaryNotifier.batch_summary(id).deliver
    BatchSummaryNotifier.cc_failure_summary(id).deliver

    # Destroy empty batches
    return unless self.total_amount.zero?

    log("Payment Batch", "Empty Batch Destroyed", batch: id, amount: self.total_amount)
    destroy
  end

  def self.search(params)
    search = all

    if params
      search = search.where("id = ?", params[:id]) if params[:id].present?
      if params[:batch_date_min].present?
        search = search.where("batch_date >= ?", Date.strptime(params[:batch_date_min], "%m/%d/%Y"))
      end
      if params[:batch_date_max].present?
        search = search.where("batch_date <= ?", Date.strptime(params[:batch_date_max], "%m/%d/%Y"))
      end
    end

    Tunecore::Search.new(params, search)
  end
end
