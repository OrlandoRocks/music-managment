class ImmersiveAudio < ApplicationRecord
  belongs_to :song
  belongs_to :s3_asset, dependent: :destroy
  has_one :album, through: :song
  has_one :person, through: :song
  has_one :purchase, as: :related, dependent: :nullify

  alias_attribute :song_file, :s3_asset

  after_initialize :set_audio_type
  before_save :set_audio_type
  after_create :create_and_store_content_fingerprint!

  enum audio_type: { dolby_atmos: "dolby_atmos" }

  include ContentFingerprintable

  private

  def set_audio_type
    self.audio_type = self.class::AUDIO_TYPE if self.class.const_defined?("AUDIO_TYPE")
  end
end
