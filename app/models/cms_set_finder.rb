class CmsSetFinder
  def self.active_sets
    # For each unique group of [country_website_id, callout_type] in cms_sets,
    # find the max date (that is less than now).
    #
    # Then, get every record in cms_sets that matches one of the resulting
    # (max_date, callout_type, country_website_id) groupings.

    CmsSet.find_by_sql([<<~SQL, Time.current])
      SELECT *
      FROM (
        SELECT
          *,
          MAX(`start_tmsp`) AS max_date
        FROM `cms_sets`
        WHERE
          (`start_tmsp` <= ?)
        GROUP BY
          `country_website_id`,
          `country_website_language_id`,
          `callout_type`
      ) latest_set
      INNER JOIN
        `cms_sets`
      WHERE
        `cms_sets`.`start_tmsp`             = latest_set.`max_date`
        AND `cms_sets`.`callout_type`       = latest_set.`callout_type`
        AND `cms_sets`.`country_website_id` = latest_set.`country_website_id`
        AND `cms_sets`.`country_website_language_id` = latest_set.`country_website_language_id`
    SQL
  end

  def self.active_set(callout_type, country, country_website_language)
    CmsSet
      .joins(:country_website)
      .joins(:country_website_language)
      .where(callout_type: callout_type)
      .where("cms_sets.start_tmsp <= ?", Time.current)
      .where("country_websites.country = ?", country)
      .where("country_website_languages.selector_country = ?", country_website_language)
      .order(start_tmsp: :desc)
      .first
  end

  def self.upcoming_sets
    CmsSet
      .where("cms_sets.start_tmsp > ?", Time.current)
      .order("start_tmsp ASC")
  end

  def self.unscheduled_and_archived_sets
    CmsSet.find_by_sql([<<~SQL, Time.current, Time.current])
      SELECT
        `cms_sets`.*
      FROM
        `cms_sets`
      INNER JOIN
        `country_website_languages` ON `country_website_languages`.`id` = `cms_sets`.`country_website_language_id`
      LEFT OUTER JOIN (
        SELECT
          `cms_sets`.*,
          MAX(`cms_sets`.`start_tmsp`) AS max_date
        FROM
          `cms_sets`
        WHERE
          (`cms_sets`.`start_tmsp` <= ?)
        GROUP BY
          `cms_sets`.`country_website_language_id`,
          `cms_sets`.`callout_type`
      ) latest_set
        ON
          `cms_sets`.`callout_type` = latest_set.`callout_type`
          AND `cms_sets`.`start_tmsp` = latest_set.`max_date`
          AND `cms_sets`.`country_website_language_id` = latest_set.`country_website_language_id`
      WHERE (
        (
          `cms_sets`.`start_tmsp` IS NULL OR
          `cms_sets`.`start_tmsp` < ?
        )
        AND latest_set.`callout_type` IS NULL
        AND `cms_sets`.`callout_name` != 'None'
      )
      ORDER BY
        cms_sets.country_website_language_id
    SQL
  end
end
