# frozen_string_literal: true

class BatchBalanceAdjustment < ApplicationRecord
  has_many :details, class_name: "BatchBalanceAdjustmentDetail"

  after_save :summarize_earnings, if: proc { summarize_earnings? }
  after_save :withhold_taxes, if: proc { withhold_taxes? }

  def self.ingest_file(filename, options = {})
    raise "File #{filename} was already ingested" if duplicate_ingestion?(filename)

    file = "#{BATCH_BALANCE_ADJUSTMENTS_UPLOAD_PATH}/#{filename}"
    batch_csv = CSV.read(file, { headers: true }.merge!(options))

    batch_adjustment = nil
    transaction do
      batch_adjustment = BatchBalanceAdjustment.create(ingested_filename: filename)
      batch_csv.each do |row|
        required_rows = ["Tunecore Account Code", "Code", "Name", "Minimum Payment", "Hold Payment", "Current Balance"]
        optional_rows = ["Hold Type"]

        unless (required_rows - row.headers).empty?
          raise "Columns must be named #{required_rows.join(',')},#{optional_rows.join(',')}. Please check your ingestion file and try again."
        end

        person_id = row["Tunecore Account Code"]
        raise "Invalid person_id of #{person_id}" if person_id.to_i.zero?

        if row["Minimum Payment"].nil? || row["Current Balance"].nil?
          raise "Minimum Payment or Current Balance is empty, which means there are potentially bad data. The row that failed is #{row}"
        end

        detail = BatchBalanceAdjustmentDetail.create(
          person_id: person_id,
          code: row["Code"],
          name: row["Name"],
          minimum_payment: row["Minimum Payment"].delete("$"),
          hold_payment: row["Hold Payment"],
          current_balance: row["Current Balance"].delete("$"),
          hold_type: row["Hold Type"]
        )

        raise detail.errors.full_messages.to_s unless detail.valid?

        batch_adjustment.details << detail
      end
    end

    batch_adjustment
  end

  def posted?
    status == "posted"
  end

  def self.duplicate_ingestion?(filename)
    BatchBalanceAdjustment.exists?(ingested_filename: filename)
  end

  def summarize_earnings
    # NOTE: Disabling this as part of CF-684
    # Because summarization is done at point of withdrawal at a person level
    # This summarization can be considered redundant

    # TaxBlocking::BatchPersonEarningsSummarizationWorker.perform_async(batch_balance_adjustment_ids: id)
  end

  def summarize_earnings?
    FeatureFlipper.show_feature?(:advanced_tax_blocking) &&
      saved_change_to_attribute?("status", to: "posted")
  end

  def withhold_taxes?
    FeatureFlipper.show_feature?(:us_tax_withholding) &&
      saved_change_to_attribute?("status", to: "posted")
  end

  def withhold_taxes
    TaxWithholdings::MassAdjustmentsBatchWorker.perform_async
  end
end
