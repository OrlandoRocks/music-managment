class TargetedNotification < ApplicationRecord
  belongs_to :person
  belongs_to :notification_icon

  validates :text, :title, :url, :link_text, :person, presence: true

  has_many :notifications, dependent: :destroy

  def add_people_from_csv(csv_file)
    return false if global?

    person_ids = []
    CSV.parse(csv_file.read) do |row|
      person_ids << row[0].to_i if row and !row[0].index(/^[0-9]+/).nil?
    end

    # Remove people that are already in notification
    people_already_in = Person.joins(:notifications).where("targeted_notification_id = ?", id).collect(&:id)
    person_ids -= people_already_in

    # Remove invalid ids
    valid_ids = Person.where("id in (?)", person_ids).collect(&:id)

    [].tap do |notifications|
      # Add the remaining people
      valid_ids.each do  |person_id|
        notifications << Notification.build_from_targeted_notification(self, person_id)
      end

      Notification.import notifications
    end
  end

  def self.new_global_notifications(person)
    joins(
      %Q(
      left join notifications on targeted_notification_id = targeted_notifications.id
      and notifications.person_id = #{person.id}
     )
    )
      .where("notifications.id is null and targeted_notifications.created_at >= STR_TO_DATE('#{person.created_on}', '%Y-%m-%d %H:%i:%s') and global = ?", true)
  end
end
