# frozen_string_literal: true

class PlanDowngradeRequest < ApplicationRecord
  belongs_to :requested_plan, class_name: "Plan"
  belongs_to :person
  belongs_to :reason, class_name: "DowngradeCategoryReason"
  has_one :downgrade_other_reason, dependent: :destroy

  ACTIVE_STATUS = "Active"
  PROCESSED_STATUS = "Completed"
  CANCELED_STATUS = "Canceled"

  VALID_STATUSES = [
    ACTIVE_STATUS,
    PROCESSED_STATUS,
    CANCELED_STATUS
  ].freeze

  validates :person, :requested_plan, :reason, presence: true
  validates :person, uniqueness: { scope: :status }, if: -> { status == ACTIVE_STATUS }
  validate :eligible_plan_for_downgrade
  validate :active_request, on: :cancel
  validates :status, inclusion: VALID_STATUSES

  scope :active, -> { where(status: ACTIVE_STATUS) }
  scope :inactive, -> { where.not(status: ACTIVE_STATUS) }

  def cancel
    # to prevent modifying "Completed" requests
    return false unless valid?(:cancel)

    update(status: CANCELED_STATUS)
  end

  private

  def eligible_plan_for_downgrade
    # to allow for marking a status as "Completed" after downgrade is performed and
    # "Canceled" without requiring validation
    return if status != ACTIVE_STATUS

    person = Person.find_by(id: person_id)
    return if person.blank?

    errors.add(:base, I18n.t("no_person_plan")) if person.plan.blank?
    return if person.plan.blank?

    lower_plans = Plans::PlanEligibilityService.lower_plans_by_downgrade_status(person)
    eligible_plan_ids =
      lower_plans.find { |plan|
        plan[:plan].id == requested_plan_id && plan[:downgrade_eligible] == true
      }
    errors.add(
      :base,
      I18n.t("ineligible_plan")
    ) if eligible_plan_ids.blank?
  end

  def active_request
    errors.add(
      :base,
      "Not an active request"
    ) if status != ACTIVE_STATUS
  end
end
