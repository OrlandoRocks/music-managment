#!/bin/env ruby
# encoding: utf-8

# == Schema Information
# Schema version: 404
#
# Table name: check_transfers
#
#  id                    :integer(11)     not null, primary key
#  created_at            :datetime        not null
#  updated_at            :datetime        not null
#  transfer_status       :string(20)      default(""), not null
#  person_id             :integer(11)     default(0), not null
#  payment_cents         :integer(11)     default(0), not null
#  admin_charge_cents    :integer(11)     default(0), not null
#  payee                 :text            not null
#  address1              :text            not null
#  address2              :text
#  city                  :text            not null
#  state                 :text            not null
#  postal_code           :string(10)      default(""), not null
#  country               :string(56)      default(""), not null
#  phone_number          :string(255)
#  backend               :string(255)     default("ManualCheckTransfer")
#  debit_transaction_id  :integer(11)
#  refund_transaction_id :integer(11)
#  export                :boolean(1)
#  export_date           :datetime
#

class CheckTransfer < ApplicationRecord
  include CurrencyHelper
  include Tunecore::Flagging::Suspicious
  include LoginTrackable
  include WorkflowActiverecord

  MIN_AMOUNT = 100.00

  self.inheritance_column = "backend"

  belongs_to :person
  belongs_to :debit_transaction, class_name: "PersonTransaction"
  belongs_to :refund_transaction, class_name: "PersonTransaction"
  belongs_to :approved_by, class_name: "Person"

  # provided for backwards compatibility
  alias_attribute :person_transaction, :debit_transaction
  attr_accessor :password_entered
  attr_accessor :current_balance_cents, :payment_method

  before_validation :set_currency
  validates :transfer_status, :person_id, presence: true
  validates :payee, :city, :country, :address1, presence: true
  validates :state, presence: { if: :us_or_canada? }
  validates :postal_code, presence: { if: :country_known_to_have_postal_code?, message: "can't be blank for selected country" }
  validates :currency, presence: true

  validates :transfer_status, inclusion: { in: %w[pending processing processed completed failed cancelled] }

  validate :valid_payment_amount
  validate :verify_password, on: :create

  workflow_column :transfer_status

  # Finite State Machine
  workflow do
    # Indicates that the transfer is 'pending Admin approval'
    state :pending do
      event :mark_as_processing, transitions_to: :processing
      event :mark_as_cancelled,  transitions_to: :cancelled
    end

    # Indicates that the transfer is currently being processed or in the Mail
    state :processing do
      event :revert_to_pending, transitions_to: :pending
      event :mark_as_completed, transitions_to: :completed
      event :mark_as_failed, transitions_to: :failed
    end

    # Indicates that the transfer was received and processed by Paypal
    state :completed

    # Indicates that the transfer cancelled by the Admin
    state :cancelled

    # Indicates ?
    state :failed
  end

  def self.generate(person, params = {})
    # using self.new because we have ManualCheckTransfer as a baseclass
    check_transfer = new(params)
    check_transfer.person = person
    check_transfer.transfer_status = "pending"
    check_transfer.current_balance_cents = Tunecore::Numbers.decimal_to_cents(PersonBalance.find_by(person_id: person).balance)

    # 6/10/11 DCD - Mysql has a NOT NULL on the postal_code and state column, and we are tearing out this code soon,
    # so this is a hack to avoid a data migration
    if check_transfer.postal_code.nil? && !check_transfer.country_known_to_have_postal_code?
      check_transfer.postal_code = ""
    end
    # another hack
    check_transfer.state = "" if check_transfer.state.nil? && !check_transfer.us_or_canada?

    return check_transfer unless check_transfer.save

    # create a person_transaction for check amount and service fee
    debit = check_transfer.create_debit_transaction(
      person: person,
      debit: Tunecore::Numbers.cents_to_decimal(check_transfer.payment_cents),
      credit: 0,
      target: check_transfer,
      comment: "Check paid to: #{check_transfer.payee}"
    )
    check_transfer.update(debit_transaction: debit) # update the check transfer with the person_transaction
    check_transfer.create_debit_transaction(
      person: person,
      debit: Tunecore::Numbers.cents_to_decimal(check_transfer.admin_charge_cents),
      credit: 0,
      target: check_transfer,
      comment: "Check Service fee"
    )
    CheckTransferMailer.confirmation(check_transfer.id).deliver
    check_transfer
  end

  def self.min_amount
    MIN_AMOUNT
  end

  def cancel(validate = true)
    if reload.transfer_status == "pending"
      # Cancel in a transaction
      PersonTransaction.transaction do
        PersonTransaction.create!(
          person: person,
          debit: 0,
          credit: Tunecore::Numbers.cents_to_decimal(payment_cents),
          target: self,
          comment: "Rollback of Check Payment: #{person_transaction.comment}"
        )
        PersonTransaction.create!(
          person: person,
          debit: 0,
          credit: Tunecore::Numbers.cents_to_decimal(admin_charge_cents),
          target: self,
          comment: "Rollback of Check Service Fee"
        )

        self.transfer_status = "cancelled"
        if validate
          save!
        else
          save(validate: false)
        end
      end

      return true
    end
    false
  rescue StandardError => e
    logger.error(e.to_s)
    false
  end

  def set_to_export!
    self.export = true if country && country.length == 2
  end

  def payment_amount=(arg)
    # FIXME: uk-currency
    arg.tr_s!(",#{USED_CURRENCY} ", "") if arg.is_a? String
    self[:payment_cents] = (LongDecimal(arg).round_to_scale(2, LongDecimal::ROUND_DOWN).int_val rescue arg)
    self[:admin_charge_cents] = (PayoutServiceFee.current("check_service").amount * 100).to_s.to_i # this should be refactored to
  end

  def payment_amount
    return if self[:payment_cents].nil?

    LongDecimal(self[:payment_cents], 2) rescue LongDecimal(0, 2)
  end

  def payment_amount_in_money
    Money.new(payment_cents, currency)
  end

  def payment_amount_before_type_cast
    return if payment_cents_before_type_cast.nil?

    LongDecimal(payment_cents_before_type_cast, 2)
  end

  def total_cents
    Integer(payment_cents) + Integer(admin_charge_cents)
  end

  def us_or_canada?
    ["US", "CA"].include?(country)
  end

  # some countries have postal codes, some do not (like Ireland). Instead of removing the validation,
  # or creating an exhaustive list of all the countries in the world, i'm just doing the validation for
  # well known countries that have withdrawals in our system, so that people don't forget it.
  def country_known_to_have_postal_code?
    [
      "US",
      "USA",
      "CA",
      "GB",
      "AU",
      "BE",
      "IS",
      "CH",
      "PR",
      "IL",
      "IT",
      "NL",
      "NZ",
      "RU",
      "CZ",
      "GT",
      "AR",
      "DE",
      "ES",
      "FR",
      "AT",
      "AD",
      "KY",
      "JP",
      "IN",
      "NO",
      "DK",
      "CR",
      "SE",
      "CL",
      "MX",
      "RO",
      "CN",
      "DO",
      "UA",
      "SI",
      "BM",
      "AM",
      "HN",
      "KW"
    ].include?(country)
  end

  def approving_transfer(user_id)
    set_to_export!
    self.transfer_status = "processing"
    self.approved_by_id = user_id
    save!
  end

  def set_suspicious_flags
    self.suspicious_flags = Tunecore::Flagging::TransferFlagger.new(
      self,
      person.check_transfers,
      :payee
    ).set_flags
  end

  private

  def valid_payment_amount
    current_balance = person.person_balance.balance.to_money(person.person_balance.currency)
    admin_charge = Money.new(admin_charge_cents, currency)
    payment = Money.new(payment_cents, currency)

    if new_record? && (current_balance - payment - admin_charge).negative?
      errors.add(:payment_amount, I18n.t("models.billing.must_be_less_than_your_current_balance_minus_the_service_fee_your_maximum_withdrawal", max_withdrawal: format_to_currency(current_balance - admin_charge)))
    end

    return unless payment_amount < MIN_AMOUNT

    errors.add(:payment_amount, I18n.t("models.billing.minimum_check_amount_is_please_request_paypal_for_lower_amounts", min_withdrawal: format_to_currency(MIN_AMOUNT.to_money(currency))))
  end

  def verify_password
    return unless (password_entered.nil? || person.authenticate!(password_entered).nil?)

    errors.add(:base, I18n.t("models.billing.sorry_that_password_is_incorrect"))
  end

  def set_currency
    self.currency = person.currency
  end
end
