class YouTubeRoyaltyIntake < ApplicationRecord
  belongs_to :person
  has_many :you_tube_royalty_records

  scope :with_posting_id,
        ->(posting_id) {
          joins(:you_tube_royalty_records)
            .joins("LEFT JOIN outbound_invoices ON you_tube_royalty_intakes.id = outbound_invoices.related_id
            and outbound_invoices.related_type = 'YouTubeRoyaltyIntake'")
            .joins("LEFT JOIN vat_tax_adjustments ON
            you_tube_royalty_intakes.id = vat_tax_adjustments.related_id
            AND vat_tax_adjustments.related_type = 'YouTubeRoyaltyIntake'")
            .where(you_tube_royalty_records: { posting_id: posting_id }, outbound_invoices: { id: nil })
            .order("you_tube_royalty_intakes.id")
            .distinct
            .select('you_tube_royalty_intakes.id AS id,
             you_tube_royalty_intakes.person_id AS person_id,
             vat_tax_adjustments.id AS vat_tax_adjustment_id')
        }
end
