class SongStartTime < ApplicationRecord
  belongs_to :song
  belongs_to :store

  validates :song, presence: true
  validates :store, presence: true
  validates :start_time, presence: true
  validates :song_id, uniqueness: { scope: :store_id }
end
