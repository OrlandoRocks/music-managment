class DeliveryBatchItem < ApplicationRecord
  belongs_to :deliverable, polymorphic: true
  belongs_to :delivery_batch
  has_one :delivery_manifest_item

  delegate :xml_remote_path, :uuid, :upc, :xml_hashsum, :takedown, to: :delivery_manifest_item

  scope :sent, -> { where(status: "sent") }

  def mark_as_sent
    update(status: "sent")
  end

  def add_to_manifest(params = {})
    create_delivery_manifest_item(params)
  end

  def remove_from_batch
    update(status: "removed")
    delivery_manifest_item.try(&:destroy)
  end
end
