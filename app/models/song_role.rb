class SongRole < ApplicationRecord
  include Translatable
  SONGWRITER_ROLE_ID = 3
  attr_translate :role_type

  has_many :creative_song_roles, dependent: :destroy
  has_many :creatives, through: :creative_song_roles

  has_many :ddex_song_roles
  has_many :ddex_roles, through: :ddex_song_roles
end
