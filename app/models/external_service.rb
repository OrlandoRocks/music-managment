class ExternalService < ApplicationRecord
  #
  # Associations
  #

  has_many    :external_services_people
  has_many    :people, source: :person, through: :external_services_people

  #
  # Validations
  #

  validates :name, presence: true

  def self.facebook
    @facebook ||= ExternalService.where("name = ?", "facebook").first
  end
end
