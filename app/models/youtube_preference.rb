class YoutubePreference < ApplicationRecord
  has_paper_trail

  #
  # associations
  #

  belongs_to :person

  #
  # validations
  #

  validates :person, presence: true
  validates :person_id, uniqueness: true

  validates :mcn,        inclusion: { in: [true, false] }
  validates :whitelist,  inclusion: { in: [true, false] }
  validates  :mcn_agreement, presence: { if: :mcn }
  validates  :channel_id, presence: true

  validate :validate_changes, on: :update

  def mcn=(mcn_val)
    self[:mcn] = mcn_val

    self.mcn_agreement = current_time_from_proper_timezone if mcn && mcn_agreement.blank?
  end

  def reset_preferences
    update_attribute(:mcn, 0)
    update_attribute(:channel_id, "")
    update_attribute(:whitelist, 0)
    update_attribute(:mcn_agreement, nil)
  end

  #
  # private methods
  #

  private

  def validate_changes
    errors.add :base, "To cancel your MCN agreement, please contact customer care." if !mcn and mcn_changed?
  end
end
