class AlbumFlaggedContent < ApplicationRecord
  serialize :flagged_content, QuirkyJson

  self.table_name = "album_flagged_content"
  belongs_to :album
end
