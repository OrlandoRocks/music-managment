class ManualApproval < ApplicationRecord
  belongs_to :approveable, polymorphic: true
  belongs_to :created_by, class_name: "Person", optional: false
end
