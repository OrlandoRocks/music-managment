class PublishingComposer < ApplicationRecord
  include Tunecore::Publishing::NewSummaryReport
  include PrimaryComposerable

  has_paper_trail on: [:update], only: [:cae]

  PUBLISHER_ADMIN = {
    BMI: "TuneCore Digital Music (BMI)",
    ASCAP: "TuneCore Publishing (ASCAP)",
    SESAC: "TuneCore Songs (SESAC)"
  }.with_indifferent_access.freeze

  SONGWRITER_PROS = %w[
    ABRAMUS
    AEPI
    AKM
    APRA
    ASCAP
    BMI
    BUMA
    GEMA
    IMRO
    JACAP
    JASRAC
    KODA
    PRS
    RAO
    SABAM
    SACEM
    SACM
    SADAIC
    SAMRO
    SESAC
    SGAE
    SIAE
    SOCAN
    SPA
    STIM
    SUISA
    TONO
  ].freeze

  belongs_to :account
  belongs_to :person
  belongs_to :publisher
  belongs_to :publishing_role
  belongs_to :performing_rights_organization
  belongs_to :lod
  belongs_to :legacy_composer, class_name: "Composer"

  has_many :publishing_composition_splits
  has_many :publishing_compositions, through: :publishing_composition_splits

  has_many :notes, -> { order(created_at: :desc) }, as: :related
  has_many :related_purchases, as: :related, class_name: "Purchase"
  has_many :rights_app_errors, as: :requestable
  has_many :royalty_payments
  has_many :non_tunecore_albums
  has_many :non_tunecore_songs, through: :non_tunecore_albums
  has_many :legal_documents, as: :subject, class_name: "LegalDocument"

  has_one :terminated_composer
  has_one :tax_info, dependent: :destroy
  has_one :paper_agreement, as: :related

  before_save :nullify_provider_identifier

  after_create :create_unallocated_sum_publishing_composition

  validates :cae, presence: { if: :has_pro_songwriter_affiliation?, unless: :skip_cae_validation }
  validates :performing_rights_organization_id, presence: { if: :has_cae?, unless: :skip_cae_validation }

  validates :first_name,  length: { maximum: 45 }, allow_blank: true
  validates :last_name,   length: { maximum: 45 }, allow_blank: true
  validates :middle_name, length: { maximum: 100, allow_nil: true }
  validates :cae, length: { in: 9..11, allow_blank: true, unless: :skip_cae_validation }

  validates :cae, numericality: { allow_blank: true }

  attr_accessor :agreed_to_terms, :skip_cae_validation

  scope :is_paid,
        -> {
          left_outer_joins(:legacy_composer)
            .left_outer_joins(:related_purchases)
            .joins("LEFT JOIN purchases as composer_purchases ON composer_purchases.related_type = 'Composer' and composer_purchases.related_id = composers.id")
            .where("composer_purchases.paid_at IS NOT NULL OR purchases.paid_at IS NOT NULL")
            .distinct
        }
  scope :active, -> { is_paid.where.not(cae: nil, performing_rights_organization: nil, dob: nil) }
  scope :purchased_pub_admin, -> { is_paid.where(purchases: { related_type: "PublishingComposer", product_id: Product.publishing_admin_product_ids }) }
  scope :primary, -> { where(is_primary_composer: true) }
  scope :cowriting, -> { where(is_primary_composer: false) }
  scope :with_no_provider_identifier, -> { joins(:account).where(accounts: { provider_account_id: nil }) }

  scope :albums_with_splits_count,
        -> {
          select("publishing_composers.*, COUNT(distinct albums.id) AS albums_count")
            .joins(publishing_composition_splits: [publishing_composition: [songs: :album]])
            .order("publishing_composers.id")
            .group("publishing_composers.id")
        }

  scope :albums_finalized_count,
        -> {
          select("publishing_composers.*, count(distinct albums.id) as albums_count")
            .joins(person: :albums)
            .where("albums.finalized_at IS NOT NULL AND albums.takedown_at IS NULL AND albums.deleted_date IS NULL")
            .order("publishing_composers.id")
            .group("publishing_composers.id")
        }

  delegate :completed_w9?, :tax_id, :submitted_tax_id?, :completed_w8ben?, to: :tax_info, allow_nil: true

  def self.with_no_unallocated_sum_composition
    PublishingComposer.primary.find_by_sql(
      "SELECT publishing_composers.* FROM `publishing_composers`
      LEFT OUTER JOIN
      (select publishing_composition_splits.publishing_composer_id, publishing_compositions.name, publishing_compositions.is_unallocated_sum
      from publishing_composition_splits INNER JOIN publishing_compositions
      ON publishing_composition_splits.composition_id = publishing_compositions.id
      where is_unallocated_sum = 1) publishing_composition_splits
      ON publishing_composers.id = publishing_composition_splits.publishing_composer_id
      where publishing_composition_splits.publishing_composer_id is NULL"
    )
  end

  def paid_post_proc
    create_publishing_compositions if create_publishing_compositions_permitted?
  end

  def self.inventory_alias
    Composer.name
  end

  def self.songwriter_pros
    SONGWRITER_PROS
  end

  def self.selected_splits_count(publishing_composer)
    joins(:publishing_compositions)
      .where(id: publishing_composer.id)
      .where(publishing_compositions: { is_unallocated_sum: false })
      .count
  end

  def terminate_pub_admin
    update_columns(terminated_at: Date.today)
  end

  def reactivate_pub_admin
    update_columns(terminated_at: nil)
  end

  def country
    tax_info&.country
  end

  def has_rights_app?
    publishing_administrator&.account
  end

  def name
    "#{first_name} #{last_name}"
  end

  def full_name
    middle_name.present? ? "#{first_name} #{middle_name} #{last_name}" : name
  end

  def full_name_affixed
    [name_prefix, first_name, middle_name, last_name, name_suffix]
      .compact.map(&:strip).reject(&:empty?).join(" ")
  end

  def selected_splits?
    PublishingComposer.selected_splits_count(self).positive?
  end

  def unknown?
    is_unknown
  end

  def current_email
    email.presence || publishing_administrator.email
  end

  def tax_name
    if tax_info
      tax_info.is_entity? ? tax_info.entity_name.to_s : tax_info.name.to_s
    else
      name.to_s
    end
  end

  def publishing_administrator
    account&.person || person
  end

  def publisher_admin_name
    PUBLISHER_ADMIN[publisher.performing_rights_organization.name] || PUBLISHER_ADMIN["BMI"]
  end

  def full_name
    middle_name.present? ? "#{first_name} #{middle_name} #{last_name}" : name
  end

  def has_fully_terminated_composers?
    terminated_composer.present? && terminated_composer.fully_terminated?
  end

  def is_paid
    related_purchases.any?(&:paid_at) || (legacy_composer && legacy_composer.related_purchases.any?(&:paid_at))
  end

  def is_active?
    PublishingAdministration::ComposerStatusService.new(self).active?
  end

  def create_publishing_compositions_permitted?
    song_count = person.songs.where(songs: { publishing_composition_id: nil }).count
    is_paid && !has_fully_terminated_composers? && song_count.positive?
  end

  def paid_at
    @paid_purchase ||= related_and_legacy_purchases.detect { |p| p.paid_at != nil }

    @paid_purchase&.paid_at
  end

  def format_paid_at
    paid_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def format_provider_identifier
    provider_identifier || ""
  end

  def format_registration_date
    agreed_to_terms_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def format_termination_date
    date = terminated_composer.try(:effective_date) || terminated_at
    date.try(:to_date).try(:strftime, "%m/%d/%Y")
  end

  def format_termination_date_input
    Date.try(:strftime, terminated_at, "%m/%d/%Y")
  end

  def format_sync_opted_updated_at
    sync_opted_updated_at.try(:strftime, "%m/%d/%Y") || "-/-/-"
  end

  def find_or_create_terminated_composer(termination_type)
    terminated_composer || TerminatedComposer.new(publishing_composer_id: id, termination_type: termination_type)
  end

  def related_and_legacy_purchases
    [legacy_composer&.related_purchases, related_purchases].flatten.compact
  end

  def lod_status
    return unless lod

    { status: lod.status_text, date: lod.last_status_at }
  end

  def publisher_entity_name
    return "" unless has_publisher?

    publisher.name_with_pro
  end

  def has_publisher?
    publisher.present? && publisher.name.present?
  end

  def pro
    performing_rights_organization
  end

  def has_pro_songwriter_affiliation?
    pro.present?
  end

  def has_cae?
    cae.present?
  end

  def account_composer
    PublishingComposer.where(person_id: publishing_administrator.id).first
  end

  def process_paid
    send_bmi_registration_reminder

    return unless has_rights_app?

    send_to_publishing_administration
  end

  def lod_status
    return unless lod

    { status: lod.status_text, date: lod.last_status_at }
  end

  def paid_for?
    PublishingAdministration::ComposerStatusService.new(self).paid_for?
  end

  def letter_of_direction
    legal_documents.where(name: DocumentTemplate::LOD).last
  end

  def valid_letter_of_direction?
    return true if publisher.blank?

    lod = letter_of_direction
    lod ? lod.signed_at.present? : true
  end

  def create_or_update_publisher(publisher_params)
    return unless publisher_params.present? && pro.present?

    if publisher.present?
      has_publisher_values = publisher_params.values.any?(&:present?)
      has_publisher_values ? publisher.update(publisher_params) : publisher.destroy
    else
      publisher = Publisher.new(publisher_params)
      update(publisher: publisher)
    end
  end

  def form_type
    tax_info.is_w8ben? ? "W-BEN" : "W-9"
  end

  def send_update_to_publishing_administration
    return unless is_active?

    PublishingAdministration::ApiClientServices::PublishingWriterService.create_or_update(self)
    nil
  rescue PublishingAdministration::ErrorService::ApiError => e
    "Unable to send PublishingComposer to Rights App: #{e.message}"
  end

  private

  def send_to_publishing_administration
    return unless paid_for? && is_active?

    if account.provider_account_id.nil?
      PublishingAdministration::PublishingArtistAccountWriterWorker.perform_async(id)
    else
      PublishingAdministration::PublishingWriterCreationWorker.perform_async(id)
    end
  end

  def create_unallocated_sum_publishing_composition
    return if FeatureFlipper.show_feature?(:no_unallocated_sum_publishing_compositions, person) || !is_primary_composer

    begin
      publishing_composition_splits.create!(
        percent: 100,
        publishing_composition: PublishingComposition.create!(
          account: account,
          name: unallocated_sums_publishing_composition_name,
          is_unallocated_sum: true
        )
      )
    rescue StandardError => e
      logger.error "Error in creating unallocated sums publishing composition: #{e.message}"
    end
  end

  def unallocated_sums_publishing_composition_name
    if publisher
      publisher.name
    else
      name
    end
  end

  def send_bmi_registration_reminder
    return if has_pro_songwriter_affiliation?

    person = publishing_administrator

    MailerWorker.perform_async(
      "PublishingNotifier",
      :bmi_registration_reminder,
      id,
      "PublishingComposer"
    )

    Note.create(
      note: "BMI Registration Reminder sent to #{name}(#{person.email})",
      note_created_by_id: 0,
      related: person,
      subject: "Publishing Email"
    )
  end

  def nullify_provider_identifier
    return if provider_identifier.present?

    self.provider_identifier = nil
  end

  def create_publishing_compositions
    PublishingAdministration::PublishingCompositionCreationWorker.perform_async(id)
  end
end
