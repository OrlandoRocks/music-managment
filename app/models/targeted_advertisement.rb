# = Description
# TargetedAdvertisement links the Advertisement model to a specific TargetedOffer. If an advertisement is selected
# as targeted, it will be shown above any default advertisement, if applicable.
#
# = Usage
# TargetedAdvertisement isn't accessed directly. It should be accessed via the Advertsiement.for_person method.
#
# = Change Log
# [2010-04-22 -- CH]
# Created Model

class TargetedAdvertisement < ApplicationRecord
  belongs_to  :targeted_offer
  belongs_to  :advertisement

  validates :advertisement, :targeted_offer, presence: true
end
