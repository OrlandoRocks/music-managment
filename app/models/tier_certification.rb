class TierCertification < ApplicationRecord
  belongs_to :tier
  belongs_to :certification

  validates :tier_id, uniqueness: { scope: :certification_id }
end
