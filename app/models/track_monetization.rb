class TrackMonetization < ApplicationRecord
  include ActiveModel::ForbiddenAttributesProtection

  include Distribution::Distributable
  extend ArelTableMethods

  class EligibilityError < StandardError; end

  ELIGIBILITY_STATUSES = ["pending", "eligible", "ineligible"].map(&:freeze).freeze

  PENDING = ELIGIBILITY_STATUSES.first
  ELIGIBLE = ELIGIBILITY_STATUSES.second
  INELIGIBLE = ELIGIBILITY_STATUSES.third
  BLOCKED = Distribution::Stateful::BLOCKED[0]
  DELIVERED = "delivered".freeze

  DELIVERED_TC_DISTRIBUTOR_STATE = "delivered_via_tc_distributor".freeze
  START_WITHOUT_ALBUM_STATE = "start_without_album".freeze
  TAKEDOWN_DELIVERY_TYPE = "takedown".freeze

  has_many :applied_ineligibility_rules, as: :vetted_item
  has_many :ineligibility_rules, through: :applied_ineligibility_rules

  belongs_to :person
  belongs_to :song
  belongs_to :store

  has_one :album, through: :song
  has_one :manual_approval, as: :approveable

  enum tc_distributor_state: {
    new: "new",
    start_without_album: "start_without_album",
    start: "start",
    gathering_assets: "gathering_assets",
    enqueued: "enqueued",
    dequeued: "dequeued",
    packaged: "packaged",
    delivered: "delivered",
    error: "error",
    old_error: "old_error"
  },
       _prefix: :tc_distributor

  validates :person_id, :song_id, :store_id, presence: true

  after_create :block_track_monetization, if: :track_monetization_blocker

  scope :delivered_via_tc_distributor, -> {
    joins(:store).where(stores: { delivery_service: Store::TC_DISTRIBUTOR })
  }

  scope :for_tc_distributor_delivery,
        -> {
          includes(:song).where(
            tc_distributor_state: [
              "start",
              "start_without_album"
            ],
            eligibility_status: "eligible"
          ).group_by do |track_monetization|
            {
              album_id: track_monetization.song.album_id,
              store_id: track_monetization.store_id,
              delivery_type: track_monetization.taken_down? ? TAKEDOWN_DELIVERY_TYPE : track_monetization.delivery_type
            }
          end
        }

  scope :enqueued_tc_distributor_delivery,
        -> { tc_distributor_enqueued }

  scope :visible_to_user, -> { where(eligibility_status: ELIGIBLE) }
  scope :eligible_or_blocked,
        -> {
          where(
            "eligibility_status = ? OR (eligibility_status = ? AND state = ?)",
            ELIGIBLE,
            INELIGIBLE,
            BLOCKED
          )
        }
  scope :can_be_delivered,
        -> {
          where(
            track_monetization_t[:state]
                  .not_in(["blocked", "pending_approval", "dismissed", "new"])
                  .and(track_monetization_t[:takedown_at].eq(nil))
          )
        }

  def self.get_song_data_of_takedown_album(store_ids, monitized_store_id, takedown_at)
    TrackMonetization
      .joins(
        "join songs s on s.id = track_monetizations.song_id"
      )
      .joins(
        "join albums on albums.id = s.album_id"
      )
      .joins(
        "left join salepoints sp on sp.salepointable_id = s.album_id and
        sp.salepointable_type = 'Album'"
      )
      .joins(
        "left join distributions_salepoints ds on ds.salepoint_id = sp.id"
      )
      .joins(
        "left join distributions d on d.id = ds.distribution_id"
      )
      .where(
        "sp.store_id in (?) and
        track_monetizations.state =?
        and track_monetizations.takedown_at IS NULL
        and albums.takedown_at > ?
        and track_monetizations.store_id = ?", store_ids, "delivered", takedown_at, monitized_store_id
      )
      .order("albums.takedown_at desc, albums.id")
      .select("track_monetizations.song_id as id, sp.store_id as store_id, track_monetizations.store_id as tm_store_id")
  end

  def artist_name
    self[:artist_name] || album.artist.name
  end

  def has_never_been_delivered?
    state == "new" && !taken_down?
  end

  def taken_down?
    takedown_at?
  end

  def ineligible?
    eligibility_status == INELIGIBLE
  end

  def eligible?
    eligibility_status == ELIGIBLE
  end

  def pending?
    eligibility_status == PENDING
  end

  def blocked_state?
    state == BLOCKED
  end

  def approved_for_distribution?
    eligible?
  end

  def block_and_disqualify(opts)
    mark_ineligible_and_create_blocker
    perform_monetization_takedown if track_delivered?
    block(opts)
  end

  def mark_ineligible_and_create_blocker
    update(eligibility_status: INELIGIBLE)
    create_track_monetization_blocker
  end

  def perform_monetization_takedown
    TrackMonetization::TakedownService.takedown(
      track_mons: [self],
      takedown_source: "TrackMonetization"
    )
  end

  def unblock_and_qualify(opts)
    mark_eligible_and_destroy_blocker
    undo_monetization_takedown if taken_down?
    unblock(opts)
  end

  def mark_eligible_and_destroy_blocker
    update(eligibility_status: ELIGIBLE)
    track_monetization_blocker&.destroy
  end

  def undo_monetization_takedown
    TrackMonetization::TakedownService.remove_takedown(
      track_mons: [self],
      takedown_source: "TrackMonetization"
    )
  end

  def pend(opts)
    update(eligibility_status: PENDING)

    pending_approval(opts)
  end

  def retry(opts)
    renew(opts) if enqueued?
    opts[:delivery_action] = __method__

    super(opts)
  end

  def check_eligibility
    TrackMonetization::EligibilityEngineService.new(self).validate!
    reload.approved_for_distribution?
  rescue => e
    Rails.logger.error "Failure in TrackMonetization#check_eligibility: #{e.message}"
    Airbrake.notify(
      EligibilityError,
      {
        track_monetization: id,
        message: e.message
      }
    )
  end

  def track_monetization_blocker
    TrackMonetizationBlocker.find_by(store_id: store_id, song_id: song_id)
  end

  def state_name
    state == DELIVERED_TC_DISTRIBUTOR_STATE ? "delivered via TC-Distributor" : state
  end

  def state_value
    [].tap do |arr|
      arr << tc_distributor_state if state == DELIVERED_TC_DISTRIBUTOR_STATE
      arr << "(taken down)" if takedown_at?
    end.join(" ")
  end

  def create_track_monetization_blocker
    TrackMonetizationBlocker.create(song_id: song_id, store_id: store_id) if track_monetization_blocker.nil?
  end

  def create_manual_approval(current_user)
    ManualApproval.create(created_by_id: current_user.id, approveable: self) unless track_monetization_blocker
  end

  # NOTE: We are not using "delivered?" method name here to avoid overriding
  # the dynamic "state" method name set by acts_as_state_machine library.
  def track_delivered?
    state == DELIVERED || (
      state == DELIVERED_TC_DISTRIBUTOR_STATE && tc_distributor_state == DELIVERED)
  end

  private

  def block_track_monetization
    block_and_disqualify({ message: "blocking_distribution" })
  end
end
