class AlbumSalesReport
  def initialize
    @totals = []
  end

  def add_record(record)
    return unless album_id = record.album_id

    setup_album_totals(album_id)
    current = @totals[album_id]
    current[:albums_sold] += record.units_sold if record.song_id.blank?
    current[:songs_sold] += record.units_sold if record.song_id.present? && record.sale_type == "dl"
    current[:songs_streamed] += record.units_sold if record.song_id.present? && record.sale_type != "dl"
    current[:net_sales] += record.usd_total_cents if record.song_id.blank?
    current[:net_earnings] += record.usd_total_cents
  end

  def setup_album_totals(album_id)
    return if @totals[album_id]

    @totals[album_id] = {
      albums_sold: 0,
      songs_streamed: 0,
      songs_sold: 0,
      net_sales: 0,
      net_earnings: 0
    }
  end

  #
  #  Specific album numbers
  #
  def albums_sold(album_id)
    current = @totals[album_id]
    current ? current[:albums_sold] : 0
  end

  def songs_sold(album_id)
    current = @totals[album_id]
    current ? current[:songs_sold] : 0
  end

  def songs_streamed(album_id)
    current = @totals[album_id]
    current ? current[:songs_streamed] : 0
  end

  # total sales for the album as a unit
  def net_sales(album_id)
    current = @totals[album_id]
    cents = current ? current[:net_sales] : 0
    Money.new(cents)
  end

  # total sales for the album sales, song sales, and song streams
  def net_earnings(album_id)
    current = @totals[album_id]
    cents = current ? current[:net_earnings] : 0
    Money.new(cents)
  end
end
