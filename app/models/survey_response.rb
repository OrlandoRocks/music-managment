# frozen_string_literal: true

class SurveyResponse < ApplicationRecord
  # if json becomes too slow and we're still using maria db, consider this indexing strategy:
  # https://mariadb.com/resources/blog/json-with-mariadb-10-2/
  validates :person, :survey, :json, presence: true
  validates :person, uniqueness: { unless: :multiple_response?, scope: :survey }
  validate :proper_json_format

  belongs_to :survey
  belongs_to :person

  def proper_json_format
    errors.add(:json, "Missing mandatory keys from Survey") unless proper_json_format?
  end

  # we're using mandatory_json_keys as our only formatting for now, if we decide we want nested responses
  # and more thorough data analysis of these surveys we should rethink the way we format the json
  def proper_json_format?
    return true if survey.mandatory_json_keys.blank?

    Oj.load(survey.mandatory_json_keys).all? { |k| Oj.load(json).has_key?(k) }
  end

  delegate :multiple_response?, to: :survey
end
