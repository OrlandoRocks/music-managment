class LoginTrack < ApplicationRecord
  belongs_to :trackable, polymorphic: true
  belongs_to :login_event

  validates :trackable, :login_event, presence: true
end
