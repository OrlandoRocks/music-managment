# 2009-07-02 -- MJL -- Adding the after_create tumble log message
# 2009-08-21 -- MJL -- Adding the after_create method to generate a tumble log message.

# == Schema Information
# Schema version: 404
#
# Table name: annual_renewals
#
#  id                   :integer(11)     not null, primary key
#  album_id             :integer(11)     not null
#  purchase_id          :integer(11)
#  price_policy_id      :integer(11)     not null
#  created_on           :date
#  number_of_months     :integer(11)     default(12)
#  promotion_identifier :string(30)      default("Annual Renewal")
#  finalized_at         :datetime
#

class AnnualRenewal < ApplicationRecord
  include Tunecore::Purchaseable

  belongs_to :album

  validates :album_id, :number_of_months, :promotion_identifier, presence: true

  before_save :check_renewal_date_and_live_on
  before_destroy :delete_associated

  def delete_associated
    raise RuntimeError, "This annual renewal has been paid for and cannot be destroyed" if finalized?

    Purchase.destroying_purchaseable(model)
    true
  end

  def check_renewal_date_and_live_on
    return unless album.renewal_due_on.blank? || album.known_live_on.blank?

    raise RuntimeError, "This album does not have a renewal_date or a known_live_on"
  end

  def finalized?
    finalized_at.present?
  end
end
