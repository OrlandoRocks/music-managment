class CountryVariablePrice < ApplicationRecord
  belongs_to :country_website
  belongs_to :variable_price

  validates :variable_price, :country_website, presence: true

  def self.variable_prices_by_person(person)
    CountryVariablePrice.find_by_sql(
      [
        "select cvp.*, price as variable_price_amount from country_variable_prices cvp
      inner join variable_prices vp on cvp.variable_price_id = vp.id
      where country_website_id = ?",
        person.country_website_id
      ]
    )
  end

  def self.track_variable_prices_by_person(person)
    CountryVariablePrice.find_by_sql(
      [
        "select cvp.*, price as variable_price_amount from country_variable_prices cvp
      inner join variable_prices vp on cvp.variable_price_id = vp.id
      inner join variable_prices_salepointable_types vpst on vp.id = vpst.variable_price_id
      where country_website_id = ? and salepointable_type = 'Song'",
        person.country_website_id
      ]
    )
  end

  def self.album_variable_prices_by_person(person)
    CountryVariablePrice.find_by_sql(
      [
        "select cvp.*, price as variable_price_amount from country_variable_prices cvp
      inner join variable_prices vp on cvp.variable_price_id = vp.id
      inner join variable_prices_salepointable_types vpst on vp.id = vpst.variable_price_id
      where country_website_id = ? and salepointable_type = 'Album'",
        person.country_website_id
      ]
    )
  end

  def self.display_wholesale_preorder_album_price?(person)
    CountryVariablePrice.variable_prices_by_person(person).all? { |cvp| cvp.wholesale_price.present? }
  end

  def self.create_prices_from_country_price_map(country_website, price_map)
    price_map.each do |variable_price_id, price|
      attributes = {
        variable_price_id: variable_price_id,
        country_website: country_website,
        currency: country_website.currency,
        wholesale_price: price
      }
      country_variable_price = CountryVariablePrice.create(attributes)

      if country_variable_price.present?
        p "SUCCESS: creating country variable price for #{variable_price_id}"
      else
        p "FAIL: creating country variable price for #{variable_price_id}"
      end
    end
  end

  def display_price
    wholesale_price || variable_price_retail_price
  end

  def variable_price_retail_price
    respond_to?(:variable_price_amount) ? variable_price_amount : variable_price.price
  end

  delegate :non_native_currency_site?, to: :country_website
end
