class CmsSetAttribute < ApplicationRecord
  BACKGROUND_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/callouts/interstitial/background/:id/:filename",
    bucket: CMS_BUCKET_NAME,
    s3_credentials: { access_key_id: AWS_ACCESS_KEY, secret_access_key: AWS_SECRET_KEY },
    s3_permissions: :public_read,
    s3_protocol: "https"
  }

  BANNER_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/callouts/interstitial/banner/:id/:filename",
    bucket: CMS_BUCKET_NAME,
    s3_credentials: { access_key_id: AWS_ACCESS_KEY, secret_access_key: AWS_SECRET_KEY },
    s3_permissions: :public_read,
    s3_protocol: "https"
  }

  LOGIN_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/callouts/login/:id/:filename",
    bucket: CMS_BUCKET_NAME,
    s3_credentials: { access_key_id: AWS_ACCESS_KEY, secret_access_key: AWS_SECRET_KEY },
    s3_permissions: :public_read,
    s3_protocol: "https"
  }

  DASHBOARD_STORAGE_OPTIONS = {
    storage: :s3,
    path: "/callouts/dashboard/:id/:filename",
    bucket: CMS_BUCKET_NAME,
    s3_credentials: { access_key_id: AWS_ACCESS_KEY, secret_access_key: AWS_SECRET_KEY },
    s3_permissions: :public_read,
    s3_protocol: "https"
  }

  UPLOADED_IMAGE_TYPES = ["background_img", "banner_img", "login_img", "dashboard_img"]
  TEXT_FIELD_ATTRIBUTE_NAMES = ["markup"]

  validates :cms_set, :attr_name, presence: true
  has_attached_file :background_img, BACKGROUND_STORAGE_OPTIONS
  has_attached_file :banner_img, BANNER_STORAGE_OPTIONS
  has_attached_file :login_img, LOGIN_STORAGE_OPTIONS
  has_attached_file :dashboard_img, DASHBOARD_STORAGE_OPTIONS

  belongs_to :cms_set

  UPLOADED_IMAGE_TYPES.each do |img_type|
    validates_attachment_content_type img_type.to_sym, content_type: ["image/png", "image/jpg", "image/jpeg", "image/gif"], message: "is not an image"

    define_method("#{img_type}_file_name") do
      return unless attr_name == img_type

      attr_value
    end

    define_method("#{img_type}_file_name=") do |value|
      return unless attr_name == img_type

      self.attr_value = value
    end

    define_method("#{img_type}_content_type") do
      instance_variable_get("@#{img_type}_content_type".to_sym)
    end

    define_method("#{img_type}_content_type=") do |value|
      instance_variable_set("@#{img_type}_content_type".to_sym, value)
    end
  end
end
