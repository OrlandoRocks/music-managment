class SalesRecordNew < ApplicationRecord
  self.table_name = "sales_records_new"

  def self.fetch_count_of_data_per_person
    SalesRecordNew.select(
      "person_id, count(*) as count"
    ).group(
      :person_id
    ).order(
      "count desc"
    ).map { |record|
      [
        record.person_id, record.count
      ]
    }
  end
end
