class YouTubeRoyaltyRecordNew < ApplicationRecord
  self.table_name = "you_tube_royalty_records_new"

  def self.fetch_count_of_data_per_person
    YouTubeRoyaltyRecordNew.select(
      "person_id, count(*) as count"
    ).group(
      :person_id
    ).order(
      "count desc"
    ).map { |record|
      [
        record.person_id, record.count
      ]
    }
  end
end
