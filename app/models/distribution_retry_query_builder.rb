class DistributionRetryQueryBuilder
  def initialize(query = nil)
    @query = query || Distribution
             .select(distribution[Arel.star])
             .joins(petri_bundle: { album: :upcs })
             .order(distribution[:id])
  end

  attr_accessor :query

  def excluding_defunct_stores
    reflect(
      query.where(store[:in_use_flag].eq(false).and(store[:is_active].eq(false))).and(distribution[:converter_class].not_in(DEFUNCT_CONVERTERS))
    )
  end

  def in_stalled_state
    reflect(
      query.where(
        distribution[:state].not_in(["delivered", "dismissed", "new", "pending_approval"])
              .and(PetriBundle.arel_table[:state].not_eq("dismissed"))
              .and(distribution[:created_at].lt(1.month.ago))
              .and(distribution[:updated_at].lt(2.days.ago))
      )
    )
  end

  def not_in_dismissed_petri_bundle
    reflect(
      query.where(petri_bundle[:state].not_eq("dismissed"))
    )
  end

  def by_converter_class(converter_class)
    reflect(
      query.where(distribution[:converter_class].eq(converter_class))
    )
  end

  def by_store_id(store_id)
    reflect(
      query.joins(:salepoints)
        .where(salepoint[:store_id].eq(store_id))
    )
  end

  def by_upcs_list(upcs)
    reflect(
      query.where(upc[:number].in(upcs))
    )
  end

  private

  def distribution
    Distribution.arel_table
  end

  def upc
    Upc.arel_table
  end

  def petri_bundle
    PetriBundle.arel_table
  end

  def salepoint
    Salepoint.arel_table
  end

  def store
    Store.arel_table
  end

  def reflect(query)
    self.class.new(query)
  end
end
