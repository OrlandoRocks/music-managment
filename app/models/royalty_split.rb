# frozen_string_literal: true

class RoyaltySplit < ApplicationRecord
  include RoyaltySplitLoggable
  DISPLAY_MAX_RELEASES = 3
  DISPLAY_MAX_RECIPIENTS = 4
  ALBUM_TITLE_MAX_SIZE = 40
  SONG_TITLE_MAX_SIZE = 40
  DISPLAY_MAX_SONGS_ON_RELEASE = 2

  before_validation :nilify_blank_title
  after_save :ensure_owner_recipient!

  belongs_to :owner, class_name: "Person"

  has_many :recipients,
           inverse_of: :royalty_split,
           dependent: :destroy,
           class_name: "RoyaltySplitRecipient",
           autosave: true,
           validate: true
  has_many :royalty_split_songs,
           inverse_of: :royalty_split,
           dependent: :destroy,
           # counter_cache: :songs_count,
           autosave: true,
           validate: true
  has_many :songs,
           through: :royalty_split_songs,
           inverse_of: :royalty_split
  has_many :albums, -> { distinct },
           # counter_cache: true,
           through: :songs
  has_many :artworks, through: :albums
  has_many :people, through: :recipients

  validates :owner, presence: true
  validates :title, presence: true, uniqueness: { scope: :owner_id }, length: { in: 2..40 }

  validate :recipients_percents_add_to_one_hundred
  validate :uniqueness_of_recipient_emails

  accepts_nested_attributes_for :recipients, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :royalty_split_songs, reject_if: :all_blank, allow_destroy: true

  scope :invited, ->(user) {
                    user.royalty_splits.where.not(owner: user)
                  }

  def self.new_with_owner(owner)
    new(owner: owner, recipients_attributes: [{ person: owner, percent: 100 }])
  end

  # empty string :id(s) exist from the form, and are useful for breaking up the
  # params into separate recipients_attributes objects.  We remove blank ids here.
  def recipients_attributes=(attributes)
    super(attributes.map { |attr| attr[:id].blank? ? attr.except(:id) : attr })
  end

  # Overwrites RoyaltySplitSongs, can create empty RoyaltySplits that don't affect any songs.
  def associate_songs!(apply_to_songs, new_split: false)
    new_songs = apply_to_songs.without(songs)
    return if new_songs.empty?

    new_songs.each do |song|
      song.update!(royalty_split: self)
    end

    songs.reload

    # Notify all recipients that these songs have been added the the split.
    notify_songs_added_to_split(new_songs: new_songs) unless new_split
  end

  def owner_recipient
    recipients.find { |r| r.person_id == owner_id }
  end

  def not_owner_recipients
    recipients.reject { |r| r.person_id == owner_id }
  end

  def ordered_recipients
    # Brings main account holder (current user) to front of list (also makes list unique).
    [owner_recipient] | recipients
  end

  # TODO: song_count and album_count could be counter_caches
  def song_count
    songs.size
  end

  def album_count
    albums.size
  end

  def display_title
    title || I18n.t("royalty_splits.untitled_split")
  end

  def self.with_the_most_songs(album_ids:)
    joins(:songs)
      .joins(:albums)
      .where("songs.album_id": album_ids)
      .group("royalty_splits.id")
      .order("count(songs.id) desc")
      .first
  end

  def top_albums(limit = DISPLAY_MAX_RELEASES)
    @top_albums ||= albums.joins(:royalty_split_songs)
                          .group("albums.id")
                          .order("count(royalty_split_songs.id) desc, albums.id")
                          .limit(limit)
  end

  def songs_on_album(album)
    songs.where(album: album)
  end

  def what_owner_percent_should_be
    100.0 - not_owner_recipients.reject(&:_destroy).sum(&:percent)
  end

  def recipient_for(person)
    recipients.find { |r| r.person == person }
  end

  def active?
    deactivated_at.nil? || deactivated_at > Time.now
  end

  private

  def notify_songs_added_to_split(new_songs:)
    RoyaltySplits::NotificationService.songs_added_to_split(split: self, new_songs: new_songs)
    RoyaltySplitsMailerWorker.perform_async(:split_songs_added, { split_id: id, new_song_ids: new_songs.map(&:id) })
  end

  def ensure_owner_recipient!
    has_owner = recipients.any? { |r| r.person == owner }
    recipients.create(person: owner, percent: 100) unless has_owner
  end

  def recipients_percents_add_to_one_hundred
    return if recipients.empty?

    # to-be-deleted recipients aren't included in sum total
    return if recipients.reject(&:_destroy).sum(&:percent) == 100

    errors.add(:recipients, I18n.t("royalty_splits.must_have_percents_that_sum_to_100"))
  end

  def nilify_blank_title
    self.title = nil if title.blank?
  end

  # This validation is needed see issue: https://github.com/rails/rails/issues/20676
  # For some reason duplicate person_ids are OK without this extra uniqueness check,
  # unlike duplicate recipient emails (below)
  def uniqueness_of_recipient_emails
    return if recipients.empty?

    # .uniq remove duplicates, length says if lists are same.
    recipient_emails = recipients.map(&:email).reject(&:nil?)
    errors.add(:recipients, :taken) if recipient_emails.length != recipient_emails.uniq.length
  end

  def was_changed_royalty_split_loggable_events
    RoyaltySplits::EventService.split_changed_title self if saved_change_to_title?
    RoyaltySplits::EventService.split_deactivated self if saved_change_to_deactivated_at?
  end
end
