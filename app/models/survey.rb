# frozen_string_literal: true

class Survey < ApplicationRecord
  validates :name, uniqueness: true, presence: true
  validates :multiple_response, inclusion: { in: [true, false] }
  validate :mandatory_json_keys_array_or_nil

  has_many :survey_responses, dependent: :destroy

  PLAN_EMOJI_SURVEY = "Plan Emoji Survey"

  def mandatory_json_keys_array_or_nil
    errors.add(:mandatory_json_keys, "Must be Array or nil") unless mandatory_json_keys_array_or_nil?
  end

  def mandatory_json_keys_array_or_nil?
    return true if mandatory_json_keys.nil?

    Oj.load(mandatory_json_keys).is_a?(Array)
  end
end
