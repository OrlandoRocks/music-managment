class MissingSplitsNotification < Notification
  def setup_system_notification
    set_url
    set_title
    set_text
    set_link_text
    set_image_url
    auto_archive
  end

  def self.create_notifications(_options = {})
    composers = Composer.is_paid
                        .joins("inner join albums on composers.person_id = albums.person_id")
                        .joins("inner join songs on albums.id = songs.album_id")
                        .joins("left outer join compositions on songs.composition_id = compositions.id")
                        .where("compositions.id is null or compositions.state is null or compositions.state = 'new'")
                        .where("albums.is_deleted = false")
                        .where("albums.takedown_at is null")
                        .group("composers.id")
                        .having("COUNT(albums.id) < 50").all

    Rails.logger.info "Creating notifications for #{composers.size} users with missing splits"

    composers.each do |composer|
      Rails.logger.info "Creating missing split notification for person=#{composer.person_id}"
      MissingSplitsNotification.create(person: composer.person, notification_item: nil)
    end
  end

  def self.create_notifications_for_publishing_composers(_options = {})
    publishing_composers = PublishingComposer.is_paid
                                             .joins("inner join albums on publishing_composers.person_id = albums.person_id")
                                             .joins("inner join songs on albums.id = songs.album_id")
                                             .joins("left outer join publishing_compositions on songs.composition_id = publishing_compositions.id")
                                             .where("publishing_compositions.id is null or publishing_compositions.state is null or publishing_compositions.state = 'new'")
                                             .where("albums.is_deleted = false")
                                             .where("albums.takedown_at is null")
                                             .group("publishing_composers.id")
                                             .having("COUNT(albums.id) < 50").all

    Rails.logger.info "Creating notifications for #{publishing_composers.size} users with missing splits"

    publishing_composers.each do |publishing_composer|
      Rails.logger.info "Creating missing split notification for person=#{publishing_composer.person_id}"
      MissingSplitsNotification.create(person: publishing_composer.person, notification_item: nil)
    end
  end

  private

  def auto_archive
  end

  def set_url
    self.url = "/publishing_administration/composers"
  end

  def set_title
    self.title = "Missing Splits: Action Required"
  end

  def set_text
    self.text = "We can't register your compositions and collect your royalties until you tell us your split percentages (i.e., the percentage of the composition that you own or control)."
  end

  def set_image_url
    self.image_url = "/images/notifications/pubadmin.jpg"
  end

  def set_link_text
    self.link_text = "Set missing splits now"
  end
end
