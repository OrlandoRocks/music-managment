# frozen_string_literal: true

class AdditionalArtist < PlanAddon
  default_scope { additional_artist }

  scope :additional_artist, -> { where(addon_type: PlanAddon::ADDITIONAL_ARTIST) }
end
