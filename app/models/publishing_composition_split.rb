class PublishingCompositionSplit < ApplicationRecord
  include PublishingCompositionSplitValidatable

  has_paper_trail

  belongs_to :publishing_composer
  belongs_to :publishing_composition
  validates :percent, presence: true
  validates :publishing_composer, presence: true
  validate :split_percent_is_valid
  validate :total_splits_do_not_exceed_100_percent
  before_save :set_updated_by
  after_save  :update_composition_status

  scope :primary,
        -> {
          includes(:publishing_composer)
            .where(publishing_composers: { is_primary_composer: true })
        }

  scope :collectable,
        -> {
          joins(:publishing_composer)
            .where(publishing_composition_splits: { right_to_collect: true })
        }

  scope :non_collectable,
        -> {
          joins(:publishing_composer)
            .where(publishing_composition_splits: { right_to_collect: false })
        }

  scope :non_collectable_known,
        -> {
          joins(:publishing_composer)
            .where(publishing_composition_splits: { right_to_collect: false })
            .where(publishing_composers: { is_unknown: false })
        }

  scope :unknown,
        -> {
          includes(:publishing_composer)
            .where(publishing_composers: { is_unknown: true })
        }

  def related_splits
    publishing_composition.publishing_composition_splits - [self]
  end

  def total_pct
    related_splits.sum(&:percent) + percent
  end

  def submit_split(split_pct)
    self.percent = split_pct.to_d
    save
  end

  private

  def update_composition_status
    if total_pct.zero?
      publishing_composition.set_not_controlled!
    elsif total_pct == 100
      publishing_composition.submit_split!
    else
      publishing_composition.draft!
    end
  end

  def total_splits_do_not_exceed_100_percent
    validate_split_percentage_maximum(total_pct.to_d)
  end

  def split_percent_is_valid
    validate_split_percent_range(percent.to_d)
  end

  def set_updated_by
    self.updated_by = PaperTrail.request.whodunnit
  end
end
