class RoyaltyStore < ApplicationRecord
  has_many :royalty_sub_stores, dependent: :destroy

  validates :name, presence: true, uniqueness: true
end
