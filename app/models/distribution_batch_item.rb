class DistributionBatchItem < ApplicationRecord
  belongs_to :distribution
  belongs_to :distribution_batch

  def remove_from_batch
    update(status: "removed")
  end
end
