# frozen_string_literal: true

class PayoutProgram < ApplicationRecord
  has_many :payout_withdrawal_types
  has_many :payout_transfers
  has_many :payout_providers, through: :payout_withdrawal_types

  PERCENTAGE = "percentage"
  AMOUNT_CENTS = "amount_cents"

  PAYONEER = "payoneer"
  PAYPAL = "paypal"
  CHECK = "check"

  def calculate_fee(amount)
    if percentage?
      return if percentage.blank? || max_fee.blank?

      [amount * percentage, max_fee].min
    elsif amount_cents?
      amount_cents
    end
  end

  def percentage?
    fee_type == PERCENTAGE
  end

  def amount_cents?
    fee_type == AMOUNT_CENTS
  end
end
