#
# = Description
# = History
# = Usage
# = ChangeLog
#
class Label < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :name, length: { maximum: 120 }
  validates_with Utf8mb3Validator, fields: [:name]

  has_many :videos
end
