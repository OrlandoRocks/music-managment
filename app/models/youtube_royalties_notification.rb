class YoutubeRoyaltiesNotification < Notification
  validates :notification_item, presence: true
  validates :notification_item_type, inclusion: { in: %w[YouTubeRoyaltyIntake] }

  def setup_system_notification
    set_url
    set_title
    set_text
    set_link_text
    set_image_url

    self.person = notification_item.person if notification_item
  end

  def self.create_notifications
    intake_records = YouTubeRoyaltyIntake
                     .joins("left outer join notifications n on you_tube_royalty_intakes.id = n.notification_item_id and n.notification_item_type = 'YouTubeRoyaltyIntake'")
                     .where("n.id is null")
                     .preload(:person)

    Rails.logger.info "Creating notifications for #{intake_records.size} YouTube royalty intake records"

    intake_records.each do |record|
      Rails.logger.info "Creating notification for a YouTube royalty post for person=#{record.person_id}"
      create(person: record.person, notification_item: record)
    end
  end

  private

  def set_url
    self.url = "/you_tube_royalties/release_report"
  end

  def set_title
    self.title = "Your YouTube Sound Recording Revenue Statement is Now Available."
  end

  def set_text
    self.text = "Your YouTube Sound Recording Revenue Statement is now available for view. Your next revenue statement will be available in one month."
  end

  def set_image_url
    self.image_url = "/images/notifications/ytm_small.jpg"
  end

  def set_link_text
    self.link_text = "Click to view statement."
  end
end
