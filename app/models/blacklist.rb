# == Schema Information
# Schema version: 404
#
# Table name: blacklists
#
#  id   :integer(11)     not null, primary key
#  name :string(128)
#

# TODO: This Class name is problematic, We, as an org,  should consider moving to BlockList
class Blacklist < ApplicationRecord
  OUR_RESERVED_WEBLIST = Set[
    "studio",
    "staging",
    "release",
    "deploy",
    "trunk"
  ]

  def self.blacklisted?(arg)
    if Blacklist.find_by(name: arg).nil?
      if Blacklist.in_set_of_rules?(arg)
        true
      else
        false
      end
    else
      true
    end
  end

  def self.in_set_of_rules?(arg)
    Blacklist.sounds_like_tunecore?(arg) || Blacklist.our_reserved_weblist?(arg)
  end

  def self.sounds_like_tunecore?(arg)
    arg.include?("tunecore")
  end

  def self.our_reserved_weblist?(arg = "")
    arg.starts_with?("web") || arg.starts_with?("www") || OUR_RESERVED_WEBLIST.member?(arg)
  end
end
