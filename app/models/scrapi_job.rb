class ScrapiJob < ApplicationRecord
  has_many :scrapi_job_details, dependent: :destroy
  belongs_to :song
  has_one :album, through: :song
end
