# frozen_string_literal: true

class PostingMapping < ApplicationRecord
  validates :posting_id, presence: true
  validates :external_posting_id, presence: true
  validates :source, presence: true
  validates :posting_id, uniqueness: { scope: :external_posting_id }

  enum source: {
    SIP: Royalties::Posting::SOURCE_SIP,
    SYMPHONY: Royalties::Posting::SOURCE_SYMPHONY
  }
end
