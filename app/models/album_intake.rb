# last change: 10/2/09 dw add "if" to named_scope for year and month

# == Schema Information
# Schema version: 404
#
# Table name: album_intakes
#
#  id                 :integer(11)     not null, primary key
#  album_id           :integer(11)     default(0), not null
#  person_id          :integer(11)     default(0), not null
#  reporting_month_id :integer(11)     default(0), not null
#  person_intake_id   :integer(11)     default(0), not null
#  songs_sold         :integer(11)     default(0), not null
#  songs_streamed     :integer(11)     default(0), not null
#  albums_sold        :integer(11)     default(0), not null
#  usd_total_cents    :integer(11)     default(0), not null
#

class AlbumIntake < ApplicationRecord
  belongs_to  :album
  belongs_to  :person
  belongs_to  :reporting_month
  belongs_to  :person_intake

  scope :year,
        ->(year) {
          includes(:reporting_month).where(reporting_months: { report_year: year }) if year
        }

  scope :month,
        ->(month) {
          includes(:reporting_month).where(reporting_months: { report_month: month }) if month
        }

  # automatically link up the sales_record's belongs_to attributes with
  # the ones we know about
  def adding_sales_record(sales_record)
    sales_record.attributes = {
      store_intake: person_intake.store_intake,
      reporting_month: reporting_month,
      person_id: person_id,
      usd_total_cents: person_intake.store_intake.calculate_usd_cents(sales_record.local_total_cents),
      album: album
    }
  end

  def sales_record_added(sales_record)
    # tell our parent person_intake about the sale
    # so that it can record what it needs
    person_intake.sales_record_added(sales_record)

    # add the sales_record's usd_total_cents to ours
    self.usd_total_cents += sales_record.usd_total_cents

    # record the numbers sold
    case sales_record.sale_type
    when "dl"
      if sales_record.is_album_sale?
        self.albums_sold += sales_record.units_sold
      else
        self.songs_sold += sales_record.units_sold
      end
    when "st", "ss" # stream trial or stream sale
      self.songs_streamed += sales_record.units_sold
    end
  end
end
