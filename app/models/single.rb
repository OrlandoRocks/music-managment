class Single < Album
  include Tunecore::Albums::OneSong
  include Tunecore::Albums::MultipleCreatives

  after_save :move_creatives_to_album

  self.inheritance_column = "album_type"

  validate :cannot_be_is_various
  validate :check_for_various_artists
  validates :language_code, presence: { message: I18n.t("models.album.choose_language") }

  def initialize(options = {})
    options ||= {}

    @song = options[:song] || Song.new
    language_code = options[:language_code_legacy_support] || options[:language_code]
    @song.metadata_language_code = LanguageCode.find_by(code: language_code) if language_code
    super
    self.song = @song
  end

  def cannot_be_is_various
    errors.add(:base, I18n.t("models.single.cannot_be_marked_compilation")) if is_various?
  end

  def check_for_various_artists
    return unless creatives.any?

    creatives.each do |creative|
      if creative.name&.downcase&.match(/various artist(s)?/)
        errors.add(:creatives, I18n.t("models.single.cannot_be_various_artists"))
      end
    end
  end

  def parental_advisory_enabled?
    song.present? && song.parental_advisory
  end

  def clean_version_enabled?
    song.present? && song.clean_version
  end
end
