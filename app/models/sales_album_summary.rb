# This model grabs aggregate data and is only used for display
# purposes.  As such, we encapsulate the underlying ActiveRecord
# objects so that it doesn't allow access to misleading information.
class SalesAlbumSummary
  # grab all the albums that have album_intakes for the supplied
  # reporting_months, along with sums on units sold and net sales
  # Note the wacky SQL - we don't want to pull in all the album_intakes
  # records in an :include - we just want to sum their data.
  def self.all_for(person, reporting_months)
    records = Album.select(
      %q|albums.*,
        sum(album_intakes.songs_sold) as songs_sold,
        sum(album_intakes.songs_streamed) as songs_streamed,
        sum(album_intakes.albums_sold) as albums_sold,
        sum(album_intakes.usd_total_cents) as net_sales_usd|
    )
                   .joins("left outer join album_intakes on album_intakes.album_id = albums.id")
                   .where("album_intakes.person_id = ? and album_intakes.reporting_month_id in (?)", person.id, reporting_months.collect(&:id))
                   .group("albums.id")
                   .order("albums.name, albums.id")

    records.collect do |record|
      SalesAlbumSummary.new(record)
    end
  end

  def self.count_all_for(person, reporting_months)
    Album.select("distinct albums.id")
         .joins("left outer join album_intakes on album_intakes.album_id = albums.id")
         .where("album_intakes.person_id = ? and album_intakes.reporting_month_id in (?)", person.id, reporting_months.collect(&:id))
         .order("albums.name, albums.id")
  end

  def initialize(data)
    self.data = data
  end

  def album
    data
  end

  def songs_sold
    album[:songs_sold].to_i
  end

  def songs_streamed
    album[:songs_streamed].to_i
  end

  def albums_sold
    album[:albums_sold].to_i
  end

  def net_sales_usd
    album[:net_sales_usd].to_i
  end

  protected

  attr_accessor :data
end
