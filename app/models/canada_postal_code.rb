class CanadaPostalCode < ApplicationRecord
  validates :code, uniqueness: { scope: :city }

  def city_and_province
    "#{city}, #{province}"
  end

  def city_and_province_code
    "#{city},#{province_code}"
  end
end
