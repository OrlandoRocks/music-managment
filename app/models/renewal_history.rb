# = Description
# The AutoRenewal model controls all renewals for the Tunecore site
# It relies on the information set in the related product's product_item_rules
#
# = Usage
#
#
# = Change Log
# [2010-01-06 -- CH]
# Created Model
#
# [2010-05-20 -- CH]
# add add_extension method for Renewal
# [2011-08-11 -- CH]
# add add_extension method for Renewal

class RenewalHistory < ApplicationRecord
  self.table_name = "renewal_history"

  #################
  # Associations #
  #################
  belongs_to  :renewal
  belongs_to  :purchase

  attr_accessor :calculate_renewal_expiration_from

  ###########
  # Banner #
  ###########
  validates :starts_at, :expires_at, :renewal, presence: true
  validate :expires_at_gt_starts_at

  def expires_at_gt_starts_at
    return unless expires_at and starts_at and expires_at.to_date <= starts_at.to_date

    errors.add(:expires_at, I18n.t("models.product.must_be_greater_than_the_start_date"))
  end

  ############
  # Filters #
  ############
  before_validation :set_starts_at_and_expires_at, on: :create

  # adding an extension to an existing renewal utilizes product.renewal_interval and product.renewal_duration to calculate a new line of
  # of renewal history for the parent renewal.  Once added, the renewal is automatically extended to the new date.
  def self.add_extension(renewal, purchase)
    renewal_history = RenewalHistory.create(renewal: renewal, purchase: purchase)
    product = purchase.product
    renewal_history.update_attribute(:expires_at, Tunecore::AutoRenewalDateCalculator.expiration_date(renewal_history.calculate_renewal_expiration_from, product.renewal_interval, product.renewal_duration))
  end

  def set_starts_at_and_expires_at
    set_start_at
    set_calculate_renewal_expiration_from
    set_expires_at
  end

  def set_start_at
    # if this renewal is new, determine the start date
    self.starts_at =
      if renewal.starts_at.nil?
        Time.zone.now.localtime.at_midnight
      else
        renewal.expires_at
      end
  end

  # We use this method to override the default behavior of calculating the expiration always from the expiration of the last renewal period
  # We want to allow old overdue albums and get a year from the point of renewal
  # e.g. Instead of renewing an album due in 2007 and getting one year until 2008, it would get a renewal until one year from today
  # Since we takedown albums after 42 days (currently), this only really applies to albums expired before April 11, 2011
  # which is the first renewal renewal date for which we ran the automated takedowns process (and it has run for every date after that.
  def set_calculate_renewal_expiration_from
    if starts_at < (Date.today - (Renewal::GRACE_DAYS_BEFORE_TAKEDOWN + 1).days) # If it expired more than 43 days ago
      self.calculate_renewal_expiration_from = Time.zone.now.localtime.at_midnight
    else
      self.calculate_renewal_expiration_from = starts_at
    end
  end

  def set_expires_at
    # if this isn't the first
    if should_use_first_renewal_interval?
      self.expires_at = Tunecore::AutoRenewalDateCalculator.expiration_date(calculate_renewal_expiration_from, renewal.first_renewal_interval, renewal.first_renewal_duration)
    else
      self.expires_at = Tunecore::AutoRenewalDateCalculator.expiration_date(calculate_renewal_expiration_from, renewal.renewal_interval, renewal.renewal_duration)
    end
  end

  protected

  #
  # TODO: refactor into method on renewal.
  # Feature Envy of renewal
  #
  def should_use_first_renewal_interval?
    !renewal.has_history? && renewal.has_first_renewal_settings?
  end
end
