# == Schema Information
# Schema version: 404
#
# Table name: query_builders
#
#  id            :integer(11)     not null, primary key
#  user_query_id :integer(11)
#  col           :string(255)
#  operator      :string(255)
#  val           :string(255)
#  tbl           :string(255)
#

class QueryBuilder < ApplicationRecord
  belongs_to :user_query

  validates_table :tbl
  validates :tbl, :col, :val, presence: true

  DEFAULT_TABLE = "people"
  DEFAULT_MODEL = DEFAULT_TABLE.to_s.classify.constantize
  DEFAULT_OPERATORS = [["=", ""], ["!=", "_ne"], [">", "_gt"], ["<", "_lt"], [">=", "_gte"], ["<=", "_lte"], ["IN", ""]]

  before_validation :set_tbl

  def set_tbl
    self.tbl = QueryBuilder.const_get(:DEFAULT_TABLE) if tbl.blank?
  end

  ## returns an array of available operators for the query builder
  def operators
    @operators || DEFAULT_OPERATORS
  end

  ## set the valid operator list; default set is defined in initialize method
  ## the array is used to populate a select in the QB form so format it appropriately.
  ## the first value should be a human readable label for the menu
  ## the second value should be compatible with ar-extensions; leave it blank for equality
  ## example: [ [">=", "_gte"], ["=", ""] ]
  def operators=(operator_list)
    raise ArgumentError, "Parameter must be an array" unless operator_list.is_a?(Array)

    @operators = operator_list
  end

  ## returns an array of table names found for the current connection
  def self.get_tables
    ActiveRecord::Base.connection.tables || []
  end

  def self.get_query(query_conditions) # tbl, col, val)
    query_conditions[:tbl] = QueryBuilder.const_get(:DEFAULT_TABLE) if query_conditions[:tbl].blank?
    QueryBuilder.where(query_conditions).first
  end

  def self.get_operators
    ## this set of operators is based on CocoaMysql's interface
    ops = QueryBuilder.const_get(:DEFAULT_OPERATORS) || []
  end

  def self.get_columns(table = DEFAULT_TABLE)
    # table = table.nil? DEFAULT_TABLE : table
    table_model = table.to_s.classify.constantize
    table_model.column_names || []
  rescue NameError
    []
  end

  def get_columns
    self.tbl = DEFAULT_TABLE if tbl.blank?
    table_model = tbl.to_s.classify.constantize
    table_model.column_names || []
  end
end
