class FormObject
  include ActiveModel::Serialization
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend  ActiveModel::Callbacks

  # ripped from Rails 4 code for attribute assignment
  def initialize(params = {})
    params.each do |attr, value|
      public_send("#{attr}=", value)
    end if params
  end

  def persisted?
    false
  end

  def is_false_value?(val)
    ["f", "F", false, 0, "0", "false", "FALSE", "off", "OFF"].include?(val)
  end

  def boolean_value_of(val)
    !is_false_value?(val)
  end
end
