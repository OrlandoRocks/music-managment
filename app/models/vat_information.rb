# frozen_string_literal: true

class VatInformation < ApplicationRecord
  belongs_to :person

  validate :vat_acceptance, unless: -> { vat_registration_number.blank? }
  validate :vat_number_format
  validate :validate_vat_number, if: :will_save_change_to_vat_registration_number?

  before_validation :audit_vat_info, if: :needs_audit?
  before_validation :strip_vat_registration_number, if: -> { vat_registration_number.present? }

  before_validation :set_vat_registration_status
  before_destroy :audit_vat_info

  after_save :recalculate_unpaid_purchases

  BUSINESS = "business"
  INDIVIDUAL = "individual"

  VALID = "valid"
  INVALID = "invalid"

  INVALID_ERROR_CODE = "vat.errors.invalid"

  attr_reader :vat_errors

  def vat_number_format
    country_name = person&.country_name_untranslated

    unless errors.blank? &&
           VAT_FORMAT[country_name] &&
           vat_registration_number.present? &&
           vat_registration_number.match(VAT_FORMAT[country_name]).blank?

      return
    end

    errors.add(:base, "vat.errors.invalid_format")
  end

  def validate_vat_number
    return unless errors.blank? && vat_registration_number.present?

    response = TcVat::FailSafe::Validator.new(person)

    return errors.add(:base, "vat.errors.vat_service_down") if response.errors.present?
    return errors.add(:base, "vat.errors.invalid") unless response.valid

    tap do |vat_info|
      vat_info.vat_registration_status = VALID
      vat_info.trader_name = response.trader_name
    end
  end

  # save nil vat_registration_number with invalid status
  def set_vat_registration_status
    self.vat_registration_status = INVALID
    self.trader_name = ""

    return if will_save_change_to_vat_registration_number?

    response = TcVat::FailSafe::Validator.new(person)
    self.vat_registration_status = VALID if response.valid?
    self.trader_name = response.trader_name
    return if response.valid?

    @vat_errors = error_message(response)
  end

  def status_valid?
    vat_registration_status == VALID
  end

  def vat_valid?
    return true if vat_registration_number.blank?

    status_valid?
  end

  def customer_type
    return BUSINESS if status_valid?

    INDIVIDUAL
  end

  def self.historic_record(person_id, vat_number, _member_country)
    record = find_by(person_id: person_id, vat_registration_number: vat_number)
    return if record.nil? || !record.status_valid?

    {
      id: record.id,
      valid: record.status_valid?,
      trader_name: record.trader_name
    }
  end

  def invalidate!
    update(
      vat_registration_number: "",
      vat_registration_status: INVALID,
      trader_name: "",
      company_name: ""
    )
  end

  private

  def audit_vat_info
    return if id.nil?

    VatInformationAudit.audit_change!(person, audit_args)
  end

  def recalculate_unpaid_purchases
    TcVat::BulkVatCalculator.new(person).calculate!(person.purchases.unpaid.pluck(:id))
  end

  def strip_vat_registration_number
    self.vat_registration_number = vat_registration_number.strip
  end

  def vat_acceptance
    return if VatAssesmentService.new(person, vat_registration_number).acceptable?

    errors.add(:base, "vat.errors.unavailable")
  end

  def needs_audit?
    will_save_change_to_vat_registration_number? ||
      will_save_change_to_vat_registration_status? ||
      will_save_change_to_company_name?
  end

  def audit_args
    {
      vat_registration_number: vat_registration_number_was,
      vat_registration_status: vat_registration_status_was || INVALID,
      trader_name: trader_name_was,
      company_name: company_name_was,
      begin_date: updated_at
    }
  end

  def error_message(response)
    return response.errors.join(" ") if response.errors.present?

    INVALID_ERROR_CODE
  end
end
