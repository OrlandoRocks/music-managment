class AdyenMerchantConfig < ApplicationRecord
  include ProviderSecretsEncryptable

  before_create :encrypt_attributes

  has_one :cipher_datum, as: :cipherable, dependent: :destroy

  belongs_to :corporate_entity

  attr_accessor :hmac_key

  def encryptable_attributes
    { hmac_key: hmac_key }
  end
end
