class BulkReleaseSubscriptionForm < FormObject
  extend ActiveSupport::Concern
  extend ActiveModel::Naming

  attr_accessor :subscription_actions, :note_options

  def process
    processed_all = true
    processed = {}
    @subscription_actions.each do |album_id, action|
      release_subscription = ReleaseSubscription.new(
        {
          album_id: album_id,
          subscription_action: action,
          options: note_options
        }
      )
      if release_subscription.process_action
        processed[action] ? processed[action] << album_id : processed[action] = [album_id]
      else
        processed_all = false
      end
    end
    send_takedown_notifications(processed["takedown"]) if processed["takedown"]
    send_cancel_notifications(processed["cancel"]) if processed["cancel"]
    processed_all
  end

  def send_takedown_notifications(album_ids)
    albums = Album.find(album_ids)
    renewals = albums.map(&:renewal)
    BatchNotifier.takedown(albums.first.person, renewals, albums.first.takedown_at).deliver
  end

  def send_cancel_notifications(album_ids)
    album_ids.each do |album_id|
      album = Album.find(album_id)
      PersonNotifier.album_renewal_change_cancel_status(album).deliver
    end
  end
end
