class EngineRule < ApplicationRecord
  serialize :config, Oj
  has_paper_trail
  before_save :format_config

  scope :fetch_active_rules, ->(category) do
    where(enabled: true, category: category)
  end

  def config
    case self[:config]
    when "{}", nil, ""
      self[:config] = {}.with_indifferent_access
      self[:config]
    else
      self[:config].with_indifferent_access
    end
  end

  private

  def format_config
    return if config.present?

    self.config = {}.to_json
  end
end
