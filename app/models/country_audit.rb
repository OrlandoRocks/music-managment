# frozen_string_literal: true

class CountryAudit < ApplicationRecord
  class ImmutableCountryAuditError < StandardError; end

  validates :person, presence: true
  validates :country, presence: true
  validates :country_audit_source, presence: true

  belongs_to :person
  belongs_to :country
  belongs_to :country_audit_source

  before_save :verify_attributes_unchanged
  after_create :update_tax_block_status
  before_destroy :verify_attributes_unchanged

  validate :new_audit, on: :create

  scope :for_year, ->(year) {
    where(
      created_at: Date.new(year).to_datetime.beginning_of_year..Date.new(year).to_datetime.end_of_year
    )
  }

  private

  def new_audit
    previous_audit = person.country_audits.last
    return if previous_audit.blank?

    non_comparable_attrs = ["id", "created_at", "updated_at"]
    unchanged = previous_audit.attributes.except(*non_comparable_attrs) == attributes.except(*non_comparable_attrs)

    errors.add(:identical_audit, "Cannot add audit with identical attributes") if unchanged
  end

  def verify_attributes_unchanged
    return if new_record?

    raise ImmutableCountryAuditError,
          "Sorry all CountryAudits are immutable, attributes: #{attributes}, attempted changes: #{changes}"
  end

  def update_tax_block_status
    TaxBlocking::UpdatePersonTaxblockStatusService.call(person)
  end
end
