class ChargebeeCustomer < ApplicationRecord
  validates :person, :chargebee_id, presence: true

  belongs_to :person

  def customer
    ChargeBee::Customer.retrieve(chargebee_id)
  end
end
