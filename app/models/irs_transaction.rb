# frozen_string_literal: true

class IRSTransaction < ApplicationRecord
  has_many :irs_tax_withholdings, dependent: :nullify

  validates :status, inclusion: { in: :allowed_statuses }
  validates :transaction_code, presence: true, if: :completed?
  validates :transacted_at,    presence: true, if: :completed?

  before_validation :transacted!, if: :completed?

  PENDING_STATUS   = "pending"
  REPORTED_STATUS  = "reported"
  COMPLETED_STATUS = "completed"

  enum status: {
    pending: PENDING_STATUS,
    reported: REPORTED_STATUS,
    completed: COMPLETED_STATUS
  }

  ADVANCE = {
    pending: :reported,
    reported: :completed,
    completed: :completed,
  }.freeze.with_indifferent_access

  def advance_status!
    update(status: ADVANCE[status])
  end

  private

  def transacted!
    self.transacted_at = Time.current
  end

  def allowed_statuses
    ordered_statuses = self.class.statuses.keys
    ordered_statuses.select do |state|
      ordered_statuses.index(state) >= ordered_statuses.index(status_was)
    end
  end
end
