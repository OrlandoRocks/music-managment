# frozen_string_literal: true

# This table determines which PrimaryArtist should remain on releases
# when a user downgrades from a Professional Plan to a plan that only
# allows a single PrimaryArtist
class PriorityArtist < ApplicationRecord
  belongs_to :artist
  belongs_to :person

  validates :artist_id, presence: true
  validates :person_id, presence: true, uniqueness: true

  def validate_or_reset_artist
    return self if valid_priority_artist?

    reset_priority_artist!
  end

  private

  def valid_priority_artist?
    person.creatives
          .active
          .exists?(creatives: { artist_id: artist_id })
  end

  def reset_priority_artist!
    destroy && return unless person.creatives.active.exists?

    update!(artist: person.oldest_primary_artist)
  end
end
