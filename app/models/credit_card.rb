class CreditCard < ActiveMerchant::Billing::CreditCard
  attr_accessor :store_card
  attr_accessor :company
  attr_accessor :address1
  attr_accessor :address2
  attr_accessor :city
  attr_accessor :state
  attr_accessor :zip
  attr_accessor :country
  attr_accessor :expiration_month
  attr_accessor :cvv

  def self.model_name
    ActiveModel::Name.new(CreditCard, CreditCard)
  end

  def to_key
    [object_id]
  end
end
