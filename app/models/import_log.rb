class ImportLog < TrendsBase
  self.table_name = "import_log"

  validates :started_at, :ended_at, :file_name, :period_ending, presence: true
end
