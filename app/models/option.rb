class Option < ApplicationRecord
  belongs_to :typeface_effect
  has_many :settings
  validates :label, :name, presence: true
end
