#
#  Feature:
#
#  An abstraction to allow for toggling of Features
#
# USAGE
# You can call the white-label module method feature_enabled?(:feature) to
# determine if the feature has been turned off or on.
#
#  check if a feature is enabled by passing a symbol that represents the feature
#
#  example:
#
#  in controllers and views:
#  feature_enabled?(:feature)
#
class Feature < ApplicationRecord
  has_many :disabled_features_country_websites
  has_many :disabled_country_websites,
           through: :disabled_features_country_websites,
           source: :country_website
  has_many :feature_people
  has_many :people, source: :person, through: :feature_people

  scope :restrict_by_user_features, -> { where restrict_by_user: true }
  #
  #  A list of toggle-able features:
  #
  AVAILABLE_FEATURES = [
    :enhanced_trending,    # for the enhanced trending report functionalty
    :band_photo,           # for the band photo uploader
    :tour_dates,           # for the tour date manager
    :credit_cards,         # for creditcards
    :tunecore_third_party,
    :dashboard_graphs,     # for the graphs displayed on the dashboard
    :tagman,               # tracking codes managed by tagman.
    :soundcloud,
    :bigbox_uploader,
    :update_dictionaries_on_staging, # For translators, to QA their work
    :publishing,
    :gcdistro, # for guitar center code redemption
    :monthly_products,
    :friend_referral,
    :hundred_tracks_allowed,
    :promo_callouts
  ]

  # Features that are available to everyone and we want to keep it that way
  LOCKED_FEATURES = [
    :enhanced_trending,    # for the enhanced trending report functionalty
    :band_photo,           # for the band photo uploader
    :tour_dates,           # for the tour date manager
    :credit_cards,         # for creditcards
    :tunecore_third_party,
    :dashboard_graphs,     # for the graphs displayed on the dashboard
    :tagman,               # tracking codes managed by tagman.
    :soundcloud,
    :bigbox_uploader
  ]

  validate do |record|
    record.errors.add("You cannot modify a locked feature") if record.locked?
  end

  def self.find_feature(symbol)
    find_by(name: symbol.to_s)
  end

  #
  #  Call to add new features to the database
  #
  def self.reset
    AVAILABLE_FEATURES.each do |feature|
      find_or_create_by(name: feature.to_s)
    end
  end

  def self.any_restrict_by_user?
    !Feature.restrict_by_user_features.empty?
  end

  def self.restrict_by_user?(symbol)
    feature = find_feature(symbol)
    return false if feature.nil?

    feature.restrict_by_user?
  end

  def self.unlock_features
    Feature.all.select { |f| !f.locked? }
  end

  def self.unlock_and_restrict_by_user_features
    restrict_by_user_features.select { |f| !f.locked? }
  end

  def locked?
    LOCKED_FEATURES.include?(name.to_sym)
  end

  def restrict_by_user?
    restrict_by_user
  end

  def symbol
    name.to_sym
  end

  def symbol=(string)
    self.name = string.to_s
  end
end
