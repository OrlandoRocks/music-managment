class YoutubeIsrcAsset < ApplicationRecord
  serialize :api_response, QuirkyJson
  validates :youtube_asset_id, :tunecore_isrc, :api_response, presence: true
end
