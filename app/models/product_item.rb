# = Description
# The ProductItem model serves as the collection of individual pricing rules that make up a product.
# For instance, a package for an Album and 10 Ringtones would have two product items describing the pricing rules for each.
#
# = Change Log
# [2009-12-21 -- CH]
# Created Model

class ProductItem < ApplicationRecord
  #################
  # Associations #
  #################
  has_many    :product_item_rules, -> { where is_active: true }
  has_many    :inventories
  has_many    :renewals, as: :item_to_renew
  has_many    :old_renewal_products, class_name: "RenewalProductChange", as: :old_item_to_renew
  has_many    :new_renewal_products, class_name: "RenewalProductChange", as: :new_item_to_renew
  belongs_to  :product
  belongs_to  :renewal_product, class_name: "Product"

  ################
  # Validations #
  ################
  validates :product, presence: true
  validates :name, presence: true
  validates :currency, presence: true

  PLAN_PRODUCT_ITEM_IDS = ProductItem.where(product_id: Product::PLAN_PRODUCT_IDS).pluck(:id).freeze
  PLAN_RELATED_PRODUCT_ITEM_IDS = ProductItem.where(product_id: Product::PLAN_RELATED_PRODUCT_IDS).pluck(:id).freeze

  delegate :is_plan?, :plan_related?, to: :product

  ################
  # Named Scopes #
  ################

  def has_price?
    !!(price && price > 0.00)
  end

  def total_price(model_to_price)
    if has_price?
      # the price has been set at the item level, so it's a flat rate regardless of the product_item_rules
      price
    else
      top_level_rules_price(model_to_price)
    end
  end

  def top_level_rules_price(model_to_price)
    top_level_rules.inject(0.00) do |sum, rule|
      sum += rule.calculate_price(model_to_price, product)
    end
  end

  private

  def top_level_rules
    ProductItemRule.top_level_rules(self)
  end
end
