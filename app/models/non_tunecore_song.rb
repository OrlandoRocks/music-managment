class NonTunecoreSong < ApplicationRecord
  belongs_to :artist
  belongs_to :composition, dependent: :destroy
  belongs_to :publishing_composition, dependent: :destroy
  belongs_to :non_tunecore_album
  belongs_to :song

  has_one :composer, through: :non_tunecore_album
  has_one :publishing_composer, through: :non_tunecore_album
  has_many :recordings, as: :recordable

  validates :name, :non_tunecore_album_id, presence: true

  def update_album(new_album_name)
    current_album = non_tunecore_album

    return if current_album.name == new_album_name

    existing_album = composer.non_tunecore_albums.find_by(name: new_album_name)

    if existing_album.present?
      NonTunecoreSong.transaction do
        update!(non_tunecore_album_id: existing_album.id)
        current_album.destroy if current_album.non_tunecore_songs.blank?
      end
    else
      handle_new_album(new_album_name)
    end
  end

  def update_album_for_publishing_composer(params = {})
    new_album_name = params[:name]
    new_record_label = params[:record_label]
    current_album = non_tunecore_album

    return if current_album.name == new_album_name && current_album.record_label == new_record_label

    existing_album = publishing_composer.non_tunecore_albums.find_by(
      name: new_album_name,
      record_label: new_record_label
    )

    if existing_album.present?
      NonTunecoreSong.transaction do
        update!(non_tunecore_album_id: existing_album.id)
        current_album.destroy if current_album.non_tunecore_songs.blank?
      end
    else
      handle_new_album_for_publishing_composer(params)
    end
  end

  private

  def handle_new_album(new_album_name)
    if non_tunecore_album.non_tunecore_songs.count > 1
      new_album = non_tunecore_album.dup

      new_album.assign_attributes(
        {
          name: new_album_name,
          upc: nil,
          orig_release_year: nil,
          record_label: nil
        }
      )

      NonTunecoreSong.transaction do
        new_album.save!

        update!(non_tunecore_album_id: new_album.id)
      end
    else
      non_tunecore_album.update(name: new_album_name)
    end
  end

  def handle_new_album_for_publishing_composer(params = {})
    new_album_name = params[:name]
    new_record_label = params[:record_label]

    if non_tunecore_album.non_tunecore_songs.count > 1
      new_album = non_tunecore_album.dup

      new_album.assign_attributes(
        {
          name: new_album_name,
          record_label: new_record_label,
          upc: nil,
          orig_release_year: nil
        }
      )

      NonTunecoreSong.transaction do
        new_album.save!

        update!(non_tunecore_album_id: new_album.id)
      end
    else
      non_tunecore_album.update(name: new_album_name, record_label: new_record_label)
    end
  end
end
