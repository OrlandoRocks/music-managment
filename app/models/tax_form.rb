# frozen_string_literal: true

class TaxForm < ApplicationRecord
  belongs_to :person
  belongs_to :tax_form_type
  belongs_to :payout_provider_config

  has_many :tax_form_revenue_streams, dependent: :destroy
  has_many :revenue_streams, through: :tax_form_revenue_streams
  has_many :pub_earnings_metadata,
           class_name: "PersonEarningsTaxMetadatum",
           foreign_key: :pub_taxform_id,
           inverse_of: :pub_taxform,
           dependent: :nullify
  has_many :dist_earnings_metadata,
           class_name: "PersonEarningsTaxMetadatum",
           foreign_key: :dist_taxform_id,
           inverse_of: :dist_taxform,
           dependent: :nullify

  delegate :remove_withholding_exemption!, to: :person

  validates :tax_form_type_id, presence: true
  after_create :default_map_to_revenue_streams, if: :first_tax_form_submitted?
  after_save :update_metadata, if: proc { should_update_metadata? }
  after_save :remove_withholding_exemption!, if: :tax_compliant_taxform_submitted?

  scope :w8, -> { joins(:tax_form_type).merge(TaxFormType.w8_types) }
  scope :w9, -> { joins(:tax_form_type).merge(TaxFormType.w9_types) }
  scope :us, -> { joins(:tax_form_type).merge(TaxFormType.us_types) }
  scope :active, -> { w8.where(expires_at: Time.now..).or(TaxForm.w9) }
  scope :with_program_id, -> { where.not(payout_provider_config_id: nil) }
  scope :current_active_w9, -> { current.active.w9 }

  # Tax-forms submitted under a special payoneer program that accepts a secondary
  # tax-form for separate taxation of distribution and publishing revenue streams.
  scope :secondary, -> { where(payout_provider_config: PayoutProviderConfig.secondary_tax_form_config) }

  scope :revenue_stream_mapping_by, ->(actor) do
    query =
      annotate(
        Colorized.string_io(
          :light_blue,
          "fetching tax_forms mapped to revenue_streams by #{actor} (TaxFormMappingService)"
        )
      )
      .joins(:revenue_streams)

    case actor
    when :system
      query
        .where(tax_form_revenue_streams: { user_mapped_at: nil })
    when :user
      query
        .where.not(tax_form_revenue_streams: { user_mapped_at: nil })
    end
  end

  scope :current, -> do
    from(
      with_program_id
      .select(
        "*", %{
        row_number() over(
          partition by person_id, payout_provider_config_id
          order by tax_forms.submitted_at desc, tax_forms.created_at desc
        ) as rank
      }
      ), :tax_forms
    )
      .where(rank: 1)
  end

  # Prepare for safe migration that removes this column
  self.ignored_columns = %w[form_type]

  def w9?
    TaxFormType
      .w9_types
      .pluck(:id)
      .include?(tax_form_type_id)
  end

  private

  def first_tax_form_submitted?
    w9? && PayoutProviderConfig.default_us_tax_program_configs.ids.include?(payout_provider_config_id)
  end

  def default_map_to_revenue_streams
    TaxReports::TaxFormMappingService.call!(person_id)
  end

  def update_metadata
    metadata = PersonEarningsTaxMetadatum.find_or_initialize_by(person: person, tax_year: DateTime.current.year)
    metadata.update(dist_taxform: self, dist_taxform_submitted_at: DateTime.now)
  end

  def tax_compliant_taxform_submitted?
    person
      .tax_forms
      .current
      .active
      .w9
      .where(
        id: id,
        provider: PayoutProvider::PAYONEER
      ).any?
  end

  def should_update_metadata?
    FeatureFlipper.show_feature?(:advanced_tax_blocking) &&
      (provider == PayoutProvider::PAYONEER) &&
      TaxFormType.w9_types.ids.include?(tax_form_type_id)
  end
end
