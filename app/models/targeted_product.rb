# = Description
# TargetedProduct works in conjunction with TargetedOffer and TargetedPeople to define which products are offered for a population of customers,
# the sort order of the offered products, and how much the price is adjusted.
#
# = Usage
# TargetedProduct is always attached to TargetedOffer and is accessed through the TargetedOffer model for any product price adjustment. See TargetedOffer.adjust_product_price method.
#
# = Attributes
# product - Which product is offered
# price_adjustment_type (percentage_off, override, dollars_off) - determines the mathematical operation to perform with the price_adjustment field
# price_adjustment - Decimal storage of adjustment value, Percentages are stored unconverted (e.g. 10% is 10.0)
# sort_order - Overrides the sort_order stored in the main products table. If the sort_order is not set, the sort_order in the products table takes over.
#
# = Change Log
# [2010-04-22 -- CH]
# Created Model

class TargetedProduct < ApplicationRecord
  ###
  #
  # Has Many relationships
  #
  ###
  has_many :purchases
  has_many :paid_purchases, -> { where "paid_at is not NULL" }, class_name: "Purchase"

  has_many :invoice_logs,
           primary_key: "id",
           foreign_key: "purchase_targeted_product_id",
           class_name: "InvoiceLog"
  has_many :targeted_product_stores, dependent: :destroy

  ###
  #
  # Belongs to relationships
  #
  ###
  belongs_to  :targeted_offer
  belongs_to  :product

  ###
  #
  # Virtual Attributes
  #
  ###

  ###
  #
  # Constants
  #
  ###
  RENEWAL_HAVING_CONDITIONS = %Q|( ((targeted_products.renewals_past_due  = true) OR (targeted_products.renewals_past_due  = false and (DATE(last_expiration_date) >=  DATE(:past_due_date) )))                                                         AND
                                   ((targeted_products.renewals_due       = true) OR (targeted_products.renewals_due       = false and (DATE(last_expiration_date) < DATE(:past_due_date) or DATE(last_expiration_date) > DATE(:four_weeks_from_now)))) AND
                                   ((targeted_products.renewals_upcomming = true) OR (targeted_products.renewals_upcomming = false and (DATE(last_expiration_date) <= DATE(:four_weeks_from_now) ))))|
  ###
  #
  # Validations
  #
  ###
  validates :product, :targeted_offer, :price_adjustment_type, :price_adjustment, presence: true
  validate :product_country_website_same_as_targeted_offer

  ###
  #
  # Callbacks
  #
  ###
  before_validation :set_currency

  before_create  :check_targeted_offer_is_inactive
  before_destroy :check_targeted_offer_is_inactive, :check_paid_purchase_count

  scope :for_targeted_person_by_item, ->(item, person_id) {
    joins(:product, targeted_offer: [:targeted_people])
      .where(
        products: { applies_to_product: item.class.name },
        targeted_offers: {
          status: "Active",
        },
        targeted_people: { person_id: person_id },
      )
      .where("targeted_offers.start_date <= now() && now() <= targeted_offers.expiration_date")
  }

  def check_paid_purchase_count
    paid_purchases.count.zero?
  end

  def check_targeted_offer_is_inactive
    throw(:abort) if targeted_offer.present? && targeted_offer.status == "Active"
  end

  ###
  #
  # Class Methods
  #
  ###

  def self.matching_product(offer, product)
    offer.targeted_products.find { |tp| tp.product_id == product.id }
  end

  # retrieves the current and applicable targeted_product model for a given customer and product
  def self.targeted_product_for_active_offer(person, product, item_to_price = nil)
    params = {
      person_id: person.id,
      product_id: product.id,
      created_on: person.created_on,
      country_website_id: person.country_website_id
    }

    having_conditions = nil
    # If we're pricing a renewal lets listen to the renewal rules defined in targeted_product
    if (product.product_type == "Renewal" and item_to_price.is_a? Renewal and item_to_price.renewal_items.first.related_type == "Album")
      having_conditions = [RENEWAL_HAVING_CONDITIONS, { past_due_date: Date.today - 30.days, four_weeks_from_now: Date.today + 4.weeks }]
      params[:album_id] = item_to_price.renewal_items.first.related_id
    end

    if item_and_product_are_single_year?(item_to_price, product)
      params = params.merge(product_type: Product::AD_HOC, applies_to_product: item_to_price.class.name)
      targeted_stores_offer = TargetedProductStoreService.fetch_offer(params, item_to_price)

      return matching_product(targeted_stores_offer, product) if targeted_stores_offer
    end

    TargetedProduct.select("targeted_products.*, max(expires_at) as last_expiration_date")
                   .joins(<<-SQL.strip_heredoc)
        INNER JOIN targeted_offers ON targeted_products.targeted_offer_id=targeted_offers.id
        INNER JOIN targeted_people tpe ON tpe.targeted_offer_id=targeted_offers.id
        LEFT OUTER JOIN albums a ON a.person_id = tpe.person_id
        LEFT OUTER JOIN renewal_items ri ON ri.related_id = a.id
        LEFT OUTER JOIN renewal_history rh ON rh.renewal_id = ri.renewal_id
                   SQL
                   .where("#{TargetedOffer::ACTIVE_CONDITIONS} AND tpe.person_id = :person_id AND targeted_products.product_id = :product_id #{if params[:album_id]
                                                                                                                                                 'and a.id = :album_id'
                                                                                                                                               end}",
                          params)
                   .group("tpe.person_id")
                   .having(having_conditions)
                   .first
  end

  def self.targeted_product_store_offer(item_to_price:, person:, product_id:)
    return unless Product.one_yr_ad_hoc.pluck(:id).include?(product_id)

    params = {
      applies_to_product: item_to_price.class.name,
      created_on: person.created_on,
      product_type: Product::AD_HOC,
      person_id: person.id,
      product_id: product_id,
      country_website_id: person.country_website_id
    }

    TargetedProductStoreService.fetch_offer(params, item_to_price)
  end

  def self.add_all_products(params)
    targeted_offer = TargetedOffer.find(params[:targeted_offer_id])
    products = Product.where(country_website_id: targeted_offer.country_website_id)

    ActiveRecord::Base.transaction do
      products.each do |product|
        product_params = params.merge(product_id: product.id)
        product = TargetedProduct.find_or_initialize_by(targeted_offer: targeted_offer, product_id: product.id)

        product.assign_attributes(product_params)

        product.save!
      end
    end
  end

  def self.item_and_product_are_single_year?(item, product)
    item && product.one_yr_ad_hoc?
  end

  ###
  #
  # Instance Methods
  #
  ###

  # adjust_price SHOULD take a decimal data type, adjust it according to the price_adjustment_type set in the database, and return a BigDecimal.
  # However, some legacy flows pass in Money, so they need to be converted.
  def adjust_price(original_price)
    original_price_decimal = (original_price.instance_of?(Money) ? original_price.amount : original_price)

    price =
      case price_adjustment_type
      when "percentage_off"
        original_price_decimal * ((100.00 - price_adjustment) * 0.01)
      when "override"
        price_adjustment
      when "dollars_off"
        original_price_decimal - price_adjustment
      else
        original_price_decimal
      end

    [BigDecimal("0"), price].max
  end

  def discount_amount_cents(original_price)
    original_price = original_price.to_i
    disc =
      case price_adjustment_type
      when "percentage_off"
        original_price * (price_adjustment / 100)
      when "override"
        [original_price - (price_adjustment * 100), 0].max
      when "dollars_off"
        price_adjustment * 100
      else
        0
      end

    [original_price, disc].min.to_i
  end

  private

  def set_currency
    self.currency = targeted_offer.country_website.currency
  end

  def product_country_website_same_as_targeted_offer
    return unless product.country_website_id.to_i != targeted_offer.country_website_id.to_i

    errors.add(:product, I18n.t("models.promotion.must_be_for_the_same_country_website_as_the_targeted_offer"))
  end
end
