# = Description
# This model is used as a way to track and display notes/changelogs of events that happen to the
# various models in our system.
#
# Currently the two models that we are tracking are the Person and Album (and subclasses).
# To add tracking/notes for any model in the application you can create a polymorphic association
# in the model by adding the following line.
#
#     has_many :notes, :as => :related, :order => 'created_at DESC'
#
# the note_created_by_id should be set to 0 (explicitly) if you wish to show that the note was
# generated by a system even, instead of an action taken by a person
#
# = History
# This model was originally called PersonNote, it was changed on 3/4/2010 to be polymorphic and
# allow tracking/notes for multiple object types.
#
# The person_note model holds a log of notes written by administrators in relation to an individual
# user's accounts.  This was initially created to allow admins to track history as it's related to fraudulent
# activity on the site.
#
# = Change Log
# [2009-09-01 -- CH]
# Created
# [2010-03-04 -- MJL]
# Renamed to Note and make polymorphic

class Note < ApplicationRecord
  belongs_to :related, polymorphic: true
  belongs_to :note_created_by, class_name: "Person"
  validates :subject, :note, :related_id, :related_type, :note_created_by_id, presence: true

  before_save :update_created_by_id

  def note_created_by_name
    if note_created_by_id.zero?
      "System"
    else
      note_created_by.name
    end
  end

  def update_created_by_id
    return unless note_created_by_id != 0 && note_created_by.takeover_admin_id

    self.note_created_by_id = note_created_by.takeover_admin_id
  end

  def self.generate_note_from_attrs(changed_attrs)
    changed_attrs.each_with_object("") do |(field, values), note_accum|
      old_value = values[0]
      new_value = values[1]

      case field
      when "salt"
        nil
      when "password"
        note_accum << "Changed password \n"
      else
        note_accum << "Changed field \"#{field}\" from \"#{old_value}\" to \"#{new_value}\" \n"
      end
    end
  end
end
