# frozen_string_literal: true

class EmailChangeRequest < ApplicationRecord
  validates :person, :token, :old_email, :new_email, presence: true
  validates :token, uniqueness: true

  belongs_to :person

  def self.generate_token
    SecureRandom.hex
  end
end
