# == Schema Information
#
# Table name: paypal_transfers
#
#  id                 :integer          not null, primary key
#  country_website_id :string(3)        not null
#  created_at         :datetime         default(NULL), not null
#  updated_at         :datetime         default(NULL), not null
#  transfer_status    :string(20)       default(""), not null
#  person_id          :integer          default(0), not null
#  payment_cents      :integer          default(0), not null
#  admin_charge_cents :integer          default(0), not null
#  currency           :string(3)        not null
#  paypal_address     :string(80)       default(""), not null
#  approved_by_id     :integer
#  suspicious_flags   :text(65535)
#  correlation_id     :string(255)
#

class PaypalTransfer < ApplicationRecord
  include Tunecore::Flagging::Suspicious
  include LoginTrackable
  include TransferMethods
  include WorkflowActiverecord

  COMPLETED_STATUS = "completed".freeze
  PROCESSING_STATUS = "processing".freeze
  PENDING_STATUS = "pending".freeze
  FAILED_STATUS = "failed".freeze

  # This tells workflow-activerecord gem which column is the status column
  workflow_column :transfer_status

  # Finite State Machine
  workflow do
    # Indicates that the transfer is 'pending Admin approval'
    state :pending do
      event :mark_as_processing, transitions_to: :processing
      event :mark_as_cancelled,  transitions_to: :cancelled
    end

    # Indicates that the transfer is currently being processed by Sidekiq/Paypal SOAP Api
    state :processing do
      event :revert_to_pending, transitions_to: :pending
      event :mark_as_completed, transitions_to: :completed
      event :mark_as_failed, transitions_to: :failed
    end

    # Indicates that the transfer was received and processed by Paypal
    state :completed

    # Indicates that the transfer cancelled by the Admin
    state :cancelled

    # Indicates ?
    state :failed
  end

  # Payment amount limit in cents
  LIMITS = {
    CountryWebsite::UNITED_STATES => 1_000_000,
    CountryWebsite::CANADA => 1_250_000,
    CountryWebsite::UK => 1_000_000,
    CountryWebsite::AUSTRALIA => 1_250_000,
    CountryWebsite::GERMANY => 1_000_000,
    CountryWebsite::FRANCE => 1_000_000,
    CountryWebsite::ITALY => 1_000_000
  }.with_indifferent_access.freeze

  VAT_TAX = "VAT".freeze

  has_paper_trail

  has_one :person_transaction, as: :target
  has_one :vat_tax_adjustment, as: :related
  has_one :outbound_invoice, as: :related
  has_one :transfer_metadatum, as: :trackable
  belongs_to :person
  belongs_to :country_website
  belongs_to :approved_by, class_name: "Person"

  attr_accessor :current_balance_cents,
                :payment_method,
                :paypal_address_confirmation,
                :payment_amount_confirmation,
                :password_entered,
                :payment_amount_main_currency,
                :payment_amount_fractional_currency

  before_validation :assign_currency,        on: :create
  before_validation :assign_country_website, on: :create

  delegate :country, to: :country_website

  validates :transfer_status, :person, :paypal_address, :currency, :country_website_id, presence: true
  validates :transfer_status, inclusion: { in: %w[pending processing completed failed cancelled] }
  validates_as_email :paypal_address
  validates :paypal_address, confirmation: { message: "must match confirmation" }
  validates :paypal_address_confirmation, presence: { on: :create }
  validates :payment_amount, numericality: true

  validate :verify_password, on: :create
  validate :payment_cents_limit

  validates_each :payment_cents, on: :create do |record, _attr_name, _value|
    if (record.nil? || record.current_balance_cents.nil? || record.payment_cents.nil?)
      record.errors.add(:payment_cents, I18n.t("models.billing.payment_cents_is_nil"))
    elsif (record.current_balance_cents - record.payment_cents).negative?
      record.errors.add(:payment_cents, I18n.t("models.billing.exceeds_available_balance"))
      record.errors.add(:payment_amount, I18n.t("models.billing.exceeds_available_balance"))
    end
  end

  before_save :ensure_state_change_is_workflow_compatible
  before_create :fixup_admin_charge_cents
  after_create :send_confirmation_email_to_account_email
  after_create :generate_metadatum

  alias_attribute :payee_receiver, :person

  scope :completed_and_successful, ->(person_id = nil, paypal_address = nil) do
    query = with_completed_state
    query = query.where(person_id: person_id) if person_id.present?
    query = query.where(paypal_address: paypal_address) if paypal_address.present?
    query
  end

  scope :recently_approved_transactions, ->(person_id, within_days) {
    completed_and_successful(person_id).where(
      paypal_transfers: {
        updated_at: DateTime.now.ago(Integer(within_days).days).beginning_of_day..DateTime.now
      }
    )
  }

  scope :auto_approval_eligible_transactions, ->(eligible) {
    case eligible
    when "yes"
      joins(:transfer_metadatum)
    when "no"
      left_joins(:transfer_metadatum).where(transfer_metadata: { trackable_id: nil })
    end
  }

  scope :find_all_pending_under_approval_threshold, -> {
    joins(:person)
      .with_pending_state
      .where(
        arel_table[:payment_cents].lteq(threshold_in_cents)
      )
  }

  scope :find_all_pending_over_approval_threshold, -> {
    joins(:person)
      .with_pending_state
      .where(
        arel_table[:payment_cents].gt(threshold_in_cents)
      )
  }

  def self.threshold_in_dollars
    Integer(SystemProperty.get(:approval_required_mass_pay_threshold), exception: false) || 0
  end

  def self.threshold_in_cents
    threshold_in_dollars * 100
  end

  def self.generate(person, params = {})
    transaction do
      paypal_transfer = new(
        paypal_address: params["paypal_address"].strip,
        payment_amount: params["payment_amount"],
        paypal_address_confirmation: params[:paypal_address_confirmation].strip,
        current_balance_cents: Tunecore::Numbers.decimal_to_cents(PersonBalance.find_and_lock_balance(person).balance),
        password_entered: params["password_entered"]
      )
      paypal_transfer.transfer_status = PENDING_STATUS
      paypal_transfer.person = person

      return paypal_transfer unless paypal_transfer.save

      PersonTransaction.create(
        person: person,
        debit: Tunecore::Numbers.cents_to_decimal(paypal_transfer.total_cents),
        target: paypal_transfer,
        comment: "Paypal funds transfer to: #{paypal_transfer.paypal_address}"
      )

      paypal_transfer
    end
  end

  # Wraps paypal_transfer-specific code in a transaction. Note: in case of
  # exceptions within the transaction, Paypal transfers may innacurately
  # wind back indicating no charge occured. There is no straightforward way
  # to avoid this (the solution is to lock the table with multiple transaction
  # blocks within it). This case is handled instead by audit log entries
  # which can be used to reconstruct the paypal transfer activity in
  # case of error. Best solution is to see a better API come from PayPal
  # that would give us two-phase commit of transactions or monitoring of
  # completed transactions.

  # Deprecated, DO NOT USE!!!
  def self.do_mass_pay(paypal_transfers, user_id)
    raise "Must provide an array of PaypalTransfers or PaypalTransfer ids." unless paypal_transfers.is_a?(Array)
    raise "No transactions specified." if paypal_transfers.empty?

    all_msgs = []

    unless paypal_transfers.first.is_a?(PaypalTransfer)
      paypal_transfers = PaypalTransfer.find(paypal_transfers) # default finder handles list of ids
    end

    # create a hash that groups transactions by country_website_id
    # e.g. {1 => [transfer1, transfer2], 2 => [transfer3, transfer4]}
    paypal_transfers_hash = paypal_transfers.group_by(&:country_website_id)

    paypal_transfers_hash.each do |country_website_id, transfers|
      country_website = CountryWebsite.find(country_website_id)

      begin
        PaypalTransfer.transaction do
          admin = Person.find(user_id)

          # TODO we have to use this for now, the code in front of the flag
          # expects a boolean and messages. Should replace boolean with a symbol once the flag is removed,
          # and Admin::PaypalMasspayService is fully deployed
          success, msgs = Admin::PaypalMasspayService.call(transfers, country_website, admin.id)

          all_msgs += msgs
          raise ActiveRecord::Rollback unless success
        end
      rescue StandardError => e
        Airbrake.notify(
          "Error while executing a paypal mass transfer",
          error: e.message,
          country_website: country_website,
          transfers: transfers,
          user_id: user_id
        )
      end
    end

    all_msgs
  end

  def self.completed_status
    COMPLETED_STATUS
  end

  def approving_transfer(user_id)
    transaction do
      mark_as_processing!
      update(approved_by_id: user_id)
      build_tax_adjustment
    end
  end

  def cancel
    return false unless reload.pending?

    transaction do
      rollback_paypal_payment!
      rollback_paypal_service_payment!
      mark_as_cancelled!
    rescue ActiveRecord::Rollback => e
      logger.error(e.to_s)
      false
    end
  end

  def set_to_export!
    # method defined to act polymorphic w/ check transfers
  end

  def payment_amount=(arg)
    arg.tr_s!(",#{USED_CURRENCY} ", "") if arg.is_a? String
    self[:payment_cents] = (LongDecimal(arg).round_to_scale(2, LongDecimal::ROUND_DOWN).int_val rescue arg)
  end

  def payment_amount
    return if self[:payment_cents].nil?

    LongDecimal(
      self[:payment_cents] +
      Integer(vat_tax_adjustment&.amount || 0, exception: false),
      2
    )
  end

  def payment_amount_before_type_cast
    return if payment_cents_before_type_cast.nil?

    LongDecimal(payment_cents_before_type_cast, 2)
  end

  def payment_cents_limit
    limit = LIMITS.fetch(Integer(country_website_id), 0)

    return if (payment_cents >= 0.01 && payment_cents <= limit)

    currency_symbol = MONEY_CONFIG[person.person_balance.currency][:unit]
    errors.add(
      :withdrawal_amount,
      I18n.t(
        "models.billing.must_be_between", currency_symbol: currency_symbol,
                                          limit: sprintf("%.2f", limit / 100.0)
      )
    )
  end

  def total_cents
    Integer(payment_cents) + Integer(admin_charge_cents)
  end

  def set_suspicious_flags
    self.suspicious_flags = Tunecore::Flagging::TransferFlagger.new(
      self,
      person.paypal_transfers,
      :paypal_address
    ).set_flags
  end

  # @deprecated Used only by do_mass_pay_without_transaction, which is itself deprecated.
  def self.withdrawal_account(country_website)
    PAYPAL_BUSINESS[country_website.currency] || PAYPAL_BUSINESS["DEFAULT"]
  end

  class << self
    ActiveSupport::Deprecation.new(nil, "tc-www").deprecate_methods(self, :withdrawal_account)
  end

  def payee
    paypal_address
  end

  def create_outbound_invoice
    return unless person.vat_applicable?

    build_outbound_invoice(
      person_id: person.id,
      user_invoice_prefix: person.outbound_invoice_prefix,
      vat_tax_adjustment_id: vat_tax_adjustment&.id,
      invoice_date: Date.today,
      currency: person.currency
    ).save
  end

  def auto_approveable?
    pending?
  end

  protected

  def fixup_admin_charge_cents
    self.admin_charge_cents ||= 0
  end

  def verify_password
    return unless person.authenticate!(password_entered).nil?

    errors.add(:base, I18n.t("models.billing.sorry_that_password_is_incorrect"))
  end

  def assign_currency
    self.currency = person.currency
  end

  def assign_country_website
    self.country_website = person.country_website
  end

  def send_confirmation_email_to_account_email
    PersonNotifier.paypal_withdrawal_confirmation(self).deliver
  end

  class << self
    ActiveSupport::Deprecation
      .new(nil, "tc-www")
      .deprecate_methods(self, do_mass_pay: "Transfers::ApproveService.call")
  end

  private

  def build_tax_adjustment
    tax_info = TcVat::OutboundVatCalculator.new(person).fetch_vat_info
    return if tax_info.empty?

    tax_amount = calc_tax_amount(Float(tax_info[:tax_rate], exception: false))
    build_vat_tax_adjustment(
      person_id: person.id,
      amount: tax_amount,
      tax_rate: Float(tax_info[:tax_rate], exception: false),
      vat_registration_number: person.vat_information&.vat_registration_number,
      tax_type: VAT_TAX,
      trader_name: tax_info[:trader_name],
      place_of_supply: tax_info[:place_of_supply],
      error_message: nil,
      customer_type: tax_info[:customer_type],
      vat_amount_in_eur: vat_amount_in_eur(tax_amount),
      foreign_exchange_rate_id: person.eur_fx_rate&.id
    )
  end

  def vat_amount_in_eur(tax_amount)
    fx_rate = person.eur_fx_rate
    return if fx_rate.nil?

    tax_amount * fx_rate.exchange_rate
  end

  def calc_tax_amount(tax_rate)
    (payment_cents * tax_rate) / 100
  end

  def generate_metadatum
    return unless FeatureFlipper.show_feature?(:auto_approved_withdrawals, person.id)

    create_transfer_metadatum(
      login_event: LoginEvent.where(person_id: person.id).last
    )
  end

  def rollback_paypal_payment!
    PersonTransaction.create!(
      person: person,
      debit: 0,
      credit: Tunecore::Numbers.cents_to_decimal(payment_cents),
      target: self,
      comment: "Rollback of Paypal Payment: #{person_transaction&.comment}"
    )
  end

  def rollback_paypal_service_payment!
    PersonTransaction.create!(
      person: person,
      debit: 0,
      credit: Tunecore::Numbers.cents_to_decimal(admin_charge_cents),
      target: self,
      comment: "Rollback of Paypal Payment Service Fee"
    )
  end

  # This is primarily if some future dev accidently uses .update(transfer_status: some_bad_status)
  # This will throw and error
  def ensure_state_change_is_workflow_compatible
    return if changes["transfer_status"].blank?
    return if changes["transfer_status"].first.blank?

    _, next_transfer_status = changes["transfer_status"].map(&:to_sym)

    return if next_transfer_status.in? legal_transition_states

    raise "Invalid state transition,
      please use WorkflowActiveRecord for state transitions"
  end

  def legal_transition_states
    previous_transfer_status, _ = changes["transfer_status"].map(&:to_sym)

    self.class
        .workflow_spec
        .states[previous_transfer_status]
        .events
        .flat_map { |_, event| event.map(&:transitions_to) }
  end
end
