# frozen_string_literal: true

class DeletedAccount < ApplicationRecord
  belongs_to :person
  belongs_to :admin, class_name: "Person"

  DELETE_TYPES = %w[GDPR OFAC].map(&:freeze).freeze
  SCRUB_STRING = "deleted"
end
