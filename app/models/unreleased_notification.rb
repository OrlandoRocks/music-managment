class UnreleasedNotification < Notification
  attr_accessor :release_type

  def setup_system_notification
    if notification_item
      @release_type = I18n.t(".#{notification_item.class.name.downcase}_display", locale: person.locale).downcase
    end

    set_url
    set_title
    set_text
    set_link_text
    set_image_url
    auto_archive
  end

  def self.create_notifications(options = {})
    albums = unreleased_albums(options)
    Rails.logger.info "Creating release notifications for #{albums.size}"
    albums.each do |album|
      Rails.logger.info "Creating a release notification for person=#{album.person.id}, album=#{album.id}"
      UnreleasedNotification.create(
        notification_item: album,
        person: album.person
      )
    end
  end

  def self.unreleased_albums(options = {})
    run_date = options[:run_date] || Date.today
    Album.joins("inner join people on albums.person_id = people.id").joins("left outer join albums a_d on a_d.person_id = people.id and a_d.payment_applied = 1").where("albums.payment_applied = 0 and a_d.id is null").where("DATEDIFF(?, albums.created_on) <= 120", run_date).where("MOD(DATEDIFF(?, albums.created_on), 30) = 0", run_date).order("albums.person_id ASC").all
  end

  private_class_method :unreleased_albums

  private

  def auto_archive
  end

  def set_url
    self.url = "/albums/#{notification_item_id}"
  end

  def set_title
    self.title = I18n.t("model.notifications.you_have_unreleased", release_type: release_type, locale: person.locale)
  end

  def set_text
    missing_items
    self.text =
      if @missing.empty?
        I18n.t("model.notifications.your_release_is_unreleased", release_type: release_type, locale: person.locale)
      else
        I18n.t("model.notifications.your_is_missing", release_type: release_type, missing: @missing.to_sentence, locale: person.locale)
      end
  end

  def missing_items
    @missing = []
    missing_artwork
    missing_tracks
    missing_artwork_template
    missing_stores
  end

  def missing_artwork
    @missing << I18n.t("model.notifications.artwork", locale: person.locale) unless notification_item.has_artwork?
  end

  def missing_tracks
    @missing << I18n.t("model.notifications.tracks", locale: person.locale)  unless notification_item.has_tracks?
  end

  def missing_artwork_template
    return if notification_item.has_artwork_template_info?

    @missing << I18n.t("model.notifications.an_artwork_template", locale: person.locale)
  end

  def missing_stores
    @missing << I18n.t("model.notifications.stores", locale: person.locale) unless notification_item.has_stores?
  end

  def set_image_url
    self.image_url =
      if notification_item && notification_item.artwork
        notification_item.artwork.artwork.url(:small)
      else
        "/images/notifications/notification_nocover.jpg"
      end
  end

  def set_link_text
    self.link_text = I18n.t("model.notifications.click_here", locale: person.locale)
  end
end
