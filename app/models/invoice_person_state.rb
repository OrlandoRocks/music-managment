class InvoicePersonState
  def initialize(invoice)
    @invoice = invoice
    @person = invoice.person
  end

  def purchased_artist_services
    @purchased_artist_services ||= @invoice.artist_services_product_names
  end

  def is_first_purchase
    @is_first_purchase ||= @invoice.first_purchase?
  end

  def has_distro_purchase
    @has_distro_purchase ||= @invoice.album_or_credit_purchase?
  end

  def is_first_time_dist_customer
    @is_first_time_dist_customer ||= @invoice.check_and_set_first_time_distribution_or_credit
  end

  def has_purchased_publishing_before
    @has_purchased_publishing_before ||= @invoice.purchased_publishing_before?
  end

  def user_has_active_ytm
    @user_has_active_ytm ||= @person.has_active_ytm?
  end

  def has_ytm_purchase
    @has_ytm_purchase ||= @invoice.ytm_purchase?
  end

  def user_has_publishing
    @user_has_publishing ||= (@person.paid_for_composer? || @person.paid_for_publishing_composer?)
  end

  def songwriter_product_purchase
    @songwriter_product_purchase ||= @invoice.publishing_purchase?
  end

  def purchased_product_names
    @purchased_product_names ||= @invoice.purchases.map { |p| p.product.name }.uniq.join(", ")
  end

  def recently_purchased_first_distro_and_publishing
    @recently_purchased_first_distro_and_publishing ||= @invoice.recently_purchased_first_distro_and_publishing?(
      is_first_time_dist_customer, songwriter_product_purchase
    )
  end

  def invoice_bought_recently
    @invoice_bought_recently ||= @invoice.settled_at > Time.now - 2.minutes
  end

  def has_missing_splits
    @has_missing_splits ||= Composer.missing_splits?(@person) if user_has_publishing
  end

  def has_unactioned_ytm_tracks
    @has_unactioned_ytm_tracks ||= YtmTracks.new(@person).has_unactioned_songs? if user_has_active_ytm
  end

  def has_first_distro_before_publishing_purchase
    !has_purchased_publishing_before && is_first_time_dist_customer
  end

  def has_publishing_and_first_purchase
    songwriter_product_purchase && is_first_purchase
  end

  def missing_ytm_tracks_or_splits
    has_ytm_purchase || active_composer_with_rights_app? || has_unactioned_ytm_tracks
  end

  def active_composer_with_rights_app?
    has_rights_app? && active_composer_or_album_purchase?
  end

  def has_distribution_purchase?
    @invoice.purchases.exists?(related_type: "Album")
  end

  private

  def has_rights_app?
    true
  end

  def active_composer_or_album_purchase?
    @invoice.purchases.exists?(related_type: ["Album", "Composer", "PublishingComposer"]) && (
      @invoice.person.composers.any?(&:is_active?) || @invoice.person.publishing_composers.any?(&:is_active?)
    )
  end
end
