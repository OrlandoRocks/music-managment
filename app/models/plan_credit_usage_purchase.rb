class PlanCreditUsagePurchase < ApplicationRecord
  validates :plan, :purchase, :credit_usage, presence: true

  belongs_to :plan
  belongs_to :purchase
  belongs_to :credit_usage

  def self.log_purchase(plan_id:, purchase_id:, credit_usage_id:)
    create!(plan_id: plan_id, purchase_id: purchase_id, credit_usage_id: credit_usage_id)
  rescue ActiveRecord::RecordInvalid => e
    log_to_airbrake(e, plan_id, purchase_id, credit_usage_id)
    false
  end

  def self.log_to_airbrake(error, plan_id, purchase_id, credit_usage_id)
    Tunecore::Airbrake.notify(
      "Unable to create PlanCreditUsagePurchase log entry",
      {
        plan: plan_id,
        purchase: purchase_id,
        credit_usage: credit_usage_id,
        error: error
      }
    )
  end
end
