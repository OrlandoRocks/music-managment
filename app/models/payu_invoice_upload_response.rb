# frozen_string_literal: true

class PayuInvoiceUploadResponse < ApplicationRecord
  self.ignored_columns = ["payments_os_transaction_id"]

  belongs_to :related, polymorphic: true
  belongs_to :payments_os_transaction, class_name: "PaymentsOSTransaction"

  validates :related, :api_response_code, :api_message, presence: true

  enum related_type: { payments_os_transaction: PaymentsOSTransaction.to_s, payu_transaction: PayuTransaction.to_s }

  SUCCESS_RESPONSE_CODE = "00"
end
