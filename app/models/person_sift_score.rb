class PersonSiftScore < ApplicationRecord
  has_paper_trail skip: [:sift_order_id]

  belongs_to :person

  validates :person_id, uniqueness: true
  validates :score, inclusion: 0..1

  def pending_v1_order?
    sift_order_id.present? && sift_order_id.starts_with?("v1")
  end
end
