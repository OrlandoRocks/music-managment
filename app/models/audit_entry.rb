# == Schema Information
# Schema version: 404
#
# Table name: audit_entries
#
#  id         :integer(11)     not null, primary key
#  action     :string(255)
#  comment    :string(255)
#  created_at :datetime
#  actor_type :string(255)
#  actor_id   :integer(11)
#

class AuditEntry < ApplicationRecord
  belongs_to :actor, polymorphic: true
end
