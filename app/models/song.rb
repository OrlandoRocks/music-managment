require "digest/md5"
require "aws-sdk"
require "S3"

class Song < ApplicationRecord
  include Tunecore::Creativeable
  include Tunecore::Publishing::Song
  include Distribution::TrackLevelSong
  include Linkable
  include S3Downloadable
  include PublishingAdministrationHelper
  include Jsonable
  include SongMethods
  include ContentFingerprintable
  extend ArelTableMethods

  # Temporary, for safe migration
  self.ignored_columns = %w[mp3_ident s3_96kb_asset_id upload_id]
  MINIMUM_VALID_DURATION = 2001
  METADATA_FOR_REVIEW_RESPONSE = { error: "No longer used." }.freeze

  has_paper_trail on: [:update], if: proc { |s| s.ready_for_distribution? }

  # ================
  # = Associations =
  # ================
  belongs_to :s3_asset
  belongs_to :s3_flac_asset, class_name: "S3Asset"
  belongs_to :s3_streaming_asset, class_name: "S3Asset"
  belongs_to :s3_orig_asset, class_name: "S3Asset"
  belongs_to :external_asset
  belongs_to :composition
  belongs_to :publishing_composition
  belongs_to :metadata_language_code, class_name: "LanguageCode"
  belongs_to :lyric_language_code, class_name: "LanguageCode"

  has_one :lyric
  has_one :cover_song_metadata
  has_one :person, through: :album
  has_one :upload
  has_one :preorder_instant_grat_song, dependent: :destroy
  has_one :copyright, as: :copyrightable, dependent: :destroy
  has_one :song_copyright_claim, dependent: :destroy
  has_one :s3_detail, through: :s3_asset
  has_one :distribution_api_song
  has_one :immersive_audio
  has_one :immersive_audio_asset, source: :s3_asset, through: :immersive_audio
  has_one :spatial_audio # do we still want these?
  has_one :spatial_audio_asset, source: :s3_asset, through: :spatial_audio # do we still want these?

  has_many :soundout_reports, as: :track
  has_many :creatives, -> { order "creatives.id" }, as: :creativeable
  has_many :notes, -> { order "created_at DESC" }, as: :related
  has_many :sales_records, as: :related
  has_many :external_purchase_items, as: :related_item, dependent: :destroy
  has_many :external_purchases, through: :external_purchase_items
  has_many :sync_license_requests
  has_many :muma_songs, primary_key: "composition_id", foreign_key: "composition_id"
  has_many :salepoint_songs
  has_many :song_salepoints, through: :salepoint_songs, source: :salepoint
  has_many :distribution_songs, through: :salepoint_songs
  has_one :ytm_ineligible_song
  has_one :ytm_blocked_song
  has_many :song_distribution_options
  has_many :track_monetizations
  has_many :track_monetization_blockers
  has_many :non_tunecore_songs
  has_many :creative_song_roles, dependent: :destroy
  has_many :song_roles, through: :creative_song_roles
  has_many :recordings, as: :recordable
  has_many :song_start_times, dependent: :destroy
  has_one  :scrapi_job, -> { merge(ScrapiJob.order(id: :desc)) }
  delegate :scrapi_job_details, to: :scrapi_job
  has_many :copyright_claims, as: :asset
  has_one :royalty_split_song, inverse_of: :song
  has_one :royalty_split, through: :royalty_split_song, inverse_of: :songs
  has_many :royalty_split_recipients, through: :royalty_split, source: :recipients, inverse_of: :songs
  has_many :royalty_split_details

  # TODO - Replace the below block with `has_and_belongs_to_many :songwriters`
  has_many :songwriter_roles,
           -> { where(song_role_id: SongRole::SONGWRITER_ROLE_ID) },
           source: :creative_song_roles,
           class_name: "CreativeSongRole"
  has_many :songwriters, through: :songwriter_roles, source: :creative, class_name: "Creative"
  # TODO - Replace the above block with `has_and_belongs_to_many :songwriters`

  #
  #  Named Scopes
  #
  scope :have_been_live,
        -> {
          includes(:album)
            .where("albums.known_live_on IS NOT NULL")
        }
  scope :by_isrc, ->(isrcs_array) { where("songs.optional_isrc in (?) or songs.tunecore_isrc in (?)", isrcs_array, isrcs_array) }
  scope :select_id_isrc, -> { select("songs.id, coalesce(nullif(songs.optional_isrc,''), songs.tunecore_isrc) as resolved_isrc") }

  scope :monetized_for,
        ->(store_id) {
          joins(:album, :track_monetizations)
            .where(track_monetizations: { store_id: store_id, eligibility_status: "eligible", takedown_at: nil })
            .where(albums: { payment_applied: 1 })
            .where.not(albums: { album_type: "Ringtone" })
            .where(<<~SQL)
              (
                (albums.takedown_at IS NULL AND albums.is_deleted = 0 AND albums.finalized_at is not null)
                OR
                track_monetizations.id IS NOT NULL
              )
              AND
              (
                (albums.legal_review_state = 'APPROVED' OR albums.legal_review_state = 'DO NOT REVIEW')
                OR
                (albums.legal_review_state = 'NEEDS REVIEW' AND track_monetizations.state = 'approved')
              )
            SQL
        }

  scope :with_s3_asset,
        ->(albums:) {
          select("distinct(songs.name), songs.id, songs.album_id, songs.s3_asset_id")
            .includes(album: :artwork)
            .joins(:s3_asset)
            .where(album: albums)
        }

  scope :null_esi_songs,
        ->(albums:) {
          where(album: albums)
            .includes(:external_service_ids)
            .where(external_service_ids: { linkable_id: nil })
            .distinct
        }

  scope :spotify_songs_with_artwork,
        ->(albums:, search_key:) {
          joins(:external_service_ids)
            .where(album: albums)
            .where(external_service_ids: { service_name: ExternalServiceId::SPOTIFY_SERVICE })
            .where("songs.name LIKE ?", search_key)
            .includes(album: :artwork).with_s3_asset(albums: albums)
        }

  # ===============
  # = Validations =
  # ===============
  validates_associated :upload
  validates :name, presence: true

  validates_with Utf8mb3Validator, fields: [:name]
  validates :name, length: { maximum: 255 }
  validates :optional_isrc, length: { is: 12, if: :has_optional_isrc? }
  validates :optional_isrc,
            format: {
              if: :has_optional_isrc?,
              with: /\A[A-Z]{2}[A-Z0-9]{3}[0-9]{7}\z/
            }

  validate :optional_isrc_validator, if: :has_optional_isrc?
  # validates_uniqueness_of :tunecore_isrc, :message => %q|TuneCore ISRC is not unique.|

  # validate :artist_is_not_blank
  validate :check_for_primary_artist
  validate :valid_previously_released_at_date
  validate :valid_duration?

  before_validation :titleize_name
  before_validation :uppercase_isrc
  before_validation :strip_whitespace
  before_validation :reject_artists_from_album
  before_validation :correct_clean_version_status

  before_create :set_track_number

  before_update :update_composition_name, if: :account_has_publishing_administration?
  after_destroy :destroy_composition, unless: :composition_has_songs?
  after_destroy :update_album_track_numbers
  after_destroy :remove_associated_files
  after_destroy :update_preorder_grat_tracks
  after_commit :invalidate_cache
  after_commit :update_tunecore_isrc
  after_commit :notify_airbrake_after_optional_isrc_update, if: :saved_change_to_optional_isrc?

  # ======================
  # = Virtual Attributes =
  # ======================
  attr_writer :big_box_meta_data

  delegate :email, :name, to: :person, prefix: true, allow_nil: true

  def artist_name=(value)
    return if value.blank?

    creatives << Creative.new(role: "primary_artist", name: value)
  end

  def artist_name
    if primary_artists.length.zero?
      if album.present? && !(album.is_various? || album.is_dj_release?)
        album.artist_name
      elsif album.present? && album.is_various?
        all_primary_creatives = album.primary_artists + primary_artists
        d = all_primary_creatives.collect(&:name)
        d.to_sentence(last_word_connector: " & ", two_words_connector: " & ")
      else
        ""
      end
    else
      all_primary_creatives = primary_artists

      all_primary_creatives += album.primary_artists unless album.is_dj_release?

      d = all_primary_creatives.collect(&:name)
      d.to_sentence(last_word_connector: " & ", two_words_connector: " & ")
    end
  end

  #
  #  Override the default setter for creatives
  #  to handle an incoming hash of parameters
  #
  #  Useful when sending form data to the setter
  #
  def creatives=(values)
    # We reject any blank names
    values.reject! { |creative| creative["name"].blank? }

    # We reject any creative with multiple names
    if values.detect { |creative| creative["name"].split(", ").length > 1 }
      @too_many_creatives = true
      return
    end

    # If we are left without any values or if there are no primary artist left and it's a various artist album,
    # we will add an error and do nothing else.
    if values.detect { |creative| creative["role"] == "primary_artist" }.blank? && (album != nil && album.is_various?)
      @creatives_missing = true
      return
    end

    #  Get an array of Creative objects
    #  from the incoming parameters
    #  unless we are setting the creative empty
    result =
      values.map { |creative|
        artist_name = creative["name"]
        creative["name"] = artist_name

        if creative.is_a?(Creative)
          creative
        else
          Creative.new(creative)
        end
      }

    # Check if we are adding two or more creatives with the same name
    seen = []
    result.each do |c|
      seen << c unless seen.map { |c1| c1.artist.name }.include? c.artist.name
    end

    creatives.clear
    seen.each do |creative|
      creatives << creative
    end
  end

  # =================
  # = Class Methods =
  # =================
  def self.find_isrc(arg)
    where("tunecore_isrc = ? or optional_isrc = ?", arg, arg).limit(1).first
  end

  def self.missing_fingerprint
    Song
      .select(song_t[Arel.star]).joins(:album, :s3_asset)
      .where(album_t[:finalized_at].not_eq(nil))
      .where(song_t[:content_fingerprint].eq(nil))
      .where(s3_asset_t[:uuid].not_eq(nil))
      .order("finalized_at DESC")
      .limit(10_000)
  end

  def self.all_fingerprinted_content
    Song
      .select(song_t[Arel.star])
      .joins(:album, :s3_asset)
      .where(song_t[:content_fingerprint].not_eq(nil))
      .order("finalized_at DESC")
  end

  # ====================
  # = Instance Methods =
  # ====================
  def allow_different_format?
    album && album.allow_different_format?
  end

  def bigbox_uuid
    if s3_asset
      s3_asset.uuid
    else
      nil
    end
  end

  def big_box_meta_data
    return @big_box_meta_data if @big_box_meta_data.present?
    return unless s3_asset&.uuid

    attributes = AwsWrapper::SimpleDb.get_attributes(
      domain: BIGBOX_ASSETS_SDB_DOMAIN,
      item: s3_asset.uuid
    )

    @big_box_meta_data =
      if attributes.empty?
        nil
      else
        attributes
      end
  end

  # Preserved for CRT compatibility, referenced by content_review_controller
  def metadata_for_review
    METADATA_FOR_REVIEW_RESPONSE
  end

  def not_big_box_asset?
    s3_asset.blank? || big_box_meta_data.blank? || big_box_meta_data["duration"].blank?
  end

  #
  # Returns a duration in ms from the bigbox meta data.  Requires an asset to be
  # a bigbox asset
  #
  def duration
    return duration_in_seconds if duration_in_seconds

    return if not_big_box_asset?

    # Convert from Milliseconds
    (big_box_meta_data["duration"].first.to_i / 1000.0).ceil
  end

  def valid_duration?
    return unless ENV["VALIDATE_SONG_MINIMUM_DURATION"]
    return unless duration

    valid = duration >= MINIMUM_VALID_DURATION
    errors.add(:invalid_duration, I18n.t("model.song.invalid_duration")) unless valid
  end

  def name=(value)
    self[:name] =
      if allow_different_format? && value.present?
        value.strip
      else
        value.blank? ? "" : Utilities::Titleizer.titleize(value)
      end
  end

  def remove_associated_files
    # When the upload is set to nil its song_id gets set to nil and the uploads after_save method destroys it.
    self.upload = nil
    s3_flac_asset.destroy! if s3_flac_asset
    s3_orig_asset.destroy! if s3_orig_asset
  end

  # We reject any names that are the same as the album
  def reject_artists_from_album
    album.creatives.each do |album_creative|
      creatives.to_a.reject! { |creative| creative["name"] == album_creative.name }
    end if album
  end

  def add_upload=(new_upload)
    remove_associated_files
    self.upload = new_upload
  end

  def correct_clean_version_status
    self.clean_version = false if parental_advisory
  end

  def has_optional_isrc?
    optional_isrc.present?
  end

  def isrc
    has_optional_isrc? ? optional_isrc : tunecore_isrc
  end

  def original_filename
    upload.original_filename if upload
  end

  def uploaded_filename_base
    upload.uploaded_filename =~ /^([^.]+)[.].+$/
    $1 # RegExp match
  end

  def md5_key
    Base64.encode64(MD5.digest("#{id}_t00nK0re58_#{album.id}_#{album.person_id}")).strip
  end

  # the following method might be able to be removed.
  def is_orig_actually_on_s3?
    s3_orig_asset && s3_orig_asset.exists?
  end

  def is_flac_actually_on_s3?
    s3_flac_asset && s3_flac_asset.exists?
  end

  def s3_key_prefix
    # generate the s3_key_prefix using the version, song_id, and album_id
    "flacv1/#{album.id}/#{id}"
  end

  def s3_orig_key_prefix
    # generate the s3_key_prefix using the version, song_id, and album_id
    "origv1/#{album.id}/#{id}"
  end

  def s3_96kb_key_prefix
    # generate the s3_key_prefix using the version, song_id, and album_id
    "mp3stream/#{album.id}/#{id}"
  end

  def paid?
    songstatus == "paid"
  end

  def uploaded?
    Upload.exists?(song_id: id)
  end

  def valid_for_album_check?
    uploaded? && upload.valid? && is_orig_actually_on_s3?
  end

  def completed?
    songstatus == "completed"
  end

  # Sends the original asset for this song to the Bigbox system.
  # Prefix should be set to the current_user#id
  def send_to_bigbox(prefix)
    BigboxWorker.perform_async(BIGBOX_JOB_QUEUE, "import", prefix, [s3_url(3600)], AWS_ACCESS_KEY, AWS_SECRET_KEY, { song_id: id, person_id: album.person_id })
    logger.info("Enqueued bigbox import for song #{id}")
  end

  # 2012-03-20 AK
  # Reads from Bigbox assets and tries to find a matching asset for this song.
  # Matches the Bigbox S3 key against song name, track number, and album name.
  #
  # item_col -- An AWS ItemCollection of assets to try an match with. If not specified will query SDB.
  #
  # Returns an array of hashes containing the Bigbox metadata.
  def suggest_assets(items = nil)
    items = album.person.bigbox_assets_hashes if items.nil?
    songname = CGI.escape(name.downcase)
    albumname = CGI.escape(album.name.downcase)
    suggested_assets = []
    items.each do |i|
      filename = i["original_filename"][0].split("-")[1..-1].join("-").downcase
      if i["isrc"] && i["isrc"][0] == isrc
        suggested_assets << i
        next
      end
      if filename.include?(track_number.to_s) && filename.include?(songname) && filename.include?(albumname)
        suggested_assets << i
      end
    end
    suggested_assets
  end

  # 2012-04-17 AK
  # Queries bigbox for an asset marked with the same ISRC as this song
  def bb_asset_for_isrc(items = nil)
    sdb = AWS::SimpleDB.new(
      {
        access_key_id: AWS_ACCESS_KEY,
        secret_access_key: AWS_SECRET_KEY,
        max_retries: 10,
        ssl_verify_peer: false
      }
    )
    domain = sdb.domains.create(BIGBOX_ASSETS_SDB_DOMAIN)

    query = "locker_name = '#{id}' and isrc = '#{isrc}'"

    item_collection = domain.items.select("*").where(query)
    items = []
    item_collection.each do |item|
      item_hash = item.attributes
      item_hash["uuid"] = [item.name]
      items << item_hash
    end
    items
  end

  def download_filename
    ext =
      if s3_asset
        s3_asset.key.split(".").last
      else
        s3_orig_asset.key.split(".").last
      end
    "#{artist.name}-#{name}.#{ext}"
  end

  # Generates a url that can be used to download the FLAC directly from amazon.  Expiration is in seconds
  def s3_flac_url(expires_in = 60)
    generator = S3::QueryStringAuthGenerator.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, true)
    generator.expires_in = expires_in
    generator.get(FLAC_BUCKET_NAME, s3_flac_asset.key)
  end

  def streaming_url(expires_in: 1.week)
    AwsWrapper::S3.read_url(
      bucket: s3_streaming_bucket,
      key: s3_streaming_key,
      options: {
        expires: expires_in,
        port: 443
      }
    )
  end

  def s3_streaming_bucket
    "streaming.tunecore.com"
  end

  def s3_streaming_key
    "#{album.id}/#{track_num}.mp3"
  end

  # Creates and returns a URL that will work n times to download the streaming asset for this song,
  # using the TicketBooth server.  Will return false if there is no streaming distribution.
  #
  # Important: This method makes an HTTP request to an external resource and could possibly fail if the
  #            TicketBooth server is offline.  Will return nil if the request to the TicketBooth service fails.
  def create_restricted_url(downloads_allowed = 1)
    return false unless album.streaming_distribution

    t = Thread.new { set_download_filename } # this takes a few seconds so do it in a new thread
    url = URI.parse("http://ticketbooth.tunecore.com/create")
    params = {
      "s3_bucket" => "streaming.tunecore.com",
      "s3_key" => "#{album_id}/#{track_num}.mp3",
      "num_of_downloads" => downloads_allowed
    }
    req = Net::HTTP::Post.new(url.path)
    req.set_form_data(params)
    logger.info "Creating URL at TicketBooth song_id=#{id} bucket=streaming.tunecore.com key=#{album_id}/#{track_num}.mp3 downloads_allowed=#{downloads_allowed}"
    res = Net::HTTP.new(url.host, url.port).start { |http| http.request(req) }
    logger.debug "TicketBooth response: #{res.inspect} #{res.to_hash.inspect}"
    if res.code == "303" # got a redirect
      url = res.to_hash["x-download-location"][0] # custom header set by TicketBooth server
      logger.info "Got TicketBooth URL: song_id=#{id} url=#{url}"
    else
      logger.info "Got invalid response from TicketBooth server: #{res.inspect}"
      url = nil
    end
    t.join
    url
  end

  def can_stream?
    s3 = S3::AWSAuthConnection.new(AWS_ACCESS_KEY, AWS_SECRET_KEY, false)
    bucket_list = s3.list_bucket("streaming.tunecore.com", "prefix" => album_id)
    !bucket_list.entries.detect { |entry| entry.key == "#{album_id}/#{track_num}.mp3" }.nil?
  end

  # Key to use with rmtp streaming through cloudfront
  def cloudfront_streaming_key
    "mp3:#{album_id}/#{track_num}"
  end

  # used by petri to download the original asset
  def external_asset_url
    return external_asset.url if external_asset

    nil
  end

  # SKU used by music stores for reporting purposes.
  def sku
    "S" + id.to_s.rjust(10, "0")
  end

  def song_name
    name
  end

  delegate :name, to: :album, prefix: true

  delegate :id, to: :album, prefix: true

  delegate :album_type, to: :album

  def artist
    x = nil
    if album && (album.is_various? || album.is_dj_release)
      x = creatives.detect { |x| x.role == "primary_artist" }
      x = album.person if x.nil?
    else
      x = album.creatives.detect { |x| x.role == "primary_artist" }
    end

    x ? x.artist : nil
  end

  # Sets the content-disposition header on the streaming S3 asset for this song, so that when the asset is
  # downloaded it's saved with a properly formatted filename instead of 12345.mp3
  #
  # Will return immediately if the album's streaming distribution was created after 9/1/2010, because albums
  # processed after that date already have the download filename set by PETRI.
  def set_download_filename
    if album.streaming_distribution.updated_at > Date.parse("2010-09-01")
      true
    else
      S3Asset.set_download_filename("streaming.tunecore.com", "#{album.id}/#{track_num}.mp3", "#{artist.name}-#{name}.mp3")
      true
    end
  end

  # alias
  def track_number
    track_num
  end

  def has_featured_artists?
    featured_artists.size.positive? || (album && album.featured_artists.size.positive?)
  end

  def album_featured_artists
    album ? album.featured_artists : []
  end

  def featured_artists
    (super | album_featured_artists).uniq
  end

  def titleize_name
    self.name = Utilities::Titleizer.titleize_name(self) unless album.try(:allow_different_format)
    true
  end

  def uppercase_isrc
    self.optional_isrc = optional_isrc.upcase if has_optional_isrc?
    true
  end

  ###### PUBLISHING METHODS #######
  def composer
    album.person.composers.first
  end

  # FIXME: Need to change this method for my_composition
  def get_publishing_split
    PublishingSplit.where(composer_id: composer.id, composition_id: composition.id).first if composition && composer
  end

  def get_publishing_split_pct
    split = get_publishing_split
    if split.nil?
      nil
    else
      split.percent
    end
  end

  def get_composer_names
    if muma_songs.present? && muma_songs.first.composers.present?
      return muma_songs.first.composers.collect { |composer| "#{composer.composer_first_name} #{composer.composer_last_name}" }
    end
    # Fall back on composer from TC composition
    if composition && composition.publishing_splits.present?
      return composition.publishing_splits.map { |split| "#{split.composer.first_name} #{split.composer.last_name}" }
    end

    []
  end

  def get_label_copy
    muma_songs.blank? ? "" : muma_songs.first.label_copy
  end

  def has_sales?
    !sales_records.first.nil?
  end

  def sync_artwork_url
    if album.artwork && album.artwork.uploaded?
      "//#{PRODUCTION_SCALED_ARTWORK_BUCKET}#{album.artwork.url(:small).match(/\/artwork.*/)}"
    else
      nil
    end
  end

  def longer_than_90s
    if duration_in_seconds.present?
      return duration_in_seconds ? duration_in_seconds >= 90 : nil
    end

    duration ? duration >= 90_000 : nil
  end

  def as_json(options = {})
    new_attributes = {
      artist_name: artist_name,
      primary_genre: album.primary_genre ? album.primary_genre.name : "",
      secondary_genre: album.secondary_genre ? album.secondary_genre.name : "",
      album_name: album_name,
      composer_names: get_composer_names,
      label_copy: get_label_copy,
      longer_than_90s: longer_than_90s
    }

    new_attributes[:stream_url] = streaming_url if s3_asset || s3_orig_asset

    merge_attributes_as_json(new_attributes, options)
  end

  def has_splits?
    return composition.has_splits? if composition
  end

  #
  # dynamically defining setter and getter methods for distribution options
  #
  SongDistributionOption.all_options.each do |option, default_value|
    define_method(option) do
      if song_distribution_option = song_distribution_options.find_by(option_name: option)
        song_distribution_option.option_value
      else
        default_value
      end
    end
    alias_method "#{option}?", option

    define_method("#{option}=") do |update|
      song_distribution_option = song_distribution_options.find_or_create_by(option_name: option)
      if update.nil? || update == "0" || update == "false" || update == false
        song_distribution_option.option_value = false
      elsif SongDistributionOption::VALUES.include?(update)
        song_distribution_option.option_value = true
      end
      song_distribution_option.save
    end
  end

  def ready_for_distribution?
    paid? && album.ready_for_distribution?
  end

  def primary_artist_is_featured?(creative)
    album.artists.map(&:name).include?(creative.artist.name) && creative.role == "featuring"
  end

  def ytm?
    track_monetizations.where(store_id: Store::YTSR_STORE_ID).exists? ||
      track_monetization_blockers.where(store_id: Store::YTSR_STORE_ID).exists?
  end

  def primary_genre_name
    album&.primary_genre&.name || ""
  end

  def secondary_genre_name
    album&.secondary_genre&.name || ""
  end

  delegate :class, to: :album, prefix: true

  def song_file
    s3_orig_asset || s3_flac_asset || s3_asset
  end

  def eligible_for_sound_cloud_block?
    return false unless album.has_active_salepoint_for?("SCloud")

    track_monetizations.where(store_id: Store::SOUNDCLOUD_BLOCK_ID).empty?
  end

  protected

  # Generate an ISRC as a unique identifier
  # after create/update of a tunecore_isrc/optional_isrc of a song
  def update_tunecore_isrc
    if isrc_blank?
      generate_tunecore_isrc
    elsif opt_isrc_present_tc_isrc_blank?
      if optional_isrc_is_tc_and_exists? || optional_isrc_is_not_tc?
        generate_tunecore_isrc
      else
        update(tunecore_isrc: optional_isrc)
      end
    end
    true
  end

  def generate_tunecore_isrc
    generator = Tunecore::SongIsrcGenerator.new(id)
    update(tunecore_isrc: generator.isrc)
    true
  end

  def isrc_blank?
    !optional_isrc && tunecore_isrc_blank?
  end

  def tunecore_isrc_blank?
    !tunecore_isrc
  end

  def opt_isrc_present_tc_isrc_blank?
    optional_isrc && tunecore_isrc_blank?
  end

  def optional_isrc_is_tc_and_exists?
    (optional_isrc[/^TC/] && Song.exists?(tunecore_isrc: optional_isrc))
  end

  def optional_isrc_is_not_tc?
    !optional_isrc[/^TC/]
  end

  #
  #  Set the album's track numbers from 1..N
  #
  def update_album_track_numbers
    return true unless album

    songs = album.songs.order(:track_num)
    transaction do
      songs.each_with_index do |song, i|
        song.track_num = i + 1
        song.save
      end
    end
    true
  end

  def invalidate_cache
    CrtCache::Invalidator.invalidate_cache_if_changed(self)
  end

  #
  # on create, set the track number to the next in the sequence
  # should allow to be set to another place in the order after create
  #
  def set_track_number
    next_num = album ? (album.songs.count + 1) : 1
    self.track_num = next_num
    true
  end

  def is_various?
    album.is_various?
  end

  def check_for_primary_artist
    errors.add(:creatives, I18n.t("model.song.must_be_primary_artist")) if @creatives_missing
  end

  def strip_whitespace
    self.optional_isrc = optional_isrc.strip if optional_isrc.present?
  end

  def optional_isrc_validator
    conditions = ["(optional_isrc = ? or tunecore_isrc = ?) and album_id = ?", optional_isrc, optional_isrc, album_id]

    unless new_record?
      conditions.first << " and id != ?"
      conditions << id
    end
    errors.add(:optional_isrc, I18n.t("model.song.optional_isrc_error")) if Song.find_by(conditions).present?
  end

  # If a track is deleted from an Album with an associated preorder, delete instant grat tracks that can no longer be applied
  def update_preorder_grat_tracks
    salepoint_preorder_data = album.itunes_ww_salepoint.salepoint_preorder_data if album.itunes_ww_salepoint
    return unless salepoint_preorder_data.present? && !salepoint_preorder_data.validate_grat_track_size

    instant_grat_tracks = salepoint_preorder_data.preorder_instant_grat_songs.last
    instant_grat_tracks.destroy
  end

  private

  def valid_previously_released_at_date
    return unless previously_released_at && previously_released_at.year < 1900

    errors.add(:previously_released_at, I18n.t("model.song.previously_released_at_error"))
  end

  def ytm_ineligible_song?
    ytm_ineligible_song.present?
  end

  def salepoint_songs_have_ytm?
    salepoint_songs.joins(salepoint: :store).exists?(stores: { short_name: "YoutubeSR" })
  end

  def account_has_publishing_administration?
    person&.publishing_composers&.any? && person.publishing_composers.primary.all?(&:is_paid)
  end

  def update_composition_name
    publishing_composition.update(name: name) if name_changed? && publishing_composition.present?
  end

  def composition_has_songs?
    composition&.songs&.any?
  end

  def destroy_composition
    composition&.destroy
  end

  def notify_airbrake_after_optional_isrc_update
    previous_optional_isrc, new_optional_isrc = previous_changes[:optional_isrc]
    return unless previous_optional_isrc && new_optional_isrc.blank?

    msg = "Song ID #{id} updated their optional ISRC number " \
          "from #{previous_optional_isrc} to #{new_optional_isrc}"

    Airbrake.notify(msg) if Rails.env.production?
  end
end
