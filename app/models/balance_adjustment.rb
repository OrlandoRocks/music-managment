# = Description
# The Balance adjustment model allows admin users to credit/debit balances from the admin panel.
# An admin must have 'Refunds' privileges in order to use this functionality. When a balance_adjustment is created, a parent person_transaction is created, which then adjusts the person_balance model.
# Note that Balance Adjustment model currently uses decimals to 2 places for money and converts to 'cents' when sending information to the older person_transaction and person_balance model. Also, to
# correspond to the other models in the system, this model has a seperate column for debit_amount and credit_amount.
#
# A balance_adjustment can also be cancelled or 'rolled back'. This happens by creating a second balance_adjustment of the opposite amount (e.g. a $50.00 debit for a $50.00 credit in the original transaction)
# and setting the rollback column = 1. Also, the orignal transaction is updated (related_id column) to reference the rollback transaction as follows:
#
# This model has a self-referential column called 'related_id' which links rollbacks and their parents and vice-versa. Here are the states of the related_id:
#  | related_id | rollback | type of transaction                                                                  |
#  | NULL       | 0        | original transaction that hasn't been rolled back yet                                |
#  | 5          | 0        | original transaction that HAS been rolled back. Its related rollback has an id of 5  |
#  | 2          | 1        | rollback transaction whose original transaction has an id of 2                       |
#
# In the system, you cannot delete a balance_adjustment, so there will always be a record for accounting purposes.
#
# = History
# The Balance Adjustments functionality was previously only available on the command line (with credit and debit method in the Person Model, which have since been deleted).
# This functionality allows the business side of Tunecore to make the adjustments.
# = Usage
# This model is stand-alone at this point and doesn't have any methods that would be called by other models.
#
# There is overdraft protection that prevents transactions resulting in a negative balance (if you debit an amount greater than the account balance, it just debits the remaining balance)
# = Changelog
# [2009-12-28 -- DCD]
# Initial Model and functionality created
# [2010-01-05 -- DCD]
# Added Finalized Categories from Business Side

class BalanceAdjustment < ApplicationRecord
  include CurrencyHelper

  ### Relationships
  belongs_to :person
  belongs_to :posted_by, class_name: "Person"
  belongs_to :related, class_name: "BalanceAdjustment"

  has_one :person_transaction, as: :target, dependent: :nullify

  ### Callbacks
  before_validation :set_currency
  before_validation :get_info_for_a_rollback, if: :rollback?
  before_validation :clean_debit_credit, if: :original_transaction?
  after_validation :display_credit_debit_on_form, if: :original_transaction?
  after_create :create_associated_person_transaction
  after_create :update_parent_of_rollback, if: :rollback?

  #### Validations
  validates :category, :person, :posted_by, :posted_by_name, :customer_note, :admin_note, :debit_amount, :credit_amount, :currency, presence: true
  validates :disclaimer, acceptance: { message: I18n.t("models.billing.must_be_agreed") }

  # The following 4 validations protect against negative dollar amounts, both credit/debit being zero at once (which would accomplish nothing), and if both credit and debit have amounts (which shouldn't happen)
  # In the view one field (e.g. debit_amount) is shown and the other (e.g. credit_amount) is hidden and set to zero
  # The message is changed for the first validations, because they are in effect checks for negative numbers, and the third is a check for equal to zero
  validates :debit_amount,  numericality: { greater_than_or_equal_to: 0, message: I18n.t("models.billing.must_be_greater_than_zero") }
  validates :credit_amount, numericality: { greater_than_or_equal_to: 0, message: I18n.t("models.billing.must_be_greater_than_zero") }
  validate :credit_and_debit_shouldnt_both_be_zero
  validate :credit_and_debit_shouldnt_both_have_numbers
  validate :overdraft_protection_check
  validate :prevent_debit_of_negative_balance

  CATEGORY_LIST = ["Refund - Renewal", "Refund - Other", "Service Adjustment", "Songwriter Royalty", "Other", "YouTube MCN Royalty"]

  attr_accessor :credit_from_form, :debit_from_form

  scope :for_country_website,
        ->(country_website_id) {
          joins(:person)
            .where(people: { country_website_id: country_website_id })
        }

  scope :for_country_states_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          for_country_website(country_website_id)
            .joins("left join country_states on lower(people.state) = lower(country_states.name) and people.country = '#{Country::INDIA_ISO}'")
        }

  scope :for_gst_configs_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          for_country_states_by_country_website(country_website_id)
            .joins("left join gst_configs on country_states.gst_config_id = gst_configs.id")
        }

  scope :for_gst_info_by_country_website,
        ->(country_website_id = CountryWebsite::INDIA) {
          # we're calling for_gst_configs_by_country_website here because, while it's not strictly necessary to get gst_configs
          # to get gst_infos, they go hand in hand, and it is necessary to get people
          # since we need people for both this and for_gst_configs_by_country_website independently, in the interest of
          # avoiding duplicate joins we're including them in the same scope
          for_gst_configs_by_country_website(country_website_id)
            .joins("left join (select max(id) as gst_id, person_id from gst_infos group by person_id) gst on people.id = gst.person_id")
            .joins("left join gst_infos on gst.gst_id = gst_infos.id")
        }

  scope :for_refunds,
        -> {
          where(category: ["Refund - Renewal", "Refund - Other"])
        }

  scope :within_month_of_date,
        ->(date) {
          where(created_at: (date - 1.month)..date)
        }

  scope :us_taxable, -> { where(category: BalanceAdjustmentCategory::US_TAXABLE_CATEGORIES) }

  private

  def clean_debit_credit
    return unless (!credit_from_form.nil? && !debit_from_form.nil?)

    self.debit_amount = BigDecimal(debit_from_form.to_s.gsub(/(\$|,)/, ""))
    self.credit_amount = BigDecimal(credit_from_form.to_s.gsub(/(\$|,)/, ""))
  end

  def display_credit_debit_on_form
    self.debit_from_form = debit_amount
    self.credit_from_form = credit_amount
  end

  def credit_and_debit_shouldnt_both_be_zero
    return unless (!credit_amount.nil? && !debit_amount.nil?) && (credit_amount == 0.00 && debit_amount == 0.00)

    errors.add :base, I18n.t("models.billing.credit_must_be_greater_than")
  end

  def credit_and_debit_shouldnt_both_have_numbers
    return unless (!credit_amount.nil? && !debit_amount.nil?) && (credit_amount.positive? && debit_amount.positive?)

    errors.add(:base, I18n.t("models.billing.balance_adjustment.system_error_only_debit_or_credit_amount_should_be_submitted_not_both"))
  end

  def overdraft_protection_check
    return if debit_amount.nil?

    balance = BigDecimal(person.person_balance.balance.to_s)
    return unless (BigDecimal(debit_amount.to_s) > balance && balance >= 0)

    errors.add(:debit_amount, "must be less than or equal to the customer's account balance of #{balance_to_currency(balance.to_money(person.person_balance.currency), iso_code: person.country_domain)}")
  end

  def prevent_debit_of_negative_balance
    return if debit_amount.nil?

    balance = BigDecimal(person.person_balance.balance.to_s)
    return unless (balance.negative? && debit_amount.positive?)

    errors.add(:base, I18n.t("models.billing.balance_adjustment.cannot_debit_an_account_with_a_negative_balance"))
  end

  def rollback?
    rollback == 1
  end

  def original_transaction?
    rollback != 1
  end

  def get_info_for_a_rollback
    @parent = BalanceAdjustment.find(related_id)
    if !@parent.nil?
      self.debit_amount = @parent.credit_amount
      self.credit_amount = @parent.debit_amount
      self.category = @parent.category
    else
      errors.add(:base, I18n.t("models.billing.balance_adjustment.system_error_could_not_find_original_transaction"))
    end
  end

  def create_associated_person_transaction
    PersonTransaction.create(
      person_id: person_id,
      debit: debit_amount,
      credit: credit_amount,
      target: self,
      comment: customer_note
    )
  end

  def update_parent_of_rollback
    @parent = BalanceAdjustment.find(related_id)
    @parent.update_attribute(:related_id, id)
  end

  def set_currency
    self.currency = person.currency
  end
end
