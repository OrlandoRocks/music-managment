class MonthSalesReport
  def initialize(_person, _options = {})
    @totals = []
  end

  #
  #  Month Numbers
  #
  def albums_sold(month_num)
    current = @totals[month_num]
    current ? current[:albums_sold] : 0
  end

  def songs_sold(month_num)
    current = @totals[month_num]
    current ? current[:songs_sold] : 0
  end

  def songs_streamed(month_num)
    current = @totals[month_num]
    current ? current[:songs_streamed] : 0
  end

  def net_earnings(month_num)
    current = @totals[month_num]
    cents = current ? current[:net_earnings] : 0
    Money.new(cents)
  end

  #
  #  Find all records, grouped by month.  Roll-up cents for  similar conditions
  #  such as album_id, song_id, and sale_type
  #
  #  def find_records
  #    select_list = "reporting_month_id, usd_payout_cents",
  #
  #    @person_intake_records = PersonIntake.
  #      month(@options[:month]).
  #      year(@options[:year]).
  #      person(@person.id).
  #      find(:all,
  #        :group => select_list,
  #        :select => select_list)
  #  end

  #
  #  Run through each record and process the
  #  rules, adding the results to the totals
  #
  #  def setup_records
  #    @records.each do |record|
  #      add_record(record)
  #    end
  #  end

  def add_record(record)
    # check for reporting month
    return unless record.reporting_month
    return unless month_num = record.reporting_month.report_month

    setup_month_totals(month_num)
    current = @totals[month_num]
    current[:albums_sold] += record.units_sold if record.song_id.blank?
    current[:songs_sold] += record.units_sold if record.song_id && record.sale_type == "dl"
    current[:songs_streamed] += record.units_sold if record.song_id && record.sale_type != "dl"
    current[:net_earnings] += record.usd_total_cents
  end

  protected

  def setup_month_totals(month_num)
    return if @totals[month_num]

    @totals[month_num] = {
      albums_sold: 0,
      songs_sold: 0,
      songs_streamed: 0,
      net_earnings: 0
    }
  end
end
