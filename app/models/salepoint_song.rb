class SalepointSong < ApplicationRecord
  validates :salepoint, :song, presence: true
  validates     :reason_for_rejection,    presence: true, if: proc { |s| s.state == "rejected" }
  validates     :comments_for_rejection,  presence: true, if: proc { |s| s.state == "rejected" && s.reason_for_rejection == "Suspicious Assets" }
  validate      :salepoint_ids_match

  before_save   :nilify_empty_strings
  after_commit  :create_distribution_song_if_approved, on: :create

  belongs_to    :salepoint
  belongs_to    :song
  has_one       :distribution_song

  def self.admin_query(options = {})
    select_sql = "salepoint_songs.*, songs.name as song_name, people.id as account_id, people.name as account_name, albums.id as release_id, albums.name as release_name,
                  people.status account_status, COALESCE(IF(optional_isrc = '', null, optional_isrc), tunecore_isrc) as isrc, COALESCE(optional.number, tc.number) as release_upc,
                  p_genres.name as primary_genre, s_genres.name as secondary_genre, concat(p_genres.name, s_genres.name) as genre_sort, labels.name as label_name,
                  group_concat(prim_album_artists.name SEPARATOR ', ') as prim_album_artists_name, group_concat(prim_song_artists.name SEPARATOR ', ') as prim_song_artists_name,
                  concat_ws(', ', group_concat(prim_album_artists.name SEPARATOR ', '), group_concat(prim_song_artists.name SEPARATOR ', ')) as prim_artists_names,
                  group_concat(sec_album_artists.name SEPARATOR ', ') as sec_album_artists_name, group_concat(sec_song_artists.name SEPARATOR ', ') as sec_song_artists_name,
                  concat_ws(', ', group_concat(sec_album_artists.name SEPARATOR ', '), group_concat(sec_song_artists.name SEPARATOR ', ')) as sec_artists_names"

    join_terms = "inner join songs on salepoint_songs.song_id = songs.id
                  inner join albums on songs.album_id = albums.id and albums.legal_review_state in ('APPROVED', 'DO NOT REVIEW')
                  inner join people on albums.person_id = people.id
                  inner join genres p_genres on albums.primary_genre_id = p_genres.id
                  left outer join labels on albums.label_id = labels.id
                  left outer join genres s_genres on albums.secondary_genre_id = s_genres.id
                  left outer join upcs optional on optional.upcable_id = albums.id and optional.inactive = false and optional.upc_type = 'optional' and optional.upcable_type = 'Album'
                  inner join upcs tc on tc.upcable_id = albums.id and tc.inactive = false and tc.upc_type = 'tunecore' and tc.upcable_type = 'Album'
                  left outer join creatives as prim_album_creatives on prim_album_creatives.creativeable_id = albums.id
                  and prim_album_creatives.creativeable_type = 'Album' and prim_album_creatives.role = 'primary_artist'
                  left outer join creatives as prim_song_creatives on prim_song_creatives.creativeable_id = songs.id
                  and prim_song_creatives.creativeable_type = 'Song' and prim_song_creatives.role = 'primary_artist'
                  left outer join artists as prim_album_artists on prim_album_artists.id = prim_album_creatives.artist_id
                  left outer join artists as prim_song_artists on prim_song_artists.id = prim_song_creatives.artist_id
                  left outer join creatives as sec_album_creatives on sec_album_creatives.creativeable_id = albums.id
                  and sec_album_creatives.creativeable_type = 'Album' and sec_album_creatives.role != 'primary_artist'
                  left outer join creatives as sec_song_creatives on sec_song_creatives.creativeable_id = songs.id
                  and sec_song_creatives.creativeable_type = 'Song' and sec_song_creatives.role != 'primary_artist'
                  left outer join artists as sec_album_artists on sec_album_artists.id = sec_album_creatives.artist_id
                  left outer join artists as sec_song_artists on sec_song_artists.id = sec_song_creatives.artist_id
                  inner join youtube_monetizations on people.id = youtube_monetizations.person_id and
                  youtube_monetizations.effective_date <= '#{Date.tomorrow.strftime('%Y-%m-%d')}'
                  and (youtube_monetizations.termination_date is null or youtube_monetizations.termination_date >= '#{Date.tomorrow.strftime('%Y-%m-%d')}')
                  "

    state_condition = state_condition(options[:valid_state].downcase) if options[:valid_state]

    if options[:search].present?
      joins_and_conditions = joins_and_conditions(options[:search], options[:query])

      # this group by is needed for generating the primary and secondary artist name lists
      salepoint_songs = SalepointSong.select(select_sql).joins(join_terms += joins_and_conditions[:join_terms].to_s)
                                     .where(joins_and_conditions[:condition]).where(state_condition).order(options[:order])
                                     .group("salepoint_songs.id").having(joins_and_conditions[:having]).preload(song: [album: [:artwork]])
    else
      salepoint_songs = SalepointSong.select(select_sql).joins(join_terms).where(state_condition).order(options[:order])
                                     .group("salepoint_songs.id").preload(song: [album: [:artwork]])
    end

    salepoint_songs
  end

  def self.joins_and_conditions(search, query)
    case query
    when "Song Name"
      { condition: ["songs.name like ?", search.delete("%").tr("*", "%").to_s] }
    when "Account ID"
      { condition: ["people.id = ?", search] }
    when "Account Name"
      { condition: ["people.name like ?", search.delete("%").tr("*", "%").to_s] }
    when "Account Email"
      { condition: ["people.email = ?", search] }
    when "Release Name"
      { condition: ["albums.name like ?", search.delete("%").tr("*", "%").to_s] }
    when "Release UPC"
      { join_terms: "inner join ( select * from upcs where number = '#{search}' ) u on u.upcable_id = albums.id and u.upcable_type = 'Album'" }
    when "Song ISRC"
      { condition: ["songs.tunecore_isrc = ? or songs.optional_isrc = ?", search, search] }
    when "Artist Name"
      artist_search = search.tr("%*", "")
      {
        having: [
          "(prim_album_artists_name like ? or prim_song_artists_name like ? or sec_album_artists_name like ? or sec_song_artists_name like ?)",
          "%#{artist_search}%",
          "%#{artist_search}%",
          "%#{artist_search}%",
          "%#{artist_search}%"
        ]
      }
    else
      {}
    end
  end

  def self.state_condition(state)
    if ["approved", "rejected", "new"].include?(state)
      "salepoint_songs.state = '#{state}'"
    elsif state != "all"
      raise "Unexpected Salepoint Song state passed in"
    end
  end

  def self.create_by_salepoint(salepoint, album = nil)
    album ||= salepoint.salepointable

    album.songs.each do |song|
      SalepointSong.create(salepoint: salepoint, song: song)
    end
  end

  def self.update_states(salepoint_songs_hash, person = nil)
    approved_salepoint_song_ids = salepoint_songs_hash.select { |_key, value| value["state"] == "approved" }.keys
    if approved_salepoint_song_ids.present?
      SalepointSong.where(["id IN (?)", approved_salepoint_song_ids]).update_all(state: "approved", updated_at: Time.now, updated_by: person&.id, reason_for_rejection: nil, comments_for_rejection: nil)
    end

    rejected_salepoint_songs = salepoint_songs_hash.select { |_key, value| value["state"] == "rejected" }

    rejected_salepoint_songs.each do |salepoint_song_id, new_values|
      SalepointSong.find(salepoint_song_id)
                   .update(
                     new_values.merge({ updated_by: person&.id }).symbolize_keys
                   )
    end

    approved_salepoint_song_ids.each { |id| Delivery::DistributionSongWorker.perform_async(id) }

    approved_salepoint_song_ids.each do |id|
      SalepointSong.find_by(id: id)&.song&.ytm_blocked_song&.destroy
    end

    errored = false
  rescue StandardError => e
    Rails.logger.error("ERROR UPDATING SALEPOINT SONG STATE: #{e.message}")
    errored = true
  end

  def self.mass_insert(state, composition_ids)
    insertion_errors = []

    joins = "inner join songs on albums.id = songs.album_id
             inner join compositions on songs.composition_id = compositions.id
             inner join muma_songs on muma_songs.composition_id = compositions.id"

    conditions = "is_deleted = 0
                  AND albums.payment_applied = 1
                  AND albums.album_type != 'Ringtone'
                  AND compositions.state = 'accepted' AND compositions.id IN (#{composition_ids.join(',')})"

    store = Store.find_by(short_name: "YoutubeSr")
    compositions = Composition.select("compositions.*").joins({ songs: [:muma_songs, :album] }).where(conditions).includes(songs: { album: :salepoints }).order("albums.id")

    # we need to process each songs and collect on album
    compositions.collect(&:songs).flatten.group_by(&:album).each do |album, grouped_songs|
      next if !album.payment_applied || album.album_type == "Ringtone" || album.is_deleted

      grouped_composition_ids = grouped_songs.collect(&:composition_id).flatten
      youtubesr_salepoint = album.salepoints.where("store_id = 48").last

      salepoints = []
      if youtubesr_salepoint.blank?
        salepoints, store_not_added = album.add_stores([store])
        insertion_errors << "Failed to add store to album=#{album.id}" if store_not_added.present?
      else
        salepoints << youtubesr_salepoint
      end

      # FIXME: Should we limit it so that we don't duplicate the data if we accidentally run the import for the same file twice?
      # Same as distribution_song
      ss_sql = "INSERT INTO salepoint_songs (takedown_at, state, salepoint_id, song_id, created_at, updated_at)
                select null, '#{state}', salepoints.id, songs.id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP FROM songs
                inner join albums on songs.album_id = albums.id
                inner join salepoints on albums.id = salepoints.salepointable_id and salepointable_type = 'Album' and store_id = #{store.id}
                where songs.composition_id IN ('#{grouped_composition_ids.join("','")}') and albums.id = #{album.id}"

      ActiveRecord::Base.connection.execute(ss_sql)

      next unless state == "approved"

      album.petri_bundle.distributions.where(converter_class: "MusicStores::YoutubeSr::Converter").first_or_initialize do |youtube_distro|
        if youtube_distro.new_record?
          youtube_distro.salepoints = salepoints
          youtube_distro.save!
        end
      end

      ds_sql = "INSERT INTO distribution_songs (state, distribution_id, salepoint_song_id, sqs_message_id, retry_count, delivery_type, created_at, updated_at)
            SELECT 'new', distributions.id, salepoint_songs.id, NULL, 0, distributions.delivery_type, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP from songs
            inner join albums on songs.album_id = albums.id
            inner join salepoints on albums.id = salepoints.salepointable_id and salepointable_type = 'Album' and store_id = #{store.id}
            inner join salepoint_songs on songs.id = salepoint_songs.song_id
            inner join petri_bundles on albums.id = petri_bundles.album_id AND petri_bundles.state != 'dismissed'
            inner join distributions on petri_bundles.id = distributions.petri_bundle_id and converter_class = 'MusicStores::YoutubeSr::Converter'
            where songs.composition_id IN ('#{grouped_composition_ids.join("','")}') and albums.id = #{album.id}"

      ActiveRecord::Base.connection.execute(ds_sql)
    end

    insertion_errors
  end

  def send_metadata_update(params = {})
    params = {
      actor: "Admin",
      message: "Sending/retrying metadata update"
    }.merge(params)

    distribution_song.delivery_type = "metadata_only"
    distribution_song.retry(params)
  end

  def self.takedown(song)
    begin
      transaction do
        salepoint_song = false
        song.salepoint_songs.each do |ss|
          ss.update_attribute(:takedown_at, Time.now)
          salepoint_song = ss if ss.distribution_song.present?
        end

        raise "No Distribution Song for this song. ID=#{song.id}" if salepoint_song.blank?

        salepoint_song.send_metadata_update

        true
      end
    rescue
      false
    end
  end

  def self.mass_takedown(songs)
    failed_songs = []
    songs.each do |s|
      is_success = takedown(s)
      failed_songs << s.isrc unless is_success
    end

    failed_songs
  end

  def self.remove_takedown(song)
    begin
      transaction do
        salepoint_song = false

        song.salepoint_songs.each do |ss|
          ss.update_attribute(:takedown_at, nil)
          ss.salepoint.update_attribute(:takedown_at, nil)

          salepoint_song = ss if ss.distribution_song.present?
        end

        raise "No Distribution Song for this song" if salepoint_song.blank?

        salepoint_song.send_metadata_update

        true
      end
    rescue
      false
    end
  end

  def self.create_rejected_salepoints_for_album(album, reason, comments)
    you_tube_store      = Store.find_by(abbrev: "ytsr")
    you_tube_salepoint  = album.salepoints_by_store(you_tube_store).first

    album.songs.each { |song| create_rejected_salepoint_for_song(song, reason, comments, you_tube_salepoint) }
  end

  def self.create_rejected_salepoint_for_song(song, reason, comments, you_tube_salepoint = nil)
    if you_tube_salepoint.nil?
      you_tube_store      = Store.find_by(abbrev: "ytsr")
      you_tube_salepoint  = song.album.salepoints_by_store(you_tube_store).first_or_create!
    end

    salepoint_song = where(salepoint_id: you_tube_salepoint.id, song_id: song.id).first_or_initialize
    return if salepoint_song.id && salepoint_song.state == "approved"

    salepoint_song.state                  = "rejected"
    salepoint_song.reason_for_rejection   = reason
    salepoint_song.comments_for_rejection = comments
    salepoint_song.save!
  end

  private

  def create_distribution_song_if_approved
    Delivery::DistributionSongWorker.perform_async(id) if state == "approved" && !distribution_song
  end

  def nilify_empty_strings
    attributes.each do |column, _value|
      self[column].present? || self[column] = nil
    end
  end

  def salepoint_ids_match
    return if song.album_id == salepoint.salepointable_id

    errors.add(:salepoint, I18n.t("models.salepoint_song.is_not_a_salepoint_for_this_songs_album"))
  end
end
