class Product < ApplicationRecord
  include Tunecore::AdminReports::PurchaseReportDataFields
  extend CurrencyHelper

  # These are used for metaprogramming (generating methods at runtime using _safely_translated in Translatable).
  include Translatable
  attr_translate :description
  # description_safely_translated
  attr_translate :display_name
  # display_name_safely_translated
  attr_translate :flag_text
  # flag_text_safely_translated

  TCS_PRODUCT_NAMES = ["TuneCore Social Pro Monthly Subscription", "TuneCore Social Pro Annual Subscription"]
  # Album, single, ringtone distribution products
  US_ONE_YEAR_ALBUM_PRODUCT_ID      = 36
  US_TWO_YEAR_ALBUM_PRODUCT_ID      = 37
  US_FIVE_YEAR_ALBUM_PRODUCT_ID     = 38
  US_MONTHLY_ALBUM_PRODUCT_ID       = 40
  US_ONE_YEAR_SINGLE_PRODUCT_ID     = 41
  US_TWO_YEAR_SINGLE_PRODUCT_ID     = 42
  US_FIVE_YEAR_SINGLE_PRODUCT_ID    = 43
  US_ONE_YEAR_RINGTONE_PRODUCT_ID   = 103

  CA_ONE_YEAR_ALBUM_PRODUCT_ID      = 69
  CA_TWO_YEAR_ALBUM_PRODUCT_ID      = 95
  CA_FIVE_YEAR_ALBUM_PRODUCT_ID     = 96
  CA_ONE_YEAR_SINGLE_PRODUCT_ID     = 70
  CA_TWO_YEAR_SINGLE_PRODUCT_ID     = 97
  CA_FIVE_YEAR_SINGLE_PRODUCT_ID    = 98
  CA_ONE_YEAR_RINGTONE_PRODUCT_ID   = 104

  UK_ONE_YEAR_ALBUM_PRODUCT_ID      = 166
  UK_TWO_YEAR_ALBUM_PRODUCT_ID      = 167
  UK_FIVE_YEAR_ALBUM_PRODUCT_ID     = 168
  UK_ONE_YEAR_SINGLE_PRODUCT_ID     = 170
  UK_TWO_YEAR_SINGLE_PRODUCT_ID     = 171
  UK_FIVE_YEAR_SINGLE_PRODUCT_ID    = 172
  UK_ONE_YEAR_RINGTONE_PRODUCT_ID   = 175

  AU_ONE_YEAR_ALBUM_PRODUCT_ID    = 209
  AU_TWO_YEAR_ALBUM_PRODUCT_ID    = 210
  AU_FIVE_YEAR_ALBUM_PRODUCT_ID   = 211
  AU_ONE_YEAR_SINGLE_PRODUCT_ID   = 212
  AU_TWO_YEAR_SINGLE_PRODUCT_ID   = 213
  AU_FIVE_YEAR_SINGLE_PRODUCT_ID  = 214
  AU_ONE_YEAR_RINGTONE_PRODUCT_ID = 217

  DE_ONE_YEAR_ALBUM_PRODUCT_ID    = 296
  DE_TWO_YEAR_ALBUM_PRODUCT_ID    = 297
  DE_FIVE_YEAR_ALBUM_PRODUCT_ID   = 298
  DE_ONE_YEAR_SINGLE_PRODUCT_ID   = 299
  DE_TWO_YEAR_SINGLE_PRODUCT_ID   = 300
  DE_FIVE_YEAR_SINGLE_PRODUCT_ID  = 301
  DE_ONE_YEAR_RINGTONE_PRODUCT_ID = 304

  FR_ONE_YEAR_ALBUM_PRODUCT_ID    = 336
  FR_TWO_YEAR_ALBUM_PRODUCT_ID    = 337
  FR_FIVE_YEAR_ALBUM_PRODUCT_ID   = 338
  FR_ONE_YEAR_SINGLE_PRODUCT_ID   = 339
  FR_TWO_YEAR_SINGLE_PRODUCT_ID   = 340
  FR_FIVE_YEAR_SINGLE_PRODUCT_ID  = 341
  FR_ONE_YEAR_RINGTONE_PRODUCT_ID = 344

  IT_ONE_YEAR_ALBUM_PRODUCT_ID    = 375
  IT_TWO_YEAR_ALBUM_PRODUCT_ID    = 376
  IT_FIVE_YEAR_ALBUM_PRODUCT_ID   = 377
  IT_ONE_YEAR_SINGLE_PRODUCT_ID   = 378
  IT_TWO_YEAR_SINGLE_PRODUCT_ID   = 379
  IT_FIVE_YEAR_SINGLE_PRODUCT_ID  = 380
  IT_ONE_YEAR_RINGTONE_PRODUCT_ID = 383

  IN_ONE_YEAR_ALBUM_PRODUCT_ID    = 427
  IN_TWO_YEAR_ALBUM_PRODUCT_ID    = 428
  IN_FIVE_YEAR_ALBUM_PRODUCT_ID   = 429
  IN_ONE_YEAR_SINGLE_PRODUCT_ID   = 430
  IN_TWO_YEAR_SINGLE_PRODUCT_ID   = 431
  IN_FIVE_YEAR_SINGLE_PRODUCT_ID  = 432
  IN_ONE_YEAR_RINGTONE_PRODUCT_ID = 435

  US_RINGTONE_RENEWAL_PRODUCT_ID = 101
  CA_RINGTONE_RENEWAL_PRODUCT_ID = 102
  DE_RINGTONE_RENEWAL_PRODUCT_ID = 303
  FR_RINGTONE_RENEWAL_PRODUCT_ID = 343
  IT_RINGTONE_RENEWAL_PRODUCT_ID = 382
  IN_RINGTONE_RENEWAL_PRODUCT_ID = 434

  US_ONE_ALBUM_CREDIT_PRODUCT_ID    = 18
  US_ONE_SINGLE_CREDIT_PRODUCT_ID   = 22
  US_ONE_RINGTONE_CREDIT_PRODUCT_ID = 13

  US_CREDIT_PRODUCT_IDS = [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 111, 112, 113, 114, 115]
  CA_CREDIT_PRODUCT_IDS = [72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 116, 117, 118, 119, 120]
  UK_CREDIT_PRODUCT_IDS = [157, 158, 159, 160, 161, 162, 163, 164, 179, 180, 181, 182, 183]
  AU_CREDIT_PRODUCT_IDS = [201, 202, 203, 204, 205, 206, 207, 208, 221, 222, 223, 224, 225]
  DE_CREDIT_PRODUCT_IDS = [288, 289, 290, 291, 292, 293, 294, 295, 308, 309, 310, 311, 312]
  FR_CREDIT_PRODUCT_IDS = [328, 329, 330, 331, 332, 333, 334, 335, 348, 349, 350, 351, 352]
  IT_CREDIT_PRODUCT_IDS = [367, 368, 369, 370, 371, 372, 373, 374, 387, 388, 389, 390, 391]
  IN_CREDIT_PRODUCT_IDS = [419, 420, 421, 422, 423, 424, 425, 426, 439, 440, 441, 442, 443]

  CREDIT_PRODUCT_IDS = [
    US_CREDIT_PRODUCT_IDS,
    CA_CREDIT_PRODUCT_IDS,
    UK_CREDIT_PRODUCT_IDS,
    AU_CREDIT_PRODUCT_IDS,
    DE_CREDIT_PRODUCT_IDS,
    FR_CREDIT_PRODUCT_IDS,
    IT_CREDIT_PRODUCT_IDS,
    IN_CREDIT_PRODUCT_IDS
  ].flatten

  # These are the products we display on the 'Buy Distribution Credits' page
  US_ALBUM_PRODUCT_IDS    = [18, 19, 20, 21]
  US_SINGLE_PRODUCT_IDS   = [22, 23, 24, 25]
  US_RINGTONE_PRODUCT_IDS = [111, 112, 113, 114, 115]

  CA_ALBUM_PRODUCT_IDS    = [77, 78, 79, 80]
  CA_SINGLE_PRODUCT_IDS   = [81, 82, 83, 84]
  CA_RINGTONE_PRODUCT_IDS = [116, 117, 118, 119, 120]

  UK_ALBUM_PRODUCT_IDS    = [157, 158, 159, 160]
  UK_SINGLE_PRODUCT_IDS   = [161, 162, 163, 164]
  UK_RINGTONE_PRODUCT_IDS = [179, 180, 181, 182, 183]

  AU_ALBUM_PRODUCT_IDS    = [201, 202, 203, 204]
  AU_SINGLE_PRODUCT_IDS   = [205, 206, 207, 208]
  AU_RINGTONE_PRODUCT_IDS = [221, 222, 223, 224, 225]

  DE_ALBUM_PRODUCT_IDS    = [288, 289, 290, 291]
  DE_SINGLE_PRODUCT_IDS   = [292, 293, 294, 295]
  DE_RINGTONE_PRODUCT_IDS = [308, 309, 310, 311, 312]

  FR_ALBUM_PRODUCT_IDS    = [328, 329, 330, 331]
  FR_SINGLE_PRODUCT_IDS   = [332, 333, 334, 335]
  FR_RINGTONE_PRODUCT_IDS = [348, 349, 350, 351, 352]

  IT_ALBUM_PRODUCT_IDS    = [367, 368, 369, 370]
  IT_SINGLE_PRODUCT_IDS   = [371, 372, 373, 374]
  IT_RINGTONE_PRODUCT_IDS = [387, 388, 389, 390, 391]

  IN_ALBUM_PRODUCT_IDS    = [419, 420, 421, 422]
  IN_SINGLE_PRODUCT_IDS   = [423, 424, 425, 426]
  IN_RINGTONE_PRODUCT_IDS = [439, 440, 441, 442, 443]

  SONGWRITER_SERVICE_ID    = 65
  CA_SONGWRITER_SERVICE_ID = 88
  UK_SONGWRITER_SERVICE_ID = 173
  AU_SONGWRITER_SERVICE_ID = 215
  DE_SONGWRITER_SERVICE_ID = 302
  FR_SONGWRITER_SERVICE_ID = 342
  IT_SONGWRITER_SERVICE_ID = 381

  MUSIC_VIDEO_DISTRIBUTION_ID  = 11
  FEATURE_FILM_DISTRIBUTION_ID = 91

  CA_MUSIC_VIDEO_DISTRIBUTION_ID  = 90
  CA_FEATURE_FILM_DISTRIBUTION_ID = 92

  US_STORE_AUTOMATOR_ID = 105

  US_CREDIT_USAGE = 107
  CA_CREDIT_USAGE = 108
  UK_CREDIT_USAGE = 177
  AU_CREDIT_USAGE = 219
  DE_CREDIT_USAGE = 306
  FR_CREDIT_USAGE = 346
  IT_CREDIT_USAGE = 385
  IN_CREDIT_USAGE = 437

  US_BOOKLET = 109
  CA_BOOKLET = 110
  IN_BOOKLET = 438

  US_TWO_YEAR_RENEWAL_ID  = 121
  CA_TWO_YEAR_RENEWAL_ID  = 122
  US_FIVE_YEAR_RENEWAL_ID = 125
  CA_FIVE_YEAR_RENEWAL_ID = 126

  US_SOUNDOUT_PRODUCT_IDS = [133, 134, 135]
  CA_SOUNDOUT_PRODUCT_IDS = [136, 137, 138]
  UK_SOUNDOUT_PRODUCT_IDS = [188, 189, 190]
  AU_SOUNDOUT_PRODUCT_IDS = [230, 231, 232]
  DE_SOUNDOUT_PRODUCT_IDS = [317, 318, 319]
  FR_SOUNDOUT_PRODUCT_IDS = [357, 358, 359]
  IT_SOUNDOUT_PRODUCT_IDS = [396, 397, 398]
  IN_SOUNDOUT_PRODUCT_IDS = [448, 449, 450]

  US_FACEBOOK_IDS = [139, 140, 143]
  CA_FACEBOOK_IDS = [141, 142, 144]
  UK_FACEBOOK_IDS = [191, 192, 193]
  AU_FACEBOOK_IDS = [233, 234, 235]
  DE_FACEBOOK_IDS = [320, 321, 322]

  US_YTM = 145
  CA_YTM = 146
  UK_YTM = 194
  AU_YTM = 236
  DE_YTM = 323
  FR_YTM = 360
  IT_YTM = 399
  IN_YTM = 451

  US_PREORDER_ID = 147
  CA_PREORDER_ID = 148
  UK_PREORDER_ID = 195
  AU_PREORDER_ID = 237
  DE_PREORDER_ID = 324
  FR_PREORDER_ID = 361
  IT_PREORDER_ID = 400
  IN_PREORDER_ID = 452

  PREORDER_CURRENCY_MAP = {
    1 => US_PREORDER_ID,
    2 => CA_PREORDER_ID,
    3 => UK_PREORDER_ID,
    4 => AU_PREORDER_ID,
    5 => DE_PREORDER_ID,
    6 => FR_PREORDER_ID,
    7 => IT_PREORDER_ID,
    8 => IN_PREORDER_ID
  }

  US_MASTERED_TRACK = 149
  CA_MASTERED_TRACK = 150
  UK_MASTERED_TRACK = 196
  AU_MASTERED_TRACK = 238
  DE_MASTERED_TRACK = 325
  FR_MASTERED_TRACK = 362
  IT_MASTERED_TRACK = 401
  IN_MASTERED_TRACK = 453

  LIFETIME_AND_MONTHLY_DISTRIBUTION_PRODUCTS  = [39, 40, 44, 45, 49, 50, 87]
  INACTIVE_DISTRIBUTION_PRODUCT_IDS           = [6, 7, 8, 28, 29, 31, 46, 47, 48, 71, 99, 100]

  ONE_YEAR = "1_yr".freeze

  ALBUM  = "Album".freeze
  SINGLE = "Single".freeze

  FREEMIUMABLE_CLASS_NAMES = [ALBUM, SINGLE].freeze
  AD_HOC_CLASS_NAMES = FREEMIUMABLE_CLASS_NAMES

  AD_HOC           = "Ad Hoc".freeze
  RENEWAL          = "Renewal".freeze
  PACKAGE          = "Package".freeze
  EXTENSION        = "Renewal Extension".freeze
  DISTRO_EXTENSION = "Distribution Extension".freeze
  AUTOMATOR        = "automator".freeze

  ACTIVE   = "Active".freeze
  INACTIVE = "Inactive".freeze

  NONE = "None".freeze

  DISTRIBUTION_ADD_ONS = "Distribution Add-Ons".freeze

  STORE_DISPLAY_NAME = "store".freeze
  DOLBY_ATMOS_DISPLAY_NAME = "dolby_atmos_audio".freeze
  SPLITS_COLLABORATOR_DISPLAY_NAME = "splits_collaborator".freeze

  RENEWAL_TYPE_PRODUCT = "Product".freeze
  RENEWAL_TYPE_ITEM    = "Item".freeze

  # ADD STORE PRODUCTS
  US_ADD_STORE = 9
  CA_ADD_STORE = 85
  UK_ADD_STORE = 156
  AU_ADD_STORE = 200
  DE_ADD_STORE = 287
  FR_ADD_STORE = 327
  IT_ADD_STORE = 366
  IN_ADD_STORE = 418

  IN_FBM = 455

  PRODUCT_COUNTRY_MAP = {
    "US" => {
      distribution: [
        US_ONE_YEAR_ALBUM_PRODUCT_ID,
        US_TWO_YEAR_ALBUM_PRODUCT_ID,
        US_FIVE_YEAR_ALBUM_PRODUCT_ID,
        US_ONE_YEAR_SINGLE_PRODUCT_ID,
        US_TWO_YEAR_SINGLE_PRODUCT_ID,
        US_FIVE_YEAR_SINGLE_PRODUCT_ID,
        US_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [US_CREDIT_USAGE],
      album_distribution: [
        US_ONE_YEAR_ALBUM_PRODUCT_ID,
        US_TWO_YEAR_ALBUM_PRODUCT_ID,
        US_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        US_ONE_YEAR_SINGLE_PRODUCT_ID,
        US_TWO_YEAR_SINGLE_PRODUCT_ID,
        US_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [US_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        US_ALBUM_PRODUCT_IDS,
        US_SINGLE_PRODUCT_IDS,
        US_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: US_ALBUM_PRODUCT_IDS,
      single: US_SINGLE_PRODUCT_IDS,
      ringtone: US_RINGTONE_PRODUCT_IDS,
      songwriter_service: [SONGWRITER_SERVICE_ID],
      video_distribution: [
        MUSIC_VIDEO_DISTRIBUTION_ID,
        FEATURE_FILM_DISTRIBUTION_ID
      ],
      ytm: US_YTM,
      mastered_track: US_MASTERED_TRACK,
      add_store: US_ADD_STORE,
      soundout: US_SOUNDOUT_PRODUCT_IDS,
      facebook: US_FACEBOOK_IDS,
      one_album_credit: 18,
      five_album_credits: 19,
      ten_album_credits: 20,
      twenty_album_credits: 21,
      one_single_credit: 22,
      five_single_credits: 23,
      ten_single_credits: 24,
      twenty_single_credits: 25,
      one_ringtone_credit: 111,
      three_ringtone_credits: 112,
      five_ringtone_credits: 113,
      ten_ringtone_credits: 114,
      twenty_ringtone_credits: 115,
      one_year_album: 36,
      one_year_single: 41,
      album_renewal: 1
    },
    "CA" => {
      distribution: [
        CA_ONE_YEAR_ALBUM_PRODUCT_ID,
        CA_TWO_YEAR_ALBUM_PRODUCT_ID,
        CA_FIVE_YEAR_ALBUM_PRODUCT_ID,
        CA_ONE_YEAR_SINGLE_PRODUCT_ID,
        CA_TWO_YEAR_SINGLE_PRODUCT_ID,
        CA_FIVE_YEAR_SINGLE_PRODUCT_ID,
        CA_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [CA_CREDIT_USAGE],
      album_distribution: [
        CA_ONE_YEAR_ALBUM_PRODUCT_ID,
        CA_TWO_YEAR_ALBUM_PRODUCT_ID,
        CA_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        CA_ONE_YEAR_SINGLE_PRODUCT_ID,
        CA_TWO_YEAR_SINGLE_PRODUCT_ID,
        CA_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [CA_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        CA_ALBUM_PRODUCT_IDS,
        CA_SINGLE_PRODUCT_IDS,
        CA_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: CA_ALBUM_PRODUCT_IDS,
      single: CA_SINGLE_PRODUCT_IDS,
      ringtone: CA_RINGTONE_PRODUCT_IDS,
      songwriter_service: [CA_SONGWRITER_SERVICE_ID],
      video_distribution: [
        CA_MUSIC_VIDEO_DISTRIBUTION_ID,
        CA_FEATURE_FILM_DISTRIBUTION_ID
      ],
      ytm: CA_YTM,
      mastered_track: CA_MASTERED_TRACK,
      add_store: CA_ADD_STORE,
      soundout: CA_SOUNDOUT_PRODUCT_IDS,
      facebook: CA_FACEBOOK_IDS,
      one_album_credit: 77,
      five_album_credits: 78,
      ten_album_credits: 79,
      twenty_album_credits: 80,
      one_single_credit: 81,
      five_single_credits: 82,
      ten_single_credits: 83,
      twenty_single_credits: 84,
      one_ringtone_credit: 116,
      three_ringtone_credits: 117,
      five_ringtone_credits: 118,
      ten_ringtone_credits: 119,
      twenty_ringtone_credits: 120,
      one_year_album: 69,
      one_year_single: 70,
      album_renewal: 66
    },
    "UK" => {
      distribution: [
        UK_ONE_YEAR_ALBUM_PRODUCT_ID,
        UK_TWO_YEAR_ALBUM_PRODUCT_ID,
        UK_FIVE_YEAR_ALBUM_PRODUCT_ID,
        UK_ONE_YEAR_SINGLE_PRODUCT_ID,
        UK_TWO_YEAR_SINGLE_PRODUCT_ID,
        UK_FIVE_YEAR_SINGLE_PRODUCT_ID,
        UK_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [UK_CREDIT_USAGE],
      album_distribution: [
        UK_ONE_YEAR_ALBUM_PRODUCT_ID,
        UK_TWO_YEAR_ALBUM_PRODUCT_ID,
        UK_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        UK_ONE_YEAR_SINGLE_PRODUCT_ID,
        UK_TWO_YEAR_SINGLE_PRODUCT_ID,
        UK_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [UK_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        UK_ALBUM_PRODUCT_IDS,
        UK_SINGLE_PRODUCT_IDS,
        UK_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: UK_ALBUM_PRODUCT_IDS,
      single: UK_SINGLE_PRODUCT_IDS,
      ringtone: UK_RINGTONE_PRODUCT_IDS,
      songwriter_service: [UK_SONGWRITER_SERVICE_ID],
      ytm: UK_YTM,
      mastered_track: UK_MASTERED_TRACK,
      add_store: UK_ADD_STORE,
      soundout: UK_SOUNDOUT_PRODUCT_IDS,
      facebook: UK_FACEBOOK_IDS,
      one_album_credit: 157,
      five_album_credits: 158,
      ten_album_credits: 159,
      twenty_album_credits: 160,
      one_single_credit: 161,
      five_single_credits: 162,
      ten_single_credits: 163,
      twenty_single_credits: 164,
      one_ringtone_credit: 179,
      three_ringtone_credits: 180,
      five_ringtone_credits: 181,
      ten_ringtone_credits: 182,
      twenty_ringtone_credits: 183,
      one_year_album: 166,
      one_year_single: 170,
      album_renewal: 153
    },
    "AU" => {
      distribution: [
        AU_ONE_YEAR_ALBUM_PRODUCT_ID,
        AU_TWO_YEAR_ALBUM_PRODUCT_ID,
        AU_FIVE_YEAR_ALBUM_PRODUCT_ID,
        AU_ONE_YEAR_SINGLE_PRODUCT_ID,
        AU_TWO_YEAR_SINGLE_PRODUCT_ID,
        AU_FIVE_YEAR_SINGLE_PRODUCT_ID,
        AU_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [AU_CREDIT_USAGE],
      album_distribution: [
        AU_ONE_YEAR_ALBUM_PRODUCT_ID,
        AU_TWO_YEAR_ALBUM_PRODUCT_ID,
        AU_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        AU_ONE_YEAR_SINGLE_PRODUCT_ID,
        AU_TWO_YEAR_SINGLE_PRODUCT_ID,
        AU_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [AU_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        AU_ALBUM_PRODUCT_IDS,
        AU_SINGLE_PRODUCT_IDS,
        AU_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: AU_ALBUM_PRODUCT_IDS,
      single: AU_SINGLE_PRODUCT_IDS,
      ringtone: AU_RINGTONE_PRODUCT_IDS,
      songwriter_service: [AU_SONGWRITER_SERVICE_ID],
      ytm: AU_YTM,
      mastered_track: AU_MASTERED_TRACK,
      add_store: AU_ADD_STORE,
      soundout: AU_SOUNDOUT_PRODUCT_IDS,
      facebook: AU_FACEBOOK_IDS,
      one_album_credit: 201,
      five_album_credits: 202,
      ten_album_credits: 203,
      twenty_album_credits: 204,
      one_single_credit: 205,
      five_single_credits: 206,
      ten_single_credits: 207,
      twenty_single_credits: 208,
      one_ringtone_credit: 221,
      three_ringtone_credits: 222,
      five_ringtone_credits: 223,
      ten_ringtone_credits: 224,
      twenty_ringtone_credits: 225,
      one_year_album: 209,
      one_year_single: 212,
      album_renewal: 198
    },
    "DE" => {
      distribution: [
        DE_ONE_YEAR_ALBUM_PRODUCT_ID,
        DE_TWO_YEAR_ALBUM_PRODUCT_ID,
        DE_FIVE_YEAR_ALBUM_PRODUCT_ID,
        DE_ONE_YEAR_SINGLE_PRODUCT_ID,
        DE_TWO_YEAR_SINGLE_PRODUCT_ID,
        DE_FIVE_YEAR_SINGLE_PRODUCT_ID,
        DE_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [DE_CREDIT_USAGE],
      album_distribution: [
        DE_ONE_YEAR_ALBUM_PRODUCT_ID,
        DE_TWO_YEAR_ALBUM_PRODUCT_ID,
        DE_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        DE_ONE_YEAR_SINGLE_PRODUCT_ID,
        DE_TWO_YEAR_SINGLE_PRODUCT_ID,
        DE_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [DE_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        DE_ALBUM_PRODUCT_IDS,
        DE_SINGLE_PRODUCT_IDS,
        DE_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: DE_ALBUM_PRODUCT_IDS,
      single: DE_SINGLE_PRODUCT_IDS,
      ringtone: DE_RINGTONE_PRODUCT_IDS,
      songwriter_service: [DE_SONGWRITER_SERVICE_ID],
      ytm: DE_YTM,
      mastered_track: DE_MASTERED_TRACK,
      add_store: DE_ADD_STORE,
      soundout: DE_SOUNDOUT_PRODUCT_IDS,
      facebook: DE_FACEBOOK_IDS,
      one_album_credit: 288,
      five_album_credits: 289,
      ten_album_credits: 290,
      twenty_album_credits: 291,
      one_single_credit: 292,
      five_single_credits: 293,
      ten_single_credits: 294,
      twenty_single_credits: 295,
      one_ringtone_credit: 308,
      three_ringtone_credits: 309,
      five_ringtone_credits: 310,
      ten_ringtone_credits: 311,
      twenty_ringtone_credits: 312,
      one_year_album: 296,
      one_year_single: 299,
      album_renewal: 285
    },
    "FR" => {
      distribution: [
        FR_ONE_YEAR_ALBUM_PRODUCT_ID,
        FR_TWO_YEAR_ALBUM_PRODUCT_ID,
        FR_FIVE_YEAR_ALBUM_PRODUCT_ID,
        FR_ONE_YEAR_SINGLE_PRODUCT_ID,
        FR_TWO_YEAR_SINGLE_PRODUCT_ID,
        FR_FIVE_YEAR_SINGLE_PRODUCT_ID,
        FR_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [FR_CREDIT_USAGE],
      album_distribution: [
        FR_ONE_YEAR_ALBUM_PRODUCT_ID,
        FR_TWO_YEAR_ALBUM_PRODUCT_ID,
        FR_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        FR_ONE_YEAR_SINGLE_PRODUCT_ID,
        FR_TWO_YEAR_SINGLE_PRODUCT_ID,
        FR_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [FR_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        FR_ALBUM_PRODUCT_IDS,
        FR_SINGLE_PRODUCT_IDS,
        FR_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: FR_ALBUM_PRODUCT_IDS,
      single: FR_SINGLE_PRODUCT_IDS,
      ringtone: FR_RINGTONE_PRODUCT_IDS,
      songwriter_service: [FR_SONGWRITER_SERVICE_ID],
      ytm: FR_YTM,
      mastered_track: FR_MASTERED_TRACK,
      add_store: FR_ADD_STORE,
      soundout: FR_SOUNDOUT_PRODUCT_IDS,
      one_album_credit: 328,
      five_album_credits: 329,
      ten_album_credits: 330,
      twenty_album_credits: 331,
      one_single_credit: 332,
      five_single_credits: 333,
      ten_single_credits: 334,
      twenty_single_credits: 335,
      one_ringtone_credit: 348,
      three_ringtone_credits: 349,
      five_ringtone_credits: 350,
      ten_ringtone_credits: 351,
      twenty_ringtone_credits: 352,
      one_year_album: 336,
      one_year_single: 339,
      album_renewal: 366
    },
    "IT" => {
      distribution: [
        IT_ONE_YEAR_ALBUM_PRODUCT_ID,
        IT_TWO_YEAR_ALBUM_PRODUCT_ID,
        IT_FIVE_YEAR_ALBUM_PRODUCT_ID,
        IT_ONE_YEAR_SINGLE_PRODUCT_ID,
        IT_TWO_YEAR_SINGLE_PRODUCT_ID,
        IT_FIVE_YEAR_SINGLE_PRODUCT_ID,
        IT_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [IT_CREDIT_USAGE],
      album_distribution: [
        IT_ONE_YEAR_ALBUM_PRODUCT_ID,
        IT_TWO_YEAR_ALBUM_PRODUCT_ID,
        IT_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        IT_ONE_YEAR_SINGLE_PRODUCT_ID,
        IT_TWO_YEAR_SINGLE_PRODUCT_ID,
        IT_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [IT_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        IT_ALBUM_PRODUCT_IDS,
        IT_SINGLE_PRODUCT_IDS,
        IT_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: IT_ALBUM_PRODUCT_IDS,
      single: IT_SINGLE_PRODUCT_IDS,
      ringtone: IT_RINGTONE_PRODUCT_IDS,
      songwriter_service: [IT_SONGWRITER_SERVICE_ID],
      ytm: IT_YTM,
      mastered_track: IT_MASTERED_TRACK,
      add_store: IT_ADD_STORE,
      soundout: IT_SOUNDOUT_PRODUCT_IDS,
      one_album_credit: 367,
      five_album_credits: 368,
      ten_album_credits: 369,
      twenty_album_credits: 370,
      one_single_credit: 371,
      five_single_credits: 372,
      ten_single_credits: 373,
      twenty_single_credits: 374,
      one_ringtone_credit: 387,
      three_ringtone_credits: 388,
      five_ringtone_credits: 389,
      ten_ringtone_credits: 390,
      twenty_ringtone_credits: 391,
      one_year_album: 375,
      one_year_single: 378,
      album_renewal: 364
    },
    "IN" => {
      distribution: [
        IN_ONE_YEAR_ALBUM_PRODUCT_ID,
        IN_TWO_YEAR_ALBUM_PRODUCT_ID,
        IN_FIVE_YEAR_ALBUM_PRODUCT_ID,
        IN_ONE_YEAR_SINGLE_PRODUCT_ID,
        IN_TWO_YEAR_SINGLE_PRODUCT_ID,
        IN_FIVE_YEAR_SINGLE_PRODUCT_ID,
        IN_ONE_YEAR_RINGTONE_PRODUCT_ID
      ],
      credit_usage: [IN_CREDIT_USAGE],
      album_distribution: [
        IN_ONE_YEAR_ALBUM_PRODUCT_ID,
        IN_TWO_YEAR_ALBUM_PRODUCT_ID,
        IN_FIVE_YEAR_ALBUM_PRODUCT_ID
      ],
      single_distribution: [
        IN_ONE_YEAR_SINGLE_PRODUCT_ID,
        IN_TWO_YEAR_SINGLE_PRODUCT_ID,
        IN_FIVE_YEAR_SINGLE_PRODUCT_ID
      ],
      ringtone_distribution: [IN_ONE_YEAR_RINGTONE_PRODUCT_ID],
      distribution_credit: [
        IN_ALBUM_PRODUCT_IDS,
        IN_SINGLE_PRODUCT_IDS,
        IN_RINGTONE_PRODUCT_IDS
      ].flatten,
      album: IN_ALBUM_PRODUCT_IDS,
      single: IN_SINGLE_PRODUCT_IDS,
      ringtone: IN_RINGTONE_PRODUCT_IDS,
      ytm: IN_YTM,
      mastered_track: IN_MASTERED_TRACK,
      add_store: IN_ADD_STORE,
      soundout: IN_SOUNDOUT_PRODUCT_IDS,
      one_album_credit: 419,
      five_album_credits: 420,
      ten_album_credits: 421,
      twenty_album_credits: 422,
      one_single_credit: 423,
      five_single_credits: 424,
      ten_single_credits: 425,
      twenty_single_credits: 426,
      one_ringtone_credit: 439,
      three_ringtone_credits: 440,
      five_ringtone_credits: 441,
      ten_ringtone_credits: 442,
      twenty_ringtone_credits: 443,
      one_year_album: 427,
      one_year_single: 430,
      album_renewal: 454
    }
  }

  SPLITS_COLLABORATOR_PRODUCT_IDS = Product.where(display_name: SPLITS_COLLABORATOR_DISPLAY_NAME).pluck(:id).freeze
  ADDITIONAL_ARTIST_PRODUCT_IDS = Product.where(display_name: "additional_artist").pluck(:id).freeze
  PLAN_PRODUCT_IDS              = PlansProduct.all.pluck(:product_id).freeze
  PLANS_WITH_ADDITIONAL_ARTISTS_PRODUCT_IDS = PlansProduct
                                              .joins(plan: :plan_features)
                                              .where(plan: { plan_features: { feature: :buy_additional_artists } })
                                              .pluck(:product_id).freeze
  PLAN_RELATED_PRODUCT_IDS = (ADDITIONAL_ARTIST_PRODUCT_IDS + PLAN_PRODUCT_IDS).freeze

  #################
  # Associations #
  #################
  has_many    :purchases
  has_many    :product_items
  has_many    :product_item_rules, -> {
                                     order("product_item_id").where("product_item_rules.is_active = true")
                                   }, through: :product_items
  has_many    :inventories, through: :product_items
  belongs_to  :created_by, class_name: "Person"
  belongs_to  :renewal_product, class_name: "Product"
  has_many    :renewal_product_items, class_name: "ProductItem", foreign_key: "renewal_product_id"
  has_many    :targeted_products
  has_many    :product_upsells
  has_many    :purchases, as: "related"
  belongs_to  :country_website
  belongs_to  :product_tax_rate
  has_one     :soundout_product
  has_one     :subscription_product
  has_many    :reporting_dashboard_product
  has_many    :marketing_event_triggers, as: :related
  has_many    :hubspot_events, through: :marketing_event_triggers

  has_many    :invoice_logs, primary_key: "id", foreign_key: "purchase_product_id", class_name: "InvoiceLog"
  has_many    :add_on_purchases, dependent: :destroy

  has_one     :plans_product
  has_one     :plan, through: :plans_product
  ################
  # Validations #
  ################
  validates :created_by, presence: true
  validates :name, presence: true
  validates :status, presence: true
  validates :product_type, presence: true
  validates :country_website, presence: true
  validates :currency, presence: true

  default_scope { order(:sort_order) }

  #################
  # Named Scopes #
  #################
  scope :active, -> { where(status: ACTIVE) }
  scope :inactive, -> { where(status: INACTIVE) }
  scope :ad_hoc, -> { where(product_type: AD_HOC) }
  scope :by_country, ->(country) { where(country_website_id: country) }
  scope :renewals, -> { where(product_type: "Renewal") }
  scope :with_targeted_products,
        ->(targeted_offer_id:) {
          joins(:targeted_products).where(targeted_products: { targeted_offer_id: targeted_offer_id })
        }
  scope :one_yr_ad_hoc, -> { active.ad_hoc.where(display_name: "1_yr", applies_to_product: AD_HOC_CLASS_NAMES) }
  scope :paid_plan_products, -> { where(id: [PLAN_PRODUCT_IDS]).where.not(price: 0) }

  ###############
  # Attributes #
  ###############

  # original_price           - decimal representing the original price
  # adjusted_price           - decimal to use as the sale price
  # current_targeted_product - Because the relationship is a one to many for products to targeted_product,
  #                            we need to specify which is the current_targeted_product, this variable allows that specification
  attr_accessor :original_price, :adjusted_price, :current_targeted_product, :discount

  def self.get_preorder_product_by_country_website_id(country_website_id)
    product_id = PREORDER_CURRENCY_MAP[country_website_id]
    Product.find(product_id)
  end

  def is_subscription?
    item_to_renew.is_a?(Album)
  end

  # use to determine if any of the product's items are entitlements such as trend reports
  def renews?
    product_items.each do |item|
      return true if item.does_not_renew == false && item.renewal_type == "entitlement"
    end
    false
  end

  # The Product.price method takes any model in the system and tries to
  # locate a corresponding product in the products, product_items, and product_item_rules
  # tables. It works like so:
  #
  # If you submit an <b>Album/Single/Ringtone</b>, the method will look for
  # a corresponding 'Ad Hoc' product in the database.  Once found, it will then iterate
  # through the Album's supporting models and price each according to the rules defined by
  # the business.
  #
  # If you submit an array to Product.price, the method will iterate through the array
  # and attempt to price each model based on the rules outlined above, ultimately returning the
  # the total price for all models in the array.  If you pass in
  # any model not priced in the database, the price for that model will be 0.00.
  #
  # If you submit an actual Product model to Product.price, the method will return the price set at the
  # product level without any further checks.
  def self.price(person, item_to_price, product_to_price = nil)
    if item_to_price.is_a?(Array)
      price_multiple_items(person, item_to_price, product_to_price)
    else
      price_item(person, item_to_price, product_to_price)
    end
  end

  def self.list(person, item)
    offer = TargetedOffer.for_person_and_item(person, item, AD_HOC)
    build_product_list(offer, item, AD_HOC, person)
  end

  def self.list_packages_by_ids(person, type)
    product_ids = find_products_for_country(person.country_domain, type)
    products = active.by_country(person.country_website_id).where("id IN (?) AND product_type = 'Package'", product_ids)

    products.each do |product|
      product.set_targeted_product(TargetedProduct.targeted_product_for_active_offer(person, product))
      product.set_adjusted_price(product.calculate_price(product))
    end
    products
  end

  def one_yr_ad_hoc?
    # Check raw products.display_name, not a translation.
    # Product is not pure in the case of an instantiated purchase.product
    product_type == AD_HOC && Product.find(id)[:display_name] == ONE_YEAR
  end

  def is_a_credit_product?
    CREDIT_PRODUCT_IDS.include?(id)
  end

  def is_ytm_product?
    [US_YTM, CA_YTM, UK_YTM, AU_YTM, DE_YTM, FR_YTM, IT_YTM, IN_YTM].include?(id)
  end

  def is_pub_admin_product?
    Product.publishing_admin_product_ids.include?(id)
  end

  def self.distribution_product_ids
    active_distribution_products +
      INACTIVE_DISTRIBUTION_PRODUCT_IDS +
      LIFETIME_AND_MONTHLY_DISTRIBUTION_PRODUCTS +
      CREDIT_PRODUCT_IDS
  end

  def self.publishing_admin_product_ids
    [
      Product::SONGWRITER_SERVICE_ID,
      Product::CA_SONGWRITER_SERVICE_ID,
      Product::UK_SONGWRITER_SERVICE_ID,
      Product::AU_SONGWRITER_SERVICE_ID,
      Product::DE_SONGWRITER_SERVICE_ID,
      Product::FR_SONGWRITER_SERVICE_ID,
      Product::IT_SONGWRITER_SERVICE_ID
    ]
  end

  def self.active_distribution_products
    active_countries = Product::PRODUCT_COUNTRY_MAP.collect { |country, _value| country }
    active_countries.collect { |c| Product::PRODUCT_COUNTRY_MAP[c][:distribution] }.flatten
  end

  # Given a person and a type (Album, Single or Ringtone), we look up the products
  # associated with that peron's country and return the ids of products for that country
  def self.find_products_for_country(country_domain, type)
    PRODUCT_COUNTRY_MAP[country_domain][type]
  end

  def self.extension_list(person, item, is_distribution_credit_extension = false)
    # is_distribution_credit_extension will no longer ever be true
    # TODO: remove conditional branches when is_distribution_credit_extension is true
    product_type = extension_product_type(is_distribution_credit_extension)
    offer = TargetedOffer.for_person_and_item(person, item, product_type)
    build_product_list(offer, item, product_type, person)
  end

  def self.default_products_for(item, country_website_id, product_type = AD_HOC)
    applies_to = item.is_a?(Album) ? item.album_type : item.class.name

    params = {
      product_type: product_type,
      applies_to_product: applies_to,
      country_website_id: country_website_id
    }
    Product.active.where(
      "product_type = :product_type AND applies_to_product = :applies_to_product AND is_default = 1 AND status='Active' AND country_website_id = :country_website_id", params
    )
  end

  def self.automator_product(person)
    price = person.eligible_for_free_automator_product? ? "price = 0" : "price != 0"

    where(display_name: AUTOMATOR, country_website: person.country_website)
      .find_by(price)
  end

  def is_free_automator_product?
    Product.find(id)[:display_name] == AUTOMATOR && price.zero?
  end

  # Creates the purchase record with the associated product and related item
  # This method take the place of the deprecated Product.purchase method, and allows
  # the product to be submitted with the product so a price can be further specified
  #
  # If a product is not submitted, a matching product is chosen from the products table (see find_ad_hoc_product)
  def self.add_to_cart(person, item_to_purchase, product_to_purchase = nil)
    # Allow logic for item to purchase to change behind the scenes.  If change occurs we reset the product
    if item_to_purchase.respond_to? :add_to_cart_pre_proc and item_to_purchase.add_to_cart_pre_proc
      product_to_purchase = nil
      item_to_purchase    = item_to_purchase.class.base_class.name.constantize.find(item_to_purchase.id)
    end

    # adds a product and the related item to a customer's cart
    if item_to_purchase.is_a?(Array)
      item_to_purchase.map do |item|
        create_purchase(person, item, product_to_purchase)
      end
    else
      create_purchase(person, item_to_purchase, product_to_purchase)
    end
  end

  # Changes the purchase record's product_id and recalculates the price
  def self.change_product_for_purchase(purchase, new_product)
    create_purchase(purchase.person, purchase.related, new_product)
  end

  # Performs the actions necessary once an item has been paid for.
  # person => the person these purchases apply to.
  # purchases => either a single purchase or an array of purchases
  #
  # When you submit an array of purchases (as happens when an invoice is settled!)
  # the method iterates through the array and calls Inventory.use! on a collection of
  # items instead of a single item. This allows the collected distribution of
  # all purchases attached to an invoice and takes care of dual delivery problems
  # for iTunes stores and delegates any distribution responsibility up to the album.
  def self.process_purchase(person, purchases)
    Plans::InventoryShimService.call(person, purchases)

    if purchases.respond_to?(:each)
      process_multiple_purchases(person, purchases)
    else
      process_single_purchase(person, purchases)
    end

    # Once the purchase is complete, send a sns notification to tc-distributor
    # if the purchase type is Salepoint(new store addition) or
    # SalepointSubscription (store automator added)
    send_sns_notification_for_purchase(person, purchases)
  end

  def self.send_sns_notification_for_purchase(person, purchases)
    return unless FeatureFlipper.show_feature?(:use_sns, person)

    album_store_id_hash = Hash.new { |h, k| h[k] = [] }
    Array(purchases).select(&:need_sns_notification?).each do |purchase|
      album_store_id_hash[purchase.related_album_id] << purchase.related_store_id
    end

    # Remove nil store_ids
    album_store_id_hash = album_store_id_hash.each { |_, value| value.compact! }

    Salepoint.send_sns_notification(album_store_id_hash) if album_store_id_hash.present?
  end

  def self.find_active_product(product_name)
    params = { name: product_name }
    active.where("name = :name", params).first
  end

  # returns a list of products related to purchases passed in.
  def self.related_products_for_purchases(person, purchases, number_of_related_products_to_show = 2)
    RelatedProduct.related_for_purchases(person, purchases, number_of_related_products_to_show)
  end

  def self.localized_one_year_release_product_id_by_type(person, release_type)
    country = person.country_website.country

    PRODUCT_COUNTRY_MAP[country]["#{release_type.downcase}_distribution".to_sym].first
  end

  ##################################################################################################
  # Instance Methods
  ##################################################################################################

  # product.display returns the product with an adjusted price
  # if applicable.
  def display(_person)
    set_adjusted_price(calculate_price(self))
    self
  end

  def set_targeted_product(targeted_product)
    self.current_targeted_product = targeted_product
    return if targeted_product.nil?

    # override the display_name and flag text if set in the current_targeted_product
    self.display_name = current_targeted_product.display_name if current_targeted_product.display_name.present?

    self.flag_text = current_targeted_product.flag_text if current_targeted_product.flag_text.present?
  end

  def base_price
    if price.nil? || price == 0.00
      base_rule = product_item_rules.where(parent_id: nil).first
      if base_rule.nil?
        0.00
      else
        base_rule.price
      end
    else
      price
    end
  end

  # Returns a list of products that are related in the RelatedProduct model
  def related_products(person, number_of_related_products_to_show = 2)
    RelatedProduct.list_of_related(person, [self], number_of_related_products_to_show)
  end

  def calculate_price(model_to_price)
    if price.nil? || price == 0.00
      product_items.inject(0.00) do |sum, item|
        sum += item.total_price(model_to_price)
      end
    else
      price
    end
  end

  def process_item_rules(person, purchase)
    if is_product_renewal?
      # create a top-level renewal for all entitlements attached to this product
      # Inventory items do not get renewals at the time of purchase, they get them at
      # the time of usage, which is processed in Inventory.use!
      renewal = Renewal.create!(
        person: person,
        purchase: purchase,
        item_to_renew: self
      )

      ProductItemRule.process_product_rules!(
        person: person,
        product: self,
        purchase: purchase,
        renewal: renewal
      )
    else

      ProductItemRule.process_product_rules!(
        person: person,
        product: self,
        purchase: purchase
      )
    end
  end

  def is_item_renewal?
    renewal_level == RENEWAL_TYPE_ITEM
  end

  def is_product_renewal?
    renewal_level == RENEWAL_TYPE_PRODUCT
  end

  def is_plan?
    PLAN_PRODUCT_IDS.include?(id)
  end

  def paid_plan?
    Product.paid_plan_products.pluck(:id).include?(id)
  end

  def additional_artist?
    ADDITIONAL_ARTIST_PRODUCT_IDS.include?(id)
  end

  def splits_collaborator?
    SPLITS_COLLABORATOR_PRODUCT_IDS.include?(id)
  end

  def plan_related?
    PLAN_RELATED_PRODUCT_IDS.include?(id)
  end

  ###############################################
  # Replacement code for Tunecore::Purchaseable #
  # refactored to use just one price policy when Purchaseable was removed, price policy can also die with a refactor of price calculator
  ###############################################
  def price_policy
    PricePolicy.find_by(short_code: "tunecore_default")
  end
  ##############################################
  # End Replacement of Tunecore::Purchaseable  #
  ##############################################

  def price_as_money(adhoc_model = nil)
    (adjusted_price || calculate_price(adhoc_model || self)).to_money(currency)
  end

  def price_for_site(country_site = nil)
    country_site ||= country_website
    Product.money_to_currency(
      price_as_money, {
        iso_code: country_site.country_iso,
        currency: country_site.user_currency,
        round_value: country_site.india?,
      }
    )
  end

  def set_adjusted_price(original_price)
    self.original_price = original_price

    self.adjusted_price =
      if current_targeted_product.nil?
        self.original_price
      else
        current_targeted_product.adjust_price(original_price)
      end
  end

  def adjusted_price?
    (adjusted_price != original_price)
  end

  def set_current_targeted_product(targeted_product = nil)
    self.current_targeted_product = targeted_product
  end

  def self.set_targeted_product_and_price(person, product)
    product.set_targeted_product(TargetedProduct.targeted_product_for_active_offer(person, product))
    product.set_adjusted_price(product.calculate_price(product))
  end

  def active?
    status == ACTIVE
  end

  def tcs_subscription?
    TCS_PRODUCT_NAMES.include?(name)
  end

  delegate :country, to: :country_website

  def india_product?
    country == "IN"
  end

  # Gather up product and items for Inventory.use!
  # and submit the group to Inventory.use!
  def self.process_multiple_purchases(person, purchases)
    items_to_use = []

    purchases.each do |purchase|
      items_to_use << purchase.item_to_use
      process_single_purchase(person, purchase, false)
    end

    # could be some nil values
    items_to_use.compact!

    Inventory.use!(person, { items_to_use: items_to_use }) if items_to_use.present?
  end

  def self.process_single_purchase(person, purchase, use_immediately = true)
    return unless purchase.paid?

    transaction do
      # process by product type
      product = purchase.product
      case product.product_type
      when AD_HOC
        product.process_item_rules(person, purchase)
        process_ad_hoc_purchase(person, purchase, product) if use_immediately
      when RENEWAL
        process_renewal_purchase(purchase)
      when EXTENSION, DISTRO_EXTENSION
        process_extension_purchase(purchase)
      else
        product.process_item_rules(person, purchase)
      end

      # Give purchase a chance to do specific handling
      purchase.related.paid_post_proc if purchase.related.respond_to? :paid_post_proc
    end
  end

  #
  #  Handle a collection of items to price
  #
  def self.price_multiple_items(person, items_to_price, product = nil)
    items_to_price.inject(0.to_money(person.person_balance.currency)) do |sum, item|
      sum += price_item(person, item, product)
    end
  end

  #
  #  Price A Single Item
  #
  def self.price_item(person, item_to_price, product = nil)
    if item_to_price.is_a?(Product)
      item_to_price.price_as_money
    else
      # if the item_to_price is not a Product model, it's an ad hoc product
      price_ad_hoc_item(person, item_to_price, product)
    end
  end

  #
  #  Find and get price for an ad hoc item
  #
  def self.price_ad_hoc_item(person, item_to_price, product)
    product_to_price = product || find_ad_hoc_product(item_to_price, person)
    return 0.00.to_money unless product_to_price

    product_to_price.price_as_money(item_to_price)
  end

  def self.find_ad_hoc_product(item, person)
    params = {
      product_type: AD_HOC,
      inventory_type: item.class.try(:inventory_alias) || class_or_superclass(item),
      country_website_id: person.country_website_id
    }

    return automator_product(person) if params[:inventory_type] == "SalepointSubscription"

    Product
      .active
      .select("products.*")
      .joins(:product_items, :product_item_rules)
      .where(
        "products.product_type = :product_type
        AND products.country_website_id = :country_website_id
        AND product_item_rules.inventory_type = :inventory_type
        AND (product_item_rules.parent_id IS NULL OR product_item_rules.parent_id = 0)
        AND product_item_rules.is_active = 1
        AND is_default=1",
        params
      )
      .order("sort_order")
      .first
  end

  def self.class_or_superclass(item)
    item.instance_of?(PlanCreditUsage) ? item.class.superclass.name : item.class.name
  end

  def self.free_release?(item_to_purchase, current_user = nil)
    FREEMIUMABLE_CLASS_NAMES.include?(item_to_purchase.class.name) &&
      item_to_purchase.strictly_discovery_release?(current_user)
  end

  def self.create_purchase(person, item_to_purchase, product_to_purchase = nil)
    unless purchase_person_matches_related_person?(person, item_to_purchase) &&
           item_to_purchase_matches_product_to_purchase?(item_to_purchase, product_to_purchase)

      return
    end

    # attempt to use credit for purchase item
    usage = CreditUsage.for(person, item_to_purchase)
    if usage
      product_to_purchase = nil
      item_to_purchase = usage

      # remove related item from cart if exists.
      Purchase.destroy_by_purchase_item(person, usage.related)
    end

    if product_to_purchase.nil? && !item_to_purchase.is_a?(Product)
      create_purchase_for_unknown_product(person, item_to_purchase)
    elsif product_to_purchase.nil? && item_to_purchase.is_a?(Product)
      create_product_purchase(person, item_to_purchase, item_to_purchase)
    else
      create_product_purchase(person, product_to_purchase, item_to_purchase)
    end
  end

  def self.create_purchase_for_unknown_product(person, item_to_purchase)
    ad_hoc_product = find_ad_hoc_product(item_to_purchase, person)
    return unless ad_hoc_product

    related_item = item_to_purchase
    Purchase.recalculate_or_create(person, ad_hoc_product, related_item)
  end

  def self.create_product_purchase(person, product, related)
    related_item =
      if related.nil?
        product
      else
        related
      end
    Purchase.recalculate_or_create(person, product, related_item)
  end

  def self.process_ad_hoc_purchase(person, purchase, _product)
    Inventory.use!(person, purchase.item_to_use)
  end

  def self.process_renewal_purchase(purchase)
    renewal = purchase.related
    renewal.renew!(purchase) if renewal
  end

  def self.process_extension_purchase(purchase)
    renewal = purchase.related
    renewal.extend!(purchase) if renewal
  end

  def self.purchase_person_matches_related_person?(person, related_item)
    item_person = person_for_related_item(related_item)
    if item_person.nil? || item_person == person
      true
    else
      false
    end
  end

  def self.person_for_related_item(item)
    return unless item

    if item.is_a?(Salepoint)
      item.salepointable.person
    elsif item.is_a?(SalepointPreorderData)
      item.salepoint.salepointable.person
    elsif item.is_a?(PreorderPurchase)
      item.album.person
    elsif !item.is_a?(Product)
      item.person
    end
  end

  def self.item_to_purchase_matches_product_to_purchase?(item, product)
    !product || !item.is_a?(Album) || product.applies_to_product == item.class.name
  end

  def self.adjust_price_for_product_list(products, item)
    products.each do |product|
      product.set_adjusted_price(product.calculate_price(item))
    end
    products
  end

  def self.extension_product_type(is_distribution_credit_extension)
    (is_distribution_credit_extension == true) ? DISTRO_EXTENSION : EXTENSION
  end

  def self.build_product_list(offer, item, product_type, person)
    if offer.nil?
      products = default_products_for(item, person.country_website_id, product_type)
      adjust_price_for_product_list(products, item)
    else
      offer.product_list(item, person.country_website_id, product_type)
    end
  end

  # this is a stub method solely for Plans
  def renew!(_purchase)
    true
  end
end
