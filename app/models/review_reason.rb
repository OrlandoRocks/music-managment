# frozen_string_literal: true

class ReviewReason < ApplicationRecord
  has_and_belongs_to_many :review_audits, join_table: :review_audits_review_reasons
  has_and_belongs_to_many :roles

  # reason_type and review_type are enum columns
  ACCEPTABLE_REVIEW_TYPES         = ["LEGAL", "STYLE"]
  ACCEPTABLE_REASON_TYPES         = ["FLAGGED", "REJECTED", "NOT SURE"]

  CHANGES_REQUESTED = "changes_requested"
  RIGHTS_VERIFICATION = "rights_verification"
  WILL_NOT_DISTRIBUTE = "will_not_distribute"

  validates :reason_type, :reason, :review_type, presence: true
  validates :reason_type, inclusion: { in: ACCEPTABLE_REASON_TYPES }
  validates :review_type, inclusion: { in: ACCEPTABLE_REVIEW_TYPES }
  validates :should_unfinalize, inclusion: { in: [true, false] }
  validates :template_type, {
    inclusion: { in: %w[changes_requested rights_verification will_not_distribute] },
    allow_nil: true
  }

  def email_template_type
    return "" if template_type.nil?

    template_type == WILL_NOT_DISTRIBUTE ? email_template_path : template_type
  end
end
