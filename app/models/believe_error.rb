class BelieveError < ApplicationRecord
  belongs_to :album

  serialize :request_args, QuirkyJson
  serialize :response_errors, QuirkyJson
end
