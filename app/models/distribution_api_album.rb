class DistributionApiAlbum < ApplicationRecord
  belongs_to :distribution_api_service
  belongs_to :album
  has_many :distribution_api_songs, dependent: :delete_all
end
