class Faq
  def self.faq_json(type)
    {
      youtube_money: "http://s3.amazonaws.com/tunecore.site.json/ytmfaq_v2.json",
      youtube_money_pubadmin: "http://s3.amazonaws.com/tunecore.site.json/publishing_youtube_faq_v2.json"
    }[type]
  end

  def self.get_faqs(type)
    faqs = ActiveSupport::JSON.decode(open(faq_json(type)).read)
    return faqs if I18n.locale.to_s == "en"

    I18n.backend.send(:init_translations) unless I18n.backend.initialized?

    terms_to_translate = I18n.backend.send(:translations)[I18n.locale]
    return faqs if terms_to_translate.empty?

    en_terms = I18n.backend.send(:translations)[:en]
    faqs.each do |faq|
      terms_to_translate.keys.each do |term|
        next if terms_to_translate[term].class != String

        faq["question"].gsub!(en_terms[term], terms_to_translate[term])
        faq["question"].gsub!(en_terms[term].capitalize, terms_to_translate[term].capitalize)
        faq["answer"].gsub!(en_terms[term], terms_to_translate[term])
        faq["answer"].gsub!(en_terms[term].capitalize, terms_to_translate[term].capitalize)
      end
    end

    faqs
  end
end
