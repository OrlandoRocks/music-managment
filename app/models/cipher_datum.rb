class CipherDatum < ActiveRecord::Base
  belongs_to :cipherable, polymorphic: true

  # this method should always be called on create, and the relation should save at the end of the transaction
  # that calls this method
  def self.encrypt(secrets, relation)
    encryption_artifact = Cipherer.new(strategy: cipherer_strategy).encrypt(secrets)
    cipher_datum = create!(
      key: encryption_artifact.key,
      iv: encryption_artifact.iv,
      tag: encryption_artifact.auth_tag,
      secrets: encryption_artifact.encrypted_string
    )
    relation.cipher_datum = cipher_datum

    encryption_artifact.encrypted_string
  end

  # TODO figure out a way to get rid of the send to a private class method
  def decrypt
    Cipherer.new(
      strategy: self.class.send(
        :cipherer_strategy,
        {
          auth_data: Cipherer::AUTH_DATA,
          key: key,
          iv: iv,
          auth_tag: tag
        }
      )
    ).decrypt(secrets)
  end

  def self.cipherer_strategy(options = {})
    merged_options = { auth_data: Cipherer::AUTH_DATA }.merge(options)
    Crypto::Strategies::Aes256GcmStrategy.new(**merged_options)
  end

  private_class_method :cipherer_strategy
end
