class PublishingSplit < ApplicationRecord
  has_paper_trail

  belongs_to :composer
  belongs_to :writer, polymorphic: true
  belongs_to :composition
  belongs_to :cowriter, foreign_key: :writer_id
  validates :percent, :composer, presence: true
  validate :total_splits_do_not_exceed_100_percent
  before_save :set_updated_by
  after_save  :update_composition_status

  scope :for_composer, ->(composer) { where(composer_id: composer.id) }

  # batch update splits based on muma tables
  def self.batch_update_splits
    realtime =
      Benchmark.realtime do
        # "select ms.code 'CP Code', ms.composition_id, ms.title 'CP Song Title', comp.name 'TC Comp Name', ips.composer_first_name 'CP Composer First Name', ips.composer_middle_name 'CP Composer Middle Name', ips.composer_last_name 'CP Composer Last Name',
        # composers.first_name 'TC Composer First Name', composers.middle_name 'TC Composer Middle Name', composers.last_name 'TC Composer Last Name',
        # ips.controlled, ips.capacity_code, ips.perf_collect_share, ips.perf_owned_share, (ips.perf_collect_share*2) 'CP Split', ps.percent 'TC Split',
        # ms.ip_updated_at 'CP IP Updated At', ps.created_at 'TC Split Created At',ps.updated_at 'TC Split Updated At'
        self.class.connection.execute(
          "update publishing_splits ps, muma_song_ips ips,
        muma_songs ms, compositions comp, composers
        set ps.percent = (ips.perf_collect_share*2),
        ps.updated_at = CURRENT_TIMESTAMP,
        ps.updated_by = 'Batch Update'
        where ps.composer_id = composers.id
        and ps.composition_id = comp.id
        and comp.id = ms.composition_id
        and ips.song_code = ms.code
        and ips.controlled = 1
        and ips.ip_c_or_p = 'C'
        and ips.composer_last_name = composers.last_name
        and ips.composer_first_name = composers.first_name
        and (ips.perf_collect_share*2) != ps.percent"
        )
      end

    puts logger.info "PublishingSplits.batch_update_splits realtime = #{realtime}"
    # update compositions in cases where splits are changed to 0 on music maestro side
    Composition.batch_update_not_controlled
  end

  # Splits that belong to the same composition.  Does not include this split.
  def related_splits
    composition.publishing_splits - [self]
  end

  def total_pct
    related_splits.sum(&:percent) + percent
  end

  def self.admin_query(options = {})
    select_sql = "publishing_splits.*, compositions.name as composition_name, compositions.id as comp_id, compositions.state as
                  composition_state, people.id as account_id, people.name as account_name, albums.id as release_id,
                  COALESCE(optional.number, tc.number) as release_upc"

    join_terms = "inner join compositions on publishing_splits.composition_id = compositions.id inner join songs on compositions.id = songs.composition_id inner join albums on songs.album_id = albums.id
                  inner join people on albums.person_id = people.id left outer join upcs optional on optional.upcable_id = albums.id and optional.inactive = false and optional.upc_type = 'optional' and optional.upcable_type = 'Album'
                  inner join upcs tc on tc.upcable_id = albums.id and tc.inactive = false and tc.upc_type = 'tunecore' and tc.upcable_type = 'Album' "

    case options[:query]
    when "Composition ID"
      condition = ["compositions.id = ?", options[:search]]
    when "Composition Name"
      join_terms += "inner join ( select * from compositions
                     where name = '#{options[:search].delete('%').sub('*', '%')}' ) c on publishing_splits.composition_id = c.id"
      # condition = ["compositions.name like ?", "#{options[:search].gsub("%", "").sub("*", "%")}"]
    when "Account ID"
      condition = ["people.id = ?", options[:search]]
    when "Account Name"
      condition = ["people.name like ?", options[:search].delete("%").sub("*", "%").to_s]
    when "Account Email"
      condition = ["people.email = ?", options[:search]]
    when "Release ID"
      condition = ["albums.id = ?", options[:search]]
    when "Release Name"
      condition = ["albums.name like ?", options[:search].delete("%").sub("*", "%").to_s]
    when "Release UPC"
      # condition = ['optional.number = ? or tc.number = ?', options[:search], options[:search]]
      join_terms += "inner join ( select * from upcs where number = '#{options[:search]}' ) u on u.upcable_id = albums.id and u.upcable_type = 'Album'"
    when "Song ISRC"
      condition = ["songs.tunecore_isrc = ? or songs.optional_isrc = ?", options[:search], options[:search]]
    end

    splits = PublishingSplit.select(select_sql).joins(join_terms).where(condition).order(options[:order])

    options[:per_page] = 9_999_999_999 if options[:per_page] == "All"

    splits.paginate(page: options[:page], per_page: options[:per_page])
  end

  def submit_split(split_pct)
    self.percent = split_pct.to_d
    save
  end

  def for_composer?
    writer_type == "Composer"
  end

  def for_unknown_cowriter?
    writer_type == "Cowriter" && writer.is_unknown
  end

  private

  def update_composition_status
    if total_pct.zero?
      composition.set_not_controlled!
    else
      composition.submit_split!
    end
  end

  def total_splits_do_not_exceed_100_percent
    errors.add(:percent, "split cannot exceed 100%") if total_pct > 100.to_d
  end

  def set_updated_by
    # piggy back on Papertrail's gem ability to set current_user
    self.updated_by = PaperTrail.request.whodunnit
  end
end
