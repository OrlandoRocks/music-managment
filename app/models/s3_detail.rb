# frozen_string_literal: true

require "httparty"

class S3Detail < ApplicationRecord
  belongs_to :s3_asset
  serialize :metadata_json, JSON

  CODEC_TYPE_AUDIO = "audio"

  NORMAL_BITRATE = 16
  NORMAL_SAMPLE_RATE = 44_100

  FLAC_TYPE = "flac"
  MP3_TYPE  = "mp3"
  WAV_TYPE  = "wav"
  FILE_TYPE_ORDER = {
    "aiff" => 1,
    "wav" => 1,
    "flac" => 2,
    "mp3" => 3,
    "m4a" => 4,
    "amrnb" => 5,
    "ogg" => 6
  }

  scope :file_types_for_songs, ->(songs) {
    where(s3_asset: songs.pluck(:s3_asset_id)).pluck(:file_type)
  }
end
