class TaxInfo < ApplicationRecord
  self.table_name = "tax_info"
  has_paper_trail

  belongs_to :composer
  belongs_to :publishing_composer

  has_one :paper_agreement, as: :related

  # Strip everything but digits, so the user can specify "555 234 34" or
  # "5552-3434" or both will mean "55523434"
  before_validation :clean_tax_id
  before_validation :check_and_set_is_entity
  before_validation :check_w8ben_name

  validates :country, :address_1, :zip, presence: true
  validates :state,           presence: { if: :is_us? }
  validates :owner_type,      presence: { if: :is_w8ben? }
  validates :country_of_corp, presence: { if: :is_w8ben? }

  validates :address_1,  length: { maximum: 45 }
  validates :address_2,  length: { maximum: 45, allow_nil: true }
  validates :city,       length: { maximum: 45 }
  validates :state,      length: { maximum: 2, if: :is_us? }
  validates :country,    length: { maximum: 2 }
  validates :zip,        length: { maximum: 10 }
  validates :tax_id,     length: { within: 9..9, if: :has_social? }

  validate :validates_agreed_to_w9
  validate :validates_agreed_to_w8ben
  validate :validates_presence_of_q9_a_resident
  validate :validates_tax_id
  validate :validates_sole, unless: :is_w8ben?
  validate :validates_corp, unless: :is_w8ben?

  attr_accessor :agreed_to_w9
  attr_accessor :agreed_to_w8ben
  attr_accessor :is_w8ben

  serialize :claim_of_treaty_benefits

  def self.reset_attributes
    keys = ["created_at", "composer_id"]
    new.attributes.reject { |k, _v| keys.include?(k) }
  end

  # rubocop:disable Style/NumericPredicate
  def has_social?
    (nosocial == 0 || !nosocial) and !is_w8ben? and !is_w8ben?
  end
  # rubocop:enable Style/NumericPredicate

  def is_us?
    country == "US"
  end

  def is_w8ben?
    agreed_to_w8ben
  end

  def understandable_error_messages
    # Overriding errors method so attributes name will make sense for customers
    list_of_messages = []
    errors.each do |attr, msg|
      list_of_messages << case attr.to_s
                          when :address_1
                            "Address #{msg}"
                          when :zip
                            "Zip/Postal code"
                          when :tax_id
                            if is_entity?
                              "U.S. Tax ID of Entity #{msg}"
                            else
                              "Social security number #{msg}"
                            end
                          when :agreed_to_w9
                            msg.to_s
                          when :agreed_to_w8ben
                            msg.to_s
                          when :claim_of_treaty_benefits
                            msg.to_s
                          when :sole
                            msg.to_s
                          when :corp
                            msg.to_s
                          else
                            "#{attr.to_s.humanize} #{msg}"
                          end
    end
    list_of_messages
  end

  def completed_w9?
    submitted_tax_id? && agreed_to_w9_at
  end

  def completed_w8ben?
    agreed_to_w8ben_at
  end

  def tax_id=(tax_id)
    self.encrypted_tax_id = encrypt_and_store_salt(tax_id)
  end

  def tax_id
    decrypted_tax_id
  end

  def entity_tax_id=(tax_id)
    self.tax_id = tax_id if is_entity?
  end

  def entity_tax_id
    tax_id if is_entity?
  end

  def entity_nosocial=(nosocial)
    self.nosocial = nosocial if is_entity?
  end

  def entity_nosocial
    return nosocial if is_entity?
  end

  def decrypted_tax_id
    return if encrypted_tax_id.blank?

    decrypt(encrypted_tax_id)
  end

  def submitted_tax_id?
    (self[:tax_id] || encrypted_tax_id) ? true : false
  end

  def is_entity?
    is_entity
  end

  def mailing_address
    if completed_w8ben? && mailing_address_1.present?
      address = mailing_address_1.to_s
      address << "\n#{mailing_address_2}" if mailing_address_2.present?
      address << "\n#{mailing_city}" if mailing_city.present?
      address << ", #{mailing_state}" if mailing_state.present?
      address << " #{mailing_zip}" if mailing_zip.present?
      address << "\n#{Country.country_name(mailing_country)}" if mailing_country.present?
    else
      address = address_1.to_s
      address << "\n#{address_2}" if address_2.present?
      address << "\n#{city}" if city.present?
      address << ", #{state}" if state.present?
      address << " #{zip}"
      address << "\n#{Country.country_name(country)}" if country.present? && country != "US"
    end

    address
  end

  def w8ben_owner_type_text
    return unless completed_w8ben?

    {
      "individual" => "Individual",
      "corp" => "Corporation",
      "entity" => "Disregarded entity",
      "partner" => "Partnership",
      "simp_trust" => "Simple trust",
      "grant_trust" => "Grantor trust",
      "compl_trust" => "Complex trust",
      "estate" => "Estate",
      "govt" => "Government",
      "intl_org" => "International organization",
      "centr_bank" => "Central bank of issue",
      "tax_exmt_org" => "Tax-exempt organization",
      "priv_fndtn" => "Private foundation"
    }[owner_type]
  end

  def w8ben_claim_of_treaty_benefits_text
    return "" unless claim_of_treaty_benefits

    cotb = "a. Checked: #{claim_of_treaty_benefits['q9']['a']['checked']}. Resident of #{Country.country_name(claim_of_treaty_benefits['q9']['a']['resident'])};"
    cotb << "b. Checked: #{claim_of_treaty_benefits['q9']['b']['checked']};"
    cotb << "c. Checked: #{claim_of_treaty_benefits['q9']['c']['checked']};"
    cotb << "d. Checked: #{claim_of_treaty_benefits['q9']['d']['checked']};"
    cotb << "e. Checked: #{claim_of_treaty_benefits['q9']['e']['checked']};"
  end

  private

  def clean_tax_id
    # Clean (remove space, dash, symbols, alpha characters) after the first character
    self.tax_id = tax_id.gsub(/[^0-9]/, "") if tax_id.present?
  end

  def validates_agreed_to_w9
    return unless !is_w8ben? && !agreed_to_w9

    errors.add(
      "agreed_to_w9",
      I18n.t("models.publishing.please_certify_the_statements_for_payment_of_your_royalties")
    )
  end

  def validates_agreed_to_w8ben
    return unless is_w8ben? && !agreed_to_w8ben

    errors.add(
      "agreed_to_w8ben",
      I18n.t("models.publishing.please_certify_the_statements_for_payment_of_your_royalties")
    )
  end

  def validates_presence_of_q9_a_resident
    unless is_w8ben? &&
           claim_of_treaty_benefits &&
           claim_of_treaty_benefits["q9"]["a"]["checked"] &&
           claim_of_treaty_benefits["q9"]["a"]["resident"].blank?

      return
    end

    errors.add("claim_of_treaty_benefits", I18n.t("models.publishing.claim_of_treaty_benefits"))
  end

  def validates_tax_id
    if tax_id and tax_id == "000000000"
      errors.add("tax_id", I18n.t("models.publishing.id_ssn_can_not_be_all_zeros"))
    elsif tax_id and tax_id[0..2] == "000"
      errors.add("tax_id", I18n.t("models.publishing.id_ssn_can_not_start_with_zero"))
    end
  end

  def validates_sole
    return unless !is_entity? and (tax_id.blank? or name.blank? or entity_name.present?)

    errors.add("sole", I18n.t("models.publishing.if_filing_as_an_individual"))
  end

  def validates_corp
    return unless is_entity? and (tax_id.blank? or entity_name.blank? or name.present?)

    errors.add("corp", I18n.t("models.publishing.if_filing_as_a_business"))
  end

  def encrypt_and_store_salt(str)
    return if str.blank?

    cipherer = Cipherer.new(
      strategy: Crypto::Strategies::Aes128CbcStrategy.new(
        secret: SECRETS["tax_key"],
        base64_encode: true
      )
    )

    encryption_artifact = cipherer.encrypt(str)

    self.salt = encryption_artifact.salt
    encryption_artifact.encrypted_string
  end

  def decrypt(encrypted)
    begin
      cipherer = Cipherer.new(
        strategy: Crypto::Strategies::Aes128CbcStrategy.new(
          secret: SECRETS["tax_key"],
          salt: salt,
          base64_encode: true
        )
      )

      cipherer.decrypt(encrypted)
    rescue => e
      logger.error "Error in decrypting tax_id. Possible cause is the tax_key was changed. error_message = #{e.message}"

      "bad_decryption"
    end
  end

  def check_and_set_is_entity
    set_w9_is_entity unless is_w8ben? # w8ben does not have a special condition

    true
  end

  def set_w9_is_entity
    self.is_entity =
      !(classification == "individual")
  end

  def check_w8ben_name
    if is_w8ben?
      if is_entity?
        self.name = nil
      else
        self.name = name || entity_name
        self.entity_name = nil
      end
    end

    true
  end

  def pretty_classification
    case classification
    when "c_corp"
      "C Corporation"
    when "s_corp"
      "S Corporation"
    when "partnership"
      "Partnership"
    when "trust_estate"
      "Trust/estate"
    when "llc"
      "Limited Liability Company"
    end
  end
end
