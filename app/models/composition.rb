class Composition < ApplicationRecord
  has_paper_trail
  has_many :publishing_splits
  has_many :songs
  has_many :muma_songs
  has_many :non_tunecore_songs
  has_many :rights_app_errors, as: :requestable
  has_many :transitions, as: :state_machine, dependent: :destroy
  has_many :recordings, dependent: :destroy

  has_many :cowriters, through: :publishing_splits, source: :writer, source_type: "Cowriter"

  before_save :save_cwr_name
  before_save :update_state_transition
  after_save :partially_terminate_composer, if: :terminated?

  acts_as_state_machine initial: :new

  # TODO: Check to see if we can dynamically defined the state, since we have to defined the state -> status mapping
  state :new
  state :pending_distribution
  state :not_controlled
  state :split_submitted
  state :sent_for_processing
  state :sent_to_society
  state :accepted
  state :conflict
  state :rejected
  state :hidden
  state :terminated
  state :submitted
  state :resubmitted
  state :verified
  state :ineligible

  STATE_TO_STATUS =
    {
      new: "Split missing",
      split_submitted: "Split submitted",
      sent_for_processing: "Sent for processing",
      sent_to_society: "Sent for registration",
      accepted: "Registered",
      conflict: "Pending",
      rejected: "Rejected",
      not_controlled: "Not controlled",
      pending_distribution: "Not distributed yet",
      hidden: "Hidden",
      terminated: "Terminated",
      submitted: "Submitted",
      resubmitted: "Resubmitted",
      verified: "Accepted",
      ineligible: "Ineligible",
    }.with_indifferent_access

  TUNECORE_COMPANY_CODE = "TUNE".freeze
  SOCIETY_CODES         = %w[BMI ASCA SESA].freeze

  scope :nu,              ->  { where(state: :new) }
  scope :pending,         ->  { where(state: :pending_distribution) }
  scope :not_controlled,  ->  { where(state: :not_controlled) }
  scope :submitted,       ->  { where(state: :split_submitted) }
  scope :processing,      ->  { where(state: :sent_for_processing) }
  scope :society,         ->  { where(state: :sent_to_society) }
  scope :accepted,        ->  { where(state: :accepted) }
  scope :conflicted,      ->  { where(state: :conflict) }
  scope :rejected,        ->  { where(state: :rejected) }
  scope :hidden,          ->  { where(state: :hidden) }
  scope :terminated,      ->  { where(state: :terminated) }
  scope :resubmitted,     ->  { where(state: :resubmitted) }
  scope :verified,        ->  { where(state: :verified) }

  event(:submit_split_without_validation) do
    transitions from: [:new, :pending_distribution], to: :split_submitted
  end

  event(:submit_split) do
    transitions from: [:new, :pending_distribution, :not_controlled],
                to: :split_submitted,
                guard: :ensure_distributed_release
  end

  event(:send_for_processing) do
    transitions from: [:new, :split_submitted], to: :sent_for_processing
  end

  event(:send_to_society) do
    transitions from: [:new, :sent_for_processing], to: :sent_to_society
  end

  event(:accepted_by_society) do
    transitions from: [:new, :sent_for_processing, :sent_to_society, :rejected, :conflict, :terminated], to: :accepted
  end

  event(:rejected_by_society) do
    transitions from: [:new, :sent_for_processing, :sent_to_society], to: :rejected
  end

  event(:conflict) do
    transitions from: [:new, :sent_for_processing, :sent_to_society], to: :conflict
  end

  event(:set_not_controlled) do
    transitions from: [:new, :split_submitted, :sent_for_processing, :sent_to_society, :conflict], to: :not_controlled
  end

  event(:set_pending_distribution) do
    transitions from: [:new, :not_controlled], to: :pending_distribution
  end

  event(:hide) do
    transitions from: STATE_TO_STATUS.keys, to: :hidden
  end

  event(:unhide) do
    transitions from: [:hidden], to: :new
  end

  event(:terminate) do
    transitions from: STATE_TO_STATUS.keys, to: :terminated
  end

  event(:submitted) do
    transitions from: STATE_TO_STATUS.keys, to: :submitted
  end

  event(:resubmitted) do
    transitions from: STATE_TO_STATUS.keys, to: :resubmitted
  end

  event(:verified) do
    transitions from: STATE_TO_STATUS.keys, to: :verified
  end

  event(:disqualify) do
    transitions from: STATE_TO_STATUS.without(:ineligible).keys, to: :ineligible
  end

  # Returns a MyCompositions object
  def self.my_compositions(person, options = {})
    my_compositions = MyComposition.new(person)
    my_compositions.compositions(options)
  end

  def self.batch_update_statuses
    # TODO: Depending on the speed of these updates, we may need to do it by updated_at date for the daily imports
    batch_update_accepted
    batch_update_sent
    batch_update_conflict
  end

  def self.batch_update_accepted
    batch_update_custom_registered
    batch_update_with_work_number

    # Accepted is a special case. Once a registration is accepted, we do not need to care what happens
    # at a later date since Burbank often send updates to the society even for song with status of AC or AS
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update compositions c,
        (
        select mrh.song_code, mrh.status, mrh.entry_created_at, mrh.society_code
        from muma_song_registration_history mrh
        inner join
        (
        select h.song_code, h.society_code, max(h.entry_created_at) as max_entry_created_at
        from muma_song_registration_history h
        inner join muma_song_ips msip
        on h.song_code = msip.song_code
        where h.status IN ('AC','AS')
        and ((msip.publisher_code = 'TUNE' and h.society_code = 'BMI')
        or (msip.publisher_code = 'TUN1' and h.society_code = 'ASCP')
        or (msip.publisher_code = 'TUN2' and h.society_code = 'SESC'))
        and msip.ip_chain = 1
        and msip.link_parent_ip_code is NULL
        group by h.song_code, h.society_code
        ) max_history
        on mrh.song_code = max_history.song_code
        and mrh.entry_created_at = max_history.max_entry_created_at
        and mrh.society_code = max_history.society_code
        ) latest_history, muma_songs ms
        set c.prev_state = c.state,
        c.state = 'accepted',
        c.state_updated_on = latest_history.entry_created_at,
        c.updated_at = CURRENT_TIMESTAMP
        where latest_history.song_code = ms.code
        and ms.composition_id = c.id
        and (c.state IS NULL OR c.state NOT IN ('accepted'))"
        )
      end

    logger.info "Composition.batch_update_accepted realtime = #{realtime}"
  end

  def self.batch_update_custom_registered
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update compositions c,
        muma_songs ms
        set c.prev_state = c.state,
        c.state = 'accepted',
        c.state_updated_on = ms.source_updated_at,
        c.updated_at = CURRENT_TIMESTAMP
        where c.id = ms.composition_id
        and ms.registered = true
        and (c.state IS NULL OR c.state NOT IN ('accepted'))"
        )
      end

    logger.info "Composition.batch_update_custom_registered realtime = #{realtime}"
  end

  def self.batch_update_with_work_number
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update
        muma_song_societies mss, compositions c, muma_songs ms
        set c.prev_state = c.state,
        c.state = 'accepted',
        c.state_updated_on = mss.registered_at,
        c.updated_at = CURRENT_TIMESTAMP
        where mss.song_code = ms.code
        and c.id = ms.composition_id
        and COALESCE(mss.`work_num`,'') != ''
        and c.state != 'accepted'"
        )
      end

    logger.info "Composition.batch_update_with_work_number realtime = #{realtime}"
  end

  def self.batch_update_conflict
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update compositions c,
        (
        select mrh.song_code, mrh.status, mrh.entry_created_at, mrh.society_code
        from muma_song_registration_history mrh
        inner join
        (
        select h.song_code, h.society_code, max(h.entry_created_at) as max_entry_created_at
        from muma_song_registration_history h
        inner join muma_song_ips msip
        on h.song_code = msip.song_code
        where ((msip.publisher_code = 'TUNE' and h.society_code = 'BMI')
        or (msip.publisher_code = 'TUN1' and h.society_code = 'ASCP')
        or (msip.publisher_code = 'TUN2' and h.society_code = 'SESC'))
        and msip.ip_chain = 1
        and msip.link_parent_ip_code is NULL
        group by h.song_code, h.society_code
        ) max_history
        on mrh.song_code = max_history.song_code
        and mrh.entry_created_at = max_history.max_entry_created_at
        and mrh.society_code = max_history.society_code
        ) latest_history, muma_songs ms
        set c.prev_state = c.state,
        c.state = 'conflict',
        c.state_updated_on = latest_history.entry_created_at,
        c.updated_at = CURRENT_TIMESTAMP
        where latest_history.song_code = ms.code
        and ms.composition_id = c.id
        and (c.state IS NULL OR c.state NOT IN ('accepted','conflict'))
        and latest_history.status IN ('CO');"
        )
      end

    logger.info "Composition.batch_update_conflict realtime = #{realtime}"
  end

  def self.batch_update_sent
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update compositions c,
        (
        select mrh.song_code, mrh.status, mrh.entry_created_at, mrh.society_code
        from muma_song_registration_history mrh
        inner join
        (
        select h.song_code, h.society_code, max(h.entry_created_at) as max_entry_created_at
        from muma_song_registration_history h
        inner join muma_song_ips msip
        on h.song_code = msip.song_code
        where ((msip.publisher_code = 'TUNE' and h.society_code = 'BMI')
        or (msip.publisher_code = 'TUN1' and h.society_code = 'ASCP')
        or (msip.publisher_code = 'TUN2' and h.society_code = 'SESC'))
        and msip.ip_chain = 1
        and msip.link_parent_ip_code is NULL
        group by h.song_code, h.society_code
        ) max_history
        on mrh.song_code = max_history.song_code
        and mrh.entry_created_at = max_history.max_entry_created_at
        and mrh.society_code = max_history.society_code
        ) latest_history, muma_songs ms
        set c.prev_state = c.state,
        c.state = 'sent_to_society',
        c.state_updated_on = latest_history.entry_created_at,
        c.updated_at = CURRENT_TIMESTAMP
        where latest_history.song_code = ms.code
        and ms.composition_id = c.id
        and (c.state IS NULL OR c.state NOT IN ('accepted','sent_to_society'))
        and latest_history.status IN ('RA','RJ','RC')"
        )
      end

    logger.info "Composition.batch_update_sent realtime = #{realtime}"
  end

  def self.batch_update_not_controlled
    realtime =
      Benchmark.realtime do
        self.class.connection.execute(
          "update
        compositions c, publishing_splits s
        set c.state = 'not_controlled',
        c.state_updated_on = s.updated_at,
        c.prev_state = IF(c.state,c.state,'new')
        where c.id = s.composition_id
        and ((c.state is NULL or c.state IN ('new','split_submitted')) and s.percent <= 0);"
        )
      end

    logger.info "Composition.batch_update_not_controlled realtime = #{realtime}"
  end

  def self.convert_to_status(state)
    matched = STATE_TO_STATUS.detect { |key, _value| key.casecmp?(state) }
    return matched[1] if matched
  end

  def self.convert_to_state(status)
    matched = STATE_TO_STATUS.detect { |_key, value| value.casecmp?(status) }
    return matched[0] if matched
  end

  def self.update_pending_submissions(album)
    return unless album.payment_applied?

    album.songs.each do |song|
      composition = song.composition
      composition.submit_split_without_validation! if composition && composition.pending_distribution?
    end
  end

  def self.update_details_post_finalized(album)
    return unless album.finalized?

    album.songs.each do |song|
      composition = song.composition
      statuses = ["new", "pending_distribution"]

      # Update composition name to reflect the final song name if the status is <= split_submitted
      composition.update_attribute(:name, song.name) if composition && statuses.include?(composition.state)
    end
  end

  def self.find_non_distributed_albums(options = {})
    options = { converter_class: "MusicStores::YoutubeSR::Converter" }.merge!(options)

    limit =
      if options[:limit] == "all"
        nil
      else
        options[:limit].presence || 100
      end
    converter_class = options[:converter_class]

    Album
      .select("distinct albums.*")
      .joins("INNER JOIN songs on albums.id = songs.album_id
                INNER JOIN compositions on compositions.id = songs.composition_id
                INNER JOIN muma_songs on muma_songs.composition_id = compositions.id
                INNER JOIN petri_bundles on (albums.id = petri_bundles.album_id AND petri_bundles.state != 'dismissed')
                LEFT JOIN distributions on petri_bundles.id = distributions.petri_bundle_id
                AND distributions.converter_class = '#{converter_class}'")
      .where("muma_songs.mech_collect_share > 0
                AND muma_songs.company_code = ?
                AND is_deleted = 0
                AND albums.payment_applied = 1
                AND albums.album_type != 'Ringtone'
                AND albums.takedown_at IS NULL
                AND compositions.state = 'accepted'
                AND distributions.id IS NULL",
             TUNECORE_COMPANY_CODE)
      .order(:finalized_at)
      .limit(limit)
  end

  def self.deliver_to_youtube_sr(options = {})
    store = Store.find_by(short_name: "YoutubeSR")

    options = { limit: 100 }.merge!(options).merge!(converter_class: "MusicStores::YoutubeSR::Converter")
    albums = find_non_distributed_albums(options)
    distros = []
    albums.each do |album|
      params = {
        actor: "Composition.deliver_to_youtube_sr",
        reason: "Delivery to YoutubeSR"
      }
      salepoint = Salepoint.create_with_default_store_var(store)
      distros << album.petri_bundle.add_salepoints([salepoint], params)
    end

    distros
  end

  def qualify!
    if ineligible? && prev_state.present?
      ScottBarron::Acts::StateMachine::SupportingClasses::StateTransition.new(from: :ineligible, to: prev_state).perform(self)
    else
      []
    end
  end

  def account
    return composer.account if composer

    songs.first.person.accounts.publishing_administration.first if songs.any?
  end

  def composer
    @composer ||= (publishing_splits.first.try(:composer) || non_tunecore_songs.first.try(:composer))
  end

  def total_split_pct
    publishing_splits.sum(:percent)
  end

  def to_s
    name
  end

  def status
    self.class.convert_to_status(state)
  end

  def has_splits?
    return true unless state.nil? || new? || hidden?
  end

  def composition_name
    translated_name || name
  end

  def clean_composition_name
    sanitize(composition_name)
  end

  def clean_name
    sanitize(name)
  end

  def clean_translated_name
    return "" if translated_name.blank?

    sanitize(translated_name)
  end

  def send_to_rights_app
    return "Composer Not Present" if composer.blank?

    args = { composer: composer, composition: self }
    PublishingAdministration::WorkListService.list(account, true)
    PublishingAdministration::ApiClientServices::RecordingService.post_recording(args)
    nil
  rescue PublishingAdministration::ErrorService::ApiError => e
    e.message
  end

  def isrc
    songs.where.not(optional_isrc: nil).first&.optional_isrc ||
      songs.where.not(tunecore_isrc: nil).first&.tunecore_isrc ||
      non_tunecore_songs.where.not(isrc: nil).first&.isrc
  end

  def share_submitted_date
    publishing_splits.order(:updated_at).last&.updated_at
  end

  def song
    non_tunecore_songs.first || songs.first
  end

  private

  def sanitize(name_type)
    regex = %r/[%\\;<=>?\/\[\]\^`{}\|~]/
    I18n.transliterate(name_type.tr("$", "S").tr("\u2019", "'")).gsub(regex, "")
  end

  def save_cwr_name
    # fixed_name = name.to_s.gsub(/[\||`]/,"") #remove invalid ascii characters |`
    fixed_name = name.to_s.gsub(/[`‚ƒ„…†‡ˆ‰‹‘’“”•–—˜™š›¡¢¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿×\|]+/, "") # remove invalid cwr characters
    t_name = I18n.transliterate(fixed_name, replacement: "*?")
    self.cwr_name = t_name.include?("*?") ? name : t_name # only use the transliterated name if all characters transliterated successfully
  end

  def ensure_distributed_release
    # TODO: Check how we should handle checking multiple songs if the albums has been paid for
    if non_tunecore_songs.any? || songs.collect(&:album).any?(&:payment_applied)
      true
    else
      set_pending_distribution!
      false
    end
  end

  def partially_terminate_composer
    return unless composer && composer.terminated_composer.nil?

    TerminatedComposer.create(composer_id: composer.id, termination_type: "partially")
  end

  def update_state_transition
    return unless state_changed?

    self.state_updated_on = Time.now.to_date
    self.prev_state = state_was
  end
end
