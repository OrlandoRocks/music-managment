class TrendsBase < ApplicationRecord
  self.abstract_class = true

  establish_connection "trends_#{Rails.env}".to_sym
end
