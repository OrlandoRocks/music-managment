class DisputeAudit < ApplicationRecord
  serialize :note
  belongs_to :dispute

  def details
    note.each_with_object([]) do |(field, values), status_accum|
      if values[0].nil?
        new_value = values[1]
        status_accum << "Initialized #{field} with #{new_value}"
      else
        old_value = values[0]
        new_value = values[1]
        status_accum << "Updated #{field} from  #{old_value} to #{new_value}"
      end
    end
  end
end
