class PayoutWithdrawalType < ApplicationRecord
  PROVIDER_WITHDRAWAL_TYPES = [
    "PAYPAL", "PREPAID_CARD", "BANK", "PAPER_CHECK"
  ].freeze

  belongs_to :payout_provider
  has_many :payout_transfers

  has_paper_trail

  validates :payout_provider_id, uniqueness: { scope: :enabled }, if: :enabled
end
