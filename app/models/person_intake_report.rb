# = Description
#
#
# = Usage
#
#
# = Change Log
# [2010-10-05 -- CH]
# Created Class

class PersonIntakeReport < ReportBase
  attr_accessor :person_id

  def initialize(options)
    self.person_id = options[:person_id]
    self.fields = [
      { field_name: :resolution, data_type: ReportDataCell::STRING, show_average: false, show_total: false, is_title: true },
      { field_name: :number_of_stores, data_type: ReportDataCell::STRING, show_average: false, show_total: false },
      { field_name: :albums, data_type: ReportDataCell::INTEGER, show_average: false, show_total: false, total_in_footer: true },
      { field_name: :songs_downloaded, data_type: ReportDataCell::INTEGER, show_average: false, show_total: false, total_in_footer: true },
      { field_name: :songs_streamed, data_type: ReportDataCell::INTEGER, show_average: false, show_total: false, total_in_footer: true },
      { field_name: :videos, data_type: ReportDataCell::INTEGER, show_average: false, show_total: false, total_in_footer: true },
      { field_name: :total_posted, data_type: ReportDataCell::CURRENCY, show_average: false, show_total: false, total_in_footer: true }
    ]

    options = options.merge(
      {
        report_type: UNGROUPED,
        data_is_flat: true,
        include_total_and_avg: false,
        headers: ["", "# of Stores", "Albums", "Songs", "Streams", "Videos", "Total Sales"],
        chart_type: LINE_CHART
      }
    )
    super
  end

  def retrieve_data
    PersonIntake.find_by_sql(
      [
        "select DATE_FORMAT(pit.created_at, '%M %Y') as resolution,
                                        TRUNCATE(sum(distinct sr.amount),2) as total_posted,
                                        CONCAT_WS(' ', count(distinct srm.sip_store_id), 'Stores') as number_of_stores,
                                        sum(if(related_type='Album' and distribution_type='Download',sr.quantity,0)) as albums,
                                        sum(if(related_type='Song' and distribution_type='Download',sr.quantity,0)) as songs_downloaded,
                                        sum(if(related_type='Song' and distribution_type='Streaming',sr.quantity,0)) as songs_streamed,
                                        sum(if(related_type='Video',sr.quantity,0)) as videos
                                      from person_intakes pit
                                      inner join sales_records sr on sr.person_intake_id=pit.id
                                      inner join sales_record_masters srm on srm.id=sr.sales_record_master_id
                                      where pit.person_id=?
                                      group by DATE_FORMAT(pit.created_at, '%M %Y')
                                      order by pit.created_at DESC",
        person_id
      ]
    )
  end
end
