class Cert < ApplicationRecord
  include Tunecore::Certs::ProductRedemption
  prepend VerifyWithProductRedemption

  validates :cert, uniqueness: { if: proc { |model| !(model.cert =~ /^-/) } }
  validates :cert_engine, :engine_params, presence: true
  belongs_to :cert_batch
  belongs_to :person
  belongs_to :purchase
  belongs_to :admin, class_name: "Person"
  validates :cert, length: { maximum: 30 }, on: :create

  before_destroy :protect_redeemed_certs

  def self.available_engines
    %w[
      DefaultPercent
      FreeSongs
      FreeStores
      GcBrochure
      GcPrepaid
      SpawningPercent
      SpawningFreeStores
      DefaultAlbumPercent
      DefaultSinglePercent
      DefaultRingtonePercent
      GcDistro
      AlfredDistro
      OurstageDistro
      SongwriterPercent
      YoutubeMonetizationPercent
      DefaultAlbumSinglePercent
      DefaultRenewalPercent
      StoreAutomatorPercent
      SpawningPercentSingle
      SpawningPercentAlbum
      SpawningPercentDistributionCr
      SpawningPercentDistributionAll
      SpawningAllDistribution
      SpawningPercentProfessional
      SpawningPercentBreakoutArtist
      SpawningPercentRisingArtist
      SpawningPercentAllPlans
    ]
  end

  def self.available_engine_classes
    available_engines.map { |e| Tunecore::CertEngine.const_get(e.to_sym) }
  end

  def self.verify(options)
    entered_code = options[:entered_code] # user entered code
    purchase = options[:purchase]         # purchase object
    admin = options[:admin]               # administrator person object

    raise ArgumentError, "expected a Purchase object" unless purchase.is_a?(Purchase)

    return "Purchase does not allow new promotion codes to be applied" unless purchase.allow_new_cert?

    code, issuer = code_and_issuer(entered_code)
    cert = find_by(cert: code)

    if cert.blank?
      message = mark_invalid_redemption_attempt(purchase.person)
      return message if message

      return "Invalid promotion code"
    end

    return "Invalid: admin only promotion code" if cert.admin_only? && (!admin or !admin.is_administrator?)

    if cert && !cert.engine.applicable_for_discount?(purchase)
      return "This promotion code cannot be applied to this purchase."
    end

    if purchase.targeted_product && (purchase.targeted_product.discount_amount_cents(purchase.cost_cents) > cert.discount_amount_cents(purchase.price_calculator))
      return "This promotion code would give a smaller discount than the offer already applied."
    end

    cert.verify(purchase.person.id, purchase).tap do |result|
      if result.is_a?(Cert)
        # we wait to store the issuer until now because
        # in the case of a spawning cert we store the
        # issuer with the spawned one.
        result.update_attribute(:issuer, issuer) if issuer

        # store the person id of the admin using the cert
        result.update_attribute(:admin_id, admin.id) if result.admin_only?

        if purchase.targeted_product
          purchase.product.set_targeted_product(nil)
          purchase.update(targeted_product: nil)
        end

        # notify the purchase about the potential for price changes
        purchase.reload.recalculate
      end
    end
  end

  def self.engine_class(engine_name)
    engine_name = "default_percent" if engine_name.blank?
    Tunecore::CertEngine.const_get(engine_name.to_s.camelize.to_sym)
  end

  # split the entered code into the cert code and issuer
  # or just return the entered code if it doesn't parse
  # as such
  def self.code_and_issuer(entered_code)
    %r{(.+)-(.+)}.match(entered_code).to_a[1..-1] || entered_code
  end

  def engine
    @engine ||= self.class.engine_class(cert_engine).new(self)
  end

  # we take a copy of the discount_amount_cents and store it for posterity
  def discount_amount_cents(*args, &block)
    engine.discount_amount_cents(*args, &block).tap do |dac|
      unless dac == last_discount_amount_cents
        if new_record?
          last_discount_amount_cents = dac
        else
          update_attribute :last_discount_amount_cents, dac
        end
      end
    end
  end

  # cert engine now handles calculations
  delegate :verify, :details, :spawning?, to: :engine

  def redeemed?
    !date_used.nil?
  end

  def calculate_total_amount(total)
    self.total_amount = (total.to_f * percent_off.to_f / 100)
  end

  def disassociate
    update(purchase: nil, last_discount_amount_cents: nil, person: nil, date_used: nil)

    destroy if ["SpawningFreeStores", "SpawningPercent"].include?(cert_engine)
  end

  def validate_expiry_date_within_2_days
    return unless expiry_date.blank? || expiry_date < 2.days.ago

    errors.add(:expiry_date, "Invalid expiration date, earliest valid date is yesterday.")
  end

  private

  def protect_redeemed_certs
    # first of all, allow yourself to be deleted even if you are
    # redeemed - so long as you were associated with a purchase and
    # your purchase has been deleted
    # need confirmation from Gary
    # return true if purchase_id? && purchase.nil?
    raise "can not delete redeemed cert" if redeemed?
  end
end
