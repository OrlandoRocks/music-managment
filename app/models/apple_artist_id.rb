class AppleArtistId < ApplicationRecord
  belongs_to :person

  validates :person, :apple_artist_id, presence: true
  validates :apple_artist_id, uniqueness: { scope: :person_id }
end
