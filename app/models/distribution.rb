# frozen_string_literal: true

class Distribution < ApplicationRecord
  include Distribution::Distributable

  METADATA_UPDATE_DELIVERY_TYPE = "metadata_update"

  belongs_to :petri_bundle

  has_many :distributions_salepoints, class_name: "DistributionSalepoint"
  has_many :salepoints, through: :distributions_salepoints, class_name: "::Salepoint"
  has_many :stores, through: :salepoints
  has_many :distribution_songs

  scope :join_creatives,
        -> {
          joins(<<-SQL.strip_heredoc)
      INNER JOIN petri_bundles on distributions.petri_bundle_id = petri_bundles.id
      INNER JOIN albums on petri_bundles.album_id = albums.id
      INNER JOIN creatives on albums.id = creatives.creativeable_id
          SQL
        }

  scope :unreviewed_albums,
        -> {
          joins(petri_bundle: :album)
            .where(albums: { legal_review_state: "NEEDS REVIEW" })
            .where(converter_class: "MusicStores::Streaming::Converter")
            .where(state: "new")
            .where(PetriBundle.arel_table[:state].not_eq("dismissed"))
        }

  scope :for_store_creatives,
        ->(person_id, artist_id, converter_class) {
          join_creatives.where(
            creatives: {
              person_id: person_id,
              artist_id: artist_id
            },
            distributions: {
              converter_class: converter_class
            }
          )
        }

  after_commit :after_commit_update_album_stream_cache

  def self.valid_state?(state)
    return false if state.blank?

    valid_states.include?(state.to_sym)
  end

  def distribution_type
    "Album"
  end

  delegate :album, to: :petri_bundle

  def distribution_song(song)
    distribution_songs.find(song: song)
  end

  def store
    stores.first || salepoints.first.try(:store)
  end

  def metadata_update?
    delivery_type == METADATA_UPDATE_DELIVERY_TYPE
  end

  def never_delivered?
    last_delivery_type.nil?
  end

  # Only guards metadata_updates on never-delivered distributions.
  # Other retries, even of presently enqueued distributions are allowed.
  def retriable?
    return false if metadata_update? && never_delivered?

    true
  end

  def taken_down?
    petri_bundle.album.takedown?
  end

  def approved_for_distribution?
    if petri_bundle&.album && !petri_bundle.album.approved_for_distribution?
      false
    else
      true
    end
  end

  def job_id=(job_id)
    self.sqs_message_id = job_id
  end

  def job_id
    self[:sqs_message_id]
  end

  def self.find_by_store_and_album(store_id, album_id)
    s_table = Salepoint.arel_table
    joins(:salepoints)
      .where(s_table[:store_id].eq(store_id)
      .and(s_table[:salepointable_type].eq("Album")
      .and(s_table[:salepointable_id].eq(album_id)))).distinct.readonly(false).first
  end

  def self.find_by_store_name_and_upc(store_name, upc)
    album    = Album.find_by_upc(upc)
    s_table  = Salepoint.arel_table
    st_table = Store.arel_table
    pb_table = PetriBundle.arel_table

    return unless album

    joins(:petri_bundle, salepoints: :store)
      .where(st_table[:short_name].eq(store_name)
      .and(s_table[:salepointable_type].eq("Album")
      .and(pb_table[:state].not_eq("dismissed"))
      .and(s_table[:salepointable_id].eq(album.id)))).distinct.readonly(false).first
  end

  def self.get_delivered_upc(store_short_name, start_date, end_date)
    joins([salepoints: :store, petri_bundle: [album: :upcs]])
      .where(state: "delivered", created_at: start_date.to_date...end_date.to_date)
      .where(stores: { short_name: store_short_name })
      .pluck(:number)
  end

  def valid_for_distribution?
    !(converter_class == "MusicStores::YoutubeMusic::Converter" && Store.consolidate_google_ytm_store?)
  end

  def is_itunes?
    converter_class == "MusicStores::Itunes::Converter"
  end

  def has_curated_artist_flags?
    album.creatives.any?(&:curated_artist_flag)
  end

  private

  def after_commit_update_album_stream_cache
    Distribution::AfterCommitUpdateAlbumStreamCache.call(self)
  end
end
