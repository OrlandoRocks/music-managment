# frozen_string_literal: true

class IterableCampaign < ApplicationRecord
  enum campaign_names: {
    RENEWAL_UPCOMING: "Renewal Upcoming"
  }

  default_scope { where(environment: Rails.env.production? ? "PRODUCTION" : "SANDBOX") }

  validates :campaign_id, presence: true

  validates :environment,
            presence: true,
            inclusion: { in: %w[PRODUCTION SANDBOX] }

  validates :name,
            presence: true,
            inclusion: { in: IterableCampaign::campaign_names.values },
            uniqueness: { scope: :environment }
end
