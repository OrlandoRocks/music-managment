class ReadOnlyReplica::Album < ReadOnlyReplica::ApplicationRecord
  self.table_name = :albums
  include AlbumMethods

  # replacement for association definition 'has_many :creatives, -> { order "creatives.id" }, as: :creativeable'
  # creativeable_type is inferred as ReadOnlyReplica::Album by default.
  def creatives
    ReadOnlyReplica::Creative.where(creativeable_id: id, creativeable_type: "Album").order("creatives.id")
  end
end
