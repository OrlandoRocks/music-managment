class ReadOnlyReplica::Song < ReadOnlyReplica::ApplicationRecord
  self.table_name = :songs

  include SongMethods
end
