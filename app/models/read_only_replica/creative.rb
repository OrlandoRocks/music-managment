class ReadOnlyReplica::Creative < ReadOnlyReplica::ApplicationRecord
  self.table_name = :creatives

  include CreativeMethods

  # Replacement for association definition 'belongs_to :creativeable, polymorphic: true'
  # creativeable looks for non-ReadOnlyReplica class by default.
  def creativeable
    "ReadOnlyReplica::#{creativeable_type}".constantize.find(creativeable_id)
  end
end
