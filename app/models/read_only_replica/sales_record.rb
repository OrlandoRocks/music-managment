class ReadOnlyReplica::SalesRecord < ReadOnlyReplica::ApplicationRecord
  self.table_name = :sales_records

  include SalesRecordMethods
end
