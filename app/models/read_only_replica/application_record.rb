class ReadOnlyReplica::ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  establish_connection :tcw_replica if ENV["REPLICA_DB_HOST"]

  # enable strong parameters for all ApplicationRecord-derived models
  include ActiveModel::ForbiddenAttributesProtection
  include ApplicationRecordMethods

  def readonly?
    true
  end

  def before_destroy
    raise ActiveRecord::ReadOnlyRecord
  end
end
