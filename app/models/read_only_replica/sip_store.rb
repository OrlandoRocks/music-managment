class ReadOnlyReplica::SipStore < ReadOnlyReplica::ApplicationRecord
  self.table_name = :sip_stores

  include SipStoreMethods
end
