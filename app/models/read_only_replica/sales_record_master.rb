class ReadOnlyReplica::SalesRecordMaster < ReadOnlyReplica::ApplicationRecord
  self.table_name = :sales_record_masters

  include SalesRecordMasterMethods
end
