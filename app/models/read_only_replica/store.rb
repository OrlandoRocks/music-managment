class ReadOnlyReplica::Store < ReadOnlyReplica::ApplicationRecord
  self.table_name = :stores

  include StoreMethods
end
