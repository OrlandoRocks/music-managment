class CountryWebsiteLanguage < ApplicationRecord
  RU_US = "ru-us"
  ID_US = "id-us"
  RO_US = "ro-us"
  TH_US = "th-us"
  HU_US = "hu-us"
  NL_US = "nl-us"
  CS_US = "cs-us"
  PL_US = "pl-us"
  TR_US = "tr-us"
  FR_CA = "fr-ca"
  SC_US = "sc-us"
  TC_US = "tc-us"

  belongs_to :country_website

  has_one :redirect_country_website, foreign_key: "redirect_country_website_id", class_name: "CountryWebsite"

  validates :selector_language, presence: true
  validates :selector_country, presence: true
  validates :selector_order, presence: true
  validates :country_website_id, presence: true

  LANGUAGE_CODE_TO_COUNTRY_MAP = {
    GERMANY_LOCALE => "Deutschland",
    AUSTRALIA_LOCALE => "Australia",
    GREAT_BRITAIN_LOCALE => "United Kingdom",
    INDIA_LOCALE => "India",
    FRANCE_LOCALE => "France",
    ITALY_LOCALE => "Italia",
  }

  def self.excluded_locales(user, country_website)
    excluded_list = []

    excluded_list << RU_US unless show_language_feature?(:russian_language_option, user, country_website)

    excluded_list << ID_US unless show_language_feature?(:indonesian_language_option, user, country_website)

    excluded_list << RO_US unless show_language_feature?(:romanian_language_option, user, country_website)

    excluded_list << TH_US unless show_language_feature?(:thai_language_option, user, country_website)

    excluded_list << HU_US unless show_language_feature?(:hungarian_language_option, user, country_website)

    excluded_list << NL_US unless show_language_feature?(:dutch_language_option, user, country_website)

    excluded_list << CS_US unless show_language_feature?(:czech_language_option, user, country_website)

    excluded_list << PL_US unless show_language_feature?(:polish_language_option, user, country_website)

    excluded_list << FR_CA unless show_language_feature?(:fr_ca_language_option, user, country_website)

    excluded_list << TR_US unless show_language_feature?(:turkish_language_option, user, country_website)

    excluded_list << SC_US unless show_language_feature?(:sc_language_option, user, country_website)

    excluded_list << TC_US unless show_language_feature?(:tc_language_option, user, country_website)

    excluded_list
  end

  # show_website_feature? is for when we don't have a current_user
  def self.show_language_feature?(feature, user, country_website)
    FeatureFlipper.show_feature?(feature, user) ||
      FeatureFlipper.show_website_feature?(feature, country_website)
  end

  def self.filter_locales(user, country_website)
    locales_to_exclude = excluded_locales(user, country_website)

    where.not(yml_languages: locales_to_exclude)
  end

  def language_abbreviation
    yml_languages.split("-")[0]
  end
end
