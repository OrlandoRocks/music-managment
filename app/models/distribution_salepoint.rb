class DistributionSalepoint < ActiveRecord::Base
  self.table_name = "distributions_salepoints"

  belongs_to :salepoint
  belongs_to :distribution

  validate :validate_uniqueness_salepoint_distribution, on: :create

  def validate_uniqueness_salepoint_distribution
    duplicate = DistributionSalepoint.exists?(distribution_id: distribution_id, salepoint_id: salepoint_id)

    errors.add(:base, "Duplicate DistributionSalepoint") if duplicate
  end
end
