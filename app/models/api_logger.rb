# frozen_string_literal: true

class ApiLogger < ActiveRecord::Base
  serialize :request_body

  belongs_to  :requestable, polymorphic: true

  enum state: {
    enqueued: "enqueued",
    processing: "processing",
    error: "error",
    completed: "completed"
  }
end
