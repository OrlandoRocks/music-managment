class TierPerson < ApplicationRecord
  belongs_to :tier
  belongs_to :person

  validates :status, presence: true
  validates :tier_id, uniqueness: { scope: :person_id, if: -> { status == "active" } }

  enum status: {
    active: "active",
    inactive: "inactive"
  }
end
