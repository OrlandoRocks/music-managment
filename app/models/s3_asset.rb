require "net/http"
require "uri"

class S3Asset < ApplicationRecord
  include S3MetadataFetchable
  attr_writer :filename
  attr_writer :filetype

  has_one :media_asset
  has_one :derived_media_asset
  has_one :song
  has_one :s3_detail
  has_one :spatial_audio
  after_commit :enqueue_get_metadata, on: [:create, :update]

  validates :uuid,   length: { maximum: 50,  message: "UUID exceeded 50 characters", allow_nil: true }
  validates :key,    length: { maximum: 255, message: "Key exceeded 255 characters", allow_nil: true }
  validates :bucket, length: { maximum: 255, message: "Bucket exceeded 255 characters", allow_nil: true }

  def put!(opts = {})
    access = opts[:access] || nil
    file   = opts[:file]   || open(@filename)
    raise "You must specficy the filename of the S3Asset before you put" unless @filename

    logger.debug(
      "About to PUT #{File.size @filename if File.exist?(@filename)} bytes into S3 in bucket '#{bucket}' key '#{key}'"
    )

    # put the stream as binary, private-acl into the FLAC_BUCKET
    options = access.nil? ? {} : { acl: access }
    obj = get_bucket.objects[key]
    obj.write(file, options)

    logger.debug("S3 transfer complete.")

    # make the record as updated... just so we know the last upload time
    self.updated_at = Time.now
    save!
  end

  def exists?
    current_bucket = get_bucket
    return false unless current_bucket

    current_bucket.key(key).exists?
  end

  def get!
    raise "You must specficy the filename of the S3Asset before you put" unless @filename

    logger.debug("About to GET '#{@filename}' from S3 in bucket '#{bucket}' key '#{key}'")

    file = open(@filename, "w")
    file << get_bucket.get(key)

    logger.debug("Got #{File.size @filename} bytes from S3")
  end

  # Delete an asset from S3 and destroy the record for it
  def destroy!
    destroy if delete_key!
  end

  def get_bucket
    raise "You are not in the correct environment to access this bucket" unless VALID_S3_BUCKETS.include?(bucket)

    S3_CLIENT.buckets[bucket]
  end

  def delete_key!
    # just to protect ourselves if we're developing with production data
    return unless Rails.env.production? || bucket == "s3assets.tunecore.com.development"

    logger.debug("About to DELETE key #{key} from bucket #{bucket}")
    begin
      get_bucket.key(key).delete.nil? ? false : true
    rescue => e
      logger.error("Problem deleting key: #{e.message}")
    end
  end

  # Creates and returns a URL that will work n times to access the asset, using the TicketBooth server.
  # Important: This method makes an HTTP request to an external resource and could possibly fail if the
  #            TicketBooth server is offline.
  def create_restricted_url(downloads_allowed = 1)
    url = URI.parse("http://ticketbooth.tunecore.com/create")
    params = {
      "s3_bucket" => bucket,
      "s3_key" => key,
      "num_of_downloads" => downloads_allowed
    }
    req = Net::HTTP::Post.new(url.path)
    req.set_form_data(params)
    logger.info "Creating URL at TicketBooth s3_asset_id=#{id} bucket=#{bucket} key=#{key} downloads_allowed=#{downloads_allowed}"
    res = Net::HTTP.new(url.host, url.port).start { |http| http.request(req) }
    logger.debug "TicketBooth response: #{res.inspect} #{res.to_hash.inspect}"
    if res.code == "303" # got a redirect
      url = res.to_hash["x-download-location"][0] # custom header set by TicketBooth server
      logger.info "Got TicketBooth URL: s3_asset_id=#{id} url=#{url}"
    else
      logger.warn "Got invalid response from TicketBooth server: #{res.inspect}"
      url = nil
    end
    url
  end

  # Utility class method for setting the download filename on an arbitrary S3 asset
  def self.set_download_filename(bucket_name, key_name, filename)
    logger.info("Setting filename for  bucket=#{bucket_name} key=#{key_name} filename=#{filename}")
    AwsWrapper::S3.update_options(
      bucket: bucket_name,
      key: key_name,
      options: {
        content_disposition: "attachment; filename=\"#{filename}\""
      }
    )
  end

  def self.get_put_presigned_url(bucket_name, file_name, person_id)
    s3 = Aws::S3::Resource.new
    key_name = "#{person_id}/#{Time.current.strftime('%s%3N')}-#{file_name}"
    object = s3.bucket(bucket_name).object(key_name)
    presigned_url = object.presigned_url(:put, expires_in: 3600)

    {
      key_name: key_name,
      bucket_name: bucket_name,
      presigned_url: presigned_url
    }
  end

  # 2012-12-29 AK For renaming keys so that they can be archived in to glacier
  def rename_key(new_key_name)
    raise "New key name must not be empty" if new_key_name.blank?
    # Don't allow Bigbox assets to be renamed.  They need to be renamed through Bigbox.
    # See the asset.archive() method in Bigbox if that's what you're after.
    return false if bucket[/bigbox/]

    bucket = get_bucket
    key = bucket.key(self.key)
    transaction do
      self.key = new_key_name
      key.rename(new_key_name)
      save
    end
    true
  end

  def original_url(expiry = 1.hour)
    AwsWrapper::S3.read_url(
      bucket: bucket,
      key: key,
      options: {
        expires: expiry
      }
    )
  end

  def enqueue_get_metadata
    GetMetadataWorker.perform_async(id)
  end
end
