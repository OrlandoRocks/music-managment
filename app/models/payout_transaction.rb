class PayoutTransaction < PersonTransaction
  self.table_name = "person_transactions"

  skip_callback :create, :before, :lock_person_balance
  skip_callback :create, :before, :set_previous_balance
  skip_callback :create, :after, :update_balance
end
