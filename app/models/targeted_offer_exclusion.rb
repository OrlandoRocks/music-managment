class TargetedOfferExclusion < ApplicationRecord
  ###
  # Belongs to
  ###
  belongs_to :targeted_offer
  belongs_to :exclude_targeted_offer, class_name: "TargetedOffer"

  ###
  # Validations
  ###
  validate :targeted_offer_and_exclude_targeted_offer_are_not_equal

  def targeted_offer_and_exclude_targeted_offer_are_not_equal
    return unless same_as_targeted_offer?

    errors.add(
      :exclude_targeted_offer,
      I18n.t("models.promotion.cannot_be_the_same_as_the_targeted_offer")
    )
  end

  def same_as_targeted_offer?
    targeted_offer_id == exclude_targeted_offer_id
  end
end
