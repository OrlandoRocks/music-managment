# frozen_string_literal: true

class CountryAuditSource < ApplicationRecord
  validates :name, presence: true
  has_many :country_audits # don't expect to use this has_many relationship, but including as a formality

  DEFAULT_TO_US_NAME      = "Default to US"
  SELF_IDENTIFIED_NAME    = "Self-Identified"
  CHECKOUT_NAME           = "Checkout"
  STORED_CREDIT_CARD_NAME = "Stored Credit Card"
  IP_ADDRESS_NAME         = "IP Address"
  TWO_FACTOR_AUTH_NAME    = "Two Factor Auth"
  PAYONEER_KYC_NAME       = "Payoneer KYC"
  PAYONEER_POST_KYC_NAME  = "Payoneer Post-KYC"
  AUTO_RENEWAL_FORM_NAME  = "Auto-Renewal Form"
  PAYPAL_KYC_NAME         = "Paypal KYC"
  LEGACY_UK_NAME          = "Legacy UK to GB"

  ALL_SOURCE_NAMES = [
    DEFAULT_TO_US_NAME,
    SELF_IDENTIFIED_NAME,
    CHECKOUT_NAME,
    STORED_CREDIT_CARD_NAME,
    IP_ADDRESS_NAME,
    TWO_FACTOR_AUTH_NAME,
    PAYONEER_KYC_NAME,
    PAYONEER_POST_KYC_NAME,
    AUTO_RENEWAL_FORM_NAME,
    PAYPAL_KYC_NAME,
    LEGACY_UK_NAME
  ].freeze
end
