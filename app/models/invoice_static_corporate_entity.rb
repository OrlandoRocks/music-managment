# frozen_string_literal: true

class InvoiceStaticCorporateEntity < ApplicationRecord
  FIELDS = Set[
    "address1",
    "address2",
    "city",
    "postal_code",
    "country",
    "crn",
    "vat_registration_number"
  ].freeze

  belongs_to :related, polymorphic: true

  validates :related_id, :related_type, presence: true

  def affiliated_to_bi?
    name == CorporateEntity::BI_LUXEMBOURG_NAME
  end

  def tunecore_us?
    name == CorporateEntity::TUNECORE_US_NAME
  end
end
