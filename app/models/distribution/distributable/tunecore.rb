module Distribution::Distributable::Tunecore
  attr_reader :distribution_system

  def distribute_through_tunecore
    @distribution_system = DistributionSystem::Deliverer.new(DELIVERY_CONFIG)
    distribution_system.distribute(@converted_album, self)
    if archive_metadata
      handle_success("metadata is archived")
    else
      handle_success
    end
  ensure
    distribution_system.clean_up
  end

  def archive_metadata
    metadata_filename = job_id || SecureRandom.uuid
    # Fix ytsr by adding isrc
    distribution_system.archive_metadata(store.short_name, @converted_album.upc, metadata_filename)
  end

  def handle_success(message = nil)
    Distribution::StateUpdateService.update(self, { state: "delivered", message: message })
  end
end
