module Distribution::Distributable::Petri
  def distribute_through_petri
    upload_to_aws
  end

  def upload_to_aws
    PetriUpload.upload(self, @converted_album, @queue)
  end
end
