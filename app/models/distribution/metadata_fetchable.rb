module Distribution::MetadataFetchable
  def fetch_metadata
    MetadataFetcher.fetch_metadata(self)
  end

  def archived_metadata_url
    MetadataFetcher.archived_metadata_url(self)
  end
end
