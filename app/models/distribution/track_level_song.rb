module Distribution::TrackLevelSong
  def takedown?(salepoint)
    !!(salepoint.takedown_at || salepoint_songs.where(
      [
        "salepoint_id = ? AND takedown_at is not null",
        salepoint.id
      ]
    ).first)
  end
end
