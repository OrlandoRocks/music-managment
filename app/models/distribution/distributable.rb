module Distribution::Distributable
  extend ActiveSupport::Concern

  included do
    include Distribution::MetadataFetchable
    include Distribution::Stateful
    include Distribution::Convertible
    include Distribution::Batchable
  end

  class DistributionDeliveryError < StandardError; end

  include Distribution::Distributable::Tunecore
  include Distribution::Distributable::Petri

  DISTRIBUTION_ACTIONS = {
    "Distribution" => {
      not_for_streaming?: :dismiss,
      not_blocked?: :log_and_ignore,
      store_is_in_use?: :dismiss,
      approved_for_distribution?: :pending_approval,
      set_delivery_type: :errored,
      has_valid_converter_class?: :errored,
      build_converted_album: :errored,
      bundle_itunes_salepoints: :errored,
      do_distribution: :errored,
      update_delivery_type: :errored
    },
    "DistributionSong" => {
      store_is_in_use?: :dismiss,
      approved_for_distribution?: :pending_approval,
      set_delivery_type: :errored,
      has_valid_converter_class?: :errored,
      build_converted_album: :errored,
      do_distribution: :errored
    },
    "TrackMonetization" => {
      store_is_in_use?: :dismiss,
      approved_for_distribution?: :pending_approval,
      set_delivery_type: :errored,
      has_valid_converter_class?: :errored,
      build_converted_album: :errored,
      do_distribution: :errored
    }
  }.freeze

  def not_for_streaming?
    salepoints.joins(:store).where(stores: { name: Store::STREAMING }).blank?
  end

  def deliver(delivery_type_param, queue = nil)
    @delivery_type_param = delivery_type_param
    @queue               = queue
    if requires_track_level_distribution?
      deliver_distribution_songs if has_monetized_tracks?
    else
      DISTRIBUTION_ACTIONS[self.class.name].each do |action, state|
        short_circuit_delivery(action, state) unless send(action)
      end
    end
  rescue DistributionDeliveryError => e
    puts e.message
    false
  end

  def has_monetized_tracks?
    distribution_songs.any?
  end

  def short_circuit_delivery(action, state)
    message = "Failed to successfully distribute distro ID: #{id} during action: #{action}"
    send(state, message: message)
    raise DistributionDeliveryError.new(message)
  end

  def store_is_in_use?
    store && store.in_use_flag
  end

  def set_delivery_type
    self.delivery_type =
      if taken_down?
        "metadata_only"
      elsif @delivery_type_param == "metadata_only"
        has_been_delivered? ? "metadata_only" : "full_delivery"
      else
        "full_delivery"
      end
  end

  def bundle_itunes_salepoints
    if converter_class == "MusicStores::Itunes::Converter"
      itunes_salepoints_deltas =
        petri_bundle
        .album
        .salepoints
        .where.not(id: salepoints)
        .select { |sp| sp.finalized? && sp.store.short_name[/iTunes/] }

      salepoints.concat(itunes_salepoints_deltas)
    end
    true
  end

  def do_distribution
    send("distribute_through_#{store.delivery_service}") unless store.delivery_service == "tc_distributor"
  end

  def approved_for_distribution?
    raise NotImplementedError
  end

  def taken_down?
    raise NotImplementedError
  end

  def update_delivery_type
    self.last_delivery_type = delivery_type
    self.delivery_type      = nil
    save
  end

  def deliver_distribution_songs
    distribution_songs.each do |distro_song|
      distro_song.delivery_type = @delivery_type_param
      distro_song.retry(actor: "Youtubesr Distribution Retry", message: "Full YoutubeSr Distro Retry")
    end
  end

  def requires_track_level_distribution?
    is_a?(Distribution) && converter_class.include?("YoutubeSr")
  end

  def build_converted_album
    @converted_album = convert(@delivery_type_param)
  end
end
