class Distribution::MassRetryForm < FormObject
  attr_accessor :job_name,
                :person_id,
                :album_ids_or_upcs,
                :store_ids,
                :metadata_only,
                :retry_in_batches,
                :daily_retry_limit,
                :remote_ip,
                :valid_batch,
                :use_sns_only,
                :priority

  MAX_RETRY_ALBUMS = 5000

  define_model_callbacks :save, only: [:after]
  after_save :enqueue_mass_retry_worker, unless: :valid_batch?
  after_save :store_batch_in_redis, if: :valid_batch?

  validates :person_id, :remote_ip, presence: true
  validates :album_ids_or_upcs,
            presence: { message: "At least one valid Album Id or UPC is required." }
  validates :store_ids, presence: { message: "At least one store is required." }
  validates :job_name, presence: { if: :retry_in_batches?, message: "A Valid Job name is required if retrying in batches." }
  validates :daily_retry_limit, presence: { if: :retry_in_batches?, message: "Provide daily retry limit if retrying in batches." }
  validate :valid_daily_retry_limit?, if: :retry_in_batches? # NOTE: custom because we need integer not string

  def valid_daily_retry_limit?
    daily_retry_limit_max =
      ENV.fetch("BATCH_RETRY_DAILY_LIMIT", Store::BATCH_RETRY_DAILY_LIMIT_DEFAULT)&.to_i

    return true if daily_retry_limit&.to_i.in?(1..daily_retry_limit_max)

    errors.add(:daily_retry_limit, "Provide daily retry limit if retrying in batches.")
    false
  end

  def save
    Rails.logger.info("MASSRETRIES: Distribution::MassRetryForm valid? = #{valid?}")
    Rails.logger.info("MASSRETRIES: Distribution::MassRetryForm valid_for_sns? = #{valid_for_sns?}")
    Rails.logger.info("MASSRETRIES: Distribution::MassRetryForm errors = #{errors.messages}")

    run_callbacks :save and return true if valid? && valid_for_sns?

    false
  end

  def valid_for_sns?
    return true unless send_sns_notification?

    valid? || (errors[:album_ids_or_upcs].empty? && errors[:store_ids].empty?)
  end

  private

  def enqueue_mass_retry_worker
    if priority.to_i == 1
      Rails.logger.info("MASSRETRIES: Distribution::MassPriorityRetryWorker.perform_async calling with #{mass_retry_params.inspect}")
    else
      Rails.logger.info("MASSRETRIES: Distribution::MassRetryWorker.perform_async calling with #{mass_retry_params.inspect}")
    end
    # distributes albums across multiple workers and spaces them by 3 hours each if number of albums is greater than MAX_RETRY_ALBUMS
    # this prevents sidekiq containers from failing due to the processing of a large number of albums
    perform_in_hours = 0.hours
    album_ids_or_upcs.each_slice(albums_per_worker) do |albums_slice|
      worker_params = mass_retry_params
      worker_params[:album_ids_or_upcs] = albums_slice

      if (priority.to_i == 1)
        Distribution::MassPriorityRetryWorker.perform_in(perform_in_hours, worker_params)
      else
        Distribution::MassRetryWorker.perform_in(perform_in_hours, worker_params)
      end

      perform_in_hours += 3.hours
    end
  end

  def albums_per_worker
    total_no_of_albums = album_ids_or_upcs.count

    # computes no of workers needed, rounds to the next highest integer i.e. 5001 / 5000 = 2
    number_of_workers = (total_no_of_albums / MAX_RETRY_ALBUMS.to_f).ceil

    # evenly distribute number of albums across workers, rounds to next highest integer to account for non whole numbers
    (total_no_of_albums / number_of_workers.to_f).ceil
  end

  def mass_retry_params
    {
      job_name: job_name,
      person_id: person_id,
      album_ids_or_upcs: album_ids_or_upcs[0..last],
      store_ids: store_ids,
      metadata_only: metadata_only,
      remote_ip: remote_ip,
      use_sns_only: use_sns_only?,
      retry_in_batches: valid_batch?,
      daily_retry_limit: daily_retry_limit,
    }
  end

  def uuid
    @uuid ||= SecureRandom.uuid
  end

  def last
    if valid_batch?
      daily_retry_limit.to_i - 1
    else
      album_ids_or_upcs.count - 1
    end
  end

  def send_sns_notification?
    use_sns_only? || !retry_in_batches?
  end

  def use_sns_only?
    !!ActiveModel::Type::Boolean.new.cast(use_sns_only)
  end

  def retry_in_batches?
    !!ActiveModel::Type::Boolean.new.cast(retry_in_batches)
  end

  def valid_batch?
    retry_in_batches? && daily_retry_limit.to_i.positive?
  end

  def store_batch_in_redis
    key = job_name.present? ? (uuid.to_s + job_name) : uuid
    $redis.rpush("distribution:batch-retry-albums:#{uuid}", album_ids_or_upcs)
    $redis.hmset(
      "distribution:batch-retry:#{key}",
      "person_id",
      person_id,
      "store_ids",
      store_ids.join(","),
      "metadata_only",
      metadata_only,
      "daily_retry_limit",
      daily_retry_limit,
      "remote_ip",
      remote_ip,
      "current_index",
      0,
      "uuid",
      uuid,
      "job_name",
      job_name,
      "retry_in_batches",
      false,
      "use_sns_only",
      use_sns_only?
    )
  end
end
