module Distribution::Convertible
  def has_valid_converter_class?
    Delivery::AlbumConverter.new(self).has_valid_converter_class?
  end

  def convert(delivery_type)
    Delivery::AlbumConverter.new(self, delivery_type).convert
  end

  def converter
    Delivery::AlbumConverter.new(self).converter
  end

  def supports_takedown?
    Delivery::AlbumConverter.new(self).converter.supports_takedown?
  end
end
