module Distribution::Stateful
  STATES = [
    :new,
    :pending_approval,
    :enqueued,
    :dequeued,
    :gathering_assets,
    :packaged,
    :delivered,
    :error,
    :dismissed,
    :autoretry,
    :paused,
    :delivered_via_tc_distributor
  ]

  CAN_ENQUEUE = [:new, :error, :delivered, :pending_approval, :paused, :delivered_via_tc_distributor]
  DELIVERY_TYPE_METADATA_ONLY = "metadata_only".freeze
  SECONDARY_DELIVERY_TYPES = [DELIVERY_TYPE_METADATA_ONLY].freeze

  DELIVERED_BY_TC_WWW_STATE = "delivered".freeze
  DELIVERED_BY_TC_DISTRIBUTOR_STATE = "delivered_via_tc_distributor".freeze

  BLOCKED = [:blocked]

  def self.included(base)
    attr_accessor :now
    attr_reader   :state_changed
    attr_reader   :metadata

    base.has_many :transitions, as: :state_machine, dependent: :destroy, class_name: "::Transition"

    base.acts_as_state_machine initial: :new

    base.state :new
    base.state :pending_approval
    base.state :enqueued, enter: :enqueue_distribution
    base.state :paused
    base.state :dequeued
    base.state :gathering_assets
    base.state :packaged
    base.state :delivered
    base.state :delivered_via_tc_distributor
    base.state :error
    base.state :dismissed
    base.state :discarded
    base.state :blocked
    base.state :autoretry

    base.event(:pause)            { transitions from: STATES,      to: :paused           }
    base.event(:enqueue)          { transitions from: CAN_ENQUEUE, to: :enqueued         }
    base.event(:pending_approval) { transitions from: STATES,      to: :pending_approval }
    base.event(:errored)          { transitions from: STATES,      to: :error            }
    base.event(:dequeue)          { transitions from: STATES,      to: :dequeued         }
    base.event(:delivered)        { transitions from: STATES,      to: :delivered        }
    base.event(:retry)            { transitions from: STATES,      to: :enqueued         }
    base.event(:dismiss)          { transitions from: STATES,      to: :dismissed        }
    base.event(:discard)          { transitions from: STATES,      to: :discarded        }
    base.event(:block)            { transitions from: STATES,      to: :blocked          }
    base.event(:autoretry)        { transitions from: STATES,      to: :autoretry        }
    base.event(:unblock)          { transitions from: BLOCKED,     to: :new              }
    base.event(:renew)            { transitions from: STATES,      to: :new              }
    base.event(:tc_distributor)   { transitions from: STATES,      to: :delivered_via_tc_distributor }

    base.extend(ClassMethods)
  end

  module ClassMethods
    # Returns all errored distributions
    def find_errors
      where(state: "error")
    end

    def valid_delivery_types
      ["full_delivery", "metadata_only", "metadata_with_assets"]
    end

    def valid_states
      STATES + [:blocked]
    end
  end

  def processing?
    ["new", "dequeued", "enqueued", "gathering_assets", "packaged", "requested"].member?(state)
  end

  def enqueuable?
    state.to_sym.in?(CAN_ENQUEUE)
  end

  def has_been_delivered?
    @has_been_delivered ||=
      transitions.where(new_state: [DELIVERED_BY_TC_WWW_STATE, DELIVERED_BY_TC_DISTRIBUTOR_STATE]).any?
  end

  def has_been_delivered_by_tc_www?
    @has_been_delivered_by_tc_www ||= transitions.where(new_state: DELIVERED_BY_TC_WWW_STATE).any?
  end

  def takedown?
    return false if is_a?(TrackMonetization)

    delivery_type.in?(SECONDARY_DELIVERY_TYPES) &&
      (album.takedown? ||
      (respond_to?(:salepoints) && salepoints.find_by(store_id: store.id)&.taken_down?)
      )
  end

  # :retry is CALLED BY THE ADMIN API
  # :start is CALLED BY THE PETRI BUNDLE
  def start(opts)
    return false unless store.store_delivery_config

    tc_distributor_start! if instance_of?(TrackMonetization)

    opts[:delivery_action] ||= __method__
    if delivered_by_tc_distributor?
      delivered_by_tc_distributor(opts.merge(message: "delivered by tc-distributor"))
      return
    end
    return secondary_delivery(opts) if takedown?

    primary_delivery(opts)
  end
  alias_method :retry, :start

  def secondary_delivery(opts)
    return primary_delivery(opts) if has_been_delivered?

    state_update(opts) do
      @state_changed = discard!
    end
  end

  def primary_delivery(opts)
    delivery_action = opts.delete(:delivery_action)
    if store.paused? && delivery_action == :start
      pause(opts.merge(message: opts[:message].gsub("enqueue", "pause")))
    else
      state_update(opts) do
        case delivery_action
        when :start
          @state_changed = enqueue!
        when :retry
          self.retry_count = 0 if retry_count.blank?
          self.retry_count = retry_count + 1
          @state_changed = retry!
        end
      end
    end
  end

  def pause(opts)
    state_update(opts) do
      @state_changed = pause!
    end
  end

  def delivered_by_tc_distributor(opts)
    state_update(opts) do
      @state_changed = tc_distributor!
    end
  end

  def delivered_by_tc_distributor?
    (
      !instance_of?(TrackMonetization) &&
      tc_distributor_delivery?(:tc_distributor_album_cutover_users)
    ) ||
      (
        instance_of?(TrackMonetization) &&
        tc_distributor_delivery?(:tc_distributor_track_cutover_users)
      )
  end

  def tc_distributor_delivery?(tc_distributor_feature_flag)
    # rubocop:disable Style/GlobalVars
    store.delivered_by_tc_distributor? ||
      (
        store.delivered_by_tunecore? &&
        FeatureFlipper.show_feature?(tc_distributor_feature_flag, album.person.id) &&
        $redis.smembers(:TC_DISTRIBUTOR_CUTOVER_STORES).include?(store.id.to_s)
      )
    # rubocop:enable Style/GlobalVars
  end

  def dismiss(opts)
    state_update(opts) do
      @state_changed = dismiss!
    end
  end

  # CALLED BY THE PETRI WORKER
  def dequeue(opts)
    state_update(opts) do
      @state_changed = dequeue!
    end
  end

  # CALLED BY THE PETRI WORKER
  def delivered(opts)
    state_update(opts) do
      @state_changed = delivered!
    end
  end

  # CALLED BY THE PETRI WORKER
  def errored(opts)
    state_update(opts) do
      @state_changed = errored!
    end
  end

  # CALLED BY THE PETRY WORKER TO AUTOMATICALLY RETRY
  def autoretry(opts)
    state_update(opts) do
      @state_changed = autoretry!
    end
  end

  # CALLED BY ADMIN
  def block(opts)
    state_update(opts) do
      @state_changed = block!
    end
  end

  def unblock(opts)
    state_update(opts) do
      @state_changed = unblock!
    end
  end

  def renew(opts)
    state_update(opts) do
      @state_changed = renew!
    end
  end

  def not_blocked?
    !blocked?
  end

  def log_and_ignore(_opts = {})
    Rails.logger.info("Skipping enqueue #{self.class.name.titleize} (#{id}) for album #{petri_bundle.album.id} because it has been blocked.")
    true
  end

  def state_update(params, &_block)
    begin
      audit(params) do
        yield
      end
    rescue => e
      raise e
    rescue NoMemoryError => e
      raise e
    rescue SystemExit => e
      raise e
    rescue StandardError => e
      audit(params.merge({ message: e.message, backtrace: e.backtrace.join(";") })) do
        errored!
      end
    end
    petri_bundle.distribution_updated(params) if respond_to?(:petri_bundle)
  end

  def enqueue_distribution
    queue = store.try(:store_delivery_config).try(:queue) || "delivery-default"
    Rails.logger.info("Sending #{self.class.name} #{id} to Worker on queue #{queue}")
    job_id = Delivery::DistributionWorker.set(queue: queue).perform_async(self.class.name, id, delivery_type)
    Rails.logger.info("Sent #{self.class.name} #{id} to Worker on queue #{queue} with job id of #{job_id}")
    job_id
  end

  def pending_approval(opts)
    state_update(opts) do
      @state_changed = pending_approval!
    end
  end

  def audit(params)
    old_state = state
    yield
    return unless @state_changed == true

    now = Time.now
    params = { actor: "Mystery Actor" }.merge(params)
    if backtrace = params[:backtrace]
      backtrace = backtrace.kind_of?(Array) ? backtrace.join(";") : backtrace
    end
    self.updated_at = now
    transitions.create(
      {
        actor: params[:actor],
        message: params[:message],
        backtrace: backtrace,
        old_state: old_state,
        new_state: state,
        created_at: now
      }
    )
  end
end
