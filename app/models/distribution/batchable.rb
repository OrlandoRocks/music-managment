module Distribution::Batchable
  extend ActiveSupport::Concern

  included do
    has_many :delivery_batch_items, as: :deliverable
    has_many :delivery_batches, through: :delivery_batch_items
  end
end
