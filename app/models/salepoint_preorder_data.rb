class SalepointPreorderData < ApplicationRecord
  belongs_to  :salepoint
  belongs_to  :preorder_purchase
  has_one     :purchase, as: :related
  has_many    :preorder_instant_grat_songs
  has_many    :songs, through: :preorder_instant_grat_songs
  belongs_to  :variable_price

  scope :paid, -> { where("paid_at IS NOT NULL") }
  scope :by_store,
        ->(store_short_name) {
          joins(:salepoint)
            .where(salepoints: { store_id: Store.find_by(short_name: store_short_name).id })
            .includes(:preorder_purchase)
        }

  delegate :store, to: :salepoint

  validates :salepoint_id, presence: true
  validates :salepoint_id, uniqueness: true
  validates :preorder_purchase_id, presence: true
  validate :type_of_album
  validate :store_of_salepoint

  before_create :prevent_duplicate_store_id
  before_destroy :check_enable_flag_on_preorder_purchase

  def valid_price_tier_prices
    variable_prices_stores = salepoint.store.variable_prices_stores.includes(:variable_price)
    variable_prices_stores.map { |variable_prices_store| variable_prices_store.variable_price.price.to_f }
  end

  def paid_post_proc
    paid!
  end

  def paid!
    update(paid_at: Time.now) if paid_at.blank?
  end

  def set_preorder_price_tier(preorder_album_price)
    variable_prices_store =
      if album_type == "Album"
        salepoint
          .store
          .variable_prices_stores
          .joins(:variable_price)
          .order("variable_prices.price")
          .find_by("variable_prices.price >= ?", preorder_album_price)
      else
        salepoint
          .store
          .variable_prices_stores
          .joins(:variable_price)
          .find_by("variable_prices.price = ?", 9.99)
      end

    update(variable_price_id: variable_prices_store.variable_price_id)
  end

  def update_grat_tracks(song_ids)
    if song_ids.present?
      PreorderInstantGratSong.where(
        "salepoint_preorder_data_id = ? AND song_id NOT IN (?)",
        id,
        song_ids
      ).destroy_all
    else
      PreorderInstantGratSong.where("salepoint_preorder_data_id = ?", id).destroy_all
    end

    song_ids.each do |song_id|
      grat_song = PreorderInstantGratSong.find_or_initialize_by(song_id: song_id)
      grat_song.salepoint_preorder_data = self
      grat_song.save
    end
  end

  def set_salepoint_album_tier
    if album_type == "Album"
      price = track_price * album_songs_count
      order = "variable_prices.price"

      if album_songs_count > 10 && preorder_price <= 9.99
        conditions = ["variable_prices.price = ?", 9.99]
      elsif album_songs_count > 10 && preorder_price > 9.99
        conditions = ["variable_prices.price >= ?", preorder_price]
      elsif preorder_instant_grat_songs.present?
        conditions = ["variable_prices.price < ?", price]
        order += " DESC"
      else
        conditions = ["variable_prices.price >= ?", price]
      end

      variable_prices_store =
        salepoint
        .store
        .variable_prices_stores
        .joins(:variable_price)
        .order(order)
        .find_by(conditions)
    else
      variable_prices_store =
        salepoint
        .store
        .variable_prices_stores
        .joins(:variable_price)
        .find_by("variable_prices.price = ?", 9.99)
    end

    salepoint.update(variable_price_id: variable_prices_store.variable_price_id)
  end

  def set_track_price(track_price)
    variable_prices_store =
      salepoint
      .store
      .variable_prices_stores
      .joins(:variable_price)
      .where("variable_prices.price = ?", track_price)
      .first

    salepoint.update(track_variable_price_id: variable_prices_store.variable_price_id)
  end

  def preorder_price
    return if variable_price_id.nil?

    preorder_price = variable_price.price
    max_price = track_price * album_songs_count
    if max_price < preorder_price && preorder_price - max_price < 1
      preorder_price = max_price
    end # this is to confirm that the preorder_price tier is set correctly for validations
    preorder_price.to_f
  end

  def track_price
    tp = salepoint.track_variable_price
    tp.price if tp
  end

  def track_price_display
    cw = preorder_country_website
    (cw.currency == "USD") ? track_price : salepoint.track_variable_price.price_by_country_website(cw)
  end

  def preorder_price_display
    cw = preorder_country_website
    (cw.currency == "USD") ? preorder_price : variable_price.price_by_country_website(cw)
  end

  def album_songs_count
    salepoint.salepointable.songs.count
  end

  def album_type
    salepoint.salepointable.album_type
  end

  def validate_grat_track_size
    instant_grat_tracks = preorder_instant_grat_songs

    if instant_grat_tracks.present?
      if album_songs_count >= 4
        instant_grat_tracks.count <= album_songs_count / 2
      else
        instant_grat_tracks.count.zero?
      end
    else
      true
    end
  end

  def validate_preorder_start_date
    !!start_date && start_date <= salepoint.salepointable.sale_date - 1.day && Date.today + 10.days <= start_date
  end

  def validate_preorder_pricing
    instant_grat_songs = preorder_instant_grat_songs

    if !variable_price_id
      return false
    elsif preorder_price > 15.99
      return false
    elsif album_type == "Album" && track_price * album_songs_count < preorder_price
      return false
    elsif album_songs_count > 2 && salepoint.salepointable.minimum_price_by_track_size > preorder_price
      return false
    elsif instant_grat_songs.present?
      if track_price * album_songs_count <= preorder_price
        return false
      elsif instant_grat_songs.count * track_price + 1.98 > preorder_price
        return false
      elsif !valid_price_tier_prices.include?(preorder_price)
        return false
      end
    end

    true
  end

  def preorder_data_errors
    errors = []
    errors << I18n.t("model.salepoint_preorder_data.preorder_data_errors_one") unless validate_preorder_start_date

    if Store.find_by(short_name: "iTunesWW") == store
      errors << I18n.t("model.salepoint_preorder_data.preorder_data_errors_two") unless validate_preorder_pricing
      errors << I18n.t("model.salepoint_preorder_data.preorder_data_errors_three") unless validate_grat_track_size
    end

    errors
  end

  def valid_preorder_data?
    validate_preorder_start_date && (store != Store.find_by(short_name: "iTunesWW") || (validate_preorder_pricing && validate_grat_track_size))
  end

  def preorder_data
    grat_track_ids = preorder_instant_grat_songs.map(&:song_id)
    if variable_price && variable_price_id != salepoint.variable_price_id
      preorder_price_tier = variable_price.price_code
    end
    {
      preorder_start_date: start_date,
      instant_grat_tracks: grat_track_ids,
      wholesale_price_tier: preorder_price_tier,
      preview_songs: preview_songs
    } if preorder_purchase.paid_at?
  end

  def admin_update(all, itunes = {})
    update(
      start_date: Date.strptime(all[:start_date], I18n.t(:parse_reg_slash_date_format)),
      preview_songs: all[:preview_songs].present?
    )
    if salepoint.store.short_name == "iTunesWW"
      update(variable_price_id: itunes[:preorder_price_id])
      salepoint.update(
        variable_price_id: itunes[:variable_price_id],
        track_variable_price_id: itunes[:track_variable_price_id]
      )
      update_grat_tracks(itunes[:instant_grat_songs] || [])
    end
  end

  def is_enabled?
    preorder_purchase.send(PreorderPurchase::STORE_NAME_TO_ENABLE_FLAG[store.short_name])
  end

  private

  def type_of_album
    if salepoint && salepoint.salepointable.respond_to?(:type_supports_preorder?) && !salepoint.salepointable.type_supports_preorder?
      errors.add(:salepoint, I18n.t("model.salepoint_preorder_data.not_supported_type"))
    end
  end

  def store_of_salepoint
    store = Store.where(["short_name in (?)", ["iTunesWW", "Google"]])
    if salepoint && !store.include?(salepoint.store)
      errors.add(:salepoint, I18n.t("model.salepoint_preorder_data.not_supported_store"))
    end
  end

  def prevent_duplicate_store_id
    all_preorder_data = SalepointPreorderData.where(preorder_purchase_id: preorder_purchase_id)
    if all_preorder_data.any? { |d| d.salepoint.store_id == salepoint.store_id }
      errors.add(:base, I18n.t("model.salepoint_preorder_data.cant_have_dups"))
    end
  end

  # This method needs to be updated if another store is added for preorder
  def check_enable_flag_on_preorder_purchase
    store_name = salepoint.store.short_name

    case store_name
    when "Google"
      preorder_purchase.update_attribute(:google_enabled, false)
    when "iTunesWW"
      preorder_purchase.update_attribute(:itunes_enabled, false)
    end
  end

  def preorder_country_website
    salepoint.salepointable.person.country_website
  end
end
