class YoutubeSrMailTemplate::EmailBuilder
  attr_reader :processed_people, :processed_songs, :template, :email_data, :counter

  def initialize(template, email_data)
    @template             = template
    @email_data           = email_data
    @processed_people     = []
    @processed_songs      = []
    @counter              = 0
  end

  def process
    email_data.each do |person_id, data|
      message_data = []
      if data.delete("send_email") == "true"
        data.each do |song_id, custom_fields|
          message_data << {
            song_id: song_id,
            custom_fields: custom_fields
          }

          processed_songs << song_id
        end
      end
      send_to_mailer(person_id, message_data)
      processed_people << person_id
      @counter += 1
    end
    process_completed?
  end

  def processed_isrcs
    Song.find(processed_songs).map(&:isrc)
  end

  private

  def process_completed?
    counter == processed_people.size
  end

  def send_to_mailer(person_id, message_data)
    Ytsr::EmailWorker.perform_async(person_id, template.template_name, message_data)
  end
end
