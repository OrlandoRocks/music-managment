# frozen_string_literal: true

class Dispute < ApplicationRecord
  belongs_to :source, polymorphic: true
  belongs_to :processed_by, class_name: "Person"
  belongs_to :dispute_status
  belongs_to :person
  belongs_to :invoice
  belongs_to :refund, optional: true
  belongs_to :foreign_exchange_rate, optional: true

  has_many :dispute_evidences, dependent: :destroy

  before_validation :set_invoice_and_person, if: -> { source.present? }

  validates :source, :dispute_status, :person, :invoice,
            :transaction_identifier, :dispute_identifier, :reason,
            :amount_cents, :currency, :initiated_at, presence: true

  validates :dispute_identifier, uniqueness: {
    scope: %i[source_id source_type]
  }

  after_commit { DisputeAudit.create(dispute: self, note: change_in_status) }
  after_commit :create_refund, if: :create_refund?

  scope :refunded, -> {
    joins(:dispute_status)
      .where.not(refund_id: nil)
      .where(
        dispute_statuses: {
          status: DisputeStatus::REFUNDABLE_DISPUTE_STATUSES
        }
      )
  }

  def refundable?
    dispute_status.refundable? &&
      refund.blank? &&
      (maximum_chargeback_amount_cents - amount_cents) >= 0
  end

  def refunded?
    dispute_status.refundable? && refund.present?
  end

  def maximum_chargeback_amount_cents
    Disputes::QueryObjects::MaxChargebackAmount
      .new
      .for_dispute_id(id)
      .max_chargeback_amount_cents
  end

  private

  def change_in_status
    saved_changes.slice(:dispute_status_id)
  end

  def set_invoice_and_person
    self.invoice = source.invoice
    self.person = source.person
  end

  def create_refund?
    return false unless person.chargebacks_feature_on?

    refundable? && saved_changes.key?(:dispute_status_id)
  end

  def create_refund
    AutoRefunds::DisputeRefund.new(self).process
  end
end
