class Publisher < ApplicationRecord
  include PublishingAdministrationHelper

  has_many :composers
  has_many :publishing_composers

  belongs_to :performing_rights_organization

  before_validation :clean_cae, if: proc { |record| record.cae.present? }

  validates :name, presence: { if: proc { |record| record.cae.present? } }
  validates :cae, presence: { if: proc { |record| record.name.present? } }
  validates :performing_rights_organization_id, presence: true

  validates :cae, length: { in: 9..11, allow_blank: true }
  validates :name, length: { maximum: 100, allow_nil: true }

  def clean_cae
    # Ignore first character (can be any character)
    # Clean (remove space, dash, symbols, alpha characters) after the first character
    cae.strip!
    self.cae = (cae[0..0] + cae[1..cae.size].gsub(/[^0-9]/, "")) if cae.present? and cae.size > 1
  end

  def understandable_error_messages
    # Overriding errors method so attributes name will make sense for customers
    list_of_messages = []
    errors.each do |attr, msg|
      list_of_messages << case attr.to_s
                          when "cae"
                            "Publishing CAE/IPN number #{msg}"
                          when "name"
                            "Publishing entity name #{msg}"
                          when "performing_rights_organization_id"
                            "Publishing affiliation #{msg}"
                          else
                            "#{attr.to_s.humanize} #{msg}"
                          end
    end
    list_of_messages
  end

  def pro_affiliation
    performing_rights_organization.try(:name)
  end

  def name_with_pro
    "#{name} (#{performing_rights_organization&.name})"
  end
end
