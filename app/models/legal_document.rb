class LegalDocument < ApplicationRecord
  belongs_to :document_template
  belongs_to :person
  belongs_to :subject, polymorphic: true

  DOCUMENT_CONFIG = {
    s3_credentials: {
      access_key_id: DOCUMENT_ACCESS_KEY_ID,
      secret_access_key: DOCUMENT_SECRET_ACCESS_KEY
    },
    bucket: DOCUMENT_BUCKET,
    storage: :s3,
    s3_permissions: "private".freeze
  }.freeze

  DOCUMENT_STORAGE_CLIENT = AWS::S3.new(
    access_key_id: DOCUMENT_ACCESS_KEY_ID,
    secret_access_key: DOCUMENT_SECRET_ACCESS_KEY
  )

  enum status: {
    sent: "sent",
    completed: "completed",
    voided: "voided"
  }

  has_attached_file :asset, DOCUMENT_CONFIG

  validates_attachment :asset, content_type: { content_type: ["application/pdf"] }

  before_create :set_external_uuid

  def set_external_uuid
    self.external_uuid = SecureRandom.uuid
  end

  def document_url
    "LegalDocument::#{provider}".constantize.new(
      self, DOCUMENT_STORAGE_CLIENT.buckets[DOCUMENT_BUCKET]
    ).document_url
  end
end
