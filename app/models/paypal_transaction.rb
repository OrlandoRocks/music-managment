class PaypalTransaction < ApplicationRecord
  ### REQUIRES / EXTENDS / INCLUDES ###
  require "paypal_sdk/profile"
  require "paypal_sdk/paypal_api"
  require "paypal_sdk/paypal_utils"
  extend PayPalSDKUtils

  has_many :stored_paypal_accounts
  has_many :invoice_logs

  belongs_to :referenced_paypal_account, class_name: "StoredPaypalAccount"
  belongs_to :invoice
  belongs_to :person
  belongs_to :country_website
  belongs_to :payin_provider_config

  EXPRESS_CHECKOUT_URL = PayPalSDKProfiles::Profile.PAYPAL_EC_URL
  DIGITAL_GOODS_URL = PayPalSDKProfiles::Profile.PAYPAL_DG_URL
  DEV_URL = PayPalSDKProfiles::Profile.DEV_CENTRAL_URL
  CHECKOUT_RETURN_URL = PayPalSDKProfiles::Profile.checkout_return_url
  CHECKOUT_CANCEL_URL = PayPalSDKProfiles::Profile.checkout_cancel_url
  STORED_RETURN_URL = PayPalSDKProfiles::Profile.stored_return_url
  STORED_CANCEL_URL = PayPalSDKProfiles::Profile.stored_cancel_url

  VALID_ACTIONS = { sale: "Sale", authorization: "Authorization", refund: "Refund", void: "Void" }

  validates :country_website_id, :person_id, presence: true
  before_create :mark_failure
  after_create :log_transaction, if: ->(transaction) { transaction.action == "Sale" }
  after_create :set_error_message, if: ->(transaction) { transaction.ack == "Failure" }

  def success?
    ack != "Failure"
  end

  def completed?
    success? && status == "Completed"
  end

  def sale?
    action == "Sale"
  end

  def refund?
    action == "Refund"
  end

  def payin_provider_config
    self[:payin_provider_config] || person.paypal_payin_provider_config
  end

  ### CLASS METHODS ###

  def self.process_reference_transaction(stored_paypal_account, invoice)
    # Send a reference transaction to paypal
    country_website = stored_paypal_account.country_website
    payin_config = stored_paypal_account.payin_provider_config

    transaction = PayPalAPI.reference_transaction(payin_config, stored_paypal_account.billing_agreement_id, invoice.currency, invoice.items_for_paypal)

    paypal_transaction = create(
      invoice: invoice,
      country_website: country_website,
      person_id: invoice.person_id,
      email: stored_paypal_account.email,
      action: "Sale",
      referenced_paypal_account: stored_paypal_account,
      ack: transaction.response["ACK"].to_s,
      status: transaction.response["PAYMENTSTATUS"].to_s,
      amount: transaction.response["AMT"].to_s,
      billing_agreement_id: transaction.response["BILLINGAGREEMENTID"].to_s,
      transaction_id: transaction.response["TRANSACTIONID"].to_s,
      transaction_type: transaction.response["TRANSACTIONTYPE"].to_s,
      fee: transaction.response["FEEAMT"].to_s,
      pending_reason: transaction.response["PENDINGREASON"].to_s,
      error_code: transaction.response["REASONCODE"].to_s,
      currency: transaction.response["CURRENCYCODE"].to_s,
      raw_response: hash2cgiString(transaction.response),
      payin_provider_config: payin_config
    )

    # settle the invoice if transaction was succesful
    begin
      paypal_transaction.settle_invoice
    rescue => e
      Airbrake.notify(e)
      Rails.logger.error "PaypalTransaction#process_reference_transaction Failed: #{e.message}"
      Rails.logger.error e.backtrace
    end
    paypal_transaction
  end

  def self.search_transactions(conditions, options = {})
    select("pp.id, pp.invoice_id, pp.status, pp.transaction_id, pp.amount, pp.updated_at, p.name, pp.person_id, pp.action, pp.currency, pp.country_website_id")
      .joins("pp INNER JOIN people p ON pp.person_id=p.id")
      .where(conditions)
      .order(options[:sort] || "updated_at DESC")
      .paginate(page: options[:page], per_page: (options[:per_page] || 10).to_i)
  end

  # Helper method for auto refunds tool
  def self.process_auto_refund!(refund, amount, invoice_settlement)
    # Amount should be in dollars for paypal.
    amount /= 100.0
    original_transaction = invoice_settlement.source
    paypal_payin_config = original_transaction.person.paypal_payin_provider_config
    raise AutoRefunds::Utils::SettlementFailed.new("Missing Paypal Configuration") if paypal_payin_config.nil?

    refunded_transaction = PayPalAPI.partial_refund(paypal_payin_config, original_transaction.transaction_id, amount, refund.label)
    transaction = record_refunded_transaction(refunded_transaction, original_transaction, amount)

    unless transaction.success?
      raise AutoRefunds::Utils::SettlementFailed.new("Paypal Transaction Failed: #{transaction.error_code_message}")
    end

    transaction
  end

  ### OBJECT METHODS ###

  # legacy conversion to cents
  def amount_cents
    Tunecore::Numbers.decimal_to_cents(amount)
  end

  def settle_invoice
    return unless completed? && sale? && !invoice.nil? && !invoice.settled?

    invoice.settlement_received(self, amount_cents)
    invoice.settled! if invoice.can_settle?
  end

  def payment_amount
    amount
  end

  def error_code_message
    case raw_response
    when /L_ERRORCODE0=10201/
      "Whoops! Looks like your saved PayPal account is no longer valid with us."
    when /L_ERRORCODE0=10417/
      "PayPal has returned the following message: PayPal could not complete the transaction, please instruct the customer to use an alternative payment method."
    when /L_ERRORCODE0=10007/
      "You do not have permission to refund this transaction"
    else
      nil
    end
  end

  def self.record_refunded_transaction(transaction, original_transaction, amount = nil)
    create(
      ack: transaction.response["ACK"].to_s,
      action: "Refund",
      amount: amount || original_transaction.amount,
      country_website: original_transaction.country_website,
      currency: original_transaction.currency,
      email: original_transaction.email,
      invoice_id: original_transaction.invoice_id,
      person_id: original_transaction.person_id,
      raw_response: hash2cgiString(transaction.response),
      referenced_paypal_account_id: original_transaction.referenced_paypal_account_id,
      status: (transaction.response["ACK"].to_s == "Success" ? "Complete" : ""),
      transaction_id: transaction.response["REFUNDTRANSACTIONID"].to_s,
      payin_provider_config: original_transaction.payin_provider_config
    )
  end

  private

  ### VALIDATION METHODS ###

  ### CALLBACK METHODS ###

  # preserving the format of the paypal_ipn logging so that splunk and other monitoring tools do not need to be adjusted.
  def log_transaction
    begin
      logged_action = "PayPal Transaction |"
      logged_action += " transaction_id=#{transaction_id} " if transaction_id
      logged_action += " amount=#{amount} " if amount
      logged_action += " person_id=#{person_id} " if person_id
      logged_action += " status=#{status} " if status
      logged_action += " response_code=100 " if status == "Completed"
      logged_action += " currency=#{currency} " if currency
      logger.info logged_action
      logged_action
    rescue
    end
  end

  def mark_failure
    self.status = "Failed" if ack == "Failure" && status.empty?
  end

  def set_error_message
    errors.add(:base, error_code_message) if error_code_message.present?
  end

  ### OTHER PRIVATE METHODS ###
  # TODO remove this once we're sure StoredPaypalAccount can handle all of the auth stuff
  def self.record_voided_transaction(transaction, original_transaction)
    create(
      person_id: original_transaction.person_id,
      country_website: original_transaction.country_website,
      email: original_transaction.email,
      action: "Void",
      ack: transaction.response["ACK"].to_s,
      status: ((transaction.response["ACK"].to_s == "Success") ? "Complete" : ""),
      amount: original_transaction.amount,
      transaction_id: transaction.response["AUTHORIZATIONID"].to_s,
      currency: original_transaction.currency,
      raw_response: hash2cgiString(transaction.response),
      payin_provider_config: original_transaction.payin_provider_config
    )
  end
end
