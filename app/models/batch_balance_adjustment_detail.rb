class BatchBalanceAdjustmentDetail < ApplicationRecord
  belongs_to :batch_balance_adjustment
  belongs_to :balance_adjustment

  validate :valid_hold_type

  def posted?
    status == "posted"
  end

  def payment_held?
    !!(hold_payment =~ /(Y|Yes)$/i)
  end

  private

  def valid_hold_type
    return unless payment_held? && !RoyaltyPayment::HOLD_TYPES.value?(hold_type)

    errors.add :hold_type, "Invalid value of #{hold_type}. Valid values are #{RoyaltyPayment::HOLD_TYPES.values.join(',')}" and return false
  end
end
