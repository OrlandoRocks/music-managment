# frozen_string_literal: true

class RoyaltySplitRecipient < ApplicationRecord
  include RoyaltySplitLoggable
  before_validation :downcase_email
  before_validation :convert_existing_email_to_person
  after_create :automatically_accept_owner_recipients!
  before_destroy :check_cannot_destroy_split_owner, unless: :destroyed_by_association

  belongs_to :royalty_split, inverse_of: :recipients
  belongs_to :person
  has_many :songs, through: :royalty_split, inverse_of: :royalty_split_recipients
  has_many :albums, -> { distinct }, through: :songs, inverse_of: :royalty_split_recipients

  validates :percent, presence: true, numericality: { greater_than_or_equal_to: 0.0, less_than_or_equal_to: 100.0 }
  validates :person, presence: true, allow_nil: true
  validates :person_id, allow_nil: true, uniqueness: { scope: :royalty_split_id }
  validates :email, presence: true, allow_nil: true, uniqueness: { scope: :royalty_split_id }
  validates :royalty_split, presence: true

  validate :only_one_of_either_person_or_email
  validate :only_owner_allows_zero_percent

  scope :collaborators, -> {
                          joins(:royalty_split).where("royalty_splits.owner_id != royalty_split_recipients.person_id")
                        }
  scope :pending_invites, -> { where(accepted_at: nil) }

  def description
    sprintf(
      "%s (%.1f%%)",
      person.present? ? person.first_initial_and_last_name : email.truncate(10),
      percent.floor(2)
    ).html_safe
  end

  def display_email
    email || person&.email
  end

  def owner?
    person? && person == royalty_split&.owner
  end

  def person?
    person.present?
  end

  def active?
    deactivated_at.nil? && !accepted_at.nil?
  end

  def convert_existing_email_to_person
    return if person?
    return if email.blank?

    person_from_email = Person.find_by(email: email)
    return if person_from_email.blank?

    self.email = nil
    self.person = person_from_email
  end

  private

  def downcase_email
    self.email = email.downcase if email.present?
  end

  def only_one_of_either_person_or_email
    errors.add(
      :email,
      I18n.t("royalty_splits.must_be_blank_when_person_id_exists")
    ) if email.present? && person.present?
    errors.add(:email, :blank) if email.nil? && person.nil?
  end

  def automatically_accept_owner_recipients!
    return unless owner?

    update(accepted_at: created_at)
  end

  def only_owner_allows_zero_percent
    return if owner?
    return if percent.nil?

    errors.add(:percent, I18n.t("royalty_splits.cannot_be_zero_if_not_mah")) if percent.zero?
  end

  def check_cannot_destroy_split_owner
    throw :abort if owner?
  end

  def was_changed_royalty_split_loggable_events
    RoyaltySplits::EventService.recipient_changed_percent(
      self, from: previous_changes[:percent].first
    ) if saved_change_to_percent?
    RoyaltySplits::EventService.recipient_deactivated self if saved_change_to_deactivated_at?
  end
end
