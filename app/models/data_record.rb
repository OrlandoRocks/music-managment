# == Schema Information
# Schema version: 404
#
# Table name: data_records
#
#  id                    :integer(11)     not null, primary key
#  created_at            :datetime        not null
#  data_report_id        :integer(11)     not null
#  resolution_unit       :string(10)      not null
#  resolution_identifier :string(20)      not null
#  measure               :float           not null
#  description           :string(150)     not null
#  resolution_display    :string(30)
#

class DataRecord < ApplicationRecord
  belongs_to :data_report

  # setting this comparison allows you to call sort on a collection of records that have the same resolution and have them sorted by id
  # which means that they will be sorted in the order that they are written.
  def <=>(comp)
    id <=> comp.id
  end
end
