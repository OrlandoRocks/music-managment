# = Description
# This class is used as a base for ProductReport, ProductSalesOverviewReport, and RenewalReport. It creates an interface
# through which a single query is issued to the database to pull back report data.  It is then grouped or ungrouped
# and returned back to the interface.  There is no corresponding database tables for this report, it simply wraps the call to the database.
# This construction allows us to build complex queries into a single call.
#
# = Usage / Options
# * report_type
# * resolution
# * range
# * grouped_by -
# * headers - If set, will override the creation of headers based on passed in fields.
# * data_field_name - ReportDataTable uses this field to group the data when assembling the rows for display.
# * chart_type -  Set a chart type from one of the constants listed below if this dataset can be charted. This will tell the
#                 Raphael library how to render the graph.
# * include_total_and_avg - Set to true if you want each row to have an average and total appended to the end of the column.
# * data_is_flat -  set to true if the data coming out of the database is already in the shape you want it in. This stop the report
#                   from attempting to group it on the data_field_name above.  See person_intake_report for an example.
#
#
# = Change Log
# [2010-02-04 -- CH]
# Created Class

class ReportBase
  # report types
  CSV       = "csv"
  GROUPED   = "grouped"
  UNGROUPED = "ungrouped"
  FLAT      = "flat"

  # report resolutions
  DAILY   = "day"
  MONTHLY = "month"
  WEEKLY  = "week"
  YEAR    = "year"

  BAR_CHART   = "Bar"
  LINE_CHART  = "Line"
  HBAR_CHART  = "HorizontalBar"

  GROUPED_BY_RESOLUTION = "resolution"
  GROUPED_BY_PRODUCT    = "product"

  DAILY_RANGE_OPTIONS = [
    ["Last 7 Days", "last_7_days"],
    ["Last 2 Weeks", "last_2_weeks"],
    ["This Month", "this_month"],
    ["Last 30 Days", "last_30_days"],
    ["Custom Date Range", "custom"]
  ]

  MONTHLY_RANGE_OPTIONS = [
    ["This Month", "this_month"],
    ["Last 2 Months", "last_2_months"],
    ["Last 3 Months", "last_3_months"],
    ["Last 6 Months", "last_6_months"],
    ["Last Year", "last_year"],
    ["Custom Date Range", "custom"]
  ]

  WEEKLY_RANGE_OPTIONS = [
    ["This Week", "this_week"],
    ["Last Two Weeks", "last_2_weeks"],
    ["This Month", "this_month"],
    ["Last 6 Weeks", "last_6_weeks"],
    ["Last 3 Months", "last_3_months"],
    ["Custom Date Range", "custom"]
  ]

  attr_accessor :report_type, :resolution, :grouped_by
  attr_accessor :range, :start_date, :end_date, :fields, :custom_dates
  attr_accessor :resolution_date_format, :group_by_date_format
  attr_accessor :headers, :data, :data_table, :raw_data, :data_field_name
  attr_accessor :include_total_and_avg, :data_is_flat
  attr_accessor :chart_type, :country_website_id

  def initialize(options)
    self.report_type  = options[:report_type]
    self.resolution   = options[:resolution]
    self.range        = options[:range]
    self.grouped_by   = options[:grouped_by]
    self.custom_dates = options[:custom_dates]
    self.country_website_id = options[:country_website_id]
    self.headers = options[:headers] || nil
    self.data_field_name = options[:data_field_name] || "resolution"
    self.chart_type = options[:chart_type]
    self.include_total_and_avg =
      if options[:include_total_and_avg].nil?
        true
      else
        options[:include_total_and_avg]
      end
    self.data_is_flat = options[:data_is_flat] || false

    generate_report
  end

  def can_chart?
    chart_type != nil
  end

  protected

  def retrieve_data
    raise "retrieve_data should be set in the subclass."
  end

  def prepare_data
    if report_type == GROUPED
      case grouped_by
      when GROUPED_BY_PRODUCT
        self.raw_data = raw_data.group_by(&:id)
      when GROUPED_BY_RESOLUTION
        self.raw_data = raw_data.group_by(&:resolution)
      end
    end
    self.data_table = ReportDataTable.new(
      {
        headers: headers,
        raw_data: raw_data,
        fields: fields,
        include_row_title: true,
        include_total_and_avg: include_total_and_avg,
        data_field_name: data_field_name,
        data_is_flat: data_is_flat
      }
    )
  end

  private

  def generate_report
    create_date_format_strings
    self.raw_data = retrieve_data
    prepare_headers
    prepare_data
  end

  # default headers to submitted date resolutions
  def prepare_headers
    self.headers = raw_data.map(&:resolution).uniq if headers.nil?
  end

  def build_condition_array
    make_date_conditions
    ["paid_at >= ? and paid_at <= ?", start_date, end_date]
  end

  def create_date_format_strings
    case resolution
    when DAILY
      self.resolution_date_format = "%b %e, %Y"
      self.group_by_date_format = "%b %e, %Y"
    when MONTHLY
      self.resolution_date_format = "%M %Y"
      self.group_by_date_format = "%M %Y"
    when WEEKLY
      self.resolution_date_format = "%b %e, %Y"
      self.group_by_date_format = "%v - %x"
    end
  end

  def make_date_conditions
    self.end_date      = generate_end_date
    self.start_date    = generate_start_date
  end

  def generate_end_date
    case resolution
    when DAILY
      Time.now
    when MONTHLY
      Time.now.end_of_month
    when WEEKLY
      Time.now.end_of_week
    end
  end

  def generate_start_date
    case resolution
    when DAILY
      generate_start_date_for_daily
    when MONTHLY
      generate_start_date_for_monthly
    when WEEKLY
      generate_start_date_for_weekly
    end
  end

  def generate_start_date_for_daily
    case range
    when "last_7_days"
      self.start_date = end_date - 6.days
    when "last_2_weeks"
      self.start_date = end_date - 2.weeks
    when "this_month"
      self.start_date = end_date.beginning_of_month
    when "last_30_days"
      self.start_date = end_date - 30.days
    when "custom"
      self.start_date = custom_dates[:start_date] rescue nil
      self.end_date = custom_dates[:end_date] rescue nil
    end

    self.start_date = start_date.to_time.at_midnight
  end

  def generate_start_date_for_weekly
    case range
    when "this_week"
      self.start_date = end_date - 1.week
    when "last_2_weeks"
      self.start_date = end_date - 2.weeks
    when "last_3_weeks"
      self.start_date = end_date - 3.weeks
    when "this_month"
      self.start_date = end_date - 1.month
    when "last_6_weeks"
      self.start_date = end_date - 6.weeks
    when "last_3_months"
      self.start_date = end_date - 3.months
    when "custom"
      self.start_date = custom_dates[:start_date] rescue nil
      self.end_date = custom_dates[:end_date] rescue nil
    end

    self.start_date = start_date.beginning_of_week.to_time.at_midnight
  end

  def generate_start_date_for_monthly
    case range
    when "this_month"
      self.start_date = end_date
    when "last_2_months"
      self.start_date = end_date - 1.month
    when "last_3_months"
      self.start_date = end_date - 3.months
    when "last_6_months"
      self.start_date = end_date - 6.months
    when "this_year"
      self.start_date = end_date.beginning_of_year
    when "last_year"
      self.start_date = end_date - 1.year
    when "custom"
      self.start_date = custom_dates[:start_date] rescue nil
      self.end_date = custom_dates[:end_date] rescue nil
    end

    self.start_date = start_date.beginning_of_month.to_time.at_midnight
  end
end
