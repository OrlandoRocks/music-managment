class SalepointSubscription < ApplicationRecord
  class DestroyAfterUsed < StandardError; end

  #
  # Associations
  #

  belongs_to  :album
  has_one     :person, through: :album
  has_one     :purchase, as: :related, dependent: :destroy

  #
  # Validations
  #

  validates   :album, presence: true
  validates :album_id, uniqueness: true
  validate :type_of_album

  #
  # Delegations
  #

  delegate :person_id, to: :album

  scope :active_and_album_approved_or_pending, -> { joins(:album).merge(Album.approved_or_pending_approval).where(is_active: true) }

  scope :active_subscriptions_for_new_store,
        ->(store, limit) {
          select("distinct salepoint_subscriptions.*")
            .joins(<<-SQL.strip_heredoc)
        INNER JOIN albums ON albums.id = salepoint_subscriptions.album_id
        LEFT JOIN (
          SELECT albums.id FROM albums
            INNER JOIN salepoints
              ON salepointable_id = albums.id
              AND salepointable_type = 'Album'
            WHERE store_id = #{store.id}
        ) excluded_albums
          ON albums.id = excluded_albums.id
            SQL
            .where(<<-SQL.strip_heredoc, (store.launched_at || Time.now))
        salepoint_subscriptions.finalized_at IS NOT NULL
          AND albums.finalized_at IS NOT NULL
          AND albums.takedown_at IS NULL
          AND albums.is_deleted = 0
          AND excluded_albums.id IS NULL
          AND salepoint_subscriptions.effective < ?
          AND salepoint_subscriptions.is_active = TRUE
            SQL
            .includes(:purchase)
            .limit(limit)
        }

  #
  # Class Methods
  #

  # Assumes array of albums supplied.
  # creates subscriptions using single sql statement,
  # therefore, validations will be skipped.
  #
  # returns that album_ids that subscriptions were
  # created for
  def self.create_batch_subscriptions(albums)
    sp_sub_values = []
    album_ids = []

    albums.each do |album|
      if album.salepoint_subscription.blank? and album.supports_subscriptions
        sp_sub_values << "(#{album.id}, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        album_ids << album.id
      end
    end

    if sp_sub_values.present?
      sql = "INSERT INTO salepoint_subscriptions (`album_id`, `created_at`, `updated_at`) VALUES #{sp_sub_values.join(', ')}"
      ActiveRecord::Base.connection.execute sql
    end

    album_ids
  end

  #
  # Instance Methods
  #

  def name
    to_s
  end

  def to_s
    "Store Automator for #{album.name}"
  end

  def finalized?
    finalized_at.present?
  end

  def active?
    is_active && finalized?
  end

  def can_distribute?
    if ((album.finalized? || Purchase.currently_in_cart?(person, album)) && album.can_distribute? && !finalized? && album.supports_subscriptions)
      true
    else
      false
    end
  end

  #
  # Callback made once a purchase is marked as paid, gives us a chance
  # to mark ourself as finalized.
  #
  def paid_post_proc
    finalize!
  end

  #
  # Override default setter to update effective when setting
  # active status to true. Ensure that status is actually
  # changing to prevent updating the effective date when
  # not needed
  #
  def is_active=(status)
    return unless is_active != status

    self[:is_active] = status
    self.effective = current_time_from_proper_timezone if active?
  end

  def toggle_active!
    return false unless finalized? && person.can_toggle_automator?

    self.effective = nil if is_active

    self.is_active = !is_active
    save!
  end

  #
  # Marks self as finalized by setting finalized_at if not already set. This can be called
  # multiple times if the album has already been distributed and new salepoints are added, etc.,
  # so the check is important.
  #
  def finalize!
    return if finalized?

    attrs = { finalized_at: current_time_from_proper_timezone }
    if is_active
      attrs[:effective] = current_time_from_proper_timezone
    end # don't use active? since finalized_at not written yet
    update(attrs)
  end

  def destroy
    raise DestroyAfterUsed, "Cannot delete a purchased salepoint subscription" if finalized?

    super
  end

  # Add the store to the salepoint subscription and use the inventory
  def add_to_subscription(stores, initiate_distribution = true)
    salepoints_added, failed_stores = album.add_stores(stores)

    unless salepoints_added.empty?
      Rails.logger.info "Stores #{salepoints_added.collect(&:store).collect(&:id)} added to #{album.id}"
    end

    successful_inventory_usages = []
    salepoints_added.each do |salepoint|
      inventory_items = Inventory.use!(album.person, { item_to_use: salepoint, purchase: purchase, distribute: initiate_distribution })

      inventory_items.each do |inv_item|
        successful_inventory_usages << inv_item.inventory.inventory_usages.detect { |iu| iu.related_id == salepoint.id }
        inv_item.item&.update_post_purchase
      end
      if inventory_items.present?
        Rails.logger.info "Inventory.use! for salepoint=#{salepoint.id}; store=#{salepoint.store.id};"
      end
    end

    failed_stores.each do |store|
      Rails.logger.warn "Failed to add store=#{store.id} to album=#{album.id}"
    end

    successful_inventory_usages.flatten
  end

  private

  def type_of_album
    return unless album and !album.type_supports_subscription?

    errors.add(:album, I18n.t("model.salepoint_subscription.not_supported_type"))
  end
end
