class SubscriptionEvent < ApplicationRecord
  include Jsonable

  belongs_to :subscription_purchase
  belongs_to :person_subscription_status
  validates :event_type, :person_subscription_status, presence: true

  has_many :notes, -> { order "created_at DESC" }, as: :related

  def self.create_purchase_event(subscription_purchase)
    subscription_status = subscription_purchase.find_related_person_subscription_status(subscription_purchase.name)

    if subscription_status.blank?
      date_time = DateTime.now

      subscription_status = PersonSubscriptionStatus.create(
        subscription_type: subscription_purchase.name,
        person: subscription_purchase.person,
        effective_date: date_time,
        termination_date: subscription_purchase.next_termination_date(date_time),
        grace_period_length: DEFAULT_SUBSCRIPTION_GRACE_PERIOD
      )

      event_type = "Purchase"
    else
      event_type  = subscription_status.next_purchase_event_type
      date_time   = subscription_status.next_purchase_effective_start_date

      subscription_status.update(
        termination_date: subscription_purchase.next_termination_date(date_time),
        canceled_at: nil
      )
    end

    subscription_purchase.update(
      termination_date: subscription_status.termination_date,
      effective_date: date_time
    )

    create(
      event_type: event_type,
      person_subscription_status: subscription_status,
      subscription_purchase: subscription_purchase,
      subscription_type: subscription_purchase.name
    )
  end

  def as_json(options = {})
    new_attributes = subscription_purchase.present? ? { price: price } : {}

    merge_attributes_as_json(new_attributes, options)
  end

  private

  def price
    subscription_purchase.purchase.purchase_total_cents / BigDecimal("100").to_f
  end
end
