# = Description
# This model is used to store open authorization providers that our application supports.
#
# Currently there's only 1 OAuth provider- Souncloud with an ID of 1.
#
# = Change Log
# 2010-07-13 -- ED -- created

class OauthProvider < ApplicationRecord
  has_many :open_auths

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: true }
end
