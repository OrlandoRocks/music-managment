# frozen_string_literal: true

class CorporateEntity < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  has_many :persons
  has_many :payout_provider_configs
  has_many :countries

  TUNECORE_US_NAME = "TuneCore US"
  BI_LUXEMBOURG_NAME = "BI Luxembourg"

  BI_CREDIT_NOTE_INVOICE_PREFIX = "BI-CR-INV"
  TC_CREDIT_NOTE_INVOICE_PREFIX = "TC-CR-INV"

  def affiliated_to_bi?
    name == BI_LUXEMBOURG_NAME
  end
end
