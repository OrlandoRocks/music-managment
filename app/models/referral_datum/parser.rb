class ReferralDatum::Parser
  def initialize(parameters)
    parse(parameters)
  end

  attr_reader :parsed, :timestamp

  private

  def parse(parameters)
    parameters = JSON.parse(parameters) if valid_json?(parameters.to_s)
    @timestamp = handle_timestamp(parameters.delete("tc_timestamp"))
    @parsed    = filter_parameters(parameters)
  end

  def filter_parameters(parameters)
    if parameters.respond_to?(:select)
      parameters.select { |k, _v| ReferralDatum.allowed_parameters.include?(k.to_sym) }.with_indifferent_access
    else
      {}
    end
  end

  def handle_timestamp(timestamp = nil)
    Time.strptime(timestamp.to_s, "%s")
  rescue TypeError, ArgumentError
    Time.now
  end

  def valid_json?(json_string)
    true if JSON.parse(json_string)
  rescue JSON::ParserError
    false
  end
end
