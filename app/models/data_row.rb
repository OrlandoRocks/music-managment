# 2009-11-23 -- MJL -- Creating a data row model to help DataReport and DataRecord.
#                      This model allows [:key] and .get_item(key) access to a set of DataRecords
#
# 2009-12-01 -- MJL -- Return nil if a key is not present
# 2009-12-01 -- MJL -- Adding the each_pair method so we can cycle through the key/value pairs

class DataRow
  # Make sure that all of the data elements that are in the array are for the same day/resolution
  def initialize(data_records)
    raise "You must pass in an array" unless data_records.instance_of?(Array)
    raise "Your array cannot be empty" if data_records.empty?
    raise "Your array must contain at least one data record" unless data_records.all? { |x| x.instance_of?(DataRecord) }

    # Make sure that all resolution identifiers are the same, you don't want different days or weeks getting merged together.
    res_identifier = data_records.first.resolution_identifier
    unless data_records.all? { |x| x.resolution_identifier == res_identifier }
      raise "All resolution identifiers must be the same"
    end

    @data_records = data_records
  end

  def get_item(key)
    item = @data_records.detect { |x| x.description == key }
    if item
      item.measure
    else
      nil
    end
  end

  def [](key)
    get_item(key)
  end

  def each_pair
    @data_records.each do |record|
      yield record.description, record.measure
    end
  end
end
