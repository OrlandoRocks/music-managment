# == Schema Information
# Schema version: 404
#
# Table name: apple_identifiers
#
#  id                 :integer(11)     not null, primary key
#  created_at         :datetime        not null
#  duplicated_isrc_id :integer(11)     default(0), not null
#  apple_id           :string(50)
#

class AppleIdentifier < ApplicationRecord
  belongs_to :duplicated_isrc
  validates :apple_id, uniqueness: { scope: nil }
end
