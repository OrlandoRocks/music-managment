class TunecoreTracking < ApplicationRecord
  self.table_name = "tunecore_tracking"

  belongs_to :person
  validates :model_id, uniqueness: { scope: [:person_id, :model, :service] }
end
