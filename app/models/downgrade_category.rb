class DowngradeCategory < ApplicationRecord
  has_many :downgrade_category_reasons, dependent: :destroy
end
