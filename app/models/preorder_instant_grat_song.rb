class PreorderInstantGratSong < ApplicationRecord
  belongs_to :salepoint_preorder_data
  belongs_to :song

  validates :song, presence: true
  validates :salepoint_preorder_data, presence: true
  validate :song_preorder_albums_match
  validates :song_id, uniqueness: { scope: :salepoint_preorder_data_id }

  private

  def song_preorder_albums_match
    return if song.album_id == salepoint_preorder_data.salepoint.salepointable_id

    errors.add(:song, "does not match salepoint_preorder_data")
  end
end
