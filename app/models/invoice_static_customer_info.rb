# frozen_string_literal: true

class InvoiceStaticCustomerInfo < ApplicationRecord
  FIELDS = Set[
    "company_registry_data",
    "customer_type",
    "enterprise_number",
    "name",
    "person_currency",
    "place_of_legal_seat",
    "registered_share_capital",
    "vat_registration_number"
  ].freeze

  belongs_to :related, polymorphic: true

  validates :related_id, :related_type, presence: true
end
