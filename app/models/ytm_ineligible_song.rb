class YtmIneligibleSong < ApplicationRecord
  belongs_to  :song
  validates   :song, presence: true
  validates   :song_id, uniqueness: true
end
