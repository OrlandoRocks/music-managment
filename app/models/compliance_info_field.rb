# frozen_string_literal: true

class ComplianceInfoField < ApplicationRecord
  belongs_to :person

  validates :person,
            :field_name,
            :field_value,
            presence: true,
            if: -> { Array(validation_context).exclude?(:disable_validations) }

  validates :person, uniqueness: { scope: :field_name }
  validate :permitted_field_name?

  after_save :evaluate_vat_acceptance

  # keep alphabetical
  PERMITTED_FIELD_NAMES = Set[
    :company_name,
    :first_name,
    :last_name
  ]

  def self.set_value_for(person:, field_name:, field_value:)
    field = find_by(person: person, field_name: field_name)
    if field.present?
      field.update!(field_value: field_value)
      field
    else
      create!(
        person: person,
        field_name: field_name,
        field_value: field_value
      )
    end
  end

  def permitted_field_name?
    return if PERMITTED_FIELD_NAMES.member?(field_name.to_sym)

    *head, tail = PERMITTED_FIELD_NAMES.to_a
    msg = "Invalid field_name, #{field_name}! Permitted field_names are #{head.join(', ')} and #{tail}"
    errors.add(:field_name, msg)
  end

  private

  def evaluate_vat_acceptance
    person.evaluate_vat_acceptance
  end
end
