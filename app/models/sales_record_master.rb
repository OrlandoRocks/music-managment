class SalesRecordMaster < ApplicationRecord
  include SalesRecordMasterMethods

  has_many :sales_records

  belongs_to :sip_store
  belongs_to :country_website

  scope :summarized, ->(summarized) { where(summarized: summarized) }
end
