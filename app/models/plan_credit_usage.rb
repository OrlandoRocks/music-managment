# frozen_string_literal: true

class PlanCreditUsage < CreditUsage
  default_scope { plan_credit }

  scope :plan_credit, -> { where(plan_credit: true) }

  private

  # this is a stub method for now to overwrite CreditUsages method - eventually will add Plan logic to this
  # CreditUsage (which this model inherits from) runs a validation that checks whether the "credit"
  # is available in the Inventory. We will replace that with a check with on the Plan once we build out that logic,
  # but without this overwrite we wouldn't be able to add a PlanCreditUsage for testing purposes
  def ensure_credit_available
    true
  end
end
