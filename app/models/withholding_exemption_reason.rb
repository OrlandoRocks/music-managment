# frozen_string_literal: true

class WithholdingExemptionReason < ApplicationRecord
  has_many :withholding_exemptions, dependent: :destroy
  before_destroy :check_for_exemptions
  enum name: {
    contractual_or_business_decision: "Contractual or Business Decision",
    locked_account: "Locked Account",
    dormant_account: "Dormant Account",
    remitted_account: "Remitted Account",
    non_us_account: "Non US Account",
    pending_evaluation: "Pending Evaluation",
    other: "Other"
  }

  private

  def check_for_exemptions
    number_of_exemptions = withholding_exemptions.count
    return if number_of_exemptions.zero?

    errors.add_to_base(
      "cannot destroy WithholdingExemptionReason while #{number_of_exemptions} WithholdingExemption exist"
    )
    false
  end
end
