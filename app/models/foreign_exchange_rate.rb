# frozen_string_literal: true

class ForeignExchangeRate < ApplicationRecord
  validates :exchange_rate, :source_currency, :target_currency, :valid_from, presence: true
  before_create :update_previous_rate_valid_till

  # This map is needed because there is no table that links the country with its customer currency
  # country_websites table is supposed to have it. But for India site, since USD is the system's operating currency,
  # it has USD instead of INR.
  CUSTOMER_CURRENCY_OVERRIDES = {
    "IN" => "INR"
  }.freeze

  scope :latest,
        ->(currency) {
          where(target_currency: currency).order(valid_from: :desc)
        }

  scope :current_rates_by_currency, ->(source, target) do
    where(source_currency: source, target_currency: target)
      .order(valid_from: :desc)
  end

  scope :valid_ordered_rates_by_day, ->(date) do
    where(valid_from: date.beginning_of_day..date.end_of_day)
      .order(valid_from: :desc)
  end

  scope :historical_rates, ->(source, target, date) do
    current_rates_by_currency(source, target).valid_ordered_rates_by_day(date)
  end

  def self.current_exchange_rate(country_id)
    country_iso = Country.find(country_id).iso_code
    current_rate(iso: country_iso)
  end

  def self.current_rate(iso:)
    currency = CUSTOMER_CURRENCY_OVERRIDES[iso] || CountryWebsite.find_by(country: iso)&.currency
    latest(currency)&.first
  end

  def self.latest_by_currency(source:, target:)
    current_rates_by_currency(source, target).take
  end

  def self.latest_by_currency!(source:, target:)
    current_rates_by_currency(source, target).take!
  end

  def self.for_date(source:, target:, date:)
    historical_rates(source, target, date).take
  end

  def update_previous_rate_valid_till
    prev_rate = self.class
                    .where(source_currency: source_currency, target_currency: target_currency)
                    .order(valid_from: :desc)
                    .first
    prev_rate.update(valid_till: valid_from) if prev_rate && prev_rate.valid_till.blank?
  end
end
