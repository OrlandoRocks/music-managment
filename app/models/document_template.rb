class DocumentTemplate < ApplicationRecord
  TEMPLATE_CATEGORIES = ["publishing"].map(&:freeze).freeze
  LANGUAGE_CODES      = ["en", "fr", "it", "de"].map(&:freeze).freeze
  ROLES               = ["composer_without_publisher", "composer_with_publisher"].map(&:freeze).freeze
  LOD = "Letter of Direction"

  has_many :legal_documents

  has_paper_trail

  validates :template_id, presence: true
  validates :document_type, inclusion: { in: TEMPLATE_CATEGORIES }
  validates :language_code, inclusion: { in: LANGUAGE_CODES, allow_blank: true }
  validates :role,          inclusion: { in: ROLES, allow_blank: true }
end
