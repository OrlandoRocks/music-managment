class DisputeEvidence < ApplicationRecord
  has_attached_file :file_evidence
  belongs_to :dispute
  belongs_to :submitted_by, class_name: "Person"

  validates_attachment_file_name :file_evidence, matches: [/png\z/, /jpe?g\z/, /pdf\z/]

  after_commit { DisputeAudit.create(dispute: dispute, note: change_in_evidence) }

  def change_in_evidence
    saved_changes.slice(:text_evidence, :file_evidence_file_name)
  end
end
