class BlacklistedArtist < ApplicationRecord
  belongs_to :artist
  validates :artist, presence: true

  scope :by_artist, ->(artist_ids) { where(artist_id: artist_ids) }
  scope :active,    -> { where(active: true)  }
  scope :inactive,  -> { where(active: false) }

  def self.includes_scrubbed_artist_name?(scrubbed_artist_name)
    scrubbed_artist_name_trie.has_key?(scrubbed_artist_name)
  end

  def self.scrubbed_artist_name_trie
    @scrubbed_artist_name_trie ||= create_scrubbed_artist_name_trie
  end

  def self.create_scrubbed_artist_name_trie
    trie = Triez.new
    active.joins(:artist).pluck(:scrubbed_name).compact.each do |scrubbed_artist_name|
      trie << scrubbed_artist_name
    end
    trie
  end
  private_class_method :create_scrubbed_artist_name_trie
end
