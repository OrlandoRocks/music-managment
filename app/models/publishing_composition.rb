class PublishingComposition < ApplicationRecord
  has_paper_trail

  belongs_to :account

  has_many :publishing_composition_splits
  has_many :publishing_composers, through: :publishing_composition_splits
  has_many :primary_composers, -> { where(is_primary_composer: true) }, through: :publishing_composition_splits, source: :publishing_composer
  has_many :cowriting_composers, -> { where(is_primary_composer: false) }, through: :publishing_composition_splits, source: :publishing_composer
  has_many :unknown_composers, -> { where(is_unknown: false) }, through: :publishing_composition_splits, source: :publishing_composer

  has_many :songs
  has_many :muma_songs
  has_many :non_tunecore_songs
  has_many :rights_app_errors, as: :requestable
  has_many :recordings, dependent: :destroy

  has_many :transitions, as: :state_machine, dependent: :destroy

  before_save :update_state_transition
  before_save :nullify_provider_identifier
  acts_as_state_machine initial: :new

  state :new
  state :pending_distribution
  state :not_controlled
  state :draft
  state :split_submitted
  state :sent_for_processing
  state :sent_to_society
  state :accepted
  state :conflict
  state :rejected
  state :hidden
  state :terminated
  state :submitted
  state :resubmitted
  state :verified
  state :ineligible

  STATE_TO_STATUS =
    {
      new: "Split missing",
      draft: "Draft",
      split_submitted: "Split submitted",
      sent_for_processing: "Sent for processing",
      sent_to_society: "Sent for registration",
      accepted: "Registered",
      conflict: "Pending",
      rejected: "Rejected",
      not_controlled: "Not controlled",
      pending_distribution: "Not distributed yet",
      hidden: "Hidden",
      terminated: "Terminated",
      submitted: "Submitted",
      resubmitted: "Resubmitted",
      verified: "Accepted",
      ineligible: "Ineligible",
    }.freeze.with_indifferent_access

  scope :nu,                ->  { where(state: :new) }
  scope :pending,           ->  { where(state: :pending_distribution) }
  scope :not_controlled,    ->  { where(state: :not_controlled) }
  scope :draft,             ->  { where(state: :draft) }
  scope :submitted,         ->  { where(state: :split_submitted) }
  scope :processing,        ->  { where(state: :sent_for_processing) }
  scope :society,           ->  { where(state: :sent_to_society) }
  scope :accepted,          ->  { where(state: :accepted) }
  scope :conflicted,        ->  { where(state: :conflict) }
  scope :rejected,          ->  { where(state: :rejected) }
  scope :hidden,            ->  { where(state: :hidden) }
  scope :terminated,        ->  { where(state: :terminated) }
  scope :resubmitted,       ->  { where(state: :resubmitted) }
  scope :verified,          ->  { where(state: :verified) }

  event(:submit_split_without_validation) do
    transitions from: [:new, :pending_distribution], to: :split_submitted
  end

  event(:draft) do
    transitions from: STATE_TO_STATUS.keys, to: :draft
  end

  event(:submit_split) do
    transitions from: [:new, :pending_distribution, :not_controlled, :draft],
                to: :split_submitted
  end

  event(:send_for_processing) do
    transitions from: [:new, :split_submitted], to: :sent_for_processing
  end

  event(:send_to_society) do
    transitions from: [:new, :sent_for_processing], to: :sent_to_society
  end

  event(:accepted_by_society) do
    transitions from: [:new, :sent_for_processing, :sent_to_society, :rejected, :conflict, :terminated], to: :accepted
  end

  event(:rejected_by_society) do
    transitions from: [:new, :sent_for_processing, :sent_to_society], to: :rejected
  end

  event(:conflict) do
    transitions from: [:new, :sent_for_processing, :sent_to_society], to: :conflict
  end

  event(:set_not_controlled) do
    transitions from: [:new, :split_submitted, :sent_for_processing, :sent_to_society, :conflict], to: :not_controlled
  end

  event(:set_pending_distribution) do
    transitions from: [:new, :not_controlled], to: :pending_distribution
  end

  event(:hide) do
    transitions from: STATE_TO_STATUS.keys, to: :hidden
  end

  event(:unhide) do
    transitions from: [:hidden], to: :new
  end

  event(:terminate) do
    transitions from: STATE_TO_STATUS.keys, to: :terminated
  end

  event(:submitted) do
    transitions from: STATE_TO_STATUS.keys, to: :submitted
  end

  event(:resubmitted) do
    transitions from: STATE_TO_STATUS.keys, to: :resubmitted
  end

  event(:verified) do
    transitions from: STATE_TO_STATUS.keys, to: :verified
  end

  event(:disqualify) do
    transitions from: STATE_TO_STATUS.without(:ineligible).keys, to: :ineligible
  end

  def self.convert_to_status(state)
    STATE_TO_STATUS[state.downcase]
  end

  def self.convert_to_state(status)
    @@status_to_state ||= STATE_TO_STATUS.invert
    @@status_to_state[status.downcase]
  end

  def qualify!
    return [] unless ineligible? && prev_state.present?

    ScottBarron::Acts::StateMachine::SupportingClasses::StateTransition.new(from: :ineligible, to: prev_state).perform(self)
  end

  def publishing_composer
    @publishing_composer ||= (publishing_composition_splits.first.try(:publishing_composer) || non_tunecore_songs.first.try(:publishing_composer))
  end

  def total_split_pct
    publishing_composition_splits.sum(:percent)
  end

  def isrc
    songs.where.not(optional_isrc: nil).first&.optional_isrc ||
      songs.where.not(tunecore_isrc: nil).first&.tunecore_isrc ||
      non_tunecore_songs.where.not(isrc: nil).first&.isrc
  end

  def status
    self.class.convert_to_status(state)
  end

  def composition_name
    translated_name || name
  end

  def clean_composition_name
    sanitize(composition_name)
  end

  def clean_name
    sanitize(name)
  end

  def clean_translated_name
    return "" if translated_name.blank?

    sanitize(translated_name)
  end

  def send_to_rights_app
    return "Publishing Composer Not Present" if publishing_composer.blank?
    return "Cannot Send Draft Publishing Composition" if held_from_rights_app?

    args = { publishing_composer: publishing_composer, publishing_composition: self }
    PublishingAdministration::PublishingWorkListService.list(account, true)
    PublishingAdministration::ApiClientServices::PublishingRecordingService.post_recording(args)
    nil
  rescue PublishingAdministration::ErrorService::ApiError => e
    e.message
  end

  def share_submitted_date
    publishing_composition_splits.order(:updated_at).last&.updated_at
  end

  def song
    non_tunecore_songs.first || songs.first
  end

  def composer_share
    publishing_composition_splits.collectable.sum(&:percent)
  end

  def cowriter_share
    publishing_composition_splits.non_collectable.sum(&:percent)
  end

  def held_from_rights_app?
    state == "draft"
  end

  private

  def sanitize(name_type)
    regex = %r/[%\\;<=>?\/\[\]\^`{}\|~]/
    I18n.transliterate(name_type.tr("$", "S").tr("\u2019", "'")).gsub(regex, "")
  end

  def update_state_transition
    return unless state_changed?

    self.state_updated_on = Time.now.to_date
    self.prev_state = state_was
  end

  def nullify_provider_identifier
    return if provider_identifier.present?

    self.provider_identifier = nil
  end
end
