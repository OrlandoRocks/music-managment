class SubscriptionProduct < ApplicationRecord
  belongs_to :product
  has_many :subscription_events

  attr_accessor :discount

  def self.create_subscription_purchase(person, product, subscription_type)
    return if person.has_subscription_product_in_cart_for?(subscription_type)

    subscription_purchase = SubscriptionPurchase.create!(
      subscription_product_id: product.subscription_product.id,
      payment_channel: "Tunecore",
      person: person
    )
    Product.add_to_cart(person, subscription_purchase, product)
  end

  def self.create_subscription_purchase_for_social(person, product, subscription_type)
    ActiveRecord::Base.transaction do
      person.remove_unpaid_subscriptions_for_social(subscription_type)
      subscription_purchase = SubscriptionPurchase.create!(
        subscription_product_id: product.subscription_product.id,
        payment_channel: "Tunecore",
        person: person
      )
      purchase = Product.add_to_cart(person, subscription_purchase, product)
      purchase
    end
  end

  def self.active_subscription_product_for_user(person, product_name)
    SubscriptionProduct
      .joins(:product)
      .where(arel_table[:product_name].eq(product_name)
      .and(Product.arel_table[:country_website_id].eq(person.country_website_id))
      .and(Product.arel_table[:status].eq(Product::ACTIVE)))
  end

  def self.active_product_for_user(person, product_name)
    active_subscription_product_for_user(person, product_name).first.try(:product)
  end

  def self.active_by_product_type_for(person, product_name, product_type)
    active_subscription_product_for_user(person, product_name).find_by(product_type: product_type)
  end

  def self.subscription_products_by_country_website(subscription_type, country_website_id)
    selects = "products.display_name, products.price, products.currency, subscription_products.product_type, products.id as product_id"
    select(selects).joins(:product).where({ product_name: subscription_type, products: { country_website_id: country_website_id } })
  end
end
