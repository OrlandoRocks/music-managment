class CaptchaWhitelist < ApplicationRecord
  validates :ip, :description, presence: true
  validates :ip, uniqueness: true
end
