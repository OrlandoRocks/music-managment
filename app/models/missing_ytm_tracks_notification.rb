class MissingYtmTracksNotification < Notification
  validates :notification_item, presence: true
  validates :notification_item_type, inclusion: { in: %w[Song] }

  def setup_system_notification
    self.person = notification_item.album.person if notification_item
    set_url
    set_title
    set_text
    set_image_url
  end

  def self.create_notifications
    store = Store.find_by(abbrev: "ytsr")
    people = Person.find_by_sql(
      "select people.* from people
               left outer join (select people.* from people
                 inner join youtube_monetizations ytm on people.id = ytm.person_id
                 inner join albums on people.id = albums.person_id
                 inner join salepoints on albums.id = salepoints.salepointable_id and salepoints.salepointable_type = 'Album' and salepoints.store_id = #{store.id}
                 inner join songs on albums.id = songs.album_id
                 left outer join salepoint_songs on salepoints.id = salepoint_songs.salepoint_id
                 left outer join ytm_ineligible_songs on songs.id = ytm_ineligible_songs.song_id
                 where ytm.effective_date is not null and ytm.termination_date is null and (salepoint_songs.id is not null or ytm_ineligible_songs.id is not null)
                 group by people.id) action_taken_people on people.id = action_taken_people.id
               inner join youtube_monetizations ytm on people.id = ytm.person_id
               where action_taken_people.id is null and ytm.termination_date is null"
    )

    Rails.logger.info "Creating notifications for ytm tracks not submitted to be monetized"

    people.each do |p|
      Rails.logger.info "Creating notification for ytm tracks not submitted to be monetized for person=#{p.id}"
      if salepoint = p.salepoints.where(store_id: store.id).order("salepoints.id desc").first && song = salepoint.salepointable.try(:songs).try(:last)
        MissingYtmTracksNotification.create(notification_item: song)
      end
    end
  end

  private

  def set_url
    self.url = "/ytm_tracks"
  end

  def set_title
    store = Store.find_by(abbrev: "ytsr")
    self.title = "#{notification_item.name} could be earning you money from YouTube. Go for it!"
  end

  def set_text
    num_of_tracks = person.salepoints.joins("inner join songs on albums.id = songs.album_id").count

    self.text = "You're missing an opportunity to earn money from the sound recording on #{notification_item.name} and #{num_of_tracks} other tracks-at no additional costs. All your current and future tracks are covered under the one-time setup fee you paid for YouTube Sound Recording Revenue service. Just go to your \"Select Tracks for Monetization\" page to add more tracks. It's easy to make money on YouTube!"
  end

  def set_image_url
    self.image_url = "/images/notifications/ytm_small.jpg"
  end
end
