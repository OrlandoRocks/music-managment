class PublishingNotifier < BaseMailer
  def bmi_registration_reminder(composer_id, composer_class_name = "Composer")
    composer      = composer_class_name.constantize.find(composer_id)
    @recipients   = composer.current_email
    @from         = "noreply@tunecore.com"
    @client_name  = composer.full_name

    I18n.with_locale(COUNTRY_LOCALE_MAP[composer.publishing_administrator.country_domain]) {
      mail(to: @recipients, subject: I18n.t("models.publishing_notifier.signup_bmi"), from: @from)
    }
  end

  def non_tunecore_composition_email(composer_id, composer_class_name = "Composer")
    composer      = composer_class_name.constantize.find(composer_id)
    @recipients   = composer.current_email
    @from         = "songwriters@tunecore.com"
    @client_name  = composer.full_name

    I18n.with_locale(COUNTRY_LOCALE_MAP[composer.publishing_administrator.country_domain]) {
      mail(to: @recipients, subject: I18n.t("models.publishing_notifier.submit_songs"), from: @from)
    }
  end
end
