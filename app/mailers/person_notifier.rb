class PersonNotifier < BaseMailer
  include ApplicationHelper

  layout "customer_email",
         only: [
           :change_email,
           :change_password,
           :payment_thank_you,
           :forgot_password,
           :cyrillic_formatting,
           :lead_migrated,
           :youtube_sound_recording_update,
           :auto_renewal_billing_info_confirmation,
           :vat_info_update_reminder,
           :verify_email
         ]
  helper :invoices
  helper :vat

  DOMAINLESS_LOCALE_COUNTRIES = ["RU", "BR", "PT", "ES"].map(&:freeze).freeze

  DOMAINLESS_LOCALE_MAP = {
    RU: "RU-US",
    BR: "PT-US",
    PT: "PT-US",
    ES: "ES-US"
  }.freeze.with_indifferent_access

  def verify(person, options = {})
    setup_email(person, true, options)
    url_params = {
      host: options[:host],
      only_path: false,
      key: person.generate_invite_code
    }
    url =
      if options[:is_from_social]
        complete_verify_api_person_url(person, url_params)
      else
        complete_verify_person_url(person, url_params)
      end
    @from = "TuneCore <tcnews@tunecore.com>"
    @subject = I18n.t("models.person_notifier.verify")

    @person = person
    @domain = person.domain
    @url   = url
    @host  = options[:host]

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def lead_migrated(person)
    setup_email(person, true)
    @from = "TuneCore <tcnews@tunecore.com>"
    @person = person
    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(
        to: @recipients,
        from: @from,
        subject: I18n.t(
          "models.person_notifier.lead_migrated.subject",
          first_name: @person.name.split(" ").first,
          country_name: @person.country
        )
      )
    end
  end

  def sync_approval(sync_membership_request, host, options = {})
    setup_email(sync_membership_request, true, options)

    url = new_sync_person_url(
      host: host,
      request_code: sync_membership_request.request_code
    )

    @from = "TuneCore <tcnews@tunecore.com>"
    @subject = "Sync Membership Approved"

    @sync_membership_request = sync_membership_request
    @url = url

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def sync_mass_invite(sync_membership_request, host, options = {})
    setup_email(sync_membership_request, true, options)

    url = new_sync_person_url(
      host: host,
      request_code: sync_membership_request.request_code
    )

    @from = "Pete Rogers <pete@tunecore.com>"
    @subject = "TuneCore Sampler, Sync & Master Licensing Website!"

    @sync_membership_request = sync_membership_request
    @url = url

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def sync_register_reminder(sync_membership_request, host, options = {})
    setup_email(sync_membership_request, true, options)

    url = new_sync_person_url(
      host: host,
      request_code: sync_membership_request.request_code
    )

    @from = "creative@tunecore.com"
    @subject = "Confirm Your TuneCore Sync & Master Registration"

    @sync_membership_request = sync_membership_request
    @url = url

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def change_password(person, password, _options = {})
    setup_email(person)

    # Email header info
    @subject += I18n.t("models.person_notifier.change_password")

    # Email body substitutions
    @name     = person.name
    @email    = person.email
    @person   = person
    @password = password

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def change_stored_bank_account(person, _options = {})
    setup_email(person)

    # Email header info
    @subject += I18n.t("models.person_notifier.change_bank")

    # Email body substitutions
    @name     = person.name
    @email    = person.email
    @person   = person

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def change_email(person, _email, options = {})
    setup_email(person, false, options)
    # Email header info
    @subject += I18n.t("email_changed.success")
    # Email body substitutions
    @name   = person.name
    @person = person
    @email  = person.email
    @url    = "https:" + knowledgebase_link_for(
      :new_request,
      person.country_website.country
    )

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def payment_thank_you(person, invoice, domain = "www.tunecore.com")
    return if FeatureFlipper.show_feature?(:block_indian_invoices, person)

    @person         = person
    @recipients     = person.email
    @subject        = payment_confirmation_subject
    @invoice        = invoice
    @domain         = domain
    @show_message   = FeatureFlipper.show_feature?(
      :show_delivery_delay_email_message,
      person
    )
    @reseller = person.india_user? ? ENV.fetch("INDIA_INVOICE_RECIPIENTS", "test1@tunecore.com") : ""
    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, bcc: @reseller.split(","), subject: @subject)
    end
  end

  def album_renewal_change_cancel_status(album)
    @person     = album.person
    @renewal    = Renewal.renewal_for(album)
    @recipients = @person.email
    @subject    = @renewal.canceled? ? "TuneCore Confirmation: Your renewal has been canceled" : "TuneCore Confirmation: Your release has been reactivated"
    @album      = album
    @domain     = @person.domain

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def bizdev_thankyou(user)
    @recipients  = user[:email]
    @from        = "TuneCore <bizdev@tunecore.com>"
    @subject     = I18n.t("models.person_notifier.thank_you_bizdev")

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def contact_bizdev(user)
    @recipients      = "bizdev@tunecore.com"
    @from            = "noreply@tunecore.com"
    @subject         = "Business Development Inquiry"

    @company              = user[:company]
    @business             = user[:business]
    @website              = user[:website]
    @facebook             = user[:facebook]
    @twitter              = user[:twitter]
    @blog                 = user[:blog]
    @customer_email_count = user[:customer_email_count]
    @name                 = user[:name]
    @title                = user[:title]
    @phone                = user[:phone]
    @email                = user[:email]
    @biz_description      = user[:biz_description]
    @yrs_in_biz           = user[:yrs_in_biz]
    @customer_base_size   = user[:customer_base_size]
    @customer_demographic = user[:customer_demographic]
    @area_of_interest     = user[:area_of_interest]
    @details              = user[:details]

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def submit_ingrooves_fontana(user, report)
    @recipients = user[:email]
    @subject    = "INgrooves Fontana Artist Submission"
    @user = user
    @song = report.track
    mail(to: @recipients, subject: @subject)
  end

  def get_rejected_reason(reason)
    reason_subject_mapping = {
      album_not_sure_maybe_block_from_sales: "Rights Verification",
      album_blocked_from_sales_cover_song_formatting: "Cover Song Formatting - Action Required",
      denied_ringtone_albums_if_itunes_is_selected: "Unable to distribute Ringtone Album to iTunes",
      denied_audiobooks_if_itunes_is_selected: "Unable to distribute Audiobook Album to iTunes",
      denied_album_high_profile_features: "Rights verification",
      denied_album_unauthorized_artwork: "Replacement artwork required",
      denied_ringtone_submitted_textone: "Unable to distribute text tones",
      denied_karaoke_ringtone: "release changes required",
      denied_album_unauthorized_remix: "Content removed",
      denied_album_possibly_using_masters: "Content removed",
      denied_album_unauthorized_samples_replace_not_OK: "Content removed",
      denied_album_unauthorized_samples_replace_OK: "Content removed",
      denied_album_covers_before_original_release_date: "Covers/Karaoke Versions Before Original Release Date",
      denied_album_artist_name_additional_artist_info: "release changes required",
      denied_album_titles_producer_credit: "update required",
      denied_album_artwork_cover_metadata_mismatch: "release changes required",
      denied_album_artwork_extra_info: "artwork update required",
      denied_album_artwork_contact_info: "release changes required",
      denied_album_artwork_store_logos: "release changes required",
      denied_album_artwork_reference_physical_packaging: "release changes required",
      denied_album_artist_name_too_generic: "update required",
      denied_album_dashes_spaces_underscores: "release changes required",
      denied_album_artwork_quality: "Artwork update required",
      denied_album_unsupported_language: "important information regarding your release",
      denied_album_classical_album: "important information regarding your release",
      denied_album_remove_iTunes_iPhone_references: "release changes required",
      denied_album_extra_info_associated_bands_instruments_members: "release changes required",
      denied_album_aka_or_alternate_artist_name: "release changes required",
      denied_album_reference_in_title: "release changes required",
      denied_album_trademark_in_artist_field: "release changes required",
      denied_album_subliminal_binaural: "release changes required",
      denied_album_artist_name_misleading_lyrics_original: "release changes required",
      denied_album_artist_name_incorrect_features: "release changes required",
      denied_streaming_fraud: "Important Information",
      denied_unnecessary_title: "release changes required",
      denied_update_artist_name: "release changes required",
      denied_live_formatting: "release changes required",
      denied_soundtrack_formatting: "release changes required",
      denied_duplicate_titles: "update required",
      denied_translations: "update required",
      denied_incorrect_parental_logo: "Artwork issue"
    }

    reason_subject_mapping.fetch reason.to_sym, "Content Removed"
  end

  def cert_redemption_thank_you(person, account_overview, domain = "www.tunecore.com")
    #
    # FIXME: Still need more info on the content
    # This would probably need the following:
    #   - Thank you note
    #   - Distribution credits available
    #   - Link to start creating an album?
    #
    @recipients       = person.email
    @subject          = "Guitar Center Distribution Credit Redemption at Tunecore.com"
    @person           = person
    @account_overview = account_overview
    @domain           = domain

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def publishing_welcome(person)
    @recipients = person.email
    @subject    = I18n.t("models.person_notifier.welcome_pubadmin")
    @person     = person
    @help_url   = knowledgebase_link_for("publishing-nontunecore-releases", person.country_domain)

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject)
    end
  end

  def resolve_disputes(disputes)
    @recipients   = "songwriters@tunecore.com"
    @from         = "noreply@tunecore.com"
    @subject      = "Composition Dispute for #{disputes[:composer][:name]}"
    @compositions = disputes[:compositions]
    @composer     = disputes[:composer]
    @email        = disputes[:email]
    @phone        = disputes[:phone]
    @message      = disputes[:message]

    mail(to: @recipients, from: @from, subject: @subject)
  end

  # email sent to tunecore account email to confirm a paypal request.
  def paypal_withdrawal_confirmation(paypal_transfer)
    person        = paypal_transfer.person
    @recipients   = person.email
    @subject      = I18n.t("models.person_notifier.withdraw_confirm")
    @person          = person
    @domain          = person.domain
    @paypal_transfer = paypal_transfer

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(to: @recipients, subject: @subject).transport_encoding = "quoted-printable"
    end
  end

  def cyrillic_formatting(person)
    @recipients = person.email
    @subject    = "Important: Information About Formatting Your Music For Stores"
    @person     = person

    mail(to: @recipients, subject: @subject)
  end

  def youtube_publishing_confirmation(user)
    @recipients = user.email

    @subject = "Your YouTube Preferences"
    @from    = "youtube@tunecore.com"

    @person = user

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(to: @recipients, from: @from, subject: @subject)
    end
  end

  def youtube_sound_recording_update(person_id, template_name, message_data)
    @email_template = YoutubeSrMailTemplate::TemplateBuilder.build(
      person_id,
      template_name,
      message_data
    )
    @subject = @email_template.subject
    @person = @email_template.person
    from = "youtube@tunecore.com"
    I18n.with_locale(COUNTRY_LOCALE_MAP[@email_template.country]) do
      mail(to: @email_template.to_email, from: from, subject: @subject)
    end
  end

  def terms_and_conditions_update(person_id)
    @person        = Person.find(person_id)

    country        = @person.attributes["country"]
    locale_country =
      if DOMAINLESS_LOCALE_COUNTRIES.include?(country)
        DOMAINLESS_LOCALE_MAP[country]
      else
        @person.country_website.country
      end

    @recipients = ENV.fetch("QA_T_AND_C", @person.email)

    I18n.with_locale(COUNTRY_LOCALE_MAP[locale_country]) do
      mail(
        to: @recipients,
        from: ENV.fetch("T_AND_C_FROM", "no-reply@tunecore.com"),
        subject: I18n.t("person_notifier.terms_and_conditions_update.subject")
      )
    end
  end

  def auto_renewal_billing_info_confirmation(person, tc_social = false)
    setup_category_header(self)

    @person = person
    locale = @person.country_website_language&.yml_languages || @person.locale
    @url = WEB_ONLY_URLS[@person.country_domain]

    @expires_at =
      if tc_social
        @person.active_subscription_expires_at(subscription_type: "Social") || Time.now
      else
        @person.first_upcoming_renewal_expires_at || Time.now
      end

    @person.mark_auto_renewal_email_sent!

    I18n.with_locale(locale) do
      mail(
        to: @person.email,
        from: "TuneCore <no-reply@tunecore.com>",
        subject: I18n.t("person_notifier.auto_renewal_billing_info_confirmation.subject")
      )
    end
  end

  def vat_info_update_reminder(person)
    @person = person
    locale = @person.country_website_language&.yml_languages || @person.locale
    @url = account_settings_url(host: WEB_ONLY_URLS[@person.country_domain], protocol: "https")

    I18n.with_locale(locale) do
      mail(
        to: @person.email,
        from: "TuneCore <no-reply@tunecore.com>",
        subject: I18n.t("person_notifier.vat_info_update_reminder.subject")
      )
    end
  end

  def payment_confirmation_subject
    return I18n.t("models.person_notifier.payment_confirmation") unless bi_transfer_and_gst_enabled?

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      I18n.t("models.person_notifier.payment_confirmation")
    end
  end

  def update_tax_forms(person_id, person_name, tin, send_date = Date.today)
    @tin = tin
    @person_name = person_name
    @person = Person.find(person_id)
    @last_date = send_date + 30.days
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(to: @person.email, subject: I18n.t("models.person_notifier.update_tax_forms"))
    end
  end

  def verify_email(person, new_email, token)
    setup_email(person)
    # Email body substitutions
    @name = person.name
    @person = person
    @old_email = person.email
    @new_email = new_email

    @url = complete_verification_email_change_requests_url(
      host: WEB_ONLY_URLS[person.country_domain],
      protocol: "https",
      params: {
        token: token
      }
    )

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      mail(
        to: @old_email,
        from: "TuneCore <no-reply@tunecore.com>",
        subject: I18n.t("email_change_email_with_confirmation_link.subject")
      )
    end
  end
end
