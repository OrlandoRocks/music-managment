class BaseMailer < ActionMailer::Base
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::TextHelper
  include KnowledgebaseLinkHelper
  helper  :custom_translation
  helper  :application
  helper  :currency
  default from: AccountSystem::CONFIG[:email_from] || "TuneCore <noreplies@tunecore.com>"

  protected

  def setup_category_header(original_caller)
    begin
      category = "#{self.class}::#{original_caller[0][/`.*'/][1..-2]}"
    rescue StandardError => e
      category = "#{self.class}::Uncategorized"
      logger.debug "Unable to set category header: #{e.message}"
    end

    headers["X-SMTPAPI"] = { category: [category] }.to_json
  end

  def setup_email(person, _html = nil, options = {})
    setup_category_header(caller)

    @recipients =
      if options[:specified_email] != nil
        "#{options[:specified_email]},#{person.email}"
      else
        person.email.to_s
      end

    @subject = "[#{AccountSystem::CONFIG[:app_name]}]"
  end
end
