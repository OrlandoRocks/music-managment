class NegativeBalanceAlertMailer < BaseMailer
  def alert_email(person_balances, date)
    @person_balances = person_balances
    @date            = date
    recipient        = "techteam@tunecore.com"
    subject          = "Negative Person Balance List"
    mail(to: recipient, subject: subject)
  end
end
