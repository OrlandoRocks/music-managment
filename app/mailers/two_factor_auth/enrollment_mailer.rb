class TwoFactorAuth::EnrollmentMailer < BaseMailer
  layout "customer_email"

  def send_enrollment_mailer(person)
    setup_email(person)
    @person = person

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) do
      subject = I18n.t("two_factor_authentication.enrollment_mailer.send_enrollment_mailer.subject")
      mail(to: @recipients, subject: subject, from: "accountsecurity@tunecore.com")
    end
  end
end
