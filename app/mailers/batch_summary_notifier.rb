class BatchSummaryNotifier < BaseMailer
  include Logging
  layout "admin_email"

  def reminders_summary(renewal_email_statuses, run_date = nil)
    # variables for email text
    run_date = run_date ? Date.parse(run_date) : Time.now

    # Get total reminders sent
    reminders_count = renewal_email_statuses.sum { |_key, status| status[:sent_count] }

    # email options
    @recipients = ENV["RENEWAL_SUMMARY_RECIPIENTS"]
    @subject    = "Renewal Reminders Sent"

    @renewal_email_statuses = renewal_email_statuses
    @run_date               = run_date
    @reminders_count        = reminders_count

    mail(to: @recipients, subject: @subject)
  end

  # summary email from batch processing sent each time a batch finishes
  def batch_summary(batch_id)
    batch = PaymentBatch.find(batch_id)
    batch_summary = Batch::BatchSummary.new(batch)

    # If you want to use an attachment with a multipart email, you must EXPLICITLY define each content type as I did below.
    @recipients    = ENV["RENEWAL_SUMMARY_RECIPIENTS"]
    @subject       = "Batch Summary"

    @batch         = batch
    @batch_summary = batch_summary

    mail(to: @recipients, subject: @subject)
  end

  # summary email from batch processing sent weekly to Customer service
  def not_processed_summary(batch_transactions)
    # If you want to use an attachment with a multipart email, you must EXPLICITLY define each content type as I did below.
    @recipients  = ENV["RENEWAL_CUSTOMER_REPORT_RECIPIENTS"]
    @subject     = "Failed Renewals Summary"

    @batch = batch_transactions.first.payment_batch
    @batch_transactions_count = batch_transactions.size
    @batch_percentage         = ((batch_transactions.size.to_f / @batch.batch_transactions.count.to_f) * 100).round(1)
    @amount                   = batch_transactions.sum(&:amount)

    filename = "batch_not_processed_summary_" + @batch.batch_date.strftime("%Y-%m-%d-#{@batch.id}-#{@batch.id}") + ".csv"
    filepath = BRAINTREE_BATCH[:local_reports] + filename

    CSV.open(filepath, "w") do |csv|
      csv << [
        "Person ID",
        "Person Name",
        "Email",
        "Invoice ID",
        "Amount",
        "Person Balance",
        "Person Preferences Set?",
        "Person Pay with Balance?",
        "Preferred Credit Card ID",
        "Preferred Credit Card Last 4",
        "CC Expiration"
      ]
      batch_transactions.reject { |bt| bt.invoice.blank? }.each do |txn|
        invoice = txn.invoice
        person = invoice.person
        preference = person.person_preference
        csv << [
          person.id,
          person.name,
          person.email,
          invoice.id,
          txn.amount,
          person.balance,
          person.preferences_set? ? "Y" : "N",
          person.renew_with_balance? ? "Y" : "N",
          person.preferences_set? ? preference.preferred_credit_card_id : "",
          if (person.preferences_set? && preference.preferred_credit_card)
            preference.preferred_credit_card.last_four
          else
            ""
          end,
          if (person.preferences_set? && preference.preferred_credit_card)
            "#{preference.preferred_credit_card.expiration_month}-#{preference.preferred_credit_card.expiration_year}"
          else
            ""
          end
        ]
      end
    end

    attachments[filename] = File.read(filepath)
    mail(to: @recipients, subject: @subject)
  end

  def cc_failure_summary(batch_id)
    batch = PaymentBatch.find(batch_id)

    batch_transactions = BatchTransaction
                         .includes(:invoice)
                         .where(payment_batch_id: batch.id,
                                processed: "processed",
                                matching_type: %w[BraintreeTransaction PaymentsOSTransaction])

    filename = "batch_cc_failure_summary_" + batch.batch_date.strftime("%Y_%m_%d_#{batch.id}_%H%M%S") + ".csv"
    filepath = BRAINTREE_BATCH[:local_reports] + filename
    count, total_amount = 0, 0

    CSV.open(filepath, "w") do |csv|
      csv << [
        "Person ID",
        "Person Name",
        "Email",
        "Invoice ID",
        "Amount",
        "Person Balance",
        "Person Preferences Set?",
        "Person Pay with Balance?",
        "Preferred Credit Card ID",
        "Preferred Credit Card Last 4",
        "CC Expiration",
        "Transaction ID",
        "Status",
        "Response Code",
        "CVV Response",
        "AVS Response",
        "Raw Response"
      ]
      batch_transactions.each do |txn|
        invoice = txn.invoice
        next unless invoice.batch_attempts == BRAINTREE_BATCH[:run_limit] && !txn.matching.success?

        person = invoice.person
        preference = person.person_preference
        count += 1
        total_amount += txn.amount
        csv << [
          person.id,
          person.name,
          person.email,
          invoice.id,
          txn.amount,
          person.balance,
          person.preferences_set? ? "Y" : "N",
          person.renew_with_balance? ? "Y" : "N",
          person.preferences_set? ? preference.preferred_credit_card_id : "",
          if (person.preferences_set? && preference.preferred_credit_card)
            preference.preferred_credit_card.last_four
          else
            ""
          end,
          if (person.preferences_set? && preference.preferred_credit_card)
            "#{preference.preferred_credit_card.expiration_month}-#{preference.preferred_credit_card.expiration_year}"
          else
            ""
          end,
          txn.matching.transaction_id,
          txn.matching.status,
          txn.matching.respond_to?(:response_code) ? txn.matching.response_code : "",
          txn.matching.respond_to?(:cvv_response) ? txn.matching.cvv_response : "",
          txn.matching.respond_to?(:avs_response) ? txn.matching.avs_response : "",
          txn.matching.respond_to?(:raw_response) ? txn.matching&.raw_response : ""
        ]
      end
    end

    # If you want to use an attachment with a multipart email, you must EXPLICITLY define each content type as I did below.
    @recipients      =  ENV["RENEWAL_CUSTOMER_REPORT_RECIPIENTS"]
    @subject         =  "Batch Credit Card Declined Summary"
    @declined_count  = count
    @declined_amount = total_amount
    @batch           = batch

    attachments[filename] = File.read(filepath)
    if total_amount.positive?
      mail(to: @recipients, subject: @subject)
    else
      log("Batch Summary Notifier", "Not sending cc failure summary as there are no failed transactions for batch id #{batch_id}")
    end
  end
end
