class ArtistMappingMailer < BaseMailer
  layout "customer_email"

  def failed_apple_artist_match_email(options)
    @person            = Person.find(options["person_id"])
    @artist            = Artist.find(options["artist_id"])
    @identifier        = options["identifier"]
    @recipients        = @person.email
    @subject           = I18n.t("artist_mapping_mailer.failed_apple_artist_match_email.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def redistribution_success_email(redistribution_package)
    @person     = redistribution_package.person
    @artist     = redistribution_package.artist
    @recipients = @person.email
    @subject    = I18n.t("artist_mapping_mailer.redistribution_success_email.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
