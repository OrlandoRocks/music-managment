class SyncNotifier < BaseMailer
  def license(sync_license)
    @recipients   = sync_license.person.email
    @composition  = sync_license.song.composition.name
    @subject      = "Audio for licensed composition <%= @composition %>"
    @download_url = sync_license.ticketbooth_url
    @sync_license = sync_license
    @composition  = @composition

    mail(to: @recipients, subject: @subject)
  end
end
