class AdminNotifier < BaseMailer
  def suspicious_activity(person, note)
    @recipients = "fraud_report@tunecore.com"
    @subject    = "SUSPICIOUS ACTIVIY ALERT: #{person.name}"
    @person     = person
    @note       = note

    mail(to: @recipients, subject: @subject)
  end

  def sync_license_requested(person, license_request, song, muma_song, host)
    @recipients = "licensing@tunecore.com"
    @subject    = "Quote Request - #{muma_song.title}"

    # url to to display info for new request
    @url          = sync_admin_license_request_url(license_request, host: host)
    @artist_name  = song.artist_name
    @person       = person
    @label_copy   = muma_song.label_copy
    @request      = license_request
    @writers      = []
    @composition  = muma_song.title

    # Use composers from the muma song first
    if song.muma_songs.present? && song.muma_songs.first.composers.present?
      @writers =
        song.muma_songs.first.composers.collect { |composer|
          "#{composer.composer_first_name} #{composer.composer_last_name}"
        }
    end

    # Fall back on composer from TC composition
    if @writers.blank? and song.composition && song.composition.publishing_splits.present?
      @writers =
        song.composition.publishing_splits.map { |split|
          "#{split.composer.first_name} #{split.composer.last_name}"
        }
    end

    mail(to: @recipients, subject: @subject)
  end

  def sync_membership_requested(membership_request, host)
    @recipients = "licensing@tunecore.com"
    @subject    = "Sync Membership Requested"

    @url = sync_admin_memberships_url(host: host)
    @membership_request = membership_request

    mail(to: @recipients, subject: @subject)
  end

  def muma_import_success(stats)
    @recipients = AccountSystem.config[:admin_publishing_email]
    @subject    = "Successful Music Maestro Import"
    @stats = stats

    mail(to: @recipients, subject: @subject)
  end

  def muma_import_failure(error)
    @recipients = AccountSystem.config[:admin_publishing_email]
    @subject    = "Failed Music Maestro Import"
    @error = error

    mail(to: @recipients, subject: @subject)
  end

  def youtube_preferences_export(export, export_file, reset_file)
    @recipients =  AccountSystem.config[:admin_youtube_export_email]
    @subject    =  "Youtube Preferences Export"
    @export = export

    attachments["youtube_preferences_add_#{Time.now.strftime('%Y-%m-%d')}.csv"] = File.read(export_file)
    attachments["youtube_preferences_remove_#{Time.now.strftime('%Y-%m-%d')}.csv"] = File.read(reset_file)

    mail(to: @recipients, subject: @subject)
  end

  def client_import_report(person, folder, zip_filename)
    @recipients = person.email
    @subject    = "Client Import Report"

    attachments["#{zip_filename}.zip"] = File.read(File.join(folder, "/#{zip_filename}.zip"))

    mail(to: @recipients, subject: @subject)
  end

  def ingrooves_fontana_requested(user, report)
    @recipients = "ingrooves@tunecore.com"
    @subject    = "INgrooves Fontana Artist Submission"

    @user = user
    @song = report.track

    mail(to: @recipients, subject: @subject)
  end

  def itunes_ticket_report
    @recipients = [
      "agulati@tunecore.com",
      "troy@tunecore.com",
      "elise@tunecore.com",
      "libby@tunecore.com",
      "sean@tunecore.com"
    ]
    @from       = "no-reply@tunecore.com"
    @subject    = "iTunes Tickets for Takedowns #{Time.now.strftime('%Y-%m-%d')}"

    attachments["itunes_ticket_report.csv"] = File.read("itunes_ticket_report.csv")

    mail(to: @recipients, from: @from, subject: @subject)
  end

  def itunes_hidden_content_report(delivered_date)
    @recipients = ["troy@tunecore.com", "liz@tunecore.com", "sean@tunecore.com"]
    @subject    = "iTunes Hidden Content Report For #{delivered_date}"

    attachments["itunes_hidden_content_report.csv"] = File.read("/tmp/itunes_hidden_report_#{delivered_date}.csv")

    mail(to: @recipients, subject: @subject)
  end

  def batch_balance_adjustment(batch_id, admin_email, status)
    @batch_id   = batch_id
    @recipients = [admin_email]
    @status     = status
    @subject    = "Batch Balance Adjustment Result for batch_id: #{@batch_id}"

    mail(to: @recipients, subject: @subject)
  end

  def mass_balance_adjustment(mass_adjustment_batch_id, recipients)
    @mass_adjustment_batch_id = mass_adjustment_batch_id
    @recipients = recipients
    @subject = "Mass Adjustment Batch Complete"
    @host = "#{DEPLOY_ENV}.tunecore.com"

    mail(to: @recipients, subject: @subject)
  end
end
