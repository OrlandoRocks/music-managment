class DeliveryErrorSummaryMailer < BaseMailer
  layout "admin_email"

  def delivery_errors_email(album, upc, from_date)
    @from          = "ops@tunecore.com"
    @recipients    = "deliveryerrors@tunecore.com"
    @distributions = Distribution.where(id: album.distros)
    @album         = album
    @upc           = upc
    @from_date     = from_date
    vip_label      = @album.person&.vip ? "[VIP]" : nil
    @subject       = [vip_label, "Action Required:", @upc].compact.join(" ")
    mail(to: @recipients, from: @from, subject: @subject, from_alias: "Error report")
  end
end
