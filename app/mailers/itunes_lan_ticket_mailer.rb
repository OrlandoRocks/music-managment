class ItunesLanTicketMailer < BaseMailer
  RECIPIENT = "mymusic@tunecore.com"

  def lan_ticket_creation(ticket, people)
    @subject    = assign_subject(ticket, people)
    @recipients = RECIPIENT
    @ticket     = ticket
    @people     = people
    @person     = people.first
    mail(to: @recipients, subject: @subject)
  end

  private

  def assign_subject(ticket, people)
    # Rails 4.1 bug https://github.com/rails/rails/issues/13648
    # Work around is to use count(:all) in queries which have `.select`
    if people.count(:all) > 1
      "Duplicate UPC Problem: #{ticket[:content_vendor_id]}"
    else
      person = people.first
      "#{ticket[:content_ticket_type]}: #{person.email}, #{ticket[:name]}"
    end
  end
end
