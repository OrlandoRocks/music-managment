class SoundoutMailer < BaseMailer
  layout "customer_email"

  def report_available(soundout_report)
    @person          = soundout_report.person
    @soundout_report = soundout_report
    @recipients      = [soundout_report.person.email]

    subject = "#{soundout_report.track.name}:  Your TuneCore Fan Reviews report is ready"
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: subject) }
  end
end
