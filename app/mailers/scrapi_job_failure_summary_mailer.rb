class ScrapiJobFailureSummaryMailer < BaseMailer
  layout "admin_email"

  def scrapi_jobs_errors_email(subject, scrapi_jobs_error)
    @from = "ops@tunecore.com"
    @recipients = ["scrapi_status@tunecore.com"]
    @subject = subject
    @scrapi_jobs_error = scrapi_jobs_error
    mail(to: @recipients, from: @from, subject: @subject, from_alias: "Error report")
  end
end
