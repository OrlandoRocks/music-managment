class TargetedOfferMailer < BaseMailer
  layout "admin_email"

  def activation_job_status(targeted_offer, issuing_user, delta_t)
    @recipients     = issuing_user.email
    @subject        = "Targeted Offer Change Activation Job Status"
    @targeted_offer = targeted_offer
    @delta_t        = delta_t

    mail(to: @recipients, subject: @subject)
  end

  def add_people_job_status(targeted_offer, could_not_find_ids, validation_fails, success_ids, already_added_ids, issuing_user, delta_t)
    @recipients          = issuing_user.email
    @subject             = "Targeted Offer Add People Job Status"
    @targeted_offer      = targeted_offer
    @could_not_find_ids  = could_not_find_ids
    @success_ids         = success_ids
    @already_added_ids   = already_added_ids
    @delta_t             = delta_t

    # Group validation fail ids by the error message
    grouped_validation_fails = {}

    validation_fails.each do |person_id, full_messages|
      full_messages.each do |message|
        grouped_validation_fails[message] = [] if grouped_validation_fails[message].nil?
        grouped_validation_fails[message] << person_id
      end
    end

    @grouped_validation_fails = grouped_validation_fails

    mail(to: @recipients, subject: @subject)
  end

  def remove_people_job_status(targeted_offer, could_not_find_ids, success_ids, could_not_destroy_ids, issuing_user, delta_t)
    @recipients            = issuing_user.email
    @subject               = "Targeted Offer Remove People Job Status"
    @targeted_offer        = targeted_offer
    @could_not_find_ids    = could_not_find_ids
    @could_not_destroy_ids = could_not_destroy_ids
    @success_ids           = success_ids
    @delta_t               = delta_t

    mail(to: @recipients, subject: @subject)
  end
end
