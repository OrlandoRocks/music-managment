class ReportNotifier < BaseMailer
  layout "admin_email"

  def mail_delivered_upcs(upcs = [], options = {})
    return if upcs.empty?

    @recipients = options[:recipients] || []
    @start_date = Date.parse(options[:start_date]).strftime("%d %b, %Y")
    @end_date = Date.parse(options[:end_date]).strftime("%d %b, %Y")
    @store_short_name = options[:store_short_name]
    @subject = "#{@store_short_name} report: #{@start_date} to #{@end_date}"
    filename = "#{@store_short_name.downcase}_delivered_upcs#{Time.current.strftime('%Y_%m_%d_%H%M%S')}.csv"

    filepath = "#{File.join(Rails.root, 'tmp')}/#{filename}"

    CSV.open(filepath, "w") do |csv|
      csv << ["UPC"]
      upcs.each do |upc|
        csv << [upc]
      end
    end

    attachments[filename] = File.read(filepath)

    mail(to: @recipients, subject: @subject)
  end
end
