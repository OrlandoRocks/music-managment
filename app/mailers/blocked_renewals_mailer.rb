# frozen_string_literal: true

class BlockedRenewalsMailer < BaseMailer
  layout "admin_email"

  BLOCKED_RENEWALS_SUBJECT   = "Blocked Renewals"
  FINANCE_TEAM_EMAIL         = "financeteam@tunecore.com"
  SANCTIONED_COUNTRY_REASON  = "Customer's country is sanctioned"

  def blocked_renewals(params)
    to      = FINANCE_TEAM_EMAIL
    subject = BLOCKED_RENEWALS_SUBJECT

    country = parse_country(params[:person][:country])

    @account_email = params[:person].email
    @reason        = "#{SANCTIONED_COUNTRY_REASON}: #{country}."
    @renewal_ids   = params[:renewals].map(&:id)

    mail(to: to, subject: subject)
  end

  private

  def parse_country(country)
    Country.unscoped.where(iso_code: country).pick(:name)
  end
end
