class InvoiceExportMailer < BaseMailer
  layout "admin_email"

  def invoice_export_complete(invoice_type, status)
    @invoice_type = invoice_type.capitalize
    @subject = "#{@invoice_type} Invoice Export Completed"
    @recipients = ENV.fetch("INVOICE_EXPORT_STATUS_RECIPIENTS").split(",")
    @failed_invoice_count = status.failures
    @exported_invoice_count = status.total - @failed_invoice_count

    mail(to: @recipients, subject: @subject)
  end
end
