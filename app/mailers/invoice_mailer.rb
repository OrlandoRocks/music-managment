class InvoiceMailer < BaseMailer
  def send_pdf_invoice(options = {})
    invoice = options.fetch(:invoice, {})
    person = invoice.person
    @name = person.name
    @invoice_date = invoice.settled_at.to_date
    @link = TC_SUPPORT_LINK

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_website.country]) do
      attachments["invoice#{Date.today}.pdf"] = options.fetch(:pdf, {})
      mail(
        to: person.email,
        subject: I18n.t("invoices.email.subject", invoice_id: invoice.id)
      )
    end
  end
end
