class DeliveryErrorFromTcDistributorMailer < BaseMailer
  layout "admin_email"

  def delivery_errors_email(album_id:, errors:, from_date:)
    @album         = Album.find(album_id)
    @upc           = @album.upc.number
    @from_date     = from_date
    @errors        = errors
    @from          = "ops@tunecore.com"
    @recipients    = ENV.fetch("DELIVERY_ERROR_TC_DISTRIBUTOR_EMAIL", "deliveryerrors@tunecore.com")
    vip_label      = @album.person&.vip ? "[VIP]" : nil
    @subject       = [vip_label, "Action Required:", @upc].compact.join(" ")
    mail(to: @recipients, from: @from, subject: @subject, from_alias: "Error report")
  end
end
