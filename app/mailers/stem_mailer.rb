class StemMailer < BaseMailer
  layout "customer_email"

  def password_reset_mail_for_stem_user(person_id)
    send_mail("stem_mailer.subject", person_id)
  end

  def onboarding_for_existing_tc_user(person_id)
    send_mail("stem_mailer.stem_transfer_subject", person_id)
  end

  def onboarding_for_new_tc_user(person_id)
    send_mail("stem_mailer.stem_transfer_subject", person_id)
  end

  def reminder_email_for_new_tc_users(person_id)
    send_mail("stem_mailer.reminder_subject", person_id)
  end

  def reminder_email_for_tc_stem_users(person_id)
    send_mail("stem_mailer.reminder_subject", person_id)
  end

  def stuck_status(person_id)
    send_mail("stem_mailer.stuck_status_mail.subject", person_id)
  end

  def stem_onboarding_reminder(person_id)
    send_mail("stem_mailer.reminder_subject", person_id)
  end

  def send_mail(subject, person_id)
    person = Person.find(person_id)
    setup_email(person)

    @subject += I18n.t(
      subject,
      locale: COUNTRY_LOCALE_MAP[person.country_domain].to_sym
    )
    @name     = person.name
    @person   = person

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
