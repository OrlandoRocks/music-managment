class StoreLaunchMailer < BaseMailer
  layout "customer_email"

  def launch(store_id, person_id)
    @person = Person.find(person_id)
    @store = Store.find(store_id)
    @recipients = @person.email
    @subject = I18n.t("models.store_launch.new_store", { store_name: @store.name })

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
