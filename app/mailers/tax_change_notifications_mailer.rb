# frozen_string_literal: true

class TaxChangeNotificationsMailer < BaseMailer
  layout "customer_email"

  def notify_users_no_inc_no_taxform(opts)
    @opts = opts
    @person = Person.find_by(email: target_user)
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) do
      mail(
        to: target_user,
        subject: I18n.t(
          "tax_change_notifications.notification_of_tax_change_no_inc_no_taxform.subject"
        )
      )
    end
  end

  def notify_user_has_earnings(opts)
    @opts = opts
    @person = Person.find_by(email: target_user)
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) do
      mail(
        to: target_user,
        subject: I18n.t(
          "tax_change_notifications.notification_of_tax_change_has_earnings.subject"
        )
      )
    end
  end

  private

  def target_user
    @target_user ||= @opts[:target_user]
  end
end
