class AdminTaxReportsMailer < BaseMailer
  def weekly_tax_withholdings_summary_report(report_csv_filepath, stats, mail_recipients)
    @report_period_start_date        = stats[:withholding_init_date]
    @report_period_end_date          = stats[:withholding_end_date]
    @total_withheld_accounts_count   = stats[:total_withheld_accounts]
    @currency_wise_withheld_amount = stats[:currency_wise_withheld_amount]
    @irs_transaction_id = stats[:irs_transaction_id]

    attachments[File.basename(report_csv_filepath)] = File.read(report_csv_filepath)

    mail(
      to: mail_recipients.split(","),
      subject: "Backup Withholding Summary Report for #{@report_period_end_date}"
    )
  end
end
