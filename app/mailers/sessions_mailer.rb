class SessionsMailer < BaseMailer
  layout "customer_email"

  def suspicious_login_mailer(person, request, timestamp, session_info = {})
    setup_email(person)
    set_session_info_variables(request, session_info)
    @person               = person
    @request_time         = timestamp.strftime("%l:%M %p")
    @password_reset_link  = new_reset_password_url(host: @person.country_website.url)
    @info_link            = "https:#{knowledgebase_link_for('two-factor-auth-faq', @person.country_domain)}"
    @help_link            = help_link(@person)

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) do
      subject = I18n.t("sessions_mailer.suspicious_login_mailer.subject")
      mail(to: @recipients, subject: subject, from: "no-reply@tunecore.com")
    end
  end

  private

  def help_link(person)
    links = {
      US: "https://support.tunecore.com/hc/en-us/requests/new?ticket_form_id=360000718752",
      AU: "https://support.tunecore.com/hc/en-au/requests/new?ticket_form_id=360000718752",
      UK: "https://support.tunecore.com/hc/en-gb/requests/new?ticket_form_id=360000718752",
      CA: "https://support.tunecore.com/hc/en-ca/requests/new?ticket_form_id=360000718752",
      DE: "https://support.tunecore.com/hc/de/requests/new?ticket_form_id=360000718752",
      FR: "https://support.tunecore.com/hc/fr-fr/requests/new?ticket_form_id=360000718752",
      IT: "https://support.tunecore.com/hc/it/requests/new?ticket_form_id=360000718752"
    }.with_indifferent_access

    links[person&.country_domain] || links["US"]
  end

  def set_session_info_variables(request, session_info)
    session_info = session_info.with_indifferent_access
    request_info = session_info_from_request(request)

    @browser = session_info[:browser] || request_info[:browser]
    @ip = session_info[:ip_address] || request_info[:ip_address]
    @location = session_info[:location] || request_info[:location]
  end

  def session_info_from_request(request)
    return {} if request.blank?

    detection = UserDetectionService.new(request)

    {
      browser: detection.browser_name,
      ip_address: detection.ip,
      location: detection.location
    }
  end
end
