class CheckTransferMailer < BaseMailer
  # email sent for success
  def confirmation(check_transfer_id)
    @check_transfer          = CheckTransfer.find(check_transfer_id)
    @person                  = @check_transfer.person
    @recipients              = @person.email
    @subject                 = I18n.t(".check_transfer_mailer.confirmation.subject")
    @domain                  = @person.domain

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
