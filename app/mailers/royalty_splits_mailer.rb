# frozen_string_literal: true

class RoyaltySplitsMailer < BaseMailer
  after_action :check_feature_flag

  layout :select_layout
  helper :royalty_split

  # NOTE: Be careful not to use @recipients (plural) since this is
  # overwritten by setup_email.

  def new_split_invite(recipient_id:, locale:)
    @recipient = RoyaltySplitRecipient.find(recipient_id)
    @split = @recipient.royalty_split
    @owner = @split.owner
    @email = @recipient.display_email
    @locale = @recipient.person? ? @recipient.person.locale : locale
    @locale_person = @recipient.person || @owner
    @country_website = @locale_person.country_website
    @product = Product.find_by(
      display_name: Product::SPLITS_COLLABORATOR_DISPLAY_NAME,
      country_website: @country_website
    )
    I18n.with_locale(@locale) do
      @product_price = @product.price_for_site(@country_website)
      # Works because @recipient responds to ".email"
      setup_email(@recipient)
      mail(to: @email, subject: t(".subject", owner_name: @owner.name))
    end
  end

  def split_percent_changed(recipient_id:, from:)
    @recipient = RoyaltySplitRecipient.find(recipient_id)
    @split = @recipient.royalty_split
    @owner = @split.owner
    @from = from
    @person = @recipient.person
    @split_title = @recipient.royalty_split.title
    setup_email(@person)
    I18n.with_locale(@person.locale) { mail(to: @person.email, subject: t(".subject")) }
  end

  def removed_from_split(person_id:, owner_id:, title:, song_ids:)
    @owner = Person.find(owner_id)
    @person = Person.find(person_id)
    @songs = Song.find(song_ids)
    @split_title = title
    setup_email(@person)
    I18n.with_locale(@person.locale) { mail(to: @person.email, subject: t(".subject")) }
  end

  def split_songs_added(split_id:, new_song_ids:)
    @split = RoyaltySplit.find(split_id)
    @songs = Song.find(new_song_ids)
    @album = @songs.first.album
    @to_people = @split.not_owner_recipients.select(&:person?).map(&:person)
    @owner = @split.owner
    # TODO: Is calling mail multiple times an anti-pattern?
    @to_people.each do |person|
      @person = person
      setup_email(@person)
      I18n.with_locale(@person.locale) { mail(to: @person.email, subject: t(".subject")) }
    end
  end

  private

  def check_feature_flag
    return true if FeatureFlipper.show_feature?(:royalty_splits_email, @owner)

    message.perform_deliveries = false
  end

  def select_layout
    return "dark_email_layout" if action_name == "new_split_invite"

    "customer_email"
  end
end
