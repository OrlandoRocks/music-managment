class PayoutTransferMailer < BaseMailer
  layout "customer_email"

  def mail_for_approved_transfer(payout_transfer_id)
    send_mail("models.payout_transfer.mail.transfer_approval_subject", payout_transfer_id)
  end

  def mail_for_failed_transfer(payout_transfer_id)
    send_mail("models.payout_transfer.mail.transfer_rejection_subject", payout_transfer_id)
  end

  def mail_for_rejected_transfer(payout_transfer_id)
    send_mail("models.payout_transfer.mail.transfer_rejection_subject", payout_transfer_id)
  end

  def mail_for_confirmed_transfer(payout_transfer_id)
    send_mail("models.payout_transfer.mail.transfer_confirmation_subject", payout_transfer_id)
  end

  private

  def send_mail(subject, payout_transfer_id)
    @payout_transfer = PayoutTransfer.find(payout_transfer_id)
    @person = @payout_transfer.person
    setup_email(@person)

    @subject += I18n.t(subject)
    @email = @person.email

    I18n.with_locale(@person.locale) do
      mail(to: @email, subject: @subject)
    end
  end
end
