class YtsrMailer < BaseMailer
  layout "customer_email"

  def ytsr_transactional_email(person_id)
    @person = Person.find(person_id)
    setup_email(@person)
    subject = "Your May 2019 YouTube Sound Recording Payment"
    recipients = @person.email

    mail to: recipients, subject: subject
  end
end
