class SocialNotifier < BaseMailer
  layout "customer_email"

  def upgrade_confirmation(person_id, params, domain = "www.tunecore.com")
    @person            = Person.find(person_id)
    @payment_amount    = Money.new(params[:payment_amount], @person.currency)
    @renewal_text      = set_renewal_period_language(params[:plan_expires_at], params[:renewal_period])
    @view_orders_link  = "#{edit_person_url(@person, host: domain)}/?tab=history"
    @domain            = domain
    @recipients        = @person.email
    @subject           = I18n.t("social_notifier.upgrade_confirmation.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def apple_purchase(person_id, params, domain = "www.tunecore.com")
    @person           = Person.find(person_id)
    @plan_expires_at  = Date.strptime(params[:plan_expires_at])
    @domain           = domain
    @recipients       = @person.email
    @subject          = I18n.t("social_notifier.apple_purchase.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def android_purchase(person_id, params, domain = "www.tunecore.com")
    @person           = Person.find(person_id)
    @plan_expires_at  = Date.strptime(params[:plan_expires_at])
    @domain           = domain
    @recipients       = @person.email
    @subject          = I18n.t("social_notifier.android_purchase.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def tunecore_initial_grace_period(person_id)
    @person                   = Person.find(person_id)
    @status                   = Social::PlanStatus.for(@person)
    @domain                   = @person.domain
    @recipients               = @person.email
    @payment_link_text        = I18n.t("social_notifier.payment_information")
    @subject                  = I18n.t("social_notifier.tunecore_initial_grace_period.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def tunecore_second_grace_period(person_id)
    @person                   = Person.find(person_id)
    @status                   = Social::PlanStatus.for(@person)
    @domain                   = @person.domain
    @recipients               = @person.email
    @subscription_term        = @status.subscription_status.product_type
    @payment_link_text        = I18n.t("social_notifier.payment_information")
    @payment_preferences_link = "#{edit_person_url(@person, host: @domain)}/?tab=payment"
    @grace_period_end         = @status.subscription_status.days_until_grace_period_ends
    @subject                  = I18n.t("social_notifier.tunecore_second_grace_period.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def tunecore_final_grace_period(person_id)
    @person              = Person.find(person_id)
    @status              = Social::PlanStatus.for(@person)
    @domain              = @person.domain
    @recipients          = @person.email
    @subscription_term   = @status.subscription_status.product_type
    @dashboard_link_text = I18n.t("social_notifier.log_in_to_social")
    @dashboard_link    = tc_social_index_url(host: @domain)
    @distro_status     = (@status.plan == "pro_expired_free") ? "with_distro" : "without_distro"
    @subject           = I18n.t("social_notifier.tunecore_final_grace_period.subject_#{@distro_status}")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def apple_initial_grace_period(_); end

  def apple_second_grace_period(_); end

  def apple_final_grace_period(person_id)
    @person              = Person.find(person_id)
    @status              = Social::PlanStatus.for(@person)
    @domain              = @person.domain
    @recipients          = @person.email
    @dashboard_link_text = I18n.t("social_notifier.log_in_to_social")
    @dashboard_link    = tc_social_index_url(host: @domain)
    @distro_status     = (@status.plan == "pro_expired_free") ? "with_distro" : "without_distro"
    @subject           = I18n.t("social_notifier.apple_final_grace_period.subject_#{@distro_status}")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def pro_user_cancellation(person_id, domain = "www.tunecore.com")
    @person           = Person.find(person_id)
    @domain           = domain
    @recipients       = @person.email
    @plan_status      = ::Social::PlanStatus.for(@person)
    @termination_date = @plan_status.plan_expires_at
    @subject          = I18n.t("social_notifier.pro_user_cancellation.subject")
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def set_renewal_period_language(exp_date, renewal_period)
    if renewal_period == "monthly"
      day = Date.strptime(exp_date).day
      I18n.t("social_notifier.upgrade_confirmation.monthly", { day: day })
    else
      date = exp_date.to_date.strftime("%B %d")
      I18n.t("social_notifier.upgrade_confirmation.yearly", { date: date })
    end
  end
end
