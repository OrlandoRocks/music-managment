class BatchNotifier < BaseMailer
  layout "customer_email", only: [:renewal_reminder, :takedown, :batch_failure_notice]

  def renewal_payment_thank_you(person, invoice, domain = "www.tunecore.com")
    @recipients                = person.email
    @subject                   = I18n.t("models.batch_notifier.renewal_payment_confirmation")
    @person                    = person
    @invoice                   = invoice
    @domain                    = domain
    @purchases                 = invoice.purchases
    @renewal_grace_days        = Renewal::GRACE_DAYS_BEFORE_TAKEDOWN
    @contains_album_renewals   = invoice.contains_album_renewals?
    @credit_card_payments      = invoice.braintree_transactions.where("status='success'").includes(:stored_credit_card)
    @paypal_payments           = invoice.paypal_transactions.where("status='Completed'")
    @stored_paypal_account     = person.preferred_paypal_account
    @tunecore_balance_payments = PersonTransaction.where(
      [
        "(target_type = 'Invoice' and target_id = ?) OR (target_type = 'BatchTransaction' and target_id =?)",
        invoice.id,
        !invoice.batch_transactions.empty? ? invoice.batch_transactions.last.id : 0
      ]
    )

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def renewal_reminder(person, subject, renewals, expiration_date)
    @recipients      = person.email
    @subject         = subject
    @person          = person
    @renewals        = renewals
    @renewal_ids     = renewals.map(&:id)
    @pre_expiration  = Date.today < expiration_date
    @expiration_date = expiration_date
    @state           = renewal_reminder_state
    @takedown        = false

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject, "X-SMTPAPI" => { category: ["RenewalReminder"] }.to_json) }
  end

  def takedown(person, renewals, takedown_date)
    @recipients   = person.email
    @subject      = I18n.t("models.batch_notifier.your_music_removed", locale: COUNTRY_LOCALE_MAP[person.country_domain].to_sym)
    @person       = person
    @renewals     = renewals
    @domain       = person.domain
    @takedown     = true
    @renewal_grace_days = Renewal::GRACE_DAYS_BEFORE_TAKEDOWN
    @takedown_date = takedown_date

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def publishing_update(person)
    @recipients = person.email
    @subject    = I18n.t("models.batch_notifier.pub_admin_update")
    @person     = person

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def batch_failure_notice(invoice)
    person         = invoice.person

    # Set the payment method
    payment_method = nil
    payment_method =
      if invoice.person.renew_with_credit_card?
        :credit_card
      elsif invoice.person.renew_with_paypal?
        :paypal
      elsif !invoice.person.autorenew?
        :do_not_auto_renew
      else
        :no_payment_method
      end

    @recipients     = person.email
    @subject        = I18n.t("models.batch_notifier.overdue_renewal")
    @person         = person
    @invoice        = invoice
    @payment_method = payment_method
    @renewals       = invoice.purchases.collect(&:renewal)
    @pre_expiration = false
    @takedown       = false

    debugging_airbrake if @renewals.nil?

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def daily_log(options = {})
    for_date    = !options[:date].nil? ? options[:date] : Time.now
    @recipients =  options[:email] || ["ops@tunecore.com"]
    @subject    =  "Today's log - #{for_date.strftime('%m/%d/%Y')}"

    text_body = `grep "#{for_date.strftime("%Y-%m-%d")}.*Payment Batch" #{Rails.root}/log/#{BATCH_LOG_FILE}`
    attachments["todays-batch-log-#{for_date.strftime('%m-%d-%Y')}.txt".to_sym] = text_body

    mail(to: @recipients, subject: @subject)
  end

  private

  def renewal_reminder_state
    return "post-expiration" unless @pre_expiration

    auto_renew_status
  end

  def auto_renew_status
    return "autorenew off" unless @person.autorenew?

    preferences_status
  end

  def preferences_status
    return "incomplete" if @person.person_preference.blank?

    incomplete_or_paypal_or_creditcard_autorenew
  end

  def incomplete_or_paypal_or_creditcard_autorenew
    return "autorenew" if paypal_on_file?
    return credit_card_status if credit_card_on_file?

    "incomplete"
  end

  def credit_card_status
    return "expired credit card" if @person.preferred_credit_card.expires_before_date?(@expiration_date)

    "autorenew"
  end

  def paypal_on_file?
    @person.person_preference.preferred_payment_type == "PayPal" &&
      @person.person_preference.preferred_paypal_account.present?
  end

  def credit_card_on_file?
    @person.person_preference.preferred_payment_type == "CreditCard" &&
      @person.person_preference.preferred_credit_card.present?
  end

  def debugging_airbrake
    Airbrake.notify(
      "Renewals failure notice cannot find renewal purchases",
      {
        recipients: @recipients,
        subject: @subject,
        person: @person,
        invoice: @invoice,
        payment_method: @payment_method,
        purchases: @invoice.purchases
      }
    )
  end
end
