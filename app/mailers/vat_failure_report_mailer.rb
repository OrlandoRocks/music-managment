# frozen_string_literal: true

class VatFailureReportMailer < BaseMailer
  layout "admin_email"

  def report(analyzer)
    @analyzer = analyzer
    @subject = generate_subject
    @recipients = ENV.fetch("VAT_FAILURE_REPORT_RECIPIENTS").split(",")

    mail(to: @recipients, subject: @subject)
  end

  private

  def generate_subject
    if @analyzer.start_date == @analyzer.end_date
      "VAT failure report for #{@analyzer.start_date.to_date}"
    else
      "VAT failure report for date range " \
      "#{@analyzer.start_date.to_date} to #{@analyzer.end_date.to_date}"
    end
  end
end
