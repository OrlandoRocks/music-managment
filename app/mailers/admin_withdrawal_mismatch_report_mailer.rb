# frozen_string_literal: true

class AdminWithdrawalMismatchReportMailer < BaseMailer
  def daily_withdrawal_amount_mismatch_report(report_csv, mail_recipients)
    @report_timestamp = DateTime.current.strftime("%m_%d_%Y")
    report_filename  = "daily_withdrawal_amount_mismatch_report_#{@report_timestamp}.csv"
    subject_line     = "Withdrawal Mismatch Report - #{@report_timestamp}"
    subject_line = ("[LOWER ENV TEST REPORT] " + subject_line) unless Rails.env.production?
    attachments[report_filename] = { mime_type: "text/csv", content: report_csv }

    mail(to: mail_recipients.split(","), subject: subject_line)
  end
end
