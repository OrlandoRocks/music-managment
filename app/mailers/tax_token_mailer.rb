class TaxTokenMailer < BaseMailer
  layout "customer_email"

  def tax_token_notification(tax_token_id)
    @tax_token = TaxToken.find(tax_token_id)
    @tax_token_cohort = @tax_token.tax_tokens_cohort
    @person = @tax_token.person
    setup_email(@person)

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(to: @recipients, subject: I18n.t("tax_token_mailer.tax_token_notification.subject"))
    end
  end
end
