class EftNotifier < BaseMailer
  include Admin::EftBatchHelper

  # email sent for failure/error/canceled
  def failure_notice(eft_batch_transaction_id, message = nil)
    transaction  = EftBatchTransaction.find(eft_batch_transaction_id)
    person       = transaction.stored_bank_account.person
    @recipients  = person.email
    @subject     = I18n.t("models.eft_notifier.customer_care")

    @person                = person
    @domain                = person.domain
    @eft_batch_transaction = transaction
    @stored_bank_account   = transaction.stored_bank_account
    @notice_type           = transaction.status
    @failure_fee_charged   = transaction.status == "failure"
    @message               = message

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  # email sent for success
  def success_notice(eft_batch_transaction_id)
    transaction =  EftBatchTransaction.includes(:stored_bank_account).find(eft_batch_transaction_id)
    person      =  transaction.stored_bank_account.person

    @recipients   = person.email
    @subject      = I18n.t("models.eft_notifier.withdraw_success")

    @person                 = person
    @domain                 = person.domain
    @eft_batch_transaction  = transaction
    @stored_bank_account    = transaction.stored_bank_account

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  # email sent for success
  def confirmation(eft_batch_transaction_id)
    transaction              = EftBatchTransaction.includes(:stored_bank_account).find(eft_batch_transaction_id)
    person                   = transaction.stored_bank_account.person
    @recipients              = person.email
    @subject                 = I18n.t("models.eft_notifier.withdraw_confirmation")
    @person                  = person
    @domain                  = person.domain
    @eft_batch_transaction   = transaction
    @stored_bank_account     = transaction.stored_bank_account
    @eft_expected_days_lower = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_LOWER
    @eft_expected_days_upper = EftBatchTransaction::ESTIMATED_BUSINESS_DAYS_FOR_DEPOSIT_UPPER

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  # email sent for failure/error/rejection
  def admin_error_notice(log_message, eft_batch_transaction_id = nil)
    transaction = !eft_batch_transaction_id.nil? ? EftBatchTransaction.includes(:stored_bank_account).find(eft_batch_transaction_id) : nil
    @recipients   = EftBatchTransaction::ADMIN_EMAILS
    @subject      = "EFT Error notice"
    @eft_batch_transaction = transaction
    @message = log_message

    mail(to: @recipients, subject: @subject)
  end

  # summary email for EFT withdrawal batchs
  def withdrawal_batch_summary(batch_id)
    batch = EftBatch.find(batch_id)

    # csv file of transactions with status of 'error'
    errored_transactions = batch.eft_batch_transactions.where(status: "error")

    unless errored_transactions.empty?
      filename = "eft_withdrawal_batch_errors_" + batch.batch_sent_at.strftime("%Y-%m-%d-#{batch.id}-#{batch.id}") + ".csv"
      filepath = BRAINTREE_BATCH[:local_reports] + filename

      CSV.open(filepath, "w") do |csv|
        csv << [
          "Batch ID",
          "Transaction ID",
          "Person ID",
          "Stored Bank Account ID",
          "Bank Name",
          "Name on Bank Account",
          "Stored Bank Acount Type",
          "Stored Bank Account Status",
          "Customer Name",
          "Customer Phone",
          "Customer Email",
          "Amount",
          "Service Fee"
        ]
        batch.eft_batch_transactions.where(status: "error").each do |txn|
          csv << [
            txn.eft_batch_id,
            txn.id,
            txn.person.id,
            txn.stored_bank_account.id,
            txn.stored_bank_account.bank_name,
            txn.stored_bank_account.name,
            txn.stored_bank_account.account_type,
            txn.stored_bank_account.status,
            txn.person.name,
            txn.stored_bank_account.phone,
            txn.person.email,
            txn.amount,
            txn.service_fee
          ]
        end
      end
    end

    @recipients    = EftBatch::WITHDRAWAL_BATCH_RECIPIENTS
    @subject       = "EFT Withdrawal Batch Summary"
    @eft_batch     = batch
    @eft_approvals = EftBatchTransaction.approval_summary.first
    @has_errors    = !errored_transactions.empty?

    text_body = get_text_body("#{batch.batch_sent_at.strftime('%Y-%m-%d')}.*EFT Batch")

    attachments["batch-log-#{batch.batch_sent_at.strftime('%m-%d-%Y')}-#{batch.id}.txt"] = text_body

    attachments[filename] = File.read(filepath) unless errored_transactions.empty?

    mail(to: @recipients, subject: @subject)
  end

  # summary email for EFT Daily Query
  def daily_query_summary(query_id)
    query = EftQuery.find(query_id)

    failed_transactions = query.eft_batch_transactions.where(status: "failure")

    unless failed_transactions.empty?
      # csv file of transactions with status of 'error'
      filename = "daily_query_failures_" + query.created_at.strftime("%Y-%m-%d-#{query.id}-#{query.id}") + ".csv"
      filepath = BRAINTREE_BATCH[:local_reports] + filename

      CSV.open(filepath, "w") do |csv|
        csv << [
          "Batch ID",
          "Transaction ID",
          "Person ID",
          "Stored Bank Account ID",
          "Bank Name",
          "Name on Bank Account",
          "Stored Bank Acount Type",
          "Stored Bank Account Status",
          "Customer Name",
          "Customer Email",
          "Amount",
          "Service Fee"
        ]
        query.eft_batch_transactions.where(status: "failure").each do |txn|
          csv << [
            txn.eft_batch_id,
            txn.id,
            txn.person.id,
            txn.stored_bank_account.id,
            txn.stored_bank_account.bank_name,
            txn.stored_bank_account.name,
            txn.stored_bank_account.account_type,
            txn.stored_bank_account.status,
            txn.person.name,
            txn.stored_bank_account.phone,
            txn.person.email,
            txn.amount,
            txn.service_fee
          ]
        end
      end
    end

    @recipients   = EftBatch::WITHDRAWAL_BATCH_RECIPIENTS
    @subject      = "EFT Daily Query Summary"
    @eft_query    = query
    @has_failures = !failed_transactions.empty?

    text_body = get_text_body("#{query.created_at.strftime('%Y-%m-%d')}.*EFT Query")

    attachments["eft-query-log-#{query.created_at.strftime('%m-%d-%Y')}-#{query.id}.txt"] = text_body

    attachments[filename] = File.read(filepath) unless failed_transactions.empty?

    mail(to: @recipients, subject: @subject)
  end

  def payout_fee_change_notice(payout_service_fee_id)
    service_fee = PayoutServiceFee.find(payout_service_fee_id)

    @recipients = PayoutServiceFee::PAYOUT_FEE_RECIPIENTS
    @subject    = "NOTICE: A Payout Service Fee has changed"

    @person     = service_fee.posted_by_name
    @fee_type   = service_fee.fee_type
    @amount     = service_fee.amount
    @created_on = service_fee.created_at
    @currency   = service_fee.currency

    mail(to: @recipients, subject: @subject)
  end

  def get_text_body(query)
    `grep "#{query}" #{Rails.root}/log/#{BATCH_LOG_FILE}`
  end
end
