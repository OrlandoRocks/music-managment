class CheckTransferExportMailer < BaseMailer
  def check_export(export, emails)
    @recipients = emails
    @subject    = "checks for #{export.time}"
    @export     = export

    attachments["tunecore_#{File.basename export.filename}"] = File.read(export.filename)
    mail(to: @recipients, subject: @subject)
  end
end
