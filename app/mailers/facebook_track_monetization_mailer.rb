class FacebookTrackMonetizationMailer < BaseMailer
  layout "customer_email"

  def mail_for_sign_up_and_select(person_id)
    @person = Person.find(person_id)
    setup_email(@person)
    @host = @person.country_website.url
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(
        to: @recipients,
        subject: I18n.t("facebook_track_monetization_mailer.mail_for_sign_up_and_select.subject")
      )
    end
  end

  def mail_for_sign_up_and_select_all(person_id)
    @person = Person.find(person_id)
    setup_email(@person)
    @host = @person.country_website.url
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      mail(
        to: @recipients,
        subject: I18n.t("facebook_track_monetization_mailer.mail_for_sign_up_and_select_all.subject")
      )
    end
  end
end
