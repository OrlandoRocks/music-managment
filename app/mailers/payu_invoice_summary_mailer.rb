class PayuInvoiceSummaryMailer < BaseMailer
  include Logging
  layout "admin_email"

  # daily summary email for tunecore .in PayU invoices
  def invoice_summary
    @date = Date.current
    @settled_invoices  = Invoice.joins(:invoice_settlements)
                                .where(invoice_settlements: { source_type: PayuTransaction.to_s })
                                .where(
                                  invoices: {
                                    created_at: @date.beginning_of_day..@date.end_of_day
                                  }
                                )
    @uploaded_invoices = @settled_invoices.where(response_sql("in"), PayuInvoiceUploadResponse::SUCCESS_RESPONSE_CODE)
    @failed_invoices = @settled_invoices.where(response_sql("not in"), PayuInvoiceUploadResponse::SUCCESS_RESPONSE_CODE)
                                        .select(:id, :person_id, :settled_at, :final_settlement_amount_cents, :currency)

    @recipients    = ENV.fetch("PAYU_INVOICE_SUMMARY_RECIPIENTS")
    @subject       = "PayU Invoice Summary"

    mail(to: @recipients, subject: @subject)
  end

  def response_sql(in_or_out)
    <<-SQL
    invoices.id #{in_or_out} (
      select distinct#{' '}
      payu_transactions.invoice_id#{' '}
      from payu_invoice_upload_responses
      join payu_transactions on payu_transactions.id = payu_invoice_upload_responses.related_id and payu_invoice_upload_responses.related_type = "PayuTransaction"#{' '}
      where payu_invoice_upload_responses.api_response_code = ?
    )
    SQL
  end
end
