class AccountActivityMailer < BaseMailer
  default from: "TuneCore <account-activity@tunecore.com>"
  include CurrencyHelper

  layout "customer_email"

  def inactivity_email(person_id, balance_history_url)
    @person = Person.find(person_id)
    setup_email(@person)

    @name = @person.name
    @current_date = Time.current.strftime("%m/%d/%Y") # TODO: create date helper to format dates for each country website
    @cutoff_date = (Time.current + 2.weeks).strftime("%m/%d/%Y") # TODO: create date helper to format dates for each country website
    @balance = balance_to_currency(Money.new(@person.balance * 100), iso_code: @person.country_domain)
    @balance_history_url = balance_history_url
    @tc_email = "account-activity@tunecore.com"

    @subject += I18n.t("account_activity_mailer.inactivity_email.subject", { balance: @balance })

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def inactivity_email_for_targeted_users(person_id, balance_history_url)
    @person = Person.find(person_id)
    setup_email(@person)

    @name = @person.name
    @current_date = Time.current.strftime("%m/%d/%Y") # TODO: create date helper to format dates for each country website
    @cutoff_date = (Time.current + 2.weeks).strftime("%m/%d/%Y") # TODO: create date helper to format dates for each country website
    @balance = balance_to_currency(Money.new(@person.balance * 100), iso_code: @person.country_domain)
    @balance_history_url = balance_history_url
    @tc_email = "account-activity@tunecore.com"

    @subject += I18n.t("account_activity_mailer.inactivity_email_for_targeted_users.subject", { balance: @balance })

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
