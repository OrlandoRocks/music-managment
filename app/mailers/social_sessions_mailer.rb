class SocialSessionsMailer < BaseMailer
  layout "customer_email"

  def suspicious_login_mailer(person, params, timestamp)
    setup_email(person)
    @person               = person
    @device_type          = params.fetch(:device_type)
    @ip                   = params.fetch(:ip)
    @location             = fetch_location
    @request_time         = timestamp.strftime("%l:%M %p")

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) do
      subject = I18n.t("sessions_mailer.suspicious_login_mailer.subject")
      mail(to: @recipients, subject: subject, from: "no-reply@tunecore.com")
    end
  end

  private

  def fetch_location
    result = ::Geocoder.search(@ip).first
    return if result.nil? || (result.city.nil? && result.state.nil? && result.country.nil?)

    location ||= [[result.city, result.state].join(", "), result.country].join(" ")
  end
end
