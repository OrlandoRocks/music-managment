class PubOptOutMailer < BaseMailer
  def mail_for_regular_user(person_id)
    @person = Person.find(person_id)
    setup_email(@person)
    @composer = @person.composers.first
    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_website.country]) do
      pdf = WickedPdf.new.pdf_from_string(
        render_to_string("pub_opt_out_mailer/mail_attachment_for_regular_user")
      )
      attachments["pub_opt_out.pdf"] = pdf

      mail(to: @recipients, subject: I18n.t("pub_opt_out.mail_for_regular_user.subject"))
    end
  end

  def mail_for_high_earnings_user(person_id)
    @person = Person.find(person_id)
    @composer = @person.composers.first

    mail(to: "pub_opt_out@tunecore.com", subject: "High Earner Opt Out")
  end
end
