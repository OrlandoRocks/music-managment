class PublishingCatalogNotifier < BaseMailer
  def catalog_reports(report_filenames, subject, content = {})
    @recipients = PublishingReport::CATALOG_RECIPIENTS
    @subject    = subject
    @total_composers = content[:total_composers]
    @total_songs     = content[:total_songs]
    report_filenames.each do |filename|
      attachments[filename.split("/").last] = File.read(filename)
    end
    mail(to: @recipients, subject: @subject)
  end
end
