class TakedownNotifier < BaseMailer
  layout "customer_email", except: [:albums_for_takedown]

  def albums_for_takedown(albums = [], options = { recipients: [], store_id: nil })
    return if albums.empty? || options[:store_id].nil?

    store = Store.find(options[:store_id])

    return if store.nil?

    @recipients = options[:recipients]
    @bcc        = AccountSystem.config[:email_from_for_takedown]
    @from       = AccountSystem.config[:email_from_for_takedown]
    @subject    = "Albums for Takedown"

    # save the store so we can access it in the template
    @store = store

    filename = "takedown_for_expired_albums" + "_#{store.short_name}_" + Time.now.strftime("%Y_%m_%d_%H%M%S") + ".csv"
    filepath = TAKEDOWN_REPORT_PATH + "/#{filename}"

    CSV.open(filepath, "w") do |csv|
      csv << ["StoreName", "TakenDownAt", "TunecoreUPC",	"OptionalUPC",	"ArtistName",	"AlbumName"]
      albums.each do |album|
        spt = album.salepoints.find { |s| s.store_id == store.id }
        optional_upc = album.optional_upcs.first.nil? ? "" : album.optional_upcs.first.number
        csv << [
          store.name,
          spt.takedown_at.strftime("%Y-%m-%d %H:%M:%S"),
          album.tunecore_upc.number,
          optional_upc,
          album.artist_name,
          album.name
        ]
      end
    end

    attachments[filename] = File.read(filepath)

    mail(to: @recipients, bcc: @bcc, from: @from, subject: @subject, template_name: "takedown_for_expired_albums")
  end

  def takedown_notification(person, albums)
    @recipients   = person.email
    @subject      = "Takedown Confirmation (#{albums.size} release#{'s' if albums.size > 1})"
    @content_type = "text/html"
    @person       = person
    @albums       = albums

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  def streaming_fraud_notification(params)
    @from = AccountSystem.config[:email_from_for_streaming_issues]
    @person       = params[:person]
    @recipients   = @person.email
    @subject = "Streaming Fraud"
    @content_type = "text/html"
    @albums       = params[:albums]
    @store_names  = (params[:stores] == "All Stores") ? params[:stores] : Store.where(id: params[:stores]).map(&:name)

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, from: @from, subject: @subject) }
  end

  def chargeback_on_renewals_notification(params)
    @from = AccountSystem.config[:email_from_for_copyright_issues]
    @person       = params[:person]
    @recipients   = @person.email
    @subject = "Chargeback On Renewals"
    @content_type = "text/html"

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, from: @from, subject: @subject) }
  end

  def internal_review_for_rights_notification(params)
    @from = AccountSystem.config[:email_from_for_copyright_issues]
    @person       = params[:person]
    @recipients   = @person.email
    @subject = "Internal Review For Rights"
    @content_type = "text/html"
    @albums       = params[:albums]

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, from: @from, subject: @subject) }
  end

  def internal_claim_from_stores_notification(params)
    @from = AccountSystem.config[:email_from_for_copyright_issues]
    @person       = params[:person]
    @recipients   = @person.email
    @subject = "Internal Claim from Stores"
    @content_type = "text/html"
    @albums       = params[:albums]

    I18n.with_locale(COUNTRY_LOCALE_MAP[@person.country_domain]) { mail(to: @recipients, from: @from, subject: @subject) }
  end
end
