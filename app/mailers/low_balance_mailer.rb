# frozen_string_literal: true

class LowBalanceMailer < BaseMailer
  layout "customer_email"

  def low_balance(params)
    to             = params[:to]
    @account_name  = params[:person].email
    @amount        = params[:amount]
    @person        = params[:person]

    subject        = "TuneCore low balance notification"

    mail(to: to, subject: subject)
  end
end
