class AutomatorMailer < BaseMailer
  MAX_ALBUMS_TO_SHOW = 25

  layout "customer_email"

  def store_launch(store, person, release_list)
    @recipients       = person.email
    @subject          = I18n.t("models.automator_mailer.new_store", { store_name: store.name })
    @store            = store
    @person           = person
    @release_list     = release_list
    @albums_to_show   = [MAX_ALBUMS_TO_SHOW, release_list.size].min
    @add_csv          = release_list.size > MAX_ALBUMS_TO_SHOW

    if @add_csv
      csv_string =
        CSV.generate do |csv|
          csv << ["Artist Name", "Relase Name", "Release Type", "UPC"]
          release_list.each do |release|
            csv << [release["artist_name"], release["album_name"], release["album_type"], release["upc"]]
          end
        end

      attachments["releases.csv"] = csv_string
    end

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end
end
