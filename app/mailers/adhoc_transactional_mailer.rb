class AdhocTransactionalMailer < BaseMailer
  @@logger = ActiveSupport::Logger.new("log/adhoc_transactional_emailer.log")

  attr_reader :variables
  attr_reader :template_string

  def deliver_multiple_from_csv(csv_data, template_string, options = {})
    @csv_data         = csv_data
    @template_string  = template_string
    @variables        = define_variables(csv_data.shift)
    @subject          = options[:subject] || "Notification From TuneCore, Inc."
    unique_emails     = {}
    csv_data.each_with_index do |recipient_row, csv_index|
      # avoid sending duplicates
      if unique_emails[recipient_row.first]
        @@logger.info "#{Time.zone.now}: #{recipient_row.first} on line #{csv_index + 1} is a duplicate entry. skipping..."
        next
      end

      # skip invalid email addresses
      unless valid_email?(recipient_row.first)
        @@logger.info "#{Time.zone.now}: #{recipient_row.first} is not a valid email address"
        next
      end

      # reset missing vars array for this email
      missing_variables = []
      variables.each_with_index do |variable, i|
        if recipient_row[i + 1].blank?
          missing_variables << variable
          next
        end
        instance_variable_set("@#{variable}", recipient_row[i + 1]) # + 1 to skip email address
      end

      # skip lines with missing data
      unless missing_variables.empty?
        @@logger.info "#{Time.zone.now}: #{recipient_row.first} missing the #{missing_variables.join(', ')} variables.  skipping..."
        next
      end

      # generate template using current binding/varibles
      @formatted_template = ERB.new(template_string).result(binding)

      # build mail object and deliver it
      mail = from_csv(recipient_row.first, @subject, @formatted_template)
      mail.deliver && unique_emails[recipient_row.first] = 1
    end
  end

  def from_csv(email_address, subject, formatted_template)
    mail(
      to: email_address,
      body: formatted_template,
      content_type: "text/html",
      subject: subject
    )
  end

  private

  def define_variables(header)
    header[1..-1].map(&:strip)
  end

  def valid_email?(address)
    address =~ /^([^@\s]+)@((?:[-a-z0-9\.]+\.)+[a-z]{2,})$/
  end
end
