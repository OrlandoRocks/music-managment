class ZendeskTicketCreateMailer < BaseMailer
  RECIPIENT = "content_forward@tunecore.com"

  def create_ticket(options)
    @subject    = "iTunes: Artist mapping request"
    @recipients = RECIPIENT
    @person ||= Person.find_by(id: options[:person_id])
    @artist ||= Artist.find_by(id: options[:artist_id])
    initialize_data
    mail(to: @recipients, subject: @subject)
  end

  private

  def initialize_data
    @genre ||= releases.order("release_date").pluck("primary_genre_name").first
    @apple_album_ids ||= releases.pluck("apple_id")
  end

  def releases
    @releases ||= MyArtist.releases_for("apple", @person.id, @artist.id)
  end
end
