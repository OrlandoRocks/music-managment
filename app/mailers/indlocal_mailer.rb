class IndlocalMailer < BaseMailer
  def monthly_invoice_statements(purchase_csv: nil, credit_note_csv: nil, options: {})
    ind_local_mailboxes = ENV.fetch("INDIA_INVOICE_RECIPIENTS", "test1@tunecore.com")
    finance_mailbox = ENV.fetch("TUNECORE_FINANCE_MAILBOX", "test2@tunecore.com")
    tc_mailbox = ENV.fetch("TC_INDIA_MAILBOX", "test3@tunecore.com")
    #  Example, for the monthly report run for the 1st of September the Subject would be:
    #  “TuneCore India Monthly Invoice Statements - August 2020”
    subject = "#{I18n.t('indlocal.invoice_statements_email.subject')} - #{options[:date].strftime('%B %Y')}"

    attachments["purchase_invoice_statements.csv"] = purchase_csv
    # commenting out credit_note_csv for now - we don't currently use it for tax calculation, but very well may
    # end up using it in future
    # attachments['credit_note_invoice_statements.csv'] = credit_note_csv
    mail(
      to: ind_local_mailboxes.split(","),
      bcc: [finance_mailbox, tc_mailbox],
      subject: subject
    )
  end
end
