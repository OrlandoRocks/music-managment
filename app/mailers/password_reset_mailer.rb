class PasswordResetMailer < BaseMailer
  layout "customer_email"

  def reset(person, options = {})
    setup_email(person)

    @subject += I18n.t("models.password_reset_mailer.reset_request")
    @name = person.name
    @email = person.email
    @person = person
    @url = options[:url]
    @initiator = options[:initiator]

    @hello_artist_key = build_full_key "hello_tunecore_artist"
    @reset_password_key = build_full_key "reset_your_password"
    @clicking_key = build_full_key "clicking"

    I18n.with_locale(COUNTRY_LOCALE_MAP[person.country_domain]) { mail(to: @recipients, subject: @subject) }
  end

  private

  # Keys look like password_reset_mailer.reset.hello_tunecore_artist
  # or password_reset_mailer.reset.admin.reset_your_password
  def key_prefixes
    {
      user: nil,
      social: nil,
      admin: "admin",
      system: "system"
    }
  end

  def common_key_parts
    ["password_reset_mailer", "reset", key_prefixes[@initiator]].compact
  end

  def build_full_key(key)
    [common_key_parts, key].flatten.join(".")
  end
end
