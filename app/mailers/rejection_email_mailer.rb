class RejectionEmailMailer < BaseMailer
  LEGAL_ISSUES_EMAIL = AccountSystem.config[:email_from_for_legal_issues]

  def changes_requested(review_audit)
    @album = review_audit.album
    @person = @album.person
    @email_template_paths = review_audit
                            .review_reasons
                            .pluck(:email_template_path)

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.changes_requested.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def rights_verification(review_audit)
    @album = review_audit.album
    @person = @album.person
    @email_template_paths = review_audit
                            .review_reasons
                            .pluck(:email_template_path)

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.rights_verification.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def album_blocked_from_sales(review_audit)
    @album = review_audit.album

    locale = COUNTRY_LOCALE_MAP[@album.person.country_domain]
    recipients = @album.person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.album_blocked_from_sales.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def album_will_not_be_distributed(review_audit)
    @album = review_audit.album

    locale = COUNTRY_LOCALE_MAP[@album.person.country_domain]
    recipients = @album.person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.album_will_not_be_distributed.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def denied_album_classical_album(review_audit)
    @album = review_audit.album
    @person = @album.person

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.denied_album_classical_album.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def denied_album_unsupported_language(review_audit)
    @album = review_audit.album
    @person = @album.person

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.denied_album_unsupported_language.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def denied_ringtone_submitted_textone(review_audit)
    @album = review_audit.album
    @person = @album.person

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.denied_ringtone_submitted_textone.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def denied_streaming_fraud(review_audit)
    @album = review_audit.album
    @person = @album.person

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.denied_streaming_fraud.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end

  def fb_ppp_rejection(review_audit)
    @album = review_audit.album
    @person = @album.person

    locale = COUNTRY_LOCALE_MAP[@person.country_domain]
    recipients = @person.email

    I18n.with_locale(locale) do
      mail(
        to: recipients,
        subject: I18n.t(".rejection_email_mailer.fb_ppp_rejection.subject", album_id: @album.id),
        from: LEGAL_ISSUES_EMAIL
      )
    end
  end
end
