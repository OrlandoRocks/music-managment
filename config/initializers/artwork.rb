case Rails.env
when 'production', 'staging', 'development'
  UPLOAD_ARTWORK_PATH        = '/public/images/artwork/upload/'
  OUTPUT_FULL_RES_TIFFS_PATH = '/public/images/artwork/complete/tiffs/'
  TIFF_FULLRES_PATH          = '/public/images/artwork/complete/tiffs/'
  OUTPUT_THUMBNAILS_PATH     = '/public/images/artwork/complete/thumbs/'
  OUTPUT_WEB_PATH            = '/public/images/artwork/complete/web/'
  WEB_THUMBNAILS_PATH        = '/images/artwork/complete/thumbs/'
  WEB_FULLRES_PATH           = '/images/artwork/complete/web/'


  # promotion images
  PROMOTION_FULL_IMAGES_PATH      = '/public/images/promotions/full/'
  PROMOTION_FULL_IMAGES_WEBPATH   = '/images/promotions/full/'
  PROMOTION_MINI_IMAGES_PATH      = '/public/images/promotions/mini/'
  PROMOTION_MINI_IMAGES_WEBPATH   = '/images/promotions/mini/'

  #store images
  PRODUCT_FULL_IMAGES_PATH      = '/public/images/products/full/'
  PRODUCT_FULL_IMAGES_WEBPATH   = '/images/products/full/'
  PRODUCT_MINI_IMAGES_PATH      = '/public/images/products/mini/'
  PRODUCT_MINI_IMAGES_WEBPATH   = '/images/products/mini/'

  UPLOAD_DIR    = '/public/media/upload/'

  MANUFACTURING_ARTWORK_PATH = '/public/images/artwork/manufacturing'

when 'test'
  UPLOAD_ARTWORK_PATH        = "tmp"
  OUTPUT_FULL_RES_TIFFS_PATH = '/tmp'
  TIFF_FULLRES_PATH          = '/tmp'
  OUTPUT_THUMBNAILS_PATH     = '/tmp'
  OUTPUT_WEB_PATH            = '/tmp'
  WEB_THUMBNAILS_PATH        = '/tmp/'
  WEB_FULLRES_PATH           = '/tmp'
  MANUFACTURING_ARTWORK_PATH = '/tmp'

  # promotion images
  PROMOTION_FULL_IMAGES_PATH      = '/tmp'
  PROMOTION_FULL_IMAGES_WEBPATH   = '/tmp'
  PROMOTION_MINI_IMAGES_PATH      = '/tmp'
  PROMOTION_MINI_IMAGES_WEBPATH   = '/tmp'


  #store images
  PRODUCT_FULL_IMAGES_PATH      = '/public/images/products/full/'
  PRODUCT_FULL_IMAGES_WEBPATH   = '/images/products/full/'
  PRODUCT_MINI_IMAGES_PATH      = '/public/images/products/mini/'
  PRODUCT_MINI_IMAGES_WEBPATH   = '/images/products/mini/'
  UPLOAD_DIR    = '/tmp'
end
