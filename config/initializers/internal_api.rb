DEFER_BELIEVE = true
$believe_api_client = Believe::ApiClient.new({
    "HOST"      => ENV["BELIEVE_API_HOST"],
    "API_KEY"   => ENV["BELIEVE_API_KEY"],
    "API_PW"    => ENV["BELIEVE_API_PW"],
    "API_DEBUG" => true,
    "DEFER"     => true
})

BELIEVE_CONFIG = {
  "FTP_HOST"  => ENV["BELIEVE_FTP_HOST"],
  "FTP_USER"  => ENV["BELIEVE_FTP_USER"],
  "FTP_PASS"  => ENV["BELIEVE_FTP_PASS"]
}

TC_SOCIAL_CONFIG = {
  "HOST"            => ENV["SOCIAL_API_HOST"],
  "API_KEY"         => ENV["SOCIAL_API_KEY"],
  "tunecore_key"    => ENV["SOCIAL_TUNECORE_KEY"],
  "tunecore_secret" => ENV["SOCIAL_TUNECORE_SECRET"]
}

$social_api_client = Faraday.new(url: ENV["SOCIAL_API_HOST"])
$social_api_client.authorization :Token, token: ENV["SOCIAL_API_KEY"]
$tc_reporter_client = Faraday.new(url: ENV["REPORTER_API_HOST"])
$tc_reporter_client.basic_auth(ENV["REPORTER_API_USERNAME"], ENV["REPORTER_API_PASSWORD"])
