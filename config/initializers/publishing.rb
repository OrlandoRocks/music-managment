# 2010-08-01 AK
case Rails.env
when 'production'
  # Used during development to set product name in migration, and to display proper 'thank you' page text.
  # Shouldn't be modified after deployment.
  SONGWRITER_PRODUCT_NAME="Songwriter Service"

  ROYALTY_PDF_DIRECTORY = "tmp/royalty_payments/"
  ROYALTY_CSV_DIRECTORY = "tmp/royalty_payments/"
when 'development', 'staging', 'test'
  SONGWRITER_PRODUCT_NAME = "Songwriter Service"
  ROYALTY_PDF_DIRECTORY   = "tmp/royalty_payments/"
  ROYALTY_CSV_DIRECTORY   = "tmp/royalty_payments/"
end
