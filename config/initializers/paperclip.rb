Paperclip.interpolates(:album_id_hash) do |attachment, style|
  attachment.instance.album_id.to_s.scan(/..?/).join("/") 
end