if !defined?(TRENDS_DB)
  TRENDS_CONNECTION_INFO = {
    adapter:  "mysql2",
    encoding: "utf8",
    port:     ENV["TRENDS_DB_PORT"],
    pool:     ENV["TRENDS_DB_POOL"],
    host:     ENV["TRENDS_DB_HOST"],
    username: ENV["TRENDS_DB_USER"],
    password: ENV["TRENDS_DB_PASS"],
    database: ENV["TRENDS_DB_NAME"]
  }
  TRENDS_DB   = ENV["TRENDS_DB_NAME"]
  TUNECORE_DB = ENV["TRENDS_TCW_DB_NAME"]
end
