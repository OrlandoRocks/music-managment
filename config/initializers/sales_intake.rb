case Rails.env
when 'production'
  SALES_INTAKES_ROOT         = '/home/tunecore/sales_intakes'
when 'staging'
  SALES_INTAKES_ROOT         = '/var/www/tunecore/shared/sales_intakes'
when 'development'
  user = `whoami`.chomp
  SALES_INTAKES_ROOT         = "/Users/#{user}/sales_intakes"
end
