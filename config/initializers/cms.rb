CONTENT_DIR_REL_PATH  = "app/views/shared/content/"
CONTENT_DIR_VIEW_PATH = "shared/content/"
CMS_LOG               = Logger.new("#{Rails.root}/log/cms.log")
CMS_BUCKET_NAME       = ENV['CMS_BUCKET_NAME']
cms_yml               = YAML.load_file(File.join(Rails.root, "config", "cms.yml"))
CMS_FILES             = cms_yml["cms_files"]
ASSET_RELATIVE_PATH   = "/cms_assets/"

case Rails.env
when 'production', 'staging'
  CMS_FULL_IMAGES_PATH = "/var/www/tunecore/shared/media/images/cms/"
  ASSET_FULL_PATH      = "/var/www/tunecore/shared/media/cms_assets/"
when 'development'
  CMS_FULL_IMAGES_PATH = File.join(Rails.root, "/shared/images/")
  ASSET_FULL_PATH      = File.join(Rails.root, "/public/cms_assets/")
when 'test'
  ASSET_FULL_PATH      = File.join(Rails.root, "/public/cms_assets/")
  CMS_FULL_IMAGES_PATH = '/tmp'
end
