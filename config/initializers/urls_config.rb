unless defined? GLOBAL_URLS_CONFIG
  GLOBAL_URLS_CONFIG = YAML.load_file(File.join(Rails.root, "config", "urls.yml"))[Rails.env.to_s]
end
