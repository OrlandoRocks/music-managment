require 'shrine'

if Rails.env.development?
  require 'shrine/storage/file_system'

  Shrine.storages = {
    cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"),
    store: Shrine::Storage::FileSystem.new("public", prefix: "uploads/store")
  }
elsif Rails.env.test?
  require 'shrine/storage/memory'

  Shrine.storages = {
    cache: Shrine::Storage::Memory.new,
    store: Shrine::Storage::Memory.new
  }
else
  require 'shrine/storage/s3'

  s3_options = {
    access_key_id: AWS_ACCESS_KEY,
    secret_access_key: AWS_SECRET_KEY,
    bucket: SONG_LIBRARY_UPLOADS_BUCKET
  }

  Shrine.storages = {
    cache: Shrine::Storage::S3.new(prefix: "cache_", **s3_options),
    store: Shrine::Storage::S3.new(**s3_options)
  }
end

Shrine.plugin :activerecord
