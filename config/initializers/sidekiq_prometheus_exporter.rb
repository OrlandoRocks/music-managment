require 'sidekiq/prometheus/exporter'
Sidekiq::Prometheus::Exporter.configure do |config|
  config.exporters = %i(cron scheduler)
end
