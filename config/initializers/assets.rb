Rails.application.configure do
  # Enable the asset pipeline
  config.assets.enabled = true

  # Version of your assets, change this if you want to expire all your assets
  config.assets.version = "1.0.1"

  %w[app vendor].each do |dir|
    Dir.glob("#{Rails.root}/#{dir}/assets/**/*").each do |path|
      config.assets.paths << path
    end
  end

  config.assets.precompile += %w[
    admin.css
    admin/eft_batch_transactions.js
    admin/foundation.js
    admin/payment.js
    admin/payout/payout_transfers.js
    admin/payout_providers.css
    admin_foundation.css
    adyen_dropin.css
    adyen_dropin.js
    amfa/amfa_banner.css
    application.css
    application.js
    apps/*.js
    artist_urls_modal.css
    artist_urls_modal.js
    artwork_suggestions.css
    artwork_suggestions.js
    balance-history.css
    cable_utilities.js
    cart.css
    cart.js
    city_autocomplete_by_state.js
    common/mini-carousel.js
    common/mini-carousel.js
    compliance_info_fields.js
    confirm_and_pay.js
    country_and_store_selector.css
    credit_note_invoices/preview.css
    customer_type.css
    dark_mode/*.css
    dashboard.css
    dashboard_reskin.css
    dashboard_reskin.js
    facebook_track_monetizations_modal.css
    footer_reskin.css
    foundation_overrides.scss
    general_reskin.css
    inside_header.scss
    invoices/outbound_invoice.css
    invoices/outbound_invoice_footer.css
    invoices/outbound_invoice_header.css
    invoices/tc_accelerator_modal.scss
    invoices/tc_accelerator_modal.js
    invoice_thanks.css
    language_selector.css
    login.css
    lyric/lyric-advance.js
    lyric/lyric-learn-more-modal.js
    master.css
    modal_popup.js
    my_artist.js
    payoneer_foundation.css
    payout/payoneer-currency-selection.css
    payout/payoneer-modal.css
    payout/payoneer-signup.css
    payout/payout-states.css
    payout_providers.js
    people.css
    people.js
    people_fields.js
    people/company_info.scss
    people/self_billing.scss
    people/adyen_preference.scss
    plans/index.js
    plans/*.css
    refund_list_modal.js
    registration.js
    registration.css
    registration_reskin.css
    registration_reskin.js
    release_creation.css
    royalty_splits/*.js
    royalty_splits/recipients_form.scss
    royalty_splits/split_album.scss
    royalty_splits/split_release_details.scss
    royalty_splits/splits_popup.scss
    salepoint_subscriptions.js
    self_billing_on_checkout.js
    shared/modal.js
    shared/pay_now_button.js
    shared/prompt_modal.css
    shared/ticker.css
    shared/ticker.js
    signup.css
    signup/domain_modal.js
    signup/signup_reskin_country_watcher.js
    singles.js
    singles/tiktok_abandonment
    singles/tiktok_triggered
    singles/tiktok_upsell
    splits_collaborator_popup.js
    songs.css
    songwriter.js
    store_selector.js
    tc_accelerator/dashboard_index.js
    tc_accelerator/opt
    tc-foundation.js
    transaction_list.css
    two_factor_auth.css
    two_factor_auth.js
    unique_payout_tooltip.js
    v2-nav.css
    vat_tooltip.css
    withdraw_paypal.css
  ]
end
