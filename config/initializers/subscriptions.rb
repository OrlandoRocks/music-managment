SUBSCRIPTION_TYPE_CONFIG = {
  "Social" => {
    allowed_payment_methods: ["credit_card", "paypal"]
  },
  "FbTracks" => {
    allowed_payment_methods: ["balance"]
  }
}.freeze

DEFAULT_SUBSCRIPTION_GRACE_PERIOD = 5
