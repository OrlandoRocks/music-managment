config = YAML.load_file(Rails.root.join("config", "external_service_id_artwork.yml"))[Rails.env]

EXTERNAL_SERVICE_ID_ARTWORK_CONFIG = {}

config.each do |k, v|
  v = v.constantize if k == "hash_digest"
  EXTERNAL_SERVICE_ID_ARTWORK_CONFIG[k.to_sym] = v
end

