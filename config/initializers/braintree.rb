BRAINTREE = {
  :key            => ENV['BRAINTREE_KEY'],
  :key_id         => ENV['BRAINTREE_KEY_ID'],
  :password       => ENV['BRAINTREE_PASSWORD'],
  :username       => ENV['BRAINTREE_USERNAME'],
  :query_username => ENV['BRAINTREE_QUERY_USERNAME'],
  :query_password => ENV['BRAINTREE_QUERY_PASSWORD'],
  :query_api_url  => ENV['BRAINTREE_QUERY_API_URL'],
  1               => ENV['BRAINTREE_US_PROCESSOR'],
  2               => ENV['BRAINTREE_CA_PROCESSOR']
}

BRAINTREE_VENDOR = "braintree".freeze

BRAINTREE_BATCH = {
  :username          => ENV['BRAINTREE_BATCH_USERNAME'],
  :password          => ENV['BRAINTREE_BATCH_PASSWORD'],
  :url               => ENV['BRAINTREE_BATCH_URL'],
  :max_upload_time   => ENV['BRAINTREE_BATCH_MAX_UPLOAD_TIME'],
  :max_download_time => ENV['BRAINTREE_BATCH_MAX_DOWNLOAD_TIME'],
  :run_limit         => ENV['BRAINTREE_BATCH_RUN_LIMIT'],
  :local_uploads     => ENV['BRAINTREE_BATCH_LOCAL_UPLOADS'],
  :local_downloads   => ENV['BRAINTREE_BATCH_LOCAL_DOWNLOADS'],
  :local_reports     => ENV['BRAINTREE_BATCH_LOCAL_REPORTS']
}

BATCH_LOG_FILE = "#{Rails.env}.log"

BRAINTREE_UPLOAD = "-k --ftp-ssl --tlsv1 -1 --max-time #{BRAINTREE_BATCH[:max_upload_time]} -w \"[response_code=%{http_code} time_total=%{time_total} upload_size=%{size_upload}]\n\" -u #{BRAINTREE_BATCH[:username]}:#{BRAINTREE_BATCH[:password]} #{BRAINTREE_BATCH[:url]}upload/ -T"
BRAINTREE_DOWNLOAD = "-k --ftp-ssl --tlsv1 -1 --max-time #{BRAINTREE_BATCH[:max_download_time]} -w \"[response_code=%{http_code} time_total=%{time_total} download_size=%{size_download}]\n\" -u #{BRAINTREE_BATCH[:username]}:#{BRAINTREE_BATCH[:password]} #{BRAINTREE_BATCH[:url]}response/"


Braintree::Configuration.environment = Rails.env.production? ? :production : :sandbox
Braintree::Configuration.merchant_id = ENV["BRAINTREE_BLUE_MERCH_ID"]
Braintree::Configuration.public_key  = ENV["BRAINTREE_BLUE_PUBLIC_KEY"]
Braintree::Configuration.private_key = ENV["BRAINTREE_BLUE_PRIVATE_KEY"]

BRAINTREE_MERCHANT_IDS = {
  "USD" => ENV['BRAINTREE_USD_MERCHANT_ID'],
  "CAD" => ENV['BRAINTREE_CAD_MERCHANT_ID'],
  "GBP" => ENV['BRAINTREE_GBP_MERCHANT_ID'],
  "AUD" => ENV['BRAINTREE_AUD_MERCHANT_ID'],
  "EUR" => ENV['BRAINTREE_EUR_MERCHANT_ID']
}
