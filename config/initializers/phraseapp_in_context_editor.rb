PhraseApp::InContextEditor.configure do |config|
  # Enable or disable the In-Context-Editor in general
  # To enable phrase app, comment out the following line
  # of code with the environment you want to enable it on
  config.enabled = false

  # Fetch your project id after creating your first project
  # in Translation Center.
  # You can find the project id in your project settings
  # page (https://phraseapp.com/projects)
  config.project_id = "28f1c050223d94b3036e7710c54c29f8"

  # You can create and manage access tokens in your profile settings
  # in Translation Center or via the Authorizations API
  # (http://docs.phraseapp.com/api/v2/authorizations/).
  config.access_token = ENV["PHRASEAPP_ACCESS_TOKEN"]

  # Configure an array of key names that should not be handled
  # by the In-Context-Editor.
  config.ignored_keys = [
    "number.*",
    "support*",
    "breadcrumb.*",
    "country.name.*",
    "language_code.description.*",
    "song_role.role_type.*"
  ]

  # PhraseApp uses decorators to generate a unique identification key
  # in context of your document. However, this might result in conflicts
  # with other libraries (e.g. client-side template engines) that use a similar syntax.
  # If you encounter this problem, you might want to change this decorator pattern.
  # More information: http://docs.phraseapp.com/guides/in-context-editor/configure/
  # config.prefix = "{{__"
  # config.suffix = "__}}"
end
