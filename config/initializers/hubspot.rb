MOCK_HUBSPOT_CALL                   = !Rails.env.production?

HUBSPOT_REGISTRATION_FORM_NAME      = "registration".freeze
FIRST_DISTRIBUTION_FORM_NAME        = "First Distribution".freeze
VERIFIED_EMAIL_FORM_NAME            = "Verified Email".freeze
PUB_ADMIN_SIGNUP_FORM_NAME          = "PubAdminSignup".freeze
PUB_ADMIN_PAID_FORM_NAME            = "PubAdminPaid".freeze
RESEND_VERIFICATION_FORM_NAME       = "ResendVerification".freeze

TRACK_SMARTS_COMPARISON_FROM_TS     = "000000301075".freeze
TRACK_SMARTS_COMPARISON_FROM_AS     = "000000301095".freeze
TRACK_SMARTS_COMPARISON_FROM_SO     = "000000304240".freeze
TRACK_SMARTS_COMPARISON_FROM_DASH   = "000000304243".freeze
YTM_ACTION_TAKEN                    = "000000166053".freeze

APPLE_PURCHASE_EVENT                = {
                                        "Purchase" => "000001994250".freeze,
                                        "Renewal"  => "000002017286".freeze
                                      }
ANDROID_PURCHASE_EVENT              = {
                                        "Purchase" => "000001994250".freeze,
                                        "Renewal"  => "000002017286".freeze
                                      }

YTM_ROYALTIES_POSTED                = "000000178466".freeze

HUBSPOT_CONTACT_API_URL             = "http://api.hubapi.com".freeze
HUBSPOT_CONTACT_API_KEY             = ENV.fetch('HUBSPOT_CONTACT_API_KEY', nil)

# More events are in Hubspotter::Events
