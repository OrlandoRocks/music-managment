unless Rails.env.production?
  require 'chargebee'
  ChargeBee.configure(site: ENV['CHARGEBEE_SITE'],
                      api_key: ENV['CHARGEBEE_API_KEY'])
end
