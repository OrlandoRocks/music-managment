Rails.application.configure do
  # Check the ERB template's last-modified-time when rendering templates.
  # If stale, recompile and render
  config.action_view.cache_template_loading = false

  # Override default markup for a field with errors to use class of fieldWithErrors like rails2
  config.action_view.field_error_proc = Proc.new do |html_tag, instance|
    "<div class=\"fieldWithErrors\">#{html_tag}</div>".html_safe
  end
end
