# 2009-05-27 -- MJL -- Overriding radio_button so semantic form builder will generate labels correctly.
# 2009-07-01 -- MJL -- Adding the required field flags
# 2009-08-16 -- MJL -- Moving the error display to be wrapped in a div tag with its field
# 2009-08-16 -- MAT -- Removing label name from contextual help
# 2009-08-25 -- MAT -- Removing DOM lvl 0 inline events

# Use semantic form builder
ActionView::Base.default_form_builder = SemanticFormBuilder

class SemanticFormBuilder
  #overriding this method to add the 'method' to the options to be used in the rest of semantic form builder
  def field_settings(method, options = {}, tag_value = nil)
    field_name = "#{@object_name}_#{method.to_s}"
    default_label = tag_value.nil? ? "#{method.to_s.gsub(/\_/, " ")}" : "#{tag_value.to_s.gsub(/\_/, " ")}"
    label = options[:label] ? options.delete(:label) : default_label
    options[:class] ||= ""
    options.update(:method => method.to_sym)
    [field_name, label, options]
  end

  def radio_button(method, tag_value, options = {})
    field_name, label, options = field_settings(method, options)
    field_name += "_" + tag_value.to_s
    wrapping("radio", field_name, label, super, options)
  end

  def email_field(method, options = {})
    field_name, label, options = field_settings(method, options)
    wrapping("email", field_name, label, super, options)
  end

  def wrapping(type, field_name, label, field, options = {})
    help = %Q{<span class="help">#{options[:help]}</span>} if options[:help]
    class_text = %Q{class="#{options.delete(:li_class)}"} unless options[:li_class].blank?
    li_id_text = options[:li_id].blank? ? "#{field_name}_list_item" : options[:li_id]
    label_for_builder = label
    label_for_builder += '<strong class="required">*</strong>'.html_safe if options[:tc_required]

    if !options[:new_form].blank? && !["radio","check-box"].include?(type)
      options.delete(:new_form)
      to_return = []
      to_return << "<fieldset id='#{li_id_text}' #{class_text }>"
      to_return << %Q{<label for="#{field_name}">#{label_for_builder}</label>} unless ["submit"].include?(type)
      to_return << field

      # Add additional error and context tags
      error_tags(label,field_name,options).each { |e| to_return << e }
      contextual_help_tags(field_name,options).each { |t| to_return << t }

      to_return << "</fieldset>"
    elsif options[:left_label] && type == "check-box"
      options.delete(:left_label)
      to_return = []
      to_return << "<li id='#{li_id_text}' #{class_text}>"
      to_return << "<label for='#{field_name}'>#{label}</label>"
      to_return << "<div class=\"context-wrapper\">"
      to_return << field
      # Add additional error and context tags
      error_tags(label,field_name,options).each { |e| to_return << e }
      contextual_help_tags(field_name,options).each { |t| to_return << t }
      if options[:note]
        to_return << "<p class=\"checkbox-note\">#{options[:note]}</p>"
        options.delete(:note)
      end
      to_return << "</div>"
      to_return << "</li>"
    elsif !options[:new_form].blank? && ["radio","check-box"].include?(type)
      options.delete(:new_form)
      to_return = []
      to_return << "<li id='#{li_id_text}' #{class_text }>"
      to_return << "<label for='#{field_name}'>"
      to_return << field
      to_return << " #{label}"
      to_return << "</label>"

      # Add additional error and context tags
      error_tags(label,field_name,options).each { |e| to_return << e }
      contextual_help_tags(field_name,options).each { |t| to_return << t }

      to_return << "</li>"
    else
      to_return = []
      to_return << "<li id='#{li_id_text}' #{class_text }>"
      to_return << %Q{<label for="#{field_name}">#{label_for_builder}</label>} unless ["radio","check-box", "submit"].include?(type)
      to_return << %Q{<div class="context-wrapper">}
      to_return << field

      # Add additional error and context tags
      error_tags(label,field_name,options).each { |e| to_return << e }
      contextual_help_tags(field_name,options).each { |t| to_return << t }

      to_return << %Q{</div>}
      to_return << %Q{<label for="#{field_name}">#{label_for_builder}</label>} if ["radio","check-box"].include?(type)
      to_return << "</li>"
    end


    to_return.join("\n").html_safe
  end

  #
  #  Build contextual help message tags for semantic form builder
  #
  def contextual_help_tags(field_name,options)
    contextual_help = options.delete(:contextual_help)
    force_display = options.delete(:force_display) # mostly in the case of checkboxes/radiobuttons
    to_return = []

    # guard against nil contextual help
    return to_return if options[:contextual_help]

    help_class = force_display ? "forced" : "hidden"
    to_return << %Q{<div class="contextual-help #{help_class}" id="#{field_name}-help">}
    to_return << %Q{<p>#{contextual_help}</p>}
    to_return << %Q{</div>}
    to_return
  end

  #
  #  Build error message tags for semantic form builder
  #
  def error_tags(label,field_name,options)
    errors = @object.errors[options[:method].to_sym] if @object && @object.respond_to?(:errors)
    to_return = []

    # guard against nil errors
    return to_return if errors.blank?

    # Open wrapper
    to_return << %Q{<div class="formError" id="#{field_name}-error">}

    # content
    if errors.to_a.size > 1
      to_return << %Q{<strong>#{options[:error_label] || label}: </strong> }
      errors.each do |error|
        to_return << %Q{<br />- #{error}}
      end
    else
      to_return << %Q{<strong>#{options[:error_label] || label}</strong> }
      errors.each do |error|
        to_return << %Q{#{error}}
      end
    end

    # Close Wrapper
    to_return << %Q{</div>}
    to_return
  end
end
