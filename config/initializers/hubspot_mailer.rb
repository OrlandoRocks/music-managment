HUBSPOT_EMAIL_API_URL   = "http://api.hubapi.com/email/public/v1/singleEmail/send".freeze
HUBSPOT_EMAIL_API_KEY   = HUBSPOT_CONTACT_API_KEY

real_client = Faraday.new(url: HUBSPOT_EMAIL_API_URL) do |f|
  f.headers["Content-Type"] = "application/json"
  f.params[:hapikey] = HUBSPOT_EMAIL_API_KEY
  f.adapter Faraday.default_adapter
end

fake_client = TransactionalEmailFaker.new.fake_hubspot_email_client
$hubspot_email_api_client = MOCK_HUBSPOT_CALL ? fake_client : real_client
