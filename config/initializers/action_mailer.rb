unless ActiveModel::Type::Boolean.new.cast(ENV.fetch('ACTION_MAILER_PERFORM_DELIVERY', true))
  ActionMailer::Base.delivery_method = :test
end