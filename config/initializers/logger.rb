Rails.application.configure do
  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # The basic logger writes to STDOUT, ala https://12factor.net/logs
  config.logger = ActiveSupport::Logger.new(STDOUT).tap do |console_logger|
    console_logger.formatter = config.log_formatter
  end

  # Use the same logger for ActionMailer
  if Rails.env.test?
    config.action_mailer.logger = ActiveSupport::Logger.new("log/#{Rails.env}.mailer.log")
  else
    config.action_mailer.logger = config.logger
  end

  # Implement tagged logging
  config.log_tags = [:subdomain, :uuid]

  # optionally create a secondary logger that logs to a file
  if ENV.fetch("LOG_TO_FILE", "false") == "true"
    file_logger = ActiveSupport::Logger.new("log/#{Rails.env}.log").tap do |f_logger|
      f_logger.formatter = config.log_formatter
    end
    config.logger.extend(ActiveSupport::Logger.broadcast(file_logger))
  end

  # Finally, set the Rails.logger to be the logger configured above
  Rails.logger = config.logger
end
