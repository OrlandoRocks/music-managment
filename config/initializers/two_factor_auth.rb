require "authy"
require "twilio-ruby"
Authy.api_key = ENV["AUTHY_API_KEY"]
Authy.api_uri = ENV["AUTHY_API_URI"]
