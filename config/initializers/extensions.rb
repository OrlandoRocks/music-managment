# an addition to Enumerable to quickly generate array sums.

# NOTE: these should be removed and retested after rails upgrade

class Array
  def rand
   at(Kernel.rand(size))
  end

  def nextval
    # add a nextval method to array class that cycles through array elements & gives the next value
    # after each method call.
    # for example, in rails views it is good to use:
    # <% css_class = ['odd','even'] %>
    # <tr <%= css_class.nextval %>><td></td></tr>
    # <tr <%= css_class.nextval %>><td></td></tr>
    if @curval_ptr
      @curval_ptr += 1
    else
      @curval_ptr = 0
    end
    self[(@curval_ptr)%self.size]
  end

  def currval

    if @curval_ptr.nil?
      @curval_ptr = 0
    end
    #see nextval for description; currval is current value instead of next
    self[(@curval_ptr)%self.size]
  end
end

class SessionCleanup
  def self.truncate_sessions
    CGI::Session::ActiveRecordStore::Session.where('updated_at > ?', 60.minutes.ago).destroy_all
  end
end
