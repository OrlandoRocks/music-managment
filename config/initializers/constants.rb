# frozen_string_literal: true

ANDROID = "android"
IOS = "ios"
VAT_FORMAT = YAML.load_file('config/vat_number_formats.yml')
APPLE_SANDBOX_VERIFICATION_CODE = 21007
ANDROID_PURCHASE_JSON_CONFIG = 'config/tunecore-social-2-971bdac22313.json'
RF_PURCHASE = 'RenderForest Video Purchase'.freeze
TCS_PURCHASE = 'TuneCore Social Video Purchase'.freeze
TUNECORE_US_COUNTRIES = ["American Samoa", "AS", "Guam", "GU",
                         "Puerto Rico", "PR", "United States",
                         "US", "Virgin Islands, United States",
                         "VI", "Virgin Islands, U.S."].freeze
BUSINESS = "business"
CONFIRM_AND_PAY = "confirm_and_pay".freeze
# Invoice pdf header
BI_WEBSITE = "www.TuneCore.com"
TC_WEBSITE = "www.tunecore.com"
TC_SUPPORT_LINK = 'https://support.tunecore.com/'
TC_CUSTOMER_CARE_LINK = TC_SUPPORT_LINK  + "hc/en-us/articles/115006501747-Contact-Us"
EUR = 'EUR'

LEGACY_NEW_COUNTRY_CODE_MAP = {
  "UK" => "GB",
  "KV" => "XK"
}

CREDIT_CARD = "Credit Card"
