USA_FALLBACK_LOCALE = "en".freeze
USA_LOCALE = "en-us".freeze
USA_PT_LOCALE = "pt-us".freeze
USA_ES_LOCALE = "es-us".freeze
USA_RU_LOCALE = "ru-us".freeze
USA_ID_LOCALE = "id-us".freeze
USA_RO_LOCALE = "ro-us".freeze
USA_TH_LOCALE = "th-us".freeze
USA_HU_LOCALE = "hu-us".freeze
USA_NL_LOCALE = "nl-us".freeze
USA_CS_LOCALE = "cs-us".freeze
USA_PL_LOCALE = "pl-us".freeze
USA_TR_LOCALE = "tr-us".freeze
USA_SC_LOCALE = "sc-us".freeze
USA_TC_LOCALE = "tc-us".freeze
CANADA_LOCALE = "en-ca".freeze
CANADA_FR_LOCALE = "fr-ca".freeze
GREAT_BRITAIN_LOCALE = "en-gb".freeze
AUSTRALIA_LOCALE = "en-au".freeze
GERMANY_LOCALE = "de-de".freeze
GERMANY_FALLBACK_LOCALE = "de".freeze
FRANCE_LOCALE = "fr".freeze
FRANCE_FALLBACK_LOCALE = "fr".freeze
ITALY_LOCALE = "it-it".freeze
ITALY_FALLBACK_LOCALE = "it".freeze
INDIA_LOCALE = "en-in".freeze

COUNTRY_LOCALE_MAP = {
  "US" => USA_LOCALE,
  "PT-US" => USA_PT_LOCALE,
  "ES-US" => USA_ES_LOCALE,
  "RU-US" => USA_RU_LOCALE,
  "ID-US" => USA_ID_LOCALE,
  "RO-US" => USA_RO_LOCALE,
  "TH-US" => USA_TH_LOCALE,
  "HU-US" => USA_HU_LOCALE,
  "NL-US" => USA_NL_LOCALE,
  "CS-US" => USA_CS_LOCALE,
  "PL-US" => USA_PL_LOCALE,
  "TR-US" => USA_TR_LOCALE,
  "SC-US" => USA_SC_LOCALE,
  "TC-US" => USA_TC_LOCALE,
  "CA" => CANADA_LOCALE,
  "FR-CA" => CANADA_FR_LOCALE,
  "UK" => GREAT_BRITAIN_LOCALE,
  "AU" => AUSTRALIA_LOCALE,
  "DE" => GERMANY_LOCALE,
  "DE-DE" => GERMANY_FALLBACK_LOCALE,
  "FR" => FRANCE_LOCALE,
  "IT" => ITALY_LOCALE,
  "IT-IT" => ITALY_FALLBACK_LOCALE,
  "IN" => INDIA_LOCALE
}

TLD_DEFAULT_LOCALE_MAP = {
  "ca"     => CANADA_LOCALE,
  "co.uk"  => GREAT_BRITAIN_LOCALE,
  "com"    => USA_LOCALE,
  "com.au" => AUSTRALIA_LOCALE,
  "de"     => GERMANY_LOCALE,
  "fr"     => FRANCE_LOCALE,
  "in"     => INDIA_LOCALE,
  "it"     => ITALY_LOCALE,
}.freeze

BCP_47_LOCALE_MAP = {
  USA_LOCALE => "en-US",
  USA_FALLBACK_LOCALE => "en-US",
  USA_PT_LOCALE => "pt-US",
  USA_ES_LOCALE => "es-US",
  USA_RU_LOCALE => "ru-US",
  USA_ID_LOCALE => "id-US",
  USA_RO_LOCALE => "ro-US",
  USA_TH_LOCALE => "th-US",
  USA_HU_LOCALE => "hu-US",
  USA_NL_LOCALE => "nl-US",
  USA_CS_LOCALE => "cs-US",
  USA_PL_LOCALE => "pl-US",
  USA_TR_LOCALE => "tr-US",
  USA_SC_LOCALE => "sc-US",
  USA_TC_LOCALE => "tc-US",
  CANADA_LOCALE => "en-CA",
  CANADA_FR_LOCALE => "fr-CA",
  GREAT_BRITAIN_LOCALE => "en-GB",
  AUSTRALIA_LOCALE => "en-AU",
  GERMANY_LOCALE => "de-DE",
  GERMANY_FALLBACK_LOCALE => "de-DE",
  FRANCE_LOCALE => "fr-FR",
  FRANCE_FALLBACK_LOCALE => "fr-FR",
  ITALY_LOCALE => "it-IT",
  ITALY_FALLBACK_LOCALE => "it-IT",
  INDIA_LOCALE => "en-IN",
}.freeze

def bcp47_locale(locale = I18n.locale)
  BCP_47_LOCALE_MAP.fetch(locale.to_s.downcase, BCP_47_LOCALE_MAP[USA_LOCALE])
end

COUNTRY_LOCALE_OG_MAP = {
  USA_LOCALE => "en_US",
  CANADA_LOCALE => "en_CA",
  GREAT_BRITAIN_LOCALE => "en_GB",
  AUSTRALIA_LOCALE => "en_AU",
  GERMANY_LOCALE => "de_DE",
  FRANCE_LOCALE => "fr_FR",
  ITALY_LOCALE => "it_IT",
  INDIA_LOCALE => "en_IN"
}.freeze

LOCALE_TO_COUNTRY_MAP = {
  USA_FALLBACK_LOCALE => "us",
  USA_LOCALE => "us",
  USA_PT_LOCALE => "us",
  USA_ES_LOCALE => "us",
  USA_RU_LOCALE => "us",
  USA_ID_LOCALE => "us",
  USA_RO_LOCALE => "us",
  USA_TH_LOCALE => "us",
  USA_HU_LOCALE => "us",
  USA_NL_LOCALE => "us",
  USA_CS_LOCALE => "us",
  USA_PL_LOCALE => "us",
  USA_TR_LOCALE => "us",
  USA_SC_LOCALE => "us",
  USA_TC_LOCALE => "us",
  CANADA_LOCALE => "ca",
  CANADA_FR_LOCALE => "ca",
  GREAT_BRITAIN_LOCALE => "uk",
  AUSTRALIA_LOCALE => "au",
  GERMANY_LOCALE => "de",
  GERMANY_FALLBACK_LOCALE => "de",
  FRANCE_LOCALE => "fr",
  FRANCE_FALLBACK_LOCALE => "fr",
  ITALY_LOCALE => "it",
  ITALY_FALLBACK_LOCALE => "it",
  INDIA_LOCALE => "in"
}

I18n.available_locales = COUNTRY_LOCALE_MAP.values
