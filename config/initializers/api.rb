API_CONFIG = {
  "API_KEY" => ENV['DEFAULT_API_KEY'],
  "royalty_solutions" => {
    API_KEY: ENV['ROYALTY_SOLUTIONS_API_KEY'],
  },
  "songspace" => {
    API_KEY: ENV['SONGSPACE_API_KEY'],
  },
  "songs" => {
    API_KEY: ENV['SONGS_API_KEY'],
  },
  "albums" => {
    API_KEY: ENV['ALBUMS_API_KEY'],
  },
  "artwork" => {
    API_KEY: ENV['ARTWORK_API_KEY'],
  },
  "sip_vat" => {
    API_KEY: ENV['SIP_VAT_API_KEY'],
  }
}.with_indifferent_access
