case Rails.env
when 'production'
  CheckPayment::Export::EMAILS.set 'checks@integrationinc.com', 'checkprocessing@tunecore.com'
when 'staging'
  CheckPayment::Export::EMAILS.set 'qa@tunecore.com'
when 'development'
  CheckPayment::Export::EMAILS.set 'qa@tunecore.com'
end
