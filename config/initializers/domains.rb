require_relative "./urls_config"
WEB_ENV        = "web".freeze
DEPLOY_ENV     = ENV["DEPLOY_ENV"].freeze || WEB_ENV

COUNTRY_URLS   = LocaleUrls.build_locale_urls(DEPLOY_ENV).freeze
WEB_ONLY_URLS  = LocaleUrls.build_locale_urls(WEB_ENV).freeze
CALLBACK_API_URLS  = Rails.env.production? ? WEB_ONLY_URLS : COUNTRY_URLS
