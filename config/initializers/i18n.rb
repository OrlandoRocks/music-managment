Rails.application.configure do
  config.enforce_available_locales = false

  # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
  config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{yml}')]
  config.i18n.default_locale = :en
  config.i18n.fallbacks = [I18n.default_locale]
end

Rails.application.configure do
  config.enforce_available_locales = false

  # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
  config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{yml}')]
  config.i18n.default_locale = :en
  config.i18n.fallbacks = true
end

I18n.config.missing_interpolation_argument_handler = I18nMissingInterpolationArgumentService.new
