if Tunecore::Tracing.enabled?
  require 'opentracing'

  tracer = Tunecore::Tracing::Factory.create_tracer(
    Tunecore::Tracing.service_name,
    Tunecore::Tracing.connection_string
  )
  OpenTracing.global_tracer = tracer if tracer

  require 'rails/tracer'
  Rails::Tracer.instrument(rack: true, full_trace: true)

  require 'rack/tracer'
  Rails.configuration.middleware.use(Rack::Tracer)
  Rails.configuration.middleware.insert_after(Rack::Tracer, Rails::Rack::Tracer)
end
