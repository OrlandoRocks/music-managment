# Generate Apple Music token with a 6 month expiration, for AppleMusic::ApiClient

# payload
iat = Time.now.to_i
exp = (Time.now.to_i + 180 * 86_400).to_i # 180 days in seconds
iss = ENV["APPLE_MUSIC_TEAM_ID"]
payload = { exp: exp, iat: iat, iss: iss }

# private key
# Convert the p8 file provided by Apple to pem format used by ruby-jwt.
# p8 file format must be the default - 64 chars per line - to be parsed by OpenSSL.
key_file_name = ENV["APPLE_MUSIC_PRIVATE_KEY_FILE_NAME"]
p8_file = "config/certs/#{key_file_name}"

# For non-docker dev environments missing the p8 file
if !File.file?(p8_file)
  APPLE_MUSIC_TOKEN = 'fake token'.freeze
  return
end

pem = `openssl pkcs8 -nocrypt -in #{p8_file}`
private_key = OpenSSL::PKey::EC.new(pem)

# header
kid = key_file_name.split('.')[0].split('_').last
header = { kid: kid }

APPLE_MUSIC_TOKEN = JWT.encode(payload, private_key, 'ES256', header).freeze
