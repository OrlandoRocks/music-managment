# TODO: We will probably want to remove this when we get to a newer Rails
# version
# https://tunecore.atlassian.net/browse/UP-258
module Sass
  module CacheStores
    class Base
      def store(*)
        true
      end
    end
  end
end
