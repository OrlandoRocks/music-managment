Rails.application.configure do
  # Enable escaping HTML in JSON.
  config.active_support.escape_html_entities_in_json = true
end

class ActionController::Parameters
  def with_indifferent_access
    ::ActiveSupport::HashWithIndifferentAccess.new(self.permit!.to_h)
  end
end
