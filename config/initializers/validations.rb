# extending and creating ActiveRecord validations
# --------------------------
# 
# (C)2007 Jacqui Maher <jacqui@brighter.net>
#
# http://brighter.net/
#
module ActiveRecord
  module Validations
    module ClassMethods
      
      ## validates emails against the RFC
      def validates_as_email(*attr_names)
        address_pattern = begin
          qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]'
          dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]'
          atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-' +
            '\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+'
          quoted_pair = '\\x5c[\\x00-\\x7f]'
          domain_literal = "\\x5b(#{dtext}|#{quoted_pair})*\\x5d"
          quoted_string = "\\x22(#{qtext}|#{quoted_pair})*\\x22"
          domain_ref = atom
          sub_domain = "(#{domain_ref}|#{domain_literal})"
          word = "(#{atom}|#{quoted_string})"
          domain = "#{sub_domain}(\\x2e#{sub_domain})*"
          local_part = "#{word}(\\x2e#{word})*"
          addr_spec = "#{local_part}\\x40#{domain}"
          pattern = Regexp.new("^#{addr_spec}$", nil, "n")
        end
        
        configuration = { :message => "is an invalid email" }
        configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)

        validates_each(attr_names, configuration) do |record, attr_name, value|
          value_before_type_cast = record.send("#{attr_name}_before_type_cast")
        
          # Ignore blank emails, these can be validated with validates_presence_of
          if value.empty?
            next
          end

          unless value =~ address_pattern
            record.errors.add(attr_name, configuration[:message])
          end
        end
      end
      
      ## checks if the given table name is found in the current database schema
      def validates_table(*attr_names)
        raise ActiveRecordError, "No database connection found" unless ActiveRecord::Base.connection
        raise ActiveRecordError, "Can't find current database" unless ActiveRecord::Base.connection.current_database
        
        configuration = { :message => "is not a valid table name for the database #{ActiveRecord::Base.connection.current_database}", :on => :save }
        configuration.update(attr_names.pop) if attr_names.last.is_a?(Hash)

        validates_each(attr_names, configuration) do |record, attr_name, value|
          record.errors.add(attr_name, configuration[:message]) unless ActiveRecord::Base.connection.tables.include?(value)
        end
      end
    end
  end
end
