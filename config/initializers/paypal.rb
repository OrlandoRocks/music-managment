PAYPAL_CONFIG = {
  "EC_URL"              => ENV["PAYPAL_EC_URL"],
  "DG_URL"              => ENV["PAYPAL_DG_URL"],
  "CHECKOUT_RETURN_URL" => ENV["PAYPAL_CHECKOUT_RETURN_URL"],
  "CHECKOUT_CANCEL_URL" => ENV["PAYPAL_CHECKOUT_CANCEL_URL"],
  "STORED_RETURN_URL"   => ENV["PAYPAL_STORED_RETURN_URL"],
  "STORED_CANCEL_URL"   => ENV["PAYPAL_STORED_CANCEL_URL"],
  "SERVER"              => ENV["PAYPAL_SERVER"],
  "SERVICE"             => ENV["PAYPAL_SERVICE"],
  "USE_PROXY"           => false,
  "PROXY_ADDRESS"       => nil,
  "PROXY_PORT"          => nil,
  "PROXY_USER"          => nil,
  "PROXY_PASSWORD"      => nil,
  "VERSION"             => "65.1",
  "SOURCE"              => "PayPalRubySDKV1.2.0"
}

require "paypal_sdk/caller"
require "paypal_sdk/paypal_api"
require "paypal_sdk/paypal_utils"
require "paypal_sdk/profile"


PAYPAL_CREDS = {
  "USD"     => {
    "USER" => ENV["PAYPAL_USD_USER"],
    "PWD" => ENV["PAYPAL_USD_PWD"],
    "SIGNATURE" => ENV["PAYPAL_USD_SIGNATURE"]
  },
  "CAD"     => {
    "USER" => ENV["PAYPAL_CAD_USER"],
    "PWD" => ENV["PAYPAL_CAD_PWD"],
    "SIGNATURE" => ENV["PAYPAL_CAD_SIGNATURE"]
  },
  "DEFAULT" => {
    "USER" => ENV["PAYPAL_DEFAULT_USER"],
    "PWD" => ENV["PAYPAL_DEFAULT_PWD"],
    "SIGNATURE" => ENV["PAYPAL_DEFAULT_SIGNATURE"]
  }
}

PAYPAL_BUSINESS_CREDS = {
  "USD" => {
    "username"  => ENV["PAYPAL_USD_WITHDRAWAL_USER"],
    "password"  => ENV["PAYPAL_USD_WITHDRAWAL_PWD"],
    "cert_file" => ENV["PAYPAL_USD_WITHDRAWAL_CERT_FILE"],
    "key_file"  => ENV["PAYPAL_USD_WITHDRAWAL_KEY_FILE"]
  },
  "CAD" => {
    "username"  => ENV["PAYPAL_CAD_WITHDRAWAL_USER"],
    "password"  => ENV["PAYPAL_CAD_WITHDRAWAL_PWD"],
    "cert_file" => ENV["PAYPAL_CAD_WITHDRAWAL_CERT_FILE"],
    "key_file"  => ENV["PAYPAL_CAD_WITHDRAWAL_KEY_FILE"]
  },
  "DEFAULT" => {
    "username"  => ENV["PAYPAL_DEFAULT_WITHDRAWAL_USER"],
    "password"  => ENV["PAYPAL_DEFAULT_WITHDRAWAL_PWD"],
    "cert_file" => ENV["PAYPAL_DEFAULT_WITHDRAWAL_CERT_FILE"],
    "key_file"  => ENV["PAYPAL_DEFAULT_WITHDRAWAL_KEY_FILE"]
  }
}

PAYPAL_BUSINESS = {
  "CAD" => PayPalBusiness::API.new(ActiveSupport::HashWithIndifferentAccess.new(PAYPAL_BUSINESS_CREDS["CAD"]), Rails.env.production?),
  "USD" => PayPalBusiness::API.new(ActiveSupport::HashWithIndifferentAccess.new(PAYPAL_BUSINESS_CREDS["USD"]), Rails.env.production?),
  "DEFAULT" => PayPalBusiness::API.new(ActiveSupport::HashWithIndifferentAccess.new(PAYPAL_BUSINESS_CREDS["DEFAULT"]), Rails.env.production?)
}

case Rails.env
when "production"
  PAYPAL_URL = "https://www.paypal.com/cgi-bin/webscr"
  PAYPAL_ADDR = "accounts@tunecore.com"
when "development", "test", "staging","cucumber"
  PAYPAL_URL = "https://www.sandbox.paypal.com/cgi-bin/webscr"
  PAYPAL_ADDR = "dan_1303496619_biz@tunecore.com"
end
