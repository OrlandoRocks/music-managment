Rails.application.configure do
  # Use SQL instead of Active Record's schema dumper when creating the database.
  # This is necessary if your schema can't be completely dumped by the schema dumper,
  # like if you have constraints or database-specific column types
  config.active_record.schema_format = :sql

  # Set default timezone to local
  config.active_record.default_timezone = :local
  config.active_record.time_zone_aware_attributes = false

  # JSON serializes with model name in root of json
  # Eg: { album: { album_attrs } } vs. { album_attrs }
  config.active_record.include_root_in_json = true

  # TODO: Work is still required to get valid factories with this set to `true`
  # https://tunecore.atlassian.net/browse/UP-223
  config.active_record.belongs_to_required_by_default = false
end
