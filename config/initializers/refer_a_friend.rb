$refer_a_friend_api_config = {
  "PREFIX"       => ENV["REFER_A_FRIEND_PREFIX"],
  "HOST"         => ENV["REFER_A_FRIEND_HOST"],
  "API_USERNAME" => ENV["REFER_A_FRIEND_API_USERNAME"],
  "API_PASSWORD" => ENV["REFER_A_FRIEND_API_PASSWORD"],
  "US"           => { "header_link" => "ceT-gaE", "landing_page" => "ceT-jm3" },
  "CA"           => { "header_link" => "c84-j91", "landing_page" => "c84-j92" },
  "UK"           => { "header_link" => "c85-j95", "landing_page" => "c85-j96" },
  "AU"           => { "header_link" => "c9b-kat", "landing_page" => "c9b-kau" },
  "DE"           => { "header_link" => "c88-kah", "landing_page" => "c88-kai" },
  "FR"           => { "header_link" => "c87-kad", "landing_page" => "c87-kae" },
  "IT"           => { "header_link" => "c89-kal", "landing_page" => "c89-kam" }
}
$refer_a_friend_api_client      = ReferAFriend::ApiClient.new($refer_a_friend_api_config)
