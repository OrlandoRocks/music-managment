namespace        = "trends_#{Rails.env}".to_sym
$trends_redis    = Redis::Namespace.new(namespace, redis: Redis.new(url: ENV["TRENDS_REDIS_URL"]))
