case Rails.env
when 'production'
    ALBUMS_SDB_DOMAIN = 'tcc_albums_production'
when 'staging'
    ALBUMS_SDB_DOMAIN = 'tcc_albums_staging'
when 'development', 'test'
    ALBUMS_SDB_DOMAIN = 'tcc_albums_development'
end
