Rails.application.configure do
  # Enable page/fragment caching by setting a file-based store
  # (remember to create the caching directory and make it readable to the application)
  config.action_controller.cache_store = :file_store, %W[#{Rails.root}/public/media/complete/frag_cache]

  # With this option turned on, forms in your application will each have
  # their own CSRF token that is specific to the action
  # and method for that form.
  config.action_controller.per_form_csrf_tokens = true
end
