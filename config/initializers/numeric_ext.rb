module NumericAddOns
  def percent_of(val)
    self.to_f / val.to_f * 100
  end

  #example: 20.percent_of_s(100) ==> "20.00%"
  def percent_of_s(val, precision=2,signeage='%%')
    raise "Invalid precision" unless precision.is_a?(Integer)
    sprintf("%.#{precision}f#{signeage}",self.percent_of(val))
  end

  def as_dollars(precision=2,signeage='$',delimiter=',')
    raise "Invalid precision" unless precision.is_a?(Integer)
    amount = sprintf("%.#{precision}f",self)
    parts = amount.to_s.split('.')
    parts[0].gsub!(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1#{delimiter}")   #taken from rails number_with_delimiter
    signeage + parts.join('.')
  end

end

class Integer
  include NumericAddOns
end

class Float
  include NumericAddOns
end
