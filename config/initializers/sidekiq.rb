require 'sidekiq/web'
require 'sidekiq-ent/web'

Sidekiq::Web.set :session_secret, Rails.configuration.secret_key_base
Sidekiq::Web.disable(:sessions)

Sidekiq::Enterprise.unique! unless Rails.env.test?

Sidekiq.configure_server do |config|
  config.redis = { url: ENV["SIDEKIQ_REDIS_URL"], network_timeout: 120, size: 32 }

  if ENV["ENABLE_SIDEKIQ_SLACK_NOTIFICATIONS"]
    config.error_handlers << proc { |exception, ctx_hash| Slack::Sidekiq.notify(exception, ctx_hash) }
  end
  if ENV['PROFILE']
    require 'sidekiq_profiler'
    require "objspace"
    ObjectSpace.trace_object_allocations_start
    Sidekiq.logger.info "allocations tracing enabled"
    config.logger.level = Logger::DEBUG

    config.server_middleware do |chain|
      chain.add Sidekiq::Middleware::Server::Profiler
    end
  end

  # Please Keep in alphabetical order
  # Please don't put logic in the block below, i.e. if ENV["something"]...  
  config.periodic do |mgr|
    mgr.register("0 0 1 * *", "CurrencyLayer::GetForeignExchangeRatesWorker")
    mgr.register("0 1 * * *", "CurrencyLayer::SoutheastAsianForeignExchangeRateFetchWorker")
    mgr.register('0 1 1 * *', "IndlocalNotifications::InvoiceStatementsWorker", queue: :default)
    mgr.register('0 2 * * *', "InvoiceExport::InboundBatchWorker", queue: :default)
    mgr.register('0 2 * * *', "InvoiceExport::OutboundBatchWorker", queue: :default)
    mgr.register("0 1 * * *", "Payoneer::ApiTokenRefreshWorker", queue: :critical)
    mgr.register('0 1 * * *', "Payu::WorkflowWorker", queue: :default)
    mgr.register('0 2 * * *', "RefundExport::InboundBatchWorker", queue: :default)
    mgr.register('0 2 * * *', "RefundExport::OutboundBatchWorker", queue: :default)
    mgr.register('0 8 */2 * *', "RoyaltySolutionsStatusWorker", queue: :default)
    mgr.register("1 12 * * 4", "TaxWithholdings::Reporting::IRSPayablesReportWorker", queue: :critical)
    mgr.register("1 12 31 12 *", "TaxWithholdings::Reporting::IRSPayablesReportWorker", queue: :critical)
    mgr.register("0 1 * * *", "VatFailureAnalyzeWorker", queue: :default)
    
    # Send Report Everyday (IF REQUIRED) at 09:00 PM
    mgr.register("0 21 * * *", "WithdrawalMismatchReportWorker", queue: :critical)
  end

  config.super_fetch!
end

Sidekiq.configure_client do |config|
  config.redis = { url: ENV["SIDEKIQ_REDIS_URL"], network_timeout: 120, size: 32 }
end

Sidekiq.default_worker_options = {
  "queue"     => "default",
  "retry"     => true,
  "backtrace" => 15
}

$rights_app_limiter = Sidekiq::Limiter.concurrent(
  "rights-app-rate-limit",
  5,
  wait_timeout: 5,
  lock_timeout: 60
)
