if ENV["AIRBRAKE_PROJECT_ID"].present? && ENV["AIRBRAKE_PROJECT_KEY"].present?
  instance_name = ENV["DEPLOY_ENV"]
  Airbrake.configure do |config|
    config.environment            = (instance_name.nil? || instance_name.blank?) ? Rails.env : instance_name
    config.ignore_environments    = %w(development test)
    config.project_id             = ENV["AIRBRAKE_PROJECT_ID"]
    config.project_key            = ENV["AIRBRAKE_PROJECT_KEY"]
    config.blacklist_keys         = [/email/i, /credit_card/i, /password/i]
    config.performance_stats      = true
  end
end
