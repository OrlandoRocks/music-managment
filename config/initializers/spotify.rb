SPOTIFY_API = {
  token_request: ENV["SPOTIFY_TOKEN_REQUEST"],
  base:          ENV["SPOTIFY_BASE"],
  client_id:     ENV["SPOTIFY_CLIENT_ID"],
  client_secret: ENV["SPOTIFY_CLIENT_SECRET"]
}
