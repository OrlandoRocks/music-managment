require 'will_paginate/array'

# RAILS_5_UPGRADE: Remove this class

# module WillPaginate
#   class Collection < Array
#     def first_item
#       offset + 1
#     end
#
#     def last_item
#       [total_entries, offset + per_page].min
#     end
#   end
# end
