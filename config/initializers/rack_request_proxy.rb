module OAuth::RequestProxy
  class RackRequest < OAuth::RequestProxy::Base
    prepend ParamsWithStandardization
  end
end
