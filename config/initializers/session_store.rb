Rails.application.config.session_store :redis_store, {
  servers: ["#{ENV["REDIS_URL"]}/0/session"],
  key: "tunecore_id",
  expires_in: 24.hours
}
# Rails.application.config.session_store :cookie_store, key: '_railsdiff_session'
