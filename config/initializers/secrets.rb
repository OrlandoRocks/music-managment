SECRETS = {
  "hash_prefix"          => ENV["HASH_PREFIX"],
  "salt_prefix"          => ENV["SALT_PREFIX"],
  "upgraded_hash_prefix" => ENV["UPGRADED_HASH_PREFIX"],
  "tax_key"              => ENV["TAX_KEY"],
  "old_tax_key"          => ENV["OLD_TAX_KEY"],
  "session_cookie_key"   => ENV["SESSION_COOKIE_KEY"],
  "session_salt"         => ENV["SESSION_SALT"],
  "api_v1_secret_key"    => ENV["API_V1_SECRET_KEY"],
  "songspace" => {
    "api_key" =>  ENV["SONGSPACE_API_KEY"],
  }
}
