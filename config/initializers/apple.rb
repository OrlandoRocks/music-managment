$apple_client = Apple::ApiClient.new({
  host:     ENV["APPLE_API_CLIENT_HOST"],
  password: ENV["APPLE_API_CLIENT_PASSWORD"]
})

$sandbox_apple_client = Apple::ApiClient.new({
  host:     ENV["SANDBOX_APPLE_API_CLIENT_HOST"],
  password: ENV["SANDBOX_APPLE_API_CLIENT_PASSWORD"]
})