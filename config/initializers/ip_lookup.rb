IP_LOOKUP_CONFIG_PATH = File.join(Rails.root, "config", "ip_lookup.yml")
IP_LOOKUP_CONFIG = File.exist?(IP_LOOKUP_CONFIG_PATH) ? YAML.load_file(IP_LOOKUP_CONFIG_PATH)[Rails.env.to_s] : nil
