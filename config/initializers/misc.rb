ISRC_PREFIX = 'TC'  # prefix for ISRC codes (song level)
ISRC_VIDEO_PREFIX = 'USTCV' #prefix for ISRC codes at the video level


# VARIOUS VARIABLES ARE COMMENTED OUT because they have been added to
# development, production, and test environment .rb files.
# This is temporary until we are sure they work, then these commented
# variable lines will be removed entirely. If you uncomment any of them,
# be sure to keep in mind they will supercede any of the individual
# environments files. - Allen


VALIDATE_W3C = ENV['VALIDATE_W3C'] == 'true' ? true : false

SHELL_ERROR_FILE = 'std_error_'

# set this to false to fall back on the simple upload and encoding on one machine
USE_SEPARATE_UPLOAD_SERVER = true
USE_SEPARATE_ENCODE_SERVER = true


# TARGETED OFFER DEFAULTS for new signups

TARGETED_OFFER_CANADA_JOIN_TOKEN = 'oct11newca'
TARGETED_OFFER_US_JOIN_TOKEN = 'oct11newus'

# MOBILE BROWSER user agent strings (adding more later)

MOBILE_BROWSERS = ["android", "ipod", "opera mobi", "opera mini", "blackberry", "palm","hiptop", "iphone"] # make this more detailed later
