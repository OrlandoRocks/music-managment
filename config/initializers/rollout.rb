$redis = Redis.new(url: ENV["REDIS_URL"], timeout: 20)
$rollout = Rollout.new($redis)
$country_rollouts = {}

CountryWebsite::ISO_CODE_MAP.each do |iso_code, _country_website_id|
  country_namespace = Redis::Namespace.new("#{iso_code}_country_namespace", redis: $redis)
  $country_rollouts[iso_code] = Rollout.new(country_namespace)
end
