UPLOAD_PATH                = '/images/artwork/upload/'
FONTS_PATH                 = '/public/images/artwork/TTFs/'
WEB_IMAGES_PATH            = '/images/artwork/templates/web/'
FULL_RES_TIFFS_PATH        = '/public/images/artwork/templates/tiffs/'

# audit processing
if Rails.env.production?
  ALBUM_ARCHIVE_ROOT        = '/home/tunecore'
else
  ALBUM_ARCHIVE_ROOT        = File.join(Rails.root, '/tmp')
end

# SALEPOINT_AUDITS_ROOT      = ALBUM_ARCHIVE_ROOT + '/salepoint_audits'
# SALEPOINT_AUDIT_FILES_ROOT = ALBUM_ARCHIVE_ROOT + '/salepoint_audits'

BATCH_BALANCE_ADJUSTMENTS_UPLOAD_PATH = File.join(Rails.root, '/tmp')
