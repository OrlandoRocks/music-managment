#!/bin/env ruby
# encoding: utf-8

MONEY_CONFIG = {
    'USD' => {:unit => '$', :description => 'United States Dollars', :suppress_iso => true},
    'CAD' => {:unit => '$', :description => 'Canadian Dollars', :suppress_iso => true},
    'AUD' => {:unit => '$', :description => 'Australian Dollars', :suppress_iso => true},
    'GBP' => {:unit => "£", :description => 'British Pounds', :suppress_iso => true},
    'EUR' => {:unit => '€', :description => 'Euros', :suppress_iso => true},
    'JPY' => {:unit => '¥', :description => 'Japanese Yen', :precision => 0},
    'INR' => {:unit => "₹", :description => 'Indian Rupees', :suppress_iso => true}
}

USED_CURRENCY = "£$"

# digits >= 5 should be rounded up, others rounded down. For more types, see
# https://ruby-doc.org/stdlib-2.5.1/libdoc/bigdecimal/rdoc/BigDecimal.html#ROUND_MODE
Money.rounding_mode = BigDecimal::ROUND_HALF_UP

# Explicitly set formatting per current user's i18n locale. For formatting help,
# see https://github.com/RubyMoney/money#localization
Money.locale_backend = :i18n
