DELIVERY_CONFIG      = YAML.load_file(Rails.root.join("config", "delivery.yml"))[Rails.env]
PETRI_ARCHIVE_BUCKET = DELIVERY_CONFIG["archive_bucket"]
PETRI_MESSAGE_BUCKET = DELIVERY_CONFIG["message_bucket"]
PETRI_QUEUE          = DELIVERY_CONFIG["queue"]
SSH_KEYFILE_DIR      = DELIVERY_CONFIG["ssh_keyfile_dir"]
DDEX_SENDER_ID       = "PADPIDA2011101104E".freeze
DDEX_SENDER_NAME     = "TuneCore".freeze
