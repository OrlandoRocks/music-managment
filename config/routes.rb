require "sidekiq/web"
require "admin_constraint"
require 'sidekiq/prometheus/exporter'

Rails.application.routes.draw do
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  post "/graphql", to: "graphql#execute"
  root to: "index#index", as: :home

  namespace :oauth do
    post :access_token
    get  :access_token
    post :request_token
    get  :request_token
    post :token
    get  :token
    post :authorize
    get  :authorize
    post :revoke
    get  :revoke
  end

  get "/login",                                                                 to: "sessions#new",            as: :login
  get "/logout",                                                                to: "sessions#destroy",        as: :logout

  get "/signup",                                                                to: "people#new",              as: :signup
  post "/signup",                                                               to: "people#create"

  get "/registrieren",                                                          to: "people#new"
  get "/applemusicartist",                                                      to: redirect("/signup?ref=applemusicartist")
  get '/offer',                                                                 to: redirect("/signup?jt=offerca"), constraints: { subdomain: "ca" }
  get '/offer',                                                                 to: redirect("/signup?jt=offer")
  get '/lp/',                                                                   to: redirect('/login')
  get '/lp/:name',                                                              to: redirect('/login')
  get '/announcements/store_manager',                                           to: "interstitial#store_manager",              as: :store_manager_announcement
  get '/announcements/juke_store',                                              to: "interstitial#store_manager",              as: :juke_store_announcement
  get '/announcements/jb_hifi_store',                                           to: "interstitial#jb_hifi_store",              as: :jb_hifi_announcement
  get '/announcements/slacker_store',                                           to: "interstitial#slacker_store",              as: :slacker_announcement
  get '/announcements/bloom_store',                                             to: "interstitial#bloom_store",                as: :bloom_announcement
  get '/announcements/beats_store',                                             to: "interstitial#beats_store",                as: :beats_announcement
  get '/announcements/guvera_store',                                            to: "interstitial#guvera_store",               as: :guvera_announcement
  get '/announcements/akazoo_store',                                            to: "interstitial#akazoo_store",               as: :akazoo_announcement
  get '/announcements/march_madness',                                           to: "interstitial#march_madness",              as: :march_madness_announcement
  get '/announcements/anghami_store',                                           to: "interstitial#anghami_store",              as: :anghami_announcement
  get '/announcements/kkbox_store',                                             to: "interstitial#kkbox_store",                as: :kkbox_announcement
  get '/announcements/preorder',                                                to: "interstitial#preorder",                   as: :preorder_announcement
  get '/announcements/neurotic_media_store',                                    to: "interstitial#neurotic_media_store",       as: :neurotic_media_announcement
  get '/announcements/july_store_sale',                                         to: "interstitial#july_store_sale",            as: :july_store_sale_announcement
  get '/announcements/soundout',                                                to: "interstitial#track_smarts",               as: :soundout_announcement
  get '/announcements/credit_sale',                                             to: "interstitial#credit_sale",                as: :credit_sale_announcement
  get '/announcements/target_music',                                            to: "interstitial#target_music",               as: :target_music_announcement
  get '/announcements/ringtone_sale',                                           to: "interstitial#ringtone_sale",              as: :ringtone_sale_announcement
  get '/announcements/yandex_store',                                            to: "interstitial#yandex_store",               as: :yandex_store_announcement
  get '/announcements',                                                         to: "interstitial#index"
  get '/announcements/claro_store',                                             to: "interstitial#claro_store",                as: :claro_store_announcement
  get '/announcements/account_profile_survey',                                  to: "interstitial#account_profile_survey",     as: :account_profile_survey_announcement
  get '/announcements/mobile',                                                  to: "interstitial#mobile",                     as: :mobile_announcement
  get "/artist_services",                                                       to: "services#index",                          as: :artist_services
  get "/live",                                                                  to: redirect("http://live.tunecore.com/")
  get "/discography/myaccount",                                                 to: redirect("/")
  get "/dashboard",                                                             to: "dashboard#index", as: :dashboard
  get "/dashboard/create_an_album",                                             to: "dashboard#create_an_album"
  get "/dashboard/create_account",                                              to: "dashboard#create_account"
  post "/dashboard/close_splits_pending_banner",                                to: "dashboard#close_splits_pending_banner"
  get "/spanish_faq",                                                           to: "index#spanish_faq", as: :spanish_faq
  get "/marketing_and_promotion",                                               to: "index#m_and_p_new", as: :marketing_and_promotion
  get "/bizdev",                                                                to: "index#bizdev",      as: :bizdev
  post "/index/contact_bizdev",                                                 to: "index#contact_bizdev", as: :contact_bizdev
  get "/my_media",                                                              to: "my_media#index", as: :my_media
  get "/sxtc",                                                                  to: redirect("/")
  get "/affiliate/*any",                                                        to: redirect("/")
  get "/alfred",                                                                to: redirect("/")
  get "/alfredrecords",                                                         to: redirect("/")
  get "/campcraze",                                                             to: redirect("/")
  get "/cmw2011",                                                               to: redirect("/")
  get "/couponcabin",                                                           to: redirect("/")
  get "/daisyrockrecords",                                                      to: redirect("/")
  get "/fanbridge",                                                             to: redirect("/")
  get "/fanbridge_product",                                                     to: redirect("/")
  get "/medianet",                                                              to: redirect("/index/music_stores")
  get "/mog",                                                                   to: redirect("/")
  get "/pledgemusic",                                                           to: redirect("/")
  get "/soundcloud",                                                            to: redirect("/")
  get "/spotify",                                                               to: redirect("/index/sell_your_music_on_spotify")
  get "/music/:lookup_key",                                                     to: "my_band#index", as: :my_band
  get "/guides",                                                                to: "guides#index"
  get "/copyright",                                                             to: "guides#copyright"
  get "/guides/promote_introduction",                                           to: "guides#promote_introduction"
  get "/guides/promote_blogpromotion",                                          to: "guides#promote_blogpromotion"
  get "/guides/promote_gettingstarted",                                         to: "guides#promote_gettingstarted"
  get "/guides/promote_indieradio",                                             to: "guides#promote_indieradio"
  get "/guides/promote_itunes",                                                 to: "guides#promote_itunes"
  get "/guides/promote_marketing",                                              to: "guides#promote_marketing"
  get "/guides/promote_merchandising",                                          to: "guides#promote_merchandising"
  get "/guides/promote_pressmedia",                                             to: "guides#promote_pressmedia"
  get "/guides/promote_resources",                                              to: "guides#promote_resources"
  get "/guides/promote_streetmarketing",                                        to: "guides#promote_streetmarketing"
  get "/guides/basics_of_vinyl",                                                to: "guides#basics_of_vinyl"
  get "/guides/healthcare",                                                     to: "guides#healthcare"
  get "/guides/thirteen_ways_to_make_money",                                    to: "guides#thirteen_ways_to_make_money"
  get "/guides/basics_to_know",                                                 to: "guides#basics_to_know"
  get "/guides/basics_of_copyright",                                            to: "guides#basics_of_copyright"
  get "/guides/basics_of_mastering",                                            to: "guides#basics_of_mastering"
  get "/guides/basics_of_mixing",                                               to: "guides#basics_of_mixing"
  get "/guides/sellmusiconline",                                                to: "guides#sellmusiconline"
  get "/guides/sixrights",                                                      to: "guides#sixrights"
  get "/freealbum",                                                             to: redirect("/")
  get "/credits",                                                               to: "products#distribution_credits"
  get "/products/add_composer_registration_to_cart",                            to: "products#add_composer_registration_to_cart"
  post "/products/add_stores_to_cart", to: "products#add_stores_to_cart"
  get "/royalty_payments/:id/:filename.:format",                                to: "composers#download"
  get "/price_formats/format_price",                                            to: "price_formats#format_price"
  get "/people/profile",                                                        to: "people#profile", as: :people_profile
  get "/people/:id/restart_survey/:source",                                     to: "people#restart_survey", as: :restart_survey
  post "/uploads/get_put_presigned_url",                                        to: "uploads#get_put_presigned_url"
  post "/uploads/validate_asset",                                               to: "uploads#validate_asset"
  post "/uploads/save_streaming_asset",                                         to: "uploads#save_streaming_asset"
  match "/uploads/register", to: "uploads#register", as: :register_upload, via: [:get, :post]
  match "/sales/release_report", to: "sales#release_report", as: :release_report, via: [:get, :post]
  match "/sales/date_report", to: "sales#date_report", as: :date_report, via: [:get, :post]
  match "/sales/song_report",                                                     to: "sales#song_report",             as: :song_report, via: [:get, :post]
  match "/sales/store_report",                                                    to: "sales#store_report",            as: :store_report, via: [:get, :post]
  match "/sales/country_report",                                                  to: "sales#country_report",          as: :country_report, via: [:get, :post]
  get "/sales/individual_release",                                              to: "sales#individual_release",      as: :individual_release
  get "/sales/individual_song_release",                                         to: "sales#individual_song_release", as: :individual_song_release
  get "/sales/export",                                                          to: "sales#export",                  as: :export
  get "/store_sales_reports",                                                   to: "sales#store_report"
  match "/youtube_royalties/release_report",                                    to: "you_tube_royalties#release_report", as: :youtube_royalties_release_report, via: [:get, :post]
  get "/you_tube_royalties/release_report",                                     to: "you_tube_royalties#release_report"
  match "/youtube_royalties/sales_period_report",                               to: "you_tube_royalties#sales_period_report", as: :youtube_royalties_sales_period_report, via: [:get, :post]
  get "/you_tube_royalties/sales_period_report",                                to: "you_tube_royalties#sales_period_report"
  match "/youtube_royalties/song_report",                                       to: "you_tube_royalties#song_report", as: :youtube_royalties_song_report, via: [:get, :post]
  get "/you_tube_royalties/song_report",                                        to: "you_tube_royalties#song_report"
  match "/youtube_royalties/artist_report",                                     to: "you_tube_royalties#artist_report", as: :youtube_royalties_artist_report, via: [:get, :post]
  get "/you_tube_royalties/artist_report", to: "you_tube_royalties#artist_report"
  get "/youtube_royalties/individual_release_report",                           to: "you_tube_royalties#individual_release_report"
  get "/youtube_royalties/individual_song_report",                              to: "you_tube_royalties#individual_song_report"
  get '/albums/:album_id/preorders/set_data',                                   to: "preorders#set_data", as: :set_preorder_data
  post '/albums/:album_id/preorders/set_data',                                  to: "preorders#set_data"
  get '/videography',                                                           to: 'videos#index', as: 'videography'
  get "/my_account",                                                            to: "my_account#index",                      as: :my_account
  match "/my_account/withdraw",                                                 to: "my_account#withdraw",                   as: :withdraw, via: [:get, :post]
  match "/my_account/withdraw_paypal",                                          to: "my_account#withdraw_paypal",            as: :withdraw_paypal, via: [:get, :post]
  match "/my_account/withdraw_check",                                           to: "my_account#withdraw_check",             as: :withdraw_check, via: [:get, :post]
  get "/my_account/export_reporting_period",                                    to: "my_account#export_reporting_period",    as: :export_reporting_period
  get "/my_account/export_transactions",                                        to: "my_account#export_transactions",        as: :export_transactions
  get "/my_account/export_intake_transactions",                                 to: "my_account#export_intake_transactions", as: :export_intake_transactions
  get "/my_account/view_royalty_invoice",                                       to: "my_account#view_royalty_invoice",       as: :view_royalty_invoice
  get "/my_account/credit_note_royalty_invoices/:id",                           to: "outbound_royalty_invoices#show",        as: :view_cr_royalty_invoices
  get "/my_account/trend_reports",                                              to: "my_account#trend_reports",              as: :trend_reports
  get "/my_account",                                                            to: "my_account#index",                      as: :my_account_home
  get "/my_account/list_transactions",                                          to: "my_account#list_transactions",          as: :list_transactions
  get "/my_account/sales_record_export",                                        to: "my_account#sales_record_export",        as: :sales_record_export
  get "/my_account/sales/during/:year/:month",                                  to: "sales#date_report",                     as: :sales_month
  get "/my_account/sales/during/:year",                                         to: "sales#date_report",                     as: :sales_year
  get "/my_account/sales/by_album/during/:year",                                to: "sales#date_report",                     as: :by_album_year
  get "/my_account/sales/by_album/during/:year/:month",                         to: "sales#date_report",                     as: :by_album_month
  get "/my_account/sales/for_album/:id/during/:year",                           to: "sales#release_report",                  as: :for_album_year
  get "/my_account/sales/for_album/:id/during/:year/:month",                    to: "sales#release_report",                  as: :for_album_month
  get "/my_account/youtube_exports",                                            to: "my_account#you_tube_exports",           as: :you_tube_exports
  get "/my_account/youtube_export",                                             to: "my_account#you_tube_export",            as: :you_tube_export
  get "/my_account/exports",                                                    to: "my_account#exports"
  get "/affiliates",                                                            to: redirect("/")
  get "/affiliates/tutorial",                                                   to: redirect("/")
  get '/MSOffice/cltreq.asp',                                                   to: "index#nothing"
  get '/_vti_bin/owssvr.dll',                                                   to: "index#nothing"
  get '/public/alive',                                                          to: "index#keepalive"
  get "/song_registrations(/:path)",                                            to: redirect { |params, request| "/compositions" + (params[:path].nil? ? "" : "/#{params[:path]}") }
  get '/composers/tos',                                                         to: "composers#tos"
  get "/synch(/*other)",                                                        to: redirect { |params| "/sync/#{params[:other]}".chomp("/") }
  get "/mastered_tracks_new",                                                   to: redirect("/mastered_tracks")
  get "/index_new",                                                             to: "index#index"
  get '/friends',                                                               to: 'friend_referral#show'
  get '/index',                                                                 to: 'index#index'
  get "/ascap",                                                                 to: redirect("/")
  get "/promo1",                                                                to: redirect("/")
  get "/beheard",                                                               to: redirect("/")
  get "admin/product_reports",                                                  to: "product_reports#index"
  get "account/release_account",                                                to: "account#release_account"
  get "/add_store_to_cart",                                                     to: "missing_salepoint_purchases#show"
  get "/publishing/opt_out",                                                    to: "pub_opt_outs#show"
  post "/publishing/opt_out",                                                   to: "pub_opt_outs#create"
  get "/apple_for_artists",                                                     to: "service_for_artists#apple"
  get "/spotify_for_artists",                                                   to: "service_for_artists#spotify"
  get '/authenticate_spotify_artists',                                          to: 'service_for_artists#spotify_access'
  get '/spotify_redirect',                                                      to: 'service_for_artists#spotify_redirect_access'
  get '/youtube_oac',                                                           to: 'service_for_artists#youtube_oac'
  post '/youtube_authentication',                                               to: 'service_for_artists#authenticate_youtube_user'
  get '/youtube_auth_callback',                                                 to: 'service_for_artists#youtube_auth_callback'
  resources :plans do
    collection do
      get :dont_want_a_plan
      get :plan_renewal_due
      get :verify_cart_already_has_a_plan
      post :replace_plan_in_cart
    end
    resources :person_plans
  end
  resources :survey_responses, only: [:update, :create]

  resources :spatial_audio_uploads, only: [] do
    collection do
      post :get_put_presigned_url
      post :register
      post :validate_asset
      get :checklist_partial
    end
  end

  resource :tc_accelerator, only: [] do
    collection do
      post :opt
      post :do_not_notify
    end
  end

  resources :bulk_release_subscriptions, path: 'release_subscriptions', only: [:index, :create]

  resources :stored_bank_accounts do
    collection do
      get :process_response
      get :check_image
    end
  end

  resources :payoneer_status, only: [:index]
  resources :payoneer_fees, only: [:index]

  resources :eft_batch_transactions, path: 'bank_withdrawals', only: [:new, :create, :show, :destroy, :confirm] do
    post :confirm, on: :collection
  end

  resources :person_preferences, path: 'payment_preferences', only: [:index, :create, :update]

  namespace :people do
    get '/payment_options',  to: 'payment_options#index'
    get '/order_history',    to: 'order_history#index'
  end

  get '/account_settings',   to: "people#edit", as: "account_settings"
  get '/people/payout_details_mobile_view', to: "people#payout_details_mobile_view"
  get '/people/set_default_phone_prefix', to: "people#set_default_phone_prefix"

  resources :people, only: [:edit, :update, :new, :create] do
    member do
      get :complete_verify
      get :send_verification_email
      get :unverified
      put :update_email_phone
      get :edit_email_phone
      get :renewal_billing_info
      post :confirm_vat_info
      patch :update_renewal_billing_info
      patch :update_account_settings
      patch :update_priority_artist
      patch :update_vat_information
      patch :update_company_information
      patch :update_self_billing_status
      patch :update_paypal_kyc_info
      patch :confirm_current_tax_form
      patch :request_secondary_tax_form
    end
  end

  resources :tax_form_revenue_streams, only: [:create]
  resources :email_change_requests, only: [] do
    collection do
      get :complete_verification
    end
  end

  resources :store, controller: 'store', only: [:index] do
    get "/product/:product_id", to: "store#product", as: :product, on: :collection
  end

  get "/apple_music", to: "services#apple_music"
  post "/opt_in_apple_music", to: "services#opt_in_apple_music"
  get :discography, to: "discography#index"

  resources :cert_redemptions, path: 'redeem', only: [:index, :create, :thank_you]

  resources :tc_social, only: [:index, :new, :create]
  put "/cancel_subscription", to: "person_subscriptions#update", as: "cancel_subscription"

  resource :youtube_preferences, only: [:show, :create, :update] do
    get :log_api_error
  end

  resource :youtube_monetizations, only: [:show] do
    get :tos
    get :agree_to_terms
    get :mark_as_eligible
    get :mark_as_ineligible
    post :monetize
    post :mark_as_ineligible
    post :monetize_or_mark
    post :setup_ytm
  end

  namespace :facebook_track_monetization, path: 'facebook_tracks' do
    resource :dashboard, only: [:show]
    resource :subscription, only: [:new, :create]
  end

  namespace :youtube_track_monetization, path: "youtube_tracks" do
    resource :dashboard, only: [:show]
  end

  resource :ytm_tracks, only: [:show] do
    post :confirm
  end

  resource :cash_advances, only: [:create] do
    get :request_advance
  end

  put "/tax_tokens/:id", to: "tax_tokens#update", as: :update_tax_token
  # Sync Licensing Routes
  scope "/sync", as: "sync" do
    root to: "sync/index#index"

    get "/home", to: "sync/index#home", as: :home
    get "/terms_and_conditions", to: "sync/index#terms_and_conditions", as: :terms_and_conditions
    get "/about", to: "sync/index#about", as: :about
    get "/playlists", to: "sync/index#playlists", as: :playlists
    get "/help", to: "sync/index#help", as: :help
    get "/login", to: "sync/sessions#new", as: :login
    get "/logout", to: "sync/sessions#destroy", as: :logout
    get "/search/query", to: "sync/search#search", as: :search_query

    resource :search, controller: "sync/search", only: [:show, :create]

    resource :chart, controller: "sync/charts", only: [] do
      collection do
        get :all_time
        get :week
      end
    end

    resources :license_requests, controller: "sync/license_requests", only: [:new, :create, :index, :show, :update] do
      get  :download,     on: :member
      post :confirmation, on: :collection
    end

    resources :people,      controller: "sync/people", only: [:new, :create, :edit, :update]

    resource  :people,      controller: "sync/people",      only: [:show]
    resources :memberships, controller: "sync/memberships", only: [:new, :create]
    resources :sessions,    controller: "sync/sessions",    only: [:new, :create, :destroy]
    resources :favorites,   controller: "sync/favorites",   only: [:index] do
      post :toggle_favorite, on: :collection
    end
  end

  # Sync admin routes
  scope "/sync/admin", as: "sync_admin" do
    root to: "sync/admin/dashboards#show"

    resources :memberships, controller: "sync/admin/memberships", only: [:index] do
      get :approve, on: :member
      get :decline, on: :member
      get :resend,  on: :member
    end

    resources :license_requests, controller: "sync/admin/license_requests", only: [:index, :show] do
      post :change_status, on: :member
    end

    resources :people, controller: "sync/admin/people", only: [:index, :show]
  end

  resource :store_manager, controller: "store_manager", only: [:show, :create] do
    post :add_apple_music, on: :collection
  end

  resources :soundout_reports, only: [:index, :show], path: "fanreviews" do
    collection do
      get :purchase
      post :purchase
      get  :available
      post :available
      post :purchase_reports
    end
    member do
      get :market_potential
      get :sample_group
      get :overall_ratings
      get :review_analysis
      get :reviews
      get :ingrooves_fontana
    end
  end

  resources :soundout_albums, only: [:index, :show] do
    resources :soundout_songs, only: [:index]
  end

  resources :soundout_songs, only: [:index]

  resources :products do
    member do
      get :add_to_cart
      get :distribution_credits
    end

    collection do
      post :add_to_cart
      post :add_to_cart_with_credit
      post :add_to_cart_with_plan_credit
      get :add_renewals_to_cart
      get :add_splits_collaborator_to_cart
    end
  end

  resources :background

  namespace :two_factor_auth do
    resource :authentications, only: [:show, :new, :create]
    resource :enrollments, only: [:show, :update, :destroy]
    resource :sessions, only: [:new, :show, :create]
    resource :preferences, only: [:show, :create, :destroy]
  end

  resources :singles do
    member do
      get :preview_title, to: "preview_titles#show"
      get :preview_artist, to: "preview_artists#show"
      get :monetize, to: redirect("/youtube_tracks/dashboard")
      get :confirm_monetize
      post :confirm_monetize
    end

    collection do
      get :preview_title, to: "preview_titles#show"
      get :preview_artist, to: "preview_artists#show"
    end

    resource :song do
      get :new_upload, on: :member
    end
  end

  resources :ringtones do
    member do
      get :preview_title, to: "preview_titles#show"
      get :preview_artist, to: "preview_artists#show"
    end

    collection do
      get :preview_title, to: "preview_titles#show"
      get :preview_artist, to: "preview_artists#show"
    end
  end

  namespace :facebook, module: :specialized_release do
    resources :albums, only: [:new, :edit]
    resources :singles, only: [:new, :edit]
  end

  namespace :discovery_platforms, module: :specialized_release do
    resources :albums, only: [:new, :edit],  controller:'albums'
    resources :singles, only: [:new, :edit], controller:'singles'
  end

  resources :band_photos

  resources :trend_reports, only: [:index] do
    collection do
      get :release_view
      get :song_view
      get :top_markets
      get :top_songs
      get :top_release
      get :top_countries
    end
  end

  resource :reset_password
  resource :session

  resource :cover do
    resource :background
  end

  post '/salepoints/add_active_stores_to_album', to: 'salepoints#add_active_stores_to_album'

  resource :cart, only: [:show] do
    member do
      get  :confirm_and_pay
      post :finalize
      post :skip_confirm_and_pay
      get :finalize_after_otp
      get :finalize_after_redirect
      post :generate_stored_card_nonce
      get :adyen_payment_methods
      get :adyen_finalize_after_redirect
      get :dropin
      get :splits_collaborator_popup
    end
  end

  resources :history,       only: [:index, :show]

  resources :subscriptions do
    collection do
      post :keep
      post :cancel
      post :change_card

      get :select_card
      get  :buy
      post :buy
    end
  end

  resources :tour_dates
  resources :band_photos

  resources :sales_reports

  resources :purchases do
    delete :remove_code, on: :member

    collection do
      delete :remove_all_for_store
      delete :remove_all_salepoint_subscriptions
      delete :remove_all_for_expander
    end
  end

  resources :itunes_links

  scope("/cart/purchases/:purchase_id", as: "purchase") do
    resource :cert, controller: "cert"
  end

  #
  # Invoices
  #
  resources :invoices, only: [:show] do
    get :thanks, on: :member
    get :download, on: :member, format: true, defaults: { format: 'pdf' }
    get :refunds, on: :member

    resources :credit_note_invoices, only: [:show]
  end

  #
  # Albums
  #
  resources :albums, controller: 'album' do
    member do
      get :edit_linernotes
      get :preview_artist, to: "preview_artists#show"
      get :preview_title, to: "preview_titles#show"
      get :confirm_delete
      get :distribution_panel
      get :distribution_songs_status
      get :album_meta_data
      get :get_myspace_page
      get :monetize, to: redirect("/youtube_tracks/dashboard")
      get :confirm_monetize
      post :confirm_monetize
      get :update_renewal
      get :edit_renewal
      get :stop_cancellation
      get :cyrillic_release
      get :get_preorder_data
    end

    resource :track_order, controller: 'track_order'
    resources :songs do
      member do
        get :new_upload
        get :delete
        get :preview_title, to: "preview_titles#show"
      end
    end

    resource  :artwork, controller: "artwork"
    resources :artwork_suggestions do
      member do
        get :preview
      end
    end

    resources :purchases, controller: 'album_purchases'

    resources :salepoints do
      resource :artwork_template, controller: 'artwork_template'
      collection do
        get :discovery_platforms
        post :activate_itunes_ww
      end
    end

    resource :salepoint_subscriptions, only: [:create, :destroy]
    resources :salepoint_subscriptions, only: [] do
      get :toggle_active, on: :member
    end
  end

  delete "royalty_split_songs/:royalty_split_song_id", to: "royalty_splits#remove_song", as: :destroy_royalty_split_song
  resources :royalty_splits, except: [:create] do
    collection do
      post "/new", to: "royalty_splits#create", as: :create
      get :albums
      post "/add_songs", to: "royalty_splits#add_songs", as: :add_songs_to
      get "/invited", to: "royalty_splits#invited_splits", as: :invited
      get :invited_splits_onboarding
      post "/accept_all_splits", to: "royalty_splits#accept_all_splits", as: :accept_all
    end
    post "/accept_split", to: "royalty_splits#accept_split", as: :accept, on: :member
  end

  resources :my_artists, only: [:index, :show] do
    post :mapping, to: "artist_mappings#create"
  end

  resources :videos, only: [:index, :show]

  resources :notifications, only: [:index] do
    get :dropdown, on: :collection
    get :archive,   on: :member
    post :action,   on: :member
  end

  resources :product_reports, only: [:index] do
    get :detail_reports, on: :collection
    get :preorder_reports, on: :collection
  end

  namespace :ytm do
    resources :salepoint_songs, only: [:index] do
      post :update, on: :collection
      get :play_song
    end

    resources :salepoints, only: [:update]
  end
  resources :stored_credit_cards, except: [:show] do
    collection do
      get :country_cities
    end
  end
  resources :stored_paypal_accounts, only: [:destroy] do
    collection do
      get :ec_step1
      get :ec_step3
      get :error
    end
  end
  #
  # Publishing stuff
  #
  resources :compositions, controller: 'song_registrations' do
    collection do
      get :paginate
      get :resolve_disputes
      post :send_disputes
    end
  end

  resources :publishing_composition_splits do
    collection do
      get :paginate
      post :confirm_splits
    end
  end

  resources :composers do
    member do
      patch :update_cae_number
    end
    resource :tax_info
  end

  namespace :publishing_administration do
    resources :congratulations, only: [:show]
    resources :enrollments, only: [:show, :new, :create, :edit, :update]
    resources :terms_and_conditions, only: [:show, :update]
    resources :compositions, only: [:show]
    resources :composers, only: [:index, :edit, :update] do
      member do
        post :resend_lod
        post :sync_opt_in
        post :sync_opt_out
      end
    end

    resources :publishing_composers, only: [:index, :edit, :update] do
      member do
        post :resend_lod
        post :sync_opt_in
        post :sync_opt_out
      end

      resources :publishing_compositions, only: [:index]
      resources :publishing_terms_and_conditions, only: [:show, :update]
    end
  end

  resources :legal_documents, only: :show

  resources :mastered_tracks, to: redirect("http://aftermaster.tunecore.com")

  resources :friend_referral do
    collection do
      get :signup
      get :referee_list
      get :payouts
    end
  end

  get "refer-a-friend", to: 'friend_referral#new'

  resources :song_library_uploads, only: [:create]

  resources :new_reports, only: [:index] do
    get :refresh, on: :collection
  end

  resources :payout_provider_registrations, only: [:create]
  resources :payout_provider_logins, only: :create
  resource  :payout_provider_registration_landings, only: :show
  resource  :payout_provider_login_landings, only: :show

  resources :payout_transfers, only: [:new, :create, :show] do
    post :confirm, on: :collection
  end

  resource :balance_histories, only: [:show], path: "balance_history" do
    collection do
      get :filter, to: "balance_histories/filters#show"
    end
  end

  namespace :webhooks do
    post 'sift', to: 'sift/webhooks#update', if: FeatureFlipper.show_feature?(:sift_webhooks)
  end

  #######################################################################################
  #######################################################################################
  ################ PLACE ALL ADMIN ROUTES IN THE FOLLOWING NAMESPACE ####################
  #######################################################################################
  #######################################################################################

  namespace :admin do
    resources :songs do
      get :play_song
    end

    resources :foreign_exchange_pegged_rates, only: [:index, :create, :new] do
      get :preview_price, on: :collection
    end
    resources :track_monetization_judgements, only: [] do
      collection do
        post :mass_judgement
      end
    end
    resources :youtube_track_monetization_judgements, only: [:index]
    resources :facebook_track_monetization_judgements, only: [:index]
    resource :mass_monetization_block, only: [:show, :create]
    resources :legal_documents, only: :show

    resource :mass_retry_songs, only: [:show, :create] do
      post :encumbrance_info
    end

    namespace :payment do
      post :mass_pay_paypal
      post :mass_pay_check
      post :cancel_payment
      post :process_payment
    end
    resources :delivery_batches, only: [:index] do
      post :send_batch_complete, to: "batch_completes#create"
    end

    resource :add_salepoint, only: [:edit, :update]

    resources :composers_managers, only: [:show, :edit, :update, :destroy] do
      post :imports, to: "composers_managers_imports#create"
    end

    resources :publishing_composers_managers, only: [:show, :edit, :update, :destroy] do
      post :imports, to: "publishing_composers_managers_imports#create"
    end

    resources :composers, only: :show do
      resources :compositions, only: [:index, :update] do
        collection do
          get :search
          get :download_csv
        end
        member do
          put :update_status
          put :update_eligibility
        end
      end
      collection do
        get  :list
        get  :download_client_import
      end
    end

    namespace :accounts, path: "/accounts/:account_id" do
      resources :composers, only: [:edit, :update] do
        get :terms_of_service
      end

      resources :publishing_composers, only: [:edit, :update] do
        get :terms_of_service
      end
    end

    namespace :accounts, path: "/accounts/:account_id" do
      resources :publishing_composers, only: [:edit, :update] do
        get :terms_of_service
        post :update_lod
      end
    end

    resources :terminated_composers, only: [:new, :create, :show, :update, :destroy]
    resources :publishing_terminated_composers, only: [:new, :create, :show, :update, :destroy]
    resources :composer_sync_opt_ins, only: [:destroy]
    resources :publishing_composer_sync_opt_ins, only: [:destroy]
    resources :delivery_batches, only: [:index] do
      post :send_batch_complete, to: "batch_completes#create"
    end
    resources :tax_tokens_cohorts do
      resources :tax_tokens, only: [:index, :update]
      post :remove_cohort, to: "tax_tokens_cohorts#remove_cohort", on: :collection
    end

    put "/update_tax_tokens", to: "tax_tokens#update"

    resources :delete_accounts, only: [:new, :create]
    resources :bulk_album_approvals, only: [:index, :new, :create]
    resources :mass_adjustments, only: [:index, :new, :create, :show, :update]

    root to: "dashboard#index", as: :home
    resources :dashboard, only: [:index] do
      collection do
        get :processing_albums
        post :search
      end
    end

    resources :distribution_batch_retry_reports, only: [:index, :show]

    namespace :content_review do
      post :list
      post :show
      post :reasons
      post :song_metadata
      post :update_album
      post :change_album_format_flag
      post :update
    end

    resources :features do
      collection do
        post :remove
      end
    end

    resources :content, only: [:destroy] do
      collection do
        get :images
        get :upload
        get :image_delete
      end
    end

    resources :promotional_products, only: [:index] do
      collection do
        get :current
        get :past
        get :deleted
        get :product
        delete :delete_product
        post :restore_product
        get :order
      end
    end

    resources :reports, only: [:index] do
      collection do
        get :view
        post :view
        get :upload_trending
        get :totals
        get :orphaned_albums
        get :people_with_albums
        get :people_by_album_count
        get :store_totals
        get :itunes_vs_world
        get :show_album_detail_meta
        get :album_detail_meta
        get :album_store_counts
        get :serial_totals
        get :logins
        get :invoices
        get :album_song_counts
        get :signup_referrals
        get :settlement_totals
        get :incomplete_album_totals
        get :signup_overview
        get :videos
        get :requires_reporting_role
      end
    end

    resources :upload_server, only: [:index, :create, :destroy]

    get "/renewals/mass_update", as: :mass_update_renewals

    mount Sidekiq::Web, at: "/worker_queues", constraints: AdminConstraint.new, as: :worker_queues
    mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

    resources :distribution_retry, only: [:create] do
      member do
        patch :update_curated_artists
      end
    end

    namespace :petri do
      get '/mass_takedown',      to: "mass_takedowns#new"
      post '/mass_takedown',     to: "mass_takedowns#create"
      get '/mass_untakedown',    to: "mass_takedowns#edit"
      delete '/mass_untakedown', to: "mass_takedowns#destroy"
      post '/encumbrance_info',  to: "mass_takedowns#encumbrance_info"
      get :retry_distributions
      post :retry_distributions_create
      get :show_metadata
    end

    namespace :distribution do
      resources :errors, only: [:index, :create]
    end

    namespace :distributor do
      resources :errors, only: [:index, :create]
    end

    namespace :reports do
      resources :tc_social_cancellations, only: [:index]
    end

    resources :distribution_reports, only: [:index] do
      collection do
        get :canada
      end
    end

    resources :eft_batch_transactions, path: 'bank_withdrawals', only: [:index, :show, :approve, :remove_approval, :reject, :reject_form, :edit_multiple, :new, :create, :approval_summary] do
      member do
        get :approve
        get :remove_approval
        post :reject
        get :reject_form
      end

      collection do
        get  :approval_summary
        post :edit_multiple
      end
    end

    resources :meta_tags
    resources :stored_bank_accounts, only: [:index, :show]
    resources :eft_batches, only: [:index, :show]
    resources :eft_queries, only: [:index, :show]
    resources :payout_service_fees
    resources :aging_accounts, only: [:index, :create]
    resources :direct_advance, only: [:index, :update]
    resources :external_service_ids, only: [:create]

    resources :invoices, only: [] do
      get :search, on: :collection
    end

    resources :albums do
      collection do
        get :search
        get :flac
        get :salepoint_song_takedown
        get :salepoint_song_remove_takedown
        get :mass_unfinalize_finalize
        post :mass_unfinalize_finalize
        post :destroy_flac
      end

      member do
        get :audit_s3_assets
        get :bundle_album
        get :change_album_format_flag
        get :delete_booklet
        get :dismiss_bundle
        get :edit_renewal
        get :generate_album_tunecore_upc
        get :mark_all_tracks_as_free
        get :recover_s3_assets
        get :remove_all_tracks_as_free
        get :remove_creative
        get :remove_optional_upcs
        get :remove_salepoint_takedown
        get :salepoint_takedown
        get :send_to_bigbox
        get :set_album_as_undeleted
        get :set_album_optional_upc
        get :set_song_clean_version
        get :set_song_parental_advisory
        get :show_edit_renewal_beginning
        get :tracks
        get :update_renewal
        post :add_creative
        post :create_booklet
        post :finalize
        post :replace_booklet
        post :set_album_orig_release_year
        post :set_album_timed_release_date
        post :set_album_sale_date
        post :unfinalize
        put :update_renewal_date
      end

      resource :album_countries
      resource :renewal, only: [:edit, :update]
      resource :salepoint_subscription do
        post :toggle_active, on: :member
      end
      resource :song_distribution_option
      resource :takedown, only: [:create, :destroy]
    end
    get "/albums/:id/download_xml_metadata/:releases_store_id", to: "albums#download_xml_metadata"
    get "/albums/:id/download_json_metadata/:release_id", to: "albums#download_json_metadata"
    resources :ringtones, controller: 'albums' do
      get :tracks, on: :member
    end

    resources :singles, controller: 'albums' do
      get :tracks
    end

    resources :videos do
      get :show
      get :metadata
      get :search, on: :collection
    end

    resources :salepoints do
      member do
        get :block_distribution
        get :unblock_distribution
      end
    end

    resources :rights do
      get :not_permitted, on: :collection
    end

    resources :transactions do
      member do
        get :refund
        put :process_refund
        get :refund_failure
        get :refund_success
        get :no_permission
      end
    end

    resource :locks do
      get :invoices
      get :people
      get :bundles
    end

    resources :settlement_summaries

    resources :cert_batches do
      member do
        get  :certs
        post :certs

        get  :add
        post :add

        get :exp
        post :exp

        get  :admin_only
        post :admin_only
      end
    end

    resources :certs do
      collection do
        get :lookup
        get :new_redemption
        post :redeem
        get :redemption_success
      end
    end

    resources :fraud, only: [:index, :statistics] do
      get :statistics, on: :collection
    end

    resources :notifications do
      post :add_people, on: :member
    end

    resources :targeted_offers do
      member do
        patch  :activate
        patch  :deactivate
        post   :build_population
        get    :edit_population_settings
        patch  :update_population_settings
        get    :price_list
        post   :add_person
        delete :remove_person
        get    :targeted_people
        post   :add_people
        delete :remove_people
        get    :clone
      end

      resources :targeted_products
      resources :targeted_people
      resources :targeted_advertisements
    end

    resources :advertisements
    resources :cms_assets
    resources :cms do
      get :preview
      put :activate
      put :deactivate
    end

    resources :oauth_clients

    resources :new_admin
    resources :notes
    resources :payment, only: [:index]
    resources :blacklisted_paypal_payees, only: [:index, :create, :destroy], as: :blacklisted_paypal_payees

    resources :careers, except: [:show] do
      post :add
      post :remove
      get  :preview
    end

    resources :country_help_links, except: [:show, :edit]
    resources :knowledgebase_links, except: [:show, :edit]

    get  'careers/boilerplates', to: 'careers#boilerplates'
    post 'careers/boilerplates', to: 'careers#save_boilerplates'

    resources :two_factor_auth, only: [:update]

    # all admin person routes go here
    resources :people, only: [:show] do
      member do
        patch :update_profile
        put :reset_pw_mail
        patch :lock_account
        patch :unlock_account
        patch :mark_as_suspicious
        patch :remove_suspicion
        patch :toggle_features
        patch :mark_as_deleted
        get :transactions_for
        get :reset_pw_show
        get :set_song_name
        get :set_album_artist_name
        get :set_album_label_name
        get :set_album_name
        patch :remove_deletion
        get :release_login_lock
        get :complete_verify
        get :migrate_to_bigbox
        get :exports
        get :assets
        get :myspace_friend_id_grabber
        get :get_myspace_page
        get :ambassador
        get :control_account
        get :edit_account
        get :edit_profile
        get :credit_card_transactions
        get :change_status
        get :lock_redemption
        get :unlock_redemption
        get :make_vip
        get :undo_vip
        get :features
        put "/update_tax_tokens", to: "people#update_tax_tokens"
        put :toggle_feature
        patch :remove_dormancy
        resources :balance_transactions, only: [:index]
        get :download_assets
        post :create_recover_assets
        put :block_withdrawals
        put :unblock_withdrawals
      end

      get :search, on: :collection

      resources :soundout_reports, only: :index do
        post :toggle_cancel, on: :member
        get  :fetch_report,  on: :member
        get  :resend_report, on: :member
      end

      resources :albums do
        member do
          get :edit_linernotes
          get :confirm_delete
          post :update_cert
          get :copyright_documents
          post :create_copyright_document
        end

        get :flac, on: :collection

        resource  :album_lock
        resource  :track_order
        resources :songs
        resource  :album_distributions
        resource  :artwork
        resources :bundles

        resources :purchases, controller: 'album_purchases' do
          member do
            get :new_cert
            post :create_cert
          end
        end
      end

      resource  :album_stores, only: [:edit, :update]
      resource  :country_website, only: [:edit, :update]

      resources :invoices do
        member do
          put :settle
          post :resend
        end
      end

      resources :inventories, path: 'credits', only: [:index, :show] do
        resources :inventory_adjustments, only: [:new, :create, :index] do
          get :rollback, on: :member
        end
      end

      resources :credit_usages, only: [] do
        collection do
          post 'destroy_all'
        end
      end

      resources :targeted_offers
      resources :purchases
      resources :subscriptions, only: [:index, :update] do
        resources :subscription_cancellations, only: [:create]
        resources :subscription_events, only: [:index]
      end

      resources :login_events, only: [:index]

      resources :notes

      resources :eft_batch_transactions, path: 'bank_withdrawals', only: [:index]

      resources :stored_bank_accounts, only: [:history] do
        get :history, on: :collection
      end

      resources :stored_paypal_accounts, only: [:history] do
        get :history, on: :collection
      end

      resources :balance_adjustments, only: [:index, :new, :create] do
        get :rollback, on: :collection
      end

      resources :person_preferences, only: [:edit_auto_renewal, :update_auto_renewal] do
        collection do
          get :edit_auto_renewal
          patch :update_auto_renewal
        end
      end

      resources :mastered_tracks, only: [:index] do
        member do
          put :mark_as_refunded
          put :unmark_refund
        end
      end

      resources :creative_external_service_ids, only: [:index, :update]
    end

    resource :balance_transactions, only: [:show] do
      collection do
        get :filter, to: "balance_transactions/filters#show"
      end
    end

    resources :review_reasons
    resources :renewal_batch_transactions, only: [:index, :show]

    # Composers for publishing stuff
    resources :composers do
      collection do
        get :download_songs
        get :list
        get :download_client_import
        get :download_summary
      end
    end

    resources :publishing_composers do
      resources :publishing_compositions, only: [:index, :update] do
        collection do
          get :search
          get :download_csv
          get :populate_missing_compositions
        end
        member do
          put :update_status
          put :update_eligibility
        end
      end
      collection do
        get :download_songs
        get :list
        get :download_client_import
        get :download_summary
      end
    end

    resources :lods do
      get  :download_pdf,    on: :member
      post :update_statuses, on: :collection
      get :search, on: :collection
    end

    resources :publishing_lods do
      get  :download_pdf,    on: :member
      get :search, on: :collection
    end

    resources :batch_balance_adjustments do
      post :post_adjustments, on: :collection
    end

    resources :friend_referrals

    resources :youtube_preferences, only: [:index, :update, :destroy]

    resources :youtube_sr_mail_template_builders, only: [:index, :new, :create]
    resources :youtube_sr_mail_templates

    resources :splits, only: [:index, :update] do
      get :search, on: :collection
      post :update, on: :collection
    end

    resources :publishing_composition_splits, only: [:index, :update] do
      get :search, on: :collection
      post :update, on: :collection
    end

    resources :preorders, only: [:edit, :update]

    resources :distribution_songs, only: [:retry_distribution_song, :takedown_distribution_song, :send_metadata_update_for_distribution_song] do
      collection do
        get :retry_distribution_song
        get :takedown_distribution_song
        get :send_metadata_update_for_distribution_song
      end
    end

    namespace :track_monetization do
      resources :retries, only: [:create]
      resources :takedowns, only: [:create, :destroy]
    end

    resources :failed_takedowns, only: [:index]
    resources :translations, only: [:index]
    resources :bulk_add_stores_by_albums, only: [:new, :create]
    resources :bulk_add_stores_by_users, only: [:new, :create]
    get  'youtube_monies/setup_ytm_opt_out', to: 'youtube_monies#setup_ytm_opt_out'
    get  'youtube_monies/opt_out', to: 'youtube_monies#opt_out'
    get  'youtube_monies/mass_takedown', to: 'youtube_monies#mass_takedown'
    patch "youtube_monies/opt_out", to: "youtube_monies#opt_out"
    put  "youtube_monies/mass_takedown_tracks", to: "youtube_monies#mass_takedown_tracks"
    put  "youtube_monies/block", to: "youtube_monies#block"
    put  "youtube_monies/unblock", to: "youtube_monies#unblock"
    post "track_blocks", to: "track_blocks#create"

    resources :stores, only: [:index] do
      post "pause", to: "stores/pauses#create"
      post "unpause", to: "stores/pauses#destroy"
      post "backfill_start", to: "stores/backfill_starts#create"
    end

    namespace :payout do
      resources :transfer_rollbacks, only: [:index]
      resources :tax_forms, only: [:create], controller: "tax_forms", path: "tax_forms"
      resources :payout_provider_disassociations, only: [:create]
      resources :payout_transfers, only: [:index] do
        resources :approvals, only: [:create], controller: "transfer_approvals", path: "approve"
        resources :rejections, only: [:create], controller: "transfer_rejections", path: "reject"
      end
      post "batch_actions", to: "transfer_batch_actions#create"
    end

    resources :payout_providers, only: [:index] do
      resources :statuses, only: [:create], controller: "payout_provider_statuses", path: "status"
    end

    namespace :qa_tools do
      resources :album_approvals, only: [:index, :create]
    end

    resources :copyright_statuses, only: :update
  end

  namespace :studio do
    root to: "search#index"
  end

  ########### API ROUTING #########

  namespace :api do
    resources :soundout_report_data, only: [:show] do
      get :reviews, on: :member
    end

    get '/available_locales', to: 'locales#available_locales'

    resources :tc_social, only: [:index]
    resources :stored_credit_cards, only: [:index, :create, :update] do
      collection do
        get :braintree_token
        get :payment_method_nonce
      end
    end

    resources :sessions, only: [:create] do
      collection do
        delete :clear_session
        get :fetch_user
      end
    end

    resources :index, only: [:index]

    resources :products do
      collection do
        post :social_add_to_cart
      end
    end

    resource :cart do
      member do
        post :finalize
      end
    end

    resources :cert, only: [:create] do
      post :remove, on: :collection
    end

    resources :soundout_reports, only: [:index]

    resources :distributions, only: [] do
      collection do
        post :enqueue
        get  :ping
        get  :show_state
        post :update_state
      end
    end

    resources :albums, only: [:create]
    resources :songs, only: [:create]
    resources :artwork, only: [:create]
    resources :salepoints, only: [:create]

    resources :people, only: [] do
      collection do
        post :update_account_profile
        post :registration
        patch :update_password
        get :resend_confirmation_email
        post :send_auth_code
        get :vat_information
        post :update_vat_information
        get :non_self_identified_countries
        get :personal_info
        get :personal_info_setting
      end
      member do
        get :complete_verify
        get :show_feature
      end
    end

    resources :purchases, only: %i[show]

    resources :social_login, only: [:create]

    resources :social_two_factor_auth do
      collection do
        post  :generate_auth_code
        post  :validate_auth_code
      end
    end

    resources :song_library_uploads, only: [:index]

    resources :royalty_solutions, only: [] do
      collection do
        get   :validate_account
        get   :validate_assets
        post  :register
      end
    end

    namespace :linkshare do
      resources :songs
      resources :stores, only: [:index]
      resources :preview_link, only: [:index]
      resources :my_links, only: [:create]
    end

    namespace :renderforest do
      resources :in_app_purchase, only: [:create]
      post '/project_status', to: 'projects#project_status'
      resources :release_urls, only: [:index]
      resources :songs, only: [:index]
    end

    namespace :artist_discovery do
      resources :songs, only: [] do
        post :fetch, on: :collection
      end
    end

    namespace :v1 do
      resources :affiliate_reports, only: :create
      get  "/users", to: "users#index"
      get  "/taxform/submit", to: "tax_tokens#update"
      resources :two_factor_auths, only: [:create]
      resources :tax_forms, only: :create

      namespace :payout_transfers do
        get ":payout_id/completed", action: "completed"
        get ":payout_id/pending",   action: "pending"
        get ":payout_id/cancelled", action: "cancelled"
      end

      namespace :payout_providers do
        get ":client_payee_id/approved", action: "approved"
        get ":client_payee_id/declined", action: "declined"
        get ":client_payee_id/released", action: "released"
        post ":client_payee_id/payee_details_changed", action: "payee_details_changed"
      end

      namespace :docusign do
        post "document_status", action: "update_document_status"
      end

      namespace :braintree do
        post "account_updater_report"
        post "tc_account_updater_report"
        post "bi_account_updater_report"
      end
    end

    #To do- Currently supports both react and retool apis, Clean up the routes after retool live
    [:v2, :v3].each do |version|
      namespace version, defaults: { format: :json } do
        match "*all", to: "base#cors_preflight", constraints: { method: 'OPTIONS' }, via: [:options]
        resources :sessions, only: [:create], format: :json
        resources :retool_sessions, only: [:create], format: :json

        namespace :web do
          resources :refresh_tokens, only: :create
        end

        namespace :fraud_prevention do
          resources :copyright_claims, only: [:index, :create, :update] do
            collection do
              get :claims_with_sorting
              get :total_claim_count
            end
            member do
              put :deactivate
            end
          end
          resources :copyright_holds_summary, only: :show
        end
        namespace :content_review do
          resources :people, only: [] do
            get :plan_downgrade_eligibility
          end

          post '/get_albums_for_crt_list' => "albums#index"

          resources :albums, only: [:show, :update] do
            collection do
              get :album_for_review
              get :songs_for_album
              get :needed_review_count
            end
            resources :salepoints, only: [] do
              put :block_salepoint
              put :unblock_salepoint
            end

            resource :track_monetizations, only: [] do
              collection do
                put :block
              end
            end

            resources :review_audits, only: :create
          end

          resources :album_flagged_contents, only: [] do
            collection do
              get :expand
            end
          end

          resources :review_audits, only: [] do
            collection do
              post :approval
            end
          end

          resources :songs, only: :update do
            put :retry_audio_fingerprint
          end
        end
        resources :person_plan, only: :show
        resources :plans, only: :index
        resources :downgrade_categories, only: :index
        resources :downgrade_requests, only: [:show, :create] do
          member do
            put :cancel
          end
        end
        resources :flag_reasons, only: :index
        resources :people_flags, only: [:index, :create]
        resource :people_flags, only: [:destroy]
        resources :languages, only: [:index]
        resources :song_roles, only: [:index]
        resources :genres, only: [:index]
        resources :review_reasons, only: :index
        resources :country_websites, only: :index
        resources :stores, only: [:index]

        resources :payments_os_webhooks, only: [] do
          collection do
            post :refunds
          end
        end

        post "/payments_os_webhooks/payments", to: "payu_webhooks#payments"
        get "/suspicious_people_count", to: "people_flags#suspicious_people_count"

        get '/current_user', to: 'people#current_user_from_token'
        get :monetized_stores, to: "stores#monetized_stores"
        resources :people, only: :show do
          collection do
            get :fetch_feature_flags_status
          end
        end

        scope module: 'reward_system', path: 'rs', as: 'reward_system' do
          resources :tiers, only: [:index, :show, :create, :update] do
            member do
              post :add_achievement
              post :remove_achievement
              post :add_certification
              post :remove_certification
              post :add_reward
              post :remove_reward
            end
          end

          resources :rewards, only: [:index, :create, :show, :update] do
            collection do
              get :person_info
            end

            member do
              post :redeem
            end
          end

          resources :certifications, only: [:index, :show, :create, :update]
          resources :achievements, only: [:index, :show, :create, :update]
        end

        scope module: 'reward_system', path: 'rs', as: 'reward_system' do
          resources :tiers, only: [:index, :show, :create]
        end

        resources :refunds, only: [:new, :create] do
          collection do
            get :list_transactions
            get :refund_reasons
          end
        end

        resources :posting_currency_exchange do
          collection do
            get :get_current_eligible_posting_royalties
            put :apply_currency_exchange
            put :make_currency_exchange_editable
            put :approve_currency_exchange
            get :export_posting_data
          end
        end

        resources :disputes, only: [:index] do
          collection do
            get :transaction_types
          end
          member do
            post :accept_dispute
            post :finalise_dispute
            post :submit_evidence
          end
        end

        resources :nav_data, only: :index
      end
    end

    namespace :webhooks, defaults: { foramt: :json } do
      resources :disputes, only: [] do
        collection do
          post :braintree
        end
      end

      resources :adyen, only: [] do
        collection do
          post :authorize_payment
        end
      end
    end

    namespace :translation_service do
      post "phrase_webhook"
    end
    get    '/subscriptions',      to: 'subscriptions#index'
    delete '/subscriptions',      to: 'subscriptions#destroy'
    namespace :subscription do
      get '/plans',               to: 'plans#index', format: :json
      get '/payment_options',     to: 'payment_options#index', format: :json
      post "/purchases/tunecore", to: "tunecore_purchases#create", format: :json
      post "/purchases/apple",    to: "apple_purchases#create", format: :json
      resources :in_app_purchases, path: "purchases/in_app", only: [:create] do
        collection do
          post :purchase_token_update
          post :update_subscription_status
        end
      end
    end

    namespace :social do
      post '/authorize',      to: "sessions#create", format: :json
      get '/user_details',    to: "users#show", format: :json
      post '/password_reset', to: "password_reset#create", format: :json
      get '/transactions', to: 'transactions#index', format: :json
    end

    post "sip/mapped",  to: "sip#mapped", as: "sip_mapped"
    post "sip/vat_posting",  to: "sip#vat_posting"
    post "sip/generate_invoice",  to: "sip#generate_invoice"
    post "sip/posting_complete", to: "sip#posting_complete", as: "sip_posting_complete"
    post "sip/posting_finalize", to: "sip#posting_finalize", as: "sip_posting_finalize"

    get "systemstatus/transaction_status"

    resources :releases, only: [:show]

    resources :encumbrance_summaries, only: [:create]

    namespace :frontstage do
      namespace :two_factor_auth do
        resource :app_callbacks, only: [:create, :show, :destroy]
        resource :resends, only: [:create]
      end
      resource :artists, only: [:show]
      resource :account, only: [:show]
    end

    namespace :backstage do
      put "people/preference", to: "people#update_preference"
      resources :song_data, only: [:create, :index, :update, :destroy]
      delete "song_data/immersive_audio/:id", to: "song_data#destroy_immersive_audio"
      get "singles/:id", to: "albums#show"
      resources :albums, only: [:show, :update] do
        get :fetch_song_ids
      end
      namespace :two_factor_auth do
        resources :prompts, only: :update
        resource  :app_callbacks, only: [:create, :show, :destroy]
        resource  :resends, only: [:create]
      end

      namespace :album_app do
        resources :albums, only: [:create, :update]
        resources :tiktok_releases, only: [:create, :update]
        resources :salepoints, only: [:create]
        resource :distribution_progress, only: [:show]
        get '/external_service_ids/spotify_search_artists', to: "external_service_ids#spotify_search_artists"
        get '/external_service_ids/spotify_recent_albums', to: "external_service_ids#spotify_recent_albums"
        get '/external_service_ids/apple_search_artists', to: "external_service_ids#apple_search_artists"
        get '/external_service_ids/apple_get_artist', to: "external_service_ids#apple_get_artist"
        get '/external_service_ids/spotify_get_artist', to: "external_service_ids#spotify_get_artist"
      end

      namespace :balance_history do
        resource :exports, only: [:create]
      end

      namespace :track_monetization do
        resources :dashboard, only: [:create]
      end

      namespace :publishing_administration do
        resources :publishing_splits, only: [:create, :update]
        resources :multi_tenant_publishing_splits, only: [:create]
        resources :compositions, only: [:show] do
          resource :publishing_splits, only: [:update], on: :collection
        end
        resources :compositions_hidden, only: [:update]
        resources :composers, only: [:update] do
          resources :non_tunecore_compositions, only: [:create]
        end
        get 'non_tunecore_compositions/bulk_upload_sample', to: "non_tunecore_compositions#get_bulk_upload_sample"
        post 'non_tunecore_compositions/bulk_upload', to: "non_tunecore_compositions#bulk_upload"
      end
    end

    namespace :external do
      get '/:person_id/songs', to: 'songs#ad_songs'
      get '/scrapi/callback', to: 'scrapi#callback'
      get '/auth/get_token', to: 'auth#get_token'
      get '/api_client/get_album_status', to: 'api_client#get_album_status'
    end

    namespace :transcoder do
      post '/songs/set_asset_data', to: 'songs#set_asset_data'
      post '/songs/set_streaming_asset', to: 'songs#set_streaming_asset'
      resources :soundout_reports do
        member do
          post :complete_order
        end
      end
    end
  end

  if Rails.env.test?
    scope module: "feature_spec_db_proxy" do
      post  '__factory-bot__', to: "remote_factory_proxies#create"
      patch '__factory-bot__', to: "remote_factory_proxies#update"

      delete "__active-record__", to: "remote_active_record_base_proxies#destroy"
    end

    # NOTE: Retina .js sends head requests to specific image resources, which
    # rails errors on because there is no route defined for them. This is a
    # workaround in testing to make sure that capybara feature specs do not error
    # because of a routing error from a HEAD request to a missing asset.
    match "/images/*asset", to: proc { [200, {}, ['']] }, via: [:head]
  end

  mount Sidekiq::Prometheus::Exporter => '/metrics'
end
