GraphiQL::Rails.config.header_editor_enabled = true

Rails.application.configure do
  config.action_mailer.preview_path = "#{Rails.root}/spec/mailers/previews"
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # SQL Logging Output
  config.logger = Logger.new STDOUT

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :memory_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = false

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = false

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = false

  # Suppress output of asset requests
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.log_level = :debug

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method       = :letter_opener_web

  # WIDGET_STATUS is used to control the release of the widget from :off, :beta (used in conjuntion with the person.interstitial_shown_on), and :on
  WIDGET_STATUS = :on
  config.enable_password_restrictions = false

  # whitelist allowed hosts for dns rebinding attack protection
  # See https://prathamesh.tech/2019/09/02/dns-rebinding-attacks-protection-in-rails-6/
  config.hosts.concat(%w(
    api.tunecore.local
    ca-local.tunecore.org
    crt.tunecore.local
    development.tunecore.com
    development.tunecore.local
    development.tunecore.local.au
    development.tunecore.local.ca
    development.tunecore.local.de
    development.tunecore.local.fr
    development.tunecore.local.in
    development.tunecore.local.it
    development.tunecore.local.uk
    development.tunecore.local.in
    social.tunecore.local
    stats.tunecore.local
    development.tunecore.local.in
    tc-www-puma
  ))

  config.hosts << /[a-z0-9-]+\.ngrok\.io/

  Rails.application.config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins 'http://development.tunecore.local:3001' # tc-web-react
      resource '*', headers: :any, methods: [:get, :post, :patch, :put]
    end
  end

  config.after_initialize do
    PaperTrail.enabled = true
    ActiveRecord::Base.connection.execute("SET collation_database = 'utf8_general_ci' ")
    ActiveRecord::Base.connection.execute("SET collation_connection = 'utf8_general_ci' ")
    rescue ActiveRecord::NoDatabaseError
  end
end

Rack::MiniProfiler.config.skip_paths = ["/images/", "/stylesheets/", "/javascripts/"]
