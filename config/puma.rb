require 'dotenv'

preload_app!
threads_count = ENV.fetch("RAILS_MAX_THREADS") { 1 }.to_i
threads threads_count, threads_count
worker_timeout 300
pidfile ENV["PID_FILE"]
environment ENV.fetch("RAILS_ENV") { "development" }
bind 'tcp://0.0.0.0:3000'

# http://sorentwo.com/2014/08/27/environment-reloading.html
on_worker_boot do
  ENV.update Dotenv::Environment.new(".env.#{ENV['RAILS_ENV']}")
end

before_fork do
  ENV.update Dotenv::Environment.new(".env.#{ENV['RAILS_ENV']}")
end
