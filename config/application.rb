require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'sprockets/railtie'
require 'oauth/rack/oauth_filter'
require_relative '../lib/middlewares/set_cookie_domain'

OAUTH_10_SUPPORT = true

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Settings in config/environments/* take precedence over those specified here.
# Application configuration should go into files in config/initializers
# -- all .rb files in that directory are automatically loaded.
module TcWww
  class Application < Rails::Application
    # config.action_view.javascript_expansions[:defaults] = %w(jquery rails application)

    # Initialize configuration defaults for originally generated Rails version.
    # https://guides.rubyonrails.org/configuring.html#results-of-config-load-defaults
    config.load_defaults "6.0"
    config.autoloader = :classic

    config.active_model.i18n_customize_full_message = true

    config.active_record.cache_versioning = false

    config.active_job.queue_adapter = :sidekiq

    config.allow_external_deliveries = Rails.env.production?

    config.action_mailer.smtp_settings = {
      address: ENV["ACTION_MAILER_HOST"],
      port: ENV["ACTION_MAILER_PORT"],
      domain: ENV["ACTION_MAILER_DOMAIN"]
    }

    config.press_s3_bucket = "http://tunecore.press.s3-website-us-east-1.amazonaws.com"
    config.jobs_s3_bucket  = "http://tunecore.jobs.s3-website-us-east-1.amazonaws.com"

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    if Rails.env.development? || Rails.env.test?
      config.autoload_paths << Rails.root.join("lib")
      config.action_cable.allowed_request_origins = %w[
        http://development.tunecore.local:*
        http://development.tunecore.local.*:*
        https://tunecore.*
        https://*.tunecore.*
      ]
    else
      config.eager_load_paths << Rails.root.join("lib")
    end


    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # Setting middleware
    config.middleware.use Middlewares::SetCookieDomain
    config.middleware.use OAuth::Rack::OAuthFilter

    config.cache_store = :redis_store, {
      url: ENV["REDIS_URL"],
      expires_in: 14.days,
      namespace: ENV["REDIS_NAMESPACE"]
    }

    config.blog_feed_enabled = false
    config.cms_enabled = false
    config.enable_password_restrictions = true

    config.generators do |g|
      g.template_engine :erb
      g.stylesheets true
      g.javascripts true
    end

    config.to_prepare do
      next if Rails.env.production?

      ::GIT_BRANCH, ::GIT_COMMIT =
        if  ENV['ECS_CONTAINER_METADATA_URI'].present?
          url    = ENV.fetch('ECS_CONTAINER_METADATA_URI')
          resp   = Net::HTTP.get_response(URI.parse(url))
          commit = (JSON.load(resp.body) || {}).fetch("Image", "").split('-').last
          ["Not Available, use commit url", commit]
        else
          [`git rev-parse --abbrev-ref HEAD`.strip, `git show --pretty=%H -q`.strip]
        end

      ::GIT_COMMIT_URL = "https://github.com/tunecore/tc-www/commit/#{::GIT_COMMIT}"
    end
  end
end
