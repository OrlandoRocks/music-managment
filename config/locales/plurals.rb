# encoding: utf-8

# Sourced from https://github.com/ruby-i18n/i18n/blob/master/test/test_data/locales/plurals.rb

# The i18n gem does not know the pluralization rules of our custom-named languages.
# We should define those rules here.

# Languages like pl-us, ro-us, and ru-us grammatically have additional plural options (few, many, etc).
# If we want to support these, we will need to update this file with the appropriate rules.

{
  :en => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  :de => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  :fr => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  :it => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "en-au" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "en-ca" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "en-gb" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "en-in" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "cs-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "es-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "fr-ca" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "hu-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "id-us" => { i18n: { plural: { keys: [:other], rule: ->(n) { :other } } } },
  "nl-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "pl-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "pt-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "ro-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "ru-us" => { i18n: { plural: { keys: [:one, :other], rule: ->(n) { n == 1 ? :one : :other } } } },
  "sc-us" => { i18n: { plural: { keys: [:other], rule: ->(n) { :other } } } },
  "tc-us" => { i18n: { plural: { keys: [:other], rule: ->(n) { :other } } } },
  "th-us" => { i18n: { plural: { keys: [:other], rule: ->(n) { :other } } } },
  "tr-us" => { i18n: { plural: { keys: [:other], rule: ->(n) { :other } } } }
}
