---
it:
  compositions:
    add_compositions:
      add_composition: Aggiungi composizione
      add_composition_for: Aggiungi composizione per
      album_name: Titolo album
      cancel: Cancella
      cover_song: È una cover?
      enter_information_about_a_work: Inserisci le informazioni sul brano non distribuito
        da TuneCore
      isrc_number: Numero ISRC
      list_a_cowriter: Aggiungi un co-autore
      ownership_percentage: Quale è la percentuale di cui rivendichi la proprietà?
      ownership_percentage_note: Se ci sono co-autori e/o condivisioni di esempio
        nelle tue canzoni originali, assicurati di inviare solo la tua percentuale
        di proprietà proporzionale. Ad esempio, se condividendo la stessa proprietà
        con un altro scrittore, invii il 50%
      performing_artist: Artista
      public_domain: Questo lavoro è di pubblico dominio? (Selezionare la casella
        se Sì)
      record_label: Etichetta discografica
      register: Invia
      register_another_song: Register another song
      release_date: Data di uscita
      song_title: Titolo
      validations:
        album_name: L’utente ha inserito caratteri non validi o un testo in linguaggio
          non Latino
        duplicate_title: Hai già inviato una canzone con questo titolo per la registrazione,
          non tentare di inviare la stessa canzone più di una volta. Se desideri apportare
          modifiche al brano inviato, contatta l'Assistenza artisti.
        isrc_in_use: ISRC già in uso da questo cantautore
        isrc_number: Deve essere un ISRC valido (12 caratteri alfanumerici)
        percent: Il valore deve essere compreso tra 1 e 100
        performing_artist: L’utente ha inserito caratteri non validi o un testo in
          linguaggio non Latino
        record_label: L’utente ha inserito caratteri non validi o un testo in linguaggio
          non Latino
        release_date: Deve essere formattato come MM / GG / AAAA
        title: L’utente ha inserito caratteri non validi o un testo in linguaggio
          non Latino
    add_shares: Aggiungi quote
    all_shares_are_required_cta: I brani con condivisioni mancanti appariranno incompleti
      e non inviati per la registrazione. Devi fornire informazioni complete sull'autore
      per registrare la tua canzone.
    bulk_compositions:
      limit_exceeded_error: 'Caricamento collettivo non riuscito: supera il limite
        massimo (%{max_limit} opere)'
      modal_text_paragraph_instruction: Se hai più composizioni che ' inviare contemporaneamente,
        ti offriamo la possibilità di scaricare questo file di esempio per inserire
        tutte le informazioni sulla composizione e caricarle nella dashboard di pubblicazione.
      modal_text_paragraph_note: Tieni presente che devi caricare nel formato esatto
        fornito altrimenti le tue composizioni non verranno aggiunte con successo.
      sample_file_name: File di caricamento di esempio
      upload_button_text: Caricare un file
      upload_duplicate_isrc_error: 'ISRC duplicato trovato sulle righe: %{lines}'
      upload_duplicate_title_error: 'Titolo duplicato trovato sulle righe: %{lines}'
      upload_error: Errore durante l'elaborazione del caricamento collettivo. Per
        favore riprova.
      upload_failed_error: Una o più composizioni contengono errori. Controlla e prova
        a caricare di nuovo.
      upload_invalid_info_error: 'Informazioni non valide sulla riga: %{line} , colonne:
        %{columns}'
      upload_in_progress: Caricamento e convalida del file ...
      upload_isrc_in_use_error: 'ISRC %{isrc} è già in uso. Riga / e: %{lines}'
      upload_missing_info_error: 'Informazioni richieste mancanti sulla riga: %{line}'
      upload_succeeded: Caricamento collettivo riuscito. Le tue composizioni verranno
        create a breve.
      upload_text: Caricamento collettivo di composizioni
      upload_title_in_use_error: 'Il titolo %{title} è già in uso. Riga / e: %{lines}'
      upload_zero_rows_submitted_error: 0 righe trovate nel file
    can_not_register: Cover? Non puoi registrare questo brano.
    compositions_card:
      add_compositions: Aggiungi brano
      cae: Numero CAE/IPI (11 cifre)
      cancel: Cancel
      compositions: "{{numCompositions}} Composizioni"
      customer_support: Please contact customer support to change your date of birth,
        PRO, or add an affiliated PRO.
      dob: Date of Birth
      draft_songs_button: Canzoni con condivisioni mancanti
      edit_info: Edit Info
      edit_songwriter: Edit Songwriter
      find_cae_numbers_help_link: Per avere maggiori informazioni sui codici CAE/IPI
        e a come trovare il tuo puoi cliccare
      first_name: First Name
      last_name: Last Name
      middle_name: Middle Name
      name_change_matches_tax_records: 'Puoi cambiare il nome solo se ha già effettuato
        la modifica presso la tua PRO (ad esempio: SIAE, ASCAP, BIMI, et.)'
      ok: OK
      prefix: Prefix
      prefixes:
        Dr.: Dr.
        Mr.: Mr.
        Mrs.: Sig.ra
        Ms.: Ms.
      pro: PRO
      save: Save
      see_tax_information_here: See tax information here
      shares_missing: "{{numSharesMissing}} Quote mancanti"
      shares_reported: "{{numShares}} Quote documentate"
      suffix: Suffix
      suffixes:
        I: I
        II: II
        III: III
        IV: IV
        Jr.: Jr.
        Sr.: Sr.
    composition_note: 'Nota: solo i brani originali possono essere inviati per l''amministrazione
      della pubblicazione, ad esempio le registrazioni di copertina.'
    composition_note_description: Se la tua canzone originale include campioni, interpolazioni
      e/o ritmi concessi in licenza, per registrare la tua canzone è necessaria l'autorizzazione
      del titolare/i titolare/i del copyright.
    data_table_headers:
      appears_on: APPARE SU
      composition_title: TITOLO DELLA COMPOSIZIONE
      cowriter_share: QUOTE CO-AUTORE
      status: STATUS
      table_header: MANAGER DELLA COMPOSIZIONE
      your_share: LE TUE QUOTE
    search_placeholder: Search...
    splits_modal:
      appears_on: Appare su
      cae_error_msg: Devi inserire un CAE per il tuo co-autore o scegliere NONE per
        PRO
      cae_ipi_number: Numero CAE/IPI
      cancel: Cancella
      cannot_claim: I cannot claim writers shares
      composition_title: Titolo della composizione
      cowriters_paragraph: Se hai informazioni sui tuoi co-autori e le loro quote
        su questo brano, ti preghiamo di inserirle nei campi qui sotto. Queste informazioni
        ti aiuteranno a registrare le tue canzoni in maniera più accurata possibile.
        Se non conosci tutti i tuoi co-autori, inserisci solo i dati che conosci e
        lascia il resto vuoto.
      cowriter_placeholder: ".05-100 %"
      cowriter_share: Quota co-autore
      done: Done
      do_not_register: Non registrare
      first_name: Nome
      has_been_hidden: "{{split}} has been hidden"
      hide: Nascondere
      hide_prompt: Sei sicuro di voler nascondere questa composizione?
      last_name: Cognome
      list_additional_cowriters: Lista co-autori aggiuntivi
      list_cowriters: Lista co-autori
      pro_affiliation: PRO Affiliation
      public_domain: Questo lavoro è di pubblico dominio? (Selezionare la casella
        se Sì)
      register_and_continue: Registra e Continua
      shares: Report Writer & Co-Writer Shares ({{index}} of {{total_compositions}})
      skip: Salta
      skip_prompt: You've entered a share on this composition. Are you sure you want
        to skip?
      translations_latin_characters_only: Traduzione (caratteri latini)
      writer_and_cowriter_shares: Quote del compositore
      your_writer_share: Le tue quote
    statuses:
      accepted: Accettato
      add_share: Aggiungi quote
      draft: Bozza
      pending_distribution: In attesa di distribuzione
      shares_missing: Quote Mancanti
    thank_you:
      cancel: Chiudi
      header: Compositions added for
      headline: Grazie per aver aggiunto una canzone!
      paragraph_1: Grazie per aver inviato una canzone non distribuita tramite TuneCore!
        Ti preghiamo di attendere poche settimane per il completamento di questo processo.
      paragraph_2: Per usufruire delle opportunità delle sincronizzazioni leggi le
        istruzioni che ti arriveranno via mail.
      retroactively: retroattivamente
    tooltips:
      cannot_claim_shares_content: Se non hai scritto o non controlli questa canzone,
        la registrazione non è idonea. Clicca {{HERE}} per maggiori informazioni.
      cannot_claim_shares_header: 'Consiglio:'
      cowriter_share_content: Le quote sono il modo con cui una canzone è suddivisa
        tra vari compositori secondo l’accordo scelto. Aggiungere un co-autore non
        significa registrarli alla Publishing Administration di TuneCore, o permetterti
        di incassare le loro royalties.
      cowriter_share_header: 'Consiglio:'
      here: qui
      list_cowriters_content: Le quote sono il modo con cui una canzone è suddivisa
        tra vari compositori secondo l’accordo scelto. Aggiungere un co-autore non
        significa registrarli alla Publishing Administration di TuneCore, o permetterti
        di incassare le loro royalties.
      list_cowriters_header: 'Consiglio:'
      public_domain_content: Una composizione di pubblico dominio è una composizione
        che non ha marchi o diritti d'autore su di essa, quindi può essere utilizzata
        liberamente da altri compositori senza una licenza.
      unknown_writer: Autore Sconosciuto
      your_share_content: Se hai firmato questa canzone insieme a un altro compositore,
        comunicaci la tua percentuale di possesso del brano. Non sei sicuro di cosa
        fare? Ottieni maggiori informazioni {{HERE}}.
      your_share_header: 'Consiglio:'
      your_writer_share_content: Se hai firmato questa canzone insieme a un altro
        compositore, comunicaci la tua percentuale di possesso del brano. Non sei
        sicuro di cosa fare? Ottieni maggiori informazioni {{HERE}}.
      your_writer_share_header: 'Consiglio:'
    validations:
      cae_is_not_a_number: Il numero CAE / IPI non è valido
      cancel: Cancel
      cowriter_share: Il valore deve essere compreso tra 1 e 100.
      first_name: First Name required
      last_name: Last Name required
      translated_name: Assicurati di utilizzare solo caratteri latini senza accenti
        nei campi forniti.
  cover_song_toggle_buton_yes: Sì
  cover_song_toggle_button_no: 'No'
  splits_modal:
    can_not_register: Cover? Non puoi registrare questo brano.
