---
nl-us:
  compositions:
    add_compositions:
      add_composition: Compositie toevoegen
      add_composition_for: Compositie toevoegen voor
      album_name: Albumnaam
      cancel: Annuleren
      cover_song: Is dit een covernummer?
      enter_information_about_a_work: Voer informatie in over een nummer dat niet
        via TuneCore is verspreid.
      isrc_number: ISRC-nummer
      list_a_cowriter: Voeg Co-Writer toe
      ownership_percentage: Van welk percentage van uw aandeel claimt u eigendom te
        zijn?
      ownership_percentage_note: Als er co-schrijvers en/of voorbeeldaandelen zijn
        in uw originele nummers, zorg er dan voor dat u alleen uw pro-rata eigendomspercentage
        indient. Als u bijvoorbeeld in gelijk bezit bent met een andere schrijver,
        zou u 50% indienen
      performing_artist: Uitvoerende artiest
      public_domain: Valt dit werk binnen het Publieke Domein? (Selecteer vakje indien
        Ja)
      record_label: Platenmaatschappij
      register: Registreren
      register_another_song: Registreer nog een nummer
      release_date: Release datum
      song_title: Titel van het nummer
      validations:
        album_name: Mag geen speciale tekens of niet-Latijnse talen bevatten
        duplicate_title: Je hebt al een nummer met deze titel ingediend voor registratie.
          Probeer hetzelfde nummer niet meer dan één keer in te zenden. Neem contact
          op met de artiestenondersteuning als je het ingezonden nummer wilt bewerken.
        isrc_in_use: ISRC wordt al gebruikt door deze songwriter
        isrc_number: Moet een geldige ISRC zijn (12 alfanumerieke tekens)
        missing_performing_artist: Uitvoerend artiest vereist
        missing_title: Titel vereist
        percent: De waarde moet tussen 1 en 100 liggen
        performing_artist: Mag geen speciale tekens of niet-Latijnse talen bevatten
        record_label: Mag geen speciale tekens of niet-Latijnse talen bevatten
        release_date: Moet worden opgemaakt als MM / DD / JJJJ
        title: Mag geen speciale tekens of niet-Latijnse talen bevatten
    add_shares: Voeg aandelen toe
    all_shares_are_required_cta: Nummers met ontbrekende shares verschijnen incompleet
      en worden niet verzonden voor registratie. U moet volledige informatie over
      de songwriter verstrekken om uw nummer te registreren.
    bulk_compositions:
      limit_exceeded_error: 'Bulkupload mislukt: overschrijdt max. Limiet (%{max_limit}
        werken)'
      modal_text_paragraph_instruction: Als je meerdere composities hebt die je '
        wilt indienen, bieden we je de mogelijkheid om dit voorbeeldbestand te downloaden
        om al je compositie-informatie in te vullen en te uploaden naar je publicatiedashboard.
      modal_text_paragraph_note: Houd er rekening mee dat je moet uploaden in het
        exacte formaat dat is opgegeven, anders worden je composities niet succesvol
        toegevoegd.
      sample_file_name: Voorbeeld uploadbestand
      upload_button_text: Upload bestand
      upload_duplicate_isrc_error: 'Dubbele ISRC gevonden op regels: %{lines}'
      upload_duplicate_title_error: 'Dubbele titel gevonden op regels: %{lines}'
      upload_error: Fout bij het verwerken van de bulkupload. Probeer het a.u.b. opnieuw.
      upload_failed_error: Een of meer composities bevatten fouten. Controleer en
        probeer opnieuw te uploaden.
      upload_invalid_info_error: 'Ongeldige informatie op regel: %{line} , kolommen:
        %{columns}'
      upload_in_progress: Uw bestand uploaden en valideren ...
      upload_isrc_in_use_error: 'ISRC %{isrc} is al in gebruik. Lijn (en): %{lines}'
      upload_missing_info_error: 'Ontbrekende vereiste online informatie: %{line}'
      upload_succeeded: Bulkupload geslaagd. Uw composities worden binnenkort gemaakt.
      upload_text: Bulkuploadsamenstellingen
      upload_title_in_use_error: 'Titel %{title} is al in gebruik. Lijn (en): %{lines}'
      upload_zero_rows_submitted_error: 0 rijen gevonden in bestand
    can_not_register: Cover? U kunt dit nummer niet registreren.
    compositions_card:
      add_compositions: Composities toevoegen
      cae: CAE / IPI-nummer (11 cijfers)
      cancel: Annuleren
      compositions: "{{numCompositions}} composities"
      customer_support: Neem contact op met de klantenondersteuning om uw geboortedatum
        of PRO te wijzigen of om een aangesloten PRO toe te voegen.
      dob: Geboortedatum
      draft_songs_button: Nummers met ontbrekende aandelen
      edit_info: Bewerk informatie
      edit_songwriter: Bewerk Songwriter
      find_cae_numbers_help_link: Voor meer informatie over CAE / IPI-nummers en hoe
        u de uwe kunt vinden, klikt u op
      first_name: Voornaam
      last_name: Achternaam
      middle_name: Midden-naam
      name_change_matches_tax_records: Dien alleen een naamswijziging in als je je
        naamswijziging al succesvol hebt geregistreerd bij je PRO (bijvoorbeeld bij
        ASCAP, BMI, enz.).
      ok: OK
      prefix: Voorvoegsel
      prefixes:
        Dr.: Dr
        Mr.: Dhr.
        Mrs.: Mevrouw
        Ms.: Mej.
      pro: PRO
      save: Opslaan
      see_tax_information_here: Zie belastinginformatie hier
      shares_missing: "{{numSharesMissing}} aandelen ontbreken"
      shares_reported: "{{numShares}} aandelen gerapporteerd"
      suffix: Achtervoegsel
      suffixes:
        I: ik
        II: II
        III: III
        IV: IV
        Jr.: Jr.
        Sr.: Sr.
      tip: Tip
      tip_message: "{{TIP}}: Als u hier een nummer heeft ingevoerd dat begint met
        55, kan dit uw accountnummer zijn. Controleer of u uw {{WRITER}} CAE / IPI
        invoert"
      writer: schrijver
    composition_note: 'Opmerking: alleen originele nummers kunnen worden ingediend
      voor publicatiebeheer, d.w.z. geen cover-opnamen.'
    composition_note_description: Als je originele nummer samples, interpolaties en/of
      gelicentieerde beats bevat, heb je toestemming nodig van de auteursrechthebbende(n)
      / eigenaar(s) om je nummer te registreren.
    data_table_headers:
      action: Actie
      appears_on: VERSCHIJNT OP
      composition_title: SAMENSTELLINGSTITEL
      cowriter_share: AANDEEL VAN DE COWRITER
      isrc: ISRC
      status: STATUS
      submitted_at: INGEDIEND OP
      table_header: SAMENSTELLING MANAGER
      your_share: JIJ DEELT
    edit_composition: Compositie bewerken
    ntc_note: Als je composities hebt ingediend die niet via Tunecore worden verspreid,
      kan het tot een week duren voordat ze hier worden weergegeven, maar ze zijn
      verzonden voor registratie.
    please_note: Houd er rekening mee dat
    search_placeholder: Zoeken...
    splits_modal:
      appears_on: Verschijnt op
      cae_error_msg: U moet een CAE invoeren voor uw co-schrijver of GEEN kiezen voor
        PRO
      cae_ipi_number: CAE / IPI-nummer
      cancel: annuleren
      cannot_claim: Ik kan geen schrijversaandelen claimen
      composition_title: Samenstelling Titel
      cowriters_paragraph: Als je informatie hebt over je co-schrijvers en hun aandeel
        in dit nummer, geef die informatie dan hieronder op. Deze informatie helpt
        ons uw liedjes zo nauwkeurig mogelijk te registreren. Als u niet al uw medeschrijvers
        kent, voert u in wat u weet en kunt u de rest blanco laten.
      cowriter_placeholder: 0,05-100%
      cowriter_share: Co-schrijver aandeel
      done: Gedaan
      do_not_register: Schrijf u niet in
      first_name: Voornaam
      has_been_hidden: "{{split}} is verborgen"
      hide: Verbergen
      hide_prompt: Weet u zeker dat u deze compositie wilt verbergen?
      last_name: Achternaam
      list_additional_cowriters: Maak een lijst van extra co-schrijvers
      list_cowriters: Lijst co-schrijvers
      pro_affiliation: PRO-aansluiting
      public_domain: Valt dit werk binnen het Publieke Domein? (Selecteer vakje indien
        Ja)
      register_and_continue: Registreer en ga door
      shares: Aandelen van Report Writer & Co-Writer ({{index}} van {{total_compositions}})
      skip: Overslaan
      skip_prompt: Je hebt ' een aandeel in deze compositie ingevoerd. Weet u zeker
        dat u dit wilt overslaan?
      translations_latin_characters_only: Vertalingen (alleen Latijnse karakters)
      writer_and_cowriter_shares: Aandelen van Report Writer & Co-Writer
      your_writer_share: Uw Writer Share
    statuses:
      accepted: Aanvaard
      add_share: Voeg Share toe
      draft: Ontwerp
      ineligible: Komt niet in aanmerking
      pending_distribution: In afwachting van distributie
      resubmitted: opnieuw ingediend
      shares_missing: Aandelen ontbreken
      submitted: Ingezonden
      terminated: Beëindigd
      verified: Aanvaard
    thank_you:
      cancel: Dicht
      header: Composities toegevoegd voor
      headline: Bedankt voor het toevoegen van je nummer!
      paragraph_1: Bedankt voor het indienen van uw niet door Tunecore gedistribueerde
        nummer! Het kan enkele weken duren voordat het registratieproces is voltooid.
      paragraph_2: Volg de instructies in een e-mail die u binnenkort zult ontvangen
        om in aanmerking te komen voor synchronisatiemogelijkheden.
      retroactively: met terugwerkende kracht
    tooltips:
      cannot_claim_shares_content: Als je dit nummer niet hebt geschreven of niet
        onder controle hebt, komt het niet in aanmerking voor registratie. Klik {{HERE}}  voor
        meer informatie.
      cannot_claim_shares_header: 'Tip:'
      cowriter_share_content: Met aandelen wordt het eigendom van een nummer verdeeld
        tussen meerdere songwriters op basis van hun overeenkomst met elkaar. Door
        een co-schrijver op de lijst te zetten, registreert u ze NIET voor TuneCore
        Publishing Administration en staat u niet toe hun royalty's voor hen te innen.
      cowriter_share_header: 'Tip:'
      here: hier
      list_cowriters_content: Met aandelen wordt het eigendom van een nummer verdeeld
        tussen meerdere songwriters op basis van hun overeenkomst met elkaar. Door
        een co-schrijver op de lijst te zetten, registreert u ze NIET voor TuneCore
        Publishing Administration en staat u niet toe hun royalty's voor hen te innen.
      list_cowriters_header: 'Tip:'
      public_domain_content: Een Public Domain-compositie is een compositie waarop
        geen handelsmerk of copyright rust, zodat deze zonder licentie door andere
        componisten vrij kan worden gebruikt.
      unknown_writer: Onbekende schrijver
      your_share_content: Als je dit nummer samen met een andere schrijver hebt geschreven,
        vertel ons dan alleen wat je eigendomspercentage van het nummer is. Weet u
        niet zeker wat u moet doen? Meer informatie over aandelen {{HERE}}.
      your_share_header: 'Tip:'
      your_writer_share_content: Als je dit nummer samen met een andere schrijver
        hebt geschreven, vertel ons dan alleen wat je eigendomspercentage van het
        nummer is. Weet u niet zeker wat u moet doen? Meer informatie over aandelen
        {{HERE}}.
      your_writer_share_header: 'Tip:'
    validations:
      cae_is_not_a_number: CAE / IPI-nummer is ongeldig
      cancel: Annuleren
      cowriter_share: De waarde moet tussen 1 en 100 liggen
      first_name: Voornaam (verplicht
      last_name: Achternaam vereist
      total_percent: Het totale aandeel% moet tussen 1 en 100 liggen
      translated_name: Gebruik alleen Latijnse tekens zonder accenten in de daarvoor
        bestemde velden.
  cover_song_toggle_buton_yes: Ja
  cover_song_toggle_button_no: Nee
  splits_modal:
    can_not_register: Cover? U kunt dit nummer niet registreren.
