---
fr-ca:
  album_app:
    add_main_artist: Ajouter un autre artiste principal
    advanced_distribution_header: Fonctionnalités de diffusion avancées
    advanced_distribution_subheader: 'Remarque : les fonctionnalités suivantes ne
      sont disponibles que dans le plan professionnel et vous devrez effectuer une
      mise à niveau lors du paiement.'
    album_label: Nom du Label
    album_language: Langue de l'album
    album_title: Titre de l'album
    apple_manual_id:
      headline: Collez un lien vers votre profil Apple.
      placeholder: Lien vers la page de l'artiste
      subtext: Accédez à votre page d'artiste sur Apple et copiez votre URL Apple
    artist_errors:
      length: Le nom de l'artiste ne doit pas dépasser 120 caractères
    artist_modal_summary:
      apple:
        will_create_page: Une page d'artiste Apple sera créée.
      headline: Résumé
      spotify:
        will_create_page: Une page d'artiste Spotify sera créée.
      step_skipped: Étape sautée
      will_save_selection: Votre sélection sera enregistrée
      will_save_url: L'URL / URI de votre page sera enregistrée
    artist_name_input:
      header: Quel ' est le nom de l'artiste ' ?
      placeholder: Nom d'artiste
      step_label: Nom d'artiste
    artist_search:
      artist_missing: Vous ne voyez pas votre artiste ? Si vous avez une page d'artiste,
        collez le lien ci-dessous pour que votre sortie aille sur la bonne page
      latest_album: Dernier album
      top_album: Meilleur album
    back_button: Retour
    button_no: Non
    button_yes: Oui
    calendar_picker_cancel: Annuler
    calendar_picker_ok: OK
    clean_version_error: Veuillez sélectionner si une version explicite de cette chanson
      existe ou non
    clean_version_prompt: Existe-t-il une version explicite de cette chanson?
    clean_version_warning: Sélectionnez ' oui ' si votre communiqué comporte du contenu
      explicite. La soumission de métadonnées incorrectes entraînera un retard ou
      un rejet de la distribution
    continue_button: Continuer
    create_album: Créer un album
    create_new_artist_id:
      create_btn: Créer une page {{serviceName}} pour moi
      headline: Souhaitez-vous que nous créions une page d'artiste {{serviceName}} ?
      skip_btn: Passer
      subtext: TuneCore créera une nouvelle page d'artiste lors de la livraison de
        cet album.
    create_single: Créer un single
    duplicate_primary_artist: Cet artiste a déjà été ajouté
    edit_album: Modifier l'album
    edit_single: Modifier le single
    explicit: Explicite
    explicit_error: Veuillez sélectionner si cette piste est explicite ou non
    explicit_info: Le contenu explicite comprend tous les jurons, les références à
      la violence ou à la consommation de drogue et toutes les paroles sexuellement
      graphiques ou suggestives. Les œuvres d'art qui affichent de la violence, de
      la consommation de drogue ou du contenu graphique à caractère sexuel seront
      également considérées comme explicites et bloquées à la discrétion de TuneCore.
    explicit_prompt: Cette chanson a-t-elle des paroles explicites?
    explicit_read_more_url: https://support.tunecore.com/hc/en-us/articles/115006691948?_ga=2.98569565.2102437598.1591018418-352799260.1588960511
    golive_date: Heure de sortie (Spotify uniquement)
    golive_date_note: Définissez l'heure à ' vous ' souhaitez que votre sortie soit
      mise en ligne sur Spotify.
    label_errors:
      length: Le nom du libellé doit comporter moins de 120 caractères
    language: Langage
    main_artist: Artiste principal
    manual_id:
      button_link: Veuillez fournir un lien
      error_messages:
        fetch: La récupération a échoué
        id: ID incorrect
        link: Lien incorrect. Veuillez vous assurer que vous avez correctement copié
          l'intégralité du lien.
      paste: Pâte
      url_label: URL / URI
    missing: manquant
    not_previously_released: Non
    optional: En option
    optional_isrc: ISRC
    optional_isrc_note: Que signifie ISRC ?
    optional_isrc_note_url: https://support.tunecore.com/hc/en-us/articles/115006499567?_ga=2.166511101.2102437598.1591018418-352799260.1588960511
    original_release_date: Date de sortie d'origine
    plans:
      artist_not_yet_purchased: Frais supplémentaires requis pour tous les nouveaux
        artistes principaux sortis de ce compte.
      artist_purchase_will_apply: Limite d'artiste dépassée. Surclassement payant
        requis à la caisse.
    present: Présent
    previously_released: Publié précédemment?
    primary_genre: Genre principal
    processing_with_ellipsis: En cours de traitement...
    read_more: En savoir plus
    recording_location: Emplacement d'enregistrement
    release_scenario_absolute: 12:00 AM EST / NYC et en même temps dans tous les pays
      quel que soit le fuseau horaire
    release_scenario_absolute_example: 'Exemple: 00h00 à New York, 5h00 à Londres'
    release_scenario_relative: 12 h 00 dans le fuseau horaire de l'auditeur '
    release_scenario_relative_example: 'Exemple: 00h00 à New York, 00h00 à Londres'
    remove_button: Retirer
    required_field: Champ obligatoire
    sale_date: Date de sortie
    sale_date_error: La date de sortie doit être postérieure ou égale à la date de
      sortie d'origine
    sale_date_note: Besoin de plus d'informations sur les dates de sortie? Consultez
      notre FAQ.
    sale_date_tip: 'CONSEIL: définissez votre date de sortie dans 4 semaines à compter
      d''aujourd''hui pour donner aux magasins le temps de revoir votre sortie.'
    save_album_and_add_songs: Enregistrer l'album et ajouter des chansons
    save_selections: Enregistrer les sélections
    scheduled_release_upgrade: Mise à niveau payante requise à la caisse pour une
      version prévue à une date ultérieure
    secondary_genre: Genre secondaire
    single_title: Titre unique
    skip: Passer
    snackbars:
      artist_selected: Artiste sélectionné
      url_saved: URL / URI de la page de l'artiste enregistrés
    spotify_manual_id:
      button_link: Veuillez fournir un lien
      error_message: Lien incorrect. Veuillez vous assurer que vous avez correctement
        copié l'intégralité du lien.
      headline: Collez un lien vers votre profil Spotify.
      paste: Pâte
      placeholder: Lien vers la page de l'artiste
      subtext: Accédez à votre page d'artiste sur Spotify et copiez votre URI Spotify
    store_max_artists_warning: S'il y a quatre (ou plus) artistes principaux, les
      magasins peuvent répertorier ' Divers artistes ' à la place des noms d'artistes
      individuels.
    sub_genre: Sous-genre
    territory_picker:
      aria_modal_content_label: Formulaire modal pour spécifier les territoires de
        sortie
      back_to_form: Retour au formulaire
      back_to_overview: Retour à l'aperçu
      edit_list: Liste d'édition
      exclude: Exclure
      excluded: Exclu
      field_label: Cette sortie peut-elle être vendue dans le monde entier?
      finish_adding: Terminer l'ajout
      form_level_selection_descriptions:
        exclusion: Cet {{album_type}} ' pas été vendu ' en
        inclusion: Cet {{album_type}} sera vendu en
      include: Intégrer
      included: Inclus
      overview_header: Voulez-vous vendre cet {{album_type}} dans le monde entier?
      picker_selection_descriptions:
        exclusion: Vendez {{album_type}} dans le monde entier, mais excluez
        inclusion: Cet {{album_type}} sera vendu en
      remove_all: Enlever tout
      search_headers:
        exclusion: Quels pays souhaitez-vous exclure de la publication?
        inclusion: Dans quels pays souhaitez-vous rendre cette version disponible?
      search_instruction: Continuez à taper pour ajouter d'autres pays
      search_placeholder: Rechercher un pays
      selection_type_descriptions:
        exclusion: Vendre dans le monde entier, mais exclure certains pays
        inclusion: Vendre uniquement dans certains pays
        worldwide: Vendre dans le monde entier
      toggle_menu: Basculer le menu
      top_radio:
        restrictions: Non, j'impose certaines restrictions
        worldwide: Oui
    upc_ean_code: Code UPC / EAN
    upc_errors:
      length: UPC doit être composé de 12 ou 13 chiffres
      type: UPC doit être un nombre
    upc_note: Si vous avez déjà un UPC pour cette version, veuillez ajouter. Sinon,
      pas de problème, nous en créerons un pour vous.
    various_artists_modal_aria: Confirmez la modification de la valeur de divers artistes.
    various_artists_modal_body: Si votre album n'a pas le même artiste principal sur
      chaque piste, il s'agit d'une compilation / album d'artistes divers. L'artiste
      principal de l'album sera répertorié comme ' Artistes variés ' dans les magasins
      et vous devrez saisir l'artiste principal pour chaque piste de votre album individuellement
      pendant que vous poursuivez le processus de téléchargement.
    various_artists_modal_cancel: Annuler
    various_artists_modal_continue: Continuer
    various_artists_modal_header: Etes-vous sûr?
    various_artists_prompt: Ceci est un album d'artiste varié
    yes_previously_released: Oui
  clean_version_warning: Sélectionnez ' oui ' si votre communiqué comporte du contenu
    explicite. La soumission de métadonnées incorrectes entraînera un retard ou un
    rejet de la distribution
  explicit: Explicite
  explicit_info: Le contenu explicite comprend tous les jurons, les références à la
    violence ou à la consommation de drogue et toutes les paroles sexuellement graphiques
    ou suggestives. Les œuvres d'art qui affichent de la violence, de la consommation
    de drogue ou du contenu graphique à caractère sexuel seront également considérées
    comme explicites et bloquées à la discrétion de TuneCore.
  label_errors:
    length: Le nom du libellé doit comporter moins de 120 caractères
