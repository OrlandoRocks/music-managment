
########################################################
# THIS FILE SHOULD ONLY BE EDITED IF YOU ARE ADDING
# A NEW STAGE (A NEW ENVIRONMENT TO DEPLOY TO).
#
# ALL CONFIG CHANGES SHOULD HAPPEN IN THE APPROPRIATE
# ENVIRONMENT NAMED CONFIG FILE.
#######################################################

require 'capistrano'
require 'new_relic/recipes'
require 'bundler/capistrano'

set :stages, %w(r3_studio_p r3_www_p rails3_staging studio staging sandbox production artwork artprod qa qa2 qa3)
set :default_stage, "staging"
require 'capistrano/ext/multistage'

`ssh-add`
default_run_options[:pty] = true
set :application, "tc"

require './config/boot'
require 'airbrake/capistrano'
