require 'dotenv'

workers 5
threads 8,16
pidfile ENV["PID_FILE"]
environment ENV["RAILS_ENV"]
bind 'tcp://0.0.0.0:3000'
daemonize

on_restart do
  ENV.update Dotenv::Environment.new(".env.#{ENV['RAILS_ENV']}")
end
