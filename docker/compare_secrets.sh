#!/bin/bash

# Usage: Run a diff against two secret files
# This script takes two arguments, the secret files you want
# to compare, but will default the second argument to "production"
#
# Example:
# docker/compare_secrets.sh qa2     #=> compares qa2 secrets against production secrets
# docker/compare_secrets.sh qa2 qa5 #=> compares qa2 secrets against qa5 secrets

env1=$1
env2=${2:-"production"}

env1_file="tmp/secrets.txt.$env1.current"
env2_file="tmp/secrets.txt.$env2.current"

if [ "$1" == "development" ] || [ "$1" == "test" ]; then
  BUCKET=secrets-dev
fi

SECRET_ENVS=(
  qa
  qa1
  qa2
  qa3
  qa4
  qa5
  qa9
  staging
  production
)

for ENV in "${SECRET_ENVS[@]}"; do
  if [[ "$1" == $ENV ]]; then
    BUCKET=secrets
  fi
done

aws s3 cp "s3://$BUCKET.tunecore.com/${env1}/tc-www/secrets.txt" - | cut -d '=' -f 1 | sort >> $env1_file
aws s3 cp "s3://$BUCKET.tunecore.com/${env2}/tc-www/secrets.txt" - | cut -d '=' -f 1 | sort >> $env2_file

if [[ -s $env1_file  ]] && [[ -s $env2_file ]]; then
  difftext=$(git diff --no-index $env1_file $env2_file)

  if [[ -n "$difftext" ]]; then
    echo "$difftext"
  fi
else
  echo 'Error: Unable to locate secrets.txt file on s3'
fi

rm $env1_file
rm $env2_file
