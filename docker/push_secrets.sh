#!/bin/bash

scrub() {
  text="$1"
  echo "$text" | sed 's/=.*//g'
}

notify_slack() {
  difftext="$1"
  # Get user info from aws, needs jq
  user=$(aws sts get-caller-identity | grep Arn)
  # If someone is curious about this, read on bash parmeter substitution
  # Remove everything till /
  user=${user//*\//}
  # Remove remaining double quote (") at the end
  user=${user//\"/}
  # Remove trailing new line characters
  user=${user//[$'\t\r\n ']/}
  # This needs env variable SLACK_WEBHOOK to be set correctly
  slack_message=$(echo -e 'Secrets Updated by '"${user}"'!\nDiff:\n```\n'"${difftext}"'\n```')
  SLACK_WEBHOOK=https://hooks.slack.com/services/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 ./bin/notify_slack "$slack_message" "push_secrets"
}

notification_text=""
push_secrets() {
  env="$1"
  if [[ ! $(command -v aws) ]]; then
    echo "You must have aws installed"
    exit
  fi

  if [ "$env" == "development" ] || [ "$env" == "test" ]; then
    BUCKET=secrets-dev
  fi

  SECRET_ENVS=(
    qa
    qa1
    qa2
    qa3
    qa4
    qa5
    qa6
    qa7
    qa8
    qa9
    qa10
    qa11
    uat
    uat2
    uat3
    dev1
    dev2
    dev3
    dev4
    dev5
    staging
    staging-auto
    believe1
    believeqa
    translate
    production
  )

  for ENV in "${SECRET_ENVS[@]}"; do
    if [ "$1" == $ENV ]; then
      BUCKET=secrets
    fi
  done

  aws s3 cp "s3://$BUCKET.tunecore.com/$env/tc-www/secrets.txt" "tmp/secrets.txt.$env.current"

  difftext=$(git diff --no-index "tmp/secrets.txt.$env.current" "tmp/secrets.txt.$env")
  if [[ -n "$difftext" ]]; then
    echo "Something has changed!"
    echo "$difftext"

    while true; do
      read -r -p "Do you still wish to push the secrets up? (y/n) " yn
      case $yn in
      [Yy]*)
        notification_text+=$(printf '%s\n' "$difftext")

        aws s3 cp "tmp/secrets.txt.$env" "s3://$BUCKET.tunecore.com/$env/tc-www/secrets.txt"

        break
        ;;
      [Nn]*) break ;;
      *) echo "Please answer y/n." ;;
      esac
    done
  fi

  rm "tmp/secrets.txt.$env.current"
}

push_secrets "development"
push_secrets "test"
push_secrets "qa"
push_secrets "qa1"
push_secrets "qa2"
push_secrets "qa3"
push_secrets "qa4"
push_secrets "qa5"
push_secrets "qa6"
push_secrets "qa7"
push_secrets "qa8"
push_secrets "qa9"
push_secrets "qa10"
push_secrets "qa11"
push_secrets "uat"
push_secrets "uat2"
push_secrets "uat3"
push_secrets "dev1"
push_secrets "dev2"
push_secrets "dev3"
push_secrets "dev4"
push_secrets "dev5"
push_secrets "staging"
push_secrets "staging-auto"
push_secrets "believe1"
push_secrets "believeqa"
push_secrets "translate"
push_secrets "production"

if [ -n "$notification_text" ]; then
  notify_slack "$(scrub "$notification_text")"
fi
