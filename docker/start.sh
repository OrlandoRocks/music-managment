#!/bin/bash

cd /tc-www
BINARY=$1

if [ "$BINARY" == "" ]; then
  echo "BINARY not set, exiting..."
  exit 1
fi

case $BINARY in
"puma")
  echo "Starting cron"
  cp ./docker/crontab.puma /etc/crontab
  /etc/init.d/cron start

  echo "Starting Web Environment..."
  bundle exec puma -C config/puma.rb
  ;;
"spring")
  echo "Starting Test Environment..."
  rm -rf /tmp/spring-* && bundle exec spring server
  ;;
"sidekiq")
  echo "Starting Sidekiq Environment..."
  mkdir -p /var/run/sidekiq

  if [ "$SIDEKIQ_ENV" == "delivery" ]; then
    bundle exec sidekiq -t 25 -C config/sidekiq_delivery.yml
  elif [ "$SIDEKIQ_ENV" == "default" ]; then
    bundle exec sidekiq -t 25 -C config/sidekiq.yml
  elif [ "$SIDEKIQ_ENV" == "delivery-backfill" ]; then
    bundle exec sidekiq -t 25 -C config/sidekiq_delivery_backfill.yml
  fi
  ;;
*)
  echo "BINARY not recognized, exiting..."
  exit 1
  ;;
esac
