#!/bin/bash

declare -A ENVKEY2PATH
declare -A PATH2VAL

if [ -z "$RAILS_ENV" ]; then
  echo >&2 "ERROR: missing RAILS_ENV environment variable"
  exit 1
fi

if [ -z "$SECRETS_FILE" ]; then
  echo >&2 "ERROR: missing SECRETS_FILE environment variable"
  exit 1
fi

if [ -z "$DEPLOYMENT_FILE" ]; then
  echo >&2 "ERROR: missing DEPLOYMENT_FILE environment variable"
  exit 1
fi

if [ "$RAILS_ENV" == "development" ] || [ "$RAILS_ENV" == "test" ]; then
  eval $(cat .env.${RAILS_ENV} | sed 's/^/export /')
else
  echo "Pulling secrets into the ENV"
  aws s3 cp s3://${SECRETS_FILE} - >.docker.secrets
  aws s3 cp s3://${DEPLOYMENT_FILE} - >.docker.deploy

  # Export only the RAILS_ENV
  eval $(cat .docker.secrets | grep RAILS_ENV | sed 's/^/export /')

  # Export the AWS_REGION
  eval $(cat .docker.secrets | grep AWS_REGION | sed 's/^/export /')

  cat .docker.secrets .docker.deploy >.env_temp

  # Get list of variables and the paths in parameter stores
  echo "Checking parameter stores variables"
  while read line; do
    if [ -z "$line" ]; then continue; fi
    key=$(echo "$line" | cut -d'=' -f1)
    path=$(echo "$line" | cut -d'=' -f2 | cut -d'%' -f4)
    ENVKEY2PATH["$key"]="$path"
  done <<<$(cat .env_temp | grep "%%%")

  #echo "Keys: ${!ENVKEY2PATH[@]}"
  #echo "Paths: ${ENVKEY2PATH[@]}"

  # Process the list of values from parameters
  if [[ ! -z "${ENVKEY2PATH[@]}" ]]; then
    # When the env use parameter stores
    # It is possible that the paths can have duplicated values. aws cli can handle it and only return values of unique paths
    echo "Download parameter stores variables from aws"
    ps_json=$(aws ssm get-parameters --region "$AWS_REGION" --with-decryption --names ${ENVKEY2PATH[@]})

    # Check invalid parameters, the script exits with error
    invalid_parameters="$(echo $ps_json | jq -r '.InvalidParameters[]')"
    if [ ! -z "$invalid_parameters" ]; then
      echo "Invalid Parameters: $invalid_parameters"
      exit 1
    fi

    # Parse parameter stores response
    echo "Parse parmater store variables"
    while IFS= read pline; do
      path=$(echo "$pline" | jq -r '.Name')
      val=$(echo "$pline" | jq -r '.Value')
      PATH2VAL["$path"]="$val"
    done <<<$(echo "$ps_json" | jq -c '.Parameters[]')

    # export the variables into the system in case no need to expose the secrets in the .env.$RAILS_ENV file
    # for env_var in ${!ENVKEY2PATH[@]}; do
    #   eval "export $env_var='${PATH2VAL[${ENVKEY2PATH[$env_var]}]}'"
    # done

    # Update the variables in .env file with values from parameter stores
    echo "Replace parameter store variables in the env file"
    touch .env.${RAILS_ENV}
    while read line; do
      if [[ "$line" == *"%%%"* ]]; then
        path=$(echo "$line" | cut -d'=' -f2 | cut -d'%' -f4)
        echo $line | sed "s?%%%$path%%%?'${PATH2VAL[$path]}'?" >>.env.${RAILS_ENV}
      else
        echo $line >>.env.${RAILS_ENV}
      fi
    done <.env_temp

    rm -f .env_temp
  else
    echo "No parameter store variables found. Continue without accessing parameter stores"
    mv .env_temp .env.${RAILS_ENV}
  fi

  eval $(cat .env.${RAILS_ENV} | sed 's/^/export /')
fi

aws s3 cp \
  s3://secrets.tunecore.com/${RAILS_ENV}/tc-www/tunecore-social-2-971bdac22313.json \
  /tc-www/config/tunecore-social-2-971bdac22313.json

# Set container timezone
# TODO: Merge set container timezone logic
echo "Container timezone set to: America/Chicago"
ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime

if [ "$SET_CONTAINER_TIMEZONE" = "true" ]; then
  echo ${CONTAINER_TIMEZONE} >/etc/timezone &&
    dpkg-reconfigure -f noninteractive tzdata
  echo "Container timezone set to: $CONTAINER_TIMEZONE"
else
  echo "Container timezone not modified"
fi

if [ "$RAILS_ENV" != "development" ] && [ "$RAILS_ENV" != "test" ]; then
  ./script/get_ssh_keys.sh
  ./script/get_paypal_keys.sh /tc-www/config/certs/ "$RAILS_ENV"
  ./script/get_apple_music_kit_key.sh /tc-www/config/certs/ "$RAILS_ENV"
  ./script/get_tc_studio_keys.sh /tc-www/config/certs/ "$RAILS_ENV"
  ./script/get_v2_keys.sh /tc-www/config/certs/ "$RAILS_ENV"

  bundle install --without="development test"

  echo "Pulling in new manifest"
  if [ "$RAILS_ENV" == "staging" ]; then
    aws s3 cp --recursive \
      s3://infra.tunecore.com/assets/staging/tc-www/$DEPLOY_ENV/manifests/ \
      public/assets/
  else
    aws s3 cp --recursive \
      s3://infra.tunecore.com/assets/$RAILS_ENV/tc-www/manifests/ \
      public/assets/
  fi
else
  bundle check || bundle install
fi

./docker/wait-for-it.sh $DB_HOST:3306 --timeout=60 --strict -- echo "Database is up!"

if [ "$RUN_MIGRATION" == "true" ]; then
  echo "Running rake db:migrate..."
  bundle exec rake db:migrate
fi

echo "Removing contents of tmp dirs..."
bundle exec rake tmp:clear

echo "Creating log/tmp directory, if not already there..."
mkdir -p log
mkdir -p tmp

exec "$@"
