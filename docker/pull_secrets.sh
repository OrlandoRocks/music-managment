#!/bin/bash

pull_secrets() {

  if [ "$1" == "development" ] || [ "$1" == "test" ]; then
    BUCKET=secrets-dev
  fi

  SECRET_ENVS=(
    qa
    qa1
    qa2
    qa3
    qa4
    qa5
    qa6
    qa7
    qa8
    qa9
    qa10
    qa11
    uat
    uat2
    uat3
    dev1
    dev2
    dev3
    dev4
    dev5
    staging
    staging-auto
    believe1
    believeqa
    translate
    production
  )

  for ENV in "${SECRET_ENVS[@]}"; do
    if [ "$1" == $ENV ]; then
      BUCKET=secrets
    fi
  done

  aws s3 cp s3://$BUCKET.tunecore.com/$1/tc-www/secrets.txt tmp/secrets.txt.$1
}

pull_secrets "development"
pull_secrets "test"
pull_secrets "qa"
pull_secrets "qa1"
pull_secrets "qa2"
pull_secrets "qa3"
pull_secrets "qa4"
pull_secrets "qa5"
pull_secrets "qa6"
pull_secrets "qa7"
pull_secrets "qa8"
pull_secrets "qa9"
pull_secrets "qa10"
pull_secrets "qa11"
pull_secrets "uat"
pull_secrets "uat2"
pull_secrets "uat3"
pull_secrets "dev1"
pull_secrets "dev2"
pull_secrets "dev3"
pull_secrets "dev4"
pull_secrets "dev5"
pull_secrets "staging"
pull_secrets "staging-auto"
pull_secrets "believe1"
pull_secrets "believeqa"
pull_secrets "translate"
pull_secrets "production"
