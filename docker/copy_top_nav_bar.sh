#!/bin/bash

# This can be changed from copies to symlinks if we determine that enabling
# the following of symlinks in nginx is safe for our application.

DIR="./static-assets/public/assets/apps"

find $DIR -name "top_nav_bar*.js.gz" \
          -exec cp '{}' $DIR/top_nav_bar_latest.js.gz \;

find $DIR -name "top_nav_bar*.js" \
          -exec cp '{}' $DIR/top_nav_bar_latest.js \;

ls $DIR
