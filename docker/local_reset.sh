#!/bin/bash

echo "stopping docker containers"
docker-compose stop

echo "removing docker containers"
docker-compose down

echo "stopping docker-sync"
docker-sync stop

echo "cleaning docker-sync"
docker-sync clean

echo "pruning system"
docker system prune

echo "removing env files"
rm -f .env.*

echo "rebuilding images"
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
docker-compose build

echo "rebuilding new env files"
for rails_env in test development; do
  aws s3 cp s3://secrets-dev.tunecore.com/${rails_env}/tc-www/secrets.txt - > tmp/${rails_env}.secrets
  aws s3 cp s3://infra.tunecore.com/deployment/${rails_env}/deployment.txt - > tmp/${rails_env}.deploy
  cat tmp/${rails_env}.secrets tmp/${rails_env}.deploy > .env.${rails_env}
  rm -f tmp/${rails_env}.secrets tmp/${rails_env}.deploy
done

echo "DONE!"

echo "starting docker"
docker-sync-stack start
