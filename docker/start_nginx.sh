#!/bin/bash

echo "Copying nginx configs into place"
>/etc/nginx/nginx.conf
cp /app/docker/nginx/base.conf /etc/nginx/nginx.conf
cp /app/docker/nginx/site.conf /etc/nginx/sites-enabled/default
