#!/usr/bin/env bash
AWS_ACCOUNT_ID=738011128897

if [[ -z "$1" ]] ; then
  echo "Usage: $0 <version_tag>"
  echo 'Please provide the tag version to for the build.'
  exit 1
fi

VERSION=$1

if [ -z "$BASE_IMAGE_TAG" ] || \
  [ -z "$BUNDLE_GEM__FURY__IO" ] || \
  [ -z "$BUNDLE_ENTERPRISE__CONTRIBSYS__COM" ]; then
  echo "Please make sure that all of
  BASE_IMAGE_TAG,
  BUNDLE_GEM__FURY__IO,
  and BUNDLE_ENTERPRISE__CONTRIBSYS__COM are set"
  exit 1
fi

$(aws ecr get-login --no-include-email --region us-east-1)

docker build \
  -f Dockerfile.test \
  -t tunecore/tc-www_test:"$VERSION" \
  --build-arg BUNDLE_ENTERPRISE__CONTRIBSYS__COM \
  --build-arg BUNDLE_GEM__FURY__IO \
  --build-arg BASE_IMAGE_TAG \
  .

docker tag \
  tunecore/tc-www_test"$VERSION" \
  ${AWS_ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/tunecore/tc-www_test:"$VERSION"

docker push \
  ${AWS_ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com/tunecore/tc-www_test:"$VERSION"
