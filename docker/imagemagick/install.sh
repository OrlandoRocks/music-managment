#!/bin/bash
RELEASE_VERSION=$1
aws s3 cp s3://infra.tunecore.com/deployment/production/web/ImageMagick-$RELEASE_VERSION.tar.gz ./ImageMagick.tar.gz
tar -xvf ImageMagick.tar.gz
cd `ls -d ImageMagick*/`
aws s3 cp s3://infra.tunecore.com/deployment/production/web/policy.xml ./config/
aws s3 cp s3://infra.tunecore.com/deployment/production/web/policy.xml ./www/source/
./configure --prefix=/usr
make
make install
ldconfig /usr/local/lib
