import sys
import Image

def print_usage():
	print "Usage: image_dims.py {file}"

# check that the necessary arguments are passed

if len(sys.argv) < 1:
	print_usage()
	quit()

# Load the command line args
original_img_path = sys.argv[1]

image = Image.open(original_img_path)
size = image.size
print "%sx%s" % (size[0],size[1])


