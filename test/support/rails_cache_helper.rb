module RailsCacheHelper
  def with_clean_caching
    Rails.cache.clear
    yield
  ensure
    Rails.cache.clear
  end

  def value_in_cache?(value)
    cache_data.values.map(&:value).any?(value)
  end

  def value_missing_from_cache?(value)
    cache_has_value?(value)
  end

  def value_at_key(value)
    Rails.cache.fetch(value)
  end

  private

  def cache_data
    Rails.cache.instance_variable_get(:@data)
  end
end
