module RailsEnvHelper
  def with_rails_env(test_env)
    before_test_env = Rails.env
    Rails.env = test_env.to_s

    yield
  ensure
    Rails.env = before_test_env
  end
end
