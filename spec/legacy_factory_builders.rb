#
#
#  TCFactory Legacy Factory Builders:
#
#  A re-opening of the TCFactory namespace
#  to allow for legacy definitions of
#  factory methods
#
#
module TCFactory

  module LegacyFactory

    def build_album(options = {})
      #us_state = TCFactory.create(:us_state)
      full_options = album_defaults(options)
      force_locked_by = full_options.delete(:force_locked_by)

      album = Album.create(full_options)
      if force_locked_by
        album.locked_by = force_locked_by
        album.save!
      end
      album
    end

    def build_artist(options = {})
      full_options = artist_defaults(options)
      Artist.new(full_options).tap do |artist|
        artist.save!
      end
    end

    def build_person(options = {})

      full_options = person_defaults(options)
      force_administrator = full_options.delete(:force_is_administrator)
      verified = full_options.delete(:is_verified)

      person = Person.find_by(email: options[:email])
      person ||= Person.new(full_options)
      person.roles << Role.find_by(name: 'Admin') if force_administrator
      person.save!

      if verified
        person.send(:is_verified=, true)
        person.save(:validate=>false)
      end

      person.reload
      person
    end

    def build_admin(options={})
      person = create(:person, options)
      person.send(:is_verified=, true)
      person.roles << Role.create(:name => "Admin")
      person.save!
      person
    end

    def build_reporting_month(options = {})
      # if no options were passed in and the reporting month already exists, then return the existing reporting month
      if options.empty? and rm = ReportingMonth.find_by(report_date: reporting_month_defaults[:report_date])
        return rm
      else
        begin
          ReportingMonth.create!(reporting_month_defaults(options))
        rescue
          ReportingMonth.first
        end
      end
    end

    def build_us_state(options={})
      return UsState.find_by(name: options[:name]) if options[:name]
      return UsState.find_by(abbreviation: options[:abbreviation]) if options[:abbreviation]
      UsState.create!(us_state_defaults(options))
    end

    def build_store_intake(options={})
      StoreIntake.class_for(options[:store_code]).create(
        unique_id:         options[:unique_id]    || rand.to_s,
        reporting_month:   options[:reporting_month]   || build_reporting_month,
        store:             options[:store]             || Store.ITUNES_US,
        local_currency:    options[:local_currency]    || 'USD',
        local_total_cents: options[:local_total_cents] || 0,
        #:usd_actual_cents:  options[:usd_actual_cents]  || 99,
        #:usd_total_cents:   options[:usd_total_cents]   || 99,
        #:usd_payout_cents:  options[:usd_payout_cents]  || 99,
        comment:           options[:comment]           || 'test comment',
        #use db defaults for these
        exchange_rate:     options[:exchange_rate], #NULL
        exchange_symbol:   options[:exchange_symbol] #NULL
      )
    end

    def build_ad_hoc_album_product(item_price = 0.00)
      product = TCFactory.create(:product, {:created_by => Person.find_by(email: "admin@tunecore.com")})
      product_item = TCFactory.create(:product_item, {:product => product, :price => item_price})
      TCFactory.create(:product_item_rule, {:product_item => product_item })
    end

  end
end
