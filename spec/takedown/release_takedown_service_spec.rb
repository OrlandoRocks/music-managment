require "rails_helper"

describe Takedown::ReleaseTakedownService do
  let(:album) { create(:album, :with_salepoints) }
  let(:store_ids) { [] }
  let(:opts) { {} }
  let(:batch_name) { "test" }
  subject(:takedown_service) {
    described_class.new(
      album_id: album.id,
      store_ids: store_ids,
      opts: opts,
      batch_name: batch_name
    ).run
  }

  describe "#run" do
    before do
      stub_const("ENV", { "SNS_RELEASE_TAKEDOWN_URGENT_TOPIC" => "takedown_queue" })
      allow(Sns::TakedownNotifier).to receive(:perform).and_return(true)
    end

    context "when salepoint takedown" do
      let(:store_ids) { album.salepoints.pluck(:store_id) }

      it "processes salepoint takedowns" do
        subject
        album.salepoints.reload.each do |salepoint|
          expect(salepoint.takedown_at).to_not eq(nil)
        end
      end

      it "sends an SNS notification" do
        expect(Sns::TakedownNotifier).to receive(:perform).with(
          {
            album_ids_or_upcs: [album.id],
            store_ids: store_ids,
            delivery_type: "takedown",
            topic_arn: ENV["SNS_RELEASE_TAKEDOWN_URGENT_TOPIC"]
          }
        )
        subject
      end

      it "notifies Airbrake when a takedown is unsuccessful" do
        allow_any_instance_of(Salepoint).to receive(:takedown!).and_return(false)
        expect(Airbrake).to receive(:notify).once
        subject
      end
    end

    context "when album takedown" do
      it "processes an album level takedown" do
        subject
        expect(album.reload.takedown_at).to_not eq(nil)
      end

      it "notifies Airbrake when a takedown is unsuccessful" do
        allow_any_instance_of(Album).to receive(:takedown!).and_return(false)
        expect(Airbrake).to receive(:notify).once
        subject
      end
    end
  end
end
