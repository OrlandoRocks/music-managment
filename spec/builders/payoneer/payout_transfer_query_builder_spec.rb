require 'rails_helper'

describe Payoneer::PayoutTransferQueryBuilder do
  let!(:person)           { create(:person, :admin, name: "Devonta Freeman") }
  let!(:person2)          { create(:person, :admin, name: "Todd Gurley") }
  let!(:payout_provider)  { create(:payout_provider, person: person) }
  let!(:payout_provider2) { create(:payout_provider, person: person2) }
  let!(:payout_transfer1) { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE, amount_cents: 3000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::ACCOUNT) }
  let!(:payout_transfer2) { create(:payout_transfer, tunecore_status: PayoutTransfer::SUBMITTED, provider_status: PayoutTransfer::NONE, amount_cents: 5000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAYPAL) }
  let!(:payout_transfer3) { create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::REQUESTED, amount_cents: 8000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAPER_CHECK, tunecore_processed_at: Date.new(2019, 1, 25)) }
  let!(:payout_transfer4) { create(:payout_transfer, tunecore_status: PayoutTransfer::APPROVED, provider_status: PayoutTransfer::COMPLETED, amount_cents: 10000, payout_provider: payout_provider, withdraw_method: PayoutTransfer::PAPER_CHECK, tunecore_processed_at: Date.new(2019, 1, 26)) }
  let!(:payout_transfer5) { create(:payout_transfer, tunecore_status: PayoutTransfer::REJECTED, provider_status: PayoutTransfer::NONE, amount_cents: 10000, payout_provider: payout_provider2, withdraw_method: PayoutTransfer::PAPER_CHECK, currency: "GBP", tunecore_processed_at: Date.new(2019, 1, 27)) }


  describe "#build" do
    context "amount filters" do
      it "loads in payout transfers that match the conditions of the > filter" do
        query_params = {
          filter_amount:     "95.00",
          filter_type:       ">"
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(2)
        expect(results.include?(payout_transfer4)).to eq(true)
      end

      it "loads in payout transfers that match the conditions of the = filter" do
        query_params = {
          filter_amount: "50.00",
          filter_type:   "="
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(1)
        expect(results.first).to eq(payout_transfer2)
      end

      it "loads in payout transfers that match the conditions of the < filter" do
        query_params = {
          filter_amount: "45.00",
          filter_type:   "<"
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(1)
        expect(results.first).to eq(payout_transfer1)
      end
    end

    context "date filter" do
      it "loads in payout transfers that fall inside the date range" do
        query_params = {
          start_date: "2019-01-25",
          end_date: "2019-01-27"
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(3)
        expect(results.include?(payout_transfer3)).to be(true)
        expect(results.include?(payout_transfer4)).to be(true)
        expect(results.include?(payout_transfer5)).to be(true)
      end

      it "loads in payout transfers that were created after a set start date" do
        query_params = {
          start_date: "2019-01-26"
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(2)
        expect(results.include?(payout_transfer4)).to be(true)
        expect(results.include?(payout_transfer5)).to be(true)
      end

      it "loads in payout transfers that were created before a end date" do
        query_params = {
          end_date: "2019-01-27"
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(3)
        expect(results.include?(payout_transfer3)).to be(true)
        expect(results.include?(payout_transfer4)).to be(true)
        expect(results.include?(payout_transfer5)).to be(true)
      end
    end

    context "rollback filter" do
      it "only returns transfers that have been marked as rolled back" do
        payout_transfer5.update_attribute(:rolled_back, true)
        query_params = {
          rollback: true
        }

        results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

        expect(results.size).to eq(1)
        expect(results.include?(payout_transfer5)).to be_truthy
      end
    end

    it "loads in all payout transfers with submitted status with no other filters enabled" do
      query_params = {}

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)
      expect(results.size).to eq(5)
    end

    it "loads in payout transfers that match the conditions of the provider status filter" do
      query_params = {
          provider_status:   PayoutTransfer::COMPLETED,
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.first).to eq(payout_transfer4)
    end

    it "loads in payout transfers that match the conditions of the tunecore status filter" do
      query_params = {
        tunecore_status:   PayoutTransfer::REJECTED,
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.first).to eq(payout_transfer5)
    end

    it "loads in payout transfers that match the conditions of the withdrawal method filter" do
      query_params = {
        withdrawal_method: PayoutTransfer::PAYPAL
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.first).to eq(payout_transfer2)
    end

    it "loads in payout transfers that match the conditions of the name filter" do
      query_params = {
        person_name: "gurley"
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.first).to eq(payout_transfer5)
    end

    it "loads in payout transfers that match the specified currency" do
      query_params = {
        currency: "GBP"
      }

      results = Payoneer::PayoutTransferQueryBuilder.build(query_params)

      expect(results.size).to eq(1)
      expect(results.first).to eq(payout_transfer5)
    end
  end
end
