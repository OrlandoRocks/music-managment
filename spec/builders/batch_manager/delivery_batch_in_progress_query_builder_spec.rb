require "rails_helper"

describe BatchManager::DeliveryBatchInProgressQueryBuilder do
  before do
    @delivery_batch_complete = DeliveryBatch.create(
      store_id: 78,
      joined_count: 100,
      processed_count: 100,
      active: false,
      currently_closing: false,
      batch_complete_sent_at: 4.minutes.ago
    )

    100.times { create(:delivery_batch_item, status: "sent", delivery_batch: @delivery_batch_complete) }

    @delivery_batch_in_progress = DeliveryBatch.create(
      store_id: 26,
      joined_count: 85,
      processed_count: 75,
      active: true,
      currently_closing: false
    )

    75.times { create(:delivery_batch_item, status: "sent", delivery_batch: @delivery_batch_in_progress) }
    10.times { create(:delivery_batch_item, status: "present", delivery_batch: @delivery_batch_in_progress) }
  end

  it "gets all the delivery batches that have not sent a batch complete file" do
    expect(BatchManager::DeliveryBatchInProgressQueryBuilder.build.first.id).to eq(@delivery_batch_in_progress.id)
  end
end
