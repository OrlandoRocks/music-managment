require "rails_helper"

describe BatchManager::DeliveryBatchIncompleteQueryBuilder do
  it "gets all the delivery batches that have not sent a batch complete file" do
    delivery_batch_complete = DeliveryBatch.create(
      store_id: 78,
      active: false,
      currently_closing: false
    )

    create_list(:delivery_batch_item, 100, status: "sent", delivery_batch: delivery_batch_complete)

    delivery_batch_incomplete = DeliveryBatch.create(
      store_id: 26,
      joined_count: 100,
      processed_count: 75,
      active: false,
      batch_complete_sent_at: nil,
      currently_closing: false
    )

    create_list(:delivery_batch_item, 75, status: "sent", delivery_batch: delivery_batch_incomplete)
    create_list(:delivery_batch_item, 15, status: "present", delivery_batch: delivery_batch_incomplete)

    expect(BatchManager::DeliveryBatchIncompleteQueryBuilder.build.map(&:id)).to match_array([delivery_batch_incomplete.id])
  end

  it "gets batches that don't have a batch complete file but are stale because of stuck distribution workers" do
    delivery_batch_incomplete = DeliveryBatch.create(
      store_id: 26,
      joined_count: 100,
      processed_count: 99,
      active: false,
      batch_complete_sent_at: nil,
      currently_closing: false
    )

    create_list(:delivery_batch_item, 99, status: "sent", delivery_batch: delivery_batch_incomplete, created_at: 3.days.ago, updated_at: 3.days.ago)
    create(:delivery_batch_item, status: "present", delivery_batch: delivery_batch_incomplete, created_at: 2.days.ago, updated_at: 2.days.ago)

    delivery_batch_complete = DeliveryBatch.create(
      store_id: 78,
      active: false,
      currently_closing: false
    )

    create_list(:delivery_batch_item, 100, status: "sent", delivery_batch: delivery_batch_complete)

    expect(BatchManager::DeliveryBatchIncompleteQueryBuilder.build.map(&:id)).to match_array([delivery_batch_incomplete.id])
  end
end
