require 'rails_helper'

describe Distribution::OrphanedSalepointQueryBuilder do
  describe "#salepoints" do
    it "returns paid salepoints for an album that do not have at least one corresponding distribution" do
      album = create(:album, :purchaseable, :paid)
      album.salepoints.update_all(payment_applied: true)
      expect(Distribution::OrphanedSalepointQueryBuilder.build(album)).not_to be_empty
    end

    it "should not return salepoints for a store without any store delivery config" do
      album = create(:album, :purchaseable, :paid, abbreviation: ["em"])
      album.salepoints.update_all(payment_applied: true)
      expect(Distribution::OrphanedSalepointQueryBuilder.build(album)).to be_empty
    end
  end
end
