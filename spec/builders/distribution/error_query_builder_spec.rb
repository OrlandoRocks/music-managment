require "rails_helper"

describe Distribution::ErrorQueryBuilder do
  let!(:erred_distro) { create(:distribution, :with_salepoint) }
  let!(:transition)   { create(:transition, state_machine: erred_distro, message: "Connection timed out") }
  let(:salepoint)     { erred_distro.salepoints.first }
  let(:date)          { DateTime.now - 23.hours }

  before do
    erred_distro.update(state: "error", updated_at: date)
  end

  before(:each) do
    Timecop.freeze
  end

  after(:each) do
    Timecop.return
  end

  describe "#filter_by_store" do
    let(:expected_key)             { salepoint.store_id }
    let(:expected_distribution_id) { erred_distro.id }
    let(:distribution_error)       { Distribution::ErrorQueryBuilder.build(filter_type: "store") }
    let(:distribution_error_key)   { distribution_error.first.first }

    it "should filter distribution errors by store_id" do
      expect(distribution_error_key).to eq expected_key
    end

    context "when a date range is passed in" do
      let!(:second_erred_distro) { create(:distribution, :with_salepoint) }
      let!(:second_transition)   { create(:transition, state_machine: second_erred_distro) }
      let(:start_date)           { (Date.today - 4).to_time }
      let(:distribution_error)   { Distribution::ErrorQueryBuilder.build(start_date: start_date, filter_type: "store") }
      let(:distribution_ids)     { distribution_error.first.last.map(&:distribution_id) }

      it "returns only the erred distributions between the date ranges" do
        expect(distribution_ids).to eq [expected_distribution_id]
        expect(distribution_ids).not_to include second_erred_distro.id
      end
    end
  end

  describe "#filter_by_error" do
    let(:distribution_error)     { Distribution::ErrorQueryBuilder.build(filter_type: "error") }
    let(:distribution_error_key) { distribution_error.first.first }

    it "should filter distribution errors by error" do
      expect(distribution_error_key).to eq "Connection timed out"
    end
  end

  describe "#filter_by_release" do
    let(:expected_key)           { salepoint.salepointable.name }
    let(:distribution_error)     { Distribution::ErrorQueryBuilder.build(filter_type: "release") }
    let(:distribution_error_key) { distribution_error.first.first }

    it "should filter distribution errors by release" do
      expect(distribution_error_key).to eq expected_key
    end
  end

  describe "#store_map" do
    let(:store_map) { Distribution::ErrorQueryBuilder.new.store_map }

    context "when a store is active" do
      it "should return the store name associated to the store_id" do
        itunes_us_id = Store.find_by(abbrev: "us").id
        expect(store_map[itunes_us_id]).to eq "iTunes U.S."
      end
    end

    context "when a store is inactive" do
      context "and is not in use" do
        it "should return nil" do
          itunes_uk_id = Store.find_by(abbrev: "uk").id
          expect(store_map[itunes_uk_id]).to eq nil
        end
      end

      context "and is in use" do
        it "should return the store name associated to the store_id" do
          streaming_store_id = Store.find_by(abbrev: "st").id
          expect(store_map[streaming_store_id]).to eq "Streaming"
        end
      end
    end
  end
end
