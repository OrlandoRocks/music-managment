require "rails_helper"

describe SongData::CompletionReportBuilder do
  let(:person)        { create(:person) }
  let(:album)         { create(:album, person_id: person.id) }
  let(:songwriter_id) { SongRole.where(role_type: "songwriter").first.id}
  let(:song_artists)  { [double(associated_to: "Album", credit: "primary_artist", role_ids: "#{songwriter_id}", name: "artist name")] }
  let!(:song)         { create(:song, album_id: album.id)}
  let(:song_data)     { SongDataBuilder.build(person.id, song)}

  describe ".build" do
    before(:each) do
      allow(song_data).to receive(:name?).and_return(true)
      allow(song_data).to receive(:asset_url?).and_return(true)
      allow(song_data).to receive(:artists).and_return(song_artists)
    end
    it "builds a completion report" do
      completion_report = SongData::CompletionReportBuilder.build(song_data)
      expect(completion_report.completed_required_fields?).to be true
      expect(completion_report.completed_suggested_fields?).to be true
    end
  end

  describe ".build_for_album" do
    it "builds a completion report for each of the album's songs" do
      reports = SongData::CompletionReportBuilder.build_for_album(album.id, person.id)
      expect(reports.length).to eq(1)
      expect(reports.first.class).to eq(SongData::CompletionReport)
    end
  end
end
