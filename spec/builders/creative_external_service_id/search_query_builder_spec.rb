require "rails_helper"

describe CreativeExternalServiceId::SearchQueryBuilder do
  let(:builder) do
    CreativeExternalServiceId::SearchQueryBuilder
  end

  let(:create_composers) do
    album = create(:album)

    @creative1 = create(:creative,
      creativeable: album,
      role: 'primary_artist',
      artist: create(:artist, name: "Alfaba")
    )

    @creative2 = create(:creative,
      creativeable: album,
      role: 'featuring',
      artist: create(:artist, name: "Betty")
    )

    @creative3 = create(:creative,
      creativeable: album,
      role: 'composer',
      artist: create(:artist, name: "Gemma")
    )
  end

  describe ".build" do
    before(:each) do
      create_composers
    end

    let(:results) do
      builder.build(params)
    end

    context "when appropriate params are passed in" do
      context "when searching by artist_name" do
        context "when searching with a full name (case-insensitive)" do
          let(:params) do
            {
              search_text: "alfaba"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results).to eq([@creative1])
          end
        end

        context "when searching with a partial name" do
          let(:params) do
            {
              search_text: "ett"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results).to eq([@creative2])
          end
        end

        context "when passing in an initial_query" do
          let(:params) do
            {
              search_text: "a",
              initial_query: Creative.where(role: ["featuring", "composer"])
            }
          end

          it "only returns results from within the passed-in initial_query" do
            expect(results.length).to eq(1)

            expect(results).to eq([@creative3])
          end
        end
      end
    end

    context "when appropriate params are not passed in" do
      context "when there is no search text" do
        let(:params) do
          {}
        end

        it "returns all creatives with artists" do
          expect(results.length).to eq(Creative.joins(:artist).count)

          expect(results).to include(@creative1, @creative2, @creative3)
        end
      end
    end
  end
end
