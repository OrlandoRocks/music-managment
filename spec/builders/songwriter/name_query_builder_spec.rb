require 'rails_helper'

describe Songwriter::NameQueryBuilder do
  let(:person) { create(:person) }

  before do
    album = create(:album, person: person)
    song = create(:song, album: album)
    artist1 = create(:artist, name: "Charlie")
    artist2 = create(:artist, name: "Frank")
    creative1 = create(:creative, creativeable: album, artist: artist1)
    creative2 = create(:creative, creativeable: album, artist: artist2)
    song_role = SongRole.find(SongRole::SONGWRITER_ROLE_ID)
    create(:creative_song_role, song: song, song_role: song_role, creative: creative1)
    create(:creative_song_role, song: song, song_role: song_role, creative: creative2)
  end

  it "builds a query for all of a persons songwriter names" do

    songwriter_names = Songwriter::NameQueryBuilder.build(person.id).pluck(:name)
    expect(songwriter_names).to include("Charlie", "Frank")
  end
end
