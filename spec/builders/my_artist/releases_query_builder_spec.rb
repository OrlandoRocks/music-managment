require 'rails_helper'

describe MyArtist::ReleasesQueryBuilder do
  describe ".build" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }
    let(:artist1) { create(:artist, name: "Charlie") }
    let(:artist2) { create(:artist, name: "Frank") }
    let(:artist3) { create(:artist, name: "JT") }

    let!(:album1) do
      create(:album, person: person1, creatives: [
        { "role" => "primary_artist", "name" => artist1.name },
        { "role" => "featuring", "name" => artist2.name },
      ])
    end

    let!(:album2) do
      create(:album, person: person2, creatives: [
        { "role" => "primary_artist", "name" => artist1.name },
        { "role" => "featuring", "name" => artist3.name },
      ])
    end

    it "builds the my artists query" do
      releases = MyArtist::ReleasesQueryBuilder.build(person1.id, artist1.id)
      expect(releases.first.name).to eq(album1.name)
    end
  end
end
