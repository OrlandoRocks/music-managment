require 'rails_helper'

describe MyArtist::NameQueryBuilder do
  let(:person) { create(:person) }

  before do
    album = create(:album, person: person)
    song = create(:song, album: album)
    artist1 = create(:artist, name: "Charlie")
    artist2 = create(:artist, name: "Frank")
    artist3 = create(:artist, name: "Esa")
    artist4 = create(:artist, name: "Andrew")
    creative1 = create(:creative, creativeable: album, artist: artist1)
    creative2 = create(:creative, creativeable: album, artist: artist2)
    creative3 = create(:creative, creativeable: album, artist: artist3)
    creative4 = create(:creative, creativeable: album, artist: artist4)

    acoustic_guitarist = SongRole.find_by(role_type: "acoustic guitar")
    baritone_saxiphonist = SongRole.find_by(role_type: "baritone saxophone")
    songwriter = SongRole.find(SongRole::SONGWRITER_ROLE_ID)

    create(:creative_song_role, song: song, song_role: songwriter, creative: creative1)
    create(:creative_song_role, song: song, song_role: songwriter, creative: creative2)
    create(:creative_song_role, song: song, song_role: acoustic_guitarist, creative: creative2)
    create(:creative_song_role, song: song, song_role: songwriter, creative: creative3)
    create(:creative_song_role, song: song, song_role: baritone_saxiphonist, creative: creative4)
  end

  it "builds a query for all of a persons artist names" do
    artist_names = MyArtist::NameQueryBuilder.build(person.id).pluck("artists.name")
    expect(artist_names).to include("Andrew", "Frank", "Charlie", "Esa")
  end
end
