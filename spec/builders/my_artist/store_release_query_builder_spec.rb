require 'rails_helper'

describe MyArtist::StoreReleaseQueryBuilder do
  let(:person) { create(:person) }
  let(:artist) { create(:artist) }
  let!(:album) do
    create(:album, person: person, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
    ])
  end
  let!(:external_service_id) { create(:external_service_id, linkable: album, identifier: "100001023", service_name: "apple") }

  it "returns all releases for an artist for a person for a specific service" do
    releases = MyArtist::StoreReleaseQueryBuilder.build(person.id, artist.id, "apple")
    expect(releases.first.album_name).to eq(album.name)
  end
end
