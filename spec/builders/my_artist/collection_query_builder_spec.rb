require 'rails_helper'

describe MyArtist::CollectionQueryBuilder do
  describe ".build" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }
    let(:artist1) { create(:artist, name: "Charlie") }
    let(:artist2) { create(:artist, name: "Frank") }
    let(:artist3) { create(:artist, name: "JT") }

    let!(:single1) do
      single = create(:single, person: person1)
      create(:creative, creativeable: single, artist: artist1, role: "primary_artist")
      create(:creative, creativeable: single, artist: artist2, role: "featuring")
      single
    end

    let!(:single2) do
      single = create(:single, person: person2)
      create(:creative, creativeable: single, artist: artist1, role: "primary_artist")
      create(:creative, creativeable: single, artist: artist3, role: "featuring")
      single
    end

    it "builds the my artists query" do
      expect(MyArtist::CollectionQueryBuilder.build.map(&:artist_name)).to include("Charlie", "Frank", "JT")
    end
  end
end
