require "rails_helper"

describe AlbumApp::CreativesBuilder do

  describe ".build" do
    it "returns the creatives on the album" do
      album     = create(:album)
      creative  = create(:creative, creativeable: album)
      builder   = AlbumApp::CreativesBuilder.build(album.id)

      expect(builder.count(:all)).to eq 2
    end
  end

end
