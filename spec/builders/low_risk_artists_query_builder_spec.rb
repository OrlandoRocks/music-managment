require "rails_helper"

describe LowRiskArtistsQueryBuilder do
  describe ".build" do
    it "returns empty if no albums are needing review" do
      expect(LowRiskArtistsQueryBuilder.build.to_a).to be_empty
    end

    it "returns an album that needs review and doesn't have blacklisted artist" do
      album = create(:album, :paid, :with_songs, legal_review_state: "NEEDS REVIEW")
      create(:creative, creativeable: album)
      create(:low_risk_account, person: album.person)

      expect(LowRiskArtistsQueryBuilder.build.first.album_title).to eq album.name
    end

    it "doesn't return an album that needs review but has a blacklisted artist" do
      album  = create(:album, :paid, :with_songs, legal_review_state: "NEEDS REVIEW")
      artist = create(:artist, :blacklisted)
      create(:creative, creativeable: album, artist: artist)
      create(:low_risk_account, person: album.person)

      expect(LowRiskArtistsQueryBuilder.build).to be_empty
    end

    it "doesn't return an album that needs review but features a blacklisted artist on one track" do
      album = create(:album, :paid, :with_songs, legal_review_state: "NEEDS REVIEW")
      song  = create(:song, album: album )
      create(:creative, creativeable: album)
      create(:low_risk_account, person: album.person)
      artist = create(:artist, :blacklisted)
      create(:creative, creativeable: song, artist: artist)

      expect(LowRiskArtistsQueryBuilder.build).to be_empty
    end
  end
end
