require "rails_helper"

describe Composer::SearchQueryBuilder do
  let(:builder) do
    Composer::SearchQueryBuilder
  end

  let(:build_composers) do
    @composer1 = create(:composer,
      :with_pub_admin_purchase,
      :with_lod,
      first_name: "Alphonse",
      middle_name: "Allison",
      last_name: "Alexander",
      email: "Alphonse.Allison.Alexander@e.mail.com"
    )

    @composer2 = create(:composer,
      :with_pub_admin_purchase,
      :with_lod,
      first_name: "Bethany",
      middle_name: "Bartholomew",
      last_name: "Bellows",
      email: "Bethany-Bartholomew-Bellows@e-mail.net"
    )

    @composer3 = create(:composer,
      :with_pub_admin_purchase,
      :with_lod,
      first_name: "Gam",
      middle_name: "Gabriel",
      last_name: "Gareth",
      email: "GamGabrielGareth@email.web"
    )
  end

  describe ".build" do
    before(:each) do
      build_composers
    end

    let(:results) do
      builder.build(params)
    end

    describe "when appropriate params are passed in" do
      describe "when searching by name" do
        describe "when searching with a first name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Alphonse"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer1.id)
          end
        end

        describe "when searching with a middle name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Bartholomew"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer2.id)
          end
        end

        describe "when searching with a last name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Gareth"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer3.id)
          end
        end

        describe "when searching with a first and last name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Bethany Bellows"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer2.id)
          end
        end

        describe "when searching with a first and middle name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Alphonse Allison"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer1.id)
          end
        end

        describe "when searching with a middle and last name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Bartholomew Bellows"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer2.id)
          end
        end

        describe "when searching with a full name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "Gam Gabriel Gareth",
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer3.id)
          end
        end

        describe "when searching with a partial name" do
          let(:params) do
            {
              search_field: "name",
              search_text: "el"
            }
          end

          it "returns every composer record with names containing the characters" do
            expect(results.length).to eq(2)

            expect(results.map(&:id)).to include(@composer2.id, @composer3.id)
          end
        end
      end

      describe "when searching by an email" do
        describe "when searching with a email prefix" do
          let(:params) do
            {
              search_field: "email",
              search_text: ".com"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer1.id)
          end
        end

        describe "when searching with a email host" do
          let(:params) do
            {
              search_field: "email",
              search_text: "e-mail"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer2.id)
          end
        end

        describe "when searching with an email handle" do
          let(:params) do
            {
              search_field: "email",
              search_text: "GamGabrielGareth"
            }
          end

          it "it returns the appropriate composer record" do
            expect(results.length).to eq(1)

            expect(results.first.id).to eq(@composer3.id)
          end
        end
      end

      describe "when searching by a person id" do
        let(:params) do
          {
            search_field: "person_id",
            search_text: @composer3.person_id
          }
        end

        it "it returns the appropriate composer record" do
          expect(results.length).to eq(1)

          expect(results.first.id).to eq(@composer3.id)
        end
      end

      describe "when searching by an id" do
        let(:params) do
          {
            search_field: "id",
            search_text: @composer2.id
          }
        end

        it "it returns the appropriate composer record" do
          expect(results.length).to eq(1)

          expect(results.first.id).to eq(@composer2.id)
        end
      end

      describe "when the search_text matches nothing" do
        let(:params) do
          {
            search_field: "name",
            search_text: "I match NOTHING"
          }
        end

        it "it returns an empty array" do
          expect(results).to eq([])
        end
      end
    end

    describe "when inappropriate params are passed in" do
      describe "when the search field is not an accepted field" do
        let(:params) do
          {
            search_field: "mystery_column",
            search_text: "Hello all!"
          }
        end

        it "returns all composer records with a person, lods, and a paid related_purchase" do
          expect(results.length).to eq(3)

          expect(results.map(&:id)).to include(@composer1.id, @composer2.id, @composer3.id)
        end
      end
    end
  end

  describe "::SEARCH_FIELD_OPTIONS" do
    it "returns a hash" do
      expect(builder::SEARCH_FIELD_OPTIONS).to be_a(Hash)
    end
  end
end
