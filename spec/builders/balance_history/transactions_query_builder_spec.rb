require "rails_helper"

describe BalanceHistory::TransactionsQueryBuilder do
  describe ".build" do
    let(:person) { create(:person) }
    let(:payout_transfer) do
      create(:person_transaction,
             person: person,
             target: create(:payout_transfer, person: person, amount_cents: 50000))
    end

    let(:person_transaction) do
      create(:person_transaction,
             person: person,
             target: create(:purchase, person: person))
    end

    it "returns person transactions" do
      transactions = BalanceHistory::TransactionsQueryBuilder.build(person: person)
      expect(transactions).to include payout_transfer, person_transaction
    end

    context "when filtering between two dates" do
      let(:start_date) { "2018-07-01" }
      let(:end_date)   { "2018-07-10" }
      let(:created_at) { "2018-07-05" }

      it "returns transactions within the searched dates" do
        payout_transfer.update(created_at: created_at)
        transactions = BalanceHistory::TransactionsQueryBuilder.build(
          person: person, start_date: start_date, end_date: end_date)

        expect(transactions).to include payout_transfer
        expect(transactions).not_to include person_transaction
      end
    end

    context "when filtering by target type" do
      it "returns transactions within the searched dates" do
        transactions = BalanceHistory::TransactionsQueryBuilder.build(
          person: person, transaction_types: ["payout_transfers"])

        expect(transactions).to include payout_transfer
        expect(transactions).not_to include person_transaction
      end
    end

    context "when filtering by transaction_id" do
      it "returns the correct transactions" do
        transactions = BalanceHistory::TransactionsQueryBuilder.build(
          person: person, txn_ids: payout_transfer.id)

        expect(transactions).to include payout_transfer
        expect(transactions).not_to include person_transaction
      end
    end
  end
end

