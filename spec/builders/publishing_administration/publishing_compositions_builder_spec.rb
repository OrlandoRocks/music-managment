require "rails_helper"

describe PublishingAdministration::PublishingCompositionsBuilder do
  describe ".build" do
    let!(:person)                 { create(:person) }
    let!(:account)                { create(:account) }
    let!(:publishing_composer)    { create(:publishing_composer, person: person, account: account ) }
    let!(:finalized_album)        { create(:album, :finalized, :approved, person: person) }
    let!(:deleted_album)          { create(:album, :is_deleted, person: person) }
    let!(:split_composition)      { create(:publishing_composition, name: "Abcde", is_unallocated_sum: false, account: account) }
    let!(:composition_sans_split) { create(:publishing_composition, name: "Bcde", is_unallocated_sum: false, account: account) }
    let!(:non_distributed_comp)   { create(:publishing_composition, name: "Cde", is_unallocated_sum: false, account: account) }
    let!(:first_finalized_song)   { create(:song, album: finalized_album, publishing_composition: split_composition) }
    let!(:second_finalized_song)  { create(:song, album: finalized_album, publishing_composition: composition_sans_split) }
    let!(:deleted_song)           { create(:song, album: deleted_album, publishing_composition: non_distributed_comp) }
    let!(:first_cowriter)         { create(:publishing_composer) }
    let!(:second_cowriter)        { create(:publishing_composer) }
    let!(:third_cowriter)        { create(:publishing_composer) }
    let!(:publishing_composition_split1) do
      create(
        :publishing_composition_split,
        percent:      25,
        right_to_collect:       false,
        publishing_composer:     first_cowriter,
        publishing_composition:  split_composition
      )
    end
    let!(:publishing_composition_split2) do
      create(
        :publishing_composition_split,
        percent:      25,
        right_to_collect:       false,
        publishing_composer:     second_cowriter,
        publishing_composition:  split_composition
      )
    end
    let!(:publishing_composition_split3) do
      create(
        :publishing_composition_split,
        percent:      50,
        right_to_collect:        true,
        publishing_composer:     third_cowriter,
        publishing_composition:  split_composition
      )
    end

    it "builds the query for publishing_compositions" do
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})

      expect(publishing_compositions.length).to eq(2)
      comp_with_split = publishing_compositions.find { |c| c == split_composition }
      expect(comp_with_split.cowriter_share.to_i).to eq(50)
      expect(comp_with_split.composer_share.to_i).to eq(50)
      expect(comp_with_split.cowriter_split_ids).to eq "#{publishing_composition_split1.id},#{publishing_composition_split2.id}"
    end

    it "includes isrc columns from songs" do
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})
      expect(publishing_compositions.length).to eq(2)

      publishing_compositions.each do |publishing_composition|
        expect(publishing_composition).to respond_to(:isrc)
        expect(publishing_composition).to respond_to(:tunecore_isrc)
        expect(publishing_composition).to respond_to(:optional_isrc)
      end
    end

    it "includes publishing_compositions associated to non_tunecore_songs" do
      ntc_comp  = create(:publishing_composition, account: account)
      ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
      create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition_id: ntc_comp.id)

      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})
      expect(publishing_compositions.length).to eq 3
      expect(publishing_compositions.map(&:id).include?(ntc_comp.id)).to be true
    end

    it "includes performing artist, release date and record_label of non_tunecore_songs" do
      ntc_comp  = create(:publishing_composition, name: 'Ntc Comp', account: account)
      ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer, record_label: "record label")
      create(:non_tunecore_song, {
        non_tunecore_album: ntc_album,
        artist: create(:artist),
        publishing_composition_id: ntc_comp.id,
        release_date: Time.current,
      })

      results = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})
      ntc_publishing_composition = results.find { |r| r.id == ntc_comp.id  }

      expect(ntc_publishing_composition).to respond_to(:performing_artist)
      expect(ntc_publishing_composition).to respond_to(:release_date)
      expect(ntc_publishing_composition).to respond_to(:record_label)
    end

    it "excludes compositions that have beed not been distributed" do
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})

      expect(publishing_compositions).to include split_composition
      expect(publishing_compositions).not_to include non_distributed_comp
    end

    it "includes hidden publishing_compositions" do
      split_composition.update(state: 'hidden')
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, hidden: true})

      expect(publishing_compositions).to include split_composition
    end

    context "when passed in search text" do
      context "when the text does not match the name of any publishing_compositions" do
        it "does not return any compositions" do
          publishing_compositions_name = "Find Me"

          split_composition.update_columns(name: "No")
          composition_sans_split.update_columns(name: "I don't think so")
          non_distributed_comp.update_columns(name: "Nuh uh")

          publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, publishing_composition_name: publishing_compositions_name})

          expect(publishing_compositions.length).to eq(0)
        end
      end

      context "when the text matches the name of certain publishing_compositions" do
        it "returns those publishing_compositions" do
          publishing_compositions_name = "Find Me"

          split_composition.update_columns(name: "Find Me in the front")
          composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, publishing_composition_name: publishing_compositions_name})

          expect(publishing_compositions.length).to eq(2)
          expect(publishing_compositions.map(&:id).include?(split_composition.id)).to be_truthy
          expect(publishing_compositions.map(&:id).include?(composition_sans_split.id)).to be_truthy
        end

        it "returns those publishing_compositions with hidden state" do
          publishing_compositions_name = "Find Me"

          split_composition.update_columns(name: "Find Me in the front", state: 'hidden')
          composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, publishing_composition_name: publishing_compositions_name, hidden: true})

          expect(publishing_compositions.length).to eq(1)
          expect(publishing_compositions.map(&:id).include?(split_composition.id)).to be_truthy
          expect(publishing_compositions.map(&:id).include?(composition_sans_split.id)).to be_falsy
        end
      end
    end

    it "should return a list of cowriter split ids" do
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})

      cowriter_split_ids = publishing_compositions.first.cowriter_split_ids.split(",").map(&:to_i)

      expect(cowriter_split_ids).to match_array([publishing_composition_split1.id, publishing_composition_split2.id])
    end

    context "when the publishing_composition does not have an unknown cowriting publishing_composer" do
      it "the unknown_split_percent should be nil " do
        publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})

        expect(publishing_compositions.first.unknown_split_percent).to be_nil
      end
    end

    context "when the publishing_composition has an unknown cowriting publishing_composer" do
      it "should return percent of the unknown cowriter" do
        unknown_cowriter = create(:publishing_composer, :cowriter, :unknown)
        unknown_cowriter_split = split_composition
          .publishing_composition_splits
          .create(publishing_composer: unknown_cowriter, percent: 10.to_d)

        publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id})

        expect(publishing_compositions.first.unknown_split_percent).to eq unknown_cowriter_split.percent
      end
    end

    it "returns the publishing_compositions verified_date" do
      date = Date.current
      split_composition.update(verified_date: date)

      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build(publishing_composer_id: publishing_composer.id)
      expect(publishing_compositions.first.verified_date.to_date).to eq date.to_date
    end

    it "returns compositions sorted alphabetically by name" do
      order_by = "composition_name"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, order_by: order_by})

      expect(publishing_compositions.length).to eq(2)
      expect(publishing_compositions.first.name).to eq "Abcde"
      expect(publishing_compositions.second.name).to eq "Bcde"
    end

    it "returns compositions sorted anti-alphabetically by name" do
      order_by = "composition_name"
      order_by_desc = true
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, order_by: order_by, order_by_desc: order_by_desc})

      expect(publishing_compositions.length).to eq(2)
      expect(publishing_compositions.first.name).to eq "Bcde"
      expect(publishing_compositions.second.name).to eq "Abcde"
    end

    it "returns compositions sorted by total_share Asc" do
      order_by = "total_share"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, order_by: order_by})

      # Bcde has share% = 0
      # Abcde has share% = 75
      expect(publishing_compositions.length).to eq(2)
      expect(publishing_compositions.first.name).to eq "Bcde"
      expect(publishing_compositions.second.name).to eq "Abcde"
    end

    it "returns compositions sorted by total_share desc" do
      order_by = "total_share"
      order_by_desc = true
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, order_by: order_by, order_by_desc: order_by_desc})

      # Abcde has share% = 75
      # Bcde has share% = 0
      expect(publishing_compositions.length).to eq(2)
      expect(publishing_compositions.first.name).to eq "Abcde"
      expect(publishing_compositions.second.name).to eq "Bcde"
    end

    it "returns compositions searched by composition name starting with search term" do
      search_by = "composition_name"
      search_term = "bcde"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(1)
      expect(publishing_compositions.first.name).to eq "Bcde"
    end

    it "returns compositions searched by composition name containing search term" do
      search_by = "composition_name_contains"
      search_term = "bcde"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(2)
      expect(publishing_compositions.first.name).to eq "Abcde"
      expect(publishing_compositions.second.name).to eq "Bcde"
    end

    it "returns compositions searched by total share % > search term" do
      search_by = "total_share_gt"
      search_term = "0"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(1)
      expect(publishing_compositions.first.name).to eq "Abcde"
    end

    it "returns compositions searched by total share % < search term" do
      search_by = "total_share_lt"
      search_term = "100"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(1)
      expect(publishing_compositions.first.name).to eq "Bcde"
    end

    it "returns compositions searched by total share % == search term" do
      search_by = "total_share_eq"
      search_term = "100"
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(1)
      expect(publishing_compositions.first.name).to eq "Abcde"
    end

    it "tests if is_unallocated_sum composition gets selected" do
      is_unallocated_sum_comp  = create(:publishing_composition, name: 'Ntc Comp', account: account, is_unallocated_sum: true)

      search_by = "composition_id"
      search_term = is_unallocated_sum_comp.id
      publishing_compositions = PublishingAdministration::PublishingCompositionsBuilder.build({publishing_composer_id: publishing_composer.id, search_by: search_by, search_term: search_term})

      expect(publishing_compositions.length).to eq(0)
    end
  end
end
