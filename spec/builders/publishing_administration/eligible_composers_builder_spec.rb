require "rails_helper"

describe PublishingAdministration::EligibleComposersBuilder do
  describe ".composers" do
    context "when composers accounts do not have a provider_account_id" do
      it "returns the elgibile composers" do
        composer_list = create_list(:composer, 2, :with_pub_admin_purchase, provider_identifier: "123456789")

        composers = PublishingAdministration::EligibleComposersBuilder.composers

        expect(composer_list).to eq composers
      end
    end

    context "when composers do not have a provider_identifier" do
      it "returns the elgibile composers" do
        composer  = create(:composer, :with_pub_admin_purchase)
        composer.account.update(provider_account_id: "123456789")

        composer2 = create(:composer, :with_pub_admin_purchase, provider_identifier: "12345")
        composer2.account.update(provider_account_id: "123456789")

        composers = PublishingAdministration::EligibleComposersBuilder.composers

        expect(composers).to include composer
        expect(composers).not_to include composer2
      end
    end

    context "when the composer has a provider_account_id and a provider_identifier" do
      it "does not return the composer" do
        composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")
        composer.account.update(provider_account_id: "123456789")

        composers = PublishingAdministration::EligibleComposersBuilder.composers
        expect(composers).to be_empty
      end
    end

    context "when the composer has a pro but not a cae number" do
      it "does not return the composer" do
        composer = create(:composer, :with_pub_admin_purchase,
                          performing_rights_organization_id: 1,
                          provider_identifier: "123456789")
        composer.account.update(provider_account_id: "123456789")

        composers = PublishingAdministration::EligibleComposersBuilder.composers
        expect(composers).to be_empty
      end
    end

    context "when the composer has a cae number but not a pro" do
      it "does not return the composer" do
        composer = create(:composer, :with_pub_admin_purchase,
                          cae: "1234567890",
                          provider_identifier: "123456789")
        composer.account.update(provider_account_id: "123456789")

        composers = PublishingAdministration::EligibleComposersBuilder.composers
        expect(composers).to be_empty
      end
    end

    it "returns composer records when the account was created between the desired date range" do
      composer  = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")
      composer2 = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")

      composer.account.update(created_at: "2019-01-02")
      composer2.account.update(created_at: "2019-01-10")

      composers = PublishingAdministration::EligibleComposersBuilder.composers(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(composers).to include composer
      expect(composers).not_to include composer2
    end

    it "returns composer records created between the desired date range" do
      composer  = create(:composer, :with_pub_admin_purchase, created_at: "2019-01-02")
      composer2 = create(:composer, :with_pub_admin_purchase, created_at: "2019-01-10")

      composer.account.update(provider_account_id: "123456789", created_at: "2019-01-02")
      composer2.account.update(provider_account_id: "123456789", created_at: "2019-01-10")

      composers = PublishingAdministration::EligibleComposersBuilder.composers(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(composers).to include composer
      expect(composers).not_to include composer2
    end
  end
end
