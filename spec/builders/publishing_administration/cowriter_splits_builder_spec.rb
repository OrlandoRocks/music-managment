require "rails_helper"

describe PublishingAdministration::CowriterSplitsBuilder do
  describe ".build" do
    let!(:person)              { create(:person) }
    let!(:composer)            { create(:composer, person: person) }
    let!(:album)               { create(:album, person: person) }
    let!(:splited_composition) { create(:composition) }
    let!(:song1)               { create(:song, album: album, composition: splited_composition) }
    let!(:cowriter1)           { create(:cowriter, composer: composer) }
    let!(:cowriter2)           { create(:cowriter, composer: composer) }
    let!(:cowriter3)           { create(:cowriter, :unknown) }
    let!(:publishing_split1) do
      create(
        :publishing_split,
        percent:      25,
        writer:       cowriter1,
        composer:     composer,
        composition:  splited_composition
      )
    end
    let!(:publishing_split2) do
      create(
        :publishing_split,
        percent:      25,
        writer:       cowriter2,
        composer:     composer,
        composition:  splited_composition
      )
    end
    let!(:publishing_split3) do
      create(
        :publishing_split,
        percent:      25,
        writer:       cowriter3,
        composer:     composer,
        composition:  splited_composition
      )
    end

    it "builds the query for cowriters and excludes unknown cowriters" do
      cowriters = PublishingAdministration::CowriterSplitsBuilder.build(composer.id)

      first_cowriter = cowriters.first
      expect(first_cowriter).to respond_to(:id)
      expect(first_cowriter).to respond_to(:first_name)
      expect(first_cowriter).to respond_to(:last_name)
      expect(first_cowriter).to respond_to(:is_unknown)
      expect(first_cowriter).to respond_to(:cowriter_share)


      cowriter_split_ids = cowriters.map(&:id)
      expect(cowriter_split_ids).to include(publishing_split1.id, publishing_split2.id)
      expect(cowriter_split_ids).not_to include(publishing_split3.id)
    end
  end
end
