require "rails_helper"

describe PublishingAdministration::EligibleCompositionsBuilder do
  describe ".compositions" do
    it "returns nothing if the composition does not have associated publishing splits" do
      composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")
      composer.publishing_splits.destroy_all

      compositions = PublishingAdministration::EligibleCompositionsBuilder.compositions
      expect(compositions).to be_empty
    end

    context "when the composition has publishing splits " do
      it "returns eligible compositions that are missing a provider_identifier" do
        composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")

        composition = composer.compositions.first
        composition.recordings << create(:recording)
        composition.publishing_splits.first.update(writer: composer)

        composition2 = create(:composition, provider_identifier: "12345")
        composition2 = create(:composition, :with_recordings, num_of_recordings: 2, provider_identifier: "12345")
        composition2.publishing_splits << create(:publishing_split, writer: composer)

        compositions = PublishingAdministration::EligibleCompositionsBuilder.compositions

        expect(compositions).to include composition
        expect(compositions).not_to include composition2
      end

      it "returns eligble compositions that are missing a recording_code" do
        composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")

        composition = composer.compositions.first
        composition.update(provider_identifier: "123456789")
        composition.publishing_splits.first.update(writer: composer)

        composition2 = create(:composition, :with_recordings, num_of_recordings: 2, provider_identifier: "12345")
        composition2.publishing_splits << create(:publishing_split, writer: composer)

        compositions = PublishingAdministration::EligibleCompositionsBuilder.compositions

        expect(compositions).to include composition
        expect(compositions).not_to include composition2
      end

      it "returns eligible compositions within the desired date range" do
        composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")

        composition = composer.compositions.first
        composition.update(created_at: "2019-01-01", provider_identifier: "123456789")
        composition.publishing_splits.first.update(writer: composer)

        composition2 = create(:composition, :with_recordings, created_at: "2019-01-10", provider_identifier: "12345")
        composition2.publishing_splits << create(:publishing_split, writer: composer)

        compositions = PublishingAdministration::EligibleCompositionsBuilder.compositions(start_date: "2019-01-01", end_date: "2019-01-05")

        expect(compositions).to include composition
        expect(compositions).not_to include composition2
      end
    end
  end
end
