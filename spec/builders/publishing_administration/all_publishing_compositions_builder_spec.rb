require "rails_helper"

describe PublishingAdministration::AllPublishingCompositionsBuilder do
  describe ".build" do
    let!(:person) { create(:person) }
    let!(:account) { create(:account, person: person) }
    let!(:publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:second_publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:finalized_album) { create(:album, :finalized, :approved, person: person) }
    let!(:deleted_album) { create(:album, :is_deleted, person: person) }
    let!(:split_publishing_composition) { create(:publishing_composition, account: account) }
    let!(:publishing_composition_sans_split) { create(:publishing_composition, account: account) }
    let!(:non_distributed_comp) { create(:publishing_composition, account: account) }
    let!(:first_finalized_song) { create(:song, album: finalized_album, publishing_composition: split_publishing_composition) }
    let!(:second_finalized_song) { create(:song, album: finalized_album, publishing_composition: publishing_composition_sans_split) }
    let!(:deleted_song) { create(:song, album: deleted_album, publishing_composition: non_distributed_comp) }
    let!(:first_cowriter) { create(:publishing_composer, :cowriter, account: account) }
    let!(:second_cowriter) { create(:publishing_composer, :cowriter, account: account) }
    let!(:publishing_composition_split1) do
      create(
        :publishing_composition_split,
        :non_collectable,
        percent: 25,
        publishing_composer: first_cowriter,
        publishing_composition: split_publishing_composition,
      )
    end
    let!(:publishing_composition_split2) do
      create(
        :publishing_composition_split,
        :non_collectable,
        percent: 25,
        publishing_composer: second_cowriter,
        publishing_composition: split_publishing_composition
      )
    end
    let(:publishing_composer_ids) do
      [
        publishing_composer.id,
        second_publishing_composer.id
      ]
    end

    it "builds the query for publishing_compositions" do
      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})
      expect(publishing_compositions.length).to eq(2)
      comp_with_split = publishing_compositions.find { |c| c == split_publishing_composition }
      expect(comp_with_split.cowriter_share.to_i).to eq(50)
      expect(comp_with_split.cowriter_split_ids).to eq "#{publishing_composition_split1.id},#{publishing_composition_split2.id}"
    end

    it "includes isrc columns from songs" do
      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})
      expect(publishing_compositions.length).to eq(2)

      publishing_compositions.each do |publishing_composition|
        expect(publishing_composition).to respond_to(:isrc)
        expect(publishing_composition).to respond_to(:tunecore_isrc)
        expect(publishing_composition).to respond_to(:optional_isrc)
      end
    end

    it "includes publishing_compositions associated to non_tunecore_songs" do
      ntc_comp  = create(:publishing_composition, account: account)
      ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
      create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition_id: ntc_comp.id)

      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})
      expect(publishing_compositions.length).to eq 3
      expect(publishing_compositions.map(&:id).include?(ntc_comp.id)).to be true
    end

    it "includes performing artist and release date of non_tunecore_songs" do
      ntc_comp  = create(:publishing_composition, name: 'Ntc Comp', account: account)
      ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
      create(:non_tunecore_song, {
        non_tunecore_album: ntc_album,
        artist: create(:artist),
        publishing_composition_id: ntc_comp.id,
        release_date: Time.current,
      })

      results = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})
      ntc_publishing_composition = results.find { |r| r.id == ntc_comp.id  }

      expect(ntc_publishing_composition).to respond_to(:performing_artist)
      expect(ntc_publishing_composition).to respond_to(:release_date)
    end


    it "excludes publishing_compositions that have beed not been distributed" do
      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})

      expect(publishing_compositions).to include split_publishing_composition
      expect(publishing_compositions).not_to include non_distributed_comp
    end

    it "includes hidden publishing_compositions" do
      split_publishing_composition.update(state: 'hidden')
      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids, hidden: true})

      expect(publishing_compositions).to include split_publishing_composition
    end

    context "when passed in search text" do
      context "when the text does not match the name of any publishing_compositions" do
        it "does not return any publishing_compositions" do
          publishing_compositions_name = "Find Me"

          split_publishing_composition.update_columns(name: "No")
          publishing_composition_sans_split.update_columns(name: "I don't think so")
          non_distributed_comp.update_columns(name: "Nuh uh")

          publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids, publishing_composition_name: publishing_compositions_name})

          expect(publishing_compositions.length).to eq(0)
        end
      end

      context "when the text matches the name of certain publishing_compositions" do
        it "returns those publishing_compositions" do
          publishing_compositions_name = "Find Me"

          split_publishing_composition.update_columns(name: "Find Me in the front")
          publishing_composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids, publishing_composition_name: publishing_compositions_name})

          expect(publishing_compositions.length).to eq(2)
          expect(publishing_compositions.map(&:id).include?(split_publishing_composition.id)).to be_truthy
          expect(publishing_compositions.map(&:id).include?(publishing_composition_sans_split.id)).to be_truthy
        end

        it "returns those publishing_compositions with hidden state" do
          publishing_compositions_name = "Find Me"

          split_publishing_composition.update_columns(name: "Find Me in the front", state: 'hidden')
          publishing_composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids, publishing_composition_name: publishing_compositions_name, hidden: true})

          expect(publishing_compositions.length).to eq(1)
          expect(publishing_compositions.map(&:id).include?(split_publishing_composition.id)).to be_truthy
          expect(publishing_compositions.map(&:id).include?(publishing_composition_sans_split.id)).to be_falsy
        end
      end
    end

    it "should return a list of cowriter split ids" do
      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})

      cowriter_split_ids = publishing_compositions.first.cowriter_split_ids.split(",").map(&:to_i)

      expect(cowriter_split_ids).to match_array([publishing_composition_split1.id, publishing_composition_split2.id])
    end

    context "when the publishing_composition does not have an unknown cowriter" do
      it "the unknown_split_percent should be nil " do
        publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})

        expect(publishing_compositions.first.unknown_split_percent).to be_nil
      end
    end

    context "when the publishing_composition has an unknown cowriter" do
      it "should return percent of the unknown cowriter" do
        unknown_cowriter = create(:publishing_composer, :cowriter, :unknown)
        unknown_cowriter_split = split_publishing_composition
          .publishing_composition_splits
          .create(publishing_composer: unknown_cowriter, percent: 10.to_d)

        publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build({publishing_composer_ids: publishing_composer_ids})

        expect(publishing_compositions.first.unknown_split_percent).to eq unknown_cowriter_split.percent
      end
    end

    it "returns the publishing_compositions verified_date" do
      date = Date.current
      split_publishing_composition.update(verified_date: date)

      publishing_compositions = PublishingAdministration::AllPublishingCompositionsBuilder.build(publishing_composer_ids: publishing_composer_ids)
      expect(publishing_compositions.first.verified_date.to_date).to eq date.to_date
    end
  end
end
