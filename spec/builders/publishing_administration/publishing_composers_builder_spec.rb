require "rails_helper"

describe PublishingAdministration::PublishingComposersBuilder do
  describe ".build" do
    context "only account_id is provided" do
      it "returns all primary publishing_composers belonging to the given account" do
        account = create(:account)
        create(:publishing_composer, account: account)
        create(:publishing_composer, account: account, person: nil)
        create(:publishing_composer, account: account, person: nil, is_primary_composer: false)

        expect(PublishingAdministration::PublishingComposersBuilder.build(account.id).length).to eq 2
      end
    end

    context "both account_id and person_id are provided" do
      it "only returns the person's publishing_composer" do
        person = create(:person)
        account = create(:account)
        create(:publishing_composer, account: account, person: person)
        create(:publishing_composer, account: account)
        create(:publishing_composer, account: account, is_primary_composer: false)

        expect(PublishingAdministration::PublishingComposersBuilder.build(account.id, person.id).length).to eq 1
      end
    end

    it "orders the publishing administrator first" do
      account = create(:account)
      other_publishing_composer = create(:publishing_composer, account: account, person: nil)
      managing_publishing_composer = create(:publishing_composer, account: account, person: account.person)

      expect(PublishingAdministration::PublishingComposersBuilder.build(account.id))
        .to eq [managing_publishing_composer, other_publishing_composer]
    end
  end
end
