require "rails_helper"

describe PublishingAdministration::AllCompositionsBuilder do
  describe ".build" do
    let!(:person)                 { create(:person) }
    let!(:composer)               { create(:composer, person: person) }
    let!(:second_composer)        { create(:composer, person: person) }
    let!(:finalized_album)        { create(:album, :finalized, :approved, person: person) }
    let!(:deleted_album)          { create(:album, :is_deleted, person: person) }
    let!(:split_composition)      { create(:composition) }
    let!(:composition_sans_split) { create(:composition) }
    let!(:non_distributed_comp)   { create(:composition) }
    let!(:first_finalized_song)   { create(:song, album: finalized_album, composition: split_composition) }
    let!(:second_finalized_song)  { create(:song, album: finalized_album, composition: composition_sans_split) }
    let!(:deleted_song)           { create(:song, album: deleted_album, composition: non_distributed_comp) }
    let!(:first_cowriter)         { create(:cowriter, composer: composer) }
    let!(:second_cowriter)        { create(:cowriter, composer: second_composer) }
    let!(:publishing_split1) do
      create(
        :publishing_split,
        percent:      25,
        writer:       first_cowriter,
        composer:     composer,
        composition:  split_composition
      )
    end
    let!(:publishing_split2) do
      create(
        :publishing_split,
        percent:      25,
        writer:       second_cowriter,
        composer:     composer,
        composition:  split_composition
      )
    end
    let(:composer_ids) do
      [
        composer.id,
        second_composer.id
      ]
    end

    it "builds the query for compositions" do
      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})
      expect(compositions.length).to eq(2)
      comp_with_split = compositions.find { |c| c == split_composition }
      expect(comp_with_split.cowriter_share.to_i).to eq(50)
      expect(comp_with_split.cowriter_split_ids).to eq "#{publishing_split1.id},#{publishing_split2.id}"
    end

    it "includes isrc columns from songs" do
      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})
      expect(compositions.length).to eq(2)

      compositions.each do |composition|
        expect(composition).to respond_to(:isrc)
        expect(composition).to respond_to(:tunecore_isrc)
        expect(composition).to respond_to(:optional_isrc)
      end
    end

    it "includes compositions associated to non_tunecore_songs" do
      ntc_comp  = create(:composition)
      ntc_album = create(:non_tunecore_album, composer: composer)
      create(:non_tunecore_song, non_tunecore_album: ntc_album, composition_id: ntc_comp.id)

      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})
      expect(compositions.length).to eq 3
      expect(compositions.map(&:id).include?(ntc_comp.id)).to be true
    end

    it "includes performing artist and release date of non_tunecore_songs" do
      ntc_comp  = create(:composition, name: 'Ntc Comp')
      ntc_album = create(:non_tunecore_album, composer: composer)
      create(:non_tunecore_song, {
        non_tunecore_album: ntc_album,
        artist: create(:artist),
        composition_id: ntc_comp.id,
        release_date: Time.current,
      })

      results = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})
      ntc_composition = results.find { |r| r.id == ntc_comp.id  }

      expect(ntc_composition).to respond_to(:performing_artist)
      expect(ntc_composition).to respond_to(:release_date)
    end


    it "excludes compositions that have beed not been distributed" do
      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})

      expect(compositions).to include split_composition
      expect(compositions).not_to include non_distributed_comp
    end

    it "includes hidden compositions" do
      split_composition.update(state: 'hidden')
      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids, hidden: true})

      expect(compositions).to include split_composition
    end

    context "when passed in search text" do
      context "when the text does not match the name of any compositions" do
        it "does not return any compositions" do
          compositions_name = "Find Me"

          split_composition.update_columns(name: "No")
          composition_sans_split.update_columns(name: "I don't think so")
          non_distributed_comp.update_columns(name: "Nuh uh")

          compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids, composition_name: compositions_name})

          expect(compositions.length).to eq(0)
        end
      end

      context "when the text matches the name of certain compositions" do
        it "returns those compositions" do
          compositions_name = "Find Me"

          split_composition.update_columns(name: "Find Me in the front")
          composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids, composition_name: compositions_name})

          expect(compositions.length).to eq(2)
          expect(compositions.map(&:id).include?(split_composition.id)).to be_truthy
          expect(compositions.map(&:id).include?(composition_sans_split.id)).to be_truthy
        end

        it "returns those compositions with hidden state" do
          compositions_name = "Find Me"

          split_composition.update_columns(name: "Find Me in the front", state: 'hidden')
          composition_sans_split.update_columns(name: "In the back you can fInD mE somewhat")

          compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids, composition_name: compositions_name, hidden: true})

          expect(compositions.length).to eq(1)
          expect(compositions.map(&:id).include?(split_composition.id)).to be_truthy
          expect(compositions.map(&:id).include?(composition_sans_split.id)).to be_falsy
        end
      end
    end

    it "should return a list of cowriter split ids" do
      compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})

      cowriter_split_ids = compositions.first.cowriter_split_ids.split(",").map(&:to_i)

      expect(cowriter_split_ids).to match_array([publishing_split1.id, publishing_split2.id])
    end

    context "when the composition does not have an unknown cowriter" do
      it "the unknown_split_percent should be nil " do
        compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})

        expect(compositions.first.unknown_split_percent).to be_nil
      end
    end

    context "when the composition has an unknown cowriter" do
      it "should return percent of the unknown cowriter" do
        unknown_cowriter = create(:cowriter, :unknown)
        unknown_cowriter_split = split_composition
          .publishing_splits
          .create(writer: unknown_cowriter, composer: composer, percent: 10.to_d)

        compositions = PublishingAdministration::AllCompositionsBuilder.build({composer_ids: composer_ids})

        expect(compositions.first.unknown_split_percent).to eq unknown_cowriter_split.percent
      end
    end

    it "returns the compositions verified_date" do
      date = Date.current
      split_composition.update(verified_date: date)

      compositions = PublishingAdministration::AllCompositionsBuilder.build(composer_ids: composer_ids)
      expect(compositions.first.verified_date.to_date).to eq date.to_date
    end
  end
end
