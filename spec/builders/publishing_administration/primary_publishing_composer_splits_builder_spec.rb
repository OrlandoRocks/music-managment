require "rails_helper"

describe PublishingAdministration::PrimaryPublishingComposerSplitsBuilder do
  describe ".build" do
    let!(:person) { create(:person) }
    let!(:account) { create(:account, person: person) }
    let!(:publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:publishing_composer2) { create(:publishing_composer, person: person, account: account) }
    let!(:publishing_composer3) { create(:publishing_composer, person: person, account: account) }
    let!(:album) { create(:album, person: person) }
    let!(:split_publishing_composition) { create(:publishing_composition, account: account) }
    let!(:song1) { create(:song, album: album, publishing_composition: split_publishing_composition) }
    let!(:other_compostion) { create(:publishing_composition, account: account) }
    let!(:other_song) { create(:song, album: album, publishing_composition: other_compostion) }
    let!(:publishing_composition_split1) do
      create(
        :publishing_composition_split,
        :collectable,
        percent: 25,
        publishing_composer: publishing_composer,
        publishing_composition: split_publishing_composition
      )
    end
    let!(:publishing_composition_split2) do
      create(
        :publishing_composition_split,
        :collectable,
        percent: 25,
        publishing_composer: publishing_composer2,
        publishing_composition: split_publishing_composition
      )
    end
    let!(:publishing_composition_split3) do
      create(
        :publishing_composition_split,
        :collectable,
        percent: 25,
        publishing_composer: publishing_composer3,
        publishing_composition: other_compostion
      )
    end

    it "builds the query for publishing_composers" do
      publishing_composers = PublishingAdministration::PrimaryPublishingComposerSplitsBuilder.build(split_publishing_composition.id)

      first_publishing_composer = publishing_composers.first
      expect(first_publishing_composer).to respond_to(:id)
      expect(first_publishing_composer).to respond_to(:composer_share)

      publishing_composer_split_ids = publishing_composers.map(&:id)
      expect(publishing_composer_split_ids).to include(publishing_composition_split1.id, publishing_composition_split2.id)
      expect(publishing_composer_split_ids).not_to include(publishing_composition_split3.id)
    end
  end
end
