require "rails_helper"

describe PublishingAdministration::EligiblePublishingCompositionsBuilder do
  describe ".publishing_compositions" do
    it "returns nothing if the publishing_composition does not have associated publishing splits" do
      publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")
      publishing_composer.publishing_composition_splits.destroy_all

      publishing_compositions = PublishingAdministration::EligiblePublishingCompositionsBuilder.publishing_compositions
      expect(publishing_compositions).to be_empty
    end

    context "when the publishing_composition has publishing splits " do
      it "returns eligible publishing_compositions that are missing a provider_identifier" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789", account: account)

        publishing_composition = publishing_composer.publishing_compositions.first
        publishing_composition.recordings << create(:recording)
        publishing_composition.publishing_composition_splits.first.update(publishing_composer: publishing_composer)

        publishing_composition2 = create(:publishing_composition, provider_identifier: "12345", account: account)
        publishing_composition2 = create(:publishing_composition, :with_recordings, num_of_recordings: 2, provider_identifier: "12345", account: account)
        publishing_composition2.publishing_composition_splits << create(:publishing_composition_split, publishing_composer: publishing_composer)

        publishing_compositions = PublishingAdministration::EligiblePublishingCompositionsBuilder.publishing_compositions

        expect(publishing_compositions).to include publishing_composition
        expect(publishing_compositions).not_to include publishing_composition2
      end

      it "returns eligble publishing_compositions that are missing a recording_code" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789", account: account)

        publishing_composition = publishing_composer.publishing_compositions.first
        publishing_composition.update(provider_identifier: "123456789")
        publishing_composition.publishing_composition_splits.first.update(publishing_composer: publishing_composer)

        publishing_composition2 = create(:publishing_composition, :with_recordings, num_of_recordings: 2, provider_identifier: "12345", account: account)
        publishing_composition2.publishing_composition_splits << create(:publishing_composition_split, publishing_composer: publishing_composer)

        publishing_compositions = PublishingAdministration::EligiblePublishingCompositionsBuilder.publishing_compositions

        expect(publishing_compositions).to include publishing_composition
        expect(publishing_compositions).not_to include publishing_composition2
      end

      it "returns eligible publishing_compositions within the desired date range" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789", account: account)

        publishing_composition = publishing_composer.publishing_compositions.first
        publishing_composition.update(created_at: "2019-01-01", provider_identifier: "123456789")
        publishing_composition.publishing_composition_splits.first.update(publishing_composer: publishing_composer)

        publishing_composition2 = create(:publishing_composition, :with_recordings, created_at: "2019-01-10", provider_identifier: "12345")
        publishing_composition2.publishing_composition_splits << create(:publishing_composition_split, publishing_composer: publishing_composer)

        publishing_compositions = PublishingAdministration::EligiblePublishingCompositionsBuilder.publishing_compositions(start_date: "2019-01-01", end_date: "2019-01-05")

        expect(publishing_compositions).to include publishing_composition
        expect(publishing_compositions).not_to include publishing_composition2
      end
    end
  end
end
