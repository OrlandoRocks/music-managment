require "rails_helper"

describe PublishingAdministration::EligibleCowritingPublishingComposersBuilder do
  describe ".cowriting_publishing_composers" do
    context "when cowriters do not have a provider_identifier" do
      it "returns the elgibile cowriters" do
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")

        cowriter  = create(:publishing_composer, :cowriter, account: publishing_composer.account)
        cowriter2 = create(:publishing_composer, :cowriter, account: publishing_composer.account, provider_identifier: "12345")

        cowriters = PublishingAdministration::EligibleCowritingPublishingComposersBuilder.cowriting_publishing_composers

        expect(cowriters).to include cowriter
        expect(cowriters).not_to include cowriter2
      end
    end

    context "when the cowriter has a provider_identifier" do
      it "does not return the cowriter" do
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")
        create(:publishing_composer, :cowriter, account: publishing_composer.account, provider_identifier: "123456789")

        cowriters = PublishingAdministration::EligibleCowritingPublishingComposersBuilder.cowriting_publishing_composers

        expect(cowriters).to be_empty
      end
    end
  end

  context "when a date range is passed in" do
    it "returns cowriter records created between the desired date range" do
      publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789", updated_at: "2019-01-02")

      cowriter  = create(:publishing_composer, :cowriter, account: publishing_composer.account, created_at: "2019-01-02")
      cowriter2 = create(:publishing_composer, :cowriter, account: publishing_composer.account, created_at: "2019-01-10")

      cowriters = PublishingAdministration::EligibleCowritingPublishingComposersBuilder.cowriting_publishing_composers(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(cowriters).to include cowriter
      expect(cowriters).not_to include cowriter2
    end
  end
end
