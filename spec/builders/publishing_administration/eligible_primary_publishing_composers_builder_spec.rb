require "rails_helper"

describe PublishingAdministration::EligiblePrimaryPublishingComposersBuilder do
  describe ".primary_publishing_composers" do
    context "when publishing_composers accounts do not have a provider_account_id" do
      it "returns the elgibile publishing_composers" do
        publishing_composer_list = create_list(:publishing_composer, 2, :with_pub_admin_purchase, provider_identifier: "123456789")

        publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers

        expect(publishing_composer_list).to eq publishing_composers
      end
    end

    context "when publishing_composers do not have a provider_identifier" do
      it "returns the elgibile publishing_composers" do
        publishing_composer  = create(:publishing_composer, :with_pub_admin_purchase)
        publishing_composer.account.update(provider_account_id: "123456789")

        publishing_composer2 = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "12345")
        publishing_composer2.account.update(provider_account_id: "123456789")

        publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers

        expect(publishing_composers).to include publishing_composer
        expect(publishing_composers).not_to include publishing_composer2
      end
    end

    context "when the publishing_composer has a provider_account_id and a provider_identifier" do
      it "does not return the publishing_composer" do
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")
        publishing_composer.account.update(provider_account_id: "123456789")

        publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers
        expect(publishing_composers).to be_empty
      end
    end

    context "when the publishing_composer has a pro but not a cae number" do
      it "does not return the publishing_composer" do
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, :skip_cae_validation,
                          performing_rights_organization_id: 1,
                          provider_identifier: "123456789")
        publishing_composer.account.update(provider_account_id: "123456789")

        publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers
        expect(publishing_composers).to be_empty
      end
    end

    context "when the publishing_composer has a cae number but not a pro" do
      it "does not return the publishing_composer" do
        publishing_composer = create(:publishing_composer, :with_pub_admin_purchase, :skip_cae_validation,
                          cae: "1234567890",
                          provider_identifier: "123456789")
        publishing_composer.account.update(provider_account_id: "123456789")

        publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers
        expect(publishing_composers).to be_empty
      end
    end

    it "returns publishing_composer records when the account was created between the desired date range" do
      publishing_composer  = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")
      publishing_composer2 = create(:publishing_composer, :with_pub_admin_purchase, provider_identifier: "123456789")

      publishing_composer.account.update(created_at: "2019-01-02")
      publishing_composer2.account.update(created_at: "2019-01-10")

      publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(publishing_composers).to include publishing_composer
      expect(publishing_composers).not_to include publishing_composer2
    end

    it "returns publishing_composer records created between the desired date range" do
      publishing_composer  = create(:publishing_composer, :with_pub_admin_purchase, created_at: "2019-01-02")
      publishing_composer2 = create(:publishing_composer, :with_pub_admin_purchase, created_at: "2019-01-10")

      publishing_composer.account.update(provider_account_id: "123456789", created_at: "2019-01-02")
      publishing_composer2.account.update(provider_account_id: "123456789", created_at: "2019-01-10")

      publishing_composers = PublishingAdministration::EligiblePrimaryPublishingComposersBuilder.primary_publishing_composers(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(publishing_composers).to include publishing_composer
      expect(publishing_composers).not_to include publishing_composer2
    end
  end
end
