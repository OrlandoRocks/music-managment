require "rails_helper"

describe PublishingAdministration::EligibleCowritersBuilder do
  describe ".cowriters" do
    context "when cowriters do not have a provider_identifier" do
      it "returns the elgibile cowriters" do
        composer  = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")

        cowriter  = create(:cowriter, composer: composer)
        cowriter2 = create(:cowriter, composer: composer, provider_identifier: "12345")

        cowriters = PublishingAdministration::EligibleCowritersBuilder.cowriters

        expect(cowriters).to include cowriter
        expect(cowriters).not_to include cowriter2
      end
    end

    context "when the cowriter has a provider_identifier" do
      it "does not return the cowriter" do
        composer = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789")
        create(:cowriter, composer: composer, provider_identifier: "123456789")

        cowriters = PublishingAdministration::EligibleCowritersBuilder.cowriters

        expect(cowriters).to be_empty
      end
    end
  end

  context "when a date range is passed in" do
    it "returns cowriter records created between the desired date range" do
      composer  = create(:composer, :with_pub_admin_purchase, provider_identifier: "123456789", updated_at: "2019-01-02")

      cowriter  = create(:cowriter, composer: composer, created_at: "2019-01-02")
      cowriter2 = create(:cowriter, composer: composer, created_at: "2019-01-10")

      cowriters = PublishingAdministration::EligibleCowritersBuilder.cowriters(start_date: "2019-01-01", end_date: "2019-01-05")

      expect(cowriters).to include cowriter
      expect(cowriters).not_to include cowriter2
    end
  end
end
