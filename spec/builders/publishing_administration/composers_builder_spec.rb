require "rails_helper"

describe PublishingAdministration::ComposersBuilder do
  describe ".build" do
    context "only account_id is provided" do
      it "returns all composers belonging to the given account" do
        account = create(:account)
        create(:composer, account: account)
        create(:composer, account: account, person: nil)
    
        expect(PublishingAdministration::ComposersBuilder.build(account.id).length).to eq 2
      end
    end

    context "both account_id and person_id are provided" do
      it "only returns the person's composer" do
        person    = create(:person)
        account   = create(:account)
        create(:composer, account: account, person: person)
        create(:composer, account: account)

        expect(PublishingAdministration::ComposersBuilder.build(account.id, person.id).length).to eq 1
      end
    end

    it "orders the publishing administrator first" do
      account           = create(:account)
      other_composer    = create(:composer, account: account, person: nil)
      managing_composer = create(:composer, account: account, person: account.person)

      expect(PublishingAdministration::ComposersBuilder.build(account.id))
        .to eq [managing_composer, other_composer]
    end
  end
end
