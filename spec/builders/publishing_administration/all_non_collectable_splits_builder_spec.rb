require "rails_helper"

describe PublishingAdministration::AllNonCollectableSplitsBuilder do
  describe ".build" do
    let!(:person) { create(:person) }
    let!(:account) { create(:account, person: person) }
    let!(:publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:second_publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:album) { create(:album, person: person) }
    let!(:split_publishing_composition) { create(:publishing_composition, account: account) }
    let!(:song1) { create(:song, album: album, publishing_composition: split_publishing_composition) }
    let!(:cowriter1) { create(:publishing_composer, :cowriter, person: person, account: account) }
    let!(:cowriter2) { create(:publishing_composer, :cowriter, person: person, account: account) }
    let!(:cowriter3) { create(:publishing_composer, :cowriter, :unknown, person: person, account: account) }
    let!(:publishing_composition_split1) do
      create(
        :publishing_composition_split,
        :non_collectable,
        percent: 25,
        publishing_composer: cowriter1,
        publishing_composition: split_publishing_composition
      )
    end
    let!(:publishing_composition_split2) do
      create(
        :publishing_composition_split,
        :non_collectable,
        percent: 25,
        publishing_composer: cowriter2,
        publishing_composition: split_publishing_composition,
      )
    end
    let!(:publishing_composition_split3) do
      create(
        :publishing_composition_split,
        :non_collectable,
        percent: 25,
        publishing_composer: cowriter3,
        publishing_composition: split_publishing_composition
      )
    end
    let(:publishing_composer_ids) do
      account.publishing_composers.pluck(:id)
    end

    it "builds the query for cowriters and excludes unknown cowriters" do
      cowriters = PublishingAdministration::AllNonCollectableSplitsBuilder.build(publishing_composer_ids)

      first_cowriter = cowriters.first
      expect(first_cowriter).to respond_to(:id)
      expect(first_cowriter).to respond_to(:first_name)
      expect(first_cowriter).to respond_to(:last_name)
      expect(first_cowriter).to respond_to(:is_unknown)
      expect(first_cowriter).to respond_to(:cowriter_share)


      cowriter_split_ids = cowriters.map(&:id)
      expect(cowriter_split_ids).to include(publishing_composition_split1.id, publishing_composition_split2.id)
      expect(cowriter_split_ids).not_to include(publishing_composition_split3.id)
    end
  end
end
