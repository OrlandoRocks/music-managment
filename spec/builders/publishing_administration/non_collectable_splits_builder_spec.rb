require "rails_helper"

describe PublishingAdministration::NonCollectableSplitsBuilder do
  describe ".build" do
    let!(:person)              { create(:person) }
    let!(:account)             { create(:account, person: person) }
    let!(:publishing_composer) { create(:publishing_composer, person: person, account: account) }
    let!(:album)               { create(:album, person: person) }
    let!(:split_publishing_composition) { create(:publishing_composition, account: account) }
    let!(:song1)               { create(:song, album: album, publishing_composition: split_publishing_composition) }
    let!(:cowriter1)           { create(:publishing_composer) }
    let!(:cowriter2)           { create(:publishing_composer) }
    let!(:cowriter3)           { create(:publishing_composer, :unknown) }
    let!(:composer_split) do
      create(
        :publishing_composition_split,
        percent: 25,
        publishing_composer: publishing_composer,
        publishing_composition: split_publishing_composition,
        right_to_collect: true
      )
    end
    let!(:publishing_composition_split1) do
      create(
        :publishing_composition_split,
        percent:      25,
        publishing_composer:     cowriter1,
        publishing_composition:  split_publishing_composition,
        right_to_collect: false
      )
    end
    let!(:publishing_composition_split2) do
      create(
        :publishing_composition_split,
        percent:      25,
        publishing_composer:     cowriter2,
        publishing_composition:  split_publishing_composition,
        right_to_collect: false
      )
    end
    let!(:publishing_composition_split3) do
      create(
        :publishing_composition_split,
        percent:      25,
        publishing_composer:     cowriter3,
        publishing_composition:  split_publishing_composition,
        right_to_collect: false
      )
    end

    it "builds the query for cowriters and excludes unknown cowriters" do
      splits = PublishingAdministration::NonCollectableSplitsBuilder.build(publishing_composer.id)

      first_split = splits.first
      expect(first_split).to respond_to(:id)
      expect(first_split).to respond_to(:first_name)
      expect(first_split).to respond_to(:last_name)
      expect(first_split).to respond_to(:cowriter_share)


      cowriter_split_ids = splits.map(&:id)
      expect(cowriter_split_ids).to include(publishing_composition_split1.id, publishing_composition_split2.id)
      expect(cowriter_split_ids).not_to include(publishing_composition_split3.id)
    end
  end
end
