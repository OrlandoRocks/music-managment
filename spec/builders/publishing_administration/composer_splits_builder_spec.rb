require "rails_helper"

describe PublishingAdministration::ComposerSplitsBuilder do
  describe ".build" do
    let!(:person)              { create(:person) }
    let!(:composer)            { create(:composer, person: person) }
    let!(:composer2)           { create(:composer, person: person) }
    let!(:composer3)           { create(:composer, person: person) }
    let!(:album)               { create(:album, person: person) }
    let!(:split_composition)   { create(:composition) }
    let!(:song1)               { create(:song, album: album, composition: split_composition) }
    let!(:other_compostion)    { create(:composition) }
    let!(:other_song)          { create(:song, album: album, composition: other_compostion) }
    let!(:publishing_split1) do
      create(
        :publishing_split,
        percent:      25,
        writer:       composer,
        composer:     composer,
        composition:  split_composition
      )
    end
    let!(:publishing_split2) do
      create(
        :publishing_split,
        percent:      25,
        writer:       composer2,
        composer:     composer,
        composition:  split_composition
      )
    end
    let!(:publishing_split3) do
      create(
        :publishing_split,
        percent:      25,
        writer:       composer3,
        composer:     composer,
        composition:  other_compostion
      )
    end

    it "builds the query for composers" do
      composers = PublishingAdministration::ComposerSplitsBuilder.build(split_composition.id)

      first_composer = composers.first
      expect(first_composer).to respond_to(:id)
      expect(first_composer).to respond_to(:composer_share)

      composer_split_ids = composers.map(&:id)
      expect(composer_split_ids).to include(publishing_split1.id, publishing_split2.id)
      expect(composer_split_ids).not_to include(publishing_split3.id)
    end
  end
end
