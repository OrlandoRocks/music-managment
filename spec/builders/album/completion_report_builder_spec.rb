require "rails_helper"

describe Album::CompletionReportBuilder do
  let(:person)       { create(:person) }
  let(:album)        { create(:album, person_id: person.id) }

  describe ".build" do
    before(:each) do
      create(:song, album_id: album.id)
      create(:artist, name: "Rick Sanchez")
    end
    it "builds the album completion report" do
      report = Album::CompletionReportBuilder.build(album.id, person.id)
      expect(report.song_data_reports.length).to eq 1
      expect(report.songs_complete?).to be false
    end
  end
end
