require "rails_helper"

describe Album::SearchQueryBuilder do
  let(:artist) { create(:artist, name: "Charlie") }
  let!(:album) do
    create(:album, apple_identifier: "601355010", creatives: [
        { "role" => "primary_artist", "name" => artist.name },
    ])
  end


  context "searching by ids" do
    it "finds an album by album id" do
      expect(Album::SearchQueryBuilder.build("id", album.id).first.id).to eq(album.id)
    end
  end

  context "searching by upcs" do
    let(:tunecore_upc) { create(:tunecore_upc, upcable: album) }
    let(:optional_upc) { create(:optional_upc, upcable: album) }
    let(:physical_upc) { create(:physical_upc, upcable: album) }

    it "finds an album by its tunecore upc" do
      expect(Album::SearchQueryBuilder.build("upc", tunecore_upc.number).first.id).to eq(album.id)
    end

    it "finds an album by its optional upc" do
      expect(Album::SearchQueryBuilder.build("upc", optional_upc.number).first.id).to eq(album.id)
    end

    it "finds an album by its physical upc" do
      expect(Album::SearchQueryBuilder.build("upc", physical_upc.number).first.id).to eq(album.id)
    end
  end

  context "searching by isrcs" do
    let (:song) { create(:song, album: album) }
    it "finds albums by songs' isrcs" do
      expect(Album::SearchQueryBuilder.build("isrc", song.tunecore_isrc).first.id).to eq(album.id)
    end
  end

  context "searching by name" do
    it "finds an album by its name" do
      expect(Album::SearchQueryBuilder.build("name", album.name).first.id).to eq(album.id)
    end

    it "can be paginated" do
      expect(Album::SearchQueryBuilder.build("name", album.name).paginate(
          page: 1,
          per_page: 50
      ).length).to eq(1)
    end
  end

  context "searching by label" do
    it "finds an album by its label name" do
      expect(Album::SearchQueryBuilder.build("label", album.label.name).first.id).to eq(album.id)
    end
  end

  context "searching by artist" do
    it "finds an album by its artist" do
      expect(Album::SearchQueryBuilder.build("artist", artist.name).first.id).to eq(album.id)
    end

    it "finds an album by a partial artist with a wildcard" do
      expect(Album::SearchQueryBuilder.build("artist", "Char*").first.id).to eq(album.id)
    end

    it "finds an album by a case insensitive artist" do
      expect(Album::SearchQueryBuilder.build("artist", "charlie").first.id).to eq(album.id)
    end
  end

  context "searching by apple_id" do
    it "finds an album by its apple id" do
      expect(Album::SearchQueryBuilder.build("apple_id", album.apple_identifier).first.id).to eq(album.id)
    end
  end
end
