require "rails_helper"

describe Album::CreativeQueryBuilder do
  let(:person) { create(:person) }

  describe ".build" do
    it "just works" do
      album = create(:album, person: person)
      artist = create(:artist, name: "Kneebody")
      create(:creative, creativeable: album, artist: artist)
      external_service_id = create(:external_service_id, linkable: person.creatives.first)

      results = Album::CreativeQueryBuilder.build(person)

      expect(results.first.id).to eq person.creatives.first.id
      expect(results.first.artist_name).to eq person.creatives.first.artist.name
      expect(results.first.artist_id.to_s).to eq external_service_id.identifier
    end
  end
end
