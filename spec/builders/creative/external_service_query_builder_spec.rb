require "rails_helper"

describe Creative::ExternalServiceQueryBuilder do
  let(:person) { create(:person) }

  describe ".build" do
    it "just works" do
      album = create(:album, person: person)
      creative = create(:creative, creativeable: album)
      results = Creative::ExternalServiceQueryBuilder.build(creative)
      expect(results.first.name).to eq album.name
    end
  end
end
