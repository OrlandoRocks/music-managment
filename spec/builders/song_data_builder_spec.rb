require "rails_helper"

describe SongDataBuilder do
  let!(:artist)       { create(:artist) }
  let(:person)       { create(:person) }
  let(:album)        { create(:album, person_id: person.id) }
  let(:song)         { create(:song, album_id: album.id) }

  describe ".song_for" do
    it "returns a song data object for the song, person and album" do
      song_data = SongDataBuilder.build(person.id, song)
      expect(song_data.id).to eq(song.id)
      expect(song_data.album_id).to eq(album.id)
      expect(song_data.person_id).to eq(person.id)
    end
  end
end
