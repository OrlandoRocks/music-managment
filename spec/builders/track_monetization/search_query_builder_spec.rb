require "rails_helper"

describe TrackMonetization::SearchQueryBuilder do
  describe "::build" do
    context "when appropriate search params are passed in" do
      context "when searching by a person id" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          person = track_monetization_1.person

          params = {
            search_field: "person_id",
            search_text: person.id
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_1.id)
        end
      end

      context "when searching by a person email" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          person = track_monetization_2.person

          params = {
            search_field: "person_email",
            search_text: person.email
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_2.id)
        end
      end

      context "when searching by a person name" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          person = track_monetization_3.person

          params = {
            search_field: "person_name",
            search_text: person.name
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_3.id)
        end
      end

      context "when searching by a song name" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          song = track_monetization_1.song

          song.update(name: "I am the song of track_monetization_1")
          song.reload

          params = {
            search_field: "song_name",
            search_text: song.name
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_1.id)
        end
      end

      context "when searching by a song isrc" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          song = track_monetization_2.song

          params = {
            search_field: "song_isrc",
            search_text: song.isrc
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_2.id)
        end
      end

      context "when searching by an album upc" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          album = track_monetization_3.album

          params = {
            search_field: "album_upc",
            search_text: album.upc.number
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_3.id)
        end
      end

      context "when searching by an album name" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          album = track_monetization_1.album

          params = {
            search_field: "album_name",
            search_text: album.name
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_1.id)
        end
      end

      context "when searching by an artist name" do
        it "returns the appropriate track monetization record" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          artist = track_monetization_2.person.artist

          params = {
            search_field: "artist_name",
            search_text: artist.name
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(1)
          expect(results.first.id).to eq(track_monetization_2.id)
        end
      end

      context "when the search_text matches nothing" do
        it "returns an empty array" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          params = {
            search_field: "song_name",
            search_text: "qwertyuiolkjhgfdsazxcvbnmlkjhgfdsqwertyuiolkjhgfdsazxcvbnmqwertyuioplkjhgfds"
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results).to eq([])
        end
      end
    end

    context "when inappropriate search params are passed in" do
      context "when the search field is not an accepted field" do
        it "returns all results" do
          track_monetization_1 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
          track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

          params = {
            search_field: "song_title",
            search_text: "qwertyuiolkjhgfdsazxcvbnmlkjhgfdsqwertyuiolkjhgfdsazxcvbnmqwertyuioplkjhgfds"
          }

          results = TrackMonetization::SearchQueryBuilder.build(params)

          expect(results.length).to eq(3)
        end
      end
    end

    context "when filtering" do
      context "by eligibility_status" do
        context "when eligibility_status is eligible" do
          it "returns all eligible track_monetizations" do
            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, :eligible)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata, :ineligible)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata, :pending)

            params = {
              eligibility_status: "eligible"
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(1)
            expect(results.first.id).to eq(track_monetization_1.id)
          end
        end

        context "when eligibility_status is ineligible" do
          it "returns all ineligible track_monetizations" do
            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, :eligible)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata, :ineligible)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata, :pending)

            params = {
              eligibility_status: "ineligible"
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(1)
            expect(results.first.id).to eq(track_monetization_2.id)
          end
        end

        context "when eligibility_status is pending" do
          it "returns all pending track_monetizations" do
            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, :eligible)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata, :ineligible)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata, :pending)

            params = {
              eligibility_status: "pending"
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(1)
            expect(results.first.id).to eq(track_monetization_3.id)
          end
        end

        context "when eligibility_status is nil" do
          it "returns all track_monetizations" do
            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, :eligible)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata, :ineligible)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata, :pending)

            params = {
              eligibility_status: nil
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(3)
          end
        end
      end

      context "by store_id" do
        context "when a store_id is passed in" do
          it "returns all track_monetizations for that store" do
            store = Store.find_by(id: Store::YTSR_STORE_ID)

            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, store: store)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

            params = {
              store_id: store.id
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(1)
            expect(results.first.id).to eq(track_monetization_1.id)
          end
        end

        context "when a store_id is not passed in" do
          it "returns all track_monetizations" do
            store = Store.find_by(id: Store::YTSR_STORE_ID)

            track_monetization_1 = create(:track_monetization, :with_searchable_metadata, store: store)
            track_monetization_2 = create(:track_monetization, :with_searchable_metadata)
            track_monetization_3 = create(:track_monetization, :with_searchable_metadata)

            params = {
              store_id: nil
            }

            results = TrackMonetization::SearchQueryBuilder.build(params)

            expect(results.length).to eq(3)
          end
        end
      end
    end
  end

  describe "::SEARCH_FIELD_OPTIONS" do
    it "returns a hash" do
      expect(TrackMonetization::SearchQueryBuilder::SEARCH_FIELD_OPTIONS).to be_a(Hash)
    end
  end
end
