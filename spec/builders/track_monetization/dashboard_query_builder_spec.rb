require 'rails_helper'

describe TrackMonetization::DashboardQueryBuilder do
  describe "#generate" do
    let!(:person)       { create(:person) }
    let!(:other_person) { create(:person) }
    let!(:album)        { create(:album, person: person) }
    let!(:optional_upc) { create(:optional_upc, upcable: album) }
    let(:tunecore_upc)  { album.tunecore_upc }
    let!(:song)         { create(:song, album: album) }
    let!(:artist)       { album.artist }
    let!(:eligible_track) do
      create(
        :track_monetization,
        person: person,
        song: song,
        eligibility_status: 'eligible'
      )
    end

    let!(:ineligible_track) do
      create(:track_monetization, person: person, eligibility_status: 'ineligible')
    end

    let!(:blocked_track) do
      create(:track_monetization, :blocked, person: person)
    end

    let!(:other_user_track) do
      create(:track_monetization, person: other_person, eligibility_status: 'eligible')
    end

    it "loads eligible track monetizations for a single user" do
      fbtracks = Store.find_by(short_name: "fbtracks")
      data = TrackMonetization::DashboardQueryBuilder.build(person.reload, fbtracks.id)

      expect(data.length).to eq 2
      expect(data.map(&:track_id)).not_to include ineligible_track.id
      expect(data.map(&:track_id)).not_to include other_user_track.id

      serialized_track = data.first
      expect(serialized_track.track_id).to           eq eligible_track.id
      expect(serialized_track.state).to              eq eligible_track.state
      expect(serialized_track.tc_distributor_state).to eq eligible_track.tc_distributor_state
      expect(serialized_track.eligibility_status).to eq eligible_track.eligibility_status
      expect(serialized_track.takedown_at).to        eq eligible_track.takedown_at
      expect(serialized_track.song_id).to            eq song.id
      expect(serialized_track.isrc).to               eq song.tunecore_isrc
      expect(serialized_track.song_name).to          eq song.name
      expect(serialized_track.artist_name).to        eq artist.name
      expect(serialized_track.album_name).to         eq album.name
      expect(serialized_track.upc).to                eq optional_upc.number
    end

    it "doesn't return inactive upc's" do
      create(:optional_upc, upcable: album, inactive: true, number: "640859000822")
      create(:tunecore_upc, upcable: album, inactive: true, number: "640859000823")
      fbtracks  = Store.find_by(short_name: "fbtracks")
      data      = TrackMonetization::DashboardQueryBuilder.build(person.reload, fbtracks.id)
      expect(data.length).to eq 2
      expect(data.first.upc).not_to eq "640859000822"
      expect(data.first.upc).not_to eq "640859000823"
    end

    it "returns the track monetization only if the person is the album's primary artist" do
      fbtracks = Store.find_by(short_name: "fbtracks")
      creative = create(
        :creative,
        creativeable_id: album.id,
        creativeable_type: "Album",
        role: "featuring"
      )
      data = TrackMonetization::DashboardQueryBuilder.build(person, fbtracks.id)

      expect(data.length).to eq 2
      expect(data.first.artist_name).not_to eq creative.name
    end

    it "returns 'Various Artists' for Artist Name when there are no album-level creatives" do
      create(:creative, creativeable_id: song.id, creativeable_type: "Song")
      album.creatives.destroy_all
      fbtracks  = Store.find_by(short_name: "fbtracks")
      data      = TrackMonetization::DashboardQueryBuilder.build(person, fbtracks.id)

      expect(data.length).to eq 2
      expect(data.first.artist_name).to eq "Various Artists"
    end
  end
end
