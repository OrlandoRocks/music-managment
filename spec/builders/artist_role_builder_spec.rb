require "rails_helper"

describe ArtistRoleBuilder do
  describe ".from_role_ids" do
    let(:songwriter) { SongRole.where(role_type: "songwriter", user_defined: true).first }
    let(:composer)   { SongRole.where(role_type: "composer", user_defined: false).first }
    let(:remixer)    { SongRole.where(role_type: "remixer", user_defined: true).first }
    let(:ddex_composer)          { DDEXRole.where(role_type: "composer").first }
    let(:ddex_composer_lyricist) { DDEXRole.where(role_type: "composer_lyricist").first }
    let(:librettist) { SongRole.where(role_type: "librettist", user_defined: true).first }
    let(:ddex_librettist) { DDEXRole.where(role_type: "librettist").first }


    context "given song roles that are user defined and do not map to ddex roles" do
      it "returns only those song roles" do
        roles = ArtistRoleBuilder.from_role_ids([remixer.id])
        expect(roles).to match_array([remixer])
      end
    end

    context "given song roles that are not user defined and do map to ddex roles" do
      it "returns only the ddex roles" do
        roles = ArtistRoleBuilder.from_role_ids([composer.id])
        expect(roles).to match_array([ddex_composer])
      end
    end

    context "given song roles that are user defined and do map to ddex roles" do
      it "returns the song roles and the ddex roles if their role_types are different" do
        roles = ArtistRoleBuilder.from_role_ids([songwriter.id])
        expect(roles).to match_array([songwriter, ddex_composer_lyricist])
      end

      it "returns the songs roles only if the role_types match" do
        roles = ArtistRoleBuilder.from_role_ids([librettist.id])
        expect(roles).to include(librettist)
        expect(roles).not_to include(ddex_librettist)
      end
    end

    context "given a mix of roles" do
      it "returns the correct roles" do
        roles = ArtistRoleBuilder.from_role_ids([songwriter.id, composer.id, remixer.id])
        expect(roles).to match_array([remixer, songwriter, ddex_composer, ddex_composer_lyricist])
      end
    end
  end
end
