require "rails_helper"

describe ServiceForArtists::QueryBuilder do
  before(:each) do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
  end

  describe "initialize" do
    it "raises error on unknown service" do
      expect {
        ServiceForArtists::QueryBuilder.new(1, 'notimpl')
      }.to raise_error('Unconfigured service passed to query builder!')
    end
  end

  describe "#artists_with_service_ids" do
    let(:person) { create(:person) }
    let(:primary_artist) { create(:artist, name: 'primary') }
    let(:featuring_artist) { create(:artist, name: 'featuring') }

    let(:album) do
      create(:album, person: person, creatives: [
        { "role" => "primary_artist", "name" => primary_artist.name }
      ])
    end

    before do
      Creative.create(creativeable: album, artist: featuring_artist, person: person, role: "featuring")
    end

    context "spotify service" do
      let(:query_builder) do
        ServiceForArtists::QueryBuilder.new(person.id, ExternalServiceId::SPOTIFY_SERVICE)
      end

      subject { query_builder.artists_with_service_ids }

      it "should return artists with primary artist / featuring artist" do
        album.reload

        create(:external_service_id, {
          linkable: album.creatives.first,
          identifier: "100001022",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })

        create(:external_service_id, {
          linkable: album.creatives.last,
          identifier: "100001023",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })

        expect(subject.map(&:artist_name)).to include(primary_artist.name, featuring_artist.name)
        expect(subject.map(&:identifier)).to include("100001022", "100001023")
      end

      context "artist in blacklist" do
        before do
          create(:blacklisted_artist, artist: primary_artist)
        end

        it "should not return the artist" do
          album.reload

          create(:external_service_id, {
            linkable: album.creatives.first,
            identifier: "100001022",
            service_name: ExternalServiceId::SPOTIFY_SERVICE,
            linkable_type: "Creative"
          })

          expect(subject.map(&:artist_name)).to_not include(primary_artist.name)
        end

        context "added to store whitelist" do
          it "should include the artist" do
            album.reload
            create(:external_service_id, {
              linkable: album.creatives.first,
              identifier: "100001022",
              service_name: ExternalServiceId::SPOTIFY_SERVICE,
              linkable_type: "Creative"
            })

            spotify_store = Store.find_by(abbrev: 'sp')
            create(:artist_store_whitelist, artist: primary_artist, store: spotify_store)

            expect(subject.map(&:artist_name)).to include(primary_artist.name)
          end
        end
      end

      it "should not return artists which has no identifier" do
        album.reload

        create(:external_service_id, {
          linkable: album.creatives.first,
          identifier: "100001022",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })

        create(:external_service_id, {
          linkable: album.creatives.last,
          identifier: "unavailable",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })

        expect(subject.map(&:artist_name)).to eq([primary_artist.name])
        expect(subject.map(&:identifier)).to eq(["100001022"])
      end
    end

    context "apple service" do
      let(:query_builder) do
        ServiceForArtists::QueryBuilder.new(person.id, ExternalServiceId::APPLE_SERVICE)
      end

      subject { query_builder.artists_with_service_ids }

      it "should only return primary_artist" do
        album.reload

        create(:external_service_id, {
          linkable: album.creatives.first,
          identifier: "100001022",
          service_name: ExternalServiceId::APPLE_SERVICE,
          linkable_type: "Creative"
        })

        create(:external_service_id, {
          linkable: album.creatives.last,
          identifier: "100001023",
          service_name: ExternalServiceId::APPLE_SERVICE,
          linkable_type: "Creative"
        })

        expect(subject.map(&:artist_name)).to eq([primary_artist.name])
        expect(subject.map(&:identifier)).to eq(["100001022"])
      end
    end
  end

  describe "#albums_with_service_ids" do
    let(:person) { create(:person) }
    let(:artist) { create(:artist) }
    let!(:album) do
      create(:album, :with_apple_distribution, person: person, creatives: [
        { "role" => "primary_artist", "name" => artist.name }
      ])
    end

    let!(:external_service_id_album) do
      create(:external_service_id, {
        linkable: album,
        identifier: "100001024",
        service_name: ExternalServiceId::APPLE_SERVICE,
        linkable_type: "Album"
      })
    end

    it "returns albums and esi ids with apple distributions for a given person" do
      query_builder = ServiceForArtists::QueryBuilder.new(person.id, ExternalServiceId::APPLE_SERVICE)
      distributions_list = query_builder.albums_with_service_ids

      expect(distributions_list.first.album_name).to eq(album.name)
      expect(distributions_list.first.identifier).to eq(external_service_id_album.identifier)
    end
  end

  describe "#artists_with_album_ids" do 
    context "with artist ids" do 
      let(:person) { create(:person) }
      let(:artist_with_album_esi) { create(:artist, person: person) }
      let!(:album) do
        create(:album, person: person, creatives: [
          { "role" => "primary_artist", "name" => artist_with_album_esi.name }
        ])
      end

      let(:artist_without_album_esi) { create(:artist, person: person) }

      let!(:album2) do
        create(:album, person: person, creatives: [
          { "role" => "primary_artist", "name" => artist_without_album_esi.name }
        ])
      end

      let!(:external_service_id_album) do
        create(:external_service_id, {
          linkable: album,
          identifier: "100001024",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Album"
        })
      end

      let!(:external_service_id_artist_1) {
        create(:external_service_id, {
          linkable: artist_with_album_esi.creatives.first,
          identifier: "100001022",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })
      }

      let!(:external_service_id_artist_2) {
        create(:external_service_id, {
          linkable: artist_without_album_esi.creatives.first,
          identifier: "100001023",
          service_name: ExternalServiceId::SPOTIFY_SERVICE,
          linkable_type: "Creative"
        })
      } 
      
      subject(:query_builder) { described_class.new(person.id, ExternalServiceId::SPOTIFY_SERVICE).artists_with_album_ids }

      it "returns artist name and identifier of artist with album having esi" do 
        expect(query_builder.first.artist_name).to eq(artist_with_album_esi.name)
        expect(query_builder.first.identifier).to eq(external_service_id_artist_1.identifier)
      end
    end
  end 
end
