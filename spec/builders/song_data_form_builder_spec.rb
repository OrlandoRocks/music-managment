require "rails_helper"

describe SongDataFormBuilder do
  let(:person) { create(:person) }
  let(:album) { create(:album, person_id: person.id) }

  describe ".song_data_forms_for" do
    it "builds song data forms" do
      song_data_form = SongDataFormBuilder.song_data_forms_for(album.id, person.id)
      expect(song_data_form).to be_empty
    end
  end

  describe ".new_song_form_for" do
    it "builds a new song data form" do
      song_data_form = SongDataFormBuilder.new_song_form_for(album.id, person.id)
      expect(song_data_form).to respond_to(:completion_report, :data, :errors)
    end
  end
end
