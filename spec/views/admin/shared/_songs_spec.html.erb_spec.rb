require "rails_helper"
# `song.albums.songs` is blank and needs songs.
xdescribe "/admin/albums/:album_id/song_distribution_option" do
  let(:admin)  { Person.find_by_name!("Super Admin") }
  let(:person) { FactoryBot.create(:person) }
  let(:album)  { FactoryBot.create(:album, person: person) }
  let(:song)   { FactoryBot.create(:song, album: album) }
  let(:song_distribution_option) { FactoryBot.create(:song_distribution_option, song: song) }

  context "available_for_streaming" do
    before do
      assign :creatives, []
      assign :song_creatives, []
    end
    it "displays 'Available for streaming'" do
      render partial: "admin/shared/songs", locals: { album: song.album }
      expect(rendered).to have_text "Available for streaming"
      expect(rendered).to have_selector "span#available_for_streaming_#{song.id}_text"
    end

    it "defaults to Yes" do
      render partial: "admin/shared/songs", locals: { album: song.album }
      expect(rendered).to have_css "span#available_for_streaming_#{song.id}_text", text: "Yes"
    end

    it "is Yes if track is opted in" do
      song.available_for_streaming = "true"
      render partial: "admin/shared/songs", locals: { album: song.album }
      expect(rendered).to have_css "span#available_for_streaming_#{song.id}_text", text: "Yes"
    end

    it "is No if track is not opted in" do
      song.available_for_streaming = "false"
      render partial: "admin/shared/songs", locals: { album: song.album }
      expect(rendered).to have_css "span#available_for_streaming_#{song.id}_text", text: "No"
    end
  end
end

# dummy helpers for view
def permissioned_distribution_options
  SongDistributionOption.all_options.keys
end

def display_yes_or_no?(attribute)
  attribute ? "Yes" : "No"
end

def current_user
end

def admin_song_status(arg1, arg2)
end

def song_on_server?(song)
end
