require "rails_helper"

describe "/admin" do

  before(:each) do
    @current_user = create(:person, :with_role)
  end

  it "should have the admin search form" do
    render template: "admin/dashboard/index.html.erb", locals: { current_user: @current_user }
    expect(response).to have_selector("form")
  end

  it "should not show YouTube Money Administration for a user without permissions" do
    render template: "admin/dashboard/index.html.erb", locals: { current_user: @current_user }
    expect(response).not_to have_text "YouTube Money Administration"
  end

  it "should show YouTube Money Administration for a user with permissions" do
    @current_user.roles << Role.find_by(name: "YTM Approver")
    render template: "admin/dashboard/index.html.erb", locals: { current_user: @current_user }
    expect(response).to have_text "YouTube Sound Recording Revenue Administration"
  end
end
