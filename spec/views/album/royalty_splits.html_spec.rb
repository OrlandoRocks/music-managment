require 'rails_helper'

describe '/albums/#id' do
  let(:album) { create(:album, :with_uploaded_songs, number_of_songs: 3) }
  let(:user) { create :person }
  before do
    view.extend(MockApplicationHelper)
    allow(view).to receive(:current_user).and_return(user)
    allow(view).to receive(:splits_link_cannot_create_disabled_classes).and_return("")
  end

  context 'when release havent royalty_splits' do
    it 'should show correct phrase keys' do
      render '/album/royalty_splits', release: album
      expect(response).to have_selector 'h2', text: custom_t('royalty_splits.royalty_split_configs')
      expect(response).to have_selector 'p.empty-text', text: custom_t('royalty_splits.splits_not_applied')
    end
  end

  context 'when release have royalty_splits' do
    let!(:royalty_split) { create :royalty_split, songs: album.songs }

    it 'should show correct phrase keys' do
      render '/album/royalty_splits', release: album
      expect(response).to have_selector 'h2', text: custom_t('royalty_splits.royalty_split_configs')
      expect(response).not_to have_selector 'p.empty-text', text: custom_t('royalty_splits.splits_not_applied')
    end
  end
end
