require "rails_helper"

describe "/royalty_splits/albums" do
  let(:form_id) { "royalty-split-recipients-form" }
  let(:user) { create :person, plan: Plan.find(Plan::RISING_ARTIST) }

  before do
    view.extend(MockApplicationHelper)
    allow(view).to receive(:current_user).and_return(user)
  end

  context "when album have more than 1 song" do
    let!(:album) { create(:album, :with_songs, number_of_songs: 3) }
    let!(:royalty_split_song) { create(:royalty_split_song, song: album.songs.first) }

    before do
      render "split_album", album: album, form_id: form_id
    end
    subject { response }

    it { should have_selector "h2", text: "Album: #{album.name}" }
    it { should have_selector "h2", text: custom_t("royalty_splits.splits_selected_tracks") }
    it { should have_selector(".track-split", count: 3) }
    it { should have_selector(".split-tracks-form-track") }
    it { should_not have_selector("div.split-tracks-form-track.select") }
  end

  context "when album have just 1 song" do
    let!(:single) { create(:finalized_single, :live) }
    let!(:royalty_split_song) { create(:royalty_split_song, song: single.songs.first) }

    before do
      render "split_album", album: single, form_id: form_id
    end

    subject { response }

    it { should have_selector "h2", text: "Album: #{single.name}" }
    it { should_not have_selector "h2", text: custom_t("royalty_splits.splits_selected_tracks") }
    it { should have_selector(".track-split", count: 1) }
    it { should have_selector(".split-tracks-form-track.selected") }
  end
end
