require "rails_helper"


describe "/videography/index" do
  it "should greet the user with the acknowledgement that they don't have any videos" do
    @complete_videos = 0
    render :template=>'/videos/index'
    expect(response).to have_text custom_t("videos.index.no_videos_to_display")
  end
end
