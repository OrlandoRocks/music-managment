require "rails_helper"

describe ComposerPresenter do
  context "when a composer is passed in" do
    describe "#registration_date" do
      context "when a composer has NOT agreed to the terms" do
        let(:composer) { create(:composer, agreed_to_terms_at: nil) }

        it "returns '-/-/-'" do
          expect(ComposerPresenter.new(composer).registration_date).to eq "-/-/-"
        end
      end

      context "when a composer has NOT agreed to the terms" do
        let(:composer) { create(:composer, agreed_to_terms_at: DateTime.now) }

        it "returns a formatted date" do
          expect(ComposerPresenter.new(composer).registration_date)
            .to eq composer.agreed_to_terms_at.strftime("%m/%d/%Y")
        end
      end
    end

    describe "#display_lod_link?" do
      it "returns false if the composer does not have a publisher" do
        composer = create(:composer)
        expect(ComposerPresenter.new(composer).display_lod_link?).to be_falsey
      end

      it "returns false if the composer has a publisher but does not have legal documents" do
        composer = create(:composer, publisher: create(:publisher))
        expect(ComposerPresenter.new(composer).display_lod_link?).to be_falsey
      end

      it "returns true if the composer has a publisher and has legal documents" do
        composer = create(:composer, publisher: create(:publisher))
        composer.legal_documents << create(:legal_document, subject: "Composer", subject_id: composer.id, provider: "Docusign", signed_at: Date.today)
        expect(ComposerPresenter.new(composer).display_lod_link?).to be_truthy
      end
    end

    describe "#display_lod_not_yet_signed?" do
      it "returns false if the composer does not have a publisher" do
        composer = create(:composer)
        expect(ComposerPresenter.new(composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns false if the composer has a publisher but does not have legal documents" do
        composer = create(:composer, publisher: create(:publisher))
        expect(ComposerPresenter.new(composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns false if the compose has a publisher and a signed legal document" do
        composer = create(:composer, publisher: create(:publisher))
        composer.legal_documents << create(:legal_document, subject: "Composer", subject_id: composer.id, provider: "Docusign", signed_at: Date.today)
        expect(ComposerPresenter.new(composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns true if the composer has a publisher and a unsigned legal documents" do
        composer = create(:composer, publisher: create(:publisher))
        composer.legal_documents << create(:legal_document, subject: "Composer", subject_id: composer.id, provider: "Docusign", signed_at: nil)
        expect(ComposerPresenter.new(composer).display_lod_not_yet_signed?).to be_truthy
      end
    end

    describe ".number_of_compositions" do
      it "should return the correct number of compositions" do
        composer    = create(:composer)
        person      = composer.person
        album       = create(:album, :approved, :finalized, :with_songs, person: person)
        song        = album.songs.first
        composition = create(:composition)
        song.update(composition: composition)

        ntc_comp  = create(:composition)
        ntc_album = create(:non_tunecore_album, composer: composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(composer.reload).number_of_compositions).to eq 2
      end
    end

    describe ".number_of_shares_missing" do
      it "should return the correct number of compositions with shares misssing" do
        composer    = create(:composer)
        person      = composer.person
        album       = create(:album, :approved, :finalized, :with_songs, person: person)
        song        = album.songs.first
        composition = create(:composition)
        song.update(composition: composition)

        ntc_comp  = create(:composition)
        ntc_comp.publishing_splits << create(:publishing_split, writer: composer)
        ntc_album = create(:non_tunecore_album, composer: composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(composer.reload).number_of_shares_missing).to eq 1
      end
    end

    describe ".number_of_shares" do
      it "should return the number of compositions subtracted by the number of shares missing" do
        composer    = create(:composer)
        person      = composer.person
        album       = create(:album, :with_songs, person: person)
        song        = album.songs.first
        composition = create(:composition)
        song.update(composition: composition)

        ntc_comp  = create(:composition)
        ntc_comp.publishing_splits << create(:publishing_split, writer: composer)
        ntc_album = create(:non_tunecore_album, composer: composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(composer.reload).number_of_shares).to eq 1
      end
    end
  end

  context "when a publishing_composer is passed in" do
    describe "#registration_date" do
      context "when a publishing_composer has NOT agreed to the terms" do
        let(:publishing_composer) { create(:publishing_composer, agreed_to_terms_at: nil) }

        it "returns '-/-/-'" do
          expect(ComposerPresenter.new(publishing_composer).registration_date).to eq "-/-/-"
        end
      end

      context "when a publishing_composer has NOT agreed to the terms" do
        let(:publishing_composer) { create(:publishing_composer, agreed_to_terms_at: DateTime.now) }

        it "returns a formatted date" do
          expect(ComposerPresenter.new(publishing_composer).registration_date)
            .to eq publishing_composer.agreed_to_terms_at.strftime("%m/%d/%Y")
        end
      end
    end

    describe "#display_lod_link?" do
      it "returns false if the publishing_composer does not have a publisher" do
        publishing_composer = create(:publishing_composer)
        expect(ComposerPresenter.new(publishing_composer).display_lod_link?).to be_falsey
      end

      it "returns false if the publishing_composer has a publisher but does not have legal documents" do
        publishing_composer = create(:publishing_composer, publisher: create(:publisher))
        expect(ComposerPresenter.new(publishing_composer).display_lod_link?).to be_falsey
      end

      it "returns true if the publishing_composer has a publisher and has legal documents" do
        publishing_composer = create(:publishing_composer, publisher: create(:publisher))
        publishing_composer.legal_documents << create(:legal_document, subject: "PublishingComposer", subject_id: publishing_composer.id, provider: "Docusign", signed_at: Date.today)
        expect(ComposerPresenter.new(publishing_composer).display_lod_link?).to be_truthy
      end
    end

    describe "#display_lod_not_yet_signed?" do
      it "returns false if the publishing_composer does not have a publisher" do
        publishing_composer = create(:publishing_composer)
        expect(ComposerPresenter.new(publishing_composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns false if the publishing_composer has a publisher but does not have legal documents" do
        publishing_composer = create(:publishing_composer, publisher: create(:publisher))
        expect(ComposerPresenter.new(publishing_composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns false if the compose has a publisher and a signed legal document" do
        publishing_composer = create(:publishing_composer, publisher: create(:publisher))
        publishing_composer.legal_documents << create(:legal_document, subject: "PublishingComposer", subject_id: publishing_composer.id, provider: "Docusign", signed_at: Date.today)
        expect(ComposerPresenter.new(publishing_composer).display_lod_not_yet_signed?).to be_falsey
      end

      it "returns true if the publishing_composer has a publisher and a unsigned legal documents" do
        publishing_composer = create(:publishing_composer, publisher: create(:publisher))
        publishing_composer.legal_documents << create(:legal_document, subject: "PublishingComposer", subject_id: publishing_composer.id, provider: "Docusign", signed_at: nil)
        expect(ComposerPresenter.new(publishing_composer).display_lod_not_yet_signed?).to be_truthy
      end
    end

    describe ".number_of_compositions" do
      it "should return the correct number of compositions" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, account: account)
        person = publishing_composer.person
        album = create(:album, :approved, :finalized, :with_songs, person: person)
        song = album.songs.first
        publishing_composition = create(:publishing_composition, account: account)
        song.update(publishing_composition: publishing_composition)

        ntc_comp = create(:publishing_composition, account: account)
        ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(publishing_composer.reload).number_of_compositions).to eq 2
      end
    end

    describe ".number_of_shares_missing" do
      it "should return the correct number of compositions with shares missing" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, account: account)
        person = publishing_composer.person
        album = create(:album, :approved, :finalized, :with_songs, person: person)
        song = album.songs.first
        publishing_composition = create(:publishing_composition, account: account)
        song.update(publishing_composition: publishing_composition)

        ntc_comp = create(:publishing_composition, account: account)
        ntc_comp.publishing_composition_splits << create(:publishing_composition_split, publishing_composer: publishing_composer, right_to_collect: true)
        ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(publishing_composer.reload).number_of_shares_missing).to eq 1
      end
    end

    describe ".number_of_shares" do
      it "should return the number of compositions subtracted by the number of shares missing" do
        account = create(:account)
        publishing_composer = create(:publishing_composer, account: account)
        person = publishing_composer.person
        album = create(:album, :with_songs, person: person)
        song = album.songs.first
        publishing_composition = create(:publishing_composition, account: account)
        song.update(publishing_composition: publishing_composition)

        ntc_comp = create(:publishing_composition, account: account)
        ntc_comp.publishing_composition_splits << create(:publishing_composition_split, publishing_composer: publishing_composer, right_to_collect: true)
        ntc_album = create(:non_tunecore_album, publishing_composer: publishing_composer)
        create(:non_tunecore_song, non_tunecore_album: ntc_album, publishing_composition_id: ntc_comp.id)

        expect(ComposerPresenter.new(publishing_composer.reload).number_of_shares).to eq 1
      end
    end
  end
end
