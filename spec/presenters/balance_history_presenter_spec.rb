require "rails_helper"

describe BalanceHistoryPresenter do
  describe "#transaction_history" do
    let(:person) { create(:person) }
    let(:params) { { person: person, page: 1 } }
    let(:payout_transfer) do
      create(:person_transaction,
             person: person,
             target: create(:payout_transfer, person: person, amount_cents: 50000))
    end
    let(:person_transaction) do
      create(:person_transaction,
             person: person,
             target: create(:purchase, person: person))
    end

    it "returns all person transactions" do
      transactions = BalanceHistoryPresenter.new(params).transaction_history
      expect(transactions).to include payout_transfer, person_transaction
    end
  end

  describe "#pending_transactions" do
    let(:person) { create(:person) }
    let(:params) { { person: person, page: 1 } }
    let(:payout_transfer) do
      create(:person_transaction,
             person: person,
             target: create(:payout_transfer, person: person, amount_cents: 50000))
    end
    let(:person_transaction) do
      create(:person_transaction,
             person: person,
             target: create(:purchase, person: person))
    end

    it "returns pending transactions" do
      pending_transactions = BalanceHistoryPresenter.new(params).pending_transactions
      expect(pending_transactions).to include payout_transfer
    end
  end

  describe "#person_balance" do
    let(:person) { create(:person, :with_balance, amount: 50_000) }
    let(:params) { { person: person, page: 1 } }

    it "returns the person balance" do
      person_balance = BalanceHistoryPresenter.new(params).person_balance
      expect(person_balance).to eq 50000
    end
  end

  describe "#pending_txn_balance" do
    let(:person) { create(:person) }
    let(:params) { { person: person, page: 1 } }
    let(:pending_transactions) do
      create_list(:person_transaction, 2,
                  person: person,
                  target: create(:payout_transfer, person: person, amount_cents: 50000))
    end

    it "calculates the pending transaction balance" do
      pending_transactions.first.update(credit: 1000, debit: 0)

      pending_txn_balance = BalanceHistoryPresenter.new(params).pending_txn_balance
      parsed_balance = Money.new(pending_txn_balance).fractional
      expect(parsed_balance).to eq 900
    end
  end

  describe "#on_last_page?" do
    let(:person) { create(:person) }
    let(:params) { { person: person, page: 1 } }

    context "when a person has less than 10 person transactions" do
      let!(:transactions) do
        create(:person_transaction,
               person: person,
               target: create(:payout_transfer, person: person, amount_cents: 50000))
      end

      it "returns true" do
        on_last_page = BalanceHistoryPresenter.new(params).on_last_page?
        expect(on_last_page).to be_truthy
      end
    end


    context "when a person has 10 or more person transactions" do
      let!(:transactions) do
        create_list(:person_transaction, 11,
                    person: person,
                    target: create(:payout_transfer, person: person, amount_cents: 50000))
      end

      it "returns false" do
        on_last_page = BalanceHistoryPresenter.new(params).on_last_page?
        expect(on_last_page).to be_falsey
      end
    end
  end
end
