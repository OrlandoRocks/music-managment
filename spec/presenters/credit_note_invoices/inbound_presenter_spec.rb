require 'rails_helper'

RSpec.describe CreditNoteInvoices::InboundPresenter, type: :presenter do
  let(:refund) { create(:refund) }
  let(:subject) { described_class.new(refund)}

  describe "#refund_details" do
    it "return hash with all necessory refund information" do
      refund_details_keys = [
        :credit_note_invoice_number,
        :refund_date,
        :currency,
        :refund_reason,
        :admin_note,
        :base_amount,
        :tax_amount,
        :total_amount,
        :vat_amount_in_euro,
        :tax_type,
        :tax_rate,
        :refunded_invoice_number,
        :refunded_invoice_date
      ]

      expect(subject.refund_details.keys)
        .to eq(refund_details_keys)
    end
  end

  describe "#show_auto_liquidation_message?" do
    it "returns false unless user is a EU(Non Luxmbourg) Business customer" do
      expect(subject.show_auto_liquidation_message?)
        .to be(false)
    end
  end

  describe "#show_eu_directive" do
    it "returns true unless country if from EU" do
      expect(subject.show_eu_directive?)
        .to be(true)
    end
  end
end
