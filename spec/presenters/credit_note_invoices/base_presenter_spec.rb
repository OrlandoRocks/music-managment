require 'rails_helper'

RSpec.describe CreditNoteInvoices::BasePresenter, type: :presenter do
  let(:refund) { create(:refund) }
  let(:subject) { described_class.new(refund)}

  describe "#tunecore_invoice?" do
    it "returns false if invoice has bi corporate entity" do
      expect(subject.tunecore_invoice?)
        .to be(true)
    end
  end

  describe "#inbound?" do
    it "return true if the refund is a inbound transaction" do
      expect(subject.inbound?)
        .to be(true)
    end
  end

  describe "#corporate_entity_address" do
    it "return hash with corporate entity address" do
      expect(subject.corporate_entity_address.keys)
        .to eq(InvoiceStaticCorporateEntity::FIELDS.to_a)
    end
  end

  describe "#customer_info" do
    it "return hash with corporate entity address" do
      expect(subject.customer_info.keys)
        .to eq(InvoiceStaticCustomerInfo::FIELDS.to_a)
    end
  end

  describe "#show_customer_vat_registration_number?" do
    it "returns false unless customer has registered vat number" do
      expect(subject.show_customer_vat_registration_number?)
        .to be(false)
    end
  end

  describe "#show_applied_tax_info?" do
    it "returns fals if refund has no tax type" do
      expect(subject.show_applied_tax_info?)
        .to be(false)
    end
  end
end
