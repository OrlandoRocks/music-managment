require "rails_helper"

RSpec.describe CreditNoteInvoices::OutboundPresenter, type: :presenter do
  let(:invoice) {
    create(
      :invoice,
      :with_two_purchases,
      :settled,
      :with_balance_settlement,
      :with_outbound_vat
    )
  }

  let(:outbound_refund) { create(:outbound_refund, outbound_invoice: invoice.outbound_invoice) }
  let(:subject) { described_class.new(outbound_refund) }

  describe "#refund_details" do
    it "return hash with all necessory refund information" do
      refund_details_keys = [
        :credit_note_invoice_number,
        :refund_date,
        :currency,
        :refund_reason,
        :admin_note,
        :base_amount,
        :tax_amount,
        :total_amount,
        :vat_amount_in_euro,
        :tax_type,
        :tax_rate,
        :refunded_invoice_number,
        :refunded_invoice_date,
        :refunded_invoice_country
      ]

      expect(subject.refund_details.keys)
        .to eq(refund_details_keys)
    end
  end

  describe "#conditions" do
    it "inbound should return false" do
      expect(subject.inbound?).to be_falsy
    end

    it "show applied tax info" do
      expect(subject.show_applied_tax_info?).to be_truthy
    end

    it "preview should be false by default" do
      expect(subject.preview?).to be_falsy
    end

    it "luxembourg customer should return false" do
      expect(subject.luxembourg_customer?).to be_falsy
    end
  end
end
