require "rails_helper"

describe ArtistUrlsPresenter do
  let(:album)            { create(:album) }
  let(:apple_url_base)   { "https://itunes.apple.com/us/artist" }
  let(:spotify_url_base) { "https://open.spotify.com/artist" }

  context "when having one main artist" do
    let(:creative) { album.creatives.first }

    context "with an apple artist id" do
      it "returns an object containing the artist_url and artist_name" do
        esi = create(:external_service_id, linkable: creative, service_name: "apple")

        apple_artist_url = "#{apple_url_base}/#{creative.name.parameterize}/#{esi.identifier}"
        expected_obj = [{ "apple" => apple_artist_url, artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true }]
        expect(ArtistUrlsPresenter.build_artist_urls(album)).to match expected_obj
      end
    end

    context "with a spotify artist id" do
      it "returns an object containing the artist_url and artist_name" do
        esi = create(:external_service_id, linkable: creative, service_name: "spotify")

        expected_obj = [{ "spotify" => "#{spotify_url_base}/#{esi.identifier}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true }]
        expect(ArtistUrlsPresenter.build_artist_urls(album)).to eq expected_obj
      end
    end

    context "with an apple and spotify artist id" do
      it "returns an object containing the artist_url and artist_name" do
        artist_id_esis = [
          create(:external_service_id, linkable: creative, service_name: "apple"),
          create(:external_service_id, linkable: creative, service_name: "spotify")
        ]

        esi_identifier   = artist_id_esis.first.identifier
        esi_identifier_2 = artist_id_esis.last.identifier

        expected_obj = [
          { "apple"   => "#{apple_url_base}/#{creative.name.parameterize}/#{esi_identifier}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true },
          { "spotify" => "#{spotify_url_base}/#{esi_identifier_2}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true }
        ]
        expect(ArtistUrlsPresenter.build_artist_urls(album)).to eq expected_obj
      end
    end

    context "with an apple, spotify artist id and youtube oac id" do
      it "returns an object containing the artist_url and artist_name wihtout youtube" do
        artist_id_esis = [
          create(:external_service_id, linkable: creative, service_name: "apple"),
          create(:external_service_id, linkable: creative, service_name: "spotify"),
          create(:external_service_id, linkable: creative, service_name: "youtube_authorization"),
          create(:external_service_id, linkable: creative, service_name: "youtube_channel_id")
        ]

        esi_identifier   = artist_id_esis.first.identifier
        esi_identifier_2 = artist_id_esis.second.identifier

        expected_obj = [
          { "apple"   => "#{apple_url_base}/#{creative.name.parameterize}/#{esi_identifier}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true },
          { "spotify" => "#{spotify_url_base}/#{esi_identifier_2}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true }
        ]
        expect(ArtistUrlsPresenter.build_artist_urls(album)).to eq expected_obj
      end
    end
  end

  context "when having more than one main artist" do
    let(:artist)     { create(:artist) }
    let(:creative)   { album.creatives.first }
    let(:creative_2) { album.creatives.last }

    before do
      album.creatives.create(role: "with", artist: artist)
    end

    it "returns an object with the artist_url and artist_name for each esi" do
      artist_id_esis = [
        create(:external_service_id, linkable: creative, service_name: "apple"),
        create(:external_service_id, linkable: creative, service_name: "spotify")
      ]
      artist_id_esis_2 = [
        create(:external_service_id, linkable: creative_2, service_name: "spotify")
      ]

      esi_identifier   = artist_id_esis.first.identifier
      esi_identifier_1 = artist_id_esis.last.identifier
      esi_identifier_2 = artist_id_esis_2.first.identifier

      expected_obj = [
        { "apple" => "#{apple_url_base}/#{creative.name.parameterize}/#{esi_identifier}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true },
        { "spotify" => "#{spotify_url_base}/#{esi_identifier_1}", artist_name: creative.name, is_new_artist: false, freeze_new_artist_editing: true },
        { "spotify" => "#{spotify_url_base}/#{esi_identifier_2}", artist_name: creative_2.name, is_new_artist: false, freeze_new_artist_editing: true }
      ]
      expect(ArtistUrlsPresenter.build_artist_urls(album)).to eq expected_obj
    end
  end
end
