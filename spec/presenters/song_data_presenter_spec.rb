require "rails_helper"

describe SongDataPresenter do
  let(:artist)     { create(:artist) }
  let(:contributor){ create(:artist) }
  let(:songwriter) { create(:artist) }
  let(:person)     { create(:person) }
  let(:genre)      { create(:genre) }
  let(:album)      { create(:album, person_id: person.id, primary_genre_id: genre.id) }
  let!(:creative)  { create(:creative, creativeable_type: "Album", creativeable_id: album.id, artist_id: artist.id)}
  let!(:songwriting)  { create(:creative, creativeable_type: "Album", creativeable_id: album.id, artist_id: songwriter.id)}
  let!(:contributing)  {
    create(:creative,
      creativeable_type: "Album",
      creativeable_id: album.id,
      role: "contributor",
      artist_id: contributor.id
    )}
  let(:presenter)  { SongDataPresenter.new(album, person.id) }

  let(:song_keys)  { %w(
    id
    name
    language_code_id
    translated_name
    version
    cover_song
    made_popular_by
    explicit
    clean_version
    optional_isrc
    lyrics
    asset_url
    spatial_audio_asset_url
    album_id
    artists
    track_number
    previously_released_at
    asset_filename
    atmos_asset_filename
    instrumental
    copyrights
    song_start_times
    bigbox_disabled
    song_copyright_claims
    cover_song_metadata
  )}

  let!(:songs) { create_list(:song, 2, album_id: album.id) }

  let!(:copyright) { create(:copyright, copyrightable: songs.first) }

  let!(:cover_song_metadata) { create(:cover_song_metadata, song: songs.first) }

  before(:each) do
    create_list(:song_role, 3)
    create(:creative_song_role, song_role_id: SongRole::SONGWRITER_ROLE_ID, creative: songwriting, song: album.reload.songs.first)
  end
  describe "#songs" do
    it "returns song data json" do
      songs = JSON.parse(presenter.songs)
      expect(songs.length).to eq(2)
      expect(songs.first["data"].keys).to eq(song_keys)
      expect(songs.first["data"]["song_copyright_claims"]).to eq(nil)
    end
  end

  describe "#default_song" do
    it "returns the default song object" do
      song = JSON.parse(presenter.default_song)
      expect(song["data"].keys).to match(song_keys)
      expect(song["data"]["id"]).to eq(nil)
    end
  end

  describe "#language_codes" do
    it "returns the included language codes and not the restricted language code" do
      language_descriptions = JSON.parse(presenter.language_codes).map { |language| language["description"] }
      restricted_language_description = "Japanse"

      expect(language_descriptions).to include "English"
      expect(language_descriptions).not_to include restricted_language_description
    end
  end

  describe "#song_roles" do
    it "returns the song roles" do
      song_roles = JSON.parse(presenter.song_roles)
      expect(song_roles.length).to eq(SongRole.all.length)
      expect(song_roles.first["role_type"]).to eq("performer")
    end
  end

  describe "#artists" do
    it "returns all of a person's artist's names" do
      expect(presenter.artists).to include(artist.name)
    end

    it "returns a contributing artist" do
      expect(presenter.artists).to include(contributor.name)
    end
  end

  describe "#songwriter_names" do
    it "returns all of a person's songwriter names" do
      expect(presenter.songwriter_names).to include(songwriter.name)
    end
  end

  describe "#album" do
    it "returns the album data" do
      album_data = JSON.parse(presenter.album)
      expect(album_data["genre"]).to eq(album.primary_genre.name)
      expect(album_data["album_type"]).to eq("Album")
    end
  end

  describe "#cover_song_metadata" do
    it "returns the cover_song_metadata's values" do
      songs = JSON.parse(presenter.songs)

      expect(songs.first["data"]["cover_song_metadata"].keys).to eq(
        [
          "id",
          "song_id",
          "cover_song",
          "licensed",
          "will_get_license",
        ]
      )
    end
  end
end
