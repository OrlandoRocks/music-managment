require "rails_helper"

describe SongSerializer, type: :serializer do
  let(:album) { FactoryBot.create(:album, :finalized) }
  let(:song)  { FactoryBot.create(:song, :with_upload, :with_s3_asset, album: album) }
  let(:serialized_song) { SongSerializer.new(song).to_json }

  subject { JSON.parse(serialized_song)["song"] }

  before(:each) do
    Timecop.freeze
    song.reload
  end

  after(:all) do
    Timecop.return
  end

  describe "#song_id" do
    it "returns the id of the song" do
      expect(subject["song_id"]).to eq song.id
    end
  end

  describe "#number" do
    it "returns a string of the number" do
      expect(subject["number"]).to eq song.track_num
    end
  end

  describe "#title" do
    it "returns song name" do
      expect(subject["title"]).to eq song.name
    end
  end

  describe "#artists" do
    it "returns a singular primary artist from the album if no creatives" do
      expect(subject["artists"][0]["name"]).to eq song.album.artist_name
      expect(subject["artists"][0]["role"]).to eq "primary_artist"
    end

    it "returns name and role of each creatives" do
      FactoryBot.create(:creative, creativeable: song)
      song.reload
      song.creatives.each_with_index do |creative,index|
        expect(subject["artists"][index + 1]["name"]).to eq creative.artist.name
        expect(subject["artists"][index + 1]["role"]).to eq creative.role
      end
    end

    it "returns primary artist if there is a featured artist on a track" do
      FactoryBot.create(:creative, creativeable: song)
      song.reload
      expect(subject["artists"][0]["name"]).to eq song.album.artist_name
      expect(subject["artists"][0]["role"]).to eq "primary_artist"
    end

    it "doesnt return primary artist if album is various" do
      FactoryBot.create(:creative, creativeable: song, role: "featuring")
      song.album.update_attribute(:is_various, true)
      song.reload
      expect(subject["artists"][0]["name"]).not_to eq song.album.artist_name
      expect(subject["artists"][0]["role"]).not_to eq "primary_artist"
    end

    it "doesnt include main album artist as a 'Featured' artist" do
      FactoryBot.create(:creative, creativeable: song, role: "featuring")
      FactoryBot.create(:creative, creativeable: song, role: "featuring", artist: song.album.artist)
      song.reload

      expect(subject["artists"].
             select { |a| a["role"] == "featuring" }.
             map { |a| a["name"] }
            ).not_to include(song.album.artist.name)
    end
  end

  describe "#sku" do
    it "returns song sku" do
      expect(subject["sku"]).to eq song.sku
    end
  end

  describe "#isrc" do
    it "returns song isrc" do
      expect(subject["isrc"]).to eq song.isrc
    end
  end

  describe "#explicit_lyrics" do
    it "returns song explicit_lyrics" do
      expect(subject["explicit_lyrics"]).to eq song.parental_advisory?
    end
  end

  describe "#orig_filename" do
    it "returns uploaded_filename" do
      expect(subject["orig_filename"]).to eq song.upload.uploaded_filename
    end
  end

  describe "#upload_timestamp" do
    it "returns upload created_at" do
      expect(subject["upload_timestamp"]).to eq song.upload.created_at.to_i
    end
  end

  describe "#s3_key" do
    it "returns the s3_asset key if s3_asset exists" do
      expect(subject["s3_key"]).to eq song.s3_asset.key
    end

    it "returns s3_orig_asset key if s3_asset doesn't exists" do
      song.s3_orig_asset_id = song.s3_asset.id
      song.s3_asset_id = nil
      song.save
      song.reload
      expect(subject["s3_key"]).to eq song.s3_orig_asset.key
    end

    it "returns s3_flac_asset key if s3_asset or s3_orig_asset doesn't exists" do
      song.s3_flac_asset_id = song.s3_asset.id
      song.s3_asset_id = nil
      song.save
      song.reload
      expect(subject["s3_key"]).to eq song.s3_flac_asset.key
    end
  end

  describe "#s3_bucket" do
    it "returns the s3_asset bucket if s3_asset exists" do
      expect(subject["s3_bucket"]).to eq song.s3_asset.bucket
    end

    it "returns s3_orig_asset bucket if s3_asset doesn't exists" do
      song.s3_orig_asset_id = song.s3_asset.id
      song.s3_asset_id = nil
      song.save
      song.reload
      expect(subject["s3_bucket"]).to eq song.s3_orig_asset.bucket
    end

    it "returns s3_flac_asset bucket if s3_asset or s3_orig_asset doesn't exists" do
      song.s3_flac_asset_id = song.s3_asset.id
      song.s3_asset_id = nil
      song.save
      song.reload
      expect(subject["s3_bucket"]).to eq song.s3_flac_asset.bucket
    end
  end

  describe "#album_only" do
    it "returns song album_only" do
      expect(subject["album_only"]).to eq song.album_only
    end
  end

  describe "#free_song" do
    it "returns song free_song" do
      expect(subject["free_song"]).to eq song.free_song
    end
  end

  describe "#asset_url" do
    it "returns song asset_url" do
      expect(subject["asset_url"]).to eq song.external_asset_url
    end
  end

  describe "#duration" do
    it "returns song duration" do
      allow(song).to receive(:duration).and_return(3400)
      expect(subject["duration"]).to eq song.duration / 1000
    end
  end
end
