require "rails_helper"

describe Sift::BrowserSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should" do
    it "successfully serialize the event" do
      json = Sift::BrowserSerializer.new(request: request, cookies: cookies).as_json
      assert_key_presence(json, valid_browser)
    end
  end
end