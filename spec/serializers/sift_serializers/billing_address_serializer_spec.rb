require "rails_helper"

describe Sift::BillingAddressSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should successfully serialize the event" do
    it "with a credit card" do
      json = Sift::BillingAddressSerializer.new({credit_card: person_with_payment_methods.stored_credit_cards[0]}).as_json
      assert_key_presence(json, valid_billing_address)
    end

    it "with a compliance info" do
      billing_address_params = {
        :compliance_info => {
          :first_name => "FIRST",
          :last_name => "LAST"
        },
        :phone_number => "+12356342",
        :address1 => "123 Main St.",
        :address2 => "42",
        :city => "Portland",
        :state => "MA",
        :country => "US",
        :zip => "12345"
      }

      json = Sift::BillingAddressSerializer.new(billing_address_params: billing_address_params).as_json
      assert_key_presence(json, valid_billing_address)
    end
  end
end


