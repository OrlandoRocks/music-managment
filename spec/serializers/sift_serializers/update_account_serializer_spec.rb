require "rails_helper"

describe Sift::UpdateAccountSerializer do
  include_context "sift_event_shared_context"

  describe "Serializer should" do
    context "successfully serialize the event" do
      before :each do
        allow(Sift::PaymentMethodSerializer).to receive(:new).and_return(valid_payment_method_json)
        allow(Sift::BillingAddressSerializer).to receive(:new).and_return(valid_billing_address_json)
      end

      it "with update_type contact_info" do
        json = Sift::EventBodySerializer.update_account_body(person: person, params: { 
          :name => "ADWAS",
          :billing_address => {:a => {}},
          :mobile_number => "+1234241412"
        }, update_type: :contact_info, cookies: cookies, request: request)
        assert_key_presence(json, valid_update_account_contact_info)
      end

      it "with update_type email" do
        json = Sift::EventBodySerializer.update_account_body(person: person, params: {}, update_type: :email, cookies: cookies, request: request)
        assert_key_presence(json, valid_update_account_email)
      end

      it "with update_type password" do
        json = Sift::EventBodySerializer.update_account_body(person: person, params: {}, update_type: :password, cookies: cookies, request: request)
        assert_key_presence(json, valid_update_account_password)
      end

      it "with update_type payment_methods" do
        json = Sift::EventBodySerializer.update_account_body(person: person_with_payment_methods, params: {}, update_type: :payment_methods, cookies: cookies, request: request)
        assert_key_presence(json, valid_update_account_payment_methods)
      end
    end
  end
end
