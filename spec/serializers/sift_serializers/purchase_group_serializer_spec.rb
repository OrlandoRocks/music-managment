require "rails_helper"

describe Sift::PurchaseGroupSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should" do
    it "successfully serialize the event" do
      json = Sift::PurchaseGroupSerializer.serialize_purchases(purchases).as_json
      expect(json).to eq(valid_purchase_group_json)
    end
  end
end