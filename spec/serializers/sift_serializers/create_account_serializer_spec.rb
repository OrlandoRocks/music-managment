require "rails_helper"

describe Sift::CreateAccountSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should" do
    context "successfully serialize the event" do
      it "email and name only" do
        json = Sift::EventBodySerializer.create_account_body(person: person, cookies: cookies, request: request)
        assert_key_presence(json, valid_create_account)
      end

      it "with valid mobile_number (India)" do
        person.mobile_number = "+12345678910"
        person.save

        json = Sift::EventBodySerializer.create_account_body(person: person, cookies: cookies, request: request)
        assert_key_presence(json, valid_create_account << :mobile_phone)
      end

      it "with invalid mobile_number (India)" do
        json = Sift::EventBodySerializer.create_account_body(person: person, cookies: cookies, request: request)
        assert_key_presence(json, valid_create_account)
      end
    end
  end
end