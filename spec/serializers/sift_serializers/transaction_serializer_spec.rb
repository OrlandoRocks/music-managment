require "rails_helper"

describe Sift::CreateOrderSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should successfully serialize the event" do
    let(:person_transaction) {
      double(
        debit: purchases.sum(&:cost_cents)
      )
    }

    before :each do
      allow(Sift::PaymentMethodSerializer).to receive(:new).and_return(valid_payment_method_json)
      allow(Sift::BillingAddressSerializer).to receive(:new).and_return(valid_billing_address_json)
      allow(Sift::PurchaseGroupSerializer).to receive(:serialize_purchases).and_return(valid_purchase_group_json)
    end

    context "with one transaction" do
      context "for a credit_card transaction" do
        let(:options) {
          {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }
        }

        before :each do
          allow(finalize_form).to receive(:use_balance).and_return(false)

          valid_transaction << :$billing_address
        end

        it "when transaction is $pending" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $success" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$success"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $failure" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$failure"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end
      end

      context "for a paypal transaction" do
        let(:options) {
          {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }
        }

        before :each do
          allow(finalize_form).to receive(:use_balance).and_return(false)
          allow(finalize_form).to receive(:payment_id).and_return("paypal")
        end

        it "when transaction is $pending" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $success" do
          options[:invoice_id] = invoice.id
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$success"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $failure" do
          options[:invoice_id] = invoice.id
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$failure"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end
      end

      context "for a balance transaction" do
        let(:options) {
          {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }
        }

        before :each do
          allow(finalize_form).to receive(:payment_id).and_return(nil)
        end

        it "when transaction is $pending" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $success" do
          allow(PersonTransaction).to receive(:where).and_return(person_transaction)
          allow(person_transaction).to receive(:last).and_return(person_transaction)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$success"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $failure" do
          allow(PersonTransaction).to receive(:where).and_return(person_transaction)
          allow(person_transaction).to receive(:last).and_return(person_transaction)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$failure"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end
      end

      context "for a payu transaction" do
        before :each do
          allow(request).to receive(:[]).with(:action).and_return("finalize_after_redirect")
        end

        it "when transaction is $pending" do
          options = {
            person: person_with_payu, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }

          allow(finalize_form).to receive(:payment_id).and_return(nil)
          allow(finalize_form).to receive(:use_balance).and_return(false)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $success" do
          purchases
          options = {
            person: person_with_payu, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$success",
            cookies: cookies, 
            request: request
          }
          
          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $failure" do
          purchases
          options = {
            person: person_with_payu, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$failure",
            cookies: cookies, 
            request: request
          }

          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end
      end

      context "for an adyen transaction" do
        before :each do
          allow(request).to receive(:[]).with(:action).and_return("adyen_finalize_after_redirect")
        end

        it "when transaction is $pending" do
          options = {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }

          allow(finalize_form).to receive(:payment_id).and_return("adyen-paymana")
          allow(finalize_form).to receive(:use_balance).and_return(false)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $success" do
          purchases
          options = {
            person: person_with_payment_methods, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$success",
            cookies: cookies, 
            request: request
          }
          
          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end

        it "when transaction is $failure" do
          purchases
          options = {
            person: person_with_payment_methods, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$failure",
            cookies: cookies, 
            request: request
          }

          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(1)
          assert_key_presence(jsons[0], valid_transaction)
        end
      end
    end

    context "with two transaction" do
      before :each do
        allow(PersonTransaction).to receive(:where).and_return(person_transaction)
        allow(person_transaction).to receive(:last).and_return(person_transaction)
      end

      context "for a credit_card and balance transaction" do
        let(:options) {
          {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }
        }

        before :each do
          allow(finalize_form).to receive(:use_balance).and_return(true)

          valid_transaction << :$billing_address
        end

        it "when transaction is $pending" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $success" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$success"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $failure" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$failure"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end
      end

      context "for a paypal and balance transaction" do
        let(:options) {
          {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }
        }

        before :each do
          allow(finalize_form).to receive(:use_balance).and_return(true)
          allow(finalize_form).to receive(:payment_id).and_return("paypal")
        end

        it "when transaction is $pending" do
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $success" do
          options[:invoice_id] = invoice.id
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$success"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $failure" do
          options[:invoice_id] = invoice.id
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$failure"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end
      end

      context "for a payu and balance transaction" do
        before :each do
          allow(request).to receive(:[]).with(:action).and_return("finalize_after_redirect")
        end

        it "when transaction is $pending" do
          options = {
            person: person_with_payu, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }

          allow(finalize_form).to receive(:payment_id).and_return(nil)
          allow(finalize_form).to receive(:use_balance).and_return(true)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $success" do
          purchases
          options = {
            person: person_with_payu, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$success",
            cookies: cookies, 
            request: request
          }
          
          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $failure" do
          purchases
          options = {
            person: person_with_payu, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$failure",
            cookies: cookies, 
            request: request
          }

          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end
      end

      context "for a adyen and balance transaction" do
        before :each do
          allow(request).to receive(:[]).with(:action).and_return("adyen_finalize_after_redirect")
        end

        it "when transaction is $pending" do
          options = {
            person: person_with_payment_methods, 
            invoice_id: nil, 
            finalize_form: finalize_form, 
            cookies: cookies, 
            request: request
          }

          allow(finalize_form).to receive(:payment_id).and_return("adyen-paymana")
          allow(finalize_form).to receive(:use_balance).and_return(true)
          jsons = Sift::EventBodySerializer.transaction_bodies(options.merge(transaction_status: "$pending"))
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $success" do
          purchases
          options = {
            person: person_with_payment_methods, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$success",
            cookies: cookies, 
            request: request
          }
          
          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end

        it "when transaction is $failure" do
          purchases
          options = {
            person: person_with_payment_methods, 
            invoice_id: invoice.id, 
            finalize_form: nil, 
            transaction_status: "$failure",
            cookies: cookies, 
            request: request
          }

          jsons = Sift::EventBodySerializer.transaction_bodies(options)
          expect(jsons.length).to eq(2)
          assert_key_presence(jsons[0], valid_transaction)
          assert_key_presence(jsons[1], valid_transaction)
        end
      end
    end
  end
end