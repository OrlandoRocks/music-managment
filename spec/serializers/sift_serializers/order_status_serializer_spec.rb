require "rails_helper"

describe Sift::OrderStatusSerializer do
  include_context "sift_event_shared_context" 
  let(:order_status) { "completed" }

  context "Serializer should" do
    it "successfully serialize the event" do
      PersonSiftScore.create!(person: person, sift_order_id: "12345")
      json = Sift::EventBodySerializer.order_status_body(person: person, order_status: order_status, cookies: cookies, request: request)
      assert_key_presence(json, valid_order_status)
    end
  end
end