require "rails_helper"

describe Sift::PaymentMethodSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should successfully serialize the event" do
    let(:valid_payment_method_serialize_all_json) {
      [
        {"$payment_type"=>"$credit_card", "$payment_gateway"=>"$braintree", "$card_last4"=>"1234", "card_type"=>"Visa"},
        {"$payment_type"=>"$third_party_processor", "$payment_gateway"=>"$paypal", "$paypal_payer_email"=>"je@okuneva.org"}
      ]
    }

    it "#serialize_all" do
      expect(Sift::PaymentMethodSerializer).to receive(:new).with({
        payment_method_type: :credit_card,
        payment_method: person_with_payment_methods.stored_credit_cards[0]
      })
      expect(Sift::PaymentMethodSerializer).to receive(:new).with({
        payment_method_type: :paypal,
        payment_method: person_with_payment_methods.stored_paypal_accounts[0]
      })
      json = Sift::PaymentMethodSerializer.serialize_all(person: person_with_payment_methods).as_json
    end

    context "#serialize_selected" do
      it "balance" do
        options = {
          person: person_with_payment_methods, 
          payment_id: nil, 
          use_balance: true
        }

        expect(Sift::PaymentMethodSerializer).to receive(:serialize).with(options)
        json = Sift::PaymentMethodSerializer.serialize_selected(options).as_json
      end

      it "credit_card" do
        options = {
          person: person_with_payment_methods, 
          payment_id: person_with_payment_methods.stored_credit_cards[0].id, 
          use_balance: false
        }

        expect(Sift::PaymentMethodSerializer).to receive(:serialize).with(options)
        json = Sift::PaymentMethodSerializer.serialize_selected(options).as_json
      end

      it "paypal" do
        options = {
          person: person_with_payment_methods, 
          payment_id: "paypal", 
          use_balance: false
        }

        expect(Sift::PaymentMethodSerializer).to receive(:serialize).with(options)
        json = Sift::PaymentMethodSerializer.serialize_selected(options).as_json
      end

      it "payu" do
        options = {
          person: person_with_payu, 
          payment_id: nil, 
          use_balance: false
        }

        expect(Sift::PaymentMethodSerializer).to receive(:serialize).with(options)
        json = Sift::PaymentMethodSerializer.serialize_selected(options).as_json
      end

      it "adyen" do
        options = {
          person: person_with_payment_methods, 
          payment_id: "adyen-paymaya", 
          use_balance: false
        }

        expect(Sift::PaymentMethodSerializer).to receive(:serialize).with(options)
        json = Sift::PaymentMethodSerializer.serialize_selected(options).as_json
      end
    end

    context "#serialize" do
      it "credit_card" do
        json = Sift::PaymentMethodSerializer.serialize(person: person_with_payment_methods, payment_id: person_with_payment_methods.stored_credit_cards[0].id, use_balance: false)
        assert_key_presence(json, valid_payment_method_credit_card)
      end

      it "paypal" do
        json = Sift::PaymentMethodSerializer.serialize(person: person_with_payment_methods, payment_id: "paypal", use_balance: false).as_json
        assert_key_presence(json, valid_payment_method_paypal)
      end

      it "balance" do
        json = Sift::PaymentMethodSerializer.serialize(person: person_with_payment_methods, payment_id: nil, use_balance: true).as_json
        assert_key_presence(json, valid_payment_method_balance)
      end

      it "payu" do
        json = Sift::PaymentMethodSerializer.serialize(person: person_with_payu, payment_id: nil, use_balance: false).as_json
        assert_key_presence(json, valid_payment_method_payu)
      end

      it "adyen" do
        json = Sift::PaymentMethodSerializer.serialize(person: person_with_payment_methods, payment_id: "adyen-paymaya", use_balance: false).as_json
        assert_key_presence(json, valid_payment_method_adyen)
      end
    end
  end
end