require "rails_helper"

describe Sift::CreateOrderSerializer do
  include_context "sift_event_shared_context"

  context "Serializer should" do
    it "successfully serialize the event" do
      allow(Sift::PaymentMethodSerializer).to receive(:new).and_return(valid_payment_method_json)
      allow(Sift::BillingAddressSerializer).to receive(:new).and_return(valid_billing_address_json)
      allow(Sift::PurchaseGroupSerializer).to receive(:serialize_purchases).and_return(valid_purchase_group_json)

      json = Sift::EventBodySerializer.create_order_body(finalize_form: finalize_form, cookies: cookies, request: request)
      assert_key_presence(json, valid_create_order)
    end
  end
end