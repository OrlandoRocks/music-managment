require "rails_helper"

describe SalepointSerializer, type: :serializer do
  let(:store)     { Store.find_by(abbrev: "fb") }
  let(:album)     { FactoryBot.create(:album) }
  let(:salepoint) { FactoryBot.create(:salepoint, salepointable: album, store: store) }
  let(:serialized_salepoint) { SalepointSerializer.new(salepoint).to_json }

  subject { JSON.parse(serialized_salepoint)["salepoint"] }

  describe "#store_id" do
    it "returns store_id" do
      expect(subject["store_id"]).to eq salepoint.store_id
    end
  end

  describe "#store_name" do
    it "returns store_name" do
      expect(subject["store_name"]).to eq salepoint.store.name
    end
  end

  describe "#price_code" do
    it "returns price_code" do
      expect(subject["price_code"]).to eq salepoint.price_code
    end
  end

  describe "#track_price_code" do
    it "returns track_price_code" do
      expect(subject["track_price_code"]).to eq salepoint.track_price_code
    end
  end
end
