require "rails_helper"

describe Api::Subscription::PurchaseHandlerSerializer, type: :serializer do
  describe "#data" do
    it "returns data" do
      params = {
        person_id: 1,
        product_type: "monthly",
        product_name: "Social",
        payment_method: "credit_card",
        last_four_cc: "1111",
      }
      purchase_handler = Subscription::Tunecore::PurchaseRequest.new(params)
      date             = Date.today + 1.month
      allow(purchase_handler).to receive(:plan).and_return("pro")
      allow(purchase_handler).to receive(:plan_expires_at).and_return(date)
      serialized_handler = Api::Subscription::PurchaseHandlerSerializer.new(purchase_handler).to_json
      serialized_data    = JSON.parse(serialized_handler)["purchase_handler"]
      expect(serialized_data["data"]["plan"]).to eq "pro"
      expect(serialized_data["data"]["plan_expires_at"]).to eq date.to_s
    end
  end

  describe "inheritance" do
    it "inherits from the Subscription::Base serializer" do
      expect(Api::Subscription::PurchaseHandlerSerializer.superclass).to eq(Api::Subscription::BaseSerializer)
    end
  end
end
