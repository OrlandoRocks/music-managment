require "rails_helper"

describe Api::Subscription::PaymentOptionsSerializer, type: :serializer do
  let(:person)                           { FactoryBot.create(:person, :with_tc_social, :with_stored_paypal_account, :with_stored_credit_card) }
  let(:card)                             { person.stored_credit_cards.first}
  let(:payment_options_fetcher)          { Subscription::PaymentOptionsFetcher.new(person) }
  let(:payment_options)                  { payment_options_fetcher.fetch_payment_options }
  let(:serialized_payment_options)       { payment_options_fetcher.fetch_payment_options; Api::Subscription::PaymentOptionsSerializer.new(payment_options_fetcher).to_json }

  subject { JSON.parse(serialized_payment_options)["payment_options"] }

  describe "#payment_options" do
    it "returns the user's payment options" do
      expect(subject["data"]).to eq({"payment_options" => [
            {"paypal" => {"email" => person.email}},
            {"credit_cards" => [
              {
                "type" => card.cc_type,
                "expiration_month" => card.expiration_month,
                "expiration_year" => card.expiration_year,
                "last_four_digits" => card.last_four
              }
            ]}
          ]})
    end
  end

  describe "#status" do
    it "returns a success status" do
      expect(subject["status"]).to eq "success"
    end
  end

  describe "inheritance" do
    it "inherits from the Subscription::Base serializer" do
      expect(Api::Subscription::PaymentOptionsSerializer.superclass).to eq(Api::Subscription::BaseSerializer)
    end
  end
end
