require "rails_helper"

describe Api::Subscription::PlansSerializer, type: :serializer do
  let(:person)             { create(:person) }
  let(:plans_fetcher)      { Subscription::PlansFetcher.new(person) }
  let(:serialized_plans)   { plans_fetcher.fetch_plans; Api::Subscription::PlansSerializer.new(plans_fetcher).to_json }
  let(:cw_id)              { person.country_website_id }

  subject { JSON.parse(serialized_plans)["plans"] }

  describe "#plans" do

    it "returns a monthly and annual social plan details" do
      expect(subject["data"]["plans"].length).to eq(2)
      expect(subject["data"]["plans"].first.keys).to match_array(["price", "currency", "display_name", "product_type", "discount"])
      expect(subject["data"]["plans"].last.keys).to match_array(["price", "currency", "display_name", "product_type", "discount"])
    end

    context "without a targeted offer discount for either product" do
      it "sets discount to nil" do
        expect(subject["data"]["plans"][0]["discount"]).to eq nil
        expect(subject["data"]["plans"][1]["discount"]).to eq nil
      end
    end

    context "with a targeted offer discount for monthly plan only" do

      before(:each) do
        product   = Product.where(display_name: "tc_social_monthly", country_website_id: cw_id).first
        t_offer   = create(:targeted_offer)
        create(:targeted_product, targeted_offer: t_offer, product: product)
        create(:targeted_person, person: person, targeted_offer: t_offer)
        t_offer.update_attribute(:status, "Active")
        allow_any_instance_of(TargetedProduct).to receive(:show_discount_price).and_return(true)
      end

      it "sets discount to a hash of details for the product with discount" do
        expect(subject["data"]["plans"][0]["discount"]).to_not eq nil
        expect(subject["data"]["plans"][0]["discount"].keys).to match_array(["discount_price", "full_price", "show_discount_price", "flag_text", "price_adj_description"])
      end

      it "sets discount to nil for the product without the discount" do
        expect(subject["data"]["plans"][1]["discount"]).to eq nil
      end
    end

    context "with a targeted offer for both monthly and annual plans" do
      before(:each) do
        product    = Product.where(display_name: "tc_social_monthly", country_website_id: cw_id).first
        product2   = Product.where(display_name: "tc_social_annually", country_website_id: cw_id).first
        t_offer    = create(:targeted_offer)
        create(:targeted_product, targeted_offer: t_offer, product: product)
        create(:targeted_product, targeted_offer: t_offer, product: product2)
        create(:targeted_person, person: person, targeted_offer: t_offer)
        t_offer.update_attribute(:status, "Active")
      end

      it "sets discount to a hash of details for first product with discount" do
        expect(subject["data"]["plans"][0]["discount"]).to_not eq nil
        expect(subject["data"]["plans"][0]["discount"].keys).to match_array(["discount_price", "full_price", "show_discount_price", "flag_text", "price_adj_description"])
      end

      it "sets discount to a hash of details for second product with discount" do
        expect(subject["data"]["plans"][1]["discount"]).to_not eq nil
        expect(subject["data"]["plans"][1]["discount"].keys).to match_array(["discount_price", "full_price", "show_discount_price", "flag_text", "price_adj_description"])
      end
    end
  end

  describe "#status" do
    it "returns a success status" do
      expect(subject["status"]).to eq "success"
    end
  end

  describe "inheritance" do
    it "inherits from the Subscription::Base serializer" do
      expect(Api::Subscription::PlansSerializer.superclass).to eq(Api::Subscription::BaseSerializer)
    end
  end
end
