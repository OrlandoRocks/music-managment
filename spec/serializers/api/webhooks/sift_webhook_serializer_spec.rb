require "rails_helper"

describe Api::Webhooks::Sift::WebhookSerializer do
  include_context "sift_webhook_shared_context"

  it "serializes the controller request into a simple service options object" do
    json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
    expect(json).to include(valid_serializer_response)
  end

  context "test validity of serialized variables:" do
    context "body should" do
      it "not be nil" do
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["body"]).to eq(sift_webhook_body)
      end

      it "be nil and throw an ArgumentError" do
        allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return({})
        expect { Api::Webhooks::Sift::WebhookSerializer.decode(request: request) }.to raise_error(ArgumentError)
      end
    end

    context "person_id should" do
      let(:sift_webhook_body_no_entity) {
        sift_webhook_body.delete "entity"
        sift_webhook_body
      }

      it "not be nil" do
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["person_id"]).to eq(person.id.to_s)
      end
      it "be nil and throw an ArgumentError" do
        allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return(StringIO.new(sift_webhook_body_no_entity.to_json))
        expect { Api::Webhooks::Sift::WebhookSerializer.decode(request: request) }.to raise_error(ArgumentError)
      end
    end

    context "note_options should" do
      let(:sift_webhook_body_no_entity) {
        sift_webhook_body.delete "entity"
        sift_webhook_body
      }

      it "not be nil" do
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["note_options"]).to eq(note_options)
      end

      it "be nil and throw an ArgumentError" do
        allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return(StringIO.new(sift_webhook_body_no_entity.to_json))
        expect { Api::Webhooks::Sift::WebhookSerializer.decode(request: request) }.to raise_error(ArgumentError)
      end
    end

    context "releases_active should" do
      it "be 0" do
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["releases_active"]).to eq(0)
      end
      it "be positive" do
        allow(Note).to receive(:create).and_return(note_options)
        allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return(StringIO.new(sift_webhook_body_with_releases.to_json))
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["releases_active"]).to_not eq(0)
      end
      it "be a number" do
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["releases_active"]).to be_a_kind_of(Integer) 

        allow(Note).to receive(:create).and_return(note)
        allow_any_instance_of(ActionController::TestRequest).to receive(:body).and_return(StringIO.new(sift_webhook_body_with_releases.to_json))
        json = Api::Webhooks::Sift::WebhookSerializer.decode(request: request)
        expect(json["releases_active"]).to be_a_kind_of(Integer) 
      end
    end
  end
end
