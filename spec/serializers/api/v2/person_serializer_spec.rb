require "rails_helper"

describe Api::V2::PersonSerializer do
  let(:person) { create(:person, :with_crt_specialist_role) }

  describe "json format" do
    it "returns the correctly structured JSON" do
      json = Api::V2::PersonSerializer.new(person).to_json
      response = JSON.parse(json)
      expect(response).to eq(
        {
          id: person.id,
          name: person.name,
          email: person.email,
          country: person.country,
          roles: [{
            name: "Content Review Specialist",
            long_name: "Content Review Specialist",
            description: "Content Review Specialist",
            is_administrative: 1,
          }],
          people_flags: [],
          balance: "0.0",
          available_balance: "0.0",
          lock_reason: person.lock_reason,
          status_updated_at: person.status_updated_at,
        }.with_indifferent_access
      )
    end
  end
end
