require "rails_helper"

describe Api::V2::ContentReview::CreativeSerializer do
  describe "#attributes" do
    context "When creative contain a songwriter role" do
      let(:song) { create(:song) }
      let(:songwriter_role) { SongRole.where(role_type: "songwriter").order("id").first }
      let(:creative) { create(:creative, creativeable: song) }
      it "should include songwriter information in creative_song_roles" do
        creative.creative_song_roles.build(song: song, song_role: songwriter_role).save!
        serialzed_result = Api::V2::ContentReview::CreativeSerializer.new(creative).as_json

        expect(serialzed_result).to include(
          name: creative.name,
          role: creative.role,
        )

        expect(serialzed_result[:creative_song_roles].length).to eq(1)

        expect(serialzed_result[:creative_song_roles][0]).to include(
          song_id: song.id,
          creative_id: creative.id,
          song_role: {
            id: songwriter_role.id,
            role_group: songwriter_role.role_group,
            role_type: songwriter_role.role_type
          }
        )
      end
    end
  end
end
