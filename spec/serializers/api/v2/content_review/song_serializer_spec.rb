require "rails_helper"

describe Api::V2::ContentReview::SongSerializer do
  let!(:person) { create(:person) }
  let!(:album) { create(:album, person: person) }
  let!(:song) { create(:song, album: album) }

  let!(:primary_artist) { create(:creative, creativeable: song, role: 'primary_artist') }
  let!(:featuring_artist) { create(:creative, creativeable: song, role: 'featuring') }
  let!(:contributor) { create(:creative, creativeable: song, role: 'contributor') }
  let(:song_role) { SongRole.find_by(id: SongRole::SONGWRITER_ROLE_ID) }
  let!(:songwriter) do
    creative = create(:creative, creativeable: song, role: 'with')
    creative_song_role = create(:creative_song_role, song_role: song_role, creative: creative, song: song)
    creative
  end
  let!(:s3_song_asset_url) { "https://s3-song-asset-url.com" }

  before(:each) do
    song.reload
    allow(song).to receive(:s3_url).and_return(s3_song_asset_url)
  end

  describe "json format" do
    it "returns the correctly structured JSON" do
      json = Api::V2::ContentReview::SongSerializer.new(song).to_json
      response = JSON.parse(json)
      expect(response['id']).to eq(song.id)
      expect(response['name']).to eq(song.name)
      expect(response['track_number']).to eq(song.track_number)
      expect(response['translated_name']).to eq(song.translated_name)
      expect(response['parental_advisory']).to eq(song.parental_advisory)
      expect(response['uuid']).to eq(nil)
      expect(response['lyrics']).to eq(song.lyric.try(:content))
      expect(response['language_code_id']).to eq(song.lyric_language_code_id)
      expect(response['language_description']).to eq(song.lyric_language_code.try(:description))

      expect(response['primary_artists']).to eq(song.primary_artists.map{ |artist|
        {
          id: artist.id,
          name: artist.name,
          role: artist.role,
          creative_song_roles: []
        }.stringify_keys
      })

      expect(response['featuring_artists']).to eq([
        {
          id: featuring_artist.id,
          name: featuring_artist.name,
          role: featuring_artist.role,
          creative_song_roles: []
        }.stringify_keys
      ])

      expect(response['contributors']).to eq([
        {
          id: contributor.id,
          name: contributor.name,
          role: contributor.role,
          creative_song_roles: []
        }.stringify_keys
      ])

      expect(response['songwriters'][0]).to include(
       {
         id: songwriter.id,
         name: songwriter.name,
         role: songwriter.role,
       }.stringify_keys
      )

      expect(response['songwriters'][0]["creative_song_roles"][0]).to include(
        {
          song_id: song.id,
          creative_id: songwriter.id,
          song_role_id: song_role.id,
        }.stringify_keys
      )
    end
  end

  describe "#asset_url" do
    let(:generator_double) { double(S3::QueryStringAuthGenerator, :expires_in= => 3600) }

    before do
      allow(S3::QueryStringAuthGenerator).to receive(:new).and_return(generator_double)
    end

    context "when the song has an s3_asset" do
      it "should append '.mp3' to the s3_asset" do
        asset = create(:s3_asset)
        song.update(s3_asset: asset)

        expect(generator_double).to receive(:get).with(asset.bucket, asset.key + ".mp3")

        Api::V2::ContentReview::SongSerializer.new(song).to_json
      end
    end

    context "when the song does not have an s3_asset" do
      it "should not append '.mp3' to the s3_orig_asset" do
        asset = create(:s3_asset)
        song.update(s3_orig_asset: asset)

        expect(generator_double).to receive(:get).with(asset.bucket, asset.key)

        Api::V2::ContentReview::SongSerializer.new(song).to_json
      end
    end
  end
end
