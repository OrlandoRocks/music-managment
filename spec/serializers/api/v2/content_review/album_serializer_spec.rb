require "rails_helper"

describe Api::V2::ContentReview::AlbumSerializer do
  let(:person)    { create(:person) }
  let(:label)     { create(:label) }
  let(:p_genre)   { create(:genre, name: 'Alternative') }
  let(:s_genre)   { create(:genre, name: 'Electronic') }
  let(:creative)  { [{ "name" => "Sam D", "role" => "primary_artist" }] }
  let(:album)     { create(:album, :with_salepoints, :with_one_year_renewal, :with_artwork, creatives: creative, person: person, label: label, primary_genre_id: p_genre.id,
                            secondary_genre_id: s_genre.id) }
  let!(:song1)    { create(:song, album: album) }
  let!(:song2)    { create(:song, album: album) }

  before do
    allow(album).to receive(:songs).and_return([song1, song2])
    album.upcs << create(:optional_upc)
  end

  describe "#attributes" do
    context "returns album attributes" do
      it "returns album metadata attributes" do
        json = Api::V2::ContentReview::AlbumSerializer.new(album.reload).to_json
        response = JSON.parse(json)
        expect(response['name']).to eq(album.name)
        expect(response['album_type']).to eq(album.album_type)
        expect(response['created_on']).to eq(album.created_on.to_s)
        expect(response['tracks']).to eq(album.songs.size)
        expect(response['album_type']).to eq(album.album_type)
        expect(response['artwork_url']).to eq(album.artwork.url(:large))
        expect(response['artwork_auto_generated']).to be false
        expect(response['is_various']).to eq(album.is_various)
        expect(response['id']).to eq(album.id)
        expect(response['person_id']).to eq(album.person_id)
        expect(response['auto_format_on']).to eq(album.allow_different_format)
        expect(response['vip_status']).to eq(false)
        expect(response['has_multiple_needs_review']).to eq(false)
        expect(response['total_releases']).to eq(person.albums.size)
        expect(response['legal_review_state']).to eq(album.legal_review_state.capitalize)
        expect(response['genres']).to eq(album.genres.map{|g| g.name})
        expect(response['metadata_language_code_id']).to eq(album.metadata_language_code_id)
        expect(response['metadata_language_description']).to eq(album.metadata_language_code.description)
        expect(response['countries']).to eq(album.countries.map{|c| [c.name,c.iso_code]})
        expect(response['allow_different_format']).to eq false
        expect(response['paid_at']).to eq(album.purchases.last.paid_at.utc.iso8601)
        expect(response['tunecore_upc']).to eq(album.tunecore_upc.number)
        expect(response['optional_upc']).to eq(album.optional_upc.number)
      end

      it "returns customer and stores attributes" do
        lock_reason = "Store End"
        person.mark_as_suspicious!("", lock_reason)
        expect(person.status).to eq(Person::SUSPICIOUS)
        expect(person.lock_reason).to eq(lock_reason)

        json = Api::V2::ContentReview::AlbumSerializer.new(album.reload).to_json
        response = JSON.parse(json)
        stores = response['salepoints'].map { |s| s["store"]  }
        expect(stores).to eq(album.salepoints.map{|s| s.store.short_name})
        expect(response['account_status']).to eq(person.status)
        expect(response['account_lock_reason']).to eq(person.lock_reason)
        expect(response['account_owner']).to eq(person.name)
        expect(response['account_email']).to eq(person.email)
        expect(response['account_country']).to eq(person.country)
        expect(response['account_domain']).to eq(person.country_domain)
        expect(response['has_active_ytm']).to eq(person.has_active_ytm?)
        expect(response['first_release']).to eq(person.first_release?)
      end

      it "returns salepoints distribution states" do
        distribution = create(:distribution, :with_salepoint)
        distribution.update(state: "delivered")
        salepoint = distribution.salepoints.first
        album = salepoint.salepointable
        create(:purchase, person_id: album.person.id, related: album)

        json = Api::V2::ContentReview::AlbumSerializer.new(album.reload).to_json
        response = JSON.parse(json)

        distribution_state = response['salepoints'].first['distributions'].first['state']
        expect(distribution_state).to eq distribution.state
      end

      it "returns songs, review audits and artists attributes" do
        json = Api::V2::ContentReview::AlbumSerializer.new(album.reload).to_json
        response = JSON.parse(json)
        expect(response['songs']).to eq(
          [
            {
              "id"=>song1.id,
              "name"=>song1.name,
              "track_number"=>song1.track_num,
              "translated_name"=>song1.translated_name,
              "parental_advisory"=>song1.parental_advisory,
              "lyrics"=>song1.lyric.try(:content),
              "primary_artists"=>[],
              "featuring_artists"=>[],
              "songwriters"=>[],
              "contributors"=>[],
              "instrumental"=>false,
              "isrc"=>song1.isrc,
              "language_code_id"=>song1.lyric_language_code_id,
              "language_description"=>song1.lyric_language_code.description,
              "track_monetizations"=>[],
              "track_monetization_blockers"=>[],
              "asset_url"=>"",
              "filename"=>nil,
              "scrapi_job"=>nil,
              "album_id"=>song1.album_id,
              "duration_in_seconds"=>nil,
              "orig_asset_url"=>""
            },
            {
              "id"=>song2.id,
              "name"=>song2.name,
              "track_number"=>song2.track_num,
              "translated_name"=>song2.translated_name,
              "parental_advisory"=>song2.parental_advisory,
              "lyrics"=>song2.lyric.try(:content),
              "primary_artists"=>[],
              "featuring_artists"=>[],
              "songwriters"=>[],
              "contributors"=>[],
              "instrumental"=>false,
              "isrc"=>song2.isrc,
              "language_code_id"=>song2.lyric_language_code_id,
              "language_description"=>song2.lyric_language_code.description,
              "track_monetizations"=>[],
              "track_monetization_blockers"=>[],
              "asset_url"=>"",
              "filename"=>nil,
              "scrapi_job"=>nil,
              "album_id"=>song2.album_id,
              "duration_in_seconds"=>nil,
              "orig_asset_url"=>""
            }
          ]
        )
        creative_id = album.creatives.first.id
        expect(response['creatives']).to eq([
          {
            "id" => creative_id,
            "name"=>"Sam D",
            "role"=>"primary_artist",
            "creative_song_roles"=>[]
          }
        ])
        expect(response['review_audits']).to eq([])
      end
    end

    it 'return album with various artists' do
      album.update(is_various: true, finalized_at: Time.now)
      json = Api::V2::ContentReview::AlbumSerializer.new(album).to_json
      response = JSON.parse(json)
      expect(response['finalized']).to eq(I18n.localize(album.finalized_at, format: :slash_with_time))
      expect(response['is_various']).to eq(true)
      expect(response['creatives']).to eq([])
    end

    it 'return album with audits' do
      person.roles << Role.find_by(name: 'Content Review Manager')
      person.roles << Role.find_by(name: 'Admin')
      audit = create(:review_audit,
        person: person,
        album: album,
        event: "STARTED REVIEW",
        note: "My Note"
      )
      json = Api::V2::ContentReview::AlbumSerializer.new(album).to_json
      review_audit = JSON.parse(json).dig("review_audits", 0)

      expect(review_audit.dig("person", "name")).to eq person.name
      expect(review_audit["event"]).to eq "STARTED REVIEW"
      expect(review_audit["note"]).to eq "My Note"
      expect(review_audit["created"]).to eq I18n.localize(audit.created_at, format: :slash_with_time)
      expect(review_audit["review_reasons"]).to eq []
    end
  end
end
