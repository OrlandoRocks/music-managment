require "rails_helper"
describe Api::V2::ContentReview::ScrapiJobDetailSerializer do
  let(:person)    { create(:person) }
  let(:album)     { create(:album, :with_uploaded_songs_and_content_fingerprint, :finalized) }
  let(:song)      { album.songs.last }
  let(:scrapi_job) { FactoryBot.create(:scrapi_job, song: song) }
  let(:scrapi_job_detail) { FactoryBot.create(:scrapi_job_detail, scrapi_job: scrapi_job) }

  describe "#return album flagged content data" do

    context "When album has matched scrapi data" do
      it "should return the matched content data" do
        serialzed_result = Api::V2::ContentReview::ScrapiJobDetailSerializer.new(scrapi_job_detail).to_json
        response = JSON.parse(serialzed_result)
        expect(response).to include(
          {
            matched_song_name: scrapi_job_detail.track_title,
            matched_song_album_name: scrapi_job_detail.album_title,
            flagged_song_id: song.id,
            flagged_song_name: song.name,
            artists: [{"name"=>"BLACKPINK"}],
            matched_album_label: scrapi_job_detail.label,
            id: scrapi_job_detail.id
          }.stringify_keys
        )
      end
    end
  end
end
