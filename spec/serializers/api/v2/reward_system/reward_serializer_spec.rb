require "rails_helper"

describe Api::V2::RewardSystem::RewardSerializer do
  let(:reward) { create(:reward) }

  describe "attributes" do
    subject { Api::V2::RewardSystem::RewardSerializer.new(reward).as_json }

    it "should have necessary reward fields" do
      expect(subject).to include({
        id: reward.id,
        name: reward.name,
        description: reward.description,
        link: reward.link,
        content_type: reward.content_type,
        points: reward.points,
        category: reward.category,
        is_active: reward.is_active
      })
    end
  end
end
