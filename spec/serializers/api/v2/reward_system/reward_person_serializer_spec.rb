require "rails_helper"

describe Api::V2::RewardSystem::RewardPersonSerializer do
  include PersonPlanSpecHelper
  let(:person) { create(:person) }

  describe "attributes" do
    subject { Api::V2::RewardSystem::RewardPersonSerializer.new(person).as_json }

    it "should have necessary person fields" do
      expect(subject).to include({
        person_id: person.id,
        name: person.name,
        email: person.email,
        currency: person.currency,
        country_iso_code: person.country_iso_code,
        status: person.status
      })
    end

    describe "lifetime earnings" do
      it "should return person's lifetime_earning_amount" do
        allow(person).to receive(:lifetime_earning_amount).and_return(25.0)

        expect(subject[:lifetime_earnings]).to eq(25.0)
      end
    end

    describe "releases" do
      it "should return number of active & taken down distributed releases" do
        create_list(:album, 2, :approved, person: person)
        create(:album, :taken_down, person: person)
        create(:album, person: person)

        expect(subject[:releases][:active]).to eq(2)
        expect(subject[:releases][:inactive]).to eq(1)
      end
    end

    describe "#facebook_music_signup" do
      it "should return user's fb music status" do
        allow(person).to receive(:has_active_subscription_product_for?).and_return(true)
        expect(subject[:facebook_music_signup]).to eq(true)
      end
    end

    describe "#youtube_monetization" do
      it "should return if user has youtube purchase" do
        allow(person).to receive(:has_youtube_money?).and_return(true)
        expect(subject[:youtube_monetization]).to eq(true)
      end
    end

    describe "#albums" do
      it "should return number of all distributed album" do
        create(:album, :live, person: person)
        create(:album, :approved, person: person)
        create(:album, :taken_down, person: person)
        create(:single, :live, person: person)
        create(:album, person: person)

        expect(subject[:albums][:active]).to eq(1)
        expect(subject[:albums][:inactive]).to eq(1)
      end
    end

    describe "#singles" do
      it "should return number of all distributed album" do
        create(:single, :approved, person: person)
        create(:single, :taken_down, person: person)
        create(:single, person: person)

        expect(subject[:singles][:active]).to eq(1)
        expect(subject[:singles][:inactive]).to eq(1)
      end
    end

    describe "#publishing_customer" do
      it "should return if user has purchased pub admin" do
        allow(person).to receive(:purchased_pub_admin?).and_return(true)
        expect(subject[:publishing_customer]).to eq(true)
      end
    end

    describe "#tc_social_customer" do
      it "should return whether user has tc social subscription" do
        allow(person).to receive(:has_tc_social?).and_return(true)
        expect(subject[:tc_social_customer]).to eq(true)
      end
    end

    describe "#tiers" do
      it "should return empty array if no tiers for person" do
        expect(subject[:tiers]).to eq([])
      end

      it "should return hierarchy sorted list of tier names" do
        tier1 = create(:tier, hierarchy: 1)
        tier2 = create(:tier, hierarchy: 2)
        create(:tier_person, tier: tier1, person: person)
        create(:tier_person, tier: tier2, person: person)

        expect(subject[:tiers]).to eq([tier2.name, tier1.name])
      end
    end

    describe "#points" do
      it "should return null points balance for person" do
        expect(subject[:points]).to be_nil
      end

      it "should return points balance for the person" do
        points_balance = create(:person_points_balance, person: person)

        expect(subject[:points]).to eq(points_balance.balance)
      end
    end

    describe "#plan_id" do
      it "should return null when the user has no plans associated" do
        expect(subject[:plan_id]).to be_nil
      end

      context "When user has a plan associated" do
        let(:plan) { Plan.first }
        before { create_person_plan!(person, plan, PersonPlanHistory::INITIAL_PURCHASE) }
        it "should return the plan_id" do
          expect(subject[:plan_id]).to eq(plan.id)
        end
      end
    end
  end
end
