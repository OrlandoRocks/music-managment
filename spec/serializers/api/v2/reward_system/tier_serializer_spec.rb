require "rails_helper"

describe Api::V2::RewardSystem::TierSerializer do
  let(:tier) { create(:tier) }

  describe "attributes" do
    subject { Api::V2::RewardSystem::TierSerializer.new(tier).as_json }

    it "should have necessary tier fields" do
      expect(subject).to include({
        id: tier.id,
        name: tier.name,
        slug: tier.slug,
        description: tier.description,
        badge_url: tier.badge_url,
        hierarchy: tier.hierarchy,
        is_active: tier.is_active,
        is_sub_tier: tier.is_sub_tier,
        parent_id: tier.parent_id
      })
    end
  end

  describe "scope filters" do
    describe "tier_associations" do
      let(:reward) { create(:reward) }
      let(:certification) { create(:certification) }

      before do
        create(:tier_reward, tier: tier, reward: reward)
        create(:tier_certification, tier: tier, certification: certification)
      end

      context "not passed in" do
        it "should exempt associations filters" do
          result = Api::V2::RewardSystem::TierSerializer.new(tier).as_json

          expect(result[:rewards]).to be_nil
          expect(result[:certifications]).to be_nil
          expect(result[:achievements]).to be_nil
        end
      end

      context "passed in" do
        it "should include appropriate associations filters" do
          result = Api::V2::RewardSystem::TierSerializer.new(
            tier,
            scope: {
              tier_associations: true
            }
          ).as_json

          expect(result[:rewards]).to include(
            Api::V2::RewardSystem::RewardSerializer.new(reward).as_json
          )

          expect(result[:certifications]).to include(
            Api::V2::RewardSystem::CertificationSerializer.new(certification, except: :tiers).as_json
          )
          expect(result[:achievements]).to eq([])
        end
      end
    end
  end
end
