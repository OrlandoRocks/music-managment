require "rails_helper"

describe Api::V2::RewardSystem::AchievementSerializer do
  let(:achievement) { create(:achievement) }
  let(:tier) { create(:tier) }

  describe "attributes" do
    subject { Api::V2::RewardSystem::AchievementSerializer.new(achievement).as_json }

    it "should have necessary achievement fields" do
      achievement.tier_achievements.create tier: tier
      expect(subject).to include({
        id: achievement.id,
        name: achievement.name,
        description: achievement.description,
        link: achievement.link,
        points: achievement.points,
        category: achievement.category,
        is_active: achievement.is_active,
        tiers: [Api::V2::RewardSystem::TierSerializer.new(tier).as_json]
      })
    end
  end
end
