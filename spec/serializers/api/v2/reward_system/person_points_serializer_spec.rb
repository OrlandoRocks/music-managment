require "rails_helper"

describe Api::V2::RewardSystem::PersonPointsSerializer do
  let(:person) { create(:person) }

  describe "attributes" do
    subject { Api::V2::RewardSystem::PersonPointsSerializer.new(person).as_json }

    it "should have appropriate person points fields" do
      expect(subject).to include({
        person_id: person.id,
        points_balance: person.available_points,
      })
    end
  end

end
