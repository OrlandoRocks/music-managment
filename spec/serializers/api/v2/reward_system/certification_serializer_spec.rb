require "rails_helper"

describe Api::V2::RewardSystem::CertificationSerializer do
  let(:certification) { create(:certification) }
  let(:tier) { create(:tier) }


  describe "attributes" do
    subject { Api::V2::RewardSystem::CertificationSerializer.new(certification).as_json }

    it "should have necessary certification fields" do
      certification.tier_certifications.create! tier: tier
      expect(subject).to include({
        id: certification.id,
        name: certification.name,
        description: certification.description,
        link: certification.link,
        badge_url: certification.badge_url,
        points: certification.points,
        category: certification.category,
        is_active: certification.is_active,
        tiers: [Api::V2::RewardSystem::TierSerializer.new(tier).as_json]
      })
    end
  end
end
