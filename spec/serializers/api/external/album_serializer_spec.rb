require "rails_helper"

describe Api::External::AlbumSerializer do
  let(:album) { create(:album, :finalized, :approved, :with_uploaded_songs_and_artwork, :with_apple_music) }
  let(:song) { album.songs.first }
  let(:genre) { album.genres.first }
  let(:apple_url) { album.external_service_ids.where(service_name: 'apple').first }

  skip "serializes an album" do
    response = {
      "album_id": album.id,
      "album_name": album.name,
      "album_s3_key": album.artwork.s3_asset.key,
      "album_artwork_url": album.artwork.url(:medium),
      "artist_name": album.artist.name,
      "artist_apple_url": a_string_starting_with("https://itunes.apple.com"),
      "artist_tcsocial_url": nil,
      "artist_spotify_url": nil,
      "album_upc": album.tunecore_upc.number,
      "release_date": album.golive_date.to_s,
      "release_type": "Album",
      "tracks": [{
                   asset_files_url: a_string_starting_with("https://s3.amazonaws.com:443/s3.asset.bucket/s3-asset-key"),
                   song_name: song.name,
                   isrc: song.tunecore_isrc,
                   track_id: song.id,
                   asset_s3_key: "s3-asset-key"
                 }],
      "genres": [{
                   name: genre.name,
                 }]
    }
    jsoned = Api::External::AlbumSerializer.new(album).as_json(root: false)

    expect(jsoned[:album_id]).to match(response[:album_id])
    expect(jsoned[:album_name]).to match(response[:album_name])
    expect(jsoned[:album_s3_key]).to match(response[:album_s3_key])
    expect(jsoned[:tracks].first[:asset_files_url]).to start_with("https://s3.amazonaws.com:443/s3.asset.bucket/s3-asset-key")
    expect(jsoned[:artist_apple_url]).to start_with("https://itunes.apple.com")
  end
end
