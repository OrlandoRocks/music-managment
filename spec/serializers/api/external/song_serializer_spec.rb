require "rails_helper"

describe Api::External::SongSerializer do
  let(:album) { create(:album, :finalized, :approved, :with_uploaded_songs_and_artwork, :with_apple_music) }
  let(:song) { album.songs.first }

  let(:response) {
    {
      song_name: song.name,
      isrc: song.tunecore_isrc,
      track_id: song.id
    }
  }

  it "serializes a song" do
    jsoned = Api::External::SongSerializer.new(song).as_json(root: false)

    expect(jsoned).to include(response)
    expect(jsoned).to include(:asset_files_url, :asset_s3_key)
  end
end
