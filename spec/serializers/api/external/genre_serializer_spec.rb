require "rails_helper"

describe Api::External::GenreSerializer do
  let(:album) { create(:album, :finalized, :approved, :with_uploaded_songs_and_artwork, :with_apple_music) }
  let(:genre) { album.genres.first }

  let(:response) {
    {
      name: genre.name,
    }.to_json
  }

  it "serializes a genre" do
    jsoned = Api::External::GenreSerializer.new(genre).to_json(root: false)

    expect(jsoned).to eq(response)
  end
end
