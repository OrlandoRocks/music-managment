require "rails_helper"

describe Api::ArtistDiscovery::SongSerializer do
  let(:album) { create(:album, :live, :with_uploaded_songs_and_artwork_and_esi) }
  let(:song) { album.songs.first }
  let(:identifier) { song.external_service_ids.last.identifier }
  let(:response) {
    {
      title: song.name,
      id: song.id,
      image: album.artwork.url(:medium),
      artist_name: album.artist.name,
      song_url: song.s3_asset.original_url,
      song_key: song.s3_asset.key,
      spotify_url: "#{ENV['SPOTIFY_STORE_BASE_URL']}/embed/track/#{identifier}",
      album_name: song.album.name
    }
  }

  it "serializes a song" do
    jsoned = Api::ArtistDiscovery::SongSerializer.new(song).as_json(root: false)
    expect(jsoned).to include(response)
  end
end
