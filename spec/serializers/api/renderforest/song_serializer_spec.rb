require "rails_helper"

describe Api::Renderforest::SongSerializer do
  let(:album) { create(:album, :live, :with_uploaded_songs_and_artwork) }
  let(:song) { album.songs.first }
  let(:response) {
    {
      title: song.name,
      id: song.id,
      image: album.artwork.url(:medium),
      artist_name: album.artist.name
    }
  }

  it "serializes a song" do
    jsoned = Api::Renderforest::SongSerializer.new(song).as_json(root: false)
    expect(jsoned).to include(response)
  end
end
