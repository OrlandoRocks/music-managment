require "rails_helper"

describe Api::Social::PlanStatusSerializer, type: :serializer do
  describe "#data" do
    CountryWebsite.all.each do |country_website|
      it "returns the user's details" do
        person            = create(:person, :with_active_social_subscription_status, country_website_id: country_website.id)
        plan_status       = Social::PlanStatus.for(person)
        serialized_user   = Api::Social::PlanStatusSerializer.new(plan_status).to_json
        expected_response = JSON.parse(serialized_user)["plan_status"]
        country_id = Country.find_by(name: person.country)&.id
        expect(expected_response["data"]).to eq(
          { "email"                 => person.email,
            "plan"                  => plan_status.plan,
            "plan_expires_at"       => plan_status.plan_expires_at.to_s,
            "grace_period_ends_on"  => plan_status.grace_period_ends_on.to_s,
            "balance"               => person.balance.to_s,
            "payment_channel"       => nil,
            "country_website"       => COUNTRY_URLS[person.country_domain],
            "is_active_releases"    => person.distributed_albums?,
            "country"               => country_id
          })
      end
    end
  end

  describe "#status" do
    it "returns a success status" do
      person            = create(:person, :with_active_social_subscription_status)
      plan_status       = Social::PlanStatus.for(person)
      serialized_user   = Api::Social::PlanStatusSerializer.new(plan_status).to_json
      expected_response = JSON.parse(serialized_user)["plan_status"]

      expect(expected_response["status"]).to eq "success"
    end
  end
end
