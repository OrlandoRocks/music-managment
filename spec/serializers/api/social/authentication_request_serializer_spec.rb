require "rails_helper"

describe Api::Social::AuthenticationRequestSerializer, type: :serializer do
  let(:person) { FactoryBot.create(:person, :with_tc_social, :with_active_social_subscription_status) }
  let(:auth_request)            { Api::Social::AuthenticationRequest.new({email: person.email, password: 'Testpass123!'}) }
  let(:serialized_auth_request) { serialize_request(auth_request) }
  subject { JSON.parse(serialized_auth_request)["authentication_request"] }

  describe "#data" do
    it "returns a socially serialized user" do
      allow(Person).to receive(:authenticate).with({email: person.email, password: 'Testpass123!'}).and_return(person)
      plan_status     = Social::PlanStatus.for(person)
      country_id = Country.find_by(name: person.country)&.id
      is_tfa_enabled = person.two_factor_auth.try(:active?) || false
      expect(subject["data"]).to eq(
        { "email"                 => person.email,
          "is_tfa_enabled"        => is_tfa_enabled,
          "plan"                  => plan_status.plan,
          "plan_expires_at"       => plan_status.plan_expires_at.to_s,
          "grace_period_ends_on"  => plan_status.grace_period_ends_on.to_s,
          "first_name"            => person.first_name,
          "last_name"             => person.last_name,
          "balance"               => person.balance.to_s,
          "payment_channel"       => nil,
          "person_id"             => person.id,
          "country"               => country_id,
          "customer_type"         => person.customer_type,
          "corporate_entity"      => person.corporate_entity.try(:name) })
    end
  end

  describe "#status" do
    it "returns status" do
      expect(subject["status"]).to eq "success"
    end
  end
end

def serialize_request(auth_request)
  auth_request.granted?
  Api::Social::AuthenticationRequestSerializer.new(auth_request).to_json
end
