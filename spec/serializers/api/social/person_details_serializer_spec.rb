require "rails_helper"

describe Api::Social::PersonDetailsSerializer, type: :serializer do
  describe "#data" do
    CountryWebsite.all.each do |country_website|
      it "returns the user's details" do
        person            = create(:person, :with_active_social_subscription_status,
                                   country_website_id: country_website.id)
        plan_status       = Social::PlanStatus.for(person)
        serialized_user   = Api::Social::PersonDetailsSerializer.new(person).to_json
        expected_response = JSON.parse(serialized_user)["person_details"]
        country_id = Country.find_by(name: person.country)&.id
        expect(expected_response["data"]).to eq(
          "person_id" => person.id,
          "email" => person.email,
          "name" => person.name,
          "plan" => plan_status.plan,
          "balance" => person.balance.to_s,
          "country_website" => COUNTRY_URLS[person.country_domain],
          "country" => country_id,
          "plan_expires_at" => plan_status.plan_expires_at.to_s,
          "grace_period_ends_on" => plan_status.grace_period_ends_on.to_s,
          "payment_channel" => person.subscription_payment_channel_for("Social")
        )
      end
    end

    it "returns user details with an invalid country" do
      person            = create(:person, :with_active_social_subscription_status, country: "invalid")
      plan_status       = Social::PlanStatus.for(person)
      serialized_user   = Api::Social::PersonDetailsSerializer.new(person).to_json
      expected_response = JSON.parse(serialized_user)["person_details"]
      country_id = Country.find_by(name: person.country)&.id
      expect(expected_response["data"]).to eq(
        "person_id" => person.id,
        "email" => person.email,
        "name" => person.name,
        "plan" => plan_status.plan,
        "balance" => person.balance.to_s,
        "country_website" => COUNTRY_URLS[person.country_domain],
        "country" => country_id,
        "plan_expires_at" => plan_status.plan_expires_at.to_s,
        "grace_period_ends_on" => plan_status.grace_period_ends_on.to_s,
        "payment_channel" => person.subscription_payment_channel_for("Social")
      )
    end
  end

  describe "#status" do
    it "returns a success status" do
      person            = create(:person, :with_active_social_subscription_status)
      serialized_user   = Api::Social::PersonDetailsSerializer.new(person).to_json
      expected_response = JSON.parse(serialized_user)["person_details"]
      expect(expected_response["status"]).to eq "success"
    end
  end
end
