require "rails_helper"

describe Api::Social::PersonTransactionFetcherSerializer, type: :serializer do
  let(:person) { create(:person, :with_transactions_old) }
  let(:transactions) { person.person_transactions }
  let(:params) {
    {
      start_date: "2017-01-01",
      end_date: "2017-02-01"
    }
  }
  let(:fetcher) { Social::PersonTransactionFetcher.execute(person.id, params) }
  let(:serialized_fetcher) { Api::Social::PersonTransactionFetcherSerializer.new(fetcher).to_json }

  subject { JSON.parse(serialized_fetcher)["person_transaction_fetcher"] }

  describe "#data" do
    it "returns the serialized person transactions" do
      expect(subject["data"]).to eq(
        [
          {
            "date" => "2017-01-02",
            "description" => "Sales Posted",
            "debit" => "0.0",
            "credit" => "90.0",
            "invoice_id" => 5678,
            "currency" => "USD"
          },
          {
            "date" => "2017-01-01",
            "description" => "Payment for Invoice 1234",
            "debit" => "100.0",
            "credit" => "0.0",
            "invoice_id" => 1234,
            "currency" => "USD"
          }
        ]
      )
    end
  end

  describe "#status" do
    it "returns a success status" do
      expect(subject["status"]).to eq "success"
    end
  end
end
