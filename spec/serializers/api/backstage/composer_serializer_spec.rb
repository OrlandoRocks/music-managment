require "rails_helper"

describe Api::Backstage::ComposerSerializer do
  let(:person)      { create(:person) }
  let(:cae)         { "1234567890" }
  let(:account)     { create(:account, person: person) }
  let(:composer)    { create(:composer,
                        person: person,
                        account: account,
                        cae: cae,
                        performing_rights_organization_id: 36)
                    }
  let(:response)    {
    {
      id: composer.id,
      account_composer: true,
      cae: cae,
      first_name: composer.first_name,
      full_name: composer.full_name,
      last_name: composer.last_name,
      middle_name: "",
      name: composer.name,
      has_tax_info: false,
      prefix: "",
      pro_id: 36,
      pro_name: "ASCAP",
      suffix: "",
      status: "pending"
    }.to_json
  }

  it "returns a serialized composer" do
    expect(Api::Backstage::ComposerSerializer.new(composer).to_json(root: false)).to eq response
  end
end
