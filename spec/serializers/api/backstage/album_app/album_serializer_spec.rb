require "rails_helper"

describe Api::Backstage::AlbumApp::AlbumSerializer do
  let(:album1) { create(:album) }
  let(:creative1) { album1.creatives.first }
  let!(:creative2) { create(:creative, creativeable: album1) }
  let!(:esid1) do
    create(:external_service_id,
           linkable: creative1,
           identifier: "12345",
           service_name: "apple")
  end
  let!(:esid2) do
    create(:external_service_id,
           linkable: creative1,
           identifier: "6789",
           service_name: "spotify")
  end

  let(:response1) do
    JSON.parse({
      "album_type": "Album",
      "allow_different_format": "",
      "artwork_url": album1.artwork&.url(:medium),
      "creatives": [
        {
          "external_service_ids": [
            {
              "identifier": esid1.identifier,
              "service_name": "apple",
              "state": nil
            },
            {
              "identifier": esid2.identifier,
              "service_name": "spotify",
              "state": nil
            }
          ],
          "id": creative1.id,
          "name": creative1.name,
          "role": "primary_artist"
        },
        {
          "external_service_ids": [],
          "id": creative2.id,
          "name": creative2.name,
          "role": "primary_artist"
        }
      ],
      "finalized": false,
      "genre": album1.genres.first.name,
      "golive_date": Album::DateTransformerService.convert_to_hash(album1.golive_date),
      "is_editable": true,
      "is_new": false,
      "is_previously_released": false,
      "is_various": false,
      "label_name": album1.label_name,
      "language_code": "en",
      "name": album1.name,
      "orig_release_year" => Date.today.to_s,
      "primary_genre_id": album1.primary_genre_id,
      "sale_date": album1.sale_date,
      "secondary_genre_id": "",
      "optional_upc_number": album1.upc.number,
      "id": album1.id,
      "track_limit_reached": false,
      "timed_release_timing_scenario": "absolute_time",
      "explicit": false,
      "clean_version": nil,
      "recording_location": "",
      "optional_isrc": "",
      "parental_advisory": false,
      "person_id": album1.person.id,
      "is_dj_release": false,
      "selected_countries": album1.country_iso_codes
    }.to_json)
  end

  let(:album2) { create(:album) }
  let(:response2) do
    JSON.parse({
      "album_type": "Album",
      "allow_different_format": "",
      "artwork_url": album2.artwork&.url(:medium),
      "creatives": [],
      "finalized": false,
      "genre": album2.genres.first.name,
      "golive_date": Album::DateTransformerService.convert_to_hash(album2.golive_date),
      "is_editable": true,
      "is_new": false,
      "is_previously_released": false,
      "is_various": false,
      "label_name": album2.label_name,
      "language_code": "en",
      "name": album2.name,
      "orig_release_year" => Date.today.to_s,
      "primary_genre_id": album2.primary_genre_id,
      "sale_date": album2.sale_date,
      "secondary_genre_id": "",
      "optional_upc_number": album2.upc.number,
      "id": album2.id,
      "track_limit_reached": false,
      "timed_release_timing_scenario": "absolute_time",
      "explicit": false,
      "clean_version": nil,
      "recording_location": "",
      "optional_isrc": "",
      "parental_advisory": false,
      "person_id": album2.person.id,
      "is_dj_release": false,
      "selected_countries": album2.country_iso_codes
    }.to_json)
  end

  it "serializes an album with creatives with and without ESIDs" do
    expect(JSON.parse(described_class.new(album1).as_json(root: false).to_json)).to eq response1
  end

  it "serializes an album without creatives" do
    album2.creatives.destroy_all
    expect(JSON.parse(described_class.new(album2).as_json(root: false).to_json)).to eq response2
  end
end
