require "rails_helper"

describe Api::Backstage::AlbumApp::StoreSerializer do
  let(:store1) { Store.first }
  let(:salepoint_store_ids1) { [store1.id] }
  let(:response1) do
    JSON.parse({
      "id": store1.id,
      "name": store1.name,
      "selected": true,
      "tagline": nil,
      "custom_abbrev": store1.custom_abbrev,
    }.to_json)
  end

  let(:store2) { Store.second }
  let(:salepoint_store_ids2) { [] }
  let(:response2) do
    JSON.parse({
      "id": store2.id,
      "name": store2.name,
      "selected": false,
      "tagline": nil,
      "custom_abbrev": store2.custom_abbrev,
    }.to_json)
  end

  it "serializes stores with salepoint selections" do
    expect(
      JSON.parse(
        described_class
          .new(store1, context: { salepoint_store_ids: salepoint_store_ids1 })
          .as_json(root: false)
          .to_json
    )).to eq response1
  end

  it "serializes stores without salepoint selections" do
    expect(
      JSON.parse(
        described_class
          .new(store2, context: { salepoint_store_ids: salepoint_store_ids2 })
          .as_json(root: false)
          .to_json
    )).to eq response2
  end
end
