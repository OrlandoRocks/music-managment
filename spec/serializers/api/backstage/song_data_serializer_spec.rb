require "rails_helper"

describe Api::Backstage::SongDataSerializer do
  let(:artist)         { create(:artist) }
  let(:person)         { create(:person) }
  let(:album)          { create(:album, person_id: person.id) }
  let(:song)           { create(:song, album_id: album.id) }
  let(:artist_names)   { ["Name 1", "Name 2"]}
  let!(:copyright)     { create(:copyright, copyrightable: song, composition: true, recording: true) }
  let(:song_data)      { SongData.build(song, person.id, artist_names) }
  let!(:cover_song_metadata) { create(:cover_song_metadata, song: song) }

  before(:each) do
    song_role1 = SongRole.first
    song_role2 = SongRole.last
    album_creative = album.creatives.first
    CreativeSongRole.create(creative_id: album_creative.id, song_role_id: song_role1, song_id: song.id)
    CreativeSongRole.create(creative_id: album_creative.id, song_role_id: song_role2, song_id: song.id)
  end

  describe "json format" do
    it "returns the correctly structured JSON" do
      json = Api::Backstage::SongDataSerializer.new(song_data).to_json
      response = JSON.parse(json)
      expect(response["song_data"].keys).to eq(%w(
        id name language_code_id translated_name version
        cover_song made_popular_by explicit clean_version optional_isrc
        lyrics asset_url spatial_audio_asset_url album_id artists track_number
        previously_released_at asset_filename atmos_asset_filename instrumental
        copyrights song_start_times
        bigbox_disabled song_copyright_claims cover_song_metadata
      ))

      expect(response["song_data"]["artists"].first.keys).to eq(["artist_name", "associated_to", "credit", "role_ids", "creative_id"])
      expect(response["song_data"]["copyrights"].keys).to eq(
        [
          "id",
          "composition",
          "recording",
        ]
      )
      expect(response["song_data"]["song_copyright_claims"]).to eq(nil)
      expect(response["song_data"]["cover_song_metadata"].keys).to eq(
        [
          "id",
          "song_id",
          "cover_song",
          "licensed",
          "will_get_license",
        ]
      )
    end
  end
end
