require "rails_helper"

describe Api::Subscription::SubscriptionRequestSerializer, type: :serializer do
  let(:person)                  { create(:person, :with_social_subscription_status_and_event_and_purchase) }
  let(:get_request)             { Subscription::SubscriptionRequest.get(person, "Social") }
  let(:serialized_subscription) { Api::Subscription::SubscriptionRequestSerializer.new(get_request).to_json }
  let(:cancel_request)          { Subscription::SubscriptionRequest.cancel(person, "Social") }
  let(:serialized_canceled)     { Api::Subscription::SubscriptionRequestSerializer.new(cancel_request).to_json}
  context "get subscription" do
    subject { JSON.parse(serialized_subscription)["subscription_request"] }
    describe "#data" do
      it "returns serialized subscription attributes" do
        expect(subject["data"]["subscription"]["payment_plan"]).to eq("monthly")
        expect(subject["data"]["subscription"]["payment_channel"]).to eq("Tunecore")
        expect(subject["data"]["subscription"]["canceled_at"]).to eq(nil)
        expect(subject["data"]["subscription"]["effective_date"].to_date).to eq(person.subscription_status_for("Social").effective_date.to_date)
        expect(subject["data"]["subscription"]["termination_date"].to_date).to eq(person.subscription_status_for("Social").termination_date.to_date)
        expect(subject["data"]["user"]["plan"]).to eq("pro")
      end
    end
  end

  context "cancel subscription" do
    subject { JSON.parse(serialized_canceled)["subscription_request"] }
    describe "#data" do
      it "returns serialized subscription attributes" do
        expect(subject["data"]["subscription"]["payment_plan"]).to eq("monthly")
        expect(subject["data"]["subscription"]["payment_channel"]).to eq("Tunecore")
        expect(subject["data"]["subscription"]["canceled_at"].to_date).to eq(Date.today.to_date)
        expect(subject["data"]["subscription"]["effective_date"].to_date).to eq(person.subscription_status_for("Social").effective_date.to_date)
        expect(subject["data"]["subscription"]["termination_date"].to_date).to eq(person.subscription_status_for("Social").termination_date.to_date)
        expect(subject["data"]["user"]["plan"]).to eq("pro")
      end
    end
  end

  describe "#status" do
    subject { JSON.parse(serialized_subscription)["subscription_request"] }
    it "returns a success status" do
      expect(subject["status"]).to eq "success"
    end
  end
end
