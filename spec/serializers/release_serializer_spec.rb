require "rails_helper"

describe ReleaseSerializer, type: :serializer do
  let(:album) { FactoryBot.create(:album, :finalized, :with_uploaded_songs_and_artwork, :with_salepoints, :with_booklet) }
  let(:serialized_album) { AlbumSerializer.new(album).to_json }
  subject { JSON.parse(serialized_album)["album"] }

  before(:each) do
    Timecop.freeze
    album.reload
  end

  after(:all) do
    Timecop.return
  end

  describe "#tunecore_id" do
    it "returns the id of the album" do
      expect(subject["tunecore_id"]).to eq album.id
    end
  end

  describe "#upc" do
    it "returns a string of the upc" do
      expect(subject["upc"]).to eq album.upc.to_s
    end
  end

  describe "#title" do
    it "returns album name" do
      expect(subject["title"]).to eq album.name
    end
  end

  describe "#copyright_pline" do
    it "returns concat string of orig_release_year and copyright_name" do
      expect(subject["copyright_pline"]).to eq "#{album.orig_release_date.year} #{album.copyright_name}"
    end
  end

  describe "#genres" do
    it "returns array of genre names" do
      expect(subject["genres"]).to eq album.genres.map(&:name)
    end
  end

  describe "#artwork_s3_key" do
    it "returns artwork_s3_key" do
      expect(subject["artwork_s3_key"]).to eq album.artwork.s3_asset.key
    end
  end

  describe "#label_name" do
    it "returns label_name" do
      expect(subject["label_name"]).to eq album.label.name
    end

    it "returns artist_name of no label" do
      expect(subject["label_name"]).to eq album.artist_name
    end
  end

  describe "#explicit_lyrics" do
    it "returns album parental_advisory" do
      expect(subject["explicit_lyrics"]).to eq album.parental_advisory
    end
  end

  describe "#liner_notes" do
    it "returns liner notes" do
      expect(subject["liner_notes"]).to eq album.liner_notes
    end
  end

  describe "#artists" do
    it "returns singular artist if no creatives exist" do
      album.creatives.destroy_all
      expect(subject["artists"].size).to eq 1
      expect(subject["artists"][0]["name"]).to eq album.artist_name
      expect(subject["artists"][0]["role"]).to eq "primary_artist"
    end

    it "returns array of artists for each creative" do
      expect(subject["artists"].size).to eq album.creatives.size
      album.creatives.each_with_index do |creative, index|
        expect(subject["artists"][index]["name"]).to eq creative.artist.name
        expect(subject["artists"][index]["role"]).to eq creative.role
      end
    end
  end

  describe "#artwork_file" do
    it "returns hash with asset and bucket" do
      expect(subject["artwork_file"]["asset"]).to eq album.artwork.s3_asset.key
      expect(subject["artwork_file"]["bucket"]).to eq album.artwork.s3_asset.bucket
    end
  end

  describe "#booklet_file" do
    it "returns empty hash if no booklet exists" do
      album.booklet.destroy
      album.reload
      expect(subject["booklet_file"]).to be_empty
    end

    it "returns hash with asset and bucket of booklet" do
      expect(subject["booklet_file"]["asset"]).to eq album.booklet.s3_asset.key
      expect(subject["booklet_file"]["bucket"]).to eq album.booklet.s3_asset.bucket
    end
  end

  describe "#delivery_type" do
    it "metadata_only if album is taken down" do
      album.takedown_at    = Time.zone.now
      album.known_live_on  = Time.zone.now
      album.renewal_due_on = Time.zone.now
      album.save!
      album.reload
      expect(subject["delivery_type"]).to eq "metadata_only"
    end

    it "metadata_only if any salepoint is taken down" do
      album.salepoints.last.takedown!
      expect(subject["delivery_type"]).to eq "metadata_only"
    end

    it "full_delivery if album/salepoints are not taken down" do
      expect(subject["delivery_type"]).to eq "full_delivery"
    end
  end

  describe "#release_type" do
    it "release_type returns class name" do
      expect(subject["release_type"]).to eq album.class.name
    end
  end

  describe "#takedown" do
    it "true if album is taken down" do
      album.takedown_at    = Time.zone.now
      album.known_live_on  = Time.zone.now
      album.renewal_due_on = Time.zone.now
      album.save!
      album.reload
      expect(subject["takedown"]).to eq true
    end

    it "true if any salepoint is taken down" do
      album.salepoints.last.takedown!
      expect(subject["takedown"]).to eq true
    end

    it "false if album/salepoints not taken down" do
      expect(subject["takedown"]).to eq false
    end
  end

  describe "#countries" do
    it "returns countries" do
      expect(subject["countries"]).to eq album.country_iso_codes
    end
  end

  describe "#language" do
    it "returns language" do
      expect(subject["language"]).to eq album.language.description
    end
  end

  describe "#customer_name" do
    it "returns customer_name" do
      expect(subject["customer_name"]).to eq album.person.name
    end
  end

  describe "#customer_id" do
    it "returns person_id" do
      expect(subject["customer_id"]).to eq album.person_id
    end
  end

  describe "#actual_release_date" do
    it "returns sale_date" do
      expect(subject["actual_release_date"]).to eq album.sale_date.strftime("%Y-%m-%d")
    end
  end

  describe "#artist_name" do
    it "returns artist_name" do
      expect(subject["artist_name"]).to eq album.artist_name
    end
  end

  describe "#various_artists" do
    it "returns various_artists" do
      expect(subject["various_artists"]).to eq album.is_various?
    end
  end

  describe "#excluded_countries" do
    it "returns excluded_countries" do
      expect(subject["excluded_countries"]).to eq album.excluded_countries
    end
  end
end
