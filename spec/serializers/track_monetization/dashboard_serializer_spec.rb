require 'rails_helper'

describe TrackMonetization::DashboardSerializer do
  describe "#isrc" do
    let(:track_monetization) { Struct.new(:isrc) }

    let(:track_mon) { track_monetization.new('TC9341059669') }

    context 'optional isrc is present' do
      it "returns the optional isrc" do

        serializer = TrackMonetization::DashboardSerializer.new(track_mon)
        expect(serializer.isrc).to eq 'TC9341059669'
      end
    end
  end

  describe "#status" do
    let(:track_monetization) { Struct.new(:state, :takedown_at, :tc_distributor_state) }

    let(:new_track)                     { track_monetization.new('new', nil) }
    let(:new_track_for_takedown)        { track_monetization.new('new', DateTime.now) }

    let(:delivered_track)               { track_monetization.new('delivered', nil) }
    let(:delivered_track_for_takedown)  { track_monetization.new('delivered', DateTime.now) }

    let(:error_track)                   { track_monetization.new('error', nil) }
    let(:tc_distributor_error_track)    { track_monetization.new('delivered_via_tc_distributor', nil, 'error') }
    let(:pending_track)                 { track_monetization.new('pending_approval', nil) }

    let(:processing_track)              { track_monetization.new('gathering_assets', nil) }
    let(:processing_track_with_takedown){ track_monetization.new('gathering_assets', DateTime.now) }

    let(:blocked_track)                 { track_monetization.new('blocked', nil) }

    context 'object can be enqueued' do
      context "when the object has a state of 'errored'" do
        it "is a not-monetized track" do
          serializer = TrackMonetization::DashboardSerializer.new(error_track)
          expect(serializer.status).to eq TrackMonetization::DashboardSerializer::NOT_MONETIZED
        end

        it "is a not-monetized track" do
          serializer = TrackMonetization::DashboardSerializer.new(tc_distributor_error_track)
          expect(serializer.status).to eq TrackMonetization::DashboardSerializer::NOT_MONETIZED
        end
      end
      it "is a new track" do
        serializer = TrackMonetization::DashboardSerializer.new(new_track)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::NEW_TRACK
      end

      it "has a takedown" do
        serializer = TrackMonetization::DashboardSerializer.new(delivered_track_for_takedown)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::NOT_MONETIZED

        serializer = TrackMonetization::DashboardSerializer.new(new_track_for_takedown)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::NOT_MONETIZED
      end

      it "does not have a takedown" do
        [delivered_track, pending_track].each do |track|
          serializer = TrackMonetization::DashboardSerializer.new(track)
          expect(serializer.status).to eq TrackMonetization::DashboardSerializer::MONETIZED
        end
      end
    end

    context 'object is processing' do
      it "has a takedown" do
        serializer = TrackMonetization::DashboardSerializer.new(processing_track_with_takedown)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::PROCESSING_TAKEDOWN
      end

      it "does not have a takedown" do
        serializer = TrackMonetization::DashboardSerializer.new(processing_track)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::PROCESSING
      end
    end

    context 'object is blocked' do
      it 'is a blocked track' do
        serializer = TrackMonetization::DashboardSerializer.new(blocked_track)
        expect(serializer.status).to eq TrackMonetization::DashboardSerializer::BLOCKED
      end
    end
  end

  describe "#upc" do
    let(:track_monetization) { Struct.new(:upc) }

    let(:track_mon) { track_monetization.new("123456789123") }

    context 'optional upc is present' do
      it "returns the optional upc" do

        serializer = TrackMonetization::DashboardSerializer.new(track_mon)
        expect(serializer.upc).to eq "123456789123"
      end
    end
  end

  describe "#discovery" do
    let(:track_monetization) { Struct.new(:created_at, :store_id) }

    let(:fb_old_track) { track_monetization.new('2020-02-02', Store::FBTRACKS_STORE_ID ) }
    let(:fb_new_track) { track_monetization.new('2022-02-02', Store::FBTRACKS_STORE_ID ) }
    let(:yt_old_track) { track_monetization.new('2020-02-02', Store::YTSR_STORE_ID ) }
    let(:yt_new_track) { track_monetization.new('2022-02-02', Store::YTSR_STORE_ID ) }

    before do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:freemium_flow, nil).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:discovery_platforms, nil).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:disable_legacy_youtube_flow, nil).and_return(false)
    end

    context "when the FB track was created before the LEGACY_FB_END_DATE" do
      it "is NOT a discovery track" do
        serializer = TrackMonetization::DashboardSerializer.new(fb_old_track, context: {})
        expect(serializer.discovery).to eq false
      end
    end

    context "when the FB track was created after the LEGACY_FB_END_DATE" do
      it "IS a discovery track" do
        serializer = TrackMonetization::DashboardSerializer.new(fb_new_track, context: {})
        expect(serializer.discovery).to eq true
      end
    end

    context "when the YT track was created before the LEGACY_YTSR_END_DATE" do
      it "is NOT a discovery track" do
        serializer = TrackMonetization::DashboardSerializer.new(yt_old_track, context: {})
        expect(serializer.discovery).to eq false
      end
    end

    context "when the disable_legacy_youtube_flow feature flag is set" do
      before do
        allow(FeatureFlipper).to receive(:show_feature?).with(:disable_legacy_youtube_flow, nil).and_return(true)
      end

      it "is a discovery track when created before LEGACY_YTSR_END_DATE" do
        serializer = TrackMonetization::DashboardSerializer.new(yt_old_track, context: {})
        expect(serializer.discovery).to eq true
      end

      it "is a discovery track when created after LEGACY_YTSR_END_DATE" do
        serializer = TrackMonetization::DashboardSerializer.new(yt_new_track, context: {})
        expect(serializer.discovery).to eq true
      end
    end
  end
end
