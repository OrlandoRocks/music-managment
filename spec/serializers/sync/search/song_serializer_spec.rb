require "rails_helper"

describe Sync::Search::SongSerializer, type: :serializer do
  describe "#to_json" do
    it "serializes song fields to JSON", :disable_verify_partial_doubles do
      album = build(:album, :finalized)
      song = build(:song, :with_upload, :with_s3_asset, album: album)
      allow(song).to receive(:mech_collect_share).and_return('100')

      serializer = Sync::Search::SongSerializer.new(song)
      json = JSON.parse(serializer.to_json)

      expect(json).to include(
                        "song" => hash_including(
                          "name" => song.name,
                          "s3_asset_id" => song.s3_asset_id,
                          "primary_genre" => song.primary_genre_name,
                          "price_policy_id" => song.price_policy_id,
                          "mech_collect_share" => '100'
                          )
                      )
    end
  end
end
