require "rails_helper"

describe Resolvers::AuthenticatedResolver do
  describe("#visible?",) do
    subject { described_class.visible?(context) }

    context "with current_user," do
      let!(:context) { { current_user: :foo } }

      it "returns true" do
        expect(subject).to eq true
      end
    end

    context "without current_user," do
      let!(:context) { { current_user: nil } }

      it "returns false" do
        expect(subject).to eq false
      end
    end
  end
end
