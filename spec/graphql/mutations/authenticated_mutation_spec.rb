require "rails_helper"

describe Mutations::AuthenticatedMutation do
  describe("#visible?,") do
    context "with current_user," do
      it "is true when current_user is present" do
        context = { current_user: { id: 123 } }

        expect(described_class.visible?(context)).to eq(true)
      end
    end

    context "without current_user," do
      it "is false when current_user is blank" do
        context = { current_user: {} }

        expect(described_class.visible?(context)).to eq(false)
      end
    end
  end
end
