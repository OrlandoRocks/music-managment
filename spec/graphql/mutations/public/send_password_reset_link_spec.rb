require "rails_helper"

describe Mutations::Public::SendPasswordResetLink do
  it "returns success with valid email address" do
    person = create(:person)
    url = "#{ENV["V2_BASE_URL"]}/reset-password"
    allow(PasswordResetService).to receive(:reset).and_return(true)

    result = gql_op(email: person.email)

    expect(PasswordResetService).to have_received(:reset).with(person.email, :user, url: url)
    expect(result[:errors]).to be_empty
  end

  it "returns success with email that does not belong to a user" do
    some_email = "foo@bar.com"

    result = gql_op(email: some_email)

    expect(result[:errors]).to be_empty
  end

  it "returns failure when something goes wrong" do
    person = create(:person)
    allow(PasswordResetService).to receive(:reset).and_return(false)

    result = gql_op(email: person.email)

    expect(result[:errors].first.to_h.values).to include("Something went wrong")
  end

  it "unlocks account" do
    person = create(:person, :with_locked_account)

    gql_op(email: person.email)

    expect(person.reload.account_locked?).to be_falsey
  end
end
