require "rails_helper"

describe Mutations::Public::TfaResend do

  context "with valid session token," do
    let!(:mock_result) { { errors: [], tfa_session_token: 'foo' } }
    let!(:args) { { tfa_session_token: "bar" } }

    it "returns a token" do
      allow(V2::Authentication::TfaResendService).to receive(:call).and_return(mock_result)

      result = gql_op(args)

      expect(result[:tfa_session_token]).to eq mock_result[:tfa_session_token]
      expect(result[:errors].empty?).to be true
    end
  end

  context "with invalid session token," do
    let!(:mock_result) do
      { errors: ['foo'], tfa_session_token: nil }
    end
    let!(:bad_args) { { tfa_session_token: "bar" } }

    it "returns a null token with error" do
      allow(V2::Authentication::TfaResendService).to receive(:call).and_return(mock_result)

      result = gql_op(bad_args)

      expect(result[:tfa_session_token]).to be_nil
      expect(result[:errors].first.to_h.values).to include("Unauthorized", {"code"=>"FORBIDDEN"})
    end
  end
end
