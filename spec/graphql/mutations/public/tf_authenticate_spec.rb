require "rails_helper"

describe Mutations::Public::TfAuthenticate do

  context "with valid code," do
    let!(:mock_result) { { errors: [], jwt: 'foo', nonce: 'bar' } }
    let!(:args) { { auth_code: "foo", tfa_session_token: "bar" } }

    it "returns a token" do
      allow(V2::Authentication::AuthenticationService).to receive(:call).and_return(mock_result)

      result = gql_op(args)

      expect(result[:jwt]).to eq mock_result[:jwt]
      expect(result[:nonce]).to eq mock_result[:nonce]
      expect(result[:errors].empty?).to be true
    end
  end

  context "with invalid code," do
    let!(:mock_result) do
      { errors: ['foo'], jwt: nil, nonce: nil, tfa_required: false, tfa_session_token: nil }
    end
    let!(:bad_args) { { auth_code: "foo", tfa_session_token: "bar" } }

    it "returns a null token with error" do
      allow(V2::Authentication::AuthenticationService).to receive(:call).and_return(mock_result)

      result = gql_op(bad_args)

      expect(result[:jwt]).to be_nil
      expect(result[:nonce]).to be_nil
      expect(result[:errors].first.to_h.values).to include("Unauthorized", {"code"=>"FORBIDDEN"})
    end
  end
end
