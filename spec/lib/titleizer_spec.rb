#!/bin/env ruby
# encoding: utf-8

require "rails_helper"

describe Utilities::Titleizer, "when titleizing normal text" do

  it "should handle the examples in the iTMS Style Guide 5.0" do
    expect(Utilities::Titleizer.titleize("in the still of the night")).to eq("In the Still of the Night")
    expect(Utilities::Titleizer.titleize("(YOU MAKE ME FEEL LIKE A) NATURAL WOMAN")).to eq("(You Make Me Feel Like A) Natural Woman")
    expect(Utilities::Titleizer.titleize("TO BE, OR NOT TO BE")).to eq("To Be, or Not to Be")
    expect(Utilities::Titleizer.titleize("the one and only")).to eq("The One and Only")
    expect(Utilities::Titleizer.titleize("god willing & the creek don't rise")).to eq("God Willing & the Creek Don't Rise")
    expect(Utilities::Titleizer.titleize("SOME KIND OF TROUBLE")).to eq("Some Kind of Trouble")
    expect(Utilities::Titleizer.titleize("love: and a million other things")).to eq("Love: And a Million Other Things")
    expect(Utilities::Titleizer.titleize("journey: greatest hits")).to eq("Journey: Greatest Hits")
    expect(Utilities::Titleizer.titleize("I NEED A DOCTOR")).to eq("I Need a Doctor")
    expect(Utilities::Titleizer.titleize("just the way you are")).to eq("Just the Way You Are")
    expect(Utilities::Titleizer.titleize("WAITING FOR THE END")).to eq("Waiting for the End")
    expect(Utilities::Titleizer.titleize("the ballad of mona lisa")).to eq("The Ballad of Mona Lisa")
    expect(Utilities::Titleizer.titleize("LOST IN A PAIR OF EYES")).to eq("Lost in a Pair of Eyes")
    expect(Utilities::Titleizer.titleize("i got the - single")).to eq("I Got The - Single")
    expect(Utilities::Titleizer.titleize("THE LOVE I’M SEARCHING FOR")).to eq("The Love I’m Searching For")
    expect(Utilities::Titleizer.titleize("THE LOVE I'M SEARCHING FOR")).to eq("The Love I'm Searching For")
    expect(Utilities::Titleizer.titleize("the love i'm searching for")).to eq("The Love I'm Searching For")
  end

it "should handle articles and prepositions" do
    expect(Utilities::Titleizer.titleize("what the world is waiting for")).to eq("What the World Is Waiting For")
    expect(Utilities::Titleizer.titleize("What The World Is Waiting For")).to eq("What the World Is Waiting For")
    expect(Utilities::Titleizer.titleize("Homecoming/the Death Of St. Jimmy/East 12th st./Nobody Likes You/Rock And Roll Girlfriend/We're Coming Home again")).to eq("Homecoming/The Death of St. Jimmy/East 12th St./Nobody Likes You/Rock and Roll Girlfriend/We're Coming Home Again")
    expect(Utilities::Titleizer.titleize("under The paving stones: the Beach")).to eq("Under the Paving Stones: The Beach")
    expect(Utilities::Titleizer.titleize("She's A Yo-yo")).to eq("She's a Yo-Yo")
    expect(Utilities::Titleizer.titleize("Rock and/or roll")).to eq("Rock And/Or Roll")
    expect(Utilities::Titleizer.titleize("Promised Land (Marching On)")).to eq("Promised Land (Marching On)")
end

it "should handle parentheses and brackets" do
    expect(Utilities::Titleizer.titleize("Eye of the Hurricane (the Hurricane)")).to eq("Eye of the Hurricane (The Hurricane)")
end

  it "should capitalize first and last name" do
    expect(Utilities::Titleizer.titleize("steve jobs")).to eq("Steve Jobs")
  end

  it "should not split up camelcase" do
    expect(Utilities::Titleizer.titleize("GogoBobo TuneCore")).to eq("GogoBobo TuneCore")
  end
end

describe Utilities::Titleizer, "when dealing with odd characters marks" do
  it "should leave a possessive apostrophe alone" do
    expect(Utilities::Titleizer.titleize("peter's music")).to eq("Peter's Music")
  end

  it "should handle quote marks" do
    expect(Utilities::Titleizer.titleize('"Hey, sexy lady"')).to eq('"Hey, Sexy Lady"')
  end

  it "should handle hyphens" do
    expect(Utilities::Titleizer.titleize("JUST ANOTHER RUN-OF-THE-MILL DAY")).to eq("Just Another Run-of-the-Mill Day")
  end

  it "should ignore words with periods" do
    expect(Utilities::Titleizer.titleize("D.J. mister")).to eq("D.J. Mister")
    expect(Utilities::Titleizer.titleize("D.j. mister")).to eq("D.J. Mister")
  end

  it "should handle enclosures" do
    expect(Utilities::Titleizer.titleize("(Don't Go Back to) Rockville")).to eq("(Don't Go Back To) Rockville")
    expect(Utilities::Titleizer.titleize("Jed {Oingo}")).to eq("Jed {Oingo}")
    expect(Utilities::Titleizer.titleize("Jed [Oingo]")).to eq("Jed [Oingo]")
    Utilities::Titleizer.titleize("beethoven (i love to)") == "Beethoven (I Love To)"
  end

  it 'should handle acronyms' do
    expect(Utilities::Titleizer.titleize("O.U.C.H")).to eq("O.U.C.H")
  end

  context "when the name is cyrillic" do 
    it "captitalizes the word" do 
      expect(Utilities::Titleizer.titleize("фэтмен")).to eq("Фэтмен")
      expect(Utilities::Titleizer.titleize("познакомиться")).to eq("Познакомиться")
    end 
    
    context "when the name is more than one word" do 
      it "will only capitalize the first word" do 
        expect(Utilities::Titleizer.titleize("pтмен mария")).to eq("Pтмен mария")
        expect(Utilities::Titleizer.titleize("говорите помедленнее")).to eq("Говорите помедленнее")
      end  
    end
  end 
end

describe Utilities::Titleizer, "when dealing with roman numerals" do

  it "should handle I, V, L, M, C" do
    expect(Utilities::Titleizer.titleize("I")).to eq("I")
    expect(Utilities::Titleizer.titleize("V")).to eq("V")
    expect(Utilities::Titleizer.titleize("L")).to eq("L")
    expect(Utilities::Titleizer.titleize("M")).to eq("M")
    expect(Utilities::Titleizer.titleize("C")).to eq("C")
    expect(Utilities::Titleizer.titleize("Ghosts I-IV")).to eq("Ghosts I-IV")
  end

  it "should handle XI, LC, III" do
    expect(Utilities::Titleizer.titleize("XI")).to eq("XI")
    expect(Utilities::Titleizer.titleize("III")).to eq("III")
    expect(Utilities::Titleizer.titleize("king henry the XIII")).to eq("King Henry the XIII")
  end
end

describe Utilities::Titleizer, "when dealing with interCapped words" do
  it "should handle Scottish and Irish" do
    expect(Utilities::Titleizer.titleize("McDonalds")).to eq("McDonalds")
    expect(Utilities::Titleizer.titleize("MacCary")).to eq("MacCary")
  end
  it "should handle TuneCore and iTunes" do
    expect(Utilities::Titleizer.titleize("TuneCore AND iTunes")).to eq("TuneCore and iTunes")
  end
end

describe Utilities::Titleizer, "when dealing with items on the common capitalized list" do
  it "should handle TV" do
    expect(Utilities::Titleizer.titleize('TV')).to eq('TV')
    expect(Utilities::Titleizer.titleize('tv')).to eq('TV')
    expect(Utilities::Titleizer.titleize('Tv')).to eq('TV')
  end
  it "should handle DJ" do
    expect(Utilities::Titleizer.titleize('DJ')).to eq('DJ')
    expect(Utilities::Titleizer.titleize('dj')).to eq('DJ')
    expect(Utilities::Titleizer.titleize('Dj')).to eq('DJ')
  end
end

describe Utilities::Titleizer, "when formatting the featured artist strings" do
  before(:each) do
    @album = create(:album, name: 'what the world is waiting for')
  end

  it "should properly format 1 featured artists" do
    @album.add_featured_artist('the Who')
    expect(Utilities::Titleizer.titleize_with_featured_artists(@album)).to eq("What the World Is Waiting For (feat. the Who)")
  end

  it "should properly format 2 featured artists" do
    @album.add_featured_artist('the Who')
    @album.add_featured_artist('the Jam')
    expect(Utilities::Titleizer.titleize_with_featured_artists(@album)).to eq("What the World Is Waiting For (feat. the Who & the Jam)")
  end

  it "should properly format 3 featured artists" do
    @album.add_featured_artist('Earth, Wind & Fire')
    @album.add_featured_artist('the Clash')
    @album.add_featured_artist('Del tha Funky Homosapien')
    expect(Utilities::Titleizer.titleize_with_featured_artists(@album)).to eq("What the World Is Waiting For (feat. Earth, Wind & Fire, the Clash & Del tha Funky Homosapien)")
  end

  it "should properly format 4 featured artists" do
    @album.add_featured_artist('Marcus')
    @album.add_featured_artist('Ewald')
    @album.add_featured_artist('Taylor')
    @album.add_featured_artist('Alex')
    expect(Utilities::Titleizer.titleize_with_featured_artists(@album)).to eq("What the World Is Waiting For (feat. Marcus, Ewald, Taylor & Alex)")
  end

  it "should use parentheses with brackets if there is already a parenthetical immediately before" do
    @album.name = "good girls don't (extended remix)"
    @album.add_featured_artist('the knack')
    expect(Utilities::Titleizer.titleize_with_featured_artists(@album)).to eq("Good Girls Don't (Extended Remix) [feat. the knack]")
  end
end
