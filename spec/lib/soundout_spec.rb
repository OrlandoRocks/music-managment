require "rails_helper"

describe Soundout do

  describe "#order_report" do

    before(:each) do
      @soundout_report = create(:soundout_report, :with_track)
      allow(@soundout_report.track).to receive_message_chain([:artist,:name]).and_return("JJ")
      allow(@soundout_report.track).to receive(:s3_url).and_return("www.awesome.com")
      allow(@soundout_report.track).to receive(:streaming_url).and_return("www.awesome.com")
    end

    context "when the report submits successfully" do

      it "should send a post to soundout and return report id" do
        report_id = "123456"
        expect(HTTParty).to receive(:post).and_return("{SUCCESS: true, reportId: #{report_id}}")
        expect(Soundout.order_report(@soundout_report)).to eq(report_id)
      end

      it "should handle song_library_uploads properly" do
        @soundout_report.track = FactoryBot.create(:song_library_upload, person: Person.first, duration:90)
        allow(@soundout_report.track).to receive(:soundout_url).and_return("www.awesome.com")
        post_body = {
          "authId"        => SOUNDOUT_CONFIG['AUTH_ID'],
          "authKey"       => SOUNDOUT_CONFIG['ACCESS_KEY'],
          "prid"          => "tc.#{@soundout_report.id}",
          "userEmail"     => SOUNDOUT_CONFIG['USER_EMAIL'],
          "userId"        => SOUNDOUT_CONFIG['USER_ID'],
          "trackTitle"    => @soundout_report.track.name,
          "trackArtist"   => @soundout_report.track.artist_name,
          "trackGenre"    => @soundout_report.track.genre.name,
          "trackStream"   => CGI.escape(@soundout_report.track.soundout_url),
          "reviewTotal"   => @soundout_report.soundout_product.number_of_reviews.to_s,
          "reviewSpeed"   => "1",
          "trackDuration" => (@soundout_report.track.duration * 1000).to_s
        }
        query_string = post_body.map{|k,v| "#{CGI.escape(k)}=#{CGI.escape(v)}"}.join("&")

        report_id = "123456"
        expect(HTTParty).to receive(:post).with("http://localhost:3001?accessKey=SOUNDOUT", body: query_string, headers: { "Content-Length" => query_string.length.to_s, "Content-Type" => "application/x-www-form-urlencoded" }).and_return("{SUCCESS: true, reportId: #{report_id}}")
        expect(Soundout.order_report(@soundout_report)).to eq(report_id)
      end

    end

    context "when soundout returns a failure" do

      it "should send a post to soundout and return false" do
        expect(HTTParty).to receive(:post).and_return("{FAILURE: true")
        expect(Soundout.order_report(@soundout_report)).to eq(false)
      end
    end

    context "when the soundout request times out" do

      it "should return false" do
        expect(HTTParty).to receive(:post).and_raise(StandardError)
        expect(Soundout.order_report(@soundout_report)).to eq(false)
      end

    end
  end

  describe "#fetch_report" do

    context "when soundout returns a COMPLETE report" do

      it "should return status and raw response" do
        json = {status: true}
        expect(HTTParty).to receive(:post).and_return(json.to_json)
        status, data = Soundout.fetch_report("123")

        expect(status).to be_truthy
        expect(data).to eq(json.to_json)
      end

    end

    context "when soundout returns an unparsable json" do

      it "should return false and raw_response" do
        json = " { bad ,,, }"
        expect(HTTParty).to receive(:post).and_return(json)
        status, data = Soundout.fetch_report("123")

        expect(status).to be_falsey
        expect(data).to eq(json)
      end

    end

    context "when soundout returns a FAILURE status" do

      it "should return false and raw response" do
        json = {status: { result: "FAILURE" }}
        expect(HTTParty).to receive(:post).and_return(json.to_json)
        status, data = Soundout.fetch_report("123")

        expect(status).to be_falsey
        expect(data).to eq(json.to_json)
      end

    end

    context "when soundout timesout" do

      it "should return false" do
        expect(HTTParty).to receive(:post).and_raise(StandardError)
        status, data = Soundout.fetch_report("123")
        expect(status).to be_falsey
      end

    end

  end

end
