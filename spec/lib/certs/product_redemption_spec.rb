require "rails_helper"

describe Tunecore::Certs::ProductRedemption do
  describe "When verify" do
    before(:each) do
      @person = TCFactory.create(:person)

      @valid_products = {}
      Tunecore::CertEngine::GcDistro::REDEEMABLE_PRODUCTS.each do |k, v|
        @valid_products[k] = Product.find(v)
      end
    end

    context "when product redemption flow" do
      it "should add the valid redemption product to cart" do
        cert = create_cert({ engine_params: 'album' }, cert: 'albumcert')
        expect(@person.purchases).to be_empty
        expect(Product).to receive(:add_to_cart).with(@person, cert.engine.redeemable_product).and_call_original
        expect { Cert.verify verify_opts(entered_code: 'albumcert') }.to change { @person.reload.purchases.count }.by(1)
        expect(@person.purchases.first.product).to eq(@valid_products[:album])
      end

      it "should apply discount to purchase" do
        @valid_products[:album].update_attribute(:price, 49.99)
        cert = create_cert({ engine_params: 'album' }, cert: 'albumcert')
        Cert.verify verify_opts(product_redemption: true, entered_code: 'albumcert')
        expect(@person.purchases.first.discount_cents).to eq(cert.reload.last_discount_amount_cents)
      end

      it "should increment failed redemption count when code is invalid" do
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        expect { Cert.verify verify_opts(entered_code: 'INVALIDCERT') }.to change { @person.reload.redemption_attempts }.by(1)
      end

      it "should not increment failed redemption count when code has been used" do
        cert = create_cert({ engine_params: 'album' }, cert: "USEDCERT", date_used: Time.now)
        expect { Cert.verify verify_opts(entered_code: 'USEDCERT') }.not_to change { @person.reload.redemption_attempts }
      end

      it "should return string message when code is invalid" do
        verified = Cert.verify verify_opts(entered_code: 'INVALIDCERT')
        expect(verified).to match(/If you tried to use a promotion code,  please enter that at check out/)
      end

      it "should return 'promotion code not for redemption' message when using non-redemption cert" do
        cert = create_cert({ cert_engine: 'DefaultPercent', engine_params: '10' }, cert: 'VALIDCERT1')
        verified = Cert.verify verify_opts(entered_code: 'VALIDCERT1')
        expect(verified).to match(/If you tried to use a promotion code,  please enter that at check out/)
      end

      it "should rollback purchase when cert has already been used" do
        cert = create_cert({ engine_params: 'album' }, cert: "USEDCERT", date_used: Time.now)
        expect(@person.purchases).to be_empty
        verified = Cert.verify verify_opts(entered_code: 'USEDCERT')
        expect(verified).to match(/already used/)
        expect(@person.reload.purchases).to be_empty
      end

      it "should rollback purchase when cert code is valid but failed cert verification" do
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        allow(cert).to receive(:verify).and_return('A message')
        allow(Cert).to receive(:find_by).and_return(cert)

        expect(@person.purchases).to be_empty
        Cert.verify verify_opts(entered_code: 'VALIDCERT1')
        expect(@person.reload.purchases).to be_empty
      end

      it "should return string message when product is not available for the country" do
        @person = TCFactory.build_person(country: "CA", postal_code: 'E4Y2E4')
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        expect(@person.purchases).to be_empty
        verified = Cert.verify verify_opts(entered_code: 'VALIDCERT1')
        expect(verified).to match(/not available for your country/)
        expect(@person.reload.purchases).to be_empty
      end

      # it "should check existence of cert only once when cert is valid" do
      #   cert = create_cert({:engine_params => 'album'}, {:cert => "VALIDCERT1"})
      #   Cert.should_receive(:find_by_cert).once.and_return(cert)
      #   verified = Cert.verify verify_opts(:entered_code => 'VALIDCERT1')
      #   verified.should be_a(Cert)
      # end
    end

    context "when regular checkout flow" do
      before(:each) do
        @product = @valid_products[:album]
        @purchase = TCFactory.create(:purchase, product: @product, person: @person, related: @product)
      end

      it "should bypass product redemption flow" do
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        expect(Product).to_not receive(:add_to_cart).with(@person, cert.engine.redeemable_product).and_call_original
        Cert.verify verify_opts(product_redemption: nil, purchase: @purchase, entered_code: 'VALIDCERT1')
      end

      it "should apply discount to purchase" do
        @valid_products[:album].update_attribute(:price, 49.99)
        cert = create_cert({ engine_params: 'album' }, cert: 'albumcert')
        purchase = TCFactory.create(:purchase, product: @valid_products[:album], person: @person, related: @valid_products[:album], currency: 'USD')
        Cert.verify verify_opts(product_redemption: nil, purchase: purchase, entered_code: 'albumcert')
        expect(purchase.reload.discount_cents).to eq(cert.reload.last_discount_amount_cents)
      end

      it "should increment failed redemption count when code is invalid" do
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        expect { Cert.verify verify_opts(product_redemption: nil, purchase: @purchase, entered_code: 'INVALIDCERT') }.to change { @person.reload.redemption_attempts }.by(1)
      end

      it "should not increment failed redemption count when code has been used" do
        cert = create_cert({ engine_params: 'album' }, cert: "USEDCERT", date_used: Time.now)
        expect { Cert.verify verify_opts(product_redemption: nil, purchase: @purchase, entered_code: 'USEDCERT') }.not_to change { @person.reload.redemption_attempts }
      end

      it "should not return 'promotion code not for redemption' message when using non-redemption cert" do
        cert = create_cert({ cert_engine: 'DefaultPercent', engine_params: '10' }, cert: 'VALIDCERT1')
        verified = Cert.verify verify_opts(product_redemption: nil, purchase: @purchase, entered_code: 'INVALIDCERT1')
        expect(verified).not_to be_a(Cert)
        expect(verified).not_to match(/If you tried to use a promotion code,  please enter that at check out/)
      end

      it "should check existence of cert only once when cert is valid" do
        cert = create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
        allow(Cert).to receive(:find_by).once.and_return(cert)

        verified = Cert.verify verify_opts(product_redemption: nil, purchase: @purchase, entered_code: 'VALIDCERT1')
        expect(verified).to be_a(Cert)
      end
    end
  end

  describe "When redeem!" do
    before(:each) do
      @person = create(:person)

      @valid_products = {}
      Tunecore::CertEngine::GcDistro::REDEEMABLE_PRODUCTS.each do |k, v|
        @valid_products[k] = Product.find(v)
      end
      create_cert({ engine_params: 'album' }, cert: "VALIDCERT1")
      @cert = Cert.verify verify_opts(entered_code: 'VALIDCERT1')
    end

    it "should create and settle the invoice" do
      expect(@cert.purchase).not_to be_nil
      @cert.redeem!
      expect(@cert.purchase.invoice.settled?).to be_truthy
    end

    context "when invoice settlement failed" do
      before(:each) do
        @invoice = TCFactory.build(:invoice, settled_at: nil, person: @person)
        expect(Invoice).to receive(:factory).and_return(@invoice)
        expect(@invoice).to receive(:settled!).and_raise(Exception)
      end

      it "should raise exception when invoice settlement failed" do
        expect { @cert.redeem! }.to raise_error
      end

      it "should keep purchase and cert intact when invoice settlement failed" do
        expect(@person.purchases).not_to be_empty
        expect { @cert.redeem! }.to raise_error
        expect(@invoice.settled?).to be_falsey

        purchases = Purchase.unpaid.where(person_id: @person.id).where(product_id: @valid_products[:album].id)

        expect(purchases.first).to eq(@cert.purchase)
      end
    end
  end

  def create_cert(cert_batch_options ={}, cert_options = {})
    @cert_batch = TCFactory.build(:cert_batch, {
      cert_engine: 'GcDistro',
      spawning_code: nil,
      brand_code: 'GCDIST',
      engine_params: 'album'
    }.merge(cert_batch_options))

    cert = Cert.create({
      cert_engine: @cert_batch.cert_engine,
      cert: 'GCDIST1234',
      expiry_date: 30.days.from_now,
      engine_params: @cert_batch.engine_params,
      person_id: nil,
      album_id: nil,
      total_amount: nil,
      cert_batch: @cert_batch
    }.merge(cert_options))
    cert
  end

  def verify_opts(options={})
    {
      product_redemption: true,
      entered_code: '0001',
      current_user: @person
    }.merge(options)
  end
end
