require "rails_helper"

describe Gtm::Page do
  describe "#user_data" do
    let(:person)   { create(:person) }
    let(:invoice)  { create(:invoice, person: person) }
    let(:album)    { create(:album, person: person) }
    let(:purchase) { create(:purchase, person: person) }

    it "builds user data for generic page for user without dist credits" do
      user_data = Gtm::Page.new(person, currency: "USD").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        customer_id: person.id.to_s,
        customer_email: person.email,
        customer_country: person.country,
        has_credits: "0",
        dashboard_state: "virgin",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for generic page for user with dist credits" do
      person_w_credits = create(:person, :with_distribution_credits)
      user_data = Gtm::Page.new(person_w_credits, currency: "USD").user_data
      expected_user_data = {
        currency_code: person_w_credits.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        customer_id: person_w_credits.id.to_s,
        customer_email: person_w_credits.email,
        customer_country: person_w_credits.country,
        has_credits: "1",
        dashboard_state: "virgin",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for dashboard" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Dashboard, page_name: "test").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        page_type: :dashboard,
        page_name: "test",
        dashboard_state: "virgin",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        customer_id: person.id.to_s,
        customer_email: person.email,
        customer_country: person.country,
        has_credits: "0",
        publishing_customer: "0",
        distribution_customer: "0",
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    context "plans user" do
      before { create(:person_plan, person: person, plan: Plan.first) }

      it "builds user data for plans" do
        user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Plans, page_name: "Plans").user_data

        [
          :current_plan_id,
          :current_plan_name,
          :next_eligible_plan_id,
          :next_eligible_plan_name
        ].each { |attr| expect(user_data[attr]).to be_truthy }
      end
    end

    it "builds user data for dashboard with facebook signup" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Dashboard, page_name: "test", facebook_sign_up: 1).user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        page_type: :dashboard,
        page_name: "test",
        dashboard_state: "virgin",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        customer_id: person.id.to_s,
        customer_email: person.email,
        customer_country: person.country,
        has_credits: "0",
        facebook_sign_up: "1",
        publishing_customer: "0",
        distribution_customer: "0",
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for invoice" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Invoice, page_name: "test", invoice: invoice).user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        page_type: :invoice,
        page_name: "test",
        dashboard_state: "virgin",
        has_credits: "0",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        customer_id: person.id.to_s,
        customer_email: person.email,
        customer_country: person.country,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0",
        contains_distribution_product: "0",
        first_time_distribution: "0",
        publishing_customer: "0",
        distribution_customer: "0",
        publishing_only_customer: "0",
        customer_state: "unknown",
        customer_city: "unknown",
        customer_phone: "+12125555555",
        customer_postal_code: person.postal_code,
        order_discount_amount: "0.0",
        order_grand_total: "0.0",
        order_id: invoice.id.to_s,
        order_payment_type: [],
        product_brand: [],
        product_category: [],
        product_id: [],
        product_name: [],
        product_price: [],
        product_quantity: [],
        product_discount: [],
        ecommerce: {
          purchase: {
            "actionField" => {
              id: invoice.id.to_s,
              revenue: "0.0"
            },
            products: []
          }
        }
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for dashboard_first_login" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Dashboard, page_name: "test", pb: "fl").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        page_type: "dashboard/first login",
        page_name: "test",
        dashboard_state: "virgin",
        has_credits: "0",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        customer_id: person.id.to_s,
        customer_email: person.email,
        customer_country: person.country,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for home" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Home, page_name: "About SoundOut").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        customer_country: person.country,
        customer_email: person.email,
        customer_id: person.id.to_s,
        dashboard_state: "virgin",
        has_credits: "0",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        page_name: "About SoundOut",
        page_type: :home,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for login" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Login, page_name: "Login Page").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        customer_country: person.country,
        customer_email: person.email,
        customer_id: person.id.to_s,
        dashboard_state: "virgin",
        has_credits: "0",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        page_name: "Login Page",
        page_type: "Login Page",
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }
      expect(user_data).to eq expected_user_data
    end

    it "builds user data for signup" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::SignUp, page_name: "Sign Up Page").user_data
      expected_user_data = {
        currency_code: person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        customer_country: person.country,
        customer_email: person.email,
        customer_id: person.id.to_s,
        dashboard_state: "virgin",
        has_credits: "0",
        logged_in: "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        page_name: "Sign Up Page",
        page_type: :signup,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for cart with purchases" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Cart, page_name: "Cart", purchases: person.purchases).user_data
      expected_user_data = {
        :currency_code => person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        :customer_country => person.country,
        :customer_email => person.email,
        :customer_id => person.id.to_s,
        :dashboard_state => "virgin",
        :has_credits => "0",
        :logged_in => "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        :order_grand_total => (person.purchases.first.to_f / 100).to_s,
        :page_name => "Cart",
        :page_type => :cart,
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    it "builds user data for session cookie" do
      user_data = Gtm::Page.new(person, currency: "USD", page_type: Gtm::Cookie).user_data
      expected_user_data = {
        :currency_code => person.person_balance.currency,
        current_plan_id: nil,
        current_plan_name: nil,
        :customer_country => person.country,
        :customer_email => person.email,
        :customer_id => person.id.to_s,
        :dashboard_state => "virgin",
        :has_credits => "0",
        :logged_in => "1",
        next_eligible_plan_id: nil,
        next_eligible_plan_name: nil,
        :country_website_id => person.country_website_id,
        :publishing_customer => person.paid_for_composer? ? "1" : "0",
        :distribution_customer => person.has_finalized_release? ? "1" : "0",
        :join_date => person.created_on.strftime("%Y-%m-%d"),
        accessed_social: false,
        social_plan: [nil, nil],
        any_release_without_YTAT: "0"
      }

      expect(user_data).to eq expected_user_data
    end

    context "when the user has accessed tc-social" do
      it "sets accessed_social value to true" do
        create(:oauth_token, :for_tc_social, user: person)
        user_data = Gtm::Page.new(person).user_data

        expect(user_data[:accessed_social]).to be_truthy
      end
    end
  end
end
