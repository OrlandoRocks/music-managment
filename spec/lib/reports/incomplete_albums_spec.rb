require "rails_helper"

describe Tunecore::Reports::IncompleteAlbums do
  it "should return albums based on date" do
    Album.joins(:person).destroy_all
    create(:album, created_on: '2020-01-05')
    album1 = create(:album, created_on: '2020-01-05')
    song1 = create(:song, album: album1, last_errors: 'Your WAV file is corrupt and cannot be parsed')
    song2 = create(:song, album: album1, last_errors: 'Your WAV file is corrupt and cannot be parsed')
    album2 = create(:album, created_on: '2020-01-05')
    song3 = create(:song, album: album2, last_errors: 'Your WAV file is corrupt and cannot be parsed')
    params = {"name"=>"albums_added", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2020", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"1", "genre_id"=>"1"}
    result = Tunecore::Reports::IncompleteAlbums.new(params).calculate!
    expect(result.first['incomplete_rate']).to eq('100.00%')
    expect(result.first['bad_songs_rate']).to eq('66.67%')
    expect(result.first['albums']).to eq(3)
    expect(result.first['bad_songs'].to_s).to eq('2')
  end
end
