# 2009-11-24 -- MJL -- Actually creating some tests that test things! Putting in tests for new and returning customers.
#                      still need lots of other tests....

require "rails_helper"

describe Tunecore::Reports::ProductSales, "when calculating returning vs. new customers" do
  before(:each) do
    Invoice.delete_all
    @customer1 = TCFactory.build_person(:email => 'customer1@tunecore.com')

    @report = Tunecore::Reports::ProductSales.create!(:name => 'Product Sales', :start_on => Date.today - 3, :resolution => [:day])
  end

  it "should record the customer as new if this is their first paid invoice" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)

    @report.record
    expect(@report.todays_row['new customers']).to eq(1)
  end

  it "should record the customer as new if they have multiple paid invoices, but today is the first paid invoice" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)

    @report.record
    expect(@report.todays_row['new customers']).to eq(1)
    expect(@report.todays_row['returning customers']).to eq(0)
  end

  it "should record the customer as new if they have multiple invoices (unpaid), but today one of them is paid for" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => nil)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => nil)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)

    @report.record
    expect(@report.todays_row['new customers']).to eq(1)
    expect(@report.todays_row['returning customers']).to eq(0)
  end

  it "should record the customer as returning if they have a paid invoice before today" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Date.today - 2)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)

    @report.record
    expect(@report.todays_row['new customers']).to eq(0)
    expect(@report.todays_row['returning customers']).to eq(1)
  end

  it "should record the customer as new if they have a first invoice paid for today but they also have an invoice paid for in the future (for forced recalculations)" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Date.today + 5)

    @report.record
    expect(@report.todays_row['new customers']).to eq(1)
    expect(@report.todays_row['returning customers']).to eq(0)
  end

  it "should record a new and returning customer if one user has paid first today (with other unpaid invoices) and customer 2 has paid invoices (past, present and future)" do
    TCFactory.create(:invoice, :person => @customer1, :settled_at => nil)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => nil)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Time.now)
    TCFactory.create(:invoice, :person => @customer1, :settled_at => Date.today + 5)

    @customer2 = TCFactory.build_person(:email => 'customer2@tunecore.com')
    TCFactory.create(:invoice, :person => @customer2, :settled_at => Date.today - 2)
    TCFactory.create(:invoice, :person => @customer2, :settled_at => Time.now)
    TCFactory.create(:invoice, :person => @customer2, :settled_at => Date.today + 4)

    @report.record
    expect(@report.todays_row['new customers']).to eq(1)
    expect(@report.todays_row['returning customers']).to eq(1)
  end

end
