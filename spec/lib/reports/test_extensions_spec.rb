# 2009-12-01 -- MJL -- Adding tests for the data method.

require "rails_helper"

# This file is used to test methods that have to be overriden from DataReport.
#
# ===================================================
# = The TestExtensions class overrides the required =
# = methods of a generic test                       =
# ===================================================
class TestExtensions < DataReport
  def build_records(resolution, start_date, stop_date)
    create_record(resolution, start_date, [[15, 'Annual Renewals created']])
  end
end


# ==================================
# = Test for a generic data report =
# ==================================
describe TestExtensions do
  it do
    @report = TestExtensions.create!(:name => 'AnnualRenewals', :start_on => Date.today << 60, :resolution => [:day])
    expect(@report).to be_valid
  end
end


describe TestExtensions, "when creating records for various resolutions" do

  it "should return 1 record when :live" do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:live], :start_on => '2008-01-01')
    expect(dr.record('2008-03-01'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(1)
  end

  it 'should return 356 records for a non-leap year when set to :day' do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:day], :start_on => '2007-01-01')
    expect(dr.record('2007-12-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(365)
  end

  it 'should return 53 weeks' do  #returns 53 because the 31st starts a new week
    dr = TestExtensions.create!(:name => 'test', :resolution => [:week], :start_on => '2007-01-01')
    expect(dr.record('2007-12-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(53)
  end

  it 'should return 12 records for months' do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:month], :start_on => '2007-01-01')
    expect(dr.record('2007-12-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(12)
  end

  it "should return 4 record when the year is one year later for :quarter resolution" do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:quarter], :start_on => '2007-01-01')
    expect(dr.record('2007-12-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(4)
  end

  it "should return 1 record when the year is one year later for :year resolution" do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:year], :start_on => '2007-01-01')
    expect(dr.record('2007-12-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(1)
  end

  it 'should return 1 live, 5 weeks, 1 month if set to go between 2008,01,01 and 2008,02,01' do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:live, :week, :month], :start_on => '2007-01-01')
    expect(dr.record('2007-01-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(7)
  end

  it "should not make more records if the last_calculated_on is reset" do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:live, :week, :month], :start_on => '2007-01-01')
    expect(dr.record('2007-01-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(7)

    #reset
    dr.update_attribute(:last_calculated_on, nil)

    expect(dr.record('2007-01-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(7)
  end

  it "it should add 32 records if the last_calculated_on is reset and :day is added to the resolution" do
    dr = TestExtensions.create!(:name => 'test', :resolution => [:live, :week, :month], :start_on => '2007-01-01')
    expect(dr.record('2007-01-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(7)

    #reset
    dr.update_attribute(:last_calculated_on, nil)
    dr.resolution = [:live, :week, :month, :day]
    dr.save

    expect(dr.record('2007-01-31'.to_date)).to be_truthy
    expect(dr.data_records.size).to eq(38)
  end
end


class DataExtensions < DataReport
  def build_records(resolution, start_date, stop_date)
    create_record(resolution, start_date, [[1.5, 'column 1'], [3000, 'column 2'], [24, 'column 3']])
  end

  def data(resolution, back = 6, columns = [])
    super(resolution, back, columns)
  end
end


describe DataExtensions, "when calling the data method" do
  before(:each) do
    @report = DataExtensions.create!(:name => 'test', :resolution => [:day], :start_on => Date.today - 3)
    @report.record
  end

  it "should raise an error if you request a resolution that is not in the data report" do
    expect { @report.data(:live) }.to raise_error(RuntimeError, /The resolution :live is not recorded for this report/)
  end

  it "should return a set of data rows" do
    @report.data(:day).each do |row|
      expect(row).to be_an_instance_of(DataRow)
    end
  end

  it "should respond to all of the recorded elements (3 pieces are recorded in our test)" do
    record = @report.data(:day).first
    expect(record['column 1']).to eq(1.5)
    expect(record['column 2']).to eq(3000)
    expect(record['column 3']).to eq(24)
  end
end

describe DataExtensions, "when calling the data method with an array of columns" do
  before(:each) do
    report = DataExtensions.create!(:name => 'test', :resolution => [:day], :start_on => Date.today - 3)
    report.record
    @records = report.data(:day, 1, ['column 3', 'column 1'])
  end

  it "should return a set of data rows" do
    @records.each do |record|
      expect(record).to be_an_instance_of(DataRow)
    end
  end

  it "should respond to only the recorded elements passed in as an array" do
    record = @records.first
    expect(record['column 1']).to eq(1.5)
    expect(record['column 2']).to be_nil
    expect(record['column 3']).to eq(24)
  end

  it "should internally keep the records in the order of the array" do
    temp_string = ""
    record = @records.first
    record.each_pair do |key, value|
      temp_string += "key:#{key},value:#{value}|"
    end
    expect(temp_string).to eq("key:column 3,value:24.0|key:column 1,value:1.5|")
  end
end

describe DataExtensions, "when calling the data method with the 'go_back' parameter set" do
  before(:each) do
    @report = DataExtensions.create!(:name => 'test', :resolution => [:day], :start_on => Date.today - 9)
    @report.record
  end

  it "should return 1 record if 1 is passed in" do
    records = @report.data(:day, 1)
    expect(records.size).to eq(1)
  end

  it "should return 7 records if 7 is passed in" do
    records = @report.data(:day, 7)
    expect(records.size).to eq(7)
  end
end

