require "rails_helper"

describe Tunecore::Reports::CurrentLocks do
  describe "#by" do
    it "raises an argument error if it can't find locks by the given input" do
      expect { Tunecore::Reports::CurrentLocks.by(:something_unknown) }
        .to raise_error(ArgumentError, /:something_unknown/)
    end

    it "retuns an empty set of albums when there are no locked albums" do
      person = create(:person)

      expect(Tunecore::Reports::CurrentLocks.by(person)).to be_empty
    end

    it "returns albums locked by a person when passed a Person record" do
      person = create(:person)
      album = create(:album, locked_by_type: "Person", locked_by_id: person.id)

      another_person = create(:person)
      create(:album, locked_by_type: "Person", locked_by_id: another_person.id)

      expect(Tunecore::Reports::CurrentLocks.by(person)).to contain_exactly(album)
    end

    it "returns albums locked by any person when passed :people" do
      person1 = create(:person)
      album1 = create(:album, locked_by_type: "Person", locked_by_id: person1.id)

      person2 = create(:person)
      album2 = create(:album, locked_by_type: "Person", locked_by_id: person2.id)

      expect(Tunecore::Reports::CurrentLocks.by(:people)).to contain_exactly(album1, album2)
    end

    it "returns albums locked by any invoice when passed :invoice" do
      album = create(:album, locked_by_type: "Invoice")

      expect(Tunecore::Reports::CurrentLocks.by(:invoices)).to contain_exactly(album)
    end

    it "returns albums locked by any bundle when passed :bundle" do
      album = create(:album, locked_by_type: "PetriBundle")

      expect(Tunecore::Reports::CurrentLocks.by(:bundles)).to contain_exactly(album)
    end
  end
end
