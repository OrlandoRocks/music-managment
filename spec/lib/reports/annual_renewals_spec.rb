require "rails_helper"

describe Tunecore::Reports::AnnualRenewals do
  it do
    @report = Tunecore::Reports::AnnualRenewals.create!(:name => 'AnnualRenewals', :start_on => Date.today << 60, :resolution => [:day])
    expect(@report).to be_valid
  end
end

describe Tunecore::Reports::AnnualRenewals, "when running report" do
  before(:each) do
    @report = Tunecore::Reports::AnnualRenewals.create!(:name => "Annual Renewals Test", :start_on => '2008-01-01'.to_date, :resolution => [:live, :month])
  end

  it "should make 25 records for live and 3 months of annual renewals figures" do
    expect(@report.record('2008-03-15'.to_date)).to be_truthy
    @report.reload
    expect(@report.data_records.all.size).to eq(124)
  end
end
