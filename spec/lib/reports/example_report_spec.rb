require "rails_helper"

describe Tunecore::Reports::ExampleReport do
  it "should return albums based on date" do
    Album.joins(:person).destroy_all
    create(:album, created_on: '2020-01-01')
    album1 = create(:album, created_on: '2020-01-05')
    album2 = create(:album, created_on: '2020-01-05')
    params = {"name"=>"albums_added", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2020", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"1", "genre_id"=>"1"}
    result = Tunecore::Reports::ExampleReport.new(params).calculate!

    expect(result.count).to eq(2)
  end
end
