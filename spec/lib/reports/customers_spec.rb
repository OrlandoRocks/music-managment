require "rails_helper"

describe Tunecore::Reports::Customers do
  it "should return customers based on date" do
    person = create(:person)
    create(:invoice, person: person, created_at: '2020-01-01', settled_at: Time.now)
    create(:invoice, person: person, created_at: '2020-01-05', settled_at: Time.now)
    create(:invoice, person: person, created_at: '2020-01-06', settled_at: Time.now)
    params = {"name"=>"customers", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2020", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"1", "genre_id"=>"1"}
    result = Tunecore::Reports::Customers.new(params).calculate!
    expect(result.map{|d| d['date'].to_s}).to include('2020-01-05')
  end
end
