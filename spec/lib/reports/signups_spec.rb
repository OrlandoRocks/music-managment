require "rails_helper"

describe Tunecore::Reports::Signups do
  it "should return signups based on date" do
    person1 = create(:person)
    create(:invoice, person: person1, created_at: '2020-01-01', settled_at: Time.now)
    person2 = create(:person)
    create(:invoice, person: person2, created_at: '2020-01-05', settled_at: Time.now)
    person3 = create(:person)
    create(:invoice, person: person3, created_at: '2020-01-05', settled_at: Time.now)
    params = {"name"=>"customers", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2020", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"0", "genre_id"=>"1"}
    result = Tunecore::Reports::Signups.new(params).calculate!
    expect(result.count).to eq(1)
  end
end
