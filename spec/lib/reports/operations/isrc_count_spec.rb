# 2009-11-24 -- MJL -- Creating the initial specification for the IsrcCount report

require "rails_helper"

describe Tunecore::Reports::Operations::IsrcCount, "when recording the number of songs created" do
  it "should return a count of two songs" do
    album = create(:album)

    # Create two new songs
    Song.delete_all
    create(:song, album: album)
    create(:song, album: album)

    # Create Report and record
    @report = Tunecore::Reports::Operations::IsrcCount.new
    @report.save!
    @report.record

    expect(@report.data.first['song count']).to eq(2)
  end
end
