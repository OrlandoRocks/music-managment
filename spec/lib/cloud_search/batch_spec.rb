require "rails_helper"

describe CloudSearch::Batch do
  fixtures :albums

  describe "#initialize" do

    before(:each) do
      @batch = CloudSearch::Batch.new
      allow(@batch).to receive(:upload)
      @batch.do_full_batch
    end

    it "should pull the last add and last delete dates from the db" do
      @batch = CloudSearch::Batch.new
      expect(@batch.last_add_date).not_to be_blank
      expect(@batch.last_delete_date).not_to be_blank
    end

    it "should allow override of the last add and last delete dates" do
      date = Time.now - 10.days
      @batch = CloudSearch::Batch.new(:last_add_date=>date, :last_delete_date=>date)
      expect(@batch.last_add_date).to eq(date)
      expect(@batch.last_delete_date).to eq(date)
    end

    it "should set the version to the current timestamp" do
      @batch = CloudSearch::Batch.new
      expect((@batch.version - Time.now.to_i).abs).to be < 10
    end

    it "should set add and delete date back when force upload" do
      beginning_of_time = Time.parse('1970-01-01 00:00:00')
      @batch = CloudSearch::Batch.new(:force_add=>true, :force_delete=>true)

      expect(@batch.last_add_date).to eq(beginning_of_time)
      expect(@batch.last_delete_date).to eq(beginning_of_time)
    end

    it "should set the last_delete_date as the second to last muma_import date less than the last batch run time" do
      @muma_import2 = MumaImport.create(:import_type=>"daily_csv", :requested_source_updated_at=>Time.now-2.weeks)
      @muma_import1 = MumaImport.create(:import_type=>"daily_csv", :requested_source_updated_at=>Time.now-1.weeks)

      @batch = CloudSearch::Batch.new
      expect(@batch.last_delete_date).not_to be_blank
      expect(@batch.last_delete_date.to_i).to eq(@muma_import2.requested_source_updated_at.to_i)
    end

  end

  describe "#get_songs_to_add" do

    it "should not include songs whose composition is not registered" do
      muma_song = TCFactory.create(:muma_song)

      @batch = CloudSearch::Batch.new
      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).not_to be_truthy

      #Add the registration
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).to be_truthy
    end

    it "should not include songs that are not company code 'TUNE'" do
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      @batch = CloudSearch::Batch.new
      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).to be_truthy

      muma_song.company_code="LOL"
      muma_song.save!

      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).not_to be_truthy
    end

    it "should not include songs that mech_collect_share = 0" do
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      @batch = CloudSearch::Batch.new
      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).to be_truthy

      muma_song.mech_collect_share = 0
      muma_song.save!

      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).not_to be_truthy
    end

    it "should not include ringtones" do
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      @batch = CloudSearch::Batch.new
      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).to be_truthy

      song  = muma_song.tc_song_and_album
      album = song.album
      album.update_attribute(:album_type, "Ringtone")

      @batch.send(:get_songs_to_add)
      expect(@batch.songs_to_add.include?(muma_song.tc_song_and_album)).not_to be_truthy
    end

  end

  describe "#get_songs_to_delete" do

    it "should include songs whose composition is not registered" do
      last_delete_date = Date.today - 1.week
      muma_song = TCFactory.create(:muma_song)

      #Has no registration
      @batch = CloudSearch::Batch.new(:last_delete_date=>last_delete_date)
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_truthy

      #Has registration
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_falsey

      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Split missing'))
      #Has no registration but was updated before last batch delete date so should not be included in delete
      #Because it should have already been deleted
      MumaSongSociety.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, ["Update compositions set updated_at = ?", (last_delete_date-1.day).to_s(:db) ]))
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_falsey

      #Has no registration but was updated after last batch delete date
      MumaSongSociety.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, ["Update compositions set updated_at = ?", (last_delete_date+1.day).to_s(:db) ]))
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_truthy
    end

    it "should include songs that are not company code 'TUNE' since last batch" do
      last_delete_date = Date.today - 1.week
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      #Affiliate is Tune
      @batch = CloudSearch::Batch.new(:last_delete_date=>last_delete_date)
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_falsey

      #Affiliate is not TUNE
      muma_song.update_attribute(:company_code, "BLAH")
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_truthy

      #Affiliate is not TUNE but was updated before last batch date
      MumaSongSociety.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, ["Update muma_songs set updated_at = ?", (last_delete_date-1.day).to_s(:db) ]))
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).not_to be_truthy
    end

    it "should include songs that are mech_collect_share = 0 since last batch" do
      last_delete_date = Date.today - 1.week
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      #Mech collect share is not 0
      @batch = CloudSearch::Batch.new(:last_delete_date=>last_delete_date)
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_falsey

      #Mech collect share is 0
      muma_song.update_attribute(:mech_collect_share, 0)
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).to be_truthy

      #Mech collect share is 0 but was updated before last batch date
      MumaSongSociety.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, ["Update muma_songs set updated_at = ?", (last_delete_date-1.day).to_s(:db) ]))
      @batch.send(:get_songs_to_delete)
      expect(@batch.songs_to_delete.include?(muma_song.tc_song_and_album)).not_to be_truthy
    end

  end

  describe "#upload" do
    it "should write to a tmp file and issue a command" do
      @document_endpoint = ActiveRecord::Base.connection.execute(
        "select document_endpoint from cloud_search_domain"
      ).first[0]
      @muma_song = TCFactory.create(:muma_song)
      @batch = CloudSearch::Batch.new(last_add_date: Date.today - 1.day, last_delete_Date: Date.today - 1.day)
      @command = double("command")
      @file = double("file")

      expect(@file).to receive(:path).at_least(:once).and_return("#{Rails.root}/tmp/cloud_search_batch.sdf")
      expect(Cocaine::CommandLine).to receive(:new)
        .with(
          "curl",
          "-X POST --upload-file :file_path :cloud_search_endpoint " \
          "--header \"Content-Type:application/json\" >> #{Rails.root}/log/#{Rails.env}.log"
        ).and_return(@command)
      expect(@command).to receive(:run)
        .with(file_path: @file.path, cloud_search_endpoint: @document_endpoint, logger: Rails.logger)
      expect(Tempfile).to receive(:new).and_return(@file)
      expect(@file).to receive(:write).with("json string")
      expect(@file).to receive(:flush)
      expect(@file).to receive(:close!)

      @batch.send(:upload, "json string")
    end

  end

  describe "#do_add" do

    before(:each) do
      @muma_song = TCFactory.create(:muma_song)
      @muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))
    end

    it "should upload a json string" do
      now = Time.now
      allow(Time).to receive(:now).and_return(now)

      @batch = CloudSearch::Batch.new(:force_add=>true)
      @songs_to_add = @batch.send(:get_songs_to_add)

      expect(@batch).to receive(:upload).with(ActiveSupport::JSON.encode([CloudSearch::MumaSongSdf.new(@songs_to_add.first, Time.now.to_i, true, :seven_day_popularity=>0).to_hash]))
      @batch.do_add
    end

    it "should save the last batch date" do
      @batch = CloudSearch::Batch.new(:force_add=>true)
      expect(@batch).to receive(:upload).at_least(:once)

      dates = CloudSearch::Batch.last_batch_dates
      expect(dates[:last_add_date]).to be_blank
      expect(dates[:last_delete_date]).to be_blank

      @batch.do_full_batch

      dates = CloudSearch::Batch.last_batch_dates
      expect(dates[:last_add_date]).not_to be_blank
      expect(dates[:last_delete_date]).not_to be_blank
    end

    it "should save the count of songs in cloudsearch" do
      @batch = CloudSearch::Batch.new(:force_add=>true)
      expect(@batch).to receive(:upload)

      expect(CloudSearch::Batch.song_count).to eq(0)

      @batch.do_add

      expect(CloudSearch::Batch.song_count).to eq(1)
    end

  end

  describe "#do_delete" do
    before(:each) do
      now = Time.now
      allow(Time).to receive(:now).and_return(now)
      MumaSong.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, ["update muma_songs set updated_at = ?", (now-1.week).to_s(:db)]))

      @muma_song = TCFactory.create(:muma_song, :mech_collect_share=>0)
    end

    it "should upload a json string" do
      @batch = CloudSearch::Batch.new(:last_delete_date=>Time.now-2.days)
      @songs_to_delete = @batch.send(:get_songs_to_delete)

      expect(@batch).to receive(:upload).with(ActiveSupport::JSON.encode([CloudSearch::MumaSongSdf.new(@songs_to_delete.first, Time.now.to_i, false, :seven_day_popularity=>0).to_hash]))
      @batch.do_delete
    end

  end

  describe "#seven_day_popularity" do
    before(:each) do
      @album = albums(:various_album)

      zip = create(:zipcode)

      tdd = create(:trend_data_detail, zip_code: zip.zipcode, country_code: "US", sale_date: 1.day.ago, album: @album, song: @album.songs.first)
      tds = create(:trend_data_summary, trend_data_detail: tdd)
      tdd.trend_data_summary_id = tds.id
      tdd.save!

      tdd = create(:trend_data_detail, zip_code: zip.zipcode, country_code: "US", sale_date: 1.day.ago, album: @album, song: @album.songs.last, qty: 3)
      tds = create(:trend_data_summary, trend_data_detail: tdd)
      tdd.trend_data_summary_id = tds.id
      tdd.save!
    end

    it "should return a hash of songs and trend quantities" do
      @batch = CloudSearch::Batch.new

      popularity = @batch.send(:get_seven_day_popularity_by_song_id)
      expect(popularity.size).to eq(2)

      expect(popularity[@album.songs.first.id]).to  eq(1)
      expect(popularity[@album.songs.last.id]).to eq(3)
    end

    it "should get popularity for a subset of songs" do
      @batch = CloudSearch::Batch.new

      popularity = @batch.send(:get_seven_day_popularity_by_song_id, :songs=>[@album.songs.first.id])
      expect(popularity.size).to eq(1)
      expect(popularity[@album.songs.first.id]).to  eq(1)
    end
  end
end
