require "rails_helper"

describe CloudSearch::MumaSongSdf do

  it "should initialize and merge muma_song information and tunecore album/song information" do
    muma_song = TCFactory.create(:muma_song)
    muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

    batch = CloudSearch::Batch.new(:force=>true)
    songs_to_add = batch.send(:get_songs_to_add)

    sdf = CloudSearch::MumaSongSdf.new(songs_to_add.first, DateTime.now.to_i, true)
    expect(sdf).to be_valid

    tc_song = muma_song.tc_song_and_album
    tc_album = tc_song.album

    expect(sdf.album_title).to eq(muma_song.album_title)
    expect(sdf.isrc).to eq(muma_song.isrc)
    expect(sdf.upc).to eq(muma_song.album_upc)
    expect(sdf.label_copy).to eq(muma_song.label_copy)
  end

  describe "to_hash" do

    it "should be type 'add' for add sdf's" do
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      batch = CloudSearch::Batch.new(:force=>true)
      songs_to_add = batch.send(:get_songs_to_add)

      sdf = CloudSearch::MumaSongSdf.new(songs_to_add.first, DateTime.now.to_i, true)

      hash = sdf.to_hash
      expect(hash[:type]).to eq("add")
    end

    it "should be type 'delete' for delete sdf's" do
      muma_song = TCFactory.create(:muma_song)
      muma_song.composition.update_attribute(:state, Composition.convert_to_state('Registered'))

      sdf = CloudSearch::MumaSongSdf.new(muma_song.tc_song_and_album, DateTime.now.to_i, false)

      hash = sdf.to_hash
      expect(hash[:type]).to eq("delete")
    end

    it "should add a version" do
      muma_song = TCFactory.create(:muma_song)
      TCFactory.create(:muma_song_society, :muma_song=>muma_song)

      version = DateTime.now.to_i
      sdf = CloudSearch::MumaSongSdf.new(muma_song.tc_song_and_album, version, false)

      hash = sdf.to_hash
      expect(hash[:version]).to eq(version)
    end
  end
end
