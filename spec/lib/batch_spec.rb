require "rails_helper"

describe "build_and_run" do

  before(:each) do
    mock_braintree_query
    mock_paypal_transaction

    allow(FeatureFlipper).to receive(:show_feature?).and_call_original
    allow(FeatureFlipper).to receive(:show_feature?).with(:try_multiple_payment_methods, anything).and_return(true)

    #Create people
    @credit_card_person  = TCFactory.create(:person)
    @paypal_person       = TCFactory.create(:person)
    @balance_person      = TCFactory.create(:person)
    @cant_process_person = TCFactory.create(:person)

    #Create products to be purchased

    renewal_product   = Product.find(Product::US_TWO_YEAR_RENEWAL_ID)
    product           = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

    product_item      = TCFactory.create(:product_item, :product=>product, :renewal_product=>renewal_product, :renewal_duration=>1, :renewal_interval=>"year", :first_renewal_duration=>1, :first_renewal_interval=>"year")
    product_item_rule = TCFactory.create(:product_item_rule, :inventory_type=>"Album", :product_item=>product_item)

    #Create payment methods
    pp = PersonPreference.new
    pp.person = @credit_card_person
    pp.preferred_credit_card = TCFactory.create(:stored_credit_card, :person=>@credit_card_person)
    pp.save!
    expect(@credit_card_person.renew_with_credit_card?).to eq(true)

    pp = PersonPreference.new(:preferred_payment_type=>"PayPal")
    pp.person = @paypal_person
    pp.preferred_paypal_account = TCFactory.create(:stored_paypal_account, :person=>@paypal_person)
    pp.save!

    expect(@paypal_person.renew_with_paypal?).to eq(true)

    pp = PersonPreference.new
    pp.person = @balance_person
    @balance_person.person_balance.update(:balance=>100)
    expect(@balance_person.renew_with_balance?).to eq(true)

    #Create a renewal to be processed by credit card
    album    = TCFactory.create(:album, :person=>@credit_card_person)
    purchase = Product.add_to_cart(@credit_card_person, album)
    invoice = Invoice.factory(@credit_card_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    #Create a renewal to be processed by paypal
    album    = TCFactory.create(:album, :person=>@paypal_person)
    purchase = Product.add_to_cart(@paypal_person, album)
    invoice = Invoice.factory(@paypal_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    #Create a renewal to be processed by balance
    album    = TCFactory.create(:album, :person=>@balance_person)
    purchase = Product.add_to_cart(@balance_person, album)
    invoice = Invoice.factory(@balance_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    #Create a renewal that can't be processed
    album    = TCFactory.create(:album, :person=>@cant_process_person)
    purchase = Product.add_to_cart(@cant_process_person, album)
    invoice = Invoice.factory(@cant_process_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    allow_any_instance_of(BatchTransaction).to receive(:process_braintree_blue_payment).and_return(FactoryBot.create(:braintree_transaction))
  end

  it "should complete_batch if transactions processed" do
    expect(PaymentBatch.count).to eq(0)
    mock_paypal_transaction('Completed', {'AMT'=>'20.00'})
    mailer = double("mailer")
    expect(BatchSummaryNotifier).to receive(:not_processed_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
    allow(mailer).to receive(:deliver)
    allow(CSV).to receive(:open).and_return("")

    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)

    #Run the batch
    Sidekiq::Testing.inline! do
      Batch.build_and_run(Date.today+1.year, true)
    end
    expect(PaymentBatch.count).to eq(1)

    batch = PaymentBatch.first
    expect(batch.batch_transactions.count).to eq(4)

    #Paypal and balance are processed immediately and now braintree
    expect(batch.batch_transactions.where(processed: "processed").count).to eq(1)

    #Cannot process status
    expect(batch.batch_transactions.where(processed: "cannot_process").count).to eq(3)
  end

  it "should handle invoices from multiple country websites" do
    @ca_person = create(:person, country: "CA", postal_code: "E4Y2E4")
    @ca_person.person_balance.update(balance: 100)
    expect(@ca_person.renew_with_balance?).to eq(true)

    #Create canadian products to be purchased
    renewal_product   = Product.find(Product::CA_TWO_YEAR_RENEWAL_ID)
    product           = Product.find(Product::CA_ONE_YEAR_ALBUM_PRODUCT_ID)

    #Purchase an album with CA person
    album    = create(:album, person: @ca_person)
    purchase = Product.add_to_cart(@ca_person, album)
    invoice = Invoice.factory(@ca_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    expect(PaymentBatch.count).to eq(0)

    mock_paypal_transaction('Completed', {'AMT'=>'20.00'})
    mailer = double("mailer")
    expect(BatchSummaryNotifier).to receive(:not_processed_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
    allow(mailer).to receive(:deliver)
    allow(CSV).to receive(:open).and_return("")

    #Run the batch
    allow_any_instance_of(Album).to receive(:has_ever_been_approved?).and_return(true)
    Sidekiq::Testing.inline! do
      Batch.build_and_run(Date.today+1.year, true)
    end
    expect(PaymentBatch.count).to eq(2)

    batch = PaymentBatch.find_by(country_website_id: CountryWebsite::UNITED_STATES)
    expect(batch.batch_transactions.count).to eq(4)

    #Paypal and balance are processed immediately and now braintree
    expect(batch.batch_transactions.where(processed: "processed").count).to eq(1)

    #Cannot process status
    expect(batch.batch_transactions.where(processed: "cannot_process").count).to eq(3)

    #Canadian batch
    batch = PaymentBatch.find_by(country_website_id: CountryWebsite::CANADA)
    expect(batch.batch_transactions.count).to eq(1)

    #Balance are processed immediately
    expect(batch.batch_transactions.where(processed: "processed").count).to eq(1)
  end
end

describe "build_and_run following days" do

  before(:each) do

    #Create products to be purchased
    renewal_product   = Product.find(Product::US_TWO_YEAR_RENEWAL_ID)
    product           = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)

    #We need an invoice that failed with a purchase. invoice needs to be waiting for batch
    @cant_process_person = TCFactory.create(:person)

    album    = TCFactory.create(:album, :person=>@cant_process_person)
    purchase = Product.add_to_cart(@cant_process_person, album)
    invoice = Invoice.factory(@cant_process_person, [purchase])
    invoice.settlement_received(purchase,purchase.cost_cents)
    invoice.settled!

    purchase = Product.add_to_cart(@cant_process_person, album.renewal, renewal_product)
    invoice = Invoice.factory(@cant_process_person, [purchase], 'waiting_for_batch' )
    allow_any_instance_of(BatchTransaction).to receive(:process_braintree_blue_payment).and_return(FactoryBot.create(:braintree_transaction))
  end

  it  "should pick up failed invoices from previous batches" do

    expect(PaymentBatch.count).to eq(0)

    mailer = double("mailer")
    expect(BatchSummaryNotifier).to receive(:not_processed_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
    allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
    allow(mailer).to receive(:deliver)

    allow(CSV).to receive(:open).and_return("")

    #Run the batch
    Sidekiq::Testing.inline! do
      Batch.build_and_run(Date.today+1.year+1.day, true)
    end
    expect(PaymentBatch.count).to eq(1)

    batch = PaymentBatch.first
    expect(batch.batch_transactions.count).to eq(1)

    #Cannot process status
    expect(batch.batch_transactions.where(processed: "cannot_process").count).to eq(1)

  end

  context "requested plan downgrades integration testing" do

    let!(:person) do 
      person = create(:person, :with_plan_renewal)
      plan_renewal = person.renewals.last
      plan_renewal.takedown_at = nil
      plan_renewal.save!
      person
    end
    let!(:requested_plan) { Plan.first }
    # let!(:renewals) { person.renewals.to_a }
    let!(:downgrade_request) { PlanDowngradeRequest.create(
      person_id: person&.id,
      status: PlanDowngradeRequest::ACTIVE_STATUS,
      reason_id: DowngradeCategoryReason.first.id,
      requested_plan_id: requested_plan&.id
    )}
    it "creates two new PersonPlanHistory records with the expected dates and change types" do
      mock_paypal_transaction('Completed', {'AMT'=>'20.00'})
      mailer = double("mailer")
      expect(BatchSummaryNotifier).to receive(:not_processed_summary).and_return(mailer)
      allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
      allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
      allow(mailer).to receive(:deliver)
      allow(CSV).to receive(:open).and_return("")

      expect(person.renewals.count).to eq(1)
      old_expires_at = person.renewals.first.expires_at
      expect(PersonPlanHistory.where(person:person).count).to eq(0)
      
      #Run the batch
      Sidekiq::Testing.inline! do
        Batch.build_and_run(Date.today+1.year, true)
      end

      expect(PersonPlanHistory.where(person:person).count).to eq(2)
      downgrade_history = PersonPlanHistory.where(person:person).first
      renewal_history = PersonPlanHistory.where(person: person).last
      expect(downgrade_history.change_type).to eq(PersonPlanHistory::REQUESTED_DOWNGRADE)
      expect(downgrade_history.plan_end_date).to eq(old_expires_at + 1.year)
      expect(downgrade_history.plan_start_date).to eq(old_expires_at)
      expect(renewal_history.change_type).to eq(PersonPlanHistory::RENEWAL)
      expect(renewal_history.plan_end_date).to eq(old_expires_at + 1.year)
      expect(renewal_history.plan_start_date).to eq(old_expires_at) 
    end

    it "creates a new renewal history record, extending the renewal by 1 year" do
      mock_paypal_transaction('Completed', {'AMT'=>'20.00'})
      mailer = double("mailer")
      expect(BatchSummaryNotifier).to receive(:not_processed_summary).and_return(mailer)
      allow(BatchSummaryNotifier).to receive(:cc_failure_summary).and_return(mailer)
      allow(BatchSummaryNotifier).to receive(:batch_summary).and_return(mailer)
      allow(mailer).to receive(:deliver)
      allow(CSV).to receive(:open).and_return("")
      
      expect(person.renewals.count).to eq(1)
      old_expires_at = person.renewals.first.expires_at
      expect(person.renewals.first.renewal_history.count).to eq(1)

      #Run the batch
      Sidekiq::Testing.inline! do
        Batch.build_and_run(Date.today+1.year, true)
      end

      expect(person.renewals.count).to eq(1)
      plan_renewal = person.renewals.first
      expect(plan_renewal.expires_at).to eq(old_expires_at + 1.year)
      expect(plan_renewal.renewal_history.count).to eq(2)
      newest_history = plan_renewal.renewal_history.last
      expect(newest_history.starts_at).to eq(old_expires_at)
      expect(newest_history.expires_at).to eq(old_expires_at + 1.year)
    end

  end

end
