require "rails_helper"

describe Odesli::ApiClient do
  describe "#get" do
    it "returns a reponse body" do
      song_url = "#{ENV['SPOTIFY_STORE_BASE_URL']}/track/53A2MsBqPdDzZzP3Dm8rfR"
      odesli_url = "#{ENV['ODESLI_BASE_URL']}?url=#{song_url}"
      expected_body = Faraday.new.get(odesli_url)
      allow_any_instance_of(Faraday::Connection).to receive(:get).and_return(Faraday::Response.new(body: expected_body.body))
      response =  Odesli::ApiClient.new(song_url, ENV['ODESLI_API_KEY']).get
      expect(response).to have_key("entitiesByUniqueId").or eq(nil)
    end
  end
end