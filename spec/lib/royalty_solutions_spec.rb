require "rails_helper"

describe RoyaltySolutions do
  before(:each) do
    @person = FactoryBot.create(:person)
  end
  describe "#validate_assets" do
    before(:each) do
      @album = TCFactory.create_purchaseable_album(person:@person)
    end

    it "returns success on matching" do
      resp = RoyaltySolutions.validate_assets(email: @person.email, upcs: @album.upc.number, isrcs: @album.songs.first.tunecore_isrc)
      expect(resp[:status]).to eq("success")
      expect(resp[:message]).to eq("OK")
    end

    it "returns list of non-matching upcs" do
      resp = RoyaltySolutions.validate_assets(email: @person.email, upcs: "FOOBAR", isrcs: @album.songs.first.tunecore_isrc)
      expect(resp[:status]).to eq("failure")
      expect(resp[:message]).to eq([{:UPCFOOBAR=>"not found"}])
    end

    it "returns list of non-matching isrcs" do
      resp = RoyaltySolutions.validate_assets(email: @person.email, upcs: @album.upc.number, isrcs: "FOOBAR")
      expect(resp[:status]).to eq("failure")
      expect(resp[:message]).to eq([{ :ISRCFOOBAR=>"not found" }])
    end
  end
  describe "#register" do
    before(:each) do
      @album = TCFactory.create_purchaseable_album(person:@person)
    end

    it "returns success on matching and inserts" do
      resp = RoyaltySolutions.register(email: @person.email, upcs: @album.upc.number, isrcs: @album.songs.first.tunecore_isrc)
      expect(resp[:status]).to eq("success")
      expect(resp[:message]).to eq("OK")
      expect(TunecoreTracking.count).to eq(2)
    end

    it "returns list of non-matching upcs and does not insert" do
      resp = RoyaltySolutions.register(email: @person.email, upcs: "FOOBAR", isrcs: @album.songs.first.tunecore_isrc)
      expect(resp[:status]).to eq("failure")
      expect(resp[:message]).to eq([{ :UPCFOOBAR=>"not found" }])
      expect(TunecoreTracking.count).to eq(0)
    end

    it "inserts new tracking records with the provided event_date" do
      event_date = (Date.current - 7.days).to_s
      resp = RoyaltySolutions.register(email: @person.email, upcs: @album.upc.number, isrcs: @album.songs.first.tunecore_isrc, event_date: event_date)
      expect(resp[:status]).to eq("success")
      expect(resp[:message]).to eq("OK")
      expect(TunecoreTracking.count).to eq(2)
      expect(TunecoreTracking.where(event_date: event_date.to_datetime).count).to eq(2)
    end

    it "doesn't insert new tracking records with an invalid event_date and returns a failure response" do
      event_date = "2020-07-33"
      resp = RoyaltySolutions.register(email: @person.email, upcs: @album.upc.number, isrcs: @album.songs.first.tunecore_isrc, event_date: event_date)
      expect(resp[:status]).to eq("failure")
      expect(resp[:message]).to eq("Invalid event_date: 2020-07-33")
      expect(TunecoreTracking.count).to eq(0)
    end
  end
end
