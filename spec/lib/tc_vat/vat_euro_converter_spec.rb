require "rails_helper"

US_ISO = "US".freeze

describe TcVat::VatEuroConverter do
  before(:each) do
    @country = Country.find_by(iso_code: US_ISO)
    @country_website = @country.country_website
    @balance_rate = create(:foreign_exchange_rate,
                           exchange_rate: 0.8,
                           source_currency: CurrencyCodeType::USD,
                           target_currency: CurrencyCodeType::EUR)
    @person = create(:person, country_website: @country_website, country: @country.iso_code)
    @album = create(:album, person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    @purchase.update(vat_amount_cents: 100, paid_at: Time.now)
    @invoice = create(:invoice, person: @person, purchases: [@purchase])

    FeatureFlipper.update_feature(:vat_tax, 100, '')
  end

  describe "#convert_to_euro!" do
    it "adds vat_amount_cents_in_eur to invoice" do
      TcVat::VatEuroConverter.new(invoice: @invoice).convert_to_euro!
      amount_in_euro = @purchase.vat_amount_cents * @balance_rate.exchange_rate

      expect(@invoice.vat_amount_cents_in_eur).to eq amount_in_euro
      expect(@invoice.foreign_exchange_rate_id).to eq @balance_rate.id
    end

    context "without vat_tax feature flipper" do
      it "should not update invoice and purchase" do
        FeatureFlipper.update_feature(:vat_tax, 0, '')
        TcVat::VatEuroConverter.new(invoice: @invoice).convert_to_euro!

        expect(@invoice.vat_amount_cents_in_eur).to eq 0
        expect(@invoice.foreign_exchange_rate_id).to eq nil
      end
    end
  end
end
