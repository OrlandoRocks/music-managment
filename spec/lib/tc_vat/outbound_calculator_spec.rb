require 'rails_helper'

describe TcVat::OutboundVatCalculator do
  before(:each) do
    @person = TCFactory.build_person(name: 'trevor', email: 'trevor@protocool.net',
                                     country: 'Luxembourg')

    allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, @person).and_return(true)
  end

  it 'should provide outbound vat info' do
    stub_vat_response
    tax_info = TcVat::OutboundVatCalculator.new(@person).fetch_vat_info

    expect(tax_info[:tax_rate]).to eq('17.0')
  end

  private

  def stub_vat_response
    allow_any_instance_of(TcVat::Base)
      .to receive(:validate_vat_number)
      .and_return(true)

    allow_any_instance_of(TcVat::Base)
      .to receive(:vat_applicable_countries)
      .and_return({ 'business' => [{ 'place_of_supply' => 'Luxembourg',
                                     'tax_rate' => '17.0',
                                     'country' => 'Luxembourg' }],
                    'individual' => [] })
  end
end
