require 'rails_helper'

describe TcVat::RoyaltyPurchase do
  before(:each) do
    allow_any_instance_of(TcVat::OutboundVatCalculator)
      .to receive(:fetch_vat_info)
      .and_return({ customer_location: 'Luxembourg',
                    vat_registration_number: 'LU22326250',
                    tax_rate: '17.0',
                    place_of_supply: 'Luxembourg' })
    @balance = Cart::Payment::Strategies::Balance.new

    @person = create(:person, :with_balance, amount: 5000.00)
    @person_balance = @person.person_balance
    album = create(:album, person: @person)
    @invoice = create(:invoice, :with_purchase, related: album, person: @person)
    @purchase = @invoice.purchases.first
    @manager = Cart::Payment::Manager.new(@person, [@purchase], "an ip address")
    allow(@manager).to receive(:invoice).and_return(@invoice)
  end

  it "should post vat if outbound vat applicable" do
    royalty_purchase = process_vat(@person, @invoice, @person_balance.balance)

    expect(royalty_purchase.amount).to eq(@invoice.outstanding_amount_cents)
    expect(@person.person_transactions.where(target_type: 'VatTaxAdjustment')).to be_present
  end

  it 'should use all balance if the purchase amount is higher than balance' do
    @person_balance.update(balance: 5)

    royalty_purchase = process_vat(@person, @invoice, @person_balance.balance)

    vat_amount = 500 * 17 / 100
    total_from_balance = 500 + vat_amount

    expect(royalty_purchase.amount).to eq(total_from_balance)
    expect(@person.person_transactions.where(target_type: 'VatTaxAdjustment')).to be_present
  end

  private

  def process_vat(person, invoice, balance)
    royalty_purchase = TcVat::RoyaltyPurchase.new(person, invoice, balance)
    royalty_purchase.process_vat!

    royalty_purchase
  end
end
