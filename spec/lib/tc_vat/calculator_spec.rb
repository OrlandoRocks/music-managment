require "rails_helper"

describe TcVat::Calculator do
  before(:each) do
    TCFactory.build_ad_hoc_album_product
    @person = TCFactory.build_person(name: 'trevor', email: 'trevor@protocool.net', country: 'BE')
    @album = TCFactory.build_album(person: @person)
    @purchase = Product.add_to_cart(@album.person, @album)
    vat_info = @person.build_vat_information(vat_registration_number: 'BE0411905847', vat_registration_status: 'valid')
    vat_info.save(validate: false)
    allow(FeatureFlipper).to receive(:show_feature?).with(:vat_tax, @person).and_return(true)
    set_cache
  end

  it 'should process vat' do
    stub_success
    @purchase.recalculate
    expect(@purchase.purchase_tax_information).to be_present
    vat_amount = ((@purchase.cost_cents - @purchase.discount_cents) * 17.0 / 100).round.to_i
    expect(@purchase.vat_amount_cents).to eq(vat_amount)
    expect(@purchase.purchase_tax_information.tax_rate).to eq(17.0)
  end

  it 'should fetch from vat information in case of vies failure' do
    stub_vies_failure
    @purchase.recalculate
    expect(@purchase.purchase_tax_information).to be_present
    expect(@purchase.purchase_tax_information.error_code).to eq('Vat Service Down - Used cache to calculate VAT')
  end

  it 'should not allow purchase recalculate if the service is down and all fail_over mechanism fails' do
    clear_cache
    stub_vat_down_error
    @person.vat_information.update_column('vat_registration_status', 'invalid')
    @purchase.recalculate

    expect(@purchase.errors[:base].first).to include('Unable to process VAT')
  end

  it 'should fetch vat applicable rates' do
    rate = TcVat::Calculator.fetch_vat_applicable_rate(TcVat::Base::INBOUND, VatInformation::BUSINESS, "Belgium")
    expect(rate).to eql(17.0)
  end

  private

  def stub_success
    allow_any_instance_of(TcVat::ApiClient)
      .to receive(:send_request!)
      .and_return(
        FakeVatApiHelper.generate do |response|
          response.valid           = true
          response.vat_number      = '123'
          response.name            = 'test'
          response.country         = 'Austria'
          response.place_of_supply = 'Luxembourg'
          response.tax_rate        = 17
          response.tax_type        = 'VAT'
          response.error_message   = nil
          response.data            = {}
        end
      )
  end

  def stub_vies_failure
    allow_any_instance_of(TcVat::ApiClient)
      .to receive(:send_request!)
      .and_raise(TcVat::Requests::ViesDownError.new)
  end

  def stub_vat_down_error
    allow_any_instance_of(TcVat::ApiClient)
      .to receive(:send_request!)
      .and_raise(TcVat::Requests::VatServiceDownError.new)
  end

  def set_cache
    data = {
      "business" => [
        { "place_of_supply" => "Luxembourg", "tax_rate" => "17.0", "country" => "Belgium" }
      ],
      "individual" => [
        { "place_of_supply" => "Luxembourg", "tax_rate" => "17.0", "country" => "Belgium" }
      ]
    }
    $redis.set('vat_applicable_countries_inbound', data.to_json)
  end

  def clear_cache
    $redis.del('vat_applicable_countries_inbound')
  end
end
