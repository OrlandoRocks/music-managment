require "rails_helper"

describe TcReporter::ApiClient do
  context "#post" do
    it "returns true when status is 200" do
      mock_response = double("response", status: 200, body: "foo body")
      allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(mock_response)
      expect(TcReporter::ApiClient.post("foo/bar", {}, Person.new)).to eq(true)
    end

    it "returns false for any non 200 status" do
      [500, 404, 401, 302].each do |status|
        mock_response = double("response", status: status, body: "foo body") 
        allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(mock_response)
        expect(TcReporter::ApiClient.post("foo/bar", {}, Person.new)).to eq(false)
      end
    end
  end
end
