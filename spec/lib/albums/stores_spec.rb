require "rails_helper"

describe Album do
  describe "#add_stores" do
    before :each do
      @album = TCFactory.build_album
      @new_store = TCFactory.build_store
      @existing_store = TCFactory.build_store
      FactoryBot.create(:salepointable_store, store: @new_store)
      TCFactory.build_salepoint(:store => @existing_store, :salepointable => @album)
      expect(@album.reload.stores).to eq([@existing_store])
    end

    it "should add stores to the album" do
      @album.add_stores([@new_store])
      expect(@album.reload.salepoints.collect(&:store)).to include(@new_store)
    end

    it "should not add existing salepoints" do
      salepoints, store_not_added = @album.add_stores([@existing_store])
      expect(store_not_added).to eq([@existing_store])
    end

    it "should handle an arrays of existing and new salepoints" do
      salepoints, store_not_added = @album.add_stores([@new_store,@existing_store])
      expect(@album.reload.salepoints.collect(&:store)).to include(@new_store)
      expect(store_not_added).to eq([@existing_store])
    end

    it "should allow attributes override by block" do
      @album.add_stores([@new_store]) do |options|
        options[:store] = @existing_store
        options
      end

      expect(@album.salepoints.collect(&:store)).to_not include(@new_store)
    end
  end

  describe "#salepoints_by_store" do
    before(:each) do
      @store = Store.find_by(abbrev: "ytsr")
      @album = TCFactory.create(:album)
    end

    context "album with no salepoints of ytsr_store" do
      it "should return an empty array" do
        expect(@album.salepoints_by_store(@store)).to eq([])
      end
    end

    context "album with at least 1 salepoint of ytsr_store" do
      before(:each) do
        @salepoint = TCFactory.create(:salepoint, salepointable: @album, store: @store)
      end

      it "should return an array of the ytsr_salepoints" do
        expect(@album.salepoints_by_store(@store)).to eq([@salepoint])
      end
    end
  end

  describe "#paid_stores" do
    let(:album) { create(:album) }

    context "paid_stores is instantiated" do
      it "returns the value from the instance variable" do
        subject_store = Store.first
        album.instance_variable_set(:@paid_stores, [subject_store])

        expect(album.paid_stores).to eq([subject_store])
      end
    end

    context "paid_stores is not instantiated" do
      let(:salepoint) { Salepoint.first }

      before do
        salepoint.store.update_column(:is_active, true)
        salepoint.update(salepointable: album, payment_applied: 1)
      end

      it "returns stores from salepoints" do
        expect(album.paid_stores.length).to eq(1)
      end

      it "should not returns unpaid stores from salepoints" do
        salepoint.update_column(:payment_applied, 0)
        expect(album.paid_stores.length).to eq(0)
      end

      it "returns stores ordered by position" do
        salepoint.store.update_column(:position, 0)
        last_salepoint = Salepoint.last
        last_salepoint.update(salepointable: album, payment_applied: 1)
        last_salepoint.store.update_column(:is_active, true)

        expect(album.paid_stores.last.id).to eq(last_salepoint.store_id)
      end

      context "show_active_stores is true" do
        before { allow(album).to receive(:show_active_stores).and_return(true) }

        it "should not return inactive stores" do
          salepoint.store.update_column(:is_active, false)
          expect(album.paid_stores.length).to be_zero
        end
      end

      context "show_active_stores is false" do
        before { allow(album).to receive(:show_active_stores).and_return(false) }

        it "should return inactive stores" do
          salepoint.store.update_column(:is_active, false)
          expect(album.paid_stores.length).to eq(1)
        end
      end
    end
  end
end
