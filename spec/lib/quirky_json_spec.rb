require 'rails_helper'

describe QuirkyJson do
  describe ".load" do
    it "returns an empty hash when arg is nil" do
      expect(QuirkyJson.load(nil)).to eq({})
    end

    it "returns an empty hash when arg is string 'null'" do
      expect(QuirkyJson.load('null')).to eq({})
    end
    it "returns an empty hash when arg is string '{}'" do
      expect(QuirkyJson.load('{}')).to eq({})
    end

    it "returns an populated hash when arg is json hash" do
      expect(QuirkyJson.load({"stored_bank_account"=>true}.to_json)).to eq({"stored_bank_account"=>true})
    end
  end

  describe ".dump" do
    it "just calls JSON.dump with arg" do
      arg = {"stored_bank_account"=>true}
      expect(QuirkyJson).to receive(:dump).with(arg)
      QuirkyJson.dump(arg)
    end
  end
end
