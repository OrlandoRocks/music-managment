require "rails_helper"

describe GoogleRecaptcha do
  describe ".new" do
    it "initializes the Faraday client with the base URL" do
      expect(Faraday).to receive(:new).with("https://www.google.com/")
      GoogleRecaptcha.new
    end
  end

  describe "#verify" do
    let(:client)           { double("client") }
    let(:failure_resp)     { double(body: {success: false}.to_json) }
    let(:success_resp)     { double(body: {success: true}.to_json) }

    before(:each) do
      allow(Faraday).to receive(:new).and_return(client)
      @google_recaptcha = GoogleRecaptcha.new
    end

    context "success" do
      before do
        allow(client).to receive(:post).with("recaptcha/api/siteverify").and_return(success_resp)
      end
      it "returns true" do
        expect(@google_recaptcha.verify_recaptcha(remoteip: "127.0.0.1", response: "captcha code")).to eq(true)
      end
    end

    context "failure" do
      before do
        allow(client).to receive(:post).with("recaptcha/api/siteverify").and_return(failure_resp)
      end
      it "returns false" do
        expect(@google_recaptcha.verify_recaptcha(remoteip: "127.0.0.1", response: "captcha code")).to eq(false)
      end
    end
  end
end
