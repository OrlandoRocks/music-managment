require "rails_helper"

require "#{Rails.root}/lib/batch/batch_summary"

describe Batch::BatchSummary do

  describe "calculate_by_payment_type" do

    it "should calculate auto_renewed_by_credit_card details for non India renewals (Braintree)" do
      invoice = create(:invoice)
      batch = create(:payment_batch, country_website_id: CountryWebsite::UNITED_STATES, batch_date: Date.today)
      bt_transaction = create(:braintree_transaction, status: 'success', amount: 12.5)
      create(:batch_transaction, invoice: invoice, matching_type: 'BraintreeTransaction', matching_id: bt_transaction.id, payment_batch: batch, amount: 12.5)
      failed_bt_transaction = create(:braintree_transaction, status: 'error', amount: 10)
      create(:batch_transaction, invoice: invoice, matching_type: 'BraintreeTransaction', matching_id: failed_bt_transaction.id, payment_batch: batch, amount: 10)

      batch_summary = Batch::BatchSummary.new(batch)

      expect(batch_summary.auto_renewed_by_credit_card_revenue.to_f).to eq(12.5)
      expect(batch_summary.auto_renewed_by_credit_card).to eq(2)
      expect(batch_summary.auto_renewed_by_credit_card_failures).to eq(1)
    end

    it "should calculate auto_renewed_by_credit_card details for India renewals (PaymentsOS)" do
      invoice = create(:invoice)
      batch = create(:payment_batch, country_website_id: CountryWebsite::INDIA, batch_date: Date.today)
      pos_transaction = create(:payments_os_transaction, charge_status: 'succeed', amount: 1450)
      create(:batch_transaction, invoice: invoice, matching_type: 'PaymentsOSTransaction', matching_id: pos_transaction.id, payment_batch: batch, amount: 14.5)
      failed_pos_transaction = create(:payments_os_transaction, charge_status: 'failed', amount: 1000)
      create(:batch_transaction, invoice: invoice, matching_type: 'PaymentsOSTransaction', matching_id: failed_pos_transaction.id, payment_batch: batch, amount: 10.0)

      batch_summary = Batch::BatchSummary.new(batch)

      expect(batch_summary.auto_renewed_by_credit_card_revenue.to_f).to eq(14.5)
      expect(batch_summary.auto_renewed_by_credit_card).to eq(2)
      expect(batch_summary.auto_renewed_by_credit_card_failures).to eq(1)
    end

  end
  
end
