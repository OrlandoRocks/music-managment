require "rails_helper"

describe "ItunesLinkinator", "when making phobos link from id" do

  it "should make the linkshare link correctly" do
    expect(ItunesLinkinator.linkshare_link_for("1234")).to eq(%Q!http://click.linksynergy.com/fs-bin/stat?id=eH8NOY8fBFE&offerid=78941&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fphobos.apple.com%252FWebObjects%252FMZStore.woa%252Fwa%252FviewAlbum%253Fid%253D1234%2526s%253D143441%2526partnerId%253D30!)
  end

  it "should put the id into the proper format" do
    expect(ItunesLinkinator.link_for("1234")).to eq(%Q!<a href="http://click.linksynergy.com/fs-bin/stat?id=eH8NOY8fBFE&offerid=78941&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fphobos.apple.com%252FWebObjects%252FMZStore.woa%252Fwa%252FviewAlbum%253Fid%253D1234%2526s%253D143441%2526partnerId%253D30"><img height="15" width="61" alt="" src="http://www.tunecore.com/images/buttons/badgeitunes61x15dark.gif" /></a>!)
  end

  it "should put alt text in too" do
    expect(ItunesLinkinator.link_for("1234","ABC")).to eq(%Q!<a href="http://click.linksynergy.com/fs-bin/stat?id=eH8NOY8fBFE&offerid=78941&type=3&subid=0&tmpid=1826&RD_PARM1=http%253A%252F%252Fphobos.apple.com%252FWebObjects%252FMZStore.woa%252Fwa%252FviewAlbum%253Fid%253D1234%2526s%253D143441%2526partnerId%253D30"><img height="15" width="61" alt="ABC" src="http://www.tunecore.com/images/buttons/badgeitunes61x15dark.gif" /></a>!)
  end
end
