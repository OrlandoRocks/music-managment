require 'spec_helper'

describe Cipherer do
  let(:string) { 'This is a test string' }
  let(:encryption_artifact) { double("EncryptionArtifact") }
  let(:strategy) { double("EncryptionStrategy", encrypt: encryption_artifact, decrypt: string) }

  describe "#encrypt" do
    it "passes the input string to the strategy" do
      cipherer = Cipherer.new(strategy: strategy)
      expect(strategy).to receive(:encrypt).with(string)

      result = cipherer.encrypt(string)
      expect(result).to eq(encryption_artifact)
    end
  end

  describe "#decrypt" do
    it "passes the input string to the strategy" do
      cipherer = Cipherer.new(strategy: strategy)
      expect(strategy).to receive(:decrypt).with(string)

      result = cipherer.decrypt(string)
      expect(result).to eq(string)
    end
  end
end

describe Cipherer::Migrator do
  let(:encrypted_string) { "This is an encrypted string" }
  let(:decrypted_string) { "This is a decrypted string" }
  let(:encryption_artifact) { double("EncryptionArtifact") }
  let(:old_strategy) { double("Aes256GcmStrategy", decrypt: decrypted_string) }
  let(:new_strategy) { double("Aes256OcbStrategy", encrypt: encryption_artifact) }

  describe "#migrate" do
    it "uses old_strategy to decrypt the value and new_strategy to re-encrypt it" do
      migrator = Cipherer::Migrator.new(old_strategy: old_strategy, new_strategy: new_strategy)

      expect(old_strategy).to receive(:decrypt).with(encrypted_string)
      expect(new_strategy).to receive(:encrypt).with(decrypted_string)

      result = migrator.migrate(encrypted_string)

      expect(result).to eq(encryption_artifact)
    end
  end
end
