require "spec_helper"

describe Crypto::Strategies::Aes128CbcStrategy do
  let(:secret) { "A test key" }
  let(:key) { "\xA8\xBE\xDE\x04\xE2\x8D*\xAE..z\xFA\xC3_\xD7~" }
  let(:salt) { "YhNvwizCNOpKSC20FA3wIA==" }
  let(:decrypted_string) { "A test string that is unencrypted" }
  let(:encrypted_string) do
    # Below are the console commands that generated the encrypted string.
    #
    # require "openssl" ruby 2.6.6
    # require "base64"
    # cipher = OpenSSL::Cipher.new("AES-128-CBC")
    # cipher.encrypt
    # test_string = "A test string that is unencrypted"
    # password = "A test key"
    # salt = Base64.encode64(Digest::SHA1.digest(OpenSSL::Random.random_bytes(16))[0..(16 - 1)]).chop
    # key = Digest::SHA1.digest(salt + password)[0..(16 - 1)]
    # cipher.key = key
    # cipher.padding = 1
    # cipher.update(test_string) + cipher.final
    "?\xB5\x11)gj\xE9\xE0.\\#\x92~\x04'\xC0w\x16\x84\xD4\xA1\xACO\xA2\xA8\x93\xE2\x9F<g\xEB\x01\x05\xFD\xBDnq2Q\x19\xB6G\xB5\xE2\x994\x81r".b
  end

  describe "using the same strategy object for encryption and decryption" do
    it "it correctly decrypts strings it has encrypted" do
      strategy = Crypto::Strategies::Aes128CbcStrategy.new(secret: secret, base64_encode: false)

      result = strategy.encrypt(decrypted_string)

      expect(strategy.decrypt(result.encrypted_string)).to eq(decrypted_string)
    end
  end

  describe "#encrypt" do
    context "when the option for base64 encoding is true" do
      it "produces a base64 encoded AES-128-CBC encrypted string" do
        options = {
          secret: secret,
          salt: salt,
          base64_encode: true
        }

        result = Crypto::Strategies::Aes128CbcStrategy.new(**options).encrypt(decrypted_string)

        expect(result.encrypted_string).to eq(Base64.encode64(encrypted_string))
        expect(result.secret).to eq(secret)
        expect(result.salt).to eq(salt)
      end
    end

    context "when the option for base64 encoding is false" do
      it "produces a AES-128-CBC encrypted string" do
        options = {
          secret: secret,
          salt: salt,
          base64_encode: false
        }

        result = Crypto::Strategies::Aes128CbcStrategy.new(**options).encrypt(decrypted_string)

        expect(result.encrypted_string).to eq(encrypted_string)
        expect(result.secret).to eq(secret)
        expect(result.salt).to eq(salt)
      end
    end

    context "when no salt is passed in" do
      it "generates and returns the salt used to encrypt the string" do
        options = {
          secret: secret,
          base64_encode: false
        }

        result = Crypto::Strategies::Aes128CbcStrategy.new(**options).encrypt(decrypted_string)

        decrypt_options = options.merge(salt: result.salt)

        expect(
          Crypto::Strategies::Aes128CbcStrategy.new(**decrypt_options).decrypt(result.encrypted_string)
        ).to eq(decrypted_string)
      end
    end
  end

  describe "#decrypt" do
    context "when the option for base64 encoding is true" do
      it "decrypts base64 encoded AES-128-CBC encrypted strings" do
        options = {
          secret: secret,
          salt: salt,
          base64_encode: true
        }

        expect(
          Crypto::Strategies::Aes128CbcStrategy.new(**options).decrypt(Base64.encode64(encrypted_string))
        ).to eq(decrypted_string)
      end
    end

    context "when the option for base64 encoding is false" do
      it "decrypts AES-128-CBC encrypted strings" do
        options = {
          secret: secret,
          salt: salt,
          base64_encode: false
        }

        expect(
          Crypto::Strategies::Aes128CbcStrategy.new(**options).decrypt(encrypted_string)
        ).to eq(decrypted_string)
      end
    end

    context "when using the wrong secret" do
      it "raises an error" do
        options = {
          secret: "I am not the right secret",
          salt: salt,
          base64_encode: false
        }

        expect do
          Crypto::Strategies::Aes128CbcStrategy.new(**options).decrypt(encrypted_string)
        end.to raise_error(OpenSSL::Cipher::CipherError)
      end
    end

    context "when using the wrong salt" do
      it "raises an error" do
        options = {
          secret: secret,
          salt: "00" + salt[2..],
          base64_encode: false
        }

        expect do
          Crypto::Strategies::Aes128CbcStrategy.new(**options).decrypt(encrypted_string)
        end.to raise_error(OpenSSL::Cipher::CipherError)
      end
    end
  end
end
