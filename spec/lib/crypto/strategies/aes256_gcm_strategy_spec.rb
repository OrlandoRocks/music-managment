require "spec_helper"

describe Crypto::Strategies::Aes256GcmStrategy do
  let(:key) { "\xCE+e/\xB6\xD8$\b\xC7\xA3\"5`\xACt\x92\x16\xC2\x8B\x8E,>c8m\xDB\xBEzl\xB9\xCF\xC2" }
  let(:iv) { "\x15+[\x99\x05:[\xE7vD\xE7\xA9" }
  let(:auth_data) { "cipherer" }
  let(:auth_tag) { "Sm\xB2R51\xA3{\xA6\xDAC\x95_+n\xAA" }
  let(:decrypted_string) { "A test string that is unencrypted" }
  let(:encrypted_string) do
    # Below are the console commands that generated the encrypted string.
    #
    # require "openssl" ruby 2.6.6
    # cipher = OpenSSL::Cipher.new("AES-256-GCM")
    # cipher.encrypt
    # key = cipher.random_key # This is the key above
    # iv = cipher.random_iv # This is the iv above
    # test_string = "A test string that is unencrypted"
    # auth_data = "cipherer"
    # cipher.auth_data = auth_data
    # s = cipher.update(test_string) + cipher.final
    # s.unpack('H*')[0].upcase
    "6DF4964CB6963F7128F0AA0B7592B1939D955339E65316056C1470870899D57EA1"
  end

  describe "using the same strategy object for encryption and decryption" do
    it "it correctly decrypts strings it has encrypted" do
      strategy = Crypto::Strategies::Aes256GcmStrategy.new

      result = strategy.encrypt(decrypted_string)

      expect(strategy.decrypt(result.encrypted_string)).to eq(decrypted_string)
    end
  end

  describe "#encrypt" do
    it "produces a hex encoded AES-256-GCM encrypted string" do
      options = {
        key: key,
        iv: iv,
        auth_data: auth_data
      }

      result = Crypto::Strategies::Aes256GcmStrategy.new(**options).encrypt(decrypted_string)

      expect(result.encrypted_string).to eq(encrypted_string)
      expect(result.iv).to eq(iv)
      expect(result.key).to eq(key)
      expect(result.auth_tag.b).to eq(auth_tag.b)
    end

    context "when no key is passed in" do
      it "generates and returns the key used to encrypt the string" do
        options = {
          iv: iv,
          auth_data: auth_data
        }

        result = Crypto::Strategies::Aes256GcmStrategy.new(**options).encrypt(decrypted_string)

        decrypt_options = options.merge(key: result.key, auth_tag: result.auth_tag)

        expect(
          Crypto::Strategies::Aes256GcmStrategy.new(**decrypt_options).decrypt(result.encrypted_string)
        ).to eq(decrypted_string)
      end
    end

    context "when no iv is passed in" do
      it "generates and returns the key used to encrypt the string" do
        options = {
          key: key,
          auth_data: auth_data
        }

        result = Crypto::Strategies::Aes256GcmStrategy.new(**options).encrypt(decrypted_string)

        decrypt_options = options.merge(iv: result.iv, auth_tag: result.auth_tag)

        expect(
          Crypto::Strategies::Aes256GcmStrategy.new(**decrypt_options).decrypt(result.encrypted_string)
        ).to eq(decrypted_string)
      end
    end
  end

  describe "#decrypt" do
    it "decrypts hex encoded AES-256-GCM encrypted strings" do
      options = {
        key: key,
        iv: iv,
        auth_data: auth_data,
        auth_tag: auth_tag
      }

      expect(
        Crypto::Strategies::Aes256GcmStrategy.new(**options).decrypt(encrypted_string)
      ).to eq(decrypted_string)
    end

    context "when using the wrong key" do
      it "raises an error" do
        options = {
          key: OpenSSL::Random.random_bytes(32),
          iv: iv,
          auth_data: auth_data,
          auth_tag: auth_tag
        }

        expect do
          Crypto::Strategies::Aes256GcmStrategy.new(**options).decrypt(encrypted_string)
        end.to raise_error(OpenSSL::Cipher::CipherError)
      end
    end

    context "when using the wrong iv" do
      it "raises an error" do
        options = {
          key: key,
          iv: OpenSSL::Random.random_bytes(12),
          auth_data: auth_data,
          auth_tag: auth_tag
        }

        expect do
          Crypto::Strategies::Aes256GcmStrategy.new(**options).decrypt(encrypted_string)
        end.to raise_error(OpenSSL::Cipher::CipherError)
      end
    end

    context "when using the wrong auth_tag" do
      it "raises an error" do
        options = {
          key: key,
          iv: iv,
          auth_data: auth_data,
          auth_tag: OpenSSL::Random.random_bytes(32)
        }

        expect do
          Crypto::Strategies::Aes256GcmStrategy.new(**options).decrypt(encrypted_string)
        end.to raise_error(OpenSSL::Cipher::CipherError)
      end
    end
  end
end
