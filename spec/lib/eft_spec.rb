require "rails_helper"

# TODO: Refactor this spec to not use a top level include
include Eft

describe Eft do
  describe "process_eft_batch_response_row" do
    before :each do
      @batch = create(:eft_batch)
      @person = create(:person, :with_balance, amount: 100, name: 'Test User', email: 'eft_test@test.com', status: 'Active')
      @stored_bank_account = create(:stored_bank_account, person: @person, customer_vault_id: 1234)

      allow_any_instance_of(StoredBankAccount).to receive(:mark_previous_account_as_deleted)
      @stored_bank_account.save

      @eft_batch_transaction = create(:eft_batch_transaction, stored_bank_account: @stored_bank_account)
      @eft_batch_transaction.update_attribute(:status, "processing_in_batch")
      @row = [].fill(nil, 0..37)
      @row[0] = 123456789 #transaction_id
      @row[2] = '' #response_text
      @row[6] = '50.27' #amount
      @row[37] = '100' #response code
      instance_variable_set("@errored_amount", 0)
      instance_variable_set("@errored_count", 0)
      ActionMailer::Base.deliveries = []
    end

    it "should call error workflow for known error response code" do
      @row[37] = "400"
      @row[2] = 'DECLINED INVALID ROUTING NUMBER'
      expect {
        process_eft_batch_response_row(@batch, @eft_batch_transaction, @row)
      }.to change(ActionMailer::Base.deliveries,:size).by(1) #just to the customer
      expect(@eft_batch_transaction.reload.status).to eq('error')
    end

    it "should call error workflow and send email to admin for unrecognized response code" do
      @row[37] = "999"
      @row[2] = 'SOME UNRECOGNIZED ERROR CODE'
      expect {
        process_eft_batch_response_row(@batch, @eft_batch_transaction, @row)
      }.to change(ActionMailer::Base.deliveries,:size).by(2) #one email to the customer and one to admin

      expect(@eft_batch_transaction.reload.status).to eq('error')
    end
  end

  describe "#process_response_hash" do
    context "when there is no response from Braintree" do
      it "should delete the EftQuery" do
        eft_query = create(:eft_query)

        expect(eft_query).to receive(:destroy)

        process_response_hash(nil, eft_query)
      end
    end

    context "when an Eft Batch Transaction doesn't exist for the Braintree transaction" do
      it "should notify admin users" do
        response_hash = {
          transaction_id: "1",
          order_id:       "2"
        }.with_indifferent_access

        eft_query    = create(:eft_query)
        eft_notifier = double(EftNotifier)
        message      = "EFT Batch | Error - Eft Batch Transaction returned by Braintree " \
                       "was not found in our database | transaction_id=#{response_hash[:transaction_id]} order_id=#{response_hash[:order_id]}"

        expect(EftNotifier).to receive(:admin_error_notice).with(message).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        process_response_hash(response_hash, eft_query)
      end
    end

    context "when an Eft Batch Transaction cannot be processed" do
      it "should notify admin users" do
        transaction_id = 1
        eft_batch_transaction = create(
          :eft_batch_transaction,
          :pending_approval,
          :with_person_balance,
          transaction_id: transaction_id
        )

        eft_query = create(:eft_query)
        response_hash = {
          transaction_id: transaction_id,
          order_id:       "2"
        }.with_indifferent_access

        eft_notifier = double(EftNotifier)
        message = "EFT Batch | Error - Eft Batch Transaction has a status other than sent_to_bank or success " \
                  "or one of the failed statuses (could be a duplicate transaction processing) | " \
                  "order_id=#{response_hash[:order_id]} eft_batch_transaction_id=#{eft_batch_transaction.id} " \
                  "status=#{eft_batch_transaction.status}"

        expect(EftNotifier).to receive(:admin_error_notice).with(message).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        process_response_hash(response_hash, eft_query)
      end
    end

    context "when an Eft Batch Transaction is succesfully deposited in the bank" do
      it "calls success on the transaction" do
        transaction_id = 1
        eft_batch_transaction = create(
          :eft_batch_transaction,
          :sent_to_bank,
          :with_person_balance,
          transaction_id: transaction_id
        )

        eft_query           = create(:eft_query)
        query_response_code = "1"
        response_text       = "Response Text"
        response_hash = {
          transaction_id: transaction_id,
          condition:      "complete",
          action: {
            success:       query_response_code,
            response_text: response_text
          }
        }.with_indifferent_access

        allow(EftBatchTransaction).to receive(:where).and_return([eft_batch_transaction])

        expect(eft_batch_transaction).to receive(:success).with(query_response_code, response_text, eft_query)

        process_response_hash(response_hash, eft_query)
      end
    end

    context "when an Eft Batch Transaction failed to be deposited into the bank" do
      it "calls failure on the transaction" do
        transaction_id = 1
        eft_batch_transaction = create(
          :eft_batch_transaction,
          :sent_to_bank,
          :with_person_balance,
          transaction_id: transaction_id
        )

        eft_query           = create(:eft_query)
        query_response_code = "1"
        response_text       = "Response Text"
        response_hash = {
          transaction_id: transaction_id,
          condition:      "failed",
          action: {
            success:       query_response_code,
            response_text: response_text
          }
        }.with_indifferent_access

        allow(EftBatchTransaction).to receive(:where).and_return([eft_batch_transaction])

        expect(eft_batch_transaction).to receive(:failure).with(query_response_code, response_text, eft_query)

        process_response_hash(response_hash, eft_query)
      end
    end

    context "when an EftBatchTransaction errored out during the initial braintree check" do
      it "should not update the eft batch transaction" do
        transaction_id = 1
        eft_batch_transaction = create(
          :eft_batch_transaction,
          :sent_to_bank,
          :with_person_balance,
          transaction_id: transaction_id
        )

        eft_query = create(:eft_query)
        response_hash = {
          transaction_id: transaction_id,
          condition:      "failed",
          action:         { success: "0" }
        }.with_indifferent_access

        expect(eft_batch_transaction.status).to eq "sent_to_bank"
        process_response_hash(response_hash, eft_query)
        expect(eft_batch_transaction.status).to eq "sent_to_bank"
      end
    end

    context "when an EftBatchTransaction has a status other than complete or failed" do
      it "should notify admin users" do
        transaction_id = 1
        create(
          :eft_batch_transaction,
          :sent_to_bank,
          :with_person_balance,
          transaction_id: transaction_id
        )

        eft_query = create(:eft_query)
        order_id      = 2
        condition     = "not complete"
        response_hash = {
          transaction_id: transaction_id,
          order_id:       order_id,
          condition:      condition
        }.with_indifferent_access

        eft_notifier = double(EftNotifier)
        message      = "EFT Batch | Error - transaction condition does not match \"complete\" or \"failure\" | " \
                       "transaction_id=#{response_hash[:transaction_id]} order_id=#{order_id} condition=#{condition}"

        expect(EftNotifier).to receive(:admin_error_notice).with(message).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        process_response_hash(response_hash, eft_query)
      end
    end
  end

  describe "#finalize_eft_query" do
    context "when the EftQuery has batch transactions" do
      it "invokes the daily query summary" do
        eft_notifier = double(EftNotifier)
        eft_query    = create(:eft_query)
        create(:eft_batch_transaction, :sent_to_bank, :with_person_balance, eft_query: eft_query)

        expect(EftNotifier).to receive(:daily_query_summary).with(eft_query.id).and_return(eft_notifier)
        expect(eft_notifier).to receive(:deliver)

        finalize_eft_query(eft_query)
      end
    end

    context "when the EftQuery does NOT have batch transactions" do
      it "deletes the Eft Query" do
        eft_query = create(:eft_query)

        expect(eft_query).to receive(:destroy)

        finalize_eft_query(eft_query)
      end
    end
  end

  describe "#mark_batches_as_complete" do
    context "when there are incomplete Eft Batches" do
      it "updates the Eft Batch to complete if the batch was completed" do
        eft_batch = create(:eft_batch, status: "upload_confirmed")
        create(:eft_batch_transaction, :success, :with_person_balance, eft_batch: eft_batch)

        mark_batches_as_complete

        expect(eft_batch.reload.status).to eq "complete"
      end
    end
  end

  describe "#create_eft_batch" do
    it "generates a batch file name and response file name" do
      eft_batch = create(:eft_batch)
      allow(EftBatch).to receive(:create).and_return(eft_batch)
      timestamp = Time.now.strftime("%Y_%m_%d_#{eft_batch.id}_%H%M%S")
      filename  = "tunecore_eft_batch_#{timestamp}.csv"

      create_eft_batch([])

      expect(eft_batch.batch_file_name).to eq filename
      expect(eft_batch.response_file_name).to eq "response-#{filename}"
    end

    it "creates an Eft Batch Transaction History" do
      processing_eft_batch_txn = create(:eft_batch_transaction, :with_person_balance, :processing_in_batch)
      pending_eft_batch_txn    = create(:eft_batch_transaction, :with_person_balance)

      create_eft_batch(
        [
          processing_eft_batch_txn,
          pending_eft_batch_txn
        ]
      )

      processing_eft_batch_txn.reload
      pending_eft_batch_txn.reload

      expect(processing_eft_batch_txn.eft_batch_transaction_history.pluck(:status)).to include("processing")
      expect(pending_eft_batch_txn.eft_batch_transaction_history.pluck(:status)).not_to include("processing")
    end

    it "logs transaction error" do
      eft_batch_transaction = create(:eft_batch_transaction, :with_person_balance)

      expect(self).to receive(:log)

      create_eft_batch([eft_batch_transaction])
    end
  end

  describe "#create_csv_from_batch" do
    after do
      File.delete('tmpTest File') if File.exist?('tmpTest File')
    end

    it "records a total amount" do
      eft_batch = create(:eft_batch, batch_file_name: "Test File")
      eft_batch_transactions = create_list(:eft_batch_transaction, 2, :with_person_balance, :processing_in_batch, eft_batch: eft_batch)

      create_csv_from_batch(eft_batch)

      expect(eft_batch.reload.total_amount.to_i).to eq eft_batch_transactions.sum(&:amount).to_i
    end

    it "logs Eft Batch Transaction is added to csv" do
      eft_batch = create(:eft_batch, batch_file_name: "Test File")
      eft_batch_transactions = create_list(:eft_batch_transaction, 2, :with_person_balance, :processing_in_batch, eft_batch: eft_batch)
      running_total = eft_batch_transactions.sum(&:amount)

      allow(self).to receive(:log).with(
        "EFT Batch",
        "EFT csv created",
        eft_batch: eft_batch.id,
        filename:  eft_batch.batch_file_name,
        count:     2,
        amount:    running_total
      )

      eft_batch_transactions.each do |txn|
        expect(self).to receive(:log).with(
          "EFT Batch",
          "EFT Batch Transaction Added to csv for upload",
          eft_batch:             txn.eft_batch.id,
          file_sent:             txn.eft_batch.batch_file_name,
          eft_batch_transaction: txn.id,
          person:                txn.stored_bank_account.person_id,
          order:                 txn.order_id,
          amount:                txn.amount
        ).at_least(:once)
      end

      create_csv_from_batch(eft_batch)
    end

    it "logs Eft CSV creation" do
      eft_batch = create(:eft_batch, batch_file_name: "Test File")
      eft_batch_transactions = create_list(:eft_batch_transaction, 2, :with_person_balance, :processing_in_batch, eft_batch: eft_batch)
      running_total = eft_batch_transactions.sum(&:amount)

      eft_batch_transactions.each do |txn|
        allow(self).to receive(:log).with(
          "EFT Batch",
          "EFT Batch Transaction Added to csv for upload",
          eft_batch:             txn.eft_batch.id,
          file_sent:             txn.eft_batch.batch_file_name,
          eft_batch_transaction: txn.id,
          person:                txn.stored_bank_account.person_id,
          order:                 txn.order_id,
          amount:                txn.amount
        )
      end

      expect(self).to receive(:log).with(
        "EFT Batch",
        "EFT csv created",
        eft_batch: eft_batch.id,
        filename:  eft_batch.batch_file_name,
        count:     2,
        amount:    running_total
      ).at_least(:once)

      create_csv_from_batch(eft_batch)
    end
  end

  describe "#upload_eft_csv_to_braintree" do
    it "logs upload event to Braintree" do
      eft_batch = create(:eft_batch, batch_file_name: "Test File", total_amount: 100.00)

      allow(self).to receive(:`).and_return(1, "curl response") # We're stubbing the grep command in this method
      allow(self).to receive(:log).with(
        "EFT Batch",
        "Count of lines in upload csv",
        count: 1
      )
      allow(self).to receive(:log).with(
        "EFT Batch",
        "Curl Output",
        batch:     eft_batch.id,
        file_sent: eft_batch.batch_file_name,
        curl:      "curl response"
      )

      expect(self).to receive(:log).with(
        "EFT Batch",
        "File uploaded to Braintree",
        batch:     eft_batch.id,
        file_sent: eft_batch.batch_file_name,
        amount:    eft_batch.total_amount
      )

      upload_eft_csv_to_braintree(eft_batch.id)
    end

    it "logs upload failure to Braintree" do
      eft_batch = create(:eft_batch, batch_file_name: "Test File", total_amount: 100.00)

      allow(self).to receive(:`) { 0 } # We're stubbing the grep command in this method
      allow(self).to receive(:log).with(
        "EFT Batch",
        "Count of lines in upload csv",
        count: 0
      )

      expect(self).to receive(:log).with(
        "EFT Batch",
        "File not uploaded to Braintree since there were no transactions",
        batch:     eft_batch.id,
        file_sent: eft_batch.batch_file_name,
        amount:    eft_batch.total_amount
      )

      upload_eft_csv_to_braintree(eft_batch.id)
    end
  end
end
