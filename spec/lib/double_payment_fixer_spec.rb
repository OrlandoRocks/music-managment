require "rails_helper"

describe DoublePaymentFixer do
  let(:person) { create(:person) }
  let(:song)   { create(:song) }
  let!(:person_balance) {PersonBalance.first_or_create(person_id: person.id)}

  before(:each) do
    person_balance.update(balance: 100.00)
    srm = SalesRecordMaster.create(country_website_id: 7, sip_store_id: 249, period_interval: 3, period_year: 2018, period_type: "monthly", period_sort: "2018-03-01", revenue_currency: "EUR", amount_currency: "EUR", country: "ES", summarized: 1, created_at: "2018-05-18")
    3.times do
      SalesRecord.create(sales_record_master_id: srm.id, person_id: person.id, sip_sales_record_id: 12345, related_id: song.id, related_type: "Song", distribution_type: "Streaming", quantity: 1, revenue_per_unit: ".0356000", revenue_total: ".035600", amount: ".035600")
    end

    @correct_sip_credit = 0.0356 * 3.0

    person_intake1 = PersonIntake.create(person_id: person.id, amount: @correct_sip_credit, currency: "EUR", created_at: "2018-05-18")
    @person_intake2 = PersonIntake.create(person_id: person.id, amount: @correct_sip_credit, currency: "EUR", created_at: "2018-05-18")
    PersonTransaction.create(person_id: person.id, credit: @correct_sip_credit, previous_balance: person_balance.balance, target_type: "PersonIntake", target_id: person_intake1, currency: "EUR")
    PersonTransaction.create(person_id: person.id, credit: @correct_sip_credit, previous_balance: person_balance.balance, target_type: "PersonIntake", target_id: @person_intake2, currency: "EUR")

    person_balance.update(balance: @correct_sip_credit * 2)
    @previous_balance = PersonBalance.find_by(person_id: person.id).balance
  end

  describe '#fix_double_sip_posting_credits' do

    before(:each) do
      DoublePaymentFixer.new.fix_double_sip_posting_credits
    end

    context "transaction fails" do

    end

    it "creates a person transaction for each person who was paid twice by SIP" do
      expect(PersonTransaction.where(person_id: person.id, debit: @correct_sip_credit, previous_balance: @previous_balance, target_type: "PersonIntake", target_id: @person_intake2.id, currency: "EUR").first).to_not eq nil
    end

    it "updates the person balance to the current balance MINUS the correct sip posting amount" do
      expect(PersonBalance.find_by(person_id: person.id).balance).to eq(@previous_balance - @correct_sip_credit)
    end

    context "some intakes have already been addressed" do
      it "does not create a person transaction for each person who was paid twice by SIP" do
        DoublePaymentFixer.new.fix_double_sip_posting_credits
        expect(PersonTransaction.where(person_id: person.id, debit: @correct_sip_credit, previous_balance: @previous_balance, target_type: "PersonIntake", target_id: @person_intake2.id, currency: "EUR").count).to eq 1
      end

      it "does not update the person balance to the current balance MINUS the correct sip posting amount" do
        DoublePaymentFixer.new.fix_double_sip_posting_credits
        expect(PersonBalance.find_by(person_id: person.id).balance).to eq(@previous_balance - @correct_sip_credit)
      end
    end
  end

  describe "fix_double_encumbrance_witholding" do
    before(:each) do
      person_balance.update(balance: 100.00)
      @encumbrance_summary = EncumbranceSummary.create(person_id: person.id, reference_id: "12345", reference_source: "LYRIC", total_amount: 100.00, outstanding_amount: 100, fee_amount: 5.00)
      deet = EncumbranceDetail.create(encumbrance_summary_id: @encumbrance_summary.id, transaction_date: "2018-05-18", transaction_amount: -50.00)
      @detail = EncumbranceDetail.create(encumbrance_summary_id: @encumbrance_summary.id, transaction_date: "2018-05-18", transaction_amount: -50.00)
      @encumbrance_summary.update(outstanding_amount: 0.00)
      person_balance.update(balance: person_balance.balance - @detail.transaction_amount.abs)
      @previous_balance = person_balance.balance
      person_balance.update(balance: @previous_balance - deet.transaction_amount.abs)
      DoublePaymentFixer.new.fix_double_encumbrance_witholding
    end

    it "creates a new encumbrance detail record with a postive transaction amount" do
      expect(EncumbranceDetail.where(transaction_amount: 50.00, encumbrance_summary_id:@encumbrance_summary.id)).to_not eq nil
    end

    it "updates the encumbrance summary outstanding amount" do
      expect(@encumbrance_summary.reload.outstanding_amount).to eq(50.00)
    end

    it "creates a person transaction record" do
      detail = EncumbranceDetail.where(transaction_amount: 50.00, encumbrance_summary_id:@encumbrance_summary.id).first
      expect(PersonTransaction.where(
          person_id: person.id,
          credit: @detail.transaction_amount.abs,
          target_type: "EncumbranceDetail",
          target_id: detail.id,
          currency: person.currency,
          comment: "Credit for incorrect encumbrance withholding",
          previous_balance: person_balance.balance)).to_not eq nil
    end

    it "updates the person balance to the current balance PLUS the withheld amount" do
      expect(PersonBalance.find_by(person_id: person.id).balance.to_i).to eq(@previous_balance.to_i)
    end

    context "double encumbrance has already been processed" do
      it "does not create a new encumbrance detail record with a postive transaction amount" do
        DoublePaymentFixer.new.fix_double_encumbrance_witholding
        expect(EncumbranceDetail.where(transaction_amount: 50.00, encumbrance_summary_id:@encumbrance_summary.id).count).to eq 1
      end

      it "does not update the encumbrance summary outstanding amount" do
        DoublePaymentFixer.new.fix_double_encumbrance_witholding
        expect(@encumbrance_summary.reload.outstanding_amount).to eq(50.00)
      end

      it "does not create a person transaction record" do
        DoublePaymentFixer.new.fix_double_encumbrance_witholding
        expect(PersonTransaction.where(
            person_id: person.id,
            comment: "Credit for encumbrance withholding from 2018-05-18").count).to eq 1
      end

      it "does not pdates the person balance to the current balance PLUS the withheld amount" do
        expect(PersonBalance.find_by(person_id: person.id).balance.to_i).to eq(@previous_balance.to_i)
      end
    end
  end
end


describe DoublePaymentFixer do
  describe '.cancel_payment_request' do
    let!(:person)              { create(:person, password: "Testpass123!", password_confirmation: "Testpass123!") }
    let!(:admin)               { create(:person, :admin)}
    let!(:stored_bank_account) { create(:stored_bank_account, person_id: person.id) }
    let!(:paypal_params)       {
      {
        "paypal_address" => "sophie@paypal.com",
        "payment_amount" => 900.00,
        "paypal_address_confirmation" => "sophie@paypal.com",
        "password_entered" => "Testpass123!"
      }.with_indifferent_access
    }

    context "there is one withdrawal request" do
      before(:each) do
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        @person_balance  = person.person_balance
        @person_balance.update(balance: 1000.00)
        @paypal_transfer = PaypalTransfer.generate(person, paypal_params)
        @person_balance.update(balance: -400.00)
        allow(DoublePaymentFixer).to receive(:people_with_negative_balances).and_return([person])
        DoublePaymentFixer.cancel_payment_request(admin.email)
      end

      it "cancels any paypal transfers" do
        expect(@paypal_transfer.reload.transfer_status).to eq("cancelled")
        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer.payment_cents, target_id: @paypal_transfer.id, comment: "Rollback of Paypal Payment: #{@paypal_transfer}.person_transaction.comment")).to_not eq nil

        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer.admin_charge_cents, target_id: @paypal_transfer.id, comment: "Rollback of Paypal Payment Service Fee")).to_not eq nil
      end

      it "puts the money back in the person's balance" do
        expect(PersonBalance.find(@person_balance.id).balance.to_i).to eq(500)
      end
    end

    context "two withdrawal requests" do
      let!(:paypal_params_500)       {
        {
          "paypal_address" => "sophie@paypal.com",
          "payment_amount" => 500.00,
          "paypal_address_confirmation" => "sophie@paypal.com",
          "password_entered" => "Testpass123!"
        }.with_indifferent_access
      }

      let!(:paypal_params_400)       {
        {
          "paypal_address" => "sophie@paypal.com",
          "payment_amount" => 400.00,
          "paypal_address_confirmation" => "sophie@paypal.com",
          "password_entered" => "Testpass123!"
        }.with_indifferent_access
      }

      before(:each) do
        @person_balance  = person.person_balance
        @person_balance.update(balance: 1000.00)
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        @paypal_transfer1 = PaypalTransfer.generate(person, paypal_params_500)
        @paypal_transfer2 = PaypalTransfer.generate(person, paypal_params_400)
        @person_balance.update(balance: -400.00)
        allow(DoublePaymentFixer).to receive(:people_who_made_withdrawal_requests).and_return([person])
        DoublePaymentFixer.cancel_payment_request(admin.email)
      end

      it "cancels any paypal transfers" do
        expect(@paypal_transfer1.reload.transfer_status).to eq("cancelled")
        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer1.payment_cents, target_id: @paypal_transfer1.id, comment: "Rollback of Paypal Payment: #{@paypal_transfer1}.person_transaction.comment")).to_not eq nil

        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer1.admin_charge_cents, target_id: @paypal_transfer1.id, comment: "Rollback of Paypal Payment Service Fee")).to_not eq nil

        expect(@paypal_transfer2.reload.transfer_status).to eq("cancelled")
        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer2.payment_cents, target_id: @paypal_transfer2.id, comment: "Rollback of Paypal Payment: #{@paypal_transfer2}.person_transaction.comment")).to_not eq nil

        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer2.admin_charge_cents, target_id: @paypal_transfer2.id, comment: "Rollback of Paypal Payment Service Fee")).to_not eq nil
      end

      it "puts the money back in the person's balance" do
        expect(PersonBalance.find(@person_balance.id).balance.to_i).to eq(500)
      end
    end

    context "withdrawal requests was made before the second person intake was paid" do
      before(:each) do
        @person_balance  = person.person_balance
        @person_balance.update(balance: 1000.00)
        @paypal_transfer = PaypalTransfer.generate(person, paypal_params)
        @paypal_transfer.created_at = "2018-05-18"
        @paypal_transfer.save
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        intake = PersonIntake.new(person_id: person.id, created_at: "2018-05-18 21:49:43")
        intake.save(validate: false)
        @person_balance.update(balance: -400.00)
        @previous_balance = @person_balance.balance.to_i
        allow(DoublePaymentFixer).to receive(:people_with_negative_balances).and_return([person])
        DoublePaymentFixer.cancel_payment_request(admin.email)
      end
      it "does not cancel the paypal transfers" do
        expect(@paypal_transfer.reload.transfer_status).to eq("pending")
        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer.payment_cents, target_id: @paypal_transfer.id, comment: "Rollback of Paypal Payment: #{@paypal_transfer}.person_transaction.comment").length).to eq 0

        expect(PersonTransaction.where(person_id: person.id, debit: 0, credit: @paypal_transfer.admin_charge_cents, target_id: @paypal_transfer.id, comment: "Rollback of Paypal Payment Service Fee").length).to eq 0
      end

      it "puts the money back in the person's balance" do
        expect(PersonBalance.find(@person_balance.id).balance.to_i).to eq(@previous_balance)
      end
    end
  end
end
