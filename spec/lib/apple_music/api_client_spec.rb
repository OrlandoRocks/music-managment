require "rails_helper"

describe AppleMusic::ApiClient do
  include AppleMusicApiHelper

  let(:faraday) { fake_apple_music_client }
  let(:client) { described_class.new }
  let(:artist_response_1) { AppleMusicApiHelper::ARTIST_RESPONSE_1 }
  let(:album_response_1) { AppleMusicApiHelper::ALBUM_RESPONSE_1 }
  let(:empty_artists_response) { AppleMusicApiHelper::EMPTY_ARTISTS_RESPONSE }
  let(:dummy_albums_response) { AppleMusicApiHelper::DUMMY_ALBUMS_RESPONSE }

  before do
    allow(Faraday).to receive(:new).and_return(faraday)
  end

  describe "#find_by_artist_name" do
    context "when success" do
      it "responds with artists and their top album" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 200)
        album_resp = double(success?: true,
                            body: album_response_1.to_json,
                            status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        artists_with_albums = client.find_by_artist_name('nine inch nails')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.first["id"]).not_to be nil
        expect(artists_with_albums.first["attributes"]["name"]).not_to be nil
        expect(artists_with_albums.map { |a| a["relationships"]["topAlbum"] }).to all(be_truthy)
      end
    end

    context "when results don't have a valid match" do
      it "responds with empty array" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp)

        artists_with_albums = client.find_by_artist_name('pooey flute')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.length).to eq(0)
      end
    end

    context "when artist call fails" do
      it "responds with empty array" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 500)
        album_resp = double(success?: true,
                            body: album_response_1.to_json,
                            status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        expect(Rails.logger).to receive(:error).twice
        artists_with_albums = client.find_by_artist_name('nin')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.length).to eq(0)
      end
    end

    context "when album call fails" do
      it "responds with empty array" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 200)
        album_resp = double(success?: true,
                            body: album_response_1.to_json,
                            status: 500)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        expect(Rails.logger).to receive(:error).twice
        artists_with_albums = client.find_by_artist_name('nine inch nails')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.length).to eq(0)
      end
    end

    context "when artist call returns no results" do
      it "responds with empty array" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: empty_artists_response.to_json,
                             status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp)

        artists_with_albums = client.find_by_artist_name('nin')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.length).to eq(0)
      end
    end

    context "when artist has no matching albums" do
      it "responds with artist with dummy album" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 200)
        album_resp = double(success?: true,
                             body: dummy_albums_response.to_json,
                             status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        artists_with_albums = client.find_by_artist_name('nine inch nails')

        expect(artists_with_albums.is_a?(Array)).to be true
        expect(artists_with_albums.first["id"]).not_to be nil
        expect(artists_with_albums.first["relationships"]["topAlbum"]["attributes"]["name"].length).to eq(0)
      end
    end
  end

  describe "#get_artist_by_id" do
    context "when success" do
      it "responds with artist and their top album" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1[:results][:artists].to_json,
                             status: 200)
        album_resp = double(success?: true,
                            body: album_response_1.to_json,
                            status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        artist_with_album = client.get_artist_by_id('123', 'nine inch nails')

        expect(artist_with_album.is_a?(Array)).to be false
        expect(artist_with_album["id"]).not_to be nil
        expect(artist_with_album["attributes"]["name"]).not_to be nil
        expect(artist_with_album["relationships"]["topAlbum"]).to be_truthy
      end
    end

    context "when artist call fails" do
      it "responds with error status 500" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1.to_json,
                             status: 500)
        album_resp = double(success?: true,
                            body: album_response_1.to_json,
                            status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp, album_resp)

        expect(Rails.logger).to receive(:error).twice
        artist_with_album = client.get_artist_by_id('123', 'nine inch nails')

        expect(artist_with_album[:error][:status]).to be 500
      end
    end

    context "when given bad ID" do
      it "responds with error status 404" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1[:results][:artists].to_json,
                             status: 404)
        allow(faraday).to receive(:get).and_return(artist_resp)

        artist_with_album = client.get_artist_by_id('123', 'nine inch nails')

        expect(artist_with_album[:error][:status]).to be 404
      end
    end

    context "when given non-matching name" do
      it "responds with error status 404" do
        stub_const("APPLE_MUSIC_TOKEN", 'some token')

        artist_resp = double(success?: true,
                             body: artist_response_1[:results][:artists].to_json,
                             status: 200)
        allow(faraday).to receive(:get).and_return(artist_resp)

        artist_with_album = client.get_artist_by_id('107917', 'pooey flute')

        expect(artist_with_album[:error][:status]).to be 404
      end
    end
  end
end
