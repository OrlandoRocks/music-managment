require "rails_helper"

describe SystemStatus do
  context "Braintree transactions" do
    it "has a score that is a floating point number" do
      status = SystemStatus::BraintreeTransactions.do_check

      expect(status).to have_key(:score)
      expect(status[:score]).to be_kind_of(Float)
    end

    it "calculates a score when some transactions are not successful" do
      create(:braintree_transaction, status: "declined")
      create(:braintree_transaction)
      create(:braintree_transaction)
      create(:braintree_transaction, status: "fraud")
      create(:braintree_transaction, status: "duplicate")

      status = SystemStatus::BraintreeTransactions.do_check

      expect(status[:score]).to be_between(0.0, 1.0).exclusive
    end
  end
end
