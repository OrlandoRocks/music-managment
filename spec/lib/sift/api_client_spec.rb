require "rails_helper"

describe Sift::ApiClient do
  let(:person) {
    create(:person)
  }

  let(:request) {
    Sift::Requests::CreateAccount.new({:$user_id => person.id.to_s})
  }

  let(:invalid_request) {
    Sift::Requests::CreateAccount.new({})
  }

  let(:score_request) {
    double(Sift::Requests::CreateOrder, return_workflow_status: true)
  }

  let(:response) {
    {}
  }

  let(:score_response) {
    {}
  }

  let(:client) {
    double(Sift::Client)
  }

  let(:api_client) {
    Sift::ApiClient.new(
      Sift::EventService::CREATE_ACCOUNT,
      {:$user_id => person.id.to_s},
      true
    )
  }

  let(:api_client_no_score) {
    Sift::ApiClient.new(
      Sift::EventService::CREATE_ACCOUNT,
      {:$user_id => person.id.to_s},
      false
    )
  }

  let(:invalid_api_client) {
    Sift::ApiClient.new(
      Sift::EventService::CREATE_ACCOUNT,
      {},
      false
    )
  }

  before :each do
    allow(FeatureFlipper).to receive(:show_feature?).with(any_args).and_return(true)
    allow(ENV).to receive(:fetch).and_return("")
    allow(Sift::Client).to receive(:new).and_return(client)
    allow(client).to receive(:track).with(anything, anything, hash_including(return_workflow_status: false)).and_return(response)
    allow(client).to receive(:track).with(anything, anything, hash_including(return_workflow_status: true)).and_return(score_response)
    allow(api_client).to receive(:send_request).and_call_original
  end

  describe "#self.send_request" do
    before :each do
      expect_any_instance_of(Sift::ApiClient).to_not receive(:log_api_errors)
      expect(Sift::Responses::Score).to_not receive(:new)
      expect(Tunecore::Airbrake).to_not receive(:notify)
    end

    it "non transaction event should call send_request only once" do
      expect(Sift::ApiClient).to receive(:new).once.and_return(double(send_request: nil))
      Sift::ApiClient.send_request(Sift::EventService::CREATE_ACCOUNT, {})
    end

    context "transaction event should call send_request" do
      it "twice" do
        expect(Sift::ApiClient).to receive(:new).twice.and_return(double(send_request: nil))
        Sift::ApiClient.send_request(Sift::EventService::TRANSACTION, [{},{:$amount => 10}])
      end

      it "once because there is only one transaction" do
        expect(Sift::ApiClient).to receive(:new).once.and_return(double(send_request: nil))
        Sift::ApiClient.send_request(Sift::EventService::TRANSACTION, [{}])
      end

      it "once because one transaction is 0$" do
        expect(Sift::ApiClient).to receive(:new).twice.and_return(double(send_request: nil))
        Sift::ApiClient.send_request(Sift::EventService::TRANSACTION, [{},{:$amount => 0}])
      end
    end
  end

  describe "#build_path" do
    context "builds a path to the appropriate request class" do
      let(:api_client) {
        Sift::ApiClient.new(Sift::EventService::CREATE_ACCOUNT, {:$user_id => person.id.to_s}, false)
      }

      it "create_account" do
        expect(api_client.build_path(Sift::EventService::CREATE_ACCOUNT)).to eq Sift::Requests::CreateAccount
      end

      it "update_account" do
        expect(api_client.build_path(Sift::EventService::UPDATE_ACCOUNT)).to eq Sift::Requests::UpdateAccount
      end

      it "create_order" do
        expect(api_client.build_path(Sift::EventService::CREATE_ORDER)).to eq Sift::Requests::CreateOrder
      end

      it "transaction" do
        expect(api_client.build_path(Sift::EventService::TRANSACTION)).to eq Sift::Requests::Transaction
      end
    end
  end

  describe "#send_request" do
    it "exits due to invalid request" do
      expect(client).to_not receive(:track)
      invalid_api_client.send_request
    end

    context "ScoreResponse is" do
      it "processed when return_workflow_status is true" do
        expect(Sift::Responses::Score).to receive(:new)
        api_client.send_request
      end

      it "not processed because return_workflow_status is false" do
        expect(Sift::Responses::Score).to_not receive(:new)
        api_client_no_score.send_request
      end
    end
  end
end
