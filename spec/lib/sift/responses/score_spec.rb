require 'rails_helper'

describe Sift::Responses::Score do
  let(:person_id) { 134 }
  let(:order_id) { "test_order_id".freeze }
  let(:status) { "status".freeze }
  let(:score) { 0.4324 }
  let(:workflow_decision_id) { "workflow_decision_id".freeze }
  let(:response_body) {
    {
      "status": status,
      "score_response": {
        "scores": {
          "payment_abuse": {
            "score": score
          }
        },
        "workflow_statuses": [
          {
            "history": [
              {
                "config": {
                  "decision_id": workflow_decision_id
                }
              }
            ]
          }
        ]
      }
    }.with_indifferent_access
  }

  context "variable should be set for" do
    let(:score_response) { 
      Sift::Responses::Score.new(
        double(body: response_body), 
        Sift::Requests::CreateOrder.new({:$user_id => "10"})) 
    }

    it "status should be set properly" do
      expect(score_response.status).to eq(status)
    end

    it "score should be set properly" do
      expect(score_response.score).to eq(score)
    end

    it "workflow_decision_id should be set properly" do
      expect(score_response.workflow_decision_id).to eq(workflow_decision_id)
    end
  end

  context "response should not be stored" do
    it "because order_id is blank" do
      request = Sift::Requests::CreateOrder.new({:$user_id => "10"})
      allow(Sift::Responses::Score).to receive(:sanitize_api_key)
      allow_any_instance_of(Sift::Responses::Score).to receive(:store_response)
      expect(PersonSiftScore).to_not receive(:find_or_initialize_by)
      score_response = Sift::Responses::Score.new({}, request)
    end
  end
end
