require 'rails_helper'

describe Sift::Requests::UpdateAccount do
  let(:request) { Sift::Requests::UpdateAccount.new({}) }

  it "Type should be :update_account" do
    expect(request.request_type).to eq("$#{Sift::EventService::UPDATE_ACCOUNT}")
  end
end
