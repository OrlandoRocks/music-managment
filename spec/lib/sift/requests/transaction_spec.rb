require 'rails_helper'

describe Sift::Requests::Transaction do
  let(:order_id) { "test_order_id".freeze }
  let(:amount) { 250000 }
  let(:request) { 
    Sift::Requests::Transaction.new(
      {
        :$order_id => order_id,
        :$amount => amount
      }
    ) 
  }

  it "Type should be :transaction" do
    expect(request.request_type).to eq("$#{Sift::EventService::TRANSACTION}")
  end

  it "order_id should be set properly" do
    expect(request.order_id).to eq(order_id)
  end

  it "amount should be set properly" do
    expect(request.amount).to eq(amount)
  end

  context "valid? should be" do
    let(:invalid_request) { 
      Sift::Requests::Transaction.new(
        {
          :$order_id => order_id,
          :$amount => -10
        }
      ) 
    }

    it "true" do
      expect(request.valid?).to eq(true)
    end

    it "false" do
      expect(invalid_request.valid?).to eq(false)
    end
  end
end
