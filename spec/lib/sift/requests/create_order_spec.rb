require 'rails_helper'

describe Sift::Requests::CreateOrder do
  let(:order_id) { "test_order_id".freeze }
  let(:request) { Sift::Requests::CreateOrder.new({:$order_id => order_id}) }

  it "Type should be :create_order" do
    expect(request.request_type).to eq("$#{Sift::EventService::CREATE_ORDER}")
  end

  it "order_id should be equal to the one in the body" do
    expect(request.order_id).to eq(order_id)
  end
end
