require 'rails_helper'

describe Sift::Requests::Base do
  let(:person_id) { 134 }
  let(:order_id) { "test_order_id".freeze }
  let(:amount) { 250000 }
  let(:body) {
    {
      :$user_id => person_id
    }
  }
  let(:request) { 
    Sift::Requests::Base.new(body, false) 
  }

  it "person_id should be set properly" do
    expect(request.person_id).to eq(person_id)
  end

  context "valid? should be" do
    context "true because" do
      it "body is valid" do
        expect(request.valid?).to eq(true)
      end

      it "is_first_time_buyer is TruthClass" do
        invalid_body = body.merge({:is_first_time_buyer => true})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(true)
      end

      it "is_first_time_buyer is FalseClass" do
        invalid_body = body.merge({:is_first_time_buyer => false})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(true)
      end
    end

    context "false because" do
      it "releases_active is not an integer" do
        invalid_body = body.merge({:releases_active => "Not a Number"})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(false)
      end

      it "releases_taken_down is not an integer" do
        invalid_body = body.merge({:releases_taken_down => "Not a Number"})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(false)
      end

      it "corporate_entity is not a string" do
        invalid_body = body.merge({:corporate_entity => :Not_A_String})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(false)
      end

      it "is_first_time_buyer is not a bool" do
        invalid_body = body.merge({:is_first_time_buyer => "Not a Bool"})
        invalid_request = Sift::Requests::Base.new(invalid_body, false)
        expect(invalid_request.valid?).to eq(false)
      end
    end
  end
end
