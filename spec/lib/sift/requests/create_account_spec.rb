require 'rails_helper'

describe Sift::Requests::CreateAccount do
  let(:request) { Sift::Requests::CreateAccount.new({}) }

  it "Type should be :create_account" do
    expect(request.request_type).to eq("$#{Sift::EventService::CREATE_ACCOUNT}")
  end
end
