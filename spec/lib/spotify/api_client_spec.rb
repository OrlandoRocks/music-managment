require "rails_helper"

describe Spotify::ApiClient do
  include SpotifyApiHelper

  before :each do
    @faraday = fake_spotify_client
    allow(Faraday).to receive(:new).and_return(@faraday)
    @client = Spotify::ApiClient.new
  end

  it "refreshes the bearer token on initialization" do
    expect(@client.client.headers["Authorization"].include?("Bearer")).to be true
  end

  it "sets the correct base url" do
    expect(@client.client.url_prefix.to_s.include?("fake.spotify.com")).to be true
  end

  describe "#find_by_upc" do
    context "success" do
      context "when one album is found" do
        it "extracts a simplified album object from the Spotify response" do
          upc = create(:tunecore_upc)
          resp = double(success?: true, body: album_from_upc_query([upc.number]))

          allow(@faraday).to receive(:get).and_return(resp)
          simple_album = @client.find_by_upc(upc.number)

          expect(simple_album).to be_a(Hash)
          expect(simple_album["id"]).to_not be nil
        end
      end

      context "when more than one album is found" do
        it "finds and extracts a simplified album object from the Spotify response" do
          upc = create(:tunecore_upc)
          album = upc.upcable
          resp = double(success?: true, body: multiple_albums_from_upc_query(upc.number))

          allow(@faraday).to receive(:get).and_return(resp)
          simple_album = @client.find_by_upc(upc.number)

          expect(simple_album).to be_a(Hash)
          expect(simple_album["id"]).to_not be nil
          expect(simple_album["name"]).to eq(album.name)
        end
      end
    end

    context "failure" do
      it "should retry exection if the rate limit is exceeded" do
        upc = create(:tunecore_upc)
        failure_resp = double(success?: false, body: {"error"=>{"status"=>401, "message"=>"The access token expired"}}.to_json, status: 401)
        success_resp = double(success?: true, body: album_from_upc_query([upc.number]))
        allow(@faraday).to receive(:get).and_return(failure_resp, success_resp)
        simple_album = @client.find_by_upc('')

        expect(simple_album).to be(nil)
      end
    end

    it "returns 'nil' if the album is not found" do
      resp = double(success?: true, body: album_from_upc_query([""]))
      allow(@faraday).to receive(:get).and_return(resp)

      expect(@client.find_by_upc('')).to be nil
    end
  end

  describe "#find_by_artist_name" do
    context "success" do
      it "extracts simplified artist objects from the Spotify response" do
        resp = double(success?: true, body: {"artists": {"items": [{"id": "lkam23l", "name": 'John Smith'}]}}.to_json)

        allow(@faraday).to receive(:get).and_return(resp)
        simple_artists = @client.find_by_artist_name('John Smith')

        expect(simple_artists).to be_a(Array)
        expect(simple_artists.first["id"]).to_not be nil
        expect(simple_artists.first["name"]).to_not be nil
      end
    end

    context "no matching names" do
      it "responds with empty array" do
        resp = double(success?: true, body: {"artists": {"items": [{"id": "lkam23l", "name": 'John Smith'}]}}.to_json)

        allow(@faraday).to receive(:get).and_return(resp)
        simple_artists = @client.find_by_artist_name('John Wick')

        expect(simple_artists).to be_a(Array)
        expect(simple_artists.length).to eq(0)
      end
    end
  end

  describe "#get_valid_artist_by_id, " do
    context "with matching name, " do
      it "returns the artist" do
        resp = double(success?: true, body: {"id": "lkam23l", "name": 'John Smith'}.to_json)

        allow(@faraday).to receive(:get).and_return(resp)
        result = JSON.parse(@client.get_valid_artist_by_id('lkam23l', 'John Smith').body)
        resp_json = JSON.parse(resp.body)

        expect(result['id']).to eq(resp_json['id'])
        expect(result['name']).to eq(resp_json['name'])
      end
    end

    context "non-matching name" do
      it "returns error" do
        resp = double(success?: true, body: {"id": "lkam23l", "name": 'John Smith'}.to_json)

        allow(@faraday).to receive(:get).and_return(resp)
        result = @client.get_valid_artist_by_id('lkam23l', 'Bon Sith')

        expect(result.status).to eq(403)
      end
    end
  end

  describe "#spotify_recent_albums" do
    context "success" do
      it "extracts most recent albums from the Spotify response" do
        resp = double(success?: true, body: {"items": [{"id": "lkam23l", "name": 'Big Hit'}]}.to_json)

        allow(@faraday).to receive(:get).and_return(resp)
        simple_tracks = @client.spotify_recent_albums('lk1m23lkmaf')

        expect(simple_tracks).to be_a(Array)
        expect(simple_tracks.first["id"]).to_not be nil
        expect(simple_tracks.first["name"]).to_not be nil
      end
    end
  end

  describe "#get_album_tracks" do
    it "requests the full album object first" do
      resp = double(success?: true, body: get_album_tracks([""]))
      allow(@faraday).to receive(:get).and_return(resp)

      expect(@faraday).to receive(:get).with("albums/uri!")

      @client.get_album_tracks("uri!")
    end

    it "returns the array of track objects" do
      resp = @client.get_album_tracks("1234")

      expect(resp).to be_a(Array)
      expect(resp.first["external_ids"]["isrc"]).to_not be nil
      expect(resp.first["id"]).to_not be nil
    end
  end

  describe "#find_artists_by_album_uri" do
    context "if spotify tracks and artist information exists on spotify" do
      it "returns a hash which maps spotify artist name with the artist id" do
        external_service_id = create(:external_service_id, linkable: create(:album), identifier: "100001023", service_name: "spotify")
        resp = double(success?: true, body: get_all_album_tracks(external_service_id.identifier))
        allow(@faraday).to receive(:get).and_return(resp)
        resp = @client.find_artists_by_album_uri("100001023")

        expect(resp).to be_a(Hash)
        expect(resp.first).to_not be nil
      end
    end

    context "if the album tracks are not found" do
      it "returns nil" do
        resp = double(success?: true, body: get_all_album_tracks(""))
        allow(@faraday).to receive(:get).and_return(resp)
        resp = @client.find_artists_by_album_uri("")

        expect(resp).to be nil
      end
    end
  end
end
