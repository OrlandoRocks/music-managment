require "rails_helper"

describe Tunecore::CertEngine::MultiyearProductIds do
  describe "#multi_year_product_ids" do
    let(:dummy_class) { Class.new { extend Tunecore::CertEngine::MultiyearProductIds } }

    it "doesn't contain the ids of products that can have certs applied" do
      # create all non multi year products
      ids = Product::PRODUCT_COUNTRY_MAP.map { |_k, v| v[:three_ringtone_credits] }
      CountryWebsite.all.each do |country_website|
        ["ONE"].each do |duration|
          ["ALBUM", "SINGLE"].each do |type|
            ids << "Product::#{country_website.country}_#{duration}_YEAR_#{type}_PRODUCT_ID".constantize
          end
        end
      end
      [ids].each do |id|
        expect(dummy_class.multi_year_product_ids.include? id).to eq false
      end
    end

  end
end
