require "rails_helper"

describe Tunecore::CertEngine::GcDistro do
  describe "When applying cert to item" do
    before(:each) do
      @person = TCFactory.create(:person)
      @redemption_product_ids = {:album => Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID, :single => Product::US_ONE_SINGLE_CREDIT_PRODUCT_ID, :ringtone => Product::US_ONE_RINGTONE_CREDIT_PRODUCT_ID}
      @ad_hoc_product_ids = {:album => Product::US_ONE_YEAR_ALBUM_PRODUCT_ID, :single => Product::US_ONE_YEAR_SINGLE_PRODUCT_ID, :ringtone => Product::US_ONE_YEAR_RINGTONE_PRODUCT_ID}
    end

    it "should only be applied to redemption product that matches the engine params" do
      @redemption_product_ids.each do |k, product_id|
        cert = create_cert({:engine_params => k.to_s}, {:cert => "GCDIST#{product_id.to_s}"})
        product = Product.find(product_id)
        @purchase =  TCFactory.create(:purchase, :product => product, :person => @person, :related_type => 'Product', :related_id => product.id)
        verified = Cert.verify verify_opts(:entered_code => cert.cert)
        expect(verified).to be_kind_of(Cert)
      end
    end

    it "should only be applied to distribution product that matches the engine params" do
      @ad_hoc_product_ids.each do |key, product_id|
        cert = create_cert({:engine_params => key.to_s}, {:cert => "GCDIST#{product_id.to_s}"})
        related = create_related_product(key)
        @purchase =  TCFactory.create(:purchase, :product_id => product_id, :person_id => @person.id, :related => related)
        verified = Cert.verify verify_opts(:entered_code => cert.cert)
        expect(verified).to be_kind_of(Cert)
      end
    end

    it "should not work for item that does not match the engine params" do
      cert = create_cert({:engine_params => 'album'}, {:cert => "GCDIST1234"})
      product = Product.find(Product::US_ALBUM_PRODUCT_IDS[1])
      expect(@redemption_product_ids.collect {|k,v| v }).not_to include(product.id)
      @purchase = TCFactory.create(:purchase, :product => product, :person_id => @person.id, :related_type => 'Product', :related_id => product.id)
      verified = Cert.verify verify_opts(:entered_code => cert.cert)
      expect(verified).not_to be_kind_of(Cert)
    end

    it "should not work when invalid engine_params is entered" do
      cert = create_cert({:engine_params => 'distribute'}, {:cert => "GCDIST1235"})
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
      @purchase =  TCFactory.create(:purchase, :product => product, :person => @person, :related_type => 'Product', :related_id => product.id)
      verified = Cert.verify verify_opts(:entered_code => cert.cert)
      expect(verified).not_to be_kind_of(Cert)
    end

    it "should apply the correct discount amount" do
      cert = create_cert({:engine_params => 'album'}, {:cert => "GCDIST1234"})
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
      @purchase = TCFactory.create(:purchase, :product => product, :person => @person, :related_type => 'Product', :related_id => product.id)
      price_calculator = @purchase.price_calculator #using price_calculator to avoid having to trigger the calculation using normal workflow
      verified = Cert.verify verify_opts(:entered_code => cert.cert)
      expect(verified).to be_kind_of(Cert)
      verified.discount_amount_cents(price_calculator)
      expect(verified.last_discount_amount_cents).to eq(2999)
    end

    it "should reset the redeem status when customer remove the item from the cart" do
      cert = create_cert({:engine_params => 'album'}, {:cert => "GCDIST1234"})
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
      @purchase =  TCFactory.create(:purchase, :product => product, :person => @person, :related_type => 'Product', :related_id => product.id)
      verified = Cert.verify verify_opts(:entered_code => cert.cert)
      expect(verified).to be_a(Cert)
      expect(verified.redeemed?).to be_truthy
      @purchase.destroy
      expect(verified.reload.redeemed?).to be_falsey
    end

    it "should be case insensitive when matching engine params" do
      cert = create_cert({:engine_params => 'Album'},{:cert => 'VALIDCERT'})
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
      @purchase =  TCFactory.create(:purchase, :product => product, :person => @person, :related_type => 'Product', :related_id => product.id)
      verified = Cert.verify verify_opts(:entered_code => 'VALIDCERT')
      expect(verified).to be_a(Cert)
    end
  end

  def create_cert(cert_batch_options ={}, cert_options = {})
    @cert_batch = TCFactory.build(:cert_batch,{
      :cert_engine => 'GcDistro',
      :spawning_code => nil,
      :brand_code => 'GCDIST',
      :engine_params => 'album'}.merge(cert_batch_options))

    cert = Cert.create({
      :cert_engine => @cert_batch.cert_engine,
      :cert => 'GCDIST1234',
      :expiry_date => 30.days.from_now,
      :engine_params => @cert_batch.engine_params,
      :person_id => nil,
      :album_id => nil,
      :total_amount => nil,
      :cert_batch => @cert_batch
    }.merge(cert_options))
    cert
  end

  def verify_opts(options={})
    {
      :entered_code => '0001',
      :purchase => @purchase,
      :current_user => @person
    }.merge(options)
  end

  def create_related_product(related)
    product = case related
      when :album
        TCFactory.create(:album, :person => @person)
      when :single
        FactoryBot.create(:single, :person => @person)
      when :ringtone
        FactoryBot.create(:ringtone, :person => @person)
    end
  end
end
