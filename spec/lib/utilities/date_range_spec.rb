require "rails_helper"


describe Utilities::DateRange, "when initializing a new instance" do
  it "should raise an error if the date_range type is not a symbol" do
    expect { Utilities::DateRange.new("day", '2008-12-02') }.to raise_error(RuntimeError, /DateRange type must be a symbol/)
  end
  
  it "should raise and error if the date is not a String, Date, or Time object" do
    expect { Utilities::DateRange.new(:day, :symbol)}.to raise_error(RuntimeError, /You must supply a date as a String, Date or Time object/)
  end
  
  it "should create a new instance with a date string" do 
    expect {Utilities::DateRange.new(:date, '2009-12-01')}.not_to raise_error
  end
  
  it "should create a new instance with Date object" do
    expect {Utilities::DateRange.new(:date, Date.new)}.not_to raise_error
  end
  
  it "should create a new instance with a Time object" do
    expect {Utilities::DateRange.new(:date, Time.now)}.not_to raise_error
  end
  
  it "should raise an error if given nil" do
    expect { 
      Utilities::DateRange.new(:day, nil)
    }.to raise_error(RuntimeError, /You must supply a date as a String, Date or Time object/)
  end

  it "should raise an error if a string can't be parsed into a valid date" do
    expect { 
      Utilities::DateRange.new(:day, '2009-99-99')
    }.to raise_error(ArgumentError, /2009-99-99 is an invalid date/)
  end
end

describe Utilities::DateRange, "when asking for date ranges" do 
  it "should return a good range for #day" do
    date_range = Utilities::DateRange.new(:day, '2008-03-11')
    
    expect(date_range.start.to_s).to eq('2008-03-11')
    expect(date_range.stop.to_s).to eq('2008-03-12')
  end
  
  it "should return a good range for #date_range_week when starting on the first of the year" do
    date_range = Utilities::DateRange.new(:week, '2008-01-01')
    expect(date_range.start.to_s).to eq('2007-12-31')
    expect(date_range.stop.to_s).to eq('2008-01-07')
  end
  
  it "should return a good range for #date_range_week when starting on Sunday" do
    date_range = Utilities::DateRange.new(:week, '2008-03-09')
    expect(date_range.start.to_s).to eq('2008-03-03')
    expect(date_range.stop.to_s).to eq('2008-03-10')
  end
  
   it "should return a good range for #date_range_week when starting on Wednesday" do
     date_range = Utilities::DateRange.new(:week, '2008-03-12')
     expect(date_range.start.to_s).to eq('2008-03-10')
     expect(date_range.stop.to_s).to eq('2008-03-17')
   end
    
   it "should return a good range for #date_range_week when starting on Saturday" do
     date_range = Utilities::DateRange.new(:week, '2008-03-15')
     expect(date_range.start.to_s).to eq('2008-03-10')
     expect(date_range.stop.to_s).to eq('2008-03-17')
   end
   
   it "should return a good range for #date_range_month when starting on the first" do
     date_range = Utilities::DateRange.new(:month, '2010-08-01')
     expect(date_range.start.to_s).to eq('2010-08-01')
     expect(date_range.stop.to_s).to eq('2010-09-01')
   end
   
   it "should return a good range for #date_range_month when starting on the 15th" do
     date_range = Utilities::DateRange.new(:month, '2008-03-15')
     expect(date_range.start.to_s).to eq('2008-03-01')
     expect(date_range.stop.to_s).to eq('2008-04-01')
   end
   
   it "should return a good range for #date_range_month when starting on the last day of the month" do
     date_range = Utilities::DateRange.new(:month, '2008-04-30')
     expect(date_range.start.to_s).to eq('2008-04-01')
     expect(date_range.stop.to_s).to eq('2008-05-01')
   end
   
   it "should return a good range for #date_range_quarter when starting on the first day of the year" do
     date_range = Utilities::DateRange.new(:quarter, '2007-01-01')
     expect(date_range.start.to_s).to eq('2007-01-01')
     expect(date_range.stop.to_s).to eq('2007-04-01')
   end
   
   it "should return a good range for #date_range_quarter when starting on my birthday" do
     date_range = Utilities::DateRange.new(:quarter, '1978-08-07')
     expect(date_range.start.to_s).to eq('1978-07-01')
     expect(date_range.stop.to_s).to eq('1978-10-01')
   end
    
   it "should return a good range for #date_range_quarter when starting on the last day of the year" do
     date_range = Utilities::DateRange.new(:quarter, '2006-12-31')
     expect(date_range.start.to_s).to eq('2006-10-01')
     expect(date_range.stop.to_s).to eq('2007-01-01')
   end    
   
   it "should return a good range for a year starting on the last day" do
     date_range = Utilities::DateRange.new(:year, '2006-12-31')
     expect(date_range.start.to_s).to eq('2006-01-01')
     expect(date_range.stop.to_s).to eq('2007-01-01')
   end
   
   it "should return a good range for a year starting on the first day" do
     date_range = Utilities::DateRange.new(:year, '2012-01-01')
     expect(date_range.start.to_s).to eq('2012-01-01')
     expect(date_range.stop.to_s).to eq('2013-01-01')
   end
   
   it "should return a good range for a year starting in the middle" do
     date_range = Utilities::DateRange.new(:year, '1978-08-07')
     expect(date_range.start.to_s).to eq('1978-01-01')
     expect(date_range.stop.to_s).to eq('1979-01-01')
   end
end


describe Utilities::DateRange, "when calling #identifier" do
  before(:each) do 
    #@data_report = TCFactory.build_data_report
    @date_range = Utilities::DateRange
  end
  
  #it "should return :live when asked about a :live" do
  #  @data_report.resolution_identifier(:live, '2003-12-31'.to_date).should == :live
  #end
  
  it "should return a :D symbol when asked about a day (leap year)" do
    expect(@date_range.new(:day, '2008-12-31').identifier).to eq(:D366_2008)
  end
  
  it "should return a :D symbol when asked about a day (not-leap year)" do
    expect(@date_range.new(:day, '2007-12-31').identifier).to eq(:D365_2007)
  end
  
  it "should return a :D symbol when asked about the first day of a year" do
    expect(@date_range.new(:day, '2008-01-01').identifier).to eq(:D1_2008)
  end
  
  it "should return a :W symbol when asked about a week (first day of the year)" do
    expect(@date_range.new(:week, '2008-01-01').identifier).to eq(:W1_2008) #yes, commercial weeks can span years.  Wikipedidia that!
  end
  
  it "should return a :W symbol when asked about a week calendar year change over" do
    expect(@date_range.new(:week, '2007-12-30').identifier).to eq(:W52_2007) #yes, commercial weeks can span years.  Wikipedidia that!
  end
  
  it "should return a :W symbol when asked about a week (last day of the year)" do
    expect(@date_range.new(:week, '2007-12-31').identifier).to eq(:W1_2008) #yes, commercial weeks can span years.  Wikipedidia that!
  end
  
  it "should return a :M symbol when asked about a month" do
    expect(@date_range.new(:month, '2009-03-15').identifier).to eq(:M3_2009)
  end
  
  it "should return a :Q symbol when asked about a quarter" do 
    expect(@date_range.new(:quarter, '2015-12-31').identifier).to eq(:Q4_2015)
  end
  
  it "should return a :Y symbol when asked about a year" do
    expect(@date_range.new(:year, '2008-01-01').identifier).to eq(:Y2008)
  end
end
