require 'spec_helper'

module Utilities
  describe ImageConverter do
    subject {
      described_class.new(File.open(File.join(
        Rails.root,
        'spec',
        'files',
        'albumcover-cmyk.tiff'
      ), 'rb'))
    }

    it 'should convert image colorspace to rgb' do
      converted_image_file = subject.change_colorspace("RGB")
      converted_image = MiniMagick::Image.open(converted_image_file)
      expect(converted_image.info('%[colorspace]')).to match('RGB')
    end
  end
end
