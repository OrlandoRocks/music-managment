require "rails_helper"

describe Tunecore::StoreManager do

  describe "self.qualified_albums" do
    it "should filter spoken work albums if shazam store passed" do
      person = FactoryBot.create(:person)
      electronic_genre = Genre.find_by(name: "Electronic")
      spoken_work_genre = Genre.find_by(name: "Spoken Word")

      store = Store.where("name = ?", "Shazam").first

      album1 = TCFactory.create(:album, :person => person, :primary_genre => electronic_genre, :finalized_at => Time.now)
      album2 = TCFactory.create(:album, :person => person, :primary_genre => spoken_work_genre, :finalized_at => Time.now)

      qualified = Tunecore::StoreManager.qualified_albums(person)
      expect(qualified.size).to eq(2)

      qualified = Tunecore::StoreManager.qualified_albums(person, store)
      expect(qualified.size).to eq(1)
      expect(qualified.first.id).to eq(album1.id)
    end

    it "should filter iTunes for classical genre" do
      person = FactoryBot.create(:person)
      electronic_genre = Genre.find_by(name: "Electronic")
      classical_genre  = Genre.find_by(name: "Classical")

      store = Store.where("name = ?", "iTunes").first

      album1 = TCFactory.create(:album, :person => person, :primary_genre => electronic_genre, :finalized_at => Time.now)
      album2 = TCFactory.create(:album, :person => person, :primary_genre => classical_genre, :finalized_at => Time.now)

      expect(Tunecore::StoreManager.qualified_albums(person).size).to eq(2)
      qualified = Tunecore::StoreManager.qualified_albums(person, store)
      expect(qualified.size).to eq(1)
      expect(qualified.first.id).to eq(album1.id)
    end
  end

  describe ".store_manager_stores" do
    let(:stores){ Tunecore::StoreManager.store_manager_stores(nil) }

    context "when returning a list of stores" do
      it "ignores AmazonOD" do
        selected = stores.select {|s| s.short_name == "AmazonOD"}
        expect(selected.blank?).to eq(true)
      end

      it "ignores iTunes regional stores" do
        selected = stores.select {|s| s.short_name =~ /iTunes.*/}
        expect(selected.size).to eq(1)
        expect(selected.first.short_name).to eq("iTunesWW")
      end

      it "returns on sale stores before normal stores" do
        sale_store = Store.find_by(short_name: "Pandora")
        sale_store.on_sale = true
        sale_store.save!

        expect(stores.first.name).to eq "Pandora"
      end

      it "returns stores in descending order by launch date, regardless of store ID" do
        launched_today     = TCFactory.create(:store, id: 10000, launched_at: Time.now)
        launched_yesterday = TCFactory.create(:store, id: 10001, launched_at: 1.day.ago)
        stores = Tunecore::StoreManager.store_manager_stores(nil)

        expect(stores.first.id).to eq launched_today.id
      end

      it "returns inactive stores if show inactive stores is added for a person" do
        store  = TCFactory.create(:store, is_active: false)
        person = FactoryBot.create(:person)
        FeatureFlipper.add_feature "show_inactive_stores"
        FeatureFlipper.update_feature "show_inactive_stores", person.id, ""
        stores = Tunecore::StoreManager.store_manager_stores(person)

        expect(stores.map(&:id)).to include store.id
        FeatureFlipper.update_feature "show_inactive_stores", '', ''
      end
    end
  end

  describe "self.create_store_purchases" do
    before(:each) do
      @person = FactoryBot.create(:person)
      FeatureFlipper.update_feature "show_inactive_stores", @person.id, ""
      @album1 = create(:album, :person => @person, :finalized_at => Time.now)
      @album2 = create(:album, :person => @person, :finalized_at => Time.now)

      @spotify  = Store.find(26)
      @pandora  = Store.find_by(short_name: "Pandora")
      @tencent  = Store.find_by(short_name: "Tencent")
      @itunes   = Store.find(36)

      SalepointableStore.create!(:store=>@spotify, :salepointable_type=>"Album")
      SalepointableStore.create!(:store=>@pandora, :salepointable_type=>"Album")
      SalepointableStore.create!(:store=>@itunes, :salepointable_type=>"Album")
      SalepointableStore.create!(:store=>@tencent, :salepointable_type=>"Album")
    end

    it "should create salepoints and purchases" do
      stores_hash = {"#{@spotify.id}" => [@album1.id, @album2.id], "#{@pandora.id}" => [@album1.id, @album2.id], "#{@tencent.id}" => [@album1.id, @album2.id]}
      TCFactory.build_ad_hoc_salepoint_product

      expect {
        expect {
          Tunecore::StoreManager.create_store_purchases(@person, stores_hash)
        }.to change(Salepoint, :count).by(6)
      }.to change(Purchase, :count).by(6)
      FeatureFlipper.update_feature "show_inactive_stores", "", ""
    end
  end

  describe "Store Manager object" do
    let!(:person){ FactoryBot.create(:person) }
    let!(:album1){ FactoryBot.create(:album, :person => person, :finalized_at => Time.now) }
    let!(:album2){ FactoryBot.create(:album, :person => person, :finalized_at => nil) }
    let!(:ringtone){ FactoryBot.create(:ringtone, :person => person, :finalized_at => Time.now) }
    let(:sm){ Tunecore::StoreManager.new(person) }

    before(:each) do
      ringtone.salepoints << TCFactory.build(:salepoint, :store => Store.find(36), :salepointable_type => ringtone.class.name, :salepointable_id => ringtone.id)
    end

    context "when initialized" do
      it "contains a list of all the stores a person can distribute to" do
        expect(sm.stores).to_not be_blank
        expect(sm.stores.first[:missing_releases]).to match_array([album1.id])
      end

      it "contains a list of albums associated with a person" do
        expect(sm.album_hash.keys).to match_array([album1.id])
      end

      it "does not contain ringtones associated with a person" do
        expect(sm.album_hash.keys).to_not include(ringtone.id)
      end

      it "does not contain unfinalized albums" do
        expect(sm.album_hash.keys).to_not include(album2.id)
      end

      it "contains only albums that do not have automator associated" do
        album3 = TCFactory.create(:album, :person => person, :finalized_at => Time.now).add_automator

        sm.automator_hash[:missing_releases].each do |id|
          a = Album.find(id)
          expect(a.salepoint_subscription).to be_blank
          expect(a.finalized?).to be true
          expect(a.supports_subscriptions).to be true
        end
      end

      it "should set up missing_total_count" do
        expect(sm.missing_total_count).to eq 1
      end
    end

    context "when an album uses Automator for a store" do
      let!(:album2){ TCFactory.create(:album, :person => person, :finalized_at => Time.now ) }

      context "and Automator was added before the store launched" do
        it "is excluded from the missing_releases list for that store" do
          time = Time.now

          Store.find_by(name: "Akazoo").update_attribute(:launched_at, time)

          sm = Tunecore::StoreManager.new(person)
          expect( sm.stores.select {|store| store[:name] == "Akazoo" }.first[:missing_releases] ).to include(album2.id)

          TCFactory.create(:salepoint_subscription, effective: time - 13.months, album_id: album2.id)
          sm = Tunecore::StoreManager.new(person)
          expect( sm.stores.select {|store| store[:name] == "Akazoo" }.first[:missing_releases] ).not_to include(album2.id)
        end
      end

      context "and Automator was added after the store launched" do
        it "is included on the missing_releases list for that stored" do
          time = Time.now
          Store.first.update_attribute(:launched_at, time)

          sm = Tunecore::StoreManager.new(person)
          expect( sm.stores.first[:missing_releases] ).to include(album2.id)

          TCFactory.create(:salepoint_subscription, effective: time + 1.year, album_id: album2.id)
          sm = Tunecore::StoreManager.new(person)
          expect( sm.stores.first[:missing_releases] ).to include(album2.id)
        end
      end
    end
  end

end
