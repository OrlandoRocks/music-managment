require "rails_helper"

describe Tunecore::Salepoints::VariablePricing do
  before(:each) do
    file_path = File.join(Rails.root, "spec/support/expected_mappings/", "variable_pricing.yml")
    @expected = YAML.load_file(file_path)
  end

  context "when a salepoint is created for an album" do
    it "should set variable price consistently" do
      @expected['store_price_expected'].each do |store_id, expected_hash|
        salepoint = create_salepoint(store_id)
        expected_id = expected_hash['album']['variable']
        expect(salepoint.variable_price_id).to eq(expected_id)
      end
    end

    it "should set track variable price consistently" do
      @expected['store_price_expected'].each do |store_id, expected_hash|
        salepoint = create_salepoint(store_id)
        expected_id = expected_hash['album']['track']
        expect(salepoint.track_variable_price_id).to eq(expected_id)
      end
    end
  end

  context "when a salepoint is created for a single" do
    it "should set variable price consistently" do
      @expected['store_price_expected'].each do |store_id, expected_hash|
        salepoint = create_salepoint(store_id, :single)
        expected_id = expected_hash['single']['variable']
        expect(salepoint.variable_price_id).to eq(expected_id)
      end
    end

    it "should set track variable price consistently" do
      @expected['store_price_expected'].each do |store_id, expected_hash|
        salepoint = create_salepoint(store_id, :single)
        expected_id = expected_hash['single']['track']
        expect(salepoint.track_variable_price_id).to eq(expected_id)
      end
    end
  end

  context "#price_code" do
    it "returns nil when no strategy or variable_price exists" do
      @expected['stores_no_strategy_no_price'].each do |store_id|
        salepoint = create_salepoint(store_id)
        expect(salepoint.price_code).to be_nil
      end
    end

    it "returns strategy when code exists" do
      @expected['stores_strategy'].each do |store_id|
        store = Store.find(store_id)
        salepoint = create_salepoint(store_id)
        expected_code = "PriceCodeStrategy::#{store.short_name.capitalize}".constantize.price_code(salepoint)
        expect(salepoint.price_code).to eq(expected_code)
      end
    end

    it "returns price code of variable_price if exists and does not have strategy" do
      @expected['stores_with_price'].each do |store_id|
        salepoint = create_salepoint(store_id)
        expected_code = salepoint.variable_price.price_code
        expect(salepoint.price_code).to eq(expected_code)
      end
    end
  end

  context "#salepoints_eligible_for_admin_update" do
    before(:each) do
      @available_strategies =  PriceCodeStrategy::Tunecore::IMPLEMENTED_STORES
    end

    it "returns trues only for available strategies" do
      @available_strategies.each do |as|
        expect(PriceCodeStrategy::Tunecore.available_strategy?(as)).to eq(true)
      end
    end

    it "returns false for all stores that have not implemented_strategies" do
      unavailable_strategies = Store.where("short_name not in (?)", @available_strategies).pluck(:short_name)
      unavailable_strategies.each do |us|
        expect(PriceCodeStrategy::Tunecore.available_strategy?(us.capitalize)).to eq(false)
      end
    end
  end
end

private

def create_salepoint(store_id, type=:album)
  person = FactoryBot.create(:person, email: "#{store_id}@tunecore.com")
  album = FactoryBot.create(type, person: person)
  FactoryBot.create(:salepoint, store_id: store_id, salepointable: album, has_rights_assignment: 1)
end
