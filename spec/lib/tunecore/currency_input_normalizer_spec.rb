require "rails_helper"

describe Tunecore::CurrencyInputNormalizer do
  describe ".normalize_currency_input" do
    it "works for American style currency" do
      currency = {
        dollars: "1,465",
        cents: "23"
      }
      normalized_currency = Tunecore::CurrencyInputNormalizer.normalize_currency_input(currency, :dollars, :cents)
      expect(normalized_currency).to eq("1465.23")
    end

    it "works for German style currency" do
      currency = {
        dollars: "1.465",
        cents: "23"
      }
      normalized_currency = Tunecore::CurrencyInputNormalizer.normalize_currency_input(currency, :dollars, :cents)
      expect(normalized_currency).to eq("1465.23")
    end
  end
end
