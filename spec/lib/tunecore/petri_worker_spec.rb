require "rails_helper"

describe Tunecore::PetriWorker do
  let(:petri_worker) { Tunecore::PetriWorker.new }
  let(:ec2_sdk) { double("ec2_sdk") }

  before do
    allow(AWS::EC2).to receive(:new).and_return(ec2_sdk)
  end

  after { Tunecore::PetriWorker.instance_variable_set(:@ec2_client, nil) }

  describe "#queue_size" do
    it "sends the production petri queue name to the aws sqs wrapper" do
      queue_size = 1

      expect(AwsWrapper::Sqs).to receive(:size)
        .with(queue_name: Tunecore::PetriWorker::PRODUCTION_PETRI_QUEUE)
        .and_return(queue_size)

      expect(petri_worker.queue_size).to eq(queue_size)
    end
  end

  describe "#list" do
    it "returns all running instances with PETRI_WORKER_IMAGE_ID" do
      non_running_instance = double(
        "AWS::EC2::Instance",
        image_id: PETRI_WORKER_IMAGE_ID,
        status: :stopped
      )

      non_petri_instance = double(
        "AWS::EC2::Instance",
        image_id: "DIFFERENT_IMAGE_ID",
        status: :stopped
      )

      running_instances = [
        double("AWS::EC2::Instance", image_id: PETRI_WORKER_IMAGE_ID, status: :running),
        double("AWS::EC2::Instance", image_id: PETRI_WORKER_IMAGE_ID, status: :running)
      ]

      instances = running_instances + [non_running_instance, non_petri_instance]

      allow(ec2_sdk).to receive(:instances).and_return(instances)

      expect(petri_worker.list).to contain_exactly(*running_instances)
    end
  end

  describe "#prune_by" do
    it "destroys the given number of running petri worker instances" do
      instances = (1..6).map do |n|
        double(
          "AWS::EC2::Instance",
          id: "100#{n}",
          image_id: PETRI_WORKER_IMAGE_ID,
          status: :running
        )
      end

      core_response = double("AWS::Core::Response")

      allow(ec2_sdk).to receive(:instances).and_return(instances)

      amount = 3

      expect(ec2_sdk).to receive(:terminate_instances).with(
        instance_ids: instances[0..(amount - 1)].map(&:id)
      ).and_return(core_response)

      expect(petri_worker.prune_by(amount)).to eq(nil)
    end
  end
end
