require "rails_helper"

describe Tunecore::People::SubscriptionService do
  let(:person) { create(:person) }

  describe "#has_subscription_product_in_cart_for?" do
    it "returns false if the user does not have a social subscription product in their cart" do
      expect(person.has_subscription_product_in_cart_for?("Social")).to eq(false)
    end

    it "returns true if the user has a social subscription product in their cart" do
      social_purchase = create(:subscription_purchase, :social_us, person: person)
      social_purchase.purchase.paid_at = nil
      social_purchase.purchase.save
      expect(person.has_subscription_product_in_cart_for?("Social")).to eq(true)
    end
  end

  describe "#has_active_subscription_product_for?" do
    it "returns true when the user has an active social subscription" do
      create(:person_subscription_status, person: person)
      expect(person.has_active_subscription_product_for?("Social")).to eq(true)
    end

    it "returns false when the user does not have an active social subscription" do
      expect(person.has_active_subscription_product_for?("Social")).to eq(false)
    end
  end

  describe "#remove_unpaid_subscriptions_for_social" do
    it "deletes unpaid purchases" do
      social_purchase = create(:subscription_purchase, :social_us, person: person)
      social_purchase.purchase.paid_at = nil
      social_purchase.purchase.related_type = "SubscriptionPurchase"
      social_purchase.purchase.save
      person.remove_unpaid_subscriptions_for_social("Social")
      expect(person.purchases.all.size).to eq 0
    end
  end

  describe "#cancel_subscription_for" do
    it "sets canceled_at date on the subscription status" do
      social_purchase = create(:subscription_purchase, :social_us, person: person)
      status      = create(:person_subscription_status, subscription_type: "Social", person: person)
      create(:subscription_event, :social, person_subscription_status: status, subscription_purchase: social_purchase)
      person.cancel_subscription_for("Social")
      expect(person.subscription_status_for("Social").canceled_at).not_to be_nil
    end
  end

  describe "#subscription_status_by_type" do
    it "grabs the correct person_subscription_status record by subscription_type" do
      social = create(:person_subscription_status, subscription_type: "Social", person: person)
      expect(person.subscription_status_for("Social")).to eq(social)
    end
  end

  describe "#as_json" do
    it "returns social subscription_events in json format" do
      purchase = create(:subscription_purchase, :social_us, person: person)
      status   = create(:person_subscription_status, subscription_type: "Social", person: person)
      event    = create(:subscription_event, :social, person_subscription_status: status, subscription_purchase: purchase)
      actual_data = person.subscription_event_history_for("Social").first
      expect(actual_data["price"]).to eq((event.subscription_purchase.purchase.purchase_total_cents / BigDecimal(100)).to_f)
      expect(actual_data["event_type"]).to eq(event.event_type)
    end
  end

  describe "#subscription_payment_channel_for" do
    it "returns the latest subscription_purchase payment_channel" do
      purchase1 = create(:subscription_purchase, :social_us, person: person, payment_channel: "Apple", termination_date: Date.today - 30.days)
      event = create(:subscription_event, subscription_purchase: purchase1, subscription_type: "Social")
      purchase2 = create(:subscription_purchase, :social_us, person: person, termination_date: Date.today)
      event = create(:subscription_event, subscription_purchase: purchase2, subscription_type: "Social")

      expect(person.subscription_payment_channel_for("Social")).to eq "Tunecore"
    end
  end

  describe "#active_subscription_expires_at" do
    let!(:person_subscription_statuses) { create_list(:person_subscription_status, 2, subscription_type: "Social", person: person) }
    let(:last_subscription_date) { person_subscription_statuses.last.termination_date.to_date }

    it "gets the expiration date of the last subscription of a given type" do
      expect(person.reload.active_subscription_expires_at(subscription_type: "Social")).to eq last_subscription_date
    end
  end

  describe ".with_active_social_subscription" do
    let!(:person_with_active_social_subscription) { create(:person, :with_active_social_subscription_status) }
    let!(:person_without_active_social_subscription) { create(:person) }

    it "returns people with active social subscription statuses" do
      result = Person.with_active_social_subscription

      expect(result).to include(person_with_active_social_subscription)
      expect(result).not_to include(person_without_active_social_subscription)
    end
  end

  describe ".with_active_social_subscription_with_termination_date" do
    let!(:person_with_active_social_subscription) { create(:person, :with_active_social_subscription_status) }
    let!(:person_with_active_social_subscription_without_termination_date) { create(:person, :with_active_social_subscription_status_with_no_termination_date) }
    let!(:person_with_expired_social_subscription) { create(:person, :with_inactive_social_subscription_status)}
    let!(:person_without_social_subscription) { create(:person) }

    it "returns people with expiring (and active) social subscription statuses" do
      result = Person.with_active_social_subscription_with_termination_date

      expect(result).to include(person_with_active_social_subscription)
      expect(result).not_to include(
        person_with_active_social_subscription_without_termination_date,
        person_with_expired_social_subscription,
        person_without_social_subscription
      )
    end
  end
end
