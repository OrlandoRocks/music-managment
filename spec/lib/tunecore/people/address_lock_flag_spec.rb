require "rails_helper"

describe Tunecore::People::AddressLockFlag do
  # This is included in the Person model, so we test there
  let!(:flag) { Flag.find_by(Flag::ADDRESS_LOCKED) }
  let(:reason) { FlagReason.payoneer_kyc_completed_address_lock }
  let!(:person) { create(:person) }

  describe "#address_locked?" do
    it "should return true if the address_locked flag is connected to the person" do
      person.lock_address!(reason: reason)
      expect(person.address_locked?).to eq(true)
    end

    it "should return false if the address_locked flag is not connected to the person" do
      expect(person.address_locked?).to eq(false)
    end
  end

  describe "#lock_address!" do
    it "should successfully set the address_locked flag on the person" do
      person.lock_address!(reason: reason)
      expect(person.address_locked?).to eq(true)
    end

    it "should add only one join association to the flag" do
      person.lock_address!(reason: reason)
      person.lock_address!(reason: reason)
      expect(person.flags.count).to eq(1)
    end

    it "should add a FlagTransition record if transitioning from unlocked to locked" do
      person.lock_address!(reason: reason)
      expect(person.flag_transitions.where(flag_id: flag.id, add_remove: "add").count).to eq(1)
    end

    it "should not add a FlagTransition record if locked state hasn't changed" do
      person.lock_address!(reason: reason)
      person.lock_address!(reason: reason)
      expect(person.flag_transitions.where(flag_id: flag.id, add_remove: "add").count).to eq(1)
    end
  end

  describe "#unlock_address!" do
    it "should successfully unset the address_locked flag on the person" do
      person.lock_address!(reason: reason)
      person.unlock_address!(reason: reason)
      expect(person.address_locked?).to eq(false)
    end

    it "should have the address_locked flag stay unset" do
      person.unlock_address!(reason: reason)
      expect(person.flags.count).to eq(0)
    end

    it "should add a FlagTransition record if transitioning from locked to unlocked" do
      person.lock_address!(reason: reason)
      person.unlock_address!(reason: reason)
      expect(person.flag_transitions.where(flag_id: flag.id, add_remove: "remove").count).to eq(1)
    end

    it "should not add a FlagTransition record if locked state hasn't changed" do
      person.unlock_address!(reason: reason)
      expect(person.flag_transitions.where(flag_id: flag.id, add_remove: "remove").count).to eq(0)
    end
  end
end
