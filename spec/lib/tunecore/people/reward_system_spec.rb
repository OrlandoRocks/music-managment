require "rails_helper"

describe Tunecore::People::RewardSystem do
  let(:person) { create(:person) }

  describe "#rewards" do
    it "should return all active rewards user has access to" do
      tier = create(:tier)
      reward = create(:reward)
      inactive_reward = create(:reward, is_active: false)
      create(:tier_reward, tier: tier, reward: reward)
      create(:tier_reward, tier: tier, reward: inactive_reward)
      create(:tier_person, tier: tier, person: person)

      expect(person.rewards.size).to eq(1)
      expect(person.rewards.ids).to include(reward.id)
    end
  end

  describe "#points_balance" do
    it "should return person_points_balance record for person" do
      balance_record = person.create_person_points_balance

      expect(person.points_balance.id).to eq(balance_record.id)
    end

    it "should create person points balance record if not available" do
      expect {
        person.points_balance
      }.to change(PersonPointsBalance, :count).by(1)

      balance_record = person.person_points_balance
      expect(balance_record.balance).to eq(0)
    end
  end

  describe "#available_points" do
    it "should return points balance" do
      balance_record = person.create_person_points_balance(balance: 100)

      expect(person.available_points).to eq(100)
    end
  end
end
