require "rails_helper"

describe Tunecore::People::VatCompliance do
  let!(:person_without_compliance_info) { create(:person) }
  let!(:person_with_compliance_info) { create(:person, :with_compliance_info) }

  describe ".without_completed_compliance_info" do
    it "should return all people who have not completed all of their compliance info" do
      people = Person.without_completed_compliance_info
      expect(people).not_to include(person_with_compliance_info)
      expect(people).to include(person_without_compliance_info)
    end
  end
end
