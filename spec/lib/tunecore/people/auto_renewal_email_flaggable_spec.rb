require "rails_helper"

describe Tunecore::People::AutoRenewalEmailFlaggable do
  let!(:flag) { Flag.find_by(Flag::AUTO_RENEWAL_EMAIL_SENT) }
  let!(:person) { create(:person) }

  describe "#auto_renewal_email_sent?" do
    it "should return true when the person is flagged to have been sent an email" do
      person.flags << flag
      expect(person.auto_renewal_email_sent?).to eq(true)
    end

    it "should return false if the person is not flagged to have been sent an email" do
      expect(person.auto_renewal_email_sent?).to eq(false)
    end
  end

  describe "#mark_auto_renewal_email_sent!" do
    it "should mark the person's email as sent" do
      person.mark_auto_renewal_email_sent!
      expect(person.auto_renewal_email_sent?).to eq(true)
    end
  end

  describe "#mark_form_incomplete" do
    it "should mark the person's email as unsent" do
      person.flags << flag
      person.mark_auto_renewal_email_unsent!
      expect(person.auto_renewal_email_sent?).to eq(false)
    end
  end

  describe ".with_sent_auto_renewal_email" do
    it "should return all people who have been sent an auto-renewal e-mail" do
      people = Person.with_sent_auto_renewal_email
      expect(people).not_to include(person)

      person.mark_auto_renewal_email_sent!
      people = Person.with_sent_auto_renewal_email
      expect(people).to include(person)
    end
  end
end
