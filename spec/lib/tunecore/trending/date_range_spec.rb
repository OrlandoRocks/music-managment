require "rails_helper"

describe Tunecore::Trending::DateRange do

  before(:each) do
    Timecop.freeze(Time.local(2016, 05, 24))
  end

  after do
    Timecop.return
  end

  context ":en locale" do
    describe "#start_date" do
      it "returns :en version of start_date" do
        dates = Tunecore::Trending::DateRange.new(
          3.months.ago,
          "January 1, 2016",
          "February 1, 2016"
        )

        expect(dates.formatted_start_date).to eq "January 1, 2016"
        expect(dates.start_date).to eq Date.parse "January 1, 2016"
      end

      it "returns default start date of 1 week - 1 day before end date" do
        dates = Tunecore::Trending::DateRange.new(
          3.months.ago,
          nil,
          "February 1, 2016"
        )

        expect(dates.formatted_start_date).to eq "January 26, 2016"
        expect(dates.start_date).to eq Date.parse "January 26, 2016"
      end
    end

    describe "#end_date" do
      it "returns :en version of end_date" do
        dates = Tunecore::Trending::DateRange.new(
          3.months.ago,
          "January 1, 2016",
          "February 1, 2016"
        )

        expect(dates.formatted_end_date).to eq "February 1, 2016"
        expect(dates.end_date).to eq Date.parse "February 1, 2016"
      end

      it "returns last trend data if end_date is not set" do
        dates = Tunecore::Trending::DateRange.new(
          3.month.ago,
          "January 1, 2016",
          nil
        )

        expect(dates.formatted_end_date).to eq 3.month.ago.strftime(dates.date_format)
        expect(dates.end_date).to eq 3.month.ago.to_date
      end


      it "returns default end date of today" do
        dates = Tunecore::Trending::DateRange.new(
          nil,
          "January 1, 2016",
          nil
        )

        expect(dates.formatted_end_date).to eq Date.today.strftime(dates.date_format)
        expect(dates.end_date).to eq Date.today
      end
    end
  end

  context ":de locale" do
    before(:each) do
      I18n.locale = :de
    end

    describe "#start_date" do
      it "returns :de version of start_date" do
        dates = Tunecore::Trending::DateRange.new(
          3.months.ago,
          "Januar 1, 2016",
          "Februar 1, 2016"
        )

        expect(dates.formatted_start_date).to eq "January 1, 2016"
        expect(dates.start_date).to eq Date.parse "January 1, 2016"
      end
    end

    describe "#end_date" do
      it "returns :de version of start_date" do
        dates = Tunecore::Trending::DateRange.new(
          3.months.ago,
          "Januar 1, 2016",
          "Februar 1, 2016"
        )

        expect(dates.formatted_end_date).to eq "February 1, 2016"
        expect(dates.end_date).to eq Date.parse "February 1, 2016"
      end
    end
  end
end
