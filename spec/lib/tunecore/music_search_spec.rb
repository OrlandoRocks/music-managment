require "rails_helper"

describe Tunecore::MusicSearch do
  before do
    allow_any_instance_of(Song).to receive(:duration).and_return(95000)
  end

  describe "::album_search" do
    before(:each) do
      @person = create(:person)
      @album1 = create(:album, :finalized, person: @person, name: "Album")
      @album2 = create(:album, :finalized, person: @person, name: "Test")
      @album3 = create(:album, :finalized, person: @person, name: "Album3")
      [@album1, @album2, @album3].each do |album|
        pb = create(:petri_bundle, album: album)
        create(:distribution, :delivered_to_streaming, petri_bundle: pb)
      end
      @song1 = create(:song, :with_s3_asset, album: @album1, name: "song1")
      @song2 = create(:song, :with_s3_asset, album: @album1, name: "song2")
      @song3 = create(:song, :with_s3_asset, album: @album1, name: "song3")
      @song4 = create(:song, :with_s3_asset, album: @album1, name: "test song4")
    end

    it "should by default return the users albums" do
      albums = Tunecore::MusicSearch.album_search(@person)
      expect(albums.size).to eq(3)
      expect(albums.include?(@album1)).to eq(true)
      expect(albums.include?(@album2)).to eq(true)
      expect(albums.include?(@album3)).to eq(true)
    end

    it "should return all albums matching the search term (case insensitive match)" do
      expect(Tunecore::MusicSearch.album_search(@person, search: "album")).to match_array([@album1, @album3])
    end

    it "should set song_matched as true if a song was matched on an album" do
      expect(Tunecore::MusicSearch.album_search(@person, search: "song1").first.song_matched).to eq(1)
    end

    it "should set song_matched as false if the album matches the search term" do
      expect(Tunecore::MusicSearch.album_search(@person, search: "test").first.song_matched).to eq(0)
    end

    it "should only return singles if the release_type is 'Single'" do
      @single = FactoryBot.create(:single, person: @person, finalized_at: Time.now)
      create(:distribution, :delivered_to_streaming, petri_bundle: create(:petri_bundle, album: @single))
      expect(Tunecore::MusicSearch.album_search(@person, release_type: 'Single')).to eq([@single])
    end
  end

  describe "::song_search" do
    before(:each) do
      @person = create(:person)
      @album1 = create(:album, :finalized, person: @person, name: "Album")
      @song1 = create(:song, :with_s3_asset, album: @album1, name: "song1")
      @song2 = create(:song, :with_s3_asset, album: @album1, name: "song2")
      @album2 = create(:album, :finalized, person: @person, name: "Songs")
      @song3 = create(:song, :with_s3_asset, album: @album2, name: "album song1")
      @song4 = create(:song, :with_s3_asset, album: @album2, name: "song 4")
      [@album1, @album2].each do |album|
        create(:distribution, :delivered_to_streaming, petri_bundle: create(:petri_bundle, album: album))
      end
    end

    it "should return all matching songs with the search terms" do
      expect(Tunecore::MusicSearch.song_search(@person, search: "album")).to eq([@song3])
    end
  end

  describe "::soundout_report_search" do
    before(:each) do
      @person = create(:person)
      @album1 = create(:album, :finalized, person: @person, name: "Album")
      @song1 = create(:song, :with_s3_asset, album: @album1, name: "song1")
      @song2 = create(:song, :with_s3_asset, album: @album1, name: "song2")
      @album2 = create(:album, :finalized, person: @person, name: "Songs")
      @song3 = create(:song, :with_s3_asset, album: @album2, name: "album song1")
      @song4 = create(:song, :with_s3_asset, album: @album2, name: "song 4")
      [@album1, @album2].each do |album|
        create(:distribution, :delivered_to_spotify_find_or_create_salepoint, petri_bundle: create(:petri_bundle, album: album))
      end
      @soundout_report1 = create(:soundout_report, track: @song1, person: @person)
      @soundout_report2 = create(:soundout_report, track: @song2, person: @person)
      so_product = create(:soundout_product)
      create(:purchase, related: @soundout_report1, paid_at: Time.now, person: @person, product: so_product.product)
      create(:purchase, related: @soundout_report2, paid_at: Time.now, person: @person, product: so_product.product)
    end

    it "should return all reports with songs matching the search term" do
      expect(Tunecore::MusicSearch.soundout_report_search(@person, search: "song1")).to eq([@soundout_report1])
    end

    it "should return all reports for the user when there is no search term" do
      results = Tunecore::MusicSearch.soundout_report_search(@person)
      expect(results.length).to eq(2)
      expect(results.include?(@soundout_report1)).to be true
      expect(results.include?(@soundout_report2)).to be true
    end

  end
end
