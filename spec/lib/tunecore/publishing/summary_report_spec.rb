require "rails_helper"

describe Tunecore::Publishing::SummaryReport do
  class TestSubject
    include Tunecore::Publishing::SummaryReport
  end

  let(:bucket_name) { "bucket_name" }
  let(:filename) { "file.xls" }

  describe ".write_file_to_s3" do
    it "calls the aws wrapper with bucket_name, filename, data, and options" do
      data = double("io")

      expect(AwsWrapper::S3).to receive(:write).with(
        bucket: bucket_name,
        key: filename,
        data: data,
        options: {
          cache_control: "no-cache",
          acl: :authenticated_read,
          content_type: "application/vnd.ms-excel"
        }
      )

      TestSubject.write_file_to_s3(bucket_name, filename, data)
    end
  end

  describe ".file_last_modified" do
    it "calls the sdk wrapper with bucket_name and filename, returns the :last_modified time from the metadata" do
      time = Time.current

      expect(AwsWrapper::S3).to receive(:metadata).with(
        bucket: bucket_name,
        key: filename
      ).and_return(last_modified: time)

      expect(TestSubject.file_last_modified(bucket_name, filename)).to eq(time)
    end
  end
end
