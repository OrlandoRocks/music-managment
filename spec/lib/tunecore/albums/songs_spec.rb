require "rails_helper"

describe Tunecore::Albums::Songs do

  describe "#esis_for_songs_by_service" do

    it "returns all external_service_ids for songs on an album given a service_name" do
      album     = create(:album, :with_uploaded_songs, number_of_songs: 3)
      song_esis = album.songs.map { |song| create(:external_service_id, linkable: song, service_name: "spotify") }

      results = album.esis_for_songs_by_service("spotify")
      expect(results).to match_array(song_esis)
    end
  end

  describe "#song_order=" do
    it "changes the order of the songs" do
      album = create(:album)

      song_1 = create(:song, album: album, track_num: 1)
      song_2 = create(:song, album: album, track_num: 2)
      song_3 = create(:song, album: album, track_num: 3)
      song_4 = create(:song, album: album, track_num: 4)

      album.reload

      new_order = [song_2.id, song_3.id, song_1.id, song_4.id]

      album.update(song_order: new_order)
      expect(album.songs.pluck(:id)).to eq(new_order)
    end
  end
end
