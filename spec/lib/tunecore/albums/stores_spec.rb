require "rails_helper"

describe Tunecore::Albums::Stores do

  describe "#has_store?" do

    before(:each) do
      @album = create(:album)
      store = build(:store, short_name: "Spotify")
      allow(@album).to receive(:stores).and_return([store])
    end

    context "given the short_name of a store that the album has" do
      it "returns true" do
        expect(@album.has_store?("Spotify")).to be_truthy
      end
    end

    context "given the short_name of a store that the album does NOT have" do
      it "returns false" do
        expect(@album.has_store?("iTunesWW")).to be_falsey
      end
    end
  end

  describe "#stores_by_short_name" do

    it "returns a list of short_names from an album's stores" do
      album = create(:album)
      storeA = build(:store, short_name: "Short Name A")
      storeB = build(:store, short_name: "Short Name B")
      allow(album).to receive(:stores).and_return([storeA, storeB])

      expect(album.stores_by_short_name).to match_array ["Short Name A", "Short Name B"]
    end
  end

  describe "#live_in_itunes?" do

    it "returns true if album has apple_id" do
      album = create(:album)
      create(:external_service_id, linkable: album, service_name: "apple")
      expect(album.live_in_itunes?).to be_truthy
    end

    it "returns false if album does NOT have apple_id" do
      album = create(:album)
      allow(album).to receive(:apple_identifier).and_return(nil)
      expect(album.live_in_itunes?).to be_falsey
    end
  end

  describe 'available_stores' do
    let(:album) { create(:album) }
    let(:storeA) { create(:store, short_name: "Store A") }
    let(:storeB) { create(:store, short_name: "Store B") }
    let(:person) { create(:person) }
    let(:person2) { create(:person) }

    it 'should return available stores' do
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      expect(album.available_stores.map(&:short_name)).to include(storeA.short_name, storeB.short_name)
    end

    it 'should not return shazam store if primary genre is spoken word ' do
      album_1 = create(:album, primary_genre_id: Genre.spoken_word_genre.id)
      expect(album_1.available_stores.map(&:short_name)).not_to include('Shazam')
    end

    it 'should not return itunes stores if primary genre is classical ' do
      album_2 = create(:album, primary_genre_id: Genre.classical_genre.id)
      expect(album_2.available_stores.map(&:short_name)).not_to include('iTunes')
    end

    it 'should return active stores if person_id is nil' do
      storeB.update(is_active: false)
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      expect(album.available_stores.map(&:short_name)).to include(storeA.short_name)
    end

    it 'should return stores based on feature flipper settings' do
      storeB.update(is_active: false)
      FeatureFlipper.add_feature "show_inactive_stores"
      FeatureFlipper.update_feature "show_inactive_stores", person.id, ""
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      expect(album.available_stores(person.id).map(&:short_name)).to include(storeA.short_name, storeB.short_name)
      expect(album.available_stores(person2.id).map(&:short_name)).to include(storeA.short_name)
    end
  end

  describe 'selected_stores' do
    let(:album) { create(:album) }
    let(:storeA) { create(:store, short_name: "Store A") }
    let(:storeB) { create(:store, short_name: "Store B") }
    let(:person) { create(:person) }
    let(:person2) { create(:person) }

    it 'should return selected stores' do
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      create(:salepoint, salepointable: album, store_id: storeA.id)
      create(:salepoint, salepointable: album, store_id: storeB.id)
      expect(album.selected_stores.map(&:short_name)).to include(storeA.short_name, storeB.short_name)
    end

    it 'should return active stores if person_id is nil' do
      storeB.update(is_active: false)
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      create(:salepoint, salepointable: album, store_id: storeA.id)
      create(:salepoint, salepointable: album, store_id: storeB.id)
      expect(album.selected_stores.map(&:short_name)).to include(storeA.short_name)
    end

    it 'should return stores based on feature flipper settings' do
      storeB.update(is_active: false)
      FeatureFlipper.add_feature "show_inactive_stores"
      FeatureFlipper.update_feature "show_inactive_stores", person.id, ""
      create(:salepointable_store, store: storeA)
      create(:salepointable_store, store: storeB)
      create(:salepoint, salepointable: album, store_id: storeA.id)
      create(:salepoint, salepointable: album, store_id: storeB.id)
      expect(album.selected_stores(person.id).map(&:short_name)).to include(storeA.short_name, storeB.short_name)
      expect(album.selected_stores(person2.id).map(&:short_name)).to include(storeA.short_name)
    end
  end
end
