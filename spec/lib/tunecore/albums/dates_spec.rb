require "rails_helper"

describe Tunecore::Albums::Dates do
  describe "#set_default_orig_release_year" do
    let(:album)  { FactoryBot.build(:album) }

    context "an album with orig_release_year > sale_date" do
      it "orig_release_year is set to sale_date on save when not_previously_released is not received" do
        album.assign_attributes(orig_release_year: Time.now, sale_date: Time.now - 2.days)
        expect(album.save).to eq(true)
        expect(album.orig_release_year).to eq(album.sale_date)
      end

      it "validation error when not_previously_released attribute is received" do
        album.assign_attributes(orig_release_year: Time.now, sale_date: Time.now - 2.days, not_previously_released: "true")
        expect(album.save).to eq(false)
        expect(album.errors[:orig_release_year]).to eq(["can't be after Digital Release Date"])
      end
    end

    context "creating an album with orig_release_year < sale_date" do
      it "leaves orig_release_year the same" do
        orig_release_year = Date.today - 2.days
        sale_date = Time.now
        album.assign_attributes(orig_release_year: orig_release_year, sale_date: sale_date)
        expect(album.save).to eq(true)
        expect(album.orig_release_year).to eq(orig_release_year)
      end
    end

    context "creating an album with orig_release_year = sale_date" do
      it "leaves orig_release_year and sale_date the same" do
        same_time = Time.now
        album.assign_attributes(orig_release_year: same_time, sale_date: same_time)
        expect(album.save).to eq(true)
        expect(album.orig_release_year).to eq(album.sale_date)
      end
    end

    context "creating an album with no orig_release_year" do
      it "orig_release_year is set to sale_date on save" do
        album.assign_attributes(orig_release_year: nil, sale_date: Time.now)
        expect(album.save).to eq(true)
        expect(album.orig_release_year).to eq(album.sale_date)
      end
    end
  end
end
