require "rails_helper"

describe Tunecore::Albums::State do
  before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

  describe "#state" do
    context "when called on a new record" do
      it "does NOT save a Single" do
        single = build(:single)
        single.state
        expect(single.persisted?).to eq false
      end

      it "does NOT save an Album" do
        album = build(:album)
        album.state
        expect(album.persisted?).to eq false
      end
    end
  end

  describe "#is_editable?" do
    let(:admin) {
      person = create(:person)
      person.roles << Role.all
      person
    }

    context "when a release is not paid for" do
      let(:album) { create(:album) }
      it "should be editable" do
        expect(album.is_editable?).to eq true
      end
    end

    context "when a release is paid for" do
      let(:album) { create(:album, :paid) }
      it "should not be editable" do
        expect(album.is_editable?).to eq false
      end
    end

    context "when a release has a history of being approved" do
      let(:album) { create(:album, :paid, :with_petri_bundle) }

      it "should not be editable" do
        album.petri_bundle.distributions << create(:distribution, petri_bundle_id: album.petri_bundle.id)
        allow_any_instance_of(Distribution).to receive(:start)
        create(:review_audit, album: album, event: "APPROVED", person: admin)
        expect(album.is_editable?).to eq false
      end
    end

    context "when a release is unfinalized without being approved" do
      let(:album) { create(:album, :paid) }
      it "should not be editable" do
        album.unfinalize(message: "Album was unfinalized.", person: admin)
        expect(album.is_editable?).to eq false
      end
    end
  end

  describe "#unfinalized?" do
    let(:admin) {
      person = create(:person)
      person.roles << Role.all
      person
    }

    context "when a release was just created" do
      let(:album) { create(:album) }
      it "is not unfinalized" do
        expect(album).not_to be_unfinalized
      end
    end

    context "when a release was unfinalized" do
      let(:album) { create(:album, :paid) }
      it "is unfinalized" do
        album.unfinalize(message: "Album was unfinalized.", person: admin)
        expect(album).to be_unfinalized
      end
    end

    context "when a release is finalized" do
      let(:album) { create(:album, :paid) }
      it "is not unfinalized" do
        expect(album).not_to be_unfinalized
      end
    end

    context "when a release was unfinalized but is now finalized again" do
      let(:album) { create(:album, :paid) }
      it "is not unfinalized" do
        album.unfinalize(message: "Album was unfinalized.", person: admin)
        allow(album).to receive(:should_finalize?).and_return(true)
        album.finalize(message: "Album was finalized.", person: admin)
        expect(album).not_to be_unfinalized
      end
    end
  end
end
