require "rails_helper"

describe Tunecore::Airbrake do
  context "#notify_airbrake with string" do
    it "should create new exception and call airbrake" do
      expect(Airbrake).to receive(:notify).with(Exception.exception("Foo Bar"), {})
      Tunecore::Airbrake.notify("Foo Bar")
    end
  end

  context "#notify_airbrake with exception" do
    it "should send exception directly to airbrake" do
      exception  = RuntimeError.new("foo bar")
      expect(Airbrake).to receive(:notify).with(exception, {})
      Tunecore::Airbrake.notify(exception)
    end
  end
end
