require "rails_helper"

describe Tunecore::People::PeopleFlagsWithdrawalBlock do
  describe ".block_withdrawals" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }

    it "should block create note" do
      expect(person2.flags.count).to eq(0)
      expect(person2.notes.count).to eq(0)
      person2.block_withdrawal!(person1, '127.0.0.1')
      expect(person2.flags.count).to eq(1)
      expect(person2.notes.count).to eq(1)
    end

    it "should not block if already blocked and return false" do
      person2.flags << Flag.find_by(**Flag::WITHDRAWAL)
      blocked = person2.block_withdrawal!(person1, '127.0.0.1')
      expect(blocked).to eq(false)
    end

    it "should create flag_transitions" do
      person2.block_withdrawal!(person1, '127.0.0.1')
      transition = person2.flag_transitions.last
      expect(transition.comment).to eq("Withdrawals for #{person2.name} is blocked by #{person1.name}")
    end
  end

  describe ".unblock_withdrawals" do
    let(:person1) { create(:person) }
    let(:person2) { create(:person) }

    it "should unblock create note" do
      person2.flags << Flag.find_by(**Flag::WITHDRAWAL)
      expect(person2.flags.count).to eq(1)
      expect(person2.notes.count).to eq(0)
      person2.unblock_withdrawal!(person1, '127.0.0.1')
      expect(person2.flags.count).to eq(0)
      expect(person2.notes.count).to eq(1)
    end

    it "should not unblock if already blocked and return false" do
      blocked = person2.unblock_withdrawal!(person1, '127.0.0.1')
      expect(blocked).to eq(false)
    end

    it "should create flag_transitions" do
      person2.flags << Flag.find_by(**Flag::WITHDRAWAL)
      person2.unblock_withdrawal!(person1, '127.0.0.1')
      transition = person2.flag_transitions.last
      expect(transition.comment).to eq("Withdrawals for #{person2.name} is unblocked by #{person1.name}")
    end
  end
end
