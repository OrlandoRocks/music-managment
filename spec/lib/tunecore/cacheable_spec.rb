require "rails_helper"

describe Tunecore::Cacheable do
  include Tunecore::Cacheable

  describe "#fetch" do
    it "should call rails cache fetch" do
      allow(Rails.cache).to receive(:fetch)
      expect(Rails.cache).to receive(:fetch)
      fetch("foo-bar") {}
    end
  end

  describe "#write" do
    it "should call rails cache fetch" do
      allow(Rails.cache).to receive(:write)
      expect(Rails.cache).to receive(:write)
      write("key", "value")
    end
  end

  describe "#invalidate" do
    it "should call rails cache fetch" do
      allow(Rails.cache).to receive(:delete)
      expect(Rails.cache).to receive(:delete)
      invalidate("foo-bar")
    end
  end
end
