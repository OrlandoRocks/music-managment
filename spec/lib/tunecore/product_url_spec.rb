require "rails_helper"
describe Tunecore::ProductUrl do
  before(:each) do
    @product_urls = YAML.load_file(File.join(Rails.root, "config", "product_urls.yml")).with_indifferent_access
  end

  context "#generate" do
    it "returns the country specific link" do
      [:us, :fr].each do |country_code|
        expect(Tunecore::ProductUrl.generate(:radio_airplay, :dashboard, country_code)).to eq @product_urls[:radio_airplay][:dashboard][country_code]
      end
    end
    it "returns the default (:us), if no specific link is found" do
      expect(Tunecore::ProductUrl.generate(:radio_airplay, :dashboard, :uk)).to eq @product_urls[:radio_airplay][:dashboard][:us]
    end
  end
end
