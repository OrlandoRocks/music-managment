require 'spec_helper'

module Tunecore::Tracing
  describe Factory do
    describe '#create_tracer' do
      let(:service_name) { 'rspec' }

      context 'when tracing is not configured' do
        it 'should ignore null connection strings' do
          expect(described_class.create_tracer(service_name, nil)).to be_nil
        end

        it 'should ignore empty connection strings' do
          expect(described_class.create_tracer(service_name, '')).to be_nil
        end

        it 'should raise UnknownDriverError for unknown implementations' do
          expect {
            described_class.create_tracer(service_name, 'opentracing://invalid_tracer')
          }.to raise_error(UnknownDriverError).with_message("Unknown driver: invalid_tracer")
        end

        it 'should raise InvalidConnectionString for invalid connection strings' do
          expect {
            described_class.create_tracer(service_name, 'open?tracing')
          }.to raise_error(InvalidConnectionString)
        end
      end

      context 'when tracing is configured with jaeger' do
        let(:mock_tracer) { double('jaeger-tracer') }

        it 'should process default connections' do
          expect(Tunecore::Tracing::JaegerFactory).to receive(:create_tracer).with(service_name, {}).and_return(mock_tracer)
          expect(described_class.create_tracer(service_name, 'opentracing://jaeger')).to be(mock_tracer)
        end

        it 'should process connection with parameters' do
          expect(Tunecore::Tracing::JaegerFactory).to receive(:create_tracer).with(service_name, {a: '1', b: '2', c: '3'}).and_return(mock_tracer)
          expect(described_class.create_tracer(service_name, 'opentracing://jaeger?a=1&b=2&c=3')).to be(mock_tracer)
        end
      end
    end
  end
end
