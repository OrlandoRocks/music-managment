require "rails_helper"


describe Tunecore::HistoryManager do

  describe "initialize" do
    it "should initialize intance variables" do
      person = TCFactory.create(:person)
      manager = Tunecore::HistoryManager.new(person)

      expect(manager.person).to eq(person)
      expect(manager.invoices).to eq(nil)
      expect(manager.manager).to eq({})
    end
  end

  # Ensure the setup of the filters. Used to make
  # sure all changes to these are considered
  describe "RELATED_FILTERS" do
    it "should have proper filters set up" do
      filters = [:salepoints, :renewals, :add_ons, :albums, :credits]
      Tunecore::HistoryManager::RELATED_FILTERS.keys.each do |key|
        expect(filters.include?(key)).to eq(true)
      end

      expect(Tunecore::HistoryManager::RELATED_FILTERS[:salepoints]).to eq('Salepoint')
      expect(Tunecore::HistoryManager::RELATED_FILTERS[:renewals]).to eq('Renewal')
      expect(Tunecore::HistoryManager::RELATED_FILTERS[:add_ons]).to eq(['SalepointSubscription', 'Booklet'])
      expect(Tunecore::HistoryManager::RELATED_FILTERS[:albums]).to eq('Album')
      expect(Tunecore::HistoryManager::RELATED_FILTERS[:credits]).to eq('CreditUsage')
    end
  end

end
