require "rails_helper"
require "pp"

describe Tunecore::CertEngine::StoreAutomatorPercent do
  let(:cert)   { FactoryBot.create(:cert, cert_engine: "StoreAutomatorPercent") }

  it "has instructions on how to use the engine" do
    expect(Tunecore::CertEngine::StoreAutomatorPercent.instructions).not_to be_empty
  end

  it "correctly says a StoreAutomator product is eligible" do
    product = FactoryBot.create(:product, :store_automator)
    purchase = FactoryBot.create(:purchase, product: product)
    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says any product other than a StoreAutomator one is ineligible" do
    product = FactoryBot.create(:product)
    purchase = FactoryBot.create(:purchase, product: product)
    expect(cert.engine.applicable_for_discount?(purchase)).to be false
  end
end
