require "rails_helper"
require "pp"

describe Tunecore::CertEngine::SpawningPercentSingle do
  let(:cert)   { FactoryBot.create(:cert, :spawning_percent_single) }
  let(:person) { FactoryBot.create(:person) }
  let(:invoice){ FactoryBot.create(:invoice, person: person, settled_at: Time.now - 1.second) }
  let(:album)  { FactoryBot.create(:album, person_id: person.id) }

  it "Gives some instructions on how to use the engine" do
    expect(Tunecore::CertEngine::SpawningPercentSingle.instructions).not_to be_empty
  end

  it "correctly says singles are eligible" do
    product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says albums are NOT eligible" do
    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be false
  end

  it "correctly says ringtones are NOT eligible" do
    product = Product.find(Product::US_ONE_YEAR_RINGTONE_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be false
  end

  context "internationalization" do
    product_countries = Product::PRODUCT_COUNTRY_MAP.keys

    product_countries.each do |country|
      it "correctly says #{country} singles are eligible (for a user from that country)" do
        product = Product.find(Product::PRODUCT_COUNTRY_MAP[country][:one_year_single])
        allow(person).to receive(:country_website_id).and_return(product.country_website_id)
        purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

        expect(cert.engine.applicable_for_discount?(purchase)).to eq true
      end
    end
  end
end
