require "rails_helper"
require "pp"

describe Tunecore::CertEngine::SpawningPercentDistributionCr do
  let(:cert)   { FactoryBot.create(:cert, :spawning_percent_distribution_cr) }
  let(:person) { FactoryBot.create(:person) }
  let(:invoice){ FactoryBot.create(:invoice, person: person, settled_at: Time.now - 1.second) }
  let(:album)  { FactoryBot.create(:album, person_id: person.id) }

  it "Gives some instructions on how to use the engine" do
    expect(Tunecore::CertEngine::SpawningPercentDistributionCr.instructions).not_to be_empty
  end

  context "distribution credits" do
    it "correctly says individual album distribution credits are eligible" do
      product = Product.find(Product::US_ONE_ALBUM_CREDIT_PRODUCT_ID)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be true
    end

    it "correctly says singles are eligible" do
      product = Product.find(Product::US_ONE_SINGLE_CREDIT_PRODUCT_ID)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be true
    end

    it "correctly says ringtones are eligible" do
      product = Product.find(111)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be true
    end

    it "correctly says 5 year albums are not eligible" do
      product = Product.find(19)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be false
    end

    it "correctly says 5 year singles are not eligible" do
      product = Product.find(23)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be false
    end

    it "correctly says 3 year ringtones are eligible" do
      product = Product.find(112)
      purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

      expect(cert.engine.applicable_for_discount?(purchase)).to be false
    end
  end

  context "internationalization" do
    product_countries = Product::PRODUCT_COUNTRY_MAP.keys

    product_countries.each do |country|
      it "correctly says #{country} albums are eligible (for a user from that country)" do
        product = Product.find(Product::PRODUCT_COUNTRY_MAP[country][:one_album_credit])
        allow(person).to receive(:country_website_id).and_return(product.country_website_id)
        purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

        expect(cert.engine.applicable_for_discount?(purchase)).to eq true
      end
    end
  end
end
