require "rails_helper"
require "pp"

describe Tunecore::CertEngine::DefaultRenewalPercent do
  let(:cert)   { FactoryBot.create(:cert, cert_engine: "DefaultRenewalPercent") }

  it "Gives some instructions on how to use the engine" do
    expect(Tunecore::CertEngine::DefaultRenewalPercent.instructions).not_to be_empty
  end

  it "correctly says album renewals are eligible" do
    product = FactoryBot.create(:product, :album_renewal)
    purchase = FactoryBot.create(:purchase, product: product)
    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says single renewals are eligible" do
    product = FactoryBot.create(:product, :single_renewal)
    purchase = FactoryBot.create(:purchase, product: product)
    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says products that are not renewals are ineligible" do
    non_renewal_product = FactoryBot.create(:product)
    purchase = FactoryBot.create(:purchase, product: non_renewal_product)
    expect(non_renewal_product.product_type).to_not eq 'Renewal'
    expect(cert.engine.applicable_for_discount?(purchase)).to be false
  end
end
