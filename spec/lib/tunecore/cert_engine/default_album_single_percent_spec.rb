require "rails_helper"
require "pp"

describe Tunecore::CertEngine::DefaultAlbumSinglePercent do
  let(:cert)   { FactoryBot.create(:cert, cert_engine: "DefaultAlbumSinglePercent") }
  let(:person) { FactoryBot.create(:person) }
  let(:invoice){ FactoryBot.create(:invoice, person: person, settled_at: Time.now - 1.second) }
  let(:album)  { FactoryBot.create(:album, person_id: person.id) }

  it "Gives some instructions on how to use the engine" do
    expect(Tunecore::CertEngine::DefaultAlbumSinglePercent.instructions).not_to be_empty
  end

  it "correctly says albums are eligible" do
    product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says singles are eligible" do
    product = Product.find(Product::US_ONE_YEAR_SINGLE_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be true
  end

  it "correctly says products other than albums and singles are ineligible" do
    product = Product.find(Product::US_ONE_YEAR_RINGTONE_PRODUCT_ID)
    purchase = FactoryBot.create(:purchase, person: person, product: product, invoice: invoice, related_id: album.id, related_type: "Album", paid_at: Time.now)

    expect(cert.engine.applicable_for_discount?(purchase)).to be false
  end
end
