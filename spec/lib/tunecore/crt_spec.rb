require "rails_helper"

describe Tunecore::Crt do
  describe "cache methods" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      allow(Tunecore::Crt).to receive(:create_album_hash).and_return({})
      @tc_album = create(:album)
    end

    context "fetch" do
      it "should call tunecore cache" do
        allow(Tunecore::Crt).to receive(:fetch)
        expect(Tunecore::Crt).to receive(:fetch)
        Tunecore::Crt.fetch_album_from_cache(@tc_album)
      end
    end
  end

  describe "#create_album_hash" do
    it "should return formatted hash" do
      tc_album = create(:artwork).album
      tc_album.finalized_at = Time.now
      expected_hash = {
        label_name: tc_album.label_name,
        country_website: tc_album.person.country_domain,
        account_country: tc_album.person.country,
        distribution_type: tc_album.album_type,
        created: tc_album.created_on,
        tracks: tc_album.songs.size,
        tunecore_upc: tc_album.tunecore_upc.number,
        title: tc_album.name,
        artwork: tc_album.artwork.url(:medium),
        songs: [],
        artists: [{name: tc_album.artist.name, role: "Primary Artist", blacklisted: false}],
        is_various: tc_album.is_various?,
        album_id: tc_album.id,
        person_id: tc_album.person_id,
          auto_format_on: tc_album.allow_different_format,
        blacklists: [],
        blacklists_disabled: true,
        stores: tc_album.salepoints.map{|s| s.store.short_name},
        vip_status: tc_album.person.vip? ? 'True':'False',
        total_releases: tc_album.person.albums.size,
        account_status: tc_album.person.status,
        account_created: tc_album.person.created_on,
        account_owner: tc_album.person.name,
        audits: [],
        legal_review_state: tc_album.legal_review_state.capitalize,
        finalized: I18n.localize(tc_album.finalized_at, format: :slash_with_time),
        account_email: tc_album.person.email,
        genres: tc_album.genres.map{|g| g.name},
        release_language: tc_album.language.description,
        countries: tc_album.countries.map{|c| [c.name,c.iso_code]},
        total_earnings: 0,
        dup_titles: [],
        flagged_content: tc_album.flagged_content_data
      }
      expect(Tunecore::Crt.create_album_hash(tc_album)).to eq(expected_hash)
    end
  end

  describe "#collect_crt_songs" do
    before do
      @song  = create(:song, metadata_language_code_id: 1)
      lyric = create(:lyric, song_id: @song.id)
      create(:creative, creativeable_id: @song.id, creativeable_type: "Song", role: "featuring")
      @expected_hash = {
        track_number: @song.track_num,
        translated_title: nil,
        title: @song.name,
        parental_advisory: @song.parental_advisory,
        artists: SongArtist.for_crt(@song),
        uuid: @song.s3_asset.nil? ? nil : @song.s3_asset.uuid,
        id: @song.id,
        song_id: @song.id,
        lyrics: lyric.content,
        language: @song.metadata_language_code.description,
        streaming_url: "url"
      }
    end

    it "should return formatted hash" do
      allow_any_instance_of(Song).to receive(:s3_url).and_return("url")
      expect(Tunecore::Crt.collect_crt_songs(@song.album.reload)).to eq([@expected_hash])
    end
  end

  describe "#collect_crt_audits" do
    it "should return formatted hash" do
      person = create(:person)
      tc_album = create(:album, person: person, )
      tc_album.legal_review_state = "NEEDS REVIEW"
      ra = create(:review_audit, album: tc_album, person: person)
      expected_hash = [ {
        auditor: person.name,
        event: ra.event,
        note: ra.note,
        created: I18n.localize(ra.created_at, format: :slash_with_time),
        reasons: []
      }]
      expect(Tunecore::Crt.collect_crt_audits(tc_album)).to match(expected_hash)
    end
  end

  describe "#collect_artist_names" do
    before(:each) do
      @tc_album = create(:album)
    end

    context "is various artist album" do
      it "should return formatted hash with various artists" do
        allow(@tc_album).to receive(:is_various?).and_return(true)
        expected_hash = [{name:"Various Artists", role:"Various"}]
        expect(Tunecore::Crt.collect_artist_names(@tc_album)).to eq(expected_hash)
      end
    end

    context "is not a various artist album" do
      it "should return formatted hash with various artists" do
        expected_hash = [{ name: @tc_album.artist.name, role:"Primary Artist", blacklisted: false }]
        expect(Tunecore::Crt.collect_artist_names(@tc_album)).to eq(expected_hash)
      end

      context "artist is blacklisted" do
        let(:album)  { create(:album) }
        let(:artist) { create(:artist) }

        it "should return true if artist is blacklisted" do
          create(:creative, creativeable: album, role: "primary_artist", artist: artist)
          create(:blacklisted_artist, artist: artist, active: true)
          artist_on_blacklist = Tunecore::Crt.collect_artist_names(album).last
          expect(artist_on_blacklist[:blacklisted]).to eq(true)
        end
      end
    end
  end

  describe "#get_song_ids_of_duplicate_titles" do
    before(:each) do
      @tc_album = create(:album, :with_uploaded_songs, number_of_songs: 2)
    end

    context "no duplicates" do
      it "should return empty array" do
        song1 = @tc_album.songs[0]
        song1.name = "Unique 1"
        song2 = @tc_album.songs[1]
        song2.name = "Unique 2"
        @tc_album.songs = [song1, song2]
        expect(Tunecore::Crt.get_song_ids_of_duplicate_titles(@tc_album)).to eq([])
      end
    end

    context "has duplicates" do
      it "should return array if duplicate_song_ids" do
        result = Tunecore::Crt.get_song_ids_of_duplicate_titles(@tc_album)
        expect(result.size).to eq(@tc_album.songs.size)
        expect(result.include?(@tc_album.songs.first.id)).to eq(true)
        expect(result.include?(@tc_album.songs.last.id)).to eq(true)
      end
    end
  end
end
