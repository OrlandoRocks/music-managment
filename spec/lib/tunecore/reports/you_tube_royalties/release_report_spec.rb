require "rails_helper"

describe Tunecore::Reports::YouTubeRoyalties::ReleaseReport do

  let(:person) { person = FactoryBot.create(:person) }
  let(:sales_period_start) { "2014-11-01" }
  let(:album_name) { "Best Album" }

  before(:each) do
    @album1 = FactoryBot.create(:album, name: album_name, person: person)
    @song1 = FactoryBot.create(:song, album: @album1)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, album_name: album_name)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, album_name: album_name)

    @album2 = FactoryBot.create(:album, person: person)
    @song2 = FactoryBot.create(:song, album: @album2)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song2, sales_period_start: sales_period_start, album_name: @album2.name)
  end

  it "should aggregate by album id" do
    @report = Tunecore::Reports::YouTubeRoyalties::ReleaseReport.new(person, Date.parse(sales_period_start), Date.parse(sales_period_start)+1.month)
    data = @report.report
    expect(data.first.size).to eq(2)
    expect(data.first[0].title).to eq(album_name)
    expect(data.first[1].title).to eq(@album2.name)
  end

end
