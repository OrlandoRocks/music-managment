require "rails_helper"

describe Tunecore::Reports::YouTubeRoyalties::SalesPeriodReport do

  let(:person) { person = FactoryBot.create(:person) }
  let(:sales_period_start1) { "2014-10-01" }
  let(:sales_period_start2) { "2014-11-01" }

  before(:each) do
    @album1 = FactoryBot.create(:album, person: person)
    @song1 = FactoryBot.create(:song, album: @album1)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start1)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start2)

    @album2 = FactoryBot.create(:album, person: person)
    @song2 = FactoryBot.create(:song, album: @album2)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song2, sales_period_start: sales_period_start2)
  end

  it "should aggregate by song id" do
    @report = Tunecore::Reports::YouTubeRoyalties::SalesPeriodReport.new(person, Date.parse(sales_period_start1), Date.parse(sales_period_start2))
    data = @report.report
    expect(data.first.size).to eq(2)
    expect(data.first[0].sales_period_start).to eq(Date.parse(sales_period_start1))
    expect(data.first[1].sales_period_start).to eq(Date.parse(sales_period_start2))
  end

end


