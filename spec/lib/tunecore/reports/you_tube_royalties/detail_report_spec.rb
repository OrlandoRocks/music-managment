require "rails_helper"

describe Tunecore::Reports::YouTubeRoyalties::ArtistReport do
  it "should return record detail" do
    person = create(:person)
    sales_period_start = "2014-11-01"
    album1 = create(:album, person: person)
    song1 = create(:song, album: album1)
    create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
    create(:you_tube_royalty_record, person: person, song: song1, sales_period_start: sales_period_start)
    album2 = create(:album, person: person)
    song2 = create(:song, album: album2)
    create(:you_tube_royalty_record, person: person, song: song2, sales_period_start: sales_period_start)

    report = Tunecore::Reports::YouTubeRoyalties::DetailReport.new(
      person,
      Date.parse(sales_period_start),
      Date.parse(sales_period_start)
    )
    data = report.streamable_csv

    expect(data.peek.split(",").first).to eq("Sales Period")
    expect(data.count).to eq(4)
  end
end
