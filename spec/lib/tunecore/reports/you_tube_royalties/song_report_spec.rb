require "rails_helper"

describe Tunecore::Reports::YouTubeRoyalties::SongReport do

  let(:person) { person = FactoryBot.create(:person) }
  let(:sales_period_start) { "2014-11-01" }
  let(:song_name) { "Best Song" }

  before(:each) do
    @album1 = FactoryBot.create(:album, person: person)
    @song1 = FactoryBot.create(:song, album: @album1, name: song_name)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, song_name: song_name)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, song_name: song_name)

    @album2 = FactoryBot.create(:album, person: person)
    @song2 = FactoryBot.create(:song, album: @album2)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song2, sales_period_start: sales_period_start, song_name: @song2.name)
  end

  it "should aggregate by song id" do
    @report = Tunecore::Reports::YouTubeRoyalties::SongReport.new(person, Date.parse(sales_period_start), Date.parse(sales_period_start)+1.month)
    data = @report.report
    expect(data.first.size).to eq(2)
    expect(data.first[0].song_name).to eq(song_name)
    expect(data.first[1].song_name).to eq(@song2.name)
  end

end

