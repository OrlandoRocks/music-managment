require "rails_helper"

describe Tunecore::Reports::YouTubeRoyalties::ArtistReport do

  let(:person) { person = FactoryBot.create(:person) }
  let(:sales_period_start) { "2014-11-01" }
  let(:artist1) { "Artist 1" }
  let(:artist2) { "Artist 2" }

  before(:each) do
    @album1 = FactoryBot.create(:album, person: person)
    @song1 = FactoryBot.create(:song, album: @album1)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, artist_name: artist1)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song1, sales_period_start: sales_period_start, artist_name: artist1)

    @album2 = FactoryBot.create(:album, person: person)
    @song2 = FactoryBot.create(:song, album: @album2)
    FactoryBot.create(:you_tube_royalty_record, person: person, song: @song2, sales_period_start: sales_period_start, artist_name: artist2)
  end

  it "should aggregate by song id" do
    @report = Tunecore::Reports::YouTubeRoyalties::ArtistReport.new(person, Date.parse(sales_period_start), Date.parse(sales_period_start)+1.month)
    data = @report.report
    expect(data.first.size).to eq(2)
    expect(data.first[0].artist_name).to eq(artist1)
    expect(data.first[1].artist_name).to eq(artist2)
  end

end
