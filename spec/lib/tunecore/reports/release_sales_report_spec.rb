require "rails_helper"

describe Tunecore::Reports::ReleaseSalesReport do
  it "should set currency based on either album sales or song sales currency if present" do
    song = Song.first
    srm = FactoryBot.create(:sales_record_master, :revenue_currency => 'GBP', :amount_currency => 'GBP')
    sr = FactoryBot.create(:sales_record, related_type: "Song", related_id: song.id, person_id: song.album.person_id, sales_record_master: srm)
    FactoryBot.send(:summarize_sales_record_master, sr.sales_record_master)

    @report = Tunecore::Reports::ReleaseSalesReport.new(sr.person, :for_release, {release: song.album})
    data, results = @report.report_and_summary({related_type: "Song"})

    expect(results[:currency]).to eq('GBP')
  end
end
