require "rails_helper"

describe Tunecore::Reports::SalesReport do
  let(:person) { create(:person) }

  it "should report on videos" do
    video = Video.first
    sr = TCFactory.create(:sales_record, related_type: "Video", related_id: video.id, person_id: video.person_id)
    TCFactory.summarize_sales_record_master(sr.sales_record_master)

    report = Tunecore::Reports::SalesReport.new(sr.person, :by_date)
    monthly_sales_records, report_totals = report.report_and_summary({releases: { videos: [video.id] }})
    expect(report_totals[:releases_sold]).to eq(1)
    expect(report_totals[:release_amount]).to eq(sr.amount)
  end

  describe "#initialize" do
    context "sales_period report" do
      let(:type) { :sales_period }

      context "month not present" do
        it "should raise month required error" do
          expect {
            Tunecore::Reports::SalesReport.new(person, type)
          }.to raise_error("Must initialize with month")
        end
      end
    end

    context "reporting_period report" do
      let(:type) { :sales_period }

      context "month not present" do
        it "should raise month required error" do
          expect {
            Tunecore::Reports::SalesReport.new(person, type)
          }.to raise_error("Must initialize with month")
        end
      end
    end
  end

  describe "#to_enumerable_csv" do
    let(:month) { "2019-08-12" }
    let(:sip_store) { create(:streaming_sip_store) }

    context "unsupported report type" do
      it "should return nil" do
        result = Tunecore::Reports::SalesReport.new(person, :by_date).to_streamable_csv({})

        expect(result).to be_nil
      end
    end

    it "should return CSV with appropriate headers" do
      expected_headers = [
        "Sales Period",
        "Posted Date",
        "Store Name",
        "Country Of Sale",
        "Artist",
        "Release Type",
        "Release Title",
        "Song Title",
        "Label",
        "UPC",
        "Optional UPC",
        "TC Song ID",
        "Optional ISRC",
        "Sales Type",
        "# Units Sold",
        "Per Unit Price",
        "Net Sales",
        "Net Sales Currency",
        "Exchange Rate",
        "Total Earned",
        "Currency"
      ]

      result = Tunecore::Reports::SalesReport
        .new(person, :sales_period, month: month)
        .to_streamable_csv({})

      # Returned are CSV:Rows hence squishing
      returned_headers = result.first.split(",").map(&:squish)

      expect(expected_headers).to match_array(returned_headers)
    end

    context "full_export_for_sales_period" do
      let(:report) { Tunecore::Reports::SalesReport.new(person, :sales_period, month: month) }
      let(:srm) { create(:sales_record_master, period_sort: month, sip_store: sip_store) }

      it "should return sales records and archives from given month" do
        create(:sales_record, person: person, sales_record_master: srm)
        create(:sales_record_archive, person: person, sales_record_master: srm)

        result = report.to_streamable_csv({})
        expect(result.to_a.size).to eq(3)
      end

      it "should not return sales records and archives not from given month" do
        create(:sales_record, person: person)
        create(:sales_record_archive, person: person)

        result = report.to_streamable_csv({})

        expect(result.to_a.size).to eq(1)
      end
    end

    context "full_export_for_reporting_period" do
      let(:report) { Tunecore::Reports::SalesReport.new(person, :reporting_period, month: month) }
      let(:srm) { create(:sales_record_master, created_at: month, sip_store: sip_store) }

      it "should return sales records and archives created in given month" do
        create(:sales_record, person: person, sales_record_master: srm)
        create(:sales_record_archive, person: person, sales_record_master: srm)

        result = report.to_streamable_csv({})
        expect(result.to_a.size).to eq(3)
      end

      it "should not return sales records and archives not created in given month" do
        create(:sales_record, person: person)
        create(:sales_record_archive, person: person)

        result = report.to_streamable_csv({})

        expect(result.to_a.size).to eq(1)
      end
    end
  end

end
