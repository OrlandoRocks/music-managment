# encoding: utf-8
require "rails_helper"

describe Tunecore::DateTranslator do
  describe "#translate" do
    after(:all) {I18n.locale = :en}

    context "to English Format" do
      context "when :en date is passed" do
        it "returns long_date format for single digit day" do
          expect(
            Tunecore::DateTranslator.translate(
              "January 1, 2016",
              "long_date",
              :en
            )
          ).to eq "January 1, 2016"
        end

        it "returns long_date format for double digit day" do
          expect(
            Tunecore::DateTranslator.translate(
              "January 18, 2016",
              "long_date",
              :en
            )
          ).to eq "January 18, 2016"
        end

        it "returns country_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "01/01/2016",
              "country_date",
              :en
            )
          ).to eq "01/01/2016"
        end

        it "returns dash format" do
          expect(
            Tunecore::DateTranslator.translate(
              "01-01-2016",
              "dash",
              :en
            )
          ).to eq "01-01-2016"
        end

        it "returns long_date_inputs format" do
          expect(
            Tunecore::DateTranslator.translate(
              "January 18 2016",
              "long_date_inputs",
              :en
            )
          ).to eq "January 18 2016"
        end

        it "returns month_day_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Jan 18 2016",
              "month_day_year",
              :en
            )
          ).to eq "Jan 18 2016"
        end

        it "returns slash_short_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "01/18/16",
              "slash_short_year",
              :en
            )
          ).to eq "01/18/16"
        end
      end

      context "when :de date is passed" do
        before(:each) { I18n.locale = :de }

        it "returns long_date format for single digit day" do
          expect(
            Tunecore::DateTranslator.translate(
              "1 Januar 2016",
              "long_date",
              :en,
              :de
            )
          ).to eq "January 1, 2016"
        end

        it "returns long_date format for double digit day" do
          expect(
            Tunecore::DateTranslator.translate(
              "18 Januar 2016",
              "long_date",
              :en
            )
          ).to eq "January 18, 2016"
        end

        it "returns country_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22.03.2016",
              "country_date",
              :en
            )
          ).to eq "03/22/2016"
        end

        it "returns dash format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22-03-2016",
              "dash",
              :en
            )
          ).to eq "03-22-2016"
        end

        it "returns long_date_inputs format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Januar 18 2016",
              "long_date_inputs",
              :en
            )
          ).to eq "January 18 2016"
        end

        it "returns month_day_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Jan 18 2016",
              "month_day_year",
              :en
            )
          ).to eq "Jan 18 2016"
        end

        it "returns slash_short_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22/03/16",
              "slash_short_year",
              :en
            )
          ).to eq "03/22/16"
        end
      end

      context "when :fr date is passed" do
        before(:each) { I18n.locale = :fr }

        it "returns long_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "2 Décembre 2016",
              "long_date",
              :en
            )
          ).to eq "December 2, 2016"
        end

        it "returns country_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22.03.2016",
              "country_date",
              :en
            )
          ).to eq "03/22/2016"
        end

        it "returns dash format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22-03-2016",
              "dash",
              :en
            )
          ).to eq "03-22-2016"
        end

        it "returns long_date_inputs format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Janvier 18 2016",
              "long_date_inputs",
              :en
            )
          ).to eq "January 18 2016"
        end

        it "returns month_day_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Janv 18 2016",
              "month_day_year",
              :en
            )
          ).to eq "Jan 18 2016"
        end

        it "returns slash_short_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22/03/16",
              "slash_short_year",
              :en
            )
          ).to eq "03/22/16"
        end
      end

      context "when :it date is passed" do
        before(:each) { I18n.locale = :it }

        it "returns long_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "2 Gennaio 2016",
              "long_date",
              :en
            )
          ).to eq "January 2, 2016"
        end

        it "returns country_date format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22/03/2016",
              "country_date",
              :en
            )
          ).to eq "03/22/2016"
        end

        it "returns dash format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22-03-2016",
              "dash",
              :en
            )
          ).to eq "03-22-2016"
        end

        it "returns long_date_inputs format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Gennaio 18 2016",
              "long_date_inputs",
              :en
            )
          ).to eq "January 18 2016"
        end

        it "returns month_day_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "Gen 18 2016",
              "month_day_year",
              :en
            )
          ).to eq "Jan 18 2016"
        end

        it "returns slash_short_year format" do
          expect(
            Tunecore::DateTranslator.translate(
              "22/03/16",
              "slash_short_year",
              :en
            )
          ).to eq "03/22/16"
        end
      end
    end

    context "to German Format" do
      it "returns long_date format for single digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 1, 2016",
            "long_date",
            :de,
            :en
          )
        ).to eq "1 Januar 2016"
      end

      it "returns long_date format for double digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 18, 2016",
            "long_date",
            :de,
            :en
          )
        ).to eq "18 Januar 2016"
      end

      it "returns country_date format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/2016",
            "country_date",
            :de,
            :en
          )
        ).to eq "22.03.2016"
      end

      it "returns dash format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03-22-2016",
            "dash",
            :de,
            :en
          )
        ).to eq "22-03-2016"
      end

      it "returns long_date_inputs format" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 18 2016",
            "long_date_inputs",
            :de,
            :en
          )
        ).to eq "Januar 18 2016"
      end

      it "returns month_day_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "Jan 18 2016",
            "month_day_year",
            :de,
            :en
          )
        ).to eq "Jan 18 2016"
      end

      it "returns slash_short_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/16",
            "slash_short_year",
            :de,
            :en
          )
        ).to eq "22/03/16"
      end
    end

    context "to French Format" do
      it "returns long_date format for single digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "December 2, 2016",
            "long_date",
            :fr,
            :en
          )
        ).to eq "2 Décembre 2016"
      end

      it "returns long_date format for double digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "December 22, 2016",
            "long_date",
            :fr,
            :en
          )
        ).to eq "22 Décembre 2016"
      end

      it "returns country_date format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/2016",
            "country_date",
            :fr,
            :en
          )
        ).to eq "22.03.2016"
      end

      it "returns dash format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03-22-2016",
            "dash",
            :fr,
            :en
          )
        ).to eq "22-03-2016"
      end

      it "returns long_date_inputs format" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 18 2016",
            "long_date_inputs",
            :fr,
            :en
          )
        ).to eq "Janvier 18 2016"
      end

      it "returns month_day_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "Jan 18 2016",
            "month_day_year",
            :fr,
            :en
          )
        ).to eq "Janv 18 2016"
      end

      it "returns slash_short_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/16",
            "slash_short_year",
            :fr,
            :en
          )
        ).to eq "22/03/16"
      end
    end

    context "to Italian Format" do
      it "returns long_date format for single digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 2, 2016",
            "long_date",
            :it,
            :en
          )
        ).to eq "2 Gennaio 2016"
      end

      it "returns long_date format for double digit day" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 18, 2016",
            "long_date",
            :it,
            :en
          )
        ).to eq "18 Gennaio 2016"
      end

      it "returns country_date format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/2016",
            "country_date",
            :it,
            :en
          )
        ).to eq "22/03/2016"
      end

      it "returns dash format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03-22-2016",
            "dash",
            :it,
            :en
          )
        ).to eq "22-03-2016"
      end

      it "returns long_date_inputs format" do
        expect(
          Tunecore::DateTranslator.translate(
            "January 18 2016",
            "long_date_inputs",
            :it,
            :en
          )
        ).to eq "Gennaio 18 2016"
      end

      it "returns month_day_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "Jan 18 2016",
            "month_day_year",
            :it,
            :en
          )
        ).to eq "Gen 18 2016"
      end

      it "returns slash_short_year format" do
        expect(
          Tunecore::DateTranslator.translate(
            "03/22/16",
            "slash_short_year",
            :it,
            :en
          )
        ).to eq "22/03/16"
      end
    end

    context "error handling for mismatching date formats" do
      before(:each) {I18n.locale = :fr}

      it "logs a message" do
        expect(Rails.logger).to receive(:info)
        Tunecore::DateTranslator.translate(
          "01/01/2016",
          "long_date",
          :en
        )
      end

      it "returns untranslated date" do
        expect(
          Tunecore::DateTranslator.translate(
            "01/01/2016",
            "long_date",
            :en
          )
        ).to eq "01/01/2016"
      end
    end
  end

  describe '#parse_date' do
    let(:formats_for_testing) {
      ["country_date", "dash", "long_date", "long_date_inputs", "month_day_year", "slash_short_year"]
    }
    let(:test_date) { Date.new(2016, 10, 25) }

    it "parses date strings of all formats and locales to date objects" do
      [:en, :fr, :de, :it].each do |locale|
        I18n.locale = locale
        formats_for_testing.each do |name|
          test_string = I18n.l(test_date, format: I18n.t("date.formats.#{name}"))
          expect(Tunecore::DateTranslator.parse_date(test_string, name.to_s, :en)).to eq(test_date)
        end
      end
    end
  end
end
