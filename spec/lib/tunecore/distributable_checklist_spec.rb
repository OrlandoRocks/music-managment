require "rails_helper"

describe Tunecore::DistributableChecklist do
  describe "#songs_have_songwriters?" do
    let(:album) { create(:album) }
    let!(:song1) { create(:song, album: album) }
    let!(:song2) { create(:song, album: album) }
    let(:songwriter_role) { SongRole.find_by(role_type: "songwriter") }

    it "returns true for an album where all the songs have at least one songwriter" do
      CreativeSongRole.create!(song: song1, song_role: songwriter_role)
      CreativeSongRole.create!(song: song2, song_role: songwriter_role)
      expect(album.reload.songs_have_songwriters?).to eq(true)
    end

    it "returns false for an album where some songs have no songwriters" do
      CreativeSongRole.create!(song: song1, song_role: songwriter_role)
      expect(album.reload.songs_have_songwriters?).to eq(false)
    end

    it "returns false for an album where no songs have any songwriters" do
      expect(album.reload.songs_have_songwriters?).to eq(false)
    end
  end

  describe "#has_primary_artist?" do
    it "should return true if an album has a primary artist" do
      album = create(:album)

      expect(album.has_primary_artist?).to eq(true)
    end

    it "should return false if an album does not have a primary artist" do
      album = create(:album)
      album.primary_artists.map(&:destroy!)
      album.reload

      expect(album.has_primary_artist?).to eq(false)
    end
  end

  describe "#should_finalize?" do
    context "missing requirements" do
      it "should not finalize an album without a primary artist" do
        album = create(:album)
        album.primary_artists.map(&:destroy!)
        album.reload

        expect(album.should_finalize?).to eq(false)
      end

      it "should not finalize an album without artwork" do
        album = create(:album, :with_uploaded_songs, :with_salepoints, :with_songwriter)

        expect(album.should_finalize?).to eq(false)
      end

      it "should not finalize an album without stores" do
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter)

        expect(album.should_finalize?).to eq(false)
      end

      it "should not finalize an album without artwork template info" do
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_salepoints, abbreviation: ["fb", "gn", "sz"])
        create(:salepoint, :aod_store, salepointable: album)
        album.reload

        expect(album.should_finalize?).to eq(false)
      end

      it "should not finalize an album without tracks" do
        album = create(:album, :with_salepoints, :with_songwriter)

        expect(album.should_finalize?).to eq(false)
      end
    end

    context "all requirements met" do
      it "should return true" do
        album = create(:album, :with_uploaded_songs_and_artwork, :with_salepoints, :with_songwriter)

        expect(album.should_finalize?).to eq(true)
      end
    end
  end
end
