require "rails_helper"

describe Tunecore::Api::Auth do
  before(:each) do
    @person = FactoryBot.create(:person)
  end
  describe "#valid_account?" do
    it "returns false on non-existent emails" do
      expect(Tunecore::Api::Auth.valid_account?(@person.email="foo.bar")).to eq(false)
    end

    it "returns true on valid emails" do
      expect(Tunecore::Api::Auth.valid_account?(@person.email)).to eq(true)
    end

    it "returns false on deleted accounts" do
      @person.update(deleted: 1)
      expect(Tunecore::Api::Auth.valid_account?(@person.email)).to eq(false)
    end
  end
  describe "#valid_key?" do
    before(:each) do
      @valid_key = API_CONFIG["royalty_solutions"]["API_KEY"]
    end

    it "returns nil when no key given" do
      expect(Tunecore::Api::Auth.valid_key?(controller: "api/royalty_solutions")).to eq(false)
    end

    it "returns false when key does not match" do
      expect(Tunecore::Api::Auth.valid_key?(controller: "api/royalty_solutions", api_key: @valid_key + "FOO")).to eq(false)
    end

    it "returns true on matching keys" do
      expect(Tunecore::Api::Auth.valid_key?(controller: "api/royalty_solutions", api_key: @valid_key)).to eq(true)
    end
  end
end
