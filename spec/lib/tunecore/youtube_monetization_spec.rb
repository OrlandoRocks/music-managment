require "rails_helper"

describe Tunecore::People::YoutubeMonetization do
  describe "#has_active_ytm?" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    context "user has no ytm" do
      it "should return false" do
        expect(@person.has_active_ytm?).to eq(false)
      end
    end

    context "user has unpurchased ytm" do
      it "should return false" do
        FactoryBot.create(:youtube_monetization, person: @person)
        expect(@person.has_active_ytm?).to eq(false)
      end
    end

    context "user has purchased, non-terminated ytm" do
      it "should return true" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now)
        expect(@person.has_active_ytm?).to eq(true)
      end
    end

    context "user has ytm that was opted-out" do
      it "should return false" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now, termination_date: Time.now)
        expect(@person.has_active_ytm?).to eq(false)
      end
    end
  end

  describe "#blocked_ytm" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    context "user has purchased ytm" do
      it "should return true if it's blocked" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now, block_date: Date.today)
        expect(@person.ytm_blocked?).to eq(true)
      end

      it "should return false if it's not blocked" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now)
        expect(@person.ytm_blocked?).to eq(false)
      end
    end
  end

  describe "#has_purchased_ytm?" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    context "user has no ytm" do
      it "should return false" do
        expect(@person.has_purchased_ytm?).to eq(false)
      end
    end

    context "user has unpurchased ytm" do
      it "should return false" do
        FactoryBot.create(:youtube_monetization, person: @person)
        expect(@person.has_purchased_ytm?).to eq(false)
      end
    end

    context "user has purchased, non-terminated ytm" do
      it "should return true" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now)
        expect(@person.has_purchased_ytm?).to eq(true)
      end
    end

    context "user has ytm that was opted-out" do
      it "should return false" do
        FactoryBot.create(:youtube_monetization, person: @person, effective_date: Time.now, termination_date: Time.now)
        expect(@person.has_purchased_ytm?).to eq(true)
      end
    end
  end

  describe "#ytm_in_cart?" do
    before(:each) do
      @person = FactoryBot.create(:person)
    end

    context "user added ytm to cart" do
      it "should return true" do
        ytm = FactoryBot.create(:youtube_monetization, person: @person)
        product = Product.find_by(country_website: @person.country_website)
        ytm.create_youtube_purchase(product)
        expect(@person.ytm_in_cart?).to eq(true)
      end
    end
  end

  describe "#terminate_ytm" do
    context "when a user has more than 1 youtubesr track_monetization" do
      it "should set the termination date to the current time" do
        person = create(:person)
        album1 = create(:album, :with_songs, person: person)
        album2 = create(:album, :with_songs, person: person)
        song1 = album1.songs.first
        song2 = album2.songs.first

        ytm = create(:youtube_monetization, person: person, effective_date: Time.now, termination_date: nil)

        store = Store.find_by_abbrev("ytsr")

        track_monetization1 = create(:track_monetization, song: song1, store: store)
        track_monetization2 = create(:track_monetization, song: song2, store: store)

        person.terminate_ytm
        expect(ytm.reload.termination_date.present?).to eq(true)
      end

      it "calls TrackMonetization::TakedownService#takedown! for each youtube track monetization" do
        person = create(:person)
        album1 = create(:album, :with_songs, person: person)
        album2 = create(:album, :with_songs, person: person)
        song1 = album1.songs.first
        song2 = album2.songs.first

        ytm = create(:youtube_monetization, person: person, effective_date: Time.now, termination_date: nil)

        store = Store.find_by_abbrev("ytsr")

        track_monetization1 = create(:track_monetization, song: song1, store: store)
        track_monetization2 = create(:track_monetization, song: song2, store: store)

        yt_track_monetizations = [track_monetization1, track_monetization2]

        allow(person).to receive_message_chain(:track_monetizations, :where)
          .and_return(yt_track_monetizations)

        expect(TrackMonetization::TakedownService).to receive(:takedown)
          .with(track_monetizations: [track_monetization1], takedown_source: "Tunecore Admin")

        expect(TrackMonetization::TakedownService).to receive(:takedown)
          .with(track_monetizations: [track_monetization2], takedown_source: "Tunecore Admin")

        person.terminate_ytm
      end
    end
  end

  describe "#block_ytm" do
    context "when a user has more than 1 youtubesr track_monetization" do
      it "should set the block date to the current time" do
        person = create(:person)
        album1 = create(:album, :with_songs, person: person)
        album2 = create(:album, :with_songs, person: person)
        song1 = album1.songs.first
        song2 = album2.songs.first

        ytm = create(:youtube_monetization, person: person, effective_date: Time.now, termination_date: nil)

        store = Store.find_by_abbrev("ytsr")

        track_monetization1 = create(:track_monetization, song: song1, store: store)
        track_monetization2 = create(:track_monetization, song: song2, store: store)

        person.block_ytm
        expect(ytm.reload.block_date.present?).to eq(true)
      end

      it "calls TrackMonetization::TakedownService#takedown! for each youtube track monetization" do
        person = create(:person)
        album1 = create(:album, :with_songs, person: person)
        album2 = create(:album, :with_songs, person: person)
        song1 = album1.songs.first
        song2 = album2.songs.first

        ytm = create(:youtube_monetization, person: person, effective_date: Time.now, termination_date: nil)

        store = Store.find_by_abbrev("ytsr")

        track_monetization1 = create(:track_monetization, song: song1, store: store, person: person)
        track_monetization2 = create(:track_monetization, song: song2, store: store, person: person)

        yt_track_monetizations = [track_monetization1, track_monetization2]

        allow(person).to receive_message_chain(:track_monetizations, :where)
          .and_return(yt_track_monetizations)

        expect(TrackMonetization::TakedownService).to receive(:takedown)
          .with(track_monetizations: [track_monetization1], takedown_source: "Tunecore Admin")

        expect(TrackMonetization::TakedownService).to receive(:takedown)
          .with(track_monetizations: [track_monetization2], takedown_source: "Tunecore Admin")

        person.block_ytm
      end
    end
  end
end
