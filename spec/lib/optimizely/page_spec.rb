require "rails_helper"

describe Optimizely::Page do
  describe "#user_data" do
    let(:person)   { create(:person) }

    context "plans user" do
      before { create(:person_plan, person: person, plan: Plan.first) }

      it "adds plans data" do
        user_data = Optimizely::Page.new(person,
                                         currency: "USD",
                                         page_type: Optimizely::Plans,
                                         page_name: "Plans")
                                    .tag_data

        [
          :current_plan_id,
          :current_plan_name,
          :next_eligible_plan_id,
          :next_eligible_plan_name
        ].each { |attr| expect(user_data[attr]).to be_truthy }
      end
    end

    context "legacy user" do
      it "adds nil plans data" do
        user_data = Optimizely::Page.new(person,
                                         currency: "USD",
                                         page_type: Optimizely::Plans,
                                         page_name: "Plans")
                                    .tag_data

        [
          :current_plan_id,
          :current_plan_name,
          :next_eligible_plan_id,
          :next_eligible_plan_name
        ].each { |attr| expect(user_data[attr]).to be_nil }
      end
    end
  end
end
