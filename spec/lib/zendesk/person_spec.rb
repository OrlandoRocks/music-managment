require "rails_helper"

describe Zendesk::Person do
  let(:person) { create(:person, :with_plan_renewal, is_verified: false) }


  describe "#create_apilogger" do
    it "should create api_logger with all the attributes" do
      person_plan = person.person_plan
      allow(ApiLoggerWorker).to receive(:perform_async).and_return(true)
      expect(Zendesk::Person.create_apilogger(person.id)).to eq(true)
      expect(ApiLogger.last.request_body).to eq(
        {
          name: person.name,
          email: person.email,
          phone: person.phone_number,
          external_id: person.id,
          user_fields:
            {
              account_status: person.status,
              country: person[:country],
              plan_level: person_plan.name,
              plan_expires: person_plan.renewal_expires_at,
              unlimited_plan_level: person_plan.name
            }
        }
      )
    end
  end

  describe "#contains_zendesk_attributes?" do
    context "receives a non-zendesk attribute" do
      it "should return false" do
        expect(Zendesk::Person::API_ATTRIBUTES.include?("partner_id")).to eq(false)
        expect(Zendesk::Person.contains_zendesk_attributes?(["partner_id"])).to eq(false)
      end
    end

    context "receives a zendesk attribute" do
      it "should return true" do
        expect(Zendesk::Person::API_ATTRIBUTES.include?("country")).to eq(true)
        expect(Zendesk::Person.contains_zendesk_attributes?(["country"])).to eq(true)
      end
    end
  end
end
