# frozen_string_literal: true
require "rails_helper"

describe RulesEngine::DSL do
  describe '.define' do
    it 'allows you to define rules' do
      my_rule = proc {}
      registry = described_class.define do
        rule :my_rule, my_rule
      end

      expect(registry).to include(my_rule: my_rule)
    end
  end
end
