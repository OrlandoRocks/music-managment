# frozen_string_literal: true
require "rails_helper"

describe RulesEngine::Evaluator do
  let(:registry) do
    {
      add: ->(a, b) { a + b }
    }
  end
  let(:globals) { {} }
  let(:evaluator) { described_class.new(registry: registry, globals: globals) }

  describe '#resolve' do
    it 'evaluates the expression/rule' do
      expect(evaluator.resolve([:add, 1, 2])).to eq(3)
    end

    it 'can evaluate nested expressions/rules' do
      expect(evaluator.resolve([:add, [:add, 1, 2], 3])).to eq(6)
    end

    # The Evaluator#resolve method is susceptible to stack overflows where the .
    # recussion stack is 3844 The test below uncovers that.
    # for the rules engine we only recurse 2 deep.
    # this is a nice to have
    xit 'can evaluate nested expressions/rules of arbitrary depth' do
      expression = ['add', 0, 1]
      3843.times do
        expression = ['add', expression, 1]
      end
      expect(evaluator.resolve(expression)).to eq(3844)
    end

    it 'supports JSON scalar values' do
      expect(evaluator.resolve('a')).to eq('a')
      expect(evaluator.resolve(1)).to eq(1)
      expect(evaluator.resolve(nil)).to be_nil
      expect(evaluator.resolve(true)).to eq(true)
      expect(evaluator.resolve(false)).to eq(false)
    end

    it 'supports JSON objects' do
      expression = {
        'foo' => ['add', 1, 2]
      }
      expect(evaluator.resolve(expression)).to eq('foo' => 3)
    end

    context 'when passing in globals' do
      let(:globals) do
        {
          'foo' => 'bar',
          'a' => { 'b' => 'c' }
        }
      end

      it 'gives you access to globals' do
        expect(evaluator.resolve(['global', 'foo'])).to eq('bar')
      end

      it 'can resolve through nested hashes' do
        expect(evaluator.resolve(['global', 'a', 'b'])).to eq('c')
      end

      it 'a nonexistent key will give you nil' do
        expect(evaluator.resolve(['global', 'nonexistent'])).to be_nil
      end
    end

    context 'when trying to call rules/expressions from other rules/expressions' do
      let(:registry) do
        {
          add1: ->(a, b) { a + b },
          add2: ->(a, b) { resolve([:add1, a, b]) }
        }
      end

      it 'will allow you to resolve rules/expressions' do
        expect(evaluator.resolve([:add2, 1, 2])).to eq(3)
      end
    end

    context 'when calling nonexistent rules' do
      let(:expr) { [:nonexistent, 1, 2] }

      it 'raises a RulesEngine::RuleMissing error' do
        expect {
          evaluator.resolve(expr)
        }.to raise_error(RulesEngine::RuleMissing)
      end

      it 'includes a stacktrace in the message' do
        expect {
          evaluator.resolve(expr)
        }.to(raise_error { |error|
          message = <<~TXT
            Unknown rule `nonexistent`
              in s-expression \e[36m[:nonexistent, 1, 2]\e[0m
          TXT
          expect(error.message).to eq(message.strip)
        })
      end
    end

    context 'when there is an error in execution' do
      let(:registry) do
        {
          add: ->(a, b) { a + b },
          divide: ->(a, b) { a / b }
        }
      end

      let(:expr) do
        {
          'foo' => [
            'add',
            1,
            [
              'divide',
              1,
              [
                'add',
                -1,
                1
              ]
            ]
          ]
        }
      end

      it 'raises a RulesEngine::RulesEngineError' do
        expect {
          evaluator.resolve(expr)
        }.to raise_error(RulesEngine::RuntimeError)
      end

      it 'includes the original error in the exception' do
        expect {
          evaluator.resolve(expr)
        }.to(raise_error { |error|
          expect(error.original_error).to be_a(ZeroDivisionError)
        })
      end

      it 'includes a stacktrace in the message' do
        expect {
          evaluator.resolve(expr)
        }.to(raise_error { |error|
          message = <<~TXT
            ZeroDivisionError: divided by 0
              while evaluating \e[31m[:divide, 1, 0]\e[0m
              in s-expression \e[36m["divide", 1, ["add", -1, 1]]\e[0m
              in s-expression \e[36m["add", 1, ["divide", 1, ["add", -1, 1]]]\e[0m
              in s-expression \e[36m{"foo"=>["add", 1, ["divide", 1, ["add", -1, 1]]]}\e[0m
          TXT

          expect(error.message).to eq(message.strip)
        })
      end
    end
  end
end