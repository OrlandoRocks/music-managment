# frozen_string_literal: true
require "rails_helper"

RSpec.describe RulesEngine::RuleRegistry do
  let(:add) { proc {} }
  let(:registry) do
    {
      add: add
    }
  end
  let(:test_engine) { RulesEngine::RuleRegistry.new(registry: registry) }

  describe '.define_rules' do
    it 'creates a new rule set using the DSL' do
      add = proc {}
      my_engine = RulesEngine::RuleRegistry.define_rules do
        rule :add, add
      end

      expect(my_engine.registry).to include(add: add)
    end
  end

  describe '#evaluate' do
    let(:expression) { ['add', 1, 2] }
    let(:evaluator) { instance_double(RulesEngine::Evaluator) }

    it 'calls out to RulesEngine::Evaluator' do
      allow(RulesEngine::Evaluator).to receive(:new).with(
        registry: registry,
        globals: {}
      ).and_return(evaluator)

      expect(evaluator).to receive(:resolve).with(expression)

      test_engine.evaluate(expression)
    end
  end

  describe '#extend' do
    it 'allows you to create rule registries' do
      subtract = proc {}
      my_engine = test_engine.extend do
        rule :subtract, subtract
      end

      expect(my_engine.registry).to include(
        add: add,
        subtract: subtract
      )
    end
  end
end