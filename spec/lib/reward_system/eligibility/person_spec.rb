require "rails_helper"

describe RewardSystem::Eligibility::Person do
  let(:person) { create(:person) }
  let(:eligibility_person) { RewardSystem::Eligibility::Person.new(person.id) }

  describe "#invalid?" do
    it "should return presence of the person record" do
      expect(eligibility_person.invalid?).to eq(person.blank?)
    end
  end

  describe "#current_tier_record" do
    it "should return last added tier class of user" do
      new_artist = create(:tier, hierarchy: 1, name: :new_artist)
      rising_artist = create(:tier, hierarchy: 2, name: :rising_artist)

      person.tiers << new_artist
      person.tiers << rising_artist

      expect(eligibility_person.current_tier_record.id).to eq(rising_artist.id)
    end
  end

  describe "#next_tier" do
    let!(:new_artist) { create(:tier, hierarchy: 1, name: :new_artist) }
    let!(:rising_artist) { create(:tier, hierarchy: 2, name: :rising_artist) }

    context "no present tier for person" do
      it "should return handler of the first available tier" do
        expect(eligibility_person.next_tier).to be_an_instance_of(new_artist.handler_service.class)
      end
    end

    context "no next tier" do
      it "should return nil" do
        person.tiers << rising_artist

        expect(eligibility_person.next_tier).to be_nil
      end
    end

    context "has next tier" do
      it "should return next tier handler by hierarchy" do
        person.tiers << new_artist

        expect(eligibility_person.next_tier).to be_an_instance_of(rising_artist.handler_service.class)
      end
    end
  end

  describe "#promote_to_tier" do
    let!(:new_artist) { create(:tier, hierarchy: 1, name: :new_artist) }

    it "should return nil when user already has the tier" do
      person.tiers << new_artist

      expect(eligibility_person.promote_to_tier(new_artist)).to be_nil
    end

    it "should add the tier to the user" do
      expect(person.tier_ids).to_not include(new_artist.id)

      eligibility_person.promote_to_tier(new_artist)

      expect(person.reload.tier_ids).to include(new_artist.id)
    end
  end
end
