require "rails_helper"

describe RewardSystem::Eligibility::Calculator do
  let(:person) { create(:person) }

  describe "#check_and_upgrade" do
    subject { RewardSystem::Eligibility::Calculator.new(person.id) }
    let(:stub_tier) { RewardSystem::Tiers::Fallback.new(nil) }

    context "no next tier" do
      it "should do nothing" do
        allow(subject.eligibility_person).to receive(:next_tier).and_return(nil)

        expect(subject.check_and_upgrade).to be_nil
      end
    end

    context "person not eligible in next tier" do
      it "should do nothing" do
        allow(subject.eligibility_person).to receive(:next_tier).and_return(stub_tier)
        allow(stub_tier).to receive(:is_person_eligible?).and_return(false)

        expect(subject.check_and_upgrade).to be_nil
      end
    end

    context "has next tiers" do
      it "should promote user to tier" do
        first_tier = create(:tier, hierarchy: 1, name: :new_artist)
        second_tier = create(:tier, hierarchy: 2, name: :rising_artist)

        expect(subject.eligibility_person).to receive(:promote_to_tier).and_call_original
        subject.check_and_upgrade
      end

      it "should invoke check_and_upgrade at the end" do
        first_tier = create(:tier, hierarchy: 1, name: :new_artist)
        second_tier = create(:tier, hierarchy: 2, name: :rising_artist)

        expect(subject).to receive(:check_and_upgrade).twice.and_call_original
        subject.check_and_upgrade
      end
    end
  end
end
