require "rails_helper"

describe RewardSystem::Events do
  let(:person_id) { 1 }

  describe "event notifier" do
    notifier_methods = {
      earnings_updated: RewardSystem::Events::EARNINGS_UPDATED,
      release_approved: RewardSystem::Events::RELEASE_APPROVED
    }

    notifier_methods.each do |notifier_method, expected_event|
      it "should enqueue worker with event name" do
        expect(RewardSystem::TierEligibilityEventWorker)
          .to receive(:perform_async)
          .with(
            person_id,
            expected_event
          )

        RewardSystem::Events.send(notifier_method, person_id)
      end
    end
  end
end
