require "rails_helper"

describe RewardSystem::TierEventer do
  let(:person) { create(:person) }

  describe "#register_event" do
    it "should create event log" do
      RewardSystem::TierEventer.new('new_event').register_event(person.id)

      log = TierEventLog.last

      expect(log.person_id).to eq(person.id)
      expect(log.event).to eq('new_event')
    end

    it "should start calculator" do
      expect_any_instance_of(RewardSystem::Eligibility::Calculator)
        .to receive(:check_and_upgrade)
      RewardSystem::TierEventer.new('new_event').register_event(person.id)
    end
  end
end
