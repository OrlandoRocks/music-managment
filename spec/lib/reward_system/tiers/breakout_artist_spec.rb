require "rails_helper"

describe RewardSystem::Tiers::BreakoutArtist do
  let(:tier) { create(:tier) }
  let(:tier_handler) { RewardSystem::Tiers::BreakoutArtist.new(tier) }

  describe "is_person_eligible?" do
    let(:person) { create(:person) }

    it "should return result from threshold criteria" do
      expect(tier_handler).to receive(:threshold_criteria_met?).and_return(true)

      expect(tier_handler.is_person_eligible?(person)).to be_truthy
    end
  end
end
