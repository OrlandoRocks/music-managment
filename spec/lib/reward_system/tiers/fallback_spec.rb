require "rails_helper"

describe RewardSystem::Tiers::Fallback do
  let(:tier) { create(:tier) }
  let(:tier_handler) { RewardSystem::Tiers::Fallback.new(tier) }

  describe "is_person_eligible?" do
    subject { tier_handler.is_person_eligible?(build(:person)) }

    it { should be_falsey }
  end
end
