require "rails_helper"

describe RewardSystem::Tiers::NewArtist do
  let(:tier) { create(:tier) }
  let(:tier_handler) { RewardSystem::Tiers::NewArtist.new(tier) }

  describe "is_person_eligible?" do
    let(:person) { create(:person) }
    subject { tier_handler.is_person_eligible?(person) }

    it { should be_truthy }

    context "user does not exist" do
      subject { tier_handler.is_person_eligible?(nil) }

      it { should be_falsey }
    end
  end
end
