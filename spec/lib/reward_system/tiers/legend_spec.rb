require "rails_helper"

describe RewardSystem::Tiers::Legend do
  let(:tier) { create(:tier) }
  let(:tier_handler) { RewardSystem::Tiers::Legend.new(tier) }

  describe "is_person_eligible?" do
    let(:person) { create(:person) }

    it "should return result from lte eligibility" do
      expect(tier_handler).to receive(:get_threshold)
      expect(tier_handler).to receive(:lte_meets_threshold?).and_return(true)

      expect(tier_handler.is_person_eligible?(person)).to be_truthy
    end
  end
end
