require "rails_helper"

describe RewardSystem::Tiers::RisingArtist do
  let(:tier) { create(:tier) }
  let(:tier_handler) { RewardSystem::Tiers::RisingArtist.new(tier) }

  describe "is_person_eligible?" do
    let(:person) { create(:person) }

    it "should return result from release threshold" do
      expect(tier_handler).to receive(:get_threshold)
      expect(tier_handler).to receive(:release_meets_threshold?).and_return(true)

      expect(tier_handler.is_person_eligible?(person)).to be_truthy
    end
  end
end
