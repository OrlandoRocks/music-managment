require "rails_helper"

describe RewardSystem::Tiers::Base do
  let(:tier) { create(:tier) }
  let(:instance) { RewardSystem::Tiers::Base.new(tier) }

  describe "is_person_eligible?" do
    it "should raise must implement message" do
      expect {
        instance.is_person_eligible?(build(:person))
      }.to raise_error("Must implement is_person_eligible?")
    end
  end

  describe "#threshold_criteria_met?" do
    let(:person) { create(:person) }

    subject { instance.threshold_criteria_met?(person) }

    context "lte threshold met" do
      before do
        expect(instance).to receive(:lte_meets_threshold?).and_return(true)
      end

      it { should be_truthy }
    end

    context "release threshold met" do
      before do
        expect(instance).to receive(:lte_meets_threshold?).and_return(false)
        expect(instance).to receive(:release_meets_threshold?).and_return(true)
      end

      it { should be_truthy }
    end

    context "user has not enough lte or required releases" do
      before do
        expect(instance).to receive(:lte_meets_threshold?).and_return(false)
        expect(instance).to receive(:release_meets_threshold?).and_return(false)
      end

      it { should be_falsey }
    end
  end

  describe "#get_threshold" do
    let(:person) { create(:person, country_website_id: CountryWebsite::CANADA) }

    it "should return threshold from user's country" do
      expected_threshold = create(
        :tier_threshold,
        tier_id: tier.id,
        country_id: person.country_website.country_id
      )

      returned_threshold = instance.get_threshold(person)
      expect(returned_threshold.id).to eq(expected_threshold.id)
    end

    context "person country does not have threshold" do
      it "should return threshold from US" do
        expected_threshold = create(
          :tier_threshold,
          tier_id: tier.id,
          country_id: Country.us_only.first.id
        )

        returned_threshold = instance.get_threshold(person)
        expect(returned_threshold.id).to eq(expected_threshold.id)
      end
    end
  end

  describe "#lte_meets_threshold?" do
    let(:person) { create(:person) }

    context "threshold is blank" do
      subject { instance.lte_meets_threshold?(nil, person) }

      it { should be_falsey }
    end

    context "with threshold" do
      let(:threshold) { create(:tier_threshold, tier: tier) }

      subject { instance.lte_meets_threshold?(threshold, person) }

      context "person earnings meets threshold lte min amount" do
        before do
          allow(person).to receive(:lifetime_earning_amount).and_return(threshold.lte_min_amount)
        end

        it { should be_truthy }
      end

      context "person earnings is less than threshold lte min amount" do
        before do
          allow(person).to receive(:lifetime_earning_amount).and_return(threshold.lte_min_amount - 1)
        end

        it { should be_falsey }
      end
    end
  end

  describe "#release_meets_threshold?" do
    let(:person) { create(:person) }

    context "threshold is blank" do
      subject { instance.release_meets_threshold?(nil, person) }

      it { should be_falsey }
    end

    context "with threshold" do
      let(:threshold) { create(:tier_threshold, tier: tier) }

      subject { instance.release_meets_threshold?(threshold, person) }

      context "releases meets threshold min release" do
        before do
          allow(person)
            .to receive(:reward_eligible_releases)
            .and_return(
              Array.new(threshold.min_release)
            )
        end

        it { should be_truthy }
      end

      context "releases is less than threshold min release" do
        before do
          allow(person)
            .to receive(:reward_eligible_releases)
            .and_return(
              Array.new(threshold.min_release - 1)
            )
        end

        it { should be_falsey }
      end
    end
  end
end
