
require "rails_helper"

describe Album, "when calling the genres method" do
  before(:each) do
    @alternative = Genre.find_by(name: 'Alternative')
    @blues = Genre.find_by(name: 'Blues')
  end

  it "should return an empty array if no genres are specified" do
    album = Album.new(:primary_genre_id => nil)
    expect(album.genres).to eq([])
  end

  it "should return a single genre array when only the primary genre has been set" do
    @album = Album.new(:primary_genre_id => @blues.id)
    expect(@album.genres).to eq([@blues])
  end

  it "should return the genres array ordered [primary_genre, secondary_genre] (test 1)" do
    album = Album.new(:primary_genre_id => @blues.id, :secondary_genre_id => @alternative.id)
    expect(album.genres).to eq([@blues, @alternative])
  end

  it "should return the genres array ordered [primary_genre, secondary_genre] (test 1)" do
    album = Album.new(:primary_genre_id => @alternative.id, :secondary_genre_id => @blues.id)
    expect(album.genres).to eq([@alternative, @blues])
  end

  it "should return the secondary genre in an array by itself if there is no primary" do
    album = Album.new(:secondary_genre_id => @blues.id)
    expect(album.genres).to eq([@blues])
  end
end
