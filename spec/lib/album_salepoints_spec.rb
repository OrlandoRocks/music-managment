require "rails_helper"

describe Album do 
  before(:each) do
    @album = TCFactory.build(:album)
    @store = TCFactory.create(:store)
    SalepointableStore.create(:store => @store, :salepointable_type => "Album")
  end

  it "should not have the same salepoint twice" do
    @album.salepoints = [TCFactory.build(:salepoint, :store => @store)]
    @album.save

    expect(@album.unselected_stores.include?(@store)).to be_falsey
  end

  it "should contain a store that is not a selected salepoint" do
    @album.salepoints = []
    @album.save
    
    expect(@album.unselected_stores.include?(@store)).to be_truthy
  end

  it "should not have the same salepoint twice when invalid salepoint" do
    @store = TCFactory.create(:store, :needs_rights_assignment => true)
    @album.salepoints = [Salepoint.new(:store => @store)]
    @album.save
    
    expect(@album.unselected_stores.include?(@store)).to be_falsey
  end
end
