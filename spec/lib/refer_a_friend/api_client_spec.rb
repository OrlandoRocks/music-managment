require "rails_helper"

describe ReferAFriend::ApiClient do
  before(:each) do
    @config_params = {"HOST" => "http:://www.test.com", "API_VERSION" => "v1", "API_USERNAME" => "foo", "API_PASSWORD" => "bar"}
    @refer_a_friend_api  = ReferAFriend::ApiClient.new(@config_params)
  end

  context "#post" do
    it "returns true when status is 201" do
      mock_response = double("response", status: 201, body: "foo body")
      allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(mock_response)
      expect(@refer_a_friend_api.post({test: "foo bar"}, "/purchases")).to eq(true)
    end

    it "returns false for any non 201 status" do
      [500, 404, 401, 302].each do |status|
        mock_response = double("response", status: status, body: "foo body")
        allow_any_instance_of(Faraday::Connection).to receive(:post).and_return(mock_response)
        expect(@refer_a_friend_api.post({test: "foo bar"}, "/purchases")).to eq(false)
      end
    end
  end
end
