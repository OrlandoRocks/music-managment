require "rails_helper"
require 'slack_notifier'

describe "#notify_in_slack" do
  it "proceeds with the notification" do
    ENV['SLACK_WEBHOOK'] = "https://sampleurl.com"
    https_double = double("https")
    expect(https_double).to receive("use_ssl=")
    expect(https_double).to receive("request")
    allow(Net::HTTP).to receive(:new).with(any_args).and_return(https_double)
    req_double = double("req").as_null_object
    allow(Net::HTTP::Post).to receive(:new).with(any_args).and_return(req_double)

    notify_in_slack("msg", "t_type")      
  end

  it "switches off notifications by not setting the env variable" do
    ENV['SLACK_WEBHOOK'] = ""
    expect {
      notify_in_slack("msg", "t_type")      
    }.not_to raise_error
  end

  it "should notify tech_deploy channel when specified" do
    slack_webhook_tech_deploy_channel = "https://techdeploywebhook.example.com/services/XFVCS"
    ENV['SLACK_WEBHOOK_TECH_DEPLOY_CHANNEL'] = slack_webhook_tech_deploy_channel

    https_double = double("https")
    expect(https_double).to receive("use_ssl=")
    expect(https_double).to receive("request")
    allow(Net::HTTP).to receive(:new).with("techdeploywebhook.example.com", 443).and_return(https_double)
    req_double = double("req").as_null_object
    allow(Net::HTTP::Post).to receive(:new).with("/services/XFVCS").and_return(req_double)

    notify_in_slack("msg", "t_type", TECH_DEPLOY_CHANNEL_NAME)
  end
end
