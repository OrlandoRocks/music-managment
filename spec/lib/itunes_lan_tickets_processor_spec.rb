require "rails_helper"

describe ItunesLanTicketsProcessor do

  let(:tickets) {
    [{
        created: Date.yesterday,
        last_modified: Date.today,
        ticket_id: 1234,
        content_ticket_type: "Audio",
        content_vendor_id: 5678,
        name: "Album Name",
        notes: [{date: Date.today, note: "note text"}]
    }]
  }

  let(:person) { create(:person) }

  let(:album) { create(:album, person_id: person.id)}

  let(:people) {
    Person
      .select("people.email,people.name,people.vip,albums.takedown_at,albums.finalized_at")
      .joins({albums: :upcs}).where({upcs: {number: album.upc.number}})
  }

  describe ".generate_ticket_emails" do
    before(:each) do
      apple_transporter_client = double("client")
      allow_any_instance_of(ItunesLanTicketsProcessor).to receive(:people_query).and_return(people)
      allow_any_instance_of(ItunesLanTicketsProcessor).to receive(:get_tickets).and_return(tickets)
    end

    it "sends the email" do
      expect(ItunesLanTicketMailer).to receive(:lan_ticket_creation).with(tickets.first, people).and_return(double({deliver: true}))
      ItunesLanTicketsProcessor.generate_ticket_emails(1)
    end
  end
end


