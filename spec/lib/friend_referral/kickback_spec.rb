require "rails_helper"

describe Tunecore::FriendReferral::Kickback do
  before(:each) do
    @person = TCFactory.build_person
    @api = Tunecore::AmbassadorApi
    @sample_response = {:garbage => :garbage}
  end
  
  describe "log events" do
    it "should log sign ups" do
      expect(@api).to receive(:get_ambassador).and_return(@sample_response)
      Tunecore::FriendReferral::Kickback.signup(@person)
      event = FriendReferralEvent.first
      expect(event.event_type).to eq('referrer_signup')
      expect(event.raw_response).to eq(@sample_response)
    end
    
    it "should log referral" do
      expect(@api).to receive(:record_event).and_return(@sample_response)
      Tunecore::FriendReferral::Kickback.record_event(TCFactory.build_invoice, @person, {:campaign_code => 'TY', :campaign_id => 123}, '192.168.2.1')
      event = FriendReferralEvent.first
      expect(event.event_type).to eq('referral')
      expect(event.raw_response).to eq(@sample_response)
    end
  end
  
  describe "handle errors" do
    before :each do
      @bad_response = {"code"=>"401", "errors"=>{"error"=>["Invalid API credentials."]}, "message"=>"UNAUTHORIZED: See error message for more details."}
      expect(@api).to receive(:get_ambassador).and_raise(Tunecore::AmbassadorApi::RequestError.new(@bad_response.inspect))
    end
    
    it "should rescue api error" do
      kickback = nil
      expect{ kickback = Tunecore::FriendReferral::Kickback.signup(@person) }.not_to raise_error
      expect(kickback.data).to be_nil
      expect(kickback.errors).to match(/Invalid API credentials/i)
    end
    
    it "should log error event" do
      expect{ kickback = Tunecore::FriendReferral::Kickback.signup(@person) }.not_to raise_error
      event = FriendReferralEvent.first
      expect(event.raw_response['errors']).to match(/Invalid API credentials/i)
    end
  end
end
