require "rails_helper"

describe AwsWrapper::Sqs, :aws_wrapper do
  let(:sdk) { double("sqs_sdk") }
  let(:queues_response) { double("queues_response") }
  let(:queue_object) { double("queue_object") }
  let(:message_object) { double("message_object") }
  let(:queue_name) { "queue_name" }
  let(:options) { {} }

  before do
    # API call
    # AWS::SQS.new.queues.named('foo_queue')
    allow(AWS::SQS).to receive(:new).and_return(sdk)
    allow(sdk).to receive(:queues).and_return(queues_response)
    allow(queues_response).to receive(:named).with(queue_name).and_return(queue_object)
  end

  describe ".size" do
    it "calls #approximate_number_of_messages on the queue looked up from the queue name" do
      size = 1
      expect(queue_object).to receive(:approximate_number_of_messages).and_return(size)
      expect(AwsWrapper::Sqs.size(queue_name: queue_name)).to eq(size)
    end

    context "when the sdk raises an error" do
      it "rescues the error and returns nil" do
        allow(sdk).to receive(:queues).and_raise("ERROR")

        expect do
          expect(AwsWrapper::Sqs.size(queue_name: queue_name)).to eq(nil)
        end.not_to raise_error
      end
    end
  end

  describe ".batch_send" do
    it "passes the messages to #batch_send of the queue looked up from the queue name" do
      messages = %w[message1 message2]

      expect(queue_object).to receive(:batch_send).with(messages)

      AwsWrapper::Sqs.batch_send(queue_name: queue_name, messages: messages)
    end

    context "when the sdk raises an error" do
      it "rescues the error" do
        allow(sdk).to receive(:queues).and_raise("ERROR")

        expect do
          AwsWrapper::Sqs.batch_send(queue_name: queue_name, messages: [])
        end.not_to raise_error
      end
    end
  end

  describe ".poll" do
    it "raises an error if a block is not provided" do
      expect do
        AwsWrapper::Sqs.poll(queue_name: queue_name)
      end.to raise_error(ArgumentError)
    end

    it "passes the options to poll on the queue looked up from the queue name" do
      expect(queue_object).to receive(:poll).with(options).and_yield(message_object)

      AwsWrapper::Sqs.poll(queue_name: queue_name, options: options) do |message|
        expect(message).to eq(message_object)
      end
    end

    context "when the sdk raises an error" do
      it "rescues the error and does not call the block" do
        allow(sdk).to receive(:queues).and_raise("ERROR")

        called_the_block = false

        AwsWrapper::Sqs.poll(queue_name: queue_name, options: options) do |message|
          called_the_block = true
        end

        expect(called_the_block).to eq(false)
      end
    end
  end
end
