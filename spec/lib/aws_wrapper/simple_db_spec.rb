require 'rails_helper'

describe AwsWrapper::SimpleDb, :aws_wrapper do
  describe ".get_attributes" do
    let(:domain_name) { "foo_domain" }
    let(:item_name) { "foo_item" }
    let(:sdk) { double("aws_sdk") }
    let(:domains_response) { double("domains_response") }
    let(:domain) { double("domain") }
    let(:items_response) { double("items_response") }
    let(:item) { double("item") }
    let(:attributes) { double("attributes") }
    let(:attributes_hash) { double("attributes_hash") }

    before(:each) do
      # API call
      # AWS::SimpleDB.new.domains['foo_domain'].items['foo_item'].attributes
      allow(AWS::SimpleDB).to receive(:new).and_return(sdk)
      allow(sdk).to receive(:domains).and_return(domains_response)
      allow(domains_response).to receive(:[]).with(domain_name).and_return(domain)
      allow(domain).to receive(:items).and_return(items_response)
      allow(items_response).to receive(:[]).with(item_name).and_return(item)
      allow(item).to receive(:attributes).and_return(attributes)
      allow(attributes).to receive(:to_h).and_return(attributes_hash)
    end

    it "returns back a hash with the right shape" do
      expect(
        AwsWrapper::SimpleDb.get_attributes(domain: domain_name, item: item_name)
      ).to eq(attributes_hash)
    end

    context "when the sdk raises an error" do
      it "returns an empty hash" do
        allow(sdk).to receive(:domains).and_raise("ERROR")

        expect(
          AwsWrapper::SimpleDb.get_attributes(domain: domain_name, item: item_name)
        ).to eq({})
      end
    end
  end
end
