require 'rails_helper'

describe AwsWrapper::S3, :aws_wrapper do
  let(:bucket_name) { "bucket_name" }
  let(:key) { "s3_object_key" }
  let(:sdk) { double("s3_sdk") }
  let(:buckets_response) { double("buckets_reponse") }
  let(:bucket) { double("bucket") }
  let(:objects_response) { double("objects_response") }
  let(:s3_object) { double("s3_object") }

  before do
    # API call
    # AWS::S3.new.buckets['foo_bucket'].objects['foo_object']
    allow(AWS::S3).to receive(:new).and_return(sdk)
    allow(sdk).to receive(:buckets).and_return(buckets_response)
    allow(buckets_response).to receive(:[]).with(bucket_name).and_return(bucket)
    allow(bucket).to receive(:objects).and_return(objects_response)
    allow(objects_response).to receive(:[]).with(key).and_return(s3_object)
  end

  describe ".write" do
    it "sends the domain, key, data, and options to the aws sdk" do
      data = double("io_object")
      options = {
        cache_control: "no-cache",
        acl: :public_read
      }

      expect(s3_object).to receive(:write).with(data, options)

      AwsWrapper::S3.write(
        bucket: bucket_name,
        key: key,
        data: data,
        options: options
      )
    end

    context "when the sdk raises an error" do
      it "rescues the error" do
        data = double("io_object")
        options = {
          cache_control: "no-cache",
          acl: :public_read
        }

        allow(sdk).to receive(:buckets).and_raise("ERROR")

        expect do
          AwsWrapper::S3.write(
            bucket: bucket_name,
            key: key,
            data: data,
            options: options
          )
        end.not_to raise_error
      end
    end
  end

  describe ".read" do
    it "sends the bucket, key, and options to the aws sdk" do
      options = {}
      read_response = double("read_response")

      expect(s3_object).to receive(:read)
        .with(options)
        .and_return(read_response)

      expect(
        AwsWrapper::S3.read(
          bucket: bucket_name,
          key: key,
          options: options
        )
      ).to eq(read_response)
    end

    context "when the sdk raises an error" do
      it "rescues the error and returns nil" do
        options = {}

        allow(sdk).to receive(:buckets).and_raise("ERROR")

        expect do
          result = AwsWrapper::S3.read(
            bucket: bucket_name,
            key: key,
            options: options
          )

          expect(result).to be_nil
        end.not_to raise_error
      end
    end
  end

  describe ".metadata" do
    it "sends the domain and key to the aws sdk" do
      time = Time.current
      allow(s3_object).to receive(:last_modified).and_return(time)

      expect(
        AwsWrapper::S3.metadata(
          bucket: bucket_name,
          key: key
        )
      ).to eq(last_modified: time)
    end

    context "when the sdk raises an error" do
      it "returns an empty hash" do
        allow(sdk).to receive(:buckets).and_raise("ERROR")

        expect(
          AwsWrapper::S3.metadata(
            bucket: bucket_name,
            key: key
          )
        ).to eq({})
      end
    end
  end

  describe ".read_url" do
    it "sends the domain, key, and options to the aws sdk" do
      port = 443
      expires = 1.week
      url_object = double("url_object")
      response_url = "https://#{bucket_name}:#{port}/test_url"

      allow(url_object).to receive(:to_s).and_return(response_url)

      expect(s3_object).to receive(:url_for).with(
        :read,
        expires: expires,
        port: port
      ).and_return(url_object)

      url = AwsWrapper::S3.read_url(
        bucket: bucket_name,
        key: key,
        options: { expires: expires, port: port }
      )

      expect(url).to eq(response_url)
    end

    context "when the sdk raises an error" do
      it "returns nil" do
        port = 443
        expires = 1.week

        allow(sdk).to receive(:buckets).and_raise("ERROR")

        url = AwsWrapper::S3.read_url(
          bucket: bucket_name,
          key: key,
          options: { expires: expires, port: port }
        )

        expect(url).to eq(nil)
      end
    end
  end

  describe ".update_options" do
    it "sends the bucket, key, and options to the aws sdk" do
      options = { content_disposition: "attachment; filname='foo.txt'" }

      expect(s3_object).to receive(:copy_from).with(
        key,
        options
      )

      AwsWrapper::S3.update_options(
        bucket: bucket_name,
        key: key,
        options: options
      )
    end

    context "when the sdk raises an error" do
      it "rescues the error" do
        options = { content_disposition: "attachment; filname='foo.txt'" }

        allow(sdk).to receive(:buckets).and_raise("ERROR")

        expect do
          AwsWrapper::S3.update_options(
            bucket: bucket_name,
            key: key,
            options: options
          )
        end.not_to raise_error
      end
    end
  end

  describe ".delete" do
    it "sends the domain, key, and options to the aws sdk" do
      options = {}

      expect(s3_object).to receive(:delete).with(options)

      AwsWrapper::S3.delete(
        bucket: bucket_name,
        key: key,
        options: options
      )
    end

    context "when the sdk raises an error" do
      it "rescues the error" do
        options = {}

        allow(sdk).to receive(:buckets).and_raise("ERROR")

        expect do
          AwsWrapper::S3.delete(
            bucket: bucket_name,
            key: key,
            options: options
          )
        end.not_to raise_error
      end
    end
  end
end
