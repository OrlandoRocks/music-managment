require 'rails_helper'

describe CheckPayment::Export do

  it "runs and creates a spreadsheet" do
    TCFactory.create(:check_transfer, export: true, export_date: nil )
    export = CheckPayment::Export.new

    expect_any_instance_of(Spreadsheet::Workbook).to receive(:write)
    export.run
  end

end
