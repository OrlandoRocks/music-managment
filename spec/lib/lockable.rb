
require "rails_helper"

describe Tunecore::Lockable, " when the album has been locked (by a person)" do

  before(:each) do
    @album = TCFactory.build_album
    @person = Person.new
    @album.locked_by = @person
  end

  it "should be editable_by? the @person" do
    expect(@album.editable_by?(@person)).to be_truthy
  end

  it "should not be editable_by a different person" do
    person = Person.new
    person2 = Person.new
    @album.locked_by = person
    expect(@album.editable_by?(person2)).to be_falsey
  end

  it "should be locked?" do
    expect(@album.locked?).to be_truthy
  end

  it "should be locked_by a type of person" do
    expect(@album.locked_by).to be_an_instance_of(Person)
  end

  it "should be locked_by @person" do
    expect(@album.locked_by).to eq(@person)
  end

  it "should be able to release the lock" do
    expect { @album.release_lock }.to change{ @album.locked? }.from(true).to(false)
  end

  it "should change .locked_by to nil after .release_lock has been called" do
    expect { @album.release_lock}.to change{ @album.locked_by }.to(nil)
  end

end
