require "rails_helper"

describe Believe::ApiClient do
  before(:each) do
    @config_params = {"HOST" => "http://www.test.com", "API_PW" => "secret"}
  end

  describe "#create_believe_error" do
    subject {Believe::ApiClient.new(@config_params)}
    before(:each) do
      @test_endpoint = "foo/bar"
      @test_errors= {errors: "FOO"}
    end

    it "should create a new record with nil album when no album remote argument" do
      subject.send(:create_believe_error, @test_endpoint, @test_errors, {})
      be = BelieveError.last
      expect(be.endpoint).to eq(@test_endpoint)
      expect(be.response_errors["errors"]).to eq(@test_errors[:errors])
      expect(be.album).to be_nil
    end

    it "should create a new record with album when album remote argument passed in" do
      album = FactoryBot.create(:album)
      subject.send(:create_believe_error, "foo/bar", @test_errors, {idAlbumRemote: album.id})
      be = BelieveError.last
      expect(be.endpoint).to eq(@test_endpoint)
      expect(be.response_errors["errors"]).to eq(@test_errors[:errors])
      expect(be.album).to eq(album)
    end
  end

  describe "#post" do
    it "should return true when status is true" do
      test = Faraday.new do |builder|
        builder.adapter :test do |stubs|
          stubs.post("foo/bar", {}) { |env| [200, {}, {"status" => true}.to_json] }
        end
      end
      allow(Faraday).to receive(:new).and_return(test)
      believe_client = Believe::ApiClient.new(@config_params)
      allow(believe_client).to receive(:get_secret).and_return({})
      expect(believe_client.post("foo/bar")).to eq({"status" => true})
    end

    it "should return false when status is false" do
      test = Faraday.new do |builder|
        builder.adapter :test do |stubs|
          stubs.post("foo/bar", {}) { |env| [200, {}, {"status" => false, "errors" => {}}.to_json] }
        end
      end
      allow(Faraday).to receive(:new).and_return(test)
      believe_client = Believe::ApiClient.new(@config_params)
      allow(believe_client).to receive(:get_secret).and_return({})
      expect(believe_client.post("foo/bar")).to eq({"status" => false, "errors" => {}})
    end
  end
end
