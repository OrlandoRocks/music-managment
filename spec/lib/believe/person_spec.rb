require "rails_helper"

describe Believe::Person do
  let(:person) { FactoryBot.create(:person, is_verified: false) }

  describe "#add_or_update" do
    it "should call post method of api client" do
      allow_any_instance_of(Believe::ApiClient).to receive(:post).and_return({status: "All good"})
      expected_client_data = person.attributes.extract!(*Believe::Person::API_ATTRIBUTES)
      expected_client_data.merge!(Believe::Person.purchased_attributes(person))
      expected_client_data.update(expected_client_data){|k, v| v.to_s}
      expected_hash = {
        idClientRemote:  person.id,
        clientData: expected_client_data.to_json
      }
      expect_any_instance_of(Believe::ApiClient).to receive(:post).with("client/addedit", expected_hash)
      expect(Believe::Person.add_or_update(person.id)).to eq({status: "All good"})
    end
  end

  describe "#contains_believe_attributes?" do
    context "receives a non-believe attribute" do
      it "should return false" do
        expect(Believe::Person::API_ATTRIBUTES.include?("partner_id")).to eq(false)
        expect(Believe::Person.contains_believe_attributes?(["partner_id"])).to eq(false)
      end
    end

    context "receives a believe attribute" do
      it "should return true" do
        expect(Believe::Person::API_ATTRIBUTES.include?("zip")).to eq(true)
        expect(Believe::Person.contains_believe_attributes?(["zip"])).to eq(true)
      end
    end
  end

  describe "#purchased_attributes?" do
    it "should set based on person methods" do
      expected_hash = {
        "has_ytsr" => person.has_active_ytm?,
        "has_publishing" => person.paid_for_composer?
      }
      expect(person).to receive(:has_active_ytm?).and_return(false)
      expect(person).to receive(:paid_for_composer?).and_return(false)

      expect(Believe::Person.purchased_attributes(person)).to eq(expected_hash)
    end
  end
end
