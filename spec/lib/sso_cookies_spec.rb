require "rails_helper"

describe SsoCookies do
  describe "#generate_hmac_sha1" do
    it 'should return hmac string for the supplied string' do
      subject = Object.new.extend(SsoCookies)

      hmac = subject.generate_hmac_sha1("1")
      expect(hmac.length).to eq(40)

      [nil, 100].each do |item|
        expect { subject.generate_hmac_sha1(item) }.to raise_error("Invalid data_str")
      end
    end
  end
end
