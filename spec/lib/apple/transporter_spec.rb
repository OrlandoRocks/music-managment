require "rails_helper"

describe Apple::Transporter do
  let(:album) { FactoryBot.create(:album) }
  describe "#transport" do
    context "transporter is not present" do
      before do
        allow($apple_transporter_client).to receive(:transporter_present).and_return(false)
      end

      it "raises an error" do
        expect{$apple_transporter_client.transport(:lookup_item, "release", album.upc.number)}.to raise_error
      end
    end

    context "transporter is present" do
      before do
        allow($apple_transporter_client).to receive(:transporter_present).and_return(true)
        allow($apple_transporter_client).to receive(:lookup_item)
      end
      it "invokes the given method on the client" do
        expect($apple_transporter_client).to receive(:lookup_item).with("release", album.upc.number)
        $apple_transporter_client.transport(:lookup_item, "release", album.upc.number)
      end
    end
  end

  describe "#lookup_item" do
    context "release" do
      before do
        @xml_document       = double("release_xml")
        config              = double({noblanks: true})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:release_xml).with(album.upc.number).and_return(@xml_document)
        allow(Nokogiri::XML).to receive(:parse).with(@xml_document).and_yield(config).and_return(@xml_document)
        allow(@xml_document).to receive(:at_xpath).with("//apple_id").and_return(double({content: "12345"}))
      end
      it "given an album UPC, gets the album XML and apple ID from Apple" do
        expect(@transporter_client.lookup_item("release", album.upc.number)).to eq({
          document: @xml_document,
          apple_id: 12345
        })
      end
    end

    context "artist" do
      before do
        xml_document        = double("artist_xml")
        config              = double({noblanks: true})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:artist_xml).with("c137").and_return(xml_document)
        allow(Nokogiri::XML).to receive(:parse).with(xml_document).and_yield(config).and_return(xml_document)
        allow(xml_document).to receive(:at_xpath).with("//name").and_return(double({text: "Summer Smith"}))
        allow(xml_document).to receive(:at_xpath).with("//appleId").and_return(double({text: "c137"}))
      end

      it "returns the artist's apple ID and name" do
        result = @transporter_client.lookup_item("artist", "c137")
        expect(result[:apple_id]).to eq("c137")
        expect(result[:name]).to eq("Summer Smith")
      end
    end
  end

  describe "#release_status" do
    before do
      @xml_document       = double({
        at_xpath: double({
          attributes: double({
            :[] => double({
              value: 12345
            })
          })
        })
      })
      config              = double({noblanks: true})
      @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
      allow(@transporter_client).to receive(:release_status_xml).with(album.upc.number).and_return(@xml_document)
      allow(Nokogiri::XML).to receive(:parse).with(@xml_document).and_yield(config).and_return(@xml_document)
    end
    it "given an album UPC, gets the album XML and apple ID from Apple" do
      expect(@transporter_client.release_status(album.upc.number)).to eq({
        document: @xml_document,
        apple_id: 12345
      })
    end
  end

  describe "#validate_package" do
    context "when valid" do
      before do
        @package            = double("package", {dir: double({path: "path/to/package"})})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:package_validation_exec).with(@package).and_return("1 packages were verified successfully")
      end
      it "returns an object with valid: true" do
        expect(@transporter_client.validate_package(@package)[:valid]).to eq(true)
      end
    end

    context "when not valid" do
      before do
        error_xml = %{
          <itunes_transporter>

            <log level="error">
              <message>
                ERROR ITMS-5001: &quot;Does not have cover art file&quot; at Album
              </message>
              <message>
                ERROR ITMS-5013: &quot;File container is not present.  An asset file is required unless: a) the provider is cleared for metadata-only preorders, b) this is a metadata-only update&quot; at Album/Tracks/Track
              </message>
              <message>
                ERROR ITMS-5013: &quot;File container is not present.  An asset file is required unless: a) the provider is cleared for metadata-only preorders, b) this is a metadata-only update&quot; at Album/Tracks/Track
              </message>
              <error_code>
                1102
              </error_code>
            </log>
          </itunes_transporter
        }
        @package            = double("package", {dir: double({path: "path/to/package"})})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:package_validation_exec).with(@package).and_return(error_xml)
      end
      it "returns an object with valid: false" do
        expect(@transporter_client.validate_package(@package)[:valid]).to eq(false)
      end
    end
  end

  describe "#send_package" do
    context "when successful" do
      before do
        @package            = double("package", {dir: double({path: "path/to/package"})})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:package_send_cmd).with(@package).and_return("1 packages were uploaded successfully")
      end
      it "returns true" do
        expect(@transporter_client.send_package(@package)[:success]).to eq(true)
      end

      context "when apple provides a success message that is gramatically correct" do
        before do
          allow(@transporter_client).to receive(:package_send_cmd).with(@package).and_return("1 package was uploaded successfully")
        end
        it "returns true" do
          expect(@transporter_client.send_package(@package)[:success]).to eq(true)
        end
      end
    end

    context "when not successful" do
      before do
        @package            = double("package", {dir: double({path: "path/to/package"})})
        @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
        allow(@transporter_client).to receive(:package_send_cmd).with(@package).and_return("failed to upload 1 package")
      end
      it "returns false" do
        expect(@transporter_client.send_package(@package)[:success]).to eq(false)
      end
    end
  end

  describe "#query_tickets" do
    before(:each) do
      @ticket_data = {
        created: Date.yesterday.to_s,
        updated: Date.today.to_s,
        id: 1234,
        content_ticket_type: "Audio",
        content_vendor_id: 5678,
        name: "Album Name"
      }
      xml_document        = double("doc")
      ticket              = double("ticket")
      note                = double("note")
      config              = double({noblanks: true})
      @transporter_client = Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG)
      allow(@transporter_client).to receive(:query_tickets_cmd).and_return(xml_document)
      allow(Nokogiri::XML).to receive(:parse).with(xml_document).and_yield(config).and_return(xml_document)
      allow(xml_document).to receive(:xpath).with("tickets//ticket").and_return([ticket])
      allow(ticket).to receive(:xpath).with("contentTicketState").and_return(double({text: "Your Action Needed"}))
      allow(ticket).to receive(:xpath).with("notes//note").and_return([note])
      allow(note).to receive(:text).and_return("note text")
      allow(note).to receive(:xpath).with("date").and_return(double({text: Date.today.to_s}))
      allow(ticket).to receive(:xpath).with("created").and_return(double({text: @ticket_data[:created]}))
      allow(ticket).to receive(:xpath).with("lastModified").and_return(double({text: @ticket_data[:updated]}))
      allow(ticket).to receive(:xpath).with("ticketId").and_return(double({text: @ticket_data[:id]}))
      allow(ticket).to receive(:xpath).with("contentTicketType").and_return(double({text: @ticket_data[:content_ticket_type]}))
      allow(ticket).to receive(:xpath).with("contentVendorId").and_return(double({text: @ticket_data[:content_vendor_id]}))
      allow(ticket).to receive(:xpath).with("name").and_return(double({text: @ticket_data[:name]}))
    end

    it "returns the tickets array" do
      tickets = @transporter_client.query_tickets
      ticket  = tickets.first
      expect(tickets.length).to eq(1)
      expect(ticket[:created]).to eq(Time.parse(@ticket_data[:created]))
      expect(ticket[:last_modified]).to eq(Time.parse(@ticket_data[:updated]))
      expect(ticket[:content_ticket_type]).to eq(@ticket_data[:content_ticket_type])
      expect(ticket[:content_vendor_id]).to eq(@ticket_data[:content_vendor_id])
      expect(ticket[:name]).to eq(@ticket_data[:name])
      expect(ticket[:notes].first[:note]).to eq("note text")
    end
  end

  describe "#create_artist" do
    before(:each) do
      @existing_artist = 'Jennifer'
      @non_existing_artist = 'Jennifer1'
      @artist_locale = 'en'
      @force_token = '1d82c6f6-26ef-3e28-8a97-558c7c5717da'
      @apple_id = '123456'
      @provider = 'TuneCore'
    end

    it "should be called with the correct params" do
      allow_any_instance_of(Apple::Transporter).to receive(:`) do |*, cmd|
        expect(cmd).to include(@non_existing_artist)
        expect(cmd).to include(@artist_locale)
        expect(cmd).to include(@provider)
      end.and_return("Artist created #{@non_existing_artist} #{@apple_id} ")

      $apple_transporter_client.create_artist(@non_existing_artist, @artist_locale)
    end

    context "when the artist name doesn't exist on Apple" do
      before do
        allow($apple_transporter_client).to receive(:create_artist_cmd)
          .with(@non_existing_artist, @artist_locale, nil)
          .and_return("Artist created #{@non_existing_artist} #{@apple_id} ")
      end
      it "should return an object with success true and the artist apple id" do
        output = $apple_transporter_client.create_artist(@non_existing_artist, @artist_locale)
        expect(output[:success]).to eq(true)
        expect(output[:force_create_token]).to be_nil
        expect(output[:apple_id]).to_not be_nil
      end
    end

    context "when the artist name already exists on Apple" do
      before do
        result = "Artist not created, to force the artist to be created use the option -forceCreateToken \"#{@force_token}\"\n"
        allow($apple_transporter_client).to receive(:create_artist_cmd)
          .with(@existing_artist, @artist_locale, nil).ordered
          .and_return(result)
        allow($apple_transporter_client).to receive(:create_artist_cmd)
          .with(@existing_artist, @artist_locale, @force_token).ordered
          .and_return("Artist created #{@existing_artist} #{@apple_id} ")
      end

      it "should force create the artist and return success true with the apple artist id" do
        output = $apple_transporter_client.create_artist(@existing_artist, @artist_locale)
        expect(output[:success]).to eq(true)
        expect(output[:force_create_token]).to be_nil
        expect(output[:apple_id]).to_not be_nil
      end
    end

    context "when invalid" do
      before do
        allow($apple_transporter_client).to receive(:create_artist_cmd)
        .with(@existing_artist, 'en2', nil).and_return("Error")
      end
      it "should return an object with success false" do
        output = $apple_transporter_client.create_artist(@existing_artist, 'en2')
        expect(output[:success]).to eq(false)
        expect(output[:force_create_token]).to be_nil
        expect(output[:apple_id]).to be_nil
      end
    end
  end

  describe '#curated_artists' do
    let(:transporter_client) { Apple::Transporter.new(ITUNES_TRANSPORTER_CONFIG) }
    let(:package) { double("package", {dir: double({path: "path/to/package"})}) }
    let(:artist_names) { ['Skateboard P'] }
    subject(:curated_artist) { transporter_client.curated_artists(package, artist_names)}

    context 'When artist is curated' do
      before do
        allow_any_instance_of(Apple::Transporter).to receive(:curated_artist_exec)
        .with(package).and_return("A very long stdder string including: Curated Artist: Curated Artist: The artist '#{artist_names.first}' has been curated")
      end

      it 'executes transporter command' do
        expect_any_instance_of(Apple::Transporter).to receive(:curated_artist_exec).once
        subject
      end

      it "executes transporter command and returns the curated_artists" do
        expect(subject).to eq(artist_names)
      end
    end

    context 'When artist is not curated' do

      before do
        allow_any_instance_of(Apple::Transporter).to receive(:curated_artist_exec)
        .with(package).and_return("A very long string of stderr from itunes transporter")
      end

      it 'executes transporter command' do
        expect_any_instance_of(Apple::Transporter).to receive(:curated_artist_exec).once
        subject
      end

      it 'executes transporter verify command and does not return an artist' do
        expect(subject).to eq([])
      end
    end
  end
end
