require "rails_helper"

describe Apple::ApiClient do
  describe "#post" do
    it "gets receipt with latest date" do
      expected_date = Date.today + 3.years
      json = { status: 0, receipt: { in_app: [{expires_date: Date.today}, {expires_date: expected_date}, {expires_date: Date.today - 1.year}] } }
      fake_apple_api(body: json, method_call: :make_post)
      expect($apple_client.post("/url", {}).data[:expires_date]).to eq expected_date.to_s
    end
  end
end
