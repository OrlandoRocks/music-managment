require "rails_helper"

describe TcDistributor::Base do

  describe '#tc_distributor_metadata' do

    let!(:album) { create(:album) }
    let!(:distributions) {
      [
        create(:distribution, :delivered_to_amazon),
        create(:distribution, :delivered_to_spotify),
      ]
    }
    let!(:monetization) { create(:track_monetization) }

    let!(:distribution_without_salepoints) { [create(:distribution)] }

    let!(:store_ids) { [distributions[0].salepoints[0].store_id, distributions[1].salepoints[0].store_id] }

    let!(:latest_release_response) { double(:latest_release_response) }
    let!(:latest_releases_stores_response) { double(:latest_releases_stores_response) }
    let!(:no_releases_store_response) { double(:no_releases_store_response) }
    let!(:latest_releases_stores_tracks_response) { double(:latest_releases_stores_tracks_response) }
    let!(:no_releases_stores_tracks_response) { double(:no_releases_stores_tracks_response) }

    before do
      allow(DistributorAPI::Album).to receive(:latest_release).
                                      with(album.id).
                                      and_return(latest_release_response)

      allow(DistributorAPI::Album).to receive(:latest_releases_stores).and_call_original

      allow(DistributorAPI::Album).to receive(:latest_releases_stores).
                                      with(album.id, {store_id: store_ids}).
                                      and_return(latest_releases_stores_response)

      allow(DistributorAPI::Album).to receive(:latest_releases_stores).
                                      with(album.id, {store_id: []}).
                                      and_return(no_releases_store_response)

      allow(DistributorAPI::Album).to receive(:latest_releases_stores_tracks).
                                      with(album.id, {track_ids: [monetization.song.id]}).
                                      and_return(latest_releases_stores_tracks_response)

      allow(DistributorAPI::Album).to receive(:latest_releases_stores_tracks).
                                      with(album.id, {track_ids: []}).
                                      and_return(no_releases_stores_tracks_response)
    end

    context 'when tc distributor does not return release, releases_stores, releases_stores_tracks' do
      before do
        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_release_response).
                                                  and_return(JSON.parse({release: []}.to_json, object_class: OpenStruct))

        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_releases_stores_response).
                                                  and_return(JSON.parse({releases_stores: []}.to_json, object_class: OpenStruct))
                                                
        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_releases_stores_tracks_response).
                                                  and_return(JSON.parse({releases_stores: []}.to_json, object_class: OpenStruct))
      end

      it 'return empty release releases_stores, and releases_stores_tracks objects when no release and releases_stores are present' do
        tc_distributor_metadata = described_class.new(album_id: album.id, distributions: distributions, monetization_song_ids: [monetization.song.id]).tc_distributor_metadata
        expect(tc_distributor_metadata[:release].present?).to eq(false)
        expect(tc_distributor_metadata[:releases_stores].present?).to eq(false)
        expect(tc_distributor_metadata[:latest_releases_stores_tracks].present?).to eq(false)
      end
    end

    context 'when tc distributor returns release, releases_stores, and releases_stores_tracks' do
      before do
        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_release_response).
                                                  and_return(
                                                    JSON.parse({
                                                      release: {
                                                        version: 'v1'
                                                      }
                                                    }.to_json, object_class: OpenStruct)
                                                  )

        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_releases_stores_response).
                                                  and_return(
                                                    JSON.parse({
                                                      releases_stores: [{
                                                        store_id: store_ids[0],
                                                        state: 'received'
                                                      }, {
                                                        store_id: store_ids[1],
                                                        state: 'delivered'
                                                      }
                                                    ]}.to_json, object_class: OpenStruct)
                                                  )

        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_releases_stores_tracks_response).
                                                  and_return(
                                                    JSON.parse({
                                                      releases_stores: [ {
                                                        song_id: monetization.song.id,
                                                        state: 'delivered'
                                                      }
                                                    ]}.to_json, object_class: OpenStruct)
                                                  )
      end

      it 'return empty release and releases_stores objects when no release and releases_stores are present' do
        tc_distributor_metadata = described_class.new(album_id: album.id, distributions: distributions, monetization_song_ids: [monetization.song.id]).tc_distributor_metadata
        expect(tc_distributor_metadata[:latest_release].version).to eq('v1')
        expect(tc_distributor_metadata[:latest_releases_stores][distributions[0].id].state).to eq('received')
        expect(tc_distributor_metadata[:latest_releases_stores][distributions[1].id].state).to eq('delivered')
        expect(tc_distributor_metadata[:latest_releases_stores_tracks][0].state).to eq('delivered')
      end
    end

    context 'when distributions salepoints have no stores and albums have no monetizations' do
      before do
        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: latest_release_response).
                                                  and_return(
                                                    JSON.parse({
                                                      release: {
                                                        version: 'v1'
                                                      }
                                                    }.to_json, object_class: OpenStruct)
                                                  )

        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: no_releases_store_response).
                                                  and_return(JSON.parse({releases_stores: []}.to_json, object_class: OpenStruct))

        allow_any_instance_of(described_class).to receive(:response_body_for).
                                                  with(api_response: no_releases_stores_tracks_response).
                                                  and_return(JSON.parse({releases_stores: []}.to_json, object_class: OpenStruct))
      end
      it 'returns release but no release_stores or releaes_stores_tracks object' do
        tc_distributor_metadata = described_class.new(album_id: album.id, distributions: distribution_without_salepoints, monetization_song_ids: []).tc_distributor_metadata
        expect(tc_distributor_metadata[:latest_release].version).to eq('v1')
        expect(tc_distributor_metadata[:releases_stores].present?).to eq(false)
        expect(tc_distributor_metadata[:latest_releases_stores_tracks].present?).to eq(false)
      end
    end
  end

  describe '#xml_metadata' do
    it 'returns XML metadata' do
      metadata_xml_path = {
        key: 'key',
        bucket: 'bucket'
      }
      allow(DistributorAPI::ReleasesStore).to receive(:list).with(id: 1).and_return(JSON.parse([
        {
          http_body: {
            releases_stores: [{
              metadata_xml_path: metadata_xml_path
            }]
          }
        }].to_json, object_class: OpenStruct)
      )

      xml_metadata = described_class.new(releases_store_id: 1).xml_metadata
      expect(xml_metadata["bucket"]).to eq("bucket")
      expect(xml_metadata["key"]).to eq("key")
    end
  end

  describe '#json_metadata' do
    it 'returns JSON metadata' do
      release_json_path = {
        key: 'key',
        bucket: 'bucket'
      }

      allow(DistributorAPI::Release).to receive(:list).with(id: 1).and_return(JSON.parse([
        {
          http_body: {
            releases: [{
              release_json_path: release_json_path
            }]
          }
        }].to_json, object_class: OpenStruct)
      )

      json_metadata = described_class.new(release_id: 1).json_metadata
      expect(json_metadata["bucket"]).to eq("bucket")
      expect(json_metadata["key"]).to eq("key")
    end
  end

  describe '#releases_stores' do
    it 'returns releases_stores with passed in releases_store_ids' do
      allow(DistributorAPI::ReleasesStore).to receive(:list).with(id: [1,2]).and_return(JSON.parse([
        {
          http_body: {
            releases_stores: [{
              id: 1,
              xml_state: 'packaged'
            }, {
              id: 2,
              xml_state: 'delivered'
            }]
          }
        }].to_json, object_class: OpenStruct)
      )

      releases_stores = described_class.new(releases_store_ids: [1,2]).releases_stores

      expect(releases_stores.length).to eq(2)
      expect(releases_stores[0].id).to eq(1)
      expect(releases_stores[0].xml_state).to eq('packaged')
      expect(releases_stores[1].id).to eq(2)
      expect(releases_stores[1].xml_state).to eq('delivered')
    end
  end
end
