ENV['RAILS_ENV'] = 'test'

require File.expand_path('../config/environment', __dir__)
abort("The Rails environment is running in production mode!") if Rails.env.production?

require_relative "./spec_helper"

require 'rspec/rails'
require 'capybara/rails'
require 'capybara/rspec'
require 'paper_trail/frameworks/rspec'
require 'digest/md5'
require 'sidekiq/testing'
require 'webmock/rspec'
require 'test-prof'
require "test_prof/recipes/rspec/sample"
require 'fake_braintree'
require_relative '../test/support/rails_cache_helper'
require_relative '../test/support/rails_env_helper'
FakeBraintree.activate!

# TODO: Remove duplication between this and spec_helper
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
Dir[Rails.root.join('spec/shared_examples/**/*.rb')].each { |f| require f }

# to suppress a bunch warnings in test output, should fix eventually
RSpec::Expectations.configuration.on_potential_false_positives = :nothing
WebMock.allow_net_connect!

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.include ActionView::Helpers::TranslationHelper # to get the t methods for views
  config.include CustomTranslationHelper # to get our custom_t method
  config.include OauthEncryptionHelper
  config.include AppleApiHelper
  # TODO_ACTIVE_MERCHANT: Clean up mocking of braintree
  config.include FakeBraintree
  config.include PaymentHelper
  config.include WaitForAjax, type: :feature
  config.include FeatureSpecDbProxy::RemoteFactoryProxy, type: :feature
  config.include FeatureSpecDbProxy::RemoteActiveRecordBaseProxy, type: :feature
  config.include MockApplicationHelper
  config.include ResponseHelper
  config.include XmlHelper
  config.include FeatureHelper
  config.include JsonHelper
  config.include TrendsHelper
  config.include VersioningHelper
  config.include FakeVatApiHelper
  config.include ActiveSupport::Testing::TimeHelpers
  config.include RailsCacheHelper
  config.include RailsEnvHelper
  config.include FakeTwilio

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.global_fixtures = [
    :people,
    :people_roles,
    :rights,
    :rights_roles,
    :features,
    :person_balances
  ]
  config.use_transactional_fixtures = true
  config.infer_base_class_for_anonymous_controllers = false

  config.example_status_persistence_file_path = \
    Rails.root.join('tmp', 'rspec_status_persistence.txt')

  # TODO: Clean up
  config.before(:each) do
    # allow(STDOUT).to receive(:puts)

    Sidekiq::Worker.clear_all

    allow(IpAddressGeocoder).to receive(:geocode).and_return({
      city:        "Brooklyn",
      subdivision: "New York",
      country:     "United States",
      latitude:    40.6501,
      longitude:   -73.9496
    }.with_indifferent_access)

    # TODO_ACTIVE_MERCHANT: Clean up mocking of braintree
    allow(Tunecore::Braintree::Query).to receive(:new).and_return(fake_braintree_query)

    allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)

    $rollout.deactivate(:recaptcha)

    allow_any_instance_of(TcVat::ApiClient)
      .to receive(:send_request!)
      .and_return(
        FakeVatApiHelper.generate do |response|
          response.valid           = true
          response.vat_number      = '123'
          response.name            = 'test'
          response.country         = 'Austria'
          response.place_of_supply = 'Luxembourg'
          response.tax_rate        = 0
          response.tax_type        = 'VAT'
          response.error_message   = nil
        end
      )
    FakeBraintree.clear!

    stub_const("Twilio::REST::Client", FakeTwilio::FakeTwilioClient)
  end

  config.before(:each, :run_sidekiq_worker) do
    Sidekiq::Testing.inline!
  end

  config.after(:each, :run_sidekiq_worker) do
    Sidekiq::Testing.fake!
  end

  Sidekiq.logger.level = Logger::FATAL

  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.before(:all) do
    $redis.flushdb
  end

  config.after(:all) do
    $redis.flushdb
  end

  config.after(:each) do
    I18n.locale = :en
    ActionMailer::Base.deliveries.clear
  end

  config.include Shoulda::Matchers::ActiveModel, type: :model
  config.include Shoulda::Matchers::ActiveRecord, type: :model
end
