require 'rails_helper'

describe PayoutProviderRegistrationsController do
  describe "#create" do
    it "renders a js file with the response json from the api" do
      person = create(:person)
      login_as(person)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)

      link = "https://payouts.sandbox.payoneer.com/partners/lp.aspx?token=d0e28b0822814128b2c3282727c4165b2282CB1C18&langid=1"
      registration_form = double(:registration_form, save: true, link: link)
      allow(PayoutProvider::RegistrationForm).to receive(:new).and_return(registration_form)

      post :create, params: { payout_provider: { name: 'payoneer', payout_methods_list: "Paypal" } }
      expect(response.status).to eq(302)
      expect(response).to redirect_to(link)
    end

    it "is not found when a user does not have the payoneer payout enabled" do
      person = create(:person, country_website_id: CountryWebsite::CANADA, country: "CA")
      login_as(person)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:maintenance_alert, person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, person).and_return(false)

      link = "https://payouts.sandbox.payoneer.com/partners/lp.aspx?token=d0e28b0822814128b2c3282727c4165b2282CB1C18&langid=1"
      registration_form = double(:registration_form, save: true, link: link)
      allow(PayoutProvider::RegistrationForm).to receive(:new).and_return(registration_form)

      post :create, params: { payout_provider: { name: 'payoneer' } }
      expect(response.status).to eq(302)
      expect(response).to_not redirect_to(link)
    end
  end
end
