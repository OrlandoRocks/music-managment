require "rails_helper"

describe BalanceHistoriesController, type: :controller do
  describe '#show' do
    it "should redirect to the legacy system" do

      person = create(:person)
      login_as(person)
      get :show

      expect(response).to redirect_to list_transactions_path
    end
  end
end
