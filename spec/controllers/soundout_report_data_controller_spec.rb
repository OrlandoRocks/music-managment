require "rails_helper"

describe Api::SoundoutReportDataController, type: :controller do

  before(:each) do
    allow_any_instance_of(Song).to receive(:duration).and_return(95000)
    @person = Person.where("email=?","admin@tunecore.com").first
    @album1 = create(:album, :finalized, person: @person, name: "Album")
    @song1 = create(:song, :with_s3_asset, album: @album1, name: "song1")
    petri_bundle = create(:petri_bundle, album: @album1)
    distribution = create(:distribution, :delivered_to_spotify_find_or_create_salepoint, petri_bundle: petri_bundle)
    login_as(@person)

    @soundout_report = create(:soundout_report, person: @person, track: @song1)
    json = File.open("#{Rails.root}/spec/fixtures/soundout_report.json", "r").read
    @soundout_report.report_data = json
  end

  describe "data" do

    it "should return the data part of a soundout report" do
      get :show, params: { id: @soundout_report.id, format: :json }
      assert_response :success, @response.body

      expect {
        @json = JSON.parse(@response.body)
      }.not_to raise_error

      expect(@json['reviews']).to be_blank
      expect(@json['data']).not_to be_blank
    end

  end

  describe "reviews" do

    it "should return only reviews" do
      get :reviews, params: { id: @soundout_report.id, :format=>:json }
      assert_response :success, @response.body

      expect {
        @json = JSON.parse(@response.body)
      }.not_to raise_error

      expect(@json['reviews']).not_to be_blank
      expect(@json['data']).to be_blank
      expect(@json['reviews'].size).to eq(50)
    end

  end

end
