require "rails_helper"

describe NotificationsController, type: :controller do
  describe "archive page" do
    it "shows a page of past archived notifications" do
      @person = create(:person)
      notification = create(:notification, person: @person, first_archived: DateTime.now)

      login_as(@person)
      get :index

      expect(assigns(:notifications)).to include(notification)
      assert_response :success, @response.body
    end
  end

  describe "dropdown" do
    it "marks all notifications as read" do
      @person = create(:person)
      login_as(@person)

      3.times do
        create(:notification, person: @person)
      end

      expect do
        get :dropdown, xhr: true
        assert_response :success, @response.body
      end.to change(@person.notifications.seen(false), :count).by(-3)
    end

    it "returns notifications" do
      @person = create(:person)
      login_as(@person)

      notification = create(:notification, person: @person)

      get :dropdown, xhr: true
      assert_response :success, @response.body

      expect(assigns(:notifications)).to include(notification)
    end
  end

  describe "#action" do
    context "when the requested MIME type is html" do
      it "marks a notification as clicked" do
        @person = create(:person)
        login_as(@person)

        notification = create(:notification, person: @person)

        post :action, params: { id: notification.id }, xhr: true, as: :html
        assert_response :success, @response.body

        expect(notification.reload.first_clicked).not_to be_blank
      end

      it "marks a notification as seen" do
        person = create(:person)
        login_as(person)
        notification = create(:notification, person: person)

        post(:action, params: { id: notification.id }, xhr: true, as: :html)

        expect(response).to be_successful
        expect(notification.reload.first_seen).to be_present
      end
    end

    context "when the requested MIME type is text/javascript" do
      it "responds with 200 OK" do
        @person = create(:person)
        login_as(@person)

        notification = create(:notification, person: @person)

        post :action, params: { id: notification.id }, xhr: true, as: :js
        assert_response :success, @response.body
      end
    end
  end

  describe "#archive" do
    context "when the requested MIME type is html" do
      it "marks a notification as archived" do
        @person = create(:person)
        login_as(@person)

        notification = create(:notification, person: @person)

        post :archive, params: { id: notification.id }, xhr: true
        assert_response :success, @response.body

        expect(notification.reload.first_archived).not_to be_blank
      end
    end

    context "when the requested MIME type is text/javascript" do
      it "responds with with 200 OK" do
        @person = create(:person)
        login_as(@person)

        notification = create(:notification, person: @person)

        post :archive, params: { id: notification.id }, xhr: true, as: :js
        assert_response :success, @response.body
      end
    end
  end
end
