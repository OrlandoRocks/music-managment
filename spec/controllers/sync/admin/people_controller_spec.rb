require "rails_helper"

describe Sync::Admin::PeopleController, type: :controller do

  describe "index" do

    it "should not include administrators with the sync feature" do
      admin = Person.where(:email=>"admin@tunecore.com").first
      customer = Person.where(:email=>"customer@tunecore.com").first

      admin.features << features(:sync_licensing)
      customer.features << features(:sync_licensing)

      login_as(admin)

      get :index
      assert_response :success

      assert assigns[:music_supervisors].include? customer
      assert !assigns[:music_supervisors].include?(admin)
    end

  end

end
