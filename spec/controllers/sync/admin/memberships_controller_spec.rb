require "rails_helper"

describe Sync::Admin::MembershipsController, type: :controller do
  let(:admin) { Person.where(name: "Tunecore Admin").first }
  let(:customer) { Person.where(name: "Tunecore Customer").first }

  describe "approve" do
    it "should require admin role" do
      @login_as = customer
      login_as(@login_as)

      sr = TCFactory.create(:sync_membership_request)

      expect {
        get :approve, params: { :id=>sr.id }
        assert_redirected_to dashboard_path
      }.not_to change{ SyncMembershipRequest.where(status: "pending").count }
    end

    it "should set the status to approved" do
      @login_as = admin
      @login_as.features << features(:sync_licensing)
      login_as(@login_as)

      sr = TCFactory.create(:sync_membership_request)

      expect {
        get :approve, params: { :id=>sr.id }
        assert_redirected_to sync_admin_memberships_path
      }.to change{ SyncMembershipRequest.where(status: "pending").count }.by(-1)

      expect(sr.reload.status).to eq("approved")
    end

    it "should send an email on approval" do
      @login_as = admin
      @login_as.features << features(:sync_licensing)
      login_as(@login_as)

      sr = TCFactory.create(:sync_membership_request)

      expect {
       get :approve, params: { :id=>sr.id }
       assert_redirected_to sync_admin_memberships_path
      }.to change(ActionMailer::Base.deliveries, :count).by(1)
    end
  end

  describe "decline" do
    it "should set the status to declined" do
      @login_as = admin
      @login_as.features << features(:sync_licensing)
      login_as(@login_as)

      sr = TCFactory.create(:sync_membership_request)

      expect {
        get :decline, params: { :id=>sr.id }
        assert_redirected_to sync_admin_memberships_path
      }.to change{ SyncMembershipRequest.where(status: "pending").count }.by(-1)

      expect(sr.reload.status).to eq("declined")
    end
  end

  describe "resend" do
    it "should resend for approved requests" do
      expect {
        @login_as = admin
        @login_as.features << features(:sync_licensing)
        login_as(@login_as)

        sr = TCFactory.create(:sync_membership_request, :status=>"approved")
        get :resend, params: { :id=>sr.id }
        assert_redirected_to sync_admin_memberships_path
      }.to change(ActionMailer::Base.deliveries, :count).by(1)
    end
  end
end
