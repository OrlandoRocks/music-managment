require "rails_helper"

describe Sync::MembershipsController, type: :controller do
  describe "create" do
    it "should create a pending membership" do
      expect {
        expect {
          post :create, params: {
            :sync_membership_request=>{:email=>"johnrambo@rambo.com",
            :name=>"John Rambo",
            :company=>"Rambo LLC",
            :country=>"United States",
            :phone_number=>"631-384-2915",
            :website=>"www.rambollc.com"}
          }
        }.to change(ActionMailer::Base.deliveries, :count).by(1)
      }.to change(SyncMembershipRequest, :count).by(1)

      assert_redirected_to sync_login_path
      assert SyncMembershipRequest.last.status == "pending"
    end

    it "should act like a membership was requested if theres already a pending one" do
      request = create(:sync_membership_request)

      expect {
        post :create, params: {
          :sync_membership_request=>{:email=>request.email,
          :name=>request.name,
          :company=>request.company,
          :website=>request.website}
        }
        assert_redirected_to sync_login_path
      }.not_to change(SyncMembershipRequest, :count)
    end

    it "should act like a membership was requested if theres already a user" do
      person = create(:person)

      expect {
        post :create, params: {
          :sync_membership_request=>{:email=>person.email,
          :name=>"John",
          :company=>"Rambo LLC",
          :website=>"Rambo.com"}
        }
        assert_redirected_to sync_login_path
      }.not_to change(SyncMembershipRequest, :count)
    end
  end
end
