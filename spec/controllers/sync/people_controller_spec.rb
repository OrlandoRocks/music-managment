require "rails_helper"

describe Sync::PeopleController, type: :controller do

  describe "new" do

    it "should redirect if it cannot find the membership request by code" do
      get :new, params: { request_code: "non-existance-code" }
      expect(response).to redirect_to(sync_root_path)
      expect(flash.keys).to include("error")
    end

    it "should redirect to login if the membership request has already registered" do
      sr = create(:sync_membership_request, person: Person.first)

      get :new, params: { request_code: sr.request_code }
      expect(response).to redirect_to(sync_login_path)
      expect(flash.keys).to include("message")
    end

    it "should render the form if it finds the membership request by code" do
      sr = create(:sync_membership_request)

      get :new, params: { request_code: sr.request_code }

      expect(response).to be_successful
      expect(flash.keys).not_to include("error")
    end

  end

  describe "create" do

    it "should redirect if it cannot find the membership request by code" do
      post :create, params: { request_code: "non-existing-code", person: {
        phone_number: "123-123-1234",
        country: "United States",
        password: "1234abcD!",
        password_confirmation: "1234abcD!"
      }}

      expect(response).to redirect_to(sync_root_path)
      expect(flash.keys).to include("error")
    end

    it "should redirect to login if the membership request has already registered" do
      sr = create(:sync_membership_request, person: Person.first)

      post :create, params: { request_code: sr.request_code, person: {
        phone_number:          "123-123-1234",
        country:               "United States",
        password:              "1234abcD!",
        password_confirmation: "1234abcD!"
      }}

      expect(response).to redirect_to(sync_login_path)
      expect(flash.keys).to include("message")
    end

    it "should fill in information from the membership request" do
      sr = create(:sync_membership_request)

      post :create, params: { request_code: sr.request_code, person: {
        accepted_terms_and_conditions: "1",
        password:                      "1234abcD!",
        password_confirmation:         "1234abcD!"
      }}

      expect(response).to redirect_to(week_sync_chart_path)
      expect(flash.keys).not_to include("error")

      assert !sr.reload.person_id.blank?
      assert sr.person.name == sr.name
      assert sr.person.business_name == sr.company
      assert sr.person.email == sr.email
      assert sr.person.country = sr.country
      assert sr.person.phone_number = sr.phone_number
    end

    it "should render back with an invalid person with a bad form submission" do
      sr = create(:sync_membership_request)

      #Don't accept terms and conditions
      post :create, params: { request_code: sr.request_code, person: {
        phone_number:          "123-123-1234",
        country:               "United States",
        password:              "1234abcD!",
        password_confirmation: "1234abcD!"
      }}

      expect(response).to be_successful

      assert assigns[:person]
      assert !assigns[:person].valid?
    end

  end

end
