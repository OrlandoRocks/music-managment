require "rails_helper"

describe Sync::SearchController, type: :controller do
  before(:each) do
    @sync_licensing = Feature.find_by(name: "sync_licensing")
    @person = TCFactory.create(:person, features: [@sync_licensing])
    login_as(@person)
  end

  it "should return matched songs in json response" do
    first_composition   = TCFactory.create(:composition)
    second_composition  = TCFactory.create(:composition)
    album               = TCFactory.create(:album)
    first_song          = TCFactory.create(:song, album: album, composition_id: first_composition.id)
    second_song         = TCFactory.create(:song, album: album, composition_id: second_composition.id)
    TCFactory.create(:muma_song, composition: first_composition)
    TCFactory.create(:muma_song, composition: second_composition)

    # Set up test results for the amazon cloud search query so we are not dependent
    # on actual results
    test_query = "test"
    test_result = { hits: { hit: [{ id: "s#{first_song.id}" }, { id: "s#{second_song.id}" }] } }.to_json
    expect(Net::HTTP).to receive(:get).and_return(test_result)
    get :search, params: { q: test_query }, xhr: true

    assert_response :success, @response.body
    assert json = ActiveSupport::JSON.decode(@response.body)
    assert !json["songs"].empty?

    song_ids = [first_song.id, second_song.id]
    composition_ids = [first_song.composition_id, second_song.composition_id]

    expect(song_ids).to include(json["songs"].first["song"]["id"])
    expect(song_ids).to include(json["songs"].last["song"]["id"])

    expect(composition_ids).to include(json["songs"].first["song"]["composition_id"])
    expect(composition_ids).to include(json["songs"].last["song"]["composition_id"])

    expect(json["songs"].first["song"]["id"]).not_to eq(json["songs"].last["song"]["id"])
    expect(json["songs"].last["song"]["compositin_id"]).not_to eq(json["songs"].last["song"]["composition_id"])

    expect(json["songs"].first["song"]["album_id"]).to eq(album.id)
    expect(json["songs"].last["song"]["album_id"]).to eq(album.id)

    expect(json["page"]).to eq("1")
  end
end
