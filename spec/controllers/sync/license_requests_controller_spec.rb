require "rails_helper"

describe Sync::LicenseRequestsController, type: :controller do
  it "should require sync_licensing feature" do
    person = create(:person)
    login_as(person)

    get :index
    assert_response :redirect, @response.body

    person.feature_people.create(feature: features(:sync_licensing))

    get :index
    assert_response :success, @response.body
  end

  it "should require song_id for new, create, and confirmation actions" do
    person = create(:person)
    person.features << features(:sync_licensing)
    login_as(person)

    muma_song = TCFactory.create(:muma_song)

    get :new
    assert_redirected_to sync_search_path

    post :create
    assert_redirected_to sync_search_path

    get :confirmation
    assert_redirected_to sync_search_path
  end

  it "creates a sync licesnse request" do
    person = create(:person)
    person.features << features(:sync_licensing)
    login_as(person)

    muma_song = TCFactory.create(:muma_song)

    expect do
      expect do
        post :create, params: { song_id: muma_song.code }

        assert_redirected_to sync_license_request_path(SyncLicenseRequest.last)
        assert flash[:message] == "Your request has been submitted. Thank you."

      end.not_to change(SyncLicenseOption, :count)
    end.not_to change(SyncLicenseProduction, :count)
  end

  it "creates a sync license request with a sync license production" do
    person = create(:person)
    person.features << features(:sync_licensing)
    login_as(person)

    muma_song = TCFactory.create(:muma_song)

    expect do
      expect do
        post :create, params: {
          song_id: muma_song.code,
          sync_license_request: {
            sync_license_production_attributes: {
              title: "test production"
            }
          }
        }

        assert_redirected_to sync_license_request_path(SyncLicenseRequest.last)
        assert flash[:message] == "Your request has been submitted. Thank you."
      end.not_to change(SyncLicenseOption, :count)
    end.to change(SyncLicenseProduction, :count).by(1)
  end

  it "crates a sync license request with sync license options" do
    person = create(:person)
    person.features << features(:sync_licensing)
    login_as(person)

    muma_song = TCFactory.create(:muma_song)

    expect do
      expect do
        post :create, params: {
          song_id: muma_song.code,
          sync_license_request: {
            sync_license_production_attributes: {
              title: "test production"
            },
            sync_license_options_attributes: {
              "0" => {
                offer: "100", number: 0
              },
              "1" => {
                offer: "200", number: 1
              }
            }
          }
        }

        assert_redirected_to sync_license_request_path(SyncLicenseRequest.last)
        assert flash[:message] == "Your request has been submitted. Thank you."

        assert SyncLicenseRequest.last.sync_license_options.count == 2
        expect(SyncLicenseRequest.last.sync_license_production).not_to be_nil

      end.to change(SyncLicenseOption, :count).by(2)
    end.to change(SyncLicenseProduction, :count).by(1)
  end
end
