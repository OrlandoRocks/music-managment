require "rails_helper"

describe Sync::IndexController do

  it "should properly redirect users on index action" do
    @person = TCFactory.create(:person)

    get :index
    assert_redirected_to sync_home_path

    login_as(@person)

    get :index
    assert_redirected_to week_sync_chart_path

    @person.features << features(:sync_licensing)

    get :index
    assert_redirected_to week_sync_chart_path
  end

end
