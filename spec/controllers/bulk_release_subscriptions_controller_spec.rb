require "rails_helper"

describe BulkReleaseSubscriptionsController, type: :controller do
  describe "#create" do
    let(:person) { create(:person) }
    let(:album)  { create(:album, person: person) }

    before(:each) do
      login_as(person)
    end

    it "creates a note when a bulk subscription is takendown" do
      post :create, params: { bulk_release_subscription: { subscription_actions: { album.id => "takedown" } } }

      expect(Note.where(related_id: album.id, note: "Album was taken down")).to exist
    end
  end
end
