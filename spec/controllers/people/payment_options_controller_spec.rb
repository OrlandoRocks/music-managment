require "rails_helper"

describe People::PaymentOptionsController do

  describe "#index" do

    context "when a user is signed in" do
      it "should redirect them to the edit payment tab" do
        @person = FactoryBot.create(:person)
        login_as(@person)
        get :index
        expect(response.redirect_url).to include("/people/#{@person.id}/edit?tab=payment")
      end
    end

    context "when a user is coming from TC Social checkout modal" do
      it "passes the return_to_tcs value along" do
        @person = FactoryBot.create(:person)
        login_as(@person)
        get :index, params: { tcs_return_msg: 'true' }
        expect(response.redirect_url).to include("tcs_return_msg=true")
      end
    end

    context "when a user is not signed in" do
      it "should redirect them to login page" do
        get :index
        expect(response.redirect_url).to include("/login")
      end
    end

  end

end
