require "rails_helper"

describe SinglesController, type: :controller do
  describe "GET #show" do
    let(:person) { create(:person)}
    let(:album) { create(:single, :explicit, person: person)}

    before(:each) do
      login_as(person)
    end

    it "backfills asset metadata when unfinalized" do
      expect(controller).to receive(:backfill_album_metadata)

      get :show, params: { id: album }
    end

    it "does not backfill asset metadata when finalized" do
      album.update_attribute(:finalized_at, Date.today.to_s)

      expect(controller).not_to receive(:backfill_album_metadata)

      get :show, params: { id: album }
    end
  end
end
