require "rails_helper"

describe VideosController, type: :controller do
  let(:person) do
    create(
      :person,
      name: 'trevor',
      email: 'trevor@protocool.net',
    )
  end

  context "when being accessed by customers w/o videos" do
    it "GET index should be successful" do
      login_as(person)

      get 'index'

      expect(response).to be_successful
    end
  end

  context "when being accessed by customers with videos" do
    before do
      create(:music_video, person: person, name: 'first video')
      create(:music_video, person: person, name: 'second video')

      create(:feature_film, person: person, name: 'first feature')
      create(:music_video, person: person, name: 'second feature')
    end

    it "GET index should be successful" do
      login_as(person)

      get 'index'

      expect(response).to be_successful
    end
  end

  context "when being accessed without a customer login" do
    it "GET index should not be successful" do
      get 'index'

      expect(response).not_to be_successful
    end
  end
end
