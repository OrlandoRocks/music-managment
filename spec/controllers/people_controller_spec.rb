require "rails_helper"

describe PeopleController, type: :controller do
  describe "sign up redirect" do
    before(:each) do
      $redis.flushdb
      allow(controller).to receive(:record_optimizely_event).and_return(true)
      allow(controller).to receive(:recaptcha).and_return(true)
    end

    it "should redirect to canadian domain when signing up as a canadian user" do
      get :new
      assert_response :success, @response.body

      post :create, params: { person: {
        accepted_terms_and_conditions: 1,
        country: "CA",
        email: "test@tunecore.com",
        is_opted_in: 0,
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        postal_code: "E4Y2E4"
      }}

      assert_redirected_to(controller: "dashboard", action: "create_account", params: { fb: "true" }, host: CountryWebsite.find(CountryWebsite::CANADA).url.to_s)
    end

    it "should redirect to united states domain when signing up as a us customer on canadian domain" do
      @request.host = CountryWebsite.find(CountryWebsite::CANADA).url.to_s

      get :new
      assert_response :success, @response.body

      post :create, params: { person: {
        accepted_terms_and_conditions: 1,
        country: "US",
        email: "test@tunecore.com",
        is_opted_in: 0,
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        postal_code: "11215"
      }}

      assert_redirected_to(controller: "dashboard", action: "create_account", params: { fb: "true" }, host: CountryWebsite.find(CountryWebsite::UNITED_STATES).url.to_s)
    end

    it "should not change hosts when signing up as a canadian user on canadian domain" do
      @request.host = CountryWebsite.find(CountryWebsite::CANADA).url.to_s

      get :new
      assert_response :success, @response.body

      post :create, params: { person: {
        accepted_terms_and_conditions: 1,
        country: "CA",
        email: "test@tunecore.com",
        is_opted_in: 0,
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        postal_code: "E4Y2E4"
      }}

      assert_redirected_to(controller: "dashboard", action: "create_account", params: { fb: "true" }, host: CountryWebsite.find(CountryWebsite::CANADA).url.to_s)
    end

    it "should not change hosts when signing up as a US user on US domain" do
      @request.host = CountryWebsite.find(CountryWebsite::UNITED_STATES).url.to_s

      get :new
      assert_response :success, @response.body

      post :create, params: { person: {
        accepted_terms_and_conditions: 1,
        country: "US",
        email: "test@tunecore.com",
        is_opted_in: 0,
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        postal_code: "11215"
      }}

      assert_redirected_to(controller: "dashboard", action: "create_account", params: { fb: "true" }, host: CountryWebsite.find(CountryWebsite::UNITED_STATES).url.to_s)
    end

    it "should not redirect when on studio" do
      @request.host = "studio.tunecore.com"

      get :new
      assert_response :success, @response.body

      post :create, params: { person: {
        accepted_terms_and_conditions: 1,
        country: "US",
        email: "test@tunecore.com",
        is_opted_in: 0,
        name: "Test Man",
        password: "Testpass123!",
        password_confirmation: "Testpass123!",
        postal_code: "11215"
      }}

      assert_redirected_to(controller: "dashboard", action: "create_account", params: { fb: "true" }, host: "studio.tunecore.com")
    end
  end

  describe PeopleController, type: :controller do
    context "referral_data from cookies" do
      it "builds from prefixed with wp_tunecore_" do
        cookie_data_1 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }
        cookie_data_2 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }
        cookies[:tunecore_wp] = cookie_data_1.to_json
        cookies[:tunecore_wp_1] = cookie_data_2.to_json
        get :new
        expect(assigns[:person].referral_data.size).to eq 14
      end

      it "doesnt build from non wp_tunecore_ cookies" do
        cookies["other_cookie"] = { ref: "TestData" }.to_json
        get :new
        expect(assigns[:person].referral_data.size).to eq 0
      end

      it "doesnt build from a wp_tunecore with a bad date" do
        cookie_data_bad_date = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: "BAAAAD"
        }
        cookies[:tunecore_wp_1] = cookie_data_bad_date.to_json
        get :new
        expect(assigns[:person].referral_data.size).to eq 7
      end

      it "deletes tunecore_wp cookies" do
        allow(controller).to receive(:recaptcha).and_return(true)

        cookie_data_1 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }
        cookie_data_2 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }

        request.cookies[:tunecore_wp] = cookie_data_1.to_json
        request.cookies[:tunecore_wp_1] = cookie_data_2.to_json

        post :create, params: { person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "test@tunecore.com",
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215"
        }}

        expect(response.cookies[:tunecore_wp]).to eq nil
        expect(response.cookies[:tunecore_wp_1]).to eq nil
      end

      it "doesnt delete other cookies" do
        cookie_data_1 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }
        cookie_data_2 = {
          ref: "TestData",
          jt: 123,
          utm_source: "pubadmin",
          utm_medium: "web",
          utm_term: "music",
          utm_content: "testing",
          utm_campaign: "footer-links",
          tc_timestamp: 1_458_752_702
        }
        cookies[:not_a_tc] = cookie_data_1.to_json
        cookies[:not_a_tc_1] = cookie_data_2.to_json

        post :create, params: { person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "test@tunecore.com",
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215"
        }}

        expect(cookies[:not_a_tc]).to eq cookie_data_1.to_json
        expect(cookies[:not_a_tc_1]).to eq cookie_data_2.to_json
      end

      it "creates referral data from params" do
        referral_data = {
            "0"=> {
              key: "jt",
              value: "offer",
              page: "signup",
              source: "cookie",
              timestamp: "2020-04-08 10:33:22 +0530"
            }
          }
        post :create, params: { person: {
          accepted_terms_and_conditions: 1,
          country: "US",
          email: "test@tunecore.com",
          is_opted_in: 0,
          name: "Test Man",
          password: "Testpass123!",
          password_confirmation: "Testpass123!",
          postal_code: "11215",
          referral_data_attributes: referral_data
        }}
        referral_data = assigns[:person].reload.referral_data.first
        expect(referral_data).not_to be_nil
        expect(referral_data.key).to eq "jt"
        expect(referral_data.value).to eq "offer"
        expect(referral_data.page).to eq "signup"
        expect(referral_data.source).to eq "cookie"
        expect(referral_data.timestamp).to eq "2020-04-08 10:33:22 +0530"
      end
    end

    context "referral_data from url parameters" do
      it "builds whitelisted" do
        get :new, params: { utm_source: "TestData", ref: "TestData2" }
        expect(assigns[:person].referral_data.size).to eq 2
      end

      it "doesnt build non-whitelisted" do
        get :new, params: { other_param: "TestData" }
        expect(assigns[:person].referral_data.size).to eq 0
      end
    end

    describe "find_targeted_offer_by_token" do
      context "when the targeted_offer exists and is valid" do
        it "it sets @targeted_offer correctly" do
          targeted_offer = create(:targeted_offer, :join_token, :active)
          cookies[:tunecore_wp] = { jt: targeted_offer.join_token }.to_json
          get :new
          expect(assigns[:targeted_offer]).to eq targeted_offer
        end
      end

      context "when the targeted_offer exists but is not valid" do
        it "@targeted_offer is nil" do
          allow_any_instance_of(TargetedOffer).to receive(:inactive?).and_return(true)
          targeted_offer = create(:targeted_offer, :join_token, :active)
          cookies[:tunecore_wp] = { jt: targeted_offer.join_token }.to_json
          get :new
          expect(assigns[:targeted_offer]).to be_nil
        end
      end

      context "when the targeted offer does not exist" do
        it "@targeted_offer is nil" do
          cookies[:tunecore_wp] = { jt: "bad-token" }.to_json
          get :new
          expect(assigns[:targeted_offer]).to be_nil
        end
      end
    end

    describe "PATCH #update_account_settings" do
      context "with valid password entered" do
        it "updates account settings" do
          person = login_as(
            create(:person, name: "Harvey Birdman")
          )
          params = {
            id: person.id,
            person: {
              account_settings_partial: "contact_info",
              address1: "1 Main St",
              address2: "Apt 1",
              city: "New York",
              name: "Space Ghost",
              original_password: "Testpass123!",
              state: "NY",
            }
          }

          patch :update_account_settings, params: params, xhr: true

          expect(person.name).to eq "Space Ghost"
          expect(person.address1).to eq "1 Main St"
          expect(person.address2).to eq "Apt 1"
          expect(person.city).to eq "New York"
          expect(person.state).to eq "NY"
        end

        it "sets the state name correctly based on passed state id" do
          state = CountryState.first
          person = login_as(
            create(:person, name: "Rajesh Koothrapalli")
          )
          params = {
            id: person.id,
            state_id: state.id,
            person: {
              account_settings_partial: "contact_info",
              address1: "Namaste Street",
              address2: "Apt 1",
              city: "Chennai",
              original_password: "Testpass123!"
            }
          }

          patch :update_account_settings, params: params, xhr: true

          expect(person.address1).to eq "Namaste Street"
          expect(person.address2).to eq "Apt 1"
          expect(person.city).to eq "Chennai"
          expect(person.state).to eq state.name
        end
      end

      context "without a valid password entered" do
        it "does NOT update account settings" do
          person = login_as(
            create(:person, name: "Harvey Birdman")
          )
          params = {
            id: person.id,
            person: {
              account_settings_partial: "contact_info",
              name: "Not Harvey",
              original_password: "BAD_password",
            }
          }

          patch :update_account_settings, params: params, xhr: true

          expect(person.name).to eq "Harvey Birdman"
        end
      end
    end

    describe "PATCH #update_priority_artist" do
      let!(:person) { create(:person) }
      let!(:birdman_artist) { create(:artist, name: "Michael Keaton") }
      let!(:other_birdman_artist) { create(:artist, name: "Lil Weezyanna") }
      before(:each) do
        login_as(person)
      end
      context "user has no primary artist" do
        it "creates a primary artist" do
          params = {
            id: person.id,
            person: {
              priority_artist_id: birdman_artist.id
            }
          }
          expect(PriorityArtist).to receive(:create)

          patch :update_priority_artist, params: params, xhr: true
        end
        it "sets the appropriate priority artist" do
          params = {
            id: person.id,
            person: {
              priority_artist_id: birdman_artist.id
            }
          }

          patch :update_priority_artist, params: params, xhr: true

          expect(person.priority_artist.artist_id).to be(birdman_artist.id)
        end
      end
      context "user has a primary artist" do
        before(:each) do
          PriorityArtist.create(person: person, artist: birdman_artist)
        end
        it "updates the user's primary artist" do
          params = {
            id: person.id,
            person: {
              priority_artist_id: other_birdman_artist.id
            }
          }
          expect_any_instance_of(PriorityArtist).to receive(:update)

          patch :update_priority_artist, params: params, xhr: true
        end
        it "sets the appropriate priority artist" do
          params = {
            id: person.id,
            person: {
              priority_artist_id: other_birdman_artist.id
            }
          }

          patch :update_priority_artist, params: params, xhr: true

          expect(person.priority_artist.artist_id).to be(other_birdman_artist.id)
        end
      end
    end

    describe "recaptcha" do
      context "without the recaptcha param" do
        it "re-renders the signup page" do
          allow_any_instance_of(RecaptchaHelper).to receive(:use_recaptcha?).and_return(true)

          post :create, params: {
            person: {
              accepted_terms_and_conditions: 1,
              country: "US",
              email: "test@tunecore.com",
              is_opted_in: 0,
              name: "Test Man",
              password: "Testpass123!",
              password_confirmation: "Testpass123!",
              postal_code: "11215"
            }
          }

          expect(response).to render_template("new")
        end
      end

      context "with the recaptcha param" do
        before(:each) do
          allow(RecaptchaService).to receive(:verify).and_return(true)
        end

        it "verifies the user's recaptcha with Google before signing up" do
          allow_any_instance_of(RecaptchaHelper).to receive(:use_recaptcha?).and_return(true)
          expect(RecaptchaService).to receive(:verify).exactly(1).times

          post :create, params: {
            person: {
              accepted_terms_and_conditions: 1,
              country: "US",
              email: "test@tunecore.com",
              is_opted_in: 0,
              name: "Test Man",
              password: "Testpass123!",
              password_confirmation: "Testpass123!",
              postal_code: "11215"
            },
            "g-recaptcha-response" => "yup"
          }
        end

        context "valid recaptcha response" do
          before(:each) do
            allow(RecaptchaService).to receive(:verify).and_return(true)
          end
          it "reroutes the signed up user" do
            expect(subject).not_to receive(:reroute_failed_recaptcha)
            post :create, params: { person: {
              accepted_terms_and_conditions: 1,
              country: "US",
              email: "test@tunecore.com",
              is_opted_in: 0,
              name: "Test Man",
              password: "Testpass123!",
              password_confirmation: "Testpass123!",
              postal_code: "11215"
            }, "g-recaptcha-response" => "yup" }
          end
        end

        context "invalid recaptcha response" do
          before(:each) do
            allow(RecaptchaService).to receive(:verify).and_return(false)
            allow_any_instance_of(RecaptchaHelper).to receive(:use_recaptcha?).and_return(true)
          end
          it "re-renders the sign up page" do
            post :create, params: { person: {
              accepted_terms_and_conditions: 1,
              country: "US",
              email: "test@tunecore.com",
              is_opted_in: 0,
              name: "Test Man",
              password: "Testpass123!",
              password_confirmation: "Testpass123!",
              postal_code: "11215"
            }, "g-recaptcha-response" => "yup" }
            expect(response).to render_template("new")
          end

          context "valid recaptcha response" do
            before(:each) do
              allow(RecaptchaService).to receive(:verify).and_return(true)
            end
            it "reroutes the signed up user" do
              expect(subject).not_to receive(:reroute_failed_recaptcha)
              post :create, params: { person: {
                accepted_terms_and_conditions: 1,
                country: "US",
                email: "test@tunecore.com",
                is_opted_in: 0,
                name: "Test Man",
                password: "Testpass123!",
                password_confirmation: "Testpass123!",
                postal_code: "11215"
              }, "g-recaptcha-response" => "yup" }
            end
          end

          context "invalid recaptcha response" do
            before(:each) do
              allow(RecaptchaService).to receive(:verify).and_return(false)
            end
            it "re-renders the sign up page" do
              post :create, params: { person: {
                accepted_terms_and_conditions: 1,
                country: "US",
                email: "test@tunecore.com",
                is_opted_in: 0,
                name: "Test Man",
                password: "Testpass123!",
                password_confirmation: "Testpass123!",
                postal_code: "11215"
              }, "g-recaptcha-response" => "yup" }
              expect(response).to render_template("new")
            end
          end
        end
      end
    end

    describe ".show_and_set_domain_modal" do
      context "when @ip_country_code is different than country website" do
        context "@ip_country_code is also a valid country website" do
          it "should set @redirect_country to the found country" do
            allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return("DE")
            allow(IpAddressGeocoder).to receive(:geocode).and_return({ country_code: "FR" }.with_indifferent_access)

            get :new

            expect(assigns(:redirect_country)).to eq("FR")
          end
        end

        context "@ip_country_code is not a supported country website" do
          it "should assign @redirect_country to 'US'" do
            allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return("DE")
            allow(IpAddressGeocoder).to receive(:geocode).and_return({ country_code: "BE" }.with_indifferent_access)

            get :new

            expect(assigns(:redirect_country)).to eq("US")
          end
        end
      end

      context "when @ip_country_code is the same as the country_website" do
        context "for US code/domain" do
          it "does not assign @show_modal or @redirect_country" do
            allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return("US")
            allow(IpAddressGeocoder).to receive(:geocode).and_return({ country_code: "US" }.with_indifferent_access)

            get :new

            expect(assigns(:show_modal)).to be_falsey
            expect(assigns(:redirect_country)).to be_falsey
          end
        end

        context "for non .com domains" do
          it "assigns @redirect_country to 'US'" do
            allow_any_instance_of(CountryDomainHelper).to receive(:country_website).and_return("FR")
            allow(IpAddressGeocoder).to receive(:geocode).and_return({ country_code: "FR" }.with_indifferent_access)

            get :new

            expect(assigns(:redirect_country)).to eq("US")
          end
        end
      end
    end

    describe 'PATCH #update_vat_information' do
      it 'updates vat information for business customer' do
        person = login_as(
          create(:person, country: 'UK')
        )
        params = {
          id: person.id,
          person: {
            customer_type: 'business',
            vat_information_attributes: { company_name: 'company', vat_registration_number: 'U57477929' }
          }
        }

        patch :update_vat_information, params: params, xhr: true

        expect(person.customer_type).to eq 'business'
        expect(person.vat_information.company_name).to eq 'company'
        expect(person.vat_information.vat_registration_number).to eq 'U57477929'
      end

      it 'updates vat information for individual customer' do
        person = login_as(
          create(:person, country: 'UK')
        )
        params = {
          id: person.id,
          person: {
            customer_type: 'individual',
            vat_information_attributes: { vat_registration_number: 'U57477929' }
          }
        }

        patch :update_vat_information, params: params, xhr: true

        expect(person.customer_type).to eq 'individual'
        expect(person.vat_information.vat_registration_number).to eq 'U57477929'
      end

      it 'updates vat information with invalid vat number for individual customer' do
        person = login_as(
          create(:person, country: 'UK')
        )
        params = {
          id: person.id,
          person: {
            customer_type: 'individual',
            vat_information_attributes: { vat_registration_number: '123456' }
          }
        }

        patch :update_vat_information, params: params, xhr: true
        expect(flash[:error]).to eq("Error: Invalid format for VAT Identification Number added, try again")
      end

      it 'updates vat information with invalid vat format for business customer' do
        person = login_as(
          create(:person, country: 'UK')
        )
        params = {
          id: person.id,
          person: {
            customer_type: 'business',
            vat_information_attributes: { company_name: 'company', vat_registration_number: '123456' }
          }
        }

        patch :update_vat_information, params: params, xhr: true
        expect(flash[:error]).to eq("Error: Invalid format for VAT Identification Number added, try again")
      end
    end

    describe 'PATCH #update_self_billing_status' do
      it 'updates self billing status' do
        person = login_as(
          create(:person, name: 'Rajesh Koothrapalli')
        )
        params = {
          id: person.id,
          person: {
            account_settings_partial: 'self_billing_info',
            self_billing_status_attributes: {
              status: 'accepted'
            }
          }
        }

        patch :update_self_billing_status, params: params, xhr: true

        expect(person.self_billing_status.status).to eq 'accepted'
      end
    end

    describe 'PATCH #update_renewal_billing_info' do
      it 'updates address info, billing status, and vat info' do
        person = login_as(
          create(:person, name: 'Cmrn Wuz Here', customer_type: 'business')
        )
        params = {
          id: person.id,
          person: {
            address1: 'new address',
            self_billing_status_attributes: {
              status: 'accepted'
            },
            vat_information_attributes: {
              company_name: 'Cmrn Inc.'
            }
          }
        }

        patch :update_renewal_billing_info, params: params, xhr: true

        expect(person.self_billing_status.status).to eq 'accepted'
        expect(person.address1).to eq 'new address'
        expect(person.vat_information.company_name).to eq 'Cmrn Inc.'
      end

      it 'sets flag to renewal_billing_form_filled'
    end

    describe 'POST #confirm_vat_info' do
      it 'should validate vat registration number and pass vat rate' do
        allow_any_instance_of(TcVat::ApiClient)
          .to receive(:send_request!).with(:validate).and_return(FakeVatApiHelper::Response.new(true))
        allow(TcVat::Calculator)
          .to receive(:fetch_vat_applicable_rate).with('Inbound', 'business', 'Luxembourg').and_return(17.0)
        allow(TcVat::Calculator)
          .to receive(:fetch_vat_applicable_rate).with('Outbound', 'business', 'Luxembourg').and_return(0.0)

        person = login_as(create(:person, name: 'Rajesh'))
        params = {
          id: person.id,
          vat_info: {
            country_code: 'LU',
            vat_registration_number: 'LU17596310'
          },
          address_values: {
            address1: "",
            address2: "",
            city: "",
            corporate_entity_id: "2",
            country: "LU",
            customer_type: "Individual",
            state: "",
            postal_code: "",
            compliance_info: {
              first_name: "Test",
              last_name: "Name",
              company_name: ""
            }
          }
        }
        response = post :confirm_vat_info, params: params, xhr: true
        parsed_response = JSON.parse(response.body)

        expect(parsed_response['valid_vat']).to eq true
        expect(parsed_response['outbound_rate']).to eq 0.0
        expect(parsed_response['inbound_rate']).to eq 17.0
      end
    end
  end
end

describe PeopleController, type: :controller do
  let(:person) { create(:person) }
  let(:tax_forms) { create_list(:tax_form, 4, person: person) }

  before(:each) do
    login_as(person)
  end

  describe "GET #edit" do
    it "initializes taxform mapping variables on edit" do
      person.stub(:primary_tax_form).and_return(tax_forms[0])
      person.stub(:secondary_tax_form).and_return(tax_forms[1])
      person.stub(:user_mapped_distribution_tax_form).and_return(tax_forms[2])
      person.stub(:user_mapped_publishing_tax_form).and_return(tax_forms[3])
      get :edit

      expect(controller.instance_variable_get(:@primary_tax_form)).to eq(tax_forms[0])
      expect(controller.instance_variable_get(:@secondary_tax_form)).to eq(tax_forms[1])
      expect(controller.instance_variable_get(:@user_mapped_distribution_tax_form)).to eq(tax_forms[2])
      expect(controller.instance_variable_get(:@user_mapped_publishing_tax_form)).to eq(tax_forms[3])
    end
  end
end
