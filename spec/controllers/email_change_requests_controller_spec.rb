# frozen_string_literal: true

require "rails_helper"

describe EmailChangeRequestsController, type: :controller do
  let!(:person) { create(:person) }
  let!(:email_title_colon) { custom_t("email_change_title") + ": "}

  before(:each) do
    login_as(person)
  end

  context "valid request" do
    let!(:change_request) do
      create(
        :email_change_request,
        person: person,
        new_email: "greggy@email.com",
        token: SecureRandom.hex
      )
    end

    it "should update the Person and EmailChangeRequest" do
      old_email = person.email
      get :complete_verification, params: { token: change_request.token }

      person.reload
      change_request.reload
      expect(person.email).not_to eq(old_email)
      expect(person.email).to eq(change_request.new_email)
      expect(change_request.finalized).to eq(true)
    end
    it "should redirect to the dashboard with successful message" do
      get :complete_verification, params: { token: change_request.token }

      expect(flash[:notice]).to eq(email_title_colon + custom_t("email_changed.success"))
      assert_redirected_to dashboard_path
    end
    it "should send email" do
      expect(PersonNotifier).to receive_message_chain(:change_email, :deliver)

      get :complete_verification, params: { token: change_request.token }
    end
  end

  context "invalid request" do
    context "no change request" do
      it "should redirect to the dashboard path with a failure message" do
        get :complete_verification, params: { token: "1111" }

        expect(flash[:error]).to eq(email_title_colon + custom_t("email_changed.fail"))
        assert_redirected_to dashboard_path
      end
      it "should not send email" do
        expect(PersonNotifier).not_to receive(:change_email)

        get :complete_verification, params: { token: "1111" }
      end
    end
    context "same email" do
      let!(:change_request) do
        create(
          :email_change_request,
          person: person,
          new_email: person.email,
          token: SecureRandom.hex
        )
      end
      it "should redirect to the dashboard path with a failure message" do
        get :complete_verification, params: { token: change_request.token }

        expect(flash[:error]).to eq(email_title_colon + custom_t("email_changed.fail"))
        assert_redirected_to dashboard_path
      end
      it "should not update the person or change request" do
        get :complete_verification, params: { token: change_request.token }

        expect(person).not_to receive(:update)
        expect(change_request).not_to receive(:update)
      end
      it "should not send email" do
        expect(PersonNotifier).not_to receive(:change_email)

        get :complete_verification, params: { token: change_request.token }
      end
    end
    context "expired request" do
      let!(:change_request) do
        create(
          :email_change_request,
          person: person,
          new_email: "greggy@email.com",
          token: SecureRandom.hex,
          created_at: Time.current - 3.days
        )
      end
      it "should redirect to the dashboard path with a failure message" do
        get :complete_verification, params: { token: change_request.token }

        expect(flash[:error]).to eq(email_title_colon + custom_t("email_changed.fail"))
        assert_redirected_to dashboard_path
      end
      it "should not update the person or change request" do
        get :complete_verification, params: { token: change_request.token }

        expect(person).not_to receive(:update)
        expect(change_request).not_to receive(:update)
      end
      it "should not send email" do
        expect(PersonNotifier).not_to receive(:change_email)

        get :complete_verification, params: { token: change_request.token }
      end
    end
  end
end
