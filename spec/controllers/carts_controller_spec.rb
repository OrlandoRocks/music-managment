require "rails_helper"

describe CartsController do

  describe "show" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
    end

    it "should call setup_cart_variables" do
      get :show
      expect(assigns(:page_title)).to eq custom_t('controllers.carts.shopping_cart')
      expect(assigns(:albums_in_cart)).to eq Purchase.albums_in_cart(@person)
      expect(assigns(:show_actions)).to eq true
      expect(assigns(:cart_is_empty)).to eq true
    end

  end

  describe "confirm_and_pay" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
    end

    it "should set up needed variables" do
      mock_card1 = double(StoredCreditCard)
      mock_card2 = double(StoredCreditCard)
      paypal     = double(StoredPaypalAccount)

      allow(@person).to receive_message_chain(:stored_credit_cards, :active).and_return([mock_card1, mock_card2])
      expect(StoredPaypalAccount).to receive(:currently_active_for_person).and_return(paypal)
      controller.instance_eval { @purchases = [] }

      get :confirm_and_pay

      expect(assigns[:credit_cards].size).to eq(2)
      expect(assigns[:paypal_account]).to eq(paypal)
    end
  end

  describe "#skip_confirm_and_pay" do
    let!(:person) { create(:person) }
    let!(:album) { create(:album, person: person) }

    before do
      create(:person_plan, person: person)
      login_as(person)
    end

    it "completes purchase" do
      credit_usage = CreditUsage.for(person, album)
      purchase = create(:purchase,
                        :free_with_plan,
                        :unpaid,
                        person: person,
                        related_id: credit_usage.id)

      expect(controller).to receive(:successful_purchase_after_effects)
      post :skip_confirm_and_pay
    end
  end

  describe "#show_payment_methods?" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return true
    end


    it "should show methods" do
      album = create(:album)
      allow(album).to receive(:strictly_facebook_release?).and_return true
      purchase = double(Purchase, related: album, related_type: Product::ALBUM)
      cart_data = Struct.new(:purchases)

      controller.instance_eval { @cart_data = cart_data.new([purchase]) }
      expected = controller.instance_eval { show_payment_methods? }

      expect(expected).to eq true
    end

    it "should NOT show methods" do
      album1 = create(:album)
      allow(album1).to receive(:strictly_facebook_release?).and_return true
      album2 = create(:album)
      allow(album2).to receive(:strictly_facebook_release?).and_return false
      purchase1 = double(Purchase, related: album1, related_type: Product::ALBUM)
      purchase2 = double(Purchase, related: album2, related_type: Product::ALBUM)
      finalize_form = double(CartFinalizeForm, use_balance: true)
      cart_data = Struct.new(:purchases)

      controller.instance_eval { @cart_data = cart_data.new([purchase1, purchase2]) }
      controller.instance_eval { @can_pay_with_balance = true }
      controller.instance_eval { @finalize_form = finalize_form }
      expected = controller.instance_eval { show_payment_methods? }

      expect(expected).to eq false
    end
  end

  describe "#show_payment_method_required" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return true
    end

    it "should show message" do
      album = double(Album, strictly_facebook_release?: true)
      purchase = double(Purchase, related: album, related_type: Product::ALBUM)

      controller.instance_eval { @purchases = [purchase] }
      expected = controller.instance_eval { show_payment_method_required? }

      expect(expected).to eq true
    end

    it "should NOT show message" do
      album1 = double(Album, strictly_facebook_release?: true)
      album2 = double(Album, strictly_facebook_release?: false)
      purchase1 = double(Purchase, related: album1, related_type: Product::ALBUM)
      purchase2 = double(Purchase, related: album2, related_type: Product::ALBUM)

      controller.instance_eval { @purchases = [purchase1, purchase2] }
      expected = controller.instance_eval { show_payment_method_required? }

      expect(expected).to eq false
    end
  end

  describe "#finalize" do
    let(:person) { create(:person, :with_unpaid_purchases) }
    let(:invoice)  { create(:invoice, person: person) }

    before(:each) do
      login_as(person)
      allow(FeatureFlipper)
        .to receive(:show_feature?)
        .and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:sift, any_args).and_return(false)
    end

    it "redirects to success if the payment was processed" do
      cart_form = double(save: true, invoice: invoice, purchased_before?: false, otp_redirect_url: nil, person: person)
      allow(CartFinalizeForm).to receive(:new).and_return(cart_form)
      expect(SuccessfulPurchaseWorker).to receive(:perform_async).with(invoice.id)
      expect(PersonPlansPurchaseService).to receive(:process).with(invoice)
      allow_any_instance_of(ComplianceInfoForm).to receive(:save).and_return([])

      post :finalize, params: { person: { first_name: "", last_name: "" } }

      expect(response).to redirect_to thanks_invoice_path(invoice, f: "first")
      expect(flash[:error_dialog]).to eq nil
    end

    it "redirects to success with f: first params if the payment was processed" do
      cart_form = double(save: true, invoice: invoice, purchased_before?: true, otp_redirect_url: nil, person: person)
      allow(CartFinalizeForm).to receive(:new).and_return(cart_form)
      allow_any_instance_of(ComplianceInfoForm).to receive(:save).and_return([])
      expect(SuccessfulPurchaseWorker).to receive(:perform_async).with(invoice.id)
      expect(PersonPlansPurchaseService).to receive(:process).with(invoice)

      post :finalize, params: { person: { first_name: "", last_name: "" } }

      expect(response).to redirect_to thanks_invoice_path(invoice)
      expect(flash[:error_dialog]).to eq nil
    end

    it "redirects to otp url when otp is needed" do
      otp_redirect_url = "http://test.in"
      cart_form = double(save: true, otp_redirect_url: otp_redirect_url, person: person)
      allow(CartFinalizeForm).to receive(:new).and_return(cart_form)
      allow_any_instance_of(ComplianceInfoForm).to receive(:save).and_return([])
      post :finalize, params: { person: { first_name: "", last_name: "" } }

      expect(response).to redirect_to otp_redirect_url
      expect(flash[:error_dialog]).to eq nil
    end

    it "redirects to failure if the payment was not processed" do
      fake_manager = double(reauthorize: true)
      cart_form = double(
        person: person,
        save: false,
        invoice: invoice,
        purchased_before?: true,
        errors: double(full_messages: ["bad error"]),
        manager: fake_manager,
        use_balance: true)
      allow(CartFinalizeForm).to receive(:new).and_return(cart_form)
      allow_any_instance_of(ComplianceInfoForm).to receive(:save).and_return([])

      post :finalize, params: { person: { first_name: "", last_name: "" } }

      expect(response).to redirect_to confirm_and_pay_cart_path
      expect(flash[:error_dialog]).to eq "bad error"
    end
  end

  describe "#finalize_after_otp" do
    let(:person) { create(:person, :with_unpaid_purchases) }

    before(:each) do
      login_as(person)
    end

    it "should handle successful otp confirmation" do
      transaction = create(:payments_os_transaction, person: person, payment_id: 123, charge_id: 456)
      expect(SuccessfulPurchaseWorker).to receive(:perform_async).with(transaction.invoice.id)
      expect(PersonPlansPurchaseService).to receive(:process).with(transaction.invoice)

      get :finalize_after_otp, params: {payment_id: 123, charge_id: 456, status: 'succeed'}

      expect(response).to redirect_to thanks_invoice_path(transaction.invoice, f: "first")
    end

    it "should handle failed otp confirmation" do
      transaction = create(:payments_os_transaction, person: person, payment_id: 123, charge_id: 456)
      expect(SuccessfulPurchaseWorker).not_to receive(:perform_async).with(transaction.invoice.id)
      expect(PersonPlansPurchaseService).not_to receive(:process).with(transaction.invoice)


      get :finalize_after_otp, params: {payment_id: 123, charge_id: 456, status: 'Failed'}

      expect(response).to redirect_to confirm_and_pay_cart_path
    end
  end

  describe "#finalize_after_redirect" do
    let(:purchase) { create(:purchase, :indian_product) }
    let(:person) { create(:person, :with_india_country_website) }
    let(:payu_txn) { create(:payu_transaction, person: person ) }
    let(:invoice) {
      payu_txn.invoice }
    let!(:not_fetched_response) { Faraday::Response.new(
      status: "200",
      body: Oj.dump(
        "status": 0,
        "msg": "0 out of 1 Transactions Fetched Successfully",
        "transaction_details": {
          "7fa6c4783a363b3da57": {
                "mihpayid": "Not Found",
                "status": "Not Found"
          }
        }
      )
    )}
    let!(:failure_response) { Faraday::Response.new(
      status: "200",
      body: Oj.dump(
        "status": 1,
        "msg": "1 out of 1 Transactions Fetched Successfully",
        "transaction_details": {
              "7fa6c4783a363b3da573": {
                    "mihpayid": "403993715521889530",
                    "txnid": "7fa6c4783a363b3da573",
                    "status": "failed"
              }
        }
      )
    )}
    let!(:success_response) { Faraday::Response.new(
      status: "200",
      body: Oj.dump(
        "status": 1,
        "msg": "1 out of 1 Transactions Fetched Successfully",
        "transaction_details": {
              "7fa6c4783a363b3da573": {
                    "mihpayid": "403993715521889530",
                    "txnid": "7fa6c4783a363b3da573",
                    "status": "success"
              }
        }
      )
    )}

    before(:each) do
      login_as(person)
    end

    it "should handle successful txn" do
      allow_any_instance_of(Payu::ApiClient).to receive(:send_request).and_return(Payu::Responses::VerifyPayment.new(success_response))

      expect(controller).to receive(:successful_purchase_after_effects)
      expect(controller).to_not receive(:failed_purchase_after_effects_for_hosted_checkout)

      get :finalize_after_redirect, params: { invoice_id: invoice.id }

      
      expect(response).to redirect_to thanks_invoice_path(invoice, { f: "first" })
    end

    it "should handle failed txn" do
      allow_any_instance_of(Payu::ApiClient).to receive(:send_request).and_return(Payu::Responses::VerifyPayment.new(failure_response))

      expect(controller).to_not receive(:successful_purchase_after_effects)
      expect(controller).to receive(:failed_purchase_after_effects_for_hosted_checkout)

      get :finalize_after_redirect, params: {invoice_id: invoice.id}

      expect(response).to redirect_to cart_path(invoice_id: invoice.id, failed: "true")
    end

    it "should handle non-fetched response" do
      allow_any_instance_of(Payu::ApiClient).to receive(:send_request).and_return(Payu::Responses::VerifyPayment.new(not_fetched_response))

      expect(controller).to_not receive(:successful_purchase_after_effects)
      expect(controller).to receive(:failed_purchase_after_effects_for_hosted_checkout)

      get :finalize_after_redirect, params: {invoice_id: invoice.id}

      expect(response).to redirect_to cart_path(invoice_id: invoice.id, failed: "true")
    end
  end

  describe "#adyen_finalize_after_redirect" do
    let(:purchase) { create(:purchase) }
    let(:person) { create(:person) }
    let(:adyen_transaction) { create(:adyen_transaction, person: person, country: Country.find_by(iso_code: "PH") ) }
    let(:invoice) { adyen_transaction.invoice }
    let(:cable_auth_token) { create(:cable_auth_token, person: person)}
    let!(:adyen_merchant_config) { create(:adyen_merchant_config) }
    let!(:success_response) { Faraday::Response.new(
      body: {
        "pspReference": "NC6HT9CRT65ZGN82",
        "resultCode": "Authorised",
        "merchantReference": "430"
      }.to_json
    )}
    let!(:failure_response) {  Faraday::Response.new(
      body: {
        "pspReference": "NC6HT9CRT65ZGN82",
        "resultCode": "Refused",
        "merchantReference": "430"
      }.to_json
    )}

    before(:each) do
      login_as(person)
    end

    it "should handle successful txn" do
      allow_any_instance_of(Person)
      .to receive(:cable_auth_token)
      .and_return(cable_auth_token)
      allow_any_instance_of(Adyen::PaymentsApiClient)
      .to receive(:send_request)
      .and_return(Adyen::Responses::PaymentDetail.new(success_response))

      expect(controller).to receive(:successful_purchase_after_effects)
      expect(controller).to_not receive(:failed_purchase_after_effects_for_hosted_checkout)

      get :adyen_finalize_after_redirect, params: { redirectResult: "ABC", invoice_id: invoice.id }

      expect(response).to redirect_to thanks_invoice_path(invoice, { f: "first" })
    end

    it "should handle failed txn" do
      allow_any_instance_of(Adyen::PaymentsApiClient)
      .to receive(:send_request)
      .and_return(Adyen::Responses::PaymentDetail.new(failure_response))

      expect(controller).to_not receive(:successful_purchase_after_effects)
      expect(controller).to receive(:failed_purchase_after_effects_for_hosted_checkout)

      get :adyen_finalize_after_redirect, params: { redirectResult: "ABC", invoice_id: invoice.id }

      expect(response).to redirect_to cart_path(invoice_id: invoice.id, failed: "true")
    end
  end

  describe "#set_plan_sufficiency" do
    let(:person) { create(:person) }

    before do
      hoisted_person = person
      controller.instance_eval { @person = hoisted_person }
    end

    context "sufficient current plan" do
      it "returns true" do
        allow(Plans::SufficientPlanService).to receive(:call).and_return true

        expect(controller.instance_eval { set_plan_sufficiency }).to be true
      end
    end

    context "sufficient carted plan" do
      it "returns true" do
        allow(Plans::SufficientPlanService).to receive(:call).and_return false
        allow(Plans::SufficientPlanService).to receive(:call).and_return true

        expect(controller.instance_eval { set_plan_sufficiency }).to be true
      end
    end

    context "legacy user, no carted plan" do
      it "returns true" do
        allow(Plans::SufficientPlanService).to receive(:call).exactly(2).times.and_return false

        expect(controller.instance_eval { set_plan_sufficiency }).to be true
      end
    end

    context "legacy user, insufficient carted plan" do
      it "returns true" do
        allow(Plans::SufficientPlanService).to receive(:call).exactly(2).times.and_return false
        allow(person).to receive(:cart_plan).and_return(Plan.first)

        expect(controller.instance_eval { set_plan_sufficiency }).to be false
      end
    end
  end

  describe "setup_cart_variables" do
    let(:person) { create(:person) }
    let(:r1) { double(Product) }
    let(:r2) { double(Product) }
    let(:p1) { double(Purchase) }
    let(:p2) { double(Purchase) }
    let(:p3) { double(Purchase) }
    let(:manager_service) { double(Cart::Purchases::Manager) }

    before do
      allow(controller).to(receive(:remove_duplicate_plans)).and_return nil

      allow(p1).to receive(:person).and_return(person)
      allow(p3).to receive(:person).and_return(person)

      allow(Cart::Purchases::Manager)
        .to receive(:new)
        .and_return(manager_service)
    end

    subject do
      # this is necessary due to the way instance_eval interacts with let blocks
      hoisted_person = person
      controller.instance_eval { @person = hoisted_person }
      controller.instance_eval { setup_cart_variables }
    end

    it "invokes remove_duplicate_plans" do
      expect(controller).to receive(:remove_duplicate_plans)

      subject
    end

    it "invokes remove_deleted_albums" do
      expect(controller).to receive(:remove_deleted_albums)

      subject
    end

    it "invokes remove_undistributable_purchases" do
      expect(controller).to receive(:remove_undistributable_purchases)

      subject
    end

    it "invokes remove_orphaned_purchases" do
      expect(controller).to receive(:remove_orphaned_purchases)

      subject
    end

    it "invokes recalculate_dolby_atmos_purchases" do
      expect(controller).to receive(:recalculate_dolby_atmos_purchases)

      subject
    end

    it "should set up needed variables" do
      allow(controller)
        .to receive_message_chain(:purchase_items, :includes)
        .and_return([p1, p2, p3])

      allow(controller)
        .to receive_message_chain(:purchase_items, :find)
        .and_return(nil)

      allow(controller).to receive(:remove_undistributable_purchases)
      allow(controller).to receive(:remove_orphaned_purchases)
      allow(controller).to receive(:remove_deleted_albums)

      expect(Product)
        .to receive(:related_products_for_purchases)
        .and_return([r1, r2])

      expect(Cart::Purchases::Manager)
        .to receive(:new)
        .with(person, [p1, p2, p3], [r1, r2])

      subject

      expect(assigns[:cart_data]).to eq(manager_service)
      expect(assigns[:purchases].size).to eq(3)
    end
  end

  describe "remove_duplicate_plans" do
    let(:person) { create(:person, country: "US") }

    context "when there are no plans in `purchase_items`" do
      let!(:purchase_items) { [] }

      before do
        allow(controller).to(receive(:purchase_items)).and_return(purchase_items)
      end

      it "should not destroy any purchase" do
        expect(Purchase.where(id: purchase_items).count).to(eq(0))
        controller.send(:remove_duplicate_plans)
        expect(Purchase.where(id: purchase_items).count).to(eq(0))
      end
    end

    context "when there is one plan in `purchase_items`" do
      let(:product) { Product.find 500 }
      let!(:purchase_items) {
        [
          create(
            :purchase,
            cost_cents: product.price * 100,
            person: person,
            product_id: product.id,
            related: product,
            paid_at: ""
          )
        ]
      }
  
      before do
        allow(controller).to(receive(:purchase_items)).and_return(purchase_items)
      end

      it "should not destroy any purchases" do
        expect(Purchase.where(id: purchase_items).count).to(eq(1))
        controller.send(:remove_duplicate_plans)
        expect(Purchase.where(id: purchase_items).count).to(eq(1))
      end
    end

    context "when there are multiple plans in `purchase_items`" do
      let!(:purchase_items) {
        Product.where(id: [498, 499, 500]).map do |product|
          create(
            :purchase,
            cost_cents: product.price * 100,
            person: person,
            product_id: product.id,
            related: product,
            paid_at: ""
          )
        end
      }
  
      before do
        allow(controller).to(receive(:purchase_items)).and_return(purchase_items)
      end

      it "should keep the highest priced plan, and destroy all others" do
        expect(Purchase.where(id: purchase_items).count).to(eq(3))
        controller.send(:remove_duplicate_plans)
        expect(Purchase.where(id: purchase_items).count).to(eq(1))
        expect(Purchase.last.product).to(eq(Product.find(500)))
      end
    end
  end

  describe "#recalculate_additional_artists_for_plan" do
    let(:person) { create(:person, country: "US") }

    before do
      allow(Plans::AdditionalArtistService).to receive(:adjust_additional_artists)
      allow(Plans::AdditionalArtistService).to receive(:clear_unpaid_additional_artist_purchases!)
      allow(Plans::RenewAdditionalArtistsService).to receive(:additional_artist_purchases!).and_return([])
      allow(Plans::CancelAdditionalArtistService).to receive(:cancel_unrenewed_artists)
    end

    context "plan user" do
      
      before do
        allow(Plan).to receive(:enabled?).and_return(true)
      end
      
      context "who has a pro plan renewal in the cart" do
        let(:purchase){instance_double("Purchase")}

        it "calls various services" do
          allow(purchase).to receive(:related)
          allow(purchase).to receive(:product)
          allow(controller).to receive(:carted_manual_pro_plan_renewal_purchase).and_return(purchase)
          
          expect(Plans::AdditionalArtistService).to receive(:clear_unpaid_additional_artist_purchases!)
          expect(Plans::RenewAdditionalArtistsService).to receive(:additional_artist_purchases!)
          expect(Plans::CancelAdditionalArtistService).to receive(:cancel_unrenewed_artists)
          expect(Plans::AdditionalArtistService).to receive(:adjust_additional_artists)
          controller.send(:recalculate_additional_artists_for_plan)
        end
      end

      context "who does not have a pro plan renewal in the cart" do

        before do
          allow(controller).to receive(:carted_manual_pro_plan_renewal_purchase)
        end

        it "calls only Plans::AdditionalArtistService.adjust_additional_artists " do
          expect(Plans::AdditionalArtistService).to_not receive(:clear_unpaid_additional_artist_purchases!)
          expect(Plans::RenewAdditionalArtistsService).to_not receive(:additional_artist_purchases!)
          expect(Plans::CancelAdditionalArtistService).to_not receive(:cancel_unrenewed_artists)
          expect(Plans::AdditionalArtistService).to receive(:adjust_additional_artists)
          controller.send(:recalculate_additional_artists_for_plan)
        end
      end
    end

    context "non-plan user" do
      
      before do
        allow(Plan).to receive(:enabled?).and_return(false)
        allow(controller).to receive(:carted_manual_pro_plan_renewal_purchase)        
      end

      it "does not call any related services" do
        expect(Plans::AdditionalArtistService).to_not receive(:clear_unpaid_additional_artist_purchases!)
        expect(Plans::RenewAdditionalArtistsService).to_not receive(:additional_artist_purchases!)
        expect(Plans::CancelAdditionalArtistService).to_not receive(:cancel_unrenewed_artists)
        expect(Plans::AdditionalArtistService).to_not receive(:adjust_additional_artists)
        controller.send(:recalculate_additional_artists_for_plan)
      end
    end
    
  end

  describe "#carted_manual_pro_plan_renewal_purchase" do
    
    let!(:person) { create(:person, country: "US") }
    let(:pro_plan_product) { Product.find(501) }
    let(:other_plan_product) { Product.find(499) }
    subject { controller.send(:carted_manual_pro_plan_renewal_purchase) }
    before { controller.instance_variable_set(:@person, person) }

    context "when there is a pro plan renewal purchase in the cart" do

      before do
        plan_purchase = create(:purchase,person:person, product: pro_plan_product,related: pro_plan_product)
        PersonPlanCreationService.call(
          person_id: person.id,
          plan_id: 4,
          purchase_id: plan_purchase.id
        )
        renewal = person.person_plan.renewals.first
        Product.add_to_cart(person, renewal, pro_plan_product)
      end

      it "should return the associated purchase" do     
        expect(subject).to_not be(nil)
        expect(subject.class).to be(Purchase)
        expect(subject).to respond_to(:product)
        expect(subject.product).to eq(pro_plan_product)     
      end

    end

    context "when there is a different plan renewal purchase in the cart" do
      
      before do
        plan_purchase = create(:purchase, person:person, product: other_plan_product,related: other_plan_product)
        PersonPlanCreationService.call(
          person_id: person.id,
          plan_id: 2,
          purchase_id: plan_purchase.id
        )
        renewal = person.person_plan.renewals.first
        Product.add_to_cart(person, renewal, other_plan_product)
      end

      it "should return nil" do
        expect(subject).to be(nil)
      end
    end

    context "when there is a original pro plan purchase in the cart" do
      before { PlanService.adjust_cart_plan!(person, 4) }

      it "should return nil" do
        expect(subject).to be(nil)
      end
    end

  end

  describe "remove_undistributable_purchases" do

    it "should remove purchases that are not distributable" do
      p1 = double(Purchase)
      allow(p1).to receive_message_chain(:related, :can_distribute?).and_return(false)
      allow(p1).to receive_message_chain(:related, :name).and_return("test 1")
      expect(p1).to receive(:destroy)

      p2 = double(Purchase)
      allow(p2).to receive_message_chain(:related, :can_distribute?).and_return(true)
      expect(p2).not_to receive(:destroy)

      p3 = double(Purchase)
      allow(p3).to receive_message_chain(:related, :can_distribute?).and_return(false)
      allow(p3).to receive_message_chain(:related, :name).and_return("test 1")
      expect(p3).to receive(:destroy)

      controller.instance_eval { @purchases = [p1, p2, p3] }
      controller.instance_eval { remove_undistributable_purchases }

      # verify
      expect(assigns[:purchases].size).to eq(1)
      expect(assigns[:purchases].first).to eq(p2)
    end
  end

  describe "remove_orphaned_purchases" do

    it "should remove purchases where related is nil" do
      p1 = double(Purchase)
      allow(p1).to receive_message_chain(:related, :nil?).and_return(true)
      expect(p1).to receive(:destroy)

      p2 = double(Purchase)
      allow(p2).to receive_message_chain(:related, :nil?).and_return(false)

      p3 = double(Purchase)
      allow(p3).to receive_message_chain(:related, :nil?).and_return(true)
      expect(p3).to receive(:destroy)

      controller.instance_eval { @purchases = [p1, p2, p3] }
      controller.instance_eval { remove_orphaned_purchases }

      # verify
      controller.instance_eval { @purchases.size == 1 }
      expect(assigns[:purchases].first).to eq(p2)
    end

  end

  describe "#recalculate_dolby_atmos_purchases" do
    let!(:person) { create(:person) }
    before(:each) do
      3.times do
        album = create(:album, person: person)
        3.times do |n|
          song = create(:song, :with_s3_asset, album: album, name: "Dobby Atlas Train #{n}")
          create(:spatial_audio, song: song)
        end
        product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
        create(:purchase, :unpaid, person: person, related: album, product: product)
      end
    end
    it "calls Album::DolbyAtmosCartService for albums in the cart" do
      hoisted_person = person
      expect(Album::DolbyAtmosCartService).to receive(:call).exactly(3).times
      controller.instance_eval { @person = hoisted_person }
      controller.instance_eval { recalculate_dolby_atmos_purchases }
    end
  end

  # #ensure_valid_purchases is a side-effect of #purchase_items
  describe "#ensure_valid_purchases" do
    let!(:person)  { create(:person) }
    let!(:product) { create(:product) }
    let!(:product_item) do
       create(:product_item,
              product: product,
              renewal_interval: "year",
              renewal_duration: 1)
    end

    before do
      login_as(person)
      hoisted_person = person
      controller.instance_eval { @person = hoisted_person }

      2.times do
        album = create(:album, person: person)
        album_purchase = create(:purchase, person: person, related: album)
        renewal = create(:renewal,
          person: person,
          item_to_renew: product_item,
          purchase: album_purchase)
        create(:renewal_item, renewal: renewal, related: album)
        create(:purchase,
          :unpaid,
          person: person,
          product: product,
          related: renewal)
      end

      create(:purchase, :unpaid, person: person)
    end

    context "plan user" do
      it "removes and destroys only release renewal purchases" do
        allow_any_instance_of(Person).to receive(:owns_or_carted_plan?).and_return(true)

        expect(controller.instance_eval { purchase_items.size }).to eq 1
      end
    end

    context "non-plan user" do
      it "removes no renewal purchases" do
        expect(controller.instance_eval { purchase_items.size }).to eq 3
      end
    end
  end

  describe "manage_invoices" do

    it "should destroy invoices that don't have any payments against them" do
      invoice1 = double(Invoice)
      allow(invoice1).to receive(:invoice_settlements).and_return([])
      expect(invoice1).to receive(:can_destroy?).and_return(true)
      expect(invoice1).to receive(:credit_and_remove_settlements!)
      expect(invoice1).to receive(:reload)
      expect(invoice1).to receive(:destroy)
      expect(invoice1).to receive(:id).and_return(1)

      invoice2 = double(Invoice)
      allow(invoice2).to receive(:invoice_settlements).and_return([])
      allow(invoice2).to receive(:can_destroy?).and_return(false)
      expect(invoice2).not_to receive(:destroy)

      person = double(Person)
      allow(person).to receive_message_chain(:invoices, :includes, :where).and_return([invoice1, invoice2])
      expect(person).to receive(:id).and_return(1)

      controller.instance_eval { @person = person }
      controller.instance_eval { manage_invoices }
    end

    it "should destroy invoices if they have only settlement types of PersonTransaction" do
      invoice1 = double(Invoice)
      allow(invoice1).to receive(:invoice_settlements).and_return([])
      expect(invoice1).to receive(:can_destroy?).and_return(false)
      expect(invoice1).not_to receive(:destroy)

      settlement1 = double(InvoiceSettlement)
      allow(settlement1).to receive(:source_type).and_return('PersonTransaction')

      settlement2 = double(InvoiceSettlement)
      allow(settlement2).to receive(:source_type).and_return('PersonTransaction')

      invoice2 = double(Invoice)
      allow(invoice2).to receive(:invoice_settlements).and_return([settlement1, settlement2])
      expect(invoice2).to receive(:can_destroy?).and_return(true)
      expect(invoice2).to receive(:credit_and_remove_settlements!)
      expect(invoice2).to receive(:reload)
      expect(invoice2).to receive(:destroy)
      expect(invoice2).to receive(:id).and_return(2)

			settlement3 = double(InvoiceSettlement)
			allow(settlement3).to receive(:source_type).and_return('PersonTransaction')

			settlement4 = double(InvoiceSettlement)
			allow(settlement4).to receive(:source_type).and_return('BraintreeTransaction')

			invoice3 = double(Invoice)
			allow(invoice3).to receive(:invoice_settlements).and_return([settlement3, settlement4])
			expect(invoice3).not_to receive(:can_destroy?)
			expect(invoice3).not_to receive(:destroy)

			person = double(Person)
      allow(person).to receive_message_chain(:invoices, :includes, :where).and_return([invoice1, invoice2, invoice3])
			expect(person).to receive(:id).and_return(1)

			controller.instance_eval { @person = person }
			controller.instance_eval { manage_invoices }
		end
	end

  describe "#adyen_payment_methods" do
    it "should render adyen payment methods" do
      @person = create(:person)
      login_as(@person)
      allow_any_instance_of(Adyen::PaymentMethodService)
        .to receive(:payment_methods)
        .and_return([
                      {
                        brands: ["cup"],
                        name: "Credit Card",
                        type: "scheme"
                      },
                      { name: "GCash", type: "gcash" },
                      { name: "Maya Wallet", type: "paymaya_wallet" }
                    ])
      get "adyen_payment_methods", params: { adyen_payment_methods: { country_code: "PH" } }
      expect(JSON.parse(response.body)).to eq(
        {
          "payment_methods" => [
            {
              "brands" => ["cup"],
              "name" => "Credit Card",
              "type" => "scheme"
            },
            { "name" => "GCash", "type" => "gcash" },
            { "name" => "Maya Wallet", "type" => "paymaya_wallet" }
          ]
        }
      )
    end
  end

  describe "#dropin" do
    let!(:adyen_merchant_config) { create(:adyen_merchant_config) }
    it "should initialize a new adyen dropin session" do
      @person = create(:person, country:"PH")
      invoice = create(:invoice, person_id: @person.id)
      login_as(@person)
      allow_any_instance_of(Adyen::NewSessionService)
        .to receive(:details)
        .and_return({session_id: "CS006DF69F60D07B16",session_data: "aqrtuxNl+IrfeY/2cRQFi/mP7pIsMCGiNZ7KEIeixdOer5XukUAu8io6uRtvaGflc8TRP5kjIXws5MbhW27+"})
      allow_any_instance_of(Adyen::NewSessionService)
        .to receive(:update_adyen_transaction)
        .and_return(true)
      create(:foreign_exchange_rate, exchange_rate: 54.0, valid_from: Time.now - 3, source_currency: 'USD', target_currency: 'PHP')
      get "dropin", params: { invoice_id: invoice.id,payment_method:"gopay_wallet" }
      expect(@controller.view_assigns["session"]).to include({:id =>"CS006DF69F60D07B16",:session_data =>"aqrtuxNl+IrfeY/2cRQFi/mP7pIsMCGiNZ7KEIeixdOer5XukUAu8io6uRtvaGflc8TRP5kjIXws5MbhW27+"})
    end

    it "should redirect to confirm_and_pay if adyen api fails" do
      @person = create(:person, country: "PH")
      invoice = create(:invoice, person_id: @person.id)
      login_as(@person)
      allow_any_instance_of(Adyen::PaymentsApiClient)
        .to receive(:send_request)
        .and_return(Adyen::Responses::NewSession.new(
                      Faraday::Response.new(
                        body: {
                          "status" => 422,
                          "errorCode" => "14_0408"
                        }.to_json
                      )
                    ))
      create(:foreign_exchange_rate, exchange_rate: 54.0, valid_from: Time.now - 3, source_currency: 'USD', target_currency: 'PHP')
      get "dropin", params: { invoice_id: invoice.id, payment_method: "gopay_wallet" }
      expect(flash[:error_dialog]).to eq custom_t("controllers.carts.issue_placing_your_order")
      expect(subject).to redirect_to confirm_and_pay_cart_path
    end
  end
end
