require "rails_helper"

describe TwoFactorAuth::AuthenticationsController do
  let(:person) { create(:person, :with_two_factor_auth) }

  before(:example) do
    allow(controller)
      .to receive(:current_user)
      .and_return(person)
  end

  describe "#show" do
    it "renders show for current action" do
      fake_auth_form = double(
        is_complete?: false,
        tab: "account_settings",
        page: "account_settings",
        on_authentication_action?: false
      )
      allow(TwoFactorAuth::AuthenticationForm)
        .to receive(:new)
        .and_return(fake_auth_form)

      params = { two_factor_auth_authentication_form: { tab: "account_settings"} }
      get :show, params: params

      expect(response).to render_template(:show)
    end

    it "redirects to page user wanted to access if completed" do
      fake_auth_form = double(
        is_complete?: true,
        page: "account_settings",
        on_authentication_action?: false
      )
      allow(TwoFactorAuth::AuthenticationForm)
        .to receive(:new)
        .and_return(fake_auth_form)

      params = { two_factor_auth_authentication_form: { foo: "bar" } }
      get :show, params: params

      expect(response).to redirect_to(edit_person_path(person))
    end
  end

  describe "#create" do
    it "renders sign_in if @authentication.valid? is false" do
      fake_auth_form = double(
        valid?: false,
        tab: "account_settings",
        page: "account_settings",
        next_action: "authentication",
        success_callbacks: [],
        errors: []
      )
      allow(TwoFactorAuth::AuthenticationForm)
        .to receive(:new)
        .and_return(fake_auth_form)

      params = { two_factor_auth_authentication_form: { tab: "account_settings"} }
      post :create, params: params

      expect(response)
        .to redirect_to(
          two_factor_auth_authentications_path(
            two_factor_auth_authentication_form: {
              page: fake_auth_form.page,
              tfa_action: 'sign_in',
              success_callbacks: fake_auth_form.success_callbacks
            }
          )
        )
    end

    it "renders next action if @authentication.perform is true" do
      fake_auth_form = double(
        valid?: true,
        perform: true,
        tab: "account_settings",
        page: "account_settings",
        next_action: "authentication",
        success_callbacks: [],
        errors: []
      )
      allow(TwoFactorAuth::AuthenticationForm)
        .to receive(:new)
        .and_return(fake_auth_form)

      params = { two_factor_auth_authentication_form: { tab: "account_settings"} }
      post :create, params: params

      next_action_params = {
        two_factor_auth_authentication_form: {
          page: fake_auth_form.page,
          tfa_action: fake_auth_form.next_action,
          success_callbacks: fake_auth_form.success_callbacks
        }
      }

      expect(response)
        .to redirect_to(
          two_factor_auth_authentications_path(next_action_params)
        )
    end
  end
end
