require "rails_helper"

describe TwoFactorAuth::PreferencesController do
  let(:two_factor_auth) { create(:two_factor_auth, last_verified_at: Time.now - 1.minute) }
  let(:person) { two_factor_auth.person }
  let(:form_class) { TwoFactorAuth::PreferenceForm }
  let(:complete_preferences_double) {
      double(
        is_complete?: true,
        tfa_action: 'confirmation',
        save_previous_state: true,
        on_authentication_action?: false
      )
    }

  let(:in_progress_preferences_double) {
      double(
        is_complete?: false,
        tfa_action: 'preferences',
        valid?: true,
        perform: true,
        next_action: 'authentication',
        errors: [],
        save_previous_state: true,
        on_authentication_action?: false
      )
    }
  before(:each) do
    allow(controller).to receive(:current_user).and_return(person)
    allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
    allow(FeatureFlipper).to receive(:show_feature?).with(:two_factor_auth_prompt, person).and_return(true)

    allow(form_class).to receive(:new).and_return(in_progress_preferences_double)
  end

  describe "#destroy" do
    let(:tfa_events) { two_factor_auth.two_factor_auth_events }
    let (:abandoning_preferences_double) {
      double(terminate: true, restore_previous_state: true)
    }

    before(:each) do
      allow(form_class).to receive(:new).and_return(abandoning_preferences_double)
    end

    it "redirects to the edit person page" do
      delete :destroy
      expect(response).to redirect_to(edit_person_path(person))
    end
  end

  describe "#show" do
    context "user has recently verified with two factor auth" do
      before(:each) do
        Timecop.freeze
        allow(two_factor_auth).to receive(:active?).and_return(true)
        allow(two_factor_auth).to receive(:within_authentication_window?).and_return(true)
      end

      after(:each) do
        Timecop.return
      end

      it "renders show if two_factor_auth verification is valid" do
        get :show
        expect(response).to render_template(:show)
      end

      it "redirects to edit_person if completed" do
        allow(form_class).to receive(:new).and_return(complete_preferences_double)
        params = { two_factor_auth_authentication_form: { tfa_action: 'confirmation' } }
        get :show, params: params

        expect(response).to redirect_to(edit_person_path(person))
      end
    end

  end

  describe "#create" do
    context "user is not authorized" do
      before(:each) do
        t = Time.now + 10.minutes
        Timecop.freeze

        Timecop.travel(t)
      end

      after(:each) do
        Timecop.return
      end

      it "redirects if the user needs to verify two_factor_auth" do
        allow(person).to receive(:two_factor_auth).and_return(two_factor_auth)
        allow(two_factor_auth).to receive(:active?).and_return(true)
        allow(two_factor_auth).to receive(:within_authentication_window?).and_return(false)

        post :create

        authentication_path = two_factor_auth_authentications_path(
          two_factor_auth_authentication_form: {
            page: 'change_preferences', tfa_action: "sign_in"
          }
        )
        expect(response).to redirect_to(authentication_path)
      end
    end

    context "user is authorized" do

      it "renders next action if @preference.perform is true" do
        allow(person).to receive(:two_factor_auth).and_return(two_factor_auth)
        allow(two_factor_auth).to receive(:active?).and_return(true)
        allow(two_factor_auth).to receive(:within_authentication_window?).and_return(true)
        allow(two_factor_auth).to receive(:save_previous_preferences).and_return(true)
        allow(form_class).to receive(:new).and_return(in_progress_preferences_double)

        post :create

        redirect_path = two_factor_auth_preferences_path(
          two_factor_auth_preference_form: { tfa_action: 'authentication' }
        )

        expect(response).to redirect_to(redirect_path)
      end
    end
  end
end
