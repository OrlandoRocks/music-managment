require "rails_helper"

describe TwoFactorAuth::EnrollmentsController do

  before(:each) do
    @person = create(:person)
    login_as(@person)
    @form = TwoFactorAuth::EnrollmentForm.new(@person)
    allow(TwoFactorAuth::EnrollmentForm).to receive(:new).and_return(@form)
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
  end

  describe "#ensure_tfa_access before filter" do
    it "redirects back to dashboard_path if in admin takeover session" do
      session["admin"] = "12345"

      get :show

      expect(response).to redirect_to(dashboard_path)
    end
  end

  describe "GET #show" do
    context "going back a step" do
      it "calls #step_back on form service" do
        expect(@form).to receive(:step_back)

        get :show, params: { back_to_previous_step: "true" }
      end
    end

    context "when form is complete" do
      before do
        enrollment_form = double(:enrollment_form, on_authentication_action?: false, is_complete?: true, reset_steps: nil)

        allow(TwoFactorAuth::EnrollmentForm).to receive(:new).and_return(enrollment_form)
      end

      it "sends a enrollment mailer" do
        expect(TwoFactorAuth::EnrollmentMailerWorker).to receive(:perform_async).with(@person.id)

        get :show
      end

      it "redirects to the edit_person_path" do
        get :show

        expect(response).to redirect_to(edit_person_path(@person.id, tab: "account"))
      end
    end
  end

  describe "PUT #update" do

    it "submits params to form service" do
      params = { fake_param: 'bogus' }
      actioncontroller_params = ActionController::Parameters.new(params)
      expect(@form).to receive(:submit_step).with(actioncontroller_params)

      put :update, params: { tfa_form: params }
    end

    it "redirects to #show action" do
      put :update, params: { tfa_form: { fake_params: true }}

      expect(response).to redirect_to(two_factor_auth_enrollments_path)
    end
  end

  describe "DELETE #destroy (when 2fa has been enabled)" do
    let(:person) { create(:person, :with_two_factor_auth) }
    let(:two_factor_auth) { person.two_factor_auth }

    before(:each) do
      allow(controller).to receive(:current_user).and_return(person)
    end

    it "forces two factor authentication if deleting" do
      delete :destroy

      expect(response).to redirect_to(
        two_factor_auth_authentications_path(
          two_factor_auth_authentication_form: {
            page: "disable_2fa",
            tfa_action: 'sign_in',
            success_callbacks: [ :disable_two_factor_auth ]
          }
        )
      )
    end
  end

  describe "DELETE #destroy (when cancelled in the middle of enrollment)" do
    context "a person has a two factor auth object" do
      let(:two_factor_auth) { create(:two_factor_auth) }

      before(:each) do
        allow(controller).to receive(:current_user).and_return(two_factor_auth.person)
      end

      it "disables without invoking 2fa if it receives cancel params" do
        delete :destroy, params: { cancel: true }

        expect(response).to redirect_to(edit_person_path(two_factor_auth.person, tab: "account"))
      end
    end

    context "the person is missing a two factor auth object" do
      let(:person) { create(:person) }

      it "redirects to the edit person path without error" do
        allow(controller).to receive(:current_user).and_return(person)

        delete :destroy, params: { cancel: true }

        expect(response).to redirect_to(edit_person_path(person, tab: "account"))
      end
    end
  end
end
