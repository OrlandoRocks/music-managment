require "rails_helper"

describe TwoFactorAuth::SessionsController do
  describe "#new" do
    context "user does not hav valid 2fa credentials" do
      it "redirects to login if person doesn't have 2FA" do
        non_tfa_person = create(:person)
        get :new, params: { two_factor_auth_authentication_form: { email: non_tfa_person.email } }

        expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      end

      it "redirects to login if person doesnt exist" do
        get :new

        expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      end
    end

    context "user has an account with 2fa enabled" do
      let(:tfa_person) { create(:person, :with_two_factor_auth) }

      it "redirects to login if person account locked" do
        tfa_person.update(status: "Locked")
        get :new, params: { two_factor_auth_authentication_form: { email: tfa_person.email } }

        expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      end

      it "redirects to login if @auth_form is invalid" do
        get :new, params: { two_factor_auth_authentication_form: { email: tfa_person.email } }

        expect(response).to redirect_to login_path(ENV.fetch("LOGIN_DDOS_MITIGATION_PARAM", "check").to_sym => 1)
      end

      context "authentication is complete" do
        let(:fake_auth_form) {
          double(
            :auth_form,
            on_authentication_action?: false,
            is_complete?: true,
            valid?: true
          )
        }

        let(:token) { TwoFactorAuth::TokenService.issue({ email: tfa_person.email }) }

        before :each do
          allow(TwoFactorAuth::AuthenticationForm).to receive(:new).and_return(fake_auth_form)
          allow(controller).to receive(:current_user).and_return(tfa_person)

          allow(ENV).to receive(:[]).and_return(:default)
          allow(ENV).to receive(:[]).with("JWT_ENCRYPTION_KEY").and_return("123357890")
          allow(ENV).to receive(:[]).with("JWT_ALGORITHM").and_return("HS256")
        end

        it "calls sign_in_success if auth is complete" do
          expect(controller).to receive(:sign_in_success)

          get :new, params: { token: token, two_factor_auth_authentication_form: { email: tfa_person.email } }
        end

        it "redirects for taxes if the feature is enabled" do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
          allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_login_redirect, tfa_person).and_return(true)

          tfa_person.tax_tokens.create(token: SecureRandom.hex, is_visible: true)

          get :new, params: { token: token, two_factor_auth_authentication_form: { email: tfa_person.email } }, xhr: true

          expect(response).to redirect_to(edit_person_path(tfa_person, tab: "tax_info"))
        end
      end
    end
  end
end
