require "rails_helper"

describe PreviewArtistsController do
  describe "#show" do
    let(:person) { create(:person) }
    let(:artist) { create(:artist, name: "Chance the Rapper") }

    before do
      login_as(person)
    end

    it "renders a list of artists for a song" do
      album = create(:album, person: person, creatives: [
          { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { song: { name: "no problem", creatives: [
        { role: "primary_artist", name: "Lil Wayne"},
        { role: "primary_artist", name: "2 Chainz"},
      ] }, id: album.id }

      expect(response.body).to eq("Chance the Rapper, Lil Wayne, & 2 Chainz")
    end

    it "renders a list of artists for a new ringtone" do
      get :show, params: { ringtone: { album_name: "blessings", creatives: [
        { role: "primary_artist", name: artist.name }.with_indifferent_access
      ] }}
      expect(response.body).to eq("Chance the Rapper")
    end

    it "renders a list of artists for a saved ringtone" do
      ringtone = create(:ringtone, person: person, creatives: [
        { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { ringtone: { creatives: [
        { role: "primary_artist", name: artist.name },
        { role: "primary_artist", name: "Saba"}
      ] }, id: ringtone.id }

      expect(response.body).to eq("Chance the Rapper & Saba")
    end

    it "renders a list of artists for a new single" do
      get :show, params: { ringtone: { album_name: "no problem", creatives: [
        { role: "primary_artist", name: artist.name }
      ]}}
      expect(response.body).to eq("Chance the Rapper")
    end

    it "renders a list of artists for a saved single" do
      single = create(:single, person: person, creatives: [
        { role: "primary_artist", name: artist.name }.with_indifferent_access
      ])
      get :show, params: { ringtone: { album_name: "no problem", creatives: [
        { role: "primary_artist", name: artist.name }
      ] }, id: single.id }
      expect(response.body).to eq("Chance the Rapper")
    end
  end
end
