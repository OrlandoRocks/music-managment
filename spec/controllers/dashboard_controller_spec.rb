require "rails_helper"

describe DashboardController do
  let(:person) { create (:person) }

  before { login_as(person) }

  describe "#create_account" do
    describe "country redirects" do
      let(:country_redirect_map) do
        {
          "default" => mobile_announcement_path,
          "IN" => payoneer_fees_path
        }
      end

      COUNTRY_URLS.each do |country_code, country_url|
        context "the user hits from their mobile page" do

          it "sets the redirect_url to appropriate path" do
            request.host = country_url
            country_website = CountryWebsite.find_by(country: country_code)
            person = create(:person)
            person.country_website = country_website
            login_as(person)
            request.env['HTTP_USER_AGENT'] = "blackberry"
            session[:verification] = true

            get :create_account

            redirect_path = country_redirect_map[country_code] || country_redirect_map["default"]
            expect(controller.instance_variable_get(:@redirect_url))
              .to eq(redirect_path)
          end
        end
      end
    end

    describe "#account_created?" do
      before do
        allow(controller).to receive(:ensure_correct_country_domain).and_return true
      end

      it "redirects when creation cookie set" do
        cookies["account_created_#{person.id}"] = :foo

        get :create_account

        assert_redirected_to dashboard_path
      end

      it "sets account creation cookie when not set" do
        get :create_account

        expect(cookies["account_created_#{person.id}"]).to be_truthy
      end

      it "redirects when already verified" do
        get :create_account

        assert_redirected_to dashboard_path
      end

      context "user verifying account" do
        before { session[:verification] = :foo }

        it "doesn't redirect" do
          get :create_account

          assert_response :success
        end

        it "updates recent login" do
          expect { get :create_account }.to change { person.recent_login }
        end

        it "sets @redirect_url for use by the create_account view" do
          get :create_account

          expect(controller.instance_variable_get(:@redirect_url)).to be_truthy
        end
      end

      context "plans feature live" do
        before do
          allow(FeatureFlipper).to receive(:show_feature?).and_return true
          cookies[:plans_redirect] = { value: true }
        end

        it "redirects when creation cookie set" do
          cookies["account_created_#{person.id}"] = :foo

          get :create_account

          assert_redirected_to plans_path
        end

        it "redirects when already verified" do
          get :create_account

          assert_redirected_to plans_path
        end
      end
    end
  end

  describe "#close_splits_pending_banner" do
    it "sets the session" do
      post :close_splits_pending_banner

      expect(session[:splits_pending_banner_closed]).to be_truthy
      expect(response.body).to eq({ success: true }.to_json)
    end
  end
end
