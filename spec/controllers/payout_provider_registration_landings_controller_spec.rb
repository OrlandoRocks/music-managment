require 'rails_helper'

describe PayoutProviderRegistrationLandingsController do
  describe "#show" do
    before do
      @person = create(:person)
      @payout_provider = create(:payout_provider,
        person: @person,
        provider_status: PayoutProvider::ONBOARDING
      )
      login_as @person
    end

    it "updates a users payout provider state to change from onboarding to pending" do
      expect{ get :show }.to change{
        @payout_provider.reload.provider_status
      }.from(PayoutProvider::ONBOARDING).to(PayoutProvider::PENDING)
    end

    it "should not update the payout provider state to pending if this url is loaded after approval" do
      @payout_provider.update(provider_status: PayoutProvider::APPROVED)
      get :show
      expect(@payout_provider.reload.provider_status).to eq(PayoutProvider::APPROVED)
    end

    context "Redesign feature flipper is enabled" do
      it "redirects to the payoneer status page" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_redesign, @person).and_return(true)

        get :show
        expect(response).to redirect_to(payoneer_status_index_path)
      end
    end

    context "redesign feature flipper is disabled" do
      it "redirects to the withdraw page" do
        get :show
        expect(response).to redirect_to(withdraw_path)
      end
    end
  end
end
