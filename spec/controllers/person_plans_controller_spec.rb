require 'rails_helper'

RSpec.describe PersonPlansController, type: :controller do
  describe "create" do
    let!(:person) { create(:person) }
    let!(:album) { create(:album, finalized_at: Date.today, person: person) }
    let!(:album2) { create(:album, finalized_at: Date.today, person: person) }

    before(:each) do
      login_as(person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
    end

    it "should remove all purchases from cart when a migrating user adds plan" do
      expect{
        Product.add_to_cart(person, album)
        Product.add_to_cart(person, album2)
      }.to change(Purchase, :count).by(2)

      expect {
        post :create, params: {plan_id: 4}
      }.to change(Purchase, :count).by(-1)

      expect(subject).to redirect_to cart_path
    end
  end
end
