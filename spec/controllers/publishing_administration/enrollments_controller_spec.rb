require "rails_helper"

describe PublishingAdministration::EnrollmentsController do
  let(:person) { create(:person) }

  before(:each) do
    allow(controller).to receive(:current_user).and_return(person)
  end

  describe "#new" do
    it "redirects to the publishing roles controller if publishing_role_id is not present" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe "#create" do
    let(:params) do
      {
        publishing_administration_publishing_enrollment_form: {
          "first_name" => "Tom",
          "last_name" => "Brady"
        }
      }
    end

    context "when the publishing_composer is invalid" do
      it "renders the new template" do
        params[:publishing_administration_publishing_enrollment_form][:first_name] = ""
        post :create, params: params
        expect(response).to render_template(:new)
      end
    end

    context "when the publishing_composer is valid" do
      it "redirects to the cart view" do
        enrollment_form = double(:enrollment_form, save: true)
        allow(enrollment_form).to receive_message_chain(:publishing_composer, :id) { 1 }
        allow(enrollment_form).to receive(:publishing_composer) { 1 }
        allow(PublishingAdministration::PublishingEnrollmentForm).to receive(:new) { enrollment_form }

        post :create, params: params
        expect(response).to redirect_to cart_path
      end
    end
  end

  describe "#edit" do
    let(:publishing_composer) { create(:publishing_composer, person: person) }

    it "invokes the ParamsComposerService to compose the enrollment form params" do
      expect(PublishingAdministration::PublishingParamsComposerService).to receive(:compose)
        .with(publishing_composer.id)
      get :edit, params: { id: publishing_composer.id }
    end
  end

  describe "#update" do
    let(:account) { create(:account, person: person) }
    let(:publishing_composer) { create(:publishing_composer, account: account, person: account.person) }

    let(:params) do
      {
        id: publishing_composer.id,
        person_id: account.person_id,
        publishing_administration_publishing_enrollment_form: {
          "first_name" => "Tom  ",
          "last_name" => "Brady  ",
          "publishing_role_id" => "5",
        }.with_indifferent_access
      }
    end

    context "when the publishing_composer is invalid" do
      it "renders the new template" do
        params[:publishing_administration_publishing_enrollment_form][:first_name] = ""
        post :update, params: params
        expect(response).to render_template(:new)
      end
    end

    context "when the publishing_composer is valid" do
      it "redirects to the cart view" do
        post :update, params: params
        expect(response).to redirect_to cart_path
      end
    end

    context "when the current_user tries to update a publishing_composer that does not belong to them" do
      it "renders the forbidden page" do
        forbidden_page = File.join(Rails.root, "public/403.html")

        post :update, params: params.merge(id: "12345")

        expect(response).to render_template("tc-foundation", forbidden_page)
      end
    end
  end
end
