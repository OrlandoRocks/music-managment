require "rails_helper"

describe PublishingAdministration::ComposersController do
  before :each do
    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
  end

  describe "#index" do
    context "when the person does NOT have publishing_composers" do
      it "redirects to the publishing roles controller" do
        account = create(:account)
        allow(controller).to receive(:current_user).and_return(account.person)

        get :index, params: { id: account.person_id }
        expect(response).to redirect_to new_publishing_administration_enrollment_path
      end
    end

    context "when the person has publishing_composers" do
      context "when the current user is the publishing administrator" do
        it "returns all the publishing_composers managed by the user" do
          account                    = create(:account)
          publishing_composer        = create(:publishing_composer, account: account, person: account.person)
          other_publishing_composer  = create(:publishing_composer, account: account)

          allow(controller).to receive(:current_user).and_return(account.person)

          expect(PublishingAdministration::PublishingComposersBuilder)
            .to receive(:build)
            .with(account.id, nil)
            .and_return([publishing_composer, other_publishing_composer])

          get :index, params: { id: account.person.id }
        end
      end

      context "when the current user's publishing_composer is managed by someone else" do
        it "returns only the current user's publishing_composer" do
          person                     = create(:person)
          account                    = create(:account)
          publishing_composer        = create(:publishing_composer, account: account, person: person)
          other_publishing_composer  = create(:publishing_composer, account: account)
          person_account_with_no_publishing_composers = create(:account, person: person)

          allow(controller).to receive(:current_user).and_return(person)

          expect(PublishingAdministration::PublishingComposersBuilder)
            .to receive(:build)
            .with(account.id, person.id)
            .and_return([publishing_composer])

          get :index, params: { id: person.id }
        end
      end
    end
  end

  describe "#update" do
    context "when only the cae number needs to get updated" do
      let(:account) { create(:account) }
      let(:publishing_composer) { create(:publishing_composer, :skip_cae_validation, cae: "123456789", account: account, person: account.person) }

      let(:params) do
        {
          id: publishing_composer.id,
          publishing_administration_publishing_enrollment_form: {
            "id" => publishing_composer.id.to_s,
            "composer_cae" => "987654321",
            "first_name" => "#{publishing_composer.first_name}  ",
            'dob(1i)' => publishing_composer.dob.year.to_s,
            'dob(2i)' => publishing_composer.dob.month.to_s,
            'dob(3i)' => publishing_composer.dob.day.to_s,
          },
          format: :js
        }
      end

      let(:expected_params) do
        {
          id: publishing_composer.id.to_s,
          name_prefix: nil,
          first_name: publishing_composer.first_name,
          middle_name: nil,
          last_name: publishing_composer.last_name,
          name_suffix: nil,
          dob: publishing_composer.dob,
          dob_y: publishing_composer.dob.year.to_s,
          dob_m: publishing_composer.dob.month.to_s,
          dob_d: publishing_composer.dob.day.to_s,
          email: publishing_composer.current_email,
          person_id: publishing_composer.person_id,
          composer_cae: "987654321",
          composer_pro_id: nil,
          publishing_role_id: nil,
        }.with_indifferent_access
      end

      it "correctly formats the params" do
        account.person.publishing_composers << publishing_composer
        allow(controller).to receive(:current_user).and_return(account.person)

        expect(PublishingAdministration::PublishingEnrollmentForm).to receive(:new).with(expected_params).and_call_original

        put :update, params: params
      end
    end
  end

  describe "#sync_opt_in" do
    it "should set the publishing_composer's sync_opted_in to true" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, person: account.person, sync_opted_in: false)
      allow(controller).to receive(:current_user).and_return(account.person)

      expect(publishing_composer.sync_opted_in).to eq(false)

      post :sync_opt_in, params: {id: publishing_composer.id}

      publishing_composer.reload

      expect(publishing_composer.sync_opted_in).to eq(true)
    end

    it "should set the publishing_composer's sync_opted_updated_at to the current time" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, person: account.person, sync_opted_in: false)
      allow(controller).to receive(:current_user).and_return(account.person)

      expect(publishing_composer.sync_opted_updated_at).to eq(nil)

      post :sync_opt_in, params: {id: publishing_composer.id}

      publishing_composer.reload

      expect(publishing_composer.sync_opted_updated_at).to_not eq(nil)
    end
  end

  describe "#sync_opt_out" do
    it "should set the publishing_composer's sync_opted_in to false" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, person: account.person, sync_opted_in: true)
      allow(controller).to receive(:current_user).and_return(account.person)

      expect(publishing_composer.sync_opted_in).to eq(true)

      post :sync_opt_out, params: {id: publishing_composer.id}

      publishing_composer.reload

      expect(publishing_composer.sync_opted_in).to eq(false)
    end

    it "should set the publishing_composer's sync_opted_updated_at to the current time" do
      account = create(:account)
      publishing_composer = create(:publishing_composer, account: account, person: account.person, sync_opted_in: true)
      allow(controller).to receive(:current_user).and_return(account.person)

      expect(publishing_composer.sync_opted_updated_at).to eq(nil)

      post :sync_opt_out, params: {id: publishing_composer.id}

      publishing_composer.reload

      expect(publishing_composer.sync_opted_updated_at).to_not eq(nil)
    end
  end
end
