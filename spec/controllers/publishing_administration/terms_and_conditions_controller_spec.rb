require "rails_helper"

describe PublishingAdministration::TermsAndConditionsController do
  describe "#update" do
    let(:person) { create(:person) }

    before(:each) do
      allow(controller).to receive(:current_user).and_return(person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    let(:publishing_composer) { create(:publishing_composer, person: person, agreed_to_terms_at: nil) }

    it "should update the agreed terms date of a publishing_composer" do
      put :update, params: { id: publishing_composer.id, event: "signing_complete" }

      publishing_composer.reload
      expect(publishing_composer.agreed_to_terms_at).to be_truthy
      expect(response).to redirect_to cart_path
    end

    it "should redirect when signing_complete event is not sent" do
      put :update, params: { id: publishing_composer.id }

      expect(publishing_composer.agreed_to_terms_at).to be_nil
      expect(response).to redirect_to publishing_administration_terms_and_condition_path(publishing_composer)
    end
  end
end
