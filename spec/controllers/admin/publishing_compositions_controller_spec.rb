require "rails_helper"

describe Admin::PublishingCompositionsController, type: :controller do
  let(:admin) { create(:person, :admin) }
  let(:account) { create(:account, person: admin) }
  let(:role) { create(:role, name: "Publishing Manager") }

  before do
    admin.roles << role
    login_as(admin)
  end

  describe "#index" do
    context "when the admin does not have access to the publishing_composer publishing_compositions page" do
      xit "redirects to the publishing_composers manager page" do
        publishing_composer = create(:publishing_composer, account: account)
        get :index, params: { publishing_composer_id: publishing_composer.id }

        expect(response).to redirect_to admin_publishing_composers_manager_path(account)
      end
    end
  end

  describe "#update" do
    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "when the publishing_composer does not have a provider identifier" do
      it "sets an error message" do
        publishing_composer = create(:publishing_composer, account: account)
        publishing_composition = create(:publishing_composition, name: "Original Name", provider_identifier: "00001C8GW", account: account)
        create(:publishing_composition_split, publishing_composer: publishing_composer, publishing_composition: publishing_composition)
        params = {
          id: publishing_composition.id,
          publishing_composer_id: publishing_composer.id,
          publishing_composition: {
            name: "Original Name"
          },
          format: :js
        }

        put :update, params: params

        expect(assigns(:error_msg)).to eq "Unable to send publishing_composition to Rights App"
      end
    end

    context "when the publishing_composer has a provider identifier" do
      it "updates the publishing_composition" do
        publishing_composer = create(:publishing_composer, :with_provider_identifier, account: account)
        publishing_composition = create(:publishing_composition, name: "Original Name", provider_identifier: "00001C8GW", account: account)
        params = {
          id: publishing_composition.id,
          publishing_composer_id: publishing_composer.id,
          publishing_composition: {
            name: "Updated Name",
            provider_identifier: "00001C8GZ",
          },
          format: :js
        }

        allow(PublishingComposition).to receive(:find_by).and_return(publishing_composition)
        expect(publishing_composition).to receive(:send_to_rights_app)

        put :update, params: params

        expect(publishing_composition.reload.name).to eq params[:publishing_composition][:name]
        expect(publishing_composition.provider_identifier).to eq params[:publishing_composition][:provider_identifier]
      end
    end
  end

  describe ".update_status" do
    it "updates the publishing_composition status" do
      publishing_composer = create(:publishing_composer, :with_provider_identifier, account: account)
      publishing_composition = create(:publishing_composition, name: "Original Name", provider_identifier: "00001C8GW", account: account)
      publishing_composition.update(state: 'hidden')
      params = {
        id: publishing_composition.id,
        publishing_composer_id: publishing_composer.id,
        format: :js
      }

      allow(PublishingComposition).to receive(:find_by).and_return(publishing_composition)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      put :update_status, params: params

      expect(publishing_composition.reload.state).to eq 'new'
    end
  end


  describe "#update_eligibility" do
    context "when the publishing_composition is ineligible" do
      it "updates the publishing_composition state to its previous state" do
        publishing_composer = create(:publishing_composer, :with_provider_identifier, account: account)
        publishing_composition = create(:publishing_composition, name: "Original Name", provider_identifier: "00001C8GW", account: account)
        publishing_composition.update(state: 'verified')
        publishing_composition.update(state: 'ineligible')

        params = {
          id: publishing_composition.id,
          publishing_composer_id: publishing_composer.id,
          format: :js
        }

        allow(PublishingComposition).to receive(:find_by).and_return(publishing_composition)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        put :update_eligibility, params: params

        expect(publishing_composition.reload.state).to eq('verified')
      end
    end

    context "when the publishing_composition is not ineligible" do
      it "updates the publishing_composition state to ineligible" do
        publishing_composer = create(:publishing_composer, :with_provider_identifier, account: account)
        publishing_composition = create(:publishing_composition, name: "Original Name", provider_identifier: "00001C8GW", account: account)
        publishing_composition.update(state: 'verified')

        params = {
          id: publishing_composition.id,
          publishing_composer_id: publishing_composer.id,
          format: :js
        }

        allow(PublishingComposition).to receive(:find_by).and_return(publishing_composition)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        put :update_eligibility, params: params

        expect(publishing_composition.reload.state).to eq('ineligible')
      end
    end
  end

  describe "#populate_missing_compositions" do
    it "queues up a sidekiq job" do
      publishing_composer = create(:publishing_composer, :with_provider_identifier, account: account)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      expect(PublishingAdministration::PublishingCompositionCreationWorker).to receive(:perform_async)

      get :populate_missing_compositions, params: { publishing_composer_id: publishing_composer.id }
    end
  end
end
