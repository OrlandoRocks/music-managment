require "rails_helper"

describe Admin::PublishingComposerSyncOptInsController do
  before :each do
    @admin = create(:person, :admin)
    @person = create(:person)
    @account = create(:account, person: @person)
    @publishing_composer = create(:publishing_composer, account: @account, sync_opted_in: true, sync_opted_updated_at: Time.now)

    login_as(@admin)

    allow_any_instance_of(Admin::PublishingComposerSyncOptInsController)
      .to receive(:redirect_to)
      .and_return(true)

    allow_any_instance_of(Admin::PublishingComposerSyncOptInsController)
      .to receive(:destruction_redirection_path)
      .and_return(true)
  end

  describe "#destroy" do
    context "when the publishing_composer is sync_opted_in" do
      it "sets sync_opted_in to false" do
        expect(@publishing_composer.reload.sync_opted_in).to be_truthy

        publishing_composer_params = { id: @publishing_composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, publishing_composer: publishing_composer_params }

        @publishing_composer.reload

        expect(@publishing_composer.sync_opted_in).to be_falsey
      end

      it "sets sync_opted_updated_at to the passed in date" do
        expect(@publishing_composer.reload.sync_opted_in).to be_truthy

        publishing_composer_params = { id: @publishing_composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, publishing_composer: publishing_composer_params }

        @publishing_composer.reload

        expect(@publishing_composer.sync_opted_updated_at.to_date.to_s).to eq("2021-10-18")
      end
    end

    context "when the publishing_composer is not sync_opted_in" do
      it "sets sync_opted_in to false" do
        @publishing_composer = create(:publishing_composer, account: @account, sync_opted_in: false)

        expect(@publishing_composer.reload.sync_opted_in).to be_falsey

        publishing_composer_params = { id: @publishing_composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, publishing_composer: publishing_composer_params }

        @publishing_composer.reload

        expect(@publishing_composer.sync_opted_in).to be_falsey
      end
    end

    context "when sync_opted_updated_at is not passed in" do
      before(:each) do
        Timecop.freeze
      end

      after(:each) do
        Timecop.return
      end

      it "sets sync_opted_updated_at to today's date" do
        date_today = Date.today

        @publishing_composer = create(:publishing_composer, account: @account, sync_opted_in: false, sync_opted_updated_at: nil)

        expect(@publishing_composer.sync_opted_updated_at).to eq(nil)

        publishing_composer_params = { id: @publishing_composer.id }

        delete :destroy, params: { id: @account.id, publishing_composer: publishing_composer_params }

        @publishing_composer.reload

        expect(@publishing_composer.sync_opted_updated_at.to_date.to_s).to eq(date_today.to_s)
      end
    end
  end
end
