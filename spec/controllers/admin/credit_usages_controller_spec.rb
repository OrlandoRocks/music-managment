require "rails_helper"

describe Admin::CreditUsagesController, type: :controller do

  describe "POST #destroy_all" do
    before(:each) do
      @person = create(:person, :admin)
      login_as(@person)
    end

     context "endpoint" do
      it "should destroy an user's credit_usages" do
        product = create(:product, :five_album_distribution_credits)
        product_item = create(:product_item, :product => product)
        distribution_credit_purchase = create(:purchase, :five_album_distribution_credits, :person_id => @person.id, :related => product)
        inventory = create(:album_credit_inventory,
                           :person => @person,
                           :purchase => distribution_credit_purchase,
                           :product_item => product_item,
                           :quantity => 5)
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        album_2 = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        credit_usage =  create(:credit_usage, :person => @person, :related => album, :applies_to_type => album.class.name)
        credit_usage_purchase = create(:purchase, :unpaid, :credit_usage, :person_id => @person.id, :related => credit_usage)
        credit_usage_2 =  create(:credit_usage, :person => @person, :related => album_2, :applies_to_type => album_2.class.name)
        credit_usage_purchase_2 = create(:purchase, :unpaid, :credit_usage, :person_id => @person.id, :related => credit_usage_2)

        expect {
          post :destroy_all, params: {person_id: @person.id}
        }.to change{CreditUsage.count}.by(-2)
      end
    end

    context "endpoint" do
      it "should destroy an user's unfinalized purchases to clear cart" do
        product = create(:product, :five_album_distribution_credits)
        product_item = create(:product_item, :product => product)
        distribution_credit_purchase = create(:purchase, :five_album_distribution_credits, :person_id => @person.id, :related => product)
        inventory = create(:album_credit_inventory,
                           :person => @person,
                           :purchase => distribution_credit_purchase,
                           :product_item => product_item,
                           :quantity => 5)
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        album_2 = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        credit_usage =  create(:credit_usage, :person => @person, :related => album, :applies_to_type => album.class.name)
        credit_usage_purchase = create(:purchase, :unpaid, :credit_usage, :person_id => @person.id, :related => credit_usage)
        credit_usage_2 =  create(:credit_usage, :person => @person, :related => album_2, :applies_to_type => album_2.class.name)
        credit_usage_purchase_2 = create(:purchase, :unpaid, :credit_usage, :person_id => @person.id, :related => credit_usage_2)

        expect {
          post :destroy_all, params: {person_id: @person.id}
        }.to change{Purchase.count}.by(-2)
      end
    end

    context "endpoint" do
      it "should not destroy previously completed paid purchase with credit_usage" do
        product = create(:product, :five_album_distribution_credits)
        product_item = create(:product_item, :product => product)
        distribution_credit_purchase = create(:purchase, :five_album_distribution_credits, :person_id => @person.id, :related => product)
        inventory = create(:album_credit_inventory,
                           :person => @person,
                           :purchase => distribution_credit_purchase,
                           :product_item => product_item,
                           :quantity => 5)
        album = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        album_2 = create(:album, :with_uploaded_songs_and_artwork, :with_songwriter, :with_multiple_uploaded_songs)
        credit_usage =  create(:credit_usage, :person => @person, :related => album, :applies_to_type => album.class.name)
        credit_usage_purchase = create(:purchase, :unpaid, :credit_usage, :person_id => @person.id, :related => credit_usage)

        previous_credit_usage =  create(:credit_usage, :person => @person, :related => album_2, :applies_to_type => album_2.class.name)
        previous_credit_usage_purchase_2 = create(:purchase, :credit_usage, :person_id => @person.id, :related => previous_credit_usage)


        expect {
          post :destroy_all, params: {person_id: @person.id}
        }.to change{Purchase.count}.by(-1)
      end
    end

  end
end
