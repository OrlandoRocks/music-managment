require "rails_helper"

describe Admin::InvoicesController, type: :controller do
context "when kicking a non-settled invoice" do
  before(:each) do
    person = TCFactory.build_person(:force_is_administrator => true)
    allow(person).to receive(:is_permitted_to?).and_return(true) #covers permission protection for settling an invoice
    login_as(person)
    @invoice = mock_model(Invoice, :can_settle? => true, :settled? => false, :settled! => nil)
    expect(@invoice).to receive(:find).and_return(@invoice)
    @person = mock_model(Person)
    expect(@person).to receive(:invoices).and_return(@invoice)
    allow(@person).to receive(:is_administrator?).and_return(true)
    allow(@person).to receive(:is_permitted_to?).and_return(true)
    expect(@invoice).to receive(:person).at_least(:once).and_return(@person)
    expect(Person).to receive(:find).at_least(:once).and_return(@person)
  end

  it "should check if the invoice can_settle?" do
    expect(@invoice).to receive(:can_settle?).and_return(false)
    put 'settle', params: { person_id: :anything, id: :whatever }
  end

  it "should check if the invoice is settled" do
    allow(@invoice).to receive(:can_settle?).and_return(false)
    expect(@invoice).to receive(:settled?).and_return(true)
    put 'settle', params: { person_id: :anything, id: :whatever }
  end

  it "should set flash explaining that the invoice is already settled" do
    allow(@invoice).to receive(:can_settle?).and_return(false)
    allow(@invoice).to receive(:settled?).and_return(true)
    put 'settle', params: { person_id: :anything, id: :whatever }
    expect(flash[:error]).to match(/already been marked/)
  end

  it "should set flash explaining that the invoice doesn't think it can be settled!()" do
    allow(@invoice).to receive(:can_settle?).and_return(false)
    allow(@invoice).to receive(:settled?).and_return(false)
    put 'settle', params: { person_id: :anything, id: :whatever }
    expect(flash[:error]).to match(/enough payments/)
  end

  it "should call settled! if the invoice can be settled" do
    expect(@invoice).to receive(:settled!).and_return(nil)
    put 'settle', params: { person_id: :anything, id: :whatever }
  end

  it "should redirect" do
    put 'settle', params: { person_id: :anything, id: :whatever }
    expect(response).to be_redirect
  end

  it "should redirect to the invoice show page" do
    put 'settle', params: { person_id: :anything, id: :whatever }
    expect(response.redirect_url).to eql(admin_person_invoice_url(@invoice.person, @invoice))
  end
end
end
