require "rails_helper"

describe Admin::PublishingTerminatedComposersController do
  before :each do
    @admin = create(:person, :admin)
    @person = create(:person)
    @account = create(:account, person: @person)
    @publishing_composer = create(:publishing_composer, account: @account)

    login_as(@admin)
    allow(FeatureFlipper).to receive(:show_feature?).and_return false

    allow_any_instance_of(Admin::PublishingTerminatedComposersController)
      .to receive(:redirect_to)
      .and_return(true)

    allow_any_instance_of(Admin::PublishingTerminatedComposersController)
      .to receive(:redirection_path)
      .and_return(true)
  end

  describe "#create" do
    it "creates a fully terminated publishing_composer" do
      post :create, params: { id: @account.id, terminated_composer: { publishing_composer_id: @publishing_composer.id } }
      expect(@publishing_composer.reload.terminated_composer).not_to eq nil
    end

    it "returns an error if the effective_date is not formatted correctly" do
      params = { publishing_composer_id: @publishing_composer.id, effective_date: "10-18-87" }
      post :create, params: { id: @account.id, terminated_composer: params }
      expect(flash[:error]).to be_present
    end
  end

  describe "#update" do
    it "updates a partially terminated publishing_composer to be fully terminated" do
      create(:terminated_composer, :partially, publishing_composer: @publishing_composer)
      params = { publishing_composer_id: @publishing_composer.id, reason: "", effective_date: "" }

      expect { put :update, params: { id: @account.id, terminated_composer: params } }
        .to change { @publishing_composer.reload.terminated_composer.termination_type }
        .from("partially").to("fully")
    end

    it "returns an error if the effective_date is not formatted correctly" do
      params = { publishing_composer_id: @publishing_composer.id, effective_date: "10-18-87" }
      put :update, params: { id: @account.id, terminated_composer: params }
      expect(flash[:error]).to be_present
    end
  end

  describe "#destroy" do
    before :each do
      @terminated_composer = create(:terminated_composer, :fully, publishing_composer: @publishing_composer)
    end

    it "destroys a terminated publishing_composer" do
      post :destroy, params: { id: @terminated_composer.id }
      @publishing_composer.reload

      expect(@publishing_composer.terminated_composer).to be nil
    end
  end
end
