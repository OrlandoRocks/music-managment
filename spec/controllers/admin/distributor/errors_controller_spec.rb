require "rails_helper"

describe Admin::Distributor::ErrorsController, type: :controller do
  before do
    login_as(Person.find_by_name!("Tunecore Admin"))
  end

  describe "#index" do
    before do
      allow_any_instance_of(Distributor::ErrorForm).to receive(:query_for_errored_releases)
    end

    it "should call the Distributor Error Form with the params passed in" do
      params = { start_date: "11/01/2017", end_date: "11/02/2017", filter_type: "store", error_type: "All", store_id: "4" }

      expect(Distributor::ErrorForm).to receive(:new).with(params).and_call_original
      get :index, params: params
    end

    context "when the start date and end date are not sent into params" do
      it "defaults to nil" do
        params = { start_date: nil, end_date: nil, filter_type: "store", error_type: "All", store_id: "4" }
        expect(Distributor::ErrorForm).to receive(:new).with(params).and_call_original
        get :index, params: { filter_type: "store", error_type: "All", store_id: "4" }
      end
    end

    context "when filter type is not passed into params" do
      it "defaults to nil" do
        params = { start_date: nil, end_date: nil, filter_type: nil, error_type: nil, store_id: nil }
        expect(Distributor::ErrorForm).to receive(:new).with(params).and_call_original
        get :index
      end
    end
  end

  describe "#create" do
    let(:person) { Person.find_by_name!("Tunecore Admin") }

    it "should retry each distribution passed into params" do
      tc_distributor_base_response = double(:tc_distributor_base_response)
      expect(TcDistributor::Base).to receive(:new).with(releases_store_ids: ["1", "2"]).and_return(tc_distributor_base_response)
      expect(tc_distributor_base_response).to receive(:releases_stores).and_return(JSON.parse([
          {
            delivery_type: 'full_delivery',
            store_id: '4',
            album_id: '5'
          }, {
            delivery_type: 'full_delivery',
            store_id: '5',
            album_id: '6'
          }
        ].to_json, object_class: OpenStruct)
      )

      expect(Sns::Notifier).to receive(:perform).with(
        topic_arn: ENV['SNS_RELEASE_RETRY_TOPIC'],
        album_ids_or_upcs: ['5'],
        store_ids: ['4'],
        delivery_type: 'full_delivery',
        person_id: person.id
      )

      expect(Sns::Notifier).to receive(:perform).with(
        topic_arn: ENV['SNS_RELEASE_RETRY_TOPIC'],
        album_ids_or_upcs: ['6'],
        store_ids: ['5'],
        delivery_type: 'full_delivery',
        person_id: person.id
      )

      post :create, params: { format: :js, releases_store_ids: [1,2] }
      expect(response).to render_template(:create)
    end
  end
end
