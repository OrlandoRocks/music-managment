require "rails_helper"

describe Admin::PaymentController, type: :controller do
  before(:each) do
    @person = Person.find_by(email: "super_admin@tunecore.com")
    login_as(@person)
  end

  describe "#index" do
    let(:paypal_transfer) { create(:paypal_transfer) }

    it "calls appropriate service method to fetch filtered transfers" do
      allow(Admin::PaymentService).to receive(:fetch_transfers).and_return({ transfers: [paypal_transfer] })
      get :index, xhr: true, params: { type: "paypal", threshold_status: "over" }
      expect(controller.instance_variable_get(:@transfers)).to eq([paypal_transfer])
    end

    it "sets flash with error message for invalid 'type' filter" do
      get :index, xhr: true, params: { type: "invalid-type", threshold_status: "over" }
      expect(flash[:error]).to eq("There is no type of invalid-type")
      expect(controller.instance_variable_get(:@transfers)).to be_nil
    end

    it "sets variables for index action" do
      allow(controller).to receive(:set_variables_for_index)
      get :index, xhr: true
    end
  end

  it "should cancel a paypal tansfer" do
    @paypal_transfer = TCFactory.create(:paypal_transfer)

    get :cancel_payment, params: { id: @paypal_transfer.id, type: "paypal" }
    assert_redirected_to :controller=>"admin/payment", :action=>"index"

    assert @paypal_transfer.reload.transfer_status=="cancelled"
  end

  it "should cancel a check transfer" do
    @check_transfer = TCFactory.create(:check_transfer)

    get :cancel_payment, params: { id: @check_transfer.id, type: "check" }
    assert_redirected_to :controller=>"admin/payment", :action=>"index"

    assert @check_transfer.reload.transfer_status=="cancelled"
  end

  describe "do mass pay paypal" do
    let(:paypal_transfer) { create(:paypal_transfer) }
    let(:payee) { paypal_transfer.person }

    it "should update person id for paypal transfer" do
      login_as(payee)
      login_as(@person)

      allow(FeatureFlipper).to receive(:show_feature?).with(anything, anything).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(anything).and_return(true)

      cw = double(CountryWebsite, country: "US", currency: "USD")
      allow(CountryWebsite).to receive(:find).and_return(cw)
      mp = double(:mass_pay)

      allow(cw).to receive_message_chain(:paypal_withdrawal_account,:mass_pay).and_return(mp)
      allow(mp).to receive(:ack).and_return("Success")
      post :mass_pay_paypal, params: { transfer: {id: [paypal_transfer.id]} }

      assert paypal_transfer.reload.approved_by_id == @person.id
    end

    context "when auto-approved-withdrawals feature flag is set" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      end
      let!(:service_params) { { action: "approve", transfer_ids: ["101"], person: @person, provider: "paypal" } }

      it "calls appropriate method in ApproveService" do
        allow_any_instance_of(controller.request.class).to receive(:referrer)
          .and_return("/home")

        expect(Transfers::ApproveService).to receive(:call)
          .with(service_params)
          .and_return({ success: true, errors: [] })
        post :mass_pay_paypal, params: { transfer: { id: [101] } }
        expect(flash[:notice]).to eq("Payments completed successfully.")
        expect(response).to redirect_to("/home")
      end

      it "handles error" do
        expect(Transfers::ApproveService).to receive(:call)
          .with(service_params)
          .and_return({ success: false, errors: ["boom"] })
        post :mass_pay_paypal, params: { transfer: { id: [101] } }
        expect(flash[:error]).to eq("Some payments not processed: boom")
        expect(response).to redirect_to("/admin/payment")
      end
    end
  end

  describe "do mass pay check" do
    it "should update person id for check" do
      @check_transfer = TCFactory.create(:check_transfer)

      post :mass_pay_check, params: { transfer: {id: [@check_transfer.id]} }

      assert @check_transfer.reload.approved_by_id==@person.id
    end
  end

  describe "process payment" do
    it "should update person id for payapl transfer" do
      @check_transfer = TCFactory.create(:check_transfer)

      post :process_payment, params: { id: @check_transfer.id, type: "check" }

      assert @check_transfer.reload.approved_by_id==@person.id
    end
  end

end
