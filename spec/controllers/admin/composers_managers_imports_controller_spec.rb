require "rails_helper"

describe Admin::ComposersManagersImportsController do
  describe "POST #create" do
    it "should invoke the IngestionBlockerWorker" do
      admin = create(:person, :admin)
      login_as(admin)
      composer_manager_account = create(:account)

      expect(PublishingAdministration::IngestionBlockerWorker)
        .to receive(:perform_async)
        .with(composer_manager_account.id.to_s)

      post :create, params: { composers_manager_id: composer_manager_account.id, format: :js }
    end
  end
end
