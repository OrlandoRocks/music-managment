require "rails_helper"

describe Admin::SalepointSubscriptionsController, type: :controller do
  describe "#toggle_active" do
    before(:each) do
      @person = TCFactory.create(:person)
      role = Role.find_by(name: "Admin")
      @person.roles << role
      login_as @person

      TCFactory.build_ad_hoc_salepoint_subscription_product(5.00)
    end

    it "should only toggle purchased salepoint subscriptions" do
      album = TCFactory.create(:album, :person => @person, :finalized_at => Date.today)
      sp_sub = TCFactory.create(:salepoint_subscription, :album => album)

      post :toggle_active, params: { album_id: album.id }, xhr: true
      assert_response :error, @response.body

      sp_sub.finalize!

      expect {
        post :toggle_active, params: { album_id: album.id }, xhr: true
        assert_response :success, @response.body
      }.to change(Note,:count).by(1)

      sp_sub.reload
      expect(sp_sub.is_active).to eq(false)

      expect {
        post :toggle_active, params: { album_id: album.id }, xhr: true
        assert_response :success, @response.body
      }.to change(Note, :count).by(1)

      sp_sub.reload
      expect(sp_sub.is_active).to eq(true)
    end

  end
end
