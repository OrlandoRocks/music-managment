require "rails_helper"

describe Admin::CopyrightStatusesController do
  describe "#update" do
    let(:admin) { create(:person, :admin) }

    before(:each) do
      login_as(admin)
    end

    it "invokes the Admin::CopyrightStatusUpdaterService" do
      copyright_status = create(:copyright_status)

      params = { id: copyright_status.id, copyright_status: { status: "approved" } }
      expected_params = ActionController::Parameters.new(status: "approved").permit!

      expect(Admin::CopyrightStatusUpdaterService)
        .to receive(:update)
        .with(copyright_status, admin, expected_params)

      put :update, params: params, xhr: true
    end
  end
end
