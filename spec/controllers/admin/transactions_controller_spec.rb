require "rails_helper"

describe Admin::TransactionsController do
  let(:indian_user) { create(:person, :with_india_country_website, :with_all_roles) }
  let(:american_user) { create(:person, :with_all_roles) }

  describe "#index" do
    let(:payments_os_transactions) { create_list(:payments_os_transaction, 5, person: indian_user) }
    let(:payu_transactions) { create_list(:payu_transaction, 5, person: indian_user) }
    let(:braintree_transactions) do
      create_list(:braintree_transaction, 5, person: american_user)
    end

    it "shows payments_os transactions when country_website filter is set to IN" do
      login_as indian_user
      get :index, params: {
        d: {
          country_website_id: 8,
          action: "All"
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      expected_transaction_ids = payments_os_transactions.pluck(:id)
      expect(assigns(:transactions).pluck(:id)).to contain_exactly(*expected_transaction_ids)
    end

    it "shows payu transactions when country_website filter is IN & payment type is PayU" do
      login_as indian_user
      get :index, params: {
        d: {
          country_website_id: 8,
          action: "All"
        },
        transaction_type: PAYU_TRANSACTION,
        commit: "Search"
      }
      expected_transaction_ids = payu_transactions.pluck(:id)
      expect(assigns(:transactions).pluck(:id)).to contain_exactly(*expected_transaction_ids)
    end

    it "shows braintree transactions when country_website filter is to to US" do
      login_as american_user
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All"
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      expected_transaction_ids = braintree_transactions.pluck(:transaction_id)
      returned_transaction_ids = assigns(:transactions).map(&:transaction_id)
      expect(returned_transaction_ids).to include(*expected_transaction_ids)
    end

    it "should query by transaction id" do
      login_as american_user
      selected_transaction = braintree_transactions.sample
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          transaction_id: selected_transaction.transaction_id
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to include(selected_transaction.id)
    end

    it "should query by min amount" do
      login_as american_user
      ten_dollar_transactions = create_list(:braintree_transaction, 5, person: american_user, amount: 10)
      twenty_dollar_transactions = create_list(:braintree_transaction, 5, person: american_user, amount: 20)
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          mina: 20
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to include(*twenty_dollar_transactions.pluck(:id))
    end

    it "should query by max amount" do
      login_as american_user
      ten_dollar_transactions = create_list(:braintree_transaction, 5, person: american_user, amount: 10)
      twenty_dollar_transactions = create_list(:braintree_transaction, 5, person: american_user, amount: 20)
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          maxa: 10
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to contain_exactly(*ten_dollar_transactions.pluck(:id))
    end

    it "should query by from_date" do
      login_as american_user
      one_week_ago = DateTime.now.ago(1.week)
      two_weeks_ago = DateTime.now.ago(2.weeks)
      transactions_created_1_wk_ago = create_list(:braintree_transaction, 5, person: american_user, created_at: one_week_ago)
      transactions_created_2_wks_ago = create_list(:braintree_transaction, 5, person: american_user, created_at: two_weeks_ago)
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          from_date: one_week_ago.strftime('%m/%d/%Y')
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to include(*transactions_created_1_wk_ago.pluck(:id))
    end

    it "should query by to_date" do
      login_as american_user
      one_week_ago = DateTime.now.ago(1.week)
      two_weeks_ago = DateTime.now.ago(2.weeks)
      transactions_created_1_wk_ago = create_list(:braintree_transaction, 5, person: american_user, created_at: one_week_ago)
      transactions_created_2_wks_ago = create_list(:braintree_transaction, 5, person: american_user, created_at: two_weeks_ago)
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          to_date: one_week_ago.strftime('%m/%d/%Y')
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to include(*transactions_created_2_wks_ago.pluck(:id))
      expect(returned_transaction_ids).not_to include(*transactions_created_1_wk_ago.pluck(:id))
    end

    it "should query by status" do
      login_as american_user
      declined_transactions = create_list(:braintree_transaction, 5, :declined, person: american_user)
      succeeded_transactions = create_list(:braintree_transaction, 5, :succeeded, person: american_user)
      get :index, params: {
        d: {
          country_website_id: 1,
          action: "All",
          status: 'success'
        },
        transaction_type: CREDIT_CARD,
        commit: "Search"
      }
      returned_transaction_ids = assigns(:transactions).map(&:id)
      expect(returned_transaction_ids).to include(*succeeded_transactions.pluck(:id))
    end
  end

  describe "#show" do
    let(:payos_transaction) { create(:payments_os_transaction) }
    let(:payu_transaction) { create(:payu_transaction) }
    let(:braintree_transaction) { create(:braintree_transaction, person: american_user) }

    it "displays the details of a payu transaction" do
      login_as indian_user
      get :show, params: { id: payu_transaction.id, payment_vendor: "payu" }
      expect(response.status).to eq(200)
    end

    it "displays the details of a payments_os transaction" do
      login_as indian_user
      get :show, params: { id: payos_transaction.id, payment_vendor: "payments_os" }
      expect(response.status).to eq(200)
    end

    it "displays the details of a braintree transaction" do
      login_as american_user
      get :show, params: { id: braintree_transaction.id, payment_vendor: "braintree" }
      expect(response.status).to eq(200)
    end
  end

  describe "#process_refund" do
    let(:braintree_transaction) { create(:braintree_transaction, person: american_user) }
    let(:payos_transaction) { create(:payments_os_transaction) }

    it "processes refund request for braintree transactions" do
      login_as american_user
      refund_transaction = create(:braintree_transaction, :refund)
      allow(BraintreeTransaction).to receive(:process_refund).and_return(refund_transaction)
      post :process_refund, params: {
        id: braintree_transaction.id,
        payment_vendor: "braintree",
        amount: braintree_transaction.amount,
        reason: 'Testing',
        category: 'Release did not go live'
      }
      expect(response).to redirect_to(
        "/admin/transactions/#{refund_transaction.id}/refund_success?payment_vendor=braintree"
      )
    end

    it "processes refund request for payments_os transactions" do
      login_as indian_user
      refund_transaction = PaymentsOSRefundDecorator.new(create(:payments_os_transaction, :successful_refund))
      allow_any_instance_of(PaymentsOS::RefundService).to receive(:refund).and_return(refund_transaction)
      post :process_refund, params: {
        id: payos_transaction.id,
        payment_vendor: "payments_os",
        amount: payos_transaction.amount,
        reason: 'Testing',
        category: 'Release did not go live'
      }
      expect(response).to redirect_to(
        "/admin/transactions/#{refund_transaction.id}/refund_success?payment_vendor=payments_os"
      )
    end

    xit "processes refund request for payu transactions" do
      #PAYU_TODO
    end
  end

  describe "#refund" do
    let(:payos_transaction) { create(:payments_os_transaction) }
    let(:braintree_transaction) { create(:braintree_transaction, person: american_user) }

    it "should display the refund form for a payments_os_transaction" do
      login_as american_user
      get :refund, params: { id: braintree_transaction.id, payment_vendor: "braintree" }
      expect(response.status).to eq(200)
    end
  end
end
