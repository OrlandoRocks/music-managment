require "rails_helper"

describe Admin::PreordersController, type: :controller do
  describe "#update" do
    before(:each) do
      @login_as = Person.find_by_name!("Tunecore Admin")
      login_as(@login_as)
      @album = TCFactory.create(:album)
      @preorder = TCFactory.create(:preorder_purchase, album: @album, paid_at: Time.now)
      allow(PetriBundle).to receive(:for).and_return(TCFactory.create(:petri_bundle, album: @album))
    end

    it "should update the correct attributes when iTunesWW is enabled only" do
      store = Store.find_by(short_name: "iTunesWW")
      salepoint = TCFactory.create(:salepoint, store: store, salepointable: @album)
      salepoint_preorder_data = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, paid_at: Time.now, preorder_purchase: @preorder)
      @preorder.update_attribute(:itunes_enabled, true)
      request.env["HTTP_REFERER"] = edit_admin_preorder_url(@preorder)

      all = {
        start_date: Time.now.to_s,
        preview_songs: true
      }

      itunes = {
        instant_grat_songs: [],
        variable_price_id: store.variable_prices.last.id,
        preorder_price_id: store.variable_prices.last.id,
        track_variable_price_id: store.variable_prices.last.id
      }

      all_received = jsonify_hash(all)
      itunes_received = jsonify_hash(itunes)

      expect(PreorderPurchase).to receive(:find).and_return(@preorder)
      expect(@preorder).to receive(:admin_update).with(all_received, itunes_received)
      expect_any_instance_of(PetriBundle).to receive(:add_salepoints).with([salepoint], actor: @login_as.name, message: "Metadata update for admin preorder data update").and_return(nil)
      put :update, params: { id: @preorder.id, all: all, itunes: itunes }

      expect(response).to be_redirect
    end

    it "should update the correct attributes when Google is enabled only" do
      @preorder.update_attribute(:google_enabled, true)
      store = Store.find_by(short_name: "Google")
      salepoint = TCFactory.create(:salepoint, store: store, salepointable: @album)
      salepoint_preorder_data = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, paid_at: Time.now, preorder_purchase: @preorder)
      @preorder.update_attribute(:itunes_enabled, true)
      request.env["HTTP_REFERER"] = edit_admin_preorder_url(@preorder)

      all = {
        start_date: Time.now.to_s,
        preview_songs: true
      }

      all_received = jsonify_hash(all)

      expect(PreorderPurchase).to receive(:find).and_return(@preorder)
      expect(@preorder).to receive(:admin_update).with(all_received, {})
      expect_any_instance_of(PetriBundle).to receive(:add_salepoints).with([salepoint], actor: @login_as.name, message: "Metadata update for admin preorder data update").and_return(nil)
      put :update, params: { id: @preorder.id, all: all }

      expect(response).to be_redirect
    end

    it "should update the correct attributes when iTunesWW and Google are enabled" do
      stores = [Store.find_by(short_name: "iTunesWW"), Store.find_by(short_name: "Google")]
      salepoints = []
      itunes = {}
      stores.each do |store|
        salepoint = TCFactory.create(:salepoint, store: store, salepointable: @album)
        salepoints << salepoint
        salepoint_preorder_data = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, paid_at: Time.now, preorder_purchase: @preorder)
        request.env["HTTP_REFERER"] = edit_admin_preorder_url(@preorder)

        itunes = {
          instant_grat_songs: [],
          variable_price_id: store.variable_prices.last.id,
          preorder_price_id: store.variable_prices.last.id,
          track_variable_price_id: store.variable_prices.last.id
        } if store.short_name == "iTunesWW"
      end
      @preorder.update_attribute(:itunes_enabled, true)
      @preorder.update_attribute(:google_enabled, true)

      all = {
        start_date: Time.now.to_s,
        preview_songs: true
      }

      all_received = jsonify_hash(all)
      itunes_received = jsonify_hash(itunes)

      expect(PreorderPurchase).to receive(:find).and_return(@preorder)
      expect(@preorder).to receive(:admin_update).with(all_received, itunes_received)
      expect_any_instance_of(PetriBundle).to receive(:add_salepoints).with(salepoints, actor: @login_as.name, message: "Metadata update for admin preorder data update").and_return(nil)

      put :update, params: { id: @preorder.id, all: all, itunes: itunes }
      expect(response).to be_redirect
    end

    it "should create instant grat tracks" do
      songs = create_list(:song, 4, album: @album)
      @album.songs = songs
      store = Store.find_by(short_name: "iTunesWW")
      salepoint = TCFactory.create(:salepoint, store: store, salepointable: @album)
      salepoint_preorder_data = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, paid_at: Time.now, preorder_purchase: @preorder)
      @preorder.update_attribute(:itunes_enabled, true)
      request.env["HTTP_REFERER"] = edit_admin_preorder_url(@preorder)

      all = {
        start_date: Time.now.strftime('%m/%d/%Y'),
        preview_songs: true
      }

      itunes = {
        instant_grat_songs: [@album.songs.first.id, @album.songs.second.id],
        variable_price_id: store.variable_prices.last.id,
        preorder_price_id: store.variable_prices.last.id,
        track_variable_price_id: store.variable_prices.last.id
      }

      expect {
        put :update, params: { id: @preorder.id, all: all, itunes: itunes }
      }.to change(PreorderInstantGratSong, :count).by(2)
    end

    it "should delete instant grat tracks" do
      songs = create_list(:song, 4, album: @album)
      @album.songs = songs
      store = Store.find_by(short_name: "iTunesWW")
      salepoint = TCFactory.create(:salepoint, store: store, salepointable: @album)
      salepoint_preorder_data = TCFactory.create(:salepoint_preorder_data, salepoint: salepoint, paid_at: Time.now, preorder_purchase: @preorder)
      @preorder.update_attribute(:itunes_enabled, true)
      request.env["HTTP_REFERER"] = edit_admin_preorder_url(@preorder)

      all = {
        start_date: Time.now.strftime('%m/%d/%Y'),
        preview_songs: true
      }

      itunes = {
        instant_grat_songs: [@album.songs.first.id, @album.songs.second.id],
        variable_price_id: store.variable_prices.last.id,
        preorder_price_id: store.variable_prices.last.id,
        track_variable_price_id: store.variable_prices.last.id
      }

      put :update, params: { id: @preorder.id, all: all, itunes: itunes }
      itunes[:instant_grat_songs] = []

      expect {
        put :update, params: { id: @preorder.id, all: all, itunes: itunes }
      }.to change(PreorderInstantGratSong, :count).by(-2)
    end
  end
end
