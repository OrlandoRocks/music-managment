require "rails_helper"

describe Admin::PetriController, "when using the mass takedown tool" do
  before(:each) do
    @person = TCFactory.build_person(:force_is_administrator => true)
    @album = TCFactory.create(:album)
    Store.all.each do |store|
      TCFactory.create(
        :salepoint,
        store: store,
        album: @album,
        variable_price: store.variable_prices.try(:first)
      )
    end

    login_as(@person)
  end

  before(:each) do
    Sns::Notifier.instance_variable_set("@sns_client", nil)
    Sns::Notifier.instance_variable_set("@topic", nil)
  end

  context "when retrying distributions" do
    context "when an admin does not supply album_ids_or_upcs" do
      it "should return a flash error message" do
        post :retry_distributions_create, params: { store_ids: ["1"] }
        expect(flash[:error]).to eq "At least one valid Album Id or UPC is required."
      end
    end

    context "when an admin does not supply store_ids" do
      it "should return a flash error message" do
        post :retry_distributions_create, params: { album_ids_or_upcs: ["1"] }
        expect(flash[:error]).to eq "At least one store is required."
      end
    end

    context "when an admin supplies album_ids and store_ids" do
      let!(:album_1) { FactoryBot.create(:album, id: 1) }
      let!(:album_2) { FactoryBot.create(:album, id: 2) }
      let!(:album_3) { FactoryBot.create(:album, id: 3) }
      let(:store_ids)         { nil }
      let(:metadata_only)     { nil }
      let(:daily_retry_limit) { nil }
      let(:itunes)            { nil }
      let(:import_csv)        { nil }
      let(:filename)          { nil }
      let(:retry_in_batches)  { nil }
      let(:retry_in_batches_bool)  { false }
      let(:use_sns_only)      { false }
      let(:use_sns_only_bool) { false }
      let(:job_name)          { "" }

      let(:params) do
        { album_ids_or_upcs: "1,2,3",
          store_ids:         [2,3],
          itunes:            itunes,
          metadata_only:     metadata_only,
          retry_in_batches:  retry_in_batches,
          daily_retry_limit: daily_retry_limit,
          import_csv:        import_csv,
          filename:          filename,
          job_name:          job_name,
          use_sns_only:      use_sns_only }
      end
      let(:mass_retry_params) do
        { person_id:         @person.id,
          album_ids_or_upcs: ["1","2","3"],
          store_ids:         store_ids,
          metadata_only:     metadata_only,
          retry_in_batches:  retry_in_batches_bool,
          daily_retry_limit: daily_retry_limit,
          remote_ip:         request.remote_ip,
          job_name:          job_name,
          use_sns_only:      use_sns_only_bool
         }
      end

      context "when retrying distributions with 'metadata_only'" do
        let(:store_ids) { ["2","3"] }
        let(:metadata_only) { "on" }

        let(:sns) { Aws::SNS::Resource.new(client: Aws::SNS::Client.new(stub_responses: true)) }

        before(:each) do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow(Aws::SNS::Resource).to receive(:new).and_return(sns)
        end

        it "should return a flash success message" do
          post :retry_distributions_create, params: params
          expect(flash[:success]).to eq "Successfully retrying distributions"
        end

        it "should call the Distribution::MassRetryForm with the correct params" do
          expect(Distribution::MassRetryForm).to receive(:new).with(mass_retry_params).and_call_original
          post :retry_distributions_create, params: params
          expect(response).to redirect_to admin_petri_retry_distributions_path
        end

        context "with use SNS feature" do

          let(:use_sns_only) { 'on' }
          let(:use_sns_only_bool) { true }

          it "should return a SNS Specific flash success message" do
            post :retry_distributions_create, params: params
            expect(flash[:success]).to eq "Successfully retrying distributions with only TC-Distributor SNS"
          end

          it 'enqueues retry' do
            new_params = {
              album_ids_or_upcs: "1,2,3",
              store_ids:         [2,3],
              metadata_only:     metadata_only,
              daily_retry_limit: nil,
              use_sns_only:      'on',
              job_name: ""
            }

            expect_any_instance_of(Distribution::MassRetryForm).to receive(:enqueue_mass_retry_worker).once.and_call_original
            expect(Distribution::MassRetryWorker).to receive(:perform_in).with(0.hours,mass_retry_params).once.and_call_original

            post :retry_distributions_create, params: new_params
          end

          it 'sends SNS notification' do
            expect_any_instance_of(Distribution::MassRetrier).to receive(:send_sns_notification).with(any_args).once
            Distribution::MassRetrier.mass_retry(mass_retry_params.with_indifferent_access)
          end

        end
      end

      context "when retrying distributions in batches" do
        let(:store_ids)             { ["2","3"] }
        let(:retry_in_batches)      { "true" }
        let(:retry_in_batches_bool) { true }
        let(:daily_retry_limit)     { "2" }

        context "When admin does not provide a job name" do
          it "should return a flash message" do
            post :retry_distributions_create, params: params
            expect(flash[:error]).to eq "A Valid Job name is required if retrying in batches."
          end
        end

        context "When Admin provides a Job name" do
          let(:job_name) { "test_job" }

          it "should call the Distribution::MassRetryForm with the correct params" do
            expect(Distribution::MassRetryForm).to receive(:new).with(mass_retry_params).and_call_original
            post :retry_distributions_create, params: params
            expect(response).to redirect_to admin_petri_retry_distributions_path
          end
        end
      end

      context "when retrying an itunes distributions" do
        let(:store_ids) { ["2", "3", 1, 2, 3, 4, 5, 6, 22, 29, 30, 34, 36] }
        let(:itunes)    { "1" }


        let(:sns) { double() }
        let(:topic) { double() }

        before(:each) do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow(Aws::SNS::Resource).to receive(:new).and_return(sns)
          allow(sns).to receive(:topic).and_return(topic)
          allow(topic).to receive(:publish)

          topic = sns.topic('mass_retry')
        end

        it "should call the Distribution::MassRetryForm with the correct params" do
          expect(Distribution::MassRetryForm).to receive(:new).with(mass_retry_params).and_call_original
          post :retry_distributions_create, params: params
          expect(response).to redirect_to admin_petri_retry_distributions_path
        end
      end

      context "when importing album_ids_or_upcs with a csv" do
        let(:store_ids)  { ["2", "3"] }
        let(:import_csv) { "on" }
        let(:filename)   { fixture_file_upload("/album_ids.csv", 'text/csv') }

        let(:sns) { double() }
        let(:topic) { double() }

        before(:each) do
          allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
          allow(Aws::SNS::Resource).to receive(:new).and_return(sns)
          allow(sns).to receive(:topic).and_return(topic)
          allow(topic).to receive(:publish)

          topic = sns.topic('mass_retry')
        end


        it "should call the Distribution::MassRetryForm with the correct params" do
          expect(Distribution::MassRetryForm).to receive(:new).with(mass_retry_params).and_call_original
          post :retry_distributions_create, params: params
          expect(response).to redirect_to admin_petri_retry_distributions_path
        end
      end
    end
  end
end
