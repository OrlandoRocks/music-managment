require "rails_helper"

describe Admin::TakedownsController, type: :controller do

  before(:each) do
    Sns::Notifier.instance_variable_set("@sns_client", nil)
    Sns::Notifier.instance_variable_set("@topic", nil)
    allow_any_instance_of(Store).to receive(:paused?).and_return false
  end

  it "sends SNS notifications for takedowns" do
    album = create(:album, :with_salepoints, :with_petri_bundle)
    album_store_ids = album.salepoints.pluck(:store_id)
    excluded_store_ids = Store::EXCLUDED_FROM_NOTIFICATION_IDS
    valid_store_ids = (album_store_ids - excluded_store_ids).sort
    valid_store_ids.map { |store_id| Store.find(store_id).update(in_use_flag: true, is_active: true) }
    admin = create(:person, :with_role)
    login_as(admin)

    expected_message = {
      message: {
        album_id: album.id,
        stores: valid_store_ids.map{ |store_id| { id: store_id, delivery_type: 'takedown', delivered: false }},
        person_id: admin.id
      }.to_json
    }

    @sns_message = double
    @sns_client = instance_double(Aws::SNS::Resource)
    @topic = instance_double(Aws::SNS::Topic)

    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
    expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
    expect(@topic).to receive(:publish).with(expected_message)

    post :create, params: { album_id: album.id }
  end

  it 'sends SNS notifications for untakedown' do
    album = create(:album, :with_salepoints, :with_petri_bundle)
    album_store_ids = album.salepoints.pluck(:store_id)
    excluded_store_ids = Store::EXCLUDED_FROM_NOTIFICATION_IDS
    valid_store_ids = (album_store_ids - excluded_store_ids).sort
    valid_store_ids.map { |store_id| Store.find(store_id).update(in_use_flag: true, is_active: true) }
    admin = create(:person, :with_role)
    login_as(admin)

    sns_expected_message = {
      message: {
        album_id: album.id,
        stores: valid_store_ids.map{ |store_id| { id: store_id, delivery_type: 'untakedown', delivered: false }},
        person_id: admin.id
      }.to_json
    }

    @sns_message = double
    @sns_client = instance_double(Aws::SNS::Resource)
    @topic = instance_double(Aws::SNS::Topic)

    allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    expect(Aws::SNS::Resource).to receive(:new).and_return(@sns_client)
    expect(@sns_client).to receive(:topic).with(any_args).and_return(@topic)
    expect(@topic).to receive(:publish).with(sns_expected_message)

    post :destroy, params: { album_id: album.id }
  end
end
