require "rails_helper"

describe Admin::FraudController do
  let(:person_without_pub)  { create(:person) }
  let(:person_with_pub)     { create(:person) }
  let(:account)             { create(:account, person: person_with_pub) }
  let(:composer)            { create(:composer, account: account, person: person_with_pub) }
  let(:purchase) do
    create(
      :purchase,
      related_type: "Composer",
      related_id:   composer.id,
      person_id:    person_with_pub.id,
      product_id:   Product.find_products_for_country(
        person_with_pub.country_domain,
        :songwriter_service
      ).first
    )
  end
  let(:invoice) { create(:invoice, person: person_with_pub) }

  before do
    login_as(create(:person, :admin))
    purchase.update(invoice_id: invoice.id)
    person_with_pub.mark_as_suspicious!(create(:note), "Credit Card Fraud")
    person_without_pub.mark_as_suspicious!(create(:note), "Credit Card Fraud")
  end

  describe "#index" do
    context "when pub admin selector is set to 'All'" do
      it "should return people regardless of pub admin status" do
        get :index, params: { pub_admin: "All" }

        expect(assigns(:people)).to include person_with_pub
        expect(assigns(:people)).to include person_without_pub
      end
    end

    context "when pub admin selector is set to 'Yes'" do
      it "should return people with a pub admin status" do
        get :index, params: { pub_admin: "Yes" }

        expect(assigns(:people)).to include person_with_pub
        expect(assigns(:people)).not_to include person_without_pub
      end
    end

    context "when pub admin selector is set to 'No'" do
      it "should return people without a pub admin status" do
        get :index, params: { pub_admin: "No" }

        expect(assigns(:people)).to include person_without_pub
        expect(assigns(:people)).not_to include person_with_pub
      end
    end

    context "when a status and status date range are set" do
      it "should return people with that status and a status_updated_at within the date range" do
        person = create(:person)
        person.lock!("Was very rude to customer service", false)

        params = {
          d: {
            status: "Locked",
            from_date: (2.weeks.ago).strftime("%m/%d/%Y"),
            to_date: (2.weeks.from_now).strftime("%m/%d/%Y"),
          },
          pub_admin: "All"
        }
        get :index, params: params

        expect(assigns(:people)).to include person
      end
    end
  end
end
