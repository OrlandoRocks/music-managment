require "rails_helper"

describe Admin::InventoriesController, type: :controller do

  before(:each) do
    @login_as = Person.find_by_name!("Tunecore Admin")
    login_as(@login_as)
  end

  it "should not include songwriter service" do

    #Purchase songwriter service as a tunecore admin
    product = TCFactory.generate_songwriter_product

    composer = TCFactory.build_composer(:person_id => @login_as.id, :agreed_to_terms => '1')

    purchase = Product.add_to_cart(@login_as, composer)
    invoice = Invoice.factory(@login_as, purchase)
    invoice.settlement_received(composer, purchase.cost_cents)
    invoice.settled!

    get :index, params: { person_id: @login_as.id }
    assert_response :success, @response.body

    #Assert that this songwriter service inventory use does not show up
    assert assigns[:inventories].blank?
    assert assigns[:inventories_all].blank?

  end

end
