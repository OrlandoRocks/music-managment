require "rails_helper"

describe Admin::StoresController do
  let(:admin) { create(:person, :with_role) }

  describe "#index" do
    it "lists all the stores with their converter class" do
      login_as(admin)
      get :index
      expect(response.status).to eq(200)
    end
  end
end
