require "rails_helper"

describe Admin::AgingAccountsController, type: :controller do
  before (:each) do
    @form_params = { user_ids: "123", date: {month: "10", year: "2016" } }
    person = FactoryBot.create(:person, :with_role, role_name: "TC Reporter")
    login_as(person)
  end

  context "#create" do
    before (:each) do
      allow(Tunecore::TcReporter::AgingAccounts).to receive(:s3_links).and_return({})
    end

    it "redirects to index page" do
      allow(TcReporter::ApiClient).to receive(:post).and_return(true)
      post :create, params: @form_params
      expect(response).to redirect_to(admin_aging_accounts_path)
    end

    it "sets notice only when post is successful" do
      allow(TcReporter::ApiClient).to receive(:post).and_return(true)
      post :create, params: @form_params
      expect(flash[:notice]).to eq("Reporting kicked off for 10-2016")
    end

    it "sets error when post is unsuccessful" do
      allow(TcReporter::ApiClient).to receive(:post).and_return(false)
      post :create, params: @form_params
      expect(flash[:error]).to eq("TC REPORTER is currently down.")
    end
  end
end
