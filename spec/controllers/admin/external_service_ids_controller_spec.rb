require "rails_helper"

describe Admin::ExternalServiceIdsController do
  before(:each) do
    person = create(:person, :with_role)
    login_as(person)
    fake_spotify_service = double(search_by_upc: [])
    allow(SpotifyService).to receive(:new).and_return(fake_spotify_service)
  end

  it "always returns success" do
    post "create"
    expect(response).to be_successful
  end

  it "assigns linkable if params are supplied" do
    album = create(:album)
    post "create", params: { linkable_type: "Album", linkable_id: album.id, external_service: "spotify_uris" }
    expect(assigns(:linkable)).to eq album
  end

  it "calls ExternalServiceIdsFetcher if linkable is assigned" do
    expect(ExternalServiceIds::ExternalServiceIdsFetcher).to receive(:fetch)
    album = create(:album)
    post "create", params: { linkable_type: "Album", linkable_id: album.id, external_service: "spotify_uris" }
  end

  it "does NOT assign linkable without params" do
    album = create(:album)
    post "create", params: { external_service: "spotify_uris" }
    expect(assigns(:linkable)).to eq nil
  end

  it "does NOT call ExternalServiceIdsFetcher if linkable is NOT assigned" do
    expect(ExternalServiceIds::ExternalServiceIdsFetcher).not_to receive(:fetch)

    album = create(:album)
    post "create", params: { external_service: "spotify_uris" }
  end
end
