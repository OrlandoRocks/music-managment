require "rails_helper"

describe Admin::ReportsController, type: :controller do
  let(:admin) { create(:person, :admin) }
  let(:account) { create(:account, person: admin) }
  let(:role) { create(:role, name: "Report Viewer") }

  before do
    admin.roles << role
    login_as(admin)
  end

  describe "view" do
    it "should load default report for get request" do
      params = {"report"=>{"name"=>"example_report", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2019", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"1", "genre_id"=>"1"}, "commit"=>"Run Report"}
      expect(Tunecore::Reports::Signups).to receive(:new).once
      get :view, params: params
      expect(@controller.instance_variable_get(:@page_title)).to eq('Choose Report')
    end

    it "should load default report for post request" do
      params = {"report"=>{"name"=>"example_report", "interval"=>"day", "start_date(3i)"=>"5", "start_date(2i)"=>"1", "start_date(1i)"=>"2019", "end_date(3i)"=>"6", "end_date(2i)"=>"1", "end_date(1i)"=>"2020", "use_end_date"=>"1", "genre_id"=>"1"}, "commit"=>"Run Report"}
      expect(Tunecore::Reports::Signups).to receive(:new).once
      get :view, params: params
      expect(@controller.instance_variable_get(:@page_title)).to eq('Choose Report')
    end
  end
end
