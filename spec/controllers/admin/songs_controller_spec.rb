require "rails_helper"

describe Admin::SongsController do
  describe "#play_song" do
    context "when the user has the appropriate permissions" do
      it "returns a successful response" do
        person = create(:person, :admin)
        song = create(:song)

        login_as(person)

        get :play_song, params: { song_id: song.id }

        expect(response).to be_successful
      end

      it "renders a json containing an audio url" do
        person = create(:person, :admin)
        song = create(:song)

        login_as(person)

        get :play_song, params: { song_id: song.id }

        response_body = response.body

        json_body = JSON.parse(response_body)

        expect(json_body.keys).to include("song_url")
      end
    end

    context "when the user does not have the appropriate permissions" do
      it "returns an unsuccessful response" do
        person = create(:person)
        song = create(:song)

        login_as(person)

        get :play_song, params: { song_id: song.id }

        expect(response).to_not be_successful
      end
    end
  end
end
