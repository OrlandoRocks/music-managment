require "rails_helper"

describe Admin::AlbumsController, type: :controller do

  before(:each) do
    Sns::Notifier.instance_variable_set("@sns_client", nil)
    Sns::Notifier.instance_variable_set("@topic", nil)
    resp = instance_double(DistributorAPI::DistributorAPIResponse,
      :data= => {},
      :http_body= => {},
      :http_headers= => {},
      :http_status= => 200,
      :http_body= => {},
      :http_body => {})
    allow(DistributorAPI::Release).to receive(:list).with(any_args).and_return([resp, {}])
    allow(DistributorAPI::ReleasesStore).to receive(:list).with(any_args).and_return([resp, {}])
  end

  context "when searching for an album" do
    before(:each) do
      @person = create(:person, :admin)
      login_as(@person)
    end

    it "should return an album with artwork" do
      person  = create(:person)
      artist  = create(:artist, name: person.name)
      album   = create(:album, :with_artwork, person: person, creatives: ["name" => person.name, "role" => "primary_artist"])

      get 'search', params: { search: "album|artist", term: "artist:#{album.person.name}" }
      expect(assigns(:search_items)).to eq ([album])
    end
  end

  context "when being accessed by non-administrators" do
    before(:each) do
      @person = create(:person)
      login_as(@person)
    end

    it "GET 'show' should not be successful" do
      album = create(:album, person: @person)
      get 'show', params: { id: album.id }
      expect(response).not_to be_successful
    end

    it "GET 'index' should not be successful" do
      get 'index'
      expect(response).not_to be_successful
    end
  end

  context "when trying to mark a song as free by non Distribution Managers" do
    before(:each) do
      @person = create(:person, :admin)
      allow(@person).to receive(:has_role?).with("Distribution Manager").and_return(false)
      login_as(@person)

      allow(Person).to receive(:find).and_return(@person)

      @album = create(:album, person: @person)

      @petri_bundle = PetriBundle.new
      @old_bundles = allow(PetriBundle).to receive( :find).and_return([@petri_bundle])

      @product_purchase_item = ProductPurchaseItem.new
      @purchase_items = allow(ProductPurchaseItem).to receive(:find).and_return([@product_purchase_item])

      renewal = Renewal.new
      @renewal = allow(Renewal).to receive(:renewal_for).and_return(renewal)
    end

    it "GET 'show' should load rights" do
      get 'show', params: { id: @album.id }
      expect(assigns(:user_can_mark_song_as_free)).to eql(false)
    end
  end

  context "when trying to mark a song as free by a Distribution Manager" do
    before(:each) do
      @person = create(:person, :admin)
      allow(@person).to receive(:has_role?).with("Distribution Manager").and_return(true)
      allow(@person).to receive(:is_permitted_to?).and_return(true)
      login_as(@person)

      allow(Person).to receive(:find).and_return(@person)

      @album = create(:album, person: @person)

      @petri_bundle = PetriBundle.new
      @old_bundles = allow(PetriBundle).to receive( :find).and_return([@petri_bundle])

      @product_purchase_item = ProductPurchaseItem.new
      @purchase_items = allow(ProductPurchaseItem).to receive(:find).and_return([@product_purchase_item])

      renewal = Renewal.new
      @renewal = allow(Renewal).to receive(:renewal_for).and_return(renewal)
    end

    it "GET 'show' should load rights" do
      get 'show', params: { id: @album.id }
      expect(assigns(:user_can_mark_song_as_free)).to eql(true)
    end
  end

  context "when finalizing and unfinalizing an album" do
    before(:each) do
      @person = create(:person, :admin)
      login_as(@person)
      @album = create(:album, legal_review_state: "DO NOT REVIEW")
    end

    describe "finalize" do
      context "album is unfinalized but ready to be finalized" do
        before(:each) do
          allow(Album).to receive(:find).and_return(@album)
          expect(@album.finalized_at).to eq(nil)
          allow(@album).to receive(:should_finalize?).and_return(true)
          allow(@album).to receive(:already_paid?).and_return(true)
        end

        it "should finalize the album and all salepoints" do
          get 'finalize', params: { id: @album.id }
          expect(@album.finalized_at.present?).to eq(true)
        end

        it 'marks album for review if requested' do
          get 'finalize', params: { id: @album.id, review: "true" }
          expect(@album.legal_review_state).to eq("NEEDS REVIEW")
        end

        it 'marks albums with state of last complete review audit' do
          allow(@album).to receive(:last_review_state).and_return("APPROVED")
          allow(@album.review_audits).to receive(:empty?).and_return(false)
          get 'finalize', params: { id: @album.id, review: "false" }
          expect(@album.reload.legal_review_state).to eq("APPROVED")
        end
      end
    end

    describe "unfinalize" do
      context "album is finalized" do
        it "should unfinalize the album and all salepoints" do
          album = create(:album, :finalized)
          allow(Album).to receive(:find).and_return(album)

          expect(album).to receive(:update).with({finalized_at: nil, created_with: 'songwriter', created_with_songwriter: true})
          get 'unfinalize', params: { id: album.id }
        end
      end
    end

    describe "POST mass_unfinalize_finalize" do

      context "selected_action: FINALIZE" do
        before(:each) do
          @album, @album2 = create_list(:album, 2)
          [@album, @album2].each{|a| allow(a).to receive(:already_paid?).and_return(true)}
          [@album, @album2].each{|a| allow(a).to receive(:should_finalize?).and_return(true)}
          finder = BulkAlbumFinder.new([@album, @album2].map(&:id).join(", "))
          allow(BulkAlbumFinder).to receive(:new).and_return(finder)
          allow(finder).to receive(:execute).and_return([@album, @album2])
        end

        it "finalizes a collection of albums" do
          post "mass_unfinalize_finalize", params: { album_ids: "#{@album.id} #{@album2.id}", selected_action: "finalize", review: "false" }
          expect(@album.finalized_at.present?).to eq(true)
          expect(@album2.finalized_at.present?).to eq(true)
        end

        it "marks a collection of albums for review" do
          post "mass_unfinalize_finalize", params: { album_ids: "#{@album.id} #{@album2.id}", selected_action: "finalize", review: "true" }
          expect(@album.legal_review_state).to eq("NEEDS REVIEW")
          expect(@album2.legal_review_state).to eq("NEEDS REVIEW")
        end

        it "marks a collection of albums with state of last completed review" do
          allow(@album).to receive(:last_review_state).and_return("APPROVED")
          allow(@album.review_audits).to receive(:empty?).and_return(false)
          allow(@album2).to receive(:last_review_state).and_return("APPROVED")
          allow(@album2.review_audits).to receive(:empty?).and_return(false)
          post :mass_unfinalize_finalize, params: { album_ids: "#{@album.id} #{@album2.id}", selected_action: "finalize", review: "false" }
          expect(@album.reload.legal_review_state).to eq("APPROVED")
          expect(@album2.reload.legal_review_state).to eq("APPROVED")
        end
      end

      context "selected_action: UNFINALIZE" do
        before(:each) do
          @album, @album2 = create_list(:album, 2, :finalized)
          [@album, @album2].each{|a| allow(a).to receive(:already_paid?).and_return(true)}
          [@album, @album2].each{|a| allow(a).to receive(:should_finalize?).and_return(true)}
          finder = BulkAlbumFinder.new([@album, @album2].map(&:id).join(", "))
          allow(BulkAlbumFinder).to receive(:new).and_return(finder)
          allow(finder).to receive(:execute).and_return([@album, @album2])
        end

        it "unfinalizes a collection of albums" do
          expect(@album).to receive(:update).with({finalized_at: nil, created_with: 'songwriter', created_with_songwriter: true})
          expect(@album2).to receive(:update).with({finalized_at: nil, created_with: 'songwriter', created_with_songwriter: true})
          post "mass_unfinalize_finalize", params: { album_ids: "#{@album.id} #{@album2.id}", selected_action: "unfinalize", review: "false" }
        end

        it "unfinalizes a collection of albums" do
          post 'mass_unfinalize_finalize', params: { album_ids: "#{@album.id} #{@album2.id}", selected_action: "unfinalize", review: 'false' }
          expect(@album.finalized?).to eq false
          expect(@album2.finalized?).to eq false
        end

        it "marks albums as DO NOT REVIEW" do
          album, album2 = create_list(:album, 2, :finalized)
          post 'mass_unfinalize_finalize', params: { album_ids: "#{album.id} #{album2.id}", selected_action: "unfinalize", review: 'false' }
          expect(album.legal_review_state).to eq("DO NOT REVIEW")
          expect(album2.legal_review_state).to eq("DO NOT REVIEW")
        end
      end
    end
  end

  describe "unfinalize" do
    before(:each) do
      @person = create(:person, :admin)
      login_as(@person)
    end

    context "album is finalized" do
      it "should unfinalize the album and all salepoints" do
        album = create(:album, :finalized)
        get :unfinalize, params: { id: album.id }
        expect(album.reload.finalized?).to eq false
        expect(album.created_with_songwriter?).to eq true
      end
    end
  end

  describe Admin::AlbumsController, "change_album_format_flag" do
    before(:each) do
      @person = create(:person)
      @person.roles << Role.find_by(name: "Admin")
      login_as(@person)
      @en_album = create(:album)
      @fr_album = create(:album, language_code: "fr")
      allow(PetriBundle).to receive(:recreate!).and_return(TCFactory.create(:petri_bundle, album: @en_album))
      allow(PetriBundle).to receive(:recreate!).and_return(TCFactory.create(:petri_bundle, album: @fr_album))
    end

    context 'albums outside of non formattable language codes' do
      it "turns off auto format" do
        @en_album.update_attribute :allow_different_format, false
        get 'change_album_format_flag', params: { id: @en_album.id }
        expect(@en_album.reload.allow_different_format).to be_truthy
      end

      it "turns on auto format" do
        @en_album.update_attribute :allow_different_format, true
        get 'change_album_format_flag', params: { id: @en_album.id }
        expect(@en_album.reload.allow_different_format).to be_falsey
      end

      it 'sets flash with success message' do
        get 'change_album_format_flag', params: { id: @en_album.id }
        expect(flash[:success]).to eq("Album and song names are#{' not' if @en_album.reload.allow_different_format } auto-formatted")
      end
    end

    context 'albums with non formattable language codes' do
      it 'turns off auto format if on' do
        @fr_album.update_attribute :allow_different_format, false
        get 'change_album_format_flag', params: { id: @fr_album.id }
        expect(@fr_album.reload.allow_different_format).to be_truthy
      end

      it 'keeps auto format off' do
        @fr_album.update_attribute :allow_different_format, true
        get 'change_album_format_flag', params: { id: @fr_album.id }
        expect(@fr_album.reload.allow_different_format).to be_truthy
      end

      it 'sets error message' do
        @fr_album.update_attribute :allow_different_format, true
        get 'change_album_format_flag', params: { id: @fr_album.id }
        expect(flash[:error]).to eq("Auto-formatting is not available for this album because it's language code is '#{@fr_album.reload.language_code}'")
      end
    end

    it 'redirects admin to the album admin page' do
      get 'change_album_format_flag', params: { id: @en_album.id }
      expect(response).to redirect_to(admin_album_path(@en_album))
    end
  end

  context 'salepoint_takedown' do
    it 'should call sns notification for takedown' do
      @person = create(:person)
      @person.roles << Role.find_by(name: "Admin")
      login_as(@person)

      album = create(:album)
      store = create(:store)
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, salepointable: album, store: store)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(Sns::Notifier).to receive(:perform).with(
        topic_arn: ENV.fetch('SNS_RELEASE_TAKEDOWN_URGENT_TOPIC'),
        album_ids_or_upcs: [salepoint.salepointable_id],
        store_ids: [salepoint.store_id],
        delivery_type: 'takedown',
        person_id: @person.id
      )

      post :salepoint_takedown, params: { id: salepoint.id }
    end
  end

  context 'salepoint_takedown' do
    it 'should call sns notification for untakedown' do
      @person = create(:person)
      @person.roles << Role.find_by(name: "Admin")
      login_as(@person)

      album = create(:album)
      store = create(:store)
      create(:salepointable_store, store: store)
      salepoint = create(:salepoint, salepointable: album, store: store)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      expect(Sns::Notifier).to receive(:perform).with(
        topic_arn: ENV.fetch('SNS_RELEASE_UNTAKEDOWN_URGENT_TOPIC'),
        album_ids_or_upcs: [salepoint.salepointable_id],
        store_ids: [salepoint.store_id],
        delivery_type: 'untakedown',
        person_id: @person.id
      )

      post :remove_salepoint_takedown, params: { id: salepoint.id }
    end
  end
end

describe Admin::AlbumsController, "song rights" do
  before(:each) do
    @song = create(:song)
    # Song.stub(:find).and_return(@song)
    allow(SalepointSong).to receive(:takedown).with(@song).and_return(true)
    allow(SalepointSong).to receive(:remove_takedown).with(@song).and_return(true)
    @admin = create(:person, :with_role)
    login_as(@admin)
  end

  describe "#salepoint_song_takedown" do
    it "takes down an album by the administrator" do
      get :salepoint_song_takedown, params: { id: @song.id }

      expect(Note.where(subject: "Rights Removed")).to exist
    end

    it "creates a ytsr track takedown note" do
      get :salepoint_song_takedown, params: { id: @song.id }

      expect(@song.notes.exists?(subject: "Youtube Sound Recording Takedown")).to be_truthy
    end
  end

  describe "#salepoint_song_remove_takedown" do
    it "removes takedown of an album by the administrator" do
      get :salepoint_song_remove_takedown, params: { id: @song.id }

      expect(Note.where(subject: "Rights Reclaimed")).to exist
    end
  end

  describe "#create_copyright_document" do
    let(:person) { create(:person, country: "IN") }

    let!(:album) { create(:album, person: person) }

    context "load album" do
      it "should find albums of a user" do
        person.roles << Role.find_by(name: "Admin")
        login_as(person)

        post :create_copyright_document, params: { person_id: person.id, id: album.id }
        expect(response).to redirect_to(copyright_documents_admin_person_album_path)
      end

      it "should redirect to person path if album does not belong to user" do
        person.roles << Role.find_by(name: "Admin")
        login_as(person)
        album1 = create(:album)

        post :create_copyright_document, params: { person_id: person.id, id: album1.id }
        expect(response).to redirect_to(admin_person_path(person.id))
      end
    end

    context "validate_copyright_document" do
      it "should redirect to copyright_documents page if document is not a valid ones" do
        person.roles << Role.find_by(name: "Admin")
        login_as(person)
        mp3_file = Rack::Test::UploadedFile.new("spec/assets/track_90_seconds.mp3", "audio/mpeg")

        post :create_copyright_document, params: { person_id: person.id, id: album.id, copyright_document: mp3_file }
        expect(response).to redirect_to(copyright_documents_admin_person_album_path)
        expect(flash[:error]).to match(/File Format is not supported, kindly upload/)
      end
    end

    context "when uploading document" do
      it "should upload and redirect with success message" do
        person.roles << Role.find_by(name: "Admin")
        login_as(person)
        pdf_file = Rack::Test::UploadedFile.new("spec/assets/booklet.pdf", 'application/pdf')
        allow(CopyrightDocument).to receive(:upload_document).and_return(true)

        post :create_copyright_document, params: { person_id: person.id, id: album.id, copyright_document: pdf_file }
        expect(response).to redirect_to(copyright_documents_admin_person_album_path)
        expect(flash[:success]).to match(/CopyrightDocument Uploaded Successfully./)
      end

      it "should not upload and redirect with error message" do
        person.roles << Role.find_by(name: "Admin")
        login_as(person)
        pdf_file = Rack::Test::UploadedFile.new("spec/assets/booklet.pdf", 'application/pdf')
        allow(CopyrightDocument).to receive(:upload_document).and_return(false)

        post :create_copyright_document, params: { person_id: person.id, id: album.id, copyright_document: pdf_file }
        expect(response).to redirect_to(copyright_documents_admin_person_album_path)
        expect(flash[:error]).to match(/Failed to upload document, please contact support./)
      end
    end
  end

  describe ".set_album_timed_release_date" do
    let(:person) { create(:person, country: "IN") }
    let(:album) { create(:album, person: person) }

    it "should save datetime with timing scenario" do
      person.roles << Role.find_by(name: "Admin")
      login_as(person)

      post :set_album_timed_release_date, params: { id: album.id, timed_release_attributes: {timed_release_timing_scenario: 'absolute_time', golive_date: '2020-12-12 12:00' }, format: :js}
      expect(album.reload.golive_date.strftime("%F")).to eq('2020-12-12')
      expect(album.reload.sale_date.strftime("%F")).to eq('2020-12-12')
      expect(album.reload.timed_release_timing_scenario).to eq('absolute_time')
    end
  end

  describe ".set_album_timed_release_date" do
    let(:person) { create(:person, country: "IN") }
    let(:album) { create(:album, person: person) }

    it "should create note for golive_date change" do
      person.roles << Role.find_by(name: "Admin")
      login_as(person)

      post :set_album_timed_release_date, params: { id: album.id, timed_release_attributes: {timed_release_timing_scenario: 'absolute_time', golive_date: '2020-12-12 12:00' }, format: :js}
      expect(album.reload.golive_date.strftime("%F")).to eq('2020-12-12')
      expect(album.reload.sale_date.strftime("%F")).to eq('2020-12-12')
      expect(album.reload.timed_release_timing_scenario).to eq('absolute_time')
      expect(album.notes.where(subject: "Release date changed").count).to eql(1)
    end
  end

  describe ".set_album_sale_date" do
    let(:person) { create(:person, country: "IN") }
    let(:album) { create(:album, person: person) }

    it "should create note for sale_date change" do
      person.roles << Role.find_by(name: "Admin")
      login_as(person)

      post :set_album_sale_date, params: { id: album.id, value: "2022-06-24", format: :js}
      expect(album.reload.sale_date.strftime("%F")).to eq('2022-06-24')
      expect(album.reload.notes.where(subject: "Release date changed").count).to eql(1)
    end
  end

  describe ".set_album_orig_release_year" do
    let(:person) { create(:person, country: "IN") }
    let(:album) { create(:album, person: person) }

    it "should create note for orig_release_year change" do
      person.roles << Role.find_by(name: "Admin")
      login_as(person)

      post :set_album_orig_release_year, params: { id: album.id, value: "2022-06-23"}
      expect(album.reload.orig_release_year.strftime("%F")).to eq('2022-06-23')
      expect(album.reload.notes.where(subject: "Release date changed").count).to eql(1)
    end
  end
end
