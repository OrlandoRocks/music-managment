# frozen_string_literal: true

require "rails_helper"

describe Admin::Payout::TransferBatchActionsController do
  let!(:admin) { create(:person, :admin) }
  let!(:person) { create(:person, :with_balance, amount: 50_000) }

  before(:each) do
    admin.roles << Role.find_by(name: "Payout Service")
    login_as(admin)
  end

  describe "#create" do
    let!(:service_params) { { action: "approve", transfer_ids: ["101", "102"], person: admin, referrer_url: "http://test.host/admin/payout/batch_actions" } }

    it "redirects to admin dashboard when admin lacks necessary roles" do
      admin.roles.delete(Role.find_by(name: "Payout Service"))
      post :create, xhr: true, params: { commit: "approve", selected_transfers: [101] }
      expect(response).to redirect_to(admin_dashboard_index_path)
    end

    it "calls appropriate method in Payoneer::BatchActionsService" do
      expect(Payoneer::BatchActionsService).to receive(:send_actions)
        .with(service_params)
        .and_return([])

      post :create, xhr: true, params: { commit: "approve", selected_transfers: [101, 102] }
      expect(flash[:notice]).to eq('Successfully updated ["101", "102"]')
      expect(response).to redirect_to(home_path)
    end

    it "handles error" do
      expect(Payoneer::BatchActionsService).to receive(:send_actions)
        .with(service_params)
        .and_return(["boom", "boom"])

      post :create, xhr: true, params: { commit: "approve", selected_transfers: [101, 102] }
      expect(flash[:error]).to eq("boom,boom")
      expect(response).to redirect_to(home_path)
    end

    context "when auto-approved-withdrawals feature flag is set" do
      before(:each) do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
        allow(FeatureFlipper).to receive(:show_feature?).with(:auto_approved_withdrawals).and_return(true)
      end
      let!(:service_params) { { action: "approve", transfer_ids: ["101", "102"], person: admin, provider: "payoneer", referrer_url: "http://test.host/admin/payout/batch_actions" } }

      it "calls appropriate method in ApproveService" do
        expect(Transfers::ApproveService).to receive(:call)
          .with(service_params)
          .and_return({ success: true, errors: [] })

        post :create, xhr: true, params: { commit: "approve", selected_transfers: [101, 102] }
        expect(flash[:notice]).to eq('Successfully updated ["101", "102"]')
        expect(response).to redirect_to(home_path)
      end

      it "handles error" do
        expect(Transfers::ApproveService).to receive(:call)
          .with(service_params)
          .and_return({ success: false, errors: ["boom", "boom"] })

        post :create, xhr: true, params: { commit: "approve", selected_transfers: [101, 102] }
        expect(flash[:error]).to eq("boom,boom")
        expect(response).to redirect_to(home_path)
      end
    end
  end
end
