require "rails_helper"

describe Admin::Payout::TransferRejectionsController do
  describe "#create" do
    # This is broken. Action not found.
    xit "redirects to dashboard if admin doesnt have permission" do
      person = create(:person, :admin)
      person.roles << Role.find_by(name: "CMS")
      login_as(person)

      post :create, xhr: true

      expect(response.status).to eq 302
      expect(response).to redirect_to admin_dashboard_index_path
    end

    it "redirects to admin payout_transfer index if save successful" do
      person = create(:person, :admin)
      payout_provider = create(:payout_provider, person: person)
      payout_transfer = create(:payout_transfer, payout_provider: payout_provider)
      form = double(save: true)
      allow(PayoutTransfer::RejectionForm).to receive(:new).with(
        payout_transfer: payout_transfer
      ).and_return(form)
      person.roles << Role.find_by(name: "Payout Service")
      login_as(person)
      request.env["HTTP_REFERER"] = admin_payout_payout_transfers_path
      post :create, params: { payout_transfer_id: payout_transfer.client_reference_id }, xhr: true

      expect(response.status).to eq 302
      expect(response).to redirect_to admin_payout_payout_transfers_path
    end

    it "adds to flash has if save not successful" do
      person = create(:person, :admin)
      payout_provider = create(:payout_provider, person: person)
      payout_transfer = create(:payout_transfer, payout_provider: payout_provider)
      form = double(save: false, errors: double(full_messages: ["error message"]))
      allow(PayoutTransfer::RejectionForm).to receive(:new).with(
        payout_transfer: payout_transfer
      ).and_return(form)
      person.roles << Role.find_by(name: "Payout Service")
      login_as(person)
      request.env["HTTP_REFERER"] = admin_payout_payout_transfers_path
      post :create, params: { payout_transfer_id: payout_transfer.client_reference_id }, xhr: true

      expect(response.status).to eq 302
      expect(response).to redirect_to admin_payout_payout_transfers_path
      expect(flash[:error]).to eq form.errors.full_messages.join('&')
    end
  end
end
