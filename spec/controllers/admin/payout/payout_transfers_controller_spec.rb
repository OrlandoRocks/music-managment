require "rails_helper"

describe Admin::Payout::PayoutTransfersController do

  let!(:person) { create(:person, :admin) }

  describe "#index" do
    it "redirects to admin dashboard path if invalid params are sent in" do
      login_as(person)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)

      get :index, xhr: true, params: { tunecore_status: "submitted", filter_type: "abcd", filter_amount: "30.00" }

      expect(response.status).to eq(302)
      expect(response).to redirect_to(admin_dashboard_index_path)
    end
  end
end
