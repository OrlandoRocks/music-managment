require "rails_helper"

describe Admin::BlacklistedPaypalPayeesController do
  let(:person) { create(:person, :admin) }

  before(:each) do
    login_as(person)
    create_list(:blacklisted_paypal_payee, 10)
  end

  describe "#index" do
    context "with pagination" do
      it "displays the correct results" do
        expect(BlacklistedPaypalPayee::SearchService).to receive(:search).with(nil, "2", "2")
        get :index, params: {page: 2, per_page: 2}
      end
    end

    context "search by payee" do
      it "displays the correct results" do
        payee = BlacklistedPaypalPayee.first.payee
        expect(BlacklistedPaypalPayee::SearchService).to receive(:search).with(payee, nil, nil)
        get :index, params: { blacklisted_paypal_payee: payee }
      end
    end
  end

  describe "#create" do
    context "user does not have admin EFT role" do
      before(:each) do
        expect(person).to receive(:has_role?).with("EFT").and_return(false)
      end

      it "does not create payees" do
        expect{post :create}.to_not change{BlacklistedPaypalPayee.count}
      end
    end

    context "admin does have admin EFT role" do
      before(:each) do
        expect(person).to receive(:has_role?).with("EFT").and_return(true)
      end
      it "creates the new payees" do
        post :create, params: { bulk_blacklisted_paypal_payees: "new_guy@paypal.com, another_guy@paypal.com" }
        expect(BlacklistedPaypalPayee.count).to eq(12)
        expect(BlacklistedPaypalPayee.last.payee).to eq("another_guy@paypal.com")
      end
    end
  end

  describe "#destroy" do
    context "user does not have admin EFT role" do
      before(:each) do
        expect(person).to receive(:has_role?).with("EFT").and_return(false)
      end
      it "does not destroy payee" do
        delete :destroy, params: { id: 1 }
        expect(BlacklistedPaypalPayee.count).to eq(10)
      end
    end

    context "admin does have admin EFT role" do
      before(:each) do
        expect(person).to receive(:has_role?).with("EFT").and_return(true)
      end
      it "destroys the payees" do
        delete :destroy, params: { id: BlacklistedPaypalPayee.first.id }
        expect(BlacklistedPaypalPayee.count).to eq(9)
      end
    end
  end
end
