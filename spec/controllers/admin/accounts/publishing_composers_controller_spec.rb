require "rails_helper"

describe Admin::Accounts::PublishingComposersController, type: :controller do
  describe "#update" do
    before do
      admin = create(:person, :admin)
      login_as(admin)
    end

    context "when the publishing_composer is updated successfully" do
      it "sends publishing_composer data to RightsApp" do
        @person       = create(:person)
        @account      = create(:account, person: @person)
        publishing_composer = create(:publishing_composer, :active, :with_pub_admin_purchase, person: @person, account: @account, first_name: "Ron", last_name: "Artest")

        params = {
          id: publishing_composer.id,
          account_id: publishing_composer.account_id,
          publishing_composer: {
            first_name: "Metta",
            last_name: "World Peace",
            publisher_attributes: {
              name: "Fight Club",
              cae: "123456789",
              performing_rights_organization_id: 56
            }
          }
        }

        expect(PublishingAdministration::ApiClientServices::PublishingWriterService).to receive(:create_or_update).with(publishing_composer)

        put :update, params: params

        expect(publishing_composer.reload.first_name).to eq params[:publishing_composer][:first_name]
        expect(publishing_composer.last_name).to eq params[:publishing_composer][:last_name]
        expect(publishing_composer.publisher.name).to eq params[:publishing_composer][:publisher_attributes][:name]
        expect(publishing_composer.publisher.cae).to eq params[:publishing_composer][:publisher_attributes][:cae]
      end
    end

    context "when the publishing_composer is not updated" do
      it "does not send publishing_composer data to RightsApp" do
        publishing_composer = create(:publishing_composer)

        params = {
          id: publishing_composer.id,
          account_id: publishing_composer.account_id,
          publishing_composer: { cae: "12345" }
        }

        expect(PublishingAdministration::ApiClientServices::PublishingWriterService).not_to receive(:create_or_update).with(publishing_composer)

        put :update, params: params
      end
    end
  end
end
