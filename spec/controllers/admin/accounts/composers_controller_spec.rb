require "rails_helper"

describe Admin::Accounts::ComposersController, type: :controller do
  describe "#update" do
    before do
      admin = create(:person, :admin)
      login_as(admin)
    end

    context "when the composer is updated successfully" do
      it "sends composer data to RightsApp" do
        composer = create(:composer, first_name: "Ron", last_name: "Artest")

        params = {
          id: composer.id,
          account_id: composer.account_id,
          composer: {
            first_name: "Metta",
            last_name: "World Peace",
            publisher_attributes: {
              name: "Fight Club",
              cae: "123456789",
              performing_rights_organization_id: 56
            }
          }
        }

        expect(PublishingAdministration::WriterCreationWorker).to receive(:perform_async).with(composer.id)

        put :update, params: params

        expect(composer.reload.first_name).to eq params[:composer][:first_name]
        expect(composer.last_name).to eq params[:composer][:last_name]
        expect(composer.publisher.name).to eq params[:composer][:publisher_attributes][:name]
        expect(composer.publisher.cae).to eq params[:composer][:publisher_attributes][:cae]
      end
    end

    context "when the composer is not updated" do
      it "does not send composer data to RightsApp" do
        composer = create(:composer)

        params = {
          id: composer.id,
          account_id: composer.account_id,
          composer: { cae: "12345" }
        }

        expect(PublishingAdministration::WriterCreationWorker).not_to receive(:perform_async).with(composer.id)

        put :update, params: params
      end
    end
  end
end
