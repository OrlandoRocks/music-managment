require 'rails_helper'

describe Admin::MassAdjustmentsController, type: :controller do

  let(:admin) { create(:person, :admin, :with_role, role_name: "Mass Adjustments")}

  before(:each) do
    login_as admin
  end

  describe "#index" do
    it "changes batch statuses from processing to complete when applicable" do
      person = create(:person)
      batch1 = create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED)
      entry = create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch1, status: MassAdjustmentEntry::SUCCESS)
      batch2 = create(:mass_adjustment_batch, status: MassAdjustmentBatch::SCHEDULED)
      entry = create(:mass_adjustment_entry, person: person, mass_adjustment_batch: batch2, status: nil)

      get :index
      expect(batch1.reload.status).to eq(MassAdjustmentBatch::COMPLETED)
      expect(batch2.reload.status).to eq(MassAdjustmentBatch::SCHEDULED)
    end
  end

  describe "#new" do
    it "should redirect to list with flash message if batch is already scheduled" do
      person = create(:person)
      latest_batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::CONFIGURED)
      entry = create(:mass_adjustment_entry, person: person, mass_adjustment_batch: latest_batch, status: nil)

      get :new
      expect(response).to redirect_to(admin_mass_adjustments_path)
      expect(flash[:notice]).not_to be_nil
    end

    it "should show the new form if no batch is scheduled" do
      allow(MassAdjustmentBatch).to receive(:batch_in_flight?).and_return(false)
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "#create" do
    it "should redirect to list with flash message if batch is already scheduled" do
      person = create(:person)
      latest_batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::CONFIGURED)
      entry = create(:mass_adjustment_entry, person: person, mass_adjustment_batch: latest_batch, status: nil)

      post :create, params: { mass_adjustments_form: {} }
      expect(response).to redirect_to(admin_mass_adjustments_path)
      expect(flash[:notice]).not_to be_nil
    end

    it "should redirect to new if there are form errors" do
      allow(MassAdjustmentBatch).to receive(:batch_in_flight?).and_return(false)
      errors = double(:errors, full_messages: ["some error"])
      allow_any_instance_of(MassAdjustmentsForm).to receive(:save).and_return(true)
      allow_any_instance_of(MassAdjustmentsForm).to receive(:errors).and_return(errors)

      post :create, params: { mass_adjustments_form: {} }
      expect(response).to render_template("new")
    end

    it "should create a new batch" do
      batch = create(:mass_adjustment_batch)
      allow(MassAdjustmentBatch).to receive(:batch_in_flight?).and_return(false)
      errors = double(:errors, full_messages: [])
      allow_any_instance_of(MassAdjustmentsForm).to receive(:save).and_return(true)
      allow_any_instance_of(MassAdjustmentsForm).to receive(:errors).and_return(errors)
      allow_any_instance_of(MassAdjustmentsForm).to receive(:batch).and_return(batch)

      post :create, params: { mass_adjustments_form: {} }
      expect(response).to redirect_to(admin_mass_adjustment_path(batch.id))
    end
  end

  describe "#update" do
    it "should schedule the batch" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::NEW, allow_negative_balance: false)
      put :update, params: { id: batch.id, mass_adjustment_batch: {id: batch.id, allow_negative_balance: true, status: MassAdjustmentBatch::CONFIGURED} }

      updated_batch = MassAdjustmentBatch.find(batch.id)
      expect(updated_batch.allow_negative_balance).to be_truthy
      expect(updated_batch.status).to eq(MassAdjustmentBatch::CONFIGURED)

      expect(response).to redirect_to("/admin/mass_adjustments/#{batch.id}")
    end

    it "should cancel the batch" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::NEW, allow_negative_balance: false)
      put :update, params: { id: batch.id, mass_adjustment_batch: {id: batch.id, allow_negative_balance: true, status: MassAdjustmentBatch::CANCELLED} }

      updated_batch = MassAdjustmentBatch.find(batch.id)
      expect(updated_batch.status).to eq(MassAdjustmentBatch::CANCELLED)
      expect(response).to redirect_to("/admin/mass_adjustments/#{batch.id}")
    end

    it "should trigger scheduler service when the batch is approved" do
      batch = create(:mass_adjustment_batch, status: MassAdjustmentBatch::CONFIGURED, allow_negative_balance: false)
      expect(MassAdjustmentSchedulerService).to receive(:schedule)

      put :update, params: { id: batch.id, mass_adjustment_batch: {id: batch.id, status: MassAdjustmentBatch::APPROVED} }

      updated_batch = MassAdjustmentBatch.find(batch.id)
      expect(updated_batch.status).to eq(MassAdjustmentBatch::APPROVED)
    end
  end

  describe "#show" do
    # This only tests the response from logging in.
    xit "should show batch details" do
      batch = create(:mass_adjustment_batch)
      expect(response.status).to be(200)
    end
  end
end
