require "rails_helper"

describe Admin::Distribution::ErrorsController, type: :controller do
  before do
    login_as(Person.find_by_name!("Tunecore Admin"))
  end

  describe "#index" do
    it "should call the Distribution Error Form with the params passed in" do
      params = { start_date: "11/01/2017", end_date: "11/02/2017", filter_type: "store", states: nil }

      expected_params = params.merge(states: "")

      expect(Distribution::ErrorForm).to receive(:new).with(expected_params).and_call_original
      get :index, params: params
    end

    context "when the start date and end date are not sent into params" do
      it "defaults to nil" do
        params = { start_date: nil, end_date: nil, filter_type: "store", states: nil }
        expect(Distribution::ErrorForm).to receive(:new).with(params).and_call_original
        get :index, params: { filter_type: "store" }
      end
    end

    context "when filter type is not passed into params" do
      it "defaults to nil" do
        params = { start_date: nil, end_date: nil, filter_type: nil, states: nil }
        expect(Distribution::ErrorForm).to receive(:new).with(params).and_call_original
        get :index
      end
    end
  end

  describe "#create" do
    let(:distributions) { create_list(:distribution, 2) }
    let(:person)        { Person.find_by_name!("Tunecore Admin") }

    it "should retry each distribution passed into params" do
      distributions.each do |distribution|
        expect(Delivery::Retry).to receive(:retry).with(
          delivery:      distribution,
          person:        person,
          delivery_type: distribution.last_delivery_type
        )
      end
      post :create, params: { format: :js, distribution_ids: distributions.map(&:id) }
      expect(response).to render_template(:create)
    end
  end
end
