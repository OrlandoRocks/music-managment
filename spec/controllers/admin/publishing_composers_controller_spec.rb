require "rails_helper"

describe Admin::PublishingComposersController do
  before :each do
    admin = create(:person, :admin)
    create(:account, person: admin)
    role = create(:role, name: "Publishing Manager")
    admin.roles << role
    login_as(admin)
  end

  describe "#update" do
    it "updates the publishing_composer and sets the pro" do
      publishing_composer = create(
        :publishing_composer,
        :skip_cae_validation, 
        cae: nil, 
        performing_rights_organization_id: 56)

      params = {
        id: publishing_composer.id,
        publishing_composer: {
          cae: "123456789",
        }
      }

      put :update, params: params

      expect(publishing_composer.reload.cae).to eq params[:publishing_composer][:cae]
    end

    context "when the publishing_composer does has a publisher" do
      it "updates the publishing_composer and the publisher" do
        publishing_composer = create(
          :publishing_composer,
          cae: nil,
          performing_rights_organization_id: nil
        )

        params = {
          id: publishing_composer.id,
          publishing_composer: {
            cae: "123456789",
            performing_rights_organization_id: 1
          },
          publisher: {
            cae: "987654321",
            name: "Publisher Name",
            performing_rights_organization_id: 56
          }
        }

        put :update, params: params

        expect(publishing_composer.reload.publisher.cae).to eq params[:publisher][:cae]
        expect(publishing_composer.publisher.performing_rights_organization_id).to eq params[:publisher][:performing_rights_organization_id]
        expect(publishing_composer.publisher.name).to eq params[:publisher][:name]
      end
    end
  end

  describe ".download_client_import" do
    context ".date range more than a year" do
      it "should trigger a sidekiq job and redirect" do
        expect(PublishingClientImportReportWorker).to receive(:perform_async)
        get :download_client_import, params: { d: {from_date: '01/01/2018', to_date: '02/02/2019' }}
        expect(response).to redirect_to list_admin_publishing_composers_path
        expect(flash[:notice]).to eq("You'll receive a report in your mail shortly, kindly check your mail.")
      end

      it "should not trigger a sidekiq job" do
        get :download_client_import, params: { d: {from_date: '10/10/2018', to_date: '02/02/2019' }}
        expect(PublishingClientImportReportWorker).not_to receive(:perform_async)
      end
    end
  end
end
