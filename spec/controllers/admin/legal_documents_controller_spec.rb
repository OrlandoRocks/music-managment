require "rails_helper"

describe Admin::LegalDocumentsController do
  describe "#show" do
    context "when the current_user is NOT a publishing manager" do
      it "redirects to the legal document asset url" do
        person         = create(:person, :admin)
        composer       = create(:composer)
        legal_document = create(:legal_document, subject: composer)

        login_as(person)

        get :show, params: { id: legal_document.id }
        expect(response.status).to eq 404
      end
    end

    context "when the current_user is a publishing manager" do
      it "redirects to the legal document asset url" do
        person         = create(:person, :admin)
        composer       = create(:composer)
        legal_document = create(:legal_document, subject: composer)

        person.roles << Role.new(name: "Publishing Manager")

        login_as(person)

        s3_url = "https://s3.amazonaws.com/documents.tunecore.com/1"
        allow_any_instance_of(LegalDocument).to receive(:document_url).and_return(s3_url)

        get :show, params: { id: legal_document.id }
        expect(subject).to redirect_to s3_url
      end
    end
  end
end
