require "rails_helper"

describe Admin::YoutubeMoniesController, type: :controller do
  before(:each) do
    @admin = Person.find_by_name!("Super Admin")
    @admin.roles << Role.find_by(name: "YTM Approver")
    login_as(@admin)
  end

  describe "#setup_ytm_opt_out" do
    context "when passing the person id of someone with a purchased ytm product" do
      before(:each) do
        @ytm_user = FactoryBot.create(:person)
        @ytm = FactoryBot.create(:youtube_monetization, effective_date: Time.now, person: @ytm_user)
      end

      it "should set the action_title and action_type instance variables" do
        get :setup_ytm_opt_out, params: { person_id: @ytm_user.id }

        expect(assigns(:action_title)).to eq("Disable YTM")
        expect(assigns(:action_type)).to eq("disable")
      end
    end

    context "when passing the person id of someone without the ytm product" do
      before(:each) do
        @person = FactoryBot.create(:person)
      end

      it "should flash an error msg and redirect to admin dashboard" do
        get :setup_ytm_opt_out, params: { person_id: @person.id }

        assert_redirected_to admin_home_url
        assert flash[:error]
      end
    end

    context "when passing the person id of someone without a purchased ytm product" do
      before(:each) do
        @person = FactoryBot.create(:person)
        @ytm = FactoryBot.create(:youtube_monetization, person: @person, effective_date: nil)
      end

      it "should flash an error msg and redirect to admin dashboard" do
        get :setup_ytm_opt_out, params: { person_id: @person.id }

        assert_redirected_to admin_home_url
        assert flash[:error]
      end
    end
  end

  describe "#opt_out" do
    context "when passing in the person id of someone with a purchased ytm product" do
      before(:each) do
        @ytm_user = FactoryBot.create(:person)
        @ytm = FactoryBot.create(:youtube_monetization, effective_date: Time.now, person: @ytm_user)
      end

      it "should call #terminate_ytm on the user" do
        allow(Person).to receive(:find).and_return(@ytm_user)
        expect(@ytm_user).to receive(:terminate_ytm)
        get :opt_out, params: { note: {}, person: { action_type: 'disable' }, person_id: @ytm_user.id }
      end
    end
  end

  describe "#mass_takedown_tracks" do
    context "when the song has a youtube track_monetization" do
      context "when the track has a tunecore_isrc" do
        it "takes down that track_monetization" do
          distribution = create(:distribution, :with_songs)
          song = distribution.reload.petri_bundle.album.songs.first
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)

          expect(song.notes).to be_empty

          post :mass_takedown_tracks, params: { isrcs: song.isrc }

          track_monetization.reload

          expect(track_monetization.takedown_at).to_not eq(nil)
          expect(track_monetization.delivery_type).to eq("metadata_only")
          expect(track_monetization.transitions.find_by("transitions.message LIKE '%Delivering takedown%'"))
            .to be_truthy
        end

        it "creates a ytsr track takedown note" do
          distribution = create(:distribution, :with_songs)
          song = distribution.reload.petri_bundle.album.songs.first
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)

          expect(song.notes).to be_empty

          post :mass_takedown_tracks, params: { isrcs: song.isrc }

          note = song.reload.notes.first

          expect(note.related_id).to eq(song.id)
          expect(note.subject).to eq("Youtube Sound Recording Takedown")
        end
      end

      context "when the track has an optional_isrc" do
        it "takes down that track_monetization" do
          distribution = create(:distribution, :with_songs)
          song = distribution.reload.petri_bundle.album.songs.first
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)

          song.update_attribute(:optional_isrc, "123456789012")

          expect(song.notes).to be_empty

          post :mass_takedown_tracks, params: { isrcs: song.isrc }

          track_monetization.reload

          expect(track_monetization.takedown_at).to_not eq(nil)
          expect(track_monetization.delivery_type).to eq("metadata_only")
          expect(track_monetization.transitions.find_by("transitions.message LIKE '%Delivering takedown%'"))
            .to be_truthy
        end

        it "creates a ytsr track takedown note" do
          distribution = create(:distribution, :with_songs)
          song = distribution.reload.petri_bundle.album.songs.first
          store = Store.find_by(id: Store::YTSR_STORE_ID)
          track_monetization = create(:track_monetization, song: song, store: store)

          song.update_attribute(:optional_isrc, "123456789012")

          expect(song.notes).to be_empty

          post :mass_takedown_tracks, params: { isrcs: song.isrc }

          note = song.reload.notes.first

          expect(note.related_id).to eq(song.id)
          expect(note.subject).to eq("Youtube Sound Recording Takedown")
        end
      end
    end

    context "when the song does not have a youtube track_monetization" do
      it "does not create a ytsr track takedown note" do
        distribution = create(:distribution, :with_songs)
        song = distribution.reload.petri_bundle.album.songs.first

        expect(song.notes).to be_empty

        post :mass_takedown_tracks, params: { isrcs: song.isrc }

        expect(song.reload.notes).to be_empty
      end

      it "returns a flash error indicating that the song cannot be taken down as it was never delivered" do
        distribution = create(:distribution, :with_songs)
        song = distribution.reload.petri_bundle.album.songs.first

        expect(song.notes).to be_empty

        post :mass_takedown_tracks, params: { isrcs: song.isrc }

        flash_error = flash[:error]

        expect(flash_error).to include("Cannot takedown a song from ytsr that was not distributed")
        expect(flash_error).to include(song.isrc)
      end
    end
  end

  describe "#block" do
    let(:ytm_user) { create(:person) }

    before do
      create(:youtube_monetization, effective_date: Time.now, person: ytm_user)
      allow(Person).to receive(:find).and_return(ytm_user)
    end

    it "should block ytm service" do
      put :block, params: { person_id: ytm_user.id }

      expect(response).to redirect_to(admin_person_path(ytm_user.id))
      expect(ytm_user.ytm_blocked?).to be true
    end

    it "should create a note stating the YTSR service was blocked" do
      put :block, params: { person_id: ytm_user.id }

      note = ytm_user.notes.first
      expect(note.subject).to eq "YTSR Service Blocked"
    end
  end


  describe "#unblock" do
    let(:ytm_user) { create(:person) }

    before do
      create(:youtube_monetization, effective_date: Time.now, person: ytm_user, block_date: Date.today)
      allow(Person).to receive(:find).and_return(ytm_user)
    end

    it "should un-block ytm service" do
      expect(ytm_user.ytm_blocked?).to be true

      put :unblock, params: { person_id: ytm_user.id }

      expect(response).to redirect_to(admin_person_path(ytm_user.id))
      expect(ytm_user.ytm_blocked?).to be false
    end

    it "should create a note stating the YTSR service was unblocked" do
      put :unblock, params: { person_id: ytm_user.id }

      note = ytm_user.notes.first
      expect(note.subject).to eq "YTSR Service Unblocked"
    end
  end

  describe "#redirect_if_no_ytm" do
    let(:person) { create(:person) }

    context "person has no YTSRProxy or legacy YTM subscription" do
      it "succeeds and redirects to admin person path" do
        put :block, params: { person_id: person.id }

        assert_redirected_to admin_person_path(person.id)
      end
    end

    context "person has a YTSRProxy release" do
      let(:album) { create(:album, :finalized, person: person) }

      before do
        create(:salepoint, salepointable: album, store_id: Store::YTSR_PROXY_ID)
      end

      it "succeeds and redirects to admin person path" do
        put :block, params: { person_id: person.id }

        assert_redirected_to admin_person_path(person.id)
      end
    end

    context "person has a legacy YTM subscription" do
      before do
        create(:youtube_monetization, effective_date: Time.now, person: person)
      end

      it "succeeds and redirects to admin person path" do
        put :block, params: { person_id: person.id }

        assert_redirected_to admin_person_path(person.id)
      end
    end
  end

end
