require "rails_helper"

describe Admin::SongDistributionOptionsController, type: :controller do
context "#update" do
  let(:admin)  { Person.find_by_name!("Super Admin") }
  let(:person) { FactoryBot.create(:person) }
  let(:album)  { FactoryBot.create(:album, person: person) }
  let(:song)   { FactoryBot.create(:song, album: album) }

  before(:each) do
    login_as(admin)
  end

  it "responds successfully to a PUT" do
    put :update, params: {
      :song_id => song.id,
      :song_distribution_option_name => :available_for_streaming,
      :song_distribution_option_value => true,
      :album_id => album.id
    }, xhr: true
    expect(response.status).to eq(200)
  end

  it "responds with 404 on missing song id" do
    put :update, params: {
      :song_id => nil,
      :song_distribution_option_name => :available_for_streaming,
      :song_distribution_option_value => true,
      :album_id => album.id
    }, xhr: true
    expect(response.status).to eq(404)
  end
end
end
