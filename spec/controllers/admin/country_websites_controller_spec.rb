require "rails_helper"

describe Admin::CountryWebsitesController do
  let(:person) { create(:person, country_website: CountryWebsite.find(1)) }
  let(:admin) { Person.find_by_name!("Tunecore Admin") }

  before do
    login_as(admin)
  end

  describe '#update' do
    it 'should render the edit template' do
      put :update, params: { person_id: person.id, id: '6' }

      expect(response.status).to eq(200)
      expect(response).to render_template(:edit)
    end

    it "updates the person's country_website" do
      put :update, params: { person_id: person.id, id: '6' }

      expect(person.reload.country_website_id).to eq 6
    end
  end
end
