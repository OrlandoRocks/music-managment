require "rails_helper"

describe Admin::TerminatedComposersController do
  before :each do
    @admin    = create(:person, :admin)
    @person   = create(:person)
    @account  = create(:account, person: @person)
    @composer = create(:composer, account: @account)

    login_as(@admin)
    allow(FeatureFlipper).to receive(:show_feature?).and_return false
  end

  describe "#create" do
    it "creates a fully terminated composer" do
      post :create, params: { id: @account.id, terminated_composer: { composer_id: @composer.id } }
      expect(@composer.reload.terminated_composer).not_to eq nil
    end

    it "returns an error if the effective_date is not formatted correctly" do
      params = { composer_id: @composer.id, effective_date: "10-18-87" }
      post :create, params: { id: @account.id, terminated_composer: params }
      expect(flash[:error]).to be_present
    end
  end

  describe "#update" do
    it "updates a partially terminated composer to be fully terminated" do
      create(:terminated_composer, :partially, composer: @composer)
      params = { composer_id: @composer.id, admin_note: "", effective_date: "", publishing_termination_reason_id: 1 }

      expect { put :update, params: { id: @account.id, terminated_composer: params } }
        .to change { @composer.reload.terminated_composer.termination_type }
        .from("partially").to("fully")
    end

    it "returns an error if the effective_date is not formatted correctly" do
      params = { composer_id: @composer.id, effective_date: "10-18-87" }
      put :update, params: { id: @account.id, terminated_composer: params }
      expect(flash[:error]).to be_present
    end
  end

  describe "#destroy" do
    before :each do
      @terminated_composer = create(:terminated_composer, :fully, composer: @composer)
    end

    it "destroys a terminated composer" do
      post :destroy, params: { id: @terminated_composer.id }
      @composer.reload

      expect(@composer.terminated_composer).to be nil
    end
  end

end
