require "rails_helper"

describe Admin::AddSalepointsController do
  before do
    admin  = create(:person, :admin)
    create(:person)
    login_as(admin)
  end

  describe "#update" do
    it "should add the salepoints to the album and invoke the AddPaidSalepointsService" do
      album = create(:album)
      store = create(:store)

      allow(Album).to receive(:find_by).and_return(album)

      expect(album).to receive(:add_stores).with([store])
      expect(AddPaidSalepointsService).to receive(:call)

      put :update, params: { album_id: album.id, store_ids: [store.id.to_s], apple_music: "on" }, format: :js
    end
  end
end
