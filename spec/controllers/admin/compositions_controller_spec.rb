require "rails_helper"

describe Admin::CompositionsController, type: :controller do
  let(:admin) { create(:person, :admin) }
  let(:account) { create(:account, person: admin) }
  let(:role) { create(:role, name: "Publishing Manager") }

  before do
    admin.roles << role
    login_as(admin)
  end

  describe "#index" do
    context "when the admin does not have access to the composer compositions page" do
      it "redirects to the composers manager page" do
        composer = create(:composer, account: account)
        get :index, params: { composer_id: composer.id }

        expect(response).to redirect_to admin_composers_manager_path(account)
      end
    end
  end

  describe "#update" do
    before :each do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "when the composer does not have a provider identifier" do
      it "sets an error message" do
        composer = create(:composer, account: account)
        composition = create(:composition, name: "Original Name", provider_identifier: "00001C8GW")
        create(:publishing_split, composer: composer, composition: composition)
        params = {
          id: composition.id,
          composer_id: composer.id,
          composition: {
            name: "Original Name"
          },
          format: :js
        }

        put :update, params: params

        expect(assigns(:error_msg)).to eq "Unable to send composition to Rights App"
      end
    end

    context "when the composer has a provider identifier" do
      it "updates the composition" do
        composer = create(:composer, :with_provider_identifier, account: account)
        composition = create(:composition, name: "Original Name", provider_identifier: "00001C8GW")
        params = {
          id: composition.id,
          composer_id: composer.id,
          composition: {
            name: "Updated Name",
            provider_identifier: "00001C8GZ",
          },
          format: :js
        }

        allow(Composition).to receive(:find_by).and_return(composition)
        expect(composition).to receive(:send_to_rights_app)

        put :update, params: params

        expect(composition.reload.name).to eq params[:composition][:name]
        expect(composition.provider_identifier).to eq params[:composition][:provider_identifier]
      end
    end
  end

  describe ".update_status" do
    it "updates the composition status" do
      composer = create(:composer, :with_provider_identifier, account: account)
      composition = create(:composition, name: "Original Name", provider_identifier: "00001C8GW")
      composition.update(state: 'hidden')
      params = {
        id: composition.id,
        composer_id: composer.id,
        format: :js
      }

      allow(Composition).to receive(:find_by).and_return(composition)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

      put :update_status, params: params

      expect(composition.reload.state).to eq 'new'
    end
  end


  describe "#update_eligibility" do
    context "when the composition is ineligible" do
      it "updates the composition state to its previous state" do
        composer = create(:composer, :with_provider_identifier, account: account)
        composition = create(:composition, name: "Original Name", provider_identifier: "00001C8GW")
        composition.update(state: 'verified')
        composition.update(state: 'ineligible')

        params = {
          id: composition.id,
          composer_id: composer.id,
          format: :js
        }

        allow(Composition).to receive(:find_by).and_return(composition)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        put :update_eligibility, params: params

        expect(composition.reload.state).to eq('verified')
      end
    end

    context "when the composition is not ineligible" do
      it "updates the composition state to ineligible" do
        composer = create(:composer, :with_provider_identifier, account: account)
        composition = create(:composition, name: "Original Name", provider_identifier: "00001C8GW")
        composition.update(state: 'verified')

        params = {
          id: composition.id,
          composer_id: composer.id,
          format: :js
        }

        allow(Composition).to receive(:find_by).and_return(composition)
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        put :update_eligibility, params: params

        expect(composition.reload.state).to eq('ineligible')
      end
    end
  end
end
