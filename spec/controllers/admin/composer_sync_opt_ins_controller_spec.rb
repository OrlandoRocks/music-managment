require "rails_helper"

describe Admin::ComposerSyncOptInsController do
  before :each do
    @admin    = create(:person, :admin)
    @person   = create(:person)
    @account  = create(:account, person: @person)
    @composer = create(:composer, account: @account, sync_opted_in: true, sync_opted_updated_at: Time.now)

    login_as(@admin)
  end

  describe "#destroy" do
    context "when the composer is sync_opted_in" do
      it "sets sync_opted_in to false" do
        expect(@composer.reload.sync_opted_in).to be_truthy

        composer_params = { id: @composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, composer: composer_params }

        @composer.reload

        expect(@composer.sync_opted_in).to be_falsey
      end

      it "sets sync_opted_updated_at to the passed in date" do
        expect(@composer.reload.sync_opted_in).to be_truthy

        composer_params = { id: @composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, composer: composer_params }

        @composer.reload

        expect(@composer.sync_opted_updated_at.to_date.to_s).to eq("2021-10-18")
      end
    end

    context "when the composer is not sync_opted_in" do
      it "sets sync_opted_in to false" do
        @composer = create(:composer, account: @account, sync_opted_in: false)

        expect(@composer.reload.sync_opted_in).to be_falsey

        composer_params = { id: @composer.id, sync_opted_updated_at: "10/18/2021" }

        delete :destroy, params: { id: @account.id, composer: composer_params }

        @composer.reload

        expect(@composer.sync_opted_in).to be_falsey
      end
    end

    context "when sync_opted_updated_at is not passed in" do
      before(:each) do
        Timecop.freeze
      end

      after(:each) do
        Timecop.return
      end

      it "sets sync_opted_updated_at to today's date" do
        date_today = Date.today

        @composer = create(:composer, account: @account, sync_opted_in: false, sync_opted_updated_at: nil)

        expect(@composer.sync_opted_updated_at).to eq(nil)

        composer_params = { id: @composer.id }

        delete :destroy, params: { id: @account.id, composer: composer_params }

        @composer.reload

        expect(@composer.sync_opted_updated_at.to_date.to_s).to eq(date_today.to_s)
      end
    end
  end
end
