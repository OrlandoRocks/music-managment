require "rails_helper"

describe Admin::TwoFactorAuthController do
  let(:sean)   { create(:person, :admin, :with_role, role_name: "tfa_admin") }
  let(:andy)   { create(:person, :admin) }
  let(:schlub) { create(:person, :with_two_factor_auth) }

  before :each do
    request.env["HTTP_REFERER"] = admin_person_path(schlub)
    @tfa = schlub.two_factor_auth
  end

  it "does not allow non-CRT-managers to disable a user's 2FA" do
    login_as(andy)

    post :update, params: { id: schlub.id }

    expect(Person).not_to receive(:find)
    expect(flash[:alert]).to be_present
    expect(@tfa.active?).to be true
  end

  it "disables the user's 2FA and returns to the person show page for CRT managers" do
    login_as(sean)

    post :update, params: { id: schlub.id }

    expect(flash[:notice]).to be_present
    expect(@tfa.reload.active?).to be false
  end
end