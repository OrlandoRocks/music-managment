require "rails_helper"

describe Admin::SoundoutReportsController, type: :controller do

  describe "toggle cancel" do

    before(:each) do
      @soundout_report = build(:soundout_report, :with_track)
      @login_as        = Person.find_by_name!("Tunecore Admin")
      login_as(@login_as)
    end

    it "should mark a report as canceled" do
      @soundout_report.save!

      post :toggle_cancel, params: { person_id: @soundout_report.person_id, id: @soundout_report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: @soundout_report.person_id)

      expect(@soundout_report.reload.canceled?).to be_truthy

      assert flash[:success]
    end

    it "should mark a report as uncanceled" do
      @soundout_report.canceled_at = Time.now
      @soundout_report.save!

      post :toggle_cancel, params: { person_id: @soundout_report.person_id, id: @soundout_report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: @soundout_report.person_id)

      expect(@soundout_report.reload.canceled?).to be_falsey
    end

    it "should error if can't change the cancel" do
      @soundout_report.save!
      expect_any_instance_of(SoundoutReport).to receive(:toggle_cancel!)

      post :toggle_cancel, params: { person_id: @soundout_report.person_id, id: @soundout_report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: @soundout_report.person_id)

      assert flash[:error]
    end

  end

  describe "fetch_report" do

    before(:each) do
      @soundout_report = build(:soundout_report, :with_track, soundout_id: "123")
      @login_as        = Person.find_by_name!("Tunecore Admin")
      login_as(@login_as)
    end

    it "should fetch the report and redirect" do
      @soundout_report.save!
      expect_any_instance_of(SoundoutProduct).to receive(:fetch_report).and_return([true, "data"])

      get :fetch_report, params: { person_id: @soundout_report.person_id, id: @soundout_report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: @soundout_report.person_id)
      assert flash[:success]
    end

    it "should set an error message if it fails" do
      @soundout_report.save!
      expect_any_instance_of(SoundoutProduct).to receive(:fetch_report).and_return([false, "data"])

      get :fetch_report, params: { person_id: @soundout_report.person_id, id: @soundout_report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: @soundout_report.person_id)
      assert flash[:error]
    end

  end

  describe "#resend report" do

    before(:each) do
      @login_as        = Person.find_by_name!("Tunecore Admin")
      login_as(@login_as)
    end

    #
    # If a report is in the error state without a soundout_id then there was an error
    # in submission and we need to be able to resend
    #
    it "should only resend a report in the error or requested state" do
      report = create(:soundout_report, :with_track, status: "error", soundout_id: nil )
      allow_any_instance_of(SoundoutReport).to receive(:paid?).and_return(true)

      expect(Soundout).to receive("order_report").and_return("123")

      get :resend_report, params: { person_id: report.person_id, id: report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: report.person_id)
      assert flash[:success]

      expect(Soundout).to receive("order_report").and_return("123")
      
      report.status="requested"
      report.soundout_id = "123"
      report.save!
      get :resend_report, params: { person_id: report.person_id, id: report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: report.person_id)
      assert flash[:success]

      expect(Soundout).not_to receive("order_report")

      #Can't resend with a status of avalable
      report.status="available"
      report.save!
      get :resend_report, params: { person_id: report.person_id, id: report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: report.person_id)
      assert flash[:error]

      expect(Soundout).not_to receive("order_report")

      #Can't resend with a status of received
      report.status="received"
      report.save!
      get :resend_report, params: { person_id: report.person_id, id: report.id }
      assert_redirected_to admin_person_soundout_reports_path(person_id: report.person_id)
      assert flash[:error]
    end

  end

end
