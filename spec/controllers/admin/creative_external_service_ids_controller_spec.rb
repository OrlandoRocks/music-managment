require "rails_helper"

describe Admin::CreativeExternalServiceIdsController do
  before(:each) do
    person = create(:person, :with_role)
    login_as(person)
  end

  describe "#index" do
    context "when the feature is enabled for the current user" do
      it "renders the index view" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(true)

        album = create(:album)
        creative = album.creatives.first
        person = creative.person

        params = { person_id: person.id }

        get :index, params: params

        expect(response).to render_template(:index)
      end
    end

    context "when the feature is not enabled for the current user" do
      it "redirects to the admin dashboard" do
        allow(FeatureFlipper).to receive(:show_feature?).and_return(false)

        album = create(:album)
        creative = album.creatives.first
        person = creative.person

        params = { person_id: person.id }

        get :index, params: params

        expect(response).to redirect_to admin_home_path
      end
    end
  end

  describe "#load_creatives" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    let(:album) { create(:album) }

    let!(:creative1) do
      create(:creative,
        creativeable: album,
        role: 'primary_artist',
        artist: create(:artist, name: "Alfaba")
      )
    end

    let!(:creative2) do
      create(:creative,
        creativeable: album,
        role: 'featuring',
        artist: create(:artist, name: "Betty")
      )
    end

    let!(:creative3) do
      create(:creative,
        creativeable: album,
        role: 'composer',
        artist: create(:artist, name: "Gemma")
      )
    end

    let(:person) { album.creatives.first.person }

    context "when search is enabled" do
      before(:each) do
        allow_any_instance_of(Admin::CreativeExternalServiceIdsController)
          .to receive(:search_enabled?)
          .and_return(true)
      end

      context "when search params are passed in" do
        it "returns creatives that match the search criteria" do
          params = { person_id: person.id, search_text: "Alfaba" }

          expect_any_instance_of(Admin::CreativeExternalServiceIdsController)
            .to receive(:queried_creatives)
            .and_return([creative1])

          get :index, params: params
        end
      end

      context "when search params are not passed in" do
        it "returns primary_featuring_or_remixer creatives" do
          params = { person_id: person.id }

          expect_any_instance_of(Admin::CreativeExternalServiceIdsController)
            .to receive(:queried_creatives)
            .and_return(person.creatives.primary_featuring_or_remixer)

          get :index, params: params
        end
      end
    end
  end
end
