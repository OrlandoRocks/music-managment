require "rails_helper"

describe Admin::DeleteAccountsController do
  let(:admin_without_lock_role) { create(:person, :admin) }
  let(:admin_with_lock_role) do
    create(:person, :admin, :with_role, role_name: "Lock")
  end

  describe "#new" do
    it "redirects to admin home if admin doesn't have lock role" do
      login_as(admin_without_lock_role)
      get :new
      expect(response).to redirect_to admin_home_path
    end

    it "is successful if admin has Lock role" do
      login_as(admin_with_lock_role)
      get :new
      expect(response.status).to eq 200
    end
  end

  describe "#create" do
    let(:person) {create(:person) }
    let(:delete_type) { DeletedAccount::DELETE_TYPES.first }

    before do
      login_as(admin_with_lock_role)
    end

    subject do
      post :create, params: {
        delete_account_form: { person_id: person.id, delete_type: delete_type }
      }
    end

    context "when the account can be soft deleted" do
      it "soft deletes a person" do
        subject

        expect(person.reload.status).to eq 'Locked'
      end

      it "redirects to /admin/delete_accounts/new" do
        expect(subject).to redirect_to new_admin_delete_account_path
      end
    end
  end
end
