require "rails_helper"

describe Admin::LocksController, type: :controller do
context "when being accessed by non-admin users" do
  before(:each) do
    person = FactoryBot.create(:person)
    login_as(person)
  end

  it "GET 'show' should not be successful" do
    get 'show'
    expect(response).not_to be_successful
  end

  it "GET 'people' should not be successful" do
    get 'people'
    expect(response).not_to be_successful
  end

  it "GET 'invoices' should not be successful" do
    get 'invoices'
    expect(response).not_to be_successful
  end

  it "GET 'bundles' should not be successful" do
    get 'bundles'
    expect(response).not_to be_successful
  end
end

context "showing an administrators own locks" do
  before(:each) do
    @person = FactoryBot.create(:person, :with_role)
    login_as(@person)
  end

  it "should should ask for CurrentLocks based on current_user" do
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(@person).and_return([])
    get 'show'
    expect(response).to be_successful
  end

  it "should set @locked_items according to CurrentLocks return" do
    @fake_items = [:anything, :is, :okay]
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(@person).and_return(@fake_items)
    get 'show'
    expect(assigns(:locked_items)).to eql(@fake_items)
  end
end

context "showing all items locked by people" do
  before(:each) do
    @person = FactoryBot.create(:person, :with_role)
    login_as(@person)
  end

  it "should should ask for CurrentLocks based on current_user" do
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(:people).and_return([])
    get 'people'
    expect(response).to be_successful
  end

  it "should set @locked_items according to CurrentLocks return" do
    @fake_items = [:anything, :is, :okay]
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(:people).and_return(@fake_items)
    get 'people'
    expect(assigns(:locked_items)).to eql(@fake_items)
  end
end

context "showing all items locked by invoices" do
  before(:each) do
    @person = FactoryBot.create(:person, :with_role)
    login_as(@person)
  end

  it "should should ask for CurrentLocks based on current_user" do
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(:invoices).and_return([])
    get 'invoices'
    expect(response).to be_successful
  end

  it "should set @locked_items according to CurrentLocks return" do
    @fake_items = [:anything, :is, :okay]
    expect(Tunecore::Reports::CurrentLocks).to receive(:by).with(:invoices).and_return(@fake_items)
    get 'invoices'
    expect(assigns(:locked_items)).to eql(@fake_items)
  end
end
end
