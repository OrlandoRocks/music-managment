require "rails_helper"

describe Admin::ContentReviewController, type: :controller do
  before :each do
    @person = Person.find_by_name!("Super Admin")
    login_as(@person)
    @album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2)
  end

  describe "#invalidate_album" do
    before(:each) do
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
    end

    context "updating an album" do
      it "should invalidate album" do
        expect(Tunecore::Crt).to receive(:invalidate_album_in_cache).with(@album).once
        post :update_album, params: { album_update: { id: @album.id, language_code: 'en'} }
      end
      context "updating songs" do
        let(:lyric_content) {
          "New lyrics o yea\r\nmy new lyrics are\r\nso good\r\nYou wish you could\r\nwrite lyrics this good\r\nPlus i'm awesome at rhyming and spelling!!!"
        }
        let(:songs) { @album.songs }
        let(:album_params) {
          { "album_update" => {
              "id"=>@album.id,
              "label_name"=>"Moebi",
              "language_code"=>"en",
              "name"=>@album.name,
              "next"=>"/content_review/show?ids=1",
              "primary_genre_id"=>"4",
              "song-#{songs.first.id}-lyrics"=>lyric_content,
              "song-#{songs.first.id}-name"=>"Little Jumps! New (feat. Sophie)",
              "song-#{songs.first.id}-translated_name"=>"Translated name edited",
              "song-#{songs.second.id}-name"=>"Second Song Edited",
            }
          }
        }

        before do
          FactoryBot.create(:lyric, song_id: songs.first.id)
        end

        it "should update the album's songs" do
          post :update_album, params: album_params.with_indifferent_access
          song_1 = songs.first.reload
          song_2 = songs.second.reload
          capitalized_lyrics = lyric_content.split("\r\n").map(&:capitalize).join("\r\n")

          expect(song_1.lyric.content).to eq(capitalized_lyrics)
          expect(song_1.name).to eq("Little Jumps! New (feat. Sophie)")
          expect(song_1.translated_name).to eq("Translated name edited")
          expect(song_2.name).to eq("Second Song Edited")
        end
      end
    end

    context "approving an album" do
      it "should invalidate album" do
        expect(Tunecore::Crt).to receive(:invalidate_album_in_cache).with(@album).once
        post :update, params: { review_audit: { album_id: @album.id } }
      end
    end
  end

  describe "#show" do
    it "should respond to an empty array for ids param" do
      get :show, params: { ids: [] }
    end

    it "should respond to a non empty array for ids param" do
      get :show, params: { ids: [@album.id] }
    end
  end

  describe '#change_album_format_flag' do
    let(:en_album) { create(:album) }
    let(:fr_album) { create(:album, language_code: "fr") }

    context 'albums outside of non formattable language codes' do
      it "turns off auto format" do
        en_album.update_attribute :allow_different_format, false
        post 'change_album_format_flag', params: { album_id: en_album.id }
        expect(en_album.reload.allow_different_format).to be_truthy
      end

      it "turns on auto format" do
        en_album.update_attribute :allow_different_format, true
        post 'change_album_format_flag', params: { album_id: en_album.id }
        expect(en_album.reload.allow_different_format).to be_falsey
      end

      it 'responds with successful response' do
        post 'change_album_format_flag', params: { album_id: en_album.id }
        response_data = JSON.parse(response.body)
        expect(response_data["response"]["messages"].first).to eq("update succeeded")
      end
    end

    context 'albums with non formattable language codes' do
      it 'turns off auto format if on' do
        fr_album.update_attribute :allow_different_format, false
        post 'change_album_format_flag', params: { album_id: fr_album.id }
        expect(fr_album.reload.allow_different_format).to be_truthy
      end

      it 'keeps auto format off' do
        fr_album.update_attribute :allow_different_format, true
        fr_album.reload
        post 'change_album_format_flag', params: { album_id: fr_album.id }
        expect(fr_album.reload.allow_different_format).to be_truthy
      end

      it 'sets error message' do
        fr_album.update_attribute :allow_different_format, true
        post 'change_album_format_flag', params: { album_id: fr_album.id }
        response_data = JSON.parse(response.body)
        expect(response_data["response"]["messages"].first).to eq("update not allowed because of language code: #{fr_album.language_code}")
      end
    end
  end

  describe "#update" do
    before { allow_any_instance_of(ReviewAudit).to receive(:send_sns_notification) }

    it "creates a new Review Audit" do
      person = Person.find_by_name!("Super Admin")
      login_as(person)
      album = create(:album, :with_petri_bundle, :with_uploaded_songs, number_of_songs: 2)
      allow(FeatureFlipper).to receive(:show_feature?).and_return(true)
      review_audit_params = { review_audit: { event: "APPROVED", album_id: album.id, person: person } }

      post 'update', params: review_audit_params

      expect(ReviewAudit.last).to have_attributes review_audit_params[:review_audit]
    end
  end
end
