require "rails_helper"

describe Admin::AlbumCountriesController do
  let(:album) { create(:album) }
  let(:admin) { create(:person, :admin) }

  before(:each) do
    login_as(admin)
  end

  context "#edit" do
    it "returns 200 and assigns selected_country_codes" do
      get :edit, params: { album_id: album.id }
      expect(response.status).to eq 200
      expect(assigns(:selected_country_codes)).to eq album.country_iso_codes
      expect(assigns(:countries)).to eq Country::COUNTRIES
      expect(assigns(:album)).to eq album
    end
  end

  context "#update" do
    it "redirects to album show if save is successful" do
      params = { album_id: album.id, album_country_relation_type_service_form: { iso_codes: ["US"] } }
      relation_form = double(save: true)
      expect(AlbumCountry::RelationTypeServiceForm).to receive(:new).with(album: album, iso_codes: ["US"]).and_return(relation_form)

      put :update, params: params

      expect(subject).to redirect_to admin_album_path(album)
    end

    it "renders edit if save is unsuccessful" do
      relation_form = double(save: false)
      params = { album_id: album.id, album_country_relation_type_service_form: { iso_codes: ["US"] } }
      expect(AlbumCountry::RelationTypeServiceForm).to receive(:new).with(album: album, iso_codes: ["US"]).and_return(relation_form)

      put :update, params: params

      expect(subject).to render_template :edit
    end
  end
end
