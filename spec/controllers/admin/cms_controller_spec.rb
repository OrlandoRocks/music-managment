require "rails_helper"

describe Admin::CmsController, type: :controller do
  let!(:cms_set) do
    FactoryBot.create(
      :dashboard_set,
      :no_date_check,
      callout_name: "Active Promo",
      country_website_id: 1,
      country_website_language_id: nil,
      start_tmsp: Time.local(2016, 2, 1),
    )
  end

  before :each do
    login_as(Person.find_by_name!("Tunecore Admin"))
  end

  describe "#create" do
    it "should call CmsSet#create_attributes if attributes are passed" do
      expect_any_instance_of(CmsSet).to receive(:create_attributes)
      post :create, params: { type: "interstitial", name: "test", country_languages: { "United States" => 1 }, asset: { "markup" => "test" } }
    end

    it "should not call CmsSet#create_attributes if no attributes are passed" do
      expect_any_instance_of(CmsSet).not_to receive(:create_attributes)
      post :create, params: { type: "interstitial", country_languages: { "United States" => 1 }, name: "test" }
    end

    it "should redirect to the CmsSet index on success" do
      post :create, params: { type: "interstitial", country_languages: { "United States" => 1 }, name: "test", asset: {} }
      assert_redirected_to admin_cms_url
    end

    it "should render the new page on failure" do
      post :create, params: { name: "" }
      assert_template "new"
    end
  end

  describe "#activate" do
    it "updates start_tmsp to now and redirects back" do
      request.env["HTTP_REFERER"] = "/admin/cms"
      put :activate, params: { cm_id: cms_set.id }
      cms_set.reload

      expect(cms_set.start_tmsp).to be > Time.local(2016, 1, 1)
      expect(response).to redirect_to "/admin/cms"
    end
  end
end
