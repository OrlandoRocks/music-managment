require "rails_helper"

describe Admin::SalepointsController, type: :controller do
  fixtures :albums

  before(:each) do
    @login_as        = Person.find_by_name!("Tunecore Admin")
    allow(PetriBundle).to receive(:for).and_return(TCFactory.create(:petri_bundle))
    login_as(@login_as)
  end

  describe "update" do

    it "should update track variable price and variable price" do
      album = albums(:customer_finalized)

      salepoint = album.salepoints.first

      put :update, params: { id: album.id, "salepoints" => { "0" => { "id" => "#{salepoint.id}", "track_variable_price_id" => "#{salepoint.store.variable_prices.first.id}", "variable_price_id" => "#{salepoint.store.variable_prices.first.id}", "apply" => "apply"}} }
      assert_redirected_to admin_album_path(album)

      assert salepoint.reload.track_variable_price_id == salepoint.store.variable_prices.first.id
      assert salepoint.reload.variable_price_id       == salepoint.store.variable_prices.first.id
    end

    it "should create a note" do
      album = albums(:customer_finalized)

      salepoint = album.salepoints.first

      notes = Note.count
      put :update, params: { id: album.id, "salepoints" => {"0" => {"id" => "#{salepoint.id}", "track_variable_price_id" => "43", "variable_price_id" => "44", "apply" => "apply"}} }
      assert_redirected_to admin_album_path(album)
      assert Note.count == (notes+1)
    end

    it "should not create a note if nothing changes" do
      album = albums(:customer_finalized)

      salepoint = album.salepoints.first

      notes = Note.count
      put :update, params: { id: album.id, "salepoints" => {"0" => {"id" => "1", "track_variable_price_id" => "43", "variable_price_id" => "44"}, "1" => {"id"=>"3", "track_variable_price_id" => "", "variable_price_id" => ""}, "2" => {"id" => "8", "track_variable_price_id" => "", "variable_price_id" => "5"}}}
      assert_redirected_to admin_album_path(album)
      assert Note.count == notes

    end

  end

  describe "block_distribution" do
    it "should call block! on salepoint and create a note" do
      person = TCFactory.create(:person)
      album = TCFactory.create(:album, :person => person)

      sp = mock_model(Salepoint)
      allow(sp).to receive(:salepointable).and_return(album)

      allow(Salepoint).to receive(:find).and_return(sp)

      expect(sp).to receive(:block!).with(:actor => "TuneCore Admin: #{@login_as.id}", :message => "blocking_distribution")
      allow(sp).to receive_message_chain(:store, :name).and_return("Youtube")

      expect{
        put :block_distribution, params: { id: sp.id }
      }.to change(Note, :count).by(1)

      assert_redirected_to admin_album_path(album)
    end
  end

  describe "unblock_distribution" do
    it "should call unblock! on salepoint and create a note" do
      person = create(:person)
      album = create(:album, :person => person)

      sp = mock_model(Salepoint)
      allow(sp).to receive(:salepointable).and_return(album)

      allow(Salepoint).to receive(:find_by).and_return(sp)

      expect(sp).to receive(:unblock!).with(:actor => "TuneCore Admin: #{@login_as.id}", :message => "unblocking_distribution")
      allow(sp).to receive_message_chain(:store, :name).and_return("Youtube")

      expect{
        put :unblock_distribution, params: { id: sp.id }
      }.to change(Note, :count).by(1)

      assert_redirected_to admin_album_path(album)
    end
  end
end
