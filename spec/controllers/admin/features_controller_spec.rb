require "rails_helper"

describe Admin::FeaturesController, type: :controller do
  before(:each) do
    $redis.flushdb
    @person = create(:person, :admin, :with_role, role_name: "Feature Manager" )
    login_as(@person)
  end

  it("should create FeatureFlipper and notify slack") do
    feature_name = "Test 1"
    message = "#{@person.name} created a FeatureFlipper '#{feature_name}' under Global Application features"

    expect_any_instance_of(Admin::FeaturesController).to receive(:notify_in_slack).with(message, "FeatureFlipper", TECH_DEPLOY_CHANNEL_NAME)
    post :create, params: {new_feature: feature_name, country_iso_code: nil}
    expect(FeatureFlipper.all_features.first.name).to eq :test_1
  end


  it("should update FeatureFlipper and notify slack") do
    feature_name = "Test 2"
    FeatureFlipper.add_feature(feature_name)
    feature = FeatureFlipper.all_features[0]
    expect(feature.percentage).to eq 0
    expect(feature.name).to eq :test_2

    updated_percentage = "10"
    updated_users = "1,2"
    message = "#{@person.name} updated the FeatureFlipper '#{feature_name}'"\
      " under Global Application features with Percentage - #{updated_percentage} and User(s) - #{updated_users}"
    expect_any_instance_of(Admin::FeaturesController).to receive(:notify_in_slack).with(message, "FeatureFlipper", TECH_DEPLOY_CHANNEL_NAME)

    post :update, params: { id: feature, feature_name: feature_name, percentage: updated_percentage, users: "1,2" }
  end

  it("should update FeatureFlipper and notify slack without user information when it is empty") do
    feature_name = "Test 21"
    FeatureFlipper.add_feature(feature_name)
    feature = FeatureFlipper.all_features[0]

    updated_percentage = "40"
    message = "#{@person.name} updated the FeatureFlipper '#{feature_name}'"\
      " under Global Application features with Percentage - #{updated_percentage}"
    expect_any_instance_of(Admin::FeaturesController).to receive(:notify_in_slack).with(message, "FeatureFlipper", TECH_DEPLOY_CHANNEL_NAME)

    post :update, params: { id: feature, feature_name: feature_name, percentage: updated_percentage }
  end

  it("should remove FeatureFlipper and notify slack") do
    feature_name = "Test 3"
    FeatureFlipper.add_feature(feature_name)
    feature = FeatureFlipper.all_features[0]
    expect(feature.percentage).to eq 0
    expect(feature.name).to eq :test_3

    ticket_link = "https://tunecore.atlassian.net/browse/STU-1"
    message = "#{@person.name} removed the FeatureFlipper '#{feature_name}' under Global Application features"
    expect_any_instance_of(Admin::FeaturesController).to receive(:notify_in_slack) .with(message, "FeatureFlipper", TECH_DEPLOY_CHANNEL_NAME)

    post :remove, params: { id: feature, feature_name: feature_name, ticket_link: ticket_link}
  end
end
