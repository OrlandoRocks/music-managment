require "rails_helper"

describe Admin::ComposersController do
  before :each do
    admin    = create(:person, :admin)
    create(:account, person: admin)
    role = create(:role, name: "Publishing Manager")
    admin.roles << role
    login_as(admin)
  end

  describe "#update" do


    it "updates the composer and sets the pro" do
      composer = create(:composer, cae: nil, performing_rights_organization_id: 56)

      params = {
        id: composer.id,
        composer: {
          cae: "123456789",
        }
      }

      put :update, params: params

      expect(composer.reload.cae).to eq params[:composer][:cae]
    end

    context "when the composer does has a publisher" do
      it "updates the composer and the publisher" do
        composer = create(
          :composer,
          cae: nil,
          performing_rights_organization_id: nil
        )

        params = {
          id: composer.id,
          composer: {
            cae: "123456789",
          },
          publisher: {
            cae: "987654321",
            name: "Publisher Name",
            performing_rights_organization_id: 56
          }
        }

        put :update, params: params

        expect(composer.reload.publisher.cae).to eq params[:publisher][:cae]
        expect(composer.publisher.performing_rights_organization_id).to eq params[:publisher][:performing_rights_organization_id]
        expect(composer.publisher.name).to eq params[:publisher][:name]
      end
    end
  end

  describe ".download_client_import" do
    context ".date range more than a year" do
      it "should trigger a sidekiq job and redirect" do
        expect(ClientImportReportWorker).to receive(:perform_async)
        get :download_client_import, params: { d: {from_date: '01/01/2018', to_date: '02/02/2019' }}
        expect(response).to redirect_to list_admin_composers_path
        expect(flash[:notice]).to eq("You'll receive a report in your mail shortly, kindly check your mail.")
      end

      it "should not trigger a sidekiq job" do
        get :download_client_import, params: { d: {from_date: '10/10/2018', to_date: '02/02/2019' }}
        expect(ClientImportReportWorker).not_to receive(:perform_async)
      end
    end
  end
end
