require "rails_helper"

describe Admin::TargetedOffersController do
  render_views
  let(:s3)         { double("s3") }
  let(:buckets)    { double("buckets") }
  let(:objects)    { double("objects") }
  let(:object_key) { "people.csv" }

  before(:each) do
    @login_as        = Person.find_by_name!("Tunecore Admin")
    login_as(@login_as)
    stub_const("S3_CLIENT", s3)
    allow(s3).to receive(:buckets).and_return(buckets)
    allow(buckets).to receive(:[]).with(TARGETED_OFFER_BUCKET_NAME).and_return(objects)
    allow(objects).to receive(:objects).and_return(objects)
    allow(objects).to receive(:create)
  end

   describe "#create" do
    it "creates a new targeted offer" do
      allow_any_instance_of(TargetedOffer).to receive(:manage_cms_files)
      allow(@login_as).to receive(:is_permitted_to?).with("admin/targeted_offers", "create").and_return(true)

      params = {
        name: "test offer",
        country_website_id: "1",
        offer_type: "existing",
        product_show_type: "price_override",
        date_constraint: "expiration",
        start_date: Date.current,
        expiration_date: Date.tomorrow,
        usage_limit: 1,
        include_sidebar_cms: 1,
        created_by_id: @login_as.id,
      }

      expect {
        post :create, params: { targeted_offer: params }
      }.to change(TargetedOffer, :count).by(1)

      to = TargetedOffer.order('created_at desc').first
      expect(to.name).to eq(params[:name])
      expect(to.country_website_id).to eq(params[:country_website_id])
      expect(to.offer_type).to eq(params[:offer_type])
      expect(to.product_show_type).to eq(params[:product_show_type])
      expect(to.date_constraint).to eq(params[:date_constraint])
      expect(to.start_date.to_date).to eq(params[:start_date])
      expect(to.expiration_date.to_date).to eq(params[:expiration_date])
      expect(to.usage_limit).to eq(params[:usage_limit])
      expect(to.include_sidebar_cms).to eq(true)
      expect(to.created_by_id).to eq(params[:created_by_id])
    end
  end

   describe "#update" do
     let(:targeted_offer)  { create(:targeted_offer) }

     it "updates an existing targeted offer" do
       params = { name: "New Name" }

       post :update, params: { id: targeted_offer.id, targeted_offer: params }
       expect(targeted_offer.reload.name).to eq("New Name")
     end

     it "updates start_date and expiration_date" do
       params = { start_date: Date.parse('1/1/2019'), expiration_date: Date.parse('31/12/2019') }
       post :update, params: { id: targeted_offer.id, targeted_offer: params }

       targeted_offer.reload
       expect(targeted_offer.start_date.to_date).to eq(params[:start_date])
       expect(targeted_offer.expiration_date.to_date).to eq(params[:expiration_date])
     end
  end

  describe "#add_person" do
    before(:each) do
      @targeted_offer = TCFactory.create(:targeted_offer, created_by_id: @login_as.id, status: "New")
      @person         = TCFactory.create(:person)
    end

    context "when products have been added to offer" do

      before(:each) do
        product = Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID)
        TargetedProduct.create(:product=>product, :targeted_offer=>@targeted_offer)
        @targeted_offer.update(:status=>"Active")
      end

      it "should add a valid person" do
        post :add_person, params: { id: @targeted_offer.id, add_person_id: @person.id }

        expect(response).to redirect_to(admin_targeted_offer_path(@targeted_offer))
      end

      it "should 404 when adding an invalid person" do
        post :add_person, params: { id: @targeted_offer.id, add_person_id: 23142134 }
        assert_response :not_found, @response.body
      end

      it "should 500 when adding a person who is part of another targeted offer with the same product" do
        post :add_person, params: { id: @targeted_offer.id, add_person_id: @person.id }

        @to2 = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
        TargetedProduct.create(:product=>@targeted_offer.targeted_products.first.product, :targeted_offer=>@to2)
        @to2.update(:status=>"Active")

        post :add_person, params: { id: @to2.id, add_person_id: @person.id }
        assert_response :internal_server_error, @response.body
      end

    end

    context "when products have not been added to offer" do

      it "should 500 when trying to add a person" do
        post :add_person, params: { id: @targeted_offer.id, add_person_id: @person.id}
        assert_response :internal_server_error, @response.body
      end

    end

  end

  describe "#remove_person" do

    before(:each) do
      @targeted_offer   = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      @targeted_product = TargetedProduct.create!(:product_id=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, :targeted_offer_id=>@targeted_offer.id)
      @targeted_offer.update(:status=>"Active")

      @person           = TCFactory.create(:person)
      @targeted_person  = TargetedPerson.create!(:targeted_offer_id=>@targeted_offer.id, :person_id=>@person.id)
    end

    it "should remove a valid person in the offer" do

      post :remove_person, params: { id: @targeted_offer.id, remove_person_id: @person.id}
      expect(response).to redirect_to(admin_targeted_offer_path(@targeted_offer))

    end

    it "should 404 when removing a person not in the offer" do
      @person2 = TCFactory.create(:person)
      post :remove_person, params: { id: @targeted_offer.id, remove_person_id: @person2.id }
      assert_response :not_found, @response.body

    end

    it "should 404 when removing an invalid person" do
      post :remove_person, params: { id: @targeted_offer.id, remove_person_id: 1231231 }
      assert_response :not_found, @response.body
    end

    it "should 500 when removing a person that has already used the offer" do
      @targeted_person.use!
      @targeted_person.save

      post :remove_person, params: { id: @targeted_offer.id, remove_person_id: @person.id}
      assert_response :internal_server_error, @response.body
    end

  end

  describe "#targeted_people" do

    before(:each) do
      @targeted_offer   = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      @targeted_product = TargetedProduct.create!(:product_id=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, :targeted_offer_id=>@targeted_offer.id)
      @targeted_offer.update(:status=>"Active")

      3.times do
        @targeted_person  = TargetedPerson.create!(:targeted_offer_id=>@targeted_offer.id, :person_id=>TCFactory.create(:person).id)
      end
    end

    it  "should download the csv" do

      get :targeted_people, params: { id: @targeted_offer.id, format: "csv" }
      assert_response :success, @response.body

    end

  end

  describe "#add_people and #remove_people" do
    before(:each) do
      @targeted_offer   = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      @targeted_product = TargetedProduct.create!(:product_id=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, :targeted_offer_id=>@targeted_offer.id)
      @targeted_offer.update(:status=>"Active")

      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))

      File.open("#{Rails.root}/spec/fixtures/people.csv","w") do |f|
        f.write @targeted_offer.export_people_to_csv
      end

      allow(@targeted_offer).to receive(:upload_csv).and_return(true)
    end

    describe "#add_people" do

      it "should add valid people" do

        post :add_people, params: { id: @targeted_offer.id, add_people_csv: fixture_file_upload("/people.csv", 'text/csv') }
        assert_redirected_to admin_targeted_offer_path(@targeted_offer)

        expect(@targeted_offer.reload.job_status).to eq(TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS)

      end

      it "should 500 when delayed job already running for the offer" do
        @targeted_offer.update_attribute(:job_status, TargetedOffer::ADD_PEOPLE_SCHEDULED_STATUS)
        post :add_people, params: { id: @targeted_offer.id, add_people_csv: fixture_file_upload("/people.csv", 'text/csv') }
        assert_response :internal_server_error, @response.body
      end

    end

    describe "#remove_people" do

      it "should remove people in the offer" do

        post :remove_people, params: { id: @targeted_offer.id, remove_people_csv: fixture_file_upload("/people.csv", 'text/csv') }
        assert_redirected_to admin_targeted_offer_path(@targeted_offer)
        expect(@targeted_offer.reload.job_status).to eq(TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS)

      end

      it "should 500 when delayed job already running for the offer" do

        @targeted_offer.update_attribute(:job_status, TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS)
        post :remove_people, params: { id: @targeted_offer.id, remove_people_csv: fixture_file_upload("/people.csv", 'text/csv') }
        assert_response :internal_server_error, @response.body

      end

    end

    after(:each) do
      File.delete("#{Rails.root}/spec/fixtures/people.csv")
    end

  end

  describe "#clone" do

    it "should clone and redirect" do
      @targeted_offer = TCFactory.create(:targeted_offer,  :created_by_id=>@login_as.id)
      @cloned_to      = @targeted_offer.dup
      @cloned_to.save!

      #Stub find and the clone to return the already cloned to so we can assert it gets redirected here
      allow(TargetedOffer).to receive(:find).and_return(@targeted_offer)
      allow(@targeted_offer).to receive(:dup).and_return(@cloned_to)

      post :clone, params: { id: @targeted_offer.id }
      assert_redirected_to admin_targeted_offer_path(@cloned_to)

    end

  end

  describe "#destroy" do

    it "should destroy and redirect on a valid destroy request" do
      #Can delete this to
      @targeted_offer = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      post :destroy, params: { id: @targeted_offer.id }
      assert_redirected_to admin_targeted_offers_path
    end

    it "should 500 when trying to delete a targeted offer that cannot be deleted" do
      #Can't delete this to
      @targeted_offer = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"Active")
      post :destroy, params: { id: @targeted_offer.id }
      assert_response :internal_server_error, @response.body

      @targeted_offer.status = "New"
      @targeted_offer.save!
      post :destroy, params: { id: @targeted_offer.id }
      assert_redirected_to admin_targeted_offers_path
    end

  end

  describe "#activate" do

    before(:each) do
      @targeted_offer   = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      @targeted_product = TargetedProduct.create!(:product_id=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, :targeted_offer_id=>@targeted_offer.id)
      @targeted_offer.update(:status=>"Active")

      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
    end

    it "should schedule a job" do

      post :activate, params: { id: @targeted_offer.id }
      assert_redirected_to admin_targeted_offer_path(@targeted_offer)

      expect(@targeted_offer.reload.job_status).to eq(TargetedOffer::CHANGE_ACTIVATION_SCHEDULED_STATUS)
    end

    it "should 500 if job already scheduled or running" do
      @targeted_offer.update_attribute(:job_status, TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS)

      post :activate, params: { id: @targeted_offer.id }
      assert_response :internal_server_error, @response.body
    end

  end

  describe "#deactivate" do

    before(:each) do
      @targeted_offer   = TCFactory.create(:targeted_offer, :created_by_id=>@login_as.id, :status=>"New")
      @targeted_product = TargetedProduct.create!(:product_id=>Product.find(Product::US_ONE_YEAR_ALBUM_PRODUCT_ID).id, :targeted_offer_id=>@targeted_offer.id)
      @targeted_offer.update(:status=>"Active")

      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
      TargetedPerson.create(:targeted_offer=>@targeted_offer, :person=>TCFactory.create(:person))
    end

    it "should schedule a job" do

      post :deactivate, params: { id: @targeted_offer.id }
      assert_redirected_to admin_targeted_offer_path(@targeted_offer)

      expect(@targeted_offer.reload.job_status).to eq(TargetedOffer::CHANGE_ACTIVATION_SCHEDULED_STATUS)
    end

    it "should 500 if job already scheduled or running" do
      @targeted_offer.update_attribute(:job_status, TargetedOffer::REMOVE_PEOPLE_SCHEDULED_STATUS)

      post :deactivate, params: { id: @targeted_offer.id }
      assert_response :internal_server_error, @response.body
    end
  end

end
