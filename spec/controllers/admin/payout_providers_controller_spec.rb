require "rails_helper"

describe Admin::PayoutProvidersController do
  describe "#index" do
    it "redirects to dashboard if admin doesn't have feature flipper enabled" do
      person = create(:person, :admin, country_website_id: CountryWebsite::CANADA, country: "CA")
      login_as(person)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout_australia_canada, person).and_return(false)

      get :index, xhr: true

      expect(response.status).to eq 302
      expect(response).to redirect_to admin_home_path
    end

    it "shows a status of 200 if both feature flipper is enabled" do
      person = create(:person, :admin, country_website_id: CountryWebsite::CANADA, country: "CA")
      login_as(person)

      allow(FeatureFlipper).to receive(:show_feature?).and_return(:default)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_payout_australia_canada, person).and_return(true)
      allow(FeatureFlipper).to receive(:show_feature?).with(:payoneer_opt_out, person).and_return(false)

      get :index, xhr: true
      expect(response.status).to eq 200
    end
  end
end
