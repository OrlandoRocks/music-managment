require "rails_helper"

describe Admin::DirectAdvanceController, type: :controller do
  before (:each) do
    @person = login_as(Person.find_by_name!("Super Admin"))
  end

  describe "GET index" do
    context "with NO search params" do
      it "does NOT set @report" do
        get :index
        expect(@report).to be_nil
      end
    end

    context "with search params" do
      it "does set @report" do
        get :index, params: { tcda_reports_search: { id_or_email: 1 } }

        expect(assigns(@report)).to_not be_nil
      end
    end
  end

  describe "PATCH update" do
    context "with valid person_id" do
      it "updates the person's override_ineligibility? attribute" do
        previous_override = @person.override_advance_ineligibility
        patch :update, params: { id: @person.id }
        @person.reload

        expect(@person.override_ineligibility?).to eq(!previous_override)
      end

      it "redirects to index" do
        subject = patch :update, params: { id: @person.id }

        expect(subject).to redirect_to(action: :index, tcda_reports_search: {id_or_email: @person.id})
      end

      it "sets report" do
        patch :update, params: { id: @person.id }

        expect(assigns(@report)).to_not be_nil
      end
    end

    context "with invalid person_id" do
      it "raises RecordNotFound error" do
        expect { patch :update, params: { id: "Greg" } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
