require 'rails_helper'

describe Admin::MassRetrySongsController, type: :controller do
  let(:admin) { create(:person, :admin) }

  let!(:album1) { TCFactory.create(:album) }
  let!(:album2) { TCFactory.create(:album) }
  let!(:album3) { TCFactory.create(:album) }

  let!(:song1) { create :song, album: album1 }
  let!(:song2) { create :song, album: album1 }
  let!(:song3) { create :song, album: album2 }
  let!(:song4) { create :song, album: album3 }

  let!(:mass_retry_songs_form_params) {
    {
      admin_mass_retry_songs_form: {
        'delivery_type' => 'full_delivery',
        'song_ids_and_isrcs' => Song.where(id:[song1.id, song2.id, song3.id, song4.id]).pluck(:id).to_s[1..-2],
        'store_ids' => ["", "48"],
        "admin" => admin
      }
    }
  }

  context "#create" do

    before(:each) do
      login_as admin

      all_songs = Song.where(id:[song1.id, song2.id, song3.id, song4.id])
      allow_any_instance_of(Admin::MassRetrySongsForm).to receive(:songs).and_return(all_songs)
    end


    it ".song_ids_by_album" do
      song_ids = [song1.id, song2.id, song3.id, song4.id]
      expect(
        described_class.new.instance_eval{ song_ids_by_album(Song.where(id: song_ids)) }
      ).to eq(
        {
          album1.id => [song1.id, song2.id],
          album2.id => [song3.id],
          album3.id => [song4.id]
        }
      )
    end

    it ".save" do
      mass_retry_song_form = instance_double(Admin::MassRetrySongsForm)
      params = ActionController::Parameters.new(mass_retry_songs_form_params)
      permitted = params.require(:admin_mass_retry_songs_form)
        .permit(:delivery_type, :song_ids_and_isrcs, :takedown, store_ids: [])
        .merge(admin: admin)

      expect(Admin::MassRetrySongsForm).to receive(:new).with(permitted).once.and_return(mass_retry_song_form)
      expect(mass_retry_song_form).to receive(:save).once

      post :create, params: mass_retry_songs_form_params
    end

    it ".notice" do
      post :create, params: mass_retry_songs_form_params

      expect(flash[:notice]).not_to be_nil
      expect(flash[:error]).to be_nil
    end

    it ".error" do
      mass_retry_song_form = instance_double(Admin::MassRetrySongsForm)
      params = ActionController::Parameters.new(mass_retry_songs_form_params)
      permitted = params.require(:admin_mass_retry_songs_form)
        .permit(:delivery_type, :song_ids_and_isrcs, :takedown, store_ids: [])
        .merge(admin: admin)
      allow(Admin::MassRetrySongsForm).to receive(:new).
        with(permitted).once.and_return(mass_retry_song_form)
      allow(mass_retry_song_form).to receive(:save).once.and_return(false)

      post :create, params: mass_retry_songs_form_params

      expect(flash[:notice]).to be_nil
      expect(flash[:error]).not_to be_nil
    end

    it ".redirect_to admin_mass_retry_songs_path" do
      post :create, params: mass_retry_songs_form_params

      expect(response).to redirect_to(admin_mass_retry_songs_path(takedown: nil))
    end
  end

  describe ".encumbrance_info" do
    before(:each) do
      login_as admin
    end
    let(:album1) {create(:album, :with_songs)}
    let(:album2) {create(:album, :with_songs)}

    it "should fetch album ids if album person has encumbrance info" do
      create(:encumbrance_summary, person: album1.person)
      post :encumbrance_info, params: {admin_mass_retry_songs_form: {song_ids_and_isrcs: "#{album1.songs.last.id}, #{album2.id}"}}
      response_body = JSON.parse(response.body)
      expect(response_body['song_ids_and_isrcs']).to eq(["#{album1.songs.last.id}"])
    end

    it "should return empty array if person doens't have encumbrance" do
      post :encumbrance_info, params: {admin_mass_retry_songs_form: {song_ids_and_isrcs: "#{album1.songs.last.id}, #{album2.id}"}}
      response_body = JSON.parse(response.body)
      expect(response_body['song_ids_and_isrcs']).to eq([])
    end

    it "should return empty array if person doens't have encumbrance" do
      create(:encumbrance_summary, person: album2.person)
      post :encumbrance_info, params: {admin_mass_retry_songs_form: {song_ids_and_isrcs: "#{album1.songs.last.id}, #{album2.songs.last.isrc}"}}
      response_body = JSON.parse(response.body)
      expect(response_body['song_ids_and_isrcs']).to eq(["#{album2.songs.last.isrc}"])
    end

    it "should return empty array params is empty" do
      post :encumbrance_info, params: {admin_mass_retry_songs_form: {song_ids_and_isrcs: ""}}
      response_body = JSON.parse(response.body)
      expect(response_body['song_ids_and_isrcs']).to eq([])
    end

  end

end
